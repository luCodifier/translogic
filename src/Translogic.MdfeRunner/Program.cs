﻿namespace Translogic.MdfeRunner
{
	using Topshelf;
    using Translogic.Modules.Core.Spd.Data.Enums;

    /// <summary>
    /// Classe estática que inicia o programa
    /// </summary>
    public static class Program
	{
		#region Métodos estáticos para inicialização
		/// <summary>|
		/// The main entry point for the application.
		/// </summary>
		/// <param name="args">Argumentos enviados ao programa</param>
		private static void Main(string[] args)
		{
			Sis.Inicializar(EnumSistema.MdfeRunner, "Job");

			HostFactory.Run(x =>
			{
				x.Service<MdfeRunnerSetupService>(s =>
				{
					s.ConstructUsing(name => new MdfeRunnerSetupService());
					s.WhenStarted(tc => tc.Start());
					s.WhenStopped(tc => tc.Stop());
				});

				x.RunAsLocalSystem();
				x.SetDescription("Serviço do MDF-e Runner");
				x.SetDisplayName("Translogic.MdfeRunner");
				x.SetServiceName("Translogic.MdfeRunner");
			});
		}
		#endregion
	}
}
