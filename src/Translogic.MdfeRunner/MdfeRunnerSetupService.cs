﻿namespace Translogic.MdfeRunner
{
	using System;
	
	using System.Configuration;
	using System.Diagnostics;
	using Castle.MicroKernel.Registration;
	using Core;
	using Services;
	using Services.Commands;

	/// <summary>
	/// Serviço de configuração do MDF-e Runner
	/// </summary>
	public class MdfeRunnerSetupService
	{
		private const string EventSource = "Translogic";

		private bool _senderConfigService;
		private bool _receiverConfigService;
		private bool _importerPdfFileService;

		/// <summary>
		/// Método chamado na inicialização do serviço
		/// </summary>
		public void Start()
		{
            try
            {
                if (!EventLog.SourceExists(EventSource))
                {
                    EventLog.CreateEventSource(EventSource, "Application");
                }

                // Inicializa as variaveis
                Initialize();
                TranslogicStarter.Initialize();
                TranslogicStarter.SetupForJobRunner();

                TranslogicContainer container = TranslogicStarter.Container;

                /* Registrar os componentes */
                container.Register(
                    Component.For<ICommandMdfe>().ImplementedBy<GravarEnvioMdfeCommand>().Named("EFA").LifeStyle.PerThread,
                    Component.For<ICommandMdfe>().ImplementedBy<GravarEncerramentoMdfeCommand>().Named("EFE").LifeStyle.PerThread,
                    Component.For<ICommandMdfe>().ImplementedBy<GravarCancelamentoMdfeCommand>().Named("EFC").LifeStyle.PerThread,
                    Component.For<SenderMdfeManagerService>().LifeStyle.PerThread,
                    Component.For<ReceiverMdfeManagerService>().LifeStyle.PerThread,
                    Component.For<ImporterPdfMdfeManagerService>().LifeStyle.PerThread,
                    Component.For<MdfeRunnerLogService>()
                );

                // Inicializa o serviço de log
                MdfeRunnerLogService mdfeRunnerLogService = container.Resolve<MdfeRunnerLogService>();
                mdfeRunnerLogService.InserirLogInfo("MdfeRunnerService", "Iniciado o serviço MdfeRunner");

                if (_senderConfigService)
                {
                    // Inicia gerente de sender
                    SenderMdfeManagerService senderMdfeManagerService = container.Resolve<SenderMdfeManagerService>();
                    senderMdfeManagerService.Inicializar();
                }

                if (_receiverConfigService)
                {
                    ReceiverMdfeManagerService receiverMdfeManagerService = container.Resolve<ReceiverMdfeManagerService>();
                    receiverMdfeManagerService.Inicializar();
                }

                if (_importerPdfFileService)
                {
                    ImporterPdfMdfeManagerService importerPdfFile = container.Resolve<ImporterPdfMdfeManagerService>();
                    importerPdfFile.Inicializar();
                }
            }
            catch (Exception ex)
            {
                if (!EventLog.SourceExists(EventSource))
                {
                    EventLog.CreateEventSource(EventSource, "Application");
                }

                EventLog.WriteEntry(EventSource, "MdfeRunner Start Error:" + ex, EventLogEntryType.Error);
                throw;
            }
		}

		/// <summary>
		/// Método chamado no término do serviço
		/// </summary>
		public void Stop()
		{
		}

		/// <summary>
		/// Inicializa com as variaveis do ambiente (arquivo de configuração)
		/// </summary>
		protected void Initialize()
		{
			Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
			_senderConfigService = Boolean.Parse(config.AppSettings.Settings["SenderConfigService"].Value);
			_receiverConfigService = Boolean.Parse(config.AppSettings.Settings["ReceiverConfigService"].Value);
			_importerPdfFileService = Boolean.Parse(config.AppSettings.Settings["ImporterPdfFileService"].Value);
		}
	}
}
