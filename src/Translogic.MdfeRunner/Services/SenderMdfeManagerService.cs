﻿namespace Translogic.MdfeRunner.Services
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;
    using System.Net;
    using ALL.Core.Util;
    using Castle.Windsor;
    using Commands;
    using Core.Infrastructure.Threading;
    using Modules.Core.Domain.Model.Diversos.Mdfe;
    using Modules.Core.Domain.Model.FluxosComerciais.Mdfes;
    using Modules.Core.Domain.Services.Mdfes;

    /// <summary>
    /// Serviço de enfileiramento de geração/envio de MDF-e
    /// </summary>
    public class SenderMdfeManagerService
    {
        private const string NomeIntervaloTimer = "IntervaloEnvioEmSegundos";
        private const string ChaveIndAmbienteSefaz = "IndAmbienteSefaz";
        private readonly MdfeRunnerLogService _mdfeRunnerLogService;
        private readonly IWindsorContainer _container;
        private readonly MdfeService _mdfeService;
        private bool _executando;
        private int _indAmbienteSefaz;

        /// <summary>
        /// Initializes a new instance of the <see cref="SenderMdfeManagerService"/> class.
        /// </summary>
        /// <param name="mdfeRunnerLogService">Serviço do log do MDF-e Runner injetado</param>
        /// <param name="container">Container injetado</param>
        /// <param name="mdfeService">Serviço do MDF-e injetado</param>
        public SenderMdfeManagerService(MdfeRunnerLogService mdfeRunnerLogService, IWindsorContainer container, MdfeService mdfeService)
        {
            _mdfeRunnerLogService = mdfeRunnerLogService;
            _container = container;
            _mdfeService = mdfeService;
            _mdfeRunnerLogService.InserirLogInfo("SenderMdfeManagerService", "criado o serviço");
        }

        /// <summary> 
        /// Inicializa o timer
        /// </summary>
        public void Inicializar()
        {
            int tempoSegundos;
            try
            {
                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                string aux = config.AppSettings.Settings[NomeIntervaloTimer].Value;
                Int32.TryParse(aux, out tempoSegundos);
                if (tempoSegundos.Equals(0))
                {
                    tempoSegundos = 30;
                }

                aux = config.AppSettings.Settings[ChaveIndAmbienteSefaz].Value;
                if (!Int32.TryParse(aux, out _indAmbienteSefaz))
                {
                    _indAmbienteSefaz = 2;
                }
            }
            catch (Exception)
            {
                tempoSegundos = 30;
            }

            _executando = false;
#if DEBUG
            if (Environment.MachineName == "PULSAR")
            {
                Executar();
                return;
            }
#endif

            new TranslogicThreadManager(Executar, TimeSpan.FromSeconds(tempoSegundos)).Start();

            _mdfeRunnerLogService.InserirLogInfo("SenderMdfeManagerService", string.Format("tempo de execução de {0} em {0} segundos", tempoSegundos));
        }

        /// <summary>
        /// Executa a geração do MDF-e
        /// </summary>
        protected void Executar()
        {
            if (_executando)
            {
                return;
            }

            try
            {
                _mdfeRunnerLogService.InserirLogInfo("SenderMdfeManagerService", "executando o processo de varredura dos dados");

                _executando = true;

                string hostName = Dns.GetHostName();

                IList<Mdfe> listaCtesNaoProcessados = _mdfeService.ObterMdfesProcessarEnvio(_indAmbienteSefaz);
                _mdfeRunnerLogService.InserirLogInfo("SenderMdfeManagerService", string.Format("enviando para o pooling de envio {0} Mdfes", listaCtesNaoProcessados.Count));

#if DEBUG2
                listaCtesNaoProcessados = listaCtesNaoProcessados.Where(p => p.Id == 1100525).ToList();
#endif

                _mdfeService.InserirPoolingEnvioMdfe(listaCtesNaoProcessados, hostName);

                IList<Mdfe> listaProcessamento = _mdfeService.ObterPoolingEnvioPorServidor(hostName);
#if DEBUG
                listaProcessamento = listaProcessamento.Where(p => p.Id == 1100525).ToList();
#endif

                _mdfeRunnerLogService.InserirLogInfo("SenderMdfeManagerService", string.Format("obtendo do pooling de envio {0} Ctes para serem processados", listaProcessamento.Count));

                foreach (Mdfe mdfe in listaProcessamento)
                {
                    try
                    {
                        _mdfeRunnerLogService.InserirLogInfo("SenderMdfeManagerService", string.Format("criando e colocando no pool a thread para o cte: {0} ", mdfe.Id));

                        string statusMdfe = Enum<SituacaoMdfeEnum>.GetDescriptionOf(mdfe.SituacaoAtual);
                        ICommandMdfe command = _container.Resolve<ICommandMdfe>(statusMdfe);

                        // Gera as informações de dispatch
                        GravarEnvioMdfeDispatchCommand dispatchCommand = new GravarEnvioMdfeDispatchCommand
                        {
                            Mdfe = mdfe,
                            CommandMdfe = command,
                            RunnerLogService = _mdfeRunnerLogService
                        };

                        dispatchCommand.Executar();
                    }
                    catch (Exception ex)
                    {
                        _mdfeRunnerLogService.InserirLogErro("SenderMdfeManagerService", string.Format("{0}", ex.Message));
                    }
                }

                _mdfeRunnerLogService.InserirLogInfo("SenderMdfeManagerService", string.Format("SenderMdfeManagerService: removendo do pooling de envio {0} MDF-es processados", listaProcessamento.Count));
                _mdfeService.RemoverPoolingEnvioMdfe(listaProcessamento);
            }
            catch (Exception ex)
            {
                _mdfeRunnerLogService.InserirLogErro("SenderMdfeManagerService", string.Format("{0}", ex.Message));
            }
            finally
            {
                _executando = false;
            }
        }
    }
}