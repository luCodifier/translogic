﻿namespace Translogic.MdfeRunner.Services.Commands
{
	using System;
	using Modules.Core.Domain.Model.Diversos.Mdfe;
	using Modules.Core.Domain.Model.FluxosComerciais.Mdfes;
	using Modules.Core.Domain.Services.Mdfes;

	/// <summary>
	/// Comando para a gravação do cancelamento do MDF-e
	/// </summary>
	public class GravarCancelamentoMdfeCommand : ICommandMdfe
	{
		private readonly GravarConfigDadosCancelamentoMdfeService _gravarConfigDadosCancelamentoMdfeService;
		private readonly MdfeService _mdfeService;
		private readonly MdfeRunnerLogService _mdfeRunnerLogService;

		/// <summary>
		/// Initializes a new instance of the <see cref="GravarCancelamentoMdfeCommand"/> class.
		/// </summary>
		/// <param name="gravarConfigDadosCancelamentoMdfeService">Serviço de gravação dos dados de cancelamento no banco da Config</param>
		/// <param name="mdfeService">Serviço do MDF-e</param>
		/// <param name="mdfeRunnerLogService">Serviço de log do MDF-e Runner</param>
		public GravarCancelamentoMdfeCommand(GravarConfigDadosCancelamentoMdfeService gravarConfigDadosCancelamentoMdfeService, MdfeService mdfeService, MdfeRunnerLogService mdfeRunnerLogService)
		{
			_gravarConfigDadosCancelamentoMdfeService = gravarConfigDadosCancelamentoMdfeService;
			_mdfeService = mdfeService;
			_mdfeRunnerLogService = mdfeRunnerLogService;
		}

		/// <summary>
		/// Executa a implementação do comando 
		/// </summary>
		/// <param name="mdfe">MDF-e a ser processado. </param>
		public void Executar(Mdfe mdfe)
		{
			try
			{
				// _gravarConfigDadosCancelamentoMdfeService.Executar(mdfe);

				_mdfeService.InserirMdfeCancelamentoFilaProcessamentoConfig(mdfe);
			}
			catch (Exception ex)
			{
				_mdfeService.MudarSituacaoMdfe(mdfe, SituacaoMdfeEnum.ErroAutorizadoReEnvio, null, new MdfeStatusRetorno { Id = 14 }, "Alterado a situação do MDF-e para ERRO", ex.Message);
				_mdfeRunnerLogService.InserirLogErro("GravarCancelamentoMdfeCommand", string.Format("{0}", ex.Message));

				throw;
			}
		}
	}
}