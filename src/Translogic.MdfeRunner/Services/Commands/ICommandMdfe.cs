﻿namespace Translogic.MdfeRunner.Services.Commands
{
	using Modules.Core.Domain.Model.FluxosComerciais.Mdfes;

	/// <summary>
	/// Interface do comando do MDF-e
	/// </summary>
	public interface ICommandMdfe
	{
		/// <summary>
		/// Executa a implementação do comando 
		/// </summary>
		/// <param name="mdfe">MDF-e a ser processado. </param>
		void Executar(Mdfe mdfe);
	}
}