namespace Translogic.MdfeRunner.Services.Commands
{
	using System;
	using Translogic.Modules.Core.Domain.Model.Diversos.Mdfe;
	using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes;
	using Translogic.Modules.Core.Domain.Services.Mdfes;

	/// <summary>
	/// Command para Encerrar um MDF-e
	/// </summary>
	public class GravarEncerramentoMdfeCommand : ICommandMdfe
	{
		private readonly MdfeService _mdfeService;
		private readonly MdfeRunnerLogService _mdfeRunnerLogService;

		/// <summary>
		/// Initializes a new instance of the <see cref="GravarCancelamentoMdfeCommand"/> class.
		/// </summary>
		/// <param name="mdfeService">Servi�o do MDF-e</param>
		/// <param name="mdfeRunnerLogService">Servi�o de log do MDF-e Runner</param>
		public GravarEncerramentoMdfeCommand(MdfeService mdfeService, MdfeRunnerLogService mdfeRunnerLogService)
		{
			_mdfeService = mdfeService;
			_mdfeRunnerLogService = mdfeRunnerLogService;
		}

		/// <summary>
		/// Executa a implementa��o do comando 
		/// </summary>
		/// <param name="mdfe">MDF-e a ser processado. </param>
		public void Executar(Mdfe mdfe)
		{
			try
			{
				_mdfeService.InserirMdfeEncerramentoFilaProcessamentoConfig(mdfe);
			}
			catch (Exception ex)
			{
				_mdfeService.MudarSituacaoMdfe(mdfe, SituacaoMdfeEnum.ErroAutorizadoReEnvio, null, new MdfeStatusRetorno { Id = 14 }, "Alterado a situa��o do MDF-e para ERRO", ex.Message);
				_mdfeRunnerLogService.InserirLogErro("GravarEncerramentoMdfeCommand", string.Format("{0}", ex.Message));

				throw;
			}
		}
	}
}