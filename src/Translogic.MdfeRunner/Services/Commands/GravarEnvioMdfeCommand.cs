﻿namespace Translogic.MdfeRunner.Services.Commands
{
	using System;
	using Modules.Core.Domain.Model.Diversos.Mdfe;
	using Modules.Core.Domain.Model.FluxosComerciais.Mdfes;
	using Modules.Core.Domain.Services.Mdfes;

	/// <summary>
	/// Comando para a gravação dos dados de envio do MDF-e
	/// </summary>
	public class GravarEnvioMdfeCommand : ICommandMdfe
	{
		private readonly GravarConfigDadosEnvioMdfeService _gravarConfigDadosEnvioMdfeService;
		private readonly MdfeService _mdfeService;
		private readonly MdfeRunnerLogService _mdfeRunnerLogService;

		/// <summary>
		/// Initializes a new instance of the <see cref="GravarEnvioMdfeCommand"/> class.
		/// </summary>
		/// <param name="gravarConfigDadosEnvioMdfeService">Serviço de gravação dos dados no banco da Config</param>
		/// <param name="mdfeService">Serviço do MDF-e</param>
		/// <param name="mdfeRunnerLogService">Serviço do log do MDF-e</param>
		public GravarEnvioMdfeCommand(GravarConfigDadosEnvioMdfeService gravarConfigDadosEnvioMdfeService, MdfeService mdfeService, MdfeRunnerLogService mdfeRunnerLogService)
		{
			_gravarConfigDadosEnvioMdfeService = gravarConfigDadosEnvioMdfeService;
			_mdfeService = mdfeService;
			_mdfeRunnerLogService = mdfeRunnerLogService;
		}

		/// <summary>
		/// Executa o comando de envio dos dados
		/// </summary>
		/// <param name="mdfe">MDF-e a ser enviado</param>
		public void Executar(Mdfe mdfe)
		{
			try
			{
				// Executa a gravação do arquivo
				 _gravarConfigDadosEnvioMdfeService.Executar(mdfe);

				// Insere o MDF-e na fila de processamento da config
				 _mdfeService.InserirMdfeEnvioFilaProcessamentoConfig(mdfe);
			}
			catch (Exception ex)
			{
				_mdfeService.MudarSituacaoMdfe(mdfe, SituacaoMdfeEnum.ErroAutorizadoReEnvio, null, new MdfeStatusRetorno { Id = 14 }, "Alterado a situação do MDF-e para ERRO", ex.Message);
				_mdfeRunnerLogService.InserirLogErro("GravarEnvioMdfeCommand", string.Format("{0}", ex.Message));
			}
		}
	}
}