﻿namespace Translogic.MdfeRunner.Services
{
	using System;
	using System.Collections.Generic;
	using System.Configuration;
	using System.Net;
	using Castle.Windsor;
	using Core.Infrastructure.Threading;
	using Modules.Core.Domain.Model.FluxosComerciais.Mdfes;
	using Modules.Core.Domain.Services.Mdfes;

	/// <summary>
	/// Serviço de gerenciamento de retornos de MDF-e
	/// </summary>
	public class ReceiverMdfeManagerService
	{
		private const string NomeIntervaloTimer = "IntervaloRecebimentoEmSegundos";
		private const string ChaveIndAmbienteSefaz = "IndAmbienteSefaz";

		private readonly MdfeService _mdfeService;
		private readonly MdfeRunnerLogService _mdfeRunnerLogService;
		private readonly IWindsorContainer _container;
		private bool _executando;
		private int _indAmbienteSefaz;

		/// <summary>
		/// Initializes a new instance of the <see cref="ReceiverMdfeManagerService"/> class.
		/// </summary>
		/// <param name="mdfeService">Serviço do MDF-e</param>
		/// <param name="mdfeRunnerLogService">Serviço de log do MDF-e injetado</param>
		/// <param name="container">Container injetado</param>
		public ReceiverMdfeManagerService(MdfeService mdfeService, MdfeRunnerLogService mdfeRunnerLogService, IWindsorContainer container)
		{
			_container = container;
			_mdfeService = mdfeService;
			_mdfeRunnerLogService = mdfeRunnerLogService;
			_mdfeRunnerLogService.InserirLogInfo("ReceiverMdfeManagerService", "criado o serviço");
		}

		/// <summary>
		/// Inicializa o timer
		/// </summary>
		public void Inicializar()
		{
			int tempoSegundos;
			try
			{
				Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
				string aux = config.AppSettings.Settings[NomeIntervaloTimer].Value;
				Int32.TryParse(aux, out tempoSegundos);
				if (tempoSegundos.Equals(0))
				{
					tempoSegundos = 30;
				}

				aux = config.AppSettings.Settings[ChaveIndAmbienteSefaz].Value;
				if (!Int32.TryParse(aux, out _indAmbienteSefaz))
				{
					_indAmbienteSefaz = 2;
				}
			}
			catch (Exception)
			{
				tempoSegundos = 30;
				_indAmbienteSefaz = 2;
			}

			_executando = false;
			new TranslogicThreadManager(Executar, TimeSpan.FromSeconds(tempoSegundos)).Start();

			_mdfeRunnerLogService.InserirLogInfo("ReceiverMdfeManagerService", string.Format("tempo de execução de {0} em {0} segundos", tempoSegundos));
		}

		/// <summary>
		/// Executa o recebimento do MDF-e
		/// </summary>
		protected void Executar()
		{
			if (_executando)
			{
				return;
			}

			try
			{
				_mdfeRunnerLogService.InserirLogInfo("ReceiverMdfeManagerService", "executando o processo de varredura dos dados");

				_executando = true;

				string hostName = Dns.GetHostName();
				IList<MdfeInterfaceRecebimentoConfig> listaNaoProcessados = _mdfeService.ObterMdfeProcessarRecebimento(_indAmbienteSefaz);

				_mdfeRunnerLogService.InserirLogInfo("ReceiverMdfeManagerService", string.Format("ReceiverMdfeManagerService: enviando para o pooling de recebimento {0} Mdfes", listaNaoProcessados.Count));
				_mdfeService.InserirPoolingRecebimentoMdfe(listaNaoProcessados, hostName);

				IList<MdfeRecebimentoPooling> listaProcessamento = _mdfeService.ObterPoolingRecebimentoPorServidor(hostName);
				_mdfeRunnerLogService.InserirLogInfo("ReceiverMdfeManagerService", string.Format("obtendo do pooling de recebimento {0} Mdfe para serem processados", listaProcessamento.Count));

				foreach (MdfeRecebimentoPooling mdfeRecebimentoPooling in listaProcessamento)
				{
					try
					{
						_mdfeRunnerLogService.InserirLogInfo("ReceiverMdfeManagerService", string.Format("criando e colocando no pool a thread para o cte: {0} ", mdfeRecebimentoPooling.Id));

						ProcessarRecebimentoMdfeDispatchCommand command = new ProcessarRecebimentoMdfeDispatchCommand(_container, _mdfeRunnerLogService)
						{
							MdfeRecebimentoPooling = mdfeRecebimentoPooling
						};

						command.Executar();
					}
					catch (Exception ex)
					{
						_mdfeRunnerLogService.InserirLogErro("ReceiverMdfeManagerService", string.Format("{0}", ex.Message));
					}
				}

				_mdfeRunnerLogService.InserirLogInfo("ReceiverMdfeManagerService", string.Format("removendo do pooling de recebimento {0} MDF-es processados", listaProcessamento.Count));
				_mdfeService.RemoverPoolingRecebimentoMdfe(listaProcessamento);
			}
			catch (Exception ex)
			{
				_mdfeRunnerLogService.InserirLogErro("ReceiverMdfeManagerService", string.Format("{0}", ex.Message));
			}
			finally
			{
				_executando = false;
			}
		}
	}
}