﻿namespace Translogic.MdfeRunner.Services
{
	using System;
	using Castle.Windsor;
	using Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Mdfes;
	using Modules.Core.Domain.Model.Diversos.Mdfe;
	using Modules.Core.Domain.Model.FluxosComerciais.Mdfes;
	using Modules.Core.Domain.Model.FluxosComerciais.Mdfes.Repositories;
	using Modules.Core.Domain.Services.Mdfes;

	/// <summary>
	/// Classe ProcessarRecebimentoMdfeDispatchCommand
	/// </summary>
	public class ProcessarRecebimentoMdfeDispatchCommand
	{
		private readonly MdfeService _mdfeService;
		private readonly MdfeRunnerLogService _mdfeRunnerLogService;

		/// <summary>
		/// Construtor da classe
		/// </summary>
		/// <param name="container">Objeto container.</param>
		/// <param name="mdfeRunnerLogService">Objeto Mdfe Runner Log. </param>
		public ProcessarRecebimentoMdfeDispatchCommand(IWindsorContainer container, MdfeRunnerLogService mdfeRunnerLogService)
		{
			_mdfeService = container.Resolve<MdfeService>();
			_mdfeRunnerLogService = mdfeRunnerLogService;
		}

		/// <summary>
		/// Propriedade do recebimento do Pooling
		/// </summary>
		public MdfeRecebimentoPooling MdfeRecebimentoPooling { get; set; }

		/// <summary>
		/// Executa o comando de processamento
		/// </summary>
		public void Executar()
		{
			ProcessarRetorno();
		}

		private void ProcessarRetorno()
		{
			if (MdfeRecebimentoPooling != null)
			{
				try
				{
					 _mdfeRunnerLogService.InserirLogInfo("ProcessarRecebimentoMdfeDispatchCommand", string.Format("Processando o recebimento do Mdfe {0}", MdfeRecebimentoPooling.Id));

					if (_mdfeService.ProcessarRetornoPorItemRecebimento(MdfeRecebimentoPooling))
					{
						_mdfeRunnerLogService.InserirLogInfo("ProcessarRecebimentoMdfeDispatchCommand", string.Format("Apagando o Mdfe {0} da interface", MdfeRecebimentoPooling.Id));
					}
					else
					{
						_mdfeRunnerLogService.InserirLogInfo("ProcessarRecebimentoMdfeDispatchCommand", string.Format("Não processado / encontrado o Cte {0} da interface", MdfeRecebimentoPooling.Id));
					}
				}
				catch (Exception ex)
				{
					Mdfe mdfeAux = new Mdfe { Id = MdfeRecebimentoPooling.Id };
					_mdfeService.MudarSituacaoMdfe(mdfeAux, SituacaoMdfeEnum.Erro, null, new MdfeStatusRetorno { Id = 14 }, "Alterado a situação do MDF-e para ERRO", ex.Message);
					_mdfeRunnerLogService.InserirLogErro("ProcessarRecebimentoMdfeDispatchCommand", string.Format("{0}", ex.Message));
				}
			}
		}
	}
}