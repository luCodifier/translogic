﻿namespace Translogic.MdfeRunner.Services
{
	using System;
	using System.Configuration;
	using System.Net;
	using System.Text;
	using Modules.Core.Domain.Model.FluxosComerciais.Mdfes.Repositories;

	/// <summary>
	/// serviço de gravação dos logs do MDF-e
	/// </summary>
	public class MdfeRunnerLogService
	{
		private readonly IMdfeRunnerLogRepository _mdfeRunnerLogRepository;
		private readonly bool _logInformacao;
		private readonly bool _logErro;

		/// <summary>
		/// Initializes a new instance of the <see cref="mdfeRunnerLogRepository"/> class.
		/// </summary>
		/// <param name="mdfeRunnerLogRepository">Repositório MDF-e Runner Log injetado</param>
		public MdfeRunnerLogService(IMdfeRunnerLogRepository mdfeRunnerLogRepository)
		{
			_mdfeRunnerLogRepository = mdfeRunnerLogRepository;
			
			string informacao = ConfigurationManager.AppSettings["LogInformacao"];
			string erro = ConfigurationManager.AppSettings["LogErro"];
			_logInformacao = ((informacao != null) && (informacao != string.Empty)) ? Convert.ToBoolean(informacao) : false;
			_logErro = ((erro != null) && (erro != string.Empty)) ? Convert.ToBoolean(erro) : false;
		}

		/// <summary>
		/// Insere o log de informação do runner
		/// </summary>
		/// <param name="nomeServico">Nome do serviço que está sendo logado</param>
		/// <param name="mensagem">Mensagem do log</param>
		public void InserirLogInfo(string nomeServico, string mensagem)
		{
			if (_logInformacao)
			{
				_mdfeRunnerLogRepository.InserirLogInfo(Dns.GetHostName(), nomeServico, mensagem);
			}
		}

		/// <summary>
		/// Insere o log de informação do runner
		/// </summary>
		/// <param name="hostName">Nome do host que está hospedando o serviço</param>
		/// <param name="nomeServico">Nome do serviço que está sendo logado</param>
		/// <param name="mensagem">Mensagem do log</param>
		public void InserirLogInfo(string hostName, string nomeServico, string mensagem)
		{
			if (_logInformacao)
			{
				_mdfeRunnerLogRepository.InserirLogInfo(hostName, nomeServico, mensagem);
			}
		}

		/// <summary>
		/// Insere log de informação do runner
		/// </summary>
		/// <param name="hostName">Nome do host que está hospedando o serviço</param>
		/// <param name="nomeServico">Nome do serviço que está sendo logado</param>
		/// <param name="mensagem">Mensagem do log</param>
		public void InserirLogInfo(string hostName, string nomeServico, StringBuilder mensagem)
		{
			if (_logInformacao)
			{
				_mdfeRunnerLogRepository.InserirLogInfo(hostName, nomeServico, mensagem);
			}
		}

		/// <summary>
		/// Insere o log de erro do runner
		/// </summary>
		/// <param name="hostName">Nome do host que está hospedando o serviço</param>
		/// <param name="nomeServico">Nome do serviço a ser logado</param>
		/// <param name="mensagem">Mensagem do log</param>
		public void InserirLogErro(string hostName, string nomeServico, string mensagem)
		{
			if (_logErro)
			{
				_mdfeRunnerLogRepository.InserirLogErro(hostName, nomeServico, mensagem);
			}
		}

		/// <summary>
		/// Insere o log de erro do runner
		/// </summary>
		/// <param name="hostName">Nome do host que está hospedando o serviço</param>
		/// <param name="nomeServico">Nome do serviço a ser logado</param>
		/// <param name="mensagem">Mensagem do log</param>
		public void InserirLogErro(string hostName, string nomeServico, StringBuilder mensagem)
		{
			if (_logErro)
			{
				_mdfeRunnerLogRepository.InserirLogInfo(hostName, nomeServico, mensagem);
			}
		}
		
		/// <summary>
		/// Insere o log de erro do runner
		/// </summary>
		/// <param name="nomeServico">Nome do serviço que está sendo logado</param>
		/// <param name="mensagem">Mensagem do log</param>
		public void InserirLogErro(string nomeServico, string mensagem)
		{
			if (_logErro)
			{
				_mdfeRunnerLogRepository.InserirLogErro(Dns.GetHostName(), nomeServico, mensagem);
			}
		}
	}
}