namespace Translogic.MdfeRunner.Services
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel;
	using System.Configuration;
	using System.Net;
	using System.Threading;
	using Castle.Windsor;
	using Translogic.Modules.Core.Domain.Model.Codificador;
	using Translogic.Modules.Core.Domain.Model.Codificador.Repositories;
	using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes;
	using Translogic.Modules.Core.Domain.Services.Mdfes;

	/// <summary>
	/// Classe ImporterPdfMdfeManagerService.
	/// </summary>
	public class ImporterPdfMdfeManagerService
	{
		private const string NomeIntervaloTimer = "IntervaloTimerImportadorEmSegundos";
		private readonly string _chavePdfPath = "MDFE_PDF_PATH";
		private readonly MdfeService _mdfeService;
		private readonly MdfeRunnerLogService _mdfeRunnerLogService;
		private readonly IWindsorContainer _container;
		private readonly IConfiguracaoTranslogicRepository _configuracaoTranslogicRepository;
		private bool _executando;

		/// <summary>
		/// Initializes a new instance of the <see cref="ImporterPdfMdfeManagerService"/> class.
		/// </summary>
		/// <param name="mdfeService"> Servi�o do Mdfe Injetado</param>
		/// <param name="mdfeRunnerLogService">Servi�o do log do Mdfe runner injetado </param>
		/// <param name="container"> Container injetado </param>
		/// <param name="configuracaoTranslogicRepository"> Repositorio de configura��o do translogic injetado </param>
		public ImporterPdfMdfeManagerService(MdfeService mdfeService, MdfeRunnerLogService mdfeRunnerLogService, IWindsorContainer container, IConfiguracaoTranslogicRepository configuracaoTranslogicRepository)
		{
			_container = container;
			_mdfeService = mdfeService;
			_mdfeRunnerLogService = mdfeRunnerLogService;
			_configuracaoTranslogicRepository = configuracaoTranslogicRepository;
			_mdfeRunnerLogService.InserirLogInfo("ImporterPdfMdfeManagerService", "criado o servi�o");
		}
		
		/// <summary>
		/// Inicializa o timer
		/// </summary>
		public void Inicializar()
		{
			int tempoSegundos;
			try
			{
				Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
				string aux = config.AppSettings.Settings[NomeIntervaloTimer].Value;
				Int32.TryParse(aux, out tempoSegundos);
				if (tempoSegundos.Equals(0))
				{
					tempoSegundos = 30;
				}
			}
			catch (Exception)
			{
				tempoSegundos = 30;
			}

			_executando = false;
			new ThreadIntervalPdfImporter(ExecutarImportacaoSingleThread, TimeSpan.FromSeconds(tempoSegundos)).Start();

			_mdfeRunnerLogService.InserirLogInfo("ImporterPdfFileManagerService", string.Format("tempo de execu��o de {0} em {0} segundos", tempoSegundos));
		}

		/// <summary>
		/// Executa a importa��o dos arquivos de pdf - Single Thread
		/// </summary>
		public void ExecutarImportacaoSingleThread()
		{
			if (_executando)
			{
				return;
			}

			try
			{
				ConfiguracaoTranslogic pdfPath = _configuracaoTranslogicRepository.ObterPorId(_chavePdfPath);

				_mdfeRunnerLogService.InserirLogInfo("ImporterPdfFileManagerService", "executando o processo de varredura dos dados");
				_executando = true;

				string hostName = Dns.GetHostName();
				IList<MdfeInterfacePdfConfig> listaMdfesNaoImportados = _mdfeService.ObterMdfesImportarPdf(hostName);

				_mdfeRunnerLogService.InserirLogInfo("ImporterPdfMdfeFileManagerService", string.Format("importando os {0} PDF para o banco de dados", listaMdfesNaoImportados.Count));

				foreach (MdfeInterfacePdfConfig mdfeInterfacePdfConfig in listaMdfesNaoImportados)
				{
					_mdfeRunnerLogService.InserirLogInfo("ImporterPdfFileManagerService", string.Format("colocando na fila de processamento para importa��o o Cte: {0} ", mdfeInterfacePdfConfig.Mdfe.Id));

					ProcessoImportacaoPdfMdfe processoImportacao = new ProcessoImportacaoPdfMdfe(_container, _mdfeRunnerLogService, mdfeInterfacePdfConfig, pdfPath);
					processoImportacao.Executar();
				}
			}
			catch (Exception ex)
			{
				_mdfeRunnerLogService.InserirLogErro("ImporterPdfFileManagerService", string.Format("{0}", ex.Message));
			}
			finally
			{
				_executando = false;
			}
		}
	}

	/// <summary>
	/// Executa o delegate a cada intervalo
	/// </summary>
	public class ThreadIntervalPdfImporter
	{
		private readonly BackgroundWorker _worker;
		private bool _executarImediatamente;

		/// <summary>
		/// Construtor padr�o
		/// </summary>
		/// <param name="delegateInterval">O que ser� executado a cada intervalo</param>
		/// <param name="interval">Intervalo que ser� executado o delegate</param>
		public ThreadIntervalPdfImporter(Action delegateInterval, TimeSpan interval)
		{
			_worker = new BackgroundWorker();
			_worker.WorkerSupportsCancellation = true;

			_worker.DoWork += delegate
			{
				if (_executarImediatamente)
				{
					try
					{
						delegateInterval.Invoke();
					}
					catch
					{
					}
				}

				while (true)
				{
					if (_worker.CancellationPending)
					{
						return;
					}

					Thread.Sleep(interval);

					if (_worker.CancellationPending)
					{
						return;
					}

					try
					{
						delegateInterval.Invoke();
					}
					catch
					{
					}
				}
			};
		}

		/// <summary>
		/// Inicia a execu��o
		/// </summary>
		public void Start()
		{
			Start(false);
		}

		/// <summary>
		/// Inicia a execu��o
		/// </summary>
		/// <param name="executarImediatamente">Indica que � para executar imediatamente</param>
		public void Start(bool executarImediatamente)
		{
			_executarImediatamente = executarImediatamente;

			_worker.RunWorkerAsync();
		}

		/// <summary>
		/// Cancela a execu��o
		/// </summary>
		public void Stop()
		{
			_worker.CancelAsync();
		}
	}
}