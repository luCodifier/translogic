﻿namespace Translogic.MdfeRunner.Services
{
	using System;
	using Commands;
	using Modules.Core.Domain.Model.FluxosComerciais.Mdfes;

	/// <summary>
	/// Classe de preparo para o "dispatch" do commando
	/// </summary>
	public class GravarEnvioMdfeDispatchCommand
	{
		/// <summary>
		/// Propriedade do MDF-e
		/// </summary>
		public Mdfe Mdfe { get; set; }

		/// <summary>
		/// Commando a ser executado pelo Dispatch
		/// </summary>
		public ICommandMdfe CommandMdfe { get; set; }

		/// <summary>
		/// Log do MDF-e Runner
		/// </summary>
		public MdfeRunnerLogService RunnerLogService { get; set; }

		/// <summary>
		/// Executa o comando assinalado
		/// </summary>
		public void Executar()
		{
			try
			{
				DispatchCommand();
			}
			catch (Exception ex)
			{
				throw;
			}
		}

		private void DispatchCommand()
		{
			try
			{
				if (Mdfe != null)
				{
					RunnerLogService.InserirLogInfo("GravarEnvioMdfeDispatchCommand", string.Format("Despachando o comando para gravação dos dados do MDF-e {0}", Mdfe.Id));
					CommandMdfe.Executar(Mdfe);
				}
			}
			catch (Exception ex)
			{
				throw;
			}
		}
	}
}