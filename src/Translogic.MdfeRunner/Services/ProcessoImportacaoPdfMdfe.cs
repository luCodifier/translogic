namespace Translogic.MdfeRunner.Services
{
	using System.Threading;
	using Castle.Windsor;
	using Translogic.Modules.Core.Domain.Model.Codificador;
	using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes;
	using Translogic.Modules.Core.Domain.Services.Mdfes;

	/// <summary>
	/// Classe de Importacao de Pdf do MDFe
	/// </summary>
	public class ProcessoImportacaoPdfMdfe
	{
		private readonly MdfeService _mdfeService;
		private readonly MdfeRunnerLogService _mdfeRunnerLogService;
		private readonly ManualResetEvent _doneEvent;
		private readonly MdfeInterfacePdfConfig _itemInterface;
		private readonly ConfiguracaoTranslogic _pdfPath;

		/// <summary>
		/// Initializes a new instance of the <see cref="ProcessoImportacaoPdfMdfe"/> class.
		/// </summary>
		/// <param name="container">Container injetado.</param>
		/// <param name="doneEvent">The done event.</param>
		/// <param name="cteRunnerLogService">Serviço de log do runner</param>
		/// <param name="itemInterface">Item da interface de importação</param>
		/// <param name="pdfPath">Item de configuração do pdf path</param>
		public ProcessoImportacaoPdfMdfe(IWindsorContainer container, ManualResetEvent doneEvent, MdfeRunnerLogService cteRunnerLogService, MdfeInterfacePdfConfig itemInterface, ConfiguracaoTranslogic pdfPath)
		{
			_mdfeService = container.Resolve<MdfeService>();
			_doneEvent = doneEvent;
			_itemInterface = itemInterface;
			_mdfeRunnerLogService = cteRunnerLogService;
			_pdfPath = pdfPath;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="ProcessoImportacaoPdfMdfe"/> class.
		/// </summary>
		/// <param name="container">Container injetado.</param>
		/// <param name="cteRunnerLogService">Serviço de log do runner</param>
		/// <param name="itemInterface">Item da interface de importação</param>
		/// /// <param name="pdfPath">Item de configuração do pdf path</param>
		public ProcessoImportacaoPdfMdfe(IWindsorContainer container, MdfeRunnerLogService cteRunnerLogService, MdfeInterfacePdfConfig itemInterface, ConfiguracaoTranslogic pdfPath)
		{
			_mdfeService = container.Resolve<MdfeService>();
			_doneEvent = null;
			_itemInterface = itemInterface;
			_mdfeRunnerLogService = cteRunnerLogService;
			_pdfPath = pdfPath;
		}

		/// <summary>
		/// Executa no modo single thread
		/// </summary>
		public void Executar()
		{
			ProcessarImportacao();
		}

		/// <summary>
		/// Wrapper method for use with thread pool.
		/// </summary>
		/// <param name="threadContext"> The thread context. </param>
		public void ThreadPoolCallback(object threadContext)
		{
			try
			{
				ProcessarImportacao();
			}
			finally
			{
				_doneEvent.Set();
			}
		}

		private void ProcessarImportacao()
		{
			if (_itemInterface != null)
			{
				_mdfeService.ImportarArquivoPdf(_itemInterface, _pdfPath.Valor);
			}
		}
	}
}
