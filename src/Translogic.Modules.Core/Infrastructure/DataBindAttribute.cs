/*namespace Translogic.Modules.Core.Infrastructure
{
	using System;
	using System.Web.Mvc;
	using ALL.Core.AcessoDados;
	using Castle.Components.Binder;
	using Microsoft.Practices.ServiceLocation;
	using MvcContrib.Castle;

	/// <summary>
	/// Atributo de data bind herdado da castle
	/// </summary>
	public class DataBindAttribute : CastleBindAttribute
	{
		#region M�TODOS

		/// <summary>
		/// Faz o bind de um model
		/// </summary>
		/// <param name="controllerContext">Contexto do controller</param>
		/// <param name="bindingContext">Contexto do binding</param>
		/// <returns>Objeto do model</returns>
		public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
		{
			IDataBinder binder = LocateBinder(controllerContext);
			string prefix = Prefix ?? bindingContext.ModelName;
			CompositeNode compositeNode = new TreeBuilder().BuildSourceNode(GetStore(controllerContext));

			// Get from database
			object model = GetFromDatabase(bindingContext, binder, prefix, compositeNode);

			// And bind entity from request
			model = UpdateFromWeb(model, binder, prefix, compositeNode);

			return model;
		}

		private object GetFromDatabase(ModelBindingContext bindingContext, IDataBinder binder, string prefix, CompositeNode compositeNode)
		{
			var repository = ServiceLocator.Current.GetInstance<IRepository>();

			Type modelType = bindingContext.ModelType;

			Type idType = modelType.GetProperty("Id").PropertyType;

			object instance = null;

			string paramName = prefix + ".Id";

			try
			{
				if (binder.CanBindParameter(idType, paramName, compositeNode))
				{
					object id = binder.BindParameter(idType, paramName, compositeNode);
					instance = repository.ObterPorId(modelType, id);
				}
			}
			catch (BindingException)
			{
				instance = null;
			}

			if (instance == null)
			{
				instance = Activator.CreateInstance(modelType);
			}

			return instance;
		}

		private object UpdateFromWeb(object model, IDataBinder binder, string prefix, CompositeNode compositeNode)
		{
			binder.BindObjectInstance(model, prefix, Exclude, null, compositeNode);

			return model;
		}

		#endregion
	}
}*/