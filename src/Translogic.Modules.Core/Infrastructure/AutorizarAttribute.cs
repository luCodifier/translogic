﻿namespace Translogic.Modules.Core.Infrastructure
{
	using System;
	using System.Net;
	using System.Security.Principal;
	using System.Web;
	using System.Web.Mvc;
	using Controllers;
	using Microsoft.Practices.ServiceLocation;
	using Translogic.Modules.Core.Domain.Model.Acesso;
	using Translogic.Modules.Core.Domain.Services.Acesso;

	/// <summary>
	/// Atributo de autorização 
	/// </summary>
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = false)]
	public sealed class AutorizarAttribute : FilterAttribute, IAuthorizationFilter
	{
		private readonly ControleAcessoService _controleAcessoService = ServiceLocator.Current.GetInstance<ControleAcessoService>();
		private string _transacao, _acao;

		/// <summary>
		/// Transação a ser autorizada
		/// </summary>
		public string Transacao
		{
			get
			{
				return _transacao ?? String.Empty;
			}

			set
			{
				_transacao = value;
			}
		}

		/// <summary>
		/// Ação a ser autorizada
		/// </summary>
		public string Acao
		{
			get
			{
				return _acao ?? String.Empty;
			}

			set
			{
				_acao = value;
			}
		}

		/// <summary>
		/// Evento de autorizãção
		/// </summary>
		/// <param name="filterContext"> Filtro de contexto. </param>
		/// <exception cref="ArgumentNullException">
		/// </exception>
		public void OnAuthorization(AuthorizationContext filterContext)
		{
			if (filterContext == null)
			{
				throw new ArgumentNullException("filterContext");
			}

			HttpContextBase httpContext = filterContext.HttpContext;

			IPrincipal user = httpContext.User;
			if (!user.Identity.IsAuthenticated)
			{
				return;
			}

			if (string.IsNullOrEmpty(Transacao))
			{
				return;
			}

			bool autorizado;

			if (string.IsNullOrEmpty(Acao))
			{
				autorizado = _controleAcessoService.Autorizar(Transacao, ((UsuarioIdentity)httpContext.User.Identity).Usuario);
			}
			else
			{
				autorizado = _controleAcessoService.Autorizar(Transacao, Acao, ((UsuarioIdentity)httpContext.User.Identity).Usuario);
			}

			if (!autorizado)
			{
				ViewDataDictionary dictionary = new ViewDataDictionary();

				dictionary["usuario"] = ((UsuarioIdentity)filterContext.HttpContext.User.Identity).Usuario.Codigo;
				dictionary["transacao"] = Transacao;
				dictionary["acao"] = Acao;

				if (filterContext.RequestContext.HttpContext.Request.IsAjaxRequest())
				{
					filterContext.RequestContext.HttpContext.Response.StatusCode = (int)HttpStatusCode.Forbidden;
					JsonResult result = new JsonResult { Data = new { success = false, statusCode = (int)HttpStatusCode.Forbidden }, ContentType = "application/json", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
					filterContext.Result = result;
					return;
				}

				filterContext.Result = new ViewResult
										{
											ViewName = "AcessoNegado",
											MasterName = null,
											ViewData = dictionary
										};

				// filterContext.Result = new RedirectResult(string.Format("~/home/AcessoNegado.all?transacao={0}&acao={1}", Transacao, Acao));
			}
		}
	}
}
