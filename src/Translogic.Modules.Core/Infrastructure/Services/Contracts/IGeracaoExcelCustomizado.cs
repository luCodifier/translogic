﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Translogic.Modules.Core.Infrastructure.Services.Contracts
{
    public interface IGeracaoExcelCustomizado
    {
        Stream GerarRelatorioCustomizado(string tituloRelatorio,
                                        IList<string> filtros,
                                        IList<string> colunas);
        int ObterIndiceUltimaColuna();
        string ObterValorLinhaColuna(char coluna, object objetoValor);
    }
}
