﻿namespace Translogic.Modules.Core.Infrastructure.Services
{
    using System.Collections.Generic;
    using System.IO;
    using Translogic.Modules.Core.Infrastructure.Services.Contracts;

    public abstract class GeracaoExcelCustomizadoService : IGeracaoExcelCustomizado
    {

        public abstract Stream GerarRelatorioCustomizado(string tituloRelatorio,
                                                        IList<string> filtros,
                                                        IList<string> colunas);

        public abstract int ObterIndiceUltimaColuna();

        public abstract string ObterValorLinhaColuna(char coluna, object objetoValor);
    }
}