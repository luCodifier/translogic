namespace Translogic.Modules.Core.Infrastructure.Services
{
	using System;
	using System.Threading;
	using Castle.Core.Logging;
	using Castle.Facilities.NHibernateIntegration;
	using Castle.Windsor;
	using Interfaces.Jobs;
	using Interfaces.Jobs.Service;

	/// <summary>
	/// Servi�o para execu��o de jobs
	/// </summary>
	public class JobRunnerService : IJobRunnerService
	{
		private readonly IWindsorContainer _container;
		private readonly ISessionManager _sessionManager;
		private ILogger _logger = NullLogger.Instance;

		/// <summary>
		/// Construtor padr�o
		/// </summary>
		/// <param name="container">
		/// Inst�ncia de <see cref="WindsorContainer"/>
		/// </param>
		/// <param name="sessionManager">
		/// The session Manager.
		/// </param>
		public JobRunnerService(IWindsorContainer container, ISessionManager sessionManager)
		{
			_container = container;
			_sessionManager = sessionManager;
		}

		/// <summary>
		/// Logger default
		/// </summary>
		public ILogger Logger
		{
			get { return _logger; }
			set { _logger = value; }
		}

		/// <summary>
		/// Executa um <see cref="IJob"/>
		/// </summary>
		/// <param name="jobKey">Chave do job no IoC</param>
		/// <param name="args">Argumentos para execu��o do job</param>
		/// <returns>Se o job foi executado com sucesso</returns>
		public bool Execute(string jobKey, params string[] args)
		{
			try
			{
                Sis.LogTrace("Iniciando class JobRunnerService. jobKey = " + jobKey);

				if (!_container.Kernel.HasComponent(jobKey))
				{
					throw new Exception(string.Format("N�o foi encontrado um servi�o com a chave: {0}", jobKey));
				}

				IJob job = _container.Resolve<IJob>(jobKey);

				try
				{
					using (_sessionManager.OpenSession())
					{
						return job.Execute(args);
					}
				}
				catch (Exception ex)
				{
					throw new ApplicationException(string.Format("Falha ao executar job '{0}'", jobKey), ex);
				}
                finally
				{
                    System.GC.SuppressFinalize(this);
				}
			}
			catch (Exception ex)
			{
                Sis.LogException(ex, string.Format("Falha ao executar job '{0}'", jobKey));
			}

			return false;
		}
	}

	/// <summary>
	/// Job de teste
	/// </summary>
	public class TesteJob : IJob
	{
		/// <summary>
		/// Executa um <see cref="IJob"/>
		/// </summary>
		/// <param name="args">Argumentos para execu��o do job</param>
		/// <returns>Se o job foi executado com sucesso</returns>
		public bool Execute(params object[] args)
		{
			Console.WriteLine("Executando job de teste. " + DateTime.Now);

			Thread.Sleep(TimeSpan.FromMinutes(3));

			return true;
		}
	}
}