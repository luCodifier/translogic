namespace Translogic.Modules.Core.Infrastructure.Services
{
	using System;
	using System.Web;
	using System.Web.Configuration;
	using System.Web.Security;

	/// <summary>
	/// Servi�o de autentica��o web
	/// </summary>
	public class AutenticacaoWebService
	{
		#region CONSTANTES

		private const bool CreatePersistentCookie = false;

		#endregion

		#region M�TODOS

		/// <summary>
		/// Cria o cookie de autentica��o e o token de acesso configurando o idioma e o tempo de validade
		/// </summary>
		/// <param name="usuario">
		/// Usuario a ser gravado na autentica��o.
		/// </param>
		/// <param name="idioma">
		/// Idioma a ser gravado no UserData do cookie.
		/// </param>
		/// <returns>
		/// Cookie autenticacao.
		/// </returns>
		public HttpCookie CriarCookieAutenticacao(string usuario, string idioma)
		{
			DateTime now = DateTime.Now;
			DateTime expiration = now.AddMinutes(ObterTimeout());

			var authTicket = new FormsAuthenticationTicket(1, usuario, now, expiration, CreatePersistentCookie, idioma);

			string encryptedTicket = FormsAuthentication.Encrypt(authTicket);

			var authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);

			HttpContext.Current.Response.Cookies.Add(authCookie);

			return authCookie;
		}

		/// <summary>
		/// Obteem o tempo que o usu�rio poder� ficar inativo no sistema at� a sess�o expirar
		/// </summary>
		/// <returns>Tempo de Timeout em minutos</returns>
		public double ObterTimeout()
		{
			var cfg = WebConfigurationManager.GetWebApplicationSection("system.web/authentication") as AuthenticationSection;

			if (cfg == null)
			{
				return 40;
			}

			return cfg.Forms.Timeout.TotalMinutes;
		}

		#endregion
	}
}