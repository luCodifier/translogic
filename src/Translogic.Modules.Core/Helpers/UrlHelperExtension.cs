namespace Translogic.Modules.Core.Helpers
{
	using System.Web.Mvc;
	using Microsoft.Practices.ServiceLocation;
	using Translogic.Core.Infrastructure.Settings;

	/// <summary>
	/// Extens�o de dados da Url
	/// </summary>
	public static class UrlHelperExtension
	{
		#region M�TODOS EST�TICOS

		/// <summary>
		/// Dados do servidor
		/// </summary>
		private static readonly ServerSettings ServerSettings = ServiceLocator.Current.GetInstance<ServerSettings>();

		/// <summary>
		/// Retorna a url do legado
		/// </summary>
		/// <param name="helper"> Objeto que est� extendendo </param>
		/// <returns>
		/// Url do legado
		/// </returns>
		public static string Legado(this UrlHelper helper)
		{
			return helper.Content(ServerSettings.UrlTranslogicLegado);
		}

		/// <summary>
		/// Retorna a url do legado dada uma url
		/// </summary>
		/// <param name="helper">Objeto que est� extendendo</param>
		/// <param name="url"> url do legado. </param>
		/// <returns>
		/// Url do legado
		/// </returns>
		public static string Legado(this UrlHelper helper, string url)
		{
			return Legado(helper) + url;
		}

		/// <summary>
		/// retorna uma url do legado
		/// </summary>
		/// <param name="helper"> Objeto que est� extendendo. </param>
		/// <param name="url"> url do legado. </param>
		/// <param name="token"> token de acesso. </param>
		/// <returns>
		/// Url do legado
		/// </returns>
		public static string Legado(this UrlHelper helper, string url, string token)
		{
			string urlLegado = Legado(helper);

			if (!urlLegado[urlLegado.Length - 1].Equals("/"))
            {
				urlLegado = string.Concat(urlLegado, "/");
			}

			return string.Concat(urlLegado, "TranslogicCompat.asp?token=", token, "&redir=", System.Web.HttpUtility.UrlEncode(url));
		}

        /// <summary>
        /// retorna uma url do legado
        /// </summary>
        /// <param name="helper"> Objeto que est� extendendo. </param>
        /// <param name="url"> url do legado. </param>
        /// <param name="token"> token de acesso. </param>
        /// <param name="theme"> Tema selecionado</param>
        /// <returns>
        /// Url do legado
        /// </returns>
        public static string Legado(this UrlHelper helper, string url, string token, string theme)
        {
            string urlLegado = Legado(helper);

            if (!urlLegado[urlLegado.Length - 1].Equals("/"))
            {
                urlLegado = string.Concat(urlLegado, "/");
            }

            return string.Concat(urlLegado, "TranslogicCompat.asp?token=", token, "&redir=", System.Web.HttpUtility.UrlEncode(url), "&theme=", System.Web.HttpUtility.UrlEncode(theme));
        }

        /// <summary>
        /// Url de para acesso ao SIV
        /// </summary>
        /// <param name="helper">Objeto que est� extendendo</param>
        /// <param name="idTela">
        /// Id da tela no sistema SIV..
        /// </param>
        /// <param name="token"> token de acesso. </param>
        /// <returns>
        /// Url no SIV
        /// </returns>
        public static string Siv(this UrlHelper helper, int idTela, string token)
        {
            Legado(helper);
            return string.Concat(ServerSettings.UrlSiv, ServerSettings.ServletSiv, "&idTela=", idTela, "&token=", token);
        }

        /// <summary>
        /// Url de para acesso ao sistema Gis dentro do SIV
        /// </summary>
        /// <param name="helper">Objeto que est� extendendo</param>
        /// <param name="token"> token de acesso. </param>
        /// <returns>
        /// Url no SIV
        /// </returns>
        public static string SivGis(this UrlHelper helper, string token)
        {
            Legado(helper);
            return string.Concat(ServerSettings.UrlSiv, ServerSettings.ServletSiv, "&idTela=", ServerSettings.Gis, "&token=", token);
        }

		/// <summary>
		/// Url de logout da nova arquitetura
		/// </summary>
		/// <param name="helper">
		/// Objeto que est� extendendo..
		/// </param>
		/// <returns>
		/// Url de logout
		/// </returns>
		public static string Logout(this UrlHelper helper)
		{
			return helper.Content("~/acesso/sair.all");
		}

		#endregion
	}
}