﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Translogic.Modules.Core.Domain.Model.Tfa.Repositories;
using Translogic.Modules.Core.Domain.DataAccess.Repositories.Tfa;
using Translogic.Modules.Core.Domain.Model.Tfa;
using System.Configuration;
using System.IO;

namespace Translogic.Modules.Core.Helpers.ControlePerdas
{
    public static class LogTfaHelper
    {
        private static ILogTfaRepository _logTfaRepository = new LogTfaRepository();

        private static object _lockObj = new object();
        public static Dictionary<string, List<string>> _responseMessage = new Dictionary<string, List<string>>();

        public static Dictionary<string, List<string>> responseMessage{
            get
            {
                lock (_lockObj)
                {
                    return _responseMessage;
                }
            }
            set {
                lock (_lockObj) {
                    _responseMessage = value;
                } 
            }
        }

        

        static LogTfaHelper()
        {
            bool value = false;

            bool.TryParse(ConfigurationManager.AppSettings["ControlePerdasTrace"], out value);

            TraceOn = value;
        }

        public static bool TraceOn { get; set; }

        public static void Trace(string trace, int? numeroProcesso = null, int? despacho = null, string serieDespacho = null, string vagao = null)
        {
            //if (TraceOn)
            //{
            //var log = new LogTfa { Descricao = string.Format("Processo: {0}\nDescpacho: {1}\nVagao: {2}\n{3}", numeroProcesso, despacho, vagao, trace), VersionDate = DateTime.Now };
            //    log.Descricao = log.Descricao.Length > 2000 ? log.Descricao.Substring(0, 2000) : log.Descricao;
            //    _logTfaRepository.Inserir(log);     
            //}

            if (TraceOn)
            {
                var logPath = @"C:\temp\log.txt";

                if (!File.Exists(logPath))
                {
                    File.Create(logPath).Close();
                }

                File.AppendAllText(logPath, string.Format("{0} - {1} - Processo: {2} Descpacho: {3}/{4} Vagao: {5} \n ", DateTime.Now.ToString(), trace, numeroProcesso, despacho, serieDespacho, vagao));
            }
        }

        public static void TraceEmail(string trace)
        {
            //if (TraceOn)
            //{
            //var log = new LogTfa { Descricao = string.Format("Processo: {0}\nDescpacho: {1}\nVagao: {2}\n{3}", numeroProcesso, despacho, vagao, trace), VersionDate = DateTime.Now };
            //    log.Descricao = log.Descricao.Length > 2000 ? log.Descricao.Substring(0, 2000) : log.Descricao;
            //    _logTfaRepository.Inserir(log);     
            //}

                var logPath = @"C:\temp\TFA\logEmail.txt";

                if (!File.Exists(logPath))
                {
                    File.Create(logPath).Close();
                }

                File.AppendAllText(logPath, string.Format("{0}" , trace));
        }
    }
}