﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Translogic.Modules.Core.Util;
using System.IO;
using Translogic.Modules.Core.Domain.Model.Tfa;
using System.Net.Mail;
using Translogic.Modules.Core.Domain.Model.Dto;

namespace Translogic.Modules.Core.Helpers.ControlePerdas
{
    public class EmailTfaHelper
    {
        private const string ASSUNTO_EMAIL_NOVO_TFA = "Termo de Falta e/ou avaria (TFA)";
        private const string ASSUNTO_EMAIL_ALTERACAO_TFA = "Termo de Falta e/ou avaria (TFA)";
        private const string ASSUNTO_EMAIL_ACEITE_TFA = "Notificação de Aceite de Processo de Seguro/TFA no CAALL";
        private const string ASSUNTO_EMAIL_RECOMENDACAO_MANUTENCAO_VAGAO = "Notificação de Recomendação de manutenção de Vagão no sistema Controle de Perdas";

        public static bool EnviarEmailTfa(StatusTfaEnum status, TfaEmailDto model, string destinatarios, string destinatariosCC, Dictionary<string, Stream> anexos)
        {
            var listaAnexos = anexos.Select(x => EmailUtil.CreateAttachment(x.Value, x.Key)).ToList<Attachment>();

            switch (status)
            {
                case StatusTfaEnum.Criando:
                    return EnviarEmailCriacaoTfa(model, destinatarios, destinatariosCC, listaAnexos);
                case StatusTfaEnum.Alterando:
                    return EnviarEmailTfaAlterado(model, destinatarios, destinatariosCC, listaAnexos);
                case StatusTfaEnum.ClienteCiente:
                    return EnviarEmailClienteCienteTfa(model, destinatarios, destinatariosCC, listaAnexos);
                case StatusTfaEnum.ClienteNaoCiente:
                    return EnviarEmailClienteNaoCiente(model, destinatarios, destinatariosCC, listaAnexos);
                default:
                    return false;
            }
        }


        private static bool EnviarEmailCriacaoTfa(TfaEmailDto model, string destinatarios, string destinatariosCC, List<Attachment> anexos)
        {
            EmailUtil.Send(destinatarios, CriarTemplateAssunto(ASSUNTO_EMAIL_NOVO_TFA, model), CriarTemplateEmailNovoTfa(model), destinatariosCC, anexos);

            return true;
        }

        private static bool EnviarEmailClienteNaoCiente(TfaEmailDto model, string destinatarios, string destinatariosCC, List<Attachment> anexos)
        {
            EmailUtil.Send(destinatarios, CriarTemplateAssunto(ASSUNTO_EMAIL_ACEITE_TFA, model), CriarTemplateClienteNaoCiente(model), destinatariosCC, anexos);

            return true;
        }

        private static bool EnviarEmailTfaAlterado(TfaEmailDto model, string destinatarios, string destinatariosCC, List<Attachment> anexos)
        {
            EmailUtil.Send(destinatarios, CriarTemplateAssunto(ASSUNTO_EMAIL_ALTERACAO_TFA, model), CriarTemplateEmailTfaAlterado(model), destinatariosCC, anexos);

            return true;
        }

        private static bool EnviarEmailClienteCienteTfa(TfaEmailDto model, string destinatarios, string destinatariosCC, List<Attachment> anexos)
        {
            EmailUtil.Send(destinatarios, CriarTemplateAssunto(ASSUNTO_EMAIL_ACEITE_TFA, model), CriarTemplateEmailClienteCienteTfa(model), destinatariosCC, anexos);

            return true;
        }

        public static bool EnviarEmailRecomendacaoVagao(TfaEmailDto model, string destinatarios, string destinatariosCC)
        {
            EmailUtil.Send(destinatarios, CriarTemplateAssunto(ASSUNTO_EMAIL_RECOMENDACAO_MANUTENCAO_VAGAO, model), CriarTemplateEmailRecomendarVagao(model), destinatariosCC);

            return true;
        }

        private static string CriarTemplateAssunto(string tituloAssunto, TfaEmailDto model)
        {
            string assunto = tituloAssunto;

            if (!string.IsNullOrEmpty(model.NumeroVagao))
            {
                assunto += string.Format(" - Vagão: {0} Série: {1}", model.NumeroVagao, model.SerieVagao);
            }

            if (model.NumeroProcesso > 0)
            {
                assunto += string.Format(" Processo: {0}", model.NumeroProcesso);
            }

            if (!string.IsNullOrEmpty(model.NumeroNotaFiscal))
            {
                assunto += string.Format(" Notas: {0}", model.NumeroNotaFiscal);
            }

            return assunto;
        }

        private static string CriarTemplateEmailNovoTfa(TfaEmailDto model)
        {
            string body = SubstituirDadosModelBase(@"
                <tr>
                  <td align='center' style='padding-top: 39px; padding-left: 45px; padding-right: 45px;'>
                    <span>Informamos que foi emitido o Termo de Falta e/ou Avaria (TFA) sob o número 
                    <span style='color: #003764;'>#NUMERO_PROCESSO#</span> , referente ao carregamento e transporte do vagão 
                    <span style='color: #003764;'>#NUMERO_VAGAO#</span> faturado com a(s) nota(s) 
                    <span style='color: #003764;'>#NUMERO_NOTA#</span>.</span>                                       
                  </td>
                </tr>                
            ", model);

            return CriarTemplate(ASSUNTO_EMAIL_NOVO_TFA, body);          
        }

        private static string CriarTemplateClienteNaoCiente(TfaEmailDto model)
        {
            string body = SubstituirDadosModelBase(@"
                <tr>
                  <td align='center' style='padding-top: 39px; padding-left: 45px; padding-right: 45px;'>
                    <span>Informamos que está disponível para validação em sistema o Termo de Falta e/ou Avaria (TFA) sob o número 
                    <span style='color: #003764;'>#NUMERO_PROCESSO#</span> , referente ao carregamento e transporte do vagão 
                    <span style='color: #003764;'>#NUMERO_VAGAO#</span> faturado com a(s) nota(s) 
                    <span style='color: #003764;'>#NUMERO_NOTA#</span>.</span>                                       
                  </td>
                </tr>                
            ", model);

            return CriarTemplate(ASSUNTO_EMAIL_ACEITE_TFA, body);          
        }

        private static string CriarTemplateEmailTfaAlterado(TfaEmailDto model)
        {
            string body = SubstituirDadosModelBase(@"
                <tr>
                  <td align='center' style='padding-top: 39px; padding-left: 45px; padding-right: 45px;'>
                    <span>Informamos que houve edição no Termo de Falta e/ou Avaria (TFA) emitido sob o número 
                    <span style='color: #003764;'>#NUMERO_PROCESSO#</span> , referente ao carregamento e transporte do vagão 
                    <span style='color: #003764;'>#NUMERO_VAGAO#</span> faturado com a(s) nota(s) 
                    <span style='color: #003764;'>#NUMERO_NOTA#</span>.</span> <br>
                    <span>Justificativa:</span><br>                                     
                    <span>#JUSTIFICATIVA#</span>
                  </td>
                </tr>                
            ", model);

            return CriarTemplate(ASSUNTO_EMAIL_ALTERACAO_TFA, body);
        }

        private static string CriarTemplateEmailClienteCienteTfa(TfaEmailDto model)
        {
            string body = SubstituirDadosModelBase(@"
                <tr>
                  <td align='center' style='padding-top: 39px; padding-left: 45px; padding-right: 45px;'>
                    <span>Informamos a efetivação da validação do Termo de Falta e/ou Avaria (TFA) sob o número 
                    <span style='color: #003764;'>#NUMERO_PROCESSO#</span> , referente ao carregamento e transporte do vagão 
                    <span style='color: #003764;'>#NUMERO_VAGAO#</span> faturado com a(s) nota(s) 
                    <span style='color: #003764;'>#NUMERO_NOTA#</span>.</span>                                       
                  </td>
                </tr>                
            ", model);

            return CriarTemplate(ASSUNTO_EMAIL_ACEITE_TFA, body);
        }

        private static string CriarTemplateEmailRecomendarVagao(TfaEmailDto model)
        {
            string body = SubstituirDadosModelBase(@"
                <tr>
                  <td align='center' style='padding-top: 39px; padding-left: 45px; padding-right: 45px;'>
                    <span>Informamos que houve emissão de Termo de Falta e/ou Avaria (TFA) sob o número 
                    <span style='color: #003764;'>#NUMERO_PROCESSO#</span> , referente ao carregamento e transporte do vagão 
                    <span style='color: #003764;'>#NUMERO_VAGAO#</span>. Solicitamos que o vagão seja recomendado de imediato.</span>                                       
                  </td>
                </tr>                
            ", model);

            return CriarTemplate(ASSUNTO_EMAIL_RECOMENDACAO_MANUTENCAO_VAGAO, body);
        }

        private static string SubstituirDadosModelBase(string content, TfaEmailDto model)
        {
            if (!string.IsNullOrEmpty(content))
            {
             content = content.Replace("#NUMERO_PROCESSO#", string.Format("{0}", model.NumeroProcesso))
             .Replace("#NUMERO_VAGAO#", string.Format("{0}", model.NumeroVagao))
             .Replace("#NUMERO_NOTA#", string.Format("{0}", model.NumeroNotaFiscal))
             .Replace("#JUSTIFICATIVA#", string.Format("{0}", model.Justificativa));
            }

            return content;
        }

        private static string CriarTemplate(string assunto, string content)
        {
            return @"
                <!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
                <html xmlns='http://www.w3.org/1999/xhtml' xml:lang='pt-br' lang='pt-br'>
                <head>
                  <meta charset='utf-8' />
                  <meta content='width=device-width, initial-scale=1' name='viewport' />
                  <meta name='x-apple-disable-message-reformatting' />
                  <meta http-equiv='Content-Type' content='text/html;charset=utf-8' />
                  <meta http-equiv='X-UA-Compatible' content='IE=edge' />
                  <meta content='telephone=no' name='format-detection' />
                  <title></title>
                  <style type='text/css'>
                    a {color: #ffffff;}
                    span.MsoHyperlink {mso-style-priority:99; color:inherit;}
                    span.MsoHyperlinkFollowed {mso-style-priority:99; color:inherit;}
                    .aStyle {color:#003764 !important; text-decoration: none !important;}
                    .aStyle:visited {color:#003764 !important;}
                    .aStyle:hover {color:#003764 !important;}
                    .aStyle:active {color:#003764 !important;}
                    .btnHover:hover {background-color:#2d5579 !important; border-color:#2d5579 !important;}
                  </style>

                  <!--[if (mso 16)]>
                    <style type='text/css'>
                      a {text-decoration: none;}
                    </style>
                  <![endif]-->
                  <!--[if gte mso 9]>
                    <style>sup { font-size: 100% !important; }</style>
                  <![endif]-->
                </head>
                <body style='background-color: #f7f7f7; color: #545454; font-size: 14px; font-weight: 400; font-family: ""Open Sans"", Arial, sans-serif;'>
                  <!--[if gte mso 9]>
                    <v:background xmlns:v='urn:schemas-microsoft-com:vml' fill='t'>
                      <v:fill type='tile' src='' color='#f6f6f6'></v:fill>
                    </v:background>
                  <![endif]-->
                  <table align='center' border='0' cellpadding='0' cellspacing='0' width='607'>
                    <tbody>
                      <tr>
                        <td>
                          <table border='0' cellpadding='0' cellspacing='0' width='100%' style='width: 100%; background-color: #ffffff; border: solid 1px #d9d9d9; color: #545454; font-size: 14px; font-weight: 400; font-family: ""Open Sans"", Arial, sans-serif;'>
                            <tbody>
                              <tr>
                                <td align='center' style='background-color: #003764; font-size: 24px; color: #ffffff; padding: 22px;'>
                                  <span>#TITULO#</span>
                                </td>
                              </tr>
                              #MAIN_CONTENT#
                              <tr>
                                <td align='center' style='padding-bottom: 39px;'>
                                  <span>Qualquer d&uacute;vida entre em contato com </span>
                                  <a href='mailto:indenizacoes@rumolog.com' target='_top' class='aStyle' style='color: #003764; text-decoration: none;'>
                                    <span>indenizacoes@rumolog.com</span>
                                  </a>
                                  <span>.</span>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <table border='0' cellpadding='0' cellspacing='0' width='100%' style='width: 100%; color: #545454; font-size: 14px; font-weight: 400; font-family: ""Open Sans"", Arial, sans-serif;'>
                            <tbody>
                              <tr>
                                <td align='center' style='padding-top: 15px;'>
                                  <a href='http://pt.rumolog.com' target='_blank' class='aStyle' style='color: #003764; text-decoration: none; text-align: center;'>
                                    <img src='http://pt.rumolog.com/images_inst/rumo_azul_201604251.png' alt='Rumo Log&iacute;stica' width='88' height='20' />
                                  </a>
                                </td>
                              </tr>
                              <tr>
                                <td align='center' style='padding-top: 15px; padding-bottom: 15px; color: #a1a1a1; font-weight: normal; font-size: 12px; text-align: center;'>
                                  <span>&copy; #DATE# Rumo Log&iacute;stica</span>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </body>
                </html>
            ".Replace("#DATE#", DateTime.Now.Year.ToString())
            .Replace("#TITULO#", assunto)
            .Replace("#MAIN_CONTENT#", content)
            .Replace("\r\n", string.Empty)
            .Replace("  ", string.Empty);
        }
    }
}