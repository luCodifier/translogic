namespace Translogic.Modules.Core.Helpers
{
	using System.Web.Mvc;

	/// <summary>
	/// Tipo da mensagem
	/// </summary>
	public enum MessageType
	{
		/// <summary> Mensagem de informa��o </summary>
		Info,
		
		/// <summary>Mensagem de sucesso </summary>
		Success,
		
		/// <summary>Mensagem de Aviso </summary>
		Warning,
		
		/// <summary>Mensagem de erro </summary>
		Error,
		
		/// <summary>Mensagem de valida��o </summary>
		Validation
	}

	/// <summary>
	/// Exten��o helper de html
	/// </summary>
	public static class HTMLHelperExtension
	{
		#region M�TODOS EST�TICOS

		/// <summary>
		/// Rertornao o titulo da pagina
		/// </summary>
		/// <param name="helper">O helper extendido</param>
		/// <param name="title">Titulo da pagina</param>
		/// <returns>String do tittulo</returns>
		public static string GetPageTitle(this HtmlHelper helper, string title)
		{
			return string.Format("{0} &#8212; Translogic", title);
		}

		/// <summary>
		/// Mensagem de erro.
		/// </summary>
		/// <param name="helper">O helper extendido.</param>
		/// <param name="message">Mensagem de erro</param>
		/// <returns>String da mensagem de erro</returns>
		public static string ErrorMessage(this HtmlHelper helper, string message)
		{
			return Message(helper, message, MessageType.Error);
		}

		/// <summary>
		/// Mensagem de informa��o
		/// </summary>
		/// <param name="helper">O helper extendido</param>
		/// <param name="message">Mensagem de info.</param>
		/// <returns>String da mensagem de info,.</returns>
		public static string InfoMessage(this HtmlHelper helper, string message)
		{
			return Message(helper, message, MessageType.Info);
		}

		/// <summary>
		/// Formata em mensagem dado um tipo
		/// </summary>
		/// <param name="helper">O helper extendido</param>
		/// <param name="message">Mensagem a ser escrita</param>
		/// <param name="type">Tipo da mensagem</param>
		/// <returns>String do html da mensagem</returns>
		public static string Message(this HtmlHelper helper, string message, MessageType type)
		{
			return string.Format("<div class=\"message {0}\">{1}</div>", type.ToString().ToLower(), message);
		}

		/// <summary>
		/// Mensagem de sucesso
		/// </summary>
		/// <param name="helper">O helper extendido</param>
		/// <param name="message">Mensagem de sucesso.</param>
		/// <returns>Html da mensagem de sucesso</returns>
		public static string SuccessMessage(this HtmlHelper helper, string message)
		{
			return Message(helper, message, MessageType.Success);
		}

		/// <summary>
		/// Mensagem de valida��o
		/// </summary>
		/// <param name="helper">O helper extendido.</param>
		/// <param name="message">Mensagem de valida��o.</param>
		/// <returns>Html da mensagem de valida��o</returns>
		public static string ValidationMessage(this HtmlHelper helper, string message)
		{
			return Message(helper, message, MessageType.Validation);
		}

		/// <summary>
		/// Mensagem de Aviso
		/// </summary>
		/// <param name="helper">O helper extendido</param>
		/// <param name="message">Mensagem de aviso.</param>
		/// <returns>Html da mensagem de aviso</returns>
		public static string WarningMessage(this HtmlHelper helper, string message)
		{
			return Message(helper, message, MessageType.Warning);
		}

		#endregion
	}
}