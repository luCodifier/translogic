using System.Web.Routing;

namespace Translogic.Modules.Core.Controllers
{
	using System;
	using System.Drawing;
	using System.IO;
	using System.Linq;
	using System.Web;
	using System.Web.Mvc;
	using System.Web.Security;
	using System.Web.UI.DataVisualization.Charting;

	using Microsoft.Practices.ServiceLocation;
	using Translogic.Core.Infrastructure;
	using Translogic.Core.Infrastructure.Settings;
	using Translogic.Core.Infrastructure.Web;
	using Translogic.Core.Infrastructure.Web.Helpers;
	using Translogic.Modules.Core.App_GlobalResources;
	using Translogic.Modules.Core.Controllers.ViewModel;
	using Translogic.Modules.Core.Domain.Model.Acesso;
	using Translogic.Modules.Core.Domain.Services.Acesso;
	using Translogic.Modules.Core.Filters;
	using Translogic.Modules.Core.Infrastructure.Services;

	using Chart = System.Web.Helpers.Chart;

	/// <summary>
	/// Controller respons�vel pelo controle de acesso
	/// </summary>
	public class AcessoController : BaseModuleController
	{
		#region CONSTANTES

		/// <summary>
		/// Constante indicando se o cookie criado ser� persistente (sem data de expira��o)
		/// </summary>
		public const bool CreatePersistentCookie = false;

		private const string ModelPrefix = "info";

		#endregion

		#region ATRIBUTOS READONLY & EST�TICOS
		private readonly ServerSettings settings = ServiceLocator.Current.GetInstance<ServerSettings>();
		private readonly AutenticacaoWebService _autenticacaoWebService;
		private readonly ControleAcessoService _controleAcessoService;

		#endregion

		#region CONSTRUTORES

		/// <summary>
		/// Construtor padr�o
		/// </summary>
		/// <param name="controleAcessoService"> Servi�o de controle de acesso injetado. </param>
		/// <param name="autenticacaoWebService"> WebService de autentica��o injetado. </param>
		public AcessoController(ControleAcessoService controleAcessoService, AutenticacaoWebService autenticacaoWebService)
		{
			_controleAcessoService = controleAcessoService;
			_autenticacaoWebService = autenticacaoWebService;
		}

		#endregion

		#region M�TODOS

		/// <summary>
		/// Valida e autentica o usu�rio no sistema
		/// </summary>
		/// <param name="info">Dados de login</param>
		/// <returns>Retorno padr�o da action</returns>
		[AcceptVerbs(HttpVerbs.Post)]
		public virtual ActionResult Autenticar(LoginInfo info)
		{
			if (!Validate(info, ModelPrefix))
			{
				return View("login", info);
			}
		    AutenticacaoResultado resultadoAutenticacao = _controleAcessoService.Autenticar(info.Usuario, info.Senha);
		    
			if (resultadoAutenticacao != AutenticacaoResultado.Sucesso)
			{
				switch (resultadoAutenticacao)
				{
					case AutenticacaoResultado.SenhaIncorreta:
					case AutenticacaoResultado.UsuarioNaoExiste:
						ModelState.AddModelError(ModelPrefix + ".Usuario", Acesso.AutenticacaoFalha);
						break;
					case AutenticacaoResultado.UsuarioInativo:
						ModelState.AddModelError(ModelPrefix + ".Usuario", Acesso.UsuarioInativo);
						break;
					case AutenticacaoResultado.UsuarioBloqueadoPorTentativas:
						ModelState.AddModelError(ModelPrefix + ".Usuario", Acesso.UsuarioBloqueadoPorTentativas);
						break;
                   /* case AutenticacaoResultado.UsuarioInativo7Dias:
                        ModelState.AddModelError(ModelPrefix+".Usuario",Acesso.UsuarioInativo7Dias);
                        Redirect(Url.Action("alterarSenhaExpirada", "home", new RouteValueDictionary { { "returnUrl", Request.Path } }));
				        break;   */
				}
			}

			if (resultadoAutenticacao == AutenticacaoResultado.Sucesso)
			{
				VerificarCookieExistenteERemover();
				CriarCookieAutenticacaoETokenAcesso(info);

				string returnUrl = Request["returnUrl"];

				Session["usuario"] = info.Usuario;

				if (!string.IsNullOrEmpty(returnUrl))
				{
					string tema = _controleAcessoService.ObterTemaUsuario(info.Usuario);
					if (!string.IsNullOrEmpty(tema))
					{
						Session["TEMA_TELA"] = tema;
					}

					Response.Redirect(returnUrl, true);
				}
				else
				{
					string tema = _controleAcessoService.ObterTemaUsuario(info.Usuario);
					if (!string.IsNullOrEmpty(tema))
					{
						Session["TEMA_TELA"] = tema;
					}

					Response.Redirect(FormsAuthentication.DefaultUrl);
				}

				return new EmptyResult();
			}

			return View("login", info);
		}

		/// <summary>
		/// Action para exibi��o do formul�rio de login
		/// </summary>
		/// <returns>HTML do formul�rio de login</returns>
		[GarantirSufixoServidorFilter(Order = 1)]
		public virtual ActionResult Login()
		{
			ViewData["PossuiAcessoCookies"] = Request.Browser.Cookies ? "S" : "N";
			// throw new Exception("teste"); // for�ar para dar erro com o CustomErrors habilitado
			return View();
		}

		/// <summary>
		/// For�a o logout de sessao expirada
		/// </summary>
		/// <returns>Resultado Json</returns>
		public JsonResult ForcarLogout()
		{
			FormsAuthentication.SignOut();
			Session.Abandon();
			return new JsonResult();
		}

		/// <summary>
		/// Action respons�vel por efetuar o logout do usu�rio no sistema
		/// </summary>
		/// <returns>HTML do formul�rio de login exibindo a mensagem de logout com sucesso</returns>
		public virtual ActionResult Sair()
		{
			FormsAuthentication.SignOut();
			Session.Abandon();
			return View("login", new LoginInfo { LogoutEfetuado = true });
		}

		/// <summary>
		/// Envia dados de acesso para o email relacionado ao usu�rio.
		/// </summary>
		/// <param name="info"> Dados de login. </param>
		/// <returns> Tela de lembrete de senha </returns>
		[AcceptVerbs(HttpVerbs.Post)]
		public ActionResult LembrarSenha(LoginInfo info)
		{
			try
			{
				_controleAcessoService.LembrarSenha(info, ControllerContext);
				info.LembreteSenhaEnviado = true;
			}
			catch (TranslogicException ex)
			{
				ModelState.AddModelError(ModelPrefix + ".Usuario", ex.Message);
			}
			
			return View("Login", info);
		}

		/// <summary>
		/// Verifica a disponibilidade do sistema
		/// </summary>
		/// <returns>Retorna para ver o Mvc est� funcionando para o Netscaler</returns>
		public ActionResult VerificarDisponibilidade()
		{
			return Content("OK");
		}

		/// <summary>
		/// action de Teste
		/// </summary>
		/// <returns>View com ok</returns>
		public ActionResult Testar()
		{
			ViewData["teste"] = "teste";

			var viewString = View("Teste").Capture(ControllerContext);
			return this.PdfResult("Relat�rio de Bla bla bla", "test.pdf", viewString);
			// return Content("OK");
		}

		/// <summary>
		/// Action de teste de conteudo
		/// </summary>
		/// <returns>View com o teste</returns>
		public ActionResult Teste()
		{
			return View();
		}

		/// <summary>
		/// The grafico teste.
		/// </summary>
		/// <returns>
		/// The <see cref="ActionResult"/>. </returns>
		public ActionResult GraficoTeste()
		{
			var testChart = new System.Web.UI.DataVisualization.Charting.Chart
			{
				Width = 600,
				Height = 300
			};

			testChart.BorderlineColor = Color.Black;
			testChart.BorderlineWidth = 1;
			testChart.BorderlineDashStyle = ChartDashStyle.Solid;

			// adiciona areas
			ChartArea area = new ChartArea()
			{
				BackColor = Color.White,
				Name = "Area1"
			};
			area.Area3DStyle.Enable3D = false;
			area.Position.X = 0;
			area.Position.Y = 10;
			area.Position.Width = 35;
			area.Position.Height = 90;
			testChart.ChartAreas.Add(area);

			// adiciona areas
			ChartArea area2 = new ChartArea()
			{
				BackColor = Color.White,
				Name = "Area2",
			};

			area2.Area3DStyle.Enable3D = false;
			area2.Position.X = 49;
			area2.Position.Y = 10;
			area2.Position.Width = 35;
			area2.Position.Height = 90;

			testChart.ChartAreas.Add(area2);

			// adiciona t�tulo
			Title title = new Title()
			{
				Docking = Docking.Top,
				Font = new Font("Trebuchet MS", 18.0f, FontStyle.Bold),
				IsDockedInsideChartArea = false,
				DockedToChartArea = "Area1"
			};
			title.Text = "Exemplo";
			testChart.Titles.Add(title);

			// adiciona t�tulo
			Title title2 = new Title()
			{
				Docking = Docking.Top,
				Font = new Font("Trebuchet MS", 18.0f, FontStyle.Bold),
				IsDockedInsideChartArea = false,
				DockedToChartArea = "Area2"
			};
			title2.Text = "Exemplo2";
			testChart.Titles.Add(title2);

			// adiciona legenda
			Legend legend = new Legend()
			{
				Alignment = StringAlignment.Far,
				Docking = Docking.Right,
				Title = "teste1",
				IsDockedInsideChartArea = false,
				DockedToChartArea = "Area1",
				Name = "Legend1"
			};
			testChart.Legends.Add(legend);

			Legend legend2 = new Legend()
			{
				Alignment = StringAlignment.Far,
				Docking = Docking.Right,
				Title = "teste2",
				IsDockedInsideChartArea = false,
				DockedToChartArea = "Area2",
				Name = "Legend2"
			};
			testChart.Legends.Add(legend2);

			Series series = new Series
			{
				ChartType = SeriesChartType.Line,
				Palette = ChartColorPalette.None,
				Color = Color.Red,
				MarkerSize = 10,
				Name = "Linha 1",
				BorderWidth = 1,
				IsValueShownAsLabel = true,
				MarkerBorderWidth = 10,
				ChartArea = "Area1",
				Legend = "Legend1"
			};

			Random rnd = new Random(DateTime.Now.Millisecond);
			for (var i = 0; i < 11; i++)
			{
				DateTime date = DateTime.Now.AddDays(i);
				series.Points.AddXY(date, rnd.Next(50, 100));
			}

			testChart.Series.Add(series);

			Series series2 = new Series
			{
				ChartType = SeriesChartType.Line,
				Palette = ChartColorPalette.None,
				Color = Color.Brown,
				MarkerSize = 10,
				Name = "Linha 2",
				BorderWidth = 5,
				ChartArea = "Area2",
				Legend = "Legend2"
			};

			for (var i = 0; i < 11; i++)
			{
				DateTime date = DateTime.Now.AddDays(i);
				series2.Points.AddXY(date, rnd.Next(50, 100));
			}

			testChart.Series.Add(series2);

			// Save the chart to a MemoryStream
			var imgStream = new MemoryStream();
			testChart.SaveImage(imgStream, ChartImageFormat.Png);
			imgStream.Seek(0, SeekOrigin.Begin);

			// Return the contents of the Stream to the client
			return File(imgStream, "image/png");
		}

		/*
		/// <summary>
		/// M�todo que muda o DNS do legado
		/// </summary>
		/// <param name="dns">Novo dns a ser configurado</param>
		/// <returns>View Vazia</returns>
		public ActionResult MudarDnsLegado(string dns)
		{
			// quando estiver testando descomentar o codigo abaixo e comentar os outros ips
			// SetarDnsLegadoServidor("localhost/translogic", dns);

			SetarDnsLegadoServidor("190.1.1.67", dns);
			SetarDnsLegadoServidor("190.1.0.57", dns);
			SetarDnsLegadoServidor("190.1.1.91", dns);

			return new EmptyResult();
		}
		*/

		/// <summary>
		/// Seta os idiomas ao executar a action deste controller
		/// </summary>
		/// <param name="filterContext">Filtro de contexto</param>
		protected override void OnActionExecuting(ActionExecutingContext filterContext)
		{
			base.OnActionExecuting(filterContext);

			SetIdiomas();
		}

		/// <summary>
		/// Cria o cookie de autentica��o e o token de acesso configurando o idioma e o tempo de validade
		/// </summary>
		/// <param name="info">Informa��es do login</param>
		private void CriarCookieAutenticacaoETokenAcesso(LoginInfo info)
		{
			HttpCookie cookie = _autenticacaoWebService.CriarCookieAutenticacao(info.Usuario, info.Idioma);

			_controleAcessoService.RegistraAcessoPorToken(cookie.Value, info.Usuario, info.Idioma);
		}

		private void VerificarCookieExistenteERemover()
		{
			HttpCookie authCookie = Request.Cookies[FormsAuthentication.FormsCookieName];

			if (authCookie != null)
			{
				FormsAuthentication.SignOut();
				Session.Abandon();
			}
		}

		/// <summary>
		/// Adiciona a lista de idiomas na ViewData
		/// </summary>
		private void SetIdiomas()
		{
			ViewData["idiomas"] = Cultura.Todas.Select(c => new SelectListItem
			                                                	{
			                                                		Value = c.Name, 
																	Text = c.DisplayName
			                                                	});
		}
		
		/*
		private bool SetarDnsLegadoServidor(string ip, string novodns)
		{
			try
			{
				HttpWebRequest fr;
				string uri = string.Concat("http://", ip, "/MudaDnsTLNovo.asp?dns=", novodns);
				Uri targetUri = new Uri(uri);
				fr = (HttpWebRequest)WebRequest.Create(targetUri);

				if (fr.GetResponse().ContentLength > 0)
				{
					System.IO.StreamReader str = new System.IO.StreamReader(fr.GetResponse().GetResponseStream());
					if (str != null)
					{
						str.Close();
					}
				}
			}
			catch (WebException ex)
			{
				Response.Write("Arquivo nao existe no servidor: " + ip );
				return false;
			}

			return true;
		}
		*/

		#endregion
	}
}