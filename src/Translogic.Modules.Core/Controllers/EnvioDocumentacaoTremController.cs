﻿namespace Translogic.Modules.Core.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Web.Mvc;
    using Newtonsoft.Json;
    using Translogic.Core.Infrastructure.Web;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Modules.Core.Domain.Services.Documentacao;
    using Translogic.Modules.Core.Domain.Services.PainelExpedicao.Relatorio;
    using Translogic.Modules.Core.Domain.Services.PainelExpedicao;
    using Translogic.Modules.Core.Domain.Model.Codificador.Repositories;

    public class EnvioDocumentacaoTremController : BaseSecureModuleController
    {
        private readonly ConfiguracaoEnvioDocumentacaoService _configuracaoEnvioDocumentacaoService;
        private readonly DocumentacaoMotivoRefaturamentoService _documentacaoMotivoRefaturamentoService;
        private readonly DocumentacaoVagaoService _documentacaoVagaoService;
        private readonly GerenciamentoDocumentacaoRefaturamentoService _gerenciamentoDocumentacaoRefaturamentoService;
        private readonly PainelExpedicaoService _painelExpedicaoService;
        private readonly IConfiguracaoTranslogicRepository _confGeralRepository;

        public EnvioDocumentacaoTremController(ConfiguracaoEnvioDocumentacaoService configuracaoEnvioDocumentacaoService,
                                               DocumentacaoMotivoRefaturamentoService documentacaoMotivoRefaturamentoService,
                                               DocumentacaoVagaoService documentacaoVagaoService,
                                               GerenciamentoDocumentacaoRefaturamentoService gerenciamentoDocumentacaoRefaturamentoService,
                                               PainelExpedicaoService painelExpedicaoService,
                                               IConfiguracaoTranslogicRepository confGeralRepository)
        {
            _configuracaoEnvioDocumentacaoService = configuracaoEnvioDocumentacaoService;
            _documentacaoMotivoRefaturamentoService = documentacaoMotivoRefaturamentoService;
            _documentacaoVagaoService = documentacaoVagaoService;
            _gerenciamentoDocumentacaoRefaturamentoService = gerenciamentoDocumentacaoRefaturamentoService;
            _painelExpedicaoService = painelExpedicaoService;
            _confGeralRepository = confGeralRepository;
        }

        #region ActionResults

        //// [Autorizar(Transacao = "DOCUMENTACAOENVIO", Acao = "Refaturamento")]
        public ActionResult Refaturamento()
        {
            return View();
        }

        //// [Autorizar(Transacao = "DOCUMENTACAOENVIO", Acao = "Refaturamento")]
        public ActionResult MotivosRefaturamento()
        {
            var json = Request["documentacoesRefaturamento"];
            ViewData["Documentacoes"] = json;

            return View();
        }

        //// [Autorizar(Transacao = "DOCUMENTACAOENVIO", Acao = "Parametrizacao")]
        public ActionResult Parametrizacao()
        {
            return View();
        }

        //// [Autorizar(Transacao = "DOCUMENTACAOENVIO", Acao = "AdicionarConfiguracao")]
        public ActionResult AdicionarConfiguracao()
        {
            return View();
        }

        //// [Autorizar(Transacao = "DOCUMENTACAOENVIO", Acao = "EnviarDocumentacaoManual")]
        public ActionResult EnviarDocumentacaoManual()
        {
            return View();
        }

        //// [Autorizar(Transacao = "DOCUMENTACAOENVIO", Acao = "AdicionarConfiguracao")]
        public ActionResult GerenciarConfiguracao(int id)
        {
            var configuracao = _configuracaoEnvioDocumentacaoService.ObterPorId(id);
            ViewData["ConfiguracaoId"] = configuracao.Id;
            ViewData["ConfiguracaoAreaOperacional"] = configuracao.AreaOperacionalEnvio.Codigo;
            ViewData["ConfiguracaoTerminalId"] = configuracao.EmpresaDestino.Id;
            ViewData["ConfiguracaoTerminalDescricao"] = string.Format("{0} - {1}", configuracao.EmpresaDestino.DescricaoResumida, FormataCnpj(configuracao.EmpresaDestino.Cgc));
            ViewData["ConfiguracaoValidadeDocumentacao"] = configuracao.DiasValidadeDocumentacao ?? 7;
            ViewData["ConfiguracaoRelatorioLaudoMercadoria"] = configuracao.GerarRelatorioLaudoMercadoria == "S";
            ViewData["ConfiguracaoEnviarChegada"] = configuracao.EnviarChegada == "S";
            ViewData["ConfiguracaoEnviarSaida"] = string.IsNullOrEmpty(configuracao.EnviarSaida) || configuracao.EnviarSaida == "S";

            return View();
        }

        //// [Autorizar(Transacao = "DOCUMENTACAOENVIO", Acao = "Monitoramento")]
        public ActionResult Monitoramento()
        {
            return View();
        }

        #endregion ActionResults

        #region JsonResults

        public JsonResult ObterMonitoramentoGrid(
            DetalhesPaginacaoWeb pagination,
            string malha,
            string destino,
            string recebedor,
            string numeroOs,
            string prefixo,
            string dataInicio,
            string dataFim,
            string tremEncerr,
            int situacaoEnvioStatus,
            string docCompl)
        {
            try
            {
                ////DateTime.ParseExact(dataInicio, "dd/MM/yyyy", CultureInfo.InvariantCulture)
                var resultados = _configuracaoEnvioDocumentacaoService.ObterMonitoramentoGrid(pagination,
                                                                                              malha,
                                                                                              destino,
                                                                                              recebedor,
                                                                                              numeroOs,
                                                                                              prefixo,
                                                                                              dataInicio,
                                                                                              dataFim + " 23:59:59",
                                                                                              tremEncerr,
                                                                                              situacaoEnvioStatus,
                                                                                              docCompl);
                var linkUrlDownlodDocumentacao = _confGeralRepository.ObterPorId("GERENCIAMENTO_DOWNLOAD_DOCUMENTACAO_EMAIL").Valor;

                var retorno = new
                {
                    resultados.Total,
                    Items = resultados.Items.Select(item => new
                    {
                        DocumentacaoId = item.DocumentacaoId,
                        OsNumero = item.OsNumero,
                        TremPrefixo = item.TremPrefixo,
                        TremEncerrado = item.TremEncerrado,
                        Enviado = item.PassouAreaParametrizada == "S" ? item.Enviado : (item.Refaturamento == "S" ? "S" : "A"), // 'A' de aguardando envio
                        EstacaoEnvio = item.EstacaoEnvio,
                        Envio = item.Envio,
                        UsuarioNome = !string.IsNullOrEmpty(item.UsuarioNome) ? item.UsuarioNome : (item.Enviado == "S" ? "Automático" : string.Empty),
                        Recebedor = item.Recebedor,
                        DocumentacaoExcluida = item.DocumentacaoExcluida,
                        LinkDownload = item.DocumentacaoExcluida == "N" ? string.Format("{0}?fileName={0}", linkUrlDownlodDocumentacao, item.LinkDownload) : string.Empty,
                        CriadoEm = item.PassouAreaParametrizada == "S" & item.Enviado == "S" ? item.EnviadoEm.HasValue ? item.EnviadoEm.Value.ToString("dd/MM/yyyy HH:mm:ss") : string.Empty : string.Empty,////usava o campo de data errado antigamete
                        Refaturamento = item.Refaturamento,
                        Recomposicao = item.Recomposicao,

                        DataChegadaTrem = item.DataChegadaTrem.HasValue ? item.DataChegadaTrem.Value.ToString("dd/MM/yyyy HH:mm:ss") : string.Empty,
                        QuantidadeVagoes = item.QuantidadeVagoes.ToString(),
                        Origem = item.Origem,
                        Destino = item.Destino,
                        Atual = item.Atual,
                        DocumentacaoCompleta = item.DocumentacaoCompleta
                    })
                };

                return Json(retorno);
            }
            catch (Exception ex)
            {
                return this.Json(new { Success = false, Message = ex.Message });
            }
        }

        public ActionResult ExportarExcel(string malha,
            string destino,
            string recebedor,
            string numeroOs,
            string prefixo,
            string dataInicio,
            string dataFim,
            string tremEncerr,
            int situacaoEnvioStatus,
            string docCompl)
        {
            try
            {
                var pagination = new DetalhesPaginacaoWeb();
                pagination.Limite = 10000;
                pagination.Inicio = 0;
                var resultados = _configuracaoEnvioDocumentacaoService.ObterMonitoramentoGrid(pagination,
                                                                                              malha,
                                                                                              destino,
                                                                                              recebedor,
                                                                                              numeroOs,
                                                                                              prefixo,
                                                                                              dataInicio,
                                                                                              dataFim + " 23:59:59",
                                                                                              tremEncerr,
                                                                                              situacaoEnvioStatus,
                                                                                              docCompl);

                var relDocumentacaoTrem = new EnvioDocumentacaoTremExcelService(resultados.Items);
                var colunas = RetornaColunasCustomizadas();
                var excelStream = relDocumentacaoTrem.GerarRelatorioCustomizado("Monitoramento de Envios",
                                                                null,
                                                                colunas);
                var memoryStream = excelStream as MemoryStream;
                excelStream.Seek(0, SeekOrigin.Begin);
                return File(memoryStream.ToArray(),
                    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                    string.Format("RelatorioEnvioDocumentacaoTrem_{0}_{1}.xlsx", DateTime.Now.ToString("ddMMyyyy"), DateTime.Now.ToString("HHmmss")));

            }
            catch (Exception ex)
            {
                return this.Json(new { Success = false, Message = ex.Message });
            }
        }

        public JsonResult ObterRefaturamentosGrid(
            DetalhesPaginacaoWeb pagination,
            string numeroOs,
            string recebedor)
        {
            try
            {
                var resultados = _configuracaoEnvioDocumentacaoService.ObterRefaturamentosGrid(pagination,
                                                                                              numeroOs,
                                                                                              recebedor);

                var retorno = new
                {
                    resultados.Total,
                    Items = resultados.Items
                };

                return Json(retorno);
            }
            catch (Exception ex)
            {
                return this.Json(new { Success = false, Message = ex.Message });
            }
        }

        public JsonResult ObterConfiguracoes(
            DetalhesPaginacaoWeb pagination,
            string areaOperacionalEnvio,
            string terminalDestino,
            string ativo
            )
        {
            try
            {
                var resultados = _configuracaoEnvioDocumentacaoService.ObterConfiguracoesGrid(pagination,
                                                                                              areaOperacionalEnvio,
                                                                                              terminalDestino,
                                                                                              ativo);

                var retorno = new
                {
                    resultados.Items.Count,
                    Items = resultados.Items.Select(item => new
                    {
                        Id = item.Id,
                        AreaOperacionalEnvio = item.AreaOperacionalEnvio.Codigo,
                        Terminal = item.EmpresaDestino.DescricaoResumida,
                        CnpjTerminal = FormataCnpj(item.EmpresaDestino.Cgc),
                        Emails = String.Join(";", item.Emails.Where(e => e.Ativo == "S")
                                                                                                         .Select(e => e.Email)),
                        DataCriacao = item.CriadoEm.ToString("dd/MM/yyyy"),
                        Ativo = item.Ativo
                    })
                };

                return Json(retorno);
            }
            catch (Exception ex)
            {
                return this.Json(new { Success = false, Message = ex.Message });
            }
        }

        public JsonResult ObterEmailsConfiguracao(DetalhesPaginacaoWeb pagination, int idConfiguracao)
        {
            try
            {
                var resultados = _configuracaoEnvioDocumentacaoService.ObterEmailsConfiguracaoGrid(pagination,
                                                                                                   idConfiguracao);

                var retorno = new
                {
                    resultados.Items.Count,
                    Items = resultados.Items.Select(item => new
                    {
                        Id = item.Id,
                        IdConfiguracao = item.ConfiguracaoEnvio.Id,
                        Email = item.Email,
                        DataCriacao = item.CriadoEm.ToString("dd/MM/yyyy"),
                        Ativo = item.Ativo
                    })
                };

                return Json(retorno);
            }
            catch (Exception ex)
            {
                return this.Json(new { Success = false, Message = ex.Message });
            }
        }

        public JsonResult ObterTerminais(string query)
        {
            try
            {
                query = query ?? String.Empty;

                if (!string.IsNullOrEmpty(query))
                {
                    var itens = _configuracaoEnvioDocumentacaoService.ObterListaNomesTerminais(query);

                    return Json(new
                    {
                        Total = itens.Count,
                        Items = itens.Select(i => new
                        {
                            Id = i.Id,
                            DescResumida = string.Format("{0} - {1}", i.DescricaoResumida, FormataCnpj(i.Cgc))
                        })
                    });
                }

                return null;
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ErrorMessage = ex.Message
                });
            }
        }

        public JsonResult ObterTerminaisPorOs(string numeroOs)
        {
            try
            {
                var itens = _configuracaoEnvioDocumentacaoService.ObterTerminaisPorOs(numeroOs);

                return Json(new
                {
                    Items = itens.Select(i => new
                    {
                        Id = i.RecebedorId,
                        DescResumida = i.Recebedor
                    })
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ErrorMessage = ex.Message
                });
            }
        }

        public JsonResult ObterMotivosRefaturamento()
        {
            try
            {
                var motivos = _documentacaoMotivoRefaturamentoService.ObterAtivos();

                return Json(new
                {
                    Items = motivos.Select(i => new
                    {
                        Id = i.Id,
                        DescResumida = i.Motivo
                    })
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ErrorMessage = ex.Message
                });
            }
        }

        public JsonResult SalvarDocumentacaoRefaturamento()
        {
            try
            {
                var jsonDocumentacoes = Request["DocumentacoesRefaturamento"];
                var documentacoesRefaturamento = JsonConvert.DeserializeObject<List<DocumentacaoRefaturamentoDto>>(jsonDocumentacoes);
                _gerenciamentoDocumentacaoRefaturamentoService.SalvarDocumentacaoRefaturamento(documentacoesRefaturamento);

                return Json(new { Success = true, Message = "Documentações de Refaturamento salvas com sucesso." });
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = ex.Message });
            }
        }

        public JsonResult SalvarConfiguracao(int? idConfiguracao, string areaOperacionalEnvio, int? terminal, int? diasValidade, bool gerarRelatorioLaudoMercadoria, bool enviarChegada, bool enviarSaida)
        {
            try
            {
                if (idConfiguracao.HasValue)
                {
                    _configuracaoEnvioDocumentacaoService.Atualizar(idConfiguracao.Value, areaOperacionalEnvio, terminal, diasValidade, gerarRelatorioLaudoMercadoria, enviarChegada, enviarSaida);
                }
                else
                {
                    _configuracaoEnvioDocumentacaoService.Inserir(areaOperacionalEnvio, terminal, gerarRelatorioLaudoMercadoria, enviarChegada, enviarSaida);
                }

                return this.Json(new { Success = true, Message = "Configuração salva com sucesso." });
            }
            catch (Exception ex)
            {
                return this.Json(new { Success = false, Message = ex.Message });
            }
        }

        public JsonResult SalvarEmailConfiguracao(int? idConfiguracao, string email)
        {
            try
            {
                _configuracaoEnvioDocumentacaoService.InserirEmail(idConfiguracao, email);

                return this.Json(new { Success = true, Message = "Configuração salva com sucesso." });
            }
            catch (Exception ex)
            {
                return this.Json(new { Success = false, Message = ex.Message });
            }
        }

        public JsonResult DeletarConfiguracao(int id)
        {
            try
            {
                _configuracaoEnvioDocumentacaoService.Deletar(id);

                return this.Json(new { Success = true, Message = "Configuração deletada com sucesso." });
            }
            catch (Exception ex)
            {
                return this.Json(new { Success = false, Message = ex.Message });
            }
        }

        /// <summary>
        /// Deleta o e-mail da configuração
        /// </summary>
        /// <param name="id">ID do email de configuração</param>
        /// <returns></returns>
        public JsonResult DeletarEmailConfiguracao(int id)
        {
            try
            {
                _configuracaoEnvioDocumentacaoService.DeletarEmail(id);

                return this.Json(new { Success = true, Message = "E-mail da configuração deletada com sucesso." });
            }
            catch (Exception ex)
            {
                return this.Json(new { Success = false, Message = ex.Message });
            }
        }

        public JsonResult HabilitarConfiguracao(int id)
        {
            try
            {
                _configuracaoEnvioDocumentacaoService.HabilitarConfiguracao(id);

                return this.Json(new { Success = true, Message = "Configuração alterada com sucesso." });
            }
            catch (Exception ex)
            {
                return this.Json(new { Success = false, Message = ex.Message });
            }
        }

        /// <summary>
        /// Habilita ou desabilita o e-mail da configuração
        /// </summary>
        /// <param name="id">ID do e-mail</param>
        /// <returns>Json Result informando se deu tudo certo com a transação</returns>
        public JsonResult HabilitarEmailConfiguracao(int id)
        {
            try
            {
                _configuracaoEnvioDocumentacaoService.HabilitarEmailConfiguracao(id);

                return this.Json(new { Success = true, Message = "E-mail da configuração alterado com sucesso." });
            }
            catch (Exception ex)
            {
                return this.Json(new { Success = false, Message = ex.Message });
            }
        }

        public JsonResult EnviarDocumentacaoManualPorId(int id)
        {
            try
            {
                decimal? usuarioId = null;
                if (UsuarioAtual.Id.HasValue)
                {
                    usuarioId = Decimal.Parse(UsuarioAtual.Id.ToString());
                }

                _documentacaoVagaoService.InserirEnviosManual(id, usuarioId);
                return this.Json(new { Success = true, Message = "E-mail inserido na fila de processamento." });
            }
            catch (Exception ex)
            {
                return this.Json(new { Success = false, Message = ex.Message });
            }
        }

        public JsonResult EnviarDocumentacaoManualPorOsRecebedor(string numeroOs, int? recebedorId)
        {
            try
            {
                decimal? usuarioId = null;
                if (UsuarioAtual.Id.HasValue)
                {
                    usuarioId = Decimal.Parse(UsuarioAtual.Id.ToString());
                }

                _documentacaoVagaoService.InserirEnviosManual(numeroOs, recebedorId, usuarioId);
                return this.Json(new { Success = true, Message = "E-mail inserido na fila de processamento." });
            }
            catch (Exception ex)
            {
                return this.Json(new { Success = false, Message = ex.Message });
            }
        }

        private IList<string> RetornaColunasCustomizadas()
        {
            IList<string> colunas = new List<string>();

            colunas.Add("Enviado");
            colunas.Add("Enviado Em");
            colunas.Add("EstacaoEnvio");
            colunas.Add("Modo Envio");
            colunas.Add("Prefixo Trem");
            colunas.Add("Número OS");
            colunas.Add("Origem");
            colunas.Add("Atual");
            colunas.Add("Destino");
            colunas.Add("Chegada Aprox.");
            colunas.Add("Qntd.Vagões");
            colunas.Add("Trem Encerrado");
            colunas.Add("Recebedor");
            colunas.Add("Usuário");
            colunas.Add("Refaturamento");
            colunas.Add("Recomposicao");
            colunas.Add("Doc. Completa");
            colunas.Add("Doc. Excluída");
            colunas.Add("Link Doc.");

            return colunas;
        }

        public JsonResult ObterMalhas()
        {
            try
            {
                var results = _painelExpedicaoService.ObterMalhas();

                //// por enquanto remove a LARGA pois na query vai tratar para trazer a LARGA junto com a NORTE no resultado
                results.Remove("LARGA");

                return Json(new
                {
                    Success = true,
                    Items = results.Select(malha => new
                    {
                        Id = malha,
                        Nome = malha
                    }).ToList()
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ErrorMessage = ex.Message
                });
            }
        }

        #endregion JsonResults

        #region Métodos

        private string FormataCnpj(string cnpj)
        {
            if (string.IsNullOrEmpty(cnpj))
            {
                return "00.000.000/0000-00";
            }

            var cnpjFormatado = cnpj.Replace(".", string.Empty).Replace("/", string.Empty).Replace("-", string.Empty).PadLeft(14, '0');
            cnpjFormatado = string.Format("{0}.{1}.{2}/{3}-{4}", cnpjFormatado.Substring(0, 2),
                                          cnpjFormatado.Substring(2, 3), cnpjFormatado.Substring(5, 3),
                                          cnpjFormatado.Substring(8, 4), cnpjFormatado.Substring(12, 2));

            return cnpjFormatado;
        }

        #endregion Métodos
    }
}

