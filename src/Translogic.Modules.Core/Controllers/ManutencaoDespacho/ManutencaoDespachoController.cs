namespace Translogic.Modules.Core.Controllers.ManutencaoDespacho
{
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Web.Mvc;
	using ALL.Core.Dominio;
	using Domain.Model.Dto;
	using Domain.Services.ManutencaoDespacho;
	using Translogic.Core.Infrastructure;
	using Translogic.Core.Infrastructure.Web;
	using Translogic.Core.Infrastructure.Web.Filters;

	/// <summary>
	/// Controller de Manuten��o de Despacho
	/// </summary>
	public class ManutencaoDespachoController : BaseSecureModuleController
    {
		private readonly ManutencaoDespachoService _manutencaoDespachoService;

		/// <summary>
		/// Construtor inicializando paremetros
		/// </summary>
		/// <param name="manutencaoDespachoService">Manuten��o Despacho Service</param>
		public ManutencaoDespachoController(ManutencaoDespachoService manutencaoDespachoService)
		{
			_manutencaoDespachoService = manutencaoDespachoService;
		}

		/// <summary>
		/// A��o padr�o ao ser chamado o controller
		/// </summary>
		/// <returns> View padr�o </returns>
		// [Autorizar(Transacao = "AJUSTEVELOMAQINST")]
		public ActionResult Index()
        {
            return View();
        }

		/*
		/// <summary>
		/// A��o de mostrar o Formulario de edi��o ao ser chamado o controller
		/// </summary>
		/// <param name="id"> Id do Ajuste de Velocidade. </param>
		/// <returns> View de inser��o/altera��o de Ajuste Velocidade</returns>
		// [Autorizar(Transacao = "AJUSTEVELOMAQINST", Acao = "Salvar")]
		public ActionResult Form(int? id)
		{
			if (id.HasValue)
			{
				AjusteVelocidade ajusteVelocidade = _maquinistaInstrutorService.ObterAjusteVelocidadePorId(id.Value);

				return View(ajusteVelocidade);
			}

			return View(new AjusteVelocidade());
		}
		*/

		/// <summary>
		/// Obt�m a lista de Fatores de ajuste de velocidade
		/// </summary>
		/// <param name="filter">Detalhes do filtro</param>
		/// <returns>Resultado Json</returns>
		// [Autorizar(Transacao = "AJUSTEVELOMAQINST", Acao = "Pesquisar")]
		public JsonResult Obter(DetalhesFiltro<object>[] filter)
		{
			// IList<ManutencaoDespachoDto> listaDto = _manutencaoDespachoService.ObterDadosDespacho(filter);
			IList<ManutencaoDespachoDto> listaDto = new List<ManutencaoDespachoDto>();
			ManutencaoDespachoDto dto = new ManutencaoDespachoDto();
			dto.Id = 1;
			dto.Motivo = "Teste";
			dto.Serie = "Teste";
            dto.Vagao = 1;
			dto.Origem = "Teste";
			dto.OriInterc = "Teste";
			dto.Destino = "Teste";
            dto.SerDes = 1;
            dto.NumDes = 1;
			dto.Ser6 = "Teste";
            dto.Num6 = 1;
            dto.DtDesp = DateTime.Now;
			dto.Mercadoria = "Teste";
			dto.Fluxo = "Teste";
            dto.Tu = 1;
            dto.Volume = 1;
            dto.SerNot = 1;
            dto.Nota = 1;
            dto.Chave = "1";
            dto.Peso = 1;
            dto.DtNota = DateTime.Now;
            dto.Valor = 1;
			dto.Usuario = "Teste";
			dto.Conteiner = "Teste";
            
			 listaDto.Add(dto);

			return Json(new
						{
							Items =
									listaDto.Select(
									g =>
									new
									{
										g.Id,
										Motivo = g.Motivo ?? string.Empty,
										Serie = g.Serie ?? string.Empty,
										g.Vagao,
										Origem = g.Origem ?? string.Empty,
										OriInterc = g.OriInterc ?? string.Empty,
										Destino = g.Destino ?? string.Empty,
										g.SerDes,
										g.NumDes,
										Ser6 = g.Ser6 ?? string.Empty,
										g.Num6,
										DtDesp = g.DtDesp.ToString("dd/MM/yyyy"),
										Mercadoria = g.Mercadoria ?? string.Empty,
										Fluxo = g.Fluxo ?? string.Empty,
										g.Tu,
										g.Volume,
										g.SerNot,
										g.Nota,
										g.Chave,
										g.Peso,
										DtNota = g.DtNota.ToString(),
										g.Valor,
										Usuario = g.Usuario ?? string.Empty,
										Conteiner = g.Conteiner ?? string.Empty
									})
								});
		}

		/// <summary>
		/// Salva uma lista de configura��es
		/// </summary>
		/// <param name="dtos"> Lista de models enviados pela grid </param>
		/// <returns> Resultado  JSON </returns>
		[JsonFilter(Param = "dtos", JsonDataType = typeof(ManutencaoDespachoDto[]))]
		public JsonResult Salvar(ManutencaoDespachoDto[] dtos)
		{
			try
			{
				// _detacService.InserirAtualizarConfiguracoes(dtos);
				return Json(new { success = true });
			}
			catch (Exception e)
			{
				return Json(new { success = false, e.Message, e.StackTrace });
			}
		}

		/*
		/// <summary>
		/// M�todo que remove um determinado fator de ajuste de velocidade
		/// </summary>
		/// <param name="id">Id do fator de ajuste de velocidade a ser exclu�do</param>
		/// <returns>Resultado Json</returns>
		// [Autorizar(Transacao = "AJUSTEVELOMAQINST", Acao = "Remover")]
		public JsonResult Remover(int id)
		{
			try
			{
				_maquinistaInstrutorService.RemoverAjusteVelocidade(id);

				return Json(new { success = true });
			}
			catch (Exception e)
			{
				return Json(new { success = false, e.Message, e.StackTrace });
			}
		}

		/// <summary>
		/// M�todo que Insere Altera o Fator de ajuste de velocidade
		/// </summary>
		/// <param name="ajusteVelocidade">Objeto Ajuste de Velocidade</param>
		/// <param name="status">Status do Fator Ajuste de Velocidade</param>
		/// <returns> Resultado JSonResult </returns>
		// [Autorizar(Transacao = "AJUSTEVELOMAQINST", Acao = "Salvar")]
		public JsonResult Salvar(AjusteVelocidade ajusteVelocidade, string status)
		{
			try
			{
				IAreaOperacional areaOperacional = _maquinistaInstrutorService.ObterAreaOperacionalPorCodigo(ajusteVelocidade.CodigoOrigem);
				if (areaOperacional == null)
				{
					throw new TranslogicException("Codigo de Origem n�o encontrado!");
				}

				areaOperacional = _maquinistaInstrutorService.ObterAreaOperacionalPorCodigo(ajusteVelocidade.CodigoDestino);
				if (areaOperacional == null)
				{
					throw new TranslogicException("Codigo de Destino n�o encontrado!");
				}

				if (ajusteVelocidade.Id > 0)
				{
					string origem = ajusteVelocidade.CodigoOrigem;
					string destino = ajusteVelocidade.CodigoDestino;
					double fator = ajusteVelocidade.FatorAjusteVelocidade;
					DateTime inicioVigencia = ajusteVelocidade.DataInicioVigencia;
					DateTime fimVigencia = ajusteVelocidade.DataFimVigencia;
					fimVigencia = fimVigencia.AddHours(23);
					fimVigencia = fimVigencia.AddMinutes(59);
					fimVigencia = fimVigencia.AddSeconds(59);
					bool vigente = status == null ? false : true;

					ajusteVelocidade = _maquinistaInstrutorService.ObterAjusteVelocidadePorId(ajusteVelocidade.Id.Value);

					ajusteVelocidade.CodigoOrigem = origem;
					ajusteVelocidade.CodigoDestino = destino;
					ajusteVelocidade.FatorAjusteVelocidade = fator;
					ajusteVelocidade.DataInicioVigencia = inicioVigencia;
					ajusteVelocidade.DataFimVigencia = fimVigencia;
					ajusteVelocidade.Vigente = vigente;
				}

				_maquinistaInstrutorService.InserirAtualizarAjusteVelocidade(ajusteVelocidade);

				return Json(new { success = true });
			}
			catch (Exception e)
			{
				return Json(new { success = false, e.Message, e.StackTrace });
			}
		}
		*/
    }
}