﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Translogic.Core.Infrastructure.Web;
using Translogic.Modules.Core.Spd.Data.Enums;

namespace Translogic.Modules.Core.Controllers.Logs
{
    public class LogsController : BaseModuleController
    {

        /// <summary>
        /// Salva um log ocorrido no JavaScript
        /// </summary>
        /// <param name="obj"></param>
        [HttpPost]
        public void SalvarWebException(object obj)
        {
            try
            {
                var serializer = new JavaScriptSerializer { MaxJsonLength = Int32.MaxValue };
                string objJson = serializer.Serialize(obj);
                Sis.Log("Erro em :" + Request.Url, EnumLogTipo.WebException, null, null, null, null, null, Session["usuario"] as string);
            }
            catch
            {
            }
        }

    }

}
