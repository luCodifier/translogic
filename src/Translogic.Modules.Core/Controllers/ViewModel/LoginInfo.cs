namespace Translogic.Modules.Core.Controllers.ViewModel
{
	using App_GlobalResources;
	using NHibernate.Validator.Cfg.Loquacious;

	/// <summary>
	/// Define os dados de login
	/// </summary>
	public class LoginInfo
	{
		#region PROPRIEDADES
		/// <summary>
		/// Idioma a fazer login
		/// </summary>
		public string Idioma { get; set; }

		/// <summary>
		/// Indica se foi efetuado o logout
		/// </summary>
		public bool LogoutEfetuado { get; set; }

		/// <summary>
		/// Indica se foi enviado o lembrete de senha
		/// </summary>
		public bool LembreteSenhaEnviado { get; set; }

		/// <summary>
		/// Campo de Senha do usu�rio
		/// </summary>
		public string Senha { get; set; }

		/// <summary>
		/// Campo do c�digo do usu�rio
		/// </summary>
		public string Usuario { get; set; }

		/// <summary>
		/// Campo do Nome do usu�rio
		/// </summary>
		public string NomeUsuario { get; set; }

		#endregion
	}

	/// <summary>
	/// Valida��es em server-side 
	/// </summary>
	public class LoginInfoValidations : ValidationDef<LoginInfo>
	{
		#region CONSTRUTORES
		/// <summary>
		/// Construtor que define as valida��es
		/// </summary>
		public LoginInfoValidations()
		{
			Define(info => info.Idioma)
				.NotNullableAndNotEmpty()
				.WithMessage(Acesso.IdiomaNaoVazio);

			Define(info => info.Senha)
				.NotNullableAndNotEmpty()
				.WithMessage(Acesso.SenhaVazia);

			/*
			Define(info => info.Senha)
				.MinLength(5)
				.WithMessage(Acesso.SenhaTamanhoMinimo);
			*/

			Define(info => info.Usuario)
				.NotNullableAndNotEmpty()
				.WithMessage(Acesso.UsuarioVazio);
		}

		#endregion
	}
}