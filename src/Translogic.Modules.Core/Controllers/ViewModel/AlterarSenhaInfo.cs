namespace Translogic.Modules.Core.Controllers.ViewModel
{
	using App_GlobalResources;
	using NHibernate.Validator.Cfg.Loquacious;

	/// <summary>
	/// Define os dados de login
	/// </summary>
	public class AlterarSenhaInfo
	{
		#region PROPRIEDADES
		
		/// <summary>
		/// Campo da Senha Atual
		/// </summary>
		public string Senha { get; set; }

		/// <summary>
		/// Campo da Nova Senha do usu�rio
		/// </summary>
		public string SenhaNova { get; set; }

		/// <summary>
		/// Campo da Confirma��o da Nova Senha do usu�rio
		/// </summary>
		public string SenhaNovaConfirmacao { get; set; }

		#endregion
	}

	/// <summary>
	/// Valida��es em server-side 
	/// </summary>
	public class AlterarSenhaInfoValidations : ValidationDef<AlterarSenhaInfo>
	{
		#region CONSTRUTORES
		/// <summary>
		/// Construtor que define as valida��es
		/// </summary>
		public AlterarSenhaInfoValidations()
		{
			Define(info => info.Senha)
				.NotNullableAndNotEmpty()
				.WithMessage(Acesso.SenhaVazia);

			Define(info => info.SenhaNova)
				.NotNullableAndNotEmpty()
				.WithMessage(Acesso.SenhaNovaVazia);

			Define(info => info.SenhaNovaConfirmacao)
				.NotNullableAndNotEmpty()
				.WithMessage(Acesso.SenhaNovaConfirmacaoVazia);

			Define(info => info.SenhaNova)
				.MinLength(8)
				.WithMessage(Acesso.SenhaDeveConter8Caracteres);
		}

		#endregion
	}
}
