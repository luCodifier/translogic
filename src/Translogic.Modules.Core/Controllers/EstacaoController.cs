namespace Translogic.Modules.Core.Controllers
{
	using System.Linq;
	using System.Web.Mvc;
	using ALL.Core.Dominio;
	using Domain.Model.CadastroUnicoEstacoes;
	using Domain.Services.CadastroUnicoEstacoes;
	using Translogic.Core.Infrastructure.Web;

	/// <summary>
	/// Controller de Esta��o do Cadastro �nico
	/// </summary>
	public class EstacaoController : BaseSecureModuleController
	{
		#region ATRIBUTOS READONLY & EST�TICOS

		private readonly EstacaoService _service;

		#endregion

		#region CONSTRUTORES
		/// <summary>
		/// Construtor injetando o servi�o
		/// </summary>
		/// <param name="service">Servido de esta��o do cadastro �nico</param>
		public EstacaoController(EstacaoService service)
		{
			_service = service;
		}

		#endregion

		#region M�TODOS
		/// <summary>
		/// M�todo que obt�m a lista de Esta��es do Cadastro �nico
		/// </summary>
		/// <param name="pagination">Detalhes de pagina��o - <see cref="DetalhesPaginacaoWeb"/></param>
		/// <param name="filter">Filtro da pesquisa</param>
		/// <returns>Resultado Json</returns>
		public virtual JsonResult GetEstacoes(DetalhesPaginacaoWeb pagination, string filter)
		{
			ResultadoPaginado<Estacao> result = _service.ObterTodosPaginado(pagination);

			return Json(new
			            	{
			            		result.Total,
			            		Items = result.Items.Select(
			            	e => new
			            	     	{
			            	     		e.Id,
			            	     		e.Altitude,
			            	     		e.Codigo,
			            	     		e.Direcao,
			            	     		e.Km,
			            	     		e.Latitude,
			            	     		e.Longitude,
			            	     		e.PontosNotaveis,
			            	     		e.Raio,
			            	     		e.SBDesviadaDireita,
			            	     		e.SBDesviadaEsquerda,
			            	     		e.SubdivisaoLinhaDesviada,
			            	     		e.SubdivisaoLinhaPrincipal,
			            	     		e.TempoDescida,
			            	     		e.TempoSubida,
			            	     		e.VelocidadeCrescente,
			            	     		e.VelocidadeDecrescente
			            	     	})
			            	});
		}

		/// <summary>
		/// Action padr�o
		/// </summary>
		/// <returns>View do cadastro de esta��es</returns>
		public virtual ActionResult Index()
		{
			return View();
		}

		#endregion
	}
}