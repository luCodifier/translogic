﻿namespace Translogic.Modules.Core.Controllers.Despacho
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Web.Mvc;
    using Domain.Model.Diversos;
    using Domain.Model.Diversos.Cte;
    using Domain.Model.Dto;
    using Domain.Model.Estrutura;
    using Domain.Model.FluxosComerciais;
    using Domain.Model.FluxosComerciais.Carregamentos;
    using Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
    using Domain.Model.FluxosComerciais.Nfes;
    using Domain.Model.Trem.Veiculo.Conteiner;
    using Domain.Model.Via;
    using Domain.Services.Ctes;
    using Domain.Services.FluxosComerciais;
    using Infrastructure;
    using Translogic.Core.Infrastructure;
    using Translogic.Core.Infrastructure.Web.Filters;
    using Translogic.Modules.Core.Domain.Model.Codificador.Repositories;
    using Translogic.Modules.Core.Domain.Model.PainelExpedicao.Parametrizacao.Enums;
    using Translogic.Modules.Core.Domain.Services.PainelExpedicao;
    using Util;
    using Translogic.Modules.Core.Domain.Model.Bolar.Repositories;

    /// <summary>
    /// Controller de carregamento
    /// </summary>
    public class CarregamentoController : BaseSecureModuleController
    {
        private readonly IConfiguracaoTranslogicRepository _configuracaoTranslogicRepository;
        private readonly CarregamentoService _carregamentoService;
        private readonly NfeService _nfeService;
        private readonly CteService _cteService;
        private readonly PainelExpedicaoService _painelExpedicaoService;
        private readonly IBolarBoRepository _bolarBoRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="CarregamentoController"/> class.
        /// </summary>
        /// <param name="carregamentoService"> Serviço de carregamento injetado. </param>
        /// <param name="nfeService"> Serviço de Nfe injetado</param>
        /// <param name="cteService">Serviço de Cte Injetado</param>
        /// <param name="configuracaoTranslogicRepository"> The configuracao translogic repository. </param>
        public CarregamentoController(CarregamentoService carregamentoService,
                                      NfeService nfeService,
                                      CteService cteService,
                                      IConfiguracaoTranslogicRepository configuracaoTranslogicRepository,
                                      PainelExpedicaoService painelExpedicaoService,
                                      IBolarBoRepository bolarBoRepository)
        {
            _carregamentoService = carregamentoService;
            _cteService = cteService;
            _nfeService = nfeService;
            _configuracaoTranslogicRepository = configuracaoTranslogicRepository;
            _painelExpedicaoService = painelExpedicaoService;
            _bolarBoRepository = bolarBoRepository;
        }

        /// <summary>
        /// Action de inicio da tela de carregametno
        /// </summary>
        /// <returns> View da tela principal de carregamento </returns>
        [Autorizar(Transacao = "DESPACHO")]
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Detalhe do carregamento
        /// </summary>
        /// <param name="idFlx"> Id do fluxo comercial. </param>
        /// <param name="codigoOrigem"> Código da estação de origem </param>
        /// <param name="codigoDestino"> Código da estação de destino</param>
        /// <param name="codigoMercadoria"> Código da mercadoria </param>
        /// <returns> Tela do detalhe do carregamento do fluxo </returns>
        [Autorizar(Transacao = "DESPACHODET", Acao = "Salvar")]
        public ActionResult Detalhe(int? idFlx, string codigoOrigem, string codigoDestino, string codigoMercadoria)
        {
            if (!idFlx.HasValue)
            {
                return RedirectToAction("Index");
            }

            FluxoComercial fluxo = _carregamentoService.VerificarValidadeFluxoCarregamento(idFlx.Value);

            // Verifica regra conteiner vazio
            string peso = _carregamentoService.VerificarConteinerVazio(fluxo);

            if (!string.IsNullOrEmpty(peso))
            {
                ViewData["CONT_SERIE"] = 1;
                ViewData["CONT_NUMERO"] = 1;
                ViewData["CONT_DATA"] = DateTime.Now;
                ViewData["CONT_PESO"] = peso;
            }

            double margemPercLiberacaoFaturamento = _carregamentoService.VerificarMargemLiberacaoFaturamento(fluxo.EmpresaRemetente);
            bool indFluxoCte = _carregamentoService.VerificarFluxoCte(fluxo);
            bool indFluxoLiberaDataDespacho = _carregamentoService.VerificarLiberacaoDataDespacho(fluxo);
            bool indValidaRemetenteFiscal = _carregamentoService.VerificarCnpjRemetenteFiscal();
            bool indValidaDestinatarioFiscal = _carregamentoService.VerificarCnpjDestinatarioFiscal();
            bool indValidaCfopContrato = _carregamentoService.VerificarCfopContrato();
            bool indTravaSaldoNfe = _carregamentoService.VerificarTravaSaldoNfe(fluxo, null);
            bool indFluxoRateioCte = _carregamentoService.VerificarFluxoRateioCte(fluxo);

            ViewData["MARGEM_PERC_LIB_FAT"] = margemPercLiberacaoFaturamento;
            ViewData["IND_FLUXO_CTE"] = indFluxoCte;
            ViewData["IND_RATEIO_CTE"] = indFluxoRateioCte;
            ViewData["IND_LIBERA_DATA_DESPACHO"] = indFluxoLiberaDataDespacho;
            ViewData["ID_FLUXO_COMERCIAL"] = fluxo.Id;
            ViewData["IND_PREENCHIMENTO_TARA"] = _carregamentoService.VerificarPreenchimentoTara(fluxo);
            ViewData["IND_TARA_OBRIGATORIO"] = fluxo.IndTransbordo.HasValue ? fluxo.IndTransbordo.Value : false;
            ViewData["IND_NFE_OBRIGATORIO"] = _carregamentoService.VerificarNfeObrigatorio(fluxo);
            ViewData["IND_TRAVA_SALDO_NFE"] = indTravaSaldoNfe;
            ViewData["CFOP_CONTRATO"] = fluxo.Contrato.CfopProduto;
            ViewData["CODIGO_FLUXO"] = fluxo.Codigo;
            ViewData["TIPO_SERVICO_CTE"] = fluxo.TipoServicoCte ?? 0;

            string variacaoCfop = string.Empty;
            bool indPreenchimentoVolume;
            bool liberarTravasCorrentista = _cteService.VerificarLiberacaoTravasCorrentista(fluxo);

            if (fluxo.Codigo.Equals("99999"))
            {
                indValidaCfopContrato = false;
                indValidaRemetenteFiscal = false;
                indValidaDestinatarioFiscal = false;

                Mercadoria mercadoria = _carregamentoService.ObterMercadoriaPorCodigo(codigoMercadoria);
                EstacaoMae origem = _carregamentoService.ObterEstacaoMaePorCodigo(codigoOrigem);
                EstacaoMae destino = _carregamentoService.ObterEstacaoMaePorCodigo(codigoDestino);

                IEmpresa empresaRemetente = _carregamentoService.ObterEmpresaPorEstacao(origem);
                if (empresaRemetente != null)
                {
                    ViewData["CNPJ_REMETENTE_FISCAL"] = empresaRemetente.TipoPessoa.Value.Equals(TipoPessoaEnum.Juridica) ? empresaRemetente.Cgc : empresaRemetente.Cpf;
                    ViewData["SIGLA_REMETENTE_FISCAL"] = empresaRemetente.Sigla;
                    ViewData["IE_REMETENTE_FISCAL"] = empresaRemetente.InscricaoEstadual;
                    ViewData["NOME_REMETENTE_FISCAL"] = empresaRemetente.RazaoSocial;
                    ViewData["DSC_REMETENTE_FISCAL"] = empresaRemetente.DescricaoResumida;
                    ViewData["UF_REMETENTE_FISCAL"] = empresaRemetente.Estado.Sigla;
                    ViewData["PJ_REMETENTE_FISCAL"] = empresaRemetente.TipoPessoa.Value.Equals(TipoPessoaEnum.Juridica);
                }

                IEmpresa empresaDestinataria = _carregamentoService.ObterEmpresaPorEstacao(destino);
                if (empresaDestinataria != null)
                {
                    ViewData["CNPJ_DESTINATARIO_FISCAL"] = empresaDestinataria.TipoPessoa.Value.Equals(TipoPessoaEnum.Juridica) ? empresaDestinataria.Cgc : empresaDestinataria.Cpf;
                    ViewData["SIGLA_DESTINATARIO_FISCAL"] = empresaDestinataria.Sigla;
                    ViewData["IE_DESTINATARIO_FISCAL"] = empresaDestinataria.InscricaoEstadual;
                    ViewData["NOME_DESTINATARIO_FISCAL"] = empresaDestinataria.RazaoSocial;
                    ViewData["DSC_DESTINATARIO_FISCAL"] = empresaDestinataria.DescricaoResumida;
                    ViewData["UF_DESTINATARIO_FISCAL"] = empresaDestinataria.Estado.Sigla;
                    ViewData["PJ_DESTINATARIO_FISCAL"] = empresaDestinataria.TipoPessoa.Value.Equals(TipoPessoaEnum.Juridica);
                }

                // ViewData["IND_FLUXO_INTERCAMBIO"] = false;
                ViewData["ID_ESTACAO_ORIGEM"] = origem.Id;
                ViewData["CODIGO_ESTACAO_ORIGEM"] = codigoOrigem;

                ViewData["IND_FLUXO_INTERNACIONAL_TIF_OBRIGATORIO"] = false;

                ViewData["ID_ESTACAO_DESTINO"] = destino.Id;
                ViewData["CODIGO_ESTACAO_DESTINO"] = codigoDestino;

                ViewData["ID_MERCADORIA"] = mercadoria.Id;
                ViewData["DESCRICAO_MERCADORIA"] = mercadoria.DescricaoResumida.Replace("\"", string.Empty);

                ViewData["IND_VERIFICA_DADOS_FISCAIS"] = false;
                ViewData["IND_FLUXO_ORIGEM_INTERCAMBIO"] = false;
                ViewData["IND_FLUXO_INTERNACIONAL"] = false;

                indPreenchimentoVolume = _carregamentoService.VerificarPreenchimentoVolume(mercadoria);
                ViewData["IND_PREENCHIMENTO_VOLUME"] = indPreenchimentoVolume;
                ViewData["IND_FLUXO_CONTEINER"] = _carregamentoService.VerificarFluxoConteiner(mercadoria);

                ViewData["IND_LONG_STACK"] = false;
                ViewData["IND_MERCADORIA_PALLETS"] = false;
            }
            else
            {
                ViewData["IND_LONG_STACK"] = fluxo.IndLongStack ?? false;
                if (fluxo.Contrato.EmpresaRemetenteFiscal != null)
                {
                    EmpresaCliente empresaRemetente = fluxo.Contrato.EmpresaRemetenteFiscal;
                    ViewData["CNPJ_REMETENTE_FISCAL"] = empresaRemetente.TipoPessoa.Value.Equals(TipoPessoaEnum.Juridica) ? empresaRemetente.Cgc : empresaRemetente.Cpf;
                    ViewData["SIGLA_REMETENTE_FISCAL"] = empresaRemetente.Sigla;
                    ViewData["IE_REMETENTE_FISCAL"] = empresaRemetente.InscricaoEstadual;
                    ViewData["DSC_REMETENTE_FISCAL"] = empresaRemetente.DescricaoResumida;
                    ViewData["NOME_REMETENTE_FISCAL"] = empresaRemetente.RazaoSocial;
                    ViewData["UF_REMETENTE_FISCAL"] = (empresaRemetente.Estado == null) ? string.Empty : empresaRemetente.Estado.Sigla;
                    ViewData["PJ_REMETENTE_FISCAL"] = empresaRemetente.TipoPessoa.Value.Equals(TipoPessoaEnum.Juridica);

                    if (!empresaRemetente.TipoPessoa.Value.Equals(TipoPessoaEnum.Juridica))
                    {
                        indValidaRemetenteFiscal = false;
                    }

                    if (indValidaRemetenteFiscal)
                    {
                        indValidaRemetenteFiscal = _carregamentoService.VerificarEmpresaNacional(fluxo.Contrato.EmpresaRemetenteFiscal);
                    }
                }

                if (fluxo.Contrato.EmpresaDestinatariaFiscal != null)
                {
                    EmpresaCliente empresaDestinataria = fluxo.Contrato.EmpresaDestinatariaFiscal;
                    ViewData["CNPJ_DESTINATARIO_FISCAL"] = empresaDestinataria.TipoPessoa.Value.Equals(TipoPessoaEnum.Juridica) ? empresaDestinataria.Cgc : empresaDestinataria.Cpf;
                    ViewData["SIGLA_DESTINATARIO_FISCAL"] = empresaDestinataria.Sigla;
                    ViewData["IE_DESTINATARIO_FISCAL"] = empresaDestinataria.InscricaoEstadual;
                    ViewData["NOME_DESTINATARIO_FISCAL"] = empresaDestinataria.RazaoSocial;
                    ViewData["DSC_DESTINATARIO_FISCAL"] = empresaDestinataria.DescricaoResumida;
                    ViewData["UF_DESTINATARIO_FISCAL"] = (empresaDestinataria.Estado == null) ? string.Empty : empresaDestinataria.Estado.Sigla;
                    ViewData["PJ_DESTINATARIO_FISCAL"] = empresaDestinataria.TipoPessoa.Value.Equals(TipoPessoaEnum.Juridica);

                    if (!empresaDestinataria.TipoPessoa.Value.Equals(TipoPessoaEnum.Juridica))
                    {
                        indValidaDestinatarioFiscal = false;
                    }

                    if (indValidaDestinatarioFiscal)
                    {
                        indValidaDestinatarioFiscal = _carregamentoService.VerificarEmpresaNacional(fluxo.Contrato.EmpresaDestinatariaFiscal);
                    }
                }

                // ViewData["IND_FLUXO_INTERCAMBIO"] = _carregamentoService.VerificarFluxoIntercambio(fluxo);
                bool indFluxoInternacional = _carregamentoService.VerificarFluxoInternacional(fluxo);

                if (indFluxoInternacional)
                {
                    indValidaCfopContrato = false;
                }

                ViewData["IND_FLUXO_ORIGEM_INTERCAMBIO"] = _carregamentoService.VerificarFluxoOrigemIntercambio(fluxo);
                ViewData["IND_FLUXO_INTERNACIONAL"] = indFluxoInternacional;
                indPreenchimentoVolume = _carregamentoService.VerificarPreenchimentoVolume(fluxo);
                ViewData["IND_PREENCHIMENTO_VOLUME"] = indPreenchimentoVolume;

                ViewData["IND_FLUXO_INTERNACIONAL_TIF_OBRIGATORIO"] = indFluxoInternacional ? _carregamentoService.VerificarTifObrigatorio(fluxo) : false;

                if (fluxo.OrigemIntercambio != null)
                {
                    ViewData["ID_ESTACAO_ORIGEM"] = fluxo.OrigemIntercambio.Id;
                    ViewData["CODIGO_ESTACAO_ORIGEM"] = fluxo.Origem.Codigo + "/" + fluxo.OrigemIntercambio.Codigo;
                    codigoOrigem = fluxo.OrigemIntercambio.Codigo;
                }
                else
                {
                    ViewData["ID_ESTACAO_ORIGEM"] = fluxo.Origem.Id;
                    ViewData["CODIGO_ESTACAO_ORIGEM"] = fluxo.Origem.Codigo;
                    codigoOrigem = fluxo.Origem.Codigo;
                }

                if (fluxo.DestinoIntercambio != null)
                {
                    ViewData["ID_ESTACAO_DESTINO"] = fluxo.DestinoIntercambio.Id;
                    ViewData["CODIGO_ESTACAO_DESTINO"] = fluxo.Destino.Codigo + "/" + fluxo.DestinoIntercambio.Codigo;
                }
                else
                {
                    ViewData["ID_ESTACAO_DESTINO"] = fluxo.Destino.Id;
                    ViewData["CODIGO_ESTACAO_DESTINO"] = fluxo.Destino.Codigo;
                }

                ViewData["ID_MERCADORIA"] = fluxo.Mercadoria.Id;
                ViewData["DESCRICAO_MERCADORIA"] = fluxo.Mercadoria.DescricaoResumida.Replace("\"", string.Empty);
                codigoMercadoria = fluxo.Mercadoria.Codigo;
                ViewData["IND_VERIFICA_DADOS_FISCAIS"] = true;
                ViewData["IND_FLUXO_CONTEINER"] = _carregamentoService.VerificarFluxoConteiner(fluxo);
                ViewData["IND_MERCADORIA_PALLETS"] = _carregamentoService.VerificarMercadoriaPallets(fluxo.Mercadoria.DescricaoDetalhada);
                variacaoCfop = _carregamentoService.ObterVariacoesCfop(fluxo.Contrato.CfopProduto);

                if (liberarTravasCorrentista || !indFluxoCte)
                {
                    indValidaRemetenteFiscal = false;
                    indValidaDestinatarioFiscal = false;
                }
            }

            ViewData["IND_LIBERAR_TRAVA_CORRENTISTA"] = liberarTravasCorrentista;
            ViewData["IND_VALIDAR_CNPJ_REMETENTE"] = indValidaRemetenteFiscal;
            ViewData["IND_VALIDAR_CNPJ_DESTINATARIO"] = indValidaDestinatarioFiscal;
            ViewData["IND_VALIDAR_CFOP_CONTRATO"] = indValidaCfopContrato;

            ViewData["VARIACAO_CFOP"] = variacaoCfop;

            ViewData["CODIGO_MERCADORIA"] = codigoMercadoria;
            if (indPreenchimentoVolume)
            {
                ViewData["IND_VERIFICA_PESO_POR_VOLUME"] = _carregamentoService.VerificarPesoPorVolume(codigoOrigem);
            }
            else
            {
                ViewData["IND_VERIFICA_PESO_POR_VOLUME"] = false;
            }

            int mesesRetroativos = _carregamentoService.ObterMesesRetroativos();
            ViewData["MESES_RETROATIVOS"] = mesesRetroativos;

            mesesRetroativos = mesesRetroativos * -1;
            ViewData["LIMITADOR_DATA_NF"] = DateTime.Now.AddMonths(mesesRetroativos);

            var configuracao = _configuracaoTranslogicRepository.ObterPorId("LIBERACAO_DT_180_CGTO_ERRO_V07");
            ViewData["LIB_NOTA_MAIOR_QUE_180_DIAS"] = configuracao.Valor;

            return View();
        }

        /// <summary>
        /// Abre tela de notas fiscais
        /// </summary>
        /// <param name="idVagao"> Id do vagao. </param>
        /// <param name="codigoVagao"> Código do Vagão </param>
        /// <returns> Tela de notas fiscais </returns>
        public ActionResult GridNotaFiscal(int? idVagao, string codigoVagao)
        {
            double pesoTotal;
            double pesoRemanescente;
            bool indMultiploDespacho = _carregamentoService.ObterPesosMultiploDespacho(idVagao, out pesoTotal, out pesoRemanescente);
            ViewData["IND_VAGAO_JA_DESPACHADO"] = indMultiploDespacho;
            ViewData["PESO_TOTAL_VAGAO"] = pesoTotal;
            ViewData["PESO_REMANESCENTE_VAGAO"] = pesoRemanescente;
            ViewData["IdVagao"] = idVagao;
            ViewData["CodigoVagao"] = codigoVagao;
            return View();
        }

        /// <summary>
        /// Abre tela de edição de nota fiscal
        /// </summary>
        /// <returns> Tela de edição de nota fiscal </returns>
        public ActionResult FormNotaFiscal()
        {
            return View();
        }

        /// <summary>
        /// Abre tela de consulta de Fluxo
        /// </summary>
        /// <param name="idFlx">Codigo do fluxo</param>
        /// <returns> Tela de edição de nota fiscal </returns>
        public ActionResult ConsultaFluxo(string idFlx)
        {
            FluxoComercial fluxoComercial = _carregamentoService.BuscarDadosFluxoComercial(idFlx);

            ViewData["FLUXO_ORIGEM"] = string.Empty;
            ViewData["FLUXO_DESTINO"] = string.Empty;
            ViewData["FLUXO_MERCADORIA"] = string.Empty;
            ViewData["FLUXO_CODIGO"] = string.Empty;
            ViewData["FLUXO_CFOP"] = string.Empty;
            ViewData["FLUXO_CFOPPROD"] = string.Empty;
            ViewData["FLUXO_REMETENTE"] = string.Empty;
            ViewData["FLUXO_REM_CNPJ"] = string.Empty;
            ViewData["FLUXO_REM_SIGLA"] = string.Empty;
            ViewData["FLUXO_REM_NOME"] = string.Empty;
            ViewData["FLUXO_REM_UF"] = string.Empty;
            ViewData["FLUXO_REM_IE"] = string.Empty;
            ViewData["FLUXO_DESTINATARIO"] = string.Empty;
            ViewData["FLUXO_DES_CNPJ"] = string.Empty;
            ViewData["FLUXO_DES_SIGLA"] = string.Empty;
            ViewData["FLUXO_DES_NOME"] = string.Empty;
            ViewData["FLUXO_DES_UF"] = string.Empty;
            ViewData["FLUXO_DES_IE"] = string.Empty;

            if (fluxoComercial != null)
            {
                ViewData["FLUXO_ORIGEM"] = fluxoComercial.Origem.Codigo;
                ViewData["FLUXO_DESTINO"] = fluxoComercial.Destino.Codigo;
                ViewData["FLUXO_MERCADORIA"] = fluxoComercial.Mercadoria.DescricaoResumida;
                ViewData["FLUXO_CODIGO"] = fluxoComercial.Codigo;

                if (fluxoComercial.Contrato != null)
                {
                    ViewData["FLUXO_CFOP"] = fluxoComercial.Contrato.Cfop ?? string.Empty;
                    ViewData["FLUXO_CFOPPROD"] = fluxoComercial.Contrato.CfopProduto ?? string.Empty;

                    if (fluxoComercial.Contrato.EmpresaRemetenteFiscal != null)
                    {
                        ViewData["FLUXO_REMETENTE"] = fluxoComercial.Contrato.EmpresaRemetenteFiscal.NomeFantasia;
                        ViewData["FLUXO_REM_CNPJ"] = fluxoComercial.Contrato.EmpresaRemetenteFiscal.Cgc ?? string.Empty;
                        ViewData["FLUXO_REM_SIGLA"] = fluxoComercial.Contrato.EmpresaRemetenteFiscal.Sigla;
                        ViewData["FLUXO_REM_NOME"] = fluxoComercial.Contrato.EmpresaRemetenteFiscal.RazaoSocial;
                        ViewData["FLUXO_REM_UF"] = fluxoComercial.Contrato.EmpresaRemetenteFiscal.Estado.Sigla;
                        ViewData["FLUXO_REM_IE"] = fluxoComercial.Contrato.EmpresaRemetenteFiscal.InscricaoEstadual;
                    }

                    if (fluxoComercial.Contrato.EmpresaDestinatariaFiscal != null)
                    {
                        ViewData["FLUXO_DESTINATARIO"] = fluxoComercial.Contrato.EmpresaDestinatariaFiscal.NomeFantasia;
                        ViewData["FLUXO_DES_CNPJ"] = fluxoComercial.Contrato.EmpresaDestinatariaFiscal.Cgc ?? string.Empty;
                        ViewData["FLUXO_DES_SIGLA"] = fluxoComercial.Contrato.EmpresaDestinatariaFiscal.Sigla;
                        ViewData["FLUXO_DES_NOME"] = fluxoComercial.Contrato.EmpresaDestinatariaFiscal.RazaoSocial;
                        ViewData["FLUXO_DES_UF"] = fluxoComercial.Contrato.EmpresaDestinatariaFiscal.Estado.Sigla;
                        ViewData["FLUXO_DES_IE"] = fluxoComercial.Contrato.EmpresaDestinatariaFiscal.InscricaoEstadual;
                        ViewData["FLUXO_DES_TPSERVICOCTE"] =
                            FluxoComercial.ObterDescricaoTipoServicoCte(fluxoComercial.TipoServicoCte);
                    }
                }
            }

            return View();
        }

        private string ObterDescricaoTipoServicoCte(int? tipoServicoCte)
        {
            var valor = string.Empty;

            if (tipoServicoCte.HasValue)
            {
                switch (tipoServicoCte.Value)
                {
                    case 0:
                        valor = "NORMAL";
                        break;
                    case 1:
                        valor = "SUBCONTRATAÇÃO";
                        break;
                    case 2:
                        valor = "REDESPACHO";
                        break;
                    case 3:
                        valor = "REDESPACHO INTERMEDIARIO";
                        break;
                    case 4:
                        valor = "VINCULADO A MULTIMODAL";
                        break;
                }
            }

            return valor;
        }

        /// <summary>
        /// Action de formulário de atualização de dados da NF-e
        /// </summary>
        /// <returns> View do formulário da NF-e </returns>
        public ActionResult FormNfe()
        {
            return View();
        }

        /// <summary>
        /// Salva o carregamento
        /// </summary>
        /// <param name="carregamento"> The carregamento. </param>
        /// <returns> Resultado Json </returns>
        [JsonFilter(Param = "carregamento", JsonDataType = typeof(CarregamentoDto))]
        [Autorizar(Transacao = "DESPACHODET", Acao = "Salvar")]
        [ValidateInput(false)]
        public JsonResult SalvarCarregamento(CarregamentoDto carregamento)
        {
            bool erro = false;
            string mensagem = string.Empty;
            List<int?> idsVagaoRemover = new List<int?>();
            string codigoVagaoRemover;
            bool removerVagao = false;

            try
            {
                _carregamentoService.LogarEventosCarregamento(carregamento.LogDespacho, UsuarioAtual);
            }
            catch (Exception)
            {
            }

            var listaVagoes = _carregamentoService.SalvarCarregamento(carregamento, UsuarioAtual);
            foreach (CarregamentoVagaoDto carregamentoVagaoDto in listaVagoes)
            {
                mensagem = string.Empty;
                if (carregamentoVagaoDto.IndErro)
                {
                    mensagem = TratarException(carregamentoVagaoDto.Excecao);
                    int? idVagaoRemover;
                    mensagem = TratarRemoverVagao(mensagem, out removerVagao, out idVagaoRemover, out codigoVagaoRemover);
                    if (removerVagao)
                    {
                        mensagem = string.Concat(mensagem, " Por este motivo foi removido da lista.");

                        idsVagaoRemover.Add(idVagaoRemover);
                    }

                    carregamentoVagaoDto.MensagemErro = mensagem;
                    erro = true;
                }
            }

            try
            {
                _carregamentoService.LogarFechamentoEventosCarregamento(carregamento.LogDespacho, erro, mensagem);
            }
            catch (Exception)
            {
                throw;
            }

            return Json(new
            {
                Erro = erro,
                Mensagens = erro ? listaVagoes.Select(g => new
                                                 {
                                                     g.IdVagao,
                                                     g.CodigoVagao,
                                                     g.IndErro,
                                                     g.MensagemErro
                                                 }) : null,
                IdsVagaoRemover = idsVagaoRemover.Count > 0 ? idsVagaoRemover.ToArray() : null
            });
        }

        /// <summary>
        /// Verifica os dados do fluxo comercial
        /// </summary>
        /// <param name="codigoFluxoComercial"> The codigo fluxo comercial. </param>
        /// <param name="codigoOrigemFluxo"> Código da origem do fluxo</param>
        /// <param name="codigoDestinoFluxo"> Código do destino do fluxo</param>
        /// <param name="codigoMercadoria"> Código da mercadoria do fluxo</param>
        /// <returns> Resultado json </returns>
        [Autorizar(Transacao = "DESPACHO", Acao = "Pesquisar")]
        public JsonResult VerificarFluxoComercial(string codigoFluxoComercial, string codigoOrigemFluxo, string codigoDestinoFluxo, string codigoMercadoria)
        {
            try
            {
                var fluxo = _carregamentoService.VerificarValidadeFluxoCarregamento(codigoFluxoComercial, codigoOrigemFluxo, codigoDestinoFluxo, codigoMercadoria);

                //if (fluxo != null)
                //{
                //    if(_bolarBoRepository.EmpresaBloqueadaFaturamentoManual(fluxo.Codigo.Substring(2)))
                //    {
                //        throw new Exception("Fluxo restrito ao faturamento automático");
                //    }
                //}

                var perfilAcessoUsuario = _painelExpedicaoService.ObterPerfilUsuarioPainelExpedicao(UsuarioAtual);
                var fluxoInterno = fluxo.Codigo == "99999";

                if (perfilAcessoUsuario != PainelExpedicaoPerfilAcessoEnum.Central && perfilAcessoUsuario != PainelExpedicaoPerfilAcessoEnum.CentralSuper)
                {
                    if (!fluxoInterno)
                    {
                        int? idEstacaoOrigem = (fluxo.Origem != null ? fluxo.Origem.Id : null);
                        var segmento = fluxo.Segmento;
                        var cnpjRaiz = (String.IsNullOrEmpty(fluxo.EmpresaPagadora.Cgc) ? String.Empty : fluxo.EmpresaRemetente.Cgc.Substring(0, 8));
                        var possuiBloqueioDespachoEstacao = _painelExpedicaoService.VerificarDespachoLocalBloqueio(idEstacaoOrigem, segmento, cnpjRaiz);
                        if (possuiBloqueioDespachoEstacao)
                        {
                            return Json(new
                            {
                                Erro = true,
                                Mensagem = "Este fluxo é de uma estação/segmento/cliente bloqueado para faturamento manual!"
                            });
                        }
                    }
                }

                var indFluxoCte = _carregamentoService.VerificarFluxoCte(fluxo);
                if (indFluxoCte)
                {
                    _carregamentoService.VerificarDadosFluxoCte(fluxo);
                }

                return Json(new
                                {
                                    Erro = false,
                                    IdFluxo = fluxo.Id
                                });
            }
            catch (Exception ex)
            {
                string mensagem = TratarException(ex);

                return Json(new
                {
                    Erro = true,
                    Mensagem = mensagem
                });
            }
        }


        /// <summary>
        /// Obtém empresas ferrovias para intercâmbio
        /// </summary>
        /// <returns> Resultado JSON </returns>
        public JsonResult ObterFerroviasIntercambio()
        {
            IList<EmpresaFerrovia> empresas = _carregamentoService.ObterFerrovias();

            return Json(new
            {
                totalCount = empresas.Count,
                Items = empresas.Select(g => new
                                                 {
                                                     g.Id,
                                                     DescricaoResumida = g.Sigla + " - " + g.DescricaoResumida
                                                 })
            });
        }

        /// <summary>
        /// Obtém os vagões que estão no patio
        /// </summary>
        /// <param name="idEstacaoMae">Id da estação mãe</param>
        /// <param name="indOrigemIntercambio">Indica se a origem do fluxo é intercambio ou nao</param>
        /// <param name="idFluxo">Id do fluxo comercial.</param>
        /// <param name="codigoVagao"> Código do vagão</param>
        /// <returns> Resultado JSON </returns>
        public JsonResult ObterVagoesPatio(int? idEstacaoMae, bool indOrigemIntercambio, int? idFluxo, string codigoVagao)
        {
            FluxoComercial fluxo = _carregamentoService.VerificarValidadeFluxoCarregamento(idFluxo.Value);
            bool fluxoBrado = _cteService.VerificarLiberacaoTravasCorrentista(fluxo);

            if (!idEstacaoMae.HasValue)
            {
                throw new TranslogicException("Parâmetros Inválidos.");
            }

            IList<VagaoCarregamentoDto> vagoes;
            if (indOrigemIntercambio)
            {
                vagoes = _carregamentoService.ObterVagaoCarregamentoIntercambio(codigoVagao);
            }
            else
            {
                vagoes = _carregamentoService.ObterVagoesCarregamento(idEstacaoMae.Value, idFluxo.Value, codigoVagao);
            }

            return Json(new
            {
                totalCount = vagoes.Count,
                Items = vagoes.Select(g => new
                {
                    g.IdVagao,
                    Serie = g.SerieVagao,
                    g.CodigoVagao,
                    g.Linha,
                    g.Sequencia,
                    g.IndCarregadoMesmoFluxo,
                    g.IndCarregadoFluxoNaoBrado,
                    FluxoBrado = fluxoBrado,
                    fluxo.TipoServicoCte
                })
            });
        }

        /// <summary>
        /// Obtém as linhas para recebimento de intercambio
        /// </summary>
        /// <param name="idAreaOperacional">Id da area operacional de origem do fluxo</param>
        /// <returns> Resultado JSON </returns>
        public JsonResult ObterLinhasIntercambio(int? idAreaOperacional)
        {
            if (!idAreaOperacional.HasValue)
            {
                throw new TranslogicException("Parâmetro de Origem Inválido.");
            }

            IList<ElementoVia> linhas = _carregamentoService.ObterLinhasRecebimentoIntercambio(idAreaOperacional.Value);

            return Json(new
            {
                totalCount = linhas.Count,
                Items = linhas.Select(g => new
                {
                    g.Id,
                    g.Codigo
                })
            });
        }

        /// <summary>
        /// Retorna os dados da empresa
        /// </summary>
        /// <param name="cnpj">Cnpj da empresa</param>
        /// <param name="razaoSocial">Razao social da empresa</param>
        /// <returns>Retorna os dados da empresa pesquisada</returns>
        public JsonResult PesquisarEmpresas(string cnpj, string razaoSocial)
        {
            IList<EmpresaCliente> empresas = _carregamentoService.ObterEmpresaPorCnpjRazaoSocial(cnpj, razaoSocial);

            return Json(new
            {
                totalCount = empresas.Count,
                Items = empresas.Select(g => new
                {
                    g.Id,
                    g.Sigla,
                    g.RazaoSocial,
                    g.InscricaoEstadual,
                    Cnpj = g.Cgc,
                    Uf = g.Estado == null ? string.Empty : g.Estado.Sigla
                })
            });
        }

        /// <summary>
        /// Obter dados do cnpj
        /// </summary>
        /// <param name="cnpj">cnpj da empresa</param>
        /// <param name="codigoFluxo">Código do fluxo</param>
        /// <param name="idAreaOperacional"> Id da AreaOperacional</param>
        /// <returns>Resultado Json</returns>
        public JsonResult ObterDadosCnpj(string cnpj, string codigoFluxo, int? idAreaOperacional)
        {
            bool erro = false;
            object dados = null;
            string mensagem = string.Empty;
            long aux;
            bool exterior = false;

            try
            {
                if (long.TryParse(cnpj.Trim(), out aux))
                {
                    // Caso seja 0, então é fluxo internacional
                    if (aux == 0)
                    {
                        erro = false;
                        mensagem = "CNPJ Destino internacional";
                        exterior = true;
                    }
                    else
                    {
                        // tenta recuperar os dados da empresa
                        IEmpresa empresa = _carregamentoService.ObterEmpresaPorCnpj(cnpj, codigoFluxo, idAreaOperacional);

                        if (empresa == null)
                        {
                            erro = true;
                            mensagem = "Não foi encontrada nenhuma empresa para este CNPJ.";
                        }
                        else
                        {
                            dados = new
                            {
                                Nome = empresa.RazaoSocial,
                                InscricaoEstadual = empresa.InscricaoEstadual,
                                Sigla = empresa.Sigla,
                                Uf = empresa.Estado.Sigla
                            };
                        }
                    }
                }
            }
            catch (Exception e)
            {
                mensagem = TratarException(e);
                erro = true;
            }

            return Json(new
            {
                Erro = erro,
                Mensagem = mensagem,
                Dados = dados,
                Exterior = exterior
            });
        }

        /// <summary>
        /// Verifica se, após ocorrer um erro ao carregar a NFe, a mesma deve ser liberada para digitação.
        /// </summary>
        /// <param name="codigoErro">código do erro ao carregar</param>
        /// <returns>valor boleano</returns>
        public JsonResult PodeLiberarDigitacaoNfe(string codigoErro)
        {
            return Json(_nfeService.PodeLiberarDigitacaoNfe(codigoErro));
        }

        /// <summary>
        /// Obtém os dados da NFe
        /// </summary>
        /// <param name="chaveNfe"> Chave da Nfe</param>
        /// <param name="codigoFluxo"> Codigo do fluxo comercial</param>
        /// <returns> Resultado JSON </returns>
        public JsonResult ObterDadosNfe(string chaveNfe, string codigoFluxo)
        {
            INotaFiscalEletronica nfe;
            string mensagem = string.Empty;
            string stackTrace = string.Empty;
            object dados = null;
            bool erro = true;
            bool indLiberaDigitacao = false;
            string codigoStatusRetorno;
            StatusNfeDto statusNfe;
            // TimeSpan ts = new TimeSpan(0, 0, 10);
            // Thread.Sleep(ts);
            // Recupera o fluxo Comercial
            FluxoComercial fluxoComercial = _carregamentoService.BuscarDadosFluxoComercial(codigoFluxo.Substring(2));

            // Recupera a sigla do estado da emrpesa remetente
            var estacaoMae = fluxoComercial.OrigemIntercambio ?? fluxoComercial.Origem;
            string siglaEstado = estacaoMae.Municipio.Estado.Sigla;

            // Recupera a serie Numero Empresa UF pelo estado
            CteSerieNumeroEmpresaUf cteSerieNumeroEmpresa = _nfeService.ObterSerieEmpUfPorEstado(siglaEstado);

            // Caso o indicador esteja true, então Efetua a consulta no SimConsultas
            if (cteSerieNumeroEmpresa.IndConsultaSimConsulta)
            {
                statusNfe = _nfeService.ObterDadosNfe(chaveNfe, TelaProcessamentoNfe.Carregamento, false, false);
            }
            else
            {
                // Se não, verifica se a nota esta no banco apenas
                statusNfe = _nfeService.ObterStatusNfeBancoDados(chaveNfe, TelaProcessamentoNfe.Carregamento, false);

                if (statusNfe == null)
                {
                    statusNfe = _nfeService.InserirNfePooling(chaveNfe, "1131");
                }
            }

            codigoStatusRetorno = statusNfe.CodigoStatusRetorno;

            if (statusNfe.ObtidaComSucesso)
            {
                if (_carregamentoService.VerificarTravaSaldoNfe(fluxoComercial, statusNfe))
                {
                    statusNfe = _nfeService.VerificarExisteSaldoNfe(statusNfe, fluxoComercial);
                }
            }

            if (!statusNfe.ObtidaComSucesso)
            {
                erro = true;
                mensagem = statusNfe.MensagemErro;
                stackTrace = statusNfe.StackTrace;
                indLiberaDigitacao = statusNfe.IndLiberaDigitacao;
            }
            else
            {
                nfe = statusNfe.NotaFiscalEletronica;
                try
                {
                    IEmpresa empresaEmitente = _carregamentoService.ObterEmpresaPorCnpj(nfe.CnpjEmitente, codigoFluxo, 0);
                    IEmpresa empresaDestinatario = _carregamentoService.ObterEmpresaPorCnpj(nfe.CnpjDestinatario, codigoFluxo, 0);

                    if (empresaEmitente == null || empresaDestinatario == null)
                    {
                        erro = true;
                        mensagem = string.Empty;
                        stackTrace = string.Empty;
                        indLiberaDigitacao = true;
                    }
                    else
                    {
                        IList<NfeProdutoReadonly> produtos = _nfeService.ObterProdutosNfe(nfe);
                        string cfopProduto = String.Join(";", produtos.Select(c => c.Cfop).Distinct().ToArray());

                        double? pesoTotal = statusNfe.Peso > 0 ? Tools.TruncateValue(statusNfe.Peso, TipoTruncateValueEnum.UnidadeMilhar) : (double?)null;
                        if (fluxoComercial.Segmento.Equals("BRADO"))
                        {
                            pesoTotal = statusNfe.PesoBruto;
                        }
                        double? volume = statusNfe.Volume > 0 ? statusNfe.Volume : null;

                        double? pesoUtilizado = statusNfe.PesoUtilizado > 0 ? Tools.TruncateValue(statusNfe.PesoUtilizado, TipoTruncateValueEnum.UnidadeMilhar) : (double?)null;
                        double? volumeUtilizado = statusNfe.VolumeUtilizado > 0 ? statusNfe.VolumeUtilizado : null;

                        bool fluxoDeConteinerVazio = _carregamentoService.VerificarFluxoDeConteinerVazio(fluxoComercial);

                        erro = false;
                        dados = new
                        {
                            nfe.SerieNotaFiscal,
                            nfe.NumeroNotaFiscal,
                            Peso = pesoTotal,
                            Volume = volume.HasValue ? Tools.TruncateValue(volume.Value, TipoTruncateValueEnum.UnidadeMilhar) : volume,
                            PesoUtilizado = pesoUtilizado,
                            VolumeUtilizado = volumeUtilizado.HasValue ? Tools.TruncateValue(volumeUtilizado.Value, TipoTruncateValueEnum.UnidadeMilhar) : volumeUtilizado,
                            nfe.Valor,
                            Remetente = nfe.RazaoSocialEmitente,
                            Destinatario = nfe.RazaoSocialDestinatario,
                            SiglaRemetente = empresaEmitente.Sigla,
                            SiglaDestinatario = empresaDestinatario.Sigla,
                            CnpjRemetente = nfe.CnpjEmitente,
                            nfe.CnpjDestinatario,
                            DataNotaFiscal = nfe.DataEmissao.ToString("dd/MM/yyyy"),
                            nfe.InscricaoEstadualDestinatario,
                            InscricaoEstadualRemetente = nfe.InscricaoEstadualEmitente,
                            nfe.UfDestinatario,
                            UfRemetente = nfe.UfEmitente,
                            Cfop = cfopProduto,
                            TipoNfe = nfe.TipoNotaFiscal != null ? (int)nfe.TipoNotaFiscal : 99,
                            ConteinerVazio = fluxoDeConteinerVazio
                        };
                    }
                }
                catch (Exception e)
                {
                    mensagem = TratarException(e);
                    stackTrace = e.StackTrace;
                    erro = true;
                    indLiberaDigitacao = true;
                }
            }

            return Json(new
            {
                CodigoStatusRetorno = codigoStatusRetorno,
                Erro = erro,
                Mensagem = mensagem,
                DadosNfe = dados,
                StackTrace = stackTrace,
                IndLiberaDigitacao = indLiberaDigitacao
            });
        }

        /// <summary>
        /// Verifica se o fluxo é de conteiner vazio
        /// </summary>
        /// <param name="codigoFluxoComercial">Código do fluxo comercial</param>
        /// <returns>Resultado Json</returns>
        public JsonResult VerificarFluxoDeConteinerVazio(string codigoFluxoComercial)
        {
            bool erro = false;
            string mensagem = string.Empty;

            try
            {
                FluxoComercial fluxoComercial = _carregamentoService.BuscarDadosFluxoComercial(codigoFluxoComercial.Substring(2));
                bool fluxoDeConteinerVazio = _carregamentoService.VerificarFluxoDeConteinerVazio(fluxoComercial);

                return Json(new
                {
                    Erro = false,
                    FluxoConteinerVazio = fluxoDeConteinerVazio
                });
            }
            catch (Exception e)
            {
                erro = true;
                mensagem = e.Message;
            }

            return Json(new
            {
                Erro = erro,
                Mensagem = mensagem
            });
        }

        /// <summary>
        /// Obtem os dados do vagão qdo nao localizado na lista
        /// </summary>
        /// <param name="codigoVagao"> The codigo vagao. </param>
        /// <returns> Resultado Json </returns>
        public JsonResult ObterDadosVagao(string codigoVagao)
        {
            string mensagem = string.Empty;
            bool erro = false;
            object dados = null;
            try
            {
                DadosVagaoForaCarregamentoDto dadosVagao = _carregamentoService.ObterDadosVagao(codigoVagao);
                string situacao = dadosVagao.Situacao;
                string prefixoTrem = dadosVagao.PrefixoTrem;
                string patio = dadosVagao.Patio;
                bool intercambio = dadosVagao.Intercambio;

                string localVagao = string.Empty;
                if (intercambio)
                {
                    localVagao = "Intercâmbio";
                }
                else
                {
                    localVagao = string.IsNullOrEmpty(prefixoTrem) ? string.Format("Pátio({0})", patio) : string.Format("Trem({0})", prefixoTrem);
                }

                dados = new
                            {
                                Situacao = situacao,
                                LocalVagao = localVagao
                            };
            }
            catch (Exception e)
            {
                mensagem = TratarException(e);
                erro = true;
            }

            return Json(new
            {
                Erro = erro,
                Mensagem = mensagem,
                DadosVagao = dados
            });
        }

        /// <summary>
        /// Grava os dados da nota quando é obtida manualmente.
        /// </summary>
        /// <param name="chave">Chave da nota</param>
        /// <param name="conteiner">Numero do Conteiner</param>
        /// <param name="serie">Serie da Nfe</param>
        /// <param name="numero">Numero da Nfe</param>
        /// <param name="valorNota">Valor da Nota</param>
        /// <param name="valorTotal">Valor Total da Nota</param>
        /// <param name="remetente">Remetente da nota</param>
        /// <param name="destinatario">Destinatario da nota</param>
        /// <param name="cnpjRemetente">Cnpj do remetente da nota</param>
        /// <param name="cnpjDestinatario">Cnpj do Destinatario da nota</param>
        /// <param name="dataNotaFiscal">Data da Nota</param>
        /// <param name="siglaRemetente">Sigla do Remetente</param>
        /// <param name="siglaDestinatario">Sigla do Destinatario</param>
        /// <param name="estadoRemetente"> UF Remetente da Nota</param>
        /// <param name="estadoDestinatario"> UF Destinatario da nota</param>
        /// <param name="inscricaoEstadualRemetente">IE do Remetente</param>
        /// <param name="inscricaoEstadualDestinatario">IE do Destinatario</param>
        /// <param name="volumeNotaFiscal">Volume da Nota</param>
        /// <param name="pesoTotal"> Peso Total</param>
        /// <param name="pesoRateio">Peso Rateio</param>
        /// <param name="tif">tif da nota fiscal</param>
        /// <param name="placaCavalo">Placa Cavalo da nota</param>
        /// <param name="placaCarreta">Placa Carreta da nota</param>
        /// <returns> Resultado Json </returns>
        public JsonResult ProcessarNfeManual(string chave, string conteiner, string serie, string numero, string valorNota, string valorTotal, string remetente, string destinatario, string cnpjRemetente, string cnpjDestinatario, string dataNotaFiscal, string siglaRemetente, string siglaDestinatario, string estadoRemetente, string estadoDestinatario, string inscricaoEstadualRemetente, string inscricaoEstadualDestinatario, string volumeNotaFiscal, string pesoTotal, string pesoRateio, string tif, string placaCavalo, string placaCarreta)
        {
            string mensagem = string.Empty;
            bool erro = false;
            try
            {
                _carregamentoService.ProcessarNfeManual(chave, conteiner, serie, numero, valorNota, valorTotal, remetente, destinatario, cnpjRemetente, cnpjDestinatario, dataNotaFiscal, siglaRemetente, siglaDestinatario, estadoRemetente, estadoDestinatario, inscricaoEstadualRemetente, inscricaoEstadualDestinatario, volumeNotaFiscal, pesoTotal, pesoRateio, tif, placaCavalo, placaCarreta, UsuarioAtual);
            }
            catch (Exception e)
            {
                mensagem = TratarException(e);
                erro = true;
            }

            return Json(new
            {
                Erro = erro,
                Mensagem = mensagem
            });
        }

        /// <summary>
        /// Obter dados do conteiner
        /// </summary>
        /// <param name="codigoConteiner">Código do conteiner</param>
        /// <param name="codAoOrigem">Código da AO origem do Fluxo Comercial</param> 
        /// <returns>Resultado Json</returns>
        public JsonResult ObterDadosConteiner(string codigoConteiner, int codAoOrigem)
        {
            bool erro = false;
            string mensagem = string.Empty;
            bool voarConteiner = false;
            int? idConteiner = 0;

            try
            {
                Conteiner conteiner = _carregamentoService.VerificarConteinerPorCodigo(codigoConteiner, codAoOrigem);
                if (conteiner == null)
                {
                    erro = true;
                    mensagem = "Não foi encontrado nenhum conteiner com este código.";
                }
                else
                {
                    idConteiner = conteiner.Id;

                    ConteinerLocalVigente localConteiner = _carregamentoService.VerificarLocalConteiner(conteiner, codAoOrigem);

                    if (localConteiner != null)
                    {
                        voarConteiner = true;
                        mensagem = string.Format("O conteiner {0} se encontra em local diferente da origem do Fluxo comercial.\nLocal do Conteiner : {1}.", conteiner.Numero, localConteiner.EstacaoOrigem.Codigo);
                    }






                }

            }
            catch (Exception e)
            {
                mensagem = TratarException(e);
                erro = true;
            }

            return Json(new
            {
                Voar = voarConteiner,
                Erro = erro,
                idConteiner,
                Mensagem = mensagem
            });
        }

        /// <summary>
        /// Voa o vagão para a estação
        /// </summary>
        /// <param name="idConteiner">Número do conteiner</param>
        /// <param name="codAoOrigem"> Id da área operacional mãe do destino do contêiner</param>
        /// <returns>Resultado Json</returns>
        public JsonResult VoarConteiner(string idConteiner, int codAoOrigem)
        {
            bool erro = false;
            string mensagem = string.Empty;

            try
            {
                _carregamentoService.VoarConteiner(idConteiner, codAoOrigem, UsuarioAtual.Codigo);
            }
            catch (Exception e)
            {
                mensagem = TratarException(e);
                erro = true;
            }

            return Json(new
            {
                Erro = erro,
                Mensagem = mensagem
            });
        }

        /// <summary>
        /// Salva o log de nfe
        /// </summary>
        /// <param name="log"> Log do carregamento quando baixa a nfe. </param>
        /// <returns> Resultado JSON </returns>
        [ValidateInput(false)]
        [JsonFilter(Param = "log", JsonDataType = typeof(LogCarregamentoNfe))]
        public JsonResult SalvarLogNotaFiscalEletronica(LogCarregamentoNfe log)
        {
            bool erro = false;
            string mensagem = string.Empty;
            try
            {
                _carregamentoService.SalvarLogCarregamentoNfe(log, UsuarioAtual);
            }
            catch (Exception e)
            {
                mensagem = e.Message;
                erro = true;
            }

            return Json(new
            {
                Erro = erro,
                Mensagem = mensagem
            });
        }

        /// <summary>
        /// Obter dados do conteiner
        /// </summary>
        /// <param name="pesoLiquido">Peso Líquido</param>
        /// <param name="pesoBruto"> Peso Bruto </param>
        /// <param name="chaveNfe"> Chave da Nfe</param>
        /// <param name="indVolume">Indicador se é volume</param>
        /// <returns>Resultado Json</returns>
        public JsonResult SalvarPesosNfe(string pesoLiquido, string pesoBruto, string chaveNfe, bool indVolume)
        {
            bool erro = false;
            string mensagem = string.Empty;
            double pesoLiquidoNumerico = double.Parse(pesoLiquido, CultureInfo.GetCultureInfo("en-US"));
            double pesoBrutoNumerico = double.Parse(pesoBruto, CultureInfo.GetCultureInfo("en-US"));

            try
            {
                pesoBrutoNumerico = pesoBrutoNumerico * 1000;
                pesoLiquidoNumerico = pesoLiquidoNumerico * 1000;

                _nfeService.AtualizarPesosNfe(pesoLiquidoNumerico, pesoBrutoNumerico, chaveNfe, indVolume);
            }
            catch (Exception e)
            {
                mensagem = TratarException(e);

                erro = true;
            }

            return Json(new
            {
                Erro = erro,
                Mensagem = mensagem
            });
        }

        /// <summary>
        /// Obtém o peso pelo volume informado
        /// </summary>
        /// <param name="codigoMercadoria">Código da mercadoria</param>
        /// <param name="volume">Volume informado no vagão</param>
        /// <returns> Resultado JSON </returns>
        public JsonResult ObterPesoPorVolume(string codigoMercadoria, string volume)
        {
            bool erro = false;
            string mensagem = string.Empty;
            double peso = 0;

            double volume2 = double.Parse(volume, CultureInfo.InvariantCulture);

            try
            {
                if (string.IsNullOrEmpty(codigoMercadoria))
                {
                    throw new TranslogicException("Parâmetro de Código de Mercadoria Inválido.");
                }

                if (volume2 <= 0)
                {
                    throw new TranslogicException("Volume Inválido.");
                }

                double pesoPorVolume = _carregamentoService.CalcularPesoPorVolume(codigoMercadoria, volume2);

                if (pesoPorVolume <= 0)
                {
                    erro = true;
                    mensagem = "Não foi encontrado nenhum conteiner com este código.";
                }
                else
                {
                    peso = pesoPorVolume;
                }
            }
            catch (Exception e)
            {
                mensagem = TratarException(e);
                erro = true;
            }

            return Json(new
            {
                Erro = erro,
                Mensagem = mensagem,
                Peso = peso
            });
        }

        private string TratarException(Exception e)
        {
            string mensagem = string.Empty;
            Regex regex = new Regex("#.*#");

            mensagem = regex.Match(e.Message).Success ? regex.Match(e.Message).Value : string.Empty;

            if (string.IsNullOrEmpty(mensagem))
            {
                return e.Message;
            }

            return mensagem.Substring(1, mensagem.Length - 2);
        }

        private string TratarRemoverVagao(string mensagem, out bool removerVagao, out int? idVagaoRemover, out string codigoVagaoRemover)
        {
            if (!mensagem.Contains("$"))
            {
                removerVagao = false;
                idVagaoRemover = null;
                codigoVagaoRemover = string.Empty;
                return mensagem;
            }

            int idxIni = mensagem.IndexOf("$") + 1;
            int idxFim = mensagem.IndexOf("|");

            removerVagao = true;
            string idVagao = mensagem.Substring(idxIni, idxFim - idxIni);
            int idVagaoR;
            if (int.TryParse(idVagao, out idVagaoR))
            {
                idVagaoRemover = idVagaoR;
            }
            else
            {
                idVagaoRemover = null;
            }

            codigoVagaoRemover = mensagem.Substring(idxFim + 1).Replace("$", string.Empty);

            return mensagem.Replace(string.Format("${0}|{1}$", idVagaoRemover, codigoVagaoRemover), string.Empty);
        }
    }
}
