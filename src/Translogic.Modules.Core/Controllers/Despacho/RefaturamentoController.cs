﻿namespace Translogic.Modules.Core.Controllers.Despacho
{
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Web;
	using System.Web.Mvc;
	using ALL.Core.Dominio;
	using Translogic.Core.Infrastructure.Web;
	using Translogic.Core.Infrastructure.Web.Filters;
	using Translogic.Modules.Core.Domain.Model.Dto;
	using Translogic.Modules.Core.Domain.Services.FluxosComerciais;
	using Translogic.Modules.Core.Infrastructure;

	/// <summary>
	/// Controller de Refaturamento
	/// </summary>
	public class RefaturamentoController : BaseSecureModuleController
	{
		private readonly RefaturamentoService _refaturamentoService;

		/// <summary>
		/// Controller do refaturamento
		/// </summary>
		/// <param name="refaturamentoService">Serviço de Refaturamento Injetado.</param>
		public RefaturamentoController(RefaturamentoService refaturamentoService)
		{
			_refaturamentoService = refaturamentoService;
		}

		/// <summary>
		/// Página inicial
		/// </summary>
		/// <returns>Retorna a View</returns>
		[Autorizar(Transacao = "CTEREFAT")]
		public ActionResult Index()
		{
			return View();
		}

		/// <summary>
		/// Obtém as linhas para recebimento de intercambio
		/// </summary>
		/// <param name="prefixo"> prefixo do trem</param>
		/// <param name="ordemServico"> ordem de Serviço</param>
		/// <returns> Resultado JSON </returns>
		[Autorizar(Transacao = "CTEREFAT", Acao = "Pesquisar")]
		public JsonResult ObterRefaturamentos(string prefixo, string ordemServico)
		{
			string codigoRetorno = string.Empty;
			string mensagem = string.Empty;

			IList<RefaturamentoDto> listaRefaturamentos = _refaturamentoService.ObterCteRefaturamento(prefixo, ordemServico);
			IList<RefaturamentoDto> linhas = _refaturamentoService.VerificarDestinosPerm(listaRefaturamentos);

			if (linhas.Count < listaRefaturamentos.Count)
			{
				codigoRetorno = "1";
				mensagem = "Não é permitido refaturamento para o Destino deste trem.";
			}

			return Json(new
			{
				codigoRetorno,
				mensagem,
				totalCount = linhas.Count,
				Items = linhas.Select(g => new
				{
					g.Bitola,
					g.IdTrem,
					g.IdComposicao,
					g.IdVagao,
					g.Veiculo,
					g.CargaBruta,
					g.CodFluxoComercial,
					g.Comprimento,
					g.CondUso,
					g.DescClasse,
					g.Destino,
					g.EmpresaCsg,
					g.EmpresaCsgDesc,
					g.EmpresaProprietaria,
					g.EmpresaRemetente,
					g.Frota,
					g.IdDespacho,
					g.IdFluxoComercial,
					g.IndFreio,
					g.Mercadoria,
					g.MercDescricao,
					g.Nota,
					g.Origem,
					g.Pedido,
					g.SequenciaVagao,
					g.SerieVagao,
					g.Tu
				})
			});
		}

		/// <summary>
		/// Efetua o Refaturamento dos vagões selecionados.
		/// </summary>
		/// <param name="listaVagoes"> Dto de lista de vagoes</param>
		/// <returns> Resultado JSON </returns>
		[JsonFilter(Param = "listaVagoes", JsonDataType = typeof(RefatVagoesDto))]
		[Autorizar(Transacao = "CTEREFAT", Acao = "Salvar")]
		public JsonResult SalvarRefaturamentos(RefatVagoesDto listaVagoes)
		{
			try
			{
				listaVagoes.Usuario = UsuarioAtual.Codigo;
				_refaturamentoService.SalvarCteRefaturamento(listaVagoes);

				return Json(new { success = true, Message = "SUCESSO" });
			}
			catch (Exception e)
			{
				return Json(new { success = false, Message = "Erro ao salvar os registros: " + e.Message });
			}
		}

		/// <summary>
		/// Recuperar saldo disponível.
		/// </summary>
		/// <param name="contrato">Número do contrato.</param>
		/// <returns> Resultado JSON </returns>
		[Autorizar(Transacao = "CTEREFAT", Acao = "Pesquisar")]
		public JsonResult ObterSaldoContratoDisponivel(string contrato)
		{
			try
			{
				decimal saldoContratoDisponivel = _refaturamentoService.ObterSaldoContratoDisponivel(contrato);

				return Json(new
						{
							success = true,
							Message = string.Empty,
							saldoDisponivel = saldoContratoDisponivel
						});
			}
			catch (Exception e)
			{
				return Json(new
				{
					success = false,
					Message = "Erro ao obter o saldo disponível do contrato: " + e.Message
				});
			}
		}
	}
}
