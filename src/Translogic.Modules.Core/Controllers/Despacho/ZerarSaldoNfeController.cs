﻿namespace Translogic.Modules.Core.Controllers.Despacho
{
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Web;
	using System.Web.Mvc;
	using ALL.Core.Util;
	using Domain.Model.FluxosComerciais.Nfes;
	using Domain.Services.FluxosComerciais;
	using Infrastructure;
	using Translogic.Core.Infrastructure.Web.Filters;
	using Translogic.Modules.Core.Domain.Model.Diversos;
	using Translogic.Modules.Core.Domain.Model.Dto;
using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Simconsultas;

	/// <summary>
    /// Classe ManutencaoNfeController
	/// </summary>
	public class ZerarSaldoNfeController : BaseSecureModuleController
	{
		private readonly NfeService _nfeService;
        private readonly BaixarNfeService _baixarNfeService;

		/// <summary>
        /// Initializes a new instance of the <see cref="ZerarSaldoNfeController"/> class.
		/// </summary>
		/// <param name="nfeService"> Serviço de Nfe injetado</param>
        /// <param name="baixarNfeService"> Serviço de baixarNfeService injetado</param>
        public ZerarSaldoNfeController(NfeService nfeService, BaixarNfeService baixarNfeService)
		{
			_nfeService = nfeService;
		    _baixarNfeService = baixarNfeService;
		}

		/// <summary>
		/// Index para consulta de nfe
		/// </summary>
		/// <returns>Retorna a View Default</returns>
        [Autorizar(Transacao = "ZERARSALDONFE")]
		public ActionResult Index()
		{
			return View();
		}

        /// <summary>
        ///     Ação do controller para apresentação da View Autorizacao
        /// </summary>
        /// <returns>Action Result</returns>
        public ActionResult Autorizacao()
        {
            return this.View();
        }

		/// <summary>
        /// Zerar a nfe da base
		/// </summary>
        /// <param name="chaves"> Chave Nfe a ser processada</param>
		/// <returns> String do status do processamento.  </returns>
        [Autorizar(Transacao = "ZERARSALDONFE")]
        public JsonResult Zerar(string chaves, string autorizadopor, string justificativa)
		{
            var chavesParseadas = _baixarNfeService.RecuperarChaves(chaves).ToArray();
            try
            {
                _nfeService.ZerarSaldo(UsuarioAtual, autorizadopor, justificativa, chavesParseadas);
                _baixarNfeService.EnviarEmail(chavesParseadas, UsuarioAtual, "OK: NFes que tiveram saldo zerado");
                return Json(chavesParseadas.Select(e => new { Chave = e, Mensagem = string.Empty, Status = "OK" }).ToList());
            }
            catch (Exception ex)
            {
                _baixarNfeService.EnviarEmail(chavesParseadas, UsuarioAtual, string.Format("ERRO: NFes que tiveram saldo zerado {0}", ex.Message));
                return Json(chavesParseadas.Select(e => new { Chave = e, Mensagem = ex.Message, Status = "ERRO" }).ToList());
            }
		}
	}
}
