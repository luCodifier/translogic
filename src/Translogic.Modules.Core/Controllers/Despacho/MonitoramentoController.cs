﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using ALL.Core.Dominio;
using Translogic.Core.Infrastructure.Web;
using Translogic.Modules.Core.Domain.Model.Bolar.Repositories;
using Translogic.Modules.Core.Domain.Model.Dto;
using Translogic.Modules.Core.Domain.Model.Via.Repositories;
using Translogic.Core.Infrastructure.Web.ContentResults;

namespace Translogic.Modules.Core.Controllers.Despacho
{
    public class MonitoramentoController : BaseSecureModuleController
    {
        private readonly IBolarBoImpCfgRepository _bolarBoImpCfgRepository;
        private readonly IAreaOperacionalRepository _areaOperacionalRepository;
        private readonly IBolarBoRepository _bolarBoRepository;
        private readonly IBoBolarControleRepository _boBolarControleRepository;

        public MonitoramentoController(IBolarBoImpCfgRepository bolarBoImpCfgRepository, IAreaOperacionalRepository areaOperacionalRepository, IBolarBoRepository bolarBoRepository, IBoBolarControleRepository boBolarControleRepository)
        {
            _bolarBoImpCfgRepository = bolarBoImpCfgRepository;
            _areaOperacionalRepository = areaOperacionalRepository;
            _bolarBoRepository = bolarBoRepository;
            _boBolarControleRepository = boBolarControleRepository;
        }

        // GET: /Monitoramento/
        //[Autorizar(Transacao = "DPACHOMONITERROR")]
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult ObterStatusProcessamento()
        {
            try
            {
                return Json(new
                {
                    Success = true,
                    Items = ObterListaStatusProcessamento()
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ErrorMessage = ex.Message
                });
            }
        }

        public ActionResult ObterClientes()
        {
            var clientes = _bolarBoImpCfgRepository.ObterTodasComFaturamentoAutomatico();

            return Json(clientes.OrderBy(x => x.DescricaoEmpresa).Select(t => new { t.CodigoEmpresa, DescricaoEmpresa = t.DescricaoEmpresa.ToUpper() }));
        }

        public ActionResult ObterLocal(string codigoEmpresa)
        {
            var lista = new List<AreaOpDto>();

            if (!string.IsNullOrEmpty(codigoEmpresa))
            {
                lista = _areaOperacionalRepository.ObterPorCliente(codigoEmpresa);
            }

            return Json(lista.Select(t => new { t.IdTerminal, t.CodTerminal })); ;
        }

        //[Autorizar(Transacao = "DPACHOMONITERROR", Acao = "Pesquisar")]
        public ActionResult Pesquisar(DetalhesPaginacaoWeb pagination, DateTime dtInicial, DateTime dtFinal, string vagao, string processado, string cliente, string local, string fluxo)
        {
            try
            {
                var resultados = _bolarBoRepository.Pesquisar(pagination, dtInicial, dtFinal, vagao, processado, cliente, local, fluxo);

                var retorno = new
                {
                    resultados.Total,
                    Items = resultados.Items.Select(item => new
                    {
                        item.Id,
                        item.Serie,
                        item.Vagao,
                        item.Fluxo,
                        item.Md,
                        item.StatusProcessamento,
                        item.NumeroTentativas,
                        ProximaExecucao = string.Format("{0:dd/MM/yyyy HH:mm:ss}", item.ProximaExecucao),
                        item.Observacao,
                        Data = string.Format("{0:dd/MM/yyyy}", item.Data),
                        item.Cliente,
                        item.Aprovado1380,
                        item.Local,
                        item.LocalAtual,
                        item.Situacao,
                        item.Lotacao,
                        item.Localizacao,
                        item.CondicaoUso,
                        item.Volume,
                        PesoDespacho = string.Format("{0:N2}", item.PesoDespacho),
                        PesoTotalVagao = string.Format("{0:N2}", item.PesoTotalVagao),
                        item.PermiteManual
                    })
                };

                return Json(retorno);
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, ex.Message });
            }
        }

        //[Autorizar(Transacao = "DPACHOMONITERROR", Acao = "LiberarFatManual")]
        public ActionResult LiberarFaturamentoManual(string[] itens)
        {
            try
            {
                var boBolarControleList = _boBolarControleRepository.ObterTodos(itens.Select(int.Parse).ToArray());

                foreach (var item in boBolarControleList)
                {
                    item.AlterarPermissao();
                    _boBolarControleRepository.Atualizar(item);
                }

                return Json(new { Success = true, Message = "Dados atualizados com sucesso" });
            }
            catch (Exception ex)
            {

                return Json(new { Success = false, ex.Message });
            }
        }

        public ActionResult Exportar(string dtInicial, string dtFinal, string vagao, string processado, string cliente, string local, string fluxo)
        {
            var resultados = _bolarBoRepository.Exportar(DateTime.Parse(dtInicial), DateTime.Parse(dtFinal), vagao, processado, cliente, local, fluxo);

            var dic = new Dictionary<string, Func<ProcessamentoFaturamentoAutomaticoDto, string>>();

            dic["Tela 363"] = c => c.PermiteManual ?? string.Empty;
            dic["Série"] = c => c.Serie ?? string.Empty;
            dic["Vagão"] = c => c.Vagao ?? string.Empty;
            dic["Fluxo"] = c => c.Fluxo.ToString();
            dic["MD-Múltiplos despachos"] = c => c.Md ?? string.Empty;
            dic["Status Processamento"] = c => ObterDescricaoStatusProcessamento(c.StatusProcessamento);
            dic["Tentativas"] = c => c.NumeroTentativas.HasValue ? c.NumeroTentativas.Value.ToString() : string.Empty;
            dic["Data próxima execução"] = c => string.Format("{0:dd/MM/yyyy HH:mm:ss}", c.ProximaExecucao);
            dic["Obs"] = c => c.Observacao ?? string.Empty;
            dic["Data"] = c => c.Data.ToString();
            dic["Cliente 363"] = c => c.Cliente ?? string.Empty;
            dic["Aprovado"] = c => c.Aprovado1380 ?? string.Empty;
            dic["Origem"] = c => c.Local ?? string.Empty;
            dic["Local atual"] = c => c.LocalAtual ?? string.Empty;
            dic["Situação"] = c => c.Situacao ?? string.Empty;
            dic["Lotação"] = c => c.Lotacao ?? string.Empty;
            dic["Localização"] = c => c.Localizacao ?? string.Empty;
            dic["Condição de uso"] = c => c.CondicaoUso ?? string.Empty;
            dic["Volume"] = c => string.Format("{0:N2}", c.Volume);
            dic["Peso"] = c => string.Format("{0:N2}", c.PesoDespacho);
            dic["Peso total vagão"] = c => string.Format("{0:N2}", c.PesoTotalVagao);

            return this.Excel(string.Format("Monitoramento{0}_{1}.xls", DateTime.Now.ToString("ddMMyyyy"), DateTime.Now.ToString("HHmmss")), dic, resultados);
        }

        private static string ObterDescricaoStatusProcessamento(decimal id)
        {
            var item = ObterListaStatusProcessamento().FirstOrDefault(x => x.Id == id);
            if (item != null)
                return item.Descricao;

            return string.Empty;
        }

        private static IList<StatusProcessamentoDto> ObterListaStatusProcessamento()
        {
            return new List<StatusProcessamentoDto>
                       {
                           new StatusProcessamentoDto { Id = 1, Descricao = "Não Processado" }, 
                           new StatusProcessamentoDto { Id = 2, Descricao = "Processado" }, 
                           new StatusProcessamentoDto { Id = 3, Descricao = "Pendente Processamento" },
                           new StatusProcessamentoDto { Id = 4, Descricao = "Fila Processamento" }
                       };
        }
    }
}
