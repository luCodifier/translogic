﻿namespace Translogic.Modules.Core.Controllers.Despacho
{
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Web;
	using System.Web.Mvc;
	using ALL.Core.Util;
	using Domain.Model.FluxosComerciais.Nfes;
	using Domain.Services.FluxosComerciais;
	using Infrastructure;

	using Translogic.Core.Infrastructure.Web;
	using Translogic.Core.Infrastructure.Web.Filters;
	using Translogic.Modules.Core.Domain.Model.Acesso;
	using Translogic.Modules.Core.Domain.Model.Diversos;
	using Translogic.Modules.Core.Domain.Model.Dto;
using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Simconsultas;

	/// <summary>
    /// Classe ManutencaoNfeController
	/// </summary>
	public class NfeConfiguracaoEmpresaController : BaseSecureModuleController
	{
		private readonly NfeConfiguracaoEmpresaService _nfeConfiguracaoEmpresaService;

		/// <summary>
		/// Initializes a new instance of the <see cref="ConsultasNfeController"/> class.
		/// </summary>
        /// <param name="nfeConfiguracaoEmpresaService">Serviço de Nfe injetado</param>
        public NfeConfiguracaoEmpresaController(NfeConfiguracaoEmpresaService nfeConfiguracaoEmpresaService)
		{
            _nfeConfiguracaoEmpresaService = nfeConfiguracaoEmpresaService;
		}

		/// <summary>
		/// Index para consulta 
		/// </summary>
		/// <returns>Retorna a View Default</returns>
        // [Autorizar(Transacao = "NFECONFEMP")]
		public ActionResult Index()
		{
			return View();
		}

        /// <summary>
        /// salva a entidade
        /// </summary>
        /// <param name="id">id da entidade</param>
        /// <param name="cnpj">cnpj da entidade</param>
        /// <param name="priorizarVolume">priorizarVolume da entidade</param>
        /// <param name="priorizarProdutos">priorizarProdutos da entidade</param>
        /// <returns>Action Result</returns>
        public ActionResult Salvar(int id, string cnpj, bool priorizarVolume, bool priorizarProdutos)
		{
            try
            {
                _nfeConfiguracaoEmpresaService.Salvar(id, cnpj, UsuarioAtual, priorizarVolume, priorizarProdutos);
                return Json(new { success = true });
            }
            catch (Exception e)
            {
                return Json(new { success = false, e.Message, e.StackTrace });
            }
		}

        /// <summary>
        /// pesquisa paginada
        /// </summary>
        /// <param name="pagination">detalhes da paginacao</param>
        /// <param name="value">valor para pesquisa</param>
        /// <returns>Action Result</returns>
        public ActionResult Pesquisar(DetalhesPaginacaoWeb pagination, string value)
        {
            var dados = _nfeConfiguracaoEmpresaService.ListarPorTrechoDeCnpj(pagination, value);

            return Json(new
            {
                dados.Total,
                Items = dados.Items.Select(r => new
                {
                    r.Id,
                    PriorizarProdutos = r.PriorizarProdutos == true ? "Sim" : "Não",
                    PriorizarVolume = r.PriorizarVolume == true ? "Sim" : "Não",
                    r.Cnpj
                })
            });
        }

        /// <summary>
        /// Exclui a entidade
        /// </summary>
        /// <param name="id">id da entidade que será excluida</param>
        /// <returns>Action Result</returns>
        public ActionResult Excluir(int id)
        {
            try
            {
                _nfeConfiguracaoEmpresaService.Excluir(id);
                return Json(new { success = true });
            }
            catch (Exception e)
            {
                return Json(new { success = false, e.Message, e.StackTrace });
            }
        }

        /// <summary>
        /// Método para abrir o Formulario de Novo Registro
        /// </summary>
        ///  <param name="id">
        /// id do registro
        /// </param>
        /// <returns>
        /// Retorna o Formulario
        /// </returns>
        public ActionResult Form(int? id)
        {
            var entity = new NfeConfiguracaoEmpresa();

            if (id.HasValue)
            {
                entity = _nfeConfiguracaoEmpresaService.ObterPorId(id.Value);
            }

            ViewData["Model"] = entity;

            return View();
        }
	}
}
