﻿using Newtonsoft.Json;
using Translogic.Core.Infrastructure.Web.ContentResults;
using Translogic.Core.Infrastructure.Web.Filters;
using Translogic.Modules.Core.Domain.Model.Dto;
using Translogic.Modules.Core.Infrastructure;

namespace Translogic.Modules.Core.Controllers.Despacho
{
    using System;
    using System.Linq;
    using System.Web.Mvc;
    using Domain.Model.Bolar.Repositories;
    using Domain.Model.Despacho.Repositories;
    using Domain.Model.Dto.Despacho;
    using Domain.Model.FluxosComerciais.Repositories;
    using Domain.Model.QuadroEstados.Repositories;
    using Domain.Model.Via.Repositories;
    using Translogic.Core.Infrastructure.Web;
    using Translogic.Modules.Core.Domain.Model.Despacho;
    using System.Collections.Generic;


    public class ConfiguracaoDespachoAutomaticoController : BaseSecureModuleController
    {
        private readonly IBolarBoImpCfgRepository _bolarBoImpCfgRepository;
        private readonly IAreaOperacionalRepository _areaOperacionalRepository;
        private readonly ILocalizacaoRepository _localizacaoRepository;
        private readonly IFluxoComercialRepository _fluxoComercialRepository;
        private readonly IConfiguracaoDespachoAutomaticoRepository _configuracaoDespachoAutomaticoRepository;

        public ConfiguracaoDespachoAutomaticoController(
            IBolarBoImpCfgRepository bolarBoImpCfgRepository
            , IAreaOperacionalRepository areaOperacionalRepository
            , ILocalizacaoRepository localizacaoRepository
            , IFluxoComercialRepository fluxoComercialRepository
            , IConfiguracaoDespachoAutomaticoRepository configuracaoDespachoAutomaticoRepository)
        {
            _bolarBoImpCfgRepository = bolarBoImpCfgRepository;
            _areaOperacionalRepository = areaOperacionalRepository;
            _localizacaoRepository = localizacaoRepository;
            _fluxoComercialRepository = fluxoComercialRepository;
            _configuracaoDespachoAutomaticoRepository = configuracaoDespachoAutomaticoRepository;
        }

        [Autorizar(Transacao = "CONFDESPAUT")]
        public ActionResult Index()
        {
            return View();
        }

        [Autorizar(Transacao = "CONFDESPAUT", Acao = "Pesquisar")]
        public ActionResult Pesquisar(DetalhesPaginacaoWeb pagination, string cliente, string localizacaoId, string areaOperacional, string segmento, string terminal, string patio, string manual, string dtInicial, string dtFim)
        {
            try
            {
                var filtro = new ConfiguracaoDespachoAutomaticoFiltroDto
                                 {
                                     Cliente = cliente,
                                     LocalizacaoId = localizacaoId,
                                     AreaOperacional = areaOperacional,
                                     Segmento = segmento,
                                     Terminal = terminal,
                                     Patio = patio,
                                     Manual = manual,
                                     DtInicial = dtInicial,
                                     DtFim = dtFim
                                 };

                var resultados = _configuracaoDespachoAutomaticoRepository.Pesquisar(pagination, filtro);

                var retorno = new
                {
                    resultados.Total,
                    Items = resultados.Items.Select(item => new
                    {
                        item.Id,
                        item.Cliente,
                        item.AreaOperacional,
                        item.Localizacao,
                        item.Segmento,
                        item.Terminal,
                        item.Patio,
                        item.Manual,
                        Data = string.Format("{0:dd/MM/yyyy}", item.Data)
                    })
                };

                return Json(retorno);
            }
            catch (Exception ex)
            {
                return Json(new { Sucesso = false, ex.Message });
            }
        }

        [Autorizar(Transacao = "CONFDESPAUT", Acao = "Editar")]
        public ActionResult Editar(string id)
        {
            try
            {
                if (!string.IsNullOrEmpty(id))
                {
                    var valor = _configuracaoDespachoAutomaticoRepository.ObterPorId(int.Parse(id));
                    if (valor == null)
                        throw new ArgumentException("Registro não localizado para Editar");

                    ViewData["id"] = valor.Id;
                    ViewData["cliente"] = valor.Cliente;
                    ViewData["areaOperaciona"] = valor.AreaOperacionaId;
                    ViewData["localizacao"] = valor.LocalizacaoId;
                    ViewData["segmento"] = valor.Segmento;
                    ViewData["terminal"] = valor.Terminal;
                    ViewData["patio"] = valor.Patio;
                    ViewData["manual"] = valor.Manual;
                }

                return View();
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }

        [Autorizar(Transacao = "CONFDESPAUT", Acao = "Deletar")]
        public ActionResult DeletarRegistro(string id)
        {
            try
            {
                var valor = _configuracaoDespachoAutomaticoRepository.ObterPorId(int.Parse(id));
                if (valor == null)
                    throw new ArgumentException("Registro não localizado para excluir");

                _configuracaoDespachoAutomaticoRepository.Remover(valor);

                return Json(new
                {
                    Success = true,
                    Message = "Registro deletado com sucesso"
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }

        public ActionResult ObterClientes()
        {
            var lista = _bolarBoImpCfgRepository.ObterTodasAtivas();

            return Json(new
            {
                Items = lista.Select(item => new
                {
                    Id = item.CodigoEmpresa,
                    Descricao = item.DescricaoEmpresa.ToUpper()
                })
            });

        }

        public ActionResult ObterAreaOperacional()
        {
            var lista = _areaOperacionalRepository.ObterTodasComFluxoComercial();

            return Json(new
            {
                Items = lista.Select(item => new
                {
                    Id = item.IdTerminal,
                    Descricao = item.CodTerminal.ToUpper()
                })
            });
        }

        public ActionResult ObterLocalizacao()
        {
            var lista = _localizacaoRepository.ObterTodos();

            return Json(new
            {
                Items = lista.Select(item => new
                {
                    item.Id,
                    Descricao = item.Descricao.ToUpper()
                })
            });
        }

        public ActionResult ObterSegmento()
        {
            var lista = _fluxoComercialRepository.ObterSegmentos();

            return Json(new
            {
                Items = lista.Select(item => new
                {
                    Id = item,
                    Descricao = item.ToUpper()
                })
            });
        }

        [Autorizar(Transacao = "CONFDESPAUT", Acao = "Salvar")]
        public ActionResult Salvar(string id, string cliente, string areaOperacional, string localizacao, string segmento, string terminal, string patio, string manual)
        {
            try
            {
                var mensagem = "";
                var valor = new ConfiguracaoDespachoAutomatico();

                if (!string.IsNullOrEmpty(id))
                    valor = _configuracaoDespachoAutomaticoRepository.ObterPorId(int.Parse(id));

                valor.Cliente = cliente;
                valor.AreaOperacionaId = !string.IsNullOrEmpty(areaOperacional) ? int.Parse(areaOperacional) : 0;
                valor.LocalizacaoId = string.IsNullOrEmpty(localizacao) ? default(int?) : int.Parse(localizacao);
                valor.Segmento = segmento;
                valor.Terminal = terminal == "true" ? "S" : "N";
                valor.Patio = patio == "true" ? "S" : "N";
                valor.Manual = manual == "true" ? "S" : "N";

                ValidarConfiguracao(valor);

                if (string.IsNullOrEmpty(id))
                {
                    _configuracaoDespachoAutomaticoRepository.Inserir(valor);
                    mensagem = "Registro criado com sucesso";
                }
                else
                {
                    _configuracaoDespachoAutomaticoRepository.Atualizar(valor);
                    mensagem = "Registro atualizado com sucesso";
                }

                return Json(new
                {
                    Success = true,
                    Message = mensagem
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }

        private void ValidarConfiguracao(ConfiguracaoDespachoAutomatico valor)
        {
            if (!valor.EValido())
                throw new ArgumentException("Configuração inválida, verifique os campos obrigatórios.");

            var existe = _configuracaoDespachoAutomaticoRepository.JaCadastrado(valor.Id, valor.Cliente,
                                                                                valor.AreaOperacionaId, valor.Segmento);

            if (existe)
                throw new ArgumentException("Já existe uma configuração cadastrada com os dados informados.");
        }

        public ActionResult ObterSimNao()
        {
            var lista = new Dictionary<string, string> { { "S", "Sim" }, { "N", "Não" } };

            return Json(new
            {
                Items = lista.Select(item => new
                {
                    Id = item.Key,
                    Descricao = item.Value.ToUpper()
                })
            });
        }

        public ActionResult Exportar(string cliente, string localizacaoId, string areaOperacional, string segmento, string terminal, string patio, string manual, string dtInicial, string dtFim)
        {
            var filtro = new ConfiguracaoDespachoAutomaticoFiltroDto
            {
                Cliente = cliente,
                LocalizacaoId = localizacaoId,
                AreaOperacional = areaOperacional,
                Segmento = segmento,
                Terminal = terminal,
                Patio = patio,
                Manual = manual,
                DtInicial = dtInicial,
                DtFim = dtFim
            };

            var resultados = _configuracaoDespachoAutomaticoRepository.Exportar(filtro);

            var dic = new Dictionary<string, Func<ConfiguracaoDespachoAutomaticoDto, string>>();

            dic["Cliente"] = c => c.Cliente ?? string.Empty;
            dic["Área operacional"] = c => c.AreaOperacional ?? string.Empty;
            dic["Localização"] = c => c.Localizacao ?? string.Empty;
            dic["Segmento"] = c => c.Segmento.ToString();
            dic["Terminal"] = c => c.Terminal ?? string.Empty;
            dic["Pátio"] = c => c.Patio;
            dic["Manual"] = c => c.Manual;
            dic["Data"] = c => c.Data.ToShortDateString();

            return this.Excel(string.Format("Configuracao{0}_{1}.xls", DateTime.Now.ToString("ddMMyyyy"), DateTime.Now.ToString("HHmmss")), dic, resultados);
        }
    }
}