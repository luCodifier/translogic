﻿namespace Translogic.Modules.Core.Controllers.Despacho
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Web.Mvc;
    using ALL.Core.Dominio;
    using ICSharpCode.SharpZipLib.Core;
    using ICSharpCode.SharpZipLib.Zip;
    using Translogic.Core.Infrastructure;
    using Translogic.Core.Infrastructure.FileSystem.ExtensionMethods;
    using Translogic.Core.Infrastructure.Web;
    using Translogic.Core.Infrastructure.Web.Helpers;
    using Translogic.Modules.Core.Domain.Model.Diversos.Cte;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Dcls;
    using Translogic.Modules.Core.Domain.Model.PainelExpedicao.Documentos.Dto;
    using Translogic.Modules.Core.Domain.Model.PainelExpedicao.Documentos.Request;
    using Translogic.Modules.Core.Domain.Services.Ctes;
    using Translogic.Modules.Core.Domain.Services.FluxosComerciais;
    using Translogic.Modules.Core.Domain.Services.GestaoDocumentos;
    using Translogic.Modules.Core.Domain.Services.LaudoMercadoria;
    using Translogic.Modules.Core.Domain.Services.Mdfes;
    using Translogic.Modules.Core.Helpers;
    using Translogic.Modules.Core.Infrastructure;
    using Translogic.Modules.EDI.Domain.DataAccess.Repositories.Nfes;
    using Translogic.Modules.EDI.Domain.Models.Nfes.Repositories;
    using Translogic.Modules.Core.Spd.BLL;
    using Speed.Common;

    /// <summary>
    /// Controller de Gestao de documentos
    /// </summary>
    public class GestaoDocumentosController : BaseSecureModuleController
    {
        private readonly RelatorioDocumentosService _relatorioDocumentosService;
        private readonly MdfeService _mdfeService;
        private readonly CteService _cteService;
        private readonly DclService _dclService;
        private readonly LaudoMercadoriaService _laudoMercadoriaService;

        private readonly INfeEdiRepository _nfeEdiRepository;

        /// <summary>
        /// Construtor padrao
        /// </summary>
        /// <param name="relatorioDocumentosService">Serviço de gestão de documentos injetado</param>
        /// <param name="mdfeService">Serviço de MDF-e injetado</param>
        /// <param name="cteService">Serviço de CT-e injetado</param>
        /// <param name="dclService">Serviço de DCL injetado</param>
        /// <param name="laudoMercadoriaService">Serviço de Laudo de Mercadorias injetado</param>
        public GestaoDocumentosController(
            RelatorioDocumentosService relatorioDocumentosService,
            MdfeService mdfeService,
            CteService cteService,
            DclService dclService,
            LaudoMercadoriaService laudoMercadoriaService,
            INfeEdiRepository nfeEdiRepository)
        {
            _relatorioDocumentosService = relatorioDocumentosService;
            _mdfeService = mdfeService;
            _cteService = cteService;
            _dclService = dclService;
            _laudoMercadoriaService = laudoMercadoriaService;
            _nfeEdiRepository = nfeEdiRepository;
        }

        /// <summary>
        /// Ação do controller
        /// </summary>
        /// <returns>Action Result</returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Pesquisa das MDFes
        /// </summary>
        /// <param name="pagination">detalhes da paginacao</param>
        /// <param name="filter">informações dos filtros</param>
        /// <returns>resultado json</returns>
        [Autorizar(Transacao = "GESTAODOCUMENTOS", Acao = "Pesquisar")]
        public ActionResult Pesquisar(DetalhesPaginacaoWeb pagination, DetalhesFiltro<object>[] filter)
        {
            var dataInicial = filter
                .Where(e => e.Campo == "dataInicial" && !string.IsNullOrWhiteSpace(e.Valor[0].ToString()))
                .Select(e => e == null ? (DateTime?)null : DateTime.ParseExact(e.Valor[0].ToString(), "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture))
                .FirstOrDefault();

            var dataFinal = filter
                .Where(e => e.Campo == "dataFinal" && !string.IsNullOrWhiteSpace(e.Valor[0].ToString()))
                .Select(e => e == null ? (DateTime?)null : DateTime.ParseExact(e.Valor[0].ToString(), "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture).AddDays(1).AddSeconds(-1))
                .FirstOrDefault();

            var prefixo = filter
                .Where(e => e.Campo == "prefixo" && !string.IsNullOrWhiteSpace(e.Valor[0].ToString()))
                .Select(e => e == null ? (string)null : e.Valor[0].ToString().ToUpperInvariant())
                .FirstOrDefault();

            var os = filter
                .Where(e => e.Campo == "os" && !string.IsNullOrWhiteSpace(e.Valor[0].ToString()))
                .Select(e => e == null ? (int?)null : int.Parse(e.Valor[0].ToString(), CultureInfo.InvariantCulture))
                .FirstOrDefault();

            var origem = filter
                .Where(e => e.Campo == "origem" && !string.IsNullOrWhiteSpace(e.Valor[0].ToString()))
                .Select(e => e == null ? (string)null : e.Valor[0].ToString().ToUpperInvariant())
                .FirstOrDefault();

            var destino = filter
                .Where(e => e.Campo == "destino" && !string.IsNullOrWhiteSpace(e.Valor[0].ToString()))
                .Select(e => e == null ? (string)null : e.Valor[0].ToString().ToUpperInvariant())
                .FirstOrDefault();

            var result = _relatorioDocumentosService.Listar(pagination, prefixo, os, dataInicial, dataFinal, origem, destino);

            return Json(new
            {
                result.Total,
                Items = result.Items.Select(e => new
                {
                    e.Id,
                    e.Prefixo,
                    e.Origem,
                    e.Destino,
                    NumeroOS = e.OS,
                    DataRealizadaPartida = e.DataRealizadaPartida.ToString()
                })
            });
        }

        /// <summary>
        /// Pesquisa das Vagões nas MDFe
        /// </summary>
        /// <param name="pagination">detalhes da paginacao</param>
        /// <param name="filter">informações dos filtros</param>
        /// <returns>resultado json</returns>
        [Autorizar(Transacao = "GESTAODOCUMENTOS", Acao = "Pesquisar")]
        public ActionResult PesquisarVagoes(DetalhesPaginacaoWeb pagination, DetalhesFiltro<object>[] filter)
        {
            var dataInicial = filter
                .Where(e => e.Campo == "dataInicial" && !string.IsNullOrWhiteSpace(e.Valor[0].ToString()))
                .Select(e => e == null ? (DateTime?)null : DateTime.ParseExact(e.Valor[0].ToString(), "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture))
                .FirstOrDefault();

            var dataFinal = filter
                .Where(e => e.Campo == "dataFinal" && !string.IsNullOrWhiteSpace(e.Valor[0].ToString()))
                .Select(e => e == null ? (DateTime?)null : DateTime.ParseExact(e.Valor[0].ToString(), "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture).AddDays(1).AddSeconds(-1))
                .FirstOrDefault();

            var estacoes = (filter
                .Where(e => e.Campo == "estacoes" && !string.IsNullOrWhiteSpace(e.Valor[0].ToString()))
                .Select(e => e == null ? (string)null : e.Valor[0].ToString().ToUpperInvariant())
                .FirstOrDefault() ?? String.Empty)
                .Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

            var vagoes = (filter
                .Where(e => e.Campo == "vagoes" && !string.IsNullOrWhiteSpace(e.Valor[0].ToString()))
                .Select(e => e == null ? (string)null : e.Valor[0].ToString().ToUpperInvariant())
                .FirstOrDefault() ?? String.Empty)
                .Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

            var tmpIdLinha = filter
                .Where(e => e.Campo == "idLinha" && !string.IsNullOrWhiteSpace(e.Valor[0].ToString()))
                .Select(e => e == null ? (string)null : e.Valor[0].ToString().ToUpperInvariant())
                .FirstOrDefault();
            int tmp;
            int? idLinha = int.TryParse(tmpIdLinha, out tmp) ? (int?)tmp : null;

            var result = _relatorioDocumentosService.ListarVagoes(dataInicial.Value, dataFinal.Value, vagoes, estacoes, idLinha);

            return Json(new
            {
                Total = result.Count(),
                Items = result.Select(e => new
                {
                    e.Id,
                    e.IdItemDespacho,
                    Vagao = string.Format("{0}-{1}", e.SerieVagao, e.CodigoVagao),
                    e.Sequencia,
                    e.Origem,
                    e.Destino,
                    e.Mercadoria,
                    TB = (e.TU + e.Tara).ToString("N3"),
                    TU = e.TU.ToString("N3"),
                    e.Correntista,
                    Tara = e.Tara.ToString("N3"),
                    e.Destinatario,
                    e.Linha,
                    e.PossuiTicket,
                    DataCarregamento = e.DataCarregamento.HasValue ? e.DataCarregamento.ToString() : string.Empty,
                    DataDescarregamento = e.DataDescarregamento.HasValue ? e.DataDescarregamento.ToString() : string.Empty
                })
            });
        }

        /// <summary>
        /// Retorna as composições do trem
        /// </summary>
        /// <param name="idTrem"> id do trem</param>
        /// <returns>Composições do trem</returns>
        public ActionResult DetalheTrem(int idTrem)
        {
            ViewData["Trem"] = _relatorioDocumentosService.Listar(idTrem);
            return View();
        }

        /// <summary>
        /// Retorna as composições do trem
        /// </summary>
        /// <param name="pagination">detalhes da paginacao</param>
        /// <param name="filter">informações dos filtros</param>
        /// <returns>resultado json</returns>
        [Autorizar(Transacao = "GESTAODOCUMENTOS", Acao = "Pesquisar")]
        public ActionResult ListarComposicao(DetalhesPaginacaoWeb pagination, DetalhesFiltro<object>[] filter)
        {
            var idTrem = filter
                .Where(e => e.Campo == "idTrem" && !string.IsNullOrWhiteSpace(e.Valor[0].ToString()))
                .Select(e => e == null ? (int?)null : int.Parse(e.Valor[0].ToString(), CultureInfo.InvariantCulture))
                .FirstOrDefault();

            var result = _relatorioDocumentosService.ObterTodasComposicao(pagination, idTrem.Value);

            return Json(new
            {
                result.Total,
                Items = result.Items.Select(e => new
                {
                    e.Id,
                    Origem = e.Origem != null ? e.Origem.Codigo : string.Empty,
                    Destino = e.Destino != null ? e.Destino.Codigo : string.Empty,
                    DataLiberacao = e.DataLiberacao != null ? e.DataLiberacao.ToString() : String.Empty
                })
            });
        }

        /// <summary>
        /// Retorna as composições do trem
        /// </summary>
        /// <param name="idComposicao">id da composição</param>
        /// <returns>resultado json</returns>
        [Autorizar(Transacao = "GESTAODOCUMENTOS", Acao = "Pesquisar")]
        public ActionResult DetalhesComposicao(int idComposicao)
        {
            IList<VagaoComposicaoDto> result = _relatorioDocumentosService.ObterVagoesComposicao(idComposicao);

            return Json(new
            {
                Items = result.Select(e => new
                {
                    e.Id,
                    Vagao = string.Format("{0}-{1}", e.SerieVagao, e.CodigoVagao),
                    e.Sequencia,
                    e.Origem,
                    e.Destino,
                    e.Mercadoria,
                    TB = (e.TU + e.Tara).ToString("N3"),
                    TU = e.TU.ToString("N3"),
                    e.Correntista,
                    Tara = e.Tara.ToString("N3"),
                    e.Destinatario,
                    DataCarregamento = e.DataCarregamento.HasValue ? e.DataCarregamento.ToString() : string.Empty,
                    DataDescarregamento = e.DataDescarregamento.HasValue ? e.DataDescarregamento.ToString() : string.Empty
                })
            });
        }

        /// <summary>
        /// Retorna as composições do trem
        /// </summary>
        /// <param name="idComposicao">id da composição</param>
        /// <param name="vagoes">lista de vagões</param>
        /// <param name="tipoImpressao">tipo de impressão 0-nota 1-ticket 2-ambos</param>
        /// <returns>resultado json</returns>
        [Autorizar(Transacao = "GESTAODOCUMENTOS", Acao = "Pesquisar")]
        public ActionResult Imprimir(int idComposicao, string vagoes, int tipoImpressao)
        {
            var arrIds = vagoes.Split(',');
            List<int> idVagoes = arrIds.Select(x => int.Parse(x)).ToList();
            Stream pdfTicket = null;
            Stream pdfNfes = null;
            Stream returnContent = new MemoryStream();
            var merge = new PdfMerge();

            if (tipoImpressao == 1 || tipoImpressao == 2)
            {
                var notasTickets = _relatorioDocumentosService.ObterTicketVagoes(idComposicao, idVagoes);
                var notas = notasTickets.Select(t => t.ChaveNfe).ToList();

                if (notas.Any())
                {
                    var dadosNotas = _relatorioDocumentosService.ObterNotasTickets(notas).ToList();

                    ViewData["DADOSNOTA"] = dadosNotas;

                    ViewData["NOTATICKET"] = notasTickets;

                    var viewString = View("ImpressaoTicket").Capture(ControllerContext);

                    pdfTicket = _mdfeService.HtmlToPdf(viewString, "ticket", true);

                    if (pdfTicket != null)
                    {
                        merge.AddFile(pdfTicket);
                    }
                }
            }

            if (tipoImpressao == 0 || tipoImpressao == 2)
            {
                var notas = _relatorioDocumentosService.ObterNfeVagoesComposicao(idComposicao, idVagoes);

                if (notas.Any())
                {
                    pdfNfes = _relatorioDocumentosService.GerarPdf(notas);

                    if (pdfNfes != null)
                    {
                        merge.AddFile(pdfNfes);
                    }
                }
            }

            if (pdfNfes != null || pdfTicket != null)
            {
                returnContent = merge.Execute();
            }

            var fsr = new FileStreamResult(returnContent, "application/pdf");
            fsr.FileDownloadName = DateTime.Now.ToString() + ".pdf";
            return fsr;
        }

        /// <summary>
        /// Retorna os documentos de todos os vagões selecionados
        /// </summary>
        /// <param name="itensDespacho">lista de itens do despacho</param>
        /// <param name="tipoImpressao">tipo de impressão 0-nota 1-ticket 2-ambos</param>
        /// <returns>resultado json</returns>
        [Autorizar(Transacao = "GESTAODOCUMENTOS", Acao = "Pesquisar")]
        public ActionResult ImprimirVagoes(string itensDespacho, int tipoImpressao)
        {
            #region Formatação dos Parâmetros

            bool statusNfeFormatado = (tipoImpressao == 0 || tipoImpressao == 2);
            bool statusTicketFormatado = (tipoImpressao == 1 || tipoImpressao == 2);

            #endregion

            return RetornarArquivos(itensDespacho, null, false,
                statusNfeFormatado, statusTicketFormatado, false);
        }


        private FileStreamResult RetornarArquivos(string itensDespacho,
                                                  string ctes,
                                                  bool imprimirCte,
                                                  bool imprimirNfe,
                                                  bool imprimirTicket,
                                                  bool imprimirDcl)
        {
            var arrIds = itensDespacho.Split(',');
            List<int> idsItensDespacho = arrIds.Select(x => int.Parse(x)).ToList();

            List<int> idsCtes = new List<int>();
            string[] arrIdsCtes = null;
            if (!String.IsNullOrEmpty(ctes))
            {
                arrIdsCtes = ctes.Split(',');
                idsCtes = arrIdsCtes.Select(x => int.Parse(x)).ToList();
            }

            Stream pdfTicket = null;
            Stream pdfNfes = null;
            byte[] pdfCte = null;
            Stream pdfDcl = null;
            Stream returnContent = new MemoryStream();
            var merge = new PdfMerge();

            #region Ticket

            if (imprimirTicket)
            {
                var notasTickets = _relatorioDocumentosService.ObterTicketItensDespacho(idsItensDespacho);
                var notas = notasTickets.Select(t => t.ChaveNfe).Distinct().ToList();
                var notasTicketsClientes = _relatorioDocumentosService.ObterTicketClienteItensDespacho(idsItensDespacho, false);

                if (notas.Any())
                {
                    var dadosNotas = _relatorioDocumentosService.ObterNotasTickets(notas).ToList();

                    ViewData["DADOSNOTA"] = dadosNotas;
                    ViewData["NOTATICKET"] = notasTickets;
                    ViewData["NOTATICKETCLIENTES"] = notasTicketsClientes;

                    var viewString = View("ImpressaoTicket").Capture(ControllerContext);

                    pdfTicket = _mdfeService.HtmlToPdf(viewString, "ticket", true);

                    merge.AddFile(pdfTicket);
                }
            }

            #endregion

            #region Nota Fiscal Eletrônica

            if (imprimirNfe)
            {
                var notas = _relatorioDocumentosService.ObterNfePdfItensDespacho(idsItensDespacho);
                if ((notas != null) && (notas.Count > 0))
                {
                    var notasInt = notas.Select(i => (int)i).ToList();
                    pdfNfes = _relatorioDocumentosService.GerarPdfNotaClienteUpload(notasInt);
                    if (pdfNfes != null)
                        merge.AddFile(pdfNfes);
                }
            }

            #endregion

            #region CT-e

            if (imprimirCte)
            {
                foreach (int idCteCod in idsCtes)
                {
                    var file = _cteService.ObterArquivoCte(idCteCod);
                    if (file != null)
                    {
                        pdfCte = file.ArquivoPdf;
                        merge.AddFile(pdfCte);
                    }
                }


            }

            #endregion

            #region DCL

            if (imprimirDcl)
            {

                // TODO: PERFORMANCE
#if DEBUG
                var despachos2 = _relatorioDocumentosService.ObterDespachosPorItensDespacho(idsItensDespacho.ToArray());
#endif
                var despachos = BL_Despacho.ObterDespachosPorItensDespacho(idsItensDespacho.ToArray());
                var listaDcls = _dclService.ObterDclImpressao(despachos);

                if (listaDcls.Any())
                {
                    List<DclImpressao> dclsImpressao = new List<DclImpressao>();

                    foreach (DclImpressao dclImp in listaDcls)
                    {

                        if (dclsImpressao.Count < 2)
                        {
                            dclsImpressao.Add(dclImp);
                        }

                        if (dclsImpressao.Count == 2)
                        {
                            pdfDcl = _relatorioDocumentosService.GerarDclPdfLote(dclsImpressao);
                            merge.AddFile(pdfDcl);
                            dclsImpressao = new List<DclImpressao>();
                        }
                    }


                    // Caso tenha sobrado algum cte não impresso no PDF (números impares)
                    if (dclsImpressao.Count > 0)
                    {
                        pdfDcl = _relatorioDocumentosService.GerarDclPdfLote(dclsImpressao);
                        merge.AddFile(pdfDcl);
                        dclsImpressao = null;
                    }
                }

            }

            #endregion

            if (pdfNfes != null || pdfTicket != null || pdfCte != null || pdfDcl != null)
            {
                returnContent = merge.Execute();
            }

            var fsr = new FileStreamResult(returnContent, "application/pdf");
            fsr.FileDownloadName = DateTime.Now.ToString() + ".pdf";
            return fsr;
        }


        /// <summary>
        /// Retorna os documentos de todos os vagões selecionados
        /// </summary>
        /// <param name="ids">lista de itens do despacho</param>
        /// <param name="codigosVagoes">lista dódigos de vagões separados por ';'</param>
        /// <param name="idMdfe">lista de itens do MDF-e</param>
        /// <param name="cte">boleano indicando se devemos imprimir CT-e</param>
        /// <param name="nfe">boleano indicando se devemos imprimir NF-e</param>
        /// <param name="ticket">boleano indicando se devemos imprimir Ticket de Pesagem</param>
        /// <param name="dcl">boleano indicando se devemos imprimir DCL</param>
        /// <param name="laudoMercadoria">boleano indicando se devemos imprimir o Laudo da Mercadoria</param>
        /// <param name="mdfe">boleano indicando se devemos imprimir MDF-e</param>
        /// <param name="historico">boleano indicando se a chamada é de uma tela de histórico</param>
        [Autorizar(Transacao = "GESTAODOCUMENTOS", Acao = "Pesquisar")]
        public ActionResult ImprimirVagoesPainelExpedicao(string ids,
                                                          string codigosVagoes,
                                                          string idMdfe,
                                                          bool cte,
                                                          bool nfe,
                                                          bool webDanfe,
                                                          bool ticket,
                                                          bool dcl,
                                                          bool laudoMercadoria,
                                                          bool mdfe,
                                                          bool historico)
        {
            return RetornarArquivosPainelExpedicao(ids, codigosVagoes, idMdfe, cte, nfe, webDanfe, ticket, dcl, laudoMercadoria, mdfe, historico);
        }

        /// <summary>
        /// Obtém as linhas para uma área operacional
        /// </summary>
        /// <param name="areaOper">Código da area operacional</param>
        /// <returns> Resultado JSON </returns>
        [Autorizar(Transacao = "GESTAODOCUMENTOS", Acao = "Pesquisar")]
        public JsonResult ObterLinhasPorAreaOperacional(string areaOper)
        {
            if (String.IsNullOrWhiteSpace(areaOper))
            {
                throw new TranslogicException("Área Operacional inválida.");
            }

            var linhas = _relatorioDocumentosService.ObterLinhasPorAreaOperacional(areaOper);

            return Json(new
            {
                totalCount = linhas.Count,
                Items = linhas.Select(g => new
                {
                    g.Id,
                    g.Codigo
                })
            });
        }

        private ActionResult RetornarArquivosPainelExpedicao(string itensDespacho,
                                                             string codigosVagoes,
                                                             string idMdfe,
                                                             bool imprimirCte,
                                                             bool imprimirNfe,
                                                             bool imprimirWebDanfe,
                                                             bool imprimirTicket,
                                                             bool imprimirDcl,
                                                             bool imprimirLaudoMercadoria,
                                                             bool imprimirMdfe,
                                                             bool historico)
        {
            var arrIds = itensDespacho.Split(',');
            var idsItensDespacho = new List<int>();
            if ((!String.IsNullOrEmpty(itensDespacho)) && (arrIds.Length > 0))
                idsItensDespacho = arrIds.Select(x => int.Parse(x)).ToList();
            else
                idsItensDespacho.Add(-1);

            List<string> listaCodigosVagoes = null;
            if (!String.IsNullOrEmpty(codigosVagoes))
            {
                listaCodigosVagoes = codigosVagoes.Split(';').ToList();
            }

            List<ImpressaoDocumentosVagaoItemDespacho> vagoesItensDespacho = null;

            #region Variáveis dos Arquivos

            /* Arquivo ZIP final */
            var arquivoZipContendoArquivosPDF = new MemoryStream();
            /* PDF Arquivos */
            var arquivosPDF = new Dictionary<string, Stream>();
            /* PDF Ticket */
            Stream pdfTicket = null;
            /* PDF Dcl */
            Stream pdfDcl = null;
            /* PDF dos Laudos das Mercadorias */
            Stream pdfLaudoMercadoria = null;

            /* Variaveis Ticket */
            IList<ItemDespachoNotaTicketVagaoDto> notasTickets = null;
            List<string> notas = null;
            IList<NotaTicketVagaoClienteDto> notasTicketsClientes = null;
            /* Variaveis Nfe */
            IList<ItemDespachoNfeDto> notasNfe = null;
            IList<ItemDespachoNfeDto> pdfsNfes = null;
            /* Variaveis Cte */
            IList<CteArquivo> ctes = null;
            List<int> idsCtes = null;
            /* Variaveis Dcl */
            List<Spd.Data.ItemDespacho> itemsDespachos = null;
            IList<DclImpressao> listaDcls = null;
            List<DclImpressao> dclsImpressao = null;
            /* Variáveis dos Laudos das Mercadorias */
            IList<LaudoMercadoriaDto> laudoMercadoriasDto = null;
            /* Variaveis Mdfe */
            Stream pdfMdfe = null;

            #endregion

            #region Busca de Informações nos Repositórios

            vagoesItensDespacho = _relatorioDocumentosService.ObterVagoesCtesItensDespacho(idsItensDespacho, listaCodigosVagoes).ToList();
            idsCtes = vagoesItensDespacho.Select(v => Decimal.ToInt32(v.IdCte)).ToList();

            if (imprimirTicket)
            {
                notasTickets = _relatorioDocumentosService.ObterTicketItensDespachoDto(idsItensDespacho, historico);
                notasTicketsClientes = _relatorioDocumentosService.ObterTicketClienteItensDespacho(idsItensDespacho, historico);
            }

            if (imprimirNfe || imprimirWebDanfe)
            {
                notasNfe = _relatorioDocumentosService.ObterNfePdfItensDespachoDto(idsItensDespacho);
                var chavesNfes = notasNfe.Select(nf => nf.ChaveNfe).ToList();
                if (chavesNfes.Any())
                {
                    pdfsNfes = _relatorioDocumentosService.GerarPdfNfes(chavesNfes, imprimirNfe, imprimirWebDanfe);
                }
            }

            if (imprimirCte)
            {
                ctes = _cteService.ObterArquivoCteLista(idsCtes);
            }

            if (imprimirDcl)
            {
                // TODO: PERFORMANCE
#if DEBUG2
                var despachos2 = _relatorioDocumentosService.ObterDespachosPorItensDespachoDto(idsItensDespacho.ToArray());
#endif
                itemsDespachos = BL_ItemDespacho.ObterDespachosPorItensDespacho(idsItensDespacho.ToArray());
            }
            else
            {
                itemsDespachos = new List<Spd.Data.ItemDespacho>();
            }

            if (imprimirLaudoMercadoria)
            {
                laudoMercadoriasDto = _relatorioDocumentosService.ObterLaudoMercadoriaPdfItensDespachoDto(vagoesItensDespacho.Select(x => x.CodigoVagao).ToList());
            }

            #endregion

            #region Loop Para cada item de carregamento (Vagao/Fluxo)

            int contadorArquivos = 1;
            var vagoesCriados = new List<string>();

            List<Spd.Data.Despacho> despachos;

            if (itemsDespachos != null && itemsDespachos.Any())
            {
                despachos = BL_Despacho.ObterDespachosPorItensDespacho(
                                            itemsDespachos.Select(p => p.IdItemDespacho.ToInt32()).ToArray());
            }
            else
            {
                despachos = new List<Spd.Data.Despacho>();
            }

            foreach (var vagaoItem in vagoesItensDespacho)
            {
                var documentosDoVagao = new PdfMerge();

                #region Documentos :: TicketPesagem, NF-e, CT-e, DCL, LaudoMercadorias

                #region Ticket Pesagem
                if (imprimirTicket)
                {
                    if (!vagoesCriados.Any(vc => vc == vagaoItem.CodigoVagao))
                    {
                        if ((notasTickets != null) && (notasTickets.Count > 0))
                        {
                            notas =
                                notasTickets.ToList().Where(
                                    t => t.NotaTicketVagao.Vagao.CodigoVagao == vagaoItem.CodigoVagao).Select(
                                        t => t.NotaTicketVagao.ChaveNfe).Distinct().ToList();
                        }

                        if ((notas != null) && (notas.Count > 0))
                        {
                            var dadosNotas = _relatorioDocumentosService.ObterNotasTickets(notas).ToList();

                            TempData["IMPRESSAOPAINELEXPEDICAO"] = true;
                            TempData["DADOSNOTA"] = dadosNotas;
                            TempData["NOTATICKET"] =
                                notasTickets.Where(t => t.NotaTicketVagao.Vagao.CodigoVagao == vagaoItem.CodigoVagao)
                                    .Select(t => t.NotaTicketVagao)
                                    .GroupBy(t => t.ChaveNfe)
                                    .Select(t => t.First())
                                    .ToList();

                            var clientes = notasTicketsClientes.Where(t => t.IdItemDespacho == vagaoItem.IdItemDespacho).ToList();
                            var cliente = clientes.FirstOrDefault();
                            clientes.Clear();
                            clientes.Add(cliente);
                            TempData["NOTATICKETCLIENTES"] = clientes;
                            //no ascx ta pegando o peso formatado do vagao...debuga q vc vai ver...por isso a diferença de pesos
                            var viewString = View("ImpressaoTicket").Capture(ControllerContext);

                            pdfTicket = _mdfeService.HtmlToPdf(viewString, "ticket", true);

                            documentosDoVagao.AddFile(pdfTicket);

                            vagoesCriados.Add(vagaoItem.CodigoVagao);
                        }
                    }
                }
                #endregion

                #region Nfe
                if (imprimirNfe || imprimirWebDanfe)
                {
                    if ((notasNfe != null) && (notasNfe.Count > 0))
                    {
                        var chavesNfeDespacho = notasNfe.Where(n => n.IdItemDespacho == vagaoItem.IdItemDespacho)
                                                    .Select(n => n.ChaveNfe)
                                                    .Distinct()
                                                    .ToList();

                        if (chavesNfeDespacho.Any() && pdfsNfes != null)
                        {
                            var pdfsNfe = pdfsNfes.Where(c => chavesNfeDespacho.Contains(c.ChaveNfe)).ToList();
                            foreach (var pdfNfe in pdfsNfe)
                            {
                                if (pdfNfe.Pdf != null)
                                {
                                    documentosDoVagao.AddFile(pdfNfe.Pdf);
                                }
                            }
                        }
                    }
                }
                #endregion

                #region Cte
                if (imprimirCte)
                {
                    if ((ctes != null) && (ctes.Count > 0))
                    {
                        var cteArquivoItemDespacho = ctes.Where(n => n.Id == vagaoItem.IdCte).SingleOrDefault();
                        if (cteArquivoItemDespacho != null)
                        {
                            documentosDoVagao.AddFile(cteArquivoItemDespacho.ArquivoPdf);
                        }
                    }
                }
                #endregion

                #region DCL

                if (imprimirDcl)
                {
                    var items = itemsDespachos.Where(d => d.IdItemDespacho == vagaoItem.IdItemDespacho);
                    var _despachos = despachos.Where(p => items.Any(q => q.IdDespacho == p.IdDespacho)).ToList();

                    listaDcls = _dclService.ObterDclImpressao(_despachos);

                    if (listaDcls.Any())
                    {
                        dclsImpressao = new List<DclImpressao>();
                        dclsImpressao.AddRange(listaDcls.AsEnumerable());
                        pdfDcl = _relatorioDocumentosService.GerarDclPdfLote(dclsImpressao);
                        documentosDoVagao.AddFile(pdfDcl);
                    }
                }

                #endregion

                #region Laudo Mercadoria

                if (imprimirLaudoMercadoria)
                {
                    var dtoDespacho = laudoMercadoriasDto.Where(l => l.CodigoVagao == vagaoItem.CodigoVagao).OrderByDescending(x => x.DataClassificacao).ToList();
                    var laudoMercadoriaImpressao = _laudoMercadoriaService.ObterLaudoMercadoriaImpressao(dtoDespacho);
                    foreach (var impressao in laudoMercadoriaImpressao)
                    {
                        pdfLaudoMercadoria = _relatorioDocumentosService.GerarLaudoMercadoriaPdf(impressao);
                        documentosDoVagao.AddFile(pdfLaudoMercadoria);
                    }
                }

                #endregion

                #endregion

                if (documentosDoVagao.FileListCount() > 0)
                {
                    var nomeArquivo = "arquivo_" + contadorArquivos.ToString().PadLeft(4, '0') + "_" +
                                         vagaoItem.CodigoVagao + "_" + vagaoItem.DataCarga.ToString("yyyyMMdd") + ".pdf";
                    arquivosPDF.Add(nomeArquivo, documentosDoVagao.Execute());
                    contadorArquivos++;
                }
            }

            #region Mdfe

            if (imprimirMdfe)
            {
                var mdfeDados = _mdfeService.ListarDetalhesDaComposicao(Convert.ToInt32(idMdfe));
                var numeroOS = (mdfeDados != null ? (mdfeDados.OrdemServico.Numero.HasValue ? mdfeDados.OrdemServico.Numero.Value : 0) : 0);
                pdfMdfe = this._mdfeService.GerarPdf(Convert.ToInt32(idMdfe));

                if (pdfMdfe != null)
                {
                    var fsrMdfe = new FileStreamResult(pdfMdfe, "application/pdf");
                    fsrMdfe.FileDownloadName = "arquivo_mdfe_OS_" + numeroOS.ToString() + ".pdf";
                    return fsrMdfe;
                }
            }

            #endregion

            arquivoZipContendoArquivosPDF = this.GerarArquivoPDFZipLote(arquivosPDF);
            #endregion

            FileStreamResult fsr = null;
            #region Retorno Arquivos PDF para Download

            if (arquivoZipContendoArquivosPDF != null)
            {
                var fileName = string.Format("ExpedicaoDocumentosPDFVagao_{0}_{1}.zip", DateTime.Now.ToString("ddMMyyyy"), DateTime.Now.ToString("HHmmss"));
                fsr = new FileStreamResult(arquivoZipContendoArquivosPDF, "application/zip");
                fsr.FileDownloadName = fileName;
            }

            #endregion

            return fsr;
        }

        /// <summary>
        /// Realiza a geração do arquivo .zip contendo todos os PDF
        /// </summary>
        /// <param name="arquivos">Dictionary contendo os nomes dos arquivos e os Stream dos arquivos</param>
        /// <returns>MemoryStream contendo os Arquivos para serem compactados em Zip</returns>
        public MemoryStream GerarArquivoPDFZipLote(Dictionary<string, Stream> arquivos)
        {
            try
            {
                return CreateToMemoryStream(arquivos);
            }
            catch (Exception)
            {
                return null;
            }
        }

        private MemoryStream CreateToMemoryStream(Dictionary<string, Stream> arquivos)
        {
            var outputMemStream = new MemoryStream();
            var zipStream = new ZipOutputStream(outputMemStream);

            zipStream.SetLevel(9); // 0-9, 9 being the highest level of compression

            foreach (var arquivo in arquivos)
            {
                var newEntry = new ZipEntry(arquivo.Key);
                newEntry.DateTime = DateTime.Now;

                zipStream.PutNextEntry(newEntry);
                StreamUtils.Copy(arquivo.Value, zipStream, new byte[4096]);
                zipStream.CloseEntry();
            }

            zipStream.IsStreamOwner = false;	// False stops the Close also Closing the underlying stream.
            zipStream.Close();			// Must finish the ZipOutputStream before using outputMemStream.

            outputMemStream.Position = 0;
            return outputMemStream;
        }
    }
}
