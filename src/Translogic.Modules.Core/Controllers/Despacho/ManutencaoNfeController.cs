﻿namespace Translogic.Modules.Core.Controllers.Despacho
{
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Web;
	using System.Web.Mvc;
	using ALL.Core.Util;
	using Domain.Model.FluxosComerciais.Nfes;
	using Domain.Services.FluxosComerciais;
	using Infrastructure;
	using Translogic.Core.Infrastructure.Web.Filters;
	using Translogic.Modules.Core.Domain.Model.Diversos;
	using Translogic.Modules.Core.Domain.Model.Dto;
using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Simconsultas;

	/// <summary>
    /// Classe ManutencaoNfeController
	/// </summary>
	public class ManutencaoNfeController : BaseSecureModuleController
	{
		private readonly NfeService _nfeService;

		/// <summary>
		/// Initializes a new instance of the <see cref="ConsultasNfeController"/> class.
		/// </summary>
		/// <param name="nfeService"> Serviço de Nfe injetado</param>
        public ManutencaoNfeController(NfeService nfeService)
		{
			_nfeService = nfeService;
		}

		/// <summary>
		/// Index para consulta de nfe
		/// </summary>
		/// <returns>Retorna a View Default</returns>
        [Autorizar(Transacao = "MANUTENCAONFE")]
		public ActionResult Index()
		{
			return View();
		}

		/// <summary>
		/// Obtém a nfe da base
		/// </summary>
        /// <param name="chave"> Chave Nfe a ser processada</param>
		/// <returns> String do status do processamento.  </returns>
        [Autorizar(Transacao = "MANUTENCAONFE", Acao = "Pesquisar")]
        public JsonResult Carregar(string chave)
		{
            bool indTemPermissaoAlterar = _nfeService.VerificarUsuarioPodeAlterarPesoNfe(UsuarioAtual);

            if (!indTemPermissaoAlterar)
            {
                return Json(new { success = false, Message = "Privilégios insuficientes para editar a Nota Fiscal!" });
            }

			var nfe = _nfeService.ObterDadosNfeBancoDados(chave);

            if (nfe == null)
			{
                return Json(new { success = false, Message = "Nota Fiscal não localizada com a chave informada!" });
			}

            var statusNfe = _nfeService.ObterStatusNfe(chave);

		    return Json(new
		                    {
                                nfe.ChaveNfe,

                                nfe.BairroDestinatario,
                                nfe.BairroEmitente,

                                nfe.CepDestinatario,
                                nfe.CepEmitente,

                                nfe.CnpjDestinatario,
                                nfe.CnpjEmitente,

                                nfe.CodigoMunicipioDestinatario,
                                nfe.CodigoMunicipioEmitente,

                                nfe.CodigoPaisDestinatario,
                                nfe.CodigoPaisEmitente,

                                nfe.ComplementoDestinatario,
                                nfe.ComplementoEmitente,

                                nfe.InscricaoEstadualDestinatario,
                                nfe.InscricaoEstadualEmitente,

                                nfe.LogradouroDestinatario,
                                nfe.LogradouroEmitente,

                                nfe.MunicipioDestinatario,
                                nfe.MunicipioEmitente,

                                nfe.NomeFantasiaDestinatario,
                                nfe.NomeFantasiaEmitente,

                                nfe.NumeroDestinatario,
                                nfe.NumeroEmitente,

                                nfe.PaisDestinatario,
                                nfe.PaisEmitente,

                                nfe.RazaoSocialDestinatario,
                                nfe.RazaoSocialEmitente,

                                nfe.TelefoneDestinatario,
                                nfe.TelefoneEmitente,

                                nfe.UfDestinatario,
                                nfe.UfEmitente,

		                        statusNfe.Origem
		                    });
		}

		/// <summary>
		/// Altera o peso e volume de uma NFe
		/// </summary>
        /// <param name="nfe">chave da nfe que sera alterada</param>
		/// <returns>json com a mensagem e sucesso da operacao</returns>
        [Autorizar(Transacao = "MANUTENCAONFE", Acao = "Salvar")]
		public ActionResult Salvar(NfeSimconsultas nfe)
		{
			try
			{
                _nfeService.AlterarNfe(nfe, UsuarioAtual.Codigo);
			}
			catch (Exception ex)
			{
                return Json(new { success = false, ex.Message });
			}

            return Json(new { success = true, Message = string.Format("Nota {0} alterada com sucesso!", nfe.ChaveNfe) });
		}
	}
}
