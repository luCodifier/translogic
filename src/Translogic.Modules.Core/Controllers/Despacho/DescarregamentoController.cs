﻿namespace Translogic.Modules.Core.Controllers.Despacho
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using Domain.Model.Acesso;
    using Domain.Model.Dto;
    using Domain.Model.Estrutura;
    using Domain.Services.FluxosComerciais;
    using Infrastructure;
    using Newtonsoft.Json.Converters;
    using Translogic.Core.Infrastructure.Web.ContentResults;
    using Translogic.Core.Infrastructure.Web.Filters;

    /// <summary>
    /// Controller para Descarregamento de cargas
    /// </summary>
    public class DescarregamentoController : BaseSecureModuleController
    {
        private readonly DescarregamentoService _descarregamentoServ;

        /// <summary>
        /// Construtor do Controller
        /// </summary>
        /// <param name="descService">Service para controle dos descarregamentos</param>
        public DescarregamentoController(DescarregamentoService descService)
        {
            _descarregamentoServ = descService;
        }

        /// <summary>
        /// Página de descarregamento
        /// </summary>
        /// <returns>HTML da página</returns>
        [Autorizar(Transacao = "DSC_AUT", Acao = "Descarregar")]
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Lista de vagões de descarregamento
        /// </summary>
        /// <param name="codAO">Código da AO/Estação</param>
        /// <param name="codLinha">Código da Linha</param>
        /// <param name="codVagao">Código do Vagão</param>
        /// <param name="codMercadoria">Código da Mercadoria</param>
        /// <param name="codFluxo">Código do Fluxo</param>
        /// <param name="codPedido">Código do Pedido</param>
        /// <param name="identCliente">Id do Cliente</param>
        /// <param name="dataInicio">Data de início da pesquisa</param>
        /// <param name="dataFim">Data de fim da pesquisa</param>
        /// <param name="tipoFiltro">
        /// Tipo do Filtro:
        ///     'N': Não descarregados;
        ///     'D': Descarregados sem erros
        ///     'E': Ocorrência de erros no descarregamento
        /// </param>
        /// <returns>Lista de vagões para descarregamento</returns>
        [Autorizar(Transacao = "DSC_AUT", Acao = "Descarregar")]
        public ActionResult VagoesParaDescarregamento(string codAO, string codLinha, string codVagao, string codMercadoria, string codFluxo, string codPedido, string identCliente, DateTime dataInicio, DateTime dataFim, string tipoFiltro)
        {
            IList<VagaoDescarregamentoDto> dataResult = null;
            switch (tipoFiltro.ToUpper())
            {
                case "N":
                    dataResult = _descarregamentoServ.ObterVagoesParaDescarregamento(codAO, codLinha, codVagao, codMercadoria, codFluxo, codPedido, identCliente, dataInicio, dataFim);
                    break;
                case "D":
                    dataResult = _descarregamentoServ.ObterVagoesDescarregados(codAO, codLinha, codVagao, codMercadoria, codFluxo, codPedido, identCliente, dataInicio, dataFim);
                    break;
                case "E":
                    dataResult = _descarregamentoServ.ObterVagoesComErro(codAO, codLinha, codVagao, codMercadoria, codFluxo, codPedido, identCliente, dataInicio, dataFim);
                    break;
            }

            var objRes = new JsonNetResult()
                             {
                                 Data = dataResult
                             };
            objRes.SerializerSettings.Converters.Add(new IsoDateTimeConverter());

            return objRes;
        }

        /// <summary>
        /// Efetuar descarregamento de vagões automaticamente
        /// </summary>
        /// <param name="codAO">Identificador da Área Operacional</param>
        /// <param name="vagaoEmCarreg">Indica se é para mudar os vagões para "EM CARREGAMENTO"</param>
        /// <param name="vagoes">Lista de vagões</param>
        /// <returns>Json com resultado da operação</returns>
        [Autorizar(Transacao = "DSC_AUT", Acao = "Descarregar")]
        public ActionResult DescarregarVagoes(string codAO, bool vagaoEmCarreg, List<VagaoDescarregamentoDto> vagoes)
        {
            try
            {
                _descarregamentoServ.DescarregarVagoes(UsuarioAtual, codAO, vagaoEmCarreg, vagoes);
            }
            catch (Exception ex)
            {
                return Json(
                    new
                    {
                        sucesso = false,
                        mensagem = ex.Message
                    },
                    JsonRequestBehavior.AllowGet
                );
            }

            return Json(
                new
                {
                    sucesso = true
                },
                JsonRequestBehavior.AllowGet
            );
        }

        /// <summary>
        /// Lista vagões 
        /// </summary>
        /// <param name="codAO">Identificador da Área Operacional</param>
        /// <returns>Json com resultado da operação</returns>
        [Autorizar(Transacao = "DSC_AUT", Acao = "Descarregar")]
        public ActionResult ClientesComVagoes(string codAO)
        {
            IEnumerable<string> tmp = null;
            try
            {
                tmp = _descarregamentoServ.ObterClientesComDescarregamentos(codAO);
            }
            catch (Exception)
            {
                return Json(
                    new
                    {
                        sucesso = false
                    },
                    JsonRequestBehavior.AllowGet
                );
            }

            return Json(
                new
                {
                    sucesso = true,
                    items = tmp.OrderBy(a => a).Select(a => new { Value = a }).ToList()
                },
                JsonRequestBehavior.AllowGet
            );
        }
    }
}
