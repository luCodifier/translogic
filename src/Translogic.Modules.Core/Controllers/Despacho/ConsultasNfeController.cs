﻿namespace Translogic.Modules.Core.Controllers.Despacho
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using Domain.Model.FluxosComerciais.Nfes;
    using Domain.Services.FluxosComerciais;
    using Infrastructure;
    using Translogic.Modules.Core.Domain.Model.Diversos;
    using Translogic.Modules.Core.Domain.Model.Diversos.Cte;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Modules.Core.Domain.Services.GestaoDocumentos;
    using Translogic.Modules.Core.Domain.Services.Edi;
    using Translogic.Core.Infrastructure.Web.ContentResults;
    using Translogic.Core.Infrastructure.Web;
    using System.IO;
    using Translogic.Modules.Core.Spd.BLL;

    /// <summary>
    /// Classe ConsultasNfeController
    /// </summary>
    public class ConsultasNfeController : BaseSecureModuleController
    {
        private readonly NfeService _nfeService;
        private readonly BaixarNfeService _baixarNfeService;
        private readonly RelatorioDocumentosService _relatorioDocumentosService;
        private readonly EdiErroService _ediErroService;

        /// <summary>
        /// Initializes a new instance of the <see cref="ConsultasNfeController"/> class.
        /// </summary>
        /// <param name="nfeService"> Serviço de Nfe injetado</param>
        /// <param name="baixarNfeService"> Serviço do Nfe Em Lote Service injetado</param>
        /// <param name="relatorioDocumentosService"> Serviço do Relatorio de documentos injetado</param>
        /// <param name="ediErroService"> Serviço do EDI Erro injetado</param>
        public ConsultasNfeController(NfeService nfeService, BaixarNfeService baixarNfeService, RelatorioDocumentosService relatorioDocumentosService, EdiErroService ediErroService)
        {
            _nfeService = nfeService;
            _baixarNfeService = baixarNfeService;
            _relatorioDocumentosService = relatorioDocumentosService;
            _ediErroService = ediErroService;
        }

        /// <summary>
        /// Index para consulta de nfe
        /// </summary>
        /// <returns>Retorna a View Default</returns>
        [Autorizar(Transacao = "CTEPESQNFE")]
        public ActionResult Index()
        {
            ViewData["NotasEdiErroRegularizar"] = "";

            return View();
        }

        /// <summary>
        /// ConsultaFluxosAssociados
        /// </summary>
        public ActionResult ConsultaFluxosAssociados()
        {
            return View();
        }

        /// <summary>
        /// Index para consulta de nfe
        /// </summary>
        /// <returns>Retorna a View Default</returns>
        [Autorizar(Transacao = "CTEPESQNFE")]
        public ActionResult RegularizarNotas(string param1)
        {
            if (param1 != null)
            {
                var chaves = _ediErroService.RetornaChaves(param1);
                ViewData["NotasEdiErroRegularizar"] = chaves;
            }

            return View("Index");
        }

        /// <summary>
        /// Iniciar Processo Nfe
        /// </summary>
        /// <param name="chaves">chaves das nfes separadas por ;</param>
        /// <param name="notasBrado">notas de clientes da Brado</param>
        /// <returns>sucesso da operacao</returns>
        [Autorizar(Transacao = "CTEPESQNFE", Acao = "Pesquisar")]
        public ActionResult IniciarProcessoNfe(string chaves, bool notasBrado)
        {
            var result = _baixarNfeService.IniciarProcessoNfe(chaves, notasBrado);
            return Json(result.Select(e => new { Chave = e, Mensagem = string.Empty, Status = string.Empty }));
        }

        /// <summary>
        /// Consultar Processo Nfe
        /// </summary>
        /// <param name="chaves">chaves das nfes separadas por ;</param>
        /// <param name="chavesQueJaDeramErro">chaves das nfes que já deram erro</param>
        /// <returns>notas faltantes</returns>
        public ActionResult ConsultarProcessoNfe(string chaves, string[] chavesQueJaDeramErro)
        {
            var chavesComErrosEDescricaoErro = _baixarNfeService.ConsultarErrosNfe(chaves);

            if (chavesQueJaDeramErro != null && chavesQueJaDeramErro.Length > 0 && chavesQueJaDeramErro[0] != string.Empty)
            {
                foreach (var item in chavesQueJaDeramErro)
                {
                    chavesComErrosEDescricaoErro.Add(item.Split('|')[0], item.Split('|')[1]);
                }
            }

            var chavesValidas = _baixarNfeService.RecuperarChaves(chaves);
            var chavesFaltantes = _baixarNfeService.ConsultarProcessoNfe(chaves);
            var chavesComErros = chavesComErrosEDescricaoErro.Select(e => e.Key).ToList();

            var matriz = chavesFaltantes.Select(e => new { Chave = e, Mensagem = string.Empty, Status = string.Empty }).ToList();
            matriz.AddRange(chavesComErros.Select(e => new { Chave = e, Mensagem = chavesComErrosEDescricaoErro[e], Status = "ERRO" }));
            matriz.AddRange(chavesValidas.Where(e => !matriz.Select(i => i.Chave).Contains(e)).Select(e => new { Chave = e, Mensagem = string.Empty, Status = "OK" }));

            matriz = matriz.OrderBy(e => e.Chave).ToList();

            return Json(matriz);
        }

        /// <summary>
        /// Enviar Email Erros
        /// </summary>
        /// <param name="chavesComErros">chaves Com Erros</param>
        /// <returns>Action Result</returns>
        public ActionResult EnviarEmailErros(string[] chavesComErros)
        {
            _baixarNfeService.EnviarEmail(chavesComErros, UsuarioAtual, "Erro ao baixar de NFe (1154)");
            return Json(new { success = true });
        }

        /// <summary>
        /// Obtém o status processamento atual
        /// </summary>
        /// <param name="chaveNfe"> Chave Nfe a ser processada</param>
        /// <returns> String do status do processamento.  </returns>
        [Autorizar(Transacao = "CTEPESQNFE", Acao = "Pesquisar")]
        public JsonResult ObterDadosNfe(string chaveNfe)
        {
            NfeReadonly dadosNfe = _nfeService.ObterDadosNfeBancoDados(chaveNfe);
            StatusNfe statusNfe = _nfeService.ObterStatusNfe(chaveNfe);
            IList<DespachosNfeDto> listaDespachosNfe = _nfeService.ObterDespachosPorNfe(chaveNfe);
            bool indTemPermissaoAlterar = _nfeService.VerificarUsuarioPodeAlterarPesoNfe(UsuarioAtual);
            bool indTemPermissaoEstornoSaldoNfe = _nfeService.VerificarUsuarioPossuiPermissaoEstornoSaldoNfe(UsuarioAtual);

            if (dadosNfe != null && statusNfe == null)
            {
                _nfeService.ValidarChaveNfe(dadosNfe.ChaveNfe, TelaProcessamentoNfe.PreCarregamento);
            }

            if (dadosNfe == null)
            {
                return Json(new
                    {
                        Erro = true,
                        Mensagem = string.Format("Nota {0} não localizada na base de dados. Favor baixar a nota, utilizando a tela 1154 (Carregamento de NF-e)", chaveNfe)
                    });
            }

            return Json(new
                            {
                                Erro = false,
                                Mensagem = string.Empty,
                                dadosNfe.NumeroNotaFiscal,
                                dadosNfe.SerieNotaFiscal,
                                DataEmissao = dadosNfe.DataEmissao.ToShortDateString(),
                                Valor = dadosNfe.Valor.ToString(),
                                dadosNfe.CnpjEmitente,
                                dadosNfe.RazaoSocialEmitente,
                                dadosNfe.InscricaoEstadualEmitente,
                                dadosNfe.UfEmitente,
                                dadosNfe.CnpjDestinatario,
                                dadosNfe.RazaoSocialDestinatario,
                                dadosNfe.InscricaoEstadualDestinatario,
                                dadosNfe.UfDestinatario,
                                dadosNfe.CnpjTransportadora,
                                dadosNfe.NomeTransportadora,
                                dadosNfe.UfTransportadora,
                                dadosNfe.MunicipioTransportadora,
                                statusNfe.Origem,
                                PesoBruto = (statusNfe.Peso).ToString(),
                                Peso = (statusNfe.Peso).ToString(),
                                Volume = (statusNfe.Volume).ToString(),
                                PesoDisponivel = (statusNfe.Peso - statusNfe.PesoUtilizado) < 0 ? 0 : Math.Round((statusNfe.Peso - statusNfe.PesoUtilizado), 2),
                                VolumeDisponivel = (statusNfe.Volume - statusNfe.VolumeUtilizado) < 0 ? 0 : Math.Round((statusNfe.Volume - statusNfe.VolumeUtilizado), 2),
                                PodeAlterar = indTemPermissaoAlterar,
                                PodeEstornarSaldoNfe = indTemPermissaoEstornoSaldoNfe,
                                Items = listaDespachosNfe.Select(d => new
                                                                        {
                                                                            d.CodigoVagao,
                                                                            d.ChaveCte,
                                                                            d.Conteiner,
                                                                            DataDespacho = d.DataDespacho.ToString("dd/MM/yyyy"),
                                                                            d.Destino,
                                                                            d.FluxoComercial,
                                                                            d.NumeroDespacho,
                                                                            d.Origem,
                                                                            d.SerieDespacho,
                                                                            d.Status,
                                                                            PesoUtilizado = d.PesoUtilizado.ToString()
                                                                        })
                            });
        }

        /// <summary>
        ///     Ação do controller para apresentação da View Autorizacao
        /// </summary>
        /// <returns>Action Result</returns>
        public ActionResult Autorizacao()
        {
            return this.View();
        }

        /// <summary>
        /// Altera o peso e volume de uma NFe
        /// </summary>
        /// <param name="chaveNfe">chave da nfe que sera alterada</param>
        /// <param name="novoPeso">novo peso de uma nfe</param>
        /// <param name="novoVolume">novo volume de uma nfe</param>
        /// <param name="novoPesoLiberar">Peso a liberar no saldo de uma nfe</param>
        /// <param name="novoVolumeLiberar">Volume a liberar no saldo de uma nfe</param>
        /// <param name="autorizadoPorEstorno">Nome da pessoa que autorizou o estorno de saldo da nfe</param>
        /// <param name="justificativaEstorno">Justificativa do estorno de saldo da nfe</param>
        /// <returns>json com a mensagem e sucesso da operacao</returns>
        public ActionResult SalvarAlteracoesNota(string chaveNfe, string novoPeso, string novoVolume, string novoPesoLiberar, string novoVolumeLiberar, bool teveLiberacaoSaldoNfe, string autorizadoPorEstorno, string justificativaEstorno)
        {
            try
            {
                var novoPesoConvertido = Convert.ToDouble(Convert.ToDecimal(novoPeso.Replace(".", ",")) * 1000);
                var novoVolumeConvertido = Convert.ToDouble(Convert.ToDecimal(novoVolume.Replace(".", ",")));
                var novoPesoLiberarConvertido = Convert.ToDouble(Convert.ToDecimal(novoPesoLiberar.Replace(".", ",")) * 1000);
                var novoVolumeLiberarConvertido = Convert.ToDouble(Convert.ToDecimal(novoVolumeLiberar.Replace(".", ",")));

                _nfeService.AlterarVolumePesoNfe(chaveNfe, novoVolumeConvertido, novoPesoConvertido, UsuarioAtual.Codigo, UsuarioAtual, novoVolumeLiberarConvertido, novoPesoLiberarConvertido, teveLiberacaoSaldoNfe, autorizadoPorEstorno, justificativaEstorno);
            }
            catch (Exception ex)
            {
                return Json(new { Sucesso = false, Mensagem = ex.Message });
            }

            return Json(new { Sucesso = true, Mensagem = "Nota alterada com sucesso!" });
        }

        /// <summary>
        /// Consulta de NFe alterada
        /// </summary>
        /// <returns>View com a consulta</returns>
        [Autorizar(Transacao = "ALTERACAONFE", Acao = "Pesquisar")]
        public ActionResult NotasAlteradas()
        {
            return View();
        }

        /// <summary>
        /// Consulta para a gri de NFe alterada
        /// </summary>
        /// <returns>View com a consulta</returns>
        [Autorizar(Transacao = "ALTERACAONFE", Acao = "Pesquisar")]
        public ActionResult ObterNotasAlteradas()
        {
            var result = _nfeService.ListarHistoricoNfe();
            return Json(new
            {
                result.Total,
                Items = result.Items.Select(e => new
                {
                    e.AlteracaoConferida,
                    e.ChaveNfe,
                    DataAlteracao = e.DataAlteracao.ToString(),
                    e.Id,
                    e.MatriculaUsuario,
                    NaturezaDaOperacao = e.NaturezaDaOperacao == NaturezaDaOperacaoEnum.Manual ? "Manual" : "Restaurado", // Enum<NaturezaDaOperacaoEnum>.GetDescriptionOf(e.NaturezaDaOperacao),
                    PesoAntigo = Math.Round(e.PesoAntigo ?? 0 / 1000, 2).ToString("0.00"),
                    PesoAtual = Math.Round(e.PesoAtual / 1000, 2).ToString("0.00"),
                    VolumeAntigo = (e.VolumeAntigo ?? 0).ToString("0.00"),
                    VolumeAtual = e.VolumeAtual.ToString("0.00")
                }),
                success = true
            });
        }

        /// <summary>
        /// Consulta para a gri de NFe alterada
        /// </summary>
        /// <param name="chaveNfe">Chave da Nota Fiscal</param>
        /// <returns>View com a consulta</returns>
        [Autorizar(Transacao = "ALTERACAONFE", Acao = "Exportar")]
        public ActionResult GerarPdf(string chaveNfe)
        {
            var returnContent = _relatorioDocumentosService.GerarPdf(new List<string> { chaveNfe });
            var fsr = new FileStreamResult(returnContent, "application/pdf");

            fsr.FileDownloadName = chaveNfe + ".pdf";
            return fsr;
        }

        /// <summary>
        /// Confere as Nfes selcionadas
        /// </summary>
        /// <param name="idsHistoricoNfe">array com os ids conferidos</param>
        /// <returns>resultado vazio</returns>
        [Autorizar(Transacao = "ALTERACAONFE", Acao = "Salvar")]
        public ActionResult ConferirAlteracaoNfe(int[] idsHistoricoNfe)
        {
            try
            {
                if (idsHistoricoNfe != null)
                {
                    foreach (var id in idsHistoricoNfe)
                    {
                        _nfeService.DarComoConferidoVolumePesoNfe(id);
                    }
                }
            }
            catch (Exception ex)
            {
                return Json(new { Sucesso = false, Mensagem = ex.Message });
            }

            return Json(new { Sucesso = true, Mensagem = "Alterações realizadas com sucesso!" });
        }



        /// <summary>
        /// Obtém os fluxos associados aos dados da nota fiscal (remetente e destinatario fiscal)
        /// </summary>
        /// <param name="chaveNfe"> Chave Nfe </param>
        /// <returns> Retorna lista de fluxos </returns>
        //[Autorizar(Transacao = "CTEPESQNFE", Acao = "PesquisarFluxosAssociacaoNfe")]
        public ActionResult PesquisarFluxosAssociadosChaveNfe(DetalhesPaginacaoWeb pagination,
                                                              string chavesNfe,
                                                              bool somenteVigentes,
                                                              bool exportarExcel)
        {
            if (String.IsNullOrEmpty(chavesNfe))
            {
                return Json(new
                {
                    Sucesso = false,
                    Mensagem = "Nenhuma chave de nota fiscal foi informada."
                });
            }

            var dadosNfes = BL_VwNfe.ObterPorChaveNfes(chavesNfe);

            if (dadosNfes == null || dadosNfes.Count == 0)
            {
                return Json(new
                {
                    Sucesso = false,
                    Mensagem = "Nenhuma nota localizada na base de dados."
                });
            }

            try
            {
                //IList<NotaFiscalFluxoAssociacaoDto> listaFluxosAssociadosNfe =
                //    _nfeService.ObterFluxosAssociadosChaveNfe(pagination,
                //                                              chavesNfe,
                //                                              somenteVigentes);

                var listaFluxosAssociadosNfe = BL_VwNfeFluxosAssociados.ObterFluxosAssociadosChaveNfe(chavesNfe, somenteVigentes);

                if (exportarExcel)
                {
                    var dic = new Dictionary<string, Func<Spd.Data.VwNfeFluxosAssociados, string>>();

                    dic["Chave Nfe"] = c => c.ChaveNfe;
                    dic["Fluxo"] = c => c.Fluxo;
                    dic["Vigência"] = c => c.FluxoVigenciaFinal.GetValueOrDefault().ToString("dd/MM/yyyy");
                    dic["Origem"] = c => c.Origem;
                    dic["Destino"] = c => c.Destino;
                    dic["CNPJ Remetente Fiscal"] = c => c.RemetenteFiscalCnpj;
                    dic["Remetente Fiscal"] = c => c.RemetenteFiscal;
                    dic["CNPJ Destinatário Fiscal"] = c => c.DestinatarioFiscalCnpj;
                    dic["Destinatário Fiscal"] = c => c.DestinatarioFiscal;
                    dic["CNPJ Tomador"] = c => c.TomadorCnpj;
                    dic["Tomador"] = c => c.Tomador;
                    dic["Expedidor"] = c => c.Expedidor;
                    dic["Recebedor"] = c => c.Recebedor;
                    dic["Mercadoria"] = c => c.Mercadoria;

                    return this.Excel(string.Format("ChaveNfeFluxosAssociados_{0}_{1}.xls", DateTime.Now.ToString("ddMMyyyy"), DateTime.Now.ToString("HHmmss")), dic, listaFluxosAssociadosNfe);
                }

                return Json(new
                {
                    listaFluxosAssociadosNfe.Count,
                    Items = listaFluxosAssociadosNfe.Select(e => new
                    {
                        ChaveNfe = e.ChaveNfe,
                        Fluxo = e.Fluxo,
                        Origem = e.Origem,
                        Destino = e.Destino,
                        RemetenteFiscalCnpj = _relatorioDocumentosService.FormatarCnpj(e.RemetenteFiscalCnpj),
                        RemetenteFiscal = e.RemetenteFiscal,
                        DestinatarioFiscalCnpj = _relatorioDocumentosService.FormatarCnpj(e.DestinatarioFiscalCnpj),
                        DestinatarioFiscal = e.DestinatarioFiscal,
                        TomadorCnpj = _relatorioDocumentosService.FormatarCnpj(e.TomadorCnpj),
                        Tomador = e.Tomador,
                        Expedidor = e.Expedidor,
                        Recebedor = e.Recebedor,
                        Mercadoria = e.Mercadoria,
                        FluxoVigenciaFinal = e.FluxoVigenciaFinal.GetValueOrDefault().ToString("dd/MM/yyyy")
                    }),
                    Sucesso = true
                });

            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Sucesso = false,
                    Mensagem = string.Format("Erro ao pesquisar por fluxos associados a Nfe informada. Tente novamente mais tarde.")
                });
            }
        }

        [HttpPost]
        public ActionResult ImprimirNfe(string chavesNfe)
        {
            try
            {
                if (String.IsNullOrEmpty(chavesNfe))
                    throw new Exception("Chave da Nota Fiscal não informada.");

                var result = _relatorioDocumentosService.PossuiNfePdf(chavesNfe);
                if (result == false)
                    throw new Exception("NF-e(s) não encontrada(s) no sistema.");

                return this.Json(new { Success = true });
            }
            catch (Exception ex)
            {
                return this.Json(new { Success = false, Message = ex.Message });
            }
        }

        [HttpGet]
        public virtual ActionResult DownloadPdfNfe(string chavesNfe)
        {
            var listaNfes = chavesNfe.ToUpper()
                                     .Trim()
                                     .Replace(" ", string.Empty)
                                     .Replace(",", ";")
                                     .Replace(".", ";")
                                     .Split(';')
                                     .Where(e => !string.IsNullOrWhiteSpace(e))
                                     .ToList();

            var result = _relatorioDocumentosService.GerarPdf(listaNfes);

            var memoryStream = result as MemoryStream;
            result.Seek(0, SeekOrigin.Begin);

            var fileName = String.Format("Nfes_{0}.pdf", DateTime.Now);

            FileStreamResult fsr = null;

            if (memoryStream != null)
            {
                fsr = new FileStreamResult(memoryStream, "application/pdf");
                fsr.FileDownloadName = fileName;
            }

            return fsr;
        }
    }
}
