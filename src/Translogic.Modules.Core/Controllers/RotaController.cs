﻿namespace Translogic.Modules.Core.Controllers.Operacao
{
    using System;
    using System.Linq;
    using System.Web.Mvc;
    using Translogic.Core.Infrastructure.Web;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais;
    using Translogic.Modules.Core.Domain.Model.Rota;
    using Translogic.Modules.Core.Domain.Services.Rota.Interface;

    public class RotaController : BaseSecureModuleController
    {
        private readonly IGrupoRotaService _grupoRotaService;


        public RotaController(IGrupoRotaService grupoRotaService)
        {
            _grupoRotaService = grupoRotaService;
        }

        public ActionResult Index()
        {
            return View();
        }

        #region GrupoRota

        public JsonResult BuscaGrupoRota(DetalhesPaginacaoWeb detalhesPaginacaoWeb, string dataInicial, string dataFinal)
        {
            ViewData["dataInicial"] = dataInicial;
            ViewData["dataFinal"] = dataFinal;

            var resultado = _grupoRotaService.ObterConsultaGrupoRota(detalhesPaginacaoWeb, dataInicial, dataFinal);
            var jsonData = Json(
                new
                {
                    Items = resultado.Items.Select(i => new
                    {
                        i.IdGrupo,
                        Grupo = i.Grupo,
                        Data = i.VersionDate != null ? i.VersionDate.ToString("dd/MM/yyyy") : string.Empty,
                        Usuario = i.Usuario
                    }),
                    Total = resultado.Total
                });
            return jsonData;
        }

        public ActionResult FormularioGrupoRota(string idGrupo, string grupo, string evento)
        {
            ViewData["idGrupo"] = idGrupo;
            ViewData["GrupoRota"] = grupo;
            ViewData["evento"] = evento;

            return View();
        }

        [HttpPost]
        public bool ConfirmaFormularioGrupo(GrupoRotaDto model)
        {
            var grupoRota = GetInstance(model);

            if (model.Evento.ToLower() != "excluir")
            {
                _grupoRotaService.Salvar(grupoRota);
            }
            else
            {
                if (model.IdGrupo == 0)
                    throw new Exception("Id para excluir não fornecido");

                _grupoRotaService.Excluir(Convert.ToInt32(model.IdGrupo));
            }
            return true;
        }

        private GrupoRota GetInstance(GrupoRotaDto model)
        {
            var grupoRota = _grupoRotaService.ObterInstancia(Convert.ToInt32(model.IdGrupo));
            grupoRota.Grupo = model.Grupo;
            grupoRota.Usuario = UsuarioAtual.Codigo;

            return grupoRota;
        }

        #endregion

        #region Associar

        public JsonResult ObterGrupoRotas()
        {
            var resultado = _grupoRotaService.ObterGrupoRotas();

            resultado.Insert(0, new GrupoRotaDto() { IdGrupo = 0, Grupo = "Todos" });

            var jsonData = Json(
                new
                {
                    Items = resultado.Select(i => new
                    {
                        IdGrupo = i.IdGrupo,
                        Grupo = i.Grupo
                    }),
                    Total = resultado.Count
                });
            return jsonData;
        }

        public JsonResult ObterRotas()
        {
            var resultado = _grupoRotaService.ObterRotas();

            resultado.Insert(0, new RotaGrupoRotaDto() { IdRota = 0, Codigo = "Todos" });

            var jsonData = Json(
                new
                {
                    Items = resultado.Select(i => new
                    {
                        IdRota = i.IdRota,
                        Descricao = i.Codigo 
                    }),
                    Total = resultado.Count
                });
            return jsonData;
        }

        public JsonResult BuscaAssociacoes(DetalhesPaginacaoWeb detalhesPaginacaoWeb, string dataInicial, string dataFinal, string grupoRota, string origem, string destino, string rota)
        {
            ViewData["dataInicial"] = dataInicial;
            ViewData["dataFinal"] = dataFinal;
             
            if (grupoRota == "Todos" || grupoRota == "0")
                grupoRota = "";

            if (rota == "Todos" || rota == "0")
                rota = "";

            var resultado = _grupoRotaService.ObterAssociacoes(detalhesPaginacaoWeb, dataInicial, dataFinal, grupoRota, origem, destino, rota);
            var jsonData = Json(
                new
                {
                    Items = resultado.Items.Select(i => new
                    {
                        Id = i.IdGrupoRotaRota,
                        i.IdGrupo,
                        i.Grupo,
                        i.IdRota,
                        i.Rota,
                        Data = i.VersionDate != null ? i.VersionDate.ToString("dd/MM/yyyy") : string.Empty,
                        i.Usuario
                    }),
                    Total = resultado.Total
                });
            return jsonData;
        }

        public ActionResult FormularioAssociar(string idGrupoRota, string idGrupo, string grupo, string idRota, string evento)
        {
            ViewData["idGrupoRota"] = idGrupoRota;
            ViewData["idGrupo"] = idGrupo;
            ViewData["Grupo"] = grupo;
            ViewData["idRota"] = idRota;
            ViewData["evento"] = evento;

            return View();
        }

        [HttpPost]
        public bool ConfirmaFormularioAssociar(GrupoRotaRotaDto model)
        {
            if (model.IdGrupo == 0)
                throw new Exception("Id do grupo não fornecido");

            if (model.IdRota == 0)
                throw new Exception("Id da rota não fornecido");

            if (model.Evento.ToLower() != "excluir")
            {
                var grupoRotaRota = _grupoRotaService.ObterInstanciaAssociar(model);
                grupoRotaRota.Usuario = UsuarioAtual.Codigo;
                _grupoRotaService.SalvarAssociacao(grupoRotaRota);
            }
            else
            {
                _grupoRotaService.ExcluirAssociar(Convert.ToInt32(model.IdGrupoRotaRota));
            }
            return true;
        }

        #endregion

        #region Limite
        public JsonResult BuscaLimites(DetalhesPaginacaoWeb detalhesPaginacaoWeb, string dataInicial, string dataFinal, string idGrupo, string frota, string idMercadoria)
        {
            ViewData["dataInicial"] = dataInicial;
            ViewData["dataFinal"] = dataFinal;

            if (idGrupo == "Todos" || idGrupo == "0")
                idGrupo = "";

            if (frota == "Todos" || frota == "0")
                frota = "";

            if (idMercadoria == "Todos" || idMercadoria == "0")
                idMercadoria = "";
            
            var resultado = _grupoRotaService.ObterLimites(detalhesPaginacaoWeb, dataInicial, dataFinal, idGrupo, frota, idMercadoria);
            var jsonData = Json(
                new
                {
                    Items = resultado.Items.Select(i => new
                    {
                        i.IdLimite,
                        i.IdGrupo,
                        i.Grupo,
                        i.IdFrota,
                        i.Frota,
                        i.IdMercadoria,
                        i.Mercadoria,
                        i.Peso,
                        i.Tolerancia,
                        Data = i.VersionDate != null ? i.VersionDate.ToString("dd/MM/yyyy") : string.Empty,
                        i.Usuario
                    }),
                    Total = resultado.Total
                });
            return jsonData;
        }

        public ActionResult FormularioLimite(string idLimite, string idGrupo, string idFrota, string idMercadoria, string peso, string evento)
        {
            ViewData["idLimite"] = idLimite;
            ViewData["idGrupo"] = idGrupo;
            ViewData["idFrota"] = idFrota;
            ViewData["idMercadoria"] = idMercadoria;
            ViewData["peso"] = peso;
            ViewData["evento"] = evento;

            return View();
        }

        private Limite GetLimiteInstance(LimiteDto model)
        {
            var limite = new Limite();

            if (model.IdLimite > 0)
                limite = _grupoRotaService.ObterLimite(Convert.ToInt32(model.IdLimite));

            limite.IdGrupo = Convert.ToInt32(model.IdGrupo);
            limite.IdFrota = Convert.ToInt32(model.IdFrota);
            limite.IdMercadoria = Convert.ToInt32(model.IdMercadoria);
            limite.Peso = model.Peso;
            limite.Usuario = UsuarioAtual.Codigo;

            return limite;

        }

        [HttpPost]
        public bool ConfirmaFormularioLimite(LimiteDto model)
        {
            var limite = GetLimiteInstance(model);

            if (model.Evento.ToLower() != "excluir")
            {
                _grupoRotaService.SalvarLimite(limite);
            }
            else
            {
                if (model.IdLimite == 0)
                    throw new Exception("Id para excluir não fornecido");

                _grupoRotaService.ExcluirLimite(limite);
            }
            return true;
        }

        public JsonResult ObtemFrotas()
        {

            var resultado = _grupoRotaService.ObtemFrotas();

            resultado.Insert(0, new FrotaDto() { IdFrota = 0, Descricao = "Todos" });

            var jsonData = Json(
                new
                {
                    Items = resultado.Select(i => new
                    {
                        i.IdFrota,
                        Descricao = i.Codigo + " - " + i.Descricao
                    }),
                    Total = resultado.Count
                });
            return jsonData;
        }

        public JsonResult ObterMercadorias()
        {
            var resultado = _grupoRotaService.ListMercadorias();

            resultado.Insert(0, new Mercadoria() { Id = 0, Codigo = "Todos" });

            var jsonData = Json(
                new
                {
                    Items = resultado.Select(i => new
                    {
                        IdMercadoria = i.Id,
                        Nome = i.Codigo == "Todos" ? "Todos" : i.Codigo + " - " + i.DescricaoResumida
                    }),
                    Total = resultado.Count
                });
            return jsonData;
        }

        #endregion

    }
}