namespace Translogic.Modules.Core.Controllers.Cte
{
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Web.Mvc;
	using ALL.Core.Dominio;
	using Domain.Model.Dto;
	using Domain.Services.Ctes;
	using Domain.Services.FluxosComerciais;
	using Infrastructure;
	using Translogic.Core.Infrastructure;
	using Translogic.Core.Infrastructure.Web;
	using Translogic.Core.Infrastructure.Web.Filters;
	using Translogic.Modules.Core.Domain.Model.Diversos.Cte;
	using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos;

    /// <summary>
    /// Controller Take or Pay Cte
    /// </summary>
    public class TakeOrPayCteController : BaseSecureModuleController
    {
        private readonly CteService _cteService;
		private readonly CarregamentoService _carregamentoService;

        /// <summary>
        /// Contrutor Defaut
        /// </summary>
        /// <param name="cteService">Service Cte</param>
        /// <param name="carregamentoService"> Service Carregamento </param>
        public TakeOrPayCteController(CteService cteService, CarregamentoService carregamentoService)
        {
        	_cteService = cteService;
        	_carregamentoService = carregamentoService;
        }

    	/// <summary>
        /// Acao Padrao ao ser chamado o controller
        /// </summary>
        /// <returns>View Padrao</returns>
		[Autorizar(Transacao = "CTETAKEORPAY")]
        public ActionResult Index()
        {
            return View();
        }

        /*

        /// <summary>
        /// Obtem uma lista de Ctes
        /// </summary>
        /// <param name="dataInicial">Data inicial de pesquisa</param>
        /// <param name="dataFinal">Data final de pesquisa</param>
        /// <param name="serie">N�mero da s�rie</param>
        /// <param name="despacho">N�mero do Despacho</param>
        /// <param name="fluxo">N�mero do Fluxo</param>
        /// <param name="serieCte">S�rie do CTE</param>
        /// <param name="nroCte">N�mero do CTE</param>
        /// <returns>Lista de Ctes</returns>
		[Autorizar(Transacao = "CTETAKEORPAY", Acao = "Pesquisar")]
        public JsonResult ObterCtes(DateTime? dataInicial, DateTime? dataFinal, int? serie, int? despacho, string fluxo, int? serieCte, int? nroCte)
        {
            DateTime dataIni = dataInicial.HasValue ? dataInicial.Value : DateTime.Now;
            DateTime dataFim = dataFinal.HasValue ? dataFinal.Value : DateTime.Now;
            int ser = serie.HasValue ? serie.Value : 0;
            int desp = despacho.HasValue ? despacho.Value : 0;
            int serCte = serieCte.HasValue ? serieCte.Value : 0;
            int numeroCte = nroCte.HasValue ? nroCte.Value : 0;

            IList<CteDto> result = _cteService.RetornaCteParaTakeOrPay(dataIni, dataFim, ser, desp, fluxo, serCte, numeroCte);

            return Json(new
            {
                Items = result.Select(c => new
                {
                    c.CteId,
                    c.Fluxo,
                    c.Origem,
                    c.Destino,
                    c.Mercadoria,
                    c.ClienteFatura,
                    c.Cte,
                    c.SerieDesp5,
                    c.NumDesp5,
                    c.SerieDesp6,
                    c.NumDesp6,
                    DateEmissao = c.DataEmissao.ToString("dd/MM/yyyy"),
                    c.Complementado,
                    c.ValorCte,
                    c.ValorDiferencaComplemento,
                    c.PesoVagao,
                    c.CodigoUsuario,
                    c.DataComplemento,
                    c.CteComAgrupamentoNaoAutorizado,
                    c.ValorTotal,
                    c.FluxoReferencia
                }),
                success = true
            });
        }

        */

		/// <summary>
		/// Obtem uma lista de Ctes
		/// </summary>
		/// <returns>Lista de Ctes do agrupamento </returns>
		[Autorizar(Transacao = "CTETAKEORPAY", Acao = "Pesquisar")]
		public JsonResult ObterCodigoSerieDesp()
		{
			IList<SerieDespachoUf> result = _carregamentoService.ObterCodigoSerie();

			// Inseri item default em branco no list
			var itemDefault = new SerieDespachoUf();
			itemDefault.CodigoControle = " ";

			result.Insert(0, itemDefault);

			return Json(new
			{
				Items = result.Select(c => new
				{
					c.Id,
					c.CodigoControle
				}),
				success = true
			});
		}

        /// <summary>
        /// Obtem uma lista de Ctes
        /// </summary>
        /// <param name="pagination"> Paginacao pesquisa</param>
        /// <param name="filter"> Filtros para pesquisa</param>
        /// <returns>Lista de Ctes</returns>
        [Autorizar(Transacao = "CTETAKEORPAY", Acao = "Pesquisar")]
        public JsonResult ObterCtes(DetalhesPaginacaoWeb pagination, DetalhesFiltro<object>[] filter)
        {
            ResultadoPaginado<CteDto> result = _cteService.RetornaCteParaTakeOrPay(pagination, filter);
            return Json(new
            {
                result.Total,
                Items = result.Items.Select(c => new
                {
                    c.CteId,
                    c.Fluxo,
                    c.Origem,
                    c.Destino,
                    c.Mercadoria,
                    c.ClienteFatura,
                    c.Cte,
                    c.ValorCte,
					Complementado = c.TipoCte == TipoCteEnum.Complementar,
                    c.ValorDiferencaComplemento,
                    c.PesoVagao,
                    Serie = c.SerieDesp6 == 0 && c.NumDesp6 == 0 ? c.SerieDesp5.ToString().PadLeft(3, '0') : string.Concat(c.CodigoControle, "-", c.SerieDesp6.ToString().PadLeft(3, '0')),
                    Despacho = c.SerieDesp6 == 0 && c.NumDesp6 == 0 ? c.NumDesp5.ToString().PadLeft(3, '0') : c.NumDesp6.ToString().PadLeft(3, '0'),
					SerieDesp5 = c.SerieDesp5 != null ? c.SerieDesp5.ToString().PadLeft(3, '0') : string.Empty,
					Desp5 = c.NumDesp5.ToString().PadLeft(3, '0'),
                    DateEmissao = c.DataEmissao.ToString("dd/MM/yyyy"),
                    DataComplemento = c.DataComplemento.HasValue ? c.DataEmissao.ToString("dd/MM/yyyy") : string.Empty,
                    c.CteComAgrupamentoNaoAutorizado,
                    c.CtePago,
                    c.ForaDoTempoCancelamento
                }),
                success = true
            });
        }

        /// <summary>
        /// Realiza o salvamento dos complementos dos Ctes Selecionados
        /// </summary>
        /// <param name="dtos">dtos dos ctes</param>
        /// <returns>resultado do salvamento</returns>
		[Autorizar(Transacao = "CTETAKEORPAY", Acao = "Salvar")]
        [JsonFilter(Param = "dtos", JsonDataType = typeof(CteDto[]) )]
        public JsonResult Salvar(CteDto[] dtos)
        {
            string mensagem;
            bool status = _cteService.SalvarComplementoTakeOrPayCtes(dtos, UsuarioAtual, out mensagem);

            if (status)
            {
                return Json(new { success = true, Message = mensagem });
            }
            else
            {
                return Json(new { success = false, Message = "N�o foi poss�vel concluir a a��o! </BR>" + mensagem });
            }
        }
    }
}
