﻿namespace Translogic.Modules.Core.Controllers.Cte
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using ALL.Core.Dominio;
    using Domain.Model.Dto;
    using Domain.Services.Ctes;
    using Infrastructure;
    using Translogic.Core.Infrastructure.Web;
    using Translogic.Modules.Core.Domain.Services.Mdfes;
    using Translogic.Modules.TPCCO.Helpers;

    /// <summary>
    /// Controller Cte Virtual Sem Vinculo com MDF-e
    /// </summary>
    public class CteVirtualSemVinculoController : BaseSecureModuleController
    {
        private readonly CteService _cteService;
        private readonly MdfeService _mdfeService;

        /// <summary>
        /// Contrutor Defaut
        /// </summary>
        /// <param name="cteService">Service Cte</param>
        /// <param name="carregamentoService">Servico de Carregamento</param>
        public CteVirtualSemVinculoController(CteService cteService, MdfeService mdfeService)
        {
            _cteService = cteService;
            _mdfeService = mdfeService;
        }

        /// <summary>
        /// Acao Padrao ao ser chamado o controller
        /// </summary>
        /// <returns>View Padrao</returns>
        [Autorizar(Transacao = "CTEVIRTUALSEMVINCULO")]
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Obtem uma lista de Ctes
        /// </summary>
        /// <param name="pagination"> Paginacao pesquisa</param>
        /// <param name="filter"> Filtros para pesquisa</param>
        /// <returns>Lista de Ctes</returns>
        [Autorizar(Transacao = "CTEVIRTUALSEMVINCULO", Acao = "Pesquisar")]
        public JsonResult ObterCtesVirtual(DetalhesPaginacaoWeb pagination, DetalhesFiltro<object>[] filter)
        {
            ResultadoPaginado<CteVirtuaisSemVinculoMdfeDto> result = _cteService.RetornaListaCteVirtuaisSemVinculoMdfe(pagination, filter, false);
            return Json(new
            {
                result.Total,
                Items = result.Items.OrderBy(x => x.COMPOSICAO).Select(c => new
                {
                    c.ID_MOVIMENTACAO,
                    c.ID_AREA_OPERACIONAL,
                    c.NUM_CTE_VIRTUAL,
                    c.SERIE_CTE_VIRTUAL,
                    DATA_EMISSAO_VIRTUAL = c.DATA_EMISSAO_VIRTUAL.ToString("dd/MM/yyyy"),
                    c.COMPOSICAO,
                    c.PREFIXO_TREM,
                    c.FLUXO,
                    c.ID_TREM,
                    c.ID_OS,
                    c.ID_DESPACHO,
                    c.VAGAO,
                    c.COD_FLUXO
                }),
                success = true
            });
        }

        /// <summary>
        /// Gera o excel conforme os parametros
        /// </summary>
        /// <param name="pagination"></param>
        /// <param name="dataInicial"></param>
        /// <param name="dataFinal"></param>
        /// <param name="fluxo"></param>
        /// <param name="chaveCte"></param>
        /// <param name="vagao"></param>
        /// <returns></returns>
        [Autorizar(Transacao = "CTEVIRTUALSEMVINCULO", Acao = "Pesquisar")]
        public ActionResult Exportar(DetalhesPaginacaoWeb pagination, string dataInicial, string dataFinal, string fluxo, string chaveCte, string vagao)
        {
            DateTime dataIni = DateTime.Now;
            DateTime dataFim = DateTime.Now;
            string codFluxo = string.Empty;
            string[] numVagao = null;
            string[] codChaveCte = null;

            if (!string.IsNullOrEmpty(dataInicial.ToString()))
            {
                dataIni = Convert.ToDateTime(dataInicial);
            }

            if (!string.IsNullOrEmpty(dataFinal.ToString()))
            {
                string texto = dataFinal;
                dataFim = Convert.ToDateTime(texto.ToString());
                dataFim = dataFim.AddDays(1).AddSeconds(-1);
            }

            if (!string.IsNullOrEmpty(fluxo.ToString()) && fluxo.ToUpper() != "NULL")
            {
                codFluxo = fluxo.ToString().ToUpperInvariant();
            }

            if (!string.IsNullOrEmpty(chaveCte.ToString()) && chaveCte.ToUpper() != "NULL")
            {
                codChaveCte = chaveCte.ToString().Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            }

            if (!string.IsNullOrEmpty(vagao.ToString()) && vagao.ToUpper() != "NULL")
            {
                numVagao = vagao.ToString().Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            }

            ResultadoPaginado<CteVirtuaisSemVinculoMdfeDto> result = _cteService.ListaCteVirtuaisSemVinculoMdfe(pagination, dataIni, dataFim, codFluxo, codChaveCte, numVagao, true);


            var colunas = new List<ExcelColuna<CteVirtuaisSemVinculoMdfeDto>>
                {
                    new ExcelColuna<CteVirtuaisSemVinculoMdfeDto>("Número do CT-e", c => c.NUM_CTE_VIRTUAL.ToString()),
                    new ExcelColuna<CteVirtuaisSemVinculoMdfeDto>("Série Cte Virtual", c => c.SERIE_CTE_VIRTUAL.ToString()),
                    new ExcelColuna<CteVirtuaisSemVinculoMdfeDto>("Data Emissão", c => c.DATA_EMISSAO_VIRTUAL.ToString("dd/MM/yyyy")),
                    new ExcelColuna<CteVirtuaisSemVinculoMdfeDto>("Composição", c => c.COMPOSICAO.ToString()),
                    new ExcelColuna<CteVirtuaisSemVinculoMdfeDto>("Prefixo Trem", c => c.PREFIXO_TREM.ToString()),
                    new ExcelColuna<CteVirtuaisSemVinculoMdfeDto>("Fluxo", c => c.COD_FLUXO.ToString()),
                    new ExcelColuna<CteVirtuaisSemVinculoMdfeDto>("Vagão", c => c.VAGAO.ToString())
                };

            return ExcelHelper.Exportar(
                    string.Format("CtesVirtuaisSemVinculo_{0}_{1}.xls", DateTime.Now.ToString("ddMMyyyy"), DateTime.Now.ToString("HHmmss")),
                    colunas,
                    result.Items,
                    ws =>
                    {
                        ws.Cells.AutoFitColumns();
                    });
        }


        /// <summary>
        /// Retorna a tela para informar os códigos dos vagões
        /// </summary>
        /// <returns>Action result com a tela</returns>
        public ActionResult FormInformarVagoes()
        {
            return View();
        }

        /// <summary>
        /// Retorna a tela para informar as chaves nfe
        /// </summary>
        /// <returns>Action result com a tela</returns>
        public ActionResult FormInformarChaves()
        {
            return View();
        }

        /// <summary>
        /// Retorna se as chaves sao validas
        /// </summary>
        /// <param name="listaChave">Lista de chaves</param>
        /// <returns>Action result com a tela</returns>
        public ActionResult ValidarChaves(List<string> listaChave)
        {
            Dictionary<string, KeyValuePair<bool, string>> result = _cteService.VerificarCteFolhaArvorePorListaChave(listaChave);

            return Json(new
            {
                Items = result.Select(c => new
                {
                    Chave = c.Key,
                    Erro = c.Value.Key,
                    Mensagem = c.Value.Value
                }),
                success = true
            });
        }

        /// <summary>
        /// Retorna se os vagões estão válidos
        /// </summary>
        /// <param name="listaVagoes">Lista de vagões</param>
        /// <returns>Action result com a tela</returns>
        public ActionResult ValidarVagoes(List<string> listaVagoes)
        {
            Dictionary<string, KeyValuePair<bool, string>> result = _cteService.VerificarListaVagoes(listaVagoes);

            return Json(new
            {
                Items = result.Select(c => new
                {
                    Vagao = c.Key,
                    Erro = c.Value.Key,
                    Mensagem = c.Value.Value
                }),
                success = true
            });
        }

        /// <summary>
        /// Força o envio do virtual para gerar o MDFe com valores do mdfe pai
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [Autorizar(Transacao = "CTEVIRTUALSEMVINCULO", Acao = "GerarDadosMdfe")]
        public JsonResult GerarDadosMdfe(List<CteVirtuaisSemVinculoMdfeDto> dto)
        {
            var result = _mdfeService.GerarMdfeVirtualSemVinculo(dto, UsuarioAtual);

            return Json(new
            {
                Items = result.Select(c => new
                {
                    Vagao = c.Key,
                    Erro = c.Value.Key,
                    Mensagem = c.Value.Value
                }),
                success = true
            });

        }
    }
}
