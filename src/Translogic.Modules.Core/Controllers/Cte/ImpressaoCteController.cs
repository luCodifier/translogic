namespace Translogic.Modules.Core.Controllers.Cte
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using Domain.Model.Dto;
    using Domain.Services.Ctes;
    using Domain.Services.FluxosComerciais;
    using Infrastructure;
    using Translogic.Core.Infrastructure;
    using Translogic.Core.Infrastructure.Web.Filters;

    /// <summary>
    /// Controller Impress�o Cte
    /// </summary>
    public class ImpressaoCteController : BaseSecureModuleController
    {
        private readonly CteService _cteService;

        /// <summary>
        /// Contrutor Defaut
        /// </summary>
        /// <param name="cteService">Service Cte</param>
        public ImpressaoCteController(CteService cteService)
        {
            _cteService = cteService;
        }

        /// <summary>
        /// Acao Padrao ao ser chamado o controller
        /// </summary>
        /// <returns>View Padrao</returns>
        [Autorizar(Transacao = "CTEIMPRESSAO")]
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Obtem uma lista de Ctes
        /// </summary>
        /// <param name="dataInicial">Data inicial de pesquisa</param>
        /// <param name="dataFinal">Data final de pesquisa</param>
        /// <param name="serie">N�mero da s�rie</param>
        /// <param name="despacho">N�mero do Despacho</param>
        /// <param name="fluxo">N�mero do Fluxo</param>
        /// <param name="estacao">Esta��o de Origem</param>
        /// <param name="nroCte">n�mero do cte</param>
        /// <param name="chave">chave do cte</param>
        /// <param name="impresso">op��o de j� impresso ou n�o</param>
        /// <returns>Lista de Ctes</returns>
        [Autorizar(Transacao = "CTEIMPRESSAO", Acao = "Pesquisar")]
        public JsonResult ObterCtes(DateTime? dataInicial, DateTime? dataFinal, int? serie, int? despacho, string fluxo, string estacao, int? nroCte, string chave, string impresso)
        {
            DateTime dataIni = dataInicial.HasValue ? dataInicial.Value : DateTime.Now;
            DateTime dataFim = dataFinal.HasValue ? dataFinal.Value : DateTime.Now;
            int ser = serie.HasValue ? serie.Value : 0;
            int desp = despacho.HasValue ? despacho.Value : 0;
            int numeroCte = nroCte.HasValue ? nroCte.Value : 0;

            bool isImpresso = false;
            if (!string.IsNullOrEmpty(impresso))
            {
                isImpresso = impresso == "on" ? true : false;
            }

            IList<CteDto> result = _cteService.RetornaCteImpressao(dataIni, dataFim, ser, desp, chave, estacao, numeroCte, fluxo, isImpresso);

            return Json(new
            {
                Items = result.Select(c => new
                {
                    c.CteId,
                    c.Fluxo,
                    c.Origem,
                    c.Cte,
                    c.Chave,
                    c.SerieDesp5,
                    c.NumDesp5,
                    c.SerieDesp6,
                    c.NumDesp6,
                    DateEmissao = c.DataEmissao.ToString("dd/MM/yyyy"),
                    Impresso = c.Impresso == true ? "Impresso" : "N�o Impresso"
                }),
                success = true
            });
        }

        /// <summary>
        /// Realiza o Cancelamento dos Ctes Selecionados
        /// </summary>
        /// <param name="ids">ids dos ctes</param>
        /// <returns>resultado do cancelamento</returns>
        [Autorizar(Transacao = "CTEIMPRESSAO", Acao = "Imprimir")]
        [JsonFilter(Param = "ids", JsonDataType = typeof(int[]))]
        public JsonResult Imprimir(int[] ids)
        {
            bool status = false; // _cteService.ImprimirCtes(ids);

            if (status)
            {
                return Json(new { success = true, Message = "SUCESSO" });
            }
            else
            {
                return Json(new { success = false, Message = "FALHA" });
            }
        }
    }
}
