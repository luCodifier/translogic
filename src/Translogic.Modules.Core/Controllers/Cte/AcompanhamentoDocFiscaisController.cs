﻿namespace Translogic.Modules.Core.Controllers.Cte
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Web.Mvc;
    using ALL.Core.Dominio;
    using Translogic.Core.Infrastructure.FileSystem.ExtensionMethods;
    using Translogic.Core.Infrastructure.Web;
    using Translogic.Core.Infrastructure.Web.ContentResults;
    using Translogic.Core.Infrastructure.Web.Filters;
    using Translogic.Modules.Core.Domain.Model.Diversos;
    using Translogic.Modules.Core.Domain.Model.Diversos.Cte;
    using Translogic.Modules.Core.Domain.Model.Diversos.Mdfe;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Modules.Core.Domain.Model.Estrutura;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes.Repositories;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Nfes;
    using Translogic.Modules.Core.Domain.Services.Ctes;
    using Translogic.Modules.Core.Domain.Services.FluxosComerciais;
    using Translogic.Modules.Core.Domain.Services.Mdfes;
    using Translogic.Modules.Core.Infrastructure;

    /// <summary>
    /// Controller AcompanhamentoDocFiscaisController
    /// </summary>
    public class AcompanhamentoDocFiscaisController : BaseSecureModuleController
    {
        private readonly CteService _cteService;
        private readonly MdfeService _mdfeService;
        private readonly NfeService _nfeService;
        private readonly ICteComplementadoRepository _cteComplementadoRepository;
        private readonly ICteRepository _cteRepository;
        private readonly IMdfeRepository _mdfeRepository;

        /// <summary>
        /// Construtor default
        /// </summary>
        /// <param name="cteService">Servico de Cte</param>
        /// <param name="cteComplementadoRepository">Repositório do CteComplementado</param>
        /// <param name="cteRepository">Repositório de Cte</param>
        /// <param name="mdfeRepository">Repositório de Mdfe</param>
        /// <param name="mdfeService">Servico de Mdfe</param>
        /// <param name="nfeService">Servico de Nfe</param>
        public AcompanhamentoDocFiscaisController(CteService cteService, ICteComplementadoRepository cteComplementadoRepository, ICteRepository cteRepository, IMdfeRepository mdfeRepository, MdfeService mdfeService, NfeService nfeService)
        {
            _cteService = cteService;
            _cteComplementadoRepository = cteComplementadoRepository;
            _cteRepository = cteRepository;
            _mdfeRepository = mdfeRepository;
            _mdfeService = mdfeService;
            _nfeService = nfeService;
        }

        /// <summary>
        /// ActionResult Index
        /// </summary>
        /// <returns>View Index</returns>
        [Autorizar(Transacao = "ADFISCAIS")]
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// ActionResult MonitoramentoSimplificado
        /// </summary>
        /// <returns>View Index</returns>
        [Autorizar(Transacao = "ADFISCAIS")]
        public ActionResult AcompanhamentoSimplificado()
        {
            return View();
        }

        /// <summary>
        /// Obtem as UFs disponíveis para filtrar
        /// </summary>
        /// <returns>Lista de Ufs para popular a combo</returns>
        [Autorizar(Transacao = "ADFISCAIS", Acao = "Pesquisar")]
        public JsonResult ObterUFs()
        {
            var result = _cteService.ObterUFs();

            return Json(new
            {
                Items = result.Select(c => new
                {
                    c.Uf
                }),
                success = true
            });
        }

        /// <summary>
        /// Retorna a tela para informar as chaves nfe
        /// </summary>
        /// <returns>Action result com a tela</returns>
        [Autorizar(Transacao = "ADFISCAIS", Acao = "Pesquisar")]
        public ActionResult FormInformarChaves()
        {
            return View();
        }

        /// <summary>
        /// Retorna se as chaves sao validas
        /// </summary>
        /// <param name="listaChave">Lista de chaves</param>
        /// <returns>Action result com a tela</returns>
        [Autorizar(Transacao = "ADFISCAIS", Acao = "Pesquisar")]
        public ActionResult ValidarChaves(List<string> listaChave)
        {
            Dictionary<string, KeyValuePair<bool, string>> result = _cteService.VerificarCtePorListaChave(listaChave, false);

            return Json(new
            {
                Items = result.Select(c => new
                {
                    Chave = c.Key,
                    Erro = c.Value.Key,
                    Mensagem = c.Value.Value
                }),
                success = true
            });
        }

        /// <summary>
        /// Obtem as notas do Cte
        /// </summary>
        /// <param name="pagination">Detalhes de paginação da Grid</param>
        /// <param name="idDespacho">Id do Despacho</param>
        /// <returns>Resultado Cte Selecionado</returns>
        [Autorizar(Transacao = "ADFISCAIS", Acao = "Pesquisar")]
        public JsonResult ObterNota(DetalhesPaginacaoWeb pagination, int idDespacho)
        {
            if (idDespacho > 0)
            {
                IList<object> lista = new List<object>();

                var dadosNfe = _nfeService.ObterDadosNotaFiscal(idDespacho);

                if (dadosNfe == null || dadosNfe.Count <= 0)
                {
                    return Json(new
                    {
                        Success = false,
                        Message = string.Format("Nota não localizada na base de dados. Favor baixar a nota, utilizando a tela 1154 (Carregamento de NF-e)"),
                        Items = lista
                    });
                }

                foreach (var notaFiscalDto in dadosNfe)
                {
                    string strCnpjRemetente = string.Empty;
                    long cnpjRemetente;
                    if (Int64.TryParse(notaFiscalDto.CnpjEmitente, out cnpjRemetente))
                    {
                        strCnpjRemetente = cnpjRemetente.ToString(@"00\.000\.000\/0000\-00");
                    }

                    var obj = new
                    {
                        notaFiscalDto.Id,
                        notaFiscalDto.TipoDocumento,
                        notaFiscalDto.SerieNotaFiscal,
                        notaFiscalDto.NumeroNotaFiscal,
                        notaFiscalDto.ChaveNfe,
                        notaFiscalDto.PesoRateio,
                        notaFiscalDto.ValorNotaFiscal,
                        notaFiscalDto.ValorTotalNotaFiscal,
                        CnpjRemetente = strCnpjRemetente,
                        notaFiscalDto.PesoTotal,
                        notaFiscalDto.Conteiner
                    };
                    lista.Add(obj);
                }

                return Json(new
                {
                    Items = lista,
                    success = true
                });
            }

            return Json(new
            {
                Items = (object)null,
                success = false,
                Message = "Não foi possível localizar a Nota Fiscal"
            });
        }

        /// <summary>
        /// Obtém o semáforo de ctes
        /// </summary>
        /// <returns>Retorna a lista de Ctes para o Semáforo</returns>
        [Autorizar(Transacao = "ADFISCAIS", Acao = "Pesquisar")]
        public JsonResult ObterSemaforoCte()
        {
            var listCtes = _cteService.RetornaListaCtesErro();
            return Json(new
            {
                SemaforoCtes = listCtes.Select(c => new
                {
                    c.Uf,
                    ZeroDoze = c.QtdZeroDozeHoras,
                    DozeVinteQuatro = c.QtdDozeVinteQuatroHoras,
                    MaisQueVinteCinco = c.QtdMaisQueVinteQuatroHoras
                })
            });
        }

        /// <summary>
        /// Obtém o semáforo de ctes
        /// </summary>
        /// <returns>Retorna a lista de Ctes para o Semáforo</returns>
        [Autorizar(Transacao = "ADFISCAIS", Acao = "Pesquisar")]
        public JsonResult ObterSemaforoMdfe()
        {
            var listMdfes = _mdfeService.RetornaListaMdfesErro();
            return Json(new
            {
                SemaforoMdfes = listMdfes.Select(m => new
                {
                    m.Uf,
                    ZeroDoze = m.QtdZeroDozeHoras,
                    DozeVinteQuatro = m.QtdDozeVinteQuatroHoras,
                    MaisQueVinteCinco = m.QtdMaisQueVinteQuatroHoras
                })
            });
        }

        /// <summary>
        /// Pesquisa das MDFes
        /// </summary>
        /// <param name="pagination">detalhes da paginacao</param>
        /// <param name="filter">informações dos filtros</param>
        /// <returns>resultado json</returns>
        [Autorizar(Transacao = "ADFISCAIS", Acao = "Pesquisar")]
        public ActionResult Pesquisar(DetalhesPaginacaoWeb pagination, DetalhesFiltro<object>[] filter)
        {
            var dataInicial = filter
                .Where(e => e.Campo == "dataInicial" && !string.IsNullOrWhiteSpace(e.Valor[0].ToString()))
                .Select(e => e == null ? (DateTime?)null : DateTime.ParseExact(e.Valor[0].ToString(), "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture))
                .FirstOrDefault();

            var dataFinal = filter
                .Where(e => e.Campo == "dataFinal" && !string.IsNullOrWhiteSpace(e.Valor[0].ToString()))
                .Select(e => e == null ? (DateTime?)null : DateTime.ParseExact(e.Valor[0].ToString(), "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture).AddDays(1).AddSeconds(-1))
                .FirstOrDefault();

            var chave = filter
                .Where(e => e.Campo == "chave" && !string.IsNullOrWhiteSpace(e.Valor[0].ToString()))
                .Select(e => e == null ? (string)null : e.Valor[0].ToString().ToUpperInvariant())
                .FirstOrDefault();

            var prefixo = filter
                .Where(e => e.Campo == "prefixo" && !string.IsNullOrWhiteSpace(e.Valor[0].ToString()))
                .Select(e => e == null ? (string)null : e.Valor[0].ToString().ToUpperInvariant())
                .FirstOrDefault();

            var os = filter
                .Where(e => e.Campo == "os" && !string.IsNullOrWhiteSpace(e.Valor[0].ToString()))
                .Select(e => e == null ? (int?)null : int.Parse(e.Valor[0].ToString(), CultureInfo.InvariantCulture))
                .FirstOrDefault();

            var origem = filter
                .Where(e => e.Campo == "origem" && !string.IsNullOrWhiteSpace(e.Valor[0].ToString()))
                .Select(e => e == null ? (string)null : e.Valor[0].ToString().ToUpperInvariant())
                .FirstOrDefault();

            var destino = filter
                .Where(e => e.Campo == "destino" && !string.IsNullOrWhiteSpace(e.Valor[0].ToString()))
                .Select(e => e == null ? (string)null : e.Valor[0].ToString().ToUpperInvariant())
                .FirstOrDefault();

            var idTrem = filter
                .Where(e => e.Campo == "idTrem" && !string.IsNullOrWhiteSpace(e.Valor[0].ToString()))
                .Select(e => e == null ? (int?)null : int.Parse(e.Valor[0].ToString(), CultureInfo.InvariantCulture))
                .FirstOrDefault();

            var idDespacho = filter
                .Where(e => e.Campo == "idDespacho" && !string.IsNullOrWhiteSpace(e.Valor[0].ToString()))
                .Select(e => e == null ? 0 : int.Parse(e.Valor[0].ToString(), CultureInfo.InvariantCulture))
                .FirstOrDefault();

            var result = idDespacho > 0 ? _mdfeService.BuscarTremPorDespachoId(pagination, idDespacho) : _mdfeService.BuscarTremPorFiltros(pagination, prefixo, os, dataInicial, dataFinal, origem, destino, chave, idTrem);

            return Json(new
            {
                result.Total,
                Items = result.Items.Select(e => new
                {
                    e.Id,
                    e.Prefixo,
                    e.Origem,
                    e.Destino,
                    e.OS
                })
            });
        }

        /// <summary>
        /// Pesquisa vagões de acordo com os filtros informados na tela
        /// </summary>
        /// <param name="pagination">detalhes da paginacao</param>
        /// <param name="filter">informações dos filtros</param>
        /// <returns>resultado json</returns>
        [Autorizar(Transacao = "ADFISCAIS", Acao = "Pesquisar")]
        public ActionResult ObterVagoesPorFiltro(DetalhesPaginacaoWeb pagination, DetalhesFiltro<object>[] filter)
        {
            var dataInicial = filter
                .Where(e => e.Campo == "dataInicial" && !string.IsNullOrWhiteSpace(e.Valor[0].ToString()))
                .Select(e => e == null ? (DateTime?)null : DateTime.ParseExact(e.Valor[0].ToString(), "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture))
                .FirstOrDefault();

            var dataFinal = filter
                .Where(e => e.Campo == "dataFinal" && !string.IsNullOrWhiteSpace(e.Valor[0].ToString()))
                .Select(e => e == null ? (DateTime?)null : DateTime.ParseExact(e.Valor[0].ToString(), "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture).AddDays(1).AddSeconds(-1))
                .FirstOrDefault();

            var chaveNfe = filter
                .Where(e => e.Campo == "chaveNfe" && !string.IsNullOrWhiteSpace(e.Valor[0].ToString()))
                .Select(e => e == null ? (string)null : e.Valor[0].ToString().ToUpperInvariant())
                .FirstOrDefault();

            var fluxo = filter
                .Where(e => e.Campo == "fluxo" && !string.IsNullOrWhiteSpace(e.Valor[0].ToString()))
                .Select(e => e == null ? (string)null : e.Valor[0].ToString().ToUpperInvariant())
                .FirstOrDefault();

            var siglaUfFerrovia = filter
                .Where(e => e.Campo == "uf" && !string.IsNullOrWhiteSpace(e.Valor[0].ToString()))
                .Select(e => e == null ? (string)null : e.Valor[0].ToString().ToUpperInvariant())
                .FirstOrDefault();

            var origem = filter
                .Where(e => e.Campo == "origem" && !string.IsNullOrWhiteSpace(e.Valor[0].ToString()))
                .Select(e => e == null ? (string)null : e.Valor[0].ToString().ToUpperInvariant())
                .FirstOrDefault();

            var destino = filter
                .Where(e => e.Campo == "destino" && !string.IsNullOrWhiteSpace(e.Valor[0].ToString()))
                .Select(e => e == null ? (string)null : e.Valor[0].ToString().ToUpperInvariant())
                .FirstOrDefault();

            int serieDesp = filter
                .Where(e => e.Campo == "serieDesp" && !string.IsNullOrWhiteSpace(e.Valor[0].ToString()))
                .Select(e => e == null ? 0 : int.Parse(e.Valor[0].ToString(), CultureInfo.InvariantCulture))
                .FirstOrDefault();

            int numeroDesp = filter
                .Where(e => e.Campo == "numeroDesp" && !string.IsNullOrWhiteSpace(e.Valor[0].ToString()))
                .Select(e => e == null ? 0 : int.Parse(e.Valor[0].ToString(), CultureInfo.InvariantCulture))
                .FirstOrDefault();

            var chavesCte = filter
                .Where(e => e.Campo == "chaveCte" && !string.IsNullOrWhiteSpace(e.Valor[0].ToString()))
                .Select(e => e == null ? (string)null : e.Valor[0].ToString().ToUpperInvariant());

            var chaveCte = chavesCte.FirstOrDefault() != null ? chavesCte.FirstOrDefault().Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries) : (string[])null;

            var codVagao = filter
                .Where(e => e.Campo == "codVagao" && !string.IsNullOrWhiteSpace(e.Valor[0].ToString()))
                .Select(e => e == null ? (string)null : e.Valor[0].ToString().ToUpperInvariant())
                .FirstOrDefault();

            var idMdfe = filter
                .Where(e => e.Campo == "idMdfe" && !string.IsNullOrWhiteSpace(e.Valor[0].ToString()))
                .Select(e => e == null ? 0 : int.Parse(e.Valor[0].ToString(), CultureInfo.InvariantCulture))
                .FirstOrDefault();

            var idTrem = filter
                .Where(e => e.Campo == "idTrem" && !string.IsNullOrWhiteSpace(e.Valor[0].ToString()))
                .Select(e => e == null ? 0 : int.Parse(e.Valor[0].ToString(), CultureInfo.InvariantCulture))
                .FirstOrDefault();

            var semaforo = filter
                .Where(e => e.Campo == "semaforo" && !string.IsNullOrWhiteSpace(e.Valor[0].ToString()))
                .Select(e => e == null ? (string)null : e.Valor[0].ToString().ToUpperInvariant())
                .FirstOrDefault();

            var siglaUf = filter
                .Where(e => e.Campo == "UF" && !string.IsNullOrWhiteSpace(e.Valor[0].ToString()))
                .Select(e => e == null ? (string)null : e.Valor[0].ToString().ToUpperInvariant())
                .FirstOrDefault();

            var periodoHoras = filter
                .Where(e => e.Campo == "periodoHoras" && !string.IsNullOrWhiteSpace(e.Valor[0].ToString()))
                .Select(e => e == null ? HorasErroEnum.TodasAsHoras : ALL.Core.Util.Enum<HorasErroEnum>.From(e.Valor[0].ToString().ToUpperInvariant()))
                .FirstOrDefault();

            var tipoCte = filter
                .Where(e => e.Campo == "tipoCte" && !string.IsNullOrWhiteSpace(e.Valor[0].ToString()))
                .Select(e => e == null ? (string)null : e.Valor[0].ToString().ToUpperInvariant())
                .FirstOrDefault();

            ResultadoPaginado<CteDto> result = null;

            if (idTrem > 0)
            {
                result = _cteService.ObterComposicaoPorTrem(pagination, idTrem);
            }
            else if (idMdfe > 0)
            {
                result = _cteService.ObterComposicaoPorMdfe(pagination, idMdfe);
            }
            else if (!String.IsNullOrWhiteSpace(semaforo))
            {
                result = _cteService.ObterComposicaoPorSemaforo(pagination, semaforo, siglaUf, periodoHoras);
            }
            else
            {
                result = _cteService.ObterComposicoesFiltros(pagination, dataInicial.Value, dataFinal.Value, chaveNfe, fluxo, siglaUfFerrovia, origem, destino, serieDesp, numeroDesp, chaveCte, codVagao, tipoCte);   
            }

            return Json(new
            {
                result.Total,
                Items = result.Items.Select(e => new
                {
                    e.IdCte,
                    Status = e.SituacaoCteStr,
                    Vagao = e.CodigoVagao,
                    e.Fluxo,
                    SerieDesp = e.SerieDespacho,
                    NumeroDesp = e.NumeroDespacho,
                    e.IdDespacho,
                    Serie = e.SerieCte,
                    e.Numero,
                    e.Chave,
                    DataEmissao = e.DataEmissao.ToString("dd/MM/yyyy HH:mm"),
                    DataCarregamento = e.DataCarregamento.ToString("dd/MM/yyyy HH:mm"),
                    Baldeio = e.HouveBaldeio
                }).Distinct()
            });            
        }

        private string ultimoStatusSefaz;
        /// <summary>
        /// Obtém o último status sefaz
        /// </summary>
        /// <param name="cteStatusRetorno">Objeto status de retorno do cte</param>
        /// <returns>Retorna a descrição do último status sefaz</returns>
        public string ObterUltimoStatusSefaz(CteStatusRetorno cteStatusRetorno)
        {
            if (cteStatusRetorno != null)
            {
                if (cteStatusRetorno.Id == 13 || cteStatusRetorno.Id == 15 || cteStatusRetorno.Id == 100 || cteStatusRetorno.Id == 301 || cteStatusRetorno.Id == 302 || cteStatusRetorno.Id == 303 || cteStatusRetorno.Id == 304 || cteStatusRetorno.Id == 305 || cteStatusRetorno.Id == 306)
                {
                    ultimoStatusSefaz = cteStatusRetorno.Descricao;
                }
            }
            
            return ultimoStatusSefaz;
        }

        /// <summary>
        /// Obtém a lista de status de uam cte de acordo com o Id da cte selecionada na grid de vagões
        /// </summary>
        /// <param name="filter">Filtro com o id da cte selecionada</param>
        /// <returns>Lista de status da Cte</returns>
        [Autorizar(Transacao = "ADFISCAIS", Acao = "Pesquisar")]
        public ActionResult ObterStatusCtePorIdCte(DetalhesFiltro<object>[] filter)
        {
            var idCte = filter
                .Where(e => e.Campo == "idCte" && !string.IsNullOrWhiteSpace(e.Valor[0].ToString()))
                .Select(e => e == null ? 0 : int.Parse(e.Valor[0].ToString(), CultureInfo.InvariantCulture))
                .FirstOrDefault();

            var result =
                _cteService.ObterStatusCtePorIdCte(idCte);

            ultimoStatusSefaz = string.Empty;

            return Json(new
            {
                Total = result.Count,
                Items = result.Select(e => new
                {
                    DataHora = e.DataHora.ToString("dd/MM/yyyy hh:mm"),
                    StatusCte = e.CteStatusRetorno.Descricao,
                    StatusSefaz = ObterUltimoStatusSefaz(e.CteStatusRetorno),
                    Usuario = e.Usuario.Nome,
                    Procedimentos = e.CteStatusRetorno != null ? _cteService.ObterProcedimentosPorCteStatusRetorno(e.CteStatusRetorno.Id.Value).
                    Select(p => new
                            {
                                p.LinkProcedimento,
                                p.NomeProcedimento,
                                p.ResponsavelProcedimento
                            }) : null
                })
            });
        }

        /// <summary>
        /// Ação do controller detalhes
        /// </summary>
        /// <param name="pagination">Detalhes da paginacao</param>
        /// <param name="filter">Filtros para obter os dados necessários</param>
        /// <returns>Action Result</returns>
        [Autorizar(Transacao = "ADFISCAIS", Acao = "Pesquisar")]
        public ActionResult DetalhesMdfe(DetalhesPaginacaoWeb pagination, DetalhesFiltro<object>[] filter)
        {
            int idTrem = filter
                .Where(e => e.Campo == "idTrem" && !string.IsNullOrWhiteSpace(e.Valor[0].ToString()))
                .Select(e => e == null ? 0 : int.Parse(e.Valor[0].ToString(), CultureInfo.InvariantCulture))
                .FirstOrDefault();

            var semaforo = filter
                .Where(e => e.Campo == "semaforo" && !string.IsNullOrWhiteSpace(e.Valor[0].ToString()))
                .Select(e => e == null ? (string)null : e.Valor[0].ToString().ToUpperInvariant())
                .FirstOrDefault();

            var siglaUf = filter
                .Where(e => e.Campo == "UF" && !string.IsNullOrWhiteSpace(e.Valor[0].ToString()))
                .Select(e => e == null ? (string)null : e.Valor[0].ToString().ToUpperInvariant())
                .FirstOrDefault();

            var periodoHoras = filter
                .Where(e => e.Campo == "periodoHoras" && !string.IsNullOrWhiteSpace(e.Valor[0].ToString()))
                .Select(e => e == null ? HorasErroEnum.TodasAsHoras : ALL.Core.Util.Enum<HorasErroEnum>.From(e.Valor[0].ToString().ToUpperInvariant()))
                .FirstOrDefault();

            int idDespacho = filter
                .Where(e => e.Campo == "idDespacho" && !string.IsNullOrWhiteSpace(e.Valor[0].ToString()))
                .Select(e => e == null ? 0 : int.Parse(e.Valor[0].ToString(), CultureInfo.InvariantCulture))
                .FirstOrDefault();

            var result = idDespacho > 0 ? _mdfeService.ListarPorDespacho(pagination, idDespacho) : _mdfeService.ListarDetalhesDaMdfe(pagination, idTrem, semaforo, siglaUf, periodoHoras);

            return Json(new
            {
                result.Total,
                Items = result.Items.Select(e => new
                {
                    e.Id,
                    e.TremId,
                    e.Chave,
                    e.Serie,
                    e.Numero,
                    SituacaoAtual = ALL.Core.Util.Enum<SituacaoMdfeEnum>.GetDescriptionOf(e.SituacaoAtual),
                    e.PdfGerado,
                    DataHora = e.DataHora.ToString("dd/MM/yyyy hh:mm")
                })
            });   
        }

        /// <summary>
        /// Ação do controller detalhes
        /// </summary>
        /// <param name="idMdfe">id da Mdfe</param>
        /// <returns>Action Result</returns>
        [Autorizar(Transacao = "ADFISCAIS", Acao = "Imprimir")]
        public ActionResult Imprimir(int idMdfe)
        {
            try
            {
                Stream returnContent = _mdfeService.GerarPdf(idMdfe);
                FileStreamResult fsr = new FileStreamResult(returnContent, "application/pdf");
                fsr.FileDownloadName = string.Format("DaMdfe_{0}.pdf", DateTime.Now.ToString("dd_MM_yyyy"));

                return fsr;
            }
            catch (Exception ex)
            {
                return View("Index");
            }
        }

        /// <summary>
        /// Obtém a lista de status de uma mdfe de acordo com o Id da mdfe selecionada na grid de mdfes
        /// </summary>
        /// <param name="filter">Filtro com o id da mdfe selecionada</param>
        /// <returns>Lista de status da Mdfe</returns>
        [Autorizar(Transacao = "ADFISCAIS", Acao = "Pesquisar")]
        public ActionResult ObterStatusMdfePorIdMdfe(DetalhesFiltro<object>[] filter)
        {
            var idMdfe = filter
                .Where(e => e.Campo == "idMdfe" && !string.IsNullOrWhiteSpace(e.Valor[0].ToString()))
                .Select(e => e == null ? 0 : int.Parse(e.Valor[0].ToString(), CultureInfo.InvariantCulture))
                .FirstOrDefault();

            var result =
                _mdfeService.ObterStatusMdfePorIdMdfe(idMdfe);

            return Json(new
            {
                Total = result.Count,
                Items = result.Select(e => new
                {
                    DataHora = e.DataHora.ToString("dd/MM/yyyy hh:mm:ss"),
                    StatusMdfe = e.MdfeStatusRetorno.Descricao,
                    Usuario = e.Usuario.Nome
                })
            });
        }

        /// <summary>
        /// Obtém a View de Detalhe da Cte
        /// </summary>
        /// <param name="idCte">Id da Cte selecionada na Grid</param>
        /// <returns>Retorna a View com Detalhes da Cte</returns>
        [Autorizar(Transacao = "ADFISCAIS", Acao = "Pesquisar")]
        public JsonResult DetalheCte(int idCte)
        {
            try
            {
                var cte = _cteService.ObterCtePorId(idCte);

                if (cte != null)
                {
                    CteComplementado cteComplementado = null;
                    CteEmpresas empresas = _cteService.ObterListaDeEmpresas(cte);

                    IEmpresa tomador = empresas == null ? cte.FluxoComercial.EmpresaPagadora : empresas.EmpresaTomadora;
                    IEmpresa expedidor = null;
                    IEmpresa recebedor = null;
                    IEmpresa remetente = null;
                    IEmpresa destinatario = null;

                    if (cte.TipoCte == TipoCteEnum.Complementar)
                    {
                        cteComplementado = _cteComplementadoRepository.ObterCteComplementadoPorCteComplementar(cte).FirstOrDefault();
                    }

                    if (cteComplementado != null)
                    {
                        remetente = _cteService.ObterEmpresaRemetenteFiscal(cteComplementado.Cte.FluxoComercial.Codigo.Substring(2), cteComplementado.Cte.Id.Value);
                        destinatario = _cteService.ObterEmpresaDestinatariaFiscal(cteComplementado.Cte.FluxoComercial.Codigo.Substring(2), cteComplementado.Cte.Id.Value);
                        expedidor = cteComplementado.Cte.FluxoComercial.EmpresaRemetente;
                        recebedor = cteComplementado.Cte.FluxoComercial.EmpresaDestinataria;
                    }
                    else
                    {
                        remetente = _cteService.ObterEmpresaRemetenteFiscal(cte.FluxoComercial.Codigo.Substring(2), cte.Id.Value);
                        destinatario = _cteService.ObterEmpresaDestinatariaFiscal(cte.FluxoComercial.Codigo.Substring(2), cte.Id.Value);
                        expedidor = cte.FluxoComercial.EmpresaRemetente;
                        recebedor = cte.FluxoComercial.EmpresaDestinataria;
                    }

                    var remetenteJson = new EmpresaInformacao(remetente);
                    var destinatarioJson = new EmpresaInformacao(destinatario);
                    var expedidorJson = new EmpresaInformacao(expedidor);
                    var recebedorJson = new EmpresaInformacao(recebedor);
                    var tomadorJson = new EmpresaInformacao(tomador);

                    var conteiners = String.Empty;
                    IList<CteDetalhe> listaDetalhe = _cteService.ObterCteDetalhePorIdCte(cte.Id.Value);
                    if (listaDetalhe.Count > 0)
                    {
                        var listaContainer = listaDetalhe.Select(x => x.ConteinerNotaFiscal).Distinct().ToList();
                        conteiners = string.Join(", ", listaContainer.ToArray());
                    }

                    var valorTarifa = (cte.PesoVagao.HasValue && cte.PesoVagao.Value > 0) && cte.BaseCalculoIcms.HasValue
                                          ? cte.BaseCalculoIcms / cte.PesoVagao
                                          : 0;

                    return Json(new
                    {
                        Success = true,
                        Item = new
                        {
                            FluxoComercial = cte.FluxoComercial.Codigo,
                            Vagao = cte.Vagao.Codigo,
                            Conteiner = conteiners,
                            cte.Observacao,
                            cte.Serie,
                            cte.Numero,
                            DataEmissao = cte.DataHora.ToString("dd/MM/yyyy"),
                            cte.Chave,
                            cte.Cfop,
                            Remetente = remetenteJson,
                            Destinatario = destinatarioJson,
                            Expedidor = expedidorJson,
                            Recebedor = recebedorJson,
                            Tomador = tomadorJson,
                            PrestacaoServico = new
                            {
                                PesoVagao = cte.PesoVagao.HasValue ? cte.PesoVagao.Value.ToString("0.00", CultureInfo.GetCultureInfo("en-US").NumberFormat) : "0.00",
                                BaseCalculo = cte.BaseCalculoIcms.ToString(),
                                AliqIcms = cte.PercentualAliquotaIcms.HasValue ? cte.PercentualAliquotaIcms.Value.ToString("0.00", CultureInfo.GetCultureInfo("en-US").NumberFormat) : "0.00",
                                VlrIcms = cte.ValorIcms.ToString(),
                                VlrDesconto = (cte.ValorCte - cte.ValorReceber).ToString(),
                                VlrTotal = cte.ValorCte.ToString(),
                                VlrReceber = cte.ValorReceber.ToString(),
                                VlrTarifa = valorTarifa.Value.ToString("0.00", CultureInfo.GetCultureInfo("en-US").NumberFormat)
                            }
                        }
                    });
                }

                return Json(new
                {
                    Success = false,
                    Message = "Cte não localizada",
                    Item = (object)null
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message,
                    Item = (object)null
                });
            }
        }

        /// <summary>
        /// Realiza o Reemissão dos Ctes escolhidos
        /// </summary>
        /// <param name="ids">ids dos ctes</param>
        /// <returns>resultado do cancelamento</returns>
        [Autorizar(Transacao = "ADFISCAIS", Acao = "Pesquisar")]
        public ActionResult GerarZipPdf(string ids)
        {
            var arrIds = ids.Split(',');
            FileStreamResult fsr = null;
            MemoryStream returnContent = _cteService.GerarArquivoZipLote(arrIds, TipoArquivoEnvioEnum.Pdf);

            if (returnContent != null)
            {
                fsr = new FileStreamResult(returnContent, "application/zip");
                fsr.FileDownloadName = "dactePdf.zip";
            }

            return fsr;
        }

        /// <summary>
        /// Realiza o Reemissão dos Ctes escolhidos
        /// </summary>
        /// <param name="ids">ids dos ctes</param>
        /// <returns>resultado do cancelamento</returns>
        [Autorizar(Transacao = "ADFISCAIS", Acao = "Pesquisar")]
        public ActionResult GerarZipXml(string ids)
        {
            var arrIds = ids.Split(',');
            FileStreamResult fsr = null;
            MemoryStream returnContent = _cteService.GerarArquivoZipLote(arrIds, TipoArquivoEnvioEnum.Xml);

            if (returnContent != null)
            {
                fsr = new FileStreamResult(returnContent, "application/zip");
                fsr.FileDownloadName = "dacteXml.zip";
            }

            return fsr;
        }

        /*
        /// <summary>
        /// Obtém o diário de bordo da Cte selecionada
        /// </summary>
        /// <param name="idCte">Id da Cte selecionada</param>
        /// <returns>Lista de resultados de acordo com a cte selecionada</returns>
        public JsonResult ObterDiarioBordoCte(int idCte)
        {
            return Json(new
                            {

                            });
        }
        */

        /// <summary>
        /// Obtém o status processamento atual
        /// </summary>
        /// <param name="idNota"> Id da Nota Fiscal</param>
        /// <returns> String do status do processamento.  </returns>
        [Autorizar(Transacao = "ADFISCAIS", Acao = "Pesquisar")]
        public JsonResult ObterDadosNfe(string idNota)
        {
            int idNotaFiscal;

            if (Int32.TryParse(idNota, out idNotaFiscal))
            {
                var nf = _nfeService.ObterDadosNfeBancoDadosPorId(idNotaFiscal);

                if (nf != null)
                {
                    var chaveNfe = nf.ChaveNfe;

                    if (!String.IsNullOrEmpty(chaveNfe))
                    {
                        NfeReadonly dadosNfe = _nfeService.ObterDadosNfeBancoDados(chaveNfe);
                        StatusNfe statusNfe = _nfeService.ObterStatusNfe(chaveNfe);
                        IList<DespachosNfeDto> listaDespachosNfe = _nfeService.ObterDespachosPorNfe(chaveNfe);
                        bool indTemPermissaoAlterar = _nfeService.VerificarUsuarioPodeAlterarPesoNfe(UsuarioAtual);

                        if (dadosNfe == null)
                        {
                            return Json(new
                            {
                                Success = false,
                                Erro = true,
                                Mensagem = string.Format("Nota {0} não localizada na base de dados. Favor baixar a nota, utilizando a tela 1154 (Carregamento de NF-e)", chaveNfe)
                            });
                        }

                        return Json(new
                        {
                            Success = true,
                            Erro = false,
                            Mensagem = string.Empty,
                            dadosNfe.NumeroNotaFiscal,
                            dadosNfe.SerieNotaFiscal,
                            dadosNfe.ChaveNfe,
                            DataEmissao = dadosNfe.DataEmissao.ToShortDateString(),
                            Valor = dadosNfe.Valor.ToString(),
                            dadosNfe.CnpjEmitente,
                            dadosNfe.RazaoSocialEmitente,
                            dadosNfe.InscricaoEstadualEmitente,
                            dadosNfe.UfEmitente,
                            dadosNfe.CnpjDestinatario,
                            dadosNfe.RazaoSocialDestinatario,
                            dadosNfe.InscricaoEstadualDestinatario,
                            dadosNfe.UfDestinatario,
                            dadosNfe.CnpjTransportadora,
                            dadosNfe.NomeTransportadora,
                            dadosNfe.UfTransportadora,
                            dadosNfe.MunicipioTransportadora,
                            statusNfe.Origem,
                            PesoBruto = (dadosNfe.PesoBruto / 1000).ToString(),
                            Peso = (dadosNfe.Peso / 1000).ToString(),
                            Volume = dadosNfe.Volume.ToString(),
                            PesoDisponivel = (statusNfe.Peso - statusNfe.PesoUtilizado) < 0 ? 0 : Math.Round((statusNfe.Peso - statusNfe.PesoUtilizado), 2),
                            VolumeDisponivel = (statusNfe.Volume - statusNfe.VolumeUtilizado) < 0 ? 0 : Math.Round((statusNfe.Volume - statusNfe.VolumeUtilizado), 2)
                        });
                    }

                    return Json(new
                    {
                        Success = true,
                        Erro = false,
                        Mensagem = string.Empty,
                        nf.NumeroNotaFiscal,
                        nf.SerieNotaFiscal,
                        nf.ChaveNfe,
                        DataEmissao = nf.DataEmissao.ToShortDateString(),
                        Valor = nf.ValorTotalNotaFiscal.ToString(),
                        nf.CnpjEmitente,
                        nf.RazaoSocialEmitente,
                        InscricaoEstadualEmitente = nf.InscEstadualEmitente,
                        nf.UfEmitente,
                        nf.CnpjDestinatario,
                        nf.RazaoSocialDestinatario,
                        InscricaoEstadualDestinatario = nf.InscEstadualDestinatario,
                        nf.UfDestinatario,
                        Origem = "Manual",
                        nf.PesoBruto,
                        Peso = nf.PesoUtilizado,
                        Volume = 0,
                        PesoDisponivel = (nf.PesoBruto - nf.PesoUtilizado) < 0 ? 0 : Math.Round((nf.PesoBruto - nf.PesoUtilizado), 2),
                        VolumeDisponivel = 0
                    });
                }
            }

            return Json(new
            {
                Success = false,
                Erro = true,
                Mensagem = "Não foi possível localizar a Nota fiscal",
                NumeroNotaFiscal = String.Empty,
                SerieNotaFiscal = String.Empty,
                ChaveNfe = String.Empty,
                DataEmissao = String.Empty,
                Valor = String.Empty,
                CnpjEmitente = String.Empty,
                RazaoSocialEmitente = String.Empty,
                InscricaoEstadualEmitente = String.Empty,
                UfEmitente = String.Empty,
                CnpjDestinatario = String.Empty,
                RazaoSocialDestinatario = String.Empty,
                InscricaoEstadualDestinatario = String.Empty,
                UfDestinatario = String.Empty,
                Origem = String.Empty,
                PesoBruto = 0,
                Peso = 0,
                Volume = 0,
                PesoDisponivel = 0,
                VolumeDisponivel = 0
            });
        }

        /// <summary>
        /// Ação do controller detalhes
        /// </summary>
        /// <param name="idMdfe">id da Mdfe</param>
        /// <returns>Action Result</returns>
        [Autorizar(Transacao = "ADFISCAIS", Acao = "EnviarAprovacao")]
        public ActionResult EnviarAprovacao(int idMdfe)
        {
            try
            {
                bool status = _mdfeService.EnviarAprovacao(idMdfe, UsuarioAtual);

                if (status)
                {
                    return Json(new { success = true, Message = "Mdfe enviado para aprovação com sucesso" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { success = false, ex.Message });
            }

            return Json(new { success = false, Message = "Ocorreu um erro ao processar a solicitação." });
        }

        /// <summary>
        /// Gera o pdf para Impressão 
        /// </summary>
        /// <param name="idCte">Identificador do cte</param>
        /// <returns>retorna o pdf de impressão</returns>
        [Autorizar(Transacao = "ADFISCAIS", Acao = "Imprimir")]
        public ActionResult ImprimirCte(string idCte)
        {
            string[] arrayCtes = idCte.Split('$');
            List<int> listaIds = new List<int>();
            PdfMerge merge = new PdfMerge();
            CteArquivo file = null;
            int auxIdCte = 0;

            foreach (string id in arrayCtes)
            {
                if (!string.IsNullOrEmpty(id))
                {
                    auxIdCte = int.Parse(id);
                    file = _cteService.ObterArquivoCte(auxIdCte);

                    if (file != null)
                    {
                        listaIds.Add(auxIdCte);
                        merge.AddFile(file.ArquivoPdf); 
                    }
                }
            }

            try
            {
                Stream returnContent = merge.Execute();
                FileStreamResult fsr = new FileStreamResult(returnContent, "application/pdf");
                fsr.FileDownloadName = "dacte.pdf";

                _cteService.ImprimirCtes(listaIds);

                return fsr;
            }
            catch (Exception ex)
            {
                return View("Index");
            }
        }

        /// <summary>
        /// Abre tela de edição de nota fiscal
        /// </summary>
        /// <param name="arrayCtes"> Array com os id dos cte</param>
        /// <returns> Tela de edição de nota fiscal </returns>
        [Autorizar(Transacao = "ADFISCAIS", Acao = "Imprimir")]
        public ActionResult FormStatusImpressao(int[] arrayCtes)
        {
            ViewData["ID_CTE_IMPRESSAO"] = arrayCtes;

            return View();
        }

        /// <summary>
        /// Abre tela de edição de nota fiscal
        /// </summary>
        /// <param name="arrayCtes"> Array com os id dos cte</param>
        /// <returns> Tela de edição de nota fiscal </returns>
        [Autorizar(Transacao = "ADFISCAIS", Acao = "Imprimir")]
        public JsonResult ObterCteImpressao(int[] arrayCtes)
        {
            IList<Cte> result = _cteService.ObterCteImpressao(arrayCtes);

            return Json(new
            {
                Items = result.Select(c => new
                {
                    CteId = c.Id,
                    NroCte = c.Numero,
                    Status = c.ArquivoPdfGerado,
                    Imprime = c.ArquivoPdfGerado && (c.SituacaoAtual == SituacaoCteEnum.Autorizado || c.SituacaoAtual == SituacaoCteEnum.AguardandoAnulacao) ? 
                    "S" : ((c.ArquivoPdfGerado && (c.SituacaoAtual != SituacaoCteEnum.Autorizado || c.SituacaoAtual != SituacaoCteEnum.AguardandoAnulacao)) ? 
                    "NP" : 
                    "N")
                }),
                success = true
            });
        }

        /// <summary>
        /// Realiza o Reemissão dos Ctes escolhidos
        /// </summary>
        /// <param name="ids">ids dos ctes</param>
        /// <returns>resultado do cancelamento</returns>
        [Autorizar(Transacao = "ADFISCAIS", Acao = "Cancelamento")]
        [JsonFilter(Param = "ids", JsonDataType = typeof(int[]))]
        public JsonResult CancelamentoCte(int[] ids)
        {
            var idsCancelar = new List<int>();
            var idsNaoPodeCancelar = new List<string>();

            foreach (var idCte in ids)
            {
                if (_cteService.PodeCancelar(idCte))
                {
                    idsCancelar.Add(idCte);
                }
                else
                {
                    var c = _cteService.ObterCtePorId(idCte);
                    idsNaoPodeCancelar.Add(c.Numero);
                }
            }

            try
            {
                if (ids.Length > 0)
                {
                    _cteService.ReemissaoCancelamentoCtes(idsCancelar.ToArray(), UsuarioAtual);
                }

                return Json(new { success = true, Message = "SUCESSO", NaoPodeCancelar = idsNaoPodeCancelar });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, Message = string.Format("Falha na Reemissão do Cancelamento: {0}", ex.Message) });
            }
        }

        /// <summary>
        /// Realiza o Reemissão dos Ctes escolhidos
        /// </summary>
        /// <param name="ids">ids dos ctes</param>
        /// <returns>resultado do cancelamento</returns>
        [Autorizar(Transacao = "ADFISCAIS", Acao = "Reenviar")]
        [JsonFilter(Param = "ids", JsonDataType = typeof(int[]))]
        public JsonResult ReenviarCte(int[] ids)
        {
            string mensagemErro = string.Empty;
            bool status = true;
            List<int> idCtes = new List<int>();

            foreach (int id in ids)
            {
                if (_cteService.VerificarCteFolha(id))
                {
                    idCtes.Add(id);
                }
            }

            if (idCtes.Count > 0)
            {
                status = _cteService.ReemissaoCtes(idCtes.ToArray(), UsuarioAtual, out mensagemErro);
            }

            if (status)
            {
                return Json(new { success = true, Message = "SUCESSO" });
            }

            // return Json(new { success = false, Message = "FALHA" });
            return Json(new { success = false, Message = string.Format("Falha na Reemissão: {0}", mensagemErro) });
        }

        /// <summary>
        /// Realiza o Reemissão dos Ctes escolhidos
        /// </summary>
        /// <param name="ids">ids dos ctes</param>
        /// <returns>resultado do cancelamento</returns>
        [Autorizar(Transacao = "ADFISCAIS", Acao = "Inutilizar")]
        [JsonFilter(Param = "ids", JsonDataType = typeof(int[]))]
        public JsonResult InutilizarCte(int[] ids)
        {
            var idsInutilizar = new List<int>();
            var idsNaoPodeInutilizar = new List<string>();

            foreach (var idCte in ids)
            {
                if (_cteService.PodeInutilzarCte(idCte))
                {
                    idsInutilizar.Add(idCte);
                }
                else
                {
                    var c = _cteService.ObterCtePorId(idCte);
                    idsNaoPodeInutilizar.Add(c.Numero);
                } 
            }

            try
            {
                _cteService.SalvarInutilizacaoCtes(idsInutilizar.ToArray(), UsuarioAtual);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, Message = string.Format("FALHA: {0}", ex.Message) });
            }

            return Json(new { success = true, Message = "SUCESSO", NaoPodeInutilizar = idsNaoPodeInutilizar });
        }

        /// <summary>
        /// Obtém o diário de bordo para a cte selecionada
        /// </summary>
        /// <param name="pagination">Detalhes de paginação</param>
        /// <param name="idCte">Id da Cte selecionada</param>
        /// <returns>Response com o sucesso ou não da operação</returns>
        [Autorizar(Transacao = "ADFISCAIS", Acao = "Pesquisar")]
        public JsonResult ObterDiarioBordoCte(DetalhesPaginacaoWeb pagination, int idCte)
        {
            var result = _cteService.ObterDiarioBordoCte(pagination, idCte);

            return Json(new
            {
                result.Total,
                Items = result.Items.Select(d => new
                {
                    d.Id,
                    DataHora = d.DataHora.ToString("dd/MM/yyyy HH:mm:ss"),
                    d.Acao,
                    Usuario = d.UsuarioCadastro.Nome
                })
            });  
        }

        /// <summary>
        /// Grava um novo registro no diário de bordo para a Cte selecionada
        /// </summary>
        /// <param name="acao">Ação ou mensagem informada</param>
        /// <param name="idCte">Id da Cte selecionada</param>
        /// <returns>Resultado com o sucesso ou não da operação</returns>
        [Autorizar(Transacao = "ADFISCAIS", Acao = "Salvar")]
        public JsonResult GravarDiarioDeBordoCte(string acao, int idCte)
        {
            try
            {
                var res = _cteService.GravarDiarioBordoCte(acao, idCte, UsuarioAtual);
                return Json(new
                {
                    Success = res,
                    Message = res ? "Diário registrado com sucesso!" : "Houve um problema ao tentar enviar para o diário de bordo"
                });
            }
            catch (Exception e)
            {
                return Json(new
                {
                    Success = false,
                    e.Message
                });
            }
        }

        /// <summary>
        /// Obtém os tipos de cte existentes
        /// </summary>
        /// <returns>Lista string de tipos de ctes</returns>
        [Autorizar(Transacao = "ADFISCAIS", Acao = "Pesquisar")]
        public JsonResult ObterTiposCte()
        {
            var result = _cteService.ObterTiposCte();

            // Adicionando Carta de Correção ao Tipo
            result.Add("CCE");

            return Json(new
            {
                Items = result.Select(c => new
                {
                    TipoCte = c,
                    TipoCteLabel = ObterLabelTipoCte(c)
                }),
                success = true
            });
        }

        /// <summary>
        /// Exporta os dados para excel
        /// </summary>
        /// <param name="dataInicial">Data de Inicio da pesquisa</param>
        /// <param name="dataFinal">Data Fim da pesquisa</param>
        /// <param name="chaveNfe">Chave do Nfee informada</param>
        /// <param name="fluxo">Fluxo selecionado</param>
        /// <param name="uf">Uf selecionada</param>
        /// <param name="origem">Origem informada</param>
        /// <param name="destino">Destino informado</param>
        /// <param name="serieDesp">Serie do despacho informado</param>
        /// <param name="numeroDesp">Numero do Despacho informado</param>
        /// <param name="chaveCte">Chave de CTE informada</param>
        /// <param name="codVagao">Vagão informado</param>
        /// <param name="idMdfe">Id da Mdfe selecionada</param>
        /// <param name="idTrem">Id do Trem Selecionado</param>
        /// <param name="tipoCte">Tipo do Cte selecionado</param>
        /// <param name="semaforo">Semaforo selecionado</param>
        /// <param name="siglaUfSemaforo">UF do semaforo selecionado</param>
        /// <param name="periodoSemaforo">Periodo do semaforo selecionado</param>
        /// <returns>Retorna o arquivo excel</returns>
        [Autorizar(Transacao = "ADFISCAIS", Acao = "Pesquisar")]
        public ActionResult Exportar(string dataInicial, string dataFinal, string chaveNfe, string fluxo, string uf, string origem, string destino, string serieDesp, string numeroDesp, string chaveCte, string codVagao, string idMdfe, string idTrem, string tipoCte, string semaforo, string siglaUfSemaforo, string periodoSemaforo)
        {
            DateTime dataInicio = DateTime.ParseExact(dataInicial, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
            DateTime dataFim = DateTime.ParseExact(dataFinal, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture).AddDays(1).AddSeconds(-1);
            origem = origem != null ? origem.ToUpper() : null;
            destino = destino != null ? destino.ToUpper() : null;

            var serieDespacho = String.IsNullOrEmpty(serieDesp) ? 0 : int.Parse(serieDesp, CultureInfo.InvariantCulture);
            var numeroDespacho = String.IsNullOrEmpty(numeroDesp) ? 0 : int.Parse(numeroDesp, CultureInfo.InvariantCulture);
            var idSelMdfe = String.IsNullOrEmpty(idMdfe) ? 0 : int.Parse(idMdfe, CultureInfo.InvariantCulture);
            var idSelTrem = String.IsNullOrEmpty(idTrem)  ? 0 : int.Parse(idTrem, CultureInfo.InvariantCulture);
            var periodoHoras = String.IsNullOrEmpty(periodoSemaforo)
                                   ? HorasErroEnum.TodasAsHoras
                                   : ALL.Core.Util.Enum<HorasErroEnum>.From(periodoSemaforo.ToUpperInvariant());

            string[] chavesCte = !string.IsNullOrEmpty(chaveCte) ? chaveCte.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries) : (string[])null;

            ResultadoPaginado<CteDto> result = null;

            if (idSelTrem > 0)
            {
                result = _cteService.ObterComposicaoPorTrem(null, idSelTrem);
            }
            else if (idSelMdfe > 0)
            {
                result = _cteService.ObterComposicaoPorMdfe(null, idSelMdfe);
            }
            else if (!String.IsNullOrWhiteSpace(semaforo))
            {
                result = _cteService.ObterComposicaoPorSemaforo(null, semaforo, siglaUfSemaforo, periodoHoras);
            }
            else
            {
                result = _cteService.ObterComposicoesFiltros(null, dataInicio, dataFim, chaveNfe, fluxo, uf, origem, destino, serieDespacho, numeroDespacho, chavesCte, codVagao, tipoCte);
            }

            var dic = new Dictionary<string, Func<CteDto, string>>();

            dic["Nº Vagão"] = c => string.IsNullOrEmpty(c.CodigoVagao) ? string.Empty : c.CodigoVagao;
            dic["SerieDesp5"] = c => c.SerieDespacho != null ? c.SerieDespacho.ToString().PadLeft(3, '0') : string.Empty;
            dic["Nº Despacho5"] = c => c.NumeroDespacho.ToString().PadLeft(3, '0');
            dic["Fluxo"] = c => c.Fluxo;
            dic["Situação Cte"] = c => ObterStatusCte(c.SituacaoCteStr ?? string.Empty);
            dic["Cte"] = c => c.Numero;
            dic["Série"] = c => c.SerieCte;
            dic["Chave Cte"] = c => string.IsNullOrEmpty(c.Chave) ? string.Empty : c.Chave;
            dic["Data Emissão"] = c => c.DataEmissao.ToString("dd/MM/yyyy");
            dic["Data Carregamento"] = c => c.DataCarregamento.ToString("dd/MM/yyyy");
            dic["Baldeio?"] = c => c.HouveBaldeio;

            return this.Excel(string.Format("CtesAcompDocFiscais_{0}_{1}.xls", DateTime.Now.ToString("ddMMyyyy"), DateTime.Now.ToString("HHmmss")), dic, result.Items);
        }

        /// <summary>
        /// Retorna descrição de um status para a Cte
        /// </summary>
        /// <param name="status">Status da CTE</param>
        /// <returns>Descrição do status da CTE</returns>
        public string ObterStatusCte(string status)
        {
            switch (status.Trim())
            {
                case "PAE":
                    return "Pendente Arquivo Envio";
                case "PAC":
                    return "Pendente Arquivo Cancelamento";
                case "PAI":
                    return "Pendente Arquivo Inutilizacao";
                case "EFA":
                    return "Enviado Fila Arquivo Envio";
                case "EFT":
                    return "Enviado Fila Arquivo Complemento";
                case "EFC":
                    return "Enviado Fila Arquivo Cancelamento";
                case "EFI":
                    return "Enviado Fila Arquivo Inutilizacao";
                case "EAE":
                    return "Enviado Arquivo Envio";
                case "EAT":
                    return "Enviado Arquivo Complemento";
                case "EAC":
                    return "Enviado Arquivo Cancelamento";
                case "EAI":
                    return "Enviado Arquivo Inutilização";
                case "AUT":
                    return "Autorizado";
                case "CAN":
                    return "Cancelado";
                case "ERR":
                    return "Erro";
                case "EAR":
                    return "Erro Autorizado Re-Envio";
                case "INV":
                    return "Invalidado";
                case "INT":
                    return "Inutilizado";
                case "UND":
                    return "Uso Denegado";
                case "AGI":
                    return "Aguardando Inutilização";
                case "AGA":
                    return "Aguardando Anulação";
                case "ANL":
                    return "Anulado";
                case "AGC":
                    return "Aguardando Cancelamento";
                case "AUC":
                    return "Autorizado Cancelamento";
                case "AUI":
                    return "Autorizado Inutilização";
                case "PCC":
                    return "Preparando Chave Cte";
                case "PCE":
                    return "Pendente Correcao Envio";
                case "PRC":
                    return "Em Processamento";
                case "ECC":
                    return "Enviado Fila Arquivo Correção";
                default:
                    return string.Empty;
            }
        }

        /// <summary>
        /// Obtém a descrição dado um tipo de cte
        /// </summary>
        /// <param name="tipoCte">Tipo de cte para se obter a descrição</param>
        /// <returns>Descrição do Cte</returns>
        public string ObterLabelTipoCte(string tipoCte)
        {
            switch (tipoCte)
            {
                case "CPL":
                    return "Complementar";
                case "SBT":
                    return "Substituto";
                case "NML":
                    return "Normal";
                case "VTL":
                    return "Virtual";
                case "CCE":
                    return "Carta de correção";
                    default:
                return String.Empty;
            }
        }

        /// <summary>
        /// Classe interna para ralizar parse de informações de Remetente, Destinatário, Expedidor, Recebedor e Tomador
        /// </summary>
        private class EmpresaInformacao
        {
            /// <summary>
            /// Construtor que realiza o parse e formada o CpfCnpj corretamente
            /// </summary>
            /// <param name="empresa">Remetente ou Destinatário ou Expedidor ou Recebedor ou Tomador</param>
            public EmpresaInformacao(IEmpresa empresa)
            {
                long docInformado;
                string cnpjCpf = String.Empty;

                if (empresa.TipoPessoa == TipoPessoaEnum.Juridica)
                {
                    cnpjCpf = Int64.TryParse(empresa.Cgc, out docInformado)
                        ? docInformado.ToString(@"00\.000\.000\/0000\-00")
                        : String.Empty;
                }
                else
                {
                    cnpjCpf = Int64.TryParse(empresa.Cpf, out docInformado) 
                        ? docInformado.ToString(@"000\.000\.000\-00") 
                        : String.Empty;
                }

                var fone = empresa.Telefone1 ?? empresa.Telefone2 ?? String.Empty;
                var foneInformado = Regex.Replace(fone.Trim(), "[^.0-9]", String.Empty);

                Nome = empresa.RazaoSocial ?? empresa.NomeFantasia ?? String.Empty;
                Endereco = empresa.Endereco ?? String.Empty;
                Cep = empresa.Cep ?? String.Empty;
                Cidade = empresa.CidadeIbge != null ? empresa.CidadeIbge.Descricao ?? String.Empty : String.Empty;
                Documento = cnpjCpf;
                UF = empresa.CidadeIbge != null ? empresa.CidadeIbge.SiglaEstado ?? String.Empty : String.Empty;
                Pais = empresa.CidadeIbge != null ? empresa.CidadeIbge.SiglaPais ?? String.Empty : String.Empty;
                InscEstadual = empresa.InscricaoEstadual ?? String.Empty;
                Fone = foneInformado;
            }

            /// <summary>
            /// Nome do Remetente ou Destinatário ou Expedidor ou Recebedor ou Tomador
            /// </summary>
            public string Nome { get; set; }

            /// <summary>
            /// Endereco do Remetente ou Destinatário ou Expedidor ou Recebedor ou Tomador
            /// </summary>
            public string Endereco { get; set; }

            /// <summary>
            /// Cep do endereco do Remetente ou Destinatário ou Expedidor ou Recebedor ou Tomador
            /// </summary>
            public string Cep { get; set; }

            /// <summary>
            /// Cidade do Remetente ou Destinatário ou Expedidor ou Recebedor ou Tomador
            /// </summary>
            public string Cidade { get; set; }

            /// <summary>
            /// Documento formatado de acordo com o tipo de pessoa do Remetente ou Destinatário ou Expedidor ou Recebedor ou Tomador
            /// </summary>
            public string Documento { get; set; }

            /// <summary>
            /// UF do Remetente ou Destinatário ou Expedidor ou Recebedor ou Tomador
            /// </summary>
            public string UF { get; set; }

            /// <summary>
            /// País do Remetente ou Destinatário ou Expedidor ou Recebedor ou Tomador
            /// </summary>
            public string Pais { get; set; }

            /// <summary>
            /// Inscrição Estadual do Remetente ou Destinatário ou Expedidor ou Recebedor ou Tomador
            /// </summary>
            public string InscEstadual { get; set; }

            /// <summary>
            /// Telefone do Remetente ou Destinatário ou Expedidor ou Recebedor ou Tomador
            /// </summary>
            public string Fone { get; set; }
        }
    }
}