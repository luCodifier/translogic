﻿namespace Translogic.Modules.Core.Controllers.Cte
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Web.Mvc;
    using ALL.Core.Dominio;
    using Domain.Model.Diversos;
    using Domain.Model.Diversos.Cte;
    using Domain.Model.Dto;
    using Domain.Model.Estrutura;
    using Domain.Model.FluxosComerciais;
    using Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
    using Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;
    using Domain.Model.FluxosComerciais.Pedidos.Despachos;
    using Domain.Model.Trem.Veiculo.Conteiner;
    using Domain.Services.Ctes;
    using Domain.Services.FluxosComerciais;
    using Infrastructure;
    using Translogic.Core.Infrastructure.Web;
    using Translogic.Core.Infrastructure.Web.Filters;

    /// <summary>
    /// Controller Correção Cte
    /// </summary>
    public class CorrecaoCteController : BaseSecureModuleController
    {
        private readonly CteService _cteService;
        private readonly CarregamentoService _carregamentoService;
        private readonly ICteComplementadoRepository _cteComplementadoRepository;
        private readonly ICteArquivoRepository _cteArquivoRepository;

        /// <summary>
        /// Contrutor Defaut
        /// </summary>
        /// <param name="cteService">Service Cte</param>
        /// <param name="carregamentoService">Service Carregamento</param>
        /// <param name="cteComplementadoRepository">Repositório do cte complementado injetado</param>
        public CorrecaoCteController(CteService cteService, CarregamentoService carregamentoService, ICteComplementadoRepository cteComplementadoRepository, ICteArquivoRepository cteArquivoRepository)
        {
            _cteService = cteService;
            _carregamentoService = carregamentoService;
            _cteComplementadoRepository = cteComplementadoRepository;
            _cteArquivoRepository = cteArquivoRepository;
        }

        /// <summary>
        /// Acao Padrao ao ser chamado o controller
        /// </summary>
        /// <param name="mensagem">Mensagem de validação de cte</param>
        /// <returns>View Padrao</returns>
        [Autorizar(Transacao = "CTECORRECAO")]
        public ActionResult Index(string mensagem)
        {
            ViewData["MENSAGEM"] = mensagem;
            return View();
        }

        /// <summary>
        /// Acao de mostrar o Formulario
        /// </summary>
        /// <param name="cteId">Id da composicao</param>
        /// <returns>View de insercao composicao</returns>
        [Autorizar(Transacao = "CTECORRECAO", Acao = "Pesquisar")]
        public ActionResult Correcao(int cteId)
        {
            try
            {
                Cte cte = _cteService.ObterCtePorId(cteId);
                CteComplementado cteComplementado = null;

                IEmpresa tomador;
                IEmpresa expedidor;
                IEmpresa recebedor;
                IEmpresa remetente;
                IEmpresa destinatario;

                if (cte.TipoCte == TipoCteEnum.Complementar)
                {
                    cteComplementado = _cteComplementadoRepository.ObterCteComplementadoPorCteComplementar(cte).FirstOrDefault();
                }

                if (cteComplementado != null)
                {
                    remetente = _cteService.ObterEmpresaRemetenteFiscal(cteComplementado.Cte.FluxoComercial.Codigo.Substring(2), cteComplementado.Cte.Id.Value);
                    destinatario = _cteService.ObterEmpresaDestinatariaFiscal(cteComplementado.Cte.FluxoComercial.Codigo.Substring(2), cteComplementado.Cte.Id.Value);
                    expedidor = cteComplementado.Cte.FluxoComercial.EmpresaRemetente;
                    recebedor = cteComplementado.Cte.FluxoComercial.EmpresaDestinataria;
                    tomador = cteComplementado.Cte.FluxoComercial.EmpresaPagadora;
                }
                else
                {
                    remetente = _cteService.ObterEmpresaRemetenteFiscal(cte.FluxoComercial.Codigo.Substring(2), cte.Id.Value);
                    destinatario = _cteService.ObterEmpresaDestinatariaFiscal(cte.FluxoComercial.Codigo.Substring(2), cte.Id.Value);
                    expedidor = cte.FluxoComercial.EmpresaRemetente;
                    recebedor = cte.FluxoComercial.EmpresaDestinataria;
                    tomador = cte.FluxoComercial.EmpresaPagadora;
                }

                // Dados do Cte
                CarregarViewDataInformacoesDoCte(cte, cteComplementado);

                // Dados do remetente
                if (remetente != null)
                {
                    CarregarViewDataInformacoesDoRemetente(remetente);
                }

                // Dados do Destinatário
                if (destinatario != null)
                {
                    CarregarViewDataInformacoesDoDestinatario(destinatario);
                }

                // Dados do Expedidor
                if (expedidor != null)
                {
                    CarregarViewDataInformacoesDoExpedidor(expedidor);
                }

                // Dados do recebedor
                if (recebedor != null)
                {
                    CarregarViewDataInformacoesDoRecebedor(recebedor);
                }

                // Dados do Tomador
                if (tomador != null)
                {
                    CarregarViewDataInformacoesDoTomador(tomador);
                }

                // Valores da Prestação de serviço
                CarregarViewDataInformacoesDePrestacaoDeServico(cteComplementado != null ? cteComplementado.Cte : cte);

                CarregarViewDataCorrecaoCte(cteComplementado != null ? cteComplementado.Cte : cte);

                return View();
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index", new { mensagem = ex.Message });
            }
        }

        /// <summary>
        /// Obtém os dados de alteração de fluxo comercial
        /// </summary>
        /// <param name="codFluxoComercial">Código do fluxo comercial</param>
        /// <param name="codFluxoComercialCorrigido">Novo Código do fluxo comercial</param>
        /// <param name="idCte">Id do Cte a ser alterado</param>
        /// <returns>Resultado Json</returns>
        [Autorizar(Transacao = "CTECORRECAO", Acao = "Pesquisar")]
        public JsonResult ValidarAlteracaoDoFluxoComercial(string codFluxoComercial, string codFluxoComercialCorrigido, int idCte)
        {
            try
            {
                FluxoComercial fluxoComercial = _cteService.VerificarValidadeFluxo(codFluxoComercial);
                FluxoComercial fluxoComercialCorrigido = _cteService.VerificarValidadeFluxo(codFluxoComercialCorrigido);
                Cte cte = _cteService.ObterCtePorId(idCte);

                bool indFluxoCte = _carregamentoService.VerificarFluxoCte(fluxoComercial);
                bool indNovoFluxoCte = _carregamentoService.VerificarFluxoCte(fluxoComercialCorrigido);

                if (!indFluxoCte)
                {
                    throw new Exception("Esse não é um fluxo(" + fluxoComercial + ") de CT-e! Favor informar outro fluxo.");
                }

                if (!indNovoFluxoCte)
                {
                    throw new Exception("Esse não é um fluxo(" + fluxoComercialCorrigido + ") de CT-e! Favor informar outro fluxo.");
                }

                Dictionary<string, object> dic = CarregarDicionarioComInformacoesDoFluxoCorrigido(fluxoComercialCorrigido, cte);

                return Json(new
                {
                    Erro = false,
                    RazaoSocialExpedidor = dic["EMPRESA_EXPEDIDOR"],
                    EnderecoExpedidor = dic["ENDERECO_EXPEDIDOR"],
                    CepExpedidor = dic["CEP_EXPEDIDOR"],
                    MunicipioExpedidor = dic["MUNICIPIO_EXPEDIDOR"],
                    CnpjExpedidor = dic["CJPN_EXPEDIDOR"],
                    UfExpedidor = dic["UF_EXPEDIDOR"],
                    PaisExpedidor = dic["PAIS_EXPEDIDOR"],
                    InscEstExpedidor = dic["INSC_EST_EXPEDIDOR"],
                    RazaoSocialRecebedor = dic["EMPRESA_RECEBEDOR"],
                    EnderecoRecebedor = dic["ENDERECO_RECEBEDOR"],
                    CepRecebedor = dic["CEP_RECEBEDOR"],
                    MunicipioRecebedor = dic["MUNICIPIO_RECEBEDOR"],
                    CnpjRecebedor = dic["CJPN_RECEBEDOR"],
                    UfRecebedor = dic["UF_RECEBEDOR"],
                    PaisRecebedor = dic["PAIS_RECEBEDOR"],
                    InscEstRecebedor = dic["INSC_EST_RECEBEDOR"],
                    DescricaoCfop = dic["CFOP_DESCRICAO"],
                    Cfop = dic["CFOP"],
                    AlteracaoExpedidor = dic["ALTERACAO_EXPEDIDOR"],
                    AlteracaoRecebedor = dic["ALTERACAO_RECEBEDOR"]
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Erro = true,
                    Mensagem = ex.Message
                });
            }
        }

        /// <summary>
        /// Obter dados do conteiner
        /// </summary>
        /// <param name="codigoVagao">Código do vagão</param>
        /// <returns>Resultado Json</returns>
        [Autorizar(Transacao = "CTECORRECAO", Acao = "Pesquisar")]
        public JsonResult VerificarMudancaVagao(string codigoVagao)
        {
            bool erro = false;
            string mensagem = string.Empty;
            int? idVagao = null;
            try
            {
                var vagao = _cteService.ObterVagaoPorCodigo(codigoVagao);
                if (vagao == null)
                {
                    erro = true;
                    mensagem = "Não foi encontrado nenhum Vagão com este código.";
                }
                else
                {
                    idVagao = vagao.Id;
                }
            }
            catch (Exception e)
            {
                erro = true;
                mensagem = e.Message;
            }

            return Json(new
            {
                Erro = erro,
                Mensagem = mensagem,
                IdVagao = idVagao
            });
        }

        /// <summary>
        /// Obter dados do conteiner
        /// </summary>
        /// <param name="codigoConteiner">Código do conteiner</param>
        /// <returns>Resultado Json</returns>
        [Autorizar(Transacao = "CTECORRECAO", Acao = "Pesquisar")]
        public JsonResult VerificarMudancaConteiner(string codigoConteiner)
        {
            bool erro = false;
            string mensagem = string.Empty;
            try
            {
                var conteiner = _carregamentoService.ObterConteinerPorCodigo(codigoConteiner);
                if (conteiner == null)
                {
                    erro = true;
                    mensagem = "Não foi encontrado nenhum conteiner com este código.";
                }
            }
            catch (Exception e)
            {
                erro = true;
                mensagem = e.Message;
            }

            return Json(new
            {
                Erro = erro,
                Mensagem = mensagem
            });
        }

        /// <summary>
        /// Obtem uma lista de Ctes
        /// </summary>
        /// <returns>Lista de Ctes do agrupamento </returns>
        [Autorizar(Transacao = "CTECORRECAO", Acao = "Pesquisar")]
        public JsonResult ObterCodigoSerieDesp()
        {
            IList<SerieDespachoUf> result = _carregamentoService.ObterCodigoSerie();

            // Insere um item default em branco na lista
            var itemDefault = new SerieDespachoUf { CodigoControle = " " };

            result.Insert(0, itemDefault);

            return Json(new
            {
                Items = result.Select(c => new
                {
                    c.Id,
                    c.CodigoControle
                }),
                success = true
            });
        }

        /// <summary>
        /// Obtem uma lista de Ctes
        /// </summary>
        /// <param name="pagination"> Paginacao pesquisa</param>
        /// <param name="filter"> Filtros para pesquisa</param>
        /// <returns>Lista de Ctes</returns>
        [Autorizar(Transacao = "CTECORRECAO", Acao = "Pesquisar")]
        public JsonResult ObterCtesCorrecao(DetalhesPaginacaoWeb pagination, DetalhesFiltro<object>[] filter)
        {
            ResultadoPaginado<CorrecaoCteDto> result = _cteService.ObterListaCteParaCorrecao(pagination, filter);

            return Json(new
            {
                result.Total,
                Items = result.Items.Select(c => new
                {
                    c.CteId,
                    c.IdDespacho,
                    Serie = c.SerieDesp6 == 0 && c.NumDesp6 == 0 ? !string.IsNullOrEmpty(c.SerieDesp5) ? c.SerieDesp5.ToString().PadLeft(3, '0') : string.Empty : !string.IsNullOrEmpty(c.CodigoControle) ? string.Concat(c.CodigoControle, "-", c.SerieDesp6.ToString().PadLeft(3, '0')) : string.Empty,
                    Despacho = c.SerieDesp6 == 0 && c.NumDesp6 == 0 ? c.NumDesp5 != 0 ? c.NumDesp5.ToString().PadLeft(3, '0') : string.Empty : c.NumDesp6 != 0 ? c.NumDesp6.ToString().PadLeft(3, '0') : string.Empty,
                    SerieDesp5 = !string.IsNullOrEmpty(c.SerieDesp5) ? c.SerieDesp5.ToString().PadLeft(3, '0') : string.Empty,
                    CodigoVagao = !string.IsNullOrEmpty(c.CodigoVagao) ? c.CodigoVagao : string.Empty,
                    c.CodFluxo,
                    c.ToneladaUtil,
                    c.Volume,
                    c.NroCte,
                    c.SerieCte,
                    c.ChaveCte,
                    DataEmissao = c.DataEmissao.ToString("dd/MM/yyyy")
                }),
                success = true
            });
        }

        /// <summary>
        /// Obtem as notas do Cte
        /// </summary>
        /// <param name="id">id do Cte Selecionado</param>
        /// <returns>Resultado Cte Selecionado</returns>
        [Autorizar(Transacao = "CTECORRECAO", Acao = "Pesquisar")]
        public JsonResult ObterNotaCte(int id)
        {
            Cte cte = _cteService.ObterCtePorId(id);

            if (cte.TipoCte == TipoCteEnum.Complementar)
            {
                CteComplementado cteComplementado = _cteComplementadoRepository.ObterCteComplementadoPorCteComplementar(cte).FirstOrDefault();

                if (cteComplementado != null)
                {
                    cte = cteComplementado.Cte;
                }
            }

            IList<CteDetalhe> result = _cteService.ObterCteDetalhePorIdCte(cte.Id.Value);

            IList<object> lista = new List<object>();

            foreach (CteDetalhe c in result)
            {
                string tipoDocumento;
                string strCnpjRemetente = string.Empty;

                if (!string.IsNullOrEmpty(c.ChaveNfe))
                {
                    tipoDocumento = "NF-e";
                }
                else
                {
                    tipoDocumento = c.Cte.FluxoComercial.ModeloNotaFiscal == "99" ? "Outros" : "NF";
                }

                long cnpjRemetente;
                if (Int64.TryParse(c.CgcRemetente, out cnpjRemetente))
                {
                    strCnpjRemetente = cnpjRemetente.ToString(@"00\.000\.000\/0000\-00");
                }

                var obj = new
                {
                    TipoDocumento = tipoDocumento,
                    SerieNotaFiscal = c.SerieNota,
                    NumeroNotaFiscal = c.NumeroNota,
                    PesoRateio = c.PesoNotaFiscal,
                    c.ValorNotaFiscal,
                    c.ValorTotalNotaFiscal,
                    CnpjRemetente = strCnpjRemetente,
                    c.PesoTotal,
                    Conteiner = c.ConteinerNotaFiscal,
                    CodConteiner = c.ConteinerNotaFiscal,
                    c.ChaveNfe
                };
                lista.Add(obj);
            }

            return Json(new
            {
                Items = lista,
                success = true
            });
        }

        /// <summary>
        /// Realiza o salvamento dos complementos dos Ctes Selecionados
        /// </summary>
        /// <param name="cteDto">Dto do Cte</param>
        /// <returns>resultado do salvamento</returns>
        [Autorizar(Transacao = "CTECORRECAO", Acao = "Salvar")]
        [JsonFilter(Param = "cte", JsonDataType = typeof(CorrecaoCteDto))]
        public JsonResult SalvarCorrecaoCte(CorrecaoCteDto cteDto)
        {
            bool erro = false;
            string mensagem = string.Empty;
            bool correcaoFluxoComercial = true;
            try
            {
                if (!string.IsNullOrEmpty(cteDto.CodFluxo))
                {
                    FluxoComercial fluxoComercial = _cteService.VerificarValidadeFluxo(cteDto.CodFluxo);
                    correcaoFluxoComercial = _cteService.VerificarInconsistenciasNaAlteracaoDoFluxoComercial(fluxoComercial, cteDto.CteId);
                }

                if (correcaoFluxoComercial)
                {
                    _cteService.SalvarCorrecaoCte(cteDto, UsuarioAtual);
                }
                else
                {
                    erro = true;
                }
            }
            catch (Exception e)
            {
                erro = true;
                mensagem = e.Message;
            }

            return Json(new
            {
                Erro = erro,
                Mensagem = mensagem
            });
        }

        private void ValidarSeOcorreuAlteracaoDeAliquota(Cte cte)
        {
            if (cte.PercentualAliquotaIcms != cte.FluxoComercial.Contrato.PercentualAliquotaIcms)
            {
                ViewData["ALTERACAO_ALIQUOTA"] = true;
            }

            ViewData["ALTERACAO_ALIQUOTA"] = false;
        }

        private void CarregarViewDataInformacoesDoCte(Cte cte, CteComplementado cteComplementado)
        {
            ViewData["ID_CTE_SELECIONADO"] = cte.Id;
            ViewData["CONTAINER"] = String.Empty;
            ViewData["SERIE_CTE"] = cte.Serie;
            ViewData["NUMERO_CTE"] = cte.Numero;
            ViewData["DATA_EMISSAO_CTE"] = cte.DataHora.ToString("dd/MM/yyyy");
            ViewData["CHAVE_CTE"] = cte.Chave;
            ViewData["CTE_OBSERVACAO"] = cte.Observacao;
            ViewData["CODIGO_FLUXO"] = cte.FluxoComercial.Codigo.Substring(2);
            ViewData["IND_FLUXO_CONTEINER"] = _carregamentoService.VerificarFluxoConteiner(cte.FluxoComercial);

            if (cteComplementado != null)
            {
                ViewData["VAGAO_COD"] = string.Empty;
                ViewData["CFOP"] = cteComplementado.Cte.ContratoHistorico.Cfop;
                ViewData["CFOP_DESCRICAO"] = string.Concat(cteComplementado.Cte.Cfop, " - ", cteComplementado.Cte.ContratoHistorico.DescricaoCfop);

                IList<CteDetalhe> listaDetalhe = _cteService.ObterCteDetalhePorIdCte(cte.Id.Value);
                if (listaDetalhe.Count > 0)
                {
                    var listaContainer = listaDetalhe.Select(x => x.ConteinerNotaFiscal).Distinct().ToList();
                    ViewData["CONTAINER"] = string.Join(", ", listaContainer.ToArray());
                }
            }
            else
            {
                ViewData["VAGAO_COD"] = cte.Vagao.Codigo;
                ViewData["CFOP"] = cte.FluxoComercial.Contrato.Cfop;
                ViewData["CFOP_DESCRICAO"] = string.Concat(cte.FluxoComercial.Contrato.Cfop, " - ", cte.FluxoComercial.Contrato.DescricaoCfop);

                IList<CteDetalhe> listaDetalhe = _cteService.ObterCteDetalhePorIdCte(cte.Id.Value);
                if (listaDetalhe.Count > 0)
                {
                    var listaContainer = listaDetalhe.Select(x => x.ConteinerNotaFiscal).Distinct().ToList();
                    ViewData["CONTAINER"] = string.Join(", ", listaContainer.ToArray());
                }
            }
        }

        private void CarregarViewDataInformacoesDoRemetente(IEmpresa remetente)
        {
            // Dados do Remetente
            ViewData["EMPRESA_REMETENTE"] = remetente.RazaoSocial;
            ViewData["ENDERECO_REMETENTE"] = remetente.Endereco;
            ViewData["CEP_REMETENTE"] = remetente.Cep;
            ViewData["MUNICIPIO_REMETENTE"] = remetente.CidadeIbge.Descricao;
            ViewData["CJPN_REMETENTE"] = string.Empty;
            ViewData["UF_REMETENTE"] = remetente.CidadeIbge.SiglaEstado;
            ViewData["PAIS_REMETENTE"] = remetente.CidadeIbge.SiglaPais;
            ViewData["INSC_EST_REMETENTE"] = remetente.InscricaoEstadual;

            if (remetente.TipoPessoa == TipoPessoaEnum.Juridica)
            {
                long cnpjRemetente;
                if (Int64.TryParse(remetente.Cgc, out cnpjRemetente))
                {
                    ViewData["CJPN_REMETENTE"] = cnpjRemetente.ToString(@"00\.000\.000\/0000\-00");
                }
            }
            else
            {
                long cpfRemetente;
                if (Int64.TryParse(remetente.Cpf, out cpfRemetente))
                {
                    ViewData["CJPN_REMETENTE"] = cpfRemetente.ToString(@"000\.000\.000\-00");
                }
            }
        }

        private void CarregarViewDataInformacoesDoDestinatario(IEmpresa destinatario)
        {
            ViewData["EMPRESA_DESTINATARIO"] = destinatario.RazaoSocial;
            ViewData["ENDERECO_DESTINATARIO"] = destinatario.Endereco;
            ViewData["CEP_DESTINATARIO"] = destinatario.Cep;
            ViewData["MUNICIPIO_DESTINATARIO"] = destinatario.CidadeIbge.Descricao;
            ViewData["CJPN_DESTINATARIO"] = string.Empty;
            ViewData["UF_DESTINATARIO"] = destinatario.CidadeIbge.SiglaEstado;
            ViewData["PAIS_DESTINATARIO"] = destinatario.CidadeIbge.SiglaPais;
            ViewData["INSC_EST_DESTINATARIO"] = destinatario.InscricaoEstadual;

            if (destinatario.TipoPessoa == TipoPessoaEnum.Juridica)
            {
                long cnpjDestinatario;
                if (Int64.TryParse(destinatario.Cgc, out cnpjDestinatario))
                {
                    ViewData["CJPN_DESTINATARIO"] = cnpjDestinatario.ToString(@"00\.000\.000\/0000\-00");
                }
            }
            else
            {
                long cpfDestinatario;
                if (Int64.TryParse(destinatario.Cpf, out cpfDestinatario))
                {
                    ViewData["CJPN_DESTINATARIO"] = cpfDestinatario.ToString(@"000\.000\.000\-00");
                }
            }
        }

        private void CarregarViewDataInformacoesDoExpedidor(IEmpresa expedidor)
        {
            ViewData["EMPRESA_EXPEDIDOR"] = expedidor.RazaoSocial;
            ViewData["ENDERECO_EXPEDIDOR"] = expedidor.Endereco;
            ViewData["CEP_EXPEDIDOR"] = expedidor.Cep;
            ViewData["MUNICIPIO_EXPEDIDOR"] = expedidor.CidadeIbge.Descricao;
            ViewData["CJPN_EXPEDIDOR"] = string.Empty;
            ViewData["UF_EXPEDIDOR"] = expedidor.CidadeIbge.SiglaEstado;
            ViewData["PAIS_EXPEDIDOR"] = expedidor.CidadeIbge.SiglaPais;
            ViewData["INSC_EST_EXPEDIDOR"] = expedidor.InscricaoEstadual;

            if (expedidor.TipoPessoa == TipoPessoaEnum.Juridica)
            {
                long cnpjExpedidor;
                if (Int64.TryParse(expedidor.Cgc, out cnpjExpedidor))
                {
                    ViewData["CJPN_EXPEDIDOR"] = cnpjExpedidor.ToString(@"00\.000\.000\/0000\-00");
                }
            }
            else
            {
                long cpfExpedidor;
                if (Int64.TryParse(expedidor.Cpf, out cpfExpedidor))
                {
                    ViewData["CJPN_EXPEDIDOR"] = cpfExpedidor.ToString(@"000\.000\.000\-00");
                }
            }
        }

        private void CarregarViewDataInformacoesDoRecebedor(IEmpresa recebedor)
        {
            ViewData["EMPRESA_RECEBEDOR"] = recebedor.RazaoSocial;
            ViewData["ENDERECO_RECEBEDOR"] = recebedor.Endereco;
            ViewData["CEP_RECEBEDOR"] = recebedor.Cep;
            ViewData["MUNICIPIO_RECEBEDOR"] = recebedor.CidadeIbge.Descricao;
            ViewData["CJPN_RECEBEDOR"] = string.Empty;
            ViewData["UF_RECEBEDOR"] = recebedor.CidadeIbge.SiglaEstado;
            ViewData["PAIS_RECEBEDOR"] = recebedor.CidadeIbge.SiglaPais;
            ViewData["INSC_EST_RECEBEDOR"] = recebedor.InscricaoEstadual;

            if (recebedor.TipoPessoa == TipoPessoaEnum.Juridica)
            {
                long cnpjRecebedor;
                if (Int64.TryParse(recebedor.Cgc, out cnpjRecebedor))
                {
                    ViewData["CJPN_RECEBEDOR"] = cnpjRecebedor.ToString(@"00\.000\.000\/0000\-00");
                }
            }
            else
            {
                long cpfRecebedor;
                if (Int64.TryParse(recebedor.Cpf, out cpfRecebedor))
                {
                    ViewData["CJPN_RECEBEDOR"] = cpfRecebedor.ToString(@"000\.000\.000\-00");
                }
            }
        }

        private void CarregarViewDataInformacoesDoTomador(IEmpresa tomador)
        {
            ViewData["EMPRESA_TOMADORA"] = tomador.RazaoSocial;
            ViewData["ENDERECO_TOMADORA"] = tomador.Endereco;
            ViewData["CEP_TOMADORA"] = tomador.Cep;
            ViewData["MUNICIPIO_TOMADORA"] = tomador.CidadeIbge.Descricao;
            ViewData["CJPN_TOMADORA"] = string.Empty;
            ViewData["UF_TOMADORA"] = tomador.CidadeIbge.SiglaEstado;
            ViewData["PAIS_TOMADORA"] = tomador.CidadeIbge.SiglaPais;
            ViewData["INSC_EST_TOMADORA"] = tomador.InscricaoEstadual;

            if (tomador.TipoPessoa == TipoPessoaEnum.Juridica)
            {
                long cnpjTomador;
                if (Int64.TryParse(tomador.Cgc, out cnpjTomador))
                {
                    ViewData["CJPN_TOMADORA"] = cnpjTomador.ToString(@"00\.000\.000\/0000\-00");
                }
            }
            else
            {
                long cpfTomador;
                if (Int64.TryParse(tomador.Cpf, out cpfTomador))
                {
                    ViewData["CJPN_TOMADORA"] = cpfTomador.ToString(@"000\.000\.000\-00");
                }
            }
        }

        private void CarregarViewDataInformacoesDePrestacaoDeServico(Cte cte)
        {
            ViewData["BASE_CALCULO"] = cte.BaseCalculoIcms.ToString();
            ViewData["ALIQ_ICMS"] = "0.00";

            if (cte.PercentualAliquotaIcms.HasValue)
            {
                ViewData["ALIQ_ICMS"] = cte.PercentualAliquotaIcms.Value.ToString("0.00", CultureInfo.GetCultureInfo("en-US").NumberFormat);
            }

            ViewData["VALOR_ICMS"] = cte.ValorIcms.ToString();
            ViewData["DESCONTO"] = (cte.ValorCte - cte.ValorReceber).ToString();
            ViewData["VALOR_TOTAL"] = cte.ValorCte.ToString();
            ViewData["VALOR_RECEBER"] = cte.ValorReceber.ToString();
        }

        private void CarregarViewDataCorrecaoCte(Cte cte)
        {
            CarregarViewDataValidacaoDadosCorrigidos();
            
            var cteArquivo = _cteArquivoRepository.ObterPorId(cte.Id);
            if (cteArquivo != null)
            {
                ValidarSeOcorreuAlteracaoDeCfop(cte);
                ValidarSeOcorreuAlteracaoDeRemetente(cte);
                ValidarSeOcorreuAlteracaoDeDestinatario(cte);
                ValidarSeOcorreuAlteracaoDeTomador(cte);
                ValidarSeOcorreuAlteracaoDeExpedidor(cte);
                ValidarSeOcorreuAlteracaoDeRecebedor(cte);
                ValidarSeOcorreuAlteracaoDeAliquota(cte); 
            }
        }

        private void CarregarViewDataValidacaoDadosCorrigidos()
        {
            ViewData["ALTERACAO_CFOP"] = false;
            ViewData["VALIDACAO_ALTERACAO_CFOP"] = true;
            ViewData["VALIDACAO_ALTERACAO_EXPEDIDOR"] = true;
            ViewData["VALIDACAO_ALTERACAO_RECEBEDOR"] = true;
        }

        private Dictionary<string, object> CarregarDicionarioComInformacoesDoFluxoCorrigido(FluxoComercial fluxoComercial, Cte cte)
        {
            CteEmpresas empresas = _cteService.ObterListaDeEmpresas(cte);
            Dictionary<string, object> dic = new Dictionary<string, object>();
            IEmpresa expedidor;
            IEmpresa recebedor;

            long cnpjExpedidor;
            long cnpjRecebedor;
            long cpfExpedidor;
            long cpfRecebedor;

            dic["ALTERACAO_EXPEDIDOR"] = false;
            dic["ALTERACAO_RECEBEDOR"] = false;
            dic["CJPN_EXPEDIDOR"] = string.Empty;
            dic["CJPN_RECEBEDOR"] = string.Empty;

            bool validarAlteracaoFluxoComercial = _cteService.VerificarInconsistenciasNaAlteracaoDoFluxoComercial(fluxoComercial, cte.Id.Value);

            expedidor = validarAlteracaoFluxoComercial ? fluxoComercial.EmpresaRemetente : empresas.EmpresaExpedidor;
            recebedor = validarAlteracaoFluxoComercial ? fluxoComercial.EmpresaDestinataria : empresas.EmpresaRecebedor;
            dic["CFOP"] = validarAlteracaoFluxoComercial ? fluxoComercial.Contrato.Cfop : cte.Cfop;
            dic["CFOP_DESCRICAO"] = validarAlteracaoFluxoComercial ? string.Concat(fluxoComercial.Contrato.Cfop, " - ", fluxoComercial.Contrato.DescricaoCfop)
                                                                   : string.Concat(cte.Cfop, " - ", cte.ContratoHistorico.DescricaoCfop);

            bool alteracaoExpedidor = _cteService.ValidarSeOcorreuAlteracaoDeExpedidor(cte, fluxoComercial);
            bool alteracaoRecebedor = _cteService.ValidarSeOcorreuAlteracaoDeRecebedor(cte, fluxoComercial);

            dic["ALTERACAO_EXPEDIDOR"] = alteracaoExpedidor;
            dic["ALTERACAO_RECEBEDOR"] = alteracaoRecebedor;

            if (expedidor.TipoPessoa == TipoPessoaEnum.Juridica)
            {
                if (Int64.TryParse(expedidor.Cgc, out cnpjExpedidor))
                {
                    dic["CJPN_EXPEDIDOR"] = cnpjExpedidor.ToString(@"00\.000\.000\/0000\-00");
                }
            }
            else
            {
                if (Int64.TryParse(expedidor.Cpf, out cpfExpedidor))
                {
                    dic["CJPN_EXPEDIDOR"] = cpfExpedidor.ToString(@"000\.000\.000\-00");
                }
            }

            dic["EMPRESA_EXPEDIDOR"] = expedidor.RazaoSocial;
            dic["ENDERECO_EXPEDIDOR"] = expedidor.Endereco;
            dic["CEP_EXPEDIDOR"] = expedidor.Cep;
            dic["MUNICIPIO_EXPEDIDOR"] = expedidor.CidadeIbge.Descricao;
            dic["UF_EXPEDIDOR"] = expedidor.CidadeIbge.SiglaEstado;
            dic["PAIS_EXPEDIDOR"] = expedidor.CidadeIbge.SiglaPais;
            dic["INSC_EST_EXPEDIDOR"] = expedidor.InscricaoEstadual;

            if (recebedor.TipoPessoa == TipoPessoaEnum.Juridica)
            {
                if (Int64.TryParse(recebedor.Cgc, out cnpjRecebedor))
                {
                    dic["CJPN_RECEBEDOR"] = cnpjRecebedor.ToString(@"00\.000\.000\/0000\-00");
                }
            }
            else
            {
                if (Int64.TryParse(recebedor.Cpf, out cpfRecebedor))
                {
                    dic["CJPN_RECEBEDOR"] = cpfRecebedor.ToString(@"000\.000\.000\-00");
                }
            }

            dic["EMPRESA_RECEBEDOR"] = recebedor.RazaoSocial;
            dic["ENDERECO_RECEBEDOR"] = recebedor.Endereco;
            dic["CEP_RECEBEDOR"] = recebedor.Cep;
            dic["MUNICIPIO_RECEBEDOR"] = recebedor.CidadeIbge.Descricao;
            dic["UF_RECEBEDOR"] = recebedor.CidadeIbge.SiglaEstado;
            dic["PAIS_RECEBEDOR"] = recebedor.CidadeIbge.SiglaPais;
            dic["INSC_EST_RECEBEDOR"] = recebedor.InscricaoEstadual;

            return dic;
        }

        private void ValidarSeOcorreuAlteracaoDeCfop(Cte cte)
        {
            bool validaAlteracaoCfop;
            bool alteracaoCfop;

            try
            {
                alteracaoCfop = _cteService.ValidarSeOcorreuAlteracaoCfop(cte, cte.FluxoComercial, out validaAlteracaoCfop);
            }
            catch (Exception)
            {
                    
                throw;
            }
            
            if (alteracaoCfop)
            {
                ViewData["ALTERACAO_CFOP"] = alteracaoCfop;
                ViewData["VALIDACAO_ALTERACAO_CFOP"] = validaAlteracaoCfop;
            }
        }

        private void ValidarSeOcorreuAlteracaoDeRemetente(Cte cte)
        {
            ViewData["ALTERACAO_REMETENTE"] = _cteService.ValidarSeOcorreuAlteracaoDeRemetente(cte, cte.FluxoComercial);
        }

        private void ValidarSeOcorreuAlteracaoDeDestinatario(Cte cte)
        {
            ViewData["ALTERACAO_DESTINATARIO"] = _cteService.ValidarSeOcorreuAlteracaoDeDestinatario(cte, cte.FluxoComercial);
        }

        private void ValidarSeOcorreuAlteracaoDeTomador(Cte cte)
        {
            ViewData["ALTERACAO_TOMADOR"] = _cteService.ValidarSeOcorreuAlteracaoDeTomador(cte, cte.FluxoComercial);
        }

        private void ValidarSeOcorreuAlteracaoDeExpedidor(Cte cte)
        {
            CteEmpresas empresas = _cteService.ObterListaDeEmpresas(cte);
            bool alteracaoExpedidor = _cteService.ValidarSeOcorreuAlteracaoDeExpedidor(cte, cte.FluxoComercial);

            // Se o expedidor for o tomador, o mesmo não pode ser alterado
            if (empresas.CodigoTomador == 1 && alteracaoExpedidor)
            {
                ViewData["VALIDACAO_ALTERACAO_EXPEDIDOR"] = false;
            }

            ViewData["ALTERACAO_EXPEDIDOR"] = alteracaoExpedidor;
        }

        private void ValidarSeOcorreuAlteracaoDeRecebedor(Cte cte)
        {
            CteEmpresas empresas = _cteService.ObterListaDeEmpresas(cte);
            bool alteracaoRecebedor = _cteService.ValidarSeOcorreuAlteracaoDeRecebedor(cte, cte.FluxoComercial);

            // Se o expedidor for o tomador, o mesmo não pode ser alterado
            if (empresas.CodigoTomador == 2 && alteracaoRecebedor)
            {
                ViewData["VALIDACAO_ALTERACAO_RECEBEDOR"] = false;
            }

            ViewData["ALTERACAO_RECEBEDOR"] = alteracaoRecebedor;
        }
    }
}
