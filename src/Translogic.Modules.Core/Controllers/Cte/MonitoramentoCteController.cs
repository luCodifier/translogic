﻿using System.Collections;

namespace Translogic.Modules.Core.Controllers.Cte
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Web.Mvc;
    using ALL.Core.Dominio;
    using Domain.Model.Diversos.Cte;
    using Domain.Model.Dto;
    using Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
    using Domain.Model.FluxosComerciais.Pedidos.Despachos;
    using Domain.Services.Ctes;
    using Domain.Services.FluxosComerciais;
    using Infrastructure;
    using Translogic.Core.Infrastructure;
    using Translogic.Core.Infrastructure.FileSystem.ExtensionMethods;
    using Translogic.Core.Infrastructure.Web;
    using Translogic.Core.Infrastructure.Web.Filters;
    using Translogic.Modules.TPCCO.Helpers;

    /// <summary>
    /// Controller Monitoramento Cte 
    /// </summary>
    public class MonitoramentoCteController : BaseSecureModuleController
    {
        private readonly CteService _cteService;
        private readonly CarregamentoService _carregamentoService;
        private readonly CteStatusRetorno _cteStatusRetorno;

        /// <summary>
        /// Contrutor Defaut
        /// </summary>
        /// <param name="cteService">Service Cte</param>
        /// <param name="carregamentoService"> Service Carregamento</param>
        public MonitoramentoCteController(CteService cteService, CarregamentoService carregamentoService)
        {
            _cteService = cteService;
            _carregamentoService = carregamentoService;
        }

        /// <summary>
        /// Acao Padrao ao ser chamado o controller
        /// </summary>
        /// <returns>View Padrao</returns>
        [Autorizar(Transacao = "CTEMONITORAMENTO")]
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Retorna a tela para informar os códigos dos vagões
        /// </summary>
        /// <returns>Action result com a tela</returns>
        [Autorizar(Transacao = "CTEMONITORAMENTO")]
        public ActionResult FormInformarVagoes()
        {
            return View();
        }

        /// <summary>
        /// Gera o pdf para Impressão 
        /// </summary>
        /// <param name="idCte">Identificador do cte</param>
        /// <returns>retorna o pdf de impressão</returns>
        [Autorizar(Transacao = "CTEMONITORAMENTO", Acao = "Imprimir")]
        public ActionResult Imprimir(string idCte)
        {
            string[] arrayCtes = idCte.Split('$');
            List<int> listaIds = new List<int>();
            PdfMerge merge = new PdfMerge();
            CteArquivo file = null;
            int auxIdCte = 0;

            foreach (string id in arrayCtes)
            {
                if (!string.IsNullOrEmpty(id))
                {
                    auxIdCte = int.Parse(id);
                    file = _cteService.ObterArquivoCte(auxIdCte);

                    if (file != null)
                    {
                        listaIds.Add(auxIdCte);
                        merge.AddFile(file.ArquivoPdf);
                    }
                }
            }

            try
            {
                Stream returnContent = merge.Execute();
                FileStreamResult fsr = new FileStreamResult(returnContent, "application/pdf");
                fsr.FileDownloadName = "dacte.pdf";

                _cteService.ImprimirCtes(listaIds);

                return fsr;
            }
            catch (Exception ex)
            {
                return View("Index");
            }
        }

        /// <summary>
        /// Obtem uma lista de Ctes
        /// </summary>
        /// <param name="pagination"> Paginacao pesquisa</param>
        /// <param name="filter"> Filtros para pesquisa</param>
        /// <returns>Lista de Ctes</returns>
        [Autorizar(Transacao = "CTEMONITORAMENTO", Acao = "Pesquisar")]
        public JsonResult ObterCtes(DetalhesPaginacaoWeb pagination, DetalhesFiltro<object>[] filter)
        {
            ResultadoPaginado<CteDto> result = _cteService.RetornaCteMonitoramento(pagination, filter);

            return Json(new
            {
                result.Total,
                Items = result.Items.Select(c => new
                {
                    c.CteId,
                    c.Fluxo,
                    c.Origem,
                    c.Destino,
                    c.Mercadoria,
                    c.Chave,
                    c.Cte,
                    SituacaoCte = ALL.Core.Util.Enum<SituacaoCteEnum>.GetDescriptionOf(c.SituacaoCte),
                    DateEmissao = c.DataEmissao.ToString("dd/MM/yyyy"),
                    c.CodigoUsuario,
                    c.CodigoErro,
                    c.FerroviaOrigem,
                    c.FerroviaDestino,
                    CodigoVagao = c.CodigoVagao ?? "COMPLEMENTAR",
                    c.UfOrigem,
                    c.UfDestino,
                    Serie = c.SerieDesp6 == 0 && c.NumDesp6 == 0 ? c.SerieDesp5 != null ? c.SerieDesp5.ToString().PadLeft(3, '0') : string.Empty : string.Concat(c.CodigoControle, "-", c.SerieDesp6.ToString().PadLeft(3, '0')),
                    Despacho = c.SerieDesp6 == 0 && c.NumDesp6 == 0 ? c.NumDesp5.ToString().PadLeft(3, '0') : c.NumDesp6.ToString().PadLeft(3, '0'),
                    SerieDesp5 = c.SerieDesp5 != null ? c.SerieDesp5.ToString().PadLeft(3, '0') : string.Empty,
                    Desp5 = c.NumDesp5.ToString().PadLeft(3, '0'),
                    c.StatusCte,
                    c.ArquivoPdfGerado,
                    c.AcaoSerTomada,
                    c.Impresso,
                    ReemitirCancelamento = c.ReemitirCancelamento > 0,
                    c.QtdeAutorizacoes,
                    SituacaoCteAnulacao = ALL.Core.Util.Enum<SituacaoCteEnum>.GetDescriptionOf(c.SituacaoCteAnulacao),
                }),
                success = true
            });
        }

        [Autorizar(Transacao = "CTEMONITORAMENTO", Acao = "Pesquisar")]
        public ActionResult ObterCtesExportar(string dataInicial,
            string dataFinal,
            string serie,
            string despacho,
            string fluxo,
            string chave,
            string errocte,
            string origem,
            string destino,
            string impresso,
            string ufdcl,
            string vagoes)
        {
            DetalhesFiltro<object>[] filtros = new DetalhesFiltro<object>[12];
            filtros[0] = new DetalhesFiltro<object>();
            filtros[0].Campo = "dataInicial";
            filtros[0].Valor = new object[1];
            filtros[0].Valor[0] = dataInicial;

            filtros[1] = new DetalhesFiltro<object>();
            filtros[1].Campo = "dataFinal";
            filtros[1].Valor = new object[1];
            filtros[1].Valor[0] = dataFinal;

            filtros[2] = new DetalhesFiltro<object>();
            filtros[2].Campo = "serie";
            filtros[2].Valor = new object[1];
            filtros[2].Valor[0] = serie;

            filtros[3] = new DetalhesFiltro<object>();
            filtros[3].Campo = "despacho";
            filtros[3].Valor = new object[1];
            filtros[3].Valor[0] = despacho;

            filtros[4] = new DetalhesFiltro<object>();
            filtros[4].Campo = "fluxo";
            filtros[4].Valor = new object[1];
            filtros[4].Valor[0] = fluxo;

            filtros[5] = new DetalhesFiltro<object>();
            filtros[5].Campo = "chave";
            filtros[5].Valor = new object[1];
            filtros[5].Valor[0] = chave;

            filtros[6] = new DetalhesFiltro<object>();
            filtros[6].Campo = "errocte";
            filtros[6].Valor = new object[1];
            filtros[6].Valor[0] = errocte;

            filtros[7] = new DetalhesFiltro<object>();
            filtros[7].Campo = "Origem";
            filtros[7].Valor = new object[1];
            filtros[7].Valor[0] = origem;

            filtros[8] = new DetalhesFiltro<object>();
            filtros[8].Campo = "Destino";
            filtros[8].Valor = new object[1];
            filtros[8].Valor[0] = destino;

            filtros[9] = new DetalhesFiltro<object>();
            filtros[9].Campo = "Vagao";
            filtros[9].Valor = new object[1];
            filtros[9].Valor[0] = vagoes;

            filtros[10] = new DetalhesFiltro<object>();
            filtros[10].Campo = "Impresso";
            filtros[10].Valor = new object[1];
            filtros[10].Valor[0] = impresso;

            filtros[11] = new DetalhesFiltro<object>();
            filtros[11].Campo = "UfDcl";
            filtros[11].Valor = new object[1];
            filtros[11].Valor[0] = ufdcl;

            var pagination = new DetalhesPaginacaoWeb();
            pagination.Limite = 10000;
            pagination.Inicio = 0;

            var resultado = _cteService.RetornaCteMonitoramento(pagination, filtros);

            var colunas = new List<ExcelColuna<CteDto>>
            {
                new ExcelColuna<CteDto>("Situação", c => BuscarDescricaoSituacaoCte(ALL.Core.Util.Enum<SituacaoCteEnum>.GetDescriptionOf(c.SituacaoCte), ALL.Core.Util.Enum<SituacaoCteEnum>.GetDescriptionOf(c.SituacaoCteAnulacao))), 
                new ExcelColunaInteira<CteDto>("Num Vagão", c => c.CodigoVagao ?? "COMPLEMENTAR"),
                new ExcelColuna<CteDto>("Série", c =>  c.SerieDesp6 == 0 && c.NumDesp6 == 0 ? c.SerieDesp5 != null ? c.SerieDesp5.ToString().PadLeft(3, '0') : string.Empty : string.Concat(c.CodigoControle, "-", c.SerieDesp6.ToString().PadLeft(3, '0'))),
                new ExcelColunaInteira<CteDto>("Despacho", c => c.SerieDesp6 == 0 && c.NumDesp6 == 0 ? c.NumDesp5.ToString().PadLeft(3, '0') : c.NumDesp6.ToString().PadLeft(3, '0')),
                new ExcelColuna<CteDto>("CTe", c => c.Cte),
                new ExcelColuna<CteDto>("Origem", c => c.Origem),
                new ExcelColuna<CteDto>("Destino", c => c.Destino),
                new ExcelColuna<CteDto>("Fluxo", c => c.Fluxo),
                new ExcelColuna<CteDto>("Mercadoria", c => c.Mercadoria),
                new ExcelColuna<CteDto>("Chave CTe", c => c.Chave),
                new ExcelColunaData<CteDto>("Data Emissão", c => c.DataEmissao),
                new ExcelColuna<CteDto>("Ferrovia Ori.", c => c.FerroviaOrigem),
                new ExcelColuna<CteDto>("Ferrovia Dest.", c => c.FerroviaDestino),
                new ExcelColuna<CteDto>("UF Ori.", c => c.UfOrigem),
                new ExcelColuna<CteDto>("UF Dest.", c => c.UfDestino),
                new ExcelColuna<CteDto>("SerieDesp5", c => c.SerieDesp5 != null ? c.SerieDesp5.ToString().PadLeft(3, '0') : string.Empty),
                new ExcelColuna<CteDto>("Desp5", c => c.NumDesp5.ToString().PadLeft(3, '0')),
            };

            return ExcelHelper.Exportar(
             "MonitoramentoCte.xlsx",
             colunas,
             resultado.Items,
             ws =>
             {
                 ws.Cells.AutoFitColumns();
             });

        }

        /// <summary>
        /// Obtem uma lista de Ctes
        /// </summary>
        /// <param name="idcte">Id do Cte pai</param>
        /// <returns>Lista de Ctes do agrupamento </returns>
        [Autorizar(Transacao = "CTEMONITORAMENTO", Acao = "Pesquisar")]
        public JsonResult ObterAgrupamentoCte(int idcte)
        {
            IList<CteDto> result = _cteService.RetornaCteMonitoramentoAgrupamento(idcte);

            return Json(new
            {
                Items = result.Select(c => new
                {
                    c.CteId,
                    c.Fluxo,
                    c.Origem,
                    c.Destino,
                    c.Mercadoria,
                    c.Chave,
                    c.Cte,
                    SituacaoCte = ALL.Core.Util.Enum<SituacaoCteEnum>.GetDescriptionOf(c.SituacaoCte),
                    DateEmissao = c.DataEmissao.ToString("dd/MM/yyyy"),
                    c.CodigoUsuario,
                    c.CodigoErro,
                    c.FerroviaOrigem,
                    c.FerroviaDestino,
                    c.UfOrigem,
                    c.UfDestino,
                    c.SerieDesp5,
                    c.NumDesp5,
                    c.SerieDesp6,
                    c.NumDesp6,
                    c.StatusCte,
                    c.ArquivoPdfGerado
                }),
                success = true
            });
        }

        /// <summary>
        /// Obtem uma lista de Ctes
        /// </summary>
        /// <returns>Lista de Ctes do agrupamento </returns>
        [Autorizar(Transacao = "CTEMONITORAMENTO", Acao = "Pesquisar")]
        public JsonResult ObterCodigoSerieDesp()
        {
            IList<SerieDespachoUf> result = _carregamentoService.ObterCodigoSerie();

            // Inseri item default em branco no list
            var itemDefault = new SerieDespachoUf();
            itemDefault.CodigoControle = " ";

            result.Insert(0, itemDefault);

            return Json(new
            {
                Items = result.Select(c => new
                {
                    c.Id,
                    c.CodigoControle
                }),
                success = true
            });
        }

        /// <summary>
        /// Obtem uma lista de Ctes
        /// </summary>
        /// <param name="idcte">Id do Cte pai</param>
        /// <returns>Lista de Ctes do agrupamento </returns>
        [Autorizar(Transacao = "CTEMONITORAMENTO", Acao = "Pesquisar")]
        public JsonResult ObterHistoricoStatusCte(int idcte)
        {
            var result = _cteService.RetornaCteMonitoramentoStatusCte(idcte);

            /*
            IList<CteStatusRetorno> listStR = new List<CteStatusRetorno>();
            foreach (var cteStatuse in result)
            {
                listStR = listStR.Concat(_cteService.ObterCteStatusRetornoPorId(cteStatuse.XmlRetorno)).ToList();
            }
            */

            var resultItems = result.Select(c => new
            {
                DescricaoStatus = c.CteStatusRetorno.Id.ToString() + " - " + (c.CteStatusRetorno.Id == 14
                                        ? c.XmlRetorno
                                        : c.CteStatusRetorno.Id == 4001
                                            ? c.Cte.XmlAutorizacao
                                            : c.CteStatusRetorno.Descricao),

                DateEmissao = c.DataHora.ToString("dd/MM/yyyy HH:mm:ss"),
                AcaoTomada = c.CteStatusRetorno.Id == 14 ? c.XmlRetorno : c.CteStatusRetorno.AcaoSerTomada,

                Situacao = Translogic.Core.Commons.Enum<SituacaoCteEnum>.GetDescriptionOf(c.Cte.SituacaoAtual)
            });
            return Json(new
            {
                Items = resultItems,
                success = true
            });
        }

        /// <summary>
        /// Realiza o Reemissão dos Ctes escolhidos
        /// </summary>
        /// <param name="ids">ids dos ctes</param>
        /// <returns>resultado do cancelamento</returns>
        [Autorizar(Transacao = "CTEMONITORAMENTO", Acao = "Reenviar")]
        [JsonFilter(Param = "ids", JsonDataType = typeof(int[]))]
        public JsonResult ReemissaoCancelamento(int[] ids)
        {
            try
            {
                if (ids.Length > 0)
                {
                    _cteService.ReemissaoCancelamentoCtes(ids, UsuarioAtual);
                }

                return Json(new { success = true, Message = "SUCESSO" });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, Message = string.Format("Falha na Reemissão do Cancelamento: {0}", ex.Message) });
            }
        }

        /// <summary>
        /// Realiza o Reemissão dos Ctes escolhidos
        /// </summary>
        /// <param name="ids">ids dos ctes</param>
        /// <returns>resultado do cancelamento</returns>
        [Autorizar(Transacao = "CTEMONITORAMENTO", Acao = "Reenviar")]
        [JsonFilter(Param = "ids", JsonDataType = typeof(int[]))]
        public JsonResult Reemissao(int[] ids)
        {
            string mensagemErro = string.Empty;
            bool status = true;
            List<int> idCtes = new List<int>();

            foreach (int id in ids)
            {
                if (_cteService.VerificarCteFolha(id))
                {
                    idCtes.Add(id);
                }
            }

            if (idCtes.Count > 0)
            {
                status = _cteService.ReemissaoCtes(idCtes.ToArray(), UsuarioAtual, out mensagemErro);
            }

            if (status)
            {
                return Json(new { success = true, Message = "SUCESSO" });
            }

            // return Json(new { success = false, Message = "FALHA" });
            return Json(new { success = false, Message = string.Format("Falha na Reemissão: {0}", mensagemErro) });
        }

        /// <summary>
        /// Realiza o Reemissão dos Ctes escolhidos
        /// </summary>
        /// <param name="ids">ids dos ctes</param>
        /// <returns>resultado do cancelamento</returns>
        [Autorizar(Transacao = "CTEMONITORAMENTO", Acao = "Inutilizar")]
        [JsonFilter(Param = "ids", JsonDataType = typeof(int[]))]
        public JsonResult Inutilizacao(int[] ids)
        {
            try
            {
                _cteService.SalvarInutilizacaoCtes(ids, UsuarioAtual);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, Message = string.Format("FALHA: {0}", ex.Message) });
            }

            return Json(new { success = true, Message = "SUCESSO" });
        }

        /// <summary>
        /// Abre tela de edição de nota fiscal
        /// </summary>
        /// <param name="arrayCtes"> Array com os id dos cte</param>
        /// <returns> Tela de edição de nota fiscal </returns>
        [Autorizar(Transacao = "CTEMONITORAMENTO", Acao = "Imprimir")]
        public ActionResult FormStatusImpressao(int[] arrayCtes)
        {
            ViewData["ID_CTE_IMPRESSAO"] = arrayCtes;

            return View();
        }

        /// <summary>
        /// Abre tela de edição de nota fiscal
        /// </summary>
        /// <param name="arrayCtes"> Array com os id dos cte</param>
        /// <returns> Tela de edição de nota fiscal </returns>
        // [Autorizar(Transacao = "CTEMANUTENCAO", Acao = "Imprimir")]
        public JsonResult ObterCteImpressao(int[] arrayCtes)
        {
            IList<Cte> result = _cteService.ObterCteImpressao(arrayCtes);

            return Json(new
            {
                Items = result.Select(c => new
                {
                    CteId = c.Id,
                    NroCte = c.Numero,
                    Status = c.ArquivoPdfGerado
                }),
                success = true
            });
        }

        /// <summary>
        /// Retorna se os vagões estão válidos
        /// </summary>
        /// <param name="listaVagoes">Lista de vagões</param>
        /// <returns>Action result com a tela</returns>
        public ActionResult ValidarVagoes(List<string> listaVagoes)
        {
            Dictionary<string, KeyValuePair<bool, string>> result = _cteService.VerificarListaVagoes(listaVagoes);

            return Json(new
            {
                Items = result.Select(c => new
                {
                    Vagao = c.Key,
                    Erro = c.Value.Key,
                    Mensagem = c.Value.Value
                }),
                success = true
            });
        }

        /// <summary>
        /// Buscar descrições da situação
        /// </summary>
        /// <param name="situacao"></param>
        /// <returns></returns>
        public string BuscarDescricaoSituacaoCte(string situacao, string situacaoAnulacao)
        {
            switch (situacao)
            {
                case "AUT":
                    return "CTe Aprovado";
                    break;
                case "AGI":
                    return "Aguardando Cancelamento / Inutilização";
                    break;
                case "EAE":
                    return "Enviado arquivo CTe para Sefaz";
                    break;
                case "EAR":
                    return "Erro Autorizado reenvio do CTe";
                    break;
                case "CAN":
                    return "CTe Cancelado";
                    break;
                case "INT":
                    return "CTe Invalidado";
                    break;
                case "AGC":
                    return "Aguardando cancelamento";
                    break;
                case "AUC":
                    return "Autorizado cancelamento";
                    break;
                case "ERR":
                    return "Erro na geração do CTe";
                    break;
                case "PCC":
                    return "Cte aguardando a geração automática da numeração / chave";
                    break;
                case "ANL":
                    return "CTe Anulado";
                    break;
                case "ANC":
                    {
                        if (situacaoAnulacao == "AUT")
                        {
                            return "CTe anulado na SEFAZ";
                        }
                        else
                        {
                            return "CTe enviado anulação para config";
                        }
                    }
                default:
                    return "Cte em processamento";
                    break;
            }
        }

    }
}