namespace Translogic.Modules.Core.Controllers.Cte
{
	using System.Collections.Generic;
	using System.IO;
	using System.Linq;
	using System.Web.Mvc;
	using ALL.Core.Dominio;
	using Domain.Model.Diversos.Cte;
	using Domain.Model.Dto;
	using Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
	using Domain.Model.FluxosComerciais.Pedidos.Despachos;
	using Domain.Services.Ctes;
	using Domain.Services.FluxosComerciais;
	using Infrastructure;
	using Translogic.Core.Infrastructure.Web;
	using Translogic.Core.Infrastructure.Web.Filters;

	/// <summary>
	/// Classe GeracaoArquivoLoteController
	/// </summary>
	public class GeracaoArquivoLoteController : BaseSecureModuleController
	{
		private readonly CteService _cteService;
		private readonly CarregamentoService _carregamentoService;

		/// <summary>
		/// Construtor da Classe
		/// </summary>
		/// <param name="cteService">Servi�o CteService</param>
		/// <param name="carregamentoService"> Service Carregamento</param>
		public GeracaoArquivoLoteController(CteService cteService, CarregamentoService carregamentoService)
		{
			_cteService = cteService;
			_carregamentoService = carregamentoService;
		}

		/// <summary>
		/// Acao Padrao ao ser chamado o controller
		/// </summary>
		/// <returns>Retorna a view default</returns>
		[Autorizar(Transacao = "CTEGERARARQUIVOLOTE")]
		public ActionResult Index()
		{
			return View();
		}

		/// <summary>
		/// Obtem uma lista de Ctes
		/// </summary>
		/// <returns>Lista de Ctes do agrupamento </returns>
		[Autorizar(Transacao = "CTEGERARARQUIVOLOTE", Acao = "Pesquisar")]
		public JsonResult ObterCodigoSerieDesp()
		{
			IList<SerieDespachoUf> result = _carregamentoService.ObterCodigoSerie();

			// Inseri item default em branco no list
			var itemDefault = new SerieDespachoUf();
			itemDefault.CodigoControle = " ";

			result.Insert(0, itemDefault);

			return Json(new
			{
				Items = result.Select(c => new
				{
					c.Id,
					c.CodigoControle
				}),
				success = true
			});
		}

		/// <summary>
		/// Obtem uma lista de Ctes
		/// </summary>
		/// <param name="idcte">Id do Cte pai</param>
		/// <returns>Lista de Ctes do agrupamento </returns>
		[Autorizar(Transacao = "CTEGERARARQUIVOLOTE", Acao = "Pesquisar")]
		public JsonResult ObterHistoricoStatusCte(int idcte)
		{
			IList<CteStatus> result = _cteService.RetornaCteMonitoramentoStatusCte(idcte);

			return Json(new
			{
				Items = result.Select(c => new
				{
					DescricaoStatus = c.CteStatusRetorno.Id.ToString() + " - " + (c.CteStatusRetorno.Id == 14 ? c.XmlRetorno : c.CteStatusRetorno.Id == 4001 ? c.Cte.XmlAutorizacao : c.CteStatusRetorno.Descricao),
					DateEmissao = c.DataHora.ToString("dd/MM/yyyy HH:mm:ss"),
					AcaoTomada = c.CteStatusRetorno.AcaoSerTomada,
					Situacao = Translogic.Core.Commons.Enum<SituacaoCteEnum>.GetDescriptionOf(c.Cte.SituacaoAtual)
				}),
				success = true
			});
		}

		/// <summary>
		/// Obtem uma lista de Ctes
		/// </summary>
		/// <param name="pagination"> Paginacao pesquisa</param>
		/// <param name="filter"> Filtros para pesquisa</param>		
		/// <returns>Lista de Ctes</returns>
		[Autorizar(Transacao = "CTEGERARARQUIVOLOTE", Acao = "Pesquisar")]
		public JsonResult ObterCtes(DetalhesPaginacaoWeb pagination, DetalhesFiltro<object>[] filter)
		{
		    pagination.Limit = 100;
			ResultadoPaginado<CteDto> result = _cteService.RetornaCteGerarArquivoLote(pagination, filter);

			return Json(new
			{
				result.Total,
				Items = result.Items.Select(c => new
				{
					c.CteId,
					c.Fluxo,
					c.Origem,
					c.Destino,
					c.Mercadoria,
					c.Chave,
					c.Cte,
					SituacaoCte = ALL.Core.Util.Enum<SituacaoCteEnum>.GetDescriptionOf(c.SituacaoCte),
					DateEmissao = c.DataEmissao.ToString("dd/MM/yyyy"),
					c.CodigoUsuario,
					c.CodigoErro,
					c.FerroviaOrigem,
					c.FerroviaDestino,
					CodigoVagao = c.CodigoVagao ?? "COMPLEMENTAR",
					c.UfOrigem,
					c.UfDestino,
					Serie = c.SerieDesp6 == 0 && c.NumDesp6 == 0 ? c.SerieDesp5 != null ? c.SerieDesp5.ToString().PadLeft(3, '0') : string.Empty : string.Concat(c.CodigoControle, "-", c.SerieDesp6.ToString().PadLeft(3, '0')),
					Despacho = c.SerieDesp6 == 0 && c.NumDesp6 == 0 ? c.NumDesp5.ToString().PadLeft(3, '0') : c.NumDesp6.ToString().PadLeft(3, '0'),
					SerieDesp5 = c.SerieDesp5 != null ? c.SerieDesp5.ToString().PadLeft(3, '0') : string.Empty,
					Desp5 = c.NumDesp5.ToString().PadLeft(3, '0'),
					c.StatusCte,
					c.ArquivoPdfGerado,
					c.AcaoSerTomada,
					c.Impresso
				}),
				success = true
			});
		}

		/// <summary>
		/// Realiza o Reemiss�o dos Ctes escolhidos
		/// </summary>
		/// <param name="ids">ids dos ctes</param>
		/// <returns>resultado do cancelamento</returns>
		[Autorizar(Transacao = "CTEGERARARQUIVOLOTE", Acao = "Reenviar")]
		public ActionResult GerarZipPdf(string ids)
		{
			var arrIds = ids.Split(',');
			FileStreamResult fsr = null;
			MemoryStream returnContent = _cteService.GerarArquivoZipLote(arrIds, TipoArquivoEnvioEnum.Pdf);

			if (returnContent != null)
			{
				fsr = new FileStreamResult(returnContent, "application/zip");
				fsr.FileDownloadName = "dactePdf.zip";
			}

			return fsr;
		}

		/// <summary>
		/// Realiza o Reemiss�o dos Ctes escolhidos
		/// </summary>
		/// <param name="ids">ids dos ctes</param>
		/// <returns>resultado do cancelamento</returns>
		[Autorizar(Transacao = "CTEGERARARQUIVOLOTE", Acao = "Reenviar")]
		public ActionResult GerarZipXml(string ids)
		{
			var arrIds = ids.Split(',');
			FileStreamResult fsr = null;
			MemoryStream returnContent = _cteService.GerarArquivoZipLote(arrIds, TipoArquivoEnvioEnum.Xml);

			if (returnContent != null)
			{
				fsr = new FileStreamResult(returnContent, "application/zip");
				fsr.FileDownloadName = "dacteXml.zip";
			}

			return fsr;
		}

		/// <summary>
		/// Retorna a tela para informar as chaves nfe
		/// </summary>
		/// <returns>Action result com a tela</returns>
		public ActionResult FormInformarChaves()
		{
			return View();
		}

        /// <summary>
        /// Retorna a tela para informar os c�digos dos vag�es
        /// </summary>
        /// <returns>Action result com a tela</returns>
        [Autorizar(Transacao = "CTEMONITORAMENTO")]
        public ActionResult FormInformarVagoes()
        {
            return View();
        }

		/// <summary>
		/// Retorna se as chaves sao validas
		/// </summary>
		/// <param name="listaChave">Lista de chaves</param>
		/// <returns>Action result com a tela</returns>
		public ActionResult ValidarChaves(List<string> listaChave)
		{
			Dictionary<string, KeyValuePair<bool, string>> result = _cteService.VerificarCteFolhaArvorePorListaChave(listaChave);

			return Json(new
			{
				Items = result.Select(c => new
				{
					Chave = c.Key,
					Erro = c.Value.Key,
					Mensagem = c.Value.Value
				}),
				success = true
			});
		}

        /// <summary>
        /// Retorna se os vag�es est�o v�lidos
        /// </summary>
        /// <param name="listaVagoes">Lista de vag�es</param>
        /// <returns>Action result com a tela</returns>
        public ActionResult ValidarVagoes(List<string> listaVagoes)
        {
            Dictionary<string, KeyValuePair<bool, string>> result = _cteService.VerificarListaVagoes(listaVagoes);

            return Json(new
            {
                Items = result.Select(c => new
                {
                    Vagao = c.Key,
                    Erro = c.Value.Key,
                    Mensagem = c.Value.Value
                }),
                success = true
            });
        }
	}
}