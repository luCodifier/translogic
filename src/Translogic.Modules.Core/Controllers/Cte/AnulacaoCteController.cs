﻿using Microsoft.Practices.ServiceLocation;
using Translogic.Modules.Core.Domain.DataAccess.Repositories.Codificador;
using Translogic.Modules.Core.Domain.Model.Codificador;
using Translogic.Modules.Core.Domain.Model.Codificador.Repositories;
using Translogic.Modules.Core.Domain.Services.Acesso;

namespace Translogic.Modules.Core.Controllers.Cte
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Web.Mvc;
    using ALL.Core.Dominio;
    using Translogic.Core.Infrastructure.Web;
    using Translogic.Core.Infrastructure.Web.Filters;
    using Translogic.Modules.Core.Domain.Model.Diversos.Cte;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Modules.Core.Domain.Model.Estrutura;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Nfes;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;
    using Translogic.Modules.Core.Domain.Services.Ctes;
    using Translogic.Modules.Core.Domain.Services.FluxosComerciais;
    using Translogic.Modules.Core.Infrastructure;
    using Translogic.Modules.Core.Util;

    /// <summary>
    /// Controller AnulacaoCteController
    /// </summary>
    public class AnulacaoCteController : BaseSecureModuleController
    {
        private readonly CteService _cteService;
        private readonly AnulacaoService _AnulacaoService;
        private readonly NfeAnulacaoService _nfeAnulacaoService;
        private readonly IConfiguracaoTranslogicRepository _configuracaoTranslogicRepository;
        private readonly ICteRepository _cteRepository;

        /// <summary>
        /// Contrutor Defaut
        /// </summary>
        /// <param name="cteService">Service Cte</param>
        /// <param name="anulacaoService">Serviço de Carregamento</param>
        /// <param name="nfeAnulacaoService">Serviço de anulação de nfe.</param>
        /// <param name="cteRepository">Repository do cte.</param>
        public AnulacaoCteController(CteService cteService, AnulacaoService anulacaoService, NfeAnulacaoService nfeAnulacaoService, IConfiguracaoTranslogicRepository configuracaoTranslogicRepository, ICteRepository cteRepository)
        {
            _cteService = cteService;
            _nfeAnulacaoService = nfeAnulacaoService;
            _AnulacaoService = anulacaoService;
            _configuracaoTranslogicRepository = configuracaoTranslogicRepository;
            _cteRepository = cteRepository;
        }

        /// <summary>
        /// ActionResult Index
        /// </summary>
        /// <returns>View Index</returns>
        [Autorizar(Transacao = "ANULACAOCTE")]
        public ActionResult Index()
        {
            double margem = _AnulacaoService.ObterMargemNotaAnulacao();
            ViewData["MARGEM_LIB_ANULACAO"] = margem;

            var verificaEventoDesacordo = ServiceLocator.Current.GetInstance<ControleAcessoService>().Autorizar("ANULACAOCTE", "AnularSefaz", UsuarioAtual);
            //var verificaEventoDesacordo = ObterValorConfGeral("CTE_EVENTO_DESACORDO");
            ViewData["CTE_EVENTO_DESACORDO"] = verificaEventoDesacordo.ToString();


            var versaoCte = ObterValorConfGeral("CTE_VERSAO_SEFAZ");
            ViewData["CTE_VERSAO_SEFAZ"] = versaoCte.ToString();

            return View();
        }

        /// <summary>
        /// Retorna a tela para informar as chaves nfe
        /// </summary>
        /// <returns>Action result com a tela</returns>
        [Autorizar(Transacao = "ANULACAOCTE", Acao = "Pesquisar")]
        public ActionResult FormInformarChaves()
        {
            return View();
        }

        /// <summary>
        /// Retorna se as chaves sao validas
        /// </summary>
        /// <param name="listaChave">Lista de chaves</param>
        /// <returns>Action result com a tela</returns>
        [Autorizar(Transacao = "ANULACAOCTE", Acao = "Pesquisar")]
        public ActionResult ValidarChaves(List<string> listaChave)
        {
            Dictionary<string, KeyValuePair<bool, string>> result = _cteService.VerificarCtePorListaChave(listaChave, true);

            return Json(new
            {
                Items = result.Select(c => new
                {
                    Chave = c.Key,
                    Erro = c.Value.Key,
                    Mensagem = c.Value.Value
                }),
                success = true
            });
        }

        /// <summary>
        /// Obtem uma lista de Ctes
        /// </summary>
        /// <param name="pagination"> Paginacao pesquisa</param>
        /// <param name="filter"> Filtros para pesquisa</param>
        /// <returns>Lista de Ctes</returns>
        [Autorizar(Transacao = "ANULACAOCTE", Acao = "Pesquisar")]
        public JsonResult ObterCtes(DetalhesPaginacaoWeb pagination, DetalhesFiltro<object>[] filter)
        {
            ResultadoPaginado<CteDto> result = _cteService.RetornaCteAnulacao(pagination, filter);

            return Json(new
            {
                result.Total,
                Items = result.Items.Select(c => new
                {
                    c.CteId,
                    c.Fluxo,
                    c.Origem,
                    c.Destino,
                    c.Mercadoria,
                    c.Chave,
                    c.Cte,
                    c.SerieCte,
                    c.ValorCte,
                    DateEmissao = c.DataEmissao.ToString("dd/MM/yyyy"),
                    c.CodigoUsuario,
                    c.CodigoErro,
                    c.FerroviaOrigem,
                    c.FerroviaDestino,
                    CodigoVagao = c.CodigoVagao ?? "COMPLEMENTAR",
                    c.UfOrigem,
                    c.UfDestino,
                    Serie = c.SerieDesp6 == 0 && c.NumDesp6 == 0 ? c.SerieDesp5 != null ? c.SerieDesp5.ToString().PadLeft(3, '0') : string.Empty : string.Concat(c.CodigoControle, "-", c.SerieDesp6.ToString().PadLeft(3, '0')),
                    Despacho = c.SerieDesp6 == 0 && c.NumDesp6 == 0 ? c.NumDesp5.ToString().PadLeft(3, '0') : c.NumDesp6.ToString().PadLeft(3, '0'),
                    SerieDesp5 = c.SerieDesp5 != null ? c.SerieDesp5.ToString().PadLeft(3, '0') : string.Empty,
                    Desp5 = c.NumDesp5.ToString().PadLeft(3, '0'),
                    c.StatusCte,
                    c.ArquivoPdfGerado,
                    c.AcaoSerTomada,
                    c.Impresso,
                    c.Acima168hs,
                    c.Descarregado,
                    PermiteCancRejeitado = c.PermiteCancRejeitado.HasValue ? c.PermiteCancRejeitado : true,
                    c.RateioCte,
                    c.IndIeToma,
                    c.PapelTomador
                }),
                success = true
            });
        }

        /// <summary>
        /// Obter dados do cnpj
        /// </summary>
        /// <param name="cnpj">cnpj da empresa</param>
        /// <returns>Resultado Json</returns>
        [Autorizar(Transacao = "ANULACAOCTE", Acao = "Pesquisar")]
        public JsonResult ObterDadosCnpj(string cnpj)
        {
            bool erro = false;
            object dados = null;
            string mensagem = string.Empty;
            long aux;
            bool exterior = false;

            try
            {
                if (long.TryParse(cnpj.Trim(), out aux))
                {
                    // Caso seja 0, então é fluxo internacional
                    if (aux == 0)
                    {
                        erro = false;
                        mensagem = "CNPJ Destino internacional";
                        exterior = true;
                    }
                    else
                    {
                        // tenta recuperar os dados da empresa
                        IEmpresa empresa = _AnulacaoService.ObterEmpresaPorCnpj(cnpj);

                        if (empresa == null)
                        {
                            erro = true;
                            mensagem = "Não foi encontrada nenhuma empresa para este CNPJ.";
                        }
                        else
                        {
                            dados = new
                            {
                                Nome = empresa.RazaoSocial,
                                InscricaoEstadual = empresa.InscricaoEstadual,
                                Sigla = empresa.Sigla,
                                Uf = empresa.Estado.Sigla
                            };
                        }
                    }
                }
            }
            catch (Exception e)
            {
                mensagem = TratarException(e);
                erro = true;
            }

            return Json(new
            {
                Erro = erro,
                Mensagem = mensagem,
                Dados = dados,
                Exterior = exterior
            });
        }

        /// <summary>
        /// Autorizado o cancelamento Rejeitado do Cte
        /// </summary>
        /// <param name="listaCte"> Lista de Id de Cte</param>
        /// <returns>Status do processamento</returns>
        [Autorizar(Transacao = "ANULACAOCTE", Acao = "Salvar")]
        [JsonFilter(Param = "listaCte", JsonDataType = typeof(List<CteDto>))]
        public JsonResult AutorizarCancelamentoRejeitado(List<CteDto> listaCte)
        {
            var listaIdCte = listaCte.Select(x => x.IdCte).ToList();

            var erros = _cteService.AutorizarCancelamentoRejeitado(listaCte, UsuarioAtual);

            return Json(new
            {
                success = string.IsNullOrEmpty(erros.ToString()),
                erros = erros.ToString()
            });
        }

        /// <summary>
        /// Obtém os dados da NFe
        /// </summary>
        /// <param name="chaveNfe"> Chave da Nfe</param>
        /// <returns> Resultado JSON </returns>
        [Autorizar(Transacao = "ANULACAOCTE", Acao = "Pesquisar")]
        public JsonResult ObterDadosNfe(string chaveNfe)
        {
            INotaFiscalEletronica nfe;
            string mensagem = string.Empty;
            string stackTrace = string.Empty;
            object dados = null;
            bool erro = true;
            bool indLiberaDigitacao = false;
            string codigoStatusRetorno;
            StatusNfeDto statusNfe;

            // Se não, verifica se a nota esta no banco apenas
            statusNfe = _nfeAnulacaoService.ObterDadosNfe(chaveNfe, TelaProcessamentoNfe.Carregamento, true);

            codigoStatusRetorno = statusNfe.CodigoStatusRetorno;

            if (!statusNfe.ObtidaComSucesso)
            {
                erro = true;
                mensagem = statusNfe.MensagemErro;
                stackTrace = statusNfe.StackTrace;
                indLiberaDigitacao = statusNfe.IndLiberaDigitacao;
            }
            else
            {
                nfe = statusNfe.NotaFiscalEletronica;
                try
                {
                    IEmpresa empresaEmitente = _AnulacaoService.ObterEmpresaPorCnpj(nfe.CnpjEmitente);
                    IEmpresa empresaDestinatario = _AnulacaoService.ObterEmpresaPorCnpj(nfe.CnpjDestinatario);

                    if (_AnulacaoService.ValidaNfeJaUtilizada(chaveNfe))
                    {
                        erro = true;
                        mensagem = string.Format("A Nota({0}) já foi utlizada para anulação. Favor informar outra nota.", chaveNfe);
                        stackTrace = string.Empty;
                        indLiberaDigitacao = false;
                    }
                    else if (empresaEmitente == null || empresaDestinatario == null)
                    {
                        erro = true;
                        mensagem = "Empresa não encontrada.";
                        stackTrace = string.Empty;
                        indLiberaDigitacao = true;
                    }
                    else
                    {
                        double? pesoTotal = statusNfe.Peso > 0 ? Tools.TruncateValue(statusNfe.Peso, TipoTruncateValueEnum.UnidadeMilhar) : (double?)null;
                        double? volume = statusNfe.Volume > 0 ? statusNfe.Volume : null;

                        double? pesoUtilizado = statusNfe.PesoUtilizado > 0 ? Tools.TruncateValue(statusNfe.PesoUtilizado, TipoTruncateValueEnum.UnidadeMilhar) : (double?)null;
                        double? volumeUtilizado = statusNfe.VolumeUtilizado > 0 ? statusNfe.VolumeUtilizado : null;

                        erro = false;
                        dados = new
                        {
                            nfe.SerieNotaFiscal,
                            nfe.NumeroNotaFiscal,
                            Peso = pesoTotal,
                            Volume = volume.HasValue ? Tools.TruncateValue(volume.Value, TipoTruncateValueEnum.UnidadeMilhar) : volume,
                            PesoUtilizado = pesoUtilizado,
                            VolumeUtilizado = volumeUtilizado.HasValue ? Tools.TruncateValue(volumeUtilizado.Value, TipoTruncateValueEnum.UnidadeMilhar) : volumeUtilizado,
                            nfe.Valor,
                            Remetente = nfe.RazaoSocialEmitente,
                            Destinatario = nfe.RazaoSocialDestinatario,
                            SiglaRemetente = empresaEmitente.Sigla,
                            SiglaDestinatario = empresaDestinatario.Sigla,
                            CnpjRemetente = nfe.CnpjEmitente,
                            nfe.CnpjDestinatario,
                            DataNotaFiscal = nfe.DataEmissao.ToString("dd/MM/yyyy"),
                            nfe.InscricaoEstadualDestinatario,
                            InscricaoEstadualRemetente = nfe.InscricaoEstadualEmitente,
                            nfe.UfDestinatario,
                            UfRemetente = nfe.UfEmitente
                        };
                    }
                }
                catch (Exception e)
                {
                    mensagem = TratarException(e);
                    stackTrace = e.StackTrace;
                    erro = true;
                    indLiberaDigitacao = true;
                }
            }

            return Json(new
            {
                CodigoStatusRetorno = codigoStatusRetorno,
                Erro = erro,
                Mensagem = mensagem,
                DadosNfe = dados,
                StackTrace = stackTrace,
                IndLiberaDigitacao = indLiberaDigitacao
            });
        }

        /// <summary>
        /// Grava os dados da nota quando é obtida manualmente.
        /// </summary>
        /// <param name="chave">Chave da nota</param>
        /// <param name="serie">Serie da Nfe</param>
        /// <param name="numero">Numero da Nfe</param>
        /// <param name="valorTotal">Valor Total da Nota</param>
        /// <param name="remetente">Remetente da nota</param>
        /// <param name="destinatario">Destinatario da nota</param>
        /// <param name="cnpjRemetente">Cnpj do remetente da nota</param>
        /// <param name="cnpjDestinatario">Cnpj do Destinatario da nota</param>
        /// <param name="dataNotaFiscal">Data da Nota</param>
        /// <param name="siglaRemetente">Sigla do Remetente</param>
        /// <param name="siglaDestinatario">Sigla do Destinatario</param>
        /// <param name="estadoRemetente"> UF Remetente da Nota</param>
        /// <param name="estadoDestinatario"> UF Destinatario da nota</param>
        /// <param name="inscricaoEstadualRemetente">IE do Remetente</param>
        /// <param name="inscricaoEstadualDestinatario">IE do Destinatario</param>
        /// <param name="protocolo">protocolo de autorização de uso da nota no sefaz</param>
        /// <returns> Resultado Json </returns>
        [Autorizar(Transacao = "ANULACAOCTE", Acao = "Salvar")]
        public JsonResult ProcessarNfeManual(string chave, string serie, string numero, string valorTotal, string remetente, string destinatario, string cnpjRemetente, string cnpjDestinatario, string dataNotaFiscal, string siglaRemetente, string siglaDestinatario, string estadoRemetente, string estadoDestinatario, string inscricaoEstadualRemetente, string inscricaoEstadualDestinatario, string protocolo)
        {
            string mensagem = string.Empty;
            bool erro = false;

            try
            {
                _AnulacaoService.ProcessarNfeManual(chave, serie, numero, valorTotal, remetente, destinatario, cnpjRemetente, cnpjDestinatario, dataNotaFiscal, siglaRemetente, siglaDestinatario, estadoRemetente, estadoDestinatario, inscricaoEstadualRemetente, inscricaoEstadualDestinatario, protocolo, UsuarioAtual);
            }
            catch (Exception e)
            {
                mensagem = TratarException(e);
                erro = true;
            }

            return Json(new
            {
                Erro = erro,
                Mensagem = mensagem
            });
        }

        /// <summary>
        /// Retorna os dados da empresa
        /// </summary>
        /// <param name="cnpj">Cnpj da empresa</param>
        /// <param name="razaoSocial">Razao social da empresa</param>
        /// <returns>Retorna os dados da empresa pesquisada</returns>
        [Autorizar(Transacao = "ANULACAOCTE", Acao = "Pesquisar")]
        public JsonResult PesquisarEmpresas(string cnpj, string razaoSocial)
        {
            IList<EmpresaCliente> empresas = _AnulacaoService.ObterEmpresaPorCnpjRazaoSocial(cnpj, razaoSocial);

            return Json(new
            {
                totalCount = empresas.Count,
                Items = empresas.Select(g => new
                {
                    g.Id,
                    g.Sigla,
                    g.RazaoSocial,
                    g.InscricaoEstadual,
                    Cnpj = g.Cgc,
                    Uf = g.Estado == null ? string.Empty : g.Estado.Sigla
                })
            });
        }

        /// <summary>
        /// Abre tela de edição de nota fiscal
        /// </summary>
        /// <returns> Tela de edição de nota fiscal </returns>
        [Autorizar(Transacao = "ANULACAOCTE", Acao = "Pesquisar")]
        public ActionResult FormNotaFiscal()
        {
            return View();
        }


        [Autorizar(Transacao = "ANULACAOCTE", Acao = "Salvar")]
        [JsonFilter(Param = "dadosVerificacao", JsonDataType = typeof(List<CteDto>))]
        public JsonResult ObterCtesRateio(List<CteDto> ctes)
        {
            // Verifica nos ctes selecionados se algum deles é de rateio, e se é de rateio, busca quais os ctes rateados no conteiner.
            string mensagem = string.Empty;
            bool possuiMsg = false;
            var chavesMsgGeradas = new List<string>();

            if (ctes.Any(p => p.RateioCte))
            {
                mensagem = ctes.Aggregate(mensagem, (current, cte) => current + _cteService.ObterMsgCtesAgrupados(cte.CteId, ref chavesMsgGeradas, true));
            }

            if (!string.IsNullOrEmpty(mensagem))
            {
                mensagem = "Existem CT-es de rateio associados a outros ctes do mesmo carregamento na seleção para anulação.</br>" +
                           "Ao confirmar a anulação, os outros CT-es agrupados no mesmo carregamento também serão anulados. Ct-es a serem anulados:</br>" + mensagem + "</br>" +
                           "Confirma a operação?";

                possuiMsg = true;
            }

            return Json(new { PossuiMsg = possuiMsg, Message = mensagem });
        }

        /// <summary>
        /// Obtém os dados da NFe
        /// </summary>
        /// <param name="chaveNfe"> Chave da Nfe</param>
        /// <param name="protocolo"> protocolo de autenticação da nfe na sefaz</param>
        /// <param name="ctes"> Lista de Id de Ct-e</param>
        /// <returns> Resultado JSON </returns>
        [Autorizar(Transacao = "ANULACAOCTE", Acao = "Salvar")]
        public JsonResult SalvarAnulacaoCte(string chaveNfe, string protocolo, List<int> ctes)
        {
            bool erro = false;
            string mensagem = string.Empty;
            string stackTrace = string.Empty;

            if ((chaveNfe.Trim() != "") && (!_AnulacaoService.ValidaAnulacaoCte(chaveNfe, ctes)))
            {
                erro = true;
                mensagem = string.Format("O tomador deve ser o mesmo que o Emitente da nota.");
                stackTrace = string.Empty;
            }
            else
                if ((chaveNfe.Trim() != "") && (_AnulacaoService.ValidaNfeJaUtilizada(chaveNfe)))
                {
                    erro = true;
                    mensagem = string.Format("A Nota({0}) já foi utlizada para anulação. Favor informar outra nota.", chaveNfe);
                    stackTrace = string.Empty;
                }
                else
                {
                    _AnulacaoService.ProcessarAnulacaoCte(ctes, chaveNfe, protocolo, UsuarioAtual);
                }

            return Json(new
            {
                Erro = erro,
                Mensagem = mensagem,
                StackTrace = stackTrace
            });
        }

        /// <summary>
        ///   Colocas as anulações para enviar para SEFAZ.
        /// </summary>
        /// <param name="listaCte"> Lista dos Ctes</param>
        /// <returns> Resultado JSON </returns>       
        [Autorizar(Transacao = "ANULACAOCTE", Acao = "Salvar")]
        [JsonFilter(Param = "listaCte", JsonDataType = typeof(List<CteDto>))]
        public JsonResult SalvarAnulacaoCtesEnvioSefazNC(List<CteDto> ctes)
        {
            return SalvarAnulacaoCtesSefaz(ctes);
        }

        [Autorizar(Transacao = "ANULACAOCTE", Acao = "AnularSefaz")]
        [JsonFilter(Param = "listaCte", JsonDataType = typeof(List<CteDto>))]
        public JsonResult SalvarAnulacaoCtesEnvioSefaz(List<CteDto> ctes)
        {
            return SalvarAnulacaoCtesSefaz(ctes);
        }

        private JsonResult SalvarAnulacaoCtesSefaz(List<CteDto> ctes)
        {
            bool erro = false;
            string mensagem = string.Empty;
            string stackTrace = string.Empty;

            if (!_AnulacaoService.ValidaAnulacaoCteSefaz(ctes))
            {
                erro = true;
                mensagem = "Existem CTe-s já anulados nesta lista, verifique.";
                stackTrace = string.Empty;
            }
            else
            {
                try
                {
                    _AnulacaoService.ProcessarAnulacaoCteEnvioSefaz(ctes, UsuarioAtual);
                }
                catch (Exception ex)
                {
                    erro = true;
                    mensagem = ex.Message;
                }
            }

            return Json(new
                {
                    Erro = erro,
                    Mensagem = mensagem,
                    StackTrace = stackTrace
                });
        }

        private string TratarException(Exception e)
        {
            string mensagem = string.Empty;
            Regex regex = new Regex("#.*#");

            mensagem = regex.Match(e.Message).Success ? regex.Match(e.Message).Value : string.Empty;

            if (string.IsNullOrEmpty(mensagem))
            {
                return e.Message;
            }

            return mensagem.Substring(1, mensagem.Length - 2);
        }

        private string ObterValorConfGeral(string chave)
        {
            ConfiguracaoTranslogic configuracaoTranslogic = _configuracaoTranslogicRepository.ObterPorId(chave);

            try
            {
                return configuracaoTranslogic.Valor;
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

    }
}
