namespace Translogic.Modules.Core.Controllers.Cte
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Web.Mvc;
    using ALL.Core.Dominio;
    using Domain.Model.Diversos;
    using Domain.Model.Diversos.Cte;
    using Domain.Model.Dto;
    using Domain.Model.Estrutura;
    using Domain.Model.FluxosComerciais;
    using Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
    using Domain.Model.FluxosComerciais.Nfes;
    using Domain.Model.FluxosComerciais.Pedidos.Despachos;
    using Domain.Model.Trem.Veiculo.Conteiner;
    using Domain.Model.Trem.Veiculo.Vagao;
    using Domain.Services.Ctes;
    using Domain.Services.FluxosComerciais;
    using Infrastructure;
    using Translogic.Core.Infrastructure;
    using Translogic.Core.Infrastructure.Web;
    using Translogic.Core.Infrastructure.Web.Filters;
    using Translogic.Modules.Core.Domain.Model.Codificador.Repositories;
    using Util;

    /// <summary>
    /// Controller Manutencao Cte
    /// </summary>
    public class ManutencaoCteController : BaseSecureModuleController
    {
        private readonly CteService _cteService;
        private readonly CarregamentoService _carregamentoService;
        private readonly IConfiguracaoTranslogicRepository _configuracaoTranslogicRepository;
        private readonly NfeService _nfeService;
        private ResultadoPaginado<ManutencaoCteDto> _result;

        /// <summary>
        /// Contrutor Defaut
        /// </summary>
        /// <param name="cteService">Service Cte</param>
        /// <param name="carregamentoService">Service Carregamento</param>
        /// <param name="configuracaoTranslogicRepository">Service de configuracao</param>
        /// <param name="nfeService"> Servi�o de Nfe </param>
        public ManutencaoCteController(CteService cteService,
                                       CarregamentoService carregamentoService,
                                       IConfiguracaoTranslogicRepository configuracaoTranslogicRepository,
                                       NfeService nfeService)
        {
            _cteService = cteService;
            _carregamentoService = carregamentoService;
            _configuracaoTranslogicRepository = configuracaoTranslogicRepository;
            _nfeService = nfeService;
            _result = new ResultadoPaginado<ManutencaoCteDto>();
        }

        /// <summary>
        /// Acao Padrao ao ser chamado o controller
        /// </summary>
        /// <returns>View Padrao</returns>
        [Autorizar(Transacao = "CTEMANUTENCAO")]
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Acao de mostrar o Formulario
        /// </summary>
        /// <param name="cteId">Id da composicao</param>
        /// <returns>View de insercao composicao</returns>
        [Autorizar(Transacao = "CTEMANUTENCAO", Acao = "Pesquisar")]
        public ActionResult Manutencao(int? cteId)
        {
            int id = cteId.HasValue ? cteId.Value : 0;
            ManutencaoCteDto manutencaoCteDto = _cteService.RetornaCteManutencao(id);
            ViewData["ID_CTE_SELECIONADO"] = manutencaoCteDto.CteId;
            ViewData["VAGAO_COD"] = manutencaoCteDto.CodVagao;
            ViewData["VAGAO_ID"] = manutencaoCteDto.VagaoId;
            ViewData["VOLUME"] = manutencaoCteDto.Volume;
            ViewData["TONELADA_UTIL"] = manutencaoCteDto.ToneladaUtil;

            FluxoComercial fluxo = _carregamentoService.VerificarValidadeFluxoManutencao(manutencaoCteDto.Fluxo.Substring(2));
            bool indFluxoCte = _carregamentoService.VerificarFluxoCte(fluxo);
            bool indValidaRemetenteFiscal = _carregamentoService.VerificarCnpjRemetenteFiscal();
            bool indValidaDestinatarioFiscal = _carregamentoService.VerificarCnpjDestinatarioFiscal();

            if (fluxo.Contrato.EmpresaRemetenteFiscal != null)
            {
                EmpresaCliente empresaRemetente = fluxo.Contrato.EmpresaRemetenteFiscal;
                ViewData["CNPJ_REMETENTE_FISCAL"] = empresaRemetente.TipoPessoa.Value.Equals(TipoPessoaEnum.Juridica) ? empresaRemetente.Cgc : empresaRemetente.Cpf;
                ViewData["UF_REMETENTE_FISCAL"] = (empresaRemetente.Estado == null) ? string.Empty : empresaRemetente.Estado.Sigla;
            }

            var verificaEventoDesacordo = ObterValorConfGeral("CTE_EVENTO_DESACORDO");

            ViewData["MOSTRAR_ALTERA_TOMADOR"] = "N";
            if (manutencaoCteDto.Situacao == SituacaoCteEnum.Anulado && fluxo.Contrato.IndIeToma == 1 && verificaEventoDesacordo == "S")
            {
                ViewData["MOSTRAR_ALTERA_TOMADOR"] = "S";
            }

            if (fluxo.Contrato.EmpresaDestinatariaFiscal != null)
            {
                EmpresaCliente empresaDestinataria = fluxo.Contrato.EmpresaDestinatariaFiscal;
                ViewData["CNPJ_DESTINATARIO_FISCAL"] = empresaDestinataria.TipoPessoa.Value.Equals(TipoPessoaEnum.Juridica) ? empresaDestinataria.Cgc : empresaDestinataria.Cpf;
            }

            ViewData["MARGEM_PERC_LIB_FAT"] = _carregamentoService.VerificarMargemLiberacaoFaturamento(fluxo.EmpresaRemetente);

            if (indValidaRemetenteFiscal)
            {
                indValidaRemetenteFiscal = _carregamentoService.VerificarEmpresaNacional(fluxo.Contrato.EmpresaRemetente);
            }

            if (indValidaDestinatarioFiscal)
            {
                indValidaDestinatarioFiscal = _carregamentoService.VerificarEmpresaNacional(fluxo.Contrato.EmpresaDestinataria);
            }

            if (_cteService.VerificarLiberacaoTravasCorrentista(fluxo))
            {
                indValidaRemetenteFiscal = false;
                indValidaDestinatarioFiscal = false;
            }

            ViewData["IND_VALIDAR_CFOP_CONTRATO"] = _carregamentoService.VerificarCfopContrato();
            ViewData["IND_VALIDAR_CNPJ_REMETENTE"] = indValidaRemetenteFiscal;
            ViewData["IND_VALIDAR_CNPJ_DESTINATARIO"] = indValidaDestinatarioFiscal;
            ViewData["IND_FLUXO_CTE"] = indFluxoCte;
            ViewData["ID_FLUXO_COMERCIAL"] = fluxo.Id;
            ViewData["IND_PREENCHIMENTO_TARA"] = false; // _carregamentoService.VerificarPreenchimentoTara(fluxo);
            ViewData["IND_TARA_OBRIGATORIO"] = false; // fluxo.IndTransbordo.HasValue ? fluxo.IndTransbordo.Value : false;
            ViewData["IND_NFE_OBRIGATORIO"] = _carregamentoService.VerificarNfeObrigatorio(fluxo);
            ViewData["CFOP_CONTRATO"] = fluxo.Contrato.CfopProduto;
            ViewData["CODIGO_FLUXO"] = fluxo.Codigo.Substring(2);

            bool indFluxoInternacional = _carregamentoService.VerificarFluxoInternacional(fluxo);

            ViewData["IND_FLUXO_ORIGEM_INTERCAMBIO"] = _carregamentoService.VerificarFluxoOrigemIntercambio(fluxo);
            ViewData["IND_FLUXO_INTERNACIONAL"] = indFluxoInternacional;
            bool indPreenchimentoVolume = _carregamentoService.VerificarPreenchimentoVolume(fluxo);
            ViewData["IND_PREENCHIMENTO_VOLUME"] = indPreenchimentoVolume;

            ViewData["IND_FLUXO_INTERNACIONAL_TIF_OBRIGATORIO"] = indFluxoInternacional ? _carregamentoService.VerificarTifObrigatorio(fluxo) : false;

            string codigoOrigem;
            if (fluxo.OrigemIntercambio != null)
            {
                ViewData["ID_ESTACAO_ORIGEM"] = fluxo.OrigemIntercambio.Id;
                ViewData["CODIGO_ESTACAO_ORIGEM"] = fluxo.Origem.Codigo + "/" + fluxo.OrigemIntercambio.Codigo;
                codigoOrigem = fluxo.OrigemIntercambio.Codigo;
            }
            else
            {
                ViewData["ID_ESTACAO_ORIGEM"] = fluxo.Origem.Id;
                ViewData["CODIGO_ESTACAO_ORIGEM"] = fluxo.Origem.Codigo;
                codigoOrigem = fluxo.Origem.Codigo;
            }

            if (fluxo.DestinoIntercambio != null)
            {
                ViewData["ID_ESTACAO_DESTINO"] = fluxo.DestinoIntercambio.Id;
                ViewData["CODIGO_ESTACAO_DESTINO"] = fluxo.Destino.Codigo + "/" + fluxo.DestinoIntercambio.Codigo;
            }
            else
            {
                ViewData["ID_ESTACAO_DESTINO"] = fluxo.Destino.Id;
                ViewData["CODIGO_ESTACAO_DESTINO"] = fluxo.Destino.Codigo;
            }

            ViewData["ID_MERCADORIA"] = fluxo.Mercadoria.Id;
            ViewData["DESCRICAO_MERCADORIA"] = fluxo.Mercadoria.DescricaoResumida.Replace("\"", string.Empty);
            ViewData["IND_VERIFICA_DADOS_FISCAIS"] = true;
            ViewData["IND_FLUXO_CONTEINER"] = _carregamentoService.VerificarFluxoConteiner(fluxo);
            ViewData["IND_FLUXO_PALLETS"] = _carregamentoService.VerificarFluxoPallet(fluxo);
            ViewData["CODIGO_MERCADORIA"] = fluxo.Mercadoria.Codigo;
            if (indPreenchimentoVolume)
            {
                ViewData["IND_VERIFICA_PESO_POR_VOLUME"] = _carregamentoService.VerificarPesoPorVolume(codigoOrigem);
            }
            else
            {
                ViewData["IND_VERIFICA_PESO_POR_VOLUME"] = false;
            }

            int mesesRetroativos = _carregamentoService.ObterMesesRetroativos();
            ViewData["MESES_RETROATIVOS"] = mesesRetroativos;

            mesesRetroativos = mesesRetroativos * -1;
            ViewData["LIMITADOR_DATA_NF"] = DateTime.Now.AddMonths(mesesRetroativos);
            return View();
        }

        /// <summary>
        /// Verifica os dados do fluxo comercial
        /// </summary>
        /// <param name="codigoFluxoComercial"> The codigo fluxo comercial. </param>
        /// <param name="codigoOrigemFluxo"> C�digo da origem do fluxo</param>
        /// <param name="codigoDestinoFluxo"> C�digo do destino do fluxo</param>
        /// <param name="codigoMercadoria"> C�digo da mercadoria do fluxo</param>
        /// <returns> Resultado json </returns>
        [Autorizar(Transacao = "CTEMANUTENCAO", Acao = "Pesquisar")]
        public JsonResult VerificarFluxoComercial(string codigoFluxoComercial, string codigoOrigemFluxo, string codigoDestinoFluxo, string codigoMercadoria)
        {
            try
            {
                FluxoComercial fluxo = _carregamentoService.VerificarValidadeFluxoManutencao(codigoFluxoComercial.Substring(2));

                return Json(new
                {
                    Erro = false,
                    IdFluxo = fluxo.Id
                });
            }
            catch (Exception ex)
            {
                string mensagem = TratarException(ex);

                return Json(new
                {
                    Erro = true,
                    Mensagem = mensagem
                });
            }
        }

        /// <summary>
        /// Verifica se o CTE alguma vez possuiu status AUT
        /// </summary>
        /// <param name="idCte">identificador do cte</param>
        /// <returns>resultado da opera��o</returns>
        [Autorizar(Transacao = "CTEMANUTENCAO", Acao = "Pesquisar")]
        public JsonResult JaFoiAutorizadoAlgumaVez(int idCte)
        {
            var result = _cteService.EARJaFoiAutorizadoAlgumaVez(new Cte { Id = idCte });

            if (result)
            {
                return Json(new
                {
                    Erro = true,
                    Mensagem = "N�o � poss�vel dar manuten��o ou reenviar o CTE, pois o mesmo j� esteve autorizado em algum momento."
                });
            }

            return Json(new
            {
                Erro = false
            });
        }

        /// <summary>
        /// Obt�m o peso pelo volume informado
        /// </summary>
        /// <param name="codigoMercadoria">C�digo da mercadoria</param>
        /// <param name="volume">Volume informado no vag�o</param>
        /// <returns> Resultado JSON </returns>
        [Autorizar(Transacao = "CTEMANUTENCAO", Acao = "Pesquisar")]
        public JsonResult ObterPesoPorVolume(string codigoMercadoria, string volume)
        {
            bool erro = false;
            string mensagem = string.Empty;
            double peso = 0;

            double volume2 = double.Parse(volume, CultureInfo.InvariantCulture);

            try
            {
                if (string.IsNullOrEmpty(codigoMercadoria))
                {
                    throw new TranslogicException("Par�metro de C�digo de Mercadoria Inv�lido.");
                }

                if (volume2 <= 0)
                {
                    throw new TranslogicException("Volume Inv�lido.");
                }

                double pesoPorVolume = _carregamentoService.CalcularPesoPorVolume(codigoMercadoria, volume2);

                if (pesoPorVolume <= 0)
                {
                    erro = true;
                    mensagem = "N�o foi encontrado nenhum conteiner com este c�digo.";
                }
                else
                {
                    peso = pesoPorVolume;
                }
            }
            catch (Exception e)
            {
                erro = true;
                mensagem = e.Message;
            }

            return Json(new
            {
                Erro = erro,
                Mensagem = mensagem,
                Peso = peso
            });
        }

        /// <summary>
        /// Obter dados do conteiner
        /// </summary>
        /// <param name="codigoConteiner">C�digo do conteiner</param>
        /// <returns>Resultado Json</returns>
        [Autorizar(Transacao = "CTEMANUTENCAO", Acao = "Pesquisar")]
        public JsonResult ObterDadosConteiner(string codigoConteiner)
        {
            bool erro = false;
            string mensagem = string.Empty;
            try
            {
                Conteiner conteiner = _carregamentoService.ObterConteinerPorCodigo(codigoConteiner);
                if (conteiner == null)
                {
                    erro = true;
                    mensagem = "N�o foi encontrado nenhum conteiner com este c�digo.";
                }
            }
            catch (Exception e)
            {
                erro = true;
                mensagem = e.Message;
            }

            return Json(new
            {
                Erro = erro,
                Mensagem = mensagem
            });
        }

        /// <summary>
        /// Obter dados do conteiner
        /// </summary>
        /// <param name="codigoVagao">C�digo do vag�o</param>
        /// <returns>Resultado Json</returns>
        [Autorizar(Transacao = "CTEMANUTENCAO", Acao = "Pesquisar")]
        public JsonResult ObterDadosVagao(string codigoVagao)
        {
            bool erro = false;
            string mensagem = string.Empty;
            int? idVagao = null;
            try
            {
                Vagao vagao = _cteService.ObterVagaoPorCodigo(codigoVagao);
                if (vagao == null)
                {
                    erro = true;
                    mensagem = "N�o foi encontrado nenhum Vag�o com este c�digo.";
                }
                else
                {
                    idVagao = vagao.Id;
                }
            }
            catch (Exception e)
            {
                erro = true;
                mensagem = e.Message;
            }

            return Json(new
            {
                Erro = erro,
                Mensagem = mensagem,
                IdVagao = idVagao
            });
        }

        /// <summary>
        /// Obt�m os dados de altera��o de fluxo comercial
        /// </summary>
        /// <param name="codigoFluxoComercial">C�digo do fluxo comercial</param>
        /// <param name="idCte">Id do Cte a ser alterado</param>
        /// <returns>Resultado Json</returns>
        [Autorizar(Transacao = "CTEMANUTENCAO", Acao = "Pesquisar")]
        public JsonResult ObterDadosFluxoComercial(string codigoFluxoComercial, int idCte)
        {
            try
            {
                FluxoComercial fluxo = _carregamentoService.VerificarValidadeFluxoManutencao(codigoFluxoComercial);

                bool indFluxoCte = _carregamentoService.VerificarFluxoCte(fluxo);

                if (!indFluxoCte)
                {
                    throw new Exception("Esse n�o � um fluxo(" + codigoFluxoComercial + ") de CT-e! Favor informar outro fluxo.");
                }

                _carregamentoService.VerificarContratoFluxoPossuiVigenciaPorCte(fluxo, idCte);
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic["IND_VALIDAR_CFOP_CONTRATO"] = _carregamentoService.VerificarCfopContrato();

                if (_cteService.VerificarLiberacaoTravasCorrentista(fluxo))
                {
                    dic["IND_VALIDAR_CNPJ_REMETENTE"] = false;
                    dic["IND_VALIDAR_CNPJ_DESTINATARIO"] = false;
                }
                else
                {
                    dic["IND_VALIDAR_CNPJ_REMETENTE"] = _carregamentoService.VerificarCnpjRemetenteFiscal();
                    dic["IND_VALIDAR_CNPJ_DESTINATARIO"] = _carregamentoService.VerificarCnpjDestinatarioFiscal();
                }

                dic["IND_FLUXO_CTE"] = indFluxoCte;
                dic["ID_FLUXO_COMERCIAL"] = fluxo.Id;
                dic["IND_PREENCHIMENTO_TARA"] = false; // _carregamentoService.VerificarPreenchimentoTara(fluxo);
                dic["IND_TARA_OBRIGATORIO"] = false; // fluxo.IndTransbordo.HasValue ? fluxo.IndTransbordo.Value : false;
                dic["IND_NFE_OBRIGATORIO"] = _carregamentoService.VerificarNfeObrigatorio(fluxo);
                dic["CFOP_CONTRATO"] = fluxo.Contrato.CfopProduto;
                dic["CODIGO_FLUXO"] = fluxo.Codigo.Substring(2);
                dic["CNPJ_REMETENTE_FISCAL"] = fluxo.Contrato.EmpresaRemetenteFiscal.Cgc;
                dic["CNPJ_DESTINATARIO_FISCAL"] = fluxo.Contrato.EmpresaDestinatariaFiscal.Cgc;

                bool indFluxoInternacional = _carregamentoService.VerificarFluxoInternacional(fluxo);

                dic["IND_FLUXO_ORIGEM_INTERCAMBIO"] = _carregamentoService.VerificarFluxoOrigemIntercambio(fluxo);
                dic["IND_FLUXO_INTERNACIONAL"] = indFluxoInternacional;
                bool indPreenchimentoVolume = _carregamentoService.VerificarPreenchimentoVolume(fluxo);
                dic["IND_PREENCHIMENTO_VOLUME"] = indPreenchimentoVolume;

                dic["IND_FLUXO_INTERNACIONAL_TIF_OBRIGATORIO"] = indFluxoInternacional ? _carregamentoService.VerificarTifObrigatorio(fluxo) : false;

                string codigoOrigem;
                if (fluxo.OrigemIntercambio != null)
                {
                    dic["ID_ESTACAO_ORIGEM"] = fluxo.OrigemIntercambio.Id;
                    dic["CODIGO_ESTACAO_ORIGEM"] = fluxo.Origem.Codigo + "/" + fluxo.OrigemIntercambio.Codigo;
                    codigoOrigem = fluxo.OrigemIntercambio.Codigo;
                }
                else
                {
                    dic["ID_ESTACAO_ORIGEM"] = fluxo.Origem.Id;
                    dic["CODIGO_ESTACAO_ORIGEM"] = fluxo.Origem.Codigo;
                    codigoOrigem = fluxo.Origem.Codigo;
                }

                if (fluxo.DestinoIntercambio != null)
                {
                    dic["ID_ESTACAO_DESTINO"] = fluxo.DestinoIntercambio.Id;
                    dic["CODIGO_ESTACAO_DESTINO"] = fluxo.Destino.Codigo + "/" + fluxo.DestinoIntercambio.Codigo;
                }
                else
                {
                    dic["ID_ESTACAO_DESTINO"] = fluxo.Destino.Id;
                    dic["CODIGO_ESTACAO_DESTINO"] = fluxo.Destino.Codigo;
                }

                dic["ID_MERCADORIA"] = fluxo.Mercadoria.Id;
                dic["DESCRICAO_MERCADORIA"] = fluxo.Mercadoria.DescricaoResumida.Replace("\"", string.Empty);
                dic["IND_VERIFICA_DADOS_FISCAIS"] = true;
                dic["IND_FLUXO_CONTEINER"] = _carregamentoService.VerificarFluxoConteiner(fluxo);
                dic["IND_FLUXO_PALLETS"] = _carregamentoService.VerificarFluxoPallet(fluxo);
                dic["CODIGO_MERCADORIA"] = fluxo.Mercadoria.Codigo;
                if (indPreenchimentoVolume)
                {
                    dic["IND_VERIFICA_PESO_POR_VOLUME"] = _carregamentoService.VerificarPesoPorVolume(codigoOrigem);
                }
                else
                {
                    dic["IND_VERIFICA_PESO_POR_VOLUME"] = false;
                }

                return Json(new
                {
                    Erro = false,
                    IndValidarCfopContrato = dic["IND_VALIDAR_CFOP_CONTRATO"],
                    IndValidarRemetente = dic["IND_VALIDAR_CNPJ_REMETENTE"],
                    IndValidarCnpjDestinatario = dic["IND_VALIDAR_CNPJ_DESTINATARIO"],
                    IndFluxoCte = dic["IND_FLUXO_CTE"] = indFluxoCte,
                    IdFluxoComercial = dic["ID_FLUXO_COMERCIAL"],
                    IndPreenchimentoTara = dic["IND_PREENCHIMENTO_TARA"],
                    IndTaraObrigatorio = dic["IND_TARA_OBRIGATORIO"],
                    IndNfeObrigatorio = dic["IND_NFE_OBRIGATORIO"],
                    CfopContrato = dic["CFOP_CONTRATO"],
                    CnpjRemetenteFiscal = dic["CNPJ_REMETENTE_FISCAL"],
                    CnpjDestinatarioFiscal = dic["CNPJ_DESTINATARIO_FISCAL"],
                    IndFluxoOrigemIntercambio = dic["IND_FLUXO_ORIGEM_INTERCAMBIO"],
                    IndFluxoInternacional = dic["IND_FLUXO_INTERNACIONAL"],
                    IndPreenchimentoVolume = dic["IND_PREENCHIMENTO_VOLUME"],
                    IndFluxoInternacionalTifObrigatorio = dic["IND_FLUXO_INTERNACIONAL_TIF_OBRIGATORIO"],
                    IdEstacaoOrigem = dic["ID_ESTACAO_ORIGEM"],
                    CodigoEstacaoOrigem = dic["CODIGO_ESTACAO_ORIGEM"],
                    IdEstacaoDestino = dic["ID_ESTACAO_DESTINO"],
                    CodigoEstacaoDestino = dic["CODIGO_ESTACAO_DESTINO"],
                    IdMercadoria = dic["ID_MERCADORIA"],
                    DescricaoMercadoria = dic["DESCRICAO_MERCADORIA"],
                    IndVerificaDadosFiscais = dic["IND_VERIFICA_DADOS_FISCAIS"],
                    IndFluxoConteiner = dic["IND_FLUXO_CONTEINER"],
                    IndFluxoPallet = dic["IND_FLUXO_PALLETS"],
                    CodigoMercadoria = dic["CODIGO_MERCADORIA"],
                    IndVerificaPesoPorVolume = dic["IND_VERIFICA_PESO_POR_VOLUME"],
                    IndObtidoAutomatico = true
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Erro = true,
                    Mensagem = ex.Message
                });
            }
        }

        /// <summary>
        /// Obt�m os dados da NFe
        /// </summary>
        /// <param name="chaveNfe"> Chave da Nfe</param>
        /// <param name="codigoFluxo"> Codigo do fluxo comercial </param>
        /// <returns> Resultado JSON </returns>
        [Autorizar(Transacao = "CTEMANUTENCAO", Acao = "Pesquisar")]
        public JsonResult ObterDadosNfe(string chaveNfe, string codigoFluxo)
        {
            INotaFiscalEletronica nfe;
            string mensagem = string.Empty;
            string stackTrace = string.Empty;
            object dados = null;
            bool erro = true;
            bool indLiberaDigitacao = false;

            // TimeSpan ts = new TimeSpan(0, 0, 10);
            // Thread.Sleep(ts);
            StatusNfeDto statusNfe = _nfeService.ObterDadosNfe(chaveNfe, TelaProcessamentoNfe.Manutencao, false, false);

            if (!statusNfe.ObtidaComSucesso)
            {
                erro = true;
                mensagem = statusNfe.MensagemErro;
                stackTrace = statusNfe.StackTrace;
                indLiberaDigitacao = statusNfe.IndLiberaDigitacao;
            }
            else
            {
                nfe = statusNfe.NotaFiscalEletronica;
                try
                {
                    IEmpresa empresaEmitente = _carregamentoService.ObterEmpresaPorCnpj(nfe.CnpjEmitente, codigoFluxo, 0);
                    IEmpresa empresaDestinatario = _carregamentoService.ObterEmpresaPorCnpj(nfe.CnpjDestinatario, codigoFluxo, 0);

                    if (empresaEmitente == null)
                    {
                        empresaEmitente = new EmpresaCliente();
                    }

                    if (empresaDestinatario == null)
                    {
                        empresaDestinatario = new EmpresaCliente();
                    }

                    IList<NfeProdutoReadonly> produtos = _nfeService.ObterProdutosNfe(nfe);
                    string cfopProduto = String.Join(";", produtos.Select(c => c.Cfop).Distinct().ToArray());

                    double? pesoTotal = statusNfe.Peso > 0 ? Tools.TruncateValue(statusNfe.Peso, TipoTruncateValueEnum.UnidadeMilhar) : (double?)null;
                    double? volume = statusNfe.Volume > 0 ? statusNfe.Volume : null;

                    double? pesoUtilizado = statusNfe.PesoUtilizado > 0 ? Tools.TruncateValue(statusNfe.PesoUtilizado, TipoTruncateValueEnum.UnidadeMilhar) : (double?)null;
                    double? volumeUtilizado = statusNfe.VolumeUtilizado > 0 ? statusNfe.VolumeUtilizado : null;

                    erro = false;
                    dados = new
                    {
                        nfe.SerieNotaFiscal,
                        nfe.NumeroNotaFiscal,
                        Peso = pesoTotal,
                        Volume = volume.HasValue ? Tools.TruncateValue(volume.Value, TipoTruncateValueEnum.UnidadeMilhar) : volume,
                        PesoUtilizado = pesoUtilizado,
                        VolumeUtilizado = volumeUtilizado.HasValue ? Tools.TruncateValue(volumeUtilizado.Value, TipoTruncateValueEnum.UnidadeMilhar) : volumeUtilizado,
                        nfe.Valor,
                        Remetente = nfe.RazaoSocialEmitente,
                        Destinatario = nfe.RazaoSocialDestinatario,
                        SiglaRemetente = empresaEmitente.Sigla,
                        SiglaDestinatario = empresaDestinatario.Sigla,
                        CnpjRemetente = nfe.CnpjEmitente,
                        nfe.CnpjDestinatario,
                        DataNotaFiscal = nfe.DataEmissao.ToString("dd/MM/yyyy"),
                        nfe.InscricaoEstadualDestinatario,
                        InscricaoEstadualRemetente = nfe.InscricaoEstadualEmitente,
                        nfe.UfDestinatario,
                        UfRemetente = nfe.UfEmitente,
                        Cfop = cfopProduto,
                        TipoNotaFiscal = nfe.TipoNotaFiscal != null ? (int)nfe.TipoNotaFiscal : 99
                    };
                }
                catch (Exception e)
                {
                    mensagem = e.Message;
                    stackTrace = e.StackTrace;
                    erro = true;
                    indLiberaDigitacao = true;
                }
            }

            return Json(new
            {
                Erro = erro,
                Mensagem = mensagem,
                DadosNfe = dados,
                StackTrace = stackTrace,
                IndLiberaDigitacao = indLiberaDigitacao
            });
        }

        /// <summary>
        /// Obtem uma lista de Ctes
        /// </summary>
        /// <returns>Lista de Ctes do agrupamento </returns>
        [Autorizar(Transacao = "CTEMONITORAMENTO", Acao = "Pesquisar")]
        public JsonResult ObterCodigoSerieDesp()
        {
            IList<SerieDespachoUf> result = _carregamentoService.ObterCodigoSerie();

            // Inseri item default em branco no list
            var itemDefault = new SerieDespachoUf();
            itemDefault.CodigoControle = " ";

            result.Insert(0, itemDefault);

            return Json(new
            {
                Items = result.Select(c => new
                {
                    c.Id,
                    c.CodigoControle
                }),
                success = true
            });
        }

        /// <summary>
        /// Obter dados do cnpj
        /// </summary>
        /// <param name="cnpj">cnpj da empresa</param>
        /// <param name="codigoFluxo">Codigo do fluxo comercial</param>
        /// <returns>Resultado Json</returns>
        [Autorizar(Transacao = "CTEMANUTENCAO", Acao = "Pesquisar")]
        public JsonResult ObterDadosCnpj(string cnpj, string codigoFluxo)
        {
            bool erro = false;
            object dados = null;
            string mensagem = string.Empty;
            int? idAreaOperacional = null;
            try
            {
                IEmpresa empresa = _carregamentoService.ObterEmpresaPorCnpj(cnpj, codigoFluxo, idAreaOperacional);
                if (empresa == null)
                {
                    erro = true;
                    mensagem = "N�o foi encontrada nenhuma empresa para este CNPJ.";
                }
                else
                {
                    dados = new
                            {
                                Nome = empresa.RazaoSocial,
                                empresa.InscricaoEstadual,
                                empresa.Sigla,
                                Uf = empresa.Estado.Sigla
                            };
                }
            }
            catch (Exception e)
            {
                erro = true;
                mensagem = e.Message;
            }

            return Json(new
            {
                Erro = erro,
                Mensagem = mensagem,
                Dados = dados
            });
        }

        /// <summary>
        /// Abre tela de notas fiscais
        /// </summary>
        /// <param name="idVagao"> Id do vagao. </param>
        /// <param name="codigoVagao"> C�digo do Vag�o </param>
        /// <returns> Tela de notas fiscais </returns>
        [Autorizar(Transacao = "CTEMANUTENCAO", Acao = "Pesquisar")]
        public ActionResult GridNotaFiscal(int? idVagao, string codigoVagao)
        {
            ViewData["IdVagao"] = idVagao;
            ViewData["CodigoVagao"] = codigoVagao;
            return View();
        }

        /// <summary>
        /// Obtem o Cte Selecionado
        /// </summary>
        /// <param name="id">id do Cte Selecionado</param>
        /// <returns>Resultado Cte Selecionado</returns>
        [Autorizar(Transacao = "CTEMANUTENCAO", Acao = "Pesquisar")]
        public JsonResult ObterCteSelecionado(int? id)
        {
            int cteId = id.HasValue ? id.Value : 0;
            ManutencaoCteDto dto = _cteService.RetornaCteManutencao(cteId);

            IList<ManutencaoCteDto> result = new List<ManutencaoCteDto>();
            result.Add(dto);

            return Json(new
            {
                Items = result.Select(c => new
                {
                    c.CteId,
                    c.IdDespacho,
                    c.SerieDesp5,
                    c.NumDesp5,
                    c.SerieDesp6,
                    c.NumDesp6,
                    c.CodVagao,
                    Fluxo = c.Fluxo.Substring(2),
                    c.ToneladaUtil,
                    c.Volume,
                    c.NroCte,
                    c.SerieCte,
                    DataEmissao = c.DataEmissao.ToString("dd/MM/yyyy")
                    // c.Status
                }),
                success = true
            });
        }

        /// <summary>
        /// Abre tela de edi��o de nota fiscal
        /// </summary>
        /// <returns> Tela de edi��o de nota fiscal </returns>
        [Autorizar(Transacao = "CTEMANUTENCAO", Acao = "Pesquisar")]
        public ActionResult FormNotaFiscal()
        {
            return View();
        }

        /// <summary>
        /// Obtem as notas do Cte
        /// </summary>
        /// <param name="id">id do Cte Selecionado</param>
        /// <returns>Resultado Cte Selecionado</returns>
        [Autorizar(Transacao = "CTEMANUTENCAO", Acao = "Pesquisar")]
        public JsonResult ObterNotaCte(int? id)
        {
            IList<CteDetalhe> result = _cteService.ObterCteDetalhePorIdCte(id.HasValue ? id.Value : 0);
            IList<object> lista = new List<object>();
            foreach (CteDetalhe c in result)
            {
                IEmpresa empresaEmitente = _carregamentoService.ObterEmpresaPorCnpj(c.CgcRemetente, c.Cte.FluxoComercial.Codigo, 0);
                IEmpresa empresaDestinatario = _carregamentoService.ObterEmpresaPorCnpj(c.CgcDestinatario, c.Cte.FluxoComercial.Codigo, 0);
                string nomeRemetente = empresaEmitente == null ? string.Empty : empresaEmitente.RazaoSocial;
                string nomeDestinatario = empresaDestinatario == null ? string.Empty : empresaDestinatario.RazaoSocial;

                var obj = new
                              {
                                  c.ChaveNfe,
                                  Conteiner = c.ConteinerNotaFiscal,
                                  SerieNotaFiscal = c.SerieNota,
                                  NumeroNotaFiscal = c.NumeroNota,
                                  PesoRateio = c.PesoNotaFiscal,
                                  c.ValorNotaFiscal,
                                  c.ValorTotalNotaFiscal,
                                  Remetente = nomeRemetente,
                                  Destinatario = nomeDestinatario,
                                  CnpjRemetente = c.CgcRemetente,
                                  CnpjDestinatario = c.CgcDestinatario,
                                  TIF = c.NumeroTif,
                                  SiglaRemetente = c.CodigoRemetente,
                                  SiglaDestinatario = c.CodigoDestinatario,
                                  DataNotaFiscal = c.DataNotaFiscal,
                                  VolumeNotaFiscal = c.VolumeNotaFiscal,
                                  c.UfRemetente,
                                  c.UfDestinatario,
                                  InscricaoEstadualRemetente = c.InsEstadualRemetente,
                                  InscricaoEstadualDestinatario = c.InsEstadualDestinatario,
                                  MultiploDespacho = false,
                                  IndObtidoAutomatico = !string.IsNullOrEmpty(c.ChaveNfe),
                                  c.PesoTotal
                              };
                lista.Add(obj);
            }
            // IList<NotaCteDto> result = new List<NotaCteDto>();
            // result.Add(dto);

            return Json(new
            {
                Items = lista,
                success = true
            });
        }

        /// <summary>
        /// Obtem uma lista de Ctes
        /// </summary>
        /// <param name="pagination"> Paginacao pesquisa</param>
        /// <param name="filter"> Filtros para pesquisa</param>
        /// <returns>Lista de Ctes</returns>
        [Autorizar(Transacao = "CTEMANUTENCAO", Acao = "Pesquisar")]
        public JsonResult ObterCtesManutencao(DetalhesPaginacaoWeb pagination, DetalhesFiltro<object>[] filter)
        {
            _result = _cteService.RetornaListaCteManutencao(pagination, filter);

            return Json(new
            {
                _result.Total,
                Items = _result.Items.Select(c => new
                {
                    c.CteId,
                    c.IdDespacho,
                    Serie = c.SerieDesp6 == 0 && c.NumDesp6 == 0 ? c.SerieDesp5 != null ? c.SerieDesp5.ToString().PadLeft(3, '0') : string.Empty : string.Concat(c.CodigoControle, "-", c.SerieDesp6.ToString().PadLeft(3, '0')),
                    Despacho = c.SerieDesp6 == 0 && c.NumDesp6 == 0 ? c.NumDesp5.ToString().PadLeft(3, '0') : c.NumDesp6.ToString().PadLeft(3, '0'),
                    SerieDesp5 = c.SerieDesp5 != null ? c.SerieDesp5.ToString().PadLeft(3, '0') : string.Empty,
                    DespachoCancelado = c.DataCancDespacho.HasValue,
                    c.CodVagao,
                    c.Fluxo,
                    c.ToneladaUtil,
                    c.Volume,
                    c.NroCte,
                    c.SerieCte,
                    c.ChaveCte,
                    DataEmissao = c.DataEmissao.ToString("dd/MM/yyyy"),
                    c.CteComAgrupamentoNaoCancelado,
                    SituacaoCte = c.StatusRetornoDescricao,
                    c.MotivoDescricao,
                    TipoServicoCte = FluxoComercial.ObterDescricaoTipoServicoCte(c.TipoServicoCte)
                }),
                success = true
            });
        }

        /// <summary>
        /// Realiza o salvamento dos complementos do cte reemitido
        /// </summary>
        /// <param name="listaCte">lista de id do cte a ser reemitido</param>
        /// <returns>resultado do salvamento</returns>
        [Autorizar(Transacao = "CTEMANUTENCAO", Acao = "Salvar")]
        [JsonFilter(Param = "listaCte", JsonDataType = typeof(List<int>))]
        public JsonResult ReenviarLoteManutencaoCte(List<int> listaCte)
        {
            bool erro = false;
            string mensagem = string.Empty;

            try
            {
                var listaCtes = _cteService.ObterListaCte(listaCte);

                foreach (var cte in listaCtes)
                {
                    if (ValidarFluxoNormal(cte))
                    {
                        throw new Exception("Existem fluxos diferente de normal");
                    }
                }

                foreach (var cte in listaCtes)
                {
                    var dto = new ManutencaoCteTempDto()
                    {
                        CteTemp = new CteTemp()
                        {
                            Id = cte.Vagao.Id.Value,
                            CodigoVagao = cte.Vagao.Codigo,
                            IdCteOrigem = cte.Id,
                            IdFluxoComercial = cte.FluxoComercial.Id,
                            IdDespacho = cte.Despacho.Id,
                            PesoVagao = cte.PesoVagao.Value,
                            VolumeVagao = cte.VolumeVagao
                        },
                        ListaDetalhes = null
                    };

                    _cteService.SalvarManutencaoCte(dto, UsuarioAtual);

                }
                //_cteService.SalvarLoteManutencaoCte(listaCte, UsuarioAtual);
            }
            catch (Exception e)
            {
                erro = true;
                mensagem = e.Message;
            }

            return Json(new
            {
                Erro = erro,
                Mensagem = mensagem
            });
        }

        private static bool ValidarFluxoNormal(Cte cte)
        {
            var valor = false;
            var tipoServicoCte = cte.ObterTipoServicoCte();
            if (tipoServicoCte.HasValue)
            {
                if (tipoServicoCte.Value != 0)
                {
                    valor = true;
                }
            }

            return valor;
        }

        /// <summary>
        /// Realiza o salvamento dos complementos do cte reemitido
        /// </summary>
        /// <param name="idCte">Id do cte a ser reemitido</param>
        /// <returns>resultado do salvamento</returns>
        [Autorizar(Transacao = "CTEMANUTENCAO", Acao = "Salvar")]
        public JsonResult ReenviarManutencaoCte(int idCte, string chaveCte)
        {
            bool erro = false;
            string mensagem = string.Empty;

            try
            {
                //var cteTemp = _result.Items.Where(x => x.CteId == idCte).SingleOrDefault();
                var cte = _cteService.ObterCtePorId(idCte);

                if (string.IsNullOrEmpty(chaveCte) && ValidarFluxoNormal(cte))
                {
                    throw new Exception(string.Format("Fluxo: {0} diferente de normal", cte.FluxoComercial.Codigo));
                }

                var dto = new ManutencaoCteTempDto()
                                               {
                                                   CteTemp = new CteTemp()
                                                                 {
                                                                     Id = cte.Vagao.Id.Value,
                                                                     CodigoVagao = cte.Vagao.Codigo,
                                                                     IdCteOrigem = cte.Id,
                                                                     IdFluxoComercial = cte.FluxoComercial.Id,
                                                                     IdDespacho = cte.Despacho.Id,
                                                                     PesoVagao = cte.PesoVagao.Value,
                                                                     VolumeVagao = cte.VolumeVagao
                                                                 },
                                                   ListaDetalhes = null,
                                                   NovaChaveCte = chaveCte
                                               };

                _cteService.SalvarManutencaoCte(dto, UsuarioAtual);

            }
            catch (Exception e)
            {
                erro = true;
                mensagem = e.Message;
            }

            return Json(new
            {
                Erro = erro,
                Mensagem = mensagem
            });
        }

        /// <summary>
        /// Realiza o salvamento dos complementos dos Ctes Selecionados
        /// </summary>
        /// <param name="manutencaoCte">Dto de cteTemp e CteTempDetalhe</param>
        /// <returns>resultado do salvamento</returns>
        [Autorizar(Transacao = "CTEMANUTENCAO", Acao = "Salvar")]
        [JsonFilter(Param = "manutencaoCte", JsonDataType = typeof(ManutencaoCteTempDto))]
        public JsonResult SalvarManutencaoCte(ManutencaoCteTempDto manutencaoCte)
        {
            bool erro = false;
            string mensagem = string.Empty;

            try
            {
                _cteService.SalvarManutencaoCte(manutencaoCte, UsuarioAtual);
            }
            catch (Exception e)
            {
                erro = true;
                mensagem = e.Message;
            }

            return Json(new
            {
                Erro = erro,
                Mensagem = mensagem
            });
        }

        private string TratarException(Exception e)
        {
            string mensagem = string.Empty;
            Regex regex = new Regex("#.*#");

            mensagem = regex.Match(e.Message).Success ? regex.Match(e.Message).Value : string.Empty;

            if (string.IsNullOrEmpty(mensagem))
            {
                return e.Message;
            }

            return mensagem.Substring(1, mensagem.Length - 2);
        }

        private string ObterValorConfGeral(string chave)
        {
            var configuracaoTranslogic = _configuracaoTranslogicRepository.ObterPorId(chave);

            try
            {
                return configuracaoTranslogic.Valor;
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        public ActionResult VerificarNecessidadeChave(string cteId)
        {
            int valor = 0;
            if (string.IsNullOrEmpty(cteId))
            {
                return Json(new
                {
                    Erro = true,
                    Mensagem = "Cte n�o informado"
                });
            }

            if (!int.TryParse(cteId, out valor))
            {
                return Json(new
                {
                    Erro = true,
                    Mensagem = "Id do Cte inv�lido"
                });
            }

            var manutencaoCteDto = _cteService.RetornaCteManutencao(valor);
            var cte = _cteService.ObterCtePorId(valor);

            if (manutencaoCteDto == null || cte == null)
            {
                return Json(new
                {
                    Erro = true,
                    Mensagem = "Dados do Cte n�o localizado"
                });
            }

            var cteOrigem = _cteService.ObterChaveCteOrigemPorCte(cte);
            var tipoServicoCte = cte.ObterTipoServicoCte();

            return Json(new
            {
                Erro = false,
                PrecisaChave = tipoServicoCte != null &&
                               ((manutencaoCteDto.Situacao == SituacaoCteEnum.Cancelado || manutencaoCteDto.Situacao == SituacaoCteEnum.Anulado)
                               && tipoServicoCte.Value != 0),
                TipoServicoCte = tipoServicoCte != null ? tipoServicoCte.Value : 0,
                ChaveCteOrigem = ((manutencaoCteDto.Situacao == SituacaoCteEnum.Cancelado || manutencaoCteDto.Situacao == SituacaoCteEnum.Anulado) &&
                                    cteOrigem != null &&
                                    !string.IsNullOrEmpty(cteOrigem.ChaveCte)) ? cteOrigem.ChaveCte : string.Empty
            });
        }

        public ActionResult ChaveCte(string cte, string chaveCteOrigem)
        {
            ViewData["CTE"] = cte;
            ViewData["ChaveCteOrigem"] = chaveCteOrigem;

            return View();
        }
    }
}
