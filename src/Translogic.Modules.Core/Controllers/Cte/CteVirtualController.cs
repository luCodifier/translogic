﻿namespace Translogic.Modules.Core.Controllers.Cte
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Web.Mvc;
    using ALL.Core.Dominio;
    using Domain.Model.Diversos.Cte;
    using Domain.Model.Dto;
    using Domain.Model.Estrutura;
    using Domain.Model.FluxosComerciais;
    using Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
    using Domain.Model.FluxosComerciais.Nfes;
    using Domain.Model.FluxosComerciais.Pedidos.Despachos;
    using Domain.Model.Trem.Veiculo.Conteiner;
    using Domain.Model.Trem.Veiculo.Vagao;
    using Domain.Model.Via;
    using Domain.Services.Ctes;
    using Domain.Services.FluxosComerciais;
    using Infrastructure;
    using Translogic.Core.Infrastructure;
    using Translogic.Core.Infrastructure.Web;
    using Translogic.Core.Infrastructure.Web.Filters;
    using Util;

    /// <summary>
    /// Controller Cte Virtual
    /// </summary>
    public class CteVirtualController : BaseSecureModuleController
    {
        private readonly CteService _cteService;
        private readonly CarregamentoService _carregamentoService;
        private readonly NfeService _nfeService;

        /// <summary>
        /// Contrutor Defaut
        /// </summary>
        /// <param name="cteService">Service Cte</param>
        /// <param name="carregamentoService">Service Carregamento</param>
        /// <param name="nfeService"> Serviço de Nfe </param>
        public CteVirtualController(CteService cteService, CarregamentoService carregamentoService, NfeService nfeService)
        {
            _cteService = cteService;
            _carregamentoService = carregamentoService;
            _nfeService = nfeService;
        }

        /// <summary>
        /// Acao Padrao ao ser chamado o controller
        /// </summary>
        /// <returns>View Padrao</returns>
        [Autorizar(Transacao = "CTEVIRTUAL")]
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Acao de mostrar o Formulario
        /// </summary>
        /// <param name="cteId">Id da composicao</param>
        /// <returns>View de insercao composicao</returns>
        [Autorizar(Transacao = "CTEVIRTUAL", Acao = "Pesquisar")]
        public ActionResult Virtual(int? cteId)
        {
            /*if (!idFlx.HasValue)
			{
				return RedirectToAction("Index");
			}*/

            int id = cteId.HasValue ? cteId.Value : 0;
            ManutencaoCteDto manutencaoCteDto = _cteService.RetornaCteManutencao(id);
            ViewData["ID_CTE_SELECIONADO"] = manutencaoCteDto.CteId;
            ViewData["VAGAO_COD"] = manutencaoCteDto.CodVagao;
            ViewData["VAGAO_ID"] = manutencaoCteDto.VagaoId;
            ViewData["VOLUME"] = manutencaoCteDto.Volume;
            ViewData["TONELADA_UTIL"] = manutencaoCteDto.ToneladaUtil;

            FluxoComercial fluxo = _carregamentoService.VerificarValidadeFluxoManutencao(manutencaoCteDto.Fluxo.Substring(2));
            bool indFluxoCte = _carregamentoService.VerificarFluxoCte(fluxo);
            bool indValidaRemetenteFiscal = _carregamentoService.VerificarCnpjRemetenteFiscal();
            bool indValidaDestinatarioFiscal = _carregamentoService.VerificarCnpjDestinatarioFiscal();
            double margemPercLiberacaoFaturamento = _carregamentoService.VerificarMargemLiberacaoFaturamento(fluxo.EmpresaRemetente);

            if (indValidaRemetenteFiscal)
            {
                indValidaRemetenteFiscal = _carregamentoService.VerificarEmpresaNacional(fluxo.Contrato.EmpresaRemetente);
            }

            if (indValidaDestinatarioFiscal)
            {
                indValidaDestinatarioFiscal = _carregamentoService.VerificarEmpresaNacional(fluxo.Contrato.EmpresaDestinataria);
            }

            if (_cteService.VerificarLiberacaoTravasCorrentista(fluxo))
            {
                indValidaRemetenteFiscal = false;
                indValidaDestinatarioFiscal = false;
            }

            ViewData["IND_VALIDAR_CFOP_CONTRATO"] = _carregamentoService.VerificarCfopContrato();
            ViewData["IND_VALIDAR_CNPJ_REMETENTE"] = indValidaRemetenteFiscal;
            ViewData["IND_VALIDAR_CNPJ_DESTINATARIO"] = indValidaDestinatarioFiscal;
            ViewData["IND_FLUXO_CTE"] = indFluxoCte;
            ViewData["ID_FLUXO_COMERCIAL"] = fluxo.Id;
            ViewData["IND_PREENCHIMENTO_TARA"] = false; // _carregamentoService.VerificarPreenchimentoTara(fluxo);
            ViewData["IND_TARA_OBRIGATORIO"] = false; // fluxo.IndTransbordo.HasValue ? fluxo.IndTransbordo.Value : false;
            ViewData["IND_NFE_OBRIGATORIO"] = _carregamentoService.VerificarNfeObrigatorio(fluxo);
            ViewData["CFOP_CONTRATO"] = fluxo.Contrato.CfopProduto;
            ViewData["CODIGO_FLUXO"] = fluxo.Codigo.Substring(2);
            ViewData["CNPJ_REMETENTE_FISCAL"] = fluxo.CnpjRemetenteFiscal;
            ViewData["CNPJ_DESTINATARIO_FISCAL"] = fluxo.CnpjDestinatarioFiscal;
            ViewData["MARGEM_PERC_LIB_FAT"] = margemPercLiberacaoFaturamento;
            bool indFluxoInternacional = _carregamentoService.VerificarFluxoInternacional(fluxo);

            ViewData["IND_FLUXO_ORIGEM_INTERCAMBIO"] = _carregamentoService.VerificarFluxoOrigemIntercambio(fluxo);
            ViewData["IND_FLUXO_INTERNACIONAL"] = indFluxoInternacional;
            bool indPreenchimentoVolume = _carregamentoService.VerificarPreenchimentoVolume(fluxo);
            ViewData["IND_PREENCHIMENTO_VOLUME"] = indPreenchimentoVolume;

            ViewData["IND_FLUXO_INTERNACIONAL_TIF_OBRIGATORIO"] = indFluxoInternacional ? _carregamentoService.VerificarTifObrigatorio(fluxo) : false;

            string codigoOrigem;
            if (fluxo.OrigemIntercambio != null)
            {
                ViewData["ID_ESTACAO_ORIGEM"] = fluxo.OrigemIntercambio.Id;
                ViewData["CODIGO_ESTACAO_ORIGEM"] = fluxo.Origem.Codigo + "/" + fluxo.OrigemIntercambio.Codigo;
                codigoOrigem = fluxo.OrigemIntercambio.Codigo;
            }
            else
            {
                ViewData["ID_ESTACAO_ORIGEM"] = fluxo.Origem.Id;
                ViewData["CODIGO_ESTACAO_ORIGEM"] = fluxo.Origem.Codigo;
                codigoOrigem = fluxo.Origem.Codigo;
            }

            if (fluxo.DestinoIntercambio != null)
            {
                ViewData["ID_ESTACAO_DESTINO"] = fluxo.DestinoIntercambio.Id;
                ViewData["CODIGO_ESTACAO_DESTINO"] = fluxo.Destino.Codigo + "/" + fluxo.DestinoIntercambio.Codigo;
            }
            else
            {
                ViewData["ID_ESTACAO_DESTINO"] = fluxo.Destino.Id;
                ViewData["CODIGO_ESTACAO_DESTINO"] = fluxo.Destino.Codigo;
            }

            ViewData["ID_MERCADORIA"] = fluxo.Mercadoria.Id;
            ViewData["DESCRICAO_MERCADORIA"] = fluxo.Mercadoria.DescricaoResumida.Replace("\"", string.Empty);
            ViewData["IND_VERIFICA_DADOS_FISCAIS"] = true;
            ViewData["IND_FLUXO_CONTEINER"] = _carregamentoService.VerificarFluxoConteiner(fluxo);
            ViewData["IND_FLUXO_PALLETS"] = _carregamentoService.VerificarFluxoPallet(fluxo);
            ViewData["CODIGO_MERCADORIA"] = fluxo.Mercadoria.Codigo;
            if (indPreenchimentoVolume)
            {
                ViewData["IND_VERIFICA_PESO_POR_VOLUME"] = _carregamentoService.VerificarPesoPorVolume(codigoOrigem);
            }
            else
            {
                ViewData["IND_VERIFICA_PESO_POR_VOLUME"] = false;
            }

            int mesesRetroativos = _carregamentoService.ObterMesesRetroativos();
            ViewData["MESES_RETROATIVOS"] = mesesRetroativos;

            mesesRetroativos = mesesRetroativos * -1;
            ViewData["LIMITADOR_DATA_NF"] = DateTime.Now.AddMonths(mesesRetroativos);
            return View();
        }

        /// <summary>
        /// Verifica os dados do fluxo comercial
        /// </summary>
        /// <param name="codigoFluxoComercial"> The codigo fluxo comercial. </param>
        /// <param name="codigoOrigemFluxo"> Código da origem do fluxo</param>
        /// <param name="codigoDestinoFluxo"> Código do destino do fluxo</param>
        /// <param name="codigoMercadoria"> Código da mercadoria do fluxo</param>
        /// <returns> Resultado json </returns>
        [Autorizar(Transacao = "CTEVIRTUAL", Acao = "Pesquisar")]
        public JsonResult VerificarFluxoComercial(string codigoFluxoComercial, string codigoOrigemFluxo, string codigoDestinoFluxo, string codigoMercadoria)
        {
            try
            {
                FluxoComercial fluxo = _carregamentoService.VerificarValidadeFluxoManutencao(codigoFluxoComercial.Substring(2));

                return Json(new
                {
                    Erro = false,
                    IdFluxo = fluxo.Id
                });
            }
            catch (Exception ex)
            {
                string mensagem = TratarException(ex);

                return Json(new
                {
                    Erro = true,
                    Mensagem = mensagem
                });
            }
        }

        /// <summary>
        /// Verifica se o CTE alguma vez possuiu status AUT
        /// </summary>
        /// <param name="idCte">identificador do cte</param>
        /// <returns>resultado da operação</returns>
		[Autorizar(Transacao = "CTEVIRTUAL", Acao = "Pesquisar")]
        public JsonResult JaFoiAutorizadoAlgumaVez(int idCte)
        {
            var result = _cteService.EARJaFoiAutorizadoAlgumaVez(new Cte { Id = idCte });

            if (result)
            {
                return Json(new
                {
                    Erro = true,
                    Mensagem = "Não é possível dar manutenção ou reenviar o CTE, pois o mesmo já esteve autorizado em algum momento."
                });
            }

            return Json(new
            {
                Erro = false
            });
        }

        /// <summary>
        /// Obtém o peso pelo volume informado
        /// </summary>
        /// <param name="codigoMercadoria">Código da mercadoria</param>
        /// <param name="volume">Volume informado no vagão</param>
        /// <returns> Resultado JSON </returns>
        [Autorizar(Transacao = "CTEVIRTUAL", Acao = "Pesquisar")]
        public JsonResult ObterPesoPorVolume(string codigoMercadoria, string volume)
        {
            bool erro = false;
            string mensagem = string.Empty;
            double peso = 0;

            double volume2 = double.Parse(volume, CultureInfo.InvariantCulture);

            try
            {
                if (string.IsNullOrEmpty(codigoMercadoria))
                {
                    throw new TranslogicException("Parâmetro de Código de Mercadoria Inválido.");
                }

                if (volume2 <= 0)
                {
                    throw new TranslogicException("Volume Inválido.");
                }

                double pesoPorVolume = _carregamentoService.CalcularPesoPorVolume(codigoMercadoria, volume2);

                if (pesoPorVolume <= 0)
                {
                    erro = true;
                    mensagem = "Não foi encontrado nenhum conteiner com este código.";
                }
                else
                {
                    peso = pesoPorVolume;
                }
            }
            catch (Exception e)
            {
                erro = true;
                mensagem = e.Message;
            }

            return Json(new
            {
                Erro = erro,
                Mensagem = mensagem,
                Peso = peso
            });
        }

        /// <summary>
        /// Obter dados do conteiner
        /// </summary>
        /// <param name="codigoConteiner">Código do conteiner</param>
        /// <returns>Resultado Json</returns>
        [Autorizar(Transacao = "CTEVIRTUAL", Acao = "Pesquisar")]
        public JsonResult ObterDadosConteiner(string codigoConteiner)
        {
            bool erro = false;
            string mensagem = string.Empty;
            try
            {
                Conteiner conteiner = _carregamentoService.ObterConteinerPorCodigo(codigoConteiner);
                if (conteiner == null)
                {
                    erro = true;
                    mensagem = "Não foi encontrado nenhum conteiner com este código.";
                }
            }
            catch (Exception e)
            {
                erro = true;
                mensagem = e.Message;
            }

            return Json(new
            {
                Erro = erro,
                Mensagem = mensagem
            });
        }

        /// <summary>
        /// Obter dados do conteiner
        /// </summary>
        /// <param name="codigoVagao">Código do vagão</param>
        /// <returns>Resultado Json</returns>
        [Autorizar(Transacao = "CTEVIRTUAL", Acao = "Pesquisar")]
        public JsonResult ObterDadosVagao(string codigoVagao)
        {
            bool erro = false;
            string mensagem = string.Empty;
            int? idVagao = null;
            try
            {
                Vagao vagao = _cteService.ObterVagaoPorCodigo(codigoVagao);
                if (vagao == null)
                {
                    erro = true;
                    mensagem = "Não foi encontrado nenhum Vagão com este código.";
                }
                else
                {
                    idVagao = vagao.Id;
                }
            }
            catch (Exception e)
            {
                erro = true;
                mensagem = e.Message;
            }

            return Json(new
            {
                Erro = erro,
                Mensagem = mensagem,
                IdVagao = idVagao
            });
        }

        /// <summary>
        /// Obtém os dados de alteração de fluxo comercial
        /// </summary>
        /// <param name="codigoFluxoComercial">Código do fluxo comercial</param>
        /// <param name="idCte">Id do Cte a ser alterado</param>
        /// <returns>Resultado Json</returns>
        [Autorizar(Transacao = "CTEVIRTUAL", Acao = "Pesquisar")]
        public JsonResult ObterDadosFluxoComercial(string codigoFluxoComercial, int idCte)
        {
            try
            {
                FluxoComercial fluxo = _carregamentoService.VerificarValidadeFluxoManutencao(codigoFluxoComercial);
                bool indFluxoCte = _carregamentoService.VerificarFluxoCte(fluxo);

                if (!indFluxoCte)
                {
                    throw new Exception("Esse não é um fluxo(" + codigoFluxoComercial + ") de CT-e! Favor informar outro fluxo.");
                }

                _carregamentoService.VerificarContratoFluxoPossuiVigenciaPorCte(fluxo, idCte);
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic["IND_VALIDAR_CFOP_CONTRATO"] = _carregamentoService.VerificarCfopContrato();

                if (_cteService.VerificarLiberacaoTravasCorrentista(fluxo))
                {
                    dic["IND_VALIDAR_CNPJ_REMETENTE"] = false;
                    dic["IND_VALIDAR_CNPJ_DESTINATARIO"] = false;
                }
                else
                {
                    dic["IND_VALIDAR_CNPJ_REMETENTE"] = _carregamentoService.VerificarCnpjRemetenteFiscal();
                    dic["IND_VALIDAR_CNPJ_DESTINATARIO"] = _carregamentoService.VerificarCnpjDestinatarioFiscal();
                }

                dic["IND_FLUXO_CTE"] = indFluxoCte;
                dic["ID_FLUXO_COMERCIAL"] = fluxo.Id;
                dic["IND_PREENCHIMENTO_TARA"] = false; // _carregamentoService.VerificarPreenchimentoTara(fluxo);
                dic["IND_TARA_OBRIGATORIO"] = false; // fluxo.IndTransbordo.HasValue ? fluxo.IndTransbordo.Value : false;
                dic["IND_NFE_OBRIGATORIO"] = _carregamentoService.VerificarNfeObrigatorio(fluxo);
                dic["CFOP_CONTRATO"] = fluxo.Contrato.CfopProduto;
                dic["CODIGO_FLUXO"] = fluxo.Codigo.Substring(2);
                dic["CNPJ_REMETENTE_FISCAL"] = fluxo.CnpjRemetenteFiscal;
                dic["CNPJ_DESTINATARIO_FISCAL"] = fluxo.CnpjDestinatarioFiscal;

                bool indFluxoInternacional = _carregamentoService.VerificarFluxoInternacional(fluxo);

                dic["IND_FLUXO_ORIGEM_INTERCAMBIO"] = _carregamentoService.VerificarFluxoOrigemIntercambio(fluxo);
                dic["IND_FLUXO_INTERNACIONAL"] = indFluxoInternacional;
                bool indPreenchimentoVolume = _carregamentoService.VerificarPreenchimentoVolume(fluxo);
                dic["IND_PREENCHIMENTO_VOLUME"] = indPreenchimentoVolume;

                dic["IND_FLUXO_INTERNACIONAL_TIF_OBRIGATORIO"] = indFluxoInternacional ? _carregamentoService.VerificarTifObrigatorio(fluxo) : false;

                string codigoOrigem;
                if (fluxo.OrigemIntercambio != null)
                {
                    dic["ID_ESTACAO_ORIGEM"] = fluxo.OrigemIntercambio.Id;
                    dic["CODIGO_ESTACAO_ORIGEM"] = fluxo.Origem.Codigo + "/" + fluxo.OrigemIntercambio.Codigo;
                    codigoOrigem = fluxo.OrigemIntercambio.Codigo;
                }
                else
                {
                    dic["ID_ESTACAO_ORIGEM"] = fluxo.Origem.Id;
                    dic["CODIGO_ESTACAO_ORIGEM"] = fluxo.Origem.Codigo;
                    codigoOrigem = fluxo.Origem.Codigo;
                }

                if (fluxo.DestinoIntercambio != null)
                {
                    dic["ID_ESTACAO_DESTINO"] = fluxo.DestinoIntercambio.Id;
                    dic["CODIGO_ESTACAO_DESTINO"] = fluxo.Destino.Codigo + "/" + fluxo.DestinoIntercambio.Codigo;
                }
                else
                {
                    dic["ID_ESTACAO_DESTINO"] = fluxo.Destino.Id;
                    dic["CODIGO_ESTACAO_DESTINO"] = fluxo.Destino.Codigo;
                }

                dic["ID_MERCADORIA"] = fluxo.Mercadoria.Id;
                dic["DESCRICAO_MERCADORIA"] = fluxo.Mercadoria.DescricaoResumida.Replace("\"", string.Empty);
                dic["IND_VERIFICA_DADOS_FISCAIS"] = true;
                dic["IND_FLUXO_CONTEINER"] = _carregamentoService.VerificarFluxoConteiner(fluxo);
                dic["IND_FLUXO_PALLETS"] = _carregamentoService.VerificarFluxoPallet(fluxo);
                dic["CODIGO_MERCADORIA"] = fluxo.Mercadoria.Codigo;
                if (indPreenchimentoVolume)
                {
                    dic["IND_VERIFICA_PESO_POR_VOLUME"] = _carregamentoService.VerificarPesoPorVolume(codigoOrigem);
                }
                else
                {
                    dic["IND_VERIFICA_PESO_POR_VOLUME"] = false;
                }

                return Json(new
                {
                    Erro = false,
                    IndValidarCfopContrato = dic["IND_VALIDAR_CFOP_CONTRATO"],
                    IndValidarRemetente = dic["IND_VALIDAR_CNPJ_REMETENTE"],
                    IndValidarCnpjDestinatario = dic["IND_VALIDAR_CNPJ_DESTINATARIO"],
                    IndFluxoCte = dic["IND_FLUXO_CTE"] = indFluxoCte,
                    IdFluxoComercial = dic["ID_FLUXO_COMERCIAL"],
                    IndPreenchimentoTara = dic["IND_PREENCHIMENTO_TARA"],
                    IndTaraObrigatorio = dic["IND_TARA_OBRIGATORIO"],
                    IndNfeObrigatorio = dic["IND_NFE_OBRIGATORIO"],
                    CfopContrato = dic["CFOP_CONTRATO"],
                    CnpjRemetenteFiscal = dic["CNPJ_REMETENTE_FISCAL"],
                    CnpjDestinatarioFiscal = dic["CNPJ_DESTINATARIO_FISCAL"],
                    IndFluxoOrigemIntercambio = dic["IND_FLUXO_ORIGEM_INTERCAMBIO"],
                    IndFluxoInternacional = dic["IND_FLUXO_INTERNACIONAL"],
                    IndPreenchimentoVolume = dic["IND_PREENCHIMENTO_VOLUME"],
                    IndFluxoInternacionalTifObrigatorio = dic["IND_FLUXO_INTERNACIONAL_TIF_OBRIGATORIO"],
                    IdEstacaoOrigem = dic["ID_ESTACAO_ORIGEM"],
                    CodigoEstacaoOrigem = dic["CODIGO_ESTACAO_ORIGEM"],
                    IdEstacaoDestino = dic["ID_ESTACAO_DESTINO"],
                    CodigoEstacaoDestino = dic["CODIGO_ESTACAO_DESTINO"],
                    IdMercadoria = dic["ID_MERCADORIA"],
                    DescricaoMercadoria = dic["DESCRICAO_MERCADORIA"],
                    IndVerificaDadosFiscais = dic["IND_VERIFICA_DADOS_FISCAIS"],
                    IndFluxoConteiner = dic["IND_FLUXO_CONTEINER"],
                    IndFluxoPallet = dic["IND_FLUXO_PALLETS"],
                    CodigoMercadoria = dic["CODIGO_MERCADORIA"],
                    IndVerificaPesoPorVolume = dic["IND_VERIFICA_PESO_POR_VOLUME"],
                    IndObtidoAutomatico = true
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Erro = true,
                    Mensagem = ex.Message
                });
            }
        }

        /// <summary>
        /// Obtém os dados da NFe
        /// </summary>
        /// <param name="chaveNfe"> Chave da Nfe</param>
        /// <param name="codigoFluxo"> Codigo do fluxo comercial </param>
        /// <returns> Resultado JSON </returns>
        [Autorizar(Transacao = "CTEVIRTUAL", Acao = "Pesquisar")]
        public JsonResult ObterDadosNfe(string chaveNfe, string codigoFluxo)
        {
            INotaFiscalEletronica nfe;
            string mensagem = string.Empty;
            string stackTrace = string.Empty;
            object dados = null;
            bool erro = true;
            bool indLiberaDigitacao = false;

            // TimeSpan ts = new TimeSpan(0, 0, 10);
            // Thread.Sleep(ts);
            StatusNfeDto statusNfe = _nfeService.ObterDadosNfe(chaveNfe, TelaProcessamentoNfe.Manutencao, false, false);

            if (!statusNfe.ObtidaComSucesso)
            {
                erro = true;
                mensagem = statusNfe.MensagemErro;
                stackTrace = statusNfe.StackTrace;
                indLiberaDigitacao = statusNfe.IndLiberaDigitacao;
            }
            else
            {
                nfe = statusNfe.NotaFiscalEletronica;
                try
                {
                    IEmpresa empresaEmitente = _carregamentoService.ObterEmpresaPorCnpj(nfe.CnpjEmitente, codigoFluxo, 0);
                    IEmpresa empresaDestinatario = _carregamentoService.ObterEmpresaPorCnpj(nfe.CnpjDestinatario, codigoFluxo, 0);

                    if (empresaEmitente == null)
                    {
                        empresaEmitente = new EmpresaCliente();
                    }

                    if (empresaDestinatario == null)
                    {
                        empresaDestinatario = new EmpresaCliente();
                    }

                    IList<NfeProdutoReadonly> produtos = _nfeService.ObterProdutosNfe(nfe);
                    string cfopProduto = String.Join(";", produtos.Select(c => c.Cfop).Distinct().ToArray());

                    double? pesoTotal = statusNfe.Peso > 0
                                                 ? Tools.TruncateValue(statusNfe.Peso, TipoTruncateValueEnum.UnidadeMilhar)
                                                 : (double?)null;

                    double? volume = statusNfe.Volume > 0 ? statusNfe.Volume : null;

                    double? pesoUtilizado = statusNfe.PesoUtilizado > 0 ? Tools.TruncateValue(statusNfe.PesoUtilizado, TipoTruncateValueEnum.UnidadeMilhar) : (double?)null;
                    double? volumeUtilizado = statusNfe.VolumeUtilizado > 0 ? statusNfe.VolumeUtilizado : null;

                    if (pesoTotal != null && pesoTotal > 0 && pesoUtilizado != null && pesoUtilizado > 0)
                    {
                        pesoTotal = (pesoTotal - pesoUtilizado) < 0 ? 0 : Math.Round(((double)pesoTotal - (double)pesoUtilizado), 2);
                    }
                    erro = false;
                    dados = new
                    {
                        nfe.SerieNotaFiscal,
                        nfe.NumeroNotaFiscal,
                        Peso = pesoTotal,
                        Volume = volume.HasValue ? Tools.TruncateValue(volume.Value, TipoTruncateValueEnum.UnidadeMilhar) : volume,
                        PesoUtilizado = pesoUtilizado,
                        VolumeUtilizado = volumeUtilizado.HasValue ? Tools.TruncateValue(volumeUtilizado.Value, TipoTruncateValueEnum.UnidadeMilhar) : volumeUtilizado,
                        nfe.Valor,
                        Remetente = nfe.RazaoSocialEmitente,
                        Destinatario = nfe.RazaoSocialDestinatario,
                        SiglaRemetente = empresaEmitente.Sigla,
                        SiglaDestinatario = empresaDestinatario.Sigla,
                        CnpjRemetente = nfe.CnpjEmitente,
                        nfe.CnpjDestinatario,
                        DataNotaFiscal = nfe.DataEmissao.ToString("dd/MM/yyyy"),
                        nfe.InscricaoEstadualDestinatario,
                        InscricaoEstadualRemetente = nfe.InscricaoEstadualEmitente,
                        nfe.UfDestinatario,
                        UfRemetente = nfe.UfEmitente,
                        Cfop = cfopProduto
                    };
                }
                catch (Exception e)
                {
                    mensagem = e.Message;
                    stackTrace = e.StackTrace;
                    erro = true;
                    indLiberaDigitacao = true;
                }
            }

            return Json(new
            {
                Erro = erro,
                Mensagem = mensagem,
                DadosNfe = dados,
                StackTrace = stackTrace,
                IndLiberaDigitacao = indLiberaDigitacao
            });
        }

        /// <summary>
        /// Obtem uma lista de Ctes
        /// </summary>
        /// <returns>Lista de Ctes do agrupamento </returns>
        [Autorizar(Transacao = "CTEVIRTUAL", Acao = "Pesquisar")]
        public JsonResult ObterCodigoSerieDesp()
        {
            IList<SerieDespachoUf> result = _carregamentoService.ObterCodigoSerie();

            // Inseri item default em branco no list
            var itemDefault = new SerieDespachoUf();
            itemDefault.CodigoControle = " ";

            result.Insert(0, itemDefault);

            return Json(new
            {
                Items = result.Select(c => new
                {
                    c.Id,
                    c.CodigoControle
                }),
                success = true
            });
        }

        /// <summary>
        /// Obter dados do cnpj
        /// </summary>
        /// <param name="cnpj">cnpj da empresa</param>
        /// <param name="codigoFluxo">Codigo do fluxo comercial</param>
        /// <returns>Resultado Json</returns>
        [Autorizar(Transacao = "CTEVIRTUAL", Acao = "Pesquisar")]
        public JsonResult ObterDadosCnpj(string cnpj, string codigoFluxo)
        {
            bool erro = false;
            object dados = null;
            string mensagem = string.Empty;
            int? idAreaOperacional = null;
            try
            {
                IEmpresa empresa = _carregamentoService.ObterEmpresaPorCnpj(cnpj, codigoFluxo, idAreaOperacional);
                if (empresa == null)
                {
                    erro = true;
                    mensagem = "Não foi encontrada nenhuma empresa para este CNPJ.";
                }
                else
                {
                    dados = new
                    {
                        Nome = empresa.RazaoSocial,
                        empresa.InscricaoEstadual,
                        empresa.Sigla,
                        Uf = empresa.Estado.Sigla
                    };
                }
            }
            catch (Exception e)
            {
                erro = true;
                mensagem = e.Message;
            }

            return Json(new
            {
                Erro = erro,
                Mensagem = mensagem,
                Dados = dados
            });
        }

        /// <summary>
        /// Abre tela de notas fiscais
        /// </summary>
        /// <param name="idVagao"> Id do vagao. </param>
        /// <param name="codigoVagao"> Código do Vagão </param>
        /// <returns> Tela de notas fiscais </returns>
        [Autorizar(Transacao = "CTEVIRTUAL", Acao = "Pesquisar")]
        public ActionResult GridNotaFiscal(int? idVagao, string codigoVagao)
        {
            ViewData["IdVagao"] = idVagao;
            ViewData["CodigoVagao"] = codigoVagao;
            return View();
        }

        /// <summary>
        /// Obtem o Cte Selecionado
        /// </summary>
        /// <param name="id">id do Cte Selecionado</param>
        /// <returns>Resultado Cte Selecionado</returns>
        [Autorizar(Transacao = "CTEVIRTUAL", Acao = "Pesquisar")]
        public JsonResult ObterCteSelecionado(int? id)
        {
            int cteId = id.HasValue ? id.Value : 0;
            ManutencaoCteDto dto = _cteService.RetornaCteManutencao(cteId);

            IList<ManutencaoCteDto> result = new List<ManutencaoCteDto>();
            result.Add(dto);

            return Json(new
            {
                Items = result.Select(c => new
                {
                    c.CteId,
                    c.IdDespacho,
                    c.SerieDesp5,
                    c.NumDesp5,
                    c.SerieDesp6,
                    c.NumDesp6,
                    c.CodVagao,
                    Fluxo = c.Fluxo.Substring(2),
                    c.ToneladaUtil,
                    c.Volume,
                    c.NroCte,
                    c.SerieCte,
                    DataEmissao = c.DataEmissao.ToString("dd/MM/yyyy")
                    // c.Status
                }),
                success = true
            });
        }

        /// <summary>
        /// Abre tela de edição de nota fiscal
        /// </summary>
        /// <returns> Tela de edição de nota fiscal </returns>
        [Autorizar(Transacao = "CTEVIRTUAL", Acao = "Pesquisar")]
        public ActionResult FormNotaFiscal()
        {
            return View();
        }

        /// <summary>
        /// Obtem as notas do Cte
        /// </summary>
        /// <param name="id">id do Cte Selecionado</param>
        /// <returns>Resultado Cte Selecionado</returns>
        [Autorizar(Transacao = "CTEVIRTUAL", Acao = "Pesquisar")]
        public JsonResult ObterNotaCte(int? id)
        {
            IList<CteDetalhe> result = _cteService.ObterCteDetalhePorIdCte(id.HasValue ? id.Value : 0);
            IList<object> lista = new List<object>();
            foreach (CteDetalhe c in result)
            {
                IEmpresa empresaEmitente = _carregamentoService.ObterEmpresaPorCnpj(c.CgcRemetente, c.Cte.FluxoComercial.Codigo, 0);
                IEmpresa empresaDestinatario = _carregamentoService.ObterEmpresaPorCnpj(c.CgcDestinatario, c.Cte.FluxoComercial.Codigo, 0);
                string nomeRemetente = empresaEmitente == null ? string.Empty : empresaEmitente.RazaoSocial;
                string nomeDestinatario = empresaDestinatario == null ? string.Empty : empresaDestinatario.RazaoSocial;

                var obj = new
                {
                    c.ChaveNfe,
                    Conteiner = c.ConteinerNotaFiscal,
                    SerieNotaFiscal = c.SerieNota,
                    NumeroNotaFiscal = c.NumeroNota,
                    PesoRateio = c.PesoNotaFiscal,
                    c.ValorNotaFiscal,
                    c.ValorTotalNotaFiscal,
                    Remetente = nomeRemetente,
                    Destinatario = nomeDestinatario,
                    CnpjRemetente = c.CgcRemetente,
                    CnpjDestinatario = c.CgcDestinatario,
                    TIF = c.NumeroTif,
                    SiglaRemetente = c.CodigoRemetente,
                    SiglaDestinatario = c.CodigoDestinatario,
                    DataNotaFiscal = c.DataNotaFiscal,
                    VolumeNotaFiscal = c.VolumeNotaFiscal,
                    c.UfRemetente,
                    c.UfDestinatario,
                    InscricaoEstadualRemetente = c.InsEstadualRemetente,
                    InscricaoEstadualDestinatario = c.InsEstadualDestinatario,
                    MultiploDespacho = false,
                    IndObtidoAutomatico = !string.IsNullOrEmpty(c.ChaveNfe),
                    c.PesoTotal
                };
                lista.Add(obj);
            }
            // IList<NotaCteDto> result = new List<NotaCteDto>();
            // result.Add(dto);

            return Json(new
            {
                Items = lista,
                success = true
            });
        }

        /// <summary>
        /// Obtem uma lista de Ctes
        /// </summary>
        /// <param name="pagination"> Paginacao pesquisa</param>
        /// <param name="filter"> Filtros para pesquisa</param>
        /// <returns>Lista de Ctes</returns>
        [Autorizar(Transacao = "CTEVIRTUAL", Acao = "Pesquisar")]
        public JsonResult ObterCtesVirtual(DetalhesPaginacaoWeb pagination, DetalhesFiltro<object>[] filter)
        {
            ResultadoPaginado<ManutencaoCteDto> result = _cteService.RetornaListaCteVirtual(pagination, filter);

            return Json(new
            {
                result.Total,
                Items = result.Items.Select(c => new
                {
                    c.CteId,
                    c.IdDespacho,
                    Serie = c.SerieDesp6 == 0 && c.NumDesp6 == 0 ? c.SerieDesp5.ToString().PadLeft(3, '0') : string.Concat(c.CodigoControle, "-", c.SerieDesp6.ToString().PadLeft(3, '0')),
                    Despacho = c.SerieDesp6 == 0 && c.NumDesp6 == 0 ? c.NumDesp5.ToString().PadLeft(3, '0') : c.NumDesp6.ToString().PadLeft(3, '0'),
                    SerieDesp5 = c.SerieDesp5.ToString().PadLeft(3, '0'),

                    c.CodVagao,
                    c.Fluxo,
                    c.ToneladaUtil,
                    c.Volume,
                    c.NroCte,
                    c.SerieCte,
                    c.ChaveCte,
                    DataEmissao = c.DataEmissao.ToString("dd/MM/yyyy"),
                    // c.Status
                    c.CteComAgrupamentoNaoCancelado
                }),
                success = true
            });
        }

        /// <summary>
        /// Realiza o salvamento dos complementos dos Ctes Selecionados
        /// </summary>
        /// <param name="virtualCte">Dto de cteTemp e CteTempDetalhe</param>
        /// <returns>resultado do salvamento</returns>
        [Autorizar(Transacao = "CTEVIRTUAL", Acao = "Salvar")]
        [JsonFilter(Param = "virtualCte", JsonDataType = typeof(ManutencaoCteTempDto))]
        public JsonResult SalvarCteVirtual(ManutencaoCteTempDto virtualCte)
        {
            bool erro = false;
            string mensagem = string.Empty;

            try
            {
                _cteService.SalvarCteVirtual(virtualCte, UsuarioAtual);
            }
            catch (Exception e)
            {
                erro = true;
                mensagem = e.Message;
            }

            return Json(new
            {
                Erro = erro,
                Mensagem = mensagem
            });
        }

        private string TratarException(Exception e)
        {
            string mensagem = string.Empty;
            Regex regex = new Regex("#.*#");

            mensagem = regex.Match(e.Message).Success ? regex.Match(e.Message).Value : string.Empty;

            if (string.IsNullOrEmpty(mensagem))
            {
                return e.Message;
            }

            return mensagem.Substring(1, mensagem.Length - 2);
        }
    }
}
