namespace Translogic.Modules.Core.Controllers.Cte
{
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Text;
	using System.Web.Mvc;
	using ALL.Core.Dominio;
	using Domain.Model.Dto;
	using Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
	using Domain.Model.FluxosComerciais.Pedidos.Despachos;
	using Domain.Services.Ctes;
	using Domain.Services.FluxosComerciais;
	using Infrastructure;
	using Translogic.Core.Infrastructure;
	using Translogic.Core.Infrastructure.Web;
	using Translogic.Core.Infrastructure.Web.Filters;

	/// <summary>
	/// Controller Cancelamento Cte
	/// </summary>
	public class InutilizacaoCteController : BaseSecureModuleController
	{
		private readonly CteService _cteService;
		private readonly CarregamentoService _carregamentoService;

		/// <summary>
		/// Contrutor Defaut
		/// </summary>
		/// <param name="cteService">Service Cte</param>
		/// <param name="carregamentoService">Servico de Carregamento</param>
		public InutilizacaoCteController(CteService cteService, CarregamentoService carregamentoService)
		{
			_cteService = cteService;
			_carregamentoService = carregamentoService;
		}

		/// <summary>
		/// Acao Padrao ao ser chamado o controller
		/// </summary>
		/// <returns>View Padrao</returns>
		[Autorizar(Transacao = "CTEINUTILIZACAO")]
		public ActionResult Index()
		{
			return View();
		}

		/// <summary>
		/// Obtem uma lista de Ctes
		/// </summary>
		/// <returns>Lista de Ctes do agrupamento </returns>
		[Autorizar(Transacao = "CTEINUTILIZACAO", Acao = "Pesquisar")]
		public JsonResult ObterCodigoSerieDesp()
		{
			IList<SerieDespachoUf> result = _carregamentoService.ObterCodigoSerie();

			// Inseri item default em branco no list
			var itemDefault = new SerieDespachoUf();
			itemDefault.CodigoControle = " ";

			result.Insert(0, itemDefault);

			return Json(new
			{
				Items = result.Select(c => new
				{
					c.Id,
					c.CodigoControle
				}),
				success = true
			});
		}

		/// <summary>
		/// Obtem uma lista de Ctes
		/// </summary>
		/// <param name="pagination"> Paginacao pesquisa</param>
		/// <param name="filter"> Filtros para pesquisa</param>
		/// <returns>Lista de Ctes</returns>
        [Autorizar(Transacao = "CTEINUTILIZACAO", Acao = "Pesquisar")]
		public JsonResult ObterCtes(DetalhesPaginacaoWeb pagination, DetalhesFiltro<object>[] filter)
		{
			ResultadoPaginado<CteDto> result = _cteService.RetornaCteParaInutilizacao(pagination, filter);

			return Json(new
			{
				result.Total,
				Items = result.Items.Select(c => new
				{
					c.CteId,
					c.Fluxo,
					c.Origem,
					c.Destino,
					c.Mercadoria,
					c.ClienteFatura,
					CodigoVagao = c.CodigoVagao ?? "COMPLEMENTAR",
					c.Cte,
					Serie = c.SerieDesp6 == 0 && c.NumDesp6 == 0 ? c.SerieDesp5 != null ? c.SerieDesp5.ToString().PadLeft(3, '0') : string.Empty : string.Concat(c.CodigoControle, "-", c.SerieDesp6.ToString().PadLeft(3, '0')),
					Despacho = c.SerieDesp6 == 0 && c.NumDesp6 == 0 ? c.NumDesp5.ToString().PadLeft(3, '0') : c.NumDesp6.ToString().PadLeft(3, '0'),
					DateEmissao = c.DataEmissao.ToString("dd/MM/yyyy"),				
                }),
				success = true
			});
		}

        /// <summary>
        /// Realiza o Reemiss�o dos Ctes escolhidos
        /// </summary>
        /// <param name="ids">ids dos ctes</param>
        /// <returns>resultado do cancelamento</returns>
        [Autorizar(Transacao = "CTEINUTILIZACAO", Acao = "Salvar")]
        [JsonFilter(Param = "ids", JsonDataType = typeof(int[]))]
        public JsonResult Inutilizacao(int[] ids)
        {
            try
            {
                _cteService.SalvarInutilizacaoCtes(ids, UsuarioAtual);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, Message = string.Format("FALHA: {0}", ex.Message) });
            }

            return Json(new { success = true, Message = "SUCESSO" });
        }
	}
}
