namespace Translogic.Modules.Core.Controllers.Cte
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Web.Mvc;
    using ALL.Core.Dominio;
    using Domain.Model.Dto;
    using Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
    using Domain.Model.FluxosComerciais.Pedidos.Despachos;
    using Domain.Services.Ctes;
    using Domain.Services.FluxosComerciais;
    using Infrastructure;
    using Translogic.Core.Infrastructure;
    using Translogic.Core.Infrastructure.Web;
    using Translogic.Core.Infrastructure.Web.Filters;

    /// <summary>
    /// Controller Cancelamento Cte
    /// </summary>
    public class CancelamentoCteController : BaseSecureModuleController
    {
        private readonly CteService _cteService;
        private readonly CarregamentoService _carregamentoService;

        /// <summary>
        /// Contrutor Defaut
        /// </summary>
        /// <param name="cteService">Service Cte</param>
        /// <param name="carregamentoService">Servico de Carregamento</param>
        public CancelamentoCteController(CteService cteService, CarregamentoService carregamentoService)
        {
            _cteService = cteService;
            _carregamentoService = carregamentoService;
        }

        /// <summary>
        /// Acao Padrao ao ser chamado o controller
        /// </summary>
        /// <returns>View Padrao</returns>
        [Autorizar(Transacao = "CTECANCELAMENTO")]
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Retorna a tela para informar os c�digos dos vag�es
        /// </summary>
        /// <returns>Action result com a tela</returns>
        public ActionResult FormInformarVagoes()
        {
            return View();
        }

        /// <summary>
        /// Retorna a tela para informar as chaves nfe
        /// </summary>
        /// <returns>Action result com a tela</returns>
        public ActionResult FormInformarChaves()
        {
            return View();
        }

        /// <summary>
        /// Retorna se as chaves sao validas
        /// </summary>
        /// <param name="listaChave">Lista de chaves</param>
        /// <returns>Action result com a tela</returns>
        public ActionResult ValidarChaves(List<string> listaChave)
        {
            Dictionary<string, KeyValuePair<bool, string>> result = _cteService.VerificarCteFolhaArvorePorListaChave(listaChave);

            return Json(new
            {
                Items = result.Select(c => new
                {
                    Chave = c.Key,
                    Erro = c.Value.Key,
                    Mensagem = c.Value.Value
                }),
                success = true
            });
        }

        /// <summary>
        /// Retorna se os vag�es est�o v�lidos
        /// </summary>
        /// <param name="listaVagoes">Lista de vag�es</param>
        /// <returns>Action result com a tela</returns>
        public ActionResult ValidarVagoes(List<string> listaVagoes)
        {
            Dictionary<string, KeyValuePair<bool, string>> result = _cteService.VerificarListaVagoes(listaVagoes);

            return Json(new
            {
                Items = result.Select(c => new
                {
                    Vagao = c.Key,
                    Erro = c.Value.Key,
                    Mensagem = c.Value.Value
                }),
                success = true
            });
        }

        /// <summary>
        /// Obtem uma lista de Ctes
        /// </summary>
        /// <returns>Lista de Ctes do agrupamento </returns>
        [Autorizar(Transacao = "CTECANCELAMENTO", Acao = "Pesquisar")]
        public JsonResult ObterCodigoSerieDesp()
        {
            IList<SerieDespachoUf> result = _carregamentoService.ObterCodigoSerie();

            // Inseri item default em branco no list
            var itemDefault = new SerieDespachoUf();
            itemDefault.CodigoControle = " ";

            result.Insert(0, itemDefault);

            return Json(new
            {
                Items = result.Select(c => new
                {
                    c.Id,
                    c.CodigoControle
                }),
                success = true
            });
        }

        /// <summary>
        /// Obtem lista de motivos de cancelamento
        /// </summary>
        /// <returns>Lista de Ctes do agrupamento </returns>
        public JsonResult ObterMotivosCancelamento()
        {
            IList<CteMotivoCancelamento> result = _cteService.ObterMotivosCancelamentoAtivos();

            return Json(new
            {
                Items = result.Select(c => new
                {
                    c.Id,
                    Descricao = c.Id + " - " + c.Descricao
                })
            });
        }

        /// <summary>
        /// Obtem uma lista de Ctes
        /// </summary>
        /// <param name="pagination"> Paginacao pesquisa</param>
        /// <param name="filter"> Filtros para pesquisa</param>
        /// <returns>Lista de Ctes</returns>
        [Autorizar(Transacao = "CTECANCELAMENTO", Acao = "Pesquisar")]
        public JsonResult ObterCtes(DetalhesPaginacaoWeb pagination, DetalhesFiltro<object>[] filter)
        {
            ResultadoPaginado<CteDto> result = _cteService.RetornaCteParaCancelamento(pagination, filter);

            return Json(new
            {
                result.Total,
                Items = result.Items.Select(c => new
                {
                    c.CteId,
                    c.Chave,
                    c.Fluxo,
                    c.Origem,
                    c.Destino,
                    c.Mercadoria,
                    c.ClienteFatura,
                    CodigoVagao = c.CodigoVagao ?? "COMPLEMENTAR",
                    c.Cte,
                    Serie = c.SerieDesp6 == 0 && c.NumDesp6 == 0 ? c.SerieDesp5 != null ? c.SerieDesp5.ToString().PadLeft(3, '0') : string.Empty : string.Concat(c.CodigoControle, "-", c.SerieDesp6.ToString().PadLeft(3, '0')),
                    Despacho = c.SerieDesp6 == 0 && c.NumDesp6 == 0 ? c.NumDesp5.ToString().PadLeft(3, '0') : c.NumDesp6.ToString().PadLeft(3, '0'),
                    DateEmissao = c.DataEmissao.ToString("dd/MM/yyyy"),
                    c.CteComAgrupamentoNaoAutorizado,
                    c.CtePago,
                    c.ForaDoTempoCancelamento,
                    DataPedido = c.DataEmissao.ToString("dd/MM/yyyy hh:mm:ss"),
                    c.Acima168hs,
                    c.RateioCte
                }),
                success = true
            });
        }

        [Autorizar(Transacao = "CTECANCELAMENTO", Acao = "Salvar")]
        [JsonFilter(Param = "dto", JsonDataType = typeof(CancelamentoCteDto))]
        public JsonResult ObterCtesRateio(CancelamentoCteDto dto)
        {
            // Verifica nos ctes selecionados se algum deles � de rateio, e se � de rateio, busca quais os ctes rateados no conteiner.
            string mensagem = string.Empty;
            bool possuiMsg = false;
            var chavesMsgGeradas = new List<string>();

            if (dto.Dtos.Any(p => p.RateioCte))
            {
                mensagem = dto.Dtos.Aggregate(mensagem, (current, cte) => current + _cteService.ObterMsgCtesAgrupados(cte.CteId, ref chavesMsgGeradas, true));
            }

            if (!string.IsNullOrEmpty(mensagem))
            {
                mensagem = "Existem CT-es de rateio associados a outros ctes do mesmo carregamento na sele��o para cancelamento.</br>" +
                           "Ao confirmar o cancelamento, os outros CT-es agrupados no mesmo carregamento tamb�m ser�o cancelados. Ct-es a serem cancelados:</br>" + mensagem + "</br>" +
                           "Confirma a opera��o?";
                possuiMsg = true;
            }

            bool possuiMsgValidacao = false;
            var mensagemValidacao = String.Empty;
            if (dto.NumeroVagoesSelecionados.HasValue && dto.NumeroVagoesSelecionados.Value > 0)
            {
                var vagoesRepetidos = new List<string>();
                foreach (var registro in dto.Dtos)
                {
                    if (dto.Dtos.Count(d => d.CodigoVagao == registro.CodigoVagao) > 1)
                    {
                        if (vagoesRepetidos.Any(v => v == registro.CodigoVagao) == false)
                        {
                            vagoesRepetidos.Add(String.Format(registro.CodigoVagao));
                        }
                    }
                }

                if (vagoesRepetidos.Count > 0)
                {
                    var vagoesRepetidosString = String.Empty;
                    foreach(var vagaoRepetido in vagoesRepetidos)
                    {
                        if (String.IsNullOrEmpty(vagoesRepetidosString))
                        {
                            vagoesRepetidosString = vagaoRepetido;
                        }
                        else
                        {
                            vagoesRepetidosString = vagoesRepetidosString + ", " + vagaoRepetido;
                        }
                    }

                    mensagemValidacao = String.Format("Os vag�es: {0} foram selecionados mais de uma vez para cancelamento de CT-e. Deseja continuar?", vagoesRepetidosString);
                    possuiMsgValidacao = true;    
                }
            }

            return Json(new { PossuiMsg = possuiMsg, PossuiMsgValidacao = possuiMsgValidacao, Message = mensagem, MensagemValidacao = mensagemValidacao });
        }

        /// <summary>
        /// Realiza o Cancelamento dos Ctes Selecionados
        /// </summary>
        /// <param name="dto">dtos dos ctes</param>
        /// <returns>resultado do cancelamento</returns>
        [Autorizar(Transacao = "CTECANCELAMENTO", Acao = "Salvar")]
        [JsonFilter(Param = "dto", JsonDataType = typeof(CancelamentoCteDto))]
        public JsonResult Salvar(CancelamentoCteDto dto)
        {
            string mensagemErro = string.Empty;
            bool status = false;

            // Efetua o cancelamento dos ctes que n�o est�o pagos
            KeyValuePair<bool, string> retorno = _cteService.SalvarCancelamentoCtes(dto.Dtos, dto.IdMotivoCancelamento, UsuarioAtual);

            // Verifica se retorno erro
            status = retorno.Key;
            mensagemErro = retorno.Value;

            return Json(new { success = status, Message = status ? "Cancelado com Sucesso!" : mensagemErro });
        }
    }
}
