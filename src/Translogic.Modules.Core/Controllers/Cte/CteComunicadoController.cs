﻿namespace Translogic.Modules.Core.Controllers.Cte
{
	using System;
	using System.Collections.Generic;
	using System.IO;
	using System.Linq;
	using System.Web;
	using System.Web.Mvc;
	using Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
	using Domain.Model.FluxosComerciais.Pedidos.Despachos;
	using Domain.Services.Ctes;
	using Domain.Services.FluxosComerciais;

	/// <summary>
	/// Classe CteComunicadoController
	/// </summary>
	public class CteComunicadoController : BaseSecureModuleController
	{
		private readonly CteService _cteService;

		/// <summary>
		/// Contrutor Defaut
		/// </summary>
		/// <param name="cteService">Service Cte</param>
		public CteComunicadoController(CteService cteService)
		{
			_cteService = cteService;
		}

		/// <summary>
		/// Acao Padrao ao ser chamado o controller
		/// </summary>
		/// <returns>Retorna a view default</returns>
		public ActionResult Index()
		{
			return View();
		}

		/// <summary>
		/// Novo Comunicado
		/// </summary>
		/// <returns>View de Inclusão/ Alteração de comunicado</returns>
		/// <param name="idComunicado"> Id do Comunicado</param>
		public ActionResult FormNovoComunicado(int idComunicado)
		{
			ViewData["idComunicado"] = idComunicado;
			return View();
		}

		/// <summary>
		/// Obtem uma lista de Ctes
		/// </summary>
		/// <returns>Lista de Ctes do agrupamento </returns>
		public JsonResult ObterEstados()
		{
			IList<string> result = _cteService.ObterEstadosSerieDespUf();

			return Json(new
			{
				Items = result.Select(c => new
				{
					Id = c,
					Descricao = c
				}),
				success = true
			});
		}

		/// <summary>
		/// Metodo para Cadastrar novo comunicado
		/// </summary>
		/// <returns>
		/// Retorna Status se foi cadastrado com sucesso
		/// </returns>
		public JsonResult ObterComunicados()
		{
			IList<CteComunicado> result = _cteService.ObterComunicados();

			return Json(new
			{
				Items = result.Select(c => new
				{
					c.Id,
					Estado = c.SiglaUf,
					DataCadastro = c.DataCadastro.Value.ToString("dd/MM/yyyy")
				}),
				success = true
			});
		}

		/// <summary>
		/// Metodo para Excluir novo comunicado
		/// </summary>
		/// <param name="idComunicado">Id do comunicado a ser excluido</param>
		/// <returns>
		/// Retorna Status se foi excluido com sucesso
		/// </returns>
		public JsonResult Excluir(int idComunicado)
		{
			try
			{
				_cteService.ExcluirComunicado(idComunicado);

				JsonResult jsonTmp = Json(new { success = true });
				jsonTmp.ContentType = "text/html";
				return jsonTmp;
			}
			catch (Exception e)
			{
				JsonResult jsonTmp = Json(new { success = false, message = e.Message, e.StackTrace });
				jsonTmp.ContentType = "text/html";
				return jsonTmp;
			}
		}

		/// <summary>
		/// Metodo para Cadastrar novo comunicado
		/// </summary>
		/// <returns>
		/// Retorna Status se foi cadastrado com sucesso
		/// </returns>
		public JsonResult Salvar()
		{
			try
			{
				JsonResult jsonTmp;
				HttpPostedFileBase arquivo = Request.Files["fieldArquivo"];
				string estado = Request["filtro-Estado"];

				if (arquivo != null)
				{
					if (Path.GetExtension(arquivo.FileName).ToUpper() != ".PDF")
					{
						jsonTmp = Json(new { success = false, message = "Arquivo deve ser no formato .pdf" });
						jsonTmp.ContentType = "text/html";
						return jsonTmp;
					}

					_cteService.SalvarComunicado(arquivo.InputStream, estado);
				}

				jsonTmp = Json(new { success = true });
				jsonTmp.ContentType = "text/html";
				return jsonTmp;
			}
			catch (Exception e)
			{
				JsonResult jsonTmp = Json(new { success = false, message = e.Message, e.StackTrace });
				jsonTmp.ContentType = "text/html";
				return jsonTmp;
			}
		}
	}
}
