﻿namespace Translogic.Modules.Core.Controllers.Cte
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using ALL.Core.Dominio;
    using Domain.Model.Dto;
    using Domain.Services.Ctes;
    using Domain.Services.FluxosComerciais;
    using Infrastructure;
    using Translogic.Core.Infrastructure;
    using Translogic.Core.Infrastructure.Web;
    using Translogic.Core.Infrastructure.Web.Filters;
    using Translogic.Modules.Core.Domain.Model.Diversos.Cte;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos;

    /// <summary>
    /// Controller Complemento Peso Cte
    /// </summary>
    public class ComplementoPesoCteController : BaseSecureModuleController
    {
       private readonly CteService _cteService;
		private readonly CarregamentoService _carregamentoService;

		/// <summary>
		/// Contrutor Defaut
		/// </summary>
		/// <param name="cteService">Service Cte</param>
		/// <param name="carregamentoService"> Service Carregamento </param>
        public ComplementoPesoCteController(CteService cteService, CarregamentoService carregamentoService)
		{
			_cteService = cteService;
			_carregamentoService = carregamentoService;
		}

		/// <summary>
		/// Acao Padrao ao ser chamado o controller
		/// </summary>
		/// <returns>View Padrao</returns>
		[Autorizar(Transacao = "CTECOMPLEMENTO")]
		public ActionResult Index()
		{
			return View();
		}

		/// <summary>
		/// Obtem uma lista de Ctes
		/// </summary>
		/// <param name="pagination"> Paginacao pesquisa</param>
		/// <param name="filter"> Filtros para pesquisa</param>
		/// <returns>Lista de Ctes</returns>
		[Autorizar(Transacao = "CTECOMPLEMENTO", Acao = "Pesquisar")]
		public JsonResult ObterCtes(DetalhesPaginacaoWeb pagination, DetalhesFiltro<object>[] filter)
		{
			ResultadoPaginado<CteDto> result = _cteService.RetornaCteParaComplemento(pagination, filter);

			return Json(new
			{
				result.Total,
				Items = result.Items.Select(c => new
				{
					c.CteId,
					c.Fluxo,
					c.Origem,
					c.Destino,
					c.Mercadoria,
					c.ClienteFatura,
					c.Cte,
					c.ValorCte,
					Complementado = c.TipoCte == TipoCteEnum.Complementar,
					c.ValorDiferencaComplemento,
					c.PesoVagao,
                    DataComplemento = c.DataComplemento.HasValue ? c.DataComplemento.Value.ToString("dd/MM/yyyy") : null,
					Serie = c.SerieDesp6 == 0 && c.NumDesp6 == 0 ? c.SerieDesp5.ToString().PadLeft(3, '0') : string.Concat(c.CodigoControle, "-", c.SerieDesp6.ToString().PadLeft(3, '0')),
					Despacho = c.SerieDesp6 == 0 && c.NumDesp6 == 0 ? c.NumDesp5.ToString().PadLeft(3, '0') : c.NumDesp6.ToString().PadLeft(3, '0'),
					DateEmissao = c.DataEmissao.ToString("dd/MM/yyyy"),
					c.CteComAgrupamentoNaoAutorizado,
					c.CtePago,
					c.ForaDoTempoCancelamento,
					c.SerieDesp5,
					c.NumDesp5,
					c.SerieDesp6,
					c.NumDesp6,
                    c.CodigoUsuario,
					c.CodigoControle,
                    ValorTarifa = Math.Round(c.ValorCte / c.PesoVagao, 2),
                    c.PesoDiferencaComplemento // Math.Abs(c.ValorDiferencaComplemento) != Math.Abs(0) ? (c.ValorDiferencaComplemento / Math.Round(c.ValorCte / c.PesoVagao, 2)) : 0
				}),
				success = true
			});
		}

		/// <summary>
		/// Obtem uma lista de Ctes
		/// </summary>
		/// <returns>Lista de Ctes do agrupamento </returns>
        [Autorizar(Transacao = "CTECOMPLEMENTO", Acao = "Pesquisar")]
		public JsonResult ObterCodigoSerieDesp()
		{
			IList<SerieDespachoUf> result = _carregamentoService.ObterCodigoSerie();

			// Inseri item default em branco no list
			var itemDefault = new SerieDespachoUf();
			itemDefault.CodigoControle = " ";

			result.Insert(0, itemDefault);

			return Json(new
			{
				Items = result.Select(c => new
				{
					c.Id,
					c.CodigoControle
				}),
				success = true
			});
		}

		/// <summary>
		/// Realiza o salvamento dos complementos dos Ctes Selecionados
		/// </summary>
		/// <param name="dtos">dtos dos ctes</param>
		/// <returns>resultado do salvamento</returns>
		[Autorizar(Transacao = "CTECOMPLEMENTO", Acao = "Salvar")]
		[JsonFilter(Param = "dtos", JsonDataType = typeof(CteDto[]))]
		public JsonResult Salvar(CteDto[] dtos)
		{
            string mensagem;
            bool status = _cteService.SalvarComplementoPesoCtes(dtos, UsuarioAtual, out mensagem);

            if (status)
            {
                return Json(new { success = true, Message = mensagem });
            }
            else
            {
                return Json(new { success = false, Message = "Não foi possível concluir a ação! </BR>" + mensagem });
            }
		}
	}
}
