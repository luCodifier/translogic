﻿using Translogic.Core.Infrastructure.Web.ContentResults;

namespace Translogic.Modules.Core.Controllers.Cte
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Web.Mvc;
    using ALL.Core.Dominio;
    using Domain.Model.Diversos.Cte;
    using Domain.Model.Dto;
    using Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
    using Domain.Model.FluxosComerciais.Pedidos.Despachos;
    using Domain.Services.Ctes;
    using Domain.Services.FluxosComerciais;
    using Infrastructure;
    using Translogic.Core.Infrastructure.Web;
    using Translogic.Core.Infrastructure.Web.Filters;
    using Newtonsoft.Json;

    /// <summary>
    /// Classe ReenvioArquivoController
    /// </summary>
    public class ReenvioArquivoCteController : BaseSecureModuleController
    {
        private readonly CteService _cteService;
        private readonly CarregamentoService _carregamentoService;

        /// <summary>
        /// Construtor da Classe
        /// </summary>
        /// <param name="cteService">Serviço CteService</param>
        /// <param name="carregamentoService"> Service Carregamento</param>
        public ReenvioArquivoCteController(CteService cteService, CarregamentoService carregamentoService)
        {
            _cteService = cteService;
            _carregamentoService = carregamentoService;
        }

        /// <summary>
        /// Acao Padrao ao ser chamado o controller
        /// </summary>
        /// <returns>Retorna a view default</returns>
        [Autorizar(Transacao = "CTEENVIOARQUIVO")]
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Retorna a lista de e-mail para reenvio
        /// </summary>
        /// <param name="idCte">Id do Cte selecionado</param>
        /// <returns>Retorna lista de email</returns>
        [Autorizar(Transacao = "CTEENVIOARQUIVO", Acao = "Pesquisar")]
        public JsonResult ObterEmailCte(int idCte)
        {
            IList<ReenvioEmailDto> result = _cteService.RetornaEmailReenvioArquivo(idCte);

            return Json(new
            {
                Items = result.Select(c => new
                    {
                        Id = c.IdXmlEnvio,
                        c.IdCte,
                        Email = c.EnvioPara,
                        TpEnvio = Translogic.Core.Commons.Enum<TipoEnvioCteEnum>.GetDescriptionOf(c.TipoEnvioCte),
                        TpArquivo = Translogic.Core.Commons.Enum<TipoArquivoEnvioEnum>.GetDescriptionOf(c.TipoArquivoEnvio.Value),
                        xml = false,
                        pdf = false,
                        c.Erro
                    }),
                success = true
            });
        }

        /// <summary>
        /// Alterar e-mail
        /// </summary>
        /// <param name="email"> Email do filtro</param>
        /// <param name="fluxo"> fluxo do filtro</param>
        /// <returns>View de alterar e-mail</returns>
        [Autorizar(Transacao = "CTEENVIOARQUIVO", Acao = "Alterar")]
        public ActionResult FormAlterarEmail(string email, string fluxo)
        {
            ViewData["Email"] = email;
            ViewData["Fluxo"] = fluxo;
            return View();
        }

        /// <summary>
        /// Alterar e-mail
        /// </summary>
        /// <returns>View de alterar e-mail</returns>
        [Autorizar(Transacao = "CTEENVIOARQUIVO", Acao = "Alterar")]
        public ActionResult FormGerarLoteArquivo()
        {
            return View();
        }

        /// <summary>
        /// Novo e-mail
        /// </summary>
        /// <returns>View de alterar e-mail</returns>
        /// <param name="idCte"> Id do cte a incluir e-mail</param>
        [Autorizar(Transacao = "CTEENVIOARQUIVO", Acao = "Adicionar")]
        public ActionResult FormAdicionarEmail(int idCte)
        {
            ViewData["idCte"] = idCte;
            return View();
        }

        /// <summary>
        /// Obtem uma lista de Ctes
        /// </summary>
        /// <param name="pagination"> Paginacao pesquisa</param>
        /// <param name="filter"> Filtros para pesquisa</param>
        /// <returns>Lista de Ctes</returns>
        [Autorizar(Transacao = "CTEENVIOARQUIVO", Acao = "Pesquisar")]
        public JsonResult ObterCtes(DetalhesPaginacaoWeb pagination, DetalhesFiltro<object>[] filter)
        {
            ResultadoPaginado<CteDto> result = _cteService.RetornaCteReenvioArquivo(pagination, filter);

            return Json(new
            {
                result.Total,
                Items = result.Items.Select(c => new
                {
                    c.CteId,
                    c.Fluxo,
                    c.Origem,
                    c.Destino,
                    c.Mercadoria,
                    c.Chave,
                    c.Cte,
                    DateEmissao = c.DataEmissao.ToString("dd/MM/yyyy"),
                    c.FerroviaOrigem,
                    c.FerroviaDestino,
                    c.CodigoVagao,
                    c.UfOrigem,
                    c.UfDestino,
                    Serie = c.SerieDesp6 == 0 && c.NumDesp6 == 0 ? c.SerieDesp5 != null ? c.SerieDesp5.ToString().PadLeft(3, '0') : "0" : string.Concat(c.CodigoControle, "-", c.SerieDesp6.ToString().PadLeft(3, '0')),
                    Despacho = c.SerieDesp6 == 0 && c.NumDesp6 == 0 ? c.NumDesp5.ToString().PadLeft(3, '0') : c.NumDesp6.ToString().PadLeft(3, '0'),
                    SerieDesp5 = c.SerieDesp5 != null ? c.SerieDesp5.ToString().PadLeft(3, '0') : "0",
                    Desp5 = c.NumDesp5.ToString().PadLeft(3, '0')
                }),
                success = true
            });
        }

        /// <summary>
        /// Obtem uma lista de Ctes
        /// </summary>
        /// <returns>Lista de Ctes do agrupamento </returns>
        [Autorizar(Transacao = "CTEENVIOARQUIVO", Acao = "Pesquisar")]
        public JsonResult ObterCodigoSerieDesp()
        {
            IList<SerieDespachoUf> result = _carregamentoService.ObterCodigoSerie();

            // Inseri item default em branco no list
            var itemDefault = new SerieDespachoUf();
            itemDefault.CodigoControle = " ";

            result.Insert(0, itemDefault);

            return Json(new
            {
                Items = result.Select(c => new
                {
                    c.Id,
                    c.CodigoControle
                }),
                success = true
            });
        }

        /// <summary>
        /// Obtem uma lista de Ctes
        /// </summary>
        /// <param name="email"> e-mail a ser alterado</param>
        /// <param name="novoEmail"> Novo e-mail a ser alterado</param>
        /// <param name="fluxo"> Fluxo informado</param>
        /// <returns>Lista de Ctes</returns>
        [Autorizar(Transacao = "CTEENVIOARQUIVO", Acao = "Salvar")]
        public JsonResult Salvar(string email, string novoEmail, string fluxo)
        {
            bool erro = false;
            string mensagem = string.Empty;

            try
            {
                _cteService.SalvarEmailCteReenvioArquivo(email, novoEmail, fluxo);
            }
            catch (Exception e)
            {
                erro = true;
                mensagem = e.Message;
            }

            return Json(new
            {
                Erro = erro,
                Mensagem = mensagem
            });
        }

        /// <summary>
        /// Obtem uma lista de Ctes
        /// </summary>
        /// <param name="idXmlEnvio"> Id Xml Envio</param>
        /// <returns>Lista de Ctes</returns>
        [Autorizar(Transacao = "CTEENVIOARQUIVO", Acao = "Salvar")]
        public JsonResult ExcluirEmail(int idXmlEnvio)
        {
            bool erro = false;
            string mensagem = string.Empty;

            try
            {
                _cteService.ExcluirEmailCteReenvioArquivo(idXmlEnvio);
            }
            catch (Exception e)
            {
                erro = true;
                mensagem = e.Message;
            }

            return Json(new
            {
                Erro = erro,
                Mensagem = mensagem
            });
        }

        /// <summary>
        /// Cadastra um novo e-mail
        /// </summary>
        /// <param name="idCte">Id do Cte a ser cadastrado</param>
        /// <param name="email">Email a ser cadastrado</param>
        /// <param name="tipoEnvio">Tipo de Envio do email</param>
        /// <param name="tipoArquivo">Tipo do Arquivo</param>
        /// <returns>Retorna o status do processo</returns>
        [Autorizar(Transacao = "CTEENVIOARQUIVO", Acao = "Adicionar")]
        public JsonResult AdicionarEmail(int idCte, string email, string tipoEnvio, string tipoArquivo)
        {
            bool erro = false;
            string mensagem = string.Empty;

            try
            {
                _cteService.AdicionarEmailCteReenvioArquivo(idCte, email, Translogic.Core.Commons.Enum<TipoEnvioCteEnum>.From(tipoEnvio), Translogic.Core.Commons.Enum<TipoArquivoEnvioEnum>.From(tipoArquivo));
            }
            catch (Exception e)
            {
                erro = true;
                mensagem = e.Message;
            }

            return Json(new
            {
                Erro = erro,
                Mensagem = mensagem
            });
        }

        /// <summary>
        /// Efetua o reenvio dos e-mail
        /// </summary>
        /// <param name="idXmlEnvio"> Lista de Cte a serem reenviados</param>
        /// <param name="tipoEnvio"> Tipo de envio</param>
        /// <returns>Status de retorno</returns>
        [Autorizar(Transacao = "CTEENVIOARQUIVO", Acao = "Reenvio")]
        [JsonFilter(Param = "idXmlEnvio", JsonDataType = typeof(int))]
        public JsonResult ReenviarEmail(int idXmlEnvio, string tipoEnvio)
        {
            bool erro = false;
            string mensagem = string.Empty;

            try
            {
                _cteService.ReenviarEmail(idXmlEnvio, Translogic.Core.Commons.Enum<TipoArquivoEnvioEnum>.From(tipoEnvio));
            }
            catch (Exception e)
            {
                erro = true;
                mensagem = e.Message;
            }

            return Json(new
            {
                Erro = erro,
                Mensagem = mensagem
            });
        }

        /// <summary>
        /// Efetua o reenvio dos e-mail
        /// </summary>
        /// <param name="idCtes"> Lista de Cte a serem reenviados</param>
        /// <returns>Status de retorno</returns>
        [Autorizar(Transacao = "CTEENVIOARQUIVO", Acao = "Reenvio")]
        [JsonFilter(Param = "idCtes", JsonDataType = typeof(int[]))]
        public JsonResult ReenviarEmailLote(int[] idCtes)
        {
            bool erro = false;
            string mensagem = string.Empty;

            try
            {
                _cteService.ReenviarEmailporCte(idCtes);
            }
            catch (Exception e)
            {
                erro = true;
                mensagem = e.Message;
            }

            return Json(new
            {
                Erro = erro,
                Mensagem = mensagem
            });
        }

        /// <summary>
        /// Confirmação de e-mail
        /// </summary>
        /// <param name="idCtes"> Lista de Cte a serem reenviados</param>
        /// <returns>Status de retorno</returns>
        //[Autorizar(Transacao = "CTEENVIOARQUIVO", Acao = "Confirm")]
        [JsonFilter(Param = "idCtes", JsonDataType = typeof(int[]))]
        public ActionResult ExportarConfirmacaoEnvioEmailLote(string idCtes)
        {
            string mensagem;

            try
            {
                var ids = ObterListIds(idCtes);
                var result = _cteService.ObterConfirmacaoEnvioEmail(ids.ToArray());

                var dic = new Dictionary<string, Func<HistoricoEnvioEmailDto, string>>();

                dic["Código do vagão"] = c => string.IsNullOrEmpty(c.CodigoVagao) ? string.Empty : c.CodigoVagao;
                dic["Número do CT-e"] = c => string.IsNullOrEmpty(c.NumeroCte) ? string.Empty : c.NumeroCte;
                dic["Chave do CT-e"] = c => string.IsNullOrEmpty(c.ChaveCte) ? string.Empty : c.ChaveCte;
                dic["Data e Hora do CT-e"] = c => c.DataHoraCte.ToString();
                dic["Tipo do arquivo"] = c => string.IsNullOrEmpty(c.TipoArquivo) ? string.Empty : c.TipoArquivo;
                dic["Data e Hora do Envio"] = c => c.DataHoraEnvio.ToString();
                dic["Status"] = c => string.IsNullOrEmpty(c.Status) ? string.Empty : c.Status;
                dic["Enviado para"] = c => string.IsNullOrEmpty(c.EnviadoPara) ? string.Empty : c.EnviadoPara;
                dic["Em cópia"] = c => string.IsNullOrEmpty(c.EnviadoCc) ? string.Empty : c.EnviadoCc;
                dic["Cópia oculta"] = c => string.IsNullOrEmpty(c.EnviadoCo) ? string.Empty : c.EnviadoCo;


                return this.Excel("ConfirmacaoEnvio.xls", dic, result);

            }
            catch (Exception e)
            {
                mensagem = e.Message;
            }

            return Json(new
            {
                Erro = true,
                Mensagem = mensagem
            });
        }

        /// <summary>
        /// Confirmação de e-mail
        /// </summary>
        /// <param name="filter"> Lista de Cte a serem reenviados</param>
        /// <returns>Status de retorno</returns>
        [JsonFilter(Param = "filter", JsonDataType = typeof(object[]))]
        public ActionResult ExportarConfirmacaoEnvioEmailLoteTotal(string filter)
        {
            string mensagem;

            try
            {
                var filtroserializado = JsonConvert.DeserializeObject<List<DetalhesFiltro<object>>>(filter);
                
                var paginacao = new DetalhesPaginacaoWeb();
                ResultadoPaginado<CteDto> result = _cteService.RetornaCteReenvioArquivo(paginacao, filtroserializado.ToArray());////filtroserializado.ToArray());
                var ids = result.Items.Select(s => s.CteId).ToList();
                var licte = _cteService.ObterConfirmacaoEnvioEmail(ids.ToArray());

                var dic = new Dictionary<string, Func<HistoricoEnvioEmailDto, string>>();

                dic["Código do vagão"] = c => string.IsNullOrEmpty(c.CodigoVagao) ? string.Empty : c.CodigoVagao;
                dic["Número do CT-e"] = c => string.IsNullOrEmpty(c.NumeroCte) ? string.Empty : c.NumeroCte;
                dic["Chave do CT-e"] = c => string.IsNullOrEmpty(c.ChaveCte) ? string.Empty : c.ChaveCte;
                dic["Data e Hora do CT-e"] = c => c.DataHoraCte.ToString();
                dic["Tipo do arquivo"] = c => string.IsNullOrEmpty(c.TipoArquivo) ? string.Empty : c.TipoArquivo;
                dic["Data e Hora do Envio"] = c => c.DataHoraEnvio.ToString();
                dic["Status"] = c => string.IsNullOrEmpty(c.Status) ? string.Empty : c.Status;
                dic["Enviado para"] = c => string.IsNullOrEmpty(c.EnviadoPara) ? string.Empty : c.EnviadoPara;
                dic["Em cópia"] = c => string.IsNullOrEmpty(c.EnviadoCc) ? string.Empty : c.EnviadoCc;
                dic["Cópia oculta"] = c => string.IsNullOrEmpty(c.EnviadoCo) ? string.Empty : c.EnviadoCo;


                return this.Excel("ConfirmacaoEnvio.xls", dic, licte);

            }
            catch (Exception e)
            {
                mensagem = e.Message;
            }

            return Json(new
            {
                Erro = true,
                Mensagem = mensagem
            });
        }

        private IEnumerable<int> ObterListIds(string idCtes)
        {
            return idCtes.Split(',').Select(id => Convert.ToInt32(id));
        }

        /// <summary>
        /// Retorna a tela para informar as chaves nfe
        /// </summary>
        /// <returns>Action result com a tela</returns>
        public ActionResult FormInformarChaves()
        {
            return View();
        }

        /// <summary>
        /// Retorna se as chaves sao validas
        /// </summary>
        /// <param name="listaChave">Lista de chaves</param>
        /// <returns>Action result com a tela</returns>
        public ActionResult ValidarChaves(List<string> listaChave)
        {
            Dictionary<string, KeyValuePair<bool, string>> result = _cteService.VerificarCteFolhaArvorePorListaChave(listaChave);

            return Json(new
            {
                Items = result.Select(c => new
                {
                    Chave = c.Key,
                    Erro = c.Value.Key,
                    Mensagem = c.Value.Value
                }),
                success = true
            });
        }
    }
}
