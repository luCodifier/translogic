namespace Translogic.Modules.Core.Controllers
{
	using System;
	using System.Web.Mvc;
	using Castle.Facilities.NHibernateIntegration;
	using Domain.Model.Acesso;
	using NHibernate;
	using Translogic.Core.Infrastructure.Modules;
	using Translogic.Core.Infrastructure.Web;
	using Views.AppChecker;

	/// <summary>
	/// Controller de fazer verifica��o se todos os componentes est�o instalados
	/// </summary>
	public class AppCheckerController : BaseModuleController
	{
		#region ATRIBUTOS READONLY & EST�TICOS

		private readonly ISessionManager _sessionManager;

		#endregion

		#region ATRIBUTOS

		private readonly IModuleInstaller[] _modules;

		#endregion

		#region CONSTRUTORES
		/// <summary>
		/// Inicializa uma nova inst�ncia da classe
		/// </summary>
		/// <param name="modules">Array de Instaladores de m�dulos a ser injetado</param>
		/// <param name="sessionManager">SessionManager a ser a ser injetado</param>
		public AppCheckerController(IModuleInstaller[] modules, ISessionManager sessionManager)
		{
			_modules = modules;
			_sessionManager = sessionManager;
		}

		#endregion

		#region M�TODOS

		/// <summary>
		/// M�todo inicial do controller
		/// </summary>
		/// <returns>
		/// Retorna a view default, passando o model <see cref="EnvironmentInfo"/>
		/// </returns>
		public ActionResult Index()
		{
			string connectionString = string.Empty;
			bool databaseStatus;

			try
			{
				using (ISession session = _sessionManager.OpenSession())
				{
					connectionString = session.Connection.ConnectionString;
					session.CreateSQLQuery("SELECT * FROM DUAL").List();
				}

				databaseStatus = true;
			}
			catch (Exception)
			{
				databaseStatus = false;
			}

			return View(new EnvironmentInfo
			            	{
			            		Modules = _modules,
			            		ConnectionString = connectionString,
			            		DatabaseIsOk = databaseStatus,
								Culturas = Cultura.Todas
			            	});
		}

		#endregion
	}
}