namespace Translogic.Modules.Core.Controllers
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.Mvc.Ajax;
    using Domain.Model.Estrutura;
    using Domain.Model.Via;
    using Domain.Services.Via;
    
    /// <summary>
    /// Classe Controller do Trecho TKB
    /// </summary>
    public class TrechoTkbController : BaseSecureModuleController
    {
        private readonly TrechoTkbService _trechoTkbService;
        
        /// <summary>
        /// M�todo Construtor
        /// </summary>
        /// <param name="trechoTkbService">Servi�o do trecho TKB</param>
        public TrechoTkbController(TrechoTkbService trechoTkbService)
        {
            _trechoTkbService = trechoTkbService;
        }

        /// <summary>
        /// M�todo para Retornar a View Index
        /// </summary>
        /// <returns>Retorna a View Index</returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// M�todo para obter os trechos TKBs
        /// </summary>
        /// <param name="trechoTkb"> Parametro trecho Tkb. </param>
        /// <param name="trechoDiesel"> Parametro trecho Diesel. </param>
        /// <param name="up"> Parametro Up </param>
        /// <param name="corredor"> Parametro corredor. </param>
        /// <param name="idUp"> Id da Up (classe) </param>
        /// <returns> Retorna lista de Trechos </returns>
        public JsonResult PesquisarTrechos(string trechoTkb, string trechoDiesel, string up, string corredor, int? idUp)
        {
            IList<TrechoTkb> resultado = _trechoTkbService.ObterTodosPorParametros(trechoTkb, trechoDiesel, corredor, up, idUp);
            return Json(resultado.Select(
                            r => new
                                     {
                                                    id = r.Id,
                                                    trechoTkb = r.Trecho,
                                                    trechoDiesel = r.TrechoDiesel,
                                                    corredor = r.Corredor,
                                                    sentido = r.Sentido,
                                                    up = r.Up,
                                                    idUp = r.UnidadeProducao != null ? r.UnidadeProducao.Id : null,
                                                    unidadeProducao = r.UnidadeProducao != null ? r.UnidadeProducao.DescricaoResumida : "N�o Informado"
                                     }));
        }

        /// <summary>
        /// M�todo para Retornar a view Adicionar
        /// </summary>
        /// <returns>Retorna a View Adicionar</returns>
        public ActionResult Adicionar()
        {
            return View();
        }

        /// <summary>
        /// M�todo para Adicionar
        /// </summary>
        /// <param name="trechoTkb">Parametro Trecho Tkb</param>
        /// <param name="trechoDiesel">Parametro Trecho Diesel</param>
        /// <param name="up">Parametro Up</param>
        /// <param name="corredor">Parametro Corredor</param>
        /// <param name="sentido">Parametro Sentido</param>
        /// <param name="idUp">Parametro Up(Classe)</param>
        /// <returns>Retorna Status se foi adicionado ou n�o</returns>
        public JsonResult AdicionarRegistro(string trechoTkb, string trechoDiesel, string up, string corredor, string sentido, int? idUp)
        {
            try
            {
                UnidadeProducao unidadeP = null;

                if (idUp != null)
                {
                    unidadeP = _trechoTkbService.ObterUpPorId((int) idUp);
                }

                var trecho = new TrechoTkb
                {
                    Corredor = corredor,
                    Sentido = sentido,
                    Trecho = trechoTkb,
                    TrechoDiesel = trechoDiesel,
                    Up = up,
                    UnidadeProducao = unidadeP
                };

                _trechoTkbService.Adicionar(trecho);
                return Json(new
                {
                        success = true
                });
            }
            catch (Exception)
            {
                return Json(new
                {
                       success = false
                });
            }
        }

        /// <summary>
        /// M�todo para obter Ups
        /// </summary>
        /// <returns>Retorna as Ups</returns>
        public JsonResult ObterUps()
        {
           IList<UnidadeProducao> ups = _trechoTkbService.ObterListaUps();
            return Json(ups.Select(u => new
                                            {
                                                id = u.Id,
                                                descricao = u.DescricaoResumida
                                            })
                                            );
        }

        /// <summary>
        /// M�todo para chamar a View EditarRegistro
        /// </summary>
        /// <param name="id">id do registro</param>
        /// <param name="trechoTkb">Parametro Trecho Tkb</param>
        /// <param name="trechoDiesel">Parametro Trecho Diesel</param>
        /// <param name="up">Parametro Up</param>
        /// <param name="corredor">Parametro Corredor</param>
        /// <param name="sentido">Parametro Sentido</param>
        /// <param name="idUp">Parametro Up(Classe)</param>
        /// <returns>Retorna a view EditarRegistro</returns>
        public ActionResult EditarRegistro(int id, string trechoTkb, string trechoDiesel, string up, string corredor, string sentido, int? idUp)
        {
            ViewData["id"] = id;
            ViewData["trechoTkb"] = trechoTkb;
            ViewData["trechoDiesel"] = trechoDiesel;
            ViewData["up"] = up;
            ViewData["sentido"] = sentido;
            ViewData["corredor"] = corredor;
            ViewData["idUp"] = idUp;
            return View();
        }

        /// <summary>
        /// M�todo para Excluir um Registro por Id
        /// </summary>
        /// <param name="id">id do Registro</param>
        /// <returns>Retorna mensagem Sucesso/Erro</returns>
        public JsonResult ExcluirRegistro(int id)
        {
            try
            {
                _trechoTkbService.ExcluirPorId(id);
                {
                    return Json(
                        new
                        {
                            success = true
                        });
                }
            }
            catch (Exception)
            {
                return Json(
                    new
                        {
                            success = false
                        });
            }
        }

        /// <summary>
        /// M�todo para Alterar um registro
        /// </summary>
        /// <param name="id">id do registro</param>
        /// <param name="trechoTkb">Parametro Trecho Tkb</param>
        /// <param name="trechoDiesel">Parametro Trecho Diesel</param>
        /// <param name="up">Parametro Up</param>
        /// <param name="corredor">Parametro Corredor</param>
        /// <param name="sentido">Parametro Sentido</param>
        /// <param name="idUp">Parametro Up(Classe)</param>
        /// <returns>Retorna mensagem Sucesso/Erro</returns>
        public JsonResult AlterarRegistro(int id, string trechoTkb, string trechoDiesel, string up, string corredor, string sentido, int? idUp)
        {
            try
            {
                UnidadeProducao unidadeP = null;
                if (idUp != null)
                {
                    unidadeP = _trechoTkbService.ObterUpPorId((int) idUp);
                }

                TrechoTkb trecho = _trechoTkbService.ObterPorId(id);
                trecho.Corredor = corredor;
                trecho.Sentido = sentido;
                trecho.Trecho = trechoTkb;
                trecho.TrechoDiesel = trechoDiesel;
                trecho.Up = up;
                trecho.UnidadeProducao = unidadeP;
                _trechoTkbService.Alterar(trecho);
                return Json(new
                                {
                                    success = true
                                });
            }
            catch (Exception)
            {
                return Json(new
                {
                    success = false
                });
            }
        }
    }
}
