﻿namespace Translogic.Modules.Core.Controllers.ExcessaoVagaoPeso
{
    using System;
    using System.Linq;
    using System.Web.Mvc;
    using Domain.Model.Vagoes.Repositories;
    using Infrastructure;
    using Translogic.Core.Infrastructure.Web;
using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao.Repositories;

    public class ExcessaoVagaoPesoController : BaseSecureModuleController
    {
        private readonly IExcessaoVagaoPesoRepository _excessaoVagaoPesoRepository;
        private readonly IVagaoRepository _vagaoRepository;

        public ExcessaoVagaoPesoController(IExcessaoVagaoPesoRepository excessaoVagaoPesoRepository, IVagaoRepository vagaoRepository)
        {
            _excessaoVagaoPesoRepository = excessaoVagaoPesoRepository;
            _vagaoRepository = vagaoRepository;
        }

        [Autorizar(Transacao = "EXCVAGAOPESO")]
        public ActionResult Index()
        {
            return View();
        }

        [Autorizar(Transacao = "EXCVAGAOPESO", Acao = "Pesquisar")]
        public ActionResult Pesquisar(DetalhesPaginacaoWeb pagination, string dtInicial, string dtFim, string codigoVagao, string peso)
        {
            try
            {
                decimal? pesoVagao = null;

                if (!string.IsNullOrEmpty(peso))
                {
                    decimal valor = 0;
                    decimal.TryParse(peso, out valor);
                    pesoVagao = valor;
                }

                var resultados = _excessaoVagaoPesoRepository.Obter(pagination, DateTime.Parse(dtInicial), DateTime.Parse(dtFim), codigoVagao, pesoVagao);

                var retorno = new
                {
                    Total = resultados.Items.Count,
                    Items = resultados.Items.Select(item => new
                    {
                        item.Id,
                        item.CodigoVagao,
                        Peso = string.Format("{0:N2}", item.Peso),
                        Data = string.Format("{0:dd/MM/yyyy}", item.Data)
                    })
                };

                return Json(retorno);
            }
            catch (Exception ex)
            {
                return Json(new { Sucesso = false, ex.Message });
            }
        }

        [Autorizar(Transacao = "EXCVAGAOPESO", Acao = "Editar")]
        public ActionResult Editar(string id)
        {
            try
            {
                if (!string.IsNullOrEmpty(id))
                {
                    var valor = _excessaoVagaoPesoRepository.ObterPorId(int.Parse(id));
                    if (valor == null)
                        throw new ArgumentException("Registro não localizado para Editar");

                    ViewData["id"] = valor.Id;
                    ViewData["codigoVagao"] = valor.CodigoVagao;
                    ViewData["peso"] = valor.Peso;
                }

                return View();
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }

        [Autorizar(Transacao = "EXCVAGAOPESO", Acao = "Deletar")]
        public ActionResult DeletarRegistro(string id)
        {
            try
            {
                var valor = _excessaoVagaoPesoRepository.ObterPorId(int.Parse(id));
                if (valor == null)
                    throw new ArgumentException("Registro não localizado para excluir");

                _excessaoVagaoPesoRepository.Remover(valor);

                return Json(new
                {
                    Success = true,
                    Message = "Registro deletado com sucesso"
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }

        [Autorizar(Transacao = "EXCVAGAOPESO", Acao = "Salvar")]
        public ActionResult Salvar(string id, string codigoVagao, string peso)
        {
            try
            {
                var mensagem = "";
                if (string.IsNullOrEmpty(id))
                {
                    var valor = new Domain.Model.Vagoes.ExcessaoVagaoPeso { CodigoVagao = codigoVagao, Peso = decimal.Parse(peso) };
                    ValidarVagao(codigoVagao);
                    _excessaoVagaoPesoRepository.Inserir(valor);
                    mensagem = "Registro criado com sucesso";
                }
                else
                {
                    var valor = _excessaoVagaoPesoRepository.ObterPorId(int.Parse(id));
                    if (valor == null)
                        throw new ArgumentException("Registro não localizado para editar");

                    valor.Peso = decimal.Parse(peso);

                    _excessaoVagaoPesoRepository.Atualizar(valor);
                    mensagem = "Registro atualizado com sucesso";
                }


                return Json(new
                {
                    Success = true,
                    Message = mensagem
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }

        private void ValidarVagao(string codigoVagao)
        {
            var vagao = _vagaoRepository.ObterPorCodigo(codigoVagao);

            if (vagao == null)
                throw new ArgumentException("Vagão inexistente");

            var excessaoVagaoPeso = _excessaoVagaoPesoRepository.ObterPorCodigoVagao(codigoVagao);

            if(excessaoVagaoPeso != null)
                throw new ArgumentException("Já existe um registro para este vagão cadastrado");


        }
    }
}