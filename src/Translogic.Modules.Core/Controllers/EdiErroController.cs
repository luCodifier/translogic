﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ALL.Core.Extensions;
using Translogic.Core.Infrastructure.Web;
using Translogic.Modules.Core.Infrastructure;
using System.Globalization;
using Translogic.Modules.Core.Domain.Services.Edi;
using Translogic.Modules.Core.Domain.Model.Dto;
using Translogic.Modules.Core.Domain.Model.Edi;

namespace Translogic.Modules.Core.Controllers
{
    public class EdiErroController : Controller
    {
        private readonly EdiErroService _ediErroService;

        public EdiErroController(EdiErroService ediErroService)
        {
            _ediErroService = ediErroService;
        }

        //
        // GET: /EdiErro/

        //public ActionResult Index()
        //{
        //    return View();
        //}

        [Autorizar(Transacao = "PAINELEXPEDICAOARQUIVOS", Acao = "PesquisarEdiErro")]
        public JsonResult ObterEdiErrosPesquisa(DetalhesPaginacaoWeb pagination,
                                                                    string dataInicial,
                                                                    string dataFinal,
                                                                    string fluxo,
                                                                    string vagoes,
                                                                    string responsavelEnvio)
        {

            try
            {

                DateTime dataInicialFiltro = DateTime.Now;
                DateTime dataFinalFiltro = DateTime.Now;

                if (!string.IsNullOrEmpty(dataInicial))
                    dataInicialFiltro = DateTime.ParseExact(dataInicial, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                if (!string.IsNullOrEmpty(dataFinal))
                    dataFinalFiltro = DateTime.ParseExact(dataFinal, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                var result = _ediErroService.ObterEdiErrosPesquisa(pagination, 
                                                                    dataInicialFiltro,
                                                                    dataFinalFiltro, 
                                                                    fluxo, 
                                                                    vagoes,
                                                                    responsavelEnvio);


                return
                    this.Json(
                        new
                        {
                            result.Total,
                            Items =
                                result.Items.Select(
                                    e =>
                                    new
                                    {
                                        IdMensagemRecebimento = e.IdMensagemRecebimento,
                                        IdErroEdiNfe = e.IdErroEdiNfe,
                                        Agrupador = e.Agrupador,
                                        Vagao = e.Vagao,
                                        Fluxo = e.Fluxo,
                                        DataErro = e.DataErro.ToString("dd/MM/yyyy"),
                                        DataEnvio = (e.DataEnvio.HasValue ? e.DataEnvio.Value.ToString("dd/MM/yyyy") : ""),
                                        ResponsavelEnvio = e.ResponsavelEnvio,
                                        ChaveNfe = e.ChaveNfe,
                                        DataRegularizacao = (e.DataRegularizacao.HasValue ? e.DataRegularizacao.Value.ToString("dd/MM/yyyy") : ""),
                                        Status = (e.Regularizado==false ? 0 : (e.Regularizado)&&(e.Reprocessado.ToUpper() == "N") ? 1 : 2 ) 
                                    }),
                            success = true
                        });
            }
            catch (Exception ex)
            {
                return this.Json(new { success = false });
            }
        }

        [Autorizar(Transacao = "PAINELEXPEDICAOARQUIVOS", Acao = "RegularizarEdiErro")]
        public JsonResult RegularizarNotasSelecionadas(List<EdiErroResultPesquisa> listaEdiErros)
        {
            var notasNaoRegularizadasTela1154 = new List<int>();

            try
            {
                if (listaEdiErros == null)
                    throw new Exception("Nenhuma nota foi selecionada!");
                if (listaEdiErros.Count <= 0)
                    throw new Exception("Nenhuma nota foi selecionada!");

                var listaNaoRegularizados = listaEdiErros.Where(i => (i.DataRegularizacao.HasValue==false)).ToList();
                if (listaNaoRegularizados != null)
                {
                    if (listaNaoRegularizados.Count <= 0)
                        throw new Exception("Todas as notas selecionadas já foram baixadas e regularizadas!");    
                }

                var dto = listaEdiErros.Map<List<EdiErroResultPesquisa>, List<EdiErroResultPesquisaDto>>();
                notasNaoRegularizadasTela1154 = _ediErroService.RegularizarNotas(dto);
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = ex.Message });
            }

            string strNotasEdiErroRegularizar = String.Empty;
            if (notasNaoRegularizadasTela1154.Count > 0)
            {
                foreach (var id in notasNaoRegularizadasTela1154)
                {
                    if (String.IsNullOrEmpty(strNotasEdiErroRegularizar))
                        strNotasEdiErroRegularizar = id.ToString();
                    else
                        strNotasEdiErroRegularizar = strNotasEdiErroRegularizar + ";" + id.ToString();
                }
            }

            string sMensagemSucesso = "Notas baixadas com sucesso! Favor reprocessá-las.";
            if (notasNaoRegularizadasTela1154.Count > 0)
                sMensagemSucesso = "Algumas notas não foram encontradas. Por este motivo, redirecionaremos para a tela 1154.";

            return Json(
                new
                    {
                        Success = true,
                        Message = sMensagemSucesso,
                        RedirecionarTela1154 = (notasNaoRegularizadasTela1154.Count > 0),
                        NotasNaoRegularizadas = strNotasEdiErroRegularizar
                    });
        }

        [Autorizar(Transacao = "PAINELEXPEDICAOARQUIVOS", Acao = "ReprocessarEdiErro")]
        public JsonResult ReprocessarNotasSelecionadas(List<EdiErroResultPesquisa> listaNotasReprocessar)
        {
            try
            {
                if (listaNotasReprocessar == null)
                    throw new Exception("Nenhuma nota foi selecionada para reprocessamento!");
                if (listaNotasReprocessar.Count <= 0)
                    throw new Exception("Nenhuma nota foi selecionada para reprocessamento!");

                var dto = listaNotasReprocessar.Map<List<EdiErroResultPesquisa>, List<EdiErroResultPesquisaDto>>();
                _ediErroService.ReprocessarNotas(dto);
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = ex.Message });
            }

            return Json(
                new
                {
                    Success = true,
                    Message = "Notas reprocessadas com sucesso!"
                });
        }

        public JsonResult BaixarNFes()
        {
            throw new NotImplementedException();
        }
    }
}
