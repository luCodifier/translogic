﻿namespace Translogic.Modules.Core.Controllers.MonitoramentoPlanejamentoDescarga
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Web.Mvc;
    using ALL.Core.Dominio;
    using ExcelLibrary.SpreadSheet;
    using ICSharpCode.SharpZipLib.Core;
    using Translogic.Core.Infrastructure.Web;
    using Translogic.Core.Infrastructure.Web.ContentResults;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Modules.Core.Domain.Services.Ctes;
    using Translogic.Modules.Core.Domain.Services.Terminal.Interface;
    using Translogic.Modules.Core.Infrastructure;
    using Translogic.Modules.TPCCO.Helpers;

    public class MonitoramentoPlanejamentoDescargaController : BaseSecureModuleController
    {
        private readonly ITerminalService _terminalService;
        private readonly CteService _cteService;

        public MonitoramentoPlanejamentoDescargaController(ITerminalService terminalService, CteService cteService)
        {
            _terminalService = terminalService;
            _cteService = cteService;
        }

        [Autorizar(Transacao = "PLANEJAMENTODESCARGA")]
        public ActionResult Index()
        {
            return View();
        }

        [Autorizar(Transacao = "PLANEJAMENTODESCARGA", Acao = "Pesquisar")]
        public JsonResult ObterTerminal()
        {
            var result = _terminalService.ObterListaTerminalSigla();

            return Json(new
            {
                Items = result.Select(c => new
                {
                    c.CodigoTerminal,
                    c.SiglaTerminal
                }),
                success = true
            });
        }

        [Autorizar(Transacao = "PLANEJAMENTODESCARGA", Acao = "Pesquisar")]
        public JsonResult ObterPlanejamentoDescarga(DetalhesFiltro<object>[] filter)
        {
            IList<MonitoramentoPlanejamentoDescargaDto> result = _cteService.RetornaListaMonitoramentoPlanejamentoDescarga(filter);
            var list = result.OrderBy(m => m.DataSaidaEstacao);
            return Json(new
            {
                Items = result.Select(c => new
                {
                    c.Vagao,
                    c.Serie,
                    DataSaidaEstacao = c.DataSaidaEstacao.ToString("dd/MM/yyyy hh:mm:ss"),
                    c.StatusCte,
                    c.NumeroCte,
                    c.ChaveCte,
                    c.Pdf,
                    c.Xml,
                    c.Estacao,
                    EmpresaCnpj = ("Data Saída Estação: " + c.DataSaidaEstacao.ToString("dd/MM/yyyy hh:mm:ss") + " / Estação: " + c.Estacao + " / OS: " + c.NumOS + " / Prefixo: " + c.Prefixo + " / Empresa: " + c.Cnpj + " - " + c.RazaoSocial),
                    c.NumOS,
                    c.Cnpj,
                    c.RazaoSocial,
                    c.Prefixo
                }),
                success = true
            });
        }

        public ActionResult modalVagao(string os, string dataInicio, string dataFim, string prefixo, string terminal)
        {
            ViewData["OS"] = os;
            ViewData["DataInicio"] = dataInicio;
            ViewData["DataFim"] = dataFim;
            ViewData["Prefixo"] = prefixo;
            ViewData["Terminal"] = terminal;
            return View();
        }

        public JsonResult ObterVagao(string os, string dataInicial, string dataFinal, string prefixo, string terminal, string vagao)
        {
            if (terminal != string.Empty)
                terminal = terminal.Replace(".", "").Replace("/", "").Replace("-", "");

            var dataIni = DateTime.Now;
            var dataFim = DateTime.Now;
            if (!string.IsNullOrEmpty(dataInicial.ToString()))
                dataIni = Convert.ToDateTime(dataInicial);

            if (!string.IsNullOrEmpty(dataFinal.ToString()))
            {
                string texto = dataFinal;
                dataFim = Convert.ToDateTime(texto.ToString());
                dataFim = dataFim.AddDays(1).AddSeconds(-1);
            }

            if (!string.IsNullOrWhiteSpace(prefixo.ToString()))
            {
                prefixo = prefixo.ToString().ToUpperInvariant();
            }

            IList<MonitoramentoPlanejamentoDescargaDto> result = _cteService.ListaMonitoramentoPlanejamentoDescarga(dataIni, dataFim, prefixo, os, terminal, vagao);
            return Json(new
            {

                Items = result.Select(c => new
                {
                    c.Vagao,
                    c.Serie,
                    DataSaidaEstacao = c.DataSaidaEstacao.ToString("dd/MM/yyyy hh:mm:ss"),
                    c.StatusCte,
                    c.NumeroCte,
                    c.ChaveCte,
                    c.Pdf,
                    c.Xml,
                    c.Estacao,
                    EmpresaCnpj = (c.Cnpj + " - " + c.RazaoSocial + " / OS: " + c.NumOS),
                    c.NumOS
                }),
                success = true
            });
        }

        [Autorizar(Transacao = "PLANEJAMENTODESCARGA", Acao = "Pesquisar")]
        public ActionResult Exportar(string dataInicial, string dataFinal, string prefixo, string os, string terminal)
        {
            if (terminal != string.Empty)
                terminal = terminal.Replace(".", "").Replace("/", "").Replace("-", "");

            var dataIni = DateTime.Now;
            var dataFim = DateTime.Now;
            if (!string.IsNullOrEmpty(dataInicial.ToString()))
                dataIni = Convert.ToDateTime(dataInicial);

            if (!string.IsNullOrEmpty(dataFinal.ToString()))
            {
                string texto = dataFinal;
                dataFim = Convert.ToDateTime(texto.ToString());
                dataFim = dataFim.AddDays(1).AddSeconds(-1);
            }
            if (!string.IsNullOrWhiteSpace(prefixo.ToString()))
            {
                prefixo = prefixo.ToString().ToUpperInvariant();
            }
            var result = _cteService.ListaMonitoramentoPlanejamentoDescarga(dataIni, dataFim, prefixo, os, terminal, string.Empty);
            var listEmpresa = new List<MonitoramentoDescarga>();
            foreach (var item in result.OrderBy(x => x.DataSaidaEstacao))
            {
                if (!listEmpresa.Any(x => x.empCnpj == item.Cnpj && x.empRazaoSocial == item.RazaoSocial && x.empNumOS == item.NumOS && x.empPrefixo == item.Prefixo && x.empEstacao == item.Estacao))
                {
                    listEmpresa.Add(new MonitoramentoDescarga
                    {

                        EmpresaAgrupada = "Planejamento: Data Saída Estação: " + item.DataSaidaEstacao.ToString("dd/MM/yyyy hh:mm:ss") + " / Estação: " + item.Estacao + " / OS: " + item.NumOS + " / Prefixo: " + item.Prefixo + " / Empresa: " + item.Cnpj + " - " + item.RazaoSocial,
                        empCnpj = item.Cnpj,
                        empEstacao = item.Estacao,
                        empNumOS = item.NumOS,
                        empPrefixo = item.Prefixo,
                        empRazaoSocial = item.RazaoSocial,
                    });
                }
            }

            foreach (var emp in listEmpresa)
            {
                foreach (var item in result)
                {
                    if (item.Cnpj == emp.empCnpj && emp.empRazaoSocial == item.RazaoSocial && emp.empNumOS == item.NumOS && emp.empPrefixo == item.Prefixo && emp.empEstacao == item.Estacao)
                    {
                        emp.MonitoramentoPlanejamentoDescarga.Add(new MonitoramentoPlanejamentoDescargaDto
                        {
                            ChaveCte = item.ChaveCte,
                            Cnpj = item.Cnpj,
                            DataSaidaEstacao = item.DataSaidaEstacao,
                            Estacao = item.Estacao,
                            NumeroCte = item.NumeroCte,
                            NumOS = item.NumOS,
                            Pdf = item.Pdf,
                            Prefixo = item.Prefixo,
                            RazaoSocial = item.RazaoSocial,
                            Serie = item.Serie,
                            StatusCte = item.StatusCte,
                            Vagao = item.Vagao,
                            Xml = item.Xml
                        });
                    }
                }
            }
            var dic = new Dictionary<string, Func<MonitoramentoPlanejamentoDescargaDto, string>>
            {
                ["Vagão"] = c => c.Vagao,
                ["Série"] = c => c.Serie,
                ["Estação"] = c => c.Estacao,
                ["Data Saída Estção"] = c => c.DataSaidaEstacao.ToString("dd/MM/yyyy hh:mm:ss"),
                ["Status CT-e"] = c => c.StatusCte,
                ["Número CT-e"] = c => c.NumeroCte.ToString(),
                ["Chave CT-e"] = c => c.ChaveCte,
                ["Pdf"] = c => c.Pdf,
                ["Xml"] = c => c.Xml
            };

            var list = listEmpresa != null ? listEmpresa.OrderBy(m => m.EmpresaAgrupada).ToList() : null;
            return ExcelAgrupado(string.Format("MonitoramentoPlanejamentoDescarga_{0}_{1}.xls", DateTime.Now.ToString("ddMMyyyy"), DateTime.Now.ToString("HHmmss")), dic, list);
        }

        private ActionResult ExcelAgrupado(string fileName, Dictionary<string, Func<MonitoramentoPlanejamentoDescargaDto, string>> fields, List<MonitoramentoDescarga> list)
        {
            string tempFilePath = Path.GetTempFileName();
            Workbook workbook = new Workbook();
            Worksheet worksheet = new Worksheet("Dados");

            int contador = 0;
            int linhas = 0;
            for (int i = 0; i < list.Count; i++)
            {
                if (linhas != 0)
                    linhas += 1;
                contador = 0;
                var valorTexto = list[i];
                var texto = list[i].EmpresaAgrupada.ToString();
                texto = texto.Replace("\n", " ").Replace("\r", " ");
                worksheet.Cells[linhas, contador] = new Cell(texto);
                linhas += 1;
                foreach (string key in fields.Keys)
                {
                    worksheet.Cells[linhas, contador] = new Cell(key);
                    contador++;
                }
                var monito = list[i].MonitoramentoPlanejamentoDescarga;
                for (int j = 0; j < monito.Count; j++)
                {
                    var ln = linhas;
                    var cont = 0;
                    foreach (var key in fields.Keys)
                    {
                        var valor = fields[key];
                        string valorString = valor.Invoke(monito[j]);
                        valorString = valorString.Replace("\n", " ").Replace("\r", " ");
                        worksheet.Cells[ln + 1, cont] = new Cell(valorString, CellFormatType.Percentage.ToString());
                        cont++;
                    }
                    linhas++;
                }
                linhas++;
            }
            linhas++;
            for (int i = linhas; i < linhas + 100; i++)
            {
                for (int z = 0; z < 8; z++)
                {
                    worksheet.Cells[i + 1, z] = new Cell(" ");

                }
            }
            workbook.Worksheets.Add(worksheet);
            workbook.Save(tempFilePath);

            var sr = new StreamReader(tempFilePath);
            var ms = new MemoryStream();
            StreamUtils.Copy(sr.BaseStream, ms, new byte[4096]);
            sr.Close();
            System.IO.File.Delete(tempFilePath);
            ms.Seek(0, SeekOrigin.Begin);

            var fsr = new FileStreamResult(ms, "application/ms-excel");
            fsr.FileDownloadName = fileName;
            return fsr;
        }
    }
}