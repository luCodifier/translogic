﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using ALL.Core.Dominio;
using NHibernate.Criterion;
using Translogic.Core.Infrastructure.Web;
using Translogic.Modules.Core.Domain.Model.Acesso;
using Translogic.Modules.Core.Domain.Model.Dto;
using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;
using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Nfes.Repositories;
using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos;
using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos.Repositories;
using Translogic.Modules.Core.Domain.Model.Seguro;
using Translogic.Modules.Core.Domain.Model.Seguro.Repositories;
using Translogic.Modules.Core.Domain.Services.FluxosComerciais;
using Translogic.Modules.Core.Domain.Services.Seguro;
using Translogic.Modules.Core.Infrastructure;

namespace Translogic.Modules.Core.Controllers.Seguro
{
    using Domain.Model.Via.Repositories;
    using System.Collections.Specialized;
    using Translogic.Modules.Core.Domain.Model.Tfa.Repositories;
using Translogic.Modules.Core.Domain.Model.Diversos.Repositories;
    using Translogic.Modules.Core.Domain.Services.Seguro.Interface;

    /// <summary>
    ///     Controller Seguro
    /// </summary>
    public class SeguroController : BaseSecureModuleController
    {
        const string CONFIGURACAO_GERAR_TFA_LAUDO = "GERAR_TFA_LAUDO";

        private readonly IAnaliseSeguroRepository _analiseSeguroRepository;
        private readonly IClienteSeguroRepository _clienteSeguroRepository;
        private readonly ICausaSeguroRepository _causaSeguroRepository;
        private readonly IConfGeralRepository _confGeralRepository;
        private readonly IContaContabilSeguroRepository _contaContabilSeguroRepository;
        private readonly ICteDetalheRepository _cteDetalheRepository;
        private readonly ICteEmpresasRepository _cteEmpresasRepository;
        private readonly ICteRepository _cteRepository;
        private readonly IDespachoTranslogicRepository _despachoTranslogicRepository;
        private readonly IItemDespachoRepository _itemDespachoRepository;
        private readonly IModalSeguroRepository _modalSeguroRepository;
        private readonly IProcessoSeguroService _processoSeguroService;
        private readonly ProcessoSeguroCacheService _processoSeguroCacheService;
        private readonly IProcessoSeguroRepository _processoSeguroRepository;
        private readonly IRateioSeguroRepository _rateioSeguroRepository;
        private readonly IUnidadeSeguroRepository _unidadeSeguroRepository;
        private readonly IAreaOperacionalRepository _areaOperacionalRepository;
        private readonly ICarregamentoRepository _carregamentoRepository;
        private readonly IHistoricoSeguroRepository _historicoSeguroRepository;
        private readonly INotaFiscalTranslogicRepository _notaFiscalRepository;
        private readonly NfeService _nfeService;
        private readonly IMangaRepository _mangaRepository;
        private readonly ITfaRepository _tfaRepository;
        private static string[] arrayKg = new string[] { "kg", "sac" };

        public SeguroController(ProcessoSeguroCacheService processoSeguroCacheService,
            IDespachoTranslogicRepository despachoTranslogicRepository, ICteRepository cteRepository,
            ICteDetalheRepository cteDetalheRepository, ICteEmpresasRepository cteEmpresasRepository,
            ICausaSeguroRepository causaSeguroRepository, IUnidadeSeguroRepository unidadeSeguroRepository,
            IProcessoSeguroRepository processoSeguroRepository, IModalSeguroRepository modalSeguroRepository,
            IRateioSeguroRepository rateioSeguroRepository, IAnaliseSeguroRepository analiseSeguroRepository,
            IContaContabilSeguroRepository contaContabilSeguroRepository, IClienteSeguroRepository clienteSeguroRepository,
            IAreaOperacionalRepository areaOperacionalRepository, ICarregamentoRepository carregamentoRepository,
            IHistoricoSeguroRepository historicoSeguroRepository, INotaFiscalTranslogicRepository notaFiscalRepository,
            NfeService nfeService,
            IMangaRepository mangaRepository,
            ITfaRepository tfaRepository,
            IConfGeralRepository confGeralRepository,
            IProcessoSeguroService processoSeguroService,
            IItemDespachoRepository itemDespachoRepository)
        {
            _processoSeguroCacheService = processoSeguroCacheService;
            _despachoTranslogicRepository = despachoTranslogicRepository;
            _itemDespachoRepository = itemDespachoRepository;
            _cteRepository = cteRepository;
            _cteDetalheRepository = cteDetalheRepository;
            _cteEmpresasRepository = cteEmpresasRepository;
            _causaSeguroRepository = causaSeguroRepository;
            _unidadeSeguroRepository = unidadeSeguroRepository;
            _processoSeguroRepository = processoSeguroRepository;
            _modalSeguroRepository = modalSeguroRepository;
            _rateioSeguroRepository = rateioSeguroRepository;
            _analiseSeguroRepository = analiseSeguroRepository;
            _contaContabilSeguroRepository = contaContabilSeguroRepository;
            _clienteSeguroRepository = clienteSeguroRepository;
            _areaOperacionalRepository = areaOperacionalRepository;
            _carregamentoRepository = carregamentoRepository;
            _historicoSeguroRepository = historicoSeguroRepository;
            _notaFiscalRepository = notaFiscalRepository;
            _nfeService = nfeService;
            _mangaRepository = mangaRepository;
            _tfaRepository = tfaRepository;
            _confGeralRepository = confGeralRepository;
            _processoSeguroService = processoSeguroService;
        }

        /// <summary>
        ///     Acao Padrao ao ser chamado o controller
        /// </summary>
        /// <returns>View Padrao</returns>
        [Autorizar(Transacao = "SEG_REG_LAUDOS")]
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        ///     Editar Processo
        /// </summary>
        /// <returns>View Edit</returns>
        [Autorizar(Transacao = "SEG_REG_LAUDOS")]
        public ActionResult Edit()
        {
            return View();
        }

        /// <summary>
        ///     Obtem uma lista de Processos em Cache
        /// </summary>
        /// <param name="pagination"> Paginacao pesquisa</param>
        /// <param name="filter"> Filtros para pesquisa</param>
        /// <returns>Lista de Processos em Cache</returns>
        [Autorizar(Transacao = "SEG_REG_LAUDOS", Acao = "Pesquisar")]
        public JsonResult ObterProcessosCacheUsuario(DetalhesPaginacaoWeb pagination, DetalhesFiltro<object>[] filter)
        {
            Usuario usuario = base.UsuarioAtual;
            ResultadoPaginado<ProcessoSeguroCacheDto> result =
                _processoSeguroCacheService.ObterProcessosSeguroCacheUsuario(pagination, filter, usuario);

            return Json(new
            {
                result.Total,
                Items = result.Items.Select(p => new
                {
                    Id = p.ProcessoSeguroCacheId,
                    p.Despacho,
                    p.Serie,
                    p.Nota,
                    p.Cliente,
                    p.Produto,
                    p.Peso,
                    p.Tipo,
                    p.Vagao,
                    p.Origem,
                    Data = p.DataSinistro.HasValue ? p.DataSinistro.Value.ToString("dd/MM/yyyy HH:mm:ss") : string.Empty,
                    //Data = p.DataSinistro.HasValue ? p.DataSinistro.Value.ToString("dd/MM/yyyy") : string.Empty,
                    p.Laudo,
                    p.Terminal,
                    p.ValorMercadoria,
                    S = p.TerminalDesc = _areaOperacionalRepository.ObterPorId((int?)p.Terminal).Descricao,
                    p.Perda,
                    Causa = _causaSeguroRepository.ObterPorId(p.Causa.Id).Descricao,
                    Gambit =
                        p.Gambit == "S" ? "SIM" : p.Gambit == "N" ? "NÃO" : p.Gambit == "P" ? "PARCIAL" : string.Empty,
                    Lacre = p.Lacre == "S" ? "SIM" : p.Lacre == "N" ? "NÃO" : p.Lacre == "P" ? "PARCIAL" : string.Empty,
                    p.Vistoriador,
                    Avaliacao = p.Avaliacao == "N" ? "DEVIDA" : p.Avaliacao == "S" ? "INDEVIDA" : string.Empty,
                    Unidade = _unidadeSeguroRepository.ObterPorId(p.Unidade.Id).Descricao,
                    p.Rateio,
                    ContaContabil = _contaContabilSeguroRepository.ObterPorId(p.ContaContabil.Id).Descricao,
                    p.Historico,
                    p.Sindicancia
                }),
                success = true
            });
        }

        /// <summary>
        ///     Obtem o combo de terminais
        /// </summary>
        /// <returns>Lista de Terminais</returns>
        [Autorizar(Transacao = "SEG_REG_LAUDOS", Acao = "Pesquisar")]
        public ActionResult ObterTerminais()
        {
            return
                Json(
                    _unidadeSeguroRepository.ObterTerminais()
                        .Select(t => new { Terminal = t.Id, TerminalLabel = t.Descricao }));
        }

        /// <summary>
        ///     Obtem o combo de terminais
        /// </summary>
        /// <returns>Lista de Terminais</returns>
        [Autorizar(Transacao = "SEG_REG_LAUDOS", Acao = "Pesquisar")]
        public ActionResult ObterTerminaisAo(string despacho, string serie, bool intercambio)
        {
            try
            {
                if (!intercambio)
                {
                    var lista = _areaOperacionalRepository.ObterTerminaisPorDespacho(Convert.ToInt32(despacho), serie);

                    return
                        Json(
                           lista
                                .Select(t => new { Terminal = t.IdTerminal, TerminalLabel = t.DescricaoTerminal }));
                }
                else
                {
                    var lista = _areaOperacionalRepository.ObterTerminaisPorDespachoIntercambio(Convert.ToInt32(despacho), serie);

                    return
                        Json(
                           lista
                                .Select(t => new { Terminal = t.IdTerminal, TerminalLabel = t.DescricaoTerminal }));
                }
            }
            catch
            {
                return null;
            }
        }

        bool IsAllDigits(string s)
        {
            foreach (char c in s)
            {
                if (!char.IsDigit(c))
                    return false;
            }
            return true;
        }

        /// <summary>
        ///     Obtem o combo de unidades
        /// </summary>
        /// <returns>Lista de Unidades</returns>
        [Autorizar(Transacao = "SEG_REG_LAUDOS", Acao = "Pesquisar")]
        public ActionResult ObterUnidades()
        {
            return
                Json(
                    _unidadeSeguroRepository.ObterUnidades()
                        .Select(t => new { Unidade = t.Id, UnidadeLabel = t.Descricao }));
        }

        /// <summary>
        ///     Obtem o combo de causas
        /// </summary>
        /// <returns>Lista de Causas</returns>
        [Autorizar(Transacao = "SEG_REG_LAUDOS", Acao = "Pesquisar")]
        public ActionResult ObterCausas()
        {
            return Json(_causaSeguroRepository.ObterCausas().Select(t => new { Causa = t.Id, CausaLabel = t.Descricao }));
        }

        /// <summary>
        ///     Obtem o combo de gambits
        /// </summary>
        /// <returns>Lista de Gambits</returns>
        [Autorizar(Transacao = "SEG_REG_LAUDOS", Acao = "Pesquisar")]
        public ActionResult ObterGambits()
        {
            IDictionary<string, string> listaGambit = new Dictionary<string, string>();

            listaGambit.Add("S", "SIM");
            listaGambit.Add("N", "NÃO");
            listaGambit.Add("P", "PARCIAL");

            return Json(listaGambit.Select(t => new { Gambit = t.Key, GambitLabel = t.Value }));
        }

        /// <summary>
        ///     Obtem o combo de lacres
        /// </summary>
        /// <returns>Lista de Lacres</returns>
        [Autorizar(Transacao = "SEG_REG_LAUDOS", Acao = "Pesquisar")]
        public ActionResult ObterLacres()
        {
            IDictionary<string, string> listaLacre = new Dictionary<string, string>();

            listaLacre.Add("S", "SIM");
            listaLacre.Add("N", "NÃO");
            listaLacre.Add("P", "PARCIAL");

            return Json(listaLacre.Select(t => new { Lacre = t.Key, LacreLabel = t.Value }));
        }

        /// <summary>
        ///     Obtem o combo de avalia��es
        /// </summary>
        /// <returns>Lista de Avalia��es</returns>
        [Autorizar(Transacao = "SEG_REG_LAUDOS", Acao = "Pesquisar")]
        public ActionResult ObterAvaliacoes()
        {
            IDictionary<string, string> listaAvaliacao = new Dictionary<string, string>();

            listaAvaliacao.Add("N", "DEVIDA");
            listaAvaliacao.Add("S", "INDEVIDA");

            return Json(listaAvaliacao.Select(t => new { Avaliacao = t.Key, AvaliacaoLabel = t.Value }));
        }

        /// <summary>
        ///     Obtem o combo de contas cont�beis
        /// </summary>
        /// <returns>Lista de Contas Cont�beis</returns>
        [Autorizar(Transacao = "SEG_REG_LAUDOS", Acao = "Pesquisar")]
        public ActionResult ObterContasContabeis()
        {
            return
                Json(
                    _contaContabilSeguroRepository.ObterContasContabeis()
                        .Select(t => new { ContaContabil = t.Id, ContaContabilLabel = t.Codigo + " - " + t.Descricao }));
        }

        /// <summary>
        ///     Excluir Processo em Cache
        /// </summary>
        /// <returns>Mensagem de retorno</returns>
        [Autorizar(Transacao = "SEG_REG_LAUDOS", Acao = "Salvar")]
        public JsonResult ExcluirProcessoSeguroCache(int idLaudo)
        {
            try
            {
                var processoSeguroCacheRemover = _processoSeguroCacheService.ObterPorId(idLaudo);
                _processoSeguroCacheService.RemoverProcessoSeguroCache(processoSeguroCacheRemover);
            }
            catch (Exception)
            {
                return Json(new
                {
                    sucesso = false,
                    message = "Ocorreu um erro inesperado ao excluir. Contate o administrador do sistema."
                });
            }

            return Json(new
            {
                sucesso = true,
                message = "Laudo excluído."
            });
        }

        /// <summary>
        ///     Salvar Cache de Processos do Usu�rio
        /// </summary>
        /// <returns>Lista de Processos Seguro em Cache</returns>
        [Autorizar(Transacao = "SEG_REG_LAUDOS", Acao = "Salvar")]
        public JsonResult SalvarProcessoSeguroCache(
            int? id,
            int? despacho,
            string serie,
            DateTime? data,
            int? laudo,
            string valorMercadoria,
            int? terminal,
            string perda,
            int? causa,
            string gambit,
            string lacre,
            string vistoriador,
            string avaliacao,
            int? unidade,
            string rateio,
            int? contaContabil,
            string historico,
            bool despachoIntercambio,
            double? sindicancia
            )
        {
            try
            {
                serie = serie.ToUpper();
                if (despacho == null)
                {
                    return Json(new
                    {
                        sucesso = false,
                        message = "Preencha o número do despacho!"
                    });
                }

                if (string.IsNullOrEmpty(serie))
                {
                    return Json(new
                    {
                        sucesso = false,
                        message = "Preencha a série do despacho!"
                    });
                }

                //Data NULA
                if (data == null)
                {
                    return Json(new
                    {
                        sucesso = false,
                        message = "Preencha a data do sinistro!"
                    });
                }

                //E1: Data Futura Superior ao data hora atual
                if (data > DateTime.Now)
                {
                    return Json(new
                    {
                        sucesso = false,
                        message = "Data limite permitida apenas até momento atual ou inferior."
                    });
                }

                if (laudo == null)
                {
                    return Json(new
                    {
                        sucesso = false,
                        message = "Preencha o número do laudo!"
                    });
                }

                if (string.IsNullOrEmpty(valorMercadoria))
                {
                    return Json(new
                    {
                        sucesso = false,
                        message = "Preencha o valor da mercadoria!"
                    });
                }

                if (terminal == null && id == null)
                {
                    return Json(new
                    {
                        sucesso = false,
                        message = "Selecione o terminal!"
                    });
                }

                if (string.IsNullOrEmpty(perda))
                {
                    return Json(new
                    {
                        sucesso = false,
                        message = "Preencha a quantidade de perda!"
                    });
                }
                //else
                //{

                double perdaEmToneladas = double.Parse(perda, System.Globalization.CultureInfo.InvariantCulture);
                //    if (perdaEmToneladas > 120.00)
                //    {
                //        return Json(new
                //        {
                //            sucesso = false,
                //            message = "A perda n�o pode ser superior a 120 toneladas!"
                //        });
                //    }
                //}

                if (causa == null && id == null)
                {
                    return Json(new
                    {
                        sucesso = false,
                        message = "Selecione a causa!"
                    });
                }

                if (string.IsNullOrEmpty(gambit))
                {
                    return Json(new
                    {
                        sucesso = false,
                        message = "Selecione a gambitagem!"
                    });
                }

                if (string.IsNullOrEmpty(lacre))
                {
                    return Json(new
                    {
                        sucesso = false,
                        message = "Selecione o lacre!"
                    });
                }

                if (string.IsNullOrEmpty(vistoriador))
                {
                    return Json(new
                    {
                        sucesso = false,
                        message = "Informe o vistoriador!"
                    });
                }

                if (string.IsNullOrEmpty(avaliacao))
                {
                    return Json(new
                    {
                        sucesso = false,
                        message = "Selecione a avaliação!"
                    });
                }

                if (unidade == null && id == null)
                {
                    return Json(new
                    {
                        sucesso = false,
                        message = "Selecione a unidade!"
                    });
                }

                if (string.IsNullOrEmpty(rateio))
                {
                    return Json(new
                    {
                        sucesso = false,
                        message = "Informe o percentual de rateio!"
                    });
                }

                if (contaContabil == null && id == null)
                {
                    return Json(new
                    {
                        sucesso = false,
                        message = "Selecione a conta contábil!"
                    });
                }


                ProcessoSeguroCache processoSeguroCache = id != null ? _processoSeguroCacheService.ObterPorId((int)id) : new ProcessoSeguroCache();

                if (id == null || ((processoSeguroCache.Despacho != despacho) || (processoSeguroCache.Serie != serie)))
                {

                    bool existeProcessoCacheDespachoSerie =
                        _processoSeguroCacheService.ExisteDespachoSerie((int)despacho, serie);
                    if (existeProcessoCacheDespachoSerie)
                    {
                        return Json(new
                        {
                            sucesso = false,
                            message = "Despacho e série já informados!"
                        });
                    }

                    DespachoTranslogic despachoSelecionado;

                    if (IsAllDigits(serie) && (!despachoIntercambio))
                    {
                        despachoSelecionado =
                            _despachoTranslogicRepository.ObterDespachoPeloNumeroDespachoSerie((int)despacho, serie)
                                                         .FirstOrDefault();
                    }
                    else
                    {
                        // Despacho de interc�mbio

                        despachoSelecionado =
                            _despachoTranslogicRepository.ObterDespachoPeloNumeroDespachoSerieIntercambio((int)despacho, serie)
                                                         .FirstOrDefault();
                    }

                    if (despachoSelecionado == null)
                    {
                        return Json(new
                        {
                            sucesso = false,
                            message = "Despacho e/ou série inválidos!"
                        });
                    }

                    //// Faturamento 2.0
                    //// Verificação e atribuição dos valores do para salvar o dados do processo seguro cache.
                    if (despachoSelecionado.PedidoDerivado != null &&
                        despachoSelecionado.PedidoDerivado.FluxoComercial != null &&
                        despachoSelecionado.PedidoDerivado.FluxoComercial.FluxoOperacional)
                    {
                        var pedidoDerivado = despachoSelecionado.PedidoDerivado;
                        var fluxoComercial = despachoSelecionado.PedidoDerivado.FluxoComercial;

                        var notas = _notaFiscalRepository.ObterNotaFiscalPorDespacho(despachoSelecionado);

                        string nota = string.Empty;
                        double peso = 0;
                        foreach (NotaFiscalTranslogic nf in notas)
                        {
                            if (nota == string.Empty)
                            {
                                nota = nf.NumNota;
                            }
                            else
                            {
                                nota += ";" + nf.NumNota;
                            }

                            peso += nf.PesoNotaFiscal.HasValue ? nf.PesoNotaFiscal.Value : 0.0;
                        }

                        if (fluxoComercial.EmpresaPagadora == null)
                        {
                            return Json(new
                            {
                                sucesso = false,
                                //message = "A perda n�o pode ser superior a " + manga + " toneladas!"
                                message = "Empresa pagadora não cadastrada para o fluxo operacional."
                            });
                        }

                        processoSeguroCache.ClienteSeguro =
                            _clienteSeguroRepository.ObterClientePorSigla(fluxoComercial.EmpresaPagadora.Sigla);

                        //E2: Valor do peso superior ao peso do vag�o
                        var itemDespacho = _itemDespachoRepository.ObterItemDespachoPorDespacho(despachoSelecionado);
                        var manga = Convert.ToDouble(_mangaRepository.ObterPesoMangaPorVagao(itemDespacho.Vagao.Codigo));

                        if (perdaEmToneladas > manga && manga != 0)
                        {
                            return Json(new
                            {
                                sucesso = false,
                                //message = "A perda n�o pode ser superior a " + manga + " toneladas!"
                                message = "Peso n�o permitido, n�o pode ser superior ao peso do vag�o."
                            });
                        }

                        //E3: Valores Negativos
                        if (perdaEmToneladas < 0)
                        {
                            return Json(new
                            {
                                sucesso = false,
                                message = "Valores negativos não permitidos."
                            });
                        }


                        processoSeguroCache.Despacho = despacho;
                        processoSeguroCache.Serie = serie;
                        processoSeguroCache.Nota = nota;
                        processoSeguroCache.Cliente = pedidoDerivado.EmpresaRemete.RazaoSocial;
                        processoSeguroCache.Produto = pedidoDerivado.FluxoComercial.Mercadoria.Produto.DescricaoDetalhada;
                        processoSeguroCache.Peso = peso;
                        processoSeguroCache.Tipo = itemDespacho.Vagao.Serie.Tipo.Codigo;
                        processoSeguroCache.CodFluxo = pedidoDerivado.FluxoComercial.Codigo;
                        processoSeguroCache.CodMercadoria = pedidoDerivado.FluxoComercial.Mercadoria.Codigo;
                        processoSeguroCache.Destino = pedidoDerivado.FluxoComercial.Destino.Id;
                        processoSeguroCache.DestinoFluxo = pedidoDerivado.FluxoComercial.Destino.Id;
                        processoSeguroCache.Origem = pedidoDerivado.FluxoComercial.Origem.Id;
                        processoSeguroCache.OrigemFluxo = pedidoDerivado.FluxoComercial.Origem.Id;
                        processoSeguroCache.Vagao = itemDespacho.Vagao;
                        processoSeguroCache.CodVagao = itemDespacho.Vagao.Codigo;
                        processoSeguroCache.OrigemDesc = pedidoDerivado.FluxoComercial.Origem.Descricao;
                        //processoSeguroCache.Piscofins = cte.FluxoComercial.Contrato.PercentualPis +
                        //                              cte.FluxoComercial.Contrato.PercentualCofins;


                        processoSeguroCache.Piscofins = pedidoDerivado.FluxoComercial.Contrato.ValorAliquotaPisProduto +
                                                        pedidoDerivado.FluxoComercial.Contrato.ValorAliquotaCofinsProduto;
                    }
                    else
                    {
                        Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Cte cte =
                            _cteRepository.ObterCtesPorDespacho((int)despachoSelecionado.Id).FirstOrDefault();

                        if (cte == null)
                        {
                            return Json(new
                            {
                                sucesso = false,
                                message = "Nenhum CTE cadastrado para o despacho e série informados!"
                            });
                        }

                        IList<CteDetalhe> listaCteDetalhe = _cteDetalheRepository.ObterPorCte(cte);
                        string nota = string.Empty;
                        double peso = 0;
                        foreach (CteDetalhe cteDetalhe in listaCteDetalhe)
                        {
                            if (nota == string.Empty)
                            {
                                nota = cteDetalhe.NumeroNota;
                            }
                            else
                            {
                                nota += ";" + cteDetalhe.NumeroNota;
                            }

                            peso += cteDetalhe.PesoNotaFiscal;
                        }
                        CteEmpresas cteEmpresa = _cteEmpresasRepository.ObterPorCte(cte);

                        if (cteEmpresa != null)
                        {
                            processoSeguroCache.ClienteSeguro =
                                _clienteSeguroRepository.ObterClientePorSigla(cteEmpresa.EmpresaTomadora.Sigla);
                        }

                        //E2: Valor do peso superior ao peso do vag�o
                        var manga = Convert.ToDouble(_mangaRepository.ObterPesoMangaPorVagao(cte.Vagao.Codigo));

                        if (perdaEmToneladas > manga && manga != 0)
                        {
                            return Json(new
                            {
                                sucesso = false,
                                //message = "A perda n�o pode ser superior a " + manga + " toneladas!"
                                message = "Peso n�o permitido, n�o pode ser superior ao peso do vag�o."
                            });
                        }

                        //E3: Valores Negativos
                        if (perdaEmToneladas < 0)
                        {
                            return Json(new
                            {
                                sucesso = false,
                                message = "Valores negativos não permitidos."
                            });
                        }


                        processoSeguroCache.Despacho = despacho;
                        processoSeguroCache.Serie = serie;
                        processoSeguroCache.Nota = nota;
                        processoSeguroCache.Cliente = cteEmpresa != null
                            ? cteEmpresa.EmpresaTomadora.RazaoSocial
                            : "Cliente Indisponível";
                        processoSeguroCache.Produto = cte.FluxoComercial.Mercadoria.Produto.DescricaoDetalhada;
                        processoSeguroCache.Peso = peso;
                        processoSeguroCache.Tipo = cte.Vagao.Serie.Tipo.Codigo;
                        processoSeguroCache.CodFluxo = cte.FluxoComercial.Codigo;
                        processoSeguroCache.CodMercadoria = cte.FluxoComercial.Mercadoria.Codigo;
                        processoSeguroCache.Destino = cte.FluxoComercial.Destino.Id;
                        processoSeguroCache.DestinoFluxo = cte.FluxoComercial.Destino.Id;
                        processoSeguroCache.Origem = cte.FluxoComercial.Origem.Id;
                        processoSeguroCache.OrigemFluxo = cte.FluxoComercial.Origem.Id;
                        processoSeguroCache.Vagao = cte.Vagao;
                        processoSeguroCache.CodVagao = cte.Vagao.Codigo;
                        processoSeguroCache.OrigemDesc = cte.FluxoComercial.Origem.Descricao;
                        //processoSeguroCache.Piscofins = cte.FluxoComercial.Contrato.PercentualPis +
                        //                              cte.FluxoComercial.Contrato.PercentualCofins;


                        processoSeguroCache.Piscofins = cte.FluxoComercial.Contrato.ValorAliquotaPisProduto +
                                                        cte.FluxoComercial.Contrato.ValorAliquotaCofinsProduto;

                    }
                }

                processoSeguroCache.Avaliacao = avaliacao == "DEVIDA" ? "N" : avaliacao == "INDEVIDA" ? "S" : avaliacao;
                processoSeguroCache.Unidade = unidade != null ? _unidadeSeguroRepository.ObterPorId((int)unidade) : processoSeguroCache.Unidade;
                processoSeguroCache.Rateio = double.Parse(rateio, System.Globalization.CultureInfo.InvariantCulture);
                //Convert.ToDouble(rateio.Replace('.', ','));
                processoSeguroCache.Causa = causa != null ? _causaSeguroRepository.ObterPorId((int)causa) : processoSeguroCache.Causa;
                processoSeguroCache.DataSinistro = data;
                processoSeguroCache.Gambit = gambit[0].ToString();
                processoSeguroCache.Historico = historico.ToUpper();
                processoSeguroCache.Lacre = lacre[0].ToString();
                processoSeguroCache.Laudo = laudo;
                processoSeguroCache.ValorMercadoria = double.Parse(valorMercadoria, System.Globalization.CultureInfo.InvariantCulture);
                //Convert.ToDouble(valorMercadoria.Replace(',', '.'));
                processoSeguroCache.Perda = double.Parse(perda, System.Globalization.CultureInfo.InvariantCulture);
                //Convert.ToDouble(perda.Replace(',', '.'));
                processoSeguroCache.Terminal = (decimal)terminal;
                processoSeguroCache.Usuario = base.UsuarioAtual;
                processoSeguroCache.Vistoriador = vistoriador.ToUpper();
                processoSeguroCache.ContaContabil = contaContabil != null ? _contaContabilSeguroRepository.ObterPorId((int)contaContabil) : processoSeguroCache.ContaContabil;
                processoSeguroCache.Sindicancia = sindicancia.HasValue ? Convert.ToInt64(sindicancia) : new Nullable<long>();

                _processoSeguroCacheService.Salvar(processoSeguroCache);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    sucesso = false,
                    message = "Ocorreu um erro inesperado ao salvar. Contate o administrador do sistema."
                });
            }

            var mensagem = id == null ? "Laudo adicionado." : "Laudo alterado.";

            return Json(new
            {
                sucesso = true,
                message = mensagem
            });
        }

        /// <summary>
        ///     Salvar Processos em Cache do Usu�rio
        /// </summary>
        /// <returns>Lista de Processos Seguro em Cache</returns>
        [Autorizar(Transacao = "SEG_REG_LAUDOS", Acao = "Salvar")]
        public JsonResult SalvarProcessosEmCache()
        {
            IDictionary<int?, int?> listaLaudosProcessos = new Dictionary<int?, int?>();
            IList<ModalSeguro> listaModaisDuplicadosDespachoSerie = new List<ModalSeguro>();
            // IList<ProcessoSeguro> listaProcessoComErroDespachoSerie = new List<ProcessoSeguro>();

            IList<ProcessoSeguroCache> listaProcessosUsuario = _processoSeguroCacheService.ObterPorUsuario(base.UsuarioAtual);
            var msg = string.Empty;

            // Valida��es
            foreach (var processoCache in listaProcessosUsuario)
            {
                if (!String.IsNullOrEmpty(processoCache.Historico))
                {
                    if (processoCache.Historico.Length > 500)
                    {
                        msg = "<tr><td>Histórico maior que 500 caracteres (despacho:" + processoCache.Despacho + " serie:" + processoCache.Serie + ")</td></tr>";
                    }
                }
            }

            if (!String.IsNullOrEmpty(msg))
            {
                return Json(new
                {
                    sucesso = false,
                    message = msg
                });
            }


            foreach (ProcessoSeguroCache processoCache in listaProcessosUsuario)
            {
                var listaModaisDespachoSerie = _modalSeguroRepository.ObterPorDespachoSerieVagao((int)processoCache.Despacho, processoCache.Serie, processoCache.Vagao.Codigo);
                if (!listaModaisDespachoSerie.Any())
                {
                    var processoSeguro = new ProcessoSeguro();

                    try
                    {
                        processoSeguro.Data = DateTime.Now;
                        processoSeguro.DataCadastro = DateTime.Now;
                        processoSeguro.DataSinistro = processoCache.DataSinistro;
                        processoSeguro.DataVistoria = processoCache.DataSinistro;
                        processoSeguro.IndicadorIndevido = string.IsNullOrEmpty(processoCache.Avaliacao)
                            ? "N"
                            : processoCache.Avaliacao;
                        processoSeguro.DataProcesso = DateTime.Now;
                        processoSeguro.UsuarioCadastro = base.UsuarioAtual.Codigo;
                        processoSeguro.Vistoriador = processoCache.Vistoriador;
                        processoSeguro.ContaContabil = processoCache.ContaContabil;
                        processoSeguro.ClienteSeguro = processoCache.ClienteSeguro;
                        processoSeguro.IndicadorSituacao = "P";
                        processoSeguro.Sindicancia = processoCache.Sindicancia;
                        _processoSeguroRepository.InserirOuAtualizar(processoSeguro);                        

                    }
                    catch
                    {
                        msg = "<tr><td>Erro ao salvar processo (despacho:" + processoCache.Despacho + " serie:" + processoCache.Serie + ")</td></tr>";
                        _processoSeguroRepository.Remover(processoSeguro);
                        continue;
                    }

                    try
                    {
                        processoSeguro.NumeroProcesso = processoSeguro.Id;
                        _processoSeguroRepository.InserirOuAtualizar(processoSeguro);
                    }
                    catch
                    {
                        msg = "<tr><td>Erro ao salvar processo (despacho:" + processoCache.Despacho + " serie:" + processoCache.Serie + ")</td></tr>";
                        _processoSeguroRepository.Remover(processoSeguro);
                        continue;
                    }

                    var historicoSeguro = new HistoricoSeguro();
                    try
                    {
                        historicoSeguro.TimeStamp = DateTime.Now;
                        historicoSeguro.ProcessoSeguro = processoSeguro;
                        historicoSeguro.Descricao = processoCache.Historico;
                        historicoSeguro.Usuario = processoCache.Usuario.Codigo;

                        _historicoSeguroRepository.Inserir(historicoSeguro);
                    }
                    catch
                    {
                        msg = "<tr><td>Erro ao salvar historico (despacho:" + processoCache.Despacho + " serie:" + processoCache.Serie + ")</td></tr>";
                        _historicoSeguroRepository.Remover(historicoSeguro);
                        _processoSeguroRepository.Remover(processoSeguro);
                        continue;
                    }

                    var modalSeguro = new ModalSeguro();
                    try
                    {

                        modalSeguro.CausaSeguro = processoCache.Causa;
                        modalSeguro.CodFluxo = processoCache.CodFluxo;
                        modalSeguro.CodMercadoria = processoCache.CodMercadoria;
                        modalSeguro.Data = DateTime.Now;
                        modalSeguro.DataTermo = processoCache.DataSinistro;
                        modalSeguro.Despacho = processoCache.Despacho;
                        modalSeguro.Destino = processoCache.Destino;
                        modalSeguro.DestinoFluxo = processoCache.DestinoFluxo;
                        modalSeguro.Gambit = processoCache.Gambit;
                        modalSeguro.NotaFiscal = processoCache.Nota;
                        modalSeguro.Origem = processoCache.Origem;
                        modalSeguro.OrigemFluxo = processoCache.OrigemFluxo;
                        modalSeguro.ProcessoSeguro = processoSeguro;
                        modalSeguro.QuantidadePerda = processoCache.Perda;
                        modalSeguro.ResponsavelTermo = processoCache.Vistoriador;
                        modalSeguro.Serie = processoCache.Serie;
                        modalSeguro.Termo = processoCache.Laudo.ToString();
                        modalSeguro.Terminal = (int?)processoCache.Terminal;

                        modalSeguro.Usuario = base.UsuarioAtual.Codigo;
                        modalSeguro.Vagao = processoCache.Vagao;
                        modalSeguro.ValorMercadoria = processoCache.ValorMercadoria;
                        modalSeguro.Lacre = processoCache.Lacre;
                        modalSeguro.Historico = processoCache.Historico;
                        modalSeguro.Piscofins = processoCache.Piscofins;

                        _modalSeguroRepository.InserirOuAtualizar(modalSeguro);
                    }
                    catch
                    {
                        msg = "<tr><td>Erro ao salvar modal (despacho:" + processoCache.Despacho + " serie:" + processoCache.Serie + ")</td></tr>";
                        _historicoSeguroRepository.Remover(historicoSeguro);
                        _processoSeguroRepository.Remover(processoSeguro);
                        continue;
                    }

                    var rateioSeguro = new RateioSeguro();
                    try
                    {
                        rateioSeguro.CentroCusto = "";
                        rateioSeguro.CodUsuario = base.UsuarioAtual.Codigo;
                        rateioSeguro.Data = DateTime.Now;
                        rateioSeguro.PercentualRateio = processoCache.Rateio;
                        rateioSeguro.ProcessoSeguro = processoSeguro;
                        rateioSeguro.UnidadeSeguro = processoCache.Unidade;

                        _rateioSeguroRepository.InserirOuAtualizar(rateioSeguro);
                    }
                    catch
                    {
                        msg = "<tr><td>Erro ao salvar rateio (despacho:" + processoCache.Despacho + " serie:" + processoCache.Serie + ")</td></tr>";
                        _historicoSeguroRepository.Remover(historicoSeguro);
                        _modalSeguroRepository.Remover(modalSeguro);
                        _processoSeguroRepository.Remover(processoSeguro);
                        continue;
                    }

                    var analiseSeguro = new AnaliseSeguro();
                    try
                    {
                        analiseSeguro.Data = DateTime.Now;
                        analiseSeguro.IndicadorSituacao = "A";
                        analiseSeguro.ProcessoSeguro = processoSeguro;
                        _analiseSeguroRepository.InserirOuAtualizar(analiseSeguro);
                    }
                    catch
                    {
                        msg = "<tr><td>Erro ao salvar analise (despacho:" + processoCache.Despacho + " serie:" + processoCache.Serie + ")</td></tr>";
                        _historicoSeguroRepository.Remover(historicoSeguro);
                        _modalSeguroRepository.Remover(modalSeguro);
                        _rateioSeguroRepository.Remover(rateioSeguro);
                        _processoSeguroRepository.Remover(processoSeguro);
                        continue;
                    }

                    listaLaudosProcessos.Add(processoSeguro.NumeroProcesso, processoCache.Laudo);

                    _processoSeguroCacheService.RemoverProcessoSeguroCache(processoCache);

                    //Só gera TFA se o parâmetro estiver ativo 
                    var configuracaoGerarTfaLaudo = _confGeralRepository.ObterPorId(CONFIGURACAO_GERAR_TFA_LAUDO);

                    if (configuracaoGerarTfaLaudo != null && configuracaoGerarTfaLaudo.Valor.Equals("S"))
                    {
                        //Cria automaticamente o TFA para o Processo informado
                        _tfaRepository.CriarTfaProcesso(processoSeguro.NumeroProcesso.Value);

                        //Gera automaticamente o dossiê para o processo informado
                        _processoSeguroService.GerarDossie(processoSeguro.NumeroProcesso.Value);
                    }
                }
                else
                {
                    foreach (var modalSeguroDespachoSerie in listaModaisDespachoSerie)
                    {
                        listaModaisDuplicadosDespachoSerie.Add(modalSeguroDespachoSerie);
                    }
                }
            }

            string mensagem = string.Empty;

            if (listaModaisDuplicadosDespachoSerie.Any())
            {
                mensagem += "Já existe um processo criado para o despacho e série: <br /><br />";

                mensagem += "<table width='100%' style='border:1px black solid;'>";
                mensagem += "<tr style='background-color:#ccc;'><td width='50%' align='left'><b>Despacho</b></td><td width='50%' align='left'><b>Série</b></td></tr>";

                bool trocalinha = false;
                const string linhaEscura = "#efefef";
                const string linhaClara = "#fff";
                foreach (var modalDuplicadosDespachoSerie in listaModaisDuplicadosDespachoSerie.GroupBy(m => new { m.Despacho, m.Serie }).Select(mo => mo.First()).ToList())
                {
                    string corDalinha = trocalinha ? linhaClara : linhaEscura;
                    mensagem += "<tr style='background-color: " + corDalinha + ";'><td width='50%' align='left'>" +
                                modalDuplicadosDespachoSerie.Despacho + "</td><td width='50%' align='left'>" + modalDuplicadosDespachoSerie.Serie +
                                "</td></tr>";
                    trocalinha = !trocalinha;
                }
                mensagem += "</table><br/><br/>";
            }

            if (listaLaudosProcessos.Any())
            {
                mensagem += "Laudos salvos com sucesso: <br /><br />";
                mensagem += "<table width='100%' style='border:1px black solid;'>";
                mensagem +=
                    "<tr style='background-color:#ccc;'><td width='50%' align='left'><b>Laudo</b></td><td width='50%' align='left'><b>Processo</b></td></tr>";

                bool trocalinha = false;
                const string linhaEscura = "#efefef";
                const string linhaClara = "#fff";
                foreach (var processoLaudo in listaLaudosProcessos)
                {
                    string corDalinha = trocalinha ? linhaClara : linhaEscura;
                    mensagem += "<tr style='background-color: " + corDalinha + ";'><td width='50%' align='left'>" +
                                processoLaudo.Value + "</td><td width='50%' align='left'>" + processoLaudo.Key +
                                "</td></tr>";
                    trocalinha = !trocalinha;
                }
                mensagem += "</table>";
            }

            if (!string.IsNullOrEmpty(msg))
            {
                msg = "<table>" + msg + "</table>";
            }

            return Json(new
            {
                sucesso = true,
                message = mensagem + msg
            });
        }

        /// <summary>
        ///     A��o do controller para apresenta��o da View DespachoNotas
        /// </summary>
        /// <param name="numeroDespacho">n�mero do despacho</param>
        /// <param name="numeroSerie">n�mero da s�rie do despacho</param>
        /// <returns>Action Result</returns>
        public ActionResult DespachoNotas()
        {
            return this.View();
        }

        /// <summary>
        ///     A��o do controller para consulta das notas do despacho e s�rie
        /// </summary>
        /// <param name="numeroDespacho">n�mero do despacho</param>
        /// <param name="numeroSerie">n�mero da s�rie do despacho</param>
        /// <param name="despachoIntercambio">indica se o despacho � de interc�mbio</param>
        /// <returns>Action Result</returns>
        public ActionResult DespachoNotasConsulta(string numeroDespacho, string numeroSerie, bool despachoIntercambio)
        {

            try
            {
                IList<DespachoTranslogic> despachos = new List<DespachoTranslogic>();

                if ((String.IsNullOrEmpty(numeroDespacho)) || (String.IsNullOrEmpty(numeroSerie)))
                {
                    return
                    this.Json(
                        new
                        {
                            Sucesso = false,
                            Mensagem = "Nenhum nota foi encontrada, pois o Despacho e a Série não foram informados!"
                        });
                }

                numeroDespacho = numeroDespacho.ToUpper();
                numeroSerie = numeroSerie.ToUpper();

                if (despachoIntercambio)
                    despachos = _despachoTranslogicRepository.ObterDespachoPeloNumeroDespachoSerieIntercambio(
                        Convert.ToInt32(numeroDespacho), numeroSerie);
                else
                    despachos = _despachoTranslogicRepository.ObterDespachoPeloNumeroDespachoSerie(
                        Convert.ToInt32(numeroDespacho), numeroSerie);

                List<NotaFiscalTranslogicProdutoDto> result = new List<NotaFiscalTranslogicProdutoDto>();

                if (despachos != null)
                {
                    var dsp = despachos.FirstOrDefault();
                    var notas = _notaFiscalRepository.ObterNotaFiscalPorDespacho(dsp);

                    if (notas != null)
                    {
                        //if (notas.Any())
                        //{                       
                        //result.AddRange(notas);
                        //}                    
                        foreach (var nf in notas)
                        {
                            NotaFiscalTranslogicProdutoDto nfProdDto = new NotaFiscalTranslogicProdutoDto();
                            nfProdDto.notaFiscalTranslogic = nf;

                            var nfeBd = _nfeService.ObterDadosNfeBancoDados(nf.ChaveNotaFiscalEletronica);
                            var nfeProdutos = (nfeBd == null ? null : _nfeService.ObterProdutosNfe(nfeBd));

                            if (nfeProdutos != null)
                            {
                                foreach (var prod in nfeProdutos)
                                {
                                    nfProdDto.valorUnitarioProduto = prod.ValorUnitarioComercializacao;
                                    nfProdDto.unidadeMedidaProduto = prod.UnidadeComercial;
                                    nfProdDto.quantidadeProduto = prod.QuantidadeComercial;

                                    // Se estiver em KG, multiplica por 1000 para obter valor em toneladas
                                    if (arrayKg.Contains(prod.UnidadeComercial.ToLower()))
                                        nfProdDto.valorUnitarioProduto = nfProdDto.valorUnitarioProduto * 1000;
                                }
                            }

                            result.Add(nfProdDto);
                        }
                    }
                }

                return
                    this.Json(
                        new
                        {
                            Sucesso = true,
                            Mensagem = "",
                            items = result.Select(
                                    nf =>
                                    new
                                    {
                                        NumNota = nf.notaFiscalTranslogic.NumNota,
                                        SerieNota = nf.notaFiscalTranslogic.SerieNota,
                                        ChaveNota = nf.notaFiscalTranslogic.ChaveNotaFiscalEletronica,
                                        Volume = (nf.notaFiscalTranslogic.VolumeNotaFiscal.HasValue ?
                                                        nf.notaFiscalTranslogic.VolumeNotaFiscal.Value : 0.00),
                                        Peso = (nf.notaFiscalTranslogic.PesoNotaFiscal.HasValue ?
                                                        nf.notaFiscalTranslogic.PesoNotaFiscal.Value : 0.00),
                                        ProdutoValor = nf.valorUnitarioProduto,
                                        ProdutoUnidadeMedida = nf.unidadeMedidaProduto,
                                        ProdutoQuantidade = nf.quantidadeProduto
                                    }).Distinct()
                        });
            }
            catch (Exception ex)
            {
                return
                    this.Json(
                        new
                        {
                            Sucesso = false,
                            Mensagem = "Falha ao obter as notas fiscais",
                        });
            }

        }

        /// <summary>
        ///     A��o do controller para obter o valor da mercadoria do despacho e s�rie
        /// </summary>
        /// <param name="numeroDespacho">n�mero do despacho</param>
        /// <param name="serieDespacho">n�mero da s�rie do despacho</param>
        /// <param name="despachoIntercambio">indica se o despacho � de interc�mbio</param>
        public ActionResult ObterDespachoValorUnitarioMercadoria(string numeroDespacho, string serieDespacho, bool despachoIntercambio)
        {

            try
            {
                double? valorProdutoUnitario = null;
                double? somaValorProdutoNotas = null;
                double? valorMercadoriaUnitario = null;                

                if ((String.IsNullOrEmpty(numeroDespacho)) || (String.IsNullOrEmpty(serieDespacho)))
                {
                    return Json(new
                    {
                        Success = true,
                        Message = "",
                        ValorMercadoriaUnitario = 0.00
                    });
                }

                numeroDespacho = numeroDespacho.ToUpper();
                serieDespacho = serieDespacho.ToUpper();

                IList<DespachoTranslogic> despachos = new List<DespachoTranslogic>();

                if (despachoIntercambio)
                    despachos = _despachoTranslogicRepository.ObterDespachoPeloNumeroDespachoSerieIntercambio(
                        Convert.ToInt32(numeroDespacho), serieDespacho);
                else
                    despachos = _despachoTranslogicRepository.
                        ObterDespachoPeloNumeroDespachoSerie(Convert.ToInt32(numeroDespacho), serieDespacho);

                if (despachos != null)
                {
                    if (despachos.Count() <= 0)
                    {
                        return Json(new
                        {
                            Success = false,
                            Message = "Despacho/Série inválido!",
                            ValorMercadoriaUnitario = 0.00
                        });
                    }

                    var dsp = despachos.FirstOrDefault();
                    var notas = _notaFiscalRepository.ObterNotaFiscalPorDespacho(dsp);

                    //// Faturamento 2.0
                    //// Verificação do fluxo operacional ao obter o valor da nota fiscal.
                    var pedidoDerivado = dsp.PedidoDerivado;
                    if (pedidoDerivado != null && 
                        pedidoDerivado.FluxoComercial.FluxoOperacional)
                    {
                        foreach (var nf in notas)
                        {
                            valorProdutoUnitario = nf.ValorNotaFiscal;
                            somaValorProdutoNotas = somaValorProdutoNotas.HasValue ? somaValorProdutoNotas : 0.0;
                            somaValorProdutoNotas += valorProdutoUnitario.Value;
                        }

                        valorMercadoriaUnitario = somaValorProdutoNotas.HasValue ? Math.Round(somaValorProdutoNotas.Value, 2) : 0.00;
                    }
                    else
                    {
                        if (notas != null)
                        {
                            var count = notas.Count();

                            var chavesNfe = "";
                            var contador = 1;
                            foreach (var nf in notas)
                            {
                                chavesNfe += nf.ChaveNotaFiscalEletronica;
                                if (contador < count)
                                {
                                    chavesNfe += ";";
                                }
                                contador++;
                            }

                            var notasFiscais = _nfeService.ObterDadosNfesBancoDados(chavesNfe);
                            var listNfeLida = new StringCollection();
                            foreach (var nf in notasFiscais)
                            {
                                if (!listNfeLida.Contains(nf.ChaveNfe))
                                {
                                    listNfeLida.Add(nf.ChaveNfe);
                                    var nfeProdutos = _nfeService.ObterProdutosNfe(nf);
                                    if (nfeProdutos != null)
                                    {
                                        foreach (var prod in nfeProdutos)
                                        {
                                            if (!valorProdutoUnitario.HasValue)
                                            {
                                                valorProdutoUnitario = prod.ValorUnitarioComercializacao;
                                                // Se estiver em KG, multiplica por 1000 para obter valor em toneladas
                                                if (arrayKg.Contains(prod.UnidadeComercial.ToLower()))
                                                    valorProdutoUnitario = valorProdutoUnitario.Value * 1000;
                                            }
                                        }

                                        valorProdutoUnitario = valorProdutoUnitario.HasValue ? valorProdutoUnitario : 0.0;
                                        somaValorProdutoNotas = somaValorProdutoNotas.HasValue ? somaValorProdutoNotas : 0.0;

                                        somaValorProdutoNotas += valorProdutoUnitario.Value;

                                        valorProdutoUnitario = null;
                                    }
                                }
                            }


                            //foreach (var nf in notas)
                            //{
                            //    var nfeBd = _nfeService.ObterDadosNfeBancoDados(nf.ChaveNotaFiscalEletronica);
                            //    var nfeProdutos = (nfeBd == null ? null : _nfeService.ObterProdutosNfe(nfeBd));
                            //    if (nfeProdutos != null)
                            //    {
                            //        foreach (var prod in nfeProdutos)
                            //        {
                            //            if (!valorProdutoUnitario.HasValue)
                            //            {
                            //                valorProdutoUnitario = prod.ValorUnitarioComercializacao;
                            //                // Se estiver em KG, multiplica por 1000 para obter valor em toneladas
                            //                if (arrayKg.Contains(prod.UnidadeComercial.ToLower()))
                            //                    valorProdutoUnitario = valorProdutoUnitario.Value * 1000;
                            //            }
                            //        }

                            //        valorProdutoUnitario = valorProdutoUnitario.HasValue ? valorProdutoUnitario : 0.0;
                            //        somaValorProdutoNotas = somaValorProdutoNotas.HasValue ? somaValorProdutoNotas : 0.0;

                            //        somaValorProdutoNotas += valorProdutoUnitario.Value;

                            //        valorProdutoUnitario = null;
                            //    }
                            //}

                            somaValorProdutoNotas = somaValorProdutoNotas.HasValue ? somaValorProdutoNotas : 0.0;

                            // C�lculo da m�dia caso tenha 2 notas ou mais
                            if (count > 1)
                                somaValorProdutoNotas = (somaValorProdutoNotas.Value / count);


                            valorMercadoriaUnitario = somaValorProdutoNotas.HasValue ? Math.Round(somaValorProdutoNotas.Value, 2) : 0.00;
                        }
                    }
                }

                return Json(new
                {
                    Success = true,
                    Message = "",
                    ValorMercadoriaUnitario = valorMercadoriaUnitario
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message,
                    ValorMercadoriaUnitario = 0.00
                });
            }
        }

    }
}