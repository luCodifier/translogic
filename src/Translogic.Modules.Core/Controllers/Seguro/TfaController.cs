﻿namespace Translogic.Modules.Core.Controllers.Seguro
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.IO;
    using System.Linq;
    using System.Web.Mvc;
    using Translogic.Core.Infrastructure.Aws;
    using Translogic.Core.Infrastructure.FileSystem;
    using Translogic.Core.Infrastructure.Web;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Modules.Core.Domain.Services.Seguro;
    using Translogic.Modules.Core.Domain.Services.Seguro.Interface;
    using Translogic.Modules.Core.Domain.Services.Tfa.Interface;
    using Translogic.Modules.Core.Infrastructure;
    using Translogic.Modules.TPCCO.Helpers;
    using System.Collections.Specialized;
    using Translogic.Modules.Core.Domain.Model.Tfa;
    using Translogic.Modules.Core.Domain.Model.Seguro.Repositories;
    using System.ComponentModel.DataAnnotations;
    using System.Web;
    using System.Text;

    public class TfaController : BaseSecureModuleController
    {

        #region enumerations

        [Flags]
        private enum DossieOptions
        {
            TFA = 1,
            CTE = 2,
            NFE = 4,
            TKT = 8,
            Foto = 16
        }

        #endregion

        #region Constants
        private const string TemporaryFolderKey = "TemporaryFolder";
        private const string TemporaryZipFileName = "laudos.zip";
        private const string PermitedFileExtension = ".pdf";
        #endregion

        #region Serviços
        private readonly ITfaService _tfaService;
        private readonly ITfaAnexoService _tfaAnexoService;
        private readonly IMalhaSeguroService _malhaService;
        private readonly CausaSeguroService _causaSeguroService;
        private readonly IModalSeguroRepository _modalSeguroRepository;

        #endregion

        #region Repositórios
        private readonly IProcessoSeguroRepository _processoSeguroRepository;
        #endregion

        #region Construtores



        public TfaController(
                                    ITfaService tfaService,
                                    ITfaAnexoService tfaAnexoService,
                                    IMalhaSeguroService malhaService,
                                    CausaSeguroService causaSeguroService,
                                    IModalSeguroRepository modalSeguroRepository,
                                    IProcessoSeguroRepository processoSeguroRepository)
        {

            _tfaService = tfaService;
            _tfaAnexoService = tfaAnexoService;
            _malhaService = malhaService;
            _causaSeguroService = causaSeguroService;
            _modalSeguroRepository = modalSeguroRepository;
            _processoSeguroRepository = processoSeguroRepository;
        }

        #endregion

        #region ActionViews
        // GET: /ConsultaTfa/
        [HttpGet]
        public ActionResult Tfa()
        {
            if (TempData["TemAnexos"] != null)
            {
                ViewData["TemAnexos"] = true;
                ViewData["Anexos"] = TempData["Anexos"];
            }
            else
            {
                ViewData["TemAnexos"] = false;
            }

            if (TempData["Erros"] != null)
            {
                if ((TempData["Erros"] as StringCollection).Count > 0)
                {
                    ViewData["Erros"] = TempData["Erros"] as StringCollection;
                }
            }

            return View();
        }

        public ActionResult ModalStatus(string[] anexos, string[] statusAnexos)
        {
            if (anexos.Length > 0)
            {
                if (anexos[0] != "")
                {
                    ViewData["Anexos"] = anexos;
                    ViewData["StatusAnexos"] = statusAnexos;
                }
            }

            return View();
        }

        #endregion

        #region Métodos

        private string GetTemporaryFolder()
        {
            string temporaryFolderValue = string.Empty;
            AppSettingsReader reader = new AppSettingsReader();
            try
            {
                temporaryFolderValue = (string)reader.GetValue(TemporaryFolderKey, temporaryFolderValue.GetType());

                //if (!string.IsNullOrEmpty(temporaryFolderValue))
                //{
                //    var temporaryFolder = Server.MapPath(temporaryFolderValue);
                //    return temporaryFolder;
                //}

            }
            catch { }

            return temporaryFolderValue;
        }

        [HttpPost]
        [Autorizar(Transacao = "CONSUTFA", Acao = "Pesquisar")]
        public ActionResult Upload()
        {
            var anexos = new List<AnexoLaudoDto>();
            var erros = new StringCollection();
            if (Request.Files.Count == 1) // Somente um arquivo .zip
            {
                try
                {
                    var fileContent = Request.Files[0];
                    if (fileContent != null && fileContent.ContentLength > 0)
                    {
                        if (!fileContent.FileName.ToLower().Contains(".zip"))
                        {
                            erros.Add("Arquivo em anexo deve ser um .zip");
                            TempData["Erros"] = erros;
                            return RedirectToAction("Tfa");
                        }

                        if (fileContent.ContentLength > 50000000)
                        {
                            erros.Add("Arquivo em anexo deve ter no máximo 50 MB");
                            TempData["Erros"] = erros;
                            return RedirectToAction("Tfa");
                        }

                        FileService fileService = new FileService();
                        AmazonUploader cloudUploader = new AmazonUploader();
                        var temporaryFolder = Path.Combine(GetTemporaryFolder(), UsuarioAtual.Codigo.ToString());
                        var temporaryPath = fileService.GetAndCreateDirectory(temporaryFolder);
                        var zipFileDirectory = Path.Combine(temporaryPath, TemporaryZipFileName);

                        // Remove arquivos anteriores
                        fileService.DeleteFilesInDirectory(temporaryPath);
                        fileService.DeleteAllFolders(temporaryPath);

                        // Salva arquivo .zip
                        fileService.CopyFileStreamTo(fileContent.InputStream, zipFileDirectory);

                        // Extrai arquivos
                        fileService.ExtractZipToDirectory(zipFileDirectory, temporaryPath);

                        // Valida arquivos descarregados
                        var files = fileService.GetDirectoryFiles(temporaryPath, true);
                        int fileCount = files.Count;
                        foreach (var file in files)
                        {
                            if (file.ToLower().Contains(PermitedFileExtension))
                            {
                                var index = file.LastIndexOf(@"\") + 1;
                                string fileName = file.Substring(index, (file.Length - index) - PermitedFileExtension.Length);
                                string status = "";

                                var numProcesso = fileName.ToLower().Replace("tfa_", "");
                                int idProcesso = 0;
                                int.TryParse(numProcesso, out idProcesso);
                                var processoSeguro = _processoSeguroRepository.ObterPorId(Convert.ToInt32(idProcesso));
                                if (processoSeguro == null)
                                {
                                    status = "inexistente";
                                }
                                else
                                {
                                    // Validando se foi carregado por mobile
                                    var tfa = _tfaService.ObterPorNumProcesso(idProcesso);
                                    var origemApp = false;

                                    if (tfa != null)
                                    {
                                        if (tfa.OrigemProcessoApp)
                                        {
                                            origemApp = true;
                                        }
                                    }

                                    if (!origemApp)
                                    {
                                        var tfaAnexo = _tfaAnexoService.ObterPorNumProcesso(numProcesso);
                                        if (tfaAnexo != null)
                                        {
                                            status = "atualizado";
                                        }
                                        else
                                        {
                                            status = "carregado";

                                            tfaAnexo = new TfaAnexo();
                                            decimal decProcesso = 0;
                                            decimal.TryParse(numProcesso, out decProcesso);
                                            tfaAnexo.NumProcesso = decProcesso;
                                        }

                                        tfaAnexo.ArquivoPdf = System.IO.File.ReadAllBytes(file);

                                        _tfaAnexoService.Salvar(tfaAnexo);

                                        using (FileStream fileStream = new FileStream(file, FileMode.Open, FileAccess.Read))
                                        {
                                            string folder = numProcesso;
                                            string uploadFileName = "TFA_" + folder + ".pdf";

                                            // Upload para Amazon
                                            var carregado = cloudUploader.UploadObject(fileStream, uploadFileName, folder);

                                            if (!carregado)
                                            {
                                                erros.Add("Erro ao carregar arquivo " + file);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        status = "origemapp";
                                    }
                                }
                                // Lista de anexados
                                anexos.Add(new AnexoLaudoDto() { NumProcesso = fileName, Status = status });

                                string fileFullPath = file;
                                //Se arquivo ZIP possui apenas 1 anexo e este anexo é de formato diferente de .PDF retorna msg erro ao usuario
                                if (fileCount == 2 && file.ToLower().Contains(PermitedFileExtension) == false && fileFullPath.Substring(file.Length - 3, 3) != "zip")
                                {
                                    throw new Exception("Arquivo ZIP possui anexo em formato incorreto!");
                                }
                            }
                        }

                        //Excluir arquivos e diretório
                        fileService.DeleteDirectory(temporaryFolder);
                    }
                }
                catch (Exception ex)
                {
                    erros.Add("Erro oa executar processo de upload. Erro: " + ex.Message.Replace("'", ""));
                }
            }

            TempData["TemAnexos"] = anexos.Count > 0;
            TempData["Anexos"] = anexos;
            TempData["Erros"] = erros;

            return RedirectToAction("Tfa");
        }

        /// <summary>
        /// Transformar lista em string contendo dados separados por ; para exportar para CSV
        /// </summary>
        /// <typeparam name="T">Tipo</typeparam>
        /// <param name="list">Lista</param>
        /// <returns>String CSV</returns>
        private string ListToCSV<T>(IEnumerable<T> list)
        {
            System.Text.StringBuilder sList = new System.Text.StringBuilder();
            Type type = typeof(T);
            var props = type.GetProperties();
            //sList.Append(string.Join(";", props.Select(p => (p.GetCustomAttributes(true)[0] as DisplayAttribute).Name))).Replace("DATA PROCESSO LEGADO",string.Empty);
            sList.Append(string.Join(";", props.Select(p => (p.GetCustomAttributes(true)[0] as DisplayAttribute).Name)));
            //sList.Append(string.Join(";", props.Select(p => (p.GetCustomAttributes(true)[0] as DisplayAttribute).Name))).Replace("SINDICÂNCIA", string.Empty);
            sList.Append(Environment.NewLine);

            foreach (var element in list)
            {
                sList.Append(string.Join(";", props.Select(p => p.GetValue(element, null))));
                sList.Append(Environment.NewLine);
            }

            return sList.ToString();
        }

        /// <summary>
        /// Pesquisa para exportação para Excel
        /// </summary>
        /// <param name="LiberacaoFormacaoTremDto"></param>
        /// <returns></returns>
        [Autorizar(Transacao = "CONSUTFA", Acao = "Pesquisar")]
        public ActionResult ObterConsultaTFAExportar(string dtInicioProcesso, string dtFinalProcesso, string terminal, string un, string up, string malha, string provisionado,
                                                     string dtInicioSinistros, string dtFinalSinistros, string situacao, string causa, string anexado)
        {
            string numProcesso = string.Empty;
            string numTermo = string.Empty;
            string os = string.Empty;
            string sindicancia = string.Empty;
            string vagao = string.Empty;

            if (this.ControllerContext.HttpContext.Request.Cookies.AllKeys.Contains("numProcesso"))
            {
                HttpCookie cookieProcesso = this.ControllerContext.HttpContext.Request.Cookies["numProcesso"];
                cookieProcesso.Expires = DateTime.Now.AddDays(-1);
                numProcesso = cookieProcesso.Value;
                //this.ControllerContext.HttpContext.Response.Cookies.Add(cookieProcesso);
            }

            if (this.ControllerContext.HttpContext.Request.Cookies.AllKeys.Contains("numTermo"))
            {
                HttpCookie cookieTermo = this.ControllerContext.HttpContext.Request.Cookies["numTermo"];
                cookieTermo.Expires = DateTime.Now.AddDays(-1);
                numTermo = cookieTermo.Value;
                //this.ControllerContext.HttpContext.Response.Cookies.Add(cookieTermo);
            }

            if (this.ControllerContext.HttpContext.Request.Cookies.AllKeys.Contains("os"))
            {
                HttpCookie cookieOs = this.ControllerContext.HttpContext.Request.Cookies["os"];
                cookieOs.Expires = DateTime.Now.AddDays(-1);
                os = cookieOs.Value;
                //this.ControllerContext.HttpContext.Response.Cookies.Add(cookieTermo);
            }

            if (this.ControllerContext.HttpContext.Request.Cookies.AllKeys.Contains("sindicancia"))
            {
                HttpCookie cookiesindicancia = this.ControllerContext.HttpContext.Request.Cookies["sindicancia"];
                cookiesindicancia.Expires = DateTime.Now.AddDays(-1);
                sindicancia = cookiesindicancia.Value;
                //this.ControllerContext.HttpContext.Response.Cookies.Add(cookieTermo);
            }

            if (this.ControllerContext.HttpContext.Request.Cookies.AllKeys.Contains("vagao"))
            {
                HttpCookie cookievagao = this.ControllerContext.HttpContext.Request.Cookies["vagao"];
                cookievagao.Expires = DateTime.Now.AddDays(-1);
                vagao = cookievagao.Value;
                //this.ControllerContext.HttpContext.Response.Cookies.Add(cookieTermo);
            }

            var vagoes = vagao.Trim().Replace(";", ",").Replace("-", ",").Replace(" ", ",");

            #region Colunas Usando Excel Helper

            //var colunas = new List<ExcelColuna<TfaExportaDto>>
            //{
            //    new ExcelColunaInteira<TfaExportaDto>("DESPACHO", c => c.Despacho.HasValue ? c.Despacho.Value : 00),
            //    new ExcelColuna <TfaExportaDto>("SÉRIE_DESPACHO", c => !string.IsNullOrEmpty(c.SerieDespacho) ? c.SerieDespacho : "00"),
            //    new ExcelColunaInteira<TfaExportaDto>("OS", c => c.OS.HasValue ? c.OS.Value : 00),
            //    new ExcelColunaInteira<TfaExportaDto>("NUM PROCESSO", c => c.NumProcesso.HasValue ? c.NumProcesso.Value : 00),
            //    new ExcelColunaInteira<TfaExportaDto>("NUM TERMO", c => !string.IsNullOrEmpty(c.NumTermo) ? Convert.ToInt32(c.NumTermo) : 00),
            //    new ExcelColuna<TfaExportaDto>("STATUS", c => !string.IsNullOrEmpty(c.Status) ? c.Status : string.Empty),
            //    new ExcelColunaInteira<TfaExportaDto>("VAGÃO", c => !string.IsNullOrEmpty(c.Vagao) ? Convert.ToInt32(c.Vagao) : 00),
            //    new ExcelColuna<TfaExportaDto>("SERIE_VAGÃO", c => !string.IsNullOrEmpty(c.SerieVagao) ? c.SerieVagao : string.Empty),
            //    new ExcelColuna<TfaExportaDto>("NF", c => !string.IsNullOrEmpty(c.NF) ? c.NF.ToString(): string.Empty),
            //    new ExcelColuna<TfaExportaDto>("FLUXO", c => !string.IsNullOrEmpty(c.Fluxo) ? c.Fluxo : string.Empty),
            //    new ExcelColuna<TfaExportaDto>("UN", c => !string.IsNullOrEmpty(c.UN) ? c.UN : string.Empty),
            //    new ExcelColuna<TfaExportaDto>("MALHA", c => !string.IsNullOrEmpty(c.Malha) ? c.Malha : string.Empty),
            //    new ExcelColuna<TfaExportaDto>("BITOLA", c => !string.IsNullOrEmpty(c.Bitola) ? c.Bitola : string.Empty),
            //    new ExcelColuna<TfaExportaDto>("EST. ORIGEM", c => !string.IsNullOrEmpty(c.EstacaoOrigem) ? c.EstacaoOrigem : string.Empty),
            //    new ExcelColuna<TfaExportaDto>("EST. DESTINO", c => !string.IsNullOrEmpty(c.EstacaoDestino) ? c.EstacaoDestino : string.Empty),
            //    new ExcelColuna<TfaExportaDto>("CAUSA", c => !string.IsNullOrEmpty(c.Causa) ? c.Causa : string.Empty),
            //    new ExcelColuna<TfaExportaDto>("COD_CLIENTE", c => !string.IsNullOrEmpty(c.CodCliente) ? c.CodCliente.ToString() : string.Empty),
            //    new ExcelColuna<TfaExportaDto>("CLIENTE", c => !string.IsNullOrEmpty(c.Cliente) ? c.Cliente : string.Empty),
            //    new ExcelColunaInteira<TfaExportaDto>("COD_MERCADORIA", c => !string.IsNullOrEmpty(c.CodMercadoria) ? Convert.ToInt32(c.CodMercadoria) : 00),
            //    new ExcelColuna<TfaExportaDto>("MERCADORIA", c => !string.IsNullOrEmpty(c.Mercadoria) ? c.Mercadoria : string.Empty),
            //    new ExcelColunaData <TfaExportaDto>("DATA SINISTRO", c => c.DtSinistro.HasValue ? c.DtSinistro.Value : (DateTime?)null),
            //    new ExcelColunaData <TfaExportaDto>("DATA VISTORIA", c => c.DtVistoria.HasValue ? c.DtVistoria.Value : (DateTime?)null ),
            //    new ExcelColunaData <TfaExportaDto>("DATA PROCESSO", c => c.DtProcesso.HasValue ? c.DtProcesso.Value : (DateTime?)null),
            //    new ExcelColunaData <TfaExportaDto>("DATA PGTO CLIENTE", c => c.DtPgtoCliente.HasValue ? c.DtPgtoCliente.Value : (DateTime?)null),
            //    new ExcelColuna<TfaExportaDto>("UNIDADE", c => !string.IsNullOrEmpty(c.Unidade) ? c.Unidade : string.Empty),
            //    new ExcelColunaInteira<TfaExportaDto>("PERCENTUAL", c => c.Percentual.HasValue ? c.Percentual.Value : 00),
            //    //new ExcelColuna<TfaExportaDto>("PESO ORIGEM", c => c.PesoOrigem.HasValue ? c.PesoOrigem.Value : 00),
            //    new ExcelColuna<TfaExportaDto>("PERDA (TON) VAGÃO", c => c.PerdaTonVagao.HasValue ? c.PerdaTonVagao.Value : 00),
            //    new ExcelColuna<TfaExportaDto>("TOLERÂNCIA", c => c.Tolerancia.HasValue ? c.Tolerancia.Value : 00),
            //    new ExcelColuna<TfaExportaDto>("CONSIDERAR TOLERÂNCIA", c => !string.IsNullOrEmpty(c.ConsiderarTolerancia) ? c.ConsiderarTolerancia : string.Empty),
            //    new ExcelColuna<TfaExportaDto>("PERDA À PAGAR", c => _tfaService.CalcularPerdaPagar(c)),                
            //    new ExcelColuna<TfaExportaDto>("VALOR MERCADORIA UNITÁRIO", c => c.ValorMercadoriaUnitario.HasValue ? c.ValorMercadoriaUnitario.Value : 00),                
            //    new ExcelColuna<TfaExportaDto>("VALOR À PAGAR PRODUTO", c => (_tfaService.CalcularValorPagarProduto(c))),
            //    //new ExcelColuna<TfaExportaDto>("TARIFA DE FRETE", c => c.TarifaFrete.HasValue ? c.TarifaFrete.Value : 00),                
            //    new ExcelColuna<TfaExportaDto>("VALOR À PAGAR DE FRETE", c => (_tfaService.CalcularValorPagarFrete(c))),
            //    new ExcelColuna<TfaExportaDto>("PISCOFINS", c => c.PisCofins.HasValue ? c.PisCofins.Value : 00),
            //    new ExcelColuna<TfaExportaDto>("ICMS", c => c.ICMS.HasValue ? c.ICMS.Value : 00),
            //    new ExcelColuna<TfaExportaDto>("IMPOSTO", c => (_tfaService.CalcularImposto(c))),
            //    new ExcelColuna<TfaExportaDto>("DESCONTO", c => c.Desconto.HasValue ? c.Desconto.Value : 00),
            //    new ExcelColuna<TfaExportaDto>("MOTIVO", c => !string.IsNullOrEmpty(c.Motivo) ? c.Motivo : string.Empty),
            //    new ExcelColuna<TfaExportaDto>("TOTAL VAGÃO", c => (_tfaService.CalcularTotalVagao(c))),
            //    new ExcelColuna<TfaExportaDto>("FRANQUIA", c => c.Franquia.HasValue ? c.Franquia.Value : 00),
            //    new ExcelColuna<TfaExportaDto>("GRUPO DE CAUSA", c => !string.IsNullOrEmpty(c.GrupoCausa) ? c.GrupoCausa : string.Empty),
            //    new ExcelColunaInteira<TfaExportaDto>("MÊS SINISTRO", c => !string.IsNullOrEmpty(c.MesSinistro) ? Convert.ToInt16(c.MesSinistro) : (int?)null),
            //    new ExcelColuna<TfaExportaDto>("SEGURO", c => !string.IsNullOrEmpty(c.Seguro) ? c.Seguro : string.Empty),
            //    new ExcelColuna<TfaExportaDto>("REGULADORA", c =>c.Reguladora.HasValue ? c.Reguladora.Value : 0),
            //    new ExcelColunaData<TfaExportaDto>("DATA RESSARCIMENTO SEGURADORA", c => c.DtRessarcimentoSeguradora.HasValue ? c.DtRessarcimentoSeguradora.Value : (DateTime?)null),
            //    new ExcelColuna<TfaExportaDto>("VALOR RESSARCIMENTO SEGURADORA", c => c.ValorRessarcimentoSeguradora.HasValue ? c.ValorRessarcimentoSeguradora.Value : 00),
            //    new ExcelColunaData<TfaExportaDto>("DATA CRÉDITO SALVADOS REGULADORA", c => c.DtCreditoSalvadosReguladora.HasValue ? c.DtCreditoSalvadosReguladora.Value :(DateTime?)null),
            //    new ExcelColunaDecimal<TfaExportaDto>("VALOR CRÉDITO SALVADOS REGULADORA", c => c.ValorCreditoSalvadosReguladora.HasValue ? c.ValorCreditoSalvadosReguladora.Value : 00),
            //    new ExcelColunaData<TfaExportaDto>("DATA CRÉDITO SALVADOS UNIDADE", c => c.DtCreditoSalvadosUnidade.HasValue ? c.DtCreditoSalvadosUnidade.Value :(DateTime?)null),
            //    new ExcelColuna<TfaExportaDto>("VALOR CRÉDITO SALVADOS UNIDADE", c => c.ValorCreditoSalvadosUnidade.HasValue ? c.ValorCreditoSalvadosUnidade.Value : 00),
            //    new ExcelColunaDecimal<TfaExportaDto>("(TON) SALVADO", c => c.TonSalvado.HasValue ? c.TonSalvado.Value : 00),
            //    new ExcelColunaDecimal <TfaExportaDto>("CUSTO REGULAÇÃO", c => c.CustoRegulacao.HasValue ? c.CustoRegulacao.Value : 00),
            //    new ExcelColunaData<TfaExportaDto>("GAMBITAGEM", c => !string.IsNullOrEmpty(c.Gambitagem) ? c.Gambitagem : string.Empty),
            //    new ExcelColunaData<TfaExportaDto>("DATA ANALISE", c => c.DtAnalise.HasValue ? c.DtAnalise.Value : (DateTime?) null),
            //    new ExcelColuna<TfaExportaDto>("STATUS PAGAMENTO", c => !string.IsNullOrEmpty(c.StatusPagamento) ? c.StatusPagamento : string.Empty),
            //    new ExcelColuna<TfaExportaDto>("TEMPO ANALISE", c => c.TempoAnalise.HasValue ? c.TempoAnalise.Value : 00),
            //    new ExcelColuna<TfaExportaDto>("CLASSE ANALISE", c => _tfaService.MostraClasseAnalise(c)),
            //    new ExcelColuna<TfaExportaDto>("Vistoriador", c => !string.IsNullOrEmpty(c.Vistoriador) ? c.Vistoriador : string.Empty),
            //    new ExcelColuna<TfaExportaDto>("Terminal Origem", c => !string.IsNullOrEmpty(c.TerminalOrigem) ? c.TerminalOrigem : string.Empty),
            //    new ExcelColuna<TfaExportaDto>("Terminal Destino", c => !string.IsNullOrEmpty(c.TerminalDestino) ? c.TerminalDestino : string.Empty),
            //    new ExcelColuna<TfaExportaDto>("TFA Anexado", c => (c.TfaAnexado.ToUpper().Equals("TRUE") ? "SIM" : "NÃO"))
            //};



            //return ExcelHelper.Exportar(
            //    "consulta_tfa.xlsx",
            //    colunas,
            //    resultado,
            //    ws =>
            //    {
            //        ws.Cells.AutoFitColumns();
            //        //ws.Column(16).Width = 25; // Observacões
            //        //ws.Column(83).Width = 40; // Dados faltando
            //    });

            #endregion

            //document.cookie = "numProcesso=" + obj.numProcesso;
            //document.cookie = "numTermo=" + obj.numTermo;  


            #region Consulta
            var resultado = _tfaService.ObterConsultaTFAExportar(
                  dtInicioProcesso,
                  dtFinalProcesso,
                  vagoes,
                  terminal,
                  un,
                  up,
                  malha,
                  provisionado,
                  dtInicioSinistros,
                  dtFinalSinistros,
                  numProcesso,
                  os,
                  situacao,
                  causa,
                  numTermo,
                  sindicancia,
                  anexado);
            #endregion

            string csv = ListToCSV(resultado);
            return File(new System.Text.UnicodeEncoding().GetBytes(csv), "text/csv", "consulta_TFA.csv");

        }

        /// <summary>
        /// Abre arquivo TFA
        /// </summary>
        /// <param name="Id">Id</param>
        /// <returns>Arquivo Anexado TFA</returns>
        [Autorizar(Transacao = "CONSUTFA", Acao = "Pesquisar")]
        public ActionResult AbrirArquivo(string numProcesso)
        {
            var temporaryFolder = Path.Combine(GetTemporaryFolder(), UsuarioAtual.Codigo.ToString());

            FileService fileService = new FileService();
            var temporaryPath = fileService.GetAndCreateDirectory(temporaryFolder);

            fileService.DeleteFilesInDirectory(temporaryPath);
            fileService.DeleteAllFolders(temporaryPath);

            AmazonUploader cloudService = new AmazonUploader();
            var fileName = string.Concat("TFA_", numProcesso, ".pdf");
            var arquivo = cloudService.GetObject(fileName, temporaryFolder, numProcesso);
            if (!string.IsNullOrEmpty(arquivo))
            {
                var stream = System.IO.File.Open(arquivo, FileMode.Open);

                var fileResult = new FileStreamResult(stream, "application/pdf");

                var index = arquivo.LastIndexOf(@"\") + 1;
                fileResult.FileDownloadName = arquivo.Substring(index, (arquivo.Length - index));

                return fileResult;
            }
            else
            {
                var erros = new StringCollection();
                erros.Add("Arquivo " + numProcesso + " não encontrado");
                TempData["Erros"] = erros;

                return RedirectToAction("Tfa");
            }
        }

        /// <summary>
        /// Baixar TFAS
        /// </summary>
        /// <param name="items"></param>
        /// <returns>Baixa arquivo ZIP contendo anexos de TFAS selecionados na grid</returns>
        [Autorizar(Transacao = "CONSUTFA", Acao = "Pesquisar")]
        public ActionResult BaixaZIPArquivosTFA(string items)
        {
            var lstItems = items.Split(',').ToList();

            var temporaryFolder = Path.Combine(GetTemporaryFolder(), UsuarioAtual.Codigo.ToString());

            FileService fileService = new FileService();
            var temporaryPath = fileService.GetAndCreateDirectory(temporaryFolder);

            fileService.DeleteFilesInDirectory(temporaryPath);
            fileService.DeleteAllFolders(temporaryPath);

            AmazonUploader cloudService = new AmazonUploader();

            foreach (var numProcesso in lstItems)
            {
                var fileName = string.Concat("TFA_", numProcesso, ".pdf");
                cloudService.GetObject(fileName, temporaryFolder, numProcesso);
            }

            var files = fileService.GetDirectoryFiles(temporaryFolder, true);
            if (files.Count > 0)
            {
                var zipFile = fileService.ZipFiles(files, temporaryFolder);

                foreach (var file in files)
                {
                    if (file.ToLower().Contains(".pdf"))
                        fileService.DeleteFile(file);
                }

                var stream = System.IO.File.Open(zipFile, FileMode.Open);
                var fileResult = new FileStreamResult(stream, "application/zip");

                var index = zipFile.LastIndexOf(@"\") + 1;
                var fileName = zipFile.Substring(index, (zipFile.Length - index));

                fileResult.FileDownloadName = fileName;

                return fileResult;
            }
            else
            {
                throw new Exception("Sem arquivos para exportar");
            }
        }


        /// <summary>
        ///     Excluir Arquivo TFA
        /// </summary>
        /// <returns>Mensagem de retorno</returns>
        [Autorizar(Transacao = "CONSUTFA", Acao = "Pesquisar")]
        public ActionResult ExcluiArquivoTFA(string numProcesso)
        {
            try
            {
                // Validando se foi carregado por mobile
                var tfa = _tfaService.ObterPorNumProcesso(Convert.ToInt32(numProcesso));
                var origemApp = false;

                if (tfa != null)
                {
                    if (tfa.OrigemProcessoApp)
                    {
                        origemApp = true;
                    }
                }

                if (!origemApp)
                {

                    var TfaAnexoRemover = _tfaAnexoService.ObterPorNumProcesso(numProcesso);
                    if (TfaAnexoRemover != null)
                        _tfaAnexoService.RemoverAnexoTFA(TfaAnexoRemover);

                    AmazonUploader cloudUploader = new AmazonUploader();
                    var fileName = string.Concat("TFA_", numProcesso, ".pdf");
                    var excluido = cloudUploader.DeleteObject(fileName, numProcesso);

                    if (excluido)
                    {
                        return Json(new
                        {
                            sucesso = true,
                            message = "Anexo excluído."
                        });
                    }
                    else
                    {
                        return Json(new
                        {
                            sucesso = false,
                            message = "Erro ao excluir anexo."
                        });
                    }
                }
                else
                {
                    return Json(new
                    {
                        sucesso = false,
                        message = "Não é possível excluir TFA criado pelo App."
                    });
                }

            }
            catch (Exception)
            {
                return Json(new
                {
                    sucesso = false,
                    message = "Ocorreu um erro inesperado ao excluir. Contate o administrador do sistema."
                });
            }
        }

        /// <summary>
        /// Baixar TFAS
        /// </summary>
        /// <param name="items"></param>
        /// <returns>Baixa arquivo ZIP contendo anexos de TFAS selecionados na grid</returns>
        [Autorizar(Transacao = "CONSUTFA", Acao = "Pesquisar")]
        public ActionResult BaixaZIPDossie(string items, int options)
        {
            var lstItems = items.Split(',').ToList();

            var temporaryFolder = Path.Combine(GetTemporaryFolder(), UsuarioAtual.Codigo.ToString());

            FileService fileService = new FileService();
            var temporaryPath = fileService.GetAndCreateDirectory(temporaryFolder);

            fileService.DeleteFilesInDirectory(temporaryPath);
            fileService.DeleteAllFolders(temporaryPath);

            AmazonUploader cloudService = new AmazonUploader();

            var filterExpression = "";

            if ((options & (int)DossieOptions.TFA) == (int)DossieOptions.TFA)
            {
                filterExpression += @"(TFA_\d*\.pdf)";
            }

            if ((options & (int)DossieOptions.CTE) == (int)DossieOptions.CTE)
            {
                filterExpression += string.IsNullOrEmpty(filterExpression) ? "" : "|";
                filterExpression += @"(CTE_\d*\.pdf)";
            }

            if ((options & (int)DossieOptions.NFE) == (int)DossieOptions.NFE)
            {
                filterExpression += string.IsNullOrEmpty(filterExpression) ? "" : "|";
                filterExpression += @"(NFE_\d*_\d*\.pdf)";
            }

            if ((options & (int)DossieOptions.TKT) == (int)DossieOptions.TKT)
            {
                filterExpression += string.IsNullOrEmpty(filterExpression) ? "" : "|";
                filterExpression += @"(TKT_\d*\.pdf)";
            }

            if ((options & (int)DossieOptions.Foto) == (int)DossieOptions.Foto)
            {
                filterExpression += string.IsNullOrEmpty(filterExpression) ? "" : "|";
                filterExpression += @"(.)+\.(jpg)";
                filterExpression += string.IsNullOrEmpty(filterExpression) ? "" : "|";
                filterExpression += @"(.)+\.(png)";
            }


            foreach (var numProcesso in lstItems)
            {
                cloudService.GetObjectsInFolder(temporaryFolder, "processos/" + numProcesso, filterExpression);
            }

            var directories = fileService.GetDirectories(temporaryFolder);
            if (directories.Count > 0)
            {
                var zipFile = fileService.ZipDirectories(directories, temporaryFolder, "Dossie.zip");

                var stream = System.IO.File.Open(zipFile, FileMode.Open);
                var fileResult = new FileStreamResult(stream, "application/zip");

                var index = zipFile.LastIndexOf(@"\") + 1;
                var fileName = zipFile.Substring(index, (zipFile.Length - index));

                fileResult.FileDownloadName = fileName;

                return fileResult;
            }
            else
            {
                return View("ErroDossie");
            }
        }

        /// <summary>
        /// Execução da atualização dos status dos itens recebidos.
        /// </summary>
        /// <param name="processos"></param>
        /// <returns></returns>
        [HttpPost]
        [Autorizar(Transacao = "CONSUTFA", Acao = "Pesquisar")]
        public ActionResult FecharLote(string[] processos, DateTime dtPrepPagamento, string formaPagamento, DateTime dtPagamento, DateTime dtEncerramento, string analiseProcesso)
        {
            try
            {
                _tfaService.FecharLote(processos, analiseProcesso, dtPrepPagamento, formaPagamento, dtPagamento, dtEncerramento, UsuarioAtual);

                return Json(new
                {
                    sucesso = true,
                    message = "Status atualizados"
                });
            }
            catch (Exception ex)
            {

                return Json(new
                {
                    sucesso = false,
                    message = "Erro ao atualizar status: " + ex.Message
                });
            }
        }

        #endregion

        #region JsonResults


        /// <summary>
        /// Retornar UN 
        /// </summary>
        /// <returns>Retorna lista de unidades</returns>
        public JsonResult ObterUN()
        {
            var resultado = _tfaService.ObterUnidadeNegocio();

            var jsonData = Json(
                new
                {
                    Items = resultado.Select(i => new
                    {
                        IdUnidade = i.Codigo,
                        Descricao = i.Codigo
                    }),
                });
            return jsonData;
        }


        /// <summary>
        /// Retornar Causas
        /// </summary>
        /// <returns></returns>
        public JsonResult ObterCausa()
        {
            var resultado = _causaSeguroService.ObterCausasTipoCausaV();

            var jsonData = Json(
                new
                {
                    Items = resultado.Select(i => new
                    {
                        i.IdCausa,
                        i.Descricao
                    }),
                });
            return jsonData;
        }

        public JsonResult ObterSituacao()
        {
            var resultado = new List<SituacaoSeguroDto>(){
                 new SituacaoSeguroDto
                {
                    Id = "P",
                    Descricao = "P-PENDENTE"
                },
                new SituacaoSeguroDto
                {
                    Id = "R",
                    Descricao = "R-PREPARADO P/ PAGAMENTO"
                },
                new SituacaoSeguroDto
                {
                    Id = "O",
                    Descricao = "O-PAGO"
                },
                new SituacaoSeguroDto
                {
                    Id = "E",
                    Descricao = "E-ENCERRADO SEM PAGAMENTO"
                },
                new SituacaoSeguroDto
                {
                    Id = "C",
                    Descricao = "C-ENCONTRO DE CONTAS"
                }
            };

            var jsonData = Json(
                new
                {
                    Items = resultado.Select(i => new
                    {
                        i.Id,
                        i.Descricao
                    }),
                });
            return jsonData;
        }

        public JsonResult ObterProvisionado()
        {
            var resultado = new List<SeguroProvisionadoDto>(){
                new SeguroProvisionadoDto
                {
                    Id = "S",
                    Descricao = "Sim"
                },
                new SeguroProvisionadoDto
                {
                    Id = "N",
                    Descricao = "Não"
                }
            };

            var jsonData = Json(
                new
                {
                    Items = resultado.Select(i => new
                    {
                        i.Id,
                        i.Descricao
                    }),
                });
            return jsonData;
        }

        public JsonResult ObterAnexoTfa()
        {
            var resultado = new List<TfaAnexadoDto>(){
                new TfaAnexadoDto
                {
                    Id = "T",
                    Descricao = "Todos"
                },
                new TfaAnexadoDto
                {
                    Id = "S",
                    Descricao = "Sim"
                },
                new TfaAnexadoDto
                {
                    Id = "N",
                    Descricao = "Não"
                }
            };

            var jsonData = Json(
                new
                {
                    Items = resultado.Select(i => new
                    {
                        i.Id,
                        i.Descricao
                    }),
                });
            return jsonData;
        }

        public JsonResult ObterConsulta(DetalhesPaginacaoWeb detalhesPaginacaoWeb, string dtInicioProcesso, string dtFinalProcesso,
                                        string vagao, string terminal, string un, string up, string malha, string provisionado,
                                        string dtInicioSinistros, string dtFinalSinistros, string numProcesso, string os,
                                        string situacao, string causa, string numTermo, string sindicancia, string anexado)
        {
            var vagoes = vagao.Trim().Replace(";", ",").Replace("-", ",").Replace(" ", ",");
            var resultado = _tfaService.ObterConsulta(detalhesPaginacaoWeb, dtInicioProcesso, dtFinalProcesso, vagoes, terminal, un, up, malha, provisionado, dtInicioSinistros, dtFinalSinistros, numProcesso, os, situacao, causa, numTermo, sindicancia, anexado);
            var jsonData = Json(
                new
                {
                    Items = resultado.Items.Select(i => new
                    {
                        i.NumTermo,
                        i.NumProcesso,
                        DataProcesso = i.DataProcesso.Value.ToString("dd/MM/yyyy"),
                        Situacao = (!string.IsNullOrEmpty(i.Situacao) ? i.Situacao.ToUpper() : string.Empty),
                        i.UnidEnvolvida,
                        i.OrdemServico,
                        DataProvisao = i.DataProvisao.HasValue ? i.DataProvisao.Value.ToString("dd/MM/yyyy") : "",
                        i.TfaAnexo,
                        i.StatusCiente,
                        i.PermiteExcluir
                    }),
                    Total = resultado.Total
                });
            return jsonData;
        }


        #endregion
    }
}
