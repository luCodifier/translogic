using Translogic.Modules.Core.Domain.DataAccess.Repositories.Acesso;

namespace Translogic.Modules.Core.Controllers
{
	using System;
	using System.Globalization;
	using System.Web.Mvc;
	using System.Web.Routing;
	using System.Web.Security;
	using App_GlobalResources;
	using Domain.Model.Acesso;
	using Domain.Services.Acesso;
	using Filters;
	using Helpers;
	using Infrastructure.Services;
	using Translogic.Core.Infrastructure.Web;
	using ViewModel;

	/// <summary>
	/// Controller com m�todos gerais
	/// </summary>
	public class HomeController : BaseSecureModuleController
	{
		#region ATRIBUTOS READONLY & EST�TICOS

		private readonly AutenticacaoWebService _autenticacaoWebService;
		private readonly ControleAcessoService _controleAcessoService;

		#endregion

		#region ATRIBUTOS

		private readonly MenuService _menuService;

		#endregion

		#region CONSTRUTORES
		/// <summary>
		/// Inicializa uma nova inst�ncia da classe
		/// </summary>
		/// <param name="menuService">Servi�o de menu</param>
		/// <param name="controleAcessoService">Servi�o de controle de acesso</param>
		/// <param name="autenticacaoWebService">Servico de autentica��o web</param>
		public HomeController(
								MenuService menuService, 
								ControleAcessoService controleAcessoService,
								AutenticacaoWebService autenticacaoWebService)
		{
			_menuService = menuService;
			_autenticacaoWebService = autenticacaoWebService;
			_controleAcessoService = controleAcessoService;
		}

		#endregion

		#region M�TODOS
		/// <summary>
		/// A��o default
		/// </summary>
		/// <param name="codigoTela">N�mero da tela</param>
		/// <returns>View da tela principal do translogic</returns>
		/// [GarantirSufixoServidorFilter(Order = 0)]
		public virtual ActionResult Index(int? codigoTela)
		{
			ViewData["menuPrincipalJSON"] = JsonSerializer.Encode(_menuService.ObterMenuPrincipal(UsuarioAtual.Codigo));
			ViewData["meuTranslogicJSON"] = JsonSerializer.Encode(_menuService.ObterMeuTranslogic(UsuarioAtual));

			ViewData["token"] = Request.Cookies[FormsAuthentication.FormsCookieName].Value;

			if (codigoTela.HasValue)
			{
				Menu tela = _menuService.ObterPorCodigoTela(codigoTela.Value, UsuarioAtual.Codigo);

				if (tela != null)
				{
					ViewData["tela_json"] = JsonSerializer.Encode(_menuService.ToSimples(tela));
				}
			}

			return View();
		}

		/// <summary>
		/// Altera o idioma do acesso corrente
		/// </summary>
		/// <param name="cultura">Para qual cultura ser� alterado</param>
		/// <param name="returnUrl">Qual a URL de retorno</param>
		/// <returns>Redirect para a URL de retorno</returns>
		public ActionResult AlterarCultura(string cultura, string returnUrl)
		{
			try
			{
				CultureInfo culture = new CultureInfo(cultura);

				_controleAcessoService.AlterarIdiomaTokenAcesso(UsuarioAtual, culture);

				_autenticacaoWebService.CriarCookieAutenticacao(UsuarioAtual.Codigo, culture.Name);
			}
			catch (Exception)
			{
			}

			if (!string.IsNullOrEmpty(returnUrl))
			{
				return Redirect(returnUrl);
			}

			return Redirect(Url.Root());
		}

        /// <summary>
        /// Altera o tema do acesso corrente
        /// </summary>
        /// <param name="tema">Para qual tema ser� alterado</param>
        /// <param name="returnUrl">Qual a URL de retorno</param>
        /// <returns>Redirect para a URL de retorno</returns>
        public ActionResult AlterarTema(string tema, string returnUrl)
        {
            try
            {
                // Tema temaSelecionado = new Tema(tema);
				Session["TEMA_TELA"] = tema;
	            _controleAcessoService.GravarTemaUsuario(UsuarioAtual, tema);
            }
            catch (Exception)
            {
            }

            if (!string.IsNullOrEmpty(returnUrl))
            {
                return Redirect(returnUrl);
            }

            return Redirect(Url.Root());
        }

		/// <summary>
		/// Pesquisa a tela pelo c�digo
		/// </summary>
		/// <param name="codigoTela">N�mero da tela</param>
		/// <returns>view da tela caso exista</returns>
		/// [GarantirSufixoServidorFilter]
		public ActionResult ExibirTela(int? codigoTela)
		{
			if (!codigoTela.HasValue)
			{
				return new EmptyResult();
			}

			Menu tela = _menuService.ObterPorCodigoTela(codigoTela.Value, UsuarioAtual.Codigo);

			if (tela == null)
			{
				return View("index");
			}

			if (tela.Legado)
			{
				return Redirect(Url.Legado(tela.Path, Request.Cookies[FormsAuthentication.FormsCookieName].Value));
			}

			return Redirect(tela.Path);
		}

		/// <summary>
		/// Tela para alterar a senha quando estiver expirada
		/// </summary>
		/// <returns>View de altera��o de senha</returns>
		public ActionResult AlterarSenhaExpirada()
		{
			float diasExpiracao = _controleAcessoService.ObterDiasExpiraSenha();
			ViewData["quantidadeDias"] = diasExpiracao.ToString();
			return View();
		}

		/// <summary>
		/// Executa a altera��o de senha
		/// </summary>
		/// <param name="info">
		/// Dados de altera��o de senha
		/// </param>
		/// <returns>
		/// Retorna para a tela de altera��o ou quando alterado com sucesso para a tela inicial
		/// </returns>
		[AcceptVerbs(HttpVerbs.Post)]
		public ActionResult AlterarSenhaExpirada(AlterarSenhaInfo info)
		{
			const string MODEL_PREFIX = "info";

			if (!Validate(info, MODEL_PREFIX))
			{
				return View("AlterarSenhaExpirada", info);
			}

			AlterarSenhaResultado result = _controleAcessoService.AlterarSenha(UsuarioAtual, info.Senha, info.SenhaNova, info.SenhaNovaConfirmacao);
			if (result != AlterarSenhaResultado.Sucesso)
			{
				switch (result)
				{
					case AlterarSenhaResultado.SenhaNovaNaoConfereConfirmacao:
						ModelState.AddModelError(MODEL_PREFIX + ".Senha", Acesso.SenhaNovaNaoConfereConfirmacao);
						break;
					case AlterarSenhaResultado.SenhaIncorreta:
						ModelState.AddModelError(MODEL_PREFIX + ".Senha", Acesso.SenhaIncorreta);
						break;
					case AlterarSenhaResultado.SenhaNovaIgualAtual:
						ModelState.AddModelError(MODEL_PREFIX + ".Senha", Acesso.SenhaNovaIgualAtual);
						break;
					case AlterarSenhaResultado.SenhaNovaIgualUsuario:
						ModelState.AddModelError(MODEL_PREFIX + ".Senha", Acesso.SenhaNovaIgualUsuario);
						break;
					case AlterarSenhaResultado.SenhaConterLetrasNumeros:
						ModelState.AddModelError(MODEL_PREFIX + ".Senha", Acesso.SenhaConterLetrasNumeros);
						break;
                    case AlterarSenhaResultado.SenhaHistorica51:
                        ModelState.AddModelError(MODEL_PREFIX + ".Senha",Acesso.SenhaHistorica51);
						break;

				}
			}

			if (result == AlterarSenhaResultado.Sucesso)
			{
				string returnUrl = Request["returnUrl"];

				if (!string.IsNullOrEmpty(returnUrl))
				{
					Response.Redirect(returnUrl, true);
				}
				else
				{
					Response.Redirect("~/home.all");
				}

				return new EmptyResult();
			}

			return View("AlterarSenhaExpirada", info);
		}

		/// <summary>
		/// Envia a mensagem para o edinho
		/// </summary>
		/// <param name="assunto">Assunto da mensagem</param>
		/// <param name="mensagem">Mensagem a ser enviada</param>
		public void EnviarMensagemEdinho(string assunto, string mensagem)
		{
			_menuService.EnviarMensagemEdinho(assunto, mensagem, UsuarioAtual, ControllerContext);
		}

		/// <summary>
		/// Chamado para mostrar o iframe do legado, para fazer a bridge entre o novo e o legado
		/// </summary>
		/// <returns> P�gina do proxy do legado </returns>
		/// <remarks>Este m�todo � utilizado apenas para gerar o arquivo ProxyLegado.html que est� no projeto Translogic.Web</remarks>
		public ActionResult IndexLegado()
		{
			return View("ProxyLegado");
		}

		/// <summary>
		/// Obt�m os segundos de expira��o configurados no Forms Autentication
		/// </summary>
		/// <returns>Objeto JSON</returns>
		public JsonResult ObterSegundosExpiracao()
		{
			double minutos = 40; // _autenticacaoWebService.ObterTimeout();
			return Json(new
			            	{
			            		segundosExpiracao = minutos * 60
							});
		}

		/// <summary>
		/// Sobreescreve o filtro de execu��o de uma action
		/// </summary>
		/// <param name="filterContext">Contexto do filtro de a��o - <see cref="ActionExecutingContext"/></param>
		protected override void OnActionExecuting(ActionExecutingContext filterContext)
		{
			base.OnActionExecuting(filterContext);

			if (filterContext.HttpContext.User.Identity.IsAuthenticated)
			{
				if (filterContext.ActionDescriptor.ActionName.ToUpper() != "alterarSenhaExpirada".ToUpper())
				{
					bool expirou = _controleAcessoService.VerificarSenhaExpirada(UsuarioAtual);
                    bool expirou7dias = _controleAcessoService.RecuperarUltimoAcesso(UsuarioAtual);
					if (expirou || expirou7dias)
					{
						filterContext.Result = Redirect(Url.Action("alterarSenhaExpirada", "home", new RouteValueDictionary { { "returnUrl", Request.Path } }));
					}
                    
				}
			}
		}

		#endregion
	}
}
