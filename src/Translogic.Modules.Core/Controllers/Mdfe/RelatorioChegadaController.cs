﻿    namespace Translogic.Modules.Core.Controllers.Mdfe
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;
    using ALL.Core.Dominio;
    using Domain.Model.Trem;
    using Translogic.Core.Infrastructure;
    using Translogic.Core.Infrastructure.Web;
    using Translogic.Core.Infrastructure.Web.Helpers;
    using Translogic.Modules.Core.Domain.Model.Diversos.Cte;
    using Translogic.Modules.Core.Domain.Model.Diversos.Mdfe;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.TriangulusConfig;
    using Translogic.Modules.Core.Domain.Services.Mdfes;
    using Translogic.Modules.Core.Infrastructure;

    /// <summary>
    /// Controller de relatorio de chegada
    /// </summary>
    public class RelatorioChegadaController : BaseSecureModuleController
    {
        private readonly MdfeService _mdfeService;

        /// <summary>
        /// Construtor padrao
        /// </summary>
        /// <param name="mdfeService">Service da MDFe</param>
        public RelatorioChegadaController(MdfeService mdfeService)
        {
            _mdfeService = mdfeService;
        }

        /// <summary>
        /// Ação do controller
        /// </summary>
        /// <returns>Action Result</returns>
        [Autorizar(Transacao = "MDFERELCHEGADA")]
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Pesquisa das MDFes
        /// </summary>
        /// <param name="pagination">detalhes da paginacao</param>
        /// <param name="filter">informações dos filtros</param>
        /// <returns>resultado json</returns>
        [Autorizar(Transacao = "MDFERELCHEGADA", Acao = "Pesquisar")]
        public ActionResult Pesquisar(DetalhesPaginacaoWeb pagination, DetalhesFiltro<object>[] filter)
        {
            var dataInicial = filter
                .Where(e => e.Campo == "dataInicial" && !string.IsNullOrWhiteSpace(e.Valor[0].ToString()))
                .Select(e => e == null ? (DateTime?)null : DateTime.ParseExact(e.Valor[0].ToString(), "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture))
                .FirstOrDefault();

            var dataFinal = filter
                .Where(e => e.Campo == "dataFinal" && !string.IsNullOrWhiteSpace(e.Valor[0].ToString()))
                .Select(e => e == null ? (DateTime?)null : DateTime.ParseExact(e.Valor[0].ToString(), "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture).AddDays(1).AddSeconds(-1))
                .FirstOrDefault();

            var chave = filter
                .Where(e => e.Campo == "chave" && !string.IsNullOrWhiteSpace(e.Valor[0].ToString()))
                .Select(e => e == null ? (string)null : e.Valor[0].ToString().ToUpperInvariant())
                .FirstOrDefault();

            var numero = filter
                .Where(e => e.Campo == "numero" && !string.IsNullOrWhiteSpace(e.Valor[0].ToString()))
                .Select(e => e == null ? (string)null : e.Valor[0].ToString().ToUpperInvariant())
                .FirstOrDefault();

            var serie = filter
                .Where(e => e.Campo == "serie" && !string.IsNullOrWhiteSpace(e.Valor[0].ToString()))
                .Select(e => e == null ? (string)null : e.Valor[0].ToString().ToUpperInvariant())
                .FirstOrDefault();

            var prefixo = filter
                .Where(e => e.Campo == "prefixo" && !string.IsNullOrWhiteSpace(e.Valor[0].ToString()))
                .Select(e => e == null ? (string)null : e.Valor[0].ToString().ToUpperInvariant())
                .FirstOrDefault();

            var os = filter
                .Where(e => e.Campo == "os" && !string.IsNullOrWhiteSpace(e.Valor[0].ToString()))
                .Select(e => e == null ? (int?)null : int.Parse(e.Valor[0].ToString(), CultureInfo.InvariantCulture))
                .FirstOrDefault();

            var origem = filter
                .Where(e => e.Campo == "origem" && !string.IsNullOrWhiteSpace(e.Valor[0].ToString()))
                .Select(e => e == null ? (string)null : e.Valor[0].ToString().ToUpperInvariant())
                .FirstOrDefault();

            var destino = filter
                .Where(e => e.Campo == "destino" && !string.IsNullOrWhiteSpace(e.Valor[0].ToString()))
                .Select(e => e == null ? (string)null : e.Valor[0].ToString().ToUpperInvariant())
                .FirstOrDefault();

            var idTrem = filter
                .Where(e => e.Campo == "idTrem" && !string.IsNullOrWhiteSpace(e.Valor[0].ToString()))
                .Select(e => e == null ? 0 : int.Parse(e.Valor[0].ToString(), CultureInfo.InvariantCulture))
                .FirstOrDefault();

            var situacao = ""; // Não passado

            var result = _mdfeService.Listar(pagination, chave, numero, serie, prefixo, os, dataInicial, dataFinal, origem, destino, situacao, idTrem);

            return Json(new
            {
                result.Total,
                Items = result.Items.Select(e => new
                {
                    e.Id,
                    e.Prefixo,
                    e.Origem,
                    e.Destino,
                    e.OS,
                    SituacaoAtualEnum = ALL.Core.Util.Enum<SituacaoMdfeEnum>.GetDescriptionOf(e.SituacaoAtual),
                    e.UltimoEvento
                })
            });
        }

        /// <summary>
        /// Ação do controller detalhes
        /// </summary>
        /// <param name="idTrem">id do trem</param>
        /// <returns>Action Result</returns>
        [Autorizar(Transacao = "MDFERELCHEGADA", Acao = "Pesquisar")]
        public ActionResult DetalhesFluxo(int idTrem)
        {
            IList<FluxosMdfeDto> result = _mdfeService.ListarFluxosComposicao(idTrem);

            return Json(new
            {
                Items = result.Select(e => new
                {
                    e.IdMdfe,
                    e.EmpresaRecebedora,
                    e.IdEmpresa,
                    e.CnpjRecebedora
                    /*   e.IdFluxo,
                    e.Mercadoria,
                    e.CodigoMercadoria,
                    e.Origem,
                    e.Destino,
                    e.EmpresaDestino,
                    e.EmpresaOrigem */
                })
            });
        }

        /// <summary>
        /// Ação do controller detalhes
        /// </summary>
        /// <param name="pagination">Informações de paginação</param>
        /// <param name="filter">Informações para filtro</param>
        /// <returns>Action Result</returns>
        [Autorizar(Transacao = "MDFERELCHEGADA", Acao = "Pesquisar")]
        public ActionResult DetalhesFluxoPorPatio(DetalhesPaginacaoWeb pagination, DetalhesFiltro<object>[] filter)
        {
            var dataInicial = filter
                .Where(e => e.Campo == "dataInicial" && !string.IsNullOrWhiteSpace(e.Valor[0].ToString()))
                .Select(e => e == null ? (DateTime?)null : DateTime.ParseExact(e.Valor[0].ToString(), "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture))
                .FirstOrDefault();

            var dataFinal = filter
                .Where(e => e.Campo == "dataFinal" && !string.IsNullOrWhiteSpace(e.Valor[0].ToString()))
                .Select(e => e == null ? (DateTime?)null : DateTime.ParseExact(e.Valor[0].ToString(), "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture).AddDays(1).AddSeconds(-1))
                .FirstOrDefault();

            var estacao = filter
                .Where(e => e.Campo == "estacao" && !string.IsNullOrWhiteSpace(e.Valor[0].ToString()))
                .Select(e => e == null ? (string)null : e.Valor[0].ToString().ToUpperInvariant())
                .FirstOrDefault();

            var tmpIdLinha = filter
                .Where(e => e.Campo == "linha" && !string.IsNullOrWhiteSpace(e.Valor[0].ToString()))
                .Select(e => e == null ? (string)null : e.Valor[0].ToString().ToUpperInvariant())
                .FirstOrDefault();
            int tmp;
            int? idLinha = int.TryParse(tmpIdLinha, out tmp) ? (int?)tmp : null;
            IList<FluxosMdfeDto> result = _mdfeService.ListarFluxosPorPatio(dataInicial.Value, dataFinal.Value, estacao, idLinha);

            return Json(new
            {
                Items = result.Select(e => new
                {
                    e.IdMdfe,
                    e.EmpresaRecebedora,
                    e.IdEmpresa,
                    e.CnpjRecebedora
                })
            });
        }

        /// <summary>
        /// Ação do controller obter linhas
        /// </summary>
        /// <param name="codAreaOperacional">código da estação</param>
        /// <returns>Action Result</returns>
        [Autorizar(Transacao = "MDFERELCHEGADA", Acao = "Pesquisar")]
        public ActionResult ObterLinhas(string codAreaOperacional)
        {
            if (String.IsNullOrWhiteSpace(codAreaOperacional))
            {
                throw new TranslogicException("Área Operacional inválida.");
            }

            var linhas = _mdfeService.ListarLinhasEstacao(codAreaOperacional);

            return Json(new
            {
                totalCount = linhas.Count,
                Items = linhas.Select(g => new
                {
                    g.Id,
                    g.Codigo
                })
            });
        }

        /// <summary>
        /// Ação do controller detalhes
        /// </summary>
        /// <param name="idTrem">id do Trem</param>
        /// <param name="idEmpresa">id da empresa recebedora</param>
        /// <returns>Action Result</returns>
        public ActionResult DetalhesComposicao(int idTrem, int idEmpresa)
        {
            IList<MdfeComposicaoDto> result = _mdfeService.DetalhesUltComposicaoTrem(idTrem, idEmpresa, null);

            return Json(new
            {
                Total = result.Count,
                Items = result.Select(e => new
                {
                    e.Id,
                    e.Sequencia,
                    e.IdVagao,
                    e.CodigoVagao,
                    SerieCte = string.IsNullOrEmpty(e.SerieCte) ? string.Empty : e.SerieCte,
                    NumeroCte = string.IsNullOrEmpty(e.NumeroCte) ? string.Empty : e.NumeroCte,
                    SituacaoCte = string.IsNullOrEmpty(e.SituacaoCte) ? string.Empty : e.SituacaoCte,
                    NotasCte = e.Notas.Count > 0 ? e.Notas.Select(a => string.Concat(a.SerieNota, "-", a.NumeroNota)).Aggregate((current, next) => current + ", " + next) : string.Empty
                })
            });
        }

        /// <summary>
        /// Ação do controller detalhes
        /// </summary>
        /// <param name="idMdfe">id do mdfe</param>
        /// <param name="idTrem">id do trem</param>
        /// <param name="idEmpresa">id da empresa</param>
        /// <returns>Action Result</returns>
        [Autorizar(Transacao = "MDFERELCHEGADA", Acao = "Pesquisar")]
        public ActionResult DetalhesVagao(int idMdfe, int idTrem, int idEmpresa)
        {
            var trem = _mdfeService.ObterDadosTrem(idTrem);
            ViewData["IdTrem"] = idTrem;
            ViewData["IdMdfe"] = idMdfe;
            ViewData["Trem"] = trem;
            ViewData["EMPRESA"] = idEmpresa;
            return View();
        }

        /// <summary>
        /// Ação do controller detalhes
        /// </summary>
        /// <param name="idEmpresa">id da empresa</param>
        /// <param name="estacao">Código da estacao</param>
        /// <param name="idLinha">id da Linha</param>
        /// <param name="dataInicial">data inicial do período</param>
        /// <param name="dataFinal">data final do período</param>
        /// <returns>Action Result</returns>
        [Autorizar(Transacao = "MDFERELCHEGADA", Acao = "Pesquisar")]
        public ActionResult DetalhesVagaoPorPatio(int idEmpresa, string estacao, int? idLinha, string dataInicial, string dataFinal)
        {
            var dataInicialDT = DateTime.ParseExact(dataInicial, "dd-MM-yyyy-HH-mm-ss", CultureInfo.CurrentUICulture);
            var dataFinalDT = DateTime.ParseExact(dataFinal, "dd-MM-yyyy-HH-mm-ss", CultureInfo.CurrentUICulture);
            estacao = estacao.ToUpperInvariant();

            ViewData["Estacao"] = estacao;
            ViewData["IdLinha"] = idLinha;

            if (idLinha.HasValue)
            {
                ViewData["Linha"] = _mdfeService.ObterLinhaEstacao(idLinha.Value);
            }

            var result = _mdfeService.ListarDetalhesDaComposicaoPorPatio(idEmpresa, dataInicialDT, dataFinalDT, estacao, idLinha);

            ViewData["IdEmpresa"] = idEmpresa;
            ViewData["Empresa"] = _mdfeService.ObterDadosEmpresa(idEmpresa).DescricaoResumida;
            ViewData["Itens"] = result.Select(e => new
                {
                    e.Id,
                    e.Sequencia,
                    e.IdVagao,
                    e.CodigoVagao,
                    SerieCte = string.IsNullOrEmpty(e.SerieCte) ? string.Empty : e.SerieCte,
                    NumeroCte = string.IsNullOrEmpty(e.NumeroCte) ? string.Empty : e.NumeroCte,
                    SituacaoCte = string.IsNullOrEmpty(e.SituacaoCte) ? string.Empty : e.SituacaoCte,
                    NotasCte = e.Notas.Count > 0 ? e.Notas.Select(a => string.Concat(a.SerieNota, "-", a.NumeroNota)).Aggregate((current, next) => current + ", " + next) : string.Empty
                });
            ViewData["DataInicial"] = dataInicialDT.ToString("dd/MM/yyyy");
            ViewData["DataFinal"] = dataFinalDT.ToString("dd/MM/yyyy");
            return View();
        }

        /// <summary>
        /// Exporta os dados para excel
        /// </summary>
        /// <param name="idEmpresa">id da empresa</param>
        /// <param name="estacao">Código da estacao</param>
        /// <param name="idLinha">id da Linha</param>
        /// <param name="dataInicial">data inicial do período</param>
        /// <param name="dataFinal">data final do período</param>
        /// <param name="idsVagao">Id dos vagões</param>
        /// <returns>Retorna o excel</returns>
        [Autorizar(Transacao = "MDFERELCHEGADA", Acao = "Salvar")]
        public ActionResult ExportarRelatorioEntregaPorPatio(int idEmpresa, string estacao, int? idLinha, string dataInicial, string dataFinal, string idsVagao)
        {
            var dataInicialDT = DateTime.ParseExact(dataInicial, "dd-MM-yyyy-HH-mm-ss", CultureInfo.CurrentUICulture);
            var dataFinalDT = DateTime.ParseExact(dataFinal, "dd-MM-yyyy-HH-mm-ss", CultureInfo.CurrentUICulture);
            estacao = estacao.ToUpperInvariant();

            var arrIds = idsVagao.Split(',');
            List<int> vagoes = arrIds.Select(int.Parse).ToList();
            var mdfeComposicao = _mdfeService.ListarDetalhesDaComposicaoPorPatio(idEmpresa, dataInicialDT, dataFinalDT, estacao, idLinha)
                .Where(a => vagoes.Contains(Convert.ToInt32(a.IdVagao)))
                .ToList();
            IList<CteEmpresas> recebedores = _mdfeService.ObterRecebedores(mdfeComposicao);

            ViewData["MDFE_COMPOSICAO"] = mdfeComposicao;
            ViewData["RECEBEDORES"] = recebedores;

            var viewString = View("RelatorioChegadaFluxo").Capture(ControllerContext);

            foreach (var idVagao in vagoes)
            {
                _mdfeService.GravarLogImpressao(null, idVagao, idEmpresa, UsuarioAtual);
            }

            return _mdfeService.PdfResult(
                this,
                string.Empty,
                string.Format("mdfe_{0:yyyy_MM_dd}.pdf", DateTime.Today),
                viewString);
        }

        /// <summary>
        /// Exporta os dados para excel
        /// </summary>
        /// <param name="idTrem">Id do Trem</param>
        /// <param name="idEmpresa">Id da empresa</param>
        /// <param name="idsVagao">Id dos vagões</param>
        /// <returns>Retorna o excel</returns>
        [Autorizar(Transacao = "MDFERELCHEGADA", Acao = "Salvar")]
        public ActionResult ExportarRelatorioEntrega(int idTrem, int idEmpresa, string idsVagao)
        {
            var arrIds = idsVagao.Split(',');
            List<int> vagoes = arrIds.Select(int.Parse).ToList();
            Trem trem = _mdfeService.ObterDadosTrem(idTrem);
            IList<MdfeComposicaoDto> mdfeComposicao = _mdfeService.DetalhesUltComposicaoTrem(idTrem, idEmpresa, vagoes);
            IList<CteEmpresas> recebedores = _mdfeService.ObterRecebedores(mdfeComposicao);

            ViewData["TREM"] = trem;
            ViewData["MDFE_COMPOSICAO"] = mdfeComposicao;
            ViewData["RECEBEDORES"] = recebedores;

            var viewString = View("RelatorioChegadaFluxo").Capture(ControllerContext);

            foreach (var idVagao in vagoes)
            {
                _mdfeService.GravarLogImpressao(idTrem, idVagao, idEmpresa, UsuarioAtual);
            }

            return _mdfeService.PdfResult(
                this,
                string.Empty,
                string.Format("mdfe_trem_{0}_{1}.pdf", trem.Prefixo, trem.OrdemServico.Numero),
                viewString);
        }

        /// <summary>
        /// Ação do controller detalhes
        /// </summary>
        /// <param name="idTrem">id do trem</param>
        /// <returns>Action Result</returns>
        [Autorizar(Transacao = "MDFERELCHEGADA", Acao = "Pesquisar")]
        public ActionResult FluxoTransportados(int idTrem)
        {
            var trem = _mdfeService.ObterDadosTrem(idTrem);
            ViewData["IdTrem"] = idTrem;
            ViewData["Trem"] = trem;
            return View();
        }
    }
}
