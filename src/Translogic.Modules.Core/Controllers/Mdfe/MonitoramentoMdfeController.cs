﻿namespace Translogic.Modules.Core.Controllers.Mdfe
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Web.Mvc;

    using ALL.Core.Dominio;
    using ALL.Core.Util;

    using Microsoft.Practices.ServiceLocation;

    using Translogic.Core.Infrastructure.Web;
    using Translogic.Core.Infrastructure.Web.Helpers; 
    using Translogic.Modules.Core.Domain.Model.Diversos.Mdfe;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.TriangulusConfig;
    using Translogic.Modules.Core.Domain.Services.Acesso;
    using Translogic.Modules.Core.Domain.Services.Mdfes;
    using Translogic.Modules.Core.Infrastructure;
    using Translogic.Modules.TPCCO.Helpers;

    using Translogic.Modules.Core.Spd.Data;

    /// <summary>
    ///     Controller do monitoramento da MDFe
    /// </summary>
    public class MonitoramentoMdfeController : BaseSecureModuleController
    {
        #region Fields

        private readonly MdfeService _mdfeService;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///     Construtor padrao
        /// </summary>
        /// <param name="mdfeService">Service da MDFe</param>
        public MonitoramentoMdfeController(MdfeService mdfeService)
        {
            this._mdfeService = mdfeService;
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     Ação do controller detalhes
        /// </summary>
        /// <param name="idMdfe">id da Mdfe</param>
        /// <returns>Action Result</returns>
        [Autorizar(Transacao = "MDFEMONITORAMENTO", Acao = "Cancelar")]
        public ActionResult Cancelar(int idMdfe)
        {
            bool status = this._mdfeService.CancelarMdfe(idMdfe, this.UsuarioAtual);

            if (status)
            {
                return this.Json(new { success = true, Message = "SUCESSO" });
            }

            return this.Json(new { success = false, Message = "FALHA" });
        }

        /// <summary>
        ///     Ação do controller detalhes
        /// </summary>
        /// <param name="idTrem">id do trem</param>
        /// <returns>Action Result</returns>
        [Autorizar(Transacao = "MDFEMONITORAMENTO", Acao = "Pesquisar")]
        public ActionResult Detalhes(int idTrem)
        {
            this.ViewData["IdTrem"] = idTrem;
            return this.View();
        }

        /// <summary>
        ///     Ação do controller detalhes
        /// </summary>
        /// <param name="idMdfe">id da Mdfe</param>
        /// <returns>Action Result</returns>
        [Autorizar(Transacao = "MDFEMONITORAMENTO", Acao = "Pesquisar")]
        public ActionResult DetalhesComposicao(int idMdfe)
        {
            IList<MdfeComposicaoDto> result = this._mdfeService.ListarDetalhesDaComposicaoAtualizada(idMdfe);

            return
                this.Json(
                    new
                        {
                            Total = result.Count,
                            Items =
                                result.Select(
                                    e =>
                                    new
                                        {
                                            e.Id,
                                            e.Sequencia,
                                            e.CodigoVagao,
                                            SerieCte = string.IsNullOrEmpty(e.SerieCte) ? string.Empty : e.SerieCte,
                                            NumeroCte = string.IsNullOrEmpty(e.NumeroCte) ? string.Empty : e.NumeroCte,
                                            SituacaoCte = string.IsNullOrEmpty(e.SituacaoCte) ? string.Empty : e.SituacaoCte,
                                            NotasCte =
                                        e.Notas.Count > 0
                                            ? e.Notas.Select(a => string.Concat(a.SerieNota, "-", a.NumeroNota))
                                                  .Aggregate((current, next) => current + ", " + next)
                                            : string.Empty
                                        })
                        });
        }

        /// <summary>
        ///     Ação do controller detalhes
        /// </summary>
        /// <param name="idTrem">id do trem</param>
        /// <returns>Action Result</returns>
        [Autorizar(Transacao = "MDFEMONITORAMENTO", Acao = "Pesquisar")]
        public ActionResult DetalhesMdfe(int idTrem)
        {
            IList<MdfeDto> result = this._mdfeService.ListarDetalhesDaMdfe(idTrem);

            return
                this.Json(
                    new
                        {
                            items =
                                result.Select(
                                    e =>
                                    new
                                        {
                                            e.Id,
                                            e.Chave,
                                            e.Serie,
                                            e.Numero,
                                            SituacaoAtual = this.TraduzirEnumerador(e.SituacaoAtual),
                                            SituacaoAtualEnum = Enum<SituacaoMdfeEnum>.GetDescriptionOf(e.SituacaoAtual),
                                            e.PdfGerado,
                                            DataHora = e.DataHora.ToString(),
                                            e.IndAnulacao
                                        })
                        });
        }

        /// <summary>
        ///     Ação do controller detalhes
        /// </summary>
        /// <param name="idMdfe">id da Mdfe</param>
        /// <returns>Action Result</returns>
        [Autorizar(Transacao = "MDFEMONITORAMENTO", Acao = "Encerrar")]
        public ActionResult Encerrar(int idMdfe)
        {
            bool status = this._mdfeService.EncerrarMdfe(idMdfe, this.UsuarioAtual);

            if (status)
            {
                return this.Json(new { success = true, Message = "SUCESSO" });
            }

            return this.Json(new { success = false, Message = "FALHA" });
        }

        /// <summary>
        ///     Ação do controller detalhes
        /// </summary>
        /// <param name="idMdfe">id da Mdfe</param>
        /// <returns>Action Result</returns>
        [Autorizar(Transacao = "MDFEMONITORAMENTO", Acao = "EnviarAprovacao")]
        public ActionResult EnviarAprovacao(int idMdfe)
        {
            try
            {
                bool status = this._mdfeService.EnviarAprovacao(idMdfe, this.UsuarioAtual);

                if (status)
                {
                    return this.Json(new { success = true, Message = "Mdfe enviado para aprovação com sucesso" });
                }
            }
            catch (Exception ex)
            {
                return this.Json(new { success = false, ex.Message });
            }

            return this.Json(new { success = false, Message = "Ocorreu um erro ao processar a solicitação." });
        }

        /// <summary>
        ///     Exporta os dados para excel
        /// </summary>
        /// <param name="idMdfe">Id do mdfe</param>
        /// <returns>Retorna o excel</returns>
        [Autorizar(Transacao = "MDFEMONITORAMENTO", Acao = "Salvar")]
        public ActionResult Exportar(int idMdfe)
        {
            var mdfe = this._mdfeService.ListarDetalhesDaComposicao(idMdfe);
            IList<MdfeComposicaoDto> mdfeComposicao = this._mdfeService.ListarDetalhesDaComposicaoAtualizada(idMdfe);
            Nfe03Filial filial = this._mdfeService.ObterDadosFerrovia(mdfe.Id.Value);

            this.ViewData["MDFE"] = mdfe;
            this.ViewData["MDFE_COMPOSICAO"] = mdfeComposicao;
            this.ViewData["FILIAL"] = filial;
            this.ViewData["LOGO"] = string.Concat(new TranslogicImageProvider().GetImageRootPath(), "/", "all_logo.png");

            string viewString = this.View("RelatorioMdfe").Capture(this.ControllerContext);

            return this._mdfeService.PdfResult(
                this,
                string.Empty,
                string.Format("mdfe_{0}.pdf", mdfe.Chave),
                viewString);
        }

        /// <summary>
        ///     Exporta os dados para excel
        /// </summary>
        /// <param name="idMdfe">Id do mdfe</param>
        /// <returns>Retorna o excel</returns>
        [Autorizar(Transacao = "MDFEMONITORAMENTO", Acao = "Salvar")]
        public ActionResult ExportarRelatorioEntrega(int idMdfe)
        {
            var mdfe = this._mdfeService.ListarDetalhesDaComposicao(idMdfe);
            IList<MdfeComposicaoDto> mdfeComposicao = this._mdfeService.ListarDetalhesDaComposicaoAtualizada(idMdfe);
            Nfe03Filial filial = this._mdfeService.ObterDadosFerrovia(idMdfe);
            IList<CteEmpresas> recebedores = this._mdfeService.ObterRecebedores(mdfeComposicao);
            this.ViewData["MDFE"] = mdfe;
            this.ViewData["MDFE_COMPOSICAO"] = mdfeComposicao;
            this.ViewData["FILIAL"] = filial;
            this.ViewData["RECEBEDORES"] = recebedores;
            this.ViewData["LOGO"] = string.Concat(new TranslogicImageProvider().GetImageRootPath(), "/", "all_logo.png");

            // viewString = View("RelatorioMdfe").Capture(ControllerContext);
            string viewString = this.View("RelatorioMdfeDetalhado").Capture(this.ControllerContext);

            return this._mdfeService.PdfResult(
                this,
                string.Empty,
                string.Format("mdfe_{0}.pdf", mdfe.Chave),
                viewString);
        }

        /// <summary>
        ///     Gerar MDFes forçadamente pela tela tela 1253
        /// </summary>
        /// <param name="os">Ordem de serviço</param>
        /// <returns>resultado json</returns>
        [Autorizar(Transacao = "MDFEMONITORAMENTO", Acao = "Gerar")]
        public ActionResult GerarMdfeForcada(int os)
        {
            bool status = this._mdfeService.GerarMdfeForcada(os);

            if (status)
            {
                return this.Json(new { success = true, Message = "SUCESSO" });
            }

            return this.Json(new { success = false, Message = "FALHA" });
        }

        /// <summary>
        ///     Ação do controller detalhes
        /// </summary>
        /// <param name="idMdfe">id da Mdfe</param>
        /// <returns>Action Result</returns>
        [Autorizar(Transacao = "MDFEMONITORAMENTO", Acao = "Imprimir")]
        public ActionResult Imprimir(int idMdfe)
        {
            try
            {
                Stream returnContent = this._mdfeService.GerarPdf(idMdfe);
                var fsr = new FileStreamResult(returnContent, "application/pdf");
                fsr.FileDownloadName = string.Format("DaMdfe_{0}.pdf", DateTime.Now.ToString("dd_MM_yyyy"));

                return fsr;
            }
            catch (Exception ex)
            {
                return this.View("Index");
            }
        }

        /// <summary>
        ///     Ação do controller
        /// </summary>
        /// <returns>Action Result</returns>
        [Autorizar(Transacao = "MDFEMONITORAMENTO")]
        public ActionResult Index()
        {
            this.ViewData["PERMISSAO_GERAR"] =
                ServiceLocator.Current.GetInstance<ControleAcessoService>()
                    .Autorizar("MDFEMONITORAMENTO", "Gerar", base.UsuarioAtual);
            return this.View();
        }

        /// <summary>
        ///     Ação do controller detalhes
        /// </summary>
        /// <param name="idMdfe">id da Mdfe</param>
        /// <returns>Action Result</returns>
        [Autorizar(Transacao = "MDFEMONITORAMENTO", Acao = "Pesquisar")]
        public ActionResult ObterHistoricoStatusMdfe(int idMdfe)
        {
            IList<MdfeStatus> result = this._mdfeService.RetornaCteMonitoramentoStatusMdfe(idMdfe);

            return
                this.Json(
                    new
                        {
                            Items =
                                result.Select(
                                    c =>
                                    new
                                        {
                                            DescricaoStatus =
                                        c.MdfeStatusRetorno.Id.ToString() + " - "
                                        + (c.MdfeStatusRetorno.Id == 14 ? c.XmlRetorno : c.MdfeStatusRetorno.Descricao),
                                            DateEmissao = c.DataHora.ToString("dd/MM/yyyy HH:mm:ss"),
                                            Usuario = c.Usuario.Id != 597 ? c.Usuario.Nome : "Processo automático",
                                            Situacao =
                                        Translogic.Core.Commons.Enum<SituacaoMdfeEnum>.GetDescriptionOf(
                                            c.Mdfe.SituacaoAtual)
                                        }),
                            success = true
                        });
        }

        /// <summary>
        ///     Pesquisa das MDFes
        /// </summary>
        /// <param name="pagination">detalhes da paginacao</param>
        /// <param name="filter">informações dos filtros</param>
        /// <returns>resultado json</returns>
        [Autorizar(Transacao = "MDFEMONITORAMENTO", Acao = "Pesquisar")]
        public ActionResult Pesquisar(DetalhesPaginacaoWeb pagination, DetalhesFiltro<object>[] filter)
        {
            DateTime? dataInicial =
                filter.Where(e => e.Campo == "dataInicial" && !string.IsNullOrWhiteSpace(e.Valor[0].ToString()))
                    .Select(
                        e =>
                        e == null
                            ? (DateTime?)null
                            : DateTime.ParseExact(
                                e.Valor[0].ToString(),
                                "dd/MM/yyyy HH:mm:ss",
                                CultureInfo.InvariantCulture))
                    .FirstOrDefault();

            DateTime? dataFinal =
                filter.Where(e => e.Campo == "dataFinal" && !string.IsNullOrWhiteSpace(e.Valor[0].ToString()))
                    .Select(
                        e =>
                        e == null
                            ? (DateTime?)null
                            : DateTime.ParseExact(
                                e.Valor[0].ToString(),
                                "dd/MM/yyyy HH:mm:ss",
                                CultureInfo.InvariantCulture).AddDays(1).AddSeconds(-1))
                    .FirstOrDefault();

            string chave =
                filter.Where(e => e.Campo == "chave" && !string.IsNullOrWhiteSpace(e.Valor[0].ToString()))
                    .Select(e => e == null ? (string)null : e.Valor[0].ToString().ToUpperInvariant())
                    .FirstOrDefault();

            string numero =
                filter.Where(e => e.Campo == "numero" && !string.IsNullOrWhiteSpace(e.Valor[0].ToString()))
                    .Select(e => e == null ? (string)null : e.Valor[0].ToString().ToUpperInvariant())
                    .FirstOrDefault();

            string serie =
                filter.Where(e => e.Campo == "serie" && !string.IsNullOrWhiteSpace(e.Valor[0].ToString()))
                    .Select(e => e == null ? (string)null : e.Valor[0].ToString().ToUpperInvariant())
                    .FirstOrDefault();

            string prefixo =
                filter.Where(e => e.Campo == "prefixo" && !string.IsNullOrWhiteSpace(e.Valor[0].ToString()))
                    .Select(e => e == null ? (string)null : e.Valor[0].ToString().ToUpperInvariant())
                    .FirstOrDefault();

            int? os =
                filter.Where(e => e.Campo == "os" && !string.IsNullOrWhiteSpace(e.Valor[0].ToString()))
                    .Select(
                        e => e == null ? (int?)null : int.Parse(e.Valor[0].ToString(), CultureInfo.InvariantCulture))
                    .FirstOrDefault();

            string origem =
                filter.Where(e => e.Campo == "origem" && !string.IsNullOrWhiteSpace(e.Valor[0].ToString()))
                    .Select(e => e == null ? (string)null : e.Valor[0].ToString().ToUpperInvariant())
                    .FirstOrDefault();

            string destino =
                filter.Where(e => e.Campo == "destino" && !string.IsNullOrWhiteSpace(e.Valor[0].ToString()))
                    .Select(e => e == null ? (string)null : e.Valor[0].ToString().ToUpperInvariant())
                    .FirstOrDefault();

            string situacao =
                filter.Where(e => e.Campo == "situacao" && !string.IsNullOrWhiteSpace(e.Valor[0].ToString()))
                    .Select(e => e == null ? (string)null : e.Valor[0].ToString().ToUpperInvariant())
                    .FirstOrDefault();

            if (situacao == "T")
            {
                situacao = string.Empty;
            }

            int idTrem =
                filter.Where(e => e.Campo == "idTrem" && !string.IsNullOrWhiteSpace(e.Valor[0].ToString()))
                    .Select(e => e == null ? 0 : int.Parse(e.Valor[0].ToString(), CultureInfo.InvariantCulture))
                    .FirstOrDefault();

            ResultadoPaginado<TremMdfeDto> result = this._mdfeService.Listar(
                pagination,
                chave,
                numero,
                serie,
                prefixo,
                os,
                dataInicial,
                dataFinal,
                origem,
                destino,
                situacao,
                idTrem);

            return
                this.Json(
                    new
                        {
                            result.Total,
                            Items =
                                result.Items.Select(
                                    e =>
                                    new
                                        {
                                            e.Id,
                                            e.Prefixo,
                                            e.Origem,
                                            e.Destino,
                                            e.OS,
                                            SituacaoAtualEnum = Enum<SituacaoMdfeEnum>.GetDescriptionOf(e.SituacaoAtual),
                                            Informativo = this.ObterMensagemAmigavel(e.Informativo),
                                            e.UltimoEvento,
                                            DataEvento = e.DataEvento.ToString("dd/MM/yyyy HH:mm:ss")
                                        })
                        });
        }

        /// <summary>
        ///     Pesquisa do Log das MDFes
        /// </summary>
        /// <param name="pagination">detalhes da paginacao</param>
        /// <param name="filter">informações dos filtros</param>
        /// <returns>resultado json</returns>
        [Autorizar(Transacao = "MDFEMONITORAMENTO", Acao = "Pesquisar")]
        public ActionResult PesquisarLogPkg(DetalhesPaginacaoWeb pagination, DetalhesFiltro<object>[] filter)
        {
            string tipoLog =
                filter.Where(e => e.Campo == "tipoLog" && !string.IsNullOrWhiteSpace(e.Valor[0].ToString()))
                    .Select(e => e == null ? (string)null : e.Valor[0].ToString().ToUpperInvariant())
                    .FirstOrDefault();

            string nomeServico =
                filter.Where(e => e.Campo == "nomeServico" && !string.IsNullOrWhiteSpace(e.Valor[0].ToString()))
                    .Select(e => e == null ? (string)null : e.Valor[0].ToString().ToUpperInvariant())
                    .FirstOrDefault();

            ResultadoPaginado<LogPkgMdfeDto> result = this._mdfeService.ListarLogPkg(pagination, tipoLog, nomeServico);

            return
                this.Json(
                    new
                        {
                            result.Total,
                            Items =
                                result.Items.Select(
                                    e =>
                                    new
                                        {
                                            e.Id,
                                            e.Descricao,
                                            DataHora = e.DataHora.ToString("dd/MM/yyyy HH:mm:ss"),
                                            MovimentacaoTrem = e.MovimentacaoTrem.ToString(),
                                            Trem = e.Trem.ToString(),
                                            AreaOperacional = e.AreaOperacional.ToString(),
                                            Composicao = e.Composicao.ToString()
                                        })
                        });
        }

        /// <summary>
        ///     Ação do controller detalhes
        /// </summary>
        /// <param name="idMdfe">id do trem</param>
        /// <returns>Action Result</returns>
        [Autorizar(Transacao = "MDFEMONITORAMENTO", Acao = "Pesquisar")]
        public ActionResult StatusProcessamento(int idMdfe)
        {
            this.ViewData["IdMdfe"] = idMdfe;
            return this.View();
        }


        /// <summary>
        /// Pesquisa para exportação para Excel
        /// </summary>
        /// <param name="LiberacaoFormacaoTremDto"></param>
        /// <returns></returns>
        [Autorizar(Transacao = "MDFEMONITORAMENTO", Acao = "Pesquisar")]
        public ActionResult ExportarExcel(DateTime? dataInicial, DateTime? dataFinal, string chave, string numero, string serie, string prefixo, int? os, string origem, string destino, string situacao)
        {
            try
            {
                chave = !string.IsNullOrWhiteSpace(chave) ? chave.ToUpperInvariant() : "";
                numero = !string.IsNullOrWhiteSpace(numero) ? numero.ToUpperInvariant() : "";
                serie = !string.IsNullOrWhiteSpace(serie) ? serie.ToUpperInvariant() : "";
                prefixo = !string.IsNullOrWhiteSpace(prefixo) ? prefixo.ToUpperInvariant() : "";
                origem = !string.IsNullOrWhiteSpace(origem) ? origem.ToUpperInvariant() : "";
                destino = !string.IsNullOrWhiteSpace(destino) ? destino.ToUpperInvariant() : "";
                situacao = !string.IsNullOrWhiteSpace(situacao) ? (situacao != "T" ? situacao.ToUpperInvariant() : "") : "";

                var colunas = new List<ExcelColuna<TremMdfeDto>>
                {
                    new ExcelColuna<TremMdfeDto>("Status", c => TraduzirEnumerador(c.SituacaoAtual)),
                    new ExcelColuna<TremMdfeDto>("Prefixo", c => c.Prefixo),                 
                    new ExcelColuna<TremMdfeDto>("Origem", c => c.Origem),
                    new ExcelColuna<TremMdfeDto>("Destino", c => c.Destino),    
                    new ExcelColuna<TremMdfeDto>("Os", c => c.OS),    
                    new ExcelColuna<TremMdfeDto>("Informativo", c => c.Informativo),
                    new ExcelColuna<TremMdfeDto>("Último Evento", c => c.UltimoEvento),    
                    new ExcelColunaData<TremMdfeDto>("Data Evento", c =>  c.DataEvento != null  ? c.DataEvento.ToString("dd/MM/yyyy HH:mm:ss") : "")
                };

                var resultado = _mdfeService.ExportarExcel(chave, numero, serie, prefixo, os, dataInicial, dataFinal, origem, destino, situacao);

                return ExcelHelper.Exportar(
                    "MDFe_Monitoramento.xlsx",
                    colunas,
                    resultado,
                    ws =>
                    {
                        ws.Cells.AutoFitColumns();
                    });

            }
            catch (Exception ex)
            {
                return this.Json(new { Success = false, Message = ex.Message });
            }
        }

        /// <summary>
        /// Exporta Relatorio de MDFe, conforme especificacao GDS 1990
        /// </summary>
        /// <param name="dataInicial"></param>
        /// <param name="dataFinal"></param>
        /// <param name="chave"></param>
        /// <param name="numero"></param>
        /// <param name="serie"></param>
        /// <param name="prefixo"></param>
        /// <param name="os"></param>
        /// <param name="origem"></param>
        /// <param name="destino"></param>
        /// <param name="situacao"></param>
        /// <returns></returns>
        [Autorizar(Transacao = "MDFEMONITORAMENTO", Acao = "Pesquisar")]
        public ActionResult ExportarRelatorioMDFe(DateTime? dataInicial, DateTime? dataFinal, string chave, string numero, string serie, string prefixo, int? os, string origem, string destino, string situacao)
        {
            try
            {
                chave = !string.IsNullOrWhiteSpace(chave) ? chave.ToUpperInvariant() : "";
                numero = !string.IsNullOrWhiteSpace(numero) ? numero.ToUpperInvariant() : "";
                serie = !string.IsNullOrWhiteSpace(serie) ? serie.ToUpperInvariant() : "";
                prefixo = !string.IsNullOrWhiteSpace(prefixo) ? prefixo.ToUpperInvariant() : "";
                origem = !string.IsNullOrWhiteSpace(origem) ? origem.ToUpperInvariant() : "";
                destino = !string.IsNullOrWhiteSpace(destino) ? destino.ToUpperInvariant() : "";
                situacao = !string.IsNullOrWhiteSpace(situacao) ? (situacao != "T" ? situacao.ToUpperInvariant() : "") : "";

                var colunas = new List<ExcelColuna<VwNfeCteMdfe>>
                {
                    new ExcelColuna<VwNfeCteMdfe>("UF Origem", c => c.UfOrigem),
                    new ExcelColunaData<VwNfeCteMdfe>("Data Emissão", c => c.DataEmissao),
                    new ExcelColuna<VwNfeCteMdfe>("Origem", c => c.OrigemEstacao),
                    new ExcelColuna<VwNfeCteMdfe>("Destino", c => c.DestinoEstacao),
                    new ExcelColuna<VwNfeCteMdfe>("Trem", c => c.Trem),
                    new ExcelColunaInteira<VwNfeCteMdfe>("Os", c => c.Os),
                    new ExcelColuna<VwNfeCteMdfe>("Status", c => c.StatusUltimoEvento),
                    new ExcelColuna<VwNfeCteMdfe>("Informativo", c => c.SituacaoAtual),
                    new ExcelColunaInteira<VwNfeCteMdfe>("Chave MDFe", c =>  c.ChaveAcessoMdfe),
                    new ExcelColuna<VwNfeCteMdfe>("CNPJ Emit. MDFe", c =>  c.CnpjEmitenteMdfe),
                    new ExcelColunaInteira<VwNfeCteMdfe>("Número MDFe", c =>  c.NumeroMdfe),
                    new ExcelColuna<VwNfeCteMdfe>("Série MDFe", c =>  c.SerieMdfe),
                    new ExcelColunaInteira<VwNfeCteMdfe>("Chave CTe", c =>  c.ChaveAcessoCte),
                    new ExcelColuna<VwNfeCteMdfe>("CNPJ Emit. CTe", c =>  c.CnpjEmitenteCte),
                    new ExcelColunaDinheiro<VwNfeCteMdfe>("Valor Total CTe", c =>  c.ValorCte),
                    new ExcelColunaInteira<VwNfeCteMdfe>("Chave NFe", c =>  c.ChaveAcessoNfe),
                    new ExcelColuna<VwNfeCteMdfe>("CNPJ Emit. NFe", c =>  c.CnpjEmitenteNfe),
                    new ExcelColunaDinheiro<VwNfeCteMdfe>("Valor Total NFe", c =>  c.ValorNfe)
                };

                var resultado = _mdfeService.ExportarExcelRelatorioMDFe(chave, numero, serie, prefixo, os, dataInicial, dataFinal, origem, destino, situacao);

                return ExcelHelper.Exportar(
                    "RelatorioMDFeCompleto.xlsx",
                    colunas,
                    resultado,
                    ws =>
                    {
                        ws.Cells.AutoFitColumns();
                    });

            }
            catch (Exception ex)
            {
                return this.Json(new { Success = false, Message = ex.Message });
            }
        }

        #endregion

        #region Methods

        private string ObterMensagemAmigavel(string situacao)
        {
            switch (situacao)
            {
                case "SEM":
                    return "O MDF-e da composição será gerado na estação de Pereque (ZPG)";
                case "AUT":
                    return "MDF-e Aprovado";
                case "AGE":
                case "AGC":
                    return "Aguardando Cancelamento/Encerramento";
                case "EAE":
                    return "Enviado arquivo MDF-e para Sefaz";
                case "EAR":
                    return "Erro Autorizado reenvio do MDF-e";
                case "CAN":
                    return "MDF-e Cancelado";
                case "ENC":
                    return "MDF-e Encerrado";
                case "ERR":
                    return "Erro na geração do MDF-e";
                case "PGC":
                    return "MDf-e aguardando a geração automática da numeração / chave";
                default:
                    return "MDF-e em processamento";
            }
        }

        private string TraduzirEnumerador(SituacaoMdfeEnum situacao)
        {
            switch (situacao)
            {
                case SituacaoMdfeEnum.PendenteArquivoEnvio:
                    return "Envio pendente";
                case SituacaoMdfeEnum.PendenteArquivoCancelamento:
                    return "Cancelamento pendente";
                case SituacaoMdfeEnum.PendenteArquivoInutilizacao:
                    return "Inutilização pendente";
                case SituacaoMdfeEnum.EnviadoFilaArquivoEnvio:
                    return "Esta na fila de envio";
                case SituacaoMdfeEnum.EnviadoFilaArquivoCancelamento:
                    return "Esta na fila de cancelamento";
                case SituacaoMdfeEnum.EnviadoFilaArquivoEncerramento:
                    return "Esta na fila de encerramento";
                case SituacaoMdfeEnum.EnviadoArquivoEnvio:
                    return "Enviado arquivo de envio";
                case SituacaoMdfeEnum.EnviadoArquivoEncerramento:
                    return "Enviado arquivo de encerramento";
                case SituacaoMdfeEnum.EnviadoArquivoCancelamento:
                    return "Enviado arquivo de cancelamento";
                case SituacaoMdfeEnum.Autorizado:
                    return "Autorizado";
                case SituacaoMdfeEnum.Cancelado:
                    return "Cancelado";
                case SituacaoMdfeEnum.Encerrado:
                    return "Encerrado";
                case SituacaoMdfeEnum.Erro:
                    return "Erro";
                case SituacaoMdfeEnum.ErroAutorizadoReEnvio:
                    return "Autorizado reenvio";
                case SituacaoMdfeEnum.Invalidado:
                    return "Inválido";
                case SituacaoMdfeEnum.UsoDenegado:
                    return "Uso denegado";
                case SituacaoMdfeEnum.AguardandoCancelamento:
                    return "Aguardando cancelamento";
                case SituacaoMdfeEnum.AguardandoEncerramento:
                    return "Aguardando encerramento";
                case SituacaoMdfeEnum.AguardandoMdfeEncerramento:
                    return "Aguardando encerramento";
                case SituacaoMdfeEnum.AutorizadoCancelamento:
                    return "Cancelamento autorizado";
                case SituacaoMdfeEnum.PendendeGeracaoChaveMdfe:
                    return "Geração da chave pendente";
                case SituacaoMdfeEnum.MdfeEmProcessamento:
                    return "Em processamento";
                default:
                    return "(desconhecido)";
            }
        }

        #endregion
    }
}