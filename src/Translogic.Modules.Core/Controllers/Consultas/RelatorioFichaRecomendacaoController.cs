﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web.Mvc;
using System.Linq;
using Translogic.Core.Infrastructure.FileSystem.ExtensionMethods;
using Translogic.Core.Infrastructure.Web;
using Translogic.Core.Infrastructure.Web.Helpers;
using Translogic.Modules.Core.Domain.Model.Dto;
using Translogic.Modules.Core.Domain.Services.Mdfes;
using Translogic.Modules.Core.Domain.Services.RelatorioFichaRecomendacao;
using Translogic.Modules.Core.Infrastructure;
using Translogic.Modules.Core.Domain.Services.RelatorioFichaRecomendacao.Interface;
using Translogic.Modules.Core.Domain.Model.Trem.Repositories;

namespace Translogic.Modules.Core.Controllers.Consultas
{
    public class RelatorioFichaRecomendacaoController : BaseSecureModuleController
    {
        #region Serviços
            private readonly MdfeService _mdfeService;
            private readonly IRelatorioFichaRecomendacaoService _relatorioFichaRecomendacaoService;
            private readonly ITremRepository _tremRepository;
        #endregion

        #region Construtores

            public RelatorioFichaRecomendacaoController(
                MdfeService mdfeService,
                IRelatorioFichaRecomendacaoService relatorioFichaRecomendacaoService,
                ITremRepository tremRepository)
            {
                _mdfeService = mdfeService;
                _relatorioFichaRecomendacaoService = relatorioFichaRecomendacaoService;
                _tremRepository = tremRepository;
            }

        #endregion

        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Pesquisa para GRID Relatorio Ficha Recomendação
        /// </summary>
        /// <param name="detalhesPaginacaoWeb"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public JsonResult ConsultaRelatorioFichaRecomendacao(DetalhesPaginacaoWeb detalhesPaginacaoWeb, string trem, decimal? numOS, string avaria)
        {
            try
            {
                //R1
                if (trem == string.Empty && numOS == null)
                {
                    throw new Exception("É necessário informar o Trem ou a OS");
                }

                var resultado = _relatorioFichaRecomendacaoService.ObterTrensRecomendados(detalhesPaginacaoWeb, trem, numOS, avaria);


                var jsonData = Json(
                    new
                    {
                        Items = resultado.Items.Select(i => new
                           {
                               //Campos
                               NumeroOS = i.NumOs, 
                               Prefixo = i.Prefixo, 
                               Origem = i.Origem, 
                               Destino = i.Destino,
                               Local = i.Local,
                               DtChegadaPrevista = i.DtChegadaPrevista.ToString("dd/MM HH:mm"), 
                               QtdAvaria = i.QtdAvaria, 
                               QtdVagao = i.QtdVagao
                           }),
                            Total = resultado.Total,
                            Sucess = true,
                            Status = 200
                    });
                return jsonData;
            }
            catch (Exception e)
            {
                return Json(new
                {
                    Success = false,
                    e.Message,
                    Status = 400 // Bad Request
                });
            }
        }

        //Retorna o PDF Relatorio Ficha Recomendacao
        [Autorizar(Transacao = "CONSUFICHARECOMEN", Acao = "Pesquisar")]
        public ActionResult Imprimir(int numOS)
        {            
            var tremDto = _tremRepository.ObterPorNumeroOrdemServicoSemFetch(numOS);

            Stream pdf = null;
            Stream returnContent = new MemoryStream();

            var merge = new PdfMerge();

            ViewData["Trem"] = tremDto.Prefixo;
            ViewData["OS"] = tremDto.OrdemServico.Numero;
            ViewData["Data"] = tremDto.DataPrevistaPartida;
            ViewData["ListaRelatorioFichaRecomendacao"] = _relatorioFichaRecomendacaoService.ObterListaFichaRecomendacaoExportar(numOS);

            var viewString = View("ImpressaoFichaRecomendacao").Capture(ControllerContext);

            pdf = _mdfeService.HtmlToPdfSemMargin(viewString, "pdfFichaRecomendacao", false);

            if (pdf != null)
            {
                merge.AddFile(pdf);
                returnContent = merge.Execute();
            }

            var fsr = new FileStreamResult(returnContent, "application/pdf");
            fsr.FileDownloadName = "Trem " + tremDto.Prefixo.ToString() + ".pdf";
            return fsr;
        }
    }
}
