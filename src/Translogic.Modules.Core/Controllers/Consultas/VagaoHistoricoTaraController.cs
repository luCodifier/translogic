﻿namespace Translogic.Modules.Core.Controllers.Consultas
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using ALL.Core.Dominio;
    using Translogic.Core.Infrastructure.Web;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Modules.Core.Domain.Services.Consulta;
    using System.ComponentModel.DataAnnotations;

    public class VagaoHistoricoTaraController : BaseSecureModuleController
    {
        private readonly VagaoHistoricoTaraService _vagaoHistoricoTaraService;

        public VagaoHistoricoTaraController(VagaoHistoricoTaraService vagaoHistoricoTaraService)
        {
            _vagaoHistoricoTaraService = vagaoHistoricoTaraService;
        }

        public ActionResult Index()
        {
            return View();
        }

        //public JsonResult ObterHistoricos(DetalhesPaginacaoWeb pagination, DetalhesFiltro<object>[] filter, string dataInicial, string dataFinal)
        public JsonResult ObterHistoricos(DetalhesPaginacaoWeb pagination, string vagao, string serie, string dataInicial, string dataFinal)
        {
            var vagoes = vagao.Trim().Replace(";", ",").Replace("-", ",").Replace(" ", ",");
            var series = serie.Trim().Replace(";", ",").Replace("-", ",").Replace(" ", ",");
          
            ResultadoPaginado<VagaoHistoricoTaraDto> result = _vagaoHistoricoTaraService.ObterHistoricoTaras(pagination, vagoes, serie, dataInicial, dataFinal);
            return Json(new
            {
                result.Total,
                Items = result.Items.Select(c => new
                {
                    Vagao = c.CODVAGAO,
                    Serie = c.SERIE,
                    Data = c.DATA.ToString("dd/MM/yyyy HH:mm:ss"),
                    TaraAnterior = c.TARAANTERIOR,
                    TaraAtual =   c.TARAATUAL,
                    TaraMediana = c.TARAMEDIANA,
                }),
                success = true
            });
        }



        public ActionResult ExportarHistoricos(string vagao, string serie, string dataInicial, string dataFinal)
        {
            var vagoes = vagao.Trim().Replace(";", ",").Replace("-", ",").Replace(" ", ",");
            var series = serie.Trim().Replace(";", ",").Replace("-", ",").Replace(" ", ",");
            var retorno = _vagaoHistoricoTaraService.ExportarHistoricoTara(vagoes, serie, dataInicial, dataFinal);

            string csv = ListToCSV(retorno);
            return File(new System.Text.UnicodeEncoding().GetBytes(csv), "text/csv", "Relatorio_Atualizacao_Tara.csv");
        }

        /// <summary>
        /// Transformar lista em string contendo dados separados por ; para exportar para CSV
        /// </summary>
        /// <typeparam name="T">Tipo</typeparam>
        /// <param name="list">Lista</param>
        /// <returns>String CSV</returns>
        private string ListToCSV<T>(IEnumerable<T> list)
        {
            System.Text.StringBuilder sList = new System.Text.StringBuilder();
            Type type = typeof(T);
            var props = type.GetProperties();
            //sList.Append(string.Join(";", props.Select(p => (p.GetCustomAttributes(true)[0] as DisplayAttribute).Name))).Replace("DATA PROCESSO LEGADO",string.Empty);
            sList.Append(string.Join(";", props.Select(p => (p.GetCustomAttributes(true)[0] as DisplayAttribute).Name)));
            //sList.Append(string.Join(";", props.Select(p => (p.GetCustomAttributes(true)[0] as DisplayAttribute).Name))).Replace("SINDICÂNCIA", string.Empty);
            sList.Append(Environment.NewLine);

            foreach (var element in list)
            {
                sList.Append(string.Join(";", props.Select(p => p.GetValue(element, null))));
                sList.Append(Environment.NewLine);
            }

            return sList.ToString();
        }
    }
}
