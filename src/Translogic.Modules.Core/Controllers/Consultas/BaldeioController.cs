﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Translogic.Modules.Core.Domain.Services.Baldeio.Interface;
using Translogic.Modules.Core.Domain.Services.ArquivoBaldeio.Interface;
using Translogic.Modules.Core.Infrastructure;
using Translogic.Modules.TPCCO.Helpers;
using Translogic.Modules.Core.Domain.Model.Dto.Baldeio;
using Newtonsoft.Json;
using System.IO;
using System.Configuration;
using Translogic.Core.Infrastructure.FileSystem;
using Translogic.Core.Infrastructure.Web;

namespace Translogic.Modules.Core.Controllers.Consultas
{
    public class BaldeioController : BaseSecureModuleController
    {
        #region Serviços
        private readonly IBaldeioService _baldeioService;
        private readonly ICartaBaldeioService _arquivoService;
        #endregion

        #region Construtores
        public BaldeioController(IBaldeioService baldeioService,
                                 ICartaBaldeioService arquivoService)
        {
            _baldeioService = baldeioService;
            _arquivoService = arquivoService;
        }
        #endregion

        #region ActionViews
        [Autorizar(Transacao = "CONSULTABALDEIO", Acao = "Pesquisar")]
        public ActionResult Index()
        {
            ViewData["Permissoes"] = _baldeioService.BuscarPermissaoUsuario(base.UsuarioAtual);
            return View();
        }

        [Autorizar(Transacao = "CONSULTABALDEIO", Acao = "Importar")]
        public ActionResult Importar(int idBaldeio, string local, string dtInicial, string dtFinal, string vagaoCedente, string vagaoRecebedor)
        {
            ViewData["idBaldeio"] = idBaldeio;
            ViewData["Local"] = local;
            ViewData["DtInicial"] = dtInicial;
            ViewData["DtFinal"] = dtFinal;
            ViewData["VagaoCedente"] = vagaoCedente;
            ViewData["VagaoRecebedor"] = vagaoRecebedor;

            return View();
        }

        private ActionResult RedirectToIndex(string mensagens, string local, string dtInicial, string dtFinal, string vagaoCedente, string vagaoRecebedor)
        {
            ViewData["Local"] = local;
            ViewData["DtInicial"] = dtInicial;
            ViewData["DtFinal"] = dtFinal;
            ViewData["VagaoCedente"] = vagaoCedente;
            ViewData["VagaoRecebedor"] = vagaoRecebedor;
            ViewData["Mensagens"] = mensagens;
            ViewData["Permissoes"] = _baldeioService.BuscarPermissaoUsuario(base.UsuarioAtual);

            return View("Index");
        }

        [Autorizar(Transacao = "CONSULTABALDEIO", Acao = "Importar")]
        public ActionResult ConfirmarImportacao()
        {
            var local = Request.Form["Local"];
            var dtInicial = Request.Form["DtInicial"];
            var dtFinal = Request.Form["DtFinal"];
            var vagaoCedente = Request.Form["VagaoCedente"];
            var vagaoRecebedor = Request.Form["VagaoRecebedor"];

            var mensagens = string.Empty;

            if (Request.Form.Keys.Count > 0 && Request.Files != null)
            {
                var idBaldeio = string.IsNullOrEmpty(Request.Form["idBaldeio"]) ? 0 : Convert.ToInt32(Request.Form["idBaldeio"]);

                if (idBaldeio == 0)
                {
                    mensagens += "Id do baldeio não fornecido";
                }
                else
                {
                    var fileContent = Request.Files[0];
                    if (fileContent != null && fileContent.ContentLength > 0)
                    {
                        try
                        {
                            BinaryReader b = new BinaryReader(fileContent.InputStream);
                            byte[] arquivo = b.ReadBytes(fileContent.ContentLength);

                            var indices = fileContent.FileName.LastIndexOf(@"\") + 1;
                            string nomeArquivo = fileContent.FileName.Substring(indices, (fileContent.FileName.Length - indices) - ".pdf".Length);

                            _arquivoService.AnexarCarta(idBaldeio, arquivo, nomeArquivo, this.UsuarioAtual.Codigo);
                        }
                        catch (Exception ex)
                        {
                            mensagens = "Erro na importação. Erro: " + ex.Message;
                        }
                    }
                    else
                    {
                        mensagens += "Arquivo da Carta não fornecido";
                    }
                }
            }
            else
            {
                mensagens += "Arquivo da Carta não fornecido";
            }



            return RedirectToIndex(mensagens, local, dtInicial, dtFinal, vagaoCedente, vagaoRecebedor);
        }
        #endregion

        #region Métodos

        /// <summary>
        /// Consulta tela de consulta baleio
        /// </summary>
        /// <param name="detalhesPaginacaoWeb"></param>
        /// <param name="baldeioDto"></param>
        /// <returns>Retorna lista consulta tela de consulta baleio</returns>
        [Autorizar(Transacao = "CONSULTABALDEIO", Acao = "Pesquisar")]
        public JsonResult ObterConsultaBaldeio(DetalhesPaginacaoWeb detalhesPaginacaoWeb, string baldeioDto)
        {
            var request = JsonConvert.DeserializeObject<BaldeioRequestDto>(baldeioDto);

            //Valida filtros(campos)
            ValidaCampos(request);

            var resultado = _baldeioService.ObterConsultaBaldeio(detalhesPaginacaoWeb,
                                                                    request.dtInicial.ToString(),
                                                                    request.dtFinal.ToString(),
                                                                    request.Local,
                                                                    request.VagaoCedente,
                                                                    request.VagaoRecebedor);

            var temPermissaoExcluir = _baldeioService.BuscarPermissaoUsuario(UsuarioAtual).ExcluirCarta;

            var jsonData = Json(
                new
                {
                    Items = resultado.Items.Select(i => new
                    {
                        IdBaldeio = i.IdBaldeio,
                        IdArquivoBaldeio = i.IdArquivoBaldeio,
                        Local = i.Local,
                        DtBaldeio = i.DataBaldeio.ToString("dd/MM/yyyy HH:mm:ss"),
                        VagaoCedente = i.VagaoCedente,
                        VagaoRecebedor = i.VagaoRecebedor,
                        QuantidadeBaldeada =  Math.Round(i.QuantidadeBaldeada,3).ToString().Replace(".",","),
                        QtdFicouVagCedente = i.QuantidadeVagaoCedente.ToString().Replace(".", ","),
                        Despacho = i.Despacho,
                        DtDespacho = i.DataDespacho.ToString("dd/MM/yyyy HH:mm:ss"),
                        HabilitarPdf = i.IdArquivoBaldeio > 0 ? 0 : 1,
                        HabilitarExcluir =  (temPermissaoExcluir ? (i.IdArquivoBaldeio > 0 ? 0 : 1) : 1)
                    }),
                    Total = resultado.Total
                });

            return jsonData;
        }

        /// <summary>
        /// Pesquisa para exportação baldeio para Excel
        /// </summary>
        /// <param name="baldeioDto"></param>
        /// <returns></returns>
        [Autorizar(Transacao = "CONSULTABALDEIO", Acao = "Pesquisar")]
        public ActionResult ExportarXlsBaldeio(string baldeioDto)
        {
            var request = JsonConvert.DeserializeObject<BaldeioRequestDto>(baldeioDto);

            ValidaCampos(request);

            var colunas = new List<ExcelColuna<BaldeioDto>>
            {
                new ExcelColuna<BaldeioDto>("Local", c => !string.IsNullOrEmpty(c.Local) ? c.Local : string.Empty),
                new ExcelColunaData<BaldeioDto>("Data Baldeio", c => c.DataBaldeio.ToString("dd/MM/yyyy HH:mm:ss")),
                new ExcelColuna<BaldeioDto>("Vagão Cedente", c => !string.IsNullOrEmpty(c.VagaoCedente) ? c.VagaoCedente : string.Empty),
                new ExcelColuna<BaldeioDto>("Vagão Recebedor", c => !string.IsNullOrEmpty(c.VagaoRecebedor) ? c.VagaoRecebedor : string.Empty),
                new ExcelColunaDecimal<BaldeioDto>("Quantidade Baldeada", c=> Math.Round(c.QuantidadeBaldeada,3).ToString().Replace(".",",")),
                new ExcelColuna<BaldeioDto>("Quantidade ficou no vagão Cedente", c => c.QuantidadeVagaoCedente),
                new ExcelColuna<BaldeioDto>("Despacho", c => c.Despacho),
                new ExcelColunaData<BaldeioDto>("Data Despacho", c => c.DataDespacho.ToString("dd/MM/yyyy HH:mm:ss")),
                new ExcelColuna<BaldeioDto>("Tem Anexo", c => (c.TemAnexo.ToUpper() == "S" ? "SIM" : "NÃO"))
            };

            var resultado = _baldeioService.ObterConsultaBaldeioExportar(request.dtInicial.ToString(),
                                                                            request.dtFinal.ToString(),
                                                                            request.Local,
                                                                            request.VagaoCedente,
                                                                            request.VagaoRecebedor);

            return ExcelHelper.Exportar(
                "Consulta Baldeio.xlsx",
                colunas,
                resultado,
                ws =>
                {
                    ws.Cells.AutoFitColumns();
                });
        }

        /// <summary>
        /// Valida regras campos tela baldeio
        /// </summary>
        /// <param name="request"></param>
        /// <returns>Retorna true se está tudo correto ou false se existir algum problema retorna mensagem do erro</returns>
        private void ValidaCampos(BaldeioRequestDto request)
        {
            if (request.dtFinal.HasValue && request.dtInicial.HasValue)
            {

                if (request.dtInicial == null || request.dtFinal == new DateTime())
                    throw new Exception("Data inicial é obrigatório");

                if (request.dtInicial == null || request.dtFinal == new DateTime())
                    throw new Exception("Data inicial é obrigatório");

                if (DateTime.Compare(request.dtFinal.Value, request.dtInicial.Value) < 0)
                    throw new Exception("Data inicial deve ser menor que a data final");
            }
        }

        /// <summary>
        /// Exporta Carta do baldeio em formato PDF
        /// </summary>
        /// <param name="idArquivoBaldeio"></param>
        /// <returns></returns>
        [Autorizar(Transacao = "CONSULTABALDEIO", Acao = "Pesquisar")]
        public ActionResult ExportarCartaPdf(int idArquivoBaldeio)
        {
            return _arquivoService.ObterArquivoBaldeio(idArquivoBaldeio);
        }

        /// <summary>
        /// Excluir o registro e arquivo
        /// </summary>
        /// <param name="idArquivoBaldeio"></param>
        [Autorizar(Transacao = "CONSULTABALDEIO", Acao = "Pesquisar")]
        public JsonResult ExcluirCartaPdf(int idArquivoBaldeio)
        {
            try
            {
                _arquivoService.ExcluirArquivo(idArquivoBaldeio, UsuarioAtual);

                return Json(new
                {
                    sucesso = true,
                    message = "Arquivo excluído."
                });

            }
            catch(Exception ex)
            {
                return Json(new
                {
                    sucesso = false,
                    message = "Erro ao excluir. " + ex.Message
                });
            }
        }
        #endregion
    }
}
