﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Translogic.Core.Infrastructure.Web;
using Translogic.Core.Infrastructure.Web.Helpers;
using Translogic.Modules.Core.Domain.Services.ConsultaTrem.Interface;
using Translogic.Modules.TPCCO.Helpers;
using Translogic.Modules.Core.Domain.Model.Dto.ConsultaTrem;
using System.Net;
using System.IO;
using System.Text;
using Translogic.Modules.Core.Infrastructure;

namespace Translogic.Modules.Core.Controllers.Consultas
{
    [Authorize]
    public class ConsultaTremController : BaseSecureModuleController
    {
        #region Services
        private readonly IConsultaTremService _consultaTremService;
        private readonly IChecklistFichaTremService _checklistFichaTremService;
        #endregion

        #region Construtores
        public ConsultaTremController(IConsultaTremService consultaTremService,
                                      IChecklistFichaTremService checklistFichaTremService
                                     )
        {
            _consultaTremService = consultaTremService;
            _checklistFichaTremService = checklistFichaTremService;
        }
        #endregion

        #region Métodos/Actions da Tela de Consulta Trem

        [Autorizar(Transacao = "CONSUFICHAACOMPCIRCTREM", Acao = "Pesquisar")]
        public ActionResult Index()
        {
            ViewData["UrlLegado"] = Settings.UrlTranslogicLegado.Replace("translogic/", "");
            //Verifica e retorna se usuario logado tem acesso ao botão de crud de perguntas do checklist
            //OBS ISSO É AS PERMISSÕES DE ACESSO
            ViewData["PerguntaCheckList"] = true;
            return View();
        }

        [Autorizar(Transacao = "CONSUFICHAACOMPCIRCTREM", Acao = "Pesquisar")]
        public JsonResult ObterConsultaTrem(DetalhesPaginacaoWeb detalhesPaginacaoWeb, string prefixo, string os, string situacao, string origem, string destino, string tempoAPT, string prefixosExcluir)
        {
            int Onu = 0;
            var resultado = _consultaTremService.ObterConsultaTrem(detalhesPaginacaoWeb,
                                                                               prefixo,
                                                                               os,
                                                                               situacao,
                                                                               origem,
                                                                               destino,
                                                                               tempoAPT,
                                                                               prefixosExcluir);

            var listaNumOs = resultado.Items.Select(r => r.NumOs.ToString()).ToList();
            var listaONUs = listaNumOs.Count > 0 ? _consultaTremService.ObterTotalMercadarioOnu(listaNumOs) : new List<ConsultaTremDto>();


            if (resultado.Total > 0 && listaONUs.ToList().Count > 0)
            {
                resultado.Items = _consultaTremService.AtritbuiTotalOnu(resultado.Items, listaONUs);
            }

            var jsonData = Json(
                new
                {
                    Items = resultado.Items.Select(i => new
                    {
                        i.IdTrem,
                        i.NumOs,
                        i.Trem,
                        i.Origem,
                        i.Destino,
                        i.HorasAPT1,
                        i.HorasAPT,
                        PartidaPrevisao = i.PartidaPrevisao.HasValue ? i.PartidaPrevisao.Value.ToString("dd/MM/yyyy HH:mm:ss") : string.Empty,
                        PartidaReal = i.PartidaReal.HasValue ? i.PartidaReal.Value.ToString("dd/MM/yyyy HH:mm:ss") : string.Empty,
                        i.Situacao,
                        i.LocalAtual,
                        DataHora = i.DataHora.HasValue ? i.DataHora.Value.ToString("dd/MM/yyyy HH:mm:ss") : string.Empty,
                        i.PosicaoVirtual,
                        Desde = i.Desde.HasValue ? i.Desde.Value.ToString("dd/MM/yyyy HH:mm:ss") : string.Empty,
                        i.TLocos,
                        i.LT,
                        i.LR,
                        i.TVagoes,
                        i.VC,
                        i.VZ,
                        i.Comprimento,
                        i.Peso,
                        i.TotalOnu
                    }),
                    Total = resultado.Total
                });
            return jsonData;

        }

        [Autorizar(Transacao = "CONSUFICHAACOMPCIRCTREM", Acao = "Pesquisar")]
        public ActionResult ExportarConsultaTrem(string prefixo, string os, string situacao, string origem, string destino, string tempoAPT, string prefixosExcluir)
        {

            var colunas = new List<ExcelColuna<ConsultaTremDto>>
            {
                new ExcelColuna<ConsultaTremDto>("NumOs", c => c.NumOs),
                new ExcelColuna<ConsultaTremDto>("Trem", c => c.Trem != null ? c.Trem.ToString() : string.Empty),
                new ExcelColuna<ConsultaTremDto>("Origem", c => c.Origem != null ? c.Origem.ToString() : string.Empty),
                new ExcelColuna<ConsultaTremDto>("Destino", c => c.Destino != null ? c.Destino.ToString() : string.Empty),
                new ExcelColuna<ConsultaTremDto>("HorasAPT1", c => c.HorasAPT1 != null ? c.HorasAPT1.ToString() : string.Empty),
                new ExcelColunaData<ConsultaTremDto>("PartidaPrevisao", c => c.PartidaPrevisao.HasValue ? c.PartidaPrevisao.Value.ToString("dd/MM/yyyy HH:mm:ss") : string.Empty),
                new ExcelColunaData<ConsultaTremDto>("PartidaReal", c => c.PartidaReal.HasValue ? c.PartidaReal.Value.ToString("dd/MM/yyyy HH:mm:ss") : string.Empty),
                new ExcelColuna<ConsultaTremDto>("Situacao", c => c.Situacao != null ? c.Situacao.ToString() : string.Empty),
                new ExcelColuna<ConsultaTremDto>("LocalAtual", c => c.LocalAtual != null ? c.LocalAtual.ToString() : string.Empty),
                new ExcelColunaData<ConsultaTremDto>("DataHora", c => c.DataHora.HasValue ? c.DataHora.Value.ToString("dd/MM/yyyy HH:mm:ss") : string.Empty),
                new ExcelColuna<ConsultaTremDto>("PosicaoVirtual", c => c.PosicaoVirtual != null ? c.PosicaoVirtual.ToString() : string.Empty),
                new ExcelColunaData<ConsultaTremDto>("Desde", c =>c.Desde.HasValue ? c.Desde.Value.ToString("dd/MM/yyyy HH:mm:ss") : string.Empty),
                new ExcelColuna<ConsultaTremDto>("TLocos", c => c.TLocos),
                new ExcelColuna<ConsultaTremDto>("LT", c => c.LT),
                new ExcelColuna<ConsultaTremDto>("LR", c => c.LR),
                new ExcelColuna<ConsultaTremDto>("TVagoes", c => c.TVagoes),
                new ExcelColuna<ConsultaTremDto>("VC", c => c.VC),
                new ExcelColuna<ConsultaTremDto>("VZ", c => c.VZ),
                new ExcelColuna<ConsultaTremDto>("Comprimento", c => c.Comprimento),
                new ExcelColuna<ConsultaTremDto>("Peso", c => c.Peso)
            };

            var resultado = _consultaTremService.ObterConsultaTremExportar(prefixo, os, situacao, origem, destino, tempoAPT, prefixosExcluir);

            return ExcelHelper.Exportar(
                "Consulta_Trem.xlsx",
                colunas,
                resultado,
                ws =>
                {
                    ws.Cells.AutoFitColumns();
                });
        }

        [HttpGet]
        public ActionResult GeraFichaComChecklist(int idTrem, bool mercPerigosa, bool papeleta)
        {
            var fichaImportada = ImportarFichaTremLegado(idTrem);
            var metaAntiga = "<meta http-equiv=\"X-UA-Compatible\" content=\"IE=11\"/>";

            if (fichaImportada.Contains(metaAntiga))
            {
                var compatibilidade = "X-UA-Compatible";
                var versão = "IE=11";
                string meta = String.Format("<meta http-equiv='{0}' content='{1}'/>", compatibilidade, versão);

                fichaImportada = fichaImportada.Replace(metaAntiga, meta);
            }
            if (mercPerigosa)
            {
                if (fichaImportada.Contains("dvMercPerigosa"))
                {
                    fichaImportada = fichaImportada.Replace("<div id=\"dvMercPerigosa\"></div>", RecuperaHtmlMercadoriaPerigosa());
                }
            }

            if (papeleta)
            {
                if (fichaImportada.Contains("dvCheckListTrem"))
                {
                    fichaImportada = fichaImportada.Replace("<div id=\"dvCheckListTrem\"></div>", RecuperaHtmlChecklistTrens());
                }
            }

            ViewData["FichaChecklist"] = fichaImportada;
            return View("PaginasImpressao/FichaTremChecklist");
        }

        public string ImportarFichaTremLegado(int idTrem)
        {
            string htmlFichaTrem = string.Empty;
            try
            {
                var url = Settings.UrlTranslogicLegado + "/translogicI/ImpressaoFichaTrem.asp?id=" + idTrem;
                var request = WebRequest.Create(url);
                var response = request.GetResponse();
                var dataStream = response.GetResponseStream();
                var reader = new StreamReader(dataStream, Encoding.GetEncoding("iso-8859-1"), true);
                htmlFichaTrem = reader.ReadToEnd();

                if (htmlFichaTrem.Contains("logoRumo"))
                {
                    htmlFichaTrem = htmlFichaTrem.Replace("<img id=\"logoRumo\" src=\"/imagens/logo_rumo.jpg\"  style=\"width:180px; height:80px;\"/>", "<img id=\"logoRumo\" src=\"../../../Resources/rumo.png\" />");
                }

                reader.Close();
                response.Close();
            }
            catch (WebException webExcp)
            {
                htmlFichaTrem = webExcp.Status.ToString();
            }

            return htmlFichaTrem.ToString();
        }

        public string RecuperaHtmlMercadoriaPerigosa()
        {
            var perguntasMercadorias = _checklistFichaTremService.ObterPerguntasChecklistPorTipo("M").ToList();

            ViewData["totalPerguntasDocumento"] = perguntasMercadorias.Count(doc => doc.IdAgrupador.Equals(1));
            ViewData["totalPerguntasCondObrigatoria"] = perguntasMercadorias.Count(cond => cond.IdAgrupador.Equals(2));
            ViewData["totalPerguntasLoco"] = perguntasMercadorias.Count(perg => perg.IdAgrupador.Equals(3));

            if (perguntasMercadorias.Count == 0)
            {
                perguntasMercadorias = new List<PerguntaCheckListDto>();
            }

            ViewData["perguntasMercadorias"] = perguntasMercadorias;
            var viewString = new StringBuilder();
            viewString.PageBreak();
            viewString.AppendLine(View("PaginasImpressao/MercadoriaPerigosa").Capture(ControllerContext));
            return viewString.ToString();
        }

        public string RecuperaHtmlChecklistTrens()
        {
            var papeleta = _checklistFichaTremService.ObterPerguntasChecklistPorTipo("P").ToList();

            if (papeleta.Count == 0)
            {
                papeleta = new List<PerguntaCheckListDto>();
            }

            ViewData["Papeleta"] = papeleta;
            var viewString = new StringBuilder();
            viewString.PageBreak();
            viewString.AppendLine(View("PaginasImpressao/PapeletaChecklistTrens").Capture(ControllerContext));
            return viewString.ToString();
        }


        public bool VerificarExisteMercadoriaPerigosa(int idTrem)
        {
            var retorno = true;

            return retorno;
        }

        #endregion

        #region Métodos/Actions da modal de Perguntas CheckList

        //Todo: Habiliar após script [Autorizar(Transacao = "CONSUFICHAACOMPCIRCTREM", Acao = "Pesquisar")]
        public JsonResult ObterConsultaPerguntasChecklist(DetalhesPaginacaoWeb detalhesPaginacaoWeb, int? idAgrupador, string tipoPergunta)
        {

            var resultado = _checklistFichaTremService.ObterConsultaPerguntasChecklist(detalhesPaginacaoWeb, idAgrupador, tipoPergunta);
            var jsonData = Json(
                new
                {
                    Items = resultado.Items.Select(i => new
                    {
                        i.IdPergunta,
                        i.IdAgrupador,
                        i.OrdemPergunta,
                        i.Pergunta
                    }),
                    Total = resultado.Total
                });

            return jsonData;
        }

        /// <summary>
        /// Salvar/Altera Pergunta CheckList
        /// </summary>
        /// <param name="perguntaCheckListDto"> Pergunta CheckList DTO</param>
        /// <returns> retorna Json true ou false</returns>
        public JsonResult SalvarPerguntaCheckList(PerguntaCheckListDto perguntaCheckListDto)
        {
            if (perguntaCheckListDto.Pergunta.Length > 500)
            {
                return Json(new
                {
                    Message = "Excedeu o limite de 500 caracteres",
                    Result = false
                });
            }


            try
            {
                _checklistFichaTremService.SalvarPergunta(perguntaCheckListDto);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Message = ex.Message,
                    Result = false
                });
            }

            return Json(new
            {
                Message = "Pergunta CheckList Salva/Alterada com Sucesso.",
                Result = true
            });
        }


        /// <summary>
        /// Deleta pergunta CHECKLIST
        /// </summary>
        /// <param name="investigacaoIncidenteDto"> Investigação de Incidente DTO</param>
        /// <returns> retorna Json true ou false</returns>
        public JsonResult DeletaPerguntaCheckList(int idPerguntaCheckList)
        {
            try
            {
                _checklistFichaTremService.ExcluirPergunta(idPerguntaCheckList);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Message = ex.Message,
                    Result = false
                });
            }

            return Json(new
            {
                Message = "Pergunta CheckList removida com Sucesso.",
                Result = true
            });
        }

        public List<PerguntaCheckListDto> ObterPerguntasChecklistPorTipo(string tipo)
        {
            var perguntas = _checklistFichaTremService.ObterPerguntasChecklistPorTipo(tipo).ToList();

            return perguntas;
        }

        #endregion
    }
}
