﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Translogic.Modules.Core.Domain.Services.Tfa.Interface;
using Translogic.Core.Infrastructure.Web;
using ALL.Core.Dominio;
using Translogic.Modules.Core.Domain.Model.Tfa;
using Translogic.Modules.Core.Infrastructure;
using Translogic.Modules.Core.Domain.Services.ControlePerdas.Interface;
using Translogic.Modules.Core.Domain.Model.ControlePerdas;
using Translogic.Core.Infrastructure.Extensions;
using System.IO;
using Translogic.Modules.Core.Domain.Model.Dto.ControlePerdas;
using Translogic.Core.Infrastructure.Web.Filters;
using Translogic.Modules.Core.Domain.Model.Seguro;
using Translogic.Modules.Core.Util;

namespace Translogic.Modules.Core.Controllers.Tfa
{
    /// <summary>
    /// Classe controller para gestão de parâmetros para Recomendação automática de Manutenção de Vagão no módulo de Controle de Perdas
    /// </summary>
    public class RecomendacaoVagaoController : BaseSecureModuleController
    {

        private readonly IControlePerdasService _controlePerdasService;

        public RecomendacaoVagaoController(IControlePerdasService controlePerdasService)
        {
            _controlePerdasService = controlePerdasService;
        }

        #region ActionViews
        /// <summary>
        /// Retorna a view de Recomendação de Vagão
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult RecomendacaoVagao()
        {
            try
            {
                return View("~/Views/ControlePerdas/RecomendacaoVagao.aspx");
            }
            catch (Exception)
            {
                return MensagemRetorno(false, "Ocorreu um erro ao abrir a tela de Configurações  de E-mails de Terminais. Contate o administrador do sistema.");
            }
        }


        /// <summary>
        /// Obtem a lista de Parâmetros de Recomendação de Vagão
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Autorizar(Transacao = "RECOMENDACAOVAGAO", Acao = "Pesquisar")]
        public JsonResult ObterListagemRecomendacaoVagao()
        {
            try
            {
                IList<RecomendacaoVagao> result = _controlePerdasService.ObterListagemRecomendacaoVagao();

                return Json(new
                {
                    Items = result.Select(t => new
                    {
                        Id = t.Id,
                        IdCausaSeguro = t.CausaSeguro.Id,
                        DescricaoCausaSeguro = t.CausaSeguro.Descricao,
                        Recomendacao = t.Recomendacao,
                        Tolerancia = t.Tolerancia
                    })
                });
            }
            catch (Exception)
            {
                return MensagemRetorno(false, "Ocorreu um erro ao recuperar a listagem de Causas de Danos para Recomendação de Manutenção de Vagão. Contate o administrador do sistema.");
            }

        }

        /// <summary>
        /// Obtem a lista de Parâmetros de Tolerâncias e respectivos emails para Recomendação de Vagão
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Autorizar(Transacao = "RECOMENDACAOVAGAO", Acao = "Pesquisar")]
        public JsonResult ObterListagemRecomendacaoTolerancia()
        {
            try
            {
                IList<RecomendacaoTolerancia> result = _controlePerdasService.ObterListagemRecomendacaoTolerancia();

                return Json(new
                {
                    Items = result.Select(t => new
                    {
                        Id = t.Id,
                        CodMalha = t.CodMalha,
                        Valor = t.Valor,
                        Email = t.Email
                    })
                });

            }
            catch (Exception)
            {
                return MensagemRetorno(false, "Ocorreu um erro ao recuperar a listagem de Tolerâncias para Recomendação de Manutenção de Vagão. Contate o administrador do sistema.");
            }

        }

        /// <summary>
        /// Salva as alterações de Parâmetros de Recomendação de Manutenção de Vagão
        /// </summary>
        /// <param name="parametrosRecomendacoes">objeto com as listas de tolerâncias, emails e parâmetros de recomendações</param>
        /// <returns></returns>
        [HttpPost]
        [Autorizar(Transacao = "RECOMENDACAOVAGAO", Acao = "Editar")]
        [JsonFilter(Param = "recomendacoes", JsonDataType = typeof(ParametroRecomendacaoVagaoDto))]
        public JsonResult SalvarRecomendacoes(ParametroRecomendacaoVagaoDto parametrosRecomendacoes)
        {            
            try
            {
                IList<RecomendacaoVagao> listaRecomendacoes = new List<RecomendacaoVagao>();
                IList<RecomendacaoTolerancia> listaTolerancias = parametrosRecomendacoes.Tolerancias;

                var retornoValidacao = ValidarRecomendacaoTolerancia(listaTolerancias);

                if (retornoValidacao != null)
                    return retornoValidacao;

                if (parametrosRecomendacoes.Recomendacoes != null)
                {
                    listaRecomendacoes = converterRecomendacaoVagaoDto(parametrosRecomendacoes.Recomendacoes);
                }

                _controlePerdasService.SalvarRecomendacoes(listaRecomendacoes, listaTolerancias);

                return MensagemRetorno(true, "Configurações de Recomendações de Vagões salvas com sucesso");

            }
            catch (Exception)
            {
                return MensagemRetorno(false, "Ocorreu um erro ao salvar as configurações de Recomendações de Vagões. Contate o administrador do sistema.");
            }

        }

        /// <summary>
        /// Converte/adapta uma List<RecomendacaoVagaoDto> para IList<RecomendacaoVagao> DE/PARA
        /// </summary>
        /// <param name="recomendacoes">lista de RecomendacaoVagaoDto</param>
        /// <returns>lista de RecomendacaoVagao</returns>
        private IList<RecomendacaoVagao> converterRecomendacaoVagaoDto(List<RecomendacaoVagaoDto> recomendacoes)
        {
            IList<RecomendacaoVagao> listaRecomendacoes = new List<RecomendacaoVagao>();

            if (recomendacoes != null)
            {
                foreach (var item in recomendacoes)
                {
                    listaRecomendacoes.Add(BuildRecomendacaoVagao(item));
                }
            }

            return listaRecomendacoes;
        }


        /// <summary>
        /// Método builder para criar/instanciar um objeto RecomendacaoVagao a partir de RecomendacaoVagaoDto
        /// </summary>
        /// <param name="recomendacaoDto">Dto com os dados para criação do objeto</param>
        /// <returns>Instância de RecomendacaoVagao</returns>
        private RecomendacaoVagao BuildRecomendacaoVagao(RecomendacaoVagaoDto recomendacaoDto){
            RecomendacaoVagao recomendacao = null;

            if (recomendacaoDto != null)
            {
                recomendacao = new RecomendacaoVagao
                {
                    Id = recomendacaoDto.Id.Value,
                    CausaSeguro = new CausaSeguro { Id = recomendacaoDto.IdCausaSeguro },
                    Recomendacao = recomendacaoDto.Recomendacao,
                    Tolerancia = recomendacaoDto.Tolerancia
                };
            }

            return recomendacao;
        }
        
        /// <summary>
        /// método útil comum para criação de mensagem de retorno no formato JsonResult
        /// </summary>
        /// <param name="success">informa se a mensagem é flag de sucesso/falha de operação</param>
        /// <param name="message">mensagem à ser retornada</param>
        /// <returns></returns>
        private JsonResult MensagemRetorno(Boolean success, string message)
        {
            return Json(new
            {
                success = success,
                message = message
            });
        }

        /// <summary>
        /// Valida os dados de RecomendacaoTolerancia
        /// </summary>
        /// <param name="listaTolerancias">lista de RecomendacaoTolerancia</param>
        /// <returns>retorna mensagem JsonResult com o status de falha ou sucesso na validação</returns>
        private JsonResult ValidarRecomendacaoTolerancia(IList<RecomendacaoTolerancia> listaTolerancias)
        {
            foreach (var item in listaTolerancias)
            {
                if (item == null)
	            {
		            return MensagemRetorno(false, "Dados de Tolerância inválidos!");
	            }
            
                if (string.IsNullOrEmpty(item.Email) || !EmailUtil.validarEmail(item.Email))
                {
                    return MensagemRetorno(false, "E-mail inválido!");
                }
            }

            return null;
        }

        #endregion
    }
}
