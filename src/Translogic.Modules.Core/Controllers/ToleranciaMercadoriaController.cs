﻿namespace Translogic.Modules.Core.Controllers.Operacao
{
    using System;
    using System.Linq;
    using System.Web.Mvc;
    using Translogic.Core.Infrastructure.Web;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais;
    using Translogic.Modules.Core.Domain.Model.Tolerancia;
    using Translogic.Modules.Core.Domain.Services.Tolerancia.Interface;

    public class ToleranciaMercadoriaController : BaseSecureModuleController
    {
        private readonly IToleranciaService _toleranciaService;

        public ToleranciaMercadoriaController(IToleranciaService toleranciaService)
        {
            _toleranciaService = toleranciaService;
        }

        #region ActionViews

        /// <summary>
        /// Painel de OS de Limpeza - Visualização
        /// </summary>
        //[Autorizar(Transacao = "CADTOLERANCIA")]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Formulario(string id, int? idMercadoria, decimal? tolerancia, string evento)
        {
            ViewData["id"] = id;
            ViewData["idMercadoria"] = idMercadoria;
            ViewData["tolerancia"] = tolerancia;
            ViewData["evento"] = evento;

            return View();
        }

        [HttpPost]
        public bool ConfirmaFormulario(ToleranciaMercadoriaFormDto model)
        {
            var toleranciaMercadoria = GetInstance(model);

            if (model.Evento.ToLower() != "excluir")
            {
                _toleranciaService.Salvar(toleranciaMercadoria);
            } 
            else
            {
                if (model.Id == 0)
                    throw new Exception("Id para excluir não fornecido");

                _toleranciaService.Excluir(Convert.ToInt32(model.Id));
            }
            return true;
        }
        #endregion

        //[Autorizar(Transacao = "CADTOLERANCIA")]
        public JsonResult PesquisaTolerancias(DetalhesPaginacaoWeb detalhesPaginacaoWeb, string dataInicial, string dataFinal, string mercadoria)
        {
            ViewData["dataInicial"] = dataInicial;
            ViewData["dataFinal"] = dataFinal;
            ViewData["mercadoria"] = mercadoria;

            if (mercadoria == "Todos")
                mercadoria = "";

            var resultado = _toleranciaService.ObterConsulta(detalhesPaginacaoWeb,
                                                                                dataInicial,
                                                                                dataFinal,
                                                                                mercadoria);
            var jsonData = Json(
                new
                {
                    Items = resultado.Items.Select(i => new
                    {
                        Id = i.Id,
                        IdMercadoria = i.IdMercadoria,
                        Mercadoria = i.Mercadoria,
                        Tolerancia = i.Tolerancia.ToString().Replace(".",","),
                        Data = i.VersionDate != null ? i.VersionDate.ToString("dd/MM/yyyy") : string.Empty,
                        Usuario = i.Usuario                        
                    }),
                    Total = resultado.Total
                });
            return jsonData;
        }

        public JsonResult ObterMercadorias()
        {
            var resultado = _toleranciaService.ListMercadorias();

            resultado.Insert(0, new Mercadoria() { Id = 0, Codigo = "Todos" }); 

            var jsonData = Json(
                new
                {
                    Items = resultado.Select(i => new
                    {
                        IdMercadoria = i.Id,
                        Nome = i.Codigo=="Todos" ? "Todos" : i.Codigo + " - " + i.DescricaoResumida
                    }),
                    Total = resultado.Count
                });
            return jsonData;
        }

        private ToleranciaMercadoria GetInstance(ToleranciaMercadoriaFormDto model)
        {
            var toleranciaMercadoria = _toleranciaService.ObterInstancia(Convert.ToInt32(model.Id));
            if (model.Id == 0)
            {
                toleranciaMercadoria.Mercadoria.Id = Convert.ToInt32(model.IdMercadoria);
            }
            
            toleranciaMercadoria.Tolerancia = model.Tolerancia;
            toleranciaMercadoria.Usuario = UsuarioAtual.Codigo;

            return toleranciaMercadoria;
        }
    }
}