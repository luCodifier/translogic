﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Translogic.Core.Infrastructure.Web.ContentResults;
using Translogic.Modules.Core.Domain.Model.Dto;
using Translogic.Modules.Core.Domain.Model.Bolar.Repositories;
using ALL.Core.Dominio;

namespace Translogic.Modules.Core.Controllers
{
    public class RecebimentoArquivoEdiController : BaseSecureModuleController
    {
        private readonly IBolarBoRepository _bolarBoRepository;
        private readonly IBolarBoImpCfgRepository _bolarBoImpCfgRepository;

        public RecebimentoArquivoEdiController(IBolarBoRepository bolarBoRepository, IBolarBoImpCfgRepository bolarBoImpCfgRepository)
        {
            _bolarBoRepository = bolarBoRepository;
            _bolarBoImpCfgRepository = bolarBoImpCfgRepository;
        }

        //
        // GET: /RecebimentoArquivoEdi/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ObterStatusProcessamento()
        {
            /*
             *  N - não despachados
             *  E - Com erros
             *  D - Despachado
             *  C - Cancelado
             *  R - Rejeitado
             *  Q - Aguardando confirmação
             *  J - Exclusão de duplicidades
             */

            var itens = new List<dynamic>
                            {
                                new {Id = "N",Descricao = "Não Despachados" },
                                new {Id = "E",Descricao = "Com Erros" },
                                new {Id = "D",Descricao = "Despachados" },
                                new {Id = "C",Descricao = "Cancelado" },
                                new {Id = "R",Descricao = "Rejeitado" },
                                new {Id = "Q",Descricao = "Ag. Confirmação" },
                                new {Id = "J",Descricao = "Exclusão Duplicidade" },
                            };

            return Json(new
            {
                Items = itens.Select(e => new
                {
                    e.Id,
                    e.Descricao
                })
            });


        }

        public ActionResult ObterCliente()
        {
            var lista = _bolarBoImpCfgRepository.ObterTodasAtivas();

            return Json(new
            {
                Items = lista.Select(item => new
                {
                    Id = item.CodigoEmpresa,
                    Descricao = item.DescricaoEmpresa.ToUpper()
                })
            });
        }

        public ActionResult Pesquisar(DetalhesPaginacao detalhesPaginacao, DateTime dtInicial, DateTime dtFinal, string fluxoComercial, string origem, string destino, string situacao, string lstVagoes, string cliente363)
        {
            var result = _bolarBoRepository.PesquisarRecebimentoArquivoEdi(detalhesPaginacao, dtInicial, dtFinal, fluxoComercial,
                                                              origem, destino, situacao, lstVagoes, cliente363);

            return Json(new
            {
                result.Total,
                Items = result.Items.Select(e => new
                {
                    e.VagaoId,
                    e.Cliente363,
                    e.Conteiner,
                    e.DataHoraCadastro,
                    e.Destino,
                    e.Expedidor,
                    e.Fluxo,
                    e.Mercadoria,
                    e.MultiploDespacho,
                    e.NotasFicais,
                    e.Observacao,
                    e.Origem,
                    e.PesoReal,
                    e.PesoTotal,
                    e.Recebedor,
                    e.Serie,
                    e.Situacao,
                    e.Vagao,
                    e.VolumeTotal
                })
            });
        }
    }
}
