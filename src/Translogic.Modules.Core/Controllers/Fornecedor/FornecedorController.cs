﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Translogic.Core.Infrastructure.Web;
using Translogic.Modules.Core.Domain.Model.Dto;
using Translogic.Modules.Core.Domain.Model.TipoServicos.Repositories;
using Translogic.Modules.Core.Domain.Services.Fornecedor.Interface;
using Translogic.Modules.Core.Domain.Services.LocaisFornecedor.Interface;
using Translogic.Modules.Core.Domain.Services.Via.Interface;
using Translogic.Modules.Core.Infrastructure;
using Translogic.Modules.Core.Domain.Model.Fornecedor;

namespace Translogic.Modules.Core.Controllers.Fornecedor
{
    public class FornecedorController : BaseSecureModuleController
    {
        private readonly ITipoServicoRepository _TipoService;
        private readonly IFornecedorOsService _fornecedorOsService;
        private readonly ILocalFornecedorService _localFornecedorService;
        private readonly IEstacaoMaeService _EstacaoMaeService;

        public FornecedorController(ITipoServicoRepository TipoService,
                                    IFornecedorOsService fornecedorOsService,
                                    ILocalFornecedorService localFornecedorService,
                                    IEstacaoMaeService EstacaoMaeService)
        {
            _TipoService = TipoService;
            _fornecedorOsService = fornecedorOsService;
            _localFornecedorService = localFornecedorService;
            _EstacaoMaeService = EstacaoMaeService;
        }

        // GET: /Fornecedor/
        //[Autorizar(Transacao = " SRVCADFORNEC")]
        public ActionResult Index()
        {
            return View();
        }
        //[Autorizar(Transacao = " SRVCADFORNEC")]
        public ActionResult CriarFornecedor(int? idFornecedor)
        {
            ViewData["usuario"] = UsuarioAtual.Codigo;
            if (idFornecedor.HasValue)
            {
                var fornecedor = _fornecedorOsService.ObterPorId(idFornecedor.Value);
                ViewData["idFornecedor"] = fornecedor.IdFornecedorOs;
                ViewData["NomeFornecedor"] = fornecedor.Nome;
                ViewData["Ativo"] = fornecedor.Ativo;
            }
            else
            {
                ViewData["idFornecedor"] = 0;
                ViewData["Ativo"] = true;
            }
            return View();
        }

        /// <summary>
        /// Salvar Locais Servicos do fornecedor
        /// </summary>
        /// <returns></returns>
        public JsonResult SalvarLocaisServicosFornecedorBD(FornecedorOsDto fornecedorOsDto)
        {
            var retorno = "Fornecedor editado com sucesso!";
            var existe = false;
            if (VerificarExisteFornecedor(Convert.ToInt32(fornecedorOsDto.IdFornecedorOs), fornecedorOsDto.Nome))
            {
                existe = true;
                retorno = "Fornecedor já cadastrado. Favor verificar!";
            }
            else if (!existe)
            {
                _fornecedorOsService.InserirAtualizarFornecedor(fornecedorOsDto, UsuarioAtual.Codigo);
                var resultado = _TipoService.ObterTipoServico();

                if (fornecedorOsDto.IdFornecedorOs == 0)
                    retorno = "Fornecedor cadastrado com sucesso!";
            }
            return Json(
                   new
                   {
                       Success = retorno,

                   });
        }

        /// <summary>
        /// Retornar tipos 
        /// </summary>
        /// <returns></returns>
        public JsonResult ObterTipoServico()
        {
            var resultado = _TipoService.ObterTipoServico();

            var jsonData = Json(
                new
                {
                    Items = resultado.Select(i => new
                    {
                        i.IdTipoServico,
                        i.DescricaoServico
                    }),
                    Total = resultado.Count
                });
            return jsonData;
        }

        /// <summary>
        /// Retornar tipos 
        /// </summary>
        /// <returns></returns>
        public JsonResult ObterTipoServicoCriarFornecedor()
        {
            var resultado = _TipoService.ObterTipoServico();
            //Remove Todos    
            resultado.RemoveAt(0);
            var jsonData = Json(
                new
                {
                    Items = resultado.Select(i => new
                    {
                        i.IdTipoServico,
                        i.DescricaoServico
                    }),
                    Total = resultado.Count
                });
            return jsonData;
        }

        /// <summary>
        /// Retornar Tipos Servico
        /// </summary>
        /// <returns></returns>
        public JsonResult ObterGridFornecedores(DetalhesPaginacaoWeb detalhesPaginacaoWeb, string nomeFornecedor, string local, int? idServico, string Ativo)
        {
            var resultado = _fornecedorOsService.ObterFornecedoresOsHabilitados(detalhesPaginacaoWeb, nomeFornecedor, local, idServico, Ativo);

            var jsonData = Json(
                new
                {
                    Items = resultado.Items.Select(i => new
                    {
                        //Campos
                        i.IdFornecedorOs,
                        Nome = i.Nome,
                        i.ConcaLocalServico,
                        DtRegistro = i.DtRegistro.ToString("dd/MM/yyyy HH:mm"),
                        i.Login
                    }),
                    Total = resultado.Total
                });
            return jsonData;
        }

        /// <summary>
        /// Retornar Tipos Servico
        /// </summary>
        /// <returns></returns>
        public JsonResult ObterGridLocaisServicosFornecedores(DetalhesPaginacaoWeb detalhesPaginacaoWeb, int idFornecedor)
        {
            var resultado = _localFornecedorService.ObterLocaisFornecedorPorId(detalhesPaginacaoWeb, idFornecedor);

            var jsonData = Json(
                new
                {
                    Items = resultado.Items.Select(i => new
                    {
                        //Campos
                        i.IdFornecedorOs,
                        i.idTipoServico,
                        i.Servico,
                        i.Local
                    }),
                    Total = resultado.Total
                });
            return jsonData;
        }

        /// <summary>
        /// Verificar se fornecedor já está cadastrado
        /// </summary>
        /// <param name="nomeFornecedor"></param>

        public bool VerificarExisteFornecedor(int idFornecedor, string nomeFornecedor)
        {
            return _fornecedorOsService.ExisteFornecedorOsPorNomeId(nomeFornecedor, idFornecedor);
        }

        [HttpPost]
        public string VerificarExisteLocal(string local)
        {
            var retorno = string.Empty;
            try
            {
                var estacaoMae = _EstacaoMaeService.ObterPorCodigo(local.ToUpper());

                if (estacaoMae == null)
                {
                    retorno = "O Local informado não existe!";
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possivel cadastrar fornecedor: " + ex.Message);
            }

            return retorno;
        }
    }
}

