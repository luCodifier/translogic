namespace Translogic.Modules.Core.Controllers
{
    using System.Linq;
    using System.Security;
    using System.Web.Mvc;
    using Domain.Model.Acesso;
    using Translogic.Core.Infrastructure.Web;
    using Translogic.Core.Infrastructure.Web.Filters;

    /// <summary>
    /// Controller base para os m�dulos, para acesso restrito.
    /// </summary>
    [AuthenticationFilter(Order = 1)]
    [HandlerAjaxErrorFilterAttribute]
    [HandleError(ExceptionType = typeof(SecurityException))]
    public class BaseSecureModuleController : BaseModuleController
    {
        /// <summary>
        /// Usu�rio logado
        /// </summary>
        public Usuario UsuarioAtual
        {
            get
            {
                return ((UsuarioIdentity)HttpContext.User.Identity).Usuario;
            }
        }

        public BaseSecureModuleController()
        {
            Sis.ChecarConexao();
        }

        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            base.OnActionExecuted(filterContext);

            // esse c�digo corrige, em todas as telas, o problema de aspas duplas
            //var controller = filterContext.Controller;
            //if (controller != null && controller.ViewData != null && controller.ViewData.Any())
            //{
            //    foreach (var key in controller.ViewData.Keys)
            //    {
            //        var str = controller.ViewData[key] as string;
            //        if (!string.IsNullOrWhiteSpace(str) && str.Contains("\""))
            //            controller.ViewData[key] = HttpUtility.HtmlEncode(str);
            //    }
            //}
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            Sis.LogException(filterContext.Exception, "Erro em BaseSecureModuleController.OnException");
            base.OnException(filterContext);
        }

    }
}