namespace Translogic.Modules.Core.Controllers
{
	using System;
	using System.Net;
	using System.Web.Mvc;
	using Domain.Model.Acesso;
	using Domain.Model.Estrutura.Repositories;
	using Domain.Services.Acesso;
	using Infrastructure;
	using Translogic.Core.Infrastructure.Web;

	/// <summary>
	/// Controller de usu�rio
	/// </summary>
	public class UsuarioController : BaseModuleController
	{
		#region ATRIBUTOS
		private readonly IEmpresaFerroviaRepository _empresaFerroviaRepository;
		private readonly UsuarioService _service;
		#endregion

		#region CONSTRUTORES
		/// <summary>
		/// Construtor injetando os servi�os
		/// </summary>
		/// <param name="service">Servi�o de usu�rio</param>
		/// <param name="empresaFerroviaRepository">Servi�o de empresa ferrovi�ria</param>
		public UsuarioController(UsuarioService service, IEmpresaFerroviaRepository empresaFerroviaRepository)
		{
			_service = service;
			_empresaFerroviaRepository = empresaFerroviaRepository;
		}

		#endregion

		#region M�TODOS

		/// <summary>
		/// Action default do controller
		/// </summary>
		/// <returns>View default</returns>
		public virtual ActionResult Index()
		{
			ViewData["empresas"] = _empresaFerroviaRepository.ObterTodos();

			return View();
		}

		/// <summary>
		/// Salva os dados do usu�rio
		/// </summary>
		/// <param name="usuario">Objeto Usu�rio</param>
		/// <returns>Resultado Json</returns>
		public virtual JsonResult Salvar(Usuario usuario)
		{
			try
			{
				_service.Salvar(usuario);
				return Json(new { success = true });
			}
			catch (Exception e)
			{
				Logger.Error("O usu�rio n�o pode ser salvo", e);
				Response.StatusCode = (int) HttpStatusCode.InternalServerError;
				return Json(new { success = false, message = "O usu�rio n�o pode ser salvo" });
			}
		}

		#endregion
	}
}