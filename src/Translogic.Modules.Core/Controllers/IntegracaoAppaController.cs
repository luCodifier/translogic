﻿namespace Translogic.Modules.Core.Controllers
{

    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Web.Mvc;
    using ALL.Core.Extensions;
    using Translogic.Core.Infrastructure.Web;
    using Translogic.Core.Infrastructure.Web.ContentResults;
    using Translogic.Modules.Core.Domain.Model.Dto;

    using OfficeOpenXml;
    using OfficeOpenXml.Style;

    using Translogic.Modules.Core.Domain.Services.Appa;


    public class IntegracaoAppaController : BaseSecureModuleController
    {
        private readonly IntegracaoAppaService _integracaoAppaService;

        public IntegracaoAppaController(IntegracaoAppaService integracaoAppaService)
        {
            _integracaoAppaService = integracaoAppaService;
        }

        #region ActionResults

        public ActionResult Consultar()
        {
            return View();
        }

        /// <summary>
        /// Retorna a tela para informar os códigos dos vagões
        /// </summary>
        /// <returns>Action result com a tela</returns>
        public ActionResult FormInformarVagoes()
        {
            return View();
        }

        /// <summary>
        /// Retorna se os vagões estão válidos
        /// </summary>
        /// <param name="listaVagoes">Lista de vagões</param>
        /// <returns>Action result com a tela</returns>
        public ActionResult ValidarVagoes(List<string> listaVagoes)
        {
            Dictionary<string, KeyValuePair<bool, string>> result = _integracaoAppaService.VerificarListaVagoes(listaVagoes);

            return Json(new
            {
                Items = result.Select(c => new
                {
                    Vagao = c.Key,
                    Erro = c.Value.Key,
                    Mensagem = c.Value.Value
                }),
                success = true
            });
        }

        public ActionResult ExportarExcel(string dataInicial, string dataFinal, string origem, string destino, string vagoes, int situacaoEnvioStatus, int situacaoRecebimentoStatus, string lote)
        {
            DateTime dataInicialFiltro = DateTime.Now;
            if (!string.IsNullOrEmpty(dataInicial))
                dataInicialFiltro = DateTime.ParseExact(dataInicial, "dd/MM/yyyy", CultureInfo.InvariantCulture);

            DateTime dataFinalFiltro = DateTime.Now;
            if (!string.IsNullOrEmpty(dataFinal))
                dataFinalFiltro = DateTime.ParseExact(dataFinal, "dd/MM/yyyy", CultureInfo.InvariantCulture);

            var resultados = _integracaoAppaService.ObterIntegracoesExcel(dataInicialFiltro, dataFinalFiltro, origem, destino, vagoes, situacaoEnvioStatus, situacaoRecebimentoStatus, lote);


            // Creating an instance 
            // of ExcelPackage 
            ExcelPackage excel = new ExcelPackage();

            int recordIndex = 1;

            string situacaoEnvioStatusFilter = String.Empty;
            string situacaoRecebimentoStatusFilter = String.Empty;

            switch(situacaoEnvioStatus)
            {
                case 0:
                    situacaoEnvioStatusFilter = "Erro";
                    break;
                case 1:
                    situacaoEnvioStatusFilter = "Pendente";
                    break;
                case 2:
                    situacaoEnvioStatusFilter = "Processado";
                    break;
                default:
                    situacaoEnvioStatusFilter = "Todos";
                    break;
            }

            switch (situacaoRecebimentoStatus)
            {
                case 0:
                    situacaoRecebimentoStatusFilter = "Erro";
                    break;
                case 1:
                    situacaoRecebimentoStatusFilter = "Pendente";
                    break;
                case 2:
                    situacaoRecebimentoStatusFilter = "Processado";
                    break;
                default:
                    situacaoRecebimentoStatusFilter = "Todos";
                    break;
            }



            // name of the sheet 
            var workSheet = excel.Workbook.Worksheets.Add("Relatorio");

            workSheet.Cells[recordIndex, 1].Value = "RELATÓRIO DE INTEGRAÇÃO APPA";
            workSheet.Cells["A1:J1"].Merge = true;
            workSheet.Cells[recordIndex, 1].Style.Font.Bold = true;
            workSheet.Cells[recordIndex, 1].Style.Font.Size = 16;

            recordIndex += 2;
            workSheet.Cells[recordIndex, 1].Value = "Filtros: ";
            workSheet.Cells[recordIndex, 1].Style.Font.Bold = true;

            recordIndex += 2;

            // setting the properties 
            // of the work sheet  
            workSheet.TabColor = System.Drawing.Color.Black;
            workSheet.DefaultRowHeight = 12;

            // Setting the properties 
            // of the first row 
            workSheet.Row(recordIndex).Height = 20;
            workSheet.Row(recordIndex).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            workSheet.Row(recordIndex).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            workSheet.Row(recordIndex).Style.Font.Bold = true;
            workSheet.Row(recordIndex).Style.Font.Size = 13;
            workSheet.Row(recordIndex).Style.Fill.PatternType = ExcelFillStyle.Solid;
            workSheet.Row(recordIndex).Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightGray);

            // Header of the Excel sheet 
            workSheet.Cells[recordIndex, 1].Value = "Lote";
            workSheet.Cells[recordIndex, 2].Value = "Data Geração";
            workSheet.Cells[recordIndex, 3].Value = "Status Envio";
            workSheet.Cells[recordIndex, 4].Value = "Status Recebimento";
            workSheet.Cells[recordIndex, 5].Value = "Vagão";
            workSheet.Cells[recordIndex, 6].Value = "Fluxo";
            workSheet.Cells[recordIndex, 7].Value = "Origem";
            workSheet.Cells[recordIndex, 8].Value = "Destino";
            workSheet.Cells[recordIndex, 9].Value = "Previsão Chegada";
            workSheet.Cells[recordIndex, 10].Value = "Mensagem";

            recordIndex++;

            foreach (var item in resultados)
            {
                workSheet.Cells[recordIndex, 1].Value = item.Lote.ToString(CultureInfo.InvariantCulture);
                workSheet.Cells[recordIndex, 2].Value = item.DataCriacao.ToString("dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);

                if (item.DataEnvio.HasValue)
                {
                    if (item.SucessoEnvioBool)
                    {
                        workSheet.Cells[recordIndex, 3].Style.Font.Name = "Webdings";
                        workSheet.Cells[recordIndex, 3].Value = "n";
                        workSheet.Cells[recordIndex, 3].Style.Font.Color.SetColor(System.Drawing.Color.Green);
                        workSheet.Cells[recordIndex, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    } else
                    {
                        workSheet.Cells[recordIndex, 3].Style.Font.Name = "Webdings";
                        workSheet.Cells[recordIndex, 3].Value = "n";
                        workSheet.Cells[recordIndex, 3].Style.Font.Color.SetColor(System.Drawing.Color.Red);
                        workSheet.Cells[recordIndex, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    }

                    if (item.SucessoRetornoBool)
                    {
                        workSheet.Cells[recordIndex, 4].Style.Font.Name = "Webdings";
                        workSheet.Cells[recordIndex, 4].Value = "n";
                        workSheet.Cells[recordIndex, 4].Style.Font.Color.SetColor(System.Drawing.Color.Green);
                        workSheet.Cells[recordIndex, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    }
                    else
                    {
                        workSheet.Cells[recordIndex, 4].Style.Font.Name = "Webdings";
                        workSheet.Cells[recordIndex, 4].Value = "n";
                        workSheet.Cells[recordIndex, 4].Style.Font.Color.SetColor(System.Drawing.Color.Red);
                    }
                }
                else
                {
                    //sucesso envio
                    workSheet.Cells[recordIndex, 3].Style.Font.Name = "Webdings";
                    workSheet.Cells[recordIndex, 3].Value = "n";
                    workSheet.Cells[recordIndex, 3].Style.Font.Color.SetColor(System.Drawing.Color.Orange);
                    //sucesso retorno
                    workSheet.Cells[recordIndex, 4].Style.Font.Name = "Webdings";
                    workSheet.Cells[recordIndex, 4].Value = "n";
                    workSheet.Cells[recordIndex, 4].Style.Font.Color.SetColor(System.Drawing.Color.Orange);

                }

                //workSheet.Cells[recordIndex, 3].Value = item.DataEnvio.HasValue ? (item.SucessoEnvioBool ? "Processada" : "Erro") : "Pendente"; // 2 significa processada com sucesso, 0 com erro e 1 que está pendente
                //workSheet.Cells[recordIndex, 4].Value = item.DataEnvio.HasValue ? (item.SucessoRetornoBool ? "Processada" : "Erro") : "Pendente"; // 2 significa processada com sucesso, 0 com erro e 1 que está pendente
                workSheet.Cells[recordIndex, 5].Value = item.PlacaVagao ?? "";
                workSheet.Cells[recordIndex, 6].Value = item.CodigoFluxoComercial ?? "";
                workSheet.Cells[recordIndex, 7].Value = item.Origem ?? "";
                workSheet.Cells[recordIndex, 8].Value = item.Destino ?? "";
                workSheet.Cells[recordIndex, 9].Value = item.DataPrevistaChegada.ToString("dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                workSheet.Cells[recordIndex, 10].Value = item.MensagemRetorno ?? "";
                recordIndex++;
            }

            workSheet.Column(1).AutoFit();
            workSheet.Column(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            workSheet.Column(2).AutoFit();
            workSheet.Column(2).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            workSheet.Column(3).AutoFit();
            workSheet.Column(3).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            workSheet.Column(4).AutoFit();
            workSheet.Column(4).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            workSheet.Column(5).AutoFit();
            workSheet.Column(5).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            workSheet.Column(6).AutoFit();
            workSheet.Column(6).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            workSheet.Column(7).AutoFit();
            workSheet.Column(7).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            workSheet.Column(8).AutoFit();
            workSheet.Column(8).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            workSheet.Column(9).AutoFit();
            workSheet.Column(9).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            workSheet.Column(10).Width = 90;

            workSheet.Cells[3, 2].Value = $"De: {dataInicial} até: {dataFinal}{(!String.IsNullOrEmpty(origem) ? " | Origem: " + origem.ToUpper() : "")}{(!String.IsNullOrEmpty(destino) ? " | Destino: " + destino.ToUpper() : "")}{(!String.IsNullOrEmpty(vagoes) ? " | Vagões: " + vagoes : "")}{" | Status Envio: " + situacaoEnvioStatusFilter}{" | Status Receb: " + situacaoRecebimentoStatusFilter}{(!String.IsNullOrEmpty(lote) ? " | Lote: " + lote : "")}";

            workSheet.Cells["B3:J3"].Merge = true;
            workSheet.Cells["B3:J3"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;


            return File(excel.GetAsByteArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", string.Format("Integracao_APPA_{0}_{1}.xlsx", DateTime.Now.ToString("ddMMyyyy"), DateTime.Now.ToString("HHmmss")));
            //return this.Excel(string.Format("Integracao_APPA_{0}_{1}.xls", DateTime.Now.ToString("ddMMyyyy"), DateTime.Now.ToString("HHmmss")), dic, resultados);
        }

        #endregion

        #region JsonResults

        public JsonResult ObterIntegracoesConsulta(DetalhesPaginacaoWeb pagination, string dataInicial, string dataFinal, string origem, string destino, string vagoes, int situacaoEnvioStatus, int situacaoRecebimentoStatus, string lote)
        {
            try
            {
                DateTime dataInicialFiltro = DateTime.Now;
                if (!string.IsNullOrEmpty(dataInicial))
                    dataInicialFiltro = DateTime.ParseExact(dataInicial, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                DateTime dataFinalFiltro = DateTime.Now;
                if (!string.IsNullOrEmpty(dataFinal))
                    dataFinalFiltro = DateTime.ParseExact(dataFinal, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                var resultados = _integracaoAppaService.ObterIntegracoesConsulta(pagination, dataInicialFiltro, dataFinalFiltro, origem, destino, vagoes, situacaoEnvioStatus, situacaoRecebimentoStatus, lote);

                return Json(new
                {
                    Success = true,
                    Message = String.Empty,
                    resultados.Total,
                    Items = resultados.Items.Select(d => new
                    {
                        Id = d.Id,
                        Lote = d.Lote,
                        DataGeracao = d.DataCriacaoString,
                        DataEnvio = d.DataEnvioString,
                        SucessoEnvio = d.DataEnvio.HasValue ? (d.SucessoEnvioBool ? 2 : 0) : 1, // 2 significa processada com sucesso, 0 com erro e 1 que está pendente
                        MensagemErro = d.MensagemErro,
                        SucessoRetorno = d.DataEnvio.HasValue ? (d.SucessoRetornoBool ? 2 : 0) : 1, // 2 significa processada com sucesso, 0 com erro e 1 que está pendente
                        MensagemRetorno = d.MensagemRetorno,
                        AreaOperacional = d.AreaOperacionalEnvio,
                        Origem = d.Origem,
                        Destino = d.Destino,
                        FluxoComercial = d.CodigoFluxoComercial,
                        Vagao = d.PlacaVagao,
                        DataPrevistaChegada = d.DataPrevistaChegadaString,
                        PossuiXml = d.PossuiXmlBool
                    })
                });
            }
            catch (Exception ex)
            {
                return this.Json(new { Success = false, Message = ex.Message });
            }
        }



        [HttpGet]
        public ActionResult ObterXmlEnvio(string ids)
        {
            FileStreamResult fsr = null;

            try
            {
                var returnContent = _integracaoAppaService.ObterXmlEnvio(int.Parse(ids));

                if (returnContent != null)
                {
                    fsr = new FileStreamResult(returnContent, "text/xml");
                    fsr.FileDownloadName = "integracao-appa.xml";
                }

                return fsr;
            }
            catch (Exception ex)
            {
                return fsr;
            }
        }

        public JsonResult ReprocessarRegistro(int id)
        {
            try
            {
                _integracaoAppaService.ReprocessarRegistro(id);

                return Json(new { Success = true });
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = ex.Message });
            }
        }

        public JsonResult ReprocessarRegistros(List<string> itens)
        {
            try
            {
                Dictionary<string, string> result;
                result = _integracaoAppaService.ReprocessarRegistros(itens);

                return Json(new { Success = true });
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = ex.Message });
            }
        }

        #endregion

    }
}
