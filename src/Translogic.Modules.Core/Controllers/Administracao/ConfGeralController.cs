﻿using Translogic.Modules.Core.Infrastructure;

namespace Translogic.Modules.Core.Controllers.Administracao
{
    using System;
    using System.Linq;
    using System.Web.Mvc;
    using Domain.Services.Administracao;
    using Translogic.Core.Infrastructure.Web;
    using Domain.Model.Administracao;

    public class ConfGeralController : BaseSecureModuleController
    {
        private readonly ConfGeralService _configGeralService;

        public ConfGeralController(ConfGeralService configGeralService)
        {
            _configGeralService = configGeralService;
        }

        [Autorizar(Transacao = "CGRAL")]
        public ActionResult Index()
        {
            return View();
        }

        [Autorizar(Transacao = "CGRAL", Acao = "Incluir")]
        public ActionResult AdicionarConfiguracao()
        {
            return View();
        }

        [Autorizar(Transacao = "CGRAL", Acao = "Editar")]
        public ActionResult EditarConfiguracao(string id)
        {
            ViewData["id"] = string.Empty;
            ViewData["valor"] = string.Empty;
            ViewData["descricao"] = string.Empty;

            if (!string.IsNullOrEmpty(id))
            {
                var configuracao = _configGeralService.Obter(id);
                if (configuracao != null)
                {
                    ViewData["id"] = id;
                    ViewData["valor"] = configuracao.Valor;
                    ViewData["descricao"] = configuracao.Descricao;
                }
                else
                {
                    return Json(new { Success = false, Message = string.Format("Configuração: {0} não encontrada", id) });
                }
            }

            return View();
        }

        public ActionResult SalvarConfiguracao(string chave, string valor, string descricao, bool edicao)
        {
            try
            {
                return edicao ? EditarConfiguracao(chave, valor, descricao) : AddConfiguracao(chave, valor, descricao);
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, ex.Message });
            }
        }

        
        private ActionResult EditarConfiguracao(string chave, string valor, string descricao)
        {
            var configuracao = _configGeralService.Obter(chave);

            if (configuracao != null)
            {
                var valorAnterior = configuracao.Valor;
                configuracao.Valor = valor;
                configuracao.Descricao = descricao;
                
                if(!configuracao.IsValid())
                {
                    return Json(new { Success = false, Message = "Informe uma chave, valor e descrição válidos." });
                }

                _configGeralService.Atualizar(configuracao, UsuarioAtual.Codigo, valorAnterior);

                return Json(new { Success = true, Message = "Registro atualizado com sucesso" });
            }

            return Json(new { Success = false, Message = string.Format("Não existe uma configuração para a chave: {0}", chave) });


        }

        
        private ActionResult AddConfiguracao(string chave, string valor, string descricao)
        {
            var configuracao = _configGeralService.Obter(chave);

            if (configuracao != null)
            {
                return
                    Json(new { Success = false, Message = string.Format("Já existe uma configuração para a CHAVE: {0}", chave) });
            }

            configuracao = new ConfGeral(chave)
                               {
                                   Valor = valor,
                                   Descricao = descricao
                               };

            if (!configuracao.IsValid())
            {
                return Json(new { Success = false, Message = "Informe uma chave, valor e descrição válidos." });
            }
            
            _configGeralService.Inserir(configuracao, UsuarioAtual.Codigo);

            return Json(new { Success = true, Message = "Registro criado com sucesso" });
        }

        [Autorizar(Transacao = "CGRAL", Acao = "Pesquisar")]
        public JsonResult ObterConfiguracoes(DetalhesPaginacaoWeb pagination, string chave)
        {
            try
            {
                chave = string.IsNullOrWhiteSpace(chave) ? null : chave.ToUpper();
                var resultados = _configGeralService.ObterTodos(pagination, chave);

                var retorno = new
                {
                    resultados.Total,
                    Items = resultados.Items.Select(item => new
                    {
                        Id = item.Id,
                        Valor = item.Valor,
                        Descricao = item.Descricao,
                        Data = string.Format("{0:dd/MM/yyyy}", item.Data)
                    })
                };

                return Json(retorno);
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = ex.Message });
            }
        }

        public ActionResult DeletarConfiguracao(string chave)
        {
            try
            {
                if (string.IsNullOrEmpty(chave))
                {
                    return Json(new { Success = false, Message = "Informe a chave da configuração" });
                }

                var configuracao = _configGeralService.Obter(chave);

                if (configuracao == null)
                {
                    return Json(new { Success = false, Message = string.Format("CHAVE: {0} não encontrada", chave) });
                }

                _configGeralService.Deletar(configuracao, UsuarioAtual.Codigo);

                return Json(new { Success = true, Message = "Configuração removida com sucesso" });
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = ex.Message });
            }

        }
    }
}