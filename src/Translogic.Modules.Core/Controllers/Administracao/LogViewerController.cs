﻿using Speed.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Translogic.Core;
using Translogic.Core.Infrastructure.Web;
using Translogic.Modules.Core.Domain.Services.Administracao;
using Translogic.Modules.Core.Infrastructure;
using Translogic.Modules.Core.Spd.BLL;
using Translogic.Modules.Core.Spd.Data;
using Translogic.Modules.TPCCO.Helpers;

namespace Translogic.Modules.Core.Controllers.Administracao
{

    public class LogViewerController : BaseSecureModuleController
    {

        public LogViewerController()
        {
        }

        [Autorizar(Transacao = "CGRAL")]
        public ActionResult Index()
        {
            return View();
        }

        [Autorizar(Transacao = "CGRAL", Acao = "Pesquisar")]
        public JsonResult Select(DetalhesPaginacaoWeb pagination, DateTime? dataIni, DateTime? dataFim, string mensagem, string exception, string sistema, string tipo)
        {
            try
            {
                string ordem;
                if (pagination.ColunasOrdenacao.Any())
                    ordem = string.Join(",", pagination.ColunasOrdenacao) + " " + pagination.Dir;
                else
                    ordem = "DATA DESC";

                var resultados = BL_VwSisLog.SelectRelatorio(pagination.Inicio, pagination.Limite, ordem, dataIni, dataFim, mensagem, exception, sistema, tipo);

                var retorno = new
                {
                    Total = Int32.MaxValue,
                    Items = resultados.Select(p => new
                    {
                        p.LogId,
                        Data = p.Data.Value.ToString("dd/MM/yy HH:mm:ss"),
                        p.SistemaNome,
                        p.TipoNome,
                        p.Mensagem,
                        p.Tag,
                        p.Exception
                    })
                };

                return Json(retorno);
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = ex.Message });
            }
        }

    }

}
