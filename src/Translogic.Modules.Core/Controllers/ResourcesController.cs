namespace Translogic.Modules.Core.Controllers
{
	using System.IO;
	using System.Net;
	using System.Text;
	using System.Web.Mvc;
	using Translogic.Core.Infrastructure.Web;
	using Translogic.Core.Infrastructure.Web.Filters;

    /// <summary>
    /// Controller do resource combiner
    /// </summary>
	public class ResourcesController : Controller
	{
		#region M�TODOS

        /// <summary>
        /// Action default do resource combiner
        /// </summary>
        /// <param name="key">chave gerada de hash</param>
        /// <param name="resourceType">tipo do recurso</param>
        /// <param name="version">n�mero da vers�o</param>
        /// <returns>Arquivo de resource</returns>
		[CompressFilter(Order = 1)]
		[CacheFilter(DurationInDays = 30, Order = 2)]
		public virtual ActionResult Index(string key, ResourceType resourceType, long version)
		{
			if (!ResourceCombiner.ExistsInCache(key, version, resourceType))
			{
				Response.StatusCode = (int)HttpStatusCode.NotFound;
				return new EmptyResult();
			}

			Response.ContentEncoding = Encoding.UTF8;

			string contentType = string.Empty;

			switch (resourceType)
			{
				case ResourceType.JS:
					contentType = "text/javascript";
					break;
				case ResourceType.CSS:
					contentType = "text/css";
					break;
			}

			var info = new FileInfo(ResourceCombiner.GetServerPath(key, version, resourceType));

			Response.Cache.SetLastModified(info.LastWriteTimeUtc);
			Response.Cache.SetETag(key + "-" + info.LastWriteTimeUtc.Ticks);

			string resourceCombined;

			using (StreamReader stream = info.OpenText())
			{
				resourceCombined = stream.ReadToEnd();
			}

			return Content(resourceCombined, contentType);
		}

		#endregion
	}
}