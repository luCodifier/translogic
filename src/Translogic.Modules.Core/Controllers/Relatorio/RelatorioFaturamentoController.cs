﻿

namespace Translogic.Modules.Core.Controllers.Relatorio
{
    using System;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using Translogic.Modules.Core.Domain.Services.RelatorioFaturamento.Interface;
    using Translogic.Core.Infrastructure.Web;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Core.Infrastructure.Web.ContentResults;
    using Newtonsoft.Json;
    using System.IO;
    using Translogic.Core.Infrastructure.FileSystem.ExtensionMethods;
    using System.Linq;
    using Translogic.Modules.Core.Domain.Services.Mdfes;
    using Translogic.Core.Infrastructure.Web;
    using Translogic.Core.Infrastructure.Web.ContentResults;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Core.Infrastructure.Web.Helpers;
    using Translogic.Modules.TPCCO.Helpers;
    using System.Text;
    using EcmaScript.NET;
    using System.Collections;

    
    public class RelatorioFaturamentoController : BaseSecureModuleController
    {
        #region Services

        private readonly IRelatorioFaturamentoService _relatorioFaturamentoService;

        #endregion

        #region Cosntrutores

        public RelatorioFaturamentoController(IRelatorioFaturamentoService relatorioFaturamentoService)
        {
            _relatorioFaturamentoService = relatorioFaturamentoService;
        }

        #endregion

        #region Views

        /// <summary>
        /// Tela de pesquisa do relatório
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }     

        #endregion

        #region Métodos

        /// <summary>
        /// Pesquisa para relatório de faturamento
        /// </summary>
        /// <param name="detalhesPaginacaoWeb"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public JsonResult ConsultaRelatorioFaturamento(DetalhesPaginacaoWeb detalhesPaginacaoWeb, string relatorioFaturamentoRequestDto)
        {
            var request = JsonConvert.DeserializeObject<RelatorioFaturamentoRequestDto>(relatorioFaturamentoRequestDto);

            if (request.DataInicio == null || request.DataInicio == new DateTime())
                throw new Exception("Data inicial é obrigatório");

            if (request.DataFinal == null || request.DataFinal == new DateTime())
                throw new Exception("Data inicial é obrigatório");

            if (DateTime.Compare(request.DataFinal, request.DataInicio) < 0)
                throw new Exception("Data inicial deve ser menor que a data final");

            if (request.DataFinal.Subtract(request.DataInicio).Days + 1 > 30)
                throw new Exception("O período da pesquisa não deve ser superior a 30 dias");

            var resultado = _relatorioFaturamentoService.ObterRelatorioFaturamento(detalhesPaginacaoWeb,
                request.DataInicio,
                request.DataFinal,
                request.Fluxo,
                request.Origem,
                request.Destino,
                request.Vagoes,
                request.Situacao,
                request.Cliente,
                request.Expeditor,
                request.Segmento,
                request.Prefixo,
                request.Os,
                request.Malha,
                request.Uf,
                request.Estacao);

            return Json(
                new
                {
                    Items = resultado.Items,
                    Total = resultado.Total,
                    Success = true,
                    Status = 200 // Ok
                });

        }
        
        /// <summary>
        /// Pesquisa para exportação para Excel
        /// </summary>
        /// <param name="relatorioFaturamentoRequestDto"></param>
        /// <returns></returns>
        // [Autorizar(Transacao = "")]
        public ActionResult ConsultaRelatorioFaturamentoExportar(string relatorioFaturamentoRequestDto)
        {
            var request = JsonConvert.DeserializeObject<RelatorioFaturamentoRequestDto>(relatorioFaturamentoRequestDto);

            if (request.DataInicio == null || request.DataInicio == new DateTime())
                throw new Exception("Data inicial é obrigatório");

            if (request.DataFinal == null || request.DataFinal == new DateTime())
                throw new Exception("Data inicial é obrigatório");

            if (DateTime.Compare(request.DataFinal, request.DataInicio) < 0)
                throw new Exception("Data inicial deve ser menor que a data final");

            if (request.DataFinal.Subtract(request.DataInicio).Days + 1 > 30)
                throw new Exception("O período da pesquisa não deve ser superior a 30 dias");


            var colunas = new List<ExcelColuna<RelatorioFaturamentoExportDto>>
            {
                new ExcelColuna<RelatorioFaturamentoExportDto>("Vagão", c => !string.IsNullOrEmpty(c.Vagao) ? c.Vagao : string.Empty),
                new ExcelColuna<RelatorioFaturamentoExportDto>("Série", c => !string.IsNullOrEmpty(c.Serie) ? c.Serie : string.Empty),
                new ExcelColuna<RelatorioFaturamentoExportDto>("Segmento", c => !string.IsNullOrEmpty(c.Segmento) ? c.Segmento : string.Empty),
                new ExcelColunaData<RelatorioFaturamentoExportDto>("Faturamento", c => c.Faturamento),
                new ExcelColuna<RelatorioFaturamentoExportDto>("UF", c => !string.IsNullOrEmpty(c.UFOrigem) ? c.UFOrigem : string.Empty),
                new ExcelColuna<RelatorioFaturamentoExportDto>("Origem", c => !string.IsNullOrEmpty(c.Origem) ? c.Origem : string.Empty),
                new ExcelColuna<RelatorioFaturamentoExportDto>("UF", c => !string.IsNullOrEmpty(c.UFDestino) ? c.UFDestino : string.Empty),
                new ExcelColuna<RelatorioFaturamentoExportDto>("Destino", c => !string.IsNullOrEmpty(c.Destino) ? c.Destino : string.Empty),
                new ExcelColuna<RelatorioFaturamentoExportDto>("Fluxo", c => !string.IsNullOrEmpty(c.Fluxo) ? c.Fluxo : string.Empty),
                new ExcelColuna<RelatorioFaturamentoExportDto>("MD", c => !string.IsNullOrEmpty(c.MD) ? c.MD : string.Empty),
                new ExcelColuna<RelatorioFaturamentoExportDto>("Nota Fiscal", c => c.NotaFiscal),
                //new ExcelColunaInteira<RelatorioFaturamentoExportDto>("Nota Fiscal", c => c.NotaFiscal),
                new ExcelColunaDecimal<RelatorioFaturamentoExportDto>("Valor NF Cliente", c => c.ValorNFCliente),
                new ExcelColunaDecimal<RelatorioFaturamentoExportDto>("Chave NF-e", c => c.ChaveNFe),
                new ExcelColunaDecimal<RelatorioFaturamentoExportDto>("Volume", c => c.Volume),
                new ExcelColunaDecimal<RelatorioFaturamentoExportDto>("Peso Rateio", c => c.PesoRateio),
                new ExcelColunaDecimal<RelatorioFaturamentoExportDto>("Peso Total", c => c.PesoTotal),
                new ExcelColunaDecimal<RelatorioFaturamentoExportDto>("Tara", c => c.Tara),
                new ExcelColunaDecimal<RelatorioFaturamentoExportDto>("TB", c => c.TB),
                new ExcelColuna<RelatorioFaturamentoExportDto>("CLIENTE 363", c =>  !string.IsNullOrEmpty(c.Cliente) ? c.Cliente : string.Empty),
                new ExcelColuna<RelatorioFaturamentoExportDto>("EXPEDIDOR", c =>  !string.IsNullOrEmpty(c.Expedidor) ? c.Expedidor : string.Empty),
                new ExcelColuna<RelatorioFaturamentoExportDto>("RECEBEDOR", c =>  !string.IsNullOrEmpty(c.Recebedor) ? c.Recebedor : string.Empty),
                new ExcelColuna<RelatorioFaturamentoExportDto>("REMETENTE FISCAL", c =>  !string.IsNullOrEmpty(c.RemetenteFiscal) ? c.RemetenteFiscal : string.Empty),
                new ExcelColuna<RelatorioFaturamentoExportDto>("Containers", c =>  !string.IsNullOrEmpty(c.Containers) ? c.Containers : string.Empty),
                new ExcelColuna<RelatorioFaturamentoExportDto>("363", c =>  !string.IsNullOrEmpty(c.Str363) ? c.Str363 : string.Empty),
                new ExcelColuna<RelatorioFaturamentoExportDto>("1131", c =>  !string.IsNullOrEmpty(c.Str1131) ? c.Str1131 : string.Empty),
                new ExcelColuna<RelatorioFaturamentoExportDto>("Prefixo Trem", c =>  !string.IsNullOrEmpty(c.PrefixoTrem) ? c.PrefixoTrem : string.Empty),
                new ExcelColuna<RelatorioFaturamentoExportDto>("OS", c => c.OS)
            };

            var resultado = _relatorioFaturamentoService.ObterRelatorioFaturamentoExportar(
             request.DataInicio,
             request.DataFinal,
             request.Fluxo,
             request.Origem,
             request.Destino,
             request.Vagoes,
             request.Situacao,
             request.Cliente,
             request.Expeditor,
             request.Segmento,
             request.Prefixo,
             request.Os,
             request.Malha,
             request.Uf,
             request.Estacao);

            return ExcelHelper.Exportar(
                "RelatorioFaturamento.xlsx",
                colunas,
                resultado,
                ws =>
                {
                    ws.Cells.AutoFitColumns();
                    //ws.Column(16).Width = 25; // Observacões
                    //ws.Column(83).Width = 40; // Dados faltando
                });
        }

        /// <summary>
        /// Retorna o PDF
        /// </summary>
        // [Autorizar(Transacao = "RELOSLIMPVGO")]
        public FileResult Imprimir(string relatorioFaturamentoRequestDto)
        {
            var request = JsonConvert.DeserializeObject<RelatorioFaturamentoRequestDto>(relatorioFaturamentoRequestDto);

            var resultado = _relatorioFaturamentoService.ObterRelatorioFaturamentoExportar(
                  request.DataInicio,
                  request.DataFinal,
                  request.Fluxo,
                  request.Origem,
                  request.Destino,
                  request.Vagoes,
                  request.Situacao,
                  request.Cliente,
                  request.Expeditor,
                  request.Segmento,
                  request.Prefixo,
                  request.Os,
                  request.Malha,
                  request.Uf,
                  request.Estacao);

            //String speichern = "C:\\Arquivo\\Relatorio de Faturamento.txt";
            // String caminho = "Server.MapPath(Relatorio de Faturamento.txt)";
            using (MemoryStream memoryStream = new MemoryStream())
            {
                using (TextWriter tw = new StreamWriter(memoryStream))
                {
                    tw.WriteLine("Vagão    |"
                        + "SERIE|"
                        + "Segmento |"
                        + "Faturamento        |"
                        + "UF|"
                        + "Origem|"
                        + "UF|"
                        + "Destino|"
                        + "Fluxo    |"
                        + "MD |"
                        + "Nota Fiscal|"
                        + "     Valor NF|"
                        + "Chave NF-e                                  |"
                        + "   Volume|"
                        + "Peso Rateio|"
                        + "Peso Total|"
                        + "    Tara|"
                        + "       TB|"
                        + "CLIENTE 363                   |"
                        + "EXPEDIDOR                     |"
                        + "RECEBEDOR                     |"
                        + "REMETENTE FISCAL              |"
                        + "Containers  |"
                        + "363|"
                        + "1131|"
                        + "Prefixo Trem|"
                        + "OS     ");

                    foreach (RelatorioFaturamentoExportDto p in resultado)
                    {

                        tw.WriteLine((p.Vagao == null ? "" : p.Vagao).PadRight(9) +
                            "|" + (p.Serie == null ? "" : p.Serie).PadRight(5) +
                            "|" + (p.Segmento == null ? "" : p.Segmento).PadRight(9) +
                            "|" + p.Faturamento.ToString().PadRight(19) +
                            "|" + (p.UFOrigem == null ? "" : p.UFOrigem).PadRight(2) +
                            "|" + (p.Origem == null ? "" : p.Origem).PadRight(6) +
                            "|" + (p.UFDestino == null ? "" : p.UFDestino).PadRight(2) +
                            "|" + (p.Destino == null ? "" : p.Destino).PadRight(7) +
                            "|" + (p.Fluxo == null ? "" : p.Fluxo).PadRight(9) +
                            "|" + (p.MD == null ? "" : p.MD).PadRight(3) +
                            "|" + (p.NotaFiscal == null ? "" : p.NotaFiscal).PadRight(11) +
                            "|" + p.ValorNFCliente.ToString().PadLeft(13) +
                            "|" + (p.ChaveNFe == null ? "" : p.ChaveNFe).PadRight(44) +
                            "|" + p.Volume.ToString().PadLeft(9) +
                            "|" + p.PesoRateio.ToString().PadLeft(11) +
                            "|" + p.PesoTotal.ToString().PadLeft(10) +
                            "|" + p.Tara.ToString().PadLeft(8) +
                            "|" + p.TB.ToString().PadLeft(9) +
                            "|" + (p.Cliente == null ? "" : p.Cliente).PadRight(30) +
                            "|" + (p.Expedidor == null ? "" : p.Expedidor).PadRight(30) +
                            "|" + (p.Recebedor == null ? "" : p.Recebedor).PadRight(30) +
                            "|" + (p.RemetenteFiscal == null ? "" : p.RemetenteFiscal).PadRight(30) +
                            "|" + (p.Containers == null ? "" : p.Containers).PadRight(12) +
                            "|" + (p.Str363 == null ? "" : p.Str363).PadRight(3) +
                            "|" + (p.Str1131 == null ? "" : p.Str1131).PadRight(4) +
                            "|" + (p.PrefixoTrem == null ? "" : p.PrefixoTrem).PadRight(12) +
                            "|" + (p.OS == null ? "" : p.OS).PadRight(7)
                            );
                    }
                    tw.Flush();
                    tw.Close();
                }
                return File(memoryStream.GetBuffer(), "text/plain", "Relatorio de Faturamento.txt");
            }
        }

        //public ActionResult Imprimir(string relatorioFaturamentoRequestDto)
        //{

        //    var request = JsonConvert.DeserializeObject<RelatorioFaturamentoRequestDto>(relatorioFaturamentoRequestDto);

        //    var resultado = _relatorioFaturamentoService.ObterRelatorioFaturamentoExportar(
        //          request.DataInicio,
        //          request.DataFinal,
        //          request.Fluxo,
        //          request.Origem,
        //          request.Destino,
        //          request.Vagoes,
        //          request.Situacao,
        //          request.Cliente,
        //          request.Expeditor,
        //          request.Segmento,
        //          request.Prefixo,
        //          request.Os,
        //          request.Malha,
        //          request.Uf,
        //          request.Estacao); 

        //    ViewData["relatorio"] = resultado;

        //    return View("ImpressaoFaturamento");
        //}

        /// <summary>
        /// Retornar tipos 
        /// </summary>
        /// <returns></returns>
        public JsonResult ObterClientes()
        {
            var resultado = _relatorioFaturamentoService.ObterClientes();

            var jsonData = Json(
                new
                {
                    Items = resultado.Select(i => new
                    {
                       Id = i.IdCliente,
                       Nome = i.Cliente
                    }),
                    Total = resultado.Count
                });
            return jsonData;
        }

        //private MemoryStream geraRelatorioFaturamentoTxt(IList<RelatorioFaturamentoExportDto> resultado)
        //{
        //    String speichern = "C:\\Arquivo\\teste.txt";

        //    using (StreamWriter writer = new StreamWriter(speichern, true))

        //        {

        //            writer.Write(speichern, resultado.ToArray());

        //        }


        //    return geraRelatorioFaturamentoTxt;
        //}

        #endregion
    }
}