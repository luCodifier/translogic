﻿using System.Web.Mvc;
using Translogic.Modules.Core.Infrastructure;

namespace Translogic.Modules.Core.Controllers.Relatorio
{
    public class RelatorioOSRevistamentoVagaoController : BaseSecureModuleController
    {

        [Autorizar(Transacao = "RELOSREVISTVGO")]
        public ActionResult Index()
        {
            return View();
        }

    }
}
