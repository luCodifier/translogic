namespace Translogic.Modules.Core.Controllers.Relatorio
{
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Web.Mvc;
	using ALL.Core.Dominio;
	using Domain.Model.Diversos.Cte;
	using Domain.Model.Dto;
	using Domain.Model.FluxosComerciais.Pedidos.Despachos;
	using Domain.Services.Ctes;
	using Domain.Services.FluxosComerciais;
	using Infrastructure;
	using Translogic.Core.Infrastructure.Web;
	using Translogic.Core.Infrastructure.Web.ContentResults;
    using Translogic.Modules.Core.Domain.Services.Trem;

	/// <summary>
	/// Controller Relat�rio da Amaggi
	/// </summary>
	public class RelatorioAmaggiController : BaseSecureModuleController
	{
        private readonly RelatorioAmaggiService _relatorioAmaggiService;

        /// <summary>
        /// Construtor Padr�o
        /// </summary>
        /// <param name="relatorioAmaggiService">relatorioAmaggi Service</param>
        public RelatorioAmaggiController(RelatorioAmaggiService relatorioAmaggiService)
        {
            _relatorioAmaggiService = relatorioAmaggiService;
        }

        /// <summary>
        /// Action Index
        /// </summary>
        /// <returns>Action Result</returns>
		public ActionResult Index()
		{
			return View();
		}

        /// <summary>
        /// gera o relatorio da amaggi
        /// </summary>
        /// <param name="dataInicial">data inicial da filtyragem</param>
        /// <param name="dataFinal">data final da filtragem</param>
        /// <param name="estacoes">esta��es separadas por ;</param>
        /// <param name="emails">e-mails separados por ;</param>
        /// <returns>Action Result</returns>
        public ActionResult Gerar(DateTime dataInicial, DateTime dataFinal, string estacoes, string emails)
        {
            try
            {
                _relatorioAmaggiService.Gerar(dataInicial, dataFinal, estacoes, emails);
                return Json(new { success = true, Message = "Gerado com sucesso!" });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, ex.Message });
            }
        }
	}
}

