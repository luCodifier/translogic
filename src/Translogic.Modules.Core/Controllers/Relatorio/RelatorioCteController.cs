namespace Translogic.Modules.Core.Controllers.Relatorio
{
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Web.Mvc;
	using ALL.Core.Dominio;
	using Domain.Model.Diversos.Cte;
	using Domain.Model.Dto;
	using Domain.Model.FluxosComerciais.Pedidos.Despachos;
	using Domain.Services.Ctes;
	using Domain.Services.FluxosComerciais;
	using Infrastructure;
	using Translogic.Core.Infrastructure.Web;
	using Translogic.Core.Infrastructure.Web.ContentResults;

	/// <summary>
	/// Controller Relat�rio de Cte
	/// </summary>
	public class RelatorioCteController : BaseSecureModuleController
	{
		private readonly CarregamentoService _carregamentoService;
		private readonly CteService _cteService;

		/// <summary>
		/// Relat�rio de Cte
		/// </summary>
		/// <param name="carregamentoService"> Service Carregamento</param>
		/// <param name="cteService">Service Cte</param>
		public RelatorioCteController(CarregamentoService carregamentoService, CteService cteService)
		{
			_carregamentoService = carregamentoService;
			_cteService = cteService;
		}

		/// <summary>
		/// Action de inicio da tela de carregametno
		/// </summary>
		/// <returns> View da tela principal de carregamento </returns>
		[Autorizar(Transacao = "RELATORIOCTE")]
		public ActionResult Index()
		{
			return View();
		}

		/// <summary>
		/// Obtem uma lista de Ctes
		/// </summary>
		/// <returns>Lista de Ctes do agrupamento </returns>
		[Autorizar(Transacao = "RELATORIOCTE", Acao = "Pesquisar")]
		public JsonResult ObterCodigoSerieDesp()
		{
			IList<SerieDespachoUf> result = _carregamentoService.ObterCodigoSerie();

			// Inseri item default em branco no list
			var itemDefault = new SerieDespachoUf();
			itemDefault.CodigoControle = " ";

			result.Insert(0, itemDefault);

			return Json(new
			{
				Items = result.Select(c => new
				{
					c.Id,
					c.CodigoControle
				}),
				success = true
			});
		}

		/// <summary>
		/// Obtem uma lista de Ctes
		/// </summary>
		/// <param name="pagination"> Paginacao pesquisa</param>
		/// <param name="filter"> Filtros para pesquisa</param>
		/// <returns>Lista de Ctes</returns>
		[Autorizar(Transacao = "RELATORIOCTE", Acao = "Pesquisar")]
		public JsonResult ObterCtes(DetalhesPaginacaoWeb pagination, DetalhesFiltro<object>[] filter)
		{
			ResultadoPaginado<CteDto> result = _cteService.RetornaCteMonitoramento(pagination, filter);

			return Json(new
			{
				result.Total,
				Items = result.Items.Select(c => new
				{
					c.CteId,
					c.Fluxo,
					c.Origem,
					c.Destino,
					c.Mercadoria,
					c.Chave,
					c.Cte,
					SituacaoCte = ALL.Core.Util.Enum<SituacaoCteEnum>.GetDescriptionOf(c.SituacaoCte),
					DateEmissao = c.DataEmissao.ToString("dd/MM/yyyy"),
					c.CodigoUsuario,
					c.CodigoErro,
					c.FerroviaOrigem,
					c.FerroviaDestino,
					CodigoVagao = c.CodigoVagao ?? "COMPLEMENTAR",
					c.UfOrigem,
					c.UfDestino,
					Serie = c.SerieDesp6 == 0 && c.NumDesp6 == 0 ? c.SerieDesp5 != null ? c.SerieDesp5.ToString().PadLeft(3, '0') : string.Empty : string.Concat(c.CodigoControle, "-", c.SerieDesp6.ToString().PadLeft(3, '0')),
					Despacho = c.SerieDesp6 == 0 && c.NumDesp6 == 0 ? c.NumDesp5.ToString().PadLeft(3, '0') : c.NumDesp6.ToString().PadLeft(3, '0'),
					SerieDesp5 = c.SerieDesp5 != null ? c.SerieDesp5.ToString().PadLeft(3, '0') : string.Empty,
					Desp5 = c.NumDesp5.ToString().PadLeft(3, '0'),
					c.StatusCte,
					c.ArquivoPdfGerado,
					c.AcaoSerTomada,
					c.Impresso,
                    c.Nfe,
                    PesoNfe = c.PesoNfe == null ? string.Empty : c.PesoNfe.ToString(),
                    PesoUtilizadoNfe = c.PesoUtilizadoNfe == null ? string.Empty : c.PesoUtilizadoNfe.ToString()
				}),
				success = true
			});
		}

		/// <summary>
		/// Exporta os dados para excel
		/// </summary>
		/// <param name="dataInicial">Data de Inicio da pesquisa</param>
		/// <param name="dataFinal">Data Fim da pesquisa</param>
		/// <param name="serie">s�rie do Cte</param>
		/// <param name="despacho">despacho Cte</param>
		/// <param name="codFluxo">Fluxo do Cte</param>
		/// <param name="chaveCte">Chave do Cte</param>
		/// <param name="erro">Erro do Cte</param>
		/// <param name="origem">Origem do cte</param>
		/// <param name="destino">Destino do cte</param>
		/// <param name="numVagao">Numero do vagao</param>
		/// <param name="impresso">Cte Impresso</param>
		/// <param name="codigoUfDcl">Codigo Uf do dcl</param>
        /// <param name="incluirNfe">deve incluir nfe</param>
		/// <returns>Retorna o arquivo excel</returns>
        [Autorizar(Transacao = "RELATORIOCTE", Acao = "Pesquisar")]
        public ActionResult Exportar(string dataInicial, string dataFinal, string serie, int? despacho, string codFluxo, string chaveCte, string erro, string origem, string destino, string numVagao, bool? impresso, string codigoUfDcl, string incluirNfe)
		{
			DateTime dataInicio = DateTime.Parse(dataInicial);
			DateTime dataFim = DateTime.Parse(dataFinal);
		    origem = origem != null ? origem.ToUpper() : null;
            destino = destino != null ? destino.ToUpper() : null;

		    var deveIncluirNfe = incluirNfe == "S";

			DateTime auxDataFinal = new DateTime(dataFim.Year, dataFim.Month, dataFim.Day, 23, 59, 59);
			ResultadoPaginado<CteDto> result = _cteService.RetornaCteMonitoramento(null, dataInicio, auxDataFinal, serie, despacho.HasValue ? despacho.Value : 0, codFluxo, chaveCte, erro, origem, destino, numVagao, impresso, codigoUfDcl, deveIncluirNfe);

			Dictionary<string, Func<CteDto, string>> dic = new Dictionary<string, Func<CteDto, string>>();

			dic["FLUXO"] = c => string.IsNullOrEmpty(c.Fluxo) ? string.Empty : c.Fluxo;
			dic["N� Vag�o"] = c => string.IsNullOrEmpty(c.CodigoVagao) ? string.Empty : c.CodigoVagao;
			dic["S�rie6"] = c => c.SerieDesp6 == 0 && c.NumDesp6 == 0 ? c.NumDesp5.ToString().PadLeft(3, '0') : c.NumDesp6.ToString().PadLeft(3, '0');
			dic["N� Despacho6"] = c => c.SerieDesp6 == 0 && c.NumDesp6 == 0 ? c.NumDesp5.ToString().PadLeft(3, '0') : c.NumDesp6.ToString().PadLeft(3, '0');
			dic["SerieDesp5"] = c => c.SerieDesp5 != null ? c.SerieDesp5.ToString().PadLeft(3, '0') : string.Empty;
			dic["N� Despacho5"] = c => c.NumDesp5.ToString().PadLeft(3, '0');
			dic["Cte"] = c => c.Cte;
			dic["Origem"] = c => string.IsNullOrEmpty(c.Origem) ? string.Empty : c.Origem;
			dic["Destino"] = c => string.IsNullOrEmpty(c.Destino) ? string.Empty : c.Destino;
			dic["Uf Origem"] = c => c.UfOrigem ?? string.Empty;
			dic["Uf Destino"] = c => c.UfDestino ?? string.Empty;
			dic["Mercadoria"] = c => string.IsNullOrEmpty(c.Mercadoria) ? string.Empty : c.Mercadoria;
			dic["Chave Cte"] = c => string.IsNullOrEmpty(c.Chave) ? string.Empty : c.Chave;
			dic["Data Emiss�o"] = c => c.DataEmissao.ToString("dd/MM/yyyy");
			dic["Ferrovia Origem"] = c => c.FerroviaOrigem ?? string.Empty;
			dic["Ferrovia Destino"] = c => c.FerroviaDestino ?? string.Empty;
			dic["Situa��o Cte"] = c => ALL.Core.Util.Enum<SituacaoCteEnum>.GetDescriptionOf(c.SituacaoCte);
			dic["C�digo usu�rio"] = c => c.CodigoUsuario ?? string.Empty;
			dic["C�digo Erro"] = c => c.CodigoErro ?? string.Empty;
			dic["Codigo Vag�o"] = c => c.CodigoVagao ?? "COMPLEMENTAR";
			dic["Acao Ser Tomada"] = c => c.AcaoSerTomada ?? string.Empty;

            if (deveIncluirNfe)
            {
                dic["NFe"] = c => c.Nfe ?? string.Empty;
                dic["Peso da NFe"] = c => c.PesoNfe == null ? string.Empty : c.PesoNfe.ToString();
                dic["Peso Utilizado da NFe"] = c => c.PesoUtilizadoNfe == null ? string.Empty : c.PesoUtilizadoNfe.ToString();
            }
            
			return this.Excel("Cte_Emitidos.xls", dic, result.Items);
		}
	}
}

