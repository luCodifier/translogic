﻿namespace Translogic.Modules.Core.Controllers.Relatorio
{
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Web.Mvc;
	using ALL.Core.Dominio;
	using Domain.Model.Diversos.Cte;
	using Domain.Model.Dto;
	using Domain.Model.FluxosComerciais.Pedidos.Despachos;
	using Domain.Services.Ctes;
	using Domain.Services.FluxosComerciais;
	using Infrastructure;
	using Translogic.Core.Infrastructure.Web;
	using Translogic.Core.Infrastructure.Web.ContentResults;

	/// <summary>
	/// Classe RelatorioCteManutencaoPendenteController
	/// </summary>
	public class RelatorioCteManutencaoPendenteController : BaseSecureModuleController
	{
		private readonly CarregamentoService _carregamentoService;
		private readonly CteService _cteService;

		/// <summary>
		/// Construtor da classe RelatorioCteManutencaoPendenteController
		/// </summary>
		/// <param name="carregamentoService">Serviço de Carregamento</param>
		/// <param name="cteService">Serviço de Cte</param>
		public RelatorioCteManutencaoPendenteController(CarregamentoService carregamentoService, CteService cteService)
		{
			_carregamentoService = carregamentoService;
			_cteService = cteService;
		}

		/// <summary>
		/// Action index
		/// </summary>
		/// <returns>Retorna a view principal</returns>
		[Autorizar(Transacao = "RELATORIOMANUTENCAOCTE")]
		public ActionResult Index()
		{
			return View();
		}

		/// <summary>
		/// Obtem uma lista de Ctes
		/// </summary>
		/// <returns>Lista de Ctes do agrupamento </returns>
		[Autorizar(Transacao = "RELATORIOMANUTENCAOCTE", Acao = "Pesquisar")]
		public JsonResult ObterCodigoSerieDesp()
		{
			IList<SerieDespachoUf> result = _carregamentoService.ObterCodigoSerie();

			// Inseri item default em branco no list
			var itemDefault = new SerieDespachoUf();
			itemDefault.CodigoControle = " ";

			result.Insert(0, itemDefault);

			return Json(new
			{
				Items = result.Select(c => new
				{
					c.Id,
					c.CodigoControle
				}),
				success = true
			});
		}

		/// <summary>
		/// Obtem uma lista de Ctes
		/// </summary>
		/// <param name="pagination"> Paginacao pesquisa</param>
		/// <param name="filter"> Filtros para pesquisa</param>
		/// <returns>Lista de Ctes</returns>
		[Autorizar(Transacao = "RELATORIOMANUTENCAOCTE", Acao = "Pesquisar")]
		public JsonResult ObterCtes(DetalhesPaginacaoWeb pagination, DetalhesFiltro<object>[] filter)
		{
			ResultadoPaginado<CteDto> result = _cteService.RetornaCteManutencaoPendente(pagination, filter);

			return Json(new
			{
				result.Total,
				Items = result.Items.Select(c => new
				{
					c.CteId,
					c.Fluxo,
					c.Origem,
					c.Destino,
					c.Mercadoria,
					c.Chave,
					c.Cte,
					SituacaoCte = ALL.Core.Util.Enum<SituacaoCteEnum>.GetDescriptionOf(c.SituacaoCte),
					DateEmissao = c.DataEmissao.ToString("dd/MM/yyyy"),
					c.CodigoUsuario,
					c.CodigoErro,
					c.FerroviaOrigem,
					c.FerroviaDestino,
					CodigoVagao = c.CodigoVagao ?? "COMPLEMENTAR",
					c.UfOrigem,
					c.UfDestino,
					Serie = c.SerieDesp6 == 0 && c.NumDesp6 == 0 ? c.SerieDesp5 != null ? c.SerieDesp5.ToString().PadLeft(3, '0') : string.Empty : string.Concat(c.CodigoControle, "-", c.SerieDesp6.ToString().PadLeft(3, '0')),
					Despacho = c.SerieDesp6 == 0 && c.NumDesp6 == 0 ? c.NumDesp5.ToString().PadLeft(3, '0') : c.NumDesp6.ToString().PadLeft(3, '0'),
					SerieDesp5 = c.SerieDesp5 != null ? c.SerieDesp5.ToString().PadLeft(3, '0') : string.Empty,
					Desp5 = c.NumDesp5.ToString().PadLeft(3, '0'),
					c.StatusCte,
					c.ArquivoPdfGerado,
					c.AcaoSerTomada,
					c.Impresso
				}),
				success = true
			});
		}

		/// <summary>
		/// Exporta os dados para excel
		/// </summary>
		/// <param name="dataInicial">Data de Inicio da pesquisa</param>
		/// <param name="dataFinal">Data Fim da pesquisa</param>
		/// <param name="serie">série do Cte</param>
		/// <param name="despacho">despacho Cte</param>
		/// <param name="codFluxo">Fluxo do Cte</param>
		/// <param name="chaveCte">Chave do Cte</param>
		/// <param name="origem">Origem do cte</param>
		/// <param name="destino">Destino do cte</param>
		/// <param name="numVagao">Numero do vagao</param>
		/// <param name="codigoUfDcl">Codigo Uf do dcl</param>
		/// <returns>Retorna o arquivo excel</returns>
		[Autorizar(Transacao = "RELATORIOMANUTENCAOCTE", Acao = "Salvar")]
		public ActionResult Exportar(string dataInicial, string dataFinal, string serie, int? despacho, string codFluxo, string chaveCte, string origem, string destino, string numVagao, string codigoUfDcl)
		{
			DateTime dataInicio = DateTime.Parse(dataInicial);
			DateTime dataFim = DateTime.Parse(dataFinal);

			DateTime auxDataFinal = new DateTime(dataFim.Year, dataFim.Month, dataFim.Day, 23, 59, 59);
			ResultadoPaginado<CteDto> result = _cteService.RetornaCteManutencaoPendente(null, dataInicio, auxDataFinal, serie, despacho.HasValue ? despacho.Value : 0, codFluxo, chaveCte, origem, destino, numVagao, codigoUfDcl);

			Dictionary<string, Func<CteDto, string>> dic = new Dictionary<string, Func<CteDto, string>>();
			
			dic["FLUXO"] = c => string.IsNullOrEmpty(c.Fluxo) ? string.Empty : c.Fluxo;
			dic["Nº Vagão"] = c => string.IsNullOrEmpty(c.CodigoVagao) ? string.Empty : c.CodigoVagao;
			dic["Série6"] = c => c.SerieDesp6 == 0 && c.NumDesp6 == 0 ? c.NumDesp5.ToString().PadLeft(3, '0') : c.NumDesp6.ToString().PadLeft(3, '0');
			dic["Nº Despacho6"] = c => c.SerieDesp6 == 0 && c.NumDesp6 == 0 ? c.NumDesp5.ToString().PadLeft(3, '0') : c.NumDesp6.ToString().PadLeft(3, '0');
			dic["SerieDesp5"] = c => c.SerieDesp5 != null ? c.SerieDesp5.ToString().PadLeft(3, '0') : string.Empty;
			dic["Nº Despacho5"] = c => c.NumDesp5.ToString().PadLeft(3, '0');
			dic["Cte"] = c => c.Cte;
			dic["Origem"] = c => string.IsNullOrEmpty(c.Origem) ? string.Empty : c.Origem;
			dic["Destino"] = c => string.IsNullOrEmpty(c.Destino) ? string.Empty : c.Destino;
			dic["Uf Origem"] = c => c.UfOrigem ?? string.Empty;
			dic["Uf Destino"] = c => c.UfDestino ?? string.Empty;
			dic["Mercadoria"] = c => string.IsNullOrEmpty(c.Mercadoria) ? string.Empty : c.Mercadoria;
			dic["Chave Cte"] = c => string.IsNullOrEmpty(c.Chave) ? string.Empty : c.Chave;
			dic["Data Emissão"] = c => c.DataEmissao.ToString("dd/MM/yyyy");
			dic["Ferrovia Origem"] = c => c.FerroviaOrigem ?? string.Empty;
			dic["Ferrovia Destino"] = c => c.FerroviaDestino ?? string.Empty;
			dic["Situação Cte"] = c => ALL.Core.Util.Enum<SituacaoCteEnum>.GetDescriptionOf(c.SituacaoCte);
			dic["Código usuário"] = c => c.CodigoUsuario ?? string.Empty;
			dic["Código Erro"] = c => c.CodigoErro ?? string.Empty;
			dic["Codigo Vagão"] = c => c.CodigoVagao ?? "COMPLEMENTAR";
			dic["Acao Ser Tomada"] = c => c.AcaoSerTomada ?? string.Empty;

			return this.Excel("Cte_Manutencao_Pendente.xls", dic, result.Items);
		}
	}
}
