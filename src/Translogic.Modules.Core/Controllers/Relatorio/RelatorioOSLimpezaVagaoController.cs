﻿using System.Web.Mvc;
using Translogic.Modules.Core.Infrastructure;

namespace Translogic.Modules.Core.Controllers.Relatorio
{
    public class RelatorioOSLimpezaVagaoController : BaseSecureModuleController
    {
        #region ActionResults

        [Autorizar(Transacao = "RELOSLIMPVGO")]
        public ActionResult Index()
        {
            return View();
        }

        #endregion
    }
}