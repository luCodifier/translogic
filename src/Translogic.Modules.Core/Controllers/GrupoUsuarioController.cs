namespace Translogic.Modules.Core.Controllers
{
	using System;
	using System.Linq;
	using System.Web.Mvc;
	using ALL.Core.Dominio;
	using Domain.Model.Acesso;
	using Domain.Model.Acesso.Repositories;
	using Infrastructure;
	using Translogic.Core.Infrastructure.Web;

	/// <summary>
	/// Controller para controle de grupos de usu�rio
	/// </summary>
	public class GrupoUsuarioController : BaseSecureModuleController
	{
		#region ATRIBUTOS

		private readonly IGrupoUsuarioRepository _grupoUsuarioRepository;

		#endregion

		#region CONSTRUTORES

		/// <summary>
		/// Construtor default
		/// </summary>
		/// <param name="grupoUsuarioRepository">Reposit�rio de Grupos de Usu�rio. <remarks>Injetado</remarks></param>
		public GrupoUsuarioController(IGrupoUsuarioRepository grupoUsuarioRepository)
		{
			_grupoUsuarioRepository = grupoUsuarioRepository;
		}

		#endregion

		#region M�TODOS
		/// <summary>
		/// Formul�rio de inser��o/altera��o
		/// </summary>
		/// <param name="id">Id do grupo de usuario, quando nulo inserir</param>
		/// <returns>View do formul�rio</returns>
		public ActionResult Form(int? id)
		{
			if (id.HasValue)
			{
				GrupoUsuario grupo = _grupoUsuarioRepository.ObterPorId(id.Value);

				return View(new { grupo, edit = true });
			}

			return View();
		}

		/// <summary>
		/// Tela de pesquisa de grupos de usu�rios
		/// </summary>
		/// <returns>View de pesquisa</returns>
		public ActionResult Index()
		{
			return View();
		}

		/// <summary>
		/// Metodo que obt�m a lista de grupos de usuarios de acordo com o par�metros informados
		/// </summary>
		/// <param name="pagination">Detalhes da pagina��o</param>
		/// <param name="filter">Detalhes do filtro</param>
		/// <returns>Resultado Json</returns>
		public JsonResult ObterTodos(DetalhesPaginacaoWeb pagination, IDetalhesFiltro<object>[] filter)
		{
			ResultadoPaginado<GrupoUsuario> result = _grupoUsuarioRepository.ObterTodosPaginado(pagination, filter);

			return Json(new
			            	{
			            		result.Total,
			            		Items = result.Items.Select(g => new { g.Id, g.Codigo, g.Descricao, g.Restrito })
			            	});
		}

		/// <summary>
		/// M�todo que remove um determinado grupo de usuarios
		/// </summary>
		/// <param name="id">Id do grupo a ser exclu�do</param>
		/// <returns>Resultado Json</returns>
		public JsonResult Remover(int id)
		{
			try
			{
				_grupoUsuarioRepository.Remover(id);

				return Json(new { success = true });
			}
			catch (Exception e)
			{
				return Json(new { success = false, e.Message, e.StackTrace });
			}
		}

		/// <summary>
		/// M�todo que insere/altera o grupo de usu�rio
		/// </summary>
		/// <param name="grupo">Grupo de usu�rio</param>
		/// <returns>Resultado Json</returns>
		public JsonResult Salvar(GrupoUsuario grupo)
		{
			try
			{
				_grupoUsuarioRepository.InserirOuAtualizar(grupo);

				return Json(new { success = true });
			}
			catch (Exception e)
			{
				return Json(new { success = false, e.Message, e.StackTrace });
			}
		}

		#endregion
	}
}