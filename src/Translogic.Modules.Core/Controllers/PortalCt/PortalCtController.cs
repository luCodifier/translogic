﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using ALL.Core.Dominio;
using Translogic.Core.Infrastructure.Web;
using Translogic.Modules.Core.Domain.Model.Diversos;
using Translogic.Modules.Core.Domain.Model.Diversos.Repositories;
using Translogic.Modules.Core.Domain.Model.Trem.OrdemServico;
using Translogic.Modules.Core.Domain.Services.Trem;

namespace Translogic.Modules.Core.Controllers.PortalCt
{
    public class PortalCtController : BaseSecureModuleController
    {
        private readonly ICadCorredorRepository _cadCorredorRepository;
        private readonly ParadaOrdemServicoProgramadaService _paradaOrdemServicoProgramadaService;

        public PortalCtController(ParadaOrdemServicoProgramadaService paradaOrdemServicoProgramadaService,
            ICadCorredorRepository cadCorredorRepository)
        {
            _paradaOrdemServicoProgramadaService = paradaOrdemServicoProgramadaService;
            _cadCorredorRepository = cadCorredorRepository;
        }

        /// <summary>
        ///     Acao Padrao ao ser chamado o controller
        /// </summary>
        /// <returns>View Padrao</returns>
        /*[Autorizar(Transacao = "PREVISOESCHEGADA")]*/
        public ActionResult PrevisoesChegada()
        {
            return View();
        }

        /// <summary>
        ///     Obtem uma lista de Paradas com suas Previsões
        /// </summary>
        /// <param name="pagination"> Paginacao pesquisa</param>
        /// <param name="filter"> Filtros para pesquisa</param>
        /// <returns>Lista de Paradas com suas Previsões</returns>
        /*[Autorizar(Transacao = "PREVISOESCHEGADA", Acao = "Pesquisar")]*/
        public ActionResult ObterPrevisoesParadas(DetalhesPaginacaoWeb pagination, DetalhesFiltro<object>[] filter)
        {
            DateTime? dataIni = null;
            DateTime? dataFim = null;
            CadCorredor corredor = null;
            int? os = null;
            string prefixo = null, origem = null, destino = null;

            foreach (var detalheFiltro in filter)
            {
                if (detalheFiltro.Campo.Equals("os") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                {
                    os = Convert.ToInt32(detalheFiltro.Valor[0].ToString());
                }

                if (detalheFiltro.Campo.Equals("prefixo") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                {
                    prefixo = detalheFiltro.Valor[0].ToString();
                }

                if (detalheFiltro.Campo.Equals("origem") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                {
                    origem = detalheFiltro.Valor[0].ToString();
                }

                if (detalheFiltro.Campo.Equals("destino") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                {
                    destino = detalheFiltro.Valor[0].ToString();
                }

                if (detalheFiltro.Campo.Equals("dataInicial") &&
                    !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                {
                    dataIni = DateTime.ParseExact(detalheFiltro.Valor[0].ToString(), "dd/MM/yyyy HH:mm:ss",
                        CultureInfo.InvariantCulture);
                    dataIni = dataIni.Value.Date.AddHours(0).AddMinutes(0).AddSeconds(0);
                }

                if (detalheFiltro.Campo.Equals("dataFinal") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                {
                    dataFim = DateTime.ParseExact(detalheFiltro.Valor[0].ToString(), "dd/MM/yyyy HH:mm:ss",
                        CultureInfo.InvariantCulture);
                    dataFim = dataFim.Value.Date.AddHours(23).AddMinutes(59).AddSeconds(59);
                }

                if (detalheFiltro.Campo.Equals("corredor") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                {
                    corredor = new CadCorredor {Id = Convert.ToInt32(detalheFiltro.Valor[0].ToString())};
                }
            }

            if (!dataIni.HasValue)
            {
                dataIni = DateTime.Now;
                dataIni = dataIni.Value.Date.AddHours(0).AddMinutes(0).AddSeconds(0);
            }

            if (!dataFim.HasValue)
            {
                dataFim = DateTime.Now;
                dataFim = dataFim.Value.Date.AddHours(23).AddMinutes(59).AddSeconds(59);
            }

            IList<ParadaOrdemServicoProgramada> listaDados =
                _paradaOrdemServicoProgramadaService.ObterParadas(os, prefixo, origem, destino,
                    dataIni.Value, dataFim.Value, corredor);

            var teste = string.Empty;
            return Json(new
            {
                listaDados.Count,
                Items = listaDados.Select(p =>
                    new
                    {
                        p.Id,
                        OS = p.OrdemServico.Numero.ToString(),
                        p.OrdemServico.Prefixo,
                        Origem = p.OrdemServico.Origem.Codigo,
                        Destino = p.OrdemServico.Destino.Codigo,
                        Parada = p.Estacao.Codigo,
                        DataPrevista = string.Empty,
                        HoraPrevista = string.Empty,
                        TempoParada = string.Empty,
                        UltimaAlteracao = string.Empty,
                        BloqueiaEdicao = _paradaOrdemServicoProgramadaService.BloqueiaEdicaoPrevisaoChegada(p).ToString()
                    }),
                success = true
            });
        }

        /// <summary>
        ///     Obtem o combo de corredores
        /// </summary>
        /// <returns>Lista de Corredores</returns>
        /*		[Autorizar(Transacao = "PREVISOESCHEGADA", Acao = "Pesquisar")]*/
        public ActionResult ObterCorredores()
        {
            return
                Json(
                    _cadCorredorRepository.ObterCorredores()
                        .Select(t => new {Corredor = t.Id, CorredorLabel = t.Descricao}));
        }
    }
}