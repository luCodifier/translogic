﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Translogic.Modules.Core.Domain.Services.Tfa.Interface;
using Translogic.Core.Infrastructure.Web;
using ALL.Core.Dominio;
using Translogic.Modules.Core.Domain.Model.Tfa;
using Translogic.Modules.Core.Infrastructure;
using Translogic.Modules.Core.Domain.Model.Dto;

namespace Translogic.Modules.Core.Controllers.Tfa
{
    /// <summary>
    /// Classe controller para gestão de Terminais e envio de emails para notificação sobre TFAs
    /// </summary>
    public class TerminalTfaController : BaseSecureModuleController
    {

        private readonly ITerminalTfaService _terminalTfaService;
        private readonly ITerminalTfaEmailService _terminalTfaEmailService;

        public TerminalTfaController(ITerminalTfaService terminalTfaService, ITerminalTfaEmailService terminalTfaEmailService)
        {
            _terminalTfaService = terminalTfaService;
            _terminalTfaEmailService = terminalTfaEmailService;
        }

        #region ActionViews
        [HttpGet]
        public ActionResult TerminalTfa()
        {
            try
            {
                return View("~/Views/Tfa/TerminalTfa.aspx");
            }
            catch (Exception)
            {
                return MensagemRetorno(false, "Ocorreu um erro ao abrir a tela de Cadastro de E-mails de Terminais. Contate o administrador do sistema.");
            }
        }


        [HttpPost]
        [Autorizar(Transacao = "EMAILTERMINAL", Acao = "Incluir")]
        public ActionResult NovoEmailTerminal(int idTerminal)
        {
            try
            {
                
                ViewData["idTerminal"] = idTerminal;
                
            }
            catch (Exception)
            {
                return MensagemRetorno(false, "Ocorreu um erro ao abrir a tela de Cadastro de E-mails de Terminais. Contate o administrador do sistema.");
            }

            return View("~/Views/Tfa/NovoEmailTerminal.ascx");
        }

        [HttpPost]
        [Autorizar(Transacao = "EMAILTERMINAL", Acao = "Incluir")]
        public ActionResult IncluirEmailTerminal(int idTerminal, string email)
        {
            try
            {
                if (!string.IsNullOrEmpty(email))
                {
                    var terminalTfaEmail = _terminalTfaEmailService.ObterPoridTerminalEmail(idTerminal, email);

                    if (terminalTfaEmail != null)
                    {
                        return MensagemRetorno(false, "E-mail já cadastrado!");
                    }

                    var terminal = _terminalTfaService.ObterPorIdTerminal(idTerminal);

                    if (terminal == null)
                    {
                        terminal = _terminalTfaService.InserirTerminalTfa(idTerminal);
                    }

                    terminalTfaEmail = _terminalTfaEmailService.IncluirTerminalTfaEmail(terminal, email);

                    if (terminalTfaEmail != null)
                    {
                        return MensagemRetorno(true, "Email incluído com sucesso!");
                    }
                }
                else
                {
                    return MensagemRetorno(false, "E-mail inválido!");
                }

            }
            catch (Exception)
            {
                return MensagemRetorno(false, "Ocorreu um erro ao tentar incluir registro no Cadastro de E-mails de Terminais. Contate o administrador do sistema.");
            }

            return MensagemRetorno(false, "Ocorreu um erro ao tentar incluir registro no Cadastro de E-mails de Terminais. Contate o administrador do sistema.");
        }

        [HttpPost]
        [Autorizar(Transacao = "EMAILTERMINAL", Acao = "Excluir")]
        public ActionResult ExcluirTerminalTfaEmail(string id)
        {
            try
            {
                _terminalTfaEmailService.Remover(int.Parse(id));
            }
            catch (Exception)
            {
                return MensagemRetorno(false, "Ocorreu um erro ao excluir o E-mail. Contate o administrador do sistema.");
            }

            return MensagemRetorno(true, "E-mail excluído com sucesso.");
        }

        [HttpPost]
        [Autorizar(Transacao = "EMAILTERMINAL", Acao = "Pesquisar")]
        public JsonResult ObterEmailsTerminalTfa(DetalhesPaginacaoWeb detalhesPaginacaoWeb, int idTerminal)
        {
            try
            {
                ResultadoPaginado<TerminalTfaEmail> result = _terminalTfaEmailService.ObterTodosPorIdTerminal(detalhesPaginacaoWeb, idTerminal);

                if (result.Items == null)
                {
                    result.Items = new List<TerminalTfaEmail>();
                }

                return Json(new
                {
                    Total = result.Total,
                    Items = result.Items.Select(p => new
                    {
                        p.Id,
                        p.Terminal,
                        p.Email
                    }),
                    success = true,
                    Message = ""
                });


            }
            catch (Exception)
            {
                return MensagemRetorno(false, "Ocorreu um erro ao recuperar os E-mails do Terminal. Contate o administrador do sistema.");
            }

            return MensagemRetorno(true, "Nenhum E-mail Cadastrado para o Terminal");
        }

        [HttpGet]
        [Autorizar(Transacao = "EMAILTERMINAL", Acao = "Pesquisar")]
        public ActionResult ObterListaTerminalTfa()
        {
            try
            {
                var result = _terminalTfaService.ObterListaTerminalDto();

                if (result == null)
                {
                    result = new List<TerminalDto>();
                }

                return Json(new
                {
                    Items = result.Select(p => new
                    {
                        p.IdTerminal,
                        p.DescricaoTerminal
                    }),
                    success = true,
                    Message = ""
                });

            }
            catch (Exception ex)
            {
                return MensagemRetorno(false, ex.Message);
            }
        }

        private JsonResult MensagemRetorno(Boolean Success, string Message)
        {
            return Json(new
            {
                Success = Success,
                Message = Message
            });
        }

        #endregion
    }
}
