﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Translogic.Modules.Core.Domain.Services.Tfa.Interface;
using Translogic.Core.Infrastructure.Web;
using ALL.Core.Dominio;
using Translogic.Modules.Core.Domain.Model.Tfa;
using Translogic.Modules.Core.Infrastructure;
using Translogic.Core.Infrastructure;


namespace Translogic.Modules.Core.Controllers.Tfa
{
    /// <summary>
    /// Classe controller para gestão Grupos de Empresas para notificação sobre TFAs
    /// </summary>
    public class GrupoEmpresaTfaController : BaseSecureModuleController
    {

        private readonly IGrupoEmpresaTfaService _grupoEmpresaTfaService;

        /// <summary>
        /// Construtor da Classe Controller
        /// </summary>
        /// <param name="grupoEmpresaTfaService">Instância da Classe de Serviços para Grupo de Empresas (DI)</param>
        public GrupoEmpresaTfaController(IGrupoEmpresaTfaService grupoEmpresaTfaService)
        {
            _grupoEmpresaTfaService = grupoEmpresaTfaService;
        }


        #region ActionViews

        /// <summary>
        /// Retorna a view principal vinculada à Controller
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GrupoEmpresaTfa()
        {
            try
            {
                return View("~/Views/Tfa/GrupoEmpresaTfa.aspx");
            }
            catch (Exception)
            {
                return MensagemRetorno(false, "Ocorreu um erro ao abrir a tela de Grupos de Empresas. Contate o administrador do sistema.");
            }
        }

        /// <summary>
        /// Retorna a view para Inclusão de Novo Grupo de Empresas
        /// </summary>
        /// <param name="idGrupoEmpresa">ID do Grupo de Empresas</param>
        /// <returns></returns>
        [HttpPost]
        [Autorizar(Transacao = "GRUPOEMPRESATFA", Acao = "Incluir")]
        public ActionResult NovoGrupoEmpresaTfa(int? idGrupoEmpresa)
        {
            try
            {
                ViewData["idGrupoEmpresa"] = 0;

                if (idGrupoEmpresa.HasValue && idGrupoEmpresa.Value > 0)
                {
                    var grupoEmpresa = _grupoEmpresaTfaService.ObterPorID(idGrupoEmpresa.Value);

                    if (grupoEmpresa != null)
                    {
                        ViewData["nomeGrupo"] = grupoEmpresa.NomeGrupo;
                        ViewData["idGrupoEmpresa"] = grupoEmpresa.Id;
                    }
                }

                
                return View("~/Views/Tfa/NovoGrupoEmpresaTfa.ascx");
            }
            catch (Exception)
            {
                return MensagemRetorno(false, "Ocorreu um erro ao abrir a tela de cadastro de Grupo de Empresas. Contate o administrador do sistema.");
            }

            
        }

        /// <summary>
        /// Inclui uma Grupo de Empresas
        /// </summary>
        /// <param name="nomeGrupo">Nome do Grupo de Empresas</param>
        /// <returns></returns>
        [HttpPost]
        [Autorizar(Transacao = "GRUPOEMPRESATFA", Acao = "Incluir")]
        public ActionResult IncluirGrupoEmpresa(string nomeGrupo)
        {
            try
            {
                if (string.IsNullOrEmpty(nomeGrupo))
                {
                    return MensagemRetorno(false, "Nome do Grupo de Empresas não pode ser vazio!");
                }

                var grupoEmpresa = _grupoEmpresaTfaService.ObterPorNome(nomeGrupo.ToUpper());

                if (grupoEmpresa != null)
                {
                    return MensagemRetorno(false, "Grupo de Empresa já cadastrado!");
                }

                grupoEmpresa = _grupoEmpresaTfaService.IncluirGrupoEmpresa(nomeGrupo.ToUpper());

                if (grupoEmpresa != null)
                {
                    return MensagemRetorno(true, "Grupo de Empresas incluído com sucesso!");
                }

            }
            catch (Exception)
            {
                return MensagemRetorno(false, "Ocorreu um erro ao tentar incluir registro de Grupo de Empresas. Contate o administrador do sistema.");
            }

            return MensagemRetorno(false, "Ocorreu um erro ao tentar incluir registro de Grupo de Empresas. Contate o administrador do sistema.");
        }

        /// <summary>
        /// Exclui um Grupo de Empresas
        /// </summary>
        /// <param name="idGrupoEmpresa">ID do Grupo de Empresas à ser excluído</param>
        /// <returns></returns>
        [HttpPost]
        [Autorizar(Transacao = "GRUPOEMPRESATFA", Acao = "Excluir")]
        public ActionResult ExcluirGrupoEmpresa(int idGrupoEmpresa)
        {
            try
            {
                _grupoEmpresaTfaService.RemoverGrupoEmpresa(idGrupoEmpresa);
            }
            catch (TranslogicException ex)
            {
                return MensagemRetorno(false, ex.Message);
            }
            catch (Exception)
            {
                return MensagemRetorno(false, "Ocorreu um erro ao excluir Grupo de Empresas. Contate o administrador do sistema.");
            }

            return MensagemRetorno(true, "Grupo de Empresas excluído com sucesso.");
        }


        /// <summary>
        /// Obtem a lista paginada de Grupos de Empresas, filtrando pelo nome do Grupo de Empresas passado como parâmetro
        /// </summary>
        /// <param name="detalhesPaginacaoWeb">Objeto com os parâmetros para paginação</param>
        /// <param name="nomeGrupo">Nome do Grupo de Empresas</param>
        /// <returns></returns>
        [HttpPost]
        [Autorizar(Transacao = "GRUPOEMPRESATFA", Acao = "Pesquisar")]
        public JsonResult PesquisarGruposEmpresas(DetalhesPaginacaoWeb detalhesPaginacaoWeb, string nomeGrupo)
        {

            try
            {
                ResultadoPaginado<GrupoEmpresaTfa> result = null;

                if (string.IsNullOrEmpty(nomeGrupo))
                {
                    result = _grupoEmpresaTfaService.ObterTodosPaginado(detalhesPaginacaoWeb);
                }
                else
                {
                    result = _grupoEmpresaTfaService.ObterPaginadoPorNomeGrupo(detalhesPaginacaoWeb, nomeGrupo.ToUpper());
                }

                if (result.Items == null)
                {
                    result.Items = new List<GrupoEmpresaTfa>();
                }

                return Json(new
                {
                    Total = result.Total,
                    Items = result.Items.Select(p => new
                    {
                        p.Id,
                        p.NomeGrupo
                    }),
                    success = true,
                    Message = ""
                });

            }
            catch (Exception)
            {
                return MensagemRetorno(false, "Ocorreu um erro ao recuperar os Grupos de Empresas. Contate o administrador do sistema.");
            }            
        }

        /// <summary>
        /// Altera o nome do Grupo de Empresas passado como parâmetro
        /// </summary>
        /// <param name="idGrupoEmpresa">ID do Grupo de Empresas à ser alterado</param>
        /// <param name="nomeGrupo">Nome do Grupo de Empresas à ser alterado</param>
        /// <returns></returns>
        [HttpPost]
        [Autorizar(Transacao = "GRUPOEMPRESATFA", Acao = "Editar")]
        public ActionResult AlterarGrupoEmpresa(int idGrupoEmpresa, string nomeGrupo)
        {
            try
            {
                if (idGrupoEmpresa > 0)
                {
                    var grupoEmpresaTfa = _grupoEmpresaTfaService.ObterPorID(idGrupoEmpresa);

                    if (grupoEmpresaTfa == null)
                        return MensagemRetorno(false, "Grupo de Empresas não localizado!");

                    if (!grupoEmpresaTfa.NomeGrupo.ToUpper().Equals(nomeGrupo.ToUpper()))
                    {
                        var grupoEmpresa = _grupoEmpresaTfaService.ObterPorNome(nomeGrupo.ToUpper());

                        if (grupoEmpresa != null)
                        {
                            return MensagemRetorno(false, "Nome do Grupo de Empresas já cadastrado!");
                        }

                        _grupoEmpresaTfaService.AlterarGrupoEmpresa(idGrupoEmpresa, nomeGrupo.ToUpper());
                    }
                    
                    return Json(new
                    {
                        Success = true,
                        Message = "Grupo de Empresas alterado com sucesso!",
                        IdGrupoEmpresa = grupoEmpresaTfa.Id,
                        NomeGrupoEmpresa = grupoEmpresaTfa.NomeGrupo
                    });

                }
                else
                {
                    return MensagemRetorno(false, "ID do Grupo de Empresas Inválido");
                }
            }
            catch (Exception ex)
            {
                return MensagemRetorno(false, ex.Message);
            }
        }

        private JsonResult MensagemRetorno(Boolean Success, string Message)
        {
            return Json(new
            {
                Success = Success,
                Message = Message
            });
        }

        #endregion
    }
}

