﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Translogic.Modules.Core.Domain.Services.Tfa.Interface;
using Translogic.Core.Infrastructure.Web;
using ALL.Core.Dominio;
using Translogic.Modules.Core.Domain.Model.Tfa;
using Translogic.Modules.Core.Infrastructure;
using Translogic.Core.Infrastructure;

namespace Translogic.Modules.Core.Controllers.Tfa
{
    /// <summary>
    /// Classe controller para gestão de Empresas vinculadas a um Grupo de Empresas
    /// </summary>
    public class EmpresaGrupoTfaController : BaseSecureModuleController
    {

        private readonly IGrupoEmpresaTfaService _grupoEmpresaTfaService;
        private readonly IEmpresaGrupoTfaService _empresaGrupoTfaService;

        /// <summary>
        /// Construtor da Classe Controller
        /// </summary>
        /// <param name="grupoEmpresaTfaService">Instância da Classe de Serviços para Grupo de Empresas (DI)</param>
        /// <param name="empresaGrupoTfaService">Instância da Classe de Serviços para Empresa vinculada ao Grupo de Empresas (DI)</param>
        public EmpresaGrupoTfaController(IGrupoEmpresaTfaService grupoEmpresaTfaService, IEmpresaGrupoTfaService empresaGrupoTfaService)
        {
            _grupoEmpresaTfaService = grupoEmpresaTfaService;
            _empresaGrupoTfaService = empresaGrupoTfaService;
        }

        #region ActionViews

        /// <summary>
        /// Retorna a view principal vinculada à Controller
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult EmpresaGrupoTfa()
        {
            try
            {
                return View("~/Views/Tfa/EmpresaGrupoTfa.aspx");
            }
            catch (Exception)
            {
                return MensagemRetorno(false, "Ocorreu um erro ao abrir a tela de Gestão de Empresas de TFA. Contate o administrador do sistema.");
            }
        }


        /// <summary>
        /// Retorna a view para Inclusão de Nova Empresa ao Grupo de Empresas
        /// </summary>
        /// <param name="idGrupoEmpresa">ID do Grupo de Empresas</param>
        /// <returns></returns>
        [HttpPost]
        [Autorizar(Transacao = "EMPRESAGRUPOTFA", Acao = "Incluir")]
        public ActionResult NovoEmpresaGrupoTfa(int idGrupoEmpresa)
        {
            try
            {
                ViewData["idGrupoEmpresa"] = idGrupoEmpresa;

                return View("~/Views/Tfa/NovoEmpresaGrupoTfa.ascx");
            }
            catch (Exception)
            {
                return MensagemRetorno(false, "Ocorreu um erro ao abrir a tela de Cadastro de Empresa no Grupo de Empresas. Contate o administrador do sistema.");
            }

        }

        /// <summary>
        /// Inclui uma Empresa ao Grupo de Empresas, de acordo com os IDs informados
        /// </summary>
        /// <param name="idEmpresa">ID da Empresa</param>
        /// <param name="idGrupoEmpresa">ID do Grupo de Empresas</param>
        /// <returns></returns>
        [HttpPost]
        [Autorizar(Transacao = "EMPRESAGRUPOTFA", Acao = "Incluir")]
        public ActionResult IncluirEmpresaGrupoTfa(string cnpj, int idGrupoEmpresa)
        {
            try
            {
                if (!string.IsNullOrEmpty(cnpj) && idGrupoEmpresa != 0)
                {
                    var empresaGrupo = _empresaGrupoTfaService.IncluirEmpresaGrupoTfa(cnpj, idGrupoEmpresa);

                    if (empresaGrupo != null)
                    {
                        return MensagemRetorno(true, "Empresa incluída com sucesso!");
                    }
                }
                else
                {
                    return MensagemRetorno(false, "Dados para inclusão de Empresa inválidos!");
                }

            }
            catch (TranslogicException ex)
            {
                return MensagemRetorno(false, ex.Message);
            }
            catch (Exception)
            {
                return MensagemRetorno(false, "Ocorreu um erro ao tentar incluir registro no Cadastro de Gestão de Empresas. Contate o administrador do sistema.");
            }

            return MensagemRetorno(false, "Ocorreu um erro ao tentar incluir registro no Cadastro de Gestão de Empresas. Contate o administrador do sistema.");
        }

        /// <summary>
        /// Exclui a Empresa do Grupo de Empresas
        /// </summary>
        /// <param name="idEmpresaGrupoTfa">ID para exclusão da Empresa no Grupo de Empresas</param>
        /// <returns></returns>
        [HttpPost]
        [Autorizar(Transacao = "EMPRESAGRUPOTFA", Acao = "Excluir")]
        public ActionResult ExcluirEmpresaGrupoTfa(int idEmpresaGrupoTfa)
        {
            try
            {
                _empresaGrupoTfaService.ExcluirEmpresaGrupoTfa(idEmpresaGrupoTfa);
            }
            catch (Exception)
            {
                return MensagemRetorno(false, "Ocorreu um erro ao excluir o E-mail. Contate o administrador do sistema.");
            }

            return MensagemRetorno(true, "Empresa excluída do Grupo de Empresas com sucesso!");
        }

        /// <summary>
        /// Obtem a lista paginada de Empresas vinculadas ao Grupo de Empresas
        /// </summary>
        /// <param name="detalhesPaginacaoWeb">Objeto com os parâmetros para paginação</param>
        /// <param name="idGrupoEmpresa">ID do Grupo de Empresas</param>
        /// <returns></returns>
        [HttpPost]
        [Autorizar(Transacao = "EMPRESAGRUPOTFA", Acao = "Pesquisar")]
        public JsonResult ObterListaEmpresaGrupoTfa(DetalhesPaginacaoWeb detalhesPaginacaoWeb, int idGrupoEmpresa)
        {
            try
            {
                if (idGrupoEmpresa != 0)
                {
                    ResultadoPaginado<EmpresaGrupoTfa> result = _empresaGrupoTfaService.ObterTodosPorIdGrupoEmpresa(detalhesPaginacaoWeb, idGrupoEmpresa);

                    if (result.Items == null)
                    {
                        result.Items = new List<EmpresaGrupoTfa>();
                    }

                    return Json(new
                    {
                        Total = result.Total,
                        Items = result.Items.Select(p => new
                        {
                            p.Id,
                            p.Empresa.Cgc,
                            p.Empresa.RazaoSocial
                        }),
                        success = true,
                        Message = ""
                    });

                }

            }
            catch (Exception)
            {
                return MensagemRetorno(false, "Ocorreu um erro ao recuperar as Empresas do Grupo de Empresa. Contate o administrador do sistema.");
            }

            return MensagemRetorno(true, "Nenhuma Empresa Cadastrada para o Grupo de Empresas");
        }

        /// <summary>
        /// Localiza a Empresa no cadastro de Empresas, de acordo com o CNPJ informado
        /// </summary>
        /// <param name="cnpj">CNPJ da Empresa</param>
        /// <returns></returns>
        [HttpPost]        
        public ActionResult ObterEmpresa(string cnpj)
        {
            try
            {
                if (!string.IsNullOrEmpty(cnpj))
                {
                    var empresa = _empresaGrupoTfaService.LocalizarEmpresa(cnpj);

                    if (empresa == null)
                        return MensagemRetorno(false, "Empresa não localizada!");

                    return Json(new
                    {
                        Success = true,
                        Message = "",
                        IdEmpresa = empresa.Id,                        
                        RazaoSocial = empresa.RazaoSocial
                    });

                }
                else
                {
                    return MensagemRetorno(false, "CNPJ Inválido");
                }
            }
            catch (Exception ex)
            {
                return MensagemRetorno(false, ex.Message);
            }
        }


        /// <summary>
        /// Obtem a lista de Grupos de Empresa
        /// </summary>
        /// <returns></returns>        
        public ActionResult ObterListaGrupoEmpresa()
        {
            try
            {
                var result = _grupoEmpresaTfaService.ObterTodos();

                var jsonData = Json(
                    new
                    {
                        Items = result.Select(p => new
                        {
                            p.Id,
                            p.NomeGrupo,
                        })
                    });
                return jsonData;
                
            }
            catch (Exception ex)
            {
                return MensagemRetorno(false, ex.Message);
            }
        }


        private JsonResult MensagemRetorno(Boolean Success, string Message)
        {
            return Json(new
            {
                Success = Success,
                Message = Message
            });
        }

        #endregion
    }
}
