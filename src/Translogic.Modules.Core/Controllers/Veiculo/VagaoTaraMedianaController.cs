namespace Translogic.Modules.Core.Controllers.Veiculo
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using ALL.Core.Dominio;
    using Domain.Model.Trem.Veiculo.Vagao;
    using Domain.Model.Trem.Veiculo.Vagao.Repositories;
    using Infrastructure;
    using Translogic.Core.Infrastructure.Web;

    /// <summary>
	/// Controller respons�vel pelo controle de acesso
	/// </summary>
	public class VagaoTaraMedianaController : BaseSecureModuleController
    {
        private readonly IVagaoRepository _vagaoRepository;
        private readonly IVagaoLogTaraRepository _vagaoLogTaraRepository;
        private readonly IVagaoPatioVigenteRepository _vagaoPatioVigenteRepository;
        private readonly IVagaoMedianaTempPlanilhaRepository _vagaoMedianaTempPlanilhaRepository;
        private readonly IVagaoPesagemPlanilhaRepository _vagaoPesagemPlanilhaRepository;
        private readonly int _TotalRegistrosMarcadosCalculoMediana = 6;
        private readonly int _TotalRegistrosTempSugestao = 20;

        public VagaoTaraMedianaController(IVagaoRepository vagaoRepository, IVagaoLogTaraRepository vagaoLogTaraRepository, IVagaoPatioVigenteRepository vagaoPatioVigenteRepository, IVagaoMedianaTempPlanilhaRepository vagaoMedianaTempPlanilhaRepository, IVagaoPesagemPlanilhaRepository vagaoPesagemPlanilhaRepository)
        {
            _vagaoRepository = vagaoRepository;
            _vagaoLogTaraRepository = vagaoLogTaraRepository;
            _vagaoPatioVigenteRepository = vagaoPatioVigenteRepository;            
            _vagaoMedianaTempPlanilhaRepository = vagaoMedianaTempPlanilhaRepository;
            _vagaoPesagemPlanilhaRepository = vagaoPesagemPlanilhaRepository;
        }

        /// <summary>
        /// Acao Padrao ao ser chamado o controller
        /// </summary>
        /// <returns>View Padrao</returns>
        [Autorizar(Transacao = "VGTARMED")]
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Pesquisar vagoes
        /// </summary>
        /// <param name="pagination"> Paginacao pesquisa</param>
        /// <param name="filter"> Filtros para pesquisa</param>
        /// <returns>Action Result</returns>
        public ActionResult ObterVagoes(DetalhesPaginacaoWeb pagination, DetalhesFiltro<object>[] filter)
        {
            try
            {
                if (!string.IsNullOrEmpty(filter[0].Valor[0].ToString()) ||
                    !string.IsNullOrEmpty(filter[1].Valor[0].ToString()))
                {
                    var codigosDosVagoes = filter[1].Valor[0].ToString();

                    string[] codigosDosVagoesSeparados =
                        codigosDosVagoes.ToUpper()
                                        .Trim()
                                        .Replace(" ", string.Empty)
                                        .Replace(",", ";")
                                        .Replace(".", ";")
                                        .Split(';')
                                        .Where(e => !string.IsNullOrWhiteSpace(e))
                                        .ToArray();

                    ResultadoPaginado<Vagao> listaVagoes = new ResultadoPaginado<Vagao>();

                    // Se o Local n�o foi informado
                    if (string.IsNullOrEmpty(filter[0].Valor[0].ToString().ToUpper()))
                    {
                        var listaRegistros = _vagaoRepository.ObterPorFiltro(pagination,
                                                                      codigosDosVagoesSeparados);
                        var jobject = Json(
                          new
                          {
                              listaRegistros.Total,
                              Items =
                                  listaRegistros.Items.Select(
                                      e =>
                                      new
                                      {
                                          e.Id,
                                          Codigo = e.Codigo,
                                          PesoTara = e.PesoTara != null ? e.PesoTara : e.FolhaEspecificacao.PesoTara,
                                          PesoTotal = CalcularTaraMediana(e)
                                      }),
                              success = true
                          });
                        return jobject;
                    }
                    else
                    {
                        // Se o Local foi informado
                        var listaRegistros = _vagaoPatioVigenteRepository.ObterPorFiltro(
                        pagination,
                        filter[0].Valor[0].ToString().ToUpper(),
                        codigosDosVagoesSeparados);

                        var jobject = Json(
                            new
                            {
                                listaRegistros.Total,
                                Items =
                                    listaRegistros.Items.Select(
                                        e =>
                                        new
                                        {
                                            e.Vagao.Id,
                                            Codigo = e.Vagao.Codigo,
                                            PesoTara = e.Vagao.PesoTara != null ? e.Vagao.PesoTara : e.Vagao.FolhaEspecificacao.PesoTara,
                                            PesoTotal = CalcularTaraMediana(e.Vagao)
                                        }),
                                success = true
                            });
                        return jobject;
                    }



                }
                return Json(new
                        {
                            success = false
                        });
            }
            catch (Exception e)
            {
                return null;
            }
        }

        private double CorrigirValor(double tara)
        {
            if (tara > 9999)
            {
                tara = tara / 1000;
            }
            else if (tara > 999)
            {
                tara = tara / 100;
            }

            return tara;
        }

        private string CalcularTaraMediana(Vagao vagao)
        {
            var tarastemp = _vagaoMedianaTempPlanilhaRepository
                .ObterTodos()
                .Where(x => x.CodVagao == vagao.Codigo);

            if (tarastemp.Any())
            {
                var listaPesagens = new List<VagaoPesagemPlanilha>();

                foreach (var t in tarastemp)
                {
                    listaPesagens.Add(_vagaoPesagemPlanilhaRepository.ObterPorId((int?) t.RegistroPesagem));
                }

                IEnumerable<decimal> listaTaras = listaPesagens.Select(x => x.Tara);

                var media = CalcularMediana(listaTaras);

                return CorrigirValor((Math.Truncate(100 * media) / 100)).ToString();

                // var registros = _vagaoTaraEdiRepository.ObterPorCodigo(codvagao);

                // var registros2 = registros.Where(x => idDosVagoesSeparados.Contains<string>(x.Id.ToString()));

                // foreach (var l in registros2)
                // {
                // }

                // IEnumerable<decimal> listaTaras = tarastemp.Select(x => x.Tara);

                // var med = CalcularMediana(listaTaras);

                // var mediana = (Math.Truncate(100 * med) / 100).ToString();

                // return registroAlterado.TaraNova.ToString();
                
            }

            string mangaSerieVagao = ObterMangaSerieVagao(vagao);

            var registros = _vagaoPesagemPlanilhaRepository
                .ObterPorCodigo(vagao.Codigo)
                .Where(x => this.VerificarTaraIntervaloMangaVagao(mangaSerieVagao, x.Tara))
                .OrderByDescending(x => x.DataRegistro)
                .Take(_TotalRegistrosMarcadosCalculoMediana)
                .ToList();

            if (registros.Count > 0)
            {                
                IEnumerable<decimal> listaTaras = registros.Select(x => x.Tara);

                var media = CalcularMediana(listaTaras);

                return CorrigirValor((Math.Truncate(100 * media) / 100)).ToString();
            }
            else
            {
                return "-";
            }
        }

        /// <summary>
        /// Abre tela de sele��o dos pesos
        /// </summary>
        /// <param name="codVagao">Codigo do vagao</param>
        /// <returns> Tela de sele��o dos pesos </returns>
        public ActionResult DetalhePesagens(string codVagao)
        {
            ViewData["CODIGO_VAGAO"] = codVagao;

            return View();
        }

        /// <summary>
        /// Pesquisar vagoes
        /// </summary>
        /// <param name="codVagao"> codigo do cagao</param>
        /// <returns>Action Result</returns>
        public ActionResult ObterPesagensPlanilha(string codVagao)
        {
            var vagao = _vagaoRepository.ObterPorCodigo(codVagao);
            string mangaSerieVagao = ObterMangaSerieVagao(vagao);
            
            var registros = _vagaoPesagemPlanilhaRepository.ObterPorCodigo(codVagao);            
            var tarastemp = _vagaoMedianaTempPlanilhaRepository.ObterTodos().Where(x => x.CodVagao == codVagao);

            if (tarastemp.Any())
            {                
                var registrosTemp = registros.Where(x => tarastemp.Any(t => t.RegistroPesagem == x.Id)).ToList();
                if (registrosTemp.Count < _TotalRegistrosTempSugestao)
                {
                    int totalFaltantes = _TotalRegistrosTempSugestao - registrosTemp.Count;
                    var registrosFaltantes = registros
                        .Where(x => !registrosTemp.Any(t => t.Id == x.Id)
                            && (this.VerificarTaraIntervaloMangaVagao(mangaSerieVagao, x.Tara)))                        
                        .OrderByDescending(x => x.DataRegistro)
                        .Take(totalFaltantes)
                        .ToList();

                    if (registrosFaltantes != null)
                    {
                        foreach (var r in registrosFaltantes)
                        {
                            r.TrazerMarcadoPraTempMediana = false;
                            registrosTemp.Add(r);                         
                        }
                    }
                }

                return Json(
                        new
                        {
                            Items =
                                 registrosTemp.Select(
                                     e =>
                                     new
                                     {
                                         Marcar = e.TrazerMarcadoPraTempMediana,
                                         e.Id,
                                         CodVagao = e.CodVagao,
                                         Tara = CorrigirValor((double)e.Tara),
                                         DataCadastro = e.DataRegistroStr
                                     }),
                            success = true
                        });
                
            }

            registros = registros
                .OrderByDescending(x => x.DataRegistro)
                .Take(_TotalRegistrosTempSugestao)
                .Where(x => this.VerificarTaraIntervaloMangaVagao(mangaSerieVagao, x.Tara))
                .ToList();

            if (registros != null)
            {
                int idSeqRegistro = 0;
                foreach (var r in registros)
                {
                    if (idSeqRegistro < _TotalRegistrosMarcadosCalculoMediana)
                        r.TrazerMarcadoPraTempMediana = true;
                    else
                        r.TrazerMarcadoPraTempMediana = false;
                    idSeqRegistro += 1;
                }
            }
            

            if (registros.Count < _TotalRegistrosTempSugestao)
            {

                int totalFaltantes = _TotalRegistrosTempSugestao - registros.Count;
                var registrosFaltantes = registros
                    .Where(x => !registros.Any(t => t.Id == x.Id))
                    .OrderByDescending(x => x.DataRegistro)
                    .Take(totalFaltantes)
                    .ToList();

                if (registrosFaltantes != null)
                {
                    foreach (var r in registrosFaltantes)
                    {
                        r.TrazerMarcadoPraTempMediana = false;
                        registros.Add(r);
                    }
                }
            }            

            return Json(
                        new
                        {
                            Items =
                                 registros.Select(
                                     e =>
                                     new
                                     {
                                         Marcar = e.TrazerMarcadoPraTempMediana,
                                         e.Id,
                                         CodVagao = e.CodVagao,
                                         Tara = CorrigirValor((double)e.Tara),
                                         DataCadastro = e.DataRegistroStr
                                     }),
                            success = true
                        });
            
        }

        /// <summary>
        /// Recebe os ids das pesagens a serem consideradas para a nova m�dia
        /// </summary>
        /// <param name="codvagao"> codigo do cagao</param>
        /// <param name="idvagoes"> id dos vagoes</param>
        /// <returns>Action Result</returns>
        [Autorizar(Transacao = "VGTARMED", Acao = "Salvar")]
        public ActionResult SalvarNovaTara(string idvagoes, string codvagao)
        {
            try
            {
                idvagoes = idvagoes.Replace("x", ",");

                string[] idDosVagoesSeparados =
                    idvagoes.ToUpper()
                            .Trim()
                            .Replace(" ", string.Empty)
                            .Replace(",", ";")
                            .Replace(".", ";")
                            .Split(';')
                            .Where(e => !string.IsNullOrWhiteSpace(e))
                            .ToArray();

                var tarastemp = _vagaoMedianaTempPlanilhaRepository.ObterTodos().Where(x => x.CodVagao == codvagao);

                if (tarastemp.Any())
                {                    
                    var registrosPesagemPlanilha = _vagaoPesagemPlanilhaRepository.ObterPorCodigo(codvagao);
                    var registrosTempInserir = registrosPesagemPlanilha.
                        Where(x => idDosVagoesSeparados.Contains<string>(x.Id.ToString())
                              && !tarastemp.Any(t => t.RegistroPesagem == x.Id.Value));
                    var registrosTempExcluir =
                        tarastemp.Where(x => !idDosVagoesSeparados.Contains<string>(x.RegistroPesagem.ToString()));

                    // Insere Novos                    
                    foreach (var t in registrosTempInserir)
                    {

                        try
                        {
                            _vagaoMedianaTempPlanilhaRepository.Inserir(new VagaoMedianaTempPlanilha
                            {
                                CodVagao = codvagao,
                                DataRegistro = DateTime.Now,
                                RegistroPesagem = (decimal)t.Id
                            });
                        }
                        catch (Exception e)
                        {
                            Console.Write(e);
                        }    
                    }

                    // Remove os que foram desmarcados                    
                    foreach (var t in registrosTempExcluir)
                    {
                        var regTempExclusao = tarastemp.Where(x => x.RegistroPesagem == t.RegistroPesagem).FirstOrDefault();

                        if (regTempExclusao != null)
                        {
                            _vagaoMedianaTempPlanilhaRepository.Remover(regTempExclusao.Id);
                        }
                    }
                    
                }
                else
                {
                    var registrosPesagemPlanilha = _vagaoPesagemPlanilhaRepository
                        .ObterPorCodigo(codvagao);
                    var registros2 = registrosPesagemPlanilha
                        .Where(x => idDosVagoesSeparados.Contains<string>(x.Id.ToString()));

                    foreach (var l in registros2)
                    {
                        //l.Tara = (decimal)CorrigirValor((double)l.Tara);
                        try
                        {
                            _vagaoMedianaTempPlanilhaRepository.Inserir(new VagaoMedianaTempPlanilha
                            {
                                CodVagao = codvagao,
                                DataRegistro = DateTime.Now,
                                RegistroPesagem = (decimal)l.Id
                            });
                        }
                        catch (Exception e)
                        {
                            Console.Write(e);
                        }
                    }    
                }

                var jobject = Json(
                    new
                    {
                        success = true
                    });

                return jobject;
            }
            catch (Exception e)
            {
                var jobject = Json(
                   new
                   {
                       success = false,
                       mensagem = e
                   });

                return jobject;
            }
        }

        /// <summary>
        /// Recebe os ids dos vagoes e grava a nova tara que est� em mem�ria
        /// </summary>
        /// <param name="idvagoes"> id dos vagoes</param>
        /// <returns>Action Result</returns>
        [Autorizar(Transacao = "VGTARMED", Acao = "Salvar")]
        public ActionResult SalvarTarasAlteradas(string idvagoes)
        {
            string[] idDosVagoesSeparados =
                        idvagoes.ToUpper()
                                        .Trim()
                                        .Replace(" ", string.Empty)
                                        .Replace(",", ";")
                                        .Replace(".", ";")
                                        .Split(';')
                                        .Where(e => !string.IsNullOrWhiteSpace(e))
                                        .ToArray();

            foreach (var idVagao in idDosVagoesSeparados)
            {
                var vagao = _vagaoRepository.ObterPorId(Convert.ToInt32(idVagao));
                var tarastemp = _vagaoMedianaTempPlanilhaRepository.ObterTodos().Where(x => x.CodVagao == vagao.Codigo && x.DataRegistro.Date == DateTime.Now.Date);
                
                // SE EXISTIREM VALORS NA TABELA TEMPORARIA DE PESAGENS SELECIONADAS 
                // GRAVA MEDIANA CALCULADA A PARTIR DESTES VALORES
                if (tarastemp.Any())
                {
                    var listaPesagens = new List<VagaoPesagemPlanilha>();

                    foreach (var t in tarastemp)
                    {
                        listaPesagens.Add(_vagaoPesagemPlanilhaRepository.ObterPorId((int?)t.RegistroPesagem));
                    }

                    IEnumerable<decimal> listaTaras = listaPesagens.Select(x => x.Tara);

                    var media = CalcularMediana(listaTaras);

                    var valorCorrigido = (Math.Truncate(100 * media) / 100);

                    try
                    {
                        var logTara = new VagaoLogTara
                                          {
                                              CodVagao = vagao.Codigo,
                                              TaraNova = (decimal) valorCorrigido,
                                              Usuario = UsuarioAtual.Codigo,
                                              TaraAntiga = vagao.PesoTara != null ? (decimal)vagao.PesoTara : (decimal)vagao.FolhaEspecificacao.PesoTara,
                                              DataCadastro = DateTime.Now
                                          };

                        vagao.PesoTara = valorCorrigido;
                        _vagaoRepository.InserirOuAtualizar(vagao);
                        _vagaoLogTaraRepository.Inserir(logTara);

                        // Remove da tempor�ria de todos os vag�es selecionados (Zera a tempor�ria para estes vag�es)
                        foreach (var t in tarastemp)
                        {
                            _vagaoMedianaTempPlanilhaRepository.Remover(t.Id);
                        }
                    }
                    catch (Exception e)
                    {
                        Console.Write(e);
                    }
                }

                    /*var logTara = (VagaoLogTara)Session[vagao.Codigo + UsuarioAtual.Codigo + DateTime.Today.Date.ToShortTimeString()];

                    if (logTara != null)
                    {   
                        try
                        {
                            vagao.PesoTara = (double?) logTara.TaraNova;
                            _vagaoRepository.InserirOuAtualizar(vagao);

                            logTara.TaraAntiga = vagao.PesoTara != null ? (decimal)vagao.PesoTara : (decimal)vagao.FolhaEspecificacao.PesoTara ;
                            logTara.DataCadastro = DateTime.Now;
                    
                            _vagaoLogTaraRepository.Inserir(logTara);

                            Session[vagao.Codigo + UsuarioAtual.Codigo + DateTime.Today.Date.ToShortTimeString()] = null;
                        }
                        catch (Exception e)
                        {
                            Console.Write(e);
                        }
                    }*/
                else
                {

                    string mangaSerieVagao = ObterMangaSerieVagao(vagao);

                    var registros =
                        _vagaoPesagemPlanilhaRepository
                            .ObterPorCodigo(vagao.Codigo)
                            .Where(x => this.VerificarTaraIntervaloMangaVagao(mangaSerieVagao, x.Tara))
                            .OrderByDescending(x => x.DataRegistro)
                            .Take(_TotalRegistrosMarcadosCalculoMediana)
                            .ToList();

                    // SE EXISTIREM VALORES DE PESAGENS NO EDI PARA SER CALCULADA A MEDIANA 
                    // ULTILIZA OS ULTIMOS 6 PARA CALCULAR A MEDIANA (_TotalRegistrosMarcadosCalculoMediana)
                    if (registros.Count > 0)
                    {
                        IEnumerable<decimal> listaTaras = registros.Select(x => x.Tara);

                        var media = CalcularMediana(listaTaras);

                        var valorCorrigido = CorrigirValor((Math.Truncate(100*media)/100));

                        var logTara = new VagaoLogTara
                                      {
                                          CodVagao = vagao.Codigo,
                                          DataCadastro = DateTime.Now,
                                          TaraAntiga =
                                              vagao.PesoTara != null
                                                  ? (decimal) vagao.PesoTara
                                                  : (decimal) vagao.FolhaEspecificacao.PesoTara,
                                          TaraNova = (decimal) valorCorrigido,
                                          Usuario = UsuarioAtual.Codigo
                                      };

                        vagao.PesoTara = valorCorrigido;
                        _vagaoRepository.InserirOuAtualizar(vagao);
                        _vagaoLogTaraRepository.Inserir(logTara);
                    }
                    else
                    {
                        // SE N�O HOUVER REGISTROS TEMPORARIOS SELECIONADOS
                        // NEM REGISTROS VINDOS DAS PESAGENS DO EDI
                        // ASSUME O VALOR DA FOLHA DE ESPECIFICACAO
                        if (vagao.FolhaEspecificacao.PesoTara > 0)
                        {
                            var logTara = new VagaoLogTara
                                          {
                                              CodVagao = vagao.Codigo,
                                              DataCadastro = DateTime.Now,
                                              TaraAntiga = vagao.PesoTara.HasValue ? (decimal)vagao.PesoTara.Value : (decimal)0.00,
                                              TaraNova = (decimal) vagao.FolhaEspecificacao.PesoTara,
                                              Usuario = UsuarioAtual.Codigo
                                          };

                            vagao.PesoTara = vagao.FolhaEspecificacao.PesoTara;
                            _vagaoRepository.InserirOuAtualizar(vagao);
                            _vagaoLogTaraRepository.Inserir(logTara);
                        }
                    }
                }
            }

            var jobject = Json(
                        new
                        {
                            success = true
                        });

            return jobject;
        }
        
        public static double CalcularMediana(IEnumerable<decimal> source)
        {
            var sortedList = from number in source
                             orderby number
                             select number;

            int count = sortedList.Count();
            int itemIndex = count / 2;
            if (count % 2 == 0) // Even number of items. 
                return (double) ((sortedList.ElementAt(itemIndex) +
                                  sortedList.ElementAt(itemIndex - 1)) / 2);
            // Odd number of items. 
            return (double) sortedList.ElementAt(itemIndex);
        }

        private bool VerificarTaraIntervaloMangaVagao(string manga, decimal tara)
        {            
            switch (manga.ToUpper())
            {
                case "T": // Manga do Vag�o T
                    return ((double) tara >= 23.5) && ((double)tara <= 32.0);
                case "R": // Manga do Vag�o R
                    return ((double)tara >= 15) && ((double)tara <= 27);
                case "S": // Manga do Vag�o S         
                    return ((double)tara >= 21.5) && ((double)tara <= 28.5);
                    break;
                default:
                    return true;                    
            }

            return true;
        }

        private string ObterMangaSerieVagao(Vagao vagao)
        {
            string mangaSerieVagao = "";
            if (vagao.Serie != null)
            {
                mangaSerieVagao = vagao.Serie.Codigo;
                if (!String.IsNullOrEmpty(mangaSerieVagao))
                    mangaSerieVagao = mangaSerieVagao.Substring(mangaSerieVagao.Length - 1, 1);
            }
            return mangaSerieVagao;
        }
    }
}