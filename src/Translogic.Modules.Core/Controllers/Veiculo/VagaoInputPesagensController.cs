namespace Translogic.Modules.Core.Controllers.Veiculo
{
    using System;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Web;
    using System.Web.Mvc;
    using OfficeOpenXml;
    using Translogic.Modules.Core.Domain.Model.Codificador;
    using Translogic.Modules.Core.Domain.Model.Codificador.Repositories;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao.Repositories;
    using Translogic.Modules.Core.Infrastructure;

    /// <summary>
	/// Controller responsável pelo controle de acesso
	/// </summary>
	public class VagaoInputPesagensController : BaseSecureModuleController
    {
        private readonly IConfiguracaoTranslogicRepository _configuracaoTranslogicRepository;
        private readonly IVagaoPesagemPlanilhaRepository _vagaoPesagemPlanilhaRepository;

        public VagaoInputPesagensController(IVagaoPesagemPlanilhaRepository vagaoPesagemPlanilhaRepository, IConfiguracaoTranslogicRepository configuracaoTranslogicRepository)
        {
            _vagaoPesagemPlanilhaRepository = vagaoPesagemPlanilhaRepository;
            _configuracaoTranslogicRepository = configuracaoTranslogicRepository;
        }

        /// <summary>
        /// Acao Padrao ao ser chamado o controller
        /// </summary>
        /// <returns>View Padrao</returns>
        [Autorizar(Transacao = "UPLOAD_TARA")]
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Metodo Receber a planilha Excel
        /// </summary>
        /// <returns>
        /// Retorna Status se foi cadastrado com sucesso
        /// </returns>
        [HttpPost]
        [Autorizar(Transacao = "UPLOAD_TARA", Acao = "Salvar")]
        public JsonResult Salvar()
        {
            try
            {
                JsonResult jsonTmp;
                HttpPostedFileBase arquivo = Request.Files["fieldArquivo"];
                ConfiguracaoTranslogic path = _configuracaoTranslogicRepository.ObterPorId("PASTA_INPUT_PESAGENS");

                if (arquivo != null)
                {
                    var extension = Path.GetExtension(arquivo.FileName);
                    if (extension != null && extension.ToUpper() != ".XLS" && extension.ToUpper() != ".XLSX")
                    {
                        jsonTmp = Json(new { success = false, message = "Erro ao gravar dados da planilha" });
                        jsonTmp.ContentType = "text/html";
                        return jsonTmp;
                    }
                    else
                    {
                        using (var excel = new ExcelPackage(arquivo.InputStream))
                        {
                            ExcelWorksheet workSheet = excel.Workbook.Worksheets.First();
                            var start = workSheet.Dimension.Start;
                            var end = workSheet.Dimension.End;

                            for (var row = start.Row; row <= end.Row; row++)
                            {
                                try
                                {
                                    var valido = true;
                                    var serieVagao = workSheet.Cells[row, 1].Text;
                                    valido = valido & !string.IsNullOrEmpty(serieVagao); 
                                    
                                    decimal codVagao;
                                    var codVagaoStr = workSheet.Cells[row, 2].Text;
                                    valido = valido & decimal.TryParse(codVagaoStr, out codVagao);

                                    DateTime dataPesagem;
                                    var dataPesagemStr = workSheet.Cells[row, 3].Text;
                                    valido = valido & DateTime.TryParse(dataPesagemStr, out dataPesagem);

                                    var cliente = workSheet.Cells[row, 4].Text;
                                    valido = valido & !string.IsNullOrEmpty(cliente);

                                    var terminal = workSheet.Cells[row, 5].Text;
                                    valido = valido & !string.IsNullOrEmpty(terminal); 

                                    decimal pesoBruto;
                                    var pesoBrutoStr = workSheet.Cells[row, 6].Text.Replace(".", ",");
                                    valido = valido & decimal.TryParse(pesoBrutoStr, out pesoBruto);

                                    decimal pesoTara;
                                    var pesoTaraStr = workSheet.Cells[row, 7].Text.Replace(".", ",");
                                    valido = valido & decimal.TryParse(pesoTaraStr, out pesoTara);

                                    if (valido)
                                    {
                                        _vagaoPesagemPlanilhaRepository.Inserir(new VagaoPesagemPlanilha
                                        {
                                            Serie = serieVagao,
                                            CodVagao = codVagaoStr,
                                            DataRegistro = DateTime.Now,
                                            Tara = pesoTara,
                                            Usuario = UsuarioAtual.Codigo,
                                            Cliente = cliente,
                                            DataPesagem = dataPesagem,
                                            PesoBruto = pesoBruto,
                                            Terminal = terminal,
                                            Timestamp = DateTime.Now,
                                            IndProcessado = false,
                                            IndProcessadoComErro = false
                                        });
                                    }
                                }
                                catch (Exception e)
                                {
                                    Log("Input Pesagens - " + e, EventLogEntryType.Information);
                                }
                            }
                            System.IO.File.WriteAllText(path.Valor + UsuarioAtual.Codigo + "_" + string.Format("{0:yyyyMMddHHmmss}", DateTime.Now) + extension, arquivo.InputStream.ToString(), Encoding.UTF8);
                            
                            jsonTmp = Json(new { success = true, message = "Sucesso" });
                            jsonTmp.ContentType = "text/html";
                            return jsonTmp;
                        }
                    }
                }

                jsonTmp = Json(new { success = false, message = "Erro ao gravar dados da planilha" });
                jsonTmp.ContentType = "text/html";
                return jsonTmp;
            }
            catch (Exception e)
            {
                Log("Input Pesagens - " + e, EventLogEntryType.Information);

                JsonResult jsonTmp = Json(new { success = false, message = e.Message, e.StackTrace });
                jsonTmp.ContentType = "text/html";
                return jsonTmp;
            }
        }

        private void Log(string message, EventLogEntryType type)
        {
            var source = "Translogic";

            if (!EventLog.SourceExists(source))
            {
                EventLog.CreateEventSource(source, source);
            }

            EventLog.WriteEntry(source, message, type);
        }
    }
}