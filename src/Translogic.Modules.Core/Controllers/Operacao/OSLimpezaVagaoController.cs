﻿namespace Translogic.Modules.Core.Controllers.Operacao
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using Translogic.Core.Infrastructure.Web;
    using Translogic.Core.Infrastructure.Web.ContentResults;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Modules.Core.Infrastructure;
    using Translogic.Modules.Core.Domain.Services.Fornecedor.Interface;
    using Translogic.Modules.Core.Domain.Services.OSLimpezaVagao.Interface;
    using Translogic.Modules.Core.Domain.Services.Mdfes;
    using Translogic.Core.Infrastructure.Web.Helpers;
    using System.IO;
    using Translogic.Core.Infrastructure.FileSystem.ExtensionMethods;


    public class OSLimpezaVagaoController : BaseSecureModuleController
    {
        private readonly IOSLimpezaVagaoService _osLimpezaVagaoService;
        private readonly IOSStatusService _osStatusService;
        private readonly IOSTipoService _osTipoService;
        private readonly IFornecedorOsService _fornecedorOsService;
        private readonly MdfeService _mdfeService;

        public OSLimpezaVagaoController(IOSLimpezaVagaoService osLimpezaVagaoService,
            IOSStatusService osStatusService,
            IOSTipoService osTipoService,
            IFornecedorOsService fornecedorOsService,
            MdfeService mdfeService)
        {
            _osLimpezaVagaoService = osLimpezaVagaoService;
            _osStatusService = osStatusService;
            _osTipoService = osTipoService;
            _fornecedorOsService = fornecedorOsService;
            _mdfeService = mdfeService;
        }

        #region ActionViews

        /// <summary>
        /// Painel de OS de Limpeza - Visualização
        /// </summary>
        [Autorizar(Transacao = "OPEOSLIMPVGO")]
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Action da tela de criação da OS. Precisa do ID para recuperar as informações dad OS
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Autorizar(Transacao = "OPEOSLIMPVGO")]
        public ActionResult CriarOSLimpeza(int idOS, string local)
        {
            ViewData["idOs"] = idOS;
            ViewData["local"] = local;
            ViewData["data"] = DateTime.Now.ToString("dd/MM/yyyy HH:mm");

            return View();
        }

        /// <summary>
        /// Action para view de pesquisa de vagões
        /// </summary>
        /// <returns></returns>
        [Autorizar(Transacao = "OPEOSLIMPVGO")]
        public ActionResult PesquisarVagoes(int? idOs, string local)
        {
            if (idOs.HasValue)
            {
                ViewData["local"] = local;
                ViewData["idOs"] = idOs.Value;
            }

            return View();
        }

        /// <summary>
        /// Action para vis de Edição
        /// </summary>
        /// <param name="idOS"></param>
        /// <returns></returns>
        [Autorizar(Transacao = "OPEOSLIMPVGO")]
        public ActionResult EditarOSLimpeza(int idOS)
        {
            var os = _osLimpezaVagaoService.ObterOS(idOS);

            ViewData["idOs"] = idOS;
            ViewData["local"] = os.LocalServico.ToUpper();
            ViewData["data"] = os.Data.ToString("dd/MM/yyyy HH:mm");
            ViewData["idFornecedor"] = os.IdFornecedor.ToString();

            return View();
        }

        /// <summary>
        /// View para fechamento de OS
        /// </summary>
        /// <param name="idOS"></param>
        /// <returns></returns>
        [Autorizar(Transacao = "OPEOSLIMPVGO")]
        public ActionResult FecharOSLimpeza(int idOS)
        {
            var os = _osLimpezaVagaoService.ObterOS(idOS);

            ViewData["idOs"] = idOS;
            ViewData["local"] = os.LocalServico.ToUpper();
            ViewData["data"] = os.Data.ToString("dd/MM/yyyy HH:mm");
            ViewData["idFornecedor"] = os.IdFornecedor.ToString();
            ViewData["idTipo"] = os.IdTipo.ToString();

            return View();
        }

        /// <summary>
        /// View para cancelamento de OS
        /// </summary>
        /// <param name="idOS"></param>
        /// <returns></returns>
        [Autorizar(Transacao = "OPEOSLIMPVGO")]
        public ActionResult CancelarOSLimpeza(int idOS)
        {
            var os = _osLimpezaVagaoService.ObterOS(idOS);

            ViewData["idOs"] = idOS;
            ViewData["local"] = os.LocalServico.ToUpper();
            ViewData["data"] = os.Data.ToString("dd/MM/yyyy HH:mm");
            ViewData["idFornecedor"] = os.IdFornecedor.ToString();

            return View();
        }

        /// <summary>
        /// Action para view de Visualização
        /// </summary>
        /// <param name="idOS"></param>
        /// <returns></returns>
        [Autorizar(Transacao = "OPEOSLIMPVGO")]
        public ActionResult VisualizarOSLimpeza(int idOS)
        {
            ViewData["idOs"] = idOS;
            return View();
        }

        /// <summary>
        /// Retorna o PDF da OS
        /// </summary>
        /// <param name="idOS">id da OS</param>
        /// <returns>resultado json</returns>
        [Autorizar(Transacao = "RELOSLIMPVGO")]
        public ActionResult Imprimir(int idOS)
        {
            Stream pdf = null;
            Stream returnContent = new MemoryStream();

            var merge = new PdfMerge();

            var os = _osLimpezaVagaoService.ObterOS(idOS);

            ViewData["idOs"] = idOS;
            ViewData["IdOsParcial"] = (os.IdOsParcial.ToString() != "0" ? os.IdOsParcial.ToString() : "");
            ViewData["Data"] = os.Data.ToString("dd/MM/yyyy HH:mm");
            ViewData["IdFornecedor"] = os.IdFornecedor.ToString();
            ViewData["Fornecedor"] = os.Fornecedor.ToString();
            ViewData["idLocal"] = os.idLocal;
            ViewData["LocalServico"] = os.LocalServico.ToUpper();
            ViewData["IdTipo"] = os.IdTipo;
            ViewData["Tipo"] = os.Tipo;
            ViewData["IdStatus"] = os.IdStatus;
            ViewData["Status"] = os.Status;
            ViewData["DataHoraDevolucaoOS"] = (os.DataHoraDevolucaoOS != new DateTime() ? os.DataHoraDevolucaoOS.ToString("dd/MM/yyyy HH:mm") : "");
            ViewData["DataHoraEntregaOSFornecedor"] = (os.DataHoraEntregaOSFornecedor != new DateTime() ? os.DataHoraEntregaOSFornecedor.ToString("dd/MM/yyyy HH:mm") : "");
            ViewData["NomeAprovadorFornecedor"] = os.NomeAprovadorFornecedor;
            ViewData["NomeAprovadorRumo"] = os.NomeAprovadorRumo;
            ViewData["MatriculaAprovadorRumo"] = os.MatriculaAprovadorRumo;
            ViewData["MatriculaAprovadorFornecedor"] = os.MatriculaAprovadorFornecedor;
            ViewData["UltimaAlteracao"] = os.UltimaAlteracao.ToString("dd/MM/yyyy HH:mm");
            ViewData["Usuario"] = os.Usuario;
            ViewData["Justificativa"] = os.JustificativaCancelamento;

            ViewData["Vagoes"] = _osLimpezaVagaoService.ObterVagoesOS(null, idOS).Items;

            var viewString = View("ImpressaoOS").Capture(ControllerContext);

            pdf = _mdfeService.HtmlToPdf(viewString, "pdfOS", true);

            if (pdf != null)
            {
                merge.AddFile(pdf);
                returnContent = merge.Execute();
            }

            var fsr = new FileStreamResult(returnContent, "application/pdf");
            fsr.FileDownloadName = "OS " + idOS.ToString() + ".pdf";
            return fsr;
        }

        #endregion

        #region Métodos

        /// <summary>
        /// 
        /// </summary>
        /// <param name="listaDeVagoes"></param>
        /// <param name="idLocal"></param>
        /// <param name="idOS"></param>
        /// <param name="idLotacao"></param>
        /// <param name="idSituacao"></param>
        /// <param name="idMotivo"></param>
        /// <returns></returns>
        [Autorizar(Transacao = "OPEOSLIMPVGO")]
        public ActionResult ObterConsultarOSLimpezaVagoesExportar(string dataInicial, string dataFinal, string local, string numOs, string idTipo, string idMotivo, string idStatus, string retrabalho)
        {
            var result = this._osLimpezaVagaoService.ObterConsultaOsLimpezaExportar(
                                                                    dataInicial,
                                                                    dataFinal,
                                                                    local,
                                                                    numOs,
                                                                    idTipo,
                                                                    idStatus,
                                                                    retrabalho
                                                                   );

            var dicOSLimpezaVagao = new Dictionary<string, Func<OSLimpezaVagaoExportaDto, string>>();

            dicOSLimpezaVagao["Data"] = c => (c.Data != null) ? c.Data.ToString("dd/MM/yyyy HH:mm") : String.Empty;
            dicOSLimpezaVagao["OS"] = c => c.IdOsParcial > 0 ? string.Format("{0} ({1})", c.IdOs, c.IdOsParcial) : c.IdOs.ToString();
            dicOSLimpezaVagao["Local"] = c => !String.IsNullOrEmpty(c.Local) ? c.Local : String.Empty;
            dicOSLimpezaVagao["Fornecedor"] = c => !String.IsNullOrEmpty(c.Fornecedor) ? c.Fornecedor : String.Empty;
            dicOSLimpezaVagao["Entrega O.S. Fornecedor"] = c => (c.DataHoraEntregaOSFornecedor != new DateTime()) ? c.DataHoraEntregaOSFornecedor.ToString("dd/MM/yyyy HH:mm") : String.Empty;
            dicOSLimpezaVagao["Devolução O.S. Rumo"] = c => (c.DataHoraDevolucaoOS != new DateTime()) ? c.DataHoraDevolucaoOS.ToString("dd/MM/yyyy HH:mm") : String.Empty;
            dicOSLimpezaVagao["Vagão"] = c => !String.IsNullOrEmpty(c.NumeroVagao) ? c.NumeroVagao : String.Empty;
            dicOSLimpezaVagao["Limpeza"] = c => (c.Limpeza > 0 ? "Sim" : "Não");
            dicOSLimpezaVagao["Lavagem"] = c => (c.Lavagem > 0 ? "Sim" : "Não");
            dicOSLimpezaVagao["Retrabalho"] = c => (c.Retrabalho > 0 ? "Sim" : "Não");
            dicOSLimpezaVagao["Observação"] = c => !String.IsNullOrEmpty(c.Observacao) ? c.Observacao : String.Empty;
            dicOSLimpezaVagao["Aprovador RUMO"] = c => !String.IsNullOrEmpty(c.NomeAprovadorRumo) ? c.NomeAprovadorRumo : String.Empty;
            dicOSLimpezaVagao["Status OS"] = c => !String.IsNullOrEmpty(c.Status) ? c.Status : String.Empty;
            dicOSLimpezaVagao["Últ. Atualização OS"] = c => (c.UltimaAlteracao != null) ? c.UltimaAlteracao.ToString("dd/MM/yyyy HH:mm") : String.Empty;
            dicOSLimpezaVagao["Usuário"] = c => c.Usuario;

            return this.Excel(string.Format("OSLimpezaVagoes_{0}_{1}.xls", DateTime.Now.ToString("ddMMyyyy"), DateTime.Now.ToString("HHmmss")), dicOSLimpezaVagao, result);
        }

        /// <summary>
        /// Salvar vagões da OS gerando o ID da OS
        /// </summary>
        /// <param name="idsVagoes">Lista de Ids de vagões da OS</param>
        [Autorizar(Transacao = "OPEOSLIMPVGO")]
        public int SalvarVagoesDaOs(int? idOs, IList<int> idsVagoes, string local)
        {
            idOs = _osLimpezaVagaoService.InserirAtualizarVagoes(idOs, idsVagoes, UsuarioAtual.Codigo, local);

            return idOs.Value;
        }

        /// <summary>
        /// Salvar/atualizar a OS e seus vagões
        /// </summary>
        /// <param name="idOs"></param>
        /// <param name="idFornecedor"></param>
        /// <param name="local"></param>
        /// <param name="idTipo"></param>
        /// <param name="vagoes"></param>
        [Autorizar(Transacao = "OPEOSLIMPVGO")]
        public int SalvarOS(int idOs, int idFornecedor, string local)
        {
            if (idOs == 0)
                throw new Exception("Favor fornecer o número da OS para atualizar");

            if (idFornecedor == 0)
                throw new Exception("Campo fornecedor obrigatório");

            if (string.IsNullOrEmpty(local))
                throw new Exception("Campo local de prestação de serviço obrigatório");

            _osLimpezaVagaoService.AtualizarOSeVagoes(idOs, idFornecedor, local);

            return 1;
        }

        /// <summary>
        /// Editar OS, no caso somente fornecedor
        /// </summary>
        /// <param name="idOs"></param>
        /// <param name="idFornecedor"></param>
        [Autorizar(Transacao = "OPEOSLIMPVGO")]
        public int EditarOs(int idOs, int idFornecedor)
        {
            if (idOs == 0)
                throw new Exception("Favor fornecer o número da OS para atualizar");

            if (idFornecedor == 0)
                throw new Exception("Campo fornecedor obrigatório");

            _osLimpezaVagaoService.EditarOs(idOs, idFornecedor);

            return 1;
        }

        /// <summary>
        /// Cancelar OS atualizando status para 'Cancelada'
        /// </summary>
        /// <param name="idOs"></param>
        [Autorizar(Transacao = "OPEOSLIMPVGO")]
        public JsonResult CancelarOS(int idOs, string justificativa)
        {
            try
            {
                #region Validações

                if (idOs == 0)
                    throw new Exception("Favor fornecer o número da OS para cancelamento");

                if (string.IsNullOrEmpty(justificativa))
                    throw new Exception("Campo justificativa de cancelamento é obrigatório para cancelamento");
                
                if (justificativa.Length > 250)
                    throw new Exception("A justificativa de cancelamento pode conter somente até 250 caracteres");

                justificativa = (justificativa != null ? justificativa.Replace("<", "").Replace(">", "") : "");

                #endregion

                try
                {
                    _osLimpezaVagaoService.CancelarOs(idOs, justificativa);
                }
                catch (Exception ex)
                {
                    throw new Exception("Não foi possivel atualizar a OS. Erro: " + ex.Message);
                }

                return Json(new
                {
                    Success = true,
                    Status = 200,
                    idOs = idOs
                });
            }
            catch (Exception e)
            {
                return Json(new
                {
                    Success = false,
                    e.Message,
                    Status = 200
                });
            }
        }

        /// <summary>
        /// Chamada de método que cancela OS ao voltar.
        /// </summary>
        /// <param name="idOs"></param>
        /// <returns></returns>
        [Autorizar(Transacao = "OPEOSLIMPVGO")]
        public int Voltar(int idOs)
        {
            if (idOs == 0)
                throw new Exception("Favor fornecer o número da OS para cancelamento");
            try
            {
                return _osLimpezaVagaoService.CancelarOs(idOs);
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possivel atualizar a OS. Erro: " + ex.Message);
            }

        }

        /// <summary>
        /// Atualizar a OS com dados de fechamento e atualizar status para 'Fechada'
        /// </summary>
        /// <param name="idOs"></param>
        /// <param name="dataHoraEntregaOsFornecedor"></param>
        /// <param name="dataHoraDevolucaoOSRumo"></param>
        /// <param name="nomeAprovadorRumo"></param>
        /// <param name="matriculaAprovadorRumo"></param>
        /// <param name="nomeAprovadorFornecedor"></param>
        /// <param name="matriculaAprovadorFornecedor"></param>
        [Autorizar(Transacao = "OPEOSLIMPVGO")]
        public int FecharOs(int idOs, int idFornecedor, int idTipo, string dataEntregaOsFornecedor, string horaEntregaOsFornecedor,
            string dataDevolucaoOSRumo, string horaDevolucaoOSRumo, string nomeAprovadorRumo, string matriculaAprovadorRumo,
            string nomeAprovadorFornecedor, string matriculaAprovadorFornecedor, IList<OSLimpezaVagaoItemDto> vagoes)
        {
            if (idOs == 0)
                throw new Exception("Favor fornecer o número da OS para fechamento");
            if (idFornecedor == 0)
                throw new Exception("Campo fornecedor é obrigatório!");
            if (string.IsNullOrEmpty(dataEntregaOsFornecedor) || string.IsNullOrEmpty(horaEntregaOsFornecedor))
                throw new Exception("Data e hora da entrega da OS para o fornecedor é obrigatório");
            if (string.IsNullOrEmpty(dataDevolucaoOSRumo) || string.IsNullOrEmpty(horaDevolucaoOSRumo))
                throw new Exception("Data e hora da devolução da OS para a Rumo é obrigatório");
            if (string.IsNullOrEmpty(nomeAprovadorRumo))
                throw new Exception("Nome do Aprovador na Rumo é obrigatório");
            if (string.IsNullOrEmpty(nomeAprovadorFornecedor))
                throw new Exception("Nome do Aprovador no Fornecedor é obrigatório");



            try
            {
                return _osLimpezaVagaoService.FecharOs(idOs, idFornecedor, idTipo, dataEntregaOsFornecedor, horaEntregaOsFornecedor, dataDevolucaoOSRumo, horaDevolucaoOSRumo,
                    nomeAprovadorRumo, matriculaAprovadorRumo, nomeAprovadorFornecedor, matriculaAprovadorFornecedor, UsuarioAtual.Codigo, vagoes);
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possivel atualizar a OS. Erro: " + ex.Message);
            }
        }

        /// <summary>
        /// Salvar observação do vagão
        /// </summary>
        /// <param name="idItem"></param>
        /// <param name="observacao"></param>
        /// <returns></returns>
        [Autorizar(Transacao = "OPEOSLIMPVGO")]
        public int SalvaObsVagao(int idItem, string observacao)
        {
            return _osLimpezaVagaoService.SalvarObservacaoVagao(idItem, observacao.Replace("<", "").Replace(">", ""));
        }

        #endregion

        #region JsonResults

        /// <summary>
        /// Retornar tipos 
        /// </summary>
        /// <returns></returns>
        public JsonResult ObterTipos()
        {
            var resultado = _osTipoService.ObterTiposOSLimpeza();

            var jsonData = Json(
                new
                {
                    Items = resultado.Select(i => new
                    {
                        i.IdTipo,
                        i.DescricaoTipo
                    }),
                    Total = resultado.Count
                });
            return jsonData;
        }

        /// <summary>
        /// Retornar status
        /// </summary>
        /// <returns></returns>
        public JsonResult ObterStatus()
        {
            var resultado = _osStatusService.ObterStatus();

            resultado.Insert(resultado.Count, new OSStatusDto { IdStatus = 99, DescricaoStatus = "Aberta/Parcial" });

            var jsonData = Json(
                new
                {
                    Items = resultado.Select(i => new
                    {
                        i.IdStatus,
                        i.DescricaoStatus
                    }),
                    Total = resultado.Count
                });
            return jsonData;
        }

        /// <summary>
        /// Retonar as lotações
        /// </summary>
        /// <returns></returns>
        public JsonResult ObterLotacoes()
        {
            var resultado = _osLimpezaVagaoService.ObterLotacoes();

            var jsonData = Json(
                new
                {
                    Items = resultado.Select(i => new
                    {
                        i.IdLotacao,
                        i.DescricaoLotacao
                    }),
                    Total = resultado.Count
                });
            return jsonData;
        }

        /// <summary>
        /// Retornar fornecedores filtrados por ativos e local
        /// </summary>
        /// <returns></returns>
        public JsonResult ObterFornecedores(string local)
        {
            var resultado = _fornecedorOsService.ObterFornecedor(local, Util.EnumTipoFornecedor.LimpezaLavagem, false);

            resultado.Insert(0, new FornecedorOsDto { IdFornecedorOs = 0, Nome = "" });

            var jsonData = Json(
                new
                {
                    Items = resultado.Select(i => new
                    {
                        i.IdFornecedorOs,
                        i.Nome
                    }),
                    Total = resultado.Count
                });
            return jsonData;
        }

        /// <summary>
        /// Retornar todos os fornecedores
        /// </summary>
        /// <returns></returns>
        public JsonResult ObterTodosFornecedores(string local)
        {
            var resultado = _fornecedorOsService.ObterFornecedor(local, Util.EnumTipoFornecedor.LimpezaLavagem, true);

            resultado.Insert(0, new FornecedorOsDto { IdFornecedorOs = 0, Nome = "" });

            var jsonData = Json(
                new
                {
                    Items = resultado.Select(i => new
                    {
                        i.IdFornecedorOs,
                        i.Nome
                    }),
                    Total = resultado.Count
                });
            return jsonData;
        }

        /// <summary>
        /// Consultar Processo Nfe
        /// </summary>
        /// <param name="chaves">chaves das nfes separadas por ;</param>
        /// <param name="chavesQueJaDeramErro">chaves das nfes que já deram erro</param>
        /// <returns>notas faltantes</returns>
        public JsonResult ConsultaOSLimpeza(DetalhesPaginacaoWeb detalhesPaginacaoWeb, string dataInicial, string dataFinal, string local, string numOs,
            string idTipo, string idStatus, string retrabalho)
        {
            var resultado = _osLimpezaVagaoService.ObterConsultaListaOs(detalhesPaginacaoWeb, dataInicial, dataFinal, local, numOs, idTipo, idStatus, retrabalho);

            var jsonData = Json(
                new
                {
                    Items = resultado.Items.Select(i => new
                    {
                        //Campos
                        Data = i.Data.ToString("dd/MM/yyyy HH:mm:ss"),
                        i.LocalServico,
                        i.IdOs,
                        NumOs = i.IdOsParcial > 0 ? string.Format("{0} ({1})", i.IdOs, i.IdOsParcial) : i.IdOs.ToString(),
                        i.Tipo,
                        i.Fornecedor,
                        i.Status,
                        UltimaAlteracao = i.UltimaAlteracao.ToString("dd/MM/yyyy HH:mm:ss"),
                        Retrabalho = i.Status.ToLower() == "aberta" || i.Status.ToLower() == "parcial" ? "" : (i.QtRetrabalho > 0 ? "Sim" : "Não"),
                        Usuario = i.Usuario
                    }),
                    Total = resultado.Total
                });
            return jsonData;
        }

        /// <summary>
        /// Retornar a lista de vagões para OS
        /// </summary>
        /// <param name="detalhesPaginacaoWeb"></param>
        /// <param name="numVagao"></param>
        /// <param name="serie"></param>
        /// <param name="local"></param>
        /// <param name="lotacao"></param>
        /// <returns></returns>
        public JsonResult ConsultaVagoes(DetalhesPaginacaoWeb detalhesPaginacaoWeb, int? numOs, string numVagao, string serie, string local, string linha, string lotacao, int sequenciaInicio, int sequenciaFim)
        {
            var resultado = _osLimpezaVagaoService.ObterConsultaDeVagoesParaOs(detalhesPaginacaoWeb, numOs, numVagao, serie, local, linha, lotacao, sequenciaInicio, sequenciaFim);

            var jsonData = Json(
              new
              {
                  Items = resultado.Items.Select(i => new
                  {
                      //Campos
                      Checked = (i.Marcado == 1 ? true : false),
                      i.NumeroVagao,
                      i.Serie,
                      i.Local,
                      i.Linha,
                      i.Sequencia,
                      i.DescricaoLotacao
                  }),
                  Total = resultado.Total
              });
            return jsonData;
        }

        /// <summary>
        /// Obter vagões da OS
        /// </summary>
        /// <param name="idOs">ID da OS</param>
        /// <returns></returns>
        public JsonResult ObterVagoesDaOs(DetalhesPaginacaoWeb detalhesPaginacaoWeb, int idOs)
        {
            var resultado = _osLimpezaVagaoService.ObterVagoesOS(detalhesPaginacaoWeb, idOs);

            var jsonData = Json(
                new
                {
                    Items = resultado.Items.Select(i => new
                    {
                        i.IdItem,
                        i.IdVagao,
                        i.NumeroVagao,
                        i.Observacao,
                        i.ConcluidoDescricao,
                        i.Concluido,
                        i.RetrabalhoDescricao,
                        i.Retrabalho
                    }),
                    Total = resultado.Total
                });
            return jsonData;
        }

        /// <summary>
        /// Buscar OS para edição
        /// </summary>
        /// <param name="idOs"></param>
        /// <returns></returns>
        public JsonResult ObterOs(int idOs)
        {
            var resultado = _osLimpezaVagaoService.ObterOS(idOs);

            var jsonData = Json(
                new
                {
                    resultado.IdOs,
                    resultado.IdOsParcial,
                    Data = resultado.Data.ToString("dd/MM/yyyy HH:mm:ss"),
                    resultado.IdFornecedor,
                    resultado.Fornecedor,
                    resultado.idLocal,
                    resultado.LocalServico,
                    resultado.IdTipo,
                    resultado.Tipo,
                    resultado.IdStatus,
                    resultado.Status,
                    DataHoraDevolucaoOS = resultado.DataHoraDevolucaoOS.ToString("dd/MM/yyyy HH:mm:ss"),
                    DataHoraEntregaOSFornecedor = resultado.DataHoraEntregaOSFornecedor.ToString("dd/MM/yyyy HH:mm:ss"),
                    resultado.NomeAprovadorRumo,
                    resultado.NomeAprovadorFornecedor,
                    resultado.MatriculaAprovadorFornecedor,
                    resultado.MatriculaAprovadorRumo
                });
            return jsonData;
        }
        #endregion
    }
}