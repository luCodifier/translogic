﻿using System.Collections.Generic;
using Translogic.Modules.Core.Domain.Model.Estrutura;
using Translogic.Modules.Core.Domain.Model.FluxosComerciais;
using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao;

namespace Translogic.Modules.Core.Controllers.Operacao
{
    using System;
    using System.Globalization;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Web.Mvc;
    using ALL.Core.Dominio;
    using Domain.Model.Estrutura.Repositories;
    using Domain.Model.FluxosComerciais.Atrasos;
    using Domain.Model.FluxosComerciais.Repositories;
    using Domain.Model.Trem.Veiculo.Vagao.Repositories;
    using Domain.Model.Via;
    using Domain.Model.Via.Repositories;
    using Domain.Services.FluxosComerciais;
    using Translogic.Core.Infrastructure.Web;

    public class CadastroAtrasosController : BaseSecureModuleController
    {
        private readonly AtrasosService _atrasosService;
        private readonly IEmpresaClienteRepository _clienteRepository;
        private readonly IMercadoriaRepository _mercadoriaRepository;
        private readonly IAreaOperacionalRepository _aoRepository;
        private readonly IVagaoRepository _vagaoRepository;
        private readonly IEmpresaRepository _empresaRepository;

        private static readonly Regex _regexCNPJ = new Regex(@"(?<FS1>\d{2})(?<FS2>\d{3})(?<FS3>\d{3})(?<FS4>\d{4})(?<FS5>\d{2})", RegexOptions.Singleline | RegexOptions.IgnoreCase | RegexOptions.CultureInvariant | RegexOptions.Compiled);

        public CadastroAtrasosController(
            AtrasosService atrasosService,
            IEmpresaClienteRepository clienteRepository,
            IMercadoriaRepository mercadoriaRepository,
            IAreaOperacionalRepository aoRepository,
            IVagaoRepository vagaoRepository, IEmpresaRepository empresaRepository)
        {
            _atrasosService = atrasosService;
            _clienteRepository = clienteRepository;
            _mercadoriaRepository = mercadoriaRepository;
            _aoRepository = aoRepository;
            _vagaoRepository = vagaoRepository;
            _empresaRepository = empresaRepository;
        }

        /// <summary>
        /// View de inicialização
        /// </summary>
        /// <returns>Return view</returns>
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ObterAtrasos(DetalhesPaginacaoWeb pagination, DetalhesFiltro<object>[] filter)
        {
            DateTime? dataIni = null;
            DateTime? dataFim = null;
            TipoAtraso tipo = null;
            ResponsavelAtraso responsavel = null;

            foreach (var detalheFiltro in filter)
            {
                if (detalheFiltro.Campo.Equals("dataInicial") &&
                    !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                {
                    string texto = detalheFiltro.Valor[0].ToString();

                    dataIni = DateTime.ParseExact(texto, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                }

                if (detalheFiltro.Campo.Equals("dataFinal") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                {
                    string texto = detalheFiltro.Valor[0].ToString();

                    dataFim = DateTime.ParseExact(texto, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);

                    dataFim = dataFim.Value.AddDays(1).AddSeconds(-1);
                }

                if (detalheFiltro.Campo.Equals("tipo") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                {
                    tipo = new TipoAtraso { Id = detalheFiltro.Valor[0].ToString() };
                }

                if (detalheFiltro.Campo.Equals("responsavel") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                {
                    responsavel = new ResponsavelAtraso { Id = detalheFiltro.Valor[0].ToString() };
                }
            }

            var listaDados = _atrasosService.ObterAtrasos(pagination, dataIni.Value, dataFim.Value, tipo, responsavel);
            return Json(new
            {
                Total = listaDados.Total,
                Items = listaDados.Items.Select(a =>
                new
                {
                    Id = a.Id,
                    QtdeVagoes = a.QtdeVagoes,
                    Cliente = a.Cliente.DescricaoResumida,
                    TU = a.TU,
                    Data = a.Data.ToString("dd/MM/yyyy"),
                    TempoEmHrs = TimeSpan.FromHours((double)a.HorasAtraso).ToString("hh\\:mm"),
                    Responsavel = a.Responsavel.Nome,
                    Obs = a.Observacao,
                    Tipo = a.Tipo.Nome,
                    Motivo = a.Motivo.Nome,
                    Estacao = a.Estacao.Codigo,
                    Mercadoria = a.Mercadoria != null?string.Format("{1}-{0}", a.Mercadoria.DescricaoResumida, a.Mercadoria.Codigo):"",
                    Vagoes = string.IsNullOrEmpty(a.VagoesDigitados)?"":a.VagoesDigitados
                }),
                success = true
            });
        }
        
        public ActionResult ObterTipos()
        {
            return Json(_atrasosService.ObterTipoAtrasos().Select(t => new { Tipo = t.Id, TipoLabel = t.Nome }));
        }

        public ActionResult ObterMotivos()
        {
            return Json(_atrasosService.ObterMotivoAtrasos().Select(t => new { Motivo = t.Id, MotivoLabel = t.Nome }));
        }

        public ActionResult ObterResponsaveis()
        {
            return Json(_atrasosService.ObterResponsavelAtrasos().Select(t => new { Responsavel = t.Id, ResponsavelLabel = t.Nome }));
        }

        /// <summary>
        /// Ação de mostrar o Formulario de edição ao ser chamado o controller
        /// </summary>
        /// <param name="id"> Id do Atraso. </param>
        /// <returns> View de inserção/alteração</returns>
        public ActionResult Edit(int? id)
        {
            //if (id.HasValue)
            //{
            //    AvisoAtraso avisoAtraso = _atrasosService.ObterAvisoAtrasoPorId(id.Value);
            //    return View(avisoAtraso);
            //}

            //return View(new AvisoAtraso());
            return View();
        }

        public JsonResult Salvar(
            int? id,
            string tipo,
            string estacao,
            decimal? tu,
            string vagoes,
            int? qtdeVagoes,
            DateTime? data,
            TimeSpan? horasAtraso,
            string cliente,
            string mercadoria,
            string motivo,
            string responsavel,
            string observacao
            )
        {
            var tipos = _atrasosService.ObterTipoAtrasos();
            var respons = _atrasosService.ObterResponsavelAtrasos();
            var motivos = _atrasosService.ObterMotivoAtrasos();
            List<EmpresaCliente> clientes;
            List<Mercadoria> mercadorias = new List<Mercadoria>();

            if (!tipos.Any(R => R.Nome == tipo || R.Id == tipo))
            {
                return Json(new
                {
                    sucesso = false,
                    message = "Informe o tipo de atraso."
                });
            }
            if (!motivos.Any(R => R.Nome == motivo || R.Id == motivo))
            {
                return Json(new
                {
                    sucesso = false,
                    message = "Informe o motivo do atraso."
                });
            }
            if (!data.HasValue || data.Value < DateTime.Today)
            {
                return Json(new
                {
                    sucesso = false,
                    message = "A data informada é inválida."
                });
            }
            if (!horasAtraso.HasValue || horasAtraso.Value <= TimeSpan.Zero)
            {
                return Json(new
                {
                    sucesso = false,
                    message = "O tempo de atraso informado é inválido."
                });
            }
            if (tipo == "Carga" && (!tu.HasValue || tu.Value <= 0m))
            {
                return Json(new
                {
                    sucesso = false,
                    message = "Informe a TU."
                });
            }
            if (tipo == "Descarga" && String.IsNullOrWhiteSpace(vagoes))
            {
                return Json(new
                {
                    sucesso = false,
                    message = "Informe os vagões."
                });
            }
            
            if(tipo == "Descarga")
            {
                if (!string.IsNullOrEmpty(mercadoria))
                {
                    mercadorias = _mercadoriaRepository.ObterLista(mercadoria.Split('-')[1]);
                    if (!mercadorias.Any(m => string.Format("{1}-{0}", m.DescricaoResumida, m.Codigo) == mercadoria))
                    {
                        return Json(new
                        {
                            sucesso = false,
                            message = "Informe a mercadoria."
                        });
                    }
                }
                else
                {
                    return Json(new
                    {
                        sucesso = false,
                        message = "Informe a mercadoria."
                    });
                }

            }

            if (!qtdeVagoes.HasValue)
            {
                return Json(new
                {
                    sucesso = false,
                    message = "Informe a quantidade de vagões."
                });
            }
            if (!string.IsNullOrEmpty(cliente))
            {
                clientes = _clienteRepository.ObterListaDescResumida(cliente);
                if (!clientes.Any(c => c.DescricaoResumida == cliente))
                {
                    return Json(new
                                    {
                                        sucesso = false,
                                        message = "Informe o cliente."
                                    });
                }
            }
            else
            {
                return Json(new
                {
                    sucesso = false,
                    message = "Informe o cliente."
                });
            }


            if (!respons.Any(R => R.Nome == responsavel || R.Id == responsavel))
            {
                return Json(new
                {
                    sucesso = false,
                    message = "Informe o responsável."
                });
            }
            if (String.IsNullOrWhiteSpace(observacao))
            {
                return Json(new
                {
                    sucesso = false,
                    message = "Informe uma observação."
                });
            }
            var ao = _aoRepository.ObterPorCodigo(estacao.ToUpper()) as EstacaoMae;
            if (ao == null)
            {
                return Json(new
                {
                    sucesso = false,
                    message = "Informe uma estação mãe válida."
                });
            }

            AvisoAtraso objAA = new AvisoAtraso();
            if (id.HasValue)
            {
                objAA = _atrasosService.ObterAvisoAtrasoPorId(id.Value);
            }

            objAA.Estacao = ao;
            objAA.Data = data.Value;
            objAA.HorasAtraso = (decimal)horasAtraso.Value.TotalHours;
            objAA.Observacao = observacao;
            objAA.QtdeVagoes = qtdeVagoes.Value;
            objAA.TU = tu;
            objAA.VagoesDigitados = vagoes;
            objAA.Usuario = UsuarioAtual;
            objAA.VersionDate = DateTime.Now;
            
            if (!string.IsNullOrEmpty(mercadoria))
                objAA.Mercadoria = mercadorias.First(m => string.Format("{1}-{0}", m.DescricaoResumida, m.Codigo) == mercadoria);

            objAA.Tipo = tipos.First(t => t.Nome == tipo || t.Id == tipo);
            objAA.Responsavel = respons.First(r => r.Nome == responsavel || r.Id == responsavel);
            objAA.Motivo = motivos.First(r => r.Nome == motivo || r.Id == motivo);

            try
            {
                objAA.Cliente = clientes.First(c => c.DescricaoResumida == cliente);
                objAA.DescResumida = cliente;
                
                if (!String.IsNullOrWhiteSpace(vagoes))
                {
                    var vagoesInf = vagoes.Split(',');
                    foreach (var vg in vagoesInf)
                    {
                        var vgO = _vagaoRepository.ObterPorCodigo(vg);
                        if (vgO == null)
                        {
                            return Json(new
                            {
                                sucesso = false,
                                message = "Vagão '" + vg + "' informado, não foi encontrado."
                            });
                        }

                        if (!objAA.Vagoes.Any(v => v.Codigo == vg))
                            objAA.Vagoes.Add(vgO);
                    }
                    /* caso tenha excluido um vagão da listagem */
                    Vagao[] liVag = objAA.Vagoes.ToArray();
                    
                    if (liVag.Count() > vagoesInf.Count())
                    {
                        foreach (var vg in liVag)
                        {
                            if (!vagoesInf.Any(v => v == vg.Codigo))
                                objAA.Vagoes.Remove(vg);
                        }
                        //objAA.Vagoes = liVag;
                    }
                }

                var obj = _atrasosService.Salvar(objAA);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    sucesso = false,
                    message = "Ocorreu um erro inesperado ao salvar. Contate o administrador do sistema."
                });
            }

            return Json(new
            {
                sucesso = true,
                message = "Registro salvo com sucesso."
            });
        }

        /// <summary>
        /// Auto Complete de clientes para o Cadastro
        /// </summary>
        /// <param name="query"> Filtro para os clientes</param>
        /// <returns>Lista dos clientes</returns>
        public JsonResult ListClientes(string query)
        {
            query = query ?? "";

            if (!string.IsNullOrEmpty(query))
            {
                var itens = _clienteRepository.ObterListaDescResumida(query);

                var teste = itens.Select(a => new
                {
                    a.Id,
                    a.DescricaoResumida
                }).ToList();

                return Json(new
                {
                    Total = itens.Count,
                    Items = teste.Select(a => new
                    {
                        DescResumida = a.DescricaoResumida
                    })
                .OrderBy(a => a.DescResumida)
                .Distinct()
                });
            }

            //return Json(new {
            //               Total = itens.Count,
            //               Items = itens.Select(a => new
            //                   {
            //                       DescResumida = a.DescricaoResumida
            //                   })
            //               .OrderBy(a => a.DescResumida)
            //               .GroupBy(a => a.DescResumida)
            //           });
            return null;
        }

        /// <summary>
        /// Auto Complete de mercadorias para o Cadastro
        /// </summary>
        /// <param name="query"> Filtro para os mercadorias</param>
        /// <returns>Lista dos mercadorias</returns>
        public JsonResult ListMercadorias(string query)
        {
            query = query ?? "";
            var itens = _mercadoriaRepository.ObterLista(query);
            return Json(
                       new
                       {
                           Total = itens.Count,
                           Items = itens.Select(a => new
                           {
                               a.Id,
                               a.Codigo,
                               Nome = a.DescricaoResumida,
                               CodigoNome = string.Format("{1}-{0}", a.DescricaoResumida, a.Codigo)
                           }).OrderBy(a => a.CodigoNome)
                       });
        }

        /// <summary>
        /// Métodos para formatação de CNPJ
        /// </summary>
        /// <param name="cnpj">CNPJ para formatação</param>
        /// <returns>Retorna o CNPJ formatado</returns>
        public static string FormatCNPJ(string cnpj)
        {
            return _regexCNPJ.Replace(cnpj, "${FS1}.${FS2}.${FS3}/${FS4}-${FS5}");
        }
    }
}