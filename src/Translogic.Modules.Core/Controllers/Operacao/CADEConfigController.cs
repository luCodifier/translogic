﻿namespace Translogic.Modules.Core.Controllers.Operacao
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Security.Cryptography;
    using System.Web.Mvc;
    using Domain.Model.Diversos.ConfigCADE;
    using Domain.Model.Estrutura.Repositories;
    using Domain.Model.Trem;
    using Domain.Services.FluxosComerciais;
    using Translogic.Core.Infrastructure.Web.Filters;
    using Translogic.Modules.Core.Domain.Model.Codificador;
    using Translogic.Modules.Core.Domain.Model.Codificador.Repositories;

    /// <summary>
    /// Telas de configuração de dados para o CADE
    /// </summary>
    public class CADEConfigController : BaseSecureModuleController
    {
        private readonly IEmpresaClienteRepository _clienteRepository;
        private readonly CADEConfigService _service;
        private readonly IConfiguracaoTranslogicRepository _configuracaoTranslogicRepository;

        /// <summary>
        /// Construor padrão com injeções
        /// </summary>
        public CADEConfigController(
            IEmpresaClienteRepository clienteRepository, CADEConfigService service, IConfiguracaoTranslogicRepository configuracaoTranslogicRepository)
        {
            _clienteRepository = clienteRepository;
            _service = service;
            _configuracaoTranslogicRepository = configuracaoTranslogicRepository;
        }

        /// <summary>
        /// View de inicialização
        /// </summary>
        /// <returns>Return view</returns>
        public ActionResult AjusteTUProgramada()
        {
            return View();
        }

        /// <summary>
        /// View de inicialização
        /// </summary>
        /// <returns>Return view</returns>
        public ActionResult TremTipo()
        {
            return View();
        }

        public ActionResult SalvarAjusteTU(
            DateTime mesReferencia,
            string fluxo,
            string cliente,
            string segmento,
            FluxoAjusteTU[] ajustes)
        {
            if (ajustes == null || !ajustes.Any())
            {
                return Json(new
                {
                    sucesso = false,
                    message = "Nenhum registro para salvar."
                });
            }

            ConfiguracaoTranslogic configuracaoTranslogic = _configuracaoTranslogicRepository.ObterPorId("PERMITE_AJUSTE_MES_ANTERIOR");

            if (configuracaoTranslogic != null && configuracaoTranslogic.Valor.Equals("N"))
            {
                if (mesReferencia <= DateTime.Today.AddDays(1 - DateTime.Today.Day))
                {
                    return Json(new
                                    {
                                        sucesso = false,
                                        message = "Não é possível salvar dados de meses passados/em andamento."
                                    });
                }
            }

            try
            {
                List<string> liFluxo = ajustes.Select(x => x.Fluxo).ToList();

                var dados = _service.ObterAjustesTU(mesReferencia, liFluxo, cliente, segmento);
                var groups = dados.GroupBy(d => new
                {
                    MesReferencia = d.DataReferencia.AddDays(1 - d.DataReferencia.Day),
                    d.Fluxo,
                    d.Origem,
                    d.Destino,
                    d.ClienteRementente,
                    d.Mercadoria
                })
                .ToList();
                List<AjusteTUMensal> toSave = new List<AjusteTUMensal>();
                foreach (var g in groups)
                {
                    var vAj = ajustes.FirstOrDefault(a => a.Fluxo == g.Key.Fluxo);
                    if (vAj == null)
                    {
                        continue;
                    }

                    foreach (var reg in g)
                    {
                        var razao = Convert.ToDecimal(vAj.RazaoAjuste, new CultureInfo("en-us"));
                        reg.TUMesAjust = reg.TUMes * razao;
                        reg.CarregamentosAjust = reg.Carregamentos * razao;
                        toSave.Add(reg);
                    }
                }

                _service.SalvarAjustes(toSave);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    sucesso = false,
                    message = "Ocorreu um erro inesperado ao salvar. Contate o administrador do sistema."
                });
            }

            return Json(new
            {
                sucesso = true,
                message = "Registro salvo com sucesso."
            });
        }

        public ActionResult SalvarTrensTipo(
            DateTime mesReferencia,
            string origem,
            string destino,
            string cliente,
            string segmento,
            TrensTipoCadastro[] trensTipo)
        {
            if (trensTipo == null || !trensTipo.Any())
            {
                return Json(new
                {
                    sucesso = false,
                    message = "Nenhum registro para salvar."
                });
            }

            try
            {
                var dados = _service.ObterTrensTipo(mesReferencia, origem, destino, cliente, segmento);

                List<TremTipo> toSave = new List<TremTipo>();
                foreach (var tt in trensTipo)
                {
                    var vAj = dados.FirstOrDefault(a => a.Id.ToString() == tt.Id);
                    if (vAj == null)
                    {
                        vAj = new TremTipo
                        {
                            Ano = mesReferencia.Year,
                            Mes = mesReferencia.Month,
                            Cliente = tt.Cliente,
                            Segmento = segmento,
                            Destino = tt.Destino,
                            Origem = tt.Origem,
                            VersionDate = DateTime.Now
                        };
                    }
                    vAj.QtdeVagoes = Convert.ToDecimal(tt.QtdeVagoes, new CultureInfo("en-us"));
                    toSave.Add(vAj);
                }

                _service.SalvarTrensTipo(toSave);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    sucesso = false,
                    message = "Ocorreu um erro inesperado ao salvar. Contate o administrador do sistema."
                });
            }

            return Json(new
            {
                sucesso = true,
                message = "Registro salvo com sucesso."
            });
        }
        public JsonResult ObterAjustesTU(
            DateTime mesReferencia, 
            string fluxo, 
            string cliente, 
            string segmento)
        {
            List<string> liFluxo = fluxo.Split(',').ToList();

            var dados = _service.ObterAjustesTU(mesReferencia, liFluxo, cliente, segmento);
            var groups = dados.GroupBy(d => new
            {
                MesReferencia = d.DataReferencia.AddDays(1 - d.DataReferencia.Day),
                d.Fluxo,
                d.Origem,
                d.Destino,
                d.ClienteRementente,
                d.Mercadoria
            });

            return Json(new
            {
                Total = groups.Count(),
                Items = groups.Select(g => new
                {
                    g.Key.MesReferencia,
                    g.Key.Fluxo,
                    g.Key.Origem,
                    g.Key.Destino,
                    g.Key.ClienteRementente,
                    g.Key.Mercadoria,
                    QtdeVagoes = Math.Round(g.Sum(a => a.Carregamentos), 2),
                    TUMes = Math.Round(g.Sum(a => a.TUMes), 2),
                    QtdeVagoesNova = Math.Round(g.Sum(a => a.CarregamentosAjust ?? a.Carregamentos), 2),
                    TUMesNova = Math.Round(g.Sum(a => a.TUMesAjust ?? a.TUMes), 2),
                })
            });
        }

        public JsonResult ObterTrensTipo(
            DateTime mesReferencia,
            string origem,
            string destino,
            string cliente,
            string segmento)
        {
            var dados = _service.ObterTrensTipo(mesReferencia, origem, destino, cliente, segmento);

            return Json(new
            {
                Total = dados.Count(),
                Items = dados
            });
        }

        /// <summary>
        /// Auto Complete de clientes para o Cadastro
        /// </summary>
        /// <param name="query"> Filtro para os clientes</param>
        /// <returns>Lista dos clientes</returns>
        public JsonResult ListClientes(string query)
        {
            query = query ?? "";

            if (!string.IsNullOrEmpty(query))
            {
                var itens = _clienteRepository.ObterListaDescResumida(query);

                var teste = itens.Select(a => new
                {
                    a.Id,
                    a.DescricaoResumida
                }).ToList();

                return Json(new
                {
                    Total = itens.Count,
                    Items = teste.Select(a => new
                    {
                        DescResumida = a.DescricaoResumida
                    })
                .OrderBy(a => a.DescResumida)
                .Distinct()
                });
            }
            return null;
        }


        /// <summary>
        /// Auto Complete de clientes para o Cadastro - Novo método para tornar o auto complete mais rápido
        /// </summary>
        /// <param name="query"> Filtro para os clientes</param>
        /// <returns>Lista dos clientes</returns>
        public JsonResult ListClientesNomes(string query)
        {
            query = query ?? "";

            if (!string.IsNullOrEmpty(query))
            {
                var itens = _clienteRepository.ObterListaNomesDescResumida(query);

                var teste = itens.Select(a => new
                {
                    a.Id,
                    a.DescricaoResumida
                }).ToList();

                return Json(new
                {
                    Total = itens.Count,
                    Items = teste.Select(a => new
                    {
                        Id = a.Id,
                        DescResumida = a.DescricaoResumida
                    })
                .OrderBy(a => a.DescResumida)
                .Distinct()
                });
            }
            return null;
        }

        public class FluxoAjusteTU
        {
            public string Fluxo { get; set; }
            public string RazaoAjuste { get; set; }
        }

        public class TrensTipoCadastro
        {
            public string Id { get; set; }
            public string Origem { get; set; }
            public string Destino { get; set; }
            public string Cliente { get; set; }
            public string QtdeVagoes { get; set; }
        }
    }
}