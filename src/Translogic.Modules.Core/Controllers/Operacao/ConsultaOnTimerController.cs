﻿namespace Translogic.Modules.Core.Controllers.Operacao
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using System.Web.Script.Serialization;
    using Interfaces.Trem;
    using Interfaces.Trem.OnTime;
    using OfficeOpenXml;
    using Translogic.Core.Infrastructure.Web.Helpers;
    using Translogic.Modules.Core.Domain.Services.Trem;
    using Translogic.Modules.Core.Infrastructure;
    using Util;

    /// <summary>
    /// Classe de consulta do ontime do trem
    /// </summary>
    public class ConsultaOnTimerController : BaseSecureModuleController
    {
        #region Atributos

        private readonly ICentroTremService _centroTremService;
        private readonly OnTimeService _onTimeService;

        #endregion

        #region Construtor

        /// <summary>
        /// Construtor injetado
        /// </summary>
        /// <param name="centroTremService">Serviço de CentroTrem</param>
        /// <param name="onTimeService">Serviço de OnTime</param>
        public ConsultaOnTimerController(ICentroTremService centroTremService, OnTimeService onTimeService)
        {
            _centroTremService = centroTremService;
            _onTimeService = onTimeService;
        }

        #endregion

        #region Index

        /// <summary>
        /// View de inicialização
        /// </summary>
        /// <returns>Return view</returns>
        [Autorizar(Transacao = "CONSULTAONTIME")]
        public ActionResult Index()
        {
            return View();
        }

        #endregion

        #region ObterUnidadeProducao

        /// <summary>
        /// Retorna as Unidades de Producao
        /// </summary>
        /// <returns>Unidades de Producao</returns>
        [Autorizar(Transacao = "CONSULTAONTIME", Acao = "Pesquisar")]
        public ActionResult ObterUnidadeProducao()
        {
            var result = _onTimeService.ObterUnidadesProducao();

            return Json(new
                {
                    Items = result.Select(u => new
                    {
                        u.Id,
                        u.DescricaoResumida
                    })
                });
        }

        #endregion

        #region Imprimir

        /// <summary>
        /// Obtem os Ontime
        /// </summary>
        /// <param name="codigoUpOrigem">Up de Origem</param>
        /// <param name="codigoUpDestino">Up de Destino</param>
        /// <param name="estOrigem">Estação de Origem</param>
        /// <param name="estDestino">Estação de Destino</param>
        /// <param name="prefixo">Prefixo do trem</param>
        /// <param name="partidaPlanejadaInicial">Partida planejada inicial</param>
        /// <param name="partidaPlanejadaFinal">Partida planejada Final</param>
        /// <param name="chegadaPlanejadaInicial">Chegada planejada inicial</param>
        /// <param name="chegadaPlanejadaFinal">Chegada planejada Final</param>
        /// <param name="prefixosIncluir">Prefixos a incluir</param>
        /// <param name="prefixosExcluir">Prefixos a excluir</param>
        /// <returns>Retorna os ontime</returns>
        [Autorizar(Transacao = "CONSULTAONTIME", Acao = "Pesquisar")]
        public ActionResult Imprimir(string codigoUpOrigem, string codigoUpDestino, string estOrigem, string estDestino, string prefixo, DateTime? partidaPlanejadaInicial, DateTime? partidaPlanejadaFinal, DateTime? chegadaPlanejadaInicial, DateTime? chegadaPlanejadaFinal, string prefixosIncluir, string prefixosExcluir)
        {
            var retorno = _centroTremService.ObterDadosOnTime(
                new RequisicaoDadosOnTime
                    {
                        UnidadeProducaoOrigem = codigoUpOrigem,
                        UnidadeProducaoDestino = codigoUpDestino,
                        AreaOperacionalOrigem = estOrigem,
                        AreaOperacionalDestino = estDestino,
                        Prefixo = prefixo,
                        PrefixosIncluir = prefixosIncluir,
                        PrefixosExcluir = prefixosExcluir,
                        DataInicialPartida = partidaPlanejadaInicial,
                        DataFinalPartida = partidaPlanejadaFinal,
                        DataInicialChegada = chegadaPlanejadaInicial,
                        DataFinalChegada = chegadaPlanejadaFinal
                    });

            var items = ObterPartidas(retorno.PartidasTrens);

            ViewData["items"] = items;
            ViewData["ups"] = retorno.Performances;
            ViewData["indicadores"] = retorno.Indicadores;

            var viewString = View("Impressao").Capture(ControllerContext);

            return this.PdfResult("Consulta On Time", "ConsultaOnTime.pdf", viewString);
        }

        #endregion

        #region ObterOnTime

        /// <summary>
        /// Obtem os Ontime
        /// </summary>
        /// <param name="codigoUpOrigem">Up de Origem</param>
        /// <param name="codigoUpDestino">Up de Destino</param>
        /// <param name="estOrigem">Estação de Origem</param>
        /// <param name="estDestino">Estação de Destino</param>
        /// <param name="prefixo">Prefixo do trem</param>
        /// <param name="partidaPlanejadaInicial">Partida planejada inicial</param>
        /// <param name="partidaPlanejadaFinal">Partida planejada Final</param>
        /// <param name="chegadaPlanejadaInicial">Chegada planejada inicial</param>
        /// <param name="chegadaPlanejadaFinal">Chegada planejada Final</param>
        /// <param name="prefixosIncluir">Prefixos a incluir</param>
        /// <param name="prefixosExcluir">Prefixos a excluir</param>
        /// <param name="gerarExcel">Gerar Excel</param>
        /// <returns>Retorna os ontime</returns>
        [Autorizar(Transacao = "CONSULTAONTIME", Acao = "Pesquisar")]
        public ActionResult ObterOnTime(string codigoUpOrigem, string codigoUpDestino, string estOrigem, string estDestino, string prefixo, DateTime? partidaPlanejadaInicial, DateTime? partidaPlanejadaFinal, DateTime? chegadaPlanejadaInicial, DateTime? chegadaPlanejadaFinal, string prefixosIncluir, string prefixosExcluir, bool? gerarExcel)
        {
            var retorno = _centroTremService.ObterDadosOnTime(
                new RequisicaoDadosOnTime
                {
                    UnidadeProducaoOrigem = codigoUpOrigem,
                    UnidadeProducaoDestino = codigoUpDestino,
                    AreaOperacionalOrigem = estOrigem,
                    AreaOperacionalDestino = estDestino,
                    Prefixo = prefixo,
                    PrefixosIncluir = prefixosIncluir,
                    PrefixosExcluir = prefixosExcluir,
                    DataInicialPartida = partidaPlanejadaInicial,
                    DataFinalPartida = partidaPlanejadaFinal,
                    DataInicialChegada = chegadaPlanejadaInicial,
                    DataFinalChegada = chegadaPlanejadaFinal
                });

            var items = ObterPartidas(retorno.PartidasTrens);

            if (gerarExcel == true)
            {
                return File(ObterExcel(retorno), "application/vnd.ms-excel", "ConsultaOnTime.xlsx");
            }

            var serializer = new JavaScriptSerializer { MaxJsonLength = Int32.MaxValue };

            var resultData = new
            {
                Sucesso = true,
                Mensagem = retorno,
                Performance = retorno.Performances,
                Indicadores = retorno.Indicadores,
                Items = items
            };

            var result = new ContentResult
            {
                Content = serializer.Serialize(resultData),
                ContentType = "application/json"
            };

            return result;
        }

        #endregion

        #region Alterar OnTime

        /// <summary>
        /// Cancela virtualmente a OS para penalização
        /// </summary>
        /// <param name="idOS">Id da OS a ser cancelada</param>
        /// <returns>Resultado da operação</returns>
        [Autorizar(Transacao = "CONSULTAONTIME", Acao = "Cancelar")]
        public ActionResult CancelarOSOnTime(int idOS)
        {
            try
            {
                _onTimeService.EfetuarCancelamentoVirtualOSOnTime(UsuarioAtual, idOS);
                return Json(new
                {
                    sucesso = true,
                    message = "Registro de OnTime para OS alterado com sucesso!"
                });
            }
            catch (Exception ex)
            {
                return Json(new
                                {
                                    sucesso = false,
                                    message = ex.Message
                                });
            }
        }

        /// <summary>
        /// Considerar data do TL
        /// </summary>
        /// <param name="idOS">Id da OS a ser alterada</param>
        /// <returns>Resultado da operação</returns>
        [Autorizar(Transacao = "CONSULTAONTIME", Acao = "Salvar")]
        public ActionResult ConsiderarDataTL(int idOS)
        {
            try
            {
                _onTimeService.EfetuarAltPartidaTLOSOnTime(UsuarioAtual, idOS);
                return Json(new
                {
                    sucesso = true,
                    message = "Registro de OnTime para OS alterado com sucesso!"
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    sucesso = false,
                    message = ex.Message
                });
            }
        }

        #endregion

        #region Privados

        private IList ObterPartidas(IList<PartidaTremOnTime> retorno)
        {
            var items = retorno.Select(u => new PartidaOT
            {
                Id = u.Id.ToString(),
                NroOs = u.Numero.ToString(),
                Trem = u.Prefixo,
                UnidadeProducaoOrigem = u.UnidadeProducaoOrigem,
                UnidadeProducaoDestino = u.UnidadeProducaoDestino,
                EstacaoOrigem = u.AreaOperacionalOrigem,
                EstacaoDestino = u.AreaOperacionalDestino,
                PartidaPrevista = u.DataPartidaOficial.HasValue
                       ? u.DataPartidaOficial.Value.ToString("dd/MM/yyyy HH:mm:ss")
                       : string.Empty,
                PartidaRealizada = u.DataPartidaReal.HasValue
                        ? u.DataPartidaReal.Value.ToString("dd/MM/yyyy HH:mm:ss")
                        : string.Empty,
                AtrasoPartida = u.AtrasoPartida.HasValue ? u.AtrasoPartida.Value.ToTimeString() : string.Empty,
                ChegadaPrevista = u.DataChegadaOficial.HasValue
                        ? u.DataChegadaOficial.Value.ToString("dd/MM/yyyy HH:mm:ss")
                        : string.Empty,
                ChegadaRealizada = u.DataChegadaReal.HasValue
                        ? u.DataChegadaReal.Value.ToString("dd/MM/yyyy HH:mm:ss")
                        : string.Empty,
                AtrasoChegada = u.AtrasoChegada.HasValue ? u.AtrasoChegada.Value.ToTimeString() : string.Empty,
                Situacao = u.Situacao.ToString(),
                DtProg = u.DataPartidaAnterior.HasValue
                    ? u.DataPartidaAnterior.Value.ToString("dd/MM/yyyy HH:mm:ss")
                    : string.Empty,
                DtCadastro = u.DataCadastro.ToString("dd/MM/yyyy HH:mm:ss"),
                DtPartidaCadastro = u.DataPartidaCadastro.ToString("dd/MM/yyyy HH:mm:ss"),
                MatriculaCadastro = u.MatriculaCadastro,
                DtPartidaUltimaAlteracao = (u.DataCadastro == u.DataUltimaAlteracao && u.DataPartidaCadastro == u.DataPartidaUltimaAlteracao && u.MatriculaCadastro == u.MatriculaUltimaAlteracao)
                                               ? String.Empty
                                               : u.DataPartidaUltimaAlteracao.HasValue ? u.DataPartidaUltimaAlteracao.Value.ToString("dd/MM/yyyy HH:mm:ss") : string.Empty,
                MatriculaUltimaAlteracao = (u.DataCadastro == u.DataUltimaAlteracao && u.DataPartidaCadastro == u.DataPartidaUltimaAlteracao && u.MatriculaCadastro == u.MatriculaUltimaAlteracao)
                                               ? String.Empty
                                               : u.MatriculaUltimaAlteracao,
                DtUltimaAlteracao = (u.DataCadastro == u.DataUltimaAlteracao && u.DataPartidaCadastro == u.DataPartidaUltimaAlteracao && u.MatriculaCadastro == u.MatriculaUltimaAlteracao)
                                        ? String.Empty
                                        : u.DataUltimaAlteracao.HasValue ? u.DataUltimaAlteracao.Value.ToString("dd/MM/yyyy HH:mm:ss") : string.Empty,
                DtPartidaTL = u.DataPartidaTranslogic.HasValue ? u.DataPartidaTranslogic.Value.ToString("dd/MM/yyyy HH:mm:ss") : String.Empty,
                AlteradoTranslogic = u.AlteradoTranslogic,
                PermitirAlteracaoOnTime = u.PermitirAlteracaoOnTime
            }).ToList();
            return items;
        }

        /// <summary>
        /// Obtem o Excel
        /// </summary>
        /// <param name="dadosOnTime">Dados de OnTime</param>
        /// <returns>array de bytes do excel</returns>
        private byte[] ObterExcel(DadosOnTime dadosOnTime)
        {
            using (var pkg = new ExcelPackage())
            {
                var ws = pkg.Workbook.Worksheets.Add("OnTime");
                ws.Cells["A1"].LoadFromCollection(dadosOnTime.PartidasTrens.Select(e => new
                {
                    Trem = e.Prefixo,
                    NroOs = e.Numero.ToString(),
                    e.AreaOperacionalOrigem,
                    e.AreaOperacionalDestino,
                    DataPartidaOficial = e.DataPartidaOficial.HasValue ?
                                            e.DataPartidaOficial.Value.ToString("dd/MM/yyyy HH:mm:ss") : String.Empty,
                    DataPartidaReal = e.DataPartidaReal.HasValue ?
                                            e.DataPartidaReal.Value.ToString("dd/MM/yyyy HH:mm:ss") : String.Empty,
                    e.Situacao,
                    DataCadastro = e.DataCadastro.ToString("dd/MM/yyyy HH:mm:ss"),
                    e.MatriculaCadastro,
                    DataPartidaCadastro = e.DataPartidaCadastro.ToString("dd/MM/yyyy HH:mm:ss"),
                    DataUltimaAlteracao =
                        e.DataCadastro == e.DataUltimaAlteracao || !e.DataUltimaAlteracao.HasValue ?
                        null : e.DataUltimaAlteracao.Value.ToString("dd/MM/yyyy HH:mm:ss"),
                    MatriculaUltimaAlteracao =
                        e.DataCadastro == e.DataUltimaAlteracao ?
                        null : e.MatriculaUltimaAlteracao,
                    DataPartidaUltimaAlteracao =
                        e.DataCadastro == e.DataUltimaAlteracao || !e.DataPartidaUltimaAlteracao.HasValue ?
                        null : e.DataPartidaUltimaAlteracao.Value.ToString("dd/MM/yyyy HH:mm:ss"),
                    AtrasoPartida = e.AtrasoPartida.HasValue ? e.AtrasoPartida.Value.ToTimeString() : String.Empty
                }));
                // ws.Cells["N:O"].Clear();

                ws.InsertRow(1, 1);
                ws.Cells["A1:X1"].Style.Font.Bold = true;

                ws.Cells["A1"].Value = "Trem";
                ws.Cells["B1"].Value = "Os";
                ws.Cells["C1"].Value = "Origem";
                ws.Cells["D1"].Value = "Destino";
                ws.Cells["E1"].Value = "Partida Prev.";
                ws.Cells["F1"].Value = "Partida Real.";
                ws.Cells["G1"].Value = "Sit. Trem";
                ws.Cells["H1"].Value = "Data Cadastro";
                ws.Cells["I1"].Value = "Matrícula Cadastro";
                ws.Cells["J1"].Value = "Partida Prev. Cadastro";
                ws.Cells["K1"].Value = "Data Ult. Alteração";
                ws.Cells["L1"].Value = "Matrícula Ult. Alteração";
                ws.Cells["M1"].Value = "Partida Prev. Ult. Alteração";
                ws.Cells["N1"].Value = "Atraso";

                // -> Sumário
                var linhaPartidaSumario = dadosOnTime.PartidasTrens.Count + 2;

                ws.Cells[string.Format("A{0}", linhaPartidaSumario + 1)].Value = "Trens encontrados dentro do Filtro Especificado";
                ws.Cells[string.Format("A{0}", linhaPartidaSumario + 2)].Value = "Trens Encerrrados";
                ws.Cells[string.Format("A{0}", linhaPartidaSumario + 3)].Value = "Trens parados/circulando";
                ws.Cells[string.Format("A{0}", linhaPartidaSumario + 4)].Value = "Trens que tiveram Partida";
                ws.Cells[string.Format("A{0}", linhaPartidaSumario + 5)].Value = "...com OS Regular";
                ws.Cells[string.Format("A{0}", linhaPartidaSumario + 6)].Value = "...com OS Irregular";
                ws.Cells[string.Format("A{0}", linhaPartidaSumario + 7)].Value = "...com Atraso na Partida";
                ws.Cells[string.Format("A{0}", linhaPartidaSumario + 8)].Value = "...com OS Regular";
                ws.Cells[string.Format("A{0}", linhaPartidaSumario + 9)].Value = "...com OS Irregular";
                ws.Cells[string.Format("A{0}", linhaPartidaSumario + 10)].Value = "Trens que tiveram Chegada";
                ws.Cells[string.Format("A{0}", linhaPartidaSumario + 11)].Value = ".... com Chegada no Horário";
                ws.Cells[string.Format("A{0}", linhaPartidaSumario + 12)].Value = ".... com Atraso na Chegada";
                ws.Cells[string.Format("A{0}", linhaPartidaSumario + 13)].Value = "Trens Suprimidos";
                ws.Cells[string.Format("A{0}", linhaPartidaSumario + 14)].Value = "Trens Cancelados";
                ws.Cells[string.Format("A{0}", linhaPartidaSumario + 15)].Value = "Horas de Multa";
                ws.Cells[string.Format("A{0}", linhaPartidaSumario + 16)].Value = "Atraso OnTime";
                ws.Cells[string.Format("A{0}", linhaPartidaSumario + 17)].Value = "Atraso OnTime (Total)";
                ws.Cells[string.Format("A{0}", linhaPartidaSumario + 18)].Value = " ";

                ws.Cells[string.Format("A{0}:A{1}", linhaPartidaSumario + 1, linhaPartidaSumario + 18)].Style.Font.Bold = true;

                ws.Cells[string.Format("B{0}", linhaPartidaSumario + 1)].Value = dadosOnTime.Indicadores.TotalTrem;
                ws.Cells[string.Format("B{0}", linhaPartidaSumario + 2)].Value = dadosOnTime.Indicadores.TremEncerrados;
                ws.Cells[string.Format("B{0}", linhaPartidaSumario + 3)].Value = dadosOnTime.Indicadores.TremParados;

                ws.Cells[string.Format("B{0}", linhaPartidaSumario + 4)].Value = dadosOnTime.Indicadores.TremPartida;
                ws.Cells[string.Format("B{0}", linhaPartidaSumario + 5)].Value = dadosOnTime.Indicadores.OsRegular;
                ws.Cells[string.Format("B{0}", linhaPartidaSumario + 6)].Value = dadosOnTime.Indicadores.OsIrregular;

                ws.Cells[string.Format("B{0}", linhaPartidaSumario + 7)].Value = dadosOnTime.Indicadores.TremPartidaAtraso;
                ws.Cells[string.Format("B{0}", linhaPartidaSumario + 8)].Value = dadosOnTime.Indicadores.TremPartidaAtrasoOsRegular;
                ws.Cells[string.Format("B{0}", linhaPartidaSumario + 9)].Value = dadosOnTime.Indicadores.TremPartidaAtrasoOsIrregular;

                ws.Cells[string.Format("B{0}", linhaPartidaSumario + 10)].Value = dadosOnTime.Indicadores.TremChegada;
                ws.Cells[string.Format("B{0}", linhaPartidaSumario + 11)].Value = dadosOnTime.Indicadores.TremChegadaHorario;
                ws.Cells[string.Format("B{0}", linhaPartidaSumario + 12)].Value = dadosOnTime.Indicadores.TremChegadaAtraso;

                ws.Cells[string.Format("B{0}", linhaPartidaSumario + 13)].Value = dadosOnTime.Indicadores.TrensSuprimidos433;
                ws.Cells[string.Format("B{0}", linhaPartidaSumario + 14)].Value = dadosOnTime.Indicadores.TrensCancelador371;

                ws.Cells[string.Format("B{0}", linhaPartidaSumario + 15)].Value = dadosOnTime.Indicadores.HorasDeMulta.ToString("00") + ":00";

                ws.Cells[string.Format("B{0}", linhaPartidaSumario + 16)].Value = dadosOnTime.Indicadores.AtrasoOnTime;
                ws.Cells[string.Format("B{0}", linhaPartidaSumario + 17)].Value = dadosOnTime.Indicadores.AtrasoOnTimeTotal;

                ws.Cells[string.Format("B{0}", linhaPartidaSumario + 18)].Value = " ";

                // -> aquela outra tabelinha do final da tela
                ws.Cells[string.Format("D{0}", linhaPartidaSumario + 1)].Value = "Unid. Prod.";
                ws.Cells[string.Format("E{0}", linhaPartidaSumario + 1)].Value = "Partida";
                ws.Cells[string.Format("F{0}", linhaPartidaSumario + 1)].Value = "%";
                ws.Cells[string.Format("G{0}", linhaPartidaSumario + 1)].Value = "Chegada";
                ws.Cells[string.Format("H{0}", linhaPartidaSumario + 1)].Value = "%";

                ws.Cells[string.Format("D{0}:H{0}", linhaPartidaSumario + 1)].Style.Font.Bold = true;

                ws.Cells[string.Format("D{0}", linhaPartidaSumario + 2)].LoadFromCollection(
                    dadosOnTime.Performances.Select(
                        e => new { e.UP, e.PartidaOrigem, e.PercOrigem, e.ChegadaDestino, e.PercDestino })
                    );

                return pkg.GetAsByteArray();
            }
        }

        #endregion

        #region PartidaOT

        /// <summary>
        /// Partida de OnTime
        /// </summary>
        public class PartidaOT
        {
            /// <summary>
            /// Id da OS da partida
            /// </summary>
            public string Id { get; set; }

            /// <summary>
            /// Numero da OS
            /// </summary>
            public string NroOs { get; set; }

            /// <summary>
            /// Prefixo do Trem
            /// </summary>
            public string Trem { get; set; }

            /// <summary>
            /// Unidade Producao Origem
            /// </summary>
            public string UnidadeProducaoOrigem { get; set; }

            /// <summary>
            /// Unidade Producao DEstino
            /// </summary>
            public string UnidadeProducaoDestino { get; set; }

            /// <summary>
            /// Estação de Origem
            /// </summary>
            public string EstacaoOrigem { get; set; }

            /// <summary>
            /// Estação de Destino
            /// </summary>
            public string EstacaoDestino { get; set; }

            /// <summary>
            /// Partida Prevista
            /// </summary>
            public string PartidaPrevista { get; set; }

            /// <summary>
            /// Partida Realizada
            /// </summary>
            public string PartidaRealizada { get; set; }

            /// <summary>
            /// Atraso na Partida
            /// </summary>
            public string AtrasoPartida { get; set; }

            /// <summary>
            /// Chegada Prevista
            /// </summary>
            public string ChegadaPrevista { get; set; }

            /// <summary>
            /// Chegada Realizada
            /// </summary>
            public string ChegadaRealizada { get; set; }

            /// <summary>
            /// Atraso na chegada
            /// </summary>
            public string AtrasoChegada { get; set; }

            /// <summary>
            /// Situacao da Partida
            /// </summary>
            public string Situacao { get; set; }

            /// <summary>
            /// Data Programada
            /// </summary>
            public string DtProg { get; set; }

            /// <summary>
            /// Data Cadastro
            /// </summary>
            public string DtCadastro { get; set; }

            /// <summary>
            /// Data de partida no Cadastro
            /// </summary>
            public string DtPartidaCadastro { get; set; }

            /// <summary>
            /// Matricula de Cadastro
            /// </summary>
            public string MatriculaCadastro { get; set; }

            /// <summary>
            /// Data Partida na ultima alteração
            /// </summary>
            public string DtPartidaUltimaAlteracao { get; set; }

            /// <summary>
            /// Matricula da ultima alteração
            /// </summary>
            public string MatriculaUltimaAlteracao { get; set; }

            /// <summary>
            /// Data da ultima alteração
            /// </summary>
            public string DtUltimaAlteracao { get; set; }

            /// <summary>
            /// Data de partida no Translogic
            /// </summary>
            public string DtPartidaTL { get; set; }

            /// <summary>
            /// Se o registro de OnTime foi alterado no Translogic
            /// </summary>
            public bool AlteradoTranslogic { get; set; }

            /// <summary>
            /// Permitir alteracao OnTime
            /// </summary>
            public bool PermitirAlteracaoOnTime { get; set; }
        }

        #endregion
    }
}
