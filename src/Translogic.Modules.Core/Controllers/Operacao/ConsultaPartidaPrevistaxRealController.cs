﻿namespace Translogic.Modules.Core.Controllers.Operacao
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using System.Web.Script.Serialization;
    using Interfaces.Trem;
    using Interfaces.Trem.OnTime;
    using OfficeOpenXml;
    using Translogic.Core.Infrastructure.Web.Helpers;
    using Translogic.Modules.Core.Domain.Services.Trem;
    using Translogic.Modules.Core.Infrastructure;
    using Util;

    /// <summary>
    /// Classe de consulta do ontime do trem
    /// </summary>
    public class ConsultaPartidaPrevistaxRealController : BaseSecureModuleController
    {
        #region Atributos

        private readonly ICentroTremService _centroTremService;
        private readonly OnTimeService _onTimeService;

        #endregion

        #region Construtor

        /// <summary>
        /// Construtor injetado
        /// </summary>
        /// <param name="centroTremService">Serviço de CentroTrem</param>
        /// <param name="onTimeService">Serviço de OnTime</param>
        public ConsultaPartidaPrevistaxRealController(ICentroTremService centroTremService, OnTimeService onTimeService)
        {
            _centroTremService = centroTremService;
            _onTimeService = onTimeService;
        }

        #endregion

        #region Index

        /// <summary>
        /// View de inicialização
        /// </summary>
        /// <returns>Return view</returns>
        [Autorizar(Transacao = "CONSULTAONTIMEANTT")]
        public ActionResult Index()
        {
            return View();
        }

        #endregion

        #region ObterUnidadeProducao

        /// <summary>
        /// Retorna as Unidades de Producao
        /// </summary>
        /// <returns>Unidades de Producao</returns>
        [Autorizar(Transacao = "CONSULTAONTIMEANTT", Acao = "Pesquisar")]
        public ActionResult ObterUnidadeProducao()
        {
            var result = _onTimeService.ObterUnidadesProducao();

            return Json(new
            {
                Items = result.Select(u => new
                {
                    u.Id,
                    u.DescricaoResumida
                })
            });
        }

        #endregion

        #region Imprimir

        /// <summary>
        /// Obtem os Ontime
        /// </summary>
        /// <param name="codigoUpOrigem">Up de Origem</param>
        /// <param name="codigoUpDestino">Up de Destino</param>
        /// <param name="estOrigem">Estação de Origem</param>
        /// <param name="estDestino">Estação de Destino</param>
        /// <param name="prefixo">Prefixo do trem</param>
        /// <param name="partidaPlanejadaInicial">Partida planejada inicial</param>
        /// <param name="partidaPlanejadaFinal">Partida planejada Final</param>
        /// <param name="chegadaPlanejadaInicial">Chegada planejada inicial</param>
        /// <param name="chegadaPlanejadaFinal">Chegada planejada Final</param>
        /// <param name="prefixosIncluir">Prefixos a incluir</param>
        /// <param name="prefixosExcluir">Prefixos a excluir</param>
        /// <returns>Retorna os ontime</returns>
        [Autorizar(Transacao = "CONSULTAONTIMEANTT", Acao = "Pesquisar")]
        public ActionResult Imprimir(string codigoUpOrigem, string codigoUpDestino, string estOrigem, string estDestino, string prefixo, DateTime? partidaPlanejadaInicial, DateTime? partidaPlanejadaFinal, DateTime? chegadaPlanejadaInicial, DateTime? chegadaPlanejadaFinal, string prefixosIncluir, string prefixosExcluir)
        {
            var retorno = _centroTremService.ObterDadosOnTime(
                new RequisicaoDadosOnTime
                {
                    UnidadeProducaoOrigem = codigoUpOrigem,
                    UnidadeProducaoDestino = codigoUpDestino,
                    AreaOperacionalOrigem = estOrigem,
                    AreaOperacionalDestino = estDestino,
                    Prefixo = prefixo,
                    PrefixosIncluir = prefixosIncluir,
                    PrefixosExcluir = prefixosExcluir,
                    DataInicialPartida = partidaPlanejadaInicial,
                    DataFinalPartida = partidaPlanejadaFinal,
                    DataInicialChegada = chegadaPlanejadaInicial,
                    DataFinalChegada = chegadaPlanejadaFinal
                });

            var items = ObterPartidas(retorno.PartidasTrens);

            ViewData["items"] = items;
            ViewData["ups"] = retorno.Performances;
            ViewData["indicadores"] = retorno.Indicadores;

            var viewString = View("Impressao").Capture(ControllerContext);

            return this.PdfResult("Consulta On Time", "ConsultaOnTime.pdf", viewString);
        }

        #endregion

        #region ObterOnTime

        /// <summary>
        /// Obtem os Ontime
        /// </summary>
        /// <param name="codigoUpOrigem">Up de Origem</param>
        /// <param name="codigoUpDestino">Up de Destino</param>
        /// <param name="estOrigem">Estação de Origem</param>
        /// <param name="estDestino">Estação de Destino</param>
        /// <param name="prefixo">Prefixo do trem</param>
        /// <param name="partidaPlanejadaInicial">Partida planejada inicial</param>
        /// <param name="partidaPlanejadaFinal">Partida planejada Final</param>
        /// <param name="chegadaPlanejadaInicial">Chegada planejada inicial</param>
        /// <param name="chegadaPlanejadaFinal">Chegada planejada Final</param>
        /// <param name="prefixosIncluir">Prefixos a incluir</param>
        /// <param name="prefixosExcluir">Prefixos a excluir</param>
        /// <param name="gerarExcel">Gerar Excel</param>
        /// <returns>Retorna os ontime</returns>
        [Autorizar(Transacao = "CONSULTAONTIMEANTT", Acao = "Pesquisar")]
        public ActionResult ObterOnTime(string codigoUpOrigem, string codigoUpDestino, string estOrigem, string estDestino, string prefixo, DateTime? partidaPlanejadaInicial, DateTime? partidaPlanejadaFinal, DateTime? chegadaPlanejadaInicial, DateTime? chegadaPlanejadaFinal, string prefixosIncluir, string prefixosExcluir, bool? gerarExcel)
        {
            var retorno = _centroTremService.ObterDadosOnTime(
                new RequisicaoDadosOnTime
                {
                    UnidadeProducaoOrigem = codigoUpOrigem,
                    UnidadeProducaoDestino = codigoUpDestino,
                    AreaOperacionalOrigem = estOrigem,
                    AreaOperacionalDestino = estDestino,
                    Prefixo = prefixo,
                    PrefixosIncluir = prefixosIncluir,
                    PrefixosExcluir = prefixosExcluir,
                    DataInicialPartida = partidaPlanejadaInicial,
                    DataFinalPartida = partidaPlanejadaFinal,
                    DataInicialChegada = chegadaPlanejadaInicial,
                    DataFinalChegada = chegadaPlanejadaFinal
                });

            var items = ObterPartidas(retorno.PartidasTrens);

            if (gerarExcel == true)
            {
                return File(ObterExcel(retorno), "application/vnd.ms-excel", "PlanejadaxRealizada.xlsx");
            }

            var serializer = new JavaScriptSerializer { MaxJsonLength = Int32.MaxValue };

            var resultData = new
            {
                Sucesso = true,
                Mensagem = retorno,
                Performance = retorno.Performances,
                Indicadores = retorno.Indicadores,
                Items = items
            };

            var result = new ContentResult
            {
                Content = serializer.Serialize(resultData),
                ContentType = "application/json"
            };

            return result;
        }

        #endregion

        #region Privados

        private IList ObterPartidas(IList<PartidaTremOnTime> retorno)
        {
            var items = retorno.Select(u => new PartidaOT
            {
                Id = u.Id.ToString(),
                NroOs = u.Numero.ToString(),
                Trem = u.Prefixo,
                UnidadeProducaoOrigem = u.UnidadeProducaoOrigem,
                UnidadeProducaoDestino = u.UnidadeProducaoDestino,
                EstacaoOrigem = u.AreaOperacionalOrigem,
                EstacaoDestino = u.AreaOperacionalDestino,
                PartidaPrevista = u.DataPartidaOficial.HasValue
                       ? u.DataPartidaOficial.Value.ToString("dd/MM/yyyy HH:mm:ss")
                       : string.Empty,
                PartidaRealizada = u.DataPartidaReal.HasValue
                        ? u.DataPartidaReal.Value.ToString("dd/MM/yyyy HH:mm:ss")
                        : string.Empty
            }).ToList();
            return items;
        }

        /// <summary>
        /// Obtem o Excel
        /// </summary>
        /// <param name="dadosOnTime">Dados de OnTime</param>
        /// <returns>array de bytes do excel</returns>
        private byte[] ObterExcel(DadosOnTime dadosOnTime)
        {
            using (var pkg = new ExcelPackage())
            {
                var ws = pkg.Workbook.Worksheets.Add("OnTime");
                ws.Cells["A1"].LoadFromCollection(dadosOnTime.PartidasTrens.Select(e => new
                {
                    Trem = e.Prefixo,
                    NroOs = e.Numero.ToString(),
                    e.AreaOperacionalOrigem,
                    e.AreaOperacionalDestino,
                    DataPartidaOficial = e.DataPartidaOficial.HasValue ?
                                            e.DataPartidaOficial.Value.ToString("dd/MM/yyyy HH:mm:ss") : String.Empty,
                    DataPartidaReal = e.DataPartidaReal.HasValue ?
                                            e.DataPartidaReal.Value.ToString("dd/MM/yyyy HH:mm:ss") : String.Empty
                }));

                ws.InsertRow(1, 1);
                ws.Cells["A1:X1"].Style.Font.Bold = true;

                ws.Cells["A1"].Value = "Trem";
                ws.Cells["B1"].Value = "Os";
                ws.Cells["C1"].Value = "Origem";
                ws.Cells["D1"].Value = "Destino";
                ws.Cells["E1"].Value = "Partida Prev.";
                ws.Cells["F1"].Value = "Partida Real.";

              return pkg.GetAsByteArray();
            }
        }

        #endregion

        #region PartidaOT

        /// <summary>
        /// Partida de OnTime
        /// </summary>
        public class PartidaOT
        {
            /// <summary>
            /// Id da OS da partida
            /// </summary>
            public string Id { get; set; }

            /// <summary>
            /// Numero da OS
            /// </summary>
            public string NroOs { get; set; }

            /// <summary>
            /// Prefixo do Trem
            /// </summary>
            public string Trem { get; set; }

            /// <summary>
            /// Unidade Producao Origem
            /// </summary>
            public string UnidadeProducaoOrigem { get; set; }

            /// <summary>
            /// Unidade Producao DEstino
            /// </summary>
            public string UnidadeProducaoDestino { get; set; }

            /// <summary>
            /// Estação de Origem
            /// </summary>
            public string EstacaoOrigem { get; set; }

            /// <summary>
            /// Estação de Destino
            /// </summary>
            public string EstacaoDestino { get; set; }

            /// <summary>
            /// Partida Prevista
            /// </summary>
            public string PartidaPrevista { get; set; }

            /// <summary>
            /// Partida Realizada
            /// </summary>
            public string PartidaRealizada { get; set; }

            /// <summary>
            /// Atraso na Partida
            /// </summary>
            public string AtrasoPartida { get; set; }

            /// <summary>
            /// Chegada Prevista
            /// </summary>
            public string ChegadaPrevista { get; set; }

            /// <summary>
            /// Chegada Realizada
            /// </summary>
            public string ChegadaRealizada { get; set; }

            /// <summary>
            /// Atraso na chegada
            /// </summary>
            public string AtrasoChegada { get; set; }

            /// <summary>
            /// Situacao da Partida
            /// </summary>
            public string Situacao { get; set; }

            /// <summary>
            /// Data Programada
            /// </summary>
            public string DtProg { get; set; }

            /// <summary>
            /// Data Cadastro
            /// </summary>
            public string DtCadastro { get; set; }

            /// <summary>
            /// Data de partida no Cadastro
            /// </summary>
            public string DtPartidaCadastro { get; set; }

            /// <summary>
            /// Matricula de Cadastro
            /// </summary>
            public string MatriculaCadastro { get; set; }

            /// <summary>
            /// Data Partida na ultima alteração
            /// </summary>
            public string DtPartidaUltimaAlteracao { get; set; }

            /// <summary>
            /// Matricula da ultima alteração
            /// </summary>
            public string MatriculaUltimaAlteracao { get; set; }

            /// <summary>
            /// Data da ultima alteração
            /// </summary>
            public string DtUltimaAlteracao { get; set; }

            /// <summary>
            /// Data de partida no Translogic
            /// </summary>
            public string DtPartidaTL { get; set; }

            /// <summary>
            /// Se o registro de OnTime foi alterado no Translogic
            /// </summary>
            public bool AlteradoTranslogic { get; set; }

            /// <summary>
            /// Permitir alteracao OnTime
            /// </summary>
            public bool PermitirAlteracaoOnTime { get; set; }
        }

        #endregion
    }
}
