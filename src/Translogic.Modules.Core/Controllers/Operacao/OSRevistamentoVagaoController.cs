﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Translogic.Modules.Core.Infrastructure;
using Translogic.Modules.Core.Domain.Model.Dto;
using Translogic.Core.Infrastructure.Web;
using Translogic.Modules.Core.Domain.Services.OSLimpezaVagao.Interface;
using Translogic.Modules.Core.Domain.Services.Fornecedor.Interface;
using Translogic.Modules.Core.Domain.Services.OSRevistamentoVagao.Interface;
using System.IO;
using Translogic.Core.Infrastructure.FileSystem.ExtensionMethods;
using Translogic.Modules.Core.Domain.Services.Mdfes;
using Translogic.Core.Infrastructure.Web.Helpers;
using Translogic.Core.Infrastructure.Web.ContentResults;

namespace Translogic.Modules.Core.Controllers.Operacao
{
    public class OSRevistamentoVagaoController : BaseSecureModuleController
    {
        #region Serviços

        private readonly IOSTipoService _osTipoService;
        private readonly IOSRevistamentoVagaoService _osRevistamentoVagaoService;
        private readonly IOSStatusService _osStatusService;
        private readonly IFornecedorOsService _fornecedorOsService;
        private readonly MdfeService _mdfeService;

        #endregion

        #region Construtores

        public OSRevistamentoVagaoController(
            IOSTipoService osTipoService,
            IOSStatusService osStatusService,
            IFornecedorOsService fornecedorOsService,
            IOSRevistamentoVagaoService osRevistamentoVagaoService,
            MdfeService mdfeService)
        {
            _osTipoService = osTipoService;
            _osStatusService = osStatusService;
            _fornecedorOsService = fornecedorOsService;
            _osRevistamentoVagaoService = osRevistamentoVagaoService;
            _mdfeService = mdfeService;
        }

        #endregion

        #region ActionViews

        /// <summary>
        /// Formulário de consulta da OS de Revistamento de Vagão.
        /// </summary>
        /// <returns>Retorna a view referente a tela de </returns>
        [Autorizar(Transacao = "OPEOSREVISTVGO")]
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// View de pesquisa de vagões para criar OS
        /// </summary>
        /// <returns></returns>
        [Autorizar(Transacao = "OPEOSREVISTVGO")]
        public ActionResult PesquisarVagoes(int? idOs, string local)
        {
            if (idOs.HasValue)
            {
                ViewData["local"] = local;
                ViewData["idOs"] = idOs.Value;
            }

            return View();
        }

        /// <summary>
        /// View para Criação da OS
        /// </summary>
        /// <param name="idOS"></param>
        /// <param name="local"></param>
        /// <returns></returns>
        [Autorizar(Transacao = "OPEOSREVISTVGO")]
        public ActionResult CriarOSRevistamento(int idOS, string local)
        {
            ViewData["idOs"] = idOS;
            ViewData["local"] = local;
            ViewData["data"] = DateTime.Now.ToString("dd/MM/yyyy HH:mm");

            return View();
        }

        [Autorizar(Transacao = "OPEOSREVISTVGO")]
        public ActionResult EditarOSRevistamento(int idOS)
        {
            var os = _osRevistamentoVagaoService.ObterOS(idOS);

            ViewData["idOs"] = idOS;
            ViewData["local"] = os.LocalServico.ToUpper();
            ViewData["data"] = os.Data.ToString("dd/MM/yyyy HH:mm");
            ViewData["idFornecedor"] = os.IdFornecedor.ToString();

            return View();
        }

        /// <summary>
        /// View para fechamento de OS
        /// </summary>
        /// <param name="idOS"></param>
        /// <returns></returns>
        [Autorizar(Transacao = "OPEOSREVISTVGO")]
        public ActionResult FecharOSRevistamento(int idOS)
        {
            var os = _osRevistamentoVagaoService.ObterOS(idOS);

            ViewData["idOs"] = idOS;
            ViewData["local"] = os.LocalServico.ToUpper();
            ViewData["data"] = os.Data.ToString("dd/MM/yyyy HH:mm");
            ViewData["idFornecedor"] = os.IdFornecedor.ToString();

            return View();
        }

        [Autorizar(Transacao = "OPEOSREVISTVGO")]
        public ActionResult CancelarOSRevistamento(int idOS)
        {
            var os = _osRevistamentoVagaoService.ObterOS(idOS);

            ViewData["idOs"] = idOS;
            ViewData["local"] = os.LocalServico.ToUpper();
            ViewData["data"] = os.Data.ToString("dd/MM/yyyy HH:mm");
            ViewData["idFornecedor"] = os.IdFornecedor.ToString();

            return View();
        }

        /// <summary>
        /// Action para view de Visualização
        /// </summary>
        /// <param name="idOS"></param>
        /// <returns></returns>
        [Autorizar(Transacao = "OPEOSREVISTVGO")]
        public ActionResult VisualizarOSRevistamento(int idOS)
        {
            ViewData["idOs"] = idOS;
            return View();
        }

        /// <summary>
        /// Retorna o PDF da OS
        /// </summary>
        /// <param name="idOS">id da OS</param>
        /// <returns>resultado json</returns>
        [Autorizar(Transacao = "RELOSLIMPVGO")]
        public ActionResult Imprimir(int idOS)
        {
            Stream pdf = null;
            Stream returnContent = new MemoryStream();

            var merge = new PdfMerge();

            var os = _osRevistamentoVagaoService.ObterOS(idOS);

            ViewData["IdOS"] = idOS;
            ViewData["IdOsParcial"] = (os.IdOsParcial.ToString() != "0" ? os.IdOsParcial.ToString() : "");
            ViewData["Data"] = os.Data.ToString("dd/MM/yyyy HH:mm");
            ViewData["HoraInicio"] = os.HoraInicio;
            ViewData["HoraTermino"] = os.HoraTermino;
            ViewData["HoraEntrega"] = os.HoraEntrega;
            ViewData["Linha"] = os.Linha;
            ViewData["Convocacao"] = os.Convocacao;
            ViewData["QtdeVagoes"] = os.QtdeVagoes;
            ViewData["QtdeVagoesVedados"] = os.QtdeVagoesVedados;
            ViewData["QtdeVagoesGambitados"] = os.QtdeVagoesGambitados;
            ViewData["QtdeVagoesVazios"] = os.QtdeVagoesVazios;
            ViewData["Fornecedor"] = os.Fornecedor;
            ViewData["LocalServico"] = os.LocalServico;
            ViewData["Tipo"] = os.Tipo;
            ViewData["Status"] = os.Status;
            ViewData["NomeManutencao"] = os.NomeManutencao;
            ViewData["MatriculaManutencao"] = os.MatriculaManutencao;
            ViewData["NomeEstacao"] = os.NomeEstacao;
            ViewData["MatriculaEstacao"] = os.MatriculaEstacao;
            ViewData["Justificativa"] = os.JustificativaCancelamento;
            ViewData["UltimaAlteracao"] = os.UltimaAlteracao.ToString("dd/MM/yyyy HH:mm");
            ViewData["Usuario"] = os.Usuario;

            ViewData["Vagoes"] = _osRevistamentoVagaoService.ObterVagoesOS(null, idOS).Items;

            var viewString = View("ImpressaoOS").Capture(ControllerContext);

            pdf = _mdfeService.HtmlToPdf(viewString, "pdfOS", true);

            if (pdf != null)
            {
                merge.AddFile(pdf);
                returnContent = merge.Execute();
            }

            var fsr = new FileStreamResult(returnContent, "application/pdf");
            fsr.FileDownloadName = "OS " + idOS.ToString() + ".pdf";
            return fsr;
        }

        #endregion

        #region Métodos

        /// <summary>
        /// 
        /// </summary>
        /// <param name="listaDeVagoes"></param>
        /// <param name="idLocal"></param>
        /// <param name="idOS"></param>
        /// <param name="idLotacao"></param>
        /// <param name="idSituacao"></param>
        /// <param name="idMotivo"></param>
        /// <returns></returns>
        [Autorizar(Transacao = "OPEOSREVISTVGO")]
        public ActionResult ObterConsultarOSRevistamentoVagoesExportar(string dataInicial, string dataFinal, string local, string numOs, string idTipo, string idMotivo, string idStatus, string retrabalho, bool vagaoRetirado, bool problemaVedacao, bool vagaoCarregado)
        {
            var result = this._osRevistamentoVagaoService.ObterConsultaOsRevistamentoExportar(
                                                                    dataInicial,
                                                                    dataFinal,
                                                                    local,
                                                                    numOs,
                                                                    idTipo,
                                                                    idStatus,
                                                                    vagaoRetirado,
                                                                    problemaVedacao,
                                                                    vagaoCarregado
                                                                   );

            var dicOSRevistamentoVagao = new Dictionary<string, Func<OSRevistamentoVagaoExportaDto, string>>();

            dicOSRevistamentoVagao["Data"] = c => (c.Data != null) ? c.Data.ToString("dd/MM/yyyy HH:mm") : String.Empty;
            dicOSRevistamentoVagao["OS"] = c => c.IdOsParcial > 0 ? string.Format("{0} ({1})", c.IdOs.ToString(), c.IdOsParcial) : c.IdOs.ToString();
            dicOSRevistamentoVagao["Local"] = c => !String.IsNullOrEmpty(c.Local) ? c.Local : String.Empty;
            dicOSRevistamentoVagao["Fornecedor"] = c => !String.IsNullOrEmpty(c.Fornecedor) ? c.Fornecedor : String.Empty;
            dicOSRevistamentoVagao["Hora Início"] = c => (c.HoraInicio != null) ? c.HoraInicio.Value.ToString("HH:mm") : String.Empty;
            dicOSRevistamentoVagao["Hora Término"] = c => (c.HoraTermino != null) ? c.HoraTermino.Value.ToString("HH:mm") : String.Empty;
            dicOSRevistamentoVagao["Vagão"] = c => (c.NumeroVagao != null) ? c.NumeroVagao : String.Empty;
            dicOSRevistamentoVagao["Retido"] = c => (c.Retirar == 1) ? "Sim" : "Não";
            dicOSRevistamentoVagao["Vedados"] = c => c.QtdeVagoesVedados.ToString();
            dicOSRevistamentoVagao["Gambitados"] = c => c.QtdeVagoesGambitados.ToString();
            dicOSRevistamentoVagao["Vedação/Gambitagem com Problema"] = c => (c.ProblemaVedacao == 1) ? "Sim" : "Não";
            dicOSRevistamentoVagao["Observacao"] = c => !String.IsNullOrEmpty(c.Observacao) ? c.Observacao : String.Empty;
            dicOSRevistamentoVagao["Fornecedor: Nome"] = c => !String.IsNullOrEmpty(c.NomeManutencao) ? c.NomeManutencao : String.Empty;
            dicOSRevistamentoVagao["Status OS"] = c => !String.IsNullOrEmpty(c.Status) ? c.Status : String.Empty;
            dicOSRevistamentoVagao["Últ. Alteração OS"] = c => (c.UltimaAlteracao != null) ? c.UltimaAlteracao.ToString("dd/MM/yyyy HH:mm") : String.Empty;
            dicOSRevistamentoVagao["Usuário"] = c => !String.IsNullOrEmpty(c.Usuario) ? c.Usuario : String.Empty;

            return this.Excel(string.Format("OSRevistamentoVagoes_{0}_{1}.xls", DateTime.Now.ToString("ddMMyyyy"), DateTime.Now.ToString("HHmmss")), dicOSRevistamentoVagao, result);
        }

        /// <summary>
        /// Salvar Vagoes Da Os
        /// </summary>
        /// <param name="idsVagoes"></param>
        /// <param name="string local"></param>
        /// <returns></returns>
        [Autorizar(Transacao = "OPEOSREVISTVGO")]
        public int SalvarVagoesDaOs(int? idOs, IList<int> idsVagoes, string local)
        {
            idOs = _osRevistamentoVagaoService.InserirAtualizarVagoes(idOs, idsVagoes, UsuarioAtual.Codigo, local);

            return idOs.Value;
        }

        [Autorizar(Transacao = "OPEOSREVISTVGO")]
        public int SalvarOS(int idOs, int idFornecedor, string local)
        {
            if (idOs == 0)
                throw new Exception("Favor fornecer o número da OS para atualizar");

            if (idFornecedor == 0)
                throw new Exception("Campo fornecedor obrigatório");

            if (string.IsNullOrEmpty(local))
                throw new Exception("Campo local de prestação de serviço obrigatório");

            _osRevistamentoVagaoService.AtualizarOSeVagoes(idOs, idFornecedor, local);

            return 1;
        }

        /// <summary>
        /// Editar OS, no caso somente fornecedor
        /// </summary>
        /// <param name="idOs"></param>
        /// <param name="idFornecedor"></param>
        [Autorizar(Transacao = "OPEOSREVISTVGO")]
        public int EditarOs(int idOs, int idFornecedor)
        {
            if (idOs == 0)
                throw new Exception("Favor fornecer o número da OS para atualizar");

            if (idFornecedor == 0)
                throw new Exception("Campo fornecedor obrigatório");

            _osRevistamentoVagaoService.EditarOs(idOs, idFornecedor);

            return 1;
        }

        /// <summary>
        /// Cancelar OS atualizando status para 'Cancelada'
        /// </summary>
        /// <param name="idOs"></param>
        [Autorizar(Transacao = "OPEOSREVISTVGO")]
        public JsonResult CancelarOS(int idOs, string justificativa)
        {
            try
            {
                #region Validações

                if (idOs == 0)
                    throw new Exception("Favor fornecer o número da OS para cancelamento");
                if (string.IsNullOrEmpty(justificativa))
                    throw new Exception("Campo justificativa de cancelamento é obrigatório para cancelamento");

                if (justificativa.Length > 250)
                    throw new Exception("A justificativa de cancelamento pode conter somente até 250 caracteres");
                

                justificativa = (justificativa != null ? justificativa.Replace("<", "").Replace(">", "") : "");

                #endregion

                try
                {
                    _osRevistamentoVagaoService.CancelarOs(idOs, justificativa);
                }
                catch (Exception ex)
                {
                    throw new Exception("Não foi possivel atualizar a OS. Erro: " + ex.Message);
                }

                return Json(new
                {
                    Success = true,
                    Status = 200,
                    idOs = idOs
                });
            }
            catch (Exception e)
            {
                return Json(new
                {
                    Success = false,
                    e.Message,
                    Status = 200
                });
            }
        }

        /// <summary>
        /// Chamada de método que cancela OS ao voltar.
        /// </summary>
        /// <param name="idOs"></param>
        /// <returns></returns>
        [Autorizar(Transacao = "OPEOSREVISTVGO")]
        public int Voltar(int idOs)
        {
            if (idOs == 0)
                throw new Exception("Favor fornecer o número da OS para cancelamento");
            try
            {
                return _osRevistamentoVagaoService.CancelarOs(idOs);
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possivel atualizar a OS. Erro: " + ex.Message);
            }
        }

        /// <summary>
        /// Atualizar a OS com dados de fechamento e atualizar status para 'Fechada'
        /// </summary>
        /// <param name="idOs"></param>
        /// <param name="dataHoraEntregaOsFornecedor"></param>
        /// <param name="dataHoraDevolucaoOSRumo"></param>
        /// <param name="nomeAprovadorRumo"></param>
        /// <param name="matriculaAprovadorRumo"></param>
        /// <param name="nomeAprovadorFornecedor"></param>
        /// <param name="matriculaAprovadorFornecedor"></param>
        [Autorizar(Transacao = "OPEOSREVISTVGO")]
        public int FecharOs(int idOs, string horaInicio, string horaTermino, string horaEntrega, string linha, string convocacao, int quantVagoes, 
            int idFornecedor, string local, int quantVagoesVedados, int quantVagoesGambitados, int quantVagoesVazios, 
            string nomeManutencao, string matriculaManutencao, string nomeEstacao, string matriculaEstacao,
            IList<OSRevistamentoVagaoItemDto> vagoes)
        {
            if (idOs == 0)
                throw new Exception("Favor fornecer o número da OS para fechamento");
            if (idFornecedor == 0)
                throw new Exception("Campo fornecedor é obrigatório");
            if (string.IsNullOrEmpty(horaInicio))
                throw new Exception("Campo hora inicial é obrigatório");
            if (string.IsNullOrEmpty(horaTermino))
                throw new Exception("Campo hora termino é obrigatório");
            if (string.IsNullOrEmpty(horaEntrega))
                throw new Exception("Campo hora entrega é obrigatório");
            if (string.IsNullOrEmpty(linha))
                throw new Exception("Campo linha é obrigatório");
            if (quantVagoes == 0)
                throw new Exception("Campo quantidade de vagões é obrigatório");
            if (string.IsNullOrEmpty(nomeManutencao))
                throw new Exception("Campo Nome Manutenção é obrigatório");
            if (string.IsNullOrEmpty(nomeEstacao))
                throw new Exception("Campo Nome Estação é obrigatório");

            try
            {
                return _osRevistamentoVagaoService.FecharOs(idOs, horaInicio, horaTermino, horaEntrega, linha, convocacao, quantVagoes, idFornecedor,
                   local, quantVagoesVedados, quantVagoesGambitados, quantVagoesVazios,
                   nomeManutencao, matriculaManutencao, nomeEstacao, matriculaManutencao, UsuarioAtual.Codigo, vagoes);
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possivel atualizar a OS. Erro: " + ex.Message);
            }

            return idOs;
        }
     
        #endregion

        #region JsonResults

        /// <summary>
        /// Retornar tipos 
        /// </summary>
        /// <returns></returns>
        public JsonResult ObterTipos()
        {
            var resultado = _osTipoService.ObterTiposOSRevistamento();

            var jsonData = Json(
                new
                {
                    Items = resultado.Select(i => new
                    {
                        i.IdTipo,
                        i.DescricaoTipo
                    }),
                    Total = resultado.Count
                });
            return jsonData;
        }

        /// <summary>
        /// Obter locais de fornecedores com tipo de serviço revistamento
        /// </summary>
        /// <returns></returns>
        public JsonResult ObterLocais()
        {
            var resultado = _fornecedorOsService.ObterLocaisParaTipoServico();

            var jsonData = Json(
                new
                {
                    Items = resultado.Select(i => new
                    {
                        i.IdLocal,
                        i.Local
                    }),
                    Total = resultado.Count
                });
            return jsonData;
        }

        /// <summary>
        /// Obter Status
        /// </summary>
        /// <returns></returns>
        public JsonResult ObterStatus()
        {
            var resultado = _osStatusService.ObterStatus();

            resultado.Insert(resultado.Count, new OSStatusDto { IdStatus = 99, DescricaoStatus = "Aberta/Parcial" });

            var jsonData = Json(
                new
                {
                    Items = resultado.Select(i => new
                    {
                        i.IdStatus,
                        i.DescricaoStatus
                    }),
                    Total = resultado.Count
                });
            return jsonData;
        }

        /// <summary>
        /// Retonar as lotações
        /// </summary>
        /// <returns></returns>
        public JsonResult ObterLotacoes()
        {
            //Reaproveitado método do OSLimpezaVagaoService
            var resultado = _osRevistamentoVagaoService.ObterLotacoes();

            var jsonData = Json(
                new
                {
                    Items = resultado.Select(i => new
                    {
                        i.IdLotacao,
                        i.DescricaoLotacao
                    }),
                    Total = resultado.Count
                });
            return jsonData;
        }

        /// <summary>
        /// Retornar fornecedores filtrados por ativos e local
        /// </summary>
        /// <returns></returns>
        public JsonResult ObterFornecedores(string local)
        {
            var resultado = _fornecedorOsService.ObterFornecedor(local, Util.EnumTipoFornecedor.Revistamento, false);

            resultado.Insert(0, new FornecedorOsDto { IdFornecedorOs = 0, Nome = "" });

            var jsonData = Json(
                new
                {
                    Items = resultado.Select(i => new
                    {
                        i.IdFornecedorOs,
                        i.Nome,
                    }),
                    Total = resultado.Count
                });
            return jsonData;
        }


        /// <summary>
        /// Retornar todos os fornecedores
        /// </summary>
        /// <returns></returns>
        public JsonResult ObterTodosFornecedores(string local)
        {
            var resultado = _fornecedorOsService.ObterFornecedor(local, Util.EnumTipoFornecedor.Revistamento, true);

            resultado.Insert(0, new FornecedorOsDto { IdFornecedorOs = 0, Nome = "" });

            var jsonData = Json(
                new
                {
                    Items = resultado.Select(i => new
                    {
                        i.IdFornecedorOs,
                        i.Nome,
                    }),
                    Total = resultado.Count
                });
            return jsonData;
        }
      
        /// <summary>
        /// Retornar a lista de vagões para OS
        /// </summary>
        /// <param name="detalhesPaginacaoWeb"></param>
        /// <param name="numVagao"></param>
        /// <param name="serie"></param>
        /// <param name="local"></param>
        /// <param name="lotacao"></param>
        /// <param name="sequenciaInicio"></param>
        /// <param name="sequenciaFim"></param>
        /// <returns></returns>
        public JsonResult ConsultaVagoes(DetalhesPaginacaoWeb detalhesPaginacaoWeb, int? numOs, string numVagao, string serie, string local, string linha, string lotacao, int sequenciaInicio, int sequenciaFim)
        {
            var resultado = _osRevistamentoVagaoService.ObterConsultaDeVagoesParaOs(detalhesPaginacaoWeb, numOs, numVagao, serie, local, linha, lotacao, sequenciaInicio, sequenciaFim);

            var jsonData = Json(
              new
              {
                  Items = resultado.Items.Select(i => new
                  {
                      //Campos
                      Checked = (i.Marcado == 1 ? true : false),
                      i.NumeroVagao,
                      i.Serie,
                      i.Local,
                      i.Linha,
                      i.Sequencia,
                      i.DescricaoLotacao
                  }),
                  Total = resultado.Total
              });
            return jsonData;
        }

        /// <summary>
        /// Obter vagões da OS
        /// </summary>
        /// <param name="idOs">ID da OS</param>
        /// <returns></returns>
        public JsonResult ObterVagoesDaOs(DetalhesPaginacaoWeb detalhesPaginacaoWeb, int idOs)
        {
            var resultado = _osRevistamentoVagaoService.ObterVagoesOS(detalhesPaginacaoWeb, idOs);
            
            var jsonData = Json(
                new
                {
                    Items = resultado.Items.Select(i => new
                    {
                        i.IdItem,
                        i.IdVagao,
                        i.Serie,
                        i.NumeroVagao,
                        i.Observacao,
                        i.Concluido,
                        i.ConcluidoDescricao,
                        i.Retirar,
                        i.RetirarDescricao,
                        i.Comproblema,
                        i.ComproblemaDescricao
                    }),
                    Total = resultado.Total
                });
            return jsonData;
        }

        public JsonResult ConsultaOSRevistamento(DetalhesPaginacaoWeb detalhesPaginacaoWeb, string dataInicial, string dataFinal,
            string local, string numOs, string idTipo, string idStatus, bool vagaoRetirado, bool problemaVedacao, bool vagaoCarregado)
        {
            var resultado = _osRevistamentoVagaoService.ObterConsultaListaOs(detalhesPaginacaoWeb, dataInicial, dataFinal, local, numOs, idTipo, idStatus, vagaoRetirado, problemaVedacao, vagaoCarregado);

            var jsonData = Json(
                new
                {
                    Items = resultado.Items.Select(i => new
                    {
                        //Campos
                        IdOs = i.IdOs,
                        Data = i.Data.ToString("dd/MM/yyyy HH:mm:ss"),
                        i.LocalServico,
                        NumOs = i.IdOsParcial > 0 ? string.Format("{0} ({1})", i.IdOs, i.IdOsParcial) : i.IdOs.ToString(),
                        i.Tipo,
                        i.Fornecedor,
                        i.Status,
                        UltimaAlteracao = i.UltimaAlteracao.ToString("dd/MM/yyyy HH:mm:ss"),
                        VagaoRetirado = i.Status.ToLower() == "aberta" || i.Status.ToLower() == "parcial" ? "" : (i.VagaoRetirado == "S" ? "Sim" : "Não"),
                        ProblemaVedacao = i.Status.ToLower() == "aberta" || i.Status.ToLower() == "parcial" ? "" : (i.ProblemaVedacao == "S" ? "Sim" : "Não"),
                        CarregadoEntreVazios = i.Status.ToLower() == "aberta" || i.Status.ToLower() == "parcial" ? "" : (i.CarregadoEntreVazios == "S" ? "Sim" : "Não"),
                        Usuario = i.Usuario
                    }),
                    Total = resultado.Total
                });
            return jsonData;
        }

        /// <summary>
        /// Buscar OS para edição
        /// </summary>
        /// <param name="idOs"></param>
        /// <returns></returns>
        public JsonResult ObterOs(int idOs)
        {
            var resultado = _osRevistamentoVagaoService.ObterOS(idOs);

            var jsonData = Json(
                new
                {
                    IdOs = resultado.IdOs,
                    IdOsParcial = resultado.IdOsParcial,
                    Data = resultado.Data.ToString("dd/MM/yyyy HH:mm:ss"),
                    resultado.Fornecedor,
                    resultado.LocalServico,
                    resultado.Tipo,
                    resultado.Status,
                    Convocacao = string.IsNullOrEmpty(resultado.Convocacao) ? "" : resultado.Convocacao,
                    HoraInicio = (resultado.HoraInicio.HasValue ? resultado.HoraInicio.Value.ToString("HH:mm:ss") : ""),
                    HoraTermino = (resultado.HoraTermino.HasValue ? resultado.HoraTermino.Value.ToString("HH:mm:ss") : ""),
                    HoraEntrega = (resultado.HoraEntrega.HasValue ? resultado.HoraEntrega.Value.ToString("HH:mm:ss") : ""),
                    Linha = string.IsNullOrEmpty(resultado.Linha) ? "" : resultado.Linha,
                    NomeEstacao = string.IsNullOrEmpty(resultado.NomeEstacao) ? "" : resultado.NomeEstacao,
                    MatriculaEstacao = string.IsNullOrEmpty(resultado.MatriculaEstacao) ? "" : resultado.MatriculaEstacao,
                    NomeManutencao = string.IsNullOrEmpty(resultado.NomeManutencao) ? "" : resultado.NomeManutencao,
                    MatriculaManutencao = string.IsNullOrEmpty(resultado.MatriculaManutencao) ? "" : resultado.MatriculaManutencao,
                    QtdeVagoes = resultado.QtdeVagoes,
                    QtdeVagoesGambitados = resultado.QtdeVagoesGambitados,
                    QtdeVagoesVazios = resultado.QtdeVagoesVazios,
                    QtdeVagoesVedados = resultado.QtdeVagoesVedados,
                    UltimaAlteracao = resultado.UltimaAlteracao

                });
            return jsonData;
        }

        #endregion
    }
}
