﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Translogic.Modules.Core.Domain.Model.Dto;
using Translogic.Modules.Core.Util;
using Translogic.Core.Infrastructure.Web;
using Translogic.Modules.Core.Domain.Services.VooAtivo.Interface;
using Translogic.Modules.Core.Domain.Services.Via.Interface;
using Translogic.Modules.Core.Domain.Model.Via;
using ALL.Core.Dominio;
using Translogic.Modules.Core.Domain.Model.VooAtivo;
using System.Text;

namespace Translogic.Modules.Core.Controllers.Operacao
{
    public class VooAtivoController : BaseSecureModuleController
    {
        #region Services

        private readonly ISolicitacaoVooService _solicitacaoVooService;
        private readonly IElementoViaService _elementoViaService;
        private readonly IMotivoVooService _motivoVooService;
        private readonly IEstacaoMaeService _estacaoMaeService;       
        
        #endregion

        #region Construtores

        public VooAtivoController(ISolicitacaoVooService solicitacaoVooService, 
            IElementoViaService elementoViaService,
            IMotivoVooService motivoVooService,
            IEstacaoMaeService estacaoMaeService)
        {
            _solicitacaoVooService = solicitacaoVooService;
            _elementoViaService = elementoViaService;
            _motivoVooService = motivoVooService;
            _estacaoMaeService = estacaoMaeService;           
        }

        #endregion     
        
        #region Solicitação
        
        #region Views

        /// <summary>
        /// View para pesquisar ativos para solicitação de Voo
        /// </summary>
        /// <returns></returns>
        public ActionResult BuscarAtivosSolicitar(int? tipoAtivo, string ativos, string patio, string linha, string os)
        {
            ViewData["tipoAtivo"] = tipoAtivo;
            ViewData["ativos"] = ativos;
            ViewData["patio"] = patio;
            ViewData["linha"] = linha;
            ViewData["os"] = os;

            return View();
        }

        /// <summary>
        /// View para realizar a solicitação de Voo
        /// </summary>
        /// <param name="ativos">Lista de ativos pesquisados</param>
        /// <returns></returns>
        public ActionResult Solicitar(int tipoAtivo, string ativos, string patio, string linha, string os)
        {
            ViewData["tipoAtivo"] = tipoAtivo;
            ViewData["ativos"] = ativos;
            ViewData["patio"] = patio;
            ViewData["linha"] = linha;
            ViewData["os"] = os;

            return View();
        }

        #endregion

        #region Métodos

        /// <summary>
        /// Realizar split dos ativos e monta o filtro para uso em "In"
        /// </summary>
        /// <param name="ativos">Ativos separados por virgula</param>
        /// <returns>Filtros preparados para uso em IN</returns>
        private string MontaFiltroAtivos(string ativos) 
        {
            string[] separados = ativos.Split(',');

            var filtroAtivos = new StringBuilder();
            var rowCount = 1;
            foreach(var ativo in separados){
                filtroAtivos.AppendFormat("'{0}'", ativo);
                if (rowCount < separados.Length)
                    filtroAtivos.Append(",");
                rowCount++;

            }
            return filtroAtivos.ToString();
        }

        /// <summary>
        /// Pesquisar os ativos para realizar solicitação de voo validando se houve resultados
        /// </summary>
        /// <returns></returns>
        public JsonResult PesquisarAtivosSolicitar(DetalhesPaginacaoWeb detalhesPaginacaoWeb, int tipoAtivo, string ativos, string patio, string linha, string os)
        {
            var filtroAtivos = MontaFiltroAtivos(ativos.Trim().Replace(" ",""));
            patio = patio.ToUpper();         
            if (linha == "Selecione")
                linha = "";

            var list = BuscarDadosAtivosSolicitacao(detalhesPaginacaoWeb, filtroAtivos, tipoAtivo, patio.Trim(), linha.Trim(), os.Trim());
            if (list.Total == 0)
            {
                return Json(new
                {
                    Result = false,
                    Message = "Não foi localizado nenhum item para os filtros utilizados.",
                    Total = 0
                });
            }
            else
            {
                return Json(new
                {
                    Result = true,
                    Message = "Pesquisado com sucesso",
                    Items = list.Items.Select(i => new
                    {
                        Selecionavel = i.Selecionavel,
                        Ativo = i.Ativo,
                        IdAtivo = i.IdAtivo,
                        TipoAtivo = i.TipoAtivo,
                        DataEvento = i.DataEvento.ToString("dd/MM/yyyy HH:mm:ss"),
                        PatioOrigem = i.PatioOrigem,
                        Situacao = i.Situacao,
                        CondicaoUso = i.CondicaoUso,
                        Local = i.Local,
                        Responsavel = i.Responsavel,
                        Lotacao = i.Lotacao,
                        Intercambio = i.Intercambio,
                        Linha = i.Linha,
                        Serie = i.Serie,
                        Sequencia = i.Sequencia,
                        Bitola = i.Bitola
                    }),
                    Total = list.Total
                });
            }
        }

        /// <summary>
        /// Buscar os ativos conforme parâmetros de busca e listar em GRID
        /// </summary>
        /// <returns></returns>
        private ResultadoPaginado<SolicitacaoVooAtivoItemDto> BuscarDadosAtivosSolicitacao(DetalhesPaginacaoWeb detalhesPaginacaoWeb, string ativos, int tipo, string patio, string linha, string os) 
        {
            var enumTipo = (EnumTipoAtivo)tipo;
            var ativosFiltro = ativos.Replace("'", "");

            if (!string.IsNullOrEmpty(ativosFiltro))
            {
                ativosFiltro = ativosFiltro.Replace(';', ',');
                
                if (ativosFiltro.LastIndexOf(',') > 0 && ativosFiltro.LastIndexOf(',') == (ativosFiltro.Length - 1))
                {
                    ativosFiltro = ativosFiltro.Remove(ativosFiltro.LastIndexOf(','));
                }

                var spAtivos = ativosFiltro.Split(',');
                ativosFiltro = string.Join("','", spAtivos);
                ativosFiltro = "'" + ativosFiltro + "'";
            }

            return _solicitacaoVooService.PesquisaAtivosDisponiveis(detalhesPaginacaoWeb, ativosFiltro, enumTipo, patio, linha, os);
        }
      
        /// <summary>
        /// Enviar ativos para aprovação
        /// </summary>
        public ActionResult EnviarParaAprovacao(int tipoAtivo, string justificativa, string motivo, string patio, string solicitante, string patioSolicitante, DateTime dataHoraChegada, List<SolicitacaoVooAtivoItem> ativos)
        {
            if (justificativa.Length > 500)
            {
                return Json(new
                {
                    Message = "Campo Justificativa não pode ter mais que 500 caracteres.",
                    Result = false
                });
            }
            if (!CamposEstaoValidos(tipoAtivo, justificativa, motivo, patio, solicitante, patioSolicitante, dataHoraChegada, ativos))
            {
                return Json(new
                {
                    Message = "Preencha os campos obrigatórios em destaque.",
                    Result = false
                });
            }
            else
            {
                if (tipoAtivo != 4)
                {
                    // Validar o pátio para quando não for refaturamento.
                    var estacaoMae = _estacaoMaeService.ObterPorCodigo(patio.ToUpper());
                    if (estacaoMae == null)
                    {
                        return Json(new
                        {
                            Message = "Verifique o Nome do Pátio de origem informado.",
                            Result = false
                        });
                    }
                }

                try
                {
                    _solicitacaoVooService.EnviarAprovacao(tipoAtivo, justificativa, motivo, patio, solicitante, patioSolicitante, dataHoraChegada, ativos, UsuarioAtual.Codigo);

                    return Json(new
                    {
                        Message = "Solicitação de aprovação de voo enviada com sucesso, clique em 'Avaliar voo de ativos' para acompanhamento.",
                        Result = true
                    });
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        Message = "Erro ao enviar a aprovação. Erro: " + ex.Message,
                        Result = false
                    });
                }
            }
        }

        /// <summary>
        /// Validação de campos obrigatórios para solicitação
        /// </summary>
        /// <returns></returns>
        private bool CamposEstaoValidos(int tipoAtivo, string justificativa, string motivo, string patio, string solicitante, string patioSolicitante, DateTime dataHoraChegada, List<SolicitacaoVooAtivoItem> ativos) 
        {
            
            if (justificativa == string.Empty
                || motivo == string.Empty                
                || solicitante == string.Empty
                || patioSolicitante == string.Empty
                || dataHoraChegada == new DateTime()
                || ativos == null
                || (tipoAtivo != 4 && patio == ""))
                return false;
            else
                return true;
        }

        /// <summary>
        /// Obter as linhas do pátio
        /// </summary>
        /// <param name="patio"></param>
        /// <returns></returns>
        public JsonResult ObterLinhas(string patio)
        {
            var resultado =_elementoViaService.ObterPorCodigoEstacaoMae(patio.ToUpper());
            resultado.Add(new ElementoViaDto() { Descricao = "Todas", Id = 0 });

            var jsonData = Json(
                 new
                 {
                     Items = resultado.Select(i => new
                     {
                         i.Id,
                         i.Descricao
                     }),
                     Total = resultado.Count
                 });
            return jsonData;
        }

        #endregion

        #endregion

        #region Avaliação

        #region Views
        public ActionResult BuscarAtivosAvaliar()
        {
            return View();
        }

        public ActionResult Avaliar(int indice)
        {
            var itemAtivoAvaliar = _solicitacaoVooService.BuscaDadosAtivosVooAvaliarPorID(indice);
            
            ViewData["id"] = indice;
            ViewData["Motivo"] = itemAtivoAvaliar.Motivo;
            ViewData["Justificativa"] = itemAtivoAvaliar.Justificativa;// !string.IsNullOrEmpty(itemAtivoAvaliar.Justificativa) ? StringUtil.RemoveAspas(itemAtivoAvaliar.Justificativa.Replace("\n1.\t", ".").Replace("\n2.\t", ".").Replace("\n3.\t", ".").Replace("\n", "").Replace(@"\x", "")) : itemAtivoAvaliar.Justificativa;
            ViewData["DataChegada"] = itemAtivoAvaliar.dtHrChegada.ToString("yyyy-MM-dd");
            ViewData["HraChegada"] = itemAtivoAvaliar.dtHrChegada.ToString("HH:mm");
            ViewData["Solicitante"] = itemAtivoAvaliar.Solicitante;
            ViewData["LoginSolicitante"] = UsuarioAtual.Codigo;
            ViewData["PatioSolicitante"] = itemAtivoAvaliar.PatioSolicitante;
            ViewData["AreaResponsavel"] = (string.IsNullOrEmpty(itemAtivoAvaliar.AreaResponsavel) ? string.Empty : itemAtivoAvaliar.AreaResponsavel.ToString().ToUpper());
            //ViewData["JustificativaCCO"] = itemAtivoAvaliar.JustificativaCCO;//!string.IsNullOrEmpty(itemAtivoAvaliar.JustificativaCCO) ? StringUtil.RemoveAspas(itemAtivoAvaliar.JustificativaCCO.Replace("\n1.\t", ".").Replace("\n2.\t", ".").Replace("\n3.\t", ".").Replace("\n", "").Replace(@"\x","")) : itemAtivoAvaliar.JustificativaCCO;    
            //ViewData["ObservacaoCCO"] = itemAtivoAvaliar.ObservacaoCCO;
            ViewData["JustificativaCCO"] = (string.IsNullOrEmpty(itemAtivoAvaliar.JustificativaCCO) ? string.Empty : itemAtivoAvaliar.JustificativaCCO);
            ViewData["ObservacaoCCO"] = (string.IsNullOrEmpty(itemAtivoAvaliar.ObservacaoCCO) ? string.Empty : itemAtivoAvaliar.ObservacaoCCO);
            ViewData["PatioDestino"] = itemAtivoAvaliar.PatioDestino;
            ViewData["tipoAtivo"] = itemAtivoAvaliar.tipoAtivo;
            var descricaoTipoAtivo = "";
            switch ((EnumTipoAtivo)itemAtivoAvaliar.tipoAtivo)
            {
                case EnumTipoAtivo.Eot:
                    {
                        descricaoTipoAtivo = "EOT";
                        break;
                    }
                case EnumTipoAtivo.Locomotiva:
                    {
                        descricaoTipoAtivo = "Locomotiva";
                        break;
                    }
                case EnumTipoAtivo.Vagao:
                    {
                        descricaoTipoAtivo = "Vagão";
                        break;
                    }
                case EnumTipoAtivo.VagaoRefaturamento:
                    {
                        descricaoTipoAtivo = "Vagão refaturamento";
                        break;
                    }
            }
            ViewData["descricaoTipoAtivo"] = descricaoTipoAtivo;
            ViewData["Status"] = itemAtivoAvaliar.Status;            

            return View();
        }
        #endregion

        #region Métodos

        /// <summary>
        /// Consultar Voo Ativos
        /// </summary>
        /// <returns>Voo Ativos</returns>
        public JsonResult ConsultaVooAtivos(DetalhesPaginacaoWeb detalhesPaginacaoWeb, int? Id, DateTime? dtPeriodoInicio, DateTime? dtPeriodoFinal, string solicitante, string status, string tipoSolicitacao)
        {

            var resultado = _solicitacaoVooService.BuscarAtivosVooAvaliar(detalhesPaginacaoWeb, Id, dtPeriodoInicio, dtPeriodoFinal, solicitante, Convert.ToInt16(status), Convert.ToInt16(tipoSolicitacao));

            var jsonData = Json(
                new
                {
                    Items = resultado.Items.Select(i => new
                    {
                        Id = i.Id,
                        DtSolicitacao = i.dtSolicitacao.ToString("dd/MM/yyyy HH:mm:ss"),
                        TipoAtivo = i.TipoAtivo,
                        Solicitante = i.Solicitante,
                        AreaSolicitante = i.AreaSolicitante,
                        Motivo = i.Motivo,
                        Status = i.Status
                    }),
                    Total = resultado.Total
                });
            return jsonData;
        }
        
        /// <summary>
        /// Consulta Voo Ativos Items
        /// </summary>
        /// <param name="Id">id</param>
        /// <returns></returns>
        public JsonResult ConsultaVooAtivosItems(DetalhesPaginacaoWeb detalhesPaginacaoWeb, int Id, int tipoAtivo)
        {
            var resultado = _solicitacaoVooService.BuscarItemsAtivosVooAvaliarPorID(detalhesPaginacaoWeb, Id, (EnumTipoAtivo)tipoAtivo);

            var jsonData = Json(
                new
                {
                    Items = resultado.Items.Select(i => new
                    {
                      Id = i.IdAtivo,
                      StatusItem = i.StatusItem,
                      Ativo = i.Ativo,
                      TipoAtivo = i.TipoAtivo,
                      dtEvento = i.DataEvento.ToString("dd/MM/yyyy HH:mm:ss"),
                      Origem =  i.PatioOrigem,
                      Destino =  i.PatioDestino,
                      CondUso =  i.CondicaoUso,
                      Situacao =  i.Situacao,
                      Local =  i.Local,
                      Responsavel =  i.Responsavel,
                      Lotacao =  i.Lotacao,
                      Intercambio =  i.Intercambio,
                      EstadoFuturo =  i.EstadoFuturo,
                      OlvTimestamp = i.OlvTimestamp.ToString("dd/MM/yyyy HH:mm:ss"),
                      dtDespacho = i.dtDespacho.ToString("dd/MM/yyyy HH:mm:ss"),
                      Linha = i.Linha,
                      Serie = i.Serie,
                      Sequencia = i.Sequencia,
                      Bitola = i.Bitola
                    }),
                    Total = resultado.Total
                });
            return jsonData;
        }
        
        /// <summary>
        /// Realizar aprovação da solicitação
        /// </summary>
        /// <param name="id"></param>
        /// <param name="motivo"></param>
        /// <param name="justificativa"></param>
        /// <param name="dataHoraChegada"></param>
        /// <param name="solicitante"></param>
        /// <param name="areaResponsavel"></param>
        /// <param name="justificativaCCO"></param>
        /// <param name="observacaoCCO"></param>
        /// <param name="items"></param>
        /// <param name="patioDestino"></param>
        /// <returns></returns>
        public ActionResult Aprovar(int id, string motivo, string justificativa, DateTime dataHoraChegada, string solicitante, string areaResponsavel, string justificativaCCO, string observacaoCCO, List<int> items, string patioDestino)
        {
            string mensagem = string.Empty;

            if (justificativaCCO.Length > 500)
            {
                return Json(new
                {
                    Message = "Campo Justificativa CCO não pode ter mais que 500 caracteres.",
                    Result = false
                });
            }

            if (observacaoCCO.Length > 255)
            {
                return Json(new
                {
                    Message = "Campo Observação CCO não pode ter mais que 255 caracteres.",
                    Result = false
                });
            }
            ////Se ocorreu tudo certo execução PKG retorna mensagem NULA
            // Atualizar aprovação
            mensagem = _solicitacaoVooService.Aprovar(id, motivo, justificativa, dataHoraChegada, solicitante, areaResponsavel, justificativaCCO, observacaoCCO, items, UsuarioAtual.Codigo, patioDestino);
            if (mensagem == "<XML><SUCESSO>OK</SUCESSO></XML>" || string.IsNullOrEmpty(mensagem))
            {
                return Json(new
                {
                    Message = "O Voo de Ativos foi aprovado e realizado para os Ativos Selecionados.",
                    Result = true
                });
            }
            else
            {
                return Json(new
                {
                    Message = mensagem,
                    Result = false
                });
            }
        }

        /// <summary>
        /// Reprovar ativos selecionados
        /// </summary>
        public ActionResult Reprovar(int id, string motivo, string justificativa, DateTime dataHoraChegada, string solicitante, string areaResponsavel, string justificativaCCO, string observacaoCCO, List<int> items, string patioDestino)
        {
            try
            {
                if (justificativaCCO.Length > 500)
                {
                    return Json(new
                    {
                        Message = "Campo Justificativa CCO não pode ter mais que 500 caracteres.",
                        Result = false
                    });
                }

                if (observacaoCCO.Length > 255)
                {
                    return Json(new
                    {
                        Message = "Campo Observação CCO não pode ter mais que 255 caracteres.",
                        Result = false
                    });
                }

                // Atualizar aprovação
                _solicitacaoVooService.Reprovar(id, motivo, justificativa, dataHoraChegada, solicitante, areaResponsavel, justificativaCCO, observacaoCCO, items, UsuarioAtual.Codigo, patioDestino);
                return Json(new
                           {
                               Message = "O Voo de Ativos foi reprovado.",
                               Result = true
                           });

            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Message = "Erro ao enviar a aprovação. Erro: " + ex.Message,
                    Result = false
                });
            }
        }

        /// <summary>
        /// Preparar para refaturamento
        /// </summary>
        /// <param name="id"></param>
        /// <param name="motivo"></param>
        /// <param name="justificativa"></param>
        /// <param name="dataHoraChegada"></param>
        /// <param name="solicitante"></param>
        /// <param name="areaResponsavel"></param>
        /// <param name="justificativaCCO"></param>
        /// <param name="observacaoCCO"></param>
        /// <param name="items"></param>
        /// <param name="patioDestino"></param>
        /// <returns></returns>
        public ActionResult Preparar(int id, string motivo, string justificativa, DateTime dataHoraChegada, string solicitante, string areaResponsavel, string justificativaCCO, string observacaoCCO, List<int> items, string patioDestino)
        {
            string mensagem = string.Empty;

            ////Se ocorreu tudo certo execução PKG retorna mensagem NULA
            // Atualizar aprovação
            mensagem = _solicitacaoVooService.Preparar(id, motivo, justificativa, dataHoraChegada, solicitante, areaResponsavel, justificativaCCO, observacaoCCO, items, UsuarioAtual.Codigo, patioDestino);
            if (mensagem == "<XML><SUCESSO>OK</SUCESSO></XML>" || string.IsNullOrEmpty(mensagem))
            {
                return Json(new
                {
                    Message = "O Voo de Ativos realizado e aguardando refaturamento para voo de origem.",
                    Result = true
                });
            }
            else
            {
                return Json(new
                {
                    Message = mensagem,
                    Result = false
                });
            }
        }
        #endregion

        #endregion

        #region Visualização

        #region Views

        public ActionResult BuscarAtivosVisualizar()
        {
            return View();
        }

        public ActionResult Visualizar(int indice)
        {
            var itemAtivoAvaliar = _solicitacaoVooService.BuscaDadosAtivosVooAvaliarPorID(indice);

            ViewData["id"] = indice;
            ViewData["Motivo"] = itemAtivoAvaliar.Motivo;
            ViewData["Justificativa"] = itemAtivoAvaliar.Justificativa; //!string.IsNullOrEmpty(itemAtivoAvaliar.Justificativa) ? StringUtil.RemoveAspas(itemAtivoAvaliar.Justificativa.Replace("\n1.\t", ".").Replace("\n2.\t", ".").Replace("\n3.\t", ".").Replace("\n", "").Replace(@"\x", "")) : itemAtivoAvaliar.Justificativa;
            ViewData["DataChegada"] = itemAtivoAvaliar.dtHrChegada.ToString("yyyy-MM-dd");
            ViewData["HraChegada"] = itemAtivoAvaliar.dtHrChegada.ToString("HH:mm");
            ViewData["Solicitante"] = itemAtivoAvaliar.Solicitante;
            ViewData["LoginSolicitante"] = UsuarioAtual.Codigo;
            ViewData["PatioSolicitante"] = itemAtivoAvaliar.PatioSolicitante;
            ViewData["AreaResponsavel"] = (string.IsNullOrEmpty(itemAtivoAvaliar.AreaResponsavel) ? string.Empty : itemAtivoAvaliar.AreaResponsavel.ToString().ToUpper());
            ViewData["JustificativaCCO"] = itemAtivoAvaliar.JustificativaCCO;// !string.IsNullOrEmpty(itemAtivoAvaliar.JustificativaCCO) ? StringUtil.RemoveAspas(itemAtivoAvaliar.JustificativaCCO.Replace("\n1.\t", ".").Replace("\n2.\t", ".").Replace("\n3.\t", ".").Replace("\n", "").Replace(@"\x", "")) : itemAtivoAvaliar.JustificativaCCO;
            ViewData["ObservacaoCCO"] = itemAtivoAvaliar.ObservacaoCCO;
            ViewData["tipoAtivo"] = itemAtivoAvaliar.tipoAtivo;
            ViewData["PatioDestino"] = itemAtivoAvaliar.PatioDestino;
            var descricaoTipoAtivo = "";
            switch ((EnumTipoAtivo)itemAtivoAvaliar.tipoAtivo)
            {
                case EnumTipoAtivo.Eot:
                    {
                        descricaoTipoAtivo = "EOT";
                        break;
                    }
                case EnumTipoAtivo.Locomotiva:
                    {
                        descricaoTipoAtivo = "Locomotiva";
                        break;
                    }
                case EnumTipoAtivo.Vagao:
                    {
                        descricaoTipoAtivo = "Vagão";
                        break;
                    }
                case EnumTipoAtivo.VagaoRefaturamento:
                    {
                        descricaoTipoAtivo = "Vagão refaturamento";
                        break;
                    }
            }
            ViewData["descricaoTipoAtivo"] = descricaoTipoAtivo;

            return View();

        }

        #endregion


        #endregion
        
        #region Métodos Comuns

        /// <summary>
        /// Buscar motivos usados em combobox
        /// </summary>
        public JsonResult BuscarMotivos(int tipoAtivo)
        {
            var enumTipoAtivo = (EnumTipoAtivo)tipoAtivo;
            var resultado = _motivoVooService.BuscarMotivos(enumTipoAtivo);

            var jsonData = Json(
               new
               {
                   Items = resultado.Select(i => new
                   {
                       i.Id,
                       i.Descricao
                   }),
                   Total = resultado.Count
               });
            return jsonData;            
        }

        /// <summary>
        /// Buscar status de solicitações para combobox
        /// </summary>
        public void BuscarStatusSolicitacao() {

            throw new NotImplementedException();        
        }

        /// <summary>
        /// Buscar o registro de uma solicitação e seus itens
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public object BuscarSolicitacao(int id) { return new object(); }

        /// <summary>
        /// Validar se patio destino é válido.
        /// </summary>
        /// <param name="patio">Abreviatura do pátin</param>
        /// <returns></returns>
        public JsonResult ValidarPatio(string patio)
        {
            var resultado = _estacaoMaeService.ObterPorCodigo(patio.ToUpper());

            if (resultado != null)
            {
                return Json(new
                {
                    Message = "",
                    Result = true
                });
            }
            
            return Json(new
            {
                Message = "Verifique o Nome do # informado.",
                Result = false
            });
        }

        #endregion 
    }
}
