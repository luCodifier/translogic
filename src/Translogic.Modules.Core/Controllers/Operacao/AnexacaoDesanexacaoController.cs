﻿using System.Linq;
using System.Web.Mvc;
using Newtonsoft.Json;
using Translogic.Core.Infrastructure.Web;
using Translogic.Modules.Core.Domain.Services.AnexacaoDesanexacao.Interface;
using System;
using Translogic.Modules.Core.Domain.Model.AnexacaoDesanexacao.Repositories;
using Translogic.Modules.Core.Domain.Model.Dto.AnexacaoDesanexacao;
using Translogic.Modules.Core.Domain.Model.AnexacaoDesanexacao;
using Translogic.Modules.TPCCO.Helpers;
using System.Collections.Generic;
using Translogic.Modules.Core.Infrastructure;

namespace Translogic.Modules.Core.Controllers.Consultas
{
    public class AnexacaoDesanexacaoController : BaseSecureModuleController
    {

        #region Serviços
        private readonly IAnexacaoDesanexacaoService _anexacaoDesanexacaoService;
        private readonly ILogAnxHistTremService _logAnxHistTremService;
        private readonly ILogAnxHistVeiculoService _logAnxHistVeiculoService;

        private readonly IAnexacaoDesanexacaoRepository _anexacaoDesanexacaoRepository;
        private readonly ILogAnxHistTremRepository _logAnxHistTremRepository;
        private readonly ILogAnxHistVeiculoRepository _logAnxHistVeiculoRepository;
        
        #endregion

        #region Construtores
        public AnexacaoDesanexacaoController(IAnexacaoDesanexacaoService anexacaoDesanexacaoService,
                                             ILogAnxHistTremService logAnxHistTremService,
                                             ILogAnxHistVeiculoService logAnxHistVeiculoService,
                                             IAnexacaoDesanexacaoRepository anexacaoDesanexacaoRepository,
                                             ILogAnxHistTremRepository logAnxHistTremRepository,
                                             ILogAnxHistVeiculoRepository logAnxHistVeiculoRepository)
        {
            _anexacaoDesanexacaoService = anexacaoDesanexacaoService;
            _logAnxHistTremService = logAnxHistTremService;
            _logAnxHistVeiculoService = logAnxHistVeiculoService;

            _anexacaoDesanexacaoRepository = anexacaoDesanexacaoRepository;
            _logAnxHistTremRepository = logAnxHistTremRepository;
            _logAnxHistVeiculoRepository = logAnxHistVeiculoRepository;
            
        }
        #endregion

        #region ActionViews
        [Autorizar(Transacao = "ANEXACAODESANEXACAOAUTO")]
        public ActionResult Index()
        {
            //ViewData["Permissoes"] = _baldeioService.BuscarPermissaoUsuario(base.UsuarioAtual);
            return View();
        }

        /// <summary>
        /// Modal Veiculos Trem
        /// </summary>
        /// <returns>Modal Veiculos Trem</returns>
        public ActionResult modVeiculosTrem(decimal os, string trem, string localParada, DateTime dtProximaExecucao, string acao)
        {
            ViewData["OS"] = os;
            ViewData["Trem"] = trem;
            ViewData["LocalParada"] = localParada;
            ViewData["DataProximaExecucao"] = dtProximaExecucao.ToString("dd/MM/yyyy");
            ViewData["HoraProximaExecucao"] = dtProximaExecucao.ToString("HH:mm");
            ViewData["Acao"] = acao;
            return View();
        }

        /// <summary>
        /// Modal Editar
        /// </summary>
        /// <returns>Modal Edit</returns>
        public ActionResult modEdit(decimal os, string vagaoLocomotiva, string cad, string pedido, string localizacao, string situacao, string lotacao, string recomendacao)
        {
            ViewData["Os"] = os;
            ViewData["VagaoLocomotiva"] = vagaoLocomotiva;
            ViewData["Cad"] = cad;
            ViewData["Pedido"] = pedido;
            ViewData["Localizacao"] = localizacao;
            ViewData["Situacao"] = situacao;
            ViewData["Lotacao"] = lotacao;
            ViewData["Recomendacao"] = recomendacao;
            return View();
        }
        
        #endregion

        //TREM
        #region ACTIONS

        /// <summary>
        /// Pesquisa para exportação baldeio para Excel
        /// </summary>
        /// <param name="baldeioDto"></param>
        /// <returns></returns>
        //[Autorizar(Transacao = "CONSULTABALDEIO", Acao = "Pesquisar")]
        public ActionResult ExportarXlsAnexacaoDesanexacao(string anexacaoDesanexacaoFiltroDto)
        {
            var request = JsonConvert.DeserializeObject<AnexacaoDesanexacaoRequestDto>(anexacaoDesanexacaoFiltroDto);
            //ValidaCampos(request);

            var colunas = new List<ExcelColuna<AnexacaoDesanexacaoDto>>
            {
                new ExcelColuna<AnexacaoDesanexacaoDto>("Operação", c => !string.IsNullOrEmpty(c.Operacao) ? c.Operacao : string.Empty),
                new ExcelColuna<AnexacaoDesanexacaoDto>("OS", c => c.OS),
                new ExcelColuna<AnexacaoDesanexacaoDto>("Trem", c => !string.IsNullOrEmpty(c.Trem) ? c.Trem : string.Empty),
                new ExcelColuna<AnexacaoDesanexacaoDto>("Origem", c => !string.IsNullOrEmpty(c.Origem) ? c.Origem : string.Empty),
                new ExcelColuna<AnexacaoDesanexacaoDto>("Destino", c => !string.IsNullOrEmpty(c.Destino) ? c.Destino : string.Empty),
                new ExcelColuna<AnexacaoDesanexacaoDto>("Local Parada", c => !string.IsNullOrEmpty(c.LocalParada) ? c.LocalParada : string.Empty),
                new ExcelColuna<AnexacaoDesanexacaoDto>("Status Processamento", c => !string.IsNullOrEmpty(c.StatusProcessamento) ? c.StatusProcessamento : string.Empty),
                new ExcelColuna<AnexacaoDesanexacaoDto>("Status Parada", c => !string.IsNullOrEmpty(c.StatusParada) ? c.StatusParada : string.Empty),
                new ExcelColuna<AnexacaoDesanexacaoDto>("Tentativas", c => c.Tentativas),
                new ExcelColunaData<AnexacaoDesanexacaoDto>("Data Proxíma Execução", c => c.dtProximaExecucao.ToString("dd/MM/yyyy HH:mm:ss")),
                new ExcelColuna<AnexacaoDesanexacaoDto>("Obs", c => !string.IsNullOrEmpty(c.Obs) ? c.Obs.Replace("<XML><ERRO>","").Replace("</ERRO></XML>","") : string.Empty)
            };

            var resultado = _anexacaoDesanexacaoService.ObterConsultaAnexacaoDesanexacaoExportar(request);

            return ExcelHelper.Exportar(
                "AnexacaoDesanexacao.xlsx",
                colunas,
                resultado,
                ws =>
                {
                    ws.Cells.AutoFitColumns();
                });

            return null;
        }

        #endregion

        #region JSONS

        /// <summary>
        /// Consulta tela Anexação e Desanexação
        /// </summary>
        /// <param name="detalhesPaginacaoWeb"></param>
        /// <param name="anexacaoDesanexacaoFiltroDto"> dto filtros anexacao e desanexacao</param>
        /// <returns>Retorna lista consulta tela de consulta baleio</returns>
        //[Autorizar(Transacao = "CONSULTABALDEIO", Acao = "Pesquisar")]
        public JsonResult ObterConsultaAnexacaoDesanexacao(DetalhesPaginacaoWeb detalhesPaginacaoWeb, string anexacaoDesanexacaoFiltroDto)
        {
            var request = JsonConvert.DeserializeObject<AnexacaoDesanexacaoRequestDto>(anexacaoDesanexacaoFiltroDto);

            var resultado = _anexacaoDesanexacaoService.ObterConsultaAnexacaoDesanexacao(detalhesPaginacaoWeb, request);

            var jsonData = Json(
                new
                {
                    Items = resultado.Items.Select(i => new
                    {
                        //Campos
                        Operacao = i.Operacao,
                        OS = i.OS,
                        Trem = i.Trem,
                        Origem = i.Origem,
                        Destino = i.Destino,
                        LocalParada = i.LocalParada,
                        StatusProcessamento = i.StatusProcessamento,
                        StatusParada = i.StatusParada,
                        Tentativas = i.Tentativas,
                        dtProximaExecucao = i.dtProximaExecucao.ToString("dd/MM/yyyy HH:mm:ss"),
                        Obs = !string.IsNullOrEmpty(i.Obs) ? i.Obs.Replace("<XML><ERRO>", "").Replace("</ERRO></XML>", "") : string.Empty  
                    }),
                    Total = resultado.Total
                });
            return jsonData;
        }

        public JsonResult SalvarAlteracaoTrem(decimal os, string dataHora)
        {
            try
            {
                var objLogAnxHistTrem = new LogAnxHistTrem()
                {
                    Id = 0,
                    OS = os,
                    DataAcao = Convert.ToDateTime(dataHora),
                    Acao = "Editado",
                    Timestamp = DateTime.Now
                };
                _logAnxHistTremRepository.Inserir(objLogAnxHistTrem);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Message = "Erro ao salvar edição Trem. Erro: " + ex.Message,
                    Result = false
                });
            }

            return Json(new
            {
                Message = "Edição Trem salva com Sucesso.",
                Result = true
            });
        }

        public JsonResult SalvarExclusaoTrem(decimal os)
        {
            try
            {
                var objLogAnxHistTrem = new LogAnxHistTrem()
                {
                    Id = 0,
                    OS = os,
                    DataAcao = DateTime.Now,
                    Acao = "Exclusao",
                    Timestamp = DateTime.Now
                };

                _logAnxHistTremRepository.Inserir(objLogAnxHistTrem);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Message = "Erro ao salvar exclusão Trem. Erro: " + ex.Message,
                    Result = false
                });
            }

            return Json(new
            {
                Message = "Exclusão Trem salva com Sucesso.",
                Result = true
            });
        }

        /// <summary>
        /// Evento PKG Trava Trem
        /// </summary>
        /// <param name="encerrarAPTDto"> dto com OrdemServiço e Usuario</param>
        /// <returns>Retorna Operação ok ou o erro gerado ao executar pkg de trava</returns>
        public JsonResult ZerarTentativa(string operacao, decimal os)
        { 
            try
            {
                _anexacaoDesanexacaoService.ZeraTentativas(operacao, os);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Message = "Erro",
                    Result = false
                });
            }

            return Json(new
            {
                Message = "Tentativas zeradas com sucesso!",
                Result = true
            });
        }
        
        /// <summary>
        /// Evento PKG Trava Trem
        /// </summary>
        /// <param name="encerrarAPTDto"> dto com OrdemServiço e Usuario</param>
        /// <returns>Retorna Operação ok ou o erro gerado ao executar pkg de trava</returns>
        public JsonResult EncerrarAPT(string operacao, decimal os, string localParada) 
        {
            ResultTravaTremDto resultTravaTremDto;

            try
            {
                resultTravaTremDto = _anexacaoDesanexacaoService.EnviaSOAP(operacao, os, UsuarioAtual.ToString(), localParada);               
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Message = ConvertErroTravaTrem(ex.Message),
                    MsgEncerrarParadaPKG = "",
                    Result = false
                });
            }

            if (resultTravaTremDto.conformidade == "true" && resultTravaTremDto.retornoPKGEncerrarParada == null)
            {
                return Json(new
                {
                    Message = "Trava do Trem efetuada com Sucesso.",
                    Result = true
                });
            }
            
            return Json(new
            {
                Message = "Erro ao efetuar Trava Trem.",
                MsgEncerrarParadaPKG = resultTravaTremDto.retornoPKGEncerrarParada,
                Result = false
            });
        }

        #endregion

        //VEICULO
        #region ACTIONS
        public JsonResult ObterConsultaVeiculosTrem(DetalhesPaginacaoWeb detalhesPaginacaoWeb, decimal os, string estacao, string acao)
            {
                var resultado = _anexacaoDesanexacaoService.ObterConsultaVeiculosTrem(detalhesPaginacaoWeb, os, estacao, acao);

                var jsonData = Json(
                    new
                    {
                        Items = resultado.Items.Select(i => new
                        {
                            //Campos
                            VagaoLocomotiva = i.VagaoLocomotiva,
                            StatusProcessamento = i.StatusProcessamento,
                            Cad = i.Cad,
                            Pedido = i.Pedido,
                            Localizacao = i.Localizacao,
                            Situacao = i.Situacao,
                            Lotacao = i.Lotacao,
                            Recomendacao = i.Recomendacao,
                            LocalParada = i.LocalParada,
                            DescErro = i.DescricaoErro
                        }),
                        Total = resultado.Total
                    });
                return jsonData;
            }        
            #endregion
        
        #region JSONS
            public JsonResult SalvarAlteracaoVeiculo(decimal os, string numeroVeiculo, string numeroVeiculoOLD, string localizacao)
            {
                try
                {
                    var obj = _anexacaoDesanexacaoService.ObterPorOS(os, numeroVeiculoOLD);

                    var objLogAnxHistVeiculo = new LogAnxHistVeiculo()
                    {
                        Id = 0,
                        OS = os,
                        DataAcao = DateTime.Now,
                        NumeroVeiculo = numeroVeiculo,
                        NumeroVeiculoOLD = numeroVeiculoOLD,
                        Local = localizacao,
                        Acao = "Editado",
                        Processado = obj.StatusProcessamento,
                        Timestamp = DateTime.Now
                    };

                    _logAnxHistVeiculoRepository.Inserir(objLogAnxHistVeiculo);
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        Message = "Erro ao salvar edição Veiculo. Erro: " + ex.Message,
                        Result = false
                    });
                }

                return Json(new
                {
                    Message = "Edição Veiculo salva com Sucesso.",
                    Result = true
                });
            }

            public JsonResult SalvarExclusaoVeiculo(decimal os, string numeroVeiculo, string localizacao)
            {
                try
                {
                    var objLogAnxHistVeiculo = new LogAnxHistVeiculo()
                    {
                        Id = 0,
                        OS = os,
                        DataAcao = DateTime.Now,
                        NumeroVeiculoOLD = numeroVeiculo,
                        Local = localizacao,
                        Acao = "Exclusao",
                        Processado = "S",
                        Timestamp = DateTime.Now
                    };


                    _logAnxHistVeiculoRepository.Inserir(objLogAnxHistVeiculo);
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        Message = "Erro ao salvar exclusão Veiculo. Erro: " + ex.Message,
                        Result = false
                    });
                }

                return Json(new
                {
                    Message = "Exclusão Veiculo salva com Sucesso.",
                    Result = true
                });
            }
        #endregion

        //Private
        private string ConvertErroTravaTrem(string msgErro)
        {
            if(msgErro.Equals("O servidor remoto retornou um erro: (500) Erro Interno do Servidor."))
            {
                return "Serviço indisponível, contate o NOC 0 2141-7444";
            }
            return msgErro; 
        }
    }
}
