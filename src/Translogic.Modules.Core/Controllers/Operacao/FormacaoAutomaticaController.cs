﻿using System.Linq;
using System.Web.Mvc;
using Newtonsoft.Json;
using Translogic.Core.Infrastructure.Web;
using System;
using Translogic.Modules.TPCCO.Helpers;
using System.Collections.Generic;
using Translogic.Modules.Core.Infrastructure;
using Translogic.Modules.Core.Domain.Services.FormacaoAutomatica.Interface;
using Translogic.Modules.Core.Domain.Model.FormacaoAutomatica.Repositories;
using Translogic.Modules.Core.Domain.Model.Dto.FormacaoAutomatica;

namespace Translogic.Modules.Core.Controllers.Consultas
{
    public class FormacaoAutomaticaController : BaseSecureModuleController
    {

        #region Serviços
        private readonly ILogFormacaoAutoService _logFormacaoAutoService;
        #endregion

        #region Construtores
        public FormacaoAutomaticaController(ILogFormacaoAutoService logFormacaoAutoService)
        {
            _logFormacaoAutoService = logFormacaoAutoService;
        }
        #endregion

        #region ActionViews
        [Autorizar(Transacao = "FORMACAOAUTOMATICA")]
        public ActionResult Index()
        {
            //ViewData["Permissoes"] = _baldeioService.BuscarPermissaoUsuario(base.UsuarioAtual);
            return View();
        }

        //<summary>
        //Modal Veiculos Trem
        //</summary>
        //<returns>Modal Veiculos Trem</returns>
        public ActionResult modVeiculosTrem(decimal os, string trem, string localParada, DateTime dtProximaExecucao, decimal IdMensagem)
        {
            ViewData["OS"] = os;
            ViewData["Trem"] = trem;
            ViewData["LocalParada"] = localParada;
            ViewData["DataProximaExecucao"] = dtProximaExecucao.ToString("dd/MM/yyyy");
            ViewData["HoraProximaExecucao"] = dtProximaExecucao.ToString("HH:mm");
            ViewData["IdMensagem"] = IdMensagem;
            return View();
        }

        #endregion

        //TREM
        #region ACTIONS

        /// <summary>
        /// Pesquisa para exportação formação automatica para Excel
        /// </summary>
        /// <param name="formacaoAutoFiltroDto"></param>
        /// <returns></returns>
        [Autorizar(Transacao = "FORMACAOAUTOMATICA", Acao = "Exportar")]
        public ActionResult ExportarXlsFormacaoAutomatica(string formacaoAutoFiltroDto)
        {
            var request = JsonConvert.DeserializeObject<FormacaoAutoRequestDto>(formacaoAutoFiltroDto);
            //ValidaCampos(request);

            var colunas = new List<ExcelColuna<FormacaoAutoDto>>
            {
                new ExcelColuna<FormacaoAutoDto>("Operação", c => !string.IsNullOrEmpty(c.Operacao) ? c.Operacao : string.Empty),
                new ExcelColuna<FormacaoAutoDto>("OS", c => c.OS),
                new ExcelColuna<FormacaoAutoDto>("Trem", c => !string.IsNullOrEmpty(c.Trem) ? c.Trem : string.Empty),
                new ExcelColuna<FormacaoAutoDto>("Origem", c => !string.IsNullOrEmpty(c.LfaOrigem) ? c.LfaOrigem : string.Empty),
                new ExcelColuna<FormacaoAutoDto>("Destino", c => !string.IsNullOrEmpty(c.LfaDestino) ? c.LfaDestino : string.Empty),
                new ExcelColuna<FormacaoAutoDto>("Status Processamento", c => !string.IsNullOrEmpty(c.StatusProcessamento) ? c.StatusProcessamento : string.Empty),
                new ExcelColuna<FormacaoAutoDto>("Status Liberação", c => !string.IsNullOrEmpty(c.StatusLiberacao) ? c.StatusLiberacao : string.Empty),
                new ExcelColuna<FormacaoAutoDto>("Tentativas", c => c.LfaTentativas),
                new ExcelColunaData<FormacaoAutoDto>("Data Proxíma Execução", c => c.DtProximaExecucao.ToString("dd/MM/yyyy HH:mm:ss")),
                new ExcelColuna<FormacaoAutoDto>("Obs", c => !string.IsNullOrEmpty(c.Obs) ? c.Obs.Replace("<XML><ERRO>","").Replace("</ERRO></XML>","") : string.Empty)
            };

            var resultado = _logFormacaoAutoService.ObterConsultaFormacaoAutomaticaExportar(request);

            return ExcelHelper.Exportar(
                "FormacaoAutomatica.xlsx",
                colunas,
                resultado,
                ws =>
                {
                    ws.Cells.AutoFitColumns();
                });

            return null;
        }

        #endregion

        #region JSONS

        /// <summary>
        /// Consulta tela Formação Automatica
        /// </summary>
        /// <param name="detalhesPaginacaoWeb"></param>
        /// <param name="formacaoAutoFiltroDto"> dto filtros Formação Automatica</param>
        /// <returns>Retorna lista consulta tela de Formação Automatica</returns>
        [Autorizar(Transacao = "FORMACAOAUTOMATICA", Acao = "Pesquisar")]
        public JsonResult ObterConsultaFormacaoAutomatica(DetalhesPaginacaoWeb detalhesPaginacaoWeb, string formacaoAutoFiltroDto)
        {
            var request = JsonConvert.DeserializeObject<FormacaoAutoRequestDto>(formacaoAutoFiltroDto);

            var resultado = _logFormacaoAutoService.ObterConsultaFormacaoAutomatica(detalhesPaginacaoWeb, request);

            var jsonData = Json(
                new
                {
                    Items = resultado.Items.Select(i => new
                    {
                        //Campos
                        LfaId = i.LfaId,
                        Operacao = i.Operacao,
                        OS = i.OS,
                        Trem = i.Trem,
                        LfaOrigem = i.LfaOrigem,
                        LfaDestino = i.LfaDestino,
                        StatusProcessamento = i.StatusProcessamento,
                        StatusLiberacao = i.StatusLiberacao,
                        LfaTentativas = i.LfaTentativas,
                        DtProximaExecucao = i.DtProximaExecucao.ToString("dd/MM/yyyy HH:mm:ss"),
                        Obs = i.Obs,
                        LIdMensagem = i.LIdMensagem
                    }),
                    Total = resultado.Total
                });
            return jsonData;
        }

        /// <summary>
        /// Zerar tentativas
        /// </summary>
        /// <returns>Zera tentativas formação automatica</returns>
        public JsonResult ZerarTentativa(decimal IdMensagem)
        {
            try
            {
                _logFormacaoAutoService.ZeraTentativas(IdMensagem);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Message = "Erro",
                    Result = false
                });
            }

            return Json(new
            {
                Message = "Tentativas zeradas com sucesso!",
                Result = true
            });
        }

        #endregion

        //VEICULO
        #region ACTIONS
        public JsonResult ObterConsultaVeiculosTrem(DetalhesPaginacaoWeb detalhesPaginacaoWeb, decimal IdMensagem)
        {
            var resultado = _logFormacaoAutoService.ObterConsultaFormacaoAutomaticaVeiculos(detalhesPaginacaoWeb, IdMensagem);

            var jsonData = Json(
                new
                {
                    Items = resultado.Items.Select(i => new
                    {
                        //Campos
                        VagaoLocomotiva = i.VagaoLocomotiva,
                        StatusProcessamento = i.StatusProcessamento,
                        Cad = i.Cad,
                        Pedido = i.Pedido,
                        Localizacao = i.Localizacao,
                        Situacao = i.Situacao,
                        Lotacao = i.Lotacao,
                        Recomendacao = i.Recomendacao,
                        LocalParada = i.LocalParada,
                        DescErro = i.DescricaoErro
                    }),
                    Total = resultado.Total
                });
            return jsonData;
        }
        #endregion
    }
}
