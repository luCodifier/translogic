﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Translogic.Modules.Core.Domain.Model.Trem.Repositories;
using Newtonsoft.Json;

namespace Translogic.Modules.Core.Controllers.Operacao
{
    public class RedestinaTremController : BaseSecureModuleController
    {
        private readonly ITremRepository _tremRepository;

        public RedestinaTremController(
                                    ITremRepository tremRepository)
        {
            _tremRepository = tremRepository;
        }

        // GET: /RedestinaTrem/
        //[Autorizar(Transacao = " REDESTINATREM")]
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Pesquisa para Liberacao Formacao Trem
        /// </summary>
        /// <param name="detalhesPaginacaoWeb"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public JsonResult ConsultaTrem(int numeroOrdemServico)
        {
            try
            {
                //var parametros = JsonConvert.DeserializeObject<RedestinaTremDto>(liberacaoFormacaoTremDto);

                //if (request.DataInicio == null || request.DataInicio == new DateTime())
                //    throw new Exception("Data inicial é obrigatório");

                //if (request.DataFinal == null || request.DataFinal == new DateTime())
                //    throw new Exception("Data inicial é obrigatório");

                //if (DateTime.Compare(request.DataFinal, request.DataInicio) < 0)
                //    throw new Exception("Data inicial deve ser menor que a data final");

                //if (request.DataFinal.Subtract(request.DataInicio).Days + 1 > 30)
                //    throw new Exception("O período da pesquisa não deve ser superior a 30 dias");
                var resultado = _tremRepository.ObterPorNumeroOrdemServico(numeroOrdemServico);

                // Verifica se a ordem de serviço existe.
                if (resultado == null)
                {
                    return Json(new
                    {
                        Success = false,
                        Message = "O trem não foi encontrado!",
                        Status = 500 // Bad Request
                    });
                }

                var jsonData = Json(
                    new
                    {
                        Items = new
                        {
                            //Campos
                            OrdemServico = resultado.OrdemServico.Numero,
                            PrefixoTrem = resultado.Prefixo,
                            EstacaoOrigem = resultado.Origem.Codigo,
                            EstacaoDestino = resultado.Destino.Codigo,
                            Rota = resultado.Rota.Codigo,
                            LocalAtual = "",
                            SituacaoTrem = resultado.Situacao
                        }
                    });
                return jsonData;
            }
            catch (Exception e)
            {
                return Json(new
                {
                    Success = false,
                    e.Message,
                    Status = 400 // Bad Request
                });
            }
        }

        /// <summary>
        /// Lista de estações para definição do novo destino do trem.
        /// </summary>
        /// <returns></returns>
        public JsonResult ListaEstacaoDestino()
        {
            try
            {
                var jsonData = Json(
                    new
                    {
                        Items = new
                        {
                            //Campos
                            Estacao = new string[] {"PCZ", "PSN"}
                        }
                    });
                return jsonData;
            }
            catch (Exception e)
            {
                return Json(new
                {
                    Success = false,
                    e.Message,
                    Status = 400 // Bad Request
                });
            }
        }

        /// <summary>
        /// Otem informações do trem que será alterado o destino.
        /// </summary>
        /// <returns></returns>
        public JsonResult Redestinar(int numOs, string novoDestino)
        {
            try
            {
                var jsonData = Json(
                    new
                    {
                        Items = new
                        {
                            //Campos
                            Estacao = new string[] { "PCZ", "PSN" }
                        }
                    });
                return jsonData;
            }
            catch (Exception e)
            {
                return Json(new
                {
                    Success = false,
                    e.Message,
                    Status = 400 // Bad Request
                });
            }
        }
    }
}

