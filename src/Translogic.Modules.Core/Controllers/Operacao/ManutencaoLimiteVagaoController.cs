﻿namespace Translogic.Modules.Core.Controllers.Operacao
{
    using System;
    using System.Linq;
    using System.Text;
    using System.Web.Mvc;
    using Translogic.Core.Infrastructure.Web;
    using Translogic.Modules.Core.Domain.Services.Trem;
    using Translogic.Modules.Core.Infrastructure;

    /// <summary>
    /// Classe ManutencaoLimiteVagaoController
    /// </summary>
    public class ManutencaoLimiteVagaoController : BaseSecureModuleController
    {
        private readonly VagaoLimitePesoService _vagaoMangaService;

        /// <summary>
        /// Cosntrutor Padrão
        /// </summary>
        /// <param name="vagaoMangaService">Vagão Manga Service</param>
        public ManutencaoLimiteVagaoController(VagaoLimitePesoService vagaoMangaService)
        {
            _vagaoMangaService = vagaoMangaService;
        }

        /// <summary>
        /// Método Index
        /// </summary>
        /// <returns>Action Result</returns>
        [Autorizar(Transacao = "MLIMITE")]
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Realiza a pesquisa de Vagão ou Frota de acordo com os filtros
        /// </summary>
        /// <param name="detalhePaginacao">Detalhes de paginação das grids</param>
        /// <param name="vagao">Vagão informado na tela</param>
        /// <param name="mercadoria">Mercadoria informada na tela</param>
        /// <param name="frota">Frota informada na tela</param>
        /// <returns>Lista de Items</returns>
        [Autorizar(Transacao = "MLIMITE", Acao = "Pesquisar")]
        public ActionResult Pesquisar(DetalhesPaginacaoWeb detalhePaginacao, string vagao, string mercadoria, string frota)
        {
            if (!String.IsNullOrEmpty(frota))
            {
                var result = _vagaoMangaService.ObterPorFiltroFrota(detalhePaginacao, frota);

                return Json(new
                {
                    result.Total,
                    Items = result.Items.Select(e => new
                    {
                        e.Id,
                        Frota = e.Frota.Codigo,
                        e.PesoMaximo,
                        DataCadastro = e.DataInicial.ToString("dd/MM/yyyy HH:mm:ss"),
                        DataAlteracao = e.DataFinal.HasValue ? e.DataFinal.Value.ToString("dd/MM/yyyy HH:mm:ss") : String.Empty,
                        Usuario = e.UsuarioCadastro.Nome
                    }).OrderBy(e => e.DataCadastro),
                    success = true
                });
            }
            else
            {
                var result = _vagaoMangaService.ObterPorFiltro(detalhePaginacao, vagao, mercadoria);

                return Json(new
                {
                    result.Total,
                    Items = result.Items.Select(e => new
                    {
                        e.Id,
                        Vagao = e.Vagao.Codigo,
                        Limite = e.LimiteMinimoBase.MenorValorRota,
                        Mercadoria = e.LimiteMinimoBase.Mercadoria.Codigo,
                        e.PesoMinimo,
                        DataCadastro = e.DataInicial.ToString("dd/MM/yyyy HH:mm:ss"),
                        DataAlteracao = e.DataFinal.HasValue ? e.DataFinal.Value.ToString("dd/MM/yyyy HH:mm:ss") : string.Empty,
                        Usuario = e.UsuarioCadastro.Nome
                    }).OrderBy(e => e.DataCadastro),
                    success = true
                });
            }

            return null;
        }

        /// <summary>
        /// Salva um limite mínimo para o vagão e mercadoria
        /// </summary>
        /// <param name="limite">limite de peso para esta mercadoria neste vagão</param>
        /// <param name="mercadoria">mercadoria que receberá o limite no vagão</param>
        /// <param name="vagao">vagão que receberá o limite</param>
        /// <param name="pesoMinimo">Peso mímino informado</param>
        /// <returns>Response com o sucesso ou não da operação</returns>
        [Autorizar(Transacao = "MLIMITE", Acao = "Pesquisar")]
        public JsonResult VerificarLimiteMinimo(string limite, string mercadoria, string vagao, string pesoMinimo)
        {
            try
            {
                decimal limiteVerificar = 0;
                decimal pesoMinimoVerificar = 0;

                limite = limite.Replace(".", ",");

                if (decimal.TryParse(limite, out limiteVerificar) && decimal.TryParse(pesoMinimo, out pesoMinimoVerificar))
                {
                    var limiteMinimoBase = _vagaoMangaService.VerificarLimiteMinimoBase(limiteVerificar, mercadoria, vagao);

                    if (limiteMinimoBase == null)
                    {
                        var strMsgm = new StringBuilder("Não existe limite consolidado:<br/>");
                        strMsgm.AppendLine(string.Format("Limite Rota: {0}<br/>", limite));
                        strMsgm.AppendLine(string.Format("Mercadoria: {0}<br/>", mercadoria));
                        strMsgm.AppendLine(string.Format("Vagão: {0}<br/>", vagao));

                        throw new Exception(strMsgm.ToString());
                    }

                    var limiteExcecao = _vagaoMangaService.ObterLimiteExcecaoVagaoPorLimiteMinimoBaseId(limiteMinimoBase.Id);
                    
                    if (limiteExcecao != null)
                    {
                        return Json(new
                        {
                            Success = true,
                            LimiteMinimoBase = new
                                                   {
                                                       limiteMinimoBase.Id,
                                                       limiteMinimoBase.MenorValorRota,
                                                       limiteMinimoBase.Mercadoria
                                                   },
                            LimiteExcecaoVagao = new
                            {
                                limiteExcecao.Id,
                                limiteExcecao.PesoMinimo
                            },
                        });    
                    }

                    return Json(new
                    {
                        Success = true,
                        LimiteMinimoBase = new
                        {
                            limiteMinimoBase.Id,
                            limiteMinimoBase.MenorValorRota,
                            limiteMinimoBase.Mercadoria
                        },
                        LimiteExcecaoVagao = (object)null
                    });
                }

                return Json(new
                {
                    Success = true,
                    LimiteMinimoBase = (object)null,
                    LimiteExcecaoVagao = (object)null
                });
            }
            catch (Exception e)
            {
                return Json(new
                {
                    Success = false,
                    e.Message,
                    Status = 200
                });
            }
        }

        /// <summary>
        /// Salva um limite mínimo para o vagão e mercadoria
        /// </summary>
        /// <param name="idFrota">Id da Frota selecionada a ser verificado o Limite</param>
        /// <returns>Response com o sucesso ou não da operação</returns>
        [Autorizar(Transacao = "MLIMITE", Acao = "Pesquisar")]
        public JsonResult VerificarLimiteMaximo(int idFrota)
        {
            try
            {
                var limiteExcecao = _vagaoMangaService.ObterLimiteExcecaoFrota(idFrota);

                if (limiteExcecao != null)
                {
                    return Json(new
                    {
                        Success = true,
                        LimiteExcecaoFrota = new
                        {
                            limiteExcecao.Id,
                            limiteExcecao.PesoMaximo
                        }
                    });   
                }
                else
                {
                    return Json(new
                    {
                        Success = true,
                        LimiteExcecaoFrota = (object)null
                    });
                }
            }
            catch (Exception e)
            {
                return Json(new
                {
                    Success = false,
                    e.Message,
                    Status = 200
                });
            }
        }

        /// <summary>
        /// Salva um limite mínimo para o vagão e mercadoria
        /// </summary>
        /// <param name="limiteMinimoBaseId">limite de peso para esta mercadoria neste vagão</param>
        /// <param name="vagao">vagão que receberá o limite</param>
        /// <param name="pesoMinimo">o novo limite mínimo para o vagão com esta mercadoria</param>
        /// <returns>Response com o sucesso ou não da operação</returns>
        [Autorizar(Transacao = "MLIMITE", Acao = "Salvar")]
        public JsonResult SalvarLimiteMinimo(int limiteMinimoBaseId, string vagao, string pesoMinimo)
        {
            try
            {
                _vagaoMangaService.SalvarLimiteMinimoVagao(limiteMinimoBaseId, vagao, pesoMinimo, UsuarioAtual);
                
                return Json(new
                {
                    Success = true,
                    CodigoVagao = vagao,
                    Message = "Limite alterado com sucesso!"
                });
            }
            catch (Exception e)
            {
                return Json(new
                {
                    Success = false,
                    e.Message,
                    Status = 200
                });
            }
        }

        /// <summary>
        /// Remover o limite minimo setando a data final
        /// </summary>
        /// <param name="limiteMinimoId">Id do limite selecionado para remover</param>
        /// <returns>Response da operacao</returns>
        [Autorizar(Transacao = "MLIMITE", Acao = "Salvar")]
        public JsonResult RemoverLimiteMinimo(int limiteMinimoId)
        {
            try
            {
                _vagaoMangaService.RemoverLimiteMinimoVagao(limiteMinimoId);
                return Json(new
                {
                   Success = true,
                   Message = "Limite mínimo removido com sucesso!"
                });
            }
            catch (Exception e)
            {
                return Json(new
                {
                   Success = false,
                   e.Message,
                   Status = 200
                });
            }
        }

        /// <summary>
        /// Salva um limite máximo para a frota
        /// </summary>
        /// <param name="frota">Id da Frota selecionada para gravação do limite máximo</param>
        /// <param name="peso">Peso para o limite máximo</param>
        /// <returns>Response com o sucesso ou não da operação</returns>
        [Autorizar(Transacao = "MLIMITE", Acao = "Salvar")]
        public JsonResult SalvarLimiteMaximo(int frota, string peso)
        {
            try
            {
                _vagaoMangaService.SalvarLimiteMaximoFrota(frota, peso, UsuarioAtual);

                return Json(new
                {
                    Success = true,
                    Message = "Limite alterado com sucesso!"
                });
            }
            catch (Exception e)
            {
                return Json(new
                {
                    Success = false,
                    e.Message,
                    Status = 200
                });
            }
        }

        /// <summary>
        /// Remover o limite maximo setando a data final
        /// </summary>
        /// <param name="limiteMaximoId">Id do limite selecionado para remover</param>
        /// <returns>Response da operacao</returns>
        [Autorizar(Transacao = "MLIMITE", Acao = "Salvar")]
        public JsonResult RemoverLimiteMaximo(int limiteMaximoId)
        {
            try
            {
                _vagaoMangaService.RemoverLimiteMaximoFrota(limiteMaximoId);
                return Json(new
                {
                   Success = true,
                   Message = "Limite máximo removido com sucesso!"
                });
            }
            catch (Exception e)
            {
                return Json(new
                {
                   Success = false,
                   e.Message,
                   Status = 200
                });
            }
        }

        /// <summary>
        /// Método retorna as frotas_vagão
        /// </summary>
        /// <param name="frota">detalhes da paginacao</param>
        /// <returns>Action Result</returns>
        [Autorizar(Transacao = "MLIMITE", Acao = "Pesquisar")]
        public ActionResult ObterFrotas(string frota)
        {
            var result = _vagaoMangaService.ObterFrotas(frota);

            return Json(new
            {
                Items = result.Select(e => new
                {
                    e.Id,
                    e.Codigo
                }),
                success = true
            });
        }
    }
}
