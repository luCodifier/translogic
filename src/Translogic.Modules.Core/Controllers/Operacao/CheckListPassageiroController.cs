﻿namespace Translogic.Modules.Core.Controllers.Operacao
{
    using System;
    using System.Collections.Specialized;
    using System.IO;
    using System.IO.Compression;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;
    using Translogic.Core.Infrastructure.FileSystem.ExtensionMethods;
    using Translogic.Core.Infrastructure.Web;
    using Translogic.Core.Infrastructure.Web.Helpers;
    using Translogic.Core.Infrastructure.Settings;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Modules.Core.Domain.Services.ChecklistPassageiro.Interface;
    using Translogic.Modules.Core.Domain.Services.Mdfes;
    using Translogic.Modules.Core.Infrastructure;
    using System.Text;
    using System.Net;
    using iTextSharp.text;
    using iTextSharp.text.html.simpleparser;
    using iTextSharp.text.pdf;
    using Translogic.Modules.Core.Domain.Model.Dto.ChecklistPassageiro;
    using System.Collections.Generic;
    using Translogic.Core.Infrastructure.FileSystem;
    using ICSharpCode.SharpZipLib.Zip;


    public class CheckListPassageiroController : BaseSecureModuleController
    {
        private readonly IOsChecklistService _checkListService;
        private readonly MdfeService _mdfeService;


        public CheckListPassageiroController(IOsChecklistService checkListService,
            MdfeService mdfeService)
        {
            _checkListService = checkListService;
            _mdfeService = mdfeService;
        }

        // Todo: Definir filtros de permissões

        #region Métodos/Actions da tela de pesquisa
        [Autorizar(Transacao = "CHECKLISTPASS", Acao = "Pesquisar")]
        public ActionResult Index()
        {
            ViewData["Permissoes"] = _checkListService.BuscarPermissaoUsuario(base.UsuarioAtual);
            return View();
        }

        [Autorizar(Transacao = "CHECKLISTPASS", Acao = "Pesquisar")]
        public JsonResult ObterConsultaOsCheckList(DetalhesPaginacaoWeb detalhesPaginacaoWeb, string dataInicial, string dataFinal, string numOs)
        {
            var permissoes = _checkListService.BuscarPermissaoUsuario(base.UsuarioAtual);

            ViewData["dataInicial"] = dataInicial;
            ViewData["dataFinal"] = dataFinal;
            ViewData["numOs"] = numOs;

            var resultado = _checkListService.ObterConsultaOsCheckList(detalhesPaginacaoWeb,
                                                                                dataInicial,
                                                                                dataFinal,
                                                                                numOs);
            var jsonData = Json(
                new
                {
                    Items = resultado.Items.Select(i => new
                    {
                        OS = i.NumOs != null ? i.NumOs : 0,
                        Prefixo = i.Prefixo,
                        Origem = i.Origem,
                        Destino = i.Destino,
                        Data = i.Data != null ? i.Data.ToString("dd/MM/yyyy") : string.Empty,
                        Via = i.Via,
                        Locomotiva = i.Locomotiva,
                        Tracao = i.Tracao,
                        Vagao = i.Vagao,
                        Ida = i.Ida,
                        Volta = i.Volta,
                        IdOsCheckList = i.IdOsCheckList != null ? i.IdOsCheckList : 0,
                        Controlador = i.Controlador,
                        IdOs = i.IdOs != null ? i.IdOs : 0,
                        Concluido = i.Concluido,
                        IdOsCheckListAprovacao = i.IdOsCheckListAprovacao,
                        HabilitaEditar = permissoes.EditarChecklist == true ? (i.HabilitaEditar == 1 ? 1 : 0) : 1
                    }),
                    Total = resultado.Total
                });
            return jsonData;
        }

        #endregion

        #region Métodos/Actions da tela CheckList - Aba Aprovações

        [Autorizar(Transacao = "CHECKLISTPASS", Acao = "Visualizar")]
        public ActionResult Checklist(int idOs, int idOsCheckList = 0, int idOsCheckListAprovacao = 0, string visualizar = "não")
        {
            int idOsChecklistPassageiro = 0;
            int numOs;

            //Busca OS T2 pelo id Os Check
            numOs = _checkListService.BuscarNumOs(idOs);

            if (idOs == 0)
            {
                throw new Exception("Id da OS não fornecido.");
            }

            //Verifica se existe OS Passageiro para CHECKLIST
            var OsPassageiro = _checkListService.ObterOSPassageiroPorIDCheckList(idOsCheckList);

            if (OsPassageiro != null)
            {
                ViewData["idOsPassageiro"] = OsPassageiro.Id;
            }

            if (idOsCheckList == 0)
            {
                idOsCheckList = _checkListService.ObterOSCheckListPorIdOS(idOs).Id;
            }

            //OBJ CheckList
            var osChecklist = _checkListService.ObterOSCheckListPorIdOS(idOs);

            if (osChecklist != null)
            {
                if (osChecklist.Ida == 0 && osChecklist.Volta == 0)
                {
                    osChecklist.Ida = 1;
                }
                ViewData["Ida"] = osChecklist.Ida == 1 ? true : false;
                ViewData["Volta"] = osChecklist.Volta == 1 ? true : false;


            }
            //OBJ CheckListPassageiro
            var osCheckListPassageiro = _checkListService.ObterOSPassageiroPorIDCheckList(idOsCheckList);

            if (osCheckListPassageiro != null)
            {
                ViewData["idOsChecklist"] = idOsCheckList;
                idOsChecklistPassageiro = _checkListService.ObterOSPassageiroCheckList(osCheckListPassageiro.Id).Id;
                ViewData["idOsChecklistPassageiro"] = idOsChecklistPassageiro;
            }

            //Busca dados conforme regras
            //RN17: caso a OS esteja com a formação concluída, preencher a quantidade de vagões com base na formação do sistema e manter o campo aberto para edição.
            //RN18: caso a OS esteja com a formação concluída, preencher a TB com base na formação do sistema e manter o campo aberto para edição.
            //RN19: caso a OS esteja com a formação concluída, preencher as locomotivas com base na formação do sistema e manter o campo aberto para edição do operador.
            var osDadosTrem = _checkListService.BuscaDadosTremCheckList(numOs);

            if (idOsCheckList == 0 || osCheckListPassageiro == null)
            {
                if (osDadosTrem != null)
                {
                    //QTD Vagões
                    ViewData["QtdeVagao"] = Convert.ToInt32(osDadosTrem.QtdeVagoes);
                    //TB
                    ViewData["Tb"] = osDadosTrem.TB;
                    //Locomotiva 1
                    if (osDadosTrem.listLocomotiva != null && osDadosTrem.listLocomotiva.Count > 0)
                    {
                        ViewData["Locomotiva1"] = osDadosTrem.listLocomotiva[0].Locomotiva;

                        //Locomotiva 2
                        if (osDadosTrem.listLocomotiva.Count >= 2)
                        {
                            ViewData["Locomotiva2"] = osDadosTrem.listLocomotiva[1].Locomotiva;
                        }
                        //Locomotiva 3
                        if (osDadosTrem.listLocomotiva.Count >= 3)
                        {
                            ViewData["Locomotiva3"] = osDadosTrem.listLocomotiva[2].Locomotiva;
                        }
                        //Locomotiva 4
                        if (osDadosTrem.listLocomotiva.Count >= 4)
                        {
                            ViewData["Locomotiva4"] = osDadosTrem.listLocomotiva[3].Locomotiva;
                        }
                    }

                    //Previsto
                    ViewData["DataPrevisto"] = (osDadosTrem.DataPartidaPrevista.HasValue ? osDadosTrem.DataPartidaPrevista.Value.ToString("dd/MM/yyyy") : "");
                    ViewData["HoraPrevisto"] = (osDadosTrem.DataPartidaPrevista.HasValue ? osDadosTrem.DataPartidaPrevista.Value.ToString("HH:mm") : "");

                    //Data Liberacao (REAL)
                    ViewData["DataReal"] = (osDadosTrem.DataPartidaReal.HasValue ? osDadosTrem.DataPartidaReal.Value.ToString("dd/MM/yyyy") : "");
                    ViewData["HoraReal"] = (osDadosTrem.DataPartidaReal.HasValue ? osDadosTrem.DataPartidaReal.Value.ToString("HH:mm") : "");

                    //Data Chegada Real
                    ViewData["DataChegadaReal"] = (osDadosTrem.DataChegadaReal.HasValue ? osDadosTrem.DataChegadaReal.Value.ToString("dd/MM/yyyy") : "");
                    ViewData["HoraChegadaReal"] = (osDadosTrem.DataChegadaReal.HasValue ? osDadosTrem.DataChegadaReal.Value.ToString("HH:mm") : "");
                }
            }

            if (idOsCheckList != 0)
            {
                ViewData["Aprovacoes"] = _checkListService.BuscarDadosAprovados(idOsCheckList);

                if (osCheckListPassageiro != null)
                {
                    //CCO Circulação
                    ViewData["CcoSupervisorNome"] = osCheckListPassageiro.CcoSupervisorNome;
                    ViewData["CcoSupervisorMatricula"] = osCheckListPassageiro.CcoSupervisorMatricula;
                    ViewData["CtrSupervisorNome"] = osCheckListPassageiro.CtrSupervisorNome;
                    ViewData["CtrSupervisorMatricula"] = osCheckListPassageiro.CtrSupervisorMatricula;
                    //Equipagem
                    ViewData["MaqEquipSupervisorNome"] = osCheckListPassageiro.MaqEquipSupervisorNome;
                    ViewData["MaqEquipSupervisorMatricula"] = osCheckListPassageiro.MaqEquipSupervisorMatricula;
                    ViewData["AuxEquipSupervisorNome"] = osCheckListPassageiro.AuxEquipSupervisorNome;
                    ViewData["AuxEquipSupervisorMatricula"] = osCheckListPassageiro.AuxEquipSupervisorMatricula;
                    ViewData["EquipSupervisorNome"] = osCheckListPassageiro.EquipSupervisorNome;
                    ViewData["EquipSupervisorMatricula"] = osCheckListPassageiro.EquipSupervisorMatricula;
                    //Comentários Adicionais
                    ViewData["ComentAdicionais"] = osCheckListPassageiro.ComentAdicionais;

                    //CheckList
                    ViewData["Resposta1"] = osCheckListPassageiro.Resposta1;
                    ViewData["Resposta2"] = osCheckListPassageiro.Resposta2;
                    ViewData["Resposta3"] = osCheckListPassageiro.Resposta3;
                    ViewData["Resposta4"] = osCheckListPassageiro.Resposta4;
                    ViewData["Resposta5NumCruzamentos"] = osCheckListPassageiro.Resposta5NumCruzamentos;
                    ViewData["Resposta5Supervisor"] = osCheckListPassageiro.Resposta5Supervisor;
                    ViewData["Resposta5Matricula"] = osCheckListPassageiro.Resposta5Matricula;
                    ViewData["Resposta6TotalCruzamentos"] = osCheckListPassageiro.Resposta6TotalCruzamentos;
                    ViewData["Resposta7QtdePararam"] = osCheckListPassageiro.Resposta7QtdePararam;
                    //Vagões
                    ViewData["QtdeVagao"] = osCheckListPassageiro.QtdeVagao;
                    ViewData["Tb"] = osCheckListPassageiro.Tb;
                    //Locomotivas
                    //(1)
                    ViewData["Locomotiva1"] = osCheckListPassageiro.Locomotiva1;
                    ViewData["Diesel1"] = osCheckListPassageiro.Diesel1;

                    ViewData["DataVencimento1"] = osCheckListPassageiro.DataVencimento1.HasValue ? osCheckListPassageiro.DataVencimento1.Value.ToString("dd/MM/yyyy") : "";
                    ViewData["Cativas1"] = osCheckListPassageiro.Cativas1;
                    ViewData["Observacao1"] = osCheckListPassageiro.Observacao1;
                    //(2)
                    ViewData["Locomotiva2"] = osCheckListPassageiro.Locomotiva2;
                    ViewData["Diesel2"] = osCheckListPassageiro.Diesel2;
                    ViewData["DataVencimento2"] = osCheckListPassageiro.DataVencimento2.HasValue ? osCheckListPassageiro.DataVencimento2.Value.ToString("dd/MM/yyyy") : "";
                    ViewData["Cativas2"] = osCheckListPassageiro.Cativas2;
                    ViewData["Observacao2"] = osCheckListPassageiro.Observacao2;
                    //(3)
                    ViewData["Locomotiva3"] = osCheckListPassageiro.Locomotiva3;
                    ViewData["Diesel3"] = osCheckListPassageiro.Diesel3;

                    ViewData["DataVencimento3"] = osCheckListPassageiro.DataVencimento3.HasValue ? osCheckListPassageiro.DataVencimento3.Value.ToString("dd/MM/yyyy") : "";
                    ViewData["Cativas3"] = osCheckListPassageiro.Cativas3;
                    ViewData["Observacao3"] = osCheckListPassageiro.Observacao3;
                    //(4)
                    ViewData["Locomotiva4"] = osCheckListPassageiro.Locomotiva4;
                    ViewData["Diesel4"] = osCheckListPassageiro.Diesel4;
                    ViewData["DataVencimento4"] = osCheckListPassageiro.DataVencimento4.HasValue ? osCheckListPassageiro.DataVencimento4.Value.ToString("dd/MM/yyyy") : "";
                    ViewData["Cativas4"] = osCheckListPassageiro.Cativas4;
                    ViewData["Observacao4"] = osCheckListPassageiro.Observacao4;

                    ViewData["HoraChegada"] = osCheckListPassageiro.DataChegada.HasValue ? osCheckListPassageiro.DataChegada.Value.ToString("HH:mm") : "";

                    //Previsto
                    ViewData["DataPrevisto"] = osCheckListPassageiro.DataPrevisto.HasValue ? osCheckListPassageiro.DataPrevisto.Value.ToString("dd/MM/yyyy") : "";
                    ViewData["HoraPrevisto"] = osCheckListPassageiro.DataPrevisto.HasValue ? osCheckListPassageiro.DataPrevisto.Value.ToString("HH:mm") : "";
                    
                    //Data Liberacao (REAL)
                    ViewData["DataReal"] = osCheckListPassageiro.DataReal.HasValue ? osCheckListPassageiro.DataReal.Value.ToString("dd/MM/yyyy") : "";
                    ViewData["HoraReal"] = osCheckListPassageiro.DataReal.HasValue ? osCheckListPassageiro.DataReal.Value.ToString("HH:mm") : "";
                    
                    //Data Chegada Real
                    ViewData["DataChegadaReal"] = osCheckListPassageiro.DataChegadaReal.HasValue ? osCheckListPassageiro.DataChegadaReal.Value.ToString("dd/MM/yyyy") : "";
                    ViewData["HoraChegadaReal"] = osCheckListPassageiro.DataChegadaReal.HasValue ? osCheckListPassageiro.DataChegadaReal.Value.ToString("HH:mm") : "";
                }
            }

            ViewData["Aprovador"] = UsuarioAtual.Nome;
            ViewData["MatriculaAprovador"] = UsuarioAtual.Codigo;
            ViewData["idOsChecklist"] = idOsCheckList.ToString();
            ViewData["idOs"] = idOs;
            ViewData["numOs"] = numOs;
            ViewData["idOsCheckListAprovacao"] = idOsCheckListAprovacao.ToString();

            ViewData["Permissoes"] = _checkListService.BuscarPermissaoUsuario(base.UsuarioAtual);

            ViewData["SomenteVisualizarCheklist"] = visualizar;

            return View();
        }

        [Autorizar(Transacao = "CHECKLISTPASS", Acao = "Gerar")]
        public JsonResult SalvarAprovacoes(OsChecklistAprovacaoDto osChecklistAprovacaoDto)
        {
            var idOsChecklist = 0;
            try
            {
                if (osChecklistAprovacaoDto.Salvar)
                {
                    idOsChecklist = _checkListService.SalvarAprovacoes(osChecklistAprovacaoDto);
                }

            }
            catch (Exception ex)
            {

                return Json(new
                {
                    IdOsChecklist = idOsChecklist,
                    Message = ex.Message,
                    Result = false
                });
            }

            return Json(new
            {
                IdOsChecklist = idOsChecklist,
                Message = "Aprovações salvas com sucesso",
                Result = true
            });
        }

        //[Autorizar(Transacao = "CHECKLISTPASS", Acao = "Salvar")]
        public JsonResult SalvarCheckListPassageiro(OsChecklistPassageiroDto osChecklistPassageiroDto)
        {
            var osChecklistPassageiro = new OsChecklistPassageiroDto();

            try
            {
                osChecklistPassageiroDto.Matricula = UsuarioAtual.Codigo;
                osChecklistPassageiroDto.Usuario = UsuarioAtual.Nome;
                osChecklistPassageiro = _checkListService.SalvarCheckListPassageiro(osChecklistPassageiroDto);

            }
            catch (Exception ex)
            {

                return Json(new
                {
                    idOsChecklistPassageiro = osChecklistPassageiro.idOsChecklistPassageiro,
                    idOsChecklist = osChecklistPassageiro.idOsChecklist,
                    Message = ex.Message,
                    Result = false
                });
            }

            return Json(new
            {
                idOsChecklistPassageiro = osChecklistPassageiro.idOsChecklistPassageiro,
                idOsChecklist = osChecklistPassageiro.idOsChecklist,
                Message = "CheckList salvo com sucesso",
                Result = true
            });
        }

        public JsonResult ConcluirCheckListPassageiro(OsChecklistPassageiroDto osChecklistPassageiroDto)
        {
            try
            {
                _checkListService.ConcluirCheckListPassageiro(osChecklistPassageiroDto);

            }
            catch (Exception ex)
            {

                return Json(new
                {
                    Message = ex.Message,
                    Result = false
                });
            }

            return Json(new
            {
                IdOsChecklist = osChecklistPassageiroDto.idOsChecklistPassageiro,
                Message = "CheckList concluido com sucesso",
                Result = true
            });
        }

        /// <summary>
        /// Retorna o PDF
        /// </summary>
        /// <param name="idOS">ids das OS</param>
        /// <returns>Arquivo PDF</returns>
        //[Autorizar(Transacao = "RELOSLIMPVGO")]
        public ActionResult ExportarPDF(string idOS)
        {
            Stream pdf = null;
            Stream returnContent = new MemoryStream();
            var merge = new PdfMerge();

            var osChecklistExportacaoDto = _checkListService.ObterConsultaExportacaoPDF(idOS);

            var osDadosTrem = new OsDadosTremDto();
            //Busca dados trem e insere dentro objeto

            var dadosTremChecklist = _checkListService.ObterOSPassageiroPorIDCheckList(Convert.ToInt32(osChecklistExportacaoDto.IdOSCheckList));

            if (dadosTremChecklist == null)
            {
                osDadosTrem = _checkListService.BuscaDadosTremCheckList(Convert.ToInt32(osChecklistExportacaoDto.OS));

                if (osDadosTrem != null)
                {
                    osChecklistExportacaoDto.QuantidadeVagoes = osDadosTrem.QtdeVagoes.ToString();
                    osChecklistExportacaoDto.TB = osDadosTrem.TB.ToString();
                    osChecklistExportacaoDto.QuantidadeLocomotivas = osDadosTrem.listLocomotiva.Count.ToString();
                    osChecklistExportacaoDto.Real = osDadosTrem.DataPartidaReal;
                    osChecklistExportacaoDto.Previsto = osDadosTrem.DataPartidaPrevista;
                    osChecklistExportacaoDto.IdTrem = osDadosTrem.IdTrem;
                    osChecklistExportacaoDto.IdComposicao = osDadosTrem.IdComposicao;
                }
            }
            else
            {
                osChecklistExportacaoDto.QuantidadeVagoes = dadosTremChecklist.QtdeVagao != null ? dadosTremChecklist.QtdeVagao.Value.ToString() : "0";
                osChecklistExportacaoDto.TB = dadosTremChecklist.Tb != null ? dadosTremChecklist.Tb.Value.ToString() : "0";
                osChecklistExportacaoDto.QuantidadeLocomotivas = dadosTremChecklist.TotalLoco.Value.ToString();
                osChecklistExportacaoDto.Real = dadosTremChecklist.DataReal.HasValue ? dadosTremChecklist.DataReal : null;
                osChecklistExportacaoDto.Previsto = dadosTremChecklist.DataPrevisto.HasValue ? dadosTremChecklist.DataPrevisto : null;
            }

            var viewString = new StringBuilder();

            ViewData["itemOsChecklistExportacaoDto"] = osChecklistExportacaoDto;
            viewString.AppendLine(View("PaginasImpressao/CheckList").Capture(ControllerContext));

            if (osChecklistExportacaoDto.IdTrem.HasValue && osChecklistExportacaoDto.IdComposicao.HasValue)
            {
                var htmlFichaTrem = GeraFichaTremComposicao(osChecklistExportacaoDto.IdTrem.Value, osChecklistExportacaoDto.IdComposicao.Value);                
                if (!string.IsNullOrEmpty(htmlFichaTrem))
                {
                    viewString.PageBreak();
                    viewString.AppendLine(htmlFichaTrem);
                }
            }

            return this.PdfResult(
                       "CheckList",
                       string.Format("CheckList OS: {0}.pdf", idOS),
                       viewString.ToString());
        }


        public string GeraFichaTremComposicao(decimal idTrem, decimal idComposicao)
        {
            string htmlFichaTrem = string.Empty;
            try
            {
                var url = Settings.UrlTranslogicLegado.Replace("translogic", "translogicI") + "ImpressaoFichaTrem.asp?pIdComp=" + idComposicao + "&pIdTrem=" + idTrem;
                //var url = Settings.UrlTranslogicLegado.Replace("translogic", "translogicI") + "ImpressaoFichaTrem.asp?pIdComp=" + 3543913 + "&pIdTrem=" + 1445475;  
                var request = WebRequest.Create(url);
                var response = request.GetResponse();
                var dataStream = response.GetResponseStream();
                var reader = new StreamReader(dataStream);
                htmlFichaTrem = reader.ReadToEnd();

                if (!htmlFichaTrem.Contains("C�digo de Erro:") && !string.IsNullOrWhiteSpace(htmlFichaTrem))
                {

                    if (htmlFichaTrem.Contains("S�rie do Vag�o"))
                    {
                        htmlFichaTrem = htmlFichaTrem.Replace("<td id=\"tdSerieVg\" style=\"text-align: center;\">S�rie do Vag�o &nbsp;</td>", "<td id=\"tdSerieVg\" style=\"text-align: center;\">Serie do Vagao &nbsp;</td>");
                    }

                    if (htmlFichaTrem.Contains("Quantidade de Vag�e"))
                    {
                        htmlFichaTrem = htmlFichaTrem.Replace("<td id=\"tdQtdVg\" style=\"text-align: center;\">Quantidade de Vag�es &nbsp;</td>", "<td id=\"tdQtdVg\" style=\"text-align: center;\">Quantidade de Vagoes &nbsp;</td>");
                    }

                    if (htmlFichaTrem.Contains("Ader�ncia � Blocagem"))
                    {
                        htmlFichaTrem = htmlFichaTrem.Replace("<td id=\"tdAderencia \" style=\"text-align: center;\">Ader�ncia � Blocagem</td>", "<td id=\"tdAderencia\" style=\"text-align: center;\">Aderencia Blocagem &nbsp;</td>");
                    }

                    if (htmlFichaTrem.Contains("Total de Vag�es da Frota 532"))
                    {
                        htmlFichaTrem = htmlFichaTrem.Replace("<td id=\"tdFrota532\" style=\"text-align: center;\">Total de Vag�es da Frota 532</td>", "<td id=\"tdFrota532\" style=\"text-align: center;\">Total de Vagoes da Frota 532 </td>");
                    }

                    if (htmlFichaTrem.Contains("Total de Vag�es da Frota 534"))
                    {
                        htmlFichaTrem = htmlFichaTrem.Replace("<td id=\"tdFrota534\"style=\"text-align: center;\">Total de Vag�es da Frota 534</td>", "<td id=\"tdFrota534\"style=\"text-align: center;\">Total de Vagoes da Frota 534</td>");
                    }

                    if (htmlFichaTrem.Contains("Posi��o"))
                    {
                        htmlFichaTrem = htmlFichaTrem.Replace("<td id=\"tdPosicao\" style=\"font-size:13px; text-align: center; width:64px;\">Posi��o</td>", "<td id=\"tdPosicao\" style=\"font-size:13px; text-align: center; width:64px;\">Posicao</td>");
                    }

                    if (htmlFichaTrem.Contains("S�rie"))
                    {
                        htmlFichaTrem = htmlFichaTrem.Replace("<td id=\"tdSerie\"style=\"font-size:13px; text-align: center; width:46px;\">S�rie</td>", "<td id=\"tdSerie\"style=\"font-size:13px; text-align: center; width:46px;\">Serie</td>");
                    }

                    if (htmlFichaTrem.Contains("Observa��o"))
                    {
                        htmlFichaTrem = htmlFichaTrem.Replace("<td id=\"tdOBS\" style=\"font-size:13px; text-align: center; width:183px;\">Observa��o</td> ", "<td id=\"tdOBS\" style=\"font-size:13px; text-align: center; width:183px;\">Observacao</td> ");
                    }

                    if (htmlFichaTrem.Contains("Dt recomenda��o"))
                    {
                        htmlFichaTrem = htmlFichaTrem.Replace("<td id=\"tdDtRecomed\" style=\"font-size:12px; text-align: center;\">Dt recomenda��o</td> ", "<td id=\"tdDtRecomed\" style=\"font-size:12px; text-align: center;\">Dt recomendacao</td> ");
                    }


                    if (htmlFichaTrem.Contains("tbMaquinistas"))
                    {
                        htmlFichaTrem = htmlFichaTrem.Replace("<table id=\"tbMaquinistas\" style=\"font-weight:bold;\">", "<table id=\"tbMaquinistas\"  align=\"left\" style=\"font-weight:bold; text-align:left;\">");
                    }

                    if (htmlFichaTrem.Contains("Ve�culos com Restri��o/Avarias"))
                    {
                        htmlFichaTrem = htmlFichaTrem.Replace("<td id=\"tdRestricao\" style=\"font-size:12px; text-align: center;\" colspan=\"6\">Ve�culos com Restri��o/Avarias</td>", "<td id=\"tdRestricao\" style=\"font-size:12px; text-align: center;\" colspan=\"6\">Veiculos com Restricao/Avarias</td>");
                    }
                   
                }
                else
                {
                    htmlFichaTrem = string.Empty;
                }
                
                reader.Close();
                response.Close();
            }
            catch (WebException webExcp)
            {
                var status = webExcp.Status;
                htmlFichaTrem = string.Empty;
            }
            return htmlFichaTrem;
        }

        public string GeraFichaTrem(decimal idTrem)
        {
            string htmlFichaTrem = string.Empty;
            try
            {
                var url = Settings.UrlTranslogicLegado.Replace("translogic", "translogicI") + "ImpressaoFichaTrem.asp?id=" + idTrem;
                var request = WebRequest.Create(url);
                var response = request.GetResponse();
                var dataStream = response.GetResponseStream();
                var reader = new StreamReader(dataStream);
                htmlFichaTrem = reader.ReadToEnd();

                //Tratamento do html            
                htmlFichaTrem = htmlFichaTrem.Replace("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\">", string.Empty);
                htmlFichaTrem = htmlFichaTrem.Replace("table {font-size:12;}", "table {font-size:8;}");
                htmlFichaTrem = htmlFichaTrem.Replace("<table width=\"100%\" id=\"Cabecalho\">", "<table width=\"100%\" style=\"font-size:8px;\">");
                htmlFichaTrem = htmlFichaTrem.Replace("<table align=\"center\" style=\"border: 1px solid black\">", "<table align=\"center\" style=\"border: 1px solid black; font-size:9px;\">");
                htmlFichaTrem = htmlFichaTrem.Replace("<table align=\"center\">", "<table align=\"center\" style=\"font-size:8px;\">");
                htmlFichaTrem = htmlFichaTrem.Replace("<td><div align=\"right\">&nbsp;Qtd. &nbsp;</div></td>", "<td>&nbsp;&nbsp; Qtd. &nbsp;</td>");
                htmlFichaTrem = htmlFichaTrem.Replace("</td></td>", "</td>");
                htmlFichaTrem = htmlFichaTrem.Replace("<td>Veiculo</td>", "<td style=\"width: 42px;\">Veiculo</td>");
                htmlFichaTrem = htmlFichaTrem.Replace("<td>Veiculo</td>", "<td style=\"width: 42px;\">Veiculo</td>");
                htmlFichaTrem = htmlFichaTrem.Replace("<td>Ped</td>", "<td style=\"width:40px;\">Ped</td>");
                htmlFichaTrem = htmlFichaTrem.Replace("<td>Tb</td>", "<td style=\"width:35px;\">Tb</td>");
                htmlFichaTrem = htmlFichaTrem.Replace("<td>Comp.</td>", "<td style=\"width:43px;\">Comp.</td>");
                htmlFichaTrem = htmlFichaTrem.Replace("<td style=\"font-size:9px\"><span class='spanSerie'>", "<td style=\"font-size:9px; width: 54px;\"><span class='spanSerie'>");
                htmlFichaTrem = htmlFichaTrem.Replace("<table align=\"center\" id=\"RodapeTot\">", "<br/><br/><br/><table align=\"center\" id=\"RodapeTot\" style=\"font-size:8px; width:315px;\">");
                htmlFichaTrem = htmlFichaTrem.Replace("<br/><table id=\"ResSerVagao\" align=\"center\" style=\"border: 1px solid black \">", "<br/><br/><br/><table id=\"ResSerVagao\" align=\"center\" style=\"border: 1px solid black; width:180px;\">");
                htmlFichaTrem = htmlFichaTrem.Replace("<div id=\"dvFim\" align=\"center\">FIM DA EXECU&Ccedil;&Atilde;O</div>", string.Empty);
                htmlFichaTrem = htmlFichaTrem.Replace("<td id=\"tdHoraRel\"><div align=\"right\">", "<td id=\"tdHoraRel\" style=\"display:none; overflow:hidden;color:White; \"><div align=\"right\" style=\"display:none; overflow:hidden; color:White;\">");
                htmlFichaTrem = htmlFichaTrem.Replace("<td id=\"tdHoraLoca\" colspan=\"2\"><div align=\"right\">", "<td id=\"tdHoraLoca\" colspan=\"2\" style=\"display:none; overflow:hidden;color:White;\"><div align=\"right\" style=\"display:none; overflow:hidden;color:White;\">");
                reader.Close();
                response.Close();
            }
            catch (WebException webExcp)
            {
                var status = webExcp.Status;
                htmlFichaTrem = string.Empty;
            }

            return htmlFichaTrem;
        }

        #endregion

        #region Importação

        public ActionResult Importar(int idOs, int idOsChecklist, string numOs, string dataInicial, string dataFinal, string situacaoVia, string situacaoLocomotiva, string situacaoTracao, string situacaoVagao)
        {
            ViewData["idCheckList"] = idOsChecklist;
            ViewData["idOs"] = idOs;
            ViewData["numOs"] = numOs;
            ViewData["dataInicial"] = dataInicial;
            ViewData["dataFinal"] = dataFinal;
            ViewData["situacaoVia"] = situacaoVia;
            ViewData["situacaoLocomotiva"] = situacaoLocomotiva;
            ViewData["situacaoTracao"] = situacaoTracao;
            ViewData["situacaoVagao"] = situacaoVagao;

            return View();
        }

        //public ActionResult ConfirmaImpotacao()

        [HttpPost]
        public ActionResult ConfirmaImpotacao()
        {
            var mensagens = string.Empty;
            var numOs = string.Empty;
            var dataInicial = string.Empty;
            var dataFinal = string.Empty;

            if (Request.Form.Keys.Count > 1 && Request.Files != null)
            {
                var area = Request.Form["Area"];
                var aprovado = Request.Form["Aprovado"] == "1" ? true : false;
                var idOs = string.IsNullOrEmpty(Request.Form["idOs"]) ? 0 : Convert.ToInt32(Request.Form["idOs"]);
                var idOsChecklist = string.IsNullOrEmpty(Request.Form["idOsChecklist"]) ? 0 : Convert.ToInt32(Request.Form["idOsChecklist"]);
                numOs = Request.Form["numOs"];
                dataInicial = Request.Form["dataInicial"];
                dataFinal = Request.Form["dataFinal"];

                var fileContent = Request.Files[0];
                if (fileContent != null && fileContent.ContentLength > 0)
                {
                    if (!fileContent.FileName.ToLower().Contains(".msg"))
                    {
                        mensagens = "Arquivo em anexo deve ser um .msg";
                        return RedirectToIndex(mensagens, numOs, dataInicial, dataFinal);
                    }

                    var aprovacao = new OsChecklistAprovadaDto();

                    if (idOsChecklist > 0)
                    {
                        aprovacao = _checkListService.BuscarDadosAprovados(idOsChecklist);
                    }

                    var permissoes = _checkListService.BuscarPermissaoUsuario(base.UsuarioAtual);
                    if (area.ToLower() == "via")
                    {
                        if (!permissoes.AprovarVia && aprovado)
                        {
                            mensagens = "Você não possui permissão para aprovar Via";
                            return RedirectToIndex(mensagens, numOs, dataInicial, dataFinal);
                        }

                        if (!permissoes.ReprovarVia && !aprovado)
                        {
                            mensagens = "Você não possui permissão para reprovar Via";
                            return RedirectToIndex(mensagens, numOs, dataInicial, dataFinal);
                        }

                        //if (idOsChecklist > 0)
                        //{
                        //    if (aprovacao.ViaStatus == 'A')
                        //    {
                        //        mensagens = "Aprovação de VIA já aprovada";
                        //        return RedirectToIndex(mensagens, numOs, dataInicial, dataFinal);
                        //    }
                        //    else if (aprovacao.ViaStatus == 'R')
                        //    {
                        //        mensagens = "Reprovação de VIA já reprovado";
                        //        return RedirectToIndex(mensagens, numOs, dataInicial, dataFinal);
                        //    }
                        //}
                    }

                    if (area.ToLower().Contains("tra"))
                    {
                        if (!permissoes.AprovarTracao && aprovado)
                        {
                            mensagens = "Você não possui permissão para aprovar Tração";
                            return RedirectToIndex(mensagens, numOs, dataInicial, dataFinal);
                        }

                        if (!permissoes.ReprovarTracao && !aprovado)
                        {
                            mensagens = "Você não possui permissão para reprovar Tração";
                            return RedirectToIndex(mensagens, numOs, dataInicial, dataFinal);
                        }

                        //if (idOsChecklist > 0)
                        //{
                        //    if (aprovacao.TracStatus == 'A')
                        //    {
                        //        mensagens = "Aprovação de Tração já aprovada";
                        //        return RedirectToIndex(mensagens, numOs, dataInicial, dataFinal);
                        //    }
                        //}
                    }

                    if (area.ToLower().Contains("loc"))
                    {
                        if (!permissoes.AprovarLoco && aprovado)
                        {
                            mensagens = "Você não possui permissão para aprovar Locomotiva";
                            return RedirectToIndex(mensagens, numOs, dataInicial, dataFinal);
                        }

                        if (!permissoes.ReprovarLoco && !aprovado)
                        {
                            mensagens = "Você não possui permissão para reprovar Locomotiva";
                            return RedirectToIndex(mensagens, numOs, dataInicial, dataFinal);
                        }

                        //if (idOsChecklist > 0)
                        //{
                        //    if (aprovacao.LocoStatus == 'A')
                        //    {
                        //        mensagens = "Aprovação de Locomotiva já aprovada";
                        //        return RedirectToIndex(mensagens, numOs, dataInicial, dataFinal);
                        //    }
                        //}
                    }

                    if (area.ToLower().Contains("vag"))
                    {
                        if (!permissoes.AprovarVagao && aprovado)
                        {
                            mensagens = "Você não possui permissão para aprovar Vagão";
                            return RedirectToIndex(mensagens, numOs, dataInicial, dataFinal);
                        }

                        if (!permissoes.ReprovarVagao && !aprovado)
                        {
                            mensagens = "Você não possui permissão para reprovar Vagão";
                            return RedirectToIndex(mensagens, numOs, dataInicial, dataFinal);
                        }

                        //if (idOsChecklist > 0)
                        //{
                        //    if (aprovacao.VagStatus == 'A')
                        //    {
                        //        mensagens = "Aprovação de Vagão já aprovada";
                        //        return RedirectToIndex(mensagens, numOs, dataInicial, dataFinal);
                        //    }
                        //}
                    }

                    BinaryReader b = new BinaryReader(fileContent.InputStream);
                    byte[] binData = b.ReadBytes(fileContent.ContentLength);

                    try
                    {
                        _checkListService.Importar(idOs, idOsChecklist, area, aprovado, UsuarioAtual, fileContent.FileName, binData);
                        mensagens = "Importação realizada.";
                    }
                    catch (Exception ex)
                    {
                        mensagens = "Erro na importação. Erro: " + ex.Message;
                    }
                }
            }

            return RedirectToIndex(mensagens, numOs, dataInicial, dataFinal);
        }

        [Autorizar(Transacao = "CHECKLISTPASS", Acao = "Pesquisar")]
        private ActionResult RedirectToIndex(string mensagens, string numOs, string dataInicial, string dataFinal)
        {
            ViewData["Permissoes"] = _checkListService.BuscarPermissaoUsuario(base.UsuarioAtual);
            ViewData["Mensagens"] = mensagens;
            ViewData["NumOs"] = numOs;
            ViewData["DataInicial"] = dataInicial;
            ViewData["DataFinal"] = dataFinal;

            return View("Index");
        }

        [HttpGet]
        public FileResult Download(int idCheckListAprovado, string quadro)
        {
            try
            {
                var anexo = _checkListService.ObterArquivo(idCheckListAprovado, quadro);

                FileStreamResult fsr = null;
                MemoryStream returnContent = new MemoryStream(anexo.Arquivo);

                if (returnContent != null)
                {
                    fsr = new FileStreamResult(returnContent, "aapplication/vnd.ms-outlook");
                    fsr.FileDownloadName = string.Format("evidencia_checklist_{0}_{1}.msg", idCheckListAprovado, quadro);
                }

                return fsr;

            }
            catch (Exception e)
            {

                throw e;
            }
        }

        public ActionResult ExportarZIP(string idsOS)
        {
            var fls = new FileService();
            var listArquivosCompactar = new List<FileZipDto>();
            FileStreamResult result = null;

            #region Exportar PDF Checklist
            string[] ids = idsOS.Split(',');

            foreach (var id in ids)
            {

                var fileResultPDF = ExportarPDF(id);

                using (MemoryStream ms = new MemoryStream())
                {
                    var pdfResult = ((FileStreamResult)fileResultPDF);
                    pdfResult.FileStream.CopyTo(ms);

                    listArquivosCompactar.Add(new FileZipDto
                    {

                        NomeArquivo = "OS" + "_" + id + "\\" + pdfResult.FileDownloadName,
                        bytes = ms.ToArray()
                    });
                }

                #region Exportando arquivos auxiliares

                var anexos = _checkListService.ObterImportacaoPorNumOS(Convert.ToInt32(id));
                foreach (var anexo in anexos)
                {
                    listArquivosCompactar.Add(new FileZipDto
                    {
                        NomeArquivo = "OS" + "_" + id + "\\" + "evidencia_checklist_" + anexo.IdOsAprovacao + "_" + anexo.TipoAprovacao + ".msg",
                        bytes = anexo.Arquivo
                    });
                }

                #endregion

                #region Compactando Arquivo

                var streamResult = fls.ZipFiles(listArquivosCompactar);

                #endregion

                if (streamResult != null)
                {
                    result = new FileStreamResult(streamResult, "application/zip");
                    result.FileDownloadName = "Checklist.zip";

                }
            }

            if (result != null)
            {
                return result;
            }

            #endregion

            return null;
        }

        public JsonResult ObterAreas()
        {
            var areas = new Dictionary<string, string>();

            var permissoes = _checkListService.BuscarPermissaoUsuario(base.UsuarioAtual);

            if (permissoes.AprovarVia || permissoes.ReprovarVia)
                areas.Add("via", "Via");

            if (permissoes.AprovarLoco || permissoes.ReprovarLoco)
                areas.Add("loc", "Locomotiva");

            if (permissoes.AprovarTracao || permissoes.ReprovarTracao)
                areas.Add("tra", "Tração");

            if (permissoes.AprovarVagao || permissoes.ReprovarVagao)
                areas.Add("vag", "Vagão");

            var jsonData = Json(
                new
                {
                    Items = areas.Select(i => new
                    {
                        Id = i.Key,
                        Descricao = i.Value
                    }),
                });
            return jsonData;
        }
        #endregion

    }
}

