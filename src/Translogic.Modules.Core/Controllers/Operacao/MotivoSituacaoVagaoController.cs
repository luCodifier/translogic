﻿namespace Translogic.Modules.Core.Controllers.Operacao
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using Translogic.Core.Infrastructure.Web;
    using Translogic.Core.Infrastructure.Web.ContentResults;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Modules.Core.Domain.Model.MotivoSituacaoVagao;
    using Translogic.Modules.Core.Domain.Services.MotivoSituacaoVagao;
    using Translogic.Modules.Core.Infrastructure;

    public class MotivoSituacaoVagaoController : BaseSecureModuleController
    {
        private readonly MotivoSituacaoVagaoService _motivoSituacaoVagaoService;

        public MotivoSituacaoVagaoController(MotivoSituacaoVagaoService motivoSituacaoVagaoService)
        {
            _motivoSituacaoVagaoService = motivoSituacaoVagaoService;
        }

        #region ActionResults

        /// <summary>
        /// Painel de Expedição - Visualização
        /// </summary>
        [Autorizar(Transacao = "MOTIVOVAGAO")]        
        public ActionResult Index()
        {
            return View();
        }       
               
        #endregion

        #region JsonResults

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public JsonResult ObterLotacoes()
        {
            var resultado = _motivoSituacaoVagaoService.ObterLotacoes();

            var jsonData = Json(
                new
                {
                    Items = resultado.Select(i => new
                    {
                        i.IdLotacao,
                        i.DescricaoLotacao                        
                    }),
                    Total = resultado.Count
                });
            return jsonData;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public JsonResult ObterSituacoes()
        {
            var resultado = _motivoSituacaoVagaoService.ObterSituacoes();

            var jsonData = Json(
                new
                {
                    Items = resultado.Select(i => new
                    {
                        i.IdSituacao,
                        i.DescricaoSituacao
                    }),
                    Total = resultado.Count
                });
            return jsonData;
        }

       /// <summary>
       /// 
       /// </summary>
       /// <param name="idSituacao"></param>
       /// <returns></returns>
        public JsonResult ObterMotivos(string idSituacao)
        {
            var resultado = _motivoSituacaoVagaoService.ObterMotivos(idSituacao);

            if (!String.IsNullOrEmpty(idSituacao))
            {
                // Remove Todos            
                var itemTodos = resultado.FirstOrDefault(r => r.DescricaoMotivo.ToLower() == "todos");
                if (itemTodos != null)
                {
                    resultado.Remove(itemTodos);
                }
            }

            var jsonData = Json(
                new
                {
                    Items = resultado.Select(i => new
                    {
                        i.IdMotivo,
                        i.DescricaoMotivo
                    }),
                    Total = resultado.Count
                });
            return jsonData;
        }

        /// <summary>
        /// Consultar Processo Nfe
        /// </summary>
        /// <param name="chaves">chaves das nfes separadas por ;</param>
        /// <param name="chavesQueJaDeramErro">chaves das nfes que já deram erro</param>
        /// <returns>notas faltantes</returns>
        public JsonResult ConsultarMotivosVagoes(DetalhesPaginacaoWeb detalhesPaginacaoWeb, string listaDeVagoes, string idLocal, string idOS, string idLotacao, 
                                                    string idSituacao, string idMotivo)
        {                        
            var resultado = _motivoSituacaoVagaoService.ObterConsultaDeVagoesPatio(detalhesPaginacaoWeb, listaDeVagoes, idLocal, idOS, idLotacao, idSituacao, idMotivo);
                       
            var jsonData = Json(
                new
                    {   Items = resultado.Items.Select(i => new
                              {
                                  i.NumeroVagao, 
                                  i.IdVagao,
                                  i.Local, 
                                  i.Serie, 
                                  i.Linha, 
                                  i.Sequencia, 
                                  i.IdLotacao, 
                                  i.DescricaoLotacao, 
                                  i.IdSituacao, 
                                  i.DescricaoSituacao, 
                                  i.IdCondicaoDeUso, 
                                  i.DescricaoCondicaoDeUso, 
                                  i.IdLocalizacao, 
                                  i.DescricaoLocalizacao, 
                                  i.IdVagaoMotivo,
                                  i.IdMotivo,
                                  i.DescricaoMotivo,

                                  DataEmissao = i.DataEmissao.HasValue ? i.DataEmissao.Value.ToString("dd/MM/yyyy HH:mm:ss") : "",
                                  NomeUsuario = String.IsNullOrEmpty(i.NomeUsuario) ? String.Empty : i.NomeUsuario
                        
                            }),
                        Total = resultado.Total
                    });
            return jsonData;
        }

        /// <summary>
        /// Salvar Motivo
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [Autorizar(Transacao = "MOTIVOVAGAO", Acao = "Salvar")]
        public JsonResult SalvarMotivosVagoes(List<VagaoMotivo> request)
        {
           var incluiuTodosOsRegistros =  _motivoSituacaoVagaoService.InserirMotivosVagoes(request, UsuarioAtual.Codigo);

            if (incluiuTodosOsRegistros){
                return Json(new {Success = true, Message = "Motivos salvos com sucesso!"});
            }
            else{
                return Json(new { Success = false, Message = "Ocorreu um erro ao salvar os motivos!" });
            }

        }

        /// <summary>
        /// Exclusão de Motivo
        /// </summary>
        /// <param name="idVagaoMotivo"></param>
        /// <param name="idVagao"></param>
        /// <param name="idMotivo"></param>
        /// <returns></returns>
        [Autorizar(Transacao = "MOTIVOVAGAO", Acao = "Salvar")] 
        public JsonResult ExcluirMotivoDoVagao(string idVagaoMotivo, string idVagao, string idMotivo)
        {
            var excluiuMotivoVagao = _motivoSituacaoVagaoService.ExcluirMotivoDoVagao(idVagaoMotivo, idVagao, idMotivo, UsuarioAtual.Codigo);


            if (excluiuMotivoVagao)
            {
                return Json(new { Success = true, Message = "Vagão excluido com sucesso!" });
            }
            else
            {
                 return Json(new { Success = false, Message = "Ocorreu um erro ao excluir o motivo!" });
            }
            
        }

        #endregion

        #region Métodos

        /// <summary>
        /// 
        /// </summary>
        /// <param name="listaDeVagoes"></param>
        /// <param name="idLocal"></param>
        /// <param name="idOS"></param>
        /// <param name="idLotacao"></param>
        /// <param name="idSituacao"></param>
        /// <param name="idMotivo"></param>
        /// <returns></returns>
        public ActionResult ObterConsultarMotivosVagoesExportar(string listaDeVagoes, string idLocal, string idOS, string idLotacao, string idSituacao, string idMotivo)
        {

            DetalhesPaginacaoWeb detalhesPaginacaoWeb = new DetalhesPaginacaoWeb() {Limite = 0};

            var result = this._motivoSituacaoVagaoService.ObterConsultaDeVagoesPatio(
                                                                    detalhesPaginacaoWeb,
                                                                    listaDeVagoes,
                                                                    idLocal,
                                                                    idOS,
                                                                    idLotacao,
                                                                    idSituacao,
                                                                    idMotivo);

            var dic = new Dictionary<string, Func<MotivoSituacaoVagaoDto, string>>();
           
            dic["Numero Vagão"] = c => !String.IsNullOrEmpty(c.NumeroVagao) ? c.NumeroVagao : String.Empty;
            dic["Série"] = c => !String.IsNullOrEmpty(c.Serie) ? c.Serie : String.Empty;
            dic["Local"] = c => !String.IsNullOrEmpty(c.Local) ? c.Local : String.Empty;
            dic["Linha"] = c => !String.IsNullOrEmpty(c.Linha) ? c.Linha : String.Empty;
            dic["Sequencia"] = c => (c.Sequencia > 0 ? c.Sequencia.ToString() : String.Empty);
            dic["Lotação"] = c => !String.IsNullOrEmpty(c.DescricaoLotacao) ? c.DescricaoLotacao : String.Empty;
            dic["Situação"] = c => !String.IsNullOrEmpty(c.DescricaoSituacao) ? c.DescricaoSituacao : String.Empty;
            dic["Condições de Uso"] = c => !String.IsNullOrEmpty(c.DescricaoCondicaoDeUso) ? c.DescricaoCondicaoDeUso : String.Empty;
            dic["Motivo"] = c => !String.IsNullOrEmpty(c.DescricaoMotivo) ? c.DescricaoMotivo : String.Empty;
            dic["Data de Emissão"] = c => (c.DataEmissao.HasValue ? c.DataEmissao.Value.ToString("dd/MM/yyyy") : String.Empty);
            
            return this.Excel(string.Format("MotivosSituacoesVagoes_{0}_{1}.xls", DateTime.Now.ToString("ddMMyyyy"), DateTime.Now.ToString("HHmmss")), dic, result.Items );
        }

        #endregion
    }

}
