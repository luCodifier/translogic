﻿namespace Translogic.Modules.Core.Controllers.Operacao
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using ALL.Core.Dominio;
    using Translogic.Core.Infrastructure.Web;
    using Translogic.Core.Infrastructure.Web.ContentResults;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao;
    using Translogic.Modules.Core.Domain.Services.Trem;
    using Translogic.Modules.Core.Infrastructure;

    /// <summary>
    ///     Classe LiberacaoAnexacaoController
    /// </summary>
    public class VagoesTravadosAnxController : BaseSecureModuleController
    {
        #region Fields

        private readonly VagoesTravadosAnxService _vagoesTravadosAnxService;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///     Cosntrutor Padrão
        /// </summary>
        /// <param name="vagoesTravadosAnxService">Liberação Anexação Service</param>
        public VagoesTravadosAnxController(VagoesTravadosAnxService vagoesTravadosAnxService)
        {
            this._vagoesTravadosAnxService = vagoesTravadosAnxService;
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     Método Exportar
        /// </summary>
        /// <param name="ids">ids dos vagões para exportar</param>
        /// <returns>Action Result</returns>
        [Autorizar(Transacao = "LIBERANEXACAO", Acao = "Salvar")]
        public ActionResult Exportar(string ids)
        {
            try
            {
                IList<VagoesTravadosAnx> listaHistoricoPorVagao = new List<VagoesTravadosAnx>();
                var dic = new Dictionary<string, Func<VagoesTravadosAnx, string>>();

                ////dic["Vagão"] = c => (c.Vagao.Codigo == null || c.Vagao.Serie.Codigo == null) ? string.Empty : c.Vagao.Serie.Codigo + "-" + c.Vagao.Codigo;
                ////dic["Peso Limite"] = c => c.Limite == null ? string.Empty : c.Limite.ToString();
                ////dic["Tipo"] = c => c.ObterTravadoPor();
                ////dic["Origem/Destino"] = c => c.FluxoComercial.Origem.Codigo + "/" + c.FluxoComercial.Destino.Codigo;
                ////dic["Data"] = c => c.VersionDate == null ? string.Empty : c.VersionDate.Value.ToString("dd/MM/yyyy hh:mm:ss");
                ////dic["Data da Liberação"] = c => c.DataLiberacao == null ? string.Empty : c.DataLiberacao.Value.ToString("dd/MM/yyyy hh:mm:ss");
                ////dic["Motivo Travamento"] = c => "Vagão travado por " + c.ObterTravadoPor() + ". Peso Limite: " + c.Limite + " Peso Informado: " + c.PesoInformado;

                return this.Excel("Historico_Liberacao_Vagao.xls", dic, listaHistoricoPorVagao);
            }
            catch (Exception e)
            {
                return this.Json(new { success = false, e.Message, e.StackTrace });
            }
        }

        /// <summary>
        ///     Método Index
        /// </summary>
        /// <returns>Action Result</returns>
        public ActionResult Index()
        {
            return this.View();
        }

        /// <summary>
        ///     Método pesquisar
        /// </summary>
        /// <param name="pagination">detalhes da paginacao</param>
        /// <param name="dataInicial">Data Inicial</param>
        /// <param name="dataFinal">Data Final</param>
        /// <param name="local">local do vagão</param>
        /// <param name="prefixo">prefixo do trem</param>
        /// <param name="vagoes">vagoes para serem pesquisados</param>
        /// <returns>Action Result</returns>
        public ActionResult Pesquisar(
            DetalhesPaginacaoWeb pagination,
            DateTime? dataInicial,
            DateTime? dataFinal,
            string local,
            string prefixo,
            string vagoes)
        {
            ResultadoPaginado<VagoesTravadosAnx> result = this._vagoesTravadosAnxService.ObterPorFiltro(
                                                                    pagination,
                                                                    dataInicial,
                                                                    dataFinal,
                                                                    local.ToUpperInvariant(),
                                                                    prefixo.ToUpperInvariant(),
                                                                    vagoes);
            
            return
                this.Json(
                    new
                        {
                            result.Total,
                            Items =
                                result.Items.Select(
                                    e =>
                                    new
                                        {
                                            e.Id,
                                            Vagao = e.Vagao.Codigo,
                                            Serie_Vagao = e.Vagao.Serie.Codigo + "-" + e.Vagao.Codigo,
                                            e.Prefixo,
                                            Local = e.Local.Codigo,
                                            TravadoPor = e.Usuario.Codigo,
                                            DataTrava = e.DataTrava.HasValue ? e.DataTrava.Value.ToString("dd/MM/yyyy hh:mm:ss") : "",
                                            DataAnexacao = e.DataAnexacao.HasValue? e.DataAnexacao.Value.ToString("dd/MM/yyyy hh:mm:ss"): "",
                                            DataLiberacao = e.DataLiberacao.HasValue ? e.DataLiberacao.Value.ToString("dd/MM/yyyy hh:mm:ss") : ""
                                        }),
                            success = true
                        });
        }

        /// <summary>
        ///     Método Salvar
        /// </summary>
        /// <param name="ids">ids para salvar</param>
        /// <returns>Action Result</returns>
        [Autorizar(Transacao = "LIBERANEXACAO", Acao = "Salvar")]
        public ActionResult Salvar(int[] ids)
        {
            try
            {
                this._vagoesTravadosAnxService.Liberar(this.UsuarioAtual, ids);
                return this.Json(new { success = true });
            }
            catch (Exception e)
            {
                return this.Json(new { success = false, e.Message, e.StackTrace });
            }
        }
        
        #endregion
    }
}