﻿namespace Translogic.Modules.Core.Controllers.Operacao
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Web.Mvc;

    using ALL.Core.Dominio;
    using ALL.Core.Util;

    using Translogic.Core.Infrastructure.Web;
    using Translogic.Core.Infrastructure.Web.ContentResults;
    using Translogic.Modules.Core.Domain.Model.Diversos;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao;
    using Translogic.Modules.Core.Domain.Services.Trem;
    using Translogic.Modules.Core.Infrastructure;

    /// <summary>
    ///     Classe LiberacaoVagaoMangaController
    /// </summary>
    public class LiberacaoVagaoMangaController : BaseSecureModuleController
    {
        #region Fields

        private readonly VagaoLimitePesoService _vagaoMangaService;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///     Cosntrutor Padrão
        /// </summary>
        /// <param name="vagaoMangaService">vagao Manga Service</param>
        public LiberacaoVagaoMangaController(VagaoLimitePesoService vagaoMangaService)
        {
            this._vagaoMangaService = vagaoMangaService;
        }

        #endregion

        #region Public Methods and Operators
        /*
        /// <summary>
        ///     Método Exportar
        /// </summary>
        /// <param name="ids">ids dos vagões para exportar</param>
        /// <returns>Action Result</returns>
        [Autorizar(Transacao = "LIBERVAGAOMANGA", Acao = "Salvar")]
        public ActionResult Exportar(string ids)
        {
            try
            {
                ids = ids ?? string.Empty;
                int[] codigosDosVagoesSeparados =
                    ids.ToUpper()
                        .Trim()
                        .Replace(" ", string.Empty)
                        .Replace(",", ";")
                        .Replace(".", ";")
                        .Split(';')
                        .Where(e => !string.IsNullOrWhiteSpace(e))
                        .Select(int.Parse)
                        .ToArray();

                var dataUltimosSeisMeses = DateTime.Now.AddMonths(-6);

                IList<VagaoLimitePeso> resultados = this._vagaoMangaService.ObterPorIds(codigosDosVagoesSeparados);
                IList<VagaoLimitePeso> listaHistoricoPorVagao = resultados.Where(vagaoLimitePeso => vagaoLimitePeso.Vagao.Id != null && vagaoLimitePeso.VersionDate >= dataUltimosSeisMeses)
                                                                .SelectMany(vagaoLimitePeso => this._vagaoMangaService.ObterPorVagao((int)vagaoLimitePeso.Vagao.Id))
                                                                .ToList();

                var dic = new Dictionary<string, Func<VagaoLimitePeso, string>>();

                dic["Vagão"] = c => (c.Vagao.Codigo == null || c.Vagao.Serie.Codigo == null) ? string.Empty : c.Vagao.Serie.Codigo + "-" + c.Vagao.Codigo;
                dic["Peso Informado"] = c => c.PesoInformado == null ? string.Empty : c.PesoInformado.ToString();
                dic["Média mínimo"] = c => (c.LimitadoPor == LimitadoPorEnum.Minimo || c.LimitadoPor == LimitadoPorEnum.PesoMinVagao) ? this._vagaoMangaService.ObterMediaPesoInformadoVagaoTravadoPorPesoMinimo((int)c.Vagao.Id).ToString() : string.Empty;
                dic["Peso Limite"] = c => c.Limite == null ? string.Empty : c.Limite.ToString();
                dic["Tipo"] = c => c.ObterTravadoPor();
                dic["Origem/Destino"] = c => c.FluxoComercial.Origem.Codigo + "/" + c.FluxoComercial.Destino.Codigo;
                dic["Data"] = c => c.VersionDate == null ? string.Empty : c.VersionDate.Value.ToString("dd/MM/yyyy hh:mm:ss");
                dic["Data da Liberação"] = c => c.DataLiberacao == null ? string.Empty : c.DataLiberacao.Value.ToString("dd/MM/yyyy hh:mm:ss");
                dic["Motivo Travamento"] = c => "Vagão travado por " + c.ObterTravadoPor() + ". Peso Limite: " + c.Limite + " Peso Informado: " + c.PesoInformado;

                return this.Excel("Historico_Liberacao_Vagao.xls", dic, listaHistoricoPorVagao);
            }
            catch (Exception e)
            {
                return this.Json(new { success = false, e.Message, e.StackTrace });
            }
        }
        */

        /// <summary>
        ///     Método Index
        /// </summary>
        /// <returns>Action Result</returns>
        public ActionResult Index()
        {
            return this.View();
        }

        /// <summary>
        ///     Método pesquisar
        /// </summary>
        /// <param name="pagination">detalhes da paginacao</param>
        /// <param name="dataInicial">Data Inicial</param>
        /// <param name="dataFinal">Data Final</param>
        /// <param name="origem">Linha de origem</param>
        /// <param name="destino">Linha de destino</param>
        /// <param name="vagoes">vagoes para serem pesquisados</param>
        /// <returns>Action Result</returns>
        public ActionResult Pesquisar(
            DetalhesPaginacaoWeb pagination,
            DateTime? dataInicial,
            DateTime? dataFinal,
            string origem,
            string destino,
            string vagoes)
        {
            ResultadoPaginado<VagaoLimitePesoVigente> result = this._vagaoMangaService.ObterPorFiltro(
                                                                    pagination,
                                                                    dataInicial,
                                                                    dataFinal,
                                                                    origem.ToUpperInvariant(),
                                                                    destino.ToUpperInvariant(),
                                                                    vagoes);
            string tolerancia = this._vagaoMangaService.ObterTolerancia();
            string toleranciaMinima = this._vagaoMangaService.ObterToleranciaMinima();

            return
                this.Json(
                    new
                        {
                            result.Total,
                            Items =
                                result.Items.Select(
                                    e =>
                                    new
                                        {
                                            e.Id,
                                            Vagao = e.Vagao.Codigo,
                                            Serie_Vagao = e.Vagao.Serie.Codigo + "-" + e.Vagao.Codigo,
                                            e.Limite,
                                            LimitePeso =
                                        e.LimitadoPor == LimitadoPorEnum.Minimo
                                            ? e.Limite + Convert.ToDouble(toleranciaMinima)
                                            : e.Limite - Convert.ToDouble(tolerancia),
                                            Tolerancia =
                                        e.LimitadoPor == LimitadoPorEnum.Minimo ? toleranciaMinima : tolerancia,
                                            e.PesoInformado,
                                            MediaMinimo = (e.LimitadoPor == LimitadoPorEnum.Minimo || e.LimitadoPor == LimitadoPorEnum.PesoMinVagao) ? this._vagaoMangaService.ObterMediaPesoInformadoVagaoTravadoPorPesoMinimo((int)e.Vagao.Id).ToString() : string.Empty,
                                            FluxoComercial = e.FluxoComercial.Codigo,
                                            TravadoPor = e.ObterTravadoPor(),
                                            LimitadoPor = Enum<LimitadoPorEnum>.GetDescriptionOf(e.LimitadoPor),
                                            Origem = e.FluxoComercial.Origem.Codigo,
                                            Destino = e.FluxoComercial.Destino.Codigo,
                                            Mercadoria = e.FluxoComercial.Mercadoria.DescricaoResumida,
                                            MercadoriaCod = e.FluxoComercial.Mercadoria.Codigo,
                                            DataEvento = e.VersionDate.Value.ToString("dd/MM/yyyy hh:mm:ss"),
                                            Local = 
                                        e.Vagao.ListaVagaoPatioVigente.FirstOrDefault() != null
                                            ? e.Vagao.ListaVagaoPatioVigente.FirstOrDefault().Patio.Codigo
                                            : string.Empty
                                        }),
                            success = true
                        });
        }

        /// <summary>
        ///     Método Salvar
        /// </summary>
        /// <param name="ids">ids para salvar</param>
        /// <returns>Action Result</returns>
        [Autorizar(Transacao = "LIBERVAGAOMANGA", Acao = "Salvar")]
        public ActionResult Salvar(int[] ids)
        {
            try
            {
                this._vagaoMangaService.Liberar(this.UsuarioAtual, ids);
                return this.Json(new { success = true });
            }
            catch (Exception e)
            {
                return this.Json(new { success = false, e.Message, e.StackTrace });
            }
        }

        /// <summary>
        ///     Salva um limite mínimo para o vagão e mercadoria
        /// </summary>
        /// <param name="limiteMinimoBaseId">limite de peso para esta mercadoria neste vagão</param>
        /// <param name="vagao">vagão que receberá o limite</param>
        /// <param name="pesoMinimo">o novo limite mínimo para o vagão com esta mercadoria</param>
        /// <returns>Response com o sucesso ou não da operação</returns>
        [Autorizar(Transacao = "LIBERVAGAOMANGA", Acao = "Cadastrar")]
        public JsonResult SalvarLimiteMinimo(int limiteMinimoBaseId, string vagao, string pesoMinimo)
        {
            try
            {
                this._vagaoMangaService.SalvarLimiteMinimoVagao(
                    limiteMinimoBaseId,
                    vagao,
                    pesoMinimo,
                    this.UsuarioAtual);

                return this.Json(new { Success = true, CodigoVagao = vagao, Message = "Limite alterado com sucesso!" });
            }
            catch (Exception e)
            {
                return this.Json(new { Success = false, e.Message, Status = 200 });
            }
        }

        /// <summary>
        ///     Salva um limite mínimo para o vagão e mercadoria
        /// </summary>
        /// <param name="limite">limite de peso para esta mercadoria neste vagão</param>
        /// <param name="mercadoria">mercadoria que receberá o limite no vagão</param>
        /// <param name="vagao">vagão que receberá o limite</param>
        /// <param name="pesoMinimo">Peso mímino informado</param>
        /// <returns>Response com o sucesso ou não da operação</returns>
        [Autorizar(Transacao = "LIBERVAGAOMANGA", Acao = "Cadastrar")]
        public JsonResult VerificarLimiteMinimo(string limite, string mercadoria, string vagao, string pesoMinimo)
        {
            try
            {
                decimal limiteVerificar = 0;
                decimal pesoMinimoVerificar = 0;

                limite = limite.Replace(".", ",");

                if (decimal.TryParse(limite, out limiteVerificar)
                    && decimal.TryParse(pesoMinimo, out pesoMinimoVerificar))
                {
                    LimiteMinimoBase limiteMinimoBase =
                        this._vagaoMangaService.VerificarLimiteMinimoBaseTuMedia(limiteVerificar, mercadoria, vagao);

                    if (limiteMinimoBase == null)
                    {
                        var strMsgm = new StringBuilder("Não existe limite consolidado:<br/>");
                        strMsgm.AppendLine(string.Format("Limite Rota: {0}<br/>", limite));
                        strMsgm.AppendLine(string.Format("Mercadoria: {0}<br/>", mercadoria));
                        strMsgm.AppendLine(string.Format("Vagão: {0}<br/>", vagao));

                        throw new Exception(strMsgm.ToString());
                    }

                    LimiteExcecaoVagao limiteExcecao =
                        this._vagaoMangaService.ObterLimiteExcecaoVagaoPorLimiteMinimoBaseId(limiteMinimoBase.Id);

                    if (limiteExcecao != null)
                    {
                        return
                            this.Json(
                                new
                                    {
                                        Success = true,
                                        LimiteMinimoBase =
                                            new
                                                {
                                                    limiteMinimoBase.Id,
                                                    limiteMinimoBase.MenorValorRota,
                                                    limiteMinimoBase.Mercadoria
                                                },
                                        LimiteExcecaoVagao = new { limiteExcecao.Id, limiteExcecao.PesoMinimo },
                                    });
                    }

                    return
                        this.Json(
                            new
                                {
                                    Success = true,
                                    LimiteMinimoBase =
                                        new
                                            {
                                                limiteMinimoBase.Id,
                                                limiteMinimoBase.MenorValorRota,
                                                limiteMinimoBase.Mercadoria
                                            },
                                    LimiteExcecaoVagao = (object)null
                                });
                }

                return
                    this.Json(
                        new { Success = true, LimiteMinimoBase = (object)null, LimiteExcecaoVagao = (object)null });
            }
            catch (Exception e)
            {
                return this.Json(new { Success = false, e.Message, Status = 200 });
            }
        }

        #endregion
    }
}