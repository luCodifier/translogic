﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Translogic.Core.Infrastructure.Web;
using Translogic.Modules.Core.Domain.Model.Dto;
using Translogic.Modules.Core.Domain.Model.TipoServicos.Repositories;
using Translogic.Modules.Core.Domain.Services.Fornecedor.Interface;
using Translogic.Modules.Core.Domain.Services.LocaisFornecedor.Interface;
using Translogic.Modules.Core.Domain.Services.Via.Interface;
using Translogic.Modules.Core.Infrastructure;
using Translogic.Modules.Core.Domain.Model.Fornecedor;
using Translogic.Modules.Core.Domain.Model.LiberacaoFormacaoTrem.Repositories;
using Newtonsoft.Json;
using Translogic.Modules.TPCCO.Helpers;

namespace Translogic.Modules.Core.Controllers.Fornecedor
{
    public class LiberacaoFormacaoTremController : BaseSecureModuleController
    {
        private readonly ILiberacaoFormacaoTremRepository _liberacaoFormacaoTremServiceRepository;
        private readonly ILiberacaoFormacaoTremService _liberacaoFormacaoTremService;

        public LiberacaoFormacaoTremController(
                                    ILiberacaoFormacaoTremRepository liberacaoFormacaoTrem,
                                    ILiberacaoFormacaoTremService liberacaoFormacaoTremService)
        {
            _liberacaoFormacaoTremServiceRepository = liberacaoFormacaoTrem;
            _liberacaoFormacaoTremService = liberacaoFormacaoTremService;
        }

        // GET: /Fornecedor/
        //[Autorizar(Transacao = " SRVCADFORNEC")]
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Pesquisa para Liberacao Formacao Trem
        /// </summary>
        /// <param name="detalhesPaginacaoWeb"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public JsonResult ConsultaLiberacaoFormacaoTrem(DetalhesPaginacaoWeb detalhesPaginacaoWeb, string liberacaoFormacaoTremDto)
        {
            try
            {
                var parametros = JsonConvert.DeserializeObject<LiberacaoFormacaoTremDto>(liberacaoFormacaoTremDto);

                //if (request.DataInicio == null || request.DataInicio == new DateTime())
                //    throw new Exception("Data inicial é obrigatório");

                //if (request.DataFinal == null || request.DataFinal == new DateTime())
                //    throw new Exception("Data inicial é obrigatório");

                //if (DateTime.Compare(request.DataFinal, request.DataInicio) < 0)
                //    throw new Exception("Data inicial deve ser menor que a data final");

                //if (request.DataFinal.Subtract(request.DataInicio).Days + 1 > 30)
                //    throw new Exception("O período da pesquisa não deve ser superior a 30 dias");
                var resultado = _liberacaoFormacaoTremServiceRepository.ObterConsulta(detalhesPaginacaoWeb,
                    parametros.OrdemServico,
                    parametros.OrigemTrem,
                    parametros.DestinoTrem,
                    parametros.LocalAtual,
                    parametros.NomeSol,
                    parametros.NomeAutorizador,
                    parametros.DtPeriodoInicial,
                    parametros.DtPeriodoFinal,
                    parametros.SituacaoTrava);

                var jsonData = Json(
                    new
                    {
                        Items = resultado.Items.Select(i => new
                        {
                            //Campos
                            NomeSol = i.NomeSol,
                            OrdemServico = i.OrdemServico,
                            OrigemDestinoTrem = i.OrigemDestinoTrem,
                            LocalAtual = i.LocalAtual,
                            TipoTrava = i.TipoTrava,
                            MotivoTrava = i.MotivoTrava,
                            DtPartida = i.DtPartida.ToString("dd/MM/yyyy HH:mm:ss"),
                            DtSolicitacao = i.DtSolicitacao.ToString("dd/MM/yyyy HH:mm:ss"),
                            DtResposta = i.DtResposta.ToString("dd/MM/yyyy HH:mm:ss"),
                            NomeAutorizador = i.NomeAutorizador,
                            SituacaoTrava = i.SituacaoTrava
                        }),
                        Total = resultado.Total
                    });
                return jsonData;
            }
            catch (Exception e)
            {
                return Json(new
                {
                    Success = false,
                    e.Message,
                    Status = 400 // Bad Request
                });
            }
        }

        /// <summary>
        /// Pesquisa para exportação para Excel
        /// </summary>
        /// <param name="LiberacaoFormacaoTremDto"></param>
        /// <returns></returns>
        // [Autorizar(Transacao = "")]
        public ActionResult ConsultaLiberacaoFormacaoTremExportar(string liberacaoFormacaoTremDto)
        {
            var request = JsonConvert.DeserializeObject<LiberacaoFormacaoTremDto>(liberacaoFormacaoTremDto);

            //if (request.DataInicio == null || request.DataInicio == new DateTime())
            //    throw new Exception("Data inicial é obrigatório");

            //if (request.DataFinal == null || request.DataFinal == new DateTime())
            //    throw new Exception("Data inicial é obrigatório");


            //if (DateTime.Compare(request.DataFinal, request.DataInicio) < 0)
            //    throw new Exception("Data inicial deve ser menor que a data final");

            //if (request.DataFinal.Subtract(request.DataInicio).Days + 1 > 30)
            //    throw new Exception("O período da pesquisa não deve ser superior a 30 dias");


            var colunas = new List<ExcelColuna<LiberacaoFormacaoTremDto>>
            {
                new ExcelColuna<LiberacaoFormacaoTremDto>("Solicitante", c => !string.IsNullOrEmpty(c.NomeSol) ? c.NomeSol : string.Empty),
                new ExcelColunaDecimal<LiberacaoFormacaoTremDto>("OS", c => c.OrdemServico),
                new ExcelColuna<LiberacaoFormacaoTremDto>("Origem/Destino", c => !string.IsNullOrEmpty(c.OrigemDestinoTrem) ? c.OrigemDestinoTrem : string.Empty),
                new ExcelColuna<LiberacaoFormacaoTremDto>("Local", c => !string.IsNullOrEmpty(c.LocalAtual) ? c.LocalAtual : string.Empty),
                new ExcelColuna<LiberacaoFormacaoTremDto>("Trava", c => !string.IsNullOrEmpty(c.TipoTrava) ? c.TipoTrava : string.Empty),
                new ExcelColuna<LiberacaoFormacaoTremDto>("Detalhe", c => !string.IsNullOrEmpty(c.MotivoTrava) ? c.MotivoTrava : string.Empty),
                new ExcelColuna<LiberacaoFormacaoTremDto>("Data/Hora Trava", c => c.DtPartida.ToString("dd/MM/yyyy HH:mm")),
                new ExcelColuna<LiberacaoFormacaoTremDto>("Data/Hora Solicitação", c => c.DtSolicitacao.ToString("dd/MM/yyyy HH:mm")),
                new ExcelColunaData<LiberacaoFormacaoTremDto>("Data Resposta", c => c.DtResposta),
                new ExcelColuna<LiberacaoFormacaoTremDto>("Autorizador", c => !string.IsNullOrEmpty(c.NomeAutorizador) ? c.NomeAutorizador : string.Empty),
                new ExcelColuna<LiberacaoFormacaoTremDto>("Situação", c => !string.IsNullOrEmpty(c.SituacaoTrava) ? c.SituacaoTrava : string.Empty)
            };

            var resultado = _liberacaoFormacaoTremServiceRepository.ObterLiberacaoFormacaoTremExportar(
                    request.OrdemServico,
                    request.OrigemTrem,
                    request.DestinoTrem,
                    request.LocalAtual,
                    request.NomeSol,
                    request.NomeAutorizador,
                    request.DtPeriodoInicial,
                    request.DtPeriodoFinal,
                    request.SituacaoTrava);

            return ExcelHelper.Exportar(
                DateTime.Now + " SolicitacoesLiberacao.xlsx",
                colunas,
                resultado,
                ws =>
                {
                    ws.Cells.AutoFitColumns();
                    //ws.Column(16).Width = 25; // Observacões
                    //ws.Column(83).Width = 40; // Dados faltando
                });
        }

        /// <summary>
        /// Pesquisa para TIMMER Liberação Trem
        /// </summary>
        /// <returns></returns>
        public JsonResult ConsultaTimmerFormacaoTrem()
        {
            try
            {
                var resultado = _liberacaoFormacaoTremServiceRepository.ObterTimmerConsultaLiberacaoFormacaoTrem();

                return Json(resultado);
            }
            catch (Exception e)
            {
                return Json(new
                {
                    Success = false,
                    e.Message,
                    Status = 400 // Bad Request
                });
            }
        }
    }
}

