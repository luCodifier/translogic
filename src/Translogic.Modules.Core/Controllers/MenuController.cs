namespace Translogic.Modules.Core.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using System.Web.Security;
    using App_GlobalResources;
    using Domain.Model.Acesso;
    using Domain.Model.Acesso.DTO;
    using Domain.Model.IntegracaoSIV;
    using Domain.Services.Acesso;
    using Helpers;
    using Translogic.Core.Infrastructure;

    /// <summary>
    /// Controller respons�vel pelas opera��es de Menu
    /// </summary>
    public class MenuController : BaseSecureModuleController
    {
        private readonly MenuService _menuService;

        /// <summary>
        /// Construtor injetando o servi�o
        /// </summary>
        /// <param name="menuService">Servi�o do Menu</param>
        /// <param name="configuracaoTranslogicRepository">Reposit�rio das configura��es gerais do translogic injetado</param>
        public MenuController(MenuService menuService)
        {
            _menuService = menuService;
        }

        /// <summary>
        /// Obt�m o menu de acordo com a tela que ser� aberta
        /// </summary>
        /// <param name="codigoTela">C�digo da tela que est� sendo aberta</param>
        /// <param name="sistemaOrigem">Sistema Origem da tela que est� sendo aberta</param>
        /// <returns>Resultado Json</returns>
        public virtual JsonResult ObterMenuParaTela(int codigoTela, string sistemaOrigem)
        {
            if ("SIV".Equals(sistemaOrigem))
            {
                return ObterSubMenu(codigoTela, sistemaOrigem, true);
            }

            if (sistemaOrigem.StartsWith("GIS"))
            {
                return ObterSubMenu(102, "SIV", true);
            }

            Menu menu = _menuService.ObterItensAtivos(UsuarioAtual.Codigo).Where(m => m.CodigoTela.HasValue && m.CodigoTela.Value.Equals(codigoTela)).First();
            if (menu.Pai == null)
            {
                throw new TranslogicException("N�o foi poss�vel carregar o menu para a tela {0}", codigoTela.ToString());
            }

            var pais = new Stack<Menu>();

            pais.Push(menu.Pai);

            while (true)
            {
                Menu pai = pais.Peek().Pai;

                if (pai == null)
                {
                    break;
                }

                pais.Push(pai);
            }

            if (pais.Count < 2)
            {
                throw new TranslogicException("O c�digo de tela informado deve estar no m�nimo no terceiro n�vel de menus.");
            }

            return ObterSubMenu(pais.ToList()[1].Id, sistemaOrigem, true);
        }

        /// <summary>
        /// Obt�m itens de menu, dado um sub-menu
        /// </summary>
        /// <param name="pai">Id do menu pai</param>
        /// <param name="sistemaOrigem">Sistem Origem</param>
        /// <param name="itemMenu">Indica se e um item de menu</param>
        /// <returns>Resultado Json</returns>
        public virtual JsonResult ObterSubMenu(int pai, string sistemaOrigem, bool itemMenu)
        {
            if ("SIV".Equals(sistemaOrigem))
            {
                return Json(_menuService.ItemMenuSivToSimples(pai, itemMenu, UsuarioAtual.Codigo));
            }

            return Json(_menuService.Transformar(_menuService.ObterItensSubMenu(pai, UsuarioAtual.Codigo)));
        }

        /// <summary>
        /// Adiciona o menu no MeuTranslogic
        /// </summary>
        /// <param name="codigoTela">Tela a ser adicionada</param>
        /// <param name="sistemaOrigem">Sistema de Origem da Tela a ser adicionada</param>
        public void AdicionarItemMeuTranslogic(int codigoTela, string sistemaOrigem)
        {
            Menu tela = null;
            ItemMenuSiv itmMenuSiv = null;

            if (sistemaOrigem.ToUpper().StartsWith("S"))
            {
                itmMenuSiv = _menuService.ObterItemSivPorCodigoTela(codigoTela, UsuarioAtual.Codigo);
                itmMenuSiv.Id = codigoTela;
            }
            else
            {
                tela = _menuService.ObterPorCodigoTela(codigoTela, UsuarioAtual.Codigo);
            }

            /*		    if (tela == null)
                        {
                            throw new ArgumentException(string.Format("N�o foi poss�vel encontrar uma tela com o c�digo {0}", codigoTela), "codigoTela");
                        }*/
            if (tela != null || itmMenuSiv != null)
            {
                _menuService.AdicionarMenuAoMeuTranslogic(UsuarioAtual, tela, itmMenuSiv);
            }
        }

        /// <summary>
        /// Remove o menu do menu translogic
        /// </summary>
        /// <param name="id">Id do meu translogic</param>
        public void RemoverItemMeuTranslogic(int? id)
        {
            if (!id.HasValue)
            {
                throw new ArgumentException("N�o foi poss�vel encontrar o item do Meu Translogic para remover.");
            }

            _menuService.RemoverMenuMeuTranslogic(id.Value);
        }

        /// <summary>
        /// Obtem os itens do MeuTranslogic
        /// </summary>
        /// <returns>Lista de Itens do meu translogic</returns>
        public IList<MeuTranslogicSimples> ObterMeuTranslogic()
        {
            return _menuService.ObterMeuTranslogic(UsuarioAtual);
        }

        /// <summary>
        /// Carrega a lista de itens do meu translogic
        /// </summary>
        /// <returns>Resultado Json</returns>
        public JsonResult CarregarMeuTranslogic()
        {
            return Json(ObterMeuTranslogic());
        }

        /// <summary>
        /// M�todo que obt�m a lista de menus dado uma string a ser pesquisada
        /// </summary>
        /// <param name="query">String de pesquisa</param>
        /// <returns>Resultado Json</returns>
        public JsonResult PesquisaTela(string query)
        {
            IList<Menu> telas = _menuService.PesquisarTela(query, UsuarioAtual.Codigo);

            IList<MenuSimples> telasSimples = _menuService.Transformar(telas, 1, 1);

            IList<ItemMenuSiv> menus = _menuService.PesquisarSivTela(query, UsuarioAtual.Codigo);

            foreach (ItemMenuSiv itemMenuSiv in menus)
            {
                var simples = new MenuSimples
                                  {
                                      Id = Convert.ToInt32(itemMenuSiv.Id),
                                      CodigoTela = Convert.ToInt32(itemMenuSiv.Id),
                                      Titulo = "SIV:" + itemMenuSiv.Nome,
                                      SistemaOrigem = "SIV",
                                      Filhos = new List<MenuSimples>()
                                  };
                telasSimples.Add(simples);
            }

            return Json(
                new
                {
                    items = telasSimples,
                    total = telasSimples.Count
                });
        }

        /// <summary>
        /// M�todo que obt�m os dados da tela pelo c�digo informado
        /// </summary>
        /// <param name="codigoTela">C�digo da Tela</param>
        /// <param name="sistemaOrigem">Sistema Origem da Tela</param>
        /// <returns>Resultado Json</returns>
        public JsonResult ObterTela(string codigoTela, string sistemaOrigem)
        {
            //// verifica se esta dentro do servidor do Restri��o
            if (Request.Url != null && _menuService.VerificarServidorRestricao(Request.Url.Host.ToLower()))
            {
                if (!codigoTela.ToUpper().Contains("S") && (_menuService.VerificarTelaRestricao(codigoTela)))
                {
                    return Json(new { success = false, message = Acesso.TelaForaDoRestricao });
                }
            }

            //// verifica se est� dentro do servidor do SIV
            if (Request.Url != null && _menuService.VerificarServidorSiv(Request.Url.Host.ToLower()))
            {
                if (!codigoTela.ToUpper().StartsWith("S"))
                {
                    if (_menuService.VerificarTelaRestricao(codigoTela))
                    {
                        return Json(new { success = false, message = Acesso.TelaForaDoSIV });
                    }
                }
            }

            codigoTela = sistemaOrigem + codigoTela;
            //// int?
            if (!(codigoTela != null && !string.Empty.Equals(codigoTela)))
            {
                //// if (!codigoTela.HasValue)   
                return Json(new { success = false, message = Acesso.InformarCodigoTela });
            }

            MenuSimples simp = null;
            var tela = new Menu();

            if (codigoTela.ToUpper().StartsWith("GIS"))
            {
                if (!_menuService.VerificarAcessoTelaSiv(102, UsuarioAtual.Codigo))
                {
                    return Json(new { success = false, message = Resources.Acesso.TelaNaoEncontrada });
                }

                //// chamada gis-mapas via siv
                simp = new MenuSimples();
                simp.Path = Url.SivGis(Request.Cookies[FormsAuthentication.FormsCookieName].Value);
                simp.Titulo = "MAPAS";
                simp.SistemaOrigem = "GIS-MAPAS";
                simp.CodigoTela = 0;
            }

            if (codigoTela.ToUpper().StartsWith("S"))
            {
                int auxiliar = 0;

                string codigo = codigoTela.ToUpper().Replace("SIV", string.Empty).Replace("S", string.Empty);

                if (!int.TryParse(codigo, out auxiliar))
                {
                    return Json(new { success = false, message = Acesso.TelaNaoEncontrada });
                }

                int idTela = Convert.ToInt32(codigo);
                //// chamada siv. 
                if (!_menuService.VerificarAcessoTelaSiv(idTela, UsuarioAtual.Codigo))
                {
                    return Json(new { success = false, message = Resources.Acesso.TelaNaoEncontrada });
                }

                simp = new MenuSimples();
                codigoTela = codigoTela.ToUpper().Replace("SIV", "S");
                simp.Path = Url.Siv(idTela, Request.Cookies[FormsAuthentication.FormsCookieName].Value);
                simp.Titulo = "SIV";
                simp.SistemaOrigem = "SIV";
                simp.CodigoTela = idTela;
            }

            if (simp == null)
            {
                int auxiliar = 0;

                if (!int.TryParse(codigoTela, out auxiliar))
                {
                    return Json(new { success = false, message = Acesso.TelaNaoEncontrada });
                }

                tela = _menuService.ObterPorCodigoTela(Convert.ToInt32(codigoTela), UsuarioAtual.Codigo);
                //// codigoTela.Value
            }

            if (tela == null)
            {
                return Json(new { success = false, message = Acesso.TelaNaoEncontrada });
            }

            MenuSimples simples = null;

            if (simp != null)
            {
                simples = simp;
            }
            else
            {
                simples = _menuService.ToSimples(tela);

                string tema = (string)Session["TEMA_TELA"];
                string codigoTema = string.Empty;
                if (tema != null)
                {
                    codigoTema = tema;
                }

                simples.Path = tela.Legado ? Url.Legado(tela.Path, Request.Cookies[FormsAuthentication.FormsCookieName].Value, codigoTema) : tela.Path.Replace("~/", string.Empty);
            }

            if (UsuarioAtual != null && tela != null && simples != null)
            {
                Sis.Log(tela.Titulo, Spd.Data.Enums.EnumLogTipo.Page, tela.Path, tela.CodigoTela, UsuarioAtual.ToString(), null, null, UsuarioAtual.ToString());
            }


            return Json(new { success = true, tela = simples });
        }
    }
}
