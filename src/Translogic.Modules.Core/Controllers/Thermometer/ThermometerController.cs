﻿namespace Translogic.Modules.Core.Controllers.Thermometer
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using Domain.Model.Thermometer;
    using Domain.Services.Thermometer;
    using Translogic.Core.Infrastructure.Web.ContentResults;
    using Translogic.Modules.Core.Domain.Model.Diversos;

    /// <summary>
    /// Controller Termômetro
    /// </summary>
    public class ThermometerController : BaseSecureModuleController
    {
        private readonly ThermometerService _thermometerService;

        /// <summary>
        /// Contrutor Defaut
        /// </summary>
        /// <param name="thermometerService">Serviço do Termometro</param>
        public ThermometerController(ThermometerService thermometerService)
        {
            _thermometerService = thermometerService;
        }

        /// <summary>
        /// Acao Padrao ao ser chamado o controller
        /// </summary>
        /// <returns>View Padrao</returns>
        // [Autorizar(Transacao = "TERMOMETRO")]
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Método para obter as estações
        /// </summary>
        /// <returns> retorna lista de termômetros </returns>
        public JsonResult ObterTermometros()
        {
            IList<Thermometer> termometros = _thermometerService.ObterTermometros();

            return Json(new
            {
                totalCount = termometros.Count,
                Items = termometros.OrderBy(x => x.NomeTermometro).Select(x => new
                {
                    x.Id,
                    x.NomeTermometro
                })
            });
        }

        /// <summary>
        /// Obtém as medições de um determinado termômetro
        /// </summary>
        /// <returns> Resultado JSON </returns>
        /// <param name="idTermometro">Identificador do termômetro</param>
        /// <param name="dataInicial">Data Incicial de pesquisa</param>
        /// <param name="dataFinal">Data Final de pesquisa</param>
        /// <returns></returns>
        public JsonResult ObterLeiturasTermometro(int idTermometro, DateTime dataInicial, DateTime dataFinal)
        {
            int? rangePesquisa = null;
            int? minimaRestricao = null;
            int? minimaRonda = null;
            int? minimaInterdicao = null;
            int? maximaRestricao = null;
            int? maximaRonda = null;
            int? maximanterdicao = null;

            Thermometer thermometer = _thermometerService.ObterMedicoesTermometros(idTermometro, dataInicial, dataFinal);

            if (thermometer == null)
            {
                return Json(new
                {
                    Erro = true,
                    Mensagem = "Não há dados para o período pesquisado!"
                });
            }

            // Se houver muitas medições, remove algumas da lista para o gráfico não ficar tão pesado
            if (thermometer.ListaMedicoes.Count > 5000)
            {
                thermometer.ListaMedicoes = RefatorarLista(thermometer.ListaMedicoes.ToList());
            }

            if (thermometer.ListaMedicoes != null)
            {
                DateTime primeiroDia = thermometer.ListaMedicoes.Min(x => x.DataLeitura);
                DateTime ultimoDia = thermometer.ListaMedicoes.Max(x => x.DataLeitura);

                rangePesquisa = (ultimoDia - primeiroDia).Days;
            }

            if (thermometer.ListaRegion != null)
            {
                minimaRestricao = thermometer.ListaRegion.Where(x => x.TermometroAtivo).SelectMany(x => x.ListaMinimasTemperaturas.Where(y => y.TipoRestricao == RestricaoEnum.Restricao)).Select(x => x.Temperatura).DefaultIfEmpty().Max();
                minimaRonda = thermometer.ListaRegion.Where(x => x.TermometroAtivo).SelectMany(x => x.ListaMinimasTemperaturas.Where(y => y.TipoRestricao == RestricaoEnum.Ronda)).Select(x => x.Temperatura).DefaultIfEmpty().Max();
                minimaInterdicao = thermometer.ListaRegion.Where(x => x.TermometroAtivo).SelectMany(x => x.ListaMinimasTemperaturas.Where(y => y.TipoRestricao == RestricaoEnum.Interdicao)).Select(x => x.Temperatura).DefaultIfEmpty().Max();

                maximaRestricao = thermometer.ListaRegion.Where(x => x.TermometroAtivo).SelectMany(x => x.ListaMaximasTemperaturas.Where(y => y.TipoRestricao == RestricaoEnum.Restricao)).Select(x => x.Temperatura).DefaultIfEmpty().Min();
                maximaRonda = thermometer.ListaRegion.Where(x => x.TermometroAtivo).SelectMany(x => x.ListaMaximasTemperaturas.Where(y => y.TipoRestricao == RestricaoEnum.Ronda)).Select(x => x.Temperatura).DefaultIfEmpty().Min();
                maximanterdicao = thermometer.ListaRegion.Where(x => x.TermometroAtivo).SelectMany(x => x.ListaMaximasTemperaturas.Where(y => y.TipoRestricao == RestricaoEnum.Interdicao)).Select(x => x.Temperatura).DefaultIfEmpty().Min();
            }

            return Json(new
            {
                total = thermometer.ListaMedicoes.Count,
                DiferencaDeDias = rangePesquisa,
                MinimaRestricao = minimaRestricao,
                MinimaRonda = minimaRonda,
                MinimaInterdicao = minimaInterdicao,
                MaximaRestricao = maximaRestricao,
                MaximaRonda = maximaRonda,
                MaximaInterdicao = maximanterdicao,
                Items = thermometer.ListaMedicoes.Select(x => new
                {
                    DataLeitura = x.DataLeitura.ToString("MM-dd-yy HH:mm"),
                    x.TemperaturaTermometro
                })
            });
        }

        /// <summary>
        /// Obtém as medições de um determinado termômetro
        /// </summary>
        /// <returns> Retorna um relatório em excel com as leituras de um determinado termômetro </returns>
        /// <param name="idTermometro">Identificador do termômetro</param>
        /// <param name="dataInicial">Data Incicial de pesquisa</param>
        /// <param name="dataFinal">Data Final de pesquisa</param>
        /// <returns></returns>
        public ActionResult ExportarLeiturasTermometro(int idTermometro, DateTime dataInicial, DateTime dataFinal)
        {
            string nomeArquivo = string.Empty;

            Thermometer thermometer = _thermometerService.ObterMedicoesTermometros(idTermometro, dataInicial, dataFinal);

            nomeArquivo = thermometer != null ? thermometer.NomeTermometro : "Relatorio";

            thermometer = thermometer ?? new Thermometer();
            thermometer.ListaMedicoes = thermometer.ListaMedicoes ?? new List<ReadThermometer>();

            Dictionary<string, Func<ReadThermometer, string>> dic = new Dictionary<string, Func<ReadThermometer, string>>();

            dic["Data Leitura"] = t => t.DataLeitura.ToString("dd/MM/yyyy H:mm");
            dic["Temperatura"] = t => t.TemperaturaTermometro.ToString();

            return this.Excel(string.Format("{0}.xls", nomeArquivo), dic, thermometer.ListaMedicoes.ToList());
        }

        private IList<ReadThermometer> RefatorarLista(IList<ReadThermometer> medicoes)
        {
            IList<ReadThermometer> listaMedicoes = new List<ReadThermometer>();

            var groupBy10Minutes = medicoes.GroupBy(a => new DateTime(a.DataLeitura.Year, a.DataLeitura.Month, a.DataLeitura.Day, a.DataLeitura.Hour, a.DataLeitura.Minute - (a.DataLeitura.Minute % 10), 0));

            foreach (var groupBy10Minute in groupBy10Minutes)
            {
                ReadThermometer readThermometer = new ReadThermometer();
                readThermometer.DataLeitura = groupBy10Minute.Key;
                readThermometer.TemperaturaTermometro = (int)groupBy10Minute.Average(a => a.TemperaturaTermometro);

                listaMedicoes.Add(readThermometer);
            }

            return listaMedicoes;
        }
    }
}
