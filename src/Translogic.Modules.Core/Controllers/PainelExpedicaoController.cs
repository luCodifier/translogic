﻿namespace Translogic.Modules.Core.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Web.Mvc;
    using ALL.Core.Extensions;
    using Translogic.Core.Infrastructure.Web;
    using Translogic.Core.Infrastructure.Web.ContentResults;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Modules.Core.Domain.Model.PainelExpedicao;
    using Translogic.Modules.Core.Domain.Model.PainelExpedicao.Parametrizacao;
    using Translogic.Modules.Core.Domain.Model.PainelExpedicao.Parametrizacao.Enums;
    using Translogic.Modules.Core.Domain.Model.Via;
    using Translogic.Modules.Core.Domain.Services.MotivoSituacaoVagao;
    using Translogic.Modules.Core.Domain.Services.PainelExpedicao;
    using Translogic.Modules.Core.Domain.Services.PainelExpedicao.Relatorio;
    using Translogic.Modules.Core.Infrastructure;
    using Translogic.Modules.Core.Util;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Conteiner;
    using Translogic.Modules.Core.Domain.Services.FluxosComerciais;
    using System.Text.RegularExpressions;
    using Translogic.Modules.Core.Domain.Model.Via.Repositories;
    using Translogic.Modules.Core.Domain.Services.Terminal;
    using Translogic.Modules.Core.Domain.Services.Terminal.Interface;
    using System.Diagnostics;

    using ExcelLibrary.SpreadSheet;
    using ICSharpCode.SharpZipLib.Core;
    using Microsoft.Practices.ServiceLocation;
    using Translogic.Modules.Core.Domain.Services.Acesso;
    using Translogic.Modules.Core.Spd.Data;
    using Translogic.Modules.Core.Domain.Model.Codificador;
    using Translogic.Modules.Core.Domain.Model.Codificador.Repositories;

    public class PainelExpedicaoController : BaseSecureModuleController
    {
        private readonly PainelExpedicaoService _painelExpedicaoService;
        private readonly MotivoSituacaoVagaoService _motivoSituacaoVagaoService;
        private readonly ITerminalService _terminalService;
        private readonly CarregamentoService _carregamentoService;
        private readonly IAreaOperacionalRepository _areaOperacionalRepository;
        private readonly IConfiguracaoTranslogicRepository _configuracaoTranslogicRepository;

        public PainelExpedicaoController(PainelExpedicaoService painelExpedicaoService, MotivoSituacaoVagaoService motivoSituacaoVagaoService, CarregamentoService carregamentoService, IAreaOperacionalRepository areaOperacionalRepository, ITerminalService terminalService, IConfiguracaoTranslogicRepository configuracaoTranslogicRepository)
        {
            _painelExpedicaoService = painelExpedicaoService;
            _motivoSituacaoVagaoService = motivoSituacaoVagaoService;
            _carregamentoService = carregamentoService;
            _areaOperacionalRepository = areaOperacionalRepository;
            _terminalService = terminalService;
            _configuracaoTranslogicRepository = configuracaoTranslogicRepository;
        }

        #region ActionResults

        /// <summary>
        /// Painel de Expedição - Visualização
        /// </summary>
        public ActionResult Visualizacao()
        {
            return View();
        }

        /// <summary>
        /// Painel de Expedição - Pedra
        /// </summary>
        public ActionResult PedraStatus()
        {
            return View();
        }

        /// <summary>
        /// Painel de Expedição - Pedra Resumo 
        /// </summary>
        public ActionResult PedraStatusResumo()
        {
            return View();
        }

        /// <summary>
        /// Painel de Expedição - Parametrização
        /// </summary>
        public ActionResult Parametrizacao()
        {
            return View();
        }

        /// <summary>
        /// Painel de Expedição - Consulta
        /// </summary>
        public ActionResult Consultar()
        {
            return View();
        }

        /// <summary>
        /// Painel de Expedição - Consulta de Documentos
        /// </summary>
        /// <returns></returns>
        public ActionResult ConsultarDocumentos()
        {
            return View();
        }

        /// <summary>
        /// Painel de Expedição - Conferência de Arquivos
        /// </summary>
        public ActionResult ConferenciaArquivos()
        {

            ViewData["PerfilAcessoPainelExpedicaoTitulo"] = "";

            PainelExpedicaoPerfilAcessoEnum perfilAcessoUsuario =
                _painelExpedicaoService.ObterPerfilUsuarioPainelExpedicao(UsuarioAtual);

            ViewData["PerfilAcessoCentralFaturamento"] = "false";
            ViewData["PerfilAcessoCentralFaturamentoSuper"] = "false";

            if (perfilAcessoUsuario == PainelExpedicaoPerfilAcessoEnum.Patio)
                ViewData["PerfilAcessoPainelExpedicaoTitulo"] = " - Pátio";
            else if ((perfilAcessoUsuario == PainelExpedicaoPerfilAcessoEnum.Central)
                     ||
                      (perfilAcessoUsuario == PainelExpedicaoPerfilAcessoEnum.CentralSuper)
                    )
            {
                ViewData["PerfilAcessoCentralFaturamento"] = "true";
                ViewData["PerfilAcessoPainelExpedicaoTitulo"] = " - Central de Faturamento";
                if (perfilAcessoUsuario == PainelExpedicaoPerfilAcessoEnum.CentralSuper)
                    ViewData["PerfilAcessoCentralFaturamentoSuper"] = "true";
            }

            ViewData["Usuario"] = !String.IsNullOrEmpty(UsuarioAtual.Nome) ? UsuarioAtual.Nome : UsuarioAtual.Codigo;

            return View();
        }

        /// <summary>
        /// Painel de Expedição - Relatório de Histórico de Faturamentos
        /// </summary>
        /// <returns></returns>
        public ActionResult HistoricoFaturamentos()
        {
            return View();
        }

        /// <summary>
        /// Retorna a tela para informar os códigos dos vagões
        /// </summary>
        /// <returns>Action result com a tela</returns>
        public ActionResult FormInformarVagoes()
        {
            return View();
        }

        #endregion

        #region JsonResults

        [Autorizar(Transacao = "PAINELEXPEDICAOVISUALIZACAO", Acao = "Pesquisar")]
        public JsonResult ObterPainelExpedicaoVisualizacao(DetalhesPaginacaoWeb pagination, string dataInicio, string uf, string origem, string origemFaturamento, string segmento, string situacao, string malhaCentral, string metricaLarga, string metricaNorte, string metricaSul)
        {

            try
            {

                DateTime dataInicialFiltro = DateTime.Now;
                if (!string.IsNullOrEmpty(dataInicio))
                    dataInicialFiltro = DateTime.ParseExact(dataInicio, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                int situacaoVisualizacaoId = Convert.ToInt32(situacao);

                var result = _painelExpedicaoService.ObterPainelExpedicaoVisualizacao(pagination,
                                                                                        dataInicialFiltro,
                                                                                        uf,
                                                                                        origem,
                                                                                        origemFaturamento,
                                                                                        segmento,
                                                                                        situacaoVisualizacaoId,
                                                                                        malhaCentral,
                                                                                        metricaLarga,
                                                                                        metricaNorte,
                                                                                        metricaSul);

                return Json(new
                {
                    result.Count,
                    Items = result.Select(d => new
                    {
                        TempoRefreshMilisegundos = d.TempoRefreshMilisegundos,
                        Origem = d.Origem,
                        OrigemDescricao = d.OrigemDescricao,
                        OrigemUF = d.OrigemUf,
                        PendenteConfirmacao = d.PendenteConfirmacaoValorCor,
                        TotalRecusadoEstacao = d.TotalRecusadoEstacaoValorCor,
                        PendenteFaturado = d.PendenteFaturadoValorCor,
                        Faturado = d.FaturadoValorCor,
                        FaturadoManual = d.FaturadoManualValorCor,
                        CtePendente = d.CtePendenteValorCor,
                        CteAutorizado = d.CteAutorizadoValorCor,
                        SituacaoVisualizacaoId = 0 //situacaoVisualizacaoId
                    })
                });
            }
            catch (Exception ex)
            {
                return this.Json(new { success = false });
            }
        }

        [Autorizar(Transacao = "PAINELEXPEDICAOPEDRARESUMO", Acao = "ALTERAR")]
        public JsonResult GravarAlteracoesPedra(string acao, string id, string campo, string valor, string pedraRealizadoId, string dataPedra, string operacao, string estacao, string terminalDescricao, string segmento, string realizado, string planejadoSade, string status)
        {

            try
            {
                bool permissaoAlterar = ServiceLocator.Current.GetInstance<ControleAcessoService>().Autorizar("PAINELEXPEDICAOPEDRARESUMO", "Alterar", UsuarioAtual);
                bool permissaoAlterarPassado = ServiceLocator.Current.GetInstance<ControleAcessoService>().Autorizar("PAINELEXPEDICAOPEDRARESUMO", "AlterarD-1", UsuarioAtual);

                // verifica se usuario tem acesso de alteração
                if (permissaoAlterar || permissaoAlterarPassado)
                {
                    // verifica se vai alterar passado
                    if ((DateTime.Now.Subtract(Convert.ToDateTime(dataPedra))).Days > 0)
                    {
                        // verifica se vai alterar dia anterior
                        if ((DateTime.Now.Subtract(Convert.ToDateTime(dataPedra))).Days == 1)
                        {
                            // verifica se vai alterar dia anterior ate 3 da madrugada
                            if (DateTime.Now.Hour < 3)
                            {
                                // verifica permissao alterar dia anterior ate 3 da madrugada
                                if (!permissaoAlterar && !permissaoAlterarPassado)
                                    return Json(new { Success = false, Message = "Usuário sem acesso para realizar alterações." }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                // verifica permissao alterar passado
                                if (!permissaoAlterarPassado)
                                    return Json(new { Success = false, Message = "Usuário sem acesso para realizar alterações." }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            return Json(new { Success = false, Message = "Fora do horarário limite para alterações." }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                else
                {
                    return Json(new { Success = false, Message = "Usuário sem acesso para realizar alterações." }, JsonRequestBehavior.AllowGet);
                }

                var saveObject = new PePedraRealizado()
                {
                    Id = !String.IsNullOrEmpty(pedraRealizadoId) ? Int32.Parse(pedraRealizadoId) : 0,
                    DataPedra = DateTime.Parse(dataPedra),
                    EstacaoOrigem = Decimal.Parse(estacao),
                    PedraEditado = campo == "Status" ? -1 : Decimal.Parse(planejadoSade),
                    Segmento = segmento,
                    Terminal = terminalDescricao.ToUpper(),
                    StatusEditado = status.ToUpper(),
                    PedraSade = 0,
                    TerminalId = 0 // provisorio, depois tem que transformar a coluna em nullab
                };

                if (saveObject.Id != 0)
                {
                    saveObject.DataAlteracao = DateTime.Now;
                    saveObject.UsuarioAlteracao = UsuarioAtual.Id != null ? UsuarioAtual.Id.Value : 0;

                    if (campo == "Status")
                        _painelExpedicaoService.AtualizarStatusEditado(saveObject);
                    else
                        _painelExpedicaoService.AtualizarPedraEditado(saveObject);
                }
                else
                {
                    saveObject.DataCadastro = DateTime.Now;
                    saveObject.UsuarioCadastro = UsuarioAtual.Id != null ? UsuarioAtual.Id.Value : 0;
                    _painelExpedicaoService.InserirPedraRealizado(saveObject);
                }
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { Success = true, Message = "Alterações realizadas com sucesso!" }, JsonRequestBehavior.AllowGet);
        }

        #region Exportar Excel
        [Autorizar(Transacao = "PAINELEXPEDICAOPEDRARESUMO", Acao = "ALTERAR")]
        public ActionResult ExportarExcelPedraDetalhe(string operacao, string regiao, string dataPedra, string uf, string origem, string estacaoFaturamento, string segmento, string ocultarPedraZerada, string telaExcelEmail)
        {

            var resultExcel = this._painelExpedicaoService.ObterPainelExpedicaoPedra(operacao, regiao, dataPedra, uf, origem, estacaoFaturamento, segmento, ocultarPedraZerada);

            var dic = new Dictionary<string, Func<VwPedraRealizado, string>>();

            dic["DATA"] = c => c.DataPedra.Value.ToString("dd/MM/yyyy");
            dic["OPERAÇÃO"] = c => c.Operacao != null ? c.Operacao : String.Empty;
            dic["REGIÃO"] = c => c.Regiao != null ? c.Regiao : String.Empty;
            dic["ESTAÇÃO"] = c => c.EstacaoFaturamento != null ? c.EstacaoFaturamento : String.Empty;
            dic["ORIGEM"] = c => c.EstacaoOrigem != null ? c.EstacaoOrigem : String.Empty;
            dic["TERMINAL"] = c => c.Terminal != null ? c.Terminal : String.Empty;
            dic["SEGMENTO"] = c => c.Segmento != null ? c.Segmento : String.Empty;
            dic["PROGRAMADO"] = c => c.Pedra.ToString();
            dic["REALIZADO"] = c => c.Realizado.ToString();
            dic["MONITORAR"] = c => c.Saldo.ToString();
            dic["%"] = c => String.Format("{0}%", c.Porcentagem);
            dic["STATUS"] = c => c.StatusEditado != null ? c.StatusEditado : String.Empty;

            return this.Excel(string.Format("PedraAnalitico{0}_{1}.xls", DateTime.Now.ToString("ddMMyyyy"), DateTime.Now.ToString("HHmmss")), dic, resultExcel);

        }

        [Autorizar(Transacao = "PAINELEXPEDICAOPEDRARESUMO", Acao = "ALTERAR")]
        public ActionResult ExportarExcelPedraRegiao(string operacao, string regiao, string dataPedra, string uf, string origem, string estacaoFaturamento, string segmento, string ocultarPedraZerada, string telaExcelEmail)
        {

            var listaAnalitico = this._painelExpedicaoService.ObterPainelExpedicaoPedra(operacao, regiao, dataPedra, uf, origem, estacaoFaturamento, segmento, ocultarPedraZerada);

            var regioesGroup = listaAnalitico.GroupBy(g => g.RegiaoId);

            List<VwPedraRealizado> analiticoRegioes = new List<VwPedraRealizado>();

            analiticoRegioes.AddRange(regioesGroup.Select(d => new VwPedraRealizado
            {
                DataPedra = d.First().DataPedra,
                Regiao = d.First().Regiao,
                PedraSade = d.Sum(p => p.Pedra),
                Realizado = d.Sum(p => p.Realizado)
            }));

            var dic = new Dictionary<string, Func<VwPedraRealizado, string>>();

            dic["DATA"] = c => c.DataPedra.Value.ToString("dd/MM/yyyy");
            dic["REGIÃO"] = c => c.Regiao != null ? c.Regiao : String.Empty;
            dic["PROGRAMADO"] = c => c.Pedra.ToString();
            dic["REALIZADO"] = c => c.Realizado.ToString();
            dic["MONITORAR"] = c => c.Saldo.ToString();
            dic["%"] = c => String.Format("{0}%", c.Porcentagem);

            return this.Excel(string.Format("PedraRegioes{0}_{1}.xls", DateTime.Now.ToString("ddMMyyyy"), DateTime.Now.ToString("HHmmss")), dic, analiticoRegioes);

        }

        [Autorizar(Transacao = "PAINELEXPEDICAOPEDRARESUMO", Acao = "ALTERAR")]
        public ActionResult ExportarExcelPedraOrigem(string operacao, string regiao, string dataPedra, string uf, string origem, string estacaoFaturamento, string segmento, string ocultarPedraZerada, string telaExcelEmail)
        {
            var listaAnalitico = this._painelExpedicaoService.ObterPainelExpedicaoPedra(operacao, regiao, dataPedra, uf, origem, estacaoFaturamento, segmento, ocultarPedraZerada);

            var origemGroup = listaAnalitico.GroupBy(g => g.EstacaoOrigemId);

            List<VwPedraRealizado> analiticoOrigens = new List<VwPedraRealizado>();

            analiticoOrigens.AddRange(origemGroup.Select(d => new VwPedraRealizado
            {
                DataPedra = d.First().DataPedra,
                EstacaoOrigem = d.First().Regiao,
                PedraSade = d.Sum(p => p.Pedra),
                Realizado = d.Sum(p => p.Realizado)
            }));

            var dic = new Dictionary<string, Func<VwPedraRealizado, string>>();

            dic["DATA"] = c => c.DataPedra.Value.ToString("dd/MM/yyyy");
            dic["ORIGEM"] = c => c.EstacaoOrigem != null ? c.EstacaoOrigem : String.Empty;
            dic["PROGRAMADO"] = c => c.Pedra.ToString();
            dic["REALIZADO"] = c => c.Realizado.ToString();
            dic["MONITORAR"] = c => c.Saldo.ToString();
            dic["%"] = c => String.Format("{0}%", c.Porcentagem);

            return this.Excel(string.Format("PedraEstacaoOrigem{0}_{1}.xls", DateTime.Now.ToString("ddMMyyyy"), DateTime.Now.ToString("HHmmss")), dic, analiticoOrigens);
        }

        [Autorizar(Transacao = "PAINELEXPEDICAOPEDRARESUMO", Acao = "ALTERAR")]
        public ActionResult ExportarExcelPedraGeral(string operacao, string regiao, string dataPedra, string uf, string origem, string estacaoFaturamento, string segmento, string ocultarPedraZerada, string telaExcelEmail)
        {

            var analiticoDetalhe = this._painelExpedicaoService.ObterPainelExpedicaoPedra(operacao, regiao, dataPedra, uf, origem, estacaoFaturamento, segmento, ocultarPedraZerada);

            List<Dictionary<string, Func<VwPedraRealizado, string>>> dicList = new List<Dictionary<string, Func<VwPedraRealizado, string>>>();

            List<List<VwPedraRealizado>> dataList = new List<List<VwPedraRealizado>>();

            var dicDetalhe = new Dictionary<string, Func<VwPedraRealizado, string>>();

            dicDetalhe["DATA"] = c => c.DataPedra.Value.ToString("dd/MM/yyyy");
            dicDetalhe["OPERAÇÃO"] = c => c.Operacao != null ? c.Operacao : String.Empty;
            dicDetalhe["REGIÃO"] = c => c.Regiao != null ? c.Regiao : String.Empty;
            dicDetalhe["ESTAÇÃO"] = c => c.EstacaoFaturamento != null ? c.EstacaoFaturamento : String.Empty;
            dicDetalhe["ORIGEM"] = c => c.EstacaoOrigem != null ? c.EstacaoOrigem : String.Empty;
            dicDetalhe["TERMINAL"] = c => c.Terminal != null ? c.Terminal : String.Empty;
            dicDetalhe["SEGMENTO"] = c => c.Segmento != null ? c.Segmento : String.Empty;
            dicDetalhe["PROGRAMADO"] = c => c.Pedra.ToString();
            dicDetalhe["REALIZADO"] = c => c.Realizado.ToString();
            dicDetalhe["MONITORAR"] = c => c.Saldo.ToString();
            dicDetalhe["%"] = c => String.Format("{0}%", c.Porcentagem);
            dicDetalhe["STATUS"] = c => c.StatusEditado != null ? c.StatusEditado : String.Empty;


            var dicRegioes = new Dictionary<string, Func<VwPedraRealizado, string>>();

            var regioesGroup = analiticoDetalhe.GroupBy(g => g.RegiaoId);

            List<VwPedraRealizado> analiticoRegioes = new List<VwPedraRealizado>();

            analiticoRegioes.AddRange(regioesGroup.Select(d => new VwPedraRealizado
            {
                DataPedra = d.First().DataPedra,
                Regiao = d.First().Regiao,
                RegiaoId = d.First().RegiaoId,
                PedraSade = d.Sum(p => p.Pedra),
                Realizado = d.Sum(p => p.Realizado)
            }));

            dicRegioes["DATA"] = c => c.DataPedra.Value.ToString("dd/MM/yyyy");
            dicRegioes["REGIÃO"] = c => c.Regiao != null ? c.Regiao : String.Empty;
            dicRegioes["PROGRAMADO"] = c => c.Pedra.ToString();
            dicRegioes["REALIZADO"] = c => c.Realizado.ToString();
            dicRegioes["MONITORAR"] = c => c.Saldo.ToString();
            dicRegioes["%"] = c => String.Format("{0}%", c.Porcentagem);

            var dicOrigens = new Dictionary<string, Func<VwPedraRealizado, string>>();

            var origemGroup = analiticoDetalhe.GroupBy(g => g.EstacaoOrigemId);

            List<VwPedraRealizado> analiticoOrigens = new List<VwPedraRealizado>();

            analiticoOrigens.AddRange(origemGroup.Select(d => new VwPedraRealizado
            {
                DataPedra = d.First().DataPedra,
                EstacaoOrigem = d.First().EstacaoOrigem,
                RegiaoId = d.First().RegiaoId,
                PedraSade = d.Sum(p => p.Pedra),
                Realizado = d.Sum(p => p.Realizado)
            }));

            dicOrigens["DATA"] = c => c.DataPedra.Value.ToString("dd/MM/yyyy");
            dicOrigens["ORIGEM"] = c => c.EstacaoOrigem != null ? c.EstacaoOrigem : String.Empty;
            dicOrigens["PROGRAMADO"] = c => c.Pedra.ToString();
            dicOrigens["REALIZADO"] = c => c.Realizado.ToString();
            dicOrigens["MONITORAR"] = c => c.Saldo.ToString();
            dicOrigens["%"] = c => String.Format("{0}%", c.Porcentagem);

            dicList.Add(dicRegioes);
            dicList.Add(dicDetalhe);
            dicList.Add(dicOrigens);

            dataList.Add(analiticoRegioes);
            dataList.Add(analiticoDetalhe.ToList());
            dataList.Add(analiticoOrigens);

            var fileName = string.Format("PedraGeral{0}_{1}.xls", DateTime.Now.ToString("ddMMyyyy"), DateTime.Now.ToString("HHmmss"));

            return ExcelPedraCustom(fileName, dicList, dataList);

        }

        #endregion

        [Autorizar(Transacao = "PAINELEXPEDICAOPEDRARESUMO", Acao = "ALTERAR")]
        public ActionResult EmailPedraDetalhe(string operacao, string regiao, string dataPedra, string uf, string origem, string estacaoFaturamento, string segmento, string ocultarPedraZerada)
        {

            this._painelExpedicaoService.EnviaEmailDetalhe(operacao, regiao, dataPedra, uf, origem, estacaoFaturamento, segmento, ocultarPedraZerada);

            return this.Json(new { success = true });

        }

        [Autorizar(Transacao = "PAINELEXPEDICAOPEDRARESUMO", Acao = "ALTERAR")]
        public ActionResult EmailPedraRegiao(string operacao, string regiao, string dataPedra, string uf, string origem, string estacaoFaturamento, string segmento, string ocultarPedraZerada)
        {

            this._painelExpedicaoService.EnviaEmailRegiao(operacao, regiao, dataPedra, uf, origem, estacaoFaturamento, segmento, ocultarPedraZerada);

            return this.Json(new { success = true });

        }

        [Autorizar(Transacao = "PAINELEXPEDICAOPEDRARESUMO", Acao = "ALTERAR")]
        public ActionResult EmailPedraOrigem(string operacao, string regiao, string dataPedra, string uf, string origem, string estacaoFaturamento, string segmento, string ocultarPedraZerada)
        {

            this._painelExpedicaoService.EnviaEmailOrigem(operacao, regiao, dataPedra, uf, origem, estacaoFaturamento, segmento, ocultarPedraZerada);

            return this.Json(new { success = true });

        }

        [Autorizar(Transacao = "PAINELEXPEDICAOPEDRARESUMO", Acao = "ALTERAR")]
        public ActionResult EmailPedraGeral(string operacao, string regiao, string dataPedra, string uf, string origem, string estacaoFaturamento, string segmento, string ocultarPedraZerada, string telaExcelEmail)
        {

            this._painelExpedicaoService.EnviaEmailGeral(operacao, regiao, dataPedra, uf, origem, estacaoFaturamento, segmento, ocultarPedraZerada);

            return this.Json(new { success = true });

        }

        [Autorizar(Transacao = "PAINELEXPEDICAOPEDRARESUMO", Acao = "Pesquisar")]
        public JsonResult ObterPainelExpedicaoPedra(string operacao, string regiao, string dataPedra, string uf, string origem, string estacaoFaturamento, string segmento, string ocultarPedraZerada, string telaExcelEmail)
        {

            try
            {
                ConfiguracaoTranslogic intervaloPedra = _configuracaoTranslogicRepository.ObterPorId("INTERVALO_ATUALIZACAO_PEDRA");

                bool permissaoAlterar = ServiceLocator.Current.GetInstance<ControleAcessoService>().Autorizar("PAINELEXPEDICAOPEDRARESUMO", "Alterar", UsuarioAtual);
                bool permissaoAlterarPassado = ServiceLocator.Current.GetInstance<ControleAcessoService>().Autorizar("PAINELEXPEDICAOPEDRARESUMO", "AlterarD-1", UsuarioAtual);

                var listaAnalitico = _painelExpedicaoService.ObterPainelExpedicaoPedra(operacao, regiao, dataPedra, uf, origem, estacaoFaturamento, segmento, ocultarPedraZerada);

                List<VwPedraRealizado> sumarioAnalitico = new List<VwPedraRealizado>();

                sumarioAnalitico.Add(new VwPedraRealizado
                {
                    PedraSade = listaAnalitico.Sum(s => s.Pedra),
                    Realizado = listaAnalitico.Sum(r => r.Realizado)
                });

                var regioesGroup = listaAnalitico.GroupBy(g => g.RegiaoId);

                List<VwPedraRealizado> analiticoRegioes = new List<VwPedraRealizado>();

                analiticoRegioes.AddRange(regioesGroup.Select(d => new VwPedraRealizado
                {
                    DataPedra = d.First().DataPedra,
                    Regiao = d.First().Regiao,
                    PedraSade = d.Sum(p => p.Pedra),
                    Realizado = d.Sum(p => p.Realizado)
                }));

                List<VwPedraRealizado> sumarioRegioes = new List<VwPedraRealizado>();

                sumarioRegioes.Add(new VwPedraRealizado
                {
                    Regiao = "TOTAL",
                    PedraSade = analiticoRegioes.Sum(p => p.Pedra),
                    Realizado = analiticoRegioes.Sum(p => p.Realizado)
                });

                var origensGroup = listaAnalitico.GroupBy(g => g.EstacaoOrigemId);

                List<VwPedraRealizado> analiticoOrigens = new List<VwPedraRealizado>();

                analiticoOrigens.AddRange(origensGroup.Select(d => new VwPedraRealizado
                {
                    DataPedra = d.First().DataPedra,
                    EstacaoOrigem = d.First().EstacaoOrigem,
                    PedraSade = d.Sum(p => p.Pedra),
                    Realizado = d.Sum(p => p.Realizado)
                }));

                List<VwPedraRealizado> sumarioOrigens = new List<VwPedraRealizado>();

                sumarioOrigens.Add(new VwPedraRealizado
                {
                    Regiao = "TOTAL",
                    PedraSade = analiticoOrigens.Sum(p => p.Pedra),
                    Realizado = analiticoOrigens.Sum(p => p.Realizado)
                });

                var first = listaAnalitico.FirstOrDefault();

                string dataPedraString = first != null ? first.DataPedra.Value.ToString("dd/MM/yyyy") : String.Empty;

                string UltimaAtualizacao = first != null ? first.RpTimestamp.Value.ToString("HH:mm") : String.Empty;
                string ProximaAtualizacao = first != null ? first.RpTimestamp.Value.AddMinutes(Convert.ToDouble(intervaloPedra.Valor)).ToString("HH:mm") : String.Empty;

                return Json(new
                {
                    UltimaAtualizacao = UltimaAtualizacao,
                    ProximaAtualizacao = ProximaAtualizacao,
                    Contagem = listaAnalitico.Count,
                    PermissaoExportar = permissaoAlterar || permissaoAlterarPassado,
                    Analitico = listaAnalitico.Select(d => new
                    {
                        PedraRealizadoId = d.PedraRealizadoId,
                        TempoRefreshMilisegundos = 6000,
                        DataPedra = dataPedraString,
                        Operacao = d.Operacao,
                        EstacaoDescricao = d.EstacaoFaturamento,
                        Origem = d.EstacaoOrigemId,
                        OrigemDescricao = d.EstacaoOrigem,
                        TerminalDescricao = d.Terminal,
                        Regiao = d.RegiaoId,
                        RegiaoDescricao = d.Regiao,
                        Segmento = d.Segmento,
                        SegmentoDescricao = d.Segmento,
                        PlanejadoSade = d.Pedra,
                        Realizado = d.Realizado,
                        Saldo = d.Saldo,
                        Porcentagem = d.Porcentagem,
                        Status = d.StatusEditado,
                        PedraEditadaFlag = d.PedraEditadaFlag,
                        PermiteEditarPedra = d.PedraSade == 0
                    }),
                    SumarioAnalitico = sumarioAnalitico.Select(d => new
                    {
                        Id = 0,
                        DataPedra = dataPedraString,
                        Operacao = "",
                        RegiaoDescricao = "",
                        EstacaoDescricao = "",
                        OrigemDescricao = "",
                        TerminalDescricao = "",
                        SegmentoDescricao = "TOTAL: ",
                        PlanejadoSade = d.Pedra,
                        Realizado = d.Realizado,
                        Saldo = d.Saldo,
                        Porcentagem = d.Porcentagem,
                        Status = String.Format("{0}% da Meta Realizada", d.Porcentagem)
                    }),
                    SinteticoRegioes = analiticoRegioes.Select(d => new
                    {
                        Id = 0,
                        DataPedra = dataPedraString,
                        Regiao = d.Regiao,
                        Planejado = d.Pedra,
                        Realizado = d.Realizado,
                        Saldo = d.Saldo,
                        Porcentagem = d.Porcentagem
                    }),
                    SumarioRegioes = sumarioRegioes.Select(d => new
                    {
                        Id = 0,
                        DataPedra = dataPedraString,
                        Regiao = "TOTAL",
                        Planejado = d.Pedra,
                        Realizado = d.Realizado,
                        Saldo = d.Saldo,
                        Porcentagem = d.Porcentagem,
                        Status = String.Format("{0}% da Meta Realizada", d.Porcentagem)
                    }),
                    SinteticoOrigens = analiticoOrigens.Select(d => new
                    {
                        Id = 0,
                        DataPedra = dataPedraString,
                        Origem = d.EstacaoOrigem,
                        Planejado = d.Pedra,
                        Realizado = d.Realizado,
                        Saldo = d.Saldo,
                        Porcentagem = d.Porcentagem
                    }),
                    SumarioOrigens = sumarioOrigens.Select(d => new
                    {
                        Id = 0,
                        DataPedra = dataPedraString,
                        Origem = "TOTAL",
                        Planejado = d.Pedra,
                        Realizado = d.Realizado,
                        Saldo = d.Saldo,
                        Porcentagem = d.Porcentagem,
                        Status = String.Format("{0}% da Meta Realizada", d.Porcentagem)
                    })
                });
            }
            catch (Exception ex)
            {
                return this.Json(new { success = false, Mensagem = ex.Message });
            }
        }

        [Autorizar(Transacao = "PAINELEXPEDICAOCONSULTA", Acao = "Pesquisar")]
        public ActionResult ObterPainelExpedicaoConsulta(DetalhesPaginacaoWeb pagination, string dataInicio, string dataFinal, string fluxo, string origem, string destino, string vagoes, string situacao, string lotacao, string localAtual, string cliente, string terminalFaturamento, string mercadorias, string segmento, bool exportarExcel)
        {

            try
            {
                var dataInicialFiltro = DateTime.Now;
                var dataFinalFiltro = DateTime.Now;

                if (!string.IsNullOrEmpty(dataInicio))
                    dataInicialFiltro = DateTime.ParseExact(dataInicio, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                if (!string.IsNullOrEmpty(dataFinal))
                    dataFinalFiltro = DateTime.ParseExact(dataFinal, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                if (exportarExcel)
                {
                    segmento = String.IsNullOrEmpty(segmento) ? String.Empty : segmento;

                    var resultExcel = this._painelExpedicaoService.ObterPainelExpedicaoConsulta(
                                                                            null,
                                                                            dataInicialFiltro,
                                                                            dataFinalFiltro,
                                                                            fluxo,
                                                                            origem,
                                                                            destino,
                                                                            vagoes,
                                                                            situacao,
                                                                            lotacao,
                                                                            localAtual,
                                                                            cliente,
                                                                            terminalFaturamento,
                                                                            mercadorias,
                                                                            segmento);

                    var dic = new Dictionary<string, Func<PainelExpedicaoConsultaDto, string>>();

                    dic["Data Faturamento"] = c => (c.Data.HasValue ? c.Data.Value.ToString() : String.Empty);
                    dic["Série"] = c => !String.IsNullOrEmpty(c.SerieVagao) ? c.SerieVagao : String.Empty;
                    dic["Vagão"] = c => !String.IsNullOrEmpty(c.Vagao) ? c.Vagao : String.Empty;
                    dic["Lotação"] = c => !String.IsNullOrEmpty(c.Lotacao) ? c.Lotacao : String.Empty;
                    dic["Situação"] = c => !String.IsNullOrEmpty(c.Situacao) ? c.Situacao : String.Empty;
                    dic["TU"] = c => c.PesoTb;
                    dic["Tara"] = c => c.PesoTt;
                    dic["TB"] = c => c.PesoTb;
                    dic["MD"] = c => (String.IsNullOrEmpty(c.MultiploDespacho) || c.MultiploDespacho == "NAO") ? "Não" : "Sim";
                    dic["Fluxo"] = c => !String.IsNullOrEmpty(c.Fluxo) ? c.Fluxo : String.Empty;
                    dic["Origem"] = c => !String.IsNullOrEmpty(c.Origem) ? c.Origem : String.Empty;
                    dic["Destino"] = c => !String.IsNullOrEmpty(c.Destino) ? c.Destino : String.Empty;
                    dic["Expedidor"] = c => !String.IsNullOrEmpty(c.TerminalFaturamento) ? c.TerminalFaturamento : String.Empty;
                    dic["Cliente"] = c => !String.IsNullOrEmpty(c.Cliente) ? c.Cliente : String.Empty;
                    dic["Mercadoria"] = c => !String.IsNullOrEmpty(c.Mercadoria) ? c.Mercadoria : String.Empty;
                    dic["Container"] = c => !String.IsNullOrEmpty(c.Container) ? c.Container : String.Empty;
                    dic["Local"] = c => !String.IsNullOrEmpty(c.Local) ? c.Local : String.Empty;
                    dic["Prefixo Trem"] = c => !String.IsNullOrEmpty(c.Trem) ? c.Trem : String.Empty;
                    dic["Faturado 363"] = c => !String.IsNullOrEmpty(c.Faturado) ? c.Faturado : String.Empty;
                    dic["Faturado 1131"] = c => !String.IsNullOrEmpty(c.FaturadoManual) ? c.FaturadoManual : String.Empty;
                    dic["Erro Faturamento"] = c => !String.IsNullOrEmpty(c.ErroFaturamento) ? c.ErroFaturamento : String.Empty;
                    dic["CT-e"] = c => !String.IsNullOrEmpty(c.CteStatus) ? c.CteStatus : String.Empty;
                    dic["Segmento"] = c => !String.IsNullOrEmpty(c.Segmento) ? c.Segmento : String.Empty;
                    dic["Ticket"] = c => !String.IsNullOrEmpty(c.Ticket) ? c.Ticket : String.Empty;
                    dic["Usuário Aprovador na 1380"] = c => !String.IsNullOrEmpty(c.Aprovador) ? c.Aprovador : String.Empty;
                    dic["Usuário Faturamento"] = c => !String.IsNullOrEmpty(c.UsuarioEventoCarregamento) ? c.UsuarioEventoCarregamento : String.Empty;
                    dic["Data Receb. Sistema Rumo"] = c => (c.HorarioRecebimentoArquivo.HasValue ? c.HorarioRecebimentoArquivo.Value.ToString() : String.Empty);
                    dic["Data Recebimento 1380"] = c => (c.HorarioLiberacao1380.HasValue ? c.HorarioLiberacao1380.Value.ToString() : String.Empty);
                    dic["Data Recebimento 363"] = c => (c.HorarioRecebimentoFaturamento.HasValue ? c.HorarioRecebimentoFaturamento.Value.ToString() : String.Empty);
                    dic["Data Faturamento"] = c => (c.HorarioFaturamento.HasValue ? c.HorarioFaturamento.Value.ToString() : String.Empty);

                    return this.Excel(string.Format("PainelExpedicao_{0}_{1}.xls", DateTime.Now.ToString("ddMMyyyy"), DateTime.Now.ToString("HHmmss")), dic, resultExcel.Items);
                }

                var result = this._painelExpedicaoService.ObterPainelExpedicaoConsulta(
                                                                        pagination,
                                                                        dataInicialFiltro,
                                                                        dataFinalFiltro,
                                                                        fluxo,
                                                                        origem,
                                                                        destino,
                                                                        vagoes,
                                                                        situacao,
                                                                        lotacao,
                                                                        localAtual,
                                                                        cliente,
                                                                        terminalFaturamento,
                                                                        mercadorias,
                                                                        segmento);

                return
                    this.Json(
                        new
                        {
                            result.Total,
                            Items =
                                result.Items.Select(
                                    e =>
                                    new
                                    {
                                        Data = (e.Data.HasValue ? e.Data.Value.ToString("dd/MM/yyyy") : ""),
                                        DataBo = (e.Data_Bo.HasValue ? e.Data_Bo.Value.ToString("dd/MM/yyyy") : ""),
                                        HorarioRecebimentoFaturamento = (e.HorarioRecebimentoFaturamento.HasValue ? e.HorarioRecebimentoFaturamento.Value.ToString() : String.Empty),
                                        HorarioRecebimentoArquivo = (e.HorarioRecebimentoArquivo.HasValue ? e.HorarioRecebimentoArquivo.Value.ToString() : String.Empty),
                                        HorarioFaturamento = (e.HorarioFaturamento.HasValue ? e.HorarioFaturamento.Value.ToString() : String.Empty),
                                        HorarioLiberacao1380 = (e.HorarioLiberacao1380.HasValue ? e.HorarioLiberacao1380.Value.ToString() : String.Empty),
                                        SerieVagao = e.SerieVagao,
                                        Vagao = e.Vagao,
                                        Situacao = e.Situacao,
                                        Lotacao = e.Lotacao,
                                        PesoTt = e.PesoTt,
                                        PesoTu = e.PesoTu,
                                        PesoTb = e.PesoTb,
                                        MultiploDespacho = String.IsNullOrEmpty(e.MultiploDespacho) || e.MultiploDespacho == "NAO" ? "N" : "S",
                                        Fluxo = e.Fluxo,
                                        Origem = e.Origem,
                                        Destino = e.Destino,
                                        TerminalFaturamento = e.TerminalFaturamento,
                                        Cliente = e.Cliente,
                                        Mercadoria = e.Mercadoria,
                                        Container = e.Container,
                                        Local = e.Local,
                                        Trem = e.Trem,
                                        Faturado = e.Faturado,
                                        FaturadoManual = e.FaturadoManual,
                                        ErroFaturamento = e.ErroFaturamento,
                                        CTe = e.CteStatus,
                                        Segmento = e.Segmento,
                                        Ticket = e.Ticket,
                                        Aprovador = e.Aprovador,
                                        UsuarioFaturamento = e.UsuarioEventoCarregamento
                                    }),
                            success = true
                        });
            }
            catch (Exception)
            {
                return this.Json(new { success = false });
            }
        }

        [Autorizar(Transacao = "PAINELEXPEDICAODOCUMENTOS", Acao = "Pesquisar")]
        public ActionResult ObterPainelExpedicaoConsultaDocumentos(DetalhesPaginacaoWeb pagination, string dataInicio, string dataFim, string fluxo, string origem, string destino, string vagoes, string cliente, string expedidor, string remetente, string recebedor, string mercadorias, string segmento, string os, string prefixo, string statusCte, string statusNfe, string statusWebDanfe, string statusTicket, string statusDcl, string statusLaudoMercadoria, string outrasFerrovias, bool exportarExcel, string malhaOrigem)
        {
            try
            {
                if (exportarExcel)
                    pagination.Limite = 9999999;

                string cnpjRaizRecebedor = "";

                var result = _painelExpedicaoService.ObterPainelExpedicaoConsultaDocumentos(pagination,
                                                                                        dataInicio,
                                                                                        dataFim,
                                                                                        fluxo,
                                                                                        origem,
                                                                                        destino,
                                                                                        vagoes,
                                                                                        cliente,
                                                                                        expedidor,
                                                                                        remetente,
                                                                                        recebedor,
                                                                                        mercadorias,
                                                                                        segmento,
                                                                                        os,
                                                                                        prefixo,
                                                                                        statusCte,
                                                                                        statusNfe,
                                                                                        statusWebDanfe,
                                                                                        statusTicket,
                                                                                        statusDcl,
                                                                                        statusLaudoMercadoria,
                                                                                        outrasFerrovias,
                                                                                        null,
                                                                                        cnpjRaizRecebedor,
                                                                                        malhaOrigem,
                                                                                        exportarExcel);

                if (exportarExcel)
                {
                    var relDocumentosVagao = new ExpedicaoDocumentosVagaoExcelService(result.Items);
                    var colunas = RetornaColunasCustomizadas(false);
                    var excelStream = relDocumentosVagao.GerarRelatorioCustomizado("Relatório Documentos do Vagão para Descarga (Transação 1381)",
                                                                    null,
                                                                    colunas);
                    var memoryStream = excelStream as MemoryStream;
                    excelStream.Seek(0, SeekOrigin.Begin);
                    return File(memoryStream.ToArray(),
                        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                        string.Format("RelatorioExpedicaoDocumentosVagao_{0}_{1}.xlsx", DateTime.Now.ToString("ddMMyyyy"), DateTime.Now.ToString("HHmmss")));
                }

                return this.Json(
                    new
                    {
                        result.Total,
                        Items =
                            result.Items.Select(
                                e =>
                                new
                                {
                                    Id = e.IdDespachoItem,
                                    IdCte = e.IdCte,
                                    IdMdfe = e.IdMdfe,
                                    Vagao = e.Vagao,
                                    DataCarga = e.DataCarga.ToString("dd/MM/yyyy HH:mm"),
                                    Fluxo = e.Fluxo,
                                    Origem = e.Origem,
                                    Destino = e.Destino,
                                    MalhaOrigem = e.MalhaOrigem,
                                    Correntista = e.Cliente,
                                    Expedidor = e.Expedidor,
                                    Recebedor = e.Recebedor,
                                    RemetenteFiscal = e.RemetenteFiscal,
                                    Mercadoria = e.DescricaoMercadoria,
                                    Os = (e.Os.HasValue ? e.Os.Value : 0),
                                    Prefixo = e.Prefixo,
                                    Seq = e.Seq,
                                    PossuiTicket = e.PossuiTicket,
                                    CteStatus = (e.PossuiCte == null ? "" : e.PossuiCte),
                                    PossuiNfe = e.PossuiNfe,
                                    PossuiWebDanfe = e.PossuiWebDanfe,
                                    QuantidadePdfsEnviados = e.QuantidadeNfeRecebidas,
                                    QuantidadeTotalPdfs = e.TotalNfe,
                                    PossuiDcl = e.PossuiDcl,
                                    PossuiMdfe = e.PossuiMdfe,
                                    PossuiXmlNfe = e.PossuiXmlNfe,
                                    PossuiLaudoMercadoria = e.PossuiLaudoMercadoria
                                }),
                        success = true
                    });
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [Autorizar(Transacao = "PAINELEXPEDICAODOCUMENTOS", Acao = "Pesquisar")]
        public ActionResult ObterHistoricoFaturamentos(
            DetalhesPaginacaoWeb pagination,
            string dataInicio,
            string dataFim,
            string fluxo,
            string origem,
            string destino,
            string vagoes,
            string cliente,
            string expedidor,
            string remetente,
            string recebedor,
            string mercadorias,
            string segmento,
            string os,
            string prefixo,
            string statusCte,
            string statusNfe,
            string statusWebDanfe,
            string statusTicket,
            string statusDcl,
            string statusLaudoMercadoria,
            string outrasFerrovias,
            bool exportarExcel,
            string malhaOrigem)
        {
            try
            {
                if (exportarExcel)
                    pagination.Limite = 9999999;

                var cnpjRaizRecebedor = String.Empty;

                var result = _painelExpedicaoService.ObterHistoricoFaturamentos(pagination,
                                                                                dataInicio,
                                                                                dataFim,
                                                                                fluxo,
                                                                                origem,
                                                                                destino,
                                                                                vagoes,
                                                                                cliente,
                                                                                expedidor,
                                                                                remetente,
                                                                                recebedor,
                                                                                mercadorias,
                                                                                segmento,
                                                                                os,
                                                                                prefixo,
                                                                                statusCte,
                                                                                statusNfe,
                                                                                statusWebDanfe,
                                                                                statusTicket,
                                                                                statusDcl,
                                                                                statusLaudoMercadoria,
                                                                                outrasFerrovias,
                                                                                null,
                                                                                cnpjRaizRecebedor,
                                                                                malhaOrigem,
                                                                                exportarExcel);

                if (exportarExcel)
                {
                    var relDocumentosVagao = new HistoricoFaturamentosExcelService(result.Items);
                    var colunas = RetornaColunasCustomizadas(true);
                    var excelStream = relDocumentosVagao.GerarRelatorioCustomizado("Relatório de Histórico de Faturamentos",
                                                                    null,
                                                                    colunas);
                    var memoryStream = excelStream as MemoryStream;
                    excelStream.Seek(0, SeekOrigin.Begin);
                    return File(memoryStream.ToArray(),
                        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                        string.Format("RelatorioHistoricoFaturamentos_{0}_{1}.xlsx", DateTime.Now.ToString("ddMMyyyy"), DateTime.Now.ToString("HHmmss")));
                }

                return this.Json(
                    new
                    {
                        result.Total,
                        Items =
                            result.Items.Select(
                                e =>
                                new
                                {
                                    Id = e.IdDespachoItem,
                                    IdCte = e.IdCte,
                                    IdMdfe = e.IdMdfe,
                                    Vagao = e.Vagao,
                                    DataCarga = e.DataCarga.ToString("dd/MM/yyyy HH:mm"),
                                    Fluxo = e.Fluxo,
                                    Origem = e.Origem,
                                    Destino = e.Destino,
                                    MalhaOrigem = e.MalhaOrigem,
                                    Correntista = e.Cliente,
                                    Expedidor = e.Expedidor,
                                    Recebedor = e.Recebedor,
                                    RemetenteFiscal = e.RemetenteFiscal,
                                    Mercadoria = e.DescricaoMercadoria,
                                    Os = (e.Os.HasValue ? e.Os.Value : 0),
                                    Prefixo = e.Prefixo,
                                    Seq = e.Seq,
                                    PossuiTicket = e.PossuiTicket,
                                    CteStatus = (e.PossuiCte == null ? "" : e.PossuiCte),
                                    PossuiNfe = e.PossuiNfe,
                                    PossuiWebDanfe = e.PossuiWebDanfe,
                                    QuantidadePdfsEnviados = e.QuantidadeNfeRecebidas,
                                    QuantidadeTotalPdfs = e.TotalNfe,
                                    PossuiDcl = e.PossuiDcl,
                                    PossuiMdfe = e.PossuiMdfe,
                                    PossuiXmlNfe = e.PossuiXmlNfe,
                                    PossuiLaudoMercadoria = e.PossuiLaudoMercadoria,
                                    e.QtdXmlNfe,
                                    e.XmlNaoRecebidos


                                }),
                        success = true
                    });
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public ActionResult ObterPainelExpedicaoRelatorioLaudos(
            DetalhesPaginacaoWeb pagination,
            string dataInicio,
            string dataFim,
            string fluxo,
            string origem,
            string destino,
            string vagoes,
            string cliente,
            string expedidor,
            string remetente,
            string mercadorias,
            string segmento,
            string os,
            string prefixo,
            string malhaOrigem)
        {
            try
            {
                string cnpjRaizRecebedor = String.Empty;

                var resultados = _painelExpedicaoService.ObterPainelExpedicaoRelatorioLaudos(pagination,
                                                                                             dataInicio,
                                                                                             dataFim,
                                                                                             fluxo,
                                                                                             origem,
                                                                                             destino,
                                                                                             vagoes,
                                                                                             cliente,
                                                                                             expedidor,
                                                                                             remetente,
                                                                                             mercadorias,
                                                                                             segmento,
                                                                                             os,
                                                                                             prefixo,
                                                                                             null,
                                                                                             cnpjRaizRecebedor,
                                                                                             malhaOrigem);

                var relatorioLaudos = new RelatorioLaudosMercadoriaExcelService(resultados);
                var colunas = relatorioLaudos.RetornaNomeColunas();

                var excelStream = relatorioLaudos.GerarRelatorioCustomizado("Relatório Laudos Mercadoria", null, colunas);
                var memoryStream = excelStream as MemoryStream;
                excelStream.Seek(0, SeekOrigin.Begin);
                return File(memoryStream.ToArray(),
                    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                    string.Format("RelatorioLaudosMercadoria_{0}_{1}.xlsx", DateTime.Now.ToString("ddMMyyyy"), DateTime.Now.ToString("HHmmss")));
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public ActionResult ExportarDocumentosVagaoSelecionados(DetalhesPaginacaoWeb pagination, string dataInicio, string dataFim, string fluxo, string origem, string destino, string vagoes, string cliente,
            string expedidor, string remetente, string recebedor, string mercadorias, string segmento, string os, string prefixo, string statusCte, string statusNfe, string statusWebDanfe, string statusTicket,
            string statusDcl, string statusLaudoMercadoria, string outrasFerrovias, List<string> itensDespacho, string malhaOrigem)
        {

            try
            {
                if (pagination == null)
                {
                    pagination = new DetalhesPaginacaoWeb();
                    pagination.Dir = "ASC";
                    pagination.ColunasOrdenacao = new List<string>();
                    pagination.DirecoesOrdenacao = new List<string>();
                }

                var itensDespachoSeparadosPorVirgula = String.Join(", ", itensDespacho.ToArray());

                var result = _painelExpedicaoService.ObterPainelExpedicaoConsultaDocumentos(pagination,
                                                                                            dataInicio,
                                                                                            dataFim,
                                                                                            fluxo,
                                                                                            origem,
                                                                                            destino,
                                                                                            vagoes,
                                                                                            cliente,
                                                                                            expedidor,
                                                                                            remetente,
                                                                                            recebedor,
                                                                                            mercadorias,
                                                                                            segmento,
                                                                                            os,
                                                                                            prefixo,
                                                                                            statusCte,
                                                                                            statusNfe,
                                                                                            statusWebDanfe,
                                                                                            statusTicket,
                                                                                            statusDcl,
                                                                                            statusLaudoMercadoria,
                                                                                            outrasFerrovias,
                                                                                            itensDespachoSeparadosPorVirgula,
                                                                                            "",
                                                                                            malhaOrigem,
                                                                                            true);

                var relDocumentosVagao = new ExpedicaoDocumentosVagaoExcelService(result.Items);

                var colunas = RetornaColunasCustomizadas(false);
                var excelStream = relDocumentosVagao.GerarRelatorioCustomizado("Relatório Documentos do Vagão para Descarga (Transação 1381)",
                                                                null,
                                                                colunas);
                var memoryStream = excelStream as MemoryStream;
                excelStream.Seek(0, SeekOrigin.Begin);

                var handler = "RelatorioExpedicaoDocumentosVagaoHandler_" + UsuarioAtual.Id;

                Session[handler] = memoryStream.ToArray();

                var fileName = string.Format("RelatorioExpedicaoDocumentosVagao_{0}_{1}.xlsx", DateTime.Now.ToString("ddMMyyyy"), DateTime.Now.ToString("HHmmss"));

                return new JsonResult()
                {
                    Data = new { success = true, FileGuid = handler, FileName = fileName }
                };
            }
            catch (Exception)
            {
                return this.Json(new { success = false });
            }
        }

        public ActionResult ExportarHistoricoFaturamentosVagoesSelecionados(DetalhesPaginacaoWeb pagination, string dataInicio, string dataFim, string fluxo, string origem, string destino, string vagoes,
            string cliente, string expedidor, string remetente, string recebedor, string mercadorias, string segmento, string os, string prefixo, string statusCte, string statusNfe, string statusTicket,
            string statusDcl, string statusLaudoMercadoria, string outrasFerrovias, List<string> itensDespacho, string malhaOrigem)
        {

            try
            {
                if (pagination == null)
                {
                    pagination = new DetalhesPaginacaoWeb();
                    pagination.Dir = "ASC";
                    pagination.ColunasOrdenacao = new List<string>();
                    pagination.DirecoesOrdenacao = new List<string>();
                }

                var itensDespachoSeparadosPorVirgula = String.Join(", ", itensDespacho.ToArray());

                var result = _painelExpedicaoService.ObterHistoricoFaturamentos(pagination,
                                                                                dataInicio,
                                                                                dataFim,
                                                                                fluxo,
                                                                                origem,
                                                                                destino,
                                                                                vagoes,
                                                                                cliente,
                                                                                expedidor,
                                                                                remetente,
                                                                                recebedor,
                                                                                mercadorias,
                                                                                segmento,
                                                                                os,
                                                                                prefixo,
                                                                                statusCte,
                                                                                statusNfe,
                                                                                "true",
                                                                                statusTicket,
                                                                                statusDcl,
                                                                                statusLaudoMercadoria,
                                                                                outrasFerrovias,
                                                                                itensDespachoSeparadosPorVirgula,
                                                                                "",
                                                                                malhaOrigem,
                                                                                true);

                var relDocumentosVagao = new HistoricoFaturamentosExcelService(result.Items);

                var colunas = RetornaColunasCustomizadas(true);
                var excelStream = relDocumentosVagao.GerarRelatorioCustomizado("Relatório de Histórico de Faturamentos",
                                                                null,
                                                                colunas);
                var memoryStream = excelStream as MemoryStream;
                excelStream.Seek(0, SeekOrigin.Begin);

                var handler = "RelatorioHistoricoFaturamentoHandler_" + UsuarioAtual.Id;

                Session[handler] = memoryStream.ToArray();

                var fileName = string.Format("RelatorioHistoricoFaturamentosVagoes_{0}_{1}.xlsx", DateTime.Now.ToString("ddMMyyyy"), DateTime.Now.ToString("HHmmss"));

                return new JsonResult()
                {
                    Data = new { success = true, FileGuid = handler, FileName = fileName }
                };
            }
            catch (Exception)
            {
                return this.Json(new { success = false });
            }
        }

        [HttpGet]
        public virtual ActionResult DownloadArquivoExcel(string fileGuid, string fileName)
        {
            if (Session[fileGuid] != null)
            {
                byte[] data = Session[fileGuid] as byte[];
                return File(data, "application/vnd.ms-excel", fileName);
            }
            else
            {
                return new EmptyResult();
            }
        }

        [Autorizar(Transacao = "PAINELEXPEDICAOARQUIVOS", Acao = "Pesquisar")]
        public JsonResult ObterPainelExpedicaoConferenciaArquivos(DetalhesPaginacaoWeb pagination,
                                                                    string dataCadastroBo,
                                                                    string dataCadastroFinalBo,
                                                                    string fluxo,
                                                                    string origem,
                                                                    string destino,
                                                                    string serie,
                                                                    string vagoes,
                                                                    string localAtual,
                                                                    string cliente,
                                                                    string expedidor,
                                                                    string mercadorias,
                                                                    string segmento,
                                                                    string arquivosPatio)
        {
            try
            {

                DateTime dataCadastroBoFiltro = DateTime.Now;
                DateTime dataCadastroFinalBoFiltro = DateTime.Now;

                if (!string.IsNullOrEmpty(dataCadastroBo))
                    dataCadastroBoFiltro = DateTime.ParseExact(dataCadastroBo, "dd/MM/yyyy", CultureInfo.InvariantCulture);


                if (!string.IsNullOrEmpty(dataCadastroFinalBo))
                    dataCadastroFinalBoFiltro = DateTime.ParseExact(dataCadastroFinalBo, "dd/MM/yyyy", CultureInfo.InvariantCulture);


                bool visualizarArquivosPatio = (!String.IsNullOrEmpty(arquivosPatio) && (arquivosPatio.ToUpper() == "S")
                    ? true
                    : false);

                var perfilAcessoUsuario = PainelExpedicaoPerfilAcessoEnum.Indefinido;
                var result = _painelExpedicaoService.ObterPainelExpedicaoConferenciaArquivos(pagination,
                                                                                            dataCadastroBoFiltro,
                                                                                            dataCadastroFinalBoFiltro,
                                                                                            fluxo,
                                                                                            origem,
                                                                                            destino,
                                                                                            serie,
                                                                                            vagoes,
                                                                                            localAtual,
                                                                                            cliente,
                                                                                            expedidor,
                                                                                            mercadorias,
                                                                                            segmento,
                                                                                            visualizarArquivosPatio,
                                                                                            UsuarioAtual,
                                                                                            ref perfilAcessoUsuario);


                return
                    this.Json(
                        new
                        {
                            result.Total,
                            Items =
                                result.Items.Select(
                                    e =>
                                    new
                                    {
                                        IdAgrupamentoMD = e.IdAgrupamentoMd,
                                        IdAgrupamentoSimples = e.IdAgrupamentoSimples,
                                        Agrupador = e.Agrupador,
                                        Id = e.Id,
                                        Serie = e.Serie,
                                        VagaoId = e.VagaoId,
                                        Vagao = e.Vagao,
                                        PesoTotal = e.PesoTotal,
                                        Fluxo = e.Fluxo,
                                        Origem = e.Origem,
                                        Recebedor = e.Recebedor,
                                        Expedidor = e.Expedidor,
                                        ChaveNota = e.ChaveNota,
                                        PesoNFe = e.PesoNfe,
                                        PesoRateioNFe = e.PesoRateioNfe,
                                        Conteiner = e.Conteiner,
                                        FluxoSegmento = e.FluxoSegmento,
                                        FluxoMercadoria = e.FluxoMercadoria,
                                        ObservacaoRecusado = e.ObservacaoRecusado,
                                        Cliente363 = e.Cliente363,
                                        LiberacaoRegistroMultiploDespacho = e.LiberacaoRegistroMultiploDespacho,
                                        MultiploDespacho = e.MultiploDespacho,
                                        CorSucesso = e.CorSucesso,
                                        PodeAlterar = (((perfilAcessoUsuario == PainelExpedicaoPerfilAcessoEnum.Central)
                                                            && (!visualizarArquivosPatio))
                                                        ||
                                                        (perfilAcessoUsuario == PainelExpedicaoPerfilAcessoEnum.CentralSuper)
                                                            ? true : false),
                                        PodeConfirmarRecusar = ((perfilAcessoUsuario == PainelExpedicaoPerfilAcessoEnum.Central
                                                                      ||
                                                                      perfilAcessoUsuario == PainelExpedicaoPerfilAcessoEnum.CentralSuper
                                                                     ) && (!visualizarArquivosPatio) ? true :


                                                                    ((perfilAcessoUsuario == PainelExpedicaoPerfilAcessoEnum.Patio
                                                                      ||
                                                                      perfilAcessoUsuario == PainelExpedicaoPerfilAcessoEnum.CentralSuper
                                                                    ) && (visualizarArquivosPatio) ? true : false)
                                                                )
                                    }),
                            success = true
                        });
            }
            catch (Exception)
            {
                return this.Json(new { success = false });
            }
        }

        public ActionResult ObterPainelExpedicaoOrigens(bool exportarExcel)
        {
            var result = _painelExpedicaoService.ObterOrigens();

            if (exportarExcel)
            {
                var dic = new Dictionary<string, Func<Origem, string>>();

                dic["Estação"] = c => c.AreaOperacional.Codigo;
                dic["Descrição"] = c => c.AreaOperacional.Descricao;
                dic["Estação Fat."] = c => (c.AreaOperacionalFaturamento == null) ? String.Empty : c.AreaOperacionalFaturamento.Codigo;
                dic["Descrição Estação Fat."] = c => (c.AreaOperacionalFaturamento == null) ? String.Empty : c.AreaOperacionalFaturamento.Descricao;
                dic["Região"] = c => (c.Regiao == null) ? String.Empty : c.Regiao.Nome;

                return this.Excel(string.Format("PainelExpedicaoEstacoes_{0}_{1}.xls", DateTime.Now.ToString("ddMMyyyy"), DateTime.Now.ToString("HHmmss")), dic, result);
            }

            return Json(new
            {
                result.Count,
                Items = result.Select(d => new
                {
                    Id = d.IdOrigem,
                    Estacao = d.AreaOperacional.Codigo,
                    Descricao = d.AreaOperacional.Descricao,
                    EstacaoFaturamento = (d.AreaOperacionalFaturamento == null) ? String.Empty : d.AreaOperacionalFaturamento.Codigo,
                    DescricaoEstacaoFaturamento = (d.AreaOperacionalFaturamento == null) ? String.Empty : d.AreaOperacionalFaturamento.Descricao,
                    RegiaoId = (d.Regiao == null) ? -1 : d.Regiao.Id,
                    DescricaoRegiao = (d.Regiao == null) ? String.Empty : d.Regiao.Nome,
                })
            });
        }

        public JsonResult ObterOperadores()
        {
            try
            {
                var result = _painelExpedicaoService.ObterOperadores();

                return Json(new
                {
                    Success = true,
                    Quantity = result.Count,
                    Items = result.Select(op => new
                    {
                        Id = op.IdOperador,
                        Simbolo = op.Simbolo,
                        Descricao = op.Descricao
                    })
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    Quantity = 0,
                    ErrorMessage = ex.Message
                });
            }
        }

        public JsonResult ObterOperacoesMalha()
        {
            try
            {
                var result = _painelExpedicaoService.ObterOperacoes();

                result.Add(new PeOperacao() { Id = 0, Nome = "TODAS" });

                return Json(new
                {
                    Success = true,
                    Quantity = result.Count,
                    Items = result.OrderBy(a => a.Id).Select(op => new
                    {
                        Id = op.Id,
                        Nome = op.Nome
                    })
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    Quantity = 0,
                    ErrorMessage = ex.Message
                });
            }
        }

        public JsonResult ObterIndicadores(int? colunaId)
        {
            try
            {
                var result = _painelExpedicaoService.ObterIndicadores(colunaId);

                return Json(new
                {
                    Success = true,
                    Quantity = result.Count,
                    Items = result.Select(indicador => new
                    {
                        IdIndicador = indicador.IdIndicador,
                        IdCor = indicador.IdCor,
                        Cor = ALL.Core.Util.Enum<CorEnum>.GetDescriptionOf(indicador.CorEnum),
                        Op1Id = indicador.Operador1.IdOperador,
                        Vl1 = indicador.Valor1,
                        OpLogId = indicador.IdOperadorLogico ?? (int)OperadorLogicoEnum.Nenhum,
                        Op2Id = indicador.Operador2 != null ? indicador.Operador2.IdOperador : -1,
                        Vl2 = indicador.Valor2
                    }).ToList()
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    Quantity = 0,
                    ErrorMessage = ex.Message
                });
            }
        }

        public JsonResult ObterConfiguracoesClientes(string query)
        {
            try
            {
                var results = _painelExpedicaoService.ObterConfiguracoesClientes(query);

                return Json(new
                {
                    Success = true,
                    Items = results.Select(cliente => new
                    {
                        Id = cliente.Id,
                        Nome = cliente.DescricaoEmpresa
                    }).ToList()
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ErrorMessage = ex.Message
                });
            }
        }

        public JsonResult ObterClientes()
        {
            try
            {
                var results = _painelExpedicaoService.ObterClientes();

                return Json(new
                {
                    Success = true,
                    Items = results.Select(cliente => new
                    {
                        Id = cliente.Id,
                        Nome = cliente.NomeFantasia
                    }).ToList()
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ErrorMessage = ex.Message
                });
            }
        }

        public JsonResult ObterClientesDistintos(string query)
        {
            try
            {
                query = query ?? "";

                if (!string.IsNullOrEmpty(query))
                {
                    var itens = _painelExpedicaoService.ObterListaNomesClientesDescResumida(query);

                    var teste = itens.Select(a => new
                    {
                        a.Id,
                        a.DescricaoResumida
                    }).ToList();

                    return Json(new
                    {
                        Total = itens.Count,
                        Items = teste.Select(a => new
                        {
                            Id = a.Id,
                            DescResumida = a.DescricaoResumida
                        })
                    });
                }

                return null;
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ErrorMessage = ex.Message
                });
            }
        }

        public JsonResult ObterExpedidores(string query)
        {
            try
            {
                query = query ?? "";

                if (!string.IsNullOrEmpty(query))
                {
                    var itens = _painelExpedicaoService.ObterListaNomesExpedidoresDescResumida(query);

                    var teste = itens.Select(a => new
                    {
                        a.Id,
                        a.DescricaoResumida
                    }).ToList();

                    return Json(new
                    {
                        Total = itens.Count,
                        Items = teste.Select(a => new
                        {
                            Id = a.Id,
                            DescResumida = a.DescricaoResumida
                        })
                    });
                }

                return null;
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ErrorMessage = ex.Message
                });
            }
        }

        public JsonResult ObterRemetentes(string query)
        {
            try
            {
                query = query ?? "";

                if (!string.IsNullOrEmpty(query))
                {
                    var itens = _painelExpedicaoService.ObterListaNomesRemetentesDescResumida(query);

                    var teste = itens.Select(a => new
                    {
                        a.Id,
                        a.DescricaoResumida
                    }).ToList();

                    return Json(new
                    {
                        Total = itens.Count,
                        Items = teste.Select(a => new
                        {
                            Id = a.Id,
                            DescResumida = a.DescricaoResumida
                        })
                    });
                }

                return null;
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ErrorMessage = ex.Message
                });
            }
        }

        public JsonResult ObterRecebedores(string query)
        {
            try
            {
                query = query ?? "";

                if (!string.IsNullOrEmpty(query))
                {
                    var itens = _painelExpedicaoService.ObterListaNomesRecebedoresDescResumida(query);

                    var teste = itens.Select(a => new
                    {
                        a.Id,
                        a.DescricaoResumida
                    }).ToList();

                    return Json(new
                    {
                        Total = itens.Count,
                        Items = teste.Select(a => new
                        {
                            Id = a.Id,
                            DescResumida = a.DescricaoResumida
                        })
                    });
                }

                return null;
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ErrorMessage = ex.Message
                });
            }
        }

        public JsonResult ObterMercadorias()
        {
            try
            {
                var results = _painelExpedicaoService.ObterMercadorias();

                return Json(new
                {
                    Success = true,
                    Items = results.Select(mercadoria => new
                    {
                        Id = mercadoria.Id,
                        Nome = mercadoria.DescricaoResumida
                    }).ToList()
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ErrorMessage = ex.Message
                });
            }
        }

        public JsonResult ObterEstacoesFaturamento(string uf, string malha)
        {
            try
            {
                var results = _painelExpedicaoService.ObterOrigensFaturamento2(uf);

                List<string> ufsMalha = _painelExpedicaoService.ObterUfsDaMalha(malha);

                if (ufsMalha != null)
                {
                    results = results.Where(r => ufsMalha.Contains(r.UF)).ToList();
                }

                var listItens = results.Select(ef => new
                {
                    Id = ef.Codigo,
                    Nome = ef.Descricao
                }).Distinct().ToList();

                //
                listItens.Add(new { Id = "0", Nome = "TODAS" });

                return Json(new
                {
                    Success = true,
                    Items = listItens
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ErrorMessage = ex.Message
                });
            }
        }

        public JsonResult ObterSegmentos()
        {
            try
            {
                var results = _painelExpedicaoService.ObterSegmentos();

                return Json(new
                {
                    Success = true,
                    Items = results.Select(segmento => new
                    {
                        Id = segmento,
                        Nome = segmento
                    }).ToList()
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ErrorMessage = ex.Message
                });
            }
        }

        public JsonResult ObterMalhas()
        {
            try
            {
                var results = _painelExpedicaoService.ObterMalhas();

                return Json(new
                {
                    Success = true,
                    Items = results.Select(malha => new
                    {
                        Id = malha,
                        Nome = malha
                    }).ToList()
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ErrorMessage = ex.Message
                });
            }
        }

        public JsonResult ObterRegioes()
        {
            try
            {
                var results = _painelExpedicaoService.ObterRegioes();

                return Json(new
                {
                    Success = true,
                    Items = results.Select(x => new
                    {
                        Id = x.Id,
                        Nome = x.Nome,
                        OperacaoId = x.OperacaoId
                    }).ToList()
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ErrorMessage = ex.Message
                });
            }
        }

        public JsonResult ObterDetalhePedraPlanejadoXRealizado()
        {
            try
            {
                //var results = _painelExpedicaoService.O

                var results = new List<RegiaoDto>();

                results.Add(new RegiaoDto
                {
                    Id = 1,
                    Nome = "NORTE DO PARANÁ",
                    OperacaoMalha = new OperacaoMalhaDto { Id = 1, Nome = "SUL", DataCadastro = DateTime.Now, UsuarioAlteracao = 1 }
                });

                results.Add(new RegiaoDto
                {
                    Id = 2,
                    Nome = "PARANÁ",
                    OperacaoMalha = new OperacaoMalhaDto { Id = 1, Nome = "SUL", DataCadastro = DateTime.Now, UsuarioAlteracao = 1 }
                });

                results.Add(new RegiaoDto
                {
                    Id = 3,
                    Nome = "PORTO",
                    OperacaoMalha = new OperacaoMalhaDto { Id = 1, Nome = "SUL", DataCadastro = DateTime.Now, UsuarioAlteracao = 1 }
                });

                return Json(new
                {
                    Success = true,
                    Items = results.Select(x => new
                    {
                        Id = x.Id,
                        Nome = x.Nome,
                        OperacaoId = x.OperacaoMalha.Id,
                        OperacaoNome = x.OperacaoMalha.Nome
                    }).ToList()
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ErrorMessage = ex.Message
                });
            }
        }

        public JsonResult ObterRegioesDDL(string operacaoId)
        {
            try
            {
                string where = !String.IsNullOrEmpty(operacaoId) ? $"OPERACAO_ID = {operacaoId}" : null;

                var result = _painelExpedicaoService.ObterRegioes(where);

                result.Add(new PeRegiao() { Id = 0, Nome = "TODAS" });

                return Json(new
                {
                    Success = true,
                    Quantity = result.Count,
                    Items = result.OrderBy(a => a.Id).Select(op => new
                    {
                        Id = op.Id,
                        Nome = op.Nome,
                        OperacaoId = op.OperacaoId
                    })
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    Quantity = 0,
                    ErrorMessage = ex.Message
                });
            }
        }

        public JsonResult ObterRegraEnvioEmailRegiao()
        {
            try
            {
                var result = _painelExpedicaoService.ObterRegraEmails();

                return Json(new
                {
                    Success = true,
                    Items = result.Select(x => new
                    {
                        Id = x.Id,
                        OperacaoId = x.OperacaoId,
                        RegiaoId = x.RegiaoId,
                        Titulo = x.TituloEmail,
                        Corpo = x.CorpoEmail,
                        Emails = x.Destinatarios
                    }).ToList()
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ErrorMessage = ex.Message
                });
            }
        }

        //public JsonResult ObterRegraEmails()
        //{
        //    try
        //    {
        //        var result = _painelExpedicaoService.ObterRegraEmails();

        //        return Json(new
        //        {
        //            Success = true,
        //            Quantity = result.Count,
        //            Items = result.OrderBy(a => a.Id).Select(op => new
        //            {
        //                Id = op.Id,
        //                Nome = op.
        //            })
        //        });
        //    }
        //    catch (Exception ex)
        //    {
        //        return Json(new
        //        {
        //            Success = false,
        //            Quantity = 0,
        //            ErrorMessage = ex.Message
        //        });
        //    }
        //}

        public JsonResult ObterTerminais()
        {
            try
            {
                var results = _terminalService.ObterTerminais();

                return Json(new
                {
                    Success = true,
                    Items = results.Select(terminal => new
                    {
                        Id = terminal.CodTerminal,
                        Nome = terminal.DescricaoTerminal
                    }).ToList()
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ErrorMessage = ex.Message
                });
            }
        }

        public JsonResult ObterTerminaisDDL()
        {
            try
            {
                var results = _terminalService.ObterTerminais();

                results.Add(new TerminalSeguroDto()
                {
                    CodTerminal = "0",
                    DescricaoTerminal = "TODOS"
                });

                return Json(new
                {
                    Success = true,
                    Items = results.Select(terminal => new
                    {
                        Id = terminal.CodTerminal,
                        Nome = terminal.DescricaoTerminal
                    }).ToList()
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ErrorMessage = ex.Message
                });
            }
        }

        public JsonResult ObterSituacoes()
        {
            try
            {
                var results = _motivoSituacaoVagaoService.ObterTodasSituacoes();

                return Json(new
                {
                    Success = true,
                    Items = results.Select(situacao => new
                    {
                        Id = situacao.IdSituacao,
                        Nome = situacao.DescricaoSituacao
                    }).ToList()
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ErrorMessage = ex.Message
                });
            }
        }

        public JsonResult ObterLotacoes()
        {
            try
            {
                var results = _motivoSituacaoVagaoService.ObterLotacoes();

                return Json(new
                {
                    Success = true,
                    Items = results.Select(lotacao => new
                    {
                        Id = lotacao.IdLotacao,
                        Nome = lotacao.DescricaoLotacao
                    }).ToList()
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ErrorMessage = ex.Message
                });
            }
        }

        #region CRUD REGIOES
        public JsonResult IncluirPainelExpedicaoOrigem()
        {
            var origemInformada = Request.Params["origem"];
            var estacaoFaturamentoInformada = Request.Params["estacaoFaturamento"];
            var regiaoId = Request.Params["regiaoId"];

            int idOrigem = -1;
            string estacaoOrigem = "";
            string descricaoOrigem = "";

            int idEstacaoFaturamento = -1;
            string estacaoFaturamentoOrigem = String.Empty;
            string descricaoEstacaoFaturamentoOrigem = String.Empty;

            try
            {

                if (String.IsNullOrEmpty(origemInformada))
                {
                    return Json(new
                    {
                        Message = "Nenhuma origem foi informada!",
                        Estacao = "",
                        Descricao = "",
                        EstacaoFaturamento = "",
                        DescricaoEstacaoFaturamento = "",
                        Success = false
                    });
                }

                if (String.IsNullOrEmpty(estacaoFaturamentoInformada))
                {
                    return Json(new
                    {
                        Message = "Nenhuma estação de faturamento informada!",
                        Estacao = "",
                        Descricao = "",
                        EstacaoFaturamento = "",
                        DescricaoEstacaoFaturamento = "",
                        Success = false
                    });
                }

                var origem = _painelExpedicaoService.ObterOrigem(origemInformada);

                if (origem == null)
                {
                    return Json(new
                    {
                        Message = "A origem informada não é válida!",
                        Estacao = "",
                        Descricao = "",
                        EstacaoFaturamento = "",
                        DescricaoEstacaoFaturamento = "",
                        Success = false
                    });
                }
                else
                {
                    if (origem.Codigo == null)
                    {
                        return Json(new
                        {
                            Message = "A origem informada não é válida!",
                            Estacao = "",
                            Descricao = "",
                            EstacaoFaturamento = "",
                            DescricaoEstacaoFaturamento = "",
                            Success = false
                        });
                    }
                }

                var origemFaturamento = _painelExpedicaoService.ObterOrigem(estacaoFaturamentoInformada);

                if (origemFaturamento == null)
                {
                    return Json(new
                    {
                        Message = "A estação de faturamento informada não é válida!",
                        Estacao = "",
                        Descricao = "",
                        EstacaoFaturamento = "",
                        DescricaoEstacaoFaturamento = "",
                        Success = false
                    });
                }
                else
                {
                    if (origemFaturamento.Codigo == null)
                    {
                        return Json(new
                        {
                            Message = "A estação de faturamento informada não é válida!",
                            Estacao = "",
                            Descricao = "",
                            EstacaoFaturamento = "",
                            DescricaoEstacaoFaturamento = "",
                            Success = false
                        });
                    }
                }


                estacaoOrigem = (origem != null ? origem.Codigo : "");
                descricaoOrigem = (origem != null ? origem.Descricao : "");

                estacaoFaturamentoOrigem = origemFaturamento.Codigo;
                descricaoEstacaoFaturamentoOrigem = origemFaturamento.Descricao;

                Origem origemCadastro = new Origem();
                origemCadastro.AreaOperacional = new EstacaoMae();
                origemCadastro.AreaOperacional = origem;

                origemCadastro.AreaOperacionalFaturamento = new EstacaoMae();
                origemCadastro.AreaOperacionalFaturamento = origemFaturamento;

                origemCadastro.Regiao = new Regiao();
                origemCadastro.Regiao.Id = Int32.Parse(regiaoId);

                Origem origemResult = _painelExpedicaoService.InserirOrigem(origemCadastro);
                idOrigem = origemResult.IdOrigem;
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Message = "Falha na inclusão da origem!",
                    Estacao = "",
                    Descricao = "",
                    EstacaoFaturamento = "",
                    DescricaoEstacaoFaturamento = "",
                    Success = false
                });
            }

            return Json(new
            {
                Message = "Origem incluída com sucesso!",
                Id = idOrigem,
                Estacao = estacaoOrigem,
                Descricao = descricaoOrigem,
                EstacaoFaturamento = estacaoFaturamentoOrigem,
                DescricaoEstacaoFaturamento = descricaoEstacaoFaturamentoOrigem,
                Success = true
            });
        }

        [Autorizar(Transacao = "PAINELEXPEDICAOVISUALIZACAO", Acao = "Pesquisar")]
        public JsonResult GravarAlteracoesRegiao(string acao, string id, string campo, string valor, string regiaoId, string nome, string operacaoId)
        {
            Tuple<bool, string> resultado = new Tuple<bool, string>(true, "Dados gravados com sucesso!");

            try
            {

                var saveObject = new PeRegiao()
                {
                    Id = !String.IsNullOrEmpty(regiaoId) ? Int32.Parse(regiaoId) : 0,
                    Nome = nome.ToUpper(),
                    OperacaoId = decimal.Parse(operacaoId),
                    DataAlteracao = DateTime.Now,
                    UsuarioAlteracao = UsuarioAtual.Id != null ? UsuarioAtual.Id.Value : 0
                };

                if (saveObject.Id != 0)
                    resultado = _painelExpedicaoService.AtualizarRegiao(saveObject);
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = ex.Message });
            }

            return Json(new { Success = resultado.Item1, Message = resultado.Item2 });
        }

        public JsonResult IncluirRegiaoPainelExpedicao()
        {
            Tuple<bool, string> resultado = new Tuple<bool, string>(true, "Região incluída com sucesso!");

            var nomeRegiao = Request.Params["Nome"];
            var operacaoMalhaId = Request.Params["OperacaoMalha_Id"];

            PeRegiao regiao = new PeRegiao()
            {
                Nome = nomeRegiao.ToUpper(),
                OperacaoId = decimal.Parse(operacaoMalhaId),
                UsuarioCadastro = UsuarioAtual.Id != null ? UsuarioAtual.Id.Value : 0,
                DataCadastro = DateTime.Now
            };

            try
            {
                resultado = _painelExpedicaoService.InserirRegiao(regiao);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Message = "Falha na inclusão da Regiao: " + ex.Message,
                    RegistrosInseridos = new List<RegiaoDto>(),
                    Success = false
                });
            }

            return Json(new
            {
                Message = resultado.Item2,
                Success = resultado.Item1
            });
        }

        public JsonResult ExcluirRegiao()
        {
            var regiaoId = Request.Params["regiaoId"];

            try
            {

                if (String.IsNullOrEmpty(regiaoId))
                {
                    return Json(new
                    {
                        Message = "Nenhuma região foi informada!",
                        Success = false
                    });
                }

                _painelExpedicaoService.ExcluirRegiao(Convert.ToInt32(regiaoId));

            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Message = "Falha na exclusão da região!",
                    Success = false
                });
            }

            return Json(new
            {
                Message = "Região excluída com sucesso!",
                Success = true
            });
        }

        #endregion

        #region CRUD RegraEmail
        public JsonResult IncluirRegraEnvioEmailsPainelExpedicao()
        {
            Tuple<bool, string> resultado = new Tuple<bool, string>(true, "Regra incluída com sucesso!");

            var operacaoId = Request.Params["operacaoId"];
            var regiaoId = Request.Params["regiaoId"];
            var titulo = Request.Params["titulo"];
            var corpo = Request.Params["corpo"];
            var destinatarios = Request.Params["destinatarios"];


            PeRegraEmails model = new PeRegraEmails()
            {
                OperacaoId = decimal.Parse(operacaoId),
                RegiaoId = decimal.Parse(regiaoId),
                TituloEmail = titulo,
                CorpoEmail = corpo,
                Destinatarios = destinatarios,
                UsuarioCadastro = UsuarioAtual.Id != null ? UsuarioAtual.Id.Value : 0,
                DataCadastro = DateTime.Now
            };

            try
            {
                resultado = _painelExpedicaoService.InserirRegraEmails(model);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Message = "Falha na inclusão da Regiao: " + ex.Message,
                    RegistrosInseridos = new List<RegiaoDto>(),
                    Success = false
                });
            }

            return Json(new
            {
                Message = resultado.Item2,
                Success = resultado.Item1
            });
        }

        [Autorizar(Transacao = "PAINELEXPEDICAOVISUALIZACAO", Acao = "Pesquisar")]
        public JsonResult GravarAlteracoesRegraEmail(string acao, string id, string campo, string valor, string operacaoId, string regiaoId, string titulo, string corpo, string destinatarios, string nome, string regraId)
        {
            Tuple<bool, string> resultado = new Tuple<bool, string>(true, "Dados gravados com sucesso!");

            try
            {
                var saveObject = new PeRegraEmails()
                {
                    Id = !String.IsNullOrEmpty(regraId) ? Int32.Parse(regraId) : 0,
                    OperacaoId = Decimal.Parse(operacaoId),
                    RegiaoId = Decimal.Parse(regiaoId),
                    TituloEmail = titulo,
                    CorpoEmail = corpo,
                    Destinatarios = destinatarios,
                    DataAlteracao = DateTime.Now,
                    UsuarioAlteracao = UsuarioAtual.Id != null ? UsuarioAtual.Id.Value : 0
                };

                if (saveObject.Id != 0)
                    resultado = _painelExpedicaoService.AtualizarRegraEmails(saveObject);
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = ex.Message });
            }

            return Json(new { Success = resultado.Item1, Message = resultado.Item2 });
        }

        public JsonResult ExcluirRegraEnvioEmail()
        {
            var regraId = Request.Params["Id"];

            try
            {

                if (String.IsNullOrEmpty(regraId))
                {
                    return Json(new
                    {
                        Message = "Nenhuma Regra de Envio de Email foi informada!",
                        Success = false
                    });
                }

                _painelExpedicaoService.ExcluirRegraEnvioEmail(Convert.ToInt32(regraId));
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Message = "Falha na exclusão da região!",
                    Success = false
                });
            }

            return Json(new
            {
                Message = "Região excluída com sucesso!",
                Success = true
            });
        }

        #endregion

        [Autorizar(Transacao = "PAINELEXPEDICAOPARAMETROS", Acao = "ExcluirOrigem")]
        public JsonResult ExcluirPainelExpedicaoOrigem()
        {
            var origemExclusaoId = Request.Params["origemId"];

            try
            {

                if (String.IsNullOrEmpty(origemExclusaoId))
                {
                    return Json(new
                    {
                        Message = "Nenhuma origem foi informada!",
                        Success = false
                    });
                }

                _painelExpedicaoService.ExcluirOrigem(Convert.ToInt32(origemExclusaoId));

            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Message = "Falha na exclusão da origem!",
                    Success = false
                });
            }

            return Json(new
            {
                Message = "Origem excluída com sucesso!",
                Success = true
            });
        }

        [Autorizar(Transacao = "PAINELEXPEDICAOPARAMETROS", Acao = "Salvar")]
        public JsonResult SalvarIndicadores(List<Indicador> indicadores)
        {
            try
            {
                _painelExpedicaoService.SalvarIndicadores(indicadores);
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = ex.Message });
            }

            return Json(new { Success = true, Message = "Configuração salva com sucesso!" });
        }

        [Autorizar(Transacao = "PAINELEXPEDICAOPEDRA", Acao = "Salvar")]
        public JsonResult SalvarJustificativaPedra(List<Indicador> indicadores)
        {
            try
            {
                _painelExpedicaoService.SalvarIndicadores(indicadores);
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = ex.Message });
            }

            return Json(new { Success = true, Message = "Configuração salva com sucesso!" });
        }

        [Autorizar(Transacao = "PAINELEXPEDICAOARQUIVOS", Acao = "Salvar")]
        public JsonResult SalvarAlteracoesConferenciaArquivos(List<ConferenciaArquivos> request)
        {
            try
            {
                var dto = request.Map<List<ConferenciaArquivos>, List<PainelExpedicaoConferenciaArquivosDto>>();
                _painelExpedicaoService.SalvarAlteracoesConferenciaArquivos(dto);
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = ex.Message });
            }

            return Json(new { Success = true, Message = "Configuração salva com sucesso!" });
        }

        [Autorizar(Transacao = "PAINELEXPEDICAOARQUIVOS", Acao = "ConfirmarRecusar")]
        public JsonResult AtualizarStatusConferenciaArquivos(List<ConferenciaArquivos> model, bool recusar)
        {
            try
            {
                var dto = model.Map<List<ConferenciaArquivos>, List<PainelExpedicaoConferenciaArquivosDto>>();
                _painelExpedicaoService.AtualizarStatusConferenciaArquivos(dto, recusar, UsuarioAtual);
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = ex.Message });
            }

            return Json(new { Success = true, Message = "Configuração salva com sucesso!" });
        }

        [Autorizar(Transacao = "PAINELEXPEDICAOARQUIVOS", Acao = "ConfirmarRecusarFull")]
        public JsonResult AtualizarStatusConferenciaArquivosFull(List<ConferenciaArquivos> model, bool recusar)
        {
            try
            {
                var dto = model.Map<List<ConferenciaArquivos>, List<PainelExpedicaoConferenciaArquivosDto>>();
                _painelExpedicaoService.AtualizarStatusConferenciaArquivos(dto, recusar, UsuarioAtual);
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = ex.Message });
            }

            return Json(new { Success = true, Message = "Configuração salva com sucesso!" });
        }

        [Autorizar(Transacao = "PAINELEXPEDICAODOCUMENTOS", Acao = "Imprimir")]
        public JsonResult ImprimirDocumentos(List<int> ids)
        {
            try
            {
                var result = _painelExpedicaoService.ImprimirDocumentos(ids);
                return this.Json(new { success = true });
            }
            catch (Exception)
            {
                return this.Json(new { success = false });
            }
        }

        public ActionResult ObterPainelExpedicaoBloqueiosDespacho(DetalhesPaginacaoWeb pagination, bool exportarExcel)
        {
            var result = _painelExpedicaoService.ObterBloqueiosDespacho(pagination);

            if (exportarExcel)
            {
                var dic = new Dictionary<string, Func<DespachoLocalBloqueioDto, string>>();

                dic["Estação"] = c => c.CodigoEstacao;
                dic["Descrição"] = c => c.DescricaoEstacao;
                dic["Segmento"] = c => c.Segmento;
                dic["Cnpj Raiz"] = c => (c.CnpjRaiz ?? String.Empty);
                dic["Cliente"] = c => (String.IsNullOrEmpty(c.CnpjRaiz) ? "Todos" : c.Cliente);
                dic["Data de Bloqueio"] = c => (c.DataBloqueio.HasValue ? c.DataBloqueio.Value.ToString("dd/MM/yyyy HH:mm") : "");
                dic["Usuário"] = c => !String.IsNullOrEmpty(c.UsuarioBloqueio) ? c.UsuarioBloqueio : String.Empty;

                return this.Excel(string.Format("BloqueiosEstacoes_{0}_{1}.xls", DateTime.Now.ToString("ddMMyyyy"), DateTime.Now.ToString("HHmmss")), dic, result);
            }

            return Json(new
            {
                result.Count,
                Items = result.Select(d => new
                {
                    Estacao = (d.CodigoEstacao + " - " + d.DescricaoEstacao),
                    Id = d.Id,
                    EstacaoCodigo = d.CodigoEstacao,
                    EstacaoDescricao = d.DescricaoEstacao,
                    Segmento = d.Segmento,
                    CnpjRaiz = (d.CnpjRaiz ?? String.Empty),
                    Cliente = (String.IsNullOrEmpty(d.CnpjRaiz) ? "Todos" : d.Cliente),
                    DataBloqueio = (d.DataBloqueio.HasValue ? d.DataBloqueio.Value.ToString("dd/MM/yyyy HH:mm") : ""),
                    UsuarioBloqueio = d.UsuarioBloqueio
                })
            });
        }

        //[Autorizar(Transacao = "PAINELEXPEDICAOPARAMETROS", Acao = "IncluirBloqueioDespacho")]
        public JsonResult IncluirBloqueioDespacho()
        {
            var origemInformada = Request.Params["origem"];
            var segmento = Request.Params["segmento"];
            var cnpj = Request.Params["cnpj"];

            List<DespachoLocalBloqueioDto> bloqueiosInseridos = null;

            try
            {
                bloqueiosInseridos = _painelExpedicaoService.InserirBloqueioDespacho(origemInformada,
                    segmento, cnpj, UsuarioAtual);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Message = "Falha na inclusão do bloqueio: " + ex.Message,
                    BloqueiosInseridos = new List<DespachoLocalBloqueioDto>(),
                    Success = false
                });
            }

            return Json(new
            {
                Message = "Bloqueio incluído com sucesso!",
                BloqueiosInseridos = bloqueiosInseridos,
                Success = true
            });
        }

        //[Autorizar(Transacao = "PAINELEXPEDICAOPARAMETROS", Acao = "ExcluirBloqueioDespacho")]
        public JsonResult ExcluirBloqueioDespacho()
        {
            var bloqueioExclusaoId = Request.Params["bloqueioId"];

            try
            {

                if (String.IsNullOrEmpty(bloqueioExclusaoId))
                {
                    return Json(new
                    {
                        Message = "Nenhum bloqueio foi informado!",
                        Success = false
                    });
                }

                _painelExpedicaoService.ExcluirBloqueioDespacho(Convert.ToInt32(bloqueioExclusaoId));

            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Message = "Falha na exclusão do bloqueio!",
                    Success = false
                });
            }

            return Json(new
            {
                Message = "Bloqueio excluído com sucesso!",
                Success = true
            });
        }

        public JsonResult ObterTiposIntegracao()
        {
            try
            {
                IList<TipoIntegracaoEnum> tiposIntegracao = new List<TipoIntegracaoEnum>();
                tiposIntegracao.Add(TipoIntegracaoEnum.ShippingNote);
                tiposIntegracao.Add(TipoIntegracaoEnum.Bizconnect);
                tiposIntegracao.Add(TipoIntegracaoEnum.Caall);
                tiposIntegracao.Add(TipoIntegracaoEnum.Manual);

                return Json(new
                {
                    tiposIntegracao.Count,
                    success = true,
                    Items = tiposIntegracao.Select(d => new
                    {
                        Id = (int)d,
                        Descricao = Tools.Desc(d)
                    })
                });

            }
            catch (Exception)
            {
                return this.Json(new { success = false });
            }
        }

        public ActionResult ObterPainelExpedicaoClientesTipoIntegracao(DetalhesPaginacaoWeb pagination, bool exportarExcel)
        {
            var result = _painelExpedicaoService.ObterClientesTipoIntegracao(pagination);

            if (exportarExcel)
            {
                var dic = new Dictionary<string, Func<ClienteTipoIntegracaoDto, string>>();

                dic["Estação"] = c => c.EstacaoCodigo;
                dic["Descrição"] = c => c.Estacao;
                dic["Cliente"] = c => c.Cliente;
                dic["Tipo de Integração"] = c => c.TipoIntegracao;

                return this.Excel(string.Format("EstacoesClientesTipoIntegracao_{0}_{1}.xls", DateTime.Now.ToString("ddMMyyyy"), DateTime.Now.ToString("HHmmss")), dic, result);
            }

            return Json(new
            {
                result.Count,
                Items = result.Select(d => new
                {
                    Id = d.Id,
                    EstacaoCodigo = d.EstacaoCodigo,
                    Estacao = d.Estacao,
                    Cliente = d.Cliente,
                    TipoIntegracao = d.TipoIntegracao
                })
            });
        }

        public JsonResult IncluirClienteTipoIntegracao()
        {
            var origem = Request.Params["origem"];
            var clienteId = Request.Params["clienteId"];
            var tipoIntegracaoId = Request.Params["tipoIntegracaoId"];

            ClienteTipoIntegracaoDto clienteTipoIntegracaoInserido = null;

            try
            {
                clienteTipoIntegracaoInserido = _painelExpedicaoService.InserirClienteTipoIntegracao(origem, Convert.ToInt32(clienteId), Convert.ToInt32(tipoIntegracaoId));
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Message = "Falha na inclusão do Tipo de Integração: " + ex.Message,
                    ClienteTipoIntegracaoInserido = new ClienteTipoIntegracaoDto(),
                    Success = false
                });
            }

            return Json(new
            {
                Message = "Tipo de Integração incluído com sucesso!",
                ClienteTipoIntegracaoInserido = clienteTipoIntegracaoInserido,
                Success = true
            });
        }

        public JsonResult ExcluirClienteTipoIntegracao()
        {
            var clienteTipoId = Request.Params["clienteTipoId"];

            try
            {

                if (String.IsNullOrEmpty(clienteTipoId))
                {
                    return Json(new
                    {
                        Message = "Nenhum tipo de integração foi informado!",
                        Success = false
                    });
                }

                _painelExpedicaoService.ExcluirClienteTipoIntegracao(Convert.ToInt32(clienteTipoId));

            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Message = "Falha na exclusão do tipo de integração!",
                    Success = false
                });
            }

            return Json(new
            {
                Message = "Tipo de Integração excluído com sucesso!",
                Success = true
            });
        }

        /// <summary>
        /// Retorna se os vagões estão válidos
        /// </summary>
        /// <param name="vagoes">Lista de vagões</param>
        /// <returns>Action result com a tela</returns>
        public ActionResult ValidarVagoes(List<string> vagoes)
        {
            var result = _painelExpedicaoService.VerificarVagoes(vagoes);

            return Json(new
            {
                Items = result.Select(c => new
                {
                    Vagao = c.Key,
                    Erro = c.Value.Key,
                    Mensagem = c.Value.Value
                }),
                success = true
            });
        }

        /// <summary>
        /// Obter dados do conteiner
        /// </summary>
        /// <param name="codigoConteiner">Código do conteiner</param>
        /// <param name="siglaAoOrigem">Sigla da AO origem do Fluxo Comercial</param> 
        /// <returns>Resultado Json</returns>
        public JsonResult ObterDadosConteiner(string codigoConteiner, string siglaAoOrigem)
        {
            bool erro = false;
            string mensagem = string.Empty;
            bool voarConteiner = false;
            int? idConteiner = 0;
            var codAoOrigem = _areaOperacionalRepository.ObterPorCodigo(siglaAoOrigem);

            try
            {
                if (codAoOrigem == null)
                {
                    erro = true;
                    mensagem = "Não foi encontrado nenhuma estação de origem com este código. '" + siglaAoOrigem + "'";
                }
                else
                {

                    Domain.Model.Trem.Veiculo.Conteiner.Conteiner conteiner = _carregamentoService.VerificarConteinerPorCodigo(codigoConteiner,
                                                                                           codAoOrigem.Id.Value);
                    if (conteiner == null)
                    {
                        erro = true;
                        mensagem = "Não foi encontrado nenhum conteiner com este código. '" + codigoConteiner + "'";
                    }
                    else
                    {
                        idConteiner = conteiner.Id;
                        ConteinerLocalVigente localConteiner = _carregamentoService.VerificarLocalConteiner(conteiner,
                                                                                                            codAoOrigem.
                                                                                                                Id.Value);

                        if (localConteiner != null)
                        {
                            voarConteiner = true;
                            mensagem =
                                string.Format(
                                    "O conteiner {0} se encontra em local diferente da origem do Fluxo comercial.\nLocal do Conteiner : {1}.",
                                    conteiner.Numero, localConteiner.EstacaoOrigem.Codigo);
                        }
                    }
                }

            }
            catch (Exception e)
            {
                mensagem = TratarException(e);
                erro = true;
            }

            return Json(new
            {
                Voar = voarConteiner,
                Erro = erro,
                idConteiner,
                Mensagem = mensagem,
                codAoOrigem = codAoOrigem != null ? codAoOrigem.Id.Value : 0
            });
        }

        /// <summary>
        /// Voa o vagão para a estação
        /// </summary>
        /// <param name="idConteiner">Número do conteiner</param>
        /// <param name="codAoOrigem"> Id da área operacional mãe do destino do contêiner</param>
        /// <returns>Resultado Json</returns>
        public JsonResult VoarConteiner(string idConteiner, int codAoOrigem)
        {
            bool erro = false;
            string mensagem = string.Empty;

            try
            {
                _carregamentoService.VoarConteiner(idConteiner, codAoOrigem, UsuarioAtual.Codigo);
            }
            catch (Exception e)
            {
                mensagem = TratarException(e);
                erro = true;
            }

            return Json(new
            {
                Erro = erro,
                Mensagem = mensagem
            });
        }

        #endregion

        #region Métodos

        private IList<string> RetornaColunasCustomizadas(bool historicoFaturamento)
        {
            IList<string> colunas = new List<string>();

            if (historicoFaturamento == false)
            {
                colunas.Add("Vagão");
                colunas.Add("Data Carga");
                colunas.Add("Fluxo");
                colunas.Add("Segmento");
                colunas.Add("Origem");
                colunas.Add("Destino");
                colunas.Add("Correntista");
                colunas.Add("Expedidor");
                colunas.Add("Remetente");
                colunas.Add("Recebedor");
                colunas.Add("Mercadoria");
                colunas.Add("Os");
                colunas.Add("Prefixo");
                colunas.Add("Seq");
                colunas.Add("Ticket");
                colunas.Add("Nº CTE");
                colunas.Add("Nfe");
                colunas.Add("Danfe");
                colunas.Add("PDFs Recebidos");
                colunas.Add("PDFs Total");
                colunas.Add("XML Nfe");
                colunas.Add("DCL");
                colunas.Add("Laudo Mercadoria");
                colunas.Add("Usuário");
            }
            else
            {
                colunas.Add("Vagão");
                colunas.Add("Data Carga");
                colunas.Add("Fluxo");
                colunas.Add("Segmento");
                colunas.Add("Origem");
                colunas.Add("Destino");
                colunas.Add("Correntista");
                colunas.Add("Expedidor");
                colunas.Add("Remetente");
                colunas.Add("Recebedor");
                colunas.Add("Mercadoria");
                colunas.Add("Os");
                colunas.Add("Prefixo");
                colunas.Add("Seq");
                colunas.Add("Ticket");
                colunas.Add("Nº CTE");
                colunas.Add("Nfe");
                colunas.Add("Danfe");
                colunas.Add("PDFs Recebidos");
                colunas.Add("PDFs Total");
                colunas.Add("XML Nfe");
                colunas.Add("DCL");
                colunas.Add("Laudo Mercadoria");
                colunas.Add("Usuário");
            }

            return colunas;
        }

        private string TratarException(Exception e)
        {
            string mensagem = string.Empty;
            Regex regex = new Regex("#.*#");

            mensagem = regex.Match(e.Message).Success ? regex.Match(e.Message).Value : string.Empty;

            if (string.IsNullOrEmpty(mensagem))
            {
                return e.Message;
            }

            return mensagem.Substring(1, mensagem.Length - 2);
        }

        #endregion

        private ActionResult ExcelPedraCustom(string fileName, List<Dictionary<string, Func<VwPedraRealizado, string>>> dicList, List<List<VwPedraRealizado>> dataList)
        {
            Stream returnContent = GetFromComponentPedra(dicList, dataList);

            var fsr = new FileStreamResult(returnContent, "application/ms-excel");
            fsr.FileDownloadName = fileName;
            return fsr;
            // return new ExcelResult(fileName, returnContent);
        }

        private Stream GetFromComponentPedra(List<Dictionary<string, Func<VwPedraRealizado, string>>> dicList, List<List<VwPedraRealizado>> dataList)
        {
            string tempFilePath = Path.GetTempFileName();
            Workbook workbook = new Workbook();
            for (int x = 0; x < dicList.Count(); x++)
            {
                workbook.Worksheets.Add(GetWorkSheetPedra(dicList[x], dataList[x], "Tabela_" + (x + 1)));
            }
            workbook.Save(tempFilePath);

            StreamReader sr = new StreamReader(tempFilePath);
            MemoryStream ms = new MemoryStream();
            StreamUtils.Copy(sr.BaseStream, ms, new byte[4096]);
            sr.Close();
            System.IO.File.Delete(tempFilePath);
            ms.Seek(0, SeekOrigin.Begin);
            return ms;
        }

        private Worksheet GetWorkSheetPedra<T>(Dictionary<string, Func<T, string>> fields, IList<T> list, string workSheetName)
        {
            Worksheet worksheet = new Worksheet(workSheetName);

            int contador = 0;
            foreach (string key in fields.Keys)
            {
                worksheet.Cells[0, contador] = new Cell(key);
                contador++;
            }

            int linhas = 0;
            for (int i = 0; i < list.Count; i++)
            {
                contador = 0;
                foreach (var key in fields.Keys)
                {
                    Func<T, string> valor = fields[key];
                    string valorString = valor.Invoke(list[i]);
                    valorString = valorString.Replace("\n", " ");
                    valorString = valorString.Replace("\r", " ");
                    worksheet.Cells[i + 1, contador] = new Cell(valorString);
                    contador++;
                }

                linhas++;
            }

            linhas++;
            for (int i = linhas; i < linhas + 100; i++)
            {
                for (int z = 0; z < 8; z++)
                {
                    worksheet.Cells[i + 1, z] = new Cell(" ");
                }
            }

            return worksheet;
        }
    }
}
