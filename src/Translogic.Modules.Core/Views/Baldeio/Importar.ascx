﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>

<div id="div-importar">
</div>

 <script type="text/javascript">

     var idBaldeio = '<%=ViewData["idBaldeio"] %>';
     var filtrolocal = '<%=ViewData["Local"] %>';
     var filtrodtInicial = '<%=ViewData["DtInicial"] %>';
     var filtrodtFinal = '<%=ViewData["DtFinal"] %>';
     var filtrovagaoCedente = '<%=ViewData["VagaoCedente"] %>';
     var filtrovagaoRecebedor = '<%=ViewData["VagaoRecebedor"] %>';

     var hdnIdBaldeio = { xtype: 'hidden', id: 'idBaldeio', name: 'idBaldeio', value: idBaldeio };
     var hdnLocal = { xtype: 'hidden', id: 'Local', name: 'Local', value: filtrolocal };
     var hdnDtInicial = { xtype: 'hidden', id: 'DtInicial', name: 'DtInicial', value: filtrodtInicial };
     var hdnDtFinal = { xtype: 'hidden', id: 'DtFinal', name: 'DtFinal', value: filtrodtFinal };
     var hdnVagaoCedente = { xtype: 'hidden', id: 'VagaoCedente', name: 'VagaoCedente', value: filtrovagaoCedente };
     var hdnVagaoRecebedor = { xtype: 'hidden', id: 'VagaoRecebedor', name: 'VagaoRecebedor', value: filtrovagaoRecebedor };

     var fileupload = new Ext.ux.form.FileUploadField({
         id: 'btnImportarArquivo',
         allowBlank: false,
         name: 'documentURL',
         buttonText: 'Importar',
         grow: true,
         emptyText: 'anexar carta',
         listeners: {
             afterrender: function () {


                 $("#btnImportarArquivo-file").change(function (event) {
                     var file = event.target.value;
                     var extension = file.split('.').pop();
                     var Arquivo = file.substring(file.lastIndexOf('\\') + 1);

                     Ext.getCmp("btnConfirmar").setDisabled(true);

                     if (Arquivo.length > 40) {
                         Ext.getCmp("btnImportarArquivo").setValue("");
                         Ext.Msg.show({
                             title: 'Aviso',
                             msg: 'Nome do arquivo superior a 40 caracteres.',
                             buttons: Ext.Msg.OK,
                             icon: Ext.MessageBox.WARNING
                         });
                     } else {
                         Ext.getCmp("btnConfirmar").setDisabled(false);
                     }

                     //Verifica EXTENSÃO ARQUIVO
                     if (extension != "pdf") {
                         Ext.getCmp("btnImportarArquivo").setValue("");
                         Ext.Msg.show({
                             title: 'Aviso',
                             msg: 'Apenas extensão .pdf é permitida para importação.',
                             buttons: Ext.Msg.OK,
                             icon: Ext.MessageBox.WARNING
                         });
                     } // if
                     else {
                         Ext.getCmp("btnConfirmar").setDisabled(false);
                     }
                 }); //change
             } //afterrender
         }//listeners
     });

     var frmbtnImportar = {
         width: 200,
         layout: 'form',
         border: false,
         items: [fileupload]
     };

     var formImportar = new Ext.form.FormPanel({
         id: 'formImportar',
         name: 'formImportar',
         width: 310,
         border: true,
         autoScroll: false,
         url: '<%= Url.Action("Importar", "Baldeio") %>',
         standardSubmit: true,
         bodyStyle: 'padding: 15px',
         labelAlign: 'top',
         items:
		 [

             hdnIdBaldeio, hdnLocal, hdnDtInicial, hdnDtFinal, hdnVagaoCedente, hdnVagaoRecebedor,
             frmbtnImportar
		],
         buttonAlign: "center",
         buttons: [
            {
                text: 'Confirmar',
                id: 'btnConfirmar',
                type: 'submit',
                iconCls: 'icon-save',
                disabled: true,
                formBind: true,
                handler: function (b, e) {

                    if (idBaldeio == "") {

                        Ext.Msg.show({
                            title: 'Aviso',
                            msg: 'Id do Baldeio não encontrado.',
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.WARNING
                        });
                    }
                    else {

                        if (fileupload.getValue() == "") {
                            Ext.Msg.show({
                                title: 'Aviso',
                                msg: 'Arquivo não fornecido',
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.WARNING
                            });
                        }
                        else {

                            Ext.getBody().mask("Carregando arquivo, isso pode levar alguns minutos, por favor aguarde a finalização", "x-mask-loading");

                            var url = '<%= Url.Action("ConfirmarImportacao", "Baldeio") %>';
                            $("#formImportar").find("form").attr("action", url);
                            $("#formImportar").find("form").attr("encoding", "multipart/form-data");

                            formImportar.getForm().submit();
                        }
                    }
                }
            },
            {
                text: 'Cancelar',
                type: 'submit',
                handler: function (b, e) {
                    window.wModalArquivo.close();
                }
            }
        ]
     });

    /******************************* RENDER *******************************/
    Ext.onReady(function () {

        formImportar.render("div-importar");
    });

 </script>