﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Interna.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content runat="server" ID="HeadContent" ContentPlaceHolderID="HeadContent">
    <%
        var permissoes = ViewData["Permissoes"] != null ? ViewData["Permissoes"] as Translogic.Modules.Core.Domain.Model.Dto.Baldeio.BaldeioPermissaoDto : new Translogic.Modules.Core.Domain.Model.Dto.Baldeio.BaldeioPermissaoDto();
        // Variável para retorno Json com mensagem
        var mensagens = ViewData["Mensagens"] != null ? ViewData["Mensagens"].ToString() : string.Empty;

        // Filtros
        var Local = ViewData["Local"] != null ? ViewData["Local"].ToString() : string.Empty;
        var DtInicial = ViewData["DtInicial"] != null ? ViewData["DtInicial"].ToString() : string.Empty;
        var DtFinal = ViewData["DtFinal"] != null ? ViewData["DtFinal"].ToString() : string.Empty;
        var VagaoCedente = ViewData["VagaoCedente"] != null ? ViewData["VagaoCedente"].ToString() : string.Empty;
        var VagaoRecebedor = ViewData["VagaoRecebedor"] != null ? ViewData["VagaoRecebedor"].ToString() : string.Empty;         
    %>
    <%--Importação JavaScripts Externos--%>
    <script src="/GetFile.efh?file={Translogic.Core.Infrastructure.Web;}/Content/Javascript/Utilfunctions.js"> </script>
    
    <script type="text/javascript">
        //Variaveis controle de ações por tipo de usuario
        var AcaoPesquisar = '<%=permissoes.Pesquisar %>';
        var AcaoImportar = '<%=permissoes.Importar %>';



        //*****STORES*****
        var gridStore = new Ext.data.JsonStore
        ({
            root: "Items",
            totalProperty: 'Total',
            id: 'gridStore',
            name: 'gridStore',
            proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("ObterConsultaBaldeio", "Baldeio") %>', timeout: 600000 }),
            paramNames:
            {
                sort: "detalhesPaginacaoWeb.Sort",
                dir: "detalhesPaginacaoWeb.Dir",
                start: "detalhesPaginacaoWeb.Start",
                limit: "detalhesPaginacaoWeb.Limit"
            },
            fields: ['IdBaldeio', 'IdArquivoBaldeio', 'Local', 'DtBaldeio', 'VagaoCedente', 'VagaoRecebedor', 'QuantidadeBaldeada', 'QtdFicouVagCedente', 'Despacho', 'DtDespacho', 'HabilitarPdf', 'HabilitarExcluir']
        });

        var sm = new Ext.grid.CheckboxSelectionModel
        ({
            listeners: {
                rowselect: function (check, rowIndex, record) {
                    habilitarImportar(check);
                },
                rowdeselect: function (check, rowIndex, record) {
                    habilitarImportar(check);
                }
            }
        });

        Ext.onReady(function () {


            //****CAMPOS*****
            var txtLocal =
            {
                xtype: 'textfield',
                name: 'txtLocal',
                id: 'txtLocal',
                fieldLabel: 'Local',
                autoCreate: {
                    tag: 'input',
                    type: 'text',
                    autocomplete: 'off',
                    maxlength: '3'
                },
                maskRe: /[a-zA-Z]/,
                style: 'text-transform:uppercase;',
                width: 37
            };

            var txtDtInicial = new Ext.form.DateField({
                fieldLabel: 'Data Inicial',
                id: 'txtDtInicial',
                name: 'txtDtInicial',
                dateFormat: 'd/n/Y',
                width: 83,
                plugins: [new Ext.ux.InputTextMask('99/99/9999', true)]
            });


            var txtDtFinal = new Ext.form.DateField({
                fieldLabel: 'Data Final',
                id: 'txtDtFinal',
                name: 'txtDtFinal',
                dateFormat: 'd/n/Y',
                width: 83,
                plugins: [new Ext.ux.InputTextMask('99/99/9999', true)]
            });

            var txtVagaoCedente = {
                xtype: 'textfield',
                name: 'txtVagaoCedente',
                id: 'txtVagaoCedente',
                fieldLabel: 'Vagão Cedente',
                autoCreate: {
                    tag: 'input',
                    type: 'text',
                    autocomplete: 'off',
                    maxlength: '7'
                },
                maskRe: /[0-9]/,
                style: 'text-transform:uppercase;',
                width: 80
            };

            var txtVagaoRecebedor = {
                xtype: 'textfield',
                name: 'txtVagaoRecebedor',
                id: 'txtVagaoRecebedor',
                fieldLabel: 'Vagão Recebedor',
                autoCreate: {
                    tag: 'input',
                    type: 'text',
                    autocomplete: 'off',
                    maxlength: '7'
                },
                maskRe: /[0-9]/,
                style: 'text-transform:uppercase;',
                width: 80
            };

            //***** Form de campos da linha inicial *****//
            var formtxtLocal =
            {
                width: 47,
                layout: 'form',
                border: false,
                items: [txtLocal]
            };

            var formtxtDtInicial =
            {
                width: 93,
                layout: 'form',
                border: false,
                items: [txtDtInicial]
            };

            var formtxtDtFinal =
            {
                width: 93,
                layout: 'form',
                border: false,
                items: [txtDtFinal]
            };

            var formtxtVagaoCedente =
            {
                width: 90,
                layout: 'form',
                border: false,
                items: [txtVagaoCedente]
            };

            var formtxtVagaoRecebedor =
            {
                width: 95,
                layout: 'form',
                border: false,
                items: [txtVagaoRecebedor]
            };

            var lineTop =
            {
                layout: 'column',
                border: false,
                items: [formtxtLocal, formtxtDtInicial, formtxtDtFinal, formtxtVagaoCedente, formtxtVagaoRecebedor]
            };

            var fieldset = {
                xtype: 'fieldset',
                border: false,
                items: [lineTop]
            };


            //***** GRID *****//

            // Toolbar da Grid
            var pagingToolbar = new Ext.PagingToolbar({
                pageSize: 50,
                store: gridStore,
                displayInfo: true,
                displayMsg: App.Resources.Web.GridExibindoRegistros,
                emptyMsg: App.Resources.Web.GridSemRegistros,
                paramNames: {
                    start: "detalhesPaginacaoWeb.Start",
                    limit: "detalhesPaginacaoWeb.Limit"
                }
            });

            var Actions = new Ext.ux.grid.RowActions({
                id: 'acoes',
                dataIndex: '',
                header: 'Ações',
                align: 'center',
                autoWidth: false,
                width: 60,
                actions: [
                            { iconCls: 'icon-pdf', tooltip: 'Download do arquivo importado', hideIndex: 'HabilitarPdf' },
                            { iconCls: 'icon-del', tooltip: 'Excluir arquivo importado', hideIndex: 'HabilitarExcluir' }
                         ],
                callbacks: {

                    'icon-del': function (grid, record, action, row, col) {
                        removerAnexo(record);
                    },
                    'icon-pdf': function (grid, record, action, row, col) {
                        downloadArquivoBaldeio(record.data.IdArquivoBaldeio);
                    }
                }
            });

            var cm = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true
                },
                plugins: Actions,
                columns: [
                    new Ext.grid.RowNumberer(),
                    sm,
                    Actions,
                    { header: 'IdBaldeio', dataIndex: "IdBaldeio", sortable: false, hidden: true, width: 40 },
                    { header: 'IdArquivoBaldeio', dataIndex: "IdArquivoBaldeio", sortable: false, hidden: true, width: 40 },
                    { header: 'Local', dataIndex: "Local", sortable: false, width: 40 },
                    { header: 'Data Baldeio', dataIndex: "DtBaldeio", sortable: false, width: 60 },
                    { header: 'Vagão Cedente', dataIndex: "VagaoCedente", sortable: false, width: 50 },
                    { header: 'Vagão Recebedor', dataIndex: "VagaoRecebedor", sortable: false, width: 50 },
                    { header: 'Quantidade Baldeada', dataIndex: "QuantidadeBaldeada", sortable: false, width: 60 },
                    { header: 'Quantidade ficou no vagão Cedente', dataIndex: "QtdFicouVagCedente", sortable: false, width: 60 },
                    { header: 'Despacho', dataIndex: "Despacho", sortable: false, width: 60 },
                    { header: 'Data Despacho', dataIndex: "DtDespacho", sortable: false, width: 60 }
                ]
            });


            var grid = new Ext.grid.EditorGridPanel({
                id: 'grid',
                name: 'grid',
                autoLoadGrid: false,
                height: 450,
                stripeRows: true,
                cm: cm,
                region: 'center',
                viewConfig: {
                    forceFit: true,
                    emptyText: 'Não possui dado(s) para exibição.'
                },
                loadMask: { msg: App.Resources.Web.Carregando },
                store: gridStore,
                plugins: [Actions],
                bbar: pagingToolbar,
                sm: sm
            });

            grid.getStore().proxy.on('beforeload', function (p, params) {
                //Parametros de pesquisa(filtros) grid
                params['baldeioDto'] = $.toJSON(criarObjetoBaldeio());
            });

            var painelFiltros = new Ext.form.FormPanel
             ({
                 id: 'painelFiltro',
                 layout: 'form',
                 labelAlign: 'top',
                 standardSubmit: true,
                 url: '<%= Url.Action("upload", "Tfa") %>',
                 border: false,
                 autoHeight: true,
                 buttonAlign: "center",
                 region: 'north',
                 title: "",
                 bodyStyle: 'padding: 5px 10px 0px 10px',
                 items: [fieldset],
                 buttons:
                [{
                    id: 'btnPesquisar',
                    text: 'Pesquisar',
                    tooltip: 'Pesquisar',
                    type: 'button',
                    iconCls: 'icon-find',
                    handler: function (c) {
                        pesquisar();
                    }
                },
                {
                    id: 'btnLimpar',
                    text: 'Limpar',
                    tooltip: 'Limpar Filtros',
                    type: 'button',
                    iconCls: 'icon-delete',
                    handler: function (b, e) {
                        limparForm();
                    }
                },
                {
                    id: 'btnImportar',
                    text: 'Importar',
                    tooltip: 'Importar Carta',
                    type: 'button',
                    disabled: true,
                    iconCls: 'icon-up',
                    handler: function (b, e) {
                        if (sm.getSelections().length == 1) {
                            importar('<%=Url.Action("Importar")%>', sm.getSelections()[0].data.IdBaldeio);
                        }
                    }
                }
                ,
                {
                    id: 'btnExportarExcel',
                    text: 'Exportar',
                    tooltip: 'Exportar para Excel',
                    type: 'submit',
                    iconCls: 'icon-page-excel',
                    handler: function (b, e) {
                        exportarExcel('<%= Url.Action("ExportarXlsBaldeio", "Baldeio") %>');
                    }
                }
                ]
             });

            var painelGrid = new Ext.form.FormPanel
            ({
                id: 'painelGrid',
                layout: 'form',
                labelAlign: 'top',
                border: false,
                autoHeight: true,
                title: "",
                region: 'center',
                bodyStyle: 'padding: 0px 10px 0px 10px',
                items: [grid]
            });

            // Rotinas para rodar após criação dos campos
            painelFiltros.render("divFiltros");
            painelGrid.render("divGrid");
            confirmacaoImportacao();            
        });


        function pesquisar() {
            if (validarFiltros()) {
                sm.clearSelections();
                gridStore.load({ params: { start: 0, limit: 50} });
            }
        };

        function limparForm() {
            var grid = Ext.getCmp("grid");

            grid.getStore().removeAll();

            Ext.getCmp("painelFiltro").getForm().reset();
        };

        function exportarExcel(url) {
            if (validarFiltros()) {

                loader("Processando dados...");

                url += "?baldeioDto=" + $.toJSON(criarObjetoBaldeio());

                window.open(url, "");

                closeLoader();
            }
        }

        function validarFiltros() {

            var baldeio = criarObjetoBaldeio();

            //Verificação local é texto(valido)
            if (isNumber(baldeio.local)) {
                alerta("Apenas letras são permitidas no Campo Local.");
                return false;
            }

            if (baldeio.dtInicial == null || baldeio.dtFinal == null) {
                alerta("Ambos os filtros de Data são obrigatórios");
                return false;
            }

            //Validação se data inicio é maior que data final
            if (baldeio.dtInicial != null || baldeio.dtFinal != null) {

                if (comparaDatas(Ext.getCmp("txtDtInicial").getRawValue(), Ext.getCmp("txtDtFinal").getRawValue())) {
                    alerta("Data final deve ser superior a data inicial nos filtros");
                    return false;
                }
            }
            return true;
        };
        
        //OBJETO TELA
        function criarObjetoBaldeio() {
            var dateParts;
            var baldeioDto = new Object();

            baldeioDto.local = Ext.getCmp("txtLocal").getValue();

            //Converte data inicial para formato MM/DD/YYYY para controller conseguir identificar
            dateParts = (Ext.getCmp("txtDtInicial").getRawValue()).split("/");
            if (dateParts.length > 1) {
                baldeioDto.dtInicial = dateParts[1] + "/" + dateParts[0] + "/" + dateParts[2];
            } else {
                baldeioDto.dtInicial = null;
            }

            //Converte data final para formato MM/DD/YYYY para controller conseguir identificar
            dateParts = (Ext.getCmp("txtDtFinal").getRawValue()).split("/");
            if (dateParts.length > 1) {
                baldeioDto.dtFinal = dateParts[1] + "/" + dateParts[0] + "/" + dateParts[2];
            } else {
                baldeioDto.dtFinal = null;
            }

            baldeioDto.vagaoCedente = Ext.getCmp("txtVagaoCedente").getValue().trim();
            baldeioDto.vagaoRecebedor = Ext.getCmp("txtVagaoRecebedor").getValue().trim();
            return baldeioDto;
        }

        function downloadArquivoBaldeio(Id) {

            var url = '<%= Url.Action("ExportarCartaPdf") %>';
            url += "?idArquivoBaldeio=" + Id;

            window.open(url, "");
        };

        function habilitarImportar(sm) {
            if (sm.getSelections().length == 1 && AcaoImportar == 'True') {
                if (sm.getSelections()[0].data.IdArquivoBaldeio == 0)
                    Ext.getCmp("btnImportar").setDisabled(false);
                else
                    Ext.getCmp("btnImportar").setDisabled(true);
            }
            else {
                Ext.getCmp("btnImportar").setDisabled(true);
            }
        }

        function importar(urlImportar, idBaldeio) {

            var local = Ext.getCmp("txtLocal").getRawValue();
            var dtInicial = Ext.getCmp("txtDtInicial").getRawValue();
            var dtFinal = Ext.getCmp("txtDtFinal").getRawValue();
            var vagaoCedente = Ext.getCmp("txtVagaoCedente").getRawValue();
            var vagaoRecebedor = Ext.getCmp("txtVagaoRecebedor").getRawValue();

            //*****JANELA MODAL PARA UPLOAD DE ARQUIVOS*****
            var modalArquivo = new Ext.Window
            ({
                id: 'modalArquivo',
                title: 'Importar Carta',
                modal: true,
                width: 324,
                height: 147,
                resizable: false,
                autoScroll: false,
                autoLoad:
                    {
                        url: urlImportar,
                        params: { idBaldeio: idBaldeio, local: local, dtInicial: dtInicial, dtFinal: dtFinal, vagaoCedente: vagaoCedente, vagaoRecebedor: vagaoRecebedor },
                        text: "Abrindo importação...",
                        scripts: true,
                        callback: function (el, sucess) {
                            if (!sucess) {
                                formModal.close();
                            }
                        }
                    },
                listeners:
                    {
                        resize: function (win, width, height, eOpts) {
                            win.center();
                        }
                    }
            });

            window.wModalArquivo = modalArquivo;
            modalArquivo.show(this);
        };

        function confirmacaoImportacao() {

            var local = '<%=Local%>';
            var dtInicial = '<%=DtInicial%>';
            var dtFinal = '<%=DtFinal%>';
            var vagaoCedente = '<%=VagaoCedente%>';
            var vagaoRecebedor = '<%=VagaoRecebedor%>';

            var mensagens = '<%=mensagens %>';

            if (local != "" || dtInicial != "" || dtFinal != "" || vagaoCedente != "" || vagaoRecebedor != "" || mensagens != "") {

                if (local != "") {
                    Ext.getCmp("txtLocal").setValue(local);
                }

                if (dtInicial != "") {
                    Ext.getCmp("txtDtInicial").setValue(dtInicial);
                }

                if (dtFinal != "") {
                    Ext.getCmp("txtDtFinal").setValue(dtFinal);
                }

                if (vagaoCedente != "") {
                    Ext.getCmp("txtVagaoCedente").setValue(vagaoCedente);
                }

                if (vagaoRecebedor != "") {
                    Ext.getCmp("txtVagaoRecebedor").setValue(vagaoRecebedor);
                }

                gridStore.load({ params: { start: 0, limit: 50} });

                if (mensagens != null && mensagens != "") {
                    alerta(mensagens);
                }
            }
        }

        function removerAnexo(record) {
            if (record.data.IdArquivoBaldeio == 0) {
                alerta("Arquivo não encontrado para este registro");
            }
            else {

                if (Ext.Msg.confirm("Excluir", "Deseja excluir arquivo?", function (btn, text) {
                    if (btn == 'yes') {
                        loader("Excluindo arquivo, isso pode levar alguns minutos, por favor aguarde a finalização");
                        confirmarExclusao(record.data.IdArquivoBaldeio);
                    }
                }));
            }
        }

        function confirmarExclusao(idArquivoBaldeio) {

            var url = '<%= Url.Action("ExcluirCartaPdf") %>';
            url += "?idArquivoBaldeio=" + idArquivoBaldeio;

            $.ajax({
                url: url,
                type: "POST",
                dataType: 'json',
                async: false,
                timeout: 300000,
                contentType: "application/json; charset=utf-8",
                failure: function (conn, data) {
                    Ext.Msg.alert("Mensagem de Erro", "Ocorreu um erro inesperado");
                    Ext.getBody().unmask();
                },
                success: function (data) {
                    if (data != null) {
                        if (data.sucesso) {
                            pesquisar();
                        }
                        else {
                            alerta(data.message, Ext.MessageBox.ERROR);
                        }
                    }
                    closeLoader();
                }
            });
        }

        function ExportarClick() {

        }
        
    </script>
</asp:Content>
<asp:Content runat="server" ID="Content2" ContentPlaceHolderID="Content">
    <div id="header-content">
        <h1>
            Consulta Baldeio</h1>
        <small>Você está em Consulta > Vagões > Consulta Baldeio</small>
        <br />
    </div>
    <div id="divFiltros">
    </div>
    <div id="divGrid">
    </div>
</asp:Content>
