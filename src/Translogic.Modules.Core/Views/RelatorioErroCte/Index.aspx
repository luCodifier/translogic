﻿<%@ Import Namespace="System.Security.Policy" %>

<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Interna.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Import Namespace="Translogic.Modules.Core.Domain.Model.Diversos.Cte" %>
<asp:Content runat="server" ID="HeadContent" ContentPlaceHolderID="HeadContent">
	<script type="text/javascript">
	
		var grid = null;
		var ds = null;

		function showResult(btn) {
			executarPesquisa = true;
		}

		function Pesquisar()
		{
			var diferenca = Ext.getCmp("filtro-data-final").getValue() - Ext.getCmp("filtro-data-inicial").getValue(); 
			diferenca = Math.floor(diferenca / (1000 * 60 * 60 * 24)); 

			if (Ext.getCmp("filtro-data-inicial").getValue() == '' || Ext.getCmp("filtro-data-final").getValue() == '') {
				executarPesquisa = false;
				Ext.Msg.show({
				title: "Mensagem de Informação",
				msg: "Preencha os filtro datas!",
				buttons: Ext.Msg.OK,
				icon: Ext.MessageBox.INFO,
				minWidth: 200,
				fn: showResult
				});
			}
			else if (diferenca > 30) {
				executarPesquisa = false;
				Ext.Msg.show({
				title: "Mensagem de Informação",
				msg: "O período da pesquisa não deve ultrapassar 30 dias",
				buttons: Ext.Msg.OK,
				icon: Ext.MessageBox.INFO,
				minWidth: 200,
				fn: showResult
				});
			}
			else {
					grid.getStore().load();
				}
			}

			function FormSuccess(form, action) {
				Ext.getCmp("gridCteRaiz").getStore().removeAll();
				Ext.getCmp("gridCteRaiz").getStore().loadData(action.result, true);
			}
			
			function FormError(form, action) {
				Ext.Msg.alert('Mensagem de Erro', action.result.Message);
				ds.removeAll();
			}

			var storeImpressao = new Ext.data.JsonStore({
				root: "Items",
				fields: [
						'CteId',
						'NroCte',
						'Status'
				]});

     /* ------------------------------------------------------- */
 
    $(function () {
		
	 function statusRenderer(val)
      {
        switch(val){
            case "AUT":
                return "<img src='<%=Url.Images("Icons/tick.png") %>' alt='CTe Aprovado'>";
                break;
            case "EAE":
                return "<img src='<%=Url.Images("Icons/email_go.png") %>' alt='Enviado arquivo CTe para Sefaz.'>";
                break;
            case "EAR":
                return "<img src='<%=Url.Images("Icons/arrow_redo_yellow.png") %>' alt='Erro Autorizado reenvio do CTe.'>";
                break;            
            case "CAN":
                return "<img src='<%=Url.Images("Icons/cancel.png") %>' alt='CTe Cancelado '>";
                break;                        
             case "INV":
                return "<img src='<%=Url.Images("Icons/delete.png") %>' alt='CTe Invalidado'>";
                break;                        
             case "AGC":
                return "<img src='<%=Url.Images("Icons/lock_edit.png") %>' alt='Aguardando cancelamento.'>";
                break;                                    
             case "AUC":
                return "<img src='<%=Url.Images("Icons/lock_open.png") %>' alt='Autorizado cancelamento.'>";
                break;
             case "ERR":
                return "<img src='<%=Url.Images("Icons/cross.png") %>' alt='Erro na geração do CTe.'>";
                break;            
            default:
                return "<img src='<%=Url.Images("Icons/alert.png") %>' alt='Cte aguardando a geração automática da numeração / chave.'>";
                break;
        }
      }                  
           
	grid = new Translogic.PaginatedGrid({
		autoLoadGrid: false,
		id: "gridCteRaiz", 
		height: 300,
		width: 800,			
		url: '<%= Url.Action("ObterCtes") %>',
		region: 'center',
		viewConfig: {
			forceFit: false,
			getRowClass: MudaCor
		},
		fields: [
			'CteId',
			'Fluxo',
			'Origem',
			'Destino',
			'Mercadoria',                
			'Chave',
			'Cte',
			'SituacaoCte',
			'DateEmissao',             
			'Impresso',
			'CodigoErro',
			'FerroviaOrigem',
			'FerroviaDestino',
			'UfOrigem',
			'CodigoVagao',
			'UfDestino',
			'Serie',
			'Despacho',          
			'SerieDesp5', 
			'Desp5',
			'ArquivoPdfGerado',
			'AcaoSerTomada'
		],
		filteringToolbar: [{
			id: 'btnExportar',
			text: 'Exportar',
			tooltip: 'Exportar',
			iconCls: 'icon-page-excel',
			handler: function (c) {
				
				if(grid.getStore().getCount() > 0){
					
					var dataInicio = Ext.getCmp("filtro-data-inicial").getValue().format('d/m/Y'); 
					var dataFim = Ext.getCmp("filtro-data-final").getValue().format('d/m/Y');  
					var fluxo = Ext.getCmp("filtro-fluxo").getValue();
					var origem = Ext.getCmp("filtro-Ori").getValue();
					var destino = Ext.getCmp("filtro-Dest").getValue();

					var url =  "<%= Url.Action("Exportar") %>";
				
					url += "?dataInicial=" + dataInicio + "&dataFinal=" + dataFim + "&codFluxo=" + fluxo + "&origem=" + origem +"&destino=" + destino ;

					window.open(url,"");			
				}
			}
		}],
		columns: [ 
			{ dataIndex: "CteId", hidden: true },
			{ dataIndex: "Impresso", hidden: true },			
			{ header: '...', dataIndex: "SituacaoCte",  width: 25, sortable: false,renderer:statusRenderer },
			{ header: 'Fluxo', dataIndex: "Fluxo",  width: 70, sortable: false },			
			{ header: 'Num Vagão', dataIndex: "CodigoVagao", width: 100, sortable: false },
			{ header: 'Série', dataIndex: "Serie", width: 70, sortable: false },
			{ header: 'Despacho', dataIndex: "Despacho", width: 70, sortable: false },
			{ header: 'SerieDesp5', dataIndex: "SerieDesp5", width: 70, sortable: false },
			{ header: 'Desp5', dataIndex: "Desp5", width: 70, sortable: false },
			{ header: 'CTE', dataIndex: "Cte",  width: 100, sortable: false },
			{ header: 'Origem', dataIndex: "Origem", width: 50, sortable: false },
			{ header: 'Destino', dataIndex: "Destino", width: 50, sortable: false },
			{ header: 'UF Ori.', dataIndex: "UfOrigem", width: 70, sortable: false },
			{ header: 'UF Dest.', dataIndex: "UfDestino", width: 70, sortable: false },			
			{ header: 'Mercadoria', dataIndex: "Mercadoria", width: 70, sortable: false },
			{ header: 'Chave CTE', dataIndex: "Chave", width: 280, sortable: false },
			{ header: 'Data Emissão', dataIndex: "DateEmissao", width: 75, sortable: false },
			{ header: 'Ferrovia Ori.', dataIndex: "FerroviaOrigem", sortable: false },
			{ header: 'Ferrovia Dest.', dataIndex: "FerroviaDestino", sortable: false }			
			
		],
		listeners: {
			rowclick: function(grid, rowIndex, e) {
				
			},
			rowdblclick: function(grid, record, rowIndex, e) {
					grid.getSelectionModel().selectRow(item);
			}
		}
	});
       
       function MudaCor(row, index) {
            if (row.data.Impresso) {
                return 'corGreen';
            }
        }

	   ds = Ext.getCmp("gridCteRaiz").getStore();
	   grid.getStore().proxy.on('beforeload', function(p, params) {	   
	   
			params['filter[0].Campo'] = 'dataInicial';
			params['filter[0].Valor'] = new Array(Ext.getCmp("filtro-data-inicial").getValue().format('d/m/Y') + " " + Ext.getCmp("filtro-data-inicial").getValue().format('H:i:s')); 
			params['filter[0].FormaPesquisa'] = 'Start';

			params['filter[1].Campo'] = 'dataFinal';
			params['filter[1].Valor'] = new Array(Ext.getCmp("filtro-data-final").getValue().format('d/m/Y') + " " + Ext.getCmp("filtro-data-inicial").getValue().format('H:i:s'));  
			params['filter[1].FormaPesquisa'] = 'Start';

			params['filter[4].Campo'] = 'fluxo';
			params['filter[4].Valor'] = Ext.getCmp("filtro-fluxo").getValue();
			params['filter[4].FormaPesquisa'] = 'Start';

			params['filter[7].Campo'] = 'Origem';
			params['filter[7].Valor'] = Ext.getCmp("filtro-Ori").getValue();
			params['filter[7].FormaPesquisa'] = 'Start';
			
			params['filter[8].Campo'] = 'Destino';
			params['filter[8].Valor'] = Ext.getCmp("filtro-Dest").getValue();
			params['filter[8].FormaPesquisa'] = 'Start';
	   });

		var dataAtual = new Date();

		var dataInicial =	{
			xtype: 'datefield',
			fieldLabel: 'Data Inicial',
			id: 'filtro-data-inicial',
			name: 'dataInicial',
			width: 83,
			allowBlank: false,
			vtype: 'daterange',
			endDateField: 'filtro-data-final',
			hiddenName: 'dataInicial',
			value: dataAtual		
		};
	
		var dataFinal = {
			xtype: 'datefield',
			fieldLabel: 'Data Final',
			id: 'filtro-data-final',
			name: 'dataFinal',
			width: 83,
			allowBlank: false,
			vtype: 'daterange',
			startDateField: 'filtro-data-inicial',
			hiddenName: 'dataFinal'  ,
		    value: dataAtual
		};
        
		var origem =	{
			xtype: 'textfield',
			vtype: 'cteestacaovtype',
			style: 'text-transform: uppercase',
			id: 'filtro-Ori',
			fieldLabel: 'Origem',
			autoCreate: { tag: 'input', type: 'text', maxlength: '3', autocomplete: 'off' },
			name: 'Origem',
			allowBlank: true,
			maxLength: 3,
			width: 35,
			hiddenName: 'Origem'
		};

		var destino = {
			xtype: 'textfield',
			vtype: 'cteestacaovtype',
			style: 'text-transform: uppercase',
			id: 'filtro-Dest',
			fieldLabel: 'Destino',
			autoCreate: { tag: 'input', type: 'text', maxlength: '3', autocomplete: 'off' },
			name: 'Destino',
			allowBlank: true,
			maxLength: 3,
			width: 35,
			hiddenName: 'Destino'
		};

		var fluxo = {
			xtype: 'numberfield',
			style: 'text-transform: uppercase',
			id: 'filtro-fluxo',
			fieldLabel: 'Fluxo',
			name: 'fluxo',
			allowBlank: true,
			autoCreate: { tag: 'input', type: 'text', maxlength: '5', autocomplete: 'off' },
			maxLength: 20,
			width: 45,
			hiddenName: 'fluxo',
			minValue: 0,
			maxValue: 99999
		};

		var arrDataIni = {
			width: 87,
			layout: 'form',
			border: false,
			items:
				[dataInicial]
		};

		var arrDataFim = {
				    width: 87,
				    layout: 'form',
				    border: false,
				    items:
						[dataFinal]
				};

		var arrOrigem = {
                    width: 43,
                    layout: 'form',
                    border: false,
                    items:
						[origem]
                };
		var arrDestino = {
					width: 43,
					layout: 'form',
					border: false,
					items:
						[destino]
				};

		var arrFluxo = {
                    width: 50,
                    layout: 'form',
                    border: false,
                    items:
						[fluxo]
                };
		
		var arrlinha1 = {
			    layout: 'column',
			    border: false,
			    items: [arrDataIni,arrDataFim,arrFluxo, arrOrigem, arrDestino ]
			};

		var arrCampos = new Array();
		arrCampos.push(arrlinha1);

        filters = new Ext.form.FormPanel({
            id: 'grid-filtros',
            title: "Filtros",
            region: 'center',
            bodyStyle: 'padding: 15px',
            labelAlign: 'top',
            items:
			[arrCampos],
            buttonAlign: "center",
            buttons:
	        [
                 {
                    text: 'Pesquisar',
                    type: 'submit',
                    iconCls: 'icon-find',	
                    handler: function (b, e) {
                         Pesquisar();
                    }
                },
	            {
	                text: 'Limpar',
	                handler: function (b, e) {
	                    ds.removeAll();
                        
	                },
	                scope: this
	            }                
            ]
        });

        var colunaDet1 = {
                    title: 'Relatório de Erros Cte',
                    width: 800,
                    height: 320,
                    layout: 'form',
                    border: true,
                    items: [grid]
        };

        var columns = {
			    layout: 'column',
			    border: false,
				autoScroll: true,		
                region: 'center',
			    items: [colunaDet1]
        };

        new Ext.Viewport({
				layout: 'border',
				margins: 10,
				items: [
					{
						region: 'north',
						height: 190,
						width: 700,
						items: [{
							region: 'center',
							applyTo: 'header-content'
						}, filters]
					},
					columns
			]
			});
         
    });

	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
	<div id="header-content">
		<h1>
			CTe - Relatório Erros</h1>
		<small>Você está em CTe > Relatório de Erros </small>
		<br />
	</div>
</asp:Content>
