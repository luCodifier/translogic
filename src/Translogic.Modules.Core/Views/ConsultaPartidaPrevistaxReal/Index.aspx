﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Interna.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content runat="server" ID="HeadContent" ContentPlaceHolderID="HeadContent">
    <style type="text/css">
        .x-grid3-cell-first .x-grid3-cell-inner
        {
            padding-left: 0px !important;
        }
        .act-displ-hide
        {
            visibility: hidden;
        }
    </style>
    <script type="text/javascript">

        /******************************* STORE *******************************/
        var storeTrem = new Ext.data.JsonStore({
            root: "Items",
            autoLoad: false,
            sortInfo: { field: "PartidaPrevista", direction: "ASC" },
            fields: [
                'Id',
                'NroOs',
                'Trem',
                'EstacaoOrigem',
                'EstacaoDestino', 
                'PartidaPrevista',
                'PartidaRealizada'
            ]
        });    
      
        var dataAtual = new Date();

        var dataPartidaPlanejadaInicial = {
            xtype: 'datefield',
            fieldLabel: 'De',
            id: 'dataPartidaPlanejadaInicial',
            name: 'dataInicial',
            width: 83,
            allowBlank: false,
            vtype: 'daterange',
            endDateField: 'dataPartidaPlanejadaFinal',
            hiddenName: 'dataPartidaPlanejadaInicial',
            value: dataAtual
        };

        var arrdataPartidaPlanejadaInicial = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding-right:10px',
            items: [dataPartidaPlanejadaInicial]
        };

        var dataPartidaPlanejadaFinal = {
            xtype: 'datefield',
            fieldLabel: 'Para',
            id: 'dataPartidaPlanejadaFinal',
            name: 'dataPartidaPlanejadaFinal',
            width: 83,
            allowBlank: false,
            vtype: 'daterange',
            startDateField: 'dataPartidaPlanejadaInicial',
            hiddenName: 'dataPartidaPlanejadaFinal',
            value: dataAtual
        };

        var arrdataPartidaPlanejadaFinal = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding-right:10px',
            items: [dataPartidaPlanejadaFinal]
        };

        var linha2_1 = {
            layout: 'column',
            border: false,
            items: [arrdataPartidaPlanejadaInicial, arrdataPartidaPlanejadaFinal]
        };

        var fieldPartidaPlanejada = {
            xtype: 'fieldset',
            title: 'Partida Planejada', // title, header, or checkboxToggle creates fieldset header
            autoHeight: true,
            columnWidth: 0.5,
            collapsed: false, // fieldset initially collapsed
            layout: 'anchor',
            items: [linha2_1]
        };

        var dataChegadaPlanejadaInicial = {
            xtype: 'datefield',
            fieldLabel: 'De',
            id: 'dataChegadaPlanejadaInicial',
            name: 'dataInicial',
            width: 83,
            allowBlank: false,
            vtype: 'daterange',
            endDateField: 'dataChegadaPlanejadaFinal',
            hiddenName: 'dataChegadaPlanejadaInicial'
        };

        var arrdataChegadaPlanejadaInicial = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding-right:10px',
            items: [dataChegadaPlanejadaInicial]
        };

        var dataChegadaPlanejadaFinal = {
            xtype: 'datefield',
            fieldLabel: 'Para',
            id: 'dataChegadaPlanejadaFinal',
            name: 'dataChegadaPlanejadaFinal',
            width: 83,
            allowBlank: false,
            vtype: 'daterange',
            startDateField: 'dataChegadaPlanejadaInicial',
            hiddenName: 'dataChegadaPlanejadaFinal'
        };

        var arrdataChegadaPlanejadaFinal = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding-right:10px',
            items: [dataChegadaPlanejadaFinal]
        };

        var linha2_2 = {
            layout: 'column',
            border: false,
            bodyStyle: 'padding-right:10px',
            items: [arrdataChegadaPlanejadaInicial, arrdataChegadaPlanejadaFinal]
        };

        var fieldPartidaPlanejada = {
            xtype: 'fieldset',
            title: 'Partida Planejada', // title, header, or checkboxToggle creates fieldset header
            autoHeight: true,
            bodyStyle: 'padding-left:10px',
            width: 255,
            collapsed: false, // fieldset initially collapsed
            items: [linha2_1]
        };

        var fieldChegadaPlanejada = {
            xtype: 'fieldset',
            title: 'Chegada Planejada', // title, header, or checkboxToggle creates fieldset header
            autoHeight: true,
            width: 255,
            collapsed: false, // fieldset initially collapsed
            bodyStyle: 'padding-left:10px',
            items: [linha2_2]
        };

        var linha2 = {
            layout: 'column',
            border: false,
            items: [fieldPartidaPlanejada, fieldChegadaPlanejada]
        };

        var PrefixoIncluir = {
            xtype: 'textfield',
            style: 'text-transform: uppercase',
            width: 230,
            name: 'PrefixoIncluir',
            allowBlank: true,
            id: 'PrefixoIncluir',
            fieldLabel: 'Prefixos a Incluir(separe por vírgula)',
            enableKeyEvents: true
        };

        var arrPrefixoIncluir = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding-right:10px;padding-top:10px',
            items: [PrefixoIncluir]
        };

        var PrefixoExcluir = {
            xtype: 'textfield',
            style: 'text-transform: uppercase',
            width: 230,
            name: 'PrefixoExcluir',
            allowBlank: true,
            id: 'PrefixoExcluir',
            fieldLabel: 'Prefixos a Excluir(separe por vírgula)',
            enableKeyEvents: true,
            value: 'N,E,W,S,V,A'
        };

        var arrPrefixoExcluir = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding-right:10px;padding-top:10px',
            items: [PrefixoExcluir]
        };

        var linha3 = {
            layout: 'column',
            border: false,
            items: [arrPrefixoIncluir, arrPrefixoExcluir]
        };

        var btnPesquisar = {
            xtype: 'button',
            name: 'btnEstacaoPesquisar',
            id: 'btnEstacaoPesquisar',
            text: 'Pesquisar',
            iconCls: 'icon-find',
            handler: function (b, e) {
                Pesquisar();
            }
        };

        var btnLimpar = {
            xtype: 'button',
            name: 'btnEstacaoLimpar',
            id: 'btnEstacaoLimpar',
            text: 'Limpar',
            handler: function (b, e) {

                Limpar();
            }
        };
        
        var btnExportarExcel = {
            xtype: 'button',
            name: 'btnExportarExcel',
            id: 'btnExportarExcel',
            text: 'Gerar Excel',
            iconCls: 'icon-page-excel',
            handler: GerarExcel
        };

        filtros = new Ext.form.FormPanel({
            id: 'filtros',
            name: 'filtros',
            bodyStyle: 'padding: 10px',
            height: 190,
            width: 600,
            labelAlign: 'top',
            items: [linha2, linha3],
            buttonAlign: 'center',
            buttons: [btnPesquisar, btnExportarExcel, btnLimpar]
        });
        
       var grid = new Ext.grid.GridPanel({
            id: 'grid',
            name: 'grid',
            autoLoadGrid: false,
            height: 300,
            width: 1250,
            stripeRows: true,
            region: 'center',
            store: storeTrem,
            enableHdMenu: false,
            viewConfig: {
                forceFit: true
            },
            loadMask: { msg: App.Resources.Web.Carregando },
            colModel: new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true
                },
                columns: [
                    { header: 'Trem', width: 60, dataIndex: "Trem", sortable: true },
                    { header: 'Os', width: 60, dataIndex: "NroOs", sortable: true },
                    { header: 'Origem', width: 60, dataIndex: "EstacaoOrigem", sortable: true },
                    { header: 'Destino', width: 60, dataIndex: "EstacaoDestino", sortable: true },
                    { header: 'Partida Prev.', width: 120, dataIndex: "PartidaPrevista", sortable: true },
                    { header: 'Partida Real.', width: 120, dataIndex: "PartidaRealizada", sortable: true }
             ]
            })
        });
        
        /**************************************************************/
        // MÉTODOS       
        /**************************************************************/
        
        function Imprimir() {
          var partidaPlanejadaInicial = null;
            var partidaPlanejadaFinal = null;
            var chegadaPlanejadaInicial = null;
            var chegadaPlanejadaFinal = null;
            var prefixosIncluir = null;
            var prefixosExcluir = null;

            if (Ext.getCmp("dataPartidaPlanejadaInicial").getValue() != "" && Ext.getCmp("dataPartidaPlanejadaFinal").getValue() != "") {
                partidaPlanejadaInicial = new Array(Ext.getCmp("dataPartidaPlanejadaInicial").getValue().format('m/d/Y') + " " + Ext.getCmp("dataPartidaPlanejadaInicial").getValue().format('H:i:s'));
                partidaPlanejadaFinal = new Array(Ext.getCmp("dataPartidaPlanejadaFinal").getValue().format('m/d/Y') + " " + Ext.getCmp("dataPartidaPlanejadaFinal").getValue().format('H:i:s'));
            }
            
            if (Ext.getCmp("dataChegadaPlanejadaInicial").getValue() != "" && Ext.getCmp("dataChegadaPlanejadaFinal").getValue() != "") {
                chegadaPlanejadaInicial = new Array(Ext.getCmp("dataChegadaPlanejadaInicial").getValue().format('m/d/Y') + " " + Ext.getCmp("dataChegadaPlanejadaInicial").getValue().format('H:i:s'));
                chegadaPlanejadaFinal = new Array(Ext.getCmp("dataChegadaPlanejadaFinal").getValue().format('m/d/Y') + " " + Ext.getCmp("dataChegadaPlanejadaFinal").getValue().format('H:i:s'));
            }

            prefixosIncluir = Ext.getCmp('PrefixoIncluir').getValue();
            prefixosExcluir = Ext.getCmp('PrefixoExcluir').getValue();

            var url = "<%= Url.Action("Imprimir") %>";

            url += "?codigoUpOrigem=";
            url += "&codigoUpDestino=";
            url += "&estOrigem=";
            url += "&estDestino=";
            url += "&prefixo=";
            url += "&partidaPlanejadaInicial=" + partidaPlanejadaInicial;
            url += "&partidaPlanejadaFinal=" + partidaPlanejadaFinal;
            url += "&chegadaPlanejadaInicial=" + chegadaPlanejadaInicial;
            url += "&chegadaPlanejadaFinal=" + chegadaPlanejadaFinal;
            url += "&prefixosIncluir=" + prefixosIncluir;
            url += "&prefixosExcluir=" + prefixosExcluir;
            
             window.open(url, "");
        }
        
        function Pesquisar() {
            var partidaPlanejadaInicial = null;
            var partidaPlanejadaFinal = null;
            var chegadaPlanejadaInicial = null;
            var chegadaPlanejadaFinal = null;
            var prefixosIncluir = null;
            var prefixosExcluir = null;

            partidaPlanejadaInicial = Ext.getCmp('dataPartidaPlanejadaInicial').getValue();
            partidaPlanejadaFinal = Ext.getCmp('dataPartidaPlanejadaFinal').getValue();
            chegadaPlanejadaInicial = Ext.getCmp('dataChegadaPlanejadaInicial').getValue();
            chegadaPlanejadaFinal = Ext.getCmp('dataChegadaPlanejadaFinal').getValue();
            
            prefixosIncluir = Ext.getCmp('PrefixoIncluir').getValue();
            prefixosExcluir = Ext.getCmp('PrefixoExcluir').getValue();

            if (partidaPlanejadaInicial == "" && partidaPlanejadaFinal == "" && chegadaPlanejadaInicial == "" && chegadaPlanejadaFinal == "") {
                ExibirConsistencia("Ao menos um intervalo de datas deve ser informado para consulta");
                return;
            }

            if ((partidaPlanejadaInicial != "" && partidaPlanejadaFinal == "") || (partidaPlanejadaInicial == "" && partidaPlanejadaFinal != "")) {
                ExibirConsistencia("Ao menos um intervalo de datas deve ser informado para consulta");
                return;
            }

            if ((chegadaPlanejadaInicial != "" && chegadaPlanejadaFinal == "") || (chegadaPlanejadaInicial == "" && chegadaPlanejadaFinal != "")) {
                ExibirConsistencia("Ao menos um intervalo de datas deve ser informado para consulta");
                return;
            }
            
            Ext.getCmp("grid").getEl().mask("Aguarde...", "x-mask-loading");
            
             Ext.Ajax.request({                
			    url: "<% =Url.Action("ObterOnTime") %>",
			    success: function(response) {
                    var data = Ext.decode(response.responseText);
			        LimparPesquisa();
			        if (!data.Sucesso) {
			            ExibirConsistencia(data.Mensagem);
			        } else {

			            if (data.Items != "") {
			            
			                storeTrem.loadData(data);
			                // storePerformance.loadData(data);
			            }
			        }
			        Ext.getCmp("grid").getEl().unmask();
			    },
                failure: function(conn, data) {
                    Ext.getCmp("grid").getEl().unmask();
                    ExibirErro("Ocorreu um erro inesperado");
                },
			    method: "POST",
			    params: {
                         partidaPlanejadaInicial: partidaPlanejadaInicial,
                         partidaPlanejadaFinal: partidaPlanejadaFinal,
                         chegadaPlanejadaInicial: chegadaPlanejadaInicial,
                         chegadaPlanejadaFinal: chegadaPlanejadaFinal,
                         prefixosIncluir: prefixosIncluir,
                         prefixosExcluir: prefixosExcluir
				     }
		    });
        }

        function ExibirConsistencia(mensagem) {
            Ext.Msg.show({
                title: "Mensagem de Informação",
                msg: mensagem,
                buttons: Ext.Msg.OK,
                icon: Ext.MessageBox.INFO,
                minWidth: 200
            });
        }
        
        function ExibirErro(mensagem) {
            Ext.Msg.show({
                title: "Mensagem de Erro",
                msg: mensagem,
                buttons: Ext.Msg.OK,
                icon: Ext.MessageBox.ERROR,
                minWidth: 200
            });
        }
         
        function GerarExcel() {
            var partidaPlanejadaInicial = null;
            var partidaPlanejadaFinal = null;
            var chegadaPlanejadaInicial = null;
            var chegadaPlanejadaFinal = null;
            var prefixosIncluir = null;
            var prefixosExcluir = null;
            
            if (Ext.getCmp("dataPartidaPlanejadaInicial").getValue() != "" && Ext.getCmp("dataPartidaPlanejadaFinal").getValue() != "") {
                partidaPlanejadaInicial = new Array(Ext.getCmp("dataPartidaPlanejadaInicial").getValue().format('m/d/Y') + " " + Ext.getCmp("dataPartidaPlanejadaInicial").getValue().format('H:i:s'));
                partidaPlanejadaFinal = new Array(Ext.getCmp("dataPartidaPlanejadaFinal").getValue().format('m/d/Y') + " " + Ext.getCmp("dataPartidaPlanejadaFinal").getValue().format('H:i:s'));
            }
            
            if (Ext.getCmp("dataChegadaPlanejadaInicial").getValue() != "" && Ext.getCmp("dataChegadaPlanejadaFinal").getValue() != "") {
                chegadaPlanejadaInicial = new Array(Ext.getCmp("dataChegadaPlanejadaInicial").getValue().format('m/d/Y') + " " + Ext.getCmp("dataChegadaPlanejadaInicial").getValue().format('H:i:s'));
                chegadaPlanejadaFinal = new Array(Ext.getCmp("dataChegadaPlanejadaFinal").getValue().format('m/d/Y') + " " + Ext.getCmp("dataChegadaPlanejadaFinal").getValue().format('H:i:s'));
            }

            prefixosIncluir = Ext.getCmp('PrefixoIncluir').getValue();
            prefixosExcluir = Ext.getCmp('PrefixoExcluir').getValue();

            var url = "<%= Url.Action("ObterOnTime") %>";
            
            url += "?codigoUpOrigem=";
            url += "&codigoUpDestino=";
            url += "&estOrigem=";
            url += "&estDestino=";
            url += "&prefixo=";
            url += "&partidaPlanejadaInicial=" + partidaPlanejadaInicial;
            url += "&partidaPlanejadaFinal=" + partidaPlanejadaFinal;
            url += "&chegadaPlanejadaInicial=" + chegadaPlanejadaInicial;
            url += "&chegadaPlanejadaFinal=" + chegadaPlanejadaFinal;
            url += "&prefixosIncluir=" + prefixosIncluir;
            url += "&prefixosExcluir=" + prefixosExcluir;
            url += "&gerarExcel=" + true;
            
            window.open(url, "");
         }

         function Limpar() {
            LimparPesquisa();
			Ext.getCmp("filtros").getForm().reset();
         }
         
         function LimparPesquisa() {
            grid.getStore().removeAll();
         }
         
        Ext.onReady(function () {
            filtros.render(document.body);
            grid.render(document.body);
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div id="header-content">
        <h1>
            Consulta Partida Planejada x Realizada
        </h1>
        <small>Você está em Consultas > Trem > Consulta Partida Planejada x Realizada</small>
        <br />
    </div>
</asp:Content>
