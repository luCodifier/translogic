﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%
    var totDocumento = (int)ViewData["totalPerguntasDocumento"];
    var totCondicao = (int)ViewData["totalPerguntasCondObrigatoria"];
    var totLocomotiva = (int)ViewData["totalPerguntasLoco"];

    var perguntas = ViewData["perguntasMercadorias"] as List<Translogic.Modules.Core.Domain.Model.Dto.ConsultaTrem.PerguntaCheckListDto>;
    var criouRowsPan = false;
%>

<style type="text/css">
    @page
    {
        size: A4;
        margin-top: 10mm;
        margin-bottom: 10mm;
        margin-left: 8mm;
        margin-right: 8mm;
        border: 2px solid #000000;
    }
    .style4
    {
        width: 42px;
    }
    .style5
    {
        width: 50px;
    }
    .style6
    {
        width: 387px;
    }
    .style7
    {
        width: 530px;
    }
    .style9
    {
        width: 173px;
    }
    .style11
    {
        width: 171px;
    }
    .style12
    {
        width: 148px;
    }
    .style13
    {
        width: 197px;
    }
</style>

<div id="dvMercPerigosa">
    <p />
    <h1 style="text-align: center">
        Check-List Mercadorias perigosas</h1>
    <br />
    <table style="margin: 0 auto;" border="1" cellspacing="0" cellpadding="2" bordercolor=" 2px solid #000000;"
        width="800px">
        <tr>
            <td colspan="2">
                Maquinista:
            </td>
            <td colspan="2">
                Origem do carregamento:
            </td>
        </tr>
        <tr>
            <td class="style12">
                Trem:
            </td>
            <td class="style11">
                TB:
            </td>
            <td class="style13">
                Comprimento:
            </td>
            <td class="style9">
                Data:
            </td>
        </tr>
    </table>
    <br />
    <table style="border-collapse: collapse; border-right: 2px solid #000000;
        margin: 0 auto; font-family: Courier New, Courier, mono; font-size: 12; page-break-after: always;">
        <thead>
            <tr>
                <th style="border-top: 2px solid black; border-bottom: 2px solid #000000; border-left: 2px solid black;
                    text-align: center;" colspan="2">
                    Itens
                </th>
                <th style="border-top: 2px solid black; border-bottom: 2px solid #000000; text-align: center;"
                    class="style4">
                    AT
                </th>
                <th style="border-top: 2px solid black; border-bottom: 2px solid #000000; text-align: center;"
                    class="style5">
                    NAT
                </th>
                <th style="border-top: 2px solid black; border-bottom: 2px solid #000000; text-align: center;"
                    class="style6">
                    Observações
                </th>
            </tr>
        </thead>
        <tbody>
            <% if (totDocumento > 0)
               {
                   criouRowsPan = false;
                   foreach (var item in perguntas.Where(doc => doc.IdAgrupador.Equals(1)))
                   {%>
            <tr>
                <% if (!criouRowsPan)
                   { %>
                <td rowspan="<%:totDocumento%>" style="text-align: center; border-bottom: 2px solid #000000; border-right: 2px solid #000000;
                    border-left: 2px solid #000000;" >
                    <b>Documentos</b>
                </td>
                <% criouRowsPan = true;%>
                <% } %>
                <td style="text-align: justify; border-bottom: 2px solid #000000; border-right: 2px solid #000000;
                    border-left: 2px solid #000000;" class="style7">
                    <%:item.Pergunta %>
                </td>
                <td style="text-align: justify; border-bottom: 2px solid #000000; border-right: 2px solid #000000;
                    border-left: 2px solid #000000;" class="style4">
                    &nbsp;
                </td>
                <td style="text-align: justify; border-bottom: 2px solid #000000; border-right: 2px solid #000000;
                    border-left: 2px solid #000000;" class="style5">
                    &nbsp;
                </td>
                <td style="text-align: justify; border-bottom: 2px solid #000000; border-right: 2px solid #000000;
                    border-left: 2px solid #000000;" class="style6">
                    &nbsp;
                </td>
            </tr>
            <%}%>
            <%}%>
            <% if (totCondicao > 0)
               {
                   criouRowsPan = false;
                   foreach (var item in perguntas.Where(doc => doc.IdAgrupador.Equals(2)))
                   {%>
            <tr>
                <% if (!criouRowsPan)
                   { %>
                <td rowspan="<%:totCondicao %>" style="text-align: center; border-bottom: 2px solid #000000; border-right: 2px solid #000000;
                    border-left: 2px solid #000000;">
                    <b>Condições<br />
                        Obrigatórias<br />
                        para
                        <br />
                        operação</b>
                </td>
                <% criouRowsPan = true; %>
                <% } %>
                <td style="text-align: justify; border-bottom: 2px solid #000000; border-right: 2px solid #000000;
                    border-left: 2px solid #000000;" class="style7">
                    <%:item.Pergunta %>
                </td>
               <td style="text-align: justify; border-bottom: 2px solid #000000; border-right: 2px solid #000000;
                    border-left: 2px solid #000000;" class="style4">
                    &nbsp;
                </td>
                <td style="text-align: justify; border-bottom: 2px solid #000000; border-right: 2px solid #000000;
                    border-left: 2px solid #000000;" class="style5">
                    &nbsp;
                </td>
                <td style="text-align: justify; border-bottom: 2px solid #000000; border-right: 2px solid #000000;
                    border-left: 2px solid #000000;" class="style6">
                    &nbsp;
                </td>
            </tr>
            <% } %>
            <%}%>
            <% if (totLocomotiva > 0)
               {
                   criouRowsPan = false;
                   foreach (var item in perguntas.Where(doc => doc.IdAgrupador.Equals(3)))
                   {%>
            <tr>
                <% if (!criouRowsPan)
                   { %>
                <td rowspan="<%:totLocomotiva %>" style="text-align: center; border-bottom: 2px solid #000000; border-right: 2px solid #000000;
                    border-left: 2px solid #000000;">
                    <b>Locomotiva</b>
                </td>
                <% criouRowsPan = true; %>
                <%}%>
                <td style="text-align: justify; border-bottom: 2px solid #000000; border-right: 2px solid #000000;
                    border-left: 2px solid #000000;" class="style7">
                    <%:item.Pergunta %>
                </td>
                 <td style="text-align: justify; border-bottom: 2px solid #000000; border-right: 2px solid #000000;
                    border-left: 2px solid #000000;" class="style4">
                    &nbsp;
                </td>
                <td style="text-align: justify; border-bottom: 2px solid #000000; border-right: 2px solid #000000;
                    border-left: 2px solid #000000;" class="style5">
                    &nbsp;
                </td>
                <td style="text-align: justify; border-bottom: 2px solid #000000; border-right: 2px solid #000000;
                    border-left: 2px solid #000000;" class="style6">
                    &nbsp;
                </td>
            </tr>

            <%}%>
            <%}%>
            <tr>
                <td style="text-align: justify;">
                    &nbsp;
                </td>
                <td style="text-align: justify;">
                    &nbsp;
                </td>
                <td style="text-align: justify; border-bottom: 2px solid #000000; border-right: 2px solid #000000;
                    border-left: 2px solid #000000;" class="style4">
                    &nbsp;
                </td>
                <td style="text-align: justify; border-bottom: 2px solid #000000; border-right: 2px solid #000000;
                    border-left: 2px solid #000000;" class="style5">
                    &nbsp;
                </td>
                <td style="text-align: justify; border-bottom: 2px solid #000000; border-right: 2px solid #000000;
                    border-left: 2px solid #000000;" class="style6">
                    Trem Conforme
                </td>
            </tr>
        </tbody>
    </table>
</div>
<p />

