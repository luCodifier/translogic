﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%    
    var papeleta = ViewData["Papeleta"] as List<Translogic.Modules.Core.Domain.Model.Dto.ConsultaTrem.PerguntaCheckListDto>;
    var i = 1;
%>
<style type="text/css">
    @page
    {
        size: A4;
        margin-top: 10mm;
        margin-bottom: 10mm;
        margin-left: 8mm;
        margin-right: 8mm;
    }
    p
    {
        widows: 3;
    }
    
    p
    {
        orphans: 3;
    }
    
    .style1
    {
        width: 32px;
        text-align: center;
    }
</style>
<br /><br />
<div id="dvCheckListTrem" >
    <p />
    <table style="margin:auto; font-family: Courier New, Courier, mono; width: 100%;" >
        <%foreach (var item in papeleta)
          { %>
        <tr>
            <td class="style1">
                <%: i++ %>
            </td>
            <td style="height: 40px;">
                <%: item.Pergunta %>
            </td>
        </tr>
        <%} %>
    </table>
   
</div>
