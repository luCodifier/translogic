﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Interna.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content runat="server" ID="HeadContent" ContentPlaceHolderID="HeadContent">
    <script src="/GetFile.efh?file={Translogic.Core.Infrastructure.Web;}/Content/Javascript/Utilfunctions.js"> </script>
    <script type="text/javascript">
        var Onu;
        var idTrem
        var acessoPerguntaCheckList = '<%=ViewData["PerguntaCheckList"].ToString().ToLower()%>';
        var urlLegado = '<%=ViewData["UrlLegado"] %>';

        var gridPerguntas = null;

        var formModal = null;

        var IdPergunta = 0;

        var cmbAgrupador = false;
        //*****STORES GRIDS*****

        //***** STORE MODAL PERGUNTAS*****
        var gridPerguntasStore = new Ext.data.JsonStore({
            root: "Items",
            totalProperty: 'Total',
            id: 'gridPerguntasStore',
            name: 'gridPerguntasStore',
            proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("ObterConsultaPerguntasChecklist", "ConsultaTrem") %>', timeout: 600000 }),
            paramNames:
            {
                sort: "detalhesPaginacaoWeb.Sort",
                dir: "detalhesPaginacaoWeb.Dir",
                start: "detalhesPaginacaoWeb.Start",
                limit: "detalhesPaginacaoWeb.Limit"
            },
            fields: ['IdPergunta', 'IdAgrupador', 'OrdemPergunta', 'Pergunta']
        });

        var agrupadorStore = new Ext.data.ArrayStore({
            id: 'agrupadorStore',
            fields: [
            'Id',
            'Nome'
            ],
            data: [
                [1, 'Documentos'],
                [2, 'Condições obrigatórias para operação'],
                [3, 'Locomotiva']
            ]
        });

        //***** FIM MODAL***** 
        var situacaoStore = new Ext.data.ArrayStore({
            id: 'situacaoStore',
            fields: ['Id', 'Descricao'],
            data: [['T', 'Todos'], ['C', 'Circulando'], ['P', 'Em Formação'], ['R', 'Parado'], ['L', 'Liberado']]
        });

        var gridStore = new Ext.data.JsonStore({
            root: "Items",
            totalProperty: 'Total',
            id: 'gridStore',
            name: 'gridStore',
            proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("ObterConsultaTrem", "ConsultaTrem") %>', timeout: 600000 }),
            paramNames: {
                sort: "detalhesPaginacaoWeb.Sort",
                dir: "detalhesPaginacaoWeb.Dir",
                start: "detalhesPaginacaoWeb.Start",
                limit: "detalhesPaginacaoWeb.Limit"
            },
            fields: ['IdTrem', 'TotalOnu', 'NumOs', 'Trem', 'Origem', 'Destino', 'HorasAPT1', 'PartidaPrevisao', 'PartidaReal', 'Situacao', 'LocalAtual', 'DataHora', 'PosicaoVirtual', 'Desde', 'TLocos', 'LT', 'LR', 'TVagoes', 'VC', 'VZ', 'Comprimento', 'Peso']
        });

        var filtrotrem = {
            xtype: 'textfield',
            style: 'text-transform: uppercase',
            fieldLabel: 'Trem Prefixo',
            id: 'filtro-trem',
            name: 'trem',
            maskRe: /[a-zA-Z0-9]/,
            anchor: '95%',
            autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '5' },
            maxLength: 5,
            width: 40
        };

        var filtroOs = {
            xtype: 'numberfield',
            fieldLabel: 'OS',
            id: 'filtro-os',
            name: 'OS',
            anchor: '95%',
            maskRe: /[0-9]/,
            maxLength: 20,
            autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '20' },
            width: 40,
            allowDecimals: false
        };

        var filtroSituacao = {
            xtype: 'combo',
            editable: true,
            typeAhead: true,
            forceSelection: true,
            disableKeyFilter: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            valueField: 'Id',
            displayField: 'Descricao',
            fieldLabel: 'Situação',
            id: 'filtro-situacao',
            name: 'filtro-situacao',
            width: 120,
            store: situacaoStore,
            emptyText: 'Selecione...'
        };

        var form1 = {
            columnWidth: .30,
            layout: 'form',
            border: false,
            items: [filtrotrem, filtroOs, filtroSituacao]
        };

        var filtroOrigem = {
            xtype: 'textfield',
            style: 'text-transform: uppercase',
            fieldLabel: 'Origem',
            id: 'filtro-origem',
            name: 'trem',
            anchor: '-18',
            autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '3' },
            maskRe: /[A-Za-z]/,
            maxLength: 3,
            width: 25
        };

        var filtroDestino = {
            xtype: 'textfield',
            style: 'text-transform: uppercase',
            fieldLabel: 'Destino',
            id: 'filtro-destino',
            name: 'trem',
            anchor: '-18',
            autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '3' },
            maskRe: /[A-Za-z]/,
            maxLength: 3,
            width: 25
        };

        var filtroTempoApt = {
            xtype: 'numberfield',
            fieldLabel: 'Tempo APT',
            id: 'filtro-tempoApt',
            name: 'tempoApt',
            maskRe: /[0-9]/,
            maxLength: 10,
            autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '10' },
            width: 70,
            allowDecimals: true,
            decimalSeparator: ','
        };

        var form2 = {
            columnWidth: .30,
            layout: 'form',
            border: false,
            items: [filtroOrigem, filtroDestino, filtroTempoApt]
        };

        var filtroPrefixoExcluir = {
            xtype: 'textfield',
            style: 'text-transform: uppercase',
            width: 230,
            name: 'PrefixoExcluir',
            allowBlank: true,
            id: 'filtro-prefixoExcluir',
            fieldLabel: 'Prefixos a Excluir(separe por vírgula)',
            enableKeyEvents: true,
            value: 'N,E,W,S,V,A',
            decimalSeparator: ',',
            maskRe: /[A-Za-z,]/
        };

        var form3 = {
            columnWidth: .30,
            layout: 'form',
            labelAlign: 'top',
            border: false,
            items: [filtroPrefixoExcluir]
        };

        var coluna1 = {
            columnWidth: .30,
            layout: 'column',
            border: false,
            items: [form1, form2, form3]
        };

        /**** modal para opção de impressão da ficha****/

        var lblMercadoriaPerigosa = {
            xtype: 'label',
            id: 'lblMercadoriaPerigosa',
            text: 'CL Mercadoria Perigosa'
        };

        var frmlblMercadoriaPerigosa = {
            layout: 'form',
            border: false,
            autoWidth: true,
            items: [lblMercadoriaPerigosa]
        };

        var cbMercadoriaPerigosa = {
            xtype: 'checkbox',
            fieldLabel: '',
            hideLabel: true,
            id: 'cbMercadoriaPerigosa',
            name: 'cbMercadoriaPerigosa',
            value: true,
            listeners: {
                check: function (checkbox, checked) {
                    if (!checked) {
                        if (Onu > 0) {
                            Ext.Msg.show({
                                title: 'Aviso',
                                msg: 'Trem transportando mercadoria perigosa!',
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.WARNING
                            });
                        }
                    }
                }
            }
        };

        var frmcbMercadoriaPerigosa = {
            layout: 'form',
            border: false,
            autoWidth: true,
            items: [cbMercadoriaPerigosa]
        };

        var lblPapeleta = {
            xtype: 'label',
            id: 'lblPapeleta',
            text: 'Papeleta'
        };

        var frmlblPapeleta = {
            layout: 'form',
            border: false,
            autoWidth: true,
            items: [lblPapeleta]
        };

        var cbPapeleta = {
            xtype: 'checkbox',
            fieldLabel: '',
            hideLabel: true,
            id: 'cbPapeleta',
            name: 'cbPapeleta',
            value: true
        };

        var frmcbPapeleta = {
            layout: 'form',
            border: false,
            autoWidth: true,
            items: [cbPapeleta]
        };

        var modalImpressao = new Ext.Window({
            title: 'Imprimir Consulta Trem',
            modal: true,
            width: 200,
            //autowidth: true,
            autoheight: true,
            constrainHeader: true,
            closeAction: 'hide',
            resizable: false,
            listeners: {
                hide: function () {
                    limpaModal();
                },
                render: function () {
                    $('.x-tool-close').hide();
                }
            },
            items: [
                {
                    border: false,
                    bodyStyle: 'padding: 5px',
                    //labelWidth: 110,
                    buttonAlign: 'center',
                    buttons: [
                        {
                            text: 'Imprimir',
                            tooltip: 'Imprimir',
                            //iconCls: 'icon-save',
                            handler: function () {
                                imprimir();
                                //Limpa campos modal
                                limpaModal();
                            }
                        },
                        {
                            text: 'Cancelar',
                            tooltip: 'Cancelar',
                            //iconCls: 'icon-save',
                            handler: function () {
                                //Deixa MODAL INVISIVEL
                                modalImpressao.hide();
                                //Limpa campos modal
                                limpaModal();
                            }
                        }
                    ],
                    items:
                        [
                            {
                                layout: 'column',
                                border: false,
                                autoWidth: true,
                                items: [frmcbMercadoriaPerigosa, frmlblMercadoriaPerigosa]
                            },
                            {
                                layout: 'column',
                                border: false,
                                autoWidth: true,
                                items: [frmcbPapeleta, frmlblPapeleta]
                            }
                        ]
                }
            ]

        });

        /***FIM***/

        var actionFind = new Ext.ux.grid.RowActions({
            dataIndex: 'iconedit',
            header: '',
            width: 26,
            align: 'center',
            actions: [
                    { iconCls: 'icon-find', tooltip: 'Movimentações do trem' }
                 ],
            callbacks: {
                'icon-find': function (grid, record, action, row, col) {

                    var idTrem = record.data.IdTrem;
                    var situacao = record.data.Situacao;
                    var situacaoSigla = '';
                    switch (situacao) {
                        case "Circulando":
                            situacaoSigla = 'C'
                            break;
                        case "Em Formacao":
                            situacaoSigla = 'C'
                            break;
                        case "Parado":
                            situacaoSigla = 'D'
                            break;
                        case "Liberado":
                            situacaoSigla = 'E'
                            break;
                    }
                    var url = urlLegado + 'translogic//translogicII/cDetTrem.asp?pid=' + idTrem + '&Prefixo=&OS=&DataPartida=&Origem=&Destino=&LocalAtual=&Situacao=' + situacaoSigla;
                    window.open(url);
                }
            }
        });

        var actionPrinter = new Ext.ux.grid.RowActions({
            dataIndex: 'iconeprinter',
            header: '',
            width: 26,
            align: 'center',
            actions: [

                    { iconCls: 'icon-printer', tooltip: 'Ficha do trem' }
                 ],
            callbacks: {
                'icon-printer': function (grid, record, action, row, col) {
                    Onu = record.data.TotalOnu;
                    idTrem = record.data.IdTrem;
                    if (record.data.TotalOnu > 0) {
                        Ext.getCmp("cbMercadoriaPerigosa").setValue(true);
                        modalImpressao.show();
                    }
                    else {
                        imprimir();
                    }

                    //Torna modal impressão visivel
                    //modalImpressao.show();
                }
            }
        });

        var gridRowNumber = new Ext.grid.RowNumberer({
            width: 30
        });

        grid = new Ext.grid.GridPanel({
            id: 'grid',
            name: 'grid',
            autoLoadGrid: false,
            height: 450,
            width: 1450,
            stripeRows: true,
            region: 'center',
            store: gridStore,
            enableHdMenu: false,
            viewConfig: {
                forceFit: true
            },
            loadMask: { msg: App.Resources.Web.Carregando },
            colModel: new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true
                },
                columns: [
                gridRowNumber,
                actionFind, actionPrinter,
                { header: 'IdTrem', width: 50, dataIndex: "IdTrem", hidden: true },
                { header: 'TotalOnu', width: 50, dataIndex: "TotalOnu", hidden: true },
                { header: 'Os', width: 90, dataIndex: "NumOs", sortable: true },
                { header: 'Trem', width: 65, dataIndex: "Trem", sortable: true },
                { header: 'Org', width: 60, dataIndex: "Origem", sortable: true },
                { header: 'Dst', width: 60, dataIndex: "Destino", sortable: true },
                { header: 'Horas APT', width: 120, dataIndex: "HorasAPT1", sortable: true },
                { header: 'Partida Prev.', width: 180, dataIndex: "PartidaPrevisao", sortable: true },
                { header: 'Partida Real.', width: 180, dataIndex: "PartidaReal", sortable: true },
                { header: 'Sit. Trem', width: 130, dataIndex: "Situacao", sortable: true },
                { header: 'Local Atual', width: 120, dataIndex: "LocalAtual", sortable: true },
                { header: 'Data/Hora', width: 180, dataIndex: "DataHora", sortable: true },
                { header: 'Pos. Virtual', width: 115, dataIndex: "PosicaoVirtual", sortable: true },
                { header: 'Desde', width: 180, dataIndex: "Desde", sortable: true },
                { header: 'T.Locos', width: 95, dataIndex: "TLocos", sortable: true },
                { header: 'LT', width: 40, dataIndex: "LT", sortable: true },
                { header: 'LR', width: 40, dataIndex: "LR", sortable: true },
                { header: 'T.Vagões', width: 100, dataIndex: "TVagoes", sortable: true },
                { header: 'VC', width: 40, dataIndex: "VC", sortable: true },
                { header: 'VZ', width: 40, dataIndex: "VZ", sortable: true },
                { header: 'Comprimento', width: 130, dataIndex: "Comprimento", sortable: true },
                { header: 'Peso', width: 150, dataIndex: "Peso", sortable: true }
            ]
            })
        , plugins: [actionFind, actionPrinter]
        });

        var btnPesquisar = {
            xtype: 'button',
            name: 'btnPesquisar',
            id: 'btnPesquisar',
            text: 'Pesquisar',
            iconCls: 'icon-find',
            handler: pesquisar
        };

        var btnLimpar = {
            xtype: 'button',
            name: 'btnLimpar',
            id: 'btnLimpar',
            text: 'Limpar',
            handler: limpar
        };

        var btnExportarExcel = {
            xtype: 'button',
            name: 'btnExportarExcel',
            id: 'btnExportarExcel',
            text: 'Gerar Excel',
            iconCls: 'icon-page-excel',
            handler: geraExcel
        };

        var btnAbreModCrudCheckList = {
            xtype: 'button',
            name: 'btnAbreModCrudCheckList',
            id: 'btnAbreModCrudCheckList',
            text: 'Perguntas CheckList',
            iconCls: 'icon-wrench',
            hidden: (acessoPerguntaCheckList == 'true' ? false : true),
            handler: function () {
                formModal.show();
            }
        };

        filtros = new Ext.form.FormPanel({
            id: 'form-filtros',
            title: "Filtros",
            height: 160,
            width: 810,
            region: 'center',
            bodyStyle: 'padding: 10px',
            items: [coluna1],
            buttonAlign: 'center',
            buttons: [btnPesquisar, btnExportarExcel, btnLimpar, btnAbreModCrudCheckList]
        });

        Ext.onReady(function () {
            filtros.render(document.body);
            grid.render(document.body);
        });


        function pesquisar() {

            grid.getStore().proxy.on('beforeload', function (p, params) {

                var os = Ext.getCmp("filtro-os").getValue();
                if (os != "") {
                    params["os"] = os;
                }
                else {
                    params["prefixo"] = Ext.util.Format.uppercase(Ext.getCmp("filtro-trem").getValue().trim());
                    if (Ext.getCmp("filtro-situacao").getRawValue() != "Todos") {
                        params["situacao"] = Ext.getCmp("filtro-situacao").getValue();
                    }
                    params["origem"] = Ext.util.Format.uppercase(Ext.getCmp("filtro-origem").getValue().trim());
                    params["destino"] = Ext.util.Format.uppercase(Ext.getCmp("filtro-destino").getValue().trim());
                    params["tempoAPT"] = Ext.getCmp("filtro-tempoApt").getValue();
                    params["prefixosExcluir"] = Ext.util.Format.uppercase(Ext.getCmp("filtro-prefixoExcluir").getValue().trim());
                }
            });

            gridStore.load({ params: { start: 0, limit: 1000} });
        }

        function limpar() {
            grid.getStore().removeAll();
            Ext.getCmp("form-filtros").getForm().reset();
        }

        function geraExcel() {

            var url = '<%= Url.Action("ExportarConsultaTrem", "ConsultaTrem") %>';
            var os = Ext.getCmp("filtro-os").getValue();
            if (os != "") {
                url += "?os=" + os;
            }
            else {
                url += "?prefixo=" + Ext.util.Format.uppercase(Ext.getCmp("filtro-trem").getValue().trim());
                if (Ext.getCmp("filtro-situacao").getRawValue() != "Todos") {
                    url += "&situacao=" + Ext.getCmp("filtro-situacao").getValue();
                }
                url += "&origem=" + Ext.util.Format.uppercase(Ext.getCmp("filtro-origem").getValue().trim());
                url += "&destino=" + Ext.util.Format.uppercase(Ext.getCmp("filtro-destino").getValue().trim());
                url += "&tempoapt=" + Ext.getCmp("filtro-tempoApt").getValue();
                url += "&prefixosExcluir=" + Ext.util.Format.uppercase(Ext.getCmp("filtro-prefixoExcluir").getValue().trim());
            }

            window.open(url, "");
        }

        function imprimir() {
            var mercPerigosa = Ext.getCmp("cbMercadoriaPerigosa").getValue();
            var papeleta = Ext.getCmp("cbPapeleta").getValue();


            var url = '<%= Url.Action("GeraFichaComChecklist", "ConsultaTrem") %>';
            url += "?idTrem=" + idTrem;
            url += "&mercPerigosa=" + mercPerigosa;
            url += "&papeleta=" + papeleta;
            window.open(url, "");
        }

        function limpaModal() {
            Onu = 0;
            Ext.getCmp("cbMercadoriaPerigosa").setValue(false);
            Ext.getCmp("cbPapeleta").setValue(false);

            //Deixa MODAL INVISIVEL
            modalImpressao.hide();
        }


        //JAVA SCRIPT MODAL
        //FUNCTION CRIA OBJETO DTO TELA
        function CriarObjetoPerguntaCheckListDto() {
            var perguntaCheckListDto = new Object();
            perguntaCheckListDto.IdPergunta = IdPergunta;
            perguntaCheckListDto.OrdemPergunta = Ext.getCmp("txtNumPergunta").getValue();
            perguntaCheckListDto.IdAgrupador = Ext.getCmp("ddlAgrupador").getValue();
            perguntaCheckListDto.Pergunta = Ext.getCmp("txtPergunta").getValue();

            if (Ext.getCmp("rdbTipo").items.items[0].checked == true) {
                perguntaCheckListDto.TipoPerguntaCheckList = 'M';
            } else {
                perguntaCheckListDto.TipoPerguntaCheckList = 'P';
            }
            return perguntaCheckListDto;
        }

        //FUNCTION SALVAR PerguntaCheckList
        function SalvaPerguntaCheckList(perguntaCheckListDto) {
            var IdAgrupador = perguntaCheckListDto.IdAgrupador;
            var tipoPergunta = "";
            if (Ext.getCmp("rdbTipo").items.items[0].checked == true) {
                tipoPergunta = 'M';
            } else {
                tipoPergunta = 'P';
            }
            // Progress BAR
            loader("Processando Pergunta CheckList...");
            $.ajax
                ({
                    url: '<%= Url.Action("SalvarPerguntaCheckList","ConsultaTrem") %>',
                    type: "POST",
                    dataType: 'json',
                    data: $.toJSON({
                        perguntaCheckListDto: perguntaCheckListDto
                    }),
                    contentType: "application/json; charset=utf-8",
                    async: false,
                    success: function (result) {

                        if (result.Result) {

                            LimpaCampos(); ;

                            setarValorCombo(Ext.getCmp('ddlAgrupador'), IdAgrupador);

                            //Reload GRID
                            //gridPerguntasStore.load({ params: { start: 0, limit: 1000, idAgrupador: IdAgrupador} });
                            gridPerguntasStore.load({ params: { start: 0, limit: 1000, idAgrupador: IdAgrupador, tipoPergunta: tipoPergunta} });

                            closeLoader();

                            Ext.Msg.show({
                                title: "",
                                msg: result.Message,
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.INFO,
                                minWidth: 200
                            });
                        } else {

                            closeLoader();

                            Ext.Msg.show({
                                title: "Erro",
                                msg: result.Message,
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.ERROR,
                                minWidth: 200
                            });
                        }
                    },
                    error: function (response) {

                        closeLoader();

                        Ext.Msg.show({
                            title: 'Aviso',
                            msg: retorno.Message,
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.WARNING
                        });
                    }
                });
            //}
        }

        //FUNCTION DELETA PerguntaCheckList
        function DeletaPerguntaCheckList(idPerguntaCheckList, IdAgrupador) {
            var tipoPergunta = "";

            if (Ext.getCmp("rdbTipo").items.items[0].checked == true) {
                tipoPergunta = 'M';
            } else {
                tipoPergunta = 'P';
            }

            // Progress BAR
            loader("Processando Pergunta CheckList...");
            $.ajax
        ({
            url: '<%= Url.Action("DeletaPerguntaCheckList","ConsultaTrem") %>',
            type: "POST",
            dataType: 'json',
            data: $.toJSON({
                idPerguntaCheckList: idPerguntaCheckList
            }),
            contentType: "application/json; charset=utf-8",
            async: false,
            success: function (result) {

                if (result.Result) {
                    LimpaCampos();
                    setarValorCombo(Ext.getCmp('ddlAgrupador'), IdAgrupador);
                    //Reload GRID                    
                    gridPerguntasStore.load({ params: { start: 0, limit: 1000, idAgrupador: IdAgrupador, tipoPergunta: tipoPergunta} });

                    closeLoader();

                    Ext.Msg.show({
                        title: "",
                        msg: result.Message,
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO,
                        minWidth: 200
                    });
                } else {

                    closeLoader();

                    Ext.Msg.show({
                        title: "Erro",
                        msg: result.Message,
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.ERROR,
                        minWidth: 200
                    });
                }
            },
            error: function (response) {

                closeLoader();

                Ext.Msg.show({
                    title: 'Aviso',
                    msg: retorno.Message,
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.WARNING
                });
            }
        });
            //}
        }

        function LimpaCampos() {
            IdPergunta = 0;
            Ext.getCmp('ddlAgrupador').clearValue();
            Ext.getCmp('txtNumPergunta').setValue("");
            Ext.getCmp('txtPergunta').setValue("");
            gridPerguntasStore.removeAll();
        }

        function validaCamposObrigatorios() {
            var objTela = CriarObjetoPerguntaCheckListDto();
            var listPerguntas = gridPerguntas.getStore();

            //Verifica se Ordem Perghunta está null
            if (objTela.OrdemPergunta == "") {
                Ext.Msg.show(
                {
                    title: 'Aviso',
                    msg: 'Campo Nº Pergunta é obrigatório.',
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.WARNING
                });

                return;
            }

            if (objTela.TipoPerguntaCheckList == 'M' && objTela.IdAgrupador == "") {
                //Verifica se Agrupador está null
                if (objTela.IdAgrupador == "") {
                    Ext.Msg.show(
                    {
                        title: 'Aviso',
                        msg: 'Campo Agrupador é obrigatório.',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.WARNING
                    });

                    return;
                }
            }
            //Verifica se Pergunta está null
            if (objTela.Pergunta == "") {
                Ext.Msg.show(
                {
                    title: 'Aviso',
                    msg: 'Campo Pergunta é obrigatório.',
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.WARNING
                });

                return;
            }

            if (IdPergunta == 0) {
                var existe = false;

                //Verifica ordem pergunta já existe
                for (var i = 0; i < listPerguntas.data.length; i++) {
                    var OrdemPergunta = listPerguntas.data.items[i].data.OrdemPergunta;

                    if (objTela.OrdemPergunta == OrdemPergunta) {
                        existe = true;
                    }
                }

                if (existe) {
                    Ext.Msg.show(
                    {
                        title: 'Aviso',
                        msg: 'Ordem da pergunta já cadastrada.',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.WARNING
                    });

                    return;
                }
            }
            return true;
        }

        //Inicio Campos MODAL
        var fieldsetRBT = {
            xtype: 'radiogroup',
            flex: 8,
            vertical: true,
            columns: 2,
            labelWidth: 1,
            width: 250,
            id: 'rdbTipo',
            //fieldLabel: 'Tipo',
            //disabled: idOsChecklist != 0 && AlteraStatusCheckList == false ? true : false,
            items: [{
                id: 'rdbMercadoriaPerigosa',
                checked: true,
                boxLabel: 'Mercadoria Perigosa',
                name: 'rb-auto',
                inputValue: 1
            }, {
                id: 'rdbPapeleta',
                checked: false,
                boxLabel: 'Papeleta',
                name: 'rb-auto',
                inputValue: 2
            }],
            listeners:
        {
            change: function (obj, checked) {
                if (obj.items.items[0].checked == true) {
                    Ext.getCmp("idFrm1").setVisible(true);

                    LimpaCampos();
                }
                if (obj.items.items[1].checked == true) {
                    Ext.getCmp("idFrm1").setVisible(false);

                    LimpaCampos();

                    gridPerguntasStore.load({ params: { start: 0, limit: 1000, tipoPergunta: 'P'} });
                }
            }
        }
        };

        var frmfieldsetRBT = {
            layout: 'form',
            border: false,
            labelAlign: 'top',
            items: [fieldsetRBT]
        };

        var col1 = {
            layout: 'column',
            border: false,
            padding: 2,
            style: 'margin-left:5px;',
            items: [frmfieldsetRBT]
        };

        //****CAMPOS*****
        var txtNumPergunta =
    {
        xtype: 'textfield',
        name: 'txtNumPergunta',
        id: 'txtNumPergunta',
        fieldLabel: 'Nº Pergunta',
        autoCreate: {
            tag: 'input',
            type: 'text',
            autocomplete: 'off',
            maxlength: '3'
        },
        maskRe: /[0-9]/,
        style: 'text-transform:uppercase;',
        width: 50
    };

        var formtxtNumPergunta =
    {
        width: 100,
        layout: 'form',
        border: false,
        labelAlign: 'top',
        items: [txtNumPergunta]
    };

        var ddlAgrupador = new Ext.form.ComboBox({
            editable: false,
            typeAhead: false,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            store: agrupadorStore,
            valueField: 'Id',
            displayField: 'Nome',
            fieldLabel: 'Agrupador',
            id: 'ddlAgrupador',
            width: 200,
            listeners:
        {

            select: function (combo, record, index) {
                //gridPerguntasStore.removeAll();
                //LimpaCampos();
                cmbAgrupador = true;
                gridPerguntasStore.load({ params: { start: 0, limit: 1000, idAgrupador: record.data.Id, tipoPergunta: 'M'} });
            },
            blur: function (combo) {
                if (combo.value != "" && combo.selectedIndex >= 0) {
                    if (cmbAgrupador = false) {
                        gridPerguntasStore.removeAll();
                        gridPerguntasStore.load({ params: { start: 0, limit: 1000, idAgrupador: combo.value, tipoPergunta: 'M'} });
                    }
                }
            }
        }
        });

        var frm1 = {
            id: 'idFrm1',
            layout: 'form',
            border: false,
            labelAlign: 'top',
            items: [ddlAgrupador]
        };

        var frmAgrupador = {
            width: 900,
            layout: 'column',
            border: false,
            labelAlign: 'top',
            items: [formtxtNumPergunta, frm1]
        };

        var txtPergunta = new Ext.form.TextArea({
            id: 'txtPergunta',
            name: 'txtPergunta',
            fieldLabel: 'Pergunta',
            enableKeyEvents: true,
            width: 400,
            height: 50,
            autoCreate: {
                maxlength: '500',
                tag: 'textarea'
                //rows: '6',
                //cols: '65'
            },
            listeners: {
                keyUp: function (field, e) {
                    if (field.getRawValue().length >= 1000) {
                        $("#frmtxtPergunta").val($("#frmtxtPergunta").val().substring(0, 1000));
                    }
                }
            }
            //labelStyle: 'font-weight: bold;'
        });

        var frmtxtPergunta = {
            layout: 'form',
            border: false,
            //width: 405,
            labelAlign: 'top',
            items: [txtPergunta]
        };

        //@@ INICIO @@ - GRID PERGUNTAS
        //BOTÕES GRID
        //BOTÃO EXCLUIR
        var botaoGridExcluir = new Ext.ux.grid.RowActions({
            dataIndex: '',
            header: '',
            align: 'center',
            actions: [{
                iconCls: 'icon-delete',
                tooltip: 'Excluir Registro'
            }],
            callbacks:
                    {
                        'icon-delete': function (grid, record, action, row, col) {
                            //Confirmação se usuario realmente deseja remover
                            Ext.Msg.show
                            ({
                                title: 'Confirmação',
                                msg: 'Deseja excluir Pergunta?',
                                buttons: Ext.Msg.YESNO,
                                icon: Ext.MessageBox.QUESTION,
                                animEl: 'elId',
                                fn: function (btn, text) {

                                    if (btn == 'yes') {
                                        DeletaPerguntaCheckList(record.data.IdPergunta, record.data.IdAgrupador);
                                    }
                                }
                            });
                        }
                    }

        });
        // BOTÃO ALTERAR
        var botaoGridEditar = new Ext.ux.grid.RowActions({
            dataIndex: '',
            header: '',
            align: 'center',
            actions: [{
                iconCls: 'icon-edit',
                tooltip: 'Editar Registro'
            }],
            callbacks: {
                'icon-edit': function (grid, record, action, row, col) {
                    IdPergunta = record.data.IdPergunta;
                    setarValorCombo(Ext.getCmp('ddlAgrupador'), record.data.IdAgrupador);
                    Ext.getCmp('txtNumPergunta').setValue(record.data.OrdemPergunta);
                    Ext.getCmp('txtPergunta').setValue(record.data.Pergunta);
                }
            }
        });

        var cm = new Ext.grid.ColumnModel({
            defaults: {
                sortable: true
            },
            columns: [
                    new Ext.grid.RowNumberer(),
                    botaoGridExcluir,
                    botaoGridEditar,
                    { header: 'IdPergunta', dataIndex: 'IdPergunta', hidden: true },
                    { header: 'IdAgrupador', dataIndex: 'IdAgrupador', hidden: true },
                    { header: 'Ordem', dataIndex: 'OrdemPergunta', sortable: false, width: 20 },
                    { header: 'Pergunta', dataIndex: 'Pergunta', sortable: false, width: 200 }
                ]
        });

        var gridPerguntas = new Ext.grid.EditorGridPanel({
            id: 'gridPerguntas',
            name: 'gridPerguntas',
            viewConfig: { forceFit: true },
            stripeRows: true,
            cm: cm,
            region: 'center',
            height: 190,
            store: gridPerguntasStore,
            plugins: [botaoGridExcluir,
                    botaoGridEditar
                ],
            loadMask: { msg: App.Resources.Web.Carregando }
        });

        var linha1 = {
            layout: 'column',
            border: false,
            padding: 2,
            style: 'margin-left:5px;',
            items: [frmAgrupador]
        };

        var linha2 = {
            layout: 'column',
            border: false,
            padding: 2,
            style: 'margin-left:5px;',
            items: [frmtxtPergunta]
        };

        var linha3 = {
            layout: 'column',
            border: false,
            items: [gridPerguntas]
        };

        var formModal = new Ext.Window({
            id: 'formModal',
            title: 'Perguntas CheckList',
            modal: true,
            width: 800,
            closeAction: 'hide',
            height: 465,
            resizable: false,
            autoScroll: false,
            buttonAlign: "center",
            items: [
                    {
                        xtype: 'panel',
                        width: 800,
                        autoheight: true,
                        //renderTo: Ext.getBody(),
                        // TabPanel child items are of type 'Panel' by default
                        items: [
                                   col1, linha1, linha2, linha3
                               ]
                    }
        ],
            listeners: {
                resize: function (win, width, height, eOpts) {
                    win.center();
                },
                hide: function () {
                    //LimpaCampos();
                },
                render: function () {
                    $('.x-tool-close').hide();
                }
            },
            buttons: [
                        {
                            text: 'Adicionar',
                            type: 'submit',
                            id: 'btnAdicionar',
                            //disabled: true,
                            iconCls: 'icon-new',
                            handler: function (b, e) {
                                if (validaCamposObrigatorios()) {

                                    SalvaPerguntaCheckList(CriarObjetoPerguntaCheckListDto());
                                }
                            }
                        },
                        {
                            text: 'Fechar',
                            type: 'submit',
                            id: 'btnFechar',
                            //disabled: true,
                            iconCls: 'icon-cancel',
                            handler: function (b, e) {
                                LimpaCampos();
                                formModal.hide();
                            }
                        }
                ]
        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div id="header-content">
        <h1>
            Consulta Trem</h1>
        <small>Você está em Consultas > Trem > Consulta Trem > Ficha do Trem / Acompanhamento
            de Trens / Circulação Trem</small>
        <br />
        <small></small>
        <br />
    </div>
</asp:Content>
