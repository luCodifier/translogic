﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%@ Import Namespace="Translogic.Modules.Core.Domain.Model.Tfa" %>
<div id="divPrincipal">
</div>
<script type="text/javascript">

    ////var idGrupoEmpresa = '<%=ViewData["idGrupoEmpresa"] %>';
    var idEmpresa = 0;
    var cnpjAnterior = '';

    var fieldCnpj = {
        xtype: 'textfield',
        name: 'txtCnpj',
        id: 'txtCnpj',
        autoCreate: { tag: 'input', type: 'text', maxlength: '18', autocomplete: 'off' },
        cls: 'hidden-clear',
        clearIcon: false,
        typeAhead: true,
        fieldLabel: 'CNPJ',
        width: 150,
        plugins: [new Ext.ux.InputTextMask('99.999.999/9999-99', false)],
        enableKeyEvents: true,
        listeners: {
            Keyup: function (f, e) {
                atualizarCampos();
            },
            change: function (field, newValue, oldValue) {
                atualizarCampos();
            }

        }
    };

    var fieldRazaoSocial = {
        xtype: 'textfield',
        disabled: true,
        readOnly: true,
        name: 'txtRazaoSocial',
        id: 'txtRazaoSocial',
        fieldLabel: 'Razão Social',
        width: 280
    };

    var colCnpj = {
        width: 300,
        layout: 'form',
        border: false,
        items: [fieldCnpj, fieldRazaoSocial]
    };


    var arrlinha1 = {
        layout: 'column',
        border: false,
        items: [colCnpj]
    };


    var arrCampos = new Array();
    arrCampos.push(arrlinha1);

    var fields = new Ext.form.FormPanel({
        id: 'fields',
        title: "",
        region: 'center',
        bodyStyle: 'padding: 15px',
        labelAlign: 'top',
        items: [arrCampos],
        buttonAlign: "center",
        buttons:
	            [
                 {
                     text: 'Salvar',
                     name: 'btnSalvar',
                     id: 'btnSalvar',
                     type: 'submit',
                     iconCls: 'icon-save',
                     handler: function (b, e) {
                         Salvar(); 
                     }
                 },
                 {
                     text: 'Cancelar',
                     handler: function (b, e) {
                         windowNovoEmpresaGrupoTfa.close();                         
                     },
                     scope: this
                 }
                ]
    });

    function Salvar() {
        var cnpj = Ext.getCmp("txtCnpj").getValue().replace(/[^\d]+/g, '');

        if (cnpj == '') {
            Ext.Msg.show({
                title: "Erro",
                msg: "Informe o CNPJ!",
                buttons: Ext.Msg.OK,
                icon: Ext.MessageBox.INFO,
                minWidth: 200
            });
        }
        else if (!validarCNPJ(cnpj)) {
            Ext.Msg.show({
                title: "Erro",
                msg: "CNPJ inválido!",
                buttons: Ext.Msg.OK,
                icon: Ext.MessageBox.INFO,
                minWidth: 200
            });
        }
        else {
            Ext.Ajax.request({
                url: '<%= Url.Action("IncluirEmpresaGrupoTfa", "EmpresaGrupoTfa") %>',
                success: function (response) {
                    var result = Ext.decode(response.responseText);
                    if (result.Success) {

                        Ext.Msg.alert('Informação', "Cadastro efetuado com sucesso!");
                        windowNovoEmpresaGrupoTfa.close();
                        grid.getStore().reload();
                    }
                    else {

                        Ext.Msg.alert('Aviso', result.Message);
                        Ext.getCmp("txtCnpj").focus();
                    }
                },
                failure: function (conn, data) {
                    Ext.Msg.alert('Erro no cadastro da Empresa!');
                },
                method: "POST",
                params: { cnpj: cnpj, idGrupoEmpresa: idGrupoEmpresa }
            });
        }
    }

    function buscarEmpresa() {
        var cnpj = Ext.getCmp('txtCnpj').getValue().replace(/[^\d]+/g, '');

        if (validarCNPJ(cnpj)) {
            Ext.Ajax.request({
                url: '<%= Url.Action("ObterEmpresa", "EmpresaGrupoTfa") %>',
                success: function (response) {                    
                    var result = Ext.decode(response.responseText);
                    if (result.Success) {
                        idEmpresa = result.IdEmpresa;
                        Ext.getCmp("txtRazaoSocial").setValue(result.RazaoSocial);
                        Ext.getCmp("btnSalvar").setDisabled(false);
                    }
                    else {
                        idEmpresa = 0;
                        Ext.getCmp("btnSalvar").setDisabled(true);
                        alerta(result.Message);
                        Ext.getCmp("txtCnpj").focus();
                    }
                },
                failure: function (conn, data) {
                    idEmpresa = 0;
                    Ext.Msg.alert('Erro na pesquisa da Empresa!');
                },
                method: "POST",
                params: { cnpj: cnpj }
            });
        } else {
            idEmpresa = 0;
            alerta('CNPJ inválido!');
            limparCampos();           
        }
    }

    function limparCampos() {
        idEmpresa = 0;
        Ext.getCmp("btnSalvar").setDisabled(true);
        Ext.getCmp("txtRazaoSocial").setValue('');        
    }

    function atualizarCampos() {
        var cnpj = Ext.getCmp('txtCnpj').getValue().replace(/[^\d]+/g, '');
        if (cnpj.length == 14) {
            if (cnpj != cnpjAnterior) {
                buscarEmpresa();                
            }
        }
        else {
            limparCampos();
        }

        cnpjAnterior = cnpj;
    }

    function alerta(msg) {
        Ext.Msg.show({
            title: "Aviso",
            msg: msg,
            buttons: Ext.Msg.OK,
            icon: Ext.MessageBox.QUESTION,
            minWidth: 200
        });
    }

    function validarCNPJ(cnpj) {

        cnpj = cnpj.replace(/[^\d]+/g, '');

        if (cnpj == '')
            return false;

        if (cnpj.length != 14)
            return false;

        // Elimina CNPJs invalidos conhecidos
        if (cnpj == "00000000000000" ||
        cnpj == "11111111111111" ||
        cnpj == "22222222222222" ||
        cnpj == "33333333333333" ||
        cnpj == "44444444444444" ||
        cnpj == "55555555555555" ||
        cnpj == "66666666666666" ||
        cnpj == "77777777777777" ||
        cnpj == "88888888888888" ||
        cnpj == "99999999999999")
            return false;

        // Valida DVs
        tamanho = cnpj.length - 2
        numeros = cnpj.substring(0, tamanho);
        digitos = cnpj.substring(tamanho);
        soma = 0;
        pos = tamanho - 7;
        for (i = tamanho; i >= 1; i--) {
            soma += numeros.charAt(tamanho - i) * pos--;
            if (pos < 2)
                pos = 9;
        }
        resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
        if (resultado != digitos.charAt(0))
            return false;

        tamanho = tamanho + 1;
        numeros = cnpj.substring(0, tamanho);
        soma = 0;
        pos = tamanho - 7;
        for (i = tamanho; i >= 1; i--) {
            soma += numeros.charAt(tamanho - i) * pos--;
            if (pos < 2)
                pos = 9;
        }
        resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
        if (resultado != digitos.charAt(1))
            return false;

        return true;

    }

    fields.render("divPrincipal");

    $(function () {
        limparCampos();
        Ext.getCmp("txtCnpj").focus();
    });

</script>

