<%@ Page Title="Alterar Senha" Language="C#" MasterPageFile="~/Views/Shared/Simples.Master" Inherits="Translogic.Modules.Core.Views.Home.AlterarSenha" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server">

	<%using (var combiner = new ResourceCombiner("login"))
	 {
		// CSS
		combiner.Add(Url.CSS("Common.css"));
		combiner.Add(Url.CSS("Login.css"));
		combiner.Add(Url.CSS("Icons.css"));
		combiner.Add(Url.CSS("Messages.css"));
	 } %>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
	
	<div id="login">
	
		<div style="text-align: center;">
			<img src="<%= Url.Images("logo_310x68.png") %>" width="310" height="68" alt="<%= Resources.Geral.Translogic %>" />
			<br /><br />
		</div>
		<%= Html.ValidationSummary() %>
		<%= Html.WarningMessage(string.Format(Resources.Acesso.TituloSenhaExpirada, ViewData["quantidadeDias"]).Replace("\n", "<br />"))%>
		
		<%using (Html.BeginRouteForm("homeController", new { action = "AlterarSenhaExpirada" }))
	{ %>
			
			<br /><br />

			<%= Html.Hidden("returnUrl", Request["returnUrl"])%>

			<label>
				<%= Resources.Acesso.Senha %><br />
				<%= Html.Password("info.Senha", "" ,new { maxLength = 8 })%>
			</label>
			<br />
			<label>
				<%= Resources.Acesso.SenhaNova %><br />
				<%= Html.Password("info.SenhaNova", "",new { maxLength = 8 })%>
			</label>
			<br />
			<label>
				<%= Resources.Acesso.SenhaNovaConfirmacao %><br />
				<%= Html.Password("info.SenhaNovaConfirmacao", "", new { maxLength = 8 })%>
			</label>
			
			<br />
			
			<table width="100%">
				<tr>
					<td align="right">
						<input type="submit" value="<%= Resources.Acesso.Alterar %>" class="button-primary" />
					</td>
				</tr>
			</table>

		<%} %>
	</div>
	<script type="text/javascript">

		function focusSenha()
		{
			document.getElementById("info_Senha").focus();
		}
		
		focusSenha();
	</script>
</asp:Content>