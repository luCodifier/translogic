<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>ProxyLegado</title>
    <%
	using (var combiner = new ResourceCombiner("proxy_legado"))
	{
		combiner.Add(Url.Javascript("jQuery/jquery-1.3.2.min.js"));
		combiner.Add(Url.Javascript("jQuery/Plugins/jquery.history.js"));
	} %>
</head>
<body>
    <a href="#altA"></a>
    <a href="#moveAbaParaEsquerda"></a>
    <a href="#moveAbaParaDireita"></a>
    <a href="#novaAba"></a>
	<span id="state"></span>
	
    <script type="text/javascript">
    	$.History.bind('altA', function(state) {
    		// Custom Handler for: two
    		// alert("Alt+A");
    		parent.parent.focoNaTela();
    	});

    	$.History.bind('moveAbaParaEsquerda', function(state) {
    		// Custom Handler for: three
    		//alert("Esquerda");
    		parent.parent.MudaFocoTabDireita();
    	});

    	$.History.bind('moveAbaParaDireita', function(state) {
    		// Custom Handler for: three
    		//alert("Esquerda");
    		parent.parent.MudaFocoTabEsquerda();
    	});

    	$.History.bind('novaAba', function(state) {
    		// Custom Handler for: three
    		//alert("Esquerda");
    		parent.parent.NovaAbaTl();
    	});

    	$.History.bind('fechaAba', function(state) {
    		// Custom Handler for: three
    		//alert("Esquerda");
    		parent.parent.FechaAbaAtual();
    	});

    	$.History.bind('sairSistema', function(state) {
    		// Custom Handler for: three
    		//alert("Esquerda");
    		parent.parent.chamaSairSistema();
    	});

	</script>
</body>
</html>

