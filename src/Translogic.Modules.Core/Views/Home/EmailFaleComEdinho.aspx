<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Email.Master" Inherits="Translogic.Modules.Core.Views.Acesso.Login" %>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
	<style>
		.texto{
			font-family:Sans-Serif, Arial, Verdana, Tahoma;
			font-size: 13pt;
		}
	</style>
	<table cellpadding="0" cellspacing="0" border="0" width="300">
		<tr>
			<td width="30%"><span class="texto"><%= Resources.Geral.DataEnvio %>:</span></td>
			<td width="70%"><span class="texto"><%=ViewData["DataEnvio"] %></span>
			</td>
		</tr>
		
		<tr>
			<td width="30%">
				<span class="texto"><%= Resources.Acesso.NomeUsuario %>:</span>
			</td>
			<td width="70%">
				<span class="texto"><%=ViewData["NomeUsuario"] %></span>
			</td>
		</tr>
		<tr>
			<td><hr /></td>
		</tr>
		<tr>
			<td colspan="2">
				<div class="texto"><%=ViewData["Mensagem"] %></div>
			</td>
		</tr>
	</table>
</asp:Content>
