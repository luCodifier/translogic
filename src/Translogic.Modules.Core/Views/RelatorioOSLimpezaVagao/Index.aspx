﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Interna.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content runat="server" ID="HeadContent" ContentPlaceHolderID="HeadContent">
    <script type="text/javascript">

        //#Region StoreDDLs

        var tipoStore = new Ext.data.JsonStore({
            root: "Items",
            autoLoad: true,
            proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("ObterTipos", "OSLimpezaVagao") %>', timeout: 600000 }),
            id: 'tipoStore',
            fields: ['IdTipo', 'DescricaoTipo']
        });

        var statusStore = new Ext.data.JsonStore({
            root: "Items",
            autoLoad: true,
            proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("ObterStatus", "OSLimpezaVagao") %>', timeout: 600000 }),
            id: 'statusStore',
            fields: ['IdStatus', 'DescricaoStatus']
        });

        var gridOSLimpezaVagaoStore = new Ext.data.JsonStore({
            root: "Items",
            totalProperty: 'Total',
            id: 'gridOSLimpezaVagaoStore',
            name: 'gridOSLimpezaVagaoStore',
            proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("ConsultaOSLimpeza", "OSLimpezaVagao") %>', timeout: 600000 }),
            paramNames: {
                sort: "detalhesPaginacaoWeb.Sort",
                dir: "detalhesPaginacaoWeb.Dir",
                start: "detalhesPaginacaoWeb.Start",
                limit: "detalhesPaginacaoWeb.Limit"
            },
            fields: ['Data', 'LocalServico', 'IdOs', 'NumOs', 'Tipo', 'Fornecedor', 'Status', 'UltimaAlteracao', 'Retrabalho', 'Usuario']
        });

        var sm = new Ext.grid.CheckboxSelectionModel({
            singleSelect: true,
            header: ''

        });


        //#Region FuncoesDaTela

        function comparaDatas(dataIni, dataFim) {

            var arrDtIni = dataIni.split('/');
            var arrDtFim = dataFim.split('/');

            var dtIni = new Date(arrDtIni[2] + '/' + arrDtIni[1] + '/' + arrDtIni[0]);
            var dtFim = new Date(arrDtFim[2] + '/' + arrDtFim[1] + '/' + arrDtFim[0]);

            return dtIni <= dtFim;
        }
        
        function Pesquisar() {
            var dtI = Ext.getCmp("txtDataInicial").getRawValue();
            var dtF = Ext.getCmp("txtDataFinal").getRawValue();
            if ((dtI != "" && dtI != "__/__/____") && (dtF != "" && dtF != "__/__/____")) {
                if (comparaDatas(dtI, dtF)) {
                    gridOSLimpezaVagaoStore.load({ params: { start: 0, limit: 50} });
                    DesabilitaBotoes();
                }
                else
                    Ext.Msg.show({ title: 'Aviso', msg: 'Data Inicial deve ser menor ou igual a Data Final!', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
            }
            else
                Ext.Msg.show({ title: 'Aviso', msg: 'Favor informar as Datas Inicial e Final!', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
        }


        function ColocarHojeEmDatas() {

            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();

            if (dd < 10) {
                dd = '0' + dd
            }

            if (mm < 10) {
                mm = '0' + mm
            }

            today = dd + '/' + mm + '/' + yyyy;

            Ext.getCmp("txtDataInicial").setValue(today);
            Ext.getCmp("txtDataFinal").setValue(today);

        }

        function Limpar() {
            gridOSLimpezaVagaoStore.removeAll();
            Ext.getCmp("grid-filtros").getForm().reset();
        }

        // Busca e armazena lista de ids de vagões
        function BuscaIDOrdemServicoSelecionado() {
            var selection = sm.getSelections();
            var id;
            if (selection && selection.length > 0) {
                var IdOs = selection[0].data.IdOs;
                var listaOS = [];

                selection = sm.getSelections();

                $.each(selection, function (i, e) {
                    id = e.data.IdOs;
                });
            }

            return id;
        }
        
        function DesabilitaBotoes(){
            Ext.getCmp("btnImprimir").setDisabled(true);
            Ext.getCmp("btnVizualizar").setDisabled(true);
        }

        function HabilitaBotoes() {
            Ext.getCmp("btnImprimir").setDisabled(false);
            Ext.getCmp("btnVizualizar").setDisabled(false);
        }


       

        //#endRegion

        //#Region PainelMotivosVagoes

        $(function () {

            // Toolbar da Grid
            var pagingToolbar = new Ext.PagingToolbar({
                pageSize: 50,
                store: gridOSLimpezaVagaoStore,
                displayInfo: true,
                displayMsg: App.Resources.Web.GridExibindoRegistros,
                emptyMsg: App.Resources.Web.GridSemRegistros,
                paramNames: {
                    start: "detalhesPaginacaoWeb.Start",
                    limit: "detalhesPaginacaoWeb.Limit"
                }
            });

            var detalheActionOrigens = new Ext.ux.grid.RowActions({
                dataIndex: '',
                header: '',
                align: 'center',
                actions: [
                    {
                        iconCls: 'icon-del',
                        tooltip: 'Excluir Motivo'
                    }],
                callbacks: {
                    'icon-del': function (grid, record, action, row, col) {

                        if (record.data.IdMotivo) {
                            if (Ext.Msg.confirm("Excluir", "Deseja realmente excluir esse Motivo ?", function (btn, text) {
                                if (btn == 'yes') {
                                    ExcluirMotivo(record.data.IdVagaoMotivo, record.data.IdVagao, record.data.IdMotivo);
                                    Pesquisar();
                                }

                            }));
                        }
                    }
                }
            });

          

            var cm = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true
                },
                columns: [
                    new Ext.grid.RowNumberer(),
                    sm,
                    { header: 'Data', dataIndex: "Data", sortable: false, width: 100 },
                    { header: 'Local', dataIndex: "LocalServico", sortable: false, width: 80 },
                    { header: 'OS', dataIndex: "NumOs", sortable: false, width: 60 },
                    { header: 'Tipo', dataIndex: "Tipo", sortable: false, width: 100 },
                    { header: 'Fornecedor', dataIndex: "Fornecedor", sortable: false, width: 250 },
                    { header: 'Status', dataIndex: "Status", sortable: false, width: 90 },
                    { header: 'Retrabalho', dataIndex: "Retrabalho", sortable: false, width: 50 },
                    { header: 'Última mudança de status', dataIndex: "UltimaAlteracao", sortable: false, width: 140 },
                    { header: 'Usuário', dataIndex: "Usuario", sortable: false, width: 100 }
                ]
            });

            var gridPainelOSLimpeza = new Ext.grid.EditorGridPanel({
                id: 'gridPainelOSLimpeza',
                name: 'gridPainelOSLimpeza',
                autoLoadGrid: false,
                height: 360,
                width: 870,
                stripeRows: true,
                cm: cm,
                region: 'center',
                viewConfig: {
                    forceFit: true,
                    emptyText: 'Não possui dado(s) para exibição.'
                },
                autoScroll: true,
                loadMask: { msg: App.Resources.Web.Carregando },
                store: gridOSLimpezaVagaoStore,
                plugins: [detalheActionOrigens],
                tbar: [
                 {
                     id: 'btnVizualizar',
                     text: 'Visualizar última via',
                     tooltip: 'Visualizar última via',
                     disabled: true,
                     iconCls: 'icon-detail',
                     handler: function (c) {

                         Ext.getBody().mask("Processando dados...", "x-mask-loading");

                         // Busca idOS linha selecionada
                         var idOs = BuscaIDOrdemServicoSelecionado();
                         var url = '<%= Url.Action("VisualizarOSLimpeza", "OSLimpezaVagao") %>';

                         url += "?idOs=" + idOs;

                         window.location = url;
                     }
                 },
                    {
                        id: 'btnImprimir',
                        text: 'Imprimir',
                        tooltip: 'Imprimir uma OS em PDF',
                        iconCls: 'icon-printer',
                        disabled: true,
                        handler: function (c) {
                            var ItSel = sm.getSelections();
                            var i, qt = ItSel.length;
                            if (qt == 1) {
                                var url = '<%= Url.Action("Imprimir", "OSLimpezaVagao") %>?idOS=' + ItSel[0].data.IdOs;
                                window.open(url, "");
                            }
                        }
                    },
                    {
                        id: 'btnExportar',
                        text: 'Exportar',
                        tooltip: 'Exportar para Excel',
                        iconCls: 'icon-page-excel',
                        handler: function (c) {


                            var dtI = Ext.getCmp("txtDataInicial").getRawValue();
                            var dtF = Ext.getCmp("txtDataFinal").getRawValue();
                            if ((dtI != "" && dtI != "__/__/____") && (dtF != "" && dtF != "__/__/____")) {
                                if (comparaDatas(dtI, dtF)) {

                                    Ext.getBody().mask("Processando dados...", "x-mask-loading");

                                    var url = '<%= Url.Action("ObterConsultarOSLimpezaVagoesExportar", "OSLimpezaVagao") %>';

                                    var dataInicial = Ext.getCmp("txtDataInicial").getRawValue();
                                    var dataFinal = Ext.getCmp("txtDataFinal").getRawValue();

                                    if (dataInicial == "__/__/____")
                                        dataInicial = "";
                                    if (dataFinal == "__/__/____")
                                        dataFinal = "";

                                    var txtLocal = Ext.getCmp("txtLocal").getValue();
                                    var txtNumOs = Ext.getCmp("txtNumOs").getValue();

                                    ddlTipo = Ext.getCmp("ddlTipo");
                                    var idTipo = (ddlTipo.getValue() == "Todos" || ddlTipo.getValue() == "0") ? "" : ddlTipo.getValue();

                                    ddlStatus = Ext.getCmp("ddlStatus");
                                    var idStatus = (ddlStatus.getValue() == "Todos" || ddlStatus.getValue() == "0") ? "" : ddlStatus.getValue();

                                    ddlRetrabalho = Ext.getCmp("ddlRetrabalho");
                                    var retrabalho = (ddlRetrabalho.getValue() == "Todos" || ddlRetrabalho.getValue() == "0") ? "" : ddlRetrabalho.getValue();

                                    url += "?dataInicial=" + dataInicial + '&dataFinal=' + dataFinal + '&local=' + txtLocal;
                                    url += '&numOs=' + txtNumOs + '&idTipo=' + idTipo + '&idStatus=' + idStatus + '&retrabalho=' + retrabalho;

                                    window.open(url, "");

                                    Ext.getBody().unmask();
                                   
                                }
                                else
                                    Ext.Msg.show({ title: 'Aviso', msg: 'Data Inicial deve ser menor ou igual a Data Final!', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                            }
                            else
                                Ext.Msg.show({ title: 'Aviso', msg: 'Favor informar as Datas Inicial e Final!', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                            
                        }
                    }
                ],
                bbar: pagingToolbar,
                sm: sm,
                listeners: {

                    cellclick: function (e, rowIndex, columnIndex) {
                        var record = gridPainelOSLimpeza.getStore().getAt(rowIndex);
                        var status = record.get('Status');
                        var selection = sm.getSelections();
                        var selecionado = selection.length > 0 ? true : false;

                        if (selecionado == true) {

                            HabilitaBotoes();
                        }
                        else {

                            DesabilitaBotoes();
                        }

                    }

                } 
            });

            gridPainelOSLimpeza.getStore().proxy.on('beforeload', function (p, params) {

                var dataInicial = Ext.getCmp("txtDataInicial").getRawValue();
                var dataFinal = Ext.getCmp("txtDataFinal").getRawValue();

                if (dataInicial == "__/__/____")
                    dataInicial = "";
                if (dataFinal == "__/__/____")
                    dataFinal = "";


                var txtLocal = Ext.getCmp("txtLocal").getValue();
                var txtNumOs = Ext.getCmp("txtNumOs").getValue();

                ddlTipo = Ext.getCmp("ddlTipo");
                var idTipo = (ddlTipo.getValue() == "Todos" || ddlTipo.getValue() == "0") ? "" : ddlTipo.getValue();


                ddlStatus = Ext.getCmp("ddlStatus");
                var idStatus = (ddlStatus.getValue() == "Todos" || ddlStatus.getValue() == "0") ? "" : ddlStatus.getValue();

                ddlRetrabalho = Ext.getCmp("ddlRetrabalho");
                var retrabalho = (ddlRetrabalho.getValue() == "Todos" || ddlRetrabalho.getValue() == "0") ? "" : ddlRetrabalho.getValue();

                params['dataInicial'] = dataInicial;
                params['dataFinal'] = dataFinal;
                params['local'] = txtLocal;
                params['numOs'] = txtNumOs;
                params['idTipo'] = idTipo;
                params['idStatus'] = idStatus;
                params['retrabalho'] = retrabalho;

            });


            //#endRegion

            var txtDataInicial = new Ext.form.DateField({
                fieldLabel: 'Data Inicial',
                id: 'txtDataInicial',
                name: 'txtDataInicial',
                dateFormat: 'd/n/Y',
                width: 83,
                plugins: [new Ext.ux.InputTextMask('99/99/9999', true)]

            });

            var txtDataFinal = new Ext.form.DateField({
                fieldLabel: 'Data Final',
                id: 'txtDataFinal',
                name: 'txtDataFinal',
                dateFormat: 'd/n/Y',
                width: 83,
                plugins: [new Ext.ux.InputTextMask('99/99/9999', true)]

            });

            var txtLocal = {
                xtype: 'textfield',
                name: 'txtLocal',
                id: 'txtLocal',
                fieldLabel: 'Local',
                autoCreate: {
                    tag: 'input',
                    type: 'text',
                    autocomplete: 'off',
                    maxlength: '12'
                },
                style: 'text-transform:uppercase;',
                width: 85
            };


            var txtNumOs = {
                xtype: 'textfield',
                name: 'txtNumOs',
                id: 'txtNumOs',
                fieldLabel: 'OS',
                autoCreate: {
                    tag: 'input',
                    type: 'text',
                    autocomplete: 'off',
                    maxlength: '12'
                },
                maskRe: /[0-9+;]/,
                style: 'text-transform:uppercase;',
                width: 85
            };



            var ddlTipo = new Ext.form.ComboBox({
                editable: true,
                typeAhead: true,
                forceSelection: true,
                disableKeyFilter: true,
                triggerAction: 'all',
                lazyRender: true,
                mode: 'local',
                valueField: 'IdTipo',
                displayField: 'DescricaoTipo',
                fieldLabel: 'Tipo',
                id: 'ddlTipo',
                name: 'ddlTipo',
                width: 200,
                store: tipoStore,
                value: 'Todos'
            });


            var ddlStatus = new Ext.form.ComboBox({
                editable: true,
                typeAhead: true,
                forceSelection: true,
                disableKeyFilter: true,
                triggerAction: 'all',
                lazyRender: true,
                mode: 'local',
                store: statusStore,
                valueField: 'IdStatus',
                displayField: 'DescricaoStatus',
                fieldLabel: 'Status',
                id: 'ddlStatus',
                width: 100,
                value: '99'
            });


            var ddlRetrabalho = new Ext.form.ComboBox({
                editable: true,
                typeAhead: true,
                forceSelection: true,
                disableKeyFilter: true,
                triggerAction: 'all',
                lazyRender: true,
                mode: 'local',
                valueField: 'IdRetrabalho',
                displayField: 'Retrabalho',
                fieldLabel: 'Retrabalho',
                id: 'ddlRetrabalho',
                width: 85,
                store: [
                            'Todos',
                            'Sim',
                            'Não'
                        ],
                value: 'Todos'
            });

            //Item do Array de Componentes (1 por cada componente criado)
            var arrDataInicial = {
                width: 100,
                layout: 'form',
                border: false,
                items: [txtDataInicial]
            };

            var arrDataFinal = {
                width: 100,
                layout: 'form',
                border: false,
                items: [txtDataFinal]
            };

            var arrLocal = {
                width: 100,
                layout: 'form',
                border: false,
                items: [txtLocal]
            };

            var arrNumOs = {
                width: 100,
                layout: 'form',
                border: false,
                items: [txtNumOs]
            };

            var arrTipo = {
                width: 210,
                layout: 'form',
                border: false,
                items: [ddlTipo]
            };

            var arrStatus = {
                width: 110,
                layout: 'form',
                border: false,
                items: [ddlStatus]
            };

            var arrRetrabalho = {
                width: 100,
                layout: 'form',
                border: false,
                items: [ddlRetrabalho]
            };

            //Array de componentes do form 
            var arrCampos = {
                layout: 'column',
                border: false,
                items: [arrDataInicial, arrDataFinal, arrLocal, arrNumOs, arrTipo, arrStatus, arrRetrabalho]
            };


            //Conteudo do Form que sera criado
            var filtros = new Ext.form.FormPanel({
                id: 'grid-filtros',
                title: "Filtros",
                region: 'center',
                bodyStyle: 'padding:15px;',
                layout: 'form',
                labelAlign: 'top',
                items: [arrCampos],
                buttonAlign: "center",
                buttons:
                    [{
                        text: 'Pesquisar',
                        type: 'submit',
                        iconCls: 'icon-find',
                        handler: function (b, e) {
                            Pesquisar();
                        }
                    },
                        {
                            text: 'Limpar',
                            type: 'submit',
                            handler: function (b, e) {
                                Limpar();
                            }
                        }
                    ]
            });

            //#endRegion



            new Ext.Viewport({
                layout: 'border',
                margins: 10,
                items: [
                    {
                        region: 'north',
                        height: 190,
                        autoScroll: true,
                        items: [{
                            region: 'center',
                            applyTo: 'header-content'
                        },
                            filtros]
                    },
                    gridPainelOSLimpeza
                ]
            });




            Ext.onReady(function () {
                $("#txtDataInicial").val("__/__/____");
                $("#txtDataFinal").val("__/__/____");

                ColocarHojeEmDatas();


                statusStore.on('load', function (store) {
                    Ext.getCmp('ddlStatus').setValue("99");
                });

                Pesquisar();
            });

        });
    </script>
</asp:Content>
<asp:Content runat="server" ID="Content2" ContentPlaceHolderID="Content">
    <div id="header-content">
        <h1>
            Relat&oacute;rio de Ordem de Serviço de Limpeza de Vagão</h1>
        <small>Você está em Consultas > Relat&oacute;rios > Relat&oacute;rio de Ordem de Serviço
            de Limpeza de Vagão</small>
        <br />
    </div>
</asp:Content>
