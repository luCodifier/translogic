<%@ Import Namespace="System.Security.Policy" %>

<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Interna.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content runat="server" ID="HeadContent" ContentPlaceHolderID="HeadContent">
	<script type="text/javascript">
		var idComunicado = 0;
		var windowNovoComunicado;

		var dsGrid = new Ext.data.JsonStore({
			url: '<%= Url.Action("ObterComunicados") %>',
			root: "Items",
			autoLoad: true,
			remoteSort: true,
			fields: [
				'Id',
				'Estado',
				'DataCadastro'
				]
		});

		var detalheAction = new Ext.ux.grid.RowActions({
			dataIndex: '',
			header: '',
			align: 'center',
			actions: [
            {
            	iconCls: 'icon-detail',
            	tooltip: 'Detalhes'
            },
            {
            	iconCls: 'icon-del',
            	tooltip: 'Excluir'
            }],
			callbacks: {
				'icon-detail': function (grid, record, action, row, col) {
					alert('detalhe');
				},
				'icon-del': function (grid, record, action, row, col) {
				
					var id = grid.getStore().getAt(row).data.Id;


					Ext.Ajax.request({    
                        url: "<% =Url.Action("Excluir") %>",
                        method: "POST",                        
			            params: {  idComunicado : id },
			            success: function(response) {
                             
                             var result = Ext.util.JSON.decode(response.responseText);
                             
                            if(result.success) {
								Ext.Msg.show({
									title: "Mensagem de Informa��o",
									msg: "Sucesso na Exclus�o do comunicado!",
									buttons: Ext.Msg.OK,
									minWidth: 200
								});
							}

							dsGrid.load();
			            },
                        failure: function(conn, data){
                            Ext.Msg.show({
								title: "Mensagem de Erro",
								msg: "Erro na exclus�o do comunicado!</BR>" + result.Message,
								buttons: Ext.Msg.OK,
								icon: Ext.MessageBox.ERROR,
								minWidth: 200
							});
                        }			            
		            });

				
					// Excluir(int idComunicado)
				}
			}
		});

		var grid = new Ext.grid.EditorGridPanel({
			viewConfig: {
				forceFit: true
			},
			stripeRows: true,
			region: 'center',
			height: 318,
			width: 400,
			autoLoadGrid: false,
			store: dsGrid,
			plugins: [detalheAction],
			loadMask: { msg: App.Resources.Web.Carregando },
			colModel: new Ext.grid.ColumnModel({
				defaults: {
					sortable: false
				},
				columns: [
						{ dataIndex: "Id", hidden: true },
						detalheAction,
						{ header: 'Estado', dataIndex: "Estado", width: 20, sortable: false },
						{ header: 'Data Cadastro', dataIndex: "DataCadastro", width: 20, sortable: false }
					]
			}),
			tbar: [{
				id: 'btnNovo',
				text: 'Novo',
				tooltip: 'Adicionar novo comunicado',
				iconCls: 'icon-new',
				handler: function (c) {
					ExibirFormNovoComunicado();
				}
			}]

		});

		var colunaDet1 = {
			title: '',
			width: 751,
			height: 320,
			layout: 'form',
			border: true,
			items: [grid]
		};


		//***********************
		// Novo E-mail      *
		//***********************
		function ExibirFormNovoComunicado() {
			windowNovoComunicado = new Ext.Window({
				id: 'windowNovoComunicado',
				title: 'Cadastro de Comunicado',
				modal: true,
				width: 500,
				height: 170,
				autoLoad: {
					url: '<%= Url.Action("FormNovoComunicado") %>',
					params: { 'idComunicado': idComunicado },
					scripts: true
				}
			});

			windowNovoComunicado.show();
		}

		filters = new Ext.form.FormPanel({
			id: 'grid-filtros',
			title: "",
			region: 'center',
			bodyStyle: 'padding: 15px',
			labelAlign: 'top',
			items: [grid],
			buttonAlign: "center"
		});

		$(function () {

			filters.render('header-content');

		});
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
	<div id="header-content">
		<h1>
			Comunicado Cte</h1>
		<small>Voc� est� em Cte > Comunicado</small>
		<br />
	</div>
</asp:Content>
