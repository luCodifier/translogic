﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%@ Import Namespace="Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico" %>
<div id="div-form-novo-Comunicado">
</div>
<script type="text/javascript">

	var idComunicado = '<%=ViewData["idComunicado"] %>';

	var dsEstados = new Ext.data.JsonStore({
		url: '<%= Url.Action("ObterEstados") %>',
		root: "Items",
		autoLoad: true,
		remoteSort: true,
		fields: [
				'Id',
				'Descricao'
				]
	});

	var fieldArquivo = {
		xtype: 'fileuploadfield',
		id: 'field-Arquivo',
		fieldLabel: 'Arquivo',
		name: 'fieldArquivo',
		allowBlank: false,
		maxLength: 100,
		width: 250,
		hiddenName: 'fieldArquivo'
	};

	var cboEstado = {
		xtype: 'combo',
		store: dsEstados,
		allowBlank: false,
		mode: 'local',
		typeAhead: false,
		triggerAction: 'all',
		fieldLabel: 'Estado',
		name: 'fieldEstado',
		id: 'field-Estado',
		hiddenName: 'filtro-Estado',
		displayField: 'Descricao',
		forceSelection: true,
		width: 100,
		valueField: 'Id',
		emptyText: 'Selecione...'
	};

	var colEstado = {
		width: 105,
		layout: 'form',
		border: false,
		items: [cboEstado]
	};

	var colArquivo = {
		width: 255,
		layout: 'form',
		border: false,
		items: [fieldArquivo]
	};

	var arrlinha1 = {
		layout: 'column',
		border: false,
		items: [colEstado, colArquivo]
	};

	var arrCampos = new Array();
	arrCampos.push(arrlinha1);

	var fields = new Ext.form.FormPanel({
		id: 'fields-Cadastro',
		title: "Filtros",
		fileUpload: true,
		region: 'center',
		bodyStyle: 'padding: 15px',
		labelAlign: 'top',
		items: [arrCampos],
		buttonAlign: "center",
		buttons:
	            [
                 {
                 	text: 'Salvar',
                 	type: 'submit',
                 	iconCls: 'icon-save',
                 	handler: function (b, e) {
                 		Salvar();
                 	}
                 },
                 {
                 	text: 'Limpar',
                 	handler: function (b, e) {
                 		Ext.getCmp("fields-Cadastro").getForm().reset();
                 	},
                 	scope: this
                 }
                ]
	});

	function Salvar() {

		fields.form.submit({
			url: '<%= Url.Action("Salvar") %>',
			success: function (result) {
				alert("Registro Cadastrado com Sucesso!");
				windowNovoComunicado.close();
				dsGrid.load();
			},
			failure: function (conn, data) {
				alert("Ocorreu um erro inesperado.");
			}
		}
        );
	}

	fields.render("div-form-novo-Comunicado");
	
</script>
