﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" %>
<%@ Import Namespace="System.Globalization" %>
<html>
    <head>
        <title></title>
        <style type="text/css">
            .pdfPage {
                margin-top: 10px;
                width: 635px;
                font-size: 10pt;
            }
        </style>
    </head>
    <body>
    
    <table class="pdfPage" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td><%=ViewData["teste"] %></td>
        </tr>
        <% for (int i = 1; i < 101; i++) { %>
        <tr>
            <td>Teste <%=i %></td>
        </tr>
        <% }%>
    </table>
    
</body>
</html>
