<%@ Page Title="Login" Language="C#" MasterPageFile="~/Views/Shared/Simples.Master" Inherits="Translogic.Modules.Core.Views.Acesso.Login" %>
<%@ Import Namespace="Translogic.Modules.Core.App_GlobalResources" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server">

	<%using (var combiner = new ResourceCombiner("login"))
	 {
		// CSS
		combiner.Add(Url.CSS("Common.css"));
		combiner.Add(Url.CSS("Login.css"));
		combiner.Add(Url.CSS("Icons.css"));
		combiner.Add(Url.CSS("Messages.css"));
		combiner.Add(Url.Javascript("jQuery/jquery-1.3.2.min.js"));
	 } %>
	 <script type="text/javascript" language="javascript">
	 function CookieSetText()
		{
			var cookieEnabled=(navigator.cookieEnabled)? true : false
			//if not IE4+ nor NS6+
			if (typeof navigator.cookieEnabled=="undefined" && !cookieEnabled)
			{
				document.cookie = "testcookie";
				cookieEnabled = (document.cookie.indexOf("testcookie") != -1) ? true : false;
			}
			return cookieEnabled;
		}

		function focusUsuario() {
			document.getElementById("info_Usuario").focus();
		}

		function lembrarSenha() {
			var codigoUsuario = document.getElementById("info_Usuario").value;

			if (codigoUsuario && codigoUsuario.length > 0) {
				document.forms[0].action = '<%= Url.RouteUrl("acesso", new {action = "LembrarSenha"}) %>';
				document.forms[0].submit();
			} else {
				alert("<%= Acesso.UsuarioVazio %>");
				focusUsuario();
			}
		}

		function setaUrlLogin() {
			document.forms[0].action = '<%= Url.RouteUrl("acesso", new {action = "autenticar"}) %>';
		}
		
		/*
		$(function() {
			posicionarDiv();
		});

		function posicionarDiv() {
			var pos = $("#login").offset();
			var width = $("#login").width();
			if (pos.left >= 320) {
				$("#widgetCopa").css({ "left": (pos.left - width) + "px", "top": pos.top + "px" });
			} else {
				$("#widgetCopa").css({ "left": (pos.left + width + 20) + "px", "top": pos.top + "px" });
			}
			$("#widgetCopa").show();
		}

		$(window).bind('resize', function() {
			posicionarDiv();
		});*/
	 </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">

	<div id="login">
	
		<div style="text-align: center;">
			<img src="<%=Url.Images("logo_310x68.png")%>" width="310" height="68" alt="<%=Geral.Translogic%>" />
			<br /><br />
		</div>
		
		<div id="divFormLogin">
			<%= Html.ValidationSummary() %>
			
			<%
			if (ViewData.Model != null)
			{
				if ( ViewData.Model.LogoutEfetuado ){%>
					<%= Html.SuccessMessage(Acesso.LogoutEfetuado) %>
				<%}%>
			
				<%if ( ViewData.Model.LembreteSenhaEnviado ){%>
					<%= Html.SuccessMessage(Acesso.LembreteSenhaEnviado)%>
				<%}
			}%>

			<%using (Html.BeginRouteForm("acesso", new {action = "autenticar"})){ %>

				<br />

				<%= Html.Hidden("returnUrl", Request["returnUrl"])%>

				<label>
					<%= Acesso.Usuario %><br />
					<%= Html.TextBox("info.Usuario")%>
				</label>
				
				<label>
					<%= Acesso.Senha %><br />
					<%= Html.Password("info.Senha") %>
				</label>
				
				<label>
					<%= Acesso.Idioma %><br />
					<%= Html.DropDownList("info.Idioma", ViewData["idiomas"] as IEnumerable<SelectListItem>)%>
				</label>
				
				<br />
				
				<table width="100%">
					<tr>
						<td align="left" width="200">
							<a href="#" id="esqueceu-senha" onclick="lembrarSenha(); return false;"><%= Acesso.EsqueceuSuaSenha %></a>
						</td>
						<td align="right">
							<input type="submit" value="<%= Acesso.Conectar %>" class="button-primary" onclick="setaUrlLogin();" />
						</td>
					</tr>
				</table>
			<%} %>
			<% // = Html.InfoMessage(Resources.Acesso.MsgTmp) %>
		</div>
		
		<%
		if (ViewData["PossuiAcessoCookies"]!=null)
		{
			if (ViewData["PossuiAcessoCookies"].ToString() == "N")
			{%>
				<%=Html.ErrorMessage(Acesso.CookiesDevemEstarHabilitados)%>
			<%
			}
			else
			{%>
				<script type="text/javascript">
					var isEnabled = CookieSetText();
					var msg = '<%=Html.ErrorMessage(Acesso.CookiesDevemEstarHabilitados)%>';
					if (!isEnabled) {
						document.write(msg);
						document.getElementById("divFormLogin").style.display = "none";
					} else {
						focusUsuario();
					}
				</script>
			<%
			}
		}%>
	</div>
</asp:Content>
