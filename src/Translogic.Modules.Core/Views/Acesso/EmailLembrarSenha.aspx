<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Email.Master" Inherits="Translogic.Modules.Core.Views.Acesso.Login" %>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
	<span>Os seus dados de acesso ao TRANSLOGIC:</span>
	<br />
	<br />
	<table cellpadding="0" cellspacing="0" border="0" width="300" style="font-family: Verdana; font-size: 9px;">
		<tr>
			<td width="30%"><%= Resources.Acesso.Usuario %>:</td>
			<td width="70%"> <%=ViewData.Model.Usuario %>
			</td>
		</tr>
		<tr>
			<td width="30%">
				<%= Resources.Acesso.NomeUsuario %>:
			</td>
			<td width="70%">
				<%=ViewData.Model.NomeUsuario %>
			</td>
		</tr>
		<tr>
			<td>
				<%= Resources.Acesso.Senha %>:
			</td>
			<td>
				<%=ViewData.Model.Senha %>
			</td>
		</tr>
	</table>
	<br />
	<%= Resources.Acesso.MensagemAlterarSenha%>
</asp:Content>
