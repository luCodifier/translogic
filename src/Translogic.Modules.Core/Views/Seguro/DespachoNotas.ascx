﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div id="div-form-despacho-notas" style="display: none;">
</div>
<script type="text/javascript">

    var camposDespachoNotas = Ext.data.Record.create(
    [
        { name: 'NumNota', mapping: 'NumNota' },
        { name: 'SerieNota', mapping: 'SerieNota' },
        { name: 'ChaveNota', mapping: 'ChaveNota' },
        { name: 'Volume', mapping: 'Volume' },
        { name: 'Peso', mapping: 'Peso' },
        { name: 'ProdutoValor', mapping: 'ProdutoValor' },
        { name: 'ProdutoUnidadeMedida', mapping: 'ProdutoUnidadeMedida' },
        { name: 'ProdutoQuantidade', mapping: 'ProdutoQuantidade' }
    ]);

    var readerDespachoNotas = new Ext.data.JsonReader({
        root: 'Items',
        id: 'ReaderGridDespachoNotas'
    }, camposDespachoNotas);


    var dsDespachoNotas = new Ext.ux.MultiGroupingStore({
        idIndex: 0,
        reader: readerDespachoNotas,
        storeId: 'dsDespachoNotas',
        groupField: ['NumNota']
    });


    var txtDespachoNota = {
        xtype: 'numberfield',
        allowDecimals: false,
        id: 'txtDespachoNota',
        name: 'txtDespachoNota',
        typeAhead: true,
        fieldLabel: 'Despacho',
        width: 80,
        allowBlank: false,
        cls: 'x-item-disabled'
    };

    var txtSerieNota = {
        xtype: 'textfield',
        id: 'txtSerieNota',
        name: 'txtSerieNota',
        typeAhead: true,
        fieldLabel: 'Série',
        width: 80,
        enableKeyEvents: true,
        cls: 'x-item-disabled',        
        style: {
            textTransform: 'uppercase'
        },
        allowBlank: false,
        listeners: {
            change: function() {
                
            }
        }
    };

    var chkDespachoIntercambioNota = {
        xtype: 'checkbox',
        fieldLabel: 'Intercâmbio',
        id: 'chkDespachoIntercambioNota',
        name: 'chkDespachoIntercambioNota',
        value: true        
    }

    var gridDespachoNotas = new Ext.grid.GridPanel({
        id: 'gridDespachoNotas',
        name: 'gridDespachoNotas',
        autoLoadGrid: false,
        height: 400,
        width: 750,
        viewConfig: {
            forceFit: true
        },
        stripeRows: true,
        //region: 'center',
        store: dsDespachoNotas, //storeDespachoNotas,
        loadMask: { msg: App.Resources.Web.Carregando },
        colModel: new Ext.grid.ColumnModel({
            defaults: {
                sortable: false
            },
            columns: [
                    { header: 'Número', width: 40, dataIndex: "NumNota", sortable: false },
                    { header: 'Série Nota', width: 30, dataIndex: "SerieNota", sortable: false },
                    { header: 'Chave NFe', width: 180, dataIndex: "ChaveNota", sortable: false },
					{ header: 'Volume NFe', width: 40, dataIndex: "Volume", sortable: false },
					{ header: 'Peso NFe', width: 40, dataIndex: "Peso", sortable: false },
                    { header: 'Valor Prod.', width: 40, dataIndex: "ProdutoValor", sortable: false },
                    { header: 'Qtde. Prod.', width: 40, dataIndex: "ProdutoQuantidade", sortable: false },
                    { header: 'Unid. Prod.', width: 40, dataIndex: "ProdutoUnidadeMedida", sortable: false }
				]
        }),
        listeners: {
            rowdblclick: function (grid, rowIndex, record, e) {                
            },
            rowclick: function (grid, rowIndex, e) {
            }
        }
    });    

    var coluna1Linha1 = {
        layout: 'form',
        border: false,
        bodyStyle: 'padding-right:10px',
        items: [txtDespachoNota]
    };

    var coluna2Linha1 = {
        layout: 'form',
        border: false,
        bodyStyle: 'padding-right:10px',
        items: [txtSerieNota]
    };

    var coluna3Linha1 = {
        layout: 'form',
        border: false,
        bodyStyle: 'padding-right:10px',
        items: [chkDespachoIntercambioNota]
    };

    var linha1 = {
        layout: 'column',
        border: false,
        items: [coluna1Linha1, coluna2Linha1, coluna3Linha1]
    };

    var btnFechar = {
        name: 'btnFecharDetalhes',
        id: 'btnFecharDetalhes',
        text: 'Fechar',
        handler: function () {
            //Ext.getCmp('formModalDespachoNotas').close();
            windowDespachoNotas.hide();
        }
    };

    var painelDespachoNotas = new Ext.form.FormPanel({
        id: 'painelDespachoNotas',
        title: 'Despacho/Série',
        name: 'painelDespachoNotas',
        bodyStyle: 'padding: 10px',
        labelAlign: 'top',
        items: [linha1],
        buttonAlign: 'center',
        buttons: [btnFechar]
    });


    function LimparGridDespachoNotas() {
        gridDespachoNotas.getStore().removeAll();
    }

    function CarregarGridDespachoNotas() {

        var loadingMaskDespachoNotas = new Ext.LoadMask(gridDespachoNotas.getEl(), { msg: "Buscando as notas fiscais. Aguarde alguns instantes..." });         
        loadingMaskDespachoNotas.show();

        window.jQuery.ajax({
                        url: '<%= Url.Action("DespachoNotasConsulta") %>',
                        type: "POST",
                        data: {
                            numeroDespacho: Ext.getCmp("txtDespachoNota").getValue(),
                            numeroSerie: Ext.getCmp("txtSerieNota").getValue(),
                            despachoIntercambio: Ext.getCmp("chkDespachoIntercambioNota").getValue()
                        },
                        dataType: "json",
                        success: function(data) {

                            gridDespachoNotas.getStore().removeAll();

                            if (data.Sucesso) {

                                $.each(data.items, function (index, element) {

                                    var rec = new dsDespachoNotas.recordType({
                                        NumNota: element.NumNota,
                                        SerieNota: element.SerieNota,
                                        ChaveNota: element.ChaveNota,
                                        Volume: element.Volume,
                                        Peso: element.Peso,
                                        ProdutoValor: element.ProdutoValor,
                                        ProdutoQuantidade: element.ProdutoQuantidade,
                                        ProdutoUnidadeMedida: element.ProdutoUnidadeMedida
                                    });
                                    rec.commit();
                                    dsDespachoNotas.add(rec);

                                });

                                loadingMaskDespachoNotas.hide();

                            } else {
                                loadingMaskDespachoNotas.hide();

                                window.Ext.Msg.show({
                                    title: "Despacho/Série Notas",
                                    msg: data.Mensagem,
                                    buttons: window.Ext.Msg.OK,
                                    icon: window.Ext.MessageBox.ERROR,
                                    minWidth: 200,
                                    fn: function () {
                                    },
                                    close: function () {
                                    }
                                });   
                            }
                            
                        },
                        error: function(jqXHR, textStatus) {
                            window.Ext.Msg.show({
                                title: "Erro no Servidor",
                                msg: 'Ocorreu um erro no servidor, por favor, tente novamente mais tarde.',
                                buttons: window.Ext.Msg.OK,
                                icon: window.Ext.MessageBox.ERROR,
                                minWidth: 200
                            });                            
                        }
                    });

    }

    /******************************* RENDER *******************************/
    Ext.onReady(function () {
        painelDespachoNotas.render("div-form-despacho-notas");
        gridDespachoNotas.render("div-form-despacho-notas");

//        Ext.getCmp("txtDespachoNota").setValue('<%=ViewData["numeroDespacho"] %>');
//        Ext.getCmp("txtSerieNota").setValue('<%=ViewData["numeroSerie"] %>');
//        var despachoIntercambio = '<%=ViewData["despachoIntercambio"] %>';

//        Ext.getCmp("chkDespachoIntercambioNota").setValue(false);
//        if (despachoIntercambio === 'True') {
//            Ext.getCmp("chkDespachoIntercambioNota").setValue(true);    
//        }
        
        Ext.getCmp('txtDespachoNota').setReadOnly(true);
        Ext.getCmp('txtSerieNota').setReadOnly(true);

        //CarregarGridDespachoNotas();
    });

    
</script>