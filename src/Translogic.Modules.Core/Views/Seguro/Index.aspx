﻿<%@ Page Title="LAudo de Indenizações" Language="C#" MasterPageFile="~/Views/Shared/Interna.Master"
    Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">

        var formModal = null;
        var grid = null;
        var viewPort = null;

        var fm = Ext.form;

        function salvarAlteracoes() {
            var modifiedRecords = grid.getStore().getModifiedRecords();
            if (modifiedRecords.length > 0) {
                if (Ext.Msg.confirm("Laudo de Indenizações", "Confirma a alteração?",
                        function(btn, text) {
                            if (btn == 'yes') {
                                for (var i = 0; i < modifiedRecords.length; i++) {
                                    var fields = modifiedRecords[i];
                                    var dataSinistro =  fields.get("Data");

                                    alert(dataSinistro);
                                    Ext.Ajax.request({
                                        url: '<%= Url.Action("SalvarProcessoSeguroCache") %>',
                                        method: 'POST',
                                        timeout: 120000,
                                        params: {
                                            'id': fields.get("Id"),
                                            'despacho': fields.get("Despacho"),
                                            'serie': fields.get("Serie"),
                                            'data': dataSinistro,
                                            'laudo': fields.get("Laudo"),
                                            'valorMercadoria': fields.get("valorMercadoria"),
                                            'terminal': fields.get("Terminal"),
                                            'perda': fields.get("Perda"),
                                            'causa': fields.get("Causa"),
                                            'gambit': fields.get("Gambit"),
                                            'lacre': fields.get("Lacre"),
                                            'vistoriador': fields.get("Vistoriador"),
                                            'avaliacao': fields.get("Avaliacao"),
                                            'unidade': fields.get("Unidade"),
                                            'rateio': fields.get("Rateio"),
                                            'contaContabil': fields.get("ContaContabil"),
                                            'historico': fields.get("Historico"),
                                            'Sindicancia': fields.get("Sindicancia")
                                        },
                                        success: function(response) {

                                            var result = Ext.util.JSON.decode(response.responseText);

                                            Ext.Msg.show({
                                                title: "Mensagem de Informação",
                                                msg: result.message,
                                                buttons: Ext.Msg.OK,
                                                minWidth: 200
                                            });

                                            grid.getStore().clearModified();
                                            grid.getStore().reload();
                                        },
                                        failure: function(response) {
                                            grid.getStore().clearModified();
                                            grid.getStore().reload();
                                        }
                                    });
                                }
                            } else {
                                grid.getStore().clearModified();
                                grid.getStore().reload();
                            }
                        })
                );
            }
        }

        function onSave() {
            var myMask = new Ext.LoadMask(Ext.getBody(), {msg:"Criando Processo(s) e Dossiê(s). Aguarde alguns instantes..."});
            myMask.show();

            window.jQuery.ajax({
                url: '<%= Url.Action("SalvarProcessosEmCache") %>',
                type: "POST",
                dataType: "json",
                success: function(data) {
                    myMask.hide();
                    var iconCad = window.Ext.MessageBox.ERROR;
                    if (data.sucesso)
                        iconCad = window.Ext.MessageBox.SUCCESS;
                    window.Ext.Msg.show({
                        title: "Salvar Laudos",
                        msg: data.message,
                        buttons: window.Ext.Msg.OK,
                        icon: iconCad,
                        minWidth: 200,
                        fn: function() {
                            if (data.sucesso) {
                                grid.getStore().reload();
                            }
                        },
                        close: function() {
                            if (data.sucesso) {
                                grid.getStore().reload();
                                myMask.hide();
                            }
                        }
                    });

                },
                error: function(jqXHR, textStatus) {
                    myMask.hide();
                    window.Ext.Msg.show({
                        title: "Erro no Servidor",
                        msg: 'Ocorreu um erro no servidor, por favor, tente novamente mais tarde.',
                        buttons: window.Ext.Msg.OK,
                        icon: window.Ext.MessageBox.ERROR,
                        minWidth: 200
                    });
                }
            });
        }

        function onAdd(btn, evt) {
            formModal = new Ext.Window({
                id: 'formModalAgrup',
                title: 'Novo Laudo',
                modal: true,
                width: 360,
                height: 530,
                resizable: true,               
                autoScroll: true,
                autoLoad: { url: '<%= Url.Action("Edit") %>', scripts: true },
                listeners: {
                    destroy: function() {
                        if (grid) {
                            grid.getStore().reload();
                        }
                    }
                }
            });
            window.wFormEdicao = formModal;
            formModal.show(this);
        }

        /* ------------------------------------------------------- */

        function tooltip(value, metaData) {
            metaData.attr = 'ext:qtip="' + value + '"';
            return value;
        }
        
        $(function() {
            var detalheAction = new Ext.ux.grid.RowActions({
                dataIndex: '',
                header: '',
                align: 'center',
                actions: [
                    {
                        iconCls: 'icon-del',
                        tooltip: 'Excluir'
                    }
                ],
                callbacks: {
                    'icon-del': function(grid, record, action, row, col) {
                        Ext.Ajax.request({
                            url: "<% = Url.Action("ExcluirProcessoSeguroCache") %>",
                            method: "POST",
                            params: { idLaudo: record.data.Id },
                            success: function(response) {

                                var result = Ext.util.JSON.decode(response.responseText);

                                if (result.sucesso) {
                                    grid.getStore().reload();
                                } else {
                                    Ext.Msg.show({
                                        title: "Mensagem de Informação",
                                        msg: result.message,
                                        buttons: Ext.Msg.OK,
                                        minWidth: 200
                                    });
                                }
                            },
                            failure: function(conn, data) {
                                Ext.Msg.show({
                                    title: "Mensagem de Erro",
                                    msg: "Erro na exclusão do laudo!</BR>" + result.Message,
                                    buttons: Ext.Msg.OK,
                                    icon: Ext.MessageBox.ERROR,
                                    minWidth: 200
                                });
                            }
                        });
                    }
                }
            });

            var dsTerminais = new Ext.data.JsonStore({
                autoLoad: true,
                url: '<%= Url.Action("ObterTerminais") %>',
                fields: [
                    'Terminal',
                    'TerminalLabel'
                ],
                listeners: {
                    load: function(store, records) {
                        var rt = store.recordType;
                        store.insert(0, new rt({}));
                    }
                }
            });
            window.dsTerminais = dsTerminais;

            var dsUnidades = new Ext.data.JsonStore({
                autoLoad: true,
                url: '<%= Url.Action("ObterUnidades") %>',
                fields: [
                    'Unidade',
                    'UnidadeLabel'
                ],
                listeners: {
                    load: function(store, records) {
                        var rt = store.recordType;
                        store.insert(0, new rt({}));
                    }
                }
            });
            window.dsUnidades = dsUnidades;

            var dsCausas = new Ext.data.JsonStore({
                autoLoad: true,
                url: '<%= Url.Action("ObterCausas") %>',
                fields: [
                    'Causa',
                    'CausaLabel'
                ],
                listeners: {
                    load: function(store, records) {
                        var rt = store.recordType;
                        store.insert(0, new rt({}));
                    }
                }
            });
            window.dsCausas = dsCausas;

            var dsGambits = new Ext.data.JsonStore({
                autoLoad: true,
                url: '<%= Url.Action("ObterGambits") %>',
                fields: [
                    'Gambit',
                    'GambitLabel'
                ],
                listeners: {
                    load: function(store, records) {
                        var rt = store.recordType;
                        store.insert(0, new rt({}));
                    }
                }
            });
            window.dsGambits = dsGambits;

            var dsLacres = new Ext.data.JsonStore({
                autoLoad: true,
                url: '<%= Url.Action("ObterLacres") %>',
                fields: [
                    'Lacre',
                    'LacreLabel'
                ],
                listeners: {
                    load: function(store, records) {
                        var rt = store.recordType;
                        store.insert(0, new rt({}));
                    }
                }
            });
            window.dsLacres = dsLacres;

            var dsAvaliacoes = new Ext.data.JsonStore({
                autoLoad: true,
                url: '<%= Url.Action("ObterAvaliacoes") %>',
                fields: [
                    'Avaliacao',
                    'AvaliacaoLabel'
                ],
                listeners: {
                    load: function(store, records) {
                        var rt = store.recordType;
                        store.insert(0, new rt({}));
                    }
                }
            });
            window.dsAvaliacoes = dsAvaliacoes;

            var dsContasContabeis = new Ext.data.JsonStore({
                autoLoad: true,
                url: '<%= Url.Action("ObterContasContabeis") %>',
                fields: [
                    'ContaContabil',
                    'ContaContabilLabel'
                ],
                listeners: {
                    load: function(store, records) {
                        var rt = store.recordType;
                        store.insert(0, new rt({}));
                    }
                }
            });
            window.dsContasContabeis = dsContasContabeis;

            grid = new Translogic.PaginatedEditorGrid({
                autoLoadGrid: false,
                region: 'center',
                url: '<%= Url.Action("ObterProcessosCacheUsuario") %>',
                viewConfig: {
                    forceFit: true
                },
                fields:
                [
                    'Id',
                    'Despacho',
                    'Serie',
                    'Nota',
                    'Cliente',
                    'Produto',
                    'Peso',
                    'Tipo',
                    'Vagao',
                    'Origem',
                    'Data',
                    'Laudo',
                    'ValorMercadoria',
                    'Terminal',
                    'Perda',
                    'Causa',
                    'Gambit',
                    'Lacre',
                    'Vistoriador',
                    'Avaliacao',
                    'Unidade',
                    'Rateio',
                    'ContaContabil',
                    'Historico',
                    'Sindicancia'
                ],
                columns: [
                    //new Ext.grid.RowNumberer(),
                    { Id: 'Id', hidden: true },
                    detalheAction,
                    {
                        header: 'Despacho',
                        dataIndex: "Despacho",
                        sortable: true,
                        renderer: tooltip,
                        editor: new fm.NumberField({
                            allowBlank: false,
                            allowDecimals: false,
                            listeners: {
                                change: function(field, e) {
                                    salvarAlteracoes();
                                }
                            }
                        })
                    },
                    {
                        header: 'Série',
                        dataIndex: "Serie",
                        sortable: true,
                        renderer: tooltip,
                        editor: new fm.TextField({
                            allowBlank: false,
                            listeners: {
                                change: function(field, e) {
                                    salvarAlteracoes();
                                }
                            }
                        })
                    },
                    { header: 'Nº Nota', dataIndex: "Nota", sortable: true, renderer: tooltip },
                    { header: 'Cliente', dataIndex: "Cliente", sortable: true, renderer: tooltip },
                    {   
                        header: 'Produto', 
                        dataIndex: "Produto", 
                        sortable: true, 
                        renderer: tooltip 
                    },
                    { header: 'Peso', dataIndex: "Peso", sortable: true, renderer: tooltip },
                    { header: 'Tipo', dataIndex: "Tipo", sortable: true, renderer: tooltip },
                    { header: 'Vagão', dataIndex: "Vagao", sortable: true, renderer: tooltip },
                    { header: 'Origem', dataIndex: "Origem", sortable: true, renderer: tooltip },
                    {
                        header: 'Data',
                        dataIndex: "Data",
                        sortable: true,
                        //renderer: Ext.util.Format.dateRenderer('d/m/Y'),
                        editor: new fm.DateField({
                            allowBlank: false,
                            listeners: {
                                change: function(field, e) {
                                    salvarAlteracoes();
                                }
                            }
                        })
                    },
                    {
                        header: 'Laudo',
                        dataIndex: "Laudo",
                        sortable: true,
                        renderer: tooltip,
                        editor: new fm.NumberField({
                            allowBlank: false,
                            allowDecimals: false,
                            listeners: {
                                change: function(field, e) {
                                    salvarAlteracoes();
                                }
                            }
                        })
                    },
                    {
                        header: 'Valor Mercadoria',
                        dataIndex: "ValorMercadoria",
                        sortable: true,
                        renderer: tooltip,
                        editor: new fm.NumberField({
                            allowBlank: false,
                            allowDecimals: true,
                            listeners: {
                                change: function(field, e) {
                                    salvarAlteracoes();
                                }
                            }
                        })
                    },
                    {
                        header: 'Terminal',
                        dataIndex: "Terminal",
                        sortable: true,
                        renderer: tooltip,
                        editor: new fm.ComboBox({
                            forceSelection: true,
                            store: window.dsTerminais,
                            triggerAction: 'all',
                            mode: 'remote',
                            typeAhead: true,
                            fieldLabel: 'Terminal',
                            displayField: 'TerminalLabel',
                            width: 200,
                            valueField: 'Terminal',
                            editable: false,
                            tpl: '<tpl for="."><div class="x-combo-list-item">{TerminalLabel}&nbsp;</div></tpl>',
                            allowBlank: false,
                            listeners: {
                                change: function(field, e) {
                                    salvarAlteracoes();
                                }
                            }
                        })
                    },
                    {
                        header: 'Perda',
                        dataIndex: "Perda",
                        sortable: true,
                        renderer: tooltip,
                        editor: new fm.NumberField({
                            allowBlank: false,
                            allowDecimals: true,
                            listeners: {
                                change: function(field, e) {
                                    salvarAlteracoes();
                                }
                            }
                        })
                    },
                    {
                        header: 'Causa',
                        dataIndex: "Causa",
                        sortable: true,
                        renderer: tooltip,
                        editor: new fm.ComboBox({
                            forceSelection: true,
                            store: window.dsCausas,
                            triggerAction: 'all',
                            mode: 'remote',
                            typeAhead: true,
                            fieldLabel: 'Causa',
                            displayField: 'CausaLabel',
                            width: 200,
                            valueField: 'Causa',
                            editable: false,
                            tpl: '<tpl for="."><div class="x-combo-list-item">{CausaLabel}&nbsp;</div></tpl>',
                            allowBlank: false,
                            listeners: {
                                change: function(field, e) {
                                    salvarAlteracoes();
                                }
                            }
                        })
                    },
                    {
                        header: 'Gambit',
                        dataIndex: "Gambit",
                        sortable: true,
                        renderer: tooltip,
                        editor: new fm.ComboBox({
                            forceSelection: true,
                            store: window.dsGambits,
                            triggerAction: 'all',
                            mode: 'remote',
                            typeAhead: true,
                            fieldLabel: 'Gambit',
                            displayField: 'GambitLabel',
                            width: 200,
                            valueField: 'Gambit',
                            editable: false,
                            tpl: '<tpl for="."><div class="x-combo-list-item">{GambitLabel}&nbsp;</div></tpl>',
                            allowBlank: false,
                            listeners: {
                                change: function(field, e) {
                                    salvarAlteracoes();
                                }
                            }
                        })
                    },
                    {
                        header: 'Lacre',
                        dataIndex: "Lacre",
                        sortable: true,
                        renderer: tooltip,
                        editor: new fm.ComboBox({
                            forceSelection: true,
                            store: window.dsLacres,
                            triggerAction: 'all',
                            mode: 'remote',
                            typeAhead: true,
                            fieldLabel: 'Lacre',
                            displayField: 'LacreLabel',
                            width: 200,
                            valueField: 'Lacre',
                            editable: false,
                            tpl: '<tpl for="."><div class="x-combo-list-item">{LacreLabel}&nbsp;</div></tpl>',
                            allowBlank: false,
                            listeners: {
                                change: function(field, e) {
                                    salvarAlteracoes();
                                }
                            }
                        })
                    },
                    {
                        header: 'Vistoriador',
                        dataIndex: "Vistoriador",
                        sortable: true,
                        renderer: tooltip,
                        editor: new fm.TextField({
                            allowBlank: false,
                            listeners: {
                                change: function(field, e) {
                                    salvarAlteracoes();
                                }
                            }
                        })
                    },
                    {
                        header: 'Avaliação',
                        dataIndex: "Avaliacao",
                        sortable: true,
                        renderer: tooltip,
                        editor: new fm.ComboBox({
                            forceSelection: true,
                            store: window.dsAvaliacoes,
                            triggerAction: 'all',
                            mode: 'remote',
                            typeAhead: true,
                            fieldLabel: 'Avaliacao',
                            displayField: 'AvaliacaoLabel',
                            width: 200,
                            valueField: 'Avaliacao',
                            editable: false,
                            tpl: '<tpl for="."><div class="x-combo-list-item">{AvaliacaoLabel}&nbsp;</div></tpl>',
                            allowBlank: false,
                            listeners: {
                                change: function(field, e) {
                                    salvarAlteracoes();
                                }
                            }
                        })
                    },
                    {
                        header: 'Unidade',
                        dataIndex: "Unidade",
                        sortable: true,
                        renderer: tooltip,
                        editor: new fm.ComboBox({
                            forceSelection: true,
                            store: window.dsUnidades,
                            triggerAction: 'all',
                            mode: 'remote',
                            typeAhead: true,
                            fieldLabel: 'Unidade',
                            displayField: 'UnidadeLabel',
                            width: 200,
                            valueField: 'Unidade',
                            editable: false,
                            tpl: '<tpl for="."><div class="x-combo-list-item">{UnidadeLabel}&nbsp;</div></tpl>',
                            allowBlank: false,
                            listeners: {
                                change: function(field, e) {
                                    salvarAlteracoes();
                                }
                            }
                        })
                    },
                    {
                        header: 'Rateio',
                        dataIndex: "Rateio",
                        sortable: true,
                        renderer: tooltip,
                        editor: new fm.NumberField({
                            allowBlank: false,
                            allowDecimals: true,
                            listeners: {
                                change: function(field, e) {
                                    salvarAlteracoes();
                                }
                            }
                        })
                    },
                    {
                        header: 'Conta Contábil',
                        dataIndex: "ContaContabil",
                        sortable: true,
                        renderer: tooltip,
                        editor: new fm.ComboBox({
                            forceSelection: true,
                            store: window.dsContasContabeis,
                            triggerAction: 'all',
                            mode: 'remote',
                            typeAhead: true,
                            fieldLabel: 'ContaContabil',
                            displayField: 'ContaContabilLabel',
                            width: 200,
                            valueField: 'ContaContabil',
                            editable: false,
                            tpl: '<tpl for="."><div class="x-combo-list-item">{ContaContabilLabel}&nbsp;</div></tpl>',
                            allowBlank: false,
                            listeners: {
                                change: function(field, e) {
                                    salvarAlteracoes();
                                }
                            }
                        })
                    },
                    {
                        header: 'Histórico',
                        dataIndex: "Historico",
                        sortable: true,
                        renderer: tooltip,
                        editor: new fm.TextField({
                            allowBlank: false,
                            listeners: {
                                change: function(field, e) {
                                    salvarAlteracoes();
                                }
                            }
                        })
                    },
                    {
                        header: 'Sindicância',
                        dataIndex: "Sindicancia",
                        sortable: true,
                        renderer: tooltip,
                        editor: new fm.TextField({
                            allowBlank: false,
                            listeners: {
                                change: function(field, e) {
                                    salvarAlteracoes();
                                }
                            }
                        })
                    }
                ],
                plugins: [detalheAction],
                filteringToolbar: [
                    {
                        text: 'Salvar',
                        tooltip: 'Salvar laudos',
                        iconCls: 'icon-save',
                        handler: onSave
                    },
                    {
                        text: 'Novo',
                        tooltip: 'Adicionar novo registro',
                        iconCls: 'icon-new',
                        handler: onAdd
                    }
                ]
            });

            viewPort = new Ext.Viewport({
                layout: 'border',
                margins: 10,
                items: [
                    {
                        region: 'north',
                        height: 60,
                        items: [
                            {
                                region: 'center',
                                applyTo: 'header-content'
                            }
                        ]
                    },
                    grid,
                    {
                        id: 'detailPanel',
                        region: 'south',
                        autoScroll: true,
                        bodyStyle: {
                            background: '#CCCCCC',
                            padding: '5px'
                        }
                    }
                ]
            });
            grid.getStore().load();
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div id="header-content">
        <h1>
            Seguro</h1>
        <small>Você está em Seguro > Laudo de Indenizações</small>
        <br />
        <center>
            <p style="font-size: 28px; font-weight: bold;">
                Laudos - Resumo Diário</p>
        </center>
    </div>
</asp:Content>
