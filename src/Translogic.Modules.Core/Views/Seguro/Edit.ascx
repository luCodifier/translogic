﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div id="dvSalvando" style="position: absolute; background-color: #fff; top:0; left:0; width: 340px; height: 510px; display: none; z-index: 9999;"><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><center>Aguarde&nbsp;&nbsp;<img src="<%=Url.Images("loading.gif")%>"/><br/><br/><br/>(Obs.: Isso pode levar alguns minutos)</center></div>
<div id="div-form-processo">
</div>
<% Html.RenderPartial("DespachoNotas"); %>

<style type="text/css">
    .x-btn-text.icon-detail {
        width: 70px !important;
    }
    
</style>

<script type="text/javascript">
    
    var formModalDespachoNotas = null;
    var windowDespachoNotas = null;    
    var loadingMask = new Ext.LoadMask(Ext.getBody(), { msg: "Buscando o valor da mercadoria. Aguarde alguns instantes..." });

    var txtDespacho = {
        xtype: 'numberfield',
        allowDecimals: false,
        id: 'txtDespacho',
        name: 'txtDespacho',
        typeAhead: true,
        fieldLabel: 'Despacho',
        width: 80,
        allowBlank: false,
        listeners: {
            change: function() {                
                //Ext.getCmp('txtValorMercadoria').setReadOnly(false);
                //Ext.getCmp("txtValorMercadoria").setValue('');
                //Ext.getCmp('txtValorMercadoria').setReadOnly(true);
            }
        }
    };


    var chkDespachoIntercambio =  {
		xtype: 'checkbox',
		fieldLabel: 'Intercâmbio',
		id: 'chkDespachoIntercambio',
		name: 'chkDespachoIntercambio',
		validateField: true,
		value: false
	}

    var txtSerie = {
        xtype: 'textfield',
        id: 'txtSerie',
        name: 'txtSerie',
        typeAhead: true,
        fieldLabel: 'Série',
        width: 80,
        enableKeyEvents: true,
        style: {
            textTransform: 'uppercase'
        },
        allowBlank: false,
        listeners: {
            change: function() {
                var dsTerminaisx = new Ext.data.JsonStore({
                    autoLoad: true,
                    url: '<%= Url.Action("ObterTerminaisAo") %>' + "?despacho=" + document.getElementById("txtDespacho").value + "&serie=" + document.getElementById("txtSerie").value + "&intercambio=" + Ext.getCmp("chkDespachoIntercambio").getValue(),
                    fields: [
                        'Terminal',
                        'TerminalLabel'
                    ]
                   /* ,listeners: {
                        load: function(store, records) {
                            // Popula a combo box Terminal com a lista de terminais
                            //formLaudo.items.items[7].store = window.dsTerminaisx;
                            
                        }
                    } */
                });
                //window.dsTerminaisx = dsTerminaisx;

                Ext.getCmp("cadTerminal").store = dsTerminaisx;
                //Ext.getCmp("cadTerminal").store.Load();

                // Preenche o valor do campo Valor da mercadoria
                var numeroDespacho = document.getElementById("txtDespacho").value;
                var serieDespacho = document.getElementById("txtSerie").value;
                var despachoIntercambio = Ext.getCmp("chkDespachoIntercambio").getValue();

                if ((numeroDespacho) && (serieDespacho)) {

                    BuscarValorMercadoriaUnitario(numeroDespacho, serieDespacho, despachoIntercambio);   
                }
            }
        }
    };
    
    var btnDespachoNotas = new Ext.Button({
        id: 'btnDespachoNotas',
        name: 'btnDespachoNotas',
        text: 'Ver Notas',
        width: 50,
        iconCls: 'icon-detail',
        handler: function (b, e) {
            CarregarTelaModalDespachoNotas();
        }
    });

    //Ext.getCmp('btnDespachoNotas').setDisabled(true);

    var dataSinistro = {
        xtype: 'datefield',
        fieldLabel: 'Data Sinistro',
        id: 'dataSinistro',
        name: 'dataSinistro',
        width: 83,
        hiddenName: 'dataSinistro',
        value: new Date(),
        allowBlank: false
    };

    var txtLaudo = {
        xtype: 'numberfield',
        allowDecimals: false,
        id: 'txtLaudo',
        name: 'txtLaudo',
        typeAhead: true,
        fieldLabel: 'Laudo',
        width: 80,
        allowBlank: false
    };

    var txtValorMercadoria = {
        xtype: 'numberfield',
        decimalPrecision: 2,
        allowDecimals: true,
        id: 'txtValorMercadoria',
        name: 'txtValorMercadoria',
        typeAhead: true,
        fieldLabel: 'Valor Mercadoria',
        width: 120,
        allowBlank: false,
        maxLength: 4,
        autoCreate: {tag: 'input', type: 'text', size: '120', autocomplete: 'off', maxlength: '4'}
    };

//    var txtValorMercadoria = {
//        xtype: 'textfield',
//        id: 'txtValorMercadoria',
//        name: 'txtValorMercadoria',
//        fieldLabel: 'Valor Mercadoria',
//        width: 120,
//        style: {
//            textTransform: 'uppercase'
//        },
//        allowBlank: false
//        //cls: 'x-item-disabled'
//    };    

    var cboTerminalCad = {
        xtype: 'combo',
        id: 'cadTerminal',
        name: 'cadTerminal',
        forceSelection: true,
        //store: window.dsTerminaisx,
        triggerAction: 'all',
        mode: 'remote',
        typeAhead: true,
        fieldLabel: 'Terminal',
        displayField: 'TerminalLabel',
        width: 200,
        valueField: 'Terminal',
        editable: false,
        tpl: '<tpl for="."><div class="x-combo-list-item">{TerminalLabel}&nbsp;</div></tpl>',
        allowBlank: false
    };

    var txtPerda = {
        xtype: 'numberfield',
        decimalPrecision: 8,
        allowDecimals: true,
        id: 'txtPerda',
        name: 'txtPerda',
        typeAhead: true,
        fieldLabel: 'Perda',
        //maxLength: 8,
        autoCreate: {tag: 'input', type: 'text', size: '80', autocomplete: 'off', maxlength: '8'},
        width: 80,
        allowBlank: false
    };

    var cboCausaCad = {
        xtype: 'combo',
        id: 'cadCausa',
        name: 'cadCausa',
        forceSelection: true,
        store: window.dsCausas,
        triggerAction: 'all',
        mode: 'remote',
        typeAhead: true,
        fieldLabel: 'Causa',
        displayField: 'CausaLabel',
        width: 200,
        valueField: 'Causa',
        editable: false,
        tpl: '<tpl for="."><div class="x-combo-list-item">{CausaLabel}&nbsp;</div></tpl>',
        allowBlank: false
    };

    var cboGambitCad = {
        xtype: 'combo',
        id: 'cadGambit',
        name: 'cadGambit',
        forceSelection: true,
        store: window.dsGambits,
        triggerAction: 'all',
        mode: 'remote',
        typeAhead: true,
        fieldLabel: 'Gambit',
        displayField: 'GambitLabel',
        width: 200,
        valueField: 'Gambit',
        editable: false,
        tpl: '<tpl for="."><div class="x-combo-list-item">{GambitLabel}&nbsp;</div></tpl>',
        allowBlank: false
    };

    var cboLacreCad = {
        xtype: 'combo',
        id: 'cadLacre',
        name: 'cadLacre',
        forceSelection: true,
        store: window.dsLacres,
        triggerAction: 'all',
        mode: 'remote',
        typeAhead: true,
        fieldLabel: 'Lacre',
        displayField: 'LacreLabel',
        width: 200,
        valueField: 'Lacre',
        editable: false,
        tpl: '<tpl for="."><div class="x-combo-list-item">{LacreLabel}&nbsp;</div></tpl>',
        allowBlank: false
    };

    var txtVistoriador = {
        xtype: 'textfield',
        id: 'txtVistoriador',
        name: 'txtVistoriador',
        typeAhead: true,
        fieldLabel: 'Vistoriador',
        width: 200,
        enableKeyEvents: true,
        style: {
            textTransform: 'uppercase'
        },
        allowBlank: false
    };

    var cboAvaliacaoCad = {
        xtype: 'combo',
        id: 'cadAvaliacao',
        name: 'cadAvaliacao',
        forceSelection: true,
        store: window.dsAvaliacoes,
        triggerAction: 'all',
        mode: 'remote',
        typeAhead: true,
        fieldLabel: 'Avaliação',
        displayField: 'AvaliacaoLabel',
        width: 200,
        valueField: 'Avaliacao',
        editable: false,
        tpl: '<tpl for="."><div class="x-combo-list-item">{AvaliacaoLabel}&nbsp;</div></tpl>',
        allowBlank: false
    };

    var cboUnidadeCad = {
        xtype: 'combo',
        id: 'cadUnidade',
        name: 'cadUnidade',
        forceSelection: true,
        store: window.dsUnidades,
        triggerAction: 'all',
        mode: 'remote',
        typeAhead: true,
        fieldLabel: 'Unidade',
        displayField: 'UnidadeLabel',
        width: 200,
        valueField: 'Unidade',
        editable: false,
        tpl: '<tpl for="."><div class="x-combo-list-item">{UnidadeLabel}&nbsp;</div></tpl>',
        allowBlank: false
    };

    var txtRateio = {
        xtype: 'numberfield',
        decimalPrecision: 8,
        allowDecimals: true,
        id: 'txtRateio',
        name: 'txtRateio',
        typeAhead: true,
        fieldLabel: 'Rateio',
        width: 80,
        allowBlank: false
    };

    var cboContaContabilCad = {
        xtype: 'combo',
        id: 'cadContaContabil',
        name: 'cadContaContabil',
        forceSelection: true,
        store: window.dsContasContabeis,
        triggerAction: 'all',
        mode: 'remote',
        typeAhead: true,
        fieldLabel: 'Conta Contábil',
        displayField: 'ContaContabilLabel',
        width: 200,
        valueField: 'ContaContabil',
        editable: false,
        tpl: '<tpl for="."><div class="x-combo-list-item">{ContaContabilLabel}&nbsp;</div></tpl>',
        allowBlank: false
    };

    var txtHistorico = {
        xtype: 'textfield',
        autoCreate: { tag: 'input', type: 'text', maxlength: '500', autocomplete: 'off' },
        id: 'txtHistorico',
        name: 'txtHistorico',
        typeAhead: true,
        fieldLabel: 'Histórico (máx:500)',
        width: 200,
        enableKeyEvents: true,
        style: {
            textTransform: 'uppercase'
        },
        allowBlank: true,
        listeners: {
            render: function(c) {
                Ext.QuickTips.register({
                    target: c,
                    text: 'Informe o histórico com no máximo 500 caracteres.'
                });
            }
        }
    };

     var txtSindicancia = {
        xtype: 'numberfield',
        allowDecimals: false,
        id: 'txtSindicancia',
        name: 'txtSindicancia',
        typeAhead: true,
        fieldLabel: 'Sindicância',
        width: 80,
        allowBlank: false,
        maxLength: 7,
        autoCreate: {tag: 'input', type: 'text', autocomplete: 'off', maxlength: '7'}
    };

    function CarregarTelaModalDespachoNotas() {
        formModalDespachoNotas = null;
        windowDespachoNotas = null;

        var numDespacho = document.getElementById("txtDespacho").value;
        var numSerie = document.getElementById("txtSerie").value;
        var despachoIntercambio = Ext.getCmp("chkDespachoIntercambio").getValue();

        windowDespachoNotas = new Ext.Window({
            name: 'windowDespachoNotas',
            id: 'windowDespachoNotas',
            title: "Despacho/Série - Notas fiscais",
            expandonShow: true,
            modal: true,
            width: 750,
            height: 520,
            closable: true,
            closeAction: 'hide',
                items: [painelDespachoNotas, gridDespachoNotas],
                listeners: {
                    'beforeshow': function() {
                        Ext.getCmp("txtDespachoNota").setValue(numDespacho);
                        Ext.getCmp("txtSerieNota").setValue(numSerie);
                        Ext.getCmp("chkDespachoIntercambioNota").setValue(false);
                        var despachoIntercambio = Ext.getCmp("chkDespachoIntercambio").getValue();
                        
                        if ((despachoIntercambio === 'True') || (despachoIntercambio == true)) {                            
                            Ext.getCmp("chkDespachoIntercambioNota").setValue(true);    
                        }

                    },
                    'hide': function() {
                    }
                }
            });

            LimparGridDespachoNotas();

            windowDespachoNotas.show();

            CarregarGridDespachoNotas();
    }

    function HabilitaCampos ()
    {
        Ext.getCmp("txtDespacho").enable(true)
        Ext.getCmp("chkDespachoIntercambio").enable(true)
        Ext.getCmp("txtSerie").enable(true)
        Ext.getCmp("btnDespachoNotas").enable(true)
        Ext.getCmp("dataSinistro").enable(true)
        Ext.getCmp("txtLaudo").enable(true)
        Ext.getCmp("txtValorMercadoria").enable(true)
        Ext.getCmp("cadTerminal").enable(true)
        Ext.getCmp("txtPerda").enable(true)
        Ext.getCmp("cadCausa").enable(true)
        Ext.getCmp("cadGambit").enable(true)
        Ext.getCmp("cadLacre").enable(true)
        Ext.getCmp("txtVistoriador").enable(true)
        Ext.getCmp("cadAvaliacao").enable(true)
        Ext.getCmp("cadUnidade").enable(true)
        Ext.getCmp("txtRateio").enable(true)
        Ext.getCmp("cadContaContabil").enable(true)
        Ext.getCmp("txtHistorico").enable(true)  
        Ext.getCmp("txtSindicancia").enable(true) 
        Ext.getCmp("btnOK").enable(true)
        Ext.getCmp("formModalAgrup").enable(true)     
    }

    function DesabilitaCampos ()
    {
        Ext.getCmp("txtDespacho").disable(true)
        Ext.getCmp("chkDespachoIntercambio").disable(true)
        Ext.getCmp("txtSerie").disable(true)
        Ext.getCmp("btnDespachoNotas").disable(true)
        Ext.getCmp("dataSinistro").disable(true)
        Ext.getCmp("txtLaudo").disable(true)
        Ext.getCmp("txtValorMercadoria").disable(true)
        Ext.getCmp("cadTerminal").disable(true)
        Ext.getCmp("txtPerda").disable(true)
        Ext.getCmp("cadCausa").disable(true)
        Ext.getCmp("cadGambit").disable(true)
        Ext.getCmp("cadLacre").disable(true)
        Ext.getCmp("txtVistoriador").disable(true)
        Ext.getCmp("cadAvaliacao").disable(true)
        Ext.getCmp("cadUnidade").disable(true)
        Ext.getCmp("txtRateio").disable(true)
        Ext.getCmp("cadContaContabil").disable(true)
        Ext.getCmp("txtHistorico").disable(true)
        Ext.getCmp("txtSindicancia").disable(true)
        Ext.getCmp("btnOK").disable(true)
        Ext.getCmp("formModalAgrup").disable(true)    
    }

     function BuscarValorMercadoriaUnitario(numeroDespacho, serieDespacho, despachoIntercambio) {
    
        DesabilitaCampos();
       
        loadingMask.show();
        //Ext.getCmp('txtValorMercadoria').setReadOnly(false);
        //Ext.getCmp("txtValorMercadoria").setValue('');

        Ext.Ajax.request(
        {
            url: '<%= Url.Action("ObterDespachoValorUnitarioMercadoria") %>',
            params: { 
                        numeroDespacho: numeroDespacho, 
                        serieDespacho: serieDespacho,
                        despachoIntercambio: despachoIntercambio },
            timeout: 9000000,
            failure: function(conn, response, options, eOpts) {


                loadingMask.hide();
                
                Ext.Msg.show({
                    title: 'Ops...',
                    msg: "Erro inesperado. Ocorreu uma falha ao buscar o valor da mercadoria.",
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.ERROR
                });
            },    
            success: function (response) {

                 loadingMask.hide();
                 HabilitaCampos();

                var result = Ext.decode(response.responseText);

                if (result.Success == false) {
                    
                    
                    Ext.Msg.show({
                        title: 'Ops...',
                        msg: result.Message,
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.ERROR
                    });
                    
                }
                else {
                    
                    // preenche o campo Valor Mercadoria
                    Ext.getCmp("txtValorMercadoria").setValue(result.ValorMercadoriaUnitario);

                }
            }
        });
    }

    var arrNumeroDespacho = {
        width: 330,
        layout: 'form',
        border: false,
        items: [txtDespacho, chkDespachoIntercambio]
    };

    var arrlinha1 = {
			    layout: 'column',
			    border: false,
			    items: [arrNumeroDespacho]
			};
    	
	var arrCampos = new Array();
	arrCampos.push(arrlinha1);


    var arrlinhaSerie = {
			    layout: 'form',
                width: 190,
			    border: false,
			    items: [txtSerie]
			};

    var arrBtnDespachoNotas = {
			    layout: 'form',
                width: 90,
			    border: false,
			    items: [btnDespachoNotas]
			};

    var arrlinhaSerieBotaoVerNotas = {
			    layout: 'column',
			    border: false,
			    items: [arrlinhaSerie, arrBtnDespachoNotas]
			};

            

    var formLaudo = new Ext.form.FormPanel
    ({
        id: 'formLaudo',
        //labelWidth: 80,
        //labelAlign: 'top',
        bodyStyle: 'padding: 10px',
        items:
        [
            arrCampos,
            arrlinhaSerieBotaoVerNotas,
            dataSinistro,
            txtLaudo,
            txtValorMercadoria,
            cboTerminalCad,
            txtPerda,
            cboCausaCad,
            cboGambitCad,
            cboLacreCad,
            txtVistoriador,
            cboAvaliacaoCad,
            cboUnidadeCad,
            txtRateio,
            cboContaContabilCad,
            txtHistorico,
            txtSindicancia
            //arrlinha1, //txtDespacho,
            /*txtDespacho,
            txtSerie,
            btnDespachoNotas,
            dataSinistro,
            txtLaudo,
            txtValorMercadoria,
            cboTerminalCad,
            txtPerda,
            cboCausaCad,
            cboGambitCad,
            cboLacreCad,
            txtVistoriador,
            cboAvaliacaoCad,
            cboUnidadeCad,
            txtRateio,
            cboContaContabilCad,
            txtHistorico*/
        ],
        buttonAlign: "center",
        buttons:
        [
            {
                id: 'btnOK',
                text: "OK",
                formBind:true,
                iconCls: 'icon-save',
                handler: function() {

                    document.getElementById("dvSalvando").style.display = "block";

                    var dataSinistro =
                        Ext.getCmp("dataSinistro").getValue()
                            ? new Array(Ext.getCmp("dataSinistro").getValue().format('d/m/Y') + " " + Ext.getCmp("dataSinistro").getValue().format('H:i:s'))
                            : '';
                    window.jQuery.ajax({
                        url: '<%= Url.Action("SalvarProcessoSeguroCache") %>',
                        type: "POST",
                        data: {
                            id: <%= Request["ID"] ?? "null" %>,
                            despacho: Ext.getCmp("txtDespacho").getValue(),
                            serie: Ext.getCmp("txtSerie").getValue(),
                            data: dataSinistro,
                            laudo: Ext.getCmp("txtLaudo").getValue(),
                            valorMercadoria: Ext.getCmp("txtValorMercadoria").getValue(),
                            terminal: Ext.getCmp("cadTerminal").getValue(),
                            perda: Ext.getCmp("txtPerda").getValue(),
                            causa: Ext.getCmp("cadCausa").getValue(),
                            gambit: Ext.getCmp("cadGambit").getValue(),
                            lacre: Ext.getCmp("cadLacre").getValue(),
                            vistoriador: Ext.getCmp("txtVistoriador").getValue(),
                            avaliacao: Ext.getCmp("cadAvaliacao").getValue(),
                            unidade: Ext.getCmp("cadUnidade").getValue(),
                            rateio: Ext.getCmp("txtRateio").getValue(),
                            contaContabil: Ext.getCmp("cadContaContabil").getValue(),
                            historico: Ext.getCmp("txtHistorico").getValue(),
                            despachoIntercambio: Ext.getCmp("chkDespachoIntercambio").getValue(),
                            sindicancia: Ext.getCmp("txtSindicancia").getValue()
                        },
                        dataType: "json",
                        success: function(data) {
                            var iconCad = window.Ext.MessageBox.ERROR;
             
                            if (data.sucesso)
                                iconCad = window.Ext.MessageBox.SUCCESS;
                            window.Ext.Msg.show({
                                title: "Salvar Laudo",
                                msg: data.message,
                                buttons: window.Ext.Msg.OK,
                                icon: iconCad,
                                minWidth: 200,
                                fn: function() {
                                    if (data.sucesso) {
                                        window.wFormEdicao.close();
                                    }
                                },
                                close: function() {
                                    if (data.sucesso) {
                                        window.wFormEdicao.close();
                                    }
                                }
                            });
                            document.getElementById("dvSalvando").style.display = "none";
                        },
                        error: function(jqXHR, textStatus) {
                            window.Ext.Msg.show({
                                title: "Erro no Servidor",
                                msg: 'Ocorreu um erro no servidor, por favor, tente novamente mais tarde.',
                                buttons: window.Ext.Msg.OK,
                                icon: window.Ext.MessageBox.ERROR,
                                minWidth: 200
                            });
                            document.getElementById("dvSalvando").style.display = "none";
                        }
                    });
                }
            }
        ]
    });


    /******************************* RENDER *******************************/
    Ext.onReady(function () {

        formLaudo.render("div-form-processo");
        //Ext.getCmp('txtValorMercadoria').setReadOnly(true);
    });

</script>