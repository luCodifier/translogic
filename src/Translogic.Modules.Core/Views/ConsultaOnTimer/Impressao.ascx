﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%@ Import Namespace="Translogic.Modules.Core.Controllers.Operacao" %>
<%@ Import Namespace="Translogic.Modules.Core.Interfaces.Trem.OnTime" %>
<%  

    var items = ViewData["items"] as IList<ConsultaOnTimerController.PartidaOT>;
    var ups = ViewData["ups"] as IList<UpPerformanceOnTime>;
    var indicadores = ViewData["indicadores"] as IndicadoresOnTime;
%>
<div>
    <table>
        <tr>
            <td colspan="2">
                <table style="width: 800px; font-family: Arial; font-size: 11px; border-style: solid;
                    border-width: 1px 1px 1px 1px;">
                    <tr>
                        <td>
                            Trem
                        </td>
                        <td>
                            Origem
                        </td>
                        <td>
                            Destino
                        </td>
                        <td>
                            Partida Prev.
                        </td>
                        <td>
                            Partida Real
                        </td>
                        <td>
                            Sit. Trem
                        </td>
                        <td>
                            Dt. Cadastro
                        </td>
                        <td>
                            Matrícula Cadastro
                        </td>
                        <td>
                            Partida Prev. Cadastro
                        </td>
                        <td>
                            Dt. Alteração
                        </td>
                        <td>
                            Matrícula Alteração
                        </td>
                        <td>
                            Partida Prev. Alteração
                        </td>
                        <td>
                            Atraso Partida
                        </td>
                    </tr>
                    <% foreach (ConsultaOnTimerController.PartidaOT partida in items)
           {
            
                    %>
                    <tr>
                        <td>
                            <%=partida.Trem %>
                        </td>
                        <td>
                            <%=partida.EstacaoOrigem %>
                        </td>
                        <td>
                            <%=partida.EstacaoDestino %>
                        </td>
                        <td>
                            <%=partida.PartidaPrevista %>
                        </td>
                        <td>
                            <%=partida.PartidaRealizada %>
                        </td>
                        <td>
                            <%=partida.Situacao %>
                        </td>
                        <td>
                            <%=partida.DtCadastro %>
                        </td>
                        <td>
                            <%=partida.MatriculaCadastro %>
                        </td>
                        <td>
                            <%=partida.DtPartidaCadastro %>
                        </td>
                        <td>
                            <%=partida.DtPartidaUltimaAlteracao %>
                        </td>
                        <td>
                            <%=partida.MatriculaUltimaAlteracao %>
                        </td>
                        <td>
                            <%=partida.DtPartidaUltimaAlteracao %>
                        </td>
                        <td>
                            <%=partida.AtrasoPartida %>
                        </td>
                    </tr>
                    <%  } %>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table style="width: 350px; font-family: Arial; font-size: 11px; border-style: solid;
                    border-width: 1px 1px 1px 1px;">
                    <tr>
                        <td>
                            Trens encontrados dentro do Filtro Especificado
                        </td>
                        <td>
                            <%=indicadores.TotalTrem %>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Trens Encerrrados
                        </td>
                        <td>
                            <%=indicadores.TremEncerrados%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Trens parados/circulando
                        </td>
                        <td>
                            <%=indicadores.TremParados %>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Trens que tiveram Partida
                        </td>
                        <td>
                            <%=indicadores.TremPartida %>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            ...com OS Regular
                        </td>
                        <td>
                            <%=indicadores.OsRegular %>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            ...com OS Irregular
                        </td>
                        <td>
                            <%=indicadores.OsIrregular %>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            ...com Atraso na Partida
                        </td>
                        <td>
                            <%=indicadores.TremPartidaAtraso %>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            ...com OS Regular
                        </td>
                        <td>
                            <%=indicadores.TremPartidaAtrasoOsRegular%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            ...com OS Irregular
                        </td>
                        <td>
                            <%=indicadores.TremPartidaAtrasoOsIrregular%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Trens que tiveram Chegada
                        </td>
                        <td>
                            <%=indicadores.TremChegada%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            .... com Chegada no Horário
                        </td>
                        <td>
                            <%=indicadores.TremChegadaHorario%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            .... com Atraso na Chegada
                        </td>
                        <td>
                            <%=indicadores.TremChegadaAtraso%>
                        </td>
                    </tr>
                       <tr>
                        <td>
                            Trens Suprimidos
                        </td>
                        <td>
                            <%=indicadores.TrensSuprimidos433%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Trens Cancelados
                        </td>
                        <td>
                            <%=indicadores.TrensCancelador371%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Horas de Multa
                        </td>
                        <td>
                            <%=indicadores.HorasDeMulta.ToString("00")%>:00
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Atraso OnTime
                        </td>
                        <td>
                            <%=indicadores.AtrasoOnTime%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Atraso OntTimeTotal
                        </td>
                        <td>
                            <%=indicadores.AtrasoOnTimeTotal%>
                        </td>
                    </tr>
                </table>
            </td>
            <td valign="top">
                <table style="width: 443px; font-family: Arial; font-size: 11px; border-style: solid;
                    border-width: 1px 1px 1px 1px;">
                    <tr>
                        <td>
                            Unid. Prod.
                        </td>
                        <td>
                            Hora de Partida
                        </td>
                        <td>
                            %
                        </td>
                        <td>
                            Hora de Chegada
                        </td>
                        <td>
                            %
                        </td>
                    </tr>
                    <% foreach (UpPerformanceOnTime up in ups)
                       {  
                    %>
                    <tr>
                        <td>
                            <%=up.UP %>
                        </td>
                        <td>
                            <%=up.PartidaOrigem %>
                        </td>
                        <td>
                            <%=up.PercOrigem %>
                        </td>
                        <td>
                            <%=up.ChegadaDestino %>
                        </td>
                        <td>
                            <%=up.PercDestino %>
                        </td>
                    </tr>
                    <%  } %>
                </table>
            </td>
        </tr>
    </table>
</div>
