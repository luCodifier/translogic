﻿<%@ Import Namespace="System.Security.Policy" %>

<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Interna.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content runat="server" ID="HeadContent" ContentPlaceHolderID="HeadContent">
    <style type="text/css">
        .x-grid3-cell-first .x-grid3-cell-inner
        {
            padding-left: 0px !important;
        }
        .act-displ-hide
        {
            visibility: hidden;
        }
    </style>
    <script type="text/javascript">

        /******************************* STORE *******************************/
        var winModalConfig;
        var IdOSEdicao;
        
        function initModalCancelamentoVirtual()
        {
            var arrlinha0Config = {
                layout: 'column',
                border: false,
                items: [{
                    width: 320,
                    layout: 'form', 
                    border: false,
                    items: [{
                        id: 'lblTremMdl',
                        xtype: 'label',
                        style: 'font-weight:bold',
                        fieldLabel: 'Trem',
                        width: 300
                    }]
                }, {
                    width: 320,
                    layout: 'form', 
                    border: false,
                    items: [{
                        id: 'lblOSMdl',
                        xtype: 'label',
                        style: 'font-weight:bold',
                        fieldLabel: 'OS nº.',
                        width: 300
                    }]
                }, {
                    id:'rbgAcaoMdl',
                    width: 320,
                    xtype: 'radiogroup',
                    fieldLabel: 'Ação',
                    itemCls: 'x-check-group-alt',
                    columns: 1,
                    items: [{
                        boxLabel: 'Cancelar', 
                        name: 'rb-acao', 
                        value: 'cancelar-virtual'
                    }, {
                        boxLabel: 'Validar Partida TL: <label id="lblDataPartida" style="font-weight:bold">01/04/2014 00:50:32</label>', 
                        name: 'rb-acao',
                        value: 'validar-data-tl'
                    }]
                }]
            };

            winModalConfig = new Ext.Window({
                layout: 'fit',
                width: 300,
                height: 240,
                closeAction: 'hide',
                plain: true, modal: true,
                title: 'Informar Ação Manualmente',
                items: [
                    new window.Ext.form.FormPanel({
                        id: 'grid-modal-config',
                        region: 'north',
                        bodyStyle: 'padding: 15px',
                        labelAlign: 'top',
                        items: [arrlinha0Config]
                    })
                ],
                buttons: [
                    {
                        iconCls: 'icon-accept',
                        text: 'Salvar',
                        handler: function () {
                            var urlAcao = undefined;
                            var tmlRBL = Ext.getCmp('rbgAcaoMdl');
                            if(tmlRBL.getValue() == null) {
                                window.Ext.Msg.show({
                                    title: "Ação Não Selecionada",
                                    msg: 'Você não selecionou uma ação para ser executada, selecione e tente novamente.',
                                    buttons: window.Ext.Msg.OK,
                                    icon: window.Ext.MessageBox.WARNING,
                                    minWidth: 200
                                });
                                return;
                            }
                            else if(tmlRBL.getValue().value == 'cancelar-virtual') {
                                urlAcao = '<%= Url.Action("CancelarOSOnTime") %>';
                            }
                            else if(tmlRBL.getValue().value == 'validar-data-tl') {
                                urlAcao = '<%= Url.Action("ConsiderarDataTL") %>';
                            }
                            
                            window.jQuery.ajax({
                                url: urlAcao,
                                type: "POST",
                                data: jQuery.toJSON({idOS: IdOSEdicao}),
                                dataType: "json",
                                contentType: "application/json; charset=utf-8",
                                success: function (data) {
                                    var iconCad = window.Ext.MessageBox.ERROR;
                                    if (data.sucesso)
                                        iconCad = window.Ext.MessageBox.SUCCESS;

                                    window.Ext.Msg.show({
                                        title: "Salvar Alteração de Registro OnTime",
                                        msg: data.message,
                                        buttons: window.Ext.Msg.OK,
                                        icon: iconCad,
                                        minWidth: 350,
                                        fn: function () {
                                            winModalConfig.hide();
                                            Pesquisar();
                                        },
                                        close: function () {
                                            winModalConfig.hide();
                                            Pesquisar();
                                        }
                                    });

                                },
                                error: function (jqXHR, textStatus) {
                                    window.Ext.Msg.show({
                                        title: "Erro no Servidor",
                                        msg: 'Ocorreu um erro no servidor, por favor, tente novamente mais tarde.',
                                        buttons: window.Ext.Msg.OK,
                                        icon: window.Ext.MessageBox.ERROR,
                                        minWidth: 200
                                    });
                                }
                            });
                        }
                    },
                    {
                        iconCls: 'icon-cancel',
                        text: 'Cancelar',
                        handler: function () {
                            winModalConfig.hide();
                        }
                    }
                ]
            });
        }
        initModalCancelamentoVirtual();
        var storeTrem = new Ext.data.JsonStore({
            root: "Items",
            autoLoad: false,
            sortInfo: { field: "PartidaPrevista", direction: "ASC" },
            fields: [
                'Id',
                'NroOs',
                'Trem',
                'EstacaoOrigem',
                'EstacaoDestino', 
                'PartidaPrevista',
                'PartidaRealizada',
                'AtrasoPartida',
                'ChegadaPrevista',
                'ChegadaRealizada',
                'AtrasoChegada',
                'Situacao',
                'DtCadastro',
                'DtProg',
                'DtPartidaCadastro',
                'MatriculaCadastro',
                'DtPartidaUltimaAlteracao',
                'MatriculaUltimaAlteracao',
                'DtUltimaAlteracao', 
                'DtPartidaTL', 
                {
                    name: 'StatusAlteracoes',
                    convert: function (v, rec) {
                        if(rec.AlteradoTranslogic)
                            return 2;
                        if(rec.PermitirAlteracaoOnTime)
                            return 1;
                        return 0;
                    }
                },
                {
                    name: 'ExibirAlteradoTranslogic',
                    convert: function (v, rec) {
                        return !rec.AlteradoTranslogic;
                    }
                },
                {
                    name: 'ExibirPartidaTL',
                    convert : function (v, rec) {
                        return !rec.PermitirAlteracaoOnTime;
                    }
                }
            ]
        });
        
        var storePerformance = new Ext.data.JsonStore({
            root: "Performance",
            autoLoad: false,
            fields: [
                'UP',
                'PartidaOrigem',
                'PercOrigem',
                'ChegadaDestino',
                'PercDestino'
            ]
        });

        var dsUpOrigem = new window.Ext.data.JsonStore({
            id: 'dsUpOrigem',
            name: 'dsUpOrigem',
            root: 'Items',
            url: '<%= Url.Action("ObterUnidadeProducao") %>',
            fields: ['Id', 'DescricaoResumida']
        });

        var dsUpDestino = new window.Ext.data.JsonStore({
            id: 'dsUpDestino',  
            name: 'dsUpDestino',
            root: 'Items',
            url: '<%= Url.Action("ObterUnidadeProducao") %>',
            fields: ['Id', 'DescricaoResumida']
        });

        var cbOrigem = {
            xtype: 'combo',
            id: 'cbOrigem',
            name: 'cbOrigem',
            forceSelection: true,
            store: dsUpOrigem,
            triggerAction: 'all',
            mode: 'remote',
            typeAhead: true,
            displayField: 'DescricaoResumida',
            valueField: 'Id',
            fieldLabel: 'UP Origem',
            width: 130,
            minChars: 1
        };

        var arrUpOrigem = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding-right:10px',
            items: [cbOrigem]
        };

        var cbDestino = {
            xtype: 'combo',
            id: 'cbDestino',
            name: 'cbDestino',
            forceSelection: true,
            store: dsUpDestino,
            triggerAction: 'all',
            mode: 'remote',
            typeAhead: true,
            displayField: 'DescricaoResumida',
            valueField: 'Id',
            fieldLabel: 'UP Destino',
            width: 130,
            minChars: 1
        };

        var arrUpDestino = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding-right:10px',
            items: [cbDestino]
        };

        var EstacaoOrigem = {
            xtype: 'textfield',
            vtype: 'cteestacaovtype',
            style: 'text-transform: uppercase',
            id: 'EstacaoOrigem',
            fieldLabel: 'Est. Origem',
            autoCreate: { tag: 'input', type: 'text', maxlength: '3', autocomplete: 'off' },
            name: 'EstacaoOrigem',
            allowBlank: true,
            maxLength: 3,
            width: 60,
            hiddenName: 'EstacaoOrigem'
        };

        var arrEstacaoOrigem = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding-right:10px',
            items: [EstacaoOrigem]
        };

        var EstacaoDestino = {
            xtype: 'textfield',
            vtype: 'cteestacaovtype',
            style: 'text-transform: uppercase',
            id: 'EstacaoDestino',
            fieldLabel: 'Est. Destino',
            autoCreate: { tag: 'input', type: 'text', maxlength: '3', autocomplete: 'off' },
            name: 'EstacaoDestino',
            allowBlank: true,
            maxLength: 3,
            width: 60,
            hiddenName: 'EstacaoDestino'
        };

        var arrEstacaoDestino = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding-right:10px',
            items: [EstacaoDestino]
        };

        var Prefixo = {
            xtype: 'textfield',
            style: 'text-transform: uppercase',
            autoCreate: { tag: 'input', type: 'text', maxlength: '3', autocomplete: 'off' },
            maskRe: /[a-zA-Z0-9]/,
            width: 40,
            name: 'Prefixo',
            allowBlank: true,
            id: 'Prefixo',
            fieldLabel: 'Prefixo',
            enableKeyEvents: true
        };

        var arrPrefixo = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding-right:10px',
            items: [Prefixo]
        };


        var linha1 = {
            layout: 'column',
            border: false,
            items: [arrUpOrigem, arrUpDestino, arrEstacaoOrigem, arrEstacaoDestino, arrPrefixo]
        };

        var dataAtual = new Date();

        var dataPartidaPlanejadaInicial = {
            xtype: 'datefield',
            fieldLabel: 'De',
            id: 'dataPartidaPlanejadaInicial',
            name: 'dataInicial',
            width: 83,
            allowBlank: false,
            vtype: 'daterange',
            endDateField: 'dataPartidaPlanejadaFinal',
            hiddenName: 'dataPartidaPlanejadaInicial',
            value: dataAtual
        };

        var arrdataPartidaPlanejadaInicial = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding-right:10px',
            items: [dataPartidaPlanejadaInicial]
        };

        var dataPartidaPlanejadaFinal = {
            xtype: 'datefield',
            fieldLabel: 'Para',
            id: 'dataPartidaPlanejadaFinal',
            name: 'dataPartidaPlanejadaFinal',
            width: 83,
            allowBlank: false,
            vtype: 'daterange',
            startDateField: 'dataPartidaPlanejadaInicial',
            hiddenName: 'dataPartidaPlanejadaFinal',
            value: dataAtual
        };

        var arrdataPartidaPlanejadaFinal = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding-right:10px',
            items: [dataPartidaPlanejadaFinal]
        };

        var linha2_1 = {
            layout: 'column',
            border: false,
            items: [arrdataPartidaPlanejadaInicial, arrdataPartidaPlanejadaFinal]
        };

        var fieldPartidaPlanejada = {
            xtype: 'fieldset',
            title: 'Partida Planejada', // title, header, or checkboxToggle creates fieldset header
            autoHeight: true,
            columnWidth: 0.5,
            collapsed: false, // fieldset initially collapsed
            layout: 'anchor',
            items: [linha2_1]
        };

        var dataChegadaPlanejadaInicial = {
            xtype: 'datefield',
            fieldLabel: 'De',
            id: 'dataChegadaPlanejadaInicial',
            name: 'dataInicial',
            width: 83,
            allowBlank: false,
            vtype: 'daterange',
            endDateField: 'dataChegadaPlanejadaFinal',
            hiddenName: 'dataChegadaPlanejadaInicial'
        };

        var arrdataChegadaPlanejadaInicial = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding-right:10px',
            items: [dataChegadaPlanejadaInicial]
        };

        var dataChegadaPlanejadaFinal = {
            xtype: 'datefield',
            fieldLabel: 'Para',
            id: 'dataChegadaPlanejadaFinal',
            name: 'dataChegadaPlanejadaFinal',
            width: 83,
            allowBlank: false,
            vtype: 'daterange',
            startDateField: 'dataChegadaPlanejadaInicial',
            hiddenName: 'dataChegadaPlanejadaFinal'
        };

        var arrdataChegadaPlanejadaFinal = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding-right:10px',
            items: [dataChegadaPlanejadaFinal]
        };

        var linha2_2 = {
            layout: 'column',
            border: false,
            bodyStyle: 'padding-right:10px',
            items: [arrdataChegadaPlanejadaInicial, arrdataChegadaPlanejadaFinal]
        };

        var fieldPartidaPlanejada = {
            xtype: 'fieldset',
            title: 'Partida Planejada', // title, header, or checkboxToggle creates fieldset header
            autoHeight: true,
            bodyStyle: 'padding-left:10px',
            width: 255,
            collapsed: false, // fieldset initially collapsed
            items: [linha2_1]
        };

        var fieldChegadaPlanejada = {
            xtype: 'fieldset',
            title: 'Chegada Planejada', // title, header, or checkboxToggle creates fieldset header
            autoHeight: true,
            width: 255,
            collapsed: false, // fieldset initially collapsed
            bodyStyle: 'padding-left:10px',
            items: [linha2_2]
        };

        var linha2 = {
            layout: 'column',
            border: false,
            items: [fieldPartidaPlanejada, fieldChegadaPlanejada]
        };

        var PrefixoIncluir = {
            xtype: 'textfield',
            style: 'text-transform: uppercase',
            width: 230,
            name: 'PrefixoIncluir',
            allowBlank: true,
            id: 'PrefixoIncluir',
            fieldLabel: 'Prefixos a Incluir(separe por vírgula)',
            enableKeyEvents: true
        };

        var arrPrefixoIncluir = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding-right:10px;padding-top:10px',
            items: [PrefixoIncluir]
        };

        var PrefixoExcluir = {
            xtype: 'textfield',
            style: 'text-transform: uppercase',
            width: 230,
            name: 'PrefixoExcluir',
            allowBlank: true,
            id: 'PrefixoExcluir',
            fieldLabel: 'Prefixos a Excluir(separe por vírgula)',
            enableKeyEvents: true,
            value: 'N,E,W,S,V,A'
        };

        var arrPrefixoExcluir = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding-right:10px;padding-top:10px',
            items: [PrefixoExcluir]
        };

        var linha3 = {
            layout: 'column',
            border: false,
            items: [arrPrefixoIncluir, arrPrefixoExcluir]
        };

        var btnPesquisar = {
            xtype: 'button',
            name: 'btnEstacaoPesquisar',
            id: 'btnEstacaoPesquisar',
            text: 'Pesquisar',
            iconCls: 'icon-find',
            handler: function (b, e) {
                Pesquisar();
            }
        };

        var btnImprimir = {
            xtype: 'button',
            name: 'btnImprimir',
            id: 'btnImprimir',
            text: 'Imprimir',
            iconCls: 'icon-find',
            handler: function (b, e) {
                if (grid.getStore().getCount() > 0) {
                    Imprimir();
                } else {
                    ExibirConsistencia("Nenhum registro encontrado para impressão!");
                }
            }
        };

        var btnLimpar = {
            xtype: 'button',
            name: 'btnEstacaoLimpar',
            id: 'btnEstacaoLimpar',
            text: 'Limpar',
            handler: function (b, e) {

                Limpar();
            }
        };
        
        var btnExportarExcel = {
            xtype: 'button',
            name: 'btnExportarExcel',
            id: 'btnExportarExcel',
            text: 'Gerar Excel',
            iconCls: 'icon-page-excel',
            handler: GerarExcel
        };

        filtros = new Ext.form.FormPanel({
            id: 'filtros',
            name: 'filtros',
            bodyStyle: 'padding: 10px',
            height: 250,
            width: 600,
            labelAlign: 'top',
            items: [linha1, linha2, linha3],
            buttonAlign: 'center',
            buttons: [btnImprimir, btnPesquisar, btnExportarExcel, btnLimpar]
        });


        var groupCols = new Ext.ux.plugins.GroupHeaderGrid({
            rows: [[
                {header: '&nbsp;', colspan: 8, align: 'center'},
                {header: 'Cadastro de OS', colspan: 3, align: 'center'},
                {header: 'Alteração de OS', colspan: 3, align: 'center'},
                {}
            ]]
        });
        
        var actGridOnTime = new window.Ext.ux.grid.RowActions({
            dataIndex: 'StatusAlteracoes',
            sortable: true,
            autoWidth: true,
            header: '',
            width: 100,
            hideMode: 'display',
            align: 'center',
            actions: [{
                iconCls: 'icon-edit',
                tooltip: 'Editar',
                hideIndex: 'ExibirPartidaTL'
            }, {
                iconCls: 'icon-accept',
                tooltip: 'Registro editado na tela 1319',
                hideIndex: 'ExibirAlteradoTranslogic'
            }],
            callbacks: {
                'icon-edit': function (grid, record, action, row, col)
                {
                    Ext.getCmp('lblTremMdl').setText(record.json.Trem);
                    Ext.getCmp('lblOSMdl').setText(record.json.NroOs);
                    IdOSEdicao = record.json.Id;
                    Ext.getCmp('rbgAcaoMdl').reset();
                    winModalConfig.show();
                    jQuery('#lblDataPartida').text(record.json.DtPartidaTL);
                }
            }
        });

        var grid = new Ext.grid.GridPanel({
            id: 'grid',
            name: 'grid',
            autoLoadGrid: false,
            height: 300,
            width: 1250,
            stripeRows: true,
            region: 'center',
            store: storeTrem,
            enableHdMenu: false,
            viewConfig: {
                forceFit: true
            },
            loadMask: { msg: App.Resources.Web.Carregando },
            colModel: new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true
                },
                columns: [
                    actGridOnTime,
                    { header: 'Trem', width: 60, dataIndex: "Trem", sortable: true },
                    { header: 'Os', width: 60, dataIndex: "NroOs", sortable: true },
                    { header: 'Origem', width: 60, dataIndex: "EstacaoOrigem", sortable: true },
                    { header: 'Destino', width: 60, dataIndex: "EstacaoDestino", sortable: true },
                    { header: 'Partida Prev.', width: 120, dataIndex: "PartidaPrevista", sortable: true },
                    { header: 'Partida Real.', width: 120, dataIndex: "PartidaRealizada", sortable: true },
                    { header: 'Sit. Trem', width: 70, dataIndex: "Situacao", sortable: true },
                    /*Cadastro OS*/
                    { header: 'Data/Hora', width: 120, dataIndex: "DtCadastro", sortable: true },
                    { header: 'Matrícula', width: 90, dataIndex: "MatriculaCadastro", sortable: true },
                    { header: 'Partida Prevista', width: 120, dataIndex: "DtPartidaCadastro", sortable: true },
                    /*Alteração OS*/
                    { header: 'Data/Hora', width: 120, dataIndex: "DtUltimaAlteracao", sortable: true },
                    { header: 'Matrícula', width: 90, dataIndex: "MatriculaUltimaAlteracao", sortable: true },
                    { header: 'Partida Prevista', width: 120, dataIndex: "DtPartidaUltimaAlteracao", sortable: true },
                    
                    /*Atraso Partida*/
                    { header: 'Atraso', width: 80, dataIndex: "AtrasoPartida", sortable: true }
             ]
            }),
            plugins: [actGridOnTime, groupCols]
        });

        /**************************************************************/
        // SUMARIO DADOS TREM
        /**************************************************************/
        var arrSumario = new Array();

        function AddLabelSumario(idBase, label) {
            var cmpLabel = {
                layout: 'form',
                border: false,
                width: 300,
                bodyStyle: 'padding-top:4px',
                items: [{
                    xtype: 'label',
                    id: idBase,
                    text: label
                }]
            };

            var cmpLabelValue = {
                layout: 'form',
                border: false,
                bodyStyle: 'padding-top:4px;',
                items: [{
                    xtype: 'label',
                    id: idBase + 'Value',
                    text: ''
                }]
            };

            var summaryRow = {
                layout: 'column',
                border: false,
                items: [cmpLabel, cmpLabelValue]
            };

            arrSumario.push(summaryRow);
        }

        AddLabelSumario('totalTrens', 'Trens encontrados dentro do Filtro Especificado');
        AddLabelSumario('TrensEncerrados', 'Trens Encerrrados');
        AddLabelSumario('TrensParadosCirculando', 'Trens parados/circulando');
        
        AddLabelSumario('TrensPartidas', 'Trens que tiveram Partida');
        AddLabelSumario('TrensPartidasOsIrregular', '...com OS Regular');
        AddLabelSumario('TrensPartidasOsReg', '...com OS Irregular');
        
        AddLabelSumario('TrensAtrasoPartida', '...com Atraso na Partida');
        AddLabelSumario('TremPartidaAtrasoOsRegular', '...com OS Regular');
        AddLabelSumario('TremPartidaAtrasoOsIrregular', '...com OS Irregular');
        
        AddLabelSumario('TrensTiveramChegada', 'Trens que tiveram Chegada');
        AddLabelSumario('TrensChegadaHorario', '.... com Chegada no Horário');
        AddLabelSumario('TrensAtrasoChegada', '.... com Atraso na Chegada');
        
        AddLabelSumario('TrensSuprimidos', 'Trens Suprimidos');
        AddLabelSumario('TrensCancelados', 'Trens Cancelados');
        
        AddLabelSumario('HorasDeMulta', 'Horas de Multa');
        
        AddLabelSumario('AtrasoOnTime', 'Atraso OnTime');
        AddLabelSumario('AtrasoOnTimeTotal', 'Atraso OnTime (Total)');
        
        var fieldSumario = {
            xtype: 'fieldset',
            title: 'Sumário',
            autoHeight: true,
            width: 450,
            // height: 500,
            collapsible: true,
            bodyStyle: 'padding-left:10px',
            items: [arrSumario]
        };

        /**************************************************************/
                // SUMARIO ON TIME PERFORMANCE
        /**************************************************************/

        var arrOnTimeSumario = new Array();

        var gridOnTime = new Ext.grid.GridPanel({
            id: 'gridOnTime',
            name: 'gridOnTime',
            autoLoadGrid: false,
            height: 290,
             width: 360,
            stripeRows: true,
            region: 'center',
            store: storePerformance,
            loadMask: { msg: App.Resources.Web.Carregando },
            colModel: new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true
                },
                columns: [
                    { header: 'Unid. Prod.', width: 120, dataIndex: "UP", sortable: true },
                    { header: 'Partida', width: 55, dataIndex: "PartidaOrigem", sortable: true },
                    { header: '%', width: 50, dataIndex: "PercOrigem", sortable: true },
                    { header: 'Chegada', width: 55, dataIndex: "ChegadaDestino", sortable: true },
                    { header: '%', width: 50, dataIndex: "PercDestino", sortable: true }
             ]
            })
        });

        var arrGridOnTime = {
            layout: 'form',
            border: false,
            width: 355,
            bodyStyle: 'padding-top:4px',
            items: [gridOnTime]
        };

        var SumarioOnTimeLinha1 = {
            layout: 'column',
            border: false,
            items: [arrGridOnTime]
        };

        arrOnTimeSumario.push(SumarioOnTimeLinha1);


        var fieldOnTimeSumario = {
            xtype: 'fieldset',
            title: 'On Time Performance',
            autoHeight: true,
            width: 390,
            collapsible: true,
            bodyStyle: 'padding-left:10px',
            items: [arrOnTimeSumario]
        };

        /**************************************************************/
        // DADOS DO RODAPE
        /**************************************************************/

        var rodapeLinha1 = {
            layout: 'column',
            border: false,
            items: [fieldSumario, fieldOnTimeSumario]
        };
        
        rodapePanel = new Ext.form.FormPanel({
            id: 'rodapePanel',
            name: 'rodapePanel',
            bodyStyle: 'padding: 10px',
            // height: 300,
            labelAlign: 'top',
            width: jQuery(window).width(),
            items: [rodapeLinha1]
        });

        
        /**************************************************************/
        // MÉTODOS       
        /**************************************************************/
        
        function Imprimir() {
           var upOrigem = null;
            var upDestino = null;
            var estOrigem = null;
            var estDestino = null;
            var prefixo = null;
            var partidaPlanejadaInicial = null;
            var partidaPlanejadaFinal = null;
            var chegadaPlanejadaInicial = null;
            var chegadaPlanejadaFinal = null;
            var prefixosIncluir = null;
            var prefixosExcluir = null;

            upOrigem = Ext.getCmp('cbOrigem').getRawValue();
            upDestino = Ext.getCmp('cbDestino').getRawValue();
            estOrigem = Ext.getCmp('EstacaoOrigem').getValue();
            estDestino = Ext.getCmp('EstacaoDestino').getValue();
            prefixo = Ext.getCmp('Prefixo').getValue();

            if (Ext.getCmp("dataPartidaPlanejadaInicial").getValue() != "" && Ext.getCmp("dataPartidaPlanejadaFinal").getValue() != "") {
                partidaPlanejadaInicial = new Array(Ext.getCmp("dataPartidaPlanejadaInicial").getValue().format('m/d/Y') + " " + Ext.getCmp("dataPartidaPlanejadaInicial").getValue().format('H:i:s'));
                partidaPlanejadaFinal = new Array(Ext.getCmp("dataPartidaPlanejadaFinal").getValue().format('m/d/Y') + " " + Ext.getCmp("dataPartidaPlanejadaFinal").getValue().format('H:i:s'));
            }
            
            if (Ext.getCmp("dataChegadaPlanejadaInicial").getValue() != "" && Ext.getCmp("dataChegadaPlanejadaFinal").getValue() != "") {
                chegadaPlanejadaInicial = new Array(Ext.getCmp("dataChegadaPlanejadaInicial").getValue().format('m/d/Y') + " " + Ext.getCmp("dataChegadaPlanejadaInicial").getValue().format('H:i:s'));
                chegadaPlanejadaFinal = new Array(Ext.getCmp("dataChegadaPlanejadaFinal").getValue().format('m/d/Y') + " " + Ext.getCmp("dataChegadaPlanejadaFinal").getValue().format('H:i:s'));
            }

            prefixosIncluir = Ext.getCmp('PrefixoIncluir').getValue();
            prefixosExcluir = Ext.getCmp('PrefixoExcluir').getValue();

            var url = "<%= Url.Action("Imprimir") %>";
            
            url += "?codigoUpOrigem=" + upOrigem;
            url += "&codigoUpDestino=" + upDestino;
            url += "&estOrigem=" + estOrigem;
            url += "&estDestino=" + estDestino;
            url += "&prefixo=" + prefixo;
            url += "&partidaPlanejadaInicial=" + partidaPlanejadaInicial;
            url += "&partidaPlanejadaFinal=" + partidaPlanejadaFinal;
            url += "&chegadaPlanejadaInicial=" + chegadaPlanejadaInicial;
            url += "&chegadaPlanejadaFinal=" + chegadaPlanejadaFinal;
            url += "&prefixosIncluir=" + prefixosIncluir;
            url += "&prefixosExcluir=" + prefixosExcluir;
            
             window.open(url, "");
        }
        
        function Pesquisar() {

            var upOrigem = null;
            var upDestino = null;
            var estOrigem = null;
            var estDestino = null;
            var prefixo = null;
            var partidaPlanejadaInicial = null;
            var partidaPlanejadaFinal = null;
            var chegadaPlanejadaInicial = null;
            var chegadaPlanejadaFinal = null;
            var prefixosIncluir = null;
            var prefixosExcluir = null;

            upOrigem = Ext.getCmp('cbOrigem').getRawValue();
            upDestino = Ext.getCmp('cbDestino').getRawValue();
            estOrigem = Ext.getCmp('EstacaoOrigem').getValue();
            estDestino = Ext.getCmp('EstacaoDestino').getValue();
            prefixo = Ext.getCmp('Prefixo').getValue();

            partidaPlanejadaInicial = Ext.getCmp('dataPartidaPlanejadaInicial').getValue();
            partidaPlanejadaFinal = Ext.getCmp('dataPartidaPlanejadaFinal').getValue();
            chegadaPlanejadaInicial = Ext.getCmp('dataChegadaPlanejadaInicial').getValue();
            chegadaPlanejadaFinal = Ext.getCmp('dataChegadaPlanejadaFinal').getValue();
            
            prefixosIncluir = Ext.getCmp('PrefixoIncluir').getValue();
            prefixosExcluir = Ext.getCmp('PrefixoExcluir').getValue();

            if (partidaPlanejadaInicial == "" && partidaPlanejadaFinal == "" && chegadaPlanejadaInicial == "" && chegadaPlanejadaFinal == "") {
                ExibirConsistencia("Ao menos um intervalo de datas deve ser informado para consulta");
                return;
            }

            if ((partidaPlanejadaInicial != "" && partidaPlanejadaFinal == "") || (partidaPlanejadaInicial == "" && partidaPlanejadaFinal != "")) {
                ExibirConsistencia("Ao menos um intervalo de datas deve ser informado para consulta");
                return;
            }

            if ((chegadaPlanejadaInicial != "" && chegadaPlanejadaFinal == "") || (chegadaPlanejadaInicial == "" && chegadaPlanejadaFinal != "")) {
                ExibirConsistencia("Ao menos um intervalo de datas deve ser informado para consulta");
                return;
            }
            
            Ext.getCmp("grid").getEl().mask("Aguarde...", "x-mask-loading");
            
             Ext.Ajax.request({                
			    url: "<% =Url.Action("ObterOnTime") %>",
			    success: function(response) {
                    var data = Ext.decode(response.responseText);
			        LimparPesquisa();
			        
			        if (!data.Sucesso) {
			            ExibirConsistencia(data.Mensagem);
			        } else {

			            if (data.Items != "") {
			            
			                storeTrem.loadData(data);
			                storePerformance.loadData(data);
			                
			                if (data.Indicadores != "") {
			                    Ext.getCmp('totalTrensValue').setText(data.Indicadores.TotalTrem);
			                    Ext.getCmp('TrensEncerradosValue').setText(data.Indicadores.TremEncerrados);
			                    Ext.getCmp('TrensParadosCirculandoValue').setText(data.Indicadores.TremParados);
			                    
			                    Ext.getCmp('TrensPartidasValue').setText(data.Indicadores.TremPartida);
			                    Ext.getCmp('TrensPartidasOsIrregularValue').setText(data.Indicadores.OsRegular);
			                    Ext.getCmp('TrensPartidasOsRegValue').setText(data.Indicadores.OsIrregular);
			                    
			                    Ext.getCmp('TrensAtrasoPartidaValue').setText(data.Indicadores.TremPartidaAtraso);
			                    Ext.getCmp('TremPartidaAtrasoOsRegularValue').setText(data.Indicadores.TremPartidaAtrasoOsRegular);
			                    Ext.getCmp('TremPartidaAtrasoOsIrregularValue').setText(data.Indicadores.TremPartidaAtrasoOsIrregular);
			                    
			                    Ext.getCmp('TrensTiveramChegadaValue').setText(data.Indicadores.TremChegada);
			                    Ext.getCmp('TrensChegadaHorarioValue').setText(data.Indicadores.TremChegadaHorario);
			                    Ext.getCmp('TrensAtrasoChegadaValue').setText(data.Indicadores.TremChegadaAtraso);
			                    
			                    Ext.getCmp('TrensSuprimidosValue').setText(data.Indicadores.TrensSuprimidos433);
			                    Ext.getCmp('TrensCanceladosValue').setText(data.Indicadores.TrensCancelador371);
			                    
			                    Ext.getCmp('HorasDeMultaValue').setText((data.Indicadores.HorasDeMulta.toString()) + ":00");

			                    Ext.getCmp('AtrasoOnTimeValue').setText(data.Indicadores.AtrasoOnTime);
			                    Ext.getCmp('AtrasoOnTimeTotalValue').setText(data.Indicadores.AtrasoOnTimeTotal);
			                } 
			            }
			        }
			        Ext.getCmp("grid").getEl().unmask();
			    },
                failure: function(conn, data) {
                    Ext.getCmp("grid").getEl().unmask();
                    ExibirErro("Ocorreu um erro inesperado");
                },
			    method: "POST",
			    params: {
					     codigoUpOrigem: upOrigem, 
                         codigoUpDestino: upDestino,
                         estOrigem: estOrigem,
                         estDestino: estDestino,
                         prefixo: prefixo,
                         partidaPlanejadaInicial: partidaPlanejadaInicial,
                         partidaPlanejadaFinal: partidaPlanejadaFinal,
                         chegadaPlanejadaInicial: chegadaPlanejadaInicial,
                         chegadaPlanejadaFinal: chegadaPlanejadaFinal,
                         prefixosIncluir: prefixosIncluir,
                         prefixosExcluir: prefixosExcluir
				     }
		    });
        }

        function ExibirConsistencia(mensagem) {
            Ext.Msg.show({
                title: "Mensagem de Informação",
                msg: mensagem,
                buttons: Ext.Msg.OK,
                icon: Ext.MessageBox.INFO,
                minWidth: 200
            });
        }

        function LimparCamposIndicadores() {
            Ext.getCmp('totalTrensValue').setText('');
			Ext.getCmp('TrensEncerradosValue').setText('');
			Ext.getCmp('TrensParadosCirculandoValue').setText('');
			                    
			Ext.getCmp('TrensPartidasValue').setText('');
			Ext.getCmp('TrensPartidasOsIrregularValue').setText('');
			Ext.getCmp('TrensPartidasOsRegValue').setText('');
			                    
			Ext.getCmp('TrensAtrasoPartidaValue').setText('');
			Ext.getCmp('TremPartidaAtrasoOsRegularValue').setText('');
			Ext.getCmp('TremPartidaAtrasoOsIrregularValue').setText('');
			                    
			Ext.getCmp('TrensTiveramChegadaValue').setText('');
			Ext.getCmp('TrensChegadaHorarioValue').setText('');
			Ext.getCmp('TrensAtrasoChegadaValue').setText('');
			                    
			Ext.getCmp('TrensSuprimidosValue').setText('');
			Ext.getCmp('TrensCanceladosValue').setText('');
			                    
			Ext.getCmp('HorasDeMultaValue').setText('');

			Ext.getCmp('AtrasoOnTimeValue').setText('');
			Ext.getCmp('AtrasoOnTimeTotalValue').setText('');
        }
        
         function ExibirErro(mensagem) {
            Ext.Msg.show({
                title: "Mensagem de Erro",
                msg: mensagem,
                buttons: Ext.Msg.OK,
                icon: Ext.MessageBox.ERROR,
                minWidth: 200
            });
        }
         
        function GerarExcel() {
            var upOrigem = null;
            var upDestino = null;
            var estOrigem = null;
            var estDestino = null;
            var prefixo = null;
            var partidaPlanejadaInicial = null;
            var partidaPlanejadaFinal = null;
            var chegadaPlanejadaInicial = null;
            var chegadaPlanejadaFinal = null;
            var prefixosIncluir = null;
            var prefixosExcluir = null;

            upOrigem = Ext.getCmp('cbOrigem').getRawValue();
            upDestino = Ext.getCmp('cbDestino').getRawValue();
            estOrigem = Ext.getCmp('EstacaoOrigem').getValue();
            estDestino = Ext.getCmp('EstacaoDestino').getValue();
            prefixo = Ext.getCmp('Prefixo').getValue();

            if (Ext.getCmp("dataPartidaPlanejadaInicial").getValue() != "" && Ext.getCmp("dataPartidaPlanejadaFinal").getValue() != "") {
                partidaPlanejadaInicial = new Array(Ext.getCmp("dataPartidaPlanejadaInicial").getValue().format('m/d/Y') + " " + Ext.getCmp("dataPartidaPlanejadaInicial").getValue().format('H:i:s'));
                partidaPlanejadaFinal = new Array(Ext.getCmp("dataPartidaPlanejadaFinal").getValue().format('m/d/Y') + " " + Ext.getCmp("dataPartidaPlanejadaFinal").getValue().format('H:i:s'));
            }
            
            if (Ext.getCmp("dataChegadaPlanejadaInicial").getValue() != "" && Ext.getCmp("dataChegadaPlanejadaFinal").getValue() != "") {
                chegadaPlanejadaInicial = new Array(Ext.getCmp("dataChegadaPlanejadaInicial").getValue().format('m/d/Y') + " " + Ext.getCmp("dataChegadaPlanejadaInicial").getValue().format('H:i:s'));
                chegadaPlanejadaFinal = new Array(Ext.getCmp("dataChegadaPlanejadaFinal").getValue().format('m/d/Y') + " " + Ext.getCmp("dataChegadaPlanejadaFinal").getValue().format('H:i:s'));
            }

            prefixosIncluir = Ext.getCmp('PrefixoIncluir').getValue();
            prefixosExcluir = Ext.getCmp('PrefixoExcluir').getValue();

            var url = "<%= Url.Action("ObterOnTime") %>";
            
            url += "?codigoUpOrigem=" + upOrigem;
            url += "&codigoUpDestino=" + upDestino;
            url += "&estOrigem=" + estOrigem;
            url += "&estDestino=" + estDestino;
            url += "&prefixo=" + prefixo;
            url += "&partidaPlanejadaInicial=" + partidaPlanejadaInicial;
            url += "&partidaPlanejadaFinal=" + partidaPlanejadaFinal;
            url += "&chegadaPlanejadaInicial=" + chegadaPlanejadaInicial;
            url += "&chegadaPlanejadaFinal=" + chegadaPlanejadaFinal;
            url += "&prefixosIncluir=" + prefixosIncluir;
            url += "&prefixosExcluir=" + prefixosExcluir;
            url += "&gerarExcel=" + true;
            
            window.open(url, "");
         }

         function Limpar() {
            LimparPesquisa();
			Ext.getCmp("filtros").getForm().reset();
         }
         
         function LimparPesquisa() {
            grid.getStore().removeAll();
			gridOnTime.getStore().removeAll();
            LimparCamposIndicadores();
         }
         
        Ext.onReady(function () {
            filtros.render(document.body);
            grid.render(document.body);
            rodapePanel.render(document.body);
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div id="header-content">
        <h1>
            Consulta On Time Performance
        </h1>
        <small>Você está em Consultas > Trem > OnTime Perf.</small>
        <br />
    </div>
</asp:Content>
