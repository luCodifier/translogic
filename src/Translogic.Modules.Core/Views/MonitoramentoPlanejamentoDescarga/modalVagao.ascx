﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div id="div-modal-Vagao"></div>
<%
    var OS = ViewData["OS"];
    var DataInicio = ViewData["DataInicio"];
    var DataFim = ViewData["DataFim"];
    var CPrefixo = ViewData["Prefixo"];
    var CTerminal = ViewData["Terminal"];
%>
<script type="text/javascript">
    var gridVagoes;
    var OS = '<%=OS %>';
    var DataInicio = '<%=DataInicio %>';
    var DataFim = '<%=DataFim %>';
    var Prefixo = '<%=CPrefixo %>';
    var Terminal = '<%=CTerminal %>';
    //JAVASCRIPT

    function PesquisarVagao() {
        if (Ext.getCmp("txtVagao").getValue() != '' && Ext.getCmp("txtVagao").getValue().length < 7) {
            executarPesquisa = false;
            Ext.Msg.show({
                title: "Mensagem de Erro",
                msg: "É necessário informar os 7 digítos do vagão!",
                buttons: Ext.Msg.OK,
                icon: Ext.MessageBox.ERROR,
                minWidth: 270
            });
        }
        else {
           
            gridVagoes.getStore().removeAll();
            gridVagoes.getStore().load();
        }
    }
    function LimparVagao() {
        $("#txtVagao").val('');
        gridVagoes.getStore().removeAll();
        gridVagoes.getStore().load();
    }

    //STORES
    var gridVagoesStore = new Ext.data.JsonStore({
        root: "Items",
        totalProperty: 'Total',
        id: 'gridVagoesStore',
        name: 'gridVagoesStore',
        autoLoad: true,
        proxy: new Ext.data.HttpProxy({
            url: '<%= Url.Action("ObterVagao","MonitoramentoPlanejamentoDescarga") %>',
            timeout: 600000
        }),
        fields: ['NumOS',
            'EmpresaCnpj',
            'Vagao',
            'Serie',
            'DataSaidaEstacao',
            'StatusCte',
            'NumeroCte',
            'ChaveCte',
            'Pdf',
            'Xml',
            'Estacao']
    });

    var txtVagao = {
        xtype: 'textfield',
        name: 'txtVagao',
        id: 'txtVagao',
        fieldLabel: 'Vagão',
        maskRe: /[Z0-9]/,
        autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '7' },
        allowBlank: false,
        width: 75,
        style: 'text-transform:uppercase;'
    };

    var frmtxtVagao = {
        layout: 'form',
        border: false,
        labelAlign: 'right',
        items: [txtVagao]
    };

    var btnPesquisar = {
        fieldLabel: 'Pesquisar',
        text: 'Pesquisar',
        type: 'submit',
        iconCls: 'icon-find',
        handler: function (b, e) {
            PesquisarVagao();
        }
    };

    var btnLimparVagao = {
        id: "btnLimparVagao",
        fieldLabel: 'Limpar',
        text: 'Limpar',
        type: 'submit',
        handler: function (b, e) {
            LimparVagao();
            PesquisarVagao();
        }
    };
    
    var linhaBotoes = {
        layout: 'column',
        border: false,
        bodyStyle: 'padding-bottom:10px;',
        buttons: [btnPesquisar, btnLimparVagao],
        buttonAlign: 'center'
    };

   
    //GRID Veiculos
    var cm = new Ext.grid.ColumnModel({
        defaults: {
            sortable: true
        },
        columns: [
            { header: "OS", dataIndex: "NumOS", hidden: true },
            { header: "Empresa", dataIndex: "EmpresaCnpj", hidden: true },
            { header: "Vagão", dataIndex: "Vagao", sortable: false, width: 80 },
            { header: "Série", dataIndex: "Serie", sortable: false, width: 60 },
            { header: "Estação", dataIndex: "Estacao", sortable: false, width: 120 },
            { header: "Data Saída Estação", dataIndex: "DataSaidaEstacao", sortable: false, width: 120 },
            { header: 'Status CT-e', dataIndex: "StatusCte", sortable: false, width: 40 },
            { header: 'Número CT-e', dataIndex: "NumeroCte", sortable: false, width: 70 },
            { header: 'Chave CT-e', dataIndex: "ChaveCte", sortable: false, width: 300 },
            { header: "PDF", dataIndex: "Pdf", sortable: false, width: 80 },
            { header: "XML", dataIndex: "Xml", sortable: false, width: 80 }
        ]
    });

    gridVagoes = new Ext.grid.EditorGridPanel({
        id: 'gridVagoes',
        name: 'gridVagoes',
        autoLoadGrid: true,
        height: 400,
        limit: 10,
        stripeRows: true,
        cm: cm,
        region: 'center',
        viewConfig: {
            forceFit: true,
            emptyText: 'Não possui dado(s) para exibição.'
        },
        autoScroll: true,
        loadMask: { msg: App.Resources.Web.Carregando },
        store: gridVagoesStore,
        tbar: [
            txtVagao,
            btnPesquisar,
            btnLimparVagao
        ]
    });

    gridVagoes.getStore().proxy.on('beforeload', function (p, params) {
        params['os'] = OS;
        params['dataInicial'] = DataInicio;
        params['dataFinal'] = DataFim;
        params['prefixo'] = Prefixo;
        params['terminal'] = Terminal;
        if (Ext.getCmp("txtVagao").getValue() != "")
            params['vagao'] = Ext.getCmp("txtVagao").getValue();
        else
            params['vagao'] = '';
    });


    var frmgridVagoes = {
        layout: 'form',
        border: false,
        autoScroll: true,
        items: [
            {
                layout: 'column',
                title: 'Localizar Vagão',
                border: false,
                items: [gridVagoes]
            }]
    };

    var arrCampos = new Array();
    arrCampos.push(frmgridVagoes);

    var formVagoes = new Ext.form.FormPanel
        ({
            id: 'formVagoes',
            height: 450,
            bodyStyle: 'padding: 5px',
            items:
                [
                    arrCampos
                ],
            buttonAlign: "center",
            buttons:
                [
                    {
                        id: 'btnSair',
                        text: "Fechar",
                        handler: function () {
                            modVagao.close();
                        }
                    }
                ]
        });

    /******************************* RENDER *******************************/
    Ext.onReady(function () {
        formVagoes.render("div-modal-Vagao");
    });

</script>
