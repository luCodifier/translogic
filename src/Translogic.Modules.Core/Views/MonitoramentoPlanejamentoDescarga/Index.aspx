﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Interna.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content runat="server" ID="HeadContent" ContentPlaceHolderID="HeadContent">

    <script type="text/javascript" language="javascript">
        var grid = null;
        var isPesquisar = true;
        function ValidarPesquisa() {


            var diferenca = Ext.getCmp("dataFim").getValue() - Ext.getCmp("dataInicio").getValue();
            diferenca = Math.floor(diferenca / (1000 * 60 * 60 * 24));
            if (Ext.getCmp("dataInicio").getValue() == '' || Ext.getCmp("dataFim").getValue() == '') {
                executarPesquisa = false;

                Ext.Msg.show({
                    title: "Mensagem de Erro",
                    msg: "Preencha os filtro datas!",
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.ERROR,
                    minWidth: 250,
                    fn: showResult
                });
                return isPesquisar = false;
            }
            else if (diferenca > 2) {
                executarPesquisa = false;
                Ext.Msg.show({
                    title: "Mensagem de Erro",
                    msg: "O período da pesquisa não deve ultrapassar 3 dias",
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.ERROR,
                    minWidth: 250
                });
                return isPesquisar = false;
            } else if (Ext.getCmp("filtroOs").getValue() == '') {
                executarPesquisa = false;
                Ext.Msg.show({
                    title: "Mensagem de Erro",
                    msg: "É necessário informar a OS!",
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.ERROR,
                    minWidth: 270
                });
                return isPesquisar = false;
            } else if (Ext.getCmp("filtroOs").getValue() != '' && Ext.getCmp("filtroOs").getValue().length < 7) {
                executarPesquisa = false;
                Ext.Msg.show({
                    title: "Mensagem de Erro",
                    msg: "É necessário informar os 7 digítos para a OS!",
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.ERROR,
                    minWidth: 250
                });
                return isPesquisar = false;
            } else if (Ext.getCmp("txtPrefixo").getValue() != '' && Ext.getCmp("txtPrefixo").getValue().length < 3) {
                executarPesquisa = false;
                Ext.Msg.show({
                    title: "Mensagem de Erro",
                    msg: "É necessário informar os 3 digítos para a Prefixo!",
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.ERROR,
                    minWidth: 250
                });
                return isPesquisar = false;
            } else {
                return isPesquisar = true;
            }
        }

        function Pesquisar() {
            if (ValidarPesquisa() != false) {
                executarPesquisa = true;
                grid.getStore().load();
            }
        }

        function Limpar() {
            grid.getStore().removeAll();
            Ext.getCmp("grid-filtros").getForm().reset();
        }

        function Excel() {
            var dataInicial = Ext.getCmp("dataInicio").getValue().format('d/m/Y');
            var dataFinal = Ext.getCmp("dataFim").getValue().format('d/m/Y');
            var prefixo = Ext.getCmp("txtPrefixo").getValue();
            var os = Ext.getCmp("filtroOs").getValue();
            var ddlTerminal = Ext.getCmp("filtroCodTerminal");
            var terminal = ddlTerminal.getValue();

            var url = "<%= this.Url.Action("Exportar") %>";
            url += String.format("?dataInicial={0}", dataInicial);
            url += String.format("&dataFinal={0}", dataFinal);
            url += String.format("&prefixo={0}", prefixo);
            url += String.format("&os={0}", os);
            url += String.format("&terminal={0}", terminal);
            window.open(url, "");
        }

        function showModalVagao(record) {
            var dataInicio = Ext.getCmp("dataInicio").getValue().format('d/m/Y');
            var dataFim = Ext.getCmp("dataFim").getValue().format('d/m/Y');
            modVagao = new Ext.Window({
                name: 'formModalVagao',
                id: 'formModalVagao',
                title: 'Monitoramento Planejamento Descarga',
                modal: true,
                width: 970,
                height: 485,
                closable: true,
                autoLoad:
                {
                    url: '<%= Url.Action("modalVagao","MonitoramentoPlanejamentoDescarga") %>',
                    params: {
                        os: record.data.NumOS,
                        dataInicio: dataInicio,
                        dataFim: dataFim,
                        prefixo: record.data.Prefixo,
                        terminal: record.data.Cnpj
                    },
                    text: "Carregando a lista de vagões...",
                    scripts: true
                },
                listeners: {
                    resize: function (win, width, height, eOpts) {
                        win.center();
                    }
                }
            });
            modVagao.show();
        };

        var dataInicio = {
            xtype: 'datefield',
            fieldLabel: 'Data Início',
            id: 'dataInicio',
            name: 'dataInicio',
            width: 85,
            allowBlank: false,
            value: new Date()
        };

        var dataFim = {
            xtype: 'datefield',
            fieldLabel: 'Data Fim',
            id: 'dataFim',
            name: 'dataFim',
            width: 85,
            allowBlank: false,
            value: new Date()
        };

        var txtPrefixo = {
            xtype: 'textfield',
            style: 'text-transform: uppercase',
            name: 'txtPrefixo',
            id: 'txtPrefixo',
            fieldLabel: 'Prefixo',
            autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '3' },
            maskRe: /[a-zA-Z0-9]/,
            style: 'text-transform: uppercase',
            width: 85
        };

        var txtOs = {
            xtype: 'textfield',
            name: 'filtroOs',
            id: 'filtroOs',
            fieldLabel: 'Os',
            autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '7' },
            maskRe: /[Z0-9]/,
            style: 'text-transform: uppercase',
            width: 85
        };

        var dsTerminal = new Ext.data.JsonStore({
            root: "Items",
            autoLoad: true,
            url: '<%= Url.Action("ObterTerminal") %>',
            fields: [
                'CodigoTerminal',
                'SiglaTerminal'
            ]
        });
        var cboTerminal = new Ext.form.ComboBox({
            id: 'filtroCodTerminal',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            store: dsTerminal,
            valueField: 'CodigoTerminal',
            displayField: 'SiglaTerminal',
            value: '',
            fieldLabel: 'Terminal',
            width: 255
        });

        var btnPesquisar = {
            fieldLabel: 'Terminal',
            text: 'Pesquisar',
            type: 'submit',
            iconCls: 'icon-find',
            handler: function (b, e) {
                Pesquisar();
            }
        };
        var btnLimpar = {
            id: "btnLimpar",
            fieldLabel: 'Limpar',
            text: 'Limpar',
            type: 'submit',
            handler: function (b, e) {
                Limpar();
            }
        };
        var btnExportar = {
            fieldLabel: 'btnExportar',
            text: 'Exportar',
            type: 'submit',
            iconCls: 'icon-page-excel',
            handler: function (b, e) {
                if (ValidarPesquisa() != false) {
                    Excel();
                }
            }
        };

        var arrDataIni = {
            width: 90,
            layout: 'form',
            border: false,
            items: [dataInicio]
        };

        var arrDataFim = {
            width: 90,
            layout: 'form',
            border: false,
            items: [dataFim]
        };

        var arrPrefixo = {
            width: 90,
            layout: 'form',
            border: false,
            items: [txtPrefixo]
        };

        var arrOs = {
            width: 90,
            layout: 'form',
            border: false,
            items: [txtOs]
        };

        var arrTerminal = {
            width: 270,
            layout: 'form',
            border: false,
            items: [cboTerminal]
        };

        var linhaBotoes = {
            layout: 'column',
            border: false,
            bodyStyle: 'padding-bottom:10px;',
            buttons: [btnPesquisar, btnLimpar, btnExportar],
            buttonAlign: 'center'
        };

        var arrlinha1 = {
            layout: 'column',
            border: false,
            width: 1200,
            items: [
                arrDataIni,
                arrDataFim,
                arrOs,
                arrPrefixo,
                arrTerminal,
                linhaBotoes
            ]
        };

        var detalheAction = new Ext.ux.grid.RowActions({
            dataIndex: '',
            header: '',
            align: 'center',
            autoWidth: false,
            width: 35,
            actions: [
                {
                    iconCls: 'icon-detail',
                    tooltip: 'Pesquisar Vagão'
                }
            ],
            callbacks: {
                'icon-detail': function (grid, record, action, row, col) {
                    showModalVagao(record);
                }
            }
        });

        var fm = Ext.form;
        var cm = new Ext.grid.ColumnModel({
            defaults: {
                sortable: true
            },
            columns: [
                detalheAction,
                { header: "Razão Social", dataIndex: "RazaoSocial", hidden: true },
                { header: "Cnpj", dataIndex: "Cnpj", hidden: true },
                { header: "Razão Social", dataIndex: "RazaoSocial", hidden: true },
                { header: "Prefixo", dataIndex: "Prefixo", hidden: true },
                { header: "OS", dataIndex: "NumOS", hidden: true },
                { header: "Vagão", dataIndex: "Vagao", sortable: false, width: 100 },
                { header: "Série", dataIndex: "Serie", sortable: false, width: 100 },
                { header: "Estação", dataIndex: "Estacao", sortable: false, width: 170 },
                { header: "Data Saída Estação", dataIndex: "DataSaidaEstacao", sortable: false, width: 150 },
                { header: 'Status CT-e', dataIndex: "StatusCte", sortable: false, width: 130 },
                { header: 'Número CT-e', dataIndex: "NumeroCte", sortable: false, width: 130 },
                { header: 'Chave CT-e', dataIndex: "ChaveCte", sortable: false, width: 300 },
                { header: "PDF", dataIndex: "Pdf", sortable: false, width: 100 },
                { header: "XML", dataIndex: "Xml", sortable: false, width: 100 },
                { header: "Planejamento", dataIndex: "EmpresaCnpj", hidden: true }
            ]
        });

        // Data Source/Store da Grid
        var gridStore = new Ext.data.GroupingStore({
            id: 'gridStore',
            url: '<%=Url.Action("ObterPlanejamentoDescarga", "MonitoramentoPlanejamentoDescarga") %>',
            autoLoad: false,
            groupField: ['EmpresaCnpj'],

            reader: new Ext.data.JsonReader({
                totalProperty: 'Total',
                root: 'Items',
                id: 'gridStoreReader',
                fields: [
                    'Cnpj',
                    'RazaoSocial',
                    'Prefixo',
                    'NumOS',
                    'EmpresaCnpj',
                    'Vagao',
                    'Serie',
                    'DataSaidaEstacao',
                    'StatusCte',
                    'NumeroCte',
                    'ChaveCte',
                    'Pdf',
                    'Xml',
                    'Estacao'
                ]
            })
        });

        grid = new Ext.grid.GridPanel({
            autoLoadGrid: false,
            id: "grid",
            store: gridStore,
            stripeRows: true,
            cm: cm,
            clicksToEdit: 1,
            region: 'center',
            height: 450,
            width: 1350,
            loadMask: { msg: App.Resources.Web.Carregando },
            plugins: [detalheAction],
            viewConfig: {
                forceFit: true,
                emptyText: 'Não possui dado(s) para exibição.'
            },
            view: new Ext.grid.GroupingView({
                startCollapsed: true
            })
        });

        grid.getStore().proxy.on('beforeload', function (p, params) {
            var ddlTerminal = Ext.getCmp("filtroCodTerminal");
            var terminalId = ddlTerminal.getValue();

            params['filter[0].Campo'] = 'dataInicial';
            params['filter[0].Valor'] = new Array(Ext.getCmp("dataInicio").getValue().format('d/m/Y') + " " + Ext.getCmp("dataInicio").getValue().format('H:i:s'));
            params['filter[0].FormaPesquisa'] = 'Start';

            params['filter[1].Campo'] = 'dataFinal';
            params['filter[1].Valor'] = new Array(Ext.getCmp("dataFim").getValue().format('d/m/Y') + " " + Ext.getCmp("dataFim").getValue().format('H:i:s'));
            params['filter[1].FormaPesquisa'] = 'Start';

            params['filter[2].Campo'] = 'prefixo';
            params['filter[2].Valor'] = Ext.getCmp("txtPrefixo").getValue();
            params['filter[2].FormaPesquisa'] = 'Start';

            params['filter[3].Campo'] = 'os';
            params['filter[3].Valor'] = Ext.getCmp("filtroOs").getValue();
            params['filter[3].FormaPesquisa'] = 'Start';

            params['filter[4].Campo'] = 'terminal';
            params['filter[4].Valor'] = terminalId;
            params['filter[4].FormaPesquisa'] = 'Start';
        });

        var arrCampos = new Array();
        arrCampos.push(arrlinha1);
        arrCampos.push(grid);

        var filtros = new Ext.form.FormPanel({
            id: 'grid-filtros',
            title: "Filtros",
            name: 'filtros',
            region: 'center',
            bodyStyle: 'padding: 15px',
            labelAlign: 'top',
            layout: 'column',
            labelAlign: 'top',
            border: false,
            items: [arrCampos]
        });

        $(function () {
            new Ext.TabPanel({
                id: 'tabPanel',
                renderTo: Ext.getBody(),
                activeTab: 0,
                height: 550,
                plain: true,
                deferredRender: false,
                defaults: { bodyStyle: 'padding:5px' },
                margins: 10,
                items: [filtros]
            });

            grid.getView().refresh();
        });
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div id="header-content">
        <h1>Consulta de Planejamento de Descarga</h1>
        <small>Você está em Descarga > Monitoramento Planejamento Descarga</small>
        <br />
        <br />
    </div>
</asp:Content>
