﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<script type="text/javascript">

var gridLimiteMaximoFrota = null;
var dsFrotas = null;

/************************* STORE **************************/
dsFrotas = new Ext.data.JsonStore({
    url: '<%=Url.Action("ObterFrotas")%>',
    root: "Items",
    autoLoad: true,
    fields: [
					{ name: 'Id' },
					{ name: 'Codigo' }
				],
    listeners: {
        load: function (a, b, c) {
            dsCarregado = true;
        }
    }
});
    
/************************* BOTÕES **************************/

var btnCadastrarFrota = new Ext.Button({
    id: 'btnCadastrarFrota',
    name: 'btnCadastrarFrota',
    text: 'Cadastrar',
    iconCls: 'icon-save',
    handler: function() {
        Ext.getCmp("txtCadastroFrota").setDisabled(false);
        Ext.getCmp("txtCadastroFrota").setValue("");
        Ext.getCmp("txtCadastroFrota").setRawValue("");
        Ext.getCmp("txtCadastroPesoMaximo").setValue("");
        CadastrarLimiteMaximo();
    }
});

/************************* CAMPOS FILTRO **************************/
var txtFiltroFrota = {
    xtype: 'textfield',
    name: 'txtFiltroFrota',
    id: 'txtFiltroFrota',
    vtype: 'ctevagaovtype',
    fieldLabel: 'Frota',
    maxLength: 3,
    autoCreate: { tag: 'input', type: 'text', maxlength: '3', autocomplete: 'off' },
    style: 'text-transform: uppercase',
    width: 50,
    listeners: {
        specialkey: function (f, e) {
            if (e.getKey() == e.ENTER) {
                Pesquisar();
            }
        }
    }
};

/************************* LAYOUT **************************/
    
var coluna1FiltroMaximo = {
    layout: 'form',
    border: false,
    bodyStyle: 'padding: 5px',
    items: [txtFiltroFrota]
};

var linhaFiltroMaximo = {
    layout: 'column',
    border: false,
    items: [coluna1FiltroMaximo]
};

/************************* FILTRO **************************/
var filtrosMaximo = new Ext.form.FormPanel({
    id: 'filtrosMaximo',
    title: 'Filtros',
    name: 'filtrosMaximo',
    initEl: function (el) {
        this.el = Ext.get(el);
        this.id = this.el.id || Ext.id();
        if (this.standardSubmit) {
            this.el.dom.action = this.url;
        } else {
            this.el.on('submit', this.onSubmit, this);
        }
        this.el.addClass('x-form');
    },
    layout: 'form',
    labelAlign: 'top',
    border: false,
    items: [linhaFiltroMaximo],
    buttonAlign: 'center',
    buttons: [
            {
                name: 'btnPesquisarFrota',
                id: 'btnPesquisarFrota',
                text: 'Pesquisar',
                iconCls: 'icon-find',
                handler: Pesquisar
            },
            {
                name: 'btnLimparFrota',
                id: 'btnLimparFrota',
                text: 'Limpar',
                handler: Limpar
            }
        ]
});

/************************* GRID **************************/

var detalheActionLimiteMaximo = new Ext.ux.grid.RowActions({
	    dataIndex: '',
	    header: '...',
	    align: 'center',
	    actions: [
            {
                iconCls: 'icon-edit',
                tooltip: 'Editar'
            },
            {
                iconCls: 'icon-del',
                tooltip: 'Excluir'
            }],
	    callbacks: {
	        'icon-edit': function (grid, record, action, row, col) {

	            editarLimite(record.data.Frota, record.data.PesoMaximo);
	            
	        },
	        'icon-del': function (grid, record, action, row, col) {
	            if (Ext.Msg.confirm("Excluir", "Deseja realmente excluir limite?", function (btn, text) {
	                if (btn == 'yes') 
	                {
	                    RemoverLimiteMaximo(record.data.Id);
	                    gridLimiteMaximoFrota.getStore().remove(record);
	                }
	            }));
	        }
	    }
	});


function editarLimite(frota, limite) 
{   
    var comboBox = Ext.getCmp("txtCadastroFrota");
    var store = comboBox.store;
    var valueField = comboBox.valueField;
    var displayField = comboBox.displayField;

    var recordNumber = store.findExact(displayField, frota, 0);

    while (recordNumber < 0) 
    {
        store = comboBox.store;
        recordNumber = store.findExact(displayField, frota, 0);
    }
    
    var displayValue = store.getAt(recordNumber).data[displayField];
    var value = store.getAt(recordNumber).data[valueField];
    
    comboBox.setValue(value);
    comboBox.setRawValue(displayValue);
    comboBox.selectedIndex = recordNumber;
    comboBox.setDisabled(true);
    Ext.getCmp("txtCadastroPesoMaximo").setValue(limite);
	            
	CadastrarLimiteMaximo();
}

function RemoverLimiteMaximo(limiteId) {
    Ext.Ajax.request({
            url: "<%= Url.Action("RemoverLimiteMaximo") %>",
            method: "POST",
            params: { limiteMaximoId: limiteId },
            success: function(resultRemover)
            {
                var result = Ext.util.JSON.decode(resultRemover.responseText);
                
                Ext.Msg.show({
                    title: "Mensagem",
                    msg: result.Message,
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.INFO,
                    minWidth: 200
                }); 
            },
            failure: function(resVerificarFail)
            {
                var resFail = Ext.util.JSON.decode(resVerificarFail.responseText);

                Ext.Msg.show({
                        title: "Mensagem de Erro",
                        msg: resFail.Message != null && resFail.Message != undefined ? resFail.Message : resFail.message,
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.ERROR,
                        minWidth: 200
                    });
            }
        });
}

gridLimiteMaximoFrota = new Translogic.PaginatedGrid({
    autoLoadGrid: false,
    id: "gridLimiteMaximoFrota",
    name: 'gridLimiteMaximoFrota',
    stripeRows: true,
    region: 'center',
    height: 400,
    loadMask: { msg: App.Resources.Web.Carregando },
    filteringToolbar: [btnCadastrarFrota],
    url: '<%= Url.Action("Pesquisar") %>',
    viewConfig: {
        forceFit: true
    },
    fields: [
    'Id',
    'Frota',
    'PesoMaximo',
    'DataCadastro',
    'DataAlteracao',
    'Usuario'
],
    plugins: detalheActionLimiteMaximo,
    columns: [
    new Ext.grid.RowNumberer(),
    detalheActionLimiteMaximo,
    { dataIndex: "Id", hidden: true },
    { header: 'Frota', dataIndex: "Frota", sortable: false },
    { header: 'Peso Máximo', dataIndex: "PesoMaximo", sortable: false },
    { header: 'Data Cadastro', dataIndex: "DataCadastro", sortable: false },
    //{ header: 'Data Alteração', dataIndex: "DataAlteracao", sortable: false },
    { header: 'Usuário', dataIndex: "Usuario", sortable: false }
]
});

gridLimiteMaximoFrota.getStore().proxy.on('beforeload', function (p, params) {
    params['vagao'] = "";
    params['mercadoria'] = "";
    params['frota'] = $.trim(Ext.getCmp("txtFiltroFrota").getValue()) == "" ? "T" : $.trim(Ext.getCmp("txtFiltroFrota").getValue());
});
            
/************************* POPUP CADASTRO MAXIMO **************************/

var txtCadastroFrota = {
    id: 'txtCadastroFrota',
    xtype: 'combo',
    editable: false,
    name: 'txtCadastroFrota',
    allowBlank: false,
    fieldLabel: 'Frota',
    store: dsFrotas,
    displayField: 'Codigo',
    loadingText: 'Carregando',
    valueField: 'Id',
    forceSelection: true,
    mode: 'local',
    triggerAction: 'all',
    typeAhead: false,
    width: 50,
    listeners: {
        specialkey: function (f, e) {
            if (e.getKey() == e.ENTER) {
                VerificarLimiteMaximo();
            }
        }
    }
};

var txtCadastroPesoMaximo = {
    xtype: 'numberfield',
    name: 'txtCadastroPesoMaximo',
    id: 'txtCadastroPesoMaximo',
    fieldLabel: 'Peso Máximo',
    maxLength: 8,
    width: 70,
    enableKeyEvents: true,
    autoCreate: { tag: 'input', type: 'text', maxlength: '8', autocomplete: 'off' },
    listeners: {
        specialkey: function (f, e) {
            if (e.getKey() == e.ENTER) {
                VerificarLimiteMaximo();
            }
        }
    }
};

var CadastroMaximoColuna1 = {
    layout: 'form',
    border: false,
    bodyStyle: 'padding: 5px',
    items: [txtCadastroFrota]
};

var CadastroMaximoColuna2 = {
    layout: 'form',
    border: false,
    bodyStyle: 'padding: 5px',
    items: [txtCadastroPesoMaximo]
};

var linhaMaximo = {
    layout: 'column',
    bodyStyle: 'padding-left: 55px',
    width: '100%',
    border: false,
    items: [CadastroMaximoColuna1, CadastroMaximoColuna2]
};

var formCadastroMaximo = new Ext.form.FormPanel
({
    id: 'formCadastroMaximo',
    labelWidth: 80,
    width: '100%',
    //height: 100,
    autoScroll: true,
    labelAlign: 'top',
    items:
        [
            linhaMaximo
        ],
    buttonAlign: "center",
    buttons:
        [{
            text: "Salvar",
            handler: function () 
            {
                VerificarLimiteMaximo();
            }
        },
            {
                text: "Cancelar",
                handler: function () {
                windowCadastroMaximo.hide();
            }
        }]
});

var windowCadastroMaximo = null;

function SalvarLimiteMaximo(frota, peso) 
{
    var frotaSel = Ext.getCmp("txtCadastroFrota").getRawValue();
    
    $.ajax(
    {
        url: "<%= Url.Action("SalvarLimiteMaximo") %>",
        type: "POST",
        dataType: 'json',
        data: "{ 'frota': '" + frota + "', 'peso':'" + peso + "' }",
        contentType: "application/json; charset=utf-8",
        success: function(resultSalvar)
        {
            if(resultSalvar.Success) {
                
                Ext.getCmp("txtCadastroFrota").setValue("");
                Ext.getCmp("txtCadastroPesoMaximo").setValue("");
                
                windowCadastroMaximo.hide();
                
                Ext.getCmp("txtFiltroFrota").setValue(frotaSel);
                gridLimiteMaximoFrota.getStore().load();
            }
            
            Ext.Msg.show({
                    title: "Mensagem de Informação",
                    msg: resultSalvar.Message != null && resultSalvar.Message != undefined ? resultSalvar.Message : resultSalvar.message,
                    buttons: Ext.Msg.OK,
                    minWidth: 200
                });
        },
        failure: function(resultErr) {
            Ext.Msg.show({
                title: "Mensagem de Erro",
                msg: resultErr.Message != null && resultErr.Message != undefined ? resultErr.Message : resultErr.message,
                buttons: Ext.Msg.OK,
                icon: Ext.MessageBox.ERROR,
                minWidth: 200
            });
        }
    });
}
            
function VerificarResponseFrota(responseData, frota, pesoMaximo) 
{
    if (responseData.LimiteExcecaoFrota != null || responseData.LimiteExcecaoFrota != undefined) 
    {
        var pesoAtual = parseFloat(responseData.LimiteExcecaoFrota.PesoMaximo);
        if (pesoAtual != parseFloat(pesoMaximo)) 
        {
            if (Ext.Msg.confirm("Alteração de Limite", "Confirma a alteração do Limite: "+pesoAtual+" para: "+pesoMaximo, function(btn, text) {
                if (btn == 'yes')
                {
                    SalvarLimiteMaximo(frota, pesoMaximo);
                }
            }));
        }
        else {
            Ext.Msg.show({
                    title: "Mensagem de Informação",
                    msg: "Não há alterações de limite<br/>Limite Atual: "+pesoAtual+"<br/>Novo Limite: "+pesoMaximo,
                    buttons: Ext.Msg.OK,
                    minWidth: 200
                });
        }
    }
    else
    {
        SalvarLimiteMaximo(frota, pesoMaximo);
    }
}
            
function VerificarLimiteMaximo() 
{
    var idFrota = Ext.getCmp("txtCadastroFrota").getValue();
    var pesoMax = Ext.getCmp("txtCadastroPesoMaximo").getValue();
    
    if($.trim(idFrota.toString()) != "" && $.trim(pesoMax.toString()) != "") {
        $.ajax(
        {
            url: "<%= Url.Action("VerificarLimiteMaximo") %>",
            type: "POST",
            dataType: 'json',
            data: "{ 'limite':'"+ pesoMax + "', 'idFrota':'"+ idFrota +"' }",
            contentType: "application/json; charset=utf-8",
            success: function(result) 
            {
                if (result.Success) 
                {
                    VerificarResponseFrota(result, idFrota, pesoMax);
                }
                else {
                    if (result.Status == 200) {
                     Ext.Msg.show({
                            title: "Mensagem de Erro",
                            msg: result.Message,
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.ERROR,
                            minWidth: 200
                        });   
                    }
                }
            },
            failure: function(resFail) {
                Ext.Msg.show({
                        title: "Mensagem de Erro",
                        msg: resFail.Message != null && resFail.Message != undefined ? resFail.Message : resFail.message,
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.ERROR,
                        minWidth: 200
                    });
            }
        });
    }
    else {
        Ext.Msg.show({
            title: "Mensagem de Erro",
            msg: 'Preencha todas as informações para continuar',
            buttons: Ext.Msg.OK,
            icon: Ext.MessageBox.ERROR,
            minWidth: 200
        });
    }
}

function CadastrarLimiteMaximo() {

    windowCadastroMaximo = new Ext.Window({
        id: 'windowCadastroMaximo',
        title: 'Cadastrar Limite Máximo por Frota Vagão',
        modal: true,
        width: 270,
        closeAction: 'hide',
        height: 130,
        items: [formCadastroMaximo],
        listeners: {
            'hide': function(){
                Ext.getCmp("txtCadastroFrota").setDisabled(false);
                Ext.getCmp("txtCadastroFrota").setValue("");
                Ext.getCmp("txtCadastroFrota").setRawValue("");
                Ext.getCmp("txtCadastroPesoMaximo").setValue("");
			}
        }
    });
    windowCadastroMaximo.show();
}
</script>