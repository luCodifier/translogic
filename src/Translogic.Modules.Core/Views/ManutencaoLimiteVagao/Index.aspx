﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Interna.Master" Inherits="System.Web.Mvc.ViewPage" %>
<asp:Content runat="server" ID="HeadContent" ContentPlaceHolderID="HeadContent">
<script type="text/javascript">
    var dsCarregado = false;
    
    /************************* FUNCOES **************************/
    function Pesquisar() {

        var activeTabFiltros = tabs.getActiveTab();
        var activeTabFiltrosIndex = tabs.items.findIndex('id', activeTabFiltros.id);

        if (activeTabFiltrosIndex == 0) {
            Ext.getCmp("txtFiltroFrota").setValue("");
            gridLimiteMinimoVagao.getStore().load(); 
        }
        else {
            Ext.getCmp("txtFiltroMercadoria").setValue("");
            Ext.getCmp("txtFiltroVagao").setValue("");
            gridLimiteMaximoFrota.getStore().load();
            if (!dsCarregado) {
                var mask = new Ext.LoadMask(Ext.getBody(), { msg: 'Carregando Frotas . . .', store: dsFrotas });
                mask.show();
            }
        }
    }

    function Limpar() {
        gridLimiteMinimoVagao.getStore().removeAll();
        Ext.getCmp("filtrosMinimo").getForm().reset();

        gridLimiteMaximoFrota.getStore().removeAll();
        Ext.getCmp("filtrosMaximo").getForm().reset();
    }
</script>
<!--   User Controls componentes de Layout   -->
<%Html.RenderPartial("LimiteMinimoVagao"); %>
<%Html.RenderPartial("LimiteMaximoFrotaVagao"); %>
<!-- Fim User Controls componentes de Layout -->

    <script type="text/javascript">

        
        
        /************************* BOTÕES **************************/

        var tabs = new Ext.TabPanel({
            activeTab: 0,
            height: 670,
            items: [{
                    title: 'Limite Mínimo por Vagão',
                    items: [filtrosMinimo, gridLimiteMinimoVagao]
                }, {
                    title: 'Limite Máximo por Frota Vagão',
                    items: [filtrosMaximo, gridLimiteMaximoFrota]
                }
            ]
        });


        /************************* RENDER **************************/
        Ext.onReady(function () {
            dsFrotas.load();
            tabs.render(document.body);
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div id="header-content">
        <h1>
            Operação - Manutenção de Limite de Peso de Vagões</h1>
        <small>Operação > Manutenção de Limite de Peso de Vagões</small>
        <br />
    </div>
</asp:Content>
