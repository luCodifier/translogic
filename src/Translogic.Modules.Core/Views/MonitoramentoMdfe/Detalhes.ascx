﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div id="divDetalhes">
</div>
<script type="text/javascript">
    /******************************* VARIAVEIS *******************************/
    var _idMdfe;

    /******************************* FUNCOES *******************************/
    function MostrarDetalhesComposicao(idMdfe) {	
        _idMdfe = idMdfe;
        gridComposicaoDetalhe.getStore().load();
    }	

	/******************************* STORE *******************************/
	 var storeMdfe = new Ext.data.JsonStore({
        root: "items",
		autoLoad: false,
		url: '<%= Url.Action("DetalhesMdfe", new { idTrem = ViewData["IdTrem"] } ) %>',
        fields: [
                 'Id',
                'Chave',
                'SituacaoAtual',
                'Numero',
                'Serie',
                'SituacaoAtualEnum',
                'PdfGerado',
                'PdfGerado',
                'DataHora',
                'IndAnulacao'
			    ]
    });	 
	 
    var storeDetalhe = new Ext.data.JsonStore({
        root: "Items",
		autoLoad: false,
        url: '<%= Url.Action("DetalhesComposicao") %>',
        fields: [
                'Id',
                'Sequencia',
                'CodigoVagao',
                'SerieCte',
                'SituacaoCte',
                'NotasCte',
                'NumeroCte'
            ]
    });

    /******************************* GRIDS *******************************/
    var changeCursorToHand = function (val, metadata, record) {
        metadata.style = 'cursor: pointer;';
        return val;
    };	 

	 function printRenderer(val, metadata, record) {         	
		if (val == 'S') {
			
			var urlPrint = '<%= Url.Action("Imprimir") %>' +"?idMdfe="+ record.data.Id;
			
			return "<a href='"+urlPrint+"'><img src='<%=Url.Images("Icons/pdf.png") %>' alt='Imprimir Mdfe'></a>";
		} else {
			return "";
		}

		metadata.style = 'cursor: pointer;';
		return val;
    };
    
    var detalheAction = new Ext.ux.grid.RowActions({
		dataIndex: '',
		header: '',
		align: 'center',
		actions: [
            {
				iconCls: 'icon-detail',
				tooltip: 'Detalhes'
			}],
		callbacks: {
			'icon-detail': function(grid, record, action, row, col) {
			    MostrarStatus(record.data.Id);
			}
		}
	});
	
    var gridMdfeDetalhe = new Ext.grid.GridPanel({
        id: 'gridMdfeDetalhe',
        name: 'gridMdfeDetalhe',
        autoLoadGrid: false,
		height: 100,
        width: 535,
		viewConfig: {
            forceFit: true
        },
        stripeRows: true,
        region: 'center',
        plugins: [detalheAction],
        store: storeMdfe,
        loadMask: { msg: App.Resources.Web.Carregando },
        colModel: new Ext.grid.ColumnModel({
            defaults: {
                sortable: false
            },
            columns: [
					{ header: '...', width: 30, dataIndex: "PdfGerado", renderer:printRenderer  },
                    detalheAction,
					// { header: 'Chave', width: 300, autoExpandColumn: true, dataIndex: "Chave" },
                    { header: 'Serie', width: 30, dataIndex: "Serie", sortable: false },
                    { header: 'Numero', width: 50, dataIndex: "Numero", sortable: false },
					{ header: 'Status', width: 30, dataIndex: "SituacaoAtualEnum", renderer:statusRenderer },
					{ header: 'Data', width: 100, dataIndex: "DataHora" }
				]
        }),
		listeners: {
			rowdblclick: function (grid, rowIndex, record, e) {
				MostrarDetalhesComposicao(gridMdfeDetalhe.getStore().getAt(rowIndex).get('Id'));
			},
			rowclick: function(grid, rowIndex, e){
			    Ext.getCmp('btnExportar').setDisabled(false);
			    // Ext.getCmp('btnRelatorioEntrega').setDisabled(false);
			    
				if(grid.getStore().getAt(rowIndex).data.SituacaoAtualEnum == 'AUT')
				{
				    Ext.getCmp('btnEncerrar').setDisabled(false);
                    var indAnulacao = grid.getStore().getAt(rowIndex).data.IndAnulacao;

				    if(indAnulacao == 1) {
					    Ext.getCmp('btnCancelar').setDisabled(false);
				    } else {
				        Ext.getCmp('btnCancelar').setDisabled(true);
				    }

				}else if (grid.getStore().getAt(rowIndex).data.SituacaoAtualEnum == 'PME' ||
				          grid.getStore().getAt(rowIndex).data.SituacaoAtualEnum == 'EAR' ||
				          grid.getStore().getAt(rowIndex).data.SituacaoAtualEnum == 'ERR') {
				    Ext.getCmp('btnEnviarAprovacao').setDisabled(false);
				}
                else{
					Ext.getCmp('btnEncerrar').setDisabled(true);
					Ext.getCmp('btnCancelar').setDisabled(true);
				    Ext.getCmp('btnEnviarAprovacao').setDisabled(true);
				}
					_idMdfe = gridMdfeDetalhe.getStore().getAt(rowIndex).get('Id');
				}
			}            
    });

       /************************* DETALHES **************************/
        var formStatusProcessamento;

        function MostrarStatus(idMdfe) {
            formStatusProcessamento = null;

            formStatusProcessamento = new Ext.Window({
                name: 'formStatusProcessamento',
                id: 'formStatusProcessamento',
                title: "Status processamento do MDFe",
                expandonShow: true,
                modal: true,
                width: 517,
                height: 300,
                closable: true,
                autoLoad:
                {
                    url: '<%= Url.Action("StatusProcessamento") %>',
                    params: { idMdfe: idMdfe },
                    scripts: true
                }
            });

            formStatusProcessamento.show();
        }
        

    function statusRendererCte(val)
      {
        switch(val){
            case "AUT":
                return "<img src='<%=Url.Images("Icons/tick.png") %>' alt='CTe Aprovado'>";
                break;
						case "AGI":
						case "AGC":
                return "<img src='<%=Url.Images("Icons/application_go.png") %>' alt='Aguardando Cancelamento / Inutilização'>";
                break;
            case "EAE":
                return "<img src='<%=Url.Images("Icons/email_go.png") %>' alt='Enviado arquivo CTe para Sefaz.'>";
                break;
            case "EAR":
                return "<img src='<%=Url.Images("Icons/arrow_redo_yellow.png") %>' alt='Erro Autorizado reenvio do CTe.'>";
                break;            
            case "CAN":
                return "<img src='<%=Url.Images("Icons/cancel.png") %>' alt='CTe Cancelado '>";
                break;                        
             case "INV":
                return "<img src='<%=Url.Images("Icons/delete.png") %>' alt='CTe Invalidado'>";
                break;                        
             case "AGC":
                return "<img src='<%=Url.Images("Icons/lock_edit.png") %>' alt='Aguardando cancelamento.'>";
                break;                                    
             case "AUC":
                return "<img src='<%=Url.Images("Icons/lock_open.png") %>' alt='Autorizado cancelamento.'>";
                break;
             case "ERR":
                return "<img src='<%=Url.Images("Icons/cross.png") %>' alt='Erro na geração do CTe.'>";
                break; 
			case "PCC":
                return "<img src='<%=Url.Images("Icons/information.png") %>' alt='Cte aguardando a geração automática da numeração / chave.'>";
                break;
            case "":
                return "";
                break; 	           
            default:
                return "<img src='<%=Url.Images("Icons/alert.png") %>' alt='Cte em processamento.'>";
                break;
        }
      }      
    
       var gridComposicaoDetalhe = new Ext.grid.GridPanel({
        id: 'grid',
        name: 'grid',
        autoLoadGrid: false,
        height: 300,
        width: 535,
		viewConfig: {
            forceFit: true
        },
        stripeRows: true,
        region: 'center',
        store: storeDetalhe,
        loadMask: { msg: App.Resources.Web.Carregando },
        colModel: new Ext.grid.ColumnModel({
            defaults: {
                sortable: false
            },
             columns: [
                 { header: 'Seq.', width: 40, dataIndex: "Sequencia", sortable: false },
                { header: 'Cód. Vg.', width: 80, dataIndex: "CodigoVagao", sortable: false },
                { header: 'Ser. CTE', width: 80, dataIndex: "SerieCte", sortable: false },
                { header: 'Nr. CTE', width: 80, dataIndex: "NumeroCte", sortable: false },
                { header: '...',width: 25, dataIndex: "SituacaoCte",   sortable: false,renderer:statusRendererCte },
                { header: 'Notas',width: 200, dataIndex: "NotasCte",   sortable: false}
             ]
        })           
    });
  /*  
  var gridComposicaoDetalhe = new Translogic.PaginatedGrid({
        id: 'grid',
        name: 'grid',
        autoLoadGrid: false,
        height: 300,
        width: 540,
        fields: [
                'Id',
                'Sequencia',
                'CodigoVagao',
                'SerieCte',
                'SituacaoCte',
                'NotasCte',
                'NumeroCte'
            ],
        url: '<%= Url.Action("DetalhesComposicao") %>',
        columns: [
                { header: 'Seq.', width: 40, dataIndex: "Sequencia", sortable: false },
                { header: 'Cód. Vg.', width: 80, dataIndex: "CodigoVagao", sortable: false },
                { header: 'Ser. CTE', width: 80, dataIndex: "SerieCte", sortable: false },
                { header: 'Nr. CTE', width: 80, dataIndex: "NumeroCte", sortable: false },
                { header: '...',width: 25, dataIndex: "SituacaoCte",   sortable: false,renderer:statusRendererCte },
                { header: 'Notas',width: 200, dataIndex: "NotasCte",   sortable: false}
            ]
    });
    */
    gridComposicaoDetalhe.getStore().proxy.on('beforeload', function (p, params) {
        params['idMdfe'] = _idMdfe;
    });
    
    /******************************* LAYOUT *******************************/
    var coluna1 = {
        title: 'MDFe',
        layout: 'form',
        border: false,
        width: 550,
        items: [gridMdfeDetalhe]
    };

    var coluna2 = {
        title: 'Composição',
        layout: 'form',
        border: false,
        width: 550,
        items: [gridComposicaoDetalhe]
    };

    var linha = {
        layout: 'column',
        border: false,
        items: [coluna1, coluna2]
    };

    var btnEncerrar = {
       	name: 'btnEncerrar',
       	id: 'btnEncerrar',
       	text: 'Encerrar',
		disabled: true,
       	handler: function () {
       		
			Ext.getCmp("gridMdfeDetalhe").getEl().mask();
			Ext.getCmp("grid").getEl().mask();
       	    if (Ext.Msg.confirm("Encerrar", "Você deseja realmente Encerrar o MDF-e?", function(btn, text) {
       	        if (btn == 'yes') {
       	            Ext.Ajax.request({
       	                url: "<% =Url.Action("Encerrar") %>",
       	                method: "POST",
       	                params: { idMdfe: _idMdfe },
       	                success: function(response) {
       	                    var result = Ext.util.JSON.decode(response.responseText);

       	                    if (result.success) {
       	                        Ext.Msg.show({
       	                            title: "Mensagem de Informação",
       	                            msg: "Mdf-e enviado para encerramento!",
       	                            buttons: Ext.Msg.OK,
       	                            minWidth: 200
       	                        });
       	                    } else {
       	                        Ext.Msg.show({
       	                            title: "Mensagem de Erro",
       	                            msg: "Erro ao enviar o Mdf-e para encerramento!</BR>" + result.Message,
       	                            buttons: Ext.Msg.OK,
       	                            icon: Ext.MessageBox.ERROR,
       	                            minWidth: 200
       	                        });
       	                    }

       	                    Ext.getCmp("gridMdfeDetalhe").getEl().unmask();
       	                    Ext.getCmp("grid").getEl().unmask();
       	                    CarregarGridDetalheMdfe();

       	                },
       	                failure: function(conn, data) {
       	                    Ext.Msg.show({
       	                        title: "Mensagem de Erro",
       	                        msg: "Erro ao enviar o Mdf-e para encerramento!",
       	                        buttons: Ext.Msg.OK,
       	                        icon: Ext.MessageBox.ERROR,
       	                        minWidth: 200
       	                    });
       	                }
       	            });
       	        }
       	    })) ;
       	}
     };

      var btnEnviarAprovacao = {
          name: 'btnEnviarAprovacao',
          id: 'btnEnviarAprovacao',
          text: 'Enviar Aprovação',
          disabled: true,
          handler: function() {

              if (Ext.Msg.confirm("Aprovação", "Você deseja realmente enviar o MDF-e para aprovação?", function(btn, text) {
                  if (btn == 'yes') {

                      Ext.Ajax.request({
                          url: "<% =Url.Action("EnviarAprovacao") %>",
                          method: "POST",
                          params: { idMdfe: _idMdfe },
                          success: function(response) {
                              var result = Ext.util.JSON.decode(response.responseText);

                              if (result.success) {
                                  Ext.Msg.show({
                                      title: "Mensagem de Informação",
                                      msg: "Mdf-e enviado para aprovação!",
                                      buttons: Ext.Msg.OK,
                                      minWidth: 200
                                  });
                              } else {
                                  Ext.Msg.show({
                                      title: "Mensagem de Erro",
                                      msg: "Erro ao enviar o Mdf-e para aprovação!</BR>" + result.Message,
                                      buttons: Ext.Msg.OK,
                                      icon: Ext.MessageBox.ERROR,
                                      minWidth: 200
                                  });
                              }

                              CarregarGridDetalheMdfe();
                          },
                          failure: function(conn, data) {
                              Ext.Msg.show({
                                  title: "Mensagem de Erro",
                                  msg: "Erro ao enviar o Mdf-e para aprovação!",
                                  buttons: Ext.Msg.OK,
                                  icon: Ext.MessageBox.ERROR,
                                  minWidth: 200
                              });
                          }
                      });
                  }
              })) ;
          }
      };

     var btnCancelar = {
     	name: 'btnCancelar',
     	id: 'btnCancelar',
     	text: 'Cancelar',
		disabled: true,
     	handler: function () {

     	    if (Ext.Msg.confirm("Cancelar", "Você deseja realmente Cancelar o MDF-e?", function(btn, text) {
     	        if (btn == 'yes') {

     	            Ext.Ajax.request({
     	                url: "<% =Url.Action("Cancelar") %>",
     	                method: "POST",
     	                params: { idMdfe: _idMdfe },
     	                success: function(response) {
     	                    var result = Ext.util.JSON.decode(response.responseText);

     	                    if (result.success) {
     	                        Ext.Msg.show({
     	                            title: "Mensagem de Informação",
     	                            msg: "Mdf-e enviado para cancelamento!",
     	                            buttons: Ext.Msg.OK,
     	                            minWidth: 200
     	                        });
     	                    } else {
     	                        Ext.Msg.show({
     	                            title: "Mensagem de Erro",
     	                            msg: "Erro ao enviar o Mdf-e para cancelamento!</BR>" + result.Message,
     	                            buttons: Ext.Msg.OK,
     	                            icon: Ext.MessageBox.ERROR,
     	                            minWidth: 200
     	                        });
     	                    }

     	                    CarregarGridDetalheMdfe();
     	                },
     	                failure: function(conn, data) {
     	                    Ext.Msg.show({
     	                        title: "Mensagem de Erro",
     	                        msg: "Erro ao enviar o Mdf-e para cancelamento!",
     	                        buttons: Ext.Msg.OK,
     	                        icon: Ext.MessageBox.ERROR,
     	                        minWidth: 200
     	                    });
     	                }
     	            });
     	        }
     	    })) ;
     	}
     };

    var btnFechar = {
        name: 'btnFecharDetalhes',
        id: 'btnFecharDetalhes',
        text: 'Fechar',
        handler: function () {
            Ext.getCmp('formDetalhes').close();
        }
    };
    
    var btnExportar = {
        name: 'btnExportar',
        id: 'btnExportar',
        text: 'Exportar',
        handler: function () {
            var url =  "<%= Url.Action("Exportar") %>";
				
		    url += "?idMdfe=" + _idMdfe;

		    window.open(url,"");
        }
    };

     var btnRelatorioEntrega = {
        name: 'btnRelatorioEntrega',
        id: 'btnRelatorioEntrega',
        text: 'Relatório Entrega',
        handler: function () {
            var url =  "<%= Url.Action("ExportarRelatorioEntrega") %>";
				
		    url += "?idMdfe=" + _idMdfe;

		    window.open(url,"");
        }
    };
     
    var painelDetalhes = new Ext.form.FormPanel({
        id: 'painelDetalhes',
        name: 'painelDetalhes',
        items: [linha],
        buttonAlign: 'center',
        buttons: [btnExportar, btnEnviarAprovacao, btnEncerrar,btnCancelar, btnFechar]
    });
    
	function CarregarGridDetalheMdfe()
	{
		Ext.getCmp('btnEnviarAprovacao').setDisabled(true);
		Ext.getCmp('btnEncerrar').setDisabled(true);
		Ext.getCmp('btnCancelar').setDisabled(true);
		Ext.getCmp('btnExportar').setDisabled(true);
		// Ext.getCmp('btnRelatorioEntrega').setDisabled(true);
		gridComposicaoDetalhe.getStore().removeAll();
		gridMdfeDetalhe.getStore().load();		
	}

    /******************************* RENDER *******************************/
    Ext.onReady(function () {
        painelDetalhes.render("divDetalhes");
        CarregarGridDetalheMdfe();
    });
</script>
