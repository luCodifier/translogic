﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div id="divStatus">
</div>
<script type="text/javascript">
    /******************************* VARIAVEIS *******************************/
    var _idMdfe;

    _idMdfe = '<%=ViewData["IdMdfe"] %>';

    /******************************* STORE *******************************/
    var dsStatus = new Ext.data.JsonStore({
        root: "Items",
        autoLoad: false,
        //url: '<%= Url.Action("ObterHistoricoStatusMdfe",ViewData["IdMdfe"] ) %>',
         proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("ObterHistoricoStatusMdfe") %>', timeout: 600000 }),
        fields: [
				    'DescricaoStatus',
				    'DateEmissao',
				    'Usuario',
				    'Situacao'
			    ]
    });

    var gridMdfeStatus = new Ext.grid.GridPanel({
        stripeRows: true,
        height: 293,
        width: 502,
        region: 'center',
        store: dsStatus,
        loadMask: { msg: App.Resources.Web.Carregando },
        colModel: new Ext.grid.ColumnModel({
            defaults: {
                sortable: false
            },
            columns: [
							{ header: 'Data', dataIndex: "DateEmissao", width: 110, sortable: false },
							{ header: 'Descrição', dataIndex: "DescricaoStatus", width: 230, sortable: false },
							{ header: 'Usuario', dataIndex: "Usuario", width: 150, sortable: false }
						]
        })
    });

    /******************************* GRIDS *******************************/
    var changeCursorToHand = function (val, metadata, record) {
        metadata.style = 'cursor: pointer;';
        return val;
    };

    /******************************* LAYOUT *******************************/
    var coluna1 = {
        title: 'Status do Mdfe',
        layout: 'form',
        border: false,
        width: 500,
        items: [gridMdfeStatus]
    };

    var linha = {
        layout: 'column',
        border: false,
        items: [coluna1]
    };

    var btnFechar = {
        name: 'btnFecharDetalhes',
        id: 'btnFecharDetalhes',
        text: 'Fechar',
        handler: function () {
            Ext.getCmp('painelStatus').close();
        }
    };

    var painelDetalhes = new Ext.form.FormPanel({
        id: 'painelStatus',
        name: 'painelStatus',
        items: [gridMdfeStatus],
        buttonAlign: 'center',
        buttons: [btnFechar]
    });

    function CarregarGridStatusMdfe() {
        gridMdfeStatus.getStore().removeAll();
        gridMdfeStatus.getStore().load({
            params: {
                idMdfe: _idMdfe
            }
        });
    }

    /******************************* RENDER *******************************/
    Ext.onReady(function () {
        painelDetalhes.render("divStatus");
        CarregarGridStatusMdfe();
    });
</script>
