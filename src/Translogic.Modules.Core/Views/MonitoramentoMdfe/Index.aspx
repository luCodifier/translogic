﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Interna.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content runat="server" ID="HeadContent" ContentPlaceHolderID="HeadContent">
    <script type="text/javascript" language="javascript">
        /************************* GRID **************************/
        var changeCursorToHand = function(val, metadata, record) {
            metadata.style = 'cursor: pointer;';
            return val;
        };

        function statusRenderer(val) {
            switch (val) {
            case "AUT":
                return "<img src='<%= this.Url.Images("Icons/tick.png") %>' alt='MDF-e Aprovado'>";
                break;
            case "AGE":
            case "AGC":
                return "<img src='<%= this.Url.Images("Icons/application_go.png") %>' alt='Aguardando Cancelamento/Encerramento'>";
                break;
            case "EAE":
                return "<img src='<%= this.Url.Images("Icons/email_go.png") %>' alt='Enviado arquivo MDF-e para Sefaz.'>";
                break;
            case "EAR":
                return "<img src='<%= this.Url.Images("Icons/arrow_redo_yellow.png") %>' alt='Erro Autorizado reenvio do MDF-e.'>";
                break;
            case "CAN":
                return "<img src='<%= this.Url.Images("Icons/cancel.png") %>' alt='MDF-e Cancelado '>";
                break;
            case "ENC":
                return "<img src='<%= this.Url.Images("Icons/delete.png") %>' alt='MDF-e Encerrado'>";
                break;
            case "ERR":
                return "<img src='<%= this.Url.Images("Icons/cross.png") %>' alt='Erro na geração do MDF-e.'>";
                break;
            case "PGC":
                return "<img src='<%= this.Url.Images("Icons/information.png") %>' alt='MDf-e aguardando a geração automática da numeração / chave.'>";
                break;
            case "SEM":
                return "<img src='<%= this.Url.Images("Icons/lock.png") %>' alt='O MDF-e da composição será gerado na estação de Pereque (ZPG).'>";
                break;
            default:
                return "<img src='<%= this.Url.Images("Icons/alert.png") %>' alt='MDF-e em processamento.'>";
                break;
            }
        }

        var detalheAction = new Ext.ux.grid.RowActions({
            dataIndex: '',
            header: '',
            align: 'center',
            actions: [
                {
                    iconCls: 'icon-detail',
                    tooltip: 'Exibir Detalhes'
                }
            ],
            callbacks: {
                'icon-detail': function(grid, record, action, row, col) {
                    MostrarDetalhes(record.data.Id);
                }
            }
        });

        var grid = new Translogic.PaginatedGrid({
            id: 'grid',
            name: 'grid',
            autoLoadGrid: false,
            height: 400,
            plugins: [detalheAction],
            fields: [
                'Id',
                'Prefixo',
                'Origem',
                'Destino',
                'OS',
                'SituacaoAtualEnum',
                'Informativo',
                'UltimoEvento',
                'DataEvento'
            ],
            url: '<%= this.Url.Action("Pesquisar") %>',
            columns: [
                { dataIndex: "Id", hidden: true },
                detalheAction,
                { header: 'Status', width: 20, dataIndex: "SituacaoAtualEnum", renderer: statusRenderer },
                { header: 'Prefixo', width: 50, dataIndex: "Prefixo", sortable: false },
                { header: 'Origem', width: 50, dataIndex: "Origem", sortable: false },
                { header: 'Destino', width: 50, dataIndex: "Destino", sortable: false },
                { header: 'OS', width: 50, dataIndex: "OS", sortable: false },
                { header: 'Informativo', width: 190, dataIndex: "Informativo", sortable: false },
                { header: 'Último Evento', width: 210, dataIndex: "UltimoEvento", sortable: false },
                { header: 'Data Evento', width: 60, dataIndex: "DataEvento", sortable: false }
            ],
            listeners: {
                rowdblclick: function(grid, rowIndex, record, e) {
                    //  MostrarDetalhes(grid.getStore().getAt(rowIndex).get('Id'));
                }
            }
        });

        grid.getStore().proxy.on('beforeload', function(p, params) {
            var dataInicial = Ext.getCmp("txtFiltroDataInicial").getValue();
            var dataFinal = Ext.getCmp("txtFiltroDataFinal").getValue();
            var chave = Ext.getCmp("txtFiltroChave").getValue();
            var numero = Ext.getCmp("txtFiltroNumero").getValue();
            var serie = Ext.getCmp("txtFiltroSerie").getValue();
            var prefixo = Ext.getCmp("txtFiltroPrefixo").getValue();
            var os = Ext.getCmp("txtFiltroOS").getValue();
            var origem = Ext.getCmp("filtro-Ori").getValue();
            var destino = Ext.getCmp("filtro-Dest").getValue();

            var ddlStatus = Ext.getCmp("ddlStatus");
            var situacao = (ddlStatus.getValue() == "Todos" || ddlStatus.getValue() == "0") ? "" : ddlStatus.getValue();

            params['filter[0].Campo'] = 'dataInicial';
            params['filter[0].Valor'] = new Array(dataInicial.format('d/m/Y') + " " + dataInicial.format('H:i:s'));
            params['filter[0].FormaPesquisa'] = 'Start';

            params['filter[1].Campo'] = 'dataFinal';
            params['filter[1].Valor'] = new Array(dataFinal.format('d/m/Y') + " " + dataFinal.format('H:i:s'));
            params['filter[1].FormaPesquisa'] = 'Start';

            params['filter[2].Campo'] = 'serie';
            params['filter[2].Valor'] = serie;
            params['filter[2].FormaPesquisa'] = 'Start';

            params['filter[3].Campo'] = 'chave';
            params['filter[3].Valor'] = chave;
            params['filter[3].FormaPesquisa'] = 'Start';

            params['filter[4].Campo'] = 'numero';
            params['filter[4].Valor'] = numero;
            params['filter[4].FormaPesquisa'] = 'Start';

            params['filter[5].Campo'] = 'prefixo';
            params['filter[5].Valor'] = prefixo;
            params['filter[5].FormaPesquisa'] = 'Start';

            params['filter[6].Campo'] = 'os';
            params['filter[6].Valor'] = os;
            params['filter[6].FormaPesquisa'] = 'Start';

            params['filter[7].Campo'] = 'origem';
            params['filter[7].Valor'] = origem;
            params['filter[7].FormaPesquisa'] = 'Start';

            params['filter[8].Campo'] = 'destino';
            params['filter[8].Valor'] = destino;
            params['filter[8].FormaPesquisa'] = 'Start';

            params['filter[9].Campo'] = 'situacao';
            params['filter[9].Valor'] = situacao;
            params['filter[9].FormaPesquisa'] = 'Start';

        });

        /************************* FILTROS **************************/
        var txtFiltroDataInicial = {
            xtype: 'datefield',
            fieldLabel: 'Data Inicial',
            id: 'txtFiltroDataInicial',
            name: 'txtFiltroDataInicial',
            width: 90,
            allowBlank: false,
            vtype: 'daterange',
            endDateField: 'txtFiltroDataFinal',
            value: new Date(),
            enableKeyEvents: true,
            listeners: {
                specialkey: TratarTeclaEnter
            }
        };

        var txtFiltroDataFinal = {
            xtype: 'datefield',
            fieldLabel: 'Data Final',
            id: 'txtFiltroDataFinal',
            name: 'txtFiltroDataFinal',
            width: 90,
            allowBlank: false,
            vtype: 'daterange',
            startDateField: 'txtFiltroDataInicial',
            value: new Date(),
            enableKeyEvents: true,
            listeners: {
                specialkey: TratarTeclaEnter
            }
        };

        var txtFiltroOS = {
            xtype: 'textfield',
            autoCreate: { tag: 'input', type: 'text', maxlength: '10', autocomplete: 'off' },
            width: 60,
            maskRe: /[0-9]/,
            name: 'txtFiltroOS',
            allowBlank: true,
            id: 'txtFiltroOS',
            fieldLabel: 'OS',
            enableKeyEvents: true,
            listeners: {
                specialkey: TratarTeclaEnter
            }
        };

        var txtFiltroPrefixo = {
            xtype: 'textfield',
            style: 'text-transform: uppercase',
            autoCreate: { tag: 'input', type: 'text', maxlength: '3', autocomplete: 'off' },
            maskRe: /[a-zA-Z0-9]/,
            width: 60,
            name: 'txtFiltroPrefixo',
            allowBlank: true,
            id: 'txtFiltroPrefixo',
            fieldLabel: 'Prefixo',
            enableKeyEvents: true,
            listeners: {
                specialkey: TratarTeclaEnter
            }
        };

        var txtFiltroChave = {
            xtype: 'textfield',
            maskRe: /[0-9]/,
            autoCreate: { tag: 'input', type: 'text', maxlength: '44', autocomplete: 'off' },
            width: 280,
            name: 'txtFiltroChave',
            allowBlank: true,
            id: 'txtFiltroChave',
            fieldLabel: 'Chave da MDFe',
            enableKeyEvents: true,
            listeners: {
                specialkey: TratarTeclaEnter
            }
        };

        var txtOrigem = {
            xtype: 'textfield',
            vtype: 'cteestacaovtype',
            style: 'text-transform: uppercase',
            id: 'filtro-Ori',
            fieldLabel: 'Origem',
            autoCreate: { tag: 'input', type: 'text', maxlength: '3', autocomplete: 'off' },
            name: 'Origem',
            allowBlank: true,
            maxLength: 3,
            width: 35,
            hiddenName: 'Origem'
        };

        var txtDestino = {
            xtype: 'textfield',
            vtype: 'cteestacaovtype',
            style: 'text-transform: uppercase',
            id: 'filtro-Dest',
            fieldLabel: 'Destino',
            autoCreate: { tag: 'input', type: 'text', maxlength: '3', autocomplete: 'off' },
            name: 'Destino',
            allowBlank: true,
            maxLength: 3,
            width: 35,
            hiddenName: 'Destino'
        };
        var txtFiltroSerie = {
            xtype: 'textfield',
            maskRe: /[0-9]/,
            autoCreate: { tag: 'input', type: 'text', maxlength: '3', autocomplete: 'off' },
            width: 40,
            name: 'txtFiltroSerie',
            allowBlank: true,
            id: 'txtFiltroSerie',
            fieldLabel: 'Série',
            enableKeyEvents: true,
            listeners: {
                specialkey: TratarTeclaEnter
            }
        };

        var txtFiltroNumero = {
            xtype: 'textfield',
            maskRe: /[0-9]/,
            autoCreate: { tag: 'input', type: 'text', maxlength: '9', autocomplete: 'off' },
            width: 70,
            name: 'txtFiltroNumero',
            allowBlank: true,
            id: 'txtFiltroNumero',
            fieldLabel: 'Número',
            enableKeyEvents: true,
            listeners: {
                specialkey: TratarTeclaEnter
            }
        };

        var situacaoStore = new Ext.data.ArrayStore({
            id: 'situacaoStore',
            fields: ['Id', 'Descricao'],
            data: [['T', 'Todos'], 
                ['PAE', 'Envio pendente'], 
                ['PAC', 'Cancelamento pendente'], 
                ['PAI', 'Inutilização pendente'], 
                ['EFA', 'Está na fila de envio'],
                ['EFC', 'Está na fila de cancelamento'],
                ['EFE', 'Está na fila de encerramento'],
                ['EAE', 'Enviado arquivo de envio'],
                ['EAF', 'Enviado arquivo de encerramento'],
                ['EAC', 'Enviado arquivo de cancelamento'],
                ['AUT', 'Autorizado'],
                ['CAN', 'Cancelado'],
                ['ENC', 'Encerrado'],
                ['ERR', 'Erro'],
                ['EAR', 'Autorizado reenvio'],
                ['INV', 'Inválido'],
                ['UND', 'Uso denegado'],
                ['AGC', 'Aguardando cancelamento'],
                ['AGE', 'Aguardando encerramento'],
                ['AME', 'Aguardando encerramento'],
                ['AGC', 'Cancelamento autorizado'],
                ['PGC', 'Geração da chave pendente'],
                ['MEP', 'Em processamento']]
        });

        var ddlStatus = new Ext.form.ComboBox({
            editable: true,
            typeAhead: true,
            forceSelection: true,
            disableKeyFilter: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            store: situacaoStore,
            valueField: 'Id',
            displayField: 'Descricao',
            fieldLabel: 'Situação',
            id: 'ddlStatus',
            width: 220,
            value: 'Todos'
        });

        var btnForcarGeracaoMdfe = {
            name: 'btnForcarGeracaoMdfe',
            id: 'btnForcarGeracaoMdfe',
            text: 'Forçar Geração MDF-e',
            handler: ForcarGeracaoMdfe
        };

        var btnPesquisar = {
            name: 'btnPesquisar',
            id: 'btnPesquisar',
            text: 'Pesquisar',
            iconCls: 'icon-find',
            handler: CarregarGrid
        };

        var btnRelatorioMDFeExcel = {
            name: 'btnRelatorioMDFe',
            id: 'btnRelatorioMDFe',
            text: 'Relatorio MDFe',
            tooltip: 'Exporta para Excel Relatório Completo de MDFe´s',
            iconCls: 'icon-page-excel',
            handler: RelatorioMDFe
        };


        var btnLimpar = {
            name: 'btnLimpar',
            id: 'btnLimpar',
            text: 'Limpar',
            handler: Limpar
        };


        var btnExportar = {
            id: 'btnExportarExcel',
            name: 'btnExportarExcel',
            text: 'Exportar para Excel',
            tooltip: 'Exportar para Excel',
            iconCls: 'icon-page-excel',
            handler: ExportarExcel
        };

        var coluna1Linha1 = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding-right:10px',
            items: [txtFiltroDataInicial]
        };

        var coluna2Linha1 = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding-right:10px',
            items: [txtFiltroDataFinal]
        };

        var coluna3Linha1 = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding-right:10px',
            items: [txtFiltroPrefixo]
        };

        var coluna4Linha1 = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding-right:10px',
            items: [txtFiltroOS]
        };

        var coluna5Linha1 = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding-right:10px',
            items: [txtOrigem]
        };

        var coluna6Linha1 = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding-right:10px',
            items: [txtDestino]
        };

        var linha1 = {
            layout: 'column',
            border: false,

            items: [coluna1Linha1, coluna2Linha1, coluna3Linha1, coluna4Linha1, coluna5Linha1, coluna6Linha1]
        };

        var coluna1Linha2 = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding-right:10px',
            items: [txtFiltroChave]
        };

        var coluna2Linha2 = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding-right:10px',
            items: [txtFiltroSerie]
        };

        var coluna3Linha2 = {
            layout: 'form',
            border: false,
            items: [txtFiltroNumero]
        };

        var coluna4Linha2 = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding-left:10px',
            width: 250,
            items: [ddlStatus]
        };

        var linha2 = {
            layout: 'column',
            border: false,
            items: [coluna1Linha2, coluna2Linha2, coluna3Linha2, coluna4Linha2]
        };


        var filtros = new Ext.form.FormPanel({
            id: 'filtros',
            title: 'Filtros',
            name: 'filtros',
            bodyStyle: 'padding: 10px',
            labelAlign: 'top',
            items: [linha1, linha2],
            buttonAlign: 'center',
            buttons: [btnForcarGeracaoMdfe, btnPesquisar, btnLimpar, btnExportar, btnRelatorioMDFeExcel]
        });

        /************************* DETALHES **************************/
        var formDetalhes;

        function MostrarDetalhes(idTrem) {
            formDetalhes = null;

            formDetalhes = new Ext.Window({
                name: 'formDetalhes',
                id: 'formDetalhes',
                title: "Detalhes MDFe",
                expandonShow: true,
                modal: true,
                width: 550,
                height: 520,
                closable: true,
                autoLoad:
                {
                    url: '<%= this.Url.Action("Detalhes") %>',
                    params: { idTrem: idTrem },
                    scripts: true
                }
            });

            formDetalhes.show();
        }

        /************************* FUNCOES DE APOIO **************************/
        function TratarTeclaEnter(f, e) {
            if (e.getKey() == e.ENTER) {
                CarregarGrid();
            }
        }

        function Limpar() {
            grid.getStore().removeAll();
            Ext.getCmp("filtros").getForm().reset();
        }

        function CarregarGrid() {
            grid.getStore().load();
        }

        function ExportarExcel() {

            var dataInicial = Ext.getCmp("txtFiltroDataInicial").getRawValue();
            if (dataInicial != "") {
                var dateParts = dataInicial.split("/");
                dataInicial = dateParts[1] + "/" + dateParts[0] + "/" + dateParts[2];
            }

            var dataFinal = Ext.getCmp("txtFiltroDataFinal").getRawValue();
            if (dataFinal != "") {
                var dateParts = dataFinal.split("/");
                dataFinal = dateParts[1] + "/" + dateParts[0] + "/" + dateParts[2];
            }

            var chave = Ext.getCmp("txtFiltroChave").getValue() == "null" ? "" : Ext.getCmp("txtFiltroChave").getValue();
            var numero = Ext.getCmp("txtFiltroNumero").getValue() == "null" ? "" : Ext.getCmp("txtFiltroNumero").getValue();
            var serie = Ext.getCmp("txtFiltroSerie").getValue() == "null" ? "" : Ext.getCmp("txtFiltroSerie").getValue();
            var prefixo = Ext.getCmp("txtFiltroPrefixo").getValue() == "null" ? "" : Ext.getCmp("txtFiltroPrefixo").getValue();
            var os = Ext.getCmp("txtFiltroOS").getValue() == "null" ? "" : Ext.getCmp("txtFiltroOS").getValue();
            var origem = Ext.getCmp("filtro-Ori").getValue() == "null" ? "" : Ext.getCmp("filtro-Ori").getValue();
            var destino = Ext.getCmp("filtro-Dest").getValue() == "null" ? "" : Ext.getCmp("filtro-Dest").getValue();

            var ddlStatus = Ext.getCmp("ddlStatus");
            var situacao = (ddlStatus.getValue() == "Todos" || ddlStatus.getValue() == "0") ? "" : ddlStatus.getValue();

            if (dataInicial == null &&
                dataFinal == null &&
                chave == "" &&
                numero == "" &&
                serie == "" &&
                prefixo == "" &&
                os == "" &&
                origem == "" &&
                destino == "") {
                alerta("Pelo menos um filtro deve ser fornecido");
                return false;
            }

            var url = "<%= this.Url.Action("ExportarExcel") %>";

            url += String.format("?dataInicial={0}", dataInicial);
            url += String.format("&dataFinal={0}", dataFinal);
            url += String.format("&chave={0}", chave);
            url += String.format("&numero={0}", numero);
            url += String.format("&serie={0}", serie);
            url += String.format("&prefixo={0}", prefixo);
            url += String.format("&os={0}", os);
            url += String.format("&origem={0}", origem);
            url += String.format("&destino={0}", destino);
            url += String.format("&situacao={0}", situacao);

            window.open(url, "");
        };

        // relatorio solicitado atraves da GDS 1909
        function RelatorioMDFe() {

            var erro = '';
            var chave = Ext.getCmp("txtFiltroChave").getValue() == "null" ? "" : Ext.getCmp("txtFiltroChave").getValue();
            var numero = Ext.getCmp("txtFiltroNumero").getValue() == "null" ? "" : Ext.getCmp("txtFiltroNumero").getValue();
            var serie = Ext.getCmp("txtFiltroSerie").getValue() == "null" ? "" : Ext.getCmp("txtFiltroSerie").getValue();
            var prefixo = Ext.getCmp("txtFiltroPrefixo").getValue() == "null" ? "" : Ext.getCmp("txtFiltroPrefixo").getValue();
            var os = Ext.getCmp("txtFiltroOS").getValue() == "null" ? "" : Ext.getCmp("txtFiltroOS").getValue();
            var origem = Ext.getCmp("filtro-Ori").getValue() == "null" ? "" : Ext.getCmp("filtro-Ori").getValue();
            var destino = Ext.getCmp("filtro-Dest").getValue() == "null" ? "" : Ext.getCmp("filtro-Dest").getValue();

            var ddlStatus = Ext.getCmp("ddlStatus");
            var situacao = (ddlStatus.getValue() == "Todos" || ddlStatus.getValue() == "0") ? "" : ddlStatus.getValue();

            var dataInicial = Ext.getCmp("txtFiltroDataInicial").getRawValue();
            if (dataInicial != "") {
                var dateParts = dataInicial.split("/");
                dataInicial = dateParts[1] + "/" + dateParts[0] + "/" + dateParts[2];
            }

            var dataFinal = Ext.getCmp("txtFiltroDataFinal").getRawValue();
            if (dataFinal != "") {
                var dateParts = dataFinal.split("/");
                dataFinal = dateParts[1] + "/" + dateParts[0] + "/" + dateParts[2];
            }

            if ((dataInicial == '__/__/____' || dataInicial == null) || (dataFinal == '__/__/____' || dataFinal ==  null)) {
                erro = "Data inicial e Data final são obrigatorios";
            }

            if (numero != '' && serie == '' || numero == '' && serie != '' ) {
                erro = "Quando número for preenchido série também deverá ser preenchido, e vice-versa";
            }

            //if (chave == "" && numero == "" && serie == "" && prefixo == "" && os == "" && origem == "" && destino == "") {
            //    erro ="Pelo menos um filtro deve ser informado";
            //}

            if (erro != '') {
                Ext.Msg.show({
                    title: "Filtro",
                    msg: erro,
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.ERROR,
                    minWidth: 200
                });
            }
            else {

                var url = "<%= this.Url.Action("ExportarRelatorioMDFe") %>";

                url += String.format("?dataInicial={0}", dataInicial);
                url += String.format("&dataFinal={0}", dataFinal);
                url += String.format("&chave={0}", chave);
                url += String.format("&numero={0}", numero);
                url += String.format("&serie={0}", serie);
                url += String.format("&prefixo={0}", prefixo);
                url += String.format("&os={0}", os);
                url += String.format("&origem={0}", origem);
                url += String.format("&destino={0}", destino);
                url += String.format("&situacao={0}", situacao);

                window.open(url, "");

            }

        };

        var txtOS = {
            xtype: 'textfield',
            autoCreate: { tag: 'input', type: 'text', maxlength: '10', autocomplete: 'off' },
            width: 300,
            maskRe: /[0-9]/,
            name: 'txtOS',
            allowBlank: true,
            id: 'txtOS',
            fieldLabel: 'OS',
            enableKeyEvents: true
        };

        var btnGerar = {
            name: 'btnGerar',
            id: 'btnGerar',
            text: 'Gerar MDFe',
            handler: function () {

                if (Ext.Msg.confirm("Gerar MDFe", "Você deseja realmente Gerar o MDF-e?", function(btn, text) {
                    if (btn == 'yes') {

                        if (Ext.getCmp("txtOS").getValue() == "") {

                            Ext.Msg.show({
                                title: "Mensagem de Erro",
                                msg: "Preencha o número da OS!",
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.ERROR,
                                minWidth: 200
                            });

                        } else {

                            Ext.Ajax.request({
                                url: "<% =Url.Action("GerarMdfeForcada") %>",
                                method: "POST",
                                params: { os: Ext.getCmp("txtOS").getValue() },
                                success: function(response) {
                                    var result = Ext.util.JSON.decode(response.responseText);

                                    if (result.success) {
                                        Ext.Msg.show({
                                            title: "Mensagem de Informação",
                                            msg: "Mdf-e gerado!",
                                            buttons: Ext.Msg.OK,
                                            minWidth: 200
                                        });
                                    } else {
                                        Ext.Msg.show({
                                            title: "Mensagem de Erro",
                                            msg: "Erro ao gerar o Mdf-e!</BR>" + result.Message,
                                            buttons: Ext.Msg.OK,
                                            icon: Ext.MessageBox.ERROR,
                                            minWidth: 200
                                        });
                                    }

                                    Ext.getCmp("txtOS").setValue("");
                                    gridLogPkgMdfe.getStore().load();
                                },
                                failure: function(conn, data) {
                                    Ext.Msg.show({
                                        title: "Mensagem de Erro",
                                        msg: "Erro ao gerar o Mdf-e!",
                                        buttons: Ext.Msg.OK,
                                        icon: Ext.MessageBox.ERROR,
                                        minWidth: 200
                                    });
                                }
                            });
                        }
                    }
                })) ;
            }
        };

        var linhaOS = new Ext.form.FormPanel
        ({
            border: false,
            labelAlign: 'right',
            bodyStyle: 'padding-left: 220px',
            labelWidth: 70,
            buttonAlign: "center",
            items: [txtOS],
            buttons: [btnGerar]
        });

        var gridLogPkgMdfe = new Translogic.PaginatedGrid({
            id: 'gridLogPkgMdfe',
            name: 'gridLogPkgMdfe',
            autoLoadGrid: false,
            height: 300,
            fields: [
                'Id',
                'Descricao',
                'DataHora',
                'MovimentacaoTrem',
                'Trem',
                'AreaOperacional',
                'Composicao'
            ],
            url: '<%= this.Url.Action("PesquisarLogPkg") %>',
            columns: [
                { dataIndex: "Id", hidden: true },
                { header: 'Descrição', width: 350, dataIndex: "Descricao", sortable: true },
                { header: 'Data/Hora', width: 150, dataIndex: "DataHora", sortable: true },
                { header: 'Movimentação Trem', width: 100, dataIndex: "MovimentacaoTrem", sortable: true },
                { header: 'Trem', width: 100, dataIndex: "Trem", sortable: true },
                { header: 'Área Operacional', width: 100, dataIndex: "AreaOperacional", sortable: true },
                { header: 'Composição', width: 100, dataIndex: "Composicao", sortable: true }
            ]
        });

        gridLogPkgMdfe.getStore().proxy.on('beforeload', function(p, params) {
            params['filter[0].Campo'] = 'tipoLog';
            params['filter[0].Valor'] = 'ERRO';
            params['filter[0].FormaPesquisa'] = 'Start';

            params['filter[1].Campo'] = 'nomeServico';
            params['filter[1].Valor'] = 'TELA';
            params['filter[1].FormaPesquisa'] = 'Start';
        });

        var formForcarGeracaoMdfe;

        function ForcarGeracaoMdfe() {

            if (formForcarGeracaoMdfe == null) {
                formForcarGeracaoMdfe = new Ext.Window({
                    name: 'formForcarGeracaoMdfe',
                    id: 'formForcarGeracaoMdfe',
                    title: "Forçar Geração MDF-e",
                    expandonShow: true,
                    modal: true,
                    width: 900,
                    height: 400,
                    closable: true,
                    closeAction: 'hide',
                    items: [linhaOS, gridLogPkgMdfe]
                });
            }

            formForcarGeracaoMdfe.show();
            gridLogPkgMdfe.getStore().load();
        }

        function VerificarPermissaoGerarMdfe() {
            if (<%= (!((bool)ViewData["PERMISSAO_GERAR"])).ToString().ToLower() %>) {
                Ext.getCmp("btnForcarGeracaoMdfe").hide();
            } else {
                Ext.getCmp("btnForcarGeracaoMdfe").show();
            }
        }

        /************************* RENDER **************************/
        Ext.onReady(function() {
            filtros.render(document.body);
            grid.render(document.body);
            VerificarPermissaoGerarMdfe();
        });

    </script>
</asp:Content>
<asp:Content runat="server" ID="Content2" ContentPlaceHolderID="Content">
    <div id="header-content">
        <h1>
            MDFe - Monitoramento</h1>
        <small>Você está em MDFe > Monitoramento</small>
        <br />
    </div>
</asp:Content>
