﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%@ Import Namespace="Translogic.Modules.Core.Domain.Model.Diversos.Cte" %>
<%@ Import Namespace="Translogic.Modules.Core.Domain.Model.Dto" %>
<%@ Import Namespace="Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes" %>
<%@ Import Namespace="Translogic.Modules.Core.Domain.Model.FluxosComerciais.TriangulusConfig" %>
<%
    Mdfe mdfe = (Mdfe)ViewData["MDFE"];
    IList<MdfeComposicaoDto> mdfeComposicao = (IList<MdfeComposicaoDto>)ViewData["MDFE_COMPOSICAO"];
    Nfe03Filial filial = (Nfe03Filial)ViewData["FILIAL"];
    string srcLogo = ViewData["LOGO"] as string;    
%>
<div style="page-break-after: always;">
    <div>
        <table style="width: 90%; border: 1px solid #666; border-collapse: collapse; font-family: Arial;
            font-size: 10px; height: 120px; ">
            <tr>
                <td rowspan="10" width="130px">
                    <img src='<%=srcLogo%>' />,
                </td>
                <td colspan="4" style="font-family: Arial-Bold; font-size: 17px;">
                    <%=filial.RazSoc%>
                </td>
                <td style="border-width: 0px 1px 0px 1px">
                    &nbsp;
                </td>
                <td style="font-family: 'Arial-Bold'; font-size: 17px; border-width: 0px 0px 1px 0px">
                    MDFE
                </td>
                <td colspan="4" style="border-width: 0px 0px 1px 0px;">
                    Relatório dos Conhecimentos de Transporte Eletronicos vinculados ao MDFe.
                </td>
            </tr>
            <tr>
                <td style="width: 80px">
                    CNPJ:
                </td>
                <td style="width: 100px">
                    <%=filial.Cnpj %>
                </td>
                <td style="width: 10px">
                    IE:
                </td>
                <td style="width: 50px">
                    <%=filial.Ie %>
                </td>
                <td style="border-width: 0px 1px 0px 1px">
                    &nbsp;
                </td>
                <td style="border-width: 0px 1px 0px 0px;">
                    Série
                </td>
                <td style="border-width: 0px 1px 0px 0px;">
                    Número
                </td>
                <td style="border-width: 0px 1px 0px 0px;" colspan="2">
                    Dt/Hr Emissão
                </td>
                <td style="border-width: 0px 1px 0px 0px;">
                    UF Carreg.
                </td>
            </tr>
            <tr>
                <td>
                    Razão Social:
                </td>
                <td colspan="3">
                    <%=filial.RazSoc %>
                </td>
                <td style="border-width: 0px 1px 0px 1px">
                    &nbsp;
                </td>
                <td style="border-width: 0px 1px 1px 0px;">
                    <%=mdfe.Serie%>
                </td>
                <td style="border-width: 0px 1px 1px 0px;">
                    <%=mdfe.Numero %>
                </td>
                <td style="border-width: 0px 1px 1px 0px;" colspan="2">
                    <%=mdfe.DataHora %>
                </td>
                <td style="border-width: 0px 1px 1px 0px;">
                    <%=mdfe.SiglaUfFerrovia%>
                </td>
            </tr>
            <tr>
                <td>
                    Logradouro:
                </td>
                <td colspan="3">
                    <%=filial.Endereco %>
                </td>
                <td style="border-width: 0px 1px 0px 1px">
                    &nbsp;
                </td>
                <td colspan="5">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    Complemento:
                </td>
                <td colspan="3">
                    <%=filial.Compl %>
                </td>
                <td style="border-width: 0px 1px 0px 1px">
                    &nbsp;
                </td>
                <td colspan="5">
                    CHAVE DE ACESSO
                </td>
            </tr>
            <tr>
                <td>
                    Bairro:
                </td>
                <td colspan="3">
                    <%=filial.Bairro %>
                </td>
                <td style="border-width: 0px 1px 0px 1px">
                    &nbsp;
                </td>
                <td style="border-width: 0px 0px 1px 0px;" colspan="5">
                    <%=mdfe.Chave%>
                </td>
            </tr>
            <tr>
                <td>
                    UF:
                    <%=filial.Uf %>
                </td>
                <td colspan="3">
                    CEP: &nbsp;&nbsp;
                    <%=filial.Cep %>
                </td>
                <td style="border-width: 0px 1px 0px 1px">
                    &nbsp;
                </td>
                <td colspan="5">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    Município:
                </td>
                <td colspan="3">
                    <%=filial.Cidade %>
                </td>
                <td style="border-width: 0px 1px 0px 1px">
                    &nbsp;
                </td>
                <td style="border-width: 0px 0px 1px 0px;" colspan="5">
                    Informações da composição do trem:
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    &nbsp;
                </td>
                <td style="border-width: 0px 1px 0px 1px">
                    &nbsp;
                </td>
                <td style="border-width: 0px 1px 0px 0px;">
                    Prefixo
                </td>
                <td style="border-width: 0px 1px 0px 0px;">
                    Data e hora
                </td>
                <td style="border-width: 0px 1px 0px 0px;">
                    Origem
                </td>
                <td style="border-width: 0px 1px 0px 0px;">
                    Destino
                </td>
                <td style="border-width: 0px 0px 0px 0px;">
                    Qtde de CTes
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    &nbsp;
                </td>
                <td style="border-width: 0px 1px 0px 1px">
                    &nbsp;
                </td>
                <td style="border-width: 0px 1px 0px 0px;">
                    <%=mdfe.Trem.Prefixo %>
                </td>
                <td style="border-width: 0px 1px 0px 0px;">
                    <%=mdfe.Trem.DataLiberacao %>
                </td>
                <td style="border-width: 0px 1px 0px 0px;">
                    <%=mdfe.Trem.Origem.Codigo %>
                </td>
                <td style="border-width: 0px 1px 0px 0px;">
                    <%=mdfe.Trem.Destino.Codigo %>
                </td>
                <td style="border-width: 0px 0px 0px 0px;">
                    <%= mdfeComposicao.Where(m => m.IdCte != 0).Count()%>
                </td>
            </tr>
        </table>
    </div>
    <div>
        <table style="border: 1px solid #666; border-collapse: collapse; text-align: center;
            width: 90%; font-family: Arial; font-size: 10px;">
            <tr style="font-family: 'Arial-Bold'; font-size: 12px;">
                <td>
                    Seq.
                </td>
                <td>
                    Nº Vagão
                </td>
                <td>
                    Série
                </td>
                <td>
                    Nº CTe
                </td>
                <td>
                    Situação Cte
                </td>
                <td>
                    Notas Cte
                </td>
            </tr>
            <% foreach (var comp in mdfeComposicao.Where(m => m.IdCte != 0).OrderBy(x => x.Sequencia).ToList())
               { %>
            <tr>
                <td>
                    <%=comp.Sequencia %>
                </td>
                <td>
                    <%=comp.CodigoVagao %>
                </td>
                <td>
                    <%= !string.IsNullOrEmpty(comp.SerieCte) ? comp.SerieCte.PadLeft(3, '0') : string.Empty%>
                </td>
                <td>
                    <%= !string.IsNullOrEmpty(comp.NumeroCte) ? comp.NumeroCte : string.Empty%>
                </td>
                <td>
                    <%= !string.IsNullOrEmpty(comp.SituacaoCte) ? comp.SituacaoCte : string.Empty%>
                </td>
                <td>
                    <%=comp.Notas.Count > 0 ? comp.Notas.Select(a => string.Concat(a.SerieNota, "-", a.NumeroNota)).Aggregate((current, next) => current + ", " + next) : string.Empty %>
                </td>
            </tr>
            <% } %>
        </table>
    </div>
</div>
