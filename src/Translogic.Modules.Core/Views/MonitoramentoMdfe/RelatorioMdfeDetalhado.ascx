﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%@ Import Namespace="Translogic.Modules.Core.Domain.Model.Diversos.Cte" %>
<%@ Import Namespace="Translogic.Modules.Core.Domain.Model.Dto" %>
<%@ Import Namespace="Translogic.Modules.Core.Domain.Model.Estrutura" %>
<%@ Import Namespace="Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico" %>
<%@ Import Namespace="Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes" %>
<%@ Import Namespace="Translogic.Modules.Core.Domain.Model.FluxosComerciais.TriangulusConfig" %>
<div>
    <%
        Mdfe mdfe = (Mdfe)ViewData["MDFE"];
        IList<MdfeComposicaoDto> mdfeComposicao = (IList<MdfeComposicaoDto>)ViewData["MDFE_COMPOSICAO"];
        Nfe03Filial filial = (Nfe03Filial)ViewData["FILIAL"];
        string srcLogo = ViewData["LOGO"] as string;
        IList<CteEmpresas> recebedores = ViewData["RECEBEDORES"] as IList<CteEmpresas>;
        List<IEmpresa> EmpresasRecebedoras = recebedores.Select(r => r.EmpresaRecebedor).Distinct().ToList();
        int qtdeRecebedor = EmpresasRecebedoras.Count();

        for (int i = 0; i < qtdeRecebedor; i++)
        {
    %>
    <div style="page-break-after: always;">
        <table style="width: 600px; font-family: Arial; font-size: 10px; height: 120px">
                <tr>
                    <td style="text-align: center; font-size: 15px;" colspan="2">
                        <b>Aviso de Chegada de Vagões</b>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center; font-size: 15px;" colspan="2">
                        <%=DateTime.Now.ToString() %>
                    </td>
                </tr>
                <tr>
                    <td>
                        Remetente:
                    </td>
                    <td>
                       <%= recebedores[0].EmpresaRemetente.DescricaoDetalhada%>
                    </td>
                </tr>
                <tr>
                    <td>
                        Consignatário:
                    </td>
                    <td>
                        <%= recebedores[0].EmpresaRecebedor.DescricaoDetalhada%>
                    </td>
                </tr>
                <tr>
                    <td>
                        Mercadoria:
                    </td>
                    <td>
                        OLEOS DIESEL EM VAGOES TANQUE (PERIGOSO)
                    </td>
                </tr>
                <tr>
                    <td>
                        Origem:
                    </td>
                    <td>
                        <%=mdfe.Trem.Origem.Codigo %>
                    </td>
                </tr>
                <tr>
                    <td>
                        pref - os:
                    </td>
                    <td>
                        <%=mdfe.Trem.Prefixo %>-<%=mdfe.Trem.OrdemServico.Numero %> 
                    </td>
                </tr>
            </table>
        <table style="border: 1px solid #666; border-collapse: collapse; text-align: center;
            width: 90%; font-family: Arial; font-size: 10px;">
            <tr style="font-family: 'Arial-Bold'; font-size: 12px;">
                <td>
                    Nº Vagão
                </td>
                <td>
                    Série CTe
                </td>
                <td>
                    Nº CTe
                </td>
                <td>
                    NF
                </td>
                <td>
                    Peso NF
                </td>
                <td>
                    Peso Rateio NF
                </td>
                <td>
                    Peso Vagão
                </td>
            </tr>
            <% foreach (var comp in mdfeComposicao.Where(m => m.IdCte != 0 && recebedores.Any(r => r.Cte.Id == m.IdCte && r.EmpresaRecebedor.Equals(recebedores[0].EmpresaRecebedor))).OrderBy(x => x.Sequencia).ToList())
               { %>
            <tr>
                <td>
                    <%=comp.CodigoVagao %>
                </td>
                <td>
                    <%= !string.IsNullOrEmpty(comp.SerieCte) ? comp.SerieCte.PadLeft(3, '0') : string.Empty%>
                </td>
                <td>
                    <%= !string.IsNullOrEmpty(comp.NumeroCte) ? comp.NumeroCte : string.Empty%>
                </td>
                <td>
                    <%=comp.Notas.Count > 0 ? string.Concat(comp.Notas[0].SerieNota,"-",comp.Notas[0].NumeroNota) : string.Empty %>
                </td>
                <td>
                    <%=comp.Notas.Count > 0 ? comp.Notas[0].PesoTotalNF.ToString("N3") : string.Empty %>
                </td>
                <td>
                    <%=comp.Notas.Count > 0 ? comp.Notas[0].PesoRateio.ToString("N3") : string.Empty %>
                </td>
                <td>
                    <%=comp.PesoVagao.ToString("N3")%>
                </td>
            </tr>
            <% if (comp.Notas.Count > 1)
               {
                   for (int j = 1; j < comp.Notas.Count; j++)
                   {
            %>
            <tr>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                    <%=comp.Notas.Count > 0 ? string.Concat(comp.Notas[j].SerieNota,"-",comp.Notas[j].NumeroNota) : string.Empty %>
                </td>
                <td>
                    <%=comp.Notas.Count > 0 ? comp.Notas[j].PesoTotalNF.ToString("N3") : string.Empty %>
                </td>
                <td>
                    <%=comp.Notas.Count > 0 ? comp.Notas[j].PesoRateio.ToString("N3") : string.Empty %>
                </td>
                <td>
                </td>
            </tr>
            <% }
               } %>
            <% } %>
        </table>
    </div>
    <% } %>
</div>
