﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Interna.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content runat="server" ID="Content2" ContentPlaceHolderID="Content">
    <div id="header-content">
        <h1>
            Despacho - Monitoramento erro</h1>
        <br />
    </div>
    <script type="text/javascript" language="javascript">

        /* region :: Functions */
        function RetornaSelecionados() {
            var results = {
                itens: undefined
            };

            results.itens = new Array();
            var selection = sm.getSelections();

            for (var i = 0; i < selection.length; i++) {

                results.itens.push(selection[i].data.Id);
            }

            return results;
        }

        function LiberarFaturamentoManual() {
            var selecionados = RetornaSelecionados();

            if (selecionados.itens.length == 0) {
                alert("Selecione um registro.");
                return;
            }

            $.ajax({
                type: "POST",
                data: { itens: selecionados.itens },
                url: '<%= Url.Action("LiberarFaturamentoManual") %>',
                success: function (msg) {
                    alert('Dados atualizado com sucesso.');
                    Pesquisar();
                }
            });
        }

        function Exportar() {
            var dtInicial = Ext.getCmp("txtDataIni").getValue();
            var dtFinal = Ext.getCmp("txtDataFinal").getValue();
            var vagao = Ext.getCmp("txtVagao").getValue();
            var processado = Ext.getCmp("cbStatusProcessamento").getValue();
            var cliente = Ext.getCmp("cbCliente").getValue();
            var local = Ext.getCmp("cbLocal").getValue();
            var fluxo = Ext.getCmp("txtFluxo").getValue();

            var url = "<%= this.Url.Action("Exportar") %>";
            
            url += String.format("?dtInicial={0}", new Array(dtInicial.format('d/m/Y') + " " + dtInicial.format('H:i:s')));
            url += String.format("&dtFinal={0}", new Array(dtFinal.format('d/m/Y') + " " + dtFinal.format('H:i:s')));
            url += String.format("&vagao={0}", vagao);
            url += String.format("&processado={0}", processado);
            url += String.format("&cliente={0}", cliente);
            url += String.format("&local={0}", local);
            url += String.format("&fluxo={0}", fluxo);

            window.open(url, "");
        }

        function Pesquisar() {
            grid.getStore().load();
        }

        function Limpar() {
            grid.getStore().removeAll();
            Ext.getCmp("filtros").getForm().reset();
        }

        function ExibirImagem(statusProcessamento) {
            var html = "";
            switch (statusProcessamento) {
                case 1:
                    html = "<img src=\"/Get.efh?file={Translogic.Modules.Core;}/circle-red.png\" alt=\"Não processada\" />";
                    break;
                case 2:
                    html = "<img src=\"/Get.efh?file={Translogic.Modules.Core;}/circle-green.png\" alt=\"Processado\" />";
                    break;
                case 3:
                    html = "<img src=\"/Get.efh?file={Translogic.Modules.Core;}/circle-yellow.png\" alt=\"Pendente\" />";
                    break;
                case 4:
                    html = "<img src=\"/Get.efh?file={Translogic.Modules.Core;}/circle-orange.png\" alt=\"Fila Processamento\" />";
                    break;
            }

            return html;
        }


        var dsStatusProcessamento = new Ext.data.JsonStore({
            root: "Items",
            autoLoad: true,
            url: '<%= Url.Action("ObterStatusProcessamento") %>',
            fields: [
                'Id',
                'Descricao'
            ],
            listeners: {
                load: function (store, records) {
                    var combobox = Ext.getCmp("cbStatusProcessamento");
                    var rt = store.recordType;
                    store.insert(0, new rt({}));
                    combobox.setValue(1);
                }
            }
        });

        var dsCliente = new Ext.data.JsonStore({
            autoLoad: true,
            url: '<%= Url.Action("ObterClientes") %>',
            fields: [
                'CodigoEmpresa',
                'DescricaoEmpresa'
            ],
            listeners: {
                load: function (store, records) {
                    var rt = store.recordType;
                    store.insert(0, new rt({}));
                    var cbLocal = Ext.getCmp("cbLocal");
                    cbLocal.clearValue();
                    cbLocal.setDisabled(true);

                }
            }
        });

        var dsLocal = new Ext.data.JsonStore({
            autoDestroy: true,
            autoLoad: true,
            url: '<%= Url.Action("ObterLocal") %>',
            fields: ['IdTerminal', 'CodTerminal'],
            listeners: {
                load: function (store, records) {
                    var rt = store.recordType;
                    store.insert(0, new rt({}));

                }
            }
        });

        /* endregion :: Functions */

        /* region :: Filtros */

        var txtDataIni = {
            xtype: 'datefield',
            fieldLabel: 'Data Inicial',
            id: 'txtDataIni',
            name: 'txtDataIni',
            width: 85,
            allowBlank: false,
            value: new Date()
        };

        var txtDataFinal = {
            xtype: 'datefield',
            fieldLabel: 'Data Final',
            id: 'txtDataFinal',
            name: 'txtDataFinal',
            width: 85,
            allowBlank: false,
            value: new Date()
        };

        var txtVagao = {
            xtype: 'textfield',
            name: 'txtVagao',
            id: 'txtVagao',
            fieldLabel: 'Vagão',
            autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '256' },
            style: 'text-transform: uppercase',
            width: 85
        };

        var cbStatusProcessamento = new Ext.form.ComboBox({
            typeAhead: false,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            store: dsStatusProcessamento,
            valueField: 'Id',
            displayField: 'Descricao',
            fieldLabel: 'Status processamento',
            id: 'cbStatusProcessamento',
            width: 225
        });

        var cbLocal = new Ext.form.ComboBox({
            typeAhead: false,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            store: dsLocal,
            valueField: 'IdTerminal',
            displayField: 'CodTerminal',
            fieldLabel: 'Origem',
            id: 'cbLocal',
            width: 110
        });

        var cbClientes = {
            xtype: 'combo',
            id: 'cbCliente',
            name: 'cbCliente',
            forceSelection: false,
            store: dsCliente,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            typeAhead: false,
            displayField: 'DescricaoEmpresa',
            valueField: 'CodigoEmpresa',
            fieldLabel: 'Cliente 363',
            width: 110,
            listeners: {
                select: {
                    fn: function () {
                        var codigoEmpresa = this.getValue();
                        var cbLocal = Ext.getCmp("cbLocal");
                        var btnPesquisar = Ext.getCmp("btnPesquisar");
                        cbLocal.clearValue();
                        cbLocal.setDisabled(true);
                        btnPesquisar.setDisabled(true);
                        cbLocal.setValue("Carregando...");
                        dsLocal.load({ params: { codigoEmpresa: codigoEmpresa },
                            callback: function (options, success, response, records) {
                                if (success) {
                                    cbLocal.setValue("");
                                    cbLocal.setDisabled(false);
                                    btnPesquisar.setDisabled(false);
                                }
                            }
                        });
                    }
                }
            }
        };

        var txtFluxo = {
            xtype: 'textfield',
            fieldLabel: 'Fluxo',
            id: 'txtFluxo',
            name: 'txtFluxo',
            width: 85
        };

        var ItemDataInicio = {
            width: 100,
            layout: 'form',
            border: false,
            items: [txtDataIni]
        };

        var ItemDataFinal = {
            width: 100,
            layout: 'form',
            border: false,
            items: [txtDataFinal]
        };

        var ItemVagao = {
            width: 100,
            layout: 'form',
            border: false,
            items: [txtVagao]
        };

        var ItemStatusProcessamento = {
            width: 230,
            layout: 'form',
            border: false,
            items: [cbStatusProcessamento]
        };

        var ItemCliente = {
            width: 120,
            layout: 'form',
            border: false,
            items: [cbClientes]
        };

        var ItemLocal = {
            width: 120,
            layout: 'form',
            border: false,
            items: [cbLocal]
        };

        var ItemFluxo = {
            width: 120,
            layout: 'form',
            border: false,
            items: [txtFluxo]
        };

        var arrlinha1 = {
            layout: 'column',
            border: false,
            items: [ItemDataInicio, ItemDataFinal, ItemStatusProcessamento]
        };

        var arrlinha2 = {
            layout: 'column',
            border: false,
            items: [ItemVagao, ItemCliente, ItemLocal, ItemFluxo]
        };

        var arrCampos = new Array();
        arrCampos.push(arrlinha1);
        arrCampos.push(arrlinha2);

        var filtros = new Ext.form.FormPanel({
            id: 'filtros',
            title: 'Filtros',
            name: 'filtros',
            region: 'center',
            bodyStyle: 'padding: 15px',
            initEl: function (el) {
                this.el = Ext.get(el);
                this.id = this.el.id || Ext.id();
                if (this.standardSubmit) {
                    this.el.dom.action = this.url;
                } else {
                    this.el.on('submit', this.onSubmit, this);
                }
                this.el.addClass('x-form');
            },
            labelAlign: 'top',
            border: false,
            items: [arrCampos],
            buttonAlign: 'center',
            buttons: [
                {
                    name: 'btnPesquisar',
                    id: 'btnPesquisar',
                    text: 'Pesquisar',
                    iconCls: 'icon-find',
                    handler: Pesquisar
                },
                {
                    name: 'btnLimpar',
                    id: 'btnLimpar',
                    text: 'Limpar',
                    handler: Limpar
                },
                {
                    id: 'btnExportar',
                    name: 'btnExportar',
                    text: 'Exportar',
                    iconCls: 'icon-page-excel',
                    handler: Exportar
                }
            ]
        });

        var sm = new Ext.grid.CheckboxSelectionModel({
            listeners: {
                selectionchange: function (sm) {
                    var recLen = Ext.getCmp('grid').store.getRange().length;
                    var selectedLen = this.selections.items.length;
                    if (selectedLen == recLen) {
                        var view = Ext.getCmp('grid').getView();
                        var chkdiv = Ext.fly(view.innerHd).child(".x-grid3-hd-checker");
                        chkdiv.addClass("x-grid3-hd-checker-on");
                    }
                }
            }
            ,
            rowdeselect: function (sm, rowIndex, record) {
                var view = Ext.getCmp('grid').getView();
                var chkdiv = Ext.fly(view.innerHd).child(".x-grid3-hd-checker");
                chkdiv.removeClass('x-grid3-hd-checker-on');
            }
        });

        /* endregion :: Filtros */
        var cm = new Ext.grid.ColumnModel({
            defaults: {
                sortable: true
            },
            columns: [sm,
                        { dataIndex: "Id", hidden: true },
                        { header: 'Tela 363', dataIndex: 'PermiteManual', width: 70 },
                        { header: 'Série', dataIndex: 'Serie', width: 70 },
                        { header: 'Vagão', dataIndex: 'Vagao', width: 70 },
                        { header: 'Fluxo', dataIndex: 'Fluxo', width: 70 },
                        { header: 'MD-Múltiplos despachos', dataIndex: 'Md', width: 70 },
                        {
                            header: 'Status Processamento',
                            dataIndex: 'StatusProcessamento',
                            width: 65,
                            align: "center",
                            sortable: false,
                            renderer: function (value, metaData, record, rowIndex, colIndex, store) {
                                return ExibirImagem(record.data.StatusProcessamento);
                            }
                        },
                        { header: 'Tentativas', dataIndex: 'NumeroTentativas', width: 70 },
                        { header: 'Data próxima execução', dataIndex: 'ProximaExecucao', width: 70 },
                        { header: 'Obs', dataIndex: 'Observacao', width: 70 },
                        { header: 'Data', dataIndex: 'Data', width: 70 },
                        { header: 'Cliente 363', dataIndex: 'Cliente', width: 70 },
                        { header: 'Aprovado', dataIndex: 'Aprovado1380', width: 70 },
                        { header: 'Origem', dataIndex: 'Local', width: 70 },
                        { header: 'LocalAtual', dataIndex: 'LocalAtual', width: 70 },
                        { header: 'Situação', dataIndex: 'Situacao', width: 70 },
                        { header: 'Lotação', dataIndex: 'Lotacao', width: 70 },
                        { header: 'Localização', dataIndex: 'Localizacao', width: 70 },
                        { header: 'Condição de uso', dataIndex: 'CondicaoUso', width: 70 },
                        { header: 'Volume', dataIndex: 'Volume', width: 70 },
                        { header: 'Peso', dataIndex: 'PesoDespacho', width: 70 },
                        { header: 'Peso total vagão', dataIndex: 'PesoTotalVagao', width: 70 }
                ]
        });

        var btnLiberarFaturamentoManual = new Ext.Button({
            id: 'btnLiberarFaturamentoManual',
            name: 'btnLiberarFaturamentoManual',
            text: 'Liberar tela 363',
            iconCls: 'icon-go',
            handler: LiberarFaturamentoManual
        });

        var grid = new Translogic.PaginatedGrid({
            id: 'grid',
            name: 'grid',
            autoLoadGrid: false,
            region: 'center',
            viewConfig: { emptyText: 'Não possui dado(s) para exibição.' },
            filteringToolbar: [btnLiberarFaturamentoManual],
            loadMask: { msg: App.Resources.Web.Carregando },
            sm: sm,
            fields: [
                        'Id',
                        'PermiteManual',
                        'Vagao',
                        'Serie',
                        'Fluxo',
                        'Md',
                        'StatusProcessamento',
                        'NumeroTentativas',
                        'ProximaExecucao',
                        'Observacao',
                        'Data',
                        'Cliente',
                        'Aprovado1380',
                        'Local',
                        'LocalAtual',
                        'Situacao',
                        'Lotacao',
                        'Localizacao',
                        'CondicaoUso',
                        'Volume',
                        'PesoDespacho',
                        'PesoTotalVagao'
            ],
            cm: cm,
            stripeRows: true,
            url: '<%= Url.Action("Pesquisar") %>'
        });

        grid.getStore().proxy.on('beforeload', function (p, params) {
            var dtInicial = Ext.getCmp("txtDataIni").getValue();
            var dtFim = Ext.getCmp("txtDataFinal").getValue();
            
            params['dtInicial'] =  new Array(dtInicial.format('d/m/Y') + " " + dtInicial.format('H:i:s'));
            params['dtFinal'] = new Array(dtFim.format('d/m/Y') + " " + dtFim.format('H:i:s'));
            params['vagao'] = Ext.getCmp("txtVagao").getValue();
            params['processado'] = Ext.getCmp("cbStatusProcessamento").getValue();
            params['cliente'] = Ext.getCmp("cbCliente").getValue();
            params['local'] = Ext.getCmp("cbLocal").getValue();
            params['fluxo'] = Ext.getCmp("txtFluxo").getValue();
        });


        /* region :: Render */

        $(document).ready(function () {
            var panel = new Ext.Viewport({
                layout: 'border',
                margins: 10,
                items: [
			{
			    region: 'north',
			    height: 300,
			    autoScroll: true,
			    items: [{
			        region: 'center',
			        applyTo: 'header-content'
			    },
				filtros]
			},
			grid]
            });
        });
    </script>
</asp:Content>
