﻿<%@ Import Namespace="System.Security.Policy" %>

<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Interna.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content runat="server" ID="HeadContent" ContentPlaceHolderID="HeadContent">
	<script type="text/javascript">

	var formModal = null;
	var grid = null;
	var lastRowSelecteSelected = -1;

	var dsCodigoControle = new Ext.data.JsonStore({
        root: "Items",
        autoLoad: true,
        url: '<%= Url.Action("ObterCodigoSerieDesp") %>',
        fields: [
                    'Id',
                    'CodigoControle'
			    ]   		
    });

    // var dataAtual = new String('<%= String.Format("{0:dd/MM/yyyy}", DateTime.Now) %>');
		
		// Flag para garantir que uma tela de mensagem que está sobreposta não chame novamente a tela de pesquisa
		var executarPesquisa = true;
		function showResult(btn) {
			executarPesquisa = true;
		}

	function FormSuccess(form, action) {
		ds.removeAll();
		ds.loadData(action.result, true);
	}

	function Pesquisar(){
		var diferenca = Ext.getCmp("filtro-data-final").getValue() - Ext.getCmp("filtro-data-inicial").getValue(); 
		diferenca = Math.floor(diferenca / (1000 * 60 * 60 * 24)); 

		if (Ext.getCmp("filtro-data-inicial").getValue() == '' || Ext.getCmp("filtro-data-final").getValue() == '') {
			executarPesquisa = false;
			Ext.Msg.show({
				title: "Mensagem de Informação",
				msg: "Preencha os filtro datas!",
				buttons: Ext.Msg.OK,
				icon: Ext.MessageBox.INFO,
				minWidth: 200,
				fn: showResult
			});
		}
		else if (diferenca > 30) {
			executarPesquisa = false;
			Ext.Msg.show({
				title: "Mensagem de Informação",
				msg: "O período da pesquisa não deve ultrapassar 30 dias",
				buttons: Ext.Msg.OK,
				icon: Ext.MessageBox.INFO,
				minWidth: 200,
				fn: showResult
			});
		}
		else if  (Ext.getCmp("filtro-serie").getValue() != '' && Ext.getCmp("filtro-despacho").getValue() == '') {
			executarPesquisa = false;
			Ext.Msg.show({
				title: "Mensagem de Informação",
				msg: "Não é possível filtrar apenas pela a Série do Despacho.<br/>Também é necessário preencher o número do Despacho.",
				buttons: Ext.Msg.OK,
				icon: Ext.MessageBox.INFO,
				minWidth: 200,
				fn: showResult
			});
		}
		else if (Ext.getCmp("filtro-despacho").getValue() != '' && Ext.getCmp("filtro-serie").getValue() == '') {
			executarPesquisa = false;
			Ext.Msg.show({
				title: "Mensagem de Informação",
				msg: "Não é possível filtrar apenas pelo número do Despacho.<br/>Também é necessário preencher a Série do Despacho.",
				buttons: Ext.Msg.OK,
				icon: Ext.MessageBox.INFO,
				minWidth: 200,
				fn: showResult
			});
		}
		else {
		executarPesquisa = true;
			grid.getStore().load();
		}
	}

	function FormError(form, action) {
		Ext.Msg.alert('Mensagem de Erro', action.result.Message);
		ds.removeAll();
	}

	function InserirFluxoReferencia(listaEnvio){
		windowNotaFiscal = new Ext.Window({
			id: 'windowNotaFiscal',
			title: 'Editar dados da Nota Fiscal',
			modal: true,
			width: 444,
			height: 550,
			autoLoad: {
			url: '<%= Url.Action("FormFluxoReferencia") %>',
			scripts: true
			}
		});
		windowNotaFiscal.show();
	}

    var ds = new Ext.data.JsonStore({
        root: "Items",
        url: '<%= Url.Action("ObterCtes") %>',
        totalProperty: 'Total',
		remoteSort: true,
        paramNames: {
				sort: "pagination.Sort",
				dir: "pagination.Dir",
				start: "pagination.Start",
				limit: "pagination.Limit"
		},
        fields: [
                    'CteId',
                    'Fluxo',
                    'Origem',
                    'Destino',
                    'Mercadoria',
                    'ClienteFatura',
                    'Cte',
                     'Serie',
                    'Despacho',  
                    'DateEmissao',
                    'ValorComplemento',
                    'Complementado',
                    'ValorCte',
                    'ValorDiferencaComplemento',
                    'PesoVagao',
                    'CodigoUsuario',
                    'DataComplemento',
                    'DataCompl',
                    'CteComAgrupamentoNaoAutorizado',
                    'ValorTotal',
                    'FluxoReferencia'
			    ]
    });

/* ------------- Ação CTRL+C nos campos da Grid ------------- */
    var isCtrl = false;
    document.onkeyup = function (e) {

        var keyID = event.keyCode;

        // 17 = tecla CTRL
        if (keyID == 17) {
            isCtrl = false; //libera tecla CTRL
        }
    }

	document.onkeydown = function (e) {

			var keyID = event.keyCode;

		// verifica se CTRL esta acionado
		if (keyID == 17) {
			isCtrl = true;
		}

		if ((keyID == 13) && (executarPesquisa)) {
			Pesquisar();
			return;
		}

        // 67 = tecla 'c'
        if (keyID == 67 && isCtrl == true) {

            // verifica se há selecão na tabela
            if (grid.getSelectionModel().hasSelection()) {
                // captura todas as linhas selecionadas
                var recordList = grid.getSelectionModel().getSelections();

                var a = [];
                var separator = '\t'; // separador para colunas no excel
                for (var i = 0; i < recordList.length; i++) {
                    var s = '';
                    var item = recordList[i].data;
                    for (key in item) {
                        if (key != 'Id') { //elimina o campo id da linha
                            s = s + item[key] + separator;
                        }
                    }
                    s = s.substr(0, s.length - 1); //retira o ultimo separador '\t' da linha
                    a.push(s);
                }
                window.clipboardData.setData('Text', (a.join('\n'))); //insere linhas no clipBoard
            }
        }
    }
/* ------------------------------------------------------- */

    Contains = function(arr, obj) {
                for(var i = 0; i < arr.length; i++) {
                    if(arr[i] === obj) {
                        return true;
                    }
                }
                return false;
              }

    
    $(function () {

        sm2 = new Ext.grid.CheckboxSelectionModel({
            checkOnly : false,
            listeners: {
                selectionchange: function (sm) {
                    if (sm.getCount()) {
                        Ext.getCmp("salvar").enable();
                    } else {
                        Ext.getCmp("salvar").disable(); 
                    }
                },
                beforerowselect : function (sm, rowIndex, keep, record) {
                
                    if (record.data.Complementado || record.data.DataComplemento != ""){
//                      Ext.Msg.show({
//					                  title: "Mensagem de Informação",
//					                  msg: "Impossível selecionar o CTE!</BR>CTE já complementado!",
//					                  buttons: Ext.Msg.OK,
//					                  icon: Ext.MessageBox.INFO,
//					                  minWidth: 200
//				              });
//                      return false;
                        return true;
                    }
                    else if (record.data.CteComAgrupamentoNaoAutorizado){
                      Ext.Msg.show({
					                  title: "Mensagem de Informação",
					                  msg: "Impossível selecionar o CTE!</BR>CTE está com pendência de autorização!",
					                  buttons: Ext.Msg.OK,
					                  icon: Ext.MessageBox.INFO,
					                  minWidth: 200
				              });
                      return false;
                    }
                }
            }
        });

        campo = new Ext.form.FormPanel({
            id: 'grid-campo',
            bodyStyle: 'padding-left:21px',
            labelAlign: 'top',
            width: 250,
            border: false,
            items:
			[{
			    layout: 'column',
			    border: false,
			    items: [{
			        width: 110,
			        layout: 'form',
			        border: false,
			        items:
						[{
							xtype: 'masktextfield',
							fieldLabel: 'Valor Total',
							id: 'valor-total',
							name: 'valorTotal',
							allowBlank: false,
							width: 100,
							mask: '#9.999.990,00',
							money: true,
							maxLength: 13,
							value: "0,00", 
							hiddenName: 'valorTotal'
							}]
			    },
				{
					width: 100,
					layout: 'form',
					border: false,
					items:
					[{
						xtype: 'numberfield',
						id: 'fluxo-referencia',
						fieldLabel: 'Fluxo Referência',
						name: 'fluxoReferencia',
						allowBlank: false,
						maxLength: 20,
						width: 100,
						hiddenName: 'fluxoReferencia',
						style: 'text-align: left',
						maxValue: 99999,
						minValue: 0,
						autoCreate: { tag: 'input', type: 'text', maxlength: '5', autocomplete: 'off' }
					}]
				}]
			}],
				buttonAlign: "center",
				buttons: [{
					text: 'Salvar',
					name: 'salvar',
					id: 'salvar',
					iconCls: 'icon-save',
					disabled: true, 
					handler: function(b, e) {
						var listaSelecionados = sm2.getSelections();
						if (listaSelecionados.length > 10) {
							Ext.Msg.show({
								title: "Mensagem de Informação",
								msg: "Não é possível salvar mais que 10 registros!",
								buttons: Ext.Msg.OK,
								icon: Ext.MessageBox.INFO,
								minWidth: 200
							});
							return false;
						}

						var valorTotal = Ext.getCmp("valor-total").getValue();
						if (valorTotal == 0) {
							Ext.Msg.show({
								title: "Mensagem de Informação",
								msg: "Necessário preencher o valor total!",
								buttons: Ext.Msg.OK,
								icon: Ext.MessageBox.INFO,
								minWidth: 200
							});
							return false;
						}

						var fluxoReferencia = Ext.getCmp("fluxo-referencia").getValue();
						if (fluxoReferencia == ''){
							Ext.Msg.show({
								title: "Mensagem de Informação",
								msg: "Necessário preecher o Fluxo Referência!",
								buttons: Ext.Msg.OK,
								icon: Ext.MessageBox.INFO,
								minWidth: 200
							});
							return false;
						}

						for (var i = 0; i < listaSelecionados.length; i++) {
							listaSelecionados[i].data.ValorTotal = valorTotal;
							listaSelecionados[i].data.FluxoReferencia = fluxoReferencia;
						}

						var listaEnvio = Array();
						var registrosParaSeremSalvos = '';

						for (var i = 0; i < listaSelecionados.length; i++) {
						
							listaSelecionados[i].data.DataCompl = listaSelecionados[i].data.DataComplemento;
							listaSelecionados[i].data.DataComplemento = null;

							listaEnvio.push(listaSelecionados[i].data);

							registrosParaSeremSalvos += listaSelecionados[i].data.Cte;
							if (i != listaSelecionados.length-1)
							{
								registrosParaSeremSalvos += ', ';
							}
						}

						//// VALIDA SE OS CTES SELECIONADOS TEM O MESMO FLUXO /////////////
						var fluxo = '';
						var fluxoDiferente = false;

						if (listaSelecionados.length > 1)
						{
							fluxo = listaSelecionados[0].data.Fluxo;
							for (var i = 0; i < listaSelecionados.length; i++) {

								if (fluxo != listaSelecionados[i].data.Fluxo)
								{
									fluxoDiferente = true;
									break;
								}
							}
						}

						if (fluxoDiferente)
						{
							Ext.Msg.show({
								title: "Mensagem de Informação",
								msg: "Os CTEs selecionados devem ter o mesmo Fluxo!",
								buttons: Ext.Msg.OK,
								icon: Ext.MessageBox.INFO,
								minWidth: 200
							});
							return false;
						}
						
						////////////////////////////////////////////////////////////

						if (Ext.Msg.confirm("Confirmação", "Deseja realmente alterar os seguintes registros: <br/>" + registrosParaSeremSalvos + "?", function(btn, text) {
							if (btn == 'yes') {
								var dados = $.toJSON(listaEnvio);
								$.ajax({
									url: "<%= Url.Action("Salvar") %>",
									type: "POST",
									dataType: 'json',
									data: dados,
									contentType: "application/json; charset=utf-8",
									success: function(result) {
										
										if (result.success)
										{
											ds.load();
											// sucesso
											executarPesquisa = false;
											Ext.Msg.show({
												title: "Mensagem de Informação",
												msg: "Take Or Pay criado com sucesso.",
												buttons: Ext.Msg.OK,
												icon: Ext.MessageBox.INFO,
												minWidth: 200,
												fn: showResult
											});
											Ext.getCmp("valor-total").setValue('0,00');
											Ext.getCmp("fluxo-referencia").reset();
										}
										else {
											Ext.Msg.alert('Mensagem de Erro', result.Message);
										}
									},
									failure: function(result) {
										Ext.Msg.alert('Mensagem de Erro', result.Message);
									}
								});
							}
							else {
								return false;
							}

						})); 
					}
					}, {
    	            text: 'Cancelar',
    	            name: 'cancelar',
    	            id: 'cancelar',
    	            iconCls: 'icon-cancel',
    	            handler: function(b, e) {

                        var listaSelecionados = sm2.getSelections();

                        if (listaSelecionados.length != 0) {
    		                if (Ext.Msg.confirm("Confirmação", "A selecão dos registros será perdida.<br />Deseja Continuar?", function(btn, text) {
							            if (btn == 'yes') {
								            sm2.clearSelections();
						                }
					            }));
    		            }
                        else
                        {
                            Ext.Msg.show({
					            title: "Mensagem de Informação",
					            msg: "Nenhum registro selecionado!",
					            buttons: Ext.Msg.OK,
					            icon: Ext.MessageBox.INFO,
					            minWidth: 200
				            });
                        }
    	            }
                }
            ]

        });

        var pagingToolbar = new Ext.PagingToolbar({
				pageSize: 50,
				store: ds,
				displayInfo: true,
				displayMsg: App.Resources.Web.GridExibindoRegistros,
				emptyMsg: App.Resources.Web.GridSemRegistros,
				paramNames: {
					start: "pagination.Start",
					limit: "pagination.Limit"
				}
			});


        grid = new Ext.grid.GridPanel({
            viewConfig: {
                forceFit: true,
                getRowClass: MudaCor
            },
            stripeRows: true,
            region: 'center',
            store: ds,
            loadMask: { msg: App.Resources.Web.Carregando },
            colModel: new Ext.grid.ColumnModel({
                defaults: {
                    sortable: false
                },
               columns: [
						new Ext.grid.RowNumberer(),
                        sm2,
                        { dataIndex: "CteId", hidden: true },
						{ header: 'Fluxo', dataIndex: "Fluxo", sortable: false },
						{ header: 'Origem', dataIndex: "Origem", sortable: false },
						{ header: 'Destino', dataIndex: "Destino", sortable: false },
						{ header: 'Mercadoria', dataIndex: "Mercadoria", sortable: false },
						{ header: 'Cliente Fatura', dataIndex: "ClienteFatura", sortable: false },
						{ header: 'CTe', dataIndex: "Cte", sortable: false },
						{ header: 'Série', dataIndex: "Serie", sortable: false },
						{ header: 'Despacho', dataIndex: "Despacho", sortable: false },
						{ header: 'Data Emissão', dataIndex: "DateEmissao", sortable: false },
						{ header: 'Valor', dataIndex: "ValorCte", sortable: false, renderer: Ext.util.Format.numberRenderer('0.000,00/i') },
						{ header: 'Diferença', dataIndex: "ValorDiferencaComplemento", sortable: false, renderer: Ext.util.Format.numberRenderer('0.000,00/i') },
						{ header: 'Peso', dataIndex: "PesoVagao", sortable: false, renderer: Ext.util.Format.numberRenderer('0.000,00/i') },
						{ header: 'Data Complemento', dataIndex: "DataComplemento", sortable: false },
						{ dataIndex: "Complementado", hidden: true },
						{ dataIndex: "CteComAgrupamentoNaoAutorizado", hidden: true }
					]
            }),
            sm: sm2,
			listeners: {
				rowclick: function(grid, rowIndex, e) {
					if (sm2.isSelected(rowIndex)) {
						if (rowIndex == lastRowSelected) {
							grid.getSelectionModel().deselectRow(rowIndex);
							lastRowSelected = -1;
						}
						else {
							lastRowSelected = rowIndex;
						}
					}
				}
			},
            buttonAlign: 'center',
            bbar: pagingToolbar,
            fbar: [
                   campo
                  ]
        });

         grid.on('render', function () {
				if (grid.autoLoadGrid)
					grid.getStore().load({ params: { 'pagination.Start': 0, 'pagination.Limit': pagingToolbar.pageSize} });
			});

			grid.getStore().proxy.on('beforeload', function (p, params) {
            
				params['filter[0].Campo'] = 'dataInicial';
				params['filter[0].Valor'] = new Array(Ext.getCmp("filtro-data-inicial").getValue().format('d/m/Y') + " " + Ext.getCmp("filtro-data-inicial").getValue().format('H:i:s'));
				params['filter[0].FormaPesquisa'] = 'Start';

				params['filter[1].Campo'] = 'dataFinal';
				params['filter[1].Valor'] = new Array(Ext.getCmp("filtro-data-final").getValue().format('d/m/Y') + " " + Ext.getCmp("filtro-data-inicial").getValue().format('H:i:s'));
				params['filter[1].FormaPesquisa'] = 'Start';

				params['filter[2].Campo'] = 'serie';
				params['filter[2].Valor'] = Ext.getCmp("filtro-serie").getValue();
				params['filter[2].FormaPesquisa'] = 'Start';

				params['filter[3].Campo'] = 'despacho';
				params['filter[3].Valor'] = Ext.getCmp("filtro-despacho").getValue();
				params['filter[3].FormaPesquisa'] = 'Start';

				params['filter[4].Campo'] = 'fluxo';
				params['filter[4].Valor'] = Ext.getCmp("filtro-fluxo").getValue();
				params['filter[4].FormaPesquisa'] = 'Start';

				params['filter[5].Campo'] = 'chave';
				params['filter[5].Valor'] = Ext.getCmp("filtro-Chave").getValue();
				params['filter[5].FormaPesquisa'] = 'Start';

				params['filter[6].Campo'] = 'Origem';
				params['filter[6].Valor'] = Ext.getCmp("filtro-Ori").getValue();
				params['filter[6].FormaPesquisa'] = 'Start';

				params['filter[7].Campo'] = 'Destino';
				params['filter[7].Valor'] = Ext.getCmp("filtro-Dest").getValue();
				params['filter[7].FormaPesquisa'] = 'Start';

				params['filter[8].Campo'] = 'Vagao';
				params['filter[8].Valor'] = Ext.getCmp("filtro-numVagao").getValue();
				params['filter[8].FormaPesquisa'] = 'Start'

			    params['filter[9].Campo'] = 'UfDcl';
				params['filter[9].Valor'] = Ext.getCmp("filtro-UfDcl").getRawValue();
				params['filter[9].FormaPesquisa'] = 'Start';            
			});

        function MudaCor(row, index) {
              if (row.data.Complementado || row.data.DataComplemento != "" || row.data.CteComAgrupamentoNaoAutorizado) {
                return 'cor';
            }
        }

        var dataAtual = new Date();
		
		var dataInicial =	{
			xtype: 'datefield',
			fieldLabel: 'Data Inicial',
			id: 'filtro-data-inicial',
			name: 'dataInicial',
			width: 100,
			allowBlank: false,
			vtype: 'daterange',
			endDateField: 'filtro-data-final',
			hiddenName: 'dataInicial',
			value: dataAtual		
		};
	
		var dataFinal = {
			xtype: 'datefield',
			fieldLabel: 'Data Final',
			id: 'filtro-data-final',
			name: 'dataFinal',
			width: 100,
			allowBlank: false,
			vtype: 'daterange',
			startDateField: 'filtro-data-inicial',
			hiddenName: 'dataFinal',
			value: dataAtual
		};

		var origem = {
			xtype: 'textfield',
			vtype: 'cteestacaovtype',
			style: 'text-transform: uppercase',
			id: 'filtro-Ori',
			fieldLabel: 'Origem',
			name: 'Origem',
			allowBlank: true,
			maxLength: 3,
			width: 50,
			hiddenName: 'Origem',
			autoCreate: { tag: 'input', type: 'text', maxlength: '3', autocomplete: 'off' }
		};

		var cboCteCodigoControle = {
			xtype: 'combo',
			store: dsCodigoControle,
			allowBlank: true,
			lazyInit: false,
			lazyRender: false, 
			mode: 'local',
			typeAhead: false,
			triggerAction: 'all',
			fieldLabel: 'UF DCL',
			name: 'filtro-UfDcl',
			id: 'filtro-UfDcl',
			hiddenName: 'filtro-UfDcl',
			displayField: 'CodigoControle',
			forceSelection: true,
			width: 70,
			valueField: 'Id',
			emptyText: 'Selecione...',
			editable: false,
			tpl : '<tpl for="."><div class="x-combo-list-item">{CodigoControle}&nbsp;</div></tpl>'		
		};

		var destino = {
			xtype: 'textfield',
			vtype: 'cteestacaovtype',
			style: 'text-transform: uppercase',
			id: 'filtro-Dest',
			fieldLabel: 'Destino',
			name: 'Destino',
			allowBlank: true,
			maxLength: 3,
			width: 50,
			hiddenName: 'Destino',
			autoCreate: { tag: 'input', type: 'text', maxlength: '3', autocomplete: 'off' }
		};

		var serie = {
			xtype: 'textfield',
			vtype: 'ctesdvtype',
			style: 'text-transform: uppercase',
			id: 'filtro-serie',
			fieldLabel: 'Série',
			name: 'serie',
			allowBlank: true,
			maxLength: 20,
			width: 50,
			hiddenName: 'serie',
			autoCreate: { tag: 'input', type: 'text', maxlength: '3', autocomplete: 'off' }
		};

		var despacho = {
			xtype: 'textfield',
			vtype: 'ctedespachovtype',
			id: 'filtro-despacho',
			fieldLabel: 'Despacho',
			name: 'despacho',
			allowBlank: true,
			maxLength: 20,
			width: 80,
			hiddenName: 'despacho',
			autoCreate: { tag: 'input', type: 'text', maxlength: '6', autocomplete: 'off' }
		};

		var fluxo = {
			xtype: 'textfield',
			vtype: 'ctefluxovtype',
			style: 'text-transform: uppercase',
			id: 'filtro-fluxo',
			fieldLabel: 'Fluxo',
			name: 'fluxo',
			allowBlank: true,
			autoCreate: { tag: 'input', type: 'text', maxlength: '5', autocomplete: 'off' },
			maxLength: 20,
			width: 60,
			minValue: 0,
			maxValue: 99999,
			hiddenName: 'fluxo'
		};

		var Vagao = {
			xtype: 'textfield',
			vtype: 'ctevagaovtype',
			style: 'text-transform: uppercase',
			id: 'filtro-numVagao',
			fieldLabel: 'Num. Vagão',
			name: 'numVagao',
			allowBlank: true,
			maxLength: 20,
			width: 80,
			hiddenName: 'numVagao',
			autoCreate: { tag: 'input', type: 'text', maxlength: '7', autocomplete: 'off' }
		};

		var chave = {
			xtype: 'textfield',
			vtype: 'ctevtype',
			id: 'filtro-Chave',
			fieldLabel: 'Chave CTe',
			name: 'Chave',
			autoCreate: { tag: 'input', type: 'text', maxlength: '44', autocomplete: 'off' },
			allowBlank: true,
			maxLength: 50,
			width: 320,
			hiddenName: 'Chave  '
		};

		var linha1col1 = {
			width: 120,
			layout: 'form',
			border: false,
			items:
				[dataInicial]
		};

		var arrCodigoDcl = {
            width: 75,
            layout: 'form',
            border: false,
            items:
				[cboCteCodigoControle]
            };

		var linha1col2 = {
				    width: 120,
				    layout: 'form',
				    border: false,
				    items:
						[dataFinal]
				};
		
		var linha1col3 = {
                width: 340,
                layout: 'form',
                border: false,
                items:
					[chave]
            };

		

		var linha2col3 = {
                    width: 70,
                    layout: 'form',
                    border: false,
                    items:
						[origem]
                };
		var linha2col4 = {
					width: 70,
					layout: 'form',
					border: false,
					items:
						[destino]
				};

		var linha2col5 =  {
							width: 70,
							layout: 'form',
							border: false,
							items:
								[serie]
						};

		var linha2col6 = {
				    width: 100,
				    layout: 'form',
				    border: false,
				    items:
						[despacho]
				};

		var linha2col7 = {
                    width: 100,
                    layout: 'form',
                    border: false,
                    items:
						[fluxo]
                };
		
		var linha2col8 = {
			width: 100,
			layout: 'form',
			border: false,
			items:
				[Vagao]
		};
		
		var arrlinha1 = {
			    layout: 'column',
			    border: false,
			    items: [linha1col1,linha1col2, linha1col3]
			};

		var arrlinha2 = {
			    layout: 'column',
			    border: false,
			    items: [arrCodigoDcl,linha2col5, linha2col6, linha2col3,linha2col4,linha2col7, linha2col8]
			};
		
		var arrCampos = new Array();
		arrCampos.push(arrlinha1);
		arrCampos.push(arrlinha2);


        filters = new Ext.form.FormPanel({
            id: 'grid-filtros',
            title: "Filtros",
            region: 'center',
            bodyStyle: 'padding: 15px',
            labelAlign: 'top',
            items:
			[arrCampos],
            buttonAlign: "center",
            buttons:
	        [
                {
                    text: 'Pesquisar',
                    type: 'submit',
                    iconCls: 'icon-find',
                    handler: function (b, e) {

                        if (Ext.getCmp("filtro-serie").getValue() != '' && Ext.getCmp("filtro-despacho").getValue() == '') {
                            Ext.Msg.show({
					            title: "Mensagem de Informação",
					            msg: "Não é possível filtrar apenas pela a Série do Despacho.<br/>Também é necessário preencher o número do Despacho.",
					            buttons: Ext.Msg.OK,
					            icon: Ext.MessageBox.INFO,
					            minWidth: 200
				            });
                        }
                        else if (Ext.getCmp("filtro-despacho").getValue() != '' && Ext.getCmp("filtro-serie").getValue() == '') {
                            Ext.Msg.show({
					            title: "Mensagem de Informação",
					            msg: "Não é possível filtrar apenas pelo número do Despacho.<br/>Também é necessário preencher a Série do Despacho.",
					            buttons: Ext.Msg.OK,
					            icon: Ext.MessageBox.INFO,
					            minWidth: 200
				            });
                        }
                        else {

                            if (filters.form.isValid()) {
							    ds.load();
						    }
                        }
                    }
                },
	            {
	                text: 'Limpar',
	                handler: function (b, e) {
	                    
						ds.removeAll();
						grid.getStore().removeAll();
						grid.getStore().totalLength = 0;
						grid.getBottomToolbar().bind(grid.getStore());							 
						grid.getBottomToolbar().updateInfo();  
	                    Ext.getCmp("grid-filtros").getForm().reset();
	                    Ext.getCmp("grid-campo").getForm().reset();

	                },
	                scope: this
	            }
            ]
        });
        /*
        ds.proxy.on('beforeload', function(p, params) {
				var dataInicial = Ext.getCmp("filtro-data-inicial").getValue();
				var dataFinal = Ext.getCmp("filtro-data-final").getValue();
				
				if (dataInicial!="")dataInicial=dataInicial.format('d/m/Y');
				if (dataFinal!="")dataFinal=dataFinal.format('d/m/Y');
				
				params['dataInicial'] = dataInicial;
				params['dataFinal'] = dataFinal;
				params['serie'] = Ext.getCmp("filtro-serie").getValue();
				params['despacho'] = Ext.getCmp("filtro-despacho").getValue();
				params['fluxo'] = Ext.getCmp("filtro-fluxo").getValue();
                params['serieCte'] = Ext.getCmp("filtro-serie_cte").getValue();
                params['nroCte'] = Ext.getCmp("filtro-nro-cte").getValue();
			});
            */
        new Ext.Viewport({
            layout: 'border',
            margins: 10,
            items: [
			        {
			            region: 'north',
			            height: 230,
			            items: 
                            [
                                {
			                        region: 'center',
			                        applyTo: 'header-content'
			                    },
				                filters
                            ]
			        },
			        grid
                    
            
		    ]

        });



    });
	</script>
	<style>
		.cor
		{
			background-color: #FFEEDD;
		}
	</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
	<div id="header-content">
		<h1>
			CTe - Take Or Pay</h1>
		<small>Você está em CTe > Take Or Pay </small>
		<br />
	</div>
</asp:Content>
