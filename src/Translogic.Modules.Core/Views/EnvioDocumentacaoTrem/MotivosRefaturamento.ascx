﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div id="div-add-mot-refat">
</div>
<script type="text/javascript">

    var documentacoes = (<%= ViewData["Documentacoes"] %>);
    var quantidadeDocumentacoes = documentacoes.length;

    /* region :: Functions */
    
    function Cancelar() {
        Ext.getCmp("form-add-mot-refat").close();        
    }

    function Confirmar() {
        // Atualizando os IDs dos motivos
        for(var i = 0; i < quantidadeDocumentacoes; i++) {
            var documentacao = documentacoes[i];
            var recebedorId = documentacao.RecebedorId;

            var ddlMotivoRecebedor = Ext.getCmp("ddlMotivoRecebedor_" + recebedorId);
            var motivoId = ddlMotivoRecebedor.getValue();

            if (motivoId === '' || motivoId === undefined) {
                
                Ext.Msg.show({
                    title: 'Documentação de Refaturamento',
                    msg: 'Alguns recebedores não possuem motivo de refaturamento.',
                    buttons: Ext.Msg.OK,
                    icon: window.Ext.MessageBox.ERROR,
                    minWidth: 200,
                    fn: function () {
                        if (data.Success) {
                            Ext.getCmp("form-add-mot-refat").close();
                        }
                    },
                    close: function () {
                        if (data.Success) {
                            Ext.getCmp("form-add-mot-refat").close();
                        }
                    }
                });

                return;    
            }
            
            documentacao.MotivoId = motivoId;
        }

        window.jQuery.ajax({
            url: '<%= Url.Action("SalvarDocumentacaoRefaturamento", "EnvioDocumentacaoTrem") %>',
            type: "POST",
            data: {
                DocumentacoesRefaturamento: Ext.util.JSON.encode(documentacoes)
            },
            success: function (response) {
                var iconeMensagem = window.Ext.MessageBox.ERROR;
                var data = Ext.decode(response);
                if (data.Success === true) {
                    iconeMensagem = window.Ext.MessageBox.SUCCESS;
                }
                
                Ext.Msg.show({
                    title: 'Documentação de Refaturamento',
                    msg: data.Message,
                    buttons: Ext.Msg.OK,
                    icon: iconeMensagem,
                    minWidth: 200,
                    fn: function () {
                        if (data.Success) {
                            Ext.getCmp("form-add-mot-refat").close();
                        }
                    },
                    close: function () {
                        if (data.Success) {
                            Ext.getCmp("form-add-mot-refat").close();
                        }
                    }
                });
            },
            error: function (jqXHR, textStatus) {
                Ext.Msg.show({
                    title: "Erro no Servidor",
                    msg: 'Ocorreu um erro no servidor, por favor, tente novamente mais tarde.',
                    buttons: Ext.Msg.ERROR,
                    minWidth: 200
                });
                
                Ext.getCmp("form-add-conf").close();
            }
        });
    }
    
    /* endregion :: Functions */

    /* region :: Stores */

    /* endregion :: Stores */

    /* region :: Filtros */

    var items = [];
    var motivosRecebedores = [];
    var recebedoresId = [];
    for (var i = 0; i < quantidadeDocumentacoes; i++) {
        var documento = documentacoes[i];
        var recebedorId = documento.RecebedorId;
        var recebedor = documento.Recebedor;

        var recebedorInserido = false;
        for(var j = 0; j < recebedoresId.length; j++) {
            var id = recebedoresId[j].RecebedorId;
            if (id === recebedorId) {
                recebedorInserido = true;
            }
        }
        
        if (recebedorInserido === false) {
            var motivoRecebedor = {
                RecebedorId: 0,
                Store: ''
            };

            var motivoRecebedorStore = new window.Ext.data.JsonStore({
                id: 'motivoRecebedor_' + recebedorId,
                name: 'motivoRecebedor_' + recebedorId,
                root: 'Items',
                url: '<%= Url.Action("ObterMotivosRefaturamento", "EnvioDocumentacaoTrem") %>',
                fields: [
                    'Id',
                    'DescResumida'
                ],
                listeners: {
                    load: function (store) {
                        var rt = store.recordType;
                        store.insert(0, new rt({}));
                    }
                }
            });

            motivoRecebedor.RecebedorId = recebedorId;
            motivoRecebedor.Store = motivoRecebedorStore;
                
            var ddlMotivoRecebedor = {
                xtype: 'combo',
                id: 'ddlMotivoRecebedor_' + recebedorId,
                name: 'ddlMotivoRecebedor_' + recebedorId,
                forceSelection: true,
                store: motivoRecebedor.Store,
                triggerAction: 'all',
                mode: 'remote',
                typeAhead: false,
                fieldLabel: recebedor,
                displayField: 'DescResumida',
                valueField: 'Id',
                width: 200,
                editable: false,
                tpl: '<tpl for="."><div class="x-combo-list-item">{DescResumida}&nbsp;</div></tpl>'
            };
                
            items.push(ddlMotivoRecebedor);
            motivosRecebedores.push(motivoRecebedor);
        }
    }

    var btnCancelar = new Ext.Button({
        id: 'btnCancelar',
        name: 'btnCancelar',
        text: 'Cancelar',
        handler: Cancelar
    });

    var btnConfirmar = new Ext.Button({
        id: 'btnConfirmar',
        name: 'btnConfirmar',
        text: 'Confirmar',
        iconCls: 'icon-new',
        handler: Confirmar
    });
    
    /* endregion :: Filtros */

    
    /* region :: Modal */

    var modal = new Ext.form.FormPanel({
        id: 'form-add-mot-refat-modal',
        labelWidth: 150,
        width: 415,
        resizable: false,
        height: 380,
        bodyStyle: 'padding: 15px',
        items: items,
        buttonAlign: "center",
        buttons: [
            btnCancelar,
            btnConfirmar
        ]
    });

    /* endregion :: Modal*/

    modal.render("div-add-mot-refat");
</script>
