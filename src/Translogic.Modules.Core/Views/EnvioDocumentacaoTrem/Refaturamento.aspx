﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Interna.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content runat="server" ID="HeadContent" ContentPlaceHolderID="HeadContent">
    <div id="header-content">
        <h1>
            Documentações de Refaturamento</h1>
        <small>Refaturamento</small>
        <br />
    </div>
    <script type="text/javascript" language="javascript">

        var formModal = null;
        var posicaoRow = -1;

        /* region :: Functions */

        function Pesquisar() {
            var numeroOs = Ext.getCmp("txtNumeroOs").getValue();
            if (numeroOs === '') {
                Ext.Msg.alert("Erro", 'Informe um Número de OS');
            } else {
                grid.pagingToolbar.pageSize = 300;
                grid.getStore().load();
            }
        }

        function Limpar() {
            grid.getStore().removeAll();
            Ext.getCmp("filtros").getForm().reset();
        }

        function RetornaRegistrosSelecionados() {
            var registrosSelecionados = new Array();

            var selection = sm.getSelections();

            for (var i = 0; i < selection.length; i++) {
                var osId = selection[i].data.OsId;
                var composicaoId = selection[i].data.ComposicaoId;
                var numeroOs = selection[i].data.NumeroOs;
                var prefixoTrem = selection[i].data.PrefixoTrem;
                var codigoVagao = selection[i].data.CodigoVagao;
                var recebedorId = selection[i].data.RecebedorId;
                var recebedor = selection[i].data.Recebedor;

                var registro = {
                    OsId: osId,
                    ComposicaoId: composicaoId,
                    NumeroOs: numeroOs,
                    PrefixoTrem: prefixoTrem,
                    CodigoVagao: codigoVagao,
                    RecebedorId: recebedorId,
                    Recebedor: recebedor
                };

                registrosSelecionados.push(registro);
            }

            return registrosSelecionados;
        }

        function EnviarDocumentacaoRefaturamento() {
            var request = RetornaRegistrosSelecionados();

            if (request.length > 0) {
                posicaoRow = 1;
                formModal = new Ext.Window({
                    id: 'form-add-mot-refat',
                    title: 'Motivos Refaturamento',
                    modal: true,
                    resizable: false,
                    width: 420,
                    height: 415,
                    autoLoad:
                    {
                        url: '<%= Url.Action("MotivosRefaturamento", "EnvioDocumentacaoTrem") %>',
                        scripts: true,
                        params: {
                            documentacoesRefaturamento: Ext.util.JSON.encode(request)
                        },
                        callback: function (el, success, c) {
                            posicaoRow = -1;
                            if (!success) {
                                formModal.close();
                            }
                        }
                    }
                });

                formModal.show(this);
            }
            else {
                Ext.Msg.show({
                    title: "Mensagem de Informação",
                    msg: 'Selecione, pelo menos, um vagão para gerar a documentação de refaturamento.',
                    buttons: Ext.Msg.ERROR,
                    minWidth: 200
                });
            }
        }

        /* endregion :: Functions */

        /* region :: Stores */

        var recebedorStore = new Ext.data.JsonStore({
            id: 'recebedorStore',
            name: 'recebedorStore',
            root: 'Items',
            autoLoad: true,
            proxy: new Ext.data.HttpProxy({ 
                url: '<%= Url.Action("ObterTerminaisPorOs", "EnvioDocumentacaoTrem") %>',
                timeout: 600000
            }),
            fields: [
                'Id',
                'DescResumida'
            ],
            listeners: {
                load: function (store) {
                    var rt = store.recordType;
                    store.insert(0, new rt({}));
                }
            }
        });
        window.recebedorStore = recebedorStore;

        /* endregion :: Stores */

        /* region :: Filtros */

        var txtNumeroOs = {
            xtype: 'textfield',
            name: 'txtNumeroOs',
            id: 'txtNumeroOs',
            fieldLabel: 'Número OS',
            autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '15' },
            maskRe: /[0-9]/,
            style: 'text-transform: uppercase',
            width: 125,
            listeners: {
                change: {
                    fn: function () {
                        var value = this.getValue();
                        recebedorStore.load({ params: { numeroOs: value },
                            callback: function (options, success, response, records) {
                                if (success) {
                                    var combobox = Ext.getCmp("ddlRecebedor");

                                    if (recebedorStore.getCount() == 0) {
                                        combobox.setValue("");
                                    } else {
                                        combobox.setValue("TODOS");
                                    }
                                }
                            }
                        });                    
                    }
                }
            }
        };

        var arrNumeroOs = {
            width: 130,
            layout: 'form',
            border: false,
            items: [txtNumeroOs]
        };

        var ddlRecebedor = {
            xtype: 'combo',
            id: 'ddlRecebedor',
            name: 'ddlRecebedor',
            store: window.recebedorStore,
            triggerAction: 'all',
            mode: 'local',
            fieldLabel: 'Recebedor',
            displayField: 'DescResumida',
            valueField: 'Id',
            width: 200,
            editable: false,
            tpl: '<tpl for="."><div class="x-combo-list-item">{DescResumida}&nbsp;</div></tpl>'
        };

        var arrRecebedor = {
            width: 205,
            layout: 'form',
            border: false,
            items: [ddlRecebedor]
        };

        var arrlinha1 = {
            layout: 'column',
            border: false,
            items: [
                arrNumeroOs,
                arrRecebedor
            ]
        };

        var arrCampos = new Array();
        arrCampos.push(arrlinha1);

        var filtros = new Ext.form.FormPanel({
            id: 'filtros',
            title: 'Filtros',
            name: 'filtros',
            region: 'center',
            bodyStyle: 'padding: 15px',
            initEl: function (el) {
                this.el = Ext.get(el);
                this.id = this.el.id || Ext.id();
                if (this.standardSubmit) {
                    this.el.dom.action = this.url;
                } else {
                    this.el.on('submit', this.onSubmit, this);
                }
                this.el.addClass('x-form');
            },
            labelAlign: 'top',
            border: false,
            items: [arrCampos],
            buttonAlign: 'center',
            buttons: [
                {
                    name: 'btnPesquisar',
                    id: 'btnPesquisar',
                    text: 'Pesquisar',
                    iconCls: 'icon-find',
                    handler: Pesquisar
                },
                {
                    name: 'btnLimpar',
                    id: 'btnLimpar',
                    text: 'Limpar',
                    handler: Limpar
                },
                {
                    name: 'btnEnviar',
                    id: 'btnEnviar',
                    text: 'Enviar',
                    handler: EnviarDocumentacaoRefaturamento
                }
            ]
        });

        /* endregion :: Filtros */

        /* region :: Grid */

        var sm = new Ext.grid.CheckboxSelectionModel({
            listeners: {
                selectionchange: function (sm) {
                    var recLen = Ext.getCmp('grid').store.getRange().length;
                    var selectedLen = this.selections.items.length;
                    if (selectedLen == recLen) {
                        var view = Ext.getCmp('grid').getView();
                        var chkdiv = Ext.fly(view.innerHd).child(".x-grid3-hd-checker");
                        chkdiv.addClass("x-grid3-hd-checker-on");
                    }
                }
            },
            rowdeselect: function (sm, rowIndex, record) {
                var view = Ext.getCmp('grid').getView();
                var chkdiv = Ext.fly(view.innerHd).child(".x-grid3-hd-checker");
                chkdiv.removeClass('x-grid3-hd-checker-on');
            }
        });

        var cm = new Ext.grid.ColumnModel({
            defaults: {
                sortable: true
            },
            columns: [
                new Ext.grid.RowNumberer(),
                sm,
                {
                    dataIndex: "Id", hidden: true
                },
                {
                    dataIndex: "OsId", hidden: true
                },
                {
                    dataIndex: "RecebedorId", hidden: true
                },
                {
                    dataIndex: "ComposicaoId", hidden: true
                },
                {
                    header: 'OS',
                    dataIndex: 'NumeroOs',
                    width: 55
                },
                {
                    header: 'Prefixo',
                    dataIndex: 'PrefixoTrem',
                    width: 60
                },
                {
                    header: 'Vagão',
                    dataIndex: 'CodigoVagao',
                    width: 80
                },
                {
                    header: 'Recebedor',
                    dataIndex: 'Recebedor',
                    width: 300
                }
            ]
        });

        var grid = new Translogic.PaginatedGrid({
            id: 'grid',
            name: 'grid',
            autoLoadGrid: false,
            region: 'center',
            viewConfig: {
                emptyText: 'Não possui dado(s) para exibição.'
            },
            loadMask: { msg: App.Resources.Web.Carregando },
            sm: sm,
            fields: [
                'OsId',
                'NumeroOs',
                'ComposicaoId',
                'PrefixoTrem',
                'CodigoVagao',
                'RecebedorId',
                'Recebedor'
            ],
            cm: cm,
            stripeRows: true,
            url: '<%= Url.Action("ObterRefaturamentosGrid", "EnvioDocumentacaoTrem") %>'
        });

        grid.getStore().proxy.on('beforeload', function (p, params) {
            var ddlRecebedor = Ext.getCmp("ddlRecebedor");
            var recebedorId = ddlRecebedor.getValue();

            params['numeroOs'] = Ext.getCmp("txtNumeroOs").getValue();
            params['recebedor'] = recebedorId;
        });

        /* endregion :: Grid */

        /* region :: Render */

        new Ext.Viewport({
            layout: 'border',
            margins: 10,
            items: [{
                region: 'north',
                height: 200,
                autoScroll: true,
                items: [{
                    region: 'center',
                    applyTo: 'header-content'
                },
                        filtros]
            },
                grid]
        });

        /* endregion :: Render */

    </script>
</asp:Content>
