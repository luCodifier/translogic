﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Interna.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content runat="server" ID="HeadContent" ContentPlaceHolderID="HeadContent">
    <div id="header-content">
        <h1>Monitoramento de Envios</h1>
        <small>Monitoramento dos e-mails de documentação enviados</small>
        <br />
    </div>
    <script type="text/javascript" language="javascript">

        /* region :: Váriaveis */
        
        var gridResultados = null;

        var statusEnvioStore = new Ext.data.ArrayStore({
            id: 'statusEnvioStore',
            fields: [ 'ID', 'Text' ],
            data: [ [-1, 'Todos'], [0, 'Pendentes'], [1, 'Enviadas'] ]
        });

        /* endregion :: Váriaveis */

        /* region :: Stores */

        var recebedorStore = new Ext.data.JsonStore({
            id: 'recebedorStore',
            name: 'recebedorStore',
            root: 'Items',
            autoLoad: true,
            proxy: new Ext.data.HttpProxy({
                url: '<%= Url.Action("ObterTerminaisPorOs", "EnvioDocumentacaoTrem") %>',
                timeout: 600000
            }),
            fields: [
                'Id',
                'DescResumida'
            ],
            listeners: {
                load: function (store) {
                    var rt = store.recordType;
                    store.insert(0, new rt({}));
                }
            }
        });
        window.recebedorStore = recebedorStore;
        
        /* endregion :: Stores */

        /* region :: Functions */

        var malhaOrigemStore = new Ext.data.JsonStore({
            root: "Items",
            autoLoad: true,
            proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("ObterMalhas", "EnvioDocumentacaoTrem") %>',
                timeout: 600000
            }),
            id: 'malhaOrigemStore',
            fields: [
                'Id',
                'Nome'
            ]
        });
        
        function EnviarEmail(id) {
            window.jQuery.ajax({
                url: '<%= Url.Action("EnviarDocumentacaoManualPorId", "EnvioDocumentacaoTrem") %>',
                type: "POST",
                data: {
                    id: id
                },
                dataType: "json",
                success: function (data) {
                    var iconeCadastro = window.Ext.MessageBox.ERROR;
                    if (data.Success == true) {
                        iconeCadastro = window.Ext.MessageBox.SUCCESS;
                    }

                    window.Ext.Msg.show({
                        title: "Enviar Documentação Manual",
                        msg: data.Message,
                        buttons: window.Ext.Msg.OK,
                        icon: iconeCadastro,
                        minWidth: 200
                    });
                },
                error: function (jqXHR, textStatus) {
                    window.Ext.Msg.show({
                        title: "Erro no Servidor",
                        msg: 'Ocorreu um erro no servidor, por favor, tente novamente mais tarde.',
                        buttons: window.Ext.Msg.OK,
                        icon: window.Ext.MessageBox.ERROR,
                        minWidth: 200
                    });
                }
            });
        }
        
        function EnviarEmailsRenderer(id) {
            return "<img style='cursor: pointer; cursor: hand;' title='Enviar' src='<%=Url.Images("Icons/email.png") %>' alt='Enviar' onclick='EnviarEmail(" + id + ");'>";
        }
        
        function DownloadDocumentacao(url) {
            console.log(url);
            window.open(url, "", "");
        }
        
        function DownloadDocumentacaoRenderer(urlDownload) {
            var html = '';
            html = "<img style='cursor: pointer; cursor: hand;' title='Download' src='<%=Url.Images("Icons/link.png") %>' alt='Download' onclick='DownloadDocumentacao(\"" + urlDownload + "\");'>";

            return html;
        }
        
        function GerenciarRenderer(id, urlDownload) {
            var html = '';
            var espaco = '&nbsp';
            
            if (urlDownload !== '') {
                html += EnviarEmailsRenderer(id);
                html += (espaco + DownloadDocumentacaoRenderer(urlDownload));
            }
            
            return html;
        }
        
        function Pesquisar() {
            gridResultados.getStore().load();
        }

        function Limpar() {
            gridResultados.getStore().removeAll();
            Ext.getCmp("filtros").getForm().reset();
        }

        function StatusRenderer(status) {
            if (status == 'N') {
                return "<img src='/Get.efh?file={Translogic.Modules.Core;}/circle-red.png' alt='Erro'>";
            } else if (status == 'S') {
                return "<img src='/Get.efh?file={Translogic.Modules.Core;}/circle-green.png' alt='Enviada'>";
            } else {
                return "<img src='/Get.efh?file={Translogic.Modules.Core;}/circle-yellow.png' alt='Aguardando'>";
            }
        }
        
        var statusStore = new Ext.data.ArrayStore({
            id: 0,
            fields: [
                'Id',
                'Text'
            ],
            data: [
                ['', 'Todos'],
                ['S', 'Sim'],
                ['N', 'Não']
            ]
        });
        window.statusStore = statusStore;
        
        /* endregion Functions */

        /* region :: Filtros */

        var dataInicio = {
            xtype: 'datefield',
            fieldLabel: 'Data Início',
            id: 'dataInicio',
            name: 'dataInicio',
            width: 85,
            allowBlank: false,
            value: new Date()
        };

        var dataFim = {
            xtype: 'datefield',
            fieldLabel: 'Data Fim',
            id: 'dataFim',
            name: 'dataFim',
            width: 85,
            allowBlank: false,
            value: new Date()
        };
        
        var arrDataIni = {
            width: 90,
            layout: 'form',
            border: false,
            items: [dataInicio]
        };

        var arrDataFim = {
            width: 90,
            layout: 'form',
            border: false,
            items: [dataFim]
        };
        
        var ddlMalhaOrigem = new Ext.form.ComboBox({
            typeAhead: false,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            store: malhaOrigemStore,
            valueField: 'Id',
            width: 100,
            displayField: 'Nome',
            fieldLabel: 'Malha',
            id: 'ddlMalhaOrigem'
        });
        
        var arrMalhaOrigem = {
            width: 105,
            layout: 'form',
            border: false,
            items: [ddlMalhaOrigem]
        };
        
        var txtDestino = {
            xtype: 'textfield',
            name: 'txtDestino',
            id: 'txtDestino',
            fieldLabel: 'Destino',
            autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '3' },
            maskRe: /[a-zA-Z]/,
            style: 'text-transform: uppercase',
            width: 50
        };
        
        var arrDestino = {
            width: 55,
            layout: 'form',
            border: false,
            items: [txtDestino]
        };
        
        var ddlRecebedor = {
            xtype: 'combo',
            id: 'ddlRecebedor',
            name: 'ddlRecebedor',
            store: window.recebedorStore,
            triggerAction: 'all',
            mode: 'local',
            fieldLabel: 'Recebedor',
            displayField: 'DescResumida',
            valueField: 'Id',
            width: 200,
            editable: false,
            tpl: '<tpl for="."><div class="x-combo-list-item">{DescResumida}&nbsp;</div></tpl>'
        };

        var arrRecebedor = {
            width: 205,
            layout: 'form',
            border: false,
            items: [ddlRecebedor]
        };
        
        var txtNumeroOs = {
            xtype: 'textfield',
            name: 'txtNumeroOs',
            id: 'txtNumeroOs',
            fieldLabel: 'Número OS',
            autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '15' },
            maskRe: /[0-9]/,
            style: 'text-transform: uppercase',
            width: 125,
            listeners: {
                change: {
                    fn: function () {
                        var value = this.getValue();
                        recebedorStore.load({ params: { numeroOs: value },
                            callback: function (options, success, response, records) {
                                if (success) {
                                    var combobox = Ext.getCmp("ddlRecebedor");

                                    if (recebedorStore.getCount() == 0) {
                                        combobox.setValue("");
                                    } else {
                                        combobox.setValue("TODOS");
                                    }
                                }
                            }
                        });
                    }
                }
            }
        };

        var arrNumeroOs = {
            width: 130,
            layout: 'form',
            border: false,
            items: [txtNumeroOs]
        };
        
        var txtPrefixo = {
            xtype: 'textfield',
            name: 'txtPrefixo',
            id: 'txtPrefixo',
            fieldLabel: 'Prefixo',
            autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '3' },
            style: 'text-transform: uppercase',
            width: 50
        };

        var arrPrefixo = {
            width: 55,
            layout: 'form',
            border: false,
            items: [txtPrefixo]
        };  
        
        var ddlSituacaoEnvioStatus = new Ext.form.ComboBox({
            id: 'ddlSituacaoEnvioStatus',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            store: statusEnvioStore,
            valueField: 'ID',
            displayField: 'Text',
            value: -1,
            width: 110,
            fieldLabel: 'Situação Envio'
        });
        
        var arrSituacaoEnvioStatus = {
            layout: 'form',
            border: false,
            width: 120,
            items: [ddlSituacaoEnvioStatus]
        };
        
        var ddlDocCompl = new Ext.form.ComboBox({
            id: 'ddlDocCompl',
            name: 'ddlDocCompl',
            fieldLabel: 'Documentação Completa',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            store: window.statusStore,
            valueField: 'Id',
            displayField: 'Text',
            width: 135
        });

        var arrDocCompl = {
            width: 145,
            layout: 'form',
            border: false,
            items: [ddlDocCompl]
        };
        
        var ddlTremEncerr = new Ext.form.ComboBox({
            id: 'ddlTremEncerr',
            name: 'ddlTremEncerr',
            fieldLabel: 'Trem Encerrado',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            store: window.statusStore,
            valueField: 'Id',
            displayField: 'Text',
            width: 125
        });

        var arrTremEncerr = {
            width: 130,
            layout: 'form',
            border: false,
            items: [ddlTremEncerr]
        };
        
        var btnPesquisar = {
            xtype: "button",
            name: 'btnPesquisar',
            id: 'btnPesquisar',
            text: 'Pesquisar',
            fieldLabel: '&nbsp;',
            labelSeparator: ' ',
            iconCls: 'icon-find',
            width: 85,
            handler: Pesquisar
        };

        var arrBtnPesquisar = {
            width: 90,
            layout: 'form',
            border: false,
            items: [btnPesquisar]
        };

        var btnLimpar = {
            xtype: "button",
            name: 'btnLimpar',
            id: 'btnLimpar',
            text: 'Limpar',
            fieldLabel: '&nbsp;',
            labelSeparator: ' ',
            width: 75,
            handler: Limpar
        };

        var arrBtnLimpar = {
            width: 90,
            layout: 'form',
            border: false,
            items: [btnLimpar]
        };

        var arrlinha1 = {
            layout: 'column',
            border: false,
            items: [
                arrMalhaOrigem,
                arrDestino,
                arrRecebedor,
                arrNumeroOs,
                arrPrefixo,
                arrDataIni,
                arrDataFim
            ]
        };
        
        var arrlinha2 = {
            layout: 'column',
            border: false,
            items: [
                arrTremEncerr,
                arrSituacaoEnvioStatus,
                arrDocCompl
            ]
        };

        /* endregion :: Filtros */

        /* region :: Grid */

        var cm = new Ext.grid.ColumnModel({
            defaults: {
                sortable: true
            },
            columns: [
                {
                    dataIndex: "DocumentacaoId", hidden: true
                },
                {
                    header: '',
                    dataIndex: 'DocumentacaoId',
                    width: 80,
                    align: "center",
                    sortable: false,
                    renderer: function (value, metaData, record, rowIndex, colIndex, store) {
                        return GerenciarRenderer(record.data.DocumentacaoId, record.data.LinkDownload);
                    }
                },
                {
                    header: 'Enviado',
                    dataIndex: 'Enviado',
                    align: "center",
                    width: 100,
                    renderer: StatusRenderer
                },
                {
                    header: 'Enviado Em',
                    dataIndex: 'CriadoEm',
                    align: "center",
                    width: 120
                },
                {
                    header: 'Estação Envio',
                    dataIndex: 'EstacaoEnvio',
                    align: "center",
                    width: 100
                },
                {
                    header: 'Modo Envio',
                    dataIndex: 'Envio',
                    align: "center",
                    width: 100
                },
                {
                    header: 'Prefixo Trem',
                    dataIndex: 'TremPrefixo',
                    align: "center",
                    width: 100
                },
                {
                    header: 'Número OS',
                    dataIndex: 'OsNumero',
                    align: "center",
                    width: 100
                },
                {
                    header: 'Origem',
                    dataIndex: 'Origem',
                    align: "center",
                    width: 120
                },
                {
                    header: 'Atual',
                    dataIndex: 'Atual',
                    align: "center",
                    width: 120
                },
                {
                    header: 'Destino',
                    dataIndex: 'Destino',
                    align: "center",
                    width: 120
                },
                {
                    header: 'Chegada Aprox.',
                    dataIndex: 'DataChegadaTrem',
                    align: "center",
                    width: 120
                },
                {
                    header: 'Qntd. Vagões',
                    dataIndex: 'QuantidadeVagoes',
                    align: "center",
                    width: 120
                },
                {
                    header: 'Trem Encerrado',
                    dataIndex: 'TremEncerrado',
                    align: "center",
                    width: 100
                },
                {
                    header: 'Recebedor',
                    dataIndex: 'Recebedor',
                    align: "center",
                    width: 150
                },
                {
                    header: 'Usuário',
                    dataIndex: 'UsuarioNome',
                    align: "center",
                    width: 150
                },
                {
                    header: 'Refaturamento',
                    dataIndex: 'Refaturamento',
                    align: "center",
                    width: 100
                },
                {
                    header: 'Recomposição',
                    dataIndex: 'Recomposicao',
                    align: "center",
                    width: 100
                },
                {
                    header: 'Doc. Completa',
                    dataIndex: 'DocumentacaoCompleta',
                    align: "center",
                    width: 100
                },
                {
                    header: 'Doc. Excluída',
                    dataIndex: 'DocumentacaoExcluida',
                    align: "center",
                    width: 100
                }
            ]
        });

        gridResultados = new Translogic.PaginatedGrid({
            id: 'gridResultados',
            name: 'gridResultados',
            cm: cm,
            autoLoadGrid: false,
            region: 'center',
            viewConfig: {
                forceFit: false,
                emptyText: 'Não possui dado(s) para exibição.'
            },
            loadMask: { msg: App.Resources.Web.Carregando },
            fields: [
                'DocumentacaoId',
                'OsNumero',
                'TremPrefixo',
                'Recebedor',
                'TremEncerrado',
                'Enviado',
                'EstacaoEnvio',
                'Envio',
                'CriadoEm',
                'EnvioManual',
                'UsuarioNome',
                'DocumentacaoExcluida',
                'LinkDownload',
                'Refaturamento',
                'Recomposicao',
                'DataChegadaTrem',
                'PassouAreaParametrizada',
                'QuantidadeVagoes',
                'Origem',
                'Destino',
                'Atual',
                'DocumentacaoCompleta'
            ],
            stripeRows: true,
            url: '<%= Url.Action("ObterMonitoramentoGrid", "EnvioDocumentacaoTrem") %>'
        });

        gridResultados.getStore().proxy.on('beforeload', function (p, params) {
            var ddlSituacaoEnvioStatus = Ext.getCmp("ddlSituacaoEnvioStatus");
            var situacaoEnvioStatus = ddlSituacaoEnvioStatus.getValue();
            var ddlRecebedor = Ext.getCmp("ddlRecebedor");
            var recebedorId = ddlRecebedor.getValue();
            var malhaId = ddlMalhaOrigem.getValue();
            var dataInicio = Ext.getCmp("dataInicio").getValue();
            var dataFim = Ext.getCmp("dataFim").getValue();
            var ddlTremEncerr = Ext.getCmp("ddlTremEncerr");
            var tremEncerrId = ddlTremEncerr.getValue();
            var ddlDocCompl = Ext.getCmp("ddlDocCompl");
            var docComplId = ddlDocCompl.getValue();

            params['malha'] = malhaId;
            params['destino'] = Ext.getCmp("txtDestino").getValue().toUpperCase();
            params['recebedor'] = recebedorId;
            params['numeroOs'] = Ext.getCmp("txtNumeroOs").getValue();
            params['prefixo'] = Ext.getCmp("txtPrefixo").getValue().toUpperCase();
            params['dataInicio'] = dataInicio.format('d/m/Y');
            params['dataFim'] = dataFim.format('d/m/Y');
            params['tremEncerr'] = tremEncerrId;
            params['situacaoEnvioStatus'] = situacaoEnvioStatus;
            params['docCompl'] = docComplId;
        });

        var arrCampos = new Array();
        arrCampos.push(arrlinha1);
        arrCampos.push(arrlinha2);
        
        var filtros = new Ext.form.FormPanel({
            id: 'filtros',
            title: 'Filtros',
            name: 'filtros',
            region: 'center',
            bodyStyle: 'padding: 15px 15px 0px 15px',
            initEl: function (el) {
                this.el = Ext.get(el);
                this.id = this.el.id || Ext.id();
                if (this.standardSubmit) {
                    this.el.dom.action = this.url;
                } else {
                    this.el.on('submit', this.onSubmit, this);
                }
                this.el.addClass('x-form');
            },
            labelAlign: 'top',
            border: false,
            items: [arrCampos],
            buttonAlign: 'center',
            buttons: [
                {
                    name: 'btnPesquisar',
                    id: 'btnPesquisar',
                    text: 'Pesquisar',
                    iconCls: 'icon-find',
                    handler: Pesquisar
                },
                {
                    name: 'btnLimpar',
                    id: 'btnLimpar',
                    text: 'Limpar',
                    handler: Limpar
                },
                {
                    name: 'btnExportar',
                    id: 'btnExportar',
                    text: 'Exportar',
                    iconCls: 'icon-page-excel',
                    handler: Exportar
                }]
        });
        
        function Exportar() {
            var url = '<%= Url.Action("ExportarExcel") %>';
            
            var ddlSituacaoEnvioStatus = Ext.getCmp("ddlSituacaoEnvioStatus");
            var situacaoEnvioStatus = ddlSituacaoEnvioStatus.getValue();
            var ddlRecebedor = Ext.getCmp("ddlRecebedor");
            var recebedorId = ddlRecebedor.getValue();
            var malhaId = ddlMalhaOrigem.getValue();
            var dataInicio = Ext.getCmp("dataInicio").getValue();
            var dataFim = Ext.getCmp("dataFim").getValue();
            var ddlTremEncerr = Ext.getCmp("ddlTremEncerr");
            var tremEncerrId = ddlTremEncerr.getValue();
            var ddlDocCompl = Ext.getCmp("ddlDocCompl");
            var docComplId = ddlDocCompl.getValue();

            url += "?malha=" + encodeURIComponent(malhaId) +
                "&destino=" + Ext.getCmp("txtDestino").getValue().toUpperCase() +
                "&recebedor=" + recebedorId +
                "&numeroOs=" + Ext.getCmp("txtNumeroOs").getValue() +
                "&prefixo=" + Ext.getCmp("txtPrefixo").getValue().toUpperCase() +
                "&dataInicio=" +dataInicio.format('d/m/Y') +
                "&dataFim=" +dataFim.format('d/m/Y') +
                "&tremEncerr=" + tremEncerrId + 
                "&situacaoEnvioStatus=" + situacaoEnvioStatus +
                "&docCompl=" + docComplId;
            
            window.open(url, "");
        }

        /* endregion :: Grid */
        new Ext.Viewport({
            layout: 'border',
            margins: 10,
            items: [
			{
			    region: 'north',
			    height: 220,
			    autoScroll: false,
			    items: [{
			        region: 'center',
			        applyTo: 'header-content'
			    },
				filtros]
			},
			gridResultados
		]
        });
    </script>
</asp:Content>
