﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div id="div-form-edit-conf">
</div>
<script type="text/javascript">

    var configuracaoId = (<%= ViewData["ConfiguracaoId"] %>);
    var configuracaoAreaOperacional = ('<%= ViewData["ConfiguracaoAreaOperacional"] %>');
    var configuracaoTerminal = {
        Id: (<%= ViewData["ConfiguracaoTerminalId"] %>),
        DescResumida: ('<%= ViewData["ConfiguracaoTerminalDescricao"] %>')
    };
    var configuracaoValidadeDocumentacao = ('<%= ViewData["ConfiguracaoValidadeDocumentacao"] %>');

    var configuracaoRelatorioLaudoMercadoria = ('<%= ViewData["ConfiguracaoRelatorioLaudoMercadoria"] %>');
    configuracaoRelatorioLaudoMercadoria = configuracaoRelatorioLaudoMercadoria.toLowerCase() == 'true' || configuracaoRelatorioLaudoMercadoria == true;

    var configuracaoEnvioChegada = ('<%= ViewData["ConfiguracaoEnviarChegada"] %>');
    configuracaoEnvioChegada = configuracaoEnvioChegada.toLowerCase() == 'true' || configuracaoEnvioChegada == true;
    
    var configuracaoEnvioSaida = ('<%= ViewData["ConfiguracaoEnviarSaida"] %>');
    configuracaoEnvioSaida = configuracaoEnvioSaida.toLowerCase() == 'true' || configuracaoEnvioSaida == true;

    /* region :: Functions */
    
    function PesquisarEmails() {
        gridEmails.getStore().load();
    }
    
    function LimparCampos() {
        Ext.getCmp("txtAreaOperacionalEnvioEmailModal").setValue(configuracaoAreaOperacional);
        Ext.getCmp("ddlTerminalEmailModal").reset(); 
        Ext.getCmp("ddlTerminalEmailModal").setValue(configuracaoTerminal);
        Ext.getCmp("txtEmailInclusao").setValue('');
        Ext.getCmp("txtDiasValidade").setValue(configuracaoValidadeDocumentacao);
    }

    function AdicionarEmail() {
        var idConfiguracao = configuracaoId;
        var email = Ext.getCmp("txtEmailInclusao").getValue();
        
        Ext.Ajax.request({
            url: '<%= Url.Action("SalvarEmailConfiguracao", "EnvioDocumentacaoTrem") %>',
            method: "POST",
            params: { 
                idConfiguracao: idConfiguracao,  
                email: email
            },
            success: function(response, options) {
                var result = Ext.decode(response.responseText);
                
                if (result.Success == true) {
                    PesquisarEmails();
                    Ext.getCmp("txtEmailInclusao").setValue('');
                    
                    Ext.Msg.show({
                        title: 'Aviso',
                        msg: result.Message,
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                    });
                } else {
                    Ext.Msg.show({
                        title: 'Aviso',
                        msg: result.Message,
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.ERROR
                    });
                }

                Ext.getBody().unmask();
            },
            failure: function(response, options) {
                Ext.Msg.show({
                    title: 'Aviso',
                    msg: response.Message,
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.ERROR
                });

                Ext.getBody().unmask();
            }
        });
    }
    
    function HabilitarDesabilitarEmailConfiguracao(id) {
        Ext.Ajax.request({
            url: '<%= Url.Action("HabilitarEmailConfiguracao", "EnvioDocumentacaoTrem") %>',
            method: "POST",
            params: { id: id },
            success: function(response, options) {
                var result = Ext.decode(response.responseText);
                
                if (result.Success == true) {
                    PesquisarEmails();

                    Ext.Msg.show({
                        title: 'Aviso',
                        msg: result.Message,
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                    });
                } else {
                    Ext.Msg.show({
                        title: 'Aviso',
                        msg: result.Message,
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.ERROR
                    });
                }

                Ext.getBody().unmask();
            },
            failure: function(response, options) {
                Ext.Msg.show({
                    title: 'Aviso',
                    msg: response.Message,
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.ERROR
                });

                Ext.getBody().unmask();
            }
        });
    }
    
    function DeletarEmailConfiguracao(id) {
        Ext.Ajax.request({
            url: '<%= Url.Action("DeletarEmailConfiguracao", "EnvioDocumentacaoTrem") %>',
            method: "POST",
            params: { id: id },
            success: function(response, options) {
                var result = Ext.decode(response.responseText);
                
                if (result.Success == true) {
                    PesquisarEmails();

                    Ext.Msg.show({
                        title: 'Aviso',
                        msg: result.Message,
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                    });
                } else {
                    Ext.Msg.show({
                        title: 'Aviso',
                        msg: result.Message,
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.ERROR
                    });
                }

                Ext.getBody().unmask();
            },
            failure: function(response, options) {
                Ext.Msg.show({
                    title: 'Aviso',
                    msg: response.Message,
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.ERROR
                });

                Ext.getBody().unmask();
            }
        });
    }

    function HabilitarEmailRenderer(id) {
        return "<img style='cursor: pointer; cursor: hand;' title='Habilitar/Desativar' src='<%=Url.Images("Icons/accept.png") %>' alt='Habilitar/Desativar' onclick='HabilitarDesabilitarEmailConfiguracao(" + id + ");'>";
    }
    
    function DeletarEmailRenderer(id) {
        return "<img style='cursor: pointer; cursor: hand;' title='Deletar' src='<%=Url.Images("Icons/delete.png") %>' alt='Deletar' onclick='DeletarEmailConfiguracao(" + id + ");'>";
    }

    function GerenciarEmailRenderer(id) {
        var html = '';
        var espaco = '&nbsp';
        
        html += HabilitarEmailRenderer(id);
        html += (espaco + DeletarEmailRenderer(id));

        return html;
    }

    /* endregion :: Functions */

    /* region :: Stores */

    var terminalEmailStore = new window.Ext.data.JsonStore({
        id: 'terminalEmailStore',
        name: 'terminalEmailStore',
        root: 'Items',
        url: '<%= Url.Action("ObterTerminais", "EnvioDocumentacaoTrem") %>',
        fields: [
            'Id',
            'DescResumida'
        ],
        listeners: {
            load: function (store) {
                var rt = store.recordType;
                store.insert(0, new rt({}));
            }
        }
    });
    window.terminalEmailStore = terminalEmailStore;

    /* endregion :: Stores */

    /* region :: Filtros */

    var txtIdConfiguracao = {
        xtype: 'textfield',
        name: 'txtIdConfiguracao',
        id: 'txtIdConfiguracao',
        fieldLabel: 'ID Configuração',
        autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '3' },
        maskRe: /[0-9]/,
        style: 'text-transform: uppercase',
        width: 220,
        value: configuracaoId,
        disabled: true
    };

    var txtAreaOperacionalEnvioEmailModal = {
        xtype: 'textfield',
        name: 'txtAreaOperacionalEnvioEmailModal',
        id: 'txtAreaOperacionalEnvioEmailModal',
        fieldLabel: 'Área Operacional Envio',
        autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '6' },
        style: 'text-transform: uppercase',
        width: 220,
        value: configuracaoAreaOperacional
    };

    var ddlTerminalEmailModal = {
        xtype: 'combo',
        id: 'ddlTerminalEmailModal',
        name: 'ddlTerminalEmailModal',
        forceSelection: false,
        store: window.terminalEmailStore,
        triggerAction: 'all',
        mode: 'remote',
        typeAhead: true,
        minChars: 1,
        fieldLabel: 'Terminal Recebimento',
        displayField: 'DescResumida',
        valueField: 'Id',
        width: 220,
        editable: true,
        selectOnFocus:true,
        tpl: '<tpl for="."><div class="x-combo-list-item">{DescResumida}&nbsp;</div></tpl>',
        value: configuracaoTerminal.DescResumida
    };

    var txtDiasValidade = {
        xtype: 'textfield',
        name: 'txtDiasValidade',
        id: 'txtDiasValidade',
        fieldLabel: 'Dias Validade',
        autoCreate: { tag: 'input', type: 'text', autocomplete: 'off' },
        style: 'text-transform: lowercase',
        maskRe: /[0-9]/,
        width: 220,
        value: configuracaoValidadeDocumentacao
    };
    
    var chkRelatorioLaudoMercadoria = {
        xtype: 'checkbox',
        fieldLabel: 'Relatório Laudo Mercadoria',
        id: 'chkRelatorioLaudoMercadoria',
        cls: 'chkRelatorioLaudoMercadoria',
        name: 'chkRelatorioLaudoMercadoria',
        validateField: true,
        checked: configuracaoRelatorioLaudoMercadoria
    };
    
    var chkEnvioChegada = {
        xtype: 'checkbox',
        fieldLabel: 'Enviar na Chegada',
        id: 'chkEnvioChegada',
        cls: 'chkEnvioChegada',
        name: 'chkEnvioChegada',
        validateField: true,
        checked: configuracaoEnvioChegada
    };
    
    var chkEnvioSaida = {
        xtype: 'checkbox',
        fieldLabel: 'Enviar na Saída',
        id: 'chkEnvioSaida',
        cls: 'chkEnvioSaida',
        name: 'chkEnvioSaida',
        validateField: true,
        checked: configuracaoEnvioSaida
    };
    
    var txtEmailInclusao = {
        xtype: 'textfield',
        name: 'txtEmailInclusao',
        id: 'txtEmailInclusao',
        fieldLabel: 'Novo E-mail',
        autoCreate: { tag: 'input', type: 'text', autocomplete: 'off' },
        style: 'text-transform: lowercase',
        width: 220
    };

    var btnAdicionarEmail = new Ext.Button({
        id: 'btnAdicionarEmail',
        name: 'btnAdicionarEmail',
        text: 'Adicionar E-mail',
        iconCls: 'icon-new',
        handler: AdicionarEmail
    });
    
    /* endregion :: Filtros */

    /* region :: Grid de Emails */
    
    var cmEmails = new Ext.grid.ColumnModel({
        defaults: {
            sortable: true
        },
        columns: [
            {
                dataIndex: "IdConfiguracao", 
                hidden: true
            },
            {
                dataIndex: "Id", 
                hidden: true
            },
            new Ext.grid.RowNumberer(),
            {
                header: 'Ações',
                dataIndex: 'Id',
                width: 60,
                align: "center",
                sortable: false,
                renderer: function (value, metaData, record, rowIndex, colIndex, store) {
                    return GerenciarEmailRenderer(record.data.Id);
                }
            },
            {
                header: 'E-mail',
                dataIndex: 'Email',
                width: 150
            },
            {
                header: 'Criado em',
                dataIndex: 'DataCriacao',
                width: 80,
                align: 'left'
            },
            {
                header: 'Ativo',
                dataIndex: 'Ativo',
                width: 50,
                renderer: function (value) {
                    if (value.toUpperCase() == 'S')
                        return 'Sim';
                    else
                        return 'Não';
                }
            }
        ]
    });
    
    gridEmails = new Translogic.PaginatedGrid({
        id: 'grid-emails',
        name: 'grid-emails',
        autoLoadGrid: true,
        region: 'center',
        width: 380,
        height: 200,
        viewConfig: {
            emptyText: 'Não possui dado(s) para exibição.'
        },
        filteringToolbar: [btnAdicionarEmail],
        loadMask: { msg: App.Resources.Web.Carregando },
        fields: [
            'Id',
            'IdConfiguracao',
            'Email', 
            'DataCriacao',
            'Ativo'
        ],
        cm: cmEmails,
        stripeRows: true,
        url: '<%= Url.Action("ObterEmailsConfiguracao", "EnvioDocumentacaoTrem") %>'
    });
    
    gridEmails.getStore().proxy.on('beforeload', function (p, params) {
        params['idConfiguracao'] = configuracaoId;
    });

    /* endregion :: Grid de Emails */

    /* region :: Modal */

    var modal = new Ext.form.FormPanel({
        id: 'form-add-conf-modal',
        labelWidth: 150,
        width: 415,
        resizable: false,
        height: 420,
        bodyStyle: 'padding: 15px',
        items: [
            txtIdConfiguracao,
            txtAreaOperacionalEnvioEmailModal,
            ddlTerminalEmailModal,
            txtDiasValidade,
            chkRelatorioLaudoMercadoria,
            chkEnvioChegada,
            chkEnvioSaida,
            txtEmailInclusao,
            gridEmails
        ],
        buttonAlign: "center",
        buttons: [
            {
                text: "Salvar",
                id: "btnSalvar",
                iconCls: 'icon-save',
                handler: function () {
                    Ext.getCmp("btnSalvar").setDisabled(true);

                    var ddlTerminal = Ext.getCmp("ddlTerminalEmailModal");
                    var terminalId = ddlTerminal.getValue();
                        
                    window.jQuery.ajax({
                        url: '<%= Url.Action("SalvarConfiguracao") %>',
                        type: "POST",
                        data: {
                            idConfiguracao: Ext.getCmp("txtIdConfiguracao").getValue(),
                            areaOperacionalEnvio: Ext.getCmp("txtAreaOperacionalEnvioEmailModal").getValue(),
                            terminal: terminalId,
                            diasValidade: Ext.getCmp("txtDiasValidade").getValue(),
                            gerarRelatorioLaudoMercadoria: Ext.getCmp("chkRelatorioLaudoMercadoria").getValue(),
                            enviarChegada: Ext.getCmp("chkEnvioChegada").getValue(),
                            enviarSaida: Ext.getCmp("chkEnvioSaida").getValue()
                        },
                        dataType: "json",
                        success: function (data) {
                            var iconeCadastro = window.Ext.MessageBox.ERROR;
                            if (data.Success == true) {
                                iconeCadastro = window.Ext.MessageBox.SUCCESS;
                            } else {
                                Ext.getCmp("btnSalvar").setDisabled(false);
                            }

                            window.Ext.Msg.show({
                                title: "Salvar Configuração",
                                msg: data.Message,
                                buttons: window.Ext.Msg.OK,
                                icon: iconeCadastro,
                                minWidth: 200,
                                fn: function () {
                                    if (data.Success) {
                                        Ext.getCmp("form-add-conf").close();
                                        LimparCampos();
                                    }
                                },
                                close: function () {
                                    if (data.Success) {
                                        Ext.getCmp("form-add-conf").close();
                                        LimparCampos();
                                    }
                                }
                            });
                        },
                        error: function (jqXHR, textStatus) {
                            window.Ext.Msg.show({
                                title: "Erro no Servidor",
                                msg: 'Ocorreu um erro no servidor, por favor, tente novamente mais tarde.',
                                buttons: window.Ext.Msg.OK,
                                icon: window.Ext.MessageBox.ERROR,
                                minWidth: 200
                            });
                            Ext.getCmp("form-add-conf").close();
                        }
                    });
                }
            }
        ]
    });

    /* endregion :: Modal*/

    modal.render("div-form-edit-conf");
</script>
