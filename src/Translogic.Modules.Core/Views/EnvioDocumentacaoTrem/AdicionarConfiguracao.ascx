﻿<%@  Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>

<div id="div-form-add-conf"></div>

<script type="text/javascript">
    /* region :: Functions */

    function limpaCampos() {
        Ext.getCmp("txtAreaOperacionalEnvioModal").setValue("");
        Ext.getCmp("ddlTerminalModal").setValue("");
    }

    /* endregion :: Functions */

    /* region :: Stores */

    var terminalStoreModal = new window.Ext.data.JsonStore({
        id: 'terminalStore',
        name: 'terminalStore',
        root: 'Items',
        url: '<%= Url.Action("ObterTerminais", "EnvioDocumentacaoTrem") %>',
        fields: [
                'Id',
                'DescResumida'
            ],
        listeners: {
            load: function (store) {
                var rt = store.recordType;
                store.insert(0, new rt({}));
            }
        }
    });
    window.terminalStoreModal = terminalStoreModal;

    /* endregion :: Stores */

    /* region :: Filtros */

    var txtAreaOperacionalEnvioModal = {
        xtype: 'textfield',
        name: 'txtAreaOperacionalEnvioModal',
        id: 'txtAreaOperacionalEnvioModal',
        fieldLabel: 'Área Operacional Envio',
        autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '6' },
        style: 'text-transform: uppercase',
        width: 200
    };

    var ddlTerminalModal = {
        xtype: 'combo',
        id: 'ddlTerminalModal',
        name: 'ddlTerminalModal',
        forceSelection: true,
        store: window.terminalStoreModal,
        triggerAction: 'all',
        mode: 'remote',
        typeAhead: true,
        minChars: 1,
        fieldLabel: 'Terminal Recebimento',
        displayField: 'DescResumida',
        valueField: 'Id',
        width: 200,
        editable: true,
        tpl: '<tpl for="."><div class="x-combo-list-item">{DescResumida}&nbsp;</div></tpl>'
    };

    var chkRelatorioLaudoMercadoria = {
        xtype: 'checkbox',
        fieldLabel: 'Relatório Laudo Mercadoria',
        id: 'chkRelatorioLaudoMercadoria',
        cls: 'chkRelatorioLaudoMercadoria',
        name: 'chkRelatorioLaudoMercadoria',
        validateField: true,
        checked: false
    };

    var chkEnvioChegada = {
        xtype: 'checkbox',
        fieldLabel: 'Enviar na Chegada',
        id: 'chkEnvioChegada',
        cls: 'chkEnvioChegada',
        name: 'chkEnvioChegada',
        validateField: true,
        checked: false
    };

    var chkEnvioSaida = {
        xtype: 'checkbox',
        fieldLabel: 'Enviar na Saída',
        id: 'chkEnvioSaida',
        cls: 'chkEnvioSaida',
        name: 'chkEnvioSaida',
        validateField: true,
        checked: true
    };
    
    /* endregion :: Filtros */

    /* region :: Modal */

    var modal = new Ext.form.FormPanel({
        id: 'form-add-conf-modal',
        labelWidth: 150,
        width: 400,
        resizable: false,
        height: 200,
        bodyStyle: 'padding: 15px',
        items: [
                txtAreaOperacionalEnvioModal,
                ddlTerminalModal,
                chkRelatorioLaudoMercadoria,
                chkEnvioChegada,
                chkEnvioSaida
            ],
        buttonAlign: "center",
        buttons: [
                {
                    text: "Salvar",
                    id: "btnSalvar",
                    iconCls: 'icon-save',
                    handler: function () {
                        Ext.getCmp("btnSalvar").setDisabled(true);

                        var ddlTerminal = Ext.getCmp("ddlTerminalModal");
                        var terminalId = ddlTerminal.getValue();

                        window.jQuery.ajax({
                            url: '<%= Url.Action("SalvarConfiguracao") %>',
                            type: "POST",
                            data: {
                                areaOperacionalEnvio: Ext.getCmp("txtAreaOperacionalEnvioModal").getValue(),
                                terminal: terminalId,
                                gerarRelatorioLaudoMercadoria: Ext.getCmp("chkRelatorioLaudoMercadoria").getValue(),
                                enviarChegada: Ext.getCmp("chkEnvioChegada").getValue(),
                                enviarSaida: Ext.getCmp("chkEnvioSaida").getValue()
                            },
                            dataType: "json",
                            success: function (data) {
                                var iconeCadastro = window.Ext.MessageBox.ERROR;
                                if (data.Success == true) {
                                    iconeCadastro = window.Ext.MessageBox.SUCCESS;
                                } else {
                                    Ext.getCmp("btnSalvar").setDisabled(false);
                                }

                                window.Ext.Msg.show({
                                    title: "Salvar Configuração",
                                    msg: data.Message,
                                    buttons: window.Ext.Msg.OK,
                                    icon: iconeCadastro,
                                    minWidth: 200,
                                    fn: function () {
                                        if (data.Success) {
                                            Ext.getCmp("form-add-conf").close();
                                            limpaCampos();
                                            posicaoRow = -1;
                                        } else {
                                            Ext.getCmp("btnSalvar").setDisabled(false);
                                        }
                                    },
                                    close: function () {
                                        if (data.Success) {
                                            Ext.getCmp("form-add-conf").close();
                                            limpaCampos();
                                            posicaoRow = -1;
                                        }
                                    }
                                });
                            },
                            error: function (jqXHR, textStatus) {
                                window.Ext.Msg.show({
                                    title: "Erro no Servidor",
                                    msg: 'Ocorreu um erro no servidor, por favor, tente novamente mais tarde.',
                                    buttons: window.Ext.Msg.OK,
                                    icon: window.Ext.MessageBox.ERROR,
                                    minWidth: 200
                                });
                            }
                        });
                    }
                }
            ]
    });

    /* endregion :: Modal*/

    modal.render("div-form-add-conf");
</script>