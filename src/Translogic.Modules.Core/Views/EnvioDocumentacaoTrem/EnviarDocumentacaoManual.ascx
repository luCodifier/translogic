﻿<%@  Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>

<div id="div-form-enviar-documentacao"></div>

<script type="text/javascript">
    /* region :: Functions */

    function limpaCampos() {
        Ext.getCmp("txtNumeroOs").setValue("");
        Ext.getCmp("ddlRecebedor").setValue("");
    }

    /* endregion :: Functions */

    /* region :: Stores */

    var recebedorStore = new Ext.data.JsonStore({
        id: 'recebedorStore',
        name: 'recebedorStore',
        root: 'Items',
        autoLoad: true,
        proxy: new Ext.data.HttpProxy({
            url: '<%= Url.Action("ObterTerminaisPorOs", "EnvioDocumentacaoTrem") %>',
            timeout: 600000
        }),
        fields: [
                'Id',
                'DescResumida'
            ],
        listeners: {
            load: function (store) {
                var rt = store.recordType;
                store.insert(0, new rt({}));
            }
        }
    });
    window.recebedorStore = recebedorStore;
    
    /* endregion :: Stores */

    /* region :: Filtros */

    var txtNumeroOs = {
        xtype: 'textfield',
        name: 'txtNumeroOs',
        id: 'txtNumeroOs',
        fieldLabel: 'Número OS',
        autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '15' },
        maskRe: /[0-9]/,
        style: 'text-transform: uppercase',
        width: 125,
        listeners: {
            change: {
                fn: function () {
                    var value = this.getValue();
                    recebedorStore.load({ params: { numeroOs: value },
                        callback: function (options, success, response, records) {
                            if (success) {
                                var combobox = Ext.getCmp("ddlRecebedor");

                                if (recebedorStore.getCount() == 0) {
                                    combobox.setValue("");
                                } else {
                                    combobox.setValue("TODOS");
                                }
                            }
                        }
                    });
                }
            }
        }
    };

    var ddlRecebedor = {
        xtype: 'combo',
        id: 'ddlRecebedor',
        name: 'ddlRecebedor',
        store: window.recebedorStore,
        triggerAction: 'all',
        mode: 'local',
        fieldLabel: 'Recebedor',
        displayField: 'DescResumida',
        valueField: 'Id',
        width: 200,
        editable: false,
        tpl: '<tpl for="."><div class="x-combo-list-item">{DescResumida}&nbsp;</div></tpl>'
    };

    /* endregion :: Filtros */

    /* region :: Modal */

    var modal = new Ext.form.FormPanel({
        id: 'form-add-conf-modal',
        labelWidth: 150,
        width: 400,
        resizable: false,
        height: 140,
        bodyStyle: 'padding: 15px',
        items: [
                txtNumeroOs,
                ddlRecebedor
            ],
        buttonAlign: "center",
        buttons: [
                {
                    text: "Enviar",
                    id: "btnEnviarModal",
                    iconCls: 'icon-save',
                    handler: function () {
                        Ext.getCmp("btnEnviarModal").setDisabled(true);

                        var ddlRecebedor = Ext.getCmp("ddlRecebedor");
                        var recebedorId = ddlRecebedor.getValue();

                        window.jQuery.ajax({
                            url: '<%= Url.Action("EnviarDocumentacaoManualPorOsRecebedor") %>',
                            type: "POST",
                            data: {
                                numeroOs: Ext.getCmp("txtNumeroOs").getValue(),
                                recebedorId: recebedorId
                            },
                            dataType: "json",
                            success: function (data) {
                                var iconeCadastro = window.Ext.MessageBox.ERROR;
                                if (data.Success == true) {
                                    iconeCadastro = window.Ext.MessageBox.SUCCESS;
                                } else {
                                    Ext.getCmp("btnEnviarModal").setDisabled(false);
                                }

                                window.Ext.Msg.show({
                                    title: "Enviar Documentação Manual",
                                    msg: data.Message,
                                    buttons: window.Ext.Msg.OK,
                                    icon: iconeCadastro,
                                    minWidth: 200,
                                    fn: function () {
                                        if (data.Success) {
                                            Ext.getCmp("form-enviar").close();
                                            limpaCampos();
                                            posicaoRow = -1;
                                        } else {
                                            Ext.getCmp("btnEnviarModal").setDisabled(false);
                                        }
                                    },
                                    close: function () {
                                        if (data.Success) {
                                            // Fechando a Modal
                                            Ext.getCmp("form-enviar").close();
                                            limpaCampos();
                                            posicaoRow = -1;
                                        }
                                    }
                                });
                            },
                            error: function (jqXHR, textStatus) {
                                window.Ext.Msg.show({
                                    title: "Erro no Servidor",
                                    msg: 'Ocorreu um erro no servidor, por favor, tente novamente mais tarde.',
                                    buttons: window.Ext.Msg.OK,
                                    icon: window.Ext.MessageBox.ERROR,
                                    minWidth: 200
                                });
                            }
                        });
                    }
                }
            ]
    });
    
    /* endregion :: Modal*/

    modal.render("div-form-enviar-documentacao");
</script>