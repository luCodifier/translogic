﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Interna.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content runat="server" ID="HeadContent" ContentPlaceHolderID="HeadContent">
    <div id="header-content">
        <h1>
            Envio de Documentação</h1>
        <small>Parametrização</small>
        <br />
    </div>
    <script type="text/javascript" language="javascript">
        var formModal = null;
        var grid = null;
        var posicaoRow = -1;

        /* region :: Functions */

        function Pesquisar() {
            grid.getStore().load();
        }

        function Limpar() {
            grid.getStore().removeAll();
            Ext.getCmp("filtros").getForm().reset();
        }
        
        function Enviar() {
            posicaoRow = 1;
            formModal = new Ext.Window({
                id: 'form-enviar',
                title: 'Enviar Documentação Manual',
                modal: true,
                resizable: false,
                width: 400,
                height: 170,
                autoLoad:
                    {
                        url: '<%= Url.Action("EnviarDocumentacaoManual", "EnvioDocumentacaoTrem") %>',
                        scripts: true,
                        callback: function (el, success, c) {
                            posicaoRow = -1;
                            if (!success) {
                                 formModal.close();
                            }
                        }
                    },
                listeners: {
                    destroy: function () {
                        if (grid) {
                            grid.getStore().reload();
                        }
                    }
                }
            });

            formModal.show(this);
        }

        function HabilitarDesabilitarConfiguracao(id) {
            Ext.Ajax.request({
                    url: '<%= Url.Action("HabilitarConfiguracao", "EnvioDocumentacaoTrem") %>',
                    method: "POST",
                    params: { id: id },
                    success: function(response, options) {
                        var result = Ext.decode(response.responseText);
                        
                        if (result.Success == true) {
                            Pesquisar();

                            Ext.Msg.show({
                                title: 'Aviso',
                                msg: result.Message,
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.INFO
                            });
                        } else {
                            Ext.Msg.show({
                                title: 'Aviso',
                                msg: result.Message,
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.ERROR
                            });
                        }

                        Ext.getBody().unmask();
                    },
                    failure: function(response, options) {
                        Ext.Msg.show({
                            title: 'Aviso',
                            msg: response.Message,
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.ERROR
                        });

                        Ext.getBody().unmask();
                    }
                });
        }

        function DeletarConfiguracao(id) {
            Ext.Ajax.request({
                    url: '<%= Url.Action("DeletarConfiguracao", "EnvioDocumentacaoTrem") %>',
                    method: "POST",
                    params: { id: id },
                    success: function(response, options) {
                        var result = Ext.decode(response.responseText);
                        
                        if (result.Success == true) {
                            Pesquisar();

                            Ext.Msg.show({
                                title: 'Aviso',
                                msg: result.Message,
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.INFO
                            });
                        } else {
                            Ext.Msg.show({
                                title: 'Aviso',
                                msg: result.Message,
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.ERROR
                            });
                        }

                        Ext.getBody().unmask();
                    },
                    failure: function(response, options) {
                        Ext.Msg.show({
                            title: 'Aviso',
                            msg: response.Message,
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.ERROR
                        });

                        Ext.getBody().unmask();
                    }
                });
        }
        
        function AdicionarConfiguracao() {
            posicaoRow = 1;
            formModal = new Ext.Window({
                id: 'form-add-conf',
                title: 'Adicionar Configuração',
                modal: true,
                resizable: false,
                width: 400,
                height: 225,
                autoLoad:
                    {
                        url: '<%= Url.Action("AdicionarConfiguracao", "EnvioDocumentacaoTrem") %>',
                        scripts: true,
                        callback: function (el, success, c) {
                            posicaoRow = -1;
                            if (!success) {
                                 formModal.close();
                            }
                        }
                    },
                listeners: {
                    destroy: function () {
                        if (grid) {
                            grid.getStore().reload();
                        }
                    }
                }
            });

            formModal.show(this);
        }
        
        function GerenciarEmails(id) {
            formModal = new Ext.Window({
                id: 'form-add-conf',
                title: 'Gerenciamento da Configuração',
                modal: true,
                resizable: false,
                width: 420,
                height: 455,
                autoLoad:
                    {
                        url: '<%= Url.Action("GerenciarConfiguracao", "EnvioDocumentacaoTrem") %>',
                         params  : { id: id },
                        scripts: true,
                        callback: function (el, success, c) {
                            posicaoRow = -1;
                            if (!success) {
                                 formModal.close();
                            }
                        }
                    },
                listeners: {
                    destroy: function () {
                        if (grid) {
                            grid.getStore().reload();
                        }
                    }
                }
            });

            formModal.show(this);
        }

        function ListarEmailsRenderer(emails) {
            var html = '';
            
            if (emails !== undefined) {
                html = '<ul>';
                
                var emailsSeparados = emails.split(';');
                var quantidadeEmails = emailsSeparados.length;
                for (var i = 0; i < quantidadeEmails; i ++) {
                    html += ('<li>' + emailsSeparados[i] + '</li>');
                }

                html += '</ul>';
            }
            
            return html;
        }
        
        function GerenciarEmailsRenderer(id) {
            return "<img style='cursor: pointer; cursor: hand;' title='Gerenciar' src='<%=Url.Images("Icons/email_edit.png") %>' alt='Gerenciar' onclick='GerenciarEmails(" + id + ");'>";
        }
        
        function HabilitarRenderer(id) {
            return "<img style='cursor: pointer; cursor: hand;' title='Habilitar/Desativar' src='<%=Url.Images("Icons/accept.png") %>' alt='Habilitar/Desativar' onclick='HabilitarDesabilitarConfiguracao(" + id + ");'>";
        }
        
        function DeletarConfiguracaoRenderer(id) {
            return "<img style='cursor: pointer; cursor: hand;' title='Deletar' src='<%=Url.Images("Icons/delete.png") %>' alt='Deletar' onclick='DeletarConfiguracao(" + id + ");'>";
        }

        function GerenciarRenderer(id) {
            var html = '';
            var espaco = '&nbsp';
            
            html += GerenciarEmailsRenderer(id);
            html += (espaco + HabilitarRenderer(id));
            html += (espaco + DeletarConfiguracaoRenderer(id));

            return html;
        }

        /* endregion :: Functions */

        /* region :: Stores */

        var terminalStore = new window.Ext.data.JsonStore({
            id: 'terminalStore',
            name: 'terminalStore',
            root: 'Items',
            url: '<%= Url.Action("ObterTerminais", "EnvioDocumentacaoTrem") %>',
            fields: [
                'Id',
                'DescResumida'
            ],
            listeners: {
                load: function (store) {
                    var rt = store.recordType;
                    store.insert(0, new rt({}));
                }
            }
        });
        window.terminalStore = terminalStore;

        var statusStore = new Ext.data.ArrayStore({
            id: 0,
            fields: [
                'Id',
                'Text'
            ],
            data: [
                ['', 'Todos'],
                ['true', 'Sim'],
                ['false', 'Não']
            ]
        });
        window.statusStore = statusStore;   

        /* endregion :: Stores */

        /* region :: Filtros */

        var txtAreaOperacionalEnvio = {
            xtype: 'textfield',
            name: 'txtAreaOperacionalEnvio',
            id: 'txtAreaOperacionalEnvio',
            fieldLabel: 'Área Operacional Envio',
            autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '6' },
            style: 'text-transform: uppercase',
            width: 125
        };

        var arrAreaOperacionalEnvio = {
            width: 130,
            layout: 'form',
            border: false,
            items: [txtAreaOperacionalEnvio]
        };

        var ddlTerminal = {
            xtype: 'combo',
            id: 'ddlTerminal',
            name: 'ddlTerminal',
            forceSelection: true,
            store: window.terminalStore,
            triggerAction: 'all',
            mode: 'remote',
            typeAhead: true,
            minChars: 1,
            fieldLabel: 'Terminal Recebimento',
            displayField: 'DescResumida',
            valueField: 'Id',
            width: 200,
            editable: true,
            tpl: '<tpl for="."><div class="x-combo-list-item">{DescResumida}&nbsp;</div></tpl>'
        };

        var arrTerminal = {
            width: 205,
            layout: 'form',
            border: false,
            items: [ddlTerminal]
        };

        var ddlStatus = new Ext.form.ComboBox({
            id: 'ddlStatus',
            name: 'ddlStatus',
            fieldLabel: 'Ativo',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            store: window.statusStore,
            valueField: 'Id',
            displayField: 'Text',
            width: 125
        });

        var arrStatus = {
            width: 130,
            layout: 'form',
            border: false,
            items: [ddlStatus]
        };

        var arrlinha1 = {
            layout: 'column',
            border: false,
            items: [
                arrAreaOperacionalEnvio,
                arrTerminal,
                arrStatus
            ]
        };

        var arrCampos = new Array();
        arrCampos.push(arrlinha1);

        var filtros = new Ext.form.FormPanel({
            id: 'filtros',
            title: 'Filtros',
            name: 'filtros',
            region: 'center',
            bodyStyle: 'padding: 15px',
            initEl: function(el) {
                this.el = Ext.get(el);
                this.id = this.el.id || Ext.id();
                if (this.standardSubmit) {
                    this.el.dom.action = this.url;
                } else {
                    this.el.on('submit', this.onSubmit, this);
                }
                this.el.addClass('x-form');
            },
            labelAlign: 'top',
            border: false,
            items: [arrCampos],
            buttonAlign: 'center',
            buttons: [
                {
                    name: 'btnPesquisar',
                    id: 'btnPesquisar',
                    text: 'Pesquisar',
                    iconCls: 'icon-find',
                    handler: Pesquisar
                },
                {
                    name: 'btnLimpar',
                    id: 'btnLimpar',
                    text: 'Limpar',
                    handler: Limpar
                },
                {
                    name: 'btnEnviar',
                    id: 'btnEnviar',
                    text: 'Enviar',
                    handler: Enviar
                }
            ]
        });

        /* endregion :: Filtros */

        /* region :: Grid */

        var cm = new Ext.grid.ColumnModel({
            defaults: {
                sortable: true
            },
            columns: [
                {
                    dataIndex: "Id", 
                    hidden: true
                },
                new Ext.grid.RowNumberer(),
                {
                    header: 'Ações',
                    dataIndex: 'Id',
                    width: 65,
                    align: "center",
                    sortable: false,
                    renderer: function (value, metaData, record, rowIndex, colIndex, store) {
                        return GerenciarRenderer(record.data.Id);
                    }
                },
                {
                    header: 'Envio em',
                    dataIndex: 'AreaOperacionalEnvio',
                    width: 100
                },
                {
                    header: 'CNPJ Recebedor',
                    dataIndex: 'CnpjTerminal',
                    width: 130
                },
                {
                    header: 'Recebedor',
                    dataIndex: 'Terminal',
                    width: 100
                },
                {
                    header: 'E-mails',
                    dataIndex: 'Emails',
                    width: 500,
                    sortable: false,
                    renderer: function (value, metaData, record, rowIndex, colIndex, store) {
                        return ListarEmailsRenderer(record.data.Emails);
                    }
                },
                {
                    header: 'Criado em',
                    dataIndex: 'DataCriacao',
                    width: 100,
                    align: 'left'
                },
                {
                    header: 'Ativo',
                    dataIndex: 'Ativo',
                    width: 80,
                    renderer: function (value) {
                        if (value.toUpperCase() == 'S')
                            return 'Sim';
                        else
                            return 'Não';
                    }
                }
            ]
        });
        
        var btnAdicionarConfiguracoes = new Ext.Button({
            id: 'btnAdicionarConfiguracoes',
            name: 'btnAdicionarConfiguracoes',
            text: 'Adicionar Configuração',
            iconCls: 'icon-new',
            handler: AdicionarConfiguracao
        });

        grid = new Translogic.PaginatedGrid({
            id: 'grid',
            name: 'grid',
            autoLoadGrid: true,
            region: 'center',
            viewConfig: {
                emptyText: 'Não possui dado(s) para exibição.'
            },
            filteringToolbar: [btnAdicionarConfiguracoes],
            loadMask: { msg: App.Resources.Web.Carregando },
            fields: [
                'Id',
                'AreaOperacionalEnvio',
                'CnpjTerminal',
                'Terminal',
                'Emails', 
                'DataCriacao',
                'Ativo'
            ],
            cm: cm,
            stripeRows: true,
            url: '<%= Url.Action("ObterConfiguracoes", "EnvioDocumentacaoTrem") %>'
        });

        grid.getStore().proxy.on('beforeload', function (p, params) {
            var ddlTerminal = Ext.getCmp("ddlTerminal");
            var terminalId = ddlTerminal.getValue();

            var ddlStatus = Ext.getCmp("ddlStatus");
            var statusId = ddlStatus.getValue();

            params['areaOperacionalEnvio'] = Ext.getCmp("txtAreaOperacionalEnvio").getValue();
            params['terminalDestino'] = terminalId;
            params['ativo'] = statusId;
        });

        /* endregion :: Grid */

        /* region :: Render */
        
        new Ext.Viewport({
            layout: 'border',
            margins: 10,
            items: [{
                    region: 'north',
                    height: 200,
                    autoScroll: true,
                    items: [{
                            region: 'center',
                            applyTo: 'header-content'
                        },
                        filtros]
                },
                grid]
        });

    /* endregion :: Render */
        
    </script>
</asp:Content>