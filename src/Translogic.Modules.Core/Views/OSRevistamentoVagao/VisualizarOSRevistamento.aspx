﻿<%@ Page Title="" Language="C#" Inherits="System.Web.Mvc.ViewPage" MasterPageFile="~/Views/Shared/Interna.master" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        //***************************** VARIÁVEIS E INICIALIZAÇÃO**************//
        var idOs = '<%=ViewData["idOs"] %>';
      
        //***************************** CAMPOS *****************************//

        var lblNumOS = {
            xtype: 'label',
            name: 'lblNumOS',
            id: 'lblNumOS',
            fieldLabel: 'Número OS'
        };

        var lblOsParcial = {
            xtype: 'label',
            name: 'lblOsParcial',
            id: 'lblOsParcial',
            fieldLabel: 'Número OS Parcial'
        };

        var lblDataHora = {
            xtype: 'label',
            name: 'lblDataHora',
            id: 'lblDataHora',
            fieldLabel: 'Data e hora'
        };

        var lblHoraInicio = {
            xtype: 'label',
            name: 'lblHoraInicio',
            id: 'lblHoraInicio',
            fieldLabel: 'Hora início'
        };

        var lblHoraTermino = {
            xtype: 'label',
            name: 'lblHoraTermino',
            id: 'lblHoraTermino',
            fieldLabel: 'Hora término'
        };

        var lblHoraEntrega = {
            xtype: 'label',
            name: 'lblHoraEntrega',
            id: 'lblHoraEntrega',
            fieldLabel: 'Hora de entrega para fornecedor'
        };

        var lblLinha = {
            xtype: 'label',
            name: 'lblLinha',
            id: 'lblLinha',
            fieldLabel: 'Linha'
        };

        var lblConvocacao = {
            xtype: 'label',
            name: 'lblConvocacao',
            id: 'lblConvocacao',
            fieldLabel: 'Convocação'
        };

        var lblQuantVagoes = {
            xtype: 'label',
            name: 'lblQuantVagoes',
            id: 'lblQuantVagoes',
            fieldLabel: 'Quantidade de Vagões'
        };

        var lblFornecedor = {
            xtype: 'label',
            name: 'lblFornecedor',
            id: 'lblFornecedor',
            fieldLabel: 'Fornecedor'
        };

        var lblLocal = {
            xtype: 'label',
            name: 'lblLocal',
            id: 'lblLocal',
            fieldLabel: 'Local'
        };

        var lblQuantVagoesVedados = {
            xtype: 'label',
            name: 'lblQuantVagoesVedados',
            id: 'lblQuantVagoesVedados',
            fieldLabel: 'Quantidade de vagões vedados'
        };

        var lblQuantVagoesGambitados = {
            xtype: 'label',
            name: 'lblQuantVagoesGambitados',
            id: 'lblQuantVagoesGambitados',
            fieldLabel: 'Quantidade de vagões gambitados'
        };

        var lblHaVagoesCarregados = {
            xtype: 'label',
            name: 'lblHaVagoesCarregados',
            id: 'lblHaVagoesCarregados',
            fieldLabel: 'Há vagões carregados entre os vazios?'
        };

        var lblQuantVazios = {
            xtype: 'label',
            name: 'lblQuantVazios',
            id: 'lblQuantVazios',
            fieldLabel: 'Quantidade de carregados entre os vazios'
        };

        var lblNomeManutencao = {
            xtype: 'label',
            name: 'lblNomeManutencao',
            id: 'lblNomeManutencao',
            fieldLabel: 'Posto de manutenção. Nome'
        };

        var lblMatriculaManutencao = {
            xtype: 'label',
            name: 'lblMatriculaManutencao',
            id: 'lblMatriculaManutencao',
            fieldLabel: 'Posto de manutenção. Matrícula'
        };

        var lblNomeEstacao = {
            xtype: 'label',
            name: 'lblNomeEstacao',
            id: 'lblNomeEstacao',
            fieldLabel: 'Fornecedor. Nome'
        };

        var lblMatriculaEstacao = {
            xtype: 'label',
            name: 'lblMatriculaEstacao',
            id: 'lblMatriculaEstacao',
            fieldLabel: 'Fornecedor. Matrícula'
        };


        //***************************** GRID VAGÕES**********************//

        var gridVagoes = new Translogic.PaginatedGrid({
            id: 'gridVagoes',
            name: 'gridVagoes',
            autoLoadGrid: true,
            viewConfig: {
                forceFit: true,
                emptyText: 'Não possui dado(s) para exibição.'
            },
            height: 200,
            autoScroll: true,
            fields: [
                    'IdItem',
                    'Serie',
                    'NumeroVagao',
                    'ConcluidoDescricao',
                    'RetirarDescricao',
                    'ComproblemaDescricao',
                    'Observacao'                 
                ],
            url: '<%= Url.Action("ObterVagoesDaOs", "OSRevistamentoVagao") %>',
            columns: [
				    new Ext.grid.RowNumberer(),
                    { header: 'Série', dataIndex: "Serie", sortable: true, width: 80 },
                    { header: 'Nº Vagão', dataIndex: "NumeroVagao", sortable: true, width: 80 },
                    { header: 'Serviço concluído', dataIndex: "ConcluidoDescricao", sortable: true, width: 80 },
                    { header: 'Retirar vagão', dataIndex: "RetirarDescricao", sortable: true, width: 80 },
                    { header: 'Vedação ou Gambitagem com problema', dataIndex: "ComproblemaDescricao", sortable: true, width: 160 },
                    { header: 'Observação', dataIndex: "Observacao", sortable: false, width: 300 }
                ]
        });

        gridVagoes.getStore().proxy.on('beforeload', function (p, params) {
            params['idOs'] = idOs;
        });

        //***************************** LAYOUT *****************************//


        var formlblNumOS = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            width: 95,
            items: [lblNumOS]
        };

        var formlblOsParcial = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            width: 140,
            items: [lblOsParcial]
        };

        var formlblDataHora = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            width: 130,
            items: [lblDataHora]
        };

        var formlblHoraInicio = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            width: 105,
            items: [lblHoraInicio]
        };

        var formlblHoraTermino = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            width: 95,
            items: [lblHoraTermino]
        };

        var formlblHoraEntrega = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            width: 250,
            items: [lblHoraEntrega]
        };

        var formlblLinha = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            width: 150,
            items: [lblLinha]
        };

        var formlblConvocacao = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            width: 150,
            items: [lblConvocacao]
        };

        var formlblQuantVagoes = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            width: 200,
            items: [lblQuantVagoes]
        };

        var formlblFornecedor = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            width: 200,
            items: [lblFornecedor]
        };

        var formlblLocal = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            width: 42,
            items: [lblLocal]
        };

        var formlblQuantVagoesVedados = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            width: 200,
            items: [lblQuantVagoesVedados]
        };

        var formlblQuantVagoesGambitados = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            width: 200,
            items: [lblQuantVagoesGambitados]
        };

        var formlblHaVagoesCarregados = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            width: 220,
            items: [lblHaVagoesCarregados]
        };

        var formlblQuantVazios = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            width: 250,
            items: [lblQuantVazios]
        };

        var formlblNomeManutencao = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            width: 210,
            items: [lblNomeManutencao]
        };

        var formlblMatriculaManutencao = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            width: 210,
            items: [lblMatriculaManutencao]
        };

        var formlblNomeEstacao = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            width: 210,
            items: [lblNomeEstacao]
        };

        var formlblMatriculaEstacao = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            width: 210,
            items: [lblMatriculaEstacao]
        };

        //Containers
        var containerLinha1 = {
            layout: 'column',
            border: true,
            items: [formlblNumOS, formlblOsParcial, formlblDataHora, formlblHoraInicio, formlblHoraTermino, formlblHoraEntrega]
        };

        var containerLinha2 = {
            layout: 'column',
            border: true,
            items: [formlblLinha, formlblConvocacao, formlblQuantVagoes]
        };

        var containerLinha3 = {
            layout: 'column',
            border: true,
            items: [gridVagoes]
        };

        var containerLinha4 = {
            layout: 'column',
            border: true,
            items: [formlblFornecedor, formlblLocal, formlblQuantVagoesVedados, formlblQuantVagoesGambitados]
        };

        var containerLinha5 = {
            layout: 'column',
            border: true,
            items: [formlblHaVagoesCarregados, formlblQuantVazios]
        };

        var containerLinha6 = {
            layout: 'column',
            border: true,
            items: [formlblNomeManutencao, formlblMatriculaManutencao, formlblNomeEstacao, formlblMatriculaEstacao]
        };

        //***************************** FUNÇÕES*****************************//
        function GetOs(idOs) {

            Ext.getBody().mask("Processando dados...", "x-mask-loading");

            var jsonRequest = $.toJSON({ idOs: idOs });

            $.ajax({
                url: '<%= Url.Action("ObterOs", "OSRevistamentoVagao") %>',
                type: "POST",
                dataType: 'json',
                data: jsonRequest,
                timeout: 300000,
                contentType: "application/json; charset=utf-8",
                failure: function (conn, data) {
                    Ext.Msg.alert("Mensagem de Erro", "Ocorreu um erro inesperado.");
                    Ext.getBody().unmask();
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.status);
                    Ext.getBody().unmask();
                },
                success: function (result) {

                    Ext.getCmp("lblNumOS").setText(result.IdOs);
                    Ext.getCmp("lblOsParcial").setText(result.IdOsParcial);
                    Ext.getCmp("lblDataHora").setText(result.Data);
                    Ext.getCmp("lblHoraInicio").setText(result.HoraInicio);
                    Ext.getCmp("lblHoraTermino").setText(result.HoraTermino);
                    Ext.getCmp("lblHoraEntrega").setText(result.HoraEntrega);
                    Ext.getCmp("lblLinha").setText(result.Linha);
                    Ext.getCmp("lblConvocacao").setText(result.Convocacao);
                    Ext.getCmp("lblQuantVagoes").setText(result.QtdeVagoes);
                    Ext.getCmp("lblFornecedor").setText(result.Fornecedor);
                    Ext.getCmp("lblLocal").setText(result.LocalServico);
                    Ext.getCmp("lblQuantVagoesVedados").setText(result.QtdeVagoesVedados);
                    Ext.getCmp("lblQuantVagoesGambitados").setText(result.QtdeVagoesGambitados);
                    Ext.getCmp("lblQuantVazios").setText(result.QtdeVagoesVazios);
                    if (result.QtdeVagoesVazios > 0)
                        Ext.getCmp("lblHaVagoesCarregados").setText("Sim");
                    else
                        Ext.getCmp("lblHaVagoesCarregados").setText("Não");

                    Ext.getCmp("lblNomeManutencao").setText(result.NomeManutencao);
                    Ext.getCmp("lblMatriculaManutencao").setText(result.MatriculaManutencao);
                    Ext.getCmp("lblNomeEstacao").setText(result.NomeEstacao);
                    Ext.getCmp("lblMatriculaEstacao").setText(result.MatriculaEstacao);

                    Ext.getBody().unmask();
                }

            });
        }
        
        function Voltar() {
            window.history.back();
        }

        //***************************** BOTOES *****************************//
       
        var btnVoltar = {
            name: 'btnVoltar',
            id: 'btnVoltar',
            text: 'Voltar',
            iconCls: 'icon-left',
            handler: Voltar
        };

        //***************************** PAINEL *****************************//
        var panelForm = new Ext.form.FormPanel({
            id: 'panelForm',
            layout: 'form',
            labelAlign: 'top',
            border: false,
            autoHeight: true,
            title: "Gerar Ordem de Serviço",
            region: 'center',
            bodyStyle: 'padding: 15px',
            items: [containerLinha1, containerLinha2, containerLinha3, containerLinha4, containerLinha5, containerLinha6],
            buttonAlign: "center",
            buttons: [btnVoltar]
        });

        //***************************** RENDER *****************************//
        Ext.onReady(function () {
            panelForm.render("divContent");
            
            //Buscar dados da OS e preencher os campos
            GetOs(idOs);
        });
        
    
    </script>
</asp:Content>
<asp:Content ID="MainContent" ContentPlaceHolderID="Content" runat="server">
    <div id="header-content">
        <h1>
            Manutenção</h1>
        <small>Você está em Manutenção > Visualização de Ordem de Serviço</small>
        <br />
    </div>
    <div id="divContent">
    </div>
</asp:Content>
