﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% bool osAberta = (((string)ViewData["Status"]) == "Aberta" || ((string)ViewData["Status"]) == "Parcial"); %>
<% bool TemVagoesVazios = (ViewData["QtdeVagoesVazios"] != null ? (Convert.ToInt32(ViewData["QtdeVagoesVazios"]) > 0 ? true : false) : false);%>
       
<script runat="server">

    protected void Page_Load(object sender, EventArgs e)
    {

    }
</script>
<style type="text/css">
    .style1
    {
        width: 618px;
    }
    .style2
    {
        width: 84px;
        font-size: 11px;
        font-weight: 700;
    }
    .style3
    {
        width: 265px;
        font-size: 11px;
        font-weight: 700;
    }
    .style4
    {
        width: 122px;
        font-size: 11px;
        font-weight: 700;
    }
    .style5
    {
        width: 183px;
    }
    .style7
    {
        width: 154px;
        font-size: 8pt;
    }
    .style10
    {
        width: 178px;
        text-align: right;
    }
    .style11
    {
        width: 181px;
        text-align: right;
    }
    .style12
    {
        width: 177px;
        font-size: 8pt;
    }
    .style13
    {
        width: 277px;
    }
    .style14
    {
        width: 56px;
        font-size: 8pt;
    }
    .style15
    {
        width: 23%;
        font-size: 8pt;
    }
    .style16
    {
        width: 56px;
        height: 41px;
        font-size: 8pt;
    }
    .style17
    {
        height: 41px;
    }
    .style18
    {
        width: 23%;
        height: 43px;
        font-size: 8pt;
    }
    .style19
    {
        height: 43px;
    }
    .style25
    {
        font-size: 8pt;
        text-align: right;
    }
    .style28
    {
        height: 26px;
        font-size: 14px;
    }
    .style29
    {
        width: 154px;
        font-size: 8pt;
        height: 33px;
    }
    .style30
    {
        height: 33px;
    }
    .style43
    {
        width: 618px;
        height: 29px;
    }
    .style44
    {
        width: 130px;
    }
    .style45
    {
        width: 216px;
    }
    .style48
    {
        height: 28px;
        font-size: 14px;
    }
    .style49
    {
        width: 91px;
    }
    .style51
    {
        width: 277px;
        height: 21px;
    }
    .style52
    {
        height: 21px;
    }
    .style53
    {
        width: 108px;
    }
    .style54
    {
        width: 203px;
    }
    .style55
    {
        width: 63px;
    }
</style>
<div>
    <table style="font-family: Arial; font-size: 11px;" cellpadding="0" cellspacing="0">
        <tr>
            <th style="border: 1px solid #000000; text-align: center; font-size: 15px; font-weight: bold;" class="style43">
                Gerar Ordem de Serviço
            </th>
        </tr>
        <tr>
            <td class="style1">&nbsp;</td>
        </tr>
        <tr>
            <td class="style1">
                <table width="100%" cellpadding="0" cellspacing="0" style="height: 37px; border-top-width:1px; border-top-style:solid; border-top-color:#000000; border-right-width:1px; border-right-style:solid; border-right-color:#000000;">
                    <tr style="font-size: 8pt; text-align: center;">
                        <th style="border-bottom-width:1px; border-bottom-style:solid; border-bottom-color:#000000; border-left-width:1px; border-left-style:solid; border-left-color:#000000; width:25%;">Número da O.S.</th>
                        <th style="border-bottom-width:1px; border-bottom-style:solid; border-bottom-color:#000000; border-left-width:1px; border-left-style:solid; border-left-color:#000000; width:25%;">Número da O.S. Parcial</th>
                        <th style="border-bottom-width:1px; border-bottom-style:solid; border-bottom-color:#000000; border-left-width:1px; border-left-style:solid; border-left-color:#000000; width:25%;">Data - Hora</th>
                        <th style="border-bottom-width:1px; border-bottom-style:solid; border-bottom-color:#000000; border-left-width:1px; border-left-style:solid; border-left-color:#000000; width:25%;">Status</th>
                    </tr>
                    <tr>
                        <td class="style4" style="padding: 10px; border-bottom-width:1px; border-bottom-style:solid; border-bottom-color:#000000; border-left-width:1px; border-left-style:solid; border-left-color:#000000; text-align: center; font-size: 10px; vertical-align: middle;">
                            <%=ViewData["IdOs"]%><br />
                        </td>
                        <td class="style3" style="border-bottom-width:1px; border-bottom-style:solid; border-bottom-color:#000000; border-left-width:1px; border-left-style:solid; border-left-color:#000000; text-align: center; font-size: 10px; vertical-align: middle;">
                            <%=ViewData["IdOsParcial"]%><br />
                        </td>
                        <td style="border-bottom-width:1px; border-bottom-style:solid; border-bottom-color:#000000; border-left-width:1px; border-left-style:solid; border-left-color:#000000; text-align: center; font-size: 10px; vertical-align: middle;">
                            <%=ViewData["Data"]%><br />
                        </td>
                        <td class="style2" style="border-bottom-width:1px; border-bottom-style:solid; border-bottom-color:#000000; border-left-width:1px; border-left-style:solid; border-left-color:#000000; text-align: center; font-size: 10px; vertical-align: middle;">
                            <%=ViewData["Status"]%><br />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="style1">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="style1">
                <table width="100%" cellpadding="0" cellspacing="0" style="border-top-width:1px; border-top-style:solid; border-top-color:#000000; border-right-width:1px; border-right-style:solid; border-right-color:#000000;">
                    <tr>
                        <th colspan="3" style="font-size: 14px; height: 27px; border-bottom-width:1px; border-bottom-style:solid; border-bottom-color:#000000; border-left-width:1px; border-left-style:solid; border-left-color:#000000; text-align: center;">
                            Revistamento
                        </th>
                    </tr>
                    <tr class="style25" style="text-align:center;">
                        <td class="style5" style="border-bottom-width:1px; border-bottom-style:solid; border-bottom-color:#000000; border-left-width:1px; border-left-style:solid; border-left-color:#000000;">
                            Hora de início
                        </td>
                        <td class="style5" style="border-bottom-width:1px; border-bottom-style:solid; border-bottom-color:#000000; border-left-width:1px; border-left-style:solid; border-left-color:#000000;">
                            Hora de término
                        </td>
                        <td style="border-bottom-width:1px; border-bottom-style:solid; border-bottom-color:#000000; border-left-width:1px; border-left-style:solid; border-left-color:#000000;">
                            Hora de entrega para fornecedor
                        </td>
                    </tr>
                    <tr>
                        <td class="style5" style="padding: 10px 0px 10px 0px; border-bottom-width:1px; border-bottom-style:solid; border-bottom-color:#000000; border-left-width:1px; border-left-style:solid; border-left-color:#000000; text-align: center; font-size: 10px; vertical-align: middle;">
                            <%=(osAberta ? "" : ViewData["HoraInicio"])%>&nbsp;
                        </td>
                        <td class="style5" style="padding: 10px 0px 10px 0px; border-bottom-width:1px; border-bottom-style:solid; border-bottom-color:#000000; border-left-width:1px; border-left-style:solid; border-left-color:#000000; text-align: center; font-size: 10px; vertical-align: middle;">
                            <%=(osAberta ? "" : ViewData["HoraTermino"])%>&nbsp;
                        </td>
                        <td style="padding: 10px 0px 10px 0px; border-bottom-width:1px; border-bottom-style:solid; border-bottom-color:#000000; border-left-width:1px; border-left-style:solid; border-left-color:#000000; text-align: center; font-size: 10px; vertical-align: middle;">
                            <%=(osAberta ? "" : ViewData["HoraEntrega"])%>&nbsp;
                        </td>
                    </tr>
                    <tr class="style25" style="text-align:center;">
                        <td class="style5" style="border-bottom-width:1px; border-bottom-style:solid; border-bottom-color:#000000; border-left-width:1px; border-left-style:solid; border-left-color:#000000;">
                            Linha
                        </td>
                        <td class="style5" style="border-bottom-width:1px; border-bottom-style:solid; border-bottom-color:#000000; border-left-width:1px; border-left-style:solid; border-left-color:#000000;">
                            Convocação
                        </td>
                        <td style="border-bottom-width:1px; border-bottom-style:solid; border-bottom-color:#000000; border-left-width:1px; border-left-style:solid; border-left-color:#000000;">
                            Quantidade de vagões
                        </td>
                    </tr>
                    <tr>
                        <td class="style5" style="padding: 10px 0px 10px 0px; border-bottom-width:1px; border-bottom-style:solid; border-bottom-color:#000000; border-left-width:1px; border-left-style:solid; border-left-color:#000000; text-align: center; font-size: 10px; vertical-align: middle;">
                            <%=(osAberta ? "" : ViewData["Linha"])%>&nbsp;
                        </td>
                        <td class="style5" style="padding: 10px 0px 10px 0px; border-bottom-width:1px; border-bottom-style:solid; border-bottom-color:#000000; border-left-width:1px; border-left-style:solid; border-left-color:#000000; text-align: center; font-size: 10px; vertical-align: middle;">
                            <%=(osAberta ? "" : ViewData["Convocacao"])%>&nbsp;
                        </td>
                        <td style="padding: 10px 0px 10px 0px; border-bottom-width:1px; border-bottom-style:solid; border-bottom-color:#000000; border-left-width:1px; border-left-style:solid; border-left-color:#000000; text-align: center; font-size: 10px; vertical-align: middle;">
                            <%=(osAberta ? "" : ViewData["QtdeVagoes"])%>&nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="style1">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="style1">
                <table cellpadding="0" cellspacing="0" style="font-size: 11px; border-top-width:1px; border-top-style:solid; border-top-color:#000000; border-right-width:1px; border-right-style:solid; border-right-color:#000000;">
                    <tr style="font-weight: bolder; text-align: center;">
                        <th style="border-bottom-width:1px; border-bottom-style:solid; border-bottom-color:#000000; border-left-width:1px; border-left-style:solid; border-left-color:#000000;" class="style2">
                            Série
                        </th>
                        <th style="border-bottom-width:1px; border-bottom-style:solid; border-bottom-color:#000000; border-left-width:1px; border-left-style:solid; border-left-color:#000000;" class="style5">
                            Número
                        </th>
                        <th style="border-bottom-width:1px; border-bottom-style:solid; border-bottom-color:#000000; border-left-width:1px; border-left-style:solid; border-left-color:#000000;" class="style19">
                            Serviço<br />
                            Concluído<br />                            
                        </th>
                        <th style="border-bottom-width:1px; border-bottom-style:solid; border-bottom-color:#000000; border-left-width:1px; border-left-style:solid; border-left-color:#000000;" class="style19">
                            Retirar
                            <br />
                            vagão
                        </th>
                        <th style="border-bottom-width:1px; border-bottom-style:solid; border-bottom-color:#000000; border-left-width:1px; border-left-style:solid; border-left-color:#000000;" class="style19">
                            Vedação ou<br />
                            Gambitagem com problema
                        </th>
                        <th style="border-bottom-width:1px; border-bottom-style:solid; border-bottom-color:#000000; border-left-width:1px; border-left-style:solid; border-left-color:#000000;" class="style19">
                            Descrição do defeito
                            <br />
                            e/ou retrabalho
                        </th>
                    </tr>
                    <%
                        var list = (List<Translogic.Modules.Core.Domain.Model.Dto.OSRevistamentoVagaoItemDto>)ViewData["Vagoes"];

                        foreach (var item in list)
                        {%>
                     <tr>
                        <td style="border-bottom-width:1px; border-bottom-style:solid; border-bottom-color:#000000; border-left-width:1px; border-left-style:solid; border-left-color:#000000;text-align: center;">
                            <%=item.Serie%>
                        </td>
                        <td style="border-bottom-width:1px; border-bottom-style:solid; border-bottom-color:#000000; border-left-width:1px; border-left-style:solid; border-left-color:#000000;text-align: center;">
                            <%=item.NumeroVagao%>
                        </td>
                        <td style="border-bottom-width:1px; border-bottom-style:solid; border-bottom-color:#000000; border-left-width:1px; border-left-style:solid; border-left-color:#000000;text-align: center;">
                            <%=(osAberta ? "[&nbsp;]" : item.ConcluidoDescricao)%>
                        </td>
                        <td style="border-bottom-width:1px; border-bottom-style:solid; border-bottom-color:#000000; border-left-width:1px; border-left-style:solid; border-left-color:#000000;text-align: center;">
                            <%=(osAberta ? "[&nbsp;]" : item.RetirarDescricao)%>
                        </td>
                        <td style="border-bottom-width:1px; border-bottom-style:solid; border-bottom-color:#000000; border-left-width:1px; border-left-style:solid; border-left-color:#000000;text-align: center;">
                            <%=(osAberta ? "[&nbsp;]" : item.ComproblemaDescricao)%>
                        </td>
                        <td style="border-bottom-width:1px; border-bottom-style:solid; border-bottom-color:#000000; border-left-width:1px; border-left-style:solid; border-left-color:#000000;">
                            <%=(osAberta ? "" : item.Observacao)%>
                        </td>
                    </tr>
                    <%
                        }
                    %>
                </table>
            </td>
        </tr>
        <tr>
            <td class="style1">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="style1">
                <table cellpadding="0" cellspacing="0" style="height:103px; width:100%; border-top-width:1px; border-top-style:solid; border-top-color:#000000; border-right-width:1px; border-right-style:solid; border-right-color:#000000;">
                    <tr>
                        <th style="border-bottom-width:1px; border-bottom-style:solid; border-bottom-color:#000000; border-left-width:1px; border-left-style:solid; border-left-color:#000000; text-align: center;" colspan="2" class="style48">
                            Vedação e Gambitagem
                        </th>
                    </tr>
                    <tr>
                        <td class="style29" style="border-bottom-width:1px; border-bottom-style:solid; border-bottom-color:#000000; border-left-width:1px; border-left-style:solid; border-left-color:#000000;">
                            Fornecedor:
                        </td>
                        <td class="style30" style="padding: 10px 0px 10px 0px; border-bottom-width:1px; border-bottom-style:solid; border-bottom-color:#000000; border-left-width:1px; border-left-style:solid; border-left-color:#000000; font-size: 10px; vertical-align: middle;">
                            <%=ViewData["Fornecedor"]%><br />
                        </td>
                    </tr>
                    <tr>
                        <td class="style7" style="border-bottom-width:1px; border-bottom-style:solid; border-bottom-color:#000000; border-left-width:1px; border-left-style:solid; border-left-color:#000000;">
                            Local de Prestação de Serviço:
                        </td>
                        <td style="padding: 10px 0px 10px 0px; border-bottom-width:1px; border-bottom-style:solid; border-bottom-color:#000000; border-left-width:1px; border-left-style:solid; border-left-color:#000000; font-size: 10px; vertical-align: middle;">
                            <%=ViewData["LocalServico"]%><br />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="style1">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="style1">
                <table cellpadding="0" cellspacing="0" style="height:33px; width:100%; border-top-width:1px; border-top-style:solid; border-top-color:#000000; border-right-width:1px; border-right-style:solid; border-right-color:#000000;">
                    <tr>
                        <td class="style12" style="border-bottom-width:1px; border-bottom-style:solid; border-bottom-color:#000000; border-left-width:1px; border-left-style:solid; border-left-color:#000000;">
                            Quantidade de vagões vedados:
                        </td>
                        <td class="style44" style="padding: 10px 0px 10px 0px; border-bottom-width:1px; border-bottom-style:solid; border-bottom-color:#000000; border-left-width:1px; border-left-style:solid; border-left-color:#000000; font-size: 10px; vertical-align: middle; text-align: center;">
                            <%=(osAberta ? "" : ViewData["QtdeVagoesVedados"])%>&nbsp;
                        </td>
                        <td class="style11" style="border-bottom-width:1px; border-bottom-style:solid; border-bottom-color:#000000; border-left-width:1px; border-left-style:solid; border-left-color:#000000;">
                            <span class="style25">Quantidade de vagões gambitados:</span>
                        </td>
                        <td style="padding: 10px 0px 10px 0px; border-bottom-width:1px; border-bottom-style:solid; border-bottom-color:#000000; border-left-width:1px; border-left-style:solid; border-left-color:#000000; font-size: 10px; vertical-align: middle; text-align: center;">
                            <%=(osAberta ? "" : ViewData["QtdeVagoesGambitados"])%>&nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="style1">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="style1">
                <table cellpadding="0" cellspacing="0" style="height:31px; width:100%; border-top-width:1px; border-top-style:solid; border-top-color:#000000; border-right-width:1px; border-right-style:solid; border-right-color:#000000;">
                    <tr>
                        <td class="style45" style="border-bottom-width:1px; border-bottom-style:solid; border-bottom-color:#000000; border-left-width:1px; border-left-style:solid; border-left-color:#000000;">
                            <span class="style25">Há vagões carregados entre os vazios?</span>
                        </td>
                        <td class="style49" style="border-bottom-width:1px; border-bottom-style:solid; border-bottom-color:#000000; border-left-width:1px; border-left-style:solid; border-left-color:#000000;">
                            <span class="style25">Sim  <%= (TemVagoesVazios ? "[ X ]" : "[&nbsp;&nbsp;&nbsp;]") %>   &nbsp; Não <%=( !TemVagoesVazios ? "[ X ]" : "[&nbsp;&nbsp;&nbsp;]") %></span>
                               
                        </td>
                        <td class="style10" style="border-bottom-width:1px; border-bottom-style:solid; border-bottom-color:#000000; border-left-width:1px; border-left-style:solid; border-left-color:#000000;">
                            <span class="style25">Quantidade:</span>
                        </td>
                        <td style="padding: 10px 0px 10px 0px; border-bottom-width:1px; border-bottom-style:solid; border-bottom-color:#000000; border-left-width:1px; border-left-style:solid; border-left-color:#000000; font-size: 10px; vertical-align: middle; text-align: center;">
                            <%=(osAberta ? "" : ViewData["QtdeVagoesVazios"])%>&nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="style1">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="style1">
                <table cellpadding="0" cellspacing="0" style="width:100%; border-top-width:1px; border-top-style:solid; border-top-color:#000000; border-right-width:1px; border-right-style:solid; border-right-color:#000000;">
                    <tr style="text-align:center;">
                        <th colspan="2" class="style51" style="border-bottom-width:1px; border-bottom-style:solid; border-bottom-color:#000000; border-left-width:1px; border-left-style:solid; border-left-color:#000000;">
                            Posto de manutenção de vagões
                        </th>
                        <th class="style52" style="border-bottom-width:1px; border-bottom-style:solid; border-bottom-color:#000000; border-left-width:1px; border-left-style:solid; border-left-color:#000000;">
                            &nbsp;
                        </th>
                        <th colspan="2" class="style52" style="border-bottom-width:1px; border-bottom-style:solid; border-bottom-color:#000000; border-left-width:1px; border-left-style:solid; border-left-color:#000000;">
                            Fornecedor
                        </th>
                    </tr>
                    <tr>
                        <td class="style14" style="border-bottom-width:1px; border-bottom-style:solid; border-bottom-color:#000000; border-left-width:1px; border-left-style:solid; border-left-color:#000000;">
                            Nome:
                        </td>
                        <td style="padding: 10px 0px 10px 0px; border-bottom-width:1px; border-bottom-style:solid; border-bottom-color:#000000; border-left-width:1px; border-left-style:solid; border-left-color:#000000; font-size: 10px; vertical-align: middle;">
                            <%=(osAberta ? "" : ViewData["NomeManutencao"])%>&nbsp;
                        </td>
                        <td style="border-bottom-width:1px; border-bottom-style:solid; border-bottom-color:#000000; border-left-width:1px; border-left-style:solid; border-left-color:#000000;"></td>
                        <td class="style15" style="border-bottom-width:1px; border-bottom-style:solid; border-bottom-color:#000000; border-left-width:1px; border-left-style:solid; border-left-color:#000000;">
                            Nome:
                        </td>
                        <td style="padding: 10px 0px 10px 0px; border-bottom-width:1px; border-bottom-style:solid; border-bottom-color:#000000; border-left-width:1px; border-left-style:solid; border-left-color:#000000; font-size: 10px; vertical-align: middle; text-align: center;">
                            <%=(osAberta ? "" : ViewData["NomeEstacao"])%>&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td class="style14" style="border-bottom-width:1px; border-bottom-style:solid; border-bottom-color:#000000; border-left-width:1px; border-left-style:solid; border-left-color:#000000;">
                            Matrícula:
                        </td>
                        <td style="padding: 10px 0px 10px 0px; border-bottom-width:1px; border-bottom-style:solid; border-bottom-color:#000000; border-left-width:1px; border-left-style:solid; border-left-color:#000000; font-size: 10px; vertical-align: middle; text-align: center;">
                            <%=(osAberta ? "" : ViewData["MatriculaManutencao"])%>&nbsp;
                        </td>
                        <td style="border-bottom-width:1px; border-bottom-style:solid; border-bottom-color:#000000; border-left-width:1px; border-left-style:solid; border-left-color:#000000;"></td>
                        <td class="style15" style="border-bottom-width:1px; border-bottom-style:solid; border-bottom-color:#000000; border-left-width:1px; border-left-style:solid; border-left-color:#000000;">
                            Matrícula:
                        </td>
                        <td style="padding: 10px 0px 10px 0px; border-bottom-width:1px; border-bottom-style:solid; border-bottom-color:#000000; border-left-width:1px; border-left-style:solid; border-left-color:#000000; font-size: 10px; vertical-align: middle; text-align: center;">
                            <%=(osAberta ? "" : ViewData["MatriculaEstacao"])%>&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td class="style16" style="border-bottom-width:1px; border-bottom-style:solid; border-bottom-color:#000000; border-left-width:1px; border-left-style:solid; border-left-color:#000000;">
                            Assinatura:
                        </td>
                        <td class="style17" style="padding: 10px 0px 10px 0px; border-bottom-width:1px; border-bottom-style:solid; border-bottom-color:#000000; border-left-width:1px; border-left-style:solid; border-left-color:#000000; font-size: 10px; vertical-align: middle; text-align: center;">
                            &nbsp;
                        </td>
                        <td style="border-bottom-width:1px; border-bottom-style:solid; border-bottom-color:#000000; border-left-width:1px; border-left-style:solid; border-left-color:#000000;"></td>
                        <td class="style18" style="border-bottom-width:1px; border-bottom-style:solid; border-bottom-color:#000000; border-left-width:1px; border-left-style:solid; border-left-color:#000000;">
                            Assinatura:
                        </td>
                        <td class="style19" style="padding: 10px 0px 10px 0px; border-bottom-width:1px; border-bottom-style:solid; border-bottom-color:#000000; border-left-width:1px; border-left-style:solid; border-left-color:#000000; font-size: 10px; vertical-align: middle; text-align: center;">
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="style1">&nbsp;</td>
        </tr>

           
    
        <tr>
            <td class="style1">
                <table width="100%" cellpadding="0" cellspacing="0" style="height:86px; border-top-width:1px; border-top-style:solid; border-top-color:#000000; border-right-width:1px; border-right-style:solid; border-right-color:#000000;">
                    <tr>
                        <th style="font-size: 9pt;border-bottom-width:1px; border-bottom-style:solid; border-bottom-color:#000000; border-left-width:1px; border-left-style:solid; border-left-color:#000000;" class="style28">
                            Justificativa Cancelamento
                        </th>
                    </tr>
                    <tr>
                        <td style="padding: 10px 0px 10px 0px; border-bottom-width:1px; border-bottom-style:solid; border-bottom-color:#000000; border-left-width:1px; border-left-style:solid; border-left-color:#000000; font-size: 10px;">
                            <%= ViewData["Justificativa"] %><br />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        
        <tr>
            <td class="style1">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="style1">
                <table width="100%" cellpadding="0" cellspacing="0" style="height:27px; border-top-width:1px; border-top-style:solid; border-top-color:#000000; border-right-width:1px; border-right-style:solid; border-right-color:#000000;">
                    <tr>
                        <td class="style53" style="border-bottom-width:1px; border-bottom-style:solid; border-bottom-color:#000000; border-left-width:1px; border-left-style:solid; border-left-color:#000000; font-size: 10px;">
                            Última alteração:
                        </td>
                        <td class="style54" style="border-bottom-width:1px; border-bottom-style:solid; border-bottom-color:#000000; border-left-width:1px; border-left-style:solid; border-left-color:#000000; font-size: 10px; vertical-align: middle;
                            padding-top: 10px; padding-bottom: 10px;">
                            <%=ViewData["UltimaAlteracao"]%><br />
                        </td>
                        <td class="style55" style="border-bottom-width:1px; border-bottom-style:solid; border-bottom-color:#000000; border-left-width:1px; border-left-style:solid; border-left-color:#000000; font-size: 10px;">
                            Usuário:
                        </td>
                        <td style="padding: 10px 0px 10px 0px; border-bottom-width:1px; border-bottom-style:solid; border-bottom-color:#000000; border-left-width:1px; border-left-style:solid; border-left-color:#000000; font-size: 10px; vertical-align: middle;">
                            <%=ViewData["Usuario"]%><br />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>