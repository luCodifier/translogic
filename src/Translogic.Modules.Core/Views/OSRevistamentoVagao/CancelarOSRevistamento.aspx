﻿<%@ Page Title="" Language="C#" Inherits="System.Web.Mvc.ViewPage" MasterPageFile="~/Views/Shared/Interna.master" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server">

    <script type="text/javascript">
        //***************************** VARIÁVEIS E INICIALIZAÇÃO**************//
        var idOs = '<%=ViewData["idOs"] %>';
        var dataHora = '<%=ViewData["data"] %>';
        var local = '<%=ViewData["local"] %>';

        $(function () {

            //Setar campos recuperados da OS
            Ext.getCmp("txtNumOs").setValue(idOs);
            Ext.getCmp("txtDataHora").setValue(dataHora);
            Ext.getCmp("txtLocal").setValue(local);
        });

        //***************************** CAMPOS *****************************//
        //***************************** VARIÁVEIS E INICIALIZAÇÃO**************//
        var idOs = '<%=ViewData["idOs"] %>';
        var dataHora = '<%=ViewData["data"] %>';
        var local = '<%=ViewData["local"] %>';
        var idFornecedor = '<%=ViewData["idFornecedor"] %>';

        $(function () {

            //Setar campos recuperados da OS
            var txtNumOs = Ext.getCmp("txtNumOs");
            txtNumOs.value = idOs;

            var txtDataHora = Ext.getCmp("txtDataHora");
            txtDataHora.value = dataHora; 

            var txtLocal = Ext.getCmp("txtLocal"); 
            txtLocal.value = local;
        });

        //***************************** CAMPOS *****************************//
        var txtNumOs = {
            xtype: 'textfield',
            name: 'txtNumOs',
            id: 'txtNumOs',
            fieldLabel: 'Número OS',
            readonly: true,
            disabled: true,
            autoCreate: {
                tag: 'input',
                type: 'text',
                autocomplete: 'off',
                maxlength: '12',
                readonly: true
            },
            maskRe: /[0-9+;]/,
            width: 85
        };

        var txtDataHora = {
            xtype: 'textfield',
            name: 'txtDataHora',
            id: 'txtDataHora',
            fieldLabel: 'Data e hora',
            disabled: true,
            autoCreate: {
                tag: 'input',
                type: 'text',
                autocomplete: 'off',
                maxlength: '12',
                readonly: true
            },
            style: 'text-transform:uppercase;',
            width: 95
        };

        var txtHoraInicio = {
            xtype: 'textfield',
            name: 'txtHoraInicio',
            id: 'txtHoraInicio',
            fieldLabel: 'Hora início',
            disabled: true,
            autoCreate: {
                tag: 'input',
                type: 'text',
                autocomplete: 'off',
                maxlength: '5',
                readonly: true
            },
            width: 85
        };

        var txtHoraTermino = {
            xtype: 'textfield',
            name: 'txtHoraTermino',
            id: 'txtHoraTermino',
            fieldLabel: 'Hora término',
            disabled: true,
            autoCreate: {
                tag: 'input',
                type: 'text',
                autocomplete: 'off',
                maxlength: '5',
                readonly: true
            },
            width: 85
        };

        var txtHoraEntrega = {
            xtype: 'textfield',
            name: 'txtHoraEntrega',
            id: 'txtHoraEntrega',
            fieldLabel: 'Hora de entrega para fornecedor',
            disabled: true,
            autoCreate: {
                tag: 'input',
                type: 'text',
                autocomplete: 'off',
                maxlength: '5',
                readonly: true
            },
            width: 170
        };

        var txtLinha = {
            xtype: 'textfield',
            name: 'txtLinha',
            id: 'txtLinha',
            fieldLabel: 'Linha',
            disabled: true,
            autoCreate: {
                tag: 'input',
                type: 'text',
                autocomplete: 'off',
                maxlength: '255',
                readonly: true
            },
            width: 100
        };

        var txtConvocacao = {
            xtype: 'textfield',
            name: 'txtConvocacao',
            id: 'txtConvocacao',
            fieldLabel: 'Convocação',
            disabled: true,
            autoCreate: {
                tag: 'input',
                type: 'text',
                autocomplete: 'off',
                maxlength: '255',
                readonly: true
            },
            width: 130
        };

        var txtQuantVagoes = {
            xtype: 'textfield',
            name: 'txtQuantVagoes',
            id: 'txtQuantVagoes',
            fieldLabel: 'Quantidade de Vagões',
            disabled: true,
            autoCreate: {
                tag: 'input',
                type: 'text',
                autocomplete: 'off',
                maxlength: '5',
                readonly: true
            },
            maskRe: /[0-9+;]/,
            width: 100
        };

        var fornecedorStore = new Ext.data.JsonStore({
            root: "Items",
            autoLoad: true,
            proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("ObterTodosFornecedores", "OSRevistamentoVagao") %>', timeout: 600000 }),
            id: 'fornecedorStore',
            fields: ['IdFornecedorOs', 'Nome'],
            baseParams: { local: local }
        });

        var ddlFornecedor = new Ext.form.ComboBox({
            editable: false,
            typeAhead: true,
            forceSelection: true,
            disableKeyFilter: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            store: fornecedorStore,
            valueField: 'IdFornecedorOs',
            displayField: 'Nome',
            fieldLabel: 'Fornecedor',
            id: 'ddlFornecedor',
            width: 260,
            alowBlank: false,
            disabled: true
        });

        var txtLocal = {
            xtype: 'textfield',
            name: 'txtLocal',
            id: 'txtLocal',
            fieldLabel: 'Local',
            disabled: true,
            autoCreate: {
                tag: 'input',
                type: 'text',
                autocomplete: 'off',
                maxlength: '3',
                readonly: true
            },
            style: 'text-transform:uppercase;',
            width: 32
        };

        var txtQuantVagoesVedados = {
            xtype: 'textfield',
            name: 'txtQuantVagoesVedados',
            id: 'txtQuantVagoesVedados',
            fieldLabel: 'Quantidade de vagões vedados',
            disabled: true,
            autoCreate: {
                tag: 'input',
                type: 'text',
                autocomplete: 'off',
                maxlength: '5',
                readonly: true
            },
            maskRe: /[0-9+;]/,
            width: 130
        };

        var txtQuantVagoesGambitados = {
            xtype: 'textfield',
            name: 'txtQuantVagoesGambitados',
            id: 'txtQuantVagoesGambitados',
            fieldLabel: 'Quantidade de vagões gambitados',
            disabled: true,
            autoCreate: {
                tag: 'input',
                type: 'text',
                autocomplete: 'off',
                maxlength: '5',
                readonly: true
            },
            maskRe: /[0-9+;]/,
            width: 130
        };

        var ckCarregados = {
            xtype: 'checkboxgroup',
            fieldLabel: 'Há vagões carregados entre os vazios?',
            id: 'ckCarregados',
            width: 200,
            name: 'ckCarregados',
            disabled: true,
            style: 'margin-top:5px;',
            items: [
		            { boxLabel: 'Sim', name: 'ckCarregadosSim', inputValue: 1 }
		           ]
        };

        var txtQuantVazios = {
            xtype: 'textfield',
            name: 'txtQuantVazios',
            id: 'txtQuantVazios',
            fieldLabel: 'Quantidade',
            disabled: true,
            autoCreate: {
                tag: 'input',
                type: 'text',
                autocomplete: 'off',
                maxlength: '5',
                readonly: true
            },
            maskRe: /[0-9+;]/,
            width: 85
        };

        var txtNomeManutencao = {
            xtype: 'textfield',
            name: 'txtNomeManutencao',
            id: 'txtNomeManutencao',
            fieldLabel: 'Posto de manutenção. Nome',
            disabled: true,
            autoCreate: {
                tag: 'input',
                type: 'text',
                autocomplete: 'off',
                maxlength: '255',
                readonly: true
            },
            width: 200
        };

        var txtMatriculaManutencao = {
            xtype: 'textfield',
            name: 'txtMatriculaManutencao',
            id: 'txtMatriculaManutencao',
            fieldLabel: 'Posto de manutenção. Matrícula',
            disabled: true,
            autoCreate: {
                tag: 'input',
                type: 'text',
                autocomplete: 'off',
                maxlength: '50',
                readonly: true
            },
            width: 200
        };

        var txtNomeEstacao = {
            xtype: 'textfield',
            name: 'txtNomeEstacao',
            id: 'txtNomeEstacao',
            fieldLabel: 'Fornecedor. Nome',
            disabled: true,
            autoCreate: {
                tag: 'input',
                type: 'text',
                autocomplete: 'off',
                maxlength: '255',
                readonly: true
            },
            width: 200
        };

        var txtMatriculaEstacao = {
            xtype: 'textfield',
            name: 'txtMatriculaEstacao',
            id: 'txtMatriculaEstacao',
            fieldLabel: 'Fornecedor. Matrícula',
            disabled: true,
            autoCreate: {
                tag: 'input',
                type: 'text',
                autocomplete: 'off',
                maxlength: '50',
                readonly: true
            },
            width: 200
        };

        var txtJustificativa = new Ext.form.TextArea({
            id: 'txtJustificativa',
            name: 'txtJustificativa',
            fieldLabel: 'Justificativa',
            enableKeyEvents: true,
            autoCreate: {
                maxlength: '250',
                tag: 'textarea',
                rows: '3',
                cols: '140'
            },
            listeners: {
                keyUp: function (field, e) {
                    if (field.getRawValue().length >= 250) {
                        $("#txtJustificativa").val($("#txtJustificativa").val().substring(0, 250));
                    }

                }
            }
        });

        //***************************** GRID VAGÕES**********************//

    var gridVagoes = new Translogic.PaginatedGrid({
        id: 'gridVagoes',
        name: 'gridVagoes',
        autoLoadGrid: true,
        disabled: false,
        viewConfig: {
            forceFit: true,
            emptyText: 'Não possui dado(s) para exibição.'
        },
        height: 170,
        autoScroll: true,
        fields: [
                    'IdItem',
                    'Serie',
                    'NumeroVagao',
                    'ConcluidoDescricao',
                    'RetirarDescricao',
                    'ComproblemaDescricao',
                    'Observacao'
                ],
        url: '<%= Url.Action("ObterVagoesDaOs", "OSRevistamentoVagao") %>',
        columns: [
				    new Ext.grid.RowNumberer(),
                    { header: 'Série', dataIndex: "Serie", sortable: true, width: 80 },
                    { header: 'Nº Vagão', dataIndex: "NumeroVagao", sortable: true, width: 80 },
                    { header: 'Serviço concluído', dataIndex: "ConcluidoDescricao", sortable: true, width: 80 },
                    { header: 'Retirar vagão', dataIndex: "RetirarDescricao", sortable: true, width: 80 },
                    { header: 'Vedação ou Gambitagem com problema', dataIndex: "ComproblemaDescricao", sortable: true, width: 160 },
                    { header: 'Observação', dataIndex: "Observacao", sortable: false, width: 300 }
                ]
    });

    gridVagoes.getStore().proxy.on('beforeload', function (p, params) {
        params['idOs'] = idOs;
    });

    //***************************** LAYOUT *****************************//

    var formtxtNumOS = {
        layout: 'form',
        border: false,
        bodyStyle: 'padding: 5px',
        width: 95,
        items: [txtNumOs]
    };

    var formtxtDataHora = {
        layout: 'form',
        border: false,
        bodyStyle: 'padding: 5px',
        width: 105,
        items: [txtDataHora]
    };

    var formtxtHoraInicio = {
        layout: 'form',
        border: false,
        bodyStyle: 'padding: 5px',
        width: 95,
        items: [txtHoraInicio]
    };

    var formtxtHoraTermino = {
        layout: 'form',
        border: false,
        bodyStyle: 'padding: 5px',
        width: 95,
        items: [txtHoraTermino]
    };

    var formtxtHoraEntrega = {
        layout: 'form',
        border: false,
        bodyStyle: 'padding: 5px',
        width: 185,
        items: [txtHoraEntrega]
    };

    var formtxtLinha = {
        layout: 'form',
        border: false,
        bodyStyle: 'padding: 5px',
        width: 110,
        items: [txtLinha]
    };

    var formtxtConvocacao = {
        layout: 'form',
        border: false,
        bodyStyle: 'padding: 5px',
        width: 140,
        items: [txtConvocacao]
    };

    var formtxtQuantVagoes = {
        layout: 'form',
        border: false,
        bodyStyle: 'padding: 5px',
        width: 200,
        items: [txtQuantVagoes]
    };

    var formddlFornecedor = {
        layout: 'form',
        border: false,
        bodyStyle: 'padding: 5px',
        width: 270,
        items: [ddlFornecedor]
    };

    var formtxtLocal = {
        layout: 'form',
        border: false,
        bodyStyle: 'padding: 5px',
        width: 42,
        items: [txtLocal]
    };

    var formtxtQuantVagoesVedados = {
        layout: 'form',
        border: false,
        bodyStyle: 'padding: 5px',
        width: 200,
        items: [txtQuantVagoesVedados]
    };

    var formtxtQuantVagoesGambitados = {
        layout: 'form',
        border: false,
        bodyStyle: 'padding: 5px',
        width: 200,
        items: [txtQuantVagoesGambitados]
    };

    var formckCarregados = {
        layout: 'form',
        border: false,
        bodyStyle: 'padding: 5px',
        width: 220,
        items: [ckCarregados]
    };

    var formtxtQuantVazios = {
        layout: 'form',
        border: false,
        bodyStyle: 'padding: 5px',
        width: 95,
        items: [txtQuantVazios]
    };

    var formtxtNomeManutencao = {
        layout: 'form',
        border: false,
        bodyStyle: 'padding: 5px',
        width: 210,
        items: [txtNomeManutencao]
    };

    var formtxtMatriculaManutencao = {
        layout: 'form',
        border: false,
        bodyStyle: 'padding: 5px',
        width: 210,
        items: [txtMatriculaManutencao]
    };

    var formtxtNomeEstacao = {
        layout: 'form',
        border: false,
        bodyStyle: 'padding: 5px',
        width: 210,
        items: [txtNomeEstacao]
    };

    var formtxtMatriculaEstacao = {
        layout: 'form',
        border: false,
        bodyStyle: 'padding: 5px',
        width: 210,
        items: [txtMatriculaEstacao]
    };

    var formtxtJustificativa = {
        layout: 'form',
        border: false,
        bodyStyle: 'padding: 5px',
        width: 900,
        items: [txtJustificativa]
    };

    //Containers
    var containerLinha1 = {
        layout: 'column',
        border: true,
        items: [formtxtNumOS, formtxtDataHora, formtxtHoraInicio, formtxtHoraTermino, formtxtHoraEntrega, formtxtLinha, formtxtConvocacao, formtxtQuantVagoes]
    };

    var containerLinha2 = {
        layout: 'column',
        border: true,
        items: [gridVagoes]
    };

    var containerLinha3 = {
        layout: 'column',
        border: true,
        items: [formddlFornecedor, formtxtLocal, formtxtQuantVagoesVedados, formtxtQuantVagoesGambitados, formckCarregados, formtxtQuantVazios]
    };

    var containerLinha4 = {
        layout: 'column',
        border: true,
        items: [formtxtNomeManutencao, formtxtMatriculaManutencao, formtxtNomeEstacao, formtxtMatriculaEstacao]
    };

    var containerLinha5 = {
        layout: 'column',
        border: true,
        items: [formtxtJustificativa]
    };


        //***************************** FUNÇÕES*****************************//

        //Classes
        function OSRevistamentoVagaoItemDto() {
            var IdItem;
        }
        function Vagao() {
            var OSRevistamentoVagaoItemDto;
        }

       function ValidarEFecharOS() {
           // Progress BAR
           Ext.getBody().mask("Processando dados...", "x-mask-loading");
            var idOs = Ext.getCmp("txtNumOs").getValue();
            var justificativa = Ext.getCmp("txtJustificativa").getValue();

//            Ext.getBody().mask("Processando dados...", "x-mask-loading");

            var jsonRequest = $.toJSON({ idOs: idOs, justificativa: justificativa });
            //console.log(jsonRequest);

            $.ajax({
                url: '<%= Url.Action("CancelarOS", "OSRevistamentoVagao") %>',
                type: "POST",
                dataType: 'json',
                data: jsonRequest,
                timeout: 300000,
                contentType: "application/json; charset=utf-8",
                failure: function (conn, data) {
                    console.log("aqui");
                    Ext.getBody().unmask();
                    Ext.Msg.alert("Mensagem de Erro", "Ocorreu um erro inesperado.");

                },
                success: function (result) {
                    Ext.getBody().unmask();
                    if (result.Success) {
                        Ext.Msg.show({
                            title: 'Aviso',
                            msg: 'OS de Revistamento Cancelada com sucesso.',
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.WARNING,
                            fn: function (buttonValue, inputText, showConfig) {
                                var url = '<%= Url.Action("Index", "OSRevistamentoVagao") %>';
                                window.location = url;
                            }
                        });
                    }
                    else {
                        Ext.Msg.show({
                            title: "Alerta",
                            msg: result.Message,
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.ERROR
                        });
                    }
                }
            });
        }

         function Validar() {
            var justificativa = Ext.getCmp("txtJustificativa").getValue();
          
            if (justificativa == "") {
                Ext.getBody().unmask();
                Ext.Msg.show({ title: 'Aviso', msg: 'O campo justificativa é obrigatório', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                return false;
            }
            return true;
        }

        function Voltar() {

            Ext.getBody().mask("Processando dados...", "x-mask-loading");
            var url = '<%= Url.Action("Index", "OSRevistamentoVagao")%>';

            window.location = url;
        }

        //Evendo das checkboxes para marcar todos no grid
        function CheckGridColumns(arrChecked) {

            var marcarConcluidos = false;
            if (arrChecked.length >= 1) {

                if (arrChecked[0].boxLabel == "Como concluído") {
                    marcarConcluidos = true;
                }
            }

            if (marcarConcluidos) {
                SetAllCheckConcluido();
            } else { NotSetAllCheckConcluido(); }

        }

        function SetAllCheckConcluido() {

            $(".x-grid3-cc-checkConcluido").removeClass("x-grid3-check-col");
            $(".x-grid3-cc-checkConcluido").addClass("x-grid3-check-col-on");
            var itens = gridVagoes.getStore().getRange();
            for (var i = 0; i < itens.length; i++) {
                itens[i].data.Concluido = true;
            }
        }

        function NotSetAllCheckConcluido() {

            $(".x-grid3-cc-checkConcluido").removeClass("x-grid3-check-col-on");
            $(".x-grid3-cc-checkConcluido").addClass("x-grid3-check-col");
            var itens = gridVagoes.getStore().getRange();
            for (var i = 0; i < itens.length; i++) {
                itens[i].data.Concluido = false;
            }
        }

        //***************************** BOTOES *****************************//
        var btnCancelarOS = {
            name: 'btnCancelarOS',
            id: 'btnCancelarOS',
            text: 'Confirmar',
            iconCls: 'icon-tick',
            handler: ValidarEFecharOS
        };

        var btnVoltar = {
            name: 'btnVoltar',
            id: 'btnVoltar',
            text: 'Voltar',
            iconCls: 'icon-left',
            handler: Voltar
        };

        //***************************** PAINEL *****************************//
        var panelForm = new Ext.form.FormPanel({
            id: 'panelForm',
            layout: 'form',
            labelAlign: 'top',
            border: false,
            autoHeight: true,
            title: "Gerar Ordem de Serviço",
            region: 'center',
            bodyStyle: 'padding: 5px',
            items: [containerLinha1, containerLinha2, containerLinha3, containerLinha4, containerLinha5],
            buttonAlign: "center",
            buttons: [btnCancelarOS, btnVoltar]
        });

        //***************************** RENDER *****************************//
        Ext.onReady(function () {
            panelForm.render("divContent");

            //gridVagoesStore.load({ params: { start: 0, limit: 50} });
        });

        fornecedorStore.on('load', function (store) {

            var range = ddlFornecedor.getStore().getRange();
            var existe = false;
            $.each(range, function (c, col) {

                //Verifica se registro que usuario está tentando inserir já existe
                if (col.data.IdFornecedorOs == idFornecedor) {
                    existe = true;
                }
            });

            if (existe)
                Ext.getCmp('ddlFornecedor').setValue(idFornecedor);
        });
        
    
    </script>
</asp:Content>
<asp:Content ID="MainContent" ContentPlaceHolderID="Content" runat="server">
    <div id="header-content">
        <h1>
            Manutenção</h1>
        <small>Você está em Manutenção > Gerar Ordem de Serviço</small>
        <br />
    </div>
    <div id="divContent">
    </div>
</asp:Content>