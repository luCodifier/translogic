﻿<%@ Page Title="" Language="C#" Inherits="System.Web.Mvc.ViewPage" MasterPageFile="~/Views/Shared/Interna.master" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server">

    <script type="text/javascript">
        //***************************** VARIÁVEIS E INICIALIZAÇÃO**************//
        var idOs = '<%=ViewData["idOs"] %>';
        var dataHora = '<%=ViewData["data"] %>';
        var local = '<%=ViewData["local"] %>';
        var idFornecedor = '<%=ViewData["idFornecedor"] %>';

        $(function () {

           //Setar campos recuperados da OS
            Ext.getCmp("txtNumOs").setValue(idOs);
            Ext.getCmp("txtDataHora").setValue(dataHora);
            Ext.getCmp("txtLocal").setValue(local);
        });

        //***************************** CAMPOS *****************************//
        var txtNumOs = {
            xtype: 'textfield',
            name: 'txtNumOs',
            id: 'txtNumOs',
            fieldLabel: 'Número OS',
            readonly: true,
            disabled: true,
            autoCreate: {
                tag: 'input',
                type: 'text',
                autocomplete: 'off',
                maxlength: '12',
                readonly: true
            },
            maskRe: /[0-9+;]/,
            width: 85
        };

        var txtDataHora = {
            xtype: 'textfield',
            name: 'txtDataHora',
            id: 'txtDataHora',
            fieldLabel: 'Data e hora',
            disabled: true,
            autoCreate: {
                tag: 'input',
                type: 'text',
                autocomplete: 'off',
                maxlength: '12',
                readonly: true
            },
            style: 'text-transform:uppercase;',
            width: 95
        };

        
      var txtHoraInicio =  new Ext.form.TimeField({
            name: 'txtHoraInicio',
            id: 'txtHoraInicio',
            format: 'H:i',
            increment: 1,
            fieldLabel: 'Hora início',
            maxlength: 5,
            plugins: [new Ext.ux.InputTextMask('99:99', true)],
            width: 85
        });

      var txtHoraTermino =  new Ext.form.TimeField({
            name: 'txtHoraTermino',
            id: 'txtHoraTermino',
            format: 'H:i',
            increment: 1,
            fieldLabel: 'Hora término',
            maxlength: 5,
            plugins: [new Ext.ux.InputTextMask('99:99', true)],
            width: 85
        });

       var txtHoraEntrega =  new Ext.form.TimeField({
            name: 'txtHoraEntrega',
            id: 'txtHoraEntrega',
            format: 'H:i',
            increment: 1,
            fieldLabel: 'Hora de entrega para fornecedor',
            maxlength: 5,
            plugins: [new Ext.ux.InputTextMask('99:99', true)],
            width: 170
        });

        var txtLinha = {
            xtype: 'textfield',
            name: 'txtLinha',
            id: 'txtLinha',
            fieldLabel: 'Linha',
            autoCreate: {
                tag: 'input',
                type: 'text',
                autocomplete: 'off',
                maxlength: '255'
            },
            width: 100
        };

        var txtConvocacao = {
            xtype: 'textfield',
            name: 'txtConvocacao',
            id: 'txtConvocacao',
            fieldLabel: 'Convocação',
            autoCreate: {
                tag: 'input',
                type: 'text',
                autocomplete: 'off',
                maxlength: '255'
            },
            width: 130
        };

        var txtQuantVagoes = {
            xtype: 'numberfield',
            name: 'txtQuantVagoes',
            id: 'txtQuantVagoes',
            fieldLabel: 'Quantidade de Vagões',
            allowDecimals: false,
            allowNegative: false,
            autoCreate: {
                tag: 'input',
                type: 'text',
                autocomplete: 'off',
                maxlength: '5'
            },            
            width: 100
        };

        var fornecedorStore = new Ext.data.JsonStore({
            root: "Items",
            autoLoad: true,
            proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("ObterFornecedores", "OSRevistamentoVagao") %>', timeout: 600000 }),
            id: 'fornecedorStore',
            fields: ['IdFornecedorOs', 'Nome'],
            baseParams: { local: local }
        });

        var ddlFornecedor = new Ext.form.ComboBox({
            editable: false,
            typeAhead: true,
            forceSelection: true,
            disableKeyFilter: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            store: fornecedorStore,
            valueField: 'IdFornecedorOs',
            displayField: 'Nome',
            fieldLabel: 'Fornecedor',
            id: 'ddlFornecedor',
            width: 260,
            alowBlank: false
        });

        var txtLocal = {
            xtype: 'textfield',
            name: 'txtLocal',
            id: 'txtLocal',
            fieldLabel: 'Local',
            disabled: true,
            autoCreate: {
                tag: 'input',
                type: 'text',
                autocomplete: 'off',
                maxlength: '3',
                readonly: true
            },
            style: 'text-transform:uppercase;',
            width: 32
        };

        var txtQuantVagoesVedados = {
            xtype: 'numberfield',
            name: 'txtQuantVagoesVedados',
            id: 'txtQuantVagoesVedados',
            fieldLabel: 'Quantidade de vagões vedados',
            allowDecimals: false,
            allowNegative: false,
            autoCreate: {
                tag: 'input',
                type: 'text',
                autocomplete: 'off',
                maxlength: '5'
            },
            width: 130
        };

        var txtQuantVagoesGambitados = {
            xtype: 'numberfield',
            name: 'txtQuantVagoesGambitados',
            id: 'txtQuantVagoesGambitados',
            fieldLabel: 'Quantidade de vagões gambitados',
            allowDecimals: false,
            allowNegative: false,
            autoCreate: {
                tag: 'input',
                type: 'text',
                autocomplete: 'off',
                maxlength: '5'
            },
            width: 130
        };

        var ckCarregados = {
            xtype: 'checkboxgroup',
            fieldLabel: 'Há vagões carregados entre os vazios?',
            id: 'ckCarregados',
            width: 200,
            name: 'ckCarregados',
            style: 'margin-top:5px;',
            items: [
		            { boxLabel: 'Sim', name: 'ckCarregadosSim', id: 'ckCarregadosSim', inputValue: 1 }
		           ],
            listeners: {
                change: {
                    fn: function (campo, checkeds) {
                        
                            
                        //Habilita quantidade de vazios para preenchimento se estiver marcado 'Sim'
                        if (checkeds.length == 1) {
                        
                            if (checkeds[0].checked) {
                                Ext.getCmp("txtQuantVazios").setDisabled(false);
                            }
                            else {
                                Ext.getCmp("txtQuantVazios").setDisabled(true);
                                Ext.getCmp("txtQuantVazios").setValue("");
                            }                            
                        }
                        else {
                            Ext.getCmp("txtQuantVazios").setDisabled(true);
                             Ext.getCmp("txtQuantVazios").setValue("");
                        }
                    }
                }
            }
        };

        var txtQuantVazios = {
            xtype: 'numberfield',
            name: 'txtQuantVazios',
            id: 'txtQuantVazios',
            fieldLabel: 'Quantidade',
            allowDecimals: false,
            allowNegative: false,
            disabled: true,
            autoCreate: {
                tag: 'input',
                type: 'text',
                autocomplete: 'off',
                maxlength: '5'
            },
            width: 85
        };

        var txtNomeManutencao = {
            xtype: 'textfield',
            name: 'txtNomeManutencao',
            id: 'txtNomeManutencao',
            fieldLabel: 'Posto de manutenção. Nome',
            autoCreate: {
                tag: 'input',
                type: 'text',
                autocomplete: 'off',
                maxlength: '255'
            },
            width: 200
        };

        var txtMatriculaManutencao = {
            xtype: 'textfield',
            name: 'txtMatriculaManutencao',
            id: 'txtMatriculaManutencao',
            fieldLabel: 'Posto de manutenção. Matrícula',
            autoCreate: {
                tag: 'input',
                type: 'text',
                autocomplete: 'off',
                maxlength: '50'
            },
            width: 200
        };

        var txtNomeEstacao = {
            xtype: 'textfield',
            name: 'txtNomeEstacao',
            id: 'txtNomeEstacao',
            fieldLabel: 'Fornecedor. Nome',
            autoCreate: {
                tag: 'input',
                type: 'text',
                autocomplete: 'off',
                maxlength: '255'
            },
            width: 200
        };

        var txtMatriculaEstacao = {
            xtype: 'textfield',
            name: 'txtMatriculaEstacao',
            id: 'txtMatriculaEstacao',
            fieldLabel: 'Fornecedor. Matrícula',
            autoCreate: {
                tag: 'input',
                type: 'text',
                autocomplete: 'off',
                maxlength: '50'
            },
            width: 200
        };

        var ckMarcarTodos = {
            xtype: 'checkboxgroup',
            fieldLabel: 'Marcar todos',
            id: 'ckMarcarTodos',
            width: 260,
            name: 'ckMarcarTodos',
            style: 'margin-top:5px;',
            items: [
		            { boxLabel: 'Como concluído', name: 'ckAllConcluido', inputValue: 1 }
		           ],
            listeners: {
                change: {
                    fn: function (campo, checkeds) {

                        CheckGridColumns(checkeds);
                    }
                }
            }
        };

        //***************************** GRID VAGÕES**********************//

          var checkConcluido = new Ext.grid.CheckColumn({
            dataIndex: 'Concluido',
            header: "Concluído",
            id: 'checkConcluido',
            sortable: false,
            width: 80
        });

        var checkRetirar = new Ext.grid.CheckColumn({
            dataIndex: 'Retirar',
            header: 'Retirar vagão',
            id: 'checkRetirar',
            width: 80
        });

        var checkComProblema = new Ext.grid.CheckColumn({
            dataIndex: 'Comproblema',
            header: 'Vedação ou Gambitagem com problema',
            id: 'checkComProblema',
            width: 240
        });

         var gridVagoesStore = new Ext.data.JsonStore({
            root: "Items",
            totalProperty: 'Total',
            id: 'gridVagoesStore',
            name: 'gridVagoesStore',
            proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("ObterVagoesDaOs", "OSRevistamentoVagao") %>', timeout: 600000 }),
            paramNames: {
                sort: "detalhesPaginacaoWeb.Sort",
                dir: "detalhesPaginacaoWeb.Dir",
                start: "detalhesPaginacaoWeb.Start",
                limit: "detalhesPaginacaoWeb.Limit"
            },
             fields: [
                    'IdItem',
                    'Serie',
                    'NumeroVagao',
                    'Concluido',
                    'Retrabalho',
                    'Observacao'
                ]
         });

         // Toolbar da Grid
         var pagingToolbar = new Ext.PagingToolbar({
             pageSize: 50,
             store: gridVagoesStore,
             displayInfo: true,
             displayMsg: App.Resources.Web.GridExibindoRegistros,
             emptyMsg: App.Resources.Web.GridSemRegistros,
             paramNames: {
                 start: "detalhesPaginacaoWeb.Start",
                 limit: "detalhesPaginacaoWeb.Limit"
             }
         });

         var rowEditing = {
             xtype: 'textfield',
             name: 'txtObservacao',
             id: 'txtObservacao',
             autoCreate: {
                 tag: 'input',
                 type: 'text',
                 autocomplete: 'off',
                 maxlength: '255'
             },
             width: 400
         };

         var gridColumns = new Ext.grid.ColumnModel({
             defaults: {
                 sortable: true
             },
             columns: [
                    new Ext.grid.RowNumberer(),
                    { header: 'Série', dataIndex: "Serie", sortable: false, width: 80 },
                    { header: 'Nº Vagão', dataIndex: "NumeroVagao", sortable: false, width: 80 },
                    checkConcluido,
                    checkRetirar,
                    checkComProblema,
                    {header: 'Observação', dataIndex: "Observacao", sortable: false, width: 400, editable: true, editor: rowEditing }
                ]
         });
                  
        var gridVagoes = new Ext.grid.EditorGridPanel({
            id: 'gridVagoes',
            name: 'gridVagoes',
            autoLoadGrid: false,
            height: 200,
            stripeRows: true,
            cm: gridColumns,
            autoExpandColumn : true,
            region: 'center',
            autoScroll: true,
            loadMask: { msg: App.Resources.Web.Carregando },
            store: gridVagoesStore,
            bbar: pagingToolbar,
            plugins: [checkConcluido, checkRetirar, checkComProblema] // include plugin
        });

        gridVagoes.getStore().proxy.on('beforeload', function (p, params) {
            params['idOs'] = idOs;
        });

        
        //***************************** LAYOUT *****************************//

        var formtxtNumOS = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            width: 95,
            items: [txtNumOs]
        };

        var formtxtDataHora = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            width: 105,
            items: [txtDataHora]
        };

        var formtxtHoraInicio = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            width: 95,
            items: [txtHoraInicio]
        };

        var formtxtHoraTermino = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            width: 95,
            items: [txtHoraTermino]
        };

        var formtxtHoraEntrega = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            width: 185,
            items: [txtHoraEntrega]
        };

        var formtxtLinha = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            width: 110,
            items: [txtLinha]
        };

        var formtxtConvocacao = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            width: 140,
            items: [txtConvocacao]
        };

        var formtxtQuantVagoes = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            width: 200,
            items: [txtQuantVagoes]
        };

        var formddlFornecedor = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            width: 270,
            items: [ddlFornecedor]
        };

        var formtxtLocal = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            width: 42,
            items: [txtLocal]
        };

        var formtxtQuantVagoesVedados = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            width: 200,
            items: [txtQuantVagoesVedados]
        };

        var formtxtQuantVagoesGambitados = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            width: 200,
            items: [txtQuantVagoesGambitados]
        };

        var formckCarregados = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            width: 220,
            items: [ckCarregados]
        };

        var formtxtQuantVazios = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            width: 95,
            items: [txtQuantVazios]
        };

        var formtxtNomeManutencao = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            width: 210,
            items: [txtNomeManutencao]
        };

        var formtxtMatriculaManutencao = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            width: 210,
            items: [txtMatriculaManutencao]
        };

        var formtxtNomeEstacao = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            width: 210,
            items: [txtNomeEstacao]
        };

        var formtxtMatriculaEstacao = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            width: 210,
            items: [txtMatriculaEstacao]
        };

        var formckMarcarTodos = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding-left: 5px',
            height: 40,
            width: 210,
            items: [ckMarcarTodos]
        };


        //Containers
        var containerLinha1 = {
            layout: 'column',
            border: true,
            items: [formtxtNumOS, formtxtDataHora, formtxtHoraInicio, formtxtHoraTermino, formtxtHoraEntrega, formtxtLinha, formtxtConvocacao, formtxtQuantVagoes]
        };

        var containerLinha2 = {
            layout: 'column',
            border: true,
            items: [formckMarcarTodos]
        };

         var containerLinha3 = {
            layout: 'column',
            border: true,
            items: [gridVagoes]
        };

        var containerLinha4 = {
            layout: 'column',
            border: true,
            items: [formddlFornecedor, formtxtLocal, formtxtQuantVagoesVedados, formtxtQuantVagoesGambitados, formckCarregados, formtxtQuantVazios]
        };

        var containerLinha5 = {
            layout: 'column',
            border: true,
            items: [formtxtNomeManutencao, formtxtMatriculaManutencao, formtxtNomeEstacao, formtxtMatriculaEstacao]
        };


        //***************************** FUNÇÕES*****************************//

        //Classes
        function OSRevistamentoVagaoItemDto() {
            var IdItem;
        }
        function Vagao() {
            var OSRevistamentoVagaoItemDto;
        }

         //Realiza validação da OS e se passar chama o salvar
        function ValidarEFecharOS() {

            //Recupera vagões do GRID
            var itens = gridVagoes.getStore().getRange();
            var temAlgumConcluido = false;
            var temAlgumNaoConcluido = false;
            var concluido = false;
            for (var i = 0; i < itens.length; i++) {
                concluido = itens[i].data.Concluido;
                if (concluido == false)
                    temAlgumNaoConcluido = true;
                if (concluido)
                    temAlgumConcluido = true;
            }

            if (temAlgumConcluido == false) {

                Ext.Msg.show({
                    title: 'Serviço concluído',
                    msg: 'Para confirmar o fechamento é necessário marcar pelo menos um vagão como concluído',
                    buttons: Ext.Msg.OK,
                    animEl: 'elId',
                    icon: Ext.MessageBox.WARNING
                });

            }
            else if (temAlgumNaoConcluido) {
                // Show a dialog using config options:
                Ext.Msg.show({
                    title: 'Fechamento parcial',
                    msg: 'Deseja fechar a OS parcialmente?',
                    buttons: Ext.Msg.OKCANCEL,
                    fn: SalvarOs,
                    animEl: 'elId',
                    icon: Ext.MessageBox.QUESTION
                });
            }
            else {
                SalvarOs('valido');
            }
        }

        function SalvarOs(btn) {

          //se btn escolhido for o OK irá realizar a validação e fechamento
            if (btn == 'ok' || btn == 'valido') {
                
                var quantListaVagoes = gridVagoesStore.getCount();

                //***************************** VALIDAÇÕES******************//
                var horaInicio = Ext.getCmp("txtHoraInicio").getValue();
                if (horaInicio == ""){
                    Ext.Msg.show({ title: 'Aviso', msg: 'O campo hora início é obrigatório!', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                    return;
                }

                var horaTermino = Ext.getCmp("txtHoraTermino").getValue();
                if (horaTermino == ""){
                    Ext.Msg.show({ title: 'Aviso', msg: 'O campo hora término é obrigatório!', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                    return;
                }

                var horaEntrega = Ext.getCmp("txtHoraEntrega").getValue();
                if (horaEntrega == ""){
                    Ext.Msg.show({ title: 'Aviso', msg: 'O campo hora de entrega para fornecedor é obrigatório!', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                    return;
                }

                var linha = Ext.getCmp("txtLinha").getValue();
                if (linha == ""){
                    Ext.Msg.show({ title: 'Aviso', msg: 'O campo linha é obrigatório!', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                    return;
                }
                
                var quantVagoes = Ext.getCmp("txtQuantVagoes").getValue();
                if (quantVagoes == "" || quantVagoes == "0"){
                    Ext.Msg.show({ title: 'Aviso', msg: 'O campo quantidade de vagões é obrigatório!', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                    return;
                }

                if (parseInt(quantVagoes) > parseInt(quantListaVagoes)){
                    Ext.Msg.show({ title: 'Aviso', msg: 'O campo quantidade de vagões não pode ser maior que a quantidade de vagões listados!', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                    return;
                }

                var ddlFornecedor = Ext.getCmp("ddlFornecedor").getValue();
                if (ddlFornecedor == "Selecione" || ddlFornecedor == "0" || ddlFornecedor == "") {
                    Ext.Msg.show({ title: 'Aviso', msg: 'O campo fornecedor é obrigatório!', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                    return;
                }

                var localServico = Ext.getCmp("txtLocal").getValue();
                if (localServico == "") {
                    Ext.Msg.show({ title: 'Aviso', msg: 'O campo local é obrigatório!', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                    return;
                }

                var quantVagoesVedados = Ext.getCmp("txtQuantVagoesVedados").getValue();
                if (quantVagoesVedados === ""){
                    Ext.Msg.show({ title: 'Aviso', msg: 'O campo quantidade de vagões vedados é obrigatório!', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                    return;
                }

                if (parseInt(quantVagoesVedados) > parseInt(quantListaVagoes)){
                    Ext.Msg.show({ title: 'Aviso', msg: 'O campo quantidade de vagões vedados não pode ser maior que a quantidade de vagões listados!', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                    return;
                }
                
                var quantVagoesGambitados = Ext.getCmp("txtQuantVagoesGambitados").getValue();
                if (quantVagoesGambitados === ""){
                    Ext.Msg.show({ title: 'Aviso', msg: 'O campo quantidade de vagões gambitados é obrigatório!', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                    return;
                }

                if (parseInt(quantVagoesGambitados) > parseInt(quantListaVagoes)){
                    Ext.Msg.show({ title: 'Aviso', msg: 'O campo quantidade de vagões gambitados não pode ser maior que a quantidade de vagões listados', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                    return;
                }  

                var quantVazios = Ext.getCmp("txtQuantVazios").getValue();
                if ($("#ckCarregadosSim").is(":checked") && (quantVazios === "" || quantVazios == "0")){
                    Ext.Msg.show({ title: 'Aviso', msg: 'O campo quantidade de vagões carregados entre os vazios é obrigatório e maior que zero', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                    return;
                }
                else if ($("#ckCarregadosSim").is(":checked")) {

                    if (parseInt(quantVazios) > parseInt(quantListaVagoes)){
                        Ext.Msg.show({ title: 'Aviso', msg: 'O campo quantidade de vagões carregados entre os vazios não pode ser maior que a quantidade de vagões listados!', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                        return;
                    }
                }
                
                var horaInicio = Ext.getCmp("txtHoraInicio").getValue();
                var horaTermino = Ext.getCmp("txtHoraTermino").getValue();
                var horaEntrega = Ext.getCmp("txtHoraEntrega").getValue();

                //Verifica Hora Inicio Maior que Hora TERMINO
                if (horaInicio > horaTermino) {
                    Ext.Msg.show({ title: 'Aviso', msg: 'O campo Hora Inicio, não pode ser superior a Hora Término!', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                    return;
                }
                //Verifica Hora Inicio Maior que Hora ENTREGA
                if (horaInicio > horaEntrega) {
                    Ext.Msg.show({ title: 'Aviso', msg: 'O campo Hora Inicio, não pode ser superior a Hora de Entrega para fornecedor!', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                    return;
                }

                if (quantVazios == "")
                    quantVazios = "0";

                var nomeManutencao = Ext.getCmp("txtNomeManutencao").getValue();
                if (nomeManutencao == ""){
                    Ext.Msg.show({ title: 'Aviso', msg: 'O campo Posto de manutenção. Nome é obrigatório!', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                    return;
                }

                var nomeEstacao = Ext.getCmp("txtNomeEstacao").getValue();
                if (nomeEstacao == ""){
                    Ext.Msg.show({ title: 'Aviso', msg: 'O campo Fornecedor. Nome é obrigatório!', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                    return;
                }   
            
                var convocacao = Ext.getCmp("txtConvocacao").getValue();
                var matriculaManutencao = Ext.getCmp("txtMatriculaManutencao").getValue();
                var matriculaEstacao = Ext.getCmp("txtMatriculaEstacao").getValue();

                var vagoes = [];

                //Recupera vagões do GRID
                var itens = gridVagoes.getStore().getRange();
                //console.log(itens);
                for (var i = 0; i < itens.length; i++) {

                    var _osRevistamentoVagaoItemDto = new OSRevistamentoVagaoItemDto();
                    _osRevistamentoVagaoItemDto.IdItem = itens[i].data.IdItem;
                    _osRevistamentoVagaoItemDto.Concluido = itens[i].data.Concluido;
                    _osRevistamentoVagaoItemDto.Retirar = itens[i].data.Retirar;
                    _osRevistamentoVagaoItemDto.Comproblema = itens[i].data.Comproblema;
                    _osRevistamentoVagaoItemDto.Observacao = itens[i].data.Observacao;

                    vagoes.push(_osRevistamentoVagaoItemDto);
                }

                Ext.getBody().mask("Processando dados...", "x-mask-loading");

                //Parâmetros para método SAVE
                var jsonRequest = $.toJSON({ idOs: idOs, horaInicio: horaInicio, horaTermino: horaTermino, horaEntrega: horaEntrega,
                    linha: linha, convocacao: convocacao, quantVagoes: quantVagoes, idFornecedor: ddlFornecedor, local: localServico,
                    quantVagoesVedados: quantVagoesVedados, quantVagoesGambitados: quantVagoesGambitados, quantVagoesVazios: quantVazios,
                    nomeManutencao: nomeManutencao, matriculaManutencao: matriculaManutencao, nomeEstacao: nomeEstacao, matriculaEstacao: matriculaEstacao,
                    vagoes: vagoes
                });                   

                $.ajax({
                    url: '<%= Url.Action("FecharOs", "OSRevistamentoVagao") %>',
                    type: "POST",
                    dataType: 'json',
                    data: jsonRequest,
                    timeout: 300000,
                    contentType: "application/json; charset=utf-8",
                    failure: function (conn, data) {
                        Ext.Msg.alert("Mensagem de Erro", "Ocorreu um erro inesperado");
                        Ext.getBody().unmask();
                    },
                    success: function () {

                        Ext.getBody().unmask();
                        Ext.Msg.show({ title: 'Aviso', msg: 'OS de Limpeza/Lavagem fechada com sucesso. Redirecionando...', buttons: Ext.Msg.OK, icon: Ext.MessageBox.WARNING });

                        var url = '<%= Url.Action("Index", "OSRevistamentoVagao") %>';
                        window.location = url;
                    }

                });
            }
        }

        function comparaTime(timeInicio, timeFim) {
            var timeInicio = new DateTime(timeInicio);
            var timeFim = new DateTime(timeFim);
            if (timeInicio > timeFim) {
                return false;
            } else {
                return true;
            }
        }

        function Voltar() {
            
            Ext.getBody().mask("Processando dados...", "x-mask-loading");
            var url = '<%= Url.Action("Index", "OSRevistamentoVagao")%>';

            window.location = url;
        }

         //Evendo das checkboxes para marcar todos no grid
        function CheckGridColumns(arrChecked) {

            var marcarConcluidos = false;
            if (arrChecked.length >= 1) {

                if (arrChecked[0].boxLabel == "Como concluído") {
                    marcarConcluidos = true;
                }
            }

            if (marcarConcluidos) {
                SetAllCheckConcluido();
            } else { NotSetAllCheckConcluido(); }

        }

        function SetAllCheckConcluido() {
            
            $(".x-grid3-cc-checkConcluido").removeClass("x-grid3-check-col");
            $(".x-grid3-cc-checkConcluido").addClass("x-grid3-check-col-on");
            var itens = gridVagoes.getStore().getRange();
            for (var i = 0; i < itens.length; i++) {
                itens[i].data.Concluido = true;
            }
        }

        function NotSetAllCheckConcluido() {

            $(".x-grid3-cc-checkConcluido").removeClass("x-grid3-check-col-on");
            $(".x-grid3-cc-checkConcluido").addClass("x-grid3-check-col");
            var itens = gridVagoes.getStore().getRange();
            for (var i = 0; i < itens.length; i++) {
                itens[i].data.Concluido = false;
            }
        }

        //***************************** BOTOES *****************************//
        var btnSalvarImprimirOS = {
            name: 'btnSalvarImprimirOS',
            id: 'btnSalvarImprimirOS',
            text: 'Salvar e Imprimir',
            iconCls: 'icon-printer',
            handler: ValidarEFecharOS
        };

        var btnVoltar = {
            name: 'btnVoltar',
            id: 'btnVoltar',
            text: 'Voltar',
            iconCls: 'icon-left',
            handler: Voltar
        };

        //***************************** PAINEL *****************************//
        var panelForm = new Ext.form.FormPanel({
            id: 'panelForm',
            layout: 'form',
            labelAlign: 'top',
            border: false,
            autoHeight: true,
            title: "Gerar Ordem de Serviço",
            region: 'center',
            bodyStyle: 'padding: 15px',
            items: [containerLinha1, containerLinha2, containerLinha3, containerLinha4, containerLinha5],
            buttonAlign: "center",
            buttons: [btnSalvarImprimirOS, btnVoltar]
        });

        //***************************** RENDER *****************************//
        Ext.onReady(function () {
            panelForm.render("divContent");


            fornecedorStore.on('load', function (store) {

                var range = ddlFornecedor.getStore().getRange();
                var existe = false;
                $.each(range, function (c, col) {

                    //Verifica se registro que usuario está tentando inserir já existe
                    if (col.data.IdFornecedorOs == idFornecedor) {
                        existe = true;
                    }
                });

                if (existe)
                    Ext.getCmp('ddlFornecedor').setValue(idFornecedor);
            });

            gridVagoesStore.load({ params: { start: 0, limit: 50} });

            $("#ckMarcarTodos").click(function () {
                
                
            });
        });
        
    
    </script>
</asp:Content>
<asp:Content ID="MainContent" ContentPlaceHolderID="Content" runat="server">
    <div id="header-content">
        <h1>
            Manutenção</h1>
        <small>Você está em Manutenção > Gerar Ordem de Serviço</small>
        <br />
    </div>
    <div id="divContent">
    </div>
</asp:Content>
