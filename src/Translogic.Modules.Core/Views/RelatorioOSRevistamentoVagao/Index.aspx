﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Interna.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content runat="server" ID="HeadContent" ContentPlaceHolderID="HeadContent">
    <script type="text/javascript">

        ////////////////////////////////////////
        // 
        // Definição dos serviços acessados
        //
        ////////////////////////////////////////

        // Carrega os tipos de os existentes
        var localStore = new Ext.data.JsonStore({
            root: "Items",
            autoLoad: true,
            proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("ObterLocais", "OSRevistamentoVagao") %>', timeout: 600000 }),
            id: 'localStore',
            fields: ['IdLocal', 'Local']
        });

        var tipoStore = new Ext.data.JsonStore({
            root: "Items",
            autoLoad: true,
            proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("ObterTipos", "OSRevistamentoVagao") %>', timeout: 600000 }),
            id: 'tipoStore',
            fields: ['IdTipo', 'DescricaoTipo']
        }); 

        var statusStore = new Ext.data.JsonStore({
            root: "Items",
            autoLoad: true,
            proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("ObterStatus", "OSRevistamentoVagao") %>', timeout: 600000 }),
            id: 'statusStore',
            fields: ['IdStatus', 'DescricaoStatus']
        });

        var resultStore = new Ext.data.JsonStore({
            root: "Items",
            totalProperty: 'Total',
            id: 'resultStore',
            name: 'resultStore',
            proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("ConsultaOSRevistamento", "OSRevistamentoVagao") %>', timeout: 600000 }),
            paramNames: {
                sort: "detalhesPaginacaoWeb.Sort",
                dir: "detalhesPaginacaoWeb.Dir",
                start: "detalhesPaginacaoWeb.Start",
                limit: "detalhesPaginacaoWeb.Limit"
            },
            fields: ['IdOs','Data', 'LocalServico', 'NumOs', 'Tipo', 'Fornecedor', 'Status', 'UltimaAlteracao', 'VagaoRetirado', 'ProblemaVedacao', 'CarregadoEntreVazios', 'Usuario']
        });

        ////////////////////////////////////////
        //
        // Objetos renderizados em tela
        //
        ////////////////////////////////////////

        $(function () {

            ////////////////////////////////////////
            //
            // FILTROS
            //
            ////////////////////////////////////////
            var txtDataInicial = new Ext.form.DateField({
                fieldLabel: 'Data Inicial',
                id: 'txtDataInicial',
                name: 'txtDataInicial',
                dateFormat: 'd/n/Y',
                width: 83,
                plugins: [new Ext.ux.InputTextMask('99/99/9999', true)]

            });

            var txtDataFinal = new Ext.form.DateField({
                fieldLabel: 'Data Final',
                id: 'txtDataFinal',
                name: 'txtDataFinal',
                dateFormat: 'd/n/Y',
                width: 83,
                plugins: [new Ext.ux.InputTextMask('99/99/9999', true)]

            });

            var ddlLocal = new Ext.form.ComboBox({
                editable: true,
                typeAhead: true,
                forceSelection: true,
                disableKeyFilter: true,
                triggerAction: 'all',
                lazyRender: true,
                mode: 'local',
                valueField: 'IdLocal',
                displayField: 'Local',
                fieldLabel: 'Local',
                id: 'ddlLocal',
                name: 'ddlLocal',
                width: 135,
                store: localStore,
                value: 'Todos'
            });


            var txtNumOs = {
                xtype: 'textfield',
                name: 'txtNumOs',
                id: 'txtNumOs',
                fieldLabel: 'OS',
                autoCreate: {
                    tag: 'input',
                    type: 'text',
                    autocomplete: 'off',
                    maxlength: '12'
                },
                maskRe: /[0-9+;]/,
                style: 'text-transform:uppercase;',
                width: 85
            };


            var ddlTipo = new Ext.form.ComboBox({
                editable: true,
                typeAhead: true,
                forceSelection: true,
                disableKeyFilter: true,
                triggerAction: 'all',
                lazyRender: true,
                mode: 'local',
                valueField: 'IdTipo',
                displayField: 'DescricaoTipo',
                fieldLabel: 'Tipo',
                id: 'ddlTipo',
                name: 'ddlTipo',
                width: 200,
                store: tipoStore,
                value: 'Todos'
            });


            var ddlStatus = new Ext.form.ComboBox({
                editable: true,
                typeAhead: true,
                forceSelection: true,
                disableKeyFilter: true,
                triggerAction: 'all',
                lazyRender: true,
                mode: 'local',
                store: statusStore,
                valueField: 'IdStatus',
                displayField: 'DescricaoStatus',
                fieldLabel: 'Status',
                id: 'ddlStatus',
                width: 100,
                value: '99'
            });

            var chkFiltros = new Ext.form.CheckboxGroup({
                columns: 1,
                id: 'chkFiltros',
                name: 'chkFiltros',
                items: [
                    { boxLabel: 'Vagão retirado', name: 'chkVagaoRetirado', id: 'chkVagaoRetirado', height: 8 },
                    { boxLabel: 'Problema na vedação e/ou gambitagem', name: 'chkProblemaVedacao', id: 'chkProblemaVedacao', height: 8 },
                    { boxLabel: 'Vagão carregado entre os vazios', name: 'chkCarregadoEntreVazios', id: 'chkCarregadoEntreVazios', height: 8 }
                ]
            });

            //Item do Array de Componentes (1 por cada componente criado)
            var arrDataInicial = {
                width: 100,
                layout: 'form',
                border: false,
                items: [txtDataInicial]
            };

            var arrDataFinal = {
                width: 100,
                layout: 'form',
                border: false,
                items: [txtDataFinal]
            };

            var arrLocal = {
                width: 150,
                layout: 'form',
                border: false,
                items: [ddlLocal]
            };

            var arrNumOs = {
                width: 100,
                layout: 'form',
                border: false,
                items: [txtNumOs]
            };

            var arrTipo = {
                width: 210,
                layout: 'form',
                border: false,
                items: [ddlTipo]
            };

            var arrStatus = {
                width: 110,
                layout: 'form',
                border: false,
                items: [ddlStatus]
            };

            var arrFiltros = {
                width: 250,
                layout: 'form',
                border: false,
                items: [chkFiltros]
            };

            //Array de componentes do form 
            var arrCampos = {
                layout: 'column',
                border: false,
                items: [arrDataInicial, arrDataFinal, arrLocal, arrNumOs, arrTipo, arrStatus, arrFiltros]
            };


            //Conteudo do Form que sera criado
            var filtros = new Ext.form.FormPanel({
                id: 'grid-filtros',
                title: "Filtros",
                region: 'center',
                bodyStyle: 'padding:15px 15px 0px 15px;',
                layout: 'form',
                labelAlign: 'top',
                items: [arrCampos],
                buttonAlign: "center",
                buttons:
                    [{
                        text: 'Pesquisar',
                        type: 'submit',
                        iconCls: 'icon-find',
                        handler: function (b, e) {
                            Pesquisar();
                        }
                    },
                        {
                            text: 'Limpar',
                            type: 'submit',
                            handler: function (b, e) {
                                Limpar();
                            }
                        }
                    ]
            });


            ////////////////////////////////////////
            //
            // GRID
            //
            ////////////////////////////////////////


            // Toolbar da Grid
            var pagingToolbar = new Ext.PagingToolbar({
                pageSize: 50,
                store: resultStore,
                displayInfo: true,
                displayMsg: App.Resources.Web.GridExibindoRegistros,
                emptyMsg: App.Resources.Web.GridSemRegistros,
                paramNames: {
                    start: "detalhesPaginacaoWeb.Start",
                    limit: "detalhesPaginacaoWeb.Limit"
                }
            });

            var sm = new Ext.grid.CheckboxSelectionModel({
                singleSelect: true,
                header: ''
            });

            var cm = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true
                },
                columns: [
                    new Ext.grid.RowNumberer(),
                    sm,
                    { header: 'Data', dataIndex: "Data", sortable: false, width: 110 },
                    { header: 'Local', dataIndex: "LocalServico", sortable: false, width: 50 },
                    { header: 'OS', dataIndex: "NumOs", sortable: false, width: 35 },
                    { header: 'Tipo', dataIndex: "Tipo", sortable: false, width: 80 },
                    { header: 'Fornecedor', dataIndex: "Fornecedor", sortable: false, width: 200 },
                    { header: 'Status', dataIndex: "Status", sortable: false, width: 90 },
                    { header: 'Última mudança de status', dataIndex: "UltimaAlteracao", sortable: false, width: 130 },
                    { header: 'Vagão retirado', dataIndex: "VagaoRetirado", sortable: false, width: 90 },
                    { header: 'Problema de vedação e/ou gambitagem', dataIndex: "ProblemaVedacao", sortable: false, width: 120 },
                    { header: 'Vagão carregado entre os vazios', dataIndex: "CarregadoEntreVazios", sortable: false, width: 120 },
                    { header: 'Usuário', dataIndex: "Usuario", sortable: false, width: 100 }
                ]
            });

            var gridRowActions = new Ext.ux.grid.RowActions({
                dataIndex: '',
                header: '',
                align: 'center',
                actions: [
                    {
                        iconCls: 'icon-del',
                        tooltip: 'Excluir Motivo'
                    }],
                callbacks: {
                    'icon-del': function (grid, record, action, row, col) {

                        if (record.data.IdMotivo) {
                            if (Ext.Msg.confirm("Excluir", "Deseja realmente excluir esse Motivo ?", function (btn, text) {
                                if (btn == 'yes') {
                                    //ExcluirMotivo(record.data.IdVagaoMotivo, record.data.IdVagao, record.data.IdMotivo);
                                    Pesquisar();
                                }

                            }));
                        }
                    }
                }
            });

            var gridResults = new Ext.grid.EditorGridPanel({
                id: 'gridResults',
                name: 'gridResults',
                autoLoadGrid: false,
                height: 360,

                stripeRows: true,
                cm: cm,
                region: 'center',
                viewConfig: {
                    forceFit: true,
                    emptyText: 'Não possui dado(s) para exibição.'
                },
                autoScroll: true,
                loadMask: { msg: App.Resources.Web.Carregando },
                store: resultStore,
                plugins: [gridRowActions],
                tbar: [
                 {
                     id: 'btnVizualizar',
                     text: 'Visualizar última via',
                     tooltip: 'Visualizar última via',
                     disabled: true,
                     iconCls: 'icon-detail',
                     handler: function (c) {

                         Ext.getBody().mask("Processando dados...", "x-mask-loading");

                         // Busca idOS linha selecionada
                         var idOs = BuscaIDOrdemServicoSelecionado();
                         var url = '<%= Url.Action("VisualizarOSRevistamento", "OSRevistamentoVagao") %>';

                         url += "?idOs=" + idOs;

                         window.location = url;
                     }
                 },
                    {
                        id: 'btnGerarPdf',
                        text: 'Imprimir',
                        tooltip: 'Imprimir uma OS em PDF',
                        iconCls: 'icon-printer',
                        disabled: true,
                        handler: function (c) {

                            Ext.getBody().mask("Processando dados...", "x-mask-loading");

                            // Busca idOS linha selecionada
                            var idOs = BuscaIDOrdemServicoSelecionado();

                            var url = '<%= Url.Action("Imprimir", "OSRevistamentoVagao") %>?idOS=' + idOs;
                            window.open(url, "");

                            Ext.getBody().unmask();
                        }
                    },
                    {
                        id: 'btnExportar',
                        text: 'Exportar',
                        tooltip: 'Exportar para Excel',
                        iconCls: 'icon-page-excel',
                        handler: function (c) {

                            var dtI = Ext.getCmp("txtDataInicial").getRawValue();
                            var dtF = Ext.getCmp("txtDataFinal").getRawValue();
                            if ((dtI != "" && dtI != "__/__/____") && (dtF != "" && dtF != "__/__/____")) {
                                if (comparaDatas(dtI, dtF)) {

                                    Ext.getBody().mask("Processando dados...", "x-mask-loading");

                                    var url = '<%= Url.Action("ObterConsultarOSRevistamentoVagoesExportar", "OSRevistamentoVagao") %>';

                                    var dataInicial = Ext.getCmp("txtDataInicial").getRawValue();
                                    var dataFinal = Ext.getCmp("txtDataFinal").getRawValue();
                                    if (dataInicial == "__/__/____")
                                        dataInicial = "";
                                    if (dataFinal == "__/__/____")
                                        dataFinal = "";

                                    ddlLocal = Ext.getCmp("ddlLocal");
                                    var idLocal = (ddlLocal.getRawValue() == "Todos" || ddlLocal.getRawValue() == "0") ? "" : ddlLocal.getRawValue();

                                    var txtNumOs = Ext.getCmp("txtNumOs").getValue();

                                    ddlTipo = Ext.getCmp("ddlTipo");
                                    var idTipo = (ddlTipo.getValue() == "Todos" || ddlTipo.getValue() == "0") ? "" : ddlTipo.getValue();

                                    ddlStatus = Ext.getCmp("ddlStatus");
                                    var idStatus = (ddlStatus.getValue() == "Todos" || ddlStatus.getValue() == "0") ? "" : ddlStatus.getValue();

                                    //                            ddlRetrabalho = Ext.getCmp("ddlRetrabalho");
                                    //                            var retrabalho = (ddlRetrabalho.getValue() == "Todos" || ddlRetrabalho.getValue() == "0") ? "" : ddlRetrabalho.getValue();

                                    //Todo checks
                                    var vagaoRetirado = $("#chkVagaoRetirado").is(':checked');
                                    var problemaVedacao = $("#chkProblemaVedacao").is(':checked');
                                    var vagaoCarregado = $("#chkCarregadoEntreVazios").is(':checked');


                                    url += "?dataInicial=" + dataInicial + '&dataFinal=' + dataFinal + '&local=' + idLocal;
                                    url += '&numOs=' + txtNumOs + '&idTipo=' + idTipo + '&idStatus=' + idStatus;
                                    url += '&vagaoRetirado=' + vagaoRetirado + '&problemaVedacao=' + problemaVedacao + '&vagaoCarregado=' + vagaoCarregado;

                                    window.open(url, "");

                                    Ext.getBody().unmask();

                                }
                                else
                                    Ext.Msg.show({ title: 'Aviso', msg: 'Data Inicial deve ser menor ou igual a Data Final!', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                            }
                            else
                                Ext.Msg.show({ title: 'Aviso', msg: 'Favor informar as Datas Inicial e Final!', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                        }
                    }
                ],
                bbar: pagingToolbar,
                sm: sm,
                listeners: {
                    cellclick: function (e, rowIndex, columnIndex) {

                        var record = gridResults.getStore().getAt(rowIndex);
                        var status = record.get('Status');
                        var selection = sm.getSelections();
                        var selecionado = selection.length > 0 ? true : false;

                        if (selecionado == true) {

                            HabilitaBotoes();
                        }
                        else {

                            DesabilitaBotoes();
                        }
                    }
                }
            });

            gridResults.getStore().proxy.on('beforeload', function (p, params) {

                var dataInicial = Ext.getCmp("txtDataInicial").getRawValue();
                var dataFinal = Ext.getCmp("txtDataFinal").getRawValue();
                if (dataInicial == "__/__/____")
                    dataInicial = "";
                if (dataFinal == "__/__/____")
                    dataFinal = "";
                var txtNumOs = Ext.getCmp("txtNumOs").getValue();

                ddlLocal = Ext.getCmp("ddlLocal");
                var idLocal = (ddlLocal.getRawValue() == "Todos" || ddlLocal.getRawValue() == "0") ? "" : ddlLocal.getRawValue();

                ddlTipo = Ext.getCmp("ddlTipo");
                var idTipo = (ddlTipo.getValue() == "Todos" || ddlTipo.getValue() == "0") ? "" : ddlTipo.getValue();

                ddlStatus = Ext.getCmp("ddlStatus");
                var idStatus = (ddlStatus.getValue() == "Todos" || ddlStatus.getValue() == "0") ? "" : ddlStatus.getValue();

                //Todo checks
                var vagaoRetirado = $("#chkVagaoRetirado").is(':checked');
                var problemaVedacao = $("#chkProblemaVedacao").is(':checked');
                var vagaoCarregado = $("#chkCarregadoEntreVazios").is(':checked');

                params['dataInicial'] = dataInicial;
                params['dataFinal'] = dataFinal;
                params['local'] = idLocal;
                params['numOs'] = txtNumOs;
                params['idTipo'] = idTipo;
                params['idStatus'] = idStatus;
                params['vagaoRetirado'] = vagaoRetirado;
                params['problemaVedacao'] = problemaVedacao;
                params['vagaoCarregado'] = vagaoCarregado;

            });

            new Ext.Viewport({
                layout: 'border',
                margins: 10,
                items: [
                    {
                        region: 'north',
                        height: 220,
                        autoScroll: true,
                        items: [{
                            region: 'center',
                            applyTo: 'header-content'
                        },
                            filtros]
                    },
                    gridResults
                ]
            });


            ////////////////////////////////////////
            //
            // Funções utilizadas nas telas
            //
            ////////////////////////////////////////
            function comparaDatas(dataIni, dataFim) {

                var arrDtIni = dataIni.split('/');
                var arrDtFim = dataFim.split('/');

                var dtIni = new Date(arrDtIni[2] + '/' + arrDtIni[1] + '/' + arrDtIni[0]);
                var dtFim = new Date(arrDtFim[2] + '/' + arrDtFim[1] + '/' + arrDtFim[0]);

                return dtIni <= dtFim;
            }

            function Pesquisar() {
                var dtI = Ext.getCmp("txtDataInicial").getRawValue();
                var dtF = Ext.getCmp("txtDataFinal").getRawValue();
                if ((dtI != "" && dtI != "__/__/____") && (dtF != "" && dtF != "__/__/____")) {
                    if (comparaDatas(dtI, dtF)) {
                        resultStore.load({ params: { start: 0, limit: 50} });
                    }
                    else
                        Ext.Msg.show({ title: 'Aviso', msg: 'Data Inicial deve ser menor ou igual a Data Final!', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                }
                else
                    Ext.Msg.show({ title: 'Aviso', msg: 'Favor informar as Datas Inicial e Final!', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
            }

            function Limpar() {
                resultStore.removeAll();
                Ext.getCmp("grid-filtros").getForm().reset();
                DesabilitaBotoes();
            }


            function ColocarHojeEmDatas() {

                var today = new Date();
                var dd = today.getDate();
                var mm = today.getMonth() + 1; //January is 0!
                var yyyy = today.getFullYear();

                if (dd < 10) {
                    dd = '0' + dd
                }

                if (mm < 10) {
                    mm = '0' + mm
                }

                today = dd + '/' + mm + '/' + yyyy;

                Ext.getCmp("txtDataInicial").setValue(today);
                Ext.getCmp("txtDataFinal").setValue(today);

            }


            function VerificaVagoesSelecionado() {
                var resultado = false;
                var selection = sm.getSelections();
                var iCont = 0;

                if (selection && selection.length > 0) {
                    var idSituacao = selection[0].data.IdSituacao;

                    selection = sm.getSelections();

                    $.each(selection, function (i, e) {
                        if (idSituacao == e.data.IdSituacao) {
                            iCont++;
                        }
                    });

                    resultado = selection.length == iCont ? true : false;
                }

                return resultado;
            }

            // Busca e armazena lista de ids de vagões
            function BuscaIDOrdemServicoSelecionado() {
                var selection = sm.getSelections();
                var selectedId = 0;
                if (selection && selection.length > 0) {
                    var selectedId = selection[0].data.IdOs;
                }

                return selectedId;
            }

            function VerificaOSSelecionado() {
                var resultado = false;
                var selection = sm.getSelections();
                var iCont = 0;
                if (selection && selection.length > 0) {

                    $.each(selection, function (i, e) {
                        iCont++;
                    });

                    // Verifica mensagem de erro para usuario
                    if (iCont > 1) {
                        resultado = "maior";
                    } else if (iCont == 1 && selection[0].data.Status == "Fechada") {
                        resultado = "Fechada";
                    } else if (iCont == 1) {
                        resultado = "foi";
                    } else {
                        resultado = "menor";
                    }
                };
                return resultado;
            }

            function DesabilitaBotoes() {
                Ext.getCmp("btnGerarPdf").setDisabled(true);
                Ext.getCmp("btnVizualizar").setDisabled(true);
            }

            function HabilitaBotoes() {
                Ext.getCmp("btnGerarPdf").setDisabled(false);
                Ext.getCmp("btnVizualizar").setDisabled(false);
            }


            Ext.onReady(function () {
                $("#txtDataInicial").val("__/__/____");
                $("#txtDataFinal").val("__/__/____");

                ColocarHojeEmDatas();


                statusStore.on('load', function (store) {
                    Ext.getCmp('ddlStatus').setValue("99");
                });

                Pesquisar();
            });

        });

    </script>
</asp:Content>
<asp:Content runat="server" ID="Content2" ContentPlaceHolderID="Content">
    <div id="header-content">
        <h1>
              Relat&oacute;rio de Ordem de Serviço de Revistamento de Vagão</h1>
        <small>Você está em Operação > Manutenção > Ordem de serviço de revistamento de vagão</small>
        <br />
    </div>
</asp:Content>
