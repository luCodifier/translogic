﻿<%@ Page Title="Portal CT" Language="C#" MasterPageFile="~/Views/Shared/Interna.Master"
         Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        var grid = null;
        var viewPort = null;
        var fm = Ext.form;

        function tooltip(value, metaData) {
            metaData.attr = 'ext:qtip="' + value + '"';
            return value;
        }        

        Ext.onReady(function () {
            var gridStore = new Ext.data.GroupingStore({
                url: '<%= Url.Action("ObterPrevisoesParadas") %>',
                autoLoad: false,

                remoteGroup: true,
                groupField: 'OS',

                sortInfo: {
                    field: 'Id',
                    direction: 'ASC'
                },

                reader: new Ext.data.JsonReader({
                    totalProperty: 'Total',
                    root: 'Items',
                    idProperty: 'Id',

                    fields: [
                        { name: 'Id', type: 'int' },
                        { name: 'OS', type: 'int' },
                        { name: 'Prefixo', type: 'string' },
                        { name: 'Origem', type: 'string' },
                        { name: 'Destino', type: 'string' },
                        { name: 'Parada', type: 'string' },
                        { name: 'DataPrevista', type: 'date' },
                        { name: 'HoraPrevista', type: 'string' },
                        { name: 'TempoParada', type: 'string' },
                        { name: 'UltimaAlteracao', type: 'string' },
                        { name: 'BloqueiaEdicao', type: 'string' }
                    ]
                })
            });

            grid = new Ext.grid.EditorGridPanel({
                name: 'grid',
                id: 'grid',
                region: 'center',
                store: gridStore,
                loadMask: { msg: App.Resources.Web.Carregando },
                clicksToEdit: 1,
                colModel: new Ext.grid.ColumnModel({
                    columns: [
                        { Id: 'Id', hidden: true },
                        { header: 'OS', dataIndex: "OS", sortable: true, renderer: tooltip, hidden: true },
                        { header: 'Prefixo', dataIndex: "Prefixo", sortable: true, renderer: tooltip, hidden: true },
                        { header: 'Origem', dataIndex: "Origem", sortable: true, renderer: tooltip, hidden: true },
                        { header: 'Destino', dataIndex: "Destino", sortable: true, renderer: tooltip, hidden: true },
                        { header: 'Parada', dataIndex: "Parada", sortable: true, renderer: tooltip },
                        {
                            header: 'Data Prevista', dataIndex: "DataPrevista", sortable: true, renderer: Ext.util.Format.dateRenderer('d/m/Y'),
                            editor: new fm.DateField({
                                allowBlank: true,
                                listeners: {
                                    change: function (field, e) {
                                        //salvarAlteracoes();
                                    }
                                }
                            })
                        },
                        {
                            header: 'Hora Prevista', dataIndex: "HoraPrevista", sortable: true,
                            editor: new Ext.form.TimeField({
                                format: 'H:i',
                                altFormats: 'H:i',
                                selectOnFocus: true,
                                minValue: '00:00',
                                increment: 60,
                                maxValue: '23:59',
                                maxlength: 5,
                                maskRe: /[0-9:]/
                            })
                        },
                        {
                            header: 'Tempo Parada', dataIndex: "TempoParada", sortable: true, renderer: tooltip,
                            editor: new fm.TimeField({
                                allowBlank: false,
                                listeners: {
                                    change: function (field, e) {
                                        //salvarAlteracoes();
                                    }
                                }
                            })
                            
                        },
                        { header: 'Última Alteração', dataIndex: "UltimaAlteracao", sortable: true, renderer: tooltip },
                        { Id: 'BloqueiaEdicao', hidden: true }
                    ],

                    defaults: {
                        sortable: true,
                        menuDisabled: false,
                        width: 20
                    }
                }),

                view: new Ext.grid.GroupingView({
                    startCollapsed: true,
                    forceFit: true,
                    groupTextTpl: '({text}) Prefixo: ({[values.rs[0].data["Prefixo"]]}) Origem: ({[values.rs[0].data["Origem"]]}) Destino: ({[values.rs[0].data["Destino"]]})'
                })
            });

            grid.on('beforeedit', function (editor) {
                if (editor.record.get('BloqueiaEdicao') == "True") {
                    return false;                    
                } else {
                    return true;                    
                }
            });
            
            var dsCorredores = new Ext.data.JsonStore({
                autoLoad: true,
                url: '<%= Url.Action("ObterCorredores") %>',
                fields: [
                    'Corredor',
                    'CorredorLabel'
                ],
                listeners: {
                    load: function(store, records) {
                        var rt = store.recordType;
                        store.insert(0, new rt({}));
                    }
                }
            });
            
            var filtroOs = {
                xtype: 'numberfield',
                allowDecimals: false,
                fieldLabel: 'OS',
                id: 'filtro-os',
                name: 'filtroOS',
                typeAhead: true,
                width: 83,
                allowBlank: true
            };
            
            var filtroPrefixo = {
                xtype: 'textfield',
                fieldLabel: 'Prefixo',
                id: 'filtro-prefixo',
                name: 'filtroPrefixo',
                typeAhead: true,
                width: 83,
                enableKeyEvents: true,
                style: {
                    textTransform: 'uppercase'
                },
                allowBlank: true
            };
            
            var filtroOrigem = {
                xtype: 'textfield',
                fieldLabel: 'Origem',
                id: 'filtro-origem',
                name: 'filtroOrigem',
                typeAhead: true,
                width: 83,
                enableKeyEvents: true,
                style: {
                    textTransform: 'uppercase'
                },
                allowBlank: true
            };
            
            var filtroDestino = {
                xtype: 'textfield',
                fieldLabel: 'Destino',
                id: 'filtro-destino',
                name: 'filtroDestino',
                typeAhead: true,
                width: 83,
                enableKeyEvents: true,
                style: {
                    textTransform: 'uppercase'
                },
                allowBlank: true
            };
            
            window.dsCorredores = dsCorredores;
            var cboCorredor = {
                xtype: 'combo',
                id: 'filtro-corredor',
                name: 'filtroCorredor',
                forceSelection: false,
                store: dsCorredores,
                triggerAction: 'all',
                mode: 'local',
                typeAhead: true,
                fieldLabel: 'Corredor',
                displayField: 'CorredorLabel',
                width: 115,
                valueField: 'Corredor',
                emptyText: '-- TODOS --',
                editable: false //,
                //tpl: '<tpl for="."><div class="x-combo-list-item">{TipoLabel}&nbsp;</div></tpl>'
            };
            
            var dataInicial = {
                xtype: 'datefield',
                fieldLabel: 'Data Início',
                id: 'filtro-data-inicial',
                name: 'dataInicial',
                width: 83,
                allowBlank: false,
                hiddenName: 'dataInicial',
                value: new Date()
            };
            
            var dataFinal = {
                xtype: 'datefield',
                fieldLabel: 'Data Fim',
                id: 'filtro-data-final',
                name: 'dataFinal',
                width: 83,
                allowBlank: false,
                hiddenName: 'dataFinal',
                value: new Date()
            };
            
            var arrOs = {
                width: 87,
                layout: 'form',
                border: false,
                items: [filtroOs]
            };
            
            var arrPrefixo = {
                width: 87,
                layout: 'form',
                border: false,
                items: [filtroPrefixo]
            };
            
            var arrOrigem = {
                width: 87,
                layout: 'form',
                border: false,
                items: [filtroOrigem]
            };
            
            var arrDestino = {
                width: 87,
                layout: 'form',
                border: false,
                items: [filtroDestino]
            };
            
            var arrCorredor = {
                width: 117,
                layout: 'form',
                border: false,
                items: [cboCorredor]
            };
            
            var arrDataIni = {
                width: 87,
                layout: 'form',
                border: false,
                items: [dataInicial]
            };
            
            var arrDataFim = {
                width: 87,
                layout: 'form',
                border: false,
                items: [dataFinal]
            };
            
            var arrlinha1 = {
                layout: 'column',
                border: false,
                items: [arrOs, arrPrefixo, arrOrigem, arrDestino, arrCorredor, arrDataIni, arrDataFim]
            };
            
            var arrCampos = new Array();
            arrCampos.push(arrlinha1);
            
            var filters = new Ext.FormPanel({
                id: 'grid-filtros',
                title: "Filtros",
                region: 'center',
                bodyStyle: 'padding: 15px',
                labelAlign: 'top',
                items: [arrCampos],
                buttonAlign: "left",
                buttons:
                [
                    {
                        text: 'Pesquisar',
                        type: 'submit',
                        iconCls: 'icon-find',
                        handler: function(b, e) {
                            if (filters.form.isValid()) {
                                gridStore.load();
                            }
                        }
                    },
                    {
                        text: 'Limpar',
                        handler: function (b, e) {
                            gridStore.removeAll();
                            Ext.getCmp("grid-filtros").getForm().reset();
                        },
                        scope: this
                    }
                ]
            });

            gridStore.proxy.on('beforeload', function (p, params) {
                var os = Ext.getCmp("filtro-os").getValue();
                var prefixo = Ext.getCmp("filtro-prefixo").getValue();
                var origem = Ext.getCmp("filtro-origem").getValue();
                var destino = Ext.getCmp("filtro-destino").getValue();
                var corredor = Ext.getCmp("filtro-corredor").getValue();
                var dataInicialAux = Ext.getCmp("filtro-data-inicial").getValue();
                var dataFinalAux = Ext.getCmp("filtro-data-final").getValue();

                params['filter[0].Campo'] = 'os';
                params['filter[0].Valor'] = os;
                params['filter[0].FormaPesquisa'] = 'Start';

                params['filter[1].Campo'] = 'prefixo';
                params['filter[1].Valor'] = prefixo;
                params['filter[1].FormaPesquisa'] = 'Start';

                params['filter[2].Campo'] = 'origem';
                params['filter[2].Valor'] = origem;
                params['filter[2].FormaPesquisa'] = 'Start';

                params['filter[3].Campo'] = 'destino';
                params['filter[3].Valor'] = destino;
                params['filter[3].FormaPesquisa'] = 'Start';

                params['filter[4].Campo'] = 'corredor';
                params['filter[4].Valor'] = corredor;
                params['filter[4].FormaPesquisa'] = 'Start';

                params['filter[5].Campo'] = 'dataInicial';
                params['filter[5].Valor'] = new Array(dataInicialAux.format('d/m/Y') + " " + dataInicialAux.format('H:i:s'));
                params['filter[5].FormaPesquisa'] = 'Start';

                params['filter[6].Campo'] = 'dataFinal';
                params['filter[6].Valor'] = new Array(dataFinalAux.format('d/m/Y') + " " + dataFinalAux.format('H:i:s'));
                params['filter[6].FormaPesquisa'] = 'Start';
            });

            viewPort = new Ext.Viewport({
                layout: 'border',
                margins: 10,
                items: [
                    {
                        region: 'north',
                        height: 185,
                        items: [
                            {
                                region: 'center',
                                applyTo: 'header-content'
                            }, filters
                        ]
                    },
                    grid,
                    {
                        id: 'detailPanel',
                        region: 'south',
                        autoScroll: true,
                        bodyStyle: {
                            background: '#CCCCCC',
                            padding: '5px'
                        }
                    }
                ]
            });
        });        
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div id="header-content">
        <h1>
            Portal CT</h1>
        <small>Você está em Portal CT > Previsões de Chegada</small>
        <br />
    </div>
</asp:Content>