﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Interna.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content runat="server" ID="HeadContent" ContentPlaceHolderID="HeadContent">
    <script type="text/javascript">

        //#Region StoreDDLsbtncance
        var destinosStore = new Ext.data.JsonStore({
            root: "Items",
            autoLoad: true,
            proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("ListaEstacaoDestino", "RedestinaTrem") %>', timeout: 600000 }),
            id: 'destinosStore',
            fields: ['Estacao', 'Estacao']
        });

        var obterTremStore = new Ext.data.JsonStore({
            root: "Items",
            id: 'obterTremStore',
            name: 'obterTremStore',
            proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("ConsultaTrem", "RedestinaTrem") %>', timeout: 600000 }),
            fields: ['OrdemServico', 'PrefixoTrem', 'Origem', 'Destino', 'LocalAtual', 'Rota', 'Situacao']
        });

        var redestinarTremStore = new Ext.data.JsonStore({
            root: "Items",
            id: 'redestinarTremStore',
            name: 'redestinarTremStore',
            proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("Redestinar", "RedestinaTrem") %>', timeout: 600000 })
        });

        //#endRegion

        function Pesquisar() {
            var varOs = Ext.getCmp("txtNumOs").getValue();

            // Verifica ordem de serviço.
            if (varOs == "")
            {
                Ext.Msg.show({ title: 'Aviso', msg: 'Você deve informar uma OS para pesquisar as informações do trem.', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                return;
            }

            obterTremStore.load({ 
                params: { 
                    numOs: varOs
                 } 
            });

            alert(obterTremStore.Items['PrefixoTrem']);
        }

        function Redestinar() {
            var varOs = Ext.getCmp("txtNumOs").getValue();
            var varNovoDestino = Ext.getCmp("ddlNovoDestino").getValue();

            // Verifica ordem de serviço.
            if (varOs == "") {
                Ext.Msg.show({ title: 'Aviso', msg: 'Você deve informar uma OS para pesquisar as informações do trem.', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                Ext.getCmp("txtNumOs").setFocus();
                return;
            }

            if (varNovoDestino == "" || varNovoDestino == "0") {
                Ext.Msg.show({ title: 'Aviso', msg: 'Você deve informar um novo destino para o trem.', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                Ext.getCmp("ddlNovoDestino").setFocus();
                return;
            }

            redestinarTremStore.load({
                params: {
                    numOs: varOs,
                    novoDestino: varNovoDestino
                }
            });
        }

        //#endRegion

        //#Region PainelMotivosVagoes

        $(function () {

            //#Region PesquisaFiltros
            var txtNumOs = {
                xtype: 'numberfield',
                name: 'txtNumOs',
                id: 'txtNumOs',
                fieldLabel: 'OS',
                allowDecimals: false,
                autoCreate: {
                    tag: 'input',
                    type: 'text',
                    autocomplete: 'off',
                    maxlength: '12'
                },
                width: 85,
                enableKeyEvents: true,
                listeners: {
                    keyup: function (field, e) {
                        field.setValue(field.getRawValue().replace("'", ""));
                    }
                }
            };

            var txtPrefixo = {
                xtype: 'textfield',
                name: 'txtPrefixo',
                id: 'txtPrefixo',
                fieldLabel: 'Prefixo',
                disabled: true,
                autoCreate: {
                    tag: 'input',
                    type: 'text',
                    autocomplete: 'off',
                    maxlength: '12'
                },
                width: 85
            };

            var txtOrigem = {
                xtype: 'textfield',
                name: 'txtOrigem',
                id: 'txtOrigem',
                fieldLabel: 'Origem',
                disabled: true,
                autoCreate: {
                    tag: 'input',
                    type: 'text',
                    autocomplete: 'off',
                    maxlength: '12'
                },
                width: 85
            };

            var txtDestino = {
                xtype: 'textfield',
                name: 'txtDestino',
                id: 'txtDestino',
                fieldLabel: 'Destino',
                disabled: true,
                autoCreate: {
                    tag: 'input',
                    type: 'text',
                    autocomplete: 'off',
                    maxlength: '12'
                },
                width: 85
            };

            var txtLocal = {
                xtype: 'textfield',
                name: 'txtLocal',
                id: 'txtLocal',
                fieldLabel: 'Local atual',
                disabled: true,
                autoCreate: {
                    tag: 'input',
                    type: 'text',
                    autocomplete: 'off',
                    maxlength: '12'
                },
                width: 85
            };

            var txtRota = {
                xtype: 'textfield',
                name: 'txtRota',
                id: 'txtRota',
                fieldLabel: 'Rota',
                disabled: true,
                autoCreate: {
                    tag: 'input',
                    type: 'text',
                    autocomplete: 'off',
                    maxlength: '12'
                },
                width: 85
            };

            var txtSituacao = {
                xtype: 'textfield',
                name: 'txtSituacao',
                id: 'txtSituacao',
                fieldLabel: 'Situação',
                disabled: true,
                autoCreate: {
                    tag: 'input',
                    type: 'text',
                    autocomplete: 'off',
                    maxlength: '12'
                },
                width: 85
            };

            var ddlNovoDestino = new Ext.form.ComboBox({
                editable: true,
                typeAhead: true,
                forceSelection: true,
                disableKeyFilter: true,
                triggerAction: 'all',
                lazyRender: true,
                mode: 'local',
                valueField: 'IdEstacao',
                displayField: 'Estacao',
                fieldLabel: 'Novo Destino',
                id: 'ddlNovoDestino',
                name: 'ddlNovoDestino',
                width: 100,
                store: [
                            'PCZ',
                            'PSN'
                        ],
                value: ''
            });

            //Item do Array de Componentes (1 por cada componente criado)
            var arrNumOs = {
                width: 100,
                layout: 'form',
                border: false,
                items: [txtNumOs]
            };

            var arrPrefixo = {
                width: 100,
                layout: 'form',
                border: false,
                items: [txtPrefixo]
            };

            var arrOrigem = {
                width: 100,
                layout: 'form',
                border: false,
                items: [txtOrigem]
            };

            var arrDestino = {
                width: 100,
                layout: 'form',
                border: false,
                items: [txtDestino]
            };

            var arrLocal = {
                width: 100,
                layout: 'form',
                border: false,
                items: [txtLocal]
            };

            var arrRota = {
                width: 100,
                layout: 'form',
                border: false,
                items: [txtRota]
            };

            var arrSituacao = {
                width: 100,
                layout: 'form',
                border: false,
                items: [txtSituacao]
            };

            var arrNovoDestino = {
                width: 110,
                layout: 'form',
                border: false,
                items: [ddlNovoDestino]
            };

            //Array de componentes do form 
            var arrCamposL1 = {
                layout: 'column',
                border: false,
                items: [arrNumOs, arrPrefixo, arrOrigem, arrDestino, arrRota, arrLocal, arrSituacao]
            };

            var arrCamposL2 = {
                layout: 'column',
                border: false,
                items: [arrNovoDestino]
            };

            //Conteudo do Form que sera criado
            var filtros = new Ext.form.FormPanel({
                id: 'grid-filtros',
                title: "Filtros",
                region: 'center',
                bodyStyle: 'padding:15px;',
                layout: 'form',
                labelAlign: 'top',
                items: [arrCamposL1, arrCamposL2],
                buttonAlign: "center",
                buttons:
                    [{
                        text: 'Pesquisar',
                        type: 'submit',
                        iconCls: 'icon-find',
                        handler: function (b, e) {
                            Pesquisar();
                        }
                    },
                        {
                            text: 'Redestinar',
                            type: 'submit',
                            handler: function (b, e) {
                                Redestinar();
                            }
                        }
                    ]
            });

            //#endRegion

            new Ext.Viewport({
                labelAlign: 'top',
                region: 'center',
                layout: 'border',
                border: false,
                
                items: [
                    {
                        region: 'center',
                        height: 200,
                        autoScroll: true,
                        items: [{
                            region: 'center',
                            applyTo: 'header-content'
                        },
                            filtros]
                    }
                ]
            });
        });

        Ext.onReady(function () {

            destinosStore.on('load', function (store) {
                //Ext.getCmp('ddlDestino').setValue("99");
            });

        });

    </script>
</asp:Content>
<asp:Content runat="server" ID="Content2" ContentPlaceHolderID="Content">
    <div id="header-content">
        <h1>
            Redestinar Trem</h1>
        <small>Você está em Operação > Circulação > Redestinar Trem</small>
        <br />
    </div>
     
</asp:Content>