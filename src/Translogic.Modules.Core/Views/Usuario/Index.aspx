<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Simples.Master" Inherits="Translogic.Modules.Core.Views.Usuario.Index" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server">
	<% Html.RenderPartial("MainResources"); %>
	
	<script type="text/javascript">

		function FormSuccess(form, action)
		{
			Ext.Msg.alert('Sucesso', action.message);
		}
		
		function FormError(form, action)
		{
			Ext.Msg.alert('Erro', action.message);
		}
	
	</script>
	
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">

	<%using (Html.BeginForm("Salvar", "Usuario", FormMethod.Post, new { id = "usuario-form", @class = "ext-form"})){ %>
	
		<label>
			C�digo:
			<%= Html.TextBox("usuario.Codigo", ViewData.Model.Codigo, ExtJs.Required) %>
		</label>
		
		<br />
		
		<label>
			Nome:
			<%= Html.TextBox("usuario.Nome", ViewData.Model.Nome, ExtJs.Required) %>
		</label>
		
		<br />
		
		<label>
			Senha:
			<%= Html.Password("usuario.Senha", ViewData.Model.Senha, ExtJs.Required && ExtJs.MinLength(5) && ExtJs.MaxLength(50)) %>
		</label>
		
		<br />
		
		<label>
			Ativo:
			<%= Html.CheckBox("usuario.Ativo", ViewData.Model.Ativo) %>
		</label>
		
		<br />
		
		<label>
			Empresa:
			<%= Html.ListBox("usuario.Empresa.Id", new SelectList((IEnumerable) ViewData["empresas"], "Id", "DescricaoDetalhada"), ExtJs.Required && ExtJs.EmptyText("Selecione..."))%>
		</label>
		
		<br />
		
		<input type="submit" class="submit" value="Salvar" />

	<%} %>

</asp:Content>
