﻿<%@ Page Title="Recomendação de Vagões" Language="C#" MasterPageFile="~/Views/Shared/Interna.Master"
    Inherits  ="System.Web.Mvc.ViewPage" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">

        //*****VARIÁVEIS*****
        var valoresAlterados = false;
        var arrayMalhas = ["LG", "MS", "MN"];

        //*****STORES*****
        var dsSimNao = new Ext.data.ArrayStore({
            fields: ['value', 'text'],
            data: [[true, 'SIM'], [false, 'NÃO']]
        });

        var gridStore = new Ext.data.JsonStore({
            root: "Items",
            id: 'gridStore',
            name: 'gridStore',
            proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("ObterListagemRecomendacaoVagao","RecomendacaoVagao") %>', timeout: 600000 }),
            fields: ['Id', 'IdCausaSeguro', 'DescricaoCausaSeguro', 'Recomendacao', 'Tolerancia']
        });

        var toleranciaStore = new Ext.data.JsonStore({
                root: "Items",
                autoLoad: true,
                url: '<%= Url.Action("ObterListagemRecomendacaoTolerancia","RecomendacaoVagao") %>',
                fields: ['Id', 'CodMalha', 'Valor', 'Email'],
                listeners: {
                    load: function(store, records) {
                        for (var i = 0; i < records.length ; i++) {
                            Ext.getCmp("hdn" + records[i].data.CodMalha + "Id").setValue(records[i].data.Id);
                            Ext.getCmp("txt" + records[i].data.CodMalha + "Valor").setValue(records[i].data.Valor);
                            Ext.getCmp("txt" + records[i].data.CodMalha + "Email").setValue(records[i].data.Email);                            
                        }

                        valoresAlterados = false;
                    }
                }
        });

        //****CAMPOS*****


        var painelTolerancia = new Ext.form.FormPanel(
        {
            id: 'painelTolerancia',
            title: 'Recomendação de Vagões',
            name: 'tolerancia',
            width: "100%",
            region: 'center',
            bodyStyle: 'padding: 10px',
            labelAlign: 'right',
            align: 'left',
            border: false,
            items:
            [
                {
                    width: '100%',
                    border: false,
                    layout: 'form',
                    align: 'left',
                    buttonAlign: 'left',
                    items:
                    [
                       {
                            border: false,
                            layout: 'column',
                             defaults : {
                                marging : 10
                            },
                            items:
                            [
                                {
                                    border: false,
                                    layout: 'form',
                                    items:
                                    [
                                        { xtype: 'hidden', id: 'hdnLGId', name: 'hdnLGId' },
                                        {
                                            xtype: 'numberfield',
                                            id: 'txtLGValor',
                                            name: 'txtLGValor',
                                            fieldLabel: 'UNLG',
                                            allowBlank: false,
                                            allowDecimals: true,
                                            allowNegative: false,
                                            decimalSeparator: ',',
                                            maxLength: 11,
                                            maxValue: 9999999.99,
                                            typeAhead: true,
                                            width: 80,
                                            autoCreate:
                                            {
                                                tag: "input",
                                                maxlength: 11
                                            },
                                            listeners: {
                                                'change': function(){
                                                  valoresAlterados = true;
                                                }
                                            }   
                                        }                               
                                    ]
                                },
                                {
                                    xtype: 'label',
                                    text: "Ton."

                                },
                                {
                                    border: false,
                                    layout: 'form',
                                    items:
                                    [
                                       {
                                            xtype: 'textfield',
                                            id: 'txtLGEmail',
                                            fieldLabel: 'E-mail',
                                            name: 'txtLGEmail',
                                            allowBlank: false,
                                            maxLength: 50,
                                            autoCreate: { 
                                                tag: "input",
                                                maxlength: 50,
                                                type: "text",
                                                size: "50",
                                                autocomplete: "off"
                                            }, 
                                            width: 240,
                                            hiddenName: 'txtLGEmail',
                                            style: 'text-transform: lowercase;', 
                                            listeners: {
                                                'change': function(){
                                                  valoresAlterados = true;
                                                }
                                            }                                          
                                        }                               
                                    ]
                                }                              
                            ]
                        },

                        {
                            border: false,
                            layout: 'column',
                             defaults : {
                                marging : 10
                            },
                            items:
                            [
                                {
                                    border: false,
                                    layout: 'form',                                    
                                    items:
                                    [
                                        { xtype: 'hidden', id: 'hdnMNId', name: 'hdnMNId' },
                                        {
                                            xtype: 'numberfield',
                                            id: 'txtMNValor',
                                            name: 'txtMNValor',
                                            fieldLabel: 'UNPR, UNMN',
                                            allowBlank: false,
                                            allowDecimals: true,
                                            allowNegative: false,
                                            decimalSeparator: ',',
                                            maxLength: 11,
                                            maxValue: 9999999.99,
                                            typeAhead: true,
                                            width: 80,
                                            autoCreate:
                                            {
                                                tag: "input",
                                                maxlength: 11
                                            },
                                            listeners: {
                                                'change': function(){
                                                  valoresAlterados = true;
                                                }
                                            }                                          

                                        }                               
                                    ]
                                },
                                {
                                    xtype: 'label',
                                    text: "Ton."

                                },
                                {
                                    border: false,
                                    layout: 'form',
                                    items:
                                    [
                                       {
                                            xtype: 'textfield',
                                            id: 'txtMNEmail',
                                            fieldLabel: 'E-mail',
                                            name: 'txtMNEmail',
                                            allowBlank: false,
                                            maxLength: 50,
                                            autoCreate: { 
                                                tag: "input",
                                                maxlength: 50,
                                                type: "text",
                                                size: "50",
                                                autocomplete: "off"
                                            }, 
                                            width: 240,
                                            hiddenName: 'txtMNEmail',
                                            style: 'text-transform: lowercase;',  
                                            listeners: {
                                                'change': function(){
                                                  valoresAlterados = true;
                                                }
                                            }                                          
                                          
                                        }                               
                                    ]
                                }                              
                            ]
                        },

                        {
                            border: false,
                            layout: 'column',
                             defaults : {
                                marging : 10
                            },
                            items:
                            [
                                {
                                    border: false,
                                    layout: 'form',
                                    items:
                                    [
                                        { xtype: 'hidden', id: 'hdnMSId', name: 'hdnMSId' },
                                        {
                                            xtype: 'numberfield',
                                            id: 'txtMSValor',
                                            name: 'txtMSValor',
                                            fieldLabel: 'UNRS',
                                            allowBlank: false,
                                            allowDecimals: true,
                                            allowNegative: false,
                                            decimalSeparator: ',',
                                            maxLength: 11,
                                            maxValue: 9999999.99,
                                            typeAhead: true,
                                            width: 80,
                                            autoCreate:
                                            {
                                                tag: "input",
                                                maxlength: 11
                                            },
                                            listeners: {
                                                'change': function(){
                                                  valoresAlterados = true;
                                                }
                                            }                                          

                                        }                               
                                    ]
                                },
                                {
                                    xtype: 'label',
                                    text: "Ton."

                                },
                                {
                                    border: false,
                                    layout: 'form',
                                    items:
                                    [
                                       {
                                            xtype: 'textfield',
                                            id: 'txtMSEmail',
                                            fieldLabel: 'E-mail',
                                            name: 'txtMSEmail',
                                            allowBlank: false,
                                            maxLength: 50,
                                            autoCreate: { 
                                                tag: "input",
                                                maxlength: 50,
                                                type: "text",
                                                size: "50",
                                                autocomplete: "off"
                                            }, 
                                            width: 240,
                                            hiddenName: 'txtMSEmail',
                                            style: 'text-transform: lowercase;',                                            
                                            listeners: {
                                                'change': function(){
                                                  valoresAlterados = true;
                                                }
                                            }                                          
                                        }                               
                                    ]
                                }                              
                            ]
                        }
                    ],
                    buttons:
                    [
                        {
                            name: 'btnSalvar',
                            id: 'btnSalvar',
                            text: 'Salvar',
                            iconCls: 'icon-save',
                            handler: function () {
                                salvarAlteracoes();
                            }
                        },
                        {
                            name: 'btnCancelar',
                            id: 'btnCancelar',
                            text: 'Cancelar',
                            iconCls: 'icon-cancel',
                            handler: function () {
                                cancelarAlteracoes();
                            }
                        }
                    ]
                }
            ]
        });

        //***** GRID *****//

        // create reusable renderer
        Ext.util.Format.comboRenderer = function (combo) {
            return function (value) {
                var record = combo.findRecord(combo.valueField, value);
                return record ? record.get(combo.displayField) : combo.valueNotFoundText;
            }
        }

        // create the combo instance
        var combo = new Ext.form.ComboBox({
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            store: dsSimNao,
            valueField: 'value',
            displayField: 'text',
            editable: false,
            tpl: '<tpl for="."><div class="x-combo-list-item">{text}&nbsp;</div></tpl>'
        });

        // snippet of column model used within grid
        var cm = new Ext.grid.ColumnModel(
                [
                    //new Ext.grid.RowNumberer(),
                    {
                    header: 'ID',
                    dataIndex: "Id",
                    hidden: true
                    },
                    {
                        header: 'CAUSA',
                        width: 100,
                        autoExpandColumn: true,
                        dataIndex: "DescricaoCausaSeguro",
                        sortable: true
                    },
                    {
                        header: 'RECOMENDAÇÃO',
                        dataIndex: "Recomendacao",
                        autoExpandColumn: false,
                        width: 30,
                        sortable: false,
                        editor: combo, // specify reference to combo instance
                        renderer: Ext.util.Format.comboRenderer(combo) // pass combo instance to reusable renderer
                    },
                    {
                        header: 'CONSIDERA TOLERÂNCIA',
                        dataIndex: "Tolerancia",
                        autoExpandColumn: false,
                        width: 50,
                        sortable: false,
                        editor: combo, // specify reference to combo instance
                        renderer: Ext.util.Format.comboRenderer(combo) // pass combo instance to reusable renderer
                    }
                ]);


        var grid = new Ext.grid.EditorGridPanel({
            id: 'grid',
            name: 'grid',
            autoLoadGrid: false,
            height: 450,
            width: 700,
            limit: 10,
            stripeRows: true,
            region: 'center',
            viewConfig: {
                forceFit: true,
                emptyText: 'Não possui dado(s) para exibição.'
            },
            autoScroll: true,
            loadMask: { msg: App.Resources.Web.Carregando },
            store: gridStore,
            cm: cm,
            clicksToEdit: 1

        });


        var painelGrid = new Ext.form.FormPanel({
            id: 'painelGrid',
            layout: 'form',
            labelAlign: 'top',
            border: false,
            autoHeight: true,
            title: "",
            region: 'center',
            bodyStyle: 'padding: 0px 10px 0px 10px',
            items: [grid]
        });


        //***** FUNÇÕES *****//
        function isNullOrEmpty(value) {
            return value === null || value === "";
        }
       
       function validateMaxValue(value, max) 
        {
            return value > max;
        }

        function validarEmail(email) {

            var er = RegExp(/^[A-Za-z0-9_\-\.]+@[A-Za-z0-9_\-\.]{2,}\.[A-Za-z0-9]{2,}(\.[A-Za-z0-9])?/);

            if (isNullOrEmpty(email)) {
                return false;
            } else if (er.test(email) == false) {
                return false;
            }
            return true;
        }
        
        function validarCampos(){

            if(isNullOrEmpty(Ext.getCmp("txtLGValor").getValue()) || isNullOrEmpty(Ext.getCmp("txtMSValor").getValue()) || isNullOrEmpty(Ext.getCmp("txtMNValor").getValue())){
                alerta("Favor preencher as Configurações de Tolerância");
                return false;
            }

            if(validateMaxValue(Ext.getCmp("txtLGValor").getValue(), 9999999.99) || validateMaxValue(Ext.getCmp("txtMSValor").getValue(), 9999999.99) || validateMaxValue(Ext.getCmp("txtMNValor").getValue(), 9999999.99)){
                alerta("Valor máximo para Configurações de Tolerância 9999999,99");
                return false;
            }
                
            if(!validarEmail(Ext.getCmp("txtLGEmail").getValue()) || !validarEmail(Ext.getCmp("txtMSEmail").getValue()) || !validarEmail(Ext.getCmp("txtMNEmail").getValue())){            
                alerta("Email inválido");
                return false;
            }
            
            return true;            
        }

        function salvarAlteracoes() 
        {                           
            var modifiedRecords = grid.getStore().getModifiedRecords();
            if (modifiedRecords.length > 0 || valoresAlterados) 
            {
                if (Ext.Msg.confirm("Manutenção de Vagões", "Confirma a alteração?",
                    function (btn, text) {
                        if (btn == 'yes') 
                        {
                            if(validarCampos()){
                                var listaRecomendacoes = new Array();
                                for (var i = 0; i < modifiedRecords.length; i++) 
                                {
                                    var fields = modifiedRecords[i];
                                    listaRecomendacoes.push
                                    (
                                        {
                                            Id: fields.get('Id'),
                                            IdCausaSeguro: fields.get('IdCausaSeguro'),
                                            Recomendacao: fields.get('Recomendacao'),
                                            Tolerancia: fields.get('Tolerancia')
                                        }
                                    );
                                }

                                
                                var listaTolerancias = new Array();
                                var codMalha = null;
                                
                                for (var i = 0; i < arrayMalhas.length; i++) 
                                {
                                    codMalha = arrayMalhas[i];

                                    listaTolerancias.push
                                    (
                                        {
                                            Id: Ext.getCmp("hdn" + codMalha + "Id").getValue(),
                                            CodMalha: codMalha,
                                            Valor: Ext.getCmp("txt" + codMalha +"Valor").getValue(),
                                            Email: Ext.getCmp("txt" + codMalha + "Email").getValue()
                                        }
                                    );
                                }


                                var recomendacoes = 
                                {
                                    Recomendacoes: listaRecomendacoes,
                                    Tolerancias: listaTolerancias
                                }

                                $.ajax
                                (
                                    {
                                        url: '<%= Url.Action("SalvarRecomendacoes","RecomendacaoVagao") %>',
                                        type: "POST",
                                        async: false,
                                        dataType: 'json',
                                        data: $.toJSON(recomendacoes),
                                        contentType: "application/json; charset=utf-8",
                                        success: function (response) 
                                        {
                                            alerta(response.message);
                                            
                                            if (response.success) {
                                                toleranciaStore.reload();
                                                grid.getStore().clearModified();
                                                grid.getStore().reload();                                                
                                            }
                                            
                                        },
                                        failure: function (result) 
                                        {
                                            //grid.getStore().clearModified();
                                            //grid.getStore().reload();
                                            alerta("Erro ao salvar alterações");
                                        }
                                    }
                                );
                            }
                        }
                    })
                );
            }
            else{
                alerta("Não existem alterações pendentes");
            }                     
        }

        function cancelarAlteracoes() 
        {
            var modifiedRecords = grid.getStore().getModifiedRecords();
            if (modifiedRecords.length > 0 || valoresAlterados) {
                if (Ext.Msg.confirm("Manutenção de Vagões", "Deseja cancelar as alterações?",
                    function (btn, text) {
                        if (btn == 'yes') {
                            toleranciaStore.reload();
                            grid.getStore().clearModified();
                            grid.getStore().reload();
                        }
                    })
                );
            }
            else {
                alerta("Não existem alterações pendentes");
            }            
        }


        
        function booleanRenderer(value, metaData) {
            //metaData.attr = 'ext:qtip="' + value + '"';
            //            return value;
            return value ? 'SIM' : 'NÃO';
        }

        function alerta(msg) {
            Ext.Msg.show({
                title: "Aviso",
                msg: msg,
                buttons: Ext.Msg.OK,
                icon: Ext.MessageBox.QUESTION,
                minWidth: 200
            });
        }


        //***** inicializa tela ******
        Ext.onReady(function () {
            painelTolerancia.render("divTolerancia");
            painelGrid.render("divGrid");
            grid.getStore().load();
        });


    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div id="header-content">
        <h1>
            Controle de Perdas</h1>
        <small>Você está em Cadastro > TFA > Recomendação de Vagões</small>
        <br />
    </div>
    <div id="divTolerancia">
    </div>
    <div id="divGrid">
    </div>
</asp:Content>
