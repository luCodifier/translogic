<%@ Page Title="" Language="C#" Inherits="System.Web.Mvc.ViewPage" MasterPageFile="~/Views/Shared/Interna.master" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server">
	<script type="text/javascript">

		var saldoTotalDisponivel;
		var arraySelectItens = new Array();

		/********************************************************************/
		/* Objeto:  JsonStore												*/
		/* Fun��o:  Respons�vel pela busca dos registros da grid.			*/
		/********************************************************************/
		var dsGrid = new Ext.data.JsonStore({
			root: "Items",			
			fields: [
                    'Bitola',
                    'IdTrem',
                    'IdComposicao',
                    'IdVagao',
                    'Contrato',
					'CargaBruta',
					'CodFluxoComercial',
					'Comprimento',
					'CondUso',
					'DescClasse',
					'Destino',
					'EmpresaCsg',
					'EmpresaCsgDesc',
					'EmpresaProprietaria',
					'EmpresaRemetente',
					'Frota',
					'IdDespacho',
					'IdFluxoComercial',
					'IndFreio',
					'Mercadoria',
					'MercDescricao',
					'Nota',
					'Veiculo',
					'Origem',
					'Pedido',
					'SequenciaVagao',
					'SerieVagao',
					'Contrato',
					'SaldoDisponivel',
					'Tu'
			    ]
		});

		 var dsSaldoBaseDisponivel = new Ext.data.ArrayStore({
                fields: ['contrato', 'saldoTotal', 'saldoDisponivel']				
            });

		/********************************************************************/
		/* Objeto:  Fields													*/
		/* Fun��o:  Campos de filtro da tela.								*/
		/********************************************************************/
		var OrdemServico = {
			xtype: 'textfield',
			fieldLabel: 'OS',
			id: 'filtro-OS',
			vtype: 'ctevagaovtype',
			name: 'OS',
			width: 83,
			allowBlank: false,
			hiddenName: 'OS'
		};

		var Prefixo = {
			xtype: 'textfield',
			fieldLabel: 'Prefixo',
			vtype: 'ctesdvtype',
			id: 'filtro-prefixo',
			style: 'text-transform: uppercase',
			autoCreate: { tag: 'input', type: 'text', maxlength: '3', autocomplete: 'off' },
			maxLength: 20,
			name: 'prefixo',
			width: 83,
			allowBlank: false,
			hiddenName: 'prefixo'
		};		

		/********************************************************************/
		/* Objeto:  FormLayout												*/
		/* Fun��o:  Respons�vel por montar o layout dos campos				*/
		/********************************************************************/
		var arrOS = {
			width: 87,
			layout: 'form',
			border: false,
			items: [OrdemServico]
		};

		var arrPrefixo = {
			width: 87,
			layout: 'form',
			border: false,
			items: [Prefixo]
		};

		selectModel = new Ext.grid.CheckboxSelectionModel({
		 listeners: {
                rowselect: function(f, rowIndex, r ) {
						/*var linha = arraySelectItens.indexOf(rowIndex);
						arraySelectItens.splice(linha,1);
						selectModel.selectRows(arraySelectItens, false);*/
						// alert(1);						
                },
				rowdeselect: function(f, rowIndex, record ){
						/*arraySelectItens.push(rowIndex);
						selectModel.selectRows(arraySelectItens, false);*/
				}
			}
		
		});

		/********************************************************************/
		/* Objeto:  Grid													*/
		/* Fun��o:  Respons�vel por exibir os registros.					*/
		/********************************************************************/
		grid = new Ext.grid.EditorGridPanel({
			autoLoadGrid: false,
			id: "gridRefaturamento",
			width: 450,
			heigth: 600,
			ds: dsGrid,
			region: 'center',
			loadMask: { msg: App.Resources.Web.Carregando },
			sm: selectModel,
			viewConfig: {
				forceFit: false
			},
			colModel: new Ext.grid.ColumnModel({
                defaults: {
                    sortable: false
                },			
				columns: [
				//new Ext.grid.RowNumberer(),
				selectModel,
				{ header: 'Seq', dataIndex: "SequenciaVagao", width: 40, sortable: false },
				{ header: 'Serie', dataIndex: "SerieVagao", width: 40, sortable: false },
				{ header: 'Bitola', dataIndex: "Bitola", width: 40, sortable: false },
				{ header: 'Veiculo', dataIndex: "Veiculo", width: 60, sortable: false },
				{ header: 'TU', dataIndex: "Tu", width: 50, sortable: false },
				{ header: 'TB', dataIndex: "CargaBruta", width: 50, sortable: false },
				{ header: 'Fluxo', dataIndex: "CodFluxoComercial", width: 60, sortable: false },
				{ header: 'Origem', dataIndex: "Origem", width: 70,sortable: false },			
				{ header: 'Destino', dataIndex: "Destino", width: 70, sortable: false },
				{ header: 'Remetente', dataIndex: "EmpresaRemetente", sortable: false },
				{ header: 'Destinat�rio', dataIndex: "EmpresaCsg", width: 70, sortable: false },
				{ header: 'Frota', dataIndex: "Frota", sortable: false },
				{ header: 'Mercadoria', dataIndex: "Mercadoria", width: 70, sortable: false },
				{ header: 'Descricao Mercadoria', dataIndex: "MercDescricao", width: 150, sortable: false }//,
				// { header: 'Contrato', dataIndex: "Contrato", sortable: false, css: 'border-style:inset;border-color:#0000ff;border-width: 1px;', editor: fctCreateContratoField() },
				// { header: 'Saldo', dataIndex: "SaldoDisponivel", width: 200, sortable: false }
				// { header: 'Pedido', dataIndex: "Pedido",width: 200,  sortable: false }		
			
			],
			isCellEditable: function(col, row) {
				if(arraySelectItens.indexOf(row) < 0)
				{
					arraySelectItens.push(row);
				}
				
				selectModel.selectRows(arraySelectItens, false);	
				return Ext.grid.ColumnModel.prototype.isCellEditable.call(this, col, row);
			}
		  }),
			listeners: {
				rowclick: function(g, rowIndex, e) {
						
					var linha = arraySelectItens.indexOf(rowIndex);
						
					if(linha >= 0)
					{		
						// Recupera o contrato e a Tu
						//var contrato	= grid.getView().getCell(rowIndex, 15).all[0].innerText;
						var tu			= grid.getView().getCell(rowIndex, 5).all[0].innerText;
						
						arraySelectItens.splice(linha,1);						
						selectModel.selectRows(arraySelectItens, false);												

						if(contrato.trim() != "")
						{						
							// libera o saldo
							//fctRemoveSaldoDisponivel(contrato.trim(), tu);
						
							// Atualiza a Grid
							//fctAtualizarGrid(contrato);
						}				
						
					}else{						
						arraySelectItens.push(rowIndex);
						selectModel.selectRows(arraySelectItens, false);
					}					
				}
			}
		});


		grid.on('afteredit', afterEdit, this );		

		/********************************************************************/
		/* Objeto:  columnLayout											*/
		/* Fun��o:  Respons�vel por organizar os campos em colunas			*/
		/********************************************************************/
		var arrlinha1 = {
			layout: 'column',
			border: false,
			items: [arrOS, arrPrefixo]
		};

		var arrCampos = new Array();
		arrCampos.push(arrlinha1);
		
		/********************************************************************/
		/* M�todo:  fctLimpar												*/
		/* Fun��o:  M�todo respons�vel por limpar os campos de Filtro		*/
		/********************************************************************/
		function fctLimpar() {
			arraySelectItens = new Array();
			dsSaldoBaseDisponivel.removeAll();
			grid.getStore().removeAll();
			Ext.getCmp("grid-filtros").getForm().reset();
		}		

		/********************************************************************************/
		/* M�todo:  RoundWithDecimals													*/
		/* Fun��o:  M�todo respons�vel por efetuar o arredondamento de decimais.     	*/
		/********************************************************************************/
		 function RoundWithDecimals(val, dec){
            var decimals = Math.pow(10,dec);
            var result = Math.round(val*decimals)/decimals;
	        return result;
        }
		
		/********************************************************************************/
		/* M�todo:  fctCreateContratoField												*/
		/* Fun��o:  M�todo respons�vel por criar o objeto contarto utilizado na grid.	*/
		/********************************************************************************/
		function fctCreateContratoField() {
			var contrato = new Ext.form.TextField({						
						// vtype: 'ctevagaovtype',
						enableKeyEvents : true,
						autoCreate: { tag: 'input', type: 'text', maxlength: '10', autocomplete: 'off' },
						allowBlank: true
					});
					

			return contrato;
		}

		function afterEdit(e) {
			//fctValidarSaldo(e);
			e.record.commit();
		};

		function fctValidarSaldo(e)
		{
			var contrato	=	e.record.data.Contrato;
			var tu			=	e.record.data.Tu;
			var index		=	dsSaldoBaseDisponivel.find("contrato",contrato);

			if(index < 0)
			{				
				// Se n�o possuir o contrato ent�o busca o saldo na base
				fctObterSaldoBase(contrato, tu, e);
			}else{
				if(contrato.trim() != "")
				{
					if(!fctAddSaldoDisponivel(contrato,tu))
					{
						e.record.reject();
						LimparCampoSaldoContrato(e.record, e.row);
					}else{
						fctAtualizarGrid(contrato);
					}
				}
			}
		}
		
		function fctAtualizarGrid(contrato)
		{
			dsGrid.filterBy(function (record){
				var ok = true;
				if (contrato != ""){
					if (record.data.Contrato != contrato){
						ok = false;
					}
				}               
				return ok;
			});

			var index		=	dsSaldoBaseDisponivel.find("contrato",contrato);
			var saldoAtualizado = dsSaldoBaseDisponivel.getAt(index).data.saldoDisponivel;
			var i=0;

			dsGrid.each(
			    function (record) {				
					if(selectModel.isSelected(i))
                    {
						var objVagao = record.data;
						objVagao.SaldoDisponivel = RoundWithDecimals(parseFloat(saldoAtualizado), 3);
						grid.getView().getCell(i, 16).all[0].innerText = RoundWithDecimals(parseFloat(saldoAtualizado), 3);        
						
					}else{
						LimparCampoSaldoContrato(record, i);
					}
					i++;
			    }
		    );

			dsGrid.clearFilter();
			
			selectModel.selectRows(arraySelectItens, false);
		}

		function LimparCampoSaldoContrato(record, index)
		{
			record.data.Contrato = "";
			record.data.SaldoDisponivel = "";
			grid.getView().getCell(index, 15).all[0].innerText = "";
			grid.getView().getCell(index, 16).all[0].innerText = "";
		}

		function fctRemoveSaldoDisponivel(contrato, tu)
		{
			if(contrato.trim() != "")
			{				
				var index =	dsSaldoBaseDisponivel.find("contrato",contrato);
				var saldoRestante = dsSaldoBaseDisponivel.getAt(index).data.saldoDisponivel;
				dsSaldoBaseDisponivel.getAt(index).data.saldoDisponivel= (parseFloat(saldoRestante) + parseFloat(tu));
			}
		}

		function fctAddSaldoDisponivel(contrato, tu)
		{		
			if(contrato.trim() != "")
			{
				var index =	dsSaldoBaseDisponivel.find("contrato",contrato);
				var saldoRestante = dsSaldoBaseDisponivel.getAt(index).data.saldoDisponivel;
				
				if(tu > saldoRestante)
				{
					fctExibirConsistencia('1',"Saldo indispon�vel!");
					return false;
				}else{
					dsSaldoBaseDisponivel.getAt(index).data.saldoDisponivel= (saldoRestante - tu);
					return true;
				}
			}
		}

		/************************************************************************/
		/* M�todo:  Contains													*/
		/* Fun��o:  M�todo respons�vel verificar se contem o item  na lista.  	*/
		/************************************************************************/
		Contains = function(arr, obj) {
						for(var i = 0; i < arr.length; i++) {
							if(arr[i] === obj) {
								return true;
							}
						}
						return false;
					}

		/************************************************************************/
		/* M�todo:  fctObterSaldoContratoDisponivel								*/
		/* Fun��o:  M�todo respons�vel por obter os daedos na base de dados   	*/
		/************************************************************************/
		function fctObterSaldoBase(contrato, tu, e){
			$.ajax({
				url: "<%= Url.Action("ObterSaldoContratoDisponivel") %>",
				type: "POST",
				dataType: 'json',
				data: ($.toJSON({	'contrato': contrato	})),				
                timeout: 600000,
				contentType: "application/json; charset=utf-8",
				beforeSend: function() { Ext.getBody().mask(App.Resources.Web.Carregando, "x-mask-loading"); },
				success: function(result) {
                    Ext.getBody().unmask();
					
					if(result.success)
					{						
						dsSaldoBaseDisponivel.add(new dsSaldoBaseDisponivel.recordType({contrato: contrato, saldoTotal:result.saldoDisponivel, saldoDisponivel: result.saldoDisponivel}));
						
						if(!fctAddSaldoDisponivel(contrato,tu))
						{
							e.record.reject();
							LimparCampoSaldoContrato(e.record, e.row);
						}else{
							//fctAtualizarGrid(contrato);
						}						

					}else{
						fctExibirConsistencia("99", result.Message);
					}					  
				},
                error : function (result){				
					fctExibirConsistencia("99","Ocorreu um erro inesperado.<br/>" + result.Message);
                    Ext.getBody().unmask();
                }
			});        
		}
				
	
		/********************************************************************/
		/* M�todo:  fctPesquisar										    */
		/* Fun��o:  M�todo respons�vel por carregar a grid.   				*/
		/********************************************************************/
		function fctPesquisar() {			
			arraySelectItens = new Array();
			dsSaldoBaseDisponivel.removeAll();

			if (Ext.getCmp("filtro-prefixo").getValue() == '' && Ext.getCmp("filtro-OS").getValue() == '') {

				Ext.Msg.show({
					title: "Mensagem de Informa��o",
					msg: "Favor informar o Prefixo do Trem ou N�mero da OS.",
					buttons: Ext.Msg.OK,
					icon: Ext.MessageBox.INFO,
					minWidth: 220
				});
				
				return;
			}
			
			fctEfetuarConsulta();			
		}


		/********************************************************************/
		/* M�todo:  fctEfetuarConsulta										*/
		/* Fun��o:  M�todo respons�vel por buscar os dados na base.			*/
		/********************************************************************/
		function fctEfetuarConsulta() {
		
			Ext.getBody().mask(App.Resources.Web.Carregando, "x-mask-loading");

                  Ext.Ajax.request({    
                        url: "<% =Url.Action("ObterRefaturamentos") %>",
                        method: "POST",                        
			            params: { 
								prefixo: Ext.getCmp("filtro-prefixo").getValue(),
								ordemServico: Ext.getCmp("filtro-OS").getValue()
							 },
			            success: function(response) {                             
                             
							 var result = Ext.util.JSON.decode(response.responseText);
							 							 
							 fctExibirConsistencia(result.codigoRetorno, result.mensagem);
							
							dsGrid.loadData(result);                              
							Ext.getBody().unmask();
			            }/*,
                        failure: function(conn, data){
							fctExibirConsistencia("99","Erro ao efetuar a pesquisa!");
							Ext.getBody().unmask();
                        }*/			            
		            });		

		}
		
		/********************************************************************/
		/* M�todo:  fctExibirConsistencia									*/
		/* Fun��o:  M�todo respons�vel por exibir a							*/
        /*			mensagem de consist�ncia(Erro,INFO)						*/
		/* Params:  codRet:   C�digo de retorno								*/
		/*          mensagem: Mensagem de retorno							*/
		/********************************************************************/
		function fctExibirConsistencia(codRet, mensagem) {

			if(codRet != ''){
				if (parseInt(codRet) == 0 || parseInt(codRet) == 1) {

					Ext.Msg.show({
						title: "Mensagem de Informa��o",
						msg: mensagem,
						buttons: Ext.Msg.OK,
						icon: Ext.MessageBox.INFO,
						minWidth: 220
					});

				} else {

					Ext.Msg.show({
						title: "Mensagem de Erro",
						msg: mensagem,
						buttons: Ext.Msg.OK,
						icon: Ext.MessageBox.ERROR,
						minWidth: 220
					});

				}
			}
		}			

		/********************************************************************/
		/* M�todo:  fctRefaturar											*/
		/* Fun��o:  M�todo respons�vel por fazer o refaturamento do vag�o	*/
		/********************************************************************/
		function fctRefaturar(){
			var listaVagao = new Array();
			var idTrem;
			var idComposicao;
			
			var mensagemErro = "";
			var recordList = selectModel.getSelections();			
						
			for (var i = 0; i < recordList.length; i++) {
            
                    var item	 =	recordList[i].data;
					idTrem		 =	item.IdTrem;
					idComposicao =	item.IdComposicao;
//                  var Contrato =	item.Contrato;
					
//					if(Contrato.trim() != "")
//					{
						listaVagao.push( 
						{
							 IdVagao:	 item.IdVagao,
							 IdDespacho: item.IdDespacho //,
							 //Contrato:	 Contrato
						});
//					}else{
//						mensagemErro += item.Veiculo;
//						
//						if(i != recordList.length -1)
//						{
//							mensagemErro += ", ";
//						}
//					}
                }			
			
			if(mensagemErro != "")
			{
				fctExibirConsistencia("1","Os seguintes registros n�o est�o com o contrato preenchido: <br/>" + mensagemErro);
				return;
			}

			 var listaVagoes = {
                IdTrem: idTrem,
				IdComposicao: idComposicao,
				ListaVagao: listaVagao				
            };

			var listaVagoesDto = $.toJSON(listaVagoes);

			$.ajax({
				url: "<%= Url.Action("SalvarRefaturamentos") %>",
				type: "POST",
				dataType: 'json',
				data: listaVagoesDto,
                timeout: 600000,
				contentType: "application/json; charset=utf-8",
				beforeSend: function() { Ext.getBody().mask("Refaturamento em andamento", "x-mask-loading"); },
				success: function(result) {
                    Ext.getBody().unmask();
					if(result.success)
					{
							fctExibirConsistencia("0","Registros processados com sucesso.");
					}else{
							fctExibirConsistencia("99", result.Message);
					}		
				    
				    fctPesquisar();
				},
                error : function (result){
					fctExibirConsistencia("99","Ocorreu um erro inesperado.<br/>" + result.Message);
                    Ext.getBody().unmask();
                }
			}); 
		}	

		/********************************************************************/
		/* Objeto:  FormPanel												*/
		/* Fun��o:  Respons�vel por organizar os campos de filtros			*/
		/*			e bot�es de a��es.										*/
		/********************************************************************/
		filters = new Ext.form.FormPanel({
			id: 'grid-filtros',
			title: "Filtros",
			region: 'center',
			bodyStyle: 'padding: 15px',
			labelAlign: 'top',
			items: [arrCampos],
			buttonAlign: "center",
			buttons:
	        [
                 {
                 	text: 'Pesquisar',
                 	type: 'submit',
                 	iconCls: 'icon-find',
                 	handler: function (b, e) {
                 		fctPesquisar();
                 	}
                 },{
                 	text: 'Salvar',
                 	type: 'submit',
                 	iconCls: 'icon-save',
                 	handler: function (b, e) {
                 		fctRefaturar();
                 	}
                 },
				 {
				 	text: 'Limpar',
				 	handler: function (b, e) {
				 		fctLimpar();
				 	}
				 }
            ]
		});


		/********************************************************************/
		/* Evento:  Ready													*/
		/* Fun��o:  Respons�vel por renderizar os campos na tela.			*/
		/********************************************************************/
		$(function () {			
			new Ext.Viewport({
				layout: 'border',
				items: [
				{
					region: 'north',
					height: 200,
					items: [{
						region: 'center',
						applyTo: 'header-content'
					}, filters]
				}, grid]
			});
		});

	</script>
</asp:Content>
<asp:Content ID="MainContent" ContentPlaceHolderID="Content" runat="server">
	<div id="header-content">
		<h1>
			Refaturamento</h1>
		<small>Voc� est� em CTe > Refaturamento</small>
		<br />
	</div>
	<div id="divFiltros">
	</div>
</asp:Content>
