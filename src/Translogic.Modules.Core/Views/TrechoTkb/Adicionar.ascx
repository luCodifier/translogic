<%@ Import Namespace="Translogic.Modules.Core.Domain.Model"%>
<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div id="div-form-adicionar"></div>
<script type="text/javascript">

    var storeComboUpAdd = new Ext.data.JsonStore({
        id: 'storeComboUpAdd',
        name: 'storeComboUpAdd',
        url:'<%=Url.Action("ObterUps") %>',
        fields: [
            'id',
            'descricao'
        ]
    });

    var painelAdicionar = new Ext.form.FormPanel({
        id: 'painelAdicionar',
        name: 'painelAdicionar',
        title: '',
        bodyStyle: 'padding:15px',
        layout: 'column',
        items: [{
            bodyStyle: 'border:0px',
            layout: 'form',
            items: [{
                xtype: 'textfield',
                id: 'txtTrechoTkbAdd',
                name: 'txtTrechoTkbAdd',
                allowBlank:false,
                fieldLabel: 'Trecho TKB',
                style: { textTransform: 'uppercase' },
                autoCreate: { tag: 'input', type: 'text', maxlength: '7', autocomplete: 'off' },
                maskRe: /^[0-9a-zA-Z]+$/
            }, {
                xtype: 'textfield',
                id: 'txtTrechoDieselAdd',
                allowBlank: false,
                name: 'txtTrechoDieselAdd',
                fieldLabel: 'Trecho Diesel',
                style: { textTransform: 'uppercase' },
                autoCreate: { tag: 'input', type: 'text', maxlength: '7', autocomplete: 'off' },
                maskRe: /^[0-9a-zA-Z]+$/
            }, {
                xtype: 'textfield',
                id: 'txtSentidoAdd',
                allowBlank: false,
                name: 'txtSentidoAdd',
                fieldLabel: 'Sentido',
                style: { textTransform: 'uppercase' },
                autoCreate: { tag: 'input', type: 'text', maxlength: '10', autocomplete: 'off' },
                maskRe: /^[0-9a-zA-Z]+$/
            }, {
                xtype: 'textfield',
                id: 'txtCorredorAdd',
                allowBlank: false,
                name: 'txtCorredorAdd',
                fieldLabel: 'Corredor',
                style: { textTransform: 'uppercase' },
                autoCreate: { tag: 'input', type: 'text', maxlength: '20', autocomplete: 'off' },
                maskRe: /^[0-9a-zA-Z]+$/
            }, {
                xtype: 'textfield',
                id: 'txtUpAdd',
                allowBlank: false,
                name: 'txtUpAdd',
                fieldLabel: 'UP',
                style: { textTransform: 'uppercase' },
                autoCreate: { tag: 'input', type: 'text', maxlength: '20', autocomplete: 'off' },
                maskRe: /^[0-9a-zA-Z]+$/
            }, {
                xtype: 'combo',
                id: 'comboUpAdd',
                name: 'comboUpAdd',
                forceSelection:true,
                store: storeComboUpAdd,
                triggerAction: 'all',
                mode: 'remote',
                displayField: 'descricao',
                valueField: 'id',
                fieldLabel: 'Up Resumida'
            }]
        }],
        buttonAlign: 'center',
        buttons: [{
            id: 'btnAdd',
            name: 'btnAdd',
            text: 'Adicionar',
            iconCls: 'icon-new',
            handler: function() {
                adicionar();
            }
        },{
            id: 'btnFecharJanelaAdd',
            name: 'btnFecharJanelaAdd',
            text: 'Fechar',
            handler: function() {
                Ext.getCmp('janelaNovo').close();
            }
        }]
    });


    function adicionar() {
        Ext.Ajax.request({
            url: '<%= Url.Action("AdicionarRegistro") %>',
            params: {
                'trechoTkb': Ext.getCmp('txtTrechoTkbAdd').getValue(),
                'trechoDiesel': Ext.getCmp('txtTrechoDieselAdd').getValue(),
                'corredor': Ext.getCmp('txtCorredorAdd').getValue(),
                'up': Ext.getCmp('txtUpAdd').getValue(),
                'sentido': Ext.getCmp('txtSentidoAdd').getValue(),
                'idUp': Ext.getCmp('comboUpAdd').getValue()
            },
            success: function(result, success) {

                if (success) {
                    Ext.Msg.show({
                        title: 'Sucesso!',
                        msg: 'Registro adicionado com sucesso.',
                        buttons: Ext.Msg.OK,
                        icon: Ext.Msg.INFO
                    });
                    Ext.getCmp('janelaNovo').close();
                    Ext.getCmp('gridTrecho').getStore().load({
                        params: {
                            'trechoTkb': Ext.getCmp('txtTrechoTkb').getValue(),
                            'trechoDiesel': Ext.getCmp('txtTrechoDiesel').getValue(),
                            'corredor': Ext.getCmp('txtCorredor').getValue(),
                            'up': Ext.getCmp('txtUp').getValue()
                        }
                    });
                } else {
                    Ext.Msg.show({
                        title: 'Erro!',
                        msg: 'Erro n�o esperado!',
                        buttons: Ext.Msg.OK,
                        icon: Ext.Msg.ERROR
                    });
                }
            }
        });
    }
    
    Ext.onReady(function() {
        painelAdicionar.render("div-form-adicionar");
    });
</script>