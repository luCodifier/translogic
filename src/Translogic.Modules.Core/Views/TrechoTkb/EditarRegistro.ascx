<%@ Import Namespace="Translogic.Modules.Core.Domain.Model"%>
<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div id="div-form-editar"></div>
<script type="text/javascript">
    var idUp = null;

    var storeComboUpEdit = new Ext.data.JsonStore({
        id: 'storeComboUpEdit',
        name: 'storeComboUpEdit',
        url: '<%=Url.Action("ObterUps") %>',
        fields: [
            'id',
            'descricao'
        ],
        listeners: {
            load: function() {
                Ext.getCmp('comboUpEdit').setValue(idUp);
            }
        }
    });


    var painelEditar = new Ext.form.FormPanel({
        id: 'painelEditar',
        name: 'painelEditar',
        title: '',
        bodyStyle: 'padding:15px',
        layout: 'column',
        items: [{
            bodyStyle: 'border:0px',
            layout: 'form',
            items: [{
                xtype: 'textfield',
                id: 'txtTrechoTkbEdit',
                allowBlank: false,
                name: 'txtTrechoTkbEdit',
                fieldLabel: 'Trecho TKB',
                style: { textTransform: 'uppercase' },
                autoCreate: { tag: 'input', type: 'text', maxlength: '7', autocomplete: 'off' },
                maskRe: /^[0-9a-zA-Z]+$/
            }, {
                xtype: 'textfield',
                allowBlank: false,
                id: 'txtTrechoDieselEdit',
                name: 'txtTrechoDieselEdit',
                fieldLabel: 'Trecho Diesel',
                style: { textTransform: 'uppercase' },
                autoCreate: { tag: 'input', type: 'text', maxlength: '7', autocomplete: 'off' },
                maskRe: /^[0-9a-zA-Z]+$/
            }, {
                xtype: 'textfield',
                allowBlank: false,
                id: 'txtSentidoEdit',
                name: 'txtSentidoEdit',
                fieldLabel: 'Sentido',
                style: { textTransform: 'uppercase' },
                autoCreate: { tag: 'input', type: 'text', maxlength: '10', autocomplete: 'off' },
                maskRe: /^[0-9a-zA-Z]+$/
            }, {
                xtype: 'textfield',
                allowBlank: false,
                id: 'txtCorredorEdit',
                name: 'txtCorredorEdit',
                fieldLabel: 'Corredor',
                style: { textTransform: 'uppercase' },
                autoCreate: { tag: 'input', type: 'text', maxlength: '20', autocomplete: 'off' },
                maskRe: /^[0-9a-zA-Z]+$/
            }, {
                xtype: 'textfield',
                allowBlank: false,
                id: 'txtUpEdit',
                name: 'txtUpEdit',
                fieldLabel: 'UP',
                style: { textTransform: 'uppercase' },
                autoCreate: { tag: 'input', type: 'text', maxlength: '20', autocomplete: 'off' },
                maskRe: /^[0-9a-zA-Z]+$/
            }, {
                xtype: 'combo',
                id: 'comboUpEdit',
                name: 'comboUpEdit',
                forceSelection:true,
                store: storeComboUpEdit,
                triggerAction: 'all',
                mode: 'remote',
                displayField: 'descricao',
                emptyText:'N�o Informado',
                valueField: 'id',
                fieldLabel: 'Up Resumida',
                view: new Ext.DataView({
                                applyTo: this.innerList,
                                tpl: this.tpl,
                                singleSelect: true,
                                selectedClass: this.selectedClass,
                                itemSelector: this.itemSelector || '-item',
                                emptyText: this.listEmptyText,
                                deferEmptyText: false
                                })
                }]
            }],
                buttonAlign: 'center',
                buttons: [{
                    id: 'btnEdit',
                    name: 'btnEdit',
                    text: 'Salvar',
                    iconCls: 'icon-accept',
                    handler: function() {
                        salvar();
                    }
                }, {
                    id: 'btnFecharJanelaEdit',
                    name: 'btnFecharJanelaEdit',
                    text: 'Fechar',
                    handler: function() {
                        Ext.getCmp('janelaEditarRegistro').close();
                    }
}]
                });


                Ext.onReady(function() {
                    idUp = '<%=ViewData["idUp"]%>'
                    painelEditar.render("div-form-editar");
                    Ext.getCmp('txtTrechoTkbEdit').setValue('<%=ViewData["trechoTkb"]%>');
                    Ext.getCmp('txtTrechoDieselEdit').setValue('<%=ViewData["trechoDiesel"]%>');
                    Ext.getCmp('txtCorredorEdit').setValue('<%=ViewData["up"] %>');
                    Ext.getCmp('txtSentidoEdit').setValue('<%=ViewData["sentido"] %>');
                    Ext.getCmp('txtUpEdit').setValue('<%=ViewData["corredor"]%>');
                    storeComboUpEdit.load({});
                });

                function salvar() {
                    if (painelEditar.form.isValid()) {
                        Ext.Ajax.request({
                            url: '<%=Url.Action("AlterarRegistro") %>',
                            params: {
                                'id': '<%=ViewData["id"] %>',
                                'trechoTkb': Ext.getCmp('txtTrechoTkbEdit').getValue(),
                                'trechoDiesel': Ext.getCmp('txtTrechoDieselEdit').getValue(),
                                'sentido': Ext.getCmp('txtSentidoEdit').getValue(),
                                'corredor': Ext.getCmp('txtCorredorEdit').getValue(),
                                'up': Ext.getCmp('txtUpEdit').getValue(),
                                'idUp': Ext.getCmp('comboUpEdit').getValue()
                            },
                            success: function(result, success) {
                                if (success) {
                                    Ext.Msg.show({
                                        title: 'Sucesso!',
                                        msg: 'Registro salvo com sucesso.',
                                        buttons: Ext.Msg.OK,
                                        icon: Ext.Msg.INFO
                                    });
                                    Ext.getCmp('janelaEditarRegistro').close();
                                    Ext.getCmp('gridTrecho').getStore().load({
                                        params: {
                                            'trechoTkb': Ext.getCmp('txtTrechoTkb').getValue(),
                                            'trechoDiesel': Ext.getCmp('txtTrechoDiesel').getValue(),
                                            'corredor': Ext.getCmp('txtCorredor').getValue(),
                                            'up': Ext.getCmp('txtUp').getValue()
                                        }
                                    });
                                } else {
                                    Ext.Msg.show({
                                        title: 'Erro!',
                                        msg: 'Erro n�o esperado!',
                                        buttons: Ext.Msg.OK,
                                        icon: Ext.Msg.ERROR
                                    });
                                }
                            }
                        });
                    }
                }
</script>
