<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Interna.Master" Inherits="System.Web.Mvc.ViewPage" %>
<asp:Content runat="server" ID="HeadContent" ContentPlaceHolderID="HeadContent">
<script type="text/javascript">

    var storeComboUp = new Ext.data.JsonStore({
        id:'storeComboUp',
        name:'storeComboUp',
        url: '<%=Url.Action("ObterUps") %>',
        fields: [
            'id',
            'descricao'
        ]    
    });

    function formAdicionar() {
        var janelaNovo = null;
        janelaNovo = new Ext.Window({
            id:'janelaNovo',
            name:'janelaNovo',
            title:'Adicionar',
            modal:true,
            height:255,
            width:400,
            autoLoad:{
                url:'<%=Url.Action("Adicionar") %>',
                scripts:true
            }
        });
        janelaNovo.show();
    }


    var editarGridTrecho = new Ext.ux.grid.RowActions({
        id: 'detalhesGridTrecho',
        name: 'detalhesGridTrecho',
        align: 'center',
        actions: [{
            iconCls: 'icon-edit',
            tooltip: 'Editar Registro'
}],
            callbacks: {
                'icon-edit': function(grid, rec, action, row) {
                    editarRegistro(row);
                }
            }
        });

        function editarRegistro(row) {
            var janelaEditarRegistro = null;
            janelaEditarRegistro = new Ext.Window({
                id: 'janelaEditarRegistro',
                name: 'janelaEditarRegistro',
                title: 'Editar Registro',
                modal: true,
                height: 255,
                width: 400,
                autoLoad: {
                    url: '<%=Url.Action("EditarRegistro") %>',
                    scripts: true,
                    params: {
                        'id': storeGridTrecho.getAt(row).get('id'),
                        'trechoTkb': storeGridTrecho.getAt(row).get('trechoTkb'),
                        'trechoDiesel': storeGridTrecho.getAt(row).get('trechoDiesel'),
                        'sentido': storeGridTrecho.getAt(row).get('sentido'),
                        'corredor': storeGridTrecho.getAt(row).get('corredor'),
                        'up': storeGridTrecho.getAt(row).get('up'),
                        'idUp': storeGridTrecho.getAt(row).get('idUp')
                    }
                }
            });
            janelaEditarRegistro.show();
        }

        var excluirGridTrecho = new Ext.ux.grid.RowActions({
            id: 'excluirGridTrecho',
            name: 'excluirGridTrecho',
            align: 'center',
            actions: [{
                iconCls: 'icon-del',
                tooltip: 'Excluir Registro'
}],
                callbacks: {
                    'icon-del': function(grid, rec, action, row) {
                        Ext.Msg.confirm('Exclus�o...', 'Deseja excluir o registro?', function(botao, texto) {
                            if (botao == "yes") {
                                Ext.Ajax.request({
                                    url: '<%=Url.Action("ExcluirRegistro") %>',
                                    params: {
                                        'id': storeGridTrecho.getAt(row).get('id')
                                    },
                                    success: function(result, success) {
                                        if (success) {
                                            Ext.Msg.show({
                                                title: 'Sucesso!',
                                                msg: 'Registro exclu�do com sucesso.',
                                                buttons: Ext.Msg.OK,
                                                icon: Ext.Msg.INFO
                                            });
                                            Ext.getCmp('gridTrecho').getStore().load({
                                                params: {
                                                    'trechoTkb': Ext.getCmp('txtTrechoTkb').getValue(),
                                                    'trechoDiesel': Ext.getCmp('txtTrechoDiesel').getValue(),
                                                    'corredor': Ext.getCmp('txtCorredor').getValue(),
                                                    'up': Ext.getCmp('txtUp').getValue(),
                                                    'idUp': Ext.getCmp('comboUp').getValue()
                                                }
                                            });
                                        } else {
                                            Ext.Msg.show({
                                                title: 'Erro!',
                                                msg: 'Erro n�o esperado!',
                                                buttons: Ext.Msg.OK,
                                                icon: Ext.Msg.ERROR
                                            });
                                        }
                                    }
                                });
                            }
                        });
                    }
                }
            });

    var storeGridTrecho = new Ext.data.JsonStore({
        id:'storeGridTrecho',
        name: 'storeGridTrecho',
        url: '<%=Url.Action("PesquisarTrechos") %>',
        fields: [
            'id',
            'trechoTkb',
            'trechoDiesel',
            'corredor',
            'sentido',
            'up',
            'idUp',
            'unidadeProducao'
        ]
    });

    var gridTrecho = new Ext.grid.GridPanel({
        id: 'gridTrecho',
        name: 'gridTrecho',
        title: '',
        loadMask: true,
        loadText:'Carregadando...',
        height: 400,
        plugins: [editarGridTrecho,excluirGridTrecho],
        viewConfig: { forceFit: true },
        columns: [
            new Ext.grid.RowNumberer({}),
            editarGridTrecho,
            excluirGridTrecho,
            { header: 'Trecho TKB', dataIndex: 'trechoTkb', sortable: true },
            { header: 'Trecho Diesel', dataIndex: 'trechoDiesel', sortable: true },
            { header: 'Corredor', dataIndex: 'corredor', sortable: true },
            { header: 'Sentido', dataIndex: 'sentido', sortable: true },
            { header: 'Up (Apelido)', dataIndex: 'up', sortable: true },
            { header: 'Up', dataIndex: 'unidadeProducao', sortable: true }
        ],
        store: storeGridTrecho,
        tbar: [{
            xtype: 'button',
            id: 'btnNovo',
            name: 'btnNovo',
            text: 'Adicionar',
            iconCls: 'icon-new',
            handler: function() {
                formAdicionar();
            }
}]
        });

        gridTrecho.on('celldblclick', function(grid,row) {
            editarRegistro(row);
        });

    var painelTrecho = new Ext.form.FormPanel({
        labelAlign: 'top',
        id: 'painelTrecho',
        name: 'painelTrecho',
        title: 'Trecho TKB Diesel',
        bodyStyle: 'padding:15px',
        layout: 'column',
        items: [{
            bodyStyle: 'border:0px',
            layout: 'form',
            items: [{
                xtype: 'textfield',
                id: 'txtTrechoTkb',
                name: 'txtTrechoTkb',
                fieldLabel: 'Trecho TKB'
            }, {
                xtype: 'textfield',
                id: 'txtTrechoDiesel',
                name: 'txtTrechoDiesel',
                fieldLabel: 'Trecho Diesel'
            }]
        }, {
            bodyStyle:'border:0px;margin:0px,0px,0px,10px',
            layout:'form',
            items: [{
                xtype: 'textfield',
                id: 'txtCorredor',
                name: 'txtCorredor',
                fieldLabel: 'Corredor'
            }, {
                xtype: 'textfield',
                id: 'txtUp',
                name: 'txtUp',
                fieldLabel: 'UP (Apelido)'
            }]
        }, {
            layout:'form',
            bodyStyle:'border:0px;margin:0px,0px,0px,10px',
            items: [{
                xtype:'combo',
                id:'comboUp',
                name: 'comboUp',
                store:storeComboUp,
                triggerAction:'all',
                mode:'remote',
                displayField: 'descricao',
                valueField:'id',
                fieldLabel:'Unidade Produ��o'
            }]
        }],
        buttonAlign:'center',
        buttons: [{
            id:'btnPesquisar',
            name:'btnPesquisar',
            text:'Pesquisar',
            iconCls:'icon-find',
            handler: function() {
                storeGridTrecho.load({
                params: {
                        'trechoTkb':Ext.getCmp('txtTrechoTkb').getValue(),
                        'trechoDiesel': Ext.getCmp('txtTrechoDiesel').getValue(),
                        'corredor':Ext.getCmp('txtCorredor').getValue(),
                        'up':Ext.getCmp('txtUp').getValue(),
                        'idUp': Ext.getCmp('comboUp').getValue()
                    }
                });
            }
        }]
    });


    Ext.onReady(function() {
        painelTrecho.render(document.body);
        gridTrecho.render(document.body);
    });
</script>
</asp:Content>
<asp:Content runat="server" ID="Content2" ContentPlaceHolderID="Content">
	<div id="header-content">
		<h1>Tela de --</h1>
		<small>Voc� est� em -- > ---</small>
	</div>
</asp:Content>