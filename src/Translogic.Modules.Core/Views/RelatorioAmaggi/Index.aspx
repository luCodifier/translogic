﻿<%@ Page Title="" Language="C#" Inherits="System.Web.Mvc.ViewPage" MasterPageFile="~/Views/Shared/Interna.master" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server">

<script type="text/javascript">
    // **************************** CAMPOS ******************************** //
	var txtFiltroDataInicial = {
	    xtype: 'datefield',
	    fieldLabel: 'Data Inicial',
	    id: 'txtFiltroDataInicial',
	    name: 'txtFiltroDataInicial',
	    width: 100,
	    allowBlank: false,
	    vtype: 'daterange',
	    endDateField: 'txtFiltroDataFinal',
	    value: new Date(),
	    enableKeyEvents: true
	};

	var txtFiltroDataFinal = {
	    xtype: 'datefield',
	    fieldLabel: 'Data Final',
	    id: 'txtFiltroDataFinal',
	    name: 'txtFiltroDataFinal',
	    width: 100,
	    allowBlank: false,
	    vtype: 'daterange',
	    startDateField: 'txtFiltroDataInicial',
	    value: new Date(),
	    enableKeyEvents: true
	};

	var txtFiltroEstacao = {
	    xtype: 'textfield',
	    width: 100,
	    name: 'txtFiltroEstacao',
	    allowBlank: false,
	    id: 'txtFiltroEstacao',
	    fieldLabel: 'Estação',
	    enableKeyEvents: true,
	    style: { textTransform: "uppercase" },
	    listeners:
        {
            change: Upper
        }
	};

	var txtFiltroEmail = {
	    xtype: 'textfield',
	    width: 300,
	    name: 'txtFiltroEmail',
	    allowBlank: false,
	    id: 'txtFiltroEmail',
	    fieldLabel: 'E-mail',
	    enableKeyEvents: true
	};

	var btnGerar = {
	    name: 'btnGerar',
	    id: 'btnGerar',
	    text: 'Gerar',
	    handler: Gerar
	};

	var btnLimpar = {
	    name: 'btnLimpar',
	    id: 'btnLimpar',
	    text: 'Limpar',
	    handler: Limpar
	};
	    
    //******************************** FUNCOES **********************************//
	function Limpar() {
	    Ext.getCmp("filtros").getForm().reset();
	}

	function Gerar() {
	    if (!filtros.form.isValid()) {
	        Ext.Msg.show({ title: 'Aviso', msg: 'Por favor preencha os campo obrigátorios!', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
	        return;
	    }

	    Ext.Ajax.request(
	    {
	        url: '<%= Url.Action("Gerar") %>',
	        params:
			{
			    dataInicial: Ext.getCmp('txtFiltroDataInicial').getValue(),
			    dataFinal: Ext.getCmp('txtFiltroDataFinal').getValue(),
			    estacoes: Ext.getCmp('txtFiltroEstacao').getValue(),
			    emails: Ext.getCmp('txtFiltroEmail').getValue()
			},
			timeout: 3000000,
	        success: function (response) {
	            var result = Ext.decode(response.responseText);

	            if (result.success) {
	                Ext.Msg.show({ title: 'Aviso', msg: result.Message, buttons: Ext.Msg.OK, icon: Ext.MessageBox.INFO });
	                Limpar();
	            }
	            else {
	                Ext.Msg.show({ title: 'Ops...', msg: result.Message, buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
	            }
	        }
	    });
	}

	function Upper(field, newValue, oldValue) {
	    field.setValue(newValue.toUpperCase());
	}
	    
    //******************************** PANEL **********************************//
    var filtros = new Ext.form.FormPanel({
        id: 'filtros',
        region: 'center',
        bodyStyle: 'padding: 5px',
        width: 1020,
        items: [txtFiltroEmail, txtFiltroDataInicial, txtFiltroDataFinal, txtFiltroEstacao],
        buttonAlign: "center",
        buttons: [btnGerar, btnLimpar]
    });

	//***************************** RENDER *****************************//
	Ext.onReady(function () {
	    filtros.render("divContent");
	});
		
</script>

</asp:Content>
<asp:Content ID="MainContent" ContentPlaceHolderID="Content" runat="server">
	<div id="header-content">
		<h1>Gerar Arquivo Amaggi</h1>
		<small>Você está em CTe > Gerar Arquivo Amaggi</small>
		<br />
	</div>
	<div id="divContent">
	</div>
</asp:Content>
