﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Interna.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content runat="server" ID="HeadContent" ContentPlaceHolderID="HeadContent">
    <% this.Html.RenderPartial("CadastrarLimiteMinimo"); %>
    <script type="text/javascript">
        /************************* BOTÕES **************************/
        var btnSalvar = new Ext.Button({
            id: 'btnSalvar',
            name: 'btnSalvar',
            text: 'Salvar',
            iconCls: 'icon-save',
            handler: Salvar
        });

        /************************* GRID **************************/
        var sm = new Ext.grid.CheckboxSelectionModel({
            listeners: {
                selectionchange: function(sm) {
                    var recLen = Ext.getCmp('grid').store.getRange().length;
                    var selectedLen = this.selections.items.length;
                    if (selectedLen == recLen) {
                        var view = Ext.getCmp('grid').getView();
                        var chkdiv = Ext.fly(view.innerHd).child(".x-grid3-hd-checker");
                        chkdiv.addClass("x-grid3-hd-checker-on");
                    }
                }
            },
            rowdeselect: function(sm, rowIndex, record) {
                var view = Ext.getCmp('grid').getView();
                var chkdiv = Ext.fly(view.innerHd).child(".x-grid3-hd-checker");
                chkdiv.removeClass('x-grid3-hd-checker-on');
            }
        });

        grid = new Translogic.PaginatedGrid({
            autoLoadGrid: false,
            id: "grid",
            stripeRows: true,
            region: 'center',
            height: 400,
            loadMask: { msg: App.Resources.Web.Carregando },
            filteringToolbar: [
                btnSalvar, {
                    id: 'btnExportar',
                    text: 'Exportar',
                    tooltip: 'Exportar',
                    iconCls: 'icon-page-excel',
                    handler: function(c) {
                        if (grid.getSelectionModel().hasSelection()) {
                            var ids = "";
                            var selection = grid.getSelectionModel().getSelections();

                            for (var i = 0; i < selection.length; i++) {
                                ids = ids + selection[i].data.Id + ";";
                            }

                            var url = "<%= this.Url.Action("Exportar") %>";

                            url += "?ids=" + ids;

                            window.open(url, "");
                        } else {
                            Ext.Msg.show({
                                title: 'Aviso',
                                msg: 'Selecione pelo menos um vagão para exportar!',
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.INFO
                            });
                        }
                    }
                }
            ],
            url: '<%= this.Url.Action("Pesquisar") %>',
            viewConfig: {
                forceFit: true
            },
            fields: [
                'Id',
                'Vagao',
                'Serie_Vagao',
                'Limite',
                'LimitePeso',
                'Tolerancia',
                'PesoInformado',
                'MediaMinimo',
                'FluxoComercial',
                'TravadoPor',
                'LimitadoPor',
                'Origem',
                'Destino',
                'Mercadoria',
                'MercadoriaCod',
                'DataEvento'
            ],
            columns: [
                new Ext.grid.RowNumberer(),
                sm,
                { dataIndex: "LimitadoPor", width: 25, sortable: false, renderer: rendererCadastrarLimiteMinimo },
                { dataIndex: "Id", hidden: true },
                { header: 'Vagão', dataIndex: "Serie_Vagao", sortable: false },
                { header: 'Limite + Tolerância', dataIndex: "Limite", sortable: false },
                { header: 'Média Mínimo', dataIndex: "MediaMinimo", sortable: false },
                { header: 'Peso Inf.', dataIndex: "PesoInformado", sortable: false },
                { header: 'Travado Por', dataIndex: "TravadoPor", sortable: false },
                { header: 'Mercadoria', dataIndex: "Mercadoria", sortable: false },
                { header: 'Origem', dataIndex: "Origem", sortable: false },
                { header: 'Destino', dataIndex: "Destino", sortable: false },
                { header: 'Dt. Evento', dataIndex: "DataEvento", sortable: false }
            ],
            sm: sm
        });

        grid.getStore().proxy.on('beforeload', function(p, params) {
            var dataInicial = Ext.getCmp("txtDataInicial").getValue();
            var dataFinal = Ext.getCmp("txtDataFinal").getValue();

            params['dataInicial'] = dataInicial.format('d/m/Y') + " " + dataInicial.format('H:i:s');
            params['dataFinal'] = dataFinal.format('d/m/Y') + " " + dataFinal.format('H:i:s');
            params['origem'] = Ext.getCmp("txtOrigem").getValue();
            params['destino'] = Ext.getCmp("txtDestino").getValue();
            params['vagoes'] = Ext.getCmp("txtVagoes").getValue();
        });

        /************************* FUNCOES **************************/

        function LimiteRenderer(val, metaData, record, rowIndex, colIndex, store) {

            var tolerancia = grid.getStore().getAt(rowIndex).get("Tolerancia");
            var limite = grid.getStore().getAt(rowIndex).get("LimitadoPor");

            if (limite == 'A') {
                var peso = parseInt(val) + parseInt(tolerancia);
                return "(" + peso + " - " + tolerancia + ") = " + parseInt(val);

            }
            var peso = parseInt(val) - parseInt(tolerancia);
            return "(" + peso + " + " + tolerancia + ") = " + parseInt(val);
        }

        function Salvar() {
            var ids = new Array();
            var selection = sm.getSelections();

            for (var i = 0; i < selection.length; i++) {
                ids.push(selection[i].data.Id);
            }

            Ext.Msg.show({
                title: 'Atenção',
                msg: 'Deseja liberar os vagões selecionados?',
                buttons: Ext.Msg.YESNO,
                fn: function(btn) {
                    if (btn == 'yes') {
                        Ext.Ajax.request({
                            url: '<%= this.Url.Action("Salvar") %>',
                            params:
                            {
                                ids: ids
                            },
                            success: function(result) {
                                var data = Ext.decode(result.responseText);

                                if (data.success) {
                                    Pesquisar();

                                    Ext.Msg.show({
                                        title: 'Aviso',
                                        msg: 'Vagões liberado com sucesso!',
                                        buttons: Ext.Msg.OK,
                                        icon: Ext.MessageBox.INFO
                                    });

                                } else {
                                    Ext.Msg.show({
                                        title: 'Aviso',
                                        msg: data.message,
                                        buttons: Ext.Msg.OK,
                                        icon: Ext.MessageBox.ERROR
                                    });
                                }
                            }
                        });
                    }
                },
                icon: Ext.MessageBox.QUESTION
            });
        }

        function Pesquisar() {
            grid.getStore().load();
        }

        function Limpar() {
            grid.getStore().removeAll();
            Ext.getCmp("filtros").getForm().reset();
        }

        /************************* CAMPOS FILTRO **************************/
        var dataAtual = new Date();

        var txtDataInicial = {
            xtype: 'datefield',
            fieldLabel: 'Data Inicial',
            id: 'txtDataInicial',
            name: 'txtDataInicial',
            width: 83,
            allowBlank: false,
            vtype: 'daterange',
            endDateField: 'txtDataFinal',
            hiddenName: 'txtDataInicial',
            value: dataAtual
        };

        var txtDataFinal = {
            xtype: 'datefield',
            fieldLabel: 'Data Final',
            id: 'txtDataFinal',
            name: 'txtDataFinal',
            width: 83,
            allowBlank: false,
            vtype: 'daterange',
            startDateField: 'txtDataInicial',
            hiddenName: 'txtDataFinal',
            value: dataAtual
        };

        var txtOrigem = {
            xtype: 'textfield',
            name: 'txtOrigem',
            id: 'txtOrigem',
            fieldLabel: 'Origem',
            maxLength: 3,
            autoCreate: { tag: 'input', type: 'text', maxlength: '3', autocomplete: 'off' },
            vtype: 'cteestacaovtype',
            style: 'text-transform: uppercase',
            width: 50
        };

        var txtDestino = {
            xtype: 'textfield',
            name: 'txtDestino',
            id: 'txtDestino',
            fieldLabel: 'Destino',
            maxLength: 3,
            autoCreate: { tag: 'input', type: 'text', maxlength: '3', autocomplete: 'off' },
            vtype: 'cteestacaovtype',
            style: 'text-transform: uppercase',
            width: 50
        };

        var txtVagoes = {
            xtype: 'textfield',
            name: 'txtVagoes',
            id: 'txtVagoes',
            fieldLabel: 'Lista de Vagões(separados por ";")',
            maxLength: 500,
            width: 200,
            enableKeyEvents: true,
            listeners: {
                specialkey: function(f, e) {
                    if (e.getKey() == e.ENTER) {
                        Pesquisar();
                    }
                }
            }
        };

        /************************* LAYOUT **************************/
        var coluna1 = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            items: [txtDataInicial]
        };

        var coluna2 = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            items: [txtDataFinal]
        };

        var coluna3 = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            items: [txtOrigem]
        };

        var coluna4 = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            items: [txtDestino]
        };

        var coluna5 = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            items: [txtVagoes]
        };

        var linha = {
            layout: 'column',
            border: false,
            items: [coluna1, coluna2, coluna3, coluna4, coluna5]
        };

        /************************* FILTRO **************************/
        var filtros = new Ext.form.FormPanel({
            id: 'filtros',
            title: 'Filtros',
            name: 'filtros',
            initEl: function(el) {
                this.el = Ext.get(el);
                this.id = this.el.id || Ext.id();
                if (this.standardSubmit) {
                    this.el.dom.action = this.url;
                } else {
                    this.el.on('submit', this.onSubmit, this);
                }
                this.el.addClass('x-form');
            },
            layout: 'form',
            labelAlign: 'top',
            border: false,
            items: [linha],
            buttonAlign: 'center',
            buttons: [
                {
                    name: 'btnPesquisar',
                    id: 'btnPesquisar',
                    text: 'Pesquisar',
                    iconCls: 'icon-find',
                    handler: Pesquisar
                },
                {
                    name: 'btnLimpar',
                    id: 'btnLimpar',
                    text: 'Limpar',
                    handler: Limpar
                }
            ]
        });

        /************************* RENDER **************************/
        Ext.onReady(function() {
            filtros.render(document.body);
            grid.render(document.body);
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div id="header-content">
        <h1>
            Operação - Liberação de Vagões</h1>
        <small>Operação > Liberação de Vagões</small>
        <br />
    </div>
</asp:Content>