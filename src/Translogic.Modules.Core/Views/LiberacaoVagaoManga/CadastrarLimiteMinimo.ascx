﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<script type="text/javascript">

var windowCadastroMinimo = null;

/************************* POPUP CADASTRO MINIMO **************************/

/*************************** CAMPOS *****************************/

var txtCadastroLimiteRota = {
    xtype: 'numberfield',
    name: 'txtCadastroLimiteRota',
    id: 'txtCadastroLimiteRota',
    fieldLabel: 'Limite Rota',
    maxLength: 8,
    autoCreate: { tag: 'input', type: 'text', maxlength: '8', autocomplete: 'off' },
    style: 'text-transform: uppercase',
    width: 70,
    listeners: {
        specialkey: function (f, e) {
            if (e.getKey() == e.ENTER) {
                VerificarLimiteMinimo();
            }
        }
    }
};

var txtCadastroMercadoria = {
    xtype: 'textfield',
    name: 'txtCadastroMercadoria',
    id: 'txtCadastroMercadoria',
    vtype: 'ctevagaovtype',
    fieldLabel: 'Mercadoria',
    maxLength: 8,
    autoCreate: { tag: 'input', type: 'text', maxlength: '8', autocomplete: 'off' },
    style: 'text-transform: uppercase',
    width: 70,
    listeners: {
        specialkey: function (f, e) {
            if (e.getKey() == e.ENTER) {
                VerificarLimiteMinimo();
            }
        }
    }
};

var txtCadastroVagao = {
    xtype: 'textfield',
    name: 'txtCadastroVagao',
    id: 'txtCadastroVagao',
    vtype: 'ctevagaovtype',
    fieldLabel: 'Vagão',
    maxLength: 7,
    width: 70,
    enableKeyEvents: true,
    autoCreate: { tag: 'input', type: 'text', maxlength: '7', autocomplete: 'off' },
    listeners: {
        specialkey: function (f, e) {
            if (e.getKey() == e.ENTER) {
                VerificarLimiteMinimo();
            }
        }
    }
};

var txtCadastroPesoMinimo = {
    xtype: 'numberfield',
    name: 'txtCadastroPesoMinimo',
    id: 'txtCadastroPesoMinimo',
    fieldLabel: 'Peso Mínimo',
    maxLength: 8,
    width: 70,
    enableKeyEvents: true,
    autoCreate: { tag: 'input', type: 'text', maxlength: '8', autocomplete: 'off' },
    listeners: {
        specialkey: function (f, e) {
            if (e.getKey() == e.ENTER) {
                VerificarLimiteMinimo();
            }
        }
    }
};

/*************************** LAYOUT *****************************/
var CadastroMinimoColuna1 = {
    layout: 'form',
    border: false,
    bodyStyle: 'padding: 5px',
    items: [txtCadastroLimiteRota]
};

var CadastroMinimoColuna2 = {
    layout: 'form',
    border: false,
    bodyStyle: 'padding: 5px',
    items: [txtCadastroMercadoria]
};

var CadastroMinimoColuna3 = {
    layout: 'form',
    border: false,
    bodyStyle: 'padding: 5px',
    items: [txtCadastroVagao]
};

var CadastroMinimoColuna4 = {
    layout: 'form',
    border: false,
    bodyStyle: 'padding: 5px',
    items: [txtCadastroPesoMinimo]
};

var linhaCadastroMinimo = {
    layout: 'column',
    border: false,
    items: [CadastroMinimoColuna1, CadastroMinimoColuna2, CadastroMinimoColuna3, CadastroMinimoColuna4]
};

/*************************** FORM *****************************/
var formCadastroMinimo = new Ext.form.FormPanel
({
    id: 'formCadastroMinimo',
    labelWidth: 80,
    width: '100%',
    height: 105,
    autoScroll: true,
    labelAlign: 'top',
    items:
        [
            linhaCadastroMinimo
        ],
    buttonAlign: "center",
    buttons:
        [{
                text: "Salvar",
                handler: function() { VerificarLimiteMinimo(); }
        },
            {
                text: "Cancelar",
                handler: function () { windowCadastroMinimo.hide(); }
        }]
});

function CadastrarLimiteMinimo() 
{
    if (windowCadastroMinimo == null) {
            windowCadastroMinimo = new Ext.Window({
            id: 'windowCadastroMinimo',
            title: 'Cadastrar Limite Mínimo por Vagão',
            modal: true,
            width: 340,
            closeAction: 'hide',
            height: 130,
            items: [formCadastroMinimo],
            listeners: {
                'hide': function(){
                    Ext.getCmp("txtCadastroLimiteRota").setReadOnly(false);
                    Ext.getCmp("txtCadastroMercadoria").setReadOnly(false);
                    Ext.getCmp("txtCadastroVagao").setReadOnly(false);
                    Ext.getCmp("txtCadastroLimiteRota").setValue("");
                    Ext.getCmp("txtCadastroMercadoria").setValue("");
                    Ext.getCmp("txtCadastroVagao").setValue("");
                    Ext.getCmp("txtCadastroPesoMinimo").setValue("");
				}
            }
        });    
    }
    
    windowCadastroMinimo.show();
}

/**************************** RowAction GRID *****************************/

function onClickCadastrarLimite(vagao, mercadoriaCod, limite) 
{
    Ext.getCmp("txtCadastroLimiteRota").setReadOnly(true);
    Ext.getCmp("txtCadastroMercadoria").setReadOnly(true);
    Ext.getCmp("txtCadastroVagao").setReadOnly(true);
    Ext.getCmp("txtCadastroVagao").setValue(vagao);
    Ext.getCmp("txtCadastroMercadoria").setValue(mercadoriaCod);
    Ext.getCmp("txtCadastroLimiteRota").setValue(limite);
    Ext.getCmp("txtCadastroPesoMinimo").setValue(0);
	            
	CadastrarLimiteMinimo();
}

function rendererCadastrarLimiteMinimo(val, c, record) 
{
    if($.trim(val) == "A") 
    {
        var ret = "<div class='ux-row-action'><div class='ux-row-action-item icon-edit' title='Cadastrar Limite Mímino' onclick=\"javascript:onClickCadastrarLimite('{0}','{1}','{2}');\" /></div>";
        
        return String.format(ret, record.data.Vagao, record.data.MercadoriaCod, record.data.LimitePeso);
    }
    else {
        return "";
    }
}

/************************** FIM GRID ***************************/

/*************************** FUNÇÕES *****************************/
function SalvarLimiteMinimo(lmteMinimoBaseId, vgao, pMinimo) 
{
    Ext.Ajax.request({    
        url: "<%= Url.Action("SalvarLimiteMinimo") %>",
        method: "POST",                        
		params: { limiteMinimoBaseId:lmteMinimoBaseId, vagao:vgao, pesoMinimo:pMinimo },
        success: function(resultSalvar)
        {
            var resSalvar = Ext.util.JSON.decode(resultSalvar.responseText);
            
            if(resSalvar.Success) 
            {
                Ext.Msg.show({
                    title: "Mensagem de Informação",
                    msg: "Limite cadastrado com sucesso",
                    buttons: Ext.Msg.OK,
                    minWidth: 200
                });
                
                Ext.getCmp("txtCadastroVagao").setValue("");
                Ext.getCmp("txtCadastroMercadoria").setValue("");
                Ext.getCmp("txtCadastroLimiteRota").setValue("");
                Ext.getCmp("txtCadastroPesoMinimo").setValue("");
                
                windowCadastroMinimo.hide();
            }
            else 
            {
                if (resSalvar.Status == 200) 
                {
                    Ext.Msg.show({
                        title: "Mensagem de Erro",
                        msg: resSalvar.Message,
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.ERROR,
                        minWidth: 200
                    });   
                }
            }
        },
        failure: function(resultFail) 
        {
            var resFail = Ext.util.JSON.decode(resultFail.responseText);
            
            Ext.Msg.show({
                title: "Mensagem de Erro",
                msg: resFail.Message != null && resFail.Message != undefined ? resFail.Message : resFail.message,
                buttons: Ext.Msg.OK,
                icon: Ext.MessageBox.ERROR,
                minWidth: 200
            });
        }			            
	});
}

function VerificarResponse(responseData, vagao, pesoMinimo) 
{
    if (responseData.LimiteExcecaoVagao != null || responseData.LimiteExcecaoVagao != undefined) 
    {
        if (parseFloat(responseData.LimiteExcecaoVagao.PesoMinimo) != parseFloat(pesoMinimo)) 
        {
            if (Ext.Msg.confirm("Alteração de Limite", "Confirma a alteração do Limite: "+parseFloat(responseData.LimiteExcecaoVagao.PesoMinimo)+" para: "+pesoMinimo, function(btn, text) {
                if (btn == 'yes')
                {
                    SalvarLimiteMinimo(responseData.LimiteMinimoBase.Id, vagao, pesoMinimo);
                }
            }));
        }
        else {
            Ext.Msg.show({
                    title: "Mensagem de Informação",
                    msg: "Não há alterações de limite<br/>Limite Atual: "+responseData.LimiteExcecaoVagao.PesoMinimo+"<br/>Novo Limite: "+pesoMinimo,
                    buttons: Ext.Msg.OK,
                    minWidth: 200
                });
        }
    }
    else if (parseFloat(responseData.LimiteMinimoBase.MenorValorRota) != parseFloat(pesoMinimo)) 
    {
        if (Ext.Msg.confirm("Alteração de Limite", "Confirma a alteração do Limite: "+responseData.LimiteMinimoBase.MenorValorRota+" para: "+pesoMinimo, function(btn, text) {
            if (btn == 'yes')
            {
                SalvarLimiteMinimo(responseData.LimiteMinimoBase.Id, vagao, pesoMinimo);
            }
        }));
    }
    else {
        Ext.Msg.show({
                title: "Mensagem de Informação",
                msg: "Não há alterações de limite<br/>Limite Atual: "+responseData.LimiteMinimoBase.MenorValorRota+"<br/>Novo Limite: "+pesoMinimo,
                buttons: Ext.Msg.OK,
                minWidth: 200
            });
    }
}

function VerificarLimiteMinimo()
{
    var vgao = Ext.getCmp("txtCadastroVagao").getValue();
    var mcadoria = Ext.getCmp("txtCadastroMercadoria").getValue();
    var lmite = Ext.getCmp("txtCadastroLimiteRota").getValue();
    var pesoMinimo = Ext.getCmp("txtCadastroPesoMinimo").getValue();

    if ($.trim(lmite.toString()) != "" && $.trim(mcadoria.toString()) != "" && $.trim(vgao.toString()) != "" && $.trim(pesoMinimo.toString()) != "")
    {
        Ext.Ajax.request({
            url: "<%= Url.Action("VerificarLimiteMinimo") %>",
            method: "POST",
            params: { limite: lmite, mercadoria: mcadoria, vagao: vgao, pesoMinimo: pesoMinimo },
            success: function(resVerificar)
            {
                var result = Ext.util.JSON.decode(resVerificar.responseText);

                if (result.Success)
                {
                    VerificarResponse(result, vgao, pesoMinimo);
                }
                else 
                {
                    if (result.Status == 200) {
                        Ext.Msg.show({
                            title: "Mensagem de Erro",
                            msg: result.Message,
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.ERROR,
                            minWidth: 200
                        });   
                    }
                }
            },
            failure: function(resVerificarFail)
            {
                var resFail = Ext.util.JSON.decode(resVerificarFail.responseText);

                Ext.Msg.show({
                        title: "Mensagem de Erro",
                        msg: resFail.Message != null && resFail.Message != undefined ? resFail.Message : resFail.message,
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.ERROR,
                        minWidth: 200
                    });
            }
        });

    }
    else 
    {
        Ext.Msg.show({
            title: "Mensagem de Erro",
            msg: 'Preencha todas as informações para continuar',
            buttons: Ext.Msg.OK,
            icon: Ext.MessageBox.ERROR,
            minWidth: 200
        });
    }
}
</script>