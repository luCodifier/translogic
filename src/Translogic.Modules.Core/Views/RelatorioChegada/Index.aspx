﻿<%@ Import Namespace="System.Security.Policy" %>
<%@ Import Namespace="Translogic.Modules.Core.Domain.Model.Diversos.Mdfe" %>

<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Interna.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content runat="server" ID="HeadContent" ContentPlaceHolderID="HeadContent">
<%-- ReSharper disable LValueIsExpected --%>
    <script type="text/javascript" language="javascript">

       /************************* STORES **************************/
        var storeDetalhe = new Ext.data.JsonStore({
            root: "Items",
            autoLoad: false,
            url: '<%= Url.Action("DetalhesFluxoPorPatio") %>',
            fields: [
                    'IdMdfe',
                    'EmpresaRecebedora',
                    'IdEmpresa',
                    'CnpjRecebedora'
			    ]
        });
     
     var storeLinhas = new Ext.data.JsonStore({
            root: "Items",
            autoLoad: false,
            url: '<%= Url.Action("ObterLinhas") %>',
            fields: [
                 'Id',
                 'Codigo'
			    ]
        });
             

        /************************* GRID **************************/
        var changeCursorToHand = function (val, metadata, record) {
            metadata.style = 'cursor: pointer;';
            return val;
         };
		  
		function statusRenderer(val)
		{
		switch(val){
			case "AUT":
				return "<img src='<%=Url.Images("Icons/tick.png") %>' alt='MDF-e Aprovado'>";
				break;
						case "AGE":
						case "AGC":
				return "<img src='<%=Url.Images("Icons/application_go.png") %>' alt='Aguardando Cancelamento/Encerramento'>";
				break;
			case "EAE":
				return "<img src='<%=Url.Images("Icons/email_go.png") %>' alt='Enviado arquivo MDF-e para Sefaz.'>";
				break;
			case "EAR":
				return "<img src='<%=Url.Images("Icons/arrow_redo_yellow.png") %>' alt='Erro Autorizado reenvio do MDF-e.'>";
				break;            
			case "CAN":
				return "<img src='<%=Url.Images("Icons/cancel.png") %>' alt='MDF-e Cancelado '>";
				break;                        
				case "ENC":
				return "<img src='<%=Url.Images("Icons/delete.png") %>' alt='MDF-e Encerrado'>";
				break;                                                                                 
				case "ERR":
				return "<img src='<%=Url.Images("Icons/cross.png") %>' alt='Erro na geração do MDF-e.'>";
				break; 
			case "PGC":
				return "<img src='<%=Url.Images("Icons/information.png") %>' alt='MDf-e aguardando a geração automática da numeração / chave.'>";
				break; 	           
			default:
				return "<img src='<%=Url.Images("Icons/alert.png") %>' alt='MDF-e em processamento.'>";
				break;
		}
		}     

        var detalheAction = new Ext.ux.grid.RowActions({
           	dataIndex: '',
           	header: '',
           	align: 'center',
           	actions: [{
           		iconCls: 'icon-detail',
           		tooltip: 'Exibir Detalhes'
           	}],
           	callbacks: {
           		'icon-detail': function (grid, record, action, row, col) {
           			Detalhe(record.data.Id);
           		}
           	}
           });

        var grid = new Translogic.PaginatedGrid({
            id: 'grid',
            name: 'grid',
            autoLoadGrid: false,
            height: 400,
            plugins: [detalheAction],
            fields: [
                'Id',
                'Prefixo',
                'Origem',
                'Destino',
                'OS',
				'SituacaoAtualEnum'
            ],
            url: '<%= Url.Action("Pesquisar") %>',
            columns: [
                { dataIndex: "Id", hidden: true },
				detalheAction,
				{ header: 'Status', width: 20, dataIndex: "SituacaoAtualEnum", renderer:statusRenderer },
                { header: 'Prefixo', width: 80, dataIndex: "Prefixo", sortable: false},
                { header: 'Origem', width: 80, dataIndex: "Origem", sortable: false },
                { header: 'Destino', width: 80, dataIndex: "Destino", sortable: false },
                { header: 'OS', width: 80, dataIndex: "OS", sortable: false }
            ],
            listeners: {
                rowdblclick: function (grid, rowIndex, record, e) {
                  //  MostrarDetalhes(grid.getStore().getAt(rowIndex).get('Id'));
                }
            }
        });

        grid.getStore().proxy.on('beforeload', function (p, params) {
            var dataInicial = Ext.getCmp("txtFiltroDataInicial").getValue();
            var dataFinal = Ext.getCmp("txtFiltroDataFinal").getValue();
            var chave = Ext.getCmp("txtFiltroChave").getValue();
            var numero = Ext.getCmp("txtFiltroNumero").getValue();
            var serie = Ext.getCmp("txtFiltroSerie").getValue();
            var prefixo = Ext.getCmp("txtFiltroPrefixo").getValue();
            var os = Ext.getCmp("txtFiltroOS").getValue();
            var origem = Ext.getCmp("filtro-Ori").getValue();
            var destino = Ext.getCmp("filtro-Dest").getValue();
            
            params['filter[0].Campo'] = 'dataInicial';
            params['filter[0].Valor'] = new Array(dataInicial.format('d/m/Y') + " " + dataInicial.format('H:i:s'));
            params['filter[0].FormaPesquisa'] = 'Start';

            params['filter[1].Campo'] = 'dataFinal';
            params['filter[1].Valor'] = new Array(dataFinal.format('d/m/Y') + " " + dataFinal.format('H:i:s'));
            params['filter[1].FormaPesquisa'] = 'Start';

            params['filter[2].Campo'] = 'serie';
            params['filter[2].Valor'] = serie;
            params['filter[2].FormaPesquisa'] = 'Start';

            params['filter[3].Campo'] = 'chave';
            params['filter[3].Valor'] = chave;
            params['filter[3].FormaPesquisa'] = 'Start';

            params['filter[4].Campo'] = 'numero';
            params['filter[4].Valor'] = numero;
            params['filter[4].FormaPesquisa'] = 'Start';

            params['filter[5].Campo'] = 'prefixo';
            params['filter[5].Valor'] = prefixo;
            params['filter[5].FormaPesquisa'] = 'Start';

            params['filter[6].Campo'] = 'os';
            params['filter[6].Valor'] = os;
            params['filter[6].FormaPesquisa'] = 'Start';
            
            params['filter[7].Campo'] = 'origem';
            params['filter[7].Valor'] = origem;
            params['filter[7].FormaPesquisa'] = 'Start';
            
            params['filter[8].Campo'] = 'destino';
            params['filter[8].Valor'] = destino;
            params['filter[8].FormaPesquisa'] = 'Start';
        });

        /************************* FILTROS **************************/
        var txtFiltroDataInicial = {
            xtype: 'datefield',
            fieldLabel: 'Data Inicial',
            id: 'txtFiltroDataInicial',
            name: 'txtFiltroDataInicial',
            width: 90,
            allowBlank: false,
            vtype: 'daterange',
            endDateField: 'txtFiltroDataFinal',
            value: new Date(),
            enableKeyEvents: true,
            listeners: {
            	specialkey: TratarTeclaEnter
            }
        };

        var txtFiltroDataFinal = {
            xtype: 'datefield',
            fieldLabel: 'Data Final',
            id: 'txtFiltroDataFinal',
            name: 'txtFiltroDataFinal',
            width: 90,
            allowBlank: false,
            vtype: 'daterange',
            startDateField: 'txtFiltroDataInicial',
            value: new Date(),
            enableKeyEvents: true,
            listeners: {
            	specialkey: TratarTeclaEnter
            }
        };
        
        var txtFiltroOS = {
        	xtype: 'textfield',
        	autoCreate: { tag: 'input', type: 'text', maxlength: '10', autocomplete: 'off' },
            width: 70,
            maskRe: /[0-9]/,
            name: 'txtFiltroOS',
            allowBlank: true,
            id: 'txtFiltroOS',
            fieldLabel: 'OS',
            enableKeyEvents: true,
            listeners: {
                specialkey: TratarTeclaEnter
            }
         };
   
        var txtFiltroPrefixo = {
        	xtype: 'textfield',
        	style: 'text-transform: uppercase',
        	autoCreate: { tag: 'input', type: 'text', maxlength: '3', autocomplete: 'off' },
        	maskRe: /[a-zA-Z0-9]/,
            width: 60,
            name: 'txtFiltroPrefixo',
            allowBlank: true,
            id: 'txtFiltroPrefixo',
            fieldLabel: 'Prefixo',
            enableKeyEvents: true,
            listeners: {
                specialkey: TratarTeclaEnter
            }
        };     		

        var txtFiltroChave = {
        	xtype: 'textfield',
        	maskRe: /[0-9]/,
        	autoCreate: { tag: 'input', type: 'text', maxlength: '44', autocomplete: 'off' },
            width: 280,
            name: 'txtFiltroChave',
            allowBlank: true,
            id: 'txtFiltroChave',
            fieldLabel: 'Chave da MDFe',
            enableKeyEvents: true,
            listeners: {
                specialkey: TratarTeclaEnter
            }
        };

        var txtFiltroSerie = {
        	xtype: 'textfield',
        	maskRe: /[0-9]/,
        	autoCreate: { tag: 'input', type: 'text', maxlength: '3', autocomplete: 'off' },
            width: 40,
            name: 'txtFiltroSerie',
            allowBlank: true,
            id: 'txtFiltroSerie',
            fieldLabel: 'Série',
            enableKeyEvents: true,
            listeners: {
                specialkey: TratarTeclaEnter
            }
        };

        var txtFiltroNumero = {
            xtype: 'textfield',
            maskRe: /[0-9]/,
            autoCreate: { tag: 'input', type: 'text', maxlength: '9', autocomplete: 'off' },
            width: 70,
            name: 'txtFiltroNumero',
            allowBlank: true,
            id: 'txtFiltroNumero',
            fieldLabel: 'Número',
            enableKeyEvents: true,
            listeners: {
                specialkey: TratarTeclaEnter
            }
        };

        
        var txtOrigem =	{
			xtype: 'textfield',
			vtype: 'cteestacaovtype',
			style: 'text-transform: uppercase',
			id: 'filtro-Ori',
			fieldLabel: 'Origem',
			autoCreate: { tag: 'input', type: 'text', maxlength: '3', autocomplete: 'off' },
			name: 'Origem',
			allowBlank: true,
			maxLength: 3,
			width: 35,
			hiddenName: 'Origem'
		};

		var txtDestino = {
			xtype: 'textfield',
			vtype: 'cteestacaovtype',
			style: 'text-transform: uppercase',
			id: 'filtro-Dest',
			fieldLabel: 'Destino',
			autoCreate: { tag: 'input', type: 'text', maxlength: '3', autocomplete: 'off' },
			name: 'Destino',
			allowBlank: true,
			maxLength: 3,
			width: 35,
			hiddenName: 'Destino'
		};
		
        var btnPesquisar = {
            name: 'btnPesquisar',
            id: 'btnPesquisar',
            text: 'Pesquisar',
            iconCls: 'icon-find',
            handler: CarregarGrid
        };

        var btnLimpar = {
            name: 'btnLimpar',
            id: 'btnLimpar',
            text: 'Limpar',
            handler: Limpar
        };


        var coluna1Linha1 = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding-right:10px',
            items: [txtFiltroDataInicial]
        };

        var coluna2Linha1 = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding-right:10px',
            items: [txtFiltroDataFinal]
        };

        var coluna3Linha1 = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding-right:10px',
            items: [txtFiltroPrefixo]
        };

        var coluna4Linha1 = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding-right:10px',
            items: [txtFiltroOS]
        };

        var coluna5Linha1 = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding-right:10px',
            items: [txtOrigem]
        };

        var coluna6Linha1 = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding-right:10px',
            items: [txtDestino]
        };

        var linha1 = {
            layout: 'column',
            border: false,

            items: [coluna1Linha1, coluna2Linha1, coluna3Linha1, coluna4Linha1, coluna5Linha1,coluna6Linha1]
        };


        var coluna1Linha2 = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding-right:10px',
            items: [txtFiltroChave]
        };

        var coluna2Linha2 = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding-right:10px',
            items: [txtFiltroSerie]
        };

        var coluna3Linha2 = {
            layout: 'form',
            border: false,
            items: [txtFiltroNumero]
        };

        var linha2 = {
            layout: 'column',
            border: false,
            items: [coluna1Linha2, coluna2Linha2, coluna3Linha2]
        };
        
         /************************* FILTROS VAGAO-ESTACAO **************************/
        var txtFiltroEstacaoDataInicial = {
            xtype: 'datefield',
            fieldLabel: 'Data Inicial',
            id: 'txtFiltroEstacaoDataInicial',
            name: 'txtFiltroEstacaoDataInicial',
            width: 90,
            allowBlank: false,
            vtype: 'daterange',
            endDateField: 'txtFiltroEstacaoDataFinal',
            value: new Date(),
            enableKeyEvents: true,
            listeners: {
            	specialkey: TratarTeclaEnter
            }
        };

        var txtFiltroEstacaoDataFinal = {
            xtype: 'datefield',
            fieldLabel: 'Data Final',
            id: 'txtFiltroEstacaoDataFinal',
            name: 'txtFiltroEstacaoDataFinal',
            width: 90,
            allowBlank: false,
            vtype: 'daterange',
            startDateField: 'txtFiltroEstacaoDataInicial',
            value: new Date(),
            enableKeyEvents: true,
            listeners: {
            	specialkey: TratarTeclaEnter
            }
        };
               
        var txtFiltroEstacaoOrigem =	{
			xtype: 'textfield',
			vtype: 'cteestacaovtype',
			style: 'text-transform: uppercase',
			id: 'txtFiltroEstacaoOrigem',
			fieldLabel: 'Estação',
			autoCreate: { tag: 'input', type: 'text', maxlength: '3', autocomplete: 'off' },
			name: 'txtFiltroEstacaoOrigem',
			allowBlank: true,
			maxLength: 3,
			width: 35,
			hiddenName: 'txtFiltroEstacaoOrigem',
			listeners: {
            	specialkey: TratarTeclaEnter,
			    'change': function () {
                    window.Ext.getCmp('cboFiltroLinha').clearValue();
                    var tmpStore = window.Ext.getCmp('cboFiltroLinha').getStore();
                    tmpStore.setBaseParam('codAreaOperacional', window.Ext.getCmp('txtFiltroEstacaoOrigem').getValue());
                    tmpStore.removeAll();
                    tmpStore.reload({
                        'params': {
                            codAreaOperacional: window.Ext.getCmp('txtFiltroEstacaoOrigem').getValue()
                        }
                    });
                }
            }
		};
        
        var cboFiltroLinha = {
            id: 'cboFiltroLinha',
            xtype: 'combo',
            name: 'cboFiltroLinha',
            forceSelection: true,
            store: new window.Ext.data.JsonStore({
                id: 'dsLinhas',
                name: 'dsLinhas',
                root: 'Items',
                url: '<%= Url.Action("ObterLinhas") %>',
                fields: ['Id', 'Codigo']
            }),
            triggerAction: 'all',
            mode: 'remote',
            typeAhead: true,
            displayField: 'Codigo',
            valueField: 'Id',
            fieldLabel: 'Linhas',
            width: 100,
            minChars: 1,
            listeners: {
            	specialkey: TratarTeclaEnter
            }
        };
        var btnEstacaoPesquisar = {
            xtype: 'button',
            name: 'btnEstacaoPesquisar',
            id: 'btnEstacaoPesquisar',
            text: 'Pesquisar',
            iconCls: 'icon-find',
            handler: CarregarGridEstacao
        };

        var btnEstacaoLimpar = {
            xtype: 'button',
            name: 'btnEstacaoLimpar',
            id: 'btnEstacaoLimpar',
            text: 'Limpar',
            handler: LimparEstacao
        };

        var estacaoColuna1Linha1 = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding-right:10px',
            items: [txtFiltroEstacaoDataInicial]
        };

        var estacaoColuna2Linha1 = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding-right:10px',
            items: [txtFiltroEstacaoDataFinal]
        };

        var estacaoColuna3Linha1 = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding-right:10px',
            items: [txtFiltroEstacaoOrigem]
        };

        var estacaoColuna4Linha1 = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding-right:10px',
            items: [cboFiltroLinha]
        };
        
        var linhaEstacao1 = {
            layout: 'column',
            border: false,
            items: [estacaoColuna1Linha1, estacaoColuna2Linha1, estacaoColuna3Linha1,estacaoColuna4Linha1]
        };
        
         var estacaoDetalheAction = new Ext.ux.grid.RowActions({
           	dataIndex: '',
           	header: '',
            width: 50,
           	align: 'center',
           	actions: [{
           		iconCls: 'icon-detail',
           		tooltip: 'Exibir Detalhes'
           	}],
           	callbacks: {
           		'icon-detail': function (grid, record, action, row, col) {
           			DetalheEstacao(record.data.IdEmpresa);
           		}
           	}
           });

         var gridEstacao = new Ext.grid.GridPanel({
            id: 'gridEstacao',
            name: 'gridEstacao',
            autoLoadGrid: false,
            height: 500,
            plugins: [estacaoDetalheAction],
            viewConfig: {
                forceFit: true
            },
            stripeRows: true,
            region: 'center',
            store: storeDetalhe,

            loadMask: { msg: App.Resources.Web.Carregando },
            colModel: new Ext.grid.ColumnModel({
                defaults: {
                    sortable: false
                },
                columns: [
                { dataIndex: "IdMdfe", hidden: true },
                estacaoDetalheAction,
                { header: 'Empresa Recebedora', width: 100, dataIndex: "EmpresaRecebedora", sortable: false },
                { header: 'Cnpj', width: 100, dataIndex: "CnpjRecebedora", sortable: false }
             ]
            })
        });
         
         gridEstacao.getStore().proxy.on('beforeload', function (p, params) {
            var dataInicial = Ext.getCmp("txtFiltroEstacaoDataInicial").getValue();
            var dataFinal = Ext.getCmp("txtFiltroEstacaoDataFinal").getValue();
            var estacao = Ext.getCmp("txtFiltroEstacaoOrigem").getValue();
            var linha = Ext.getCmp("cboFiltroLinha").getValue();
            
            params['filter[0].Campo'] = 'dataInicial';
            params['filter[0].Valor'] = new Array(dataInicial.format('d/m/Y') + " " + dataInicial.format('H:i:s'));
            params['filter[0].FormaPesquisa'] = 'Start';

            params['filter[1].Campo'] = 'dataFinal';
            params['filter[1].Valor'] = new Array(dataFinal.format('d/m/Y') + " " + dataFinal.format('H:i:s'));
            params['filter[1].FormaPesquisa'] = 'Start';

            params['filter[2].Campo'] = 'estacao';
            params['filter[2].Valor'] = estacao;
            params['filter[2].FormaPesquisa'] = 'Start';

            params['filter[3].Campo'] = 'linha';
            params['filter[3].Valor'] = linha;
            params['filter[3].FormaPesquisa'] = 'Start';
        });

        /************************* TABS E PANELS **************************/
         

        var filtros = new Ext.form.FormPanel({
            id: 'filtros',
            title: 'Filtros',
            name: 'filtros',
            bodyStyle: 'padding: 10px',
            labelAlign: 'top',
            items: [linha1, linha2],
            buttonAlign: 'center',
            buttons: [btnPesquisar, btnLimpar]
        });
             
        var tabFiltrosEstacao = new Ext.form.FormPanel({
            id: 'filtros-estacao',
            title: 'Pesquisa por Estação',
            name: 'filtros-estacao',
            bodyStyle: 'padding: 10px',
            labelAlign: 'top',
            items: [linhaEstacao1, gridEstacao],
            buttonAlign: 'center',
            buttons: [btnEstacaoPesquisar, btnEstacaoLimpar]
        });
        
        var tabs = new Ext.TabPanel({
            activeTab: 0,
            height: 670,
            items: [{
                        title: 'Pesquisa por Trem',                
                        items: [filtros, grid]
                    },{
                        title: 'Pesquisa por Estação',                
                        items: [tabFiltrosEstacao, gridEstacao]
                    }
             ]
        });
   
        /************************* DETALHES **************************/
        var formDetalhes;

        var url = '<%=Url.Action("FluxoTransportados") %>';

		function Detalhe(id) {
		    Ext.getCmp("grid").getEl().mask("Aguarde...", "x-mask-loading");
			url = url + "?idTrem=" + id;
			document.location.href = url;
		}
		
        function DetalheEstacao(idEmpresa) {
            var dataInicial = Ext.getCmp("txtFiltroEstacaoDataInicial").getValue();
            var dataFinal = Ext.getCmp("txtFiltroEstacaoDataFinal").getValue();
            var estacao = Ext.getCmp("txtFiltroEstacaoOrigem").getValue();
            var linha = Ext.getCmp("cboFiltroLinha").getValue();
            
            var urlTmp = '<%=Url.Action("DetalhesVagaoPorPatio") %>' + "?idEmpresa=" + idEmpresa;
            urlTmp += '&estacao=' + estacao;
            urlTmp += '&idLinha=' + linha;
            urlTmp += '&dataInicial=' + new Array(dataInicial.format('d-m-Y') + "-00-00-00");
            urlTmp += '&dataFinal=' + new Array(dataFinal.format('d-m-Y') + "-23-59-59");
            document.location.href = urlTmp;
        }
        
	    /************************* FUNCOES DE APOIO **************************/
        function TratarTeclaEnter(f, e) {
            if (e.getKey() == e.ENTER) {
            	CarregarGrid();
            }
        }

        function Limpar() {
            grid.getStore().removeAll();
            Ext.getCmp("filtros").getForm().reset();
        }

        function LimparEstacao() {
            gridEstacao.getStore().removeAll();
            tabFiltrosEstacao.getForm().reset();
        }
        
        function CarregarGrid() {
            grid.getStore().load();
        }
        
        function CarregarGridEstacao() {
          /*  var dataInicio  = Ext.getCmp('txtFiltroEstacaoDataInicial').getValue();
            var dataFim     = Ext.getCmp('txtFiltroEstacaoDataFinal').getValue();
            var estacao     = Ext.getCmp('txtFiltroEstacaoOrigem').getValue();
            var linha       = Ext.getCmp('cboFiltroLinha').getValue();
            
            if (dataInicio == "" || dataFim == "" || estacao == "" || linha == "") {

                Ext.Msg.show({
				    title: "Mensagem de Informação",
				    msg: "Preencha os filtro!",
				    buttons: Ext.Msg.OK,
				    icon: Ext.MessageBox.INFO,
				    minWidth: 200
                });
                
                return;
            }*/
            
            gridEstacao.getStore().load();
        }

        /************************* RENDER **************************/
        Ext.onReady(function () {
            tabs.render(document.body);
            // grid.render(document.body);
        });
        
    </script>
<%-- ReSharper restore LValueIsExpected --%>
</asp:Content>
<asp:Content runat="server" ID="Content2" ContentPlaceHolderID="Content">
    <div id="header-content">
        <h1>
            MDFe - Aviso de chegada</h1>
        <small>Você está em MDFe > Aviso de chegada</small>
        <br />
    </div>
</asp:Content>
