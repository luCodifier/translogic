﻿<%@ Import Namespace="System.Security.Policy" %>
<%@ Import Namespace="Translogic.Modules.Core.Domain.Model.Diversos.Mdfe" %>

<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Interna.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Import Namespace="Translogic.Modules.Core.Domain.Model.Trem" %>
<asp:Content runat="server" ID="HeadContent" ContentPlaceHolderID="HeadContent">
    <%
        Trem trem = (Trem)ViewData["Trem"]; 
    %>
    <script type="text/javascript" language="javascript">
          
        /******************************* STORE *******************************/
        var storeDetalhe = new Ext.data.JsonStore({
            root: "Items",
            autoLoad: false,
            url: '<%= Url.Action("DetalhesFluxo", new { idTrem = ViewData["IdTrem"] } ) %>',
            fields: [
                 'IdMdfe',
                    'EmpresaRecebedora',
                    'IdEmpresa',
                    'CnpjRecebedora'
			    ]
        });

        /************************* GRID **************************/
        var changeCursorToHand = function (val, metadata, record) {
            metadata.style = 'cursor: pointer;';
            return val;
        };
        
        var detalheAction = new Ext.ux.grid.RowActions({
            dataIndex: '',
            header: '',
            align: 'center',
            actions: [{
                iconCls: 'icon-detail',
                tooltip: 'Exibir Detalhes'
            }],
            callbacks: {
                'icon-detail': function (grid, record, action, row, col) {
                    Detalhe(record.data.IdEmpresa, record.data.IdMdfe);
                }
            }
        });


        var grid = new Ext.grid.GridPanel({
            id: 'grid',
            name: 'grid',
            autoLoadGrid: false,
            height: 400,
            width: 735,
            plugins: [detalheAction],
            viewConfig: {
                forceFit: true
            },
            stripeRows: true,
            region: 'center',
            store: storeDetalhe,

            loadMask: { msg: App.Resources.Web.Carregando },
            colModel: new Ext.grid.ColumnModel({
                defaults: {
                    sortable: false
                },
                columns: [
                {hidden: true },
                detalheAction,
                { header: 'Empresa Recebedora', width: 100, dataIndex: "EmpresaRecebedora", sortable: false },
                { header: 'Cnpj', width: 100, dataIndex: "CnpjRecebedora", sortable: false }
             ]
            })
        });

        /************************* FILTROS **************************/
        var txtFiltroDataPartida = {
            xtype: 'textfield',
            fieldLabel: 'Data Partida',
            id: 'txtFiltroDataPartida',
            name: 'txtFiltroDataPartida',
            width: 120,
            allowBlank: false,
            readOnly:true,
            enableKeyEvents: true,
            listeners: {
                specialkey: TratarTeclaEnter
            }
        };

        var txtFiltroDataChegada = {
            xtype: 'textfield',
            fieldLabel: 'Data Chegada',
            id: 'txtFiltroDataChegada',
            name: 'txtFiltroDataChegada',
            width: 120,
            allowBlank: false,
            readOnly:true,
            enableKeyEvents: true,
            listeners: {
                specialkey: TratarTeclaEnter
            }
        };

        var txtFiltroOS = {
            xtype: 'textfield',
            autoCreate: { tag: 'input', type: 'text', maxlength: '10', autocomplete: 'off' },
            width: 70,
            maskRe: /[0-9]/,
            name: 'txtFiltroOS',
            allowBlank: true,
            id: 'txtFiltroOS',
            fieldLabel: 'OS',
            enableKeyEvents: true,
            readOnly:true,
            listeners: {
                specialkey: TratarTeclaEnter
            }
        };

        var txtFiltroPrefixo = {
            xtype: 'textfield',
            style: 'text-transform: uppercase',
            autoCreate: { tag: 'input', type: 'text', maxlength: '3', autocomplete: 'off' },
            maskRe: /[a-zA-Z0-9]/,
            width: 60,
            name: 'txtFiltroPrefixo',
            allowBlank: true,
            id: 'txtFiltroPrefixo',
            fieldLabel: 'Prefixo',
            enableKeyEvents: true,
             readOnly:true,
            listeners: {
                specialkey: TratarTeclaEnter
            }
        };


        var origem = {
            xtype: 'textfield',
            vtype: 'cteestacaovtype',
            style: 'text-transform: uppercase',
            id: 'filtro-Ori',
            fieldLabel: 'Origem',
            autoCreate: { tag: 'input', type: 'text', maxlength: '3', autocomplete: 'off' },
            name: 'Origem',
            allowBlank: true,
            maxLength: 3,
            width: 35,
            readOnly:true,
            hiddenName: 'Origem'
        };

        var destino = {
            xtype: 'textfield',
            vtype: 'cteestacaovtype',
            style: 'text-transform: uppercase',
            id: 'filtro-Dest',
            fieldLabel: 'Destino',
            autoCreate: { tag: 'input', type: 'text', maxlength: '3', autocomplete: 'off' },
            name: 'Destino',
            allowBlank: true,
            maxLength: 3,
            width: 35,
            readOnly: true,
            hiddenName: 'Destino'
        };
        
        var coluna1Linha1 = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding-right:10px',
            items: [txtFiltroPrefixo]
        };

        var coluna2Linha1 = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding-right:10px',
            items: [txtFiltroOS]
        };

        var coluna3Linha1 = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding-right:10px',
            items: [origem]
        };

        var coluna4Linha1 = {
            layout: 'form',
            border: false,
            items: [destino]
        };

        var coluna5Linha1 = {
            layout: 'form',
            border: false,
            width: 130,
            items: [txtFiltroDataPartida]
        };

        var coluna6Linha1 = {
            layout: 'form',
            border: false,
            width: 130,
            items: [txtFiltroDataChegada]
        };

        var linha1 = {
            layout: 'column',
            border: false,
            items: [coluna1Linha1, coluna2Linha1, coluna3Linha1, coluna4Linha1, coluna5Linha1, coluna6Linha1]
        };

        var filtros = new Ext.form.FormPanel({
            id: 'filtros',
            title: 'Filtros',
            name: 'filtros',
            bodyStyle: 'padding: 10px',
            width: 735,
            labelAlign: 'top',
            items: [linha1],
            buttonAlign: 'center'
        });

        /************************* FUNCOES DE APOIO **************************/
        function TratarTeclaEnter(f, e) {
            if (e.getKey() == e.ENTER) {
                CarregarGrid();
            }
        }

        function Limpar() {
            grid.getStore().removeAll();
            Ext.getCmp("filtros").getForm().reset();
        }

        function CarregarDadosTrem() {
            var dtPartida = '<%=trem.DataRealizadaPartida %>';
            var dtChegada = '<%=trem.DataRealizadaChegada %>';
            var origem = '<%=trem.Origem.Codigo %>';
            var destino = '<%=trem.Destino.Codigo %>';
            var prefixo = '<%=trem.Prefixo %>';
            var osNumero = '<%=trem.OrdemServico.Numero %>';

            Ext.getCmp('txtFiltroDataPartida').setValue(dtPartida);
            Ext.getCmp('txtFiltroDataChegada').setValue(dtChegada);
            Ext.getCmp('filtro-Ori').setValue(origem);
            Ext.getCmp('filtro-Dest').setValue(destino);
            Ext.getCmp('txtFiltroPrefixo').setValue(prefixo);
            Ext.getCmp('txtFiltroOS').setValue(osNumero);
        }
        
        function CarregarGrid() {
            grid.getStore().load();
        }


        /************************* DETALHES **************************/
        var formDetalhes;

        var url = '<%=Url.Action("DetalhesVagao") %>';

        function Detalhe(idEmpresa, idMdfe) {
            url = url + "?idMdfe=" + idMdfe;
            url = url + "&idTrem=<%=ViewData["IdTrem"]%>";
            url = url + "&idEmpresa=" + idEmpresa;
            
            Ext.getCmp("grid").getEl().mask("Aguarde...", "x-mask-loading");
            document.location.href = url;
        }
        
        /************************* RENDER **************************/
        Ext.onReady(function () {
            // filtros.render(document.body);
            // CarregarGrid();
            filtros.render(document.body);
            grid.render(document.body);
            CarregarGrid();
            CarregarDadosTrem();
        });
    </script>
</asp:Content>
<asp:Content runat="server" ID="Content2" ContentPlaceHolderID="Content">
    <div id="header-content">
        <h1>
            Fluxos Transportados
        </h1>
        <small>Você está em MDFe > Aviso de chegada</small>
        <br />
    </div>
</asp:Content>
