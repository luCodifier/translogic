﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%@ Import Namespace="Translogic.Modules.Core.Domain.Model.Dto" %>
<%@ Import Namespace="Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico" %>
<%@ Import Namespace="Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes" %>
<%@ Import Namespace="Translogic.Modules.Core.Domain.Model.Trem" %>
<%
    Trem trem = (Trem)ViewData["TREM"];
    IList<MdfeComposicaoDto> mdfeComposicao = ViewData["MDFE_COMPOSICAO"] as IList<MdfeComposicaoDto>;
    IList<CteEmpresas> recebedores = ViewData["RECEBEDORES"] as IList<CteEmpresas>;
%>
<div style="page-break-after: always;">
    <table style="width: 90%; font-family: Arial; font-size: 10px; height: 120px">
        <tr>
            <td style="text-align: center; font-size: 15px;" colspan="2">
                <b>Recibo entrega de vagões</b>
            </td>
        </tr>
        <tr>
            <td style="text-align: center;" colspan="2">
                <%=DateTime.Now.ToString() %>
            </td>
        </tr>
        <tr>
            <td style="text-align: center;" colspan="2">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td style="width: 100px">
                Recebedor:
            </td>
            <td>
                <%= recebedores[0].EmpresaRecebedor.DescricaoDetalhada%>
                -
                <%=string.Format("{0}.{1}.{2}/{3}-{4}", recebedores[0].EmpresaRecebedor.Cgc.Substring(0, 2),
                                                                                 recebedores[0].EmpresaRecebedor.Cgc.Substring(2, 3),
                                                                                 recebedores[0].EmpresaRecebedor.Cgc.Substring(5, 3),
                                                                                 recebedores[0].EmpresaRecebedor.Cgc.Substring(8, 4),
                                                                                 recebedores[0].EmpresaRecebedor.Cgc.Substring(12, 2))%>
            </td>
        </tr>
        <tr>
            <td>
                Origem:
            </td>
            <td>
                <%= trem == null ? String.Empty : trem.Origem.Codigo %>
            </td>
        </tr>
        <tr>
            <td>
                Prefixo - OS:
            </td>
            <td>
                <%= trem == null ? String.Empty : (trem.Prefixo + "-" + trem.OrdemServico.Numero)%>
            </td>
        </tr>
    </table>
    <table style="text-align: center; width: 90%; font-family: Arial; font-size: 10px;">
        <tr>
            <td>
                <table style="text-align: center; width: 100%; font-family: Arial; font-size: 10px;">
                    <tr>
                        <td colspan="10">
                            &nbsp;
                        </td>
                    </tr>
                    <tr style="font-family: 'Arial-Bold'; font-size: 12px; border: 1px solid #666;">
                        <td>
                            Vagão
                        </td>
                        <td>
                            CTe
                        </td>
                        <td>
                            NF
                        </td>
                        <td>
                            Data NF
                        </td>
                        <td>
                            Peso NF
                        </td>
                        <td>
                            Rateio NF
                        </td>
                        <td>
                            Peso Vagão
                        </td>
                        <td>
                            Mercadoria
                        </td>
                        <td>
                            Local Carreg
                        </td>
                        <td>
                            Data Carreg.
                        </td>
                        <td>
                            Remetente
                        </td>
                        <td>
                            Destinatário
                        </td>
                    </tr>
                    <% foreach (var comp in mdfeComposicao.Where(m => m.IdCte != 0 && recebedores.Any(r => r.Cte.Id == m.IdCte && r.EmpresaRecebedor.Equals(recebedores[0].EmpresaRecebedor))).OrderBy(x => x.Sequencia).ToList())
                       { %>
                    <tr>
                        <td>
                            <%= comp.SerieVagao + "-" + comp.CodigoVagao %>
                        </td>
                        <td>
                            <%= !string.IsNullOrEmpty(comp.SerieCte) ? comp.SerieCte.PadLeft(3, '0') : string.Empty%>-<%= !string.IsNullOrEmpty(comp.NumeroCte) ? comp.NumeroCte : string.Empty%>
                        </td>
                        <td>
                            <%=comp.Notas.Count > 0 ? string.Concat(comp.Notas[0].SerieNota.PadLeft(3, '0'), "-", comp.Notas[0].NumeroNota) : string.Empty%>
                        </td>
                        <td>
                            <%=comp.Notas.Count > 0 ? comp.Notas[0].DataNfe.ToShortDateString() : string.Empty %>
                        </td>
                        <td>
                            <%=comp.Notas.Count > 0 ? comp.Notas[0].PesoTotalNF.ToString("N3") : string.Empty %>
                        </td>
                        <td>
                            <%=comp.Notas.Count > 0 ? comp.Notas[0].PesoRateio.ToString("N3") : string.Empty %>
                        </td>
                        <td>
                            <%=comp.PesoVagao.ToString("N3")%>
                        </td>
                        <td>
                            <%= string.IsNullOrEmpty(comp.Notas[0].Produto) ? comp.Mercadoria : comp.Notas[0].Produto %>
                        </td>
                        <td>
                            <%=comp.LocalCarregamento%>
                        </td>
                        <td>
                            <%=comp.DataCarregamento.ToShortDateString()%>
                        </td>
                        <td>
                            <%=comp.EmpresaOrigem%>
                        </td>
                        <td>
                            <%=comp.EmpresaDestino%>
                        </td>
                    </tr>
                    <% if (comp.Notas.Count > 1)
                       {
                           for (int j = 1; j < comp.Notas.Count; j++)
                           {
                    %>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            <%=comp.Notas.Count > 0 ? string.Concat(comp.Notas[j].SerieNota.PadLeft(3, '0'), "-", comp.Notas[j].NumeroNota) : string.Empty%>
                        </td>
                        <td>
                            <%=comp.Notas.Count > 0 ? comp.Notas[0].DataNfe.ToShortDateString() : string.Empty %>
                        </td>
                        <td>
                            <%=comp.Notas.Count > 0 ? comp.Notas[j].PesoTotalNF.ToString("N3") : string.Empty %>
                        </td>
                        <td>
                            <%=comp.Notas.Count > 0 ? comp.Notas[j].PesoRateio.ToString("N3") : string.Empty %>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <% }
                       } %>
                    <% } %>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table style="width: 100%; font-family: Arial; font-size: 10px">
                    <tr>
                        <td colspan="3">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Total de Vagões
                        </td>
                        <td>
                            <%=mdfeComposicao.Count%>
                        </td>
                        <td style="border-style: solid; border-width: 0px 0px 1px 0px;">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Peso Total
                        </td>
                        <td>
                            <%=mdfeComposicao.Sum(x=> x.PesoVagao).ToString("N3")%>
                        </td>
                        <td style="text-align: center;">
                            Assinatura do Agente
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td style="border-style: solid; border-width: 0px 0px 1px 0px;" colspan="3">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            Recebemos este Aviso em ___/___/________ às ___:___ Horas.
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            &nbsp;
                        </td>
                        <td style="border-style: solid; border-width: 0px 0px 1px 0px;">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            &nbsp;
                        </td>
                        <td style="text-align: center;">
                            Destinatário ou Preposto
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
