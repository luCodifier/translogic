﻿<%@ Import Namespace="System.Security.Policy" %>
<%@ Import Namespace="Translogic.Modules.Core.Domain.Model.Diversos.Mdfe" %>

<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Interna.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Import Namespace="Translogic.Modules.Core.Domain.Model.Trem" %>
<asp:Content runat="server" ID="HeadContent" ContentPlaceHolderID="HeadContent">
    <script type="text/javascript" language="javascript">
          
          <%
                Trem trem = (Trem)ViewData["Trem"]; 
            %>
        /******************************* STORE *******************************/

        var storeDetalhe = new Ext.data.JsonStore({
            root: "Items",
            autoLoad: false,
            url: '<%= Url.Action("DetalhesComposicao", new { idTrem = ViewData["IdTrem"], idEmpresa = ViewData["EMPRESA"] } ) %>',
            fields: [
                'Id',
                'Sequencia',
                'IdVagao',
                'CodigoVagao',
                'SerieCte',
                'SituacaoCte',
                'NotasCte',
                'NumeroCte'
            ]
        });

        /************************* GRID **************************/
         

    function statusRendererCte(val)
      {
        switch(val){
            case "AUT":
                return "<img src='<%=Url.Images("Icons/tick.png") %>' alt='CTe Aprovado'>";
                break;
						case "AGI":
						case "AGC":
                return "<img src='<%=Url.Images("Icons/application_go.png") %>' alt='Aguardando Cancelamento / Inutilização'>";
                break;
            case "EAE":
                return "<img src='<%=Url.Images("Icons/email_go.png") %>' alt='Enviado arquivo CTe para Sefaz.'>";
                break;
            case "EAR":
                return "<img src='<%=Url.Images("Icons/arrow_redo_yellow.png") %>' alt='Erro Autorizado reenvio do CTe.'>";
                break;            
            case "CAN":
                return "<img src='<%=Url.Images("Icons/cancel.png") %>' alt='CTe Cancelado '>";
                break;                        
             case "INV":
                return "<img src='<%=Url.Images("Icons/delete.png") %>' alt='CTe Invalidado'>";
                break;                        
             case "AGC":
                return "<img src='<%=Url.Images("Icons/lock_edit.png") %>' alt='Aguardando cancelamento.'>";
                break;                                    
             case "AUC":
                return "<img src='<%=Url.Images("Icons/lock_open.png") %>' alt='Autorizado cancelamento.'>";
                break;
             case "ERR":
                return "<img src='<%=Url.Images("Icons/cross.png") %>' alt='Erro na geração do CTe.'>";
                break; 
			case "PCC":
                return "<img src='<%=Url.Images("Icons/information.png") %>' alt='Cte aguardando a geração automática da numeração / chave.'>";
                break;
            case "":
                return "";
                break; 	           
            default:
                return "<img src='<%=Url.Images("Icons/alert.png") %>' alt='Cte em processamento.'>";
                break;
        }
      }      

       sm = new Ext.grid.CheckboxSelectionModel();
        
       var gridComposicaoDetalhe = new Ext.grid.GridPanel({
        id: 'grid',
        name: 'grid',
        autoLoadGrid: false,
        height: 300,
        width: 735,
        sm: sm,
		viewConfig: {
            forceFit: true
        },
        stripeRows: true,
        region: 'center',
        
        store: storeDetalhe,
        loadMask: { msg: App.Resources.Web.Carregando },
        colModel: new Ext.grid.ColumnModel({
            defaults: {
                sortable: false
            },
             columns: [
                sm, 
                { header: 'Seq.', width: 40, dataIndex: "Sequencia", sortable: false },
                { header: 'Cód. Vg.', width: 80, dataIndex: "CodigoVagao", sortable: false },
                { header: 'Ser. CTE', width: 80, dataIndex: "SerieCte", sortable: false },
                { header: 'Nr. CTE', width: 80, dataIndex: "NumeroCte", sortable: false },
                { header: '...',width: 25, dataIndex: "SituacaoCte",   sortable: false,renderer:statusRendererCte },
                { header: 'Notas',width: 200, dataIndex: "NotasCte",   sortable: false}
             ]
        })           
    });
   
       /************************* FILTROS **************************/
        var txtFiltroDataPartida = {
            xtype: 'textfield',
            fieldLabel: 'Data Partida',
            id: 'txtFiltroDataPartida',
            name: 'txtFiltroDataPartida',
            width: 120,
            allowBlank: false,
            readOnly:true,
            enableKeyEvents: true,
            listeners: {
                specialkey: TratarTeclaEnter
            }
        };

        var txtFiltroDataChegada = {
            xtype: 'textfield',
            fieldLabel: 'Data Chegada',
            id: 'txtFiltroDataChegada',
            name: 'txtFiltroDataChegada',
            width: 120,
            allowBlank: false,
            readOnly:true,
            enableKeyEvents: true,
            listeners: {
                specialkey: TratarTeclaEnter
            }
        };

        var txtFiltroOS = {
            xtype: 'textfield',
            autoCreate: { tag: 'input', type: 'text', maxlength: '10', autocomplete: 'off' },
            width: 70,
            maskRe: /[0-9]/,
            name: 'txtFiltroOS',
            allowBlank: true,
            id: 'txtFiltroOS',
            fieldLabel: 'OS',
            enableKeyEvents: true,
            readOnly:true,
            listeners: {
                specialkey: TratarTeclaEnter
            }
        };

        var txtFiltroPrefixo = {
            xtype: 'textfield',
            style: 'text-transform: uppercase',
            autoCreate: { tag: 'input', type: 'text', maxlength: '3', autocomplete: 'off' },
            maskRe: /[a-zA-Z0-9]/,
            width: 60,
            name: 'txtFiltroPrefixo',
            allowBlank: true,
            id: 'txtFiltroPrefixo',
            fieldLabel: 'Prefixo',
            enableKeyEvents: true,
             readOnly:true,
            listeners: {
                specialkey: TratarTeclaEnter
            }
        };

        var origem = {
            xtype: 'textfield',
            vtype: 'cteestacaovtype',
            style: 'text-transform: uppercase',
            id: 'filtro-Ori',
            fieldLabel: 'Origem',
            autoCreate: { tag: 'input', type: 'text', maxlength: '3', autocomplete: 'off' },
            name: 'Origem',
            allowBlank: true,
            maxLength: 3,
            width: 35,
            readOnly:true,
            hiddenName: 'Origem'
        };

        var destino = {
            xtype: 'textfield',
            vtype: 'cteestacaovtype',
            style: 'text-transform: uppercase',
            id: 'filtro-Dest',
            fieldLabel: 'Destino',
            autoCreate: { tag: 'input', type: 'text', maxlength: '3', autocomplete: 'off' },
            name: 'Destino',
            allowBlank: true,
            maxLength: 3,
            width: 35,
            readOnly: true,
            hiddenName: 'Destino'
        };
        
        var coluna1Linha1 = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding-right:10px',
            items: [txtFiltroPrefixo]
        };

        var coluna2Linha1 = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding-right:10px',
            items: [txtFiltroOS]
        };

        var coluna3Linha1 = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding-right:10px',
            items: [origem]
        };

        var coluna4Linha1 = {
            layout: 'form',
            border: false,
            items: [destino]
        };

        var coluna5Linha1 = {
            layout: 'form',
            border: false,
            width: 130,
            items: [txtFiltroDataPartida]
        };

        var coluna6Linha1 = {
            layout: 'form',
            border: false,
            width: 130,
            items: [txtFiltroDataChegada]
        };

        var linha1 = {
            layout: 'column',
            border: false,
            items: [coluna1Linha1, coluna2Linha1, coluna3Linha1, coluna4Linha1, coluna5Linha1, coluna6Linha1]
        };
       
        var btnExportar = {
            name: 'btnExportar',
            id: 'btnExportar',
            text: 'Exportar',
            handler: function () {
                if (gridComposicaoDetalhe.getSelectionModel().hasSelection()) {
                    var selected = gridComposicaoDetalhe.getSelectionModel().getSelections();
                    var listaEnvio = new Array();
	                for (var i = 0; i < selected.length; i++) {
			                listaEnvio.push(selected[i].data.IdVagao);
	                }

                    var url = "<%= Url.Action("ExportarRelatorioEntrega") %>";

                    url += "?idTrem=<%=ViewData["IdTrem"] %>";
                    url += "&idEmpresa=<%=ViewData["EMPRESA"] %>";
                    url += "&idsVagao="+ listaEnvio;

                    window.open(url, "");
                } else {
                     Ext.Msg.show({
       	                            title: "Mensagem de Informação",
       	                            msg: "Favor selecionar pelo menos um vagão.",
       	                            buttons: Ext.Msg.OK,
       	                            minWidth: 200
       	                        });
                }
            }
        };

        var filtros = new Ext.form.FormPanel({
            id: 'filtros',
            title: 'Filtros',
            name: 'filtros',
            bodyStyle: 'padding: 10px',
            width: 735,
            labelAlign: 'top',
            items: [linha1],
            buttons: [btnExportar],
            buttonAlign: 'center'
        });
        
        /************************* FUNCOES DE APOIO **************************/
        function TratarTeclaEnter(f, e) {
            if (e.getKey() == e.ENTER) {
                CarregarGrid();
            }
        }
        
        function CarregarGrid() {
            gridComposicaoDetalhe.getStore().load();
        }
        
        function CarregarDadosTrem() {
            var dtPartida = '<%=trem.DataRealizadaPartida %>';
            var dtChegada = '<%=trem.DataRealizadaChegada %>';
            var origem = '<%=trem.Origem.Codigo %>';
            var destino = '<%=trem.Destino.Codigo %>';
            var prefixo = '<%=trem.Prefixo %>';
            var osNumero = '<%=trem.OrdemServico.Numero %>';

            Ext.getCmp('txtFiltroDataPartida').setValue(dtPartida);
            Ext.getCmp('txtFiltroDataChegada').setValue(dtChegada);
            Ext.getCmp('filtro-Ori').setValue(origem);
            Ext.getCmp('filtro-Dest').setValue(destino);
            Ext.getCmp('txtFiltroPrefixo').setValue(prefixo);
            Ext.getCmp('txtFiltroOS').setValue(osNumero);
        }
        
        Ext.onReady(function () {
           filtros.render(document.body);
            gridComposicaoDetalhe.render(document.body);
            CarregarGrid();
            CarregarDadosTrem();
        });
    </script>
</asp:Content>
<asp:Content runat="server" ID="Content2" ContentPlaceHolderID="Content">
    <div id="header-content">
        <h1>
            Fluxos Transportados - Vagões
        </h1>
        <small>Você está em MDFe > Aviso de chegada</small>
        <br />
    </div>
</asp:Content>
