﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Interna.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content runat="server" ID="HeadContent" ContentPlaceHolderID="HeadContent">
    <script type="text/javascript">

        var formModal = null;

        var gridStore = new Ext.data.JsonStore({
            root: "Items",
            totalProperty: 'Total',
            id: 'grid',
            name: 'grid',
            proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("PesquisaTolerancias","ToleranciaMercadoria") %>', timeout: 600000 }),
            paramNames: {
                sort: "detalhesPaginacaoWeb.Sort",
                dir: "detalhesPaginacaoWeb.Dir",
                start: "detalhesPaginacaoWeb.Start",
                limit: "detalhesPaginacaoWeb.Limit"
            },
            fields: ['Id', 'IdMercadoria', 'Mercadoria', 'Tolerancia', 'Data', 'Usuario']
        });

        var ddlMercadoriaStore = new Ext.data.JsonStore({
            root: "Items",
            autoLoad: true,
            proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("ObterMercadorias", "ToleranciaMercadoria") %>', timeout: 600000 }),
            id: 'ddlMercadoriaStore',
            fields: ['IdMercadoria', 'Nome']
        });

        $(function () {

            var pagingToolbar = new Ext.PagingToolbar({
                pageSize: 50,
                store: gridStore,
                displayInfo: true,
                displayMsg: App.Resources.Web.GridExibindoRegistros,
                emptyMsg: App.Resources.Web.GridSemRegistros,
                paramNames: {
                    start: "detalhesPaginacaoWeb.Start",
                    limit: "detalhesPaginacaoWeb.Limit"
                }
            });

            var Actions = new Ext.ux.grid.RowActions({
                id: '',
                dataIndex: '',
                header: '',
                align: 'center',
                autoWidth: false,
                width: 10,
                actions: [
                    { iconCls: 'icon-edit', tooltip: 'Editar' },
                    { iconCls: 'icon-del', tooltip: 'Excluir' }
                ],
                callbacks: {
                    'icon-del': function (grid, record, action, row, col) {
                        remover(record);
                    },
                    'icon-edit': function (grid, record, action, row, col) {
                        editar(record);
                    }
                }
            });

            var cm = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true
                },
                plugins: Actions,
                columns: [
                    new Ext.grid.RowNumberer(),
                    Actions,
                    { header: 'Id', dataIndex: "Id", sortable: false, width: 10, hidden: true },
                    { header: 'IdMercadoria', dataIndex: "IdMercadoria", sortable: false, width: 10, hidden: true },
                    { header: 'Mercadoria', dataIndex: "Mercadoria", sortable: false, width: 20 },
                    { header: 'Tolerância', dataIndex: "Tolerancia", sortable: false, width: 20 },
                    { header: 'Data Cadastro', dataIndex: "Data", sortable: false, width: 30 },
                    { header: 'Usuário', dataIndex: "Usuario", sortable: false, width: 27 }
                ]
            });

            var grid = new Ext.grid.EditorGridPanel({
                id: 'grid',
                name: 'grid',
                autoLoadGrid: false,
                height: 360,
                limit: 10,
                width: 870,
                stripeRows: true,
                cm: cm,
                region: 'center',
                viewConfig: {
                    forceFit: true,
                    emptyText: 'Não possui dado(s) para exibição.'
                },
                autoScroll: true,
                loadMask: { msg: App.Resources.Web.Carregando },
                store: gridStore,
                tbar: [
                    {
                        id: 'btnCadastrar',
                        text: 'Cadastrar',
                        tooltip: 'Exportar',
                        iconCls: 'icon-save',
                        handler: function (c) {
                            cadastrar();
                        }
                    }
                ],
                bbar: pagingToolbar,
                plugins: [Actions]
            });

            grid.getStore().proxy.on('beforeload', function (p, params) {
                params['dataInicial'] = Ext.getCmp("txtDataInicial").getRawValue();
                params['dataFinal'] = Ext.getCmp("txtDataFinal").getRawValue();
                params['mercadoria'] = Ext.getCmp("ddlMercadoria").getValue();
            });

            var txtDataInicial = new Ext.form.DateField({
                fieldLabel: 'Data Inicial',
                id: 'txtDataInicial',
                name: 'txtDataInicial',
                dateFormat: 'd/n/Y',
                width: 83,
                plugins: [new Ext.ux.InputTextMask('99/99/9999', true)]

            });

            var txtDataFinal = new Ext.form.DateField({
                fieldLabel: 'Data Final',
                id: 'txtDataFinal',
                name: 'txtDataFinal',
                dateFormat: 'd/n/Y',
                width: 83,
                plugins: [new Ext.ux.InputTextMask('99/99/9999', true)]
            });

            var ddlMercadoria = new Ext.form.ComboBox({
                editable: true,
                typeAhead: true,
                forceSelection: true,
                disableKeyFilter: true,
                triggerAction: 'all',
                lazyRender: true,
                mode: 'local',
                valueField: 'IdMercadoria',
                displayField: 'Nome',
                fieldLabel: 'Mercadoria',
                id: 'ddlMercadoria',
                name: 'ddlMercadoria',
                store: ddlMercadoriaStore,
                value: 'Todos'
            });

            var formDataInicial = {
                width: 100,
                layout: 'form',
                border: false,
                items: [txtDataInicial]
            };

            var formDataFinal = {
                width: 100,
                layout: 'form',
                border: false,
                items: [txtDataFinal]
            };

            var formMercadoria = {
                layout: 'form',
                border: false,
                items: [ddlMercadoria]
            };

            var colFiltros = {
                layout: 'column',
                border: false,
                items: [formDataInicial, formDataFinal, formMercadoria]
            };

            var filtros = new Ext.form.FormPanel({
                id: 'filtros',
                title: "Filtros",
                region: 'center',
                bodyStyle: 'padding:15px;',
                layout: 'form',
                labelAlign: 'top',
                items: [colFiltros],
                buttonAlign: "center",
                buttons:
                    [{
                        text: 'Pesquisar',
                        id: 'btnPesquisar',
                        type: 'submit',
                        cls: 'clsBtnPesquisar',
                        iconCls: 'icon-find',
                        handler: function (b, e) {
                            pesquisar();
                        }
                    },
                    {
                        text: 'Limpar',
                        id: 'btnLimpar',
                        type: 'submit',
                        handler: function (b, e) {
                            limpar();
                        }
                    }]
            });

            new Ext.Viewport({
                layout: 'border',
                margins: 10,
                items: [
                    {
                        region: 'north',
                        height: 200,
                        autoScroll: true,
                        items: [{
                            region: 'center',
                            applyTo: 'header-content'
                        },
                            filtros]
                    },
                    grid
                ]
            });
        });

        function pesquisar() {
            if (validarFiltros()) {
                gridStore.load({ params: { start: 0, limit: 50 } });
            }
        };



        function validarFiltros() {

            let dtInicial = null;
            let dtFinal = null;

            //Converte data inicial para formato MM/DD/YYYY para controller conseguir identificar
            let dateParts = (Ext.getCmp("txtDataInicial").getRawValue()).split("/");
            if (dateParts.length > 1) {
                dtInicial = dateParts[1] + "/" + dateParts[0] + "/" + dateParts[2];
            } else {
                dtInicial = null;
            }

            //Converte data final para formato MM/DD/YYYY para controller conseguir identificar
            dateParts = (Ext.getCmp("txtDataFinal").getRawValue()).split("/");
            if (dateParts.length > 1) {
                dtFinal = dateParts[1] + "/" + dateParts[0] + "/" + dateParts[2];
            } else {
                dtFinal = null;
            }

            if (dtInicial == null || dtFinal == null) {
                alerta("Ambos os filtros de Data são obrigatórios");
                return false;
            }
            return true;
        };

        function limpar() {
            var grid = Ext.getCmp("grid");

            grid.getStore().removeAll();

            Ext.getCmp("filtros").getForm().reset();

            colocarHojeEmDatas();
        };

        function comparaDatas(dataIni, dataFim) {

            var arrDtIni = dataIni.split('/');
            var arrDtFim = dataFim.split('/');

            var dtIni = new Date(arrDtIni[2] + '/' + arrDtIni[1] + '/' + arrDtIni[0]);
            var dtFim = new Date(arrDtFim[2] + '/' + arrDtFim[1] + '/' + arrDtFim[0]);

            return dtIni <= dtFim;
        }

        function colocarHojeEmDatas() {

            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();

            if (dd < 10) {
                dd = '0' + dd
            }

            if (mm < 10) {
                mm = '0' + mm
            }

            today = dd + '/' + mm + '/' + yyyy;

            Ext.getCmp("txtDataInicial").setValue(today);
            Ext.getCmp("txtDataFinal").setValue(today);
        }

        function cadastrar() {
            modal('Cadastrar');
        }

        function editar(record) {
            modal('Editar', record.data.Id, record.data.IdMercadoria, record.data.Tolerancia);
        }

        function remover(record) {
            modal('Excluir', record.data.Id, record.data.IdMercadoria, record.data.Tolerancia);
        }

        function alerta(message) {
            Ext.Msg.show({
                title: 'Aviso',
                msg: message,
                buttons: Ext.Msg.OK,
                icon: Ext.MessageBox.WARNING
            });
        }

        function modal(evento, id, idMercadoria, tolerancia) {

            title = evento + " Tolerância";

            formModal = new Ext.Window({
                id: 'formModal',
                title: title,
                modal: true,
                width: 420,
                height: 150,
                resizable: false,
                autoScroll: false,
                autoLoad:
                {
                    url: '<%=Url.Action("Formulario")%>',
                    params: { id: id, idMercadoria: idMercadoria, tolerancia: tolerancia, evento: evento },
                    text: "Abrindo Cadastro...",
                    scripts: true,
                    callback: function (el, sucess) {
                        if (!sucess) {
                            formModal.close();
                        }
                    }
                },
                listeners: {
                    resize: function (win, width, height, eOpts) {
                        win.center();
                    }
                }
            });
            window.wformModal = formModal;
            formModal.show(this);
        }

        Ext.onReady(function () {
            $("#txtDataInicial").val("__/__/____");
            $("#txtDataFinal").val("__/__/____");

            colocarHojeEmDatas();
        });

    </script>
</asp:Content>
<asp:Content runat="server" ID="Content2" ContentPlaceHolderID="Content">
    <div id="header-content">
        <h1>Cadastro de Tolerância por mercadoria</h1>
        <small>Você está em Operação > Cadastro de Tolerância por mercadoria</small>
        <br />
    </div>

</asp:Content>
