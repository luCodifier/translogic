﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>

<div id="div-cadastro">
</div>

 <script type="text/javascript">

     var idEditar = '<%=ViewData["id"] %>';    
     var idMercadoria = '<%=ViewData["idMercadoria"] %>';
     var tolerancia = '<%=ViewData["tolerancia"] %>';
     var evento = '<%=ViewData["evento"] %>';

     if (!idMercadoria)
         idMercadoria = 'Todos';
    
    

     if (evento == "Cadastrar")
         evento = "Salvar";


    /**************
    ******STORE****/
   <%--  var ddlMercadoriaStore = new Ext.data.JsonStore({
         root: "Items",
         autoLoad: true,
         proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("ObterMercadorias", "ToleranciaMercadoria") %>', timeout: 600000 }),
            id: 'ddlMercadoriaStore',
            fields: ['IdMercadoria', 'Nome']
        });--%>

     /**************
     ****CAMPOS****/

     var hdnId = { xtype: 'hidden', id: 'hdnId', name: 'hdnId', value: idEditar };
     var hdnEvento = { xtype: 'hidden', id: 'evento', name: 'evento', value: evento };

     var ddlMercadoriaFormulario = new Ext.form.ComboBox({
         editable: true,
         typeAhead: true,
         disabled: evento != "Salvar",
         forceSelection: true,
         disableKeyFilter: true,
         triggerAction: 'all',
         lazyRender: true,
         mode: 'local',
         valueField: 'IdMercadoria',
         displayField: 'Nome',
         fieldLabel: 'Mercadoria',
         id: 'ddlMercadoriaFormulario',
         name: 'ddlMercadoriaFormulario',
         store: ddlMercadoriaStore,         
         width: 210
     });

     //var txtTolerancia = {
     //    xtype: 'textfield',
     //    name: 'txtTolerancia',
     //    id: 'txtTolerancia',
     //    fieldLabel: 'Tolerância (Ton.)',
     //    disabled: evento == "Excluir",
     //    autoCreate: {
     //        tag: 'input',
     //        type: 'text',
     //        autocomplete: 'off',
     //        maxlength: '8'             
     //    },
     //    maskRe: /[0-9+;]/,
     //    width: 50
     //};



     var txtTolerancia = {
         xtype: 'numberfield',
         name: 'txtTolerancia',
         id: 'txtTolerancia',
         fieldLabel: 'Tolerância (Ton.)',
         allowDecimals: true,
         disabled: evento == "Excluir",
         decimalSeparator: ',',
         autoCreate: {
             tag: 'input',
             type: 'text',
             autocomplete: 'off',
             maxlength: '8'
         },
         width: 90,
         enableKeyEvents: true,
         listeners: {
             keydown: function (field, e) {
                 if (e.ctrlKey) {
                     e.stopEvent();
                 }
             }
         }        
     };

     var formMercadoriaFormulario = {
         layout: 'form',
         border: false,
         width: 220,
         items: [ddlMercadoriaFormulario]
     };

     var formTolerancia = {
         layout: 'form',
         border: false,
         width: 100,
         items: [txtTolerancia]
     };

     var colFiltrosFormulario = {
         layout: 'column',
         border: false,
         items: [formMercadoriaFormulario, formTolerancia]
     };

     var formCadastro = new Ext.form.FormPanel({
         id: 'formCadastro',
         name: 'formCadastro',
         width: 450,
         border: true,
         autoScroll: true,
         url: '<%= Url.Action("ConfirmaFormulario", "ToleranciaMercadoria") %>',
         standardSubmit: true,
         bodyStyle: 'padding: 15px',
         labelAlign: 'top',
         items:
		[
                 hdnId, colFiltrosFormulario
		],
         buttonAlign: "left",
         buttons: [
            {
                text: evento,
                type: 'submit',
                formBind: true,
                handler: function (b, e) {

                     var ddlMercadoria = Ext.getCmp("ddlMercadoriaFormulario").getValue();
                    var txtTolerancia = Ext.getCmp("txtTolerancia").getValue();

                    console.log(txtTolerancia);
                    console.log(txtTolerancia.toString().replace('.', ','));

                     if (ddlMercadoria == "Todos") {
                         Ext.Msg.show({
                             title: 'Aviso',
                             msg: 'Ambos os campos são obrigatórios.',
                             buttons: Ext.Msg.OK,
                             icon: Ext.MessageBox.WARNING
                         });
                     }

                     if (txtTolerancia == "" || txtTolerancia == null) {
                         Ext.Msg.show({
                             title: 'Aviso',
                             msg: 'Ambos os campos são obrigatórios.',
                             buttons: Ext.Msg.OK,
                             icon: Ext.MessageBox.WARNING
                         });
                     }
                   
                     if (ddlMercadoria && txtTolerancia) {

                         var payload = { "Id": idEditar, "IdMercadoria": ddlMercadoria, "Tolerancia": txtTolerancia.toString().replace('.', ','), "Evento": evento };

                         console.log(payload);

                         $.ajax({
                             url: '<%= Url.Action("ConfirmaFormulario", "ToleranciaMercadoria") %>',
                             type: "POST",
                             data: payload,
                             success: function (response) {
                                 if (response) {
                                     Ext.Msg.show({
                                         title: 'Aviso',
                                         msg: 'Alterações salvas com sucesso!',
                                         buttons: Ext.Msg.OK,
                                         icon: Ext.MessageBox.INFO
                                     });

                                     Ext.getCmp("formModal").close();
                                     pesquisar();
                                 }
                             },
                             failure: function (response, options) {
                                 Ext.Msg.show({
                                     title: 'Aviso',
                                     msg: response.Message,
                                     buttons: Ext.Msg.OK,
                                     icon: Ext.MessageBox.ERROR
                                 });
                             }
                         });

                         //formCadastro.getForm().submit();
                     }
                 }
                
            }
        ]
     });

    /******************************* RENDER *******************************/
    Ext.onReady(function () {

        formCadastro.render("div-cadastro");

        if (idMercadoria > 0)
            Ext.getCmp('ddlMercadoriaFormulario').setValue(idMercadoria);
        
        if (tolerancia != "" && tolerancia != null) {
            Ext.getCmp('txtTolerancia').setValue(tolerancia);
        }
    });

     $(function () {
         console.log('aqui 1');
     });


 </script>
