﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>

<div id="div-cadastro">
</div>

<script type="text/javascript">

    var idLimite = '<%=ViewData["idLimite"] %>';
    var idGrupo = '<%=ViewData["idGrupo"] %>';
    var idFrota = '<%=ViewData["idFrota"] %>';
    var idMercadoria = '<%=ViewData["idMercadoria"] %>';
    var peso = '<%=ViewData["peso"] %>';
    var evento = '<%=ViewData["evento"] %>';

    var ddlGrupoRotaStoreLimite = new Ext.data.JsonStore({
        root: "Items",
        proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("ObterGrupoRotas", "Rota") %>', timeout: 600000 }),
        id: 'ddlGrupoRotaStoreLimite',
        fields: ['IdGrupo', 'Grupo'],
        autoLoad: {
            callback: function () {

                if (idGrupo)
                    Ext.getCmp("ddlGrupoRotaLimite").setValue(idGrupo);
                else
                    Ext.getCmp("ddlGrupoRotaLimite").setValue("Todos");
            }
        }
    });


    var ddlFrotaStoreLimiteForm = new Ext.data.JsonStore({
        root: "Items",
        proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("ObtemFrotas", "Rota") %>', timeout: 600000 }),
        id: 'ddlFrotaStoreLimiteForm',
        fields: ['IdFrota', 'Descricao'],
        autoLoad: {
            callback: function () {
                  
                    if (idFrota)
                        Ext.getCmp("ddlFrotaLimite").setValue(idFrota);
                    else
                        Ext.getCmp("ddlFrotaLimite").setValue("Todos");
            }                
        }
    });

    var ddlMercadoriaStoreLimiteForm = new Ext.data.JsonStore({
        root: "Items",
        proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("ObterMercadorias", "Rota") %>', timeout: 600000 }),
        id: 'ddlMercadoriaStoreLimiteForm',
        fields: ['IdMercadoria', 'Nome'],
         autoLoad: {
            callback: function () {

                 if (idMercadoria)
                    Ext.getCmp("ddlMercadoriaLimiteForm").setValue(idMercadoria);
                else
                    Ext.getCmp("ddlMercadoriaLimiteForm").setValue("Todos");
            }
        }
    });

    var myComboStores = ['ddlGrupoRotaStoreLimite', 'ddlFrotaStoreLimiteForm', 'ddlMercadoriaStoreLimiteForm']

    var labelCarregandoLimite = {
        xtype: 'label',
        id: 'labelCarregandoLimite',
        name: 'labelCarregandoLimite',
        text: 'Carregando ...'
    };

    var ddlGrupoRotaLimite = new Ext.form.ComboBox({
        editable: true,
        typeAhead: true,
        forceSelection: true,
        disableKeyFilter: true,
        triggerAction: 'all',
        lazyRender: true,
        mode: 'local',
        valueField: 'IdGrupo',
        displayField: 'Grupo',
        fieldLabel: 'Grupo de Rotas',
        id: 'ddlGrupoRotaLimite',
        name: 'ddlGrupoRotaLimite',
        store: ddlGrupoRotaStoreLimite,
        width: 210,
        disabled: evento != "Salvar"
    });


    var formddlGrupoRotaLimite = {
        width: 220,
        layout: 'form',
        border: false,
        items: [ddlGrupoRotaLimite]
    };


    var ddlFrotaLimite = new Ext.form.ComboBox({
        editable: true,
        typeAhead: true,
        forceSelection: true,
        disableKeyFilter: true,
        triggerAction: 'all',
        lazyRender: true,
        mode: 'local',
        valueField: 'IdFrota',
        displayField: 'Descricao',
        fieldLabel: 'Frota',
        id: 'ddlFrotaLimite',
        name: 'ddlFrotaLimite',
        store: ddlFrotaStoreLimiteForm,
        width: 200,
        disabled: evento != "Salvar"
    });

    var formddlFrotaLimite = {
        width: 210,
        layout: 'form',
        border: false,
        items: [ddlFrotaLimite]
    };

    var ddlMercadoriaLimiteForm = new Ext.form.ComboBox({
        editable: true,
        typeAhead: true,
        forceSelection: true,
        disableKeyFilter: true,
        triggerAction: 'all',
        lazyRender: true,
        mode: 'local',
        valueField: 'IdMercadoria',
        displayField: 'Nome',
        fieldLabel: 'Mercadoria',
        id: 'ddlMercadoriaLimiteForm',
        name: 'ddlMercadoriaLimiteForm',
        store: ddlMercadoriaStoreLimiteForm,
        width: 200,
        disabled: evento != "Salvar"
    });

    var formddlMercadoriaLimite = {
        width: 210,
        layout: 'form',
        border: false,
        items: [ddlMercadoriaLimiteForm]
    };


    var txtPeso = {
        xtype: 'numberfield',
        name: 'txtPeso',
        id: 'txtPeso',
        fieldLabel: 'Peso Médio',
        allowDecimals: true,
        decimalSeparator: ',',
        autoCreate: {
            tag: 'input',
            type: 'text',
            autocomplete: 'off',
            maxlength: '8'
        },
        width: 60,
        enableKeyEvents: true,
        listeners: {
            keydown: function (field, e) {
                if (e.ctrlKey) {
                    e.stopEvent();
                }
            }
        },
        disabled: evento == "Excluir"
    };

    var formtxtPeso = {
        layout: 'form',
        border: false,
        width: 70,
        items: [txtPeso]
    };

    var colFiltrosFormularioLimite = {
        id: "colFiltrosFormularioLimite",
        layout: 'column',
        border: false,
        items: [formddlGrupoRotaLimite, formddlFrotaLimite, formddlMercadoriaLimite, formtxtPeso]
    };

    var formCadastroGrupo = new Ext.form.FormPanel({
        id: 'formCadastroLimite',
        name: 'formCadastroLimite',
        width: 750,
        border: true,
        autoScroll: true,
        url: '<%= Url.Action("ConfirmaFormularioLimite", "Rota") %>',
        standardSubmit: true,
        bodyStyle: 'padding: 15px',
        labelAlign: 'top',
        items:
            [
                labelCarregandoLimite, colFiltrosFormularioLimite
            ],
        buttonAlign: "left",
        buttons: [
            {
                text: evento,
                id: "btnSalvarLimite",
                type: 'submit',
                formBind: true,
                handler: function (b, e) {

                    var idEditando = idLimite > 0 ? idLimite : 0;
                    var idGrupoSalvar = Ext.getCmp("ddlGrupoRotaLimite").getValue();
                    var idFrotaSalvar = Ext.getCmp("ddlFrotaLimite").getValue();
                    debugger
                    var idMercadoriaoSalvar = Ext.getCmp("ddlMercadoriaLimiteForm").getValue();
                    var pesoSalvar = Ext.getCmp("txtPeso").getValue();

                    if (idGrupoSalvar == "" || idGrupoSalvar == null || idGrupoSalvar == "Todos") {
                        Ext.Msg.show({
                            title: 'Aviso',
                            msg: 'O campo Grupo é obrigatório.',
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.WARNING
                        });
                        return;
                    }

                    if (idFrotaSalvar == "" || idFrotaSalvar == null || idFrotaSalvar == "Todos") {
                        Ext.Msg.show({
                            title: 'Aviso',
                            msg: 'O campo Frota é obrigatório.',
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.WARNING
                        });
                        return;
                    }

                    if (idMercadoriaoSalvar == "" || idMercadoriaoSalvar == null || idMercadoriaoSalvar == "Todos") {
                        Ext.Msg.show({
                            title: 'Aviso',
                            msg: 'O campo Mercadoria é obrigatório.',
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.WARNING
                        });
                        return;
                    }

                    if (pesoSalvar == "" || pesoSalvar == null || pesoSalvar == "0") {
                        Ext.Msg.show({
                            title: 'Aviso',
                            msg: 'O campo Peso Médio é obrigatório.',
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.WARNING
                        });
                        return;
                    }

                    var payload = { "IdLimite": idEditando, "IdGrupo": idGrupoSalvar, "IdFrota": idFrotaSalvar, "IdMercadoria": idMercadoriaoSalvar, "Peso": pesoSalvar.toString().replace('.', ','), "evento": evento };

                    console.log(payload);

                    $.ajax({
                        url: '<%= Url.Action("ConfirmaFormularioLimite", "Rota") %>',
                         type: "POST",
                         data: payload,
                         success: function (response) {
                             if (response) {

                                 Ext.getCmp("formModalLimite").close();

                                 Ext.Msg.show({
                                     title: 'Aviso',
                                     msg: 'Alterações salvas com sucesso!',
                                     buttons: Ext.Msg.OK,
                                     icon: Ext.MessageBox.INFO
                                 });
                             }
                         },
                         failure: function (response, options) {
                             Ext.Msg.show({
                                 title: 'Aviso',
                                 msg: response.Message,
                                 buttons: Ext.Msg.OK,
                                 icon: Ext.MessageBox.ERROR
                             });
                         }
                     });
                }

            }
        ]
    });

    Ext.onReady(function () {
        formCadastroGrupo.render("div-cadastro");

        Ext.getCmp("btnSalvarLimite").setDisabled(true);

        setTimeout(function () {
            if (peso)
                Ext.getCmp("txtPeso").setValue(peso);

            Ext.getCmp("labelCarregandoLimite").setVisible(false);
            Ext.getCmp("btnSalvarLimite").setDisabled(false);

        }, 6000);
    });

</script>
