﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>

<script type="text/javascript">

    window.geral = (function (geral) {

        var AbaLimite = geral.AbaLimite || {};
        var formModalLimite = null;

        var gridStoreLimite = new Ext.data.JsonStore({
            root: "Items",
            totalProperty: 'Total',
            id: 'gridStoreLimite',
            name: 'gridStoreLimite',
            proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("BuscaLimites","Rota") %>', timeout: 600000 }),
            paramNames: {
                sort: "detalhesPaginacaoWeb.Sort",
                dir: "detalhesPaginacaoWeb.Dir",
                start: "detalhesPaginacaoWeb.Start",
                limit: "detalhesPaginacaoWeb.Limit"
            },
            fields: ['IdLimite', 'IdGrupo', 'Grupo', 'IdFrota', 'Frota','IdMercadoria', 'Mercadoria', 'Peso', 'Tolerancia', 'Data', 'Usuario']
        });

        var ddlGrupoRotaStore = new Ext.data.JsonStore({
            root: "Items",
            autoLoad: true,
            proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("ObterGrupoRotas", "Rota") %>', timeout: 600000 }),
            id: 'ddlGrupoRotaStore',
            fields: ['IdGrupo', 'Grupo']
        });

        var ddlFrotaStore = new Ext.data.JsonStore({
            root: "Items",
            autoLoad: true,
            proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("ObtemFrotas", "Rota") %>', timeout: 600000 }),
            id: 'dllFrotaStore',
             fields: ['IdFrota', 'Descricao']
        });

        var ddlMercadoriaStore = new Ext.data.JsonStore({
            root: "Items",
            autoLoad: true,
            proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("ObterMercadorias", "Rota") %>', timeout: 600000 }),
            id: 'ddlMercadoriaStore',
              fields: ['IdMercadoria', 'Nome']
          });

        (function () {
            
            var pagingToolbarLimite = new Ext.PagingToolbar({
                pageSize: 50,
                store: gridStoreLimite,
                displayInfo: true,
                displayMsg: App.Resources.Web.GridExibindoRegistros,
                emptyMsg: App.Resources.Web.GridSemRegistros,
                paramNames: {
                    start: "detalhesPaginacaoWeb.Start",
                    limit: "detalhesPaginacaoWeb.Limit"
                }
            });

            var ActionsLimite = new Ext.ux.grid.RowActions({
                id: '',
                dataIndex: '',
                header: '',
                align: 'center',
                autoWidth: false,
                width: 15,
                actions: [
                    { iconCls: 'icon-edit', tooltip: 'Editar' },
                    { iconCls: 'icon-del', tooltip: 'Excluir' }
                ],
                callbacks: {
                    'icon-del': function (grid, record, action, row, col) {
                        remover(record);
                    },
                    'icon-edit': function (grid, record, action, row, col) {
                        editar(record);
                    }
                }
            });

            var cmLimite = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true
                },
                plugins: ActionsLimite,
                columns: [
                    new Ext.grid.RowNumberer(),
                    ActionsLimite,
                    { header: 'IdLimite', dataIndex: "IdLimite", sortable: false, width: 10, hidden: true },
                    { header: 'IdGrupo', dataIndex: "IdGrupo", sortable: false, width: 10, hidden: true },
                    { header: 'Grupo Rotas', dataIndex: "Grupo", sortable: false, width: 70 },
                    { header: 'Frota', dataIndex: "Frota", sortable: false, width:70 },
                    { header: 'IdMercadoria', dataIndex: "IdGrupo", sortable: false, width: 10, hidden: true },
                    { header: 'Mercadoria', dataIndex: "Mercadoria", sortable: false, width: 70 },
                    { header: 'Peso Médio', dataIndex: "Peso", sortable: false, width: 30 },
                    { header: 'Tolerância', dataIndex: "Tolerancia", sortable: false, width: 30 },
                    { header: 'Data Cadastro', dataIndex: "Data", sortable: false, width: 30 },
                    { header: 'Usuário', dataIndex: "Usuario", sortable: false, width: 30 }
                ]
            });

            var gridLimite = new Ext.grid.EditorGridPanel({
                id: 'gridLimite',
                name: 'gridLimite',
                autoLoadGrid: false,
                height: 360,
                limit: 10,
                stripeRows: true,
                cm: cmLimite,
                region: 'center',
                viewConfig: {
                    forceFit: true,
                    emptyText: 'Não possui dado(s) para exibição.'
                },
                autoScroll: true,
                loadMask: { msg: App.Resources.Web.Carregando },
                store: gridStoreLimite,
                bbar: pagingToolbarLimite,
                plugins: [ActionsLimite],
                tbar: [
                    {
                        id: 'btnCadastrar',
                        text: 'Cadastrar',
                        tooltip: 'Cadastrar',
                        iconCls: 'icon-save',
                        handler: function (c) {
                            cadastrarLimite();
                        }
                    }
                ],
            });

            gridLimite.getStore().proxy.on('beforeload', function (p, params) {

                params['dataInicial'] = Ext.getCmp("txtDataInicialLimite").getRawValue();
                params['dataFinal'] = Ext.getCmp("txtDataFinalLimite").getRawValue();
                params['idGrupo'] = Ext.getCmp("ddlGrupoRotasLimite").getRawValue() == "Todos" ? "" : Ext.getCmp("ddlGrupoRotasLimite").getValue();
                params['frota'] = Ext.getCmp("ddlFrota").getRawValue() == "Todos" ? "" : Ext.getCmp("ddlFrota").getValue();
                params['idMercadoria'] = Ext.getCmp("ddlMercadoriaLimite").getRawValue() == "Todos" ? "" : Ext.getCmp("ddlMercadoriaLimite").getValue();
            });

            var txtDataInicialLimite = new Ext.form.DateField({
                fieldLabel: 'Data Inicial',
                id: 'txtDataInicialLimite',
                name: 'txtDataInicialLimite',
                dateFormat: 'd/n/Y',
                width: 83,
                plugins: [new Ext.ux.InputTextMask('99/99/9999', true)]

            });

            var formDataInicialLimite = {
                width: 100,
                layout: 'form',
                border: false,
                items: [txtDataInicialLimite]
            };

            var txtDataFinalLimite = new Ext.form.DateField({
                fieldLabel: 'Data Final',
                id: 'txtDataFinalLimite',
                name: 'txtDataFinalLimite',
                dateFormat: 'd/n/Y',
                width: 83,
                plugins: [new Ext.ux.InputTextMask('99/99/9999', true)]
            });

            var formDataFinalLimite = {
                width: 100,
                layout: 'form',
                border: false,
                items: [txtDataFinalLimite]
            };
           
            var ddlGrupoRotasLimite = new Ext.form.ComboBox({
                editable: true,
                typeAhead: true,
                forceSelection: true,
                disableKeyFilter: true,
                triggerAction: 'all',
                lazyRender: true,
                mode: 'local',
                valueField: 'IdGrupo',
                displayField: 'Grupo',
                fieldLabel: 'Grupo Rotas',
                id: 'ddlGrupoRotasLimite',
                name: 'ddlGrupoRotasLimite',
                store: ddlGrupoRotaStore,
                value: "Todos",
                width: 240
            });

            var formGrupoRotasLimite = {
                id: "formGrupoRotasLimite",
                width: 250,
                layout: 'form',
                border: false,
                items: [ddlGrupoRotasLimite]
            };

            var ddlFrota = new Ext.form.ComboBox({
                editable: true,
                typeAhead: true,
                forceSelection: true,
                disableKeyFilter: true,
                triggerAction: 'all',
                lazyRender: true,
                mode: 'local',
                valueField: 'IdFrota',
                displayField: 'Descricao',
                fieldLabel: 'Frota',
                id: 'ddlFrota',
                name: 'ddlFrota',
                store: ddlFrotaStore,
                value: "Todos",
                width: 240
            });

            var fromddlFrota = {
                width: 250,
                layout: 'form',
                border: false,
                items: [ddlFrota]
            };

          
            var ddlMercadoriaLimite = new Ext.form.ComboBox({
                editable: true,
                typeAhead: true,
                forceSelection: true,
                disableKeyFilter: true,
                triggerAction: 'all',
                lazyRender: true,
                mode: 'local',
                valueField: 'IdMercadoria',
                displayField: 'Nome',
                fieldLabel: 'Mercadoria',
                id: 'ddlMercadoriaLimite',
                name: 'ddlMercadoriaLimite',
                store: ddlMercadoriaStore,
                value: "Todos",
                width: 240
            });

            var formddlMercadoriaLimite = {
                width: 250,
                layout: 'form',
                border: false,
                items: [ddlMercadoriaLimite]
            };

            var colFiltrosLimite = {
                id: 'colFiltrosLimite',
                layout: 'column',
                border: false,
                items: [formDataInicialLimite, formDataFinalLimite, formGrupoRotasLimite, fromddlFrota, formddlMercadoriaLimite]
            };

            var filtrosLimite = new Ext.form.FormPanel({
                id: 'filtrosLimite',
                title: "Filtros",
                region: 'center',
                bodyStyle: 'padding:15px;',
                layout: 'form',
                labelAlign: 'top',
                items: [colFiltrosLimite],
                buttonAlign: "center",
                buttons:
                    [{
                        text: 'Pesquisar',
                        id: 'btnPesquisar',
                        type: 'submit',
                        cls: 'clsBtnPesquisar',
                        iconCls: 'icon-find',
                        handler: function (b, e) {
                            pesquisarLimite();
                        }
                    },
                    {
                        text: 'Limpar',
                        id: 'btnLimpar',
                        type: 'submit',
                        handler: function (b, e) {
                            limparLimite();
                        }
                    }]
            });

            var formPainelLimite = {
                layout: 'form',
                border: false,
                items: [filtrosLimite, gridLimite]
            };

            AbaLimite.getForm = function () {
                return formPainelLimite;
            }

        }());

        function pesquisarLimite() {
            if (validarFiltros()) {
                gridStoreLimite.load({ params: { start: 0, limit: 50 } });
            }
        };

        function validarFiltros() {

            let dtInicial = null;
            let dtFinal = null;

            //Converte data inicial para formato MM/DD/YYYY para controller conseguir identificar
            let dateParts = (Ext.getCmp("txtDataInicialLimite").getRawValue()).split("/");
            if (dateParts.length > 1) {
                dtInicial = dateParts[1] + "/" + dateParts[0] + "/" + dateParts[2];
            } else {
                dtInicial = null;
            }

            //Converte data final para formato MM/DD/YYYY para controller conseguir identificar
            dateParts = (Ext.getCmp("txtDataFinalLimite").getRawValue()).split("/");
            if (dateParts.length > 1) {
                dtFinal = dateParts[1] + "/" + dateParts[0] + "/" + dateParts[2];
            } else {
                dtFinal = null;
            }

            if (dtInicial == null || dtFinal == null) {
                alerta("Ambos os filtros de Data são obrigatórios");
                return false;
            }
            return true;
        };

        function limparLimite() {
            var gridLimite = Ext.getCmp("gridLimite");

            gridLimite.getStore().removeAll();

            Ext.getCmp("filtrosLimite").getForm().reset();

            colocarHojeEmDatasLimite();
        };

        function colocarHojeEmDatasLimite() {

            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();

            if (dd < 10) {
                dd = '0' + dd
            }

            if (mm < 10) {
                mm = '0' + mm
            }

            today = dd + '/' + mm + '/' + yyyy;

            Ext.getCmp("txtDataInicialLimite").setValue(today);
            Ext.getCmp("txtDataFinalLimite").setValue(today);
        }

        function cadastrarLimite() {
            modal('Salvar');
        }

        function remover(record) {
            modal('Excluir', record.data.IdLimite, record.data.IdGrupo, record.data.IdFrota, record.data.IdMercadoria, record.data.Peso);
        }

        function editar(record) {
            modal('Editar', record.data.IdLimite, record.data.IdGrupo, record.data.IdFrota, record.data.IdMercadoria, record.data.Peso);
        }

        function alerta(message) {
            Ext.Msg.show({
                title: 'Aviso',
                msg: message,
                buttons: Ext.Msg.OK,
                icon: Ext.MessageBox.WARNING
            });
        }

        function modal(evento, idLimite, idGrupo, idFrota, idMercadoria, peso) {

            formModalLimite = new Ext.Window({
                id: 'formModalLimite',
                title: "Associação Rota/Grupo",
                modal: true,
                width: 760,
                height: 150,
                resizable: false,
                autoScroll: false,
                autoLoad:
                {
                    url: '<%=Url.Action("FormularioLimite")%>',
                    params: { idLimite: idLimite, idGrupo: idGrupo, idFrota: idFrota, idMercadoria: idMercadoria, peso:peso, evento: evento },
                    text: "Abrindo Cadastro...",
                    scripts: true,
                    callback: function (el, sucess) {
                        if (!sucess) {
                            formModalLimite.close();
                        }
                    }
                },
                listeners: {
                    resize: function (win, width, height, eOpts) {
                        win.center();
                    },
                    'close': function (win) {
                        pesquisarLimite();
                    }
                }
            });
            window.wformModalLimite = formModalLimite;
            formModalLimite.show(this);
        };

        Ext.onReady(function () {
            $("#txtDataInicialLimite").val("__/__/____");
            $("#txtDataFinalLimite").val("__/__/____");

            colocarHojeEmDatasLimite();
        });

        geral.AbaLimite = AbaLimite;
        return geral;
    }(window.geral || {}));

</script>
