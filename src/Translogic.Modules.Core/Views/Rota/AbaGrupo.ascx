﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>

<script type="text/javascript">

    window.geral = (function (geral) {

        var AbaGrupo = geral.AbaGrupo || {};
        var formModalGrupoRota = null;

        var gridStoreGrupoRota = new Ext.data.JsonStore({
            root: "Items",
            totalProperty: 'Total',
            id: 'gridStoreGrupoRota',
            name: 'gridStoreGrupoRota',
            proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("BuscaGrupoRota","Rota") %>', timeout: 600000 }),
            paramNames: {
                sort: "detalhesPaginacaoWeb.Sort",
                dir: "detalhesPaginacaoWeb.Dir",
                start: "detalhesPaginacaoWeb.Start",
                limit: "detalhesPaginacaoWeb.Limit"
            },
            fields: ['IdGrupo', 'Grupo', 'Data', 'Usuario']
        });

        (function () {
            
            var pagingToolbarGrupoRota = new Ext.PagingToolbar({
                pageSize: 50,
                store: gridStoreGrupoRota,
                displayInfo: true,
                displayMsg: App.Resources.Web.GridExibindoRegistros,
                emptyMsg: App.Resources.Web.GridSemRegistros,
                paramNames: {
                    start: "detalhesPaginacaoWeb.Start",
                    limit: "detalhesPaginacaoWeb.Limit"
                }
            });

            var ActionsGrupoRota = new Ext.ux.grid.RowActions({
                id: '',
                dataIndex: '',
                header: '',
                align: 'center',
                autoWidth: false,
                width: 10,
                actions: [
                    { iconCls: 'icon-del', tooltip: 'Excluir' }
                ],
                callbacks: {
                    'icon-del': function (grid, record, action, row, col) {
                        remover(record);
                    }
                }
            });

            var cmGrupoRota = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true
                },
                plugins: ActionsGrupoRota,
                columns: [
                    new Ext.grid.RowNumberer(),
                    ActionsGrupoRota,
                    { header: 'Id', dataIndex: "IdGrupo", sortable: false, width: 10, hidden: true },
                    { header: 'Grupo Rotas', dataIndex: "Grupo", sortable: false, width: 100 },
                    { header: 'Data Cadastro', dataIndex: "Data", sortable: false, width: 30 },
                    { header: 'Usuário', dataIndex: "Usuario", sortable: false, width: 30 }
                ]
            });

            var gridGrupoRota = new Ext.grid.EditorGridPanel({
                id: 'gridGrupoRota',
                name: 'gridGrupoRota',
                autoLoadGrid: false,
                height: 360,
                limit: 10,
                stripeRows: true,
                cm: cmGrupoRota,
                region: 'center',
                viewConfig: {
                    forceFit: true,
                    emptyText: 'Não possui dado(s) para exibição.'
                },
                autoScroll: true,
                loadMask: { msg: App.Resources.Web.Carregando },
                store: gridStoreGrupoRota,
                tbar: [
                    {
                        id: 'btnCadastrar',
                        text: 'Cadastrar',
                        tooltip: 'Cadastrar',
                        iconCls: 'icon-save',
                        handler: function (c) {
                            cadastrarGrupoRota();
                        }
                    }
                ],
                bbar: pagingToolbarGrupoRota,
                plugins: [ActionsGrupoRota]
            });

            gridGrupoRota.getStore().proxy.on('beforeload', function (p, params) {
                params['dataInicial'] = Ext.getCmp("txtDataInicialGrupoRota").getRawValue();
                params['dataFinal'] = Ext.getCmp("txtDataFinalGrupoRota").getRawValue();
            });

            var txtDataInicialGrupoRotaGrupoRota = new Ext.form.DateField({
                fieldLabel: 'Data Inicial',
                id: 'txtDataInicialGrupoRota',
                name: 'txtDataInicialGrupoRota',
                dateFormat: 'd/n/Y',
                width: 83,
                plugins: [new Ext.ux.InputTextMask('99/99/9999', true)]

            });

            var txtDataFinalGrupoRotaGrupoRota = new Ext.form.DateField({
                fieldLabel: 'Data Final',
                id: 'txtDataFinalGrupoRota',
                name: 'txtDataFinalGrupoRota',
                dateFormat: 'd/n/Y',
                width: 83,
                plugins: [new Ext.ux.InputTextMask('99/99/9999', true)]
            });

            var formDataInicialGrupoRota = {
                width: 100,
                layout: 'form',
                border: false,
                items: [txtDataInicialGrupoRotaGrupoRota]
            };

            var formDataFinalGrupoRota = {
                width: 100,
                layout: 'form',
                border: false,
                items: [txtDataFinalGrupoRotaGrupoRota]
            };

            var colFiltrosGrupoRota = {
                layout: 'column',
                border: false,
                items: [formDataInicialGrupoRota, formDataFinalGrupoRota]
            };

            var filtrosGrupoRota = new Ext.form.FormPanel({
                id: 'filtrosGrupoRota',
                title: "Filtros",
                region: 'center',
                bodyStyle: 'padding:15px;',
                layout: 'form',
                labelAlign: 'top',
                items: [colFiltrosGrupoRota],
                buttonAlign: "center",
                buttons:
                    [{
                        text: 'Pesquisar',
                        id: 'btnPesquisar',
                        type: 'submit',
                        cls: 'clsBtnPesquisar',
                        iconCls: 'icon-find',
                        handler: function (b, e) {
                            pesquisarGrupoRota();
                        }
                    },
                    {
                        text: 'Limpar',
                        id: 'btnLimpar',
                        type: 'submit',
                        handler: function (b, e) {
                            limparGrupoRota();
                        }
                    }]
            });

            var formPainelGrupoRota = {
                layout: 'form',
                border: false,
                items: [filtrosGrupoRota, gridGrupoRota]
            };

            AbaGrupo.getForm = function () {
                return formPainelGrupoRota;
            }

        }());

        function pesquisarGrupoRota() {
            if (validarFiltros()) {
                gridStoreGrupoRota.load({ params: { start: 0, limit: 50 } });
            }
        };

        function validarFiltros() {

            let dtInicial = null;
            let dtFinal = null;

            //Converte data inicial para formato MM/DD/YYYY para controller conseguir identificar
            let dateParts = (Ext.getCmp("txtDataInicialGrupoRota").getRawValue()).split("/");
            if (dateParts.length > 1) {
                dtInicial = dateParts[1] + "/" + dateParts[0] + "/" + dateParts[2];
            } else {
                dtInicial = null;
            }

            //Converte data final para formato MM/DD/YYYY para controller conseguir identificar
            dateParts = (Ext.getCmp("txtDataFinalGrupoRota").getRawValue()).split("/");
            if (dateParts.length > 1) {
                dtFinal = dateParts[1] + "/" + dateParts[0] + "/" + dateParts[2];
            } else {
                dtFinal = null;
            }

            if (dtInicial == null || dtFinal == null) {
                alerta("Ambos os filtros de Data são obrigatórios");
                return false;
            }
            return true;
        };

        function limparGrupoRota() {
            var gridGrupoRota = Ext.getCmp("gridGrupoRota");

            gridGrupoRota.getStore().removeAll();

            Ext.getCmp("filtrosGrupoRota").getForm().reset();

            colocarHojeEmDatasGrupoRota();
        };

        function colocarHojeEmDatasGrupoRota() {

            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();

            if (dd < 10) {
                dd = '0' + dd
            }

            if (mm < 10) {
                mm = '0' + mm
            }

            today = dd + '/' + mm + '/' + yyyy;

            Ext.getCmp("txtDataInicialGrupoRota").setValue(today);
            Ext.getCmp("txtDataFinalGrupoRota").setValue(today);
        }

        function cadastrarGrupoRota() {
            modal('Salvar');
        }

        function remover(record) {
            debugger
            modal('Excluir', record.data.IdGrupo, record.data.Grupo);
        }

        function alerta(message) {
            Ext.Msg.show({
                title: 'Aviso',
                msg: message,
                buttons: Ext.Msg.OK,
                icon: Ext.MessageBox.WARNING
            });
        }

        function modal(evento, IdGrupo, grupo) {

            formModalGrupoRota = new Ext.Window({
                id: 'formModalGrupoRota',
                title: "Cadastrar Grupo de Rotas",
                modal: true,
                width: 420,
                height: 150,
                resizable: false,
                autoScroll: false,
                autoLoad:
                {url: '<%=Url.Action("FormularioGrupoRota")%>',
                    
                    params: { IdGrupo: IdGrupo, Grupo: grupo, evento: evento },
                    text: "Abrindo Cadastro...",
                    scripts: true,
                    callback: function (el, sucess) {
                        if (!sucess) {
                            formModalGrupoRota.close();
                        }
                    }
                },
                listeners: {
                    resize: function (win, width, height, eOpts) {
                        win.center();
                    },
                    'close': function (win) {
                        pesquisarGrupoRota();
                    }  
                }
            });
            window.wformModalGrupoRota = formModalGrupoRota;
            formModalGrupoRota.show(this);
        };

        Ext.onReady(function () {
            $("#txtDataInicialGrupoRota").val("__/__/____");
            $("#txtDataFinalGrupoRota").val("__/__/____");

            colocarHojeEmDatasGrupoRota();
            
        });

        geral.AbaGrupo = AbaGrupo;
        return geral;
    }(window.geral || {}));

</script>
