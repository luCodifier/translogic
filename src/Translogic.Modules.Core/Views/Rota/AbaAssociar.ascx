﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>

<script type="text/javascript">

    window.geral = (function (geral) {

        var AbaAssociar = geral.AbaAssociar || {};
        var formModalAssociar = null;

        var gridStoreAssociar = new Ext.data.JsonStore({
            root: "Items",
            totalProperty: 'Total',
            id: 'gridStoreAssociar',
            name: 'gridStoreAssociar',
            proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("BuscaAssociacoes","Rota") %>', timeout: 600000 }),
            paramNames: {
                sort: "detalhesPaginacaoWeb.Sort",
                dir: "detalhesPaginacaoWeb.Dir",
                start: "detalhesPaginacaoWeb.Start",
                limit: "detalhesPaginacaoWeb.Limit"
            },
            fields: ['Id', 'IdGrupo', 'Grupo', 'IdRota','Rota', 'Data', 'Usuario']
        });

        var ddlGrupoRotaStore = new Ext.data.JsonStore({
            root: "Items",
            autoLoad: true,
            proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("ObterGrupoRotas", "Rota") %>', timeout: 600000 }),
            id: 'ddlGrupoRotaStore',
            fields: ['IdGrupo', 'Grupo']
        });

        var ddlRotaStore = new Ext.data.JsonStore({
            root: "Items",
            autoLoad: true,
            proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("ObterRotas", "Rota") %>', timeout: 600000 }),
            id: 'ddlRotaStore',
             fields: ['IdRota', 'Descricao']
         });

        (function () {
            
            var pagingToolbarAssociar = new Ext.PagingToolbar({
                pageSize: 50,
                store: gridStoreAssociar,
                displayInfo: true,
                displayMsg: App.Resources.Web.GridExibindoRegistros,
                emptyMsg: App.Resources.Web.GridSemRegistros,
                paramNames: {
                    start: "detalhesPaginacaoWeb.Start",
                    limit: "detalhesPaginacaoWeb.Limit"
                }
            });

            var ActionsAssociar = new Ext.ux.grid.RowActions({
                id: '',
                dataIndex: '',
                header: '',
                align: 'center',
                autoWidth: false,
                width: 10,
                actions: [
                    { iconCls: 'icon-edit', tooltip: 'Editar' },
                    { iconCls: 'icon-del', tooltip: 'Excluir' }
                ],
                callbacks: {
                    'icon-del': function (grid, record, action, row, col) {
                        remover(record);
                    },
                    'icon-edit': function (grid, record, action, row, col) {
                        editar(record);
                    }
                }
            });

            var cmAssociar = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true
                },
                plugins: ActionsAssociar,
                columns: [
                    new Ext.grid.RowNumberer(),
                    ActionsAssociar,
                    { header: 'Id', dataIndex: "Id", sortable: false, width: 10, hidden: true },
                    { header: 'IdGrupo', dataIndex: "IdGrupo", sortable: false, width: 10, hidden: true },
                    { header: 'IdRota', dataIndex: "IdRota", sortable: false, width: 10, hidden: true },
                    { header: 'Grupo Rotas', dataIndex: "Grupo", sortable: false, width: 100 },
                    { header: 'Rotas', dataIndex: "Rota", sortable: false, width: 100 },
                    { header: 'Data Cadastro', dataIndex: "Data", sortable: false, width: 30 },
                    { header: 'Usuário', dataIndex: "Usuario", sortable: false, width: 30 }
                ]
            });

            var gridAssociar = new Ext.grid.EditorGridPanel({
                id: 'gridAssociar',
                name: 'gridAssociar',
                autoLoadGrid: false,
                height: 360,
                limit: 10,
                stripeRows: true,
                cm: cmAssociar,
                region: 'center',
                viewConfig: {
                    forceFit: true,
                    emptyText: 'Não possui dado(s) para exibição.'
                },
                autoScroll: true,
                loadMask: { msg: App.Resources.Web.Carregando },
                store: gridStoreAssociar,
                bbar: pagingToolbarAssociar,
                plugins: [ActionsAssociar],
                tbar: [
                    {
                        id: 'btnCadastrar',
                        text: 'Associar',
                        tooltip: 'Associar',
                        iconCls: 'icon-save',
                        handler: function (c) {
                            cadastrarAssociacao();
                        }
                    }
                ],
            });

            gridAssociar.getStore().proxy.on('beforeload', function (p, params) {
                
                params['dataInicial'] = Ext.getCmp("txtDataInicialAssociar").getRawValue();
                params['dataFinal'] = Ext.getCmp("txtDataFinalAssociar").getRawValue();
                params['grupoRota'] = Ext.getCmp("ddlGrupoRotasAssociar").getValue() == "Todos" ? "" : Ext.getCmp("ddlGrupoRotasAssociar").getValue();
                params['origem'] = Ext.getCmp("txtOrigemAssociar").getValue();
                params['destino'] = Ext.getCmp("txtDestinoAssociar").getValue();
                params['rota'] = Ext.getCmp("ddlRotas").getValue() == "Todos" ? "" : Ext.getCmp("ddlRotas").getValue();
            });

            var txtDataInicialAssociar = new Ext.form.DateField({
                fieldLabel: 'Data Inicial',
                id: 'txtDataInicialAssociar',
                name: 'txtDataInicialAssociar',
                dateFormat: 'd/n/Y',
                width: 83,
                plugins: [new Ext.ux.InputTextMask('99/99/9999', true)]

            });

            var formDataInicialAssociar = {
                width: 100,
                layout: 'form',
                border: false,
                items: [txtDataInicialAssociar]
            };

            var txtDataFinalAssociar = new Ext.form.DateField({
                fieldLabel: 'Data Final',
                id: 'txtDataFinalAssociar',
                name: 'txtDataFinalAssociar',
                dateFormat: 'd/n/Y',
                width: 83,
                plugins: [new Ext.ux.InputTextMask('99/99/9999', true)]
            });

            var formDataFinalAssociar = {
                width: 100,
                layout: 'form',
                border: false,
                items: [txtDataFinalAssociar]
            };
           
            var ddlGrupoRotasAssociar = new Ext.form.ComboBox({
                editable: true,
                typeAhead: true,
                forceSelection: true,
                disableKeyFilter: true,
                triggerAction: 'all',
                lazyRender: true,
                mode: 'local',
                valueField: 'IdGrupo',
                displayField: 'Grupo',
                fieldLabel: 'Grupo Rotas',
                id: 'ddlGrupoRotasAssociar',
                name: 'ddlGrupoRotasAssociar',
                store: ddlGrupoRotaStore,
                value: "Todos",
                width: 240
            });

            var formGrupoRotasAssociar = {
                width: 250,
                layout: 'form',
                border: false,
                items: [ddlGrupoRotasAssociar]
            };

            var txtOrigemAssociar = {
                xtype: 'textfield',
                name: 'txtOrigemAssociar',
                id: 'txtOrigemAssociar',
                fieldLabel: 'Origem',
                style: 'text-transform: uppercase',
                width: 80
            };

            var formtxtOrigem = {
                width: 90,
                layout: 'form',
                border: false,
                items: [txtOrigemAssociar]
            };

            var txtDestinoAssociar = {
                xtype: 'textfield',
                name: 'txtDestinoAssociar',
                id: 'txtDestinoAssociar',
                fieldLabel: 'Destino',
                style: 'text-transform: uppercase',
                width: 80
            };

            var formtxtDestino = {
                width: 90,
                layout: 'form',
                border: false,
                items: [txtDestinoAssociar]
            };

            var ddlRotas = new Ext.form.ComboBox({
                editable: true,
                typeAhead: true,
                forceSelection: true,
                disableKeyFilter: true,
                triggerAction: 'all',
                lazyRender: true,
                mode: 'local',
                valueField: 'IdRota',
                displayField: 'Descricao',
                fieldLabel: 'Rota',
                id: 'ddlRotas',
                name: 'ddlRotas',
                store: ddlRotaStore,
                value: "Todos",
                width: 240
            });

            var formRotas = {
                width: 250,
                layout: 'form',
                border: false,
                items: [ddlRotas]
            };

            var colFiltrosAssociar = {
                id: 'colFiltrosAssociar',
                layout: 'column',
                border: false,
                items: [formDataInicialAssociar, formDataFinalAssociar, formGrupoRotasAssociar, formtxtOrigem, formtxtDestino, formRotas]
            };

            var filtrosAssociar = new Ext.form.FormPanel({
                id: 'filtrosAssociar',
                title: "Filtros",
                region: 'center',
                bodyStyle: 'padding:15px;',
                layout: 'form',
                labelAlign: 'top',
                items: [colFiltrosAssociar],
                buttonAlign: "center",
                buttons:
                    [{
                        text: 'Pesquisar',
                        id: 'btnPesquisar',
                        type: 'submit',
                        cls: 'clsBtnPesquisar',
                        iconCls: 'icon-find',
                        handler: function (b, e) {
                            pesquisarAssociar();
                        }
                    },
                    {
                        text: 'Limpar',
                        id: 'btnLimpar',
                        type: 'submit',
                        handler: function (b, e) {
                            limparAssociar();
                        }
                    }]
            });

            var formPainelAssociar = {
                layout: 'form',
                border: false,
                items: [filtrosAssociar, gridAssociar]
            };

            AbaAssociar.getForm = function () {
                return formPainelAssociar;
            }

        }());

        function pesquisarAssociar() {
            if (validarFiltros()) {
                gridStoreAssociar.load({ params: { start: 0, limit: 50 } });
            }
        };

        function validarFiltros() {

            let dtInicial = null;
            let dtFinal = null;

            //Converte data inicial para formato MM/DD/YYYY para controller conseguir identificar
            let dateParts = (Ext.getCmp("txtDataInicialAssociar").getRawValue()).split("/");
            if (dateParts.length > 1) {
                dtInicial = dateParts[1] + "/" + dateParts[0] + "/" + dateParts[2];
            } else {
                dtInicial = null;
            }

            //Converte data final para formato MM/DD/YYYY para controller conseguir identificar
            dateParts = (Ext.getCmp("txtDataFinalAssociar").getRawValue()).split("/");
            if (dateParts.length > 1) {
                dtFinal = dateParts[1] + "/" + dateParts[0] + "/" + dateParts[2];
            } else {
                dtFinal = null;
            }

            if (dtInicial == null || dtFinal == null) {
                alerta("Ambos os filtros de Data são obrigatórios");
                return false;
            }
            return true;
        };

        function limparAssociar() {
            var gridAssociar = Ext.getCmp("gridAssociar");

            gridAssociar.getStore().removeAll();

            Ext.getCmp("filtrosAssociar").getForm().reset();

            colocarHojeEmDatasAssociar();
        };

        function colocarHojeEmDatasAssociar() {

            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();

            if (dd < 10) {
                dd = '0' + dd
            }

            if (mm < 10) {
                mm = '0' + mm
            }

            today = dd + '/' + mm + '/' + yyyy;

            Ext.getCmp("txtDataInicialAssociar").setValue(today);
            Ext.getCmp("txtDataFinalAssociar").setValue(today);
        }

        function cadastrarAssociacao() {
            modal('Salvar');
        }

        function remover(record) {
            modal('Excluir', record.data.Id, record.data.IdGrupo, record.data.Grupo, record.data.IdRota);
        }

        function editar(record) {
            modal('Editar', record.data.Id, record.data.IdGrupo, record.data.Grupo, record.data.IdRota);
        }

        function alerta(message) {
            Ext.Msg.show({
                title: 'Aviso',
                msg: message,
                buttons: Ext.Msg.OK,
                icon: Ext.MessageBox.WARNING
            });
        }

        function modal(evento, idGrupoRota, idGrupo, grupo, idRota) {

            formModalAssociar = new Ext.Window({
                id: 'formModalAssociar',
                title: "Associação Rota/Grupo",
                modal: true,
                width: 510,
                height: 150,
                resizable: false,
                autoScroll: false,
                autoLoad:
                {
                    url: '<%=Url.Action("FormularioAssociar")%>',
                    params: { idGrupoRota: idGrupoRota, idGrupo: idGrupo, Grupo: grupo, idRota: idRota, evento: evento },
                    text: "Abrindo Cadastro...",
                    scripts: true,
                    callback: function (el, sucess) {
                        if (!sucess) {
                            formModalAssociar.close();
                        }
                    }
                },
                listeners: {
                    resize: function (win, width, height, eOpts) {
                        win.center();
                    },
                    'close': function (win) {
                        pesquisarAssociar();
                    }                    
                }
            });
            window.wformModalAssociar = formModalAssociar;
            formModalAssociar.show(this);
        };

        Ext.onReady(function () {
            $("#txtDataInicialAssociar").val("__/__/____");
            $("#txtDataFinalAssociar").val("__/__/____");

            colocarHojeEmDatasAssociar();
        });

        geral.AbaAssociar = AbaAssociar;
        return geral;
    }(window.geral || {}));

</script>
