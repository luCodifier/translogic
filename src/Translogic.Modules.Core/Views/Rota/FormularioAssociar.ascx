﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>

<div id="div-cadastro">
</div>

<script type="text/javascript">

    var idGrupoRota = '<%=ViewData["idGrupoRota"] %>';
    var idGrupo = '<%=ViewData["idGrupo"] %>';
    var grupo = '<%=ViewData["Grupo"] %>';
    var idRota = '<%=ViewData["idRota"] %>';
    var evento = '<%=ViewData["evento"] %>';

    var ddlGrupoRotaStoreFormulario = new Ext.data.JsonStore({
        root: "Items",
        autoLoad: true,
        proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("ObterGrupoRotas", "Rota") %>', timeout: 600000 }),
         id: 'ddlGrupoRotaStoreFormulario',
         fields: ['IdGrupo', 'Grupo']
     });
    
    var ddlRotaStoreFormulario = new Ext.data.JsonStore({
        root: "Items",
        autoLoad: true,
        proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("ObterRotas", "Rota") %>', timeout: 600000 }),
         id: 'ddlRotaStoreFormulario',
         fields: ['IdRota', 'Descricao']
     });

    var txtGrupoRotaAssociar = {
        xtype: 'textfield',
        name: 'txtGrupoRotaAssociar',
        id: 'txtGrupoRotaAssociar',
        fieldLabel: 'Grupo de Rotas',
        disabled: true,
        width: 280,
        value: grupo
    };

    var formtxtGrupoRotaAssociar = {
        id: 'formtxtGrupoRotaAssociar',
        layout: 'form',
        border: false,
        width: 290,
        items: [txtGrupoRotaAssociar],
    };

    var dllGrupoRotaAssociar = new Ext.form.ComboBox({
        editable: true,
        typeAhead: true,
        forceSelection: true,
        disableKeyFilter: true,
        triggerAction: 'all',
        lazyRender: true,
        mode: 'local',
        valueField: 'IdGrupo',
        displayField: 'Grupo',
        fieldLabel: 'Grupo de Rotas',
        id: 'dllGrupoRotaAssociar',
        name: 'dllGrupoRotaAssociar',
        store: ddlGrupoRotaStoreFormulario,
        value: idGrupo,
        width: 280
    });

    var formdllGrupoRotaAssociar = {
        id: 'formdllGrupoRotaAssociar',
        width: 290,
        layout: 'form',
        border: false,
        items: [dllGrupoRotaAssociar]
    };

    var ddlRotaAssociar = new Ext.form.ComboBox({
        editable: true,
        typeAhead: true,
        forceSelection: true,
        disableKeyFilter: true,
        triggerAction: 'all',
        lazyRender: true,
        mode: 'local',
        valueField: 'IdRota',
        displayField: 'Descricao',
        fieldLabel: 'Rota',
        id: 'ddlRotaAssociar',
        name: 'ddlRotaAssociar',
        store: ddlRotaStoreFormulario,
        width: 180
    });

    var formRotasFormulario = {
        width: 190,
        layout: 'form',
        border: false,
        items: [ddlRotaAssociar]
    };

    var colFiltrosFormularioGrupo = {
        layout: 'column',
        border: false,
        items: [formtxtGrupoRotaAssociar, formdllGrupoRotaAssociar, formRotasFormulario]
    };

    var formCadastroGrupo = new Ext.form.FormPanel({
        id: 'formCadastroGrupo',
        name: 'formCadastroGrupo',
        width: 600,
        border: true,
        autoScroll: true,
        url: '<%= Url.Action("ConfirmaFormularioAssociar", "Rota") %>',
         standardSubmit: true,
         bodyStyle: 'padding: 15px',
         labelAlign: 'top',
         items:
             [
                 colFiltrosFormularioGrupo
             ],
         buttonAlign: "left",
         buttons: [
             {
                 text: evento,
                 id: "btnSalvarAssociar",
                 type: 'submit',
                 formBind: true,
                 handler: function (b, e) {

                     var idGrupoSalvar = evento == "Salvar" ? Ext.getCmp("dllGrupoRotaAssociar").getValue() : idGrupo;
                     var idRotaAssociar = Ext.getCmp("ddlRotaAssociar").getValue();

                     if (evento == "Salvar" && (idGrupoSalvar == "" || idGrupoSalvar == null || idGrupoSalvar == "Todos")) {
                         Ext.Msg.show({
                             title: 'Aviso',
                             msg: 'O campo Grupo é obrigatório.',
                             buttons: Ext.Msg.OK,
                             icon: Ext.MessageBox.WARNING
                         });
                         return;
                     }

                     if (idRotaAssociar == "" || idRotaAssociar == null || idRotaAssociar == "Todos") {
                         Ext.Msg.show({
                             title: 'Aviso',
                             msg: 'O campo Rota é obrigatório.',
                             buttons: Ext.Msg.OK,
                             icon: Ext.MessageBox.WARNING
                         });
                         return;
                     }

                     var payload = { "IdGrupoRotaRota": idGrupoRota, "IdGrupo": idGrupoSalvar, "IdRota": idRotaAssociar, "Evento": evento, };

                     console.log(payload);

                     $.ajax({
                         url: '<%= Url.Action("ConfirmaFormularioAssociar", "Rota") %>',
                             type: "POST",
                             data: payload,
                             success: function (response) {
                                 if (response) {
                                     Ext.Msg.show({
                                         title: 'Aviso',
                                         msg: 'Alterações salvas com sucesso!',
                                         buttons: Ext.Msg.OK,
                                         icon: Ext.MessageBox.INFO
                                     });

                                     Ext.getCmp("formModalAssociar").close();
                                 }
                             },
                             failure: function (response, options) {
                                 Ext.Msg.show({
                                     title: 'Aviso',
                                     msg: response.Message,
                                     buttons: Ext.Msg.OK,
                                     icon: Ext.MessageBox.ERROR
                                 });
                             }
                         });
                 }

             }
         ]
     });

    Ext.onReady(function () {
        formCadastroGrupo.render("div-cadastro");

        if (evento == "Salvar") {
            Ext.getCmp("formtxtGrupoRotaAssociar").setVisible(false);
            Ext.getCmp("formdllGrupoRotaAssociar").setVisible(true);
        } else {
            Ext.getCmp("formtxtGrupoRotaAssociar").setVisible(true);
            Ext.getCmp("formdllGrupoRotaAssociar").setVisible(false);
        }

        Ext.getCmp("btnSalvarAssociar").setDisabled(true);

        if (idRota) {
            setTimeout(function () {
                Ext.getCmp("ddlRotaAssociar").setValue(idRota);
                Ext.getCmp("btnSalvarAssociar").setDisabled(false);
            }, 3000);
        } else {
            Ext.getCmp("btnSalvarAssociar").setDisabled(false);
        }

    });

</script>
