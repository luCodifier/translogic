﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Interna.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content runat="server" ID="Content1" ContentPlaceHolderID="HeadContent">

    <%= this.Html.Partial("AbaLimite") %>
    <%= this.Html.Partial("AbaGrupo") %>
    <%= this.Html.Partial("AbaAssociar") %>

    <script type="text/javascript">

        var tabs = new Ext.TabPanel({
            id: 'tabPanelGeral',
            activeTab: 0,
            region: 'center',
            deferredRender: false,
            autoTabs: true,
            autoHeight: true,
            items: [
                {
                    id: 'AbaLimite',
                    title: 'Limíte mínimo por grupo de rotas',
                    autoHeight: true,
                    items: [geral.AbaLimite.getForm()]
                },
                {
                    id: 'AbaGrupo',
                    title: 'Grupo de Rotas',
                    autoHeight: true,
                    items: [geral.AbaGrupo.getForm()]
                }
                ,{
                    title: 'Associação Rota/Grupo',
                    id: 'AbaAssociar',
                    autoHeight: true,
                    items: [geral.AbaAssociar.getForm()]
                }
            ]
        });

        Ext.onReady(function () {

            new Ext.Viewport({
                labelAlign: 'top',
                region: 'center',
                layout: 'border',
                border: false,
                items: [
                    {
                        region: 'center',
                        autoScroll: true,
                        items:
                            [
                                {
                                    region: 'center',
                                    applyTo: 'header-content'
                                },
                                tabs
                            ]
                    }
                ]
            });


            Ext.getCmp("tabPanelGeral").setActiveTab(1);

        });


    </script>
</asp:Content>
<asp:Content runat="server" ID="Content2" ContentPlaceHolderID="Content">
    <div id="header-content">
    </div>
</asp:Content>
