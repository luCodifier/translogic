﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>

<div id="div-cadastro">
</div>

 <script type="text/javascript">

     var idGrupo = '<%=ViewData["idGrupo"] %>';    
     var grupo = '<%=ViewData["GrupoRota"] %>';
     var evento = '<%=ViewData["evento"] %>';

     var txtGrupoRota = {
         xtype: 'textfield',
         name: 'txtGrupoRota',
         id: 'txtGrupoRota',
         fieldLabel: 'Grupo de Rotas',
         disabled: evento == "Excluir",
         style: 'text-transform: uppercase',
         width: 270,
         value: grupo
     };

     var formtxtGrupoRota = {
         layout: 'form',
         border: false,
         width: 280,
         items: [txtGrupoRota]
     };

     var colFiltrosFormularioGrupo = {
         layout: 'column',
         border: false,
         items: [formtxtGrupoRota]
     };

     var formCadastroGrupo = new Ext.form.FormPanel({
         id: 'formCadastroGrupo',
         name: 'formCadastroGrupo',
         width: 450,
         border: true,
         autoScroll: true,
         url: '<%= Url.Action("ConfirmaFormularioGrupo", "Rota") %>',
         standardSubmit: true,
         bodyStyle: 'padding: 15px',
         labelAlign: 'top',
         items:
		[
                 colFiltrosFormularioGrupo
		],
         buttonAlign: "left",
         buttons: [
            {
                text: evento,
                type: 'submit',
                formBind: true,
                handler: function (b, e) {

                    var txtGrupoRota = Ext.getCmp("txtGrupoRota").getValue();


                    if (txtGrupoRota == "" || txtGrupoRota == null) {
                         Ext.Msg.show({
                             title: 'Aviso',
                             msg: 'O campo Grupo é obrigatório.',
                             buttons: Ext.Msg.OK,
                             icon: Ext.MessageBox.WARNING
                         });
                     }
                   
                    if (txtGrupoRota) {

                        var payload = { "IdGrupo": idGrupo, "Grupo": txtGrupoRota, "Evento": evento };

                         console.log(payload);

                         $.ajax({
                             url: '<%= Url.Action("ConfirmaFormularioGrupo", "Rota") %>',
                             type: "POST",
                             data: payload,
                             success: function (response) {
                                 if (response) {
                                     
                                   

                                     Ext.getCmp("formModalGrupoRota").close();

                                     Ext.Msg.show({
                                         title: 'Aviso',
                                         msg: 'Alterações salvas com sucesso!',
                                         buttons: Ext.Msg.OK,
                                         icon: Ext.MessageBox.INFO
                                     });                                    
                                 }
                             },
                             failure: function (response, options) {
                                 Ext.Msg.show({
                                     title: 'Aviso',
                                     msg: response.Message,
                                     buttons: Ext.Msg.OK,
                                     icon: Ext.MessageBox.ERROR
                                 });
                             }
                         });
                     }
                 }
                
            }
        ]
     });

    Ext.onReady(function () {
        formCadastroGrupo.render("div-cadastro");
    });

 </script>
