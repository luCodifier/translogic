<%@ Import Namespace="System.Security.Policy" %>

<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Interna.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Import Namespace="Translogic.Modules.Core.Domain.Model.Diversos.Cte" %>
<asp:Content runat="server" ID="HeadContent" ContentPlaceHolderID="HeadContent">
    <script type="text/javascript">

		var formModal = null;
		var grid = null;
		var gridStatus = null;
		var ds = null;
		var windowStatusNFe = null;
		var lastRowSelected = -1;
		
		// Flag para garantir que uma tela de mensagem que est� sobreposta n�o chame novamente a tela de pesquisa
		var executarPesquisa = true;
		function showResult(btn) {
			executarPesquisa = true;
		}

		
		var storeChaves = new Ext.data.JsonStore({
			remoteSort: true,
			root: "Items",
			fields: [
						'Chave',
						'Erro',
						'Mensagem'
					]
		});
		
        var storeVagoes = new Ext.data.JsonStore({
		    remoteSort: true,
		    root: "Items",
		    fields: [
					    'Vagao',
					    'Erro',
					    'Mensagem'
				    ]
	    });

		function HabilitarCampos(bloquear)
		{
			Ext.getCmp("filtro-data-inicial").setDisabled(bloquear);
			Ext.getCmp("filtro-data-final").setDisabled(bloquear);
			Ext.getCmp("filtro-serie").setDisabled(bloquear);
			Ext.getCmp("filtro-despacho").setDisabled(bloquear);
			Ext.getCmp("filtro-fluxo").setDisabled(bloquear);
			Ext.getCmp("filtro-UfDcl").setDisabled(bloquear);
			Ext.getCmp("filtro-Ori").setDisabled(bloquear);
			Ext.getCmp("filtro-Dest").setDisabled(bloquear);
			Ext.getCmp("filtro-Chave").setDisabled(bloquear);
			Ext.getCmp("filtro-numVagao").setDisabled(bloquear);
		}

		function Pesquisar() {
			var diferenca = Ext.getCmp("filtro-data-final").getValue() - Ext.getCmp("filtro-data-inicial").getValue();
			diferenca = Math.floor(diferenca / (1000 * 60 * 60 * 24));

			if (Ext.getCmp("filtro-data-inicial").getValue() == '' || Ext.getCmp("filtro-data-final").getValue() == '') {
				executarPesquisa = false;
				Ext.Msg.show({
					title: "Mensagem de Informa��o",
					msg: "Preencha os filtro datas!",
					buttons: Ext.Msg.OK,
					icon: Ext.MessageBox.INFO,
					minWidth: 200,
					fn: showResult
				});
			} 
			else if (diferenca > 30) {
				executarPesquisa = false;
				Ext.Msg.show({
					title: "Mensagem de Informa��o",
					msg: "O per�odo da pesquisa n�o deve ultrapassar 30 dias",
					buttons: Ext.Msg.OK,
					icon: Ext.MessageBox.INFO,
					minWidth: 200,
					fn: showResult
				});
			}
			else if (Ext.getCmp("filtro-serie").getValue() != '' && Ext.getCmp("filtro-despacho").getValue() == '') {
				executarPesquisa = false;
				Ext.Msg.show({
					title: "Mensagem de Informa��o",
					msg: "N�o � poss�vel filtrar apenas pela a S�rie do Despacho.<br/>Tamb�m � necess�rio preencher o n�mero do Despacho.",
					buttons: Ext.Msg.OK,
					icon: Ext.MessageBox.INFO,
					minWidth: 200,
					fn: showResult
				});
			}
			else if (Ext.getCmp("filtro-despacho").getValue() != '' && Ext.getCmp("filtro-serie").getValue() == '') {
				executarPesquisa = false;
				Ext.Msg.show({
					title: "Mensagem de Informa��o",
					msg: "N�o � poss�vel filtrar apenas pelo n�mero do Despacho.<br/>Tamb�m � necess�rio preencher a S�rie do Despacho.",
					buttons: Ext.Msg.OK,
					icon: Ext.MessageBox.INFO,
					minWidth: 200,
					fn: showResult
				});
			}
			else {
			//	svar view = Ext.getCmp('gridCteRaiz').getView();
			//	var chkdiv = Ext.fly(view.innerHd).child(".x-grid3-hd-checker")
			//	chkdiv.removeClass('x-grid3-hd-checker-on');
				executarPesquisa = true;
				grid.getStore().load();
			}
		}

		function FormSuccess(form, action) {
			Ext.getCmp("gridCteRaiz").getStore().removeAll();
			dsStatus.removeAll();
			Ext.getCmp("reemissao").disabled = true;
			Ext.getCmp("gridCteRaiz").getStore().loadData(action.result, true);

		}

		function FormError(form, action) {
			Ext.Msg.alert('Mensagem de Erro', action.result.Message);
			ds.removeAll();
		}

		var storeImpressao = new Ext.data.JsonStore({
			root: "Items",
			fields: [
				'CteId',
				'NroCte',
				'Status'
			]
		});

		var dsCodigoControle = new Ext.data.JsonStore({
			root: "Items",
			autoLoad: true,
			url: '<%= Url.Action("ObterCodigoSerieDesp") %>',
			fields: [
                    'Id',
                    'CodigoControle'
			    ]
		});

		var dsStatus = new Ext.data.JsonStore({
			root: "Items",
			url: '<%= Url.Action("ObterHistoricoStatusCte") %>',
			fields: [
                    'DescricaoStatus',
                    'DateEmissao',
					'AcaoTomada',
					'Situacao'
			    ]
		});

		var cboCteCodigoControle = {
			xtype: 'combo',
			store: dsCodigoControle,
			allowBlank: true,
			lazyInit: false,
			lazyRender: false,
			mode: 'local',
			typeAhead: false,
			triggerAction: 'all',
			fieldLabel: 'UF DCL',
			name: 'filtro-UfDcl',
			id: 'filtro-UfDcl',
			hiddenName: 'filtro-UfDcl',
			displayField: 'CodigoControle',
			forceSelection: true,
			width: 70,
			valueField: 'Id',
			emptyText: 'Selecione...',
			editable: false,
			tpl: '<tpl for="."><div class="x-combo-list-item">{CodigoControle}&nbsp;</div></tpl>'
		};



		/* ------------- A��o CTRL+C nos campos da Grid ------------- */
		var isCtrl = false;
		document.onkeyup = function (e) {

			var keyID = event.keyCode;

			// 17 = tecla CTRL
			if (keyID == 17) {
				isCtrl = false; //libera tecla CTRL
			}
		}

		document.onkeydown = function (e) {

			var keyID = event.keyCode;

			// verifica se CTRL esta acionado
			if (keyID == 17) {
				isCtrl = true;
			}

		if ((keyID == 13) && (executarPesquisa)) {
			Pesquisar();
			return;
		}

			// 67 = tecla 'c'
			if (keyID == 67 && isCtrl == true) {

				// verifica se h� selec�o na tabela
				if (grid.getSelectionModel().hasSelection()) {
					// captura todas as linhas selecionadas
					var recordList = grid.getSelectionModel().getSelections();

					var a = [];
					var separator = '\t'; // separador para colunas no excel
					for (var i = 0; i < recordList.length; i++) {
						var s = '';
						var item = recordList[i].data;
						for (key in item) {
							if (key != 'Id') { //elimina o campo id da linha
								s = s + item[key] + separator;
							}
						}
						s = s.substr(0, s.length - 1); //retira o ultimo separador '\t' da linha
						a.push(s);
					}
					window.clipboardData.setData('Text', (a.join('\n'))); //insere linhas no clipBoard
				}
			}
		}



		/* ------------------------------------------------------- */

		$(function () {

			smRaiz = new Ext.grid.CheckboxSelectionModel({
				listeners: {
					selectionchange: function (sm) {
						var recLen = Ext.getCmp('gridCteRaiz').store.getRange().length;
						var selectedLen = this.selections.items.length;
						if (selectedLen == recLen) {
							var view = Ext.getCmp('gridCteRaiz').getView();
						    var chkdiv = Ext.fly(view.innerHd).child(".x-grid3-hd-checker");
							chkdiv.addClass("x-grid3-hd-checker-on");
						}
					}
				},
				rowdeselect: function (sm, rowIndex, record) {
					var view = Ext.getCmp('gridCteRaiz').getView();
				    var chkdiv = Ext.fly(view.innerHd).child(".x-grid3-hd-checker");
					chkdiv.removeClass('x-grid3-hd-checker-on');
				}
			});

			grid = new Translogic.PaginatedGrid({
				// stripeRows: false,
				autoLoadGrid: false,
				id: "gridCteRaiz",
				height: 300,
				width: 850,
				url: '<%= Url.Action("ObterCtes") %>',
				region: 'center',
				viewConfig: {
					forceFit: false
				},
				fields: [
					'CteId',
                    'Fluxo',
                    'Origem',
                    'Destino',
                    'Mercadoria',
                    'Chave',
                    'Cte',
                    'SituacaoCte',
                    'DateEmissao',
                    'Impresso',
                    'CodigoErro',
                    'FerroviaOrigem',
                    'FerroviaDestino',
                    'UfOrigem',
					'CodigoVagao',
                    'UfDestino',
                    'Serie',
                    'Despacho',
                    'SerieDesp5',
                    'Desp5',
                    'ArquivoPdfGerado',
					'AcaoSerTomada'
			    ],
				columns: [
                        smRaiz,
						{ dataIndex: "CteId", hidden: true },
						{ header: 'Num Vag�o', dataIndex: "CodigoVagao", width: 100, sortable: false },
						{ header: 'S�rie', dataIndex: "Serie", width: 70, sortable: false },
                        { header: 'Despacho', dataIndex: "Despacho", width: 70, sortable: false },
                        { header: 'CTe', dataIndex: "Cte", width: 100, sortable: false },
                        { header: 'Origem', dataIndex: "Origem", width: 50, sortable: false },
						{ header: 'Destino', dataIndex: "Destino", width: 50, sortable: false },
                        { header: 'Fluxo', dataIndex: "Fluxo", width: 70, sortable: false },
				        { header: 'Mercadoria', dataIndex: "Mercadoria", width: 70, sortable: false },
				        { header: 'Chave CTe', dataIndex: "Chave", width: 280, sortable: false },
                        { header: 'Data Emiss�o', dataIndex: "DateEmissao", width: 75, sortable: false },
                        { header: 'Ferrovia Ori.', dataIndex: "FerroviaOrigem", sortable: false },
                        { header: 'Ferrovia Dest.', dataIndex: "FerroviaDestino", sortable: false },
                        { header: 'UF Ori.', dataIndex: "UfOrigem", width: 70, sortable: false },
                        { header: 'UF Dest.', dataIndex: "UfDestino", width: 70, sortable: false },
                        { header: 'SerieDesp5', dataIndex: "SerieDesp5", width: 70, sortable: false },
                        { header: 'Desp5', dataIndex: "Desp5", width: 70, sortable: false }
					],
				sm: smRaiz,
				listeners: {
					rowclick: function(grid, rowIndex, e) {
						if (smRaiz.isSelected(rowIndex)) {
							if (rowIndex == lastRowSelected) {
								grid.getSelectionModel().deselectRow(rowIndex);
								lastRowSelected = -1;
							}
							else {
								lastRowSelected = rowIndex;
							}
						}
					}
				}
			});

			ds = Ext.getCmp("gridCteRaiz").getStore();
			grid.getStore().proxy.on('beforeload', function (p, params) {
				
				params['filter[0].Campo'] = 'dataInicial';
				params['filter[0].Valor'] = new Array(Ext.getCmp("filtro-data-inicial").getValue().format('d/m/Y') + " " + Ext.getCmp("filtro-data-inicial").getValue().format('H:i:s'));
				params['filter[0].FormaPesquisa'] = 'Start';

				params['filter[1].Campo'] = 'dataFinal';
				params['filter[1].Valor'] = new Array(Ext.getCmp("filtro-data-final").getValue().format('d/m/Y') + " " + Ext.getCmp("filtro-data-inicial").getValue().format('H:i:s'));
				params['filter[1].FormaPesquisa'] = 'Start';

				params['filter[2].Campo'] = 'serie';
				params['filter[2].Valor'] = Ext.getCmp("filtro-serie").getValue();
				params['filter[2].FormaPesquisa'] = 'Start';

				params['filter[3].Campo'] = 'despacho';
				params['filter[3].Valor'] = Ext.getCmp("filtro-despacho").getValue();
				params['filter[3].FormaPesquisa'] = 'Start';

				params['filter[4].Campo'] = 'fluxo';
				params['filter[4].Valor'] = Ext.getCmp("filtro-fluxo").getValue();
				params['filter[4].FormaPesquisa'] = 'Start';

				var listaChave = "";
				if(storeChaves.getCount() != 0)
				{
					storeChaves.each(
						function (record) {
							if (!record.data.Erro) {
								listaChave += record.data.Chave + ";";	
							}
						}
					);
				}else{
					listaChave = Ext.getCmp("filtro-Chave").getValue();	
				}

				params['filter[5].Campo'] = 'chave';
				params['filter[5].Valor'] = listaChave;
				params['filter[5].FormaPesquisa'] = 'Start';

				params['filter[6].Campo'] = 'UfDcl';
				params['filter[6].Valor'] = Ext.getCmp("filtro-UfDcl").getRawValue();
				params['filter[6].FormaPesquisa'] = 'Start';

				params['filter[7].Campo'] = 'Origem';
				params['filter[7].Valor'] = Ext.getCmp("filtro-Ori").getValue();
				params['filter[7].FormaPesquisa'] = 'Start';

				params['filter[8].Campo'] = 'Destino';
				params['filter[8].Valor'] = Ext.getCmp("filtro-Dest").getValue();
				params['filter[8].FormaPesquisa'] = 'Start';

                var listaVagoes = '';
			    if(storeVagoes.getCount() != 0)
			    {
			    	storeVagoes.each(
			    		function (record) {
			    			if (!record.data.Erro) {
			    				listaVagoes += record.data.Vagao + ";";	
			    			}
			    		}
			    	);
			    } else {
			    	listaVagoes = Ext.getCmp("filtro-numVagao").getValue();	
			    }
			    
				params['filter[9].Campo'] = 'Vagao';
			    params['filter[9].Valor'] = listaVagoes;
				params['filter[9].FormaPesquisa'] = 'Start';
				
				var countChaves = storeChaves.getCount();
				if(countChaves > 0)
				{
					grid.pagingToolbar.pageSize = countChaves;
				}else{
					grid.pagingToolbar.pageSize = 50;
				}

			});

			var dataAtual = new Date();

			var dataInicial = {
				xtype: 'datefield',
				fieldLabel: 'Data Inicial',
				id: 'filtro-data-inicial',
				name: 'dataInicial',
				width: 83,
				allowBlank: false,
				vtype: 'daterange',
				endDateField: 'filtro-data-final',
				hiddenName: 'dataInicial',
				value: dataAtual
			};

			var dataFinal = {
				xtype: 'datefield',
				fieldLabel: 'Data Final',
				id: 'filtro-data-final',
				name: 'dataFinal',
				width: 83,
				allowBlank: false,
				vtype: 'daterange',
				startDateField: 'filtro-data-inicial',
				hiddenName: 'dataFinal',
				value: dataAtual
			};

			var origem = {
				xtype: 'textfield',
				vtype: 'cteestacaovtype',
				style: 'text-transform: uppercase',
				id: 'filtro-Ori',
				fieldLabel: 'Origem',
				autoCreate: { tag: 'input', type: 'text', maxlength: '3', autocomplete: 'off' },
				name: 'Origem',
				allowBlank: true,
				maxLength: 3,
				width: 35,
				hiddenName: 'Origem'
			};

			var destino = {
				xtype: 'textfield',
				vtype: 'cteestacaovtype',
				style: 'text-transform: uppercase',
				id: 'filtro-Dest',
				fieldLabel: 'Destino',
				autoCreate: { tag: 'input', type: 'text', maxlength: '3', autocomplete: 'off' },
				name: 'Destino',
				allowBlank: true,
				maxLength: 3,
				width: 35,
				hiddenName: 'Destino'
			};

			var serie = {
				xtype: 'textfield',
				vtype: 'ctesdvtype',
				style: 'text-transform: uppercase',
				id: 'filtro-serie',
				fieldLabel: 'S�rie',
				name: 'serie',
				allowBlank: true,
				autoCreate: { tag: 'input', type: 'text', maxlength: '3', autocomplete: 'off' },
				maxLength: 20,
				width: 30,
				hiddenName: 'serie'
			};

			var despacho = {
				xtype: 'textfield',
				vtype: 'ctedespachovtype',
				id: 'filtro-despacho',
				fieldLabel: 'Despacho',
				name: 'despacho',
				allowBlank: true,
				maxLength: 20,
				autoCreate: { tag: 'input', type: 'text', maxlength: '6', autocomplete: 'off' },
				width: 55,
				hiddenName: 'despacho'
			};

			var fluxo = {
				xtype: 'textfield',
				vtype: 'ctefluxovtype',
				style: 'text-transform: uppercase',
				id: 'filtro-fluxo',
				fieldLabel: 'Fluxo',
				name: 'fluxo',
				allowBlank: true,
				autoCreate: { tag: 'input', type: 'text', maxlength: '5', autocomplete: 'off' },
				maxLength: 20,
				width: 45,
				minValue: 0,
				maxValue: 99999,
				hiddenName: 'fluxo'
			};

            var Vagao = {
			    xtype: 'textfield',
			    style: 'text-transform: uppercase',
			    id: 'filtro-numVagao',
			    fieldLabel: 'Vag�o',
			    name: 'numVagao',
			    allowBlank: true,
			    autoCreate: { tag: 'input', type: 'text', autocomplete: 'off' },
			    width: 150,
			    hiddenName: 'numVagao'
		    };
        
            var btnLoteVagao = {
		    	text: 'Informar Vag�es',
		    	xtype: 'button',
		    	iconCls: 'icon-find',
		    	handler: function (b, e) {
		    		windowStatusVagoes = new Ext.Window({
		    					            id: 'windowStatusVagoes',
		    					         	title: 'Informar vag�es',
		    					         	modal: true,
		    					         	width: 855,
		    					         	resizable: false,
		    					         	height: 380,
		    					         	autoLoad: {
		    					         		url: '<%= Url.Action("FormInformarVagoes") %>',
		    					         		scripts: true
		    					         	}
		    					         });

                    windowStatusVagoes.show();
		    	}
		    };

			var chave = {
				xtype: 'textfield',
				maskRe: /[0-9]/,
				id: 'filtro-Chave',
				fieldLabel: 'Chave CTe',
				name: 'Chave',
				allowBlank: true,
				maxLength: 44,
				width: 275,
				hiddenName: 'Chave  ',
				autoCreate: { tag: 'input', type: 'text', maxlength: '44', autocomplete: 'off' }
			};

			var btnLoteChaveNfe = {
				text: 'Informar Chaves',
				xtype: 'button',
				iconCls: 'icon-find',
				handler: function (b, e) {
					
					windowStatusNFe = new Ext.Window({
									id: 'windowStatusNFe',
									title: 'Informar chaves',
									modal: true,
									width: 855,
									resizable: false,
									height: 380,
									autoLoad: {
										url: '<%= Url.Action("FormInformarChaves") %>',
										scripts: true
									}
								});

								windowStatusNFe.show();			
								
				}
			};

			var arrDataIni = {
				width: 87,
				layout: 'form',
				border: false,
				items:
				[dataInicial]
			};

			var arrDataFim = {
				width: 87,
				layout: 'form',
				border: false,
				items:
						[dataFinal]
			};

			var arrOrigem = {
				width: 43,
				layout: 'form',
				border: false,
				items:
						[origem]
			};
			var arrDestino = {
				width: 43,
				layout: 'form',
				border: false,
				items:
						[destino]
			};

			var arrSerie = {
				width: 37,
				layout: 'form',
				border: false,
				items:
								[serie]
			};

			var arrDespacho = {
				width: 60,
				layout: 'form',
				border: false,
				items:
						[despacho]
			};

			var arrFluxo = {
				width: 50,
				layout: 'form',
				border: false,
				items:
						[fluxo]
			};

			var arrVagao = {
				width: 160,
				layout: 'form',
				border: false,
				items:
				[Vagao]
			};
		    
            var arrBtnLoteVagoes = {
                width: 120,
		    	layout: 'form',
		    	style: 'padding: 17px 0px 0px 0px;',
		    	border: false,
		    	items: [btnLoteVagao]

            };

			var arrChave = {
				width: 280,
				layout: 'form',
				border: false,
				items:
						[chave]
			};


			var arrbtnLoteChaveNfe = {
				width: 280,
				layout: 'form',
				style: 'padding: 17px 0px 0px 0px;',
				border: false,
				items:
						[btnLoteChaveNfe]
			};

			var arrCodigoDcl = {
				width: 75,
				layout: 'form',
				border: false,
				items:
					[cboCteCodigoControle]
			};


			var arrlinha1 = {
				layout: 'column',
				border: false,
				items: [
				    arrDataIni, 
				    arrDataFim, 
				    arrFluxo, 
				    arrChave,
				    arrbtnLoteChaveNfe
				]
			};

			var arrlinha2 = {
				layout: 'column',
				border: false,
				items: [
				    arrCodigoDcl, 
				    arrSerie, 
				    arrDespacho, 
				    arrOrigem, 
				    arrDestino, 
				    arrVagao, 
				    arrBtnLoteVagoes
				]
			};

			var arrCampos = new Array();
			arrCampos.push(arrlinha1);
			arrCampos.push(arrlinha2);

			filters = new Ext.form.FormPanel({
				id: 'grid-filtros',
				title: "Filtros",
				region: 'center',
				bodyStyle: 'padding: 15px',
				labelAlign: 'top',
				items: [arrCampos],
				buttonAlign: "center",
				viewConfig: {
					forceFit: false
				},
				buttons:
				[
					{
						text: 'Pesquisar',
						type: 'submit',
						iconCls: 'icon-find',
						handler: function (b, e) {
							Pesquisar();
						}
					},
					{
						text: 'Limpar',
						handler: function (b, e) {
							
							ds.removeAll();							
							storeChaves.removeAll(); 
						    storeVagoes.removeAll();
							
							grid.getStore().removeAll();
							grid.getStore().totalLength = 0;
							grid.getBottomToolbar().bind(grid.getStore());							 
							grid.getBottomToolbar().updateInfo();  
							Ext.getCmp("grid-filtros").getForm().reset();
						    
						    // -> limpa o checked do head
						    var view = Ext.getCmp('gridCteRaiz').getView();
						    var chkdiv = Ext.fly(view.innerHd).child(".x-grid3-hd-checker");
							chkdiv.removeClass("x-grid3-hd-checker-on");

							HabilitarCampos(false);
						},
						scope: this
					},
				{
					text: 'Gerar Pdf',
					id: 'gerarLotePdf',
					iconCls: 'icon-printer',
					name: 'gerarLotePdf',
					// disabled: true,
					handler: function (b, e) {
						var selected = smRaiz.getSelections();
						var idCte = "";
						var arrCte = new Array();

						for (var i = 0; i < selected.length; i++) {
							arrCte.push(selected[i].data.CteId);
						}
						
						if(selected.length > 0){
							var url = "<%= Url.Action("GerarZipPdf") %> ?ids=" + arrCte
							window.open(url,"Zip");
						}
						else {
							Ext.Msg.show({
								title: "Mensagem de Erro",
								msg: "N�o existe item(s) selecionado(s) para gerar os arquivos .pdf.<br/> Por favor, selecione o(s) item(s) para gera-los novamente.",
								buttons: Ext.Msg.OK,
								icon: Ext.MessageBox.ERROR,
								minWidth: 200
							});
							return;
						}
					},
					scope: this
				},{
					text: 'Gerar Xml',
					id: 'gerarLoteXml',
					iconCls: 'icon-printer',
					name: 'gerarLoteXml',
					// disabled: true,
					handler: function (b, e) {
						var selected = smRaiz.getSelections();
						var idCte = "";
						var arrCte = new Array();

						for (var i = 0; i < selected.length; i++) {
							arrCte.push(selected[i].data.CteId);
						}
						
						if(selected.length > 0){
							var url = "<%= Url.Action("GerarZipXml") %> ?ids=" + arrCte
							window.open(url,"Zip");
						}
						else {
							Ext.Msg.show({
								title: "Mensagem de Erro",
								msg: "N�o existe item(s) selecionado(s) para gerar os arquivos .xml.<br/> Por favor, selecione o(s) item(s) para gera-los novamente.",
								buttons: Ext.Msg.OK,
								icon: Ext.MessageBox.ERROR,
								minWidth: 200
							});
							return;
						}
					},
					scope: this
				}]
			});

			var colunaDet1 = {
				title: 'CTe Raiz',
				width: 850,
				height: 320,
				layout: 'form',
				border: true,
				items: [grid]
			};

			var columns = {
				layout: 'column',
				border: false,
				autoScroll: true,
				region: 'center',
				items: [colunaDet1]
			};

			new Ext.Viewport({
				layout: 'border',
				margins: 10,
				items: [
					{
						region: 'north',
						height: 240,
						width: 700,
						items: [{
							region: 'center',
							applyTo: 'header-content'
						}, filters]
					},
					columns
			]
			});

		});
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div id="header-content">
        <h1>
            CTe - Gera��o arquivo</h1>
        <small>Voc� est� em CTe > Gera��o arquivo</small>
        <br />
    </div>
</asp:Content>
