﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Interna.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content runat="server" ID="HeadContent" ContentPlaceHolderID="HeadContent">
    <link rel="stylesheet" href="https://docs.microsoft.com/_themes/docs.theme/master/pt-br/_themes/css/2f45b070.site-ltr.css">
    <link rel="stylesheet" href="https://docs.microsoft.com/_themes/docs.theme/master/pt-br/_themes/css/fbd113ac.conceptual.css">

      <script type="text/javascript">

          $(function () {

              $("version").text("3.3.2.1");
          });

      </script>

      
</asp:Content>
<asp:Content runat="server" ID="Content2" ContentPlaceHolderID="Content">
    <div id="header-content">
        <h1>
            Release Notes</h1>
        <small>Você está em Configuração > Release Notes</small>
        <br />
        <br />
        <div id="main" role="main" class="content" data-bi-name="content" lang="pt-br" dir="ltr" style="margin: 20px">
            <ul class="metadata page-metadata" data-bi-name="page info" lang="pt-br" dir="ltr">
                <li class="displayDate">
                    <time role="presentation" datetime="2018-12-11T00:00:00.000Z" data-article-date-source="ms.date"
                        class="x-hidden-focus">23/01/2019</time>
                </li>
            </ul>
            <h1 id="img-srcmediavslogo4svg-altvisual-studio-icon-visual-studio-2017-version-159-release-notes"
                class="">
                <span data-ttu-id="6f048-106" translation="Visual Studio 2017 version 15.9 Release Notes">
                    <strong class="x-hidden-focus">Notas sobre a versão do Translogic 2019 versão <version>1.1</version></strong></span><span
                        class="sxs-lookup"><span data-stu-id="6f048-106"> </span></span>
            </h1>
            <hr>
            <h2 id="whats-new-in-159" class="heading-with-anchor">
                <span data-ttu-id="6f048-107" ><strong class="">Novidades da <version>1.1</version></strong></span>
                <span class="sxs-lookup">
                <span data-stu-id="6f048-107"></span></span></h2>
       
            <hr>
            <h3 id="summary-of-notable-new-features-in-159" class="heading-with-anchor">
                <span data-ttu-id="6f048-116">
                    Resumo dos novos recursos importantes na versão <version>1.1</version> </span></h3>
            <ul>
                <li><span data-ttu-id="6f048-117">
                    Agora você pode visualizar a versão atual do Translogic e as versões de cada módulo na página presente.</span>
                </li>
                <li><span data-ttu-id="6f048-117">
                     Correções de bugs do módulo EDI e módulo Core referente a JobRunner e EdiRunner.</span>
                </li>
                <li><span data-ttu-id="6f048-117">
                     Correções de bugs do módulo Core.</span>
                </li>
                        
            </ul>
            <h3 id="top-issues-fixed-in-159" class="heading-with-anchor">
                <span data-ttu-id="6f048-135" >
                <strong>Principais problemas corrigidos na versão <version>1.1</version></strong></span></h3>
            <ul>
                <li><span data-ttu-id="6f048-136">
                    Ajustes no serviço EdiErro devido a erro de timeout em pesquisa da tela 1380, aba erros.
                </span></li>
                <li><span data-ttu-id="6f048-136">
                    TestCases de EdiErroRepository e EdiErroService.
                </span></li>
                <li><span data-ttu-id="6f048-136">
                    Correção de encoding da tela 1131.
                </span></li>
                <li><span data-ttu-id="6f048-136">
                    Ajuste de geração de XML para envio de carregamento.
                </span></li>
                
            </ul>
            <hr style="">
            <h2 id="details-of-whats-new-in-159" class="heading-with-anchor">
                <span data-ttu-id="6f048-148">Detalhes das novidades na versão <version>1.1</version> por módulos</span></h2>
            <h3 class="heading-with-anchor">
                <span data-ttu-id="6f048-160">Core - <version>1.1</version></span></h3>
            <ul>
                <li><span data-ttu-id="6f048-161">
                    Adicionamos o recurso de visualizar a versão do Translogic na página presente.</span></li>
                
                referente a JobRunner e EdiRunner
                
            </ul>
            <h3 id="f" class="heading-with-anchor">
                <span data-ttu-id="6f048-176">EDI - 1.0.2.1</span></h3>
                <ul>
                <li><span data-ttu-id="6f048-161">
                    Criação de coluna DespachoId na tabela Consolidado.</span></li>
                    <li><span data-ttu-id="6f048-161">
                        Código para gerar cada nota de cada carregamento para cada vagão.</span></li>    
                    
                
            </ul>
              <a href="https://semver.org/lang/pt-BR/" target="_blank">Ref: Versionamento Semântico 2.0.0</a>
        </div>

      
    </div>
</asp:Content>
