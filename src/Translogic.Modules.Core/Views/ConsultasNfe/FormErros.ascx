﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%@ Import Namespace="System.Globalization" %>
<%@ Import Namespace="Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico" %>
<div id="div-form-status-Erros">
</div>
<script type="text/javascript">

		var gridErros = new Ext.grid.GridPanel({
			viewConfig: {
				forceFit: true
			},
			height: 270,
			width: 800,
			stripeRows: true,
			region: 'center',
			store: storeErros,
			autoScroll: true,
			loadMask: { msg: App.Resources.Web.Carregando },
			colModel: new Ext.grid.ColumnModel({
				defaults: {
					sortable: false
				},
				columns: [
							new Ext.grid.RowNumberer(),
							{ header: 'Status', dataIndex: "CodigoErro", sortable: false, width: 50,renderer:statusRenderer },
							{ header: 'NFe', dataIndex: "chave", sortable: false, width: 300 },
							{ header: 'Mensagem', dataIndex: "Mensagem", sortable: false, width: 450}
						 ]
			})
		});

		function statusRenderer(val)
		{
			switch(val){
				case 0:
					return "<img src='<%=Url.Images("Icons/tick.png") %>' >";
					break;
				default:
					return "<img src='<%=Url.Images("Icons/cancel.png") %>' >";
					break;
			}
		}  

		var formNfeErros = new Ext.form.FormPanel
	    ({
	        id: 'formNfeErros',
	        labelWidth: 80,
	        width: 850,
	        bodyStyle: 'padding: 15px',
	        labelAlign: 'top',
	        items: [gridErros],
	        buttonAlign: "center",
	        buttons:
		    [{
		        text: "Fechar",
		        handler: function () {
					windowStatusNFe.close();
		        }
		    }]
	        });

		 formNfeErros.render("div-form-status-Erros");
</script>