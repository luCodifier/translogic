﻿<%@ Page Title="" Language="C#" Inherits="System.Web.Mvc.ViewPage" MasterPageFile="~/Views/Shared/Interna.master" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server">
	<script type="text/javascript">
	    var store = new Ext.data.JsonStore({
	        root: "Items",
	        url: '<%= Url.Action("ObterNotasAlteradas") %>',
	        fields: [
				'Id',
				'AlteracaoConferida',
				'ChaveNfe',
				'PesoAntigo',
	            'PesoAtual',
	            'VolumeAntigo',
	            'VolumeAtual',
	            'NaturezaDaOperacao',
	            'MatriculaUsuario',
	            'DataAlteracao',
			]
	    });

	    var checkColum = new Ext.grid.CheckColumn({ header: 'Conferido', dataIndex: "AlteracaoConferida", width: 60, sortable: false, id: 'check' });
	    
	    var gridNotasAlterada = new Ext.grid.GridPanel({
	        stripeRows: true,
	        height: 460,
	        width: 1000,
	        region: 'center',
	        store: store,
	        plugins: [checkColum],
	        loadMask: { msg: App.Resources.Web.Carregando },
	        colModel: new Ext.grid.ColumnModel({
	            defaults: {
	                sortable: false
	            },
                columns: [
	                { dataIndex: "Id", hidden: true },
			        checkColum,
			        { header: 'Chave da NFe', dataIndex: "ChaveNfe", width: 300, sortable: false },
			        { header: 'Peso Antigo', dataIndex: "PesoAntigo", width: 80, sortable: false },
			        { header: 'Peso Atual', dataIndex: "PesoAtual", width: 80, sortable: false },
			        { header: 'Volume Antigo', dataIndex: "VolumeAntigo", width: 80, sortable: false },
			        { header: 'Volume Atual', dataIndex: "VolumeAtual", width: 80, sortable: false },
			        { header: 'Natureza', dataIndex: "NaturezaDaOperacao", width: 70, sortable: false },
			        { header: 'Matrícula', dataIndex: "MatriculaUsuario", width: 80, sortable: false },
			        { header: 'Data', dataIndex: "DataAlteracao", width: 120, sortable: false }
		        ]
	        })
        });
        
        /****************************************************/

        var btnSalvar = new Ext.Button({
            id: 'btnSalvar',
            name: 'btnSalvar',
            text: 'Alteração Recebida',
            width: 200,
            iconCls: 'icon-save',
            handler: function (b, e) {
                Salvar();
            }
        });

        var btnCancelar = new Ext.Button({
            id: 'btnCancelar',
            name: 'btnCancelar',
            text: 'Cancelar',
            width: 100,
            iconCls: 'icon-cancel',
            handler: function (b, e) {
                Cancelar();
            }
        });
	    
        var colSalvar = {
            layout: 'form',
            border: false,
            items: [btnSalvar]
        };

        var colCancelar = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding-left:5px',
            items: [btnCancelar]
        };

        var linhaBotoes = {
            layout: 'column',
            border: false,
            bodyStyle: 'margin: 5px',
            items: [colSalvar, colCancelar]
        };

        var linhaGrid = {
            layout: 'column',
            border: false,
            bodyStyle: 'margin: 5px',
            items: [gridNotasAlterada]
        };

        var painel = new Ext.form.FormPanel({
            id: 'painel',
            autoWidth: true,
            autoHeight: true,
            title: " Consulta de NFe Alterada",
            region: 'center',
            bodyStyle: 'padding: 15px',
            labelAlign: 'top',
            items: [linhaGrid, linhaBotoes],
            buttonAlign: "center"
        });
	    
	    /****************************************************/

        Ext.onReady(function () {
            store.load();
	        painel.render(document.body);
	    });

	    /****************************************************/

        function Salvar() {
            var ids = [];
            
            store.each(function (record) {
                if(record.get("AlteracaoConferida"))
                    ids.push(record.get("Id"));
            });

            if (ids.length == 0)
                return;
            
            Ext.getBody().mask("Salvando NFe na base de dados.", "x-mask-loading");
            
            Ext.Ajax.request({ 
				url: "<% =Url.Action("ConferirAlteracaoNfe") %>",
				success: function(response, options) {
						var result = Ext.decode(response.responseText);
                        
                        if(result.Sucesso) {
                            Ext.Msg.alert("Sucesso", result.Mensagem);
                            store.load();
                        }
                        else {
                            Ext.Msg.alert("Erro", result.Mensagem);
                        }	
					    
					    Ext.getBody().unmask();
				},
				dataType: 'json',		        
				failure: function(conn, data){
					Ext.Msg.alert("Mensagem de Erro","Ocorreu um erro inesperado.");
					Ext.getBody().unmask();
				},
				method: "POST",
				params: { idsHistoricoNfe: ids }
			});
        }

        function Cancelar() {
            store.each(function (record) { record.set("AlteracaoConferida", false); });
        }
        
	    /****************************************************/
        
    </script>
</asp:Content>
<asp:Content ID="MainContent" ContentPlaceHolderID="Content" runat="server">
	<div id="header-content">
		<h1>
			CTe - Consulta de NFe Alterada
        </h1>
		<small>Você está em CTe > Consulta de NFe Alterada</small>
		<br />
	</div>
</asp:Content>
