﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Interna.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content runat="server" ID="HeadContent" ContentPlaceHolderID="HeadContent">
    <div id="header-content">
        <h1>
            Consulta Nfe</h1>
        <small>Consulta de Fluxos Associados a NFe</small>
        <br />
    </div>
    <script type="text/javascript" language="javascript">

        /* region :: Functions */
        function fctValidarChaveNfe() {
            var chaveConsulta = Ext.getCmp("Filtro-NFe").getValue();
            if (chaveConsulta.trim() == '') {
                Ext.Msg.alert("Mensagem de Informação", "Favor informar a chave da NFe!");
                return false;
            }

            return true;
        }

        function fctImprimirNfe() {
            var sucessoNfe = fctValidarChaveNfe();
            if (sucessoNfe) {
                if (sucessoNfe == false) {
                    return false;
                }
            } else {
                return false;
            }

            var parametros = {
                chavesNfe : undefined
            };
            
            var chavesConsulta = Ext.getCmp("Filtro-NFe").getValue();
            parametros.chavesNfe = chavesConsulta;

            Ext.getBody().mask("Buscando os PDFs das Notas Fiscais. Aguarde um instante...", "x-mask-loading");

            $.ajax({
                url: '<%= Url.Action("ImprimirNfe") %>',
                type: 'post',
                cache: false,
                data: { "chavesNfe" : parametros.chavesNfe },
                dataType: "json",
                success: function (response) {
                    if (response.Success === true) {
                            var url = '<%= this.Url.Action("DownloadPdfNfe") %>';
                            url += "?chavesNfe=" + chavesConsulta;
                            window.open(url, "");
                    } else {
                        Ext.Msg.show({
                            title: 'Aviso',
                            msg: 'Falha na exportação do arquivo :: ' + response.Message,
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.ERROR
                        });
                    }
                },
                error: function (response) {
                    Ext.Msg.show({
                        title: 'Aviso',
                        msg: 'Falha na exportação do arquivo',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.ERROR
                    });
                },
                complete: function (data) {
                    Ext.getBody().unmask();
                }
            });
        }

        function fctPesquisar()
		{
            var sucessoNfe = fctValidarChaveNfe();
            if (sucessoNfe) {
                if (sucessoNfe == false) {
                    return false;
                }
            } else {
                return false;
            }

			var chavesConsulta = Ext.getCmp("Filtro-NFe").getValue();
            var tipoFluxo = Ext.getCmp("rdTipoFluxo").getValue().getRawValue();
            AlterarEstadoCampoPesquisa(false);

			Ext.getBody().mask("Buscando os fluxos associados...", "x-mask-loading");

			Ext.Ajax.request({ 
                url: '<%= this.Url.Action("PesquisarFluxosAssociadosChaveNfe") %>',
				success: function(response, options) {
						var result = Ext.decode(response.responseText);

						if(result.Sucesso == false)
						{
                            AlterarEstadoCampoPesquisa(true);
							Ext.Msg.alert("Mensagem de Erro",result.Mensagem);
						}
						else{
						    fctPreencherGridResultado(result);
						}
						Ext.getBody().unmask();
				},
				dataType: 'json',		        
				failure: function(conn, data){
                    AlterarEstadoCampoPesquisa(true);
					Ext.Msg.alert("Mensagem de Erro","Ocorreu um erro inesperado.");
					Ext.getBody().unmask();
				},
				method: "POST",
				params: { chavesNfe: chavesConsulta, somenteVigentes: (tipoFluxo == 1 ? true : false), exportarExcel: false }
			});

		}

		function fctPreencherGridResultado(store) {
		    grid.getStore().loadData(store);
		}

        function fctReiniciarPesquisa() {
            grid.getStore().removeAll();
            AlterarEstadoCampoPesquisa(true);
            Ext.getCmp("filtros").getForm().reset();
            Ext.getCmp("Filtro-NFe").setValue("");
        }

        function AlterarEstadoCampoPesquisa(habilitar) {
            if (habilitar == true) {
                Ext.getCmp("Filtro-NFe").enable();
                Ext.getCmp("btnBuscarNfe").enable();
            }
            else {
                Ext.getCmp("Filtro-NFe").disable();
            }
        }

        /* endregion :: Functions */


        /* region :: Filtros */

        var FieldNfe = new Ext.form.TextField({
            vtype: 'textfield',
            id: 'Filtro-NFe',
            fieldLabel: 'Chave(s) NFe(s): (separados por ";")',
            name: 'Filtro-NFe',
            allowBlank: false,
            autoCreate: { tag: 'input', type: 'text', autocomplete: 'off' },
            width: 320,
            hiddenName: 'Filtro-NFe',
            listeners:
            {
                specialKey: function (field, e) {
                    if (e.getKey() == e.ENTER) {
                        fctPesquisar();
                    }
                }
            }
        });


        var colNfe = {
            width: 330,
            layout: 'form',
            border: false,
            items: [FieldNfe]
        };


        var rdTipoFluxo = {
            xtype: 'radiogroup',
            fieldLabel: 'Tipo de Fluxo',
            id: 'rdTipoFluxo',
            width: 170,
            name: 'rdTipoFluxo',
            items: [
		            { boxLabel: 'Vigentes', name: 'tipoImpressao', inputValue: 1, checked: true },
		            { boxLabel: 'Todos', name: 'tipoImpressao', inputValue: 0 }
		        ]
        };

        var colTipoFluxo = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding-right:10px',
            items: [rdTipoFluxo]
        };

        var btnBuscarNfe = new Ext.Button({
            id: 'btnBuscarNfe',
            name: 'btnBuscarNfe',
            text: 'Buscar',
            width: 100,
            iconCls: 'icon-find',
            handler: function (b, e) {
                fctPesquisar();
            }
        });

        var btnLimpar = new Ext.Button({
            id: 'btnLimpar',
            name: 'btnLimpar',
            text: 'Limpar',
            width: 100,
            handler: function (b, e) {
                fctReiniciarPesquisa();
            }
        });
        
        var btnImprimirNfe = new Ext.Button({
            id: 'btnImprimirNfe',
            name: 'btnImprimirNfe',
            text: 'Imprimir NFe',
            width: 50,
            iconCls: 'icon-pdf',
            handler: function (b, e) {
                fctImprimirNfe();
            }
        });

        var colBuscarNfe = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding-top:17px',
            items: [btnBuscarNfe]
        };

        var colLimpar = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding-top:17px',
            items: [btnLimpar]
        };

        var colImprimirNfe = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding-top:17px',
            items: [btnImprimirNfe]
        };

        var arrlinha1 = {
            layout: 'column',
            border: false,
            width: 900,
            items: [colNfe, colTipoFluxo, colBuscarNfe, colLimpar, colImprimirNfe]
        };
        
        //------------------------------//

        var arrCampos = new Array();
        arrCampos.push(arrlinha1);

        var filtros = new Ext.form.FormPanel({
            id: 'filtros',
            title: 'Filtros',
            name: 'filtros',
            region: 'center',
            bodyStyle: 'padding: 15px',
            initEl: function (el) {
                this.el = Ext.get(el);
                this.id = this.el.id || Ext.id();
                if (this.standardSubmit) {
                    this.el.dom.action = this.url;
                } else {
                    this.el.on('submit', this.onSubmit, this);
                }
                this.el.addClass('x-form');
            },
            labelAlign: 'top',
            border: false,
            items: [arrCampos],
            buttonAlign: 'center'
        });

        /* endregion :: Filtros*/

        /* region :: Grid */

        grid = new Translogic.PaginatedGrid({
            autoLoadGrid: false,
            id: "grid",
            stripeRows: true,
            region: 'center',
            width: "100%",
            height: 400,
            autoScroll: true,
            loadMask: { msg: App.Resources.Web.Carregando },
            filteringToolbar: [
                {
                    id: 'btnExportar',
                    text: 'Exportar',
                    tooltip: 'Exportar',
                    iconCls: 'icon-page-excel',
                    handler: function(c) {
                        var sucessoNfe = fctValidarChaveNfe();
                        if (sucessoNfe) {
                            if (sucessoNfe == true) {
                                var chavesConsulta = Ext.getCmp("Filtro-NFe").getValue();
                                var tipoFluxo = Ext.getCmp("rdTipoFluxo").getValue().getRawValue();

                                var somenteVigentes = (tipoFluxo == 1 ? true : false);

                                var url = "<%= Url.Action("PesquisarFluxosAssociadosChaveNfe") %>";
                                url += "?chavesNfe=" + chavesConsulta + "&somenteVigentes=" + somenteVigentes.toString() + "&exportarExcel=true";
                                window.open(url, "");
                            }
                        }
                    }
                }
            ],
            url: '<%= this.Url.Action("PesquisarFluxosAssociadosChaveNfe") %>',
            viewConfig: {
                forceFit: false,
                emptyText: 'Não possui registros para exibição.'
            },
            fields: [
                'ChaveNfe',
                'Fluxo',
                'Origem',
                'Destino',
                'RemetenteFiscalCnpj',
                'RemetenteFiscal',
                'DestinatarioFiscalCnpj',
                'DestinatarioFiscal',
                'TomadorCnpj',
                'Tomador',
                'Expedidor',
                'Recebedor',
                'Mercadoria',
                'FluxoVigenciaFinal'
            ],
            columns: [
                new Ext.grid.RowNumberer(),
                { header: 'Chave NF-e', dataIndex: "ChaveNfe", width: 280, sortable: false },
                { header: 'Fluxo', dataIndex: "Fluxo", width: 60, sortable: false },
                { header: 'Vigência', dataIndex: "FluxoVigenciaFinal", width: 80, sortable: false },
                { header: 'Origem', dataIndex: "Origem", width: 60, sortable: false },
                { header: 'Destino', dataIndex: "Destino", width: 60, sortable: false },
                { header: 'Expedidor', dataIndex: "Expedidor", width: 170, sortable: false },
                { header: 'Recebedor', dataIndex: "Recebedor", width: 170, sortable: false },
                { header: 'Mercadoria', dataIndex: "Mercadoria", width: 120, sortable: false },
                { header: 'CNPJ', dataIndex: "RemetenteFiscalCnpj", width: 120, sortable: false },
                { header: 'Remetente Fiscal', dataIndex: "RemetenteFiscal", width: 170, sortable: false },
                { header: 'CNPJ', dataIndex: "DestinatarioFiscalCnpj", width: 120, sortable: false },
                { header: 'Destinatário Fiscal', dataIndex: "DestinatarioFiscal", width: 170, sortable: false },
                { header: 'CNPJ', dataIndex: "TomadorCnpj", width: 120, sortable: false },
                { header: 'Tomador', dataIndex: "Tomador", width: 170, sortable: false }
            ]
        });

        grid.getStore().proxy.on('beforeload', function (p, params) {
            params['exportarExcel'] = false;
        });

        /* endregion :: Grid */

        /* region :: Render */

        new Ext.Viewport({
				layout: 'border',
				margins: 10,
				items: [
		            {
				    		region: 'north',
				    		height: 140,
				    		width: 1065,
				    		autoScroll: true,
				    		items: [
				    		    {
				    			    region: 'center',
				    			    applyTo: 'header-content'
			                    }, 
				    		    filtros
				    		]
				    },
				    grid
			    ]
	    });

    </script>
</asp:Content>
