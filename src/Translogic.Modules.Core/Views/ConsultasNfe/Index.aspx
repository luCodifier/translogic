﻿<%@ Page Title="" Language="C#" Inherits="System.Web.Mvc.ViewPage" MasterPageFile="~/Views/Shared/Interna.master" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server">
    <% Html.RenderPartial("Autorizacao"); %>

    <style type="text/css">
        .x-grid3-cell-inner, .x-grid3-hd-inner
        {
            white-space: normal;
        }
    </style>
    <script type="text/javascript">
    // **************************** ABA DE BAIXAR NOTAS DO SIMCONSULTAS ********************************//
    var verificadorFaltantes;
    var bPodeEstornarSaldoNfe;
    var windowEstornoAutorizacao = null;
    var bCancelouEstornoSaldoNfe;
    var autorizadoPorEstorno = "";
    var justificativaEstorno = "";
    var bMostrarTelaAutorizacao = false;
    
    var storeNfeBaixando = new Ext.data.JsonStore({
        id: 'storeNfeBaixando',
        name: 'storeNfeBaixando',
        url: '<%=Url.Action("ConsultarProcessoNfe") %>',
        fields: ['Chave', 'Mensagem', 'Status']
    });

    var notasEdiErroRegularizar = '';
    
    
    <% if (ViewData["NotasEdiErroRegularizar"] != null) { %>
        notasEdiErroRegularizar = '<%= ViewData["NotasEdiErroRegularizar"].ToString() %>';
    <% } %>

    var txtChavesParaBaixar = new Ext.form.TextArea ({
        fieldLabel: 'Informe as chaves das NFes separados por ; ou linhas',
        maxLength: 1800,
        width: 390,
        height: 300,
        name: 'txtChavesParaBaixar',
        id: 'txtChavesParaBaixar',
        allowBlank: false,
        value: notasEdiErroRegularizar
    });
    
    var lbNotasBrado = {
            xtype: 'label',
            fieldLabel: '',
            text: 'Notas clientes Brado',
            cls: 'lbNotasBrado',
            id: 'lbNotasBrado'
        };
        
    var chkNotasBrado = {
            xtype: 'checkbox',
            fieldLabel: '',
            id: 'chkNotasBrado',
            cls: 'chkNotasBrado',
            name: 'chkNotasBrado',
            validateField: true,
            value: false
        };

    var gridNfeBaixando = new Ext.grid.GridPanel({
		height: 360,
		width: 720,
		stripeRows: true,
		region: 'center',
		store: storeNfeBaixando,
		autoScroll: true,
		colModel: new Ext.grid.ColumnModel({
			columns: [
				new Ext.grid.RowNumberer(),
				{ header: 'NFes', dataIndex: "Chave", sortable: false, width: 300 },
			    { header: 'Status', dataIndex: "Status", sortable: false, width: 30, renderer: StatusRenderer },
			    { header: 'Erro', dataIndex: "Mensagem", sortable: false, width: 350 }
			]
		})
	});
    
    gridNfeBaixando.getStore().proxy.on('beforeload', function (p, params) {
        params['chaves'] = Ext.getCmp("txtChavesParaBaixar").getValue();
        params['chavesQueJaDeramErro'] = LerErrosDaGrid();
    });
    
    function StatusRenderer(val)
    {
        switch (val) {
            case "OK":
                return "<img src='<%=Url.Images("Icons/tick.png") %>' >";
            case "ERRO":
                return "<img src='<%=Url.Images("Icons/cross.gif") %>' >";
            default:
                return "<img src='<%=Url.Images("loading.gif") %>' >";
        }
    }
    
    function LerErrosDaGrid() {
        var result = [];
        
        gridNfeBaixando.getStore().each(function (record) {
            if (record.data.Mensagem != null && record.data.Mensagem != "")
                result.push(record.data.Chave + "|" +  record.data.Mensagem);
        });

        return result;
    }

    var btnBaixarNfes = {
        xtype: "button",
        name: 'btnBaixarNfes',
        id: 'btnBaixarNfes',
        text: 'Baixar NFes',
        width: 100,
        handler: ProcessarNotas
    };
    
    var btnLimparTelaBaixarNfes = {
        xtype: "button",
        name: 'btnLimparTelaBaixarNfes',
        id: 'btnLimparTelaBaixarNfes',
        text: 'Limpar',
        width: 100,
        handler: LimparTelaBaixarNfes
    };
    
    var btnEnviarErrosPorEmail = {
        xtype: "button",
        name: 'btnEnviarErrosPorEmail',
        id: 'btnEnviarErrosPorEmail',
        text: 'Enviar erros por e-mail',
        width: 100,
        disabled: true,
        hidden: true,
        handler: EnviarErrosPorEmail
    };
    
    var btnRetry = {
        xtype: "button",
        name: 'btnRetry',
        id: 'btnRetry',
        text: 'Baixar NFes com Erros',
        width: 100,
        disabled: true,
        handler: Retry
    };
    
    function Ajax(url, params, funcao) {
	    Ext.Ajax.request(
        {
            url: url,
            params: params,
            timeout: 300000,
            failure: function(conn, data){
                Ext.Msg.show({
                    title: 'Ops...',
                    msg: "Ocorreu um erro inesperado.",
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.ERROR
                });
			},
            success: function (response) {
                var result = Ext.decode(response.responseText);
                 
                if (result.success == false){
                    Ext.Msg.show({
                        title: 'Ops...',
                        msg: result.Message,
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.ERROR
                    });
                }
                else {
                    funcao(result);
                }
            }
        });
    }
    
    function Retry() {
        var chaves = "";

        gridNfeBaixando.getStore().each(function (record) {
            if (record.data.Mensagem != null && record.data.Mensagem != "")
                chaves += record.data.Chave + ";\n";
        });

        LimparTelaBaixarNfes();
        Ext.getCmp("txtChavesParaBaixar").setValue(chaves);
        ProcessarNotas();
    }

    function EnviarErrosPorEmail() {
        Ajax("<%=Url.Action("EnviarEmailErros")%>", { chavesComErros: LerErrosDaGrid() }, function(data) {
//            Ext.Msg.show({
//                title: 'Sucesso!',
//                msg: 'E-mail enviado! ',
//                buttons: Ext.Msg.OK,
//                icon: Ext.MessageBox.INFO
//            });
            
            Ext.getCmp("btnEnviarErrosPorEmail").disable();
        });
    }

    function ProcessarNotas() {
        Ext.getCmp("txtChavesParaBaixar").disable();
        Ext.getCmp("btnBaixarNfes").disable();
        Ext.getCmp("btnRetry").disable();
        var notasBrd = Ext.getCmp("chkNotasBrado").getValue();
        Ajax("<%=Url.Action("IniciarProcessoNfe")%>", { chaves: Ext.getCmp("txtChavesParaBaixar").getValue(), notasBrado: notasBrd }, function(data) {
            verificadorFaltantes = setInterval(VerificarFaltantes, 3000);
        }); 
    }

    function VerificarFaltantes() {
        var localStrore = gridNfeBaixando.getStore();
        localStrore.load({
            scope: this,
            callback: function(records, operation, success) {
                var total = localStrore.getCount();
                var erros = 0;
                var sucessos = 0;
                var pendentes = 0;

                for (var i = 0; i < total; i++) {
                    switch (records[i].data.Status) {
                        case "OK":
                            sucessos++;
                            break;
                        case "ERRO":
                            erros++;
                            break;
                        default:
                            pendentes++;
                            break;
                    }
                }
                
                if(pendentes == 0) {
                    PararProcesso();
                    
                    if(erros > 0) {
                        Ext.Msg.show({
                            title: 'Ops...',
                            msg: 'NFes processadas, porém algums apresentaram problemas! ',
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.ERROR
                        });
                        
                        Ext.getCmp("btnEnviarErrosPorEmail").enable();
                        Ext.getCmp("btnRetry").enable();
                        EnviarErrosPorEmail();
                    }
                    else {
                        Ext.Msg.show({
                            title: 'Sucesso!',
                            msg: 'NFes processadas com sucesso! ',
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.INFO
                        });

                        LimparTelaBaixarNfes();
                    }
                }
            }
        }); 
    }
    
    function PararProcesso() {
        clearInterval(verificadorFaltantes);
    }
    
    function LimparTelaBaixarNfes() {        
        PararProcesso();
        Ext.getCmp("txtChavesParaBaixar").setValue("");
        Ext.getCmp("txtChavesParaBaixar").enable();
        Ext.getCmp("btnBaixarNfes").enable();
        Ext.getCmp("btnEnviarErrosPorEmail").disable();
        Ext.getCmp("btnRetry").disable();
        gridNfeBaixando.getStore().removeAll();
    }
    
    var epacamento = { xtype: 'label', html: '&nbsp;&nbsp;' };
    
    var c1BaixarNfe = {
        layout: 'form',
        bodyStyle: 'padding: 5px',
        border: false,
        items: [txtChavesParaBaixar]
    };
    
    var arrlbNotasBrado = {
            width: 120,
            layout: 'form',
            border: false,
            items: [lbNotasBrado]
        };
    
    var arrNotasBrado = {
            width: 150,
            layout: 'column',
            border: false,
            items: [chkNotasBrado, lbNotasBrado]
        };
    
    var lBotoesBaixarNfe = {
	    layout: 'column',
        bodyStyle: 'padding: 5px',
	    border: false,
	    items: [btnBaixarNfes, epacamento, btnLimparTelaBaixarNfes, epacamento, btnRetry, btnEnviarErrosPorEmail]
	};
    
    var fsC1BaixarNfe = {
        xtype: 'fieldset',
        layout: 'form',
        title: 'Chaves das NFes que serão carregadas',
        items: [arrNotasBrado, c1BaixarNfe, lBotoesBaixarNfe]
    };
    
    var c2BaixarNfe = {
	    layout: 'form',
	    border: false,
	    bodyStyle: 'padding: 5px',
	    items: [gridNfeBaixando]
	};
    
    var fsC2BaixarNfe = {
        xtype: 'fieldset',
	    layout: 'form',
        title: 'Situação das NFes processadas',
	    items: [c2BaixarNfe]
	};
    
    var l1BaixarNfe = {
	    layout: 'column',
	    border: false,
	    items: [fsC1BaixarNfe, epacamento, fsC2BaixarNfe]
	};
    
    /******************************************************************/
    
    var filtros = new Ext.form.FormPanel({
        id: 'panel-notas',
        title: "Carregamento de NFe",
        region: 'center',
        bodyStyle: 'padding: 5px',
        labelAlign: 'top',
        items: [l1BaixarNfe],
        buttonAlign: "center"          
    });
    
    /******************************************************************/
            
        var arrFieldsNfe = new Array();
            
        function fctHabilitarEdicaoNfe() {
            btnSalvarNfe.show();
            btnCancelarNfe.show();

            Ext.getCmp("field-Volume").enable();
            Ext.getCmp("field-PesoBruto").enable();

            // Caso possua permissão para alterar o peso a liberar e volume a liberar
            Ext.getCmp("field-PesoLiberar").disable();
            Ext.getCmp("field-VolumeLiberar").disable();
            if (bPodeEstornarSaldoNfe) {
                if (bPodeEstornarSaldoNfe === true) {
                    Ext.getCmp("field-PesoLiberar").enable();
                    Ext.getCmp("field-VolumeLiberar").enable();
                }
            }

            Ext.getCmp("field-PesoBruto").focus();
        }
            
        function fctDesabilitarEdicaoNfe() {
            btnSalvarNfe.hide();
            btnCancelarNfe.hide();
                
            Ext.getCmp("field-Volume").disable();
            Ext.getCmp("field-PesoBruto").disable();

            Ext.getCmp("field-PesoLiberar").setValue("0,000");
            Ext.getCmp("field-VolumeLiberar").setValue("0,000");
            
            Ext.getCmp("field-PesoLiberar").disable();
            Ext.getCmp("field-VolumeLiberar").disable();
        }
            
        function fctMostrarAlterar() {
            btnAlterarNfe.show();
        }
            
        function fctEsconderAlterar() {
            btnAlterarNfe.hide();
        }

        function fctCarregarTelaAutorizacao(volume, peso, volumeLiberar, pesoLiberar) {            
            windowEstornoAutorizacao = null;

            windowEstornoAutorizacao = new Ext.Window({
                name: 'windowEstornoAutorizacao',
                id: 'windowEstornoAutorizacao',
                title: "Estorno de Saldo de NFe",
                expandonShow: true,
                modal: true,
                width: 400,
                height: 500,
                closable: true,
                closeAction: 'hide',
                    items: [painelAutorizadoPor, painelJustificativa],
                    listeners: {
                        'beforeshow': function() {
                            Ext.getCmp("txtAutorizadoPor").setValue("");
                            Ext.getCmp("txtJustificativa").setValue("");
                        },
                        'hide': function() {
                            bCancelouEstornoSaldoNfe = flagCancelouEstornoSaldo;
                            if (bCancelouEstornoSaldoNfe == false) {
                                // Recuperar o campo Autorizado Por e a Justificativa
                                autorizadoPorEstorno = Ext.getCmp("txtAutorizadoPor").getValue();
                                justificativaEstorno = Ext.getCmp("txtJustificativa").getValue();

                                Ext.getBody().mask("Salvando NFe na base de dados.", "x-mask-loading");

                                Ext.Ajax.request({ 
	                                url: "<% =Url.Action("SalvarAlteracoesNota") %>",
	                                success: function(response, options) {
			                                var result = Ext.decode(response.responseText);

			                                if(result.Sucesso) {
				                                Ext.Msg.alert("Sucesso", result.Mensagem);
				                                fctDesabilitarEdicaoNfe();
				                                fctBuscarNfe();
                                                bMostrarTelaAutorizacao = false;
			                                }
			                                else {
				                                Ext.Msg.alert("Erro", result.Mensagem);
			                                }	

			                                Ext.getBody().unmask();
	                                },
	                                dataType: 'json',		        
	                                failure: function(conn, data){
		                                Ext.Msg.alert("Mensagem de Erro","Ocorreu um erro inesperado.");
		                                Ext.getBody().unmask();
	                                },
	                                method: "POST",
	                                params: { 
	                                novoVolume: volume.getValue(), 
	                                novoPeso: peso.getValue(),
	                                chaveNfe: FieldNfe.getValue(),
	                                novoPesoLiberar: pesoLiberar.getValue(),
	                                novoVolumeLiberar: volumeLiberar.getValue(),
                                    teveLiberacaoSaldoNfe: true,
	                                autorizadoPorEstorno: autorizadoPorEstorno,
	                                justificativaEstorno: justificativaEstorno }
                                });

                            }
                        }
                    }
            });

            windowEstornoAutorizacao.show();
        }
            
        function fctSalvarVolumePesoNfe() {
            var volume = Ext.getCmp("field-Volume");
            var peso = Ext.getCmp("field-PesoBruto");
            
            var volumeLiberar = Ext.getCmp("field-VolumeLiberar");
            var pesoLiberar = Ext.getCmp("field-PesoLiberar");

            if (volume.getValue() == "0.000" && peso.getValue() == "0.000") {
                Ext.Msg.alert("Aviso", "Preencha ao menos o Volume ou o Peso Bruto corretamente!") ;
                return;
            }

            if (pesoLiberar.getValue() > peso.getValue()) {
                Ext.Msg.alert("Aviso", "O Peso a Liberar não pode exceder o valor do Peso Bruto!") ;
                return;
            }

            if (volumeLiberar.getValue() > volume.getValue()) {
                Ext.Msg.alert("Aviso", "O Volume a Liberar não pode exceder o valor do Volume!") ;
                return;
            }
                
            if (Ext.Msg.confirm("Alteração", "Sua matrícula está sendo vinculada na auditoria da alteração dessa NFe! Deseja prosseguir com a alteração?", function(btn, text) {
				if (btn == 'yes') {

                    // Caso tenha alterado os campos de peso a liberar ou volume a liberar
				    //console.log(pesoLiberar);
                    ///console.log(volumeLiberar);
                    if ((bMostrarTelaAutorizacao == true) && ((pesoLiberar.getValue() != "0.000") || (volumeLiberar.getValue() != "0.000")) ) {
                        fctCarregarTelaAutorizacao(volume, peso, volumeLiberar, pesoLiberar);
                    } else {
                        
                        Ext.getBody().mask("Salvando NFe na base de dados.", "x-mask-loading");

                        Ext.Ajax.request({ 
	                        url: "<% =Url.Action("SalvarAlteracoesNota") %>",
	                        success: function(response, options) {
			                        var result = Ext.decode(response.responseText);

			                        if(result.Sucesso) {
				                        Ext.Msg.alert("Sucesso", result.Mensagem);
				                        fctDesabilitarEdicaoNfe();
				                        fctBuscarNfe();
			                        }
			                        else {
				                        Ext.Msg.alert("Erro", result.Mensagem);
			                        }	

			                        Ext.getBody().unmask();
	                        },
	                        dataType: 'json',		        
	                        failure: function(conn, data){
		                        Ext.Msg.alert("Mensagem de Erro","Ocorreu um erro inesperado.");
		                        Ext.getBody().unmask();
	                        },
	                        method: "POST",
	                        params: { 
	                        novoVolume: volume.getValue(), 
	                        novoPeso: peso.getValue(),
	                        chaveNfe: FieldNfe.getValue(),
	                        novoPesoLiberar: pesoLiberar.getValue(),
	                        novoVolumeLiberar: volumeLiberar.getValue(),
                            teveLiberacaoSaldoNfe: false,
	                        autorizadoPorEstorno: "",
	                        justificativaEstorno: "" }
                        });
                    }
				}
			}));
        }

        /******************************************************************/

        var btnAlterarNfe = new Ext.Button({
            id: 'btnAlterarNfe',
			name: 'btnAlterarNfe',				
			text: 'Alterar',
			width: 100,
			iconCls: 'icon-edit',
			handler: function (b, e) {
				fctHabilitarEdicaoNfe();
            }            
		});
            
        var btnSalvarNfe = new Ext.Button({
            id: 'btnSalvarNfe',
			name: 'btnSalvarNfe',				
			text: 'Salvar',
			width: 100,
			iconCls: 'icon-save',
			handler: function (b, e) {
				fctSalvarVolumePesoNfe();
            }            
		});
            
        var btnCancelarNfe = new Ext.Button({
            id: 'btnCancelarNfe',
			name: 'btnCancelarNfe',				
			text: 'Cancelar',
			width: 100,
			iconCls: 'icon-cancel',
			handler: function (b, e) {
				fctDesabilitarEdicaoNfe();
            }            
		});
            
		/******************************************************************/
            
		var btnBuscarNfe = new Ext.Button({
            id: 'btnBuscar',
			name: 'btnBuscar',				
			text: 'Buscar',
			width: 100,
			iconCls: 'icon-find',
			handler: function (b, e) {
				fctBuscarNfe();
            }            
		});
		
        var btnGerarPdf = new Ext.Button({
            id: 'btnGerarPdf',
			name: 'btnGerarPdf',				
			text: 'Gerar Pdf',
			width: 100,
			iconCls: 'icon-printer',
			handler: function (b, e) {
				fctGerarPdf();
            }            
		});

        function fctGerarPdf () {

            var chaveConsulta = Ext.getCmp("Filtro-NFe").getValue();
            if (chaveConsulta.trim() == '') {
                Ext.Msg.alert("Mensagem de Informação", "Favor informar a chave da NFe!");
                return;
            }

            if (!TipoChaveValido(chaveConsulta)) {
                Ext.Msg.alert("Mensagem de Informação", "Chave de NFe inválida, favor informar uma chave de modelo 55!");
                return;
            }

            var url = "<%= Url.Action("GerarPdf") %>";
            url += "?chaveNfe=" + chaveConsulta;

            window.open(url, "");
        }


        function TipoChaveValido(chave) {
			return chave.substring(20, 22) == "55";
		}

		function fctBuscarNfe()
		{
			var chaveConsulta = Ext.getCmp("Filtro-NFe").getValue();
			if(chaveConsulta.trim() == '')
			{
				Ext.Msg.alert("Mensagem de Informação","Favor informar a chave da NFe!") ;
				return;
			}

			if(!TipoChaveValido(chaveConsulta))
			{
				Ext.Msg.alert("Mensagem de Informação","Chave de NFe inválida, favor informar uma chave de modelo 55!") ;
				return;
			}
				
			Ext.getBody().mask("Buscando NFe na base de dados.", "x-mask-loading");

			Ext.Ajax.request({ 
				url: "<% =Url.Action("ObterDadosNfe") %>",
				success: function(response, options) {
						var result = Ext.decode(response.responseText);
							 
						fctLimparCampos();

                        bPodeEstornarSaldoNfe = false;
						if(result.Erro)
						{
							Ext.Msg.alert("Ocorreu um erro ao buscar os dados da nota",result.Mensagem);
						}
						else{
							fctPreencherCampos(result);
							fctDesabilitarEdicaoNfe();
							    
							if(result.PodeAlterar)
							    fctMostrarAlterar();

                            if (result.PodeEstornarSaldoNfe) {
                                bPodeEstornarSaldoNfe = true;
                            }
						}
						Ext.getBody().unmask();
				},
				dataType: 'json',		        
				failure: function(conn, data){
					Ext.Msg.alert("Mensagem de Erro","Ocorreu um erro inesperado.");
					Ext.getBody().unmask();
				},
				method: "POST",
				params: { chaveNfe: chaveConsulta }
			});

		}

		function fctPreencherCampos(store)
		{				
			Ext.getCmp("field-Numero").setValue(store.NumeroNotaFiscal);
			Ext.getCmp("field-Serie").setValue(store.SerieNotaFiscal);
			Ext.getCmp("field-DtEmissao").setValue(store.DataEmissao);
			Ext.getCmp("field-ValorTotalNfe").setValue(store.Valor);
			Ext.getCmp("txtOrigemNfe").setValue(store.Origem);
			Ext.getCmp("field-CNPJEmit").setValue(store.CnpjEmitente);
			Ext.getCmp("field-RazaoEmit").setValue(store.RazaoSocialEmitente);
			Ext.getCmp("field-InscEmit").setValue(store.InscricaoEstadualEmitente);
			Ext.getCmp("field-UFEmit").setValue(store.UfEmitente);
			Ext.getCmp("field-CNPJRem").setValue(store.CnpjDestinatario);
			Ext.getCmp("field-RazaoRem").setValue(store.RazaoSocialDestinatario);
			Ext.getCmp("field-InscRem").setValue(store.InscricaoEstadualDestinatario);
			Ext.getCmp("field-UFRem").setValue(store.UfDestinatario);
			Ext.getCmp("field-CNPJTrans").setValue(store.CnpjTransportadora);
			Ext.getCmp("field-RazaoTrans").setValue(store.NomeTransportadora);
			Ext.getCmp("field-MunicipioTrans").setValue(store.MunicipioTransportadora);
			Ext.getCmp("field-UFTrans").setValue(store.UfTransportadora);
			Ext.getCmp("field-PesoLiq").setValue(store.Peso);
			Ext.getCmp("field-PesoBruto").setValue(store.PesoBruto.toString().replace(",", "."));
			Ext.getCmp("field-Volume").setValue(store.Volume.toString().replace(",", "."));

			Ext.getCmp("field-PesoDisponivel").setValue(store.PesoDisponivel.toString().replace(",", "."));
			Ext.getCmp("field-VolumeDisponivel").setValue(store.VolumeDisponivel.toString().replace(",", "."));
			    
			gridDespachos.getStore().loadData(store);
		}

		function fctLimparCampos()
		{
			Ext.getCmp("field-Numero").setValue("");
			Ext.getCmp("field-Serie").setValue("");
			Ext.getCmp("field-DtEmissao").setValue("");
			Ext.getCmp("field-ValorTotalNfe").setValue("");
			Ext.getCmp("field-CNPJEmit").setValue("");
			Ext.getCmp("field-RazaoEmit").setValue("");
			Ext.getCmp("field-InscEmit").setValue("");
			Ext.getCmp("field-UFEmit").setValue("");
			Ext.getCmp("field-CNPJRem").setValue("");
			Ext.getCmp("field-RazaoRem").setValue("");
			Ext.getCmp("field-InscRem").setValue("");
			Ext.getCmp("field-UFRem").setValue("");
			Ext.getCmp("field-CNPJTrans").setValue("");
			Ext.getCmp("field-RazaoTrans").setValue("");
			Ext.getCmp("field-MunicipioTrans").setValue("");
			Ext.getCmp("field-UFTrans").setValue("");
			Ext.getCmp("field-PesoLiq").setValue("");
			Ext.getCmp("field-PesoBruto").setValue("");
			Ext.getCmp("field-Volume").setValue("");
		}

		var colBuscarNfe = {
            layout: 'form',
            border: false,
			bodyStyle: 'padding-top:17px',
            items: [btnBuscarNfe]
        };
		
		var colGerarPdf = {
            layout: 'form',
            border: false,
			bodyStyle: 'padding-top:17px',
            items: [btnGerarPdf]
        };
			
		var colSalvarNfe = {
            layout: 'form',
            border: false,
            items: [btnSalvarNfe]
        };
			
		var colCancelarNfe = {
            layout: 'form',
            border: false,
			bodyStyle: 'padding-right:5px',
            items: [btnCancelarNfe]
        };
			
		var colAlterarNfe = {
            layout: 'form',
            border: false,
			bodyStyle: 'padding-top:17px; padding-left: 10px;',
            items: [btnAlterarNfe]
        };

		var FieldNfe = new Ext.form.TextField({
			vtype: 'ctevtype',
            id: 'Filtro-NFe',
            fieldLabel: 'Chave NFe',
            name: 'Filtro-NFe',
            allowBlank: false,
            autoCreate: { tag: 'input', type: 'text', maxlength: '44', autocomplete: 'off' },
            maxLength: 50,
            width: 320,
            hiddenName: 'Filtro-NFe',
            listeners:
            {
				specialKey: function (field, e) {
					if (e.getKey() == e.ENTER) 
					{
						fctBuscarNfe();
					}
				}
			}
        });

		var colNfe = {
            width: 330,
            layout: 'form',
            border: false,
            items: [FieldNfe]
        };

		var arrlinhaBuscaNfe = {
			layout: 'column',
			border: false,
			items: [colNfe, colBuscarNfe,colGerarPdf, colAlterarNfe]
		};

		var fieldNumero = new Ext.form.TextField({
            id: 'field-Numero',
            fieldLabel: 'Número',
            name: 'field-Numero',
			disabled: true,
            width: 70,
            hiddenName: 'field-Numero'
        });

		var colNumero = {
            width: 80,
            layout: 'form',
            border: false,
            items: [fieldNumero]
        };

		var fieldSerie = new Ext.form.TextField({
            id: 'field-Serie',
            fieldLabel: 'Série',
            name: 'field-Serie',
			disabled: true,
            width: 50,
            hiddenName: 'field-Serie'
        });

		var colSerie = {
            width: 60,
            layout: 'form',
            border: false,
            items: [fieldSerie]
        };
			
	    var fieldDtEmissao = new Ext.form.TextField({
            id: 'field-DtEmissao',
            fieldLabel: 'Data Emissão',
            name: 'field-DtEmissao',
			disabled: true,
            width: 100,
            hiddenName: 'field-DtEmissao'
        });

		var colDtEmissao = {
            width: 110,
            layout: 'form',
            border: false,
            items: [fieldDtEmissao]
        };

		var fieldValorTotalNfe = new Ext.form.TextField({
            id: 'field-ValorTotalNfe',
            fieldLabel: 'Valor Total da Nota Fiscal',
            name: 'field-ValorTotalNfe',
			disabled: true,
            width: 150,
            hiddenName: 'field-ValorTotalNfe'
        });
		    
		var colValorTotalNfe = {
            width: 160,
            layout: 'form',
            border: false,
            items: [fieldValorTotalNfe]
        };
		    
		var txtOrigemNfe = new Ext.form.TextField({
            id: 'txtOrigemNfe',
            fieldLabel: 'Origem',
            name: 'txtOrigemNfe',
			disabled: true,
            width: 100,
            hiddenName: 'txtOrigemNfe'
        });

		var colOrigemNfe = {
            width: 110,
            layout: 'form',
            border: false,
            items: [txtOrigemNfe]
        };
			 
		var arrlinhaDadosNfe = {
            layout: 'column',
            border: false,
            items: [colNumero,colSerie, colDtEmissao,colValorTotalNfe, colOrigemNfe]
        };

		var FieldSetDadosNfe = {
			xtype: 'fieldset',
			autoHeight: true,
			bodyStyle: 'padding: 10px',
			width: 600,
			title: 'Dados da NF-e',
			items: [arrlinhaDadosNfe]
		};

		var arrlinhaDadosNfe = {
            layout: 'column',
            border: false,
            items: [FieldSetDadosNfe]
        };

		arrFieldsNfe.push(arrlinhaDadosNfe);

		/*************************************/
		/*  EMITENTE  
		/*************************************/

		var fieldCNPJEmit = new Ext.form.TextField({
            id: 'field-CNPJEmit',
            fieldLabel: 'CNPJ',
            name: 'field-CNPJEmit',
			disabled: true,
            width: 120,
            hiddenName: 'field-CNPJEmit'
        });

		var colCNPJEmit = {
            width: 130,
            layout: 'form',
            border: false,
            items: [fieldCNPJEmit]
        };

		var fieldRazaoEmit = new Ext.form.TextField({
            id: 'field-RazaoEmit',
            fieldLabel: 'Nome/Razão Social',
            name: 'field-RazaoEmit',
			disabled: true,
            width: 250,
            hiddenName: 'field-RazaoEmit'
        });

		var colRazaoEmit = {
            width: 260,
            layout: 'form',
            border: false,
            items: [fieldRazaoEmit]
        };

		var fieldInscEmit = new Ext.form.TextField({
            id: 'field-InscEmit',
            fieldLabel: 'Inscrição Estadual',
            name: 'field-InscEmit',
			disabled: true,
            width: 100,
            hiddenName: 'field-InscEmit'
        });

		var colInscEmit = {
            width: 110,
            layout: 'form',
            border: false,
            items: [fieldInscEmit]
        };

		var fieldUFEmit = new Ext.form.TextField({
            id: 'field-UFEmit',
            fieldLabel: 'UF',
            name: 'field-UFEmit',
			disabled: true, 
            width: 30,
            hiddenName: 'field-UFEmit'
        });

		var colUFEmit = {
            width: 40,
            layout: 'form',
            border: false,
            items: [fieldUFEmit]
        };

		var arrlinhaDadosEmitente = {
			layout: 'column',
			border: false,
			items: [colCNPJEmit, colRazaoEmit,colInscEmit, colUFEmit]
		};

		var FieldSetEmitente = {
			xtype: 'fieldset',
			autoHeight: true,
			bodyStyle: 'padding: 10px',
			width: 600,
			title: 'EMITENTE',
			items: [arrlinhaDadosEmitente]
		};

		var arrlinhaDadosEmitente = {
            layout: 'column',
            border: false,
            items: [FieldSetEmitente]
        };

		/*************************************/
		/*  REMETENTE  
		/*************************************/

		var fieldCNPJRem = new Ext.form.TextField({
            id: 'field-CNPJRem',
            fieldLabel: 'CNPJ',
            name: 'field-CNPJRem',
			disabled: true,
            width: 120,
            hiddenName: 'field-CNPJRem'
        });

			var colCNPJRem = {
            width: 130,
            layout: 'form',
            border: false,
            items: [fieldCNPJRem]
        };

		var fieldRazaoRem = new Ext.form.TextField({
            id: 'field-RazaoRem',
            fieldLabel: 'Nome/Razão Social',
            name: 'field-RazaoRem',
			disabled: true,
            width: 250,
            hiddenName: 'field-RazaoRem'
        });

		var colRazaoRem = {
            width: 260,
            layout: 'form',
            border: false,
            items: [fieldRazaoRem]
        };

		var fieldInscRem = new Ext.form.TextField({
            id: 'field-InscRem',
            fieldLabel: 'Inscrição Estadual',
            name: 'field-InscRem',
			disabled: true,
            width: 100,
            hiddenName: 'field-InscRem'
        });

		var colInscRem = {
            width: 110,
            layout: 'form',
            border: false,
            items: [fieldInscRem]
        };

		var fieldUFRem = new Ext.form.TextField({
            id: 'field-UFRem',
            fieldLabel: 'UF',
            name: 'field-UFRem',
			disabled: true, 
            width: 30,
            hiddenName: 'field-UFRem'
        });

		var colUFRem = {
            width: 40,
            layout: 'form',
            border: false,
            items: [fieldUFRem]
        };

		var arrlinhaDadosRemetente = {
			layout: 'column',
			border: false,
			items: [colCNPJRem, colRazaoRem,colInscRem, colUFRem]
		};

		var FieldSetRemetente = {
			xtype: 'fieldset',
			autoHeight: true,
			bodyStyle: 'padding: 10px',
			width: 600,
			title: 'DESTINATÁRIO',
			items: [arrlinhaDadosRemetente]
		};

		var arrlinhaDadosRemetente = {
            layout: 'column',
            border: false,
            items: [FieldSetRemetente]
        };
            
		/*********************************/
		/* Transporte
		/*********************************/

		var fieldCNPJTrans = new Ext.form.TextField({
            id: 'field-CNPJTrans',
            fieldLabel: 'CNPJ',
            name: 'field-CNPJTrans',
			disabled: true,
            width: 120,
            hiddenName: 'field-CNPJTrans'
        });

			var colCNPJTrans = {
            width: 130,
            layout: 'form',
            border: false,
            items: [fieldCNPJTrans]
        };

		var fieldRazaoTrans = new Ext.form.TextField({
            id: 'field-RazaoTrans',
            fieldLabel: 'Nome/Razão Social',
            name: 'field-RazaoTrans',
			disabled: true,
            width: 250,
            hiddenName: 'field-RazaoTrans'
        });

		var colRazaoTrans = {
            width: 260,
            layout: 'form',
            border: false,
            items: [fieldRazaoTrans]
        };

		var fieldInscTrans = new Ext.form.TextField({
            id: 'field-MunicipioTrans',
            fieldLabel: 'Município',
            name: 'field-MunicipioTrans',
			disabled: true,
            width: 100,
            hiddenName: 'field-MunicipioTrans'
        });

		var colInscTrans = {
            width: 110,
            layout: 'form',
            border: false,
            items: [fieldInscTrans]
        };

		var fieldUFTrans = new Ext.form.TextField({
            id: 'field-UFTrans',
            fieldLabel: 'UF',
            name: 'field-UFTrans',
			disabled: true, 
            width: 30,
            hiddenName: 'field-UFTrans'
        });

		var colUFTrans = {
            width: 40,
            layout: 'form',
            border: false,
            items: [fieldUFTrans]
        };			

		var fieldPesoLiq = new Ext.form.TextField({
            id: 'field-PesoLiq',
            fieldLabel: 'Peso Liquido',
            name: 'field-PesoLiq',
			disabled: true, 
            width: 80,
            hiddenName: 'field-PesoLiq'
        });

		var colPesoLiq = {
            width: 90,
            layout: 'form',
            border: false,
            items: [fieldPesoLiq]
        };

		var fieldPesoBruto = {
			xtype: 'masktextfield',
            id: 'field-PesoBruto',
            fieldLabel: 'Peso Bruto',
            name: 'field-PesoBruto',
			disabled: true, 
            width: 80,
			allowBlank: false,
			mask: '990,000',
            money: true,
            hiddenName: 'field-PesoBruto'
        };

		var colPesoBruto = {
            width: 90,
            layout: 'form',
            border: false,
            items: [fieldPesoBruto]
        };

		var fieldVolume = {
			xtype: 'masktextfield',
            id: 'field-Volume',
            fieldLabel: 'Volume ',
            name: 'field-Volume',
			disabled: true, 
            width: 80,
			allowBlank: false,
			mask: '990,000',
            money: true,
            hiddenName: 'field-Volume'
        };

		var colVolume = {
            width: 90,
            layout: 'form',
			border: false,
            items: [fieldVolume]
        };			

		var fieldPesoDisponivel = {
			xtype: 'masktextfield',
            id: 'field-PesoDisponivel',
            fieldLabel: 'Peso Disponível',
            name: 'field-PesoDisponivel',
			disabled: true, 
            width: 80,
			allowBlank: false,
			mask: '990,000',
            money: true,
            hiddenName: 'field-PesoDisponivel'
        };
		    
            
            /*
            new Ext.form.TextField({
            id: 'field-PesoDisponivel',
            fieldLabel: 'Peso Disponível ',
            name: 'field-PesoDisponivel',
			disabled: true, 
            width: 80,
            money: true,
            hiddenName: 'field-PesoDisponivel'
        });
			*/

        var fieldVolumeDisponivel = {
			xtype: 'masktextfield',
            id: 'field-VolumeDisponivel',
            fieldLabel: 'Vol. Disponível ',
            name: 'field-VolumeDisponivel',
			disabled: true, 
            width: 80,
			allowBlank: false,
			mask: '990,000',
            money: true,
            hiddenName: 'field-VolumeDisponivel'
        };

        /*
		var fieldVolumeDisponivel = new Ext.form.TextField({
            id: 'field-VolumeDisponivel',
            fieldLabel: 'Vol. Disponível ',
            name: 'field-VolumeDisponivel',
			disabled: true, 
            width: 80,
            allowBlank: false,
			mask: '990,000',
            money: true,
            hiddenName: 'field-VolumeDisponivel'
        });*/

        var fieldPesoLiberar = {
			xtype: 'masktextfield',
            id: 'field-PesoLiberar',
            fieldLabel: 'Peso a Liberar',
            name: 'field-PesoLiberar',
			disabled: true, 
            width: 80,
			allowBlank: false,
			mask: '990,000',
            money: true,
            hiddenName: 'field-PesoLiberar',
            listeners: {
                change: function () {
                    bMostrarTelaAutorizacao = true;
                }
            }
        };

        var fieldVolumeLiberar = {
			xtype: 'masktextfield',
            id: 'field-VolumeLiberar',
            fieldLabel: 'Vol. a Liberar ',
            name: 'field-VolumeLiberar',
			disabled: true, 
            width: 80,
			allowBlank: false,
			mask: '990,000',
            money: true,
            hiddenName: 'field-VolumeLiberar',
            listeners: {
                change: function () {
                    bMostrarTelaAutorizacao = true;
                }
            }
        };
			
		var storeDespachosNfe = new Ext.data.JsonStore({
			root: "Items",
			fields: [
				'CodigoVagao',
				'ChaveCte',
				'Conteiner',
				'DataDespacho',
				'Destino',
				'FluxoComercial',
				'NumeroDespacho',
				'Origem',
				'SerieDespacho',
				'Status',
				'PesoUtilizado'
			]}
        );
		
		var gridDespachos = new Ext.grid.GridPanel({
			viewConfig: {
				forceFit: false
			},
            title: '',
			ds: storeDespachosNfe,
            height: 120,
			width: 580,
            region: 'center',
			stripeRows: true,				
			loadMask: { msg: App.Resources.Web.Carregando },
            colModel: new Ext.grid.ColumnModel({
				defaults: {
					sortable: false
				},
				columns: [
                    new Ext.grid.RowNumberer(),
					{ header: 'Vagão', dataIndex: "CodigoVagao", width: 50, sortable: false },
					{ header: 'Fluxo', dataIndex: "FluxoComercial", width: 50, sortable: false },
					{ header: 'Serie', dataIndex: "SerieDespacho", width: 40, sortable: false },
					{ header: 'Despacho', dataIndex: "NumeroDespacho", width: 60, sortable: false },
					{ header: 'Data Despacho', dataIndex: "DataDespacho", width: 100, sortable: false },
					{ header: 'CTE', dataIndex: "ChaveCte", width: 300, sortable: false },					    					    
				    { header: 'Status', dataIndex: "Status", width: 50, sortable: false },	    
				    { header: 'Peso Util.', dataIndex: "PesoUtilizado", width: 80, sortable: false },
				    { header: 'Origem', dataIndex: "Origem", width: 50, sortable: false },
					{ header: 'Destino', dataIndex: "Destino", width: 50, sortable: false },		    					    
					{ header: 'Conteiner', dataIndex: "Conteiner", width: 70, sortable: false }
				]
			})               
		});
					
		var colPesoDisponivel = {
            width: 90,
            layout: 'form',
			border: false,
            items: [fieldPesoDisponivel]
        };
			
		var colVolumeDisponivel = {
            width: 90,
            layout: 'form',
			border: false,
            items: [fieldVolumeDisponivel]
        };

        var colPesoLiberar = {
            width: 90,
            layout: 'form',
			border: false,
            items: [fieldPesoLiberar]
        };
			
		var colVolumeLiberar = {
            width: 90,
            layout: 'form',
			border: false,
            items: [fieldVolumeLiberar]
        };

		var arrlinha1Transporte = {
			layout: 'column',
			border: false,
			items: [colCNPJTrans, colRazaoTrans,colInscTrans, colUFTrans]
		};

		var arrlinha2Transporte = {
			layout: 'column',
			border: false,
			items: [colPesoBruto, colPesoDisponivel, colVolume, colVolumeDisponivel, colPesoLiberar, colVolumeLiberar]
		};

		var arrLinhaBotoesSalvarCancelar = {
			layout: 'column',
			border: false,
			bodyStyle: 'margin: 5px',
			items: [colCancelarNfe, colSalvarNfe]
		};

		var FieldSetTransporte = {
			xtype: 'fieldset',
			autoHeight: true,
			bodyStyle: 'padding: 10px',
			width: 600,
			title: 'TRANSPORTE',
			items: [arrlinha1Transporte, arrlinha2Transporte, arrLinhaBotoesSalvarCancelar]
		};

		var arrlinhaDadosTransporte = {
            layout: 'column',
            border: false,
            items: [FieldSetTransporte]
        };				
			
		/*********************************/
		/* Panel Consulta
		/*********************************/
			
		var panelConsulta = new Ext.form.FormPanel({
            id: 'panel-Consulta-Nfe',
            width: 800,
			autoHeight: true,
            title: "Consulta de NFe",
            region: 'center',
            bodyStyle: 'padding: 15px',
            labelAlign: 'top',
            items: [arrlinhaBuscaNfe,arrFieldsNfe,arrlinhaDadosEmitente, arrlinhaDadosRemetente, arrlinhaDadosTransporte, gridDespachos],
            buttonAlign: "center"            	
        });
			

		$(function () {
			var tabs = new Ext.TabPanel({
				id: 'tabPanel',
				renderTo: Ext.getBody(),
				activeTab: 0,
				height: 550,
			    plain: true,
                deferredRender: false,
                defaults: { bodyStyle: 'padding:5px' },
				items: [filtros, panelConsulta]
			});
		});
		
		/******************************************************************/
        fctDesabilitarEdicaoNfe();
        fctEsconderAlterar();
    </script>
</asp:Content>
<asp:Content ID="MainContent" ContentPlaceHolderID="Content" runat="server">
    <div id="header-content">
        <h1>
            CTe - Consulta de NFe</h1>
        <small>Você está em CTe > Consulta de NFe</small>
        <br />
    </div>
    <div id="divFiltros">
    </div>
</asp:Content>
