﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<script type="text/javascript">

    /**********************************************************
    STORE DE CTES - INICIO
    **********************************************************/
    var ctesAnulacaoStore = new Ext.data.JsonStore({

        fields: [
                'IdCte',
                'Chave',
				'NroCte',
                'SerieCte',
                'DataAnulacao'
                ]
    });

    //    var ctesAnulacaoStore = Ext.create('Ext.data.Store', {
    //        fields: 'name',
    //        data: [{
    //            name: 'NroCte'
    //        },
    //        {
    //            name: 'SerieCte'
    //        },
    //        {
    //            name: 'DataAnulacao'
    //        }]
    //    });


    function formataData(val, meta, rec, rowIndex, colIndex, store) {

        ////	    if (val.toString() == '') 
        ////        {
        ////	        meta.tdAttr = 'data-qtip=" ' + val + ' is not right person"';
        ////	        //meta.style = "background-color:red;";
        ////	        rec.set('isValid', false);
        ////	        return val;
        ////	    }
        ////        else
        if (val.toString().indexOf('/') != -1) {
            //rec.set('isValid', true);
            return val;
        }
        else {
            //rec.set('isValid', true);
            return Ext.util.Format.date(val, 'd/m/Y');
        }
    }

    ////	var validRenderer = function (val, meta, rec, rowIndex, colIndex, store) {
    ////	    if (val == 'John') {
    ////	        meta.tdAttr = 'data-qtip=" ' + val + ' is not right person"';
    ////	        meta.style = "background-color:red;";
    ////	        rec.set('isValid', false);
    ////	    } else {
    ////	        meta.tdAttr = 'data-qtip=" ' + val + ' is right person"';
    ////	        meta.style = "background-color:green; color: white";
    ////	        rec.set('isValid', true);
    ////	    }
    ////	    return val;
    ////	}
    ////	
    ////    

    var arrColunasCtes = new Array();

    arrColunasCtes.push({ header: 'Id:', width: 99, hidden: true, dataIndex: "IdCte", align: 'center', sortable: false, editor: new Ext.form.TextField({ autoCreate: { tag: 'input', type: 'text', maxlength: '44', autocomplete: 'off' }, readOnly: true }) });
    arrColunasCtes.push({ header: 'Chave:', width: 99, hidden: true, dataIndex: "Chave", align: 'center', sortable: false, editor: new Ext.form.TextField({ autoCreate: { tag: 'input', type: 'text', maxlength: '44', autocomplete: 'off' }, readOnly: true }) });
    arrColunasCtes.push({ header: 'Ct-e com erro:', width: 99, dataIndex: "NroCte", align: 'center', sortable: false, editor: new Ext.form.TextField({ autoCreate: { tag: 'input', type: 'text', maxlength: '44', autocomplete: 'off' }, readOnly: true }) });
    arrColunasCtes.push({ header: 'Série:', width: 82, dataIndex: "SerieCte", align: 'center', sortable: false, editor: new Ext.form.TextField({ autoCreate: { tag: 'input', type: 'text', maxlength: '44', autocomplete: 'off' }, readOnly: true }) });
    arrColunasCtes.push({ header: 'Data da declaração da anulação:', width: 253, dataIndex: "DataAnulacao", align: 'center', renderer: formataData, sortable: false, editor: new Ext.form.DateField({ autoCreate: { tag: 'input', type: 'text', maxlength: '44', autocomplete: 'off' }, allowblank: false }) });


    //		xtype: 'datefield',
    //		fieldLabel: 'Data Final',
    //		id: 'filtro-data-final',
    //		name: 'dataFinal',
    //		width: 83,
    //		allowBlank: false,
    //		vtype: 'daterange',
    //		startDateField: 'filtro-data-inicial',
    //		hiddenName: 'dataFinal'  ,
    //		value: dataAtual


    //{name: 'startDate', mapping: 'startDate', type: 'date', dateFormat: 'Y-m-d G:i:s' // put this in your store}

    //{header: "Date", align:'center', width:120, sortable: true, dataIndex: 'startDate', renderer: Ext.util.Format.dateRenderer('d-m-Y') // put this in your column model}

    var gridCtesAnulacao = new Ext.grid.EditorGridPanel({
        id: 'gridCtesAnulacao',
        name: 'gridCtesAnulacao',
        store: ctesAnulacaoStore,
        clicksToEdit: 1,
        columns: arrColunasCtes,
        colModel: new Ext.grid.ColumnModel({
            defaults: {
                sortable: false
            },
            columns: this.columns
        }),
        stripeRows: false,
        height: 260,
        width: 438
    });

    var formCteAnulacao = new Ext.form.FormPanel
	({
	    id: 'formCteAnulacao',
	    labelWidth: 80,
	    width: 470,
	    height: 348,
	    autoScroll: true,
	    bodyStyle: 'padding: 15px',
	    labelAlign: 'top',
	    items:
		[
            gridCtesAnulacao
		],
	    buttonAlign: "center",
	    buttons: [
            {
                text: 'Confirmar',
                type: 'submit',
                handler: function (b, e) {
                    if (ValidarGravacao()) {
                        EnviarCteAnulacaoSefaz();
                    }
                }
            }, {
                text: 'Cancelar',
                type: 'submit',
                handler: function (b, e) {
                    windowCteAnulacao.hide();
                }
            }
        ]
	});

    function ValidarGravacao() {

        var mensagemErro = '';
        gridCtesAnulacao.getStore().each(
            function (recordTmp) {
                if ((mensagemErro == '') && (recordTmp.data.DataAnulacao == '')) {
                    mensagemErro += " Favor informar a data de anulação de todos os ctes. ";
                }
            }
        );

        if (mensagemErro != ''){
            Ext.Msg.alert("Mensagem de Erro", mensagemErro);
            return false;
        }
        return true;
    };

    function EnviarCteAnulacaoSefaz() {

			var listaCtesAnulacao = Array();
			
            gridCtesAnulacao.getStore().each(
                function (recordTmp) {
                    var cteAnulacao =
                    {
                        CteId: recordTmp.data.IdCte,
                        Chave: recordTmp.data.Chave,
                        Numero: recordTmp.data.NroCte,
                        SerieCte: recordTmp.data.SerieCte,
                        DataAnulacao: recordTmp.data.DataAnulacao.format('d/m/Y')  
                    };

                    listaCtesAnulacao.push(cteAnulacao);
                }
            );     

            var dadosCtesAnulacaoJson = $.toJSON(listaCtesAnulacao);

        $.ajax({
            url: "<%= Url.Action("SalvarAnulacaoCtesEnvioSefazNC") %>",
            type: "POST",
            async: false,
			dataType: 'json',
			data: dadosCtesAnulacaoJson,
			contentType: "application/json; charset=utf-8",
			success: function(result) {

            	windowCteAnulacao.hide();
            	grid.getStore().load();

                if(result.Erro == false) {
					Ext.Msg.show({
						title: "Mensagem de Informação",
						msg: "Anulação do(s) CTe(s) enviada com sucesso!",
						buttons: Ext.Msg.OK,
						minWidth: 200
					});
						} 
				else {
					Ext.Msg.show({
							title: "Mensagem de Erro",
							msg: "Erro na Anulação de Um ou Mais CTes!</BR>" + result.Mensagem,
							buttons: Ext.Msg.OK,
							icon: Ext.MessageBox.ERROR,
							minWidth: 200
						});
					//grid.getStore().load();
				}
			},
			failure: function(result) {					
				Ext.Msg.show({
						title: "Mensagem de Erro",
						msg: "Erro na Anulação do(s) CTe(s)!</BR>" + result.Mensagem,
						buttons: Ext.Msg.OK,
						icon: Ext.MessageBox.ERROR,
						minWidth: 200
					});
			}
		});

    };

</script>
