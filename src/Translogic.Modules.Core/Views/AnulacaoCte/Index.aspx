﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Interna.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Import Namespace="System.Globalization" %>
<%@ Import Namespace="Translogic.Modules.Core.Domain.Model.Diversos.Cte" %>
<asp:Content runat="server" ID="HeadContent" ContentPlaceHolderID="HeadContent">
    <%Html.RenderPartial("FormNotaFiscal"); %>
    <%Html.RenderPartial("FormPesquisaEmpresa"); %>
    <%Html.RenderPartial("FormCteAnulacao"); %>
    <%
        double margemLibAnulacao = (double)ViewData["MARGEM_LIB_ANULACAO"];
        string cteVersao = (string)ViewData["CTE_VERSAO"];
        string cteEventoDesacordo = (string)ViewData["CTE_EVENTO_DESACORDO"]; 
               
    %>
    <script type="text/javascript">

	var margemLibAnulacao = "<%=margemLibAnulacao.ToString(CultureInfo.InvariantCulture)%>";
	var cteVersao = "<%=cteVersao%>";
	var cteEventoDesacordo = "<%=cteEventoDesacordo%>"; 

    /************************* VARIÁVEIS **************************/
    var dataAtual = new Date();
    var executarPesquisa = true;
	var lastRowSelected = -1;
	var lastDblClickRowSelected = -1;
	var notaManual = false;
	var totalCtes = false;
		          
	/************************* STORE **************************/    
	var storeChaves = new Ext.data.JsonStore({
		remoteSort: true,
		root: "Items",
		fields: [
					'Chave',
					'Erro',
					'Mensagem'
				]
	});
    
    /************************* CONTROLES **************************/    
    var dataInicial = {
		xtype: 'datefield',
		fieldLabel: 'Data Inicial',
		id: 'filtro-data-inicial',
		name: 'dataInicial',
		width: 83,
		allowBlank: false,
		vtype: 'daterange',
		endDateField: 'filtro-data-final',
		hiddenName: 'dataInicial',
		value: dataAtual		
	};
	
	var dataFinal = {
		xtype: 'datefield',
		fieldLabel: 'Data Final',
		id: 'filtro-data-final',
		name: 'dataFinal',
		width: 83,
		allowBlank: false,
		vtype: 'daterange',
		startDateField: 'filtro-data-inicial',
		hiddenName: 'dataFinal',
		value: dataAtual
	};
        
	var origem =	{
		xtype: 'textfield',
		vtype: 'cteestacaovtype',
		style: 'text-transform: uppercase',
		id: 'filtro-Ori',
		fieldLabel: 'Origem',
		autoCreate: { tag: 'input', type: 'text', maxlength: '3', autocomplete: 'off' },
		name: 'Origem',
		allowBlank: true,
		maxLength: 3,
		width: 35,
		hiddenName: 'Origem'
	};

	var destino = {
		xtype: 'textfield',
		vtype: 'cteestacaovtype',
		style: 'text-transform: uppercase',
		id: 'filtro-Dest',
		fieldLabel: 'Destino',
		autoCreate: { tag: 'input', type: 'text', maxlength: '3', autocomplete: 'off' },
		name: 'Destino',
		allowBlank: true,
		maxLength: 3,
		width: 35,
		hiddenName: 'Destino'
	};

	var serie = {
		xtype: 'textfield',
		style: 'text-transform: uppercase',
		vtype: 'ctesdvtype',
		id: 'filtro-serie',
		fieldLabel: 'Série',
		name: 'serie',
		allowBlank: true,
		autoCreate: { tag: 'input', type: 'text', maxlength: '3', autocomplete: 'off' },
		maxLength: 20,
		width: 30,
		hiddenName: 'serie'
	};

	var despacho = {
		xtype: 'textfield',
		style: 'text-transform: uppercase',
		vtype: 'ctedespachovtype',
		id: 'filtro-despacho',
		fieldLabel: 'Despacho',
		name: 'despacho',
		allowBlank: true,
		maxLength: 20,
		autoCreate: { tag: 'input', type: 'text', maxlength: '6', autocomplete: 'off' },
		width: 55,
		hiddenName: 'despacho'
	};

	var fluxo = {
		xtype: 'textfield',
		style: 'text-transform: uppercase',
		vtype: 'ctefluxovtype',
		id: 'filtro-fluxo',
		fieldLabel: 'Fluxo',
		name: 'fluxo',
		allowBlank: true,
		autoCreate: { tag: 'input', type: 'text', maxlength: '5', autocomplete: 'off' },
		maxLength: 20,
		width: 45,
		hiddenName: 'fluxo',
		minValue: 0,
		maxValue: 99999
	};

	var Vagao = {
		xtype: 'textfield',
		style: 'text-transform: uppercase',
		vtype: 'ctevagaovtype',
		id: 'filtro-numVagao',
		fieldLabel: 'Vagão',
		name: 'numVagao',
		allowBlank: true,
		autoCreate: { tag: 'input', type: 'text', maxlength: '7', autocomplete: 'off' },
		maxLength: 20,
		width: 55,
		hiddenName: 'numVagao'
	};

	var chave = {
		xtype: 'textfield',
		vtype: 'ctevtype',
		id: 'filtro-Chave',
		fieldLabel: 'Chave CTe',
		name: 'Chave',
		allowBlank: true,
		autoCreate: { tag: 'input', type: 'text', maxlength: '44', autocomplete: 'off' },
		maxLength: 50,
		width: 275,
		hiddenName: 'Chave'
	};

	var btnLoteChaveNfe = {
		text: 'Informar Chaves',
		xtype: 'button',
		iconCls: 'icon-find',
		handler: function (b, e) {
					
			windowStatusNFe = new Ext.Window({
							id: 'windowStatusNFe',
							title: 'Informar chaves',
							modal: true,
							width: 855,
							resizable: false,
							height: 380,
							autoLoad: {
								url: '<%= Url.Action("FormInformarChaves") %>',
								scripts: true
							}
						});

						windowStatusNFe.show();			
								
		}
	};

	/************************* GRID **************************/
	var smRaiz = new Ext.grid.CheckboxSelectionModel({
        listeners: {
			selectionchange: function (sm) {
			    var recLen = Ext.getCmp('gridCteRaiz').store.getRange().length; 
                var selectedLen = this.selections.items.length; 
			    
                if(selectedLen == recLen){ 
                    var view   = Ext.getCmp('gridCteRaiz').getView(); 
                    var chkdiv = Ext.fly(view.innerHd).child(".x-grid3-hd-checker") 
                    chkdiv.addClass("x-grid3-hd-checker-on"); 
                } 
		    }					
        },
        rowdeselect : function ( sm ,rowIndex ,record) { 
            var view   = Ext.getCmp('gridCteRaiz').getView();
            var chkdiv = Ext.fly(view.innerHd).child(".x-grid3-hd-checker");
            chkdiv.removeClass('x-grid3-hd-checker-on'); 
        },
		renderer: function (v, p, record) {						    
			return '<div class="x-grid3-row-checker">&#160;</div>';
		}
    });
	
    var grid = new Translogic.PaginatedGrid({
		autoLoadGrid: false,
		id: "gridCteRaiz", 
		height: 400,
		url: '<%= Url.Action("ObterCtes") %>',
        stripeRows: false,
		region: 'center',
		viewConfig: {
			forceFit: true,
		    getRowClass: MudaCor
		},
		fields: [
			'CteId',
			'Fluxo',
			'Origem',
			'Destino',
			'Mercadoria',                
			'Chave',
			'Cte',
            'SerieCte',
			'DateEmissao',             
			'Impresso',
			'CodigoErro',
			'FerroviaOrigem',
			'FerroviaDestino',
			'UfOrigem',
		    'ValorCte',
			'CodigoVagao',
			'UfDestino',
			'Serie',
			'Despacho',          
			'SerieDesp5', 
			'Desp5',
			'ArquivoPdfGerado',
			'AcaoSerTomada',
            'PapelTomador',
            'Acima168hs',
		    'Descarregado',
		    'PermiteCancRejeitado',
            'RateioCte',
            'IndIeToma'
		],
		columns: [ 
			new Ext.grid.RowNumberer(),
            smRaiz,   
			{ dataIndex: "CteId", hidden: true },
			{ header: 'Num Vagão', dataIndex: "CodigoVagao", width: 100, sortable: false },
			{ header: 'Série', dataIndex: "Serie", width: 70, sortable: false },
			{ header: 'Despacho', dataIndex: "Despacho", width: 70, sortable: false },
			{ header: 'CTe', dataIndex: "Cte",  width: 100, sortable: false },
        	{ dataIndex: "CteSerie", hidden: true },
        	{ header: 'Origem', dataIndex: "Origem", width: 50, sortable: false },
			{ header: 'Destino', dataIndex: "Destino", width: 50, sortable: false },
			{ header: 'Fluxo', dataIndex: "Fluxo",  width: 70, sortable: false },
			{ header: 'Mercadoria', dataIndex: "Mercadoria", width: 70, sortable: false },
			{ header: 'Chave CTe', dataIndex: "Chave", width: 280, sortable: false },
			{ header: 'Data Emissão', dataIndex: "DateEmissao", width: 75, sortable: false },
		    { header: 'Valor', dataIndex: "ValorCte", sortable: false, renderer: Ext.util.Format.numberRenderer('0.000,00/i') },
			{ header: 'Ferrovia Ori.', dataIndex: "FerroviaOrigem", sortable: false },
			{ header: 'Ferrovia Dest.', dataIndex: "FerroviaDestino", sortable: false },
			{ header: 'UF Ori.', dataIndex: "UfOrigem", width: 70, sortable: false },
			{ header: 'UF Dest.', dataIndex: "UfDestino", width: 70, sortable: false },
			{ header: 'SerieDesp5', dataIndex: "SerieDesp5", width: 70, sortable: false },
			{ header: 'Desp5', dataIndex: "Desp5", width: 70, sortable: false },
            { header: 'Papel Tomador', dataIndex: "PapelTomador", width: 170, sortable: false },
		    { header: 'Acima168hs', hidden: true },
		    { header: 'Descarregado', hidden: true },
		    { header: 'PermiteCancRejeitado', hidden: true },
            { dataIndex: "RateioCte", hidden: true },
            { dataIndex: "IndIeToma", hidden: true }
		],
		sm: smRaiz,
		listeners: {
			rowclick: function(grid, rowIndex, e) {
				if (smRaiz.isSelected(rowIndex)) {
					if (rowIndex == lastRowSelected) {
						grid.getSelectionModel().deselectRow(rowIndex);
						lastRowSelected = -1;
					}
					else {
						lastRowSelected = rowIndex;
						lastDblClickRowSelected = rowIndex;
					}
				}
			},
			rowdblclick:  function(grid, rowIndex, record, e) {
					grid.getSelectionModel().selectRow(lastDblClickRowSelected);
					grid.getSelectionModel().selectRow(item);
			}
		}
	});
    
    var ds = Ext.getCmp("gridCteRaiz").getStore();
	   
	grid.getStore().proxy.on('beforeload', function(p, params) {	   
	   
		params['filter[0].Campo'] = 'dataInicial';
		params['filter[0].Valor'] = new Array(Ext.getCmp("filtro-data-inicial").getValue().format('d/m/Y') + " " + Ext.getCmp("filtro-data-inicial").getValue().format('H:i:s')); 
		params['filter[0].FormaPesquisa'] = 'Start';

		params['filter[1].Campo'] = 'dataFinal';
		params['filter[1].Valor'] = new Array(Ext.getCmp("filtro-data-final").getValue().format('d/m/Y') + " " + Ext.getCmp("filtro-data-inicial").getValue().format('H:i:s'));  
		params['filter[1].FormaPesquisa'] = 'Start';

		params['filter[2].Campo'] = 'serie';
		params['filter[2].Valor'] = Ext.getCmp("filtro-serie").getValue();
		params['filter[2].FormaPesquisa'] = 'Start';

		params['filter[3].Campo'] = 'despacho';
		params['filter[3].Valor'] = Ext.getCmp("filtro-despacho").getValue();
		params['filter[3].FormaPesquisa'] = 'Start';

		params['filter[4].Campo'] = 'fluxo';
		params['filter[4].Valor'] = Ext.getCmp("filtro-fluxo").getValue();
		params['filter[4].FormaPesquisa'] = 'Start';

		var listaChave = "";
		if(storeChaves.getCount() != 0)
		{
			storeChaves.each(
				function (record) {
					if (!record.data.Erro) {
						listaChave += record.data.Chave + ";";	
					}
				}
			);
		}else{
			listaChave = Ext.getCmp("filtro-Chave").getValue();	
		}

        var countChaves = storeChaves.getCount();
		
        if(countChaves > 0)
		{
			grid.pagingToolbar.pageSize = countChaves;
            params['pagination.Limit'] = countChaves;
		}else{
			grid.pagingToolbar.pageSize = 200;
            params['pagination.Limit'] = 200;
		}
	    
		params['filter[5].Campo'] = 'chave';
		params['filter[5].Valor'] = listaChave;
		params['filter[5].FormaPesquisa'] = 'Start';
	    
		params['filter[6].Campo'] = 'Origem';
		params['filter[6].Valor'] = Ext.getCmp("filtro-Ori").getValue();
		params['filter[6].FormaPesquisa'] = 'Start';
			
		params['filter[7].Campo'] = 'Destino';
		params['filter[7].Valor'] = Ext.getCmp("filtro-Dest").getValue();
		params['filter[7].FormaPesquisa'] = 'Start';
			
		params['filter[8].Campo'] = 'Vagao';
		params['filter[8].Valor'] = Ext.getCmp("filtro-numVagao").getValue();
		params['filter[8].FormaPesquisa'] = 'Start';
            
        params['filter[9].Campo'] = 'UfDcl';
	    params['filter[9].Valor'] = '';
		params['filter[9].FormaPesquisa'] = 'Start';            
	});
    
	/************************* LAYOUT **************************/
	var arrDataIni = {
		width: 87,
		layout: 'form',
		border: false,
		items: [dataInicial]
	};

	var arrDataFim = {
		width: 87,
		layout: 'form',
		border: false,
		items: [dataFinal]
	};

	var arrOrigem = {
        width: 43,
        layout: 'form',
        border: false,
        items: [origem]
    };
	
	var arrDestino = {
		width: 43,
		layout: 'form',
		border: false,
		items: [destino]
	};

	var arrSerie =  {
		width: 37,
		layout: 'form',
		border: false,
		items: [serie]
	};

	var arrDespacho = {
		width: 60,
		layout: 'form',
		border: false,
		items: [despacho]
	};

	var arrFluxo = {
        width: 50,
        layout: 'form',
        border: false,
        items: [fluxo]
    };
		
	var arrVagao = {
		width: 60,
		layout: 'form',
		border: false,
		items: [Vagao]
	};

	var arrChave = {
        width: 280,
        layout: 'form',
        border: false,
        items: [chave]
    };

	var arrbtnLoteChaveNfe = {
		width: 280,
		layout: 'form',
		style: 'padding: 17px 0px 0px 0px;',
		border: false,
		items:
		[btnLoteChaveNfe]
	};
	            
	var arrlinha1 = {
		layout: 'column',
		border: false,
		items: [arrDataIni,arrDataFim,arrFluxo, arrChave, arrbtnLoteChaveNfe]
	};

	var arrlinha2 = {
		layout: 'column',
		border: false,
		items: [arrSerie, arrDespacho, arrOrigem, arrDestino, arrVagao]
	};
		
	var arrCampos = new Array();
	arrCampos.push(arrlinha1);
	arrCampos.push(arrlinha2);
	




    /************************* PAINEL **************************/
	var filters = new Ext.form.FormPanel({
	    id: 'grid-filtros',
	    title: "Filtros",
	    region: 'center',
	    bodyStyle: 'padding: 15px',
	    labelAlign: 'top',
	    items: [arrCampos],
	    buttonAlign: "center",
	    buttons:
	    [
	    {
	        text: 'Pesquisar',
	        type: 'submit',
	        iconCls: 'icon-find',
	        handler: function(b, e) {
	            Pesquisar();
	        }
	    }, {
	        text: 'Autorizar Cancelamento Rejeitado',
	        type: 'submit',
	        iconCls: 'icon-save',
	        handler: function(b, e) {
	            AutorizarCR();
	        }
	    }, {
	        text: 'Informar Nota Eletronica',
	        type: 'submit',
	        iconCls: 'icon-save',
	        handler: function(b, e) {
	            notaManual = false;
	            SalvarNotaAnulacao();
	        }
	    }, {
	        text: 'Gerar Ct-e',
	        type: 'submit',
	        hidden: habilitaCampoNotaManual() === false,
	        iconCls: 'icon-page-red',
	        handler: function(b, e) {
////	            notaManual = true;
////	            SalvarNotaAnulacao();

                GerarCteAnulacaoContribuinte();
	        }
	    },
	    {
	        text: 'Gerar CT-e NC.',
	        type: 'submit',
	        iconCls: 'icon-page-red',
	        handler: function(b, e) {
	            GerarCteAnulacaoNContribuinte();
	        }
	    }, {
	        text: 'Limpar',
	        handler: function(b, e) {

	            storeChaves.removeAll();
	            grid.getStore().removeAll();
	            grid.getStore().totalLength = 0;
	            grid.getBottomToolbar().bind(grid.getStore());
	            grid.getBottomToolbar().updateInfo();
	            Ext.getCmp("grid-filtros").getForm().reset();

	            HabilitarCampos(false);
	        },
	        scope: this
	    }]


    });


    function MudaCor(row, index) {
        if (row.data.Acima168hs) {
            return 'corYellow';
        }
            else if (row.data.Descarregado) {
            return 'corOrange';
        }
        else if (!row.data.PermiteCancRejeitado) {
            return 'corYellow';
        }
                       
    } 
    
    /************************* Ação CTRL+C nos campos da Grid **************************/
    var isCtrl = false;
    document.onkeyup = function(e) {
        var keyID = event.keyCode;

        // 17 = tecla CTRL
        if (keyID == 17) {
            isCtrl = false; //libera tecla CTRL
        }
    };

    document.onkeydown = function(e) {
        var keyID = event.keyCode;

        // verifica se CTRL esta acionado
        if (keyID == 17) {
            isCtrl = true;
        }
		
        if ((keyID == 13) && (executarPesquisa) && (!formNotaFiscal.isVisible())) {
            Pesquisar();
            return;
        }

        // 67 = tecla 'c'
        if (keyID == 67 && isCtrl == true) {

            // verifica se há selecão na tabela
            if (grid.getSelectionModel().hasSelection()) {
                // captura todas as linhas selecionadas
                var recordList = grid.getSelectionModel().getSelections();

                var a = [];
                var separator = '\t'; // separador para colunas no excel
                for (var i = 0; i < recordList.length; i++) {
                    var s = '';
                    var item = recordList[i].data;
                    for (key in item) {
                        if (key != 'Id') { //elimina o campo id da linha
                            s = s + item[key] + separator;
                        }
                    }
                    s = s.substr(0, s.length - 1); //retira o ultimo separador '\t' da linha
                    a.push(s);
                }
                window.clipboardData.setData('Text', (a.join('\n'))); //insere linhas no clipBoard
            }
        }
    };
    
    /************************* FUNÇÕES **************************/
    function showResult(btn) {
		executarPesquisa = true; 
	}

	function habilitaCampoNotaManual() {
	    if (cteEventoDesacordo.toUpperCase() === 'TRUE') {
	        return true;
	    } else {
	        return false;
	    }
	}
 
    function SomarCtesSelecionados() 
    {
        var listaSelecionados = smRaiz.getSelections();
        totalCtes = 0;        
        for (var i = 0; i < listaSelecionados.length; i++) 
        {
            totalCtes += parseFloat(listaSelecionados[i].data.ValorCte);
        }
    }
    
	function HabilitarCampos(bloquear)
	{
		Ext.getCmp("filtro-data-inicial").setDisabled(bloquear);
		Ext.getCmp("filtro-data-final").setDisabled(bloquear);
		Ext.getCmp("filtro-serie").setDisabled(bloquear);
		Ext.getCmp("filtro-despacho").setDisabled(bloquear);
		Ext.getCmp("filtro-fluxo").setDisabled(bloquear);
		Ext.getCmp("filtro-Ori").setDisabled(bloquear);
		Ext.getCmp("filtro-Dest").setDisabled(bloquear);
		Ext.getCmp("filtro-Chave").setDisabled(bloquear);
		Ext.getCmp("filtro-numVagao").setDisabled(bloquear);
	}

    function Pesquisar()
	{
		
			var diferenca = Ext.getCmp("filtro-data-final").getValue() - Ext.getCmp("filtro-data-inicial").getValue(); 
			diferenca = Math.floor(diferenca / (1000 * 60 * 60 * 24)); 

			if (Ext.getCmp("filtro-data-inicial").getValue() == '' || Ext.getCmp("filtro-data-final").getValue() == '') {
				executarPesquisa = false;
				Ext.Msg.show({
				    title: "Mensagem de Informação",
				    msg: "Preencha os filtro datas!",
				    buttons: Ext.Msg.OK,
				    icon: Ext.MessageBox.INFO,
				    minWidth: 200,
				    fn: showResult
			    });
			}
			else if (diferenca > 30) {
				executarPesquisa = false;
				Ext.Msg.show({
				    title: "Mensagem de Informação",
				    msg: "O período da pesquisa não deve ultrapassar 30 dias",
				    buttons: Ext.Msg.OK,
				    icon: Ext.MessageBox.INFO,
				    minWidth: 200,
				    fn: showResult
				});
			}
			else if (Ext.getCmp("filtro-serie").getValue() != '' && Ext.getCmp("filtro-despacho").getValue() == '') {
				executarPesquisa = false;
				Ext.Msg.show({
					title: "Mensagem de Informação",
					msg: "Não é possível filtrar apenas pela a Série do Despacho.<br/>Também é necessário preencher o número do Despacho.",
					buttons: Ext.Msg.OK,
					icon: Ext.MessageBox.INFO,
					minWidth: 200,
					fn: showResult
				});
			}
			else if (Ext.getCmp("filtro-despacho").getValue() != '' && Ext.getCmp("filtro-serie").getValue() == '') {
				executarPesquisa = false;
				Ext.Msg.show({
					title: "Mensagem de Informação",
					msg: "Não é possível filtrar apenas pelo número do Despacho.<br/>Também é necessário preencher a Série do Despacho.",
					buttons: Ext.Msg.OK,
					icon: Ext.MessageBox.INFO,
					minWidth: 200,
					fn: showResult
				});
			}
			else {
				var view   = Ext.getCmp('gridCteRaiz').getView(); 
				var chkdiv = Ext.fly(view.innerHd).child(".x-grid3-hd-checker");
				chkdiv.removeClass('x-grid3-hd-checker-on'); 
				grid.getStore().load();
				executarPesquisa = true;
			}
		}
	    
		function AutorizarCR()
		{
			var listaEnvio = Array();
			var selected = smRaiz.getSelections();
		
			if(selected.length > 0 )
			{
				for (var i = 0; i < selected.length; i++) {                
						//listaEnvio.push(selected[i].data.CteId+";"+selected[i].data.Descarregado);
				        listaEnvio.push(selected[i].data);
				}
			
				if (Ext.Msg.confirm("Anulação", "Você possui " + listaEnvio.length + " para enviar para o SAP como Cancelamento Rejeitado, deseja realmente enviar?", 
					function(btn, text) {
						if (btn == 'yes') 
						{					
							var dados = $.toJSON(listaEnvio);

							$.ajax({
								url: "<%= Url.Action("AutorizarCancelamentoRejeitado") %>",
								type: "POST",
								dataType: 'json',
								data: dados,
								contentType: "application/json; charset=utf-8",
								success: function(result) {
									if(result.success) {
										Ext.Msg.show({
											title: "Mensagem de Informação",
											msg: "Sucesso na Anulação do(s) CTe(s)!",
											buttons: Ext.Msg.OK,
											minWidth: 200
										});
						
										grid.getStore().load();
									} 
									else {
										Ext.Msg.show({
												title: "Mensagem de Erro",
												msg: "Erro na Anulação do(s) CTe(s)!</BR>" + result.erros,
												buttons: Ext.Msg.OK,
												icon: Ext.MessageBox.ERROR,
												minWidth: 200
											});
									    grid.getStore().load();
									}
								},
								failure: function(result) {					
									Ext.Msg.show({
											title: "Mensagem de Erro",
											msg: "Erro na Anulação do(s) CTe(s)!</BR>" + result.Message,
											buttons: Ext.Msg.OK,
											icon: Ext.MessageBox.ERROR,
											minWidth: 200
										});
								}
							});

						}
				}));

			}else{
				Ext.Msg.show({
					title: "Mensagem de Informação",
					msg: "Favor selecionar o(s) CTe(s) para anulação!",
					buttons: Ext.Msg.OK,
					icon: Ext.MessageBox.ERROR,
					minWidth: 200
				});

			}
		}

	function GerarCteAnulacaoContribuinte() {
	    var listaSelecionadosCteAnu = smRaiz.getSelections();
	    var mensagemBody = '';
	    var mensagemHeader = 'Existem CTe-s com Indicativo do tomador diferente de 1-Contribuinte ICMS entre os CTe-s selecionados.';
	    var mensagemFooter = 'Selecione apenas os CTe-s com indicativo 1-Contribuinte ICMS.';
        var listaCtesAnulacao = Array();

	    if (smRaiz.getSelections().length > 0) {

	        for (var i = 0; i < listaSelecionadosCteAnu.length; i++) {

                var cteAnulacao =
                {
                    CteId: listaSelecionadosCteAnu[i].data.IdCte,
                    Chave: listaSelecionadosCteAnu[i].data.Chave,
                    Numero: listaSelecionadosCteAnu[i].data.NroCte,
                    SerieCte: listaSelecionadosCteAnu[i].data.SerieCte,
                    DataAnulacao: '' 
                };
                listaCtesAnulacao.push(cteAnulacao);
                
                if (listaSelecionadosCteAnu[i].data.IndIeToma !== 1) {
                        
                    var ieToma = listaSelecionadosCteAnu[i].data.IndIeToma === null ? 'não informado' : listaSelecionadosCteAnu[i].data.IndIeToma;
                    mensagemBody += 'Ct-e: ' + listaSelecionadosCteAnu[i].data.Cte + ', SerieCte: ' + listaSelecionadosCteAnu[i].data.SerieCte + ', Ind.Toma: ' + ieToma + '<br>';
                }
	        }
	    } else {
	        
			Ext.Msg.show({
				title: "Mensagem de Informação",
				msg: "Favor selecionar o(s) CTe(s) para anulação!",
				buttons: Ext.Msg.OK,
				icon: Ext.MessageBox.ERROR,
				minWidth: 200
			});
	        return;
	    }

	    if (mensagemBody.length > 0) {
	   
            Ext.Msg.show({
			title: "Mensagem de Informação",
			msg: mensagemHeader + '<br>' + mensagemBody + mensagemFooter,
			buttons: Ext.Msg.OK,
			icon: Ext.MessageBox.ERROR,
			minWidth: 200
			});

        } else {

            var dadosCtesAnulacaoJson = $.toJSON(listaCtesAnulacao);

    		$.ajax({
			url: "<%= Url.Action("SalvarAnulacaoCtesEnvioSefaz") %>",
			type: "POST",
			dataType: 'json',
            async: false,
			data: dadosCtesAnulacaoJson,
			contentType: "application/json; charset=utf-8",
			success: function(result) {

            	grid.getStore().load();

                if(result.Erro == false) {
					Ext.Msg.show({
						title: "Mensagem de Informação",
						msg: "Anulação do(s) CTe(s) enviada com sucesso!",
						buttons: Ext.Msg.OK,
						minWidth: 200
					});
						} 
				else {
					Ext.Msg.show({
							title: "Mensagem de Erro",
							msg: "Erro na Anulação de Um ou Mais CTes!</BR>" + result.Mensagem,
							buttons: Ext.Msg.OK,
							icon: Ext.MessageBox.ERROR,
							minWidth: 200
						});
				}
			},
			failure: function(result) {					
				Ext.Msg.show({
						title: "Mensagem de Erro",
						msg: "Erro na Anulação do(s) CTe(s)!</BR>" + result.Mensagem,
						buttons: Ext.Msg.OK,
						icon: Ext.MessageBox.ERROR,
						minWidth: 200
					});
			}
		    });

        }
	}
    
	function GerarCteAnulacaoNContribuinte() {
        var listaSelecionadosCteAnu = smRaiz.getSelections();
	    var mensagemBody = '';
	    var mensagemHeader = 'Existem CTe-s com Indicativo do tomador diferente de 9-Não Contribuinte entre os CTe-s selecionados.';
	    var mensagemFooter = 'Selecione apenas os CTe-s com indicativo 9-Não Contribuinte.';

     	if(smRaiz.getSelections().length > 0 )
		{
    		gridCtesAnulacao.getStore().removeAll();
                
            windowCteAnulacao = new Ext.Window({
									id: 'windowCteAnulacao',
									title: 'CT-e de anulação',
									modal: true,
									width: 484,
									closeAction:'hide',
									resizable: false,
									height: 380,
									items:[formCteAnulacao]
								});
  
  
            for (var i = 0; i < listaSelecionadosCteAnu.length; i++) 
            {
                var rec = new ctesAnulacaoStore.recordType
                ({
                    IdCte: listaSelecionadosCteAnu[i].data.CteId,
                    Chave: listaSelecionadosCteAnu[i].data.Chave,
                    NroCte: listaSelecionadosCteAnu[i].data.Cte,
                    SerieCte: listaSelecionadosCteAnu[i].data.SerieCte,
                    DataAnulacao: '',
                    IndIeToma: listaSelecionadosCteAnu[i].data.IndIeToma
                });
                        
                rec.commit();

                if (rec.data.IndIeToma !== 9) {
                        
                    var ieToma = rec.data.IndIeToma === null ? 'não informado' : rec.data.IndIeToma;
                    mensagemBody += 'Ct-e: ' + rec.data.NroCte + + ', SerieCte: ' + listaSelecionadosCteAnu[i].data.SerieCte + ', Ind.Toma: ' + ieToma + '<br>';
                }
                gridCtesAnulacao.getStore().add(rec);
            }

			if (mensagemBody.length > 0) {
                
                Ext.Msg.show({
					title: "Mensagem de Informação",
					msg: mensagemHeader + '<br>' + mensagemBody + mensagemFooter,
					buttons: Ext.Msg.OK,
					icon: Ext.MessageBox.ERROR,
					minWidth: 200
				    });
                     
			    windowCteAnulacao = null;
			} else {
			    windowCteAnulacao.show();
			}

		}else{
			Ext.Msg.show({
				title: "Mensagem de Informação",
				msg: "Favor selecionar o(s) CTe(s) para anulação!",
				buttons: Ext.Msg.OK,
				icon: Ext.MessageBox.ERROR,
				minWidth: 200
			});
		}
	}

	function SalvarNotaAnulacao()
		{
			if(smRaiz.getSelections().length > 0 )
			{
			    SomarCtesSelecionados();
				windowNotaFiscal = new Ext.Window({
										id: 'windowNotaFiscal',
										title: 'Informar dados da Nota Fiscal de Anulação',
										modal: true,
										width: 444,
										closeAction:'hide',
										resizable: false,
										height: 500,
										items:[formNotaFiscal],
										listeners: {
											'beforeshow' :function(){
													
													if(notaManual)
													{
														Ext.getCmp("edNfChaveNfe").hide();
														Ext.getCmp("edNfChaveNfe").label.hide();
														Ext.getCmp("edBtnBuscarNota").hide();
													}else{
														Ext.getCmp("edNfChaveNfe").show();
														Ext.getCmp("edNfChaveNfe").label.show();
														Ext.getCmp("edBtnBuscarNota").show();
													}
											 },
											'hide': function(){
												Ext.getCmp("formNotaFiscal").getForm().reset();
												Pesquisar();
											}
										}
									});
				windowNotaFiscal.show();
			}else{
				Ext.Msg.show({
					title: "Mensagem de Informação",
					msg: "Favor selecionar o(s) CTe(s) para anulação!",
					buttons: Ext.Msg.OK,
					icon: Ext.MessageBox.ERROR,
					minWidth: 200
				});

			}
		}

		
    function PesquisarEmpresa(sufixo)
    {
        windowPesquisaEmpresa = new Ext.Window({
			id: 'windowPesquisaEmpresa',
			title: 'Pesquisar Empresa',
			modal: true,
			width: 598,
			closeAction:'hide',
			height: 350,
			items:[formPesquisaEmpresa],
			listeners: {
				'hide': function(){
                    var newValue = Ext.getCmp("cpHiddenCnpj").getValue();
                    Ext.getCmp("edCnpj"+sufixo).setValue(newValue);
					Ext.getCmp("formPesquisaEmpresa").getForm().reset(); 
                    gridEmpresaStore.removeAll();
                    CarregarDadosCnpjEdicao(newValue, sufixo);                          
				}
			}
		});

		windowPesquisaEmpresa.show();
    }

    /************************* RENDER **************************/
	Ext.onReady(function () {
        filters.render(document.body);
        grid.render(document.body);
    });
	    
    </script>
    <style>
        .corGreen
        {
            background-color: #98FB98;
        }
        .corRed
        {
            background-color: #FFEEDD;
        }
        .corOrange
        {
            background-color: #FFC58A;
        }
        .corYellow
        {
            background-color: #FFFF80;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div id="header-content">
        <h1>
            CTe - Anulação</h1>
        <small>Você está em CTe > Anulação</small>
        <br />
    </div>
</asp:Content>
