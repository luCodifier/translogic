﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div id="div-form-nota-fiscal">
</div>
<script type="text/javascript">

    /**********************************************************
	FORM DADOS DAS NOTAS FISCAIS - INICIO
    ChaveNfe
    Conteiner
    SerieNotaFiscal
    NumeroNotaFiscal
    PesoTotal
    PesoRateio
    ValorNotaFiscal
    Remetente
    Destinatario
    CnpjRemetente
    CnpjDestinatario
    TIF
    DataNotaFiscal
    SiglaRemetente
    SiglaDestinatario
	**********************************************************/
    var arrLinhas = new Array();
	var IndObtidoAutomatico;

    var edNfChaveNfe =	{
        xtype: 'textfield',
        id: 'edNfChaveNfe',
        fieldLabel: 'Chave Nfe',
        name: 'edNfChaveNfe',
        width: 325,
		listeners:{
                change: function (field, newValue, oldValue) {					
					LimparCampos();
				},
				hide :function (field) {					
					IndObtidoAutomatico = false;
					LimparCampos();
					HabilitarEdicaoNfe(true);
				},
				show :function (field) {					
					IndObtidoAutomatico = true;
					LimparCampos();
					HabilitarEdicaoNfe(false);
				}
		}
    };

	 var botaoBuscarNota = new Ext.Button({
    	margins: { top: 50 },
    	name: 'edBtnBuscarNota',
    	hidden: false,
    	iconCls: 'icon-find',
    	id: 'edBtnBuscarNota',
    	handler: function (a, b, c) {
			ObterDadosNfe(Ext.getCmp('edNfChaveNfe').getValue());
    	}
    });

    var l1_coluna1 = { width: 335, layout: 'form', border: false, items: [edNfChaveNfe] };
	var l1_coluna2 = { width: 25, layout: 'form', style: { "margin-top": "17px" }, border: false, items: [botaoBuscarNota] };

    var linha1 = {
        layout: 'column',
        border: false,		
        items: [l1_coluna1,l1_coluna2]
    };
    arrLinhas.push(linha1);

    var edSerieNota = {
        xtype: 'textfield',
        id: 'edSerieNota',
        fieldLabel: 'Série',
        name: 'edSerieNota',
		cls: 'x-item-disabled',
        readOnly: true,
        width: 40
    };

    var edNumeroNota = {
        xtype: 'textfield',
        id: 'edNumeroNota',
        fieldLabel: 'Número',
        name: 'edNumeroNota',
		cls: 'x-item-disabled',
        readOnly: true,
        width: 60
    }; 

    var edDataNota = {
        xtype: 'datefield',
        id: 'edDataNota',
        fieldLabel: 'Data',
        name: 'edDataNota',
		cls: 'x-item-disabled',        
		disabled: true,
        width: 90
    };
  
       /* var edPesoTotal = {
            xtype: 'masktextfield',
            id: 'edPesoTotal',
            fieldLabel: 'Peso Total',
            name: 'edPesoTotal',
            width: 70,
            mask: '990,000',
		    money: true,
            maxLength: 7,
            value: "0,000"
        };

        var edPesoRateio = {
            xtype: 'masktextfield',
            id: 'edPesoRateio',
            fieldLabel: 'Peso Rateio',
            allowBlank: false,
            name: 'edPesoRateio',
            width: 70,
            mask: '990,000',
		    money: true,
            maxLength: 7,
            value: "0,000"
        };
		*/
	var edValorTotalNota = {
        xtype: 'masktextfield',
        id: 'edValorTotalNota',
        fieldLabel: 'Valor Total',
        name: 'edValorTotalNota',
        allowBlank: false,
        width: 70,
        mask: '#9.999.990,00',
		money: true,
        maxLength: 13,
		cls: 'x-item-disabled',
        readOnly: true,
        value: "0,00"
    };
	
	var txtProtocolo = {
        xtype: 'textfield',
        id: 'txtProtocolo',
        fieldLabel: 'Protocolo',
        name: 'txtProtocolo',
        allowBlank: false,
        width: 100,
        maxLength: 16
    };

        var l2_coluna1 = { width: 50, layout: 'form', border: false, items: [edSerieNota] };
        var l2_coluna2 = { width: 70, layout: 'form', border: false, items: [edNumeroNota] };
        var l2_coluna3 = { width: 100, layout: 'form', border: false, items: [edDataNota] };
        var l2_coluna4 = { width: 80, layout: 'form', border: false, items: [edValorTotalNota] };
        var l2_coluna5 = { width: 100, layout: 'form', border: false, items: [txtProtocolo] };
        /*var l2_coluna4 = { width: 80, layout: 'form', border: false, items: [edPesoTotal] };
        var l2_coluna5 = { width: 80, layout: 'form', border: false, items: [edPesoRateio] };
		*/
        var linha2 = {
            layout: 'column',
            border: false,
            items: [l2_coluna1, l2_coluna2, l2_coluna3, l2_coluna4, l2_coluna5]
        };
    
    arrLinhas.push(linha2);

    /*
    var edValorNota = {
        xtype: 'masktextfield',
        id: 'edValorNota',
        fieldLabel: 'Valor Rateio',
        name: 'edValorNota',
        allowBlank: false,
        width: 70,
        mask: '#9.999.990,00',
		money: true,
        maxLength: 13,
        value: "0,00"     
    };

    var edConteiner = {
        xtype: 'textfield',
        id: 'edConteiner',
        fieldLabel: 'Conteiner',
        name: 'edConteiner',
        width: 70,        
        style:{textTransform:'uppercase'},
        enableKeyEvents: true,
        listeners:{
            'blur': function (field) {
            }
        }
    };
	*/
    var l3_coluna1 = { width: 80, layout: 'form', border: false, items: [edValorTotalNota] };
    /*var l3_coluna2 = { width: 80, layout: 'form', border: false, items: [edValorNota] };
    var l3_coluna3 = { width: 80, layout: 'form', border: false, items: [edConteiner] };
	
    var arrLinha3 = new Array();
    arrLinha3.push(l3_coluna1);
    arrLinha3.push(l3_coluna2);
    arrLinha3.push(l3_coluna3);
    var linha3 = {
        layout: 'column',
        border: false,
        items: arrLinha3
    };

    arrLinhas.push(linha3);
	*/
     var campoCnpjRem = {
            fieldLabel: 'CNPJ/CPF',
            xtype: 'textfield',
            name: 'edCnpjRemetente',
            id: 'edCnpjRemetente',
            width: 115,
            allowBlank: false,
			cls: 'x-item-disabled',
			readOnly: true,
			autoCreate: { tag: 'input', type: 'text', maxlength: '14', autocomplete: 'off' },
           listeners:{
                change: function (field, newValue, oldValue) {
                    if (newValue != ""){
                        CarregarDadosCnpjEdicao(newValue, 'Remetente');
                    }
                },
                disable: function(field)
                {
                    Ext.getCmp("edBtnCnpjRemetente").hide();
                },
                enable: function(field)
                {
                    Ext.getCmp("edBtnCnpjRemetente").show();
                }
            }
        };

    var l_campo_cnpjRem = { width: 120, layout: 'form', border: false, items: [campoCnpjRem] };

    var botaoCnpjRem = new Ext.Button({
    	margins: { top: 50 },
    	name: 'edBtnCnpjRemetente',
    	hidden: true,
    	iconCls: 'icon-find',
    	id: 'edBtnCnpjRemetente',
    	handler: function (a, b, c) {
    		PesquisarEmpresa('Remetente');
    	}
    });

    // var l_campo_botaoRem = { width: 25, layout: 'form', margins: { top: 50 }, border: false, items: [botaoCnpjRem] };//
    var l_campo_botaoRem = { width: 25, layout: 'form', style: { "margin-top": "17px" }, border: false, items: [botaoCnpjRem] };
    var linhaCnpjRem = { layout: 'column', border: false, items: [l_campo_cnpjRem, l_campo_botaoRem] };

    var fieldsetRemetente = {
        xtype: 'fieldset',
        title: 'Remetente',
        collapsible: false,
        width: 195,
        height: 285,
        defaults: { width: 170 },		
        items: [linhaCnpjRem, 
        {
            fieldLabel: 'Sigla',
            xtype: 'textfield',
            name: 'edSiglaRemetente',
            id: 'edSiglaRemetente',
            cls: 'x-item-disabled',
            readOnly: true
        }, {
            fieldLabel: 'Nome',
            xtype: 'textfield',
            name: 'edRemetente',
            id: 'edRemetente',
            cls: 'x-item-disabled',
            readOnly: true
        }, {
            fieldLabel: 'UF',
            name: 'edUfRemetente',
            xtype: 'textfield',
            id: 'edUfRemetente',
            cls: 'x-item-disabled',
            readOnly: true
        }, {
            fieldLabel: 'Inscrição Estadual',
            xtype: 'textfield',
            name: 'edInscricaoEstadualRemetente',
            id: 'edInscricaoEstadualRemetente',
            cls: 'x-item-disabled',
            readOnly: true
        }]
    };
    var l5_coluna1 = { width: 200, layout: 'form', border: false, items: [fieldsetRemetente] };

    var campoCnpj = {
            xtype:'textfield',
            fieldLabel: 'CNPJ/CPF',
            name: 'edCnpjDestinatario',
            id: 'edCnpjDestinatario',
            width: 115,
            allowBlank: false,
			readOnly: true,
			cls: 'x-item-disabled',
			autoCreate: { tag: 'input', type: 'text', maxlength: '14', autocomplete: 'off' },
            listeners:{
                change: function (field, newValue, oldValue) {
                    if (newValue != ""){
                        CarregarDadosCnpjEdicao(newValue, 'Destinatario');
                    }
                },
                disable: function(field)
                {
                    Ext.getCmp("edBtnCnpjDestinatario").hide();
                },
                enable: function(field)
                {
                    Ext.getCmp("edBtnCnpjDestinatario").show();
                }
            }
        };

    var l_campo_cnpj = { width: 120, layout: 'form', border: false, items: [campoCnpj] };
    
	var botaoCnpj = new Ext.Button({
    	margins: { top: 50 },
    	name: 'edBtnCnpjDestinatario',
    	hidden: true,
    	iconCls: 'icon-find',
    	id: 'edBtnCnpjDestinatario',
    	handler: function (a, b, c) {
    		PesquisarEmpresa('Destinatario');
    	}
    });

    var l_campo_botao = { width: 25, layout: 'form', style: { "margin-top": "17px" }, border: false, items: [botaoCnpj] };

    var linhaCnpj = { layout: 'column', border: false, items: [l_campo_cnpj, l_campo_botao] };  

    var fieldsetDestinatario = {
        xtype: 'fieldset',
        title: 'Destinatário',
        collapsible: false,       
        width: 200,
        height: 285,
        defaults: { width: 170 },
        items: [linhaCnpj,{
			text: 'Empresa do exterior informar zeros.',
            name: 'edinfcnpjDestinatario',
            id: 'edinfcnpjDestinatario',  
			labelStyle: 'font: italic 8px ',
            style: 'font-style:italic',
			hidden: true,
			height: 3,
			xtype: 'label'            
		}, {
            fieldLabel: 'Sigla',
            name: 'edSiglaDestinatario',
            id: 'edSiglaDestinatario',
            cls: 'x-item-disabled',
            xtype: 'textfield',
            readOnly: true
        }, {
            fieldLabel: 'Nome',
            name: 'edDestinatario',
            id: 'edDestinatario',
            cls: 'x-item-disabled',
            xtype: 'textfield',
            readOnly: true
        }, {
            fieldLabel: 'UF',
            name: 'edUfDestinatario',
            id: 'edUfDestinatario',
            cls: 'x-item-disabled',
            xtype: 'textfield',
            readOnly: true
        }, {
            fieldLabel: 'Inscrição Estadual',
            name: 'edInscricaoEstadualDestinatario',
            id: 'edInscricaoEstadualDestinatario',
            cls: 'x-item-disabled',
            xtype: 'textfield',
            readOnly: true
        }]
    };
    var l5_coluna2 = { width: 205, layout: 'form', border: false, items: [fieldsetDestinatario] };

    var linha5 = {
        layout: 'column',
        border: false,
        items: [l5_coluna1, l5_coluna2]
    };
    arrLinhas.push(linha5);

    var formNotaFiscal = new Ext.form.FormPanel
	({
		id: 'formNotaFiscal',
		labelWidth: 80,
		width: 440,
		height: 475,
		autoScroll: true,
		bodyStyle: 'padding: 15px',
		labelAlign: 'top',
		listeners:{
            'hide': function (e) {
				HabilitarEdicaoNfe(false);
            }
        },
		items:
		[
            arrLinhas
		],
		buttonAlign: "center",
		buttons:
			[{
				text: "Salvar",
				handler: function () {
				
					 if(ValidarFormularioNota())
					 {						
						var dadosNfe = {
								chave:	Ext.getCmp('edNfChaveNfe').getValue(),						
								serie:	Ext.getCmp('edSerieNota').getValue(),
								numero: Ext.getCmp('edNumeroNota').getValue(),						
								valorTotal:	Ext.getCmp('edValorTotalNota').getValue(),
								remetente: Ext.getCmp('edRemetente').getValue(),
								destinatario: Ext.getCmp('edDestinatario').getValue(),
								cnpjRemetente: Ext.getCmp('edCnpjRemetente').getValue(),
								cnpjDestinatario: Ext.getCmp('edCnpjDestinatario').getValue(),
								dataNotaFiscal: Ext.getCmp('edDataNota').getValue(),
								siglaRemetente: Ext.getCmp('edSiglaRemetente').getValue(),
								siglaDestinatario: Ext.getCmp('edSiglaDestinatario').getValue(),
								ufRemetente: Ext.getCmp('edUfRemetente').getValue(),
								ufDestinatario: Ext.getCmp('edUfDestinatario').getValue(),
								inscricaoEstadualRemetente: Ext.getCmp('edInscricaoEstadualRemetente').getValue(),
								inscricaoEstadualDestinatario: Ext.getCmp('edInscricaoEstadualDestinatario').getValue(),
						        protocolo: Ext.getCmp('txtProtocolo').getValue()
							};

						if(!IndObtidoAutomatico)
						{
							fctProcessarNfeManual(dadosNfe);
						}

						fctSalvarNotaAnulacao(dadosNfe);					
					 }
				}
			},
			{
				text: "Cancelar",
				handler: function () {
					windowNotaFiscal.hide();
				}
			}]
	});  
	
	function fctSalvarNotaAnulacao(dadosNfe) 
    {
        var listaEnvio = Array();
        var listaVerificacao = Array();

		var selected = smRaiz.getSelections();
		var numeroCtes = "";
		for (var i = 0; i < selected.length; i++) {     
                    
                var cteAnulacao =
                {
                   CteId: selected[i].data.CteId,
                   Chave: selected[i].data.Chave,
                   Numero: selected[i].data.NroCte,
                   SerieCte: selected[i].data.SerieCte,
                   RateioCte: selected[i].data.RateioCte
                };

		    listaVerificacao.push(cteAnulacao);           				
            listaEnvio.push(selected[i].data.CteId);
            
            numeroCtes += selected[i].data.Cte +",";
        }

	    numeroCtes = numeroCtes.substr(0, numeroCtes.length - 1);
		var dadosVerificacao = $.toJSON(listaVerificacao);
		var dadosGravacao = $.toJSON(listaEnvio);
               
		$.ajax({
			url: "<%= Url.Action("ObterCtesRateio") %>",
			type: "POST",
			dataType: 'json',
			data: dadosVerificacao,
			async: false,
			contentType: "application/json; charset=utf-8",
			success: function(result) {
                if (result.PossuiMsg) {
                    msg = result.Message + "Deseja realmente informar a nota " + dadosNfe.chave + " para anulação dos ctes? ";
                }
				else {
					msg = "Você possui " + listaEnvio.length + " Ct-e para anular, deseja realmente informar a nota "+dadosNfe.chave+" para anulação dos ctes ("+numeroCtes+")?";
				}
			},
			failure: function(result) {
				Ext.getBody().unmask();
			}
		});

		if (Ext.Msg.confirm("Anulação", msg, 
				function(btn, text) {
					if (btn == 'yes') 
					{					
						Ext.getCmp("formNotaFiscal").getEl().mask("Processando a anulação do(s) CTe(s)", "x-mask-loading");
						var chave = $.toJSON(listaEnvio);

						Ext.Ajax.request({                
							url: "<% =Url.Action("SalvarAnulacaoCte") %>",
							success: function(response) {
							Ext.getCmp("formNotaFiscal").getEl().unmask();
								var data = Ext.decode(response.responseText);

								if(!data.Erro) {
									Ext.Msg.show({
										title: "Mensagem de Informação",
										msg: "Sucesso na Anulação do(s) CTe(s)!",
										buttons: Ext.Msg.OK,
										minWidth: 200
									});
						
									windowNotaFiscal.hide();
								} 
								else {
									Ext.Msg.show({
											title: "Mensagem de Erro",
											msg: "Erro na Anulação do(s) CTe(s)!</BR>" + data.Mensagem,
											buttons: Ext.Msg.OK,
											icon: Ext.MessageBox.ERROR,
											minWidth: 200
										});
								}

							},
							failure: function(conn, data){
							Ext.getCmp("formNotaFiscal").getEl().unmask();
								Ext.Msg.show({
										title: "Mensagem de Erro",
										msg: "Erro na Anulação do(s) CTe(s)!</BR>" + data.Message,
										buttons: Ext.Msg.OK,
										icon: Ext.MessageBox.ERROR,
										minWidth: 200
									});
							},
							method: "POST",
							params: { chaveNfe: dadosNfe.chave, protocolo: dadosNfe.protocolo, ctes: listaEnvio }
						});
					}
			}));

	}

	function ValidarFormularioNota() {
        var erroForm = false;
		
        erroForm = !Ext.getCmp("formNotaFiscal").getForm().isValid();

		var dataNota = Ext.getCmp("edDataNota").getValue();
		var dataAtual = new Date();

		if( dataNota > dataAtual)
		{
		    Ext.getCmp("edDataNota").markInvalid("A Data Nota Fiscal não pode ultrapassar a data atual.");
            erroForm = true;
		}
		
		if(Ext.getCmp("edSerieNota").getValue().trim() == "")
		{
			Ext.getCmp("edSerieNota").markInvalid("Campo série da Nota obrigatório.");
			erroForm = true;
		}

		if(Ext.getCmp("edNumeroNota").getValue().trim() == "")
		{
			Ext.getCmp("edNumeroNota").markInvalid("Campo número da Nota obrigatório.");
			erroForm = true;
		}

        var UFDestinatario = Ext.getCmp("edUfDestinatario").getValue();
        var NomeDestinatario = Ext.getCmp("edDestinatario").getValue();	
		
		if(UFDestinatario != "EX" &&  NomeDestinatario.trim() == "")
		{
			Ext.getCmp("edCnpjDestinatario").markInvalid("Não foi encontrada nenhuma empresa para este CNPJ de destino.");
			erroForm = true;
		}
		
		if (!ValidarCampoMaiorZero("edValorTotalNota")) 
		{
                Ext.getCmp("edValorTotalNota").markInvalid("O valor total da nota deve ser maior que zero.");
                erroForm = true;
        }

		var diferenca = totalCtes - parseFloat(Ext.getCmp("edValorTotalNota").getValue());
		diferenca = Math.abs(diferenca);
		diferenca = parseFloat(diferenca.toFixed(2));
		
		if(diferenca > margemLibAnulacao){
	         Ext.getCmp("edValorTotalNota").markInvalid("A somatória de valor dos CT-es deve igual ao valor total da nota .");
             erroForm = true;
	    }
	    
        if (erroForm){ 
            var mensagemErro = "Foram encontrados os erros abaixo:<br />";
            Ext.getCmp("formNotaFiscal").getForm().items.each(function(f){
               if(f.activeError!= undefined){
                   mensagemErro += " - " + f.fieldLabel + ": " + f.activeError + "<br />";
               }
            });

            Ext.Msg.alert('Erro de validação', mensagemErro);
            return false;
        }   
          
        return true;
    }

	 function ValidarCampoMaiorZero(nomeCampo){
        var erroMenorZero = false;

        if (Ext.getCmp(nomeCampo).isValid())
        {
            var valorCampo = Ext.getCmp(nomeCampo).getValue();
            if (isNaN(valorCampo)){
                Ext.getCmp(nomeCampo).markInvalid("O campo deve ser um valor numérico.");
                erroMenorZero = true;
            }
            else
            {
                if (valorCampo <= 0){
                    Ext.getCmp(nomeCampo).markInvalid("O campo deve ser maior que zero.");
                    erroMenorZero = true;
                }
            }
        }
        
        // alert(erroMenorZero);
        return !erroMenorZero;
    }

	function ObterDadosNfe(chaveNfe){
	
        Ext.getCmp("formNotaFiscal").getEl().mask("Obtendo dados da Nota Fiscal Eletronicamente", "x-mask-loading");
		HabilitarEdicaoNfe(false);

		$.ajax({
			url: "<%= Url.Action("ObterDadosNfe") %>",
			type: "POST",
			dataType: 'json',
			async: true,
			data: { chaveNfe: chaveNfe },
            timeout: 300000,
			success: function(data) {								
				if(!data.Erro)
				{					
					IndObtidoAutomatico = true;
				
					if(data.CodigoStatusRetorno == 3)
					{
						if(confirm(data.Mensagem + "\n\nDevido à este problema, é necessário fazer o preenchimento manual da nota fiscal.\n\n A nfe foi emitida em contigência?"))
						{
							// Habilita a edição da Nfe
							HabilitarEdicaoNfe(true);
						}
						else
						{
							windowNotaFiscal.hide();
						}
						return;
					}
				
					if(data.IndLiberaDigitacao)
					{
						var mensagem = data.Mensagem + "<br /><br />Devido à este problema, é necessário fazer o preenchimento manual da nota fiscal.";
						Ext.Msg.alert('Ops...', mensagem);
						HabilitarEdicaoNfe(true);
					}
				
					AtualizarDadosNotas(data.DadosNfe);
				
				}else{
					Ext.Msg.alert('Ops...', data.Mensagem);
				}

				Ext.getCmp("formNotaFiscal").getEl().unmask();
			},
			error: function(jqXHR, textStatus, thrownError){
			
					var mensagemDeErro;
                    var url = "<%= Url.Action("ObterDadosNfe", null, null, "http") %>";

                    if(textStatus == "timeout")
                        mensagemDeErro = "Ops... Não foi possível carregar o endereço \"" + url + "\" à tempo!\n A nota será liberada para digitação."
                    else if(textStatus != "")
                        mensagemDeErro = "Ocorreu um erro inesperado: " + jqXHR.statusText + " - " + (eval("(" + jqXHR.responseText + ")")).message;
                    else 
                        mensagemDeErro = "Ocorreu um erro inesperado.";  
                    
                    var data = { Mensagem: mensagemDeErro, StackTrace: "" };
                    
                    alert(mensagemDeErro);                    
                }
		});
    }

	function LimparCampos()
	{
		// limpa OS CAMPOS 
		AtualizarCampoNotaFiscal("edSerieNota", "");
		AtualizarCampoNotaFiscal("edNumeroNota", "");   
		AtualizarCampoNotaFiscal("edValorTotalNota", "");
		AtualizarCampoNotaFiscal("edRemetente", "");
		AtualizarCampoNotaFiscal("edDestinatario", "");
		AtualizarCampoNotaFiscal("edCnpjRemetente", "");
		AtualizarCampoNotaFiscal("edCnpjDestinatario", "");
		AtualizarCampoNotaFiscal("edDataNota", "");
		AtualizarCampoNotaFiscal("edSiglaRemetente", "");
		AtualizarCampoNotaFiscal("edSiglaDestinatario", "");
		AtualizarCampoNotaFiscal("edUfRemetente", "");
		AtualizarCampoNotaFiscal("edUfDestinatario", "");
		AtualizarCampoNotaFiscal("edInscricaoEstadualRemetente", "");
		AtualizarCampoNotaFiscal("edInscricaoEstadualDestinatario", "");
	}

	function HabilitarEdicaoNfe(liberar)
	{
		IndObtidoAutomatico = !liberar;

		Ext.getCmp('edSerieNota').setReadOnly(!liberar);
		Ext.getCmp('edNumeroNota').setReadOnly(!liberar);
		Ext.getCmp('edDataNota').setDisabled(!liberar);
		Ext.getCmp('edValorTotalNota').setReadOnly(!liberar);
		Ext.getCmp('edCnpjRemetente').setReadOnly(!liberar);
		Ext.getCmp('edCnpjDestinatario').setReadOnly(!liberar);
		
		if(liberar)
		{
			Ext.getCmp('edBtnCnpjRemetente').show();
			Ext.getCmp('edBtnCnpjDestinatario').show();
		}else{
			Ext.getCmp('edBtnCnpjRemetente').hide();
			Ext.getCmp('edBtnCnpjDestinatario').hide();
		}
	}

	function fctProcessarNfeManual(dadosNfe)
	{
		var chave							= dadosNfe.chave;
		var serie							= dadosNfe.serie ;
		var numero							= dadosNfe.numero ;
        var valorTotal						= dadosNfe.valorTotal ;
		var remetente						= dadosNfe.remetente ;
		var destinatario					= dadosNfe.destinatario;
		var cnpjRemetente					= dadosNfe.cnpjRemetente;
		var cnpjDestinatario				= dadosNfe.cnpjDestinatario;
		var dataNotaFiscal					= dadosNfe.dataNotaFiscal ;
		var siglaRemetente					= dadosNfe.siglaRemetente;
		var SiglaDestinatario				= dadosNfe.siglaDestinatario;
        var UfRemetente						= dadosNfe.UfRemetente;
		var UfDestinatario					= dadosNfe.UfDestinatario;
		var InscricaoEstadualRemetente		= dadosNfe.InscricaoEstadualRemetente;
		var InscricaoEstadualDestinatario	= dadosNfe.InscricaoEstadualDestinatario;
	    var protocolo                   	= dadosNfe.protocolo;

	    Ext.Ajax.request({                
			url: "<% =Url.Action("ProcessarNfeManual") %>",
			success: function(response) {
                var data = Ext.decode(response.responseText);

				if (data.Erro)
                {
                    Ext.Msg.alert('Ops...', data.Mensagem);
                }
			},
            failure: function(conn, data){
               Ext.Msg.alert("Ocorreu um erro inesperado");              
            },
			method: "POST",
			params: {
					chave: chave,						
					serie: serie,						
					numero: numero,						
					valorTotal:valorTotal,					
					remetente: remetente,					
					destinatario: destinatario,
					cnpjRemetente: cnpjRemetente,
					cnpjDestinatario: cnpjDestinatario,
					dataNotaFiscal: dataNotaFiscal,
					siglaRemetente: siglaRemetente,
					siglaDestinatario: SiglaDestinatario,
					ufRemetente: UfRemetente,
					ufDestinatario: UfDestinatario,
					inscricaoEstadualRemetente: InscricaoEstadualRemetente,
					inscricaoEstadualDestinatario: InscricaoEstadualDestinatario,
			        protocolo: protocolo
				 }
		});

	}

	function AtualizarCampoNotaFiscal(campo, valor, mask) {
        if (valor != null && valor != "") {
            if (mask != null && mask != "") {
                valor = Ext.util.Format.number(valor, mask);
            }
		}
        
		Ext.getCmp(campo).setValue(valor);        
    }


	function AtualizarDadosNotas(dadosNfe){
	
			if (dadosNfe != null) {
			
				// ATUALIZA OS CAMPOS 
				AtualizarCampoNotaFiscal("edSerieNota", dadosNfe.SerieNotaFiscal);
				AtualizarCampoNotaFiscal("edNumeroNota", dadosNfe.NumeroNotaFiscal);   
				AtualizarCampoNotaFiscal("edValorTotalNota", dadosNfe.Valor);
				AtualizarCampoNotaFiscal("edRemetente", dadosNfe.Remetente);
				AtualizarCampoNotaFiscal("edDestinatario", dadosNfe.Destinatario);
				AtualizarCampoNotaFiscal("edCnpjRemetente", dadosNfe.CnpjRemetente);
				AtualizarCampoNotaFiscal("edCnpjDestinatario", dadosNfe.CnpjDestinatario);
				if (dadosNfe.DataNotaFiscal != null && dadosNfe.DataNotaFiscal != "")
				{
					AtualizarCampoNotaFiscal("edDataNota", dadosNfe.DataNotaFiscal);
				}
				AtualizarCampoNotaFiscal("edSiglaRemetente", dadosNfe.SiglaRemetente);
				AtualizarCampoNotaFiscal("edSiglaDestinatario", dadosNfe.SiglaDestinatario);
				AtualizarCampoNotaFiscal("edUfRemetente", dadosNfe.UfRemetente);
				AtualizarCampoNotaFiscal("edUfDestinatario", dadosNfe.UfDestinatario);
				AtualizarCampoNotaFiscal("edInscricaoEstadualRemetente", dadosNfe.InscricaoEstadualRemetente);
				AtualizarCampoNotaFiscal("edInscricaoEstadualDestinatario", dadosNfe.InscricaoEstadualDestinatario);
				
				if (dadosNfe.IndObtidoAutomatico)
				{
					Ext.getCmp("edSerieNota").disable();
					Ext.getCmp("edNumeroNota").disable();
					Ext.getCmp("edCnpjRemetente").disable();

					if (dadosNfe.IndContingencia)
					{
						CarregarDadosCnpjEdicao(dadosNfe.CnpjRemetente, 'Remetente');
						Ext.getCmp("edCnpjDestinatario").enable();
						Ext.getCmp("edDataNota").enable();
						Ext.getCmp("edValorTotalNota").enable();
					}else{
						Ext.getCmp("edCnpjDestinatario").disable();
						Ext.getCmp("edDataNota").disable();
						Ext.getCmp("edValorTotalNota").disable();
					}
				}else{
            
					Ext.getCmp("edCnpjRemetente").setValue(dadosNfe.CnpjRemetente);
					Ext.getCmp("edInscricaoEstadualRemetente").setValue(dadosNfe.InscricaoEstadualRemetente);
					Ext.getCmp("edRemetente").setValue(dadosNfe.Remetente);
					Ext.getCmp("edSiglaRemetente").setValue(dadosNfe.SiglaRemetente);
					Ext.getCmp("edUfRemetente").setValue(dadosNfe.UfRemetente);

					Ext.getCmp("edCnpjDestinatario").setValue(dadosNfe.CnpjDestinatario);
					Ext.getCmp("edInscricaoEstadualDestinatario").setValue(dadosNfe.InscricaoEstadualDestinatario);
					Ext.getCmp("edDestinatario").setValue(dadosNfe.Destinatario);
					Ext.getCmp("edSiglaDestinatario").setValue(dadosNfe.SiglaDestinatario);
					Ext.getCmp("edUfDestinatario").setValue(dadosNfe.UfDestinatario);          
				}
			}
		}

	 function CarregarDadosCnpjEdicao(val, sufixo) { 
        if (val == "" || val == undefined || val == null){
            return false;
        }

        Ext.Ajax.request({                
			    url: "<% =Url.Action("ObterDadosCnpj") %>",
			    success: function(response) {
                    var data = Ext.decode(response.responseText);

					if (!data.Erro)
                    {				
                     if(data.Exterior && sufixo == 'Destinatario')
						{						
							Ext.getCmp("edUf"+sufixo).setValue("EX");
						}else{
							Ext.getCmp("edInscricaoEstadual"+sufixo).setValue(data.Dados.InscricaoEstadual);
							Ext.getCmp("ed"+sufixo).setValue(data.Dados.Nome);
							Ext.getCmp("edSigla"+sufixo).setValue(data.Dados.Sigla);
							Ext.getCmp("edUf"+sufixo).setValue(data.Dados.Uf);
						}
                    }else{
                        Ext.Msg.alert('Ops...', data.Mensagem);
                        Ext.getCmp("edInscricaoEstadual"+sufixo).setValue("");
                        Ext.getCmp("ed"+sufixo).setValue("");
                        Ext.getCmp("edSigla"+sufixo).setValue("");
                        Ext.getCmp("edUf"+sufixo).setValue("");
						Ext.getCmp("edCnpj"+sufixo).markInvalid(data.Mensagem);
                    }
			    },
                failure: function(conn, data){
                    Ext.getCmp("edInscricaoEstadual"+sufixo).setValue("");
                    Ext.getCmp("ed"+sufixo).setValue("");
                    Ext.getCmp("edSigla"+sufixo).setValue("");
                    Ext.getCmp("edUf"+sufixo).setValue("");
                    alert("Ocorreu um erro inesperado");
                },
			    method: "POST",
			    params: { cnpj: val}
		    });
    }

</script>
