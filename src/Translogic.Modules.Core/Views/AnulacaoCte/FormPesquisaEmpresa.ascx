﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div id="div-form-pesquisa-empresa">
</div>
<script type="text/javascript">
    /**********************************************************
    FORM PESQUISA DE EMPRESA - INICIO
    **********************************************************/
    var labelHeader = {
        xtype: 'label',
        html: '<div>Caso não seja encontrada a empresa na pesquisa, é necessário ser cadastrado pela Central de Fluxos.</div>',
        height: 15
    };

    var cpHiddenCnpj = {
        name: 'cpHiddenCnpj',
        id: 'cpHiddenCnpj',
        xtype: 'hidden'
    };

    var cpCnpjEmpresa = {
        fieldLabel: 'CNPJ/CPF',
        name: 'cpCnpjEmpresa',
        id: 'cpCnpjEmpresa',
        allowBlank: false,
        width: 120,
        xtype: 'textfield',
        autoCreate: { tag: 'input', type: 'text', maxlength: '14', autocomplete: 'off' }
    };

    var colCpCnpjEmpresa = { width: 130, layout: 'form', border: false, items: [cpCnpjEmpresa, cpHiddenCnpj] };

    var cpNomeEmpresa = {
        xtype: 'textfield',
        fieldLabel: 'Nome',
        name: 'cpNomeEmpresa',
        style: { textTransform: 'uppercase' },
        xtype: 'textfield',
        width: 120,
        id: 'cpNomeEmpresa'
    };
    var colCpNomeEmpresa = { width: 130, layout: 'form', border: false, items: [cpNomeEmpresa] };

    var lnCpEmpresa = {
        layout: 'column',
        border: false,
        items: [colCpCnpjEmpresa, colCpNomeEmpresa]
    };

    var gridEmpresaStore = new Ext.data.JsonStore({
        url: '<%= Url.Action("PesquisarEmpresas") %>',
        root: "Items",
        fields: [
				'Id',
                'Cnpj',
                'RazaoSocial',
                'Sigla',
                'Uf',
                'InscricaoEstadual'
			]
    });

    var detGridCpEmpresa = new Ext.ux.grid.RowActions({
        dataIndex: '',
        header: '',
        align: 'center',
        actions: [{
            iconCls: 'icon-go',
            tooltip: 'Detalhes'
        }],
        callbacks: {
            'icon-go': function (grid, record, action, row, col) {
                Ext.getCmp("cpHiddenCnpj").setValue(record.data.Cnpj);
                windowPesquisaEmpresa.hide();
            }
        }
    });

    var gridCpEmpresa = new Ext.grid.GridPanel({
        id: 'gridCpEmpresa',
        name: 'gridCpEmpresa',
        region: 'center',
        store: gridEmpresaStore,
        stripeRows: false,
        height: 175,
        width: 567,
        plugins: [detGridCpEmpresa],        
        colModel: new Ext.grid.ColumnModel({
            defaults: {
                sortable: false
            },
            columns: [
            new Ext.grid.RowNumberer(),
             detGridCpEmpresa,
            {header: 'CNPJ', dataIndex: "Cnpj", sortable: false },
            { header: 'Razão Social', dataIndex: "RazaoSocial", sortable: false },
            { header: 'Sigla', dataIndex: "Sigla", sortable: false },
            { header: 'UF', dataIndex: "Uf", sortable: false },
            { header: 'Insc. Est.', dataIndex: "InscricaoEstadual", sortable: false }
        ]
        })

    });

    var formPesquisaEmpresa = new Ext.form.FormPanel
	({
	    id: 'formPesquisaEmpresa',
	    labelWidth: 80,
	    width: 585,
	    labelAlign: 'top',
	    height: 320,
	    bodyStyle: 'padding: 15px',
	    items:
		[
            labelHeader,
            lnCpEmpresa,
            gridCpEmpresa
		],
	    buttonAlign: "center",
	    buttons:
			[{
			    text: "Pesquisar",
			    iconCls: 'icon-find',
			    handler: function () {
			        if (Ext.getCmp("cpCnpjEmpresa").getValue() == '' && Ext.getCmp("cpNomeEmpresa").getValue() == '') {
			            Ext.Msg.show({
			                title: "Erro",
			                msg: "Preencha os filtros!",
			                buttons: Ext.Msg.OK,
			                icon: Ext.MessageBox.INFO,
			                minWidth: 200
			            });
			        }

			        gridCpEmpresa.getStore().load({
                        params: {
                            'cnpj': Ext.getCmp("cpCnpjEmpresa").getValue(),
                            'razaoSocial': Ext.getCmp("cpNomeEmpresa").getValue()
                        }
                    });
			    }
			},
			{
			    text: "Cancelar",
			    handler: function () {
			        windowPesquisaEmpresa.hide();
			    }
			}]
});


    gridCpEmpresa.getStore().proxy.on('beforeload', function (p, params) {

        params['filter[0].Campo'] = 'CNPJ';
        params['filter[0].Valor'] = Ext.getCmp("cpCnpjEmpresa").getValue();
        params['filter[0].FormaPesquisa'] = 'Start';

        params['filter[1].Campo'] = 'RazaoSocial';
        params['filter[1].Valor'] = Ext.getCmp("cpNomeEmpresa").getValue();
        params['filter[1].FormaPesquisa'] = 'Start';
    });



    /**********************************************************
    FORM PESQUISA DE EMPRESAS - FIM
    **********************************************************/
</script>
