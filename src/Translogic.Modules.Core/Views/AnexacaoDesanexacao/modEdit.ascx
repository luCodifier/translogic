﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div id="div-modal-edit"></div>
<%
    var Os = ViewData["Os"];
    var VagaoLocomotiva = ViewData["VagaoLocomotiva"];
    var Cad = ViewData["Cad"];
    var Pedido = ViewData["Pedido"];
    var Localizacao = ViewData["Localizacao"];
    var Situacao = ViewData["Situacao"];
    var Lotacao = ViewData["Lotacao"];
    var Recomendacao = ViewData["Recomendacao"];
%>
<script type="text/javascript">
    var Os = '<%=Os %>';
    var VagaoLocomotiva = '<%=VagaoLocomotiva %>';
    var Cad = '<%=Cad %>';
    var Pedido = '<%=Pedido %>';
    var Localizacao = '<%=Localizacao %>';
    var Situacao = '<%=Situacao %>';
    var Lotacao = '<%=Lotacao %>';
    var Recomendacao = '<%=Recomendacao %>';

    //JAVASCRIPT
    function SalvarAlteracaoVeiculo(numeroVeiculo) {

        loader("Salvando LOG edição Veiculo...");
        $.ajax
        ({
            url: '<%= Url.Action("SalvarAlteracaoVeiculo","AnexacaoDesanexacao") %>',
            type: "POST",
            dataType: 'json',
            data: $.toJSON(
                {
                    os: OS,
                    numeroVeiculo : numeroVeiculo,
                    numeroVeiculoOLD : VagaoLocomotiva,
                    localizacao : Localizacao
                }),
            contentType: "application/json; charset=utf-8",
            async: true,
            success: function (result) {
                if (result.Result) {

                    closeLoader();

                    Ext.Msg.show(
                        {
                            title: "",
                            msg: "Veiculo salvo com sucesso!",
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.INFO,
                            fn: function (buttonValue) {

                                if (buttonValue == "ok") {
                                    modEdit.close();
                                    //$("#modEdit").hide();
                                    //$('.x-tool-close').click();
                                }

                            }
                        });
                }

            },
            error: function (result) {
                closeLoader();

                var mensagem = "Ocorreu erro ao editar Veiculo";

                if (result != null) {
                    if (result.Message != null) {
                        mensagem = result.Message;
                    }
                }

                Ext.Msg.show(
                    {
                        title: "Erro",
                        msg: mensagem,
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.ERROR
                    });
            }
        });
    }

    var txtVagaoLocomotiva = {
        xtype: 'textfield',
        name: 'txtVagaoLocomotiva',
        id: 'txtVagaoLocomotiva',
        fieldLabel: 'Veiculo',
        maskRe: /[a-zA-Z0-9]/,
        autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '7' },
        allowBlank: false,
        width: 70,
        style: 'text-transform:uppercase;',
        disabled: false,
        value: VagaoLocomotiva
    };
    
    var frmtxtVagaoLocomotiva = {
        //bodyStyle: 'padding-top:5px',
        layout: 'form',
        border: false,
        items: [txtVagaoLocomotiva]
    };

    var txtCad = {
        xtype: 'textfield',
        name: 'txtCad',
        id: 'txtCad',
        fieldLabel: 'Cad',
        maskRe: /[a-zA-Z0-9]/,
        autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '3' },
        allowBlank: false,
        width: 25,
        style: 'text-transform:uppercase;',
        disabled: true,
        value: Cad
    };

    var frmtxtCad = {
        //bodyStyle: 'padding-top:5px',
        layout: 'form',
        border: false,
        items: [txtCad]
    };

    
    var txtPedido = {
        xtype: 'textfield',
        name: 'txtPedido',
        id: 'txtPedido',
        fieldLabel: 'Pedido',
        maskRe: /[a-zA-Z0-9]/,
        autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '3' },
        allowBlank: false,
        width: 25,
        style: 'text-transform:uppercase;',
        disabled: true,
        value: Pedido
    };

    var frmtxtPedido = {
        //bodyStyle: 'padding-top:5px',
        layout: 'form',
        border: false,
        items: [txtPedido]
    };

    
    var txtLocalizacao = {
        xtype: 'textfield',
        name: 'txtLocalizacao',
        id: 'txtLocalizacao',
        fieldLabel: 'Trem',
        maskRe: /[a-zA-Z0-9]/,
        autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '3' },
        allowBlank: false,
        width: 100,
        style: 'text-transform:uppercase;',
        disabled: true,
        value: Localizacao
    };

    var frmtxtLocalizacao = {
        //bodyStyle: 'padding-top:5px',
        layout: 'form',
        border: false,
        items: [txtLocalizacao]
    };

    
    var txtSituacao = {
        xtype: 'textfield',
        name: 'txtSituacao',
        id: 'txtSituacao',
        fieldLabel: 'Situacao',
        maskRe: /[a-zA-Z0-9]/,
        autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '3' },
        allowBlank: false,
        width: 100,
        style: 'text-transform:uppercase;',
        disabled: true,
        value: Situacao
    };

    var frmtxtSituacao = {
        //bodyStyle: 'padding-top:5px',
        layout: 'form',
        border: false,
        items: [txtSituacao]
    };
   
    var txtLotacao = {
        xtype: 'textfield',
        name: 'txtLotacao',
        id: 'txtLotacao',
        fieldLabel: 'Lotacao',
        maskRe: /[a-zA-Z0-9]/,
        autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '3' },
        allowBlank: false,
        width: 100,
        style: 'text-transform:uppercase;',
        disabled: true,
        value: Lotacao
    };

    var frmtxtLotacao = {
        //bodyStyle: 'padding-top:5px',
        layout: 'form',
        border: false,
        items: [txtLotacao]
    };
 
    var txtRecomendacao = {
        xtype: 'textfield',
        name: 'txtRecomendacao',
        id: 'txtRecomendacao',
        fieldLabel: 'Trem',
        maskRe: /[a-zA-Z0-9]/,
        autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '3' },
        allowBlank: false,
        width: 100,
        style: 'text-transform:uppercase;',
        disabled: true,
        value: Recomendacao
    };

    var frmtxtRecomendacao = {
        //bodyStyle: 'padding-top:5px',
        layout: 'form',
        border: false,
        items: [txtRecomendacao]
    };

    var dpData = new Ext.form.DateField({
        fieldLabel: 'Data',
        id: 'dpData',
        name: 'dpData',
        dateFormat: 'd/n/Y',
        width: 83,
        plugins: [new Ext.ux.InputTextMask('99/99/9999', true)],
        //disabled: (visualizar == "sim" ? true : false),
        value: dataProximaExecucao
    });

    var tpTime = new Ext.form.TimeField({
        name: 'tpTime',
        id: 'tpTime',
        format: 'H:i',
        increment: 1,
        fieldLabel: 'Hora',
        maxlength: 5,
        plugins: [new Ext.ux.InputTextMask('99:99', true)],
        width: 60,
        editable: true,
        //disabled: (visualizar == "sim" ? true : false),
        value: horaProximaExecucao
    });

    var camposEdicao = {
        xtype: 'fieldset',
        id: 'camposEdicao',
        name: 'camposEdicao',
        //height: 50,
        //width: true,
        items: [
                frmtxtVagaoLocomotiva,
                frmtxtCad,
                frmtxtPedido,
                frmtxtLocalizacao,
                frmtxtSituacao,
                frmtxtLotacao,
                frmtxtRecomendacao 
            ]
    };

    var formVeiculosTrem = new Ext.form.FormPanel
    ({
        id: 'formVeiculosTrem',
        //labelWidth: 80,
        //labelAlign: 'top',
        height: 255,
        bodyStyle: 'padding: 5px',
        items:
        [
            camposEdicao
        ],
        buttonAlign: "center",
        buttons:
        [
            {
                id: 'btnSalvar',
                text: "Salvar",
                formBind: true,
                iconCls: 'icon-save',
                handler: function () {
                  
                    var veiculo = Ext.getCmp("txtVagaoLocomotiva").getRawValue();
                    if (veiculo == "") {
                        Ext.Msg.show({ title: 'Aviso', msg: 'Campo “VEÍCULO” é obrigatório.', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                        return false;
                    }
                    SalvarAlteracaoVeiculo(Ext.getCmp("txtVagaoLocomotiva").getValue());
                }
            },
            {
                id: 'btnCancelar',
                text: "Cancelar",
                handler: function () {
                    modEdit.close();
                }
            }
        ]
    });

    /******************************* RENDER *******************************/
    Ext.onReady(function () {
        formVeiculosTrem.render("div-modal-edit");
        //Ext.getCmp('txtValorMercadoria').setReadOnly(true);
    });

</script>