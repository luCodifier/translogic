﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div id="div-modal-VeiculosTrem"></div>
<%
    var OS = ViewData["OS"];
    var Trem = ViewData["Trem"];
    var LocalParada = ViewData["LocalParada"];
    var dataProximaExecucao = ViewData["DataProximaExecucao"];
    var horaProximaExecucao = ViewData["HoraProximaExecucao"];
    var Acao = ViewData["Acao"];
%>
<script type="text/javascript">
    var gridVeiculos;
    var OS = '<%=OS %>';
    var Trem = '<%=Trem %>';
    var LocalParada = '<%=LocalParada %>';
    var dataProximaExecucao = '<%=dataProximaExecucao %>';
    var horaProximaExecucao = '<%=horaProximaExecucao %>';
    var Acao = '<%=Acao %>';

    //JAVASCRIPT
    function SalvarAlteracaoTrem(dataHora) {

        loader("Salvando LOG edição Trem...");
        $.ajax
                ({
                    url: '<%= Url.Action("SalvarAlteracaoTrem","AnexacaoDesanexacao") %>',
                    type: "POST",
                    dataType: 'json',
                    data: $.toJSON(
                        {
                            os: OS,
                            dataHora: dataHora
                        }),
                    contentType: "application/json; charset=utf-8",
                    async: true,
                    success: function (result) {
                        if (result.Result) {

                            closeLoader();

                            Ext.Msg.show(
                                {
                                    title: "",
                                    msg: "Trem salvo com sucesso!",
                                    buttons: Ext.Msg.OK,
                                    icon: Ext.MessageBox.INFO,
                                    fn: function (buttonValue) {
                                        if (buttonValue == "ok") 
                                        {
                                            modVeiculosTrem.close();
                                        }

                                    }
                                });
                        }

                    },
                    error: function (result) {
                        closeLoader();

                        var mensagem = "Ocorreu erro ao editar Trem";

                        if (result != null) {
                            if (result.Message != null) {
                                mensagem = result.Message;
                            }
                        }

                        Ext.Msg.show(
                            {
                                title: "Erro",
                                msg: mensagem,
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.ERROR
                            });
                    }
                });
        }
        function SalvarExclusaoVeiulo(record) {
            var VagaoLocomotiva = record.data.VagaoLocomotiva;
            var Localizacao = record.data.LocalParada

            loader("Salvando LOG exclusão Veiculo...");
            $.ajax
        ({
            url: '<%= Url.Action("SalvarExclusaoVeiculo","AnexacaoDesanexacao") %>',
            type: "POST",
            dataType: 'json',
            data: $.toJSON(
                {
                    os: OS,
                    numeroVeiculo: VagaoLocomotiva,
                    localizacao: Localizacao
                }),
            contentType: "application/json; charset=utf-8",
            async: true,
            success: function (result) {
                if (result.Result) {

                    closeLoader();

                    Ext.Msg.show(
                        {
                            title: "",
                            msg: "Veiculo Excluido com sucesso!",
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.INFO,
                            fn: function (buttonValue) {
                            }
                        });
                }

            },
            error: function (result) {
                closeLoader();

                var mensagem = "Ocorreu erro ao excluir Veiculo";

                if (result != null) {
                    if (result.Message != null) {
                        mensagem = result.Message;
                    }
                }

                Ext.Msg.show(
                    {
                        title: "Erro",
                        msg: mensagem,
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.ERROR
                    });
            }
        });
    }

    function showModalEdit(record) {
        modEdit = null;
        modEdit = new Ext.Window({
            name: 'modEdit',
            id: 'modEdit',
            title: 'Veiculo',
            modal: true,
            width: 300,
            height: 285,
            closable: true,
            autoLoad:
				    {
				        url: '<%= Url.Action("modEdit") %>',
				        params: { os: OS, vagaoLocomotiva: record.data.VagaoLocomotiva, cad: record.data.Cad, pedido: record.data.Pedido, localizacao: LocalParada, situacao: record.data.Situacao, lotacao: record.data.Lotacao, recomendacao: record.data.Recomendacao },
				        text: "Carregando Veiculo...",
				        scripts: true,
				        callback: function (el, sucess) {
				            if (!sucess) {
				                modEdit.close();
				            }
				        }
				    }
        });
				modEdit.show();
    }

    //STORES
    var gridVeiculosTremStore = new Ext.data.JsonStore({
        root: "Items",
        totalProperty: 'Total',
        id: 'gridVeiculosTremStore',
        name: 'gridVeiculosTremStore',
        autoLoad: true,
        proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("ObterConsultaVeiculosTrem","AnexacaoDesanexacao") %>', timeout: 600000 }),
        paramNames: {
            sort: "detalhesPaginacaoWeb.Sort",
            dir: "detalhesPaginacaoWeb.Dir",
            start: "detalhesPaginacaoWeb.Start",
            limit: "detalhesPaginacaoWeb.Limit"
        },
        fields: ['VagaoLocomotiva', 'StatusProcessamento', 'Cad', 'Pedido', 'Localizacao', 'Situacao', 'Lotacao', 'Recomendacao', 'LocalParada', 'DescErro']
    });

    var txtTrem = {
        xtype: 'textfield',
        name: 'txtTrem',
        id: 'txtTrem',
        fieldLabel: 'Trem',
        maskRe: /[a-zA-Z0-9]/,
        autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '3' },
        allowBlank: false,
        width: 35,
        style: 'text-transform:uppercase;',
        disabled: true,
        value: Trem
    };

    var txtLocalParada = {
        xtype: 'textfield',
        name: 'txtLocalParada',
        id: 'txtLocalParada',
        fieldLabel: 'Local/Parada',
        maskRe: /[a-zA-Z0-9]/,
        autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '3' },
        allowBlank: false,
        width: 35,
        style: 'text-transform:uppercase;',
        disabled: true,
        value: LocalParada
    };

    var txtteste = {
        xtype: 'textfield',
        name: 'txtteste',
        id: 'txtteste',
        fieldLabel: 'Ltxtteste',
        maskRe: /[a-zA-Z0-9]/,
        autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '3' },
        allowBlank: false,
        width: 50,
        style: 'text-transform:uppercase;'
    };

    var lblDataHora = {
        xtype: 'label',
        id: 'lblDataHora',
        text: 'Data e Hora: '
    };

    var dpData = new Ext.form.DateField({
        fieldLabel: 'Data',
        id: 'dpData',
        name: 'dpData',
        dateFormat: 'd/n/Y',
        width: 83,
        plugins: [new Ext.ux.InputTextMask('99/99/9999', true)],
        //disabled: (visualizar == "sim" ? true : false),
        value: dataProximaExecucao
    });

    var tpTime = new Ext.form.TimeField({
        name: 'tpTime',
        id: 'tpTime',
        format: 'H:i',
        increment: 1,
        fieldLabel: 'Hora',
        maxlength: 5,
        plugins: [new Ext.ux.InputTextMask('99:99', true)],
        width: 60,
        editable: true,
        //disabled: (visualizar == "sim" ? true : false),
        value: horaProximaExecucao
    });

    var frmtxtTrem = {
        //bodyStyle: 'padding-top:5px',
        layout: 'form',
        border: false,
        labelAlign: 'right',
		items: [txtTrem]
	};

    var frmtxtLocalParada = {
        layout: 'form',
        labelAlign: 'right',
		border: false,
		items: [txtLocalParada]
    };

    var frmdpData = {
        layout: 'form',
        labelAlign: 'right',
        border: false,
        items: [dpData]
    };
    var frmtpTime = {
        layout: 'form',
        labelAlign: 'right',
        border: false,
        items: [tpTime]
    };

    var linhaCampos = {
        layout: 'column',
        border: false,
        items: [frmtxtTrem, frmtxtLocalParada, frmdpData, frmtpTime]
    };


    var fsCampos = {
        xtype: 'fieldset',
        id: 'fsCampos',
        name: 'fsCampos',
        height: 50,
        autowidth: true,
        items: [linhaCampos]
    };

    //GRID Veiculos
    var Actions = new Ext.ux.grid.RowActions({
        id: 'acoes',
        dataIndex: '',
        header: 'Ações',
        align: 'center',
        autoWidth: false,
        width: 15,
        actions: [
                    { iconCls: 'icon-edit', tooltip: 'Editar Registro' },
                    { iconCls: 'icon-del', tooltip: 'Excluir Registro' }
                ],
        callbacks: {

            'icon-edit': function (grid, record, action, row, col) {
                if (record.data.StatusProcessamento != "S") {
                    showModalEdit(record);
                } else {
                    Ext.Msg.show({ title: "Mensagem de Erro",
                        msg: "Veículo não disponível para edição.",
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.ERROR
                    });
                    return false;
                }
            },
            'icon-del': function (grid, record, action, row, col) {
                if (record.data.StatusProcessamento != "S") {
                    if (Ext.Msg.confirm("Veiculo", "Deseja excluir veiculo?", function (btn, text) {
                        if (btn == 'yes') {
                            SalvarExclusaoVeiulo(record);
                        }
                    }));
                } else {
                    Ext.Msg.show({ title: "Mensagem de Erro",
                        msg: "Veículo não disponível para exclusão.",
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.ERROR
                    });
                    return false;
                }               
            }
        }
    });

    var cm = new Ext.grid.ColumnModel({
        defaults: {
            sortable: true
        },
        plugins: Actions,
        columns: [
                    new Ext.grid.RowNumberer(),
                    Actions,
                    { header: 'Vagão/Locomotiva', dataIndex: "VagaoLocomotiva", sortable: false, width: 30 },
                    { header: 'Status', dataIndex: "StatusProcessamento", sortable: false, width:13,  
                        renderer: function(value) 
                        {                           
                            if (value == 'S')
                                return "<img src='/Get.efh?file={Translogic.Core.Infrastructure.Web;}/Content/Images/Icons/go.png' qtip='Sim' alt='Sim' />";
                            else 
                                return "<img src='/Get.efh?file={Translogic.Core.Infrastructure.Web;}/Content/Images/Icons/stop.png' qtip='Não' alt='Não' />";
                        }
                    },
                    { header: 'Cad', dataIndex: "Cad", sortable: false, width: 10 },
                    { header: 'Pedido', dataIndex: "Pedido", sortable: false, width: 14 },
                    { header: 'Localização', dataIndex: "Localizacao", sortable: false, width: 20 },               
                    { header: 'Situação', dataIndex: "Situacao", sortable: false, width: 20 },
                    { header: 'Lotação', dataIndex: "Lotacao", sortable: false, width: 20 },
                    { header: 'Recomendação', dataIndex: "Recomendacao", sortable: false, width: 25 },
                    { header: 'Descrição/Erro', dataIndex: "DescErro", sortable: false, width: 80 }
                ]
    });

    gridVeiculos = new Ext.grid.EditorGridPanel({
        id: 'gridVeiculos',
        name: 'gridVeiculos',
        autoLoadGrid: true,
        height: 300,
        limit: 10,
        width: 960,
        stripeRows: true,
        cm: cm,
        region: 'center',
        viewConfig: {
            forceFit: true,
            emptyText: 'Não possui dado(s) para exibição.'
        },
        autoScroll: true,
        loadMask: { msg: App.Resources.Web.Carregando },
        store: gridVeiculosTremStore,
        plugins: [Actions]
    });

//    gridVeiculos.getStore().proxy.on('load', function (p, params) {
//        //BTN GRID 1            
//        //if (!podeInvestigacaoIncidente) {
//        Actions.tpl = new Ext.XTemplate(Actions.tpl.master.body.replace('<div class="ux-row-action-item icon-edit " style="" qtip="Editar Registro"></div>', '<div class="ux-row-action-item icon-edit x-item-disabled" style="" qtip="Editar Registro"></div>'));
//        //}
//    });

    gridVeiculos.getStore().proxy.on('beforeload', function (p, params) {
        //Parametros de pesquisa(filtros) grid
        params['os'] = OS;
        params['estacao'] = LocalParada;
        params['acao'] = Acao;
    });


    var frmgridVeiculos = {
        layout: 'form',
        border: false,
        autoScroll: true,
        items: [{ layout: 'column', title: 'Veículos', border: false, items: [gridVeiculos]}]
    };

    var formVeiculosTrem = new Ext.form.FormPanel
    ({
        id: 'formVeiculosTrem',
        //labelWidth: 80,
        //labelAlign: 'top',
        height: 450,
        bodyStyle: 'padding: 5px',
        items:
        [
            fsCampos,
            frmgridVeiculos
        ],
        buttonAlign: "center",
        buttons:
        [
            {
                id: 'btnSalvar',
                text: "Salvar",
                formBind: true,
                iconCls: 'icon-save',
                handler: function () {

                    var data = Ext.getCmp("dpData").getRawValue();
                    var hora = Ext.getCmp("tpTime").getRawValue();
                    if (data == "") {
                        Ext.Msg.show({ title: 'Aviso', msg: 'Data não pode ser nula.', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                        return false;
                    }
                    if (hora == "") {
                        Ext.Msg.show({ title: 'Aviso', msg: 'Hora não pode ser nula.', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                        return false;
                    }

                    var dataHora = Ext.getCmp("dpData").getRawValue() + " " + Ext.getCmp("tpTime").getRawValue();
                    SalvarAlteracaoTrem(dataHora);
                }
            },
            {
                id: 'btnCancelar',
                text: "Cancelar",
                handler: function () {
                    modVeiculosTrem.close();
                }
            }
        ]
    });

    /******************************* RENDER *******************************/
    Ext.onReady(function () {
        formVeiculosTrem.render("div-modal-VeiculosTrem");
        //Ext.getCmp('txtValorMercadoria').setReadOnly(true);
    });

</script>