﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Interna.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content runat="server" ID="HeadContent" ContentPlaceHolderID="HeadContent">

    <style type="text/css">
    .aprovado /*verde*/
    {
        background-color: #2E8B57; 
        color: #000;        
    }       
       .reprovado /*vermelho*/ 
    {
        background-color: #FF7256; 
        color: #000;
    }       
    .pendente /*amarelo*/
    {
        background-color: #EEEE00; 
        color: #000;
    }        
</style>

    <script type="text/javascript">

        $(function () {

            /*GRID*******************************************************************/
            var gridStore = new Ext.data.JsonStore({
                root: "Items",
                totalProperty: 'Total',
                id: 'gridStore',
                name: 'gridStore',
                proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("ConsultaLiberacaoFormacaoTrem", "LiberacaoFormacaoTrem") %>', timeout: 600000 }),
                paramNames: {
                    sort: "detalhesPaginacaoWeb.Sort",
                    dir: "detalhesPaginacaoWeb.Dir",
                    start: "detalhesPaginacaoWeb.Start",
                    limit: "detalhesPaginacaoWeb.Limit"
                },
                fields: ['NomeSol', 'OrdemServico', 'OrigemDestinoTrem', 'LocalAtual', 'TipoTrava', 'MotivoTrava', 'DtPartida', 'DtSolicitacao', 'DtResposta', 'NomeAutorizador', 'SituacaoTrava']
            });

            var pagingToolbar = new Ext.PagingToolbar({
                pageSize: 50,
                store: gridStore,
                displayInfo: true,
                displayMsg: App.Resources.Web.GridExibindoRegistros,
                emptyMsg: App.Resources.Web.GridSemRegistros,
                paramNames: {
                    start: "detalhesPaginacaoWeb.Start",
                    limit: "detalhesPaginacaoWeb.Limit"
                }
            });

            var cm = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true
                },
                columns: [
                    new Ext.grid.RowNumberer(),
                    { header: 'Solicitante', dataIndex: "NomeSol", sortable: false, width: 100 },
                    { header: 'OS', dataIndex: "OrdemServico", sortable: false, width: 50 },
                    { header: 'Origem/Destino', dataIndex: "OrigemDestinoTrem", sortable: false, width: 80 },
                    { header: 'Local', dataIndex: "LocalAtual", sortable: false, width: 50 },
                    { header: 'Trava', dataIndex: "TipoTrava", sortable: false, width: 100 },
                    { header: 'Detalhe', dataIndex: "MotivoTrava", sortable: false, width: 150 },
                    { header: 'Data/Hora Trava', dataIndex: "DtPartida", sortable: false, width: 100 },
                    { header: 'Data/Hora Solicitação', dataIndex: "DtSolicitacao", sortable: true, width: 100 },
                    { header: 'Data Resposta', dataIndex: "DtResposta", sortable: false, width: 100 },
                    { header: 'Autorizador', dataIndex: "NomeAutorizador", sortable: false, width: 100 },
                    { header: 'Situação', dataIndex: "SituacaoTrava", sortable: false, width: 80 }
                ]
            });

            var grid = new Ext.grid.EditorGridPanel({
                id: 'grid',
                name: 'grid',
                autoLoadGrid: true,
                height: 360,
                width: 870,
                stripeRows: true,
                cm: cm,
                region: 'center',
                viewConfig: {
                    forceFit: true,
                    emptyText: 'Não possui dado(s) para exibição.',
                    getRowClass: function (row, index) {
                        return row.data.SituacaoTrava.toString().toLowerCase();
                    }
                },
                autoScroll: true,
                loadMask: { msg: App.Resources.Web.Carregando },
                store: gridStore,
                bbar: pagingToolbar
            });

            grid.getStore().proxy.on('beforeload', function (p, params) {
                var liberacaoFormacaoTremDto = montaObjetoTransferencia();
                params["liberacaoFormacaoTremDto"] = $.toJSON(liberacaoFormacaoTremDto);
            });

            /*CAMPOS*****************************************************************/
            var txtOs = {
                xtype: 'textfield',
                name: 'txtOs',
                id: 'txtOs',
                fieldLabel: 'OS',
                autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '10' },
                maskRe: /[0-9]/,
                style: 'text-transform: uppercase',
                width: 80
            };

            var txtOrigem = {
                xtype: 'textfield',
                name: 'txtOrigem',
                id: 'txtOrigem',
                fieldLabel: 'Origem',
                autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '3' },
                style: 'text-transform: uppercase',
                width: 50
            };

            var txtDestino = {
                xtype: 'textfield',
                name: 'txtDestino',
                id: 'txtDestino',
                fieldLabel: 'Destino',
                autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '3' },
                style: 'text-transform: uppercase',
                width: 50
            };

            var txtLocal = {
                xtype: 'textfield',
                name: 'txtLocal',
                id: 'txtLocal',
                fieldLabel: 'Local',
                autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '3' },
                style: 'text-transform: uppercase',
                maskRe: /[A-Za-z]/,
                width: 50
            };

            var txtNomeSolicitante = {
                xtype: 'textfield',
                name: 'txtNomeSolicitante',
                id: 'txtNomeSolicitante',
                fieldLabel: 'Nome Solicitante',
                autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '50' },
                width: 125
            };

            var txtNomeAutorizador = {
                xtype: 'textfield',
                name: 'txtNomeAutorizador',
                id: 'txtNomeAutorizador',
                fieldLabel: 'Nome Autorizador',
                autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '50' },
                width: 125
            };

            var txtDataInicial = new Ext.form.DateField({
                fieldLabel: 'Período Inicial',
                id: 'txtDataInicial',
                name: 'txtDataInicial',
                dateFormat: 'd/n/Y',
                width: 85,
                maxLength: 10,
                value: new Date(),
                plugins: [new Ext.ux.InputTextMask('99/99/9999', true)]
            });

            var txtDataFinal = new Ext.form.DateField({
                fieldLabel: 'Período Final',
                id: 'txtDataFinal',
                name: 'txtDataFinal',
                dateFormat: 'd/n/Y',
                width: 85,
                maxLength: 10,
                value: new Date(),
                plugins: [new Ext.ux.InputTextMask('99/99/9999', true)]
            });

            var radioStatus = {
                name: 'radioStatus',
                id: 'radioStatus',
                xtype: 'radiogroup',
                width: 310,
                items: [
                                 { boxLabel: 'Todos', name: 'rb-allow-credit', inputValue: 'Todos', id: 'todos' },
                                 { boxLabel: 'Pendente', name: 'rb-allow-credit', inputValue: 'Pendente', checked: true, Width: 75 },
                                 { boxLabel: 'Aprovado', name: 'rb-allow-credit', inputValue: 'Aprovado', Width: 75 },
                                 { boxLabel: 'Reprovado', name: 'rb-allow-credit', inputValue: 'Reprovado', Width: 75 }
                             ]
            };

            /*LAYOUT*****************************************************************/

            var formOs = {
                layout: 'form',
                width: 85,
                border: false,
                items: [txtOs]
            };

            var formOrigem = {
                layout: 'form',
                width: 55,
                border: false,
                items: [txtOrigem]
            };

            var formDestino = {
                layout: 'form',
                width: 55,
                border: false,
                items: [txtDestino]
            };

            var formLocal = {
                layout: 'form',
                width: 55,
                border: false,
                items: [txtLocal]
            };

            var formNomeSolicitante = {
                layout: 'form',
                width: 130,
                border: false,
                items: [txtNomeSolicitante]
            };

            var formNomeAutorizador = {
                layout: 'form',
                width: 130,
                border: false,
                items: [txtNomeAutorizador]
            };

            var formDataInicial = {
                layout: 'form',
                width: 90,
                border: false,
                items: [txtDataInicial]
            };

            var formDataFinal = {
                layout: 'form',
                width: 90,
                border: false,
                items: [txtDataFinal]
            };

            var formStatus = {
                layout: 'column',
                border: false,
                width: 315,
                items: [radioStatus]
            };

            /*Array de componentes do form************************/

            var columnItens = {
                layout: 'column',
                border: false,
                items: [formOs, formOrigem, formDestino, formLocal, formNomeSolicitante, formNomeAutorizador, formDataInicial, formDataFinal]
            };

            /*PAINEL**********************************************/

            var filtros = new Ext.form.FormPanel({
                id: 'grid-filtros',
                title: "Filtros",
                region: 'center',
                bodyStyle: 'padding:15px;',
                layout: 'form',
                labelAlign: 'top',
                items: [columnItens, formStatus],
                buttonAlign: "left",
                buttons:
                    [{
                        text: 'Pesquisar',
                        type: 'submit',
                        iconCls: 'icon-find',
                        handler: function (b, e) {

                            Pesquisar();




                            runner.start(task);
                        }
                    },
                    {
                        text: 'Exportar',
                        type: 'submit',
                        iconCls: 'icon-page-excel',
                        handler: function (b, e) {
                            Exportar();
                        }
                    },
                    {
                        text: 'Limpar',
                        type: 'submit',
                        handler: function (b, e) {
                            Limpar();
                        }
                    }
                    ]
            });

            new Ext.Viewport({
                layout: 'border',
                margins: 10,
                items: [
                    {
                        region: 'north',
                        height: 230,
                        autoScroll: false,
                        items: [{
                            region: 'center',
                            applyTo: 'header-content'
                        },
                            filtros]
                    },
                    grid
                ]
            });

            /*FUNÇÕES*********************************************/

            var interval = 30000; // Intervalo em milesegundos para o reload do grid (1000 = 1 second)

            var task = {
                run: function () {
                    Pesquisar();
                },
                interval: interval
            }

            var runner = new Ext.util.TaskRunner();

            function Pesquisar() {

                if (CamposValidos()) {
                    runner.stop(task);

                    Ext.getBody().mask("Processando dados...", "x-mask-loading");
                    gridStore.load({ params: { start: 0, limit: 50} });
                    Ext.getBody().unmask();

                    runner.start(task);
                }
            }

            function BuscaIntervalo() {

                interval = 10000;

                $.ajax({
                    url: '<%= Url.Action("ConsultaTimmerFormacaoTrem", "LiberacaoFormacaoTrem") %>',
                    type: "GET",
                    dataType: 'json',
                    timeout: 300000,
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        interval = data;
                    }
                });
            }

            function montaObjetoTransferencia() {
                var txtOs = Ext.getCmp("txtOs").getRawValue();
                var txtOrigem = Ext.getCmp("txtOrigem").getRawValue();
                var txtDestino = Ext.getCmp("txtDestino").getRawValue();
                var txtLocal = Ext.getCmp("txtLocal").getRawValue();
                var txtNomeSolicitante = Ext.getCmp("txtNomeSolicitante").getRawValue();
                var txtNomeAutorizador = Ext.getCmp("txtNomeAutorizador").getRawValue();
                var txtDataInicial = Ext.getCmp("txtDataInicial").getRawValue();
                var txtDataFinal = Ext.getCmp("txtDataFinal").getRawValue();
                var radioStatus = StatusSelecionado(Ext.getCmp("radioStatus"));

                var dateParts = txtDataInicial.split("/");
                txtDataInicial = dateParts[1] + "/" + dateParts[0] + "/" + dateParts[2]; // month is 0-based

                dateParts = txtDataFinal.split("/");
                txtDataFinal = dateParts[1] + "/" + dateParts[0] + "/" + dateParts[2]; // month is 0-based

                var liberacaoFormacaoTremDto = {
                    OrdemServico: txtOs == "" ? 0 : txtOs,
                    OrigemTrem: txtOrigem,
                    DestinoTrem: txtDestino,
                    LocalAtual: txtLocal,
                    DtPeriodoInicial: txtDataInicial,
                    DtPeriodoFinal: txtDataFinal,
                    NomeAutorizador: txtNomeAutorizador,
                    NomeSol: txtNomeSolicitante,
                    SituacaoTrava: radioStatus
                };

                return liberacaoFormacaoTremDto;
            }

            function Exportar() {

                if (CamposValidos()) {

                    Ext.getBody().mask("Processando dados...", "x-mask-loading");

                    var url = '<%= Url.Action("ConsultaLiberacaoFormacaoTremExportar", "LiberacaoFormacaoTrem") %>';
                    url += "?liberacaoFormacaoTremDto=" + $.toJSON(montaObjetoTransferencia());
                    window.open(url, "");

                    Ext.getBody().unmask();
                }
            }

            function Limpar() {
                gridStore.removeAll();
                Ext.getCmp("grid-filtros").getForm().reset();

                SetarValoresDatas();
            }

            function MontarDataString(addDays) {

                var today = new Date();
                var dd = today.getDate() + addDays;
                var mm = today.getMonth() + 1; //January is 0!
                var yyyy = today.getFullYear();

                if (dd < 10) {
                    dd = '0' + dd
                }

                if (mm < 10) {
                    mm = '0' + mm
                }

                today = dd + '/' + mm + '/' + yyyy;

                return today;
            }

            function SetarValoresDatas() {

                var hoje = MontarDataString(0);
                var ontem = MontarDataString(-1);

                Ext.getCmp("txtDataInicial").setValue(ontem);
                Ext.getCmp("txtDataFinal").setValue(hoje);
            }

            function CompararDatas(dataIni, dataFim) {

                var arrDtIni = dataIni.split('/');
                var arrDtFim = dataFim.split('/');

                var dtIni = new Date(arrDtIni[2] + '/' + arrDtIni[1] + '/' + arrDtIni[0]);
                var dtFim = new Date(arrDtFim[2] + '/' + arrDtFim[1] + '/' + arrDtFim[0]);

                return dtIni <= dtFim;
            }

            function CamposValidos() {

                var dataIni = Ext.getCmp("txtDataInicial").getRawValue();
                var dataFim = Ext.getCmp("txtDataFinal").getRawValue();
                var hoje = MontarDataString(0);

                // Data inicial menor que data de hoje
                if (!CompararDatas(dataIni, hoje)) {
                    Ext.Msg.show({ title: 'Aviso', msg: 'Data Inicial deve ser menor ou igual a data corrente!', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                    return false;
                }

                // Data inicial menor que data de hoje
                if (!CompararDatas(dataFim, hoje)) {
                    Ext.Msg.show({ title: 'Aviso', msg: 'Data Final deve ser menor ou igual a data corrente!', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                    return false;
                }

                // Data final maior que data inicial
                if (!CompararDatas(dataIni, dataFim)) {
                    Ext.Msg.show({ title: 'Aviso', msg: 'Data Final deve ser maior ou igual a Data Inicial!', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                    return false;
                }

                return true;
            }

            function StatusSelecionado(radio) {
                if (radio.items.items[0].checked)
                    return "Todos";
                if (radio.items.items[1].checked)
                    return "Pendente";
                if (radio.items.items[2].checked)
                    return "Aprovado";
                if (radio.items.items[3].checked)
                    return "Reprovado";
            }

            // Setar valores das datas
            SetarValoresDatas();

            // Setar intervalo de reload do grid
            BuscaIntervalo();

        });


        Ext.onReady(function () {

            $("#todos").parent().parent().parent().parent().attr('style', 'width:55px');

        });

    </script>
</asp:Content>
<asp:Content runat="server" ID="Content2" ContentPlaceHolderID="Content">
    <div id="header-content">
        <h1>
            Solicitação de Liberação de Trava de Formação de Trem</h1>
        <small>Você está em Operação > Circulação > Liberação de Formação</small>
        <br />
    </div>
</asp:Content>

