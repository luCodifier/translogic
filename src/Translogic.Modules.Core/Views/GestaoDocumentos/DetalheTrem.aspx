﻿<%@ Import Namespace="System.Security.Policy" %>
<%@ Import Namespace="Translogic.Modules.Core.Domain.Model.Diversos.Mdfe" %>

<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Interna.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Import Namespace="Translogic.Modules.Core.Domain.Model.Dto" %>
<asp:Content runat="server" ID="HeadContent" ContentPlaceHolderID="HeadContent">
    <%
        TremDto trem = (TremDto)ViewData["Trem"];
    %>
    <script type="text/javascript" language="javascript">

    var idComposicao = 0;
    
     /******************************* STORE *******************************/
        
        var storeDetalhe = new Ext.data.JsonStore({
            root: "Items",
            autoLoad: false,
            url: '<%= Url.Action("DetalhesComposicao") %>',
            fields: [
                    'Id',
                    'Sequencia',
                    'Vagao',
                    'Origem',
                    'Destino',
                    'Mercadoria',
                    'TU',
                    'TB',
                    'Correntista',
                    'Tara',
                    'Destinatario',
                    'DataCarregamento',
                    'DataDescarregamento'
            ]
        });
        
        /************************* GRID **************************/
        var changeCursorToHand = function (val, metadata, record) {
            metadata.style = 'cursor: pointer;';
            return val;
         };

        var detalheAction = new Ext.ux.grid.RowActions({
           	dataIndex: '',
           	header: '',
           	align: 'center',
           	actions: [{
           		iconCls: 'icon-detail',
           		tooltip: 'Exibir Detalhes'
           	}],
           	callbacks: {
           		'icon-detail': function (grid, record, action, row, col) {
           			Detalhe(record.data.Id);
           		}
           	}
           });

        var grid = new Translogic.PaginatedGrid({
            id: 'grid',
            name: 'grid',
            autoLoadGrid: false,
            height: 120,
            width: 735,
            plugins: [detalheAction],
            fields: [
                'Id',
                'DataLiberacao',
                'Origem',
                'Destino'
            ],
            url: '<%= Url.Action("ListarComposicao") %>',
            columns: [
				{ dataIndex: "Id", hidden: true },
                detalheAction,
                { header: 'Origem', width: 80, dataIndex: "Origem", sortable: false },
                { header: 'Destino', width: 80, dataIndex: "Destino", sortable: false },
                { header: 'Data Partida', width: 80, dataIndex: "DataLiberacao", sortable: false }
            ]
        });

         grid.getStore().proxy.on('beforeload', function (p, params) {
            params['filter[0].Campo'] = 'idTrem';
             params['filter[0].Valor'] = '<%=trem.Id %>';
            params['filter[0].FormaPesquisa'] = 'Start';
        });

        sm = new Ext.grid.CheckboxSelectionModel();
        
        var gridComposicaoDetalhe = new Ext.grid.GridPanel({
                id: 'gridComposicaoDetalhe',
                name: 'gridComposicaoDetalhe',
                autoLoadGrid: false,
                height: 200,
                width: 735,
                autoScroll: true,
                sm: sm,
                stripeRows: true,
                region: 'center',
                store: storeDetalhe,
                loadMask: { msg: App.Resources.Web.Carregando },
                colModel: new Ext.grid.ColumnModel({
                    defaults: {
                        sortable: false 
                    },
                        columns: [
                        sm, 
                        { header: 'Seq.', width: 35, dataIndex: "Sequencia", sortable: false },
                        { header: 'Vagão', width: 80, dataIndex: "Vagao", sortable: false },
                        { header: 'Origem', width: 50, dataIndex: "Origem", sortable: false },
                        { header: 'Destino', width: 50, dataIndex: "Destino", sortable: false },
                        { header: 'Mercadoria', width: 200, dataIndex: "Mercadoria", sortable: false },
                        { header: 'TU',width: 50, dataIndex: "TU",   sortable: false },
                        { header: 'TB', width: 50, dataIndex: "TB", sortable: false },
                        { header: 'Tara', width: 50, dataIndex: "Tara", sortable: false },
                        { header: 'Correntista', width: 150, dataIndex: "Correntista", sortable: false },
                        { header: 'Destinatario',width: 150, dataIndex: "Destinatario",   sortable: false },
                        { header: 'Carregamento', width: 120, dataIndex: "DataCarregamento", sortable: false },
                        { header: 'Descarregamento', width: 120, dataIndex: "DataDescarregamento", sortable: false }
                        ]
                })           
            });
            
        /************************* FILTROS **************************/
        var txtDataRealizadaPartida = {
        	xtype: 'textfield',
        	autoCreate: { tag: 'input', type: 'text', maxlength: '10', autocomplete: 'off' },
            width: 120,
            name: 'txtDataRealizadaPartida',
            allowBlank: true,
        	readOnly: true,
            id: 'txtDataRealizadaPartida',
            fieldLabel: 'Data Partida'
         };

        var txtFiltroOS = {
        	xtype: 'textfield',
        	autoCreate: { tag: 'input', type: 'text', maxlength: '10', autocomplete: 'off' },
            width: 70,
            maskRe: /[0-9]/,
            name: 'txtFiltroOS',
            allowBlank: true,
        	readOnly: true,
            id: 'txtFiltroOS',
            fieldLabel: 'OS'
         };
   
        var txtFiltroPrefixo = {
        	xtype: 'textfield',
        	style: 'text-transform: uppercase',
        	autoCreate: { tag: 'input', type: 'text', maxlength: '3', autocomplete: 'off' },
        	maskRe: /[a-zA-Z0-9]/,
            width: 60,
            name: 'txtFiltroPrefixo',
            allowBlank: true,
            id: 'txtFiltroPrefixo',
            fieldLabel: 'Prefixo',
        	readOnly: true
        };    
        
        var txtOrigem =	{
			xtype: 'textfield',
			vtype: 'cteestacaovtype',
			style: 'text-transform: uppercase',
			id: 'filtro-Ori',
			fieldLabel: 'Origem',
			autoCreate: { tag: 'input', type: 'text', maxlength: '3', autocomplete: 'off' },
			name: 'Origem',
			allowBlank: true,
			maxLength: 3,
			width: 35,
			hiddenName: 'Origem',
			readOnly: true
		};

		var txtDestino = {
			xtype: 'textfield',
			vtype: 'cteestacaovtype',
			style: 'text-transform: uppercase',
			id: 'filtro-Dest',
			fieldLabel: 'Destino',
			autoCreate: { tag: 'input', type: 'text', maxlength: '3', autocomplete: 'off' },
			name: 'Destino',
			allowBlank: true,
			maxLength: 3,
			width: 35,
			hiddenName: 'Destino',
			readOnly: true
		};

		var rdTipoImpressao = {
		    xtype: 'radiogroup',
		    fieldLabel: 'Tipo Impressão',
		    id: 'rdTipoImpressao',
		    width:170,
		    name: 'rdTipoImpressao',
		    items: [
		        { boxLabel: 'NF-e', name: 'tipoImpressao', inputValue: 0 , checked: true }, 
		        { boxLabel: 'Ticket', name: 'tipoImpressao', inputValue: 1}, 
		        { boxLabel: 'Ambos', name: 'tipoImpressao', inputValue: 2 }
		    ]
		};
          var btnImpressao = {
            name: 'btnImpressao',
            id: 'btnImpressao',
            text: 'Imprimir',
            handler: function () {
                if (gridComposicaoDetalhe.getSelectionModel().hasSelection()) {
                    var selected = gridComposicaoDetalhe.getSelectionModel().getSelections();
                    var listaEnvio = new Array();
                    
                    for (var i = 0; i < selected.length; i++) {
			                listaEnvio.push(selected[i].data.Id);
	                }
                    
                    var url = "<%= Url.Action("Imprimir") %>";
                    url += "?idComposicao=" + idComposicao;
                    url += "&vagoes="+ listaEnvio;
                    url += "&tipoImpressao="+ Ext.getCmp('rdTipoImpressao').getValue().getRawValue();

                    window.open(url, "");
                } else {
                     Ext.Msg.show({
       	                            title: "Mensagem de Informação",
       	                            msg: "Favor selecionar pelo menos um vagão.",
       	                            buttons: Ext.Msg.OK,
       	                            minWidth: 200
       	                        });
                }
            }
        };
        var coluna1Linha1 = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding-right:10px',
            items: [txtDataRealizadaPartida]
        };
        
        var coluna3Linha1 = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding-right:10px',
            items: [txtFiltroPrefixo]
        };

        var coluna4Linha1 = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding-right:10px',
            items: [txtFiltroOS]
        };

        var coluna5Linha1 = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding-right:10px',
            items: [txtOrigem]
        };

        var coluna6Linha1 = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding-right:10px',
            items: [txtDestino]
        };
        
        var coluna7Linha1 = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding-right:10px',
            items: [rdTipoImpressao]
        };

        var linha1 = {
            layout: 'column',
            border: false,

            items: [coluna3Linha1, coluna4Linha1, coluna5Linha1,coluna6Linha1, coluna1Linha1, coluna7Linha1]
        };
        
        var filtros = new Ext.form.FormPanel({
            id: 'filtros',
            title: 'Filtros',
            name: 'filtros',
            bodyStyle: 'padding: 10px',
            labelAlign: 'top',
            items: [linha1],
            width: 737,
            buttons: [btnImpressao],
            buttonAlign: 'center'
        });

        /************************* DETALHES **************************/
        var formDetalhes;

        function Detalhe(id) {
            this.idComposicao = id;
            gridComposicaoDetalhe.getStore().load({ params: { idComposicao: id} });
        }
        
        
	    /************************* FUNCOES DE APOIO **************************/
        function TratarTeclaEnter(f, e) {
            if (e.getKey() == e.ENTER) {
            	CarregarGrid();
            }
        }

         function CarregarDadosTrem() {
            Ext.getCmp("txtDataRealizadaPartida").setValue('<%=trem.DataRealizadaPartida %>');
            Ext.getCmp("txtFiltroPrefixo").setValue('<%=trem.Prefixo %>');
            Ext.getCmp("txtFiltroOS").setValue('<%=trem.OS %>');
            Ext.getCmp("filtro-Ori").setValue('<%=trem.Origem %>');
            Ext.getCmp("filtro-Dest").setValue('<%=trem.Destino %>');
        }
         
        function Limpar() {
            grid.getStore().removeAll();
            Ext.getCmp("filtros").getForm().reset();
        }
        
        function CarregarGrid() {
            grid.getStore().load();
        }

        /************************* RENDER **************************/
        Ext.onReady(function () {
            filtros.render(document.body);
            grid.render(document.body);
            gridComposicaoDetalhe.render(document.body);
            CarregarDadosTrem();
            CarregarGrid();
            gridComposicaoDetalhe.setAutoScroll(true);
        });

    </script>
</asp:Content>
<asp:Content runat="server" ID="Content2" ContentPlaceHolderID="Content">
    <div id="header-content">
        <h1>
            Faturamento - Composição do trem</h1>
        <small>Você está em Faturamento > Gestão de documentos</small>
        <br />
    </div>
</asp:Content>
