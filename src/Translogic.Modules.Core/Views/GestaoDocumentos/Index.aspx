﻿<%@ Import Namespace="System.Security.Policy" %>
<%@ Import Namespace="Translogic.Modules.Core.Domain.Model.Diversos.Mdfe" %>

<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Interna.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content runat="server" ID="HeadContent" ContentPlaceHolderID="HeadContent">
    <style type="text/css">
        .x-box-inner
        {
            min-height: 700px !important;            
        }
        
        #grid
        {
            top: 120px !important;
        }
    </style>
    <script src="/GetFile.efh?file={Translogic.Modules.Core;}/Content/Javascript/gestaodocumentos.js"></script>
    <script type="text/javascript" language="javascript">
        /************************* RENDER **************************/

        urlPesquisarVagoes = '<%= Url.Action("PesquisarVagoes") %>';

        urlDetalhes = '<%= Url.Action("DetalheTrem") %>';
        urlPesquisar = '<%= Url.Action("Pesquisar") %>';
        urlObterLinhas = '<%= Url.Action("ObterLinhasPorAreaOperacional") %>';
        urlImpressaoVagoes = '<%= Url.Action("ImprimirVagoes") %>';

        Ext.onReady(function () {
            iniciarGridTrem();
            iniciarFiltrosTrem();
            iniciarGridVagao();
            iniciarFiltrosVagao();
            iniciarTabs();

            tabControl.render(document.body);
        });
        
    </script>
</asp:Content>
<asp:Content runat="server" ID="Content2" ContentPlaceHolderID="Content">
    <div id="header-content">
        <h1>
            Faturamento - Gestão de Documentos</h1>
        <small>Você está em Faturamento > Gestão de Documentos</small>
        <br />
    </div>
</asp:Content>
