﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%@ Import Namespace="Translogic.Modules.Core.Domain.Model.Dto" %>
<%@ Import Namespace="Translogic.Modules.Core.Domain.Model.FluxosComerciais.DocumentosVagao" %>
<%@ Import Namespace="Translogic.Modules.Core.Domain.Model.FluxosComerciais.Nfes" %>
<%@ Import Namespace="Translogic.Modules.Core.Interfaces.GestaoDocumentos.Balanca" %>
<%  
    var notasTicket = ViewData["NOTATICKET"] as IList<NotaTicketVagao>;
    var dadosNotas = ViewData["DADOSNOTA"] as IList<NotaTicketDto>;
    var notasTicketCliente = ViewData["NOTATICKETCLIENTES"] as IList<NotaTicketVagaoClienteDto>;
    
    if (notasTicket == null)
    {
        var impressaoPainel = TempData["IMPRESSAOPAINELEXPEDICAO"] as bool?;
        if (impressaoPainel != null)
        {
            if (impressaoPainel.HasValue)
            {
                notasTicket = TempData["NOTATICKET"] as IList<NotaTicketVagao>;
                dadosNotas = TempData["DADOSNOTA"] as IList<NotaTicketDto>;
                notasTicketCliente = TempData["NOTATICKETCLIENTES"] as IList<NotaTicketVagaoClienteDto>;
            }
        }
    }
    
    var IdVagoes = notasTicket.Select(x => x.Vagao.CodigoVagao).Distinct().ToList();
%>
<div style="page-break-after: always;">
    <% for (var index = 0; index < IdVagoes.Count; index++)
       {
           var vagao = notasTicket.FirstOrDefault(x => x.Vagao.CodigoVagao == IdVagoes[index]).Vagao;
           var ticket = vagao.Ticket;
           var notaticket = notasTicket.FirstOrDefault(x => x.Vagao.CodigoVagao == IdVagoes[index]);           
           var ticket_cliente = notasTicketCliente.FirstOrDefault(x => x.NotaTicketVagaoId == notaticket.Id);
           var peso_BrutoVagaoRateio = String.Format("{0:0.000}", ((notasTicket.Where(x => x.Vagao.CodigoVagao == IdVagoes[index]).Sum(x => x.PesoRateio) + ((vagao.PesoTara / 100)*1000)) / 1000));

           if (IdVagoes.Count == 1)
           {
               var ajusteDoajuste = 1.0;
               if (vagao.AjusteTrg)
               {
                   ajusteDoajuste = 10;
               }
               vagao.PesoBruto =
                   (notasTicket.Where(x => x.Vagao.CodigoVagao == IdVagoes[index]).Sum(x => x.PesoRateio) + ((vagao.PesoTara / 100) * 1000)) / ajusteDoajuste;
           }
           else
           {
               var ajusteDoajuste = 1;
               if (vagao.AjusteTrg)
               {
                   ajusteDoajuste = 10;
               }
               vagao.PesoBruto =
                   ((notasTicket.Where(x => x.ClienteTicket == notasTicket.Where(y => y.Vagao.CodigoVagao == IdVagoes[index]).FirstOrDefault().ClienteTicket).Sum(x => x.PesoRateio) +
                     ((vagao.PesoTara / 100))) / ajusteDoajuste);
           }
               
           if (index % 4 == 0 && (index) != 0)
           {
    %>
</div>
<div style="page-break-after: always;">
    <% } %>
    <table>
        <tr>
            <td>
                <table style="width: 290px; font-family: Arial; font-size: 11px; border-style: solid;
                    border-width: 1px 1px 1px 1px;">
                    <tr style="height: 17px">
                        <td style="text-align: center; font-size: 15px; font-weight: bold; border-style: solid;
                            border-width: 0px 0px 1px 0px" colspan="2">
                            <%=ticket_cliente !=null ? ticket_cliente.Cliente : "América Latina Logistica - ALL" %>
                        </td>
                    </tr>
                    <tr style="height: 17px">
                        <td width="70px">
                            Origem
                        </td>
                        <td>
                            <%=ticket.Origem!= null ?ticket.Origem.DescricaoResumida : string.Empty%>
                            -
                            <%=ticket.Origem != null ? string.Format("{0}.{1}.{2}/{3}-{4}", ticket.Origem.Cgc.Substring(0, 2),
                                                                                               ticket.Origem.Cgc.Substring(2, 3),
                                                                                               ticket.Origem.Cgc.Substring(5, 3),
                                                                                               ticket.Origem.Cgc.Substring(8, 4),
                                                                                               ticket.Origem.Cgc.Substring(12, 2)) : string.Empty%>
                            <%=ticket.Origem!= null ?ticket.Origem.Cgc : string.Empty%>
                        </td>
                    </tr>
                    <tr style="height: 17px">
                        <td width="70px">
                            Destino
                        </td>
                        <td>
                            <%=ticket.Destino != null ? ticket.Destino.DescricaoResumida : string.Empty%>
                            -
                            <%=ticket.Destino != null ? string.Format("{0}.{1}.{2}/{3}-{4}", ticket.Destino.Cgc.Substring(0, 2),
                                                                                        ticket.Destino.Cgc.Substring(2, 3),
                                                                                        ticket.Destino.Cgc.Substring(5, 3),
                                                                                        ticket.Destino.Cgc.Substring(8, 4),
                                                                                        ticket.Destino.Cgc.Substring(12, 2)) : string.Empty%>
                            <%=ticket.Destino != null ? ticket.Destino.Cgc : string.Empty%>
                        </td>
                    </tr>
                    <tr style="height: 17px">
                        <td>
                            Data
                        </td>
                        <td>
                            <%=ticket.DataPesagem %>
                        </td>
                    </tr>
                    <tr style="height: 17px">
                        <td>
                            Balança
                        </td>
                        <td>
                            <%= ticket.NumeroBalanca ?? String.Empty %>
                        </td>
                    </tr>
                    <tr style="height: 17px">
                        <td>
                            Vagão
                        </td>
                        <td>
                            <%= !string.IsNullOrEmpty(vagao.SerieVagao) ? string.Format("{0} {1}", vagao.SerieVagao, vagao.CodigoVagao) : vagao.CodigoVagao %>
                        </td>
                    </tr>
                    <tr style="height: 17px">
                        <td>
                            Produto
                        </td>
                        <td>
                            <%= dadosNotas.FirstOrDefault(n => notasTicket.FirstOrDefault(v => v.Vagao.Id == vagao.Id).ChaveNfe.Equals(n.ChaveNfe)).Produto%>
                        </td>
                    </tr>
                    <tr style="height: 17px">
                        <td>
                            Peso Bruto
                        </td>
                        <td>
                            <%= vagao.PesoBrutoFormatado%>
                        </td>
                    </tr>
                    <tr style="height: 17px">
                        <td>
                            Peso Rateio
                        </td>
                        <td>
                            <%= peso_BrutoVagaoRateio%>
                        </td>
                    </tr>
                    <tr style="height: 17px">
                        <td>
                            Peso Tara
                        </td>
                        <td>
                            <%= vagao.PesoTaraFormatado%>
                        </td>
                    </tr>
                    <tr style="height: 17px">
                        <td>
                            Peso Líquido
                        </td>
                        <td>
                            <%--<%= vagao.PesoLiquidoFormatado%>--%>
                            <%=notasTicket.Count > 1 ? String.Format("{0:0.000}", notasTicket.Where(n => n.Vagao.CodigoVagao == vagao.CodigoVagao).Select(x => x.PesoRateio).Sum() / 1000) : vagao.PesoLiquidoFormatado%>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="border-style: solid; border-width: 0px 0px 1px 0px;">
                            <table style="width: 100%; font-family: Arial; font-size: 10px;">
                                <tr>
                                    <td style="text-align: center; font-size: 15px; border-style: solid; border-width: 1px 0px 1px 0px;"
                                        colspan="4">
                                        Notas Fiscais Carregadas
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Série
                                    </td>
                                    <td>
                                        Número
                                    </td>
                                    <td>
                                        Peso
                                    </td>
                                    <td>Saldo Disp.</td>
                                </tr>
                                <% foreach (var nota in notasTicket.Where(n => n.Vagao.CodigoVagao == vagao.CodigoVagao))
                                   {
                                       var dadosNfe = dadosNotas.FirstOrDefault(n => n.ChaveNfe.Equals(nota.ChaveNfe));                           
                                %>
                                <tr>
                                    <td>
                                        <%=dadosNfe!= null? dadosNfe.Serie : "----" %>
                                    </td>
                                    <td>
                                        <%=dadosNfe!= null? dadosNfe.Numero.ToString() : "----" %>
                                    </td>
                                    <td>
                                        <%=nota.PesoRateioFormatado %>
                                    </td>
                                    <td>
                                        <%=dadosNfe != null ? (dadosNfe.PesoDisponivel > 0 ? dadosNfe.PesoDisponivel.ToString("N3") : "0,000") : "0"%>
                                    </td>
                                </tr>
                                <%  } %>
                            </table>
                        </td>
                    </tr>
                    <tr style="height: 17px">
                        <td style="border-style: solid; border-width: 0px 0px 1px 0px;" colspan="2">
                            &nbsp;
                        </td>
                    </tr>
                    <tr style="height: 60px">
                        <td style="border-style: solid; border-width: 0px 0px 1px 0px;">
                            Observação:
                        </td>
                        <td style="border-style: solid; border-width: 0px 0px 1px 0px;">
                            <%=ticket.Observacao %>
                        </td>
                    </tr>
                    <tr style="height: 17px">
                        <td>
                            Resp:
                        </td>
                        <td>
                            <%=ticket.Responsavel %>
                        </td>
                    </tr>
                    <tr style="height: 17px">
                        <td>
                            Autenticacao
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr style="height: 17px">
                        <td colspan="2" style="text-align: left;">
                            Data Reimpressão:
                            <%=DateTime.Now.ToString() %>
                        </td>
                    </tr>
                </table>
            </td>
            <td style="width: 50px">
            </td>
            <%
                                   index++;
                                   if (index < IdVagoes.Count)
                                   {
                                       
                                       vagao = notasTicket.FirstOrDefault(x => x.Vagao.CodigoVagao == IdVagoes[index]).Vagao;
                                       ticket = vagao.Ticket;
                                       notaticket = notasTicket.FirstOrDefault(x => x.Vagao.CodigoVagao == IdVagoes[index]);
                                       ticket_cliente = notasTicketCliente.FirstOrDefault(x => x.NotaTicketVagaoId == notaticket.Id);

            %>
            <td>
                <table style="width: 290px; font-family: Arial; font-size: 11px; border-style: solid;
                    border-width: 1px 1px 1px 1px;">
                    <tr style="height: 17px">
                        <td style="text-align: center; font-size: 15px; font-weight: bold; border-style: solid;
                            border-width: 0px 0px 1px 0px" colspan="2">
                            <%=ticket_cliente !=null ? ticket_cliente.Cliente : "América Latina Logistica - ALL" %>
                        </td>
                    </tr>
                    <tr style="height: 17px">
                        <td width="70px">
                            Origem
                        </td>
                        <td>
                            <%= ticket.Origem != null ? ticket.Origem.DescricaoResumida : string.Empty %>
                            -
                            <%= ticket.Origem != null ? string.Format("{0}.{1}.{2}/{3}-{4}", ticket.Origem.Cgc.Substring(0, 2),
                                                                      ticket.Origem.Cgc.Substring(2, 3),
                                                                      ticket.Origem.Cgc.Substring(5, 3),
                                                                      ticket.Origem.Cgc.Substring(8, 4),
                                                                      ticket.Origem.Cgc.Substring(12, 2)) : string.Empty %>
                            <%= ticket.Origem != null ? ticket.Origem.Cgc : string.Empty %>
                        </td>
                    </tr>
                    <tr style="height: 17px">
                        <td width="70px">
                            Destino
                        </td>
                        <td>
                            <%= ticket.Destino != null ? ticket.Destino.DescricaoResumida : string.Empty %>-
                            <%= ticket.Destino != null ? string.Format("{0}.{1}.{2}/{3}-{4}", ticket.Destino.Cgc.Substring(0, 2),
                                                                       ticket.Destino.Cgc.Substring(2, 3),
                                                                       ticket.Destino.Cgc.Substring(5, 3),
                                                                       ticket.Destino.Cgc.Substring(8, 4),
                                                                       ticket.Destino.Cgc.Substring(12, 2)) : string.Empty %>
                            <%= ticket.Destino != null ? ticket.Destino.Cgc : string.Empty %>
                        </td>
                    </tr>
                    <tr style="height: 17px">
                        <td>
                            Data
                        </td>
                        <td>
                            <%= ticket.DataPesagem %>
                        </td>
                    </tr>
                    <tr style="height: 17px">
                        <td>
                            Balança
                        </td>
                        <td>
                            <%= ticket.NumeroBalanca ?? String.Empty %>
                        </td>
                    </tr>
                    <tr style="height: 17px">
                        <td>
                            Vagão
                        </td>
                        <td>
                            <%= !string.IsNullOrEmpty(vagao.SerieVagao) ? string.Format("{0} {1}", vagao.SerieVagao, vagao.CodigoVagao) : vagao.CodigoVagao %>
                        </td>
                    </tr>
                    <tr style="height: 17px">
                        <td>
                            Produto
                        </td>
                        <td>
                            <%= dadosNotas.FirstOrDefault(n => notasTicket.FirstOrDefault(v => v.Vagao.Id == vagao.Id).ChaveNfe.Equals(n.ChaveNfe)).Produto %>
                        </td>
                    </tr>
                    <tr style="height: 17px">
                        <td>
                            Peso Bruto
                        </td>
                        <td>
                            <%= vagao.PesoBrutoFormatado%>
                        </td>
                    </tr>
                    <tr style="height: 17px">
                        <td>
                            Peso Rateio
                        </td>
                        <td>
                            <%= peso_BrutoVagaoRateio%>
                        </td>
                    </tr>
                    <tr style="height: 17px">
                        <td>
                            Peso Tara
                        </td>
                        <td>
                            <%= vagao.PesoTaraFormatado %>
                        </td>
                    </tr>
                    <tr style="height: 17px">
                        <td>
                            Peso Líquido
                        </td>
                        <td>
                            <%= vagao.PesoLiquidoFormatado %>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="border-style: solid; border-width: 0px 0px 1px 0px;">
                            <table style="width: 100%; font-family: Arial; font-size: 10px;">
                                <tr>
                                    <td style="text-align: center; font-size: 15px; border-style: solid; border-width: 1px 0px 1px 0px;"
                                        colspan="4">
                                        Notas Fiscais Carregadas
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Série
                                    </td>
                                    <td>
                                        Número
                                    </td>
                                    <td>
                                        Peso
                                    </td>
                                    <td>
                                        Saldo Disp.
                                    </td>
                                </tr>
                                <% foreach (var nota in notasTicket.Where(n => n.Vagao.CodigoVagao == vagao.CodigoVagao))
                                   {
                                       var dadosNfe = dadosNotas.FirstOrDefault(n => n.ChaveNfe.Equals(nota.ChaveNfe));
                                %>
                                <tr>
                                    <td>
                                        <%= dadosNfe != null ? dadosNfe.Serie : "----" %>
                                    </td>
                                    <td>
                                        <%= dadosNfe != null ? dadosNfe.Numero.ToString() : "----" %>
                                    </td>
                                    <td>
                                        <%= nota.PesoRateioFormatado%>
                                    </td>
                                    <td>
                                        <%=dadosNfe != null ? (dadosNfe.PesoDisponivel > 0 ? dadosNfe.PesoDisponivel.ToString("N3") : "0,000") : "0" %>
                                    </td>
                                </tr>
                                <% } %>
                            </table>
                        </td>
                    </tr>
                    <tr style="height: 17px">
                        <td style="border-style: solid; border-width: 0px 0px 1px 0px;" colspan="2">
                            &nbsp;
                        </td>
                    </tr>
                    <tr style="height: 60px">
                        <td style="border-style: solid; border-width: 0px 0px 1px 0px;">
                            Observação:
                        </td>
                        <td style="border-style: solid; border-width: 0px 0px 1px 0px;">
                            <%= ticket.Observacao %>
                        </td>
                    </tr>
                    <tr style="height: 17px">
                        <td>
                            Resp:
                        </td>
                        <td>
                            <%= ticket.Responsavel %>
                        </td>
                    </tr>
                    <tr style="height: 17px">
                        <td>
                            Autenticacao
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr style="height: 17px">
                        <td colspan="2" style="text-align: left;">
                            Data Reimpressão:
                            <%= DateTime.Now.ToString() %>
                        </td>
                    </tr>
                </table>
            </td>
            <%
                                       }
                                       else
                                       {
            %>
            <td style="width: 290px;">
            </td>
            <%
                                   } 
            %>
        </tr>
    </table>
    <br />
    <%
           if ((index - 1) % 4 == 0 && (index - 1) != 0)
           { %>
    <%
       }
       }
   
    %>
</div>
