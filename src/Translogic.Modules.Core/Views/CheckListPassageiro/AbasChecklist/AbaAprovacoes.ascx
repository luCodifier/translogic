﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%
    var dadosAprovados = ViewData["Aprovacoes"] != null ? ViewData["Aprovacoes"] as Translogic.Modules.Core.Domain.Model.Dto.OsChecklistAprovadaDto : new Translogic.Modules.Core.Domain.Model.Dto.OsChecklistAprovadaDto();
    var idOsChecklistAprovacao = dadosAprovados.idOsChecklistAprovacao;
    var idOsChecklist = dadosAprovados.idOsChecklist;
    var aprovadorVia = dadosAprovados.ViaUsuarioNome;
    var matriculaVia = dadosAprovados.ViaUsuarioMatricula;
    var dataVia = ViewData["Aprovacoes"] != null ? (dadosAprovados.ViaDataAprovacao != new DateTime() ? (dadosAprovados.ViaDataAprovacao.HasValue ? dadosAprovados.ViaDataAprovacao.Value.ToString("dd/MM/yyyy HH:mm:ss") : "") : "") : "";
    var statusVia = ViewData["Aprovacoes"] != null ? (dadosAprovados.ViaStatus != 0 ? (dadosAprovados.ViaStatus == 'A' ? "Aprovado" : "Reprovado") : "") : "";
    var tipoAprVia = ViewData["Aprovacoes"] != null ? (dadosAprovados.ViaImportacao != 0 ? (dadosAprovados.ViaImportacao == 'I' ? "Importado" : "Usuário") : "") : "";
    var disableBtnDownVia = ViewData["Aprovacoes"] != null ? (dadosAprovados.ViaImportacao == 'I' ? "false" : "true") : "true";
    var obsVia = dadosAprovados.ViaObs != null ? dadosAprovados.ViaObs.Replace("'", "\\'").Replace("\n", "\\n") : "";    

    var aprovadorLoco = dadosAprovados.LocoUsuarioNome;
    var matriculaLoco = dadosAprovados.LocoUsuarioMatricula;
    var dataLoco = ViewData["Aprovacoes"] != null ? (dadosAprovados.LocoDataAprovacao != new DateTime() ? (dadosAprovados.LocoDataAprovacao.HasValue ? dadosAprovados.LocoDataAprovacao.Value.ToString("dd/MM/yyyy HH:mm:ss") : "") : "") : "";
    var statusLoco = ViewData["Aprovacoes"] != null ? (dadosAprovados.LocoStatus != 0 ? (dadosAprovados.LocoStatus == 'A' ? "Aprovado" : "Reprovado") : "") : "";
    var tipoAprLoco = ViewData["Aprovacoes"] != null ? (dadosAprovados.LocoImportacao != 0 ? (dadosAprovados.LocoImportacao == 'I' ? "Importado" : "Usuário") : "") : "";
    var disableBtnDownLoco = ViewData["Aprovacoes"] != null ? (dadosAprovados.LocoImportacao == 'I' ? "false" : "true") : "true";
    var obsLoco = dadosAprovados.LocoObs != null ? dadosAprovados.LocoObs.Replace("'", "\\'").Replace("\n", "\\n") : "";
    
    var aprovadorTrac = dadosAprovados.TracUsuarioNome;
    var matriculaTrac = dadosAprovados.TracUsuarioMatricula;
    var dataTrac = ViewData["Aprovacoes"] != null ? (dadosAprovados.TracDataAprovacao != new DateTime() ? (dadosAprovados.TracDataAprovacao.HasValue ? dadosAprovados.TracDataAprovacao.Value.ToString("dd/MM/yyyy HH:mm:ss") : "") : "") : "";    
    var statusTrac = ViewData["Aprovacoes"] != null ? (dadosAprovados.TracStatus != 0 ? (dadosAprovados.TracStatus == 'A' ? "Aprovado" : "Reprovado") : "") : "";
    var tipoAprTrac = ViewData["Aprovacoes"] != null ? (dadosAprovados.TracImportacao != 0 ? (dadosAprovados.TracImportacao == 'I' ? "Importado" : "Usuário") : "") : "";
    var disableBtnDownTrac = ViewData["Aprovacoes"] != null ? (dadosAprovados.TracImportacao == 'I' ? "false" : "true") : "true";
    var obsTrac = dadosAprovados.TracObs != null ? dadosAprovados.TracObs.Replace("'", "\\'").Replace("\n", "\\n") : "";

    var aprovadorVag = dadosAprovados.VagUsuarioNome;
    var matriculaVag = dadosAprovados.VagUsuarioMatricula;
    var dataVag = ViewData["Aprovacoes"] != null ? (dadosAprovados.VagDataAprovacao != new DateTime() ? (dadosAprovados.VagDataAprovacao.HasValue ? dadosAprovados.VagDataAprovacao.Value.ToString("dd/MM/yyyy HH:mm:ss") : "") : "") : "";   
    var statusVag = ViewData["Aprovacoes"] != null ? (dadosAprovados.VagStatus != 0 ?  (dadosAprovados.VagStatus == 'A' ? "Aprovado" : "Reprovado") : "") : "";
    var tipoAprVag = ViewData["Aprovacoes"] != null ? (dadosAprovados.VagImportacao != 0 ? (dadosAprovados.VagImportacao == 'I' ? "Importado" : "Usuário") : "") : "";
    var disableBtnDownVag = ViewData["Aprovacoes"] != null ? (dadosAprovados.VagImportacao == 'I' ? "false" : "true") : "true";
    var obsVag = dadosAprovados.VagObs != null ? dadosAprovados.VagObs.Replace("'", "\\'").Replace("\n", "\\n") : "";
    
    var permissoes = ViewData["Permissoes"] != null ? ViewData["Permissoes"] as Translogic.Modules.Core.Domain.Model.Dto.ChecklistPassageiro.OsChecklistPermissaoDto : new Translogic.Modules.Core.Domain.Model.Dto.ChecklistPassageiro.OsChecklistPermissaoDto();

    var somenteVisualizarCheklist = ViewData["SomenteVisualizarCheklist"] != null ? (ViewData["SomenteVisualizarCheklist"].ToString() == "sim" ? "true" : "false") : "false";
%>
<script type="text/javascript">

    window.geral = (function (geral) {
        var idOsChecklist = 0;
        var AbaAprovacoes = geral.AbaAprovacoes || {};

        var aprovarVia = '<%=permissoes.AprovarVia %>';
        var reprovarVia = '<%=permissoes.ReprovarVia %>';

        var aprovarLoco = '<%=permissoes.AprovarLoco %>';
        var reprovarLoco = '<%=permissoes.ReprovarLoco %>';

        var aprovarTracao = '<%=permissoes.AprovarTracao %>';
        var reprovarTracao = '<%=permissoes.ReprovarTracao %>';

        var aprovarVagao = '<%=permissoes.AprovarVagao %>';
        var reprovarVagao = '<%=permissoes.ReprovarVagao %>';

        var importar = '<%=permissoes.Importar %>';
        var importar = '<%=permissoes.Importar %>';

        var somenteVisualizarCheklist = '<%=somenteVisualizarCheklist %>';

        var obsVia = null;
        var obsLoco = null;
        var obsTracao = null;
        var obsVagao = null;

        function armazenaObservacoes() {
            obsVia = Ext.getCmp("txtObservacoesVia").getValue();
            obsLoco = Ext.getCmp("txtObservacoesLocomotiva").getValue();
            obsTracao = Ext.getCmp("txtObservacoesTracao").getValue();
            obsVagao = Ext.getCmp("txtObservacoesVagao").getValue();
        }

        function carregarDadosAprovacoes() {

            idOsChecklist = '<%=idOsChecklist %>';

            if (idOsChecklist == 0) {

                // Via Fields
                Ext.getCmp("lblAprovadorVia").setText("Aprovador: ");
                Ext.getCmp("lblMatriculaVia").setText("Matrícula: ");
                Ext.getCmp("lblDataVia").setText("Data: ");
                Ext.getCmp("lblStatusVia").setText("Status: ");
                Ext.getCmp("lblTipoAprovacaoVia").setText("Tipo Aprovação: ");

                // Locomotiva Fields
                Ext.getCmp("lblAprovadorLocomotiva").setText("Aprovador: ");
                Ext.getCmp("lblMatriculaLocomotiva").setText("Matrícula: ");
                Ext.getCmp("lblDataLocomotiva").setText("Data: ");
                Ext.getCmp("lblStatusLocomotiva").setText("Status: ");
                Ext.getCmp("lblTipoAprovacaoLocomotiva").setText("Tipo Aprovação: ");

                // Tracao Fields
                Ext.getCmp("lblAprovadorTracao").setText("Aprovador: ");
                Ext.getCmp("lblMatriculaTracao").setText("Matrícula: ");
                Ext.getCmp("lblDataTracao").setText("Data: ");
                Ext.getCmp("lblStatusTracao").setText("Status: ");
                Ext.getCmp("lblTipoAprovacaoTracao").setText("Tipo Aprovação: ");

                // Vagao Fields
                Ext.getCmp("lblAprovadorVagao").setText("Aprovador: ");
                Ext.getCmp("lblMatriculaVagao").setText("Matrícula: ");
                Ext.getCmp("lblDataVagao").setText("Data: ");
                Ext.getCmp("lblStatusVagao").setText("Status: ");
                Ext.getCmp("lblTipoAprovacaoVagao").setText("Tipo Aprovação: ");

                validarPermissoes();
            }
            else {

                var statusVia = '<%=statusVia%>';
                var statusLoco = '<%=statusLoco%>';
                var statusTrac = '<%=statusTrac%>';
                var statusVag = '<%=statusVag%>';

                if (statusVia == "Aprovado"
                && statusLoco == "Aprovado"
                && statusTrac == "Aprovado"
                && statusVag == "Aprovado") {
                    Ext.getCmp("AbaChecklist").setDisabled(false);
                }

                recusarPermissoes();

                if (somenteVisualizarCheklist == "true")
                    Ext.getCmp("btnSalvar").setDisabled(true);

                // Via Fields
                Ext.getCmp("lblAprovadorVia").setText("Aprovador: " + '<%=aprovadorVia%>');
                Ext.getCmp("lblMatriculaVia").setText("Matrícula: " + '<%=matriculaVia%>');
                Ext.getCmp("lblDataVia").setText("Data: " + '<%=dataVia%>');
                Ext.getCmp("lblStatusVia").setText("Status: " + statusVia);
                Ext.getCmp("lblTipoAprovacaoVia").setText("Tipo Aprovação: " + '<%=tipoAprVia%>');
                Ext.getCmp("txtObservacoesVia").setValue('<%=obsVia%>');
                obsVia = '<%=obsVia%>';

                if (aprovarVia == "True" && somenteVisualizarCheklist != "true") {
                    if (statusVia == "Aprovado") {
                        Ext.getCmp("btnAprovarVia").setDisabled(true);
                        Ext.getCmp("btnRecusarVia").setDisabled(true);
                        Ext.getCmp("txtObservacoesVia").setDisabled(true);
                    }
                    else {
                        if (statusVia == "Reprovado") {
                            Ext.getCmp("btnAprovarVia").setDisabled(false);
                            Ext.getCmp("btnRecusarVia").setDisabled(true);
                        }
                        else {
                            Ext.getCmp("btnAprovarVia").setDisabled(false);
                            Ext.getCmp("btnRecusarVia").setDisabled(false);
                        }
                    }
                }
                else {
                    Ext.getCmp("btnAprovarVia").setDisabled(true);
                    Ext.getCmp("btnRecusarVia").setDisabled(true);
                    Ext.getCmp("txtObservacoesVia").setDisabled(true);
                }

                // Locomotiva Fields
                Ext.getCmp("lblAprovadorLocomotiva").setText("Aprovador: " + '<%=aprovadorLoco%>');
                Ext.getCmp("lblMatriculaLocomotiva").setText("Matrícula: " + '<%=matriculaLoco%>');
                Ext.getCmp("lblDataLocomotiva").setText("Data: " + '<%=dataLoco%>');
                Ext.getCmp("lblStatusLocomotiva").setText("Status: " + statusLoco);
                Ext.getCmp("lblTipoAprovacaoLocomotiva").setText("Tipo Aprovação: " + '<%=tipoAprLoco%>');
                Ext.getCmp("txtObservacoesLocomotiva").setValue('<%=obsLoco%>');

                obsLoco = '<%=obsLoco%>';

                if (aprovarLoco == "True" && somenteVisualizarCheklist != "true") {
                    if (statusLoco == "Aprovado") {
                        Ext.getCmp("btnAprovarLocomotiva").setDisabled(true);
                        Ext.getCmp("btnRecusarLocomotiva").setDisabled(true);
                        Ext.getCmp("txtObservacoesLocomotiva").setDisabled(true);
                    }
                    else {
                        if (statusLoco == "Reprovado") {
                            Ext.getCmp("btnAprovarLocomotiva").setDisabled(false);
                            Ext.getCmp("btnRecusarLocomotiva").setDisabled(true);
                        }
                        else {
                            Ext.getCmp("btnAprovarLocomotiva").setDisabled(false);
                            Ext.getCmp("btnRecusarLocomotiva").setDisabled(false);
                        }
                    }
                }
                else {
                    Ext.getCmp("btnAprovarLocomotiva").setDisabled(true);
                    Ext.getCmp("btnRecusarLocomotiva").setDisabled(true);
                    Ext.getCmp("txtObservacoesLocomotiva").setDisabled(true);
                }


                // Tracao Fields
                Ext.getCmp("lblAprovadorTracao").setText("Aprovador: " + '<%=aprovadorTrac%>');
                Ext.getCmp("lblMatriculaTracao").setText("Matrícula: " + '<%=matriculaTrac%>');
                Ext.getCmp("lblDataTracao").setText("Data: " + '<%=dataTrac%>');
                Ext.getCmp("lblStatusTracao").setText("Status: " + statusTrac);
                Ext.getCmp("lblTipoAprovacaoTracao").setText("Tipo Aprovação: " + '<%=tipoAprTrac%>');
                Ext.getCmp("txtObservacoesTracao").setValue('<%=obsTrac%>');

                obsTracao = '<%=obsTrac%>';

                if (aprovarTracao == "True" && somenteVisualizarCheklist != "true") {
                    if (statusTrac == "Aprovado") {
                        Ext.getCmp("btnAprovarTracao").setDisabled(true);
                        Ext.getCmp("btnRecusarTracao").setDisabled(true);
                        Ext.getCmp("txtObservacoesTracao").setDisabled(true);
                    }
                    else {
                        if (statusTrac == "Reprovado") {
                            Ext.getCmp("btnAprovarTracao").setDisabled(false);
                            Ext.getCmp("btnRecusarTracao").setDisabled(true);
                        }
                        else {
                            Ext.getCmp("btnAprovarTracao").setDisabled(false);
                            Ext.getCmp("btnRecusarTracao").setDisabled(false);
                        }
                    }
                }
                else {
                    Ext.getCmp("btnAprovarTracao").setDisabled(true);
                    Ext.getCmp("btnRecusarTracao").setDisabled(true);
                    Ext.getCmp("txtObservacoesTracao").setDisabled(true);
                }

                // Vagao Fields
                Ext.getCmp("lblAprovadorVagao").setText("Aprovador: " + '<%=aprovadorVag%>');
                Ext.getCmp("lblMatriculaVagao").setText("Matrícula: " + '<%=matriculaVag%>');
                Ext.getCmp("lblDataVagao").setText("Data: " + '<%=dataVag%>');
                Ext.getCmp("lblStatusVagao").setText("Status: " + statusVag);
                Ext.getCmp("lblTipoAprovacaoVagao").setText("Tipo Aprovação: " + '<%=tipoAprVag%>');
                Ext.getCmp("txtObservacoesVagao").setValue('<%=obsVag%>');

                obsVagao = '<%=obsVag%>';

                if (aprovarVagao == "True" && somenteVisualizarCheklist != "true") {
                    if (statusVag == "Aprovado") {
                        Ext.getCmp("btnAprovarVagao").setDisabled(true);
                        Ext.getCmp("btnRecusarVagao").setDisabled(true);
                        Ext.getCmp("txtObservacoesVagao").setDisabled(true);
                    }
                    else {
                        if (statusVag == "Reprovado") {
                            Ext.getCmp("btnAprovarVagao").setDisabled(false);
                            Ext.getCmp("btnRecusarVagao").setDisabled(true);
                        }
                        else {
                            Ext.getCmp("btnAprovarVagao").setDisabled(false);
                            Ext.getCmp("btnRecusarVagao").setDisabled(false);
                        }
                    }
                }
                else {
                    Ext.getCmp("btnAprovarVagao").setDisabled(true);
                    Ext.getCmp("btnRecusarVagao").setDisabled(true);
                    Ext.getCmp("txtObservacoesVagao").setDisabled(true);
                }
            }
        }

        function recusarPermissoes() {
            if ((aprovarVia == 'False' && reprovarVia == 'False')
            && (aprovarLoco == 'False' && reprovarLoco == 'False')
            && (aprovarTracao == 'False' && reprovarTracao == 'False')
            && (aprovarVagao == 'False' && reprovarVagao == 'False')) {
                Ext.getCmp("btnSalvar").setDisabled(true);

                Ext.getCmp("btnAprovarVia").setDisabled(true);
                Ext.getCmp("btnRecusarVia").setDisabled(true);
                Ext.getCmp("txtObservacoesVia").setDisabled(true);

                Ext.getCmp("btnAprovarLocomotiva").setDisabled(true);
                Ext.getCmp("btnRecusarLocomotiva").setDisabled(true);
                Ext.getCmp("txtObservacoesLocomotiva").setDisabled(true);

                Ext.getCmp("btnAprovarTracao").setDisabled(true);
                Ext.getCmp("btnRecusarTracao").setDisabled(true);
                Ext.getCmp("txtObservacoesTracao").setDisabled(true);

                Ext.getCmp("btnAprovarVagao").setDisabled(true);
                Ext.getCmp("btnRecusarVagao").setDisabled(true);
                Ext.getCmp("txtObservacoesVagao").setDisabled(true);
            }
        }

        function validarPermissoes() {

            recusarPermissoes();

            if (aprovarVia == 'True' && reprovarVia == 'True') {
                Ext.getCmp("btnAprovarVia").setDisabled(false);
                Ext.getCmp("btnRecusarVia").setDisabled(false);
                Ext.getCmp("txtObservacoesVia").setDisabled(false);
            }
            else if (aprovarVia == 'True' && reprovarVia == 'False') {
                Ext.getCmp("btnAprovarVia").setDisabled(false);
                Ext.getCmp("btnRecusarVia").setDisabled(true);
                Ext.getCmp("txtObservacoesVia").setDisabled(false);
            }
            else if (aprovarVia == 'False' && reprovarVia == 'True') {
                Ext.getCmp("btnAprovarVia").setDisabled(true);
                Ext.getCmp("btnRecusarVia").setDisabled(false);
                Ext.getCmp("txtObservacoesVia").setDisabled(false);
            }
            else if (aprovarVia == 'False' && reprovarVia == 'False') {
                Ext.getCmp("btnAprovarVia").setDisabled(true);
                Ext.getCmp("btnRecusarVia").setDisabled(true);
                Ext.getCmp("txtObservacoesVia").setDisabled(true);
            }

            if (aprovarLoco == 'True' && reprovarLoco == 'True') {
                Ext.getCmp("btnAprovarLocomotiva").setDisabled(false);
                Ext.getCmp("btnRecusarLocomotiva").setDisabled(false);
                Ext.getCmp("txtObservacoesLocomotiva").setDisabled(false);
            }
            else if (aprovarLoco == 'True' && reprovarLoco == 'False') {
                Ext.getCmp("btnAprovarLocomotiva").setDisabled(false);
                Ext.getCmp("btnRecusarLocomotiva").setDisabled(true);
                Ext.getCmp("txtObservacoesLocomotiva").setDisabled(false);
            }
            else if (aprovarLoco == 'False' && reprovarLoco == 'True') {
                Ext.getCmp("btnAprovarLocomotiva").setDisabled(true);
                Ext.getCmp("btnRecusarLocomotiva").setDisabled(false);
                Ext.getCmp("txtObservacoesLocomotiva").setDisabled(false);
            }
            else if (aprovarLoco == 'False' && reprovarLoco == 'False') {
                Ext.getCmp("btnAprovarLocomotiva").setDisabled(true);
                Ext.getCmp("btnRecusarLocomotiva").setDisabled(true);
                Ext.getCmp("txtObservacoesLocomotiva").setDisabled(true);
            }

            if (aprovarTracao == 'True' && reprovarTracao == 'True') {
                Ext.getCmp("btnAprovarTracao").setDisabled(false);
                Ext.getCmp("btnRecusarTracao").setDisabled(false);
                Ext.getCmp("txtObservacoesTracao").setDisabled(false);
            }
            else if (aprovarTracao == 'True' && reprovarTracao == 'False') {
                Ext.getCmp("btnAprovarTracao").setDisabled(false);
                Ext.getCmp("btnRecusarTracao").setDisabled(true);
                Ext.getCmp("txtObservacoesTracao").setDisabled(false);
            }
            else if (aprovarTracao == 'False' && reprovarTracao == 'True') {
                Ext.getCmp("btnAprovarTracao").setDisabled(true);
                Ext.getCmp("btnRecusarTracao").setDisabled(false);
                Ext.getCmp("txtObservacoesTracao").setDisabled(false);
            }
            else if (aprovarTracao == 'False' && reprovarTracao == 'False') {
                Ext.getCmp("btnAprovarTracao").setDisabled(true);
                Ext.getCmp("btnRecusarTracao").setDisabled(true);
                Ext.getCmp("txtObservacoesTracao").setDisabled(true);
            }

            if (aprovarVagao == 'True' && reprovarVagao == 'True') {
                Ext.getCmp("btnAprovarVagao").setDisabled(false);
                Ext.getCmp("btnRecusarVagao").setDisabled(false);
                Ext.getCmp("txtObservacoesVagao").setDisabled(false);
            }
            else if (aprovarVagao == 'True' && reprovarVagao == 'False') {
                Ext.getCmp("btnAprovarVagao").setDisabled(false);
                Ext.getCmp("btnRecusarVagao").setDisabled(true);
                Ext.getCmp("txtObservacoesVagao").setDisabled(false);
            }
            else if (aprovarVagao == 'False' && reprovarVagao == 'True') {
                Ext.getCmp("btnAprovarVagao").setDisabled(true);
                Ext.getCmp("btnRecusarVagao").setDisabled(false);
                Ext.getCmp("txtObservacoesVagao").setDisabled(false);
            }
            else if (aprovarVagao == 'False' && reprovarVagao == 'False') {
                Ext.getCmp("btnAprovarVagao").setDisabled(true);
                Ext.getCmp("btnRecusarVagao").setDisabled(true);
                Ext.getCmp("txtObservacoesVagao").setDisabled(true);
            }
        }

        function download(idCheckListAprovado, quadro) {
            var url = '<%= Url.Action("Download","CheckListPassageiro") %>';
            url += "?idCheckListAprovado=" + idCheckListAprovado + "&quadro=" + quadro;
            window.open(url, "");
        }

        function recusar(quadro) {
            aprovacao(quadro, false);
        }

        function aprovar(quadro) {
            aprovacao(quadro, true);
        }

        function aprovacao(quadro, aprovar) {
            var data = dataAprovacao();
            var status = aprovar == true ? "Aprovado" : "Recusado";
            var tipo = "Usuário";

            if (quadro == 'via') {
                // Via Fields
                Ext.getCmp("lblAprovadorVia").setText("Aprovador: " + aprovador);
                Ext.getCmp("lblMatriculaVia").setText("Matrícula: " + matriculaAprovador);
                Ext.getCmp("lblDataVia").setText("Data: " + data);
                //console.log("data da tração: " + data);
                Ext.getCmp("lblStatusVia").setText("Status: " + status);
                Ext.getCmp("lblTipoAprovacaoVia").setText("Tipo Aprovação: " + tipo);
                Ext.getCmp("hdnAtualizadoVia").setValue("true");

                if (aprovar) {
                    Ext.getCmp("btnAprovarVia").setDisabled(true);
                    Ext.getCmp("btnRecusarVia").setDisabled(true);
                    Ext.getCmp("txtObservacoesVia").setDisabled(true);
                }
                else {
                    Ext.getCmp("btnAprovarVia").setDisabled(false);
                    Ext.getCmp("btnRecusarVia").setDisabled(true);
                }
            }

            if (quadro == 'locomotiva') {
                // Locomotiva Fields
                Ext.getCmp("lblAprovadorLocomotiva").setText("Aprovador: " + aprovador);
                Ext.getCmp("lblMatriculaLocomotiva").setText("Matrícula: " + matriculaAprovador);
                Ext.getCmp("lblDataLocomotiva").setText("Data: " + data);
                Ext.getCmp("lblStatusLocomotiva").setText("Status: " + status);
                Ext.getCmp("lblTipoAprovacaoLocomotiva").setText("Tipo Aprovação: " + tipo);
                Ext.getCmp("hdnAtualizadoLocomotiva").setValue("true");
                if (aprovar) {
                    Ext.getCmp("btnAprovarLocomotiva").setDisabled(true);
                    Ext.getCmp("btnRecusarLocomotiva").setDisabled(true);
                    Ext.getCmp("txtObservacoesLocomotiva").setDisabled(true);
                }
                else {
                    Ext.getCmp("btnAprovarLocomotiva").setDisabled(false);
                    Ext.getCmp("btnRecusarLocomotiva").setDisabled(true);
                }
            }

            if (quadro == 'tracao') {
                // Tracao Fields
                Ext.getCmp("lblAprovadorTracao").setText("Aprovador: " + aprovador);
                Ext.getCmp("lblMatriculaTracao").setText("Matrícula: " + matriculaAprovador);
                Ext.getCmp("lblDataTracao").setText("Data: " + data);
                //console.log("data da Vagão: " + data);
                Ext.getCmp("lblStatusTracao").setText("Status: " + status);
                Ext.getCmp("lblTipoAprovacaoTracao").setText("Tipo Aprovação: " + tipo);
                Ext.getCmp("hdnAtualizadoTracao").setValue("true");
                if (aprovar) {
                    Ext.getCmp("btnAprovarTracao").setDisabled(true);
                    Ext.getCmp("btnRecusarTracao").setDisabled(true);
                    Ext.getCmp("txtObservacoesTracao").setDisabled(true);
                }
                else {
                    Ext.getCmp("btnAprovarTracao").setDisabled(false);
                    Ext.getCmp("btnRecusarTracao").setDisabled(true);
                }
            }

            if (quadro == 'vagao') {
                // Vagao Fields
                Ext.getCmp("lblAprovadorVagao").setText("Aprovador: " + aprovador);
                Ext.getCmp("lblMatriculaVagao").setText("Matrícula: " + matriculaAprovador);
                Ext.getCmp("lblDataVagao").setText("Data: " + data);
                Ext.getCmp("lblStatusVagao").setText("Status: " + status);
                Ext.getCmp("lblTipoAprovacaoVagao").setText("Tipo Aprovação: " + tipo);
                Ext.getCmp("hdnAtualizadoVagao").setValue("true");
                if (aprovar) {
                    Ext.getCmp("btnAprovarVagao").setDisabled(true);
                    Ext.getCmp("btnRecusarVagao").setDisabled(true);
                    Ext.getCmp("txtObservacoesVagao").setDisabled(true);
                }
                else {
                    Ext.getCmp("btnAprovarVagao").setDisabled(false);
                    Ext.getCmp("btnRecusarVagao").setDisabled(true);
                }
            }
        }

        function dataAprovacao() {

            var currentdate = new Date();
            var datetime = currentdate.getDate() + "/"
                        + (currentdate.getMonth() + 1) + "/"
                        + currentdate.getFullYear() + " "
                        + currentdate.getHours() + ":"
                        + currentdate.getMinutes() + ":"
                        + currentdate.getSeconds();

            return datetime;
        }

        function salvarAprovacaoes() {

            var alterado = true;
            var alteradoVia = true;
            var alteradoLoco = true;
            var alteradoTracao = true;
            var alteradoVagao = true;
            var salvar = false;
            var atualizadoVia = Ext.getCmp("hdnAtualizadoVia").getRawValue();
            var atualizadoLoco = Ext.getCmp("hdnAtualizadoLocomotiva").getRawValue();
            var atualizadoTracao = Ext.getCmp("hdnAtualizadoTracao").getRawValue();
            var atualizadoVagao = Ext.getCmp("hdnAtualizadoVagao").getRawValue();

            var oVia = (Ext.getCmp("txtObservacoesVia").getValue()).length < 3000 ? Ext.getCmp("txtObservacoesVia").getValue() : "";
            var oLoco = (Ext.getCmp("txtObservacoesLocomotiva").getValue()).length < 3000 ? Ext.getCmp("txtObservacoesLocomotiva").getValue() : "";
            var oTracao = (Ext.getCmp("txtObservacoesTracao").getValue()).length < 3000 ? Ext.getCmp("txtObservacoesTracao").getValue() : "";
            var oVagao = (Ext.getCmp("txtObservacoesVagao").getValue()).length < 3000 ? Ext.getCmp("txtObservacoesVagao").getValue() : "";

            if ((idOsChecklist == 0 && atualizadoVagao == "") &&
                (idOsChecklist == 0 && atualizadoLoco == "") &&
                (idOsChecklist == 0 && atualizadoVia == "") &&
                (idOsChecklist == 0 && atualizadoTracao == "")) {
                alterado = false;
            }

            if ((obsVia == oVia && atualizadoVagao == "") &&
                (obsLoco == oLoco && atualizadoLoco == "") &&
                (obsTracao == oTracao && atualizadoVia == "") &&
                (obsVagao == oVagao && atualizadoTracao == "")) {
                alterado = false;
            }
            //Via
            if (obsVia != oVia && Ext.getCmp("btnRecusarVia").disabled == false && atualizadoVia != 'true') {
                alterado = false;
                alteradoVia = false;
            } else if (obsVia == oVia) {
                alteradoVia = false;
                if (atualizadoVia == 'true') {
                    alteradoVia = true;
                }
            }
            //Locomotiva
            if (obsLoco != oLoco && Ext.getCmp("btnRecusarLocomotiva").disabled == false && atualizadoLoco != 'true') {
                alterado = false;
                alteradoLoco = false;
            } else if (obsLoco == oLoco) {
                alteradoLoco = false;
                if (atualizadoLoco == 'true') {
                    alteradoLoco = true;
                }
            }
            //Tracao
            if (obsTracao != oTracao && Ext.getCmp("btnRecusarTracao").disabled == false && atualizadoTracao != 'true') {
                alterado = false;
                alteradoTracao = false;
            } else if (obsTracao == oTracao) {
                alteradoTracao = false;
                if (atualizadoTracao == 'true') {
                    alteradoTracao = true;
                }
            }
            //Vagao
            if (obsVagao != oVagao && Ext.getCmp("btnRecusarVagao").disabled == false && atualizadoVagao != 'true') {
                alterado = false;
                alteradoVagao = false;
            } else if (obsVagao == oVagao) {
                alteradoVagao = false;
                if (atualizadoVagao == 'true') {
                    alteradoVagao = true;
                }
            }

            if (alterado == false && alteradoVia == false && alteradoLoco == false && alteradoTracao == false && alteradoVagao == false) {
                Ext.Msg.show({
                    title: 'Aviso',
                    msg: 'Sem alterações para salvar.',
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.WARNING
                });
            } else {
                if ((idOsChecklist == 0 || idOsChecklist == null) && (idOs == 0 || idOs == null)) {

                    Ext.Msg.show({
                        title: 'Aviso',
                        msg: 'Não foi informada OS para aprovar.',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.WARNING
                    });
                }
                else {
                    Ext.Msg.show({
                        title: 'Aviso',
                        msg: 'Deseja salvar as informações alteradas?',
                        buttons: Ext.Msg.YESNO,
                        icon: Ext.MessageBox.WARNING,
                        fn: function (buttonValue) {
                            if (buttonValue == "yes") {

                                Ext.getBody().mask("Processando dados...", "x-mask-loading");
                                var status = false;
                                var obs = "";
                                var dataHora = new Date();
                                var usuario = aprovador;
                                var matricula = matriculaAprovador;

                                //if (atualizadoVia) {

                                var status = Ext.getCmp("lblStatusVia").text.replace("Status: ", "") == "Aprovado" ? true : Ext.getCmp("lblStatusVia").text.replace("Status: ", "") == "Reprovado" ? false : "";
                                var obs = Ext.getCmp("txtObservacoesVia").getValue()
                                var dataHora = Ext.getCmp("lblDataVia").text.replace("Data: ", "");
                                var usuario = Ext.getCmp("lblAprovadorVia").text.replace("Aprovador: ", "");
                                var matricula = Ext.getCmp("lblMatriculaVia").text.replace("Matrícula: ", "");
                                salvar = atualizadoVia == 'true' ? true : obsVia == null ? false : (obsVia != oVia && matricula != "") ? true : false;
                                salvaAprovacao('via', status, obs, dataHora, usuario, matricula, salvar);
                                salvar = false;
                                //}
                                //if (atualizadoLoco) {
                                var status = Ext.getCmp("lblStatusLocomotiva").text.replace("Status: ", "") == "Aprovado" ? true : Ext.getCmp("lblStatusLocomotiva").text.replace("Status: ", "") == "Reprovado" ? false : "";
                                var obs = Ext.getCmp("txtObservacoesLocomotiva").getValue();
                                var dataHora = Ext.getCmp("lblDataLocomotiva").text.replace("Data: ", "");
                                var usuario = Ext.getCmp("lblAprovadorLocomotiva").text.replace("Aprovador: ", "");
                                var matricula = Ext.getCmp("lblMatriculaLocomotiva").text.replace("Matrícula: ", "");
                                salvar = atualizadoLoco == 'true' ? true : obsLoco == null ? false : (obsLoco != oLoco && matricula != "") ? true : false;
                                salvaAprovacao('loco', status, obs, dataHora, usuario, matricula, salvar);
                                salvar = false;
                                //}
                                //if (atualizadoTracao) {
                                var status = Ext.getCmp("lblStatusTracao").text.replace("Status: ", "") == "Aprovado" ? true : Ext.getCmp("lblStatusTracao").text.replace("Status: ", "") == "Reprovado" ? false : "";
                                var obs = Ext.getCmp("txtObservacoesTracao").getValue();
                                var dataHora = Ext.getCmp("lblDataTracao").text.replace("Data: ", "");
                                var usuario = Ext.getCmp("lblAprovadorTracao").text.replace("Aprovador: ", "");
                                var matricula = Ext.getCmp("lblMatriculaTracao").text.replace("Matrícula: ", "");
                                salvar = atualizadoTracao == 'true' ? true : obsTracao == null ? false : (obsTracao != oTracao && matricula != "") ? true : false;
                                salvaAprovacao('tracao', status, obs, dataHora, usuario, matricula, salvar);
                                salvar = false;
                                //}
                                //if (atualizadoVagao) {  
                                var status = Ext.getCmp("lblStatusVagao").text.replace("Status: ", "") == "Aprovado" ? true : Ext.getCmp("lblStatusVagao").text.replace("Status: ", "") == "Reprovado" ? false : "";
                                var obs = Ext.getCmp("txtObservacoesVagao").getValue();
                                var dataHora = Ext.getCmp("lblDataVagao").text.replace("Data: ", "");
                                var usuario = Ext.getCmp("lblAprovadorVagao").text.replace("Aprovador: ", "");
                                var matricula = Ext.getCmp("lblMatriculaVagao").text.replace("Matrícula: ", "");
                                salvar = atualizadoVagao == 'true' ? true : obsVagao == null ? false : (obsVagao != oVagao && matricula != "") ? true : false;
                                salvaAprovacao('vagao', status, obs, dataHora, usuario, matricula, salvar);
                                salvar = false;
                                //}
                            } // if buttonvalue
                        } // function
                    }); // msg
                } //else 2
            } // else 1
        } // fim function

        function salvaAprovacao(tipo, status, obs, dataHora, usuario, matricula, salvar) {

            var osChecklistAprovacaoDto = {
                Salvar: salvar,
                idOsChecklist: idOsChecklist,
                idOs: idOs,
                Ida: true,
                TipoAprovacao: tipo,
                Aprovado: status,
                DataHoraAprocacao: dataHora,
                Observacao: obs,
                Usuario: usuario,
                Matricula: matricula
            };

            salvarAprovacaoChecklist(osChecklistAprovacaoDto);

            //} // else obs
        }

        function fecharJanela(refresh) {
            var conteudo_principal = parent.Ext.getCmp("conteudo-principal");
            if (refresh) {
                // Atualizar grid da tela de pesquisa             
                for (var i = 0; i < conteudo_principal.items.items.length; i++) {
                    if (conteudo_principal.items.items[i].title == "Tela  - 1421") {
                        var frame = conteudo_principal.items.items[i].getFrameDocument();
                        var botao = $(frame).find('.clsBtnPesquisar');
                        $(botao).trigger("click");
                    }
                }
            }

            // Fechar janela atual
            var activeTab = conteudo_principal.getActiveTab();
            conteudo_principal.remove(activeTab.id, true);
        }

        function salvarAprovacaoChecklist(osChecklistAprovacaoDto) {
            $.ajax({
                url: '<%= Url.Action("SalvarAprovacoes","CheckListPassageiro") %>',
                type: "POST",
                dataType: 'json',
                data: $.toJSON({ osChecklistAprovacaoDto: osChecklistAprovacaoDto }),
                contentType: "application/json; charset=utf-8",
                async: false,
                success: function (result) {

                    Ext.getBody().unmask();

                    if (result.Result) {

                        // Carrega o id do CheckList para registrar salvar as próximas aprovações
                        idOsChecklist = result.IdOsChecklist;

                        Ext.Msg.show({
                            title: 'Aviso',
                            msg: result.Message,
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.WARNING,
                            fn: function (buttonValue) {

                                if (buttonValue == "ok") {

                                    fecharJanela(true);
                                }
                            }
                        });
                    }
                    else {
                        Ext.Msg.show({
                            title: 'Aviso',
                            msg: result.Message,
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.ERROR
                        });
                    }
                },
                error: function (response) {
                    Ext.getBody().unmask();

                    Ext.Msg.show({
                        title: 'Aviso',
                        msg: retorno.Message,
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.WARNING
                    });
                }
            });
        }

        function fecharAprovacoes() {

            var atualizadoVia = Ext.getCmp("hdnAtualizadoVia").getRawValue();
            var atualizadoLoco = Ext.getCmp("hdnAtualizadoLocomotiva").getRawValue();
            var atualizadoTracao = Ext.getCmp("hdnAtualizadoTracao").getRawValue();
            var atualizadoVagao = Ext.getCmp("hdnAtualizadoVagao").getRawValue();


            if (atualizadoVia || atualizadoLoco || atualizadoTracao || atualizadoVagao) {

                Ext.Msg.show({
                    title: 'Aviso',
                    msg: 'Existem alterações não salvas, ao fechar você irá perder estas informações. Deseja fechar mesmo assim?',
                    buttons: Ext.Msg.YESNO,
                    icon: Ext.MessageBox.WARNING,
                    fn: function (buttonValue) {
                        if (buttonValue == "yes") {
                            fecharJanela(false);
                        }
                    }
                });
            }
            else {
                fecharJanela(false);
            }
        }

        // @@@@ READY JQUERY
        (function () {


            //valida exibição de botão download

            var disableBtnDownLoco = "<%=disableBtnDownLoco%>";
            var disableBtnDownVia = "<%=disableBtnDownVia%>";
            var disableBtnDownTrac = "<%=disableBtnDownTrac %>";
            var disableBtnDownVag = "<%=disableBtnDownVag %>";

            //FIM VALIDAÇÃO


            //@@@@@ INICIO @@@@@@@@ FIELD VIA  @@@@@@@@@@@@@@@@@@@@@@

            var hdnAtualizadoVia = { xtype: 'hidden', id: 'hdnAtualizadoVia', name: 'hdnAtualizadoVia' };

            var lblDataVia = { xtype: 'label', id: 'lblDataVia' };

            var frmlblDataVia = {
                layout: 'form',
                border: false,
                width: 160,
                items: [lblDataVia, hdnAtualizadoVia]
            };

            var lblAprovadorVia = { xtype: 'label', id: 'lblAprovadorVia' };
            var frmlblAprovadorVia = {
                layout: 'form',
                border: false,
                width: 300,
                items: [lblAprovadorVia]
            };

            var lblMatriculaVia = { xtype: 'label', id: 'lblMatriculaVia' };
            var frmlblMatriculaVia = {
                layout: 'form',
                border: false,
                width: 160,
                items: [lblMatriculaVia]
            };

            var LinhaVia1 = {
                layout: 'column',
                border: false,
                height: 25,
                items: [frmlblDataVia]
            };

            var LinhaVia2 = {
                layout: 'column',
                border: false,
                height: 25,
                items: [frmlblAprovadorVia, frmlblMatriculaVia]
            };

            var lblStatusVia = { xtype: 'label', id: 'lblStatusVia' };
            var lblTipoAprovacaoVia = { xtype: 'label', id: 'lblTipoAprovacaoVia' };

            var frmlblStatusVia = {
                layout: 'form',
                border: false,
                width: 300,
                items: [lblStatusVia]
            };

            var frmTipoAprovacaoVia = {
                layout: 'form',
                border: false,
                width: 170,
                items: [lblTipoAprovacaoVia]
            };

            var LinhaVia3 = {
                layout: 'column',
                border: false,
                height: 25,
                items: [frmlblStatusVia, frmTipoAprovacaoVia]
            };

            var txtObservacoesVia = new Ext.form.TextArea({
                id: 'txtObservacoesVia',
                name: 'txtObservacoesVia',
                //fieldLabel: 'Justificativa CCO (*)',
                enableKeyEvents: true,
                width: 400,
                height: 100,
                autoCreate: {
                    maxlength: '3000',
                    tag: 'textarea'
                    //rows: '6',
                    //cols: '65'
                },
                listeners: {
                    keyUp: function (field, e) {
                        if (field.getRawValue().length >= 3000) {
                            $("#txtObservacoesVia").val($("#txtObservacoesVia").val().substring(0, 3000));
                        }
                    }
                }
                //labelStyle: 'font-weight: bold;'
            });

            var frmtxtObservacoesVia = {
                layout: 'form',
                border: false,
                width: 405,
                items: [txtObservacoesVia]
            };

            var btnImportarVia = {
                xtype: 'button',
                name: 'btnImportarVia',
                id: 'btnImportarVia',
                iconCls: 'icon-down',
                disabled: disableBtnDownVia == "false" ? false : true,
                listeners: {
                    click: function (btn, e) {
                        e.preventDefault();
                        download("<%=idOsChecklistAprovacao %>", 'Via');

                    }
                }
            };

            var LinhabtnImportarVia = {
                layout: 'form',
                border: false,
                height: 30,
                items: [btnImportarVia]
            };

            var btnAprovarVia = {
                xtype: 'button',
                name: 'btnAprovarVia',
                id: 'btnAprovarVia',
                text: 'Aprovar',
                listeners: {
                    click: function (btn, e) {
                        var textoVia = Ext.getCmp("txtObservacoesVia").getValue();
                        if (textoVia.length > 3000) {
                            Ext.Msg.show({
                                title: 'Aviso',
                                msg: 'Campo observação VIA não pode ter mais de 3000 caracteres.',
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.WARNING
                            });
                            Ext.getCmp("txtObservacoesVia").setDisabled(false);
                            Ext.getCmp("btnAprovarVia").setDisabled(false);
                            Ext.getCmp("btnRecusarVia").setDisabled(false);
                            return;
                        }

                        aprovar('via');
                        //Verifica todas aprovações estão OK se OK habilita aba checklist
                        habilitaAbaCheckList();

                    }
                }
            };

            var LinhabtnAprovarVia = {
                layout: 'form',
                border: false,
                height: 30,
                items: [btnAprovarVia]
            };

            var btnRecusarVia = {
                xtype: 'button',
                name: 'btnRecusarVia',
                id: 'btnRecusarVia',
                text: 'Recusar',
                listeners: {
                    click: function (btn, e) {
                        var textoVia = Ext.getCmp("txtObservacoesVia").getValue();
                        if (textoVia.length > 3000) {
                            Ext.Msg.show({
                                title: 'Aviso',
                                msg: 'Campo observação VIA não pode ter mais de 3000 caracteres.',
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.WARNING
                            });
                            Ext.getCmp("txtObservacoesVia").setDisabled(false);
                            Ext.getCmp("btnAprovarVia").setDisabled(false);
                            Ext.getCmp("btnRecusarVia").setDisabled(false);
                            return;
                        }

                        recusar('via');
                    }
                }
            };

            var LinhabtnRecusarVia = {
                layout: 'form',
                border: false,
                height: 30,
                items: [btnRecusarVia]
            };

            var colVia = {
                layout: 'form',
                bodyStyle: 'padding-top: 20px',
                border: false,
                items: [LinhabtnImportarVia, LinhabtnAprovarVia, LinhabtnRecusarVia]
            };

            var LinhaVia4 = {
                layout: 'column',
                border: false,
                items: [frmtxtObservacoesVia, colVia]
            };

            //@@@@@ FIM @@@@@@@@ FIELD VIA  @@@@@@@@@@@@@@@@@@@@@@

            //@@@@@ INICIO @@@@@@@@ FIELD LOCOMOTIVA  @@@@@@@@@@@@@@@@@@@@@@

            var hdnAtualizadoLocomotiva = { xtype: 'hidden', id: 'hdnAtualizadoLocomotiva', name: 'hdnAtualizadoLocomotiva' };

            var lblAprovadorLocomotiva = { xtype: 'label', id: 'lblAprovadorLocomotiva' };
            var frmlblAprovadorLocomotiva = {
                layout: 'form',
                border: false,
                width: 300,
                items: [lblAprovadorLocomotiva]
            };

            var lblMatriculaLocomotiva = { xtype: 'label', id: 'lblMatriculaLocomotiva' };
            var frmlblMatriculaLocomotiva = {
                layout: 'form',
                border: false,
                width: 160,
                items: [lblMatriculaLocomotiva]
            };

            var lblDataLocomotiva = { xtype: 'label', id: 'lblDataLocomotiva' };
            var frmlblDataLocomotiva = {
                layout: 'form',
                border: false,
                width: 160,
                items: [lblDataLocomotiva, hdnAtualizadoLocomotiva]
            };

            var LinhaLocomotiva1 = {
                layout: 'column',
                border: false,
                height: 25,
                items: [frmlblDataLocomotiva]
            };

            var LinhaLocomotiva2 = {
                layout: 'column',
                border: false,
                height: 25,
                items: [frmlblAprovadorLocomotiva, frmlblMatriculaLocomotiva]
            };

            var lblStatusLocomotiva = { xtype: 'label', id: 'lblStatusLocomotiva' };
            var frmlblStatusLocomotiva = {
                layout: 'form',
                border: false,
                width: 300,
                items: [lblStatusLocomotiva]
            };

            var lblTipoAprovacaoLocomotiva = { xtype: 'label', id: 'lblTipoAprovacaoLocomotiva' };
            var frmTipoAprovacaoLocomotiva = {
                layout: 'form',
                border: false,
                width: 170,
                items: [lblTipoAprovacaoLocomotiva]
            };

            var LinhaLocomotiva3 = {
                layout: 'column',
                border: false,
                height: 25,
                items: [frmlblStatusLocomotiva, frmTipoAprovacaoLocomotiva]
            };

            var txtObservacoesLocomotiva = new Ext.form.TextArea({
                id: 'txtObservacoesLocomotiva',
                name: 'txtObservacoesLocomotiva',
                //fieldLabel: 'Justificativa CCO (*)',
                enableKeyEvents: true,
                width: 400,
                height: 100,
                autoCreate: {
                    maxlength: '3000',
                    tag: 'textarea'
                    //                    rows: '7',
                    //                    cols: '70'
                },
                listeners: {
                    keyUp: function (field, e) {
                        if (field.getRawValue().length >= 3000) {
                            $("#txtObservacoesLocomotiva").val($("#txtObservacoesLocomotiva").val().substring(0, 3000));
                        }

                    }
                }
                //labelStyle: 'font-weight: bold;'
            });

            var frmtxtObservacoesLocomotiva = {
                layout: 'form',
                border: false,
                width: 410,
                items: [txtObservacoesLocomotiva]
            };

            var btnImportarLocomotiva = {

                xtype: 'button',
                name: 'btnImportarLocomotiva',
                id: 'btnImportarLocomotiva',
                iconCls: 'icon-down',
                disabled: disableBtnDownLoco == "false" ? false : true,
                listeners: {
                    click: function (btn, e) {

                        e.preventDefault();
                        download("<%=idOsChecklistAprovacao %>", 'loc');
                    }
                }
            };

            var LinhabtnImportarLocomotiva = {
                layout: 'column',
                border: false,
                height: 30,
                items: [btnImportarLocomotiva]
            };

            var btnAprovarLocomotiva = {
                xtype: 'button',
                name: 'btnAprovarLocomotiva',
                id: 'btnAprovarLocomotiva',
                text: 'Aprovar',
                listeners: {
                    click: function (btn, e) {
                        var textoLoco = Ext.getCmp("txtObservacoesLocomotiva").getValue();
                        if (textoLoco.length > 3000) {
                            Ext.Msg.show({
                                title: 'Aviso',
                                msg: 'Campo observação LOCOMOTIVA não pode ter mais de 3000 caracteres.',
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.WARNING
                            });
                            Ext.getCmp("txtObservacoesLocomotiva").setDisabled(false);
                            Ext.getCmp("btnAprovarLocomotiva").setDisabled(false);
                            Ext.getCmp("btnRecusarLocomotiva").setDisabled(false);
                            return;
                        }

                        aprovar('locomotiva');
                        //Verifica todas aprovações estão OK se OK habilita aba checklist
                        habilitaAbaCheckList();
                    }
                }
            };

            var LinhabtnAprovarLocomotiva = {
                layout: 'column',
                border: false,
                height: 30,
                items: [btnAprovarLocomotiva]
            };

            var btnRecusarLocomotiva = {
                xtype: 'button',
                name: 'btnRecusarLocomotiva',
                id: 'btnRecusarLocomotiva',
                text: 'Recusar',
                listeners: {
                    click: function (btn, e) {
                        var textoLoco = Ext.getCmp("txtObservacoesLocomotiva").getValue();
                        if (textoLoco.length > 3000) {
                            Ext.Msg.show({
                                title: 'Aviso',
                                msg: 'Campo observação LOCOMOTIVA não pode ter mais de 3000 caracteres.',
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.WARNING
                            });
                            Ext.getCmp("txtObservacoesLocomotiva").setDisabled(false);
                            Ext.getCmp("btnAprovarLocomotiva").setDisabled(false);
                            Ext.getCmp("btnRecusarLocomotiva").setDisabled(false);
                            return;
                        }

                        recusar('locomotiva');
                    }
                }
            };

            var LinhabtnRecusarLocomotiva = {
                layout: 'column',
                border: false,
                height: 30,
                items: [btnRecusarLocomotiva]
            };

            var colLocomotiva = {
                layout: 'form',
                bodyStyle: 'padding-top: 20px',
                border: false,
                items: [LinhabtnImportarLocomotiva, LinhabtnAprovarLocomotiva, LinhabtnRecusarLocomotiva]
            };

            var LinhaLocomotiva4 = {
                layout: 'column',
                border: false,
                items: [frmtxtObservacoesLocomotiva, colLocomotiva]
            };

            //@@@@@ FIM @@@@@@@@ FIELD LOCOMOTIVA  @@@@@@@@@@@@@@@@@@@@@@

            //@@@@@ INICIO @@@@@@@@ FIELD TRAÇÃO  @@@@@@@@@@@@@@@@@@@@@@
            var hdnAtualizadoTracao = { xtype: 'hidden', id: 'hdnAtualizadoTracao', name: 'hdnAtualizadoTracao' };

            var lblDataTracao = { xtype: 'label', id: 'lblDataTracao' };
            var frmlblDataTracao = {
                layout: 'form',
                border: false,
                width: 160,
                items: [lblDataTracao, hdnAtualizadoTracao]
            };

            var lblAprovadorTracao = { xtype: 'label', id: 'lblAprovadorTracao' };
            var frmlblAprovadorTracao = {
                layout: 'form',
                border: false,
                width: 300,
                items: [lblAprovadorTracao]
            };

            var lblMatriculaTracao = { xtype: 'label', id: 'lblMatriculaTracao' };
            var frmlblMatriculaTracao = {
                layout: 'form',
                border: false,
                width: 160,
                items: [lblMatriculaTracao]
            };

            var LinhaTracao1 = {
                layout: 'column',
                border: false,
                height: 25,
                items: [frmlblDataTracao]
            };

            var LinhaTracao2 = {
                layout: 'column',
                border: false,
                height: 25,
                items: [frmlblAprovadorTracao, frmlblMatriculaTracao]
            };

            var lblStatusTracao = { xtype: 'label', id: 'lblStatusTracao' };
            var frmlblStatusTracao = {
                layout: 'form',
                border: false,
                width: 300,
                items: [lblStatusTracao]
            };

            var lblTipoAprovacaoTracao = { xtype: 'label', id: 'lblTipoAprovacaoTracao' };
            var frmTipoAprovacaoTracao = {
                layout: 'form',
                border: false,
                width: 170,
                items: [lblTipoAprovacaoTracao]
            };

            var LinhaTracao3 = {
                layout: 'column',
                border: false,
                height: 25,
                items: [frmlblStatusTracao, frmTipoAprovacaoTracao]
            };

            var txtObservacoesTracao = new Ext.form.TextArea({
                id: 'txtObservacoesTracao',
                name: 'txtObservacoesTracao',
                //fieldLabel: 'Justificativa CCO (*)',
                enableKeyEvents: true,
                width: 400,
                height: 100,
                autoCreate: {
                    maxlength: '3000',
                    tag: 'textarea'
                    //                    rows: '7',
                    //                    cols: '70'
                },
                listeners: {
                    keyUp: function (field, e) {
                        if (field.getRawValue().length >= 3000) {
                            $("#txtObservacoesTracao").val($("#txtObservacoesTracao").val().substring(0, 3000));
                        }

                    }
                }
                //labelStyle: 'font-weight: bold;'
            });

            var frmtxtObservacoesTracao = {
                layout: 'form',
                border: false,
                width: 410,
                items: [txtObservacoesTracao]
            };

            var btnImportarTracao = {
                xtype: 'button',
                name: 'btnImportarTracao',
                id: 'btnImportarTracao',
                iconCls: 'icon-down',
                disabled: disableBtnDownTrac == "false" ? false : true,
                listeners: {
                    click: function (btn, e) {
                        e.preventDefault();
                        download("<%=idOsChecklistAprovacao %>", 'tra');
                    }
                }
            };

            var LinhabtnImportarTracao = {
                layout: 'form',
                border: false,
                height: 30,
                items: [btnImportarTracao]
            };

            var btnAprovarTracao = {
                xtype: 'button',
                name: 'btnAprovarTracao',
                id: 'btnAprovarTracao',
                text: 'Aprovar',
                listeners: {
                    click: function (btn, e) {
                        var textoTracao = Ext.getCmp("txtObservacoesTracao").getValue();
                        if (textoTracao.length > 3000) {
                            Ext.Msg.show({
                                title: 'Aviso',
                                msg: 'Campo observação TRAÇÃO não pode ter mais de 3000 caracteres.',
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.WARNING
                            });
                            Ext.getCmp("txtObservacoesTracao").setDisabled(false);
                            Ext.getCmp("btnAprovarTracao").setDisabled(false);
                            Ext.getCmp("btnRecusarTracao").setDisabled(false);
                            return;
                        }

                        aprovar('tracao');
                        //Verifica todas aprovações estão OK se OK habilita aba checklist
                        habilitaAbaCheckList();
                    }
                }
            };

            var LinhabtnAprovarTracao = {
                layout: 'form',
                border: false,
                height: 30,
                items: [btnAprovarTracao]
            };

            var btnRecusarTracao = {
                xtype: 'button',
                name: 'btnRecusarTracao',
                id: 'btnRecusarTracao',
                text: 'Recusar',
                listeners: {
                    click: function (btn, e) {
                        var textoTracao = Ext.getCmp("txtObservacoesTracao").getValue();
                        if (textoTracao.length > 3000) {
                            Ext.Msg.show({
                                title: 'Aviso',
                                msg: 'Campo observação TRAÇÃO não pode ter mais de 3000 caracteres.',
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.WARNING
                            });
                            Ext.getCmp("txtObservacoesTracao").setDisabled(false);
                            Ext.getCmp("btnAprovarTracao").setDisabled(false);
                            Ext.getCmp("btnRecusarTracao").setDisabled(false);
                            return;
                        }

                        recusar('tracao');
                    }
                }
            };

            var LinhabtnRecusarTracao = {
                layout: 'form',
                border: false,
                height: 30,
                items: [btnRecusarTracao]
            };

            var colTracao = {
                layout: 'form',
                border: false,
                bodyStyle: 'padding-top: 20px',
                items: [LinhabtnImportarTracao, LinhabtnAprovarTracao, LinhabtnRecusarTracao]
            };

            var LinhaTracao4 = {
                layout: 'column',
                border: false,
                items: [frmtxtObservacoesTracao, colTracao]
            };

            //@@@@@ FIM @@@@@@@@ FIELD TRAÇÃO  @@@@@@@@@@@@@@@@@@@@@@

            //@@@@@ INICIO @@@@@@@@ FIELD VAGÃO  @@@@@@@@@@@@@@@@@@@@@@
            var hdnAtualizadoVagao = { xtype: 'hidden', id: 'hdnAtualizadoVagao', name: 'hdnAtualizadoVagao' };

            var lblDataVagao = { xtype: 'label', id: 'lblDataVagao' };
            var frmlblDataVagao = {
                layout: 'form',
                border: false,
                width: 160,
                items: [lblDataVagao, hdnAtualizadoVagao]
            };

            var lblAprovadorVagao = { xtype: 'label', id: 'lblAprovadorVagao' };
            var frmlblAprovadorVagao = {
                layout: 'form',
                border: false,
                width: 300,
                items: [lblAprovadorVagao]
            };

            var lblMatriculaVagao = { xtype: 'label', id: 'lblMatriculaVagao' };
            var frmlblMatriculaVagao = {
                layout: 'form',
                border: false,
                width: 160,
                items: [lblMatriculaVagao]
            };

            var LinhaVagao1 = {
                layout: 'column',
                border: false,
                height: 25,
                items: [frmlblDataVagao]
            };

            var LinhaVagao2 = {
                layout: 'column',
                border: false,
                height: 25,
                items: [frmlblAprovadorVagao, frmlblMatriculaVagao]
            };

            var lblStatusVagao = { xtype: 'label', id: 'lblStatusVagao' };
            var frmlblStatusVagao = {
                layout: 'form',
                border: false,
                width: 300,
                items: [lblStatusVagao]
            };

            var lblTipoAprovacaoVagao = { xtype: 'label', id: 'lblTipoAprovacaoVagao' };
            var frmTipoAprovacaoVagao = {
                layout: 'form',
                border: false,
                width: 170,
                items: [lblTipoAprovacaoVagao]
            };

            var LinhaVagao3 = {
                layout: 'column',
                border: false,
                height: 25,
                items: [frmlblStatusVagao, frmTipoAprovacaoVagao]
            };

            var txtObservacoesVagao = new Ext.form.TextArea({
                id: 'txtObservacoesVagao',
                name: 'txtObservacoesVagao',
                //fieldLabel: 'Justificativa CCO (*)',
                enableKeyEvents: true,
                width: 400,
                height: 100,
                autoCreate: {
                    maxlength: '3000',
                    tag: 'textarea'
                    //                    rows: '7',
                    //                    cols: '70'
                },
                listeners: {
                    keyUp: function (field, e) {
                        if (field.getRawValue().length >= 3000) {
                            $("#txtObservacoesVagao").val($("#txtObservacoesVagao").val().substring(0, 3000));
                        }

                    }
                }
                //labelStyle: 'font-weight: bold;'
            });

            var frmtxtObservacoesVagao = {
                layout: 'form',
                border: false,
                width: 410,
                items: [txtObservacoesVagao]
            };

            var btnImportarVagao = {
                xtype: 'button',
                name: 'btnImportarVagao',
                id: 'btnImportarVagao',
                iconCls: 'icon-down',
                disabled: disableBtnDownVag == "false" ? false : true,
                listeners: {
                    click: function (btn, e) {
                        e.preventDefault();
                        download("<%=idOsChecklistAprovacao %>", 'vag');
                    }
                }
            };

            var LinhabtnImportarVagao = {
                layout: 'form',
                border: false,
                height: 30,
                items: [btnImportarVagao]
            };


            var btnAprovarVagao = {
                xtype: 'button',
                name: 'btnAprovarVagao',
                id: 'btnAprovarVagao',
                text: 'Aprovar',
                listeners: {
                    click: function (btn, e) {
                        var textoVagao = Ext.getCmp("txtObservacoesVagao").getValue();
                        if (textoVagao.length > 3000) {
                            Ext.Msg.show({
                                title: 'Aviso',
                                msg: 'Campo observação VAGÃO não pode ter mais de 3000 caracteres.',
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.WARNING
                            });
                            Ext.getCmp("txtObservacoesVagao").setDisabled(false);
                            Ext.getCmp("btnAprovarVagao").setDisabled(false);
                            Ext.getCmp("btnRecusarVagao").setDisabled(false);
                            return;
                        }

                        aprovar('vagao');
                        //Verifica todas aprovações estão OK se OK habilita aba checklist
                        habilitaAbaCheckList();
                    }
                }
            };

            var LinhabtnAprovarVagao = {
                layout: 'form',
                border: false,
                height: 30,
                items: [btnAprovarVagao]
            };

            var btnRecusarVagao = {
                xtype: 'button',
                name: 'btnRecusarVagao',
                id: 'btnRecusarVagao',
                text: 'Recusar',
                listeners: {
                    click: function (btn, e) {
                        var textoVagao = Ext.getCmp("txtObservacoesVagao").getValue();

                        if (textoVagao.length > 3000) {
                            Ext.Msg.show({
                                title: 'Aviso',
                                msg: 'Campo observação VAGÃO não pode ter mais de 3000 caracteres.',
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.WARNING
                            });
                            Ext.getCmp("txtObservacoesVagao").setDisabled(false);
                            Ext.getCmp("btnAprovarVagao").setDisabled(false);
                            Ext.getCmp("btnRecusarVagao").setDisabled(false);
                            return;
                        }

                        recusar('vagao');
                    }
                }
            };

            var LinhabtnRecusarVagao = {
                layout: 'form',
                border: false,
                height: 30,
                items: [btnRecusarVagao]
            };

            var colVagao = {
                layout: 'form',
                border: false,
                bodyStyle: 'padding-top: 20px',
                items: [LinhabtnImportarVagao, LinhabtnAprovarVagao, LinhabtnRecusarVagao]
            };

            var LinhaVagao4 = {
                layout: 'column',
                border: false,
                items: [frmtxtObservacoesVagao, colVagao]
            };

            //@@@@@ FIM @@@@@@@@ FIELD VAGÃO  @@@@@@@@@@@@@@@@@@@@@@



            var fieldsetVia = {
                xtype: 'fieldset',
                title: 'Via',
                width: 500,
                height: 225,
                items: [
                                                    LinhaVia1,
                                                    LinhaVia2,
                                                    LinhaVia3,
                                                    LinhaVia4
                                                ]
            };

            var fieldsetLocomotiva = {
                xtype: 'fieldset',
                title: 'Locomotiva',
                width: 500,
                height: 225,
                items: [
                                                    LinhaLocomotiva1,
                                                    LinhaLocomotiva2,
                                                    LinhaLocomotiva3,
                                                    LinhaLocomotiva4
                                                ]
            };

            var fieldsetTracao = {
                xtype: 'fieldset',
                title: 'Tração',
                width: 500,
                height: 225,
                items: [
                                                    LinhaTracao1,
                                                    LinhaTracao2,
                                                    LinhaTracao3,
                                                    LinhaTracao4
                                            ]
            };

            var fieldsetVagao = {
                xtype: 'fieldset',
                title: 'Vagão',
                width: 500,
                height: 225,
                items: [
                                                    LinhaVagao1,
                                                    LinhaVagao2,
                                                    LinhaVagao3,
                                                    LinhaVagao4
                                            ]
            };

            var formVia = {
                layout: 'form',
                border: false,
                bodyStyle: 'padding: 5px',
                autoWidth: true,
                items: [fieldsetVia]
            };

            var formTracao = {
                layout: 'form',
                border: false,
                bodyStyle: 'padding: 5px',
                autoWidth: true,
                items: [fieldsetTracao]
            };

            var column1 = {
                layout: 'column',
                border: false,
                autoWidth: true,
                items: [formVia, formTracao]
            };


            var formLocomotiva = {
                layout: 'form',
                border: false,
                bodyStyle: 'padding: 5px',
                autoWidth: true,
                items: [fieldsetLocomotiva]
            };

            var formVagao = {
                layout: 'form',
                border: false,
                bodyStyle: 'padding: 5px',
                autoWidth: true,
                items: [fieldsetVagao]
            };


            var column2 = {
                layout: 'column',
                border: false,
                //bodyStyle: 'padding: 5px',
                autoWidth: true,
                items: [formLocomotiva, formVagao]
            };

            //Linha 2 BOTÔES
            var btnSalvar = {
                xtype: 'button',
                name: 'btnSalvar',
                id: 'btnSalvar',
                text: 'Salvar',
                listeners: {
                    click: function () {

                        salvarAprovacaoes();
                    }
                }
            };

            var frmbtnSalvar = {
                layout: 'form',
                border: false,
                width: 50,
                items: [btnSalvar]
            };

            var LinhabtnSalvar = {
                layout: 'column',
                border: false,
                items: [frmbtnSalvar]
            };

            var btnFechar = {
                xtype: 'button',
                name: 'btnFechar',
                id: 'btnFechar',
                text: 'Fechar',
                listeners: {
                    click: function () {
                        fecharAprovacoes();
                    }
                }
            };

            var frmbtnFechar = {
                layout: 'form',
                border: false,
                width: 100,
                items: [btnFechar]
            };

            var LinhabtnFechar = {
                layout: 'column',
                border: false,
                items: [frmbtnFechar]
            };

            var LinhaBotoesRodape = {
                id: 'linhaBotoesRodape',
                layout: 'column',
                border: false,
                autowidth: true,
                items: [LinhabtnSalvar, LinhabtnFechar],
                bodyStyle: 'padding-bottom: 10px',
                width: 800
            };

            var painelGeralAprovacoes = new Ext.form.FormPanel({
                id: 'painelGeralAprovacoes',
                layout: 'form',
                //labelAlign: 'top',
                standardSubmit: true,
                border: false,
                autoHeight: true,
                width: 1025,
                buttonAlign: "center",
                title: "",
                bodyStyle: 'padding: 1px',
                items: [
                            column1,
                            column2
                        ],
                buttons:
                        [
                            btnSalvar,
                            btnFechar
                        ],
                listeners:
                                    {
                                        afterrender: function (index) {
                                            armazenaObservacoes();
                                        }
                                    }
            });

            AbaAprovacoes.getForm = function () {

                return painelGeralAprovacoes;

            };

        } ());

        Ext.onReady(function () {
            carregarDadosAprovacoes();

        });

        geral.AbaAprovacoes = AbaAprovacoes;
        return geral;
    } (window.geral || {}));

    //INICIO JAVASCRIPT
    //Function Valida se os quatro quadros foram aprovados para liberar aba checklist
    function habilitaAbaCheckList() {
        if (Ext.getCmp("lblAprovadorVia").text == "Aprovador: " && Ext.getCmp("lblAprovadorLocomotiva").text == "Aprovador: " &&
                    Ext.getCmp("lblAprovadorTracao").text == "Aprovador: " && Ext.getCmp("lblAprovadorVagao").text == "Aprovador: ") {
            Ext.getCmp("AbaChecklist").setDisabled(false);
        } else {
            Ext.getCmp("AbaChecklist").setDisabled(true);

        }
    }

    //FIM JAVASCRIPT   

</script>
