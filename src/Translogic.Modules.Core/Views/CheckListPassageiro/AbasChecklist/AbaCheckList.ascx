﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%
    var permissoes = ViewData["Permissoes"] != null ? ViewData["Permissoes"] as Translogic.Modules.Core.Domain.Model.Dto.ChecklistPassageiro.OsChecklistPermissaoDto : new Translogic.Modules.Core.Domain.Model.Dto.ChecklistPassageiro.OsChecklistPermissaoDto();

    var ComentAdicionais = ViewData["ComentAdicionais"] != null ? (ViewData["ComentAdicionais"] as string).Replace("'", "\\'").Replace("\n", "\\n").Replace("\r", "\\r") : "";
    %>
<style type="text/css">
    
</style>
<script type="text/javascript">

    var SalvarC = '<%=permissoes.SalvarChecklist %>';
    var ConcluirC = '<%=permissoes.ConcluirChecklist %>'; 

    var Usuario = '<%= ViewData["Aprovador"] %>';
    var Matricula = '<%= ViewData["MatriculaAprovador"] %>';

    var idOs = '<%= ViewData["idOs"] %>';
    var idOsCheckList = '<%= ViewData["idOsChecklist"] %>';
    var numOs = '<%= ViewData["numOs"] %>'; 
    var idOsChecklistPassageiro = '<%= ViewData["idOsChecklistPassageiro"] %>';
    //CCO Circulação
    var CcoSupervisorNome = '<%= ViewData["CcoSupervisorNome"] %>';
    var CcoSupervisorMatricula = '<%= ViewData["CcoSupervisorMatricula"] %>';
    var CtrSupervisorNome = '<%= ViewData["CtrSupervisorNome"] %>';
    var CtrSupervisorMatricula = '<%= ViewData["CtrSupervisorMatricula"] %>';
    //Equipagem
    var MaqEquipSupervisorNome = '<%= ViewData["MaqEquipSupervisorNome"] %>';
    var MaqEquipSupervisorMatricula = '<%= ViewData["MaqEquipSupervisorMatricula"] %>';
    var AuxEquipSupervisorNome = '<%= ViewData["AuxEquipSupervisorNome"] %>';
    var AuxEquipSupervisorMatricula = '<%= ViewData["AuxEquipSupervisorMatricula"] %>';
    var EquipSupervisorNome = '<%= ViewData["EquipSupervisorNome"] %>';
    var EquipSupervisorMatricula = '<%= ViewData["EquipSupervisorMatricula"] %>';
    
    //CheckList
    var Resposta1 = '<%= ViewData["Resposta1"] %>';
    var Resposta2 = '<%= ViewData["Resposta2"] %>';
    var Resposta3 = '<%= ViewData["Resposta3"] %>';
    var Resposta4 = '<%= ViewData["Resposta4"] %>';
    var Resposta5NumCruzamentos = '<%= ViewData["Resposta5NumCruzamentos"] %>';
    var Resposta5Supervisor = '<%= ViewData["Resposta5Supervisor"] %>';
    var Resposta5Matricula = '<%= ViewData["Resposta5Matricula"] %>';
    var Resposta6TotalCruzamentos = '<%= ViewData["Resposta6TotalCruzamentos"] %>';
    var Resposta7QtdePararam = '<%= ViewData["Resposta7QtdePararam"] %>';

    //Vagões
    var QtdeVagao = '<%= ViewData["QtdeVagao"] %>';
    var Tb = '<%= ViewData["Tb"] %>';

    //Locomotivas
    //(1)
    var Locomotiva1 = '<%= ViewData["Locomotiva1"] %>';
    var Diesel1 = '<%= ViewData["Diesel1"] %>';
    var DataVencimento1 = '<%= ViewData["DataVencimento1"] %>';
    var Cativas1 = '<%= ViewData["Cativas1"] %>';
    var Observacao1 = '<%= ViewData["Observacao1"] %>';
    //(2)
    var Locomotiva2 = '<%= ViewData["Locomotiva2"] %>';
    var Diesel2 = '<%= ViewData["Diesel2"] %>';
    var DataVencimento2 = '<%= ViewData["DataVencimento2"] %>';
    var Cativas2 = '<%= ViewData["Cativas2"] %>';
    var Observacao2 = '<%= ViewData["Observacao2"] %>';
    //(3)
    var Locomotiva3 = '<%= ViewData["Locomotiva3"] %>';
    var Diesel3 = '<%= ViewData["Diesel3"] %>';
    var DataVencimento3 = '<%= ViewData["DataVencimento3"] %>';
    var Cativas3 = '<%= ViewData["Cativas3"] %>';
    var Observacao3  = '<%= ViewData["Observacao3"] %>';
     //(4)
    var Locomotiva4 = '<%= ViewData["Locomotiva4"] %>';
    var Diesel4 = '<%= ViewData["Diesel4"] %>';
    var DataVencimento4 = '<%= ViewData["DataVencimento4"] %>';
    var Cativas4 = '<%= ViewData["Cativas4"] %>';
    var Observacao4  = '<%= ViewData["Observacao4"] %>';
   
    //Previsão
    var HoraChegada = '<%= ViewData["HoraChegada"] %>';
    var DataPrevisto = '<%= ViewData["DataPrevisto"] %>';
    var HoraPrevisto = '<%= ViewData["HoraPrevisto"] %>';
    var DataReal = '<%= ViewData["DataReal"] %>';
    var HoraReal = '<%= ViewData["HoraReal"] %>';

    var DataChegadaReal = '<%= ViewData["DataChegadaReal"] %>';
    var HoraChegadaReal = '<%= ViewData["HoraChegadaReal"] %>';

    var objTela;
    var salvou = false;

    var visualizar = '<%= ViewData["SomenteVisualizarCheklist"] %>';

    var idOsChecklist = '<%= ViewData["idOsChecklist"] %>';

    var idOsPassageiro = '<%= ViewData["idOsPassageiro"] %>';



    window.geral = (function (geral) {

        var AbaCheckList = geral.AbaCheckList || {};

        // @@@@ READY JQUERY
        (function () {

            //@@@@@ INICIO @@@@@@@@ ABA CHECKLIST  @@@@@@@@@@@@@@@@@@@@@@ 

            //INICIO FIELD CCO CIRCULÃÇÃO

            //INICIO LINHA 1
            var txtSupervisorCCO = {
                xtype: 'textfield',
                name: 'txtSupervisorCCO',
                id: 'txtSupervisorCCO',
                fieldLabel: 'Supervisor',
                //maskRe: /[a-zA-Z0-9]/,
                autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '100' },
                width: 130,
                value: CcoSupervisorNome,
                disabled: (visualizar == "sim" ? true : false)
            };

            var frmtxtSupervisorCCO = {
                layout: 'form',
                border: false,
                autowidth: true,
                items: [txtSupervisorCCO]
            };

            var txtMatriculaSupervisorCCO = {
                xtype: 'textfield',
                name: 'txtMatriculaSupervisorCCO',
                id: 'txtMatriculaSupervisorCCO',
                fieldLabel: 'Matricula',
                //maskRe: /[a-zA-Z0-9]/,
                autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '12' },
                //maskRe: /[0-9]/,
                ////style: 'text-transform: uppercase',
                width: 90,
                maxlength:12,
                value: CcoSupervisorMatricula,
                disabled: (visualizar == "sim" ? true : false)
            };

            var frmtxtMatriculaSupervisorCCO = {
                id: 'frmtxtMatriculaSupervisorCCO',
                layout: 'form',
                border: false,
                autowidth: true,
                items: [txtMatriculaSupervisorCCO]
            };

            //FIM LINHA 1

            //INICIO LINHA 2

            var txtControlador = {
                xtype: 'textfield',
                name: 'txtControlador',
                id: 'txtControlador',
                fieldLabel: 'Controlador',
                //maskRe: /[a-zA-Z0-9]/,
                autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '100' },
                width: 130,
                value: CtrSupervisorNome,
                disabled: (visualizar == "sim" ? true : false)
            };

            var frmtxtControlador = {
                layout: 'form',
                border: false,
                autowidth: true,
                items: [txtControlador]
            };

            var txtMatriculaControlador = {
                xtype: 'textfield',
                name: 'txtMatriculaControlador',
                id: 'txtMatriculaControlador',
                fieldLabel: 'Matricula',
                //maskRe: /[a-zA-Z0-9]/,
                autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '12' },
                //maskRe: /[0-9]/,
                ////style: 'text-transform: uppercase',
                width: 90,
                value: CtrSupervisorMatricula,
                disabled: (visualizar == "sim" ? true : false)
            };

            var frmtxtMatriculaControlador = {
                id: 'frmtxtMatriculaControlador',
                layout: 'form',
                border: false,
                autowidth: true,
                items: [txtMatriculaControlador]
            };

            //FIM LINHA 2

            var camposFieldCCOCirculacao = {
                layout: 'column',
                border: false,
                autowidth: true,
                items: [

                            {
                                layout: 'form',
                                width: 200,
                                labelAlign: 'left',
                                labelWidth: 60,
                                border: false,
                                items:
                                [
                                    frmtxtSupervisorCCO,
                                    frmtxtControlador
                                ]
                            },
                             {
                                 layout: 'form',
                                 width: 150,
                                 labelAlign: 'left',
                                 labelWidth: 45,
                                 border: false,
                                 items:
                                [
                                    frmtxtMatriculaSupervisorCCO,
                                    frmtxtMatriculaControlador
                                ]
                             }
                        ]
            };

            var fieldsetCCOCirculacao = {
                xtype: 'fieldset',
                title: 'CCO Circulação',
                width: 400,
                autoHeight: true,
                bodyStyle: 'padding-top:10px',
                items: [camposFieldCCOCirculacao]
            };

            //FIM FIELD CCO CIRCULÃÇÃO

            //INICIO FIELD EQUIPAGEM
            //INICIO LINHA 1
            var txtMaquinista = {
                xtype: 'textfield',
                name: 'txtMaquinista',
                id: 'txtMaquinista',
                fieldLabel: 'Maquinista',
                //maskRe: /[a-zA-Z0-9]/,
                autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '100' },
                width: 130,
                value: MaqEquipSupervisorNome,
                disabled: (visualizar == "sim" ? true : false)
            };

            var frmtxtMaquinista = {
                layout: 'form',
                border: false,
                autowidth: true,
                items: [txtMaquinista]
            };

            var txtMatriculaMaquinista = {
                xtype: 'textfield',
                name: 'txtMatriculaMaquinista',
                id: 'txtMatriculaMaquinista',
                fieldLabel: 'Matricula',
                //maskRe: /[a-zA-Z0-9]/,
                autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '12' },
                //maskRe: /[0-9]/,
                ////style: 'text-transform: uppercase',
                width: 90,
                value: MaqEquipSupervisorMatricula,
                disabled: (visualizar == "sim" ? true : false)
            };

            var frmtxtMatriculaMaquinista = {
                id: 'frmtxtMatriculaMaquinista',
                layout: 'form',
                border: false,
                autowidth: true,
                items: [txtMatriculaMaquinista]
            };

            //FIM LINHA 1

            //INICIO LINHA 2

            var txtAuxiliar = {
                xtype: 'textfield',
                name: 'txtAuxiliar',
                id: 'txtAuxiliar',
                fieldLabel: 'Auxiliar',
                //maskRe: /[a-zA-Z0-9]/,
                autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '100' },
                width: 130,
                value: AuxEquipSupervisorNome,
                disabled: (visualizar == "sim" ? true : false)
            };

            var frmtxtAuxiliar = {
                layout: 'form',
                border: false,
                autowidth: true,
                items: [txtAuxiliar]
            };

            var txtMatriculaAuxiliar = {
                xtype: 'textfield',
                name: 'txtMatriculaAuxiliar',
                id: 'txtMatriculaAuxiliar',
                fieldLabel: 'Matricula',
                //maskRe: /[a-zA-Z0-9]/,
                autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '12' },
                //maskRe: /[0-9]/,
                ////style: 'text-transform: uppercase',
                width: 90,
                value: AuxEquipSupervisorMatricula,
                disabled: (visualizar == "sim" ? true : false)
            };

            var frmtxtMatriculaAuxiliar = {
                id: 'frmtxtMatriculaAuxiliar',
                layout: 'form',
                border: false,
                autowidth: true,
                items: [txtMatriculaAuxiliar]
            };

            //FIM LINHA 2

            //INICIO LINHA 3

            var txtSupervisorEquipagem = {
                xtype: 'textfield',
                name: 'txtSupervisorEquipagem',
                id: 'txtSupervisorEquipagem',
                fieldLabel: 'Supervisor',
                //maskRe: /[a-zA-Z0-9]/,
                autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '100' },
                width: 130,
                value: EquipSupervisorNome,
                disabled: (visualizar == "sim" ? true : false)
            };

            var frmtxtSupervisorEquipagem = {
                layout: 'form',
                border: false,
                autowidth: true,
                items: [txtSupervisorEquipagem]
            };

            var txtMatriculaSupervisorEquipagem = {
                xtype: 'textfield',
                name: 'txtMatriculaSupervisorEquipagem',
                id: 'txtMatriculaSupervisorEquipagem',
                fieldLabel: 'Matricula',
                //maskRe: /[a-zA-Z0-9]/,
                autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '12' },
                //maskRe: /[0-9]/,
                ////style: 'text-transform: uppercase',
                width: 90,
                value: EquipSupervisorMatricula,
                disabled: (visualizar == "sim" ? true : false)
            };

            var frmtxtMatriculaSupervisorEquipagem = {
                id: 'frmtxtMatriculaSupervisor',
                layout: 'form',
                border: false,
                autowidth: true,
                items: [txtMatriculaSupervisorEquipagem]
            };

            //FIM LINHA 3

            var txtComentAdicionais = new Ext.form.TextArea({
                id: 'txtComentAdicionais',
                name: 'txtComentAdicionais',
                //fieldLabel: 'Observações',
                enableKeyEvents: true,
                width: 560,
                height: 40,
                autoCreate: {
                    maxlength: '1000',
                    tag: 'textarea'
                    //rows: '6',
                    //cols: '65'
                },
                listeners: {
                    keyUp: function (field, e) {
                        if (field.getRawValue().length >= 1000) {
                            $("#txtComentAdicionais").val($("#txtComentAdicionais").val().substring(0, 1000));
                        }
                    }
                },
                value: '<%= ComentAdicionais %>',
                disabled: (visualizar == "sim" ? true : false)
            });

            var frmtxtComentAdicionais = {
                layout: 'form',
                border: false,
                labelAlign: 'top',
                autowidth: true,
                items: [txtComentAdicionais]
            };

            var fieldsetComentAdicionais = {
                xtype: 'fieldset',
                title: 'Comentários Adicionais',
                width: 600,
                autoHeight: true,
                items: [
                            frmtxtComentAdicionais
                        ]
            };

            var camposFieldEquipagem = {
                layout: 'column',
                border: false,
                autowidth: true,
                items: [

                            {
                                layout: 'form',
                                width: 200,
                                labelAlign: 'left',
                                labelWidth: 60,
                                border: false,
                                items:
                                [
                                    frmtxtMaquinista,
                                    frmtxtAuxiliar,
                                    frmtxtSupervisorEquipagem
                                ]
                            },
                             {
                                 layout: 'form',
                                 width: 150,
                                 labelAlign: 'left',
                                 labelWidth: 45,
                                 border: false,
                                 items:
                                [
                                    frmtxtMatriculaMaquinista,
                                    frmtxtMatriculaAuxiliar,
                                    frmtxtMatriculaSupervisorEquipagem
                                ]
                             }                             
                        ]
            };

            var fieldsetEquipagem = {
                xtype: 'fieldset',
                title: 'Equipagem',
                width: 400,
                autoHeight: true,
                bodyStyle: 'padding-top:10px',
                items: [
                            camposFieldEquipagem
                        ]
            };
            //FIM FIELD EQUIPAGEM

            //INICIO FIELD VAGÕES
            //INICIO LINHA 1
            var txtQTDVagoes = {
                xtype: 'textfield',
                name: 'txtQTDVagoes',
                id: 'txtQTDVagoes',
                fieldLabel: 'Quantidade Vagões',
                maskRe: /[0-9]/,
                autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '50' },
                width: 80,
                value: QtdeVagao,
                enableKeyEvents: true,
	            listeners: {
		                        keydown: function(field, e)
                                {
			                        if(e.ctrlKey)
                                    {
				                        e.stopEvent();
			                        }
		                        }
                            },
                disabled: (visualizar == "sim" ? true : false)
            };

            var frmtxtQTDVagoes = {
                layout: 'form',
                border: false,
                autowidth: true,
                items: [txtQTDVagoes]
            };

            var txtTB = {
                xtype: 'textfield',
                name: 'txtTB',
                id: 'txtTB',
                fieldLabel: 'TB',
                autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '10' },
                maskRe: /[0-9,]/,
                width: 120,
                value: Tb,
                enableKeyEvents: true,
	            listeners: {
		                        keydown: function(field, e)
                                {
			                        if(e.ctrlKey)
                                    {
				                        e.stopEvent();
			                        }
		                        }
                            },
                disabled: (visualizar == "sim" ? true : false)
            };

            var frmtxtTB = {
                id: 'frmtxtTB',
                layout: 'form',
                border: false,
                autowidth: true,
                items: [txtTB]
            };

            //FIM LINHA 1

            var camposFieldVagoes = {
                layout: 'column',
                border: false,
                autowidth: true,
                items: [

                            {
                                layout: 'form',
                                width: 200,
                                labelAlign: 'left',
                                labelWidth: 102,
                                border: false,
                                items:
                                [
                                    frmtxtQTDVagoes
                                ]
                            },
                             {
                                 layout: 'form',
                                 width: 170,
                                 labelAlign: 'left',
                                 labelWidth: 15,
                                 border: false,
                                 items:
                                [
                                    frmtxtTB
                                ]
                             }
                        ]
            };

            var fieldsetVagoes = {
                xtype: 'fieldset',
                title: 'Vagões',
                width: 400,
                height: 50,
                //autoHeight: true,
                bodyStyle: 'padding-top:10px',
                items: [camposFieldVagoes]
            };

            //FIM FIELD VAGÕES

            //INICIO FIELD LOCOMOTIVAS
            //INICIO LINHA 1
            var txtLoco1 = {
                xtype: 'textfield',
                name: 'txtLoco1',
                id: 'txtLoco1',
                fieldLabel: 'Loco 1',
                //maskRe: /[a-zA-Z0-9]/,
                autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '5' },
                width: 80,
                value: Locomotiva1,
                disabled: (visualizar == "sim" ? true : false)
            };

            var frmtxtLoco1 = {
                id: 'frmtxtLoco1',
                layout: 'form',
                border: false,
                autowidth: true,
                height: 47,
                items: [txtLoco1]
            };

            var txtDiesel1 = {
                xtype: 'numberfield',
                name: 'txtDiesel1',
                id: 'txtDiesel1',
                fieldLabel: 'Diesel',
                allowDecimals: true,
                decimalSeparator: ',',
                autoCreate: {
                    tag: 'input',
                    type: 'text',
                    autocomplete: 'off',
                    maxlength: '14'
                },
                width: 80,
                value: Diesel1,
                enableKeyEvents: true,
	            listeners: {
		                        keydown: function(field, e)
                                {
			                        if(e.ctrlKey)
                                    {
				                        e.stopEvent();
			                        }
		                        }
                            },
                disabled: (visualizar == "sim" ? true : false)
            };

            var frmtxtDiesel1 = {
                id: 'frmtxtDiesel1',
                layout: 'form',
                border: false,
                autowidth: true,
                items: [txtDiesel1]
            };

            var dtVencimento1 = new Ext.form.DateField({
                fieldLabel: 'Vencimento',
                id: 'dtVencimento1',
                name: 'dtVencimento1',
                dateFormat: 'd/n/Y',
                width: 85,
                maxLength: 10,
                value: new Date(),
                plugins: [new Ext.ux.InputTextMask('99/99/9999', true)],
                value: DataVencimento1,
                disabled: (visualizar == "sim" ? true : false),
                allowBlank: true
            });

            var frmdtVencimento1 = {
                id: 'frmdtVencimento1',
                layout: 'form',
                border: false,
                autowidth: true,
                items: [dtVencimento1]
            };

            var rdbCativas1 = {
                xtype: 'radiogroup',
                vertical: true,
                columns: 2,
                labelWidth: 50,
                id: 'rdbCativas1',
                fieldLabel: 'Cativas',
                items: [{
                    id: 'rdbCativas1Sim',
                    boxLabel: 'Sim',
                    name: 'rdbCativas1',
                    inputValue: 1,
                    checked: (Cativas1 == 'S') ? true : false
                }, {
                    id: 'rdbCativas1Nao',
                    boxLabel: 'Não',
                    name: 'rdbCativas1',
                    inputValue: 2,
                    checked: (Cativas1 == 'N') ? true : false
                }],
                disabled: (visualizar == "sim" ? true : false)
            };

            var frmrdbCativas1 = {
                id: 'frmrdbCativas1',
                layout: 'form',
                border: false,
                autowidth: true,
                height: 47,
                items: [rdbCativas1]
            };

            var txtOBSCativas1 = {
                xtype: 'textfield',
                name: 'txtOBSCativas1',
                id: 'txtOBSCativas1',
                fieldLabel: 'Observação',
                autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '2000' },
                width: 200,
                value: Observacao1,
                disabled: (visualizar == "sim" ? true : false)
            };

            var frmtxtOBSCativas1 = {
                id: 'frmtxtOBSCativas1',
                layout: 'form',
                border: false,
                autowidth: true,
                //height: 47,
                items: [txtOBSCativas1]
            };

            //FIM LINHA 1

            //INICIO LINHA 2
            var txtLoco2 = {
                xtype: 'textfield',
                name: 'txtLoco2',
                id: 'txtLoco2',
                fieldLabel: 'Loco 2',
                //maskRe: /[a-zA-Z0-9]/,
                autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '5' },
                width: 80,
                value: Locomotiva2,
                disabled: (visualizar == "sim" ? true : false)
            };

            var frmtxtLoco2 = {
                id: 'frmtxtLoco2',
                layout: 'form',
                border: false,
                autowidth: true,
                height: 47,
                items: [txtLoco2]
            };

            var txtDiesel2 = {
                xtype: 'numberfield',
                name: 'txtDiesel2',
                id: 'txtDiesel2',
                fieldLabel: 'Diesel',
                allowDecimals: true,
                decimalSeparator: ',',
                autoCreate: {
                    tag: 'input',
                    type: 'text',
                    autocomplete: 'off',
                    maxlength: '14'
                },
                width: 80,
                value: Diesel2,
                enableKeyEvents: true,
	            listeners: {
		                        keydown: function(field, e)
                                {
			                        if(e.ctrlKey)
                                    {
				                        e.stopEvent();
			                        }
		                        }
                            },
                disabled: (visualizar == "sim" ? true : false)
            };

            var frmtxtDiesel2 = {
                id: 'frmtxtDiesel2',
                layout: 'form',
                border: false,
                autowidth: true,
                items: [txtDiesel2]
            };

            var dtVencimento2 = new Ext.form.DateField({
                fieldLabel: 'Vencimento',
                id: 'dtVencimento2',
                name: 'dtVencimento2',
                dateFormat: 'd/n/Y',
                width: 85,
                maxLength: 10,
                value: new Date(),
                plugins: [new Ext.ux.InputTextMask('99/99/9999', true)],
                value: DataVencimento2,
                disabled: (visualizar == "sim" ? true : false)
            });
            var frmdtVencimento2 = {
                id: 'frmdtVencimento2',
                layout: 'form',
                border: false,
                autowidth: true,
                items: [dtVencimento2]
            };

            var rdbCativas2 = {
                xtype: 'radiogroup',
                vertical: true,
                columns: 2,
                labelWidth: 50,
                id: 'rdbCativas2',
                fieldLabel: 'Cativas',
                items: [{
                    id: 'rdbCativas2Sim',
                    boxLabel: 'Sim',
                    name: 'rdbCativas2',
                    inputValue: 1,
                    checked: (Cativas2 == 'S') ? true : false
                }, {
                    id: 'rdbCativas2Nao',
                    boxLabel: 'Não',
                    name: 'rdbCativas2',
                    inputValue: 2,
                    checked: (Cativas2 == 'N') ? true : false
                }],
                disabled: (visualizar == "sim" ? true : false)
            };

            var frmrdbCativas2 = {
                id: 'frmrdbCativas2',
                layout: 'form',
                border: false,
                autowidth: true,
                height: 47,
                items: [rdbCativas2]
            };

            var txtOBSCativas2 = {
                xtype: 'textfield',
                name: 'txtOBSCativas2',
                id: 'txtOBSCativas2',
                fieldLabel: 'Observação',
                autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '2000' },
                width: 200,
                value: Observacao2,
                disabled: (visualizar == "sim" ? true : false)
            };

            var frmtxtOBSCativas2 = {
                id: 'frmtxtOBSCativas2',
                layout: 'form',
                border: false,
                autowidth: true,
                //height: 47,
                items: [txtOBSCativas2]
            };

            //FIM LINHA 2

            //INICIO LINHA 3
            var txtLoco3 = {
                xtype: 'textfield',
                name: 'txtLoco3',
                id: 'txtLoco3',
                fieldLabel: 'Loco 3',
                //maskRe: /[a-zA-Z0-9]/,
                autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '5' },
                width: 80,
                value: Locomotiva3,
                disabled: (visualizar == "sim" ? true : false)
            };

            var frmtxtLoco3 = {
                id: 'frmtxtLoco3',
                layout: 'form',
                border: false,
                autowidth: true,
                items: [txtLoco3]
            };

            var txtDiesel3 = {
                xtype: 'numberfield',
                name: 'txtDiesel3',
                id: 'txtDiesel3',
                fieldLabel: 'Diesel',
                allowDecimals: true,
                decimalSeparator: ',',
                autoCreate: {
                    tag: 'input',
                    type: 'text',
                    autocomplete: 'off',
                    maxlength: '14'
                },
                width: 80,
                value: Diesel3,
                enableKeyEvents: true,
	            listeners: {
		                        keydown: function(field, e)
                                {
			                        if(e.ctrlKey)
                                    {
				                        e.stopEvent();
			                        }
		                        }
                            },
                disabled: (visualizar == "sim" ? true : false)
            };

            var frmtxtDiesel3 = {
                id: 'frmtxtDiesel3',
                layout: 'form',
                border: false,
                autowidth: true,
                items: [txtDiesel3]
            };

            var dtVencimento3 = new Ext.form.DateField({
                fieldLabel: 'Vencimento',
                id: 'dtVencimento3',
                name: 'dtVencimento3',
                dateFormat: 'd/n/Y',
                width: 85,
                maxLength: 10,
                value: new Date(),
                plugins: [new Ext.ux.InputTextMask('99/99/9999', true)],
                value: DataVencimento3,
                disabled: (visualizar == "sim" ? true : false)
            });
            var frmdtVencimento3 = {
                id: 'frmdtVencimento3',
                layout: 'form',
                border: false,
                autowidth: true,
                items: [dtVencimento3]
            };

            var rdbCativas3 = {
                xtype: 'radiogroup',
                vertical: true,
                columns: 2,
                labelWidth: 50,
                id: 'rdbCativas3',
                fieldLabel: 'Cativas',
                items: [{
                    id: 'rdbCativas3Sim',
                    boxLabel: 'Sim',
                    name: 'rdbCativas3',
                    inputValue: 1,
                    checked: (Cativas3 == 'S') ? true : false
                }, {
                    id: 'rdbCativas3Nao',
                    boxLabel: 'Não',
                    name: 'rdbCativas3',
                    inputValue: 2,
                    checked: (Cativas3 == 'N') ? true : false
                }],
                disabled: (visualizar == "sim" ? true : false)
            };

            var frmrdbCativas3 = {
                id: 'frmrdbCativas3',
                layout: 'form',
                border: false,
                autowidth: true,
                height: 52,
                items: [rdbCativas3]
            };

            var txtOBSCativas3 = {
                xtype: 'textfield',
                name: 'txtOBSCativas3',
                id: 'txtOBSCativas3',
                fieldLabel: 'Observação',
                autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '2000' },
                width: 200,
                value: Observacao3,
                disabled: (visualizar == "sim" ? true : false)
            };

            var frmtxtOBSCativas3 = {
                id: 'frmtxtOBSCativas3',
                layout: 'form',
                border: false,
                autowidth: true,
                //height: 45,
                items: [txtOBSCativas3]
            };

            //FIM LINHA 3

            //INICIO LINHA 4
            var txtLoco4 = {
                xtype: 'textfield',
                name: 'txtLoco4',
                id: 'txtLoco4',
                fieldLabel: 'Loco 4',
                //maskRe: /[a-zA-Z0-9]/,
                autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '5' },
                width: 80,
                value: Locomotiva4,
                disabled: (visualizar == "sim" ? true : false)
            };

            var frmtxtLoco4 = {
                id: 'frmtxtLoco4',
                layout: 'form',
                border: false,
                autowidth: true,
                height: 47,
                items: [txtLoco4]
            };

            var txtDiesel4 = {
                xtype: 'numberfield',
                name: 'txtDiesel4',
                id: 'txtDiesel4',
                fieldLabel: 'Diesel',
                allowDecimals: true,
                decimalSeparator: ',',
                autoCreate: {
                    tag: 'input',
                    type: 'text',
                    autocomplete: 'off',
                    maxlength: '14'
                },
                width: 80,
                value: Diesel4,
                enableKeyEvents: true,
	            listeners: {
		                        keydown: function(field, e)
                                {
			                        if(e.ctrlKey)
                                    {
				                        e.stopEvent();
			                        }
		                        }
                            },
                disabled: (visualizar == "sim" ? true : false)
            };

            var frmtxtDiesel4 = {
                id: 'frmtxtDiesel4',
                layout: 'form',
                border: false,
                autowidth: true,
                items: [txtDiesel4]
            };

            var dtVencimento4 = new Ext.form.DateField({
                fieldLabel: 'Vencimento',
                id: 'dtVencimento4',
                name: 'dtVencimento4',
                dateFormat: 'd/n/Y',
                width: 85,
                maxLength: 10,
                value: new Date(),
                plugins: [new Ext.ux.InputTextMask('99/99/9999', true)],
                value: DataVencimento4,
                disabled: (visualizar == "sim" ? true : false)
            });
            var frmdtVencimento4 = {
                id: 'frmdtVencimento4',
                layout: 'form',
                border: false,
                autowidth: true,
                items: [dtVencimento4]
            };

            var rdbCativas4 = {
                xtype: 'radiogroup',
                vertical: true,
                columns: 2,
                labelWidth: 50,
                id: 'rdbCativas4',
                fieldLabel: 'Cativas',
                items: [{
                    id: 'rdbCativas4Sim',
                    boxLabel: 'Sim',
                    name: 'rdbCativas4',
                    inputValue: 1,
                    checked: (Cativas4 == 'S') ? true : false
                }, {
                    id: 'rdbCativas4Nao',
                    boxLabel: 'Não',
                    name: 'rdbCativas4',
                    inputValue: 2,
                    checked: (Cativas4 == 'N') ? true : false
                }],
                disabled: (visualizar == "sim" ? true : false)
            };

            var frmrdbCativas4 = {
                id: 'frmrdbCativas4',
                layout: 'form',
                border: false,
                autowidth: true,
                height: 47,
                items: [rdbCativas4]
            };

            var txtOBSCativas4 = {
                xtype: 'textfield',
                name: 'txtOBSCativas4',
                id: 'txtOBSCativas4',
                fieldLabel: 'Observação',
                autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '2000' },
                width: 200,
                value: Observacao4,
                disabled: (visualizar == "sim" ? true : false)
            };

            var frmtxtOBSCativas4 = {
                id: 'frmtxtOBSCativas4',
                layout: 'form',
                border: false,
                autowidth: true,
                //height: 47,
                items: [txtOBSCativas4]
            };

            //FIM LINHA 1

            var camposFieldLocomotivas = {
                layout: 'column',
                border: false,
                autowidth: true,
                items: [

                            {
                                layout: 'form',
                                width: 90,
                                labelAlign: 'top',
                                border: false,
                                items:
                                [
                                    frmtxtLoco1, frmtxtLoco2, frmtxtLoco3, frmtxtLoco4
                                ]
                            },
                             {
                                 layout: 'form',
                                 width: 90,
                                 labelAlign: 'top',
                                 border: false,
                                 items:
                                [
                                    frmtxtDiesel1, frmtxtDiesel2, frmtxtDiesel3, frmtxtDiesel4
                                ]
                             },
                             {
                                 layout: 'form',
                                 width: 90,
                                 labelAlign: 'top',
                                 border: false,
                                 items:
                                [
                                    frmdtVencimento1, frmdtVencimento2, frmdtVencimento3, frmdtVencimento4
                                ]
                             },
                             {
                                 layout: 'form',
                                 width: 90,
                                 labelAlign: 'top',
                                 border: false,
                                 items:
                                [
                                    frmrdbCativas1, frmrdbCativas2, frmrdbCativas3, frmrdbCativas4
                                ]
                             },
                             {
                                 layout: 'form',
                                 width: 200,
                                 labelAlign: 'top',
                                 border: false,
                                 items:
                                [
                                    frmtxtOBSCativas1, frmtxtOBSCativas2, frmtxtOBSCativas3, frmtxtOBSCativas4
                                ]
                             }
                        ]
            };

            var fieldsetLocomotivas = {
                xtype: 'fieldset',
                title: 'Locomotivas',
                //bodyStyle: 'padding-top:10px',
                //autowidth: true,
                width: 600,
                //height: 210,
                autoHeight: true,
                items: [
                            camposFieldLocomotivas
                        ]
            };

          

            var formDireita = {
                layout: 'form',
                border: false,
                bodyStyle: 'padding: 2px',
                autoWidth: true,
                items: [fieldsetLocomotivas, fieldsetComentAdicionais]
            };
            //FIM FIELD LOCOMOTIVAS

            //INICIO FIELD CHECKLIST
            //INICIO QUADRO ESQUERDA
            //INICIO 1
            var lblTesteGradiente = { xtype: 'label', id: 'lblTesteGradiente', text: '1 - Teste Gradiente, Máximo 2 PSI' };

            var frmlblTesteGradiente = {
                id: 'frmlblTesteGradiente',
                layout: 'form',
                border: false,
                width: 250,
                items: [lblTesteGradiente]
            };

            var lblPressaoLocomotiva = { xtype: 'label', id: 'lblPressaoLocomotiva', text: 'A Pressão da locomotiva deverá estar em 90 PSI e a cauda no minímo 88 PSI' };

            var frmlblPressaoLocomotiva = {
                id: 'frmlblTesteGradiente',
                layout: 'form',
                border: false,
                width: 230,
                style:
                            {
                                'color': 'red'
                            },
                items: [lblPressaoLocomotiva]
            };

            var rdbTesteGradiente = {
                xtype: 'radiogroup',
                vertical: true,
                columns: 2,
                labelWidth: 50,
                id: 'rdbTesteGradiente',
                //fieldLabel: 'Status',
                items: [{
                    id: 'rdbTesteGradienteSim',
                    boxLabel: 'Sim',
                    name: 'rdbTesteGradiente',
                    inputValue: 1,
                    checked: (Resposta1 == 'S') ? true : false
                }, {
                    id: 'rdbTesteGradienteNao',
                    boxLabel: 'Não',
                    name: 'rdbTesteGradiente',
                    inputValue: 2,
                    checked: (Resposta1 == 'N') ? true : false
                }],
                disabled: (visualizar == "sim" ? true : false)
            };

            var frmrdbTesteGradiente = {
                id: 'frmrdbTesteGradiente',
                layout: 'form',
                border: false,
                width: 190,
                items: [rdbTesteGradiente]
            };

            //FIM 1

            //INICIO 2
            var lblTesteVazamento = { xtype: 'label', id: 'lblTesteVazamento', text: '2 - Teste de vazamento, Máximo 4 PSI' };

            var frmlblTesteVazamento = {
                id: 'frmlblTesteVazamento',
                layout: 'form',
                border: false,
                width: 250,
                items: [lblTesteVazamento]
            };

            var rdbTesteVazamento = {
                xtype: 'radiogroup',
                vertical: true,
                columns: 2,
                labelWidth: 50,
                id: 'rdbTesteVazamento',
                //fieldLabel: 'Status',
                items: [{
                    id: 'rdbTesteVazamentoSim',
                    boxLabel: 'Sim',
                    name: 'rdbTesteVazamentoSim',
                    inputValue: 1,
                    checked: (Resposta2 == 'S') ? true : false
                }, {
                    id: 'rdbTesteVazamentoNao',
                    boxLabel: 'Não',
                    name: 'rdbTesteVazamentoSim',
                    inputValue: 2,
                    checked: (Resposta2 == 'N') ? true : false
                }],
                disabled: (visualizar == "sim" ? true : false)
            };

            var frmrdbTesteVazamento = {
                id: 'frmrdbTesteVazamento',
                layout: 'form',
                border: false,
                width: 190,
                items: [rdbTesteVazamento]
            };
            //FIM 2

            //INICIO 3
            var lblTesteResposta = { xtype: 'label', id: 'lblTesteResposta', text: '3 - Teste de resposta' };

            var frmlblTesteResposta = {
                id: 'frmlblTesteResposta',
                layout: 'form',
                border: false,
                width: 250,
                items: [lblTesteResposta]
            };

            var rdbTesteResposta = {
                xtype: 'radiogroup',
                vertical: true,
                columns: 2,
                labelWidth: 50,
                id: 'rdbTesteResposta',
                //fieldLabel: 'Status',
                items: [{
                    id: 'rdbTesteRespostaSim',
                    boxLabel: 'Sim',
                    name: 'rdbTesteResposta',
                    inputValue: 1,
                    checked: (Resposta3 == 'S') ? true : false
                }, {
                    id: 'rdbTesteRespostaNao',
                    boxLabel: 'Não',
                    name: 'rdbTesteResposta',
                    inputValue: 2,
                    checked: (Resposta3 == 'N') ? true : false
                }],
                disabled: (visualizar == "sim" ? true : false)
            };

            var frmrdbTesteResposta = {
                id: 'frmrdbTesteResposta',
                layout: 'form',
                border: false,
                width: 190,
                items: [rdbTesteResposta]
            };
            //FIM 3

            //FIM QUADRO ESQUERDA

            //INICIO QUADRO DIREITA
            //INICIO 1
            var lblListaVagoes = { xtype: 'label', id: 'lblListaVagoes', text: '4 - Lista de Vagões Fisico x Sistema' };

            var frmlblListaVagoes = {
                id: 'frmlblListaVagoes',
                layout: 'form',
                border: false,
                width: 250,
                items: [lblListaVagoes]
            };

            var rdbListaVagoes = {
                xtype: 'radiogroup',
                vertical: true,
                columns: 2,
                labelWidth: 50,
                id: 'rdbListaVagoes',
                //fieldLabel: 'Status',
                items: [{
                    id: 'rdbListaVagoesSim',
                    boxLabel: 'Sim',
                    name: 'rdbListaVagoesNao',
                    inputValue: 1,
                    checked: (Resposta4 == 'S') ? true : false
                }, {
                    id: 'rdbListaVagoesNao',
                    boxLabel: 'Não',
                    name: 'rdbListaVagoesNao',
                    inputValue: 2,
                    checked: (Resposta4 == 'N') ? true : false
                }],
                disabled: (visualizar == "sim" ? true : false)
            };

            var frmrdbListaVagoes = {
                id: 'frmrdbListaVagoes',
                layout: 'form',
                labelAlign: 'top',
                border: false,
                width: 190,
                items: [rdbListaVagoes]
            };
            //FIM 1

            //INICIO 2
            var lblNCruzamentosForaPO = { xtype: 'label', id: 'lblNCruzamentosForaPO', text: '5 - Nº cruzamentos fora do PO 00581' };

            var frmlblNCruzamentosForaPO = {
                id: 'frmlblNCruzamentosForaPO',
                layout: 'form',
                border: false,
                width: 250,
                items: [lblNCruzamentosForaPO]
            };

            var txtNCruzamentosForaPO = {
                xtype: 'textfield',
                name: 'txtNCruzamentosForaPO',
                id: 'txtNCruzamentosForaPO',
                //fieldLabel: '',
                hideLabel: true,
                autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '20' },
                maskRe: /[0-9]/,
                ////style: 'text-transform: uppercase',
                width: 80,
                value: Resposta5NumCruzamentos,
                enableKeyEvents: true,
	            listeners: {
		                        keydown: function(field, e)
                                {
			                        if(e.ctrlKey)
                                    {
				                        e.stopEvent();
			                        }
		                        }

                            },
                disabled: (visualizar == "sim" ? true : false)
            };

            var frmtxtNCruzamentosForaPO = {
                id: 'frmtxtNCruzamentosForaPO',
                layout: 'form',
                border: false,
                autowidth: true,
                items: [txtNCruzamentosForaPO]
            };

            var txtSupervisorNCruzamentoForaPO = {
                xtype: 'textfield',
                name: 'txtSupervisorNCruzamentoForaPO',
                id: 'txtSupervisorNCruzamentoForaPO',
                fieldLabel: 'Supervisor',
                autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '70' },
                //maskRe: /[0-9]/,
                labelwidth: 10,
                //style: 'text-transform: uppercase',
                width: 80,
                value: Resposta5Supervisor,
                disabled: (visualizar == "sim" ? true : false)
            };

            var frmtxtSupervisorNCruzamentoForaPO = {
                id: 'frmtxtSupervisorNCruzamentoForaPO',
                layout: 'form',
                border: false,
                labelWidth: 55,
                autowidth: true,
                items: [txtSupervisorNCruzamentoForaPO]
            };

            var txtMatriculaNCruzamentoForaPO = {
                xtype: 'textfield',
                name: 'txtMatriculaNCruzamentoForaPO',
                id: 'txtMatriculaNCruzamentoForaPO',
                fieldLabel: 'Matricula',
                autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '12' },
                //maskRe: /[0-9]/,
                //style: 'text-transform: uppercase',
                width: 80,
                value: Resposta5Matricula,
                disabled: (visualizar == "sim" ? true : false)
            };

            var frmtxtMatriculaNCruzamentoForaPO = {
                id: 'frmtxtMatriculaNCruzamentoForaPO',
                layout: 'form',
                border: false,
                labelWidth: 45,
                autowidth: true,
                items: [txtMatriculaNCruzamentoForaPO]
            };

            var linha1camposNCruzamentosForaPO = {
                layout: 'column',
                border: false,
                width: 600,
                items: [frmtxtNCruzamentosForaPO, frmtxtSupervisorNCruzamentoForaPO, frmtxtMatriculaNCruzamentoForaPO]
            };
            //FIM 2

            //INICIO 3
            var lblTotalCruzamentos = { xtype: 'label', id: 'lblTotalCruzamentos', text: '6 - Total de cruzamentos?' };

            var frmlblTotalCruzamentos = {
                id: 'frmlblTotalCruzamentos',
                layout: 'form',
                border: false,
                width: 250,
                items: [lblTotalCruzamentos]
            };

            var txtTotalCruzamentos = {
                xtype: 'textfield',
                name: 'txtTotalCruzamentos',
                id: 'txtTotalCruzamentos',
                //fieldLabel: '',
                hideLabel: true,
                autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '10' },
                maskRe: /[0-9]/,
                //style: 'text-transform: uppercase',
                width: 80,
                value: Resposta6TotalCruzamentos,
                enableKeyEvents: true,
	            listeners: {
		                        keydown: function(field, e)
                                {
			                        if(e.ctrlKey)
                                    {
				                        e.stopEvent();
			                        }
		                        }
                            },
                disabled: (visualizar == "sim" ? true : false)
            };

            var frmtxtTotalCruzamentos = {
                id: 'frmtxtTotalCruzamentos',
                layout: 'form',
                border: false,
                autowidth: true,
                items: [txtTotalCruzamentos]
            };
            //FIM 3

            //INICIO 4
            var lblQuantCruzamentosPararam = { xtype: 'label', id: 'lblQuantCruzamentosPararam', text: '7 - Quantos cruzamentos pararam?' };

            var frmlblQuantCruzamentosPararam = {
                id: 'frmlblQuantCruzamentosPararam',
                layout: 'form',
                border: false,
                width: 250,
                items: [lblQuantCruzamentosPararam]
            };

            var txtQuantCruzamentosPararam = {
                xtype: 'textfield',
                name: 'txtQuantCruzamentosPararam',
                id: 'txtQuantCruzamentosPararam',
                //fieldLabel: '',
                hideLabel: true,
                autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '10' },
                maskRe: /[0-9]/,
                //style: 'text-transform: uppercase',
                width: 80,
                value: Resposta7QtdePararam,
                enableKeyEvents: true,
	            listeners: {
		                        keydown: function(field, e)
                                {
			                        if(e.ctrlKey)
                                    {
				                        e.stopEvent();
			                        }
		                        }
                            },
                disabled: (visualizar == "sim" ? true : false)
            };

            var frmtxtQuantCruzamentosPararam = {
                id: 'frmtxtQuantCruzamentosPararam',
                layout: 'form',
                border: false,
                autowidth: true,
                items: [txtQuantCruzamentosPararam]
            };
            //FIM 4
            //FIM QUADRO DIREITA

            //FIM FIELD CHECKLIST

            var camposFieldCheckList = {
                layout: 'column',
                border: false,
                autowidth: true,
                items: [

                            {
                                layout: 'form',
                                width: 270,
                                labelAlign: 'top',
                                border: false,
                                items:
                                [
                                    lblTesteGradiente,
                                    frmlblPressaoLocomotiva,
                                    frmrdbTesteGradiente,
                                    frmlblTesteVazamento,
                                    frmrdbTesteVazamento,
                                    frmlblTesteResposta,
                                    frmrdbTesteResposta
                                ]
                            },
                             {
                                 layout: 'form',
                                 width: 370,
                                 labelAlign: 'left',
                                 border: false,
                                 items:
                                [
                                    frmlblListaVagoes,
                                    frmrdbListaVagoes,
                                    frmlblNCruzamentosForaPO,
                                    linha1camposNCruzamentosForaPO,
                                    frmlblTotalCruzamentos,
                                    frmtxtTotalCruzamentos,
                                    frmlblQuantCruzamentosPararam,
                                    frmtxtQuantCruzamentosPararam
                                ]
                             }
                        ]
            };

            //INICIO FIELD PREVISÃO

            //INICIO 1
            var lblHoraChegadaLocoOrigem = { xtype: 'label', id: 'lblHoraChegadaLocoOrigem', text: 'Hora Chegada Loco na Origem' };

            var frmlblHoraChegadaLocoOrigem = {
                id: 'frmlblHoraChegadaLocoOrigem',
                layout: 'form',
                border: false,
                autowidth: true,
                items: [lblHoraChegadaLocoOrigem]
            };

            var horaChegadaLocoOrigem = new Ext.form.TimeField({
                name: 'horaChegadaLocoOrigem',
                id: 'horaChegadaLocoOrigem',
                format: 'H:i',
                increment: 1,
                fieldLabel: 'Hora : Minuto',
                maxlength: 5,
                plugins: [new Ext.ux.InputTextMask('99:99', true)],
                width: 65,
                value: HoraChegada,
                editable: true,
                disabled: (visualizar == "sim" ? true : false)
            });

            var frmhoraChegadaLocoOrigem = {
                id: 'frmhoraChegadaLocoOrigem',
                layout: 'form',
                border: false,
                width: 90,
                items: [horaChegadaLocoOrigem]
            };

            var linha1fieldPrevisao = {
                layout: 'column',
                border: false,
                autowidth: true,
                items: [frmlblHoraChegadaLocoOrigem]
            };

            var linha2fieldPrevisao = {
                layout: 'column',
                border: false,
                autowidth: true,
                items: [frmhoraChegadaLocoOrigem]
            };
            //FIM 1

            //INICIO 2
            var txtdtPrevisto = {
                xtype: 'textfield',
                name: 'txtdtPrevisto',
                id: 'txtdtPrevisto',
                fieldLabel: 'Data',
                disabled: true,
                //hideLabel: true,
                autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '10' },
                //maskRe: /[0-9]/,
                //style: 'text-transform: uppercase',
                width: 65,
                value: DataPrevisto
            };

            var frmtxtdtPrevisto= {
                id: 'frmdtPrevisto',
                layout: 'form',
                border: false,
                width: 100,
                items: [txtdtPrevisto]
            };

            var lblPrevisto = { xtype: 'label', id: 'lblPrevisto', text: 'Partida prevista' };

            var frmlblPrevisto = {
                id: 'frmPrevisto',
                layout: 'form',
                border: false,
                width: 180,
                items: [lblPrevisto]
            };

            var horaPrevisto = {
                xtype: 'textfield',
                name: 'horaPrevisto',
                id: 'horaPrevisto',
                fieldLabel: 'Hora : Minuto',
                //hideLabel: true,
                autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '10' },
                disabled: true,
                //maskRe: /[0-9]/,
                //style: 'text-transform: uppercase',
                width: 50,
                value: HoraPrevisto
            };

            var frmhoraPrevisto = {
                id: 'frmhoraPrevisto',
                layout: 'form',
                border: false,
                width: 100,
                items: [horaPrevisto]
            };

            var frmPrevisto = {
                id: 'frmPrevisto',
                layout: 'column',
                border: false,
                autowidth: true,
                autoheight: true,
                items: [frmlblPrevisto, frmtxtdtPrevisto,frmhoraPrevisto]
            };

            //FIM 2

            //INICIO 3

            var txtdtReal = {
                xtype: 'textfield',
                name: 'txtdtReal',
                id: 'txtdtReal',
                fieldLabel: 'Data',
                //hideLabel: true,
                disabled: true,
                autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '10' },
                //maskRe: /[0-9]/,
                //style: 'text-transform: uppercase',
                width: 65,
                value: DataReal
            };

            var frmtxtdtReal= {
                id: 'frmdtReal',
                layout: 'form',
                border: false,
                width: 100,
                items: [txtdtReal]
            };

            var lblReal = { xtype: 'label', id: 'lblReal', text: 'Partida real' };

            var frmlblReal = {
                id: 'frmReal',
                layout: 'form',
                border: false,
                width: 180,
                items: [lblReal]
            };

            var horaReal = {
                xtype: 'textfield',
                name: 'horaReal',
                id: 'horaReal',
                fieldLabel: 'Hora : Minuto',
                //hideLabel: true,
                disabled: true,
                autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '10' },
                //maskRe: /[0-9]/,
                //style: 'text-transform: uppercase',
                width: 50,
                value: HoraReal
            };


            var frmhoraReal = {
                id: 'frmhoraReal',
                layout: 'form',
                border: false,
                width: 100,
                items: [horaReal]
            };

            var frmReal = {
                id: 'frmReal',
                layout: 'column',
                border: false,
                autowidth: true,
                autoheight: true,
                items: [frmlblReal, frmtxtdtReal,frmhoraReal]
            };

            //FIM 3

            // CHEGADA REAL
            var txtdtChegadaReal = {
                xtype: 'textfield',
                name: 'txtdtChegadaReal',
                id: 'txtdtChegadaReal',
                fieldLabel: 'Data',
                disabled: true,
                //hideLabel: true,
                autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '10' },
                //maskRe: /[0-9]/,
                //style: 'text-transform: uppercase',
                width: 65,
                value: DataChegadaReal
            };

            var frmtxtdtChegadaReal = {
                id: 'frmtxtdtChegadaReal',
                layout: 'form',
                border: false,
                width: 100,
                items: [txtdtChegadaReal]
            };

            var lblChegadaReal = { xtype: 'label', id: 'lblPrevisto', text: 'Chegada Real' };

            var frmlblChegadaReal = {
                id: 'frmlblChegadaReal',
                layout: 'form',
                border: false,
                width: 180,
                items: [lblChegadaReal]
            };

            var horaChegadaReal = {
                xtype: 'textfield',
                name: 'horaChegadaReal',
                id: 'horaChegadaReal',
                fieldLabel: 'Hora : Minuto',
                //hideLabel: true,
                autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '10' },
                disabled: true,
                //maskRe: /[0-9]/,
                //style: 'text-transform: uppercase',
                width: 50,
                value: HoraChegadaReal 
            };

            var frmhoraChegadaReal = {
                id: 'frmhoraChegadaReal',
                layout: 'form',
                border: false,
                width: 100,
                items: [horaChegadaReal]
            };

            var frmChegadaReal = {
                id: 'frmChegadaReal',
                layout: 'column',
                border: false,
                autowidth: true,
                autoheight: true,
                items: [frmlblChegadaReal, frmtxtdtChegadaReal, frmhoraChegadaReal]
            };

            // FIM CHEGADA REAL




            //FIM FIELD PREVISÃO

            var camposFieldPrevisao = {
                layout: 'column',
                border: false,
                autowidth: true,
                height: 242,
                items: [

                            {
                                layout: 'form',
                                //autowidth: true,
                                autoheight: true,
                                width: 230,
                                labelAlign: 'top',
                                border: false,
                                items:
                                [
                                    lblHoraChegadaLocoOrigem,
                                    horaChegadaLocoOrigem
                                ]
                            },
                            {
                                layout: 'form',
                                //autowidth: true,
                                width: 230,
                                autoheight: true,
                                labelAlign: 'top',
                                border: false,
                                items:
                                [
                                    frmPrevisto
                                ]
                            },
                            {
                                layout: 'form',
                                //autowidth: true,
                                width: 230,
                                autoheight: true,
                                labelAlign: 'top',
                                border: false,
                                items:
                                [
                                    frmReal
                                ]
                            },
                            {
                                layout: 'form',
                                //autowidth: true,
                                width: 230,
                                autoheight: true,
                                labelAlign: 'top',
                                border: false,
                                items:
                                [
                                    frmChegadaReal
                                ]
                            }
                        ]
            };

            var fieldsetPrevisao = {
                xtype: 'fieldset',
                title: 'Previsão',
                width: 260,
                //heigth: 240,
                //autoHeight: true,
                items: [
                        camposFieldPrevisao
                        ]
            };

            var formPrevisao = {
                layout: 'form',
                border: false,
                bodyStyle: 'padding-left: 5px',
                //autoWidth: true,
                width: 290,
                items: [fieldsetPrevisao]
            };

            var fieldsetCheckList = {
                xtype: 'fieldset',
                title: 'CheckList',
                width: 740,
                //height: 240,
                bodyStyle: 'padding-right:5px;padding-top:5px',
                //autoHeight: true,
                items: [camposFieldCheckList]
            };

            var formCheckList = {
                layout: 'form',
                border: false,
                //bodyStyle: 'padding: 2px',
                width: 740,
                items: [fieldsetCheckList]
            };

            var formCheckListPrevisao = {
                layout: 'column',
                border: false,
                bodyStyle: 'padding: 2px',
                width: 1100,
                items: [formCheckList, formPrevisao]
            };

            //INICIO BOTÕES RODAPÉ ABA CHECKLIST 
            var btnSalvarCheckList = {
                xtype: 'button',
                name: 'btnSalvarCheckList',
                id: 'btnSalvarCheckList',
                text: 'Salvar',
                //iconCls: 'icon-down',
                disabled: (SalvarC == 'False' ? true : visualizar == "sim" ? true : idOsChecklist != 0 || visualizar != "sim" ? false : true),
                listeners: {
                    click: function (btn, e) 
                    {
                        if(validaCampos())
                        {
                            SalvarChecklist(criarObjetoAbaCheckList());
                        }
                    }
                }
            };

            var btnCancelarCheckList = {
                xtype: 'button',
                name: 'btnCancelarCheckList',
                id: 'btnCancelarCheckList',
                text: 'Cancelar',
                //iconCls: 'icon-down',
                disabled: visualizar == "sim" ? true : false,
                listeners: {
                    click: function (btn, e) {
                    //console.log("vai cancelar");
                        CancelarCheckList();
                    }
                }
            };

            var btnConcluirCheckList = {
                xtype: 'button',
                name: 'btnConcluirCheckList',
                id: 'btnConcluirCheckList',
                text: 'Concluir',
                //iconCls: 'icon-down',
                disabled: (ConcluirC == 'False' ? true : visualizar == "sim" ? true : idOsChecklist == 0 || visualizar == "sim" ? true : idOsChecklistPassageiro == "" ? true : idOsChecklist != 0 || visualizar != "sim" ? false : true),
                listeners: {
                    click: function (btn, e) 
                    {
                        if(validaCampos())
                        {
                            SalvarChecklist(criarObjetoAbaCheckList());
                            ConcluirChecklist(criarObjetoAbaCheckList());
                        }
                    }
                }
            };

            //FIM BOTÕES RODAPÉ ABA CHECKLIST

              var formEsquerda = {
                layout: 'form',
                border: false,
                bodyStyle: 'padding: 2px',
                autoWidth: true,
                items: [fieldsetCCOCirculacao, fieldsetEquipagem,  fieldsetVagoes]
            };



            var linha1CheckList = {
                layout: 'column',
                border: false,
                //bodyStyle: 'padding: 2px',
                autoWidth: true,
                items: [
                        formEsquerda,
                        formDireita
                        ]
            };

            var linha2CheckList = {
                layout: 'column',
                border: false,
                //bodyStyle: 'padding: 2px',
                autowidth: true,
                items: [
                           formCheckListPrevisao
                        ]
            };

            //@@@@@ FIM @@@@@@@@ ABA CHECKLIST  @@@@@@@@@@@@@@@@@@@@@@ 

            var painelGeralCheckList = new Ext.form.FormPanel({
                id: 'painelGeralCheckList',
                layout: 'form',
                //labelAlign: 'top',
                standardSubmit: true,
                border: false,
                //autoHeight: true,
                height: 670,
                buttonAlign: "center",
                title: "",
                bodyStyle: 'padding: 1px',
                items: [
                                linha1CheckList,
                                linha2CheckList
                        ],
                buttons:
	                    [
                            btnSalvarCheckList,
                            btnCancelarCheckList,
                            btnConcluirCheckList
                        ]
            });

            AbaCheckList.getForm = function () {

                return painelGeralCheckList;

            };



        } ());

        Ext.onReady(function () 
        {
            //Carrega Dados Tela para verificar se usuario alterou algo antes de sair da tela
            objTela = criarObjetoAbaCheckListMequetrefe();
        });

        geral.AbaCheckList = AbaCheckList;
        return geral;
    } (window.geral || {}));

    // INICIO *****javascript*****  

    function SalvarChecklist(osChecklistPassageiroDto) {
        // Progress BAR
        Ext.getBody().mask("Processando dados...", "x-mask-loading");

        $.ajax({
            url: '<%= Url.Action("SalvarCheckListPassageiro","CheckListPassageiro") %>',
            type: "POST",
            dataType: 'json',
            data: $.toJSON({ osChecklistPassageiroDto: osChecklistPassageiroDto }),
            contentType: "application/json; charset=utf-8",
            async: false,
            success: function (result) {

                Ext.getBody().unmask();

                if (result.Result) {
                    //Atualiza pesquisa tela 1421
                    AtualizaPesquisaTelaBusca();
          
                    idOsChecklistPassageiro = result.idOsChecklistPassageiro;
                    idOsChecklist = result.idOsChecklist;

                    //Desabilita RADIOS tipo OS após salva-la
                    Ext.getCmp("rdbTipo").setDisabled(true);

                    //Habilita botão concluir checklist após salvar com sucesso
                    Ext.getCmp("btnConcluirCheckList").setDisabled(false);

                    Ext.Msg.show({
                        title: "",
                        msg: "CheckList foi salvo com sucesso.",
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO,
                        minWidth: 200
                    });
                    salvou = true;
                } else {
                    Ext.Msg.show({
                        title: "Erro",
                        msg: result.Message,
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.ERROR,
                        minWidth: 200
                    });
                    salvou = false;
                }
            },
            error: function (response) {

                Ext.getBody().unmask();

                Ext.Msg.show({
                    title: 'Aviso',
                    msg: retorno.Message,
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.WARNING
                });

            }
        });
    }

    function ConcluirChecklist(osChecklistPassageiroDto) {
        Ext.Msg.show({
            title: 'Aviso',
            msg: 'Deseja concluir o checklist?',
            buttons: Ext.Msg.YESNO,
            icon: Ext.MessageBox.WARNING,
            fn: function (buttonValue) {
                if (buttonValue == "yes") {
                    //Progress BAR
                    Ext.getBody().mask("Processando dados...", "x-mask-loading");
                    //Execução AJAX
                    $.ajax({
                        url: '<%= Url.Action("ConcluirCheckListPassageiro","CheckListPassageiro") %>',
                        type: "POST",
                        dataType: 'json',
                        data: $.toJSON({ osChecklistPassageiroDto: osChecklistPassageiroDto }),
                        timeout: 300000,
                        contentType: "application/json; charset=utf-8",
                        async: false,
                        success: function (result) 
                        {
                            Ext.getBody().unmask();
                            //Atualiza pesquisa tela 1421
                            AtualizaPesquisaTelaBusca();
                            if (result.Result) 
                            {   
                                salvou = true;
                                Ext.Msg.show
                                ({
                                            title: "",
                                            msg: "CheckList foi concluido com sucesso.",
                                            buttons: Ext.Msg.OK,
                                            icon: Ext.MessageBox.INFO,
                                            minWidth: 200
                                });
                                
                            }
                        },
                         failure: function (conn, data) {
                            Ext.Msg.show({
                                    title: "Alerta",
                                    msg: result.Message,
                                    buttons: Ext.Msg.OK,
                                    icon: Ext.MessageBox.ERROR
                                });
                            Ext.getBody().unmask();
                        }
                    });
                }
            }
        });      
    }

   function ValidaCancelar() {
    var retorno = true;
    var objDadosAtualTela = criarObjetoAbaCheckList();
    retorno = (objTela.CCOtxtSupervisorCCO != objDadosAtualTela.CCOtxtSupervisorCCO ||
         objTela.CCOtxtMatriculaSupervisorCCO != objDadosAtualTela.CCOtxtMatriculaSupervisorCCO ||
         objTela.CCOtxtControlador != objDadosAtualTela.CCOtxtControlador ||
         objTela.CCOtxtMatriculaControlador != objDadosAtualTela.CCOtxtMatriculaControlador ||
         objTela.EquiptxtMaquinista != objDadosAtualTela.EquiptxtMaquinista ||
         objTela.EquiptxtMatriculaMaquinista != objDadosAtualTela.EquiptxtMatriculaMaquinista ||
         objTela.EquiptxtAuxiliar != objDadosAtualTela.EquiptxtAuxiliar ||
         objTela.EquiptxtMatriculaAuxiliar != objDadosAtualTela.EquiptxtMatriculaAuxiliar ||
         objTela.EquiptxtSupervisorEquipagem != objDadosAtualTela.EquiptxtSupervisorEquipagem ||
         objTela.EquiptxtMatriculaSupervisorEquipagem != objDadosAtualTela.EquiptxtMatriculaSupervisorEquipagem ||
         objTela.VagoestxtQTDVagoes != objDadosAtualTela.VagoestxtQTDVagoes ||
         objTela.VagoestxtTB != objDadosAtualTela.VagoestxtTB ||
         objTela.LocotxtLoco1 != objDadosAtualTela.LocotxtLoco1 ||
         objTela.LocotxtDiesel1 != objDadosAtualTela.LocotxtDiesel1 ||
         objTela.LocodtVencimento1 != objDadosAtualTela.LocodtVencimento1 ||
         objTela.LocordbCativas1 != objDadosAtualTela.LocordbCativas1 ||
         objTela.LocotxtOBSCativas1 != objDadosAtualTela.LocotxtOBSCativas1 ||
         objTela.LocotxtLoco2 != objDadosAtualTela.LocotxtLoco2 ||
         objTela.LocotxtDiesel2 != objDadosAtualTela.LocotxtDiesel2 ||
         objTela.LocodtVencimento2 != objDadosAtualTela.LocodtVencimento2 ||
         objTela.LocordbCativas2 != objDadosAtualTela.LocordbCativas2 ||
         objTela.LocotxtOBSCativas2 != objDadosAtualTela.LocotxtOBSCativas2 ||
         objTela.LocotxtLoco3 != objDadosAtualTela.LocotxtLoco3 ||
         objTela.LocotxtDiesel3 != objDadosAtualTela.LocotxtDiesel3 ||
         objTela.LocodtVencimento3 != objDadosAtualTela.LocodtVencimento3 ||
         objTela.LocordbCativas3 != objDadosAtualTela.LocordbCativas3 ||
         objTela.LocotxtOBSCativas3 != objDadosAtualTela.LocotxtOBSCativas3 ||
         objTela.LocotxtLoco4 != objDadosAtualTela.LocotxtLoco4 ||
         objTela.LocotxtDiesel4 != objDadosAtualTela.LocotxtDiesel4 ||
         objTela.LocodtVencimento4 != objDadosAtualTela.LocodtVencimento4 ||
         objTela.LocordbCativas4 != objDadosAtualTela.LocordbCativas4 ||
         objTela.LocotxtOBSCativas4 != objDadosAtualTela.LocotxtOBSCativas4 ||
         objTela.rdbListrdbTesteGradiente != objDadosAtualTela.rdbListrdbTesteGradiente ||
         objTela.rdbListrdbTesteVazamento != objDadosAtualTela.rdbListrdbTesteVazamento ||
         objTela.rdbListrdbTesteResposta != objDadosAtualTela.rdbListrdbTesteResposta ||
         objTela.rdbListrdbListaVagoes != objDadosAtualTela.rdbListrdbListaVagoes ||
         objTela.ListtxtNCruzamentosForaPO != objDadosAtualTela.ListtxtNCruzamentosForaPO ||
         objTela.ListtxtSupervisorNCruzamentoForaPO != objDadosAtualTela.ListtxtSupervisorNCruzamentoForaPO ||
         objTela.ListtxtMatriculaNCruzamentoForaPO != objDadosAtualTela.ListtxtMatriculaNCruzamentoForaPO ||
         objTela.ListtxtTotalCruzamentos != objDadosAtualTela.ListtxtTotalCruzamentos ||
         objTela.ListtxtQuantCruzamentosPararam != objDadosAtualTela.ListtxtQuantCruzamentosPararam ||
         objTela.PrevdtHoraChegadaLocoOrigem != objDadosAtualTela.PrevdtHoraChegadaLocoOrigem);
    return retorno;
}



    function CancelarCheckList() {

        if(SalvarC == 'False')
        {
            fecharJanela(true);
            return;
        }

        var exibeMSG =  ValidaCancelar();

         if (salvou == false && exibeMSG == true){
                   
                Ext.Msg.show({
                    title: 'Aviso',
                    msg: 'Existem alterações não salvas, ao fechar você irá perder estas informações. Deseja fechar mesmo assim?',
                    buttons: Ext.Msg.YESNO,
                    icon: Ext.MessageBox.WARNING,
                    fn: function (buttonValue) {
                        if (buttonValue == "yes") {
                            fecharJanela(true);

                        }
                    }
                });

            } else {

            fecharJanela(true);
//                Ext.Msg.show({
//                    title: 'Aviso',
//                    msg: 'Deseja fechar esta janela?',
//                    buttons: Ext.Msg.YESNO,
//                    icon: Ext.MessageBox.WARNING,
//                    fn: function (buttonValue) {
//                        if (buttonValue == "yes") {
//                            fecharJanela(false);
//                        }
//                    }
//                });

            }
        }

       function fecharJanela(refresh) {
       var conteudo_principal = parent.Ext.getCmp("conteudo-principal");
            if (refresh) {
                // Atualizar grid da tela de pesquisa
                
                for (var i = 0; i < conteudo_principal.items.items.length; i++) {
                    if (conteudo_principal.items.items[i].title == "Tela  - 1421") {
                        var frame = conteudo_principal.items.items[i].getFrameDocument();
                        var botao = $(frame).find('.clsBtnPesquisar');
                        $(botao).trigger("click");
                    }
                }
            }

            // Fechar janela atual
            var activeTab = conteudo_principal.getActiveTab();
            conteudo_principal.remove(activeTab.id, true);
        }

        function AtualizaPesquisaTelaBusca()
        {
            // Atualizar grid da tela de pesquisa
            var conteudo_principal = parent.Ext.getCmp("conteudo-principal");
            for (var i = 0; i < conteudo_principal.items.items.length; i++) {
                if (conteudo_principal.items.items[i].title == "Tela  - 1421") {
                                    
                    var frame = conteudo_principal.items.items[i].getFrameDocument();
                    var botao = $(frame).find('.clsBtnPesquisar');
                    $(botao).trigger("click");
                }
            }
        }

        function criarObjetoAbaCheckList() {
        var osChecklistPassageiroDto = new Object();

        //Tipo CHECKLIST
        osChecklistPassageiroDto.Ida = (Ext.getCmp("rdbTipo").items.items[0].checked == true ? true : false);
        osChecklistPassageiroDto.Volta = (Ext.getCmp("rdbTipo").items.items[1].checked == true ? true : false);
     
        osChecklistPassageiroDto.Usuario = Usuario;
        osChecklistPassageiroDto.Matricula = Matricula;

        osChecklistPassageiroDto.idOsChecklist = idOsCheckList;
        osChecklistPassageiroDto.idOs = idOs;
        osChecklistPassageiroDto.idOsChecklistPassageiro = idOsChecklistPassageiro;
      

        //CCO Circulação
            //Linha 1
            osChecklistPassageiroDto.CCOtxtSupervisorCCO = Ext.getCmp("txtSupervisorCCO").getRawValue();
            osChecklistPassageiroDto.CCOtxtMatriculaSupervisorCCO = Ext.getCmp("txtMatriculaSupervisorCCO").getRawValue();
            //Linha 2
            osChecklistPassageiroDto.CCOtxtControlador = Ext.getCmp("txtControlador").getRawValue();
            osChecklistPassageiroDto.CCOtxtMatriculaControlador = Ext.getCmp("txtMatriculaControlador").getRawValue();

        //EQUIPAGEM
            //Linha 1
            osChecklistPassageiroDto.EquiptxtMaquinista = Ext.getCmp("txtMaquinista").getValue();
            osChecklistPassageiroDto.EquiptxtMatriculaMaquinista = Ext.getCmp("txtMatriculaMaquinista").getValue();
            //Linha 2
            osChecklistPassageiroDto.EquiptxtAuxiliar = Ext.getCmp("txtAuxiliar").getValue();
            osChecklistPassageiroDto.EquiptxtMatriculaAuxiliar = Ext.getCmp("txtMatriculaAuxiliar").getValue();
            //Linha 3
            osChecklistPassageiroDto.EquiptxtSupervisorEquipagem = Ext.getCmp("txtSupervisorEquipagem").getValue();
            osChecklistPassageiroDto.EquiptxtMatriculaSupervisorEquipagem = Ext.getCmp("txtMatriculaSupervisorEquipagem").getValue();

            //Comentários Adicionais
            osChecklistPassageiroDto.txtComentAdicionais = Ext.getCmp("txtComentAdicionais").getValue();

        //VAGÕES
            //Linha 1
            osChecklistPassageiroDto.VagoestxtQTDVagoes = Ext.getCmp("txtQTDVagoes").getValue();
            osChecklistPassageiroDto.VagoestxtTB = Ext.getCmp("txtTB").getValue();
           
        //LOCOMOTIVAS
            //Linha 1
            osChecklistPassageiroDto.LocotxtLoco1 = Ext.getCmp("txtLoco1").getValue();
            osChecklistPassageiroDto.LocotxtDiesel1 = Ext.getCmp("txtDiesel1").getValue();
            
            osChecklistPassageiroDto.LocodtVencimento1 = Ext.getCmp("dtVencimento1").getValue();
            osChecklistPassageiroDto.LocordbCativas1 = (Ext.getCmp("rdbCativas1").items.items[0].checked == true ? true : Ext.getCmp("rdbCativas1").items.items[1].checked == true ? false : null);                                                
            osChecklistPassageiroDto.LocotxtOBSCativas1 = Ext.getCmp("txtOBSCativas1").getValue();
            //Linha 2
            osChecklistPassageiroDto.LocotxtLoco2 = Ext.getCmp("txtLoco2").getValue();
            osChecklistPassageiroDto.LocotxtDiesel2 = Ext.getCmp("txtDiesel2").getValue();
            osChecklistPassageiroDto.LocodtVencimento2 = Ext.getCmp("dtVencimento2").getValue();
            osChecklistPassageiroDto.LocordbCativas2 = (Ext.getCmp("rdbCativas2").items.items[0].checked == true ? true : Ext.getCmp("rdbCativas2").items.items[1].checked == true ? false : null);
            osChecklistPassageiroDto.LocotxtOBSCativas2 = Ext.getCmp("txtOBSCativas2").getValue();
            //Linha 3
            osChecklistPassageiroDto.LocotxtLoco3 = Ext.getCmp("txtLoco3").getValue();
            osChecklistPassageiroDto.LocotxtDiesel3 = Ext.getCmp("txtDiesel3").getValue();
            osChecklistPassageiroDto.LocodtVencimento3 = Ext.getCmp("dtVencimento3").getValue();
            osChecklistPassageiroDto.LocordbCativas3 = (Ext.getCmp("rdbCativas3").items.items[0].checked == true ? true : Ext.getCmp("rdbCativas3").items.items[1].checked == true ? false : null);
            osChecklistPassageiroDto.LocotxtOBSCativas3 = Ext.getCmp("txtOBSCativas3").getValue();
            //Linha 4
            osChecklistPassageiroDto.LocotxtLoco4 = Ext.getCmp("txtLoco4").getValue();
            osChecklistPassageiroDto.LocotxtDiesel4 = Ext.getCmp("txtDiesel4").getValue();
            osChecklistPassageiroDto.LocodtVencimento4 = Ext.getCmp("dtVencimento4").getValue();
            osChecklistPassageiroDto.LocordbCativas4 = (Ext.getCmp("rdbCativas4").items.items[0].checked == true ? true : Ext.getCmp("rdbCativas4").items.items[1].checked == true ? false : null);
            osChecklistPassageiroDto.LocotxtOBSCativas4 = Ext.getCmp("txtOBSCativas4").getValue();

            //CHECKLIST
            //CHK 1
            osChecklistPassageiroDto.rdbListrdbTesteGradiente = (Ext.getCmp("rdbTesteGradiente").items.items[0].checked == true ? true : Ext.getCmp("rdbTesteGradiente").items.items[1].checked == true ? false : null);
            //CHK 2
            osChecklistPassageiroDto.rdbListrdbTesteVazamento = (Ext.getCmp("rdbTesteVazamento").items.items[0].checked == true ? true : Ext.getCmp("rdbTesteVazamento").items.items[1].checked == true ? false : null);
            //CHK 3
            osChecklistPassageiroDto.rdbListrdbTesteResposta = (Ext.getCmp("rdbTesteResposta").items.items[0].checked == true ? true : Ext.getCmp("rdbTesteResposta").items.items[1].checked == true ? false : null);
            //CHK 4
            osChecklistPassageiroDto.rdbListrdbListaVagoes = (Ext.getCmp("rdbListaVagoes").items.items[0].checked == true ? true : Ext.getCmp("rdbListaVagoes").items.items[1].checked == true ? false : null);
            //TXT 5
            osChecklistPassageiroDto.ListtxtNCruzamentosForaPO = Ext.getCmp("txtNCruzamentosForaPO").getValue();
            //TXT 5.1
            osChecklistPassageiroDto.ListtxtSupervisorNCruzamentoForaPO = Ext.getCmp("txtSupervisorNCruzamentoForaPO").getValue();
            //TXT 5.2
            osChecklistPassageiroDto.ListtxtMatriculaNCruzamentoForaPO = Ext.getCmp("txtMatriculaNCruzamentoForaPO").getValue();
            //TXT 6
            osChecklistPassageiroDto.ListtxtTotalCruzamentos = Ext.getCmp("txtTotalCruzamentos").getValue();
            //TXT 7
            osChecklistPassageiroDto.ListtxtQuantCruzamentosPararam = Ext.getCmp("txtQuantCruzamentosPararam").getValue();

        //PREVISÃO
            //Linha 1
            osChecklistPassageiroDto.PrevdtHoraChegadaLocoOrigem = Ext.getCmp("horaChegadaLocoOrigem").getValue();
            //Linha 2
            osChecklistPassageiroDto.PrevtxtPrevisto = Ext.getCmp("txtdtPrevisto").getValue() + " " + Ext.getCmp("horaPrevisto").getValue();
            //Linha 3
            osChecklistPassageiroDto.PrevtxtReal = Ext.getCmp("txtdtReal").getValue() + " " + Ext.getCmp("horaReal").getValue();
            //Linha 4
            osChecklistPassageiroDto.DataChegadaReal = Ext.getCmp("txtdtChegadaReal").getValue() + " " + Ext.getCmp("horaChegadaReal").getValue();     

            return osChecklistPassageiroDto;
        }

        function criarObjetoAbaCheckListMequetrefe() {
            var osChecklistPassageiroDto = new Object();

            osChecklistPassageiroDto.idOsChecklist = idOsChecklist;
            osChecklistPassageiroDto.idOs = idOs;
            osChecklistPassageiroDto.idOsChecklistPassageiro = idOsChecklistPassageiro;
       
            //CCO Circulação
            //Linha 1
            osChecklistPassageiroDto.CCOtxtSupervisorCCO = Ext.getCmp("txtSupervisorCCO").getRawValue();
            osChecklistPassageiroDto.CCOtxtMatriculaSupervisorCCO = Ext.getCmp("txtMatriculaSupervisorCCO").getRawValue();
            //Linha 2
            osChecklistPassageiroDto.CCOtxtControlador = Ext.getCmp("txtControlador").getRawValue();
            osChecklistPassageiroDto.CCOtxtMatriculaControlador = Ext.getCmp("txtMatriculaControlador").getRawValue();

            //EQUIPAGEM
            //Linha 1
            osChecklistPassageiroDto.EquiptxtMaquinista = Ext.getCmp("txtMaquinista").getValue();
            osChecklistPassageiroDto.EquiptxtMatriculaMaquinista = Ext.getCmp("txtMatriculaMaquinista").getValue();
            //Linha 2
            osChecklistPassageiroDto.EquiptxtAuxiliar = Ext.getCmp("txtAuxiliar").getValue();
            osChecklistPassageiroDto.EquiptxtMatriculaAuxiliar = Ext.getCmp("txtMatriculaAuxiliar").getValue();
            //Linha 3
            osChecklistPassageiroDto.EquiptxtSupervisorEquipagem = Ext.getCmp("txtSupervisorEquipagem").getValue();
            osChecklistPassageiroDto.EquiptxtMatriculaSupervisorEquipagem = Ext.getCmp("txtMatriculaSupervisorEquipagem").getValue();

            //Comentários Adicionais
            osChecklistPassageiroDto.txtComentAdicionais = Ext.getCmp("txtComentAdicionais").getValue();

            //VAGÕES
            //Linha 1
            osChecklistPassageiroDto.VagoestxtQTDVagoes = Ext.getCmp("txtQTDVagoes").getValue();
            osChecklistPassageiroDto.VagoestxtTB = Ext.getCmp("txtTB").getValue();

            //LOCOMOTIVAS
            //Linha 1
            osChecklistPassageiroDto.LocotxtLoco1 = Ext.getCmp("txtLoco1").getValue();
            osChecklistPassageiroDto.LocotxtDiesel1 = Ext.getCmp("txtDiesel1").getValue();
            osChecklistPassageiroDto.LocodtVencimento1 = Ext.getCmp("dtVencimento1").getValue();
            osChecklistPassageiroDto.LocordbCativas1 = (Ext.getCmp("rdbCativas1").items[0].checked == true ? true : Ext.getCmp("rdbCativas1").items[1].checked == true ? false : null);
            osChecklistPassageiroDto.LocotxtOBSCativas1 = Ext.getCmp("txtOBSCativas1").getValue();
            //Linha 2
            osChecklistPassageiroDto.LocotxtLoco2 = Ext.getCmp("txtLoco2").getValue();
            osChecklistPassageiroDto.LocotxtDiesel2 = Ext.getCmp("txtDiesel2").getValue();
            osChecklistPassageiroDto.LocodtVencimento2 = Ext.getCmp("dtVencimento2").getValue();
            osChecklistPassageiroDto.LocordbCativas2 = (Ext.getCmp("rdbCativas2").items[0].checked == true ? true : Ext.getCmp("rdbCativas2").items[1].checked == true ? false : null);
            osChecklistPassageiroDto.LocotxtOBSCativas2 = Ext.getCmp("txtOBSCativas2").getValue();
            //Linha 3
            osChecklistPassageiroDto.LocotxtLoco3 = Ext.getCmp("txtLoco3").getValue();
            osChecklistPassageiroDto.LocotxtDiesel3 = Ext.getCmp("txtDiesel3").getValue();
            osChecklistPassageiroDto.LocodtVencimento3 = Ext.getCmp("dtVencimento3").getValue();
            osChecklistPassageiroDto.LocordbCativas3 = (Ext.getCmp("rdbCativas3").items[0].checked == true ? true : Ext.getCmp("rdbCativas3").items[1].checked == true ? false : null);
            osChecklistPassageiroDto.LocotxtOBSCativas3 = Ext.getCmp("txtOBSCativas3").getValue();
            //Linha 4
            osChecklistPassageiroDto.LocotxtLoco4 = Ext.getCmp("txtLoco4").getValue();
            osChecklistPassageiroDto.LocotxtDiesel4 = Ext.getCmp("txtDiesel4").getValue();
            osChecklistPassageiroDto.LocodtVencimento4 = Ext.getCmp("dtVencimento4").getValue();
            osChecklistPassageiroDto.LocordbCativas4 = (Ext.getCmp("rdbCativas4").items[0].checked == true ? true : Ext.getCmp("rdbCativas4").items[1].checked == true ? false : null);
            osChecklistPassageiroDto.LocotxtOBSCativas4 = Ext.getCmp("txtOBSCativas4").getValue();

            //CHECKLIST
            //CHK 1
            osChecklistPassageiroDto.rdbListrdbTesteGradiente = (Ext.getCmp("rdbTesteGradiente").items[0].checked == true ? true : Ext.getCmp("rdbTesteGradiente").items[1].checked == true ? false : null);
            //CHK 2
            osChecklistPassageiroDto.rdbListrdbTesteVazamento = (Ext.getCmp("rdbTesteVazamento").items[0].checked == true ? true : Ext.getCmp("rdbTesteVazamento").items[1].checked == true ? false : null);
            //CHK 3
            osChecklistPassageiroDto.rdbListrdbTesteResposta = (Ext.getCmp("rdbTesteResposta").items[0].checked == true ? true : Ext.getCmp("rdbTesteResposta").items[1].checked == true ? false : null);
            //CHK 4
            osChecklistPassageiroDto.rdbListrdbListaVagoes = (Ext.getCmp("rdbListaVagoes").items[0].checked == true ? true : Ext.getCmp("rdbListaVagoes").items[1].checked == true ? false : null);
            //TXT 5
            osChecklistPassageiroDto.ListtxtNCruzamentosForaPO = Ext.getCmp("txtNCruzamentosForaPO").getValue();
            //TXT 5.1
            osChecklistPassageiroDto.ListtxtSupervisorNCruzamentoForaPO = Ext.getCmp("txtSupervisorNCruzamentoForaPO").getValue();
            //TXT 5.2
            osChecklistPassageiroDto.ListtxtMatriculaNCruzamentoForaPO = Ext.getCmp("txtMatriculaNCruzamentoForaPO").getValue();
            //TXT 6
            osChecklistPassageiroDto.ListtxtTotalCruzamentos = Ext.getCmp("txtTotalCruzamentos").getValue();
            //TXT 7
            osChecklistPassageiroDto.ListtxtQuantCruzamentosPararam = Ext.getCmp("txtQuantCruzamentosPararam").getValue();
       
            //PREVISÃO
            //Linha 1
            osChecklistPassageiroDto.PrevdtHoraChegadaLocoOrigem = Ext.getCmp("horaChegadaLocoOrigem").getValue() != null ? Ext.getCmp("horaChegadaLocoOrigem").getValue() : "";
            //Linha 2
            osChecklistPassageiroDto.PrevtxtPrevisto = Ext.getCmp("txtdtPrevisto").getValue() && Ext.getCmp("horaPrevisto").getValue() != null ? Ext.getCmp("txtdtPrevisto").getValue() && Ext.getCmp("horaPrevisto").getValue() : "";
            //Linha 3
            osChecklistPassageiroDto.PrevtxtReal = Ext.getCmp("txtdtReal").getValue() && Ext.getCmp("horaReal").getValue() != null ? Ext.getCmp("txtdtReal").getValue() && Ext.getCmp("horaReal").getValue() : "";
            //Linha 4
            osChecklistPassageiroDto.DataChegadaReal = Ext.getCmp("txtdtChegadaReal").getValue() && Ext.getCmp("horaChegadaReal").getValue() != null ? Ext.getCmp("txtdtChegadaReal").getValue() && Ext.getCmp("horaChegadaReal").getValue() : "";
            //Tipo CHECKLIST
//            OsChecklistPassageiroDto.Ida = Ext.getCmp("rdbIDA").checked;
//            OsChecklistPassageiroDto.Volta = Ext.getCmp("rdbVOLTA").checked;

            return osChecklistPassageiroDto;
        }

        function isNumber(n) 
        {
            return !isNaN(parseFloat(n)) && isFinite(n);
        }

        function validaCampos()
        {
            var textoComentAdicionais = Ext.getCmp("txtComentAdicionais").getValue();
                if (textoComentAdicionais.length > 1000) {
                    Ext.Msg.show({
                        title: 'Aviso',
                        msg: 'Campo Comentários Adicionais não pode ter mais de 1000 caracteres.',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.WARNING
                    });
                    return;
                }

            var valor;
            //Diesel LOCO 1
            valor = Ext.getCmp("txtDiesel1").getRawValue().replace(",", "");
            if(valor != "")
            {
                if(!isNumber(valor))
                {
                        Ext.Msg.show({
                            title: "Alerta",
                            msg: "Campo Diesel Loco 1 apenas número!",
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.ERROR
                        });
                        return;
                }
            }
            //Diesel LOCO 2
            valor = Ext.getCmp("txtDiesel2").getRawValue().replace(",", "");
            if(valor != "")
            {
                if(!isNumber(valor))
                {
                        Ext.Msg.show({
                            title: "Alerta",
                            msg: "Campo Diesel Loco 2 apenas número!",
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.ERROR
                        });
                        return;
                }
            }
            //Diesel LOCO 3
            valor = Ext.getCmp("txtDiesel3").getRawValue().replace(",", "");
            if(valor != "")
            {
                if(!isNumber(valor))
                {
                        Ext.Msg.show({
                            title: "Alerta",
                            msg: "Campo Diesel Loco 3 apenas número!",
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.ERROR
                        });
                        return;
                }
            }
            //Diesel LOCO 4
            valor = Ext.getCmp("txtDiesel4").getRawValue().replace(",", "");
            if(valor != "")
            {
                if(!isNumber(valor))
                {
                        Ext.Msg.show({
                            title: "Alerta",
                            msg: "Campo Diesel Loco 4 apenas número!",
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.ERROR
                        });
                        Ext.getCmp("txtDiesel4").focus();
                        return;
                }
            }
            //5 - Nº cruzamentos fora do PO 00581
            valor = Ext.getCmp("txtNCruzamentosForaPO").getRawValue();
            if(valor != "")
            {
                if(!isNumber(valor))
                {
                        Ext.Msg.show({
                            title: "Alerta",
                            msg: "Campo Nº cruzamentos apenas número!",
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.ERROR
                        });
                        Ext.getCmp("txtNCruzamentosForaPO").focus();
                        return;
                }
            }
            //6 – Total de Cruzamentos?
            valor = Ext.getCmp("txtTotalCruzamentos").getRawValue();
            if(valor != "")
            {
                if(!isNumber(valor))
                {
                        Ext.Msg.show({
                            title: "Alerta",
                            msg: "Campo Total de Cruzamentos? apenas número!",
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.ERROR
                        });
                        return;
                }
            }
            //7 – Quantos pararam?
            valor = Ext.getCmp("txtQuantCruzamentosPararam").getRawValue();
            if(valor != "")
            {
                if(!isNumber(valor))
                {
                        Ext.Msg.show({
                            title: "Alerta",
                            msg: "Campo Quantos pararam? apenas número!",
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.ERROR
                        });
                        return;
                }
            }
            return true;
        }
    // FIM *****javascript*****  
</script>