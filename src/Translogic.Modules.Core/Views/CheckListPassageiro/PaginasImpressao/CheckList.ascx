﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%
    var itemOsChecklistExportacaoDto = ViewData["itemOsChecklistExportacaoDto"] as Translogic.Modules.Core.Domain.Model.Dto.ChecklistPassageiro.OsChecklistExportacaoDto;
    var Prefixo = itemOsChecklistExportacaoDto.Prefixo;
    var Controlador = itemOsChecklistExportacaoDto.Controlador != null ? (itemOsChecklistExportacaoDto.Controlador.Length > 30 ? itemOsChecklistExportacaoDto.Controlador.Substring(0, 30) : itemOsChecklistExportacaoDto.Controlador) : string.Empty;
    var Supervisor = itemOsChecklistExportacaoDto.Supervisor != null ? (itemOsChecklistExportacaoDto.Supervisor.Length > 30 ? itemOsChecklistExportacaoDto.Supervisor.Substring(0, 30) : itemOsChecklistExportacaoDto.Supervisor) : string.Empty;
    var QuantidadeVagoes = itemOsChecklistExportacaoDto.QuantidadeVagoes;
    var QuantidadeLocomotivas = itemOsChecklistExportacaoDto.QuantidadeLocomotivas;
    var Os = itemOsChecklistExportacaoDto.OS;
    var Tipo = (itemOsChecklistExportacaoDto.Tipo.Substring(0, 3)) == "Com" ? "Com Aprovação" : "Sem Aprovação";
    var TB = itemOsChecklistExportacaoDto.TB;
    //ABA Aprovações
    //Via
    var ViaAprovador = itemOsChecklistExportacaoDto.ViaAprovador;
    var ViaMatricula = itemOsChecklistExportacaoDto.ViaMatricula;
    var ViaData = itemOsChecklistExportacaoDto.ViaData;
    var ViaStatus = itemOsChecklistExportacaoDto.ViaStatus;
    var ViaJustificativa = itemOsChecklistExportacaoDto.ViaJustificativa == null ? string.Empty : (itemOsChecklistExportacaoDto.ViaJustificativa.Length > 35 ? itemOsChecklistExportacaoDto.ViaJustificativa.Substring(1, 35) : itemOsChecklistExportacaoDto.ViaJustificativa);
    //Locomotiva
    var LocomotivaAprovador = itemOsChecklistExportacaoDto.LocomotivaAprovador;
    var LocomotivaMatricula = itemOsChecklistExportacaoDto.LocomotivaMatricula;
    var LocomotivaData = itemOsChecklistExportacaoDto.LocomotivaData;
    var LocomotivaStatus = itemOsChecklistExportacaoDto.LocomotivaStatus;
    var LocomotivaJustificativa = itemOsChecklistExportacaoDto.LocomotivaJustificativa == null ? string.Empty : (itemOsChecklistExportacaoDto.LocomotivaJustificativa.Length > 35 ? itemOsChecklistExportacaoDto.LocomotivaJustificativa.Substring(1, 35) : itemOsChecklistExportacaoDto.LocomotivaJustificativa);
    //Tração
    var TracaoAprovador = itemOsChecklistExportacaoDto.TracaoAprovador;
    var TracaoMatricula = itemOsChecklistExportacaoDto.TracaoMatricula;
    var TracaoData = itemOsChecklistExportacaoDto.TracaoData;
    var TracaoStatus = itemOsChecklistExportacaoDto.TracaoStatus;
    var TracaoJustificativa = itemOsChecklistExportacaoDto.TracaoJustificativa == null ? string.Empty : (itemOsChecklistExportacaoDto.TracaoJustificativa.Length > 35 ? itemOsChecklistExportacaoDto.TracaoJustificativa.Substring(1, 35) : itemOsChecklistExportacaoDto.TracaoJustificativa);
    //Vagão
    var VagaoAprovador = itemOsChecklistExportacaoDto.VagaoAprovador;
    var VagaoMatricula = itemOsChecklistExportacaoDto.VagaoMatricula;
    var VagaoData = itemOsChecklistExportacaoDto.VagaoData;
    var VagaoStatus = itemOsChecklistExportacaoDto.VagaoStatus;
    var VagaoJustificativa = itemOsChecklistExportacaoDto.VagaoJustificativa == null ? string.Empty : (itemOsChecklistExportacaoDto.VagaoJustificativa.Length > 35 ? itemOsChecklistExportacaoDto.VagaoJustificativa.Substring(1, 35) : itemOsChecklistExportacaoDto.VagaoJustificativa);

    var MaquinistaNome = itemOsChecklistExportacaoDto.MaquinistaNome;
    var MaquinistaMatricula = itemOsChecklistExportacaoDto.MaquinistaMatricula;
    var AuxiliarNome = itemOsChecklistExportacaoDto.AuxiliarNome;
    var AuxiliarMatricula = itemOsChecklistExportacaoDto.AuxiliarMatricula;
    var SupervisorNome = itemOsChecklistExportacaoDto.SupervisorNome;
    var SupervisorMatricula = itemOsChecklistExportacaoDto.SupervisorMatricula;

    //ABA CheckList
    //Locomotiva 1
    var CodigoLoco1 = itemOsChecklistExportacaoDto.CodigoLoco1;
    var DieselLoco1 = itemOsChecklistExportacaoDto.DieselLoco1;
    var DtVencimentoLoco1 = itemOsChecklistExportacaoDto.DtVencimentoLoco1.HasValue ? itemOsChecklistExportacaoDto.DtVencimentoLoco1.Value.ToString("dd/MM/yyyy") : "";
    var CativasLoco1 = itemOsChecklistExportacaoDto.CativasLoco1 != null ? (itemOsChecklistExportacaoDto.CativasLoco1.Substring(0, 1) == "S" ? "Sim" : "Não") : ""; 
    var AdicionaisLoco1 = itemOsChecklistExportacaoDto.AdicionaisLoco1 == null ? string.Empty : (itemOsChecklistExportacaoDto.AdicionaisLoco1.Length > 30 ? itemOsChecklistExportacaoDto.AdicionaisLoco1.Substring(1, 30) : itemOsChecklistExportacaoDto.AdicionaisLoco1);

    //Locomotiva 2
    var CodigoLoco2 = itemOsChecklistExportacaoDto.CodigoLoco2;
    var DieselLoco2 = itemOsChecklistExportacaoDto.DieselLoco2;
    var DtVencimentoLoco2 = itemOsChecklistExportacaoDto.DtVencimentoLoco2.HasValue ? itemOsChecklistExportacaoDto.DtVencimentoLoco2.Value.ToString("dd/MM/yyyy") : "";
    var CativasLoco2 = itemOsChecklistExportacaoDto.CativasLoco2 != null ? (itemOsChecklistExportacaoDto.CativasLoco2.Substring(0, 1) == "S" ? "Sim" : "Não") : ""; 
    var AdicionaisLoco2 = itemOsChecklistExportacaoDto.AdicionaisLoco2 == null ? string.Empty : (itemOsChecklistExportacaoDto.AdicionaisLoco2.Length > 30 ? itemOsChecklistExportacaoDto.AdicionaisLoco2.Substring(1, 30) : itemOsChecklistExportacaoDto.AdicionaisLoco2);

    //Locomotiva 3
    var CodigoLoco3 = itemOsChecklistExportacaoDto.CodigoLoco3;
    var DieselLoco3 = itemOsChecklistExportacaoDto.DieselLoco3;
    var DtVencimentoLoco3 = itemOsChecklistExportacaoDto.DtVencimentoLoco3.HasValue ? itemOsChecklistExportacaoDto.DtVencimentoLoco3.Value.ToString("dd/MM/yyyy") : "";
    var CativasLoco3 = itemOsChecklistExportacaoDto.CativasLoco3 != null ? (itemOsChecklistExportacaoDto.CativasLoco3.Substring(0, 1) == "S" ? "Sim" : "Não") : "";
    var AdicionaisLoco3 = itemOsChecklistExportacaoDto.AdicionaisLoco3 == null ? string.Empty : (itemOsChecklistExportacaoDto.AdicionaisLoco3.Length > 30 ? itemOsChecklistExportacaoDto.AdicionaisLoco3.Substring(1, 30) : itemOsChecklistExportacaoDto.AdicionaisLoco3);


    //Locomotiva 4
    var CodigoLoco4 = itemOsChecklistExportacaoDto.CodigoLoco4;
    var DieselLoco4 = itemOsChecklistExportacaoDto.DieselLoco4;
    var DtVencimentoLoco4 = itemOsChecklistExportacaoDto.DtVencimentoLoco4.HasValue ? itemOsChecklistExportacaoDto.DtVencimentoLoco4.Value.ToString("dd/MM/yyyy") : "";
    var CativasLoco4 = itemOsChecklistExportacaoDto.CativasLoco4 != null ? (itemOsChecklistExportacaoDto.CativasLoco4.Substring(0, 1) == "S" ? "Sim" : "Não") : "";
    var AdicionaisLoco4 = itemOsChecklistExportacaoDto.AdicionaisLoco4 == null ? string.Empty : (itemOsChecklistExportacaoDto.AdicionaisLoco4.Length > 30 ? itemOsChecklistExportacaoDto.AdicionaisLoco4.Substring(1, 30) : itemOsChecklistExportacaoDto.AdicionaisLoco4);

    var Resposta1 = itemOsChecklistExportacaoDto.Resposta1 != null ? (itemOsChecklistExportacaoDto.Resposta1.Substring(0, 1) == "S" ? "Sim" : "Não") : "";
    var Resposta2 = itemOsChecklistExportacaoDto.Resposta2 != null ? (itemOsChecklistExportacaoDto.Resposta2.Substring(0, 1) == "S" ? "Sim" : "Não") : "";
    var Resposta3 = itemOsChecklistExportacaoDto.Resposta3 != null ? (itemOsChecklistExportacaoDto.Resposta3.Substring(0, 1) == "S" ? "Sim" : "Não") : "";
    var Resposta4 = itemOsChecklistExportacaoDto.Resposta4 != null ? (itemOsChecklistExportacaoDto.Resposta4.Substring(0, 1) == "S" ? "Sim" : "Não") : "";
    var Resposta5 = itemOsChecklistExportacaoDto.Resposta5;
    var Resposta6 = itemOsChecklistExportacaoDto.Resposta6;
    var Resposta7 = itemOsChecklistExportacaoDto.Resposta7;

    var ChegadaLocoOrigem = itemOsChecklistExportacaoDto.ChegadaLocoOrigem != null ? itemOsChecklistExportacaoDto.ChegadaLocoOrigem.Value.ToString("dd/MM/yyyy HH:mm:ss") : null;
    var Previsto = itemOsChecklistExportacaoDto.Previsto != null ? itemOsChecklistExportacaoDto.Previsto.Value.ToString("dd/MM/yyyy HH:mm:ss") : null;
    var Real = itemOsChecklistExportacaoDto.Real != null ? itemOsChecklistExportacaoDto.Real.Value.ToString("dd/MM/yyyy HH:mm:ss") : null;
    var ComentAdicionais = itemOsChecklistExportacaoDto.ComentAdicionais;
%>
<style type="text/css">
    .Cabecalhos
    {
        background-color: #002966;
        color: White;
        font-weight: bold;
        background: #002966;
        text-align: center;
    }
    .QuadroTop
    {
        text-align: left;
        font-weight: bold;
    }
    .style1
    {
        width: 1200px;
    }
    .style2
    {
        width: 250px;
    }
    .style8
    {
        width: 491px;
    }
    .style9
    {
        width: 484px;
    }
    .style11
    {
        width: 468px;
    }
    .style14
    {
        width: 35%;
    }
    .style15
    {
        width: 650px;
    }
    .style16
    {
        width: 102px;
    }
    .style17
    {
        width: 87px;
    }
    .style18
    {
        width: 466px;
    }
    .style19
    {
        height: 20px;
    }
    .style20
    {
        width: 466px;
        height: 20px;
    }
    .style21
    {
        width: 2747px;
        height: 19px;
    }
    .style22
    {
        width: 543px;
        height: 19px;
    }
    .style23
    {
        width: 2747px;
    }
    .style25
    {
        width: 400px;
    }
    .style27
    {
        width: 532px;
    }
    .style28
    {
        width: 460px;
    }
    .style29
    {
        width: 532px;
        height: 170px;
    }
    .style31
    {
        background-color: #002966;
        color: White;
        font-weight: bold;
        background: #002966;
        text-align: center;
        width: 399px;
    }
    .style32
    {
        width: 413px;
    }
    .style33
    {
        width: 305px;
    }
    .style34
    {
        width: 343px;
    }
    .style35
    {
        width: 361px;
    }
    .style36
    {
        width: 399px;
    }
</style>
<div style="padding: 10px 40px 0;">
    <table class="QuadroTop" border="0" width="100%" cellpadding="2" cellspacing="0"
        style="font-size: 11px; margin-top: 10px;">
        <tr>
            <td>
                Checklist Trem de Passageiros
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Prefixo:
                <%=Prefixo%>
            </td>
            <td class="style15">
            </td>
            <td class="style14">
                <%=DateTime.Now%>
            </td>
        </tr>
        <tr>
            <td class="style25">
                Controlador:
                <%=Controlador%>
            </td>
            <td class="style15">
            </td>
            <td class="style14">
                OS:
                <%=Os%>
            </td>
        </tr>
        <tr>
            <td class="style25">
                Supervisor:
                <%=Supervisor%>
            </td>
            <td class="style15">
            </td>
            <td class="style14">
                Tipo:
                <%=Tipo%>
            </td>
        </tr>
        <tr>
            <td class="style25">
                <%= QuantidadeVagoes != "" ? QuantidadeVagoes + " Vagões" : ""%>
                <%= QuantidadeLocomotivas != "" ? QuantidadeLocomotivas + " Locomotivas" : ""%>
            </td>
            <td class="style15">
            </td>
            <td class="style14">
                TB:
                <%= TB != "" ? TB + " ton" : ""%>
            </td>
        </tr>
    </table>
    <%if (ViaAprovador != null || LocomotivaAprovador != null || TracaoAprovador != null || VagaoAprovador != null)
      { %>
    <table border="1" width="100%" cellpadding="2" cellspacing="0" style="font-size: 11px;
        margin-top: 10px;">
        <thead>
            <tr class="Cabecalhos">
                <th class="style16">
                    &nbsp;
                </th>
                <th style="width: 16.6%">
                    Aprovadores
                </th>
                <th style="width: 16.6%">
                    Matricula
                </th>
                <th style="width: 16.6%">
                    Data
                </th>
                <th style="width: 16.6%">
                    Status
                </th>
                <th style="width: 16.6%">
                    Justificativa
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="Cabecalhos">
                    Via
                </td>
                <td>
                    <%=ViaAprovador%>
                </td>
                <td>
                    <%=ViaMatricula%>
                </td>
                <td>
                    <%=ViaData%>
                </td>
                <td>
                    <%=ViaStatus%>
                </td>
                <td>
                    <%=ViaJustificativa%>
                </td>
            </tr>
            <tr>
                <td class="Cabecalhos">
                    Locomotiva
                </td>
                <td>
                    <%=LocomotivaAprovador%>
                </td>
                <td>
                    <%=LocomotivaMatricula%>
                </td>
                <td>
                    <%=LocomotivaData%>
                </td>
                <td>
                    <%=LocomotivaStatus%>
                </td>
                <td>
                    <%=LocomotivaJustificativa%>
                </td>
            </tr>
            <tr>
                <td class="Cabecalhos">
                    Tração
                </td>
                <td>
                    <%=TracaoAprovador%>
                </td>
                <td>
                    <%=TracaoMatricula%>
                </td>
                <td>
                    <%=TracaoData%>
                </td>
                <td>
                    <%=TracaoStatus%>
                </td>
                <td>
                    <%=TracaoJustificativa%>
                </td>
            </tr>
            <tr>
                <td class="Cabecalhos">
                    Vagão
                </td>
                <td>
                    <%=VagaoAprovador%>
                </td>
                <td>
                    <%=VagaoMatricula%>
                </td>
                <td>
                    <%=VagaoData%>
                </td>
                <td>
                    <%=VagaoStatus%>
                </td>
                <td>
                    <%=VagaoJustificativa%>
                </td>
            </tr>
        </tbody>
    </table>
    <%}%>
    <table border="1" width="100%" cellpadding="2" cellspacing="0" style="font-size: 11px;
        margin-top: 10px;">
        <thead>
            <tr class="Cabecalhos">
                <th class="style17">
                    &nbsp;
                </th>
                <th class="style2">
                    &nbsp; Nome
                </th>
                <th style="width: 16.6%">
                    Matricula
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="Cabecalhos">
                    Maquinista
                </td>
                <td class="style20">
                    <%=MaquinistaNome%>
                </td>
                <td class="style19">
                    <%=MaquinistaMatricula%>
                </td>
            </tr>
            <tr>
                <td class="Cabecalhos">
                    Auxiliar
                </td>
                <td class="style18">
                    <%=AuxiliarNome%>
                </td>
                <td>
                    <%=AuxiliarMatricula%>
                </td>
            </tr>
            <tr>
                <td class="Cabecalhos">
                    Supervisor
                </td>
                <td>
                    <%=SupervisorNome%>
                </td>
                <td>
                    <%=SupervisorMatricula%>
                </td>
            </tr>
        </tbody>
    </table>
    <table border="1" width="100%" cellpadding="2" cellspacing="0" style="font-size: 11px;
        margin-top: 10px;">
        <thead>
            <tr class="Cabecalhos">
                <th class="style36">
                    &nbsp;
                </th>
                <th class="style35">
                    Código
                </th>
                <th class="style34">
                    Diesel
                </th>
                <th class="style33">
                    Vencimento
                </th>
                <th class="style32">
                    Cativas
                </th>
                <th class="style1">
                    Adicionais
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td rowspan="4" class="style31">
                    Locomotiva
                </td>
                <td class="style35">
                    <%=CodigoLoco1%>
                </td>
                <td class="style34">
                    <%=DieselLoco1%>
                </td>
                <td class="style33">
                    <%=DtVencimentoLoco1%>
                </td>
                <td class="style32">
                    <%=CativasLoco1%>
                </td>
                <td>
                    <%=AdicionaisLoco1%>
                </td>
            </tr>
            <tr>
                <td class="style35">
                    <%=CodigoLoco2%>
                </td>
                <td class="style34">
                    <%=DieselLoco2 %>
                </td>
                <td class="style33">
                    <%=DtVencimentoLoco2%>
                </td>
                <td class="style32">
                    <%=CativasLoco2%>
                </td>
                <td>
                    <%=AdicionaisLoco2%>
                </td>
            </tr>
            <tr>
                <td class="style35">
                    <%=CodigoLoco3%>
                </td>
                <td class="style34">
                    <%=DieselLoco3%>
                </td>
                <td class="style33">
                    <%=DtVencimentoLoco3%>
                </td>
                <td class="style32">
                    <%=CativasLoco3 %>
                </td>
                <td>
                    <%=AdicionaisLoco3 %>
                </td>
            </tr>
            <tr>
                <td class="style35">
                    <%=CodigoLoco4%>
                </td>
                <td class="style34">
                    <%=DieselLoco4%>
                </td>
                <td class="style33">
                    <%=DtVencimentoLoco4%>
                </td>
                <td class="style32">
                    <%=CativasLoco4 %>
                </td>
                <td>
                    <%=AdicionaisLoco4 %>
                </td>
            </tr>
        </tbody>
    </table>
    <table border="1" width="100%" cellpadding="2" cellspacing="0" style="font-size: 11px;
        margin-top: 10px;">
        <thead>
            <tr class="Cabecalhos">
                <th class="style23">
                    Perguntas
                </th>
                <th class="style2">
                    Resposta
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="style21" style="text-align: left; background-color: #002966; color: White;
                    font-weight: bold;">
                    1 - Teste Gradiente, Máximo 2 PSI. A Pressão da locomotiva deverá estar em 90 PSI
                    e a cauda no mínimo 88 PSI
                </td>
                <td class="style22" style="font-weight: normal;">
                    <%=Resposta1%>
                </td>
            </tr>
            <tr>
                <td class="style23" style="text-align: left; background-color: #002966; color: White;
                    font-weight: bold;">
                    2 - Teste de vazamento, Máximo 4 PSI
                </td>
                <td class="style11" style="font-weight: normal;">
                    <%=Resposta2%>
                </td>
            </tr>
            <tr>
                <td class="style23" style="text-align: left; background-color: #002966; color: White;
                    font-weight: bold;">
                    3 - Teste de resposta
                </td>
                <td class="style11" style="font-weight: normal;">
                    <%=Resposta3%>
                </td>
            </tr>
            <tr>
                <td class="style23" style="text-align: left; background-color: #002966; color: White;
                    font-weight: bold;">
                    4 - Lista de Vagões Físico x Sistema
                </td>
                <td class="style11" style="font-weight: normal;">
                    <%=Resposta4%>
                </td>
            </tr>
            <tr>
                <td class="style23" style="text-align: left; background-color: #002966; color: White;
                    font-weight: bold;">
                    5 - Nº cruzamentos fora do PO 00581
                </td>
                <td class="style11" style="font-weight: normal;">
                    <%=Resposta5%>
                </td>
            </tr>
            <tr>
                <td class="style23" style="text-align: left; background-color: #002966; color: White;
                    font-weight: bold;">
                    6 - Total de cruzamentos?
                </td>
                <td class="style11" style="font-weight: normal;">
                    <%=Resposta6%>
                </td>
            </tr>
            <tr>
                <td class="style23" style="text-align: left; background-color: #002966; color: White;
                    font-weight: bold;">
                    7 - Quantos cruzamentos pararam?
                </td>
                <td class="style11" style="font-weight: normal;">
                    <%=Resposta7%>
                </td>
            </tr>
        </tbody>
    </table>
    <table border="1" width="100%" cellpadding="2" cellspacing="0" style="font-size: 11px;
        margin-top: 10px;">
        <thead>
            <tr class="Cabecalhos">
                <th class="style27" style="color: White; font-weight: bold; background: #002966;
                    text-align: center;">
                    Chegada Loco na Origem
                </th>
                <th class="style9" rowspan="2" style="color: White; font-weight: bold; background: #002966;
                    text-align: center;">
                    Previsão
                </th>
                <th class="style8" style="color: White; font-weight: bold; background: #002966; text-align: center;">
                    Previsto
                </th>
                <th class="style28" style="color: White; font-weight: bold; background: #002966;
                    text-align: center;">
                    Real
                </th>
            </tr>
        </thead>
        <tbody>
            <tr style="border: none;">
                <td class="style27" style="font-weight: bold; text-align: center; font-weight: normal;"
                    rowspan="2">
                    <%=ChegadaLocoOrigem%>
                    <p>
                    </p>
                    &nbsp;<br />
                </td>
                <td class="style28" style="font-weight: bold; text-align: center; font-weight: normal;">
                    <%=Previsto%>
                    <p>
                    </p>
                    &nbsp;<br />
                </td>
                <td class="style28" style="font-weight: bold; text-align: center; font-weight: normal;">
                    <%=Real%>
                    <p>
                    </p>
                    &nbsp;<br />
                </td>
            </tr>
        </tbody>
    </table>
    <table border="1" width="100%" cellpadding="2" cellspacing="0" style="font-size: 11px;
        margin-top: 10px;">
        <thead>
            <tr class="Cabecalhos">
                <th>
                    Comentários Adicionais
                </th>
            </tr>
        </thead>
        <tbody>
            <tr style="border: none;">
                <td style="font-weight: bold; font-weight: normal;" rowspan="2">
                    <%=ComentAdicionais%>
                    <br />
                    <br />
                    <br />
                </td>
            </tr>
        </tbody>
    </table>
</div>
