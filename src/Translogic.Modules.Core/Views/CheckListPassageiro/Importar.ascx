﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>

<div id="div-importar">
</div>

 <script type="text/javascript">

     var idCheckList = '<%=ViewData["idCheckList"] %>';
     var idOs = '<%=ViewData["idOs"] %>';    
     var filtroOs = '<%=ViewData["numOs"] %>';
     var filtroDataInicial = '<%=ViewData["dataInicial"] %>';
     var filtroDataFinal = '<%=ViewData["dataFinal"] %>';
     var permissoes = '<%=ViewData["Permissoes"] != null ? ViewData["Permissoes"] as Translogic.Modules.Core.Domain.Model.Dto.ChecklistPassageiro.OsChecklistPermissaoDto : new Translogic.Modules.Core.Domain.Model.Dto.ChecklistPassageiro.OsChecklistPermissaoDto() %>';

     var situacaoVia = '<%=ViewData["situacaoVia"] %>';
     var situacaoLocomotiva = '<%=ViewData["situacaoLocomotiva"] %>';
     var situacaoTracao = '<%=ViewData["situacaoTracao"] %>';
     var situacaoVagao = '<%=ViewData["situacaoVagao"] %>';

    /**************
    ******STORE****/

     var AreaStore = new Ext.data.JsonStore({
         root: "Items",
         autoLoad: true,
         proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("ObterAreas", "CheckListPassageiro") %>', timeout: 600000 }),
         id: 'AreaStore',
         fields: [
                    'Id',
                    'Descricao'
                ]
     });

     /**************
     ****CAMPOS****/

     var hdnIdOs = { xtype: 'hidden', id: 'idOs', name: 'idOs', value: idOs };
     var hdnIdOsChecklist = { xtype: 'hidden', id: 'idOsChecklist', name: 'idOsChecklist', value: idCheckList };
     var hdndNumOs = { xtype: 'hidden', id: 'numOs', name: 'numOs', value: filtroOs };
     var hdndDataIni = { xtype: 'hidden', id: 'dataInicial', name: 'dataInicial', value: filtroDataInicial };
     var hdndDataFim = { xtype: 'hidden', id: 'dataFinal', name: 'dataFinal', value: filtroDataFinal };

     var rdbAprovarReprovar = {
         xtype: 'radiogroup',
         flex: 8,
         vertical: true,
         columns: 2,
         //labelWidth: 50,
         id: 'rdbAprovarReprovar',
         name: 'rdbAprovarReprovar',
         //fieldLabel: 'Aprovacao',
         width: 140,
         items: [{
             id: 'rdbAprovar',
             boxLabel: 'Aprovar',
             name: 'Aprovado',
             inputValue: 1
         }, {
             id: 'rdbReprovar',
             boxLabel: 'Reprovar',
             name: 'Aprovado',
             inputValue: 0
         }]
     };

     var frmRdbAprovarReprovar = {
         width: 150,
         layout: 'form',
         border: false,
         items: [rdbAprovarReprovar]
     };

     var ddlArea = new Ext.form.ComboBox({
         editable: false,
         typeAhead: true,
         forceSelection: true,
         disableKeyFilter: true,
         triggerAction: 'all',
         lazyRender: true,
         mode: 'local',
         store: AreaStore,
         valueField: 'Id',
         displayField: 'Descricao',
         fieldLabel: 'Área',
         id: 'Area',
         name: 'Area',
         allowBlank: false,
         blankText: 'Escolha uma área!',
         width: 100,
         alowBlank: false
     });

     var frmArea = {
         width: 110,
         layout: 'form',
         border: false,
         items: [ddlArea]
     };

     var fileupload = new Ext.ux.form.FileUploadField({
         id: 'btnImportarArquivo',
         allowBlank: false,
         name: 'documentURL',
         buttonText: 'Importar',
         grow: true,
         emptyText: 'arquivo e-mail',
        
         listeners: {
             afterrender: function () {

                 $("#btnImportarArquivo-file").change(function (event) {
                     var file = event.target.value;
                     var extension = file.split('.').pop();

                     if (extension != "msg") {
                         Ext.getCmp("btnImportarArquivo").setValue("");
                         Ext.Msg.show({
                             title: 'Aviso',
                             msg: 'Apenas extensão .msg é permitida para importação.',
                             buttons: Ext.Msg.OK,
                             icon: Ext.MessageBox.WARNING
                         });
                     } // if 
                 }); //change
             } //afterrender
         }//listeners
     });

     var frmbtnImportar = {
         width: 200,
         layout: 'form',
         border: false,
         items: [fileupload]
     };

     var formImportar = new Ext.form.FormPanel({
         id: 'formImportar',
         name: 'formImportar',
         width: 350,
         border: true,
         autoScroll: true,
         url: '<%= Url.Action("ConfirmaImpotacao", "CheckListPassageiro") %>',
         standardSubmit: true,
         bodyStyle: 'padding: 15px',
         labelAlign: 'top',
         items:
		[

             hdnIdOs, hdnIdOsChecklist, hdndNumOs, hdndDataIni, hdndDataFim,
             {
                 layout: 'column',
                 border: false,
                 items: [frmArea, frmRdbAprovarReprovar]
             },
             frmbtnImportar
		],
         buttonAlign: "center",
         buttons: [
            {
                text: 'Confirmar',
                type: 'submit',
                iconCls: 'icon-save',
                formBind: true,
                handler: function (b, e) {


                    var area = Ext.getCmp("Area").getValue();
                    var aprovado = Ext.getCmp("rdbAprovar").getRawValue();
                    var arquivo = Ext.getCmp("btnImportarArquivo").getValue();
                    var idOs = Ext.getCmp("idOs").getValue();

                    if (idOs == "") {

                        Ext.Msg.show({
                            title: 'Aviso',
                            msg: 'Id OS não encontrada.',
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.WARNING
                        });
                    }
                    else {

                        if (area == "" || aprovado == "" || arquivo == "" ||
                        (Ext.getCmp('rdbAprovarReprovar').items.items[0].checked == false && Ext.getCmp('rdbAprovarReprovar').items.items[1].checked == false)) {
                            Ext.Msg.show({
                                title: 'Aviso',
                                msg: 'Todos os campos são obrigatórios para importação.',
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.WARNING
                            });
                        }
                        else {
                            if (Ext.getCmp('rdbAprovarReprovar').items.items[1].checked == true && (situacaoVia == 'Aprovado' || situacaoVia == 'Aprovado - Importado') && area == 'via') 
                            {
                                Ext.Msg.show({
                                    title: 'Aviso',
                                    msg: 'Você não pode Reprovar Via Aprovada anteriormente!',
                                    buttons: Ext.Msg.OK,
                                    icon: Ext.MessageBox.WARNING
                                });
                                return;
                            }

                            if (Ext.getCmp('rdbAprovarReprovar').items.items[1].checked == true && (situacaoLocomotiva == 'Aprovado' || situacaoLocomotiva == 'Aprovado - Importado') && area == 'loc') 
                            {
                                Ext.Msg.show({
                                    title: 'Aviso',
                                    msg: 'Você não pode Reprovar Locomotiva Aprovada anteriormente!',
                                    buttons: Ext.Msg.OK,
                                    icon: Ext.MessageBox.WARNING
                                });
                                return;
                            }

                            if (Ext.getCmp('rdbAprovarReprovar').items.items[1].checked == true && (situacaoTracao == 'Aprovado' || situacaoTracao == 'Aprovado - Importado') && area == 'tra') 
                            {
                                Ext.Msg.show({
                                    title: 'Aviso',
                                    msg: 'Você não pode Reprovar Tração Aprovada anteriormente!',
                                    buttons: Ext.Msg.OK,
                                    icon: Ext.MessageBox.WARNING
                                });
                                return;
                            }

                            if (Ext.getCmp('rdbAprovarReprovar').items.items[1].checked == true && (situacaoVagao == 'Aprovado' || situacaoVagao == 'Aprovado - Importado') && area == 'vag') 
                            {
                                Ext.Msg.show({
                                    title: 'Aviso',
                                    msg: 'Você não pode Reprovar Vagão Aprovada anteriormente!',
                                    buttons: Ext.Msg.OK,
                                    icon: Ext.MessageBox.WARNING
                                });
                                return;
                            }

                            Ext.getBody().mask("Carregando arquivo, isso pode levar alguns minutos, por favor aguarde a finalização", "x-mask-loading");

                            var url = '<%= Url.Action("ConfirmaImpotacao", "CheckListPassageiro") %>';
                            $("#formImportar").find("form").attr("action", url);
                            $("#formImportar").find("form").attr("encoding", "multipart/form-data");

                            formImportar.getForm().submit();

                            //                            if ((Ext.getCmp('rdbAprovarReprovar').items.items[1].checked == true && situacaoVia == 'Aprovado') ||
                            //                            (Ext.getCmp('rdbAprovarReprovar').items.items[1].checked == true && situacaoLocomotiva == 'Aprovado') ||
                            //                            (Ext.getCmp('rdbAprovarReprovar').items.items[1].checked == true && situacaoTracao == 'Aprovado') ||
                            //                            (Ext.getCmp('rdbAprovarReprovar').items.items[1].checked == true && situacaoVagao == 'Aprovado')) {
                            //                                
                            //                            } else {
                            //                                Ext.getBody().mask("Carregando arquivo, isso pode levar alguns minutos, por favor aguarde a finalização", "x-mask-loading");

                            //                                var url = '<%= Url.Action("ConfirmaImpotacao", "CheckListPassageiro") %>';
                            //                                $("#formImportar").find("form").attr("action", url);
                            //                                $("#formImportar").find("form").attr("encoding", "multipart/form-data");

                            //                                formImportar.getForm().submit();
                            //                            }
                        }
                    }
                }
            }
            ,
            {
                text: 'Cancelar',
                type: 'submit',
                handler: function (b, e) {
                    window.wformImportar.close();
                }
            }
        ]
     });



    /******************************* RENDER *******************************/
    Ext.onReady(function () {

        formImportar.render("div-importar");
    });


 </script>
