﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Interna.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content runat="server" ID="Content1" ContentPlaceHolderID="HeadContent">

<%= this.Html.Partial("AbasChecklist/AbaAprovacoes") %>
<%= this.Html.Partial("AbasChecklist/AbaCheckList") %>

<%
    //Objeto Permissões
    var PermissoesDto = ViewData["Permissoes"] != null ? ViewData["Permissoes"] as Translogic.Modules.Core.Domain.Model.Dto.ChecklistPassageiro.OsChecklistPermissaoDto : new Translogic.Modules.Core.Domain.Model.Dto.ChecklistPassageiro.OsChecklistPermissaoDto(); ;
    
    var dadosAprovados = ViewData["Aprovacoes"] != null ? ViewData["Aprovacoes"] as Translogic.Modules.Core.Domain.Model.Dto.OsChecklistAprovadaDto : new Translogic.Modules.Core.Domain.Model.Dto.OsChecklistAprovadaDto();
    
    var statusVia = ViewData["Aprovacoes"] != null ? (dadosAprovados.ViaStatus == 'A' ? "Aprovado" : "Reprovado") : "";
    var statusLoco = ViewData["Aprovacoes"] != null ? (dadosAprovados.LocoStatus == 'A' ? "Aprovado" : "Reprovado") : "";
    var statusTrac = ViewData["Aprovacoes"] != null ? (dadosAprovados.TracStatus == 'A' ? "Aprovado" : "Reprovado") : "";
    var statusVag = ViewData["Aprovacoes"] != null ? (dadosAprovados.VagStatus == 'A' ? "Aprovado" : "Reprovado") : "";
    %>
<script type="text/javascript">

    //Permissões
    var AlteraStatusCheckList = <%=PermissoesDto.AlteraStatusCheckList.ToString().ToLower() %>;
    
    var aprovador = '<%=ViewData["Aprovador"] %>';
    var matriculaAprovador = '<%=ViewData["MatriculaAprovador"] %>';
    var idCheckList = '<%=ViewData["idCheckList"] %>';
    var idOs = '<%=ViewData["idOs"] %>';
    var numOs = '<%=ViewData["numOs"] %>';
        
    //Variaveis STATUS Aprovações
    var statusVia = '<%=statusVia%>';
    var statusLoco = '<%=statusLoco%>';
    var statusTrac = '<%=statusTrac%>';
    var statusVag = '<%=statusVag%>';

    var idOsChecklist = '<%=ViewData["idOsChecklist"]%>';

    var Ida = '<%=ViewData["Ida"] %>';
    var Volta = '<%=ViewData["Volta"]%>';
    
    /* 
        Finalizada a etapa de aprovações, a aba checklist será habilitada.
        Ponto de atenção: Lembrando que se o tipo do checklist for = Volta a aba deverá ser habilitada diretamente.

        RN14 – Se existir a reprovação de uma das áreas, a aba checklist não poderá ser habilitada, até que receba o status de aprovado

        Para a opção IDA, o sistema irá obrigar as aprovações antes de liberar o preenchimento do checklist.
        Se for selecionada a opção VOLTA, o sistema desconsidera a etapa de aprovações e libera direto o preenchimento do checklist.
        RN09 – Sempre carregar o checklist por default como IDA e permitir alteração.
        RN10 – O tipo de checklist deverá ser atualizado na tela de pesquisa sempre que for alterado.

     */

    // Todo: Habilitar IDA VOLTA se ja estiver tudo aprovado
    // Todo: Habilitar Cheklist se estiver tudo aprovado
    // Todo: Ao selecionar Volta e estiver aprovado habilita Checklist apenas

        
    var lblOS = {
        xtype: 'label',
        id: 'lblOS',
        text: 'OS : ' + numOs,
        margins: '0 0 0 0'
    };

    var fieldsetRBT = {
        xtype: 'radiogroup',
        flex: 8,
        vertical: true,
        columns: 2,
        labelWidth: 1,
        width: 250,
        id: 'rdbTipo',
        //fieldLabel: 'Tipo',
        disabled: idOsChecklist != 0 && AlteraStatusCheckList == false ? true : false,
        items: [{
            id: 'rdbIDA',
            checked: Ida == "True" ? true : false,
            boxLabel: 'Com Aprovação',
            name: 'rb-auto',
            inputValue: 1
        }, {
            id: 'rdbVOLTA',
            checked: Volta == "True" ? true : false,
            boxLabel: 'Sem Aprovação',
            name: 'rb-auto',
            inputValue: 2
        }],
        listeners:
            {
                change: function (obj, checked) {

                    //                    if (obj.items.items[0].checked == true) 
                    //                    {
                    //                        Ext.getCmp("AbaAprovacoes").setDisabled(false);
                    //                                        
                    //                        if (statusVia != "Aprovado" && statusLoco != "Aprovado" && statusTrac != "Aprovado" && statusVag != "Aprovado")
                    //                            {
                    //                                Ext.getCmp("AbaChecklist").setDisabled(true);
                    //                            }
                    //                        Ext.getCmp("tabPanelGeral").setActiveTab(0);
                    //                    }
                    //                    if (obj.items.items[1].checked == true) 
                    //                    {
                    //                        Ext.getCmp("AbaChecklist").setDisabled(false);
                    //                        Ext.getCmp("AbaAprovacoes").setDisabled(true);
                    //                        Ext.getCmp("tabPanelGeral").setActiveTab(1);
                    //                    }
                    VerificaTipoCheckList();
                }

            }
    };

    var frmlbl = {
        layout: 'form',
        border: false,
        autoWidth: true,
        items: [lblOS]
    };

    var frmfieldsetRBT = {
        layout: 'form',
        border: false,
        autoWidth: true,
        items: [fieldsetRBT]
    };

    var col1 = {
        layout: 'column',
        border: false,
        autoWidth: true,
        items: [frmlbl, frmfieldsetRBT]
    };

    var tabs = new Ext.TabPanel({
        id: 'tabPanelGeral',
        activeTab: 0,
        region: 'center',
        deferredRender: false,
        autoTabs: true,
        autoHeight: true,
        items: [
                    {
                        id: 'AbaAprovacoes',
                        title: 'Aprovações',
                        autoHeight: true,
                        items: [geral.AbaAprovacoes.getForm()]
                    },
                    {
                        title: 'CheckList',
                        id: 'AbaChecklist',
                        autoHeight: true,
                        disabled: true,
                        items: [geral.AbaCheckList.getForm()]
                    }
                ]
    });


    Ext.onReady(function () {

        new Ext.Viewport({
            labelAlign: 'top',
            region: 'center',
            layout: 'border',
            border: false,
            items: [
                        {
                            region: 'center',
                            autoScroll: true,
                            items:
                            [
                                {
                                    region: 'center',
                                    applyTo: 'header-content'
                                },
                                {
                                    xtype: 'hidden', id: 'sindicanciaSalva', value: 'false'
                                },
                                col1,
                                tabs
                            ]
                        }
                    ],
            listeners:
            {
                afterrender: function (index) {
                    //Via
                    $("#x-form-el-txtObservacoesVia").removeClass("x-form-element x-form-element");
                    $("#x-form-el-txtObservacoesVia").removeAttr('style');
                    //Locomotiva
                    $('#frmBotoesVia').attr('style', 'margin-top:45px;margin-left:5px');

                    //Locomotiva
                    $("#x-form-el-txtObservacoesLocomotiva").removeClass("x-form-element x-form-element");
                    $("#x-form-el-txtObservacoesLocomotiva").removeAttr('style');
                    //Locomotiva
                    $('#frmBotoesLocomotiva').attr('style', 'margin-top:45px;margin-left:5px');

                    //Tracão
                    $("#x-form-el-txtObservacoesTracao").removeClass("x-form-element x-form-element");
                    $("#x-form-el-txtObservacoesTracao").removeAttr('style');
                    //Locomotiva
                    $('#frmBotoesTracao').attr('style', 'margin-top:45px;margin-left:5px');

                    //Vagão
                    $("#x-form-el-txtObservacoesVagao").removeClass("x-form-element x-form-element");
                    $("#x-form-el-txtObservacoesVagao").removeAttr('style');
                    //Locomotiva
                    $('#frmBotoesVagao').attr('style', 'margin-top:45px;margin-left:5px');

                    //Alinhamento botões RODAPÉ
                    //$('#linhaBotoesRodape').attr('style', 'margin-left:467px');
                    
                    VerificaTipoCheckList();
                }
            }
        });
    });

    function VerificaTipoCheckList() 
    {
        if (Ext.getCmp("rdbTipo").items.items[0].checked == true) {
            Ext.getCmp("AbaAprovacoes").setDisabled(false);

            if (statusVia != "Aprovado" && statusLoco != "Aprovado" && statusTrac != "Aprovado" && statusVag != "Aprovado") {
                Ext.getCmp("AbaChecklist").setDisabled(true);
            }
            Ext.getCmp("tabPanelGeral").setActiveTab(0);
        }
        if (Ext.getCmp("rdbTipo").items.items[1].checked == true) {
            Ext.getCmp("AbaChecklist").setDisabled(false);
            Ext.getCmp("AbaAprovacoes").setDisabled(true);
            Ext.getCmp("tabPanelGeral").setActiveTab(1);
        }
    }

</script>
       
</asp:Content>
<asp:Content runat="server" ID="Content2" ContentPlaceHolderID="Content">
 <div  id="header-content">
        <h1>
            Checklist de passageiros</h1>
        <small>Você está em Operação > Pesquisa de Checklist de passageiros > Aprovações/Checklist</small>
        <br />
    </div>
</asp:Content>
