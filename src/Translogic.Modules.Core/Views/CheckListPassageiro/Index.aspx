﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Interna.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content runat="server" ID="HeadContent" ContentPlaceHolderID="HeadContent">
    <%
        var permissoes = ViewData["Permissoes"] != null ? ViewData["Permissoes"] as Translogic.Modules.Core.Domain.Model.Dto.ChecklistPassageiro.OsChecklistPermissaoDto : new Translogic.Modules.Core.Domain.Model.Dto.ChecklistPassageiro.OsChecklistPermissaoDto();
        var DataInicial = ViewData["DataInicial"] != null ? ViewData["DataInicial"].ToString() : string.Empty;
        var DataFinal = ViewData["DataFinal"] != null ? ViewData["DataFinal"].ToString() : string.Empty;
        var NumOs = ViewData["NumOs"] != null ? ViewData["NumOs"].ToString() : string.Empty;
        var mensagens = ViewData["Mensagens"] != null ? ViewData["Mensagens"].ToString() : string.Empty; 
    %>
    <script type="text/javascript">
        var arrGridItens = [];
        var IdOsCheckList = 0;
        var IdOsCheckListAprovacao = 0;
        var concluido = 0;
        var id
        var idOs = 0;
        var url = "";
        var formModal = null;
        var importar   =  "";
        var editar     = "";
        var pesquisar  = "";
        var gerar      = "";
        var visualizar = "";
        var exportar   = "";
        

        function ValidarPermissoes(){
        
            importar   = '<%=permissoes.Importar %>';
            editar     = '<%=permissoes.EditarChecklist %>';
            pesquisar  = '<%=permissoes.Pesquisar %>';
            gerar      = '<%=permissoes.Gerar %>';
            visualizar = '<%=permissoes.Visualizar %>';
            exportar   = '<%=permissoes.Exportar %>';

            if (pesquisar == true) {
                Ext.getCmp("btnPesquisar").setDisabled(false);
            }
        }

        var sm = new Ext.grid.CheckboxSelectionModel({});
         
         function modalImportar(){

            var selection = sm.getSelections();
            var umaOsSelecionada = selection.length == 1 ? true : false;

            if (umaOsSelecionada){
                
                if (selection && selection.length > 0) {
                    idOs = selection[0].data.IdOs;
                    IdOsCheckList = selection[0].data.IdOsCheckList;
                   
                    selection = sm.getSelections();

                    $.each(selection, function (i, e) {
                        idOs = e.data.IdOs;
                        IdOsCheckList = e.data.IdOsCheckList;
                    });
                }

                if (idOs == '0' && IdOsCheckList == '0')
                {
                    Ext.Msg.show({
                        title: 'Aviso',
                        msg: 'Selecione uma OS para realizar importação.',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.WARNING
                    });
                }
                else {

                    var dataInicial = Ext.getCmp("txtDataInicial").getRawValue();
                    var dataFinal = Ext.getCmp("txtDataFinal").getRawValue();
                    var numOs = Ext.getCmp("txtNumOs").getValue(); 

                    var situacaoVia = selection[0].data.Via;
                    var situacaoLocomotiva = selection[0].data.Locomotiva;
                    var situacaoTracao = selection[0].data.Tracao;
                    var situacaoVagao = selection[0].data.Vagao;

                    formModal = new Ext.Window({
                        id: 'formModalImportar',
                        title: 'Importar',
                        modal: true,
                        width: 350,
                        height: 210,
                        resizable: false,
                        autoScroll: false,
                        autoLoad:
                        {
                            url: '<%=Url.Action("Importar")%>',
                            params: { idOs : idOs, IdOsCheckList : IdOsCheckList, numOs : numOs, dataInicial : dataInicial, dataFinal : dataFinal, situacaoVia : situacaoVia, situacaoLocomotiva : situacaoLocomotiva, situacaoTracao : situacaoTracao, situacaoVagao : situacaoVagao },
                            text: "Abrindo importação...",
                            scripts: true,
                            callback: function(el, sucess) {
                                if (!sucess) {
                                    formModal.close();
                                }
                            }
                        },
                        listeners: {  
                                resize: function(win, width, height, eOpts) {
                                    win.center();
                                }
                        }
                    });
                    window.wformImportar = formModal;
                    formModal.show(this);
               }//else
           }//if umaOsSelecionada
        }

//        var checkIda = new Ext.grid.CheckColumn({
//            dataIndex: 'Ida',
//            header: "Ida",
//            id: 'checkIda',
//            name: 'checkIda',           
//            sortable: false,            
//            disabled: true,
//            width: 10,
//            checkOnly: false            
//        });


//        var checkVolta = new Ext.grid.CheckColumn({
//            dataIndex: 'Volta',
//            header: 'Volta',
//            id: 'checkVolta',
//            sortable: false,
//            disabled: true,
//            width: 20
//        });

        //#Region StoreGridChecklist

        var gridCheckListStore = new Ext.data.JsonStore({
            root: "Items",
            totalProperty: 'Total',
            id: 'gridCheckListStore',
            name: 'gridCheckListStore',
            proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("ObterConsultaOsCheckList","CheckListPassageiro") %>', timeout: 600000 }),
            paramNames: {
                sort: "detalhesPaginacaoWeb.Sort",
                dir: "detalhesPaginacaoWeb.Dir",
                start: "detalhesPaginacaoWeb.Start",
                limit: "detalhesPaginacaoWeb.Limit"
            },
            fields: ['OS', 'Prefixo', 'Origem', 'Destino', 'Data', 'Via', 'Locomotiva', 'Tracao', 'Vagao','Controlador', 'Ida', 'Volta', 'IdOsCheckList', 'IdOs', 'IdOsCheckListAprovacao', 'HabilitaEditar', 'Concluido']
        });

        //#endRegion

        //#Region PainelChecklist GRID

        $(function () {

            // Toolbar da Grid
            var pagingToolbar = new Ext.PagingToolbar({
                pageSize: 50,
                store: gridCheckListStore,
                displayInfo: true,
                displayMsg: App.Resources.Web.GridExibindoRegistros,
                emptyMsg: App.Resources.Web.GridSemRegistros,
                paramNames: {
                    start: "detalhesPaginacaoWeb.Start",
                    limit: "detalhesPaginacaoWeb.Limit"
                }
            });

            var Actions = new Ext.ux.grid.RowActions({
                id: '',
                dataIndex: '',
                header: '',
                align: 'center',
                autoWidth: false,
                width: 10,
                actions: [
                            { iconCls: 'icon-edit', tooltip: 'Editar' , hideIndex: 'HabilitaEditar' }
                         ],
                        callbacks: {
                                   'icon-edit': function (grid, record, action, row, col){
                                        
                                        idOs = record.data.IdOs;
                                        IdOsCheckList = record.data.IdOsCheckList;  
                                        IdOsCheckListAprovacao = record.data.IdOsCheckListAprovacao;
                                        Ext.getBody().mask("Processando dados...", "x-mask-loading");
                                        EditarChecklist(idOs,IdOsCheckList, IdOsCheckListAprovacao);                   
                                 }
                            }
            });


            var cm = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true
                },
                plugins: Actions,
                columns: [
                    new Ext.grid.RowNumberer(),
                    sm,
                    Actions,
                    { header: 'OS', dataIndex: "OS", sortable: false, width: 20 },
                    { header: 'Prefixo', dataIndex: "Prefixo", sortable: false, width: 20 },
                    { header: 'Origem', dataIndex: "Origem", sortable: false, width: 20 },
                    { header: 'Destino', dataIndex: "Destino", sortable: false, width: 20 },
                    { header: 'Data', dataIndex: "Data", sortable: false, width: 30 },
                    { header: 'Via', dataIndex: "Via", sortable: false, width: 27 },
                    { header: 'Locomotiva', dataIndex: "Locomotiva", sortable: false, width: 27 },
                    { header: 'Tração', dataIndex: "Tracao", sortable: false, width: 27 },
                    { header: 'Vagão', dataIndex: "Vagao", sortable: false, width: 27 },
                    { header: 'Controlador', dataIndex: "Controlador", sortable: false, width: 40 },
                    { header: 'IdOsCheckList', dataIndex: "IdOsCheckList", sortable: false, width: 40, hidden: true  },
                    { header: 'IdOs', dataIndex: "IdOs", sortable: false, width: 40, hidden: true  },
                    { header: 'IdOsCheckListAprovacao', dataIndex: "IdOsCheckListAprovacao", sortable: false, width:10, hidden: true  },
                    { header: 'Concluido', dataIndex: "Concluido", sortable: false, width:10, hidden:true },
                    { header: 'Com Aprovação', dataIndex: "Ida", sortable: false, width:20,  
                        renderer: function(value) {
                            
                            if (value == '1')
                                return "<img src='/Get.efh?file={Translogic.Core.Infrastructure.Web;}/Content/Images/Icons/accept.png' qtip='Ida' alt='Ida' />";
                            else 
                                return "";
                         }
                    },
                    { header: 'Sem Aprovação', dataIndex: "Volta", sortable: false, width:25,  
                        renderer: function(value) {
                            
                            if (value == '1')
                                return "<img src='/Get.efh?file={Translogic.Core.Infrastructure.Web;}/Content/Images/Icons/accept.png' qtip='Volta' alt='Volta' />";
                            else 
                                return "";
                         }
                    }                     
                ]
            });

            var gridChecklist = new Ext.grid.EditorGridPanel({
                id: 'gridChecklist',
                name: 'gridChecklist',
                autoLoadGrid: true,
                height: 360,
                limit: 10,
                width: 870,
                stripeRows: true,
                cm: cm,
                sm: sm,
                region: 'center',
                viewConfig: {
                    forceFit: true,
                    emptyText: 'Não possui dado(s) para exibição.'
                },
                autoScroll: true,
                loadMask: { msg: App.Resources.Web.Carregando },
                store: gridCheckListStore,
                tbar: [
                    {
                        id: 'btnExportar',
                        text: 'Exportar',
                        tooltip: 'Exportar',
                        disabled: true,
                        iconCls: 'icon-compress',
                        handler: function (c) 
                        {
                                // Busca idOS linha selecionada
                                var idsOs = BuscaIDSselecionados();
                                var url = '<%= Url.Action("ExportarZIP", "CheckListPassageiro") %>';
                                url += "?idsOS=" + idsOs;
                                window.open(url, "");
                        }
                    }
                ],
                bbar: pagingToolbar,
                sm: sm,
                listeners: {
                     
                    cellclick: function (e, rowIndex, columnIndex) {

                            record = gridChecklist.getStore().getAt(rowIndex);
                            IdOsCheckList = record.data.IdOsCheckList;
                            idOs = record.data.IdOs;

                            concluido = record.data.Concluido;
                            VerificaSelecionadosHabilitaBtns();
                    }
                },
                plugins: [Actions]
            });

            gridChecklist.getStore().proxy.on('beforeload', function (p, params) {
                
                params['dataInicial'] = Ext.getCmp("txtDataInicial").getRawValue();
                params['dataFinal'] = Ext.getCmp("txtDataFinal").getRawValue();
                params['numOs'] = Ext.getCmp("txtNumOs").getValue(); 
                
            });
             

            //#endRegion

            //#Region PesquisaFiltros
            var txtDataInicial = new Ext.form.DateField({
                fieldLabel: 'Data Inicial',
                id: 'txtDataInicial',
                name: 'txtDataInicial',
                dateFormat: 'd/n/Y',
                width: 83,
                plugins: [new Ext.ux.InputTextMask('99/99/9999', true)]                
            });

            var txtDataFinal = new Ext.form.DateField({
                fieldLabel: 'Data Final',
                id: 'txtDataFinal',
                name: 'txtDataFinal',
                dateFormat: 'd/n/Y',
                width: 83,
                plugins: [new Ext.ux.InputTextMask('99/99/9999', true)]
            });

            var txtNumOs = {
                xtype: 'numberfield',
                name: 'txtNumOs',
                id: 'txtNumOs',
                fieldLabel: 'OS',
                allowDecimals: false,
                decimalSeparator: ',',
                autoCreate: {
                    tag: 'input',
                    type: 'text',
                    autocomplete: 'off',
                    maxlength: '7'
                },
                width: 85,
                enableKeyEvents: true,
                listeners: {
                    keyup: function (field, e) {
                        field.setValue(field.getRawValue().replace("'", ""));
                    }
                }

            };


            //Item do Array de Componentes (1 por cada componente criado)
            var arrDataInicial = {
                width: 100,
                layout: 'form',
                border: false,
                items: [txtDataInicial]
            };

            var arrDataFinal = {
                width: 100,
                layout: 'form',
                border: false,
                items: [txtDataFinal]
            };

            var arrNumOs = {
                width: 100,
                layout: 'form',
                border: false,
                items: [txtNumOs]
            };

            //Array de componentes do form 
            var arrCampos = {
                layout: 'column',
                border: false,
                items: [arrDataInicial, arrDataFinal, arrNumOs]
            };

            //Conteudo do Form que sera criado
            var filtros = new Ext.form.FormPanel({
                id: 'grid-filtros',
                title: "Filtros",
                region: 'center',
                bodyStyle: 'padding:15px;',
                layout: 'form',
                labelAlign: 'top',
                items: [arrCampos],
                buttonAlign: "center",
                buttons:
                    [{
                        text: 'Pesquisar',
                        id: 'btnPesquisar',
                        type: 'submit',
                        cls : 'clsBtnPesquisar', 
                        iconCls: 'icon-find',
                        handler: function (b, e) {
                            Pesquisar();
                        }
                    },
                        {
                            text: 'Limpar',
                            id: 'btnLimpar',
                            type: 'submit',
                            handler: function (b, e) {
                                Limpar();
                            }
                        },
                        {
                            text: 'Gerar/Abrir',
                            id: 'btnGerar',
                            type: 'submit',
                            disabled: true,
                            handler: function (b, e) {
                                     abrirAbaChecklist(false);
                            }
                        },
                        {
                              text: 'Visualizar',
                              id: 'btnVisualizar',
                              type: 'submit',
                              disabled: true,
                              handler: function (b, e) {      
                                    abrirAbaChecklist(true);
                              }
                          },
                           {
                               text: 'Importar',
                               id:'btnImpotar' ,
                               type: 'submit',
                               disabled: true,
                               handler: function (b, e) {
                                   modalImportar();
                               
                           }            
                    }]
            });

            //#endRegion
            new Ext.Viewport({
                layout: 'border',
                margins: 10,
                items: [
                    {
                        region: 'north',
                        height: 200,
                        autoScroll: true,
                        items: [{
                            region: 'center',
                            applyTo: 'header-content'
                        },
                            filtros]
                    },
                    gridChecklist
                ]
            });

            //#Region FuncoesDaTela

            function EditarChecklist(idOs,IdOsCheckList, IdOsCheckListAprovacao) {
                Ext.getBody().unmask();
                parent.layoutPrincipal.menuLateral.abrirTelaEmAba("Checklist Passageiros", "<%=Url.Action("Checklist","CheckListPassageiro") %>" + "?idOs=" + idOs + "&IdOsCheckList=" + IdOsCheckList +  "&IdOsCheckListAprovacao=" + IdOsCheckListAprovacao);
                //window.location = url;
            }

            function abrirAbaChecklist(somenteVisualizar){
            var idOs = BuscaIDOrdemServicoSelecionado();

                 var url = "<%=Url.Action("Checklist","CheckListPassageiro") %>" + "?idOs=" + idOs + "&IdOsCheckList=" + IdOsCheckList;

                 if (somenteVisualizar)
                    url+= "&visualizar=sim";
                 
                 parent.layoutPrincipal.menuLateral.abrirTelaEmAba("Checklist Passageiros", url);
            }

            function comparaDatas(dataIni, dataFim) {

                var arrDtIni = dataIni.split('/');
                var arrDtFim = dataFim.split('/');

                var dtIni = new Date(arrDtIni[2] + '/' + arrDtIni[1] + '/' + arrDtIni[0]);
                var dtFim = new Date(arrDtFim[2] + '/' + arrDtFim[1] + '/' + arrDtFim[0]);

                return dtIni <= dtFim;
            }

            function ValidarFiltros() {
                
                var dtI = Ext.getCmp("txtDataInicial").getRawValue();
                var dtF = Ext.getCmp("txtDataFinal").getRawValue();
                var numOs = Ext.getCmp("txtNumOs").getRawValue();

                if (dtI == "" &&
	                dtF == "" &&
	                numOs == "") {
                    Ext.Msg.show({
                        title: 'Aviso',
                        msg: 'Selecione ou preencha no mínimo um filtro de pesquisa.!',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.ERROR
                    });
                    return false;
                }

                if (dtI != "" && dtF != "") {
                    if (!comparaDatas(dtI, dtF)) {
                        Ext.Msg.show({
                            title: 'Aviso',
                            msg: ' Data Posterior deve ser superior a data anterior nos filtros.',
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.ERROR
                        });
                        return false;
                    }
                }
                return true;
            }

            function Pesquisar() {
                
                desabilitaTodosBotoes();

                if (ValidarFiltros()) {
                
                    gridCheckListStore.load({ params: { start: 0, limit: 50} });
                    
                }
            }

            function Limpar() {
                gridCheckListStore.removeAll();
                Ext.getCmp("grid-filtros").getForm().reset();
                $("#txtNumOs").val("");
                ColocarHojeEmDatas();
            }

            // Busca e armazena lista de ids de vagões
            function BuscaIDOrdemServicoSelecionado() {
                var selection = sm.getSelections();
                var id;
                if (selection && selection.length > 0) {
                    var IdOs = selection[0].data.IdOs;
                    var listaOS = [];

                    selection = sm.getSelections();

                    $.each(selection, function (i, e) {
                        id = e.data.IdOs;
                    });
                }

                return id;
            }
            //#endRegion

        });

        var ColocarHojeEmDatas = function () {
            var today = new Date();
            var dd = today.getDate();
            var dAnterior = today.getDate() - 1;
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();

            if (dd < 10) {
                dd = '0' + dd
            }

            if (dAnterior < 10) {
                dAnterior = '0' + dAnterior;
            }

            if (mm < 10) {
                mm = '0' + mm;
            }

            today = dd + '/' + mm + '/' + yyyy;
            var yesterday = dAnterior + '/' + mm + '/' + yyyy;
            Ext.getCmp("txtDataInicial").setValue(yesterday);
            Ext.getCmp("txtDataFinal").setValue(today);
        }

       function ConfirmacaoImportacao() {
        
         var dataInicial = '<%=DataInicial%>';
         var dataFinal = '<%=DataFinal%>';
         var numOs = '<%=NumOs%>';
         var mensagens = '<%=mensagens %>';
       
         if (dataInicial != "" || dataFinal != "" || numOs != ""){
             if (dataInicial != "")
                Ext.getCmp("txtDataInicial").setValue(dataInicial);
             if (dataFinal != "")
                Ext.getCmp("txtDataFinal").setValue(dataFinal);
             if (numOs != "")
                Ext.getCmp("txtNumOs").setValue(numOs);

              gridCheckListStore.load({ params: { start: 0, limit: 50} });
         }

         if (mensagens != ""){

            Ext.Msg.show({
                        title: 'Aviso',
                        msg: mensagens,
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.WARNING
                    });


         }
      }

    // Ao clicar em checbox todos validar se habilita ou nao o botão
  


     function VerificaSelecionadosHabilitaBtns(){
        var selection = sm.getSelections();
        desabilitaTodosBotoes();

        if (selection.length > 1) {
            Ext.getCmp("btnVisualizar").setDisabled(true);
            Ext.getCmp("btnImpotar").setDisabled(true);
            Ext.getCmp("btnGerar").setDisabled(true);

            var todosTemId = true;
            if (selection && selection.length > 0) {
                for (i = 0; i < selection.length; i++) {
                    if (selection[i].data.IdOsCheckList == 0){
                        todosTemId = false;
                    }
                }
            }

            if (todosTemId){
                 Ext.getCmp("btnExportar").setDisabled(exportar == "True"? false : true);
            }
            else{
                Ext.getCmp("btnExportar").setDisabled(true);
            }
        }
        else if(selection.length == 1) {

                if (selection[0].data.IdOsCheckList > 0){

                    Ext.getCmp("btnGerar").setDisabled(gerar == "True" ? false : true);
                    Ext.getCmp("btnImpotar").setDisabled(importar == "True" && selection[0].data.Ida > 0? false : true);
                    Ext.getCmp("btnVisualizar").setDisabled(visualizar == "True"? false : true);
                    Ext.getCmp("btnExportar").setDisabled(exportar == "True"? false : true);

                     if (concluido){
                        
                         Ext.getCmp("btnGerar").setDisabled(true);
                         Ext.getCmp("btnImpotar").setDisabled(true);
                    }
                }
                else {
                     Ext.getCmp("btnVisualizar").setDisabled(true);
                     Ext.getCmp("btnExportar").setDisabled(true);
                     Ext.getCmp("btnGerar").setDisabled(gerar == "True" ? false : true); 
                     Ext.getCmp("btnImpotar").setDisabled(importar == "True" ? false : true);             
                }
                 
        }
        else if(selection.length == 0) {
            desabilitaTodosBotoes();
        }
   }

   function desabilitaTodosBotoes(){
        Ext.getCmp("btnExportar").setDisabled(true);
        Ext.getCmp("btnGerar").setDisabled(true);
        Ext.getCmp("btnImpotar").setDisabled(true);
        Ext.getCmp("btnVisualizar").setDisabled(true);
    }

  
    // Busca e armazena lista de ID_OS
    function BuscaIDSselecionados() 
    {
  
        var selection = sm.getSelections();

        if (selection && selection.length > 0) {
            var listaOs = [];

            selection = sm.getSelections();

            $.each(selection, function (i, e) {
                listaOs[i] = e.data.OS;
            });
        }

        return listaOs;
    }

     Ext.onReady(function () {
        
        ColocarHojeEmDatas();
        ValidarPermissoes();
        ConfirmacaoImportacao();

//        $(".x-grid3-cell-inner.x-grid3-col-checkIda").find(".x-grid3-cc-checkIda").addClass("x-item-disabled");
//        $(".x-grid3-cell-inner.x-grid3-col-checkVolta").find(".x-grid3-cc-checkVolta").addClass("x-item-disabled");


        $(".x-grid3-hd-checker").click(function () {
            VerificaSelecionadosHabilitaBtns();
        });

    });

    </script>
</asp:Content>
<asp:Content runat="server" ID="Content2" ContentPlaceHolderID="Content">
    <div id="header-content">
        <h1>
            Checklist de passageiros</h1>
        <small>Você está em Operação > Pesquisa de Checklist de passageiros</small>
        <br />
    </div>
</asp:Content>
