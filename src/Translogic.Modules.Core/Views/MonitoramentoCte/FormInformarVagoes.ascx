﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%@ Import Namespace="System.Globalization" %>
<%@ Import Namespace="Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico" %>
<div id="div-form-status-vagoes">
</div>
<script type="text/javascript">

		var gridVagoes = new Ext.grid.GridPanel({
			viewConfig: {
				forceFit: true
			},
			height: 270,
			width: 800,
			stripeRows: true,
			region: 'center',
			store: storeVagoes,
			autoScroll: true,
			loadMask: { msg: App.Resources.Web.Carregando },
			colModel: new Ext.grid.ColumnModel({
				defaults: {
					sortable: false
				},
				columns: [
							new Ext.grid.RowNumberer(),
							{ header: 'Status', dataIndex: "Erro", sortable: false, width: 50, renderer: statusRenderer },
							{ header: 'Vagão', dataIndex: "Vagao", sortable: false, width: 150 },
							{ header: 'Mensagem', dataIndex: "Mensagem", sortable: false, width: 450}
						 ]
			})
		});

		function statusRenderer(val)
		{
			switch (val) {
				case false:
					return "<img src='<%=Url.Images("Icons/tick.png") %>' >";
					break;
				default:
					return "<img src='<%=Url.Images("Icons/cancel.png") %>' >";
					break;
			}
		}  

		
		/* ------------- Ação CTRL+C nos campos da Grid ------------- */
		var isCtrl = false;
		document.onkeyup = function(e) {

		    var keyID = event.keyCode;

		    // 17 = tecla CTRL
		    if (keyID == 17) {
		        isCtrl = false; //libera tecla CTRL
		    }
		};

		document.onkeydown = function (e) {

			var keyID = event.keyCode;

			// verifica se CTRL esta acionado
			if (keyID == 17) {
				isCtrl = true;
			}

			// 67 = tecla 'v'
			if (keyID == 86 && isCtrl == true) {				
				
				var retorno = window.clipboardData.getData('Text');
				
				var rePattern = new RegExp('(?:[0-9]{1,7})', 'gi');
				var arrVagoes = new Array();
				
				while (arrMatch = rePattern.exec(retorno)){
					arrVagoes.push(arrMatch[0]);
				}

				var dados = $.toJSON(arrVagoes);
				$.ajax({
					url: "<%= Url.Action("ValidarVagoes") %>",
					type: "POST",
					beforeSend: function() { Ext.getCmp("formVagoes").getEl().mask("Validando os códigos dos vagões.", "x-mask-loading"); },
					dataType: 'json',
					data: dados,
					contentType: "application/json; charset=utf-8",
					success: function(result) {						
                        storeVagoes.loadData(result);
						Ext.getCmp("formVagoes").getEl().unmask();
					},
					failure: function(result) {
						Ext.Msg.alert('Erro na obtenção dos vagões!', result.Message);
					}
				});

			}
		}


		/* ------------------------------------------------------- */

		var formVagoes = new Ext.form.FormPanel({
	        id: 'formVagoes',
	        labelWidth: 80,
	        width: 830,
	        bodyStyle: 'padding: 15px',
	        labelAlign: 'top',
	        items: [gridVagoes],
	        buttonAlign: "center",
	        buttons:
		    [
		        {
		            text: "Confirmar",
				    id: 'btConfirmar',
		            handler: function () {
				    	if(storeVagoes.getCount() != 0)
				    	{
				    		storeVagoes.each(
				    			function (record) {
				    				if (record.data.Erro) {
				    					storeVagoes.remove(record);
				    				}
				    			}
				    		);
				    		
				    		if(storeVagoes.getCount() == 0)
				    		{
				    			Ext.Msg.alert('Informação', 'Favor informar um código de vagão válido!');
				    			return;
				    		}
				    		
                            if (storeVagoes.getCount() === 1) {
                                Ext.getCmp("filtro-numVagao").setValue('1 vagão selecionado.');
                            }
				    	    else {
                                Ext.getCmp("filtro-numVagao").setValue(storeVagoes.getCount() + ' vagoes selecionados.');
                            }
				    	}

                        Ext.getCmp("filtro-numVagao").setDisabled(true);
				    	windowStatusVagoes.close();
		            }
		        },
		        {
		            text: "Cancelar",
		            handler: function () {
		                Ext.getCmp("filtro-numVagao").setValue('');
		                Ext.getCmp("filtro-numVagao").setDisabled(false);
		                storeVagoes.removeAll();
					    windowStatusVagoes.close();
		            }
		        }
		    ]
	     });

		 formVagoes.render("div-form-status-vagoes");
</script>
