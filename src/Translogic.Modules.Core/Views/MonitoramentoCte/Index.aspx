﻿<%@ Import Namespace="System.Security.Policy" %>

<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Interna.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Import Namespace="Translogic.Modules.Core.Domain.Model.Diversos.Cte" %>
<asp:Content runat="server" ID="HeadContent" ContentPlaceHolderID="HeadContent">
    <script type="text/javascript">

		var formModal = null;
		var grid = null;
		var gridStatus = null;
		var ds = null;
		// Flag para garantir que uma tela de mensagem que está sobreposta não chame novamente a tela de pesquisa
		var executarPesquisa = true;
		var lastRowSelected = -1;
		var lastDblClickRowSelected = -1;

		function showResult(btn) {
			executarPesquisa = true; 
		}

		function Pesquisar(exportar)
		{
			var diferenca = Ext.getCmp("filtro-data-final").getValue() - Ext.getCmp("filtro-data-inicial").getValue(); 
			diferenca = Math.floor(diferenca / (1000 * 60 * 60 * 24)); 

			if (Ext.getCmp("filtro-data-inicial").getValue() == '' || Ext.getCmp("filtro-data-final").getValue() == '') {
				executarPesquisa = false;
				Ext.Msg.show({
				title: "Mensagem de Informação",
				msg: "Preencha os filtro datas!",
				buttons: Ext.Msg.OK,
				icon: Ext.MessageBox.INFO,
				minWidth: 200,
				fn: showResult
				});
			}
			else if (diferenca > 30) {
				executarPesquisa = false;
				Ext.Msg.show({
				title: "Mensagem de Informação",
				msg: "O período da pesquisa não deve ultrapassar 30 dias",
				buttons: Ext.Msg.OK,
				icon: Ext.MessageBox.INFO,
				minWidth: 200,
				fn: showResult
				});
			}
			else if (Ext.getCmp("filtro-serie").getValue() != '' && Ext.getCmp("filtro-despacho").getValue() == '') {
				executarPesquisa = false;
				Ext.Msg.show({
					title: "Mensagem de Informação",
					msg: "Não é possível filtrar apenas pela a Série do Despacho.<br/>Também é necessário preencher o número do Despacho.",
					buttons: Ext.Msg.OK,
					icon: Ext.MessageBox.INFO,
					minWidth: 200,
					fn: showResult
				});
			}
			else if (Ext.getCmp("filtro-despacho").getValue() != '' && Ext.getCmp("filtro-serie").getValue() == '') {
				executarPesquisa = false;
				Ext.Msg.show({
					title: "Mensagem de Informação",
					msg: "Não é possível filtrar apenas pelo número do Despacho.<br/>Também é necessário preencher a Série do Despacho.",
					buttons: Ext.Msg.OK,
					icon: Ext.MessageBox.INFO,
					minWidth: 200,
					fn: showResult
				});
			}
			else {

                if (exportar)
                {
                    Ext.getBody().mask("Exportando...", "x-mask-loading");
                    var url = '<%= Url.Action("ObterCtesExportar", "MonitoramentoCte") %>';

                    var dataInicial = new Array(Ext.getCmp("filtro-data-inicial").getValue().format('d/m/Y') + " " + Ext.getCmp("filtro-data-inicial").getValue().format('H:i:s'));
                    var dataFinal = new Array(Ext.getCmp("filtro-data-final").getValue().format('d/m/Y') + " " + Ext.getCmp("filtro-data-inicial").getValue().format('H:i:s'));  
                    var serie = Ext.getCmp("filtro-serie").getValue();
                    var despacho = Ext.getCmp("filtro-despacho").getValue();
                    var fluxo = Ext.getCmp("filtro-fluxo").getValue();
                    var chave = Ext.getCmp("filtro-Chave").getValue();
                    var errocte = Ext.getCmp("filtro-CodigoErro").getValue();
                    var origem = Ext.getCmp("filtro-Ori").getValue();
                    var destino = Ext.getCmp("filtro-Dest").getValue();
			        var impresso = Ext.getCmp("filtro-Impresso").getValue();
                    var UfDcl = Ext.getCmp("filtro-UfDcl").getRawValue();

                    var listaVagoes = '';
			        if(storeVagoes.getCount() != 0)
			        {
				        storeVagoes.each(
					        function (record) {
						        if (!record.data.Erro) {
							        listaVagoes += record.data.Vagao + ";";	
						        }
					        }
				        );
			        } else {
				        listaVagoes = Ext.getCmp("filtro-numVagao").getValue();	
			        }

			        url += "?dataInicial=" + dataInicial +
                    "&dataFinal=" + dataFinal +
                    "&serie=" + serie +
                    "&despacho=" + despacho +
                    "&fluxo=" + fluxo +
                    "&chave=" + chave +
                    "&errocte=" + errocte + 
                    "&origem=" + origem +
                    "&destino=" + destino + 
                    "&impresso=" + impresso + 
                    "&UfDcl=" + UfDcl + 
                    "&vagoes=" + listaVagoes;
            
                    window.open(url, "");
                    Ext.getBody().unmask();
                }
                else{
						var view   = Ext.getCmp('gridCteRaiz').getView(); 
						var chkdiv = Ext.fly(view.innerHd).child(".x-grid3-hd-checker");
						chkdiv.removeClass('x-grid3-hd-checker-on'); 
						grid.getStore().load();
						dsStatus.removeAll();
						executarPesquisa = true;
				    }
                }
			 }  

			function FormSuccess(form, action) {
				Ext.getCmp("gridCteRaiz").getStore().removeAll();
				// dsFilho.removeAll();
				dsStatus.removeAll();
				Ext.getCmp("reemissao").disabled = true;
				Ext.getCmp("gridCteRaiz").getStore().loadData(action.result, true);
			}
			
			function FormError(form, action) {
				Ext.Msg.alert('Mensagem de Erro...', action.result.Message);
				ds.removeAll();
			}

			var storeImpressao = new Ext.data.JsonStore({
				root: "Items",
				fields: [
									'CteId',
									'NroCte',
									'Status'
								]});

	// DataSource da combo de Erro
	var dsSituacaoCte = new Ext.data.ArrayStore({
		fields: ['Id', 'Descricao'],
		data: [	
                ['', ''],			
				['<%= Translogic.Core.Commons.Enum<SituacaoCteEnum>.GetDescriptionOf(SituacaoCteEnum.Autorizado ) %>', 'Autorizado'],			
				['<%= Translogic.Core.Commons.Enum<SituacaoCteEnum>.GetDescriptionOf(SituacaoCteEnum.PendenteArquivoEnvio ) %>', 'Pendente Envio'],
				['<%=Translogic.Core.Commons.Enum<SituacaoCteEnum>.GetDescriptionOf(SituacaoCteEnum.Erro) %>', 'Erro'],
				['<%=Translogic.Core.Commons.Enum<SituacaoCteEnum>.GetDescriptionOf(SituacaoCteEnum.ErroAutorizadoReEnvio) %>', 'Erro Autorizado Reenvio'],
				['<%=Translogic.Core.Commons.Enum<SituacaoCteEnum>.GetDescriptionOf(SituacaoCteEnum.AutorizadoInutilizacao) %>', 'Cte Inutilizado'],
				['<%=Translogic.Core.Commons.Enum<SituacaoCteEnum>.GetDescriptionOf(SituacaoCteEnum.EnviadoArquivoEnvio) %>', 'Enviado Arquivo'],
				['<%=Translogic.Core.Commons.Enum<SituacaoCteEnum>.GetDescriptionOf(SituacaoCteEnum.Cancelado) %>', 'Cancelado'],
				['<%=Translogic.Core.Commons.Enum<SituacaoCteEnum>.GetDescriptionOf(SituacaoCteEnum.EnviadoFilaArquivoEnvio) %>', 'Enviado Fila']
			]
	});

    // DataSource da combo de Erro
	var dsImpresso = new Ext.data.ArrayStore({
		fields: ['Id', 'Descricao'],
		data: [	
                ['T', 'Todos'],
				['S', 'Impresso'],
				['N', 'Não Impresso']				
			]
	});

    var storeVagoes = new Ext.data.JsonStore({
		remoteSort: true,
		root: "Items",
		fields: [
					'Vagao',
					'Erro',
					'Mensagem'
				]
	});

    var dsCodigoControle = new Ext.data.JsonStore({
        root: "Items",
        autoLoad: true,
        url: '<%= Url.Action("ObterCodigoSerieDesp") %>',
        fields: [
                    'Id',
                    'CodigoControle'
			    ]
    });

		var dsStatus = new Ext.data.JsonStore({
			root: "Items",
		    proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("ObterHistoricoStatusCte") %>', timeout: 600000 }),
			fields: [
								'DescricaoStatus',
								'DateEmissao',
								'AcaoTomada',
								'Situacao'
							]
		});

	var cboSituacaoCte = {
		xtype: 'combo',
		store: dsSituacaoCte,
		allowBlank: true,
		lazyInit: false,
		lazyRender: false,
		mode: 'local',
		typeAhead: false,
		triggerAction: 'all',
		fieldLabel: 'Situação CTe',
		name: 'filtro-CodigoErro',
		id: 'filtro-CodigoErro',
		hiddenName: 'filtro-CodigoErro',
		displayField: 'Descricao',
		forceSelection: true,
		width: 120,
		valueField: 'Id',
		emptyText: 'Selecione...',
		editable: false
	};

	var cboCteImpresso = {
		xtype: 'combo',
		store: dsImpresso,
		allowBlank: true,
		lazyInit: false,
		lazyRender: false, 
		mode: 'local',
		typeAhead: false,
		triggerAction: 'all',
		fieldLabel: 'CTe Impresso',
		name: 'filtro-Impresso',
		id: 'filtro-Impresso',
		hiddenName: 'filtro-Impresso',
		displayField: 'Descricao',
		forceSelection: true,
		width: 100,
		value: 'N',	
		valueField: 'Id',
		emptyText: 'Selecione...',
		editable: false
	};
    
	var cboCteCodigoControle = {
		xtype: 'combo',
		store: dsCodigoControle,
		allowBlank: true,
		lazyInit: false,
		lazyRender: false, 
		mode: 'local',
		typeAhead: false,
		triggerAction: 'all',
		fieldLabel: 'UF DCL',
		name: 'filtro-UfDcl',
		id: 'filtro-UfDcl',
		hiddenName: 'filtro-UfDcl',
		displayField: 'CodigoControle',
		forceSelection: true,
		width: 70,
		valueField: 'Id',
		emptyText: 'Selecione...',
		editable: false,
		tpl : '<tpl for="."><div class="x-combo-list-item">{CodigoControle}&nbsp;</div></tpl>'		
	};

/* ------------- Ação CTRL+C nos campos da Grid ------------- */
    var isCtrl = false;
    document.onkeyup = function(e) {

        var keyID = event.keyCode;

        // 17 = tecla CTRL
        if (keyID == 17) {
            isCtrl = false; //libera tecla CTRL
        }
    };

    document.onkeydown = function(e) {

        var keyID = event.keyCode;

        // verifica se CTRL esta acionado
        if (keyID == 17) {
            isCtrl = true;
        }

        if ((keyID == 13) && (executarPesquisa)) {
            Pesquisar(false);
            return;
        }

        // 67 = tecla 'c'
        if (keyID == 67 && isCtrl == true) {

            // verifica se há selecão na tabela
            if (grid.getSelectionModel().hasSelection()) {
                // captura todas as linhas selecionadas
                var recordList = grid.getSelectionModel().getSelections();

                var a = [];
                var separator = '\t'; // separador para colunas no excel
                for (var i = 0; i < recordList.length; i++) {
                    var s = '';
                    var item = recordList[i].data;
                    for (key in item) {
                        if (key != 'Id') { //elimina o campo id da linha
                            s = s + item[key] + separator;
                        }
                    }
                    s = s.substr(0, s.length - 1); //retira o ultimo separador '\t' da linha
                    a.push(s);
                }
                window.clipboardData.setData('Text', (a.join('\n'))); //insere linhas no clipBoard
            }
        }
    };

	

    /* ------------------------------------------------------- */
 
    $(function () {
		smRaiz = new Ext.grid.CheckboxSelectionModel({
                  listeners: {
                  
                      viewready: function(view) {
                          var els = view.el.query('div[ext-xtype]');

                          Ext.each(els,
                              function(domEl) {
                                  var xtype = Ext.get(domEl).getAttribute('ext-xtype');
                                  var recIndex = Ext.get(domEl).getAttribute('recindex');
                                  var dataRec = view.getStore().getAt(recIndex);
                                  Ext.widget(xtype, { renderTo: domEl, dataRec: dataRec });
                              },
                              this);
                      },

                      selectionchange: function (sm) {
					    
					    var recLen = Ext.getCmp('gridCteRaiz').store.getRange().length; 
                        var selectedLen = this.selections.items.length; 
                        if(selectedLen == recLen){ 
                            var view   = Ext.getCmp('gridCteRaiz').getView();
                            var chkdiv = Ext.fly(view.innerHd).child(".x-grid3-hd-checker"); 
                            chkdiv.addClass("x-grid3-hd-checker-on"); 
                        } 

						if (fctVerificaItemSelecionado('EAR'))
						{
							Ext.getCmp("reemissao").enable();
						} else {
							Ext.getCmp("reemissao").disable();
						}

						if (fctVerificaItemSelecionado('AUT') || fctVerificaItemSelecionado('AGA'))
						{
							Ext.getCmp("imprimir").enable();
						} else {
							Ext.getCmp("imprimir").disable();
						}

						if (fctVerificaNaoSelecionado('AUT') && !JaFoiAutorizado())
						{
							Ext.getCmp("inutilizacao").enable();
						}else{
							Ext.getCmp("inutilizacao").disable();
						}
					 },
					beforerowselect : function (sm, rowIndex, keep, rec) {
					var sitgrid = ds.getAt(rowIndex).get('SituacaoCte');
						 if (sitgrid != "EAR" &&
							 sitgrid != "ERR" && 
    						 sitgrid != "EAC" && 
							 sitgrid != "INV" && 
							 sitgrid != "AUT" &&
						     sitgrid != "AGA" &&
							 sitgrid != "PAE"){
						 	  return false;     
						 }
					}
					
				},
                rowdeselect : function ( sm ,rowIndex ,record) { 
                    var view   = Ext.getCmp('gridCteRaiz').getView(); 
                    var chkdiv = Ext.fly(view.innerHd).child(".x-grid3-hd-checker") 
                    chkdiv.removeClass('x-grid3-hd-checker-on'); 
                }
				,
				renderer: function (v, p, record){
					if (record.data.SituacaoCte != "EAR" &&
						record.data.SituacaoCte != "ERR" && 
						record.data.SituacaoCte != "INV" && 
						record.data.SituacaoCte != "AUT" && 
    					record.data.SituacaoCte != "EAC" && 
					    record.data.SituacaoCte != "AGA" &&
						record.data.SituacaoCte != "PAE"){
						return '';
					}
					return '<div class="x-grid3-row-checker">&#160;</div>';
				}
				
        });
		
	 function fctVerificaItemSelecionado(status)
	 {
		var count = 0;
		/* var selected = sm2.getSelections();

		for (var i = 0; i < selected.length; i++) {
			if(selected[i].get('SituacaoCte') == status)
				count++;
		}
		*/
		var selected = smRaiz.getSelections();

		for (var i = 0; i < selected.length; i++) {
			if(selected[i].get('SituacaoCte') == status)
				count++;
		}

		return (count > 0 );

	 }

	  function fctVerificaNaoSelecionado(status)
	 {
		var count = 0;
		/* var selected = sm2.getSelections();

		for (var i = 0; i < selected.length; i++) {
			if(selected[i].get('SituacaoCte') == status)
				count++;
		}
		*/
		var selected = smRaiz.getSelections();

		for (var i = 0; i < selected.length; i++) {
			if(selected[i].get('SituacaoCte') != status)
				count++;
		}

		return (count > 0 );

	 }
        
     function JaFoiAutorizado()
	 {
		var count = 0;
		var selected = smRaiz.getSelections();

		for (var i = 0; i < selected.length; i++) {
			if(selected[i].get('QtdeAutorizacoes') > 0)
				count++;
		}

		return (count > 0 );
	 }

	 var statusRenderer = function(val, name, meta, record) {

        switch(val){
            case "AUT":
                return "<img src='<%=Url.Images("Icons/tick.png") %>' alt='CTe Aprovado'>";
                break;
						case "AGI":
						case "AGC":
                return "<img src='<%=Url.Images("Icons/application_go.png") %>' alt='Aguardando Cancelamento / Inutilização'>";
                break;
            case "EAE":
                return "<img src='<%=Url.Images("Icons/email_go.png") %>' alt='Enviado arquivo CTe para Sefaz.'>";
                break;
            case "EAR":
                return "<img src='<%=Url.Images("Icons/arrow_redo_yellow.png") %>' alt='Erro Autorizado reenvio do CTe.'>";
                break;            
            case "CAN":
                return "<img src='<%=Url.Images("Icons/cancel.png") %>' alt='CTe Cancelado '>";
                break;                        
             case "INT":
                return "<img src='<%=Url.Images("Icons/delete.png") %>' alt='CTe Invalidado'>";
                break;                        
             case "AGC":
                return "<img src='<%=Url.Images("Icons/lock_edit.png") %>' alt='Aguardando cancelamento.'>";
                break;                                    
             case "AUC":
                return "<img src='<%=Url.Images("Icons/lock_open.png") %>' alt='Autorizado cancelamento.'>";
                break;
             case "ERR":
                return "<img src='<%=Url.Images("Icons/cross.png") %>' alt='Erro na geração do CTe.'>";
                break; 
			case "PCC":
                return "<img src='<%=Url.Images("Icons/information.png") %>' alt='Cte aguardando a geração automática da numeração / chave.'>";
                break; 	
			case "ANL":
                return "<img src='<%=Url.Images("Icons/cancel.png") %>' alt='CTe Anulado '>";
                break; 	
            case "ANC":

                if (meta.data.SituacaoCteAnulacao === 'AUT') {
                    return "<img src='<%=Url.Images("Icons/delete.png") %>' alt='CTe anulado na SEFAZ '>";
                } else {
                    return "<img src='<%=Url.Images("Icons/error_delete.png") %>' alt='CTe enviado anulação para config '>";
                }
            default:
                return "<img src='<%=Url.Images("Icons/alert.png") %>' alt='Cte em processamento.'>";
                break;
        }
      }                  
           
      function printRenderer(val, metaData, record, rowIndex, colIndex, store) 
      {
          var id = record.get('CteId');
           var url = '<%=Url.Action("Imprimir") %>';
           url = url + "?idCte="+ id;

          switch(val){
            case true:                
                return "<a href='"+ url + "'><img src='/Content/Images/Icons/printer.png' alt='Imprimir' ></a><br />";
                break;
             default:
                return "";
                break;
        }
      }           

    var legenda = new Ext.Toolbar({
        layout:'column',
        items: [
            {
                xtype: 'button',
                text: 'Legenda:'
            }, {
                xtype: 'button',
                text: 'Erro Autorizado reenvio do CTe.',
                iconCls: 'icon-arrow_redo_yellow'
            }, {
                xtype: 'button',
                text: 'Enviado arquivo CTe para Sefaz.',
                iconCls: 'icon-email_go'
            },{
                xtype: 'button',
                text: 'CTe Cancelado.',
                iconCls: 'icon-cancel'
            },{
                xtype: 'button',
                text: 'CTe Invalidado.',
                iconCls: 'icon-deleteCte'
            },{
                xtype: 'button',
                text: 'Aguardando cancelamento.',
                iconCls: 'icon-lock_edit'
            },{
                xtype: 'button',
                text: 'Autorizado cancelamento.',
                iconCls: 'icon-lock_open'
            },{
                xtype: 'button',
                text: 'Erro na geração do CTe.',
                iconCls: 'icon-delete'
            },{
                xtype: 'button',
                text: 'Cte em processamento.',
                iconCls: 'icon-warning'
            },{
                xtype: 'button',
                text: 'CTe Aprovado.',
                iconCls: 'icon-tick'
            }
        ]
    });

	grid = new Translogic.PaginatedGrid({
		autoLoadGrid: false,
		id: "gridCteRaiz", 
		height: 300,
		width: 450,			
		url: '<%= Url.Action("ObterCtes") %>',
		region: 'center',
		viewConfig: {
			forceFit: false,
			getRowClass: MudaCor
		},
		fields: [
			'CteId',
			'Fluxo',
			'Origem',
			'Destino',
			'Mercadoria',                
			'Chave',
			'Cte',
			'SituacaoCte',
			'DateEmissao',             
			'Impresso',
			'CodigoErro',
			'FerroviaOrigem',
			'FerroviaDestino',
			'UfOrigem',
			'CodigoVagao',
			'UfDestino',
			'Serie',
			'Despacho',          
			'SerieDesp5', 
			'Desp5',
			'ArquivoPdfGerado',
			'AcaoSerTomada',
		    'ReemitirCancelamento',
		    'QtdeAutorizacoes',
		    'SituacaoCteAnulacao'
		],
		columns: [ 
			smRaiz,    
			{ dataIndex: "CteId", hidden: true },
			{ dataIndex: "Impresso", hidden: true },
		    { dataIndex: "ReemitirCancelamento", hidden: true },
		    { dataIndex: "QtdeAutorizacoes", hidden: true },
			// { header: '...', dataIndex: "ArquivoPdfGerado",  width: 25, sortable: false,renderer:printRenderer },
			{ header: '...', dataIndex: "SituacaoCte",  width: 25, sortable: false, renderer:statusRenderer },
			{ header: 'Num Vagão', dataIndex: "CodigoVagao", width: 100, sortable: false },
			{ header: 'Série', dataIndex: "Serie", width: 70, sortable: false },
			{ header: 'Despacho', dataIndex: "Despacho", width: 70, sortable: false },
			{ header: 'CTe', dataIndex: "Cte",  width: 100, sortable: false },
			{ header: 'Origem', dataIndex: "Origem", width: 50, sortable: false },
			{ header: 'Destino', dataIndex: "Destino", width: 50, sortable: false },
			{ header: 'Fluxo', dataIndex: "Fluxo",  width: 70, sortable: false },
			{ header: 'Mercadoria', dataIndex: "Mercadoria", width: 70, sortable: false },
			{ header: 'Chave CTe', dataIndex: "Chave", width: 280, sortable: false },
			{ header: 'Data Emissão', dataIndex: "DateEmissao", width: 75, sortable: false },
			{ header: 'Ferrovia Ori.', dataIndex: "FerroviaOrigem", sortable: false },
			{ header: 'Ferrovia Dest.', dataIndex: "FerroviaDestino", sortable: false },
			{ header: 'UF Ori.', dataIndex: "UfOrigem", width: 70, sortable: false },
			{ header: 'UF Dest.', dataIndex: "UfDestino", width: 70, sortable: false },
			{ header: 'SerieDesp5', dataIndex: "SerieDesp5", width: 70, sortable: false },
			{ header: 'Desp5', dataIndex: "Desp5", width: 70, sortable: false }
		],
		sm: smRaiz,
		listeners: {
			rowclick: function(grid, rowIndex, e) {
				if (smRaiz.isSelected(rowIndex)) {
					if (rowIndex == lastRowSelected) {
						grid.getSelectionModel().deselectRow(rowIndex);
						dsStatus.removeAll();
						lastRowSelected = -1;
					}
					else {
						lastRowSelected = rowIndex;
						lastDblClickRowSelected = rowIndex;
					}
				}

			    //VerificarReemissaoCancelamento(rowIndex);
			},
			rowdblclick:  function(grid, rowIndex, record, e) {
					grid.getSelectionModel().selectRow(lastDblClickRowSelected);
					grid.getSelectionModel().selectRow(item);
			}
		}
		// fbar: [legenda]
	});
       
       function MudaCor(row, index) {
            if (row.data.Impresso) {
                return 'corGreen';
            }
        }

	   ds = Ext.getCmp("gridCteRaiz").getStore();
	   grid.getStore().proxy.on('beforeload', function(p, params) {	   
	   
			params['filter[0].Campo'] = 'dataInicial';
			params['filter[0].Valor'] = new Array(Ext.getCmp("filtro-data-inicial").getValue().format('d/m/Y') + " " + Ext.getCmp("filtro-data-inicial").getValue().format('H:i:s')); 
			params['filter[0].FormaPesquisa'] = 'Start';

			params['filter[1].Campo'] = 'dataFinal';
			params['filter[1].Valor'] = new Array(Ext.getCmp("filtro-data-final").getValue().format('d/m/Y') + " " + Ext.getCmp("filtro-data-inicial").getValue().format('H:i:s'));  
			params['filter[1].FormaPesquisa'] = 'Start';

			params['filter[2].Campo'] = 'serie';
			params['filter[2].Valor'] = Ext.getCmp("filtro-serie").getValue();
			params['filter[2].FormaPesquisa'] = 'Start';

			params['filter[3].Campo'] = 'despacho';
			params['filter[3].Valor'] = Ext.getCmp("filtro-despacho").getValue();
			params['filter[3].FormaPesquisa'] = 'Start';

			params['filter[4].Campo'] = 'fluxo';
			params['filter[4].Valor'] = Ext.getCmp("filtro-fluxo").getValue();
			params['filter[4].FormaPesquisa'] = 'Start';

			params['filter[5].Campo'] = 'chave';
			params['filter[5].Valor'] = Ext.getCmp("filtro-Chave").getValue();
			params['filter[5].FormaPesquisa'] = 'Start';

			params['filter[6].Campo'] = 'errocte';
			params['filter[6].Valor'] = Ext.getCmp("filtro-CodigoErro").getValue();
			params['filter[6].FormaPesquisa'] = 'Start';	
			
			params['filter[7].Campo'] = 'Origem';
			params['filter[7].Valor'] = Ext.getCmp("filtro-Ori").getValue();
			params['filter[7].FormaPesquisa'] = 'Start';
			
			params['filter[8].Campo'] = 'Destino';
			params['filter[8].Valor'] = Ext.getCmp("filtro-Dest").getValue();
			params['filter[8].FormaPesquisa'] = 'Start';
			
            var listaVagoes = '';
			if(storeVagoes.getCount() != 0)
			{
				storeVagoes.each(
					function (record) {
						if (!record.data.Erro) {
							listaVagoes += record.data.Vagao + ";";	
						}
					}
				);
			} else {
				listaVagoes = Ext.getCmp("filtro-numVagao").getValue();	
			}

			params['filter[9].Campo'] = 'Vagao';
			params['filter[9].Valor'] = listaVagoes;
			params['filter[9].FormaPesquisa'] = 'Start';
            
            params['filter[10].Campo'] = 'Impresso';
			params['filter[10].Valor'] = Ext.getCmp("filtro-Impresso").getValue();
			params['filter[10].FormaPesquisa'] = 'Start';      
            
            params['filter[11].Campo'] = 'UfDcl';
			params['filter[11].Valor'] = Ext.getCmp("filtro-UfDcl").getRawValue();
			params['filter[11].FormaPesquisa'] = 'Start';            
	   });

      /*gridCteFilho = new Ext.grid.GridPanel({         
            id: "gridCteFilho", 
            stripeRows: false,
            height: 340,           
            store: dsFilho,
            loadMask: { msg: App.Resources.Web.Carregando },
            colModel: new Ext.grid.ColumnModel({
                defaults: {
                    sortable: false
                },
                columns: [
                        sm2,     
						{ dataIndex: "CteId", hidden: true },                   
                        { header: '...', dataIndex: "ArquivoPdfGerado",  width: 25, sortable: false,renderer:printRenderer },
						// { header: 'Id', dataIndex: "CteId", width: 40, sortable: false },
						{ header: 'Fluxo', dataIndex: "Fluxo", width: 70, sortable: false },
						{ header: 'Origem', dataIndex: "Origem", width: 50, sortable: false },
						{ header: 'Destino', dataIndex: "Destino", width: 50, sortable: false},
				        { header: 'Mercadoria', dataIndex: "Mercadoria", width: 70, sortable: false },
                        { header: '...', dataIndex: "SituacaoCte",  width: 25, sortable: false, renderer:statusRenderer },                                 
				        { header: 'Chave CTE', dataIndex: "Chave", width: 280, sortable: false },
				        { header: 'CTE', dataIndex: "Cte", width: 100, sortable: false },
                        { header: 'Data Emissão', dataIndex: "DateEmissao",  width: 75, sortable: false },
                        { header: 'Usuário', dataIndex: "CodigoUsuario", sortable: false },
                        { header: 'Situação', dataIndex: "CodigoErro", width: 220, sortable: false },                        
                        { header: 'Ferrovia Ori.', dataIndex: "FerroviaOrigem", sortable: false },
                        { header: 'Ferrovia Dest.', dataIndex: "FerroviaDestino", sortable: false },
                        { header: 'UF Ori.', dataIndex: "UfOrigem", width: 70, sortable: false },
                        { header: 'UF Dest.', dataIndex: "UfDestino", width: 70, sortable: false },
                        { header: 'Série Desp5', dataIndex: "SerieDesp5", width: 70, sortable: false },
				        { header: 'Num Desp5', dataIndex: "NumDesp5", width: 70, sortable: false },
				        { header: 'Série Desp6', dataIndex: "SerieDesp6", width: 70, sortable: false },
				        { header: 'Num Desp6', dataIndex: "NumDesp6", width: 70, sortable: false }			        
					]
            }),
            sm: sm2
        });
		*/

		gridStatus = new Ext.grid.GridPanel({
			stripeRows: true,
			height: 293,
			width: 560,	
			region: 'center',
			store: dsStatus,
			loadMask: { msg: App.Resources.Web.Carregando },
			colModel: new Ext.grid.ColumnModel({
				defaults: {
					sortable: false
				},
			columns:	[
									{ header: 'Data', dataIndex: "DateEmissao", width: 110, sortable: false },
									{ header: 'Descrição', dataIndex: "DescricaoStatus", width: 230, sortable: false },
									{ header: 'Ação a ser Tomada', dataIndex: "AcaoTomada", width: 230, sortable: false }
								]
				})
		});
	   
		// ***************************************** 
		// Adiciona o ToolTip na grid do Cte Filho 
		// *****************************************
		gridStatus.on('render', function(grid) {
			var view = gridStatus.getView();    
			gridStatus.tip = new Ext.ToolTip({
				target: view.mainBody,    
				title: 'Ação a ser tomada:',
				delegate: '.x-grid3-row', 
				trackMouse: true,         
				renderTo: document.body,  
											
				listeners: {            										
					beforeshow: function updateTipBody(tip) {
					
						var rowIndex = view.findRowIndex(tip.triggerElement);
						var situacao = gridStatus.getStore().getAt(rowIndex).get('Situacao');
						tip.resizable = true; 

						if(situacao == 'ERR' || situacao == 'EAR')
						{
							tip.maxWidth = 500;
							var acao = gridStatus.getStore().getAt(rowIndex).get('AcaoTomada');
							var erro = gridStatus.getStore().getAt(rowIndex).get('DescricaoStatus'); 
							var msg = acao + "<br/> <b>Descrição do erro:</b><br/>"+erro;
							
							if(acao == null || acao == "" )
								tip.body.dom.innerHTML =  "";
							else
								tip.body.dom.innerHTML = msg;
						}						
					},
					show: function showTip(tip) {
							var rowIndex = view.findRowIndex(tip.triggerElement);
							var situacao = gridStatus.getStore().getAt(rowIndex).get('Situacao');
							
							if(situacao != 'ERR' && situacao != 'EAR') 
							{
								tip.hide();
							}
							 if(tip.body.dom.innerHTML == "")
							{
								tip.hide();
							}
							
						}
				}
			});
		});
		
        grid.on("rowdblclick", function(g,i,e){
			var id = grid.getStore().getAt(i).get('CteId');
            // colunaDet2.title = 'Cte '+ id +' - Fluxo: '+ grid.getStore().getAt(i).get('Fluxo');

            if(id != 0)
            {
                // dsFilho.load({ params: { idcte: id} });
                dsStatus.load({ params: { idcte: id} });
            }
		});

		var dataAtual = new Date();

		var dataInicial =	{
			xtype: 'datefield',
			fieldLabel: 'Data Inicial',
			id: 'filtro-data-inicial',
			name: 'dataInicial',
			width: 83,
			allowBlank: false,
			vtype: 'daterange',
			endDateField: 'filtro-data-final',
			hiddenName: 'dataInicial',
			value: dataAtual		
		};
	
		var dataFinal = {
			xtype: 'datefield',
			fieldLabel: 'Data Final',
			id: 'filtro-data-final',
			name: 'dataFinal',
			width: 83,
			allowBlank: false,
			vtype: 'daterange',
			startDateField: 'filtro-data-inicial',
			hiddenName: 'dataFinal'  ,
		    value: dataAtual
		};
        
		var origem =	{
			xtype: 'textfield',
			vtype: 'cteestacaovtype',
			style: 'text-transform: uppercase',
			id: 'filtro-Ori',
			fieldLabel: 'Origem',
			autoCreate: { tag: 'input', type: 'text', maxlength: '3', autocomplete: 'off' },
			name: 'Origem',
			allowBlank: true,
			maxLength: 3,
			width: 35,
			hiddenName: 'Origem'
		};

		var destino = {
			xtype: 'textfield',
			vtype: 'cteestacaovtype',
			style: 'text-transform: uppercase',
			id: 'filtro-Dest',
			fieldLabel: 'Destino',
			autoCreate: { tag: 'input', type: 'text', maxlength: '3', autocomplete: 'off' },
			name: 'Destino',
			allowBlank: true,
			maxLength: 3,
			width: 35,
			hiddenName: 'Destino'
		};

		var serie = {
			xtype: 'textfield',
			style: 'text-transform: uppercase',
			vtype: 'ctesdvtype',
			id: 'filtro-serie',
			fieldLabel: 'Série',
			name: 'serie',
			allowBlank: true,
			autoCreate: { tag: 'input', type: 'text', maxlength: '3', autocomplete: 'off' },
			maxLength: 20,
			width: 30,
			hiddenName: 'serie'
		};

		var despacho = {
			xtype: 'textfield',
			style: 'text-transform: uppercase',
			vtype: 'ctedespachovtype',
			id: 'filtro-despacho',
			fieldLabel: 'Despacho',
			name: 'despacho',
			allowBlank: true,
			maxLength: 20,
			autoCreate: { tag: 'input', type: 'text', maxlength: '6', autocomplete: 'off' },
			width: 55,
			hiddenName: 'despacho'
		};

		var fluxo = {
			xtype: 'textfield',
			style: 'text-transform: uppercase',
			vtype: 'ctefluxovtype',
			id: 'filtro-fluxo',
			fieldLabel: 'Fluxo',
			name: 'fluxo',
			allowBlank: true,
			autoCreate: { tag: 'input', type: 'text', maxlength: '5', autocomplete: 'off' },
			maxLength: 20,
			width: 45,
			hiddenName: 'fluxo',
			minValue: 0,
			maxValue: 99999
		};

        var Vagao = {
			xtype: 'textfield',
			style: 'text-transform: uppercase',
			id: 'filtro-numVagao',
			fieldLabel: 'Vagão',
			name: 'numVagao',
			allowBlank: true,
			autoCreate: { tag: 'input', type: 'text', autocomplete: 'off' },
			width: 150,
			hiddenName: 'numVagao'
		};
        
        var btnLoteVagao = {
			text: 'Informar Vagões',
			xtype: 'button',
			iconCls: 'icon-find',
			handler: function (b, e) {
				windowStatusVagoes = new Ext.Window({
							            id: 'windowStatusVagoes',
							         	title: 'Informar vagões',
							         	modal: true,
							         	width: 855,
							         	resizable: false,
							         	height: 380,
							         	autoLoad: {
							         		url: '<%= Url.Action("FormInformarVagoes") %>',
							         		scripts: true
							         	}
							         });

                windowStatusVagoes.show();
			}
		};

		var chave = {
			xtype: 'textfield',
			vtype: 'ctevtype',
			id: 'filtro-Chave',
			fieldLabel: 'Chave CTe',
			name: 'Chave',
			allowBlank: true,
			autoCreate: { tag: 'input', type: 'text', maxlength: '44', autocomplete: 'off' },
			maxLength: 50,
			width: 275,
			hiddenName: 'Chave  '
		};

		var arrDataIni = {
			width: 87,
			layout: 'form',
			border: false,
			items:
				[dataInicial]
		};

		var arrDataFim = {
				    width: 87,
				    layout: 'form',
				    border: false,
				    items:
						[dataFinal]
				};

		var arrOrigem = {
                    width: 43,
                    layout: 'form',
                    border: false,
                    items:
						[origem]
                };
		var arrDestino = {
					width: 43,
					layout: 'form',
					border: false,
					items:
						[destino]
				};

		var arrSerie =  {
							width: 37,
							layout: 'form',
							border: false,
							items:
								[serie]
						};

		var arrDespacho = {
				    width: 60,
				    layout: 'form',
				    border: false,
				    items:
						[despacho]
				};

		var arrFluxo = {
                    width: 50,
                    layout: 'form',
                    border: false,
                    items:
						[fluxo]
                };
		
		var arrVagao = {
			width: 160,
			layout: 'form',
			border: false,
			items:
				[Vagao]
		};
        
        var arrBtnLoteVagoes = {
            width: 120,
			layout: 'form',
            style: 'padding: 17px 0px 0px 0px;',
			border: false,
			items: [btnLoteVagao]
        };

		var arrChave = {
                    width: 280,
                    layout: 'form',
                    border: false,
                    items:
						[chave]
                };

		var arrSituacao = {
                    width: 125,
                    layout: 'form',
                    border: false,
                    items:
						[cboSituacaoCte]
                   };
        
        var arrImpresso = {
                width: 105,
                layout: 'form',
                border: false,
                items:
					[cboCteImpresso]
                };
            
        var arrCodigoDcl = {
                width: 75,
                layout: 'form',
                border: false,
                items:
					[cboCteCodigoControle]
                };
                       
		
		var arrlinha1 = {
			    layout: 'column',
			    border: false,
			    items: [
			        arrDataIni, 
			        arrDataFim, 
			        arrFluxo, 
			        arrChave 
			    ]
			};

			var arrlinha2 = {
					layout: 'column',
					border: false,
					items: [
						arrCodigoDcl,
						arrSerie,
						arrDespacho,
						arrOrigem,
						arrDestino,
						arrVagao,
						arrImpresso,
						arrSituacao,
						arrBtnLoteVagoes
					]
				};
		
		var arrCampos = new Array();
		arrCampos.push(arrlinha1);
		arrCampos.push(arrlinha2);

        filters = new Ext.form.FormPanel({
            id: 'grid-filtros',
            title: "Filtros",
            region: 'center',
            bodyStyle: 'padding: 15px',
            labelAlign: 'top',
            items:
			[arrCampos],
            buttonAlign: "center",
            buttons:
	        [
                 {
                    text: 'Pesquisar',
                    type: 'submit',
                    iconCls: 'icon-find',	
                    handler: function (b, e) {
                         Pesquisar(false);
                    }
                },
	            {
	                text: 'Limpar',
	                handler: function (b, e) {
						grid.getStore().removeAll();
						grid.getStore().totalLength = 0;
						grid.getBottomToolbar().bind(grid.getStore());							 
						grid.getBottomToolbar().updateInfo();  
	                    Ext.getCmp("grid-filtros").getForm().reset();
	                    gridStatus.getStore().removeAll();
						gridStatus.getStore().totalLength = 0;

						storeVagoes.removeAll();
	                },
	                scope: this
	            },
				{
	                text: 'Imprimir',
					id: 'imprimir',
					iconCls: 'icon-printer',
					name: 'imprimir',
					readOnly: true,	
	                handler: function (b, e) {
                    var selected = smRaiz.getSelections();
                    var idCte = "";
                    var arrCte = new Array();
                    for (var i = 0; i < selected.length; i++) {
						//idCte += selected[i].data.CteId +"$";
                       // if(selected[i].get('SituacaoCte') == 'AUT')
                       // {
                            arrCte.push(selected[i].data.CteId);
                      //  }
                    }

                    windowStatusImpressao = new Ext.Window({
				                        id: 'windowStatusImpressao',
				                        title: 'Status da impressão',
				                        modal: true,
				                        width: 400,
				                        height: 400,
										resizable: false,
				                        autoLoad: {
					                        url: '<%= Url.Action("FormStatusImpressao") %>',
                                            params:{ 'arrayCtes': arrCte},
					                        scripts: true,
											callback: function(el, sucess)
											{
												if(!sucess)
												{
													windowStatusImpressao.close();
												}
											}
				                        },
										listeners: {
											'close': function (panel)
											{
												Pesquisar(false);
											}
										}
			                        });
			                        windowStatusImpressao.show();								
									

                        /*
						var idCte = "";
						var selected = smRaiz.getSelections();
                        var count = 0;
                        var countAut = 0;
                        for (var i = 0; i < selected.length; i++) {
                            
                            if(selected[i].get('ArquivoPdfGerado'))
                                count++;

                            if(selected[i].get('SituacaoCte') == 'AUT')
                            {
								idCte += selected[i].data.CteId +"$";
                                countAut++;
                            }
                        }

                         if(count == 0)
                        {
                                Ext.Msg.alert('Alerta','Não foi gerado nenhum arquivo para impressão');
                                return;
                        }

                        if(count != countAut)
                        {
                            Ext.Msg.alert('Alerta','Foram selecionado(s) ' + countAut +' Cte(s) Autorizados, mas ' + (countAut-count) + 'não foram gerado(s) o(s) arquivo(s) para impressão');
                        }

						var url = '<%=Url.Action("Imprimir") %>';
						url = url + "?idCte=" + idCte;

						window.location = url;
						// var url = "<a href='" + url + "'><img src='/Content/Images/Icons/printer.png' alt='Imprimir'></a><br />";
                        */
	                },
	                scope: this
	            },
				{
	                text: 'Inutilização',
					id: 'inutilizacao',
					iconCls: 'icon-save',
					readOnly: true,					
	                handler: function (b, e) {
					
	                    var listaEnvio = Array();
                        /*var selected; // = sm2.getSelections();

                        for (var i = 0; i < selected.length; i++) {
                            listaEnvio.push(selected[i].data.CteId);
                        }
						*/
						var selected = smRaiz.getSelections();

						for (var i = 0; i < selected.length; i++) {
                            if(selected[i].get('SituacaoCte') != 'AUT')
								listaEnvio.push(selected[i].data.CteId);
                        }

	if (Ext.Msg.confirm("Inutilização", "Você possui " + listaEnvio.length + " para inutilização, deseja realmente inutilizar?", function(btn, text) {
		if (btn == 'yes') 
		{
			var dados = $.toJSON(listaEnvio);
			$.ajax({
				url: "<%= Url.Action("Inutilizacao") %>",
				type: "POST",
				dataType: 'json',
				data: dados,
				contentType: "application/json; charset=utf-8",
				success: function(result) {
					if(result.success) {
						Ext.Msg.show({
							title: "Mensagem de Informação",
							msg: "Sucesso na Inutilização do(s) CTe(s)!",
							buttons: Ext.Msg.OK,
							minWidth: 200
						});

						ds.removeAll();
						dsStatus.removeAll();
						
						filters.getForm().submit({
							url: '<%= Url.Action("ObterCtes") %>',
							reset: false
						})
						grid.getStore().load();
					} 
					else {
						Ext.Msg.show({
								title: "Mensagem de Erro",
								msg: "Erro na Inutilização do(s) CTe(s)!</BR>" + result.Message,
								buttons: Ext.Msg.OK,
								icon: Ext.MessageBox.ERROR,
								minWidth: 200
							});
					}
				},
				failure: function(result) {
					Ext.Msg.show({
						title: "Mensagem de Erro",
						msg: "Erro na Inutilização do(s) CTe(s)!</BR>" + result.Message,
						buttons: Ext.Msg.OK,
						icon: Ext.MessageBox.ERROR,
						minWidth: 200
					});
				}
			});
			}
		}));
					},
	                scope: this
	            },
                {
                    text: 'Reemissão',
                    id: 'reemissao',
                    iconCls: 'icon-save',
                    readOnly: true,
                    handler: function (b, e) {	
                    
                        var listaEnvio = Array();
                        /*var selected; //= sm2.getSelections();

                        for (var i = 0; i < selected.length; i++) {
                           if(selected[i].get('SituacaoCte') == 'EAR')
								listaEnvio.push(selected[i].data.CteId);
                        }
						*/
	var selected = smRaiz.getSelections();

	for (var i = 0; i < selected.length; i++) {
		if (selected[i].get('SituacaoCte') == 'EAR')
			listaEnvio.push(selected[i].data.CteId);
	}

	if (Ext.Msg.confirm("Reemissão", "Você possui " + listaEnvio.length + " para reemissão, deseja realmente Reemitir?", function(btn, text) {
		if (btn == 'yes') {
			var dados = $.toJSON(listaEnvio);
			$.ajax({
				url: "<%= Url.Action("Reemissao") %>",
				type: "POST",
				dataType: 'json',
				data: dados,
				contentType: "application/json; charset=utf-8",
				success: function(result) {
					if (result.success) {
						Ext.Msg.show({
							title: "Mensagem de Informação",
							msg: "Sucesso na Reemissão do(s) CTE(s)!",
							buttons: Ext.Msg.OK,
							// icon: Ext.MessageBox.Informational,
							minWidth: 200
						});
						ds.removeAll();
						dsStatus.removeAll();

						filters.getForm().submit({
							url: '<%= Url.Action("ObterCtes") %>',
							reset: false
						})
						grid.getStore().load();
					}
					else {
						Ext.Msg.show({
							title: "Mensagem de Erro",
							msg: "Erro na Reemissão do(s) CTe(s)!</BR>" + result.Message,
							buttons: Ext.Msg.OK,
							icon: Ext.MessageBox.ERROR,
							minWidth: 200
						});
					}
				},
				failure: function(result) {
					Ext.Msg.show({
						title: "Mensagem de Erro",
						msg: "Erro na Reemissão do(s) CTe(s)!</BR>" + result.Message,
						buttons: Ext.Msg.OK,
						icon: Ext.MessageBox.ERROR,
						minWidth: 200
					});
				}
			});
		}
	}));
                    },
                    scope: this
                },
                 {
                    text: 'Exportar',
                    type: 'submit',
                    iconCls: 'icon-page-excel',
                    handler: function (b, e) {
                         Pesquisar(true);
                    }
                 }
            ]
        });

        var colunaDet1 = {
                    title: 'CTe Raiz',
                    width: 450,
                    height: 320,
                    layout: 'form',
                    border: true,
                    items: [grid]
        };
		/*
        var colunaDet2 = {
                    title: 'Cte filho',
                    width: 350,
                    height: 380,
                    layout: 'form',
                    border: true,
                    items: [gridCteFilho]
        };
		*/
        var colunaDet3 = {
                    title: 'Status',
                    width: 560,
                    height: 320,
                    layout: 'form',
                    border: true,
                    items: [gridStatus]
        };

        var columns = {
			    layout: 'column',
			    border: false,
				autoScroll: true,		
                region: 'center',
			    items: [ colunaDet1, /*colunaDet2,*/ colunaDet3]
        };
        
        new Ext.Viewport({
				layout: 'border',
				margins: 10,
				items: [
					{
						region: 'north',
						height: 240,
						width: 700,
						items: [{
							region: 'center',
							applyTo: 'header-content'
						}, filters]
					},
					columns
			    ]
	    });
    });

    /*
    Ext.onReady(function () {
        var dataAtual = new Date();
        
        Ext.getCmp("filtro-data-inicial").setValue(dataAtual.format('d/m/Y'));

        Ext.getCmp("filtro-data-final").setValue(dataAtual.format('d/m/Y'));

		});
        */
    </script>
    <style>
        .corGreen
        {
            background-color: #98FB98;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div id="header-content">
        <h1>
            CTe - Monitoramento</h1>
        <small>Você está em CTe > Monitoramento</small>
        <br />
    </div>
</asp:Content>
