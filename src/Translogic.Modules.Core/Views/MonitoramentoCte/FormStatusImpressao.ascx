﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%@ Import Namespace="System.Globalization" %>
<%@ Import Namespace="Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico" %>
<div id="div-form-status-impressao">
</div>
<script type="text/javascript">
    var arrayCtes = new Array();

    <%
        var arrayCtes = (int[]) (ViewData["ID_CTE_IMPRESSAO"]);
        foreach (int cteId in arrayCtes)
        {
            %> arrayCtes.push('<%= cteId %>' ); <%
        }
    %>
   

 var storeImpressao = new Ext.data.JsonStore({
        url: '<%= Url.Action("ObterCteImpressao") %>',        
        //autoLoad: false,
        remoteSort: true,
        root: "Items",
        fields: [
                    'CteId',
                    'NroCte',
                    'Status'
			    ]
    });
    
    function fctObterRegistrosAprovados()
    {
        var arrRegistro = new Array();

        storeImpressao.each(
			    function (record) {
                    if(record.data.Status)
                    { 
                       arrRegistro.push(record.data.CteId);
                    }
			    }
		    );
      return arrRegistro;
    }

    var gridStatusImpressao = new Ext.grid.GridPanel({
                                viewConfig: {
                                    forceFit: true
                                },
                                height: 250,
                                stripeRows: true,
                                autoLoad: false,
                                region: 'center',
                                store: storeImpressao ,
                               loadMask: { msg: App.Resources.Web.Carregando },
                                colModel: new Ext.grid.ColumnModel({
                                    defaults: {
                                        sortable: false
                                    },
                                    columns: [
						                        new Ext.grid.RowNumberer(),
                                                { dataIndex: "CteId", hidden: true },
						                        { header: 'Nro Cte', dataIndex: "NroCte", sortable: false, width: 100 },
				                                { header: 'Status', dataIndex: "Status", sortable: false, width: 50, renderer:statusRenderer }
					                        ]
                                })                               
                            });


function statusRenderer(val)
      {
        switch(val){
            case true:
                return "<img src='<%=Url.Images("Icons/tick.png") %>' >";
                break;
            default:
                return "<img src='<%=Url.Images("Icons/cross.gif") %>' >";
                break;
        }
      }   

      var legenda = new Ext.Toolbar({
        layout:'column',
        items: [
            {
                xtype: 'button',
                iconCls: 'icon-tick',
                id: 'btnAprovados',
                text: '<br/>'
            },{
                xtype: 'button',
                iconCls: 'icon-delete',
                 id: 'btnNaoAprovados',
                text: ''
            }
        ]
    })

  var formStatusImpressao = new Ext.form.FormPanel
	        ({
	            id: 'formStatusImpressao',
	            labelWidth: 80,
	            width: 400,
	            bodyStyle: 'padding: 15px',
	            labelAlign: 'top',
	            items: [gridStatusImpressao],
	            buttonAlign: "center",
	            buttons:
		        [{
		            text: "Confirmar",
		            handler: function () {

                    var arrAprovados = fctObterRegistrosAprovados();
                    var stringAprovados = "";
                    var count = 0;
                    
                     storeImpressao.each(
			                function (record) {
                                if(record.data.Status)
                                { 
                                     stringAprovados +=  record.data.CteId +"$";
                                     count++;
                                }
			                }
		                );

                    if(count == 0)
                    {
                            Ext.Msg.alert('Alerta','Não foi encontrado nenhum arquivo para impressão.');
                            return;
                    }

                     var url = '<%=Url.Action("Imprimir") %>';
					 url = url + "?idCte=" + stringAprovados;
					 window.location = url;
					 
		               /*
                        var arrAprovados = fctObterRegistrosAprovados();
                         windowGerandoImpressao = new Ext.Window({
				                        id: 'windowGerandoImpressao',
				                        title: 'Gerando impressão...',
				                        modal: true,
				                        width: 100,
				                        height: 100,
				                        autoLoad: {
					                        url: '<%= Url.Action("Imprimir") %>',
                                            params:{ 'idCte': arrAprovados}, 
					                        scripts: true
				                        }
			                        });
			                        windowGerandoImpressao.show();

/*
                       var url = '<%=Url.Action("Imprimir") %>';
						url = url + "?idCte=" + arrAprovados;

						window.location = url;*/						
				
					}					
		        },
		        {
		            text: "Cancelar",
		            handler: function () {
		                windowStatusImpressao.close();
						
		            }
		        }],
                bbar: legenda       
	        });

    formStatusImpressao.render("div-form-status-impressao");
    
    storeImpressao.load({
        params: {'arrayCtes': arrayCtes },
        callback: function() { 
            var arrRegistroAprovados =  fctObterRegistrosAprovados();
            var totalAprovados = arrRegistroAprovados.length;
            var total = storeImpressao.getCount();
            var msg = 'CTe para impressão: ' + totalAprovados +' CTe de ' + total + ' selecionados.';
            var msgNOK = 'Cte que não tiveram arquivos gerados: ' + (total - totalAprovados) ;
            
            Ext.getCmp('btnAprovados').setText(msg);
            Ext.getCmp('btnNaoAprovados').setText(msgNOK);
				 
		}
    });

</script>
