﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div id="div-form-add-conf">
</div>
<script type="text/javascript">

    /* region :: Functions */
    function configuraOpcaoEdicao() {
        var codigoVagao = '<%=ViewData["codigoVagao"] != null ? ViewData["codigoVagao"].ToString() : "" %>';
        var peso = '<%=ViewData["peso"] != null ? ViewData["peso"].ToString() : "" %>';
        var id = '<%=ViewData["id"] != null ? ViewData["Id"].ToString() : "" %>';

        Ext.getCmp("txtCodigoVagaoEd").setValue(codigoVagao);
        Ext.getCmp("txtPesoEd").setValue(peso);
        Ext.getCmp("hiddenId").setValue(id);

        Ext.getCmp("txtCodigoVagaoEd").setDisabled((id != ''));

    }

    function eValido() {
        var codigoVagaoValue = Ext.getCmp("txtCodigoVagaoEd").getValue();
        var peso = Ext.getCmp("txtPesoEd").getValue();

        if (codigoVagaoValue == '')
            Ext.getCmp("txtCodigoVagaoEd").markInvalid("Campo obrigatório.");

        if (peso == '')
            Ext.getCmp("txtPesoEd").markInvalid("Campo obrigatório.");

        return (codigoVagaoValue != '' && peso != '');
    }
    /* endregion :: Functions */

    /* region :: Filtros */

    var hiddenId = {
        name: 'hiddenId',
        id: 'hiddenId',
        xtype: 'hidden'
    };

    var txtCodigoVagaoEd = {
        xtype: 'textfield',
        fieldLabel: 'Código Vagão',
        id: 'txtCodigoVagaoEd',
        name: 'txtCodigoVagaoEd',
        maskRe: /[0-9]/,
        width: 85
    };

    var txtPesoEd = {
        xtype: 'textfield',
        fieldLabel: 'Peso',
        id: 'txtPesoEd',
        name: 'txtPesoEd',
        width: 85
    };

    /* endregion :: Filtros */

    /* region :: Modal */

    var modal = new Ext.form.FormPanel({
        id: 'form-add-conf-modal',
        labelWidth: 100,
        width: 400,
        resizable: false,
        bodyStyle: 'padding: 15px',
        items: [txtCodigoVagaoEd, txtPesoEd, hiddenId],
        buttonAlign: 'center',
        buttons: [{
            text: 'Salvar',
            id: 'btnSalvar',
            iconCls: 'icon-save',
            handler: function () {

                if (!eValido()) {
                    Ext.Msg.show({
                        title: 'Error',
                        msg: "Informe os campos obrigatórios",
                        buttons: Ext.Msg.ERROR,
                        icon: Ext.MessageBox.ERROR
                    });
                    return;
                }

                Ext.getCmp("btnSalvar").setDisabled(true);
                window.jQuery.ajax({
                    url: '<%= Url.Action("Salvar") %>',
                    type: "POST",
                    data: {
                        id: Ext.getCmp("hiddenId").getValue(),
                        codigoVagao: Ext.getCmp("txtCodigoVagaoEd").getValue(),
                        peso: Ext.getCmp("txtPesoEd").getValue()
                    },
                    dataType: "json",
                    success: function (data) {
                        var iconeCadastro = window.Ext.MessageBox.ERROR;
                        if (data.Success == true) {
                            iconeCadastro = window.Ext.MessageBox.SUCCESS;
                        } else {
                            Ext.getCmp("btnSalvar").setDisabled(false);
                        }

                        window.Ext.Msg.show({
                            title: "Salvar",
                            msg: data.Message,
                            buttons: window.Ext.Msg.OK,
                            icon: iconeCadastro,
                            minWidth: 200,
                            fn: function () {
                                if (data.Success) {
                                    Ext.getCmp("form-add-conf").close();
                                } else {
                                    Ext.getCmp("btnSalvar").setDisabled(false);
                                }
                            },
                            close: function () {
                                if (data.Success) {
                                    Ext.getCmp("form-add-conf").close();
                                }
                            }
                        });
                    },
                    error: function (jqXHR, textStatus) {
                        window.Ext.Msg.show({
                            title: "Erro no Servidor",
                            msg: 'Ocorreu um erro no servidor, por favor, tente novamente mais tarde.',
                            buttons: window.Ext.Msg.OK,
                            icon: window.Ext.MessageBox.ERROR,
                            minWidth: 200
                        });
                    }
                });
            }
        }
                ]
    });

    /* endregion :: Modal*/

    modal.render("div-form-add-conf");
    configuraOpcaoEdicao();

</script>
