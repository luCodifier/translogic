﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Interna.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content runat="server" ID="HeadContent" ContentPlaceHolderID="HeadContent">
    <div id="header-content">
        <h1>
            Limite de peso especial</h1>
        <br />
    </div>
    <script type="text/javascript" language="javascript">
        /* region       :: Functions */
        var formModal = null;
        var posicaoRow = -1;
                
        function EditarRender(id) {
            return "<img style=\"cursor: pointer; cursor: hand;\" title=\"Editar\" src=\"<%=Url.Images("Icons/email_edit.png") %>\" alt=\"Editar\" onclick=\"Editar('" + id + "','Edição de registro')\">";
        }
                
        function DeletarRender(id) {
            return "<img style=\"cursor: pointer; cursor: hand;\" title=\"Deletar\" src=\"<%=Url.Images("Icons/delete.png") %>\" alt=\"Deletar\" onclick=\"DeletarRegistro('" + id + "')\">";
        }
                
        function GerenciarRenderer(id) {
            var html = '';
            var espaco = '&nbsp';
                    
            html += EditarRender(id);
            html += (espaco + DeletarRender(id));

            return html;
        }
                
        function Pesquisar() {
            grid.getStore().load();
        }

        function Limpar() {
            grid.getStore().removeAll();
            Ext.getCmp("filtros").getForm().reset();
        }
                
        function DeletarRegistro(id) {
            if(!confirm("Confirma exclusão deste registro?"))
                return;
                    
            Ext.Ajax.request({
                url: '<%= Url.Action("DeletarRegistro") %>',
                method: "POST",
                params: { id: id },
                success: function(response, options) {
                    var result = Ext.decode(response.responseText);
                                
                    if (result.Success == true) {
                        Pesquisar();

                        Ext.Msg.show({
                            title: 'Aviso',
                            msg: result.Message,
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.INFO
                        });
                    } else {
                        Ext.Msg.show({
                            title: 'Aviso',
                            msg: result.Message,
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.ERROR
                        });
                    }

                    Ext.getBody().unmask();
                },
                failure: function(response, options) {
                    Ext.Msg.show({
                        title: 'Aviso',
                        msg: response.Message,
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.ERROR
                    });

                    Ext.getBody().unmask();
                }
            });
        }

        function Editar(id,title) {
            posicaoRow = 1;
            formModal = new Ext.Window({
                id: 'form-add-conf',
                title: title,
                modal: true,
                resizable: false,
                width: 400,
                height: 150,
                autoLoad:
                    {
                        url: '<%= Url.Action("Editar") %>',
                        params  : { id: id },
                        scripts: true,
                        callback: function (el, success, c) {
                            posicaoRow = -1;
                            if (!success) {
                                 formModal.close();
                            }
                        }
                    },
                listeners: {
                    destroy: function () {
                        if (grid) {
                            grid.getStore().reload();
                        }
                    }
                }
            });

            formModal.show();
        }
        /* endregion    :: Functions */

        /* region       :: Filtro */
        var txtDataIni = {
            xtype: 'datefield',
            fieldLabel: 'Data Inicial',
            id: 'txtDataIni',
            name: 'txtDataIni',
            width: 85,
            allowBlank: false,
            value: new Date()
        };
        
        var txtDataFim = {
            xtype: 'datefield',
            fieldLabel: 'Data Final',
            id: 'txtDataFim',
            name: 'txtDataFim',
            width: 85,
            allowBlank: false,
            value: new Date()
        };
        
        var txtCodigoVagao = {
            xtype: 'textfield',
            fieldLabel: 'Código Vagão',
            id: 'txtCodigoVagao',
            name: 'txtCodigoVagao',
            maskRe: /[0-9]/,
            width: 85
        };
        
         var txtPeso = {
            xtype: 'textfield',
            fieldLabel: 'Peso',
            id: 'txtPeso',
            name: 'txtPeso',
            width: 85
        };

        var item1 = {
            width: 90,
            layout: 'form',
            border: false,
            items: [txtDataIni]
        };
        
         var item2 = {
            width: 90,
            layout: 'form',
            border: false,
            items: [txtDataFim]
        };
         
         var item3 = {
            width: 90,
            layout: 'form',
            border: false,
            items: [txtCodigoVagao]
        };
         
         var item4 = {
            width: 90,
            layout: 'form',
            border: false,
            items: [txtPeso]
        };

        var arrlinha1 = {
            layout: 'column',
            border: false,
            items: [item1, item2,item3,item4]
        };

        var arrCampos = new Array();
        arrCampos.push(arrlinha1);

        var filtros = new Ext.form.FormPanel({
            id: 'filtros',
            title: 'Filtros',
            name: 'filtros',
            region: 'center',
            bodyStyle: 'padding: 15px',
            initEl: function (el) {
                this.el = Ext.get(el);
                this.id = this.el.id || Ext.id();
                if (this.standardSubmit) {
                    this.el.dom.action = this.url;
                } else {
                    this.el.on('submit', this.onSubmit, this);
                }
                this.el.addClass('x-form');
            },
            labelAlign: 'top',
            border: false,
            items: [arrCampos],
            buttonAlign: 'center',
            buttons: [
                {
                    name: 'btnPesquisar',
                    id: 'btnPesquisar',
                    text: 'Pesquisar',
                    iconCls: 'icon-find',
                    handler: Pesquisar
                },
                {
                    name: 'btnLimpar',
                    id: 'btnLimpar',
                    text: 'Limpar',
                    handler: Limpar
                }
            ]
        });
        /* endregion    :: Filtro */

        /* region       :: Grid */
        //Colunas
        var cm = new Ext.grid.ColumnModel({
            defaults: {
                sortable: true
            },
            columns: [
                {dataIndex: "Id", hidden: true },
                new Ext.grid.RowNumberer(),
                {
                    header: 'Ações',
                    dataIndex: 'Id',
                    width: 65,
                    align: "center",
                    sortable: false,
                    renderer: function (value, metaData, record, rowIndex, colIndex, store) {
                        return GerenciarRenderer(record.data.Id);
                    }
                },
                { header: 'Codigo Vagão', dataIndex: 'CodigoVagao', width: 100 },
                { header: 'Peso', dataIndex: 'Peso', width: 100 },
                { header: 'Data', dataIndex: 'Data', width: 100}
            ]
        });

        //Botão adicionar
        var btnNovo = new Ext.Button({
            id: 'btnNovoItem',
            name: 'btnNovoItem',
            text: 'Novo',
            iconCls: 'icon-go',
            handler: function () { Editar("", "Novo registro"); }
        });

        var grid = new Translogic.PaginatedGrid({
            id: 'grid',
            name: 'grid',
            autoLoadGrid: false,
            region: 'center',
            viewConfig: { emptyText: 'Não possui dado(s) para exibição.' },
            filteringToolbar: [btnNovo],
            loadMask: { msg: App.Resources.Web.Carregando },
            fields: [
                'Id',
                'CodigoVagao',
                'Peso',
                'Data'
            ],
            cm: cm,
            stripeRows: true,
            url: '<%= Url.Action("Pesquisar") %>'
        });

        //Parametros do filtro
        grid.getStore().proxy.on('beforeload', function (p, params) {
            var dtInicial = Ext.getCmp("txtDataIni").getValue();
            var dtFim = Ext.getCmp("txtDataFim").getValue();
            var vagao = Ext.getCmp("txtCodigoVagao").getValue();
            var peso = Ext.getCmp("txtPeso").getValue();
            
            var dataInicial = new Array(dtInicial.format('d/m/Y') + " " + dtInicial.format('H:i:s'));
            var dataFinal = new Array(dtFim.format('d/m/Y') + " " + dtFim.format('H:i:s'));
            
            params["dtInicial"] = dataInicial;
            params["dtFim"] = dataFinal;
            params["codigoVagao"] = vagao;
            params["peso"] = peso;
            
        });
        
        /* endregion    :: Grid */

        /* region       :: Render */
        new Ext.Viewport({
            layout: 'border',
            margins: 10,
            items: [{
                    region: 'north',
                    height: 230,
                    autoScroll: true,
                    items: [{
                            region: 'center',
                            applyTo: 'header-content'
                        },
                        filtros]
                },
                grid]
        });
    /* endregion        :: Render */
    </script>
</asp:Content>
