﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Interna.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content runat="server" ID="HeadContent" ContentPlaceHolderID="HeadContent">
    <div id="header-content">
        <h1>
            Configuração Despacho Automático</h1>
        <br />
    </div>
    <script type="text/javascript" language="javascript">
        /* region       :: JsonStore */
        var dsClientes = new Ext.data.JsonStore({
            root: "Items",
            autoLoad: true,
            url: '<%= Url.Action("ObterClientes") %>',
            fields: [
                'Id',
                'Descricao'
            ],
            listeners: {
                load: function (store, records) {
                    AddRowCombobox("ddlCliente", store);
                }
            }
        });
        
        var dsAreaOperacional = new Ext.data.JsonStore({
            root: "Items",
            autoLoad: true,
            url: '<%= Url.Action("ObterAreaOperacional") %>',
            fields: [
                'Id',
                'Descricao'
            ],
            listeners: {
                load: function (store, records) {
                    AddRowCombobox("ddlAreaOperacional", store);
                }
            }
        });
        
        var dsLocalizacao = new Ext.data.JsonStore({
            root: "Items",
            autoLoad: true,
            url: '<%= Url.Action("ObterLocalizacao") %>',
            fields: [
                'Id',
                'Descricao'
            ],
            listeners: {
                load: function (store, records) {
                    AddRowCombobox("ddlLocalizacao", store);
                }
            }
        });
        
        var dsSegmento = new Ext.data.JsonStore({
            root: "Items",
            autoLoad: true,
            url: '<%= Url.Action("ObterSegmento") %>',
            fields: [
                'Id',
                'Descricao'
            ],
            listeners: {
                load: function (store, records) {
                    AddRowCombobox("ddlSegmento", store);
                }
            }
        });
        
        var dsSimNao = new Ext.data.JsonStore({
            root: "Items",
            autoLoad: true,
            url: '<%= Url.Action("ObterSimNao") %>',
            fields: [
                'Id',
                'Descricao'
            ]
        });
        
        /* endregion       :: JsonStore */
        
        /* region       :: Functions */
        var formModal = null;
        var posicaoRow = -1;        
        
        
        function AddRowCombobox(comboboxId,store) {
            var combobox = Ext.getCmp(comboboxId);
            var rt = store.recordType;
            store.insert(0, new rt({}));
        }
        
        function RetornaSelecionados() {
            var results = {itens: undefined};

            results.itens = new Array();
            var selection = sm.getSelections();

            for (var i = 0; i < selection.length; i++) {

                results.itens.push(selection[i].data.Id);
            }

            return results;
        }
                
        function EditarRender(id) {
            return "<img style=\"cursor: pointer; cursor: hand;\" title=\"Editar\" src=\"<%=Url.Images("Icons/email_edit.png") %>\" alt=\"Editar\" onclick=\"Editar('" + id + "','Edição de registro')\">";
        }
                
        function DeletarRender(id) {
            return "<img style=\"cursor: pointer; cursor: hand;\" title=\"Deletar\" src=\"<%=Url.Images("Icons/delete.png") %>\" alt=\"Deletar\" onclick=\"DeletarRegistro('" + id + "')\">";
        }
                
        function GerenciarRenderer(id) {
            var html = '';
            var espaco = '&nbsp';
                    
            html += EditarRender(id);
            html += (espaco + DeletarRender(id));

            return html;
        }
                
        function Pesquisar() {
            grid.getStore().load();
        }

        function Limpar() {
            grid.getStore().removeAll();
            Ext.getCmp("filtros").getForm().reset();
        }
        
        function Exportar() {
            var dtInicial = Ext.getCmp("txtDataIni").getValue();
            var dtFim = Ext.getCmp("txtDataFim").getValue();
            
            var cliente = Ext.getCmp("ddlCliente").getValue();
            var areaOperacional = Ext.getCmp("ddlAreaOperacional").getValue();
            var localizacao = Ext.getCmp("ddlLocalizacao").getValue();
            var segmento = Ext.getCmp("ddlSegmento").getValue();
            var terminal = Ext.getCmp("ddlTerminal").getValue();
            var patio = Ext.getCmp("ddlPatio").getValue();
            var manual = Ext.getCmp("ddlManual").getValue();

            var url = "<%= Url.Action("Exportar") %>";
            
            url += String.format("?dtInicial={0}", new Array(dtInicial.format('d/m/Y') + " " + dtInicial.format('H:i:s')));
            url += String.format("&dtFim={0}", new Array(dtFim.format('d/m/Y') + " " + dtFim.format('H:i:s')));
            url += String.format("&cliente={0}", cliente);
            url += String.format("&areaOperacional={0}", areaOperacional);
            url += String.format("&localizacaoId={0}", localizacao);
            url += String.format("&segmento={0}", segmento);
            url += String.format("&terminal={0}", terminal);
            url += String.format("&patio={0}", patio);
            url += String.format("&manual={0}", manual);

            window.open(url, "");
        }
                
        function DeletarRegistro(id) {
            if(!confirm("Confirma exclusão deste registro?"))
                return;
                    
            Ext.Ajax.request({
                url: '<%= Url.Action("DeletarRegistro") %>',
                method: "POST",
                params: { id: id },
                success: function(response, options) {
                    var result = Ext.decode(response.responseText);
                                
                    if (result.Success == true) {
                        Pesquisar();

                        Ext.Msg.show({
                            title: 'Aviso',
                            msg: result.Message,
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.INFO
                        });
                    } else {
                        Ext.Msg.show({
                            title: 'Aviso',
                            msg: result.Message,
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.ERROR
                        });
                    }

                    Ext.getBody().unmask();
                },
                failure: function(response, options) {
                    Ext.Msg.show({
                        title: 'Aviso',
                        msg: response.Message,
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.ERROR
                    });

                    Ext.getBody().unmask();
                }
            });
        }

        function Editar(id,title) {
            posicaoRow = 1;
            formModal = new Ext.Window({
                id: 'form-add-conf',
                title: title,
                modal: true,
                resizable: false,
                width: 400,
                height: 285,
                autoLoad:
                    {
                        url: '<%= Url.Action("Editar") %>',
                        params  : { id: id },
                        scripts: true,
                        callback: function (el, success, c) {
                            posicaoRow = -1;
                            if (!success) {
                                 formModal.close();
                            }
                        }
                    },
                listeners: {
                    destroy: function () {
                        if (grid) {
                            grid.getStore().reload();
                        }
                    }
                }
            });

            formModal.show();
        }
        /* endregion    :: Functions */

        /* region       :: Filtro */
        var txtDataIni = {
            xtype: 'datefield',
            fieldLabel: 'Data Inicial',
            id: 'txtDataIni',
            name: 'txtDataIni',
            width: 85,
            allowBlank: false,
            value: new Date()
        };
        
        var txtDataFim = {
            xtype: 'datefield',
            fieldLabel: 'Data Final',
            id: 'txtDataFim',
            name: 'txtDataFim',
            width: 85,
            allowBlank: false,
            value: new Date()
        };
        
        var ddlCliente = new Ext.form.ComboBox({
            typeAhead: false,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            store: dsClientes,
            valueField: 'Id',
            displayField: 'Descricao',
            fieldLabel: 'Cliente',
            id: 'ddlCliente',
            width: 225
        });

        var ddlAreaOperacional = new Ext.form.ComboBox({
            typeAhead: false,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            store: dsAreaOperacional,
            valueField: 'Id',
            displayField: 'Descricao',
            fieldLabel: 'Area operacional',
            id: 'ddlAreaOperacional',
            width: 100
        });

         var ddlLocalizacao = new Ext.form.ComboBox({
            typeAhead: false,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            store: dsLocalizacao,
            valueField: 'Id',
            displayField: 'Descricao',
            fieldLabel: 'Localização',
            id: 'ddlLocalizacao',
            width: 125
        });
         
         var ddlSegmento = new Ext.form.ComboBox({
            typeAhead: false,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            store: dsSegmento,
            valueField: 'Id',
            displayField: 'Descricao',
            fieldLabel: 'Segmento',
            id: 'ddlSegmento',
            width: 125
        });
         
         var ddlTerminal = new Ext.form.ComboBox({
            typeAhead: false,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            store: new Ext.data.JsonStore({
                        root: "Items",
                        autoLoad: true,
                        url: '<%= Url.Action("ObterSimNao") %>',
                        fields: [
                            'Id',
                            'Descricao'
                        ],
                        listeners: {
                            load: function (store, records) {
                                AddRowCombobox("ddlTerminal", store);
                            }
                        }}),
            valueField: 'Id',
            displayField: 'Descricao',
            fieldLabel: 'Terminal',
            id: 'ddlTerminal',
            width: 100
        });
        
        var ddlPatio = new Ext.form.ComboBox({
            typeAhead: false,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            store: new Ext.data.JsonStore({
                        root: "Items",
                        autoLoad: true,
                        url: '<%= Url.Action("ObterSimNao") %>',
                        fields: [
                            'Id',
                            'Descricao'
                        ],
                        listeners: {
                            load: function (store, records) {
                                AddRowCombobox("ddlPatio", store);
                            }
                        }}),
            valueField: 'Id',
            displayField: 'Descricao',
            fieldLabel: 'Patio',
            id: 'ddlPatio',
            width: 105
        });
        
        var ddlManual = new Ext.form.ComboBox({
            typeAhead: false,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            store: new Ext.data.JsonStore({
                        root: "Items",
                        autoLoad: true,
                        url: '<%= Url.Action("ObterSimNao") %>',
                        fields: [
                            'Id',
                            'Descricao'
                        ],
                        listeners: {
                            load: function (store, records) {
                                AddRowCombobox("ddlManual", store);
                            }
                        }}),
            valueField: 'Id',
            displayField: 'Descricao',
            fieldLabel: 'Manual',
            id: 'ddlManual',
            width: 100
        });

        var item1 = {
            width: 90,
            layout: 'form',
            border: false,
            items: [txtDataIni]
        };
        
         var item2 = {
            width: 90,
            layout: 'form',
            border: false,
            items: [txtDataFim]
        };
         
        var item3 = {
            width: 250,
            layout: 'form',
            border: false,
            items: [ddlCliente]
        };
        
        var item4 = {
            width: 120,
            layout: 'form',
            border: false,
            items: [ddlAreaOperacional]
        };
        
        var item5 = {
            width: 150,
            layout: 'form',
            border: false,
            items: [ddlLocalizacao]
        };
        
        var item6 = {
            width: 150,
            layout: 'form',
            border: false,
            items: [ddlSegmento]
        };
        
        var item7 = {
            width: 120,
            layout: 'form',
            border: false,
            items: [ddlTerminal]
        };
        
        var item8 = {
            width: 130,
            layout: 'form',
            border: false,
            items: [ddlPatio]
        };
        
        var item9 = {
            width: 120,
            layout: 'form',
            border: false,
            items: [ddlManual]
        };

        var arrlinha1 = {
            layout: 'column',
            border: false,
            items: [item1, item2, item3, item4,item5]
        };
        
        var arrlinha2 = {
            layout: 'column',
            border: false,
            items: [item6, item7, item8,item9]
        };

        var arrCampos = new Array();
        arrCampos.push(arrlinha1);
        arrCampos.push(arrlinha2);

        var filtros = new Ext.form.FormPanel({
            id: 'filtros',
            title: 'Filtros',
            name: 'filtros',
            region: 'center',
            bodyStyle: 'padding: 15px',
            initEl: function (el) {
                this.el = Ext.get(el);
                this.id = this.el.id || Ext.id();
                if (this.standardSubmit) {
                    this.el.dom.action = this.url;
                } else {
                    this.el.on('submit', this.onSubmit, this);
                }
                this.el.addClass('x-form');
            },
            labelAlign: 'top',
            border: false,
            items: [arrCampos],
            buttonAlign: 'center',
            buttons: [
                {
                    name: 'btnPesquisar',
                    id: 'btnPesquisar',
                    text: 'Pesquisar',
                    iconCls: 'icon-find',
                    handler: Pesquisar
                },
                {
                    name: 'btnLimpar',
                    id: 'btnLimpar',
                    text: 'Limpar',
                    handler: Limpar
                },
                {
                    id: 'btnExportar',
                    name: 'btnExportar',
                    text: 'Exportar',
                    iconCls: 'icon-page-excel',
                    handler: Exportar
                }
            ]
        });
        /* endregion    :: Filtro */

        /* region       :: Grid */
        //Colunas
        var cm = new Ext.grid.ColumnModel({
            defaults: {
                sortable: true
            },
            columns: [
                {dataIndex: "Id", hidden: true },
                new Ext.grid.RowNumberer(),
                {
                    header: 'Ações',
                    dataIndex: 'Id',
                    width: 65,
                    align: "center",
                    sortable: false,
                    renderer: function (value, metaData, record, rowIndex, colIndex, store) {
                        return GerenciarRenderer(record.data.Id);
                    }
                },
                { header: 'Cliente', dataIndex: 'Cliente', width: 70 },
                { header: 'Area operacional', dataIndex: 'AreaOperacional', width: 70 },
                { header: 'Localizacao', dataIndex: 'Localizacao', width: 70},
                { header: 'Segmento', dataIndex: 'Segmento', width: 70},
                { header: 'Terminal', dataIndex: 'Terminal', width: 70},
                { header: 'Patio', dataIndex: 'Patio', width: 70},
                { header: 'Manual', dataIndex: 'Manual', width: 70},
                { header: 'Data', dataIndex: 'Data', width: 70}
            ]
        });

        //Botão adicionar
        var btnNovo = new Ext.Button({
            id: 'btnNovoItem',
            name: 'btnNovoItem',
            text: 'Novo',
            iconCls: 'icon-go',
            handler: function () { Editar("", "Novo registro"); }
        });

        var grid = new Translogic.PaginatedGrid({
            id: 'grid',
            name: 'grid',
            autoLoadGrid: false,
            region: 'center',
            viewConfig: { emptyText: 'Não possui dado(s) para exibição.' },
            filteringToolbar: [btnNovo],
            loadMask: { msg: App.Resources.Web.Carregando },
            fields: [
                'Id',
                'Cliente',
                'AreaOperacional',
                'Localizacao',
                'Segmento',
                'Terminal',
                'Patio',
                'Manual',
                'Data'
            ],
            cm: cm,
            stripeRows: true,
            url: '<%= Url.Action("Pesquisar") %>'
        });

        //Parametros do filtro
        grid.getStore().proxy.on('beforeload', function (p, params) {
            
            var dtInicial = Ext.getCmp("txtDataIni").getValue();
            var dtFim = Ext.getCmp("txtDataFim").getValue();
            
            var dataInicial = new Array(dtInicial.format('d/m/Y') + " " + dtInicial.format('H:i:s'));
            var dataFinal = new Array(dtFim.format('d/m/Y') + " " + dtFim.format('H:i:s'));
            
            var cliente = Ext.getCmp("ddlCliente").getValue();
            var areaOperacional = Ext.getCmp("ddlAreaOperacional").getValue();
            var localizacao = Ext.getCmp("ddlLocalizacao").getValue();
            var segmento = Ext.getCmp("ddlSegmento").getValue();
            var terminal = Ext.getCmp("ddlTerminal").getValue();
            var patio = Ext.getCmp("ddlPatio").getValue();
            var manual = Ext.getCmp("ddlManual").getValue();
           
            params["cliente"] = cliente;
            params["localizacaoId"] = localizacao;
            params["areaOperacional"] = areaOperacional;
            params["segmento"] = segmento;
            params["terminal"] = terminal;
            params["patio"] = patio;
            params["manual"] = manual;
            params["dtInicial"] = dataInicial;
            params["dtFim"] = dataFinal;
        });
        
        /* endregion    :: Grid */

        /* region       :: Render */
        new Ext.Viewport({
            layout: 'border',
            margins: 10,
            items: [{
                    region: 'north',
                    height: 230,
                    autoScroll: true,
                    items: [{
                            region: 'center',
                            applyTo: 'header-content'
                        },
                        filtros]
                },
                grid]
        });
    /* endregion        :: Render */
    </script>
</asp:Content>
