﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div id="div-form-add-conf">
</div>
<script type="text/javascript">

    /* region :: Functions */
    function configuraOpcaoEdicao() {
        /*
        ViewData["terminal"] = (valor.Terminal == "S").ToString();
        ViewData["patio"] = (valor.Patio == "S").ToString();
        ViewData["manual"] = (valor.Manual == "S").ToString();
        */

        var terminal = '<%=ViewData["terminal"] != null ? ViewData["terminal"].ToString() : "" %>';
        var patio = '<%=ViewData["patio"] != null ? ViewData["patio"].ToString() : "" %>';
        var manual = '<%=ViewData["manual"] != null ? ViewData["manual"].ToString() : "" %>';
        var id = '<%=ViewData["id"] != null ? ViewData["id"].ToString() : "" %>';

        if (terminal == 'S') {
            Ext.getCmp("cbTerminalEd").setValue(true);
        }

        if (patio == 'S') {
            Ext.getCmp("cbPatioEd").setValue(true);
        }

        if (manual == 'S') {
            Ext.getCmp("cbManualEd").setValue(true);
        }

        Ext.getCmp("hiddenId").setValue(id);
    }

    function eValido() {
        var cliente = Ext.getCmp("ddlClienteEd").getValue();
        var areaOperacional = Ext.getCmp("ddlAreaOperacionalEd").getValue();
        var segmento = Ext.getCmp("ddlSegmentoEd").getValue();

        if (cliente == '')
            Ext.getCmp("ddlClienteEd").markInvalid("Campo Cliente é obrigatório.");

        if (areaOperacional == '')
            Ext.getCmp("ddlAreaOperacionalEd").markInvalid("Campo área operacional é obrigatório.");

        if (segmento == '')
            Ext.getCmp("ddlSegmentoEd").markInvalid("Campo segmento é obrigatório.");

        return (cliente != '' && areaOperacional != '' && segmento != '');
    }

    /* endregion :: Functions */

    /* region :: Filtros */

    var ddlClienteEd = new Ext.form.ComboBox({
        typeAhead: false,
        triggerAction: 'all',
        lazyRender: true,
        mode: 'local',
        store: dsClientes,
        valueField: 'Id',
        displayField: 'Descricao',
        fieldLabel: 'Cliente',
        id: 'ddlClienteEd',
        width: 225,
        value: '<%=ViewData["cliente"] != null ? ViewData["cliente"].ToString() : "" %>'
    });

    var ddlAreaOperacionalEd = new Ext.form.ComboBox({
        typeAhead: false,
        triggerAction: 'all',
        lazyRender: true,
        mode: 'local',
        store: dsAreaOperacional,
        valueField: 'Id',
        displayField: 'Descricao',
        fieldLabel: 'Area operacional',
        id: 'ddlAreaOperacionalEd',
        width: 225,
        value: '<%=ViewData["areaOperaciona"] != null ? ViewData["areaOperaciona"].ToString() : "" %>'
    });

    var ddlLocalizacaoEd = new Ext.form.ComboBox({
        typeAhead: false,
        triggerAction: 'all',
        lazyRender: true,
        mode: 'local',
        store: dsLocalizacao,
        valueField: 'Id',
        displayField: 'Descricao',
        fieldLabel: 'Localização',
        id: 'ddlLocalizacaoEd',
        width: 225,
        value: '<%=ViewData["localizacao"] != null ? ViewData["localizacao"].ToString() : "" %>'
    });

    var ddlSegmentoEd = new Ext.form.ComboBox({
        typeAhead: false,
        triggerAction: 'all',
        lazyRender: true,
        mode: 'local',
        store: dsSegmento,
        valueField: 'Id',
        displayField: 'Descricao',
        fieldLabel: 'Segmento',
        id: 'ddlSegmentoEd',
        width: 225,
        value: '<%=ViewData["segmento"] != null ? ViewData["segmento"].ToString() : "" %>'
    });

    var cbTerminalEd = {
        xtype: 'checkbox',
        fieldLabel: 'Terminal',
        id: 'cbTerminalEd',
        name: 'cbTerminalEd'
    };

    var cbPatioEd = {
        xtype: 'checkbox',
        fieldLabel: 'Patio',
        id: 'cbPatioEd',
        name: 'cbPatioEd'
    };

    var cbManualEd = {
        xtype: 'checkbox',
        fieldLabel: 'Manual',
        id: 'cbManualEd',
        name: 'cbManualEd'
    };

    var hiddenId = {
        name: 'hiddenId',
        id: 'hiddenId',
        xtype: 'hidden'
    };
    /* endregion :: Filtros */

    /* region :: Modal */

    var modal = new Ext.form.FormPanel({
        id: 'form-add-conf-modal',
        labelWidth: 100,
        width: 400,
        resizable: false,
        bodyStyle: 'padding: 15px',
        items: [ddlClienteEd, ddlAreaOperacionalEd, ddlLocalizacaoEd, ddlSegmentoEd, cbManualEd, cbPatioEd, cbTerminalEd, hiddenId],
        buttonAlign: 'center',
        buttons: [{
            text: 'Salvar',
            id: 'btnSalvar',
            iconCls: 'icon-save',
            handler: function () {
                
                if (!eValido()) {
                    Ext.Msg.show({
                        title: 'Error',
                        msg: "Informe os campos obrigatórios",
                        buttons: Ext.Msg.ERROR,
                        icon: Ext.MessageBox.ERROR
                    });
                    return;
                }

                Ext.getCmp("btnSalvar").setDisabled(true);
                window.jQuery.ajax({
                    url: '<%= Url.Action("Salvar") %>',
                    type: "POST",
                    data: {
                        cliente: Ext.getCmp("ddlClienteEd").getValue(),
                        areaOperacional: Ext.getCmp("ddlAreaOperacionalEd").getValue(),
                        localizacao: Ext.getCmp("ddlLocalizacaoEd").getValue(),
                        segmento: Ext.getCmp("ddlSegmentoEd").getValue(),
                        terminal: Ext.getCmp("cbTerminalEd").getValue(),
                        patio: Ext.getCmp("cbPatioEd").getValue(),
                        manual: Ext.getCmp("cbManualEd").getValue(),
                        id: Ext.getCmp("hiddenId").getValue()
                    },
                    dataType: "json",
                    success: function (data) {
                        var iconeCadastro = window.Ext.MessageBox.ERROR;
                        if (data.Success == true) {
                            iconeCadastro = window.Ext.MessageBox.SUCCESS;
                        } else {
                            Ext.getCmp("btnSalvar").setDisabled(false);
                        }

                        window.Ext.Msg.show({
                            title: "Salvar",
                            msg: data.Message,
                            buttons: window.Ext.Msg.OK,
                            icon: iconeCadastro,
                            minWidth: 200,
                            fn: function () {
                                if (data.Success) {
                                    Ext.getCmp("form-add-conf").close();
                                } else {
                                    Ext.getCmp("btnSalvar").setDisabled(false);
                                }
                            },
                            close: function () {
                                if (data.Success) {
                                    Ext.getCmp("form-add-conf").close();
                                }
                            }
                        });
                    },
                    error: function (jqXHR, textStatus) {
                        window.Ext.Msg.show({
                            title: "Erro no Servidor",
                            msg: 'Ocorreu um erro no servidor, por favor, tente novamente mais tarde.',
                            buttons: window.Ext.Msg.OK,
                            icon: window.Ext.MessageBox.ERROR,
                            minWidth: 200
                        });
                    }
                });
            }
        }
                ]
    });

    /* endregion :: Modal*/

    modal.render("div-form-add-conf");
    configuraOpcaoEdicao();

</script>
