﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Interna.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content runat="server" ID="HeadContent" ContentPlaceHolderID="HeadContent">
    <style>
        .corBranco
        {
            background-color: #FFFFFF;
        }
        .corVermelho
        {
            background-color: #FFB0C4;
        }
        .corAmarelo
        {
            background-color: #FFFF80;
        }
        .corVerde
        {
            background-color: #B0FFC5;
        }
    </style>
    <script type="text/javascript">

        var windowMotivosVagoes;

        //#Region StoreDDLs

        var lotacaoStore = new Ext.data.JsonStore({
            root: "Items",
            autoLoad: true,
            proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("ObterLotacoes", "MotivoSituacaoVagao") %>', timeout: 600000 }),
            id: 'lotacaoStore',
            fields: ['IdLotacao','DescricaoLotacao']
        });
    
         var situcaoStore = new Ext.data.JsonStore({
            root: "Items",
            autoLoad: true,
            proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("ObterSituacoes", "MotivoSituacaoVagao") %>', timeout: 600000 }),
            id: 'situcaoStore',
            fields: ['IdSituacao','DescricaoSituacao']
        
         });

        var motivoStore = new Ext.data.JsonStore({
            root: "Items",
            autoLoad: true,
            proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("ObterMotivos", "MotivoSituacaoVagao") %>', timeout: 600000 }),
            id: 'motivoStore',
            fields: ['IdMotivo','DescricaoMotivo']
        });

        //#endRegion

        var sm = new Ext.grid.CheckboxSelectionModel({
                listeners: {
                    selectionchange: function() {
                        var recLen = Ext.getCmp('gridPainelMotivosVagoes').store.getRange().length;
                        var selectedLen = this.selections.items.length;
                        if (selectedLen == recLen) {
                            var view = Ext.getCmp('gridPainelMotivosVagoes').getView();
                            var chkdiv = Ext.fly(view.innerHd).child(".x-grid3-hd-checker");
                            chkdiv.addClass("x-grid3-hd-checker-on");
                        }
                    }
                },
                rowdeselect: function(rowIndex, record) {
                    var view = Ext.getCmp('grid').getView();
                    var chkdiv = Ext.fly(view.innerHd).child(".x-grid3-hd-checker");
                    chkdiv.removeClass('x-grid3-hd-checker-on');
                }
            });

        //#Region StoreGridMotivoVagoes

        var gridMotivoVagoesStore = new Ext.data.JsonStore({
            root: "Items",
            totalProperty: 'Total',
            id: 'gridMotivoVagoesStore',
            name: 'gridMotivoVagoesStore',
            proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("ConsultarMotivosVagoes", "MotivoSituacaoVagao") %>', timeout: 600000 }),
            paramNames: {
				sort: "detalhesPaginacaoWeb.Sort",
				dir: "detalhesPaginacaoWeb.Dir",
				start: "detalhesPaginacaoWeb.Start",
				limit: "detalhesPaginacaoWeb.Limit"
			},
            fields: ['NumeroVagao', 'IdVagao', 'Local', 'Serie', 'Linha', 'Sequencia', 'IdLotacao', 'DescricaoLotacao', 'IdSituacao', 'DescricaoSituacao', 'IdCondicaoDeUso', 'IdLocalizacao', 'DescricaoLocalizacao', 'IdVagaoMotivo', 'IdMotivo', 'DescricaoMotivo','DataEmissao','NomeUsuario']
        });
        
        //#endRegion     
       
        //#Region FuncoesDaTela

        function Pesquisar() {            
            gridMotivoVagoesStore.load({params:{start: 0,limit: 50}});
        }

        function AtualizaComboMotivos()  {
	        ddlMotivo.getStore().load();
        }

        function Limpar(){
            gridPainelMotivosVagoes.getStore().removeAll();
            Ext.getCmp("grid-filtros").getForm().reset();
        }

        function VerificaVagoesSelecionado() {
            var resultado = false; 
            var selection = sm.getSelections();
            var iCont = 0;

            if (selection && selection.length > 0) {
                var idSituacao = selection[0].data.IdSituacao;

                selection = sm.getSelections();

                $.each(selection, function(i, e) {
                    if (idSituacao == e.data.IdSituacao) {
                        iCont++;
                    }
                });

                resultado = selection.length == iCont ? true : false;
            }

            return resultado;
        }                          

        //#endRegion

        //#Region PainelMotivosVagoes
        
        var camposPainelMotivosVagoes = [
            { name: 'NumeroVagao', mapping: 'NumeroVagao' },
            { name: 'IDVagao', mapping: 'IdVagao' },
            { name: 'Local', mapping: 'Local' },
            { name: 'Serie', mapping: 'Serie' },
            { name: 'Linha', mapping: 'Linha' },
            { name: 'Sequencia', mapping: 'Sequencia' },
            { name: 'IdLotacao', mapping: 'IdLotacao' },
            { name: 'DescricaoLotacao', mapping: 'DescricaoLotacao' },
            { name: 'IdSituacao', mapping: 'IdSituacao' },
            { name: 'DescricaoSituacao', mapping: 'DescricaoSituacao' },
            { name: 'IdCondicaoDeUso', mapping: 'IdCondicaoDeUso' },
            { name: 'DescricaoCondicaoDeUso', mapping: 'DescricaoCondicaoDeUso' },
            { name: 'IdLocalizacao', mapping: 'IdLocalizacao' },
            { name: 'DescricaoLocalizacao', mapping: 'DescricaoLocalizacao' },
            { name: 'IDVagaoMotivo', mapping: 'IdVagaoMotivo' },        
            { name: 'IDMotivo', mapping: 'IdMotivo' },        
            { name: 'Motivo', mapping: 'DescricaoMotivo' },        
            { name: 'DataEmissao', mapping: 'DataEmissao'},
            { name: 'NomeUsuario', mapping: 'NomeUsuario'}
        ];

        $(function() {
            
            // Toolbar da Grid
            var pagingToolbar = new Ext.PagingToolbar({
                pageSize: 50,
                store: gridMotivoVagoesStore,
                displayInfo: true,
                displayMsg: App.Resources.Web.GridExibindoRegistros,
                emptyMsg: App.Resources.Web.GridSemRegistros,
                paramNames: {
                    start: "detalhesPaginacaoWeb.Start",
                    limit: "detalhesPaginacaoWeb.Limit"
                }
            });

            var detalheActionOrigens = new Ext.ux.grid.RowActions({
                dataIndex: '',
                header: '',
                align: 'center',
                actions: [
                    {
                        iconCls: 'icon-del',
                        tooltip: 'Excluir Motivo'
                    }],
                callbacks: {
                    'icon-del': function(grid, record, action, row, col) {

                        if (record.data.IdMotivo) {
                            if (Ext.Msg.confirm("Excluir", "Deseja realmente excluir esse Motivo ?", function(btn, text) {
                                if (btn == 'yes') {
                                    ExcluirMotivo(record.data.IdVagaoMotivo, record.data.IdVagao, record.data.IdMotivo);
                                    Pesquisar();
                                }

                            })) ;
                        }
                    }
                }
            });

            var cm = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true
                },
                columns: [
                    new Ext.grid.RowNumberer(),
                    sm,
                    { header: 'Num Vagão', dataIndex: "NumeroVagao", sortable: false, width: 70 },
                    { header: 'IDVagao', dataIndex: "IdVagao", sortable: false, width: 50, hidden: true },
                    { header: 'Série', dataIndex: "Serie", sortable: false, width: 50 },
                    { header: 'Local', dataIndex: "Local", sortable: false, width: 50 },
                    { header: 'Linha', dataIndex: "Linha", sortable: false, width: 75 },
                    { header: 'Sequencia', dataIndex: "Sequencia", sortable: false, width: 55 },
                    { header: 'Lotação', dataIndex: "DescricaoLotacao", sortable: false, width: 75 },
                    { header: 'Situação', dataIndex: "DescricaoSituacao", sortable: false, width: 90 },
                    { header: 'Condições de Uso', dataIndex: "DescricaoCondicaoDeUso", sortable: false, width: 120 },
                    { header: 'IDVagaoMotivo', dataIndex: "IdVagaoMotivo", sortable: false, width: 50, hidden: true },
                    { header: 'Motivo', dataIndex: "DescricaoMotivo", sortable: false, width: 200 },
                    detalheActionOrigens,
                    { header: 'Data de Emissão', dataIndex: "DataEmissao", sortable: false, width: 120 },
                    { header: 'Usuário', dataIndex: "NomeUsuario", sortable: false, width: 120 }
                ]
            });

            var gridPainelMotivosVagoes = new Ext.grid.EditorGridPanel({
                id: 'gridPainelMotivosVagoes',
                name: 'gridPainelMotivosVagoes',
                autoLoadGrid: false,
                height: 360,
                width: 360,
                stripeRows: true,
                cm: cm,
                region: 'center',
                autoScroll: true,
                loadMask: { msg: App.Resources.Web.Carregando },
                store: gridMotivoVagoesStore,
                plugins: [detalheActionOrigens],
                tbar: [
                    {
                        id: 'btnIncluir',
                        text: 'Incluir Motivo',
                        tooltip: 'Incluir Motivo',
                        iconCls: 'icon-new',
                        handler: function(c) {

                            var selection = sm.getSelections();

                            if (selection && selection.length > 0) {

                                if (VerificaVagoesSelecionado()) {
                                    CadastrarMotivoVagao();
                                } else {
                                    Ext.Msg.show({
                                        title: 'Aviso',
                                        msg: 'As situações entre os vagões são divergentes.',
                                        buttons: Ext.Msg.OK,
                                        icon: Ext.MessageBox.ERROR
                                    });
                                }
                            } else {
                                Ext.Msg.show({
                                    title: 'Aviso',
                                    msg: 'É necessário selecionar um vagão para incluir um motivo.',
                                    buttons: Ext.Msg.OK,
                                    icon: Ext.MessageBox.ERROR
                                });
                            }
                        }
                    },
                    {
                        id: 'btnExportar',
                        text: 'Exportar',
                        tooltip: 'Exportar',
                        iconCls: 'icon-page-excel',
                        handler: function(c) {

                            var url = "<%= this.Url.Action("ObterConsultarMotivosVagoesExportar") %>";

                            var listaDeVagoes = Ext.getCmp("txtListaVagao").getValue();
                            var idLocal = Ext.getCmp("txtLocal").getValue();
                            var idOS = Ext.getCmp("txtOS").getValue();

                            ddlLotacao = Ext.getCmp("ddlLotacao");
                            var idLotacao = (ddlLotacao.getValue() == "Todas") ? "0" : ddlLotacao.getValue();

                            ddlSituacao = Ext.getCmp("ddlSituacao");
                            var idSituacao = (ddlSituacao.getValue() == "Todas") ? "0" : ddlSituacao.getValue();

                            ddlMotivo = Ext.getCmp("ddlMotivo");
                            var idMotivo = (ddlMotivo.getValue() == "Todos") ? "0" : ddlMotivo.getValue();

                            url += "?listaDeVagoes=" + listaDeVagoes + '&idLocal=' + idLocal + '&idOS=' + idOS;
                            url += '&idLotacao=' + idLotacao + '&idSituacao=' + idSituacao + '&idMotivo=' + idMotivo;

                            window.open(url, "");
                        }
                    }
                ],
                bbar: pagingToolbar,
                sm: sm
            });

            gridPainelMotivosVagoes.getStore().proxy.on('beforeload', function(p, params) {

                var listaDeVagoes = Ext.getCmp("txtListaVagao").getValue();
                var idLocal = Ext.getCmp("txtLocal").getValue();
                var idOS = Ext.getCmp("txtOS").getValue();

                ddlLotacao = Ext.getCmp("ddlLotacao");
                var idLotacao = (ddlLotacao.getValue() == "Todas") ? "0" : ddlLotacao.getValue();

                ddlSituacao = Ext.getCmp("ddlSituacao");
                var idSituacao = (ddlSituacao.getValue() == "Todas") ? "0" : ddlSituacao.getValue();

                ddlMotivo = Ext.getCmp("ddlMotivo");
                var idMotivo = (ddlMotivo.getValue() == "Todos") ? "0" : ddlMotivo.getValue();

                params['listaDeVagoes'] = listaDeVagoes;
                params['idLocal'] = idLocal;
                params['idOS'] = idOS;
                params['idLotacao'] = idLotacao;
                params['idSituacao'] = idSituacao;
                params['idMotivo'] = idMotivo;
            });

            //#endRegion

            //#Region PesquisaFiltros

            var txtListaVagao = {
                xtype: 'textfield',
                name: 'txtListaVagao',
                id: 'txtListaVagao',
                fieldLabel: 'Lista de Vagões (separados por ";")',
                autoCreate: {
                    tag: 'input',
                    type: 'text',
                    autocomplete: 'off'
                },
                maskRe: /[0-9+;]/,
                style: 'text-transform: uppercase',
                width: 180
            };

            var txtLocal = {
                xtype: 'textfield',
                name: 'txtLocal',
                id: 'txtLocal',
                fieldLabel: 'Local',
                autoCreate: {
                    tag: 'input',
                    type: 'text',
                    autocomplete: 'off',
                    maxlength: '3'
                },
                maskRe: /[A-z]/,
                style: 'text-transform:uppercase;',
                width: 75
            };

            var txtOS = {
                xtype: 'textfield',
                name: 'txtOS',
                id: 'txtOS',
                fieldLabel: 'OS',
                autoCreate: {
                    tag: 'input',
                    type: 'text',
                    autocomplete: 'off',
                    maxlength: '3'
                },
                maskRe: /[0-9]/,
                style: 'text-transform:uppercase;',
                width: 75
            };

            var ddlLotacao = new Ext.form.ComboBox({
                editable: true,
                typeAhead: true,
                forceSelection: true,
                disableKeyFilter: true,
                triggerAction: 'all',
                lazyRender: true,
                mode: 'local',
                store: lotacaoStore,
                valueField: 'IdLotacao',
                displayField: 'DescricaoLotacao',
                fieldLabel: 'Lotação',
                id: 'ddlLotacao',
                width: 80,
                value: 'Todas'
            });

            var ddlSituacao = new Ext.form.ComboBox({
                typeAhead: false,
                triggerAction: 'all',
                lazyRender: true,
                mode: 'local',
                store: situcaoStore,
                valueField: 'IdSituacao',
                displayField: 'DescricaoSituacao',
                fieldLabel: 'Situação',
                id: 'ddlSituacao',
                width: 120,
                value: 'Todas',
                listeners: {
                    select: function() {
                        var ddlSituacaoLocal = Ext.getCmp("ddlSituacao");
                        var idSituacaoLocal = (ddlSituacao.getValue() == "Todas") ? "0" : ddlSituacaoLocal.getValue();
                        ddlMotivo.getStore().load({ params: { idSituacao: idSituacaoLocal } });
                        ddlMotivo.setValue(0);
                    }
                }
            });

            var ddlMotivo = new Ext.form.ComboBox({
                typeAhead: false,
                triggerAction: 'all',
                lazyRender: true,
                mode: 'local',
                store: motivoStore,
                fieldLabel: 'Motivo',
                valueField: 'IdMotivo',
                displayField: 'DescricaoMotivo',
                id: 'ddlMotivo',
                width: 160,
                value: 'Todos'
            });

            //Item do Array de Componentes (1 por cada componente criado)
            var arrListaVagao = {
                width: 195,
                layout: 'form',
                border: false,
                items: [txtListaVagao]
            };

            var arrLocal = {
                width: 85,
                layout: 'form',
                border: false,
                items: [txtLocal]
            };

            var arrOS = {
                width: 85,
                layout: 'form',
                border: false,
                items: [txtOS]
            };

            var arrLotacao = {
                width: 95,
                layout: 'form',
                border: false,
                items: [ddlLotacao]
            };

            var arrSituacao = {
                width: 135,
                layout: 'form',
                border: false,
                items: [ddlSituacao]
            };

            var arrMotivo = {
                width: 175,
                layout: 'form',
                border: false,
                items: [ddlMotivo]
            };

            //Array de componentes do form 
            var arrCampos = {
                layout: 'column',
                border: false,
                items: [arrListaVagao, arrLocal, arrOS, arrLotacao, arrSituacao, arrMotivo]
            };

            var arrButtons = {
                layout: 'column',
                border: false,
                items: [arrListaVagao, arrLocal, arrOS, arrLotacao, arrSituacao, arrMotivo]
            };

            //Conteudo do Form que sera criado
            var filtros = new Ext.form.FormPanel({
                id: 'grid-filtros',
                title: "Filtros",
                region: 'center',
                bodyStyle: 'padding:15px;',
                layout: 'column',
                labelAlign: 'top',
                items: [arrCampos],
                buttonAlign: "center",
                buttons:
                    [{
                            text: 'Pesquisar',
                            type: 'submit',
                            iconCls: 'icon-find',
                            handler: function(b, e) {
                                Pesquisar();
                            }
                        },
                        {
                            text: 'Limpar',
                            type: 'submit',
                            handler: function(b, e) {
                                Limpar();
                            }
                        }
                    ]
            });

            //#endRegion

            //#Region TelaCadastroMotivos

            var ddlCadastroSituacao = new Ext.form.ComboBox({
                typeAhead: true,
                triggerAction: 'all',
                lazyRender: true,
                mode: 'local',
                store: situcaoStore,
                valueField: 'IdSituacao',
                displayField: 'DescricaoSituacao',
                fieldLabel: 'Situação',
                id: 'ddlCadastroSituacao',
                width: 280,
                value: 'Todas'
            });

            var ddlCadastroMotivo = new Ext.form.ComboBox({
                typeAhead: true,
                triggerAction: 'all',
                lazyRender: true,
                mode: 'local',
                store: motivoStore,
                fieldLabel: 'Motivo',
                valueField: 'IdMotivo',
                displayField: 'DescricaoMotivo',
                id: 'ddlCadastroMotivo',
                width: 280
                //value: 'Todos'
            });

            var arrSituacaoCadastro = {
                width: 300,
                layout: 'form',
                border: false,
                items: [ddlCadastroSituacao]
            };

            var arrMotivoCadastro = {
                width: 300,
                layout: 'form',
                border: false,
                items: [ddlCadastroMotivo]
            };

            var arrCamposCadastro = {
                layout: 'column',
                border: false,
                items: [arrSituacaoCadastro, arrMotivoCadastro]
            };

            var formCadastroMotivoVagao = new Ext.form.FormPanel({
                id: 'formCadastroMotivoVagao',
                region: 'center',
                bodyStyle: 'padding:15px;',
                layout: 'column',
                labelAlign: 'top',
                items: [arrCamposCadastro],
                buttonAlign: "center",
                buttons:
                    [{
                            text: "Salvar",
                            handler: function() {
                                SalvarMotivosVagoes();
                            }
                        },
                        {
                            text: "Sair",
                            handler: function() {
                                windowMotivosVagoes.hide();
                            }
                        }]
            });

            function SalvarMotivosVagoes() {

                var motivoOk = ValidaMotivo();
                if (motivoOk == false) {
                    Ext.Msg.show({
                            title: 'Aviso',
                            msg: 'Favor selecione um motivo válido.',
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.ERROR
                        });

                    Ext.getBody().unmask();

                    return;
                }

                var data = RetornaDadosSelecionados();

                $.ajax({
                    url: '<%= Url.Action("SalvarMotivosVagoes", "MotivoSituacaoVagao" ) %>',
                    type: "POST",
                    dataType: 'json',
                    data: $.toJSON(data),
                    contentType: "application/json; charset=utf-8",
                    success: function(response, options) {
                        if (response.Success) {

                            Ext.Msg.show({
                                title: 'Aviso',
                                msg: 'Alterações salvas com sucesso!',
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.INFO
                            });

                            Pesquisar();

                            windowMotivosVagoes.hide();

                        } else {
                            Ext.Msg.show({
                                title: 'Aviso',
                                msg: response.Message,
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.ERROR
                            });
                        }

                        Ext.getBody().unmask();
                    },
                    failure: function(response, options) {
                        Ext.Msg.show({
                            title: 'Aviso',
                            msg: response.Message,
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.ERROR
                        });

                        Ext.getBody().unmask();
                    }
                });
            }

            function ExcluirMotivo(idVagaoMotivoLocal, idVagaoLocal, idMotivoLocal) {
                $.ajax({
                    url: '<%= Url.Action("ExcluirMotivoDoVagao", "MotivoSituacaoVagao" ) %>',
                    method: "POST",
                    async: false,
                    dataType: 'json',
                    data: "idVagaoMotivo=" + idVagaoMotivoLocal + "&IdVagao=" + idVagaoLocal + "&idMotivo=" + idMotivoLocal.toString(),
                    success: function(response) {
                        if (response.Success) {

                            Ext.Msg.show({
                                title: 'Aviso',
                                msg: 'Exclusão realizada com sucesso!',
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.INFO
                            });

                            Pesquisar();

                        } else {
                            Ext.Msg.show({
                                title: 'Aviso',
                                msg: response.Message,
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.ERROR
                            });
                        }

                        Ext.getBody().unmask();
                    },
                    failure: function(response, options) {
                        Ext.Msg.show({
                            title: 'Aviso',
                            msg: response.Message,
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.ERROR
                        });

                        Ext.getBody().unmask();
                    }
                });
            }

            function ValidaMotivo() {
                var idMotivoSelecionado = ddlCadastroMotivo.getValue();
                //console.log(idMotivoSelecionado);

                if (!idMotivoSelecionado) {
                    return false;
                }

                if (isNaN(idMotivoSelecionado)) {
                    return false;
                }

                return true;
            }

            function RetornaDadosSelecionados() {
                var retorno = new Array();
                var selection = sm.getSelections();
                var idMotivoSelecionado = ddlCadastroMotivo.getValue();                

                $.each(selection, function(i, e) {

                    var id = e.data.Id;
                    if ($.inArray(id, selection)) {
                        var element = {
                            Id: undefined,
                            IdVagaoMotivo: undefined,
                            IdVagao: undefined,
                            IdMotivo: undefined,
                            DataDaInclusao: undefined,
                            UsuarioInclusao: undefined,
                            DataDaExclusao: undefined,
                            UsuarioExclusao: undefined,
                            DataUltimaAtualizacao: undefined
                        };

                        element.Id = id;
                        element.IdVagaoMotivo = null;
                        element.IdVagao = e.data.IdVagao;
                        element.IdMotivo = idMotivoSelecionado;
                        element.DataDaInclusao = null;
                        element.UsuarioInclusao = null;
                        element.DataDaExclusao = null;
                        element.UsuarioExclusao = null;
                        element.DataUltimaAtualizacao = null;

                        retorno.push(element);
                    }
                });

                return retorno;
            }

            function CadastrarMotivoVagao() {

                windowMotivosVagoes = new Ext.Window({
                    id: 'windowMotivosVagoes',
                    title: 'Informar Motivos',
                    modal: true,
                    closeAction: 'hide',
                    width: 650,
                    height: 150,
                    items: [formCadastroMotivoVagao],
                    listeners: {
                        'hide': function() {

                        }
                    }
                });

                var selection = sm.getSelections();
                var idSituacao = selection[0].data.IdSituacao;

                ddlCadastroSituacao.setValue(idSituacao);
                ddlCadastroSituacao.setDisabled(true);

                ddlCadastroMotivo.getStore().load({ params: { idSituacao: idSituacao } });
                //ddlCadastroMotivo.setValue("Todos");

                windowMotivosVagoes.show();
            }

            //#endRegion

            new Ext.Viewport({
                layout: 'border',
                margins: 10,
                items: [
                    {
                        region: 'north',
                        height: 190,
                        autoScroll: true,
                        items: [{
                                region: 'center',
                                applyTo: 'header-content'
                            },
                            filtros]
                    },
                    gridPainelMotivosVagoes
                ]
            });
        }); 
    </script>
</asp:Content>
<asp:Content runat="server" ID="Content2" ContentPlaceHolderID="Content">
    <div id="header-content">
        <h1>
            Motivo de Situação</h1>
        <small>Você está em Operação > Motivo de situação</small>
        <br />
    </div>
</asp:Content>