﻿<%@ Page Title="Histórico de tara dos vagões" Language="C#" MasterPageFile="~/Views/Shared/Interna.Master"
    Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">

        var formModal = null;
        var grid = null;
        var viewPort = null;

        function FormSuccess(form, action) {
            formModal.close();
        }

        function FormError(form, action) {
            Ext.Msg.alert('Ops...', action.result.Message);
        }

        $(function () {

            grid = new Translogic.PaginatedGrid({
                name: 'grid',
                id: 'grid',
                autoLoad: false,
                region: 'center',
                url: '<%= Url.Action("ObterHistoricos") %>',
                //                viewConfig: {
                //                    forceFit: true
                //                },
                fields:
			    [
					'Id',
					'Vagao',
                    'Serie',
                    'Data',
			        'TaraAnterior',
					'TaraAtual',
					'TaraMediana'
			    ],
                columns: [
				new Ext.grid.RowNumberer(),
				{ Id: 'Id', hidden: true },
				{ header: 'Vagão', dataIndex: "Vagao", sortable: true },
				{ header: 'Série', dataIndex: "Serie", sortable: true },
				{ header: 'Data', dataIndex: "Data", sortable: true },
				{ header: 'Tara anterior', dataIndex: "TaraAnterior", sortable: true },
                { header: 'Tara atual', dataIndex: "TaraAtual", sortable: true },
                { header: 'Tara mediana', dataIndex: "TaraMediana", sortable: true }
			]
            });

            var dataAtual = new Date();
            var vagoes =
    {
        xtype: 'textfield',
        id: 'filtro-vagoes',
        fieldLabel: 'Vagões (Informe os códigos dos vagões entre vírgula para pesquisa de mais de um vagão.)',
        name: 'vagoes',
        allowBlank: true,
        //        mask: '990,000',
        //        money: true,
        //        maxLength: 7,
        //        value: "0,000",
        maskRe: /[0-9,]/,
        style: 'text-transform:uppercase;',
        width: 478
    };

            var serie =
               {
                   xtype: 'textfield',
                   id: 'filtro-serie',
                   fieldLabel: 'Série do vagão',
                   name: 'serie',
                   style: 'text-transform:uppercase;',
                   allowBlank: true,
                   //        mask: '990,000',
                   //        money: true,
                   maxLength: 3,
                   //        value: "0,000",
                   width: 100
               };

            var txtDataInicial = new Ext.form.DateField({
                fieldLabel: 'Data Inicial',
                id: 'txtDataInicial',
                name: 'txtDataInicial',
                dateFormat: 'd/n/Y',
                width: 83,
                plugins: [new Ext.ux.InputTextMask('99/99/9999', true)]
            });

            var txtDataFinal = new Ext.form.DateField({
                fieldLabel: 'Data Final',
                id: 'txtDataFinal',
                name: 'txtDataFinal',
                dateFormat: 'd/n/Y',
                width: 83,
                plugins: [new Ext.ux.InputTextMask('99/99/9999', true)]
            });


            var formVagoes = {
                width: 482,
                layout: 'form',
                border: false,
                items: [vagoes]
            };

            var formSerie = {
                width: 116,
                layout: 'form',
                border: false,
                items: [serie]
            };

            var formDataInicial = {
                width: 100,
                layout: 'form',
                border: false,
                items: [txtDataInicial]
            };

            var formDataFinal = {
                width: 100,
                layout: 'form',
                border: false,
                items: [txtDataFinal]
            };

            var formLinha1 = {
                layout: 'column',
                border: false,
                items: [formVagoes, formSerie, formDataInicial, formDataFinal]
            };
            var formCampos = new Array();
            formCampos.push(formLinha1);

            var filters = new Ext.FormPanel({
                id: 'grid-filtros',
                title: "Filtros",
                region: 'center',
                bodyStyle: 'padding: 15px',
                labelAlign: 'top',
                items: [formCampos],
                buttonAlign: "center",
                buttons:
			[{
			    text: 'Pesquisar',
			    type: 'submit',
			    iconCls: 'icon-find',
			    handler: function (b, e) {

			        if (validaCamposNumericos()) {
			            if (ValidarFiltros()) {
			                if (filters.form.isValid()) {
			                    grid.getStore().load();
			                }
			            }
			        }
			    }
			},
			{
			    text: 'Limpar',
			    handler: function (b, e) {
			        grid.overviewStore.removeAll();
			        Ext.getCmp("grid-filtros").getForm().reset();
			    }
			},
            {
                id: 'btnExportarExcel',
                text: 'Exportar',
                tooltip: 'Exportar',
                type: 'submit',
                region: 'north',
                iconCls: 'icon-page-excel',
                handler: function (c) {
                    if (validaCamposNumericos()) {
                        if (ValidarFiltros()) {
                            if (filters.form.isValid()) {
                                ExportarExcel();
                            }
                        }
                    }
                },
                scope: this
            }]
            });

            grid.getStore().proxy.on('beforeload', function (p, params) {

                var serieFiltro = Ext.getCmp("filtro-serie").getValue();
                var vagoesFiltro = Ext.getCmp("filtro-vagoes").getValue();
                params['FormaPesquisa'] = 'Start';
                params['vagao'] = Ext.getCmp("filtro-vagoes").getValue();
                params['serie'] = Ext.getCmp("filtro-serie").getValue();
                params['dataInicial'] = Ext.getCmp("txtDataInicial").getRawValue();
                params['dataFinal'] = Ext.getCmp("txtDataFinal").getRawValue();
            });

            viewPort = new Ext.Viewport({
                layout: 'border',
                margins: 10,
                items: [
				{
				    region: 'north',
				    height: 245,
				    items: [{
				        region: 'center',
				        applyTo: 'header-content'
				    }, filters]
				},
				grid,
				{
				    id: 'detailPanel',
				    region: 'south',
				    autoScroll: false,
				    bodyStyle: {
				        background: '#CCCCCC',
				        padding: '5px'
				    }
				}
			]
            });
        });

        function ExportarExcel() {
            //URL
            var url = '<%= Url.Action("ExportarHistoricos", "VagaoHistoricoTara") %>';
            //Parametros

            var serieFiltro = Ext.getCmp("filtro-serie").getValue();
            var vagoesFiltro = Ext.getCmp("filtro-vagoes").getValue();
            var dataInicial = Ext.getCmp("txtDataInicial").getRawValue();
            var dataFinal = Ext.getCmp("txtDataFinal").getRawValue();

            url += '?vagao=' + vagoesFiltro;
            url += '&serie=' + serieFiltro;
            url += '&dataInicial=' + dataInicial;
            url += '&dataFinal=' + dataFinal;
            window.open(url, "");

        }

        function ValidarFiltros() {

            var dtI = Ext.getCmp("txtDataInicial").getRawValue();
            var dtF = Ext.getCmp("txtDataFinal").getRawValue();

            if (dtI != "" && dtF != "") {
                if (!comparaDatas(dtI, dtF)) {
                    Ext.Msg.show({
                        title: 'Aviso',
                        msg: ' Data Posterior deve ser superior a data anterior nos filtros.',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.ERROR
                    });
                    return false;
                }
            }
            return true;
        }

        function isNumber(n) {
            return !isNaN(parseFloat(n)) && isFinite(n);
        };

        function validaCamposNumericos() {

            var valor;
            var arrayLength;
            valor = Ext.getCmp("filtro-vagoes").getRawValue().split(",");
            if (valor != "") {
                arrayLength = valor.length;
                for (var i = 0; i < arrayLength; i++) {
                    if (!isNumber(valor[i])) {
                        return false;
                    }
                }
            }
            return true;
        };

        function comparaDatas(dataIni, dataFim) {

            var arrDtIni = dataIni.split('/');
            var arrDtFim = dataFim.split('/');

            var dtIni = new Date(arrDtIni[2] + '/' + arrDtIni[1] + '/' + arrDtIni[0]);
            var dtFim = new Date(arrDtFim[2] + '/' + arrDtFim[1] + '/' + arrDtFim[0]);

            return dtIni <= dtFim;
        }

        Ext.onReady(function () {

            Ext.getCmp("filtro-vagoes").focus();

        });
	
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div id="header-content">
        <h1>
            Operação</h1>
        <small>Você está em Consultas > Histórico de tara dos vagões</small>
        <br />
    </div>
</asp:Content>
