<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Interna.Master" Inherits="Translogic.Modules.Core.Views.GrupoUsuario.Index" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server">

	<script type="text/javascript">

		var formModal = null;
		var grid = null;

		function FormSuccess(form, action)
		{
			formModal.close();
			grid.getStore().load();
		}
		
		function FormError(form, action)
		{
			console.debug(action.result.StackTrace);
			Ext.Msg.alert('Ops...', action.result.Message);
		}

		function booleanRenderer(val)
		{
			if(val == undefined)
			{
				out = '---';
			}else{
				out = val == true ? 'Sim' : 'N�o';
			}
			
			return "<div style='text-align: center;'>"+ out +"</div>";
		}

		function onAdd(btn, evt)
		{
			formModal = new Ext.Window({
				title: 'Novo',
				modal: true,
				width: 400,
				autoHeight: true,
				autoLoad: {
					url: '<%= Url.Action("Form") %>',
					callback : function(el,success,c)
					{
						$("FORM.ext-form", el.dom).transformToExtForm();
					}
				}
			});
			
			formModal.show(this);
		}

		$(function() {

			var detalheAction = new Ext.ux.grid.RowActions({
				dataIndex: '',
				header: '',
				align: 'center',
				actions: [{
					iconCls: 'icon-detail',
					tooltip: 'Detalhes'
				},{
					iconCls: 'icon-del',
					tooltip: 'Excluir'
				}],
				callbacks:{
					'icon-detail': function(grid, record, action, row, col) {

						var editWindow = new Ext.Window({
							title: 'Editar',
							modal: true,
							width: 400,
							autoHeight: true,
							autoLoad: {
								url: '<%= Url.Action("Form") %>',
								params: {Id: record.data.Id},
								callback : function(el,success,c)
								{
									$("FORM.ext-form", el.dom).transformToExtForm();
								}
							}
						});
						
						editWindow.show();

					},
					'icon-del': function(grid, record, action, row, col) {
					
						if(Ext.Msg.confirm("Excluir", "Deseja realmente excluir esse registro?", function(btn, text){
							if (btn == 'yes')
							{
								Ext.Ajax.request({
									url: '<%= Url.Action("Remover") %>',
									params: { Id: record.data.Id },
									success: function(){
										grid.getStore().load();
									}
								});
							}
						}));
						
					}
				}
			});

			grid = new Translogic.PaginatedGrid({
				region: 'center',
				url: '<%= Url.Action("ObterTodos") %>',
				fields: [
					'Id',
					'Codigo',
					'Descricao',
					'Restrito'
				],
				columns: [
					new Ext.grid.RowNumberer(),
					detalheAction,
					{ header: 'C�digo', dataIndex: "Codigo" },
					{ header: 'Descri��o', autoExpandColumn: true, dataIndex: "Descricao" },
					{ header: 'Restrito', width: 20, dataIndex: "Restrito", renderer: booleanRenderer }
				],
				plugins: [detalheAction],
				filteringToolbar: [{
					text: 'Novo',
					tooltip: 'Adicionar novo registro',
					iconCls: 'icon-new',
					handler: onAdd
				}]
			});

			grid.on("rowdblclick", function(g,i,e){
				var record = grid.getSelectionModel().getSelected();
				
				formModal = new Ext.Window({
					title: 'Editar',
					modal: true,
					width: 400,
					autoHeight: true,
					autoLoad: {
						url: '<%= Url.Action("Form") %>',
						params: {Id: record.data.Id},
						callback : function(el,success,c)
						{
							$("FORM.ext-form", el.dom).transformToExtForm();
						}
					}
				});
				
				formModal.show();

			});

			var filters = new Ext.FormPanel({
				id: 'grid-filtros',
				title: "Filtros",
				height: 150,
				region: 'center',
				bodyStyle: 'padding: 15px',
				items: [{
					xtype: 'textfield',
					id: 'codigo-filtro',
					fieldLabel: 'C�digo',
					name: 'codigo'
				},{
					xtype: 'textfield',
					id: 'descricao-filtro',
					fieldLabel: 'Descri��o',
					name: 'descricao',
					maxLength: 30
				}],
				buttons: [{
					text: 'Limpar',
					handler: function(b, e)
					{
						grid.getStore().load();
						Ext.getCmp("grid-filtros").getForm().reset();
					},
					scope: this
				}, {
					text: 'Filtrar',
					type: 'submit',
					iconCls: 'icon-find',
					handler: function(b, e)
					{
					grid.getStore()
						grid.getStore().load({
							params: {
								'filter[0].Field': 'Codigo',
								'filter[0].Value': Ext.getCmp("codigo-filtro").getValue(),
								'filter[0].MatchMode': 'Start',
								'filter[1].Field': 'Descricao',
								'filter[1].Value': Ext.getCmp("descricao-filtro").getValue(),
								'filter[1].MatchMode': 'Start'
							}
						});
					}
				}]
			});

			new Ext.Viewport({
				layout: 'border',
				margins: 10,
				items: [
					{
						region: 'north',
						height: 190,
						items : [{
							region: 'center',
							applyTo: 'header-content'
						}, filters]
					},
					grid
			]});
			
		});
	
	</script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
	<div id="header-content">
		<h1>Grupo Usu�rio</h1>
		<small>Voc� est� em Acesso > Grupo Usu�rio</small>
		<br />
	</div>
</asp:Content>
