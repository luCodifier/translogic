<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>

<%using (Html.BeginForm("Salvar", "GrupoUsuario", FormMethod.Post, new { id = "grupo-usuario-form", @class = "ext-form" })){ %>

	<%= Html.Hidden("grupo.Id") %>
	<%= Html.Hidden("grupo.VersionDate")%>
	<%= Html.Hidden("grupo.Restrito")%>

	<table width="100%">
		<tr>
			<td valign="top" width="200">
				C�digo:
			</td>
			<td>
				<%
					var codigoOptions = ExtJs.Required && ExtJs.MaxLength(8);

					if(ViewData["edit"] == (object) true)
					{
						codigoOptions = codigoOptions && ExtJs.ReadOnly;
					}

				%>
				<%= Html.TextBox("grupo.Codigo", ViewData["grupo.Codigo"], codigoOptions)%>
			</td>
		</tr>
		<tr>
			<td valign="top" width="200">
				Descri��o:
			</td>
			<td>
				<%= Html.TextArea("grupo.Descricao", ExtJs.MaxLength(30))%>
			</td>
		</tr>
		<tr>
			<td colspan="2" align="right">
				<input type="submit" class="submit" value="Salvar" />
			</td>
		</tr>
	</table>

<%}%>
