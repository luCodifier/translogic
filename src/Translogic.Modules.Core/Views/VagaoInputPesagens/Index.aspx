﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Interna.Master" Inherits="System.Web.Mvc.ViewPage" %>
<asp:Content runat="server" ID="HeadContent" ContentPlaceHolderID="HeadContent">
<div id="div-form-novo-Comunicado">
</div>
    <script type="text/javascript">
    
        var txtDataInicio = {
            xtype: 'datefield',
            fieldLabel: 'Data Início',
            id: 'txtDataInicio',
            name: 'txtDataInicio',
            width: 83,
            allowBlank: false,
            value: new Date()
        };

        var fieldArquivo = {
            xtype: 'fileuploadfield',
            id: 'field-Arquivo',
            fieldLabel: 'Arquivo',
            name: 'fieldArquivo',
            allowBlank: false,
            maxLength: 100,
            width: 250,
            hiddenName: 'fieldArquivo'
        };

        var colunaDataInicio = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            items: [fieldArquivo]
        };

        var linha = {
            layout: 'column',
            border: false,
            items: [colunaDataInicio]
        };

        var arrCampos = new Array();
        arrCampos.push(linha);

        var fields = new Ext.form.FormPanel({
            id: 'fields-Cadastro',
            title: "Filtros",
            fileUpload: true,
            region: 'center',
            bodyStyle: 'padding: 15px',
            labelAlign: 'top',
            items: [arrCampos],
            buttonAlign: "left",
            buttons:
	            [
                 {
                     text: 'Salvar',
                     type: 'submit',
                     iconCls: 'icon-save',
                     handler: function () {
                         if (Ext.Msg.confirm("Confirmar Envio de Dados", "Você deseja enviar este arquivo?", function(btn) {
                             if (btn == 'yes') {
                                 Salvar();
                             }
                         }));
                     }
                 }
                ]
        });

        function Salvar() {
            fields.form.submit({
                url: '<%= Url.Action("Salvar") %>',
                type: "POST",
                success: function (action) {
                    ////console.log(action);
                    Ext.Msg.alert('Aviso', "Planilha Enviada Com Sucesso");
                    fields.getForm().reset();
                },
                failure: function (action) {
                    ////console.log(action);
                    Ext.Msg.alert('Aviso', "Erro ao Enviar Planilha");
                    fields.getForm().reset();
                }
            });
        }

        /************************* RENDER **************************/
        Ext.onReady(function () {
            fields.render("div-form-upload");
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div id="header-content">
        <h1>
            Vagão Envio de Pesagens</h1>
        <small>Você está em Veículo > Input Planilha de Pesagens</small>
        <br /> <br /> 
    </div>
    <div id="Div1">
       <h1 style="color: red">Atenção</h1>
        <p>As colunas da planilha devem obrigatóriamente seguir este modelo:</p>
        <p>Série do Vagão | Código do vagão | Data da Pesagem | Cliente | Terminal | Peso Bruto(Ton) | Peso Tara Vagão(Ton)</p>
    </div>
    <div id="div-form-upload">
    </div>
</asp:Content>

