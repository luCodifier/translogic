﻿<%@ Page Title="Ajuste de TU Programada" Language="C#" MasterPageFile="~/Views/Shared/Interna.Master"
    Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        function nrender(value) {
            return addSeps(value.toFixed(2));
        }

        function addSeps(nStr) {
            nStr += '';
            var x = nStr.split('.');
            var x1 = x[0];
            var x2 = x.length > 1 ? ',' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + '.' + '$2');
            }
            return x1 + x2;
        }

        Ext.ux.MonthPickerPlugin = function () {
            var picker;
            var oldDateDefaults;

            this.init = function (pk) {
                picker = pk;
                picker.onTriggerClick = picker.onTriggerClick.createSequence(onClick);
                picker.getValue = picker.getValue.createInterceptor(setDefaultMonthDay).createSequence(restoreDefaultMonthDay);
                picker.beforeBlur = picker.beforeBlur.createInterceptor(setDefaultMonthDay).createSequence(restoreDefaultMonthDay);
            };

            function setDefaultMonthDay() {
                oldDateDefaults = Date.defaults.d;
                Date.defaults.d = 1;
                return true;
            }

            function restoreDefaultMonthDay(ret) {
                Date.defaults.d = oldDateDefaults;
                return ret;
            }

            function onClick(e, el, opt) {
                var p = picker.menu.picker;
                p.activeDate = p.activeDate.getFirstDateOfMonth();
                if (p.value) {
                    p.value = p.value.getFirstDateOfMonth();
                }

                p.showMonthPicker();

                if (!p.disabled) {
                    p.monthPicker.stopFx();
                    p.monthPicker.show();

                    p.mun(p.monthPicker, 'click', p.onMonthClick, p);
                    p.mun(p.monthPicker, 'dblclick', p.onMonthDblClick, p);
                    p.onMonthClick = p.onMonthClick.createSequence(pickerClick);
                    p.onMonthDblClick = p.onMonthDblClick.createSequence(pickerDblclick);
                    p.mon(p.monthPicker, 'click', p.onMonthClick, p);
                    p.mon(p.monthPicker, 'dblclick', p.onMonthDblClick, p);
                }
            }

            function pickerClick(e, t) {
                var el = new Ext.Element(t);
                if (el.is('button.x-date-mp-cancel')) {
                    picker.menu.hide();
                } else if (el.is('button.x-date-mp-ok')) {
                    var p = picker.menu.picker;
                    p.setValue(p.activeDate);
                    p.fireEvent('select', p, p.value);
                }
            }

            function pickerDblclick(e, t) {
                var el = new Ext.Element(t);
                if (el.parent()
                    && (el.parent().is('td.x-date-mp-month')
                        || el.parent().is('td.x-date-mp-year'))) {

                    var p = picker.menu.picker;
                    p.setValue(p.activeDate);
                    p.fireEvent('select', p, p.value);
                }
            }
        };

        Ext.preg('monthPickerPlugin', Ext.ux.MonthPickerPlugin);
    </script>
    <script type="text/javascript">

        var formModal = null;
        var grid = null;
        var viewPort = null;

        function tooltip(value, metaData) {
            metaData.attr = 'ext:qtip="' + value + '"';
            return value;
        }

        $(function () {
            var detalheAction = new Ext.ux.grid.RowActions
            ({
                dataIndex: '',
                header: '',
                align: 'center',
                actions:
                [{
                    iconCls: "icon-detail",
                    tooltip: "Detalhes"
                }],
                callbacks: {
                    'icon-view-details': function (grid, record) {
                        onDetalhe(record.data.Id);
                    }
                }
            });
            var dsAjustesTU = new window.Ext.data.JsonStore({
                id: 'dsAjustesTU',
                name: 'dsAjustesTU',
                root: 'Items',
                url: '<%= Url.Action("ObterAjustesTU", "CADEConfig") %>',
                fields:
                [
                    'Id',
                    'QtdeVagoes',
                    'QtdeVagoesNova',
                    'TUMes',
                    'TUMesNova',
                    'ClienteRementente',
                    'Origem',
                    'Destino',
                    'MesReferencia',
                    'Fluxo',
                    'Mercadoria'
                ]
            });
            var cols = [
                { header: 'Fluxo', dataIndex: "Fluxo", sortable: true, renderer: tooltip, width: 100 },
                { header: 'Origem', dataIndex: "Origem", sortable: true, renderer: tooltip, width: 100 },
                { header: 'Destino', dataIndex: "Destino", sortable: true, renderer: tooltip, width: 100 },
                { header: 'Cliente', dataIndex: "ClienteRementente", sortable: true, renderer: tooltip, width: 100 },
                { header: 'Mercadoria', dataIndex: "Mercadoria", sortable: true, renderer: tooltip, width: 100 },
                { header: 'TU/Mês', dataIndex: "TUMes", sortable: true, renderer: nrender, width: 100 },
                { header: 'Carregamentos', dataIndex: "QtdeVagoes", sortable: true, renderer: nrender, width: 100 },
                { header: 'Ajuste TU/Mês', dataIndex: "TUMesNova", sortable: true, editor: new Ext.form.NumberField({
                    allowBlank: false,
                    allowNegative: false,
                    allowDecimals: true,
                    decimalSeparator: ',',
                    decimalPrecision: 2
                }), renderer: nrender, width: 100
                },
                { header: 'Ajuste Carreg.', dataIndex: "QtdeVagoes", renderer: function (value, metadata, record) {
                    return addSeps(((record.data.TUMesNova * record.data.QtdeVagoes) / record.data.TUMes).toFixed(2));
                }, sortable: true, width: 100
                }
            ];
            grid = new Ext.grid.EditorGridPanel({
                store: dsAjustesTU,
                columns: cols,
                region: 'center',
                stripeRows: false,
                colModel: new Ext.grid.ColumnModel({
                    defaults: {
                        sortable: false
                    },
                    columns: cols
                }),
                clicksToEdit: 1
            });

            var dsClientes = new window.Ext.data.JsonStore({
                id: 'dsClientes',
                name: 'dsClientes',
                root: 'Items',
                url: '<%=Url.Action("ListClientes", "CADEConfig") %>',
                fields: [
                    'DescResumida'
                ],
                listeners: {
                    load: function (store) {
                        var rt = store.recordType;
                        store.insert(0, new rt({}));
                    }
                }
            });
            window.dsClientes = dsClientes;

            var dataAtual = new Date();
            var mesReferencia = {
                xtype: 'datefield',
                fieldLabel: 'Mês Referência',
                id: 'filtro-mes-ref',
                name: 'mesReferencia',
                width: 78,
                allowBlank: false,
                hiddenName: 'mesReferencia',
                value: dataAtual,
                format: 'm/Y',
                plugins: 'monthPickerPlugin'
            };

            var arrDataIni = {
                width: 87,
                layout: 'form',
                border: false,
                items: [mesReferencia]
            };

            var cboClienteCad = {
                xtype: 'combo',
                id: 'cadCliente',
                name: 'cadCliente',
                forceSelection: true,
                store: window.dsClientes,
                triggerAction: 'all',
                mode: 'remote',
                typeAhead: true,
                minChars: 3,
                fieldLabel: 'Cliente',
                displayField: 'DescResumida',
                valueField: 'DescResumida',
                width: 200,
                editable: true,
                tpl: '<tpl for="."><div class="x-combo-list-item">{DescResumida}&nbsp;</div></tpl>'
            };

            var segsStore = new Ext.data.ArrayStore({
                fields: ['segmento'],
                data: [['ACUCAR'], ['LIQUID']]
            });
            window.dsSegmentos = segsStore;
            var cboSegmento = {
                xtype: 'combo',
                id: 'cboSegmento',
                name: 'cboSegmento',
                forceSelection: true,
                store: segsStore,
                triggerAction: 'all',
                mode: 'local',
                typeAhead: true,
                minChars: 3,
                fieldLabel: 'Segmento',
                displayField: 'segmento',
                valueField: 'segmento',
                width: 200,
                editable: true,
                tpl: '<tpl for="."><div class="x-combo-list-item">{segmento}&nbsp;</div></tpl>'
            };

            var lblAviso = {
                xtype: 'label',
                forId: 'myFieldId',
                text: '* Para pesquisar vários fluxos separe os mesmos por virgula. Ex(XX12345,YY98765)',
                margins: '0 0 0 0'
            };

            var txtFluxo = {
                xtype: 'textfield',
                id: 'txtFluxo',
                name: 'txtFluxo',
                typeAhead: true,
                fieldLabel: '* Fluxo',
                //maskRe: /[a-zA-Z0-9]/,
                width: 120,
                enableKeyEvents: true,
                style: {
                    textTransform: 'uppercase'
                }//,
//                listeners: {
//                    keypress: function (thisTxt, e) {
//                        var regEst = /[a-zA-Z0-9]/;
//                        var charDig = String.fromCharCode(e.getCharCode());
//                        if (!regEst.test(charDig)) {
//                            e.preventDefault();
//                        }
//                        //                        else if (thisTxt.getValue().length >= 3) {
//                        //                            e.preventDefault();
//                        //                        }
//                    }
//                }
            };

            var arrCliente = {
                width: 205,
                layout: 'form',
                border: false,
                items: [cboClienteCad]
            };

            var arrSegmento = {
                width: 205,
                layout: 'form',
                border: false,
                items: [cboSegmento]
            };

            var arrOrigem = {
                width: 125,
                layout: 'form',
                border: false,
                items: [txtFluxo]
            };

            var arrlinha1 = {
                layout: 'column',
                border: false,
                items: [arrDataIni, arrOrigem, arrCliente, arrSegmento]
            };
            var arrCampos = new Array();
            arrCampos.push(arrlinha1);
            
            var filters = new Ext.FormPanel({
                id: 'grid-filtros',
                title: "Filtros",
                region: 'center',
                bodyStyle: 'padding: 15px',
                labelAlign: 'top',
                items: [arrCampos, lblAviso],
                buttonAlign: "center",
                buttons:
                [{
                    text: 'Carregar Dados',
                    type: 'submit',
                    id: 'btn-search',
                    iconCls: 'icon-find',
                    handler: function () {
                        var segmento = Ext.getCmp("cboSegmento").getValue();
                        var cliente = Ext.getCmp("cadCliente").getValue();
                        var fluxo = Ext.getCmp("txtFluxo").getValue();

                        if ((segmento == '' && cliente == '' && fluxo == '')) {
                            window.Ext.Msg.show({
                                title: "Filtros incorretos",
                                msg: "Você deve selecionar ao menos um dos filtros (Fluxo, Cliente, Segmento)!",
                                buttons: window.Ext.Msg.OK,
                                icon: window.Ext.MessageBox.ERROR,
                                minWidth: 200
                            });
                            return;
                        }

                        if (filters.form.isValid()) {
                            grid.getStore().rejectChanges();
                            grid.getStore().load();
//                            Ext.getCmp("btn-search").disable();
//                            Ext.getCmp("filtro-mes-ref").disable();
//                            Ext.getCmp("cboSegmento").disable();
//                            Ext.getCmp("cadCliente").disable();
//                            Ext.getCmp("txtFluxo").disable();
                            Ext.getCmp("btn-save").enable();
                        }
                    }
                },
                    {
                        text: 'Salvar',
                        type: 'submit',
                        id: 'btn-save',
                        disabled: true,
                        iconCls: 'icon-save',
                        handler: function () {
                            var modifieds = grid.store.getModifiedRecords();

                            var params = {};
                            var mesReferencia2 = Ext.getCmp("filtro-mes-ref").getValue();
                            var segmento = Ext.getCmp("cboSegmento").getValue();
                            var cliente = Ext.getCmp("cadCliente").getValue();

                            params['mesReferencia'] = new Array(mesReferencia2.format('d/m/Y') + " " + mesReferencia2.format('H:i:s'));
                            params['cliente'] = cliente;
                            params['segmento'] = segmento;

                            var toSave = [];
                            for (var i = 0, item; item = modifieds[i]; i++) {
                                params['ajustes[' + i.toString() + '].Fluxo'] = item.data.Fluxo;
                                params['ajustes[' + i.toString() + '].RazaoAjuste'] = (item.data.TUMesNova / item.data.TUMes);
                            }

                            window.jQuery.ajax({
                                url: '<%= Url.Action("SalvarAjusteTU") %>',
                                type: "POST",
                                data: params,
                                dataType: "json",
                                success: function (data) {
                                    var iconCad = window.Ext.MessageBox.ERROR;
                                    if (data.sucesso)
                                        iconCad = window.Ext.MessageBox.SUCCESS;
                                    window.Ext.Msg.show({
                                        title: "Salvar Ajustes de TU",
                                        msg: data.message,
                                        buttons: window.Ext.Msg.OK,
                                        icon: iconCad,
                                        minWidth: 200,
                                        fn: function () {
                                            if (data.sucesso) {
                                                grid.getStore().rejectChanges();
                                                grid.getStore().load();
                                            }
                                        },
                                        close: function () {
                                            if (data.sucesso) {
                                                grid.getStore().rejectChanges();
                                                grid.getStore().load();
                                            }
                                        }
                                    });

                                },
                                error: function (jqXHR, textStatus) {
                                    window.Ext.Msg.show({
                                        title: "Erro no Servidor",
                                        msg: 'Ocorreu um erro no servidor, por favor, tente novamente mais tarde.',
                                        buttons: window.Ext.Msg.OK,
                                        icon: window.Ext.MessageBox.ERROR,
                                        minWidth: 200
                                    });
                                }
                            });
                        }
                    },
                    {
                        text: 'Limpar',
                        handler: function () {
                            grid.getStore().rejectChanges();
                            grid.store.removeAll();
                            Ext.getCmp("grid-filtros").getForm().reset();
                            Ext.getCmp("btn-search").enable();
                            Ext.getCmp("filtro-mes-ref").enable();
                            Ext.getCmp("cboSegmento").enable();
                            Ext.getCmp("cadCliente").enable();
                            Ext.getCmp("txtFluxo").enable();
                            Ext.getCmp("btn-save").disable();
                        },
                        scope: this
                    }]
            });

            grid.getStore().proxy.on('beforeload', function (p, params) {
                var mesReferencia2 = Ext.getCmp("filtro-mes-ref").getValue();
                var segmento = Ext.getCmp("cboSegmento").getValue();
                var cliente = Ext.getCmp("cadCliente").getValue();
                var fluxo = Ext.getCmp("txtFluxo").getValue();

                params['mesReferencia'] = new Array(mesReferencia2.format('d/m/Y') + " " + mesReferencia2.format('H:i:s'));
                params['cliente'] = cliente;
                params['segmento'] = segmento;
                params['fluxo'] = fluxo;
            });

            grid.on('afteredit', function (e) {
                e.record.data.QtdeVagoesNova = (e.record.data.TUMesNova * e.record.data.QtdeVagoes) / e.record.data.TUMes;
            }, this);

            viewPort = new Ext.Viewport({
                layout: 'border',
                margins: 10,
                items: [
                    {
                        region: 'north',
                        height: 198,
                        items: [{
                            region: 'center',
                            applyTo: 'header-content'
                        }, filters]
                    },
                    grid,
                    {
                        id: 'detailPanel',
                        region: 'south',
                        autoScroll: true,
                        bodyStyle: {
                            background: '#CCCCCC',
                            padding: '5px'
                        }
                    }
                ]
            });
        });	
	
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div id="header-content">
        <h1>
            Configurações CADE</h1>
        <small>Você está em Cadastros > CADE > Ajuste TU Programada</small>
        <br />
    </div>
</asp:Content>
