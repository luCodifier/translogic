﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Interna.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content runat="server" ID="HeadContent" ContentPlaceHolderID="HeadContent">
    <script type="text/javascript">
        var vagoesParaDescarregar = [];

        Ext.onReady(function () {

            var loadingMask = new Ext.LoadMask(Ext.getBody(), { msg: "Aguarde alguns instantes..." });

            function PesquisaEfetuada() {
                Ext.getCmp('linhaVagoesParaDescarregar').setVisible(true);
                Ext.getCmp('linhaDatas').setVisible(true);
                Ext.getCmp('btnSalvar').setVisible(true);
            }

            function CancelarPesquisa() {
                Ext.getCmp('form-filtros').getForm().reset();
                Ext.getCmp('linhaVagoesParaDescarregar').setVisible(false);
                Ext.getCmp('linhaDatas').setVisible(true);
                Ext.getCmp('btnSalvar').setVisible(false);
            }

            function CarregarGridVagoes() {
                for (var item, i = 0; item = vagoesParaDescarregar[i]; i++) {
                    item.IDX = i;
                    if (item.SEL == undefined) {
                        item.SEL = false;
                    }
                }

                var colsHeader = jQuery('#linhaVagoesParaDescarregar .x-list-header > .x-list-header-inner > div');

                var stTmp = Ext.getCmp('lvVagoesParaDescarregar').getStore();
                stTmp.loadData(vagoesParaDescarregar);

                if (Ext.getCmp('filtro-situacao').getValue() == 'E') {
                    jQuery('.colNaoErro').parent().hide();
                    jQuery('.colErro').parent().show();
                    jQuery(colsHeader[7]).show();
                    jQuery(colsHeader[8]).show();
                    jQuery(colsHeader[2]).hide();
                    jQuery(colsHeader[3]).hide();
                    jQuery(colsHeader[6]).hide();
                }
                else {
                    jQuery('.colNaoErro').parent().show();
                    jQuery('.colErro').parent().hide();
                    jQuery(colsHeader[7]).hide();
                    jQuery(colsHeader[8]).hide();
                    jQuery(colsHeader[2]).show();
                    jQuery(colsHeader[3]).show();
                    jQuery(colsHeader[6]).show();
                }

                if (Ext.getCmp('filtro-situacao').getValue() == 'D') {
                    Ext.getCmp('btnSalvar').setVisible(false);
                }
                else {
                    Ext.getCmp('btnSalvar').setVisible(true);
                }
            }

            var timeTest = /^(0[0-9]|1[0-9]|2[0-3]):([0-5][0-9])$/i;
            Ext.apply(Ext.form.VTypes, {
                time: function (val, field) {
                    return timeTest.test(val);
                },
                timeText: 'Horário inválido. Deve ser no formato "HH:MM".'
            });

            var arrlinha0 = {
                id: 'linhaFiltro',
                layout: 'column',
                border: false,
                items: [{
                    width: 200,
                    layout: 'form',
                    border: false,
                    items:
                        [{
                            xtype: 'textfield',
                            style: 'text-transform: uppercase',
                            id: 'filtro-vagao',
                            fieldLabel: 'Vagões (separado por ";")',
                            autoCreate: { tag: 'input', type: 'text', autocomplete: 'off' },
                            name: 'filtro-vagao',
                            allowBlank: true,
                            width: 190,
                            enableKeyEvents: true
                        }]
                }, {
                    width: 150,
                    layout: 'form',
                    border: false,
                    items:
                        [{
                            xtype: 'textfield',
                            style: 'text-transform: uppercase',
                            id: 'filtro-linha',
                            fieldLabel: 'Linha',
                            autoCreate: { tag: 'input', type: 'text', maxlength: '10', autocomplete: 'off' },
                            name: 'filtro-linha',
                            allowBlank: true,
                            maxLength: 10,
                            width: 140,
                            enableKeyEvents: true
                        }]
                }, {
                    width: 130,
                    layout: 'form',
                    border: false,
                    items:
                        [{
                            xtype: 'textfield',
                            style: 'text-transform: uppercase',
                            id: 'filtro-mercadoria',
                            fieldLabel: 'Mercadoria',
                            autoCreate: { tag: 'input', type: 'text', maxlength: '50', autocomplete: 'off' },
                            name: 'filtro-mercadoria',
                            allowBlank: true,
                            maxLength: 50,
                            width: 120,
                            enableKeyEvents: true
                        }]
                }, {
                    width: 110,
                    layout: 'form',
                    border: false,
                    items:
                        [{
                            xtype: 'textfield',
                            style: 'text-transform: uppercase',
                            id: 'filtro-fluxo',
                            fieldLabel: 'Fluxo',
                            autoCreate: { tag: 'input', type: 'text', maxlength: '20', autocomplete: 'off' },
                            name: 'filtro-fluxo',
                            allowBlank: true,
                            maxLength: 20,
                            width: 100,
                            enableKeyEvents: true
                        }]
                }]
            };

            var arrlinha1 = {
                id: 'linhaDatas',
                layout: 'column',
                border: false,
                items: [{
                    width: 100,
                    layout: 'form',
                    border: false,
                    items:
                        [{
                            xtype: 'datefield',
                            fieldLabel: 'Data Inicial',
                            id: 'filtro-datainicial',
                            name: 'filtro-datainicial',
                            width: 90,
                            allowBlank: false,
                            vtype: 'daterange',
                            endDateField: 'filtro-datafinal',
                            value: new Date(),
                            enableKeyEvents: true
                        }]
                }, {
                    width: 100,
                    layout: 'form',
                    border: false,
                    items:
                        [{
                            xtype: 'datefield',
                            fieldLabel: 'Data Final',
                            id: 'filtro-datafinal',
                            name: 'filtro-datafinal',
                            width: 90,
                            allowBlank: false,
                            vtype: 'daterange',
                            startDateField: 'filtro-datainicial',
                            value: new Date(),
                            enableKeyEvents: true
                        }]
                }, {
                    width: 150,
                    layout: 'form',
                    border: false,
                    items:
                        [{
                            xtype: 'combo',
                            id: 'filtro-situacao',
                            name: 'filtro-situacao',
                            forceSelection: true,
                            autoSelect: true,
                            editable: false,
                            store: new window.Ext.data.JsonStore({
                                id: 'dsSituacao',
                                name: 'dsSituacao',
                                fields: ['value', 'display'],
                                data: [
                                    { value: 'N', display: 'Não Descarregados' },
                                    { value: 'D', display: 'Descarregados' },
                                    { value: 'E', display: 'Erros' }
                                ]
                            }),
                            triggerAction: 'all',
                            mode: 'local',
                            typeAhead: true,
                            typeAheadDelay: 300,
                            displayField: 'display',
                            valueField: 'value',
                            fieldLabel: 'Situação',
                            width: 140,
                            minChars: 1,
                            value: 'N'
                        }]
                }, {
                    width: 50,
                    layout: 'form',
                    border: false,
                    items:
                        [{
                            xtype: 'textfield',
                            vtype: 'cteestacaovtype',
                            style: 'text-transform: uppercase',
                            id: 'filtro-estacao',
                            fieldLabel: 'Local',
                            autoCreate: { tag: 'input', type: 'text', maxlength: '3', autocomplete: 'off' },
                            name: 'filtro-estacao',
                            allowBlank: true,
                            maxLength: 3,
                            width: 40,
                            enableKeyEvents: true,
                            listeners: {
                                'keyup': function () {
                                    var val = window.Ext.getCmp('filtro-estacao').getValue();
                                    if (val.length == 3) {
                                        $.ajax({
                                            method: 'GET',
                                            async: true,
                                            dataType: 'json',
                                            url: '<%= Url.Action("ClientesComVagoes") %>?codAO=' + val,
                                            success: function (data) {
                                                window.Ext.getCmp('filtro-cliente').getStore().loadData(data.items);
                                                window.Ext.getCmp('filtro-cliente').setDisabled(false);
                                            }
                                        });
                                    }
                                    else {
                                        window.Ext.getCmp('filtro-cliente').setDisabled(true);
                                        window.Ext.getCmp('filtro-cliente').clearValue();
                                    }
                                }
                            }
                        }]
                }, {
                    width: 190,
                    layout: 'form',
                    border: false,
                    items:
                        [{
                            xtype: 'combo',
                            id: 'filtro-cliente',
                            name: 'filtro-cliente',
                            forceSelection: false,
                            editable: false,
                            disabled: true,
                            store: new window.Ext.data.JsonStore({
                                id: 'dsClientes',
                                name: 'dsClientes',
                                fields: ['Value'],
                                data: []
                            }),
                            triggerAction: 'all',
                            mode: 'local',
                            typeAhead: true,
                            typeAheadDelay: 300,
                            displayField: 'Value',
                            valueField: 'Value',
                            fieldLabel: 'Cliente',
                            width: 180,
                            minChars: 1
                        }]
                }]
            };

            var arrCampos = new Array();
            arrCampos.push(arrlinha1);
            arrCampos.push(arrlinha0);
            arrCampos.push({
                id: 'linhaBotoesPesquisa',
                layout: 'column',
                border: false,
                items: [
                    { html: '&nbsp;', width: 248, border: false },
                    {
                        id: 'btnPesquisar',
                        text: 'Pesquisar',
                        xtype: 'button',
                        width: 100,
                        iconCls: 'icon-find',
                        handler: function () {
                            loadingMask.show();
                            var dateFim = Ext.getCmp('filtro-datafinal').getValue();
                            var dateInicio = Ext.getCmp('filtro-datainicial').getValue();

                            dateFim = new Date(new Date(new Date(dateFim.setHours(23)).setMinutes(59)).setSeconds(59));
                            dateInicio = new Date(dateInicio);

                            jQuery.ajax({
                                type: 'POST',
                                url: '<%= Url.Action("VagoesParaDescarregamento") %>',
                                dataType: "json",
                                contentType: "application/json; charset=utf-8",
                                data: jQuery.toJSON({
                                    codAO: Ext.getCmp('filtro-estacao').getValue(),
                                    codLinha: Ext.getCmp('filtro-linha').getValue(),
                                    codVagao: Ext.getCmp('filtro-vagao').getValue(),
                                    codMercadoria: Ext.getCmp('filtro-mercadoria').getValue(),
                                    codFluxo: Ext.getCmp('filtro-fluxo').getValue(),
                                    codPedido: '',
                                    tipoFiltro: Ext.getCmp('filtro-situacao').getValue(),
                                    identCliente: Ext.getCmp('filtro-cliente').getValue(),
                                    dataInicio: dateInicio,
                                    dataFim: dateFim
                                }),
                                success: function (data) {
                                    loadingMask.hide();
                                    vagoesParaDescarregar = data;
                                    PesquisaEfetuada();
                                    CarregarGridVagoes();
                                }
                            });
                        },
                        scope: this
                    },
                    { html: '&nbsp;', width: 4, border: false },
	                {
	                    text: 'Cancelar',
	                    xtype: 'button',
	                    width: 100,
	                    iconCls: 'icon-cancel',
	                    handler: function () {
	                        CancelarPesquisa();
	                    }
	                }]
            });
            arrCampos.push({
                border: false,
                html: '&nbsp;'
            });
            arrCampos.push({
                hidden: true,
                id: 'linhaVagoesParaDescarregar',
                width: 778,
                title: 'Vagões NÃO Selecionados',
                height: 380,
                items: [new Ext.list.ListView({
                    store: new window.Ext.data.JsonStore({
                        data: [],
                        fields: ['Linha', 'Sequencia', 'Serie', 'Vagao', 'IdVagao', 'Pedido', 'Mercadoria', 'CodPedido', 'Erro', 'XMLErro', 'SEL']
                    }),
                    id: 'lvVagoesParaDescarregar',
                    multiSelect: true,
                    emptyText: '<center>Nenhum registro para exibir!</center>',
                    reserveScrollOffset: true,
                    width: 778,
                    height: 350,
                    style: '-moz-user-select: none; -webkit-user-select: none; -ms-user-select:none; user-select:none;',
                    listeners: {
                        'click': function (lv, idx) {
                            var stTmp = Ext.getCmp('lvVagoesParaDescarregar').getStore();
                            var rec = stTmp.getAt(idx);
                            var idVagao = rec.json.IdVagao;

                            for (var i = 0, record; record = vagoesParaDescarregar[i]; i++) {
                                if (record.IdVagao == idVagao) {
                                    vagoesParaDescarregar[i].SEL = !vagoesParaDescarregar[i].SEL;
                                }
                            }
                            CarregarGridVagoes();
                        }
                    },
                    columns: [new Ext.list.BooleanColumn({
                        header: '&nbsp;',
                        tpl: '<input type="checkbox" value="{IdVagao}" <tpl if="SEL">checked="checked"</tpl> ext:qtip="Selecionar vagão para descarga!" name="CB-GRID"/>',
                        width: 0.03,
                        dataIndex: 'SEL'
                    }), {
                        header: 'Linha',
                        width: 0.15,
                        dataIndex: 'Linha'
                    }, {
                        header: 'Seq.',
                        width: 0.1,
                        dataIndex: 'Sequencia',
                        cls: 'colNaoErro'
                    }, {
                        header: 'Série',
                        width: 0.15,
                        dataIndex: 'Serie',
                        cls: 'colNaoErro'
                    }, {
                        header: 'Vagão',
                        width: 0.20,
                        dataIndex: 'Vagao'
                    }, {
                        header: 'Pedido',
                        width: 0.27,
                        dataIndex: 'CodPedido'
                    }, {
                        header: 'Mercadoria',
                        width: 0.1,
                        dataIndex: 'Mercadoria',
                        cls: 'colNaoErro'
                    }, {
                        header: 'Erro',
                        width: 0.20,
                        dataIndex: 'Erro',
                        cls: 'colErro'
                    }, {
                        header: 'XML de ERRO',
                        width: 0.15,
                        dataIndex: 'XMLErro',
                        cls: 'colErro'
                    }]
                })]
            });

            var filters = new window.Ext.form.FormPanel({
                id: 'form-filtros',
                region: 'north',
                bodyStyle: 'padding: 15px',
                labelAlign: 'top',
                width: 800,
                fileUpload: true,
                border: false,
                items: [arrCampos],
                buttonAlign: "center",
                buttons:
	            [
	                {
	                    hidden: true,
	                    id: 'btnSalvar',
	                    text: 'Descarregar Selecionados',
	                    type: 'submit',
	                    width: 100,
	                    iconCls: 'icon-save',
	                    handler: function () {
	                        loadingMask.show();
	                        var vgSelecionado = [];

	                        for (var i = 0; i < vagoesParaDescarregar.length; i++) {
	                            vagoesParaDescarregar[i].IDX = i;
	                            if (vagoesParaDescarregar[i].SEL) {
	                                vgSelecionado.push(vagoesParaDescarregar[i]);
	                            }
	                        }

	                        jQuery.ajax({
	                            type: 'POST',
	                            url: '<%= Url.Action("DescarregarVagoes") %>',
	                            dataType: "json",
	                            contentType: "application/json; charset=utf-8",
	                            data: jQuery.toJSON({
	                                codAO: Ext.getCmp('filtro-estacao').getValue(),
	                                vagaoEmCarreg: false,
	                                vagoes: vgSelecionado
	                            }),
	                            success: function (data) {
	                                loadingMask.hide();
	                                if (!data.sucesso) {
	                                    window.Ext.Msg.show({
	                                        title: "Erro ao Executar Descarga",
	                                        msg: 'Ocorreu um erro no servidor, por favor, tente novamente mais tarde.<br/><br/>' + data.mensagem,
	                                        buttons: window.Ext.Msg.OK,
	                                        icon: window.Ext.MessageBox.ERROR,
	                                        minWidth: 200
	                                    });
	                                }
	                                else {
	                                    window.Ext.Msg.show({
	                                        title: "Descarga realizada com sucesso.",
	                                        msg: 'A descarga dos vagões foi realizada com sucesso!',
	                                        buttons: window.Ext.Msg.OK,
	                                        icon: window.Ext.MessageBox.SUCCESS,
	                                        minWidth: 200
	                                    });
	                                }
	                            }
	                        });
	                    },
	                    scope: this
	                }
	            ]
            });

            var tmp = new window.Ext.Viewport({
                layout: 'border',
                region: 'north',
                margins: 10,
                items: [
			        {
			            region: 'center',
			            height: 800,
			            autoScroll: true,
			            items: [
			                {
			                    region: 'center',
			                    applyTo: 'header-content'
			                },
				            filters
			            ]
			        }
		        ]
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div id="header-content">
        <h1>
            Descarregamento</h1>
        <small>Você está em: Operação > Funções Pátio > Descarregamento</small>
        <br />
    </div>
</asp:Content>
