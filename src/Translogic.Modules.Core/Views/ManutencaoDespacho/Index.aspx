<%@ Import Namespace="System.Security.Policy"%>
<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Interna.Master" Inherits="System.Web.Mvc.ViewPage" %>
<asp:Content runat="server" ID="HeadContent" ContentPlaceHolderID="HeadContent">
	
	<script type="text/javascript">

	    var formModal = null;
	    var grid = null;

	    function FormSuccess(form, action) {
	        formModal.close();
	        grid.getStore().load();
	    }

	    function FormError(form, action) {
	        Ext.Msg.alert('Ops...', action.result.Message);
	    }

	    function onEdit(id) {
            formModal = new Ext.Window({
                id: 'formModalAjusteVelocidade',
                title: 'Editar Fator',
                modal: true,
                width: 300,
                height: 250,
                autoLoad: {
                    url: '<%= Url.Action("Form") %>',
                    scripts: true,
                    params: { Id: id }
                }
            });
            formModal.show();
        }
        
        function onAdd(btn, evt) {
            formModal = new Ext.Window({
                id: 'formModalAjusteVelocidade',
                title: 'Novo Fator',
                modal: true,
                width: 300,
                height: 250,
                autoLoad: { url: '<%= Url.Action("Form") %>',
                    scripts: true
                }
            });
            formModal.show(this);
        }


        function formataData(val){
        
            if (val.toString().indexOf('/') != -1) {
                return val;
            }
            else {
                return Ext.util.Format.date( val, 'd/m/Y');
            }
        }

        $(function() {

            var detalheAction = new Ext.ux.grid.RowActions({
                dataIndex: '',
                header: '',
                align: 'center',
                actions: [{
                    iconCls: 'icon-detail',
                    tooltip: 'Detalhes'
                }, {
                    iconCls: 'icon-del',
                    tooltip: 'Excluir'
}],
                    callbacks: {
                        'icon-detail': function(grid, record, action, row, col) {
                            onEdit(record.data.Id);
                        },
                        'icon-del': function(grid, record, action, row, col) {

                            if (Ext.Msg.confirm("Excluir", "Deseja realmente excluir esse registro?", function(btn, text) {
                                if (btn == 'yes') {
                                    Ext.Ajax.request({
                                        url: '<%= Url.Action("Remover") %>',
                                        params: { Id: record.data.Id },
                                        success: function() {
                                            grid.getStore().load();
                                        }
                                    });
                                }
                            }));

                        }
                    }
                });


                dsConsulta = new Ext.data.JsonStore({
                    url: '<%= Url.Action("Obter") %>',
                    root: "Items",
                    fields: [
					    // 'Id',
			            'Motivo',
						'Serie',
						'CodVagao',
						'Vagao',
						'Origem',
						'OrigemIntercambio',
						'Destino',
						'SerieDespacho',
						'NumDespacho',
						'Serie6',
						'Num6',
						'DtDespacho',
						'Despacho',
						'Mercadoria',
						'CodFluxo',
						'Tu',
						'Volume',
						'SerNota',
						'Nota',
						'Chave',
						'Peso',
						'DtNota',
						'Valor',
						'Usuario',
						'Conteiner',
						'Modificados'
				    ]
                });


                grid = new Ext.grid.EditorGridPanel({
                    viewConfig: {
                        forceFit: false
                    },
                    stripeRows: true,
                    store: dsConsulta,
                    selModel: new Ext.grid.RowSelectionModel(),
                    region: 'center',
                    loadMask: { msg: App.Resources.Web.Carregando },
                    colModel: new Ext.grid.ColumnModel({
                        defaults: {
                            width: 120,
                            sortable: false
                        },
                        columns: [
					    new Ext.grid.RowNumberer(),
					    { header: 'Motivo', dataIndex: "Motivo", sortable: false, width: 70 },
                        { header: 'S�rie', dataIndex: "Serie", sortable: false, width: 70 },
                        { header: 'Cod Vag�o', dataIndex: "CodVagao", sortable: false, width: 70, css: 'border-style:inset;border-color:#0000ff;border-width: 1px;', editor: new Ext.form.NumberField({ allowBlank: false }) },
                        { header: 'Origem', dataIndex: "Origem", sortable: false, width: 70 },
                        { header: 'OriInterc', dataIndex: "OrigemIntercambio", sortable: false, width: 70 },
                        { header: 'Destino', dataIndex: "Destino", sortable: false, width: 70 },
                        { header: 'Ser.Des', dataIndex: "SerieDespacho", sortable: false, width: 70 },
                        { header: 'Num.Des', dataIndex: "NumDespacho", sortable: false, width: 70 },
                        { header: 'Ser 6', dataIndex: "Serie6", sortable: false, width: 70 },
                        { header: 'Num 6', dataIndex: "Num6", sortable: false, width: 70 },
                        { header: 'Dt Desp', dataIndex: "DtDespacho", sortable: false, width: 100 },
                        { header: 'Mercadoria', dataIndex: "Mercadoria", sortable: false, width: 100 },
                        { header: 'Fluxo', dataIndex: "CodFluxo", sortable: false, width: 70, css: 'border-style:inset;border-color:#0000ff;border-width: 1px;', editor: new Ext.form.TextField({ allowBlank: false }) },
                        { header: 'TU', dataIndex: "Tu", sortable: false, width: 70, css: 'border-style:inset;border-color:#0000ff;border-width: 1px;', renderer: Ext.util.Format.numberRenderer('0.000,00/i'), editor: new Ext.form.NumberField({ allowBlank: true, allowDecimals: true, decimalSeparator: ',' }) },
                        { header: 'Volume', dataIndex: "Volume", sortable: false, width: 70, css: 'border-style:inset;border-color:#0000ff;border-width: 1px;', renderer: Ext.util.Format.numberRenderer('0.000,00/i'), editor: new Ext.form.NumberField({ allowBlank: false, allowDecimals: true, decimalSeparator: ',' }) },
                        { header: 'Ser.Not', dataIndex: "SerNota", sortable: false, width: 70, css: 'border-style:inset;border-color:#0000ff;border-width: 1px;', editor: new Ext.form.NumberField({ allowBlank: false }) },
                        { header: 'Nota', dataIndex: "Nota", sortable: false, width: 70, css: 'border-style:inset;border-color:#0000ff;border-width: 1px;', editor: new Ext.form.NumberField({ allowBlank: false }) },
                        { header: 'Chave', dataIndex: "Chave", sortable: false, width: 70 },
                        { header: 'Peso', dataIndex: "Peso", width: 70, css: 'border-style:inset;border-color:#0000ff;border-width: 1px;', renderer: Ext.util.Format.numberRenderer('0.000,00/i'), sortable: false, editor: new Ext.form.NumberField({ allowBlank: false, allowDecimals: true, decimalSeparator: ',' }) },
                        { header: 'Dt Nota', dataIndex: "DtNota", sortable: false, width: 100, css: 'border-style:inset;border-color:#0000ff;border-width: 1px;', renderer: formataData, editor: new Ext.form.DateField({ allowBlank: false, dateFormat: 'd/m/Y' }) },
                        { header: 'Valor', dataIndex: "Valor", sortable: false, width: 70, css: 'border-style:inset;border-color:#0000ff;border-width: 1px;', renderer: Ext.util.Format.numberRenderer('0.000,00/i'), editor: new Ext.form.NumberField({ allowBlank: false, allowDecimals: true, decimalSeparator: ',' }) },
                        { header: 'Usuario', dataIndex: "Usuario", sortable: false, width: 100 },
                        { header: 'Conteiner', dataIndex: "Conteiner", sortable: false, width: 100, css: 'border-style:inset;border-color:#0000ff;border-width: 1px;', editor: new Ext.form.TextField({ allowBlank: false }) }
				    ]
                    }),
                    columnLines: true,
                    buttonAlign: 'center',
                    fbar:
                        [{
    	                    text: 'Salvar',
    	                    name: 'salvar',
    	                    id: 'salvar',
    	                    iconCls: 'icon-save',
    	                    handler: function(b, e) {
    		                    
    		                    var listaEnvio = Array();

			                    var temRegistrosInconsistentes = false;
    		                    var listaModificados = dsConsulta.getModifiedRecords();
    		                    for (var i = 0; i < listaModificados.length; i++) {
    			                    var registro = listaModificados[i].data;
    			                    
    			                    var modificados = listaModificados[i].getChanges();
    			                    
                                    for(m in modificados) {
                                        if(modificados.hasOwnProperty(m)) {
                                             alert(m);
                                             registro.Modificados = registro.Modificados  + m + '|';
                                        }
                                    } 
    			                    
    			                    /*if (registro.Vagao == "" ||
                                        registro.Fluxo == "" ||
                                        registro.TU == "" ||
                                        registro.Volume == "" ||
                                        registro.SerNot == "" ||
                                        registro.Nota == "" ||
                                        registro.Peso == "" ||
                                        registro.DtNota == "" ||
                                        registro.Valor == "" ||
                                        registro.Conteiner == "") {
    				                    
    				                        temRegistrosInconsistentes = true;
    			                    }*/
                        			registro.DtNota = formataData(registro.DtNota);
    			                    
    			                    listaEnvio.push(registro);
    		                    }
                        		
			                    if (temRegistrosInconsistentes){
				                    Ext.Msg.show({
					                    title: "Erro",
					                    msg: "Existem dados inconsistentes, corrija e tente salvar novamente.",
					                    buttons: Ext.Msg.OK,
					                    icon: Ext.MessageBox.ERROR,
					                    minWidth: 200
				                    });
				                    return false;
			                    }
                				
    		                    var dados = $.toJSON(listaEnvio);
    		                    $.ajax({
    			                    url: "<%= Url.Action("Salvar") %>",
    			                    type: "POST",
    			                    dataType: 'json',
    			                    data: dados,
    			                    contentType: "application/json; charset=utf-8",
    			                    success: function(result) {
    				                    if (!result.success)
    				                    {
    					                    Ext.Msg.alert('Ops...', result.Message);
    				                    }else{
    				                        dsConsulta.commitChanges();
    				                    }
    			                    }
    		                    });


    	                    }   
                        }, {
    	                    text: 'Cancelar',
    	                    name: 'cancelar',
    	                    id: 'cancelar',
    	                    iconCls: 'icon-cancel',
    	                    handler: function(b, e) {
    		                    
    		                    if (dsConsulta.getTotalCount() != 0 && dsConsulta.getModifiedRecords().length != 0) {
    		                        if (Ext.Msg.confirm("Confirma��o", "As altera��es n�o foram salvas e ser�o perdidas.<br />Deseja Continuar?", function(btn, text) {
								        if (btn == 'yes') {
									        grid.stopEditing();
    		                                grid.getStore().rejectChanges();
                                            grid.getStore().load();
								    }
							    }));   	                        
    		                   } 
    	                    }
                     }]
                });


                var filters = new Ext.FormPanel({
                    id: 'grid-filtros',
                    title: "Filtros",
                    height: 140,
                    region: 'center',
                    bodyStyle: 'padding: 15px',
                    labelAlign: 'top',
                    items:
				[
					{ // Linha I
					    layout: 'column',
					    border: false,
					    items:
						[
							{ // Coluna
							    width: 110,
							    layout: 'form',
							    border: false,
							    items:
								[
									{
									    xtype: 'textfield',
									    id: 'filtro-serie',
									    fieldLabel: 'S�rie',
									    name: 'serie',
									    allowBlank: true,
									    width: 100,
									    style: 'text-transform: uppercase'
									}
								]
							},
							{
							    width: 110,
							    layout: 'form',
							    border: false,
							    items:
								[
									 {
									     xtype: 'numberfield',
									     id: 'filtro-despacho',
									     fieldLabel: 'Despacho',
									     name: 'despacho',
									     allowBlank: true,
									     width: 100,
									     style: 'text-transform: uppercase'
									 }
								]
							},
							{ // Coluna
							    width: 110,
							    layout: 'form',
							    border: false,
							    items:
								[
									 {
									     xtype: 'textfield',
									     id: 'filtro-fluxo',
									     fieldLabel: 'Fluxo',
									     name: 'fluxo',
									     allowBlank: true,
									     width: 100,
									     style: 'text-transform: uppercase'
									 }
								]
							},
							{ // Coluna
							    width: 210,
							    layout: 'form',
							    border: false,
							    items:
								[
									 {
									     xtype: 'numberfield',
									     id: 'filtro-vagoes',
									     fieldLabel: 'Vag�es',
									     name: 'vagoes',
									     allowBlank: true,
									     width: 200,
									     style: 'text-transform: uppercase'
									 }
								]
							}
						]
					}
				],
                    buttonAlign: "center",
                    buttons: [
				{
				    text: 'Pesquisar',
				    type: 'submit',
				    iconCls: 'icon-find',
				    handler: function(b, e) {
				        if (filters.form.isValid()) {
				            if (dsConsulta.getModifiedRecords().length != 0) {
				                if (Ext.Msg.confirm("Confirma��o", "As altera��es n�o foram salvas e ser�o perdidas.<br />Deseja Continuar?", function(btn, text) {
								    if (btn == 'yes') {
									    grid.getStore().load();
								    }
							    }));
							}
							else {
							    grid.getStore().load();
							}
				        }
				    }
				},
				{
				    text: 'Limpar',
				    handler: function(b, e) {
				       
				       if (dsConsulta.getTotalCount() != 0 && dsConsulta.getModifiedRecords().length != 0) {
	                        if (Ext.Msg.confirm("Confirma��o", "As altera��es n�o foram salvas e ser�o perdidas.<br />Deseja Continuar?", function(btn, text) {
						        if (btn == 'yes') {
							        grid.stopEditing();
	                                grid.getStore().rejectChanges();
                                    dsConsulta.removeAll();
				                    Ext.getCmp("grid-filtros").getForm().reset();
						    }
					    }));   	                        
	                   }
	                   else {
	                        dsConsulta.removeAll();
				            Ext.getCmp("grid-filtros").getForm().reset();
	                   }
				       
				       
				    },
				    scope: this
				}
				]
                });

                grid.getStore().proxy.on('beforeload', function(p, params) {

                   // var inicioVigencia = Ext.getCmp("filtro-inicioVigencia").getValue();
                   // var fimVigencia = Ext.getCmp("filtro-fimVigencia").getValue();

                    //if (inicioVigencia != "") inicioVigencia = inicioVigencia.format('d/m/Y');
                    //if (fimVigencia != "") fimVigencia = fimVigencia.format('d/m/Y');

                    params['filter[0].Campo'] = 'Serie';
                    params['filter[0].Valor'] = Ext.getCmp("filtro-serie").getValue();
                    params['filter[0].FormaPesquisa'] = 'Equals';

                    params['filter[1].Campo'] = 'Despacho';
                    params['filter[1].Valor'] = Ext.getCmp("filtro-despacho").getValue();
                    params['filter[1].FormaPesquisa'] = 'Equals';

                    params['filter[2].Campo'] = 'Fluxo';
                    params['filter[2].Valor'] = Ext.getCmp("filtro-fluxo").getValue();
                    params['filter[2].FormaPesquisa'] = 'Equals';

                    params['filter[3].Campo'] = 'Vagoes';
                    params['filter[3].Valor'] = Ext.getCmp("filtro-vagoes").getValue();
                    params['filter[3].FormaPesquisa'] = 'Equals';

                });

                new Ext.Viewport({
                    layout: 'border',
                    margins: 10,
                    autoScroll: true,
                    items: [
					        {
					            region: 'north',
					            height: 180,
					            items: [{
					                region: 'center',
					                applyTo: 'header-content'
					            },
					                    filters
					            ]
					        },
					        grid
			            ]

                });


            });
	
	</script>
</asp:Content>
<asp:Content runat="server" ID="Content2" ContentPlaceHolderID="Content">
	<div id="header-content">
		<h1>Realiza Manuten��o de Despacho</h1>
		<small>Voc� est� em Consulta > Manuten��o de Despacho > Realiza Manuten��o de Despacho</small>
	</div>
</asp:Content>