﻿<%@ Import Namespace="System.Security.Policy" %>

<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Interna.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content runat="server" ID="HeadContent" ContentPlaceHolderID="HeadContent">
    <script type="text/javascript">

    var formModal = null;
    var grid = null;
	var arraySelectItens = new Array();
	var lastRowSelected = -1;

	var dsCodigoControle = new Ext.data.JsonStore({
        root: "Items",
        autoLoad: true,
        url: '<%= Url.Action("ObterCodigoSerieDesp") %>',
        fields: [
                    'Id',
                    'CodigoControle'
			    ]   		
    });

    // var dataAtual = new String('<%= String.Format("{0:dd/MM/yyyy}", DateTime.Now) %>');
		
		// Flag para garantir que uma tela de mensagem que está sobreposta não chame novamente a tela de pesquisa
		var executarPesquisa = true;

		function showResult(btn) {
			executarPesquisa = true;
		}

    function FormSuccess(form, action) {
        ds.removeAll();
        ds.loadData(action.result, true);
    }

    function Pesquisar(){
		 arraySelectItens = new Array();	
         var diferenca = Ext.getCmp("filtro-data-final").getValue() - Ext.getCmp("filtro-data-inicial").getValue(); 
		        diferenca = Math.floor(diferenca / (1000 * 60 * 60 * 24)); 

	        if (Ext.getCmp("filtro-data-inicial").getValue() == '' || Ext.getCmp("filtro-data-final").getValue() == '') {
						executarPesquisa = false;
						Ext.Msg.show({
							title: "Mensagem de Erro",
							msg: "Preencha os filtro datas!",
							buttons: Ext.Msg.OK,
							icon: Ext.MessageBox.ERROR,
							minWidth: 200,
							fn: showResult
		        });
            }
						else if (diferenca > 30) {
							executarPesquisa = false;
              Ext.Msg.show({
								title: "Mensagem de Erro",
								msg: "O período da pesquisa não deve ultrapassar 30 dias",
								buttons: Ext.Msg.OK,
								icon: Ext.MessageBox.ERROR,
								minWidth: 200,
								fn: showResult
							});
            }	
            else if  (Ext.getCmp("filtro-serie").getValue() != '' && Ext.getCmp("filtro-despacho").getValue() == '') {
							executarPesquisa = false;
                Ext.Msg.show({
			        title: "Mensagem de Erro",
							msg: "Não é possível filtrar apenas pela a Série do Despacho.<br/>Também é necessário preencher o número do Despacho.",
			        buttons: Ext.Msg.OK,
			        icon: Ext.MessageBox.ERROR,
			        minWidth: 200,
							fn: showResult
		        });
            }
            else if (Ext.getCmp("filtro-despacho").getValue() != '' && Ext.getCmp("filtro-serie").getValue() == '') {
							executarPesquisa = false;
							Ext.Msg.show({
			        title: "Mensagem de Erro",
			        msg: "Não é possível filtrar apenas pelo número do Despacho.<br/>Também é necessário preencher a Série do Despacho.",
			        buttons: Ext.Msg.OK,
			        icon: Ext.MessageBox.ERROR,
			        minWidth: 200,
							fn: showResult
		        });
            }
            else {
					executarPesquisa = true;
					grid.getStore().load();
            }
    }

    function FormError(form, action) {
			// Ext.Msg.alert('Ops...', action.result.Message);
			Ext.Msg.show({
				title: "Mensagem de Erro",
				msg: action.result.Message,
				buttons: Ext.Msg.OK,
				icon: Ext.MessageBox.ERROR,
				minWidth: 200
			});

        ds.removeAll();
    } 

    var ds = new Ext.data.JsonStore({
        url: '<%= Url.Action("ObterCtes") %>',
        root: "Items",
        autoLoad: false,
        totalProperty: 'Total',
		remoteSort: true,
        paramNames: {
				sort: "pagination.Sort",
				dir: "pagination.Dir",
				start: "pagination.Start",
				limit: "pagination.Limit"
		},
        fields: [
                    'CteId',
                    'Fluxo',
                    'Origem',
                    'Destino',
                    'Mercadoria',
                    'ClienteFatura',
                    'Cte',
                    'Serie',
                    'Despacho',  
                    'DateEmissao',
                    'ValorComplemento',
                    'Complementado',
                    'ValorCte',
                    'ValorDiferencaComplemento',
                    'PesoVagao',
                    'CodigoUsuario',
                    'DataComplemento',
                    'CteComAgrupamentoNaoAutorizado',
					'SerieDesp5',
					'NumDesp5',
					'SerieDesp6',
					'NumDesp6',
					'CodigoControle'
			    ]
    });

/* ------------- Ação CTRL+C nos campos da Grid ------------- */
    var isCtrl = false;
    document.onkeyup = function (e) {

        var keyID = event.keyCode;

        // 17 = tecla CTRL
        if (keyID == 17) {
            isCtrl = false; //libera tecla CTRL
        }
    }

    document.onkeydown = function (e) {

        var keyID = event.keyCode;

        // verifica se CTRL esta acionado
        if (keyID == 17) {
            isCtrl = true;
        }

		if ((keyID == 13) && (executarPesquisa)) {
			Pesquisar();
			return;
		}

        // 67 = tecla 'c'
        if (keyID == 67 && isCtrl == true) {

            // verifica se há selecão na tabela
            if (grid.getSelectionModel().hasSelection()) {
                // captura todas as linhas selecionadas
                var recordList = grid.getSelectionModel().getSelections();

                var a = [];
                var separator = '\t'; // separador para colunas no excel
                for (var i = 0; i < recordList.length; i++) {
                    var s = '';
                    var item = recordList[i].data;
                    for (key in item) {
                        if (key != 'Id') { //elimina o campo id da linha
                            s = s + item[key] + separator;
                        }
                    }
                    s = s.substr(0, s.length - 1); //retira o ultimo separador '\t' da linha
                    a.push(s);
                }
                window.clipboardData.setData('Text', (a.join('\n'))); //insere linhas no clipBoard
            }
        }
    }
/* ------------------------------------------------------- */

    Contains = function(arr, obj) {
                for(var i = 0; i < arr.length; i++) {
                    if(arr[i] === obj) {
                        return true;
                    }
                }
                return false;
              }

    
    $(function () {

        sm2 = new Ext.grid.CheckboxSelectionModel({
            checkOnly : false,
            singleSelect: true,
            listeners: {
                selectionchange: function (sm) {
                    if (sm.getCount()) {
                        Ext.getCmp("salvar").enable();
                    } else {
                        Ext.getCmp("salvar").disable();
                    }
                },
                beforerowselect : function (sm, rowIndex, keep, record) {
//                    if (record.data.Complementado || record.data.DataComplemento != null){
//                      Ext.Msg.show({
//					                  title: "Mensagem de Erro",
//					                  msg: "Impossível selecionar o CTe!</BR>CTe já complementado!",
//					                  buttons: Ext.Msg.OK,
//					                  icon: Ext.MessageBox.ERROR,
//					                  minWidth: 200
//				              });
//                      return false;
//                    }
					
					return true;
                }
            }
        });

           var pagingToolbar = new Ext.PagingToolbar({
				pageSize: 50,
				store: ds,
				displayInfo: true,
				displayMsg: App.Resources.Web.GridExibindoRegistros,
				emptyMsg: App.Resources.Web.GridSemRegistros,
				paramNames: {
					start: "pagination.Start",
					limit: "pagination.Limit"
				}
			});


        grid = new Ext.grid.EditorGridPanel({
            viewConfig: {
                forceFit: true,
                getRowClass: MudaCor
            },
            stripeRows: true,
            region: 'center',
            autoLoadGrid: false,
            autoLoad: false,
            loadMask: { msg: App.Resources.Web.Carregando },
            store: ds,
            colModel: new Ext.grid.ColumnModel({
                defaults: {
                    sortable: false
                },
               columns: [
						new Ext.grid.RowNumberer(),
                        sm2,
                        { dataIndex: "CteId", hidden: true },
						{ header: 'Fluxo', dataIndex: "Fluxo", sortable: false },
						{ header: 'Origem', dataIndex: "Origem", sortable: false },
						{ header: 'Destino', dataIndex: "Destino", sortable: false },
				        { header: 'Mercadoria', dataIndex: "Mercadoria", sortable: false },
				        { header: 'Cliente Fatura', dataIndex: "ClienteFatura", sortable: false },
				        { header: 'CTe', dataIndex: "Cte", sortable: false },
                        { header: 'Série', dataIndex: "Serie", sortable: false },
				        { header: 'Despacho', dataIndex: "Despacho", sortable: false },
				        { header: 'Data Emissão', dataIndex: "DateEmissao", sortable: false },
				        { header: 'Valor', dataIndex: "ValorCte", sortable: false, renderer: Ext.util.Format.numberRenderer('0.000,00/i') },
				        { header: 'Diferença', dataIndex: "ValorDiferencaComplemento", sortable: false, css: 'border-style:inset;border-color:#0000ff;border-width: 1px;', renderer: Ext.util.Format.numberRenderer('0.000,00/i'), editor: new Ext.form.NumberField({ allowBlank: false, allowDecimals: true, decimalSeparator: ',' }) },
				        { header: 'Peso', dataIndex: "PesoVagao", sortable: false, renderer: Ext.util.Format.numberRenderer('0.000,00/i') },
                        { header: 'Usuário', dataIndex: "CodigoUsuario", sortable: false },
                        { header: 'Data Complemento', dataIndex: "DataComplemento", sortable: false },
                        { dataIndex: "Complementado", hidden: true },
                        { dataIndex: "CteComAgrupamentoNaoAutorizado", hidden: true }                       
					]
                  ,				  
                  isCellEditable: function(col, row) {
					
					
                    var record = ds.getAt(row);

                    if (record.data.Complementado) {
                      Ext.Msg.show({
					                title: "Mensagem de Erro",
					                msg: "Impossível editar o valor!</BR>CTe já complementado!",
					                buttons: Ext.Msg.OK,
					                icon: Ext.MessageBox.ERROR,
					                minWidth: 200
				              });
                      return false;
                    }
					
					arraySelectItens.push(row);
					sm2.selectRows(arraySelectItens, false);	

                    return Ext.grid.ColumnModel.prototype.isCellEditable.call(this, col, row);
                  }
            }),
			listeners: {
				rowclick: function(grid, rowIndex, e) {
						
					var linha = arraySelectItens.indexOf(rowIndex);
						
					if(linha >= 0)
					{
						arraySelectItens.splice(linha,1);	
					}else{
						arraySelectItens.push(rowIndex);
					}

					sm2.selectRows(arraySelectItens, false);
				}
			},		
            sm: sm2,
            columnLines: true,
            bbar: pagingToolbar,
            buttonAlign: 'center',
            fbar:
                [{
    	            text: 'Salvar',
    	            name: 'salvar',
    	            id: 'salvar',
    	            iconCls: 'icon-save',
                    disabled: true,
    	            handler: function(b, e) {
    		            
                        var listaModificados = ds.getModifiedRecords();

                        var listaSelecionados = sm2.getSelections();
                        
                        if (listaModificados.length > 10 || listaSelecionados.length > 10){
				                  Ext.Msg.show({
					                  title: "Mensagem de Erro",
					                  msg: "Não é possível salvar mais que 10 registros!",
					                  buttons: Ext.Msg.OK,
					                  icon: Ext.MessageBox.ERROR,
					                  minWidth: 200
				                  });
				                  return false;
			                  }


                        /////// VALIDAÇÃO NÃO SELECIONADOS //////////////////////////////////////
                        var listaNaoSelecionados = Array();
                        
                        for (var i = 0; i < listaModificados.length; i++) {
                         
                            if (!Contains(listaSelecionados, listaModificados[i]))
                            {
                                listaNaoSelecionados.push(listaModificados[i]);
                            }
                        }

                        if (listaNaoSelecionados.length > 0)
                        {
                            var naoSelecionados = '';
                            for (var i = 0; i < listaNaoSelecionados.length; i++)
                            {
                               naoSelecionados += listaNaoSelecionados[i].data.Cte;
                               if (i != listaNaoSelecionados.length-1)
                               {
                                    naoSelecionados += ', ';
                               }
                            }

                            Ext.Msg.show({
					                      title: "Mensagem de Erro",
					                      msg: "Os seguintes registros modificados não estão selecionados: <br/>" + naoSelecionados,
					                      buttons: Ext.Msg.OK,
					                      icon: Ext.MessageBox.ERROR,
					                      minWidth: 200
				                      });
				                      return false;
                          }
                        ////////////////////////////////////////////////////////////

                        /////// VALIDAÇÃO NÃO MODIFICADOS //////////////////////////////////////
                        var listaNaoModificados = Array();
                        
                        for (var i = 0; i < listaSelecionados.length; i++) {
                         
                            if (!Contains(listaModificados, listaSelecionados[i]))
                            {
                                listaNaoModificados.push(listaSelecionados[i]);
                            }
                        }

                        if (listaNaoModificados.length > 0)
                        {
                            var naoModificados = '';
                            for (var i = 0; i < listaNaoModificados.length; i++)
                            {
                               naoModificados += listaNaoModificados[i].data.Cte;
                               if (i != listaNaoModificados.length-1)
                               {
                                    naoModificados += ', ';
                               }
                            }

                            Ext.Msg.show({
					                    title: "Mensagem de Erro",
					                    msg: "Os seguintes CTe selecionados não foram modificados para serem salvos: <br/>" + naoModificados,
					                    buttons: Ext.Msg.OK,
					                    icon: Ext.MessageBox.ERROR,
					                    minWidth: 200
				                      });
				                      return false;
                          }
                        ////////////////////////////////////////////////////////////
                        
                        
                        var listaEnvio = Array();
                        var registrosParaSeremSalvos = '';

                        for (var i = 0; i < listaSelecionados.length; i++) {
                            
                            listaEnvio.push(listaSelecionados[i].data);

                            registrosParaSeremSalvos += listaSelecionados[i].data.Cte;
                            if (i != listaSelecionados.length-1)
                            {
                                registrosParaSeremSalvos += ', ';
                            }
                        }


                        //// VALIDA SE OS CTES SELECIONADOS TEM O MESMO FLUXO/////////////
                        var fluxo = '';
                        var fluxoDiferente = false;

                        if (listaSelecionados.length > 1)
                        {
                            fluxo = listaSelecionados[0].data.Fluxo;
                            for (var i = 0; i < listaSelecionados.length; i++) {

                                if (fluxo != listaSelecionados[i].data.Fluxo)
                                {
                                    fluxoDiferente = true;
                                    break;
                                }
                            }
                        }

                        if (fluxoDiferente)
                        {
                            Ext.Msg.show({
					                    title: "Mesagem de Erro",
					                    msg: "Os CTe selecionados devem ter o mesmo Fluxo!",
					                    buttons: Ext.Msg.OK,
					                    icon: Ext.MessageBox.ERROR,
					                    minWidth: 200
				                      });
				                      return false;
                        }
                        ////////////////////////////////////////////////////////////


                        if (Ext.Msg.confirm("Confirmação", "Deseja realmente alterar os seguintes registros: <br/>" + registrosParaSeremSalvos + "?", function(btn, text) {
							if (btn == 'yes') {
								var dados = $.toJSON(listaEnvio);
								$.ajax({
									url: "<%= Url.Action("Salvar") %>",
									type: "POST",
									dataType: 'json',
									data: dados,
									contentType: "application/json; charset=utf-8",
									success: function(result) {
										if (result.success)
										{
											ds.commitChanges();
											ds.load();
										}
										else {
											Ext.Msg.show({
												title: "Mensagem de Erro",
												msg: result.Message,
												buttons: Ext.Msg.OK,
												icon: Ext.MessageBox.ERROR,
												minWidth: 250
											});
										
											// Ext.Msg.alert('Ops...', result.Message);
										}
									},
									failure: function(result) {
										Ext.Msg.show({
											title: "Mensagem de Erro",
											msg: result.Message,
											buttons: Ext.Msg.OK,
											icon: Ext.MessageBox.ERROR,
											width: 300,
											minWidth: 250
										});

										// Ext.Msg.alert('OPS...', result.Message);
									}
								});
						                }
                            else
                            {
                                return false;
                            }
					    })); 
    	            }
                }, {
    	            text: 'Cancelar',
    	            name: 'cancelar',
    	            id: 'cancelar',
    	            iconCls: 'icon-cancel',
    	            handler: function(b, e) {
    		            if (ds.getCount() != 0 && ds.getModifiedRecords().length != 0) {
    		                if (Ext.Msg.confirm("Confirmação", "As alterações não foram salvas e serão perdidas.<br />Deseja Continuar?", function(btn, text) {
							            if (btn == 'yes') {
								            grid.stopEditing();
    		                                grid.getStore().rejectChanges();
                                            // grid.getStore().load();
						                }
					            }));   	                        
    		            }
                        else
                        {
                            Ext.Msg.show({
					            title: "Erro",
					            msg: "Nenhum registro modificado!",
					            buttons: Ext.Msg.OK,
					            icon: Ext.MessageBox.ERROR,
					            minWidth: 200
				            });
                        }
    	            }
                }]
        });
        
		grid.getStore().proxy.on('beforeload', function (p, params) {
           
			params['filter[0].Campo'] = 'dataInicial';
			params['filter[0].Valor'] = new Array(Ext.getCmp("filtro-data-inicial").getValue().format('d/m/Y') + " " + Ext.getCmp("filtro-data-inicial").getValue().format('H:i:s'));
			params['filter[0].FormaPesquisa'] = 'Start';

			params['filter[1].Campo'] = 'dataFinal';
			params['filter[1].Valor'] = new Array(Ext.getCmp("filtro-data-final").getValue().format('d/m/Y') + " " + Ext.getCmp("filtro-data-inicial").getValue().format('H:i:s'));
			params['filter[1].FormaPesquisa'] = 'Start';

			params['filter[2].Campo'] = 'serie';
			params['filter[2].Valor'] = Ext.getCmp("filtro-serie").getValue();
			params['filter[2].FormaPesquisa'] = 'Start';

			params['filter[3].Campo'] = 'despacho';
			params['filter[3].Valor'] = Ext.getCmp("filtro-despacho").getValue();
			params['filter[3].FormaPesquisa'] = 'Start';

			params['filter[4].Campo'] = 'fluxo';
			params['filter[4].Valor'] = Ext.getCmp("filtro-fluxo").getValue();
			params['filter[4].FormaPesquisa'] = 'Start';

			params['filter[5].Campo'] = 'chave';
			params['filter[5].Valor'] = Ext.getCmp("filtro-Chave").getValue();
			params['filter[5].FormaPesquisa'] = 'Start';

			params['filter[6].Campo'] = 'Origem';
			params['filter[6].Valor'] = Ext.getCmp("filtro-Ori").getValue();
			params['filter[6].FormaPesquisa'] = 'Start';

			params['filter[7].Campo'] = 'Destino';
			params['filter[7].Valor'] = Ext.getCmp("filtro-Dest").getValue();
			params['filter[7].FormaPesquisa'] = 'Start';

			params['filter[8].Campo'] = 'Vagao';
			params['filter[8].Valor'] = Ext.getCmp("filtro-numVagao").getValue();
			params['filter[8].FormaPesquisa'] = 'Start';

			params['filter[9].Campo'] = 'UfDcl';
			params['filter[9].Valor'] = Ext.getCmp("filtro-UfDcl").getRawValue();
			params['filter[9].FormaPesquisa'] = 'Start';    

		});

        function MudaCor(row, index) {
            
            if (row.data.Complementado || row.data.DataComplemento != null){
                return 'corRed';
            }
        }

        var dataAtual = new Date();
		
		var dataInicial =	{
			xtype: 'datefield',
			fieldLabel: 'Data Inicial',
			id: 'filtro-data-inicial',
			name: 'dataInicial',
			width: 100,
			allowBlank: false,
			vtype: 'daterange',
			endDateField: 'filtro-data-final',
			hiddenName: 'dataInicial',
			value: dataAtual		
		};
	
		var dataFinal = {
			xtype: 'datefield',
			fieldLabel: 'Data Final',
			id: 'filtro-data-final',
			name: 'dataFinal',
			width: 100,
			allowBlank: false,
			vtype: 'daterange',
			startDateField: 'filtro-data-inicial',
			hiddenName: 'dataFinal',
			value: dataAtual
		};

		var origem =	{
			xtype: 'textfield',
			vtype: 'cteestacaovtype',
			style: 'text-transform: uppercase',
			id: 'filtro-Ori',
			fieldLabel: 'Origem',
			autoCreate: { tag: 'input', type: 'text', maxlength: '3', autocomplete: 'off' },
			name: 'Origem',
			allowBlank: true,
			maxLength: 3,
			width: 50,
			hiddenName: 'Origem'
		};	

		var cboCteCodigoControle = {
			xtype: 'combo',
			store: dsCodigoControle,
			allowBlank: true,
			lazyInit: false,
			lazyRender: false, 
			mode: 'local',
			typeAhead: false,
			triggerAction: 'all',
			fieldLabel: 'UF DCL',
			name: 'filtro-UfDcl',
			id: 'filtro-UfDcl',
			hiddenName: 'filtro-UfDcl',
			displayField: 'CodigoControle',
			forceSelection: true,
			width: 70,
			valueField: 'Id',
			emptyText: 'Selecione...',
			editable: false,
			tpl : '<tpl for="."><div class="x-combo-list-item">{CodigoControle}&nbsp;</div></tpl>'		
		};

		var destino = {
			xtype: 'textfield',
			vtype: 'cteestacaovtype',
			style: 'text-transform: uppercase',
			id: 'filtro-Dest',
			fieldLabel: 'Destino',
			autoCreate: { tag: 'input', type: 'text', maxlength: '3', autocomplete: 'off' },
			name: 'Destino',
			allowBlank: true,
			maxLength: 3,
			width: 50,
			hiddenName: 'Destino'
		};

		var serie = {
			xtype: 'textfield',
			vtype: 'ctesdvtype',
			style: 'text-transform: uppercase',
			id: 'filtro-serie',
			fieldLabel: 'Série',
			name: 'serie',
			allowBlank: true,
			maxLength: 20,
			width: 50,
			hiddenName: 'serie',
			autoCreate: { tag: 'input', type: 'text', maxlength: '3', autocomplete: 'off' }
		};

		var despacho = {
			xtype: 'textfield',
			vtype: 'ctedespachovtype',
			id: 'filtro-despacho',
			fieldLabel: 'Despacho',
			name: 'despacho',
			allowBlank: true,
			maxLength: 20,
			width: 80,
			hiddenName: 'despacho',
			autoCreate: { tag: 'input', type: 'text', maxlength: '6', autocomplete: 'off' }
		};

		var fluxo = {
			xtype: 'textfield',
			vtype: 'ctefluxovtype',
			style: 'text-transform: uppercase',
			id: 'filtro-fluxo',
			fieldLabel: 'Fluxo',
			name: 'fluxo',
			allowBlank: true,
			autoCreate: { tag: 'input', type: 'text', maxlength: '5', autocomplete: 'off' },
			maxLength: 20,
			width: 60,
			minValue: 0,
			maxValue: 99999,
			hiddenName: 'fluxo'
		};

		var Vagao = {
			xtype: 'textfield',
			vtype: 'ctevagaovtype',
			style: 'text-transform: uppercase',
			id: 'filtro-numVagao',
			fieldLabel: 'Num. Vagão',
			name: 'numVagao',
			allowBlank: true,
			maxLength: 20,
			width: 80,
			hiddenName: 'numVagao',
			autoCreate: { tag: 'input', type: 'text', maxlength: '7', autocomplete: 'off' }
		};

		var chave = {
			xtype: 'textfield',
			vtype: 'ctevtype',
			id: 'filtro-Chave',
			fieldLabel: 'Chave CTe',
			name: 'Chave',
			allowBlank: true,
			maxLength: 50,
			autoCreate: { tag: 'input', type: 'text', maxlength: '44', autocomplete: 'off' },
			width: 320,
			hiddenName: 'Chave'
		};

		var linha1col1 = {
			width: 120,
			layout: 'form',
			border: false,
			items:
				[dataInicial]
		};

		var arrCodigoDcl = {
            width: 75,
            layout: 'form',
            border: false,
            items:
				[cboCteCodigoControle]
        };

		var linha1col2 = {
				    width: 120,
				    layout: 'form',
				    border: false,
				    items: [dataFinal]
				};
		
		var linha1col3 = {
                width: 340,
                layout: 'form',
                border: false,
                items:
					[chave]
            };

		

		var linha2col3 = {
                    width: 70,
                    layout: 'form',
                    border: false,
                    items:
						[origem]
                };
		var linha2col4 = {
					width: 70,
					layout: 'form',
					border: false,
					items:
						[destino]
				};

		var linha2col5 =  {
							width: 70,
							layout: 'form',
							border: false,
							items:
								[serie]
						};

		var linha2col6 = {
				    width: 100,
				    layout: 'form',
				    border: false,
				    items:
						[despacho]
				};

		var linha2col7 = {
                    width: 100,
                    layout: 'form',
                    border: false,
                    items:
						[fluxo]
                };
		
		var linha2col8 = {
			width: 100,
			layout: 'form',
			border: false,
			items:
				[Vagao]
		};
		
		var arrlinha1 = {
			    layout: 'column',
			    border: false,
			    items: [linha1col1,linha1col2, linha1col3]
			};

		var arrlinha2 = {
			    layout: 'column',
			    border: false,
			    items: [arrCodigoDcl,linha2col5, linha2col6, linha2col3,linha2col4,linha2col7, linha2col8]
			};
		
		var arrCampos = new Array();
		arrCampos.push(arrlinha1);
		arrCampos.push(arrlinha2);

        filters = new Ext.form.FormPanel({
            id: 'grid-filtros',
            title: "Filtros",
            region: 'center',
            bodyStyle: 'padding: 15px',
            labelAlign: 'top',
            items:
			[arrCampos],
            buttonAlign: "center",
            buttons:
	        [
                {
                    text: 'Pesquisar',
                    type: 'submit',
                    iconCls: 'icon-find',
                    handler: function (b, e) {
					   Pesquisar();
                    }
                },
	            {
	                text: 'Limpar',
	                handler: function (b, e) {
						arraySelectItens = new Array();												 
						grid.getStore().removeAll();
						grid.getStore().totalLength = 0;
						grid.getBottomToolbar().bind(grid.getStore());							 
						grid.getBottomToolbar().updateInfo();  
	                    Ext.getCmp("grid-filtros").getForm().reset();
							                    
	                },
	                scope: this
	            }
            ]
        });
        /*
        ds.proxy.on('beforeload', function(p, params) {
				var dataInicial = Ext.getCmp("filtro-data-inicial").getValue();
				var dataFinal = Ext.getCmp("filtro-data-final").getValue();
				
				if (dataInicial!="")dataInicial=dataInicial.format('d/m/Y');
				if (dataFinal!="")dataFinal=dataFinal.format('d/m/Y');
				
				params['dataInicial'] = dataInicial;
				params['dataFinal'] = dataFinal;
				params['serie'] = Ext.getCmp("filtro-serie").getValue();
				params['despacho'] = Ext.getCmp("filtro-despacho").getValue();
				params['fluxo'] = Ext.getCmp("filtro-fluxo").getValue();
			});
            */
        new Ext.Viewport({
            layout: 'border',
            margins: 10,
            items: [
			{
			    region: 'north',
			    height: 230,
			    items: [{
			        region: 'center',
			        applyTo: 'header-content'
			    },
				filters]
			},
			grid
		]

        });
    });
    </script>
    <style>
        .corRed
        {
            background-color: #FFEEDD;
        }
        .corOrange
        {
            background-color: #FFC58A;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div id="header-content">
        <h1>
            CTe - Complemento</h1>
        <small>Você está em CTe > Complemento</small>
        <br />
    </div>
</asp:Content>
