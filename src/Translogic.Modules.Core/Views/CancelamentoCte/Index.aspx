﻿<%@ Import Namespace="System.Security.Policy" %>

<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Interna.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content runat="server" ID="HeadContent" ContentPlaceHolderID="HeadContent">
	<script type="text/javascript">

	var formModal = null;
	var grid = null;
	var windCanc;
	var lastRowSelected = -1;

	// var dataAtual = new String('<%= String.Format("{0:dd/MM/yyyy}", DateTime.Now) %>');

	// Flag para garantir que uma tela de mensagem que está sobreposta não chame novamente a tela de pesquisa
	var executarPesquisa = true;
	function showResult(btn) {
		executarPesquisa = true;
	}

	var storeChaves = new Ext.data.JsonStore({
		remoteSort: true,
		root: "Items",
		fields: [
					'Chave',
					'Erro',
					'Mensagem'
				]
	});
	
    var storeVagoes = new Ext.data.JsonStore({
		remoteSort: true,
		root: "Items",
		fields: [
					'Vagao',
					'Erro',
					'Mensagem'
				]
	});

	function HabilitarCampos(bloquear)
	{
		Ext.getCmp("filtro-data-inicial").setDisabled(bloquear);
		Ext.getCmp("filtro-data-final").setDisabled(bloquear);
		Ext.getCmp("filtro-serie").setDisabled(bloquear);
		Ext.getCmp("filtro-despacho").setDisabled(bloquear);
		Ext.getCmp("filtro-fluxo").setDisabled(bloquear);
		Ext.getCmp("filtro-Chave").setDisabled(bloquear);
		Ext.getCmp("filtro-Ori").setDisabled(bloquear);
		Ext.getCmp("filtro-Dest").setDisabled(bloquear);
		Ext.getCmp("filtro-numVagao").setDisabled(bloquear);
		Ext.getCmp("filtro-foraDataCancelamento").setDisabled(bloquear);
		Ext.getCmp("filtro-UfDcl").setDisabled(bloquear);
	}

	var dsMotivoCancelamento = new Ext.data.JsonStore({
		root: "Items",
		url: '<%= Url.Action("ObterMotivosCancelamento") %>',
		fields: [
			'Id',
			'Descricao'
						]
    });

	function FormError(form, action) {
		// Ext.Msg.alert('Ops...', action.result.Message);
		Ext.Msg.show({
				title: "Mensagem de Erro",
				msg: action.result.Message,
				buttons: Ext.Msg.OK,
				icon: Ext.MessageBox.ERROR,
				minWidth: 200
			});
		grid.getStore().removeAll();
	}
	
	function Pesquisar() {
		var diferenca = Ext.getCmp("filtro-data-final").getValue() - Ext.getCmp("filtro-data-inicial").getValue(); 
		diferenca = Math.floor(diferenca / (1000 * 60 * 60 * 24)); 

		if (Ext.getCmp("filtro-data-inicial").getValue() == '' || Ext.getCmp("filtro-data-final").getValue() == '') {
			executarPesquisa = false;
			Ext.Msg.show({
				title: "Mensagem de Erro",
				msg: "Preencha os filtro datas!",
				buttons: Ext.Msg.OK,
				icon: Ext.MessageBox.ERROR,
				minWidth: 200,
				fn: showResult
			});
		}
		else if (diferenca > 30) {
			executarPesquisa = false;
			Ext.Msg.show({
			title: "Mensagem de Erro",
			msg: "O período da pesquisa não deve ultrapassar 30 dias",
			buttons: Ext.Msg.OK,
			icon: Ext.MessageBox.ERROR,
			minWidth: 200,
			fn: showResult
			});
		}	
		else if (Ext.getCmp("filtro-serie").getValue() != '' && Ext.getCmp("filtro-despacho").getValue() == '') {
			executarPesquisa = false;
			Ext.Msg.show({
				title: "Mensagem de Erro",
				msg: "Não é possível filtrar apenas pela a Série do Despacho.<br/>Também é necessário preencher o número do Despacho.",
				buttons: Ext.Msg.OK,
				icon: Ext.MessageBox.ERROR,
				minWidth: 200,
				fn: showResult
			});
		}
		else if (Ext.getCmp("filtro-despacho").getValue() != '' && Ext.getCmp("filtro-serie").getValue() == '') {
			executarPesquisa = false;
			Ext.Msg.show({
				title: "Mensagem de Erro",
				msg: "Não é possível filtrar apenas pelo número do Despacho.<br/>Também é necessário preencher a Série do Despacho.",
				buttons: Ext.Msg.OK,
				icon: Ext.MessageBox.ERROR,
				minWidth: 200,
				fn: showResult
			});
		}
		else {
			executarPesquisa = true;
			grid.getStore().load();
		}
	}
  
/* ------------- Ação CTRL+C nos campos da Grid ------------- */
	var isCtrl = false;
	document.onkeyup = function(e) {
	    var keyID = event.keyCode;

	    // 17 = tecla CTRL
	    if (keyID == 17) {
	        isCtrl = false; //libera tecla CTRL
	    }
	};

	document.onkeydown = function(e) {

	    var keyID = event.keyCode;

	    // verifica se CTRL esta acionado
	    if (keyID == 17) {
	        isCtrl = true;
	    }

	    if ((keyID == 13) && (executarPesquisa)) {
	        Pesquisar();
	        return;
	    }

	    // 67 = tecla 'c'
	    if (keyID == 67 && isCtrl == true) {

	        // verifica se há selecão na tabela
	        if (grid.getSelectionModel().hasSelection()) {
	            // captura todas as linhas selecionadas
	            var recordList = grid.getSelectionModel().getSelections();

	            var a = [];
	            var separator = '\t'; // separador para colunas no excel
	            for (var i = 0; i < recordList.length; i++) {
	                var s = '';
	                var item = recordList[i].data;
	                for (key in item) {
	                    if (key != 'Id') { //elimina o campo id da linha
	                        s = s + item[key] + separator;
	                    }
	                }
	                s = s.substr(0, s.length - 1); //retira o ultimo separador '\t' da linha
	                a.push(s);
	            }
	            window.clipboardData.setData('Text', (a.join('\n'))); //insere linhas no clipBoard
	        }
	    }
	};
/* ------------------------------------------------------- */

	Ext.grid.CheckboxSelectionModel.override({
        hideCheckbox: function() {
            var cm = this.grid.getColumnModel();
            var idx = cm.getIndexById(this.id);
            cm.setHidden(idx, true);
        },
        showCheckbox: function() {
            var cm = this.grid.getColumnModel();
            var idx = cm.getIndexById(this.id);
            cm.setHidden(idx, false);    
        }    
    });
	
    $(function () {

        sm2 = new Ext.grid.CheckboxSelectionModel({
                renderer: function(v, p, record){
                
                    return '<div class="x-grid3-row-checker">&#160;</div>';
                
            },
            listeners: {
                selectionchange: function (sm) {					
				
                    if (sm.getCount()) {
                        Ext.getCmp("salvar").enable();
                    } else {
                        Ext.getCmp("salvar").disable();
                    }
                }
                ,
                beforerowselect : function (sm, rowIndex, keep, record) {
				
                    if (record.data.CteComAgrupamentoNaoAutorizado){
                      Ext.Msg.show({
					                  title: "Mensagem de Erro",
					                  msg: "Impossível cancelar o CTe!</BR>CTe com pendência de autorização!",
					                  buttons: Ext.Msg.OK,
					                  icon: Ext.MessageBox.ERROR,
					                  minWidth: 200
				              });
                      return false; 
                    }
                    else if (record.data.CtePago){
                      Ext.Msg.show({
					                  title: "Mensagem de Erro",
					                  msg: "Impossível cancelar o CTe!</BR>CTe já está pago!",
					                  buttons: Ext.Msg.OK,
					                  icon: Ext.MessageBox.ERROR,
					                  minWidth: 200
				              });
                      return false;
                    }
//                    else if (record.data.ForaDoTempoCancelamento){
//                      Ext.Msg.show({
//					                  title: "Mensagem de Aviso",
//					                  msg: "CTe fora do tempo de cancelamento!</BR>",
//					                  buttons: Ext.Msg.OK,
//					                  icon: Ext.MessageBox.WARNING,
//					                  minWidth: 200
//				              });
//					  lastRowSelected = rowIndex;
//                      return false;
//                    }
                }
            }
        });

        var dsCodigoControle = new Ext.data.JsonStore({
            root: "Items",
            autoLoad: true,
            url: '<%= Url.Action("ObterCodigoSerieDesp") %>',
            fields: [
                        'Id',
                        'CodigoControle'
			        ]   		
        });

        grid = new Translogic.PaginatedGrid({
			autoLoadGrid: false,
			id: "gridCte", 
			url: '<%= Url.Action("ObterCtes") %>',			
            stripeRows: false,
            region: 'center',
			//  loadMask: { msg: App.Resources.Web.Carregando },
			 viewConfig: {
                forceFit: true,
                getRowClass: MudaCor
            },
            fields: [
                    'CteId',
                    'Fluxo',
                    'Origem',
                    'Destino',
                    'Mercadoria',
                    'ClienteFatura',
                    'Cte',
					'CodigoVagao',
                    'Serie',
                    'Despacho',  
                    'DateEmissao',
                    'CteComAgrupamentoNaoAutorizado',
                    'CtePago',
                    'ForaDoTempoCancelamento',
                    'DataPedido',
                    'Acima168hs',
                    'RateioCte',
                    'Chave'
					],
                columns: [
						new Ext.grid.RowNumberer(),
                        sm2,
                        { dataIndex: "CteId", hidden: true },
                        { header: 'Num Vagão', dataIndex: "CodigoVagao", width: 100, sortable: true },
						{ header: 'Fluxo', dataIndex: "Fluxo", sortable: true },
						{ header: 'Origem', dataIndex: "Origem", sortable: true },
						{ header: 'Destino', dataIndex: "Destino", sortable: true },
				        { header: 'Mercadoria', dataIndex: "Mercadoria", sortable: true },
				        { header: 'Cliente Fatura', dataIndex: "ClienteFatura", sortable: false },
				        { header: 'CTe', dataIndex: "Cte", sortable: true },
                        { header: 'Série', dataIndex: "Serie", sortable: false },
				        { header: 'Despacho', dataIndex: "Despacho", sortable: false },
				        { header: 'Data Emissão', dataIndex: "DateEmissao", sortable: true },
                        { dataIndex: "CteComAgrupamentoNaoAutorizado", hidden: true },
                        { dataIndex: "CtePago", hidden: true },
                        { dataIndex: "ForaDoTempoCancelamento", hidden: true },
                        { dataIndex: "DataPedido", hidden: true },
                        { dataIndex: "Acima168hs", hidden: true },
                        { dataIndex: "RateioCte", hidden: true },
                        { dataIndex: "Chave", hidden: true }
					],
            sm: sm2,
			listeners: {
				rowclick: function(grid, rowIndex, e) {
				    
					if (sm2.isSelected(rowIndex)) {
						if (rowIndex == lastRowSelected) {
							grid.getSelectionModel().deselectRow(rowIndex);
							lastRowSelected = -1;
						}
						else {
							lastRowSelected = rowIndex;
						}
					}
				}
			}
        });

        function MudaCor(row, index) {
            if (row.data.CteComAgrupamentoNaoAutorizado) {
                return 'corRed';
            }
            else if (row.data.CtePago) {
                return 'corOrange';
            }
            else if (row.data.Acima168hs) {
                return 'corYellow';
            }
        } 
        
	var cboCteCodigoControle = {
		xtype: 'combo',
		store: dsCodigoControle,
		allowBlank: true,
		lazyInit: false,
		lazyRender: false, 
		mode: 'local',
		typeAhead: false,
		triggerAction: 'all',
		fieldLabel: 'UF DCL',
		name: 'filtro-UfDcl',
		id: 'filtro-UfDcl',
		hiddenName: 'filtro-UfDcl',
		displayField: 'CodigoControle',
		forceSelection: true,
		width: 70,
		valueField: 'Id',
		emptyText: 'Selecione...',
		editable: false,
		tpl : '<tpl for="."><div class="x-combo-list-item">{CodigoControle}&nbsp;</div></tpl>'
	};

        var chkForaData = {
            xtype: 'checkbox',
            fieldLabel: 'Fora da data',
            id: 'filtro-foraDataCancelamento',
            name: 'foraDataCancelamento',
            hiddenName: 'foraDataCancelamento',
            validateField: true,
            value: false
        };

		var dataAtual = new Date();

		var dataInicial =	{
			xtype: 'datefield',
			fieldLabel: 'Data Inicial',
			id: 'filtro-data-inicial',
			name: 'dataInicial',
			width: 83,
			allowBlank: false,
			vtype: 'daterange',
			endDateField: 'filtro-data-final',
			hiddenName: 'dataInicial',
			value: dataAtual		
		};
	
		var dataFinal = {
			xtype: 'datefield',
			fieldLabel: 'Data Final',
			id: 'filtro-data-final',
			name: 'dataFinal',
			width: 83,
			allowBlank: false,
			vtype: 'daterange',
			startDateField: 'filtro-data-inicial',
			hiddenName: 'dataFinal'  ,
		    value: dataAtual
		};
        
		var origem =	{
			xtype: 'textfield',
			vtype: 'cteestacaovtype',
			style: 'text-transform: uppercase',
			id: 'filtro-Ori',
			fieldLabel: 'Origem',
			autoCreate: { tag: 'input', type: 'text', maxlength: '3', autocomplete: 'off' },
			name: 'Origem',
			allowBlank: true,
			maxLength: 3,
			width: 35,
			hiddenName: 'Origem'
		};

		var destino = {
			xtype: 'textfield',
			vtype: 'cteestacaovtype',
			style: 'text-transform: uppercase',
			id: 'filtro-Dest',
			fieldLabel: 'Destino',
			autoCreate: { tag: 'input', type: 'text', maxlength: '3', autocomplete: 'off' },
			name: 'Destino',
			allowBlank: true,
			maxLength: 3,
			width: 35,
			hiddenName: 'Destino'
		};

		var serie = {
			xtype: 'textfield',
			vtype: 'ctesdvtype',
			style: 'text-transform: uppercase',
			id: 'filtro-serie',
			fieldLabel: 'Série',
			name: 'serie',
			allowBlank: true,
			autoCreate: { tag: 'input', type: 'text', maxlength: '3', autocomplete: 'off' },
			maxLength: 20,
			width: 30,
			hiddenName: 'serie'
		};

		var despacho = {
			xtype: 'textfield',
			vtype: 'ctedespachovtype',
			id: 'filtro-despacho',
			fieldLabel: 'Despacho',
			name: 'despacho',
			allowBlank: true,
			maxLength: 20,
			autoCreate: { tag: 'input', type: 'text', maxlength: '6', autocomplete: 'off' },
			width: 55,
			hiddenName: 'despacho'
		};

		var fluxo = {
			xtype: 'textfield',
			vtype: 'ctefluxovtype',
			style: 'text-transform: uppercase',
			id: 'filtro-fluxo',
			fieldLabel: 'Fluxo',
			name: 'fluxo',
			allowBlank: true,
			autoCreate: { tag: 'input', type: 'text', maxlength: '5', autocomplete: 'off' },
			maxLength: 20,
			width: 45,
			minValue: 0,
			maxValue: 99999,
			hiddenName: 'fluxo'
		};

		var Vagao = {
			xtype: 'textfield',
			style: 'text-transform: uppercase',
			id: 'filtro-numVagao',
			fieldLabel: 'Vagão',
			name: 'numVagao',
			allowBlank: true,
			autoCreate: { tag: 'input', type: 'text', autocomplete: 'off' },
			width: 150,
			hiddenName: 'numVagao'
		};
        
        var btnLoteVagao = {
			text: 'Informar Vagões',
			xtype: 'button',
			iconCls: 'icon-find',
			handler: function (b, e) {
				windowStatusVagoes = new Ext.Window({
							            id: 'windowStatusVagoes',
							         	title: 'Informar vagões',
							         	modal: true,
							         	width: 855,
							         	resizable: false,
							         	height: 380,
							         	autoLoad: {
							         		url: '<%= Url.Action("FormInformarVagoes") %>',
							         		scripts: true
							         	}
							         });

                windowStatusVagoes.show();
			}
		};

		var chave = {
			xtype: 'textfield',
			maskRe: /[0-9]/,
			id: 'filtro-Chave',
			fieldLabel: 'Chave CTe',
			name: 'Chave',
			autoCreate: { tag: 'input', type: 'text', maxlength: '44', autocomplete: 'off' },
			allowBlank: true,
			maxLength: 50,
			width: 275,
			hiddenName: 'Chave  '
		};

		var btnLoteChaveNfe = {
			text: 'Informar Chaves',
			xtype: 'button',
			iconCls: 'icon-find',
			handler: function (b, e) {
					
				windowStatusNFe = new Ext.Window({
								id: 'windowStatusNFe',
								title: 'Informar chaves',
								modal: true,
								width: 855,
								resizable: false,
								height: 380,
								autoLoad: {
									url: '<%= Url.Action("FormInformarChaves") %>',
									scripts: true
								}
							});

							windowStatusNFe.show();			
								
			}
		};

		var arrDataIni = {
			width: 87,
			layout: 'form',
			border: false,
			items:
				[dataInicial]
		};

		var arrDataFim = {
				    width: 87,
				    layout: 'form',
				    border: false,
				    items:
						[dataFinal]
				};

		var arrOrigem = {
                    width: 43,
                    layout: 'form',
                    border: false,
                    items:
						[origem]
                };
		var arrDestino = {
					width: 43,
					layout: 'form',
					border: false,
					items:
						[destino]
				};

		var arrSerie =  {
							width: 37,
							layout: 'form',
							border: false,
							items:
								[serie]
						};

		var arrDespacho = {
				    width: 60,
				    layout: 'form',
				    border: false,
				    items:
						[despacho]
				};

		var arrFluxo = {
                    width: 50,
                    layout: 'form',
                    border: false,
                    items:
						[fluxo]
                };
		
		var arrVagao = {
			width: 160,
			layout: 'form',
			border: false,
			items:
				[Vagao]
		};

        var arrBtnLoteVagoes = {
            width: 120,
			layout: 'form',
			style: 'padding: 17px 0px 0px 0px;',
			border: false,
			items: [btnLoteVagao]

        };
        
		var arrChave = {
                    width: 280,
                    layout: 'form',
                    border: false,
                    items:
						[chave]
                };

		var arrbtnLoteChaveNfe = {
			width: 280,
			layout: 'form',
			style: 'padding: 17px 0px 0px 0px;',
			border: false,
			items:
					[btnLoteChaveNfe]
		};

		var arrForaData = {
                    width: 120,
                    layout: 'form',
                    border: false,
                    items:
						[chkForaData]
                   };
            
        var arrCodigoDcl = {
                width: 75,
                layout: 'form',
                border: false,
                items:
					[cboCteCodigoControle]
                };
                       
		
		var arrlinha1 = {
			    layout: 'column',
			    border: false,
			    items: [arrDataIni, arrDataFim, arrFluxo, arrChave, arrbtnLoteChaveNfe ]
			};

		var arrlinha2 = {
			    layout: 'column',
			    border: false,
			    items: [arrCodigoDcl, arrSerie, arrDespacho, arrOrigem, arrDestino, arrVagao, arrBtnLoteVagoes, arrForaData]
			};
		
		var arrCampos = new Array();
		arrCampos.push(arrlinha1);
		arrCampos.push(arrlinha2);

        filters = new Ext.form.FormPanel({
            id: 'grid-filtros',
            title: "Filtros",
            region: 'center',
            bodyStyle: 'padding: 15px',
            labelAlign: 'top',
            items:
			[arrCampos],			
            buttonAlign: "center",
            buttons:
	        [
                {
                    text: 'Cancelar',
                    id: 'salvar',
                    iconCls: 'icon-save',
                    disabled: true,
                    handler: function (b, e) {

                        var listaEnvio = Array();
                        var selected = sm2.getSelections();

                        for (var i = 0; i < selected.length; i++) {
                            listaEnvio.push(selected[i].data);
                        }

						confirmCancelamento(listaEnvio);
                    },
                    scope: this
                },
                {
                    text: 'Pesquisar',
                    type: 'submit',
                    iconCls: 'icon-find',
                    handler: function (b, e) {
					    Pesquisar();
                    }
                },
	            {
	                text: 'Limpar',
	                handler: function (b, e) {	    
                    
						storeChaves.removeAll();
	                    storeVagoes.removeAll();

						grid.getStore().removeAll();
						grid.getStore().totalLength = 0;
						grid.getBottomToolbar().bind(grid.getStore());							 
						grid.getBottomToolbar().updateInfo();  
	                    Ext.getCmp("grid-filtros").getForm().reset();

						HabilitarCampos(false);
	                },
	                scope: this
	            }
            ]
        });

		dsMotivoCancelamento.load();
        grid.getStore().proxy.on('beforeload', function(p, params) {	 
				
			params['filter[0].Campo'] = 'dataInicial';
			params['filter[0].Valor'] = new Array(Ext.getCmp("filtro-data-inicial").getValue().format('d/m/Y') + " " + Ext.getCmp("filtro-data-inicial").getValue().format('H:i:s')); 
			params['filter[0].FormaPesquisa'] = 'Start';

			params['filter[1].Campo'] = 'dataFinal';
			params['filter[1].Valor'] = new Array(Ext.getCmp("filtro-data-final").getValue().format('d/m/Y') + " " + Ext.getCmp("filtro-data-inicial").getValue().format('H:i:s'));  
			params['filter[1].FormaPesquisa'] = 'Start';

			params['filter[2].Campo'] = 'serie';
			params['filter[2].Valor'] = Ext.getCmp("filtro-serie").getValue();
			params['filter[2].FormaPesquisa'] = 'Start';

			params['filter[3].Campo'] = 'despacho';
			params['filter[3].Valor'] = Ext.getCmp("filtro-despacho").getValue();
			params['filter[3].FormaPesquisa'] = 'Start';

			params['filter[4].Campo'] = 'fluxo';
			params['filter[4].Valor'] = Ext.getCmp("filtro-fluxo").getValue();
			params['filter[4].FormaPesquisa'] = 'Start';

			var listaChave = "";
			if(storeChaves.getCount() != 0)
			{
				storeChaves.each(
					function (record) {
						if (!record.data.Erro) {
							listaChave += record.data.Chave + ";";	
						}
					}
				);
			} else {
				listaChave = Ext.getCmp("filtro-Chave").getValue();	
			}

			params['filter[5].Campo'] = 'chave';
            //params['filter[5].Valor'] = Ext.getCmp("filtro-Chave").getValue();
            params['filter[5].Valor'] = listaChave;
			params['filter[5].FormaPesquisa'] = 'Start';
			
			params['filter[6].Campo'] = 'Origem';
			params['filter[6].Valor'] = Ext.getCmp("filtro-Ori").getValue();
			params['filter[6].FormaPesquisa'] = 'Start';
			
			params['filter[7].Campo'] = 'Destino';
			params['filter[7].Valor'] = Ext.getCmp("filtro-Dest").getValue();
			params['filter[7].FormaPesquisa'] = 'Start';
			
            var listaVagoes = '';
			if(storeVagoes.getCount() != 0)
			{
				storeVagoes.each(
					function (record) {
						if (!record.data.Erro) {
							listaVagoes += record.data.Vagao + ";";	
						}
					}
				);
			} else {
				listaVagoes = Ext.getCmp("filtro-numVagao").getValue();	
			}

			params['filter[8].Campo'] = 'Vagao';
			params['filter[8].Valor'] = listaVagoes;
			params['filter[8].FormaPesquisa'] = 'Start';

			params['filter[9].Campo'] = 'ForaDataCancelamento';
			params['filter[9].Valor'] = Ext.getCmp("filtro-foraDataCancelamento").getValue();
			params['filter[9].FormaPesquisa'] = 'Start';

            params['filter[10].Campo'] = 'UfDcl';
			params['filter[10].Valor'] = Ext.getCmp("filtro-UfDcl").getRawValue();
			params['filter[10].FormaPesquisa'] = 'Start';  
            
			var countChaves = storeChaves.getCount();
			if(countChaves > 0)
			{
				grid.pagingToolbar.pageSize = countChaves;
			}else{
				grid.pagingToolbar.pageSize = 50;
			}             

			});

        new Ext.Viewport({
            layout: 'border',
            margins: 10,
            items: [
			{
			    region: 'north',
				height: 240,
				autoScroll: true,
			    items: [{
			        region: 'center',
			        applyTo: 'header-content'
			    },
				filters]
			},
			grid
		]

        });

    });

	function onCancelamentoSuccess(){
		grid.getStore().removeAll();
		Ext.Msg.alert('Cancelamento', 'CTe Cancelado com sucesso!');

		Pesquisar();
		grid.getStore().load();

		/*
        filters.getForm().submit({
            url: '<%= Url.Action("ObterCtes") %>',
            waitMsg: 'Aguarde, processando...',
            reset: false,
            success: function (filters, action) {
                grid.getStore().removeAll();
				grid.getStore().load();
                                                            
                    Ext.Msg.show({
			                title: "Mensagem Informativa",
			                msg: "Cte(s) cancelado(s) com sucesso!",
			                buttons: Ext.Msg.OK,
			                icon: Ext.MessageBox.INFO,
			                minWidth: 200
		                });

            },
						failure: function (filters, action) {
							if (action.result == null) {
								var jsonData = Ext.util.JSON.decode(action.response.responseText);
								var msg = jsonData.message;
                    // Ext.Msg.alert('Ops...', msg);
								Ext.Msg.show({
									title: "Mensagem de Erro",
									msg: msg,
									buttons: Ext.Msg.OK,
									icon: Ext.MessageBox.ERROR,
									minWidth: 200
								});

                }
                else {
                    FormError(filters, action);
                }
            }
        });*/ 
	}

	function MostrarWindowErros(mensagensErro){
        var textarea = new Ext.form.TextArea({
            xtype:'textarea',
            fieldLabel: 'Foram encontrados os seguintes erros',
            value: mensagensErro,
            readOnly: true,
            height: 220,
            width: 395
        });

        var formErros = new Ext.FormPanel({
            id: 'formErros',
            bodyStyle: 'padding: 15px',
            labelAlign: 'top',
            items: [textarea],
            buttonAlign: 'center',
            buttons: [{
                text: 'Fechar',
                type: 'button',
                handler: function (b, e) {
                    windowErros.close();
                }
            }]
        });

			windowErros = new Ext.Window({
			id: 'windowErros',
			title: 'Erros',
			modal: true,
			width: 444,
			height: 350,
			items:[formErros]
		});
		windowErros.show();
    }

	function salvarCancelamento(dataTmp) {

	    $.ajax({
	        url: "<%= Url.Action("Salvar") %>",
	        type: "POST",
	        dataType: 'json',
	        data: dataTmp,
	        contentType: "application/json; charset=utf-8",
	        success: function(result) {
	            if (result.success) {
	                //onCancelamentoSuccess();
	                Ext.Msg.show({
	                    title: "Mensagem de Informação",
	                    msg: "CTe(s) cancelado(s) com sucesso!",
	                    buttons: Ext.Msg.OK,
	                    icon: Ext.MessageBox.INFO,
	                    minWidth: 200
	                });
	                grid.getStore().removeAll();
	                Pesquisar();
	            } else {
	                MostrarWindowErros(result.Message);
	            }
	            Ext.getBody().unmask();
	        },
	        failure: function(result) {
	            MostrarWindowErros(result.Message);
	            Ext.getBody().unmask();
	        }
	    });

        Ext.getBody().mask("Processando cancelamento", "x-mask-loading");
		windCanc.close();

////        //onCancelamentoSuccess();
////		Ext.Msg.show({
////			title: "Mensagem de Informação",
////			msg: "CTe(s) cancelado(s) com sucesso!",
////			buttons: Ext.Msg.OK,
////			icon: Ext.MessageBox.INFO,
////			minWidth: 200
////		});
////		grid.getStore().removeAll();
////		Pesquisar();
	}

	function confirmCancelamento(listaEnvio) {
		
		if(windCanc){
			windCanc = null;
		}

		windCanc = new Ext.Window({
			id: 'windCanc',
			title: 'Cancelamento',
			modal: true,
			width: 444,
			closable: false,
			closeAction: 'close',
			buttonAlign: 'center',
			items: [
				{
					xtype: 'form',
					labelWidth: 50,
					items: [
					{
						xtype: 'label',
						html: '<div>Para cancelamento é necessário Selecionar um motivo na lista abaixo.</div>'
					},
					{
						xtype: 'combo',
						store: dsMotivoCancelamento,
						allowBlank: false,
						lazyRender: false,
						mode: 'local',
						typeAhead: false,
						triggerAction: 'all',
						fieldLabel: 'Motivo',
						name: 'cboMotivoCancelamento',
						id: 'cboMotivoCancelamento',
						displayField: 'Descricao',
						forceSelection: true,
						width: 300,
						valueField: 'Id',
						editable: false,
						emptyText: 'Selecione...'
					},
					{
						xtype: 'label',
						align: 'center',
						html: "Você possui " + listaEnvio.length + " para cancelamento, deseja realmente cancelar?"
					}
					]
				}],
			buttons: [{
				text: 'Confirmar',
				handler: function () {
				
					if (!Ext.getCmp("cboMotivoCancelamento").isValid()) {
						return;
					}

					var dto = {
						Dtos: listaEnvio,
						IdMotivoCancelamento: Ext.getCmp("cboMotivoCancelamento").getValue(),
						NumeroVagoesSelecionados: storeVagoes.getCount()
					};
					
					var dados = $.toJSON(dto);

				    $.ajax({
				        url: "<%= Url.Action("ObterCtesRateio") %>",
				        type: "POST",
				        dataType: 'json',
				        data: dados,
				        async: false,
						contentType: "application/json; charset=utf-8",
						success: function(result) {
                            if(result.PossuiMsg) {
                                if (Ext.Msg.confirm("Confirmação", result.Message, function(btn, text) {
                                    if (btn == 'yes') {
                                        if (result.PossuiMsgValidacao === true) {
                                            if (Ext.Msg.confirm("Confirmação", result.MensagemValidacao, function(btnValidacao, textValidacao) {
                                                if (btnValidacao == 'yes') {
                                                    salvarCancelamento(dados);
                                                }
                                            }));
                                        }
                                        else {
                                            salvarCancelamento(dados);
                                        }
                                    }
                                }));
                            }
                            else {
                                if (result.PossuiMsgValidacao === true) {
                                    if (Ext.Msg.confirm("Confirmação", result.MensagemValidacao, function(btnValidacao, textValidacao) {
                                        if (btnValidacao == 'yes') {
                                            salvarCancelamento(dados);
                                        }
                                    }));
                                }
                                else {
                                    salvarCancelamento(dados);
                                }
                            }
						},
						failure: function(result) {
							MostrarWindowErros(result.Message);
							Ext.getBody().unmask();
						}
					});
				}
			}, {
				text: 'Cancelar',
				handler: function () {
					windCanc.close();
				}
			}]
		});
		windCanc.show();
	}

	$(function () { 
		// Ext.getCmp("filtro-data-inicial").focus();
	});

	</script>
	<style>
		.corRed
		{
			background-color: #FFEEDD;
		}
		.corOrange
		{
			background-color: #FFC58A;
		}
		.corYellow
		{
			background-color: #FFFF80;
		}
	</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
	<div id="header-content">
		<h1>
			CTe - Cancelamento</h1>
		<small>Você está em CTe > Cancelamento</small>
		<br />
	</div>
</asp:Content>
