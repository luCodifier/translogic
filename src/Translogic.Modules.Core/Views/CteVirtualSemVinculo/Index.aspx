﻿<%@ Import Namespace="System.Security.Policy" %>

<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Interna.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content runat="server" ID="HeadContent" ContentPlaceHolderID="HeadContent">
    <script type="text/javascript">
        var formModal = null;
        var grid = null;
        var windCanc;
        var lastRowSelected = -1;

        // Flag para garantir que uma tela de mensagem que está sobreposta não chame novamente a tela de pesquisa
        var executarPesquisa = true;
        function showResult(btn) {
            executarPesquisa = true;
        }

        var storeChaves = new Ext.data.JsonStore({
            remoteSort: true,
            root: "Items",
            fields: [
                'Chave',
                'Erro',
                'Mensagem'
            ]
        });

        var storeVagoes = new Ext.data.JsonStore({
            remoteSort: true,
            root: "Items",
            fields: [
                'Vagao',
                'Erro',
                'Mensagem'
            ]
        });

        function FormError(form, action) {
            Ext.Msg.show({
                title: "Mensagem de Erro",
                msg: action.result.Message,
                buttons: Ext.Msg.OK,
                icon: Ext.MessageBox.ERROR,
                minWidth: 200
            });
            grid.getStore().removeAll();
        }

        function Pesquisar() {
            var diferenca = Ext.getCmp("filtro-data-final").getValue() - Ext.getCmp("filtro-data-inicial").getValue();
            diferenca = Math.floor(diferenca / (1000 * 60 * 60 * 24));
            if (Ext.getCmp("filtro-data-inicial").getValue() == '' || Ext.getCmp("filtro-data-final").getValue() == '') {
                executarPesquisa = false;
                Ext.Msg.show({
                    title: "Mensagem de Erro",
                    msg: "Preencha os filtro datas!",
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.ERROR,
                    minWidth: 200,
                    fn: showResult
                });
            }
            else if (diferenca > 7) {
                executarPesquisa = false;
                Ext.Msg.show({
                    title: "Mensagem de Erro",
                    msg: "O período da pesquisa não deve ultrapassar 7 dias",
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.ERROR,
                    minWidth: 200,
                    fn: showResult
                });
            }
            else {
                executarPesquisa = true;
                grid.getStore().load();
            }
        }

        /* ------------- Ação CTRL+C nos campos da Grid ------------- */
        var isCtrl = false;
        document.onkeyup = function (e) {
            var keyID = event.keyCode;
            // 17 = tecla CTRL
            if (keyID == 17) {
                isCtrl = false; //libera tecla CTRL
            }
        };
        document.onkeydown = function (e) {
            var keyID = event.keyCode;
            // verifica se CTRL esta acionado
            if (keyID == 17) {
                isCtrl = true;
            }
            if ((keyID == 13) && (executarPesquisa)) {
                Pesquisar();
                return;
            }
            // 67 = tecla 'c'
            if (keyID == 67 && isCtrl == true) {
                // verifica se há selecão na tabela
                if (grid.getSelectionModel().hasSelection()) {
                    // captura todas as linhas selecionadas
                    var recordList = grid.getSelectionModel().getSelections();
                    var a = [];
                    var separator = '\t'; // separador para colunas no excel
                    for (var i = 0; i < recordList.length; i++) {
                        var s = '';
                        var item = recordList[i].data;
                        for (key in item) {
                            if (key != 'Id') { //elimina o campo id da linha
                                s = s + item[key] + separator;
                            }
                        }
                        s = s.substr(0, s.length - 1); //retira o ultimo separador '\t' da linha
                        a.push(s);
                    }
                    window.clipboardData.setData('Text', (a.join('\n'))); //insere linhas no clipBoard
                }
            }
        };
        /* ------------------------------------------------------- */

        $(function () {
            sm2 = new Ext.grid.CheckboxSelectionModel({
                renderer: function (v, p, record) {
                    if (record.data.ID_MOVIMENTACAO != 0 && record.data.ID_AREA_OPERACIONAL != 0 &&
                        record.data.ID_TREM != 0 && record.data.COMPOSICAO != 0 && record.data.ID_DESPACHO != 0)
                        return '<div class="x-grid3-row-checker">&#160;</div>';
                    else
                        return '<div>&#160;</div>';
                }
            });

            grid = new Translogic.PaginatedGrid({
                autoLoadGrid: false,
                id: "gridCte",
                url: '<%= Url.Action("ObterCtesVirtual") %>',
                stripeRows: false,
                region: 'center',
                viewConfig: {
                    forceFit: true
                },
                fields: [
                    'ID_DESPACHO',
                    'ID_MOVIMENTACAO',
                    'ID_AREA_OPERACIONAL',
                    'NUM_CTE_VIRTUAL',
                    'SERIE_CTE_VIRTUAL',
                    'DATA_EMISSAO_VIRTUAL',
                    'COMPOSICAO',
                    'PREFIXO_TREM',
                    'COD_FLUXO',
                    'ID_TREM',
                    'VAGAO'
                ],
                columns: [
                    new Ext.grid.RowNumberer(),
                    sm2,
                    { header: "Id Movimentação", dataIndex: "ID_MOVIMENTACAO", hidden: true },
                    { header: "Id Area Operacional", dataIndex: "ID_AREA_OPERACIONAL", hidden: true },
                    { header: "Id Trem", dataIndex: "ID_TREM", hidden: true },
                    { header: 'Nº Cte Virtual', dataIndex: "NUM_CTE_VIRTUAL", sortable: true },
                    { header: 'Série Cte Virtual', dataIndex: "SERIE_CTE_VIRTUAL", sortable: false },
                    { header: 'Data Emissão', dataIndex: "DATA_EMISSAO_VIRTUAL", sortable: true },
                    { header: "Id Despacho", dataIndex: "ID_DESPACHO", hidden: true },
                    { header: 'Composição', dataIndex: "COMPOSICAO", sortable: true },
                    { header: 'Prefixo Trem', dataIndex: "PREFIXO_TREM", sortable: true },
                    { header: 'Fluxo', dataIndex: "COD_FLUXO", sortable: true },
                    { header: 'Vagão', dataIndex: "VAGAO", sortable: true }
                ],
                sm: sm2,
                listeners: {
                    cellclick: function (grid, rowIndex, columnIndex, e) {
                        if (columnIndex == 1) {
                            selecionado(rowIndex);
                        } else {
                            var list = grid.getStore().data.items;
                            for (var i = 0; i < list.length; i++) {
                                grid.getSelectionModel().deselectRow(i);
                            }
                        }                   
                    }
                }
            });

            function selecionado(rowIndex) {
                     if (sm2.isSelected(rowIndex)) {
                    var list = grid.getStore().data.items;
                    if (list[rowIndex].data.COMPOSICAO == 0) {
                        grid.getSelectionModel().deselectRow(rowIndex);
                    } else {
                        var arraySelectItens = new Array();
                        if (arraySelectItens.length == 0) {
                            arraySelectItens.push(rowIndex);
                        }
                        var arrayComposicao = new Array();
                        var listSelecionados = grid.getSelectionModel().selections;
                        if (listSelecionados.length > 0) {
                            for (var i = 0; i < listSelecionados.length; i++) {
                                if (arrayComposicao.length == 0) {
                                    arrayComposicao.push(listSelecionados.items[i].data.COMPOSICAO);
                                } else {
                                    $(arrayComposicao).each(function () {
                                        var id = this;
                                        var idComp = listSelecionados.items[i].data.COMPOSICAO
                                        if (id != idComp) {
                                            arrayComposicao.push(idComp);
                                        }
                                    });
                                }
                            }
                        }
                        if (arrayComposicao.length > 0) {
                            for (var j = 0; j < arrayComposicao.length; j++) {
                                for (var i = 0; i < list.length; i++) {
                                    if (jQuery.inArray(i, arraySelectItens) == -1) {
                                        if (list[i].data.COMPOSICAO == arrayComposicao[j]) {
                                            arraySelectItens.push(i);
                                        }
                                    }
                                }
                            }
                        }
                        sm2.selectRows(arraySelectItens, false);
                    }
                } else {
                    var list = grid.getStore().data.items;
                    var composicao = list[rowIndex].data.COMPOSICAO;
                    for (var i = 0; i < list.length; i++) {
                        if (list[i].data.COMPOSICAO == composicao) {
                            grid.getSelectionModel().deselectRow(i);
                        }
                    }
                }
            }
            var dataAtual = new Date();
            var dataInicial = {
                xtype: 'datefield',
                fieldLabel: 'Data Inicial',
                id: 'filtro-data-inicial',
                name: 'dataInicial',
                width: 83,
                allowBlank: false,
                vtype: 'daterange',
                endDateField: 'filtro-data-final',
                hiddenName: 'dataInicial',
                value: dataAtual
            };

            var dataFinal = {
                xtype: 'datefield',
                fieldLabel: 'Data Final',
                id: 'filtro-data-final',
                name: 'dataFinal',
                width: 83,
                allowBlank: false,
                vtype: 'daterange',
                startDateField: 'filtro-data-inicial',
                hiddenName: 'dataFinal',
                value: dataAtual
            };

            var fluxo = {
                xtype: 'textfield',
                vtype: 'ctefluxovtype',
                style: 'text-transform: uppercase',
                id: 'filtro-fluxo',
                fieldLabel: 'Fluxo',
                name: 'fluxo',
                allowBlank: true,
                autoCreate: { tag: 'input', type: 'text', maxlength: '5', autocomplete: 'off' },
                maxLength: 20,
                width: 45,
                minValue: 0,
                maxValue: 99999,
                hiddenName: 'fluxo'
            };

            var Vagao = {
                xtype: 'textfield',
                style: 'text-transform: uppercase',
                id: 'filtro-numVagao',
                fieldLabel: 'Vagão',
                name: 'numVagao',
                allowBlank: true,
                autoCreate: { tag: 'input', type: 'text', autocomplete: 'off' },
                width: 150,
                hiddenName: 'numVagao'
            };

            var btnLoteVagao = {
                text: 'Informar Vagões',
                xtype: 'button',
                iconCls: 'icon-find',
                handler: function (b, e) {
                    windowStatusVagoes = new Ext.Window({
                        id: 'windowStatusVagoes',
                        title: 'Informar vagões',
                        modal: true,
                        width: 855,
                        resizable: false,
                        height: 380,
                        autoLoad: {
                            url: '<%= Url.Action("FormInformarVagoes") %>',
                            scripts: true
                        }
                    });
                    windowStatusVagoes.show();
                }
            };

            var chave = {
                xtype: 'textfield',
                maskRe: /[0-9]/,
                id: 'filtro-Chave',
                fieldLabel: 'Chave CTe',
                name: 'Chave',
                autoCreate: { tag: 'input', type: 'text', maxlength: '44', autocomplete: 'off' },
                allowBlank: true,
                maxLength: 50,
                width: 275,
                hiddenName: 'Chave  '
            };

            var btnLoteChaveNfe = {
                text: 'Informar Chaves',
                xtype: 'button',
                iconCls: 'icon-find',
                handler: function (b, e) {
                    windowStatusNFe = new Ext.Window({
                        id: 'windowStatusNFe',
                        title: 'Informar chaves',
                        modal: true,
                        width: 855,
                        resizable: false,
                        height: 380,
                        autoLoad: {
                            url: '<%= Url.Action("FormInformarChaves") %>',
                            scripts: true
                        }
                    });
                    windowStatusNFe.show();
                }
            };

            var arrDataIni = {
                width: 87,
                layout: 'form',
                border: false,
                items:
                    [dataInicial]
            };

            var arrDataFim = {
                width: 87,
                layout: 'form',
                border: false,
                items: [dataFinal]
            };

            var arrFluxo = {
                width: 50,
                layout: 'form',
                border: false,
                items: [fluxo]
            };

            var arrVagao = {
                width: 160,
                layout: 'form',
                border: false,
                items: [Vagao]
            };

            var arrBtnLoteVagoes = {
                width: 120,
                layout: 'form',
                style: 'padding: 17px 0px 0px 0px;',
                border: false,
                items: [btnLoteVagao]
            };

            var arrChave = {
                width: 280,
                layout: 'form',
                border: false,
                items: [chave]
            };

            var arrbtnLoteChaveNfe = {
                width: 115,
                layout: 'form',
                style: 'padding: 17px 0px 0px 0px;',
                border: false,
                items: [btnLoteChaveNfe]
            };

            var arrlinha1 = {
                layout: 'column',
                border: false,
                items: [arrDataIni, arrDataFim, arrFluxo, arrChave, arrbtnLoteChaveNfe, arrVagao, arrBtnLoteVagoes]
            };
            filters = new Ext.form.FormPanel({
                id: 'grid-filtros',
                title: "Filtros",
                region: 'center',
                bodyStyle: 'padding: 15px',
                labelAlign: 'top',
                items: [arrlinha1],
                buttonAlign: "center",
                buttons:
                    [
                        {
                            text: 'Pesquisar',
                            type: 'submit',
                            iconCls: 'icon-find',
                            handler: function (b, e) {
                                Pesquisar();
                            }
                        },
                        {
                            text: 'Gerar MDF-e',
                            type: 'submit',
                            handler: function (b, e) {
                                GerarMDFE();
                            }
                        },
                        {
                            id: 'btnExportar',
                            name: 'btnExportar',
                            text: 'Exportar',
                            iconCls: 'icon-page-excel',
                            handler: function (c) {
                                Excel();
                            }
                        },
                        {
                            text: 'Limpar',
                            handler: function (b, e) {
                                storeChaves.removeAll();
                                storeVagoes.removeAll();
                                grid.getStore().removeAll();
                                grid.getStore().totalLength = 0;
                                grid.getBottomToolbar().bind(grid.getStore());
                                grid.getBottomToolbar().updateInfo();
                                Ext.getCmp("grid-filtros").getForm().reset();
                            },
                            scope: this
                        }
                    ]
            });

            function Excel() {
                var listaChave = "";
                if (storeChaves.getCount() != 0) {
                    storeChaves.each(
                        function (record) {
                            if (!record.data.Erro) {
                                listaChave += record.data.Chave + ";";
                            }
                        }
                    );
                } else {
                    listaChave = Ext.getCmp("filtro-Chave").getValue();
                }
                var listaVagoes = '';
                if (storeVagoes.getCount() != 0) {
                    storeVagoes.each(
                        function (record) {
                            if (!record.data.Erro) {
                                listaVagoes += record.data.Vagao + ";";
                            }
                        }
                    );
                } else {
                    listaVagoes = Ext.getCmp("filtro-numVagao").getValue();
                }

                var dataInicial = Ext.getCmp("filtro-data-inicial").getValue().format('d/m/Y');
                var dataFinal = Ext.getCmp("filtro-data-final").getValue().format('d/m/Y');
                var fluxo = Ext.getCmp("filtro-fluxo").getValue();
                var chaveCte = listaChave;
                var vagao = listaVagoes;
                var url = "<%= this.Url.Action("Exportar") %>";
                url += String.format("?dataInicial={0}", dataInicial);
                url += String.format("&dataFinal={0}", dataFinal);
                url += String.format("&fluxo={0}", fluxo);
                url += String.format("&chaveCte={0}", chaveCte);
                url += String.format("&vagao={0}", vagao);
                window.open(url, "");
            }

            grid.getStore().proxy.on('beforeload', function (p, params) {
                params['filter[0].Campo'] = 'dataInicial';
                params['filter[0].Valor'] = new Array(Ext.getCmp("filtro-data-inicial").getValue().format('d/m/Y') + " " + Ext.getCmp("filtro-data-inicial").getValue().format('H:i:s'));
                params['filter[0].FormaPesquisa'] = 'Start';

                params['filter[1].Campo'] = 'dataFinal';
                params['filter[1].Valor'] = new Array(Ext.getCmp("filtro-data-final").getValue().format('d/m/Y') + " " + Ext.getCmp("filtro-data-inicial").getValue().format('H:i:s'));
                params['filter[1].FormaPesquisa'] = 'Start';

                params['filter[2].Campo'] = 'fluxo';
                params['filter[2].Valor'] = Ext.getCmp("filtro-fluxo").getValue();
                params['filter[2].FormaPesquisa'] = 'Start';

                var listaChave = "";
                if (storeChaves.getCount() != 0) {
                    storeChaves.each(
                        function (record) {
                            if (!record.data.Erro) {
                                listaChave += record.data.Chave + ";";
                            }
                        }
                    );
                } else {
                    listaChave = Ext.getCmp("filtro-Chave").getValue();
                }

                params['filter[3].Campo'] = 'chave';
                params['filter[3].Valor'] = listaChave;
                params['filter[3].FormaPesquisa'] = 'Start';

                var listaVagoes = '';
                if (storeVagoes.getCount() != 0) {
                    storeVagoes.each(
                        function (record) {
                            if (!record.data.Erro) {
                                listaVagoes += record.data.Vagao + ";";
                            }
                        }
                    );
                } else {
                    listaVagoes = Ext.getCmp("filtro-numVagao").getValue();
                }

                params['filter[4].Campo'] = 'Vagao';
                params['filter[4].Valor'] = listaVagoes;
                params['filter[4].FormaPesquisa'] = 'Start';

                var countChaves = storeChaves.getCount();
                if (countChaves > 0) {
                    grid.pagingToolbar.pageSize = countChaves;
                } else {
                    grid.pagingToolbar.pageSize = 50;
                }
            });

            new Ext.Viewport({
                layout: 'border',
                margins: 10,
                items: [
                    {
                        region: 'north',
                        height: 180,
                        autoScroll: true,
                        items: [{
                            region: 'center',
                            applyTo: 'header-content'
                        },
                            filters]
                    },
                    grid
                ]
            });
        });

        function GerarMDFE() {
            var dto = new Array();
            var arrayMovimentacao = new Array();
            var listSelecionados = grid.getSelectionModel().selections;

            if (listSelecionados.length > 0) {
                for (var i = 0; i < listSelecionados.length; i++) {
                    var objSelecionado = listSelecionados.items[i];
                    if (objSelecionado.data.ID_MOVIMENTACAO != 0 && objSelecionado.data.ID_AREA_OPERACIONAL != 0 &&
                        objSelecionado.data.ID_TREM != 0 && objSelecionado.data.COMPOSICAO != 0 && objSelecionado.data.ID_DESPACHO != 0) {
                        if (arrayMovimentacao.length == 0) {
                            arrayMovimentacao.push(listSelecionados.items[i].data.ID_MOVIMENTACAO);
                            var obj = {
                                ID_MOVIMENTACAO: objSelecionado.data.ID_MOVIMENTACAO,
                                ID_AREA_OPERACIONAL: objSelecionado.data.ID_AREA_OPERACIONAL,
                                ID_TREM: objSelecionado.data.ID_TREM,
                                COMPOSICAO: objSelecionado.data.COMPOSICAO,
                                ID_DESPACHO: objSelecionado.data.ID_DESPACHO
                            };
                            dto.push(obj)
                        } else {
                            var idComp = objSelecionado.data.ID_MOVIMENTACAO
                            if (jQuery.inArray(idComp, arrayMovimentacao) == -1) {
                                arrayMovimentacao.push(idComp);
                                var obj = {
                                    ID_MOVIMENTACAO: objSelecionado.data.ID_MOVIMENTACAO,
                                    ID_AREA_OPERACIONAL: objSelecionado.data.ID_AREA_OPERACIONAL,
                                    ID_TREM: objSelecionado.data.ID_TREM,
                                    COMPOSICAO: objSelecionado.data.COMPOSICAO,
                                    ID_DESPACHO: objSelecionado.data.ID_DESPACHO
                                };
                                dto.push(obj)
                            }
                        }
                    }
                }
            }
            if (dto.length > 0) {
                var dados = $.toJSON(dto);
                $.ajax({
                    url: "<%= Url.Action("GerarDadosMdfe") %>",
                    type: "POST",
                    dataType: 'json',
                    data: dados,
                    contentType: "application/json; charset=utf-8",
                    success: function (result) {
                        var msg = "";
                        for (var i = 0; i < result.Items.length; i++) {
                            msg += result.Items[i].Mensagem + "\r\n";
                        }
                        MostrarMensagemWindow(msg);
                        Ext.getBody().unmask();
                    },
                    failure: function (result) {
                        var msg = "";
                        for (var i = 0; i < result.Items.length; i++) {
                            msg += result.Items[i].Mensagem + "\r\n";
                        }
                        MostrarMensagemWindow(msg);
                        Ext.getBody().unmask();
                    }
                });
                Ext.getBody().mask("Aguarde a geração do MDF-e!", "x-mask-loading");
            } else {
                Ext.Msg.show({
                    title: "Mensagem",
                    msg: "É necessário informar ao menos um valor no grid para poder gerar o MDF-e, verifique!",
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.ERROR,
                    minWidth: 200,
                    fn: showResult
                });
            }
        }

        function MostrarMensagemWindow(mensagens) {
            var textarea = new Ext.form.TextArea({
                xtype: 'textarea',
                fieldLabel: 'Foram executados os seguintes MDF-e',
                value: mensagens,
                readOnly: true,
                height: 220,
                width: 395
            });

            var formErros = new Ext.FormPanel({
                id: 'formMensagem',
                bodyStyle: 'padding: 15px',
                labelAlign: 'top',
                items: [textarea],
                buttonAlign: 'center',
                buttons: [{
                    text: 'Fechar',
                    type: 'button',
                    handler: function (b, e) {
                        windowErros.close();
                        Pesquisar();
                    }
                }]
            });

            windowErros = new Ext.Window({
                id: 'windowMensagem',
                title: 'Mensagem',
                modal: true,
                width: 444,
                height: 350,
                items: [formErros]
            });
            windowErros.show();
        }

        $(function () {
            // Ext.getCmp("filtro-data-inicial").focus();
        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div id="header-content">
        <h1>CTe Virtual sem MDF-e Vínculado</h1>
        <small>Você está em CTe > Sem Vínculo com MDF-e</small>
        <br />
    </div>
</asp:Content>