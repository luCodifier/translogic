﻿<%@ Page Title="" Language="C#" Inherits="System.Web.Mvc.ViewPage" MasterPageFile="~/Views/Shared/Interna.master" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server">
    <style>
        .campoCorrigido
        {
            background-color: #3CB371;
        }
        
        .normal
        {
            background-color: white;
        }
        
        .campoInconforme
        {
            background-color: #FA8072;
        }
    </style>
    <%
        var codigoFluxo = (string)ViewData["CODIGO_FLUXO"];
        var vagaoCodOrig = (string)ViewData["VAGAO_COD"];
        var cteId = (int)ViewData["ID_CTE_SELECIONADO"];
        var container = (string)ViewData["CONTAINER"];
        var serieCte = (string)ViewData["SERIE_CTE"];
        var numeroCte = (string)ViewData["NUMERO_CTE"];
        var dataEmissao = (string)ViewData["DATA_EMISSAO_CTE"];
        var chaveCte = (string)ViewData["CHAVE_CTE"];
        var indFluxoConteiner = (bool)ViewData["IND_FLUXO_CONTEINER"];

        var empresa_remetente = (string)ViewData["EMPRESA_REMETENTE"];
        var endereco_remetente = (string)ViewData["ENDERECO_REMETENTE"];
        var cep_remetente = (string)ViewData["CEP_REMETENTE"];
        var municipio_remetente = (string)ViewData["MUNICIPIO_REMETENTE"];
        var cnpj_remetente = (string)ViewData["CJPN_REMETENTE"];
        var uf_remetente = (string)ViewData["UF_REMETENTE"];
        var pais_remetente = (string)ViewData["PAIS_REMETENTE"];
        var insc_est_remetente = (string)ViewData["INSC_EST_REMETENTE"];
        var telefone_remetente = (string)ViewData["TELEFONE_REMETENTE"];

        var empresa_destinatario = (string)ViewData["EMPRESA_DESTINATARIO"];
        var endereco_destinatario = (string)ViewData["ENDERECO_DESTINATARIO"];
        var cep_destinatario = (string)ViewData["CEP_DESTINATARIO"];
        var municipio_destinatario = (string)ViewData["MUNICIPIO_DESTINATARIO"];
        var uf_destinatario = (string)ViewData["UF_DESTINATARIO"];
        var cnpj_destinatario = (string)ViewData["CJPN_DESTINATARIO"];
        var pais_destinatario = (string)ViewData["PAIS_DESTINATARIO"];
        var insc_est_destinatario = (string)ViewData["INSC_EST_DESTINATARIO"];
        var telefone_destinatario = (string)ViewData["TELEFONE_DESTINATARIO"];

        var empresa_expedidor = (string)ViewData["EMPRESA_EXPEDIDOR"];
        var endereco_expedidor = (string)ViewData["ENDERECO_EXPEDIDOR"];
        var cep_expedidor = (string)ViewData["CEP_EXPEDIDOR"];
        var municipio_expedidor = (string)ViewData["MUNICIPIO_EXPEDIDOR"];
        var uf_expedidor = (string)ViewData["UF_EXPEDIDOR"];
        var cnpj_expedidor = (string)ViewData["CJPN_EXPEDIDOR"];
        var pais_expedidor = (string)ViewData["PAIS_EXPEDIDOR"];
        var insc_est_expedidor = (string)ViewData["INSC_EST_EXPEDIDOR"];
        var telefone_expedidor = (string)ViewData["TELEFONE_EXPEDIDOR"];

        var empresa_recebedor = (string)ViewData["EMPRESA_RECEBEDOR"];
        var endereco_recebedor = (string)ViewData["ENDERECO_RECEBEDOR"];
        var cep_recebedor = (string)ViewData["CEP_RECEBEDOR"];
        var municipio_recebedor = (string)ViewData["MUNICIPIO_RECEBEDOR"];
        var uf_recebedor = (string)ViewData["UF_RECEBEDOR"];
        var cnpj_recebedor = (string)ViewData["CJPN_RECEBEDOR"];
        var pais_recebedor = (string)ViewData["PAIS_RECEBEDOR"];
        var insc_est_recebedor = (string)ViewData["INSC_EST_RECEBEDOR"];
        var telefone_recebedor = (string)ViewData["TELEFONE_RECEBEDOR"];

        var empresa_tomador = (string)ViewData["EMPRESA_TOMADORA"];
        var endereco_tomador = (string)ViewData["ENDERECO_TOMADORA"];
        var cep_tomador = (string)ViewData["CEP_TOMADORA"];
        var municipio_tomador = (string)ViewData["MUNICIPIO_TOMADORA"];
        var uf_tomador = (string)ViewData["UF_TOMADORA"];
        var cnpj_tomador = (string)ViewData["CJPN_TOMADORA"];
        var pais_tomador = (string)ViewData["PAIS_TOMADORA"];
        var insc_est_tomador = (string)ViewData["INSC_EST_TOMADORA"];
        var telefone_tomador = (string)ViewData["TELEFONE_TOMADORA"];

        var baseCalculo = (string)ViewData["BASE_CALCULO"];
        var aliqIcms = (string)ViewData["ALIQ_ICMS"];
        var valorIcms = (string)ViewData["VALOR_ICMS"];
        var desconto = (string)ViewData["DESCONTO"];
        var valorTotal = (string)ViewData["VALOR_TOTAL"];
        var valorReceber = (string)ViewData["VALOR_RECEBER"];
        var observacao = (string)ViewData["CTE_OBSERVACAO"];
        var cfop = (string)ViewData["CFOP"];
        var descricaoCfop = (string)ViewData["CFOP_DESCRICAO"];

        var alteracaoCfop = (bool)ViewData["ALTERACAO_CFOP"];
        var alteracaoExpedidor = (bool?)ViewData["ALTERACAO_EXPEDIDOR"] ?? false;
        var alteracaoRecebedor = (bool?)ViewData["ALTERACAO_RECEBEDOR"] ?? false;
        var validacaoAlteracaoCfop = (bool?)ViewData["VALIDACAO_ALTERACAO_CFOP"] ?? false;
        var validacaoAlteracaoExpedidor = (bool?)ViewData["VALIDACAO_ALTERACAO_EXPEDIDOR"] ?? false;
        var validacaoAlteracaoRecebedor = (bool?)ViewData["VALIDACAO_ALTERACAO_RECEBEDOR"] ?? false;
        var alteracaoRemetente = (bool?)ViewData["ALTERACAO_REMETENTE"] ?? false;
        var alteracaoDestinatario = (bool?)ViewData["ALTERACAO_DESTINATARIO"] ?? false;
        var alteracaoTomador = (bool?)ViewData["ALTERACAO_TOMADOR"] ?? false; 
    %>
    <script type="text/javascript" language="javascript">

        var cteId = "<%= cteId %>";
        var vagaoCodigoOrig = "<%= vagaoCodOrig %>";
        var codigoFluxoOrig = "<%= codigoFluxo %>";
        var vagaoCodigo = "<%= vagaoCodOrig %>";
        var codigoFluxo = "<%= codigoFluxo %>";
        var observacao = "<%= observacao %>";
        var observacaoOrig = "<%= observacao %>";
        var cfopOriginal = "<%= cfop %>";
        var descricaoCfopOriginal = "<%= descricaoCfop %>";
        var indFluxoConteiner = <%= indFluxoConteiner ? "true" : "false" %>;

        var alteracaoCfop = "<%= alteracaoCfop.ToString().ToLower() %>";
        var alteracaoExpedidor = "<%= alteracaoExpedidor.ToString().ToLower() %>";
        var alteracaoRecebedor = "<%= alteracaoRecebedor.ToString().ToLower() %>";
        var alteracaoRemetente = "<%= alteracaoRemetente.ToString().ToLower() %>";
        var alteracaoDestinatario = "<%= alteracaoDestinatario.ToString().ToLower() %>";
        var alteracaoTomador = "<%= alteracaoTomador.ToString().ToLower() %>";
        var validacaoAlteracaoCfop = "<%= validacaoAlteracaoCfop.ToString().ToLower() %>";
        var validacaoAlteracaoExpedidor = "<%= validacaoAlteracaoExpedidor.ToString().ToLower() %>";
        var validacaoAlteracaoRecebedor = "<%= validacaoAlteracaoRecebedor.ToString().ToLower() %>";

        var alteracaoFluxoComercial = alteracaoCfop == "true" || alteracaoExpedidor == "true" || alteracaoRecebedor == "true";
        var container = "<%= container %>";
        var serieCte = "<%= serieCte %>";
        var numeroCte = "<%= numeroCte %>";
        var dataEmissao = "<%= dataEmissao %>";
        var chaveCte = "<%= chaveCte %>";

        var empresa_remetente = "<%= empresa_remetente %>";
        var endereco_remetente = "<%= endereco_remetente %>";
        var cep_remetente = "<%= cep_remetente %>";
        var municipio_remetente = "<%= municipio_remetente %>";
        var cnpj_remetente = "<%= cnpj_remetente %>";
        var uf_remetente = "<%= uf_remetente %>";
        var pais_remetente = "<%= pais_remetente %>";
        var insc_est_remetente = "<%= insc_est_remetente %>";
        var telefone_remetente = "<%= telefone_remetente %>";

        var empresa_destinatario = "<%= empresa_destinatario %>";
        var endereco_destinatario = "<%= endereco_destinatario %>";
        var cep_destinatario = "<%= cep_destinatario %>";
        var municipio_destinatario = "<%= municipio_destinatario %>";
        var uf_destinatario = "<%= uf_destinatario %>";
        var cnpj_destinatario = "<%= cnpj_destinatario %>";
        var pais_destinatario = "<%= pais_destinatario %>";
        var insc_est_destinatario = "<%= insc_est_destinatario %>";
        var telefone_destinatario = "<%= telefone_destinatario %>";

        var empresa_expedidor = "<%= empresa_expedidor %>";
        var endereco_expedidor = "<%= endereco_expedidor %>";
        var cep_expedidor = "<%= cep_expedidor %>";
        var municipio_expedidor = "<%= municipio_expedidor %>";
        var uf_expedidor = "<%= uf_expedidor %>";
        var cnpj_expedidor = "<%= cnpj_expedidor %>";
        var pais_expedidor = "<%= pais_expedidor %>";
        var insc_est_expedidor = "<%= insc_est_expedidor %>";
        var telefone_expedidor = "<%= telefone_expedidor %>";

        var empresa_recebedor = "<%= empresa_recebedor %>";
        var endereco_recebedor = "<%= endereco_recebedor %>";
        var cep_recebedor = "<%= cep_recebedor %>";
        var municipio_recebedor = "<%= municipio_recebedor %>";
        var uf_recebedor = "<%= uf_recebedor %>";
        var cnpj_recebedor = "<%= cnpj_recebedor %>";
        var pais_recebedor = "<%= pais_recebedor %>";
        var insc_est_recebedor = "<%= insc_est_recebedor %>";
        var telefone_recebedor = "<%= telefone_recebedor %>";

        var empresa_tomador = "<%= empresa_tomador %>";
        var endereco_tomador = "<%= endereco_tomador %>";
        var cep_tomador = "<%= cep_tomador %>";
        var municipio_tomador = "<%= municipio_tomador %>";
        var uf_tomador = "<%= uf_tomador %>";
        var cnpj_tomador = "<%= cnpj_tomador %>";
        var pais_tomador = "<%= pais_tomador %>";
        var insc_est_tomador = "<%= insc_est_tomador %>";
        var telefone_tomador = "<%= telefone_tomador %>";

        var baseCalculo = "<%= baseCalculo %>";
        var aliqIcms = "<%= aliqIcms %>";
        var valorIcms = "<%= valorIcms %>";
        var desconto = "<%= desconto %>";
        var valorTotal = "<%= valorTotal %>";
        var valorReceber = "<%= valorReceber %>";

        var arrDetalhesFormulario = new Array();
        var fluxoAlterado = false;
        var CodfluxoAlterado = '';

        function VoltarTela() {
            if (Ext.Msg.confirm("Informação", "Você deseja realmente sair?", function(btn, text) {
                if (btn == 'yes') {
                    var url = '<%= Url.Action("Index") %>';
                    document.location.href = url;
                }
            })) ;
        }

        function VerificarMudancaVagao(codVagao, campo) {
            if (codVagao != "" && codVagao != vagaoCodigo) {
                $.ajax({
                    url: "<%= Url.Action("VerificarMudancaVagao") %>",
                    type: "POST",
                    beforeSend: function() { Ext.getCmp("formCorrecaoCte").getEl().mask("Obtendo dados do vagão.", "x-mask-loading"); },
                    async: true,
                    dataType: 'json',
                    data: ({ codigoVagao: codVagao }),
                    success: function(result) {
                        Ext.getCmp("formCorrecaoCte").getEl().unmask();

                        if (result.Erro) {
                            Ext.Msg.alert('Mensagem de Erro', result.Mensagem);
                            campo.markInvalid(result.Mensagem);
                            campo.setValue(vagaoCodigoOrig);
                        } else {
                            vagaoId = result.IdVagao;
                            vagaoCodigo = codVagao;
                            Ext.getCmp('btnSalvar').enable();
                        }
                    },
                    failure: function(result) {
                        Ext.Msg.alert('Erro ao validar alteração do vagão.', result.Message);
                        Ext.getCmp("formCorrecaoCte").getEl().unmask();
                    }
                });
            }
        }
        
           function afterEditConteiner(e) {
            /*
            e.grid - This grid
            e.record - The record being edited
            e.field - The field name being edited
            e.value - The value being set
            e.originalValue - The original value for the field, before the edit.
            e.row - The grid row index
            e.column - The grid column index
            */
            // execute an XHR to send/commit data to the server, in callback do (if successful):
            // alert(e.value);
            if (e.field == "Conteiner"){
                e.grid.getEl().mask();

                var validacao = VerificarMudancaConteiner(e.value);
                
                if (validacao){
                    e.grid.getEl().unmask();
                   }else{
                      e.grid.getEl().unmask();
                      e.cancel = true;
                }
            }
        }

        function VerificarMudancaConteiner(codigoConteiner) {
           
             if (codigoConteiner != ""){
                var responseText = $.ajax({
				    url: "<%= Url.Action("VerificarMudancaConteiner") %>",
				    type: "POST",
				    async: false,
                    dataType: 'json',
                    data: ({codigoConteiner: codigoConteiner})
			    }).responseText;

                var result = Ext.util.JSON.decode(responseText);
               
                if (result.Erro){
                    Ext.Msg.alert('Mensagem de Erro', result.Mensagem);
                }
                return !result.Erro;
                
            }
        }
        
        function RendererFloat(val) {
            return Ext.util.Format.number(val, '0.000,000/i');
        }

        function VerificarMudancaFluxoComercial(codigoFluxoComercial, campo) {

            if (codigoFluxoComercial == CodfluxoAlterado || codigoFluxoComercial == "" || (codigoFluxoComercial == codigoFluxoOrig && !fluxoAlterado)) {
                return false;
            }

            $.ajax({
                url: "<%= Url.Action("ValidarAlteracaoDoFluxoComercial") %>",
                type: "POST",
                beforeSend: function() { Ext.getCmp("formCorrecaoCte").getEl().mask("Obtendo dados do fluxo comercial.", "x-mask-loading"); },
                async: true,
                dataType: 'json',
                data: ({ codFluxoComercial: codigoFluxoOrig, codFluxoComercialCorrigido: codigoFluxoComercial, idCte: cteId }),
                success: function(result) {
                    Ext.getCmp("formCorrecaoCte").getEl().unmask();

                    if (result.Erro) {
                        Ext.Msg.alert('Mensagem de Erro', result.Mensagem);
                        campo.markInvalid(result.Mensagem);
                        campo.setValue(codigoFluxoOrig);
                        $("#fsExpedidor").css('background-color', 'white');
                        $("#fsRecebedor").css('background-color', 'white');
                        return false;
                    } else {
                        if (codigoFluxoComercial != codigoFluxoOrig) {
                            fluxoAlterado = true;
                            CodfluxoAlterado = codigoFluxoComercial;
                        } else if (codigoFluxoComercial == codigoFluxoOrig && fluxoAlterado) {
                            fluxoAlterado = false;
                            CodfluxoAlterado = '';
                        }

                        var cfop = result.Cfop;
                        var descricaoCfop = result.DescricaoCfop;
                        Ext.getCmp("lblCFOP").setValue(descricaoCfop);

                        if (cfopOriginal != cfop) {
                            $("#lblCFOP").css('background-color', '#3CB371');
                        } else {
                            $("#lblCFOP").css('background-color', 'white');
                        }

                        empresa_expedidor = result.RazaoSocialExpedidor;
                        cnpj_expedidor = result.CnpjExpedidor;
                        insc_est_expedidor = result.InscEstExpedidor;
                        uf_expedidor = result.UfExpedidor;
                        endereco_expedidor = result.EnderecoExpedidor;
                        municipio_expedidor = result.MunicipioExpedidor;
                        cep_expedidor = result.CepExpedidor;
                        pais_expedidor = result.PaisExpedidor;

                        empresa_recebedor = result.RazaoSocialRecebedor;
                        cnpj_recebedor = result.CnpjRecebedor;
                        insc_est_recebedor = result.InscEstRecebedor;
                        uf_recebedor = result.UfRecebedor;
                        endereco_recebedor = result.EnderecoRecebedor;
                        municipio_recebedor = result.MunicipioRecebedor;
                        cep_recebedor = result.CepRecebedor;
                        pais_recebedor = result.PaisRecebedor;

                        var alteracaoRec = result.AlteracaoRecebedor;
                        var alteracaoExp = result.AlteracaoExpedidor;

                        Ext.getCmp('btnSalvar').enable();

                        // Atribui as informações do expedidor
                        Ext.getCmp("lblExpedidor").setValue(empresa_expedidor);
                        Ext.getCmp("lblCPF_CNPJ_Expedidor").setValue(cnpj_expedidor);
                        Ext.getCmp("lblInscEstExpedidor").setValue(insc_est_expedidor);
                        Ext.getCmp("lblUFExpedidor").setValue(uf_expedidor);
                        Ext.getCmp("lblEnderecoExpedidor").setValue(endereco_expedidor);
                        Ext.getCmp("lblMunicipioExpedidor").setValue(municipio_expedidor);
                        Ext.getCmp("lblCEPExpedidor").setValue(cep_expedidor);
                        Ext.getCmp("lblPaisExpedidor").setValue(pais_expedidor);

                        // Atribui as informações recebedor
                        Ext.getCmp("lblRecebedor").setValue(empresa_recebedor);
                        Ext.getCmp("lblCPF_CNPJ_Recebedor").setValue(cnpj_recebedor);
                        Ext.getCmp("lblInscEstRecebedor").setValue(insc_est_recebedor);
                        Ext.getCmp("lblUFRecebedor").setValue(uf_recebedor);
                        Ext.getCmp("lblEnderecoRecebedor").setValue(endereco_recebedor);
                        Ext.getCmp("lblMunicipioRecebedor").setValue(municipio_recebedor);
                        Ext.getCmp("lblCEPRecebedor").setValue(cep_recebedor);
                        Ext.getCmp("lblPaisRecebedor").setValue(pais_recebedor);

                        if (alteracaoExp) {
                            $("#fsExpedidor").css('background-color', '#3CB371');
                        } else {
                            $("#fsExpedidor").css('background-color', 'white');
                        }

                        if (alteracaoRec) {
                            $("#fsRecebedor").css('background-color', '#3CB371');
                        } else {
                            $("#fsRecebedor").css('background-color', 'white');
                        }
                    }
                },
                failure: function(result) {
                    Ext.Msg.alert('Erro ao validar alteração do fluxo comercial.', result.Message);
                    Ext.getCmp("formCorrecaoCte").getEl().unmask();
                }
            });
        }

        function salvarCorrecoesCte(btn) {

            if (btn == 'no') {
                return false;
            }

            var campoFluxoComercial = Ext.getCmp("fluxoCorrecao").getValue();
            var campoVagao = Ext.getCmp("vagaoCorrecao").getValue();
            var campoObservacao = Ext.getCmp("txtObservacao").getValue();

            var fluxoComercial = ((codigoFluxoOrig != campoFluxoComercial || alteracaoFluxoComercial) ? campoFluxoComercial : "");
            var vagao = ((campoVagao != vagaoCodigoOrig) ? campoVagao : "");
            var observacao = (($.trim(campoObservacao) != observacaoOrig) ? campoObservacao : "");

            var correcaoCte = fluxoComercial != '' || vagao != '' || observacao != '';
            var conteinerAlterado = false;
            
            var listaCorrecaoConteiner = new Array();
            dsNotasVagoes.each(
			    function (record) {
			        var conteinerNfe = record.data.CodConteiner;
			        var conteiner = record.data.Conteiner;
			        var chaveNfe = record.data.ChaveNfe;
			        
                    if(conteinerNfe != conteiner) {
                        var correcaoConteiner = {};
			            correcaoConteiner.ConteinerNfe = conteinerNfe;
                        correcaoConteiner.ConteinerCorrigido = conteiner;
                        correcaoConteiner.ChaveNfe = chaveNfe;
			            listaCorrecaoConteiner.push(correcaoConteiner);
                    }
			    }
		    );

            if(listaCorrecaoConteiner.length > 0) {
                conteinerAlterado = true;
            }
            
            if (correcaoCte == false && conteinerAlterado == false) {
                Ext.Msg.alert('Informação', "Nenhuma informação foi corrigida");
                return false;
            }
           
            var cteTemp = {
                CteId: parseInt(cteId),
                CodigoVagao: vagao,
                CodigoVagaoOrig: vagaoCodigoOrig,
                CodFluxo: fluxoComercial,
                Conteiner: '',
                Observacao: observacao,
                CorrecaoCte: correcaoCte,
                CorrecaoConteinerCte: conteinerAlterado,
                CorrecaoConteiner: listaCorrecaoConteiner
            };
            
            var cteDto = $.toJSON(cteTemp);

            $.ajax({
                url: "<%= Url.Action("SalvarCorrecaoCte") %>",
                type: "POST",
                beforeSend: function() { Ext.getCmp("formCorrecaoCte").getEl().mask("Salvando os dados.", "x-mask-loading"); },
                dataType: 'json',
                data: cteDto,
                timeout: 120000,
                contentType: "application/json; charset=utf-8",
                success: function(result) {
                    Ext.getCmp("formCorrecaoCte").getEl().unmask();

                    if (!result.Erro) {
                        Ext.Msg.show({
                            title: 'Mensagem de Informação',
                            msg: 'Dados alterados com sucesso.',
                            buttons: Ext.Msg.OK,
                            fn: function() {
                                document.location.href = '<%= Url.Action("Index") %>';
                            }
                        });
                    } else {
                        Ext.Msg.alert('Mensagem de Erro', result.Mensagem);
                    }
                },
                error: function(result) {
                    alert("Ocorreu um erro inesperado;");
                    Ext.getCmp("formCorrecaoCte").getEl().unmask();
                }
            });
        }

        var txtFluxo = {
            xtype: 'textfield',
            vtype: 'ctefluxovtype',
            id: 'fluxoCorrecao',
            fieldLabel: 'Fluxo',
            name: 'correcao.Fluxo',
            allowBlank: false,
            maxLength: 20,
            width: 80,
            value: codigoFluxo,
            style: 'text-transform: uppercase',
            autoCreate: { tag: 'input', type: 'text', maxlength: '5', autocomplete: 'off' },
            listeners:
                {
                    'blur': function(campo) {
                        var value = campo.getValue();
                        var newValue = "";

                        for (var i = 0; i < value.length; i++) {
                            if (!isNaN(value.charAt(i))) {
                                newValue += value.charAt(i);
                            }
                        }
                        campo.setValue(newValue);

                        VerificarMudancaFluxoComercial(campo.getValue(), campo);
                    }
                }
        };

        var txtVagao = {
            xtype: 'textfield',
            vtype: 'ctevagaovtype',
            id: 'vagaoCorrecao',
            fieldLabel: 'Vagão',
            name: 'correcao.Vagao',
            autoCreate: { tag: 'input', type: 'text', maxlength: '7', autocomplete: 'off' },
            maxLength: 20,
            width: 80,
            value: vagaoCodigo,
            listeners: {
                'blur': function(field) {
                    VerificarMudancaVagao(field.getValue(), field);
                }
            }
        };

        var linha1_coluna1 = { width: 100, layout: 'form', border: false, items: [txtFluxo] };
        var linha1_coluna2 = { width: 100, layout: 'form', border: false, items: [txtVagao] };

        var linha1 = {
            layout: 'column',
            border: false,
            items: [linha1_coluna1, linha1_coluna2]
        };

        arrDetalhesFormulario.push(linha1);

        var txtObservacao = new Ext.form.TextArea({
            xtype: 'textarea',
            fieldLabel: 'Observação',
            id: 'txtObservacao',
            name: 'txtObservacao',
            height: 100,
            width: 480,
            value: observacao,
            readOnly: observacao == '',
            cls: observacao == '' ? 'x-item-disabled' : 'none'
        });

        var fsObservacao = {
            xtype: 'fieldset',
            id: 'fsObservacao',
            name: 'fsObservacao',
            height: 160,
            bodyStyle: 'padding: 2px',
            width: 530,
            title: 'Observação',
            items: [txtObservacao]
        };

        var linha2_coluna1 = { width: 540, layout: 'form', border: false, items: [fsObservacao] };

        var lblSerie = {
            xtype: 'textfield',
            fieldLabel: 'Série',
            id: 'lblSerie',
            name: 'lblSerie',
            value: serieCte,
            cls: 'x-item-disabled',
            width: 30,
            readOnly: true
        };

        var lblNumeroCte = {
            xtype: 'textfield',
            fieldLabel: 'Número',
            id: 'lblNumeroCte',
            name: 'lblNumeroCte',
            value: numeroCte,
            cls: 'x-item-disabled',
            width: 80,
            readOnly: true
        };

        var lblDataEmissao = {
            xtype: 'textfield',
            fieldLabel: 'Data de Emissão',
            id: 'lblDataEmissao',
            name: 'lblDataEmissao',
            value: dataEmissao,
            cls: 'x-item-disabled',
            width: 100,
            readOnly: true
        };

        var clsCfop = alteracaoCfop == "true" ? 'x-item-disabled campoCorrigido' : 'x-item-disabled';

        if (validacaoAlteracaoCfop == "false") {
            clsCfop = 'campoInconforme';
        }

        var lblCFOP = {
            xtype: 'textfield',
            fieldLabel: 'CFOP',
            id: 'lblCFOP',
            name: 'lblCFOP',
            value: descricaoCfopOriginal,
            cls: clsCfop,
            width: 235,
            readOnly: true
        };

        var lblChaveCte = {
            xtype: 'textfield',
            fieldLabel: 'Chave',
            id: 'lblChaveCte',
            name: 'lblChaveCte',
            value: chaveCte,
            cls: 'x-item-disabled',
            width: 485,
            readOnly: true
        };

        var linha1_coluna1FsCte = { width: 50, bodyStyle: 'margin-left: 10px', layout: 'form', border: false, items: [lblSerie] };
        var linha1_coluna2FsCte = { width: 90, layout: 'form', border: false, items: [lblNumeroCte] };
        var linha1_coluna3FsCte = { width: 110, layout: 'form', border: false, items: [lblDataEmissao] };
        var linha1_coluna4FsCte = { width: 240, layout: 'form', border: false, items: [lblCFOP] };
        var linha2_coluna1FsCte = { width: 500, bodyStyle: 'margin-left: 10px', layout: 'form', border: false, items: [lblChaveCte] };

        var linha1fsCte = {
            layout: 'column',
            border: false,
            items: [linha1_coluna1FsCte, linha1_coluna2FsCte, linha1_coluna3FsCte, linha1_coluna4FsCte]
        };

        var linha2fsCte = {
            layout: 'column',
            border: false,
            items: [linha2_coluna1FsCte]
        };

        var fsCte = {
            xtype: 'fieldset',
            id: 'fsCte',
            name: 'fsCte',
            bodyStyle: 'padding: 10px',
            height: 160,
            width: 540,
            title: 'CT-e',
            items: [linha1fsCte, linha2fsCte]
        };

        var linha2_coluna2 = { width: 540, layout: 'form', border: false, items: [fsCte] };

        var linha2 = {
            layout: 'column',
            border: false,
            items: [linha2_coluna1, linha2_coluna2]
        };

        arrDetalhesFormulario.push(linha2);

        var lblRemetente = {
            xtype: 'textfield',
            fieldLabel: 'Remetente',
            id: 'lblRemetente',
            name: 'lblRemetente',
            value: empresa_remetente,
            cls: 'x-item-disabled',
            readOnly: true,
            width: 210
        };

        var lblEnderecoRemetente = {
            xtype: 'textfield',
            fieldLabel: 'Endereço',
            id: 'lblEnderecoRemetente',
            name: 'lblEnderecoRemetente',
            value: endereco_remetente,
            cls: 'x-item-disabled',
            readOnly: true,
            width: 210
        };

        var lblMunicipioRemetente = {
            xtype: 'textfield',
            fieldLabel: 'Município',
            id: 'lblMunicipioRemetente',
            name: 'lblMunicipioRemetente',
            value: municipio_remetente,
            cls: 'x-item-disabled',
            readOnly: true,
            width: 110
        };

        var lblCEPRemetente = {
            xtype: 'textfield',
            fieldLabel: 'CEP',
            id: 'lblCEPRemetente',
            name: 'lblCEPRemetente',
            value: cep_remetente,
            cls: 'x-item-disabled',
            width: 80,
            readOnly: true
        };

        var lblCPF_CNPJ_Remetente = {
            xtype: 'textfield',
            fieldLabel: 'CPF/CNPJ',
            id: 'lblCPF_CNPJ_Remetente',
            name: 'lblCPF_CNPJ_Remetente',
            value: cnpj_remetente,
            cls: 'x-item-disabled',
            width: 110,
            readOnly: true
        };

        var lblInscEstRemetente = {
            xtype: 'textfield',
            fieldLabel: 'Insc. Est',
            id: 'lblInscEstRemetente',
            name: 'lblInscEstRemetente',
            value: insc_est_remetente,
            cls: 'x-item-disabled',
            width: 80,
            readOnly: true
        };

        var lblUFRemetente = {
            xtype: 'textfield',
            fieldLabel: 'UF',
            id: 'lblUFRemetente',
            name: 'lblUFRemetente',
            value: uf_remetente,
            cls: 'x-item-disabled',
            width: 30,
            readOnly: true
        };

        var lblPaisRemetente = {
            xtype: 'textfield',
            fieldLabel: 'País',
            id: 'lblPaisRemetente',
            name: 'lblPaisRemetente',
            value: pais_remetente,
            cls: 'x-item-disabled',
            width: 30,
            readOnly: true
        };

        var linha1_coluna1FsRemetente = { width: 220, layout: 'form', border: false, items: [lblRemetente] };
        var linha1_coluna2FsRemetente = { width: 120, layout: 'form', border: false, items: [lblCPF_CNPJ_Remetente] };
        var linha1_coluna3FsRemetente = { width: 90, layout: 'form', border: false, items: [lblInscEstRemetente] };
        var linha1_coluna4FsRemetente = { width: 35, layout: 'form', border: false, items: [lblUFRemetente] };

        var linha2_coluna1FsRemetente = { width: 220, layout: 'form', border: false, items: [lblEnderecoRemetente] };
        var linha2_coluna2FsRemetente = { width: 120, layout: 'form', border: false, items: [lblMunicipioRemetente] };
        var linha2_coluna3FsRemetente = { width: 90, layout: 'form', border: false, items: [lblCEPRemetente] };
        var linha2_coluna4FsRemetente = { width: 35, layout: 'form', border: false, items: [lblPaisRemetente] };

        var linha1fsRemetente = {
            layout: 'column',
            border: false,
            items: [linha1_coluna1FsRemetente, linha1_coluna2FsRemetente, linha1_coluna3FsRemetente, linha1_coluna4FsRemetente]
        };

        var linha2fsRemetente = {
            layout: 'column',
            border: false,
            items: [linha2_coluna1FsRemetente, linha2_coluna2FsRemetente, linha2_coluna3FsRemetente, linha2_coluna4FsRemetente]
        };

        var clsRemetente = 'none';

        if (alteracaoRemetente == "true") {
            clsRemetente = 'campoInconforme';
        }

        var fsRemetente = {
            xtype: 'fieldset',
            id: 'fsRemetente',
            name: 'fsRemetente',
            autoHeight: true,
            bodyStyle: 'padding: 10px',
            width: 520,
            title: 'Remetente',
            cls: clsRemetente,
            items: [linha1fsRemetente, linha2fsRemetente]
        };

        var lblDestinatario = {
            xtype: 'textfield',
            fieldLabel: 'Destinatário',
            id: 'lblDestinatario',
            name: 'lblDestinatario',
            value: empresa_destinatario,
            cls: 'x-item-disabled',
            readOnly: true,
            width: 210
        };


        var lblEnderecoDestinatario = {
            xtype: 'textfield',
            fieldLabel: 'Endereço',
            id: 'lblEnderecoDestinatario',
            name: 'lblEnderecoDestinatario',
            value: endereco_destinatario,
            cls: 'x-item-disabled',
            readOnly: true,
            width: 210
        };

        var lblMunicipioDestinatario = {
            xtype: 'textfield',
            fieldLabel: 'Município',
            id: 'lblMunicipioDestinatario',
            name: 'lblMunicipioDestinatario',
            value: municipio_destinatario,
            cls: 'x-item-disabled',
            readOnly: true,
            width: 110
        };

        var lblCEPDestinatario = {
            xtype: 'textfield',
            fieldLabel: 'CEP',
            id: 'lblCEPDestinatario',
            name: 'lblCEPDestinatario',
            value: cep_destinatario,
            cls: 'x-item-disabled',
            width: 80,
            readOnly: true
        };

        var lblCPF_CNPJ_Destinatario = {
            xtype: 'textfield',
            fieldLabel: 'CPF/CNPJ',
            id: 'lblCPF_CNPJ_Destinatario',
            name: 'lblCPF_CNPJ_Destinatario',
            value: cnpj_destinatario,
            cls: 'x-item-disabled',
            width: 110,
            readOnly: true
        };

        var lblInscEstDestinatario = {
            xtype: 'textfield',
            fieldLabel: 'Insc. Est',
            id: 'lblInscEstDestinatario',
            name: 'lblInscEstDestinatario',
            value: insc_est_destinatario,
            cls: 'x-item-disabled',
            width: 80,
            readOnly: true
        };

        var lblUFDestinatario = {
            xtype: 'textfield',
            fieldLabel: 'UF',
            id: 'lblUFDestinatario',
            name: 'lblUFDestinatario',
            value: uf_destinatario,
            cls: 'x-item-disabled',
            width: 30,
            readOnly: true
        };

        var lblPaisDestinatario = {
            xtype: 'textfield',
            fieldLabel: 'País',
            id: 'lblPaisDestinatario',
            name: 'lblPaisDestinatario',
            value: pais_destinatario,
            cls: 'x-item-disabled',
            width: 30,
            readOnly: true
        };

        var linha1_coluna1fsDestinatario = { width: 220, layout: 'form', border: false, items: [lblDestinatario] };
        var linha1_coluna2fsDestinatario = { width: 120, layout: 'form', border: false, items: [lblCPF_CNPJ_Destinatario] };
        var linha1_coluna3fsDestinatario = { width: 90, layout: 'form', border: false, items: [lblInscEstDestinatario] };
        var linha1_coluna4fsDestinatario = { width: 35, layout: 'form', border: false, items: [lblUFDestinatario] };

        var linha2_coluna1fsDestinatario = { width: 220, layout: 'form', border: false, items: [lblEnderecoDestinatario] };
        var linha2_coluna2fsDestinatario = { width: 120, layout: 'form', border: false, items: [lblMunicipioDestinatario] };
        var linha2_coluna3fsDestinatario = { width: 90, layout: 'form', border: false, items: [lblCEPDestinatario] };
        var linha2_coluna4fsDestinatario = { width: 35, layout: 'form', border: false, items: [lblPaisDestinatario] };

        var linha1fsDestinatario = {
            layout: 'column',
            border: false,
            items: [linha1_coluna1fsDestinatario, linha1_coluna2fsDestinatario, linha1_coluna3fsDestinatario, linha1_coluna4fsDestinatario]
        };

        var linha2fsDestinatario = {
            layout: 'column',
            border: false,
            items: [linha2_coluna1fsDestinatario, linha2_coluna2fsDestinatario, linha2_coluna3fsDestinatario, linha2_coluna4fsDestinatario]
        };

        var clsDestinatario = 'none';

        if (alteracaoDestinatario == "true") {
            clsDestinatario = 'campoInconforme';
        }

        var fsDestinatario = {
            xtype: 'fieldset',
            id: 'fsDestinatario',
            name: 'fsDestinatario',
            autoHeight: true,
            bodyStyle: 'padding: 10px',
            width: 520,
            title: 'Destinatário',
            cls: clsDestinatario,
            items: [linha1fsDestinatario, linha2fsDestinatario]
        };

        var linha1DadosDoFluxo_coluna1 = { width: 530, layout: 'form', border: false, items: [fsRemetente] };
        var linha1DadosDoFluxo_coluna2 = { width: 520, layout: 'form', border: false, items: [fsDestinatario] };

        var linha1DadosDoFluxo = {
            layout: 'column',
            border: false,
            items: [linha1DadosDoFluxo_coluna1, linha1DadosDoFluxo_coluna2]
        };

        var lblExpedidor = {
            xtype: 'textfield',
            fieldLabel: 'Expedidor',
            id: 'lblExpedidor',
            name: 'lblExpedidor',
            value: empresa_expedidor,
            cls: 'x-item-disabled',
            readOnly: true,
            width: 210
        };


        var lblEnderecoExpedidor = {
            xtype: 'textfield',
            fieldLabel: 'Endereço',
            id: 'lblEnderecoExpedidor',
            name: 'lblEnderecoExpedidor',
            value: endereco_expedidor,
            cls: 'x-item-disabled',
            readOnly: true,
            width: 210
        };

        var lblMunicipioExpedidor = {
            xtype: 'textfield',
            fieldLabel: 'Município',
            id: 'lblMunicipioExpedidor',
            name: 'lblMunicipioExpedidor',
            value: municipio_expedidor,
            cls: 'x-item-disabled',
            readOnly: true,
            width: 110
        };

        var lblCEPExpedidor = {
            xtype: 'textfield',
            fieldLabel: 'CEP',
            id: 'lblCEPExpedidor',
            name: 'lblCEPExpedidor',
            value: cep_expedidor,
            cls: 'x-item-disabled',
            width: 80,
            readOnly: true
        };

        var lblCPF_CNPJ_Expedidor = {
            xtype: 'textfield',
            fieldLabel: 'CPF/CNPJ',
            id: 'lblCPF_CNPJ_Expedidor',
            name: 'lblCPF_CNPJ_Expedidor',
            value: cnpj_expedidor,
            cls: 'x-item-disabled',
            width: 110,
            readOnly: true
        };

        var lblInscEstExpedidor = {
            xtype: 'textfield',
            fieldLabel: 'Insc. Est',
            id: 'lblInscEstExpedidor',
            name: 'lblInscEstExpedidor',
            value: insc_est_expedidor,
            cls: 'x-item-disabled',
            width: 80,
            readOnly: true
        };

        var lblUFExpedidor = {
            xtype: 'textfield',
            fieldLabel: 'UF',
            id: 'lblUFExpedidor',
            name: 'lblUFExpedidor',
            value: uf_expedidor,
            cls: 'x-item-disabled',
            width: 30,
            readOnly: true
        };

        var lblPaisExpedidor = {
            xtype: 'textfield',
            fieldLabel: 'País',
            id: 'lblPaisExpedidor',
            name: 'lblPaisExpedidor',
            value: pais_expedidor,
            cls: 'x-item-disabled',
            width: 30,
            readOnly: true
        };

        var linha1_coluna1fsExpedidor = { width: 220, layout: 'form', border: false, items: [lblExpedidor] };
        var linha1_coluna2fsExpedidor = { width: 120, layout: 'form', border: false, items: [lblCPF_CNPJ_Expedidor] };
        var linha1_coluna3fsExpedidor = { width: 90, layout: 'form', border: false, items: [lblInscEstExpedidor] };
        var linha1_coluna4fsExpedidor = { width: 35, layout: 'form', border: false, items: [lblUFExpedidor] };

        var linha2_coluna1fsExpedidor = { width: 220, layout: 'form', border: false, items: [lblEnderecoExpedidor] };
        var linha2_coluna2fsExpedidor = { width: 120, layout: 'form', border: false, items: [lblMunicipioExpedidor] };
        var linha2_coluna3fsExpedidor = { width: 90, layout: 'form', border: false, items: [lblCEPExpedidor] };
        var linha2_coluna4fsExpedidor = { width: 35, layout: 'form', border: false, items: [lblPaisExpedidor] };

        var linha1fsExpedidor = {
            layout: 'column',
            border: false,
            items: [linha1_coluna1fsExpedidor, linha1_coluna2fsExpedidor, linha1_coluna3fsExpedidor, linha1_coluna4fsExpedidor]
        };

        var linha2fsExpedidor = {
            layout: 'column',
            border: false,
            items: [linha2_coluna1fsExpedidor, linha2_coluna2fsExpedidor, linha2_coluna3fsExpedidor, linha2_coluna4fsExpedidor]
        };

        var clsExpedidor = alteracaoExpedidor == "true" ? 'campoCorrigido' : 'none';

        if (validacaoAlteracaoExpedidor == "false") {
            clsExpedidor = 'campoInconforme';
        }

        var fsExpedidor = {
            xtype: 'fieldset',
            id: 'fsExpedidor',
            name: 'fsExpedidor',
            autoHeight: true,
            bodyStyle: 'padding: 10px',
            width: 520,
            title: 'Expedidor',
            cls: clsExpedidor,
            items: [linha1fsExpedidor, linha2fsExpedidor]
        };

        var lblRecebedor = {
            xtype: 'textfield',
            fieldLabel: 'Recebedor',
            id: 'lblRecebedor',
            name: 'lblRecebedor',
            value: empresa_recebedor,
            cls: 'x-item-disabled',
            readOnly: true,
            width: 210
        };

        var lblEnderecoRecebedor = {
            xtype: 'textfield',
            fieldLabel: 'Endereço',
            id: 'lblEnderecoRecebedor',
            name: 'lblEnderecoRecebedor',
            value: endereco_recebedor,
            cls: 'x-item-disabled',
            readOnly: true,
            width: 210
        };

        var lblMunicipioRecebedor = {
            xtype: 'textfield',
            fieldLabel: 'Município',
            id: 'lblMunicipioRecebedor',
            name: 'lblMunicipioRecebedor',
            value: municipio_recebedor,
            cls: 'x-item-disabled',
            readOnly: true,
            width: 110
        };

        var lblCEPRecebedor = {
            xtype: 'textfield',
            fieldLabel: 'CEP',
            id: 'lblCEPRecebedor',
            name: 'lblCEPRecebedor',
            value: cep_recebedor,
            cls: 'x-item-disabled',
            width: 80,
            readOnly: true
        };

        var lblCPF_CNPJ_Recebedor = {
            xtype: 'textfield',
            fieldLabel: 'CPF/CNPJ',
            id: 'lblCPF_CNPJ_Recebedor',
            name: 'lblCPF_CNPJ_Recebedor',
            value: cnpj_recebedor,
            cls: 'x-item-disabled',
            width: 110,
            readOnly: true
        };

        var lblInscEstRecebedor = {
            xtype: 'textfield',
            fieldLabel: 'Insc. Est',
            id: 'lblInscEstRecebedor',
            name: 'lblInscEstRecebedor',
            value: insc_est_recebedor,
            cls: 'x-item-disabled',
            width: 80,
            readOnly: true
        };

        var lblUFRecebedor = {
            xtype: 'textfield',
            fieldLabel: 'UF',
            id: 'lblUFRecebedor',
            name: 'lblUFRecebedor',
            value: uf_recebedor,
            cls: 'x-item-disabled',
            width: 30,
            readOnly: true
        };

        var lblPaisRecebedor = {
            xtype: 'textfield',
            fieldLabel: 'País',
            id: 'lblPaisRecebedor',
            name: 'lblPaisRecebedor',
            value: pais_recebedor,
            cls: 'x-item-disabled',
            width: 30,
            readOnly: true
        };

        var linha1_coluna1fsRecebedor = { width: 220, layout: 'form', border: false, items: [lblRecebedor] };
        var linha1_coluna2fsRecebedor = { width: 120, layout: 'form', border: false, items: [lblCPF_CNPJ_Recebedor] };
        var linha1_coluna3fsRecebedor = { width: 90, layout: 'form', border: false, items: [lblInscEstRecebedor] };
        var linha1_coluna4fsRecebedor = { width: 35, layout: 'form', border: false, items: [lblUFRecebedor] };

        var linha2_coluna1fsRecebedor = { width: 220, layout: 'form', border: false, items: [lblEnderecoRecebedor] };
        var linha2_coluna2fsRecebedor = { width: 120, layout: 'form', border: false, items: [lblMunicipioRecebedor] };
        var linha2_coluna3fsRecebedor = { width: 90, layout: 'form', border: false, items: [lblCEPRecebedor] };
        var linha2_coluna4fsRecebedor = { width: 35, layout: 'form', border: false, items: [lblPaisRecebedor] };

        var linha1fsRecebedor = {
            layout: 'column',
            border: false,
            items: [linha1_coluna1fsRecebedor, linha1_coluna2fsRecebedor, linha1_coluna3fsRecebedor, linha1_coluna4fsRecebedor]
        };

        var linha2fsRecebedor = {
            layout: 'column',
            border: false,
            items: [linha2_coluna1fsRecebedor, linha2_coluna2fsRecebedor, linha2_coluna3fsRecebedor, linha2_coluna4fsRecebedor]
        };

        var clsRecebedor = alteracaoRecebedor == "true" ? 'campoCorrigido' : 'none';

        if (validacaoAlteracaoRecebedor == "false") {
            clsRecebedor = 'campoInconforme';
        }

        var fsRecebedor = {
            xtype: 'fieldset',
            id: 'fsRecebedor',
            name: 'fsRecebedor',
            autoHeight: true,
            bodyStyle: 'padding: 10px',
            width: 520,
            title: 'Recebedor',
            cls: clsRecebedor,
            items: [linha1fsRecebedor, linha2fsRecebedor]
        };

        var linha2DadosDoFluxo_coluna1 = { width: 530, layout: 'form', border: false, items: [fsExpedidor] };
        var linha2DadosDoFluxo_coluna2 = { width: 520, layout: 'form', border: false, items: [fsRecebedor] };

        var linha2DadosDoFluxo = {
            layout: 'column',
            border: false,
            items: [linha2DadosDoFluxo_coluna1, linha2DadosDoFluxo_coluna2]
        };

        var lblTomador = {
            xtype: 'textfield',
            fieldLabel: 'Tomador',
            id: 'lblTomador',
            name: 'lblTomador',
            value: empresa_tomador,
            cls: 'x-item-disabled',
            readOnly: true,
            width: 230
        };


        var lblEnderecoTomador = {
            xtype: 'textfield',
            fieldLabel: 'Endereço',
            id: 'lblEnderecoTomador',
            name: 'lblEnderecoTomador',
            value: endereco_tomador,
            cls: 'x-item-disabled',
            readOnly: true,
            width: 230
        };

        var lblMunicipioTomador = {
            xtype: 'textfield',
            fieldLabel: 'Município',
            id: 'lblMunicipioTomador',
            name: 'lblMunicipioTomador',
            value: municipio_tomador,
            cls: 'x-item-disabled',
            readOnly: true,
            width: 100
        };

        var lblCEPTomador = {
            xtype: 'textfield',
            fieldLabel: 'CEP',
            id: 'lblCEPTomador',
            name: 'lblCEPTomador',
            value: cep_tomador,
            cls: 'x-item-disabled',
            width: 80,
            readOnly: true
        };

        var lblCPF_CNPJ_Tomador = {
            xtype: 'textfield',
            fieldLabel: 'CPF/CNPJ',
            id: 'lblCPF_CNPJ_Tomador',
            name: 'lblCPF_CNPJ_Tomador',
            value: cnpj_tomador,
            cls: 'x-item-disabled',
            width: 110,
            readOnly: true
        };

        var lblInscEstTomador = {
            xtype: 'textfield',
            fieldLabel: 'Insc. Est',
            id: 'lblInscEstTomador',
            name: 'lblInscEstTomador',
            value: insc_est_tomador,
            cls: 'x-item-disabled',
            width: 80,
            readOnly: true
        };

        var lblUFTomador = {
            xtype: 'textfield',
            fieldLabel: 'UF',
            id: 'lblUFTomador',
            name: 'lblUFTomador',
            value: uf_tomador,
            cls: 'x-item-disabled',
            width: 30,
            readOnly: true
        };

        var lblPaisTomador = {
            xtype: 'textfield',
            fieldLabel: 'País',
            id: 'lblPaisTomador',
            name: 'lblPaisTomador',
            value: pais_tomador,
            cls: 'x-item-disabled',
            width: 30,
            readOnly: true
        };

        var linha1_coluna1fsTomador = { width: 240, layout: 'form', border: false, items: [lblTomador] };
        var linha1_coluna2fsTomador = { width: 120, layout: 'form', border: false, items: [lblCPF_CNPJ_Tomador] };
        var linha1_coluna3fsTomador = { width: 90, layout: 'form', border: false, items: [lblInscEstTomador] };
        var linha1_coluna4fsTomador = { width: 240, layout: 'form', border: false, items: [lblEnderecoTomador] };
        var linha1_coluna5fsTomador = { width: 90, layout: 'form', border: false, items: [lblCEPTomador] };
        var linha1_coluna6fsTomador = { width: 110, layout: 'form', border: false, items: [lblMunicipioTomador] };
        var linha1_coluna7fsTomador = { width: 40, layout: 'form', border: false, items: [lblUFTomador] };
        var linha1_coluna8fsTomador = { width: 50, layout: 'form', border: false, items: [lblPaisTomador] };

        var linha1fsTomador = {
            layout: 'column',
            border: false,
            items: [linha1_coluna1fsTomador, linha1_coluna2fsTomador, linha1_coluna3fsTomador, linha1_coluna4fsTomador, linha1_coluna5fsTomador,
                linha1_coluna6fsTomador, linha1_coluna7fsTomador, linha1_coluna8fsTomador]
        };

        var clsTomador = 'none';

        if (alteracaoTomador == "true") {
            clsTomador = 'campoInconforme';
        }

        var fsTomador = {
            xtype: 'fieldset',
            id: 'fsTomador',
            name: 'fsTomador',
            autoHeight: true,
            bodyStyle: 'padding: 10px',
            width: 1070,
            title: 'Tomador',
            cls: clsTomador,
            items: [linha1fsTomador]
        };

        var linha3DadosDoFluxo = {
            layout: 'column',
            border: false,
            items: [fsTomador]
        };

        var fsDadosDoFluxo = {
            xtype: 'fieldset',
            id: 'fsDadosDoFluxo',
            name: 'fsDadosDoFluxo',
            autoHeight: true,
            bodyStyle: 'padding: 10px',
            width: 1100,
            title: 'Dados do Fluxo',
            items: [linha1DadosDoFluxo, linha2DadosDoFluxo, linha3DadosDoFluxo]
        };

        var linhaDadosDoFluxo = {
            layout: 'column',
            border: false,
            items: [fsDadosDoFluxo]
        };

        arrDetalhesFormulario.push(linhaDadosDoFluxo);

        var lblBaseCalculo = {
            xtype: 'textfield',
            fieldLabel: 'Base de Cálculo',
            id: 'lblBaseCalculo',
            name: 'lblBaseCalculo',
            value: baseCalculo,
            cls: 'x-item-disabled',
            readOnly: true,
            width: 100
        };

        var lblAliqIcms = {
            xtype: 'textfield',
            fieldLabel: 'Alíq. ICMS',
            id: 'lblAliqIcms',
            name: 'lblAliqIcms',
            value: aliqIcms,
            cls: 'x-item-disabled',
            readOnly: true,
            width: 100
        };

        var lblValorIcms = {
            xtype: 'textfield',
            fieldLabel: 'Valor ICMS',
            id: 'lblValorIcms',
            name: 'lblValorIcms',
            value: valorIcms,
            cls: 'x-item-disabled',
            readOnly: true,
            width: 100
        };

        var lblDesconto = {
            xtype: 'textfield',
            fieldLabel: 'Desconto',
            id: 'lblDesconto',
            name: 'lblDesconto',
            value: desconto,
            cls: 'x-item-disabled',
            width: 100,
            readOnly: true
        };

        var lblValorTotal = {
            xtype: 'textfield',
            fieldLabel: 'Valor Total',
            id: 'lblValorTotal',
            name: 'lblValorTotal',
            value: valorTotal,
            cls: 'x-item-disabled',
            width: 100,
            readOnly: true
        };

        var lblValorReceber = {
            xtype: 'textfield',
            fieldLabel: 'Valor a Receber',
            id: 'lblValorReceber',
            name: 'lblValorReceber',
            value: valorReceber,
            cls: 'x-item-disabled',
            width: 80,
            readOnly: true
        };

        var linha1_coluna1fsPrestacaoServicos = { width: 110, layout: 'form', border: false, items: [lblBaseCalculo] };
        var linha1_coluna2fsPrestacaoServicos = { width: 110, layout: 'form', border: false, items: [lblAliqIcms] };
        var linha1_coluna3fsPrestacaoServicos = { width: 110, layout: 'form', border: false, items: [lblValorIcms] };
        var linha1_coluna4fsPrestacaoServicos = { width: 110, layout: 'form', border: false, items: [lblDesconto] };
        var linha1_coluna5fsPrestacaoServicos = { width: 110, layout: 'form', border: false, items: [lblValorTotal] };
        var linha1_coluna6fsPrestacaoServicos = { width: 110, layout: 'form', border: false, items: [lblValorReceber] };

        var linha1fsPrestacaoServicos = {
            layout: 'column',
            border: false,
            items: [linha1_coluna1fsPrestacaoServicos, linha1_coluna2fsPrestacaoServicos, linha1_coluna3fsPrestacaoServicos,
                linha1_coluna4fsPrestacaoServicos, linha1_coluna5fsPrestacaoServicos, linha1_coluna6fsPrestacaoServicos]
        };

        var fsValoresDaPrestacaoDeServico = {
            xtype: 'fieldset',
            id: 'fsValoresDaPrestacaoDeServico',
            name: 'fsValoresDaPrestacaoDeServico',
            autoHeight: true,
            bodyStyle: 'padding: 10px',
            width: 1080,
            title: 'Valores da Prestação de Serviço',
            items: [linha1fsPrestacaoServicos]
        };

        arrDetalhesFormulario.push(fsValoresDaPrestacaoDeServico);

        var dsNotasVagoes = new Ext.data.JsonStore({
            url: '<%= Url.Action("ObterNotaCte") %>',
            root: "Items",
            fields: [
                'TipoDocumento',
                'SerieNotaFiscal',
                'NumeroNotaFiscal',
                'PesoTotal',
                'PesoRateio',
                'CnpjRemetente',
                'Conteiner',
                'CodConteiner',
                'ChaveNfe'
            ],
            baseParams: { id: cteId }
        });

        var gridNotasVagoes = new Ext.grid.EditorGridPanel({
            viewConfig: {
                forceFit: true
            },
            autoHeight: true,
            autoScroll: true,
            columnLines: true,
            region: 'center',
            stripeRows: true,
            clicksToEdit: 1,
            store: dsNotasVagoes,
            loadMask: { msg: App.Resources.Web.Carregando },
            colModel: new Ext.grid.ColumnModel({
                defaults: {
                    sortable: false
                },
                columns: [
                    { dataIndex: "ChaveNfe", sortable: false, hidden: true  },
                    { dataIndex: "CodConteiner", sortable: false, hidden: true  },
                    { header: 'TIPO DOCUMENTO', width: 100, dataIndex: "TipoDocumento", sortable: false },
                    { header: 'CPF/CNPJ EMITENTE', width: 100, dataIndex: "CnpjRemetente", sortable: false },
                    { header: 'SÉRIE', width: 50, dataIndex: "SerieNotaFiscal", sortable: false },
                    { header: 'DOCUMENTO', width: 50, dataIndex: "NumeroNotaFiscal", sortable: false },
                    { header: 'PESO REAL', dataIndex: "PesoTotal", sortable: false, renderer: RendererFloat },
                    { header: 'PESO RATEADO', dataIndex: "PesoRateio", sortable: false, renderer: RendererFloat },
                    { header: 'CONTEINER', dataIndex: "Conteiner", sortable: false, editor: new Ext.form.TextField({ readOnly: !indFluxoConteiner, allowblank: !indFluxoConteiner, autoCreate: { tag: 'input', type: 'text', maxlength: '11', autocomplete: 'off' }, maskRe: /[a-zA-Z0-9]/ }) }
                ]
            })
        });

        gridNotasVagoes.on('validateedit', afterEditConteiner, this);

        var theSelectionModel = gridNotasVagoes.getSelectionModel();
        theSelectionModel.onEditorKey = function(field, e) {
            var k = e.getKey(), newCell, g = theSelectionModel.grid, ed = g.activeEditor;
            if (k == e.TAB) {
                e.stopEvent();
                ed.completeEdit();
            }
        };

        var fsDocumentosOriginarios = {
            xtype: 'fieldset',
            id: 'fsDocumentosOriginarios',
            name: 'fsDocumentosOriginarios',
            autoHeight: true,
            bodyStyle: 'padding: 10px',
            width: 1080,
            title: 'Documentos Originários',
            items: [gridNotasVagoes]
        };

        arrDetalhesFormulario.push(fsDocumentosOriginarios);

        var formCorrecaoCte = new Ext.FormPanel({
            id: 'formCorrecaoCte',
            name: 'formCorrecaoCte',
            autoScroll: true,
            region: 'center',
            bodyStyle: 'padding: 15px',

            items: [
                arrDetalhesFormulario
            ],
            buttonAlign: 'center',
            labelAlign: 'top',
            buttons: [
                {
                    id: 'btnSalvar',
                    name: 'btnSalvar',
                    text: 'Salvar',
                    type: 'submit',
                    //disabled: true,
                    iconCls: 'icon-save',
                    handler: function(b, e) {
                        Ext.Msg.show({
                            title: 'Mensagem de Informação',
                            msg: 'Você realmente deseja salvar as alterações?',
                            buttons: Ext.Msg.YESNO,
                            fn: salvarCorrecoesCte,
                            icon: Ext.MessageBox.QUESTION
                        });
                    }
                },
                {
                    text: 'Voltar',
                    type: 'button',
                    handler: function(b, e) {
                        VoltarTela();
                    }
                }
            ]
        });

        $(function() {

            dsNotasVagoes.load();
            var view = new Ext.Viewport({
                layout: 'border',
                items: [
                    {
                        region: 'north',
                        height: 40,
                        items: [{
                            region: 'center',
                            applyTo: 'header-content'
                        }]
                    },
                    {
                        region: 'center',
                        autoScroll: true,
                        items: [formCorrecaoCte]
                    }
                ]
            });
        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div id="header-content">
        <h1>
            CTe - Carta de Correção</h1>
        <small>Você está em CTe > Carta de Correção</small>
        <br />
    </div>
</asp:Content>
