﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Interna.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content runat="server" ID="HeadContent" ContentPlaceHolderID="HeadContent">
     <%
         var mensagem = (string)ViewData["MENSAGEM"];
     %>
    <script type="text/javascript">
        var mensagem = "<%= mensagem %>";

        if (mensagem != '') {
            alert(mensagem);
        }

        var formModal = null;
        var grid = null;
        var lastRowSelected = -1;

        // Flag para garantir que uma tela de mensagem que está sobreposta não chame novamente a tela de pesquisa
        var executarPesquisa = true;

        // chamada ajax
        var url = '<%= Url.Action("Correcao") %>';

        var dsStatus = new Ext.data.ArrayStore({
            fields: ['value', 'text'],
            data: [['0', 'Ativo'],
                ['1', 'Inativo']]
        });

        function showResult() {
            executarPesquisa = true;
        }

        function FormSuccess(form, action) {
            ds.removeAll();
            ds.loadData(action.result, true);
        }

        function MudaCor(row) {
            if (row.data.CteComAgrupamentoNaoCancelado) {
                return 'cor';
            }
            return '';
        }

        function Pesquisar() {
            var diferenca = Ext.getCmp("filtro-data-final").getValue() - Ext.getCmp("filtro-data-inicial").getValue();
            diferenca = Math.floor(diferenca / (1000 * 60 * 60 * 24));

            if (Ext.getCmp("filtro-data-inicial").getValue() == '' || Ext.getCmp("filtro-data-final").getValue() == '') {
                executarPesquisa = false;
                Ext.Msg.show({
                    title: "Mensagem de Informação",
                    msg: "Preencha os filtro datas!",
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.INFO,
                    minWidth: 200,
                    fn: showResult
                });
            } else if (diferenca > 30) {
                executarPesquisa = false;
                Ext.Msg.show({
                    title: "Mensagem de Informação",
                    msg: "O período da pesquisa não deve ultrapassar 30 dias",
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.INFO,
                    minWidth: 200,
                    fn: showResult
                });
            } else if (Ext.getCmp("filtro-serie").getValue() != '' && Ext.getCmp("filtro-despacho").getValue() == '') {
                executarPesquisa = false;
                Ext.Msg.show({
                    title: "Mensagem de Informação",
                    msg: "Não é possível filtrar apenas pela a Série do Despacho.<br/>Também é necessário preencher o número do Despacho.",
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.INFO,
                    minWidth: 200,
                    fn: showResult
                });
            } else if (Ext.getCmp("filtro-despacho").getValue() != '' && Ext.getCmp("filtro-serie").getValue() == '') {
                executarPesquisa = false;
                Ext.Msg.show({
                    title: "Mensagem de Informação",
                    msg: "Não é possível filtrar apenas pelo número do Despacho.<br/>Também é necessário preencher a Série do Despacho.",
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.INFO,
                    minWidth: 200,
                    fn: showResult
                });
            } else {
                executarPesquisa = true;
                grid.getStore().load();
            }
        }

        function FormError(form, action) {
            Ext.Msg.alert('Erro', action.result.Message);
            ds.removeAll();
        }

        function onCorrecao(id) {
            url = url + "?cteId=" + id;
            document.location.href = url;
        }

        function MostrarWindowErros(mensagensErro) {
            var textarea = new Ext.form.TextArea({
                xtype: 'textarea',
                fieldLabel: 'Foram encontrados os seguintes erros',
                value: mensagensErro,
                readOnly: true,
                height: 220,
                width: 395
            });
            var windowErros;
            var formErros = new Ext.FormPanel({
                id: 'formErros',
                bodyStyle: 'padding: 15px',
                labelAlign: 'top',
                items: [textarea],
                buttonAlign: 'center',
                buttons: [{
                    text: 'Fechar',
                    type: 'button',
                    handler: function() {
                        windowErros.close();
                    }
                }]
            });

            windowErros = new Ext.Window({
                id: 'windowErros',
                title: 'Erros',
                modal: true,
                width: 444,
                height: 350,
                items: [formErros]
            });
            windowErros.show();
        }

        /* ------------- Ação CTRL+C nos campos da Grid ------------- */
        var isCtrl = false;
        document.onkeyup = function() {

            var keyId = event.keyCode;

            // 17 = tecla CTRL
            if (keyId == 17) {
                isCtrl = false; //libera tecla CTRL
            }
        };

        document.onkeydown = function() {

            var keyId = event.keyCode;

            // verifica se CTRL esta acionado
            if (keyId == 17) {
                isCtrl = true;
            }

            if ((keyId == 13) && (executarPesquisa)) {
                Pesquisar();
                return;
            }

            // 67 = tecla 'c'
            if (keyId == 67 && isCtrl == true) {

                // verifica se há selecão na tabela
                if (grid.getSelectionModel().hasSelection()) {
                    // captura todas as linhas selecionadas
                    var recordList = grid.getSelectionModel().getSelections();

                    var a = [];
                    var separator = '\t'; // separador para colunas no excel
                    for (var i = 0; i < recordList.length; i++) {
                        var s = '';
                        var item = recordList[i].data;
                        for (key in item) {
                            if (key != 'Id') { //elimina o campo id da linha
                                s = s + item[key] + separator;
                            }
                        }
                        s = s.substr(0, s.length - 1); //retira o ultimo separador '\t' da linha
                        a.push(s);
                    }
                    window.clipboardData.setData('Text', (a.join('\n'))); //insere linhas no clipBoard
                }
            }
        };
        /* ------------------------------------------------------- */

        statusCte = function(status) {

            if (status == 0) {
                return "Ativo";
            } else {
                return "Inativo";
            }
        };

        $(function () {

            var detalheAction = new Ext.ux.grid.RowActions({
                dataIndex: '',
                header: '',
                align: 'center',
                width: 300,
                actions:
                    [{
                        iconCls: 'icon-edit',
                        tooltip: 'Carta de Correção'
                    }],
                callbacks:
                    {
                        'icon-edit': function (grid, record) {
                            Ext.getCmp("gridListaCte").getEl().mask("Carregando os dados.", "x-mask-loading"); 
                            onCorrecao(record.data.CteId);
                        }
                    }
            });

            var gridStore = new Ext.data.JsonStore({
                url: '<%= Url.Action("ObterCtesCorrecao") %>',
                root: "Items",
                totalProperty: 'Total',
                remoteSort: true,
                paramNames: {
                    sort: "pagination.Sort",
                    dir: "pagination.Dir",
                    start: "pagination.Start",
                    limit: "pagination.Limit"
                },
                fields: [
                    'CteId',
                    'Serie',
                    'Despacho',
                    'CodigoVagao',
                    'CodFluxo',
                    'ToneladaUtil',
                    'Volume',
                    'NroCte',
                    'SerieCte',
                    'ChaveCte',
                    'DataEmissao',
                    'DespachoCancelado',
                    'CteComAgrupamentoNaoCancelado'
                ]
            });

            var dsCodigoControle = new Ext.data.JsonStore({
                root: "Items",
                autoLoad: true,
                url: '<%= Url.Action("ObterCodigoSerieDesp") %>',
                fields: [
                    'Id',
                    'CodigoControle'
                ]
            });

            var pagingToolbar = new Ext.PagingToolbar({
                pageSize: 50,
                store: gridStore,
                displayInfo: true,
                displayMsg: App.Resources.Web.GridExibindoRegistros,
                emptyMsg: App.Resources.Web.GridSemRegistros,
                paramNames: {
                    start: "pagination.Start",
                    limit: "pagination.Limit"
                }
            });

            grid = new Ext.grid.EditorGridPanel({
                viewConfig: {
                    forceFit: true,
                    getRowClass: MudaCor
                },
                id: 'gridListaCte',
                stripeRows: true,
                region: 'center',
                columnLines: true,
                autoLoadGrid: true,
                store: gridStore,
                loadMask: { msg: App.Resources.Web.Carregando },
                colModel: new Ext.grid.ColumnModel({
                    defaults: {
                        sortable: false
                    },
                    columns: [
                        { hidden: true },
                        detalheAction,
                        { dataIndex: "CteId", hidden: true },
                        { header: 'Série', dataIndex: "Serie", sortable: false },
                        { header: 'Despacho', dataIndex: "Despacho", sortable: false },
                        { header: 'Vagão', dataIndex: "CodigoVagao", sortable: false },
                        { header: 'Fluxo', dataIndex: "CodFluxo", sortable: false },
                        { header: 'TU', dataIndex: "ToneladaUtil", sortable: false, renderer: Ext.util.Format.numberRenderer('0.000,00/i') },
                        { header: 'Volume', dataIndex: "Volume", sortable: false, renderer: Ext.util.Format.numberRenderer('0.000,00/i') },
                        { header: 'Nro CTe', dataIndex: "NroCte", sortable: false },
                        { header: 'Série CTe', dataIndex: "SerieCte", sortable: false },
                        { header: 'Chave', dataIndex: "ChaveCte", sortable: false, hidden: true },
                        { header: 'Data Emissão', dataIndex: "DataEmissao", sortable: false },
                        { dataIndex: "CteComAgrupamentoNaoCancelado", hidden: true }
                    ]
                }),
                bbar: pagingToolbar,
                plugins: [detalheAction]
            });
            //---
            grid.on('render', function () {
                if (grid.autoLoadGrid)
                    grid.getStore().load({ params: { 'pagination.Start': 0, 'pagination.Limit': pagingToolbar.pageSize} });
            });

            grid.getStore().proxy.on('beforeload', function (p, params) {
                params['filter[0].Campo'] = 'dataInicial';
                params['filter[0].Valor'] = new Array(Ext.getCmp("filtro-data-inicial").getValue().format('d/m/Y') + " " + Ext.getCmp("filtro-data-inicial").getValue().format('H:i:s'));
                params['filter[0].FormaPesquisa'] = 'Start';

                params['filter[1].Campo'] = 'dataFinal';
                params['filter[1].Valor'] = new Array(Ext.getCmp("filtro-data-final").getValue().format('d/m/Y') + " " + Ext.getCmp("filtro-data-inicial").getValue().format('H:i:s'));
                params['filter[1].FormaPesquisa'] = 'Start';

                params['filter[2].Campo'] = 'serie';
                params['filter[2].Valor'] = Ext.getCmp("filtro-serie").getValue();
                params['filter[2].FormaPesquisa'] = 'Start';

                params['filter[3].Campo'] = 'despacho';
                params['filter[3].Valor'] = Ext.getCmp("filtro-despacho").getValue();
                params['filter[3].FormaPesquisa'] = 'Start';

                params['filter[4].Campo'] = 'fluxo';
                params['filter[4].Valor'] = Ext.getCmp("filtro-fluxo").getValue();
                params['filter[4].FormaPesquisa'] = 'Start';

                params['filter[5].Campo'] = 'chave';
                params['filter[5].Valor'] = Ext.getCmp("filtro-Chave").getValue();
                params['filter[5].FormaPesquisa'] = 'Start';

                params['filter[6].Campo'] = 'Origem';
                params['filter[6].Valor'] = Ext.getCmp("filtro-Ori").getValue();
                params['filter[6].FormaPesquisa'] = 'Start';

                params['filter[7].Campo'] = 'Destino';
                params['filter[7].Valor'] = Ext.getCmp("filtro-Dest").getValue();
                params['filter[7].FormaPesquisa'] = 'Start';

                params['filter[8].Campo'] = 'Vagao';
                params['filter[8].Valor'] = Ext.getCmp("filtro-numVagao").getValue();
                params['filter[8].FormaPesquisa'] = 'Start';
                params['filter[9].Campo'] = 'UfDcl';
                params['filter[9].Valor'] = Ext.getCmp("filtro-UfDcl").getRawValue();
                params['filter[9].FormaPesquisa'] = 'Start';
            });

            var dataAtual = new Date();

            var dataInicial = {
                xtype: 'datefield',
                fieldLabel: 'Data Inicial',
                id: 'filtro-data-inicial',
                name: 'dataInicial',
                width: 83,
                allowBlank: false,
                vtype: 'daterange',
                endDateField: 'filtro-data-final',
                hiddenName: 'dataInicial',
                value: dataAtual
            };

            var dataFinal = {
                xtype: 'datefield',
                fieldLabel: 'Data Final',
                id: 'filtro-data-final',
                name: 'dataFinal',
                width: 83,
                allowBlank: false,
                vtype: 'daterange',
                startDateField: 'filtro-data-inicial',
                hiddenName: 'dataFinal',
                value: dataAtual
            };

            var origem = {
                xtype: 'textfield',
                vtype: 'cteestacaovtype',
                style: 'text-transform: uppercase',
                id: 'filtro-Ori',
                fieldLabel: 'Origem',
                autoCreate: { tag: 'input', type: 'text', maxlength: '3', autocomplete: 'off' },
                name: 'Origem',
                allowBlank: true,
                maxLength: 3,
                width: 35,
                hiddenName: 'Origem'
            };

            var destino = {
                xtype: 'textfield',
                vtype: 'cteestacaovtype',
                style: 'text-transform: uppercase',
                id: 'filtro-Dest',
                fieldLabel: 'Destino',
                autoCreate: { tag: 'input', type: 'text', maxlength: '3', autocomplete: 'off' },
                name: 'Destino',
                allowBlank: true,
                maxLength: 3,
                width: 35,
                hiddenName: 'Destino'
            };

            var serie = {
                xtype: 'textfield',
                vtype: 'ctesdvtype',
                style: 'text-transform: uppercase',
                id: 'filtro-serie',
                fieldLabel: 'Série',
                name: 'serie',
                allowBlank: true,
                autoCreate: { tag: 'input', type: 'text', maxlength: '3', autocomplete: 'off' },
                maxLength: 20,
                width: 30,
                hiddenName: 'serie'
            };

            var despacho = {
                xtype: 'textfield',
                vtype: 'ctedespachovtype',
                id: 'filtro-despacho',
                fieldLabel: 'Despacho',
                name: 'despacho',
                allowBlank: true,
                maxLength: 20,
                autoCreate: { tag: 'input', type: 'text', maxlength: '6', autocomplete: 'off' },
                width: 55,
                hiddenName: 'despacho'
            };

            var fluxo = {
                xtype: 'textfield',
                vtype: 'ctefluxovtype',
                style: 'text-transform: uppercase',
                id: 'filtro-fluxo',
                fieldLabel: 'Fluxo',
                name: 'fluxo',
                allowBlank: true,
                autoCreate: { tag: 'input', type: 'text', maxlength: '5', autocomplete: 'off' },
                maxLength: 20,
                width: 45,
                maxValue: 99999,
                minValue: 0,
                hiddenName: 'fluxo'
            };

            var cboCteCodigoControle = {
                xtype: 'combo',
                store: dsCodigoControle,
                allowBlank: true,
                lazyInit: false,
                lazyRender: false,
                mode: 'local',
                typeAhead: false,
                triggerAction: 'all',
                fieldLabel: 'UF DCL',
                name: 'filtro-UfDcl',
                id: 'filtro-UfDcl',
                hiddenName: 'filtro-UfDcl',
                displayField: 'CodigoControle',
                forceSelection: true,
                width: 70,
                valueField: 'Id',
                emptyText: 'Selecione...',
                editable: false,
                tpl: '<tpl for="."><div class="x-combo-list-item">{CodigoControle}&nbsp;</div></tpl>'
            };

            var vagao = {
                xtype: 'textfield',
                vtype: 'ctevagaovtype',
                style: 'text-transform: uppercase',
                id: 'filtro-numVagao',
                fieldLabel: 'Vagão',
                name: 'numVagao',
                allowBlank: true,
                autoCreate: { tag: 'input', type: 'text', maxlength: '7', autocomplete: 'off' },
                maxLength: 20,
                width: 55,
                hiddenName: 'numVagao'
            };

            var chave = {
                xtype: 'textfield',
                vtype: 'ctevtype',
                id: 'filtro-Chave',
                fieldLabel: 'Chave CTe',
                name: 'Chave',
                autoCreate: { tag: 'input', type: 'text', maxlength: '44', autocomplete: 'off' },
                allowBlank: true,
                maxLength: 50,
                width: 275,
                hiddenName: 'Chave  '
            };

            var arrDataIni = {
                width: 87,
                layout: 'form',
                border: false,
                items:
                    [dataInicial]
            };

            var arrDataFim = {
                width: 87,
                layout: 'form',
                border: false,
                items:
                    [dataFinal]
            };

            var arrOrigem = {
                width: 43,
                layout: 'form',
                border: false,
                items:
                    [origem]
            };
            var arrDestino = {
                width: 43,
                layout: 'form',
                border: false,
                items:
                    [destino]
            };

            var arrSerie = {
                width: 37,
                layout: 'form',
                border: false,
                items:
                    [serie]
            };

            var arrDespacho = {
                width: 60,
                layout: 'form',
                border: false,
                items:
                    [despacho]
            };

            var arrFluxo = {
                width: 50,
                layout: 'form',
                border: false,
                items:
                    [fluxo]
            };

            var arrVagao = {
                width: 60,
                layout: 'form',
                border: false,
                items:
                    [vagao]
            };

            var arrChave = {
                width: 280,
                layout: 'form',
                border: false,
                items:
                    [chave]
            };

            var arrCodigoDcl = {
                width: 75,
                layout: 'form',
                border: false,
                items:
                    [cboCteCodigoControle]
            };


            var arrlinha1 = {
                layout: 'column',
                border: false,
                items: [arrDataIni, arrDataFim, arrFluxo, arrChave]
            };

            var arrlinha2 = {
                layout: 'column',
                border: false,
                items: [arrCodigoDcl, arrSerie, arrDespacho, arrOrigem, arrDestino, arrVagao]
            };

            var arrCampos = new Array();
            arrCampos.push(arrlinha1);
            arrCampos.push(arrlinha2);

            var filters = new Ext.form.FormPanel({
                id: 'grid-filtros',
                title: "Filtros",
                region: 'center',
                bodyStyle: 'padding: 15px',
                labelAlign: 'top',
                items:
                    [arrCampos],
                buttonAlign: "center",
                buttons:
                    [
                        {
                            text: 'Pesquisar',
                            type: 'submit',
                            iconCls: 'icon-find',
                            handler: function () {
                                Pesquisar();
                            }
                        },
                        {
                            text: 'Limpar',
                            handler: function () {

                                grid.getStore().removeAll();
                                grid.getStore().totalLength = 0;
                                grid.getBottomToolbar().bind(grid.getStore());
                                grid.getBottomToolbar().updateInfo();
                                Ext.getCmp("grid-filtros").getForm().reset();
                            },
                            scope: this
                        }
                    ]
            });

            var view = new Ext.Viewport({
                layout: 'border',
                margins: 10,
                items: [
                    {
                        region: 'north',
                        height: 230,
                        items: [{
                            region: 'center',
                            applyTo: 'header-content'
                        },
                            filters]
                    },
                    grid
                ]
            });


        });
    </script>
    <style>
        .cor
        {
            background-color: #FFEEDD;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div id="header-content">
        <h1>
            CTe - Carta de Correção</h1>
        <small>Você está em CTe > Carta de Correção</small>
        <br />
    </div>
</asp:Content>
