﻿<%@ Page Title="" Language="C#" Inherits="System.Web.Mvc.ViewPage" MasterPageFile="~/Views/Shared/Interna.master" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        //***************************** VARIÁVEIS E INICIALIZAÇÃO**************//      

        var idFornecedor = '<%=ViewData["idFornecedor"] %>';
        var NomeFornecedor = '<%=ViewData["NomeFornecedor"] %>';
        var Login = '<%=ViewData["usuario"] %>';
        
        var ativo = '<%=ViewData["Ativo"].ToString().ToLower()  %>';
        var inativo = '<%= (!Convert.ToBoolean(ViewData["Ativo"])).ToString().ToLower()  %>';

        var persistencia;

        //******************************************************************//
        //***************************** STORE ******************************//
        //******************************************************************//
        var gridFornecedorStore = new Ext.data.JsonStore({
            root: "Items",
            totalProperty: 'Total',
            id: 'gridFornecedorStore',
            name: 'gridFornecedorStore',
            proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("ObterGridLocaisServicosFornecedores", "Fornecedor") %>', timeout: 600000 }),
            paramNames: {
                sort: "detalhesPaginacaoWeb.Sort",
                dir: "detalhesPaginacaoWeb.Dir",
                start: "detalhesPaginacaoWeb.Start",
                limit: "detalhesPaginacaoWeb.Limit"
            },
            fields: ['IdFornecedorOs', 'idTipoServico', 'Servico', 'Local']
        });

        var TipoServicoStore = new Ext.data.JsonStore({
            root: "Items",
            autoLoad: true,
            proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("ObterTipoServicoCriarFornecedor", "Fornecedor") %>', timeout: 600000 }),
            id: 'TipoServicoStore',
            fields: ['IdTipoServico', 'DescricaoServico']
            //                    baseParams: {local : local }
        });

        //******************************************************************//
        //***************************** CAMPOS *****************************//
        //******************************************************************//
        var btnIncluir = {
            xtype: 'button',
            name: 'btnIncluir',
            id: 'btnIncluir',
            text: 'Incluir',
            iconCls: 'icon-new',
            handler: IncluirLocalServico
        };

        var txtFornecedor = {
            xtype: 'textfield',
            name: 'txtFornecedor',
            id: 'txtFornecedor',
            fieldLabel: 'Fornecedor',
            autoCreate: {
                tag: 'input',
                type: 'text',
                autocomplete: 'off',
                maxlength: '70',
                allowBlank: false
            },
            allowBlank: false,
            blankText: 'Você deve informar Fornecedor!',
            //            maskRe: /[A-Za-z+;\s]/,
            //            style: 'text-transform:uppercase;',
            width: 550
        };

        var rdbHabilitarDesabilitar = {
            xtype: 'radiogroup',
            flex: 8,
            vertical: true,
            columns: 2,
            labelWidth: 50,
            id: 'rdbStatus',
            fieldLabel: 'Status',
            items: [{
                id: 'rdbHabilitado',
                boxLabel: 'Habilitado',
                name: 'rb-auto',
                inputValue: 1
            }, {
                id: 'rdbDesabilitado',
                boxLabel: 'Desabilitado',
                name: 'rb-auto',
                inputValue: 2
            }]
        };

        var txtLocal = {
            xtype: 'textfield',
            name: 'txtLocal',
            id: 'txtLocal',
            fieldLabel: 'Local',
            autoCreate: {
                tag: 'input',
                type: 'text',
                autocomplete: 'off',
                maxlength: '7'
            },
            allowBlank: false,
            blankText: 'Você deve informar Local!',
            //maskRe: /[A-Za-z+;\s]/,
            style: 'text-transform:uppercase;',
            width: 86
        };

        var ddlTipoServico = new Ext.form.ComboBox({
            editable: false,
            typeAhead: true,
            forceSelection: true,
            disableKeyFilter: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            store: TipoServicoStore,
            valueField: 'IdTipoServico',
            displayField: 'DescricaoServico',
            fieldLabel: 'Serviço',
            id: 'ddlTipoServico',
            allowBlank: false,
            blankText: 'Você deve selecionar um serviço!',
            width: 100,
            alowBlank: false
        });

        //******************************************************************//
        //***************************** GRID VAGÕES*************************//
        //******************************************************************//
        var gridFornecedor = new Translogic.PaginatedGrid({
            id: 'gridFornecedor',
            name: 'gridFornecedor',
            autoLoadGrid: true,
            viewConfig: {
                forceFit: true,
                emptyText: 'Não possui dado(s) para exibição.'
            },
            height: 300,
            width: 550,
            autoScroll: true,
            fields: [
                    'IdFornecedorOs',
                    'idTipoServico',
                    'Servico',
                    'Local'
                    ],
            url: '<%= Url.Action("ObterGridLocaisServicosFornecedores", "Fornecedor") %>',
            columns: [
                    { header: 'Tipo de Serviço', dataIndex: "Servico", sortable: false, width: 130 },
                    { header: 'Local', dataIndex: "Local", sortable: false, width: 100 },
                    { hideable: false, width: 20, renderer: function (val) { return '<div class="ux-row-action-item icon-delete" data-id="' + val + '"></div>'; }, dataIndex: 'IdFornecedor' }
                ],
            listeners: {
                cellclick: function (iView, iCellEl, iColIdx, iRecord, iRowEl, iRowIdx, iEvent) {
                    //Verifica se usuario clicou no X(coluna 3)
                    if (iColIdx == 2) {
                        Ext.Msg.show({ title: 'Salvar',
                            msg: 'Confirma a exclusão do tipo de serviço e local?',
                            buttons: Ext.Msg.YESNO,
                            icon: Ext.MessageBox.QUESTION,
                            animEl: 'elId',
                            fn: function (btn, text) {
                                
                                if (btn == 'yes') {
                                    //Remove linha grid
                                    gridFornecedor.getStore().removeAt(iCellEl);

                                    //Atualiza Grid ids
                                    gridFornecedor.getView().refresh();
                                }
                            }
                        });


                    }
                }
            }
        });

        gridFornecedor.getStore().proxy.on('beforeload', function (p, params) {
            params['idFornecedor'] = idFornecedor;
        });

        //******************************************************************//
        //***************************** LAYOUT *****************************//
        //******************************************************************//
        var field1 = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            autoWidth: true,
            items: [txtFornecedor]
        };

        var field2 = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            width: 180,
            items: [rdbHabilitarDesabilitar]
        };

        var formAux1 = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            autoWidth: true,
            items: [txtLocal]
        };
        var formAux2 = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            autoWidth: true,
            items: [ddlTipoServico]
        };

        var formAux3 = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 20px',
            autoWidth: true,
            items: [btnIncluir]
        };

        var formGrid = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            autoWidth: true,
            items: [gridFornecedor]
        };

        //Containers
        var containerLinha1 = {
            layout: 'column',
            border: true,
            items: [field1]
        };

        var containerLinha2 = {
            layout: 'column',
            border: true,
            items: [field2]
        };

        var containerLinh3 = {
            layout: 'column',
            border: false,
            items: [formAux1, formAux2, formAux3]
        };

        var containerLinh4 = {
            layout: 'form',
            border: true,
            items: [containerLinh3, formGrid]
        };

        //******************************************************************//
        //***************************** FUNÇÕES*****************************//
        //******************************************************************//
        function ValidaFornecedorEGrid() {            

            var registrosGrid = gridFornecedor.getStore().getRange().length;
            var fornecedor = Ext.getCmp('txtFornecedor').getRawValue();
           
            if (fornecedor=="" && registrosGrid==0) {
                Ext.Msg.show({ title: 'Aviso',
                    msg: 'O campo Fornecedor é obrigatório <br/>  Vincule pelo menos um local e serviço ao fornecedor!',
                    buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR,
                    fn: function (btn, text)
                    { Ext.getCmp('txtFornecedor').focus(); }
                });
                return false;
            }

            if (fornecedor=="") {
                Ext.Msg.show({ title: 'Aviso',
                    msg: 'O campo Fornecedor é obrigatório',
                    buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR,
                    fn: function (btn, text)
                    { Ext.getCmp('txtFornecedor').focus(); }
                });
                return false;
            }
            else if (registrosGrid == 0) {
                Ext.Msg.show({ title: 'Aviso',
                    msg: 'Incluir ao menos um tipo de serviço com local correspondente para o Fornecedor!',
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.ERROR,
                    fn: function (btn, text) { Ext.getCmp('txtLocal').focus(); }
                });
                return false;
            }
            else return true;
        }

        function ValidaLocalServico() {
            
            if (Ext.getCmp('txtLocal').getRawValue() == "") {
                Ext.Msg.show({ title: 'Aviso',
                    msg: 'O campo Local é obrigatório',
                    buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR,
                    fn: function (btn, text) { Ext.getCmp('txtLocal').focus(); }
                });
                return false;
            }
            else if (Ext.getCmp('txtLocal').getRawValue().length < 3) {
                Ext.Msg.show({ title: 'Aviso',
                    msg: 'O nome do Local é composto de 3 ou mais caracteres. Por favor, informe um local correto',
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.ERROR,
                    fn: function (btn, text) { Ext.getCmp('txtLocal').focus(); }
                });
                return false;
            }
            else if (Ext.getCmp('ddlTipoServico').getRawValue() == "") {
                Ext.Msg.show({ title: 'Aviso',
                    msg: 'O campo Serviço é obrigatório',
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.ERROR,
                    fn: function (btn, text) { Ext.getCmp('ddlTipoServico').focus(); }
                });
                return false;
            }
            else return true;
        }

        function IncluirLocalServico() {
            
            if (!ValidaLocalServico()) {
                return;
            }

            /*  //Verifica campos obrigatórios
            if (Ext.getCmp('txtFornecedor').getRawValue() == "") {
            return Ext.Msg.show({ title: 'Aviso', msg: 'Informe fornecedor', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR, fn: function (btn, text) { Ext.getCmp('txtFornecedor').focus(); } });
            } else if (Ext.getCmp('txtLocal').getRawValue() == "") {
            return Ext.Msg.show({ title: 'Aviso', msg: 'Informe Local', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR, fn: function (btn, text) { Ext.getCmp('txtLocal').focus(); } });
            } else if (Ext.getCmp('txtLocal').getRawValue().length < 3) {
            return Ext.Msg.show({ title: 'Aviso', msg: 'O nome do Local é composto de 3 ou mais caracteres. Por favor, informe um local correto', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR, fn: function (btn, text) { Ext.getCmp('txtLocal').focus(); } });
            }else if (Ext.getCmp('ddlTipoServico').getRawValue() == "") {
            return Ext.Msg.show({ title: 'Aviso', msg: 'Informe Serviço', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR, fn: function (btn, text) { Ext.getCmp('ddlTipoServico').focus(); } });
            }*/


            var nomeFornecedor = Ext.getCmp('txtFornecedor').getValue();
            var localServico = Ext.getCmp('txtLocal').getRawValue();
            var idTipoServico = Ext.getCmp('ddlTipoServico').getValue();
            var habilitado = Ext.getCmp("rdbHabilitado").getValue();
            var status = (habilitado == true ? 'S' : 'N');
            //Verifica fornecedor existe BD

            var msgRetorno = VerificaLocalExiste(localServico);

            if (msgRetorno != "") {
                Ext.Msg.show({ title: 'Aviso', msg: msgRetorno, buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                Ext.getCmp('txtLocal').setValue("");
                Ext.getCmp('ddlTipoServico').setValue("");
            }
            else {
                //Cria OBJ do tipo DATAGRID(SENCHA)
                var novoFornecedor = Ext.data.Record.create(['IdFornecedor', 'Local', 'Servico', 'idTipoServico']);

                //Armazena GRID em memoria
                var gridFornecedor = Ext.getCmp('gridFornecedor');
                var Local = Ext.getCmp('txtLocal').getRawValue();
                Local = Local.toUpperCase();

                //Armazena dados no OBJ do tipo da datagrid para inclusão posterior
                var record = new novoFornecedor({
                    IdFornecedor: idFornecedor,
                    Local: Local,
                    Servico: Ext.getCmp('ddlTipoServico').getRawValue(),
                    idTipoServico: idTipoServico
                });

                //Verifica se registro está na GRID antes de salvar
                if (VerificaLocalServicoExiste(gridFornecedor) == false) {
                    //Inclui OBJETO na grid

                    gridFornecedor.getStore().add(record);
                    Ext.getCmp('txtLocal').reset();
                    Ext.getCmp('ddlTipoServico').clearValue();


                } else {
                    Ext.Msg.show({ title: 'Aviso', msg: 'Este local e serviço ja esta vinculado para este fornecedor', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                }

            }
        }


        function VerificaLocalServicoExiste(gridFornecedor) {

            var Existe = false;

            var rangeLocal = gridFornecedor.getStore().getRange();
            //Armazena valor selecionado combo Local
            var LocalValue = Ext.getCmp('txtLocal').getRawValue();
            //Armazena valor selecionado combo Tipo Servico
            var cmbServicoValue = Ext.getCmp('ddlTipoServico').getRawValue();

            LocalValue = LocalValue.toUpperCase();
            //Percorre registros da GRID fornecedores
            $.each(rangeLocal, function (c, col) {

                //Verifica se registro que usuario está tentando inserir já existe
                if (col.data.Local == LocalValue && col.data.Servico == cmbServicoValue) {
                    Existe = true;
                }
            });

            if (Existe) {
                return true;
            } else {
                return false;
            }
        }

        function VerificaLocalExiste(local) {
            var retorno = "";

            // Progress BAR
            Ext.getBody().mask("Processando dados...", "x-mask-loading");
            var jsonRequest = $.toJSON({ local: local });
            $.ajax({
                url: '<%= Url.Action("VerificarExisteLocal", "Fornecedor") %>',
                type: "POST",
                dataType: 'string',
                data: jsonRequest,
                timeout: 300000,
                cache: false,
                async: false,
                contentType: "application/json; charset=utf-8",
                failure: function (conn, data) {
                    Ext.Msg.alert("Mensagem de Erro", "Ocoreu um erro inesperado");
                    Ext.getBody().unmask(); //                   
                    return false;
                },
                success: function (result) {

                    retorno = result;
                    Ext.getBody().unmask();
                }
            });

            return retorno;
        }

        function SalvarFornecedorBD() {
            
            if (!ValidaFornecedorEGrid()) {
                return;
            }

            var habilitado = Ext.getCmp("rdbHabilitado").getValue();
            //condição para verificar se é edição do registro
            if (idFornecedor > 0 && habilitado == false) {
                Ext.Msg.show({ title: 'Salvar',
                    msg: 'O fornecedor será desabilitado para a geração de O.S. Deseja prosseguir com a alteração?',
                    buttons: Ext.Msg.YESNO,
                    fn: PersisteFornecedor,
                    animEl: 'elId',
                    icon: Ext.MessageBox.QUESTION
                });
            }
            else {
                PersisteFornecedor("");
            }
        }

        function PersisteFornecedor(btn) {
            if (btn == "no") {
                return;
            }
            var itens = gridFornecedor.getStore().getRange();
            if (itens.length > 0) {

                // Progress BAR
                Ext.getBody().mask("Processando dados...", "x-mask-loading");
                fornecedorOsDto = montaObjetoTransferencia();

                var jsonRequest = $.toJSON({ fornecedorOsDto: fornecedorOsDto });
                $.ajax({
                    url: '<%= Url.Action("SalvarLocaisServicosFornecedorBD", "Fornecedor") %>',
                    //url: URL,
                    type: "POST",
                    dataType: 'json',
                    data: jsonRequest,
                    timeout: 300000,
                    async: false,
                    contentType: "application/json; charset=utf-8",
                    failure: function (conn, data) {

                        Ext.getBody().unmask();
                        Ext.Msg.alert("Mensagem de Erro", "Ocorreu um erro inesperado.");

                    },
                    success: function (result) {
                        var message = "";
                        Ext.getBody().unmask();
                        //                             console.log(result.Success);
                        if (result.Success == "OK") {

                            if (idFornecedor != null) {

                                message = 'Fornecedor cadastrado com sucesso!';
                            } else { message = 'Fornecedor editado com sucesso!' }

                            Ext.getBody().unmask();
                            Ext.Msg.show({
                                title: 'Aviso',
                                msg: (message),
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.WARNING,
                                fn: function (btn, text) {
                                    if (btn == 'ok') {
                                        var url = '<% =Url.Action("Index", "Fornecedor") %>';
                                        window.location = url;
                                    }
                                }
                            });
                        }
                        else {
                            Ext.Msg.show({
                                title: 'Aviso',
                                msg: result.Success,
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.WARNING,
                                fn: function (btn, text) {
                                    if (btn == 'ok') {
                                        var url = '<% =Url.Action("Index", "Fornecedor") %>';
                                        window.location = url;
                                    }
                                }
                            });
                        }
                    }
                });
            } else {
                Ext.Msg.show({ title: 'Aviso', msg: 'Informe o nome do Fornecedor, <br/> pelo menos um local e um serviço!', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
            }
        }

        function Cancelar() {
            Ext.getBody().mask("Cancelando...", "x-mask-loading");
            var url = '<%= Url.Action("Index", "Fornecedor")%>';

            window.location = url;
        }

        function registrarExcluir(pai, beforeDelete, afterDelete) {

            $(pai + ' .icon-delete').live("click", function () {
                var btn = $(this);

                Ext.Msg.show({
                    title: 'Exclusão do registro',
                    msg: 'Tem certeza que deseja excluir?',
                    buttons: Ext.Msg.YESNO,
                    fn: function (confirm) {
                        if (confirm === 'yes') {
                            var index = btn.parents('.x-grid3-body').find('.icon-delete').index(btn);
                            var grid = Ext.getCmp(btn.parents('.x-grid-panel').attr('id'));
                            var store = grid.getStore();

                            if (beforeDelete) {
                                beforeDelete(store, index, grid);
                            }

                            store.removeAt(index);

                            if (afterDelete) {
                                afterDelete(store, index, grid);
                            }
                        }
                    },
                    icon: Ext.MessageBox.QUESTION
                });
            });
        }

        function montaObjetoTransferencia() {

            var fornecedorOsDto = {
                IdFornecedorOs: idFornecedor,
                Nome: Ext.getCmp('txtFornecedor').getValue(),
                Ativo: Ext.getCmp("rdbHabilitado").getValue(),
                LocaisTiposServicos: []
            };

            //Recupera LocaisServicosFornecedor do GRID
            var itens = gridFornecedor.getStore().getRange();

            for (var i = 0; i < itens.length; i++) {
                var _estacaoServicoDto = { IdFornecedorOs: idFornecedor, Local: itens[i].data.Local, Servico: itens[i].data.Servico,
                    IdTipoServico: itens[i].data.idTipoServico
                };
                fornecedorOsDto.LocaisTiposServicos.push(_estacaoServicoDto);
            }
            return fornecedorOsDto;
        }

        //******************************************************************//
        //***************************** BOTOES *****************************//
        //******************************************************************//
        var btnSalvarFornecedor = {
            name: 'btnSalvarFornecedor',
            id: 'btnSalvarFornecedor',
            text: 'Salvar',
            iconCls: 'icon-save',
            handler: SalvarFornecedorBD
        };

        var btnCancelar = {
            name: 'btnCancelar',
            id: 'btnCancelar',
            text: 'Cancelar',
            iconCls: 'icon-cancel',
            handler: Cancelar
        };

        //***************************** PAINEL *****************************//
        var panelForm = new Ext.form.FormPanel({
            id: 'panelForm',
            layout: 'form',
            labelAlign: 'top',
            border: false,
            autoHeight: true,
            title: "Criação de Fornecedor",
            region: 'center',
            bodyStyle: 'padding: 15px',
            items: [containerLinha1, containerLinha2, containerLinh4],
            buttonAlign: "center",
            buttons: [btnSalvarFornecedor, btnCancelar]
        });

        //***************************** RENDER *****************************//
        Ext.onReady(function () {
            panelForm.render("divContent");

            //Setar campos recuperados da OS
            if (NomeFornecedor != "") {
                Ext.getCmp("txtFornecedor").setValue(NomeFornecedor);
                if (ativo == "true") {
                    $("#rdbHabilitado").attr('checked', 'checked');
                } else {
                    $("#rdbDesabilitado").attr('checked', 'checked');
                }

            } else {
                $("#rdbHabilitado").attr('checked', 'checked');
            }

            $().ready(function () {
                $("#ext-comp-1001").css("display", "none");

            });
        });    
    
    </script>
</asp:Content>
<asp:Content ID="MainContent" ContentPlaceHolderID="Content" runat="server">
    <div id="header-content">
        <h1>
            Fornecedor</h1>
        <small>Você está em Fornecedor > Lista Fornecedores > Inserir Fornecedor</small>
        <br />
    </div>
    <div id="divContent">
    </div>
</asp:Content>
