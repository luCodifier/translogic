﻿<%@ Page Title="" Language="C#" Inherits="System.Web.Mvc.ViewPage" MasterPageFile="~/Views/Shared/Interna.master" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        //***************************** VARIÁVEIS E INICIALIZAÇÃO**************//
        //        var idOs = '<%=ViewData["idOs"] %>';
        //        var dataHora = '<%=ViewData["data"] %>';
        //        var local = '<%=ViewData["local"] %>';
        // var local = "LMG";
        //        $(function () {

        //            //Setar campos recuperados da OS
        //                        Ext.getCmp("txtNumOs").setValue(idOs);
        //                        Ext.getCmp("txtDataHora").setValue(dataHora);
        //                        Ext.getCmp("txtLocal").setValue(local);
        //        });

        //******************************************************************//
        //***************************** STORE ******************************//
        //******************************************************************//
        var gridFornecedorStore = new Ext.data.JsonStore({
            root: "Items",
            totalProperty: 'Total',
            id: 'gridFornecedorStore',
            name: 'gridFornecedorStore',
            proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("ObterGridFornecedores", "Fornecedor") %>', timeout: 600000 }),
            paramNames: {
                sort: "detalhesPaginacaoWeb.Sort",
                dir: "detalhesPaginacaoWeb.Dir",
                start: "detalhesPaginacaoWeb.Start",
                limit: "detalhesPaginacaoWeb.Limit"
            },
            fields: ['IdFornecedorOs', 'Nome', 'ConcaLocalServico', 'DtRegistro', 'Login']
        });

        var TipoServicoStore = new Ext.data.JsonStore({
            root: "Items",
            autoLoad: true,
            proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("ObterTipoServico", "Fornecedor") %>', timeout: 600000 }),
            id: 'TipoServicoStore',
            fields: ['IdTipoServico', 'DescricaoServico']
            //                    baseParams: {local : local }
        });

        //****************************'arrCampos' **************************************//
        //***************************** CAMPOS *****************************//
        //******************************************************************//
        $(function () {
            var txtFornecedor = {
                xtype: 'textfield',
                name: 'txtFornecedor',
                id: 'txtFornecedor',
                fieldLabel: 'Nome do Fornecedor',
                autoCreate: {
                    tag: 'input',
                    type: 'text',
                    autocomplete: 'off',
                    maxlength: '70'
                },
//                maskRe: /[A-Za-z+;\s]/,
                //            style: 'text-transform:uppercase;',
                width: 550
            };

            var txtLocal = {
                xtype: 'textfield',
                name: 'txtLocal',
                id: 'txtLocal',
                fieldLabel: 'Local',
                autoCreate: {
                    tag: 'input',
                    type: 'text',
                    autocomplete: 'off',
                    maxlength: '3'
                },
                //                maskRe: /[A-Za-z+;\s]/,
                style: 'text-transform:uppercase;',
                width: 35
            };

            var ddlTipoServico = new Ext.form.ComboBox({
                editable: false,
                typeAhead: true,
                forceSelection: true,
                disableKeyFilter: true,
                triggerAction: 'all',
                lazyRender: true,
                mode: 'local',
                store: TipoServicoStore,
                valueField: 'IdTipoServico',
                displayField: 'DescricaoServico',
                fieldLabel: 'Serviço',
                id: 'ddlTipoServico',
                width: 100,
                alowBlank: false
            });

            var btnPesquisar = {
                xtype: 'button',
                name: 'btnPesquisar',
                id: 'btnPesquisar',
                text: 'Pesquisar',
                iconCls: 'icon-find',
                handler: PesquisarFornecedores
            };

            var btnLimpar = {
                xtype: 'button',
                name: 'btnLimpar',
                id: 'btnLimpar',
                text: 'Limpar',
                iconCls: 'icon-delete',
                handler: LimpaCampos
            };

            var rdbHabilitarDesabilitar = {
                xtype: 'radiogroup',
                flex: 8,
                vertical: true,
                columns: 2,
                labelWidth: 50,
                id: 'rdbStatus',
                fieldLabel: 'Status',
                items: [{
                    boxLabel: 'Habilitado',
                    name: 'rb-auto',
                    id: 'rdbHabilitado',
                    inputValue: 1,
                    checked: true
                }, {
                    boxLabel: 'Desabilitado',
                    name: 'rb-auto',
                    id: 'rdbDesabilitado',
                    inputValue: 2
                }]
            };

            // Toolbar da Grid
            var pagingToolbar = new Ext.PagingToolbar({
                pageSize: 50,
                store: gridFornecedorStore,
                displayInfo: true,
                displayMsg: App.Resources.Web.GridExibindoRegistros,
                emptyMsg: App.Resources.Web.GridSemRegistros,
                paramNames: {
                    start: "detalhesPaginacaoWeb.Start",
                    limit: "detalhesPaginacaoWeb.Limit"
                }
            });

            var sm = new Ext.grid.CheckboxSelectionModel({
                singleSelect: true,
                header: ''

            });
            // Toolbar da Grid
            var pagingToolbar = new Ext.PagingToolbar({
                pageSize: 50,
                store: gridFornecedorStore,
                displayInfo: true,
                displayMsg: App.Resources.Web.GridExibindoRegistros,
                emptyMsg: App.Resources.Web.GridSemRegistros,
                paramNames: {
                    start: "detalhesPaginacaoWeb.Start",
                    limit: "detalhesPaginacaoWeb.Limit"
                }
            });

            var cm = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true
                },
                columns: [
                    new Ext.grid.RowNumberer(),
                    { hideable: false, width: 10, renderer: function (val) { return '<div class="ux-row-action-item icon-edit" data-id="' + val + '"></div>'; }, dataIndex: 'IdFornecedor' },
                    { header: 'Fornecedor', dataIndex: "Nome", sortable: false, width: 100 },
                    { header: 'Local (Tipo de Serviço)', dataIndex: "ConcaLocalServico", sortable: false, width: 80 },
                    { header: 'Última Atualização', dataIndex: "DtRegistro", sortable: false, width: 60 },
                    { header: 'Usuário', dataIndex: "Login", sortable: false, width: 100 }
                ]
            });

            var gridFornecedor = new Ext.grid.EditorGridPanel({
                id: 'gridFornecedor',
                name: 'gridFornecedor',
                autoLoadGrid: true,
                height: 360,
                width: 870,
                autoLoadGrid: true,
                stripeRows: true,
                cm: cm,
                region: 'center',
                viewConfig: {
                    forceFit: true,
                    emptyText: 'Não possui dado(s) para exibição.'
                },
                autoScroll: true,
                loadMask: { msg: App.Resources.Web.Carregando },
                store: gridFornecedorStore,
                tbar: [
                    {
                        id: 'btnIncluir',
                        text: 'Incluir',
                        tooltip: 'Incluir',
                        //                        disabled: true,
                        iconCls: 'icon-new',
                        handler: function (c) {
                            Ext.getBody().mask("Processando dados...", "x-mask-loading");
                            var url = '<%= Url.Action("CriarFornecedor", "Fornecedor")%>';

                            window.location = url;
                        }
                    }
                ],
                bbar: pagingToolbar,
                sm: sm,
                listeners: {
                    cellclick: function (this_grid, rowIndex, columnIndex, e) {                        
                        //Verifica se usuario clicou no X(coluna 3)
                        if (columnIndex == 1) {
                            EditarFornecedor(rowIndex);
                        }
                    }
                }
            });

            gridFornecedor.getStore().proxy.on('beforeload', function (p, params) {
                var nomeFornecedor = Ext.getCmp("txtFornecedor").getRawValue();
                var local = Ext.getCmp("txtLocal").getRawValue();
                var idServico = Ext.getCmp("ddlTipoServico").getValue();
                var habilitado = Ext.getCmp("rdbHabilitado").getValue();                

                params['nomeFornecedor'] = nomeFornecedor;
                params['local'] = local;
                params['idServico'] = idServico;
                params['Ativo'] = (habilitado == true ? 'S' : 'N');
               
            });

            //******************************************************************//
            //***************************** LAYOUT *****************************//
            //******************************************************************//
            var formtxtFornecedor = {
                layout: 'form',
                width: 200,
                border: false,
                bodyStyle: 'padding: 5px',
                autoWidth: true,
                items: [txtFornecedor]
            };

            var formtxtLocal = {
                layout: 'form',
                width: 200,
                border: false,
                bodyStyle: 'padding: 5px',
                autoWidth: true,
                items: [txtLocal]
            };

            var formddlTipoServico = {
                layout: 'form',
                width: 200,
                border: false,
                bodyStyle: 'padding: 5px',
                autoWidth: true,
                items: [ddlTipoServico]
            };

            var formrdbHabilitarDesabilitar = {
                layout: 'form',
                width: 180,
                border: false,
                bodyStyle: 'padding: 5px',
                //autoWidth: true,
                items: [rdbHabilitarDesabilitar]
            };

            //        var c4 = {
            //            layout: 'form',
            //            border: false,
            //            bodyStyle: 'padding: 5px',
            //            autoWidth: true,
            //            items: [btnPesquisar]
            //        };

            //        var c5 = {
            //            layout: 'form',
            //            border: false,
            //            bodyStyle: 'padding: 5px',
            //            autoWidth: true,
            //            items: [btnLimpar]
            //        };

            //        var c6 = {
            //            layout: 'column',
            //            border: false,
            //            bodyStyle: 'padding: 5px',
            //            autoWidth: true,
            //            items: [c1,c2,c3]
            //        };

            //        var field2 = {
            //            layout: 'form',
            //            border: false,
            //            bodyStyle: 'padding: 5px',
            //            width: 180,
            //            items: [rdbHabilitarDesabilitar]
            //        };

            //        var formGrid = {
            //            layout: 'form',
            //            border: false,
            //            bodyStyle: 'padding: 5px',
            //            autoWidth: true,
            //            items: [gridFornecedor]
            //        };

            //Containers
            //        var containerLinha1 = {
            //            layout: 'form',
            //            border: true,
            //            items: [c6]
            //        };

            //        var containerLinha2 = {
            //            layout: 'form',
            //            border: true,
            //            items: [field2]
            //        };

            //        var containerLinh4 = {
            //            layout: 'form',
            //            border: true,
            //            items: [formGrid]
            //        };

            //Array de componentes do form 
            var arrCampos = {
                layout: 'column',
                border: false,
                items: [formtxtFornecedor, formtxtLocal, formddlTipoServico, formrdbHabilitarDesabilitar]
            };

            //******************************************************************//
            //***************************** FUNÇÕES*****************************//
            //******************************************************************//
            function PesquisarFornecedores() {
                var NomeFornecedor = Ext.getCmp("txtFornecedor").getValue();
                var Local = Ext.getCmp("txtLocal").getValue();
                var Servico = Ext.getCmp("ddlTipoServico").getValue();

                if (NomeFornecedor == "" && Local == "" && Servico == "") {
                    gridFornecedorStore.load({ params: { start: 0, limit: 50} });
                } else {
                    gridFornecedorStore.load({ params: { start: 0, limit: 50} });
                }
            }

            function LimpaCampos() {
                //Fornecedor
                Ext.getCmp('txtFornecedor').setValue("");
                //Local
                Ext.getCmp('txtLocal').setValue("");
                //Serviço
                Ext.getCmp('ddlTipoServico').clearValue();
                //Status
                $("#rdbHabilitado").attr('checked', 'checked');
                gridFornecedorStore.removeAll();
                //PesquisarFornecedores();
            }

            function EditarFornecedor(iCellEl) {
                var gridstore = gridFornecedor.getStore();
                var rowdata = gridstore.data.items[iCellEl];
                var idFornecedor = rowdata.data['IdFornecedorOs'];

                Ext.getBody().mask("Processando dados...", "x-mask-loading");

                var url = '<%= Url.Action("CriarFornecedor", "Fornecedor") %>';

                url += "?idFornecedor=" + idFornecedor;

                window.location = url;
            }

            //***************************** PAINEL *****************************//
            //Conteudo do Form que sera criado
            var panelFormFiltros = new Ext.form.FormPanel({
                id: 'panelForm',
                title: "Lista Fornecedores",
                region: 'center',
                bodyStyle: 'padding:15px;',
                layout: 'form',
                labelAlign: 'top',
                items: [arrCampos],
                buttonAlign: "center",
                buttons:
                    [{
                        text: 'Pesquisar',
                        type: 'submit',
                        iconCls: 'icon-find',
                        handler: function (b, e) {
                            PesquisarFornecedores();

                        }
                    },
                        {
                            text: 'Limpar',
                            type: 'submit',
                            iconCls: 'icon-del',
                            handler: function (b, e) {
                                LimpaCampos();
                            }
                        }
                    ]
            });

            new Ext.Viewport({
                layout: 'border',
                margins: 10,
                items: [
                    {
                        region: 'north',
                        height: 200,
                        autoScroll: true,
                        items: [{
                            region: 'center',
                            applyTo: 'header-content'
                        },
                            panelFormFiltros]
                    },
                    gridFornecedor
                ]
            });

            //***************************** RENDER *****************************//
            Ext.onReady(function () {
                //panelForm.render("divContent");
                PesquisarFornecedores();
                //Carrega GRID Fornecedores
                //gridFornecedor.load({ params: { start: 0, limit: 50} });
            });
        });
     

//        var panelForm = new Ext.form.FormPanel({
//            id: 'panelForm',
//            layout: 'form',
//            labelAlign: 'top',
//            border: false,
//            autoHeight: true,
//            title: "Lista Fornecedores",
//            region: 'center',
//            bodyStyle: 'padding: 15px',
//            items: [containerLinha1, containerLinha2, containerLinh4],
//            buttonAlign: "center"
//        });




//        gridOSLimpezaVagaoStore.load({ params: { start: 0, limit: 50} });
    
    </script>
</asp:Content>
<asp:Content ID="MainContent" ContentPlaceHolderID="Content" runat="server">
    <div id="header-content">
        <h1>Fornecedor</h1>
        <small>Você está em Fornecedor > Lista Fornecedores</small>
        <br />
    </div>
    <div id="divContent">
    </div>
</asp:Content>