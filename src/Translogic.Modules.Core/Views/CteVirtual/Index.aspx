﻿<%@ Import Namespace="System.Security.Policy" %>

<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Interna.Master" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Import Namespace="Translogic.Modules.Core.Domain.Model.Diversos.Cte" %>

<asp:Content runat="server" ID="HeadContent" ContentPlaceHolderID="HeadContent">
	<script type="text/javascript">
	
		var formModal = null;
		var grid = null;
		var lastRowSelected = -1;
		// var dataAtual = new String('<%= String.Format("{0:dd/MM/yyyy}", DateTime.Now) %>');

		// Flag para garantir que uma tela de mensagem que está sobreposta não chame novamente a tela de pesquisa
		var executarPesquisa = true;

		function showResult(btn) {
			executarPesquisa = true;
		}

		function FormSuccess(form, action) {
			ds.removeAll();
			ds.loadData(action.result, true);
		}

        function Pesquisar()
        {
            var diferenca = Ext.getCmp("filtro-data-final").getValue() - Ext.getCmp("filtro-data-inicial").getValue();
            diferenca = Math.floor(diferenca / (1000 * 60 * 60 * 24));

            if (Ext.getCmp("filtro-data-inicial").getValue() == '' || Ext.getCmp("filtro-data-final").getValue() == '') {
							executarPesquisa = false;
                Ext.Msg.show({
                    title: "Mensagem de Informação",
                    msg: "Preencha os filtro datas!",
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.INFO,
                    minWidth: 200,
										fn: showResult
                });
            } 
						else if (diferenca > 30) {
							executarPesquisa = false;
                Ext.Msg.show({
                    title: "Mensagem de Informação",
                    msg: "O período da pesquisa não deve ultrapassar 30 dias",
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.INFO,
                    minWidth: 200,
										fn: showResult
                });
            }
            else if (Ext.getCmp("filtro-serie").getValue() != '' && Ext.getCmp("filtro-despacho").getValue() == '') {
							executarPesquisa = false;
							Ext.Msg.show({
								title: "Mensagem de Informação",
								msg: "Não é possível filtrar apenas pela a Série do Despacho.<br/>Também é necessário preencher o número do Despacho.",
								buttons: Ext.Msg.OK,
								icon: Ext.MessageBox.INFO,
								minWidth: 200,
								fn: showResult
							});
            }
            else if (Ext.getCmp("filtro-despacho").getValue() != '' && Ext.getCmp("filtro-serie").getValue() == '') {
							executarPesquisa = false;
							Ext.Msg.show({
								title: "Mensagem de Informação",
								msg: "Não é possível filtrar apenas pelo número do Despacho.<br/>Também é necessário preencher a Série do Despacho.",
								buttons: Ext.Msg.OK,
								icon: Ext.MessageBox.INFO,
								minWidth: 200,
								fn: showResult
							});
            }
            else {
							executarPesquisa = true;
                grid.getStore().load();
            }
        }

		function FormError(form, action) {
			Ext.Msg.alert('Erro', action.result.Message);
			ds.removeAll();
		}

		var dsStatus = new Ext.data.ArrayStore({
			fields: ['value', 'text'],
			data: [['0', 'Ativo'],
               ['1', 'Inativo']]
		});

		/*
		// DataSource da combo de Erro
		var dsSituacaoCte = new Ext.data.ArrayStore({
			fields: ['Id', 'Descricao'],
			data: [
				['<%= Translogic.Core.Commons.Enum<SituacaoCteEnum>.GetDescriptionOf(SituacaoCteEnum.PendenteArquivoEnvio ) %>', 'Pendente Envio'],
				['<%=Translogic.Core.Commons.Enum<SituacaoCteEnum>.GetDescriptionOf(SituacaoCteEnum.Erro) %>', 'Erro'],
				['<%=Translogic.Core.Commons.Enum<SituacaoCteEnum>.GetDescriptionOf(SituacaoCteEnum.ErroAutorizadoReEnvio) %>', 'Erro Autorizado Reenvio'],
				['<%=Translogic.Core.Commons.Enum<SituacaoCteEnum>.GetDescriptionOf(SituacaoCteEnum.AutorizadoInutilizacao) %>', 'Cte Inutilizado'],
				['<%=Translogic.Core.Commons.Enum<SituacaoCteEnum>.GetDescriptionOf(SituacaoCteEnum.EnviadoArquivoEnvio) %>', 'Enviado Arquivo'],
				['<%=Translogic.Core.Commons.Enum<SituacaoCteEnum>.GetDescriptionOf(SituacaoCteEnum.Cancelado) %>', 'Cancelado'],
				['<%=Translogic.Core.Commons.Enum<SituacaoCteEnum>.GetDescriptionOf(SituacaoCteEnum.EnviadoFilaArquivoEnvio) %>', 'Enviado Fila']
			]
		});

		var cboSituacaoCte = {
			xtype: 'combo',
			store: dsSituacaoCte,
			allowBlank: true,
			lazyInit: false,
			lazyRender: false,
			mode: 'local',
			typeAhead: false,
			triggerAction: 'all',
			fieldLabel: 'Situação Cte',
			name: 'filtro-CodigoErro',
			id: 'filtro-CodigoErro',
			hiddenName: 'filtro-CodigoErro',
			displayField: 'Descricao',
			forceSelection: true,
			width: 150,
			valueField: 'Id',
			emptyText: 'Selecione...'
		};
		
		
		var ds = new Ext.data.JsonStore({
		root: "Items",
		fields: [
		'CteId',
		'SerieDesp5',
		'NumDesp5',
		'SerieDesp6',
		'NumDesp6',
		'CodVagao',
		'Fluxo',
		'ToneladaUtil',
		'Volume',
		'NroCte',
		'SerieCte',
		'ChaveCte',
		'DataEmissao',
		// 'Status'
		'CteComAgrupamentoNaoCancelado'
		]
		});
		*/
		// chamada ajax
		var url = '<%=Url.Action("Virtual") %>';

		function onVirtual(id) {
			url = url + "?cteId=" + id;
			document.location.href = url;
		}

		/* ------------- Ação CTRL+C nos campos da Grid ------------- */
		var isCtrl = false;
		document.onkeyup = function (e) {

			var keyID = event.keyCode;

			// 17 = tecla CTRL
			if (keyID == 17) {
				isCtrl = false; //libera tecla CTRL
			}
		}

		document.onkeydown = function (e) {

			var keyID = event.keyCode;

			// verifica se CTRL esta acionado
			if (keyID == 17) {
				isCtrl = true;
			}

		if ((keyID == 13) && (executarPesquisa)) {
			Pesquisar();
			return;
		}

			// 67 = tecla 'c'
			if (keyID == 67 && isCtrl == true) {

				// verifica se há selecão na tabela
				if (grid.getSelectionModel().hasSelection()) {
					// captura todas as linhas selecionadas
					var recordList = grid.getSelectionModel().getSelections();

					var a = [];
					var separator = '\t'; // separador para colunas no excel
					for (var i = 0; i < recordList.length; i++) {
						var s = '';
						var item = recordList[i].data;
						for (key in item) {
							if (key != 'Id') { //elimina o campo id da linha
								s = s + item[key] + separator;
							}
						}
						s = s.substr(0, s.length - 1); //retira o ultimo separador '\t' da linha
						a.push(s);
					}
					window.clipboardData.setData('Text', (a.join('\n'))); //insere linhas no clipBoard
				}
			}
		}
		/* ------------------------------------------------------- */

		statusCte = function (status) {

			if (status == 0) {
				return "Ativo";
			}
			else {
			    return "Inativo";
			}
		}

		$(function () {

		    var detalheAction = new Ext.ux.grid.RowActions
		({
		    dataIndex: '',
		    header: '',
		    align: 'center',
		    actions:
			[{
			    iconCls: 'icon-edit',
			    tooltip: 'CTe Virtual'
			}],
		    callbacks:
					{
					    'icon-edit': function (grid, record, action, row, col) {

                             Ext.Ajax.request({ 
			                        url: "<% =Url.Action("VerificarFluxoComercial") %>",
			                        success: function(data, opt) {
                                        var responseObj = Ext.decode(data.responseText);
                                        if (responseObj.Erro)
                                        {
                                            Ext.Msg.alert('Aviso', responseObj.Mensagem);
                                        }else{
											onVirtual(record.data.CteId);
										}
			                        },
                                    failure: function(conn, data){
                                        alert("Ocorreu um erro inesperado.");
                                    },
			                        method: "POST",
			                        params: { codigoFluxoComercial: record.data.Fluxo }
		                       });
					    }
				}
		});
				    
	var selectModel = new Ext.grid.CheckboxSelectionModel();

	var gridStore = new Ext.data.JsonStore({
			url: '<%= Url.Action("ObterCtesVirtual") %>',
			root: "Items",
			totalProperty: 'Total',
			remoteSort: true,
			paramNames: {
					sort: "pagination.Sort",
					dir: "pagination.Dir",
					start: "pagination.Start",
					limit: "pagination.Limit"
			},
			fields: [
							'CteId',
							'Serie',
							'Despacho',
							'CodVagao',
							'Fluxo',
							'ToneladaUtil',
							'Volume',
							'NroCte',
							'SerieCte',
							'ChaveCte',
							'DataEmissao',
							'CteComAgrupamentoNaoCancelado'
			]
	});

	function MostrarWindowErros(mensagensErro){
		var textarea = new Ext.form.TextArea({
				xtype:'textarea',
				fieldLabel: 'Foram encontrados os seguintes erros',
				value: mensagensErro,
				readOnly: true,
				height: 220,
				width: 395
		});

	var formErros = new Ext.FormPanel({
			id: 'formErros',
			bodyStyle: 'padding: 15px',
			labelAlign: 'top',
			items: [textarea],
			buttonAlign: 'center',
			buttons: [{
					text: 'Fechar',
					type: 'button',
					handler: function (b, e) {
							windowErros.close();
					}
			}]
	});

		windowErros = new Ext.Window({
			id: 'windowErros',
			title: 'Erros',
			modal: true,
			width: 444,
			height: 350,
			items:[formErros]
		});
		windowErros.show();
	}

		    var dsCodigoControle = new Ext.data.JsonStore({
		        root: "Items",
		        autoLoad: true,
		        url: '<%= Url.Action("ObterCodigoSerieDesp") %>',
		        fields: [
                    'Id',
                    'CodigoControle'
			    ]
		    });

		    var pagingToolbar = new Ext.PagingToolbar({
		        pageSize: 50,
		        store: gridStore,
		        displayInfo: true,
		        displayMsg: App.Resources.Web.GridExibindoRegistros,
		        emptyMsg: App.Resources.Web.GridSemRegistros,
		        paramNames: {
		            start: "pagination.Start",
		            limit: "pagination.Limit"
		        }
		    });

	grid = new Ext.grid.EditorGridPanel({
			viewConfig: {
					forceFit: true,
					getRowClass: MudaCor
			},
			stripeRows: true,
			region: 'center',
			columnLines: true,
			autoLoadGrid: true,
			store: gridStore,
			sm: selectModel,			
			loadMask: { msg: App.Resources.Web.Carregando },
			colModel: new Ext.grid.ColumnModel({
					defaults: {
							sortable: false
					},
					columns: [
						selectModel, 
						new Ext.grid.RowNumberer(),						
						detalheAction,
						{ dataIndex: "CteId", hidden: true },
						{ header: 'Série', dataIndex: "Serie", sortable: false },
						{ header: 'Despacho', dataIndex: "Despacho", sortable: false },
						{ header: 'Vagão', dataIndex: "CodVagao", sortable: false },
						{ header: 'Fluxo', dataIndex: "Fluxo", sortable: false },
						{ header: 'TU', dataIndex: "ToneladaUtil", sortable: false, renderer: Ext.util.Format.numberRenderer('0.000,00/i') },
						{ header: 'Volume', dataIndex: "Volume", sortable: false, renderer: Ext.util.Format.numberRenderer('0.000,00/i') },
						{ header: 'Nro CTe', dataIndex: "NroCte", sortable: false },
						{ header: 'Série CTe', dataIndex: "SerieCte", sortable: false },
						{ header: 'Chave', dataIndex: "ChaveCte", sortable: false, hidden: true },
						{ header: 'Data Emissão', dataIndex: "DataEmissao", sortable: false },
						{ dataIndex: "CteComAgrupamentoNaoCancelado", hidden: true }				
		]

			}),
			bbar: pagingToolbar,
			plugins: [detalheAction],
			listeners: {
				rowclick: function(grid, rowIndex, e) {
					if (selectModel.isSelected(rowIndex)) {
						if (rowIndex == lastRowSelected) {
							grid.getSelectionModel().deselectRow(rowIndex);
							lastRowSelected = -1;
						}
						else {
							lastRowSelected = rowIndex;
						}
					}
				}
			}
	});
				//---
		    grid.on('render', function () {
		        if (grid.autoLoadGrid)
		            grid.getStore().load({ params: { 'pagination.Start': 0, 'pagination.Limit': pagingToolbar.pageSize} });
		    });

		    grid.getStore().proxy.on('beforeload', function (p, params) {
		        params['filter[0].Campo'] = 'dataInicial';
		        params['filter[0].Valor'] = new Array(Ext.getCmp("filtro-data-inicial").getValue().format('d/m/Y') + " " + Ext.getCmp("filtro-data-inicial").getValue().format('H:i:s'));
		        params['filter[0].FormaPesquisa'] = 'Start';

		        params['filter[1].Campo'] = 'dataFinal';
		        params['filter[1].Valor'] = new Array(Ext.getCmp("filtro-data-final").getValue().format('d/m/Y') + " " + Ext.getCmp("filtro-data-inicial").getValue().format('H:i:s'));
		        params['filter[1].FormaPesquisa'] = 'Start';

		        params['filter[2].Campo'] = 'serie';
		        params['filter[2].Valor'] = Ext.getCmp("filtro-serie").getValue();
		        params['filter[2].FormaPesquisa'] = 'Start';

		        params['filter[3].Campo'] = 'despacho';
		        params['filter[3].Valor'] = Ext.getCmp("filtro-despacho").getValue();
		        params['filter[3].FormaPesquisa'] = 'Start';

		        params['filter[4].Campo'] = 'fluxo';
		        params['filter[4].Valor'] = Ext.getCmp("filtro-fluxo").getValue();
		        params['filter[4].FormaPesquisa'] = 'Start';

		        params['filter[5].Campo'] = 'chave';
		        params['filter[5].Valor'] = Ext.getCmp("filtro-Chave").getValue();
		        params['filter[5].FormaPesquisa'] = 'Start';

		        params['filter[6].Campo'] = 'Origem';
		        params['filter[6].Valor'] = Ext.getCmp("filtro-Ori").getValue();
		        params['filter[6].FormaPesquisa'] = 'Start';

		        params['filter[7].Campo'] = 'Destino';
		        params['filter[7].Valor'] = Ext.getCmp("filtro-Dest").getValue();
		        params['filter[7].FormaPesquisa'] = 'Start';

		        params['filter[8].Campo'] = 'Vagao';
		        params['filter[8].Valor'] = Ext.getCmp("filtro-numVagao").getValue();
		        params['filter[8].FormaPesquisa'] = 'Start'

		        params['filter[9].Campo'] = 'UfDcl';
		        params['filter[9].Valor'] = Ext.getCmp("filtro-UfDcl").getRawValue();
		        params['filter[9].FormaPesquisa'] = 'Start';
		    });

		    function MudaCor(row, index) {
		        if (row.data.CteComAgrupamentoNaoCancelado) {
		            return 'cor';
		        }
		    }

		    var dataAtual = new Date();

		    var dataInicial = {
		        xtype: 'datefield',
		        fieldLabel: 'Data Inicial',
		        id: 'filtro-data-inicial',
		        name: 'dataInicial',
		        width: 83,
		        allowBlank: false,
		        vtype: 'daterange',
		        endDateField: 'filtro-data-final',
		        hiddenName: 'dataInicial',
		        value: dataAtual
		    };

				var dataFinal = {
					xtype: 'datefield',
					fieldLabel: 'Data Final',
					id: 'filtro-data-final',
					name: 'dataFinal',
					width: 83,
					allowBlank: false,
					vtype: 'daterange',
					startDateField: 'filtro-data-inicial',
					hiddenName: 'dataFinal',
					value: dataAtual
				};

				var origem = {
					xtype: 'textfield',
					vtype: 'cteestacaovtype',
					style: 'text-transform: uppercase',
					id: 'filtro-Ori',
					fieldLabel: 'Origem',
					autoCreate: { tag: 'input', type: 'text', maxlength: '3', autocomplete: 'off' },
					name: 'Origem',
					allowBlank: true,
					maxLength: 3,
					width: 35,
					hiddenName: 'Origem'
				};

				var destino = {
					xtype: 'textfield',
					vtype: 'cteestacaovtype',
					style: 'text-transform: uppercase',
					id: 'filtro-Dest',
					fieldLabel: 'Destino',
					autoCreate: { tag: 'input', type: 'text', maxlength: '3', autocomplete: 'off' },
					name: 'Destino',
					allowBlank: true,
					maxLength: 3,
					width: 35,
					hiddenName: 'Destino'
				};

		    var serie = {
		        xtype: 'textfield',
				vtype: 'ctesdvtype',
		        style: 'text-transform: uppercase',
		        id: 'filtro-serie',
		        fieldLabel: 'Série',
		        name: 'serie',
		        allowBlank: true,
		        autoCreate: { tag: 'input', type: 'text', maxlength: '3', autocomplete: 'off' },
		        maxLength: 20,
		        width: 30,
		        hiddenName: 'serie'
		    };

		    var despacho = {
		        xtype: 'textfield',
				vtype: 'ctedespachovtype',
		        id: 'filtro-despacho',
		        fieldLabel: 'Despacho',
		        name: 'despacho',
		        allowBlank: true,
		        maxLength: 20,
		        autoCreate: { tag: 'input', type: 'text', maxlength: '6', autocomplete: 'off' },
		        width: 55,
		        hiddenName: 'despacho'
		    };

		    var fluxo = {
					xtype: 'textfield',
					vtype: 'ctefluxovtype',
					style: 'text-transform: uppercase',
					id: 'filtro-fluxo',
					fieldLabel: 'Fluxo',
					name: 'fluxo',
					allowBlank: true,
					autoCreate: { tag: 'input', type: 'text', maxlength: '5', autocomplete: 'off' },
					maxLength: 20,
					width: 45,
					maxValue: 99999,
					minValue: 0,
					hiddenName: 'fluxo'
				};

		    var cboCteCodigoControle = {
		        xtype: 'combo',
		        store: dsCodigoControle,
		        allowBlank: true,
		        lazyInit: false,
		        lazyRender: false,
		        mode: 'local',
		        typeAhead: false,
		        triggerAction: 'all',
		        fieldLabel: 'UF DCL',
		        name: 'filtro-UfDcl',
		        id: 'filtro-UfDcl',
		        hiddenName: 'filtro-UfDcl',
		        displayField: 'CodigoControle',
		        forceSelection: true,
		        width: 70,
		        valueField: 'Id',
		        emptyText: 'Selecione...',
						editable: false,
		        tpl: '<tpl for="."><div class="x-combo-list-item">{CodigoControle}&nbsp;</div></tpl>'
		    };

		    var Vagao = {
		        xtype: 'textfield',
				vtype: 'ctevagaovtype',
		        style: 'text-transform: uppercase',
		        id: 'filtro-numVagao',
		        fieldLabel: 'Vagão',
		        name: 'numVagao',
		        allowBlank: true,
		        autoCreate: { tag: 'input', type: 'text', maxlength: '7', autocomplete: 'off' },
		        maxLength: 20,
		        width: 55,
		        hiddenName: 'numVagao'
		    };

			var chave = {
					xtype: 'textfield',
					vtype: 'ctevtype',
					id: 'filtro-Chave',
					fieldLabel: 'Chave CTe',
					name: 'Chave',
					autoCreate: { tag: 'input', type: 'text', maxlength: '44', autocomplete: 'off' },
					allowBlank: true,
					maxLength: 50,
					width: 275,
					hiddenName: 'Chave  '
		    };

		    var arrDataIni = {
		        width: 87,
		        layout: 'form',
		        border: false,
		        items:
				[dataInicial]
		    };

		    var arrDataFim = {
		        width: 87,
		        layout: 'form',
		        border: false,
		        items:
						[dataFinal]
		    };

		    var arrOrigem = {
		        width: 43,
		        layout: 'form',
		        border: false,
		        items:
						[origem]
		    };
		    var arrDestino = {
		        width: 43,
		        layout: 'form',
		        border: false,
		        items:
						[destino]
		    };

		    var arrSerie = {
		        width: 37,
		        layout: 'form',
		        border: false,
		        items:
								[serie]
		    };

		    var arrDespacho = {
		        width: 60,
		        layout: 'form',
		        border: false,
		        items:
						[despacho]
		    };

		    var arrFluxo = {
		        width: 50,
		        layout: 'form',
		        border: false,
		        items:
						[fluxo]
		    };

		    var arrVagao = {
		        width: 60,
		        layout: 'form',
		        border: false,
		        items:
				[Vagao]
		    };

		    var arrChave = {
		        width: 280,
		        layout: 'form',
		        border: false,
		        items:
						[chave]
		    };

		    var arrCodigoDcl = {
		        width: 75,
		        layout: 'form',
		        border: false,
		        items:
					[cboCteCodigoControle]
		    };


		    var arrlinha1 = {
		        layout: 'column',
		        border: false,
		        items: [arrDataIni, arrDataFim, arrFluxo, arrChave]
		    };

		    var arrlinha2 = {
		        layout: 'column',
		        border: false,
		        items: [arrCodigoDcl, arrSerie, arrDespacho, arrOrigem, arrDestino, arrVagao]
		    };

		    var arrCampos = new Array();
		    arrCampos.push(arrlinha1);
		    arrCampos.push(arrlinha2);

		    filters = new Ext.form.FormPanel({
		        id: 'grid-filtros',
		        title: "Filtros",
		        region: 'center',
		        bodyStyle: 'padding: 15px',
		        labelAlign: 'top',
		        items:
				[arrCampos],
		        /*[{
		        layout: 'column',
		        border: false,
		        items: [{
		        width: 130,
		        layout: 'form',
		        border: false,
		        items:
		        [
		        {
		        xtype: 'datefield',
		        fieldLabel: 'Data Inicial',
		        id: 'filtro-data-inicial',
		        name: 'dataInicial',
		        width: 100,
		        allowBlank: false,
		        vtype: 'daterange',
		        endDateField: 'filtro-data-final',
		        hiddenName: 'dataInicial'
		        // ,value: dataAtual
		        }
		        ,
		        {

		        xtype: 'numberfield',
		        id: 'filtro-nro-cte',
		        fieldLabel: 'Nro CTE',
		        name: 'nroCte',
		        allowBlank: true,
		        maxLength: 20,
		        width: 100,
		        hiddenName: 'nroCte'
		        }
		        ]
		        },
		        {
		        width: 130,
		        layout: 'form',
		        border: false,
		        items:
		        [
		        {
		        xtype: 'datefield',
		        fieldLabel: 'Data Final',
		        id: 'filtro-data-final',
		        name: 'dataFinal',
		        width: 100,
		        allowBlank: false,
		        vtype: 'daterange',
		        startDateField: 'filtro-data-inicial',
		        hiddenName: 'dataFinal'
		        // ,value: dataAtual
		        }
		        ,
		        {
		        xtype: 'numberfield',
		        id: 'filtro-serie_cte',
		        fieldLabel: 'Série CTE',
		        name: 'serieCte',
		        allowBlank: true,
		        maxLength: 20,
		        width: 100,
		        hiddenName: 'serieCte'
		        }
		        ]
		        },
		        {
		        width: 130,
		        layout: 'form',
		        border: false,
		        items:
		        [
		        {
		        xtype: 'numberfield',
		        id: 'filtro-serie',
		        fieldLabel: 'Série',
		        name: 'serie',
		        allowBlank: true,
		        maxLength: 20,
		        width: 100,
		        hiddenName: 'serie'
		        }
		        ,
		        {

		        /*id: 'filtro-status',
		        xtype: 'combo',
		        name: 'status',
		        fieldLabel: 'Status',
		        store: dsStatus,
		        displayField: 'text',
		        valueField: 'value',
		        forceSelection: false,
		        mode: 'local',
		        emptyText: 'Selecione...',
		        triggerAction: 'all',
		        allowBlank: false,
		        width: 100,
		        editable: false,
		        hiddenName: 'status'

		        xtype: 'numberfield',
		        id: 'filtro-despacho',
		        fieldLabel: 'Despacho',
		        name: 'despacho',
		        allowBlank: true,
		        maxLength: 20,
		        width: 100,
		        hiddenName: 'despacho'
		        }
		        ]
		        },
		        {
		        width: 230,
		        layout: 'form',
		        border: false,
		        items:
		        [
		        {
		        xtype: 'textfield',
		        id: 'filtro-chave',
		        fieldLabel: 'Chave',
		        name: 'chave',
		        allowBlank: true,
		        maxLength: 44,
		        width: 200,
		        hiddenName: 'chave'
		        }
		        ,
		        {
		        xtype: 'textfield',
		        id: 'filtro-vagao',
		        fieldLabel: 'Vagão',
		        name: 'vagao',
		        allowBlank: true,
		        maxLength: 20,
		        width: 100,
		        hiddenName: 'vagao'
		        }

		        ]
		        }
		        /*,
		        {
		        width: 230,
		        layout: 'form',
		        border: false,
		        items:
		        [
		        {  
                               
		        }
		        ]
		        }
		        ]

		        }],*/
		        buttonAlign: "center",
		        buttons:
	        [
                {
                    text: 'Pesquisar',
                    type: 'submit',
                    iconCls: 'icon-find',
                    handler: function (b, e) {
                        Pesquisar();
                    }
                }
		        /*,
		        {
		        text: 'Salvar',
		        name: 'salvar',
		        id: 'salvar',
		        iconCls: 'icon-save',
		        handler: function(b, e) {
    		                    
		        var listaModificados = ds.getModifiedRecords();

		        if (listaModificados.length == 0) {
                            
		        Ext.Msg.show({
		        title: "Erro",
		        msg: "Nenhum CTE modificado!",
		        buttons: Ext.Msg.OK,
		        icon: Ext.MessageBox.ERROR,
		        minWidth: 200
		        });
		        return false;

		        }

		        var listaEnvio = Array();

		        for (var i = 0; i < listaModificados.length; i++) {
                            
		        listaEnvio.push(listaModificados[i].data);
		        }

		        if (Ext.Msg.confirm("Confirmação", "Deseja realmente alterar o Status do Cte?", function(btn, text) {
		        if (btn == 'yes') {
		        var dados = $.toJSON(listaEnvio);
		        $.ajax({
		        url: "<%= Url.Action("Salvar") %>",
		        type: "POST",
		        dataType: 'json',
		        data: dados,
		        contentType: "application/json; charset=utf-8",
		        success: function(result) {
		        if (result.success)
		        {
		        ds.commitChanges();
		        }else{
		        Ext.Msg.alert('Ops...', result.Message);
		        }
		        }
		        ,
		        failure: function(result) {
		        Ext.Msg.alert('OPS...', result.Message);
		        }
		        });
		        }
		        else
		        {
		        return false;
		        }
		        })); 
		        }
		        }*/
                ,
	            {
								text: 'Limpar',
								handler: function (b, e) {
									
									grid.getStore().removeAll();
									grid.getStore().totalLength = 0;
									grid.getBottomToolbar().bind(grid.getStore());							 
									grid.getBottomToolbar().updateInfo();  
									Ext.getCmp("grid-filtros").getForm().reset();
								},
								scope: this
	            }
            ]
		    });

		    new Ext.Viewport({
		        layout: 'border',
		        margins: 10,
		        items: [
			{
			    region: 'north',
			    height: 230,
			    items: [{
			        region: 'center',
			        applyTo: 'header-content'
			    },
				filters]
			},
			grid
		]

		    });



		});
	</script>
	<style>
		.cor
		{
			background-color: #FFEEDD;
		}
	</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
	<div id="header-content">
		<h1>
			CTe Virtual</h1>
		<small>Você está em CTe > Cte Virtual</small>
		<br />
	</div>
</asp:Content>
