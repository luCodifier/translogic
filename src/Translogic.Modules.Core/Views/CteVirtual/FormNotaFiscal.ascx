﻿   <%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div id="div-form-nota-fiscal"></div>
<script type="text/javascript">
    function ValidarFormularioNota() {
        var erroForm = false;

        erroForm = !Ext.getCmp("formNotaFiscal").getForm().isValid();

        if (!ValidarCampoMaiorZero("edValorTotalNota")) erroForm = true;

        /*if (indPreenchimentoVolume){
            /// A FUNÇÃO ValidarCampoMaiorZero ESTÁ DEFINIDA NO ARQUIVO Detalhe.aspx
            if (!ValidarCampoMaiorZero("edVolumeNotaFiscal")) erroForm = true;
            
            if (parseFloat(Ext.getCmp("edVolumeNotaFiscal").getValue()) <= 0){
                Ext.getCmp("edVolumeNotaFiscal").markInvalid("O volume deve ser maior que zero.");
                erroForm = true;
            }
        }
        else
        {
            /// A FUNÇÃO ValidarCampoMaiorZero ESTÁ DEFINIDA NO ARQUIVO Detalhe.aspx
            if (!ValidarCampoMaiorZero("edPesoTotal")) erroForm = true;
            if (!ValidarCampoMaiorZero("edPesoRateio")) erroForm = true;
            
            if (Ext.getCmp("edPesoRateio").isValid() && Ext.getCmp("edPesoTotal").isValid() && parseFloat(Ext.getCmp("edPesoTotal").getValue()) < parseFloat(Ext.getCmp("edPesoRateio").getValue())){
                Ext.getCmp("edPesoRateio").markInvalid("O peso rateio deve ser menor que o peso total.");
                erroForm = true;
            }
        }


        if (Ext.getCmp("edDataNota").isValid() && Ext.getCmp("edDataNota").getValue()<dataLimiteNf){
            Ext.getCmp("edDataNota").markInvalid("A Data Nota Fiscal não pode ultrapassar de " + mesesRetroativos + " meses.");
            erroForm = true;
        }

        var valorConteiner = Ext.getCmp("edConteiner").getValue();
        if (Ext.getCmp("edConteiner").isValid() && valorConteiner != ""){
            if (!VerificarConteiner(valorConteiner)){
                Ext.getCmp("edConteiner").markInvalid("O conteiner não existe.");
                erroForm = true;
            }
        }*/

        if (erroForm) {
            var mensagemErro = "Foram encontrados os erros abaixo:<br />";
            Ext.getCmp("formNotaFiscal").getForm().items.each(function (f) {
                if (f.activeError != undefined) {
                    mensagemErro += " - " + f.fieldLabel + ": " + f.activeError + "<br />";
                }
            });

            Ext.Msg.alert('Erro de validação', mensagemErro);
            return false;
        }

        return true;
    }

    function CarregarDadosCnpjEdicao(val, idAreaOperacional, sufixo) {
        if (val == "" || val == undefined || val == null) {
            return false;
        }

        Ext.Ajax.request({
            url: "<% =Url.Action("ObterDadosCnpj") %>",
            success: function (response) {
                var data = Ext.decode(response.responseText);
                if (!data.Erro) {
                    Ext.getCmp("edInscricaoEstadual" + sufixo).setValue(data.Dados.InscricaoEstadual);
                    Ext.getCmp("ed" + sufixo).setValue(data.Dados.Nome);
                    Ext.getCmp("edSigla" + sufixo).setValue(data.Dados.Sigla);
                    Ext.getCmp("edUf" + sufixo).setValue(data.Dados.Uf);
                } else {
                    Ext.Msg.alert('Erro', data.Mensagem);
                    Ext.getCmp("edInscricaoEstadual" + sufixo).setValue("");
                    Ext.getCmp("ed" + sufixo).setValue("");
                    Ext.getCmp("edSigla" + sufixo).setValue("");
                    Ext.getCmp("edUf" + sufixo).setValue("");
                }
            },
            failure: function (conn, data) {
                Ext.getCmp("edInscricaoEstadual" + sufixo).setValue("");
                Ext.getCmp("ed" + sufixo).setValue("");
                Ext.getCmp("edSigla" + sufixo).setValue("");
                Ext.getCmp("edUf" + sufixo).setValue("");
                alert("Ocorreu um erro inesperado");
            },
            method: "POST",
            params: { cnpj: val, codigoFluxo: codigoFluxo, idAreaOperacional: idAreaOperacional }
        });
    }

    /**********************************************************
	FORM DADOS DAS NOTAS FISCAIS - INICIO
    ChaveNfe
    Conteiner
    SerieNotaFiscal
    NumeroNotaFiscal
    PesoTotal
    PesoRateio
    ValorNotaFiscal
    Remetente
    Destinatario
    CnpjRemetente
    CnpjDestinatario
    TIF
    DataNotaFiscal
    SiglaRemetente
    SiglaDestinatario
	**********************************************************/
    var arrLinhas = new Array();

    var edNfChaveNfe = {
        xtype: 'textfield',
        id: 'edNfChaveNfe',
        fieldLabel: 'ChaveNfe',
        name: 'edNfChaveNfe',
        width: 393
    };

    var l1_coluna1 = { width: 415, layout: 'form', border: false, items: [edNfChaveNfe] };

    var linha1 = {
        layout: 'column',
        border: false,
        items: [l1_coluna1]
    };
    arrLinhas.push(linha1);

    var edSerieNota = {
        xtype: 'textfield',
        id: 'edSerieNota',
        fieldLabel: 'Série',
        name: 'edSerieNota',
        allowBlank: false,
        width: 40
    };

    var edNumeroNota = {
        xtype: 'textfield',
        id: 'edNumeroNota',
        fieldLabel: 'Número',
        name: 'edNumeroNota',
        allowBlank: false,
        width: 60
    };

    var edDataNota = {
        xtype: 'datefield',
        id: 'edDataNota',
        fieldLabel: 'Data',
        name: 'edDataNota',
        allowBlank: false,
        width: 90
    };


    if (indPreenchimentoVolume) {
        var edVolumeNotaFiscal = {
            xtype: 'masktextfield',
            id: 'edVolumeNotaFiscal',
            fieldLabel: 'Volume',
            name: 'edVolumeNotaFiscal',
            allowBlank: false,
            width: 50,
            mask: '990,000',
            money: true,
            maxLength: 7
        };

        var l2_coluna1 = { width: 50, layout: 'form', border: false, items: [edSerieNota] };
        var l2_coluna2 = { width: 70, layout: 'form', border: false, items: [edNumeroNota] };
        var l2_coluna3 = { width: 100, layout: 'form', border: false, items: [edDataNota] };
        var l2_coluna4 = { width: 80, layout: 'form', border: false, items: [edVolumeNotaFiscal] };

        var linha2 = {
            layout: 'column',
            border: false,
            items: [l2_coluna1, l2_coluna2, l2_coluna3, l2_coluna4]
        };
    } else {
        var edPesoTotal = {
            xtype: 'masktextfield',
            id: 'edPesoTotal',
            fieldLabel: 'Peso Disponível',
            name: 'edPesoTotal',
            width: 90,
            mask: '990,000',
            money: true,
            maxLength: 7,
            value: "0,000",
            listeners: {
                'blur': function (field) {
                    validaPesoRateio();
                }
            }
        };

        var edPesoRateio = {
            xtype: 'masktextfield',
            id: 'edPesoRateio',
            fieldLabel: 'Peso Rateio',
            allowBlank: false,
            name: 'edPesoRateio',
            width: 70,
            mask: '990,000',
            money: true,
            maxLength: 7,
            value: "0,000",
            listeners: {
                'blur': function (field) {
                    validaPesoRateio();
                }
            }
        };

        var l2_coluna1 = { width: 50, layout: 'form', border: false, items: [edSerieNota] };
        var l2_coluna2 = { width: 70, layout: 'form', border: false, items: [edNumeroNota] };
        var l2_coluna3 = { width: 100, layout: 'form', border: false, items: [edDataNota] };
        var l2_coluna4 = { width: 98, layout: 'form', border: false, items: [edPesoTotal] };
        var l2_coluna5 = { width: 80, layout: 'form', border: false, items: [edPesoRateio] };

        var linha2 = {
            layout: 'column',
            border: false,
            items: [l2_coluna1, l2_coluna2, l2_coluna3, l2_coluna4, l2_coluna5]
        };
    }


    arrLinhas.push(linha2);

    var edValorTotalNota = {
        xtype: 'masktextfield',
        id: 'edValorTotalNota',
        fieldLabel: 'Valor Total',
        name: 'edValorTotalNota',
        allowBlank: false,
        width: 70,
        mask: '#9.999.990,00',
        money: true,
        maxLength: 13,
        value: "0,00",
        listeners: {
            'blur': function (field) {
                if (indPreenchimentoVolume) {
                    Ext.getCmp("edValorNota").setValue(field.getValue());
                }
            }
        }
    };

    var edValorNota = {
        xtype: 'masktextfield',
        id: 'edValorNota',
        fieldLabel: 'Valor Rateio',
        name: 'edValorNota',
        allowBlank: false,
        width: 70,
        mask: '#9.999.990,00',
        money: true,
        maxLength: 13,
        value: "0,00"/*,
        disabled: indPreenchimentoVolume*/
    };

    var edConteiner = {
        xtype: 'textfield',
        id: 'edConteiner',
        fieldLabel: 'Conteiner',
        name: 'edConteiner',
        maskRe: /[a-zA-Z0-9]/,
        autoCreate: { tag: 'input', type: 'text', maxlength: '11', autocomplete: 'off' },
        width: 70,
        allowBlank: !indFluxoConteiner,
        enableKeyEvents: true,
        listeners: {
            'blur': function (field) {
                VerificarConteiner(field.getValue());
            }
        }
    };

    var l3_coluna1 = { width: 100, layout: 'form', border: false, items: [edValorTotalNota] };
    var l3_coluna2 = { width: 100, layout: 'form', border: false, items: [edValorNota] };
    var l3_coluna3 = { width: 100, layout: 'form', border: false, items: [edConteiner] };

    var arrLinha3 = new Array();
    arrLinha3.push(l3_coluna1);
    arrLinha3.push(l3_coluna2);
    arrLinha3.push(l3_coluna3);
    var linha3 = {
        layout: 'column',
        border: false,
        items: arrLinha3
    };

    arrLinhas.push(linha3);

    var arrLinha4 = new Array();

    if (indFluxoInternacional) {
        var edTif = {
            xtype: 'textfield',
            id: 'edTif',
            fieldLabel: 'TIF',
            name: 'edTif',
            allowBlank: false,
            width: 70
        };
        var l4_coluna1 = { width: 100, layout: 'form', border: false, items: [edTif] };
        arrLinha4.push(l4_coluna1);
    }

    var linha4 = {
        layout: 'column',
        border: false,
        items: arrLinha4
    };

    if (arrLinha4.length > 0) {
        arrLinhas.push(linha4);
    }

    var fieldsetRemetente = {
        xtype: 'fieldset',
        title: 'Remetente',
        collapsible: false,
        width: 190,
        height: 270,
        defaults: { width: 130 },
        defaultType: 'textfield',
        items: [{
            fieldLabel: 'CNPJ',
            name: 'edCnpjRemetente',
            id: 'edCnpjRemetente',
            allowBlank: false,
            listeners: {
                change: function (field, newValue, oldValue) {
                    if (newValue != "") {
                        CarregarDadosCnpjEdicao(newValue, idOrigem, 'Remetente');
                    }
                }
            }
        }, {
            fieldLabel: 'Sigla',
            name: 'edSiglaRemetente',
            id: 'edSiglaRemetente',
            disabled: true
        }, {
            fieldLabel: 'Nome',
            name: 'edRemetente',
            id: 'edRemetente',
            disabled: true
        }, {
            fieldLabel: 'UF',
            name: 'edUfRemetente',
            id: 'edUfRemetente',
            disabled: true
        }, {
            fieldLabel: 'Inscrição Estadual',
            name: 'edInscricaoEstadualRemetente',
            id: 'edInscricaoEstadualRemetente',
            disabled: true
        }]
    };
    var l5_coluna1 = { width: 180, layout: 'form', border: false, items: [fieldsetRemetente] };

    var fieldsetDestinatario = {
        xtype: 'fieldset',
        title: 'Destinatário',
        collapsible: false,
        width: 170,
        height: 270,
        defaults: { width: 110 },
        defaultType: 'textfield',
        items: [{
            fieldLabel: 'CNPJ',
            name: 'edCnpjDestinatario',
            id: 'edCnpjDestinatario',
            allowBlank: false,
            listeners: {
                change: function (field, newValue, oldValue) {
                    if (newValue != "") {
                        CarregarDadosCnpjEdicao(newValue, idDestino, 'Destinatario');
                    }
                }
            }
        }, {
            fieldLabel: 'Sigla',
            name: 'edSiglaDestinatario',
            id: 'edSiglaDestinatario',
            disabled: true
        }, {
            fieldLabel: 'Nome',
            name: 'edDestinatario',
            id: 'edDestinatario',
            disabled: true
        }, {
            fieldLabel: 'UF',
            name: 'edUfDestinatario',
            id: 'edUfDestinatario',
            disabled: true
        }, {
            fieldLabel: 'Inscrição Estadual',
            name: 'edInscricaoEstadualDestinatario',
            id: 'edInscricaoEstadualDestinatario',
            disabled: true
        }]
    };
    var l5_coluna2 = { width: 180, layout: 'form', border: false, items: [fieldsetDestinatario] };

    var linha5 = {
        layout: 'column',
        border: false,
        items: [l5_coluna1, l5_coluna2]
    };
    arrLinhas.push(linha5);

    var formNotaFiscal = new Ext.form.FormPanel
        ({
            id: 'formNotaFiscal',
            labelWidth: 80,
            width: 430,
            height: 500,
            bodyStyle: 'padding: 15px',
            labelAlign: 'top',
            items:
                [
                    arrLinhas
                ],
            buttonAlign: "center",
            buttons:
                [{
                    text: "Salvar",
                    handler: function () {
                        if (ValidarFormularioNota()) {
                            recordEdit.data.ChaveNfe = Ext.getCmp("edNfChaveNfe").getValue();
                            recordEdit.data.Conteiner = Ext.getCmp("edConteiner").getValue();
                            recordEdit.data.SerieNotaFiscal = Ext.getCmp("edSerieNota").getValue();
                            recordEdit.data.NumeroNotaFiscal = Ext.getCmp("edNumeroNota").getValue();
                            recordEdit.data.ValorNotaFiscal = Ext.getCmp("edValorNota").getValue();
                            recordEdit.data.ValorTotalNotaFiscal = Ext.getCmp("edValorTotalNota").getValue();
                            recordEdit.data.Remetente = Ext.getCmp("edRemetente").getValue();
                            recordEdit.data.Destinatario = Ext.getCmp("edDestinatario").getValue();
                            recordEdit.data.CnpjRemetente = Ext.getCmp("edCnpjRemetente").getValue();
                            recordEdit.data.CnpjDestinatario = Ext.getCmp("edCnpjDestinatario").getValue();
                            recordEdit.data.DataNotaFiscal = Ext.getCmp("edDataNota").getValue();
                            recordEdit.data.SiglaRemetente = Ext.getCmp("edSiglaRemetente").getValue();
                            recordEdit.data.SiglaDestinatario = Ext.getCmp("edSiglaDestinatario").getValue();
                            recordEdit.data.UfRemetente = Ext.getCmp("edUfRemetente").getValue();
                            recordEdit.data.UfDestinatario = Ext.getCmp("edUfDestinatario").getValue();
                            recordEdit.data.InscricaoEstadualRemetente = Ext.getCmp("edInscricaoEstadualRemetente").getValue();
                            recordEdit.data.InscricaoEstadualDestinatario = Ext.getCmp("edInscricaoEstadualDestinatario").getValue();

                            if (indPreenchimentoVolume) {
                                recordEdit.data.VolumeNotaFiscal = Ext.getCmp("edVolumeNotaFiscal").getValue();
                            } else {
                                recordEdit.data.PesoTotal = Ext.getCmp("edPesoTotal").getValue();
                                recordEdit.data.PesoRateio = Ext.getCmp("edPesoRateio").getValue();
                            }

                            if (indFluxoInternacional) {
                                recordEdit.data.TIF = Ext.getCmp("edTif").getValue();
                            }

                            recordEdit.commit();
                            recordEdit = null;
                            windowNotaFiscal.close();
                        }
                    }
                },
                {
                    text: "Cancelar",
                    handler: function () {
                        windowNotaFiscal.close();
                    }
                }]
        });

    // VERIFICA SE É EDIÇÃO DA NOTA FISCAL
    if (recordEdit != null) {
        // ATUALIZA OS CAMPOS 
        AtualizarCampoNotaFiscal("edNfChaveNfe", recordEdit.data.ChaveNfe);
        AtualizarCampoNotaFiscal("edConteiner", recordEdit.data.Conteiner);
        AtualizarCampoNotaFiscal("edSerieNota", recordEdit.data.SerieNotaFiscal);
        AtualizarCampoNotaFiscal("edNumeroNota", recordEdit.data.NumeroNotaFiscal);

        if (indPreenchimentoVolume) {
            AtualizarCampoNotaFiscal("edVolumeNotaFiscal", recordEdit.data.VolumeNotaFiscal);
        } else {
            AtualizarCampoNotaFiscal("edPesoTotal", recordEdit.data.PesoTotal);
            AtualizarCampoNotaFiscal("edPesoRateio", recordEdit.data.PesoRateio);
        }

        AtualizarCampoNotaFiscal("edValorTotalNota", recordEdit.data.ValorTotalNotaFiscal);
        AtualizarCampoNotaFiscal("edValorNota", recordEdit.data.ValorNotaFiscal);
        AtualizarCampoNotaFiscal("edRemetente", recordEdit.data.Remetente);
        AtualizarCampoNotaFiscal("edDestinatario", recordEdit.data.Destinatario);
        AtualizarCampoNotaFiscal("edCnpjRemetente", recordEdit.data.CnpjRemetente);
        AtualizarCampoNotaFiscal("edCnpjDestinatario", recordEdit.data.CnpjDestinatario);
        if (recordEdit.data.DataNotaFiscal != null && recordEdit.data.DataNotaFiscal != "") {
            AtualizarCampoNotaFiscal("edDataNota", recordEdit.data.DataNotaFiscal.format('d/m/Y'));
        }
        AtualizarCampoNotaFiscal("edSiglaRemetente", recordEdit.data.SiglaRemetente);
        AtualizarCampoNotaFiscal("edSiglaDestinatario", recordEdit.data.SiglaDestinatario);
        AtualizarCampoNotaFiscal("edUfRemetente", recordEdit.data.UfRemetente);
        AtualizarCampoNotaFiscal("edUfDestinatario", recordEdit.data.UfDestinatario);
        AtualizarCampoNotaFiscal("edInscricaoEstadualRemetente", recordEdit.data.InscricaoEstadualRemetente);
        AtualizarCampoNotaFiscal("edInscricaoEstadualDestinatario", recordEdit.data.InscricaoEstadualDestinatario);

        if (indFluxoInternacional) {
            AtualizarCampoNotaFiscal("edTif", recordEdit.data.TIF);
        }

        Ext.getCmp("edNfChaveNfe").disable();
        if (recordEdit.data.IndObtidoAutomatico) {
            Ext.getCmp("edSerieNota").disable();
            Ext.getCmp("edNumeroNota").disable();

            /* if (indPreenchimentoVolume){
                 Ext.getCmp("edVolumeNotaFiscal").disable();
             }else{
                 Ext.getCmp("edPesoTotal").disable();
             }
             
             Ext.getCmp("edValorNota").disable();*/

            Ext.getCmp("edCnpjRemetente").disable();
            Ext.getCmp("edCnpjDestinatario").disable();
            Ext.getCmp("edDataNota").disable();
            // Ext.getCmp("edValorTotalNota").disable();
        } else {
            if (indValidarCnpjRemetente) {
                if (recordEdit.data.CnpjRemetente == "") {
                    Ext.getCmp("edCnpjRemetente").setValue(cnpjRemetenteFiscal);
                    CarregarDadosCnpjEdicao(cnpjRemetenteFiscal, idOrigem, 'Remetente');
                }
                Ext.getCmp("edCnpjRemetente").disable();
            }

            if (indValidarCnpjDestinatario) {
                if (recordEdit.data.CnpjDestinatario == "") {
                    Ext.getCmp("edCnpjDestinatario").setValue(cnpjDestinatarioFiscal);
                    CarregarDadosCnpjEdicao(cnpjDestinatarioFiscal, idDestino, 'Destinatario');
                }
                Ext.getCmp("edCnpjDestinatario").disable();
            }
        }
    }

    function AtualizarCampoNotaFiscal(campo, valor, mask) {
        if (valor != null && valor != "") {
            if (mask != null && mask != "") {
                valor = Ext.util.Format.number(valor, mask);
            }

            Ext.getCmp(campo).setValue(valor);
        }
    }

	/**********************************************************
	FORM DADOS DAS NOTAS FISCAIS - FIM
	**********************************************************/
    formNotaFiscal.render("div-form-nota-fiscal");

</script>
