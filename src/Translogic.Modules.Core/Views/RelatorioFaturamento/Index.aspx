﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Interna.Master" Inherits="System.Web.Mvc.ViewPage" %>
<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        //***************************** VARIÁVEIS E INICIALIZAÇÃO**************//
        //        var idOs = '<%=ViewData["idOs"] %>';
        //        var dataHora = '<%=ViewData["data"] %>';
        //        var local = '<%=ViewData["local"] %>';
        // var local = "LMG";
        //        $(function () {

        //            //Setar campos recuperados da OS
        //                        Ext.getCmp("txtNumOs").setValue(idOs);
        //                        Ext.getCmp("txtDataHora").setValue(dataHora);
        //                        Ext.getCmp("txtLocal").setValue(local);
        //        });

        //******************************************************************//
        //***************************** STORE ******************************//
        //******************************************************************//
        var gridFaturamentoStore = new Ext.data.JsonStore({
            root: "Items",
            totalProperty: 'Total',
            id: 'gridFaturamentoStore',
            name: 'gridFaturamentoStore',
            proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("ConsultaRelatorioFaturamento", "RelatorioFaturamento") %>', timeout: 600000 }),
            paramNames: {
                sort: "detalhesPaginacaoWeb.Sort",
                dir: "detalhesPaginacaoWeb.Dir",
                start: "detalhesPaginacaoWeb.Start",
                limit: "detalhesPaginacaoWeb.Limit"
            },
            fields: ['IdFaturamento', 'FaturamentoFormatado','QuantNotaFiscal', 'Vagao', 'UFOrigem', 'Origem', 'UFDestino', 'Destino', 'TipoVagao', 'Fluxo', 'Segmento', 'Cliente', 'Expedidor', 'Recebedor', 'Str363', 'Str1131', 'MD', 'PrefixoTrem', 'OS']
        });

//        var situacaoStore = new window.Ext.data.JsonStore({
//            id: 'situacaoStore',
//            name: 'situacaoStore',
//            root: 'Items',
//            url: '<%= Url.Action("ObterSituacoes", "PainelExpedicao") %>',
//            fields: [
//                    'Id',
//                    'Nome'
//                ],
//            listeners: {
//                load: function (store) {
//                    var rt = store.recordType;
//                    store.insert(0, new rt({}));
//                }
//            }
//        });
//        window.situacaoStore = situacaoStore;

        var clienteStore = new Ext.data.JsonStore({
            root: "Items",
            autoLoad: true,
            proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("ObterClientes", "RelatorioFaturamento") %>',
                timeout: 600000
            }),
            id: 'clienteStore',
            fields: [
                'Id',
                'Nome'
            ]
        });

//        var clienteStore = new window.Ext.data.JsonStore({
//            id: 'clienteStore',
//            name: 'clienteStore',
//            root: 'Items',
//            url: '<%=Url.Action("ObterClientes", "RelatorioFaturamento") %>',
//            fields: [
//                    'Id',
//                    'Nome'
//                ],
//            listeners: {
//                load: function (store) {
//                    var rt = store.recordType;
//                    store.insert(0, new rt({}));
//                }
//            }
//        });
//        window.clienteStore = clienteStore;

        var expedidorStore = new window.Ext.data.JsonStore({
            id: 'expedidorStore',
            name: 'expedidorStore',
            root: 'Items',
            url: '<%=Url.Action("ObterExpedidores", "PainelExpedicao") %>',
            fields: [
                    'DescResumida'
                ],
            listeners: {
                load: function (store) {
                    var rt = store.recordType;
                    store.insert(0, new rt({}));
                }
            }
        });
        window.expedidorStore = expedidorStore;

        var segmentosStore = new Ext.data.JsonStore({
            root: "Items",
            autoLoad: true,
            proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("ObterSegmentos", "PainelExpedicao") %>',
                timeout: 600000
            }),
            id: 'segmentosStore',
            fields: [
                'Id',
                'Nome'
            ]
        });

        // Malhas Ferroviárias
        var MalhaCentralStore = new Ext.data.JsonStore({
            root: "Items",
            autoLoad: true,
            proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("ObterMalhas", "PainelExpedicao") %>',
                timeout: 600000
            }),
            id: 'MalhaCentralStore',
            fields: [
                'Id',
                'Nome'
            ]
        });

         var estacaoFaturamentoStore = new Ext.data.JsonStore({
            root: "Items",
            autoLoad: true,
            proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("ObterEstacoesFaturamento", "PainelExpedicao") %>',
                timeout: 600000
            }),
            id: 'estacaoFaturamentoStore',
            fields: [
                'Id',
                'Nome'
            ]
        });

          var ufsStore = new Ext.data.ArrayStore({
            id: 0,
            fields: [
            'Id',
            'Nome'
            ],
            data: [[0, 'Todas'], [1, 'MG'], [2, 'MT'], [3, 'MS'], [4, 'PR'],
                      [5, 'RJ'], [6, 'RS'], [7, 'SC'], [8, 'SP']]
        });

        //****************************'arrCampos' **************************************//
        //***************************** CAMPOS *****************************//
        //******************************************************************//
        $(function () {
            var txtDataInicial = new Ext.form.DateField({
                fieldLabel: 'Data Início',
                id: 'txtDataInicial',
                name: 'txtDataInicial',
                dateFormat: 'd/n/Y',
                width: 83,
                maxLength: 10,
                value: new Date(),
                plugins: [new Ext.ux.InputTextMask('99/99/9999', true)]
            });

            var txtDataFinal = new Ext.form.DateField({
                fieldLabel: 'Data Fim',
                id: 'txtDataFinal',
                name: 'txtDataFinal',
                dateFormat: 'd/n/Y',
                width: 83,
                maxLength: 10,
                value: new Date(),
                plugins: [new Ext.ux.InputTextMask('99/99/9999', true)]
            });

            var txtFluxo = {
                xtype: 'textfield',
                name: 'txtFluxo',
                id: 'txtFluxo',
                fieldLabel: 'Fluxo',
                autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '7' },
                maskRe: /[a-zA-Z0-9]/,
                style: 'text-transform: uppercase',
                width: 70
            };

            var txtOrigem = {
                xtype: 'textfield',
                name: 'txtOrigem',
                id: 'txtOrigem',
                fieldLabel: 'Origem',
                autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '3' },
                maskRe: /[a-zA-Z]/,
                style: 'text-transform: uppercase',
                width: 50
            };

            var txtDestino = {
                xtype: 'textfield',
                name: 'txtDestino',
                id: 'txtDestino',
                fieldLabel: 'Destino',
                autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '3' },
                maskRe: /[a-zA-Z]/,
                style: 'text-transform: uppercase',
                width: 50
            };

            var txtVagoes = {
                xtype: 'textfield',
                name: 'txtVagoes',
                id: 'txtVagoes',
                fieldLabel: 'Lista de Vagões(separados por ";")',
                maskRe: /[a-zA-Z0-9\;]/,
                maxLength: 500,
                width: 573,
                enableKeyEvents: true,
                listeners: {
                    specialkey: function (f, e) {
                        if (e.getKey() == e.ENTER) {
                            Pesquisar();
                        }
                    }
                }
            };

            var ddlCliente = new Ext.form.ComboBox({
                typeAhead: false,
                triggerAction: 'all',
                lazyRender: true,
                mode: 'local',
                store: clienteStore,
                valueField: 'Id',
                displayField: 'Nome',
                fieldLabel: 'Cliente',
                id: 'ddlCliente',
                width: 260
            });

            var ddlExpedidor = {
                xtype: 'combo',
                id: 'ddlExpedidor',
                name: 'ddlExpedidor',
                forceSelection: true,
                store: window.expedidorStore,
                triggerAction: 'all',
                mode: 'remote',
                typeAhead: true,
                minChars: 1,
                fieldLabel: 'Expedidor',
                displayField: 'DescResumida',
                valueField: 'DescResumida',
                width: 125,
                editable: true,
                tpl: '<tpl for="."><div class="x-combo-list-item">{DescResumida}&nbsp;</div></tpl>'
            };

            var ddlSegmento = new Ext.form.ComboBox({
                typeAhead: false,
                triggerAction: 'all',
                lazyRender: true,
                mode: 'local',
                store: segmentosStore,
                valueField: 'Id',
                displayField: 'Nome',
                fieldLabel: 'Segmento',
                id: 'ddlSegmento',
                width: 80
            });

            var txtPrefixo = {
                xtype: 'textfield',
                name: 'txtPrefixo',
                id: 'txtPrefixo',
                fieldLabel: 'Prefixo',
                autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '3' },
                maskRe: /[a-zA-Z0-9]/,
                style: 'text-transform: uppercase',
                width: 70
            };

            var txtOs = {
                xtype: 'textfield',
                name: 'txtOs',
                id: 'txtOs',
                fieldLabel: 'OS',
                autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '7' },
                maskRe: /[0-9]/,
                style: 'text-transform: uppercase',
                width: 45
            };

            var ddlMalhaCentral = new Ext.form.ComboBox({
                typeAhead: false,
                triggerAction: 'all',
                lazyRender: true,
                mode: 'local',
                store: MalhaCentralStore,
                valueField: 'Id',
                width: 120,
                displayField: 'Nome',
                fieldLabel: 'Malha',
                id: 'ddlMalhaCentral'
            });

            var ddlUf = new Ext.form.ComboBox({
                typeAhead: false,
                triggerAction: 'all',
                lazyRender: true,
                mode: 'local',
                store: ufsStore,
                valueField: 'Nome',
                displayField: 'Nome',
                fieldLabel: 'UF',
                id: 'ddlUf',
                width: 70,
                value: 'Todas',
                listeners: {
                    select: {
                        fn: function () {
                            var selectedValue = this.getValue();
                            var ddlMalhaCentral = Ext.getCmp("ddlMalhaCentral");
                            var malhaCentral = ddlMalhaCentral.getValue();
                            estacaoFaturamentoStore.load({ params: { uf: selectedValue, malha: malhaCentral },
                                callback: function (options, success, response, records) {
                                    if (success) {
                                        var combobox = Ext.getCmp("ddlEstacaoFaturamento");

                                        if (estacaoFaturamentoStore.getCount() == 0) {
                                            combobox.setValue("");
                                        } else {
                                            combobox.setValue("TODOS");
                                        }
                                    }
                                }
                            });
                        }
                    }
                }
            });

            var ddlEstacaoFaturamento = new Ext.form.ComboBox({
                typeAhead: false,
                triggerAction: 'all',
                lazyRender: true,
                mode: 'local',
                store: estacaoFaturamentoStore,
                valueField: 'Id',
                displayField: 'Nome',
                fieldLabel: 'Estação Faturamento',
                id: 'ddlEstacaoFaturamento',
                width: 120,
                value: 'TODOS'
            });

            var sm = new Ext.grid.CheckboxSelectionModel({
                singleSelect: true,
                header: ''

            });
            // Toolbar da Grid
            var pagingToolbar = new Ext.PagingToolbar({
                pageSize: 1000,
                store: gridFaturamentoStore,
                displayInfo: true,
                displayMsg: App.Resources.Web.GridExibindoRegistros,
                emptyMsg: App.Resources.Web.GridSemRegistros,
                paramNames: {
                    start: "detalhesPaginacaoWeb.Start",
                    limit: "detalhesPaginacaoWeb.Limit"
                }
            });

            var rowNumerador = new Ext.grid.RowNumberer()
            {
                defaultWidth: true
            };

            var cm = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true
                },
                columns: [
                    rowNumerador,
                    { header: 'Faturamento', dataIndex: "FaturamentoFormatado", sortable: false, width: 100 },
                //                    { header: 'QtdNTF', dataIndex: "QuantNotaFiscal", sortable: false, width: 30 },
                    {header: 'Vagão', dataIndex: "Vagao", sortable: false, width: 80 },
                    { header: 'UF', dataIndex: "UFOrigem", sortable: false, width: 30 },
                    { header: 'Origem', dataIndex: "Origem", sortable: false, width: 60 },
                    { header: 'UF', dataIndex: "UFDestino", sortable: false, width: 30 },
                    { header: 'Destino', dataIndex: "Destino", sortable: false, width: 60 },
                    { header: 'Tipo de Vagão', dataIndex: "TipoVagao", sortable: false, width: 80 },
                    { header: 'Fluxo', dataIndex: "Fluxo", sortable: false, width: 60 },
                    { header: 'Segmento', dataIndex: "Segmento", sortable: false, width: 60 },
                    { header: 'Cliente', dataIndex: "Cliente", sortable: false, width: 60 },
                    { header: 'Expedidor', dataIndex: "Expedidor", sortable: false, width: 60 },
                    { header: 'Recebedor', dataIndex: "Recebedor", sortable: false, width: 60 },
                    { header: '363', dataIndex: "Str363", sortable: false, width: 60 },
                    { header: '1131', dataIndex: "Str1131", sortable: false, width: 60 },
                    { header: 'MD', dataIndex: "MD", sortable: false, width: 60 },
                    { header: 'Prefixo Trem', dataIndex: "PrefixoTrem", sortable: false, width: 80 },
                    { header: 'OS', dataIndex: "OS", sortable: false, width: 60 }
                ]
            });

            var gridFaturamento = new Ext.grid.EditorGridPanel({
                id: 'gridFaturamento',
                name: 'gridFaturamento',
                autoLoadGrid: true,
                height: 360,
                width: 870,
                //autoLoadGrid: true,
                stripeRows: true,
                cm: cm,
                region: 'center',
                viewConfig: {
                    forceFit: true,
                    emptyText: 'Não possui registros para exibição.'
                },
                autoScroll: true,
                loadMask: { msg: App.Resources.Web.Carregando },
                store: gridFaturamentoStore,
                bbar: pagingToolbar,
                sm: sm
            });

            gridFaturamento.getStore().proxy.on('beforeload', function (p, params) {
                var relatorioFaturamentoRequestDto = montaObjetoTransferencia();
                params["relatorioFaturamentoRequestDto"] = $.toJSON(relatorioFaturamentoRequestDto);
            });

            gridFaturamentoStore.on('load', function () {
                //Ajusta camanho quantidade linhas grid
                $(".x-grid3-td-numberer").each(function () {
                    $(this).attr("style", "width:30px");
                });
                //Habilitada Desabilita BOTÕES
                habilitadaDesabilitaBotoes();
            });
            //******************************************************************//
            //***************************** LAYOUT *****************************//
            //******************************************************************//

            var formtxtDataInicial = {
                layout: 'form',
                width: 200,
                border: false,
                bodyStyle: 'padding: 5px',
                autoWidth: true,
                items: [txtDataInicial]
            };

            var formtxtDataFinal = {
                layout: 'form',
                width: 200,
                border: false,
                bodyStyle: 'padding: 5px',
                autoWidth: true,
                items: [txtDataFinal]
            };

            var formtxtFluxo = {
                layout: 'form',
                width: 200,
                border: false,
                bodyStyle: 'padding: 5px',
                autoWidth: true,
                items: [txtFluxo]
            };

            var formtxtOrigem = {
                layout: 'form',
                width: 200,
                border: false,
                bodyStyle: 'padding: 5px',
                autoWidth: true,
                items: [txtOrigem]
            };

            var formtxtDestino = {
                layout: 'form',
                width: 200,
                border: false,
                bodyStyle: 'padding: 5px',
                autoWidth: true,
                items: [txtDestino]
            };

            var formtxtVagoes = {
                layout: 'form',
                width: 200,
                border: false,
                bodyStyle: 'padding: 5px',
                autoWidth: true,
                items: [txtVagoes]
            };

            var formddlCliente = {
                layout: 'form',
                width: 200,
                border: false,
                bodyStyle: 'padding: 5px',
                autoWidth: true,
                items: [ddlCliente]
            };

            var formddlExpedidor = {
                layout: 'form',
                width: 200,
                border: false,
                bodyStyle: 'padding: 5px',
                autoWidth: true,
                items: [ddlExpedidor]
            };

            var formddlSegmento = {
                layout: 'form',
                width: 200,
                border: false,
                bodyStyle: 'padding: 5px',
                autoWidth: true,
                items: [ddlSegmento]
            };

            var formtxtPrefixo = {
                layout: 'form',
                width: 200,
                border: false,
                bodyStyle: 'padding: 5px',
                autoWidth: true,
                items: [txtPrefixo]
            };

            var formtxtOs = {
                layout: 'form',
                width: 200,
                border: false,
                bodyStyle: 'padding: 5px',
                autoWidth: true,
                items: [txtOs]
            };

            var formddlMalhaCentral = {
                layout: 'form',
                width: 200,
                border: false,
                bodyStyle: 'padding: 5px',
                autoWidth: true,
                items: [ddlMalhaCentral]
            };

            var formddlUf = {
                layout: 'form',
                width: 200,
                border: false,
                bodyStyle: 'padding: 5px',
                autoWidth: true,
                items: [ddlUf]
            };

            var formddlEstacaoFaturamento = {
                layout: 'form',
                width: 200,
                border: false,
                bodyStyle: 'padding: 5px',
                autoWidth: true,
                items: [ddlEstacaoFaturamento]
            };

            //Array de componentes do form 
            var arrCampos1 = {
                layout: 'column',
                border: false,
                items: [formtxtDataInicial, formtxtDataFinal, formtxtFluxo, formtxtOrigem, formtxtDestino, formtxtVagoes]
            };
            var arrCampos2 = {
                layout: 'column',
                border: false,
                items: [formddlCliente, formddlExpedidor, formddlSegmento, formtxtPrefixo, formtxtOs, formddlMalhaCentral, formddlUf, formddlEstacaoFaturamento]
            };

            //******************************************************************//
            //***************************** FUNÇÕES*****************************//
            //******************************************************************//
            function habilitadaDesabilitaBotoes() {
                //var rangeLocal = gridFaturamento.getStore().getRange();
                var quantidadeItensGrid = gridFaturamento.getStore().data.items.length;

                if (quantidadeItensGrid > 0) {
                    Ext.getCmp('btnExportar').enable();
                    Ext.getCmp('btnImprimir').enable();
                } else {
                    Ext.getCmp('btnExportar').disable();
                    Ext.getCmp('btnImprimir').disable();
                }
            };
            function PesquisaRelatorioFatura() {
                if (validaCamposObrigatorios()) {

                    //Carrega GRID
                    gridFaturamentoStore.load({ params: { start: 0, limit: 1000} });

                    //Habilitada Desabilita BOTÕES
                    habilitadaDesabilitaBotoes();

                }
            };

            function validaCamposObrigatorios() {

                var dtInicial = Ext.getCmp("txtDataInicial").getRawValue();
                var dtFinal = Ext.getCmp("txtDataFinal").getRawValue();

                if (dtInicial == "" || dtInicial == null || dtInicial == new Date()) {
                    Ext.Msg.show({ title: 'Aviso',
                        msg: 'O Campo Data Início é obrigatório',
                        buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR,
                        fn: function (btn, text)
                        { Ext.getCmp('txtDataInicial').focus(); }
                    });
                    return false;
                }

                if (dtFinal == "" || dtFinal == null || dtFinal == new Date()) {
                    Ext.Msg.show({ title: 'Aviso',
                        msg: 'O Campo Data Fim é obrigatório',
                        buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR,
                        fn: function (btn, text)
                        { Ext.getCmp('txtDataFinal').focus(); }
                    });
                    return false;
                }

                if (!comparaDatas(dtInicial, dtFinal)) {
                    Ext.Msg.show({ title: 'Aviso',
                        msg: 'Data Início deve ser menor que a Data Fim',
                        buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR
                    });
                    return false;
                }

                //                var arrDtIni = dtInicial.split('/');
                //                var arrDtFim = dtFinal.split('/');
                //                var dtIni = new Date(arrDtIni[2] + '/' + arrDtIni[1] + '/' + arrDtIni[0]);
                //                var dtFim = new Date(arrDtFim[2] + '/' + arrDtFim[1] + '/' + arrDtFim[0]);
                //                if ((dtIni - dtFim) > 30) {
                //                    Ext.Msg.show({ title: 'Aviso',
                //                        msg: 'O período não deve exceder 30 dias.',
                //                        buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR
                //                    });
                //                    return false;
                //                }
                return true;
            }

            function comparaDatas(dataIni, dataFim) {

                var arrDtIni = dataIni.split('/');
                var arrDtFim = dataFim.split('/');

                var dtIni = new Date(arrDtIni[2] + '/' + arrDtIni[1] + '/' + arrDtIni[0]);
                var dtFim = new Date(arrDtFim[2] + '/' + arrDtFim[1] + '/' + arrDtFim[0]);

                return dtIni <= dtFim;
            }

            function montaObjetoTransferencia() {
                var dtInicial = Ext.getCmp("txtDataInicial").getRawValue();
                var dtFinal = Ext.getCmp("txtDataFinal").getRawValue();

                var dateParts = dtInicial.split("/");
                dtInicial = dateParts[1] + "/" + dateParts[0] + "/" + dateParts[2]; // month is 0-based

                dateParts = dtFinal.split("/");
                dtFinal = dateParts[1] + "/" + dateParts[0] + "/" + dateParts[2]; // month is 0-based
                //FORMATO DATETIME - 12/06/2017 00:00:00

                var txtFluxo = Ext.getCmp("txtFluxo").getRawValue();
                var txtOrigem = Ext.getCmp("txtOrigem").getRawValue();
                var txtDestino = Ext.getCmp("txtDestino").getRawValue();
                var txtVagoes = Ext.getCmp("txtVagoes").getValue();
//                var idSituacao = Ext.getCmp("ddlSituacao").getValue();
                var idCliente = Ext.getCmp("ddlCliente").getRawValue();
                var idExpedidor = Ext.getCmp("ddlExpedidor").getValue();
                var idSegmento = Ext.getCmp("ddlSegmento").getValue();
                var txtPrefixo = Ext.getCmp("txtPrefixo").getValue();
                var txtOs = Ext.getCmp("txtOs").getValue();
                var idMalhaCentral = Ext.getCmp("ddlMalhaCentral").getValue();
                var idUf = Ext.getCmp("ddlUf").getValue();
                var idEstacaoFaturamento = Ext.getCmp("ddlEstacaoFaturamento").getValue();

                //Monta array Vagões separado por ;
                var Vagoes = [];
                var VagoesSplitados = txtVagoes.split(";");

                for (var i = 0; i < VagoesSplitados.length; i++) {
                    Vagoes.push(VagoesSplitados[i]);
                }

                var relatorioFaturamentoRequestDto = {
                    DataInicio: dtInicial,
                    DataFinal: dtFinal,
                    Fluxo: txtFluxo,
                    Origem: txtOrigem,
                    Destino: txtDestino,
                    Vagoes: Vagoes,
                    Cliente: idCliente,
                    Expeditor: idExpedidor,
                    Segmento: (idSegmento.trim() == " TODOS  " ? "" : idSegmento),
                    Prefixo: txtPrefixo,
                    Os: txtOs,
                    Malha: (idMalhaCentral == "Todas" ? "" : idMalhaCentral),
                    Uf: (idUf == "Todas" ? "" : idUf),
                    Estacao: (idEstacaoFaturamento == "Todas" ? "" : idEstacaoFaturamento)
                };
                //                }
                return relatorioFaturamentoRequestDto;
            }

            function LimpaCampos() {
                gridFaturamento.getStore().removeAll();
                Ext.getCmp("panelForm").getForm().reset();

                //Habilitada Desabilita BOTÕES
                habilitadaDesabilitaBotoes();
            }

            //***************************** PAINEL *****************************//
            //Conteudo do Form que sera criado
            var panelFormFiltros = new Ext.form.FormPanel({
                id: 'panelForm',
                title: "Faturamentos",
                region: 'center',
                bodyStyle: 'padding:15px;',
                layout: 'form',
                labelAlign: 'top',
                items: [arrCampos1, arrCampos2],
                buttonAlign: "center",
                buttons:
                    [{
                        text: 'Pesquisar',
                        type: 'submit',
                        iconCls: 'icon-find',
                        handler: function (b, e) {
                            PesquisaRelatorioFatura();
                        }
                    },
                        {
                            text: 'Limpar',
                            type: 'submit',
                            iconCls: 'icon-del',
                            handler: function (b, e) {
                                LimpaCampos();
                            }
                        },
                        {
                            text: 'Exportar',
                            type: 'submit',
                            id: 'btnExportar',
                            disabled: true,
                            iconCls: 'icon-page-excel',
                            handler: function (b, e) {

                                if (validaCamposObrigatorios()) {
                                    Ext.getBody().mask("Processando dados...", "x-mask-loading");

                                    var url = '<%= Url.Action("gExportar", "RelatorioFaturamento") %>';
                                    url += "?relatorioFaturamentoRequestDto=" + $.toJSON(montaObjetoTransferencia());
                                    window.open(url, "");

                                    Ext.getBody().unmask();
                                }
                            }
                        },
                        {
                            text: 'Imprimir',
                            type: 'submit',
                            id: 'btnImprimir',
                            disabled: true,
                            iconCls: 'icon-printer',
                            handler: function (b, e) {

                                if (validaCamposObrigatorios()) {
                                    Ext.getBody().mask("Processando dados...", "x-mask-loading");

                                    var url = '<%= Url.Action("Imprimir", "RelatorioFaturamento") %>';
                                    url += "?relatorioFaturamentoRequestDto=" + $.toJSON(montaObjetoTransferencia());
                                    window.open(url, "");

                                    Ext.getBody().unmask();
                                }
                            }
                        }
                    ]
            });

            new Ext.Viewport({
                layout: 'border',
                margins: 10,
                items: [
                    {
                        region: 'north',
                        height: 260,
                        autoScroll: true,
                        items: [{
                            region: 'center',
                            applyTo: 'header-content'
                        },
                            panelFormFiltros]
                    },
                    gridFaturamento
                ]
            });

            //***************************** RENDER *****************************//
            Ext.onReady(function () {
                PesquisaRelatorioFatura();

                //                $().ready(function () {
                //                    $("#ext-comp-1010").css("display", "none");

                //                });
                //panelForm.render("divContent");
                /////////////////////PesquisarFornecedores();
                //Carrega GRID Fornecedores
                //gridFornecedor.load({ params: { start: 0, limit: 50} });
            });
        });


        //        var panelForm = new Ext.form.FormPanel({
        //            id: 'panelForm',
        //            layout: 'form',
        //            labelAlign: 'top',
        //            border: false,
        //            autoHeight: true,
        //            title: "Lista Fornecedores",
        //            region: 'center',
        //            bodyStyle: 'padding: 15px',
        //            items: [containerLinha1, containerLinha2, containerLinh4],
        //            buttonAlign: "center"
        //        });




        //        gridOSLimpezaVagaoStore.load({ params: { start: 0, limit: 50} });
    
    </script>
</asp:Content>
<asp:Content runat="server" ID="Content2" ContentPlaceHolderID="Content">
    <div id="header-content">
        <h1>
            Relat&oacute;rio Faturamento</h1>
        <small>Você está em Consultas > Relat&oacute;rios > Relat&oacute;rio de Faturamento</small>
        <br />
    </div>
</asp:Content>

