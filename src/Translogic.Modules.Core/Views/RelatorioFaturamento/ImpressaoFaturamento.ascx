﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<style type="text/css">
    table
    {
        font-family: Arial;
        font-size: 8px;
        border-collapse: collapse !important;
    }
    
    .tabletitle
    {
        color: #000;
        background-color: #fff;
        border: 1px solid #eceeef;
    }
    
    .tabletd
    {
        color:#595959;
        /*padding: 0.75rem;
        vertical-align: top;
        background-color: #fff;*/
        border: 1px solid #eceeef;
    }
</style>
<div>
    <table border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                &nbsp;
            </td>
            <td>
                <%=DateTime.Now.ToString("dd/MM/yyyy HH:mm")%>
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                De:<%=ViewData["DataInicial"]%>
            </td>
            <td>
            </td>
            <td>
                Até:<%=ViewData["DataFinal"]%>
            </td>
        </tr>
    </table>
    <table cellpadding="0" cellspacing="0" class="table" border="0" width="99%">
            <tr>
                <td class="tabletitle" style="width: 50px;">
                    Vagão
                </td>
                <td class="tabletitle" style="width: 50px;">
                    Série
                </td>
                <td class="tabletitle" style="width: 60px;">
                    Segmento
                </td>
                <td class="tabletitle" style="width: 120px;">
                    Faturamento
                </td>
                <td class="tabletitle"  style="width: 30px;">
                    UF
                </td>
                <td class="tabletitle" style="width: 55px;">
                    Origem
                </td>
                <td class="tabletitle" style="width: 30px;">
                    UF
                </td>
                <td class="tabletitle" style="width: 55px;">
                    Destino
                </td>
                <td class="tabletitle" style="width: 70px;">
                    Fluxo
                </td>
                <td class="tabletitle"  style="width: 30px;">
                    MD
                </td>
                <td class="tabletitle" style="width: 70px;">
                    Nota Fiscal
                </td>
                <td class="tabletitle" style="width: 90px;">
                    Valor NF Cliente
                </td>
                <%--<td class="tabletitle">
                    Chave NF-e
                </td>--%>
                <td class="tabletitle" style="width: 60px;">
                    Volume
                </td>
                <td class="tabletitle" style="width: 70px;">
                    Peso Rateio
                </td>
                <td class="tabletitle" style="width: 60px;">
                    Peso Total
                </td>
                <td class="tabletitle" style="width: 60px;">
                    Tara
                </td>
                <td class="tabletitle" style="width: 60px;">
                    TB
                </td>
                <td class="tabletitle" style="width: 70px;">
                    Cliente 363
                </td>
                <td class="tabletitle" style="width: 70px;">
                    Expedidor
                </td>
                <td class="tabletitle" style="width: 70px;">
                    Recebedor
                </td>
                <td class="tabletitle" style="width: 70px;">
                    Remetente Fiscal
                </td>
                <td class="tabletitle" style="width: 70px;">
                    Containers
                </td>
                <td class="tabletitle" style="width: 30px;">
                    363
                </td>
                <td class="tabletitle" style="width: 30px;">
                    1131
                </td>
                <td class="tabletitle" style="width: 40px;">
                    Prefixo Trem
                </td>
                <td class="tabletitle" style="width: 60px;">
                    OS
                </td>
            </tr>
            <%     
                if (ViewData["Relatorio"] != null)
                {

                    var dadosRelatorio = ViewData["Relatorio"] as System.Collections.Generic.List<Translogic.Modules.Core.Domain.Model.Dto.RelatorioFaturamentoExportDto>;
                    foreach (var item in dadosRelatorio)
                    {
     
            %>
            <tr>
                <td class="tabletd">
                    <%=item.Vagao%>
                </td>
                <td class="tabletd">
                    <%=item.Serie%>
                </td>
                <td class="tabletd">
                    <%=item.Segmento%>
                </td>
                <td class="tabletd">
                    <%=item.Faturamento%>
                </td>
                <td class="tabletd">
                    <%=item.UFOrigem%>
                </td>
                <td class="tabletd">
                    <%=item.Origem%>
                </td>
                <td class="tabletd">
                    <%=item.UFDestino%>
                </td>
                <td class="tabletd">
                    <%=item.Destino%>
                </td>
                <td class="tabletd">
                    <%=item.Fluxo%>
                </td>
                <td class="tabletd">
                    <%=item.MD%>
                </td>
                <td class="tabletd">
                    <%=item.NotaFiscal%>
                </td>
                <td class="tabletd">
                    <%=item.ValorNFCliente%>
                </td>
               <%-- <td class="tabletd" style="font-size: 7px">
                    <%=item.ChaveNFe%>
                </td>--%>
                <td class="tabletd">
                    <%=item.Volume%>
                </td>
                <td class="tabletd">
                    <%=item.PesoRateio%>
                </td>
                <td class="tabletd">
                    <%=item.PesoTotal%>
                </td>
                <td class="tabletd">
                    <%=item.Tara%>
                </td>
                <td class="tabletd">
                    <%=item.TB%>
                </td>
                <td class="tabletd">
                    <%=item.Cliente%>
                </td>
                <td class="tabletd">
                    <%=item.Expedidor%>
                </td>
                <td class="tabletd">
                    <%=item.Recebedor%>
                </td>
                <td class="tabletd">
                    <%=item.RemetenteFiscal%>
                </td>
                <td class="tabletd">
                    <%=item.Containers%>
                </td>
                <td class="tabletd">
                    <%=item.Str363%>
                </td>
                <td class="tabletd">
                    <%=item.Str1131%>
                </td>
                <td class="tabletd">
                    <%=item.PrefixoTrem%>
                </td>
                <td class="tabletd">
                    <%=item.OS%>
                </td>
            </tr>
            <%
                    }
                }
            %>
    </table>
</div>
