﻿<%@ Page Title="" Language="C#" Inherits="System.Web.Mvc.ViewPage" MasterPageFile="~/Views/Shared/Interna.master" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript" language="javascript">
       	   
	    function PesquisarFluxo(codigoFluxo, origem, destino, mercadoria) {

            if (isNaN(codigoFluxo)){
                Ext.getCmp("codigoFluxoComercial").markInvalid("Número do fluxo inválido.");
                return;
            }
            
            var btnDespachar = Ext.getCmp("btnDespachar");
            if (btnDespachar.disabled) return;
            
            btnDespachar.disable();

            // chamada ajax
            var url = '<%=Url.Action("Detalhe") %>';
            
            Ext.Ajax.request({ 
			    url: "<% =Url.Action("VerificarFluxoComercial") %>",
			    success: function(data, opt) {
                    var responseObj = Ext.decode(data.responseText);
                    // alert(responseObj.IdFluxo);
                    if (responseObj.Erro)
                    {
                        Ext.Msg.alert('Aviso', responseObj.Mensagem, function(){ 
																					btnDespachar.enable(); 
																					Ext.getCmp('codigoFluxoComercial').focus();
																				});
                        
                    }else{
                        url = url + "?idFlx=" + responseObj.IdFluxo;

                        var origem = opt.params.codigoOrigemFluxo.toUpperCase();
                        var destino = opt.params.codigoDestinoFluxo.toUpperCase();
                        var mercadoria = opt.params.codigoMercadoria.toUpperCase();

                        if (origem != null && origem != ""){
                            url = url + "&codigoOrigem="+origem+"&codigoDestino="+destino+"&codigoMercadoria="+mercadoria;
                        }

                        // alert(url);
				        document.location.href=url;
                    }
			    },
                failure: function(conn, data){
                    alert("Ocorreu um erro inesperado.");
                    btnDespachar.enable();
                },
			    method: "POST",
			    params: { codigoFluxoComercial: codigoFluxo,
                    codigoOrigemFluxo: origem,
                    codigoDestinoFluxo: destino,
                    codigoMercadoria: mercadoria
                 }
		    });
        }

        function VerificarFluxoServicoInterno(codigoFluxo){
            if (codigoFluxo == "99999"){
                Ext.getCmp("origemFluxo").enable();
                Ext.getCmp("destinoFluxo").enable();
                Ext.getCmp("mercadoriaFluxo").enable();
                Ext.getCmp("origemFluxo").focus();
            }else{
                Ext.getCmp("origemFluxo").disable();
                Ext.getCmp("destinoFluxo").disable();
                Ext.getCmp("mercadoriaFluxo").disable();
            }
        }

        var txtCodigoFluxo = {
            autoCreate: { tag: 'input', type: 'text', maxlength: '5', autocomplete: 'off' },
            xtype: 'textfield',
            id: 'codigoFluxoComercial',
            fieldLabel: 'Fluxo',
            name: 'codigoFluxoComercial',
            width:70,
            // value: "57061",
            allowBlank: false,
            enableKeyEvents: true,
            listeners:{
                'keyup': function (field, e) {
                    VerificarFluxoServicoInterno(field.getValue());		
                },
				'keypress':  function (field, e) {
					
					// Valida se o campo é numerico
					var strChar = String.fromCharCode(e.keyCode);
					if(isNaN(strChar))
					{
						e.stopEvent();
					}
				}
            }
        };

        var txtOrigemFluxo = {
            autoCreate: { tag: 'input', type: 'text', maxlength: '3', autocomplete: 'off' },
            xtype: 'textfield',
            id: 'origemFluxo',
            fieldLabel: 'Origem',
            name: 'origemFluxo',
            maxLength: 3,
            allowBlank: false,
            style: 'text-transform: uppercase',
            width:50,
            disabled: true
        };

        var txtDestinoFluxo = {
            autoCreate: { tag: 'input', type: 'text', maxlength: '3', autocomplete: 'off' },
            xtype: 'textfield',
            id: 'destinoFluxo',
            fieldLabel: 'Destino',
            name: 'destinoFluxo',
            maxLength: 3,
            allowBlank: false,
            width:50,
            style: 'text-transform: uppercase',
            disabled: true
        };

        var txtMercadoriaFluxo = {
            autoCreate: { tag: 'input', type: 'text', maxlength: '5', autocomplete: 'off' },
            xtype: 'textfield',
            id: 'mercadoriaFluxo',
            fieldLabel: 'Mercadoria',
            name: 'mercadoriaFluxo',
            maxLength: 5,
            allowBlank: false,
            style: 'text-transform: uppercase',
            width:70,
            disabled: true
        };

        var coluna1 = { width: 80, layout: 'form', border: false, items: [txtCodigoFluxo] };
        var coluna2 = { width: 60, layout: 'form', border: false, items: [txtOrigemFluxo] };
        var coluna3 = { width: 60, layout: 'form', border: false, items: [txtDestinoFluxo] };
        var coluna4 = { width: 80, layout: 'form', border: false, items: [txtMercadoriaFluxo] };

        var linha1 = {
            layout: 'column',
            border: false,
            items: [coluna1, coluna2, coluna3, coluna4]
        };

        var filters = new Ext.FormPanel({
            id: 'filtrosFluxo',
            title: "Filtros",
            bodyStyle: 'padding: 10px',
            items: [linha1],
            buttonAlign: 'center',
            labelAlign: 'top',
            buttons: [{
                id: 'btnDespachar',
                text: 'Despachar',
                type: 'submit',
                iconCls: 'icon-find',
                handler: function (b, e) {
                    if (!Ext.getCmp("filtrosFluxo").getForm().isValid()) {
                        return false;
                    }

                    var codigoFluxo = Ext.getCmp("codigoFluxoComercial").getValue();
                    var origem = Ext.getCmp("origemFluxo").getValue();
                    var destino = Ext.getCmp("destinoFluxo").getValue();
                    var mercadoria = Ext.getCmp("mercadoriaFluxo").getValue();

                    PesquisarFluxo(codigoFluxo, origem, destino, mercadoria);
                }
            },{
                id: 'btnConsultarFluxo',
                text: 'Consultar Fluxo',
                type: 'submit',
                iconCls: 'icon-find',
                handler: function (b, e) {
					
					var codigoFluxo = Ext.getCmp("codigoFluxoComercial").getValue();

					if (codigoFluxo == "99999")
					{
						Ext.Msg.alert('Aviso','Fluxo de serviço não pode ser consultado.');
						return;

					}

                    if (!Ext.getCmp("filtrosFluxo").getForm().isValid()) {
                        return false;
                    }
					
					windowNotaFiscal = new Ext.Window({
										id: 'windowNotaFiscal',
										title: 'Detalhes do Fluxo',
										modal: true,
										width: 380,
										height: 530,
										autoLoad: {
											url: '<%= Url.Action("ConsultaFluxo") %>',
											params: { idFlx: codigoFluxo },
											scripts: true
						}
					
					});
					windowNotaFiscal.show();

                    // PesquisarFluxo(codigoFluxo, origem, destino, mercadoria);
                }
            }]
        });

        function test(e){
            if (e.getKey() == e.ENTER) {
				if (!Ext.getCmp("filtrosFluxo").getForm().isValid()) {
                    return false;
                }

                var codigoFluxo = Ext.getCmp("codigoFluxoComercial").getValue();
                var origem = Ext.getCmp("origemFluxo").getValue();
                var destino = Ext.getCmp("destinoFluxo").getValue();
                var mercadoria = Ext.getCmp("mercadoriaFluxo").getValue();

                PesquisarFluxo(codigoFluxo, origem, destino, mercadoria);
			}
        }

		
		function SomenteNumerico(campo)
		{
			var value = campo.getValue();
			var newValue = "";
                    
			for(var i=0; i < value.length; i++)
			{
				if(!isNaN(value.charAt(i)))
				{
					newValue += value.charAt(i);
				}
			}
			campo.setValue(newValue);
		}

        $(function () {
            Ext.getCmp("filtrosFluxo").render("divFiltros", 1);
            Ext.getDoc().on("keypress", test);
			Ext.getCmp("codigoFluxoComercial").focus();
        });

        
    </script>

</asp:Content>


<asp:Content ID="MainContent" ContentPlaceHolderID="Content" runat="server">
	<div id="header-content">
		<h1>Carregamento</h1>
		<small>Você está em Operação > Despacho > Carregamento</small>
		<br />
	</div>
    <div id="divFiltros"></div>
</asp:Content>
