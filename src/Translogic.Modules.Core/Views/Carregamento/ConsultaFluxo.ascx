﻿<%--<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ConsultaFluxo.ascx.cs" Inherits="Translogic.Modules.Core.Views.Carregamento.ConsultaFluxo" %>--%>
<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div id="div-form-detalhe-fluxo">
</div>
<%
    string origem = ViewData["FLUXO_ORIGEM"] != null ? ViewData["FLUXO_ORIGEM"].ToString() : string.Empty;
    string destino = ViewData["FLUXO_DESTINO"] != null ? ViewData["FLUXO_DESTINO"].ToString() : string.Empty;
    string mercadoria = ViewData["FLUXO_MERCADORIA"] != null ? ViewData["FLUXO_MERCADORIA"].ToString() : string.Empty;
    string codigo = ViewData["FLUXO_CODIGO"] != null ? ViewData["FLUXO_CODIGO"].ToString() : string.Empty;
    string remetente = ViewData["FLUXO_REMETENTE"] != null ? ViewData["FLUXO_REMETENTE"].ToString() : string.Empty;
    string destinatario = ViewData["FLUXO_DESTINATARIO"] != null ? ViewData["FLUXO_DESTINATARIO"].ToString() : string.Empty;
    string cfop = ViewData["FLUXO_CFOP"] != null ? ViewData["FLUXO_CFOP"].ToString() : string.Empty;
    string cfopProd = ViewData["FLUXO_CFOPPROD"] != null ? ViewData["FLUXO_CFOPPROD"].ToString() : string.Empty;
    string rem_cnpj = ViewData["FLUXO_REM_CNPJ"] != null ? ViewData["FLUXO_REM_CNPJ"].ToString() : string.Empty;
    string rem_sigla = ViewData["FLUXO_REM_SIGLA"] != null ? ViewData["FLUXO_REM_SIGLA"].ToString() : string.Empty;
    string rem_nome = ViewData["FLUXO_REM_NOME"] != null ? ViewData["FLUXO_REM_NOME"].ToString() : string.Empty;
    string rem_uf = ViewData["FLUXO_REM_UF"] != null ? ViewData["FLUXO_REM_UF"].ToString() : string.Empty;
    string rem_IE = ViewData["FLUXO_REM_IE"] != null ? ViewData["FLUXO_REM_IE"].ToString() : string.Empty;
    string des_cnpj = ViewData["FLUXO_DES_CNPJ"] != null ? ViewData["FLUXO_DES_CNPJ"].ToString() : string.Empty;
    string des_sigla = ViewData["FLUXO_DES_SIGLA"] != null ? ViewData["FLUXO_DES_SIGLA"].ToString() : string.Empty;
    string des_nome = ViewData["FLUXO_DES_NOME"] != null ? ViewData["FLUXO_DES_NOME"].ToString() : string.Empty;
    string des_uf = ViewData["FLUXO_DES_UF"] != null ? ViewData["FLUXO_DES_UF"].ToString() : string.Empty;
    string des_IE = ViewData["FLUXO_DES_IE"] != null ? ViewData["FLUXO_DES_IE"].ToString() : string.Empty;
    string des_tipoServicoCte = ViewData["FLUXO_DES_TPSERVICOCTE"] != null ? ViewData["FLUXO_DES_TPSERVICOCTE"].ToString() : string.Empty;

%>
<script type="text/javascript">

    var arrLinhas = new Array();

    var txtCodigoFluxo = {
        xtype: 'textfield',
        id: 'codigoFlxComercial',
        fieldLabel: 'Fluxo',
        name: 'codigoFlxComercial',
        value: '<%=codigo %>',
        width: 60,
        cls: 'x-item-disabled',
        readOnly: true
    };
    
    var txtTipoServico = {
        xtype: 'textfield',
        id: 'txtTipoServico',
        fieldLabel: 'Tipo servico',
        name: 'txtTipoServico',
        value: "<% = des_tipoServicoCte %>",
        width: 180,
        cls: 'x-item-disabled',
        readOnly: true
    };

    var txtOrigem = {
        xtype: 'textfield',
        id: 'codigoOrigem',
        fieldLabel: 'Origem',
        name: 'codigoOrigem',
        value: '<%=origem%>',
        width: 50,
        cls: 'x-item-disabled',
        readOnly: true
    };

    var txtDestino = {
        xtype: 'textfield',
        id: 'codigoDestino',
        fieldLabel: 'Destino',
        name: 'codigoDestino',
        value: '<%=destino%>',
        width: 60,
        cls: 'x-item-disabled',
        readOnly: true
    };

    var txtMercadoria = {
        xtype: 'textfield',
        id: 'descricaoMercadoria',
        fieldLabel: 'Mercadoria',
        name: 'descricaoMercadoria',
        value: '<%=mercadoria%>',
        width: 120,
        cls: 'x-item-disabled',
        readOnly: true
    };

    var txtRemFiscal = {
        xtype: 'textfield',
        id: 'descricaoRemFiscal',
        fieldLabel: 'Remetente',
        name: 'descricaoRemFiscal',
        value: '<%=remetente %>',
        width: 100,
        cls: 'x-item-disabled',
        readOnly: true
    };

    var txtDestFiscal = {
        xtype: 'textfield',
        id: 'descricaoDestFiscal',
        fieldLabel: 'Destinatário',
        name: 'descricaoDestFiscal',
        value: '<%=destinatario%>',
        width: 100,
        cls: 'x-item-disabled',
        readOnly: true
    };

    var txtCfop = {
        xtype: 'textfield',
        id: 'codigoCfop',
        fieldLabel: 'CFOP',
        name: 'codigoCfop',
        value: '<%=cfop%>',
        width: 40,
        cls: 'x-item-disabled',
        readOnly: true
    };

    var txtcfopProd = {
        xtype: 'textfield',
        id: 'codigoCfopProd',
        fieldLabel: 'CFOP Prod.',
        name: 'codigoCfopProd',
        value: '<%=cfopProd%>',
        width: 60,
        cls: 'x-item-disabled',
        readOnly: true
    };

    var fieldsetRemetente = {
        xtype: 'fieldset',
        title: 'Remetente',
        collapsible: false,
        width: 160,
        height: 270,
        defaults: { width: 110 },
        defaultType: 'textfield',
        items: [{
            fieldLabel: 'CNPJ/CPF',
            name: 'edCnpjRemetente',
            id: 'edCnpjRemetente',
            value: '<%=rem_cnpj %>',
            cls: 'x-item-disabled',
            readOnly: true
        }, {
            fieldLabel: 'Sigla',
            name: 'edSiglaRemetente',
            id: 'edSiglaRemetente',
            value: '<%=rem_sigla %>',
            cls: 'x-item-disabled',
            readOnly: true
        }, {
            fieldLabel: 'Nome',
            name: 'edRemetente',
            id: 'edRemetente',
            value: '<%=rem_nome %>',
            cls: 'x-item-disabled',
            readOnly: true
        }, {
            fieldLabel: 'UF',
            name: 'edUfRemetente',
            id: 'edUfRemetente',
            value: '<%=rem_uf %>',
            cls: 'x-item-disabled',
            readOnly: true
        }, {
            fieldLabel: 'Inscrição Estadual',
            name: 'edInscricaoEstadualRemetente',
            id: 'edInscricaoEstadualRemetente',
            value: '<%=rem_IE %>',
            cls: 'x-item-disabled',
            readOnly: true
        }]
    };


    var fieldsetDestinatario = {
        xtype: 'fieldset',
        title: 'Destinatário',
        collapsible: false,
        width: 160,
        height: 270,
        defaults: { width: 110 },
        defaultType: 'textfield',
        items: [{
            fieldLabel: 'CNPJ/CPF',
            name: 'edCnpjDestinatario',
            id: 'edCnpjDestinatario',
            value: '<%=des_cnpj %>',
            cls: 'x-item-disabled',
            readOnly: true
        }, {
            fieldLabel: 'Sigla',
            name: 'edSiglaDestinatario',
            id: 'edSiglaDestinatario',
            value: '<%=des_sigla %>',
            cls: 'x-item-disabled',
            readOnly: true
        }, {
            fieldLabel: 'Nome',
            name: 'edDestinatario',
            id: 'edDestinatario',
            value: '<%=des_nome %>',
            cls: 'x-item-disabled',
            readOnly: true
        }, {
            fieldLabel: 'UF',
            name: 'edUfDestinatario',
            id: 'edUfDestinatario',
            value: '<%=des_uf %>',
            cls: 'x-item-disabled',
            readOnly: true
        }, {
            fieldLabel: 'Inscrição Estadual',
            name: 'edInscricaoEstadualDestinatario',
            id: 'edInscricaoEstadualDestinatario',
            value: '<%=des_IE %>',
            cls: 'x-item-disabled',
            readOnly: true
        }]
    };

    var coluna1 = { width: 75, layout: 'form', border: false, items: [txtCodigoFluxo] };
    var coluna2 = { width: 200, layout: 'form', border: false, items: [txtTipoServico] };
    var coluna3 = { width: 65, layout: 'form', border: false, items: [txtOrigem] };
    var coluna4 = { width: 75, layout: 'form', border: false, items: [txtDestino] };
    var coluna5 = { width: 135, layout: 'form', border: false, items: [txtMercadoria] };
    var coluna6 = { width: 58, layout: 'form', border: false, items: [txtCfop] };
    var coluna7 = { width: 70, layout: 'form', border: false, items: [txtcfopProd] };
    var coluna8 = { width: 110, layout: 'form', border: false, items: [txtRemFiscal] };
    var coluna9 = { width: 125, layout: 'form', border: false, items: [txtDestFiscal] };

    var linha1 = { width: 380, layout: 'column', border: false, items: [coluna1, coluna2, coluna3, coluna4, coluna5, coluna6, coluna7, coluna8, coluna9] };
    // var linha2 = { width: 600, layout: 'column', border: false, items: [coluna5, coluna6, coluna7] };

    arrLinhas.push(linha1);
    // arrLinhas.push(linha2);

    var l5_coluna1 = { width: 170, layout: 'form', border: false, items: [fieldsetRemetente] };
    var l5_coluna2 = { width: 170, layout: 'form', border: false, items: [fieldsetDestinatario] };

    var linha5 = {
        width: 430,
        layout: 'column',
        border: false,
        items: [l5_coluna1, l5_coluna2]
    };

    arrLinhas.push(linha5);

    var formNotaFiscal = new Ext.form.FormPanel
	({
	    id: 'formNotaFiscal',
	    labelWidth: 80,
	    width: 366,
	    height: 500,
	    bodyStyle: 'padding: 15px',
	    labelAlign: 'top',
	    items: [arrLinhas],
	    buttonAlign: "center",
	    buttons: [{
	        text: "Sair",
	        handler: function () {
	            windowNotaFiscal.close();
	        }
	    }]
	});

    /**********************************************************
    FORM DETALHES FLUXO - FIM
    **********************************************************/
    formNotaFiscal.render("div-form-detalhe-fluxo");

</script>
