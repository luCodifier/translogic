﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%@ Import Namespace="System.Globalization" %>
<%
    double pesoTotalVagao = (double)ViewData["PESO_TOTAL_VAGAO"];
    double pesoRemanescenteVagao = (double)ViewData["PESO_REMANESCENTE_VAGAO"];
    bool indVagaoDespachado = (bool)ViewData["IND_VAGAO_JA_DESPACHADO"];
%>
<div id="div-grid-nota-fiscal">
</div>
<script type="text/javascript">
    var indVagaoDespachado = <%=indVagaoDespachado ? "true" : "false" %>;
    var pesoTotalVagaoDespachoAnterior = <%=pesoTotalVagao.ToString(CultureInfo.GetCultureInfo("en-US")) %>;
    var pesoRemanescenteVagaoDespachosAnteriores = <%=pesoRemanescenteVagao.ToString(CultureInfo.GetCultureInfo("en-US")) %>;

    function InserirLinhaNfVazia(){

        var ChaveNfe = "";
        var Conteiner = "";
        var SerieNotaFiscal = "";
        var NumeroNotaFiscal = "";
        var PesoTotal = null;
        var PesoRateio = null;
        var ValorNotaFiscal = 0;
        var ValorTotalNotaFiscal = 0;
        var Remetente = "";
        var Destinatario = "";
        var CnpjRemetente = "";
        var CnpjDestinatario = "";
        var TIF = "";
        var DataNotaFiscal = "";
        var SiglaRemetente = "";
        var SiglaDestinatario = "";
        var PlacaCavalo = "";
        var PlacaCarreta = "";
        var UfRemetente = "";
        var UfDestinatario = "";
        var InscricaoEstadualRemetente = "";
        var InscricaoEstadualDestinatario = "";
        var pesoDclVagao = null;
        var pesoTotalVagao = null;
        var multiploDespacho = "";
        var volumeNotaFiscal = null;
		var indContingencia = false;
        var ChaveCte = "";
        var CteInseridoTelaNf = "";
        InsereLinhaNotaFiscal(ChaveNfe, Conteiner, SerieNotaFiscal, NumeroNotaFiscal, PesoTotal, PesoRateio, ValorNotaFiscal, ValorTotalNotaFiscal, Remetente, Destinatario, CnpjRemetente, CnpjDestinatario, TIF, DataNotaFiscal, SiglaRemetente, SiglaDestinatario, PlacaCavalo, PlacaCarreta, UfRemetente, UfDestinatario, InscricaoEstadualRemetente, InscricaoEstadualDestinatario, pesoDclVagao, pesoTotalVagao, multiploDespacho, volumeNotaFiscal, indContingencia, ChaveCte, CteInseridoTelaNf);
    }

    function InsereLinhaNotaFiscal(ChaveNfe, Conteiner, SerieNotaFiscal, NumeroNotaFiscal, PesoTotal, PesoRateio, ValorNotaFiscal, ValorTotalNotaFiscal, Remetente, Destinatario, CnpjRemetente, CnpjDestinatario, TIF, DataNotaFiscal, SiglaRemetente, SiglaDestinatario, PlacaCavalo, PlacaCarreta, UfRemetente, UfDestinatario, InscricaoEstadualRemetente, InscricaoEstadualDestinatario, pesoDclVagao, pesoTotalVagao, multiploDespacho, volumeNotaFiscal, indContingencia, ChaveCte, CteInseridoTelaNf) {

        if (Conteiner != null && Conteiner != ""){
            Conteiner = Conteiner.toUpperCase();
        }

        ChaveCte = BuscaChaveCteInformada();

        var notaFiscalType = gridNotasFiscais.getStore().recordType;
        var notaFiscal = new notaFiscalType({
            ChaveNfe: ChaveNfe,
            Conteiner: Conteiner,
            SerieNotaFiscal: SerieNotaFiscal,
            NumeroNotaFiscal: NumeroNotaFiscal,
            PesoTotal: PesoTotal,
            PesoRateio: PesoRateio,
            ValorNotaFiscal: ValorNotaFiscal,
            ValorTotalNotaFiscal: ValorTotalNotaFiscal,
            Remetente: Remetente,
            Destinatario: Destinatario,
            CnpjRemetente: CnpjRemetente,
            CnpjDestinatario: CnpjDestinatario,
            TIF: TIF,
            DataNotaFiscal: DataNotaFiscal,
            SiglaRemetente: SiglaRemetente,
            SiglaDestinatario: SiglaDestinatario,
            PlacaCavalo: PlacaCavalo,
            PlacaCarreta: PlacaCarreta,
            UfRemetente: UfRemetente,
            UfDestinatario: UfDestinatario,
            InscricaoEstadualRemetente: InscricaoEstadualRemetente,
            InscricaoEstadualDestinatario: InscricaoEstadualDestinatario,
            IndObtidoAutomatico: false,
            IndMultiploDespacho: multiploDespacho,
            PesoDclVagao: pesoDclVagao,
            PesoTotalVagao: pesoTotalVagao,
            VolumeNotaFiscal: volumeNotaFiscal,
			IndContingencia: indContingencia,
            ChaveCte: ChaveCte,
            CteInseridoTelaNf: CteInseridoTelaNf
        });

        gridNotasFiscais.stopEditing();
        gridNotasFiscais.getStore().add(notaFiscal);
        var total = gridNotasFiscais.getStore().getCount();
        gridNotasFiscais.startEditing(total - 1, 1); //(total - 1) devido ao indice da grid
    }

    function BuscaChaveCteInformada() {

        var total = gridNotasFiscais.getStore().getCount();
        if (total > 0) {
            var chaveCte = gridNotasFiscais.getStore().getAt(0).data.ChaveCte;

            if ((chaveCte != null) && (chaveCte != "")) {
                return chaveCte;
            } else
                return "";            
        }
        return "";   
    }

    function RemoverLinhasVazias() {
        gridNotasFiscais.getStore().each(
			function (record) {
			    if (record.data.ChaveNfe == ''
                    && record.data.ChaveCte == ''
                    && record.data.SerieNotaFiscal == ''
                    && record.data.NumeroNotaFiscal == ''
                    && (record.data.PesoTotal == null || record.data.PesoTotal =='')
                    && record.data.ValorNotaFiscal == 0) {

			        gridNotasFiscais.getStore().remove(record);

			    }
			}
		);
    }

    function SalvarNotas() {
        RemoverLinhasVazias();
		
        var valido = ValidarDadosNotasVagao();
        if (valido)
        {
            RemoverNotasVagao(<%=ViewData["IdVagao"] %>);

            var tara = null;
            var volume = null;
			var nfeJaUtilizada= false;

            var multiploDespacho = Ext.getCmp("multiploDespacho").getValue();
            var pesoTotalVagao = Ext.getCmp("pesoTotalVagao").getValue();
            var pesoDclVagao = Ext.getCmp("pesoDclVagao").getValue();
            if (indPreenchimentoTara) tara = Ext.getCmp("taraVagao").getValue();
            if (indPreenchimentoVolume) volume = Ext.getCmp("volumeVagao").getValue();

            gridNotasFiscais.getStore().each(
			    function (record) {				
					/*
					notasFiscaisStore.filter("ChaveNfe", record.data.ChaveNfe);
					var countRegistro = notasFiscaisStore.getCount();
					notasFiscaisStore.clearFilter();
					dsNotasVagoes.find("ChaveNfe",record.data.ChaveNfe);
					if(countRegistro > 1)
					{	
						Ext.Msg.alert('Aviso', 'A nfe { '+record.data.ChaveNfe+' } já foi utilizada'); 					
						nfeJaUtilizada = true;
						return;
					}
					*/
			     nfeJaUtilizada =  !ImportarRegistroParaTelaPrincipal(record, <%=ViewData["IdVagao"] %>, '<%=ViewData["CodigoVagao"] %>', tara, volume, multiploDespacho, pesoDclVagao, pesoTotalVagao);
			    }
		    );
			
			if(nfeJaUtilizada)
			{
				return;
			}

            windowNotasFiscais.close();
        }else{
            var mensagemErro = "";
            Ext.getCmp("formNotasFiscais").getForm().items.each(function(f){
               if(f.activeError!= undefined){
                   mensagemErro += " - " + f.fieldLabel + ": " + f.activeError + "<br />";
               }
            });

            if (mensagemErro != "")
            {
                Ext.Msg.alert('Erro de validação', "Foram encontrados os erros abaixo:<br />" + mensagemErro);
            }
        }
    }

    function ValidarDadosNotasVagao(){
        if (!Ext.getCmp("formNotasFiscais").getForm().isValid()) {
            return false;
        }

//        if (parseFloat(Ext.getCmp("pesoDclVagao").getValue()) <= 0 || parseFloat(Ext.getCmp("pesoDclVagao").getValue()) >= 120){
//            Ext.getCmp("pesoDclVagao").markInvalid("Peso DCL inválido.");
//            return false;
//        }

//        if (parseFloat(Ext.getCmp("pesoTotalVagao").getValue()) <= 0 || parseFloat(Ext.getCmp("pesoTotalVagao").getValue()) >= 120){
//            Ext.getCmp("pesoTotalVagao").markInvalid("Peso total do vagão inválido.");
//            return false;
//        }
        
        if (parseFloat(Ext.getCmp("pesoDclVagao").getValue()) <= 0){
            Ext.getCmp("pesoDclVagao").markInvalid("Peso DCL inválido.");
            return false;
        }

        if (parseFloat(Ext.getCmp("pesoTotalVagao").getValue()) <= 0){
            Ext.getCmp("pesoTotalVagao").markInvalid("Peso total do vagão inválido.");
            return false;
        }

        //SE O VAGÃO JA FOI DESPACHADO POR OUTRO FLUXO
        if (indVagaoDespachado){
            
            // quando for multiplo despacho o peso total deve ser diferente porém não menor que o dcl
            if (parseFloat(Ext.getCmp("pesoDclVagao").getValue()) > pesoRemanescenteVagaoDespachosAnteriores){
                Ext.getCmp("pesoDclVagao").markInvalid("Peso do DCL não pode ser maior que o Peso Complementar do vagão.");
                return false;
            }

            // quando o peso DCL for igual ao peso remanescente deve-se selecionar como multiplo despacho "Não"
            if (parseFloat(Ext.getCmp("pesoDclVagao").getValue()) == pesoRemanescenteVagaoDespachosAnteriores 
                && Ext.getCmp("multiploDespacho").getValue()){
                Ext.getCmp("multiploDespacho").markInvalid("O Peso DCL está igual ao Peso Complementar, desta forma não pode ser despachado como multiplo despacho.");
                return false;
            }

            // quando o peso DCL menor do que o peso remanescente deve-se selecionar como multiplo despacho "Sim"
            if (parseFloat(Ext.getCmp("pesoDclVagao").getValue()) < pesoRemanescenteVagaoDespachosAnteriores 
                && !Ext.getCmp("multiploDespacho").getValue()){
                Ext.getCmp("multiploDespacho").markInvalid("Para fechar o multiplo despacho, o peso do DCL deve ser igual ao peso complementar.");
                return false;
            }

        }else{					

            if (Ext.getCmp("multiploDespacho").getValue()){
			
                // quando for multiplo despacho o peso total deve ser diferente porém não menor que o dcl
                if (parseFloat(Ext.getCmp("pesoDclVagao").getValue()) >= parseFloat(Ext.getCmp("pesoTotalVagao").getValue())){                
                    Ext.getCmp("pesoDclVagao").markInvalid("Peso do DCL deve ser menor que o Peso total do vagão.");
                    return false;
                }
            }
            else
            {
                // quando não for multiplo despacho obrigatoriamente o peso DCL deve ser igual ao peso total
                if (Ext.getCmp("pesoDclVagao").getValue() > Ext.getCmp("pesoTotalVagao").getValue())
				{
                    Ext.getCmp("pesoDclVagao").markInvalid("Peso do DCL deve ser igual ao Peso total do vagão.");
                    return false;
                }
				
            }
        }       

        if (indPreenchimentoVolume)
        {
            if (parseFloat(Ext.getCmp("volumeVagao").getValue()) <= 0 || parseFloat(Ext.getCmp("volumeVagao").getValue()) > 120){
                Ext.getCmp("volumeVagao").markInvalid("Volume do vagão inválido.");
                return false;
            }

            if (parseFloat(Ext.getCmp("volumeVagao").getValue()) < parseFloat(Ext.getCmp("pesoTotalVagao").getValue())){
                Ext.getCmp("pesoTotalVagao").markInvalid("Peso total do vagão não pode ser maior do que o volume.");
                return false;
            }
        }

        if (indPreenchimentoTara && indTaraObrigatorio){
            if (Ext.getCmp("taraVagao").getValue() <= 0 || Ext.getCmp("taraVagao").getValue() > 120){
                Ext.getCmp("taraVagao").markInvalid("Tara do vagão inválida.");
                return false;
            }
        }
			
        var mensagemErro = "";
        var contadorNotas = 0;
        var contadorIteracoes = 0;
        var contadorConteiners = 0;
        var somaPesoNotas = 0;
        var somaVolumeNotas = 0;
        var contadorNotasManuais = 0;
        var contadorNotasEletronicas = 0;
        gridNotasFiscais.getStore().each(
			function (record) {
                if (record.data.ChaveNfe != "" && record.data.ChaveNfe != null && record.data.ChaveNfe != undefined){
                    contadorNotasEletronicas++;
                }else{
                    contadorNotasManuais++;
                }

                if (record.data.SerieNotaFiscal != "" && record.data.NumeroNotaFiscal != ""){
                    contadorNotas++;
                    
                    var retorno = ValidaRegistro(record);
                    if (retorno!= null){
                        for(var i=0; i<retorno.length; i++)
                        {
                            Ext.fly(gridNotasFiscais.getView().getCell(contadorIteracoes,retorno[i])).setStyle({"background-color":"#E5B9B7"});
                            mensagemErro = mensagemErro + "Erro3";
                        }
                    }

                    if (record.data.Conteiner != null && record.data.Conteiner != ""){
                        contadorConteiners++;
                    }

                    if (!isNaN(record.data.PesoRateio))
                    {
                        var pesoRateioTmp = RoundWithDecimals(parseFloat(record.data.PesoRateio), 3);
                        somaPesoNotas += pesoRateioTmp;
                        // alert(pesoRateioTmp);
                    }

                    if (!isNaN(record.data.VolumeNotaFiscal))
                    {
                        somaVolumeNotas += RoundWithDecimals(parseFloat(record.data.VolumeNotaFiscal), 3);
                    }

                    contadorIteracoes++;
                }
			}
		);
			
        if (mensagemErro != ""){
            Ext.Msg.alert('Erro de validação', 'Os campos marcados em vermelho estão com valores inválidos, corrija antes de continuar.');
            return false;
        }

        if (contadorConteiners > 0 && contadorConteiners < contadorNotas){
            Ext.Msg.alert('Erro de validação', 'Necessário preencher os conteiners de todas notas quando houver uma nota com conteiner.');
            return false;
        }

        if (contadorNotas == 0){
            Ext.Msg.alert('Erro de validação', 'Necessário inserir pelo menos uma nota para o vagão.');
            return false;
        }

        if (indPreenchimentoVolume){
            if (RoundWithDecimals(parseFloat(Ext.getCmp("volumeVagao").getValue()), 3) != RoundWithDecimals(parseFloat(somaVolumeNotas),3)){
                Ext.Msg.alert('Erro de validação', 'A soma dos volumes das notas é diferente do volume do vagão informado.');
                Ext.getCmp("volumeVagao").markInvalid('A soma dos volumes das notas é diferente do volume do vagão informado.');
                return false;
            }
        }else{
            // alert(RoundWithDecimals(parseFloat(Ext.getCmp("pesoDclVagao").getValue()),3) + " - " + RoundWithDecimals(parseFloat(somaPesoNotas), 3));
            if (RoundWithDecimals(parseFloat(Ext.getCmp("pesoDclVagao").getValue()),3) != RoundWithDecimals(parseFloat(somaPesoNotas),3)){
                Ext.Msg.alert('Erro de validação', 'A soma dos pesos das notas é diferente do peso total do vagão informado.');
                Ext.getCmp("pesoDclVagao").markInvalid('A soma dos pesos das notas é diferente do peso total do vagão informado.');
                return false;
            }
        }

        if (contadorNotasManuais > 0 && contadorNotasEletronicas > 0){
            Ext.Msg.alert('Erro de validação', 'Todas as notas do vagão devem ser do mesmo tipo, ou NF-e ou de preenchimento manual.');
            return false;
        }

        return true;
    }

    function ValidaRegistro(rec){
        var erros = new Array();

        if (indPreenchimentoVolume){
            var idxVolumeNotaFsical = 4;
            if (indPreenchimentoTara) idxVolumeNotaFsical = idxVolumeNotaFsical+2;
            if (indFluxoInternacional) idxVolumeNotaFsical = idxVolumeNotaFsical+1;

            // O VOLUME DEVE SER PREENCHIDO
            if (rec.data.VolumeNotaFiscal == null || rec.data.VolumeNotaFiscal == "" || RoundWithDecimals(parseFloat(rec.data.VolumeNotaFiscal),3) == 0){
                erros.push(idxVolumeNotaFsical);
            }

        }else{
            var idxPeso = 4;
            if (indPreenchimentoTara) idxPeso = idxPeso+2;
            if (indFluxoInternacional) idxPeso = idxPeso+1;

            // O PESO DO RATEIO DEVE SER PREENCHIDO
            if (rec.data.PesoRateio == null || rec.data.PesoRateio == "" || RoundWithDecimals(parseFloat(rec.data.PesoRateio),3) == 0){
                
				erros.push(idxPeso);
            }

             var pesoTotal = RoundWithDecimals(parseFloat(rec.data.PesoTotal), 3);
            
            // O PESO DO RATEIO DEVE SER MENOR OU IGUAL AO PESO DA NOTA
            if (RoundWithDecimals(parseFloat(rec.data.PesoRateio), 3) > pesoTotal)
            {
				erros.push(idxPeso);
            }
        }

        // QUANDO O FLUXO FOR DE CONTEINER É NECESSÁRIO MARCAR O CAMPO COMO OBRIGATÓRIO
        if (indFluxoConteiner)
        {
            var idxConteiner = 3;
            if (indPreenchimentoTara) idxConteiner = idxConteiner+2;
            if (indFluxoInternacional) idxConteiner = idxConteiner+1;

            // O CONTEINER DEVE SER PREENCHIDO
            if (rec.data.Conteiner == null || rec.data.Conteiner == ""){
                erros.push(idxConteiner);
            }
        }

        // VERIFICA SE FOI PREENCHIDO O CAMPO DE PLACAS QUANDO OBRIGATORIO
        if (indPreenchimentoTara && indTaraObrigatorio){
            if (rec.data.PlacaCavalo == null || rec.data.PlacaCavalo == "" || rec.data.PlacaCavalo == 0){
                var idxPlacaCavalo = 3;
                erros.push(idxPlacaCavalo);
            }

            if (rec.data.PlacaCarreta == null || rec.data.PlacaCarreta == "" || rec.data.PlacaCarreta == 0){
                var idxPlacaCarreta = 4;
                erros.push(idxPlacaCarreta);
            }
        }

        // QUANDO FOR FLUXO INTERNACIONAL DEVE-SE VERIFICAR O SE O TIF FOI PREENCHIDO
        if (indFluxoInternacional && indFluxoInternacionalDestinoTifObrigatorio){
            var idxTif = 3
            if (indPreenchimentoTara) idxTif = idxTif+2;
            if (rec.data.TIF == null || rec.data.TIF == "" || rec.data.TIF == 0){
                erros.push(idxTif);
            }
        }

        if (erros.length > 0){
            return erros;
        }

        return null;
    }
    
    /**********************************************************
	STORE DE NOTAS FISCAIS - INICIO
	**********************************************************/
	var notasFiscaisStore = new Ext.data.JsonStore({

        fields: [
				'ChaveNfe',
                'Conteiner',
				'SerieNotaFiscal',
				'NumeroNotaFiscal',
				'PesoTotal',
                'PesoRateio',
                'ValorNotaFiscal',
                'ValorTotalNotaFiscal',
                'Remetente',
                'Destinatario',
                'CnpjRemetente',
                'CnpjDestinatario',
                'TIF',
                'SiglaRemetente',
                'SiglaDestinatario',
                'DataNotaFiscal',
                'PlacaCavalo',
                'PlacaCarreta',
                'UfRemetente',
                'UfDestinatario',
                'InscricaoEstadualRemetente',
                'InscricaoEstadualDestinatario',
                'IndObtidoAutomatico',
                'MultiploDespacho',
                'PesoDclVagao',
                'PesoTotalVagao',
                'VolumeNotaFiscal',
				'IndContingencia',
                'ChaveCte',
                'CteInseridoTelaNf'
				]
	});
	
	// notasFiscaisStore.load();
	/**********************************************************
	STORE DE NOTAS FISCAIS - FIM
	**********************************************************/
	
	/**********************************************************
	GRID DE NOTAS FISCAIS - INICIO
	**********************************************************/
	var detalheActionNotasFiscais = new Ext.ux.grid.RowActions({
		dataIndex: '',
		header: '',
		align: 'center',
		actions: [
            {
				iconCls: 'icon-detail',
				tooltip: 'Detalhes'
			},
            {
			    iconCls: 'icon-del',
			    tooltip: 'Excluir'
			}],
		callbacks: {
			'icon-detail': function(grid, record, action, row, col) {
                EditarDadosNota(record);
			},
            'icon-del': function(grid, record, action, row, col) {
				if (Ext.Msg.confirm("Excluir", "Deseja realmente excluir esta Nota Fiscal?", function(btn, text) {
					if (btn == 'yes') {
					    notasFiscaisStore.remove(record);
					}
				}));
			}
		}
	});
    
    function getNumberField(){
        return { 
            xtype: 'masktextfield',
            allowblank: false, 
            mask: '990,000',
		    money: true,
            maxLength: 7,
            value: "0,000",
            width: 70
             };
    }

    var arrColunasNf = new Array();
    arrColunasNf.push(new Ext.grid.RowNumberer());
    arrColunasNf.push(detalheActionNotasFiscais);
    arrColunasNf.push({ header: 'Chave NF-e', width: 330, dataIndex: "ChaveNfe", sortable: false, editor: new Ext.form.TextField({ vtype: 'ctevtype', autoCreate: { tag: 'input', type: 'text', maxlength: '44', autocomplete: 'off' }, allowblank: false }) });

    if (indPreenchimentoTara){
        arrColunasNf.push({ header: 'Placa Cavalo', width: 100, dataIndex: "PlacaCavalo", sortable: false, editor: new Ext.form.TextField({ allowblank: false, plugins: [new Ext.ux.InputTextMask('LLL-9999', true)] }) });
        arrColunasNf.push({ header: 'Placa Carreta', width: 100, dataIndex: "PlacaCarreta", sortable: false, editor: new Ext.form.TextField({ allowblank: false, plugins: [new Ext.ux.InputTextMask('LLL-9999', true)] }) });
    }

    if (indFluxoInternacional){
        arrColunasNf.push({ header: 'TIF', width: 100, dataIndex: "TIF", sortable: false, editor: new Ext.form.TextField({ allowblank: false }) });
    }

    arrColunasNf.push({ header: 'Conteiner', width: 100, dataIndex: "Conteiner", sortable: false, editor: new Ext.form.TextField({ allowblank: !indFluxoConteiner }) });

    if (indPreenchimentoVolume){
        arrColunasNf.push({ header: 'Volume', width: 100, dataIndex: "VolumeNotaFiscal", sortable: false, renderer: VolumeRenderer });
    }else{
        arrColunasNf.push({ header: 'Peso Rateio', width: 100, dataIndex: "PesoRateio", sortable: false, renderer: PesoRenderer, editor: getNumberField() });
        arrColunasNf.push({ header: 'Peso Total', width: 100, dataIndex: "PesoTotal", sortable: false, renderer: PesoRenderer });
    }
    
    arrColunasNf.push({ header: 'Série', width: 50, dataIndex: "SerieNotaFiscal", sortable: false });
    arrColunasNf.push({ header: 'Nro.', width: 50, dataIndex: "NumeroNotaFiscal", sortable: false });
    arrColunasNf.push({ header: 'Valor', dataIndex: "ValorTotalNotaFiscal", sortable: false, renderer: MonetarioRenderer });
    arrColunasNf.push({ header: 'Data', dataIndex: "DataNotaFiscal", sortable: false, renderer: DataRenderer });
    arrColunasNf.push({ header: 'Remetente', dataIndex: "Remetente", sortable: false });
    arrColunasNf.push({ header: 'Sigla Rem.', dataIndex: "SiglaRemetente", sortable: false});
    arrColunasNf.push({ header: 'CNPJ Rem.', dataIndex: "CnpjRemetente", sortable: false});
    arrColunasNf.push({ header: 'UF Rem.', dataIndex: "UfRemetente", sortable: false});
    arrColunasNf.push({ header: 'Insc. Est. Rem.', dataIndex: "InscricaoEstadualRemetente", sortable: false});
    arrColunasNf.push({ header: 'Destinatário', dataIndex: "Destinatario", sortable: false });
    arrColunasNf.push({ header: 'Sigla Dest.', dataIndex: "SiglaDestinatario", sortable: false });
    arrColunasNf.push({ header: 'CNPJ Dest.', dataIndex: "CnpjDestinatario", sortable: false });
    arrColunasNf.push({ header: 'UF Dest.', dataIndex: "UfDestinatario", sortable: false });
    arrColunasNf.push({ header: 'Insc. Est. Dest.', dataIndex: "InscricaoEstadualDestinatario", sortable: false });

    if ((indFluxoRateioCte) || (tipoServicoCte !== 0 ))  {
        arrColunasNf.push({ header: 'Chave CT-e', width: 330, dataIndex: "ChaveCte", sortable: false, editor: new Ext.form.TextField({ vtype: 'ctevtype', autoCreate: { tag: 'input', type: 'text', maxlength: '44', autocomplete: 'off' }, allowblank: false }) });
        arrColunasNf.push({ header: 'CteInseridoTelaNf', width: 330, dataIndex: "CteInseridoTelaNf", sortable: false, hidden: true, editor: new Ext.form.TextField({ vtype: 'ctevtype', autoCreate: { tag: 'input', type: 'text', maxlength: '44', autocomplete: 'off' }, allowblank: false }) });
    }

    var gridNotasFiscais = new Ext.grid.EditorGridPanel({
	    id: 'gridNotasFiscais',
	    name: 'gridNotasFiscais',
	    store: notasFiscaisStore,
	    clicksToEdit: 1,
	    columns: arrColunasNf,
	    plugins: [detalheActionNotasFiscais],
	    colModel: new Ext.grid.ColumnModel({
	        defaults: {
	            sortable: false
	        },
	        columns: this.columns
	    }),
	    stripeRows: false,
	    height: 230,
	    width: 600,
	    tbar: [{
	        id: 'btnNovaNF',
            text: 'Novo',
	        tooltip: 'Adicionar Nota Fiscal',
	        iconCls: 'icon-new',
	        handler: function (c) {

                InserirLinhaNfVazia();
	        }
	    }]
	});

    gridNotasFiscais.on('beforeedit', beforeEditNotaFiscal, this );
    gridNotasFiscais.on('validateedit', afterEditNotaFiscal, this );

    var theSelectionModel = gridNotasFiscais.getSelectionModel();
    theSelectionModel.onEditorKey = function(field, e)
    {
      var k = e.getKey(), newCell, g = theSelectionModel.grid, ed = g.activeEditor;
      if(k == e.TAB){
            e.stopEvent();
            ed.completeEdit();
      }
    };

	/**********************************************************
	GRID DE NOTAS FISCAIS - FIM
	**********************************************************/
	
	/**********************************************************
	FORM DADOS DAS NOTAS FISCAIS - INICIO
	**********************************************************/
    var arrCamposFormVagao = new Array();
    var txtPesoTotalVagao = 
    {
		xtype: 'masktextfield',
		id: 'pesoTotalVagao',
        fieldLabel: 'Peso Tot Vg',
        name: 'pesoTotalVagao',
        allowBlank: false,
		mask: '990,000',
		money: true,
        maxLength: 7,
        disabled: true,
        value: "0,000",
        width: 50
	};

    var colunaPesoTotalVagao = { width: 70, layout: 'form', border: false, items: [txtPesoTotalVagao] };
    arrCamposFormVagao.push(colunaPesoTotalVagao);

    if (indVagaoDespachado){
        var txtPesoRemanescenteVagao = 
        {
		    xtype: 'masktextfield',
		    id: 'pesoRemanescenteVagao',
            fieldLabel: 'Peso Compl',
            name: 'pesoRemanescenteVagao',
            allowBlank: false,
		    mask: '990,000',
		    money: true,
            maxLength: 7,
            disabled: true,
            value: pesoRemanescenteVagaoDespachosAnteriores,
            width: 50
	    };

        var colunaPesoRemanescenteVagao = { width: 70, layout: 'form', border: false, items: [txtPesoRemanescenteVagao] };
        arrCamposFormVagao.push(colunaPesoRemanescenteVagao);
    }

    var txtPesoDclVagao = 
    {
		xtype: 'masktextfield',
		id: 'pesoDclVagao',
        fieldLabel: 'Peso DCL',
        name: 'pesoDclVagao',
        allowBlank: false,
		mask: '990,000',
		money: true,
        maxLength: 7,
        value: "0,000",
        width: 50,
        listeners:{
            'blur': function (field) {
                if (!indVagaoDespachado && !Ext.getCmp("multiploDespacho").getValue())
                {
                    Ext.getCmp("pesoTotalVagao").setValue(field.getValue());
                }
            }
        }
	};

    var colunaPesoDclVagao = { width: 70, layout: 'form', border: false, items: [txtPesoDclVagao] };
    arrCamposFormVagao.push(colunaPesoDclVagao);

    var cboMultiploDespacho = {
        id: 'multiploDespacho',
        xtype: 'combo',
        name: 'multiploDespacho',
        allowBlank: false,
        fieldLabel: 'Mult. Desp.',
        store: dsSimNao,
        displayField: 'text',
        valueField: 'value',
        forceSelection: true,
        mode: 'local',
        triggerAction: 'all',
        value: false,
        typeAhead: false,
        width: 50,
        listeners:{
            select: function (combo, record, index) {
                if (!indVagaoDespachado){
                    Ext.getCmp("pesoTotalVagao").setDisabled(!record.data.value);

					if (indLongStack && indMercadoriaPallets)
					{
						if (!record.data.value){
							Ext.getCmp("pesoDclVagao").setValue(2);
							Ext.getCmp("pesoTotalVagao").setValue(2);
						}else{
							Ext.getCmp("pesoDclVagao").setValue(1);
							Ext.getCmp("pesoTotalVagao").setValue(1);
						}
					}
                }else{
					if (indLongStack && indMercadoriaPallets){
						Ext.getCmp("pesoDclVagao").setValue(1);
						Ext.getCmp("pesoTotalVagao").setValue(1);
					}
				}
            }
        }
    };
    var colunaMultiploDespacho = { width: 70, layout: 'form', border: false, items: [cboMultiploDespacho] };
    arrCamposFormVagao.push(colunaMultiploDespacho);
    
    if (indPreenchimentoVolume){
        var txtVolumeVagao = {
            xtype: 'masktextfield',
            id: 'volumeVagao',
            fieldLabel: 'Volume',
            name: 'volumeVagao',
            allowBlank: false,
            width: 50,
            mask: '990,000',
		    money: true,
            maxLength: 7,
            enableKeyEvents: true,
            listeners:{
                'blur': function (field) {
                    if (indVerificaPesoPorVolume)
                    {
                        var valorPeso = CalcularPesoPorDensidade(field.getValue());
                        if (valorPeso != null){
                            Ext.getCmp("pesoTotalVagao").setValue(valorPeso);
                            Ext.getCmp("pesoDclVagao").setValue(valorPeso);
                        }
                    }
                }
            }
        };
        var colunaVolumeVagao = { width: 70, layout: 'form', border: false, items: [txtVolumeVagao] };
        arrCamposFormVagao.push(colunaVolumeVagao);
    }

    if (indPreenchimentoTara){
        var txtTaraVagao = {
            xtype: 'masktextfield',
            id: 'taraVagao',
            fieldLabel: 'Tara',
            name: 'taraVagao',
            allowBlank: false,
            width: 50,
            mask: '990,000',
		    money: true,
            maxLength: 7,
            value: "0,000"
        };
        var colunaTaraVagao = { width: 110, layout: 'form', border: false, items: [txtTaraVagao] };
        arrCamposFormVagao.push(colunaTaraVagao);
    }

    var linha1 = {
        layout: 'column',
        border: false,//
        items: arrCamposFormVagao
    };


    var labelHtml = '';
    //if ((indFluxoRateioCte) || (tipoServicoCte != 0))  {
    //    labelHtml = '<div>Clique nos campos para editar os valores dos campos: Chave NF-e, Contêiner, Peso Rateio e Chave CT-e.</div>';
    //} else {
        labelHtml = '<div>Clique nos campos para editar os valores dos campos: Chave NF-e, Contêiner e Peso Rateio.</div>';
    //}

    var label = {
		xtype: 'label',
		html: labelHtml,
		height: 15
	};

    var linha2 = {
        layout: 'column',
        border: false,
        items: label
    };

	var formNotasFiscais = new Ext.form.FormPanel
	({
        id: 'formNotasFiscais',
		labelWidth: 80,
		width: 630,
		bodyStyle: 'padding: 15px',
        labelAlign: 'top',
		items: [linha1, linha2, gridNotasFiscais],
		buttonAlign: "center",
		buttons: 
		[{
			text: "Confirmar",
			handler: function() {
				SalvarNotas();
			}
		}, 
		{
			text: "Cancelar",
			handler: function() {
				windowNotasFiscais.close();
			}
		}]
	});
    
    dsNotasVagoes.filter("IdVagao", <%=ViewData["IdVagao"] %>);
    var cont = 0;
    var taraTemp = "";
    var volumeTemp = "";
    var pesoTotalVagaoTemp = "";
    var pesoDclVagaoTemp = "";
    var multipoDespachoVagaoTemp = "";
    dsNotasVagoes.each(
		function (record) {
	        ImportarRegistroParaPopupNotas(record);
            pesoDclVagaoTemp = record.data.PesoDclVagao;
            pesoTotalVagaoTemp = record.data.PesoTotalVagao;
            
            multipoDespachoVagaoTemp = record.data.MultiploDespacho;

            if (indPreenchimentoTara) taraTemp = record.data.TaraVagao;
            if (indPreenchimentoVolume) volumeTemp = record.data.VolumeVagao;

            cont++;
		}
	);
    if (indPreenchimentoTara) Ext.getCmp("taraVagao").setValue(taraTemp);
    if (indPreenchimentoVolume && volumeTemp!='') Ext.getCmp("volumeVagao").setValue(volumeTemp);
    

    if (pesoDclVagaoTemp!='') Ext.getCmp("pesoDclVagao").setValue(pesoDclVagaoTemp);
    

    if (multipoDespachoVagaoTemp!='') Ext.getCmp("multiploDespacho").setValue(multipoDespachoVagaoTemp);
    if (indPreenchimentoVolume){ Ext.getCmp("multiploDespacho").disable(); }
    if (Ext.getCmp("multiploDespacho").getValue()){Ext.getCmp("pesoTotalVagao").enable();}
    dsNotasVagoes.clearFilter();

    if (indVagaoDespachado){
		if (indLongStack && indMercadoriaPallets){
			Ext.getCmp("pesoDclVagao").setValue(1);
			pesoTotalVagaoTemp = 1;
			Ext.getCmp("pesoDclVagao").disable();
		}

        Ext.getCmp("pesoTotalVagao").setValue(pesoTotalVagaoDespachoAnterior);
    }else{
		if (indLongStack && indMercadoriaPallets){
			Ext.getCmp("pesoDclVagao").setValue(2);
			pesoTotalVagaoTemp = 2;
			Ext.getCmp("pesoDclVagao").disable();
		}

        if (pesoTotalVagaoTemp!='') Ext.getCmp("pesoTotalVagao").setValue(pesoTotalVagaoTemp);
    }

    // INSERE LINHAS VAZIAS
    var dif = 1-cont;
    if (dif > 0){
        for (var i=0; i<dif; i++){
            InserirLinhaNfVazia();
        }    
    }else{
        InserirLinhaNfVazia();
    }

	/**********************************************************
	FORM DADOS DAS NOTAS FISCAIS - FIM
	**********************************************************/	
	formNotasFiscais.render("div-grid-nota-fiscal");

</script>
