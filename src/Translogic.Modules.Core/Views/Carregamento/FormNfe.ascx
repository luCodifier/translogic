﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div id="div-form-nfe"></div>
<script type="text/javascript">
    /**********************************************************
	FORM DADOS DA NF-e - INICIO
    PesoBruto
    PesoLiquido
	**********************************************************/
	var unidadeMedida;
	if (indPreenchimentoVolume) {
		unidadeMedida = " volume ";	
	} else {
		unidadeMedida = " peso ";
	}

    var labelNfe = {
        xtype: 'label',
        html: '<div style="color:red;">O cliente não informou o ' + unidadeMedida + ' desta NF-e.</div><div>Preencha o' + unidadeMedida + 'para continuar a operação.</div>',
        height: 15
    };
	/*
    var edPesoLiquidoNfe = {
        xtype: 'masktextfield',
        id: 'edPesoLiquidoNfe',
        fieldLabel: 'Peso Líquido',
        name: 'edPesoLiquidoNfe',
        width: 70,
        mask: '990,000',
		money: true,
        maxLength: 7
    };
	*/
    var edPesoBrutoNfe = {
        xtype: 'masktextfield',
        id: 'edPesoBrutoNfe',
        fieldLabel: unidadeMedida +' total da NFe',
        allowBlank: false,
        name: 'edPesoBrutoNfe',
        width: 150,
        labelWidth: 100,
        mask: '990,000',
		money: true,
        maxLength: 7
    };

    var formNotaFiscalEletronica = new Ext.form.FormPanel
	({
	    id: 'formNotaFiscalEletronica',
	    labelWidth: 80,
	    width: 285,
	    height: 150,
	    bodyStyle: 'padding: 15px',
	    items:
		[
            labelNfe,
            edPesoBrutoNfe
		],
	    buttonAlign: "center",
	    buttons:
			[{
			    text: "Salvar",
			    iconCls: 'icon-save',
			    handler: function () {
			    	if (ValidarPesosNfe(Ext.getCmp('edPesoBrutoNfe'))) {
			    		SalvarPesosNfe(Ext.getCmp('edPesoBrutoNfe').getValue(), Ext.getCmp('edPesoBrutoNfe').getValue(), chaveNfeEdit);
			            windowNfe.close();
			        }
			    }
			}]
	});

	/**********************************************************
	FORM DADOS DAS NOTAS FISCAIS - FIM
	**********************************************************/
    formNotaFiscalEletronica.render("div-form-nfe");

</script>
