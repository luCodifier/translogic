﻿<%@ Page Title="" Language="C#" Inherits="System.Web.Mvc.ViewPage" MasterPageFile="~/Views/Shared/Interna.master" %>

<%@ Import Namespace="System.Globalization" %>
<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .vagao-invalido
        {
            color: red;
        }
        
        .campo-numero-internacional
        {
            text-align: right;
        }
    </style>
    <% Html.RenderPartial("FormPesquisaEmpresa"); %>
    <%
        var idFluxoComercial = (int)ViewData["ID_FLUXO_COMERCIAL"];
        var codigoFluxo = (string)ViewData["CODIGO_FLUXO"];

        string serieDefault = ViewData["CONT_SERIE"] != null ? ViewData["CONT_SERIE"].ToString() : string.Empty;
        string numeroDefault = ViewData["CONT_NUMERO"] != null ? ViewData["CONT_NUMERO"].ToString() : string.Empty;
        DateTime? dataDefault = ViewData["CONT_DATA"] != null ? (DateTime?)ViewData["CONT_DATA"] : null;
        string pesoDefault = ViewData["CONT_PESO"] != null ? ViewData["CONT_PESO"].ToString() : string.Empty;

        var indLiberaDataDespacho = (bool)ViewData["IND_LIBERA_DATA_DESPACHO"];
        var indFluxoCte = (bool)ViewData["IND_FLUXO_CTE"];
        var indFluxoRateioCte = (bool)ViewData["IND_RATEIO_CTE"];
        var indFluxoInternacional = (bool)ViewData["IND_FLUXO_INTERNACIONAL"];
        var indFluxoInternacionalDestinoTifObrigatorio = (bool)ViewData["IND_FLUXO_INTERNACIONAL_TIF_OBRIGATORIO"];
        var indFluxoOrigemIntercambio = (bool)ViewData["IND_FLUXO_ORIGEM_INTERCAMBIO"];
        var indPreenchimentoVolume = (bool)ViewData["IND_PREENCHIMENTO_VOLUME"];
        var indPreenchimentoTara = (bool)ViewData["IND_PREENCHIMENTO_TARA"];
        var indTaraObrigatorio = (bool)ViewData["IND_TARA_OBRIGATORIO"];
        var indNfeObrigatorio = (bool)ViewData["IND_NFE_OBRIGATORIO"];
        var indFluxoConteiner = (bool)ViewData["IND_FLUXO_CONTEINER"];
        var indLiberarTravaCorrentista = (bool)ViewData["IND_LIBERAR_TRAVA_CORRENTISTA"];
        var margemPercLiberacaoFat = (double)ViewData["MARGEM_PERC_LIB_FAT"];

        var cnpjRemetenteFiscal = (string)ViewData["CNPJ_REMETENTE_FISCAL"];
        var siglaRemetenteFiscal = (string)ViewData["SIGLA_REMETENTE_FISCAL"];
        var ieRemetenteFiscal = (string)ViewData["IE_REMETENTE_FISCAL"];
        var nomeRemetenteFiscal = (string)ViewData["NOME_REMETENTE_FISCAL"];
        var dscRemetenteFiscal = (string)ViewData["DSC_REMETENTE_FISCAL"];
        var ufRemetenteFiscal = (string)ViewData["UF_REMETENTE_FISCAL"];
        var pjRemetenteFiscal = (bool?)ViewData["PJ_REMETENTE_FISCAL"];

        var cnpjDestinatarioFiscal = (string)ViewData["CNPJ_DESTINATARIO_FISCAL"];
        var siglaDestinatarioFiscal = (string)ViewData["SIGLA_DESTINATARIO_FISCAL"];
        var ieDestinatarioFiscal = (string)ViewData["IE_DESTINATARIO_FISCAL"];
        var nomeDestinatarioFiscal = (string)ViewData["NOME_DESTINATARIO_FISCAL"];
        var dscDestinatarioFiscal = (string)ViewData["DSC_DESTINATARIO_FISCAL"];
        var ufDestinatarioFiscal = (string)ViewData["UF_DESTINATARIO_FISCAL"];
        var pjDestinatarioFiscal = (bool?)ViewData["PJ_DESTINATARIO_FISCAL"];

        var cfopContrato = (string)ViewData["CFOP_CONTRATO"];

        var dataLimiteNf = (DateTime)ViewData["LIMITADOR_DATA_NF"];
        var mesesRetroativos = (int)ViewData["MESES_RETROATIVOS"];
        var idOrigem = (int)ViewData["ID_ESTACAO_ORIGEM"];
        var codigoOrigem = (string)ViewData["CODIGO_ESTACAO_ORIGEM"];
        var idDestino = (int)ViewData["ID_ESTACAO_DESTINO"];
        var codigoDestino = (string)ViewData["CODIGO_ESTACAO_DESTINO"];

        var idMercadoria = (int)ViewData["ID_MERCADORIA"];
        var descricaoMercadoria = (string)ViewData["DESCRICAO_MERCADORIA"];
        var codigoMercadoria = (string)ViewData["CODIGO_MERCADORIA"];
        var libNotaMaiorQue180Dias = (string)ViewData["LIB_NOTA_MAIOR_QUE_180_DIAS"];

        var indVerificaPesoPorVolume = (bool)ViewData["IND_VERIFICA_PESO_POR_VOLUME"];
        var indValidarCfopContrato = (bool)ViewData["IND_VALIDAR_CFOP_CONTRATO"];
        var indValidarCnpjRemetente = (bool)ViewData["IND_VALIDAR_CNPJ_REMETENTE"];
        var indValidarCnpjDestinatario = (bool)ViewData["IND_VALIDAR_CNPJ_DESTINATARIO"];
        var indTravaSaldoNfe = (bool)ViewData["IND_TRAVA_SALDO_NFE"];
        var variacaoCfop = (string)ViewData["VARIACAO_CFOP"];
        var indLongStack = (bool)ViewData["IND_LONG_STACK"];
        var indMercadoriaPallets = (bool)ViewData["IND_MERCADORIA_PALLETS"];
        var tipoNotaFiscal = 99;
        var tipoServicoCte = (int)ViewData["TIPO_SERVICO_CTE"];

    %>
    <script type="text/javascript" language="javascript">
        function VoltarTela() {
            if (dsNotasVagoes.getCount() > 0) {
                if (Ext.Msg.confirm("Excluir", "Você possui vagões a serem despachados, deseja realmente sair?", function(btn, text) {
                    if (btn == 'yes') {
                        var url = '<%= Url.Action("Index") %>';
                        document.location.href = url;
                    }
                })) ;
            } else {
                var url = '<%= Url.Action("Index") %>';
                document.location.href = url;
            }
        }

        var indLiberaDataDespacho = <%= indLiberaDataDespacho ? "true" : "false" %>;
        var indFluxoCte = <%= indFluxoCte ? "true" : "false" %>;
        var margemPercLiberacaoFat = "<%= margemPercLiberacaoFat.ToString(CultureInfo.InvariantCulture) %>";
        var indFluxoInternacionalDestinoTifObrigatorio = <%= indFluxoInternacionalDestinoTifObrigatorio ? "true" : "false" %>;
        var indFluxoOrigemIntercambio = <%= indFluxoOrigemIntercambio ? "true" : "false" %>;
        var indFluxoInternacional = <%= indFluxoInternacional ? "true" : "false" %>;
        var indPreenchimentoVolume = <%= indPreenchimentoVolume ? "true" : "false" %>;
        var indPreenchimentoTara = <%= indPreenchimentoTara ? "true" : "false" %>;
        var indTaraObrigatorio = <%= indTaraObrigatorio ? "true" : "false" %>;
        var indNfeObrigatorio = <%= indNfeObrigatorio ? "true" : "false" %>;

        var indVerificaPesoPorVolume = <%= indVerificaPesoPorVolume ? "true" : "false" %>;

        var indValidarCfopContrato = <%= indValidarCfopContrato ? "true" : "false" %>;
        var indValidarCnpjRemetente = <%= indValidarCnpjRemetente ? "true" : "false" %>;
        var indValidarCnpjDestinatario = <%= indValidarCnpjDestinatario ? "true" : "false" %>;
        var indLiberarTravaCorrentista = <%= indLiberarTravaCorrentista ? "true" : "false" %>;
        var indLongStack = <%= indLongStack ? "true" : "false" %>;
        var indMercadoriaPallets = <%= indMercadoriaPallets ? "true" : "false" %>;
        var indFluxoConteiner = <%= indFluxoConteiner ? "true" : "false" %>;

        var indFluxoRateioCte =  <%= indFluxoRateioCte ? "true" : "false" %>;
        var cnpjRemetenteFiscal = "<%= cnpjRemetenteFiscal %>";
        var siglaRemetenteFiscal = "<%= siglaRemetenteFiscal %>";
        var ieRemetenteFiscal = "<%= ieRemetenteFiscal %>";
        var nomeRemetenteFiscal = "<%= nomeRemetenteFiscal %>";
        var dscRemetenteFiscal = "<%= dscRemetenteFiscal %>";
        var ufRemetenteFiscal = "<%= ufRemetenteFiscal %>";
        var pjRemetenteFiscal = <%= pjRemetenteFiscal.HasValue ? pjRemetenteFiscal.Value ? "true" : "false" : "false" %>;

        var cnpjDestinatarioFiscal = "<%= cnpjDestinatarioFiscal %>";
        var siglaDestinatarioFiscal = "<%= siglaDestinatarioFiscal %>";
        var ieDestinatarioFiscal = "<%= ieDestinatarioFiscal %>";
        var nomeDestinatarioFiscal = "<%= nomeDestinatarioFiscal %>";
        var dscDestinatarioFiscal = "<%= dscDestinatarioFiscal %>";
        var ufDestinatarioFiscal = "<%= ufDestinatarioFiscal %>";
        var pjDestinatarioFiscal = <%= pjDestinatarioFiscal.HasValue ? pjDestinatarioFiscal.Value ? "true" : "false" : "false" %>;

        var indTravaSaldoNfe = <%= indTravaSaldoNfe ? "true" : "false" %>;
        var codigoMercadoria = "<%= codigoMercadoria %>";
        var libNotaMaiorQue180Dias = "<%= libNotaMaiorQue180Dias %>";
        
        var cfopContrato = "<%= cfopContrato %>";

        var dataLimiteNf = new Date(<%= dataLimiteNf.Year %>, <%= dataLimiteNf.Month - 1 %>, <%= dataLimiteNf.Day %>, 0, 0, 0, 0);
        var mesesRetroativos = <%= mesesRetroativos %>;

        var idOrigem = <%= idOrigem %>;
        var idDestino = <%= idDestino %>;   
        var tipoServicoCte = <%= tipoServicoCte %>;      
        var variacaoCfop = "<%= variacaoCfop %>";


// deve vir no formato ###;#;#  - ex: 650;1;5
        var dataInicioDespacho = new Date();
        var dataInicioVagao = "";
        var arrListaNotasVagao = new Array();

        var codigoVagaoEdit = "";
        var arrListaVagoes = new Array();

        var dataInicioNf = "";
        var windowNotasFiscais = null;
    </script>
    <% Html.RenderPartial("FormNotaFiscal"); %>
    <script type="text/javascript" language="javascript">

        function LogarTempoDespacho(dataInicio, dataTermino, codigoFluxo, idOrigem, idDestino, idMercadoria, listaVagoes) {
            return {
                DataInicioString: dataInicio,
                DataTerminoString: dataTermino,
                CodigoFluxo: codigoFluxo,
                IdOrigem: idOrigem,
                IdDestino: idDestino,
                IdMercadoria: idMercadoria,
                ListaVagoes: listaVagoes
            };
        }

        function LogarTempoVagao(dataInicio, dataTermino, codigoVagao, listaNotas) {
            return {
                DataInicioString: dataInicio,
                DataTerminoString: dataTermino,
                CodigoVagao: codigoVagao,
                ListaNotas: listaNotas
            };
        }

        function LogarTempoNotaFiscal(dataInicio, dataTermino, indManual, chaveNfe, erro, mensagemExibida, problemaEncontrado) {
            return {
                DataInicioString: dataInicio,
                DataTerminoString: dataTermino,
                IndManual: indManual,
                ChaveNfe: chaveNfe,
                Erro: erro,
                MensagemExibida: mensagemExibida,
                ProblemaEncontrado: problemaEncontrado
            };
        }

        function VerificarVariacoesCfop(vCfop) {
            var arrDadosVariacao = variacaoCfop.split(";");
            var prefixo = arrDadosVariacao[0];
            var iniDados = arrDadosVariacao[1];
            var fimDados = arrDadosVariacao[2];
            for (var x = iniDados; x <= fimDados; x++) {
                var cfopValido = prefixo.toString() + "" + x.toString();
                // alert(cfopValido + vCfop);
                var regexCfop = new RegExp(cfopValido);
                var matchCfop = regexCfop.exec(vCfop);
                if (matchCfop != null) {
                    return true;
                }
            }

            return false;
        }

        function PesoRenderer(val) {
            return Ext.util.Format.number(val, '0.000,000/i');
        }

        function VolumeRenderer(val) {
            return Ext.util.Format.number(val, '0.000,000/i');
        }

        function MonetarioRenderer(val) {
            if (val == null || val == "") {
                return "";
            }

            return "R$ " + Ext.util.Format.number(val, '0.000,00/i');
        }

        function DataRenderer(val) {
            if (val == null || val == "") {
                return "";
            }

            return Ext.util.Format.date(val, "d/m/Y");
        }

        function ValidarCampoMaiorZero(nomeCampo) {
            var erroMenorZero = false;

            if (Ext.getCmp(nomeCampo).isValid()) {
                var valorCampo = Ext.getCmp(nomeCampo).getValue();
                if (isNaN(valorCampo)) {
                    Ext.getCmp(nomeCampo).markInvalid("O campo deve ser um valor numérico.");
                    erroMenorZero = true;
                } else {
                    if (valorCampo <= 0) {
                        Ext.getCmp(nomeCampo).markInvalid("O campo deve ser maior que zero.");
                        erroMenorZero = true;
                    }
                }
            }

            // alert(erroMenorZero);
            return !erroMenorZero;
        }

        /**
        FUNÇÕES CHAMADAS DA WINDOW DE NOTAS - INICIO
        */

        function RoundWithDecimals(val, dec) {
            var decimals = Math.pow(10, dec);
            var result = Math.round(val * decimals) / decimals;
            return result;
        }

        function CalcularPesoPorDensidade(volume) {
            if (volume != "" && volume > 0) {
                var responseText = $.ajax({
                    url: "<%= Url.Action("ObterPesoPorVolume") %>",
                    type: "POST",
                    async: false,
                    dataType: 'json',
                    data: ({
                        volume: volume,
                        codigoMercadoria: codigoMercadoria
                    })
                }).responseText;

                var result = Ext.util.JSON.decode(responseText);
                // var result = eval(tmp);
                if (result.Erro) {
                    Ext.Msg.alert('Ops...', result.Mensagem);
                    return null;
                } else {
                    return result.Peso;
                }
            }

        }

        function VerificarConteinerVazio() {
                var responseText = $.ajax({
				    url: "<%= Url.Action("VerificarFluxoDeConteinerVazio") %>",
				    type: "POST",
				    async: false,
                    dataType: 'json',
                    data: ({codigoFluxoComercial: Ext.getCmp("codigoFluxoComercial").getValue()})
			    }).responseText;

                var result = Ext.util.JSON.decode(responseText);
            
            if (result.Erro){
                    Ext.Msg.alert('Mensagem de Erro', result.Mensagem);
            } else {
                return result.FluxoConteinerVazio;
            }
        }

        function VerificarConteiner(codigoConteiner) {
            if (codigoConteiner != "") {

                codigoConteiner = codigoConteiner.toUpperCase();
                var responseText = $.ajax({
                    url: "<%= Url.Action("ObterDadosConteiner") %>",
                    type: "POST",
                    async: false,
                    dataType: 'json',
                    data: ({
                        codigoConteiner: codigoConteiner,
                        codAoOrigem: idOrigem
                    })
                }).responseText;

                var result = Ext.util.JSON.decode(responseText);

                if (result.Erro) {
                    Ext.Msg.alert('Ops...', result.Mensagem);
                    return false;
                }

                if (result.Voar) {
                    var msgErro = result.Mensagem + "\nDeseja reposicionar o contêiner?";
                    var idConteiner = result.idConteiner;

                    var blnRet = confirm(msgErro);

                    if (blnRet) {
                        var responseText = $.ajax({
                            url: "<%= Url.Action("VoarConteiner") %>",
                            type: "POST",
                            async: false,
                            dataType: 'json',
                            data: ({
                                idConteiner: idConteiner,
                                codAoOrigem: idOrigem
                            }),
                            success: function(result) {

                                if (result.Erro) {
                                    Ext.Msg.alert('Ops...', result.Mensagem);
                                    return false;
                                } else {
                                    return true;
                                }
                            }
                        }).responseText;

                    } else// Fim botão yes
                    {

                        return false;
                    }
                    // } // Fim  IF Confirmação

                } // Fim IF Voar

                return true;
            }
        }

        var recordEdit = null;

        function EditarDadosNota(record, indMostrarMascara) {
            recordEdit = record;
           
            if (indNfeObrigatorio) {
                if (!record.data.IndObtidoAutomatico) {
                    Ext.Msg.alert('Aviso', 'Todas as notas fiscais deste fluxo devem ser via NFe');
                    return;
                }
            }

            if (!record.data.IndObtidoAutomatico) {
                dataInicioNf = new Date();
            }

            windowNotaFiscal = new Ext.Window({
                id: 'windowNotaFiscal',
                title: 'Editar dados da Nota Fiscal',
                modal: true,
                width: 444,
                closeAction: 'hide',
                height: 550,
                items: [formNotaFiscal],
                listeners: {
                    'beforeshow': function() {
                        var serie = '<%= serieDefault %>';

                        if (serie != "") {
                            Ext.getCmp("edSerieNota").setValue('<%= serieDefault %>');
                            Ext.getCmp("edNumeroNota").setValue('<%= numeroDefault %>');
                            Ext.getCmp("edDataNota").setValue('<%= dataDefault.HasValue ? dataDefault.Value.ToString("dd/MM/yyyy") : null %>');
                            Ext.getCmp("edPesoTotal").setValue('<%= pesoDefault %>');
                            Ext.getCmp("edPesoRateio").setValue('<%= pesoDefault %>');
                        }
                    },
                    'hide': function() {
                        Ext.getCmp("formNotaFiscal").getForm().reset();
                        if (!record.data.IndObtidoAutomatico) {
                            SalvarProcessamentoNfe(false, "", dataInicioNf, new Date(), false, "", "");
                        }
                    }
                }
                /*,
				autoLoad: {
					url: '<%= Url.Action("FormNotaFiscal") %>',
					scripts: true
				}*/
            });
            windowNotaFiscal.show();

            AtualizarDadosNotas(recordEdit);

            if (indMostrarMascara) {
                Ext.getCmp("formNotaFiscal").getEl().mask("Obtendo dados da Nota Fiscal Eletronicamente", "x-mask-loading");
            }
        }

        function AtualizarDadosNotas(recordEdit) {

            if (recordEdit != null) {


                // ATUALIZA OS CAMPOS 
                AtualizarCampoNotaFiscal("edNfChaveNfe", recordEdit.data.ChaveNfe);
                AtualizarCampoNotaFiscal("edConteiner", recordEdit.data.Conteiner);
                AtualizarCampoNotaFiscal("edSerieNota", recordEdit.data.SerieNotaFiscal);
                AtualizarCampoNotaFiscal("edNumeroNota", recordEdit.data.NumeroNotaFiscal);

                if (indPreenchimentoVolume) {
                    AtualizarCampoNotaFiscal("edVolumeNotaFiscal", recordEdit.data.VolumeNotaFiscal);
                } else {
                    AtualizarCampoNotaFiscal("edPesoTotal", recordEdit.data.PesoTotal);
                    
                    if (recordEdit.data.ConteinerVazio) {
                        AtualizarCampoNotaFiscal("edPesoRateio", recordEdit.data.PesoTotal);
                        Ext.getCmp("edPesoRateio").disable();
                    }
                    else {
                      AtualizarCampoNotaFiscal("edPesoRateio", recordEdit.data.PesoRateio);   
                    }
                }

                AtualizarCampoNotaFiscal("edValorTotalNota", recordEdit.data.ValorTotalNotaFiscal);
                AtualizarCampoNotaFiscal("edValorNota", recordEdit.data.ValorNotaFiscal);
                AtualizarCampoNotaFiscal("edRemetente", recordEdit.data.Remetente);
                AtualizarCampoNotaFiscal("edDestinatario", recordEdit.data.Destinatario);
                AtualizarCampoNotaFiscal("edCnpjRemetente", recordEdit.data.CnpjRemetente);
                AtualizarCampoNotaFiscal("edCnpjDestinatario", recordEdit.data.CnpjDestinatario);
                if (recordEdit.data.DataNotaFiscal != null && recordEdit.data.DataNotaFiscal != "") {
                    AtualizarCampoNotaFiscal("edDataNota", recordEdit.data.DataNotaFiscal.format('d/m/Y'));
                }
                AtualizarCampoNotaFiscal("edSiglaRemetente", recordEdit.data.SiglaRemetente);
                AtualizarCampoNotaFiscal("edSiglaDestinatario", recordEdit.data.SiglaDestinatario);
                AtualizarCampoNotaFiscal("edUfRemetente", recordEdit.data.UfRemetente);
                AtualizarCampoNotaFiscal("edUfDestinatario", recordEdit.data.UfDestinatario);
                AtualizarCampoNotaFiscal("edInscricaoEstadualRemetente", recordEdit.data.InscricaoEstadualRemetente);
                AtualizarCampoNotaFiscal("edInscricaoEstadualDestinatario", recordEdit.data.InscricaoEstadualDestinatario);
                
                if (indFluxoInternacional) {
                    AtualizarCampoNotaFiscal("edTif", recordEdit.data.TIF);
                }

                if (indPreenchimentoTara) {
                    AtualizarCampoNotaFiscal("edPlacaCavalo", recordEdit.data.PlacaCavalo);
                    AtualizarCampoNotaFiscal("edPlacaCarreta", recordEdit.data.PlacaCarreta);
                    // if (!indTaraObrigatorio) {
                    // Ext.getCmp("edPlacaCavalo")
                    // }
                }
                AtualizarCampoNotaFiscal("edCteChaveCte", recordEdit.data.ChaveCte);

                Ext.getCmp("edNfChaveNfe").disable();
                if (recordEdit.data.IndObtidoAutomatico) {
                    Ext.getCmp("edSerieNota").disable();
                    Ext.getCmp("edNumeroNota").disable();
                    Ext.getCmp("edCnpjRemetente").disable();

                    if (recordEdit.data.IndContingencia) {
                        CarregarDadosCnpjEdicao(recordEdit.data.CnpjRemetente, idOrigem, 'Remetente');
                        if (indPreenchimentoVolume) {
                            Ext.getCmp("edVolumeNotaFiscal").enable();
                        } else {
                            Ext.getCmp("edPesoTotal").enable();
                        }

                        Ext.getCmp("edValorNota").enable();
                        Ext.getCmp("edCnpjDestinatario").enable();
                        Ext.getCmp("edDataNota").enable();
                        Ext.getCmp("edValorTotalNota").enable();
                    } else {
                        if (indPreenchimentoVolume) {
                            Ext.getCmp("edVolumeNotaFiscal").disable();
                        } else {
                            Ext.getCmp("edPesoTotal").disable();
                        }

                        Ext.getCmp("edValorNota").disable();
                        Ext.getCmp("edCnpjDestinatario").disable();
                        Ext.getCmp("edDataNota").disable();
                        Ext.getCmp("edValorTotalNota").disable();
                    }
                } else {

                    Ext.getCmp("edCnpjRemetente").setValue(cnpjRemetenteFiscal);
                    Ext.getCmp("edInscricaoEstadualRemetente").setValue(ieRemetenteFiscal);
                    Ext.getCmp("edRemetente").setValue(nomeRemetenteFiscal);
                    Ext.getCmp("edSiglaRemetente").setValue(siglaRemetenteFiscal);
                    Ext.getCmp("edUfRemetente").setValue(ufRemetenteFiscal);

                    Ext.getCmp("edCnpjDestinatario").setValue(cnpjDestinatarioFiscal);
                    Ext.getCmp("edInscricaoEstadualDestinatario").setValue(ieDestinatarioFiscal);
                    Ext.getCmp("edDestinatario").setValue(nomeDestinatarioFiscal);
                    Ext.getCmp("edSiglaDestinatario").setValue(siglaDestinatarioFiscal);
                    Ext.getCmp("edUfDestinatario").setValue(ufDestinatarioFiscal);

                    if (indValidarCnpjRemetente) {
                        Ext.getCmp("edCnpjRemetente").disable();
                    }

                    if (indValidarCnpjDestinatario) {
                        Ext.getCmp("edCnpjDestinatario").disable();
                    }
                }
            }
        }

        var chaveNfeEdit = "";

        function EditarDadosNfe(chaveNfe, record) {
            recordEdit = record;
            chaveNfeEdit = chaveNfe;
            windowNfe = new Ext.Window({
                id: 'windowNfe',
                title: 'Editar dados da NF-e',
                modal: true,
                closable: false,
                width: 299,
                height: 180,
                autoLoad: {
                    url: '<%= Url.Action("FormNfe") %>',
                    scripts: true
                }
            });
            windowNfe.show();
        }

        function SalvarPesosNfe(pesoLiquido, pesoBruto, chaveNfeInformada) {
            if (chaveNfeInformada != "") {
                var responseText = $.ajax({
                    url: "<%= Url.Action("SalvarPesosNfe") %>",
                    type: "POST",
                    async: false,
                    dataType: 'json',
                    data: ({
                        pesoLiquido: pesoLiquido,
                        pesoBruto: pesoBruto,
                        chaveNfe: chaveNfeInformada,
                        indVolume: indPreenchimentoVolume
                    })
                }).responseText;

                var result = Ext.util.JSON.decode(responseText);
                // var result = eval(tmp);
                if (result.Erro) {
                    Ext.Msg.alert('Ops...', result.Mensagem);
                } else {
                    if (!indPreenchimentoVolume) {
                        Ext.getCmp("edPesoTotal").setValue(pesoBruto);
                        recordEdit.data.PesoTotal = pesoBruto;
                        recordEdit.commit();
                    } else {
                        Ext.getCmp("edVolumeNotaFiscal").setValue(pesoBruto);
                        recordEdit.data.VolumeNotaFiscal = pesoBruto;
                        recordEdit.commit();
                    }

                }

                return !result.Erro;
                // return true;
            }

            recordEdit = null;
            chaveNfeEdit = "";
        }

        function ValidarPesosNfe(campoPesoBruto) {

            var pesoBruto = campoPesoBruto.getValue();

            if (pesoBruto == null || pesoBruto == "" || parseFloat(pesoBruto) <= 0) {
                campoPesoBruto.markInvalid("Peso total da Nfe deve ser maior do que zero.");
                return false;
            }
            /*  var pesoLiquido = campoPesoLiquido.getValue();
            var pesoBruto = campoPesoBruto.getValue();
            if (pesoLiquido == null || pesoLiquido == "" || parseFloat(pesoLiquido) <= 0) {
                campoPesoLiquido.markInvalid("Peso líquido deve ser maior do que zero.");
                return false;
            }

           
            if (parseFloat(pesoLiquido) > parseFloat(pesoBruto)) {
                campoPesoBruto.markInvalid("Peso bruto deve ser maior do que o peso líquido.");
                return false;
            }
			*/
            return true;
        }

        function RemoverNotasVagao(idVagao) {
            //remove do store as notas
            dsNotasVagoes.each(
                function(record) {
                    if (record.data.IdVagao == idVagao) {
                        dsNotasVagoes.remove(record);
                    }
                }
            );
        }

        function beforeEditNotaFiscal(e)
        {
           if (e.field == "ChaveCte"){
               e.cancel = true;
           }

//           if (e.field == "ChaveCte"){
//               if (e.record.data.CteInseridoTelaNf != 'S') {
//                   e.cancel = true;
//               } else {


//               }
//           }
        }

        function afterEditNotaFiscal(e) {
            /*
            e.grid - This grid
            e.record - The record being edited
            e.field - The field name being edited
            e.value - The value being set
            e.originalValue - The original value for the field, before the edit.
            e.row - The grid row index
            e.column - The grid column index
            */
            // execute an XHR to send/commit data to the server, in callback do (if successful):
            if (e.field == "ChaveNfe") {
                // var w = Ext.getCmp('gridNotasFiscais');
                e.grid.getEl().mask();

                ObterDadosNfe(e.value, e, e.grid);
            }
            if (e.field == "PesoRateio") {
                if (VerificarConteinerVazio()) {
                    e.cancel = true;
                }
            }
            if (e.field == "Conteiner") {
                e.grid.getEl().mask();

                if (!VerificarConteiner(e.value)) {
                    e.grid.getEl().unmask();
                    e.cancel = true;
                } else {
                    e.grid.getEl().unmask();
                }
            }
            if (e.field == "ChaveCte") {
                if (e.value == '') {
                    e.cancel = true;
                }
            }
        }

        function CriarCampoNfe() {
            return new Ext.form.TextField({
                allowblank: false
            });
        }

        function ImportarRegistroParaTelaPrincipal(recordPopup, idVagao, codigoVagao, tara, volume, multiploDespacho, pesoDcl, pesoTotalVagao) {

            var chaveNfe = recordPopup.data.ChaveNfe;
            var pesoTotalNota = parseFloat(recordPopup.data.PesoTotal);
            var pesoTotalUtilizado = parseFloat(recordPopup.data.PesoRateio);
            var volumeTotalUtilizado = parseFloat(recordPopup.data.VolumeNotaFiscal);
            var blnValido = true;

            /*if (!indFluxoConteiner)
			{
				dsNotasVagoes.each(			
					function (record) {				 
						if (chaveNfe != "" && chaveNfe != undefined && record.data.ChaveNfe == chaveNfe &&  record.data.IdVagao == idVagao ) {
							Ext.Msg.alert('Aviso', 'A NFe ('+chaveNfe+') já foi utilizada neste vagão, portanto não é possível utiliza-la no vagão '+codigoVagao); 
							blnValido = false;						
						}
					}
				);
			}
			
			if(indTravaSaldoNfe && chaveNfe != null && chaveNfe != "")
			{
				dsNotasVagoes.each(			
					function (record) {				 
						if (record.data.ChaveNfe == chaveNfe && record.data.CodigoVagao != codigoVagao) {
						    if (indPreenchimentoVolume) {
						        volumeTotalUtilizado += parseFloat(record.data.VolumeNotaFiscal); 
						    }
						    else 
						    {
						        pesoTotalUtilizado += parseFloat(record.data.PesoRateio);
						    }
						}
					}
				);

                if (indPreenchimentoVolume) {
                    if (volumeTotalUtilizado > parseFloat(recordPopup.data.VolumeNotaFiscal)) {
                        Ext.Msg.alert('Aviso', 'Não é possível utilizar a NF-e(' + chaveNfe + '), pois todo o volume já foi utilizado.');
                        blnValido = false;
                    }
				}
				else 
                {
                    if (pesoTotalUtilizado > pesoTotalNota) {
                        Ext.Msg.alert('Aviso', 'Não é possível utilizar a NF-e(' + chaveNfe + '), pois todo o peso já foi utilizado.');
                        blnValido = false;
                    }
                }
			}
            
			if(!blnValido)
			{
				return false;
			}*/

            var rec = new dsNotasVagoes.recordType({
                IdVagao: idVagao,
                CodigoVagao: codigoVagao,
                ChaveNfe: recordPopup.data.ChaveNfe,
                SerieNotaFiscal: recordPopup.data.SerieNotaFiscal,
                NumeroNotaFiscal: recordPopup.data.NumeroNotaFiscal,
                PesoTotal: recordPopup.data.PesoTotal,
                PesoRateio: recordPopup.data.PesoRateio,
                ValorNotaFiscal: recordPopup.data.ValorNotaFiscal,
                ValorTotalNotaFiscal: recordPopup.data.ValorTotalNotaFiscal,
                Remetente: recordPopup.data.Remetente,
                Destinatario: recordPopup.data.Destinatario,
                CnpjRemetente: recordPopup.data.CnpjRemetente,
                CnpjDestinatario: recordPopup.data.CnpjDestinatario,
                SiglaRemetente: recordPopup.data.SiglaRemetente,
                SiglaDestinatario: recordPopup.data.SiglaDestinatario,
                UfRemetente: recordPopup.data.UfRemetente,
                UfDestinatario: recordPopup.data.UfDestinatario,
                InscricaoEstadualRemetente: recordPopup.data.InscricaoEstadualRemetente,
                InscricaoEstadualDestinatario: recordPopup.data.InscricaoEstadualDestinatario,
                DataNotaFiscal: recordPopup.data.DataNotaFiscal,
                Conteiner: recordPopup.data.Conteiner,
                TIF: recordPopup.data.TIF,
                PlacaCavalo: recordPopup.data.PlacaCavalo,
                PlacaCarreta: recordPopup.data.PlacaCarreta,
                VolumeNotaFiscal: recordPopup.data.VolumeNotaFiscal,
                TaraVagao: tara,
                VolumeVagao: volume,
                IndObtidoAutomatico: recordPopup.data.IndObtidoAutomatico,
                MultiploDespacho: multiploDespacho,
                PesoTotalVagao: pesoTotalVagao,
                PesoDclVagao: pesoDcl,
                IndContingencia: recordPopup.data.IndContingencia,
                ChaveCte: recordPopup.data.ChaveCte
            });
            rec.commit();
            dsNotasVagoes.add(rec);

            return true;
        }

        function ImportarRegistroParaPopupNotas(record) {
            var rec = new notasFiscaisStore.recordType({
                ChaveNfe: record.data.ChaveNfe,
                SerieNotaFiscal: record.data.SerieNotaFiscal,
                NumeroNotaFiscal: record.data.NumeroNotaFiscal,
                PesoTotal: record.data.PesoTotal,
                PesoRateio: record.data.PesoRateio,
                ValorNotaFiscal: record.data.ValorNotaFiscal,
                ValorTotalNotaFiscal: record.data.ValorTotalNotaFiscal,
                Remetente: record.data.Remetente,
                Destinatario: record.data.Destinatario,
                CnpjRemetente: record.data.CnpjRemetente,
                CnpjDestinatario: record.data.CnpjDestinatario,
                SiglaRemetente: record.data.SiglaRemetente,
                SiglaDestinatario: record.data.SiglaDestinatario,
                UfRemetente: record.data.UfRemetente,
                UfDestinatario: record.data.UfDestinatario,
                InscricaoEstadualRemetente: record.data.InscricaoEstadualRemetente,
                InscricaoEstadualDestinatario: record.data.InscricaoEstadualDestinatario,
                DataNotaFiscal: record.data.DataNotaFiscal,
                Conteiner: record.data.Conteiner,
                TIF: record.data.TIF,
                PlacaCavalo: record.data.PlacaCavalo,
                PlacaCarreta: record.data.PlacaCarreta,
                VolumeNotaFiscal: record.data.VolumeNotaFiscal,
                IndObtidoAutomatico: record.data.IndObtidoAutomatico,
                MultiploDespacho: record.data.MultiploDespacho,
                PesoTotalVagao: record.data.PesoTotalVagao,
                PesoDclVagao: record.data.PesoDclVagao,
                IndContingencia: record.data.IndContingencia,
                ChaveCte: record.data.ChaveCte
            });
            rec.commit();
            gridNotasFiscais.getStore().add(rec);
        }

        function ConverteDataStr(data) {
            return Date.parseDate(data, "d/m/Y");
        }

        function SalvarProcessamentoNfe(indAutomatico, chaveNfe, dataInicio, dataTermino, erro, mensagemExibida, problemaEncontrado) {
            // alert(2);
            var strDataInicioNf = dataInicioNf.format('d/m/Y H:i:s');
            var dataTerminoNf = new Date();
            var strDataTerminoNf = dataTerminoNf.format('d/m/Y H:i:s');
            arrListaNotasVagao.push(LogarTempoNotaFiscal(strDataInicioNf, strDataTerminoNf, !indAutomatico, chaveNfe, erro, mensagemExibida, problemaEncontrado));

            if (indAutomatico) {
                var log =
                {
                    MensagemExibidaTela: mensagemExibida,
                    ChaveNfe: chaveNfe,
                    DataInicioString: strDataInicioNf,
                    DataTerminoString: strDataTerminoNf,
                    ProblemaEncontrado: problemaEncontrado,
                    CodigoFluxo: '<%= codigoFluxo %>'
                };

                var logNfe = $.toJSON(log);

                //todo: chamar ajax para gravar mesmo q nao seja despachado
                var responseText = $.ajax({
                    url: "<%= Url.Action("SalvarLogNotaFiscalEletronica") %>",
                    type: "POST",
                    async: false,
                    dataType: 'json',
                    data: log
                }).responseText;
            }

            dataInicioNf = "";
        }

        function ObterDadosNfe(chaveNfe, e, w) {
            dataInicioNf = new Date();
            if (chaveNfe == null || chaveNfe == "") {
                e.record.data.SerieNotaFiscal = "";
                e.record.data.NumeroNotaFiscal = "";
                e.record.data.PesoTotal = "";
                e.record.data.PesoRateio = "";
                e.record.data.VolumeNotaFiscal = "";
                e.record.data.ValorNotaFiscal = "";
                e.record.data.ValorTotalNotaFiscal = "";
                e.record.data.Remetente = "";
                e.record.data.Destinatario = "";
                e.record.data.CnpjRemetente = "";
                e.record.data.CnpjDestinatario = "";
                e.record.data.SiglaRemetente = "";
                e.record.data.SiglaDestinatario = "";
                e.record.data.DataNotaFiscal = "";
                e.record.data.UfRemetente = "";
                e.record.data.UfDestinatario = "";
                e.record.data.InscricaoEstadualRemetente = "";
                e.record.data.InscricaoEstadualDestinatario = "";
                e.record.data.TipoNfe = "";
                e.record.data.IndObtidoAutomatico = false;
                e.record.data.IndContingencia = false;
                e.record.commit();
                e.cancel = true;
                w.getEl().unmask();
                return;
            }
            e.record.data.IndObtidoAutomatico = true;
            // indMostrarMascara = true;
            Ext.getCmp("formNotaFiscal").getForm().reset();
            EditarDadosNota(e.record, true);
            Ext.getCmp("edinfcnpjDestinatario").hide();
            $.ajax({
                url: "<%= Url.Action("ObterDadosNfe") %>",
                type: "POST",
                dataType: 'json',
                async: true,
                data: { chaveNfe: chaveNfe, codigoFluxo: Ext.getCmp("codigoFluxoComercial").getValue() },
                timeout: 300000,
                success: function(data) {
                    w.getEl().unmask();
                    // var data = Ext.decode(response.responseText);

                    if (data.CodigoStatusRetorno == 3) {
                        if (confirm(data.Mensagem + "\n\nDevido à este problema, é necessário fazer o preenchimento manual da nota fiscal.\n\n A nfe foi emitida em contigência?")) {
                            // Habilita a edição da Nfe
                            HabilitarEdicaoNfe(e, dataInicioNf, data);
                        } else {
                            windowNotaFiscal.hide();

                            SalvarProcessamentoNfe(true, e.record.data.ChaveNfe, dataInicioNf, new Date(), true, data.Mensagem, data.StackTrace);

                            e.record.data.ChaveNfe = '';
                            e.record.commit();
                        }
                        return;
                    }

                    if (!data.Erro) {
                        var mensagemErro = "";
                        tipoNotaFiscal = data.DadosNfe.TipoNfe;
                        if (indValidarCnpjRemetente) {
                            if (ufRemetenteFiscal == "EX" && data.DadosNfe.TipoNfe == 0) {
                                if (data.DadosNfe.CnpjRemetente != cnpjDestinatarioFiscal) {
                                    mensagemErro += "<br /> - CNPJ do Remetente da NFe(" + data.DadosNfe.CnpjRemetente + ") não é o mesmo do Destinatário Fiscal do Fluxo(" + cnpjDestinatarioFiscal + ").";
                                }
                            } else {
                                if (cnpjRemetenteFiscal != data.DadosNfe.CnpjRemetente) {
                                    // Ext.Msg.alert('Erro', "O CNPJ do Remetente da NFe não é o mesmo do Remetente Fiscal do Fluxo.");
                                    mensagemErro += "<br /> - CNPJ do Remetente da NFe(" + data.DadosNfe.CnpjRemetente + ") não é o mesmo do Remetente Fiscal do Fluxo(" + cnpjRemetenteFiscal + ").";
                                }
                            }
                        }

                        if (indValidarCnpjDestinatario) {
                            if (ufRemetenteFiscal == "EX" && data.DadosNfe.TipoNfe == "0") {
                                if (data.DadosNfe.CnpjDestinatario != cnpjRemetenteFiscal) {
                                    mensagemErro += "<br /> - CNPJ do Destinatário da NFe(" + data.DadosNfe.CnpjDestinatario + ") não é o mesmo do Remetente Fiscal do Fluxo(" + cnpjRemetenteFiscal + ").";
                                }
                            } else {
                                if (cnpjDestinatarioFiscal != data.DadosNfe.CnpjDestinatario) {
                                    mensagemErro += "<br /> - CNPJ do Destinatário da NFe(" + data.DadosNfe.CnpjDestinatario + ") não é o mesmo do Destinatário Fiscal do Fluxo(" + cnpjDestinatarioFiscal + ").";
                                }
                            }
                        } else {
                            if (ufDestinatarioFiscal == "EX" && data.DadosNfe.UfDestinatario != "EX" && !indLiberarTravaCorrentista) {
                                mensagemErro += "<br /> - Destinatário da NFe(" + e.record.data.ChaveNfe + ") deve ser Exterior.";
                            }
                        }

                        if (indValidarCfopContrato) {
                                if (variacaoCfop == "") {
                                    var regexCfop = new RegExp(cfopContrato);
                                    var matchCfop = regexCfop.exec(data.DadosNfe.Cfop);
                                    if (matchCfop == null) {
                                        // Ext.Msg.alert('Erro', "O CFOP da NF-e não é o mesmo do contrato do Fluxo.");
                                        mensagemErro += "<br /> - CFOP da NF-e não é o mesmo do contrato do Fluxo.";
                                    }
                                } else {
                                    if (!VerificarVariacoesCfop(data.DadosNfe.Cfop)) {
                                        mensagemErro += "<br /> - CFOP da NF-e não é o mesmo do contrato do Fluxo.";
                                    }
                                }
                            }

                            var dataNf = ConverteDataStr(data.DadosNfe.DataNotaFiscal);
                            if (libNotaMaiorQue180Dias == "N" && dataNf < dataLimiteNf) {
                                // Ext.Msg.alert('Erro', "A Data Nota Fiscal não pode ultrapassar de " + mesesRetroativos + " meses.");
                                mensagemErro += "<br /> - Data Nota Fiscal não pode ultrapassar de " + mesesRetroativos + " meses.";
                            }

                            if (gridNotasFiscais.getStore().getCount() > 1) {

                                gridNotasFiscais.getStore().each(
                                    function(recordTmp) {

                                        if (recordTmp.data.ChaveNfe == ''
                                            && recordTmp.data.SerieNotaFiscal == ''
                                            && recordTmp.data.NumeroNotaFiscal == ''
                                            && (recordTmp.data.PesoTotal == null || recordTmp.data.PesoTotal == '')
                                            && recordTmp.data.ValorNotaFiscal == 0) {
                                            return true;
                                        }

                                        if (e.record.data.ChaveNfe != recordTmp.data.ChaveNfe || recordTmp.data.ChaveNfe == "") {
                                            if (recordTmp.data.CnpjRemetente != data.DadosNfe.CnpjRemetente) {
                                                mensagemErro += "<br /> - CNPJ do Remetente da NFe(" + data.DadosNfe.CnpjRemetente + ") não é o mesmo do Remetente Fiscal da nota anterior(" + recordTmp.data.CnpjRemetente + ").";
                                            }

                                            if (recordTmp.data.CnpjDestinatario != data.DadosNfe.CnpjDestinatario) {
                                                mensagemErro += "<br /> - CNPJ do Destinatário da NFe(" + data.DadosNfe.CnpjDestinatario + ") não é o mesmo do Destinatário Fiscal da nota anterior(" + recordTmp.data.CnpjDestinatario + ").";
                                            }

                                            if (recordTmp.data.CnpjRemetente != data.DadosNfe.CnpjRemetente || recordTmp.data.CnpjDestinatario != data.DadosNfe.CnpjDestinatario) {
                                                mensagemErro += "<br /> - Para NF-es de clientes diferentes, é necessário utilizar a opção de Múltiplo Despacho.";
                                                return false;
                                            }
                                        }
                                    }
                                );
                            }

                            if (mensagemErro != "") {
                                //e.record.data.ChaveNfe = '';
                                //e.record.commit;
                                windowNotaFiscal.hide();

                                Ext.Msg.alert('Erro', "Problemas encontrados na NF-e (" + e.record.data.ChaveNfe + "):<br />" + mensagemErro);

                                SalvarProcessamentoNfe(true, e.record.data.ChaveNfe, dataInicioNf, new Date(), true, mensagemErro, "");

                                e.record.data.ChaveNfe = '';
                                e.record.commit();
                                return;
                            }

                            e.record.data.SerieNotaFiscal = data.DadosNfe.SerieNotaFiscal;
                            e.record.data.NumeroNotaFiscal = data.DadosNfe.NumeroNotaFiscal;
                            e.record.data.VolumeNotaFiscal = data.DadosNfe.Volume;
                            if (e.record.data.PesoRateio == "") {
                                e.record.data.PesoRateio = data.DadosNfe.Peso;
                            }
                            e.record.data.PesoTotal = data.DadosNfe.Peso;
                            if (indPreenchimentoVolume) {
                                e.record.data.ValorTotalNotaFiscal = data.DadosNfe.Valor;
                            }
                            e.record.data.ValorTotalNotaFiscal = data.DadosNfe.Valor;
                            e.record.data.Remetente = data.DadosNfe.Remetente;
                            e.record.data.Destinatario = data.DadosNfe.Destinatario;
                            e.record.data.CnpjRemetente = data.DadosNfe.CnpjRemetente;
                            e.record.data.CnpjDestinatario = data.DadosNfe.CnpjDestinatario;
                            e.record.data.SiglaRemetente = data.DadosNfe.SiglaRemetente;
                            e.record.data.SiglaDestinatario = data.DadosNfe.SiglaDestinatario;
                            e.record.data.DataNotaFiscal = ConverteDataStr(data.DadosNfe.DataNotaFiscal);
                            e.record.data.UfRemetente = data.DadosNfe.UfRemetente;
                            e.record.data.UfDestinatario = data.DadosNfe.UfDestinatario;
                            e.record.data.InscricaoEstadualRemetente = data.DadosNfe.InscricaoEstadualRemetente;
                            e.record.data.InscricaoEstadualDestinatario = data.DadosNfe.InscricaoEstadualDestinatario;
                            e.record.data.TipoNfe = data.DadosNfe.TipoNfe;
                            e.record.data.IndObtidoAutomatico = true;
                            e.record.data.IndContingencia = false;
                            e.record.data.ConteinerVazio = data.DadosNfe.ConteinerVazio;
                            e.record.commit();

                            if (!indPreenchimentoVolume && data.DadosNfe.Peso == null) {
                                EditarDadosNfe(chaveNfe, e.record);
                            }

                            if (indPreenchimentoVolume && data.DadosNfe.Volume == null) {
                                EditarDadosNfe(chaveNfe, e.record);
                            }

                            Ext.getCmp("formNotaFiscal").getEl().unmask();
                            AtualizarDadosNotas(recordEdit);
                            if (indPreenchimentoVolume)
                                Ext.getCmp("edVolumeNotaFiscal").focus();
                            else
                                Ext.getCmp("edPesoRateio").focus();


                            SalvarProcessamentoNfe(true, e.record.data.ChaveNfe, dataInicioNf, new Date(), false, "", "");
                            // ValidarFormularioNota();
                        } else {

                            if (!data.IndLiberaDigitacao) {
                                windowNotaFiscal.hide();
                                Ext.Msg.alert('Ops...', data.Mensagem);

                                SalvarProcessamentoNfe(true, e.record.data.ChaveNfe, dataInicioNf, new Date(), true, data.Mensagem, data.StackTrace);

                                e.record.data.ChaveNfe = '';
                                e.record.commit();
                            } else {
                                if (data.Mensagem != null && data.Mensagem != "") {
                                    var mensagem = data.Mensagem + "<br /><br />Devido à este problema, é necessário fazer o preenchimento manual da nota fiscal.";
                                    Ext.Msg.alert('Ops...', mensagem);
                                }

                                e.record.data.IndObtidoAutomatico = false;

                                // Habilita a edição da Nfe
                                HabilitarEdicaoNfe(e, dataInicioNf, data);

                            }
                        }
                    }
                ,
                    error: function(jqXHR, textStatus, thrownError) {
                        var mensagemDeErro;
                        var url = "<%= Url.Action("ObterDadosNfe", null, null, "http") %>";

                        if (textStatus == "timeout")
                            mensagemDeErro = "Ops... Não foi possível carregar o endereço \"" + url + "\" à tempo!\n A nota será liberada para digitação.";
                        else if (textStatus != "")
                            mensagemDeErro = "Ocorreu um erro inesperado: " + jqXHR.statusText + " - " + (eval("(" + jqXHR.responseText + ")")).message;
                        else
                            mensagemDeErro = "Ocorreu um erro inesperado.";

                        var data = { Mensagem: mensagemDeErro, StackTrace: "" };

                        alert(mensagemDeErro);

                        $.getJSON("<%= Url.Action("PodeLiberarDigitacaoNfe") %>", { codigoErro: "V06" }, function(retorno) {
                            if (retorno) {
                                e.record.data.IndObtidoAutomatico = false;
                                HabilitarEdicaoNfe(e, dataInicioNf, data);
                            } else {
                                windowNotaFiscal.hide();
                                SalvarProcessamentoNfe(true, e.record.data.ChaveNfe, dataInicioNf, new Date(), true, mensagemDeErro, "");
                                alert("A digitação manual da nota não está liberada!");
                                e.record.data.ChaveNfe = '';
                                e.record.commit();
                            }
                        });
                    }
                });


        }

        function HabilitarEdicaoNfe(e, dataInicioNf, data) {
            SalvarProcessamentoNfe(true, e.record.data.ChaveNfe, dataInicioNf, new Date(), true, data.Mensagem, data.StackTrace);

            // Ext.getCmp("edBtnCnpjRemetente").show();

            /*********************************************************
			lógica da chave nfe
			var str="51110677294254001166550000000507871514461751";
			document.write("codigo da cidade ibge: "+str.substr(0,2) +"<br />");
			document.write("ano/mes: "+str.substr(2,4) +"<br />");
			document.write("cnpj emitente: "+str.substr(6,14) +"<br />");
			document.write("modelo nf: "+str.substr(20,2) +"<br />");
			document.write("serie nf: "+str.substr(22,3) +"<br />");
			document.write("numero nf: "+str.substr(25,9) +"<br />");
			document.write("cod numerico: "+str.substr(34,9) +"<br />");
			document.write("DV: "+str.substr(43,1) +"<br />");
			**********************************************************/
            var chaveNfeTmp = e.record.data.ChaveNfe;
            var cnpjEmitenteNotaFiscal = chaveNfeTmp.substr(6, 14);
            var serieNotaFiscal = chaveNfeTmp.substr(22, 3);
            var numeroNotaFiscal = chaveNfeTmp.substr(25, 9);

            e.record.data.CnpjRemetente = cnpjEmitenteNotaFiscal;
            e.record.data.SerieNotaFiscal = serieNotaFiscal;
            e.record.data.NumeroNotaFiscal = numeroNotaFiscal;
            e.record.data.DataNotaFiscal = null;
            e.record.data.IndObtidoAutomatico = true;
            e.record.data.IndContingencia = true;
            e.record.commit();

            AtualizarDadosNotas(e.record);
            if (indPreenchimentoVolume)
                Ext.getCmp("edVolumeNotaFiscal").focus();
            else
                Ext.getCmp("edPesoRateio").focus();


            Ext.getCmp("formNotaFiscal").getEl().unmask();

        }

/**
        FUNÇÕES CHAMADAS DA WINDOW DE NOTAS - FIM
        */

        var arrDetalhesFormulario = new Array();
        /****
        INICIO LINHA 1
        ****/
        var dsSimNao = new Ext.data.ArrayStore({
            fields: ['value', 'text'],
            data: [[true, 'Sim'], [false, 'Não']]
        });

        var txtCodigoFluxo = {
            xtype: 'textfield',
            id: 'codigoFluxoComercial',
            fieldLabel: 'Fluxo',
            name: 'codigoFluxoComercial',
            value: "<% = codigoFluxo %>",
            width: 60,
            cls: 'x-item-disabled',
            readOnly: true
        };

        var txtOrigem = {
            xtype: 'textfield',
            id: 'codigoOrigem',
            fieldLabel: 'Origem',
            name: 'codigoOrigem',
            value: "<%= codigoOrigem %>",
            width: 50,
            cls: 'x-item-disabled',
            readOnly: true
        };

        var txtDestino = {
            xtype: 'textfield',
            id: 'codigoDestino',
            fieldLabel: 'Destino',
            name: 'codigoDestino',
            value: "<%= codigoDestino %>",
            width: 50,
            cls: 'x-item-disabled',
            readOnly: true
        };

        var txtMercadoria = {
            xtype: 'textfield',
            id: 'descricaoMercadoria',
            fieldLabel: 'Mercadoria',
            name: 'descricaoMercadoria',
            value: '<%= descricaoMercadoria %>',
            width: 200,
            cls: 'x-item-disabled',
            readOnly: true
        };

        var txtRemFiscal = {
            xtype: 'textfield',
            id: 'descricaoRemFiscal',
            fieldLabel: 'Remetente',
            name: 'descricaoRemFiscal',
            value: dscRemetenteFiscal,
            width: 100,
            cls: 'x-item-disabled',
            readOnly: true
        };

        var txtDestFiscal = {
            xtype: 'textfield',
            id: 'descricaoDestFiscal',
            fieldLabel: 'Destinatário',
            name: 'descricaoDestFiscal',
            value: dscDestinatarioFiscal,
            width: 100,
            cls: 'x-item-disabled',
            readOnly: true
        };

        var coluna1 = { width: 70, layout: 'form', border: false, items: [txtCodigoFluxo] };
        var coluna2 = { width: 60, layout: 'form', border: false, items: [txtOrigem] };
        var coluna3 = { width: 60, layout: 'form', border: false, items: [txtDestino] };
        var coluna4 = { width: 210, layout: 'form', border: false, items: [txtMercadoria] };
        var coluna5 = { width: 110, layout: 'form', border: false, items: [txtRemFiscal] };
        var coluna6 = { width: 110, layout: 'form', border: false, items: [txtDestFiscal] };

        var linha1 = {
            layout: 'column',
            border: false,
            items: [coluna1, coluna2, coluna3, coluna4, coluna5, coluna6]
        };

        arrDetalhesFormulario.push(linha1);
        /***
        FIM DA LINHA 1
        ****/

        var txtDataCarregamento = {
            xtype: 'xdatetime',
            id: 'dataCarregamento',
            name: 'dataCarregamento',
            allowBlank: false,
            fieldLabel: 'Dt. Carregamento',
            timeFormat: 'H:i:s',
            timeConfig: { altFormats: 'H:i:s', allowBlank: false },
            dateFormat: 'd/m/Y',
            dateConfig: { altFormats: 'd/m/Y', allowBlank: false },
            disabled: !indLiberaDataDespacho,
            value: new String('<%= String.Format("{0:dd/MM/yyyy HH:mm:ss}", DateTime.Now) %>')
        };

        var l1_col1 = { width: 210, layout: 'form', border: false, items: [txtDataCarregamento] };

        if (!indFluxoCte) {

            var cboDespachoGrupo = {
                id: 'despachoEmGrupo',
                xtype: 'combo',
                name: 'despachoEmGrupo',
                allowBlank: false,
                fieldLabel: 'Despacho em Grupo',
                store: dsSimNao,
                displayField: 'text',
                valueField: 'value',
                forceSelection: true,
                mode: 'local',
                triggerAction: 'all',
                value: false,
                typeAhead: false,
                width: 50
            };

            var l1_col2 = { width: 140, layout: 'form', border: false, items: [cboDespachoGrupo] };

            var linha1_1 = {
                layout: 'column',
                border: false,
                items: [l1_col1, l1_col2]
            };
        } else {
            var linha1_1 = {
                layout: 'column',
                border: false,
                items: [l1_col1]
            };
        }

        arrDetalhesFormulario.push(linha1_1);

        if (indFluxoOrigemIntercambio) {
            /****
            INICIO LINHA INTERCAMBIO
            ****/
            var dsFerrovias = new Ext.data.JsonStore({
                url: '<%= Url.Action("ObterFerroviasIntercambio") %>',
                root: "Items",
                fields: [
                    { name: 'Id' },
                    { name: 'DescricaoResumida' }
                ]
            });

            var dsRegimeIntercambio = new Ext.data.ArrayStore({
                fields: ['value', 'text'],
                data: [['A', 'Aluguel'],
                    ['P', 'Pool'],
                    ['T', 'Trackage Right']]
            });

            var dsLinhasIntercambio = new Ext.data.JsonStore({
                url: '<%= Url.Action("ObterLinhasIntercambio") %>',
                root: "Items",
                fields: [
                    { name: 'Id' },
                    { name: 'Codigo' }
                ]
            });

            dsLinhasIntercambio.proxy.on('beforeload', function(p, params) {
                params['idAreaOperacional'] = idOrigem;
            });

            var txtSerieIntercambio = {
                autoCreate: { tag: 'input', type: 'text', maxlength: '3', autocomplete: 'off' },
                xtype: 'textfield',
                id: 'serieIntercambio',
                fieldLabel: 'Série Int.',
                style: 'text-transform: uppercase',
                name: 'serieIntercambio',
                allowBlank: false,
                width: 70
            };

            var txtDespachoIntercambio = {
                autoCreate: { tag: 'input', type: 'text', maxlength: '7', autocomplete: 'off' },
                xtype: 'textfield',
                id: 'despachoIntercambio',
                fieldLabel: 'Desp. Int.',
                name: 'despachoIntercambio',
                allowBlank: false,
                width: 70
            };

            var cboFerroviaIntercambio = {
                id: 'ferroviaIntercambio',
                allowBlank: false,
                xtype: 'combo',
                name: 'ferroviaIntercambio',
                fieldLabel: 'Ferrovia Interc.',
                store: dsFerrovias,
                displayField: 'DescricaoResumida',
                valueField: 'Id',
                forceSelection: true,
                loadingText: 'Buscando...',
                triggerAction: 'all',
                typeAhead: false,
                validationEvent: false,
                width: 150
            };

            var cboRegimeIntercambio = {
                id: 'regimeIntercambio',
                xtype: 'combo',
                name: 'regimeIntercambio',
                fieldLabel: 'Regime Interc.',
                store: dsRegimeIntercambio,
                displayField: 'text',
                valueField: 'value',
                allowBlank: false,
                forceSelection: true,
                mode: 'local',
                triggerAction: 'all',
                value: '',
                typeAhead: false,
                width: 100
            };

            var cboLinhaIntercambio = {
                id: 'linhaIntercambio',
                allowBlank: false,
                xtype: 'combo',
                name: 'linhaIntercambio',
                fieldLabel: 'Linha Pat. Manobra',
                store: dsLinhasIntercambio,
                displayField: 'Codigo',
                valueField: 'Id',
                forceSelection: true,
                loadingText: 'Buscando...',
                triggerAction: 'all',
                typeAhead: false,
                validationEvent: false,
                width: 150
            };

            var txtDataIntercambio = {
                xtype: 'xdatetime',
                id: 'dataIntercambio',
                name: 'dataIntercambio',
                allowBlank: false,
                fieldLabel: 'Dt. Desp. Int.',
                timeFormat: 'H:i:s',
                timeConfig: { altFormats: 'H:i:s', allowBlank: false },
                dateFormat: 'd/m/Y',
                dateConfig: { altFormats: 'd/m/Y', allowBlank: false }
            };

            var l2_coluna1 = { width: 85, layout: 'form', border: false, items: [txtSerieIntercambio] };
            var l2_coluna2 = { width: 85, layout: 'form', border: false, items: [txtDespachoIntercambio] };
            var l2_coluna3 = { width: 165, layout: 'form', border: false, items: [cboFerroviaIntercambio] };
            var l2_coluna4 = { width: 115, layout: 'form', border: false, items: [cboRegimeIntercambio] };
            var l2_coluna5 = { width: 165, layout: 'form', border: false, items: [cboLinhaIntercambio] };
            var l2_coluna6 = { width: 210, layout: 'form', border: false, items: [txtDataIntercambio] };

            var linha2 = {
                // Linha I
                layout: 'column',
                border: false,
                items: [l2_coluna1, l2_coluna2, l2_coluna3, l2_coluna4, l2_coluna5, l2_coluna6]
            };

            arrDetalhesFormulario.push(linha2);
            /***
            FIM DA LINHA INTERCAMBIO
            ****/
        }

        /****
        INICIO GRID VAGOES
        *****/

        function onEditNotasFiscaisVagao(idVagao, codigoVagao, indCarregadoMesmoFluxo, IndCarregadoFluxoNaoBrado, fluxoBrado, tipoServicoCte ) {
        
            if ((fluxoBrado && IndCarregadoFluxoNaoBrado ) || (!fluxoBrado && indCarregadoMesmoFluxo)) {
                if(tipoServicoCte != 1 && tipoServicoCte != 3 && tipoServicoCte != 4){
                    Ext.Msg.alert('Aviso', 'O vagão esta com destino diferente da Origem do Fluxo. Para carregar você deve desvincular pedido.');
                    return;
                }
            }

            if (windowNotasFiscais != null) {
                return;
            }

            /*if (indFluxoOrigemIntercambio){
				var mostrouMsg = false;
				dsNotasVagoes.each(
					function (record) {
						if (idVagao != record.data.IdVagao){
							Ext.Msg.show({
								title: "Erro",
								msg: "Não é possível despachar mais de um vagão simultaneamente em Fluxos de Origem Intercâmbio.",
								buttons: Ext.Msg.OK,
								icon: Ext.MessageBox.ERROR
							});
							mostrouMsg = true;
							return false;
						}
					}
				);

				if (mostrouMsg){
					return;
				}
			}*/

            dataInicioVagao = new Date();
            codigoVagaoEdit = codigoVagao;
            arrListaNotasVagao = new Array();

            windowNotasFiscais = new Ext.Window({
                id: 'windowNotasFiscais',
                title: 'Notas Fiscais',
                modal: true,
                width: 644,
                height: 390,
                autoLoad: {
                    url: '<%= Url.Action("GridNotaFiscal") %>',
                    scripts: true,
                    params: { idVagao: idVagao, codigoVagao: codigoVagao }
                },
                listeners: {
                    'close': function(panel) {
                        // alert(1);
                        var strDataInicioVagao = dataInicioVagao.format('d/m/Y H:i:s');
                        var dataTerminoVagao = new Date();
                        var strDataTerminoVagao = dataTerminoVagao.format('d/m/Y H:i:s');
                        arrListaVagoes.push(LogarTempoVagao(strDataInicioVagao, strDataTerminoVagao, codigoVagaoEdit, arrListaNotasVagao));
                        codigoVagaoEdit = "";
                        arrListaNotasVagao = new Array();
                        windowNotasFiscais = null;
                    }
                }
            });

            windowNotasFiscais.show();
        }

        function filtrarVagoes() {
            var codigoVagaoFiltro = Ext.getCmp("filtroCodigoVagao").getValue();

            if (indFluxoOrigemIntercambio) {
                if (codigoVagaoFiltro.length >= 4) {
                    dsVagoesPatio.load({ params: { 'codigoVagao': codigoVagaoFiltro } });
                }
            } else {
                dsVagoesPatio.clearFilter();
                Ext.getCmp("cboFiltroSerie").clearValue();
                Ext.getCmp("cboFiltroLinha").clearValue();
                // alert(1);
                dsVagoesPatio.load({ params: { 'codigoVagao': codigoVagaoFiltro } });
            }
        }

        function LocalizarVagao(codigoVagao) {
            Ext.Ajax.request({
                url: "<% = Url.Action("ObterDadosVagao") %>",
                timeout: 150000,
                success: function(response) {
                    var data = Ext.decode(response.responseText);
                    if (!data.Erro) {
                        var sitVag = data.DadosVagao.Situacao;
                        var locVag = data.DadosVagao.LocalVagao;
                        var msg = "O " + codigoVagao + " não pode ser carregado.<br />Localização: " + locVag + "<br /> Situação: " + sitVag;
                        Ext.Msg.alert('Informação', msg);
                    } else {
                        Ext.Msg.alert('Ops...', data.Mensagem);
                    }
                },
                failure: function(conn, data) {
                    alert("Ocorreu um erro inesperado.");
                },
                method: "POST",
                params: { codigoVagao: codigoVagao }
            });
        }

        function filtrarStoreVagao(filtrarCombos) {
            var codigoVagaoFiltro = Ext.getCmp("filtroCodigoVagao").getValue();
            var serieVagaoFiltro = Ext.getCmp("cboFiltroSerie").getValue();
            var linhaVagaoFiltro = Ext.getCmp("cboFiltroLinha").getValue();

            if (filtrarCombos) {
                dsVagoesPatio.filterBy(function(record) {
                    var ok = true;
                    if (serieVagaoFiltro != "") {
                        if (record.data.Serie != serieVagaoFiltro) {
                            ok = false;
                        }
                    }

                    if (linhaVagaoFiltro != "") {
                        if (record.data.Linha != linhaVagaoFiltro) {
                            ok = false;
                        }
                    }

                    return ok;
                });
            }

            // VERIFICAR ONDE ESTÁ O VAGAO
            if (dsVagoesPatio.getCount() == 0 && codigoVagaoFiltro.length == 7) {
                LocalizarVagao(codigoVagaoFiltro);
                // O [CODIGOVAGAO] está em [Trem/Patio] na situação [], por isto não é permitido carregar.    
            }

            if (dsVagoesPatio.getCount() == 0 && codigoVagaoFiltro.length != 7) {
                var msg = "O veículo não foi localizado.";
                Ext.Msg.alert('Informação', msg);
            }
        }

        var dsVagoesPatio = new Ext.data.JsonStore({
            url: '<%= Url.Action("ObterVagoesPatio") %>',
            root: "Items",
            fields: [
                'IdVagao',
                'Serie',
                'CodigoVagao',
                'Linha',
                'Sequencia',
                'IndCarregadoMesmoFluxo',
                'IndCarregadoFluxoNaoBrado',
                'FluxoBrado',
                'TipoServicoCte'
            ],
            listeners: {
                'load': function(store, records, options) {
                    /*if (!indFluxoOrigemIntercambio){
                        Ext.getCmp("filtroCodigoVagao").setValue('');
                    }*/

                    if (Ext.getCmp("filtroCodigoVagao").getValue().length == 7 && records.length == 1) {
                        var record = records[0];
                        // onEditNotasFiscaisVagao(record.data.IdVagao, record.data.CodigoVagao, record.data.IndCarregadoMesmoFluxo);
                        onEditNotasFiscaisVagao(record.data.IdVagao, record.data.CodigoVagao, record.data.IndCarregadoMesmoFluxo, record.data.IndCarregadoFluxoNaoBrado, record.data.FluxoBrado,record.data.TipoServicoCte);
                    }

                    for (var i = 0; i < records.length; i++) {
                        var record = records[i];
                        if (dsFiltroSerie.find('Codigo', record.data.Serie) == -1) {
                            var rec = new dsFiltroSerie.recordType({ Codigo: record.data.Serie });
                            dsFiltroSerie.add(rec);
                        }

                        if (dsFiltroLinha.find('Codigo', record.data.Linha) == -1) {
                            var rec = new dsFiltroLinha.recordType({ Codigo: record.data.Linha });
                            dsFiltroLinha.add(rec);
                        }
                    }

                    filtrarStoreVagao(false);
                }
            }
        });
        dsVagoesPatio.proxy.on('beforeload', function(p, params) {
            params['idEstacaoMae'] = idOrigem;
            params['indOrigemIntercambio'] = indFluxoOrigemIntercambio;
            params['idFluxo'] = "<%= idFluxoComercial %>";
        });

        var dsFiltroSerie = new Ext.data.ArrayStore({ fields: ['Codigo'] });
        var dsFiltroLinha = new Ext.data.ArrayStore({ fields: ['Codigo'] });

        var detalheVagaoAction = new Ext.ux.grid.RowActions({
            dataIndex: '',
            header: '',
            align: 'center',
            actions: [{
                iconCls: 'icon-detail',
                tooltip: 'Despachar'
            }],
            callbacks: {
                'icon-detail': function(grid, record, action, row, col) {
                    onEditNotasFiscaisVagao(record.data.IdVagao, record.data.CodigoVagao, record.data.IndCarregadoMesmoFluxo, record.data.IndCarregadoFluxoNaoBrado, record.data.FluxoBrado,record.data.TipoServicoCte);
                }
            }
        });

        var txtFiltroVagao = {
            xtype: 'textfield',
            id: 'filtroCodigoVagao',
            name: 'filtroCodigoVagao',
            emptyText: 'Vagão',
            enableKeyEvents: true,
            listeners: {
                specialKey: function(field, e) {
                    if (e.getKey() == e.ENTER) {
                        filtrarVagoes();
                    }
                },
                'change': function(field, e) {
                    filtrarVagoes();
                },
                'keypress': function(field, e) {

                    // Valida se o campo é numerico
                    var strChar = String.fromCharCode(e.keyCode);
                    if (isNaN(strChar)) {
                        e.stopEvent();
                    }
                }
            }
        };

        var cboFiltroSerie = {
            id: 'cboFiltroSerie',
            xtype: 'combo',
            name: 'cboFiltroSerie',
            fieldLabel: 'Série',
            store: dsFiltroSerie,
            displayField: 'Codigo',
            valueField: 'Codigo',
            forceSelection: true,
            mode: 'local',
            triggerAction: 'all',
            value: '',
            typeAhead: false,
            emptyText: 'Série',
            editable: false,
            hidden: indFluxoOrigemIntercambio,
            width: 100,
            listeners: {
                'change': function(field, e) {
                    filtrarStoreVagao(true);
                },
                'select': function(field, e) {
                    filtrarStoreVagao(true);
                }
            }
        };

        var cboFiltroLinha = {
            id: 'cboFiltroLinha',
            xtype: 'combo',
            name: 'cboFiltroLinha',
            fieldLabel: 'Linha',
            store: dsFiltroLinha,
            displayField: 'Codigo',
            valueField: 'Codigo',
            forceSelection: true,
            mode: 'local',
            triggerAction: 'all',
            value: '',
            typeAhead: false,
            editable: false,
            emptyText: 'Linha',
            hidden: indFluxoOrigemIntercambio,
            width: 100,
            listeners: {
                'change': function(field, e) {
                    filtrarStoreVagao(true);
                },
                'select': function(field, e) {
                    filtrarStoreVagao(true);
                }
            }
        };

        var btnAtualizarGrid = {
            text: 'Pesquisar',
            iconCls: 'icon-arrow_refresh',
            handler: function() {
                /*if (!indFluxoOrigemIntercambio){
                    dsVagoesPatio.load();
                }*/
                filtrarVagoes();
            }
        };

        var gridVagoes = new Ext.grid.GridPanel({
            viewConfig: {
                forceFit: true
            },
            title: 'Vagões',
            height: 200,
            stripeRows: true,
            region: 'center',
            store: dsVagoesPatio,
            loadMask: { msg: App.Resources.Web.Carregando },
            plugins: [detalheVagaoAction],
            viewConfig: {
                forceFit: true,
                getRowClass: function(record, index) {

                    var FluxoBrado = record.get('FluxoBrado');
                    var IndCarregadoFluxoNaoBrado = record.get('IndCarregadoFluxoNaoBrado');
                    var IndCarregadoMesmoFluxo = record.get('IndCarregadoMesmoFluxo');

                    if ((FluxoBrado && IndCarregadoFluxoNaoBrado) || (!FluxoBrado && IndCarregadoMesmoFluxo)) {
                        return 'vagao-invalido';
                    }
                }
            },
            colModel: new Ext.grid.ColumnModel({
                defaults: {
                    sortable: false
                },
                columns: [
                    new Ext.grid.RowNumberer(),
                    detalheVagaoAction,
                    { header: 'Série', dataIndex: "Serie", sortable: false },
                    { header: 'Vagão', dataIndex: "CodigoVagao", sortable: false },
                    { header: 'Linha', dataIndex: "Linha", sortable: false },
                    { header: 'Sequência', dataIndex: "Sequencia", sortable: false }
                ]
            }),
            tbar: [
                txtFiltroVagao,
                cboFiltroSerie,
                cboFiltroLinha,
                btnAtualizarGrid
            ]
        });

        // arrDetalhesFormulario.push(gridVagoes);
        /****
        FIM GRID VAGOES
        *****/

        /****
        INICIO GRID VAGOES NOTAS
        *****/
        var camposNotasVagoes = Ext.data.Record.create(
            [
                { name: 'IdVagao', mapping: 'IdVagao' },
                { name: 'CodigoVagao', mapping: 'CodigoVagao' },
                { name: 'ChaveNfe', mapping: 'ChaveNfe' },
                { name: 'SerieNotaFiscal', mapping: 'SerieNotaFiscal' },
                { name: 'NumeroNotaFiscal', mapping: 'NumeroNotaFiscal' },
                { name: 'PesoTotal', mapping: 'PesoTotal' },
                { name: 'PesoRateio', mapping: 'PesoRateio' },
                { name: 'ValorNotaFiscal', mapping: 'ValorNotaFiscal' },
                { name: 'ValorTotalNotaFiscal', mapping: 'ValorTotalNotaFiscal' },
                { name: 'Remetente', mapping: 'Remetente' },
                { name: 'Destinatario', mapping: 'Destinatario' },
                { name: 'CnpjRemetente', mapping: 'CnpjRemetente' },
                { name: 'CnpjDestinatario', mapping: 'CnpjDestinatario' },
                { name: 'Mercadoria', mapping: 'Mercadoria' },
                { name: 'SiglaRemetente', mapping: 'SiglaRemetente' },
                { name: 'SiglaDestinatario', mapping: 'SiglaDestinatario' },
                { name: 'DataNotaFiscal', mapping: 'DataNotaFiscal' },
                { name: 'TIF', mapping: 'TIF' },
                { name: 'PlacaCavalo', mapping: 'PlacaCavalo' },
                { name: 'PlacaCarreta', mapping: 'PlacaCarreta' },
                { name: 'UfRemetente', mapping: 'UfRemetente' },
                { name: 'UfDestinatario', mapping: 'UfDestinatario' },
                { name: 'InscricaoEstadualRemetente', mapping: 'InscricaoEstadualRemetente' },
                { name: 'InscricaoEstadualDestinatario', mapping: 'InscricaoEstadualDestinatario' },
                { name: 'TaraVagao', mapping: 'TaraVagao' },
                { name: 'VolumeVagao', mapping: 'VolumeVagao' },
                { name: 'VolumeNotaFiscal', mapping: 'VolumeNotaFiscal' },
                { name: 'IndObtidoAutomatico', mapping: 'IndObtidoAutomatico' },
                { name: 'MultiploDespacho', mapping: 'MultiploDespacho' },
                { name: 'PesoTotalVagao', mapping: 'PesoTotalVagao' },
                { name: 'PesoDcl', mapping: 'PesoDcl' },
                { name: 'IndContingencia', mapping: 'IndContingencia' },
                { name: 'ChaveCte', mapping: 'ChaveCte' }
            ]);

        // Reader Grid Detac
        var readerNotasVagoes = new Ext.data.JsonReader({
            root: 'Items',
            id: 'ReaderGridNotasVagoes'
        }, camposNotasVagoes);

        //Store Grid Detac
        var dsNotasVagoes = new Ext.ux.MultiGroupingStore({
            idIndex: 0,
            reader: readerNotasVagoes,
            storeId: 'dsNotasVagoes',
            groupField: ['CodigoVagao']
        });

        var detalheVagaoDespachoAction = new Ext.ux.grid.RowActions({
            dataIndex: '',
            header: '',
            align: 'center',
            actions: [{
                    iconCls: 'icon-detail',
                    tooltip: 'Editar'
                },
                {
                    iconCls: 'icon-del',
                    tooltip: 'Apagar'
                }],
            callbacks: {
                'icon-detail': function(grid, record, action, row, col) {
                    onEditNotasFiscaisVagao(record.data.IdVagao, record.data.CodigoVagao);
                },
                'icon-del': function(grid, recordDel, action, row, col) {

                    if (Ext.Msg.confirm("Excluir", "Ao apagar esta nota, todas deste vagão também serão apagadas. Deseja realmente excluir esta nota?", function(btn, text) {
                        if (btn == 'yes') {
                            RemoverNotasVagao(recordDel.data.IdVagao);
                        }
                    })) ;

                }
            }
        });

        var colunasGridNotasVagoes = new Array();
        colunasGridNotasVagoes.push(new Ext.grid.RowNumberer());
        colunasGridNotasVagoes.push(detalheVagaoDespachoAction);
        colunasGridNotasVagoes.push({ header: 'Vagão', width: 50, dataIndex: "CodigoVagao", sortable: false, menuDisabled: true, hidden: true });
        colunasGridNotasVagoes.push({ header: 'Chave', width: 250, dataIndex: "ChaveNfe", sortable: false });
        colunasGridNotasVagoes.push({ header: 'Série', width: 50, dataIndex: "SerieNotaFiscal", sortable: false });
        colunasGridNotasVagoes.push({ header: 'Nro.', width: 50, dataIndex: "NumeroNotaFiscal", sortable: false });

        if (indPreenchimentoVolume) {
            colunasGridNotasVagoes.push({ header: 'Volume', dataIndex: "VolumeNotaFiscal", sortable: false, renderer: VolumeRenderer });
        } else {
            colunasGridNotasVagoes.push({ header: 'Peso Rateio', dataIndex: "PesoRateio", sortable: false, renderer: PesoRenderer });
            colunasGridNotasVagoes.push({ header: 'Peso Total', dataIndex: "PesoTotal", sortable: false, renderer: PesoRenderer });
        }

        var gridNotasVagoes = new Ext.grid.GridPanel({
            viewConfig: {
                forceFit: true
            },
            title: 'Vagões à despachar',
            height: 200,
            region: 'center',
            stripeRows: true,
            store: dsNotasVagoes,
            loadMask: { msg: App.Resources.Web.Carregando },
            plugins: [detalheVagaoDespachoAction],
            view: new Ext.grid.GroupingView({
                forceFit: true,
                groupTextTpl: '{text} {[values.rs.length]} {[values.rs.length > 1 ? "Notas" : "Nota"]}'
            }),
            colModel: new Ext.grid.ColumnModel({
                defaults: {
                    sortable: false
                },
                columns: colunasGridNotasVagoes
            })
        });
        // arrDetalhesFormulario.push(gridNotasVagoes);
        /****
        FIM GRID VAGOES NOTAS
        *****/

        var formDetalhes = new Ext.FormPanel({
            id: 'formDetalhes',
            title: "Filtros",
            autoScroll: true,
            region: 'center',
            bodyStyle: 'padding: 15px',
            items: [arrDetalhesFormulario, gridVagoes, gridNotasVagoes],
            buttonAlign: 'center',
            labelAlign: 'top',
            buttons: [{
                    text: 'Despachar',
                    type: 'submit',
                    iconCls: 'icon-save',
                    handler: function(b, e) {

                        if (dsNotasVagoes.getCount() == 0) {
                            Ext.Msg.show({
                                title: 'Atenção',
                                msg: 'Não há vagões a serem despachados.',
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.INFO
                            });

                            return;
                        }

                        Ext.Msg.show({
                            title: 'Atenção',
                            msg: 'Você realmente deseja despachar estes vagões?',
                            buttons: Ext.Msg.YESNO,
                            fn: SalvarCarregamento,
                            icon: Ext.MessageBox.QUESTION
                        });
                    }
                },
                {
                    text: 'Sair',
                    type: 'button',
                    handler: function(b, e) {
                        VoltarTela();
                    }
                }
            ]
        });

        function ValidarCarregamento() {
            if (indFluxoOrigemIntercambio || !indFluxoCte) {
                if (!Ext.getCmp("formDetalhes").getForm().isValid()) {
                    return false;
                }
            }

            if (dsNotasVagoes.getCount() == 0) {
                Ext.Msg.alert("Erro", "Não há vagões a serem despachados.");
                return false;
            }

            return true;
        }

        function SalvarCarregamento(btn) {
            if (btn == 'no') {
                return;
            }

            var _serieIntercambio = null;
            var _numeroIntercambio = null;
            var _idFerroviaIntercambio = null;
            var _regimeIntercambio = null;
            var _idLinhaIntercambio = null;
            var _dataIntercambio = null;

            if (!ValidarCarregamento()) {
                return;
            }

            if (indFluxoOrigemIntercambio) {
                _serieIntercambio = Ext.getCmp("serieIntercambio").getValue().toUpperCase();
                ;
                _numeroIntercambio = Ext.getCmp("despachoIntercambio").getValue();
                _idFerroviaIntercambio = Ext.getCmp("ferroviaIntercambio").getValue();
                _regimeIntercambio = Ext.getCmp("regimeIntercambio").getValue();
                _idLinhaIntercambio = Ext.getCmp("linhaIntercambio").getValue();
                _dataIntercambio = Ext.getCmp("dataIntercambio").getValue();
                _dataIntercambio = _dataIntercambio.format('d/m/Y H:i:s');
            }

            var listaNotas = new Array();
            dsNotasVagoes.each(
                function(record) {
                    var objNota = record.data;
                    objNota.DataNotaFiscalString = objNota.DataNotaFiscal.format('d/m/Y');
                    listaNotas.push(objNota);
                }
            );

            var _dclPorVagao = true;
            if (!indFluxoCte) {
                _dclPorVagao = !Ext.getCmp("despachoEmGrupo").getValue();
            }

            var strDataInicioDespacho = dataInicioDespacho.format('d/m/Y H:i:s');
            var dataTerminoDespacho = new Date();
            var strDataTerminoDespacho = dataTerminoDespacho.format('d/m/Y H:i:s');
            var logDespacho = LogarTempoDespacho(strDataInicioDespacho, strDataTerminoDespacho, '<%= codigoFluxo %>', idOrigem, idDestino, <%= idMercadoria %>, arrListaVagoes);

            var carregamento = {
                CodigoTransacao: "BOI",
                IdFluxoComercial: <%= idFluxoComercial %>,
                IdAreaOperacionalSede: idOrigem,
                IdAreaOperacionalDestino: idDestino,
                IdMercadoria: <%= idMercadoria %>,
                SerieIntercambio: _serieIntercambio,
                NumeroIntercambio: _numeroIntercambio,
                IdFerroviaIntercambio: _idFerroviaIntercambio,
                RegimeIntercambio: _regimeIntercambio,
                IdLinhaIntercambio: _idLinhaIntercambio,
                DataIntercambioString: _dataIntercambio,
                DataCarregamentoString: Ext.getCmp("dataCarregamento").getValue().format('d/m/Y H:i:s'),
                DclPorVagao: _dclPorVagao,
                ListaNotasFiscais: listaNotas,
                LogDespacho: logDespacho
            };

            var carregamentoDto = $.toJSON(carregamento);

            $.ajax({
                url: "<%= Url.Action("SalvarCarregamento") %>",
                type: "POST",
                dataType: 'json',
                data: carregamentoDto,
                timeout: 1500000,
                contentType: "application/json; charset=utf-8",
                beforeSend: function() { Ext.getBody().mask("Carregamento em andamento", "x-mask-loading"); },
                success: function(result) {
                    Ext.getBody().unmask();

                    if (!result.Erro) {
                        Ext.Msg.show({
                            title: 'Mensagem',
                            msg: 'Vagões despachados com sucesso.',
                            buttons: Ext.Msg.OK,
                            fn: function() {
                                document.location.href = '<%= Url.Action("Index") %>';
                            }
                        });
                    } else {
                        // Ext.Msg.alert('Aviso', result.Mensagem); 
                        // var mensagemRetorno = result.Mensagens;
                        if (result.IdsVagaoRemover != null) {
                            for (var i = 0; i < result.IdsVagaoRemover.length; i++) {
                                RemoverNotasVagao(result.IdsVagaoRemover[i]);
                            }
                        }

                        var mensagemRetorno = "";
                        for (var i = 0; i < result.Mensagens.length; i++) {
                            var objErro = result.Mensagens[i];
                            if (objErro.IndErro) {
                                mensagemRetorno += "Vagão: " + objErro.CodigoVagao + "\n - " + objErro.MensagemErro + "\n\n------------------------------------\n\n";
                            } else {
                                RemoverNotasVagao(objErro.IdVagao);
                            }
                        }

                        MostrarWindowErros(mensagemRetorno);
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alert('Não foi possível realizar o faturamento: ' + textStatus + ': ' + jqXHR.responseText + '- ' + errorThrown);
                }
            });
        }

        $(function() {
            // Ext.getCmp("formDetalhes").render("divFiltros", 1);
            if (indFluxoOrigemIntercambio) {
                dsFerrovias.load();
                dsLinhasIntercambio.load();
            }

            /*if (!indFluxoOrigemIntercambio){
                dsVagoesPatio.load();
            }*/

            new Ext.Viewport({
                layout: 'border',
                items: [
                    {
                        region: 'north',
                        height: 40,
                        items: [{
                            region: 'center',
                            applyTo: 'header-content'
                        }]
                    },
                    {
                        region: 'center',
                        autoScroll: true,
                        items: [formDetalhes]
                    }
                ]
            });

            if (indFluxoOrigemIntercambio) {
                Ext.getCmp("serieIntercambio").focus();
            } else {
                Ext.getCmp("filtroCodigoVagao").focus();
            }
        });

        function MostrarWindowErros(mensagensErro) {
            var textarea = new Ext.form.TextArea({
                xtype: 'textarea',
                fieldLabel: 'Foram encontrados os seguintes erros',
                value: mensagensErro,
                readOnly: true,
                height: 220,
                width: 395
            });

            var formErros = new Ext.FormPanel({
                id: 'formErros',
                bodyStyle: 'padding: 15px',
                labelAlign: 'top',
                items: [textarea],
                buttonAlign: 'center',
                buttons: [{
                    text: 'Fechar',
                    type: 'button',
                    handler: function(b, e) {
                        windowErros.close();
                    }
                }]
            });

            windowErros = new Ext.Window({
                id: 'windowErros',
                title: 'Erros',
                modal: true,
                width: 444,
                height: 350,
                items: [formErros]
            });
            windowErros.show();
        }

    </script>
</asp:Content>
<asp:Content ID="MainContent" ContentPlaceHolderID="Content" runat="server">
    <div id="header-content">
        <h1>
            Carregamento</h1>
        <small>Você está em Operação > Despacho > Detalhe do Carregamento</small>
    </div>
    <div id="divFiltros">
    </div>
</asp:Content>
