﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div id="div-form-nota-fiscal">
</div>
<script type="text/javascript">
    function ValidarFormularioNota() {
        var erroForm = false;
        erroForm = !Ext.getCmp("formNotaFiscal").getForm().isValid();

        if (!recordEdit.data.IndObtidoAutomatico) {
            if (!ValidarCampoMaiorZero("edValorNota")) erroForm = true;
        }

        if (indPreenchimentoVolume) {
            /// A FUNÇÃO ValidarCampoMaiorZero ESTÁ DEFINIDA NO ARQUIVO Detalhe.aspx
            if (!ValidarCampoMaiorZero("edVolumeNotaFiscal")) erroForm = true;

            if (parseFloat(Ext.getCmp("edVolumeNotaFiscal").getValue()) <= 0) {
                Ext.getCmp("edVolumeNotaFiscal").markInvalid("O volume deve ser maior que zero.");
                erroForm = true;
            }

            if (parseFloat(Ext.getCmp("edVolumeNotaFiscal").getValue().replace(',', '.')) > 3000) {
                Ext.getCmp("edVolumeNotaFiscal").markInvalid("O volume deve ser menor ou igual a 3000.");
            }
        } else {
            /// A FUNÇÃO ValidarCampoMaiorZero ESTÁ DEFINIDA NO ARQUIVO Detalhe.aspx
            if (!ValidarCampoMaiorZero("edPesoTotal")) erroForm = true;
            if (!ValidarCampoMaiorZero("edPesoRateio")) erroForm = true;

            if (Ext.getCmp("edPesoRateio").isValid() && Ext.getCmp("edPesoTotal").isValid() && parseFloat(Ext.getCmp("edPesoTotal").getValue()) < parseFloat(Ext.getCmp("edPesoRateio").getValue())) {
                Ext.getCmp("edPesoRateio").markInvalid("O peso rateio deve ser menor que o peso total.");
                erroForm = true;
            }

            if (parseFloat(Ext.getCmp("edPesoTotal").getValue().replace(',', '.')) > 3000) {
                Ext.getCmp("edPesoTotal").markInvalid("O peso total deve ser menor ou igual a 3000.");
            }
        }

        if (libNotaMaiorQue180Dias == "N" && Ext.getCmp("edDataNota").isValid() && Ext.getCmp("edDataNota").getValue() < dataLimiteNf) {
            Ext.getCmp("edDataNota").markInvalid("A Data Nota Fiscal não pode ultrapassar de " + mesesRetroativos + " meses.");
            erroForm = true;
        }

        var dataNota = Ext.getCmp("edDataNota").getValue();
        var dataAtual = new Date();
        // dataAtual = new Date(dataAtual.getFullYear(),  dataAtual.getMonth(), dataAtual.getDate());

        if (dataNota > dataAtual) {
            Ext.getCmp("edDataNota").markInvalid("A Data Nota Fiscal não pode ultrapassar a data atual.");
            erroForm = true;
        }

        var valorConteiner = Ext.getCmp("edConteiner").getValue();
        if (Ext.getCmp("edConteiner").isValid() && valorConteiner != "") {
            if (!VerificarConteiner(valorConteiner)) {
                Ext.getCmp("edConteiner").markInvalid("O conteiner não existe.");
                erroForm = true;
            }
        }
        var UFDestinatario = Ext.getCmp("edUfDestinatario").getValue();
        var NomeDestinatario = Ext.getCmp("edDestinatario").getValue();
        var ufRemetente = Ext.getCmp("edUfRemetente").getValue();

        if (UFDestinatario != "EX" && NomeDestinatario.trim() == "") {
            Ext.getCmp("edCnpjDestinatario").markInvalid("Não foi encontrada nenhuma empresa para este CNPJ de destino.");
            erroForm = true;
        }

        if (indValidarCnpjRemetente) {
            if (ufRemetenteFiscal == "EX" && tipoNotaFiscal == "0") {
                if (Ext.getCmp("edCnpjRemetente").getValue() != cnpjDestinatarioFiscal) {
                    Ext.getCmp("edCnpjRemetente").markInvalid("CNPJ do Remetente da NFe(" + Ext.getCmp("edCnpjRemetente").getValue() + ") não é o mesmo do Destinatário Fiscal do Fluxo(" + cnpjDestinatarioFiscal + ").");
                }
            } else {
                if (cnpjRemetenteFiscal != Ext.getCmp("edCnpjRemetente").getValue()) {
                    Ext.getCmp("edCnpjRemetente").markInvalid("CNPJ do Remetente da NFe(" + Ext.getCmp("edCnpjRemetente").getValue() + ") não é o mesmo do Remetente Fiscal do Fluxo(" + cnpjRemetenteFiscal + ").");
                    erroForm = true;
                }
            }
        }
      
        if (indValidarCnpjDestinatario) {
            if (ufRemetenteFiscal == "EX" && tipoNotaFiscal == "0") {
                if (Ext.getCmp("edCnpjDestinatario").getValue() != cnpjRemetenteFiscal) {
                    Ext.getCmp("edCnpjDestinatario").markInvalid("CNPJ do Destinatário da NFe(" + Ext.getCmp("edCnpjDestinatario").getValue() + ") não é o mesmo do Remetente Fiscal do Fluxo(" + cnpjDestinatarioFiscal + ").");
                }
            } else {
                if (cnpjDestinatarioFiscal != Ext.getCmp("edCnpjDestinatario").getValue()) {
                    Ext.getCmp("edCnpjDestinatario").markInvalid("CNPJ do Destinatário da NFe(" + Ext.getCmp("edCnpjDestinatario").getValue() + ") não é o mesmo do Destinatário Fiscal do Fluxo(" + cnpjDestinatarioFiscal + ").");
                    erroForm = true;
                }
            }
        } else {
            if (ufDestinatarioFiscal == "EX" && UFDestinatario != "EX") {
                Ext.getCmp("edCnpjDestinatario").markInvalid("Destinatário da Nota fiscal deve ser Exterior.");
                erroForm = true;
            }
        }

        if (gridNotasFiscais.getStore().getCount() > 1) {

            gridNotasFiscais.getStore().each(
                function(recordTmp) {

                    if (recordTmp.data.ChaveNfe == ''
                        && recordTmp.data.SerieNotaFiscal == ''
                        && recordTmp.data.NumeroNotaFiscal == ''
                        && (recordTmp.data.PesoTotal == null || recordTmp.data.PesoTotal == '')
                        && recordTmp.data.ValorNotaFiscal == 0) {
                        return true;
                    }

                    if (recordEdit.data.ChaveNfe != recordTmp.data.ChaveNfe || recordTmp.data.ChaveNfe == "") {
                        if (recordTmp.data.CnpjRemetente != Ext.getCmp("edCnpjRemetente").getValue()) {
                            Ext.getCmp("edCnpjRemetente").markInvalid("CNPJ do Remetente da NFe(" + Ext.getCmp("edCnpjRemetente").getValue() + ") não é o mesmo do Remetente Fiscal da nota anterior(" + recordTmp.data.CnpjRemetente + ").");
                            erroForm = true;
                        }

                        if (recordTmp.data.CnpjDestinatario != Ext.getCmp("edCnpjDestinatario").getValue()) {
                            Ext.getCmp("edCnpjDestinatario").markInvalid("CNPJ do Destinatário da NFe(" + Ext.getCmp("edCnpjDestinatario").getValue() + ") não é o mesmo do Destinatário Fiscal da nota anterior(" + recordTmp.data.CnpjDestinatario + ").");
                            erroForm = true;
                        }
                    }
                }
            );
        }

        if (!ValidarCampoMaiorZero("edValorTotalNota")) {
            Ext.getCmp("edValorTotalNota").markInvalid("O valor total da nota deve ser maior que zero.");
            erroForm = true;
        }

        if ((indFluxoRateioCte) || (tipoServicoCte !== 0)) {

                var chaveCte = Ext.getCmp("edCteChaveCte").getValue();
//                var chaveNfe = Ext.getCmp("edNfChaveNfe").getValue();

                if (chaveCte.length != 44) {
                    Ext.getCmp("edCteChaveCte").markInvalid("O campo chave cte deve possuir 44 caracteres.");
                    erroForm = true;
                }

		        if (isNaN(chaveCte))
		        {
                    Ext.getCmp("edCteChaveCte").markInvalid("O campo chave cte possui caracteres não numéricos.");
                    erroForm = true;
		        }

//                if (chaveCte == chaveNfe)
//		        {
//                    Ext.getCmp("edCteChaveCte").markInvalid("O campo chave cte não pode ser igual a chave da nota fiscal.");
//                    erroForm = true;
//		        }
        }

        if (erroForm) {
            var mensagemErro = "Foram encontrados os erros abaixo:<br />";
            Ext.getCmp("formNotaFiscal").getForm().items.each(function(f) {
                if (f.activeError != undefined) {
                    mensagemErro += " - " + f.fieldLabel + ": " + f.activeError + "<br />";
                }
            });

            Ext.Msg.alert('Erro de validação', mensagemErro);
            return false;
        }

        return true;
    }

    function CarregarDadosCnpjEdicao(val, idAreaOperacional, sufixo) {
        if (val == "" || val == undefined || val == null) {
            return false;
        }

        Ext.Ajax.request({
            url: "<% = Url.Action("ObterDadosCnpj") %>",
            success: function(response) {
                var data = Ext.decode(response.responseText);

                if (!data.Erro) {
                    if (data.Exterior && sufixo == 'Destinatario') {
                        Ext.getCmp("edUf" + sufixo).setValue("EX");
                    } else {
                        Ext.getCmp("edInscricaoEstadual" + sufixo).setValue(data.Dados.InscricaoEstadual);
                        Ext.getCmp("ed" + sufixo).setValue(data.Dados.Nome);
                        Ext.getCmp("edSigla" + sufixo).setValue(data.Dados.Sigla);
                        Ext.getCmp("edUf" + sufixo).setValue(data.Dados.Uf);
                    }
                } else {
                    Ext.Msg.alert('Ops...', data.Mensagem);
                    Ext.getCmp("edInscricaoEstadual" + sufixo).setValue("");
                    Ext.getCmp("ed" + sufixo).setValue("");
                    Ext.getCmp("edSigla" + sufixo).setValue("");
                    Ext.getCmp("edUf" + sufixo).setValue("");
                    Ext.getCmp("edCnpj" + sufixo).markInvalid(data.Mensagem);
                }
            },
            failure: function(conn, data) {
                Ext.getCmp("edInscricaoEstadual" + sufixo).setValue("");
                Ext.getCmp("ed" + sufixo).setValue("");
                Ext.getCmp("edSigla" + sufixo).setValue("");
                Ext.getCmp("edUf" + sufixo).setValue("");
                alert("Ocorreu um erro inesperado");
            },
            method: "POST",
            params: { cnpj: val, codigoFluxo: Ext.getCmp("codigoFluxoComercial").getValue(), idAreaOperacional: idAreaOperacional }
        });
    }

    /**********************************************************
	FORM DADOS DAS NOTAS FISCAIS - INICIO
    ChaveNfe
    Conteiner
    SerieNotaFiscal
    NumeroNotaFiscal
    PesoTotal
    PesoRateio
    ValorNotaFiscal
    Remetente
    Destinatario
    CnpjRemetente
    CnpjDestinatario
    TIF
    DataNotaFiscal
    SiglaRemetente
    SiglaDestinatario
	**********************************************************/
    var arrLinhas = new Array();

    var edNfChaveNfe = {
        xtype: 'textfield',
        id: 'edNfChaveNfe',
        fieldLabel: 'ChaveNfe',
        name: 'edNfChaveNfe',
        width: 375
    };

    var l1_coluna1 = { width: 385, layout: 'form', border: false, items: [edNfChaveNfe] };
    
    var linha1 = 
    {
            layout: 'column',
            border: false,
            items: [l1_coluna1]
    };
    arrLinhas.push(linha1);

    if ((indFluxoRateioCte) || (tipoServicoCte !== 0))  {

        var edCteChaveCte = {
            xtype: 'textfield',
            id: 'edCteChaveCte',
            fieldLabel: 'ChaveCte',
            name: 'edCteChaveCte',
            width: 375,
            maxLength: 44,
            allowBlank: false,
            enableKeyEvents: true,
            listeners:{
				'keypress':  function (field, e) {
					
					// Valida se o campo é numerico
					var strChar = String.fromCharCode(e.keyCode);
					if(isNaN(strChar))
					{
						e.stopEvent();
					}

                    var chaveCte = Ext.getCmp("edCteChaveCte").getValue();
				    if (chaveCte.length > 43) {
				    	e.stopEvent();					
                    }
				}
            }

        };

        var l2_coluna1 = { width: 385, layout: 'form', border: false, items: [edCteChaveCte] };

        var linha2 = {
            layout: 'column',
            border: false,
            items: [l2_coluna1]
        };
        arrLinhas.push(linha2);
    }

    var edSerieNota = {
        xtype: 'textfield',
        id: 'edSerieNota',
        fieldLabel: 'Série',
        name: 'edSerieNota',
        allowBlank: false,
        width: 40
    };

    var edNumeroNota = {
        xtype: 'textfield',
        id: 'edNumeroNota',
        fieldLabel: 'Número',
        name: 'edNumeroNota',
        allowBlank: false,
        width: 60
    };

    var edDataNota = {
        xtype: 'datefield',
        id: 'edDataNota',
        fieldLabel: 'Data',
        name: 'edDataNota',
        allowBlank: false,
        width: 90
    };


    if (indPreenchimentoVolume) {
        var edVolumeNotaFiscal = {
            xtype: 'masktextfield',
            id: 'edVolumeNotaFiscal',
            fieldLabel: 'Volume',
            name: 'edVolumeNotaFiscal',
            allowBlank: false,
            width: 55,
            mask: '9990,000',
            money: true,
            maxLength: 8
        };

        var l3_coluna1 = { width: 50, layout: 'form', border: false, items: [edSerieNota] };
        var l3_coluna2 = { width: 70, layout: 'form', border: false, items: [edNumeroNota] };
        var l3_coluna3 = { width: 100, layout: 'form', border: false, items: [edDataNota] };
        var l3_coluna4 = { width: 80, layout: 'form', border: false, items: [edVolumeNotaFiscal] };

        var linha3 = {
            layout: 'column',
            border: false,
            items: [l3_coluna1, l3_coluna2, l3_coluna3, l3_coluna4]
        };
    } else {
        var edPesoTotal = {
            xtype: 'masktextfield',
            id: 'edPesoTotal',
            fieldLabel: 'Peso Total',
            name: 'edPesoTotal',
            width: 70,
            mask: '9990,000',
            money: true,
            maxLength: 8,
            value: "0,000"
        };

        var edPesoRateio = {
            xtype: 'masktextfield',
            id: 'edPesoRateio',
            fieldLabel: 'Peso Rateio',
            allowBlank: false,
            name: 'edPesoRateio',
            width: 70,
            mask: '990,000',
            money: true,
            maxLength: 7,
            value: "0,000"
        };

        var l3_coluna1 = { width: 50, layout: 'form', border: false, items: [edSerieNota] };
        var l3_coluna2 = { width: 70, layout: 'form', border: false, items: [edNumeroNota] };
        var l3_coluna3 = { width: 100, layout: 'form', border: false, items: [edDataNota] };
        var l3_coluna4 = { width: 80, layout: 'form', border: false, items: [edPesoTotal] };
        var l3_coluna5 = { width: 80, layout: 'form', border: false, items: [edPesoRateio] };

        var linha3 = {
            layout: 'column',
            border: false,
            items: [l3_coluna1, l3_coluna2, l3_coluna3, l3_coluna4, l3_coluna5]
        };
    }

    arrLinhas.push(linha3);

    var edValorTotalNota = {
        xtype: 'masktextfield',
        id: 'edValorTotalNota',
        fieldLabel: 'Valor Total',
        name: 'edValorTotalNota',
        allowBlank: false,
        width: 70,
        mask: '#9.999.990,00',
        money: true,
        maxLength: 13,
        value: "0,00",
        listeners: {
            'blur': function(field) {
                if (indPreenchimentoVolume) {
                    Ext.getCmp("edValorNota").setValue(field.getValue());
                }
            }
        }
    };

    var edValorNota = {
        xtype: 'masktextfield',
        id: 'edValorNota',
        fieldLabel: 'Valor Rateio',
        name: 'edValorNota',
        allowBlank: false,
        width: 70,
        mask: '#9.999.990,00',
        money: true,
        maxLength: 13,
        value: "0,00",
        disabled: indPreenchimentoVolume
    };

    var edConteiner = {
        xtype: 'textfield',
        id: 'edConteiner',
        fieldLabel: 'Conteiner',
        name: 'edConteiner',
        width: 70,
        allowBlank: !indFluxoConteiner,
        style: { textTransform: 'uppercase' },
        enableKeyEvents: true,
        listeners: {
            'blur': function(field) {
                VerificarConteiner(field.getValue());
            }
        }
    };

    var l4_coluna1 = { width: 80, layout: 'form', border: false, items: [edValorTotalNota] };
    var l4_coluna2 = { width: 80, layout: 'form', border: false, items: [edValorNota] };
    var l4_coluna3 = { width: 80, layout: 'form', border: false, items: [edConteiner] };

    var arrLinha4 = new Array();
    arrLinha4.push(l4_coluna1);
    arrLinha4.push(l4_coluna2);
    arrLinha4.push(l4_coluna3);
    
    var linha4 = {
        layout: 'column',
        border: false,
        items: arrLinha4
    };

    arrLinhas.push(linha4);

    var arrLinha5 = new Array();

    if (indFluxoInternacional) {
        var edTif = {
            xtype: 'textfield',
            id: 'edTif',
            fieldLabel: 'TIF',
            name: 'edTif',
            allowBlank: false,
            width: 70
        };
        var l5_coluna1 = { width: 80, layout: 'form', border: false, items: [edTif] };
        arrLinha5.push(l5_coluna1);
    }

    if (indPreenchimentoTara) {
        var edPlacaCavalo = {
            xtype: 'textfield',
            id: 'edPlacaCavalo',
            fieldLabel: 'Placa Cavalo',
            name: 'edPlacaCavalo',
            allowBlank: !indTaraObrigatorio,
            width: 70,
            plugins: [new Ext.ux.InputTextMask('LLL-9999', true)]
        };
        var l5_coluna2 = { width: 80, layout: 'form', border: false, items: [edPlacaCavalo] };
        arrLinha5.push(l5_coluna2);

        var edPlacaCarreta = {
            xtype: 'textfield',
            id: 'edPlacaCarreta',
            fieldLabel: 'Placa Carreta',
            name: 'edPlacaCarreta',
            allowBlank: !indTaraObrigatorio,
            width: 70,
            plugins: [new Ext.ux.InputTextMask('LLL-9999', true)]
        };
        var l5_coluna3 = { width: 80, layout: 'form', border: false, items: [edPlacaCarreta] };
        arrLinha5.push(l5_coluna3);
    }

    var linha5 = {
        layout: 'column',
        border: false,
        items: arrLinha5
    };

    if (arrLinha5.length > 0) {
        arrLinhas.push(linha5);
    }

    var campoCnpjRem = {
        fieldLabel: 'CNPJ/CPF',
        xtype: 'textfield',
        name: 'edCnpjRemetente',
        id: 'edCnpjRemetente',
        width: 100,
        allowBlank: false,
        autoCreate: { tag: 'input', type: 'text', maxlength: '14', autocomplete: 'off' },
        listeners: {
            change: function(field, newValue, oldValue) {
                if (newValue != "") {
                    CarregarDadosCnpjEdicao(newValue, idOrigem, 'Remetente');
                }
            },
            disable: function(field) {
                Ext.getCmp("edBtnCnpjRemetente").hide();
            },
            enable: function(field) {
                Ext.getCmp("edBtnCnpjRemetente").show();
            }
        }
    };

    var l_campo_cnpjRem = { width: 120, layout: 'form', border: false, items: [campoCnpjRem] };

    var botaoCnpjRem = new Ext.Button({
        margins: { top: 50 },
        name: 'edBtnCnpjRemetente',
        hidden: true,
        iconCls: 'icon-find',
        id: 'edBtnCnpjRemetente',
        handler: function(a, b, c) {
            PesquisarEmpresa('Remetente');
        }
    });

    var l_campo_botaoRem = { width: 25, layout: 'form', margins: { top: 50 }, border: false, items: [botaoCnpjRem] };

    var linhaCnpjRem = { layout: 'column', border: false, items: [l_campo_cnpjRem, l_campo_botaoRem] };

    var fieldsetRemetente = {
        xtype: 'fieldset',
        title: 'Remetente',
        collapsible: false,
        width: 195,
        height: 285,
        defaults: { width: 170 },
        items: [linhaCnpjRem,
            {
                fieldLabel: 'Sigla',
                xtype: 'textfield',
                name: 'edSiglaRemetente',
                id: 'edSiglaRemetente',
                cls: 'x-item-disabled',
                readOnly: true
            }, {
                fieldLabel: 'Nome',
                xtype: 'textfield',
                name: 'edRemetente',
                id: 'edRemetente',
                cls: 'x-item-disabled',
                readOnly: true
            }, {
                fieldLabel: 'UF',
                name: 'edUfRemetente',
                xtype: 'textfield',
                id: 'edUfRemetente',
                cls: 'x-item-disabled',
                readOnly: true
            }, {
                fieldLabel: 'Inscrição Estadual',
                xtype: 'textfield',
                name: 'edInscricaoEstadualRemetente',
                id: 'edInscricaoEstadualRemetente',
                cls: 'x-item-disabled',
                readOnly: true
            }]
    };
    var l6_coluna1 = { width: 200, layout: 'form', border: false, items: [fieldsetRemetente] };


    function PesquisarEmpresa(sufixo) {
        windowPesquisaEmpresa = new Ext.Window({
            id: 'windowPesquisaEmpresa',
            title: 'Pesquisar Empresa',
            modal: true,
            width: 598,
            closeAction: 'hide',
            height: 350,
            items: [formPesquisaEmpresa],
            listeners: {
                'hide': function() {
                    var newValue = Ext.getCmp("cpHiddenCnpj").getValue();
                    Ext.getCmp("edCnpj" + sufixo).setValue(newValue);
                    Ext.getCmp("formPesquisaEmpresa").getForm().reset();
                    gridEmpresaStore.removeAll();
                    CarregarDadosCnpjEdicao(newValue, idOrigem, sufixo);
                }
            }
        });

        windowPesquisaEmpresa.show();
    }

    var campoCnpj = {
        xtype: 'textfield',
        fieldLabel: 'CNPJ/CPF',
        name: 'edCnpjDestinatario',
        id: 'edCnpjDestinatario',
        width: 115,
        allowBlank: false,
        autoCreate: { tag: 'input', type: 'text', maxlength: '14', autocomplete: 'off' },
        listeners: {
            change: function(field, newValue, oldValue) {
                if (newValue != "") {
                    CarregarDadosCnpjEdicao(newValue, idDestino, 'Destinatario');
                }
            },
            disable: function(field) {
                Ext.getCmp("edBtnCnpjDestinatario").hide();
                Ext.getCmp("edinfcnpjDestinatario").hide();
            },
            enable: function(field) {
                Ext.getCmp("edBtnCnpjDestinatario").show();
                Ext.getCmp("edinfcnpjDestinatario").show();
            }
        }
    };

    var l_campo_cnpj = { width: 120, layout: 'form', border: false, items: [campoCnpj] };

    var botaoCnpj = new Ext.Button({
        margins: { top: 50 },
        name: 'edBtnCnpjDestinatario',
        hidden: true,
        iconCls: 'icon-find',
        id: 'edBtnCnpjDestinatario',
        handler: function(a, b, c) {
            PesquisarEmpresa('Destinatario');
        }
    });

    var l_campo_botao = { width: 25, layout: 'form', style: { "margin-top": "17px" }, border: false, items: [botaoCnpj] };

    var linhaCnpj = { layout: 'column', border: false, items: [l_campo_cnpj, l_campo_botao] };


    var fieldsetDestinatario = {
        xtype: 'fieldset',
        title: 'Destinatário',
        collapsible: false,
        width: 200,
        height: 285,
        defaults: { width: 170 },
        items: [linhaCnpj, {
                text: 'Empresa do exterior informar zeros.',
                name: 'edinfcnpjDestinatario',
                id: 'edinfcnpjDestinatario',
                labelStyle: 'font: italic 8px ',
                style: 'font-style:italic',
                hidden: true,
                height: 3,
                xtype: 'label'
            }, {
                fieldLabel: 'Sigla',
                name: 'edSiglaDestinatario',
                id: 'edSiglaDestinatario',
                cls: 'x-item-disabled',
                xtype: 'textfield',
                readOnly: true
            }, {
                fieldLabel: 'Nome',
                name: 'edDestinatario',
                id: 'edDestinatario',
                cls: 'x-item-disabled',
                xtype: 'textfield',
                readOnly: true
            }, {
                fieldLabel: 'UF',
                name: 'edUfDestinatario',
                id: 'edUfDestinatario',
                cls: 'x-item-disabled',
                xtype: 'textfield',
                readOnly: true
            }, {
                fieldLabel: 'Inscrição Estadual',
                name: 'edInscricaoEstadualDestinatario',
                id: 'edInscricaoEstadualDestinatario',
                cls: 'x-item-disabled',
                xtype: 'textfield',
                readOnly: true
            }]
    };
    var l6_coluna2 = { width: 205, layout: 'form', border: false, items: [fieldsetDestinatario] };

    var linha6 = {
        layout: 'column',
        border: false,
        items: [l6_coluna1, l6_coluna2]
    };
    arrLinhas.push(linha6);

    function fctProcessarNfeManual() {
        var chave = recordEdit.data.ChaveNfe;
        var conteiner = recordEdit.data.Conteiner;
        var serie = recordEdit.data.SerieNotaFiscal;
        var numero = recordEdit.data.NumeroNotaFiscal;
        var valorNota = recordEdit.data.ValorNotaFiscal;
        var valorTotal = recordEdit.data.ValorTotalNotaFiscal;
        var remetente = recordEdit.data.Remetente;
        var destinatario = recordEdit.data.Destinatario;
        var cnpjRemetente = recordEdit.data.CnpjRemetente;
        var cnpjDestinatario = recordEdit.data.CnpjDestinatario;
        var dataNotaFiscal = recordEdit.data.DataNotaFiscal;
        var siglaRemetente = recordEdit.data.SiglaRemetente;
        var SiglaDestinatario = recordEdit.data.SiglaDestinatario;
        var UfRemetente = recordEdit.data.UfRemetente;
        var UfDestinatario = recordEdit.data.UfDestinatario;
        var InscricaoEstadualRemetente = recordEdit.data.InscricaoEstadualRemetente;
        var InscricaoEstadualDestinatario = recordEdit.data.InscricaoEstadualDestinatario;
        var VolumeNotaFiscal = recordEdit.data.VolumeNotaFiscal;
        var PesoTotal = recordEdit.data.PesoTotal;
        var PesoRateio = recordEdit.data.PesoRateio;
        var TIF = recordEdit.data.TIF;
        var PlacaCavalo = recordEdit.data.PlacaCavalo;
        var PlacaCarreta = recordEdit.data.PlacaCarreta;

        if (indPreenchimentoVolume) {
            recordEdit.data.VolumeNotaFiscal = Ext.getCmp("edVolumeNotaFiscal").getValue();
        } else {
            recordEdit.data.PesoTotal = Ext.getCmp("edPesoTotal").getValue();
            recordEdit.data.PesoRateio = Ext.getCmp("edPesoRateio").getValue();
        }

        if (indFluxoInternacional) {
            recordEdit.data.TIF = Ext.getCmp("edTif").getValue();
        }

        if (indPreenchimentoTara) {
            recordEdit.data.PlacaCavalo = Ext.getCmp("edPlacaCavalo").getValue();
            recordEdit.data.PlacaCarreta = Ext.getCmp("edPlacaCarreta").getValue();
        }

        Ext.Ajax.request({
            url: "<% = Url.Action("ProcessarNfeManual") %>",
            success: function(response) {
                var data = Ext.decode(response.responseText);

                if (data.Erro) {
                    Ext.Msg.alert('Ops...', data.Mensagem);
                }
            },
            failure: function(conn, data) {
                Ext.Msg.alert("Ocorreu um erro inesperado");
            },
            method: "POST",
            params: {
                chave: chave,
                conteiner: conteiner,
                serie: serie,
                numero: numero,
                valorNota: valorNota,
                valorTotal: valorTotal,
                remetente: remetente,
                destinatario: destinatario,
                cnpjRemetente: cnpjRemetente,
                cnpjDestinatario: cnpjDestinatario,
                dataNotaFiscal: dataNotaFiscal,
                siglaRemetente: siglaRemetente,
                siglaDestinatario: SiglaDestinatario,
                ufRemetente: UfRemetente,
                ufDestinatario: UfDestinatario,
                inscricaoEstadualRemetente: InscricaoEstadualRemetente,
                inscricaoEstadualDestinatario: InscricaoEstadualDestinatario,
                volumeNotaFiscal: VolumeNotaFiscal,
                pesoTotal: PesoTotal,
                pesoRateio: PesoRateio,
                tif: TIF,
                placaCavalo: PlacaCavalo,
                placaCarreta: PlacaCarreta
            }
        });

    }

    var formNotaFiscal = new Ext.form.FormPanel({
        id: 'formNotaFiscal',
        labelWidth: 80,
        width: 440,
        height: 525,
        autoScroll: true,
        bodyStyle: 'padding: 15px',
        labelAlign: 'top',
        items:
        [
            arrLinhas
        ],
        buttonAlign: "center",
        buttons:
        [{
                text: "Salvar",
                handler: function() {

                    if (ValidarFormularioNota()) {
                        recordEdit.data.ChaveNfe = Ext.getCmp("edNfChaveNfe").getValue();
                        recordEdit.data.Conteiner = Ext.getCmp("edConteiner").getValue().toUpperCase();
                        recordEdit.data.SerieNotaFiscal = Ext.getCmp("edSerieNota").getValue();
                        recordEdit.data.NumeroNotaFiscal = Ext.getCmp("edNumeroNota").getValue();
                        recordEdit.data.ValorNotaFiscal = Ext.getCmp("edValorNota").getValue();
                        recordEdit.data.ValorTotalNotaFiscal = Ext.getCmp("edValorTotalNota").getValue();
                        recordEdit.data.Remetente = Ext.getCmp("edRemetente").getValue();
                        recordEdit.data.Destinatario = Ext.getCmp("edDestinatario").getValue();
                        recordEdit.data.CnpjRemetente = Ext.getCmp("edCnpjRemetente").getValue();
                        recordEdit.data.CnpjDestinatario = Ext.getCmp("edCnpjDestinatario").getValue();
                        recordEdit.data.DataNotaFiscal = Ext.getCmp("edDataNota").getValue();
                        recordEdit.data.SiglaRemetente = Ext.getCmp("edSiglaRemetente").getValue();
                        recordEdit.data.SiglaDestinatario = Ext.getCmp("edSiglaDestinatario").getValue();
                        recordEdit.data.UfRemetente = Ext.getCmp("edUfRemetente").getValue();
                        recordEdit.data.UfDestinatario = Ext.getCmp("edUfDestinatario").getValue();
                        recordEdit.data.InscricaoEstadualRemetente = Ext.getCmp("edInscricaoEstadualRemetente").getValue();
                        recordEdit.data.InscricaoEstadualDestinatario = Ext.getCmp("edInscricaoEstadualDestinatario").getValue();

                        if (indPreenchimentoVolume) {
                            recordEdit.data.VolumeNotaFiscal = Ext.getCmp("edVolumeNotaFiscal").getValue();
                        } else {
                            recordEdit.data.PesoTotal = Ext.getCmp("edPesoTotal").getValue();
                            recordEdit.data.PesoRateio = Ext.getCmp("edPesoRateio").getValue();
                        }

                        if (indFluxoInternacional) {
                            recordEdit.data.TIF = Ext.getCmp("edTif").getValue();
                        }

                        if (indPreenchimentoTara) {
                            recordEdit.data.PlacaCavalo = Ext.getCmp("edPlacaCavalo").getValue();
                            recordEdit.data.PlacaCarreta = Ext.getCmp("edPlacaCarreta").getValue();
                        }

                        if (recordEdit.data.IndContingencia == true) {
                            fctProcessarNfeManual();
                        }

                        if ((indFluxoRateioCte) || (tipoServicoCte !== 0)) {
                            recordEdit.data.ChaveCte = Ext.getCmp("edCteChaveCte").getValue();
                            recordEdit.data.CteInseridoTelaNf = "S";
                        }

                       if ((indFluxoRateioCte) || (tipoServicoCte !== 0)) {
                            AtualizaChavesCteGrid(Ext.getCmp("edCteChaveCte").getValue());
                        }

                        recordEdit.commit();
                        recordEdit = null;

                        Ext.getCmp("formNotaFiscal").getForm().reset();
                        gridNotasFiscais.getEl().unmask();
                        windowNotaFiscal.hide();


                    }
                }
            },
            {
                text: "Cancelar",
                handler: function() {
                    windowNotaFiscal.hide();
                }
            }]
    });

    /*// VERIFICA SE É EDIÇÃO DA NOTA FISCAL
    if (recordEdit != null) {
        // ATUALIZA OS CAMPOS 
        AtualizarCampoNotaFiscal("edNfChaveNfe", recordEdit.data.ChaveNfe);
        AtualizarCampoNotaFiscal("edConteiner", recordEdit.data.Conteiner);
        AtualizarCampoNotaFiscal("edSerieNota", recordEdit.data.SerieNotaFiscal);
        AtualizarCampoNotaFiscal("edNumeroNota", recordEdit.data.NumeroNotaFiscal);
        
        if (indPreenchimentoVolume){
            AtualizarCampoNotaFiscal("edVolumeNotaFiscal", recordEdit.data.VolumeNotaFiscal);
        }else{
            AtualizarCampoNotaFiscal("edPesoTotal", recordEdit.data.PesoTotal);
            AtualizarCampoNotaFiscal("edPesoRateio", recordEdit.data.PesoRateio);
        }
        
        AtualizarCampoNotaFiscal("edValorTotalNota", recordEdit.data.ValorTotalNotaFiscal);
        AtualizarCampoNotaFiscal("edValorNota", recordEdit.data.ValorNotaFiscal);
        AtualizarCampoNotaFiscal("edRemetente", recordEdit.data.Remetente);
        AtualizarCampoNotaFiscal("edDestinatario", recordEdit.data.Destinatario);
        AtualizarCampoNotaFiscal("edCnpjRemetente", recordEdit.data.CnpjRemetente);
        AtualizarCampoNotaFiscal("edCnpjDestinatario", recordEdit.data.CnpjDestinatario);
        if (recordEdit.data.DataNotaFiscal != null && recordEdit.data.DataNotaFiscal != "")
        {
            AtualizarCampoNotaFiscal("edDataNota", recordEdit.data.DataNotaFiscal.format('d/m/Y'));
        }
        AtualizarCampoNotaFiscal("edSiglaRemetente", recordEdit.data.SiglaRemetente);
        AtualizarCampoNotaFiscal("edSiglaDestinatario", recordEdit.data.SiglaDestinatario);
        AtualizarCampoNotaFiscal("edUfRemetente", recordEdit.data.UfRemetente);
        AtualizarCampoNotaFiscal("edUfDestinatario", recordEdit.data.UfDestinatario);
        AtualizarCampoNotaFiscal("edInscricaoEstadualRemetente", recordEdit.data.InscricaoEstadualRemetente);
        AtualizarCampoNotaFiscal("edInscricaoEstadualDestinatario", recordEdit.data.InscricaoEstadualDestinatario);

        if (indFluxoInternacional) {
            AtualizarCampoNotaFiscal("edTif", recordEdit.data.TIF);
        }

        if (indPreenchimentoTara) {
            AtualizarCampoNotaFiscal("edPlacaCavalo", recordEdit.data.PlacaCavalo);
            AtualizarCampoNotaFiscal("edPlacaCarreta", recordEdit.data.PlacaCarreta);
            // if (!indTaraObrigatorio) {
                // Ext.getCmp("edPlacaCavalo")
            // }
        }

        Ext.getCmp("edNfChaveNfe").disable();
        if (recordEdit.data.IndObtidoAutomatico) 
        {
            Ext.getCmp("edSerieNota").disable();
            Ext.getCmp("edNumeroNota").disable();
            
            if (indPreenchimentoVolume){
                Ext.getCmp("edVolumeNotaFiscal").disable();
            }else{
                Ext.getCmp("edPesoTotal").disable();
            }
            
            Ext.getCmp("edValorNota").disable();
            Ext.getCmp("edCnpjRemetente").disable();
            Ext.getCmp("edCnpjDestinatario").disable();
            Ext.getCmp("edDataNota").disable();
            Ext.getCmp("edValorTotalNota").disable();
        }else{
            
            Ext.getCmp("edCnpjRemetente").setValue(cnpjRemetenteFiscal);
            Ext.getCmp("edInscricaoEstadualRemetente").setValue(ieRemetenteFiscal);
            Ext.getCmp("edRemetente").setValue(nomeRemetenteFiscal);
            Ext.getCmp("edSiglaRemetente").setValue(siglaRemetenteFiscal);
            Ext.getCmp("edUfRemetente").setValue(ufRemetenteFiscal);

            Ext.getCmp("edCnpjDestinatario").setValue(cnpjDestinatarioFiscal);
            Ext.getCmp("edInscricaoEstadualDestinatario").setValue(ieDestinatarioFiscal);
            Ext.getCmp("edDestinatario").setValue(nomeDestinatarioFiscal);
            Ext.getCmp("edSiglaDestinatario").setValue(siglaDestinatarioFiscal);
            Ext.getCmp("edUfDestinatario").setValue(ufDestinatarioFiscal);
            
            if (indValidarCnpjRemetente)
            {
                Ext.getCmp("edCnpjRemetente").disable();                
            }

            if (indValidarCnpjDestinatario)
            {
                Ext.getCmp("edCnpjDestinatario").disable();
            }
        }
    }*/

    function AtualizaChavesCteGrid(chaveCte) 
    {
        /*suspend events to block firing the events on setting record values
        then resume and refresh the view
        */
        ////gridNotasFiscais.view.store.suspendEvents();
        for (var i = 0; i < gridNotasFiscais.getStore().getCount(); i++)
        {
            var record = gridNotasFiscais.getStore().getAt(i);
            record.set('ChaveCte', chaveCte);
            /////this.fireEvent('checkchange', this, i, check, record);
        }
    }
    
    function AtualizarCampoNotaFiscal(campo, valor, mask) {
        if (valor != null && valor != "") {
            if (mask != null && mask != "") {
                valor = Ext.util.Format.number(valor, mask);
            }

            Ext.getCmp(campo).setValue(valor);
        }
    }

/**********************************************************
	FORM DADOS DAS NOTAS FISCAIS - FIM
	********************************************************** /	
	formNotaFiscal.render("div-form-nota-fiscal");

	if (indMostrarMascara){
		Ext.getCmp("formNotaFiscal").getEl().mask("Obtendo dados da Nota Fiscal Eletronicamente", "x-mask-loading");
	}
	*/
</script>
