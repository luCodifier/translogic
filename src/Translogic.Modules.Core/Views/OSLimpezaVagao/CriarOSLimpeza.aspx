﻿<%@ Page Title="" Language="C#" Inherits="System.Web.Mvc.ViewPage" MasterPageFile="~/Views/Shared/Interna.master" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        //***************************** VARIÁVEIS E INICIALIZAÇÃO**************//
        var idOs = '<%=ViewData["idOs"] %>';
        var dataHora  = '<%=ViewData["data"] %>';
        var local = '<%=ViewData["local"] %>';

        $(function () {

            //Setar campos recuperados da OS
            Ext.getCmp("txtNumOs").setValue(idOs);
            Ext.getCmp("txtDataHora").setValue(dataHora);
            Ext.getCmp("txtLocal").setValue(local);
        });

        //***************************** CAMPOS *****************************//
        var txtNumOs = {
            xtype: 'textfield',
            name: 'txtNumOs',
            id: 'txtNumOs',
            fieldLabel: 'Número OS',
            disabled: true,
            autoCreate: {
                tag: 'input',
                type: 'text',
                autocomplete: 'off',
                maxlength: '12',
                readonly: true
            },
            maskRe: /[0-9+;]/,
            width: 85
        };

        var txtDataHora = {
            xtype: 'textfield',
            name: 'txtDataHora',
            id: 'txtDataHora',
            fieldLabel: 'Data e hora',
            disabled : true,
            autoCreate: {
                tag: 'input',
                type: 'text',
                autocomplete: 'off',
                maxlength: '12',
                readonly: true
            },
            style: 'text-transform:uppercase;',
            width: 95
        };

        var txtLocal = {
            xtype: 'textfield',
            name: 'txtLocal',
            id: 'txtLocal',
            fieldLabel: 'Local',
            disabled: true,
            autoCreate: {
                tag: 'input',
                type: 'text',
                autocomplete: 'off',
                maxlength: '3',
                readonly: true
            },
            style: 'text-transform:uppercase;',
            width: 32
        };

        var fornecedorStore = new Ext.data.JsonStore({
            root: "Items",
            autoLoad: true,
            proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("ObterFornecedores", "OSLimpezaVagao") %>', timeout: 600000 }),
            id: 'fornecedorStore',
            fields: ['IdFornecedorOs', 'Nome'],
            baseParams: { local: local }
        });     
        
        var ddlFornecedor = new Ext.form.ComboBox({
            editable: false,
            typeAhead: true,
            forceSelection: true,
            disableKeyFilter: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            store: fornecedorStore,
            valueField: 'IdFornecedorOs',
            displayField: 'Nome',
            fieldLabel: 'Fornecedor',
            id: 'ddlFornecedor',
            width: 260,
            value: 'Selecione',
            alowBlank: false
        });

        var rbtTipoServico = {
            xtype: 'radiogroup',
            fieldLabel: 'Tipo do serviço',
            id: 'rbtTipoServico',
            width: 150,
            name: 'rbtTipoServico',
            style: 'margin-top:5px;',
            readonly: true,
            items: [
		            { boxLabel: 'Limpeza', name: 'tipoServico', inputValue: 1, disabled: true },
		            { boxLabel: 'Lavagem', name: 'tipoServico', inputValue: 2, disabled: true }
		           ]
        };


        //***************************** GRID VAGÕES**********************//

       var gridVagoes = new Translogic.PaginatedGrid({
            id: 'gridVagoes',
            name: 'gridVagoes',
            autoLoadGrid: true,
            viewConfig: {
                forceFit: true,
                emptyText: 'Não possui dado(s) para exibição.'
            },
            height: 300,
            autoScroll: true, 
            fields: [
                    'IdItem',
                    'NumeroVagao',
                    'ConcluidoDescricao',
                    'RetrabalhoDescricao',
                    'Observacao'                                               
                ],
            url: '<%= Url.Action("ObterVagoesDaOs", "OSLimpezaVagao") %>',
            columns: [
				    new Ext.grid.RowNumberer(),
                    { header: 'Nº Vagão', dataIndex: "NumeroVagao", sortable: false, width: 130 },
                    { header: 'Serviço concluído', dataIndex: "ConcluidoDescricao", sortable: false, width: 100 },
                    { header: 'Teve retrabalho', dataIndex: "RetrabalhoDescricao", sortable: false, width: 100 },
                    { header: 'Observação', dataIndex: "Observacao", sortable: false, width: 100 }
                ]
        });

        gridVagoes.getStore().proxy.on('beforeload', function (p, params) {
            params['idOs'] = idOs;
        });


        //***************************** LAYOUT *****************************//

        var fieldNumOS = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            width: 95,
            items: [txtNumOs]
        };

        var fieldDataHora = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            width: 105,
            items: [txtDataHora]
        };

        var fieldLocal = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            width: 42,
            items: [txtLocal]
        };

        var fieldFornecedor = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            width: 270,
            items: [ddlFornecedor]
        };

        var fieldTipo = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding-left:5px; padding-top:5px;',
            items: [rbtTipoServico]
        };

        //Containers
        var containerLinha1 = {
            layout: 'column',
            border: true,
            items: [fieldNumOS, fieldDataHora, fieldLocal, fieldFornecedor, fieldTipo]
        };

        var containerLinha2 = {
            layout: 'column',
            border: true,
            items: [gridVagoes]
        };


        //***************************** FUNÇÕES*****************************//

        //Classes
        function OSLimpezaVagaoItemDto() {
            var IdItem;
        }
        function Vagao() {
            var OSLimpezaVagaoItemDto;
        }
        
	    function SalvarOs() {

            var ddlFornecedor = Ext.getCmp("ddlFornecedor").getValue();
            if (ddlFornecedor == "Selecione" || ddlFornecedor == "0" || ddlFornecedor == "") {
                Ext.Msg.show({ title: 'Aviso', msg: 'O campo fornecedor é obrigatório!', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                return;
            }

            var LocalServico = Ext.getCmp("txtLocal").getValue();
            if (LocalServico == "") {
                Ext.Msg.show({ title: 'Aviso', msg: 'O campo local é obrigatório!', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                return;
            }
	        
	        Ext.getBody().mask("Processando dados...", "x-mask-loading");

            //Parâmetros para método SAVE
            var jsonRequest = $.toJSON({ idOs: idOs, idFornecedor: ddlFornecedor, local: LocalServico });
            //console.log(jsonRequest);

            $.ajax({
                url: '<%= Url.Action("SalvarOS", "OSLimpezaVagao") %>',
                type: "POST",
                dataType: 'json',
                data: jsonRequest,
                timeout: 300000,
                contentType: "application/json; charset=utf-8",
                failure: function (conn, data) {
                    Ext.Msg.alert("Mensagem de Erro", "Ocorreu um erro inesperado");
                    Ext.getBody().unmask();
                },
                success: function () {
                    Ext.getBody().unmask();
                    Ext.Msg.show({ title: 'Aviso', msg: 'OS criada com sucesso.', buttons: Ext.Msg.OK, icon: Ext.MessageBox.WARNING, fn: function (btn, text) {
                        if (btn == 'ok') {
                            var url = '<%= Url.Action("Imprimir", "OSLimpezaVagao") %>?idOS=' + idOs;
                            window.open(url, "");

                            var url = '<%= Url.Action("Index", "OSLimpezaVagao") %>';
                            window.location = url;
                        }
                    }
                    });               
                }

            });

        }

        function Voltar() {

            var url = '<%= Url.Action("PesquisarVagoes", "OSLimpezaVagao") %>?idOs=' + idOs + '&local=' + local;
            window.location = url;
        }
	    
	    //***************************** BOTOES *****************************//
	    var btnSalvarImprimirOS = {
            name: 'btnSalvarImprimirOS',
            id: 'btnSalvarImprimirOS',
            text: 'Salvar e Imprimir',
            iconCls: 'icon-printer',
            handler: SalvarOs
        };

        var btnVoltar = {
            name: 'btnVoltar',
            id: 'btnVoltar',
            text: 'Voltar',
            iconCls: 'icon-left',
            handler: Voltar
        };

        //***************************** PAINEL *****************************//
        var panelForm = new Ext.form.FormPanel({
            id: 'panelForm',
            layout: 'form',
            labelAlign: 'top',
            border: false,
            autoHeight: true,
            title: "Criação de OS",
            region: 'center',
            bodyStyle: 'padding: 15px',
            items: [containerLinha1, containerLinha2],
            buttonAlign: "center",
            buttons: [btnSalvarImprimirOS, btnVoltar]

        });

   	    //***************************** RENDER *****************************//
	    Ext.onReady(function () {
	        panelForm.render("divContent");

	        //Desabilitar tipo de serviço
	        $("#rbtTipoServico input[type=radio]").attr("disabled", "disabled");
	    });
        
    
    </script>
</asp:Content>
<asp:Content ID="MainContent" ContentPlaceHolderID="Content" runat="server">
    <div id="header-content">
        <h1>
            Manutenção</h1>
        <small>Você está em Manutenção > Gerar Ordem de Serviço</small>
        <br />
    </div>
    <div id="divContent">
    </div>
</asp:Content>
