﻿<%@ Page Title="" Language="C#" Inherits="System.Web.Mvc.ViewPage" MasterPageFile="~/Views/Shared/Interna.master" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server">

    <script type="text/javascript">
        //***************************** VARIÁVEIS E INICIALIZAÇÃO**************//
        var idOs = '<%=ViewData["idOs"] %>';
        var dataHora = '<%=ViewData["data"] %>';
        var local = '<%=ViewData["local"] %>';
        var idFornecedor = '<%=ViewData["idFornecedor"] %>';
        var idTipo = '<%=ViewData["idTipo"] %>';

        $(function () {

            //Setar campos recuperados da OS
            Ext.getCmp("txtNumOs").setValue(idOs);
            Ext.getCmp("txtDataHora").setValue(dataHora);
            Ext.getCmp("txtLocal").setValue(local);
        });

        //***************************** CAMPOS *****************************//
        var txtNumOs = {
            xtype: 'textfield',
            name: 'txtNumOs',
            id: 'txtNumOs',
            fieldLabel: 'Número OS',
            disabled: true,
            autoCreate: {
                tag: 'input',
                type: 'text',
                autocomplete: 'off',
                maxlength: '12',
                readonly: true
            },
            maskRe: /[0-9+;]/,
            width: 85
        };

        var txtDataHora = {
            xtype: 'textfield',
            name: 'txtDataHora',
            id: 'txtDataHora',
            fieldLabel: 'Data e hora',
            disabled : true,
            autoCreate: {
                tag: 'input',
                type: 'text',
                autocomplete: 'off',
                maxlength: '12',
                readonly: true
            },
            style: 'text-transform:uppercase;',
            width: 95
        };

        var txtLocal = {
            xtype: 'textfield',
            name: 'txtLocal',
            id: 'txtLocal',
            fieldLabel: 'Local',
            disabled: true,
            autoCreate: {
                tag: 'input',
                type: 'text',
                autocomplete: 'off',
                maxlength: '3',
                readonly: true
            },
            style: 'text-transform:uppercase;',
            width: 32
        };

        var fornecedorStore = new Ext.data.JsonStore({
            root: "Items",
            autoLoad: true,
            proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("ObterFornecedores", "OSLimpezaVagao") %>', timeout: 600000 }),
            id: 'fornecedorStore',
            fields: ['IdFornecedorOs', 'Nome'],
            baseParams: { local: local }
        });

        var ddlFornecedor = new Ext.form.ComboBox({
            editable: false,
            typeAhead: false,
            forceSelection: false,
            disableKeyFilter: false,
            triggerAction: 'all',
            lazyRender: false,
            mode: 'local',
            store: fornecedorStore,
            valueField: 'IdFornecedorOs',
            displayField: 'Nome',
            fieldLabel: 'Fornecedor',
            id: 'ddlFornecedor',
            width: 260             
        });

        //CUIDADO: Os valores 1 e 2 estão gravados numa tabela no BD.
        var rbtTipoServico = {
            xtype: 'radiogroup',
            fieldLabel: 'Tipo do serviço',
            id: 'rbtTipoServico',
            width: 150,
            name: 'rbtTipoServico',
            style: 'margin-top:5px;',
            items: [
		            { boxLabel: 'Lavagem', name: 'tipoServico', inputValue: 1, checked: (idTipo == 1) ? true : false },
		            { boxLabel: 'Limpeza', name: 'tipoServico', inputValue: 2, checked: (idTipo == 2) ? true : false }
		           ]
        };


        var ckMarcarTodos = {
            xtype: 'checkboxgroup',
            fieldLabel: 'Marcar todos',
            id: 'ckMarcarTodos',
            width: 260,
            name: 'ckMarcarTodos',
            style: 'margin-top:5px;',
            items: [
		            { boxLabel: 'Como concluído', name: 'ckAllConcluido', inputValue: 1 },
		            { boxLabel: 'Como retrabalho', name: 'ckAllRetrabalho', inputValue: 1 }
		           ],
            listeners: {
                change: {
                    fn: function (campo, checkeds) {


                        if (checkeds == null) {
                            NotSetAllCheckConcluido();
                            NotSetAllCheckRetrabalho();
                        }
                        else {

                            if (campo.items.items[0].hasFocus) {

                                if (campo.items.items[0].checked) {
                                    SetAllCheckConcluido();
                                }
                                else {
                                    NotSetAllCheckConcluido();
                                }
                            }

                            if (campo.items.items[1].hasFocus) {

                                if (campo.items.items[1].checked) {
                                    SetAllCheckRetrabalho();
                                }
                                else {
                                    NotSetAllCheckRetrabalho();
                                }
                            }
                        }
                    }
                }
            }
        };


        //***************************** GRID VAGÕES**********************//

        var gridVagoesStore = new Ext.data.JsonStore({
            root: "Items",
            totalProperty: 'Total',
            id: 'gridVagoesStore',
            name: 'gridVagoesStore',
            proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("ObterVagoesDaOs", "OSLimpezaVagao") %>', timeout: 600000 }),
            paramNames: {
                sort: "detalhesPaginacaoWeb.Sort",
                dir: "detalhesPaginacaoWeb.Dir",
                start: "detalhesPaginacaoWeb.Start",
                limit: "detalhesPaginacaoWeb.Limit"
            },
             fields: [
                    'IdItem',
                    'NumeroVagao',
                    'Concluido',
                    'Retrabalho',
                    'Observacao'
                ]
         });

         // Toolbar da Grid
         var pagingToolbar = new Ext.PagingToolbar({
             pageSize: 50,
             store: gridVagoesStore,
             displayInfo: true,
             displayMsg: App.Resources.Web.GridExibindoRegistros,
             emptyMsg: App.Resources.Web.GridSemRegistros,
             paramNames: {
                 start: "detalhesPaginacaoWeb.Start",
                 limit: "detalhesPaginacaoWeb.Limit"
             }
         });

         var checkConcluido = new Ext.grid.CheckColumn({
             dataIndex: 'Concluido',
             header: "Concluído",
             id: 'checkConcluido',
             sortable: false,
             width: 80
         });

         var checkRetrabalho = new Ext.grid.CheckColumn({
             dataIndex: 'Retrabalho',
             header: 'Retrabalho',
             id: 'checkRetrabalho',
             sortable: false,
             width: 80            
         });

         var rowEditing = {
             xtype: 'textfield',
             name: 'txtObservacao',
             id: 'txtObservacao',
             autoCreate: {
                 tag: 'input',
                 type: 'text',
                 autocomplete: 'off',
                 maxlength: '255'
             },
             width: 800
         };

         var gridColumns = new Ext.grid.ColumnModel({
             defaults: {
                 sortable: true
             },
             columns: [
                    new Ext.grid.RowNumberer(),
                    { header: 'Nº Vagão', dataIndex: "NumeroVagao", sortable: false, width: 100 },
                    checkConcluido,
                    checkRetrabalho,
                    { header: 'Observação', dataIndex: "Observacao", sortable: false, width: 830, editable: true, editor: rowEditing }
                ]
         });

         var gridVagoes = new Ext.grid.EditorGridPanel({
             id: 'gridVagoes',
             name: 'gridVagoes',
             autoLoadGrid: false,
             height: 200,
             stripeRows: true,
             cm: gridColumns,
             region: 'center',
             autoScroll: true,
             loadMask: { msg: App.Resources.Web.Carregando },
             store: gridVagoesStore,
             bbar: pagingToolbar,
             plugins: [checkConcluido, checkRetrabalho]
                      
         });

        gridVagoes.getStore().proxy.on('beforeload', function (p, params) {
            params['idOs'] = idOs;
        });

        // Group Box DADOS DE FECHAMENTO
        var txtDataEntregaOSFornecedor = new Ext.form.DateField({
            fieldLabel: 'Data da entrega da OS para o fornecedor',
            id: 'txtDataEntregaOSFornecedor',
            name: 'txtDataEntregaOSFornecedor',
            dateFormat: 'd/n/Y',
            width: 100,
            plugins: [new Ext.ux.InputTextMask('99/99/9999', true)]

        });

        var txtHoraMinutoEntregaOSFornecedor = new Ext.form.TimeField({
            name: 'txtHoraMinutoEntregaOSFornecedor',
            id: 'txtHoraMinutoEntregaOSFornecedor',
            format: 'H:i',
            increment: 1,
            fieldLabel: 'Hora : Minuto',
            maxlength: 5,
            plugins: [new Ext.ux.InputTextMask('99:99', true)],
            width: 65
        });

        var txtDataDevolucaoOSRumo = new Ext.form.DateField({
            fieldLabel: 'Data da devolução da OS para a Rumo',
            id: 'txtDataDevolucaoOSRumo',
            name: 'txtDataDevolucaoOSRumo',
            dateFormat: 'd/n/Y',
            width: 100,
            plugins: [new Ext.ux.InputTextMask('99/99/9999', true)]
        });

        var txtHoraMinutoAprovadorRumo = new Ext.form.TimeField({
            name: 'txtHoraMinutoAprovadorRumo',
            id: 'txtHoraMinutoAprovadorRumo',
            format: 'H:i',
            increment: 1,
            fieldLabel: 'Hora : Minuto',
            maxlength: 5,
            plugins: [new Ext.ux.InputTextMask('99:99', true)],
            width: 65
        });

        var txtNomeAprovadorRumo = {
            xtype: 'textfield',
            name: 'txtNomeAprovadorRumo',
            id: 'txtNomeAprovadorRumo',
            fieldLabel: 'Nome do Aprovador na Rumo',
            autoCreate: {
                tag: 'input',
                type: 'text',
                autocomplete: 'off',
                maxlength: '50'
            },
            width: 200
        };

        var txtMatriculaAprovadorRumo = {
            xtype: 'textfield',
            name: 'txtMatriculaAprovadorRumo',
            id: 'txtMatriculaAprovadorRumo',
            fieldLabel: 'Matrícula do Aprovador na Rumo',
            autoCreate: {
                tag: 'input',
                type: 'text',
                autocomplete: 'off',
                maxlength: '50'
            },
            width: 200
        };

        var txtNomeAprovadorFornecedor = {
            xtype: 'textfield',
            name: 'txtNomeAprovadorFornecedor',
            id: 'txtNomeAprovadorFornecedor',
            fieldLabel: 'Nome do Aprovador no Fornecedor',
            autoCreate: {
                tag: 'input',
                type: 'text',
                autocomplete: 'off',
                maxlength: '50'
            },
            width: 200
        };

        var txtMatriculaAprovadorFornecedor = {
            xtype: 'textfield',
            name: 'txtMatriculaAprovadorFornecedor',
            id: 'txtMatriculaAprovadorFornecedor',
            fieldLabel: 'Matrícula do Aprovador no Fornecedor',
            autoCreate: {
                tag: 'input',
                type: 'text',
                autocomplete: 'off',
                maxlength: '50'
            },
            width: 200
        };

        //***************************** LAYOUT *****************************//

        var fieldNumOS = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            width: 95,
            items: [txtNumOs]
        };

        var fieldDataHora = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            width: 105,
            items: [txtDataHora]
        };

        var fieldLocal = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            width: 42,
            items: [txtLocal]
        };

        var fieldFornecedor = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            width: 270,
            items: [ddlFornecedor]
        };

        var fieldTipo = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding-left:5px; padding-top:5px;',
            items: [rbtTipoServico]
        };

        var fieldCheckAll = {
            layout: 'form',
            border: false,
           // width: 260,
            bodyStyle: 'padding-left:5px; padding-top:5px;',
            items: [ckMarcarTodos]
        };

        var containerLinha1 = {
            layout: 'column',
            border: true,
            items: [fieldNumOS, fieldDataHora, fieldLocal, fieldFornecedor, fieldTipo, fieldCheckAll]
        };

        var containerLinha2 = {
            layout: 'column',
            border: true,
            items: [gridVagoes]
        };
      
        //Dados fechamento
        var fieldDataEntregaOSFornecedor = {
            layout: 'form',
            border: false,
            width: 260,
            bodyStyle: 'padding: 5px',
            items: [txtDataEntregaOSFornecedor]
        };

        var fieldHoraMinutoEntregaOSFornecedor = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            items: [txtHoraMinutoEntregaOSFornecedor]
        };

        var fieldDataDevolucaoOSRumo = {
            layout: 'form',
            border: false,
            width: 260,
            bodyStyle: 'padding: 5px',
            items: [txtDataDevolucaoOSRumo]
        };

        var fieldHoraMinutoAprovadorRumo = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            items: [txtHoraMinutoAprovadorRumo]
        };

        var fieldNomeAprovadorRumo = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            items: [txtNomeAprovadorRumo]
        };

        var fieldMatriculaAprovadorRumo = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            items: [txtMatriculaAprovadorRumo]
        };

        var fieldNomeAprovadorFornecedor = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            items: [txtNomeAprovadorFornecedor]
        };

        var fieldMatriculaAprovadorFornecedor = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            items: [txtMatriculaAprovadorFornecedor]
        };

        var containerLinha3 = {
            layout: 'column',
            border: true,
            items: [fieldDataEntregaOSFornecedor, fieldHoraMinutoEntregaOSFornecedor, fieldNomeAprovadorFornecedor, fieldMatriculaAprovadorFornecedor]
        };
        
        var containerLinha4 = {
            layout: 'column',
            border: true,
            items: [fieldDataDevolucaoOSRumo, fieldHoraMinutoAprovadorRumo, fieldNomeAprovadorRumo, fieldMatriculaAprovadorRumo]
        };
      

        //***************************** FUNCOES *****************************//
        function GetTextUpperField(id, fieldLabel, maxLength, width, allowBlank) {

            maxLength = maxLength || 50;
            width = width || 100;

            if (allowBlank == null)
                allowBlank = true;

            var text = {
                xtype: 'textfield',
                maxLength: maxLength,
                width: width,
                name: id,
                id: id,
                allowBlank: allowBlank,
                fieldLabel: fieldLabel,
                style: { textTransform: "uppercase" },
                listeners:
                {
                    change: Upper
                }
            };

            return text;
        }

        function Upper(field, newValue, oldValue) {
            field.setValue(newValue.toUpperCase());
        }

        String.prototype.trim = function () {
            return this.replace(/^\s+|\s+$/g, "");
        };
        
        function SetAllCheckConcluido() {

            $(".x-grid3-cc-checkConcluido").removeClass("x-grid3-check-col");
            $(".x-grid3-cc-checkConcluido").addClass("x-grid3-check-col-on");
            var itens = gridVagoes.getStore().getRange();
            for (var i = 0; i < itens.length; i++) {
                itens[i].data.Concluido = true;
            }
        }

        function NotSetAllCheckConcluido() {

            $(".x-grid3-cc-checkConcluido").removeClass("x-grid3-check-col-on");
            $(".x-grid3-cc-checkConcluido").addClass("x-grid3-check-col");
            var itens = gridVagoes.getStore().getRange();
            for (var i = 0; i < itens.length; i++) {
                itens[i].data.Concluido = false;
            }
        }

        function SetAllCheckRetrabalho() {

            $(".x-grid3-cc-checkRetrabalho").removeClass("x-grid3-check-col");
            $(".x-grid3-cc-checkRetrabalho").addClass("x-grid3-check-col-on");
            var itens = gridVagoes.getStore().getRange();
            for (var i = 0; i < itens.length; i++) {
                itens[i].data.Retrabalho = true;
            }
        }

        function NotSetAllCheckRetrabalho() {

            $(".x-grid3-cc-checkRetrabalho").removeClass("x-grid3-check-col-on");
            $(".x-grid3-cc-checkRetrabalho").addClass("x-grid3-check-col");
            var itens = gridVagoes.getStore().getRange();
            for (var i = 0; i < itens.length; i++) {
                itens[i].data.Retrabalho = false;
            }
        }

        //Classes
        function OSLimpezaVagaoItemDto() {
            var IdVagao;
            var Concluido;
            var Retrabalho;
            var Observacao;
        }
        function Vagao() {
            var OSLimpezaVagaoItemDto;
        }

        //Realiza validação da OS e se passar chama o salvar
        function ValidarEFecharOS() {

            //Recupera vagões do GRID
            var itens = gridVagoes.getStore().getRange();
            var temAlgumConcluido = false;
            var temAlgumNaoConcluido = false;
            var concluido = false;
            for (var i = 0; i < itens.length; i++) {
                concluido = itens[i].data.Concluido;
                if (concluido == false)
                    temAlgumNaoConcluido = true;
                if (concluido)
                    temAlgumConcluido = true;
            }

            if (temAlgumConcluido == false) {

                Ext.Msg.show({
                    title: 'Serviço concluído',
                    msg: 'Para confirmar o fechamento é necessário marcar pelo menos um vagão como concluído',
                    buttons: Ext.Msg.OK,
                    animEl: 'elId',
                    icon: Ext.MessageBox.WARNING
                });

            }
            else if (temAlgumNaoConcluido) {
                // Show a dialog using config options:
                Ext.Msg.show({
                    title: 'Fechamento parcial',
                    msg: 'Deseja fechar a OS parcialmente?',
                    buttons: Ext.Msg.OKCANCEL,
                    fn: SalvarOs,
                    animEl: 'elId',
                    icon: Ext.MessageBox.QUESTION
                });
            }
            else {
                SalvarOs('valido');
            }
        }

        function comparaDatas(dataIni, dataFim) {

            var arrDtIni = dataIni.split('/');
            var arrHoraIni = dataIni.split(' ');
            var arrDtFim = dataFim.split('/');
            var arrHoraFim = dataFim.split(' ');

            var dtIni = new Date(arrDtIni[2].replace(" " + arrHoraIni[1], "") + '/' + arrDtIni[1] + '/' + arrDtIni[0] + " " + arrHoraIni[1]);
            var dtFim = new Date(arrDtFim[2].replace(" " + arrHoraFim[1], "") + '/' + arrDtFim[1] + '/' + arrDtFim[0] + " " + arrHoraFim[1]);

            return dtIni < dtFim;
        }
        //Validar os campos obrigatórios e salvar a OS
        function SalvarOs(btn) {
            //se btn escolhido for o OK irá realizar a validação e fechamento
            if (btn == 'ok' || btn == 'valido') {

                var ddlFornecedorValue = Ext.getCmp("ddlFornecedor").getValue();
                if (ddlFornecedorValue == "Todos" || ddlFornecedorValue == "0" || ddlFornecedorValue == "") {
                    Ext.Msg.show({ title: 'Aviso', msg: 'O campo fornecedor é obrigatório!', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                    return;
                }

                var dataHora = Ext.getCmp("txtDataHora").getValue();

                var txtDataEntregaOSFornecedor = Ext.getCmp("txtDataEntregaOSFornecedor").getRawValue();
                if (txtDataEntregaOSFornecedor == "" ||  txtDataEntregaOSFornecedor == "__/__/____") {
                    Ext.Msg.show({ title: 'Aviso', msg: 'O campo Data e hora da entrega da OS para o fornecedor é obrigatório!', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                    return;
                }
                var txtHoraMinutoEntregaOSFornecedor = Ext.getCmp("txtHoraMinutoEntregaOSFornecedor").getRawValue();
                if (txtHoraMinutoEntregaOSFornecedor == "") {
                    Ext.Msg.show({ title: 'Aviso', msg: 'O campo hora(hh:mm) da entrega da OS para o fornecedor é obrigatório!', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                    return;
                }
                
                if (comparaDatas((txtDataEntregaOSFornecedor + " " + txtHoraMinutoEntregaOSFornecedor), dataHora)) {
                    Ext.Msg.show({ title: 'Aviso', msg: 'O campo Data e hora da entrega da OS para o fornecedor não pode ser inferior a data e hora da OS!', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                    return;
                }                

                //Rumo
                var txtDataDevolucaoOSRumo = Ext.getCmp("txtDataDevolucaoOSRumo").getRawValue();
                if (txtDataDevolucaoOSRumo == "" || txtDataDevolucaoOSRumo == "__/__/____") {
                    Ext.Msg.show({ title: 'Aviso', msg: 'O campo Data e hora da devolução da OS para a Rumo é obrigatório!', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                    return;
                }
                var txtHoraMinutoAprovadorRumo = Ext.getCmp("txtHoraMinutoAprovadorRumo").getRawValue();
                if (txtHoraMinutoAprovadorRumo == "") {
                    Ext.Msg.show({ title: 'Aviso', msg: 'O campo hora(hh:mm) da devolução da OS para a Rumo é obrigatório!', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                    return;
                }

                if (comparaDatas((txtDataDevolucaoOSRumo + " " + txtHoraMinutoAprovadorRumo), dataHora)) {
                    Ext.Msg.show({ title: 'Aviso', msg: 'O campo Data e hora da devolução da OS para a Rumo não pode ser inferior a data e hora da OS!', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                    return;
                }


                var txtNomeAprovadorRumo = Ext.getCmp("txtNomeAprovadorRumo").getValue();
                if (txtNomeAprovadorRumo == "") {
                    Ext.Msg.show({ title: 'Aviso', msg: 'O campo Nome do Aprovador na Rumo é obrigatório!', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                    return;
                }
                var txtNomeAprovadorFornecedor = Ext.getCmp("txtNomeAprovadorFornecedor").getValue();
                if (txtNomeAprovadorFornecedor == "") {
                    Ext.Msg.show({ title: 'Aviso', msg: 'O campo Nome do Aprovador no Fornecedor é obrigatório!', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                    return;
                }

                var txtMatriculaAprovadorRumo = Ext.getCmp("txtMatriculaAprovadorRumo").getValue();
                var txtMatriculaAprovadorFornecedor = Ext.getCmp("txtMatriculaAprovadorFornecedor").getValue();

                var iTp = 0, qtTp = Ext.getCmp("rbtTipoServico").items.length;
                //Futuramente pode ser incluído mais tipos.
                while (iTp < qtTp) {
                    if (Ext.getCmp("rbtTipoServico").items.items[iTp].checked)
                        idTipo = Ext.getCmp("rbtTipoServico").items.items[iTp].inputValue;
                    iTp++;
                }

                var vagoes = [];

                //Recupera vagões do GRID
                var itens = gridVagoes.getStore().getRange();
                //console.log(itens);
                for (var i = 0; i < itens.length; i++) {

                    var _osLimpezaVagaoItemDto = new OSLimpezaVagaoItemDto();
                    _osLimpezaVagaoItemDto.IdItem = itens[i].data.IdItem;
                    _osLimpezaVagaoItemDto.Concluido = itens[i].data.Concluido;
                    _osLimpezaVagaoItemDto.Retrabalho = itens[i].data.Retrabalho;
                    _osLimpezaVagaoItemDto.Retrabalho = itens[i].data.Retrabalho;
                    _osLimpezaVagaoItemDto.Observacao = itens[i].data.Observacao;                    

                    vagoes.push(_osLimpezaVagaoItemDto);
                }

                Ext.getBody().mask("Processando dados...", "x-mask-loading");

                //Parâmetros para método SAVE
                var jsonRequest = $.toJSON({ idOs: idOs, idFornecedor: ddlFornecedorValue, idTipo: idTipo, dataEntregaOsFornecedor: txtDataEntregaOSFornecedor, horaEntregaOsFornecedor: txtHoraMinutoEntregaOSFornecedor,
                    dataDevolucaoOSRumo: txtDataDevolucaoOSRumo, horaDevolucaoOSRumo: txtHoraMinutoAprovadorRumo, nomeAprovadorRumo: txtNomeAprovadorRumo, matriculaAprovadorRumo: txtMatriculaAprovadorRumo,
                    nomeAprovadorFornecedor: txtNomeAprovadorFornecedor, matriculaAprovadorFornecedor: txtMatriculaAprovadorFornecedor, vagoes: vagoes
                });

                $.ajax({
                    url: '<%= Url.Action("FecharOs", "OSLimpezaVagao") %>',
                    type: "POST",
                    dataType: 'json',
                    data: jsonRequest,
                    timeout: 300000,
                    contentType: "application/json; charset=utf-8",
                    failure: function (conn, data) {
                        Ext.Msg.alert("Mensagem de Erro", "Ocorreu um erro inesperado.");
                        Ext.getBody().unmask();
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(xhr.status);
                        Ext.getBody().unmask();
                    },
                    success: function (result) {

                        Ext.getBody().unmask();
                        Ext.Msg.show({ title: 'Aviso', msg: 'OS fechada com sucesso. Redirecionando...' });

                        var url = '<%= Url.Action("Index", "OSLimpezaVagao") %>';
                        window.location = url;
                        
                    }

                });
            }
        }

        function Voltar() {
           window.history.back();
        }

        //***************************** BOTOES *****************************//
        var bntConfirmar = {
            name: 'bntConfirmar',
            id: 'bntConfirmar',
            text: 'Confirmar',
            iconCls: 'icon-accept',
            handler: ValidarEFecharOS
        };

        var btnVoltar = {
            name: 'btnVoltar',
            id: 'btnVoltar',
            text: 'Voltar',
            iconCls: 'icon-left',
            handler: Voltar
        };

        //***************************** PAINEL *****************************//
        var panelForm = new Ext.form.FormPanel({
            id: 'panelForm',
            layout: 'form',
            labelAlign: 'top',
            border: false,
            autoHeight: true,
            title: "Fechamento de OS",
            region: 'center',
            bodyStyle: 'padding: 15px',
            items: [containerLinha1, containerLinha2, containerLinha3, containerLinha4],
            buttonAlign: "center",
            buttons: [bntConfirmar, btnVoltar]

        });

        //***************************** RENDER *****************************//
        Ext.onReady(function () {
            panelForm.render("divContent");

            $("#ckAllConcluido").change(function () {

                if ($(this).is(":checked")) {
                    SetAllCheckConcluido();
                }
                else
                    NotSetAllCheckConcluido();
            });

            $("#checkRetrabalhoAll").change(function () {
                alert('retrabalho');
                if ($(this).is(":checked")) {
                    SetAllCheckRetrabalho();
                }
                else
                    NotSetAllCheckRetrabalho();
            });

            fornecedorStore.on('load', function (store) {
                
                var range = ddlFornecedor.getStore().getRange();
                var existe = false;
                $.each(range, function (c, col) {

                    //Verifica se registro que usuario está tentando inserir já existe
                    if (col.data.IdFornecedorOs == idFornecedor) {
                        existe = true;
                    }
                });

                if (existe)
                    Ext.getCmp('ddlFornecedor').setValue(idFornecedor);
            });

            gridVagoesStore.load({ params: { start: 0, limit: 50} });

            $("#txtDataEntregaOSFornecedor").val("__/__/____");
            $("#txtDataDevolucaoOSRumo").val("__/__/____");

        });
       
    </script>
</asp:Content>
<asp:Content ID="MainContent" ContentPlaceHolderID="Content" runat="server">
    <div id="header-content">
        <h1>
            Manutenção</h1>
        <small>Você está em Manutenção > Fechamento de Ordem de Serviço</small>
        <br />
    </div>
    <div id="divContent">
    </div>
</asp:Content>
