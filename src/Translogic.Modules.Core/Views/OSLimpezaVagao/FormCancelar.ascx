﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<script type="text/javascript">

    var txtJustificativa = {
        xtype: 'textarea',
        name: 'txtJustificativa',
        id: 'txtJustificativa',
        fieldLabel: 'Justificativa',
        maxlength: '255',
        maxLengthText: '255',
        width: 240,
        height: 60
    };

    var hdnIdOs = {
        xtype: 'hidden',
        name: 'hdnIdOs',
        id: 'hdnIdOs'
    };

    var formJustificativa = new Ext.form.FormPanel
	({
	    id: 'formJustificativa',
	    labelWidth: 80,
	    width: 300,
	    height: 200,
	    autoScroll: true,
	    bodyStyle: 'padding: 15px',
	    labelAlign: 'top',
	    items:
		[
            hdnIdOs, txtJustificativa
		],
	    buttonAlign: "center",
	    buttons: [
            {
                text: 'Confirmar',
                type: 'submit',
                handler: function (b, e) {

                    if (Validar()) {
                        SalvarJustificativa();
                    }
                }
            }, {
                text: 'Cancelar',
                type: 'submit',
                handler: function (b, e) {
                    windowCancelarOs.hide();
                }
            }
        ]
        });


        function Validar() {
            var justificativa = Ext.getCmp("txtJustificativa").getValue();
          
            if (justificativa == "") {
                Ext.Msg.show({ title: 'Aviso', msg: 'O campo justificativa é obrigatório', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                return false;
            }
            return true;
        }

        function SalvarJustificativa() {

            var idOs = Ext.getCmp("hdnIdOs").getValue();
            var justificativa = Ext.getCmp("txtJustificativa").getValue();

            Ext.getBody().mask("Processando dados...", "x-mask-loading");

            var jsonRequest = $.toJSON({ idOs: idOs, justificativa: justificativa });
            //console.log(jsonRequest);

            $.ajax({
                url: '<%= Url.Action("CancelarOS", "OSLimpezaVagao") %>',
                type: "POST",
                dataType: 'json',
                data: jsonRequest,
                timeout: 300000,
                contentType: "application/json; charset=utf-8",
                failure: function (conn, data) {
                    Ext.Msg.alert("Mensagem de Erro", "Ocorreu um erro inesperado.");
                    Ext.getBody().unmask();
                },
                success: function (result) {

                    Ext.getBody().unmask();
                    windowCancelarOs.hide();

                    gridOSLimpezaVagaoStore.load({ params: { start: 0, limit: 50} });

                }
            });
        }

</script>

