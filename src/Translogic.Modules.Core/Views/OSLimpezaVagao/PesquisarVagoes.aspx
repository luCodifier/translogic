﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Interna.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content runat="server" ID="HeadContent" ContentPlaceHolderID="HeadContent">

    <script type="text/javascript">

        var numOs = '<%=ViewData["idOs"] %>';
        var local = '<%=ViewData["local"] %>';

        //#Region StoreDDLs

        var storeLotacao = new Ext.data.JsonStore({ 
            root: "Items",
            autoLoad: true,
            proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("ObterLotacoes", "OSLimpezaVagao") %>', timeout: 600000 }),
            id: 'storeLotacao',
            fields: ['IdLotacao', 'DescricaoLotacao']
        });

        //#endRegion


        //#Region StoreGridMotivoVagoes

        var sm = new Ext.grid.CheckboxSelectionModel({
           
        }); 

        var gridOSVagoesStore = new Ext.data.JsonStore({
            root: "Items",
            totalProperty: 'Total',
            id: 'gridOSVagoesStore',
            name: 'gridOSVagoesStore',
            proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("ConsultaVagoes", "OSLimpezaVagao") %>', timeout: 600000 }),
            paramNames: {
                sort: "detalhesPaginacaoWeb.Sort",
                dir: "detalhesPaginacaoWeb.Dir",
                start: "detalhesPaginacaoWeb.Start",
                limit: "detalhesPaginacaoWeb.Limit"
            },
            fields: ['Checked','NumeroVagao', 'Serie', 'Local', 'Linha', 'Sequencia', 'DescricaoLotacao']
        });


        //#endRegion     

        //#Region FuncoesDaTela

        function PesquisarVagoes() {
            //Reseta botom checkboxtodos
            if ($(".x-grid3-hd-checker").hasClass('x-grid3-hd-checker-on')) {
                $(".x-grid3-hd-checker").removeClass('x-grid3-hd-checker-on')
            }

            if (Ext.getCmp("grid-filtros").getForm().isValid()) {
                //Gif Loading
                Ext.getBody().mask("Processando dados...", "x-mask-loading");
                    gridOSVagoesStore.load({ params: { start: 0, limit: 50} });
                Ext.getBody().unmask();
                }else{ Ext.Msg.show({ title: 'Aviso', msg: 'O campo Local é obrigatório!', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                return;
                } 
        }

        function Limpar() {
            gridOSVagoesStore.removeAll();
            Ext.getCmp("grid-filtros").getForm().reset();

            VerificarSelecionarHabilitaConfirmar();
        }


        function VerificaVagoesSelecionados() {
            var resultado = false;
            var selection = sm.getSelections();
            var iCont = 0;

            if (selection && selection.length > 0) {
                var idSituacao = selection[0].data.IdSituacao;

                selection = sm.getSelections();

                $.each(selection, function (i, e) {
                    if (idSituacao == e.data.IdSituacao) {
                        iCont++;
                    }
                });

                resultado = selection.length == iCont ? true : false;
            }

            return resultado;
        }
        // Busca e armazena lista de ids de vagões
        function BuscaIDVagoesSelecionados() {
            var selection = sm.getSelections();

            if (selection && selection.length > 0) {
                var numVagao = selection[0].data.IdSituacao;
                var listaVagoes = [];

                selection = sm.getSelections();

                $.each(selection, function (i, e) {
                    listaVagoes[i] = e.data.NumeroVagao;
                });
            }

            return listaVagoes;
        }

        function VerificarSelecionarHabilitaConfirmar() {
            var selection = sm.getSelections();
            var selecionado = selection.length > 0 ? true : false;
            

            if (selecionado == true) {
                Ext.getCmp("btnConfirmar").setDisabled(false);
            } else {
                Ext.getCmp("btnConfirmar").setDisabled(true);
            }
        }
        //#endRegion

        //#Region PainelMotivosVagoes

        $(function () {

            // Toolbar da Grid
            var pagingToolbar = new Ext.PagingToolbar({
                pageSize: 50,
                store: gridOSVagoesStore,
                displayInfo: true,
                displayMsg: App.Resources.Web.GridExibindoRegistros,
                emptyMsg: App.Resources.Web.GridSemRegistros,
                paramNames: {
                    start: "detalhesPaginacaoWeb.Start",
                    limit: "detalhesPaginacaoWeb.Limit"
                }
            });

            var cm = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true
                },
                columns: [
                    new Ext.grid.RowNumberer(),
                    new Ext.grid.CheckboxSelectionModel(),
                    { header: 'Nº Vagão', dataIndex: "NumeroVagao", sortable: false, width: 80 },
                    { header: 'Série', dataIndex: "Serie", sortable: false, width: 200 },
                    { header: 'Local', dataIndex: "Local", sortable: false, width: 60 },
                    { header: 'Linha', dataIndex: "Linha", sortable: false, width: 150 },
                    { header: 'Sequência', dataIndex: "Sequencia", sortable: false, width: 150 },
                    { header: 'Lotação', dataIndex: "DescricaoLotacao", sortable: false, width: 200 }
                ]
            });

            var gridPainelVagoes = new Ext.grid.EditorGridPanel({
                id: 'gridPainelListaVagoes',
                name: 'gridPainelListaVagoes',
                autoLoadGrid: false,
                height: 360,
                width: 360,
                stripeRows: true,
                viewConfig: {
                    forceFit: true,
                    emptyText: 'Não possui dado(s) para exibição.'
                },
                cm: cm,
                region: 'center',
                autoScroll: true,
                loadMask: { msg: App.Resources.Web.Carregando },
                store: gridOSVagoesStore,
                tbar: [
                    {
                        id: 'btnConfirmar',
                        text: 'Confirmar',
                        tooltip: 'Confirmar',
                        iconCls: 'icon-tick',
                        disabled: true,
                        handler: function (c) {

                            var local = Ext.getCmp("txtLocal").getValue();
                            if (local == "") {
                                Limpar();
                                Ext.Msg.show({ title: 'Aviso', msg: 'O campo Local é obrigatório', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                                return;
                            }


                            if (VerificaVagoesSelecionados() == true) {
                                $(function () {
                                    //Gif Loading
                                    Ext.getBody().mask("Processando dados...", "x-mask-loading");
                                    var url = '<%= Url.Action("SalvarVagoesDaOS", "OSLimpezaVagao") %>?local=' + local + "&idOs=" + numOs;
                                    var idsVagoes = BuscaIDVagoesSelecionados();
                                    $.ajax({
                                        type: "POST",
                                        url: url,
                                        data: $.toJSON(idsVagoes),
                                        contentType: "application/json; charset=utf-8",
                                        dataType: "json",
                                        success: function (idOS) {

                                            var url = '<%= Url.Action("CriarOSLimpeza", "OSLimpezaVagao")%>?idOS=' + idOS + '&local=' + local;

                                            Ext.getBody().unmask();

                                            window.location = url;

                                        },
                                        error: function (request, status, errorThrown) {
                                            alert(errorThrown);
                                            Ext.getBody().unmask();
                                        }
                                    });
                                });
                            } else { Ext.Msg.show({ title: 'Aviso', msg: 'Selecione ao menos um vagão da lista para gerar O.S', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR }) };
                        }
                    },
                    {
                        id: 'btnVoltar',
                        text: 'Voltar',
                        tooltip: 'Voltar',
                        iconCls: 'icon-left',
                        handler: function (c) {

                            Ext.getBody().mask("Processando dados...", "x-mask-loading");
                            var url = '<%= Url.Action("Index", "OSLimpezaVagao")%>';

                            window.location = url;
                        }
                    }
                ],
                bbar: pagingToolbar,
                sm: sm,
                listeners: {
                    afterrender: function () {
                        var me = this;
                        gridOSVagoesStore.on('load', function () {

                            var data = me.getStore().data.items;
                            var recs = [];
                            Ext.each(data, function (item, index) {
                                if (item.data.Checked) {
                                    recs.push(index);
                                }
                            });
                            me.getSelectionModel().selectRows(recs);
                            if (recs.length > 0) {
                                Ext.getCmp("btnConfirmar").setDisabled(false);
                            }
                        });
                        
                    },
                    cellclick: function (e, rowIndex, columnIndex) {

                        VerificarSelecionarHabilitaConfirmar();
                    }
                }
            });

            gridPainelVagoes.getStore().proxy.on('beforeload', function (p, params) {


                var numVagao = Ext.getCmp("txtNumeroVagao").getValue();
                var serie = Ext.getCmp("txtSerieVagao").getValue();
                var sequenciaInicio = Ext.getCmp("txtSequenciaInicio").getValue();
                var sequenciaFim = Ext.getCmp("txtSequenciaFim").getValue();
                var local = Ext.getCmp("txtLocal").getValue();
                var linha = Ext.getCmp("txtLinha").getValue();

                var lotacao = (cmbLotacao.getValue() == "Todos" || cmbLotacao.getValue() == "0") ? "" : cmbLotacao.getValue();

                if (numOs != "") {
                    params['numOs'] = numOs;
                }

                params['numVagao'] = numVagao;
                params['serie'] = serie;
                params['local'] = local;
                params['linha'] = linha;
                params['lotacao'] = lotacao;
                params['sequenciaInicio'] = sequenciaInicio == "" ? "0" : sequenciaInicio;
                params['sequenciaFim'] = sequenciaFim == "" ? "0" : sequenciaFim;


            });


            //#endRegion

            //#Region PesquisaFiltros




            var txtNumeroVagao = {
                xtype: 'textfield',
                name: 'txtNumeroVagao',
                id: 'txtNumeroVagao',
                fieldLabel: 'Nº de Vagões separados por XXXX;XXXX;...',
                autoCreate: {
                    tag: 'input',
                    type: 'text',
                    autocomplete: 'off',
                    maxlength: '255'
                },
                maskRe: /[0-9+;]/,
                style: 'text-transform:uppercase;',
                width: 300
            };

            var txtSerieVagao = {
                xtype: 'textfield',
                name: 'txtSerieVagao',
                id: 'txtSerieVagao',
                fieldLabel: 'Série',
                autoCreate: {
                    tag: 'input',
                    type: 'text',
                    autocomplete: 'off',
                    maxlength: '3'
                },
                maskRe: /[A-Za-z]/,
                style: 'text-transform:uppercase;',
                width: 85
            };

            var txtSequenciaInicio = {
                xtype: 'numberfield',
                name: 'txtSequenciaInicio',
                id: 'txtSequenciaInicio',
                fieldLabel: 'Sequência de',
                autoCreate: {
                    tag: 'input',
                    type: 'text',
                    autocomplete: 'off',
                    maxlength: '3'
                },
                maskRe: /[A-Za-z]/,
                style: 'text-transform:uppercase;',
                width: 85
            };

            var txtSequenciaFim = {
                xtype: 'numberfield',
                name: 'txtSequenciaFim',
                id: 'txtSequenciaFim',
                fieldLabel: 'até',
                autoCreate: {
                    tag: 'input',
                    type: 'text',
                    autocomplete: 'off',
                    maxlength: '3'
                },
                maskRe: /[A-Za-z]/,
                style: 'text-transform:uppercase;',
                width: 85
            };

            var txtLocal = {
                xtype: 'textfield',
                name: 'txtLocal',
                id: 'txtLocal',
                fieldLabel: 'Local',
                autoCreate: {
                    tag: 'input',
                    type: 'text',
                    autocomplete: 'off',
                    maxlength: '3'
                },
                allowBlank: false,
                blankText: 'Você deve informar Local!',
                maskRe: /[A-Za-z]/,
                style: 'text-transform:uppercase;',
                width: 85,
                value: local
            };

            var txtLinha = {
                xtype: 'textfield',
                name: 'txtLinha',
                id: 'txtLinha',
                fieldLabel: 'Linha',
                autoCreate: {
                    tag: 'input',
                    type: 'text',
                    autocomplete: 'off',
                    maxlength: '10'
                },
                style: 'text-transform:uppercase;',
                width: 85
            };

            var cmbLotacao = new Ext.form.ComboBox({
                editable: true,
                typeAhead: true,
                forceSelection: true,
                disableKeyFilter: true,
                triggerAction: 'all',
                lazyRender: true,
                mode: 'local',
                valueField: 'IdLotacao',
                displayField: 'DescricaoLotacao',
                fieldLabel: 'Lotacão',
                id: 'cmbLotacao',
                name: 'cmbLotacao',
                width: 200,
                store: storeLotacao,
                value: 'Todos'
            });

            //Item do Array de Componentes (1 por cada componente criado)
            var arrtxtNumeroVagao = {
                width: 315,
                layout: 'form',
                border: false,
                items: [txtNumeroVagao]
            };

            var arrSerieVagao = {
                width: 105,
                layout: 'form',
                border: false,
                items: [txtSerieVagao]
            };


            var arrSequencioInicio = {
                width: 105,
                layout: 'form',
                border: false,
                items: [txtSequenciaInicio]
            };

            var arrSequencioFim = {
                width: 105,
                layout: 'form',
                border: false,
                items: [txtSequenciaFim]
            };

            var arrLocal = {
                width: 105,
                layout: 'form',
                border: false,
                items: [txtLocal]
            };

            var arrLinha = {
                width: 105,
                layout: 'form',
                border: false,
                items: [txtLinha]
            };

            var arrLotacao = {
                width: 215,
                layout: 'form',
                border: false,
                items: [cmbLotacao]
            };

            //Array de componentes do form 
            var arrCampos = {
                layout: 'column',
                border: false,
                items: [arrtxtNumeroVagao, arrSerieVagao, arrSequencioInicio, arrSequencioFim, arrLocal, arrLinha, arrLotacao]
            };


            //Conteudo do Form que sera criado
            var filtros = new Ext.form.FormPanel({
                id: 'grid-filtros',
                title: "Filtros",
                region: 'center',
                bodyStyle: 'padding:15px;',                
                layout: 'form',
                labelAlign: 'top',
                items: [arrCampos],
                buttonAlign: "center",
                buttons:
                    [{
                        text: 'Filtrar Vagões',
                        type: 'submit',
                        iconCls: 'icon-find',
                        handler: function (b, e) {
                            PesquisarVagoes();
                        }
                    },
                        {
                            text: 'Limpar',
                            type: 'submit',
                            handler: function (b, e) {
                                Limpar();
                            }
                        }
                    ]
            });

            //#endRegion

            new Ext.Viewport({
                layout: 'border',
                margins: 10,
                items: [
                    {
                        region: 'north',
                        height: 190,
                        autoScroll: true,
                        items: [{
                            region: 'center',
                            applyTo: 'header-content'
                        },
                            filtros]
                    },
                    gridPainelVagoes
                ]
            });
        });

        //Verificação de checkboxtodos (Habilita e Desabilita Botão Confirmar)
        Ext.onReady(function () {
            $(".x-grid3-hd-checker").click(function () {

                VerificarSelecionarHabilitaConfirmar();
            });

            if (numOs != "") {
                PesquisarVagoes();              
            }
        });
    </script>
</asp:Content>
<asp:Content runat="server" ID="Content2" ContentPlaceHolderID="Content">
    <div id="header-content">
        <h1>Lista de Vagões</h1>
        <small>Você está em Operação > Lista de Vagões</small>
        <br />
    </div>
</asp:Content>