﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>

<style type="text/css">
    .style1
    {
        width: 307px;
    }
    .style2
    {
    }
    .style5
    {
        width: 64px;
        height: 22px;
    }
    .style9
    {
        width: 64px;
        height: 27px;
    }
    .style13
    {
        height: 22px;
        font-size: 15px;
        text-align: center;
    }
    .style15
    {
        width: 302px;
    }
    .style16
    {
        width: 71px;
        height: 22px;
    }
    .style17
    {
        width: 71px;
        height: 27px;
    }
    .style18
    {
        text-align: center;
        font-size: 13pt;
        height: 26px;
    }
    .style19
    {
        width: 404px;
        height: 22px;
    }
    .style20
    {
        width: 404px;
        height: 27px;
    }
    .style21
    {
        width: 56px;
        height: 22px;
    }
    .style22
    {
        width: 56px;
        height: 27px;
    }
</style>
<div>
    <table border="0" style="font-family: Arial; font-size: 11px;" cellpadding="0" cellspacing="0">
        <tr>
            <td style="border: 1px solid #000000;" colspan="2" class="style13">
                <strong>Número da OS:
                    <%=ViewData["idOS"]%>
                </strong>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="style15" style="border-top-width: 1px; border-top-style: solid; border-top-color: #000000; border-right-width: 1px; border-right-style: solid; border-right-color: #000000; border-left-width: 1px; border-left-style: solid; border-left-color: #000000;">
                <span style="font-weight: bold;">Número da OS Parcial:</span><br />
                <%=ViewData["IdOsParcial"]%><br />
                &nbsp;
            </td>
            <td class="style1" style="border-top-width: 1px; border-top-style: solid; border-top-color: #000000; border-right-width: 1px; border-right-style: solid; border-right-color: #000000;">
                <span style="font-weight: bold;">Data / Hora:</span><br />
                <%=ViewData["Data"]%><br />
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="style15" style="border-top-width: 1px; border-top-style: solid; border-top-color: #000000; border-right-width: 1px; border-right-style: solid; border-right-color: #000000; border-left-width: 1px; border-left-style: solid; border-left-color: #000000;">
                <span style="font-weight: bold;">Fornecedor:</span><br />
                <%=ViewData["Fornecedor"]%><br />
                &nbsp;
            </td>
            <td class="style1" style="border-top-width: 1px; border-top-style: solid; border-top-color: #000000; border-right-width: 1px; border-right-style: solid; border-right-color: #000000;">
                <span style="font-weight: bold;">Local de prestação do serviço:</span><br />
                <%=ViewData["LocalServico"]%><br />
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="style15" style="border-top-width: 1px; border-top-style: solid; border-top-color: #000000; border-right-width: 1px; border-right-style: solid; border-right-color: #000000; border-left-width: 1px; border-left-style: solid; border-left-color: #000000; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #000000;">
                <span style="font-weight: bold;">Tipo de serviço:</span><br />
                <%=ViewData["Tipo"]%><br />
                &nbsp;
            </td>
            <td class="style1" style="border-top-width: 1px; border-top-style: solid; border-top-color: #000000; border-right-width: 1px; border-right-style: solid; border-right-color: #000000; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #000000;">
                <span style="font-weight: bold;">Status:</span><br />
                <%=ViewData["Status"]%><br />
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style18" colspan="2" style="border: 1px solid #000000">
                <strong style="text-align: center">Vagões </strong>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="style2" colspan="2">
                <table cellpadding="0" cellspacing="0" style="font-size: 11px; border-top-width: 1px; border-top-style: solid; border-top-color: #000000; border-right-width: 1px; border-right-style: solid; border-right-color: #000000; width: 100%;">
                    <tr style="font-weight: bold; text-align: center;">
                        <th style="border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #000000; border-left-width: 1px; border-left-style: solid; border-left-color: #000000;" class="style21">
                            Número
                        </th>
                        <th style="border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #000000; border-left-width: 1px; border-left-style: solid; border-left-color: #000000;" class="style5">
                            Concluído
                        </th>
                        <th style="border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #000000; border-left-width: 1px; border-left-style: solid; border-left-color: #000000;" class="style19">
                            Observação
                        </th>
                        <th style="border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #000000; border-left-width: 1px; border-left-style: solid; border-left-color: #000000;" class="style16">
                            Retrabalho
                        </th>
                    </tr>
                    <%
                        var list = (List<Translogic.Modules.Core.Domain.Model.Dto.OSLimpezaVagaoItemDto>)ViewData["Vagoes"];

                        foreach (var item in list)
                        {%>
                    <tr>
                        <td style="border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #000000; border-left-width: 1px; border-left-style: solid; border-left-color: #000000;" class="style22">
                            <%=item.NumeroVagao%>
                        </td>
                        <td style="border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #000000; border-left-width: 1px; border-left-style: solid; border-left-color: #000000;" class="style9">
                            <%=item.ConcluidoDescricao%>
                        </td>
                        <td style="border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #000000; border-left-width: 1px; border-left-style: solid; border-left-color: #000000;" class="style20">
                            <%=item.Observacao%>
                        </td>
                        <td style="border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #000000; border-left-width: 1px; border-left-style: solid; border-left-color: #000000;" class="style17">
                            <%=item.RetrabalhoDescricao%>
                        </td>
                    </tr>
                    <%
                    }
                    %>
                </table>
            </td>
        </tr>

         <tr>
            <td>&nbsp;</td>
        </tr>
        
        <tr>
            <td colspan="2" style="border: 1px solid #000000">
                <span style="font-weight: bold;">Justificativa cancelamento:</span><br />
                <%=ViewData["Justificativa"]%>&nbsp;<br />
            </td>
            </tr>

        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style15" style="border-top-width: 1px; border-top-style: solid; border-top-color: #000000; border-right-width: 1px; border-right-style: solid; border-right-color: #000000; border-left-width: 1px; border-left-style: solid; border-left-color: #000000;">
                <span style="font-weight: bold;">Data/hora entrega de OS ao Fornecedor:</span><br />
                <%=ViewData["DataHoraEntregaOSFornecedor"]%><br />
                &nbsp;
            </td>
            <td class="style1" style="border-top-width: 1px; border-top-style: solid; border-top-color: #000000; border-right-width: 1px; border-right-style: solid; border-right-color: #000000;">
                <span style="font-weight: bold;">Data/hora devolução OS a Rumo:</span><br />
                <%=ViewData["DataHoraDevolucaoOS"]%><br />
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="style15" style="border-top-width: 1px; border-top-style: solid; border-top-color: #000000; border-right-width: 1px; border-right-style: solid; border-right-color: #000000; border-left-width: 1px; border-left-style: solid; border-left-color: #000000;">
                <span style="font-weight: bold;">Nome do aprovador do Fornecedor:</span><br />
                <%=ViewData["NomeAprovadorFornecedor"]%><br />
                &nbsp;
            </td>
            <td class="style1" style="border-top-width: 1px; border-top-style: solid; border-top-color: #000000; border-right-width: 1px; border-right-style: solid; border-right-color: #000000;">
                <span style="font-weight: bold;">Nome do aprovador da Rumo:</span><br />
                <%=ViewData["NomeAprovadorRumo"]%><br />
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="style15" style="border-top-width: 1px; border-top-style: solid; border-top-color: #000000; border-right-width: 1px; border-right-style: solid; border-right-color: #000000; border-left-width: 1px; border-left-style: solid; border-left-color: #000000;">
                <span style="font-weight: bold;">Matrícula do aprovador do Fornecedor:</span><br />
                <%=ViewData["MatriculaAprovadorFornecedor"]%><br />
                &nbsp;
            </td>
            <td class="style1" style="border-top-width: 1px; border-top-style: solid; border-top-color: #000000; border-right-width: 1px; border-right-style: solid; border-right-color: #000000;">
                <span style="font-weight: bold;">Matrícula do aprovador da Rumo:</span><br />
                <%=ViewData["MatriculaAprovadorRumo"]%><br />
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="style15" style="border-top-width: 1px; border-top-style: solid; border-top-color: #000000; border-right-width: 1px; border-right-style: solid; border-right-color: #000000; border-left-width: 1px; border-left-style: solid; border-left-color: #000000;">
                <span style="font-weight: bold;">Assinatura do aprovador do Fornecedor:</span><br />
                <br />
                &nbsp;
            </td>
            <td class="style1" style="border-top-width: 1px; border-top-style: solid; border-top-color: #000000; border-right-width: 1px; border-right-style: solid; border-right-color: #000000;">
                <span style="font-weight: bold;">Assinatura do aprovador da Rumo:</span><br />
                <br />
                &nbsp;
            </td>
        </tr>
        <tr>
            <td style="border-top-width: 1px; border-top-style: solid; border-top-color: #000000; border-right-width: 1px; border-right-style: solid; border-right-color: #000000; border-left-width: 1px; border-left-style: solid; border-left-color: #000000; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #000000;">
                <span style="font-weight: bold">Última alteração:</span><br />
                <%=ViewData["UltimaAlteracao"]%><br />
                &nbsp;
            </td>
            <td style="border-top-width: 1px; border-top-style: solid; border-top-color: #000000; border-right-width: 1px; border-right-style: solid; border-right-color: #000000; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #000000;">
                <span style="font-weight: bold">Usuário:</span><br />
                <%=ViewData["Usuario"]%><br />
                &nbsp;
            </td>
        </tr>
    </table>
</div>