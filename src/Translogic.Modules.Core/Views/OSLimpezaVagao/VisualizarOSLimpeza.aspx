﻿<%@ Page Title="" Language="C#" Inherits="System.Web.Mvc.ViewPage" MasterPageFile="~/Views/Shared/Interna.master" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server">

    <script type="text/javascript">
        //***************************** VARIÁVEIS E INICIALIZAÇÃO**************//
        var idOs = '<%=ViewData["idOs"] %>';

        //***************************** CAMPOS *****************************//

        var lblNumOS = {
            xtype: 'label',
            name: 'lblNumOS',
            id: 'lblNumOS',
            fieldLabel: 'Número OS'
        };

        var lblOsParcial = {
            xtype: 'label',
            name: 'lblOsParcial',
            id: 'lblOsParcial',
            fieldLabel: 'Número OS Parcial'
        };

        var lblDataHora = {
            xtype: 'label',
            name: 'lblDataHora',
            id: 'lblDataHora',
            fieldLabel: 'Data e hora'
        };

        var lblLocal = {
            xtype: 'label',
            name: 'lblLocal',
            id: 'lblLocal',
            fieldLabel: 'Local'
        };

        var lblFornecedor = {
            xtype: 'label',
            name: 'lblFornecedor',
            id: 'lblFornecedor',
            fieldLabel: 'Fornecedor'
        };

        var lblTipoServico = {
            xtype: 'label',
            name: 'lblTipoServico',
            id: 'lblTipoServico',
            fieldLabel: 'Tipo Serviço'
        };

        var lblStatus = {
            xtype: 'label',
            name: 'lblStatus',
            id: 'lblStatus',
            fieldLabel: 'Status'
        };

        var lblDataDevolucaoOSRumo = {
            xtype: 'label',
            name: 'lblDataDevolucaoOSRumo',
            id: 'lblDataDevolucaoOSRumo',
            fieldLabel: 'Data e hora da devolução da OS para a Rumo'
        };

        var lblNomeAprovadorRumo = {
            xtype: 'label',
            name: 'lblNomeAprovadorRumo',
            id: 'lblNomeAprovadorRumo',
            fieldLabel: 'Nome do Aprovador na Rumo'
        };

        var lblMatriculaAprovadorRumo = {
            xtype: 'label',
            name: 'lblMatriculaAprovadorRumo',
            id: 'lblMatriculaAprovadorRumo',
            fieldLabel: 'Matrícula do Aprovador na Rumo'
        };

        var lblDataEntregaOSFornecedor = {
            xtype: 'label',
            name: 'lblDataEntregaOSFornecedor',
            id: 'lblDataEntregaOSFornecedor',
            fieldLabel: 'Data e hora da entrega da OS para o fornecedor'
        };

        var lblNomeAprovadorFornecedor = {
            xtype: 'label',
            name: 'lblNomeAprovadorFornecedor',
            id: 'lblNomeAprovadorFornecedor',
            fieldLabel: 'Nome do Aprovador no Fornecedor'
        };

        var lblMatriculaAprovadorFornecedor = {
            xtype: 'label',
            name: 'lblMatriculaAprovadorFornecedor',
            id: 'lblMatriculaAprovadorFornecedor',
            fieldLabel: 'Matrícula do Aprovador no Fornecedor'
        };
       

        //***************************** LAYOUT *****************************//

        var fieldNumOS = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            width: 95,
            items: [lblNumOS]
        };

        var fieldNumOSParcial = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            width: 120,
            items: [lblOsParcial]
        };

        var fieldDataHora = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            width: 130,
            items: [lblDataHora]
        };

        var fieldLocal = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            width: 42,
            items: [lblLocal]
        };

        var fieldFornecedor = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            width: 330,
            items: [lblFornecedor]
        };

        var fieldTipo = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            items: [lblTipoServico]
        };

        var fieldStatus = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            items: [lblStatus]
        };       

        //Dados fechamento
        var fieldDataEntregaOSFornecedor = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            items: [lblDataEntregaOSFornecedor]
        };

        var fieldDataDevolucaoOSRumo = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            items: [lblDataDevolucaoOSRumo]
        };

        var fieldNomeAprovadorRumo = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            items: [lblNomeAprovadorRumo]
        };

        var fieldMatriculaAprovadorRumo = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            items: [lblMatriculaAprovadorRumo]
        };

        var fieldNomeAprovadorFornecedor = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            items: [lblNomeAprovadorFornecedor]
        };

        var fieldMatriculaAprovadorFornecedor = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            items: [lblMatriculaAprovadorFornecedor]
        };

        //GRIDs
        var gridVagoes = new Translogic.PaginatedGrid({
            id: 'gridVagoes',
            name: 'gridVagoes',
            autoLoadGrid: true,
            viewConfig: {
                forceFit: true,
                emptyText: 'Não possui dado(s) para exibição.'
            },
            height: 210,
            autoScroll: true,
            fields: [
                    'IdItem',
                    'NumeroVagao',
                    'ConcluidoDescricao',
                    'RetrabalhoDescricao',
                    'Observacao'
                ],
            url: '<%= Url.Action("ObterVagoesDaOs", "OSLimpezaVagao") %>',
            columns: [
				    new Ext.grid.RowNumberer(),
                    { header: 'Nº Vagão', dataIndex: "NumeroVagao", sortable: true, width: 130 },
                    { header: 'Serviço concluído', dataIndex: "ConcluidoDescricao", sortable: true, width: 100 },
                    { header: 'Teve retrabalho', dataIndex: "RetrabalhoDescricao", sortable: true, width: 100 },
                    { header: 'Observação', dataIndex: "Observacao", sortable: true, width: 100 }
                ]
        });

        gridVagoes.getStore().proxy.on('beforeload', function (p, params) {
            params['idOs'] = idOs;
        });

        //Containers
        var containerLinha1 = {
            layout: 'column',
            border: true,
            items: [fieldNumOS, fieldNumOSParcial, fieldDataHora, fieldLocal, fieldFornecedor, fieldTipo, fieldStatus]
        };

        var containerLinha2 = {
            layout: 'column',
            border: true,
            items: [gridVagoes]
        };

        var containerLinha3 = {
            layout: 'column',
            border: true,
            items: [fieldDataDevolucaoOSRumo, fieldNomeAprovadorRumo, fieldMatriculaAprovadorRumo]
        };

        var containerLinha4 = {
            layout: 'column',
            border: true,
            items: [fieldDataEntregaOSFornecedor, fieldNomeAprovadorFornecedor, fieldMatriculaAprovadorFornecedor]
        };

        //***************************** FUNCOES *****************************//

        function GetOs(idOs) {

            Ext.getBody().mask("Processando dados...", "x-mask-loading");

            var jsonRequest = $.toJSON({ idOs: idOs });

            $.ajax({
                url: '<%= Url.Action("ObterOs", "OSLimpezaVagao") %>',
                type: "POST",
                dataType: 'json',
                data: jsonRequest,
                timeout: 300000,
                contentType: "application/json; charset=utf-8",
                failure: function (conn, data) {
                    Ext.Msg.alert("Mensagem de Erro", "Ocorreu um erro inesperado.");
                    Ext.getBody().unmask();
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.status);
                    Ext.getBody().unmask();
                },
                success: function (result) {

                    Ext.getCmp("lblNumOS").setText(result.IdOs);
                    Ext.getCmp("lblOsParcial").setText(result.IdOsParcial);      

                    Ext.getCmp("lblDataHora").setText(result.Data);
                    Ext.getCmp("lblLocal").setText(result.LocalServico);
                    Ext.getCmp("lblFornecedor").setText(result.Fornecedor);
                    Ext.getCmp("lblTipoServico").setText(result.Tipo);
                    Ext.getCmp("lblStatus").setText(result.Status);

                    if (result.DataHoraDevolucaoOS != "01/01/0001 00:00:00")
                        Ext.getCmp("lblDataDevolucaoOSRumo").setText(result.DataHoraDevolucaoOS);
                    if (result.NomeAprovadorRumo != null)
                        Ext.getCmp("lblNomeAprovadorRumo").setText(result.NomeAprovadorRumo);
                    if (result.MatriculaAprovadorRumo != null)
                        Ext.getCmp("lblMatriculaAprovadorRumo").setText(result.MatriculaAprovadorRumo);
                    if (result.DataHoraEntregaOSFornecedor != "01/01/0001 00:00:00")
                        Ext.getCmp("lblDataEntregaOSFornecedor").setText(result.DataHoraEntregaOSFornecedor);
                    if (result.NomeAprovadorFornecedor != null)
                        Ext.getCmp("lblNomeAprovadorFornecedor").setText(result.NomeAprovadorFornecedor);
                    if (result.MatriculaAprovadorFornecedor != null)
                        Ext.getCmp("lblMatriculaAprovadorFornecedor").setText(result.MatriculaAprovadorFornecedor);

                    Ext.getBody().unmask();
                }

            });
        }

        function Voltar() {
            window.history.back();
        }

        //***************************** BOTOES *****************************//

        var btnVoltar = {
            name: 'btnVoltar',
            id: 'btnVoltar',
            text: 'Voltar',
            iconCls: 'icon-left',
            handler: Voltar 
        };

        //***************************** PAINEL *****************************//
        var panelForm = new Ext.form.FormPanel({
            id: 'panelForm',
            layout: 'form',
            labelAlign: 'top',
            border: false,
            autoHeight: true,
            title: "Visualização de OS de Limpeza",
            region: 'center',
            bodyStyle: 'padding: 15px',
            items: [containerLinha1, containerLinha2, containerLinha3, containerLinha4],
            buttonAlign: "center",
            buttons: [btnVoltar]
        });

        //***************************** RENDER *****************************//
        Ext.onReady(function () {
            panelForm.render("divContent");

            //Buscar dados da OS e preencher os campos
            GetOs(idOs);
        });
       
    </script>
</asp:Content>
<asp:Content ID="MainContent" ContentPlaceHolderID="Content" runat="server">
    <div id="header-content">
        <h1>
            Manutenção</h1>
        <small>Você está em Manutenção >Visualização de Ordem de Serviço</small>
        <br />
    </div>
    <div id="divContent">
    </div>
</asp:Content>
