﻿<%@ Page Title="" Language="C#" Inherits="System.Web.Mvc.ViewPage" MasterPageFile="~/Views/Shared/Interna.master" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server">

    <script type="text/javascript">
        //***************************** VARIÁVEIS E INICIALIZAÇÃO**************//
        var idOs = '<%=ViewData["idOs"] %>';
        var dataHora = '<%=ViewData["data"] %>';
        var local = '<%=ViewData["local"] %>';
        var idFornecedor = '<%=ViewData["idFornecedor"] %>';

        $(function () {

            //Setar campos recuperados da OS
            Ext.getCmp("txtNumOs").setValue(idOs);
            Ext.getCmp("txtDataHora").setValue(dataHora);
            Ext.getCmp("txtLocal").setValue(local);
        });

        //***************************** CAMPOS *****************************//
        var txtNumOs = {
            xtype: 'textfield',
            name: 'txtNumOs',
            id: 'txtNumOs',
            fieldLabel: 'Número OS',
            readonly: true,
            disabled: true,
            autoCreate: {
                tag: 'input',
                type: 'text',
                autocomplete: 'off',
                maxlength: '12',
                readonly: true
            },
            maskRe: /[0-9+;]/,
            width: 85
        };

        var txtDataHora = {
            xtype: 'textfield',
            name: 'txtDataHora',
            id: 'txtDataHora',
            fieldLabel: 'Data e hora',
            disabled: true,
            autoCreate: {
                tag: 'input',
                type: 'text',
                autocomplete: 'off',
                maxlength: '12',
                readonly: true
            },
            style: 'text-transform:uppercase;',
            width: 95
        };

        var fornecedorStore = new Ext.data.JsonStore({
            root: "Items",
            autoLoad: true,
            proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("ObterTodosFornecedores", "OSLimpezaVagao") %>', timeout: 600000 }),
            id: 'fornecedorStore',
            fields: ['IdFornecedorOs', 'Nome'],
            baseParams: { local: local }
        });

        var ddlFornecedor = new Ext.form.ComboBox({
            editable: false,
            typeAhead: true,
            forceSelection: true,
            disableKeyFilter: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            store: fornecedorStore,
            valueField: 'IdFornecedorOs',
            displayField: 'Nome',
            fieldLabel: 'Fornecedor',
            id: 'ddlFornecedor',
            width: 260,
            alowBlank: false,
            disabled : true
        });

        var txtLocal = {
            xtype: 'textfield',
            name: 'txtLocal',
            id: 'txtLocal',
            fieldLabel: 'Local',
            disabled: true,
            autoCreate: {
                tag: 'input',
                type: 'text',
                autocomplete: 'off',
                maxlength: '3',
                readonly: true
            },
            style: 'text-transform:uppercase;',
            width: 32,
            disabled : true
        };

        var txtJustificativa = new Ext.form.TextArea({
            id: 'txtJustificativa',
            name: 'txtJustificativa',
            fieldLabel: 'Justificativa',
            enableKeyEvents: true,
            autoCreate: {
                maxlength: '250',
                tag: 'textarea',
                rows: '3',
                cols: '140'
            },
            listeners: {
                keyUp: function (field, e) {
                    if (field.getRawValue().length >= 250) {
                        $("#txtJustificativa").val($("#txtJustificativa").val().substring(0, 250));
                    }

                }
            }
        });

        var ckMarcarTodos = {
            xtype: 'checkboxgroup',
            fieldLabel: 'Marcar todos',
            id: 'ckMarcarTodos',
            width: 260,
            name: 'ckMarcarTodos',
            style: 'margin-top:5px;',
            disabled: true,
            items: [
		            { boxLabel: 'Como concluído', name: 'ckAllConcluido', inputValue: 1 }
		           ],
            listeners: {
                change: {
                    fn: function (campo, checkeds) {

                        CheckGridColumns(checkeds);
                    }
                }
            }
        };

        //***************************** GRID VAGÕES**********************//

        var checkConcluido = new Ext.grid.CheckColumn({
            dataIndex: 'Concluido',
            header: "Concluído",
            id: 'checkConcluido',
            sortable: false,
            width: 80
        });

        var checkRetirar = new Ext.grid.CheckColumn({
            dataIndex: 'Retirar',
            header: 'Retirar vagão',
            id: 'checkRetirar',
            width: 80
        });

        var checkComProblema = new Ext.grid.CheckColumn({
            dataIndex: 'Comproblema',
            header: 'Vedação ou Gambitagem com problema',
            id: 'checkComProblema',
            width: 240
        });

        var gridVagoesStore = new Ext.data.JsonStore({
            root: "Items",
            totalProperty: 'Total',
            id: 'gridVagoesStore',
            name: 'gridVagoesStore',
            disableSelection: true,
            disabled: true,
            proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("ObterVagoesDaOs", "OSLimpezaVagao") %>', timeout: 600000 }),
            paramNames: {
                sort: "detalhesPaginacaoWeb.Sort",
                dir: "detalhesPaginacaoWeb.Dir",
                start: "detalhesPaginacaoWeb.Start",
                limit: "detalhesPaginacaoWeb.Limit"
            },
            fields: [
                    'IdItem',
                    'Serie',
                    'NumeroVagao',
                    'Concluido',
                    'Retrabalho',
                    'Observacao'
                ]
        });

        // Toolbar da Grid
        var pagingToolbar = new Ext.PagingToolbar({
            pageSize: 50,
            store: gridVagoesStore,
            displayInfo: true,
            displayMsg: App.Resources.Web.GridExibindoRegistros,
            emptyMsg: App.Resources.Web.GridSemRegistros,
            paramNames: {
                start: "detalhesPaginacaoWeb.Start",
                limit: "detalhesPaginacaoWeb.Limit"
            }
        });

        var rowEditing = {
            xtype: 'textfield',
            name: 'txtObservacao',
            id: 'txtObservacao',
            autoCreate: {
                tag: 'input',
                type: 'text',
                autocomplete: 'off',
                maxlength: '255'
            },
            width: 400
        };

        var gridColumns = new Ext.grid.ColumnModel({
            defaults: {
                sortable: true,
                menuDisabled: true
            },
            columns: [
                    new Ext.grid.RowNumberer(),
                    { header: 'Série', dataIndex: "Serie", sortable: false, width: 80 },
                    { header: 'Nº Vagão', dataIndex: "NumeroVagao", sortable: false, width: 80 },
                    checkConcluido,
                    checkRetirar,
                    checkComProblema,
                    { header: 'Observação', dataIndex: "Observacao", sortable: false, width: 400, editable: true, editor: rowEditing }
                ]
        });

        var gridVagoes = new Translogic.PaginatedGrid({
            id: 'gridVagoes',
            name: 'gridVagoes',
            autoLoadGrid: true,
            viewConfig: {
                forceFit: true,
                emptyText: 'Não possui dado(s) para exibição.'
            },
            height: 300,
            autoScroll: true,
            fields: [
                    'IdItem',
                    'NumeroVagao',
                    'ConcluidoDescricao',
                    'RetrabalhoDescricao',
                    'Observacao'
                ],
            url: '<%= Url.Action("ObterVagoesDaOs", "OSLimpezaVagao") %>',
            columns: [
				    new Ext.grid.RowNumberer(),
                    { header: 'Nº Vagão', dataIndex: "NumeroVagao", sortable: false, width: 130 },
                    { header: 'Serviço concluído', dataIndex: "ConcluidoDescricao", sortable: false, width: 100 },
                    { header: 'Teve retrabalho', dataIndex: "RetrabalhoDescricao", sortable: false, width: 100 },
                    { header: 'Observação', dataIndex: "Observacao", sortable: false, width: 100 }
                ]
        });

        gridVagoes.getStore().proxy.on('beforeload', function (p, params) {
            params['idOs'] = idOs;
        });


        //***************************** LAYOUT *****************************//

        var formtxtNumOS = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            width: 95,
            items: [txtNumOs]
        };

        var formtxtDataHora = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            width: 105,
            items: [txtDataHora]
        };

        var formddlFornecedor = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            width: 270,
            items: [ddlFornecedor]
        };

        var formtxtLocal = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            width: 42,
            items: [txtLocal]
        };

        var formtxtJustificativa = {
                    layout: 'form',
                    border: false,
                    bodyStyle: 'padding: 5px',
                    width: 900,
                    items: [txtJustificativa]
        };

        var formckMarcarTodos = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding-left: 5px',
            height: 40,
            width: 210,
            items: [ckMarcarTodos]
        };


        //Containers
        var containerLinha1 = {
            layout: 'column',
            border: true,
            items: [formtxtNumOS, formtxtDataHora, formddlFornecedor, formtxtLocal]
        };

        var containerLinha2 = {
            layout: 'column',
            border: true,
            items: [formckMarcarTodos]
        };

        var containerLinha3 = {
            layout: 'column',
            border: true,
            items: [gridVagoes]
        };

        var containerLinha4 = {
            layout: 'column',
            border: true,
            items: [formtxtJustificativa]
        };


        //***************************** FUNÇÕES*****************************//

        //Classes
        function OSLimpezaVagaoItemDto() {
            var IdItem;
        }
        function Vagao() {
            var OSLimpezaVagaoItemDto;
        }

       function ValidarEFecharOS() {
        // Progress BAR
        Ext.getBody().mask("Processando dados...", "x-mask-loading");
       
            var idOs = Ext.getCmp("txtNumOs").getValue();
            var justificativa = Ext.getCmp("txtJustificativa").getValue();

//            Ext.getBody().mask("Processando dados...", "x-mask-loading");

            var jsonRequest = $.toJSON({ idOs: idOs, justificativa: justificativa });
            //console.log(jsonRequest);

            $.ajax({
                url: '<%= Url.Action("CancelarOS", "OSLimpezaVagao") %>',
                type: "POST",
                dataType: 'json',
                data: jsonRequest,
                timeout: 300000,
                contentType: "application/json; charset=utf-8",
                failure: function (conn, data) {

                    Ext.Msg.alert("Mensagem de Erro", "Ocorreu um erro inesperado.");
                    Ext.getBody().unmask();
                },
                success: function (result) {
                    Ext.getBody().unmask();
                    if (result.Success) {
                        Ext.Msg.show({
                            title: 'Aviso',
                            msg: 'OS de Limpeza Cancelada com sucesso.',
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.WARNING,
                            fn: function (buttonValue, inputText, showConfig) {
                                var url = '<%= Url.Action("Index", "OSLimpezaVagao") %>';
                                window.location = url;
                            }
                        });
                    }
                    else {
                        Ext.Msg.show({
                            title: "Alerta",
                            msg: result.Message,
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.ERROR
                        });
                    }
                }
            });
        }

         function Validar() {
            var justificativa = Ext.getCmp("txtJustificativa").getValue();
          
            if (justificativa == "") {
                Ext.getBody().unmask();
                Ext.Msg.show({ title: 'Aviso', msg: 'O campo justificativa é obrigatório', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                return false;
            }
            return true;
        }

        function Voltar() {

            Ext.getBody().mask("Processando dados...", "x-mask-loading");
            var url = '<%= Url.Action("Index", "OSLimpezaVagao")%>';

            window.location = url;
        }

        //Evendo das checkboxes para marcar todos no grid
        function CheckGridColumns(arrChecked) {

            var marcarConcluidos = false;
            if (arrChecked.length >= 1) {

                if (arrChecked[0].boxLabel == "Como concluído") {
                    marcarConcluidos = true;
                }
            }

            if (marcarConcluidos) {
                SetAllCheckConcluido();
            } else { NotSetAllCheckConcluido(); }

        }

        function SetAllCheckConcluido() {

            $(".x-grid3-cc-checkConcluido").removeClass("x-grid3-check-col");
            $(".x-grid3-cc-checkConcluido").addClass("x-grid3-check-col-on");
            var itens = gridVagoes.getStore().getRange();
            for (var i = 0; i < itens.length; i++) {
                itens[i].data.Concluido = true;
            }
        }

        function NotSetAllCheckConcluido() {

            $(".x-grid3-cc-checkConcluido").removeClass("x-grid3-check-col-on");
            $(".x-grid3-cc-checkConcluido").addClass("x-grid3-check-col");
            var itens = gridVagoes.getStore().getRange();
            for (var i = 0; i < itens.length; i++) {
                itens[i].data.Concluido = false;
            }
        }

        //***************************** BOTOES *****************************//
        var btnCancelarOS = {
            name: 'btnCancelarOS',
            id: 'btnCancelarOS',
            text: 'Confirmar',
            iconCls: 'icon-tick',
            handler: ValidarEFecharOS
        };

        var btnVoltar = {
            name: 'btnVoltar',
            id: 'btnVoltar',
            text: 'Voltar',
            iconCls: 'icon-left',
            handler: Voltar
        };

        //***************************** PAINEL *****************************//
        var panelForm = new Ext.form.FormPanel({
            id: 'panelForm',
            layout: 'form',
            labelAlign: 'top',
            border: false,
            autoHeight: true,
            title: "Gerar Ordem de Serviço",
            region: 'center',
            bodyStyle: 'padding: 15px',
            items: [containerLinha1, containerLinha2, containerLinha3, containerLinha4],
            buttonAlign: "center",
            buttons: [btnCancelarOS, btnVoltar]
        });

        //***************************** RENDER *****************************//
        Ext.onReady(function () {
            panelForm.render("divContent");
        });

        fornecedorStore.on('load', function (store) {

            var range = ddlFornecedor.getStore().getRange();
            var existe = false;
            $.each(range, function (c, col) {

                //Verifica se registro que usuario está tentando inserir já existe
                if (col.data.IdFornecedorOs == idFornecedor) {
                    existe = true;
                }
            });

            if (existe)
                Ext.getCmp('ddlFornecedor').setValue(idFornecedor);
        });
        
    
    </script>
</asp:Content>
<asp:Content ID="MainContent" ContentPlaceHolderID="Content" runat="server">
    <div id="header-content">
        <h1>
            Manutenção</h1>
        <small>Você está em Manutenção > Gerar Ordem de Serviço</small>
        <br />
    </div>
    <div id="divContent">
    </div>
</asp:Content>