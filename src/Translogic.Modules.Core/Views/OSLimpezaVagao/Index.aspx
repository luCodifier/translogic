﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Interna.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content runat="server" ID="HeadContent" ContentPlaceHolderID="HeadContent">
    <script type="text/javascript">
 
        //#Region StoreDDLsbtncance

        var tipoStore = new Ext.data.JsonStore({
            root: "Items",
            autoLoad: true,
            proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("ObterTipos", "OSLimpezaVagao") %>', timeout: 600000 }),
            id: 'tipoStore',
            fields: ['IdTipo', 'DescricaoTipo']
        });

        var statusStore = new Ext.data.JsonStore({
            root: "Items",
            autoLoad: true,
            proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("ObterStatus", "OSLimpezaVagao") %>', timeout: 600000 }),
            id: 'statusStore',
            fields: ['IdStatus', 'DescricaoStatus']
        });

        //#endRegion

        var sm = new Ext.grid.CheckboxSelectionModel({
            singleSelect: true,
            header: ''
        });
        //#Region StoreGridMotivoVagoes

        var gridOSLimpezaVagaoStore = new Ext.data.JsonStore({
            root: "Items",
            totalProperty: 'Total',
            id: 'gridOSLimpezaVagaoStore',
            name: 'gridOSLimpezaVagaoStore',
            proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("ConsultaOSLimpeza", "OSLimpezaVagao") %>', timeout: 600000 }),
            paramNames: {
                sort: "detalhesPaginacaoWeb.Sort",
                dir: "detalhesPaginacaoWeb.Dir",
                start: "detalhesPaginacaoWeb.Start",
                limit: "detalhesPaginacaoWeb.Limit"
            },
            fields: ['Data', 'LocalServico', 'IdOs', 'NumOs', 'Tipo', 'Fornecedor', 'Status', 'UltimaAlteracao', 'Retrabalho', 'Usuario']
        });

        //#endRegion

        //#Region FuncoesDaTela
        function comparaDatas(dataIni, dataFim) {

            var arrDtIni = dataIni.split('/');
            var arrDtFim = dataFim.split('/');

            var dtIni = new Date(arrDtIni[2] + '/' + arrDtIni[1] + '/' + arrDtIni[0]);
            var dtFim = new Date(arrDtFim[2] + '/' + arrDtFim[1] + '/' + arrDtFim[0]);

            return dtIni <= dtFim;
        }

        
        function Pesquisar() {
            var dtI = Ext.getCmp("txtDataInicial").getRawValue();
            var dtF = Ext.getCmp("txtDataFinal").getRawValue();
            if ((dtI != "" && dtI != "__/__/____") && (dtF != "" && dtF != "__/__/____")) {

                if (comparaDatas(dtI, dtF)) {
                    limpaBotoes();
                    gridOSLimpezaVagaoStore.load({ params: { start: 0, limit: 50} });
                }
                else
                    Ext.Msg.show({ title: 'Aviso', msg: 'Data Inicial deve ser menor ou igual a Data Final!', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
            }
            else
                Ext.Msg.show({ title: 'Aviso', msg: 'Favor informar as Datas Inicial e Final!', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
        }

        function limpaBotoes() {
            Ext.getCmp("btnEditarOS").setDisabled(true);
            Ext.getCmp("btnFecharOS").setDisabled(true);
            Ext.getCmp("btnCancelarOS").setDisabled(true);
            Ext.getCmp("btnImprimir").setDisabled(true);
        }

        function Limpar() {
            gridOSLimpezaVagaoStore.removeAll();
            Ext.getCmp("grid-filtros").getForm().reset();

            $("#txtDataInicial").val("__/__/____");
            $("#txtDataFinal").val("__/__/____");
        }

        function ColocarHojeEmDatas() {

            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();

            if (dd < 10) {
                dd = '0' + dd
            }

            if (mm < 10) {
                mm = '0' + mm
            }

            today = dd + '/' + mm + '/' + yyyy;

            Ext.getCmp("txtDataInicial").setValue(today);
            Ext.getCmp("txtDataFinal").setValue(today);

        }


        function VerificaOSSelecionado() {
            var resultado = false;
            var selection = sm.getSelections();
            var iCont = 0;
            if (selection && selection.length > 0) {
                //                var IdOs = selection[0].data.IdOs;

                $.each(selection, function (i, e) {
                    //                    if (IdOs == e.data.IdOs) {
                    iCont++;
                    //}
                });



                // Verifica mensagem de erro para usuario
                if (iCont > 1) {
                    resultado = "maior";
                } else if (iCont == 1 && selection[0].data.Status == "Fechada") {
                    resultado = "Fechada";
                } else if (iCont == 1) {
                    resultado = "foi";
                } else {
                    resultado = "menor";
                }
            };
            return resultado;
        }


        // Busca e armazena lista de ids de vagões
        function BuscaIDOrdemServicoSelecionado() {
            var selection = sm.getSelections();
            var id;
            if (selection && selection.length > 0) {
                var IdOs = selection[0].data.IdOs;
                var listaOS = [];

                selection = sm.getSelections();

                $.each(selection, function (i, e) {
                    id = e.data.IdOs;
                });
            }

            return id;
        }

        //#endRegion

        //#Region PainelMotivosVagoes

        $(function () {

            // Toolbar da Grid
            var pagingToolbar = new Ext.PagingToolbar({
                pageSize: 50,
                store: gridOSLimpezaVagaoStore,
                displayInfo: true,
                displayMsg: App.Resources.Web.GridExibindoRegistros,
                emptyMsg: App.Resources.Web.GridSemRegistros,
                paramNames: {
                    start: "detalhesPaginacaoWeb.Start",
                    limit: "detalhesPaginacaoWeb.Limit"
                }
            });

            var cm = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true
                },
                columns: [
                    new Ext.grid.RowNumberer(),
                    sm,
                    { header: 'Data', dataIndex: "Data", sortable: false, width: 100 },
                    { header: 'Local', dataIndex: "LocalServico", sortable: false, width: 80 },
                    { header: 'OS', dataIndex: "NumOs", sortable: false, width: 60 },
                    { header: 'Tipo', dataIndex: "Tipo", sortable: false, width: 100 },
                    { header: 'Fornecedor', dataIndex: "Fornecedor", sortable: false, width: 250 },
                    { header: 'Status', dataIndex: "Status", sortable: false, width: 90 },
                    { header: 'Retrabalho', dataIndex: "Retrabalho", sortable: false, width: 50 },
                    { header: 'Última mudança de status', dataIndex: "UltimaAlteracao", sortable: false, width: 140 },
                    { header: 'Usuário', dataIndex: "Usuario", sortable: false, width: 100 }
                ]
            });

            var gridPainelOSLimpeza = new Ext.grid.EditorGridPanel({
                id: 'gridPainelOSLimpeza',
                name: 'gridPainelOSLimpeza',
                autoLoadGrid: true,
                height: 360,
                width: 870,
                stripeRows: true,
                cm: cm,
                region: 'center',
                viewConfig: {
                    forceFit: true,
                    emptyText: 'Não possui dado(s) para exibição.'
                },
                autoScroll: true,
                loadMask: { msg: App.Resources.Web.Carregando },
                store: gridOSLimpezaVagaoStore,
                tbar: [
                    {
                        id: 'btnIncluir',
                        text: 'Gerar OS',
                        tooltip: 'Gerar OS',
                        //                        disabled: true,
                        iconCls: 'icon-new',
                        handler: function (c) {

                            var url = '<%= this.Url.Action("PesquisarVagoes") %>';
                            window.location = url;
                        }
                    }
                ,
                    {
                        id: 'btnEditarOS',
                        text: 'Editar OS',
                        tooltip: 'Editar OS',
                        disabled: true,
                        iconCls: 'icon-edit',
                        handler: function (c) {

                            //Verifica se usuario selecionou OS para editar
                            if (VerificaOSSelecionado() != "Cancelada" || VerificaOSSelecionado() != "Fechada") {
                                //GIF Progress
                                Ext.getBody().mask("Processando dados...", "x-mask-loading");
                                // Busca idOS linha selecionada
                                var idOs = BuscaIDOrdemServicoSelecionado();
                                var url = '<%= Url.Action("EditarOsLimpeza", "OSLimpezaVagao") %>';

                                url += "?idOs=" + idOs;

                                window.location = url;

                            } else if (VerificaOSSelecionado() == "maior") {
                                Ext.Msg.show({ title: 'Aviso', msg: 'Você deve selecionar apenas uma OS para edição', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR })
                            }

                            if (VerificaOSSelecionado() == "menor") {
                                Ext.Msg.show({ title: 'Aviso', msg: 'Selecione uma OS para edição', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR })
                            }

                            if (VerificaOSSelecionado() == "Fechada") {
                                Ext.Msg.show({ title: 'Aviso', msg: 'Você não pode editar OS com status de Fechada', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR })
                            }

                            if (VerificaOSSelecionado() == "Cancelada") {
                                Ext.Msg.show({ title: 'Aviso', msg: 'Você não pode editar OS com status de Cancelada', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR })
                            }
                        }
                    },
                    {
                        id: 'btnFecharOS',
                        text: 'Fechar OS',
                        tooltip: 'Fechar OS',
                        disabled: true,
                        iconCls: 'icon-delete',
                        handler: function (c) {

                            //Verifica se usuario selecionou OS para editar
                            if (VerificaOSSelecionado() == "foi") {

                                Ext.getBody().mask("Processando dados...", "x-mask-loading");

                                // Busca idOS linha selecionada
                                var idOs = BuscaIDOrdemServicoSelecionado();
                                var url = '<%= Url.Action("FecharOSLimpeza", "OSLimpezaVagao") %>';

                                url += "?idOs=" + idOs;
                                window.location = url;

                            } else if (VerificaOSSelecionado() == "maior") {
                                Ext.Msg.show({ title: 'Aviso', msg: 'Você deve selecionar apenas uma OS para Fechar', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR })
                            }

                            if (VerificaOSSelecionado() == "menor") {
                                Ext.Msg.show({ title: 'Aviso', msg: 'Selecione uma OS para Fechar', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR })
                            }

                            if (VerificaOSSelecionado() == "Fechada") {
                                Ext.Msg.show({ title: 'Aviso', msg: 'Esta OS já está Fechada', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR })
                            }
                        }
                    },
                    {
                        id: 'btnCancelarOS',
                        text: 'Cancelar OS',
                        tooltip: 'Cancelar OS',
                        disabled: true,
                        iconCls: 'icon-cancel',
                        handler: function (c) {

                            //Verifica se usuario selecionou OS para editar
                            if (VerificaOSSelecionado() == "foi") {

                                Ext.getBody().mask("Processando dados...", "x-mask-loading");

                                // Busca idOS linha selecionada
                                var idOs = BuscaIDOrdemServicoSelecionado();
                                var url = '<%= Url.Action("CancelarOSLimpeza", "OSLimpezaVagao") %>';
                                url += "?idOs=" + idOs;
                                window.location = url;

                            } else if (VerificaOSSelecionado() == "maior") {
                                Ext.Msg.show({ title: 'Aviso', msg: 'Você deve selecionar apenas uma OS para Fechar', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR })
                            }

                            if (VerificaOSSelecionado() == "menor") {
                                Ext.Msg.show({ title: 'Aviso', msg: 'Selecione uma OS para Fechar', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR })
                            }

                            if (VerificaOSSelecionado() == "Fechada") {
                                Ext.Msg.show({ title: 'Aviso', msg: 'Esta OS já está Fechada', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR })
                            }
                        }
                    },
                    {
                        id: 'btnImprimir',
                        text: 'Imprimir OS',
                        tooltip: 'Imprimir OS',
                        disabled: true,
                        iconCls: 'icon-printer',
                        handler: function (c) {

                            //Verifica se usuario selecionou OS para editar
                            if (VerificaOSSelecionado() == "foi" || VerificaOSSelecionado() == "Fechada" || VerificaOSSelecionado() == "Cancelada") {

                                // Busca idOS linha selecionada
                                var idOs = BuscaIDOrdemServicoSelecionado();
                                var url = '<%= Url.Action("Imprimir", "OSLimpezaVagao") %>';
                                url += "?idOs=" + idOs;
                                window.open(url, "");

                            } else if (VerificaOSSelecionado() == "maior") {
                                Ext.Msg.show({ title: 'Aviso', msg: 'Você deve selecionar apenas uma OS para Fechar', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR })
                            } else if (VerificaOSSelecionado() == "menor") {
                                Ext.Msg.show({ title: 'Aviso', msg: 'Selecione uma OS para Fechar', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR })
                            }
                        }
                    }
                ],
                bbar: pagingToolbar,
                sm: sm,
                listeners: {

                    cellclick: function (e, rowIndex, columnIndex) {

                        var record = gridPainelOSLimpeza.getStore().getAt(rowIndex);
                        var status = record.get('Status');
                        var selection = sm.getSelections();
                        var selecionado = selection.length > 0 ? true : false;

                        // Desabilita botões
                        limpaBotoes();

                        if (selecionado == true) {
                            switch (status) {
                                case ("Aberta"):
                                    Ext.getCmp("btnEditarOS").setDisabled(false);
                                    Ext.getCmp("btnFecharOS").setDisabled(false);
                                    Ext.getCmp("btnCancelarOS").setDisabled(false);
                                    Ext.getCmp("btnImprimir").setDisabled(false);
                                    break;
                                case ("Parcial"):
                                    Ext.getCmp("btnEditarOS").setDisabled(false);
                                    Ext.getCmp("btnFecharOS").setDisabled(false);
                                    Ext.getCmp("btnCancelarOS").setDisabled(false);
                                    Ext.getCmp("btnImprimir").setDisabled(false);
                                    break;
                                case ("Fechada"):
                                    Ext.getCmp("btnImprimir").setDisabled(false);
                                    break;
                                case ("Cancelada"):
                                    Ext.getCmp("btnImprimir").setDisabled(false);
                                    break;
                                default:
                            }
                        }
                        else {
                            limpaBotoes();
                        }
                    }

                }
            });

            gridPainelOSLimpeza.getStore().proxy.on('beforeload', function (p, params) {

                var dataInicial = Ext.getCmp("txtDataInicial").getRawValue();
                var dataFinal = Ext.getCmp("txtDataFinal").getRawValue();
                var txtLocal = Ext.getCmp("txtLocal").getValue();
                var txtNumOs = Ext.getCmp("txtNumOs").getValue();

                ddlTipo = Ext.getCmp("ddlTipo");
                var idTipo = (ddlTipo.getValue() == "Todos" || ddlTipo.getValue() == "0") ? "" : ddlTipo.getValue();

                ddlStatus = Ext.getCmp("ddlStatus");
                var idStatus = (ddlStatus.getValue() == "Todos" || ddlStatus.getValue() == "0") ? "" : ddlStatus.getValue();

                ddlRetrabalho = Ext.getCmp("ddlRetrabalho");
                var retrabalho = (ddlRetrabalho.getValue() == "Todos" || ddlRetrabalho.getValue() == "0") ? "" : ddlRetrabalho.getValue();


                params['dataInicial'] = dataInicial;
                params['dataFinal'] = dataFinal;
                params['local'] = txtLocal;
                params['numOs'] = txtNumOs;
                params['idTipo'] = idTipo;
                params['idStatus'] = idStatus;
                params['retrabalho'] = retrabalho;

            });


            //#endRegion

            //#Region PesquisaFiltros
            var txtDataInicial = new Ext.form.DateField({
                fieldLabel: 'Data Inicial',
                id: 'txtDataInicial',
                name: 'txtDataInicial',
                dateFormat: 'd/n/Y',
                width: 83,
                plugins: [new Ext.ux.InputTextMask('99/99/9999', true)]

            });

            var txtDataFinal = new Ext.form.DateField({
                fieldLabel: 'Data Final',
                id: 'txtDataFinal',
                name: 'txtDataFinal',
                dateFormat: 'd/n/Y',
                width: 83,
                plugins: [new Ext.ux.InputTextMask('99/99/9999', true)]

            });

            var txtLocal = {
                xtype: 'textfield',
                name: 'txtLocal',
                id: 'txtLocal',
                fieldLabel: 'Local',
                autoCreate: {
                    tag: 'input',
                    type: 'text',
                    autocomplete: 'off',
                    maxlength: '3'
                },
                maskRe: /[A-Za-z]/,
                style: 'text-transform:uppercase;',
                width: 85
            };


            var txtNumOs = {
                xtype: 'numberfield',
                name: 'txtNumOs',
                id: 'txtNumOs',
                fieldLabel: 'OS',
                allowDecimals: false,
                decimalSeparator: ',',
                autoCreate: {
                    tag: 'input',
                    type: 'text',
                    autocomplete: 'off',
                    maxlength: '12'
                },
                width: 85,
                enableKeyEvents: true,
                listeners: {
                    keyup: function (field, e) {
                        field.setValue(field.getRawValue().replace("'", ""));
                    }
                }

            };

            var ddlTipo = new Ext.form.ComboBox({
                editable: true,
                typeAhead: true,
                forceSelection: true,
                disableKeyFilter: true,
                triggerAction: 'all',
                lazyRender: true,
                mode: 'local',
                valueField: 'IdTipo',
                displayField: 'DescricaoTipo',
                fieldLabel: 'Tipo',
                id: 'ddlTipo',
                name: 'ddlTipo',
                width: 200,
                store: tipoStore,
                value: 'Todos'
            });


            var ddlStatus = new Ext.form.ComboBox({
                editable: true,
                typeAhead: true,
                forceSelection: true,
                disableKeyFilter: true,
                triggerAction: 'all',
                lazyRender: true,
                mode: 'local',
                store: statusStore,
                valueField: 'IdStatus',
                displayField: 'DescricaoStatus',
                fieldLabel: 'Status',
                id: 'ddlStatus',
                width: 100,
                value: '99'
            });


            var ddlRetrabalho = new Ext.form.ComboBox({
                editable: true,
                typeAhead: true,
                forceSelection: true,
                disableKeyFilter: true,
                triggerAction: 'all',
                lazyRender: true,
                mode: 'local',
                valueField: 'IdRetrabalho',
                displayField: 'Retrabalho',
                fieldLabel: 'Retrabalho',
                id: 'ddlRetrabalho',
                width: 85,
                store: [
                            'Todos',
                            'Sim',
                            'Não'
                        ],
                value: 'Todos'
            });

            //Item do Array de Componentes (1 por cada componente criado)
            var arrDataInicial = {
                width: 100,
                layout: 'form',
                border: false,
                items: [txtDataInicial]
            };

            var arrDataFinal = {
                width: 100,
                layout: 'form',
                border: false,
                items: [txtDataFinal]
            };

            var arrLocal = {
                width: 100,
                layout: 'form',
                border: false,
                items: [txtLocal]
            };

            var arrNumOs = {
                width: 100,
                layout: 'form',
                border: false,
                items: [txtNumOs]
            };

            var arrTipo = {
                width: 210,
                layout: 'form',
                border: false,
                items: [ddlTipo]
            };

            var arrStatus = {
                width: 110,
                layout: 'form',
                border: false,
                items: [ddlStatus]
            };

            var arrRetrabalho = {
                width: 100,
                layout: 'form',
                border: false,
                items: [ddlRetrabalho]
            };

            //Array de componentes do form 
            var arrCampos = {
                layout: 'column',
                border: false,
                items: [arrDataInicial, arrDataFinal, arrLocal, arrNumOs, arrTipo, arrStatus, arrRetrabalho]
            };


            //Conteudo do Form que sera criado
            var filtros = new Ext.form.FormPanel({
                id: 'grid-filtros',
                title: "Filtros",
                region: 'center',
                bodyStyle: 'padding:15px;',
                layout: 'form',
                labelAlign: 'top',
                items: [arrCampos],
                buttonAlign: "center",
                buttons:
                    [{
                        text: 'Pesquisar',
                        type: 'submit',
                        iconCls: 'icon-find',
                        handler: function (b, e) {
                            Pesquisar();
                        }
                    },
                        {
                            text: 'Limpar',
                            type: 'submit',
                            handler: function (b, e) {
                                Limpar();
                            }
                        }
                    ]
            });

            //#endRegion

            new Ext.Viewport({
                layout: 'border',
                margins: 10,
                items: [
                    {
                        region: 'north',
                        height: 200,
                        autoScroll: true,
                        items: [{
                            region: 'center',
                            applyTo: 'header-content'
                        },
                            filtros]
                    },
                    gridPainelOSLimpeza
                ]
            });
        });

        Ext.onReady(function () {
            $("#txtDataInicial").val("__/__/____");
            $("#txtDataFinal").val("__/__/____");

            ColocarHojeEmDatas();

            statusStore.on('load', function (store) {
                Ext.getCmp('ddlStatus').setValue("99");
            });

            limpaBotoes();
            Pesquisar();
        });

    </script>
</asp:Content>
<asp:Content runat="server" ID="Content2" ContentPlaceHolderID="Content">
    <div id="header-content">
        <h1>
            Ordem de serviço de Limpeza de Vagão</h1>
        <small>Você está em Operação > Ordem de Serviço de Limpeza de Vagão</small>
        <br />
    </div>
     
</asp:Content>
