﻿<%@ Page Title="" Language="C#" Inherits="System.Web.Mvc.ViewPage" MasterPageFile="~/Views/Shared/Interna.master" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        //***************************** VARIÁVEIS E INICIALIZAÇÃO**************//
        var idOs = '<%=ViewData["idOs"] %>';
        var dataHora = '<%=ViewData["data"] %>';
        var local = '<%=ViewData["local"] %>';
        var idFornecedor = '<%=ViewData["idFornecedor"] %>';

        $(function () {

            //Setar campos recuperados da OS
            var txtNumOs = Ext.getCmp("txtNumOs");
            txtNumOs.value = idOs;

            var txtDataHora = Ext.getCmp("txtDataHora");
            txtDataHora.value = dataHora;

            var txtLocal = Ext.getCmp("txtLocal");
            txtLocal.value = local; 
        });

        //***************************** CAMPOS *****************************//
        var txtNumOs = {
            xtype: 'textfield',
            name: 'txtNumOs',
            id: 'txtNumOs',
            fieldLabel: 'Número OS',
            disabled: true,
            autoCreate: {
                tag: 'input',
                type: 'text',
                autocomplete: 'off',
                maxlength: '12',
                readonly: true
            },
            maskRe: /[0-9+;]/,
            width: 85
        };

        var txtDataHora = {
            xtype: 'textfield',
            name: 'txtDataHora',
            disabled: true,
            id: 'txtDataHora',
            fieldLabel: 'Data e hora',
            autoCreate: {
                tag: 'input',
                type: 'text',
                autocomplete: 'off',
                maxlength: '12',
                readonly: true
            },
            style: 'text-transform:uppercase;',
            width: 95
        };

        var txtLocal = {
            xtype: 'textfield',
            name: 'txtLocal',
            id: 'txtLocal',
            fieldLabel: 'Local',
            disabled: true,
            autoCreate: {
                tag: 'input',
                type: 'text',
                autocomplete: 'off',
                maxlength: '3',
                readonly: true
            },
            style: 'text-transform:uppercase;',
            width: 32
        };

        var fornecedorStore = new Ext.data.JsonStore({
            root: "Items",
            autoLoad: true,
            proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("ObterFornecedores", "OSLimpezaVagao") %>', timeout: 600000 }),
            id: 'fornecedorStore',
            fields: ['IdFornecedorOs', 'Nome'],
            baseParams: { local: local }
        });

        var ddlFornecedor = new Ext.form.ComboBox({
            editable: false,
            typeAhead: true,
            forceSelection: true,
            disableKeyFilter: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            store: fornecedorStore,
            valueField: 'IdFornecedorOs',
            displayField: 'Nome',
            fieldLabel: 'Fornecedor',
            id: 'ddlFornecedor',
            width: 260,
            alowBlank: false
        });
        
        var rbtTipoServico = {
            xtype: 'radiogroup',
            fieldLabel: 'Tipo do serviço',
            id: 'rbtTipoServico',
            width: 150,
            name: 'rbtTipoServico',
            style: 'margin-top:5px;',
            readonly: true,
            items: [
		            { boxLabel: 'Limpeza', name: 'tipoServico', inputValue: 1, disabled: true },
		            { boxLabel: 'Lavagem', name: 'tipoServico', inputValue: 2, disabled: true }
		           ]
        };

        //***************************** GRID VAGÕES**********************//

        var gridVagoes = new Translogic.PaginatedGrid({
            id: 'gridVagoes',
            name: 'gridVagoes',
            autoLoadGrid: true,
            viewConfig: {
                forceFit: true,
                emptyText: 'Não possui dado(s) para exibição.'
            },
            height: 300,
            autoScroll: true,
            fields: [
                    'IdItem',
                    'NumeroVagao',
                    'ConcluidoDescricao',
                    'RetrabalhoDescricao',
                    'Observacao'
                ],
            url: '<%= Url.Action("ObterVagoesDaOs", "OSLimpezaVagao") %>',
            columns: [
				    new Ext.grid.RowNumberer(),
                    { header: 'Nº Vagão', dataIndex: "NumeroVagao", sortable: false, width: 130 },
                    { header: 'Serviço concluído', dataIndex: "ConcluidoDescricao", sortable: false, width: 100 },
                    { header: 'Teve retrabalho', dataIndex: "RetrabalhoDescricao", sortable: false, width: 100 },
                    { header: 'Observação', dataIndex: "Observacao", sortable: false, width: 100 }
                ]
        });

        gridVagoes.getStore().proxy.on('beforeload', function (p, params) {
            params['idOs'] = idOs;
        });

        //***************************** LAYOUT *****************************//

        var fieldNumOS = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            items: [txtNumOs]
        };

        var fieldDataHora = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            items: [txtDataHora]
        };

        var fieldLocal = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            items: [txtLocal]
        };

        var fieldFornecedor = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            items: [ddlFornecedor]
        };

        var fieldTipoServico = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            items: [rbtTipoServico]
        };

        var containerLinha1 = {
            layout: 'column',
            border: true,
            items: [fieldNumOS, fieldDataHora, fieldLocal, fieldFornecedor, fieldTipoServico]
        };

        var containerLinha2 = {
            layout: 'column',
            border: true,
            items: [gridVagoes]
        };

        //Classes
        function OSLimpezaVagaoItemDto() {
            var IdItem;
        }
        
        function Vagao() {
            var OSLimpezaVagaoItemDto;
        }

        function EditarOs() {

            var idFornecedor = Ext.getCmp("ddlFornecedor").getValue();
            if (idFornecedor == "Todos" || idFornecedor == "0") {
                Ext.Msg.show({ title: 'Aviso', msg: 'O campo fornecedor é obrigatório!', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                return;
            }

            var LocalServico = Ext.getCmp("txtLocal").getValue();
            if (LocalServico == "") {
                Ext.Msg.show({ title: 'Aviso', msg: 'O campo local é obrigatório!', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                return;
            }

            Ext.getBody().mask("Processando dados...", "x-mask-loading");


            //Parâmetros para método SAVE
            var jsonRequest = $.toJSON({ idOs: idOs, idFornecedor: idFornecedor, local: LocalServico });
            //console.log(jsonRequest);

            $.ajax({
                url: '<%= Url.Action("EditarOs", "OSLimpezaVagao") %>',
                type: "POST",
                dataType: 'json',
                data: jsonRequest,
                timeout: 300000,
                contentType: "application/json; charset=utf-8",
                failure: function (conn, data) {
                    Ext.Msg.alert("Mensagem de Erro", "Ocorreu um erro inesperado");
                    Ext.getBody().unmask();
                },
                success: function () {
                    Ext.getBody().unmask();
                    Ext.Msg.show({ title: 'Aviso', msg: 'OS de Limpeza/Lavagem Editada com sucesso.', buttons: Ext.Msg.OK, icon: Ext.MessageBox.WARNING, fn: function (btn, text) {
                        if (btn == 'ok') {

                            var url = '<%= Url.Action("Index", "OSLimpezaVagao") %>';
                            window.location = url;
                        }
                    }
                });
            }

        });

    }

        function Voltar() {
            Ext.getBody().mask("Processando dados...", "x-mask-loading");
            var url = '<%= Url.Action("Index", "OSLimpezaVagao")%>';

            window.location = url;
        }

        //***************************** BOTOES *****************************//
        var btnSalvarOS = {
            name: 'btnSalvarOS',
            id: 'btnSalvarOS',
            text: 'Salvar',
            iconCls: 'icon-save',
            handler: EditarOs
        };

        var btnVoltar = {
            name: 'btnVoltar',
            id: 'btnVoltar',
            text: 'Voltar',
            iconCls: 'icon-left',
            handler: Voltar
        };

        //***************************** PAINEL *****************************//

        var panelForm = new Ext.form.FormPanel({
            id: 'panelForm',
            layout: 'form',
            labelAlign: 'top',
            border: false,
            autoHeight: true,
            title: "Edição de OS de Limpeza",
            region: 'center',
            bodyStyle: 'padding: 15px',
            items: [containerLinha1, containerLinha2],
            buttonAlign: "center",
            buttons: [btnSalvarOS, btnVoltar]
        });

        //***************************** RENDER *****************************//
        Ext.onReady(function () {
            panelForm.render("divContent");

            //Desabilitar tipo de serviço
            $("#rbtTipoServico input[type=radio]").attr("disabled", "disabled");

            fornecedorStore.on('load', function (store) {

                var range = ddlFornecedor.getStore().getRange();
                var existe = false;
                $.each(range, function (c, col) {

                    //Verifica se registro que usuario está tentando inserir já existe
                    if (col.data.IdFornecedorOs == idFornecedor) {
                        existe = true;
                    }
                });

                if (existe)
                    Ext.getCmp('ddlFornecedor').setValue(idFornecedor);
            });
        });
    </script>
</asp:Content>
<asp:Content ID="MainContent" ContentPlaceHolderID="Content" runat="server">
    <div id="header-content">
        <h1>
            Manutenção</h1>
        <small>Você está em Manutenção > Editar Ordem de Serviço</small>
        <br />
    </div>
    <div id="divContent">
    </div>
</asp:Content>
