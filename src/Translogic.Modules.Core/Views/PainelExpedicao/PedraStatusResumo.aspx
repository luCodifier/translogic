﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Interna.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content runat="server" ID="HeadContent" ContentPlaceHolderID="HeadContent">
    <div id="header-content">
        <h1>Painel Expedição Pedra</h1>
        <br />
    </div>
    <script type="text/javascript" language="javascript">

        var resumoAnaliticoStore = new Ext.data.JsonStore({
            root: "SumarioAnalitico",
            autoLoad: false,
            id: 'resumoAnaliticoStore',
            fields: [
                'Id',
                'DataPedra',
                'Operacao',
                'RegiaoDescricao',
                'EstacaoDescricao',
                'OrigemDescricao',
                'TerminalDescricao',
                'SegmentoDescricao',
                'PlanejadoSade',
                'Realizado',
                'Saldo',
                'Porcentagem',
                'Status'
            ]
        });

        var resumoRegioesStore = new Ext.data.JsonStore({
            root: "SinteticoRegioes",
            autoLoad: false,
            id: 'resumoRegioesStore',
            fields: ['Id', 'DataPedra', 'Regiao', 'Planejado', 'Realizado', 'Saldo', 'Porcentagem']
        });

        var totalRegioesStore = new Ext.data.JsonStore({
            root: "SumarioRegioes",
            autoLoad: false,
            id: 'totalRegioesStore',
            fields: ['Id', 'DataPedra', 'Regiao', 'Planejado', 'Realizado', 'Saldo', 'Porcentagem']
        });

        var resumoOrigensStore = new Ext.data.JsonStore({
            root: "SinteticoOrigens",
            autoLoad: false,
            id: 'resumoOrigensStore',
            fields: ['Id', 'DataPedra', 'Origem', 'Planejado', 'Realizado', 'Saldo', 'Porcentagem']
        });

        var totalOrigensStore = new Ext.data.JsonStore({
            root: "SumarioOrigens",
            autoLoad: false,
            id: 'totalOrigensStore',
            fields: ['Id', 'Origem', 'Planejado', 'Realizado', 'Saldo', 'Porcentagem']
        });

        var regiaoStore = new Ext.data.JsonStore({
            root: "Items",
            autoLoad: true,
            proxy: new Ext.data.HttpProxy({
                url: '<%= Url.Action("ObterRegioesDDL", "PainelExpedicao") %>',
                timeout: 600000
            }),
            id: 'regiaoStore',
            fields: [
                'Id',
                'Nome',
                'OperacaoId'
            ]
        });

        var estacaoFaturamentoStore = new Ext.data.JsonStore({
            root: "Items",
            autoLoad: true,
            proxy: new Ext.data.HttpProxy({
                url: '<%= Url.Action("ObterEstacoesFaturamento", "PainelExpedicao") %>',
                timeout: 600000
            }),
            id: 'estacaoFaturamentoStore',
            fields: [
                'Id',
                'Nome'
            ]
        });

        var segmentosStore = new Ext.data.JsonStore({
            root: "Items",
            autoLoad: true,
            proxy: new Ext.data.HttpProxy({
                url: '<%= Url.Action("ObterSegmentos", "PainelExpedicao") %>',
                timeout: 600000
            }),
            id: 'segmentosStore',
            fields: [
                'Id',
                'Nome'
            ]
        });


        var ufsStore = new Ext.data.ArrayStore({
            id: 0,
            fields: [
                'Id',
                'Nome'
            ],
            data: [
                [0, 'TODOS'],
                [1, 'MG'],
                [2, 'MT'],
                [3, 'MS'],
                [4, 'PR'],
                [5, 'RJ'],
                [6, 'RS'],
                [7, 'SC'],
                [8, 'SP']
            ]
        });

        var operacaoRegiaoStore = new Ext.data.JsonStore({
            root: "Items",
            autoLoad: true,
            proxy: new Ext.data.HttpProxy({
                url: '<%= Url.Action("ObterOperacoesMalha", "PainelExpedicao") %>',
                timeout: 600000
            }),
            id: 'operacaoRegiaoStore',
            fields: [
                'Id',
                'Nome'
            ]
        });

        ////////////////////////////////////////////////////////////////////////////////////////////////////

        function Pesquisar() {

            Ext.Ajax.request({
                url: '<%= Url.Action("ObterPainelExpedicaoPedra", "PainelExpedicao") %>',
                params: {
                    operacao: buscaDDLComponentValue("ddlMalhaCentral"),
                    regiao: buscaDDLComponentValue("ddlRegiao"),
                    dataPedra: buscaDateComponentValue("txtDataInicio"),
                    uf: buscaDDLComponentValue("ddlUf"),
                    origem: buscaDDLComponentValue('txtOrigem'),
                    estacaoFaturamento: buscaDDLComponentValue("ddlEstacaoFaturamento"),
                    segmento: buscaDDLComponentValue("ddlSegmento"),
                    ocultarPedraZerada: buscaDDLComponentValue("chkPedraVazia"),
                    telaExcelEmail: 'tela'
                },
                method: "POST",
                success: function (result) {

                    var myData = Ext.util.JSON.decode(result.responseText);



                    if (myData.Contagem > 0) {
                        if (myData.PermissaoExportar) {
                            Ext.getCmp('ExportExcelGeral').show();
                            Ext.getCmp('ExportEmailGeral').show();

                            Ext.getCmp('ExportExcelGridDetalhePedra').show();
                            Ext.getCmp('ExportEmailGridDetalhePedra').show();

                            Ext.getCmp('ExportExcelGridSubtotaisRegioes').show();
                            Ext.getCmp('ExportEmailGridSubtotaisRegioes').show();

                            Ext.getCmp('ExportExcelGridSubtotaisOrigens').show();
                            Ext.getCmp('ExportEmailGridSubtotaisOrigens').show();
                        }

                        Ext.getCmp('txtUltimaAtualizacao').setValue(myData.UltimaAtualizacao);
                        Ext.getCmp('txtProximaAtualizacao').setValue(myData.ProximaAtualizacao);

                        gridDetalhePedra.getStore().loadData(myData);
                        gridResumoAnalitico.getStore().loadData(myData);
                        gridSubtotaisRegioes.getStore().loadData(myData);
                        gridTotaisRegioes.getStore().loadData(myData);
                        gridSubtotaisOrigens.getStore().loadData(myData);
                        gridTotaisOrigens.getStore().loadData(myData);
                    }
                    else {
                        Ext.getCmp('txtUltimaAtualizacao').setValue(myData.UltimaAtualizacao);
                        Ext.getCmp('txtProximaAtualizacao').setValue(myData.ProximaAtualizacao);

                        gridDetalhePedra.getStore().removeAll();
                        gridResumoAnalitico.getStore().removeAll();
                        gridSubtotaisRegioes.getStore().removeAll();
                        gridTotaisRegioes.getStore().removeAll();
                        gridSubtotaisOrigens.getStore().removeAll();
                        gridTotaisOrigens.getStore().removeAll();

                        Ext.Msg.show(
                            {
                                title: "",
                                msg: "A busca não retornou resultados",
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.ERROR
                            });
                    }
                },
                failure: function (resp, opt) {
                    Ext.Msg.show(
                        {
                            title: "",
                            msg: "Erro:" + resp.Mensagem,
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.ERROR
                        });

                    e.reject();
                }

            });
        }

        function Email(gridName) {
            var url = "";

            if (gridName === 'regiao') {
                url = "<%= Url.Action("EmailPedraRegiao", "PainelExpedicao") %>";
            } else if (gridName === 'origem') {
                url = "<%= Url.Action("EmailPedraOrigem", "PainelExpedicao") %>";
            } else if (gridName === 'geral') {
                url = "<%= Url.Action("EmailPedraGeral", "PainelExpedicao") %>";
            } else {
                url = "<%= Url.Action("EmailPedraDetalhe", "PainelExpedicao") %>";
            }


            Ext.Ajax.request({
                url: url,
                params: {
                    operacao: buscaDDLComponentValue("ddlMalhaCentral"),
                    regiao: buscaDDLComponentValue("ddlRegiao"),
                    dataPedra: buscaDateComponentValue("txtDataInicio"),
                    uf: buscaDDLComponentValue("ddlUf"),
                    origem: buscaDDLComponentValue('txtOrigem'),
                    estacaoFaturamento: buscaDDLComponentValue("ddlEstacaoFaturamento"),
                    segmento: buscaDDLComponentValue("ddlSegmento"),
                    ocultarPedraZerada: buscaDDLComponentValue("chkPedraVazia"),
                    telaExcelEmail: 'excel'
                },
                method: "POST",
                success: function (result) {
                    Ext.Msg.show(
                        {
                            title: "",
                            msg: "Email enviado com sucesso!",
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.INFO
                        });
                }
            });

        }

        function Excel(gridName) {

            var operacao = buscaDDLComponentValue("ddlMalhaCentral");
            var regiao = buscaDDLComponentValue("ddlRegiao");
            var dataPedra = buscaDateComponentValue("txtDataInicio")
            var uf = buscaDDLComponentValue("ddlUf")
            var origem = buscaDDLComponentValue('txtOrigem')
            var estacaoFaturamento = buscaDDLComponentValue("ddlEstacaoFaturamento")
            var segmento = buscaDDLComponentValue("ddlSegmento")
            var ocultarPedraZerada = buscaDDLComponentValue("chkPedraVazia")
            var url = "";

            if (gridName === 'regiao') {
                url = "<%= Url.Action("ExportarExcelPedraRegiao", "PainelExpedicao") %>";
            } else if (gridName === 'origem') {
                url = "<%= Url.Action("ExportarExcelPedraOrigem", "PainelExpedicao") %>";
            } else if (gridName === 'geral') {
                url = "<%= Url.Action("ExportarExcelPedraGeral", "PainelExpedicao") %>";
            } else {
                url = "<%= Url.Action("ExportarExcelPedraDetalhe", "PainelExpedicao") %>";
            }

            url += String.format("?operacao={0}", operacao);
            url += String.format("&regiao={0}", regiao);
            url += String.format("&dataPedra={0}", dataPedra);
            url += String.format("&uf={0}", uf);
            url += String.format("&origem={0}", origem);
            url += String.format("&estacaoFaturamento={0}", estacaoFaturamento);
            url += String.format("&segmento={0}", segmento);
            url += String.format("&ocultarPedraZerada={0}", ocultarPedraZerada);

            window.open(url, "");

        }


        ////////////////////////////////////////////////////////////////////////////////////////////////////

        var chkPedraVazia = {
            xtype: 'checkbox',
            fieldLabel: 'Ocultar Zerados',
            id: 'chkPedraVazia',
            name: 'chkPedraVazia',
            width: 120,
            checked: true
        };

        var arrPedraVazia = {
            width: 130,
            layout: 'form',
            border: false,
            bodyStyle: 'padding:0px 5px 0px 5px',
            items: [chkPedraVazia]
        };

        var txtDataInicio = {
            xtype: 'datefield',
            fieldLabel: 'Data',
            id: 'txtDataInicio',
            name: 'txtDataInicio',
            width: 83,
            allowBlank: false,
            value: new Date()
        };

        var arrDataIni = {
            width: 93,
            layout: 'form',
            border: false,
            bodyStyle: 'padding:0px 5px 0px 5px',
            items: [txtDataInicio]
        };

        var ddlUf = new Ext.form.ComboBox({
            typeAhead: false,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            store: ufsStore,
            valueField: 'Nome',
            displayField: 'Nome',
            fieldLabel: 'UF',
            id: 'ddlUf',
            width: 70,
            value: 'TODOS'
        });

        var arrUf = {
            width: 80,
            layout: 'form',
            border: false,
            bodyStyle: 'padding:0px 5px 0px 5px',
            items: [ddlUf]
        };

        var txtOrigem = {
            xtype: 'textfield',
            name: 'txtOrigem',
            id: 'txtOrigem',
            fieldLabel: 'Origem',
            autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '3' },
            maskRe: /[a-zA-Z]/,
            style: 'text-transform: uppercase',
            width: 50
        };

        var txtUltimaAtualizacao = {
            xtype: 'textfield',
            name: 'txtUltimaAtualizacao',
            id: 'txtUltimaAtualizacao',
            fieldLabel: 'Última Atualização',
            disabled: true,
            value: '00:00',
            width: 150
        };

        var txtProximaAtualizacao = {
            xtype: 'textfield',
            name: 'txtProximaAtualizacao',
            id: 'txtProximaAtualizacao',
            fieldLabel: 'Próxima Atualização',
            disabled: true,
            value: '00:00',
            width: 150
        };

        var arrOrigem = {
            width: 60,
            layout: 'form',
            border: false,
            bodyStyle: 'padding:0px 5px 0px 5px',
            items: [txtOrigem]
        };

        var arrUltimaAtualizacao = {
            width: 160,
            layout: 'form',
            border: false,
            bodyStyle: 'padding:0px 5px 0px 5px',
            items: [txtUltimaAtualizacao]
        };

        var arrProximaAtualizacao = {
            width: 160,
            layout: 'form',
            border: false,
            bodyStyle: 'padding:0px 5px 0px 5px',
            items: [txtProximaAtualizacao]
        };


        var ddlEstacaoFaturamento = new Ext.form.ComboBox({
            typeAhead: false,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            store: estacaoFaturamentoStore,
            valueField: 'Id',
            displayField: 'Nome',
            fieldLabel: 'Estação Faturamento',
            id: 'ddlEstacaoFaturamento',
            width: 160,
            value: 'TODAS'
        });

        var arrEstacaoFaturamento = {
            width: 170,
            layout: 'form',
            border: false,
            bodyStyle: 'padding:0px 5px 0px 5px',
            items: [ddlEstacaoFaturamento]
        };

        var ddlSegmento = new Ext.form.ComboBox({
            typeAhead: false,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            store: segmentosStore,
            valueField: 'Id',
            displayField: 'Nome',
            fieldLabel: 'Segmento',
            id: 'ddlSegmento',
            width: 120,
            value: 'TODOS'
        });

        var arrSegmento = {
            width: 130,
            layout: 'form',
            border: false,
            bodyStyle: 'padding:0px 5px 0px 5px',
            items: [ddlSegmento]
        };

        var ddlRegiao = new Ext.form.ComboBox({
            id: 'ddlRegiao',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            store: regiaoStore,
            valueField: 'Id',
            displayField: 'Nome',
            value: 0,
            fieldLabel: 'Região',
            width: 130,
            value: 'TODAS'
        });

        var arrRegiao = {
            width: 140,
            layout: 'form',
            border: false,
            bodyStyle: 'padding:0px 5px 0px 5px',
            items: [ddlRegiao]
        };

        var ddlMalhaCentral = new Ext.form.ComboBox({
            id: 'ddlMalhaCentral',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            store: operacaoRegiaoStore,
            valueField: 'Id',
            displayField: 'Nome',
            value: 'TODAS',
            fieldLabel: 'Operação',
            width: 100,
            listeners: {
                select: {
                    fn: function () {

                        var selectedValue = this.getValue();

                        if (selectedValue > 0) {

                            regiaoStore.load({
                                params: { operacaoId: selectedValue },
                                callback: function (options, success, response, records) {
                                    if (success) {
                                        var comp = Ext.getCmp('ddlRegiao').setValue('TODAS');
                                    }
                                }
                            });
                        }
                        else {
                            var comp = Ext.getCmp('ddlRegiao');
                            comp.setValue('TODAS');
                            comp.getStore().load();
                        }

                    }
                }
            }
        });

        var arrMalhaCentral = {
            width: 110,
            layout: 'form',
            border: false,
            bodyStyle: 'padding:0px 5px 0px 5px',
            items: [ddlMalhaCentral]
        };

        var arrlinha1 = {
            layout: 'column',
            border: false,
            items: [
                arrMalhaCentral,
                arrRegiao,
                arrDataIni,
                arrUf,
                arrEstacaoFaturamento,
                arrOrigem,
                arrSegmento,
                arrPedraVazia
            ]
        };

        var arrlinha2 = {
            layout: 'column',
            border: false,
            items: [
                arrUltimaAtualizacao, arrProximaAtualizacao
            ]
        };

        var fm = Ext.form;

        var columnModelDetalhe = new Ext.grid.ColumnModel({
            // specify any defaults for each column
            defaults: {
                sortable: true
            },
            columns: [
                { dataIndex: "PedraRealizadoId", hidden: true },
                { dataIndex: "PedraEditadaFlag", hidden: true },
                { header: 'Operação', dataIndex: 'Operacao', width: 60, align: 'center' },
                { header: 'Data', dataIndex: 'DataPedra', width: 80, align: 'center' },
                { header: 'Região', dataIndex: 'RegiaoDescricao', width: 140, align: 'center' },
                { header: 'Estação', dataIndex: 'EstacaoDescricao', width: 140, align: 'center' },
                { header: 'Origem', dataIndex: 'OrigemDescricao', width: 50, align: 'center' },
                { header: 'Terminal', dataIndex: 'TerminalDescricao', width: 150, align: 'center' },
                { header: 'Segmento', dataIndex: 'SegmentoDescricao', width: 110, align: 'center' },
                {
                    header: 'Plan. Sade', dataIndex: 'PlanejadoSade', align: 'center', width: 60, //renderer: 'usMoney',
                    editor: new fm.NumberField({
                        allowBlank: false,
                        allowNegative: false
                    }),
                    renderer: function (value, metaData, record, rowIndex, colIndex, store) {
                        if (record.data.PedraEditadaFlag) {
                            metaData.attr = 'style="color:blue;font-weight: bold;"'
                        }
                        return value;
                    }
                },
                { header: 'Realizado', dataIndex: 'Realizado', width: 60, align: 'center' },
                {
                    header: 'Saldo', dataIndex: 'Saldo', width: 50, align: 'center',
                    renderer: function (value, metaData, record, rowIndex, colIndex, store) {
                        if (value < 0) {
                            metaData.attr = 'style="color:red;font-weight: bold;"'
                        }
                        return value;
                    }
                },
                {
                    header: '%', dataIndex: 'Porcentagem', width: 40, align: 'center',
                    renderer: function (v) {
                        return v + '%';
                    }
                },
                {
                    id: 'status', header: 'Status', dataIndex: 'Status', width: 300, align: 'left',
                    editor: new fm.TextField({
                        allowBlank: true,
                        style: 'text-transform: uppercase'
                    }),
                    renderer: function (value, metaData, record, rowIndex, colIndex, store) {
                        metaData.attr = 'style="text-transform: uppercase;"';
                        return value;
                    }

                }
            ]
        });

        var columnModelResumoAnalitico = new Ext.grid.ColumnModel({
            defaults: {
                sortable: false,
                menuDisabled: true
            },
            columns: [
                { dataIndex: 'Operacao', width: 60, align: 'center' },
                { dataIndex: 'Data', width: 80, align: 'center' },
                { dataIndex: 'RegiaoDescricao', width: 140, align: 'center' },
                { dataIndex: 'EstacaoDescricao', width: 140, align: 'center' },
                { dataIndex: 'OrigemDescricao', width: 50, align: 'center' },
                { dataIndex: 'TerminalDescricao', width: 150, align: 'center' },
                {
                    dataIndex: 'SegmentoDescricao', width: 110, align: 'center',
                    renderer: function (value, metaData, record, rowIndex, colIndex, store) {
                        metaData.attr = 'style="font-weight: bold;"'
                        return value;
                    }
                },
                {
                    header: 'Total </br>Plan. Sade', dataIndex: 'PlanejadoSade', align: 'center', width: 60,
                    renderer: function (value, metaData, record, rowIndex, colIndex, store) {
                        if (value === 'true' || value === 'TRUE') {
                            metaData.attr = 'style="color:red;font-weight: bold;"'
                        } else {
                            metaData.attr = 'style="font-weight: bold;"'
                        }

                        return value;
                    }
                },
                {
                    header: 'Total </br>Realizado', dataIndex: 'Realizado', width: 60, align: 'center',
                    renderer: function (value, metaData, record, rowIndex, colIndex, store) {
                        metaData.attr = 'style="font-weight: bold;"'
                        return value;
                    }
                },
                {
                    header: 'Total </br>Saldo', dataIndex: 'Saldo', width: 50, align: 'center',
                    renderer: function (value, metaData, record, rowIndex, colIndex, store) {
                        if (value < 0) {
                            metaData.attr = 'style="color:red;font-weight: bold;"'
                        }
                        else {
                            metaData.attr = 'style="font-weight: bold;"'
                        }
                        return value;
                    }
                },
                {
                    header: 'Total </br>%', dataIndex: 'Porcentagem', width: 40, align: 'center',
                    renderer: function (value, metaData, record, rowIndex, colIndex, store) {
                        metaData.attr = 'style="font-weight: bold;"'
                        return value;
                    }
                },
                {
                    id: 'status', header: '', dataIndex: 'Status', width: 300, align: 'center',
                    renderer: function (value, metaData, record, rowIndex, colIndex, store) {
                        metaData.attr = 'style="font-weight: bold;"'
                        return value;
                    }
                }
            ]
        });


        var columnModelSubtotaisRegioes = new Ext.grid.ColumnModel({
            defaults: {
                sortable: false
            },
            columns: [
                { header: 'Região', dataIndex: 'Regiao', width: 150, align: 'center' },
                { header: 'Planejado', dataIndex: 'Planejado', align: 'center', width: 60 },
                { header: 'Realizado', dataIndex: 'Realizado', width: 60, align: 'center' },
                {
                    header: 'Saldo', dataIndex: 'Saldo', width: 50, align: 'center',
                    renderer: function (value, metaData, record, rowIndex, colIndex, store) {
                        if (value < 0) {
                            metaData.attr = 'style="color:red;font-weight: bold;"'
                        }
                        return value;
                    }
                },
                { header: '% Realizado', dataIndex: 'Porcentagem', width: 150, align: 'center' }
            ]
        });

        var columnModelSubtotaisOrigens = new Ext.grid.ColumnModel({
            defaults: {
                sortable: false
            },
            columns: [
                { header: 'Origem', dataIndex: 'Origem', width: 150, align: 'center' },
                { header: 'Planejado', dataIndex: 'Planejado', align: 'center', width: 60 },
                { header: 'Realizado', dataIndex: 'Realizado', width: 60, align: 'center' },
                {
                    header: 'Saldo', dataIndex: 'Saldo', width: 50, align: 'center',
                    renderer: function (value, metaData, record, rowIndex, colIndex, store) {
                        if (value < 0) {
                            metaData.attr = 'style="color:red;font-weight: bold;"'
                        }
                        return value;
                    }
                },
                { header: '% Realizado', dataIndex: 'Porcentagem', width: 150, align: 'center' }
            ]
        });

        var columnModelTotalRegioes = new Ext.grid.ColumnModel({
            defaults: {
                sortable: false,
                menuDisabled: true
            },
            columns: [
                {
                    dataIndex: 'Regiao', width: 150, align: 'center',
                    renderer: function (value, metaData, record, rowIndex, colIndex, store) {
                        metaData.attr = 'style="font-weight: bold;"'
                        return value;
                    }
                },
                {
                    header: 'Total </br>Planejado', dataIndex: 'Planejado', align: 'center', width: 60,
                    renderer: function (value, metaData, record, rowIndex, colIndex, store) {
                        metaData.attr = 'style="font-weight: bold;"'
                        return value;
                    }
                },
                {
                    header: 'Total </br>Realizado', dataIndex: 'Realizado', width: 60, align: 'center',
                    renderer: function (value, metaData, record, rowIndex, colIndex, store) {
                        metaData.attr = 'style="font-weight: bold;"'
                        return value;
                    }
                },
                {
                    header: 'Total </br>Saldo', dataIndex: 'Saldo', width: 50, align: 'center',
                    renderer: function (value, metaData, record, rowIndex, colIndex, store) {
                        if (value < 0) {
                            metaData.attr = 'style="color:red;font-weight: bold;"'
                        }
                        else {
                            metaData.attr = 'style="font-weight: bold;"'
                        }
                        return value;
                    }
                },
                {
                    header: 'Total Realizado</br> %', dataIndex: 'Porcentagem', width: 150, align: 'center',
                    renderer: function (value, metaData, record, rowIndex, colIndex, store) {
                        metaData.attr = 'style="font-weight: bold;"'
                        return value;
                    }
                }
            ]
        });


        var columnModelTotalOrigens = new Ext.grid.ColumnModel({
            defaults: {
                sortable: false,
                menuDisabled: true
            },
            columns: [
                {
                    dataIndex: 'Origem', width: 150, align: 'center',
                    renderer: function (value, metaData, record, rowIndex, colIndex, store) {
                        metaData.attr = 'style="font-weight: bold;"'
                        return value;
                    }
                },
                {
                    header: '<div><span>Total</span></br><span>Planejado</span></div>', dataIndex: 'Planejado', align: 'center', width: 60,
                    renderer: function (value, metaData, record, rowIndex, colIndex, store) {
                        metaData.attr = 'style="font-weight: bold;"'
                        return value;
                    }
                },
                {
                    header: 'Total </br> Realizado', dataIndex: 'Realizado', width: 60, align: 'center',
                    renderer: function (value, metaData, record, rowIndex, colIndex, store) {
                        metaData.attr = 'style="font-weight: bold;"'
                        return value;
                    }
                },
                {
                    header: 'Total </br>Saldo', dataIndex: 'Saldo', width: 50, align: 'center',
                    renderer: function (value, metaData, record, rowIndex, colIndex, store) {
                        if (value < 0) {
                            metaData.attr = 'style="color:red;font-weight: bold;"'
                        }
                        else {
                            metaData.attr = 'style="font-weight: bold;"'
                        }
                        return value;
                    }
                },
                {
                    header: 'Total Realizado </br>%', dataIndex: 'Porcentagem', width: 150, align: 'center',
                    renderer: function (value, metaData, record, rowIndex, colIndex, store) {
                        metaData.attr = 'style="font-weight: bold;"'
                        return value;
                    }
                }
            ]
        });



        ////////////////////////////////////////////////////////////////////////////////////////////////////

        function buscaDDLComponentValue(componentName) {
            var comp = Ext.getCmp(componentName);
            var value = '';

            if (comp) {
                var value = comp.getValue();
            }

            return value
        }

        function buscaDateComponentValue(componentName) {
            var comp = Ext.getCmp(componentName);
            var value = '';
            if (comp) {
                var value = comp.getValue().format('d/m/Y');
            }

            return value
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////


        var pedraDetalheStore = new Ext.data.JsonStore({
            root: "Analitico",
            autoLoad: false,
            id: 'pedraDetalheStore',
            fields: [
                'PedraRealizadoId',
                'Operacao',
                'DataPedra',
                'EstacaoDescricao',
                'Origem',
                'OrigemDescricao',
                'TerminalDescricao',
                'Regiao',
                'RegiaoDescricao',
                'Segmento',
                'SegmentoDescricao',
                'PlanejadoSade',
                'Realizado',
                'Saldo',
                'Porcentagem',
                'Status',
                'PedraEditadaFlag',
                'PermiteEditarPedra'
            ]
        });


        gridDetalhePedra = new Ext.grid.EditorGridPanel({
            id: 'grid-detalhe-pedra',
            collapsible: true,
            collapsed: true,
            store: pedraDetalheStore,
            cm: columnModelDetalhe,
            clicksToEdit: 2,
            stripeRows: true,
            region: 'center',
            autoHeight: true,
            title: 'Detalhes do Planejado X Realizado',
            iconCls: 'icon-grid',
            autoExpandColumn: 'status',
            tbar: {
                items: [
                    {
                        hidden: true,
                        id: 'ExportExcelGridDetalhePedra',
                        text: 'Exportar para Excel',
                        iconCls: 'icon-page-excel',
                        handler: function () {
                            Excel('detalhe');
                        }
                    },
                    {
                        hidden: true,
                        id: 'ExportEmailGridDetalhePedra',
                        text: 'Enviar por email',
                        iconCls: 'icon-email_go',
                        handler: function () {
                            Email('detalhe');
                        }
                    }
                ]
            },
            listeners: {
                afteredit: function (e) {

                    url = "<%= Url.Action("GravarAlteracoesPedra", "PainelExpedicao") %>";
                    Ext.Ajax.request({
                        url: url,
                        params: {
                            acao: 'alterar',
                            id: e.record.data.id,
                            campo: e.field,
                            valor: e.value,
                            PedraRealizadoId: e.record.data.PedraRealizadoId,
                            DataPedra: e.record.data.DataPedra,
                            Operacao: e.record.data.Operacao,
                            Estacao: e.record.data.Origem,
                            TerminalDescricao: e.record.data.TerminalDescricao,
                            Segmento: e.record.data.Segmento,
                            Realizado: e.record.data.Realizado,
                            PlanejadoSade: e.record.data.PlanejadoSade,
                            Status: e.record.data.Status

                        },
                        method: "POST",
                        success: function (result) {
                            var resposta = Ext.util.JSON.decode(result.responseText);
                            if (resposta.Success) {
                                // atualiza o saldo no grid
                                e.record.data.Saldo = e.record.data.Realizado - e.record.data.PlanejadoSade;

                                Ext.Msg.show(
                                    {
                                        title: "Sucesso",
                                        msg: "Alteração realizada com sucesso!",
                                        buttons: Ext.Msg.OK,
                                        icon: Ext.MessageBox.INFO
                                    });
                                e.record.commit();
                            } else {
                                Ext.Msg.show(
                                    {
                                        title: "Erro ao Atualizar",
                                        msg: resposta.Message,
                                        buttons: Ext.Msg.OK,
                                        icon: Ext.MessageBox.INFO
                                    });
                                e.record.reject();

                            }
                        },
                        failure: function (resp, opt) {
                            e.record.reject();
                        }

                    });
                }
            }

        });

        gridResumoAnalitico = new Ext.grid.GridPanel({
            id: 'grid-resumo-analitico',
            collapsible: false,
            store: resumoAnaliticoStore,
            cm: columnModelResumoAnalitico,
            stripeRows: false,
            region: 'center',
            autoHeight: true,
            frame: false,
            //            title: 'Totais Detalhe',
            //            iconCls: 'icon-grid',
            autoExpandColumn: 'status'
        });

        //------------------------------------------------
        gridSubtotaisRegioes = new Ext.grid.GridPanel({
            id: 'grid-subtotais-regiao',
            collapsible: true,
            store: resumoRegioesStore,
            split: true,
            cm: columnModelSubtotaisRegioes,
            stripeRows: false,
            region: 'center',
            autoHeight: true,
            title: 'Subtotal Regiões',
            tbar: {
                items: [
                    {
                        hidden: true,
                        id: 'ExportExcelGridSubtotaisRegioes',
                        text: 'Exportar para Excel',
                        iconCls: 'icon-page-excel',
                        handler: function () {
                            Excel('regiao');
                        }
                    },
                    {
                        hidden: true,
                        id: 'ExportEmailGridSubtotaisRegioes',
                        text: 'Enviar por email',
                        iconCls: 'icon-email_go',
                        handler: function () {
                            Email('regiao');
                        }
                    }
                ]
            }
        });

        gridTotaisRegioes = new Ext.grid.GridPanel({
            id: 'grid-totais-regiao',
            collapsible: false,
            store: totalRegioesStore,
            cm: columnModelTotalRegioes,
            stripeRows: false,
            region: 'center',
            autoHeight: true,
            //            title: 'Total Regiões',
            frame: false,
            iconCls: 'icon-grid'
        });

        gridSubtotaisOrigens = new Ext.grid.GridPanel({
            id: 'grid-subtotais-origem',
            collapsible: true,
            collapsed: true,
            split: true,
            store: resumoOrigensStore,
            cm: columnModelSubtotaisOrigens,
            stripeRows: false,
            region: 'center',
            autoHeight: true,
            title: 'Subtotal Origens',
            iconCls: 'icon-grid',
            tbar: {
                items: [
                    {
                        hidden: true,
                        id: 'ExportExcelGridSubtotaisOrigens',
                        text: 'Exportar para Excel',
                        iconCls: 'icon-page-excel',
                        handler: function () {
                            Excel('origem');
                        }
                    },
                    {
                        hidden: true,
                        id: 'ExportEmailGridSubtotaisOrigens',
                        text: 'Enviar por email',
                        iconCls: 'icon-email_go',
                        handler: function () {
                            Email('origem');
                        }
                    }
                ]
            }
        });

        gridTotaisOrigens = new Ext.grid.GridPanel({
            id: 'grid-totais-origem',
            collapsible: false,
            store: totalOrigensStore,
            cm: columnModelTotalOrigens,
            stripeRows: false,
            split: true,
            region: 'center',
            autoHeight: true,
            frame: false,
            iconCls: 'icon-grid',
            footer: true,
            listeners: {
                beforeedit: function (grid) {
                },
                afteredit: function (e) {
                }
            }
        });

        var dropDownListColunas = {
            layout: 'column',
            border: false,
            items: [arrlinha1]
        };

        var timeSpanFields = {
            layout: 'column',
            border: false,
            items: [arrlinha2]
        };

        var fieldSetFilters = {
            xtype: 'fieldset',
            autoHeight: true,
            bodyStyle: 'padding: 5px',
            title: 'Filtros',
            items: [dropDownListColunas, timeSpanFields],
            buttonAlign: "center",
            buttons:
                [
                    {
                        text: 'Pesquisar',
                        type: 'submit',
                        iconCls: 'icon-find',
                        handler: function (b, e) {
                            Pesquisar();
                        }
                    },
                    {
                        hidden: true,
                        id: 'ExportExcelGeral',
                        text: 'Excel',
                        type: 'button',
                        iconCls: 'icon-page-excel',
                        handler: function (b, e) {
                            Excel('geral');
                        }
                    },
                    {
                        hidden: true,
                        id: 'ExportEmailGeral',
                        text: 'Enviar por email',
                        type: 'button',
                        iconCls: 'icon-email_go',
                        handler: function (b, e) {
                            Email('geral');
                        }
                    },
                    {
                        text: 'Limpar',
                        type: 'button',
                        handler: function (b, e) {
                            Ext.getCmp("panel-geral").getForm().reset();

                            gridDetalhePedra.getStore().removeAll();
                            gridResumoAnalitico.getStore().removeAll();
                            gridSubtotaisRegioes.getStore().removeAll();
                            gridTotaisRegioes.getStore().removeAll();
                            gridSubtotaisOrigens.getStore().removeAll();
                            gridTotaisOrigens.getStore().removeAll();

                        }
                    }
                ]
        };

        var fieldSetGrid = {
            xtype: 'fieldset',
            autoHeight: true,
            bodyStyle: 'padding: 5px',
            title: 'Resultado - Detalhe',
            items: [gridDetalhePedra, gridResumoAnalitico]
        };

        var fieldSetResumoRegiao = {
            xtype: 'fieldset',
            autoHeight: true,
            bodyStyle: 'padding: 5px',
            title: 'Resultado - Resumo Regiões',
            items: [gridSubtotaisRegioes, gridTotaisRegioes]
        };

        var fieldSetResumoOrigens = {
            xtype: 'fieldset',
            autoHeight: true,
            bodyStyle: 'padding: 5px',
            title: 'Resultado - Resumo Origens',
            items: [gridSubtotaisOrigens, gridTotaisOrigens]
        };

        $(function () {


            var panelGeral = new Ext.form.FormPanel({
                id: 'panel-geral',
                layout: 'form',
                labelAlign: 'top',
                border: false,
                autoHeight: true,
                title: "Planejado X Realizado",
                region: 'center',
                bodyStyle: 'padding: 5px',
                items: [fieldSetFilters, fieldSetGrid, fieldSetResumoRegiao, fieldSetResumoOrigens],
                buttonAlign: "center",
                split: true
            }).render(document.body);

        });
    </script>
</asp:Content>
