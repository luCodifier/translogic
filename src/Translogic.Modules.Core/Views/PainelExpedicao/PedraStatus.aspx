﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Interna.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content runat="server" ID="HeadContent" ContentPlaceHolderID="HeadContent">
    <div id="header-content">
        <h1>Painel Expedição Pedra</h1>
        <br />
    </div>
    <script type="text/javascript" language="javascript">

        var resumoAnaliticoStore = new Ext.data.JsonStore({
            root: "SumarioAnalitico",
            autoLoad: false,
            id: 'resumoAnaliticoStore',
            fields: [
                'Id',
                'DataPedra',
                'Operacao',
                'RegiaoDescricao',
                'EstacaoDescricao',
                'OrigemDescricao',
                'TerminalDescricao',
                'SegmentoDescricao',
                'PlanejadoSade',
                'Realizado',
                'Saldo',
                'Porcentagem',
                'Status'
            ]
        });

        var regiaoStore = new Ext.data.JsonStore({
            root: "Items",
            autoLoad: true,
            proxy: new Ext.data.HttpProxy({
                url: '<%= Url.Action("ObterRegioesDDL", "PainelExpedicao") %>',
                timeout: 600000
            }),
            id: 'regiaoStore',
            fields: ['Id', 'Nome']
        });

        var terminalStore = new Ext.data.JsonStore({
            root: "Items",
            autoLoad: true,
            proxy: new Ext.data.HttpProxy({
                url: '<%= Url.Action("ObterTerminaisDDL", "PainelExpedicao") %>',
                timeout: 600000
            }),
            id: 'terminalStore',
            fields: [
                'Id',
                'Nome'
            ]
        });

        var estacaoFaturamentoStore = new Ext.data.JsonStore({
            root: "Items",
            autoLoad: true,
            proxy: new Ext.data.HttpProxy({
                url: '<%= Url.Action("ObterEstacoesFaturamento", "PainelExpedicao") %>',
                timeout: 600000
            }),
            id: 'estacaoFaturamentoStore',
            fields: [
                'Id',
                'Nome'
            ]
        });

        var segmentosStore = new Ext.data.JsonStore({
            root: "Items",
            autoLoad: true,
            proxy: new Ext.data.HttpProxy({
                url: '<%= Url.Action("ObterSegmentos", "PainelExpedicao") %>',
                timeout: 600000
            }),
            id: 'segmentosStore',
            fields: [
                'Id',
                'Nome'
            ]
        });

        var ufsStore = new Ext.data.ArrayStore({
           id: 0,
           fields: [
               'Id',
               'Nome'
           ],
           data: [
               [0, 'TODOS'],
               [1, 'MG'],
               [2, 'MT'],
               [3, 'MS'],
               [4, 'PR'],
               [5, 'RJ'],
               [6, 'RS'],
               [7, 'SC'],
               [8, 'SP']
           ]
        });

        var operacaoRegiaoStore = new Ext.data.JsonStore({
            root: "Items",
            autoLoad: true,
            proxy: new Ext.data.HttpProxy({
                url: '<%= Url.Action("ObterOperacoesMalha", "PainelExpedicao") %>',
                timeout: 600000
            }),
            id: 'operacaoRegiaoStore',
            fields: [
                'Id',
                'Nome'
            ]
        });

        ////////////////////////////////////////////////////////////////////////////////////////////////////

        function Pesquisar() {

            Ext.Ajax.request({
                url: '<%= Url.Action("ObterPainelExpedicaoPedra", "PainelExpedicao") %>',
                params: {
                    operacao: buscaDDLComponentValue("ddlMalhaCentral"),
                    regiao: buscaDDLComponentValue("ddlRegiao"),
                    dataPedra: buscaDateComponentValue("txtDataInicio"),
                    uf: buscaDDLComponentValue("ddlUf"),
                    origem: buscaDDLComponentValue('txtOrigem'),
                    estacaoFaturamento: buscaDDLComponentValue("ddlEstacaoFaturamento"),
                    segmento: buscaDDLComponentValue("ddlSegmento"),
                    terminal: buscaDDLComponentValue("ddlTerminal"),
                    ocultarPedraZerada: buscaDDLComponentValue("chkPedraVazia"),
                    telaExcelEmail: 'tela'
                },
                method: "POST",
                success: function (result) {
                    var myData = Ext.util.JSON.decode(result.responseText);

                    grid.getStore().loadData(myData);
                    //gridResumoAnalitico.getStore().loadData(myData);
                },
                failure: function (resp, opt) {
                    Ext.Msg.show(
                        {
                            title: "",
                            msg: "Erro:" + resp.Mensagem,
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.ERROR
                        });

                    e.reject();
                }

            });
        }

        function Email(gridName) {
            var url = "";

            if (gridName === 'regiao') {
                url = '<%= Url.Action("EmailPedraRegiao", "PainelExpedicao") %>';
            } else if (gridName === 'origem') {
                url = '<%= Url.Action("EmailPedraOrigem", "PainelExpedicao") %>';
            } else if (gridName === 'geral') {
                url = '<%= Url.Action("EmailPedraGeral", "PainelExpedicao") %>';
            } else {
                url = '<%= Url.Action("EmailPedraDetalhe", "PainelExpedicao") %>';
            }


            Ext.Ajax.request({
                url: url,
                params: {
                    operacao: buscaDDLComponentValue("ddlMalhaCentral"),
                    regiao: buscaDDLComponentValue("ddlRegiao"),
                    dataPedra: buscaDateComponentValue("txtDataInicio"),
                    uf: buscaDDLComponentValue("ddlUf"),
                    origem: buscaDDLComponentValue('txtOrigem'),
                    estacaoFaturamento: buscaDDLComponentValue("ddlEstacaoFaturamento"),
                    segmento: buscaDDLComponentValue("ddlSegmento"),
                    terminal: buscaDDLComponentValue("ddlTerminal"),
                    ocultarPedraZerada: buscaDDLComponentValue("chkPedraVazia"),
                    telaExcelEmail: 'excel'
                },
                method: "POST",
                success: function (result) {
                    Ext.Msg.show(
                        {
                            title: "",
                            msg: "Email enviado com sucesso!",
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.INFO
                        });
                }
            });

        }

        function Excel(gridName) {

            var operacao = buscaDDLComponentValue("ddlMalhaCentral");
            var regiao = buscaDDLComponentValue("ddlRegiao");
            var dataPedra = buscaDateComponentValue("txtDataInicio")
            var uf = buscaDDLComponentValue("ddlUf")
            var origem = buscaDDLComponentValue('txtOrigem')
            var estacaoFaturamento = buscaDDLComponentValue("ddlEstacaoFaturamento")
            var segmento = buscaDDLComponentValue("ddlSegmento")
            var terminal = buscaDDLComponentValue("ddlTerminal")
            var ocultarPedraZerada = buscaDDLComponentValue("chkPedraVazia")
            var url = "";

            if (gridName === 'regiao') {
                url = '<%= Url.Action("ExportarExcelPedraRegiao", "PainelExpedicao") %>';
            } else if (gridName === 'origem') {
                url = '<%= Url.Action("ExportarExcelPedraOrigem", "PainelExpedicao") %>';
            } else if (gridName === 'geral') {
                url = '<%= Url.Action("ExportarExcelPedraGeral", "PainelExpedicao") %>';
            } else {
                url = '<%= Url.Action("ExportarExcelPedraDetalhe", "PainelExpedicao") %>';
            }

            url += String.format("?operacao={0}", operacao);
            url += String.format("&regiao={0}", regiao);
            url += String.format("&dataPedra={0}", dataPedra);
            url += String.format("&uf={0}", uf);
            url += String.format("&origem={0}", origem);
            url += String.format("&estacaoFaturamento={0}", estacaoFaturamento);
            url += String.format("&segmento={0}", segmento);
            url += String.format("&terminal={0}", terminal);
            url += String.format("&ocultarPedraZerada={0}", ocultarPedraZerada);

            window.open(url, "");

        }


        ////////////////////////////////////////////////////////////////////////////////////////////////////

        var chkPedraVazia = {
            xtype: 'checkbox',
            fieldLabel: 'Ocultar Zerados',
            id: 'chkPedraVazia',
            name: 'chkPedraVazia',
            width: 120
        };

        var arrPedraVazia = {
            width: 130,
            layout: 'form',
            border: false,
            bodyStyle: 'padding:0px 5px 0px 5px',
            items: [chkPedraVazia]
        };

        var txtDataInicio = {
            xtype: 'datefield',
            fieldLabel: 'Data Início',
            id: 'txtDataInicio',
            name: 'txtDataInicio',
            width: 83,
            allowBlank: false,
            value: new Date()
        };

        var arrDataIni = {
            width: 93,
            layout: 'form',
            border: false,
            bodyStyle: 'padding:0px 5px 0px 5px',
            items: [txtDataInicio]
        };

        var ddlUf = new Ext.form.ComboBox({
            typeAhead: false,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            store: ufsStore,
            valueField: 'Nome',
            displayField: 'Nome',
            fieldLabel: 'UF',
            id: 'ddlUf',
            width: 70,
            value: 'TODOS'
        });

        var arrUf = {
            width: 80,
            layout: 'form',
            border: false,
            bodyStyle: 'padding:0px 5px 0px 5px',
            items: [ddlUf]
        };

        var txtOrigem = {
            xtype: 'textfield',
            name: 'txtOrigem',
            id: 'txtOrigem',
            fieldLabel: 'Origem',
            autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '3' },
            maskRe: /[a-zA-Z]/,
            style: 'text-transform: uppercase',
            width: 50
        };

        var arrOrigem = {
            width: 60,
            layout: 'form',
            border: false,
            bodyStyle: 'padding:0px 5px 0px 5px',
            items: [txtOrigem]
        };

        var ddlEstacaoFaturamento = new Ext.form.ComboBox({
            typeAhead: false,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            store: estacaoFaturamentoStore,
            valueField: 'Id',
            displayField: 'Nome',
            fieldLabel: 'Estação Faturamento',
            id: 'ddlEstacaoFaturamento',
            width: 160,
            value: 'TODOS'
        });

        var arrEstacaoFaturamento = {
            width: 170,
            layout: 'form',
            border: false,
            bodyStyle: 'padding:0px 5px 0px 5px',
            items: [ddlEstacaoFaturamento]
        };

        var ddlSegmento = new Ext.form.ComboBox({
            typeAhead: false,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            store: segmentosStore,
            valueField: 'Id',
            displayField: 'Nome',
            fieldLabel: 'Segmento',
            id: 'ddlSegmento',
            width: 120,
            value: 'TODOS'
        });

        var arrSegmento = {
            width: 130,
            layout: 'form',
            border: false,
            bodyStyle: 'padding:0px 5px 0px 5px',
            items: [ddlSegmento]
        };

        var ddlRegiao = new Ext.form.ComboBox({
            id: 'ddlRegiao',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            store: regiaoStore,
            valueField: 'Id',
            displayField: 'Nome',
            value: 0,
            fieldLabel: 'Região',
            width: 130,
            value: 'TODOS'
        });

        var arrRegiao = {
            width: 140,
            layout: 'form',
            border: false,
            bodyStyle: 'padding:0px 5px 0px 5px',
            items: [ddlRegiao]
        };

        var ddlTerminal = new Ext.form.ComboBox({
            id: 'ddlTerminal',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            store: terminalStore,
            valueField: 'Id',
            displayField: 'Nome',
            value: 'TODOS',
            fieldLabel: 'Terminal',
            width: 190
        });

        var arrTerminal = {
            width: 200,
            layout: 'form',
            border: false,
            bodyStyle: 'padding:0px 5px 0px 5px',
            items: [ddlTerminal]
        };

        var ddlMalhaCentral = new Ext.form.ComboBox({
            id: 'ddlMalhaCentral',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            store: operacaoRegiaoStore,
            valueField: 'Id',
            displayField: 'Nome',
            value: 'TODOS',
            fieldLabel: 'Operação',
            width: 100
        });

        var arrMalhaCentral = {
            width: 110,
            layout: 'form',
            border: false,
            bodyStyle: 'padding:0px 5px 0px 5px',
            items: [ddlMalhaCentral]
        };

        var arrlinha1 = {
            layout: 'column',
            border: false,
            items: [
                arrMalhaCentral,
                arrRegiao,
                arrDataIni,
                arrUf,
                arrEstacaoFaturamento,
                arrOrigem,
                arrTerminal,
                arrSegmento,
                arrPedraVazia
            ]
        };

        var fm = Ext.form;

        var columnModelDetalhe = new Ext.grid.ColumnModel({
            // specify any defaults for each column
            defaults: {
                sortable: true
            },
            columns: [
                { dataIndex: "PedraRealizadoId", hidden: true },
                { dataIndex: "PedraEditadaFlag", hidden: true },
                { dataIndex: "PermiteEditarPedra", hidden: true },
                { header: 'Operação', dataIndex: 'Operacao', width: 60, align: 'center' },
                { header: 'Data', dataIndex: 'DataPedra', width: 80, align: 'center' },
                { header: 'Região', dataIndex: 'RegiaoDescricao', width: 140, align: 'center' },
                { header: 'Estação', dataIndex: 'EstacaoDescricao', width: 140, align: 'center' },
                { header: 'Origem', dataIndex: 'OrigemDescricao', width: 50, align: 'center' },
                { header: 'Terminal', dataIndex: 'TerminalDescricao', width: 150, align: 'center' },
                { header: 'Segmento', dataIndex: 'SegmentoDescricao', width: 110, align: 'center' },
                { header: 'Plan. Sade', dataIndex: 'PlanejadoSade', align: 'center', width: 60 },
                { header: 'Realizado', dataIndex: 'Realizado', width: 60, align: 'center' },
                { header: 'Saldo', dataIndex: 'Saldo', width: 50, align: 'center',
                    renderer: function (value, metaData, record, rowIndex, colIndex, store) {
                        if (value < 0) {
                            metaData.attr = 'style="color:red;font-weight: bold;"'
                        }
                        return value;
                    }
                },
                {
                    header: '%', dataIndex: 'Porcentagem', width: 40, align: 'center',
                    renderer: function (v) {
                        return v + '%';
                    }
                },
                {
                    id: 'status', header: 'Status', dataIndex: 'Status', width: 300, align: 'left' }
            ]
        });

        var columnModelResumoAnalitico = new Ext.grid.ColumnModel({
            defaults: {
                sortable: false,
                menuDisabled: true
            },
            columns: [
                { dataIndex: 'Operacao', width: 60, align: 'center' },
                { dataIndex: 'Data', width: 80, align: 'center' },
                { dataIndex: 'RegiaoDescricao', width: 140, align: 'center' },
                { dataIndex: 'EstacaoDescricao', width: 140, align: 'center' },
                { dataIndex: 'OrigemDescricao', width: 50, align: 'center' },
                { dataIndex: 'TerminalDescricao', width: 150, align: 'center' },
                {
                    dataIndex: 'SegmentoDescricao', width: 110, align: 'center',
                    renderer: function (value, metaData, record, rowIndex, colIndex, store) {
                        metaData.attr = 'style="font-weight: bold;"'
                        return value;
                    }
                },
                {
                    header: 'Total </br>Plan. Sade', dataIndex: 'PlanejadoSade', align: 'center', width: 60,
                    renderer: function (value, metaData, record, rowIndex, colIndex, store) {
                        if (value === 'true' || value === 'TRUE') {
                            metaData.attr = 'style="color:red;font-weight: bold;"'
                        } else {
                            metaData.attr = 'style="font-weight: bold;"'
                        }

                        return value;
                    }
                },
                {
                    header: 'Total </br>Realizado', dataIndex: 'Realizado', width: 60, align: 'center',
                    renderer: function (value, metaData, record, rowIndex, colIndex, store) {
                        metaData.attr = 'style="font-weight: bold;"'
                        return value;
                    }
                },
                {
                    header: 'Total </br>Saldo', dataIndex: 'Saldo', width: 50, align: 'center',
                    renderer: function (value, metaData, record, rowIndex, colIndex, store) {
                        if (value < 0) {
                            metaData.attr = 'style="color:red;font-weight: bold;"'
                        }
                        else {
                            metaData.attr = 'style="font-weight: bold;"'
                        }
                        return value;
                    }
                },
                {
                    header: 'Total </br>%', dataIndex: 'Porcentagem', width: 40, align: 'center',
                    renderer: function (value, metaData, record, rowIndex, colIndex, store) {
                        metaData.attr = 'style="font-weight: bold;"'
                        return value;
                    }
                },
                {
                    id: 'status', header: '', dataIndex: 'Status', width: 300, align: 'center',
                    renderer: function (value, metaData, record, rowIndex, colIndex, store) {
                        metaData.attr = 'style="font-weight: bold;"'
                        return value;
                    }
                }
            ]
        });

        ////////////////////////////////////////////////////////////////////////////////////////////////////

        function buscaDDLComponentValue(componentName) {
            var comp = Ext.getCmp(componentName);
            var value = '';

            if (comp) {
                var value = comp.getValue();
            }

            return value
        }

        function buscaDateComponentValue(componentName) {
            var comp = Ext.getCmp(componentName);
            var value = '';
            if (comp) {
                var value = comp.getValue().format('d/m/Y');
            }

            return value
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        var pedraGroupStore = new Ext.data.GroupingStore({
            id: 'pedraGroupStore',
            sortInfo: {         
                field: 'Regiao',           
                direction: "ASC"        
            },
            groupField: 'Regiao', 
            reader: new Ext.data.JsonReader({
                root: "Analitico",
                autoLoad: false,
                fields: [
                    { name: 'name', mapping: 'firstname' },
                    'PedraRealizadoId',
                    'ReuniaoId',
                    'FluxoId',
                    'Operacao',
                    'DataPedra',
                    'Estacao',
                    'EstacaoDescricao',
                    'Origem',
                    'OrigemDescricao',
                    'Terminal',
                    'TerminalDescricao',
                    'Regiao',
                    'RegiaoDescricao',
                    'Segmento',
                    'SegmentoDescricao',
                    'PlanejadoSade',
                    'Realizado',
                    'Saldo',
                    'Porcentagem',
                    'Status',
                    'PedraEditadaFlag',
                    'PermiteEditarPedra'
                ]
            },)      
        }); 

        var grid = new Ext.grid.GridPanel({      
            id: 'grid',
            title: 'Testes',      
            store: pedraGroupStore,      
            columns: columnModelDetalhe,      
            view: new Ext.grid.GroupingView()    
        }); 

        var pedraDetalheStore = new Ext.data.JsonStore({
            root: "Analitico",
            autoLoad: false,
            id: 'pedraDetalheStore',
            fields: [
                'PedraRealizadoId',
                'ReuniaoId',
                'FluxoId',
                'Operacao',
                'DataPedra',
                'Estacao',
                'EstacaoDescricao',
                'Origem',
                'OrigemDescricao',
                'Terminal',
                'TerminalDescricao',
                'Regiao',
                'RegiaoDescricao',
                'Segmento',
                'SegmentoDescricao',
                'PlanejadoSade',
                'Realizado',
                'Saldo',
                'Porcentagem',
                'Status',
                'PedraEditadaFlag',
                'PermiteEditarPedra'
            ]
        });

        var dropDownListColunas = {
            layout: 'column',
            border: false,
            items: [arrlinha1]
        };

        var fieldSetFilters = {
            xtype: 'fieldset',
            autoHeight: true,
            bodyStyle: 'padding: 5px',
            title: 'Filtros',
            items: [dropDownListColunas],
            buttonAlign: "center",
            buttons:
                [
                    {
                        text: 'Pesquisar',
                        type: 'submit',
                        iconCls: 'icon-find',
                        handler: function (b, e) {
                            Pesquisar();
                        }
                    },
                    {
                        text: 'Excel',
                        type: 'button',
                        iconCls: 'icon-page-excel',
                        handler: function (b, e) {
                            Excel('geral');
                        }
                    },
                    {
                        text: 'Enviar por email',
                        type: 'button',
                        iconCls: 'icon-email_go',
                        handler: function (b, e) {
                            Email('geral');
                        }
                    }
                ]
        };

        var fieldSetGrid = {
            xtype: 'fieldset',
            autoHeight: true,
            bodyStyle: 'padding: 5px',
            title: 'Resultado - Detalhe',
            items: [grid]
        };


        $(function () {


            var panelGeral = new Ext.form.FormPanel({
                id: 'panel-geral',
                layout: 'form',
                labelAlign: 'top',
                border: false,
                autoHeight: true,
                title: "Planejado X Realizado",
                region: 'center',
                bodyStyle: 'padding: 5px',
                items: [fieldSetFilters], //, fieldSetGrid
                buttonAlign: "center",
                split: true
            }).render(document.body);

        });
    </script>
</asp:Content>
