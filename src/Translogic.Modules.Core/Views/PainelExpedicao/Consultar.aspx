﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Interna.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content runat="server" ID="HeadContent" ContentPlaceHolderID="HeadContent">
    <div id="header-content">
        <h1>
            Painel Expedição</h1>
        <small>Consulta Detalhada</small>
        <br />
    </div>
    <script type="text/javascript" language="javascript">
        
        function mensagem(title, msg){
            Ext.Msg.show({
				    title: title,
				    msg: msg,
				    buttons: Ext.Msg.OK,
				    icon: Ext.MessageBox.INFO,
				    minWidth: 200
				 });            
        }

        /* region :: Functions */

        function Pesquisar() {
            
            var dataInicio = Ext.getCmp("txtDataInicio").getValue();
            var dataFinal = Ext.getCmp("txtDataFinal").getValue();
            
            if (dataInicio == "" || dataFinal == ""){
                mensagem("Atenção", "Osdois filtros de datas devem ser fornecidos!");
            }
            else 
            {
                var cliente = Ext.getCmp("ddlCliente").getValue();
                var segmento = Ext.getCmp("ddlSegmento").getValue();
                var expedidor = Ext.getCmp("ddlExpedidor").getValue();
                var situacao = Ext.getCmp("ddlSituacao").getValue();
                var lotacao = Ext.getCmp("ddlLotacao").getValue();
                var vagoes = '';
			    if(storeVagoes.getCount() != 0)
			    {
				    storeVagoes.each(
					    function (record) {
						    if (!record.data.Erro) {
							    vagoes += record.data.Vagao + ";";	
						    }
					    }
				    );
			    } else {
				    vagoes = Ext.getCmp("txtVagoes").getValue();	
			    }
           
                var fluxo = Ext.getCmp("txtFluxo").getValue();
                var origem = Ext.getCmp("txtOrigem").getValue();
                var destino = Ext.getCmp("txtDestino").getValue();
                var localAtual = Ext.getCmp("txtLocalAtual").getValue();
                var mercadorias = Ext.getCmp("txtMercadorias").getValue();

                if ((dataInicio != "" && dataFinal != "")
                    && (fluxo == "" && origem == "" && destino == "" && localAtual == "" && situacao == "" && vagoes == "" && cliente == "" && expedidor == "" && mercadorias == "" && segmento == "")
                    && (lotacao == "" || lotacao == "Todas"))
                {
                    mensagem("Atenção", "Você deve fornecer mais algum filtro para a pesquisa além da data!");
                }
                else {
                    grid.getStore().load();
                }
            }
        }

        function Limpar() {
            grid.getStore().removeAll();
            storeVagoes.removeAll();
            Ext.getCmp("txtVagoes").setDisabled(false);
            Ext.getCmp("filtros").getForm().reset();
        }

        function FormataColunaCentralizado(value, meta) {

            var valor = '';
            if (value == undefined)
                valor = '';
            else
                valor = value;
            return '<div style="text-align: center;">' + valor + '</div>';
        }


        function FormataColunaCentralizadoCte(value, meta) {


            var valor = '';
            if (value == undefined)
                valor = '';
            else {
                if (value.toUpperCase() == 'AUT')
                    valor = 'Sim';
                else
                    valor = 'Não';
            }
                
            return '<div style="text-align: center;">' + valor + '</div>';
        }


        /* endregion :: Functions */

        /* region :: Stores */

        var clienteStore = new window.Ext.data.JsonStore({
            id: 'clienteStore',
            name: 'clienteStore',
            root: 'Items',
            url: '<%=Url.Action("ObterConfiguracoesClientes", "PainelExpedicao") %>',
            fields: [
                    'Id',
                    'Nome'
                ],
            listeners: {
                load: function (store) {
                    var rt = store.recordType;
                    store.insert(0, new rt({}));
                }
            }
        });
        window.clienteStore = clienteStore;
        
        var expedidorStore = new window.Ext.data.JsonStore({
            id: 'expedidorStore',
            name: 'expedidorStore',
            root: 'Items',
            url: '<%=Url.Action("ObterExpedidores", "PainelExpedicao") %>',
            fields: [
                    'DescResumida'
                ],
            listeners: {
                load: function (store) {
                    var rt = store.recordType;
                    store.insert(0, new rt({}));
                }
            }
        });
        window.expedidorStore = expedidorStore;

        var situacaoStore = new window.Ext.data.JsonStore({
            id: 'situacaoStore',
            name: 'situacaoStore',
            root: 'Items',
            url: '<%= Url.Action("ObterSituacoes", "PainelExpedicao") %>',
            fields: [
                    'Id',
                    'Nome'
                ],
            listeners: {
                load: function (store) {
                    var rt = store.recordType;
                    store.insert(0, new rt({}));
                }
            }
        });
        window.situacaoStore = situacaoStore;
        
        var lotacaoStore = new window.Ext.data.JsonStore({
            root: "Items",
            autoLoad: true,
            proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("ObterLotacoes", "PainelExpedicao") %>',
                timeout: 600000
            }),
            id: 'lotacaoStore',
            fields: [
                'Id',
                'Nome'
            ]
        });
        window.lotacaoStore = lotacaoStore;

        var segmentosStore = new Ext.data.JsonStore({
            root: "Items",
            autoLoad: true,
            proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("ObterSegmentos", "PainelExpedicao") %>',
                timeout: 600000
            }),
            id: 'segmentosStore',
            fields: [
                'Id',
                'Nome'
            ]
        });
        
        var storeVagoes = new Ext.data.JsonStore({
		remoteSort: true,
		root: "Items",
		fields: 
		    [
			    'Vagao',
			    'Erro',
			    'Mensagem'
			]
	    });

        /* endregion :: Stores */

        /* region :: Filtros */

        var txtDataInicio = {
            xtype: 'datefield',
            fieldLabel: 'Data Início',
            id: 'txtDataInicio',
            name: 'txtDataInicio',
            width: 85,
            allowBlank: false,
            value: new Date()
        };

        var txtDataFinal = {
            xtype: 'datefield',
            fieldLabel: 'Data Fim',
            id: 'txtDataFinal',
            name: 'txtDataFinal',
            width: 85,
            allowBlank: false,
            value: new Date()
        };

        var txtFluxo = {
            xtype: 'textfield',
            name: 'txtFluxo',
            id: 'txtFluxo',
            fieldLabel: 'Fluxo',
            autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '7' },
            maskRe: /[a-zA-Z0-9]/,
            style: 'text-transform: uppercase',
            width: 70
        };

        var txtOrigem = {
            xtype: 'textfield',
            name: 'txtOrigem',
            id: 'txtOrigem',
            fieldLabel: 'Origem',
            autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '3' },
            maskRe: /[a-zA-Z]/,
            style: 'text-transform: uppercase',
            width: 50
        };

        var txtDestino = {
            xtype: 'textfield',
            name: 'txtDestino',
            id: 'txtDestino',
            fieldLabel: 'Destino',
            autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '3' },
            maskRe: /[a-zA-Z]/,
            style: 'text-transform: uppercase',
            width: 50
        };

        var txtVagoes = {
            xtype: 'textfield',
            name: 'txtVagoes',
            id: 'txtVagoes',
            fieldLabel: 'Vagão',
            autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '100' },
            maskRe: /[a-zA-Z0-9\;]/,
            //maxLength: 500,
            width: 100,
            enableKeyEvents: true,
            listeners: {
                specialkey: function (f, e) {
                    if (e.getKey() == e.ENTER) {
                        Pesquisar();
                    }
                }
            }
        };
        
        var btnLoteVagao = {
			xtype: 'button',
			text: 'Informar Vagões',
			iconCls: 'icon-find',
			handler: function (b, e) {
				windowStatusVagoes = new Ext.Window({
							            id: 'windowStatusVagoes',
							         	title: 'Informar vagões',
							         	modal: true,
							         	width: 855,
							         	resizable: false,
							         	height: 380,
							         	autoLoad: {
							         		url: '<%= Url.Action("FormInformarVagoes") %>',
							         		scripts: true
							         	}
							         });

                windowStatusVagoes.show();
			}
		};

        var txtLocalAtual = {
            xtype: 'textfield',
            name: 'txtLocalAtual',
            id: 'txtLocalAtual',
            fieldLabel: 'Local Atual',
            autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '3' },
            maskRe: /[a-zA-Z]/,
            style: 'text-transform: uppercase',
            width: 60
        };

        var ddlCliente = {
            xtype: 'combo',
            id: 'ddlCliente',
            name: 'ddlCliente',
            forceSelection: true,
            store: window.clienteStore,
            triggerAction: 'all',
            mode: 'remote',
            typeAhead: true,
            minChars: 3,
            fieldLabel: 'Cliente',
            displayField: 'Nome',
            valueField: 'Id',
            width: 175,
            editable: true,
            tpl: '<tpl for="."><div class="x-combo-list-item">{Nome}&nbsp;</div></tpl>'
        };
        
        var ddlExpedidor = {
            xtype: 'combo',
            id: 'ddlExpedidor',
            name: 'ddlExpedidor',
            forceSelection: true,
            store: window.expedidorStore,
            triggerAction: 'all',
            mode: 'remote',
            typeAhead: true,
            minChars: 1,
            fieldLabel: 'Expedidor',
            displayField: 'DescResumida',
            valueField: 'DescResumida',
            width: 125,
            editable: true,
            tpl: '<tpl for="."><div class="x-combo-list-item">{DescResumida}&nbsp;</div></tpl>'
        };
        
        var ddlSituacao = {
            xtype: 'combo',
            id: 'ddlSituacao',
            name: 'ddlSituacao',
            forceSelection: true,
            store: window.situacaoStore,
            triggerAction: 'all',
            mode: 'remote',
            typeAhead: false,
            minChars: 1,
            fieldLabel: 'Situação',
            displayField: 'Nome',
            valueField: 'Id',
            width: 100,
            editable: false,
            tpl: '<tpl for="."><div class="x-combo-list-item">{Nome}&nbsp;</div></tpl>'
        };
        
        var ddlLotacao = new Ext.form.ComboBox({
            typeAhead: false,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            store: lotacaoStore,
            valueField: 'Id',
            displayField: 'Nome',
            fieldLabel: 'Lotação',
            id: 'ddlLotacao',
            width: 90
        });

        var txtMercadorias = {
            xtype: 'textfield',
            name: 'txtMercadorias',
            id: 'txtMercadorias',
            fieldLabel: 'Lista de Mercadorias(separados por ";")',
            autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '100' },
            maskRe: /[a-zA-Z0-9\;]/,
            //maxLength: 500,
            width: 315,
            enableKeyEvents: true,
            listeners: {
                specialkey: function (f, e) {
                    if (e.getKey() == e.ENTER) {
                        Pesquisar();
                    }
                }
            }
        };

        var ddlSegmento = new Ext.form.ComboBox({
            typeAhead: false,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            store: segmentosStore,
            valueField: 'Id',
            displayField: 'Nome',
            fieldLabel: 'Segmento',
            id: 'ddlSegmento',
            width: 100
        });

        // Filtros Linha 1 //
        var arrDataIni = {
            width: 90,
            layout: 'form',
            border: false,
            items: [txtDataInicio]
        };

        var arrDataFim = {
            width: 90,
            layout: 'form',
            border: false,
            items: [txtDataFinal]
        };

        
        var arrFluxo = {
            width: 75,
            layout: 'form',
            border: false,
            items: [txtFluxo]
        };

        var arrOrigem = {
            width: 55,
            layout: 'form',
            border: false,
            items: [txtOrigem]
        };

        var arrDestino = {
            width: 55,
            layout: 'form',
            border: false,
            items: [txtDestino]
        };

        var arrVagoes = {
            width: 105,
            layout: 'form',
            border: false,
            items: [txtVagoes]
        };
        
        var arrBtnLoteVagoes = {
            width: 120,
			layout: 'form',
			style: 'padding: 16px 0px 0px 0px;',
			border: false,
			items: [btnLoteVagao]
        };

        var arrLocalAtual = {
            width: 65,
            layout: 'form',
            border: false,
            items: [txtLocalAtual]
        };
        
        var arrSituacao = {
            width: 105,
            layout: 'form',
            border: false,
            items: [ddlSituacao]
        };
        
        var arrLotacao = {
            width: 95,
            layout: 'form',
            border: false,
            items: [ddlLotacao]
        };

        var arrlinha1 = {
            layout: 'column',
            border: false,
            items: [
                arrDataIni, 
                arrDataFim, 
                arrFluxo, 
                arrOrigem, 
                arrDestino,
                arrLocalAtual, 
                arrSituacao,
                arrLotacao,
                arrVagoes, 
                arrBtnLoteVagoes
            ]
        };
        
        //------------------------------//

        // Filtros Linha 2 //

        var arrCliente = {
            width: 180,
            layout: 'form',
            border: false,
            items: [ddlCliente]
        };

        var arrTerminalFaturamento = {
            width: 130,
            layout: 'form',
            border: false,
            items: [ddlExpedidor]
        };

        var arrMercadoria = {
            width: 320,
            layout: 'form',
            border: false,
            items: [txtMercadorias]
        };

        var arrSegmento = {
            width: 105,
            layout: 'form',
            border: false,
            items: [ddlSegmento]
        };
        
        var arrlinha2 = {
            layout: 'column',
            border: false,
            items: [
                arrCliente, 
                arrTerminalFaturamento, 
                arrMercadoria, 
                arrSegmento
            ]
        };
        
        //------------------------------//

        var arrCampos = new Array();
        arrCampos.push(arrlinha1);
        arrCampos.push(arrlinha2);

        var filtros = new Ext.form.FormPanel({
            id: 'filtros',
            title: 'Filtros',
            name: 'filtros',
            region: 'center',
            bodyStyle: 'padding: 15px',
            initEl: function (el) {
                this.el = Ext.get(el);
                this.id = this.el.id || Ext.id();
                if (this.standardSubmit) {
                    this.el.dom.action = this.url;
                } else {
                    this.el.on('submit', this.onSubmit, this);
                }
                this.el.addClass('x-form');
            },
            //layout: 'form',
            labelAlign: 'top',
            border: false,
            items: [arrCampos], //[linha],
            buttonAlign: 'center',
            buttons: [
                {
                    name: 'btnPesquisar',
                    id: 'btnPesquisar',
                    text: 'Pesquisar',
                    iconCls: 'icon-find',
                    handler: Pesquisar
                },
                {
                    name: 'btnLimpar',
                    id: 'btnLimpar',
                    text: 'Limpar',
                    handler: Limpar
                }
            ]
        });

        /* endregion :: Filtros*/

        /* region :: Grid */

        grid = new Translogic.PaginatedGrid({
            autoLoadGrid: false,
            id: "grid",
            stripeRows: true,
            region: 'center',
            width: "100%",
            height: 300,
            autoScroll: true,
            loadMask: { msg: App.Resources.Web.Carregando },
            filteringToolbar: [
                {
                    id: 'btnExportar',
                    text: 'Exportar',
                    tooltip: 'Exportar',
                    iconCls: 'icon-page-excel',
                    handler: function(c) {
                        var url = "<%= this.Url.Action("ObterPainelExpedicaoConsulta") %>";
	                    
						var ddlCliente = Ext.getCmp("ddlCliente");
						var clienteId = ddlCliente.getValue();

						var ddlSegmento = Ext.getCmp("ddlSegmento");
						var segmentoId = ddlSegmento.getValue();

                        var ddlExpedidor = Ext.getCmp("ddlExpedidor");
                        var expedidorId = ddlExpedidor.getValue();

                        var ddlSituacao = Ext.getCmp("ddlSituacao");
                        var situacaoId = ddlSituacao.getValue();

                        var ddlLotacao = Ext.getCmp("ddlLotacao");
                        var lotacaoId = ddlLotacao.getValue();

						var dataInicio = Ext.getCmp("txtDataInicio").getValue();
						var dataFinal = Ext.getCmp("txtDataFinal").getValue();

						var dataInicioFormat = dataInicio.format('d/m/Y');
						var dataFinalFormat = dataFinal.format('d/m/Y');
						var fluxo = Ext.getCmp("txtFluxo").getValue();
						var origem = Ext.getCmp("txtOrigem").getValue();
						var destino = Ext.getCmp("txtDestino").getValue();
                        var terminalFaturamento = expedidorId;
                        var vagoes = '';
			            if(storeVagoes.getCount() != 0)
			            {
			            	storeVagoes.each(
			            		function (record) {
			            			if (!record.data.Erro) {
			            				vagoes += record.data.Vagao + ";";	
			            			}
			            		}
			            	);
			            } else {
			            	vagoes = Ext.getCmp("txtVagoes").getValue();	
			            }
                        
                        var situacao = situacaoId;
                        var lotacao = lotacaoId;
						var localAtual = Ext.getCmp("txtLocalAtual").getValue();
						var cliente = clienteId;
						var mercadorias = Ext.getCmp("txtMercadorias").getValue();
						var segmento = segmentoId;

						url += "?dataInicio=" + dataInicioFormat + '&dataFinal=' + dataFinalFormat + '&fluxo=' + fluxo;
	                    url += '&origem=' + origem + '&destino=' + destino + '&vagoes=' + vagoes + '&situacao=' + situacao + '&lotacao=' + lotacao + '&localAtual=' + localAtual;
	                    url += '&cliente=' + cliente + '&terminalFaturamento=' + terminalFaturamento + '&mercadorias=' + mercadorias + '&segmento=' + segmento;
                        url += '&exportarExcel=true';

                        window.open(url, "");
                    }
                }
            ],
            url: '<%= this.Url.Action("ObterPainelExpedicaoConsulta") %>',
            viewConfig: {
                forceFit: false,
                emptyText: 'Não possui registros para exibição.'
            },
            fields: [
                'Data',
                'DataBo',
                'HorarioRecebimentoFaturamento',
                'HorarioRecebimentoArquivo',
                'HorarioFaturamento',
                'HorarioLiberacao1380',
                'SerieVagao',
                'Vagao',
                'Situacao',
                'Lotacao',
                'PesoTu',
                'PesoTt',
                'PesoTb',
                'MultiploDespacho',
                'Fluxo',
                'Origem',
                'Destino',
                'TerminalFaturamento',
                'Cliente',
                'Mercadoria',
                'Container',
                'Local',
                'Trem',
                'Faturado',
                'FaturadoManual',
                'ErroFaturamento',
                'CTe',
                'Segmento',
                'Ticket',
                'Aprovador',
                'UsuarioFaturamento'
            ],
            columns: [
                new Ext.grid.RowNumberer(),
                { header: 'Data Faturamento', dataIndex: "Data", width: 100, sortable: true, align: 'center' },
                
                { header: 'Série', dataIndex: "SerieVagao", width: 40, sortable: true, align: 'center' },
                { header: 'Vagão', dataIndex: "Vagao", width: 60, sortable: true, align: 'center' },
                { header: 'Lotação', dataIndex: "Lotacao", width: 70, sortable: true, align: 'center' },
                { header: 'Situação', dataIndex: "Situacao", width: 70, sortable: true, align: 'center' },
                
                { header: 'TU', dataIndex: "PesoTu", width: 50, sortable: true, align: 'center' },
                { header: 'Tara', dataIndex: "PesoTt", width: 50, sortable: true, align: 'center' },
                { header: 'TB', dataIndex: "PesoTb", width: 50, sortable: true, align: 'center' },

                { header: 'MD', dataIndex: "MultiploDespacho", width: 30, sortable: true, align: 'center' },
                { header: 'Fluxo', dataIndex: "Fluxo", width: 60, sortable: true, align: 'center' },
                { header: 'Origem', dataIndex: "Origem", width: 55, sortable: true, align: 'center' },
                { header: 'Destino', dataIndex: "Destino", width: 55, sortable: true, align: 'center' },
                { header: 'Expedidor', dataIndex: "TerminalFaturamento", width: 100, sortable: true },
                { header: 'Cliente', dataIndex: "Cliente", sortable: true },
                { header: 'Mercadoria', dataIndex: "Mercadoria", sortable: true },
                { header: 'Container', dataIndex: "Container", sortable: true },
                { header: 'Local', dataIndex: "Local", width: 55, sortable: true, align: 'center' },
                { header: 'Prefixo Trem', dataIndex: "Trem", width: 100, sortable: true },
                { header: 'Faturado 363', dataIndex: "Faturado", width: 80, sortable: true, align: 'center' },
                { header: 'Faturado 1131', dataIndex: "FaturadoManual", width: 80, sortable: true, align: 'center' },
                { header: 'Erro Faturamento', dataIndex: "ErroFaturamento", sortable: true },
                { header: 'CT-e', dataIndex: "CTe", width: 60, sortable: true, align: 'center', renderer: FormataColunaCentralizadoCte },
                { header: 'Segmento', dataIndex: "Segmento", sortable: true, align: 'center', renderer: FormataColunaCentralizado },
                { header: 'Ticket', dataIndex: "Ticket", width: 60, sortable: true, align: 'center', renderer: FormataColunaCentralizado },
                { header: 'Usuário Aprovador na 1380', dataIndex: "Aprovador", width: 200, sortable: true },
                { header: 'Usuário Faturamento', dataIndex: "UsuarioFaturamento", width: 200, sortable: true },
                { header: 'Data Receb. Sistema Rumo', dataIndex: "HorarioRecebimentoArquivo", width: 150, sortable: true, align: 'center' },
                { header: 'Data Recebimento 1380', dataIndex: "HorarioLiberacao1380", width: 150, sortable: true, align: 'center' },
                { header: 'Data Recebimento 363', dataIndex: "HorarioRecebimentoFaturamento", width: 120, sortable: true, align: 'center' },
                { header: 'Data Faturamento', dataIndex: "HorarioFaturamento", width: 120, sortable: true, align: 'center' }
            ]
        });

        grid.getStore().proxy.on('beforeload', function (p, params) {
            var ddlCliente = Ext.getCmp("ddlCliente");
            var clienteId = ddlCliente.getValue();

            var ddlSegmento = Ext.getCmp("ddlSegmento");
            var segmentoId = ddlSegmento.getValue();

            var ddlExpedidor = Ext.getCmp("ddlExpedidor");
            var expedidorId = ddlExpedidor.getValue();

            var ddlSituacao = Ext.getCmp("ddlSituacao");
            var situacaoId = ddlSituacao.getValue();

            var ddlLotacao = Ext.getCmp("ddlLotacao");
            var lotacaoId = ddlLotacao.getValue();

            var dataInicio = Ext.getCmp("txtDataInicio").getValue();
            var dataFinal = Ext.getCmp("txtDataFinal").getValue();
            
            var vagoes = '';
			if(storeVagoes.getCount() != 0)
			{
				storeVagoes.each(
					function (record) {
						if (!record.data.Erro) {
							vagoes += record.data.Vagao + ";";	
						}
					}
				);
			} else {
				vagoes = Ext.getCmp("txtVagoes").getValue();	
			}

            params['dataInicio'] = dataInicio.format('d/m/Y');
            params['dataFinal'] = dataFinal.format('d/m/Y');
            params['fluxo'] = Ext.getCmp("txtFluxo").getValue();
            params['origem'] = Ext.getCmp("txtOrigem").getValue();
            params['destino'] = Ext.getCmp("txtDestino").getValue();
            params['vagoes'] = vagoes;
            params['situacao'] = situacaoId;
            params['lotacao'] = lotacaoId;
            params['localAtual'] = Ext.getCmp("txtLocalAtual").getValue();
            params['cliente'] = clienteId;
            params['terminalFaturamento'] = expedidorId;
            params['mercadorias'] = Ext.getCmp("txtMercadorias").getValue();
            params['segmento'] = segmentoId;
            params['exportarExcel'] = false;
        });


        // ***************************************** 
        // Adiciona o ToolTip na grid do Cte Filho 
        // *****************************************
        grid.on('render', function (grid) {
            var view = grid.getView();
            grid.tip = new Ext.ToolTip({
                target: view.mainBody,
                title: '',
                delegate: '.x-grid3-row',
                trackMouse: true,
                renderTo: document.body,

                listeners: {
                    beforeshow: function updateTipBody(tip) {

                        var rowIndex = view.findRowIndex(tip.triggerElement);
                        var erroFaturamento = grid.getStore().getAt(rowIndex).get('ErroFaturamento');
                        tip.resizable = true;
                        tip.body.dom.innerHTML = "";
                        if ((erroFaturamento != '') && (erroFaturamento != null)) {
                            tip.maxWidth = 300;
                            var msg = "<b>Erro Faturamento:</b><br/>" + erroFaturamento;
                            tip.body.dom.innerHTML = msg;
                        }
                    },
                    show: function showTip(tip) {

                    }
                }
            });
        });

        /* endregion :: Grid */

        /************************* RENDER **************************/

        var colunaDet1 = {
                    title: '',
                    width: "100%",
                    height: 320,
                    layout: 'form',
                    border: true,
                    items: [grid]
        };
		

        var columns = {
            layout: 'column',
            width: "100%",
            height: 320,
			    border: false,
				autoScroll: true,		
                region: 'center',
			    items: [ colunaDet1 ]
        };
        
        new Ext.Viewport({
				layout: 'border',
				margins: 10,
				items: [
					{
						region: 'north',
						height: 240,
						width: 1065,
						items: [{
							region: 'center',
							applyTo: 'header-content'
			}, filtros]
					},
					columns
			    ]
	    });

    </script>
</asp:Content>
