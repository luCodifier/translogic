﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Interna.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content runat="server" ID="HeadContent" ContentPlaceHolderID="HeadContent">
    <div id="header-content">
        <h1>
            Painel Expedição</h1>
        <small>Conferência de Arquivos
            <%= ViewData["PerfilAcessoPainelExpedicaoTitulo"] %>
        </small>
        <br />
    </div>
    <script type="text/javascript" language="javascript">

        var podeAlterarRegistrosPerfilCentral;
        var podeConfirmarRecusar;
        var perfilCentralFaturamento = ("true" === "" + <%= ViewData["PerfilAcessoCentralFaturamento"].ToString().ToLower() %> + "");
        var perfilCentralFaturamentoSuper = ("true" === "" + <%= ViewData["PerfilAcessoCentralFaturamentoSuper"].ToString().ToLower() %> + "");
        var grid = null;
        var gridEdiErro = null;
        
        /* region :: Stores */
        
        var storeVagoes = new Ext.data.JsonStore({
        remoteSort: true,
        root: "Items",
        fields: 
            [
                'Vagao',
                'Erro',
                'Mensagem'
            ]
        });
        
        /* endregion :: Stores */
        
        /* region :: Functions */
        
        function RetornaVagoes() {
            var vagoes = '';
            if(storeVagoes.getCount() != 0)
            {
                storeVagoes.each(
                    function (record) {
                        if (!record.data.Erro) {
                            vagoes += record.data.Vagao + ";";
                        }
                    }
                );
            } else {
                vagoes = Ext.getCmp("txtVagoes").getValue();
            }

            return vagoes;
        }

        function setarDataFinal(){
            //Set date + 1
            var d = new Date();
            d.setDate(d.getDate() + 1);
            Ext.getCmp("dataCadastroFinalBo").setValue(d);
        }
        
        function SalvarStatus(data) {
            // Confirmando registros...
            Ext.getBody().mask("Salvando as alterações.", "x-mask-loading");

            if (perfilCentralFaturamentoSuper === true) {
                $.ajax({
                    url: '<%= Url.Action("AtualizarStatusConferenciaArquivosFull", "PainelExpedicao") %>',
                    type: "POST",
                    dataType: 'json',
                    data: $.toJSON(data),
                    contentType: "application/json; charset=utf-8",
                    success: function(response, options) {
                        if (response.Success) {
                            Pesquisar();

                            Ext.Msg.show({
                                title: 'Aviso',
                                msg: 'Alterações salvas com sucesso!',
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.INFO
                            });
                        } else {
                            Ext.Msg.show({
                                title: 'Aviso',
                                msg: response.Message,
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.ERROR
                            });
                        }

                        Ext.getBody().unmask();
                    },
                    failure: function(response, options) {
                        Ext.Msg.show({
                            title: 'Aviso',
                            msg: response.Message,
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.ERROR
                        });

                        Ext.getBody().unmask();
                    }
                });
            } else {
                // Confirmando registros...

                $.ajax({
                    url: '<%= Url.Action("AtualizarStatusConferenciaArquivos", "PainelExpedicao") %>',
                    type: "POST",
                    dataType: 'json',
                    data: $.toJSON(data),
                    contentType: "application/json; charset=utf-8",
                    success: function(response, options) {
                        if (response.Success) {
                            Pesquisar();

                            Ext.Msg.show({
                                title: 'Aviso',
                                msg: 'Alterações salvas com sucesso!',
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.INFO
                            });
                        } else {
                            Ext.Msg.show({
                                title: 'Aviso',
                                msg: response.Message,
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.ERROR
                            });
                        }

                        Ext.getBody().unmask();
                    },
                    failure: function(response, options) {
                        Ext.Msg.show({
                            title: 'Aviso',
                            msg: response.Message,
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.ERROR
                        });

                        Ext.getBody().unmask();
                    }
                });
            }
        }

        function VerificaVagaoValido(vagao, idAgrupamentoMd, idBo, conteiner, idAgrupamentoSimples) {
            var retorno = '';

            var store = grid.getStore();
            var records = store.getRange();
            
            // Validações Despacho Simples
            if ((!idAgrupamentoMd) && (!idAgrupamentoSimples)) {


                $.each(records, function (index, rec) {
                    var idBoRec = rec.get('Id');
                    var idAgrupamentoMdRec = rec.get('IdAgrupamentoMD');

                    if ((idBo != idBoRec) && (!idAgrupamentoMdRec)) {

                        var vagaoRec = rec.get('Vagao');
                        if (vagao == vagaoRec) {
                            retorno = 'Não é possível alterar o vagão, pois o vagão informado já existe no dia selecionado!';
                            return false;
                        }

                    }
                });

            }
            else if (idAgrupamentoMd) {
                // Validações Despacho Múltiplo
                $.each(records, function (index, rec) {
                    var idBoRec = rec.get('Id');
                    var idAgrupamentoMdRec = rec.get('IdAgrupamentoMD');

                    if ((idBo != idBoRec) && (idAgrupamentoMdRec)) {

                        var vagaoRec = rec.get('Vagao');
                        var conteinerRec = rec.get('Conteiner');
                        if ((idAgrupamentoMd != idAgrupamentoMdRec) && (vagao != vagaoRec) && (conteiner) && (conteiner == conteinerRec)) {
                            retorno = 'Não é possível alterar o vagão pois o conteiner já está em outro vagão!';
                            return false;
                        }

                    }
                });
            }
            else if (idAgrupamentoSimples) {
                // Validações Despacho Simples
                $.each(records, function (index, rec) {
                    var idBoRec = rec.get('Id');
                    var idAgrupamentoSimplesRec = rec.get('IdAgrupamentoSimples');

                    if ((idBo != idBoRec) && (idAgrupamentoSimplesRec)) {

                        var vagaoRec = rec.get('Vagao');
                        var conteinerRec = rec.get('Conteiner');
                        if ((idAgrupamentoSimples != idAgrupamentoSimplesRec) && (vagao != vagaoRec) && (conteiner) && (conteiner == conteinerRec)) {
                            retorno = 'Não é possível alterar o vagão pois o conteiner já está em outro vagão!';
                            return false;
                        }

                    }
                });
            }
            
            return retorno;
        }

        function VerificaGrupoPaiSelecionado(idRegistro, idAgrupamentoMdRegistro, idAgrupamentoSimplesRegistro) {
            var store = grid.getStore();
            var records = store.getRange();
            var selection = sm.getSelections();
            var encontrouPrimeiroRegistro = false;
            var resultado = false;

            $.each(records, function (index, rec) {
                var idAgrupamentoMd = rec.get('IdAgrupamentoMD');
                var idAgrupamentoSimples = rec.get('IdAgrupamentoSimples');

                if (idAgrupamentoMdRegistro) {
                    if (idAgrupamentoMd == idAgrupamentoMdRegistro) {
                        if (encontrouPrimeiroRegistro == false) {
                            var id = rec.get('Id');
                            encontrouPrimeiroRegistro = true;

                            $.each(selection, function (i, e) {
                                if (e.data.Id == id)
                                    resultado = true;
                            });
                        }
                    }
                }
                else if (idAgrupamentoSimplesRegistro) {
                    if (idAgrupamentoSimples == idAgrupamentoSimplesRegistro) {
                        if (encontrouPrimeiroRegistro == false) {
                            var id = rec.get('Id');
                            encontrouPrimeiroRegistro = true;

                            $.each(selection, function (i, e) {
                                if (e.data.Id == id)
                                    resultado = true;
                            });
                        }
                    }
                }
                
            });

            return resultado;
        }

        function VerificaModificacoesPendentes(verRegistrosSelecionados) {
            var result = false;
            var grid = Ext.getCmp("grid");
            var store = grid.getStore();
            var records = store.getRange();
            var selecionados = RetornaSelecionados();

            $.each(records, function (index, rec) {
                var id = rec.get('Id');

                if (verRegistrosSelecionados == true) {
                    if ($.inArray(id, selecionados) != -1) {
                        if (rec.dirty == true) {
                            result = true;
                            return;
                        }
                    }
                } else {
                    if ($.inArray(id, selecionados) == -1) {
                        if (rec.dirty == true) {
                            result = true;
                            return;
                        }
                    }
                }

                
            });

            return result;
        }


        function VerificaDadosPendentesRecusado(verRegistrosSelecionados) {
            var result = false;
            var grid = Ext.getCmp("grid");
            var store = grid.getStore();
            var records = store.getRange();
            var selecionados = RetornaSelecionados();

            $.each(records, function (index, rec) {
                var id = rec.get('Id');
                var observacao = rec.get('ObservacaoRecusado');
                
                if (verRegistrosSelecionados == true) {
                    if ($.inArray(id, selecionados) != -1) {
                        //if (rec.dirty == true) {

                            if ((observacao == null) || (observacao == '')) {
                                result = true;
                                return;
                            }
                            
                        //}
                    }
                }

            });

            return result;
        }
        

        function RetiraModificacoesPendentes() {
            var grid = Ext.getCmp("grid");
            var store = grid.getStore();
            var records = store.getRange();

            $.each(records, function (index, rec) {
                rec.dirty = false;
            });
        }

        function RetornaSelecionados() {
            var ids = new Array();
            var selection = sm.getSelections();

            for (var i = 0; i < selection.length; i++) {
                var idAgrupamento = selection[i].data.IdAgrupamentoMD;
                var idAgrupamentoSimples = selection[i].data.IdAgrupamentoSimples;
                if (idAgrupamento) {
                    var grid = Ext.getCmp("grid");
                    var store = grid.getStore();
                    store.each(function (rec) {
                        var id = rec.get('Id');
                        var idAgrupamentoRec = rec.get('IdAgrupamentoMD');
                        if (idAgrupamentoRec == idAgrupamento) {
                            if (VerificaGrupoPaiSelecionado(id, idAgrupamentoRec, null)) {
                                if ($.inArray(id, ids) == -1)
                                    ids.push(id);
                            }
                        }
                    });
                }
                else if (idAgrupamentoSimples) {
                    var grid = Ext.getCmp("grid");
                    var store = grid.getStore();
                    store.each(function (rec) {
                        var id = rec.get('Id');
                        var idAgrupamentoRecSimples = rec.get('IdAgrupamentoSimples');
                        if (idAgrupamentoRecSimples == idAgrupamentoSimples) {
                            if (VerificaGrupoPaiSelecionado(id, null, idAgrupamentoRecSimples)) {
                                if ($.inArray(id, ids) == -1)
                                    ids.push(id);
                            }
                        }
                    });
                }
                else {
                    ids.push(selection[i].data.Id);
                }
            }

            return ids;
        }

        function RetornaDadosGrid(todosRegistros) {
            var retorno = new Array();
            var grid = Ext.getCmp("grid");
            var store = grid.getStore();
            var selecionados = RetornaSelecionados();

            store.each(function (rec) {
                var id = rec.get('Id');
                if ($.inArray(id, selecionados) != -1 || todosRegistros == true) {
                    var element = {
                        Id: undefined,
                        IdAgrupamentoMd: undefined,
                        IdAgrupamentoSimples: undefined,
                        VagaoId: undefined,
                        Vagao: undefined,
                        Conteiner: undefined,
                        PesoTotal: undefined,
                        Fluxo: undefined,
                        ChaveNota: undefined,
                        PesoRateioNFe: undefined,
                        PesoNFe: undefined,
                        ObservacaoRecusado: undefined,
                        MultiploDespacho: undefined,
                        MultiploDespachoStatus: undefined,
                        AoOrigem: undefined
                    };

                    element.Id = id;
                    element.IdAgrupamentoMd = rec.get('IdAgrupamentoMD');
                    element.IdAgrupamentoSimples = rec.get('IdAgrupamentoSimples');
                    element.VagaoId = rec.get('VagaoId');
                    element.Vagao = rec.get('Vagao');

                    if (rec.get('Conteiner') != null) {
                        element.Conteiner = rec.get('Conteiner').toUpperCase();
                    }
                    else {
                        element.Conteiner = null;
                    }

                    if (rec.get('ObservacaoRecusado') != null) {
                        element.ObservacaoRecusado = rec.get('ObservacaoRecusado');
                    }
                    else {
                        element.ObservacaoRecusado = null;
                    }

                    element.PesoTotal = rec.get('PesoTotal');
                    element.Fluxo = rec.get('Fluxo');
                    element.ChaveNota = rec.get('ChaveNota');
                    element.PesoRateioNFe = rec.get('PesoRateioNFe');
                    element.PesoNFe = rec.get('PesoNFe');
                    element.AoOrigem = rec.get('Origem');

                    element.MultiploDespacho = rec.get('MultiploDespacho');
                    if (rec.get('LiberacaoRegistroMultiploDespacho') === true) {
                        element.MultiploDespachoStatus = 'S';
                    }
                    else {
                        element.MultiploDespachoStatus = 'N';
                    }

                    retorno.push(element);
                }
            });

            return retorno;
        }
        
        function RetornaSelecionadosEdiErro() {
            var ids = new Array();
            var selection = smEdiErro.getSelections();

            for (var i = 0; i < selection.length; i++) {
                var idErroNfe = selection[i].data.IdErroEdiNfe;

                if (idErroNfe) {
                    var gridEdiErro = Ext.getCmp("gridEdiErro");
                    var store = gridEdiErro.getStore();
                    store.each(function (rec) {
                        var idErroNfeRec = rec.get('IdErroEdiNfe');
                        if (idErroNfeRec == idErroNfe) {
                            if ($.inArray(idErroNfe, ids) == -1)
                                ids.push(idErroNfe);
                        }
                    });
                }
            }

            return ids;
        }
        
        function RetornaDadosGridEdiErro(reprocessar) {
            var abortar = false;
            var retorno = new Array();
            var gridEdiErro = Ext.getCmp("gridEdiErro");
            var store = gridEdiErro.getStore();
            var selecionados = RetornaSelecionadosEdiErro();

            store.each(function (rec) {
                var idErroEdiNfe = rec.get('IdErroEdiNfe');
                // Agrupador do item selecionado
                var agrupador = rec.get('Agrupador');
                
                if ($.inArray(idErroEdiNfe, selecionados) != -1) {
                    var element = {
                        IdMensagemRecebimento: undefined,
                        IdErroEdiNfe: undefined,
                        ChaveNfe: undefined,
                        DataRegularizacao: undefined
                    };

                    element.IdMensagemRecebimento = rec.get('IdMensagemRecebimento');
                    element.IdErroEdiNfe = idErroEdiNfe;
                    element.ChaveNfe = rec.get('ChaveNfe');
                    element.DataRegularizacao = rec.get('DataRegularizacao');

                    retorno.push(element);
                    
                    // Somente se for reprocessar
                    if (reprocessar && abortar == false) {
                        // Devemos verificar se todos os grupos dos selecionados não foram esquecidos
                        store.each(function(r) {
                            var idErroEdiNfeAnalisado = r.get('IdErroEdiNfe');
                            var agrupadorAnalisado = r.get('Agrupador');
                            if (agrupadorAnalisado == agrupador && $.inArray(idErroEdiNfeAnalisado, selecionados) == -1) {
                                Ext.Msg.show({
                                        title: 'Aviso',
                                        msg: 'Selecione todos os registros do Item: ' + agrupador + ' para reprocessar.',
                                        buttons: Ext.Msg.OK,
                                        icon: Ext.MessageBox.ERROR
                                    });
                                abortar = true;
                                return;
                            }
                        });
                        
                        if (abortar == true) {
                            return;
                        }
                    }
                }
            });

            if (abortar == true)
                return new Array();
            else
                return retorno;
        }

        
        function SalvarAlteracoes() {
            Ext.Msg.show({
                title: 'Atenção',
                msg: 'Deseja salvar as alterações dos selecionados?',
                buttons: Ext.Msg.YESNO,
                fn: function (btn) {
                    if (btn == 'yes') {
                        var data = RetornaDadosGrid(false);

                        if (data.length > 0) {
                            ConfirmaSalvarAlteracoes();
                            
                        } else {

                            Ext.Msg.show({
                                title: 'Aviso',
                                msg: 'Selecione pelo menos um registro.',
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.ERROR
                            });
                        }
                    }
                },
                icon: Ext.MessageBox.QUESTION
            });
        }
        
        function ConfirmaSalvarAlteracoes() {
            var exiteModificacoesPendentesRegistrosNaoSelecionados = VerificaModificacoesPendentes(false);
            var data = RetornaDadosGrid(false);
            
            if (exiteModificacoesPendentesRegistrosNaoSelecionados) {
                
                Ext.Msg.show({
                    title: 'Atenção',
                    msg: 'Existem registros alterados que não foram selecionados. Ao confirmar, as alterações realizadas nestes registros serão desfeitas. Deseja continuar?',
                    buttons: Ext.Msg.YESNO,
                    fn: function(btn) {
                        if (btn == 'yes') {
                            Ext.getBody().mask("Salvando as alterações.", "x-mask-loading");

                            $.ajax({
                                url: '<%= Url.Action("SalvarAlteracoesConferenciaArquivos", "PainelExpedicao" ) %>',
                                type: "POST",
                                dataType: 'json',
                                data: $.toJSON(data),
                                contentType: "application/json; charset=utf-8",
                                success: function(response, options) {
                                    if (response.Success) {
                                        Pesquisar();
                                        RetiraModificacoesPendentes();

                                        Ext.Msg.show({
                                            title: 'Aviso',
                                            msg: 'Alterações salvas com sucesso!',
                                            buttons: Ext.Msg.OK,
                                            icon: Ext.MessageBox.INFO
                                        });
                                    } else {
                                        Ext.Msg.show({
                                            title: 'Aviso',
                                            msg: response.Message,
                                            buttons: Ext.Msg.OK,
                                            icon: Ext.MessageBox.ERROR
                                        });
                                    }

                                    Ext.getBody().unmask();
                                },
                                failure: function(response, options) {
                                    Ext.Msg.show({
                                        title: 'Aviso',
                                        msg: response.Message,
                                        buttons: Ext.Msg.OK,
                                        icon: Ext.MessageBox.ERROR
                                    });

                                    Ext.getBody().unmask();
                                }
                            });


                        }
                    },
                    icon: Ext.MessageBox.QUESTION
                });

            } else {

                Ext.getBody().mask("Salvando as alterações.", "x-mask-loading");

                $.ajax({
                    url: '<%= Url.Action("SalvarAlteracoesConferenciaArquivos", "PainelExpedicao" ) %>',
                    type: "POST",
                    dataType: 'json',
                    data: $.toJSON(data),
                    contentType: "application/json; charset=utf-8",
                    success: function (response, options) {
                        if (response.Success) {
                            Pesquisar();
                            RetiraModificacoesPendentes();

                            Ext.Msg.show({
                                title: 'Aviso',
                                msg: 'Alterações salvas com sucesso!',
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.INFO
                            });
                        } else {
                            Ext.Msg.show({
                                title: 'Aviso',
                                msg: response.Message,
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.ERROR
                            });
                        }

                        Ext.getBody().unmask();
                    },
                    failure: function (response, options) {
                        Ext.Msg.show({
                            title: 'Aviso',
                            msg: response.Message,
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.ERROR
                        });

                        Ext.getBody().unmask();
                    }
                });
                
            }

        }
        

        function ConfirmarSelecionados() {
            Ext.Msg.show({
                title: 'Atenção',
                msg: 'Deseja confirmar os registros selecionados?',
                buttons: Ext.Msg.YESNO,
                fn: function (btn) {
                    if (btn == 'yes') {
                        var data = {
                            model: RetornaDadosGrid(false),
                            recusar: false
                        };

                        // Usuário não selecionou nenhum registro
                        if (data.model.length == 0) {
                            Ext.Msg.show({
                                title: 'Aviso',
                                msg: 'Selecione pelo menos um registro.',
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.ERROR
                            });
                        }
                        else {
                            
                            /* verificar container voador */
                            if(VerificarConteiners(data))
                            {
                                var exiteModificacoesPendentesSelecionados = false;
                                if (podeAlterarRegistrosPerfilCentral == true)
                                    exiteModificacoesPendentesSelecionados = VerificaModificacoesPendentes(true);
                                if (exiteModificacoesPendentesSelecionados) {
                                    Ext.Msg.show({
                                            title: 'Aviso',
                                            msg: 'Existem registros selecionados com modificações não salvas.',
                                            buttons: Ext.Msg.OK,
                                            icon: Ext.MessageBox.ERROR
                                        });
                                }
                                else {
                                    var permiteSalvar = true;
                                    var vagoesMultiploDespachoIncompletos = [];
                                    $(data.model).each(function(index, element) {
                                        if (element.MultiploDespachoStatus === 'N') {
                                            vagoesMultiploDespachoIncompletos.push(element.Vagao);
                                        }
                                        else {
                                            if (element.MultiploDespacho === 'SIM') {
                                                /* Verifica se foi selecionado a sua finalização */
                                                var finalizado = false;
                                                $(data.model).each(function(i, e) {
                                                    if (e.Id !== element.Id) {
                                                        if (e.MultiploDespacho === 'NAO') {
                                                            finalizado = true;
                                                        }
                                                    }
                                                });

                                                if (finalizado === false) {
                                                    Ext.Msg.show({
                                                            title: 'Aviso',
                                                            msg: 'O registro cujo vagão é ' + element.Vagao + ' é um Múltiplo Despacho e está faltando sua finalização.',
                                                            buttons: Ext.Msg.OK,
                                                            icon: Ext.MessageBox.ERROR
                                                        });

                                                    permiteSalvar = false;
                                                    return;
                                                }
                                            }
                                        }
                                    });

                                    if (permiteSalvar === true) {


                                        if (vagoesMultiploDespachoIncompletos.length === 0) {
                                            SalvarStatus(data);
                                        } else {
                                            var vagoesString = '';
                                            $(vagoesMultiploDespachoIncompletos).each(function(index, element) {
                                                if (vagoesString === '')
                                                    vagoesString += element;
                                                else {
                                                    vagoesString += ', ' + element;
                                                }
                                            });

                                            var mensagemAlerta = 'O registro cujo vagões são ' + vagoesString + ' são Múltiplos Despachos e está faltando as suas finalizações. Se necessário, entre em contato com o Terminal/Cliente.<br /><br />Deseja prosseguir com a liberação?';
                                            if (vagoesMultiploDespachoIncompletos.length === 1) {
                                                mensagemAlerta = 'O registro cujo vagão é ' + vagoesString + ' é um Múltiplo Despacho e está faltando a sua finalização. Se necessário, entre em contato com o Terminal/Cliente.<br /><br />Deseja prosseguir com a liberação?';
                                            }
                                            Ext.Msg.show({
                                                    title: 'Atenção',
                                                    msg: mensagemAlerta,
                                                    buttons: Ext.Msg.YESNO,
                                                    fn: function(btnAtencao) {
                                                        if (btnAtencao == 'yes') {
                                                            SalvarStatus(data);
                                                        }
                                                    },
                                                    icon: Ext.MessageBox.QUESTION
                                                });
                                        }
                                    }
                                }
                            }
                        }
                    }
                },
                icon: Ext.MessageBox.QUESTION
            });
        }
        
        function VerificarConteiners(data) {
            var contErros = 0;
            var contVoos = "";
            var liConteiners = [];
            var msgErro = "";
            
            $(data.model).each(function(index, element) {
                if (element.Conteiner != null) {
                    codigoConteiner = element.Conteiner.toUpperCase();
                    var responseText = $.ajax({
                            url: "<%= Url.Action("ObterDadosConteiner") %>",
                            type: "POST",
                            async: false,
                            dataType: 'json',
                            data: ({
                                codigoConteiner: codigoConteiner,
                                siglaAoOrigem: element.AoOrigem
                            })
                        }).responseText;

                    var result = Ext.util.JSON.decode(responseText);

                    if (result.Voar) {
                        msgErro = result.Mensagem;
                        contVoos++;
                        liConteiners.push(result);
                    }
                    else if(result.Erro)
                    {
                        Ext.Msg.alert('Ops...', result.Mensagem);
                        return false;
                    }
                }
            });

            if (liConteiners.length > 0) {
                var msgVarios = "Foram selecionados alguns Contêineres que se encontram em local diferente da origem do Fluxo Comercial \nDeseja reposicionar os Contêineres?";
                msgErro = contVoos > 1 ? msgVarios : msgErro + "\nDeseja reposicionar o contêiner?";
                

                var blnRet = confirm(msgErro);
                var contSuccess = 0;
                if (blnRet){
                    $(liConteiners).each(function(index, element) {
                        var responseText = $.ajax({
                                url: "<%= Url.Action("VoarConteiner") %>",
                                type: "POST",
                                async: false,
                                dataType: 'json',
                                data: ({
                                    idConteiner: element.idConteiner,
                                    codAoOrigem: element.codAoOrigem
                                }),
                                success: function(result) {

                                    if (result.Erro) {
                                        Ext.Msg.alert('Ops...', result.Mensagem);
                                        return false;
                                    } else {
                                        contSuccess++;
                                    }
                                }
                            }).responseText;
                    });

                } else// Fim botão yes
                {
                    return false;
                }
            } // Fim IF Voar

            return true;
        }


        function RecusarSelecionados() {
            Ext.Msg.show({
                title: 'Atenção',
                msg: 'Deseja recusar os registros selecionados?',
                buttons: Ext.Msg.YESNO,
                fn: function (btn) {
                    if (btn == 'yes') {
                        var data = {
                            model: RetornaDadosGrid(false),
                            recusar: true
                        };

                        // Usuário não selecionou nenhum registro
                        if (data.model.length == 0) {
                            Ext.Msg.show({
                                title: 'Aviso',
                                msg: 'Selecione pelo menos um registro.',
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.ERROR
                            });
                        }
                        else {
                            var exiteModificacoesPendentesSelecionados = false;
                            var existemDadosPendentesRecusado = false;
                            // Perfil Central de Faturamento - Verificar se existem modificações não salvas
                            if (podeAlterarRegistrosPerfilCentral == true)
                                exiteModificacoesPendentesSelecionados = VerificaModificacoesPendentes(true);
                            else // Perfil Pátio - Verificar se existem dados não informados (Observação)
                                existemDadosPendentesRecusado = VerificaDadosPendentesRecusado(true);
                            
                            if (exiteModificacoesPendentesSelecionados) {
                                Ext.Msg.show({
                                    title: 'Aviso',
                                    msg: 'Existem registros selecionados com modificações não salvas.',
                                    buttons: Ext.Msg.OK,
                                    icon: Ext.MessageBox.ERROR
                                });
                            }
                            else {

                                if (existemDadosPendentesRecusado) {
                                    Ext.Msg.show({
                                        title: 'Aviso',
                                        msg: 'Existem registros a serem recusados sem observação informada.',
                                        buttons: Ext.Msg.OK,
                                        icon: Ext.MessageBox.ERROR
                                    });
                                } else {
                                    SalvarStatus(data);
                                }
                            }
                        }
                    }
                },
                icon: Ext.MessageBox.QUESTION
            });
        }
        

        // EDI erros - Regularização de notas selecionadas
        function RegularizarNotasSelecionadas() {
            Ext.Msg.show({
                title: 'Atenção',
                msg: 'Deseja baixar as notas selecionadas?',
                buttons: Ext.Msg.YESNO,
                fn: function (btn) {
                    if (btn == 'yes') {
                        var data = {
                            listaEdiErros: RetornaDadosGridEdiErro(false)
                        };

                        // Usuário não selecionou nenhum registro
                        if (data.listaEdiErros.length == 0) {
                            Ext.Msg.show({
                                title: 'Aviso',
                                msg: 'Selecione pelo menos um registro.',
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.ERROR
                            });
                        }
                        else {
                            
                            // chamar Action do EdiErroController
                            
                            Ext.getBody().mask("Verificando situação das notas...Favor aguarde", "x-mask-loading");

                            $.ajax({
                                url: '<%= Url.Action("RegularizarNotasSelecionadas", "EdiErro" ) %>',
                                type: "POST",
                                dataType: 'json',
                                data: $.toJSON(data),
                                contentType: "application/json; charset=utf-8",
                                success: function (response, options) {
                                    if (response.Success) {
                                        
                                        Ext.getBody().unmask();
                                        
                                        // Redirecionar Tela 1154
                                        if (response.RedirecionarTela1154 == true) {

                                            Ext.Msg.show({
                                                title: 'Aviso',
                                                msg: response.Message, //'Notas regularizadas com sucesso!',
                                                buttons: Ext.Msg.OK,
                                                icon: Ext.MessageBox.INFO
                                            });
                                            
                                            RedirecionarTela1154(response.NotasNaoRegularizadas);
                                            
                                            
                                            
                                        } else {

                                            PesquisarEdiErro();
                                            
                                            Ext.Msg.show({
                                                title: 'Aviso',
                                                msg: response.Message, //'Notas regularizadas com sucesso!',
                                                buttons: Ext.Msg.OK,
                                                icon: Ext.MessageBox.INFO
                                            });
                                        }

                                    } else {
                                        Ext.Msg.show({
                                            title: 'Aviso',
                                            msg: response.Message,
                                            buttons: Ext.Msg.OK,
                                            icon: Ext.MessageBox.ERROR
                                        });
                                    }

                                    Ext.getBody().unmask();
                                },
                                failure: function (response, options) {
                                    Ext.Msg.show({
                                        title: 'Aviso',
                                        msg: response.Message,
                                        buttons: Ext.Msg.OK,
                                        icon: Ext.MessageBox.ERROR
                                    });

                                    Ext.getBody().unmask();
                                }
                            });
                            
                        }
                    }
                },
                icon: Ext.MessageBox.QUESTION
            });
        }
        


        // EDI erros - Reprocessamento de notas selecionadas
        function ReprocessarNotasSelecionadas() {
            Ext.Msg.show({
                title: 'Atenção',
                msg: 'Deseja reprocessar as notas selecionadas?',
                buttons: Ext.Msg.YESNO,
                fn: function (btn) {
                    if (btn == 'yes') {
                        
                        var data = {
                            listaNotasReprocessar: RetornaDadosGridEdiErro(true)
                        };
                        
                        if (data.listaNotasReprocessar.length > 0) {
                            // chamar Action do EdiErroController    
                            Ext.getBody().mask("Reprocessando as notas...Favor aguarde", "x-mask-loading");

                            $.ajax({
                                url: '<%= Url.Action("ReprocessarNotasSelecionadas", "EdiErro" ) %>',
                                type: "POST",
                                dataType: 'json',
                                data: $.toJSON(data),
                                contentType: "application/json; charset=utf-8",
                                success: function(response, options) {
                                    if (response.Success) {

                                        Ext.getBody().unmask();

                                        PesquisarEdiErro();

                                        Ext.Msg.show({
                                            title: 'Aviso',
                                            msg: 'Notas reprocessadas com sucesso!',
                                            buttons: Ext.Msg.OK,
                                            icon: Ext.MessageBox.INFO
                                        });


                                    } else {
                                        Ext.Msg.show({
                                            title: 'Aviso',
                                            msg: response.Message,
                                            buttons: Ext.Msg.OK,
                                            icon: Ext.MessageBox.ERROR
                                        });

                                    }

                                    Ext.getBody().unmask();
                                },
                                failure: function(response, options) {
                                    Ext.Msg.show({
                                        title: 'Aviso',
                                        msg: response.Message,
                                        buttons: Ext.Msg.OK,
                                        icon: Ext.MessageBox.ERROR
                                    });

                                    Ext.getBody().unmask();
                                }
                            });
                        }
                    }
                },
                icon: Ext.MessageBox.QUESTION
            });
        }

        function Pesquisar() {
            btnConfirmar.hide();
            btnRecusar.hide();
            grid.getStore().load();
        }

        function Limpar() {
            grid.getStore().removeAll();
            storeVagoes.removeAll();
            Ext.getCmp("txtVagoes").setDisabled(false);
            Ext.getCmp("filtros").getForm().reset();
            setarDataFinal();
        }

        function PesquisarEdiErro() {
            gridEdiErro.getStore().load();
        }
        
        function LimparEdiErro() {
            gridEdiErro.getStore().removeAll();
            btnRegularizarNota.hide();
            btnReprocessarNota.hide();
            Ext.getCmp("panel-erros-edi").getForm().reset();
        }

        function HabilitarDesabilitarCamposEdicao(habilitar, podeConfirmarRecusar) {

            if (habilitar == true) {
                btnSalvar.show();
            } else {
                btnSalvar.hide();
            }

            /*
            Usuário pode confirmar/recusar quando:
            - Usuário é da Estação: confirma/recusa somente arquivos pendentes na estação
            - Usuario é da Central: confirma/recusa arquivos pendentes na central e se estiver
            acesso a uma determinada ação (da transação PAINELEXPEDICAOARQUIVOS), poderá também
            confirmar/recusar arquivos pendentes na estação
            */

            if (podeConfirmarRecusar == true) {
                if (grid.getStore().data.length > 0) {
                    btnConfirmar.show();
                    btnRecusar.show();
                } else {
                    btnConfirmar.hide();
                    btnRecusar.hide();
                }
            } else {
                btnConfirmar.hide();
                btnRecusar.hide();
            }
            
            btnLimparGrid.show();
        }

        function RedirecionarTela1154(notasNaoRegularizadas) {
            var menuLateral = new parent.Translogic.MenuLateral();
            menuLateral.abrirTelaPath(1154, false, true, "", "ConsultasNfe", 
                                        "RegularizarNotas", notasNaoRegularizadas, null);
        }
        

        function statusRenderer(status) {
            if (status == 0 || status == "0") {                
                return "<img src='/Get.efh?file={Translogic.Modules.Core;}/circle-red.png' alt='Com Erro'>";                
                
            }
            else {
                if (status == 1 || status == "1") {
                    return "<img src='/Get.efh?file={Translogic.Modules.Core;}/circle-yellow.png' alt='Pendente de Reprocessamento'>";   
                    
                } else {
                    return "<img src='/Get.efh?file={Translogic.Modules.Core;}/circle-green.png' alt='Reprocessada'>";
                }
            }
        }
        

        /* endregion :: Functions */

        /* region :: Stores */

        var clienteStore = new window.Ext.data.JsonStore({
            id: 'clienteStore',
            name: 'clienteStore',
            root: 'Items',
            url: '<%=Url.Action("ObterConfiguracoesClientes", "PainelExpedicao") %>',
            fields: [
                    'Id',
                    'Nome'
                ],
            listeners: {
                load: function (store) {
                    var rt = store.recordType;
                    store.insert(0, new rt({}));
                }
            }
        });
        window.clienteStore = clienteStore;

        var expedidorStore = new window.Ext.data.JsonStore({
            id: 'expedidorStore',
            name: 'expedidorStore',
            root: 'Items',
            url: '<%=Url.Action("ObterExpedidores", "PainelExpedicao") %>',
            fields: [
                    'DescResumida'
                ],
            listeners: {
                load: function (store) {
                    var rt = store.recordType;
                    store.insert(0, new rt({}));
                }
            }
        });
        window.expedidorStore = expedidorStore;

        var segmentosStore = new Ext.data.JsonStore({
            root: "Items",
            autoLoad: true,
            proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("ObterSegmentos", "PainelExpedicao") %>',
                timeout: 600000
            }),
            id: 'segmentosStore',
            fields: [
                'Id',
                'Nome'
            ]
        });

        var perfisArquivoStore = new Ext.data.ArrayStore({
               id: 2,
               fields: [
            'Id',
            'Descricao'
            ],
               data: [[1, 'Pátio'], [2, 'Central']]
           });

        /* endregion :: Stores */

        /* region :: Filtros */

        var dataCadastroBo = {
            xtype: 'datefield',
            fieldLabel: 'Data Início',
            id: 'dataCadastroBo',
            name: 'dataCadastroBo',
            width: 85,
            allowBlank: false,
            value: new Date()
        };

        var dataCadastroFinalBo = {
            xtype: 'datefield',
            fieldLabel: 'Data Fim',
            id: 'dataCadastroFinalBo',
            name: 'dataCadastroFinalBo',
            width: 85,
            allowBlank: false,
            value: new Date()
        };

        var txtFluxo = {
            xtype: 'textfield',
            name: 'txtFluxo',
            id: 'txtFluxo',
            fieldLabel: 'Fluxo',
            autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '7' },
            maskRe: /[a-zA-Z0-9]/,
            style: 'text-transform: uppercase',
            width: 85
        };

        var txtOrigem = {
            xtype: 'textfield',
            name: 'txtOrigem',
            id: 'txtOrigem',
            fieldLabel: 'Origem',
            autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '3' },
            maskRe: /[a-zA-Z]/,
            style: 'text-transform: uppercase',
            width: 50
        };

        var txtDestino = {
            xtype: 'textfield',
            name: 'txtDestino',
            id: 'txtDestino',
            fieldLabel: 'Destino',
            autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '3' },
            maskRe: /[a-zA-Z]/,
            style: 'text-transform: uppercase',
            width: 50
        };
        
        var txtSerie = {
            xtype: 'textfield',
            name: 'txtSerie',
            id: 'txtSerie',
            fieldLabel: 'Série',
            autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '3' },
            maskRe: /[a-zA-Z]/,
            style: 'text-transform: uppercase',
            width: 50
        };

        var txtVagoes = {
            xtype: 'textfield',
            name: 'txtVagoes',
            id: 'txtVagoes',
            fieldLabel: 'Vagão',
            maskRe: /[a-zA-Z0-9\;]/,
            maxLength: 100,
            width: 100,
            enableKeyEvents: true,
            listeners: {
                specialkey: function (f, e) {
                    if (e.getKey() == e.ENTER) {
                        Pesquisar();
                    }
                }
            }
        };

        var btnLoteVagao = {
            xtype: 'button',
            text: 'Informar Vagões',
            iconCls: 'icon-find',
            //style: 'padding: 18px 0px 0px 0px;',
            width: 160,
            handler: function (b, e) {
                windowStatusVagoes = new Ext.Window({
                                        id: 'windowStatusVagoes',
                                        title: 'Informar vagões',
                                        modal: true,
                                        width: 855,
                                        resizable: false,       
                                        height: 380,
                                        autoLoad: {
                                        url: '<%= Url.Action("FormInformarVagoes") %>',
                                        scripts: true
                                        }
                                     });
                windowStatusVagoes.show();
            }
        };

        var txtLocalAtual = {
            xtype: 'textfield',
            name: 'txtLocalAtual',
            id: 'txtLocalAtual',
            fieldLabel: 'Local Atual',
            autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '3' },
            maskRe: /[a-zA-Z]/,
            style: 'text-transform: uppercase',
            width: 70                                                                                                  
        };

        var ddlCliente = {
            xtype: 'combo',
            id: 'ddlCliente',
            name: 'ddlCliente',
            forceSelection: true,
            store: window.clienteStore,
            triggerAction: 'all',
            mode: 'remote',
            typeAhead: true,
            minChars: 3,
            fieldLabel: 'Cliente 363',
            displayField: 'Nome',
            valueField: 'Id',
            width: 175,
            editable: true,
            tpl: '<tpl for="."><div class="x-combo-list-item">{Nome}&nbsp;</div></tpl>'
        };
        
        var ddlExpedidor = {
            xtype: 'combo',
            id: 'ddlExpedidor',
            name: 'ddlExpedidor',
            forceSelection: true,
            store: window.expedidorStore,
            triggerAction: 'all',
            mode: 'remote',
            typeAhead: true,
            minChars: 1,
            fieldLabel: 'Expedidor',
            displayField: 'DescResumida',
            valueField: 'DescResumida',
            width: 100,
            editable: true,
            tpl: '<tpl for="."><div class="x-combo-list-item">{DescResumida}&nbsp;</div></tpl>'
        };

        var txtMercadorias = {
            xtype: 'textfield',
            name: 'txtMercadorias',
            id: 'txtMercadorias',
            fieldLabel: 'Lista de Mercadorias(separados por ";")',
            autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '100' },
            maskRe: /[a-zA-Z0-9\;]/,
            //maxLength: 500,
            width: 270,
            enableKeyEvents: true,
            listeners: {
                specialkey: function (f, e) {
                    if (e.getKey() == e.ENTER) {
                        Pesquisar();
                    }
                }
            }
        };

        var ddlSegmento = new Ext.form.ComboBox({
            typeAhead: false,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            store: segmentosStore,
            valueField: 'Id',
            width: 100,
            displayField: 'Nome',
            fieldLabel: 'Segmento',
            id: 'ddlSegmento'
        });

        var ddlPerfilArquivo = new Ext.form.ComboBox({
            typeAhead: false,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            store: perfisArquivoStore,
            valueField: 'Id',
            width: 100,
            displayField: 'Descricao',
            value: 2,
            fieldLabel: 'Visão',
            id: 'ddlPerfilArquivo'
        });

        var lblUsuario = {
            title: 'Usuário',
            html: '<%= ViewData["Usuario"] %>',
            autoHide: false,
            closable: true,
            draggable: false
        };

        var lblAviso = {
            title: 'Atenção : <%= ViewData["Usuario"] %>',
            html: 'Os botões Confirmar e Recusar registram o usuário responsável pela validação dos dados enviados pelo Cliente/Terminal.',
            autoHide: false,
            closable: false,
            draggable: false,
            width: 900,
            headerStyle: 'text-align: center; font-size: 14px;',
            bodyStyle: 'text-align: center; background-color: #F5A9A9; font-size: 12px;'
        };
        
        // Filtros Linha 1 //
        var arrDataIni = {
            width: 90,
            layout: 'form',
            border: false,
            items: [dataCadastroBo]
        };

        var arrDataFimBo = {
            width: 90,
            layout: 'form',
            border: false,
            items: [dataCadastroFinalBo]
        };

        var arrFluxo = {
            width: 90,
            layout: 'form',
            border: false,
            items: [txtFluxo]
        };

        var arrOrigem = {
            width: 55,
            layout: 'form',
            border: false,
            items: [txtOrigem]
        };
        
        var arrDestino = {
            width: 55,
            layout: 'form',
            border: false,
            items: [txtDestino]
        };
        
        var arrSerie = {
            width: 55,
            layout: 'form',
            border: false,
            items: [txtSerie]
        };

        var arrVagoes = {
            width: 105,
            layout: 'form',
            border: false,
            items: [txtVagoes]
        };

        var arrBtnLoteVagoes = {
            width: 165,
            layout: 'form',
            style: 'padding: 18px 0px 0px 0px;',
            border: false,
            items: [btnLoteVagao]
        };

        var arrLocalAtual = {
            width: 75,
            layout: 'form',
            border: false,
            //style: 'padding: 0px 0px 0px 30px;',
            items: [txtLocalAtual]
        };

        // Perfil Central de Faturamento: Filtro do perfil do arquivo na fila para faturamento (Pátio ou Central)
        if (perfilCentralFaturamento == true) {
            var arrPerfilArquivo = {
                width: 110,
                layout: 'form',
                border: false,
                items: [ddlPerfilArquivo]
            };
        } else {
            var arrPerfilArquivo = {
                width: 110,
                layout: 'form',
                border: false,
                items: []
            };
        }

        var arrlinha1 = {
            layout: 'column',
            border: false,
            width: 900,
            items: [
                arrDataIni,
                arrDataFimBo, 
                arrFluxo, 
                arrOrigem, 
                arrDestino, 
                arrSerie, 
                arrVagoes,  
                arrLocalAtual, 
                arrPerfilArquivo,
                arrBtnLoteVagoes
            ]
        };
        
        //------------------------------//

        // Filtros Linha 2 //

        var arrCliente = {
            width: 180,
            layout: 'form',
            border: false,
            items: [ddlCliente]
        };
        
        var arrExpedidor = {
            width: 110,
            layout: 'form',
            border: false,
            items: [ddlExpedidor]
        };

        var arrMercadoria = {
            width: 275,
            layout: 'form',
            border: false,
            items: [txtMercadorias]
        };

        var arrSegmento = {
            width: 110,
            layout: 'form',
            border: false,
            items: [ddlSegmento]
        };
        
        var btnPesquisar = {
            xtype: "button",
            name: 'btnPesquisar',
            id: 'btnPesquisar',
            text: 'Pesquisar',
            fieldLabel: '&nbsp;',
            labelSeparator: ' ',
            iconCls: 'icon-find',
            width: 75,
            handler: Pesquisar
        };
        
        var arrBtnPesquisar = {
            width: 110,
            layout: 'form',
            border: false,
            items: [btnPesquisar]
        };
    
        var arrAviso = {
            width: 900,
            layout: 'form',
            border: false,
            items: [lblAviso]
        };

        var btnLimpar = {
            xtype: "button",
            name: 'btnLimpar',
            id: 'btnLimpar',
            text: 'Limpar',
            fieldLabel: '&nbsp;',
            labelSeparator: ' ',
            width: 75,
            handler: Limpar
        };
        
        var arrBtnLimpar = {
            width: 90,
            layout: 'form',
            border: false,
            items: [btnLimpar]
        };

        var arrlinha2 = {
            layout: 'column',
            border: false,
            width: 900,
            items: [arrCliente, arrExpedidor, arrMercadoria, arrSegmento, arrBtnPesquisar, arrBtnLimpar]
        };
        
        var arrlinha3 = {
            layout: 'column',
            border: false,
            width: 900,
            items: [lblUsuario]
        };
        
        var arrlinha4 = {
            layout: 'column',
            border: false,
            width: 900,
            items: [arrAviso]
        };

        /* endregion :: Filtros*/

        /* region :: Grid */

        var btnSalvar = new Ext.Button({
            id: 'btnSalvar',
            name: 'btnSalvar',
            text: 'Salvar Alterações',
            iconCls: 'icon-save',
            handler: SalvarAlteracoes
        });
        btnSalvar.hide();
        
        var btnConfirmar = new Ext.Button({
            id: 'btnConfirmar',
            name: 'btnConfirmar',
            text: 'Confirmar Selecionados',
            iconCls: 'icon-save',
            handler: ConfirmarSelecionados
        });
        btnConfirmar.hide();
        
        var btnRecusar = new Ext.Button({
            id: 'btnRecusar',
            name: 'btnRecusar',
            text: 'Recusar Selecionados',
            iconCls: 'icon-save',
            handler: RecusarSelecionados
        });
        btnRecusar.hide();
        
        var btnLimparGrid = new Ext.Button({
            id: 'btnLimparGrid',
            name: 'btnLimparGrid',
            text: 'Limpar',
            iconCls: 'icon-save',
            handler: Limpar
        });
        btnLimparGrid.hide();

        var sm = new Ext.grid.CheckboxSelectionModel({
            listeners: {
                selectionchange: function (sm) {
                    var recLen = Ext.getCmp('grid').store.getRange().length;
                    var selectedLen = this.selections.items.length;
                    if (selectedLen == recLen) {
                        var view = Ext.getCmp('grid').getView();
                        var chkdiv = Ext.fly(view.innerHd).child(".x-grid3-hd-checker");
                        chkdiv.addClass("x-grid3-hd-checker-on");
                    }
                }
            },
            renderer: function (value, metaData, record, rowIndex) {
                var store = grid.getStore();
                var records = store.getRange();
                var idRegistro = record.data.Id;
                var idAgrupamentoMdRegistro = record.data.IdAgrupamentoMD;
                var idAgrupamentoSimplesRegistro = record.data.IdAgrupamentoSimples;
                var primeiroRegistro = null;

                if (idAgrupamentoMdRegistro) {
                    $.each(records, function(index, rec) {
                        var idAgrupamentoMd = rec.get('IdAgrupamentoMD');

                        if (idAgrupamentoMd == idAgrupamentoMdRegistro) {

                            if (primeiroRegistro == null) {
                                primeiroRegistro = (index == rowIndex) ? true : false;
                            }

                        }
                    });
                }
                else if (idAgrupamentoSimplesRegistro) {
                    $.each(records, function (index, rec) {
                        var idAgrupamentoSimples = rec.get('IdAgrupamentoSimples');

                        if (idAgrupamentoSimples == idAgrupamentoSimplesRegistro) {

                            if (primeiroRegistro == null) {
                                primeiroRegistro = (index == rowIndex) ? true : false;
                            }

                        }
                    });
                }
                
                if (primeiroRegistro == null || primeiroRegistro == true)
                    return '<div class="x-grid3-row-checker">&nbsp;</div>';
                else
                    return '';
            },
            rowdeselect: function (sm, rowIndex, record) {
                var view = Ext.getCmp('grid').getView();
                var chkdiv = Ext.fly(view.innerHd).child(".x-grid3-hd-checker");
                chkdiv.removeClass('x-grid3-hd-checker-on');
            }
        });

        var fm = Ext.form;

        var txtVagao = {
            xtype: 'textfield',
            style: 'text-transform: uppercase',
            autoCreate: { tag: 'input', type: 'text', maxlength: '7', autocomplete: 'off' },
            maskRe: /[0-9]/,
            width: 40,
            readOnly: false,
            name: 'txtVagao',
            allowBlank: false,
            enableKeyEvents: true,
            id: 'txtVagao'
        };

        var txtPesoTotal = {
            xtype: 'numberfield',
            id: 'txtPesoTotal',
            name: 'txtPesoTotal',
            decimalSeparator: ',',
            maxLength: 7,
            allowBlank: false,
            allowNegative: false,
            allowDecimals: true,
            decimalPrecision: 3,
            width: 30
        };
        
        var txtMdEditor = {
            xtype: 'textfield',
            style: 'text-transform: uppercase',
            autoCreate: { tag: 'input', type: 'text', maxlength: '7', autocomplete: 'off' },
            maskRe: /[a-zA-Z]/,
            width: 3,
            readOnly: false,
            name: 'txtMdEditor',
            allowBlank: false,
            id: 'txtMdEditor'
        };
        
        var txtFluxoEditor = {
            xtype: 'textfield',
            style: 'text-transform: uppercase',
            autoCreate: { tag: 'input', type: 'text', maxlength: '7', autocomplete: 'off' },
            maskRe: /[a-zA-Z0-9]/,
            width: 60,
            readOnly: false,
            name: 'txtFluxoEditor',
            allowBlank: false,
            id: 'txtFluxoEditor'
        };

        var txtConteiner = {
            xtype: 'textfield',
            style: 'text-transform: uppercase',
            autoCreate: { tag: 'input', type: 'text', maxlength: '11', autocomplete: 'off' },
            maskRe: /[a-zA-Z0-9]/,
            width: 60,
            readOnly: false,
            name: 'txtConteiner',
            allowBlank: false,
            enableKeyEvents: true,
            id: 'txtConteiner'
        };

        var txtObservacaoRecusado = {
            xtype: 'textfield',
            autoCreate: { tag: 'input', type: 'text', maxlength: '255', autocomplete: 'off' },
            width: 60,
            readOnly: false,
            name: 'txtObservacaoRecusado',
            allowBlank: true,
            enableKeyEvents: true,
            id: 'txtObservacaoRecusado'
        };

        var txtPesoRateioNfe = {
            xtype: 'numberfield',
            id: 'txtPesoRateioNfe',
            name: 'txtPesoRateioNfe',
            decimalSeparator: ',',
            maxLength: 7,
            allowBlank: false,
            allowNegative: false,
            allowDecimals: true,
            decimalPrecision: 3,
            width: 30
        };

        var cm = new Ext.grid.ColumnModel({
            defaults: {
                sortable: true
            },
            columns: [
                sm,
            {
                dataIndex: "IdAgrupamentoMD", hidden: true
            },
            {
                dataIndex: "IdAgrupamentoSimples", hidden: true
            },
            {
                header: 'Vagão', dataIndex: "Agrupador", hidden: true
            },
            {
                dataIndex: "Id", hidden: true
            },
            {
                dataIndex: "VagaoId", hidden: true
            },
            {
                header: 'Série',
                dataIndex: 'Serie',
                width: 50,
                align: "center",
                renderer: function (value, metaData, record, rowIndex,
                                    colIndex, store, view) {
                    var retorno = value;
                    var linha = rowIndex;

                    if (record.data.IdAgrupamentoMD) {

                        $.each(store.data.items, function (index, element) {
                            if (element.data.IdAgrupamentoMD == record.data.IdAgrupamentoMD) {

                                retorno = ((element.data.Id == record.data.Id) && (linha == index))
                                                    ? value : '';
                                return false;
                            }
                        });
                    }
                    else if (record.data.IdAgrupamentoSimples) {
                        $.each(store.data.items, function (index, element) {
                            if (element.data.IdAgrupamentoSimples == record.data.IdAgrupamentoSimples) {

                                retorno = ((element.data.Id == record.data.Id) && (linha == index))
                                                    ? value : '';
                                return false;
                            }
                        });
                    }

                    return retorno;
                }
            }, 
            {
                header: 'Vagão',
                dataIndex: 'Vagao',
                width: 60,
                editor: txtVagao,
                align: "center",
                renderer: function (value, metaData, record, rowIndex,
                                    colIndex, store, view) {
                    var retorno = value;
                    var linha = rowIndex;

                    if (record.data.IdAgrupamentoMD) {

                        $.each(store.data.items, function (index, element) {
                            if (element.data.IdAgrupamentoMD == record.data.IdAgrupamentoMD) {

                                retorno = ((element.data.Id == record.data.Id) && (linha == index))
                                                    ? value : '';
                                return false;
                            }
                        });
                    }
                    else if (record.data.IdAgrupamentoSimples) {
                        $.each(store.data.items, function (index, element) {
                            if (element.data.IdAgrupamentoSimples == record.data.IdAgrupamentoSimples) {

                                retorno = ((element.data.Id == record.data.Id) && (linha == index))
                                                    ? value : '';
                                return false;
                            }
                        });
                    }

                    return retorno;
                }
            },
            {
                header: 'Peso Total',
                dataIndex: 'PesoTotal',
                width: 80,
                align: 'right',
                sortable: false,
                renderer: function (value, metaData, record, rowIndex,
                                    colIndex, store, view) {
                    var retorno = value.toFixed(3).replace('.', ',');
                    var linha = rowIndex;

                    if (record.data.IdAgrupamentoMD) {
                        $.each(store.data.items, function (index, element) {
                            if (element.data.IdAgrupamentoMD == record.data.IdAgrupamentoMD) {
                                retorno = ((element.data.Id == record.data.Id) && (linha == index)) ? value.toFixed(3).replace('.', ',') : '';
                                return false;
                            }
                        });
                    }
                    else if (record.data.IdAgrupamentoSimples) {
                        $.each(store.data.items, function (index, element) {
                            if (element.data.IdAgrupamentoSimples == record.data.IdAgrupamentoSimples) {
                                retorno = ((element.data.Id == record.data.Id) && (linha == index)) ? value.toFixed(3).replace('.', ',') : '';
                                return false;
                            }
                        });
                    }

                    return retorno;
                }
            },
            {
                header: 'MD',
                dataIndex: 'MultiploDespacho',
                width: 40,
                align: "center",
                editor: txtMdEditor,
                renderer: function (value, metaData, record, rowIndex,
                                    colIndex, store, view) {
                    var retorno = value;
                    var linha = rowIndex;

                    var corSucesso = record.data.CorSucesso;
                    if (corSucesso === true) {
                        metaData.style += 'background-color:#ADFF2F;'; // Verde Claro
                    }
                    else {
                        metaData.style += 'background-color:#FF6347;'; // Vermelho Claro   
                    }

                    if (record.data.IdAgrupamentoMD) {

                        $.each(store.data.items, function (index, element) {
                            if (element.data.IdAgrupamentoMD == record.data.IdAgrupamentoMD) {

                                retorno = ((element.data.Id == record.data.Id) && (linha == index))
                                                    ? value : '';
                                return false;
                            }
                        });
                    }
                    else if (record.data.IdAgrupamentoSimples) {
                        $.each(store.data.items, function (index, element) {
                            if (element.data.IdAgrupamentoSimples == record.data.IdAgrupamentoSimples) {

                                retorno = ((element.data.Id == record.data.Id) && (linha == index))
                                                    ? value : '';
                                return false;
                            }
                        });
                    }

                    return retorno;
                }
            },
            {
                header: 'Fluxo',
                dataIndex: 'Fluxo',
                width: 65,
                align: "center",
                editor: txtFluxoEditor,
                renderer: function (value, metaData, record, rowIndex,
                                    colIndex, store, view) {
                    return value;
                }
            },
            {
                header: 'Origem',
                dataIndex: 'Origem',
                width: 50,
                align: "center",
                sortable: false,
                renderer: function (value, metaData, record, rowIndex,
                                    colIndex, store, view) {
                    return value;
                }
            },
            {
                header: 'Mercadoria',
                dataIndex: 'FluxoMercadoria',
                width: 100,
                align: "center",
                renderer: function (value, metaData, record, rowIndex,
                                    colIndex, store, view) {
                    return value;
                }
            },    
            {
                header: 'Recebedor',
                dataIndex: 'Recebedor',
                sortable: false,
                width: 120,
                align: "center",
                renderer: function (value, metaData, record, rowIndex,
                                    colIndex, store, view) {
                    return value;
                }
            },
            {
                header: 'Chave Nota',
                dataIndex: 'ChaveNota',
                sortable: false,
                width: 280,
                align: "center",
                renderer: function (value, metaData, record, rowIndex,
                                    colIndex, store, view) {
                    return value;
                }
            },
            {
                header: 'Peso Rateio',
                dataIndex: 'PesoRateioNFe',
                sortable: false,
                width: 80,
                align: 'right',
                editor: txtPesoRateioNfe,
                renderer: function (value, metaData, record, rowIndex,
                                    colIndex, store, view) {
                    if (value)
                        return value.toFixed(3).replace('.', ',');
                    else
                        return value;
                }
            },
            {
                header: 'Peso Nfe',
                dataIndex: 'PesoNFe',
                width: 70,
                align: 'right',
                sortable: false,
                renderer: function (value, metaData, record, rowIndex,
                                    colIndex, store, view) {

                    if (value != '')
                        return value.toFixed(3).replace('.', ',');
                    else
                        return value;
                }
            },
            {
               header: 'Expedidor',
               dataIndex: 'Expedidor',
               sortable: false,
               width: 120,
                align: "center",
               renderer: function (value, metaData, record, rowIndex,
                                   colIndex, store, view) {
                   return value;
               }
            },
            {
               header: 'Cliente 363',
               dataIndex: 'Cliente363',
               sortable: false,
               width: 120,
                align: "center",
               renderer: function (value, metaData, record, rowIndex,
                                   colIndex, store, view) {
                   return value;
               }
            },
            {
                header: 'Conteiner',
                dataIndex: 'Conteiner',
                width: 120,
                sortable: false,
                align: "center",
                editor: txtConteiner,
                renderer: function (value, metaData, record, rowIndex,
                                    colIndex, store, view) {

                    if (value)
                        return value.toUpperCase();
                    else
                        return value;
                }
            },
            {
                header: 'Observação',
                dataIndex: 'ObservacaoRecusado',
                width: 220,
                sortable: false,
                editor: txtObservacaoRecusado,
                renderer: function (value, metaData, record, rowIndex,
                                    colIndex, store, view) {
                    var retorno = value;
                    var linha = rowIndex;

                    if (record.data.IdAgrupamentoMD) {

                        $.each(store.data.items, function (index, element) {
                            if (element.data.IdAgrupamentoMD == record.data.IdAgrupamentoMD) {

                                retorno = ((element.data.Id == record.data.Id) && (linha == index))
                                                    ? value : '';
                                return false;
                            }
                        });
                    }
                    else if (record.data.IdAgrupamentoSimples) {
                        $.each(store.data.items, function (index, element) {
                            if (element.data.IdAgrupamentoSimples == record.data.IdAgrupamentoSimples) {

                                retorno = ((element.data.Id == record.data.Id) && (linha == index))
                                                    ? value : '';
                                return false;
                            }
                        });
                    }

                    return retorno;
                }
            }
        ]
        });

        // Data Source/Store da Grid
        var gridStore = new Ext.data.GroupingStore({
            id: 'gridStore',
            url: '<%= Url.Action("ObterPainelExpedicaoConferenciaArquivos", "PainelExpedicao") %>',
            autoLoad: false,
            groupField: ['Agrupador'],

            sortInfo: {
                field: 'Fluxo',
                direction: 'ASC'
            },

            reader: new Ext.data.JsonReader({
                totalProperty: 'Total',
                root: 'Items',
                id: 'gridStoreReader',
                fields: [
                    'IdAgrupamentoMD',
                    'IdAgrupamentoSimples',
                    'Agrupador',
                    'Id',
                    'VagaoId',
                    'Serie',
                    'Vagao',
                    'MultiploDespacho', 
                    'LiberacaoRegistroMultiploDespacho',
                    'PesoTotal',
                    'Fluxo',
                    'Origem',
                    'Recebedor',
                    'Expedidor',
                    'Cliente363',
                    'ChaveNota',
                    'PesoNFe',
                    'PesoRateioNFe',
                    'Conteiner',
                    'FluxoSegmento',
                    'FluxoMercadoria',
                    'ObservacaoRecusado',
                    'PodeAlterar',
                    'PodeConfirmarRecusar',
                    'CorSucesso'
                ]
            })
            
        });

        // Toolbar da Grid
        var pagingToolbar = new Ext.PagingToolbar({
            pageSize: 50,
            store: gridStore,
            displayInfo: true,
            displayMsg: App.Resources.Web.GridExibindoRegistros,
            emptyMsg: App.Resources.Web.GridSemRegistros,
            paramNames: {
                start: "pagination.Start",
                limit: "pagination.Limit"
            }
        });

        // Grid paginada sem reaproveitar a classe Translogic.PaginatedEditorGrid

        //grid = new Ext.grid.EditorGridPanel({
        grid = new Ext.grid.EditorGridPanel({
            autoLoadGrid: false,
            id: "grid",
            store: gridStore,
            stripeRows: true,
            cm: cm,
            clicksToEdit: 1,
            region: 'center',
            //width: '100%',
            //width: 1065,
            height: 280,
            //autoScroll: true,
            //height: 330,
            loadMask: { msg: App.Resources.Web.Carregando },
            tbar: [btnSalvar, btnConfirmar, btnRecusar, btnLimparGrid],
            sm: sm,
            bbar: pagingToolbar,
            viewConfig: {
                emptyText: 'Não possui dado(s) para exibição.'
            },
            view: new Ext.grid.GroupingView({
                startCollapsed: true,
                groupTextTpl: '({text})'
            }),
            listeners: {
                render: function (gridData) {
                    gridData.store.on('load', function (store, records, options) {
                        $.each(store.data.items, function (index, rec) {
                            podeAlterarRegistrosPerfilCentral = rec.get('PodeAlterar');
                            podeConfirmarRecusar = rec.get('PodeConfirmarRecusar');
                            return false;
                        });

                        HabilitarDesabilitarCamposEdicao(podeAlterarRegistrosPerfilCentral, podeConfirmarRecusar);
                    });
                },
                beforeedit: function (gridData) {
                    var fldname = gridData.field;
                    var store = gridData.grid.getStore();
                    var rec = gridData.record;
                    var linhaEdicao = gridData.row;

                    var retorno = true;

                    if (fldname == "Vagao") {

                        if (podeAlterarRegistrosPerfilCentral == false)
                            return false;

                        if (rec.data.IdAgrupamentoMD) {
                            $.each(store.data.items, function (index, element) {
                                if (element.data.IdAgrupamentoMD == rec.data.IdAgrupamentoMD) {
                                    retorno = ((element.data.Id == rec.data.Id) && (linhaEdicao == index)) ? true : false;
                                    return false;
                                }
                            });
                        }
                        else if (rec.data.IdAgrupamentoSimples) {
                            $.each(store.data.items, function (index, element) {
                                if (element.data.IdAgrupamentoSimples == rec.data.IdAgrupamentoSimples) {
                                    retorno = ((element.data.Id == rec.data.Id) && (linhaEdicao == index)) ? true : false;
                                    return false;
                                }
                            });
                        }

                        return retorno;
                    }
                    
                    if (fldname == "MultiploDespacho") {
                        /* Apenas perfil Central pode alterar o código do fluxo */
                        if (podeAlterarRegistrosPerfilCentral == false)
                            return false;

                        if (rec.data.IdAgrupamentoMD) {
                            $.each(store.data.items, function (index, element) {
                                if (element.data.IdAgrupamentoMD == rec.data.IdAgrupamentoMD) {
                                    retorno = ((element.data.Id == rec.data.Id) && (linhaEdicao == index)) ? true : false;
                                    return false;
                                }
                            });
                        }
                        else if (rec.data.IdAgrupamentoSimples) {
                            $.each(store.data.items, function (index, element) {
                                if (element.data.IdAgrupamentoSimples == rec.data.IdAgrupamentoSimples) {
                                    retorno = ((element.data.Id == rec.data.Id) && (linhaEdicao == index)) ? true : false;
                                    return false;
                                }
                            });
                        }

                        return retorno;
                    }   
                    
                    if (fldname == "Fluxo") {

                        /* Apenas perfil Central pode alterar o código do fluxo */
                        if (podeAlterarRegistrosPerfilCentral == false)
                            return false;

                        return true;
                    }

                    if (fldname == "PesoRateioNFe") {
                        if (podeAlterarRegistrosPerfilCentral == false)
                            return false;
                        else
                            return true;
                    }

                    if (fldname == "ObservacaoRecusado") {
                        
                        // Se for perfil pátio, habilita a observação (caso o registro seja recusado)
                        if (podeAlterarRegistrosPerfilCentral == true)
                            return false;
                        //else
                            //return false;

                        if (rec.data.IdAgrupamentoMD) {
                            $.each(store.data.items, function (index, element) {
                                if (element.data.IdAgrupamentoMD == rec.data.IdAgrupamentoMD) {
                                    retorno = ((element.data.Id == rec.data.Id) && (linhaEdicao == index)) ? true : false;
                                    return false;
                                }
                            });
                        }
                        else if (rec.data.IdAgrupamentoSimples) {
                            $.each(store.data.items, function (index, element) {
                                if (element.data.IdAgrupamentoSimples == rec.data.IdAgrupamentoSimples) {
                                    retorno = ((element.data.Id == rec.data.Id) && (linhaEdicao == index)) ? true : false;
                                    return false;
                                }
                            });
                        }

                        return retorno;
                    }

                    if (fldname == "Conteiner") {

                        if (podeAlterarRegistrosPerfilCentral == false)
                            return false;

                        var record = gridData.record;
                        if ((record.data.FluxoSegmento == "BRADO") ||
                           (record.data.FluxoSegmento == "INDUSTR" && record.data.FluxoMercadoria.Contains("PALLET"))) {
                            return true;
                        }
                        else {
                            return false;
                        }
                    }
                },
                afteredit: function (gridData) {
                    var fldname = gridData.field;
                    var record = gridData.record;
                    var gridView = this.getView();
                    var idAgrupamentoMd = record.data.IdAgrupamentoMD;
                    var idAgrupamentoSimples = record.data.IdAgrupamentoSimples;
                    var idBo = record.data.Id;
                    var conteiner = record.data.Conteiner;
                    var fluxo = record.data.Fluxo;
                    
                    if (fldname == "Vagao") {
                        var vagaoAnterior = gridData.originalValue;
                        var vagaoNovo = gridData.value;
                        var sMsgVagaoInvalido = VerificaVagaoValido(vagaoNovo, idAgrupamentoMd, idBo, conteiner, idAgrupamentoSimples);
                        var bVagaoInvalido = false;
                        if (sMsgVagaoInvalido != '')
                            bVagaoInvalido = true;

                        if (vagaoAnterior != vagaoNovo) {

                            if (bVagaoInvalido == false) {
                                if (idAgrupamentoMd) {
                                    gridData.grid.store.data.each(function (rec) {
                                        var recIdAgrupamentoMd = rec.data.IdAgrupamentoMD;

                                        if (recIdAgrupamentoMd == idAgrupamentoMd) {
                                            rec.data.Vagao = vagaoNovo;
                                            gridView.refresh();
                                        }
                                    });
                                }
                                else if (idAgrupamentoSimples) {
                                    gridData.grid.store.data.each(function (rec) {
                                        var recIdAgrupamentoSimples = rec.data.IdAgrupamentoSimples;

                                        if (recIdAgrupamentoSimples == idAgrupamentoSimples) {
                                            rec.data.Vagao = vagaoNovo;
                                            gridView.refresh();
                                        }
                                    });
                                }   
                            }
                        }

                        if (bVagaoInvalido == true) {

                            gridData.record.data.Vagao = vagaoAnterior;
                            this.getView().refresh();

                            Ext.Msg.show({
                                title: "Vagão inválido",
                                msg: sMsgVagaoInvalido,
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.ERROR,
                                minWidth: 200
                            });
                        }
                        else {
                            this.getView().refresh();
                        }

                    }
                    
                    if (fldname == "MultiploDespacho") {
                        var mdNovo = gridData.value;
                        var mdAnterior = gridData.originalValue;

                        if (mdNovo != "") {
                            mdNovo = mdNovo.toUpperCase();
                            console.log(mdNovo);

                            if (mdNovo == "S" || mdNovo == "SIM" || mdNovo == "YES") {
                                mdNovo = "SIM";
                            }
                            else if (mdNovo == "N" || mdNovo == "NAO" || mdNovo == "NÃO" || mdNovo == "NO") {
                                mdNovo = "NAO";
                            }
                            else {
                                mdNovo = mdAnterior.toUpperCase();
                            }
                            
                            if (idAgrupamentoMd) {
                                gridData.grid.store.data.each(function(rec) {
                                    var recIdAgrupamentoMd = rec.data.IdAgrupamentoMD;
                                    if (recIdAgrupamentoMd == idAgrupamentoMd) {
                                        var recIdBo = rec.data.Id;
                                        if (recIdBo == idBo) {
                                            rec.data.MultiploDespacho = mdNovo;
                                        }
                                    }
                                });
                            } else if (idAgrupamentoSimples) {
                                gridData.grid.store.data.each(function(rec) {
                                    var recIdAgrupamentoSimples = rec.data.IdAgrupamentoSimples;
                                    var recIdBo = rec.data.Id;
                                    if (recIdAgrupamentoSimples == idAgrupamentoSimples && recIdBo == idBo) {
                                        rec.data.MultiploDespacho = mdNovo;
                                        
                                    }
                                });
                            }
                            
                            gridView.refresh();
                        }
                    }

                    if (fldname == "Fluxo") {
                        var fluxoNovo = gridData.value;

                        if (fluxo != "") {
                            var fluxoExpr = /^[a-zA-Z]{2}\d{5,6}$/;
                            var resultadoFluxoValido = fluxoExpr.test(fluxoNovo);
                            if (resultadoFluxoValido == false) {
                                gridData.record.data.Fluxo = fluxo.toUpperCase();
                                this.getView().refresh();
                                Ext.Msg.show({
                                    title: "Fluxo",
                                    msg: "O Fluxo informado está em um formato inv&aacute;lido. Por favor, coloque os caracteres iniciais e os d&iacute;gitos.",
                                    buttons: Ext.Msg.OK,
                                    icon: Ext.MessageBox.ERROR,
                                    minWidth: 200
                                });
                            } else {
                                if (idAgrupamentoMd) {
                                    var atualizaFluxo = true;
                                    gridData.grid.store.data.each(function(rec) {
                                        var recIdAgrupamentoMd = rec.data.IdAgrupamentoMD;
                                        if (recIdAgrupamentoMd == idAgrupamentoMd) {
                                            var recIdBo = rec.data.Id;
                                            if (recIdBo != idBo) {
                                                var fluxoOutroBo = rec.data.Fluxo;
                                                if (fluxoOutroBo == fluxoNovo.toUpperCase()) {
                                                    atualizaFluxo = false;
                                                }
                                            }
                                        }
                                    });
                                    
                                    if (atualizaFluxo === true) {
                                        gridData.grid.store.data.each(function(rec) {
                                            var recIdAgrupamentoMd = rec.data.IdAgrupamentoMD;
                                            if (recIdAgrupamentoMd == idAgrupamentoMd) {
                                                var recIdBo = rec.data.Id;
                                                if (recIdBo == idBo) {
                                                    rec.data.Fluxo = fluxoNovo.toUpperCase();
                                                }
                                            }
                                        });
                                        gridView.refresh();
                                    }
                                    else {
                                        var fluxoAnterior = gridData.originalValue;
                                        gridData.record.data.Fluxo = fluxoAnterior.toUpperCase();
                                        this.getView().refresh();
                                        Ext.Msg.show({
                                            title: "Fluxo",
                                            msg: "O Fluxo informado j&aacute; est&aacute; neste m&uacute;ltiplo despacho.",
                                            buttons: Ext.Msg.OK,
                                            icon: Ext.MessageBox.ERROR,
                                            minWidth: 200
                                        });
                                    }
                                } else if (idAgrupamentoSimples) {
                                    gridData.grid.store.data.each(function(rec) {
                                        var recIdAgrupamentoSimples = rec.data.IdAgrupamentoSimples;

                                        if (recIdAgrupamentoSimples == idAgrupamentoSimples) {
                                            rec.data.Fluxo = fluxoNovo.toUpperCase();
                                            gridView.refresh();
                                        }
                                    });
                                }
                            }
                        }
                    }
                    
                    if (fldname == "ObservacaoRecusado") {
                        var observacaoRecusadoNovo = gridData.value;
                        
                        if (idAgrupamentoMd) {
                            gridData.grid.store.data.each(function (rec) {
                                var recIdAgrupamentoMd = rec.data.IdAgrupamentoMD;

                                if (recIdAgrupamentoMd == idAgrupamentoMd) {
                                    rec.data.ObservacaoRecusado = observacaoRecusadoNovo;
                                    gridView.refresh();
                                }
                            });
                        }
                        else if (idAgrupamentoSimples) {
                            gridData.grid.store.data.each(function (rec) {
                                var recIdAgrupamentoSimples = rec.data.IdAgrupamentoSimples;

                                if (recIdAgrupamentoSimples == idAgrupamentoSimples) {
                                    rec.data.ObservacaoRecusado = observacaoRecusadoNovo;
                                    gridView.refresh();
                                }
                            });
                        }
                        
                    }

                    if (fldname == "Conteiner") {
                        var conteinerAnterior = gridData.originalValue;
                        var conteinerNovo = gridData.value;
                        var vagao = record.data.Vagao;
                        var bCancelaConteinerNovo = false;

                        if (conteiner != "") {
                            var conteinerExpr = /^[a-zA-Z]{4}\d{7}$/;
                            var res = conteinerExpr.test(conteiner);
                            if (res == false) {
                                gridData.record.data.Conteiner = conteiner.toUpperCase();
                                this.getView().refresh();
                                Ext.Msg.show({
                                    title: "Conteiner",
                                    msg: "O Conteiner informado está em um formato inv&aacute;lido.",
                                    buttons: Ext.Msg.OK,
                                    icon: Ext.MessageBox.ERROR,
                                    minWidth: 200
                                });
                            }
                            else {

                                conteinerAnterior = conteinerAnterior.toUpperCase();
                                conteinerNovo = conteinerNovo.toUpperCase();
                                //  Se o novo conteiner ja estiver em outro vagão do mesmo dia, nao permitir
                                if (conteinerAnterior != conteinerNovo) {
                                    gridData.grid.store.data.each(function (rec) {
                                        var recVagao = rec.data.Vagao;
                                        var recConteiner = rec.data.Conteiner;

                                        if ((recVagao != vagao) &&
                                            (recConteiner == conteinerNovo) &&
                                            (recConteiner != '')) {

                                            bCancelaConteinerNovo = true;
                                            Ext.Msg.show({
                                                title: "Conteiner inválido",
                                                msg: "O conteiner informado já existe em outro vagão no dia informado!",
                                                buttons: Ext.Msg.OK,
                                                icon: Ext.MessageBox.ERROR,
                                                minWidth: 200
                                            });

                                        }
                                    });
                                }

                                if (bCancelaConteinerNovo)
                                    gridData.record.data.Conteiner = conteinerAnterior;
                                else
                                    gridData.record.data.Conteiner = conteiner.toUpperCase();

                                this.getView().refresh();
                            }
                        }
                    }

                    if (fldname == "PesoRateioNFe") {
                        var pesoRateioNfe = gridData.value;
                        var somaPesoRateio = pesoRateioNfe;

                        var pesoRateioNfeAnterior = gridData.originalValue;
                        var pesoNfe = record.data.PesoNFe;
                        //var self = this;

                        if (pesoRateioNfe > pesoNfe) {

                            gridData.record.data.PesoRateioNFe = pesoRateioNfeAnterior;
                            gridData.value = pesoRateioNfeAnterior;
                            record.data.PesoRateioNFe = pesoRateioNfeAnterior;
                            record.dirty = false;
                            this.getView().refresh();

                            Ext.Msg.show({
                                title: "Peso rateio inválido",
                                msg: 'Peso de rateio informado é maior que o valor da nota fiscal.',
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.ERROR,
                                minWidth: 200
                            });

                            return false;
                        }

                        if (idAgrupamentoMd) {
                            gridData.grid.store.data.each(function (rec) {
                                var recIdAgrupamentoMd = rec.data.IdAgrupamentoMD;

                                if (recIdAgrupamentoMd == idAgrupamentoMd) {
                                    somaPesoRateio = somaPesoRateio + rec.data.PesoRateioNFe;
                                }
                            });

                            gridData.grid.store.data.each(function (rec) {
                                var recIdAgrupamentoMd = rec.data.IdAgrupamentoMD;

                                if (recIdAgrupamentoMd == idAgrupamentoMd) {
                                    rec.data.PesoTotal = (somaPesoRateio - pesoRateioNfe);
                                    gridView.refresh();
                                }
                            });

                            this.getView().refresh();
                        }
                        else if (idAgrupamentoSimples) {
                            gridData.grid.store.data.each(function (rec) {
                                var recIdAgrupamentoSimples = rec.data.IdAgrupamentoSimples;

                                if (recIdAgrupamentoSimples == idAgrupamentoSimples) {
                                    somaPesoRateio = somaPesoRateio + rec.data.PesoRateioNFe;
                                }
                            });

                            gridData.grid.store.data.each(function (rec) {
                                var recIdAgrupamentoSimples = rec.data.IdAgrupamentoSimples;

                                if (recIdAgrupamentoSimples == idAgrupamentoSimples) {
                                    rec.data.PesoTotal = (somaPesoRateio - pesoRateioNfe);
                                    gridView.refresh();
                                }
                            });

                            this.getView().refresh();
                        }
                        
                        if ((!idAgrupamentoMd) && (!idAgrupamentoSimples)) {
                            gridData.record.data.PesoTotal = pesoRateioNfe;
                            this.getView().refresh();
                        }
                    }
                }
            }
        });


        grid.getStore().proxy.on('beforeload', function (p, params) {
            var ddlCliente = Ext.getCmp("ddlCliente");
            var clienteId = ddlCliente.getValue();

            var ddlExpedidor = Ext.getCmp("ddlExpedidor");
            var expedidorId = ddlExpedidor.getValue();

            var ddlSegmento = Ext.getCmp("ddlSegmento");
            var segmentoId = ddlSegmento.getValue();

            var dataCadastroBo = Ext.getCmp("dataCadastroBo").getValue();
            var dataCadastroFinalBo = Ext.getCmp("dataCadastroFinalBo").getValue();

            var arquivosPatio = "S";
            if (perfilCentralFaturamento == true) {
                var ddlPerfilArquivo = Ext.getCmp("ddlPerfilArquivo");
                var perfilId = ddlPerfilArquivo.getValue();
                if (perfilId !== 1) {
                    arquivosPatio = "N";
                }
            }

            params['dataCadastroBo'] = dataCadastroBo.format('d/m/Y');
            params['dataCadastroFinalBo'] = dataCadastroFinalBo.format('d/m/Y');
            params['fluxo'] = Ext.getCmp("txtFluxo").getValue();
            params['origem'] = Ext.getCmp("txtOrigem").getValue();
            params['destino'] = Ext.getCmp("txtDestino").getValue();
            params['serie'] = Ext.getCmp("txtSerie").getValue();
            
            var vagoes = RetornaVagoes();
            params['vagoes'] = vagoes;
            
            params['localAtual'] = Ext.getCmp("txtLocalAtual").getValue();
            params['cliente'] = clienteId;
            params['expedidor'] = expedidorId;
            params['mercadorias'] = Ext.getCmp("txtMercadorias").getValue();
            params['segmento'] = segmentoId;
            params['arquivosPatio'] = arquivosPatio;
        });

        /* endregion :: Grid */

        /************************* RENDER **************************/
        var campoVazio = {
            xtype: 'component',
            fieldLabel: '&nbsp;',
            labelSeparator: ' ',
            width: 80
        };
        
        var arrVazio = {
            width: 110,
            layout: 'form',
            border: false,
            items: [campoVazio]
        };
        
        var arrlinha5 = {
            layout: 'column',
            border: false,
            width: 900,
            items: [arrVazio]
        };
        
        var arrCampos = new Array();
        arrCampos.push(arrlinha4);
        arrCampos.push(arrlinha5);
        arrCampos.push(arrlinha1);
        arrCampos.push(arrlinha2);
        //arrCampos.push(arrlinha3);
        
        arrCampos.push(arrlinha5);
        arrCampos.push(grid);
        arrCampos.push(arrlinha5);

        var filtros = new Ext.form.FormPanel({
            id: 'filtros',
            title: 'Filtros',
            name: 'filtros',
            region: 'center',
            bodyStyle: 'padding: 15px',
            initEl: function (el) {
                this.el = Ext.get(el);
                this.id = this.el.id || Ext.id();
                if (this.standardSubmit) {
                    this.el.dom.action = this.url;
                } else {
                    this.el.on('submit', this.onSubmit, this);
                }
                this.el.addClass('x-form');
            },
            //layout: 'form',
            labelAlign: 'top',
            border: false,
            items: [arrCampos] //[linha],
            });
        
        var panelErrosEdi = null;
        if (perfilCentralFaturamento == true) {
            
            // Habilita a Aba EDI Erros e os campos pertencentes a esta Aba 
            var dataInicialEdiErro = {
                xtype: 'datefield',
                fieldLabel: 'Data Início',
                id: 'dataInicialEdiErro',
                name: 'dataInicialEdiErro',
                width: 83,
                allowBlank: false,
                value: new Date()
            };
            
            var dataFinalEdiErro = {
                xtype: 'datefield',
                fieldLabel: 'Data Fim',
                id: 'dataFinalEdiErro',
                name: 'dataFinalEdiErro',
                width: 83,
                allowBlank: false,
                value: new Date()
            };

            var txtFluxoEdiErro = {
                xtype: 'textfield',
                name: 'txtFluxoEdiErro',
                id: 'txtFluxoEdiErro',
                fieldLabel: 'Fluxo',
                autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '7' },
                maskRe: /[a-zA-Z0-9]/,
                style: 'text-transform: uppercase',
                width: 70
            };

            var txtVagoesEdiErro = {
                xtype: 'textfield',
                name: 'txtVagoesEdiErro',
                id: 'txtVagoesEdiErro',
                fieldLabel: 'Lista de Vagões(separados por ";")',
                autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '100' },
                //maxLength: 500,
                maskRe: /[a-zA-Z0-9\;]/,
                width: 260,
                enableKeyEvents: true,
                listeners: {
                    specialkey: function (f, e) {
                        if (e.getKey() == e.ENTER) {
                            Pesquisar();
                        }
                    }
                }
            };
            

            var txtResponsavelEnvioEdiErro = {
                xtype: 'textfield',
                name: 'txtResponsavelEnvioEdiErro',
                id: 'txtResponsavelEnvioEdiErro',
                fieldLabel: 'Cliente',
                autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '100' },
                maskRe: /[a-zA-Z0-9]/,
                style: 'text-transform: uppercase',
                width: 200
            };
            

            var arrDataIniEdiErro = {
	            width: 87,
	            layout: 'form',
	            border: false,
	            items: [dataInicialEdiErro]
            };
            
            var arrDataFimEdiErro = {
	            width: 87,
	            layout: 'form',
	            border: false,
	            items: [dataFinalEdiErro]
            };

            var arrFluxoEdiErro = {
	            width: 80,
	            layout: 'form',
	            border: false,
	            items: [txtFluxoEdiErro]
            };

            var arrVagoesEdiErro = {
	            width: 280,
	            layout: 'form',
	            border: false,
	            items: [txtVagoesEdiErro]
            };
            
            var arrResponsavelEnvioEdiErro = {
	            width: 220,
	            layout: 'form',
	            border: false,
	            items: [txtResponsavelEnvioEdiErro]
            };
            

            var btnPesquisarEdiErro = {
                xtype: "button",
                name: 'btnPesquisarEdiErro',
                id: 'btnPesquisarEdiErro',
                text: 'Pesquisar',
                fieldLabel: '&nbsp;',
                labelSeparator: ' ',
                iconCls: 'icon-find',
                width: 75,
                handler: PesquisarEdiErro
            };
        
            var arrBtnPesquisarEdiErro = {
                width: 90,
                layout: 'form',
                border: false,
                items: [btnPesquisarEdiErro]
            };
    
            var btnLimparEdiErro = {
                xtype: "button",
                name: 'btnLimparEdiErro',
                id: 'btnLimparEdiErro',
                text: 'Limpar',
                fieldLabel: '&nbsp;',
                labelSeparator: ' ',
                width: 75,
                handler: LimparEdiErro
            };
        
            var arrBtnLimparEdiErro = {
                width: 90,
                layout: 'form',
                border: false,
                items: [btnLimparEdiErro]
            };
            
            var arrlinha1EdiErro = {
	            layout: 'column',
	            border: false,
	            width: 1200,
	            items: [arrDataIniEdiErro, arrDataFimEdiErro, arrFluxoEdiErro, 
	                    arrVagoesEdiErro, arrResponsavelEnvioEdiErro, 
	                    arrBtnPesquisarEdiErro, arrBtnLimparEdiErro]
            };
            
            
            // Grid
            var btnRegularizarNota = new Ext.Button({
                id: 'btnRegularizarNota',
                name: 'btnRegularizarNota',
                text: 'Baixar Nfes',
                iconCls: 'icon-response-attend',
                handler: RegularizarNotasSelecionadas
            });
            btnRegularizarNota.hide();
            
            var btnReprocessarNota = new Ext.Button({
                id: 'btnReprocessarNota',
                name: 'btnReprocessarNota',
                text: 'Reprocessar',
                iconCls: 'icon-arrow_refresh',
                handler: ReprocessarNotasSelecionadas
            });
            btnReprocessarNota.hide();

            // Data Source/Store da Grid Edi Erro
            var gridEdiErroStore = new Ext.data.GroupingStore({
                id: 'gridEdiErroStore',
                //url: '<%= Url.Action("ObterEdiErrosPesquisa", "EdiErro") %>',
                proxy:  new Ext.data.HttpProxy(
                    new Ext.data.Connection({
                        url: '<%= Url.Action("ObterEdiErrosPesquisa", "EdiErro") %>',
                        timeout: 900000 })), //15 minutos em milesegundos
                autoLoad: false,
                groupField: ['Agrupador'],

                sortInfo: {
                    field: 'ResponsavelEnvio',
                    direction: 'ASC'
                },

                reader: new Ext.data.JsonReader({
                    totalProperty: 'Total',
                    root: 'Items',
                    id: 'gridStoreReader',
                    fields: [
                        'IdMensagemRecebimento',
                        'IdErroEdiNfe',
                        'Agrupador',
                        'Vagao',
                        'Fluxo',
                        'DataErro',
                        'DataEnvio',
                        'ResponsavelEnvio',
                        'ChaveNfe',
                        'DataRegularizacao',
                        'Status'
                    ]
                })
            
            });
        
            var smEdiErro = new Ext.grid.CheckboxSelectionModel({
                listeners: {
                    selectionchange: function (sm) {
                        var recLen = Ext.getCmp('gridEdiErro').store.getRange().length;
                        var selectedLen = this.selections.items.length;
                        if (selectedLen == recLen) {
                            var view = Ext.getCmp('gridEdiErro').getView();
                            var chkdiv = Ext.fly(view.innerHd).child(".x-grid3-hd-checker");
                            chkdiv.addClass("x-grid3-hd-checker-on");
                        }
                    }
                },
                rowdeselect: function (sm, rowIndex, record) {
                    var view = Ext.getCmp('gridEdiErro').getView();
                    var chkdiv = Ext.fly(view.innerHd).child(".x-grid3-hd-checker");
                    chkdiv.removeClass('x-grid3-hd-checker-on');
                }
            });
            

            var cmEdiErro = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true
                },
                columns: [
                    smEdiErro,
                {
                    dataIndex: "IdMensagemRecebimento", hidden: true
                },
                {
                    dataIndex: "IdErroEdiNfe", hidden: true
                },
                {
                    header: 'Item', dataIndex: "Agrupador", hidden: true
                },
                {
                    header: 'Status',
                    dataIndex: 'Status',
                    width: 35,
                    align: "center",
                    sortable: false, 
                    renderer: statusRenderer
                },
                {
                    header: 'Data Erro',
                    dataIndex: 'DataErro',
                    width: 100
                },
                {
                    header: 'Remetente',
                    dataIndex: 'ResponsavelEnvio',
                    width: 100
                },
                {
                    header: 'Vagão',
                    dataIndex: 'Vagao',
                    width: 100
                },
                {
                    header: 'Fluxo',
                    dataIndex: 'Fluxo',
                    width: 100,
                    renderer: function (value, metaData, record, rowIndex,
                                        colIndex, store, view) {
                        return value;
                    }
                },
                {
                    header: 'Chave Nota',
                    dataIndex: 'ChaveNfe',
                    sortable: false,
                    width: 280,
                    renderer: function (value, metaData, record, rowIndex,
                                        colIndex, store, view) {
                        return value;
                    }
                }
            ]
            });

            // Toolbar da Grid
            var pagingToolbarEdiErro = new Ext.PagingToolbar({
                pageSize: 50,
                store: gridEdiErroStore,
                displayInfo: true,
                displayMsg: App.Resources.Web.GridExibindoRegistros,
                emptyMsg: App.Resources.Web.GridSemRegistros,
                paramNames: {
                    start: "pagination.Start",
                    limit: "pagination.Limit"
                }
            });
            

            gridEdiErro = new Ext.grid.GridPanel({
                autoLoadGrid: false,
                id: "gridEdiErro",
                store: gridEdiErroStore,
                stripeRows: true,
                cm: cmEdiErro,
                clicksToEdit: 1,
                region: 'center',
                height: 400,
                loadMask: { msg: App.Resources.Web.Carregando },
                tbar: [btnRegularizarNota, btnReprocessarNota],
                sm: smEdiErro,
                bbar: pagingToolbarEdiErro,
                viewConfig: {
                    forceFit: true,
                    emptyText: 'Não possui dado(s) para exibição.'
                },
                view: new Ext.grid.GroupingView({
                    startCollapsed: true,
                    forceFit: true,
                    groupTextTpl: '({text})'
                }),
                listeners: {
                    render: function (gridData) {
                        gridData.store.on('load', function (store, records, options) {
                            /*$.each(store.data.items, function (index, rec) {
                                podeAlterarRegistrosPerfilCentral = rec.get('PodeAlterar');
                                return false;
                            });*/
                            btnRegularizarNota.show();
                            btnReprocessarNota.show();
                        });
                        //btnRegularizarNota.show();
                    }
                }
            });


            gridEdiErro.getStore().proxy.on('beforeload', function (p, params) {

                var dataInicialEdiErro = Ext.getCmp("dataInicialEdiErro").getValue();
                var dataFinalEdiErro = Ext.getCmp("dataFinalEdiErro").getValue();

                params['dataInicial'] = dataInicialEdiErro.format('d/m/Y');
                params['dataFinal'] = dataFinalEdiErro.format('d/m/Y');
                params['fluxo'] = Ext.getCmp("txtFluxoEdiErro").getValue();
                params['vagoes'] = Ext.getCmp("txtVagoesEdiErro").getValue();
                params['responsavelEnvio'] = Ext.getCmp("txtResponsavelEnvioEdiErro").getValue();
            });

            var arrCamposEdiErro = new Array();
            arrCamposEdiErro.push(arrlinha1EdiErro);
            arrCamposEdiErro.push(gridEdiErro);
            
            var panelErrosEdi = new Ext.form.FormPanel({
                id: 'panel-erros-edi',
                title: 'Erros EDI',
                name: 'panel-erros-edi',
                region: 'center',
                bodyStyle: 'padding: 15px',
                initEl: function (el) {
                    this.el = Ext.get(el);
                    this.id = this.el.id || Ext.id();
                    if (this.standardSubmit) {
                        this.el.dom.action = this.url;
                    } else {
                        this.el.on('submit', this.onSubmit, this);
                    }
                    this.el.addClass('x-form');
                },
                labelAlign: 'top',
                border: false,
                items: [arrCamposEdiErro]
            });
        

            $(function() {
                var tabs = new Ext.TabPanel({
                    id: 'tabPanel',
                    renderTo: Ext.getBody(),
                    activeTab: 0,
                    height: 550,
                    plain: true,
                    deferredRender: false,
                    defaults: { bodyStyle: 'padding:5px' },
                    //items: [filtros]
                    items: [filtros, panelErrosEdi]
                });

                grid.getView().refresh();

                setarDataFinal();
            });
        } else {
            $(function () {
                var tabs = new Ext.TabPanel({
                    id: 'tabPanel',
                    renderTo: Ext.getBody(),
                    activeTab: 0,
                    height: 550,
                    plain: true,
                    deferredRender: false,
                    defaults: { bodyStyle: 'padding:5px' },
                    items: [filtros]
                });
                
                grid.getView().refresh();

               setarDataFinal();
            });
        }


    </script>
</asp:Content>
