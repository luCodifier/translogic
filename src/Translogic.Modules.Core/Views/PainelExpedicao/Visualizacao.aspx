﻿		<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Interna.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content runat="server" ID="HeadContent" ContentPlaceHolderID="HeadContent">
    <script type="text/javascript" language="javascript">

</script>
</asp:Content>
<asp:Content runat="server" ID="Content2" ContentPlaceHolderID="Content">
    <div id="header-content">
        <h1>
            Painel Expedição</h1>
        <small>Visualização</small>
        <br />
    </div>
    <style>
        .corBranco
        {
            background-color: #FFFFFF;
        }
        .corVermelho
        {
            background-color: #FFB0C4;
        }
        .corAmarelo
        {
            background-color: #FFFF80;
        }
        .corVerde
        {
            background-color: #B0FFC5;
        }
    </style>
    <script type="text/javascript" language="javascript">

        var gridPainelExpedicaoVisualizacao = null;

        function MudaCorColunaIndicadorVisibilidade(value, meta) {
            meta.css = value.EstiloCelula;
            return '<div style="text-align: center; margin-top: 5px;">' + value.Valor + '</div>';
        }

        function FormataColunaCentralizadoVisibilidade(value, meta) {
            return '<div style="text-align: center; margin-top: 5px;">' + value.Valor + '</div>';
        }

        function FormataColunaCentralizado(value, meta) {
            return '<div style="text-align: center; margin-top: 5px;">' + value + '</div>';
        }

        function Pesquisar() {
            gridPainelExpedicaoVisualizacao.getStore().load();
        }

        /* Filtros */

        var estacaoFaturamentoStore = new Ext.data.JsonStore({
            root: "Items",
            autoLoad: true,
            proxy: new Ext.data.HttpProxy({
                url: '<%= Url.Action("ObterEstacoesFaturamento", "PainelExpedicao") %>',
                timeout: 600000
            }),
            id: 'estacaoFaturamentoStore',
            fields: [
                'Id',
                'Nome'
            ]
        });

        var segmentosStore = new Ext.data.JsonStore({
            root: "Items",
            autoLoad: true,
            proxy: new Ext.data.HttpProxy({
                url: '<%= Url.Action("ObterSegmentos", "PainelExpedicao") %>',
                timeout: 600000
            }),
            id: 'segmentosStore',
            fields: [
                'Id',
                'Nome'
            ]
        });


        var ufsStore = new Ext.data.ArrayStore({
            id: 0,
            fields: [
                'Id',
                'Nome'
            ],
            data: [
                [0, 'Todas'],
                [1, 'MG'],
                [2, 'MT'],
                [3, 'MS'],
                [4, 'PR'],
                [5, 'RJ'],
                [6, 'RS'],
                [7, 'SC'],
                [8, 'SP']
            ]
        });

        var situacoesVisualizacaoStore = new Ext.data.ArrayStore({
            id: 0,
            fields: [
                'Id',
                'Nome'
            ],
            data: [
                [0, 'Todas'],
                [1, 'Pendente de Confirmação'],
                [2, 'Recusado'],
                [3, 'Pendente Faturamento'],
                [4, 'Faturado'],
                [7, 'Faturado Manual'],
                [5, 'Cte Pendente'],
                [6, 'Cte Autorizado']
            ]
        });

        // Malhas Ferroviárias
        var malhasCentralStore = new Ext.data.ArrayStore({
            id: 'malhasCentralStore',
            fields: [
                'Id',
                'Nome'
            ],
            data: [
                ['Todas', 'Todas'],
                ['SUL', 'SUL'],
                ['NORTE', 'NORTE']
            ]
        });


        var txtDataInicio = {
            xtype: 'datefield',
            fieldLabel: 'Data Início',
            id: 'txtDataInicio',
            name: 'txtDataInicio',
            width: 83,
            allowBlank: false,
            value: new Date()
        };

        var ddlUf = new Ext.form.ComboBox({
            typeAhead: false,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            store: ufsStore,
            valueField: 'Nome',
            displayField: 'Nome',
            fieldLabel: 'UF',
            id: 'ddlUf',
            width: 70,
            value: 'Todas',
            listeners: {
                select: {
                    fn: function () {
                        var selectedValue = this.getValue();
                        var ddlMalhaCentral = Ext.getCmp("ddlMalhaCentral");
                        var malhaCentral = ddlMalhaCentral.getValue();
                        estacaoFaturamentoStore.load({
                            params: { uf: selectedValue, malha: malhaCentral },
                            callback: function (options, success, response, records) {
                                if (success) {
                                    var combobox = Ext.getCmp("ddlEstacaoFaturamento");

                                    if (estacaoFaturamentoStore.getCount() == 0) {
                                        combobox.setValue("");
                                    } else {
                                        combobox.setValue("TODOS");
                                    }
                                }
                            }
                        });

                    }
                }
            }
        });

        var txtOrigem = {
            xtype: 'textfield',
            name: 'txtOrigem',
            id: 'txtOrigem',
            fieldLabel: 'Origem',
            autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '3' },
            maskRe: /[a-zA-Z]/,
            style: 'text-transform: uppercase',
            width: 50
        };

        var ddlEstacaoFaturamento = new Ext.form.ComboBox({
            typeAhead: false,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            store: estacaoFaturamentoStore,
            valueField: 'Id',
            displayField: 'Nome',
            fieldLabel: 'Estação Faturamento',
            id: 'ddlEstacaoFaturamento',
            width: 120,
            value: 'TODOS'
        });

        var ddlSegmento = new Ext.form.ComboBox({
            typeAhead: false,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            store: segmentosStore,
            valueField: 'Id',
            displayField: 'Nome',
            fieldLabel: 'Segmento',
            id: 'ddlSegmento',
            width: 110,
            value: 'TODOS'
        });

        var ddlSituacao = new Ext.form.ComboBox({
            id: 'ddlSituacao',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            store: situacoesVisualizacaoStore,
            valueField: 'Id',
            displayField: 'Nome',
            value: 0,
            fieldLabel: 'Situação',
            width: 150
        });

        var ddlMalhaCentral = new Ext.form.ComboBox({
            id: 'ddlMalhaCentral',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            store: malhasCentralStore,
            valueField: 'Id',
            displayField: 'Nome',
            value: 'Todas',
            fieldLabel: 'Operação',
            width: 150,
            listeners: {
                select: {
                    fn: function () {
                        var selectedValue = this.getValue();
                        var arrayData = [];

                        if (selectedValue === 'SUL') {

                            arrayData = [
                                [0, 'Todas'],
                                [4, 'PR'],
                                [6, 'RS'],
                                [7, 'SC']
                            ];
                        }
                        else if (selectedValue === 'NORTE') {

                            arrayData = [
                                [0, 'Todas'],
                                [1, 'MG'],
                                [2, 'MT'],
                                [3, 'MS'],
                                [5, 'RJ'],
                                [8, 'SP']
                            ];
                        }
                        else if (selectedValue === 'Todas') {

                            arrayData = [
                                [0, 'Todas'],
                                [1, 'MG'],
                                [2, 'MT'],
                                [3, 'MS'],
                                [4, 'PR'],
                                [5, 'RJ'],
                                [6, 'RS'],
                                [7, 'SC'],
                                [8, 'SP']
                            ];
                        }

                        ufsStore.loadData(arrayData);
                        var comboboxUf = Ext.getCmp("ddlUf");
                        comboboxUf.setValue("Todas");

                        estacaoFaturamentoStore.load({
                            params: { uf: '', malha: selectedValue },
                            callback: function (options, success, response, records) {
                                if (success) {
                                    var combobox = Ext.getCmp("ddlEstacaoFaturamento");

                                    if (estacaoFaturamentoStore.getCount() == 0) {
                                        combobox.setValue("");
                                    } else {
                                        combobox.setValue("TODOS");
                                    }

                                }
                            }
                        });

                    }
                }
            }
        });

        var cbMetricaLarga = {
            xtype: 'checkbox',
            fieldLabel: 'Norte',
            id: 'cbMetricaLarga',
            name: 'cbMetricaLarga',
            validateField: true,
            value: true,
            checked: true
        };

        var arrMetricaLarga = {
            width: 50,
            layout: 'form',
            border: false,
            items: [cbMetricaLarga]
        };

        var cbMetricaNorte = {
            xtype: 'checkbox',
            fieldLabel: 'Métrica',
            id: 'cbMetricaNorte',
            name: 'cbMetricaNorte',
            validateField: true,
            value: true,
            checked: true
        };

        var arrMetricaNorte = {
            width: 50,
            layout: 'form',
            border: false,
            items: [cbMetricaNorte]
        };

        var cbMetricaSul = {
            xtype: 'checkbox',
            fieldLabel: 'Sul',
            id: 'cbMetricaSul',
            name: 'cbMetricaSul',
            validateField: true,
            value: true,
            checked: true
        };

        var arrMetricaSul = {
            width: 50,
            layout: 'form',
            border: false,
            items: [cbMetricaSul]
        };

        // Filtros Linha 1 //

        var arrDataIni = {
            width: 87,
            layout: 'form',
            border: false,
            items: [txtDataInicio]
        };

        var arrUf = {
            width: 90,
            layout: 'form',
            border: false,
            items: [ddlUf]
        };

        var arrOrigem = {
            width: 60,
            layout: 'form',
            border: false,
            items: [txtOrigem]
        };

        var arrEstacaoFaturamento = {
            width: 150,
            layout: 'form',
            border: false,
            items: [ddlEstacaoFaturamento]
        };

        var arrSegmento = {
            width: 130,
            layout: 'form',
            border: false,
            items: [ddlSegmento]
        };

        var arrSituacao = {
            width: 170,
            layout: 'form',
            border: false,
            items: [ddlSituacao]
        };

        var arrMalhaCentral = {
            width: 170,
            layout: 'form',
            border: false,
            items: [ddlMalhaCentral]
        };

        var arrlinha1 = {
            layout: 'column',
            border: false,
            items: [
                arrDataIni,
                arrUf,
                arrOrigem,
                arrEstacaoFaturamento,
                arrSegmento,
                arrSituacao,
                arrMalhaCentral,
                arrMetricaLarga,
                arrMetricaNorte,
                arrMetricaSul
            ]
        };

        var arrCampos = new Array();
        arrCampos.push(arrlinha1);

        // Fim Filtros Linha 1 //

        filters = new Ext.form.FormPanel({
            id: 'grid-filtros',
            title: "Filtros",
            region: 'center',
            bodyStyle: 'padding: 15px',
            labelAlign: 'top',
            items:
                [arrCampos],
            buttonAlign: "center",
            buttons:
                [
                    {
                        text: 'Pesquisar',
                        type: 'submit',
                        iconCls: 'icon-find',
                        handler: function (b, e) {
                            Pesquisar();
                        }
                    }

                ]
        });

        /* Fim Filtros */


        /* Grid de Resultado */
        var camposModelPainelExpedicaoVisualizacao = [
            { name: 'TempoRefreshMilisegundos', mapping: 'TempoRefreshMilisegundos' },
            { name: 'Origem', mapping: 'Origem' },
            { name: 'OrigemDescricao', mapping: 'OrigemDescricao' },
            { name: 'OrigemUF', mapping: 'OrigemUF' },
            { name: 'PendenteConfirmacao', mapping: 'PendenteConfirmacao' },
            { name: 'TotalRecusadoEstacao', mapping: 'TotalRecusadoEstacao' },
            { name: 'PendenteFaturado', mapping: 'PendenteFaturado' },
            { name: 'Faturado', mapping: 'Faturado' },
            { name: 'FaturadoManual', mapping: 'FaturadoManual' },
            { name: 'CtePendente', mapping: 'CtePendente' },
            { name: 'CteAutorizado', mapping: 'CteAutorizado' },
            { name: 'SituacaoVisualizacaoId', mapping: 'SituacaoVisualizacaoId' }
        ];


        var arrColunasGridPainelExpedicaoVisualizacao = new Array();
        arrColunasGridPainelExpedicaoVisualizacao.push({
            header: 'UF',
            dataIndex: "OrigemUF",
            width: 80,
            hidden: false,
            sortable: true,
            align: 'center',
            renderer: FormataColunaCentralizado
        });

        arrColunasGridPainelExpedicaoVisualizacao.push({
            header: 'Origem',
            dataIndex: "Origem",
            width: 80,
            hidden: false,
            sortable: true,
            align: 'center',
            renderer: FormataColunaCentralizado
        });

        arrColunasGridPainelExpedicaoVisualizacao.push({
            header: 'Descrição',
            dataIndex: "OrigemDescricao",
            width: 180,
            hidden: false,
            sortable: true
            //align: 'center'
            //renderer: FormataColunaCentralizado
        });

        arrColunasGridPainelExpedicaoVisualizacao.push({
            header: 'Pendente Confirmação',
            dataIndex: 'PendenteConfirmacao',
            width: 120,
            hidden: false,
            sortable: false,
            align: 'center',
            renderer: MudaCorColunaIndicadorVisibilidade
        });

        arrColunasGridPainelExpedicaoVisualizacao.push({
            header: 'Recusado Estação',
            dataIndex: 'TotalRecusadoEstacao',
            width: 120,
            hidden: false,
            sortable: false,
            align: 'center',
            renderer: MudaCorColunaIndicadorVisibilidade
        });

        arrColunasGridPainelExpedicaoVisualizacao.push({
            header: 'Pendente Fat.',
            dataIndex: 'PendenteFaturado',
            width: 100,
            hidden: false,
            sortable: false,
            align: 'center',
            renderer: MudaCorColunaIndicadorVisibilidade
        });

        arrColunasGridPainelExpedicaoVisualizacao.push({
            header: 'Faturado',
            dataIndex: 'Faturado',
            width: 100,
            hidden: false,
            sortable: false,
            align: 'center',
            renderer: FormataColunaCentralizadoVisibilidade
        });

        arrColunasGridPainelExpedicaoVisualizacao.push({
            header: 'Faturado Manual',
            dataIndex: 'FaturadoManual',
            width: 100,
            hidden: false,
            sortable: false,
            align: 'center',
            renderer: FormataColunaCentralizadoVisibilidade
        });

        arrColunasGridPainelExpedicaoVisualizacao.push({
            header: 'CT-e Pend.',
            dataIndex: 'CtePendente',
            width: 100,
            hidden: false,
            sortable: false,
            align: 'center',
            renderer: MudaCorColunaIndicadorVisibilidade
        });

        arrColunasGridPainelExpedicaoVisualizacao.push({
            header: 'CT-e Aut.',
            dataIndex: 'CteAutorizado',
            width: 100,
            hidden: false,
            sortable: false,
            align: 'center',
            renderer: FormataColunaCentralizadoVisibilidade
        });

        var loadMask = new Ext.LoadMask(Ext.getBody(), { msg: App.Resources.Web.Carregando });
        var counterLoadMask = 0;
        gridPainelExpedicaoVisualizacao = new Translogic.PaginatedGrid({
            id: 'gridPainelExpedicaoVisualizacao',
            name: 'gridPainelExpedicaoVisualizacao',
            autoLoadGrid: true,
            viewConfig: {
                forceFit: true,
                emptyText: 'Não possui dado(s) para exibição.'
            },
            fields: camposModelPainelExpedicaoVisualizacao,
            url: '<%= Url.Action("ObterPainelExpedicaoVisualizacao", "PainelExpedicao") %>',
            timeout: 1800000,
            stripeRows: true,
            region: 'center',
            columns: arrColunasGridPainelExpedicaoVisualizacao
        });

        Ext.apply(gridPainelExpedicaoVisualizacao.pagingToolbar, { pageSize: 50000 });

        gridPainelExpedicaoVisualizacao.getStore().proxy.on('beforeload', function (p, params) {
            if (counterLoadMask === 0) {
                loadMask.show();
            }

            var dataInicio = Ext.getCmp("txtDataInicio").getValue();

            var ddlUf = Ext.getCmp("ddlUf");
            var ufId = ddlUf.getValue();

            var ddlEstacaoFaturamento = Ext.getCmp("ddlEstacaoFaturamento");
            var estacaoFaturamentoId = ddlEstacaoFaturamento.getValue();

            var ddlSegmento = Ext.getCmp("ddlSegmento");
            var segmentoId = ddlSegmento.getValue();

            var ddlSituacao = Ext.getCmp("ddlSituacao");
            var situacaoId = ddlSituacao.getValue();

            var ddlMalhaCentral = Ext.getCmp("ddlMalhaCentral");
            var malhaCentral = ddlMalhaCentral.getValue();

            params['dataInicio'] = dataInicio.format('d/m/Y');
            params['uf'] = ufId;
            params['origem'] = Ext.getCmp("txtOrigem").getValue();
            params['origemFaturamento'] = estacaoFaturamentoId;
            params['segmento'] = segmentoId;
            params['situacao'] = situacaoId;
            params['malhaCentral'] = malhaCentral;
            params['metricaLarga'] = Ext.getCmp("cbMetricaLarga").getValue();
            params['metricaNorte'] = Ext.getCmp("cbMetricaNorte").getValue();
            params['metricaSul'] = Ext.getCmp("cbMetricaSul").getValue();
        });

        gridPainelExpedicaoVisualizacao.getStore().proxy.on('load', function (p, params) {
            if (counterLoadMask === 0) {
                loadMask.hide();
            }

            counterLoadMask++;
        });

        /* Fim Grid de Resultado */

        new Ext.Viewport({
            layout: 'border',
            margins: 10,
            items: [
                {
                    region: 'north',
                    height: 180,
                    autoScroll: true,
                    items: [{
                        region: 'center',
                        applyTo: 'header-content'
                    },
                        filters]
                },
                gridPainelExpedicaoVisualizacao
            ]

        });

    </script>
</asp:Content>
