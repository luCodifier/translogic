﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Interna.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content runat="server" ID="HeadContent" ContentPlaceHolderID="HeadContent">
    <div id="header-content">
        <h1>Painel Expedição</h1>
        <br />
    </div>
    <script type="text/javascript" language="javascript">

        var fm = Ext.form;

        /****************************************************** DROPDOWN STORES ******************************************************/
        var regioesDDLStore = new Ext.data.JsonStore({
            root: "Items",
            autoLoad: true,
            proxy: new Ext.data.HttpProxy({
                url: '<%= Url.Action("ObterRegioesDDL", "PainelExpedicao") %>',
                timeout: 600000
            }),
            id: 'regioesDDLStore',
            fields: [
                'Id',
                'Nome'
            ]
        });


        var arrRegiaoLinhas = new Array();

        // OperacaoStore
        var operacaoRegiaoStore = new Ext.data.JsonStore({
            root: "Items",
            autoLoad: true,
            proxy: new Ext.data.HttpProxy({
                url: '<%= Url.Action("ObterOperacoesMalha", "PainelExpedicao") %>',
                timeout: 600000
            }),
            id: 'operacaoRegiaoStore',
            fields: [
                'Id',
                'Nome'
            ]
        });

        var regioesStore = new Ext.data.JsonStore({
            root: "Items",
            autoLoad: true,
            proxy: new Ext.data.HttpProxy({
                url: '<%= Url.Action("ObterRegioes", "PainelExpedicao") %>',
                timeout: 600000
            }),
            id: 'regioesStore',
            fields: [
                'Id',
                'Nome',
                'OperacaoId'
            ]
        });


        var ddlOperacaoRegiao = new Ext.form.ComboBox({
            typeAhead: false,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            store: operacaoRegiaoStore,
            valueField: 'Id',
            width: 130,
            displayField: 'Nome',
            fieldLabel: 'Operação',
            id: 'ddlOperacaoRegiao'
        });


        var ddlGridOperacaoRegiao = new Ext.form.ComboBox({
            typeAhead: false,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            store: operacaoRegiaoStore,
            valueField: 'Id',
            displayField: 'Nome'
        });

        var btnGridExcluirRegiao = new Ext.ux.grid.RowActions({
            dataIndex: '',
            header: '',
            align: 'center',
            actions: [
                {
                    iconCls: 'icon-del',
                    tooltip: 'Excluir Região'
                }]
        });


        var columnModelRegioes = new Ext.grid.ColumnModel({
            defaults: {
                sortable: false
            },
            columns: [
                { dataIndex: "Id", hidden: true },
                {
                    header: 'Nome', dataIndex: 'Nome', width: 300, align: 'center',
                    editor: new fm.TextField({
                        allowBlank: true,
                        maxValue: 50
                    })
                },
                {
                    header: 'Operação', dataIndex: 'OperacaoId', editor: ddlGridOperacaoRegiao,
                    renderer:
                        function (value, metaData, record, rowIndex, colIndex, store) {
                            var idx = operacaoRegiaoStore.find("Id", value);
                            return (idx != "-1") ? operacaoRegiaoStore.getAt(idx).data.Nome : '';
                        }
                }

            ]
        });

        var gridRegioesRecordModel = Ext.data.Record.create(
            [
                'Id',
                'Nome',
                'OperacaoId'
            ]);


        gridRegioes = new Ext.grid.EditorGridPanel({
            id: 'grid-regioes',
            store: regioesStore,
            cm: columnModelRegioes,
            stripeRows: true,
            region: 'center',
            autoHeight: true,
            title: 'Região',
            iconCls: 'icon-grid',
            tbar: {
                items: [
                    {
                        text: 'Excluir',
                        iconCls: 'icon-del',
                        handler: function () {
                            var sm = gridRegioes.getSelectionModel();

                            if (sm.hasSelection()) {
                                var record = sm.selection.record;
                                Ext.Msg.show({
                                    title: 'Excluir Região',
                                    buttons: Ext.MessageBox.YESNOCANCEL,
                                    msg: 'Deseja excluir o registro selecionado?',
                                    fn: function (btn) {
                                        if (btn == 'yes') {

                                            var regiaoId = record.data.Id;

                                            if (regiaoId != "" && regiaoId != "0") {

                                                Ext.Ajax.request({
                                                    url: '<%= Url.Action("ExcluirRegiao", "PainelExpedicao") %>',
                                                    method: "POST",
                                                    params: { regiaoId: regiaoId },
                                                    success: function (result) {
                                                        var resSucess = Ext.util.JSON.decode(result.responseText);

                                                        if (resSucess.Success) {
                                                            gridRegioes.getStore().remove(record);

                                                            Ext.Msg.show({
                                                                title: "Excluir Região",
                                                                msg: "Região Excluída com Sucesso!",
                                                                buttons: Ext.Msg.OK,
                                                                icon: Ext.MessageBox.INFO,
                                                                minWidth: 200
                                                            });
                                                        } //success = true
                                                    },
                                                    failure: function (result) {
                                                        var resFail = Ext.util.JSON.decode(result.responseText);

                                                        Ext.Msg.show({
                                                            title: "Mensagem de Erro",
                                                            msg: resFail.Message,
                                                            buttons: Ext.Msg.OK,
                                                            icon: Ext.MessageBox.ERROR,
                                                            minWidth: 200
                                                        });
                                                    }
                                                });
                                            }

                                        }
                                    }
                                });
                            } else {
                                Ext.Msg.show({
                                    title: 'Excluir Região',
                                    buttons: Ext.MessageBox.WARNING,
                                    msg: 'É necessario clicar em uma linha da tabela para excluir'
                                });
                            }

                        }
                    },
                    {
                        text: 'Atualizar',
                        iconCls: 'icon-arrow_refresh',
                        tooltip: 'Atualiza a tabela',
                        handler: function () {
                            var sm = Ext.getCmp('grid-regioes').getStore().load();
                        }
                    }
                ]
            },
            listeners: {
                afteredit: function (e) {

                    url = "<%= Url.Action("GravarAlteracoesRegiao", "PainelExpedicao") %>";

                    Ext.Ajax.request({
                        url: url,
                        params: {
                            acao: 'alterar',
                            id: e.record.id,
                            campo: e.field,
                            valor: e.value,
                            regiaoId: e.record.data.Id,
                            Nome: e.record.data.Nome,
                            OperacaoId: e.record.data.OperacaoId
                        },
                        method: "POST",
                        success: function (result) {

                            var data = Ext.util.JSON.decode(result.responseText);

                            if (data.Success == true) {
                                Ext.Msg.show(
                                    {
                                        title: "Sucesso",
                                        msg: data.Message,
                                        buttons: Ext.Msg.OK,
                                        icon: Ext.MessageBox.INFO
                                    });
                                e.record.commit();
                            }
                            else {
                                Ext.Msg.show(
                                    {
                                        title: "Falha",
                                        msg: "Erro:" + data.Message,
                                        buttons: Ext.Msg.OK,
                                        icon: Ext.MessageBox.ERROR
                                    });
                                e.record.reject();
                            }
                        },
                        failure: function (resp, opt) {
                            e.record.reject();
                        }

                    });
                }
            }
        });

        gridRegioes.getStore();

        var btnIncluirRegiao = new Ext.Button({
            name: 'btnIncluirRegiao',
            id: 'btnIncluirRegiao',
            text: 'Incluir Região',
            width: 100,
            iconCls: 'icon-save',
            handler: function (b, e) {
                IncluirRegiao();
            }
        });

        var txtInputRegiaoNome = {
            xtype: 'textfield',
            id: 'txtInputRegiaoNome',
            fieldLabel: 'Nome',
            name: 'txtInputRegiaoNome',
            allowBlank: false,
            readOnly: false,
            //bodyStyle: 'margin-right:10px',
            autoCreate: { tag: 'input', type: 'text', maxlength: '150', autocomplete: 'off' },
            width: 200,
            style: 'text-transform: uppercase',
            enableKeyEvents: true,
            listeners: {
                specialkey: TratarTeclaEnter
            }
        };

        var arrRegiaoNome = {
            width: 300,
            layout: 'form',
            border: false,
            items: [txtInputRegiaoNome]
        };

        var arrOperacaoNome = {
            width: 300,
            layout: 'form',
            border: false,
            items: [ddlOperacaoRegiao]
        };

        var arrRegiaolinha1 = {
            layout: 'column',
            border: false,
            width: 900,
            items: [arrRegiaoNome, arrOperacaoNome]
        };

        var arrBtnIncluirRegiao = {
            width: 900,
            layout: 'form',
            border: false,
            items: [btnIncluirRegiao]
        };

        var arrRegiaolinha2 = {
            layout: 'column',
            border: false,
            width: 900,
            items: [arrBtnIncluirRegiao]
        };

        arrRegiaoLinhas.push(arrRegiaolinha1);
        arrRegiaoLinhas.push(arrRegiaolinha2);

        var formInputRegiao = new Ext.form.FormPanel
            ({
                id: 'formInputRegiao',
                labelWidth: 80,
                width: 900,
                height: 100,
                autoScroll: false,
                border: false,
                labelAlign: 'top',
                listeners: {
                    'hide': function (e) {
                    }
                },
                items:
                    [
                        arrRegiaoLinhas
                    ],
                buttonAlign: "center",
                buttons: [],
                renderTo: Ext.getBody()
            });


        var fieldSetRegioes = {
            xtype: 'fieldset',
            autoHeight: true,
            bodyStyle: 'padding: 10px',
            title: 'Cadastro das Regiões para Gestão de Pedra',
            items: [formInputRegiao, gridRegioes]
        };

        function IncluirRegiao() {
            var regiaoValue = Ext.getCmp("txtInputRegiaoNome").getValue();
            var operacaoValue = Ext.getCmp("ddlOperacaoRegiao").getValue();

            if ($.trim(regiaoValue) == "") {
                Ext.Msg.show({
                    title: "Mensagem de Erro",
                    msg: "Nome da Região não foi informada!",
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.ERROR,
                    minWidth: 200
                });
            }
            else if (operacaoValue < 0) {
                Ext.Msg.show({
                    title: "Mensagem de Erro",
                    msg: "Operação não foi informada!",
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.ERROR,
                    minWidth: 200
                });
            }

            Ext.Ajax.request({
                url: '<%= Url.Action("IncluirRegiaoPainelExpedicao", "PainelExpedicao") %>',
                method: "POST",
                params: { Nome: regiaoValue, OperacaoMalha_Id: operacaoValue },
                success: function (result) {
                    var resSucess = Ext.util.JSON.decode(result.responseText);

                    if (resSucess.Success) {
                        Ext.getCmp("txtInputRegiaoNome").setValue("");
                        Ext.getCmp("ddlOperacaoRegiao").setValue("");

                        gridRegioes.getStore().load();

                        Ext.Msg.show({
                            title: "Mensagem",
                            msg: resSucess.Message,
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.INFO,
                            minWidth: 200
                        });

                    } //success = true
                },
                failure: function (result) {
                    var resFail = Ext.util.JSON.decode(result.responseText);

                    Ext.Msg.show({
                        title: "Mensagem de Erro",
                        msg: resFail.Message,
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.ERROR,
                        minWidth: 200
                    });
                }
            });
        }


        /****************************************************** Fim Região **************************************************/
        /****************************************************** Envio Email ******************************************************/


        var arrEmailLinhas = new Array();

        // OperacaoStore para dropdown das regras
        var operacaoRegraEmailStore = new Ext.data.JsonStore({
            root: "Items",
            autoLoad: true,
            proxy: new Ext.data.HttpProxy({
                url: '<%= Url.Action("ObterOperacoesMalha", "PainelExpedicao") %>',
                timeout: 600000
            }),
            id: 'operacaoRegraEmailStore',
            fields: [
                'Id',
                'Nome'
            ]
        });

        var operacaoRegraEmailStoreGrid = new Ext.data.JsonStore({
            root: "Items",
            autoLoad: true,
            proxy: new Ext.data.HttpProxy({
                url: '<%= Url.Action("ObterOperacoesMalha", "PainelExpedicao") %>',
                timeout: 600000
            }),
            id: 'operacaoRegraEmailStoreGrid',
                    fields: [
                        'Id',
                        'Nome'
                    ]
                });


        var regraEmailStore = new Ext.data.JsonStore({
            root: "Items",
            autoLoad: true,
            proxy: new Ext.data.HttpProxy({
                url: '<%= Url.Action("ObterRegraEnvioEmailRegiao", "PainelExpedicao") %>',
                timeout: 600000
            }),
            id: 'regraEmailStore',
            fields: [
                'Id',
                'OperacaoId',
                'RegiaoId',
                'Titulo',
                'Corpo',
                'Emails'
            ]
        });


        var gridRegraEmailRecordModel = Ext.data.Record.create(
            [
                'Id',
                'OperacaoId',
                'RegiaoId',
                'Titulo',
                'Corpo',
                'Emails'
            ]);

        var ddlOperacaoRegra = new Ext.form.ComboBox({
            typeAhead: false,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            store: operacaoRegraEmailStore,
            valueField: 'Id',
            width: 100,
            displayField: 'Nome',
            fieldLabel: 'Operação',
            id: 'ddlOperacaoRegra'
        });

        var ddlOperacaoRegraGrid = new Ext.form.ComboBox({
            typeAhead: false,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            store: operacaoRegraEmailStoreGrid,
            valueField: 'Id',
            width: 130,
            displayField: 'Nome',
            fieldLabel: 'Operação',
            id: 'ddlOperacaoRegraGrid'
        });


        var ddlRegiaoRegraEnvio = new Ext.form.ComboBox({
            typeAhead: false,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            store: regioesDDLStore,
            valueField: 'Id',
            width: 130,
            displayField: 'Nome',
            fieldLabel: 'Regiao',
            id: 'ddlRegiaoRegraEnvio'
        });

        var ddlGridRegiaoRegraEnvio = new Ext.form.ComboBox({
            typeAhead: false,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            store: regioesStore,
            valueField: 'Id',
            displayField: 'Nome'
        });

        //ddlOperacaoRegra

        var columnModelRegraEmail = new Ext.grid.ColumnModel({
            defaults: { sortable: true },
            columns: [
                {
                    header: 'Operação', dataIndex: 'OperacaoId', width: 100, align: 'center', editor: ddlOperacaoRegraGrid,
                    renderer:
                        
                        function (value, metaData, record, rowIndex, colIndex, store) {
                            metaData.tdAttr = 'vertical-align: middle;';
                            var idx = operacaoRegraEmailStoreGrid.find("Id", value);
                            return (idx != "-1") ? operacaoRegraEmailStoreGrid.getAt(idx).data.Nome : '';
                        }
                },{
                    header: 'Região', dataIndex: 'RegiaoId', width: 150, align: 'center', editor: ddlGridRegiaoRegraEnvio,
                    renderer:
                        function (value, metaData, record, rowIndex, colIndex, store) {
                            var idx = regioesDDLStore.find("Id", value);
                            return (idx != "-1") ? regioesDDLStore.getAt(idx).data.Nome : '';
                        }
                },
                {
                    header: 'Título Email', dataIndex: 'Titulo', width: 250, align: 'center',
                    editor: new fm.TextField({
                        allowBlank: true,
                        maxValue: 1000
                    })
                },
                {
                    header: 'Corpo Email', dataIndex: 'Corpo', width: 400, align: 'center',
                    editor: new fm.TextField({
                        allowBlank: true,
                        maxValue: 1000
                    })
                },
                {
                    id: 'Emails', header: 'Emails (separados por ";")', dataIndex: 'Emails', align: 'left', width: 300,
                    editor: new fm.TextField({
                        allowBlank: true,
                        maxValue: 2000,
                        multiline: true
                    }),
                    renderer: function (value, metaData, record, rowIndex, colIndex, store) {
                        return ListarEmailsRenderer(record.data.Emails);
                    }
                }
            ]
        });

        function ListarEmailsRenderer(emails) {
            var html = '';

            if (emails !== undefined) {
                html = '<ul>';

                var emailsSeparados = emails.split(';');
                var quantidadeEmails = emailsSeparados.length;
                for (var i = 0; i < quantidadeEmails; i++) {
                    html += ('<li>' + emailsSeparados[i] + '</li>');
                }

                html += '</ul>';
            }

            return html;
        }


        gridEmails = new Ext.grid.EditorGridPanel({
            id: 'grid-emails',
            collapsible: true,
            store: regraEmailStore,
            cm: columnModelRegraEmail,
            stripeRows: false,
            region: 'center',
            title: 'Emails por Região',
            autoHeight: true,
            iconCls: 'icon-grid',
            autoExpandColumn: 'Emails',
            tbar: {
                items: [
                    //{
                    //    text: 'Salvar Alterações',
                    //    iconCls: 'icon-save',
                    //    handler: function () {
                    //        alert('Salvar Regra Envio Emails');
                    //    }
                    //},
                    {
                        text: 'Excluir',
                        iconCls: 'icon-del',
                        handler: function () {
                            var sm = gridEmails.getSelectionModel();

                            if (sm.hasSelection()) {
                                var record = sm.selection.record;

                                Ext.Msg.show({
                                    title: 'Excluir Regra Envio de Emails',
                                    buttons: Ext.MessageBox.YESNOCANCEL,
                                    msg: 'Deseja excluir o registro selecionado?',
                                    fn: function (btn) {
                                        var id = record.data.Id;
                                        if (btn == 'yes') {
                                            Ext.Ajax.request({
                                                url: '<%= Url.Action("ExcluirRegraEnvioEmail", "PainelExpedicao") %>',
                                                method: "POST",
                                                params: { Id: id },
                                                success: function (result) {
                                                    var resSucess = Ext.util.JSON.decode(result.responseText);

                                                    if (resSucess.Success) {
                                                        gridEmails.getStore().remove(record);

                                                        Ext.Msg.show({
                                                            title: "Excluir Regra Envio Email",
                                                            msg: "Regra Excluída com Sucesso!",
                                                            buttons: Ext.Msg.OK,
                                                            icon: Ext.MessageBox.INFO,
                                                            minWidth: 200
                                                        });
                                                    } //success = true
                                                },
                                                failure: function (result) {
                                                    var resFail = Ext.util.JSON.decode(result.responseText);

                                                    Ext.Msg.show({
                                                        title: "Mensagem de Erro",
                                                        msg: resFail.Message,
                                                        buttons: Ext.Msg.OK,
                                                        icon: Ext.MessageBox.ERROR,
                                                        minWidth: 200
                                                    });
                                                }
                                            });
                                        }
                                    }
                                });
                            } else {
                                Ext.Msg.show({
                                    title: 'Excluir Regra Envio Emails',
                                    buttons: Ext.MessageBox.WARNING,
                                    msg: 'É necessario clicar em uma linha da tabela para excluir'
                                });
                            }
                        }
                    },
                    {
                        text: 'Atualizar',
                        iconCls: 'icon-arrow_refresh',
                        tooltip: 'Atualiza a tabela',
                        handler: function () {
                            var sm = Ext.getCmp('grid-emails').getStore().load();
                        }
                    }
                ]
            },
            listeners: {
                afteredit: function (e) {

                    url = "<%= Url.Action("GravarAlteracoesRegraEmail", "PainelExpedicao") %>";

                    Ext.Ajax.request({
                        url: url,
                        params: {
                            acao: 'alterar',
                            id: e.record.id,
                            campo: e.field,
                            valor: e.value,
                            operacaoId: e.record.data.OperacaoId,
                            regiaoId: e.record.data.RegiaoId,
                            titulo: e.record.data.Titulo,
                            corpo: e.record.data.Corpo,
                            destinatarios: e.record.data.Emails,
                            nome: e.record.data.Nome,
                            regraId: e.record.data.Id
                        },
                        method: "POST",
                        success: function (result) {

                            var data = Ext.util.JSON.decode(result.responseText);

                            if (data.Success == true) {
                                Ext.Msg.show(
                                    {
                                        title: "Sucesso",
                                        msg: data.Message,
                                        buttons: Ext.Msg.OK,
                                        icon: Ext.MessageBox.INFO
                                    });
                                e.record.commit();
                            }
                            else {
                                Ext.Msg.show(
                                    {
                                        title: "Falha",
                                        msg: "Erro:" + data.Message,
                                        buttons: Ext.Msg.OK,
                                        icon: Ext.MessageBox.ERROR
                                    });
                                e.record.reject();
                            }
                        },
                        failure: function (resp, opt) {
                            e.record.reject();
                        }

                    });
                }
            }
        });

        function IncluirRegraEmail() {
            var operacaoValue = Ext.getCmp("ddlOperacaoRegra").getValue();
            var regiaoValue = Ext.getCmp("ddlRegiaoRegraEnvio").getValue();
            var titulo = Ext.getCmp("txtInputRegraEmailTitulo").getValue();
            var corpo = Ext.getCmp("txtInputRegraEmailCorpo").getValue();
            var emails = Ext.getCmp("txtInputRegraEmailEmails").getValue();

            if (operacaoValue < 0) {
                Ext.Msg.show({
                    title: "Mensagem de Erro",
                    msg: "Operação não foi informada!",
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.ERROR,
                    minWidth: 200
                });
            }
            else if (regiaoValue < 0) {
                Ext.Msg.show({
                    title: "Mensagem de Erro",
                    msg: "Região não foi informada!",
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.ERROR,
                    minWidth: 200
                });
            }
            else if ($.trim(titulo) == "") {
                Ext.Msg.show({
                    title: "Mensagem de Erro",
                    msg: "Título do Email não foi informado!",
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.ERROR,
                    minWidth: 200
                });
            } else if ($.trim(corpo) == "") {
                Ext.Msg.show({
                    title: "Mensagem de Erro",
                    msg: "Corpo do Email não foi informado!",
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.ERROR,
                    minWidth: 200
                });
            } else if ($.trim(emails) == "") {
                Ext.Msg.show({
                    title: "Mensagem de Erro",
                    msg: "Destinatários do Email não foi informado!",
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.ERROR,
                    minWidth: 200
                });
            }


            Ext.Ajax.request({
                url: '<%= Url.Action("IncluirRegraEnvioEmailsPainelExpedicao", "PainelExpedicao") %>',
                method: "POST",
                params: { operacaoId: operacaoValue, regiaoId: regiaoValue, titulo: titulo, corpo: corpo, destinatarios: emails},
                success: function (result) {
                    
                    var resSucess = Ext.util.JSON.decode(result.responseText);

                    if (resSucess.Success) {
                        Ext.getCmp("txtInputRegraEmailTitulo").setValue("");
                        Ext.getCmp("txtInputRegraEmailCorpo").setValue("");
                        Ext.getCmp("txtInputRegraEmailEmails").setValue("");


                        gridEmails.getStore().load();

                        Ext.Msg.show({
                            title: "Sucesso",
                            msg: resSucess.Message,
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.INFO,
                            minWidth: 200
                        });

                    }//success = true
                    else
                    {
                        Ext.Msg.show({
                            title: "Falha",
                            msg: resSucess.Message,
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.ERROR,
                            minWidth: 200
                        });

                    }
                },
                failure: function (result) {
                    var resFail = Ext.util.JSON.decode(result.responseText);

                    Ext.Msg.show({
                        title: "Falha",
                        msg: resFail.Message,
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.ERROR,
                        minWidth: 200
                    });
                }
            });
        }



        var btnIncluirRegraEmail = new Ext.Button({
            name: 'btnIncluirRegraEmail',
            id: 'btnIncluirRegraEmail',
            text: 'Incluir Regra de Email',
            width: 150,
            iconCls: 'icon-save',
            handler: function (b, e) {
                IncluirRegraEmail();
            }
        });

        var txtInputRegraEmailTitulo = {
            xtype: 'textfield',
            id: 'txtInputRegraEmailTitulo',
            fieldLabel: 'Título Email',
            name: 'txtInputRegraEmailTitulo',
            allowBlank: false,
            readOnly: false,
            //bodyStyle: 'margin-right:10px',
            autoCreate: { tag: 'input', type: 'text', maxlength: '200', autocomplete: 'off' },
            width: 150,
            enableKeyEvents: true,
            listeners: {
                specialkey: TratarTeclaEnter
            }
        };

        var txtInputRegraEmailCorpo = {
            xtype: 'textfield',
            id: 'txtInputRegraEmailCorpo',
            fieldLabel: 'Corpo Email',
            name: 'txtInputRegraEmailCorpo',
            allowBlank: false,
            readOnly: false,
            //bodyStyle: 'margin-right:10px',
            autoCreate: { tag: 'input', type: 'text', maxlength: '250', autocomplete: 'off' },
            width: 200,
            enableKeyEvents: true,
            listeners: {
                specialkey: TratarTeclaEnter
            }
        };

        var txtInputRegraEmailEmails = {
            xtype: 'textfield',
            id: 'txtInputRegraEmailEmails',
            fieldLabel: 'Emails (separados por ";")',
            name: 'txtInputRegraEmailEmails',
            allowBlank: false,
            readOnly: false,
            //bodyStyle: 'margin-right:10px',
            autoCreate: { tag: 'input', type: 'text', maxlength: '2000', autocomplete: 'off' },
            width: 400,
            enableKeyEvents: true,
            listeners: {
                specialkey: TratarTeclaEnter
            }
        };

        var arrRegraEmailOperacao = {
            width: 120,
            layout: 'form',
            border: false,
            items: [ddlOperacaoRegra]
        };

        var arrRegraEmailRegiao = {
            width: 170,
            layout: 'form',
            border: false,
            items: [ddlRegiaoRegraEnvio]
        };

        var arrRegraEmailTitulo = {
            width: 170,
            layout: 'form',
            border: false,
            items: [txtInputRegraEmailTitulo]
        };
        var arrRegraEmailCorpo = {
            width: 220,
            layout: 'form',
            border: false,
            items: [txtInputRegraEmailCorpo]
        };

        var arrRegraEmailEmails = {
            width: 420,
            layout: 'form',
            border: false,
            items: [txtInputRegraEmailEmails]
        };

        var arrlinhaRegraEmails1 = {
            layout: 'column',
            border: false,
            width: 1400,
            items: [arrRegraEmailOperacao, arrRegraEmailRegiao, arrRegraEmailTitulo, arrRegraEmailCorpo, arrRegraEmailEmails]
        };

        var arrbtnIncluirRegraEmail = {
            width: 900,
            layout: 'form',
            border: false,
            items: [btnIncluirRegraEmail]
        };

        var arrlinhaRegraEmails2 = {
            layout: 'column',
            border: false,
            width: 1400,
            items: [arrbtnIncluirRegraEmail]
        };

        arrEmailLinhas.push(arrlinhaRegraEmails1);
        arrEmailLinhas.push(arrlinhaRegraEmails2);

        var formInputRegraEmails = new Ext.form.FormPanel
            ({
                id: 'formInputRegraEmails',
                labelWidth: 80,
                width: 1000,
                height: 100,
                autoScroll: false,
                border: false,
                labelAlign: 'top',
                listeners: {
                    'hide': function (e) {
                    }
                },
                items:
                    [
                        arrEmailLinhas
                    ],
                buttonAlign: "center",
                buttons: [],
                renderTo: Ext.getBody()
            });

        var fieldSetEmails = {
            xtype: 'fieldset',
            autoHeight: true,
            bodyStyle: 'padding: 10px',
            title: 'Emails para entrega de Relatório de Pedra por Região',
            items: [formInputRegraEmails, gridEmails]
        };

        /****************************************************** Fim Envio Email **************************************************/

        /******************************************************Funções******************************************************/
        function salvarConfiguracaoIndicadores() {
            var store = gridIndicadores.getStore();
            var data = Array();
            var colunasDdl = Ext.getCmp("ddl-colunas");
            var colunaId = colunasDdl.getValue();

            store.each(function (rec) {
                var element = {
                    IdIndicador: undefined,
                    IdCor: undefined,
                    Operador1: {
                        IdOperador: undefined
                    },
                    Valor1: undefined,
                    Operador2: {
                        IdOperador: undefined
                    },
                    Valor2: undefined,
                    IdOperadorLogico: undefined,
                    IdColuna: undefined
                };

                element.IdIndicador = rec.get('IdIndicador');
                element.IdCor = rec.get('IdCor');
                element.Operador1.IdOperador = rec.get('Op1Id');
                element.Valor1 = rec.get('Vl1');
                element.Operador2.IdOperador = rec.get('Op2Id');;
                element.Valor2 = rec.get('Vl2');
                element.IdOperadorLogico = rec.get('OpLogId');;
                element.IdColuna = colunaId;

                data.push(element);
            });

            if (Ext.Msg.confirm("Salvar Indicadores", "Deseja salvar essas configurações de indicadores?", function (btn, text) {
                if (btn == 'yes') {
                    Ext.getBody().mask("Salvando as configurações de indicadores.", "x-mask-loading");

                    var dataJson = $.toJSON(data);

                    $.ajax({
                        url: '<%= Url.Action("SalvarIndicadores", "PainelExpedicao" ) %>',
                        type: "POST",
                        dataType: 'json',
                        data: dataJson,
                        contentType: "application/json; charset=utf-8",
                        success: function (response, options) {
                            if (response.Success) {
                                Ext.Msg.alert("Sucesso", response.Message);
                            }
                            else {
                                Ext.Msg.alert("Erro", response.Message);
                            }

                            Ext.getBody().unmask();
                        },
                        failure: function () {
                            Ext.Msg.alert("Mensagem de Erro", "Ocorreu um erro no servidor.");
                            Ext.getBody().unmask();
                        }
                    });
                }
            }));
        }
        /*****************************************************************************************************************/

        /******************************************************STORES******************************************************/
        var indicadoresStore = new Ext.data.JsonStore({
            root: "Items",
            autoLoad: false,
            proxy: new Ext.data.HttpProxy({
                url: '<%= Url.Action("ObterIndicadores", "PainelExpedicao") %>',
                timeout: 600000
            }),
            id: 'indicadoresStore',
            fields: [
                'IdIndicador',
                'IdCor',
                'Cor',
                'Op1Id',
                'Op1Simbolo',
                'Vl1',
                'OpLogId',
                'Op2Id',
                'Op2Simbolo',
                'Vl2'
            ]
        });

        var indicadoresCTePendenteStore = new Ext.data.JsonStore({
            root: "Items",
            autoLoad: false,
            proxy: new Ext.data.HttpProxy({
                url: '<%= Url.Action("ObterIndicadores", "PainelExpedicao") %>',
                timeout: 600000
            }),
            id: 'indicadoresCTePendenteStore',
            fields: [
                'IdIndicador',
                'IdCor',
                'Cor',
                'Op1Id',
                'Op1Simbolo',
                'Vl1',
                'OpLogId',
                'Op2Id',
                'Op2Simbolo',
                'Vl2'
            ]
        });

        var indicadoresPendenteConfirmacaoStore = new Ext.data.JsonStore({
            root: "Items",
            autoLoad: false,
            proxy: new Ext.data.HttpProxy({
                url: '<%= Url.Action("ObterIndicadores", "PainelExpedicao") %>',
                timeout: 600000
            }),
            id: 'indicadoresPendenteConfirmacaoStore',
            fields: [
                'IdIndicador',
                'IdCor',
                'Cor',
                'Op1Id',
                'Op1Simbolo',
                'Vl1',
                'OpLogId',
                'Op2Id',
                'Op2Simbolo',
                'Vl2'
            ]
        });

        var indicadoresRecusadoEstacaoStore = new Ext.data.JsonStore({
            root: "Items",
            autoLoad: false,
            proxy: new Ext.data.HttpProxy({
                url: '<%= Url.Action("ObterIndicadores", "PainelExpedicao") %>',
                timeout: 600000
            }),
            id: 'indicadoresRecusadoEstacaoStore',
            fields: [
                'IdIndicador',
                'IdCor',
                'Cor',
                'Op1Id',
                'Op1Simbolo',
                'Vl1',
                'OpLogId',
                'Op2Id',
                'Op2Simbolo',
                'Vl2'
            ]
        });

        var operadoresStore = new Ext.data.JsonStore({
            root: "Items",
            autoLoad: false,
            proxy: new Ext.data.HttpProxy({
                url: '<%= Url.Action("ObterOperadores", "PainelExpedicao") %>',
                timeout: 600000
            }),
            id: 'operadoresStore',
            fields: [
                'Id',
                'Simbolo',
                'Descricao'
            ]
        });

        operadoresStore.load({
            callback: function (options, success, response, records) {
                if (success) {
                    indicadoresStore.load({ params: { colunaId: 1 } });
                    indicadoresCTePendenteStore.load({ params: { colunaId: 2 } });
                    indicadoresPendenteConfirmacaoStore.load({ params: { colunaId: 3 } });
                    indicadoresRecusadoEstacaoStore.load({ params: { colunaId: 4 } });
                }
            }
        });

        var logicaStore = new Ext.data.ArrayStore({
            id: 0,
            fields: [
                'ID',
                'Text'
            ],
            data: [[0, 'Nenhum'], [1, 'E'], [2, 'OU']]
        });
        /*****************************************************************************************************************/

        // shorthand alias
        var fm = Ext.form;

        var btnSalvarCores = new Ext.Button({
            id: 'btnSalvarCores',
            name: 'btnSalvarCores',
            text: 'Salvar',
            width: 100,
            iconCls: 'icon-save',
            handler: function (b, e) {
                salvarConfiguracaoIndicadores();
            }
        });
        var colSalvarCores = {
            layout: 'form',
            border: false,
            items: [btnSalvarCores]
        };

        var arrLinhaBotoesCores = {
            layout: 'column',
            border: false,
            bodyStyle: 'margin: 5px',
            items: [colSalvarCores]
        };

        var colunasStore = new Ext.data.ArrayStore({
            id: 'colunasStores',
            fields: [
                'ID',
                'Text'
            ],
            data: [[1, 'Pendente Faturamento'], [2, 'CT-e Pendente'], [3, 'Pendente de Confirmação'], [4, 'Recusados Estação']]
        });

        var comboOp1 = new Ext.form.ComboBox({
            typeAhead: false,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            store: operadoresStore,
            valueField: 'Id',
            displayField: 'Simbolo'
        });

        var comboOp2 = new Ext.form.ComboBox({
            typeAhead: false,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            store: operadoresStore,
            valueField: 'Id',
            displayField: 'Simbolo'
        });

        var comboLog = new Ext.form.ComboBox({
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            store: logicaStore,
            valueField: 'ID',
            displayField: 'Text',
            listeners: {
                select: {
                    fn: function () {
                        var g = Ext.getCmp('grid-indicadores');
                        var sm = g.getSelectionModel();
                        var rec = sm.selection.record;
                        var selectedValue = this.getValue();
                        if (!selectedValue || selectedValue <= 0) {
                            rec.set('Op2Id', '');
                            rec.set('Vl2', '');
                        }
                    }
                }
            }
        });

        // the column model has information about grid columns
        // dataIndex maps the column to the specific data field in
        // the data store (created below)
        var cm = new Ext.grid.ColumnModel({
            // specify any defaults for each column
            defaults: {
                sortable: true
            },
            columns: [
                {
                    header: 'Cor',
                    dataIndex: 'Cor',
                    //width: 100,
                    renderer:
                        function (value, metaData, record) {
                            metaData.tdAttr = 'id="txt-cor-' + record.get('Cor') + '"';
                            return value;
                        }
                },
                {
                    header: 'Operador 1',
                    dataIndex: 'Op1Id',
                    //width: 80,
                    editor: comboOp1,
                    renderer:
                        function (value, metaData, record, rowIndex, colIndex, store) {
                            var idx = operadoresStore.find("Id", value);
                            return (idx != "-1") ? operadoresStore.getAt(idx).data.Simbolo : '';
                        }
                },
                {
                    header: 'Valor 1',
                    dataIndex: 'Vl1',
                    //width: 70,
                    align: 'right',
                    //renderer: 'usMoney',
                    editor: new fm.NumberField({
                        allowBlank: false,
                        allowNegative: true
                        //maxValue: 100000
                    })
                },
                {
                    header: 'Lógica',
                    dataIndex: 'OpLogId',
                    //width: 80,
                    editor: comboLog,
                    renderer:
                        function (value, metaData, record, rowIndex, colIndex, store) {
                            var idx = logicaStore.find("ID", value);
                            return (idx != "-1") ? logicaStore.getAt(idx).data.Text : '';
                        }
                },
                {
                    header: 'Operador 2',
                    dataIndex: 'Op2Id',
                    //width: 80,
                    editor: comboOp2,
                    renderer:
                        function (value, metaData, record, rowIndex, colIndex, store) {
                            var idx = operadoresStore.find("Id", value);
                            return (idx != "-1") ? operadoresStore.getAt(idx).data.Simbolo : '';
                        }
                },
                {
                    header: 'Valor 2',
                    dataIndex: 'Vl2',
                    //width: 70,
                    align: 'right',
                    //renderer: 'usMoney',
                    editor: new fm.NumberField({
                        allowBlank: true,
                        allowNegative: true
                        //maxValue: 100000
                    })
                }
            ]
        });

        gridIndicadores = new Ext.grid.EditorGridPanel({
            id: 'grid-indicadores',
            store: indicadoresStore,
            cm: cm,
            clicksToEdit: 1,
            stripeRows: true,
            region: 'center',
            autoHeight: true,
            title: 'Configuração',
            iconCls: 'icon-grid',
            listeners: {
                beforeedit: function (grid) {
                },
                afteredit: function (e) {
                }
            }
        });

        gridIndicadores.getStore();

        var comboColunas = new Ext.form.ComboBox({
            id: 'ddl-colunas',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            store: colunasStore,
            valueField: 'ID',
            displayField: 'Text',
            value: 1,
            fieldLabel: 'Indicador'
        });
        comboColunas.on('select', function (box, record, index) {
            var colunaId = comboColunas.getValue();
            if (colunaId == 1)
                gridIndicadores.reconfigure(indicadoresStore, cm);
            else if (colunaId == 2)
                gridIndicadores.reconfigure(indicadoresCTePendenteStore, cm);
            else if (colunaId == 3)
                gridIndicadores.reconfigure(indicadoresPendenteConfirmacaoStore, cm);
            else if (colunaId == 4)
                gridIndicadores.reconfigure(indicadoresRecusadoEstacaoStore, cm);
        });

        var colColunas = {
            layout: 'form',
            border: false,
            items: [comboColunas]
        };

        var dropDownListColunas = {
            layout: 'column',
            border: false,
            bodyStyle: 'margin: 5px',
            items: [colColunas]
        };

        var fieldSetCores = {
            xtype: 'fieldset',
            autoHeight: true,
            bodyStyle: 'padding: 10px',
            title: 'Indicadores',
            items: [dropDownListColunas, gridIndicadores, arrLinhaBotoesCores]
        };

        /*****************************************************************************************************************/
        /******************************************* ORIGENS *************************************************************/
        /*****************************************************************************************************************/

        var tabResultados = null;
        var linhaResultados = null;
        var linhaTabOrigem = null;
        var gridOrigens = null;
        var gridBloqueioDespacho = null;
        var gridClientesTipoIntegracao = null;
        var arrLinhas = new Array();
        var arrLinhasBloqueioDespacho = new Array();

        var camposModelOrigem = [
            { name: 'Id', mapping: 'Id' },
            { name: 'Estacao', mapping: 'Estacao' },
            { name: 'Descricao', mapping: 'Descricao' },
            { name: 'EstacaoFaturamento', mapping: 'EstacaoFaturamento' },
            { name: 'DescricaoEstacaoFaturamento', mapping: 'DescricaoEstacaoFaturamento' },
            { name: 'RegiaoId', mapping: 'RegiaoId' },
            { name: 'DescricaoRegiao', mapping: 'DescricaoRegiao' }
        ];

        var detalheActionOrigens = new Ext.ux.grid.RowActions({
            dataIndex: '',
            header: '',
            align: 'center',
            actions: [
                { iconCls: 'icon-del', tooltip: 'Excluir Origem' }],
            callbacks: {
                'icon-del': function (grid, record, action, row, col) {
                    var estacaoExclusao = record.data.Estacao.toUpperCase();
                    //var origemExclusao = record.data.Id;

                    if (Ext.Msg.confirm("Excluir", "Deseja realmente excluir a Origem " + estacaoExclusao + " ?", function (btn, text) {
                        if (btn == 'yes') {
                            ExcluirOrigem(record);
                        }
                    }));
                }
            }
        });

        var arrColunasGridOrigens = new Array();
        arrColunasGridOrigens.push({ header: 'Id', dataIndex: "Id", hidden: true });
        arrColunasGridOrigens.push({ header: 'Estação', dataIndex: "Estacao", hidden: false, sortable: true });
        arrColunasGridOrigens.push({ header: 'Descrição', dataIndex: 'Descricao', width: 200, hidden: false, sortable: true });
        arrColunasGridOrigens.push({ header: 'Estação Faturamento', dataIndex: 'EstacaoFaturamento', width: 200, hidden: false, sortable: true });
        arrColunasGridOrigens.push({ header: 'Descrição Estação Faturamento', dataIndex: 'DescricaoEstacaoFaturamento', width: 200, hidden: false, sortable: true });
        arrColunasGridOrigens.push({ header: 'RegiaoId', dataIndex: 'RegiaoId', hidden: true, sortable: false });
        arrColunasGridOrigens.push({ header: 'Região', dataIndex: 'DescricaoRegiao', width: 200, hidden: false, sortable: true });
        arrColunasGridOrigens.push(detalheActionOrigens);

        gridOrigens = new Translogic.PaginatedGrid({
            id: 'gridOrigens',
            name: 'gridOrigens',
            autoLoadGrid: true,
            region: 'center',
            autoHeight: true,
            title: 'Configuração',
            iconCls: 'icon-grid',
            viewConfig: {
                emptyText: 'Não possui origem(ns) para exibição.'
            },
            fields: camposModelOrigem,
            /*fields:
            [
            'Id',
            'Estacao',
            'Descricao'
            ],*/

            filteringToolbar: [
                {
                    id: 'btnExportar',
                    text: 'Exportar',
                    tooltip: 'Exportar',
                    iconCls: 'icon-page-excel',
                    handler: function (c) {
                        var url = '<%= this.Url.Action("ObterPainelExpedicaoOrigens") %>';

                        url += "?exportarExcel=true";

                        window.open(url, "");
                    }
                }
            ],

            url: '<%= Url.Action("ObterPainelExpedicaoOrigens", "PainelExpedicao") %>',
            stripeRows: true,
            loadMask: { msg: App.Resources.Web.Carregando },
            columns: arrColunasGridOrigens,
            plugins: [detalheActionOrigens],
            listeners: {
                rowClick: function (grid, rowIndex, record, e) {
                    //var sel = gridOrigens.getSelectionModel().getSelected();
                },
                rowdblclick: function (grid, rowIndex, record, e) {

                }
            }
        });

        gridOrigens.getStore().proxy.on('beforeload', function (p, params) {
            params['exportarExcel'] = false;
        });

        function IncluirOrigem() {
            var origem = Ext.getCmp("txtOrigemIncluir").getValue();
            var estacaoFaturamento = Ext.getCmp("txtOrigemResponsavel").getValue();
            var regiaoOrigem = Ext.getCmp("ddlRegiaoOrigem").getValue();

            if ($.trim(origem) == "") {
                Ext.Msg.show({
                    title: "Mensagem de Erro",
                    msg: "Não há origem informada!",
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.ERROR,
                    minWidth: 200
                });
            }
            else if ($.trim(estacaoFaturamento) == "") {
                Ext.Msg.show({
                    title: "Mensagem de Erro",
                    msg: "Não há estação de faturamento informada!",
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.ERROR,
                    minWidth: 200
                });
            }
            else if (regiaoOrigem < 0) {
                Ext.Msg.show({
                    title: "Mensagem de Erro",
                    msg: "Não há Região informada!",
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.ERROR,
                    minWidth: 200
                });
            }
            else {

                origem = origem.toUpperCase();
                estacaoFaturamento = estacaoFaturamento.toUpperCase();

                var bOrigemJaInformada = false;

                gridOrigens.overviewStore.each(
                    function (record) {
                        var estacao = record.data.Estacao.toUpperCase();
                        if (estacao == origem) {
                            bOrigemJaInformada = true;
                        }
                    }
                );

                if (bOrigemJaInformada == true) {
                    Ext.Msg.show({
                        title: "Mensagem de Erro",
                        msg: "Esta origem já foi incluída!",
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.ERROR,
                        minWidth: 200
                    });

                    return;
                }

                Ext.Ajax.request({
                    url: '<%= Url.Action("IncluirPainelExpedicaoOrigem", "PainelExpedicao") %>',
                    method: "POST",
                    params: { origem: origem, estacaoFaturamento: estacaoFaturamento, regiaoId: regiaoOrigem },
                    success: function (result) {
                        var resSucess = Ext.util.JSON.decode(result.responseText);

                        if (resSucess.Success) {
                            Ext.getCmp("txtOrigemIncluir").setValue("");
                            Ext.getCmp("txtOrigemResponsavel").setValue("");
                            Ext.getCmp("ddlRegiaoOrigem").setValue("");
                            var rec = new gridOrigens.overviewStore.recordType({
                                Id: resSucess.Id,
                                Estacao: resSucess.Estacao,
                                Descricao: resSucess.Descricao,
                                EstacaoFaturamento: resSucess.EstacaoFaturamento,
                                DescricaoEstacaoFaturamento: resSucess.DescricaoEstacaoFaturamento,
                                RegiaoId: resSucess.RegiaoId
                            });

                            rec.commit();
                            gridOrigens.overviewStore.add(rec);


                            Ext.Msg.show({
                                title: "Mensagem",
                                msg: resSucess.Message,
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.INFO,
                                minWidth: 200
                            });

                        } //success = true
                    },
                    failure: function (result) {
                        var resFail = Ext.util.JSON.decode(result.responseText);

                        Ext.Msg.show({
                            title: "Mensagem de Erro",
                            msg: resFail.Message,
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.ERROR,
                            minWidth: 200
                        });
                    }
                });
            }
        }


        function ExcluirOrigem(recExclusao) {
            var origemId = recExclusao.data.Id;

            if (origemId != "") {

                Ext.Ajax.request({
                    url: '<%= Url.Action("ExcluirPainelExpedicaoOrigem", "PainelExpedicao") %>',
                    method: "POST",
                    params: { origemId: origemId },
                    success: function (result) {
                        var resSucess = Ext.util.JSON.decode(result.responseText);

                        if (resSucess.Success) {
                            gridOrigens.overviewStore.remove(recExclusao);

                            Ext.Msg.show({
                                title: "Mensagem",
                                msg: resSucess.Message,
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.INFO,
                                minWidth: 200
                            });
                        } //success = true
                    },
                    failure: function (result) {
                        var resFail = Ext.util.JSON.decode(result.responseText);

                        Ext.Msg.show({
                            title: "Mensagem de Erro",
                            msg: resFail.Message,
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.ERROR,
                            minWidth: 200
                        });
                    }
                });
            }

        }

        function TratarTeclaEnter(f, e) {
            if (e.getKey() == e.ENTER) {
                IncluirOrigem();
            }
        }

        var btnOrigemIncluir = new Ext.Button({
            name: 'btnOrigemIncluir',
            id: 'btnOrigemIncluir',
            text: 'Incluir',
            width: 100,
            iconCls: 'icon-save',
            handler: function (b, e) {
                IncluirOrigem();
            }
        });

        var edOrigemIncluir = {
            xtype: 'textfield',
            id: 'txtOrigemIncluir',
            fieldLabel: 'Origem',
            name: 'txtOrigemIncluir',
            allowBlank: false,
            readOnly: false,
            //bodyStyle: 'margin-right:10px',
            autoCreate: { tag: 'input', type: 'text', maxlength: '3', autocomplete: 'off' },
            width: 70,
            maskRe: /[a-zA-Z]/,
            style: 'text-transform: uppercase',
            enableKeyEvents: true,
            listeners: {
                specialkey: TratarTeclaEnter
            }
        };

        var regiaoOrigemDDLStore = new Ext.data.JsonStore({
            root: "Items",
            autoLoad: true,
            proxy: new Ext.data.HttpProxy({
                url: '<%= Url.Action("ObterRegioesDDL", "PainelExpedicao") %>',
                timeout: 900000
            }),
                    id: 'regioesDDLStore',
                    fields: [
                        'Id',
                        'Nome'
                    ]
                });


        var ddlRegiaoOrigem = new Ext.form.ComboBox({
            typeAhead: false,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            store: regiaoOrigemDDLStore,
            valueField: 'Id',
            width: 130,
            displayField: 'Nome',
            fieldLabel: 'Regiao',
            id: 'ddlRegiaoOrigem'
        });


        var edOrigemResponsavel = {
            xtype: 'textfield',
            id: 'txtOrigemResponsavel',
            fieldLabel: 'Estação Faturamento',
            name: 'txtOrigemResponsavel',
            allowBlank: false,
            readOnly: false,
            //bodyStyle: 'margin-right:10px',
            autoCreate: { tag: 'input', type: 'text', maxlength: '3', autocomplete: 'off' },
            width: 70,
            maskRe: /[a-zA-Z]/,
            style: 'text-transform: uppercase',
            enableKeyEvents: true,
            listeners: {
                specialkey: TratarTeclaEnter
            }
        };

        var arrOrigem = {
            width: 150,
            layout: 'form',
            border: false,
            items: [edOrigemIncluir]
        };

        var arrOrigemResponsavel = {
            width: 150,
            layout: 'form',
            border: false,
            items: [edOrigemResponsavel]
        };

        var arrRegiaoOrigem = {
            width: 200,
            layout: 'form',
            border: false,
            items: [ddlRegiaoOrigem]
        };

        var arrlinha1 = {
            layout: 'column',
            border: false,
            width: 1000,
            items: [arrOrigem, arrOrigemResponsavel, arrRegiaoOrigem]
        };

        var arrBtnSalvar = {
            width: 150,
            layout: 'form',
            border: false,
            items: [btnOrigemIncluir]
        };

        var arrlinha2 = {
            layout: 'column',
            border: false,
            width: 1000,
            items: [arrBtnSalvar]
        };

        arrLinhas.push(arrlinha1);
        arrLinhas.push(arrlinha2);

        var formInputOrigem = new Ext.form.FormPanel
            ({
                id: 'formInputOrigem',
                labelWidth: 80,
                width: 600,
                height: 100,
                autoScroll: false,
                border: false,
                labelAlign: 'top',
                listeners: {
                    'hide': function (e) {
                    }
                },
                items:
                    [
                        arrLinhas
                    ],
                buttonAlign: "center",
                buttons: [],
                renderTo: Ext.getBody()
            });


        var fieldSetOrigens = {
            xtype: 'fieldset',
            autoHeight: true,
            bodyStyle: 'padding: 10px',
            title: 'Origens',
            items: [formInputOrigem, gridOrigens]
        };
        /* Fim Origens */


        /*****************************************************************************************************************/
        /******************************************* BLOQUEIOS *************************************************************/
        /*****************************************************************************************************************/



        function IncluirBloqueioDespacho() {
            var origem = Ext.getCmp("txtOrigemBloqueioDespacho").getValue();
            var segmento = Ext.getCmp("ddlSegmento").getValue();
            var cnpj = Ext.getCmp("txtCnpj").getValue();

            if ($.trim(origem) == "") {
                Ext.Msg.show({
                    title: "Mensagem de Erro",
                    msg: "Não há estação informada!",
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.ERROR,
                    minWidth: 200
                });

                return;
            }
            else if ($.trim(segmento) == "") {
                Ext.Msg.show({
                    title: "Mensagem de Erro",
                    msg: "Não há segmento informado!",
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.ERROR,
                    minWidth: 200
                });

                return;
            }
            else {

                origem = origem.toUpperCase().trim();
                segmento = segmento.toUpperCase().trim();
                cnpj = cnpj.toUpperCase().trim();
                var cnpjRaiz = '';
                if (cnpj != null) {
                    cnpjRaiz = cnpj.substring(0, 8);
                }

                /*if (($.trim(segmento) == "TODOS") && ($.trim(cnpj) == "")) {
                Ext.Msg.show({
                title: "Mensagem de Erro",
                msg: "Cnpj deve ser informado para segmento Todos!",
                buttons: Ext.Msg.OK,
                icon: Ext.MessageBox.ERROR,
                minWidth: 200
                });

                return;
                }*/

                var bCadastroJaRealizado = false;

                gridBloqueioDespacho.overviewStore.each(
                    function (record) {
                        var origemRec = record.data.EstacaoCodigo.toUpperCase();
                        var segmentoRec = record.data.Segmento.toUpperCase();
                        var cnpjRec = record.data.CnpjRaiz.toUpperCase();
                        if ((origemRec == origem) && (segmentoRec == segmento) && (cnpjRec == cnpjRaiz)) {
                            bCadastroJaRealizado = true;
                        }
                    }
                );

                if (bCadastroJaRealizado == true) {
                    Ext.Msg.show({
                        title: "Mensagem de Erro",
                        msg: "Esta estação já foi incluída para este segmento e cnpj!",
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.ERROR,
                        minWidth: 200
                    });

                    return;
                }

                var perguntaTodosOsClientes = false;
                if ($.trim(cnpj) == "") {
                    perguntaTodosOsClientes = true;
                }

                ConfirmaInclusao(origem, segmento, cnpj, perguntaTodosOsClientes);
            }
        }

        function ConfirmaInclusao(origem, segmento, cnpj, perguntaTodosOsClientes) {

            var msg = "Confirma a inclusão do Bloqueio da Estação " + origem + " - Segmento " + segmento + " - CNPJ " + cnpj + " ?";
            if ((perguntaTodosOsClientes) && (perguntaTodosOsClientes == true)) {
                msg = "Confirma a inclusão do Bloqueio da Estação " + origem + " - Segmento " + segmento + " - Todos os Clientes ?";
            }


            if (Ext.Msg.confirm("Incluir Bloqueio", msg, function (btn, text) {
                if (btn == 'yes') {

                    Ext.Ajax.request({
                        url: '<%= Url.Action("IncluirBloqueioDespacho", "PainelExpedicao") %>',
                        method: "POST",
                        params: { origem: origem, segmento: segmento, cnpj: cnpj },
                        success: function (result) {
                            var resSucess = Ext.util.JSON.decode(result.responseText);

                            if (resSucess.Success) {
                                Ext.getCmp("txtOrigemBloqueioDespacho").setValue("");
                                Ext.getCmp("ddlSegmento").setValue("TODOS");
                                Ext.getCmp("txtCnpj").setValue("");

                                if (resSucess.BloqueiosInseridos != null) {
                                    for (var i = 0; i < resSucess.BloqueiosInseridos.length; i++) {
                                        var bloqueioInserido = resSucess.BloqueiosInseridos[i];

                                        var rec = new gridBloqueioDespacho.overviewStore.recordType({
                                            Id: bloqueioInserido.Id,
                                            EstacaoCodigo: bloqueioInserido.CodigoEstacao,
                                            EstacaoDescricao: bloqueioInserido.DescricaoEstacao,
                                            Segmento: bloqueioInserido.Segmento,
                                            CnpjRaiz: bloqueioInserido.CnpjRaiz,
                                            Cliente: bloqueioInserido.Cliente
                                        });
                                        rec.commit();
                                        gridBloqueioDespacho.overviewStore.add(rec);
                                    }

                                    RefreshGridBloqueioDespacho();
                                }


                                Ext.Msg.show({
                                    title: "Mensagem",
                                    msg: resSucess.Message,
                                    buttons: Ext.Msg.OK,
                                    icon: Ext.MessageBox.INFO,
                                    minWidth: 200
                                });

                            } //success = true
                            else {
                                Ext.Msg.show({
                                    title: "Mensagem de Erro",
                                    msg: resSucess.Message,
                                    buttons: Ext.Msg.OK,
                                    icon: Ext.MessageBox.ERROR,
                                    minWidth: 200
                                });
                            }
                        },
                        failure: function (result) {
                            var resFail = Ext.util.JSON.decode(result.responseText);

                            Ext.Msg.show({
                                title: "Mensagem de Erro",
                                msg: resFail.Message,
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.ERROR,
                                minWidth: 200
                            });
                        }
                    });


                }
            }));

        }

        function ExcluirBloqueioDespacho(recExclusao) {
            var bloqueioId = recExclusao.data.Id;

            if (bloqueioId != "") {

                Ext.Ajax.request({
                    url: '<%= Url.Action("ExcluirBloqueioDespacho", "PainelExpedicao") %>',
                    method: "POST",
                    params: { bloqueioId: bloqueioId },
                    success: function (result) {
                        var resSucess = Ext.util.JSON.decode(result.responseText);

                        if (resSucess.Success) {
                            gridBloqueioDespacho.overviewStore.remove(recExclusao);

                            RefreshGridBloqueioDespacho();

                            Ext.Msg.show({
                                title: "Mensagem",
                                msg: resSucess.Message,
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.INFO,
                                minWidth: 200
                            });
                        } //success = true
                    },
                    failure: function (result) {
                        var resFail = Ext.util.JSON.decode(result.responseText);

                        Ext.Msg.show({
                            title: "Mensagem de Erro",
                            msg: resFail.Message,
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.ERROR,
                            minWidth: 200
                        });
                    }
                });
            }

        }

        function RefreshGridBloqueioDespacho() {
            gridBloqueioDespacho.overviewStore.load();
        }

        function TratarTeclaEnterBloqueioDespacho(f, e) {
            if (e.getKey() == e.ENTER) {
                IncluirBloqueioDespacho();
            }
        }

        var btnBloqueioDespachoIncluir = {
            xtype: "button",
            name: 'btnBloqueioDespachoIncluir',
            id: 'btnBloqueioDespachoIncluir',
            text: 'Incluir',
            fieldLabel: '&nbsp;',
            labelSeparator: ' ',
            iconCls: 'icon-save',
            width: 75,
            handler: IncluirBloqueioDespacho
        };

        var edOrigemBloqueioDespacho = {
            xtype: 'textfield',
            id: 'txtOrigemBloqueioDespacho',
            fieldLabel: 'Estação',
            name: 'txtOrigemBloqueioDespacho',
            allowBlank: false,
            readOnly: false,
            autoCreate: { tag: 'input', type: 'text', maxlength: '3', autocomplete: 'off' },
            width: 70,
            maskRe: /[a-zA-Z]/,
            style: 'text-transform: uppercase',
            enableKeyEvents: true,
            listeners: {
                specialkey: TratarTeclaEnterBloqueioDespacho
            }
        };


        // Campo Segmento
        var segmentosStore = new Ext.data.JsonStore({
            root: "Items",
            autoLoad: true,
            proxy: new Ext.data.HttpProxy({
                url: '<%= Url.Action("ObterSegmentos", "PainelExpedicao") %>',
                timeout: 600000
            }),
            id: 'segmentosStore',
            fields: [
                'Id',
                'Nome'
            ]
        });

        var ddlSegmento = new Ext.form.ComboBox({
            typeAhead: false,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            store: segmentosStore,
            valueField: 'Id',
            width: 130,
            displayField: 'Nome',
            fieldLabel: 'Segmento',
            id: 'ddlSegmento'
        });

        // Campo Cnpj
        var edCnpj = {
            xtype: 'textfield',
            id: 'txtCnpj',
            fieldLabel: 'Cnpj',
            name: 'txtCnpj',
            allowBlank: false,
            readOnly: false,
            autoCreate: { tag: 'input', type: 'text', maxlength: '14', autocomplete: 'off' },
            //mask: { text: '99.999.999/9999-99', placeholder: '#', includeInValue: true },
            width: 100,
            maskRe: /[0-9]/,
            enableKeyEvents: true,
            listeners: {
                specialkey: TratarTeclaEnterBloqueioDespacho
                /*'change': function (field, newVal, oldVal) {
                console.log(field);
                }*/
            }
        };

        var arrOrigemBloqueioDespacho = {
            width: 100,
            layout: 'form',
            border: false,
            items: [edOrigemBloqueioDespacho]
        };


        var arrSegmento = {
            width: 150,
            layout: 'form',
            border: false,
            items: [ddlSegmento]
        };

        var arrCnpj = {
            width: 150,
            layout: 'form',
            border: false,
            items: [edCnpj]
        };

        var arrBtnSalvarBloqueioDespacho = {
            width: 90,
            layout: 'form',
            border: false,
            items: [btnBloqueioDespachoIncluir]
        };

        var arrlinha1BloqueioDespacho = {
            layout: 'column',
            border: false,
            width: 800,
            items: [arrOrigemBloqueioDespacho, arrSegmento, arrCnpj, arrBtnSalvarBloqueioDespacho]
        };

        arrLinhasBloqueioDespacho.push(arrlinha1BloqueioDespacho);

        var formInputBloqueioDespacho = new Ext.form.FormPanel
            ({
                id: 'formInputBloqueioDespacho',
                labelWidth: 80,
                width: 800,
                height: 80,
                region: 'center',
                bodyStyle: 'padding: 10px',
                autoScroll: false,
                border: false,
                labelAlign: 'top',
                listeners: {
                    'hide': function (e) {
                    }
                },
                items:
                    [
                        arrLinhasBloqueioDespacho
                    ],
                buttonAlign: "center",
                buttons: [],
                renderTo: Ext.getBody()
            });

        var detalheActionBloqueioDespacho = new Ext.ux.grid.RowActions({
            dataIndex: 'Excluir',
            header: '',
            //width: 50,
            align: 'center',
            //autoWidth:true,
            actions: [
                {
                    iconCls: 'icon-del',
                    tooltip: 'Excluir Bloqueio'
                }],
            callbacks: {
                'icon-del': function (grid, record, action, row, col) {
                    var estacaoExclusao = record.data.EstacaoCodigo.toUpperCase();
                    var segmento = record.data.Segmento.toUpperCase();
                    var cnpj = record.data.CnpjRaiz;
                    var cliente = record.data.Cliente;

                    var msg = "Confirma a exclusão do Bloqueio da Estação " + estacaoExclusao + " - Segmento " + segmento;

                    if ((cnpj == null) || (cnpj == '')) {
                        msg = msg + " - Todos os Clientes ?";
                    }
                    else {
                        msg = msg + " - Cnpj " + cnpj + " - Cliente " + cliente + " ?";
                    }


                    if (Ext.Msg.confirm("Excluir", msg, function (btn, text) {
                        if (btn == 'yes') {
                            ExcluirBloqueioDespacho(record);
                        }
                    }));
                }
            }
        });

        var camposModelBloqueioDespacho = [
            { name: 'Estacao', mapping: 'Estacao' },
            { name: 'Id', mapping: 'Id' },
            { name: 'EstacaoCodigo', mapping: 'EstacaoCodigo' },
            { name: 'EstacaoDescricao', mapping: 'EstacaoDescricao' },
            { name: 'Segmento', mapping: 'Segmento' },
            { name: 'CnpjRaiz', mapping: 'CnpjRaiz' },
            { name: 'Cliente', mapping: 'Cliente' },
            { name: 'DataBloqueio', mapping: 'DataBloqueio' },
            { name: 'UsuarioBloqueio', mapping: 'UsuarioBloqueio' }

        ];

        var arrColunasGridBloqueioDespacho = new Array();
        arrColunasGridBloqueioDespacho.push({ dataIndex: "Estacao", header: 'Estação', hidden: true });
        arrColunasGridBloqueioDespacho.push({ dataIndex: "Id", hidden: true });

        arrColunasGridBloqueioDespacho.push({
            header: 'Estação',
            width: 70,
            dataIndex: "EstacaoCodigo",
            sortable: true
        });

        arrColunasGridBloqueioDespacho.push({
            header: 'Descrição',
            width: 150,
            dataIndex: "EstacaoDescricao",
            sortable: true
        });

        arrColunasGridBloqueioDespacho.push({
            header: 'Segmento',
            width: 100,
            dataIndex: "Segmento",
            sortable: true
        });

        arrColunasGridBloqueioDespacho.push({
            header: 'CNPJ Raiz',
            width: 80,
            dataIndex: "CnpjRaiz",
            sortable: true,
            renderer: function (value, metaData, record, rowIndex,
                colIndex, store, view) {
                if ((value) && (value != ''))
                    return value;
                else
                    return 'Todos';
            }
        });

        arrColunasGridBloqueioDespacho.push({
            header: 'Cliente',
            width: 150,
            dataIndex: "Cliente",
            sortable: true
        });

        arrColunasGridBloqueioDespacho.push({
            header: 'Data do Bloqueio',
            width: 120,
            dataIndex: "DataBloqueio",
            sortable: false
        });

        arrColunasGridBloqueioDespacho.push({
            header: 'Usuário',
            width: 150,
            dataIndex: "UsuarioBloqueio",
            sortable: false
        });
        arrColunasGridBloqueioDespacho.push(detalheActionBloqueioDespacho);


        gridBloqueioDespacho = new Translogic.PaginatedGrid({
            id: 'gridBloqueioDespacho',
            name: 'gridBloqueioDespacho',
            autoLoadGrid: true,
            region: 'center',
            autoHeight: true,
            title: 'Estações/Segmentos/Clientes Bloqueados',
            iconCls: 'icon-grid',
            viewConfig: {
                //forceFit: true,
                emptyText: 'Não possui bloqueio(s) para exibição.'
            },
            fields: camposModelBloqueioDespacho,
            filteringToolbar: [
                {
                    id: 'btnExportar',
                    text: 'Exportar',
                    tooltip: 'Exportar',
                    iconCls: 'icon-page-excel',
                    handler: function (c) {
                        var url = '<%= this.Url.Action("ObterPainelExpedicaoBloqueiosDespacho") %>';

                        url += "?exportarExcel=true";

                        window.open(url, "");
                    }
                }
            ],
            url: '<%= Url.Action("ObterPainelExpedicaoBloqueiosDespacho", "PainelExpedicao") %>',
            stripeRows: true,
            loadMask: { msg: App.Resources.Web.Carregando },
            columns: arrColunasGridBloqueioDespacho,
            plugins: [detalheActionBloqueioDespacho],
            listeners: {
                rowClick: function (grid, rowIndex, record, e) {
                },
                rowdblclick: function (grid, rowIndex, record, e) {

                }
            }
        });


        gridBloqueioDespacho.getStore().proxy.on('beforeload', function (p, params) {
            params['exportarExcel'] = false;
        });

        var fieldSetBloqueioDespacho = {
            xtype: 'fieldset',
            autoHeight: true,
            bodyStyle: 'padding: 10px',
            title: 'Bloqueio de Despacho',
            items: [formInputBloqueioDespacho, gridBloqueioDespacho]
        };


        /* Fim Bloqueios */



        /*****************************************************************************************************************/
        /******************************************* TIPOS DE INTEGRAÇÃO *************************************************************/
        /*****************************************************************************************************************/


        function IncluirClienteTipoIntegracao() {
            var origem = Ext.getCmp("txtOrigemClienteTipoIntegracao").getValue();
            var ddlCliente = Ext.getCmp("ddlClienteTipoIntegracao");
            var clienteId = ddlCliente.getValue();
            var tipoIntegracaoId = Ext.getCmp("ddlTiposIntegracao").getValue();

            if ($.trim(origem) == "") {
                Ext.Msg.show({
                    title: "Mensagem de Erro",
                    msg: "Não há estação informada!",
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.ERROR,
                    minWidth: 200
                });

                return;
            }
            else if ($.trim(String(clienteId)) == "") {
                Ext.Msg.show({
                    title: "Mensagem de Erro",
                    msg: "Não há cliente informado!",
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.ERROR,
                    minWidth: 200
                });

                return;
            }
            else if ($.trim(String(tipoIntegracaoId)) == "") {
                Ext.Msg.show({
                    title: "Mensagem de Erro",
                    msg: "Não há tipo de integração informado!",
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.ERROR,
                    minWidth: 200
                });

                return;
            }
            else {

                origem = origem.toUpperCase().trim();

                ConfirmaInclusaoTipoIntegracao(origem, clienteId, tipoIntegracaoId);
            }
        }

        function ConfirmaInclusaoTipoIntegracao(origem, clienteId, tipoIntegracaoId) {

            var msg = "Confirma a inclusão do Tipo de Integração?";

            if (Ext.Msg.confirm("Incluir Tipo de Integração", msg, function (btn, text) {
                if (btn == 'yes') {

                    Ext.Ajax.request({
                        url: '<%= Url.Action("IncluirClienteTipoIntegracao", "PainelExpedicao") %>',
                        method: "POST",
                        params: { origem: origem, clienteId: clienteId, tipoIntegracaoId: tipoIntegracaoId },
                        success: function (result) {
                            var resSucess = Ext.util.JSON.decode(result.responseText);

                            if (resSucess.Success) {
                                Ext.getCmp("txtOrigemClienteTipoIntegracao").setValue("");
                                Ext.getCmp("ddlClienteTipoIntegracao").setValue("");
                                Ext.getCmp("ddlTiposIntegracao").setValue("");


                                if (resSucess.ClienteTipoIntegracaoInserido != null) {

                                    var rec = new gridClientesTipoIntegracao.overviewStore.recordType({
                                        Id: resSucess.ClienteTipoIntegracaoInserido.Id,
                                        Estacao: resSucess.ClienteTipoIntegracaoInserido.Estacao,
                                        Cliente: resSucess.ClienteTipoIntegracaoInserido.Cliente,
                                        TipoIntegracao: resSucess.ClienteTipoIntegracaoInserido.TipoIntegracao
                                    });
                                    rec.commit();
                                    gridClientesTipoIntegracao.overviewStore.add(rec);
                                    RefreshGridClienteTipoIntegracao();
                                }


                                Ext.Msg.show({
                                    title: "Mensagem",
                                    msg: resSucess.Message,
                                    buttons: Ext.Msg.OK,
                                    icon: Ext.MessageBox.INFO,
                                    minWidth: 200
                                });

                            } //success = true
                            else {
                                Ext.Msg.show({
                                    title: "Mensagem de Erro",
                                    msg: resSucess.Message,
                                    buttons: Ext.Msg.OK,
                                    icon: Ext.MessageBox.ERROR,
                                    minWidth: 200
                                });
                            }
                        },
                        failure: function (result) {
                            var resFail = Ext.util.JSON.decode(result.responseText);

                            Ext.Msg.show({
                                title: "Mensagem de Erro",
                                msg: resFail.Message,
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.ERROR,
                                minWidth: 200
                            });
                        }
                    });


                }
            }));

        }


        function ExcluirClienteTipoIntegracao(recExclusao) {
            var recId = recExclusao.data.Id;

            if (recId != "") {

                Ext.Ajax.request({
                    url: '<%= Url.Action("ExcluirClienteTipoIntegracao", "PainelExpedicao") %>',
                    method: "POST",
                    params: { clienteTipoId: recId },
                    success: function (result) {
                        var resSucess = Ext.util.JSON.decode(result.responseText);

                        if (resSucess.Success) {
                            gridClientesTipoIntegracao.overviewStore.remove(recExclusao);

                            RefreshGridClienteTipoIntegracao();

                            Ext.Msg.show({
                                title: "Mensagem",
                                msg: resSucess.Message,
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.INFO,
                                minWidth: 200
                            });
                        } //success = true
                    },
                    failure: function (result) {
                        var resFail = Ext.util.JSON.decode(result.responseText);

                        Ext.Msg.show({
                            title: "Mensagem de Erro",
                            msg: resFail.Message,
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.ERROR,
                            minWidth: 200
                        });
                    }
                });
            }

        }


        var clienteStore = new window.Ext.data.JsonStore({
            id: 'clienteStore',
            name: 'clienteStore',
            root: 'Items',
            url: '<%=Url.Action("ListClientesNomes", "CADEConfig") %>',
            fields: [
                'Id',
                'DescResumida'
            ],
            listeners: {
                load: function (store) {
                    var rt = store.recordType;
                    store.insert(0, new rt({}));
                }
            }
        });

        var tiposIntegracaoStore = new Ext.data.JsonStore({
            root: "Items",
            autoLoad: true,
            proxy: new Ext.data.HttpProxy({
                url: '<%= Url.Action("ObterTiposIntegracao", "PainelExpedicao") %>',
                timeout: 600000
            }),
            id: 'tiposIntegracaoStore',
            fields: [
                'Id',
                'Descricao'
            ]
        });

        function TratarTeclaEnterClienteTipoIntegracao(f, e) {
            if (e.getKey() == e.ENTER) {
                IncluirClienteTipoIntegracao();
            }
        }

        var edOrigemClienteTipoIntegracao = {
            xtype: 'textfield',
            id: 'txtOrigemClienteTipoIntegracao',
            fieldLabel: 'Estação',
            name: 'txtOrigemClienteTipoIntegracao',
            allowBlank: false,
            readOnly: false,
            autoCreate: { tag: 'input', type: 'text', maxlength: '3', autocomplete: 'off' },
            width: 70,
            maskRe: /[a-zA-Z]/,
            style: 'text-transform: uppercase',
            enableKeyEvents: true,
            listeners: {
                specialkey: TratarTeclaEnterClienteTipoIntegracao
            }
        };

        var ddlClienteTipoIntegracao = {
            xtype: 'combo',
            id: 'ddlClienteTipoIntegracao',
            name: 'ddlClienteTipoIntegracao',
            forceSelection: true,
            store: clienteStore,
            triggerAction: 'all',
            mode: 'remote',
            typeAhead: true,
            minChars: 3,
            fieldLabel: 'Cliente',
            displayField: 'DescResumida',
            valueField: 'Id',
            width: 100,
            editable: true,
            tpl: '<tpl for="."><div class="x-combo-list-item">{DescResumida}&nbsp;</div></tpl>'
        };

        var ddlTiposIntegracao = new Ext.form.ComboBox({
            typeAhead: false,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            store: tiposIntegracaoStore,
            valueField: 'Id',
            width: 130,
            displayField: 'Descricao',
            fieldLabel: 'Tipo',
            id: 'ddlTiposIntegracao'
        });

        var btnTiposIntegracaoIncluir = {
            xtype: "button",
            name: 'btnTiposIntegracaoIncluir',
            id: 'btnTiposIntegracaoIncluir',
            text: 'Incluir',
            fieldLabel: '&nbsp;',
            labelSeparator: ' ',
            iconCls: 'icon-save',
            width: 75,
            handler: IncluirClienteTipoIntegracao
        };

        var arrLinhasClientesTipoIntegracao = new Array();

        var arrEstacaoTipoIntegracao = {
            width: 100,
            layout: 'form',
            border: false,
            items: [edOrigemClienteTipoIntegracao]
        };

        var arrClientesTipoIntegracao = {
            width: 150,
            layout: 'form',
            border: false,
            items: [ddlClienteTipoIntegracao]
        };

        var arrTiposIntegracao = {
            width: 150,
            layout: 'form',
            border: false,
            items: [ddlTiposIntegracao]
        };

        var arrBtnSalvarTiposIntegracao = {
            width: 90,
            layout: 'form',
            border: false,
            items: [btnTiposIntegracaoIncluir]
        };

        var arrlinha1ClientesTipoIntegracao = {
            layout: 'column',
            border: false,
            width: 800,
            items: [arrEstacaoTipoIntegracao, arrClientesTipoIntegracao, arrTiposIntegracao, arrBtnSalvarTiposIntegracao]
        };

        arrLinhasClientesTipoIntegracao.push(arrlinha1ClientesTipoIntegracao);

        var formInputClientesTipoIntegracao = new Ext.form.FormPanel
            ({
                id: 'formInputClientesTipoIntegracao',
                labelWidth: 80,
                width: 800,
                height: 80,
                region: 'center',
                bodyStyle: 'padding: 10px',
                autoScroll: false,
                border: false,
                labelAlign: 'top',
                listeners: {
                    'hide': function (e) {
                    }
                },
                items:
                    [
                        arrLinhasClientesTipoIntegracao
                    ],
                buttonAlign: "center",
                buttons: [],
                renderTo: Ext.getBody()
            });


        var detalheActionClientesTipoIntegracao = new Ext.ux.grid.RowActions({
            dataIndex: 'Excluir',
            header: '',
            //width: 50,
            align: 'center',
            //autoWidth:true,
            actions: [
                {
                    iconCls: 'icon-del',
                    tooltip: 'Excluir Tipo de Integração'
                }],
            callbacks: {
                'icon-del': function (grid, record, action, row, col) {
                    var estacaoExclusao = record.data.EstacaoCodigo.toUpperCase();
                    var cliente = record.data.Cliente.toUpperCase();
                    var tipo = record.data.TipoIntegracao;

                    var msg = "Confirma a exclusão do Tipo de Integração da Estação " + estacaoExclusao +
                        " - Cliente " + cliente + " - Tipo " + tipo + " ?";


                    if (Ext.Msg.confirm("Excluir", msg, function (btn, text) {
                        if (btn == 'yes') {
                            ExcluirClienteTipoIntegracao(record);
                        }
                    }));
                }
            }
        });

        var camposModelClientesTipoIntegracao = [
            { name: 'Id', mapping: 'Id' },
            { name: 'EstacaoCodigo', mapping: 'EstacaoCodigo' },
            { name: 'Estacao', mapping: 'Estacao' },
            { name: 'Cliente', mapping: 'Cliente' },
            { name: 'TipoIntegracao', mapping: 'TipoIntegracao' }

        ];

        var arrColunasGridClientesTipoIntegracao = new Array();

        arrColunasGridClientesTipoIntegracao.push({
            header: 'Estação',
            width: 150,
            dataIndex: "EstacaoCodigo",
            sortable: true
        });
        arrColunasGridClientesTipoIntegracao.push({
            header: 'Descrição',
            width: 150,
            dataIndex: "Estacao",
            sortable: true
        });
        arrColunasGridClientesTipoIntegracao.push({
            header: 'Cliente',
            width: 150,
            dataIndex: "Cliente",
            sortable: true
        });
        arrColunasGridClientesTipoIntegracao.push({
            header: 'Tipo',
            width: 150,
            dataIndex: "TipoIntegracao",
            sortable: true
        });

        arrColunasGridClientesTipoIntegracao.push(detalheActionClientesTipoIntegracao);


        gridClientesTipoIntegracao = new Translogic.PaginatedGrid({
            id: 'gridClientesTipoIntegracao',
            name: 'gridClientesTipoIntegracao',
            autoLoadGrid: true,
            region: 'center',
            autoHeight: true,
            title: 'Estações/Clientes',
            iconCls: 'icon-grid',
            viewConfig: {
                //forceFit: true,
                emptyText: 'Não possui estações/clientes(s) para exibição.'
            },
            fields: camposModelClientesTipoIntegracao,
            filteringToolbar: [
                {
                    id: 'btnExportar',
                    text: 'Exportar',
                    tooltip: 'Exportar',
                    iconCls: 'icon-page-excel',
                    handler: function (c) {
                        var url = '<%= this.Url.Action("ObterPainelExpedicaoClientesTipoIntegracao") %>';

                        url += "?exportarExcel=true";

                        window.open(url, "");
                    }
                }
            ],
            url: '<%= Url.Action("ObterPainelExpedicaoClientesTipoIntegracao", "PainelExpedicao") %>',
            stripeRows: true,
            loadMask: { msg: App.Resources.Web.Carregando },
            columns: arrColunasGridClientesTipoIntegracao,
            plugins: [detalheActionClientesTipoIntegracao],
            listeners: {
                rowClick: function (grid, rowIndex, record, e) {
                },
                rowdblclick: function (grid, rowIndex, record, e) {

                }
            }
        });

        gridClientesTipoIntegracao.getStore().proxy.on('beforeload', function (p, params) {
            params['exportarExcel'] = false;
        });

        var fieldSetClientesTipoIntegracao = {
            xtype: 'fieldset',
            autoHeight: true,
            bodyStyle: 'padding: 10px',
            title: 'Tipos de Integração',
            items: [formInputClientesTipoIntegracao, gridClientesTipoIntegracao]
        };

        function RefreshGridClienteTipoIntegracao() {
            gridClientesTipoIntegracao.overviewStore.load();
        }

        /* Fim tipo de integração */



        /* PANEL GERAL */
        $(function () {

            var panelGeral = new Ext.form.FormPanel({
                id: 'panel-geral',
                layout: 'form',
                labelAlign: 'top',
                border: false,
                autoHeight: true,
                title: "Parametrização",
                region: 'center',
                bodyStyle: 'padding: 15px',
                items: [fieldSetCores, fieldSetRegioes, fieldSetEmails, fieldSetOrigens, fieldSetBloqueioDespacho, fieldSetClientesTipoIntegracao],
                buttonAlign: "center"
            }).render(document.body);

        });

    </script>
</asp:Content>
