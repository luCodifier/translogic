﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Interna.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content runat="server" ID="HeadContent" ContentPlaceHolderID="HeadContent">
    <div id="header-content">
        <h1>
            Faturamento :: Relatório Histórico Faturamentos</h1>
        <small>Consulta de Histórico de Faturamentos</small>
        <br />
    </div>
    <style type="text/css">
        .chkStatusCTe 
        {
          margin-left: 5px;
        }
        .chkStatusNFe 
        {
          margin-left: 5px;
        }
        .chkStatusTicket
        {
          margin-left: 5px;
        }
        .chkStatusDcl
        {
          margin-left: 5px;
        }
        .chkStatusLaudoMercadoria
        {
          margin-left: 10px;
        }
    </style>
    <script type="text/javascript" language="javascript">

        /* region :: Functions */
        
        function RetornaVagoes() {
            var vagoes = '';
            if(storeVagoes.getCount() != 0)
            {
                storeVagoes.each(
                    function (record) {
                        if (!record.data.Erro) {
                            vagoes += record.data.Vagao + ";";
                        }
                    }
                );
            } else {
                vagoes = Ext.getCmp("txtVagoes").getValue();
            }

            return vagoes;
        }

        function RetornaSelecionados() {

            var results = {
                ids: undefined,
                //idsMdfe: undefined,
                ImprimeTicket: false,
                ImprimeCte: false,
                ImprimeNfe: false,
                ImprimeWebDanfe: false,
                ImprimeDcl: false,
                ImprimeLaudoMercadoria: false
                //ImprimeMdfe: false
            };

            results.ids = new Array();
            //results.idsMdfe = new Array();
            var selection = sm.getSelections();

            for (var i = 0; i < selection.length; i++) {

                results.ids.push(selection[i].data.Id);
                
                if (selection[i].data.PossuiTicket.toUpperCase() == 'S') {
                    results.ImprimeTicket = true;
                }

                if (selection[i].data.PossuiNfe.toUpperCase() == 'S') {
                    results.ImprimeNfe = true;
                }
                
                if (selection[i].data.PossuiWebDanfe.toUpperCase() == 'S') {
                    results.ImprimeWebDanfe = true;
                }
                
                if (selection[i].data.CteStatus) {

                    if (selection[i].data.CteStatus.toUpperCase() == 'AUT') {
                        results.ImprimeCte = true;
                    }
                }

                if (selection[i].data.PossuiDcl.toUpperCase() == 'S') {
                    results.ImprimeDcl = true;
                }
                
                if (selection[i].data.PossuiLaudoMercadoria.toUpperCase() == 'S') {
                    results.ImprimeLaudoMercadoria = true;
                }
            }

            return results;
        }

        function RetornaDadosGrid() {
            var retorno = {
                data: undefined,
                statusCte: undefined,
                statusNfe: undefined,
                statusWebDanfe: undefined,
                statusTicket: undefined,
                statusDcl: undefined,
                statusLaudoMercadoria: undefined
                //statusMdfe: undefined
            };

            var data = RetornaSelecionados();

            retorno.data = data;

            retorno.statusCte = Ext.getCmp("chkStatusCTe").getValue();
            retorno.statusNfe = Ext.getCmp('chkStatusNFe').getValue();
            retorno.statusWebDanfe = Ext.getCmp('chkStatusWebDanfe').getValue();
            retorno.statusTicket = Ext.getCmp('chkStatusTicket').getValue();
            retorno.statusDcl = Ext.getCmp('chkStatusDcl').getValue();
            retorno.statusLaudoMercadoria = Ext.getCmp('chkStatusLaudoMercadoria').getValue();
            
            return retorno;
        }

        function statusMdfePdfRenderer(idMdfe) {
            if (idMdfe > 0) {
                return "<img src='<%=Url.Images("Icons/pdf.png") %>' alt='Arquivo Mdf-e' onclick='ImprimirMdfe(" + idMdfe + ")' >";

            }
            else {
                return "";
            }
        }

        function ImprimirMdfe(idMdfe) {
            Ext.Msg.show({
                title: 'Atenção',
                msg: 'Deseja imprimir o arquivo MDF-e?',
                buttons: Ext.Msg.YESNO,
                fn: function (btn) {
                    if (btn == 'yes') {
                        
                        var bImprime = true;
                        var sMsgErro = '';

                        if (idMdfe <= 0) {
                            bImprime = false;
                            sMsgErro = 'Este vagão não está associado a uma Composição/OS que possua Mdf-e.';
                        }

                        if (bImprime == false) {
                            Ext.Msg.show({
                                title: "Mensagem de Informação",
                                msg: sMsgErro,
                                buttons: Ext.Msg.OK,
                                minWidth: 200
                            });
                        }
                        else {

                            var url = '<%= Url.Action("ImprimirVagoesPainelExpedicao", "GestaoDocumentos") %>';
                            url += "?ids=" + "";
                            url += "&codigosVagoes=" + RetornaVagoes();
                            url += "&idMdfe=" + idMdfe;
                            url += "&cte=" + "false";
                            url += "&nfe=" + "false";
                            url += "&ticket=" + "false";
                            url += "&dcl=" + "false";
                            url += "&laudoMercadoria=" + "false";
                            url += "&mdfe=" + "true";
                            url += "&historico=" + "true";

                            window.open(url, "");
                        }
                    }
                },
                icon: Ext.MessageBox.QUESTION
            });
        }

        function Imprimir() {
            Ext.Msg.show({
                title: 'Atenção',
                msg: 'Deseja visualizar os registros selecionados?',
                buttons: Ext.Msg.YESNO,
                fn: function (btn) {
                    if (btn == 'yes') {
                        var selecionados = RetornaDadosGrid();

                        var bImprime = true;
                        var sMsgErro = '';

                        if ((selecionados.statusCte == false) &&
                            (selecionados.statusNfe == false) &&
                            (selecionados.statusWebDanfe == false) &&
                            (selecionados.statusTicket == false) &&
                            (selecionados.statusDcl == false) &&
                            (selecionados.statusLaudoMercadoria == false)) {
                            bImprime = false;
                            sMsgErro = 'Por favor, selecione pelo menos um tipo de documento para impressão.';
                        }

                        if (selecionados.data.ids.length <= 0) {
                            bImprime = false;
                            sMsgErro = 'Favor selecionar pelo menos um vagão.';
                        }

                        if (bImprime == true) {
                            if (selecionados.statusTicket == true) {
                                if (!selecionados.data.ImprimeTicket) {
                                    bImprime = false;
                                    sMsgErro = 'Favor selecionar pelo menos um vagão que possua ticket.';
                                }
                            }

                            if (selecionados.statusCte == true) {
                                if (!selecionados.data.ImprimeCte) {
                                    bImprime = false;
                                    sMsgErro = 'Favor selecionar pelo menos um vagão que possua Cte.';
                                }
                            }

                            if (selecionados.statusNfe == true) {
                                if (!selecionados.data.ImprimeNfe) {
                                    bImprime = false;
                                    sMsgErro = 'Favor selecionar pelo menos um vagão que possua Nfe.';
                                }
                            }
                            
                            if (selecionados.statusWebDanfe == true) {
                                if (!selecionados.data.ImprimeWebDanfe) {
                                    bImprime = false;
                                    sMsgErro = 'Favor selecionar pelo menos um vagão que possua Danfe.';
                                }
                            }

                            if (selecionados.statusDcl == true) {
                                if (!selecionados.data.ImprimeDcl) {
                                    bImprime = false;
                                    sMsgErro = 'Favor selecionar pelo menos um vagão que possua Dcl.';
                                }
                            }
                            
                            if (selecionados.statusLaudoMercadoria == true) {
                                if (!selecionados.data.ImprimeLaudoMercadoria) {
                                    bImprime = false;
                                    sMsgErro = 'Favor selecionar pelo menos um vagão que possua o Laudo da Mercadoria.';
                                    console.log(selecionados.data);
                                }
                            }

                            if (bImprime == false) {
                                Ext.Msg.show({
                                    title: "Mensagem de Informação",
                                    msg: sMsgErro,
                                    buttons: Ext.Msg.OK,
                                    minWidth: 200
                                });
                            }
                            else {
                                // atribui o valor do campo Check box referente ao Ticket de Pesagem
                                selecionados.data.ImprimeTicket = selecionados.statusTicket;
                                // atribui o valor do campo Check box referente a Nfe
                                selecionados.data.ImprimeNfe = selecionados.statusNfe;
                                // atribui o valor do campo Check box referente a Nfe
                                selecionados.data.ImprimeWebDanfe = selecionados.statusWebDanfe;
                                // atribui o valor do campo Check box referente a Cte
                                selecionados.data.ImprimeCte = selecionados.statusCte;
                                // atribui o valor do campo Check box referente a Dcl
                                selecionados.data.ImprimeDcl = selecionados.statusDcl;
                                // atribui o valor do campo Check box referente ao Laudo da Mercadoria
                                selecionados.data.ImprimeLaudoMercadoria = selecionados.statusLaudoMercadoria;
                                // atribui o valor do campo Check box referente ao MDF-e
                                //selecionados.data.ImprimeMdfe = selecionados.statusMdfe;

                                var url = '<%= Url.Action("ImprimirVagoesPainelExpedicao", "GestaoDocumentos") %>';
                                url += "?ids=" + selecionados.data.ids;
                                url += "&codigosVagoes=" + RetornaVagoes();
                                url += "&idMdfe=" + "";
                                url += "&cte=" + selecionados.data.ImprimeCte;
                                url += "&nfe=" + selecionados.data.ImprimeNfe;
                                url +="&webDanfe=" + selecionados.data.ImprimeWebDanfe;
                                url += "&ticket=" + selecionados.data.ImprimeTicket;
                                url += "&dcl=" + selecionados.data.ImprimeDcl;
                                url += "&laudoMercadoria=" + selecionados.data.ImprimeLaudoMercadoria;
                                url += "&mdfe=" + "false"; //selecionados.data.ImprimeMdfe;
                                url += "&historico=" + "true";

                                window.open(url, "");
                            }
                        }
                        else {
                            Ext.Msg.show({
                                title: "Mensagem de Informação",
                                msg: sMsgErro,
                                buttons: Ext.Msg.OK,
                                minWidth: 200
                            });
                        }
                    }
                },
                icon: Ext.MessageBox.QUESTION
            });
        }


        function Pesquisar() {
            grid.getStore().load();
        }

        function Limpar() {
            grid.getStore().removeAll();
            storeVagoes.removeAll();
            Ext.getCmp("txtVagoes").setDisabled(false);
            Ext.getCmp("filtros").getForm().reset();
        }

        /* endregion :: Functions */

        /* region :: Stores */

        var clienteStore = new window.Ext.data.JsonStore({
            id: 'clienteStore',
            name: 'clienteStore',
            root: 'Items',
            url: '<%=Url.Action("ObterClientesDistintos", "PainelExpedicao") %>',
            fields: [
                    'DescResumida'
                ],
            listeners: {
                load: function (store) {
                    var rt = store.recordType;
                    store.insert(0, new rt({}));
                }
            }
        });
        window.clienteStore = clienteStore;

        var remetenteStore = new window.Ext.data.JsonStore({
            id: 'clienteStore',
            name: 'clienteStore',
            root: 'Items',
            url: '<%=Url.Action("ObterRemetentes", "PainelExpedicao") %>',
            fields: [
                    'DescResumida'
                ],
            listeners: {
                load: function (store) {
                    var rt = store.recordType;
                    store.insert(0, new rt({}));
                }
            }
        });
        window.remetenteStore = remetenteStore;

        var recebedorStore = new window.Ext.data.JsonStore({
            id: 'recebedorStore',
            name: 'recebedorStore',
            root: 'Items',
            url: '<%=Url.Action("ObterRecebedores", "PainelExpedicao") %>',
            fields: [
                    'DescResumida'
                ],
            listeners: {
                load: function (store) {
                    var rt = store.recordType;
                    store.insert(0, new rt({}));
                }
            }
        });
        window.recebedorStore = recebedorStore;
        
        var expedidorStore = new window.Ext.data.JsonStore({
            id: 'expedidorStore',
            name: 'expedidorStore',
            root: 'Items',
            url: '<%=Url.Action("ObterExpedidores", "PainelExpedicao") %>',
            fields: [
                    'DescResumida'
                ],
            listeners: {
                load: function (store) {
                    var rt = store.recordType;
                    store.insert(0, new rt({}));
                }
            }
        });
        window.expedidorStore = expedidorStore;

        var segmentosStore = new Ext.data.JsonStore({
            root: "Items",
            autoLoad: true,
            proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("ObterSegmentos", "PainelExpedicao") %>',
                timeout: 600000
            }),
            id: 'segmentosStore',
            fields: [
                'Id',
                'Nome'
            ]
        });
        
        var malhaOrigemStore = new Ext.data.JsonStore({
            root: "Items",
            autoLoad: true,
            proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("ObterMalhas", "PainelExpedicao") %>',
                timeout: 600000
            }),
            id: 'malhaOrigemStore',
            fields: [
                'Id',
                'Nome'
            ]
        });

        var storeVagoes = new Ext.data.JsonStore({
        remoteSort: true,
        root: "Items",
        fields: 
            [
                'Vagao',
                'Erro',
                'Mensagem'
            ]
        });

        // Filtros Linha 1 //

        var dataInicio = {
            xtype: 'datefield',
            fieldLabel: 'Data Início',
            id: 'dataInicio',
            name: 'dataInicio',
            width: 85,
            allowBlank: false,
            value: new Date()
        };

        var dataFim = {
            xtype: 'datefield',
            fieldLabel: 'Data Fim',
            id: 'dataFim',
            name: 'dataFim',
            width: 85,
            allowBlank: false,
            value: new Date()
        };

        var txtFluxo = {
            xtype: 'textfield',
            name: 'txtFluxo',
            id: 'txtFluxo',
            fieldLabel: 'Fluxo',
            autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '7' },
            maskRe: /[a-zA-Z0-9]/,
            style: 'text-transform: uppercase',
            width: 70
        };

        var txtOrigem = {
            xtype: 'textfield',
            name: 'txtOrigem',
            id: 'txtOrigem',
            fieldLabel: 'Origem',
            autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '3' },
            maskRe: /[a-zA-Z]/,
            style: 'text-transform: uppercase',
            width: 50
        };

        var txtDestino = {
            xtype: 'textfield',
            name: 'txtDestino',
            id: 'txtDestino',
            fieldLabel: 'Destino',
            autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '3' },
            maskRe: /[a-zA-Z]/,
            style: 'text-transform: uppercase',
            width: 50
        };

        var txtVagoes = {
            xtype: 'textfield',
            name: 'txtVagoes',
            id: 'txtVagoes',
            fieldLabel: 'Vagão',
            maskRe: /[a-zA-Z0-9\;]/,
            maxLength: 100,
            width: 100,
            enableKeyEvents: true,
            listeners: {
                specialkey: function (f, e) {
                    if (e.getKey() == e.ENTER) {
                        Pesquisar();
                    }
                }
            }
        };

        var btnLoteVagao = {
            xtype: 'button',
            text: 'Informar Vagões',
            iconCls: 'icon-find',
            handler: function (b, e) {
                windowStatusVagoes = new Ext.Window({
                                        id: 'windowStatusVagoes',
                                        title: 'Informar vagões',
                                        modal: true,
                                        width: 855,
                                        resizable: false,
                                        height: 380,
                                        autoLoad: {
                                        url: '<%= Url.Action("FormInformarVagoes") %>',
                                        scripts: true
                                        }
                                     });
                windowStatusVagoes.show();
            }
        };

        var txtPrefixo = {
            xtype: 'textfield',
            name: 'txtPrefixo',
            id: 'txtPrefixo',
            fieldLabel: 'Prefixo',
            autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '3' },
            style: 'text-transform: uppercase',
            width: 50
        };

        var arrPrefixo = {
            width: 55,
            layout: 'form',
            border: false,
            items: [txtPrefixo]
        };

        var txtOs = {
            xtype: 'textfield',
            name: 'txtOs',
            id: 'txtOs',
            fieldLabel: 'OS',
            autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '7' },
            maskRe: /[0-9]/,
            style: 'text-transform: uppercase',
            width: 60
        };

        var arrOs = {
            width: 65,
            layout: 'form',
            border: false,
            items: [txtOs]
        };

        var arrDataIni = {
            width: 90,
            layout: 'form',
            border: false,
            items: [dataInicio]
        };

        var arrDataFim = {
            width: 90,
            layout: 'form',
            border: false,
            items: [dataFim]
        };

        var arrFluxo = {
            width: 75,
            layout: 'form',
            border: false,
            items: [txtFluxo]
        };

        var arrOrigem = {
            width: 55,
            layout: 'form',
            border: false,
            items: [txtOrigem]
        };

        var arrDestino = {
            width: 55,
            layout: 'form',
            border: false,
            items: [txtDestino]
        };
        
        var ddlMalhaOrigem = new Ext.form.ComboBox({
            typeAhead: false,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            store: malhaOrigemStore,
            valueField: 'Id',
            width: 100,
            displayField: 'Nome',
            fieldLabel: 'Malha',
            id: 'ddlMalhaOrigem'
        });
        
        var arrMalhaOrigem = {
            width: 105,
            layout: 'form',
            border: false,
            items: [ddlMalhaOrigem]
        };

        var arrVagoes = {
            width: 105,
            layout: 'form',
            border: false,
            items: [txtVagoes]
        };

        var arrBtnLoteVagoes = {
            width: 120,
            layout: 'form',
            style: 'padding: 16px 0px 0px 0px;',
            border: false,
            items: [btnLoteVagao]
        };
        
        var chkOutrasFerrovias = {
            xtype: 'checkbox',
            fieldLabel: '',
            cls: 'chkOutrasFerrovias',
            id: 'chkOutrasFerrovias',
            name: 'chkOutrasFerrovias',
            validateField: true,
            value: false
        };
        
        var arrOutrasFerrovias = {
            width: 15,
            layout: 'form',
            border: false,
            items: [chkOutrasFerrovias]
        };
        
        var lbOutrasFerrovias = {
            xtype: 'label',
            fieldLabel: '',
            text: 'Outras Ferrovias',
            cls: 'lbOutrasFerrovias',
            id: 'lbOutrasFerrovias'
        };
        
        var arrLabelOutrasFerrovias = {
            width: 120,
            layout: 'form',
            border: false,
            items: [lbOutrasFerrovias]
        };

        var arrlinha1 = {
            layout: 'column',
            border: false,
            items: [
                arrDataIni, 
                arrDataFim, 
                arrFluxo, 
                arrOrigem, 
                arrDestino, 
                arrPrefixo, 
                arrOs,
                arrMalhaOrigem,
                arrVagoes, 
                arrBtnLoteVagoes,
                arrOutrasFerrovias,
                arrLabelOutrasFerrovias
            ]
        };

        // Fim Filtros Linha 1 //

        // Filtros Linha 2 //

        var ddlCliente = {
            xtype: 'combo',
            id: 'ddlCliente',
            name: 'ddlCliente',
            forceSelection: true,
            store: window.clienteStore,
            triggerAction: 'all',
            mode: 'remote',
            typeAhead: true,
            minChars: 3,
            fieldLabel: 'Correntista',
            displayField: 'DescResumida',
            valueField: 'DescResumida',
            width: 175,
            editable: true,
            tpl: '<tpl for="."><div class="x-combo-list-item">{DescResumida}&nbsp;</div></tpl>'
        };

        var ddlExpedidor = {
            xtype: 'combo',
            id: 'ddlExpedidor',
            name: 'ddlExpedidor',
            forceSelection: true,
            store: window.expedidorStore,
            triggerAction: 'all',
            mode: 'remote',
            typeAhead: true,
            minChars: 1,
            fieldLabel: 'Expedidor',
            displayField: 'DescResumida',
            valueField: 'DescResumida',
            width: 125,
            editable: true,
            tpl: '<tpl for="."><div class="x-combo-list-item">{DescResumida}&nbsp;</div></tpl>'
        };

        var ddlRemetente = {
            xtype: 'combo',
            id: 'ddlRemetente',
            name: 'ddlRemetente',
            forceSelection: true,
            store: window.remetenteStore,
            triggerAction: 'all',
            mode: 'remote',
            typeAhead: true,
            minChars: 3,
            fieldLabel: 'Remetente',
            displayField: 'DescResumida',
            valueField: 'DescResumida',
            width: 125,
            editable: true,
            tpl: '<tpl for="."><div class="x-combo-list-item">{DescResumida}&nbsp;</div></tpl>'
        };
        
        var ddlRecebedor = {
            xtype: 'combo',
            id: 'ddlRecebedor',
            name: 'ddlRecebedor',
            forceSelection: true,
            store: window.recebedorStore,
            triggerAction: 'all',
            mode: 'remote',
            typeAhead: true,
            minChars: 3,
            fieldLabel: 'Recebedor',
            displayField: 'DescResumida',
            valueField: 'DescResumida',
            width: 125,
            editable: true,
            tpl: '<tpl for="."><div class="x-combo-list-item">{DescResumida}&nbsp;</div></tpl>'
        };
        
        var txtMercadorias = {
            xtype: 'textfield',
            name: 'txtMercadorias',
            id: 'txtMercadorias',
            fieldLabel: 'Lista de Mercadorias(separados por ";")',
            maskRe: /[a-zA-Z0-9\;]/,
            maxLength: 500,
            width: 250,
            enableKeyEvents: true,
            listeners: {
                specialkey: function (f, e) {
                    if (e.getKey() == e.ENTER) {
                        Pesquisar();
                    }
                }
            }
        };

        var ddlSegmento = new Ext.form.ComboBox({
            typeAhead: false,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            store: segmentosStore,
            valueField: 'Id',
            width: 110,
            displayField: 'Nome',
            fieldLabel: 'Segmento',
            id: 'ddlSegmento'
        });
        
        var arrCliente = {
            width: 180,
            layout: 'form',
            border: false,
            items: [ddlCliente]
        };

        var arrExpedidor = {
            width: 130,
            layout: 'form',
            border: false,
            items: [ddlExpedidor]
        };

        var arrRemetente = {
            width: 130,
            layout: 'form',
            border: false,
            items: [ddlRemetente]
        };

        var arrRecebedor = {
            width: 130,
            layout: 'form',
            border: false,
            items: [ddlRecebedor]
        };
        
        var arrMercadoria = {
            width: 255,
            layout: 'form',
            border: false,
            items: [txtMercadorias]
        };

        var arrSegmento = {
            width: 115,
            layout: 'form',
            border: false,
            items: [ddlSegmento]
        };
        
        var arrlinha2 = {
            layout: 'column',
            border: false,
            items: [
                arrCliente, 
                arrExpedidor, 
                arrRemetente,
                arrRecebedor,
                arrMercadoria, 
                arrSegmento
            ]
        };
        
        // Filtros Linha 2 //

        // Filtros Linha 3 //

        var chkStatusCTe = {
            xtype: 'checkbox',
            fieldLabel: 'CT-e',
            id: 'chkStatusCTe',
            name: 'chkStatusCTe',
            validateField: true,
            value: false
        };

        var chkStatusNFe = {
            xtype: 'checkbox',
            fieldLabel: 'NF-e',
            id: 'chkStatusNFe',
            name: 'chkStatusNFe',
            validateField: true,
            value: false
        };
        
        var chkStatusWebDanfe = {
            xtype: 'checkbox',
            fieldLabel: 'Danfe',
            id: 'chkStatusWebDanfe',
            cls: 'chkStatusWebDanfe',
            name: 'chkStatuswebDanfe',
            validateField: true,
            value: false
        };

        var chkStatusTicket = {
            xtype: 'checkbox',
            fieldLabel: 'Ticket',
            id: 'chkStatusTicket',
            name: 'chkStatusTicket',
            validateField: true,
            value: false
        };

        var chkStatusDcl = {
            xtype: 'checkbox',
            fieldLabel: 'DCL',
            id: 'chkStatusDcl',
            name: 'chkStatusDcl',
            validateField: true,
            value: false
        };
        
        var chkStatusLaudoMercadoria = {
            xtype: 'checkbox',
            fieldLabel: 'Laudo',
            id: 'chkStatusLaudoMercadoria',
            name: 'chkStatusLaudoMercadoria',
            validateField: true,
            value: false
        };

        var arrStatusCTe = {
            width: 50,
            layout: 'form',
            border: false,
            items: [chkStatusCTe]
        };
        
        var arrStatusNFe = {
            width: 50,
            layout: 'form',
            border: false,
            items: [chkStatusNFe]
        };
        
        var arrStatusWebDanfe = {
            width: 60,
            layout: 'form',
            border: false,
            items: [chkStatusWebDanfe]
        };
        
        var arrStatusTicket = {
            width: 50,
            layout: 'form',
            border: false,
            items: [chkStatusTicket]
        };

        var arrStatusDcl = {
            width: 50,
            layout: 'form',
            border: false,
            items: [chkStatusDcl]
        };
        
        var arrStatusLaudoMercadoria = {
            width: 50,
            layout: 'form',
            border: false,
            items: [chkStatusLaudoMercadoria]
        };

        var arrlinha3 = {
            layout: 'column',
            border: false,
            width: 1000,
            bodyStyle: 'margin-left: 350px',
            items: [
                arrStatusCTe,
                arrStatusNFe,
                arrStatusWebDanfe,
                arrStatusTicket, 
                arrStatusDcl, 
                arrStatusLaudoMercadoria
            ]
        };

        // Filtros Linha 3 //

        var arrCampos = new Array();
        arrCampos.push(arrlinha1);
        arrCampos.push(arrlinha2);
        arrCampos.push(arrlinha3);

        var filtros = new Ext.form.FormPanel({
            id: 'filtros',
            title: 'Filtros',
            name: 'filtros',
            region: 'center',
            bodyStyle: 'padding: 15px',
            initEl: function (el) {
                this.el = Ext.get(el);
                this.id = this.el.id || Ext.id();
                if (this.standardSubmit) {
                    this.el.dom.action = this.url;
                } else {
                    this.el.on('submit', this.onSubmit, this);
                }
                this.el.addClass('x-form');
            },
            //layout: 'form',
            labelAlign: 'top',
            border: false,
            items: [arrCampos],
            buttonAlign: 'center',
            buttons: [
                {
                    name: 'btnImprimir',
                    id: 'btnImprimir',
                    text: 'Imprimir',
                    iconCls: 'icon-printer',
                    handler: Imprimir
                },
                {
                    name: 'btnPesquisar',
                    id: 'btnPesquisar',
                    text: 'Pesquisar',
                    iconCls: 'icon-find',
                    handler: Pesquisar
                },
                {
                    name: 'btnLimpar',
                    id: 'btnLimpar',
                    text: 'Limpar',
                    handler: Limpar
                },
                {
                    name: 'btnExportarRelatorio',
                    id: 'btnExportarRelatorio',
                    text: 'Exportar',
                    iconCls: 'icon-page-excel',
                    handler: function (c) {

                        var selecionados = RetornaDadosGrid(); //Itens Despacho selecionados

                        var ddlCliente = Ext.getCmp("ddlCliente");
                        var clienteId = ddlCliente.getValue();

                        var ddlExpedidor = Ext.getCmp("ddlExpedidor");
                        var expedidorId = ddlExpedidor.getValue();

                        var ddlRemetente = Ext.getCmp("ddlRemetente");
                        var remetenteId = ddlRemetente.getValue();

                        var ddlRecebedor = Ext.getCmp("ddlRecebedor");
                        var recebedorId = ddlRecebedor.getValue();
                        
                        var ddlSegmento = Ext.getCmp("ddlSegmento");
                        var segmentoId = ddlSegmento.getValue();

                        var ddlMalhaOrigem = Ext.getCmp("ddlMalhaOrigem");
                        var malhaOrigemId = ddlMalhaOrigem.getValue();

                        var dataInicio = Ext.getCmp("dataInicio").getValue();
                        var dataFim = Ext.getCmp("dataFim").getValue();

                        if (selecionados.data.ids.length > 0) {

                            // Lógica para chamar Action passando os IdsItemDespacho
                            // e colocar na Session os Ids e depois chamar método para Exportar
                            // somente os Itens da Session

                            var parametros = {
                                dataInicio: undefined,
                                dataFim: undefined,
                                fluxo: undefined,
                                origem: undefined,
                                destino: undefined,
                                vagoes: undefined,
                                cliente: undefined,
                                expedidor: undefined,
                                remetente: undefined,
                                recebedor: undefined,
                                mercadorias: undefined,
                                segmento: undefined,
                                os: undefined,
                                prefixo: undefined,
                                statusCte: undefined,
                                statusNfe: undefined,
                                statusTicket: undefined,
                                statusDcl: undefined,
                                statusLaudoMercadoria: undefined,
                                itensDespacho: undefined,
                                malhaOrigem: undefined
                            };

                            parametros.dataInicio = dataInicio.format('d/m/Y');
                            parametros.dataFim = dataFim.format('d/m/Y');
                            parametros.fluxo = Ext.getCmp("txtFluxo").getValue();
                            parametros.origem = Ext.getCmp("txtOrigem").getValue();
                            parametros.destino = Ext.getCmp("txtDestino").getValue();
                            parametros.vagoes = RetornaVagoes();
                            parametros.cliente = clienteId;
                            parametros.expedidor = expedidorId;
                            parametros.remetente = remetenteId;
                            parametros.recebedor = recebedorId;
                            parametros.mercadorias = Ext.getCmp("txtMercadorias").getValue();
                            parametros.segmento = segmentoId;
                            parametros.os = Ext.getCmp("txtOs").getValue();
                            parametros.prefixo = Ext.getCmp("txtPrefixo").getValue();
                            parametros.statusCte = false;
                            parametros.statusNfe = false;
                            parametros.statusTicket = false;
                            parametros.statusDcl = false;
                            parametros.statusLaudoMercadoria = false;
                            parametros.outrasFerrovias = Ext.getCmp("chkOutrasFerrovias").getValue();

                            parametros.itensDespacho = new Array();
                            for (var i = 0; i < selecionados.data.ids.length; i++) {
                                parametros.itensDespacho.push(selecionados.data.ids[i]);
                            }

                            parametros.malhaOrigem = malhaOrigemId;

                            $.ajax({
                                url: '<%= Url.Action("ExportarHistoricoFaturamentosVagoesSelecionados") %>',
                                type: "POST",
                                cache: false,
                                data: JSON.stringify(parametros),
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                success: function (response) {
                                    if (response.success) {
                                        if (response.success == true) {
                                            var url = '<%= this.Url.Action("DownloadArquivoExcel") %>';
                                            url += "?fileGuid=" + response.FileGuid + "&filename=" + response.FileName;
                                            window.open(url, "");
                                        }
                                    } else {
                                        Ext.Msg.show({
                                            title: 'Aviso',
                                            msg: 'Falha na exportação do relatório',
                                            buttons: Ext.Msg.OK,
                                            icon: Ext.MessageBox.ERROR
                                        });
                                    }
                                },
                                error: function (data) {
                                    Ext.Msg.show({
                                        title: 'Aviso',
                                        msg: 'Falha na exportação do relatório',
                                        buttons: Ext.Msg.OK,
                                        icon: Ext.MessageBox.ERROR
                                    });
                                },
                                complete: function (data) {
                                }
                            });
                        } else {

                            var url = '<%= this.Url.Action("ObterHistoricoFaturamentos") %>';

                            url += "?dataInicio=" + dataInicio.format('d/m/Y') +
                                    "&dataFim=" + dataFim.format('d/m/Y') +
                                    "&fluxo=" + Ext.getCmp("txtFluxo").getValue() +
                                    "&origem=" + Ext.getCmp("txtOrigem").getValue() +
                                    "&destino=" + Ext.getCmp("txtDestino").getValue() +
                                    "&vagoes=" + RetornaVagoes() +
                                    "&cliente=" + clienteId +
                                    "&expedidor=" + expedidorId +
                                    "&remetente=" + remetenteId +
                                    "&recebedor=" + recebedorId +
                                    "&mercadorias=" + Ext.getCmp("txtMercadorias").getValue() +
                                    "&segmento=" + segmentoId +
                                    "&os=" + Ext.getCmp("txtOs").getValue() +
                                    "&prefixo=" + Ext.getCmp("txtPrefixo").getValue() +
                                    "&statusCte=" + 'false' +
                                    "&statusNfe=" + 'false' +
                                    "&statusTicket=" + 'false' +
                                    "&statusDcl=" + 'false' +
                                    "&statusLaudoMercadoria=" + 'false' +
                                    "&outrasFerrovias=" + Ext.getCmp("chkOutrasFerrovias").getValue() +
                                    "&exportarExcel=true" +
                                    "&malhaOrigem=" + malhaOrigemId;

                            window.open(url, "");
                        }
                    }
                }
            ]
        });

        /* endregion :: Filtros*/

        /* region :: Grid */

        var sm = new Ext.grid.CheckboxSelectionModel({
            listeners: {
                selectionchange: function (sm) {
                    var recLen = Ext.getCmp('grid').store.getRange().length;
                    var selectedLen = this.selections.items.length;
                    if (selectedLen == recLen) {
                        var view = Ext.getCmp('grid').getView();
                        var chkdiv = Ext.fly(view.innerHd).child(".x-grid3-hd-checker");
                        chkdiv.addClass("x-grid3-hd-checker-on");
                    }
                }
            },
            rowdeselect: function (sm, rowIndex, record) {
                var view = Ext.getCmp('grid').getView();
                var chkdiv = Ext.fly(view.innerHd).child(".x-grid3-hd-checker");
                chkdiv.removeClass('x-grid3-hd-checker-on');
            }
        });

        var cm = new Ext.grid.ColumnModel({
            defaults: {
                sortable: true
            },
            columns: [
                sm,
                {
                    dataIndex: "Id", hidden: true
                },
                {
                    header: 'Vagão',
                    dataIndex: 'Vagao',
                    width: 55
                },
                {
                    header: 'Data Carga',
                    dataIndex: 'DataCarga',
                    width: 80,
                    align: 'left'
                },
                {
                    header: 'Fluxo',
                    dataIndex: 'Fluxo',
                    width: 60
                },
                {
                    header: 'Origem',
                    dataIndex: 'Origem',
                    width: 50
                },
                {
                    header: 'Destino',
                    dataIndex: 'Destino',
                    width: 50
                },
                {
                    header: 'Correntista',
                    dataIndex: 'Correntista',
                    width: 100
                },
                {
                    header: 'Expedidor',
                    dataIndex: 'Expedidor',
                    width: 100
                },
                {
                    header: 'Remetente',
                    dataIndex: 'RemetenteFiscal',
                    width: 100
                },
                {
                    header: 'Recebedor',
                    dataIndex: 'Recebedor',
                    width: 100
                },
                {
                    header: 'Mercadoria',
                    dataIndex: 'Mercadoria',
                    width: 100
                },
                {
                    header: 'OS',
                    dataIndex: 'Os',
                    width: 55
                },
                {
                    header: 'Prefixo',
                    dataIndex: 'Prefixo',
                    width: 55
                },
                {
                    header: 'MDF-e',
                    dataIndex: 'IdMdfe',
                    width: 50,
                    align: "center",
                    sortable: false,
                    renderer: statusMdfePdfRenderer
                },
                {
                    header: 'Seq',
                    dataIndex: 'Seq',
                    width: 35
                },
                {
                    header: 'Ticket',
                    dataIndex: 'PossuiTicket',
                    width: 40,
                    renderer: function (value, metaData, record, rowIndex,
                                    colIndex, store, view) {
                        if (value.toUpperCase() == 'S')
                            return 'Sim';
                        else
                            return 'Não';
                    }
                },
                {
                    header: 'Cte',
                    dataIndex: 'CteStatus',
                    width: 40,
                    renderer: function (value, metaData, record, rowIndex,
                                    colIndex, store, view) {

                        if (!value)
                            return 'Não';
                        else {
                            if (value.toUpperCase() == 'AUT')
                                return 'Sim';
                            else
                                return 'Não';
                        }

                    }
                },
                {
                    header: 'NFe',
                    dataIndex: 'PossuiNfe',
                    width: 40,
                    renderer: function (value, metaData, record, rowIndex,
                                    colIndex, store, view) {


                        if (value) {
                            if (value.toUpperCase() == 'S')
                                return 'Sim';
                            else
                                return 'Não';
                        }
                        else
                            return 'Não';
                    }
                },
                {
                    header: 'Danfe',
                    dataIndex: 'PossuiWebDanfe',
                    width: 80,
                    renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
                        if (value) {
                            if (value.toUpperCase() == 'S')
                                return 'Sim';
                            else
                                return 'Não';
                        }
                        else
                            return 'Não';
                    }
                },
                {
                    header: 'PDFs Recebidos',
                    dataIndex: 'QuantidadePdfsEnviados',
                    width: 55
                },
                {
                    header: 'PDFs Totais',
                    dataIndex: 'QuantidadeTotalPdfs',
                    width: 55
                },
                {
		            header: 'XML NFe',
		            dataIndex: 'PossuiXmlNfe',
		            width: 60,
		            renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
			            if (value.toUpperCase() == 'S')
				            return 'Sim';
			            else
				            return 'Não';
		            }
	            },
                {
                    header: 'Dcl',
                    dataIndex: 'PossuiDcl',
                    width: 40,
                    renderer: function (value, metaData, record, rowIndex,
                                    colIndex, store, view) {
                        if (value) {
                            if (value.toUpperCase() == 'S')
                                return 'Sim';
                            else
                                return 'Não';
                        }
                        else
                            return 'Não';
                    }
                },
                {
                    header: 'Laudo Mercadoria',
                    dataIndex: 'PossuiLaudoMercadoria',
                    width: 50,
                    renderer: function (value, metaData, record, rowIndex,
                                    colIndex, store, view) {
                        if (value) {
                            if (value.toUpperCase() == 'S')
                                return 'Sim';
                            else
                                return 'Não';
                        }
                        else
                            return 'Não';
                    },
                },
                {
                    header: 'XMLs Nfe',
                    dataIndex: 'QtdXmlNfe',
                    width: 60
                },
                {
                    header: 'XMLs não Recebidos',
                    dataIndex: 'XmlNaoRecebidos',
                    width: 160
                }
            ]
        });

        var grid = new Translogic.PaginatedGrid({
            id: 'grid',
            name: 'grid',
            autoLoadGrid: false,
            region: 'center',
            viewConfig: {
                emptyText: 'Não possui dado(s) para exibição.'
            },
            loadMask: { msg: App.Resources.Web.Carregando },
            sm: sm,
            fields: [
                'Id',
                'IdCte',
                'Vagao',
                'DataCarga',
                'Fluxo',
                'Origem',
                'Destino',
                'Correntista',
                'Expedidor',
                'RemetenteFiscal',
                'Recebedor',
                'Mercadoria',
                'Os',
                'Prefixo',
                'Seq',
                'PossuiTicket',
                'CteStatus',
                'PossuiNfe',
                'PossuiWebDanfe', 
                'QuantidadePdfsEnviados',
                'QuantidadeTotalPdfs',
                'PossuiDcl',
                'PossuiLaudoMercadoria',
                'IdMdfe',
                'PossuiXmlNfe',
                'QtdXmlNfe',
                'XmlNaoRecebidos'
            ],
            cm: cm,
            stripeRows: true,
            url: '<%= Url.Action("ObterHistoricoFaturamentos", "PainelExpedicao") %>'
        });

        grid.getStore().proxy.on('beforeload', function (p, params) {
            var ddlCliente = Ext.getCmp("ddlCliente");
            var clienteId = ddlCliente.getValue();

            var ddlExpedidor = Ext.getCmp("ddlExpedidor");
            var expedidorId = ddlExpedidor.getValue();

            var ddlRemetente = Ext.getCmp("ddlRemetente");
            var remetenteId = ddlRemetente.getValue();

            var ddlRecebedor = Ext.getCmp("ddlRecebedor");
            var recebedorId = ddlRecebedor.getValue();
            
            var ddlSegmento = Ext.getCmp("ddlSegmento");
            var segmentoId = ddlSegmento.getValue();

            var ddlMalhaOrigem = Ext.getCmp("ddlMalhaOrigem");
            var malhaOrigemId = ddlMalhaOrigem.getValue();

            var dataInicio = Ext.getCmp("dataInicio").getValue();
            var dataFim = Ext.getCmp("dataFim").getValue();
            
            var vagoes = '';
            if(storeVagoes.getCount() != 0)
            {
                storeVagoes.each(
                    function (record) {
                        if (!record.data.Erro) {
                            vagoes += record.data.Vagao + ";";
                        }
                    }
                );
            } else {
                vagoes = Ext.getCmp("txtVagoes").getValue();
            }

            params['dataInicio'] = dataInicio.format('d/m/Y');
            params['dataFim'] = dataFim.format('d/m/Y');
            params['fluxo'] = Ext.getCmp("txtFluxo").getValue();
            params['origem'] = Ext.getCmp("txtOrigem").getValue();
            params['destino'] = Ext.getCmp("txtDestino").getValue();
            params['vagoes'] = vagoes;
            params['cliente'] = clienteId;
            params['expedidor'] = expedidorId;
            params['remetente'] = remetenteId;
            params['recebedor'] = recebedorId;
            params['mercadorias'] = Ext.getCmp("txtMercadorias").getValue();
            params['segmento'] = segmentoId;
            params['os'] = Ext.getCmp("txtOs").getValue();
            params['prefixo'] = Ext.getCmp("txtPrefixo").getValue();
            params['statusCte'] = false;
            params['statusNfe'] = false;
            params['statusWebDanfe'] = false;
            params['statusTicket'] = false;
            params['statusDcl'] = false;
            params['statusLaudoMercadoria'] = false;
            params['outrasFerrovias'] = Ext.getCmp("chkOutrasFerrovias").getValue();
            params['exportarExcel'] = false;
            params['malhaOrigem'] = malhaOrigemId;

            // Modificando o tamanho da primeira coluna apenas para ajustar o tamanho da colunas que ExtJS gera errado
            var colMod = grid.getColumnModel();
            colMod.setColumnWidth(1, 55);
        });

        /* endregion :: Grid */

        /************************* RENDER **************************/
        new Ext.Viewport({
            layout: 'border',
            margins: 10,
            items: [
			{
			    region: 'north',
			    height: 300,
			    autoScroll: true,
			    items: [{
			        region: 'center',
			        applyTo: 'header-content'
			    },
				filtros]
			},
			grid
		]

        });

        $('#lbOutrasFerrovias').parent().css("margin-top", "15px");
    </script>
</asp:Content>