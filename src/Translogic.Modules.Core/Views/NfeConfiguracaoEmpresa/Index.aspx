﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Interna.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content runat="server" ID="HeadContent" ContentPlaceHolderID="HeadContent">
    <script type="text/javascript" language="javascript">

        /************************* BOTOES DA GRID **************************/
        var botaoGridEditar = new Ext.ux.grid.RowActions({
            dataIndex: '',
            header: '',
            align: 'center',
            actions: [{
                iconCls: 'icon-edit',
                tooltip: 'Editar Registro'
            }],
            callbacks: {
                'icon-edit': function (grid, record, action, row, col) {
                    var id = grid.getStore().getAt(row).get('Id');
                    MostrarFormulario(id, "Editar");
                }
            }
        });

        var botaoGridExcluir = new Ext.ux.grid.RowActions({
            dataIndex: '',
            header: '',
            align: 'center',
            actions: [{
                iconCls: 'icon-del',
                tooltip: 'Excluir Registro'
            }],
            callbacks: {
                'icon-del': function (grid, record, action, row, col) {
                    Ext.Msg.show({
                        scope: {
                            record: record
                        },
                        title: 'Atenção',
                        msg: 'Deseja excluir o registro?',
                        buttons: Ext.Msg.YESNO,
                        fn: function (btn) {
                            if (btn == 'no') {
                                return;
                            } else {
                                DeletarRegistro(this.record.data.Id);
                            }
                        },
                        icon: Ext.MessageBox.QUESTION
                    });
                }
            }
        });

        /************************* GRID **************************/
        var grid = new Translogic.PaginatedGrid({
            id: 'grid',
            name: 'grid',
            autoLoadGrid: false,
            height: 400,
            fields: [
            'Id',
            'Cnpj',
            'PriorizarProdutos',
            'PriorizarVolume'
        ],
            url: '<%= Url.Action("Pesquisar") %>',
            plugins: [botaoGridEditar, botaoGridExcluir],
            columns: [
            new Ext.grid.RowNumberer(),
            { header: 'CNPJ', width: 100, dataIndex: "Cnpj", sortable: false },
            { header: 'Priorizar Produtos', width: 200, dataIndex: "PriorizarProdutos", sortable: false },
            { header: 'Priorizar Volume', width: 200, dataIndex: "PriorizarVolume", sortable: false },
            botaoGridEditar,
            botaoGridExcluir
        ],
            filteringToolbar: [{
                xtype: 'button',
                name: 'btnNovo',
                id: 'btnNovo',
                text: 'Novo Registro',
                iconCls: 'icon-new',
                handler: function () {
                    MostrarFormulario(null, "Cadastrar Novo");
                }
            }]
        });

        grid.getStore().proxy.on('beforeload', function (p, params) {
            params['cnpj'] = Ext.getCmp('txtDescricaoFiltro').getValue();
        });

        /************************* FUNCOES **************************/
        function DeletarRegistro(id) {
            Ext.Ajax.request({
                url: '<%= Url.Action("Excluir") %>',
                params:
                {
                    id: id
                },
                success: function (result) {
                    var data = Ext.decode(result.responseText);
                    if (data.success) {
                        grid.getStore().load();
                        Ext.Msg.show({
                            title: 'Aviso',
                            msg: 'Registro excluido com Sucesso!',
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.INFO
                        });
                    } else {
                        Ext.Msg.show({
                            title: 'Aviso',
                            msg: data.message,
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.INFO,
                            fn: function () {
                            }
                        });
                    }
                }
            });
        }


        var formCadastro;

        function MostrarFormulario(id, acao) {
            formCadastro = null;

            formCadastro = new Ext.Window({
                name: 'formCadastro',
                id: 'formCadastro',
                title: acao + " Tipo de Equipamento",
                expandonShow: true,
                modal: true,
                width: 400,
                height: 180,
                closable: true,
                autoLoad:
                {
                    url: '<%= Url.Action("Form") %>',
                    params: { id: id },
                    scripts: true
                }
            });

            formCadastro.show();
        }

        /************************* FILTRO **************************/
        var filtros = new Ext.form.FormPanel({
            id: 'filtros',
            title: 'Filtros',
            name: 'filtros',
            initEl: function (el) {
                this.el = Ext.get(el);
                this.id = this.el.id || Ext.id();
                if (this.standardSubmit) {
                    this.el.dom.action = this.url;
                } else {
                    this.el.on('submit', this.onSubmit, this);
                }
                this.el.addClass('x-form');
            },
            layout: 'form',
            labelAlign: 'top',
            border: false,
            items: [{
                xtype: 'textfield',
                name: 'txtDescricaoFiltro',
                id: 'txtDescricaoFiltro',
                fieldLabel: 'CNPJ',
                maxLength: 50,
                width: 400,
                enableKeyEvents: true,
                listeners: {
                    specialkey: function (f, e) {
                        if (e.getKey() == e.ENTER) {
                            grid.getStore().load();
                        }
                    }
                }
            }],
            buttonAlign: 'center',
            buttons: [{
                name: 'btnPesquisar',
                id: 'btnPesquisar',
                text: 'Pesquisar',
                iconCls: 'icon-find',
                handler: function () {
                    grid.getStore().load();
                }
            }]
        });

        /************************* RENDER **************************/
        Ext.onReady(function () {
            filtros.render(document.body);
            grid.render(document.body);
        });
    
    </script>
</asp:Content>
<asp:Content runat="server" ID="Content2" ContentPlaceHolderID="Content">
    <div id="header-content">
        <h1>
            CTe - Configuração NFe por Empresa</h1>
        <small>Você está em CTe > Configuração NFe por Empresa</small>
        <br />
    </div>
</asp:Content>
