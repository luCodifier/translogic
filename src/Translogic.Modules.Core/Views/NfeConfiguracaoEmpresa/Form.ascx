﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%@ Import Namespace="Translogic.Modules.Core.Domain.Model.FluxosComerciais.Nfes" %>

<div id="div-form-novo-registro"></div>

<script type="text/javascript">

    /************************* MODEL **************************/
    <% var model = (NfeConfiguracaoEmpresa)ViewData["Model"]; %>
    var idRegistro = <%= model.Id %>;
    var cnpjRegistro = '<%= model.Cnpj %>';
    var priorizarProdutosRegistro = '<%= model.PriorizarProdutos %>';
    var priorizarVolumeRegistro = '<%= model.PriorizarVolume %>';
    
    /************************* STORE **************************/
    var storeBase = new Ext.data.SimpleStore({
        fields: ['value', 'id'],
        data: [['Sim', 'True'], ['Não', 'False']]
    });
    /************************* PAINEL **************************/
    var painel = new Ext.form.FormPanel({
        id: 'painel',
        name: 'painel',
        bodyStyle: 'padding: 15px',
        items: [{
            xtype: 'textfield',
            maxLength: 20,
            width: 200,
            name: 'txtCnpj',
            allowBlank: false,
            id: 'txtCnpj',
            fieldLabel: 'CNPJ'
        }
        , {
            xtype: 'combo',
            store: storeBase,
            name: 'cmbPriorizarProdutos',
            id: 'cmbPriorizarProdutos',
            width: 200,
            fieldLabel: 'Priorizar Produtos',
            mode: 'local',
            triggerAction: 'all',
            displayField: 'value',
            valueField: 'id',
            allowBlank: false,
            editable: false,
            selectOnFocus: true
        } , {
            xtype: 'combo',
            store: storeBase,
            name: 'cmbPriorizarVolume',
            id: 'cmbPriorizarVolume',
            width: 200,
            fieldLabel: 'Priorizar Volume',
            mode: 'local',
            triggerAction: 'all',
            displayField: 'value',
            valueField: 'id',
            allowBlank: false,
            editable: false,
            selectOnFocus: true
        }],
        buttonAlign: 'center',
        buttons: [{
            name: 'btnSalvar',
            id: 'btnSalvar',
            text: 'Salvar',
            handler: function () {
                if (!painel.form.isValid()) {
                    Ext.Msg.show({
                        title: 'Aviso',
                        msg: 'Por favor preencha os campo obrigátorios!',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                    });
                }
                else {
                    Salvar();
                }
            }
        }, {
            name: 'btnFechar',
            id: 'btnFechar',
            text: 'Fechar',
            handler: function () {
                Ext.getCmp('formCadastro').close();
            }
        }]
    });
    
    /************************* FUNÇÕES **************************/
    function Salvar() {
        cnpj = Ext.getCmp('txtCnpj').getValue().replace(/[^\d]+/g,'');
        priorizarProdutos = Ext.getCmp('cmbPriorizarProdutos').getValue();
        priorizarVolume = Ext.getCmp('cmbPriorizarVolume').getValue();
        
        Ext.Ajax.request(
	    {
	        url: '<%= Url.Action("Salvar") %>',
	        params:
			{
			    id: idRegistro,
			    cnpj: cnpj,
			    priorizarProdutos: priorizarProdutos,
			    priorizarVolume: priorizarVolume
			},
	        success: function (response) {
	            var result = Ext.decode(response.responseText);
	            
	            if(result.success) {
	                Ext.Msg.show({
	                    title: 'Aviso',
	                    msg: 'Registro Salvo com Sucesso!',
	                    buttons: Ext.Msg.OK,
	                    icon: Ext.MessageBox.INFO
	                });
	                
                    Ext.getCmp('grid').getStore().load();
	                Ext.getCmp('formCadastro').close();
                }
                else {
                     Ext.Msg.show({
	                    title: 'Ops...',
	                    msg: result.Message,
	                    buttons: Ext.Msg.OK,
	                    icon: Ext.MessageBox.ERROR
	                });
                }	
	        }
	    });
    }

    /************************* RENDER **************************/
    function Bind() {
        Ext.getCmp('txtCnpj').setValue(cnpjRegistro);
        Ext.getCmp('cmbPriorizarProdutos').setValue(priorizarProdutosRegistro);
        Ext.getCmp('cmbPriorizarVolume').setValue(priorizarVolumeRegistro);
    }

    Ext.onReady(function () {
        painel.render("div-form-novo-registro");
        Bind();
        Ext.getCmp('txtCnpj').focus();
    });
</script>
