﻿<%@ Import Namespace="System.Security.Policy" %>

<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Interna.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content runat="server" ID="HeadContent" ContentPlaceHolderID="HeadContent">
    <script type="text/javascript">
			
		// Flag para garantir que uma tela de mensagem que está sobreposta não chame novamente a tela de pesquisa
		var executarPesquisa = true;
		var lastRowSelected = -1;
		var lastDblClickRowSelected = -1;

		function showResult(btn) {
			executarPesquisa = true;
		}


    	var storeChaves = new Ext.data.JsonStore({
			remoteSort: true,
			root: "Items",
			fields: [
						'Chave',
						'Erro',
						'Mensagem'
					]
		});

        var dsEmail = new Ext.data.JsonStore({
            root: "Items",
            url: '<%= Url.Action("ObterEmailCte") %>',
            fields: [
                'Id',
                'IdCte',
                'Email',
                'TpEnvio',
				'TpArquivo',
                'Erro',
                'xml',
                'pdf'
			]
        });  

		function HabilitarCampos(bloquear)
		{
			Ext.getCmp("filtro-data-inicial").setDisabled(bloquear);
			Ext.getCmp("filtro-data-final").setDisabled(bloquear);
			Ext.getCmp("filtro-serie").setDisabled(bloquear);
			Ext.getCmp("filtro-despacho").setDisabled(bloquear);
			Ext.getCmp("filtro-fluxo").setDisabled(bloquear);
			Ext.getCmp("filtro-UfDcl").setDisabled(bloquear);
			Ext.getCmp("filtro-Ori").setDisabled(bloquear);
			Ext.getCmp("filtro-Dest").setDisabled(bloquear);
			Ext.getCmp("filtro-Chave").setDisabled(bloquear);
			Ext.getCmp("filtro-numVagao").setDisabled(bloquear);			
			Ext.getCmp("filtro-NumCte").setDisabled(bloquear);
			Ext.getCmp("filtro-Email").setDisabled(bloquear);
			Ext.getCmp("filtro-emailErro").setDisabled(bloquear);
		}

        function clickCheckBox(obj){
        
            var arrDados = obj.name.split("_");
            var tipoArquivo = arrDados[1];
            var idTmp = arrDados[2];
           var idx = gridEmail.getStore().find("Id", idTmp);
            var record = gridEmail.getStore().getAt(idx);

           if (tipoArquivo == "Xml"){
                record.data.xml = obj.checked;
            }

            if (tipoArquivo == "Pdf"){
                record.data.pdf = obj.checked;
            }
           
          }

            var  windowAlterarEmail = null;
            var  windowNovoEmail = null;
            var idCte = 0;

           
  

              var dsCodigoControle = new Ext.data.JsonStore({
                root: "Items",
                autoLoad: true,
                url: '<%= Url.Action("ObterCodigoSerieDesp") %>',
                fields: [
                            'Id',
                            'CodigoControle'
			            ]   		
            });       
            
		var fieldOrigem =	{
			xtype: 'textfield',
			vtype: 'cteestacaovtype',
			style: 'text-transform: uppercase',
			id: 'filtro-Ori',
			fieldLabel: 'Origem',
			autoCreate: { tag: 'input', type: 'text', maxlength: '3', autocomplete: 'off' },
			name: 'Origem',
			allowBlank: true,
			maxLength: 3,
			width: 35,
			hiddenName: 'Origem'
		};

		var fieldDestino = {
			xtype: 'textfield',
			vtype: 'cteestacaovtype',
			style: 'text-transform: uppercase',
			id: 'filtro-Dest',
			fieldLabel: 'Destino',
			autoCreate: { tag: 'input', type: 'text', maxlength: '3', autocomplete: 'off' },
			name: 'Destino',
			allowBlank: true,
			maxLength: 3,
			width: 35,
			hiddenName: 'Destino'
		};

		var fieldSerie = {
			xtype: 'textfield',
			vtype: 'ctesdvtype',
			style: 'text-transform: uppercase',
			id: 'filtro-serie',
			fieldLabel: 'Série',
			name: 'serie',
			allowBlank: true,
			autoCreate: { tag: 'input', type: 'text', maxlength: '3', autocomplete: 'off' },
			maxLength: 20,
			width: 30,
			hiddenName: 'serie'
		};

		var fieldDespacho = {
			xtype: 'textfield',
			vtype: 'ctedespachovtype',
			id: 'filtro-despacho',
			fieldLabel: 'Despacho',
			name: 'despacho',
			allowBlank: true,
			maxLength: 20,
			autoCreate: { tag: 'input', type: 'text', maxlength: '6', autocomplete: 'off' },
			width: 55,
			hiddenName: 'despacho'
		};

		var cboCteCodigoControle = {
			xtype: 'combo',
			store: dsCodigoControle,
			allowBlank: true,
			lazyInit: false,
			lazyRender: false, 
			mode: 'local',
			typeAhead: true,
			triggerAction: 'all',
			fieldLabel: 'UF DCL',
			name: 'filtro-UfDcl',
			id: 'filtro-UfDcl',
			hiddenName: 'filtro-UfDcl',
			displayField: 'CodigoControle',
			forceSelection: true,
			width: 70,
			valueField: 'Id',
			emptyText: 'Selecione...',
			editable: false,
			tpl : '<tpl for="."><div class="x-combo-list-item">{CodigoControle}&nbsp;</div></tpl>'		
		};

		var fieldVagao = {
			xtype: 'textfield',
			vtype: 'ctevagaovtype',
			style: 'text-transform: uppercase',
			id: 'filtro-numVagao',
			fieldLabel: 'Vagão',
			name: 'numVagao',
			allowBlank: true,
			autoCreate: { tag: 'input', type: 'text', maxlength: '7', autocomplete: 'off' },
			maxLength: 20,
			width: 55,
			maxValue: 9999999,
			hiddenName: 'numVagao'
		};

            var fluxo = {
                xtype: 'textfield',
				vtype: 'ctefluxovtype',
                style: 'text-transform: uppercase',
                id: 'filtro-fluxo',
                fieldLabel: 'Fluxo',
                name: 'fluxo',
                allowBlank: true,
                autoCreate: { tag: 'input', type: 'text', maxlength: '5', autocomplete: 'off' },
                maxLength: 20,
                width: 45,
								maxValue: 99999,
									minValue: 0,
                hiddenName: 'fluxo'
            };

		var btnLoteChaveNfe = {
			text: 'Informar Chaves',
			xtype: 'button',
			iconCls: 'icon-find',
			handler: function (b, e) {
					
				windowStatusNFe = new Ext.Window({
								id: 'windowStatusNFe',
								title: 'Informar chaves',
								modal: true,
								resizable: false,
								width: 855,
								height: 380,
								autoLoad: {
									url: '<%= Url.Action("FormInformarChaves") %>',
									scripts: true
								}
							});
							
							executarPesquisa = false;
							windowStatusNFe.show();			
								
			}
		};

        var dataAtual = new Date();

        var dataInicial = {
            xtype: 'datefield',
            fieldLabel: 'Data Inicial',
            id: 'filtro-data-inicial',
            name: 'dataInicial',
            width: 83,
            allowBlank: false,
            vtype: 'daterange',
            endDateField: 'filtro-data-final',
            hiddenName: 'dataInicial',
            value: dataAtual
        };

        var dataFinal = {
            xtype: 'datefield',
            fieldLabel: 'Data Final',
            id: 'filtro-data-final',
            name: 'dataFinal',
            width: 83,
            allowBlank: false,
            vtype: 'daterange',
            startDateField: 'filtro-data-inicial',
            hiddenName: 'dataFinal',
            value: dataAtual
        };

		var chave = {
				xtype: 'textfield',
				maskRe: /[0-9]/,
				id: 'filtro-Chave',
				fieldLabel: 'Chave CTe',
				name: 'Chave',
				allowBlank: true,
				autoCreate: { tag: 'input', type: 'text', maxlength: '44', autocomplete: 'off' },
				maxLength: 44,
				width: 275,
				hiddenName: 'Chave  '
			};
           
          var email = {
                xtype: 'textfield',
                id: 'filtro-Email',
                fieldLabel: 'E-mail',
                name: 'Email',
                allowBlank: true,
                maxLength: 100,
                width: 195,
                hiddenName: 'Email'
            };

            var chkEmailErro =  {
			    xtype: 'checkbox',
			    fieldLabel: 'CTe com log de erro',
			    id: 'filtro-emailErro',
			    name: 'emailErro',
			    hiddenName: 'emailErro',
                 width: 270,
			    validateField: true,
			    value: false
		    };

            var fieldCnpjTomador = {
                xtype:'textfield',
                fieldLabel: 'CNPJ Tomador',
                name: 'Tomador',
                id: 'filtro-Tomador',
                width: 115,
			    autoCreate: { tag: 'input', type: 'text', maxlength: '14', autocomplete: 'off' }
            };

            var fieldNumeroCte = {
                xtype:'numberfield',
                fieldLabel: 'Nro CTe',
                name: 'NumCte',
                id: 'filtro-NumCte',
                width: 95,
			    autoCreate: { tag: 'input', type: 'text', maxlength: '9', autocomplete: 'off' }
            };

			var arrbtnLoteChaveNfe = {
				width: 280,
				layout: 'form',
				style: 'padding: 17px 0px 0px 0px;',
				border: false,
				items:
						[btnLoteChaveNfe]
			};

            var arrCodigoDcl = {
                width: 75,
                layout: 'form',
                border: false,
                items:
					[cboCteCodigoControle]
            };

           var arrTomador = {
                width: 120,
                layout: 'form',
                border: false,
                items:
					[fieldCnpjTomador]
            };

			var arrChave = {
				width: 280,
				layout: 'form',
				border: false,
				items:
						[chave]
			};

		    var arrVagao = {
			    width: 60,
			    layout: 'form',
			    border: false,
			    items:
				    [fieldVagao]
		    };

           	var arrOrigem = {
                    width: 43,
                    layout: 'form',
                    border: false,
                    items:
						[fieldOrigem]
                };
		    var arrDestino = {
					width: 43,
					layout: 'form',	
					border: false,
					items:
						[fieldDestino]
				};

	    	var arrSerie =  {
							width: 37,
							layout: 'form',
							border: false,
							items:
								[fieldSerie]
						};

		    var arrDespacho = {
				    width: 60,
				    layout: 'form',
				    border: false,
				    items:
						[fieldDespacho]
				};

            var arrDataIni = {
                width: 87,
                layout: 'form',
                border: false,
                items: [dataInicial]
            };

            var arrDataFim = {
                width: 87,
                layout: 'form',
                border: false,
                items: [dataFinal]
            };

            var arrNroCte = {
                width: 100,
                layout: 'form',
                border: false,
                items: [fieldNumeroCte]
            };

            var colFluxo = {
                width: 50,
                layout: 'form',
                border: false,
                items: [fluxo]
            };

            var colEmail = {
                width: 200,
                layout: 'form',
                border: false,
                items: [email]
            };

            var colEmailErro = {
                width: 250,
                layout: 'form',
                border: false,
                items: [chkEmailErro]
            };


            var arrlinha1 = {
                layout: 'column',
                border: false,
                items: [arrDataIni, arrDataFim, colFluxo, arrChave, arrbtnLoteChaveNfe]
            };

            
            var arrlinha2 = {
                layout: 'column',
                border: false,
                items: [arrCodigoDcl, arrSerie, arrDespacho, arrOrigem, arrDestino, arrVagao, arrNroCte, colEmail, colEmailErro]
            };

            var arrCampos = new Array();
            arrCampos.push(arrlinha1);
            arrCampos.push(arrlinha2);

            var filters = new Ext.form.FormPanel({
							id: 'grid-filtros',
							title: "Filtros",
							region: 'center',
							bodyStyle: 'padding: 15px',
							labelAlign: 'top',
							items: [arrCampos],
							buttonAlign: "center",
							buttons:
							[{
								text: 'Pesquisar',
								type: 'submit',
								iconCls: 'icon-find',
								handler: function (b, e) {
									Pesquisar();
								}
							},
							{
								text: 'Reenviar',
								type: 'submit',
								iconCls: 'icon-arrow_redo',
								handler: function (b, e) {
									var selected = selectModel.getSelections();
									if(selected.length == 0)
									{
										Ext.Msg.alert('Mensagem de Informação','Favor selecionar um Cte!');
										return;
									}
									if (Ext.Msg.confirm("Reenvio de E-mail", "Você deseja realmente reenviar o(s) " + selected.length + " CTe ?", function (btn, text) {
										if (btn == 'yes') {
											var arrCte = new Array();
											for (var i = 0; i < selected.length; i++) {
												arrCte.push(selected[i].data.CteId);
											}
											ReenviarEmailLote(arrCte);
										}
									}));
								}
							},
							    {
								text: 'Exportar',
								type: 'submit',
								iconCls: 'icon-page-excel',
								handler: function (b, e) {
								    if(grid.getStore().getTotalCount()!=0) {
								        var selected = selectModel.getSelections();
								        if (selected.length == 0)
								        {
////										Ext.Msg.alert('Mensagem de Informação','Favor selecionar um Cte!');
////										return;
								            if (Ext.Msg.confirm("Alerta", "Você deseja exportar TODOS os " + grid.getStore().getTotalCount() + " CTes retornados na pesquisa?", function(btn, text) {
								                if (btn == 'yes') {
								                    ConfirmacaoEmail("");
								                }
								            })) ;
								        }
								        else {
								            var ids = "";
								            for (var i = 0; i < selected.length; i++) {
								                //arrCte.push(selected[i].data.CteId);

								                if (i > 0) {
								                    ids += ",";
								                }

								                ids += selected[i].data.CteId;

								                ConfirmacaoEmail(ids);
								            }
								        }
								    }
								    else {
								        Ext.Msg.alert('Mensagem de Informação','Faça uma pesquisa antes para ter dados a serem exportados!');
								    }
								}
							},
							{
								text: 'Alterar',
								type: 'submit',
								iconCls: 'icon-edit',
								handler: function (b, e) {
									ExibirFormAlterarEmail();
								}
							},
							{
								text: 'Limpar',
								handler: function (b, e) {
									idCte = 0;
									gridEmail.getStore().removeAll();
									storeChaves.removeAll();

									grid.getStore().removeAll();
									grid.getStore().totalLength = 0;
									grid.getBottomToolbar().bind(grid.getStore());							 
									grid.getBottomToolbar().updateInfo();  
									Ext.getCmp("grid-filtros").getForm().reset();
									
									HabilitarCampos(false); 
								},
								scope: this
							}]
            });

           var selectModel      = new Ext.grid.CheckboxSelectionModel();
           var selectModelXml   = new Ext.grid.CheckboxSelectionModel();
           var selectModelPdf   = new Ext.grid.CheckboxSelectionModel();

            //**********
            // Funções *
            //**********

            //***********************
            // Reenviar E-mail      *
            //***********************
            function ExibirFormAlterarEmail()
            {
               windowAlterarEmail = new Ext.Window({
				                            id: 'windowAlterarEmail',
				                            title: 'Alteração de e-mail',
				                            modal: true,
				                            width: 500,
				                            height: 170,
				                            autoLoad: {
					                            url: '<%= Url.Action("FormAlterarEmail") %>',
                                                params:{ 'email': Ext.getCmp("filtro-Email").getValue().trim(), 'fluxo': Ext.getCmp("filtro-fluxo").getValue() }, 
					                            scripts: true
				                            }
			                            });
                executarPesquisa = false;       
			    windowAlterarEmail.show();
            }

            function ReenviarEmailLote(arrCte)
            {
                var ids = $.toJSON(arrCte);

				$.ajax({
					url: "<%= Url.Action("ReenviarEmailLote") %>",
					type: "POST",
					dataType: 'json',
					data: ids,
					contentType: "application/json; charset=utf-8",
					success: function(result) {                               
                       
					    if(!result.Erro) {
                          Ext.Msg.alert('Mensagem de Informação','Reenvio efetuado com sucesso!');
                        }else{
                            Ext.Msg.alert('Erro na Reenvio do(s) CTE(s)!', result.Mensagem);
                        }
					},
					failure: function(result) {
                        Ext.Msg.alert('Erro na Reenvio do(s) CTE(s)!', result.Message);
					}
				});

            }
            
            function ConfirmacaoEmail(arrCte)
            {
                if(arrCte != "") {
                    //var ids = $.toJSON(arrCte);
                    var url = "<%= Url.Action("ExportarConfirmacaoEnvioEmailLote") %>";
                    url += "?idCtes=" + arrCte;
                    window.open(url, "");
                }
                else {

                    var params = new Object();
                    
                    params['filter[0].Campo'] = 'dataInicial';
                    params['filter[0].Valor'] = new Array(Ext.getCmp("filtro-data-inicial").getValue().format('d/m/Y') + " " + Ext.getCmp("filtro-data-inicial").getValue().format('H:i:s'));
                    params['filter[0].FormaPesquisa'] = 'Start';
                    
                    params['filter[1].Campo'] = 'dataFinal';
                    params['filter[1].Valor'] = new Array(Ext.getCmp("filtro-data-final").getValue().format('d/m/Y') + " " + Ext.getCmp("filtro-data-inicial").getValue().format('H:i:s'));
                    params['filter[1].FormaPesquisa'] = 'Start';

                    params['filter[2].Campo'] = 'fluxo';
                    params['filter[2].Valor'] = Ext.getCmp("filtro-fluxo").getValue();
                    params['filter[2].FormaPesquisa'] = 'Start';

                    params['filter[3].Campo'] = 'email';
                    params['filter[3].Valor'] = Ext.getCmp("filtro-Email").getValue();
                    params['filter[3].FormaPesquisa'] = 'Start';

                    params['filter[4].Campo'] = 'emailErro';
                    params['filter[4].Valor'] = Ext.getCmp("filtro-emailErro").getValue();
                    params['filter[4].FormaPesquisa'] = 'Start';

                    params['filter[5].Campo'] = 'serie';
			        params['filter[5].Valor'] = Ext.getCmp("filtro-serie").getValue();
			        params['filter[5].FormaPesquisa'] = 'Start';

			        params['filter[6].Campo'] = 'despacho';
			        params['filter[6].Valor'] = Ext.getCmp("filtro-despacho").getValue();
			        params['filter[6].FormaPesquisa'] = 'Start';

			        params['filter[7].Campo'] = 'Origem';
			        params['filter[7].Valor'] = Ext.getCmp("filtro-Ori").getValue();
			        params['filter[7].FormaPesquisa'] = 'Start';
			
			        params['filter[8].Campo'] = 'Destino';
			        params['filter[8].Valor'] = Ext.getCmp("filtro-Dest").getValue();
			        params['filter[8].FormaPesquisa'] = 'Start';
			
			        params['filter[9].Campo'] = 'Vagao';
			        params['filter[9].Valor'] = Ext.getCmp("filtro-numVagao").getValue();
			        params['filter[9].FormaPesquisa'] = 'Start';
            
                    params['filter[10].Campo'] = 'UfDcl';
			        params['filter[10].Valor'] = Ext.getCmp("filtro-UfDcl").getRawValue();
			        params['filter[10].FormaPesquisa'] = 'Start'; 

                    params['filter[11].Campo'] = 'UfDcl';
			        params['filter[11].Valor'] = Ext.getCmp("filtro-UfDcl").getRawValue();
			        params['filter[11].FormaPesquisa'] = 'Start';                
              
                    params['filter[12].Campo'] = 'NroCte';
			        params['filter[12].Valor'] = Ext.getCmp("filtro-NumCte").getRawValue();
                    params['filter[12].FormaPesquisa'] = 'Start';
                    
                    var listaChave = "";
				    
				    if(storeChaves.getCount() != 0)
				    {
					    storeChaves.each(
						    function (record) {
							    if (!record.data.Erro) {
								    listaChave += record.data.Chave + ";";	
							    }
						    }
					    );
				    }else{
					    listaChave = Ext.getCmp("filtro-Chave").getValue();	
				    }
                    
				    params['filter[13].Campo'] = 'chave';
				    params['filter[13].Valor'] = listaChave;
				    params['filter[13].FormaPesquisa'] = 'Start';
        
                    window.alert(Ext.util.JSON.encode(params));
                    var url = "<%= Url.Action("ExportarConfirmacaoEnvioEmailLoteTotal") %>";
                    url += "?filter=" + Ext.util.JSON.encode(params);
                    window.open(url, "");
                    /*Ext.Ajax.request({    
                        url: "<% =Url.Action("ExportarConfirmacaoEnvioEmailLoteTotal") %>",
                        method: "POST",                        
			            params: params,
                        success : function(data) {
                           ////window.open("data:application/ms-excel;charset=utf-8,"+window.url.createObjectURL((response),""));//'width=600,height=600,location=_newtab'
                            
                            var disposition = data.getResponseHeader('Content-Disposition');
                            var filename = disposition.slice(disposition.indexOf("=")+1,disposition.length);
                            var type = data.getResponseHeader('Content-Type');
                            
                            Response.clear();
                            Response.header.add("Content-Disposition", "attachment; filename=\"" + filename + "\"");
                            Response.header.add("Content-Length", disposition);
                            Response.contentType = type;
                            Response.flush();
                            Response.write(disposition);
                            Response.end();
                            
                            
                            var element = document.createElement('a');
                            element.setAttribute('href', 'data:application/ms-excel;charset=utf-8,' + encodeURIComponent(response));
                            element.setAttribute('download', filename);

                            element.style.display = 'none';
                            document.body.appendChild(element);

                            element.click();

                            document.body.removeChild(element);
                            
                        }
                    });*/
                }
            }
            
	//***********************
	// Novo E-mail      *
	//***********************
	function ExibirFormNovoEmail()
	{
		windowNovoEmail = new Ext.Window({
			id: 'windowNovoEmail',
			title: 'Cadastro de e-mail',
			modal: true,
			width: 500,
			height: 170,
			autoLoad: {
				url: '<%= Url.Action("FormAdicionarEmail") %>',
				params:{ 'idCte': idCte }, 
				scripts: true
			}
		});

		// habilita o "enter" para processar o botao salvar inves de efetuar a pesquisa
		executarSalvarCadastro = true;
		
		windowNovoEmail.show();
	}
            
            function ReenviarEmail(idXmlEnvio, xml, pdf, TpArquivoDefault)
            {                
                var tipoEnvio = "";
                if(xml && pdf)
                {
                    tipoEnvio = "PEX";
                }else  if(xml)
                {
                    tipoEnvio = "XML";
                }else  if(pdf)
                {
                    tipoEnvio = "PDF";
                }
                else
                {
                    tipoEnvio = TpArquivoDefault;
                }

                  Ext.Ajax.request({    
                        url: "<% =Url.Action("ReenviarEmail") %>",
                        method: "POST",                        
			            params: {  idXmlEnvio: idXmlEnvio,tipoEnvio: tipoEnvio},
			            success: function(response) {
                             
                             var result = Ext.util.JSON.decode(response.responseText);
                             
                             if(!result.Erro) {
                                  Ext.Msg.alert('Mensagem de Informação','Reenvio efetuado com sucesso!');
                                }else{
                                    Ext.Msg.alert('Mensagem de Informação','Erro na Reenvio do(s) CTE(s)!');
                                }
			            },
                        failure: function(conn, data){
                               Ext.Msg.alert('Erro na Reenvio do(s) CTE(s)!', result.Message);
                        }			            
		            });

                    dsEmail.load({ params: { idcte: idCte} });
/*
				$.ajax({
					url: "<%= Url.Action("ReenviarEmail") %>",
					type: "POST",
					dataType: 'json',
					data: idXmlEnvio,
					contentType: "application/json; charset=utf-8",
					success: function(result) {                               
                        if(result.Erro) {
                          Ext.Msg.alert('Reenvio efetuada com sucesso!');
                        }else{
                            Ext.Msg.alert('Erro na Reenvio do(s) CTE(s)!');
                        }
					},
					failure: function(result) {
                        Ext.Msg.alert('Erro na Reenvio do(s) CTE(s)!', result.Message);
					}
				});
                */
            }

            function Pesquisar() {
                var diferenca = Ext.getCmp("filtro-data-final").getValue() - Ext.getCmp("filtro-data-inicial").getValue();
                diferenca = Math.floor(diferenca / (1000 * 60 * 60 * 24));

                if (Ext.getCmp("filtro-data-inicial").getValue() == '' || Ext.getCmp("filtro-data-final").getValue() == '') {
									executarPesquisa = false;
                    Ext.Msg.show({
                        title: "Mensagem de Erro",
                        msg: "Preencha os filtro datas!",
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.ERROR,
                        minWidth: 200,
												fn: showResult
                    });
                } 
								else if (diferenca > 30) {
									executarPesquisa = false;
                    Ext.Msg.show({
                        title: "Mensagem de Erro",
                        msg: "O período da pesquisa não deve ultrapassar 30 dias",
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.ERROR,
                        minWidth: 200,
												fn: showResult
                    });
                }
		else if (Ext.getCmp("filtro-serie").getValue() != '' && Ext.getCmp("filtro-despacho").getValue() == '') {
			executarPesquisa = false;
			Ext.Msg.show({
				title: "Mensagem de Erro",
				msg: "Não é possível filtrar apenas pela a Série do Despacho.<br/>Também é necessário preencher o número do Despacho.",
				buttons: Ext.Msg.OK,
				icon: Ext.MessageBox.ERROR,
				minWidth: 200,
				fn: showResult
			});
		}
		else if (Ext.getCmp("filtro-despacho").getValue() != '' && Ext.getCmp("filtro-serie").getValue() == '') {
			executarPesquisa = false;
			Ext.Msg.show({
				title: "Mensagem de Erro",
				msg: "Não é possível filtrar apenas pelo número do Despacho.<br/>Também é necessário preencher a Série do Despacho.",
				buttons: Ext.Msg.OK,
				icon: Ext.MessageBox.ERROR,
				minWidth: 200,
				fn: showResult
			});
		}
                else {
								executarPesquisa = true;
                    idCte = 0;
                    grid.getStore().load();
                    gridEmail.getStore().removeAll();
                }
            }

            var grid = new Translogic.PaginatedGrid({
                // stripeRows: false,
                autoLoadGrid: false,
                id: "gridEmail",
                sm: selectModel,
                height: 300,
                width: 450,
                url: '<%= Url.Action("ObterCtes") %>',
                region: 'center',
                viewConfig: {
                    forceFit: false
                },
                fields: [
					'CteId',
                    'Fluxo',
                    'Origem',
                    'Destino',
                    'Mercadoria',
                    'Chave',
                    'Cte',
                    'DateEmissao',
                    'FerroviaOrigem',
                    'FerroviaDestino',
                    'UfOrigem',
                    'UfDestino',
					'CodigoVagao',
                    'Serie',
                    'Despacho',
                    'SerieDesp5',
                    'Desp5'
			    ],
                columns: [
						{ dataIndex: "CteId", hidden: true },
                        selectModel,
						{ header: 'Num Vagão', dataIndex: "CodigoVagao", width: 70, sortable: false },
						{ header: 'Série', dataIndex: "Serie", width: 70, sortable: false },
                        { header: 'Despacho', dataIndex: "Despacho", width: 70, sortable: false },
                        { header: 'CTe', dataIndex: "Cte", width: 100, sortable: false },
                        { header: 'Origem', dataIndex: "Origem", width: 50, sortable: false },
						{ header: 'Destino', dataIndex: "Destino", width: 50, sortable: false },
                        { header: 'Fluxo', dataIndex: "Fluxo", width: 70, sortable: false },
				        { header: 'Mercadoria', dataIndex: "Mercadoria", width: 70, sortable: false },
				        { header: 'Chave CTe', dataIndex: "Chave", width: 280, sortable: false },
                        { header: 'Data Emissão', dataIndex: "DateEmissao", width: 75, sortable: false },
                        { header: 'Ferrovia Ori.', dataIndex: "FerroviaOrigem", sortable: false },
                        { header: 'Ferrovia Dest.', dataIndex: "FerroviaDestino", sortable: false },
                        { header: 'UF Ori.', dataIndex: "UfOrigem", width: 70, sortable: false },
                        { header: 'UF Dest.', dataIndex: "UfDestino", width: 70, sortable: false },
                        { header: 'SerieDesp5', dataIndex: "SerieDesp5", width: 70, sortable: false },
                        { header: 'Desp5', dataIndex: "Desp5", width: 70, sortable: false }
					],
					listeners: {
						rowclick: function(grid, rowIndex, e) {
								if (selectModel.isSelected(rowIndex)) {
									if (rowIndex == lastRowSelected) {
										grid.getSelectionModel().deselectRow(rowIndex);
										dsEmail.removeAll();
										lastRowSelected = -1;
										idCte = 0;
									}
									else {
										lastRowSelected = rowIndex;
										lastDblClickRowSelected = rowIndex;									
									}
								}
						},
						rowdblclick: function(grid, rowIndex, record, e) {
							grid.getSelectionModel().selectRow(lastDblClickRowSelected);
								
							var id = grid.getStore().getAt(rowIndex).get('CteId');
							if (id != 0) {
								idCte = id;
								dsEmail.load({ params: { idcte: id} });
							}
						}
					}
            });
			/*
            grid.on("rowdblclick", function (g, i, e) {
                var id = grid.getStore().getAt(i).get('CteId');
                if (id != 0) {
                    idCte = id;
                    dsEmail.load({ params: { idcte: id} });
                }
            });
			*/
            grid.getStore().proxy.on('beforeload', function (p, params) {
                params['filter[0].Campo'] = 'dataInicial';
                params['filter[0].Valor'] = new Array(Ext.getCmp("filtro-data-inicial").getValue().format('d/m/Y') + " " + Ext.getCmp("filtro-data-inicial").getValue().format('H:i:s'));
                params['filter[0].FormaPesquisa'] = 'Start';

                params['filter[1].Campo'] = 'dataFinal';
                params['filter[1].Valor'] = new Array(Ext.getCmp("filtro-data-final").getValue().format('d/m/Y') + " " + Ext.getCmp("filtro-data-inicial").getValue().format('H:i:s'));
                params['filter[1].FormaPesquisa'] = 'Start';

                params['filter[2].Campo'] = 'fluxo';
                params['filter[2].Valor'] = Ext.getCmp("filtro-fluxo").getValue();
                params['filter[2].FormaPesquisa'] = 'Start';

                params['filter[3].Campo'] = 'email';
                params['filter[3].Valor'] = Ext.getCmp("filtro-Email").getValue();
                params['filter[3].FormaPesquisa'] = 'Start';

                params['filter[4].Campo'] = 'emailErro';
                params['filter[4].Valor'] = Ext.getCmp("filtro-emailErro").getValue();
                params['filter[4].FormaPesquisa'] = 'Start';

                params['filter[5].Campo'] = 'serie';
			    params['filter[5].Valor'] = Ext.getCmp("filtro-serie").getValue();
			    params['filter[5].FormaPesquisa'] = 'Start';

			    params['filter[6].Campo'] = 'despacho';
			    params['filter[6].Valor'] = Ext.getCmp("filtro-despacho").getValue();
			    params['filter[6].FormaPesquisa'] = 'Start';

			    params['filter[7].Campo'] = 'Origem';
			    params['filter[7].Valor'] = Ext.getCmp("filtro-Ori").getValue();
			    params['filter[7].FormaPesquisa'] = 'Start';
			
			    params['filter[8].Campo'] = 'Destino';
			    params['filter[8].Valor'] = Ext.getCmp("filtro-Dest").getValue();
			    params['filter[8].FormaPesquisa'] = 'Start';
			
			    params['filter[9].Campo'] = 'Vagao';
			    params['filter[9].Valor'] = Ext.getCmp("filtro-numVagao").getValue();
			    params['filter[9].FormaPesquisa'] = 'Start';
            
                params['filter[10].Campo'] = 'UfDcl';
			    params['filter[10].Valor'] = Ext.getCmp("filtro-UfDcl").getRawValue();
			    params['filter[10].FormaPesquisa'] = 'Start'; 

                params['filter[11].Campo'] = 'UfDcl';
			    params['filter[11].Valor'] = Ext.getCmp("filtro-UfDcl").getRawValue();
			    params['filter[11].FormaPesquisa'] = 'Start';                
              
                params['filter[12].Campo'] = 'NroCte';
			    params['filter[12].Valor'] = Ext.getCmp("filtro-NumCte").getRawValue();
			    params['filter[12].FormaPesquisa'] = 'Start';

				var listaChave = "";
				
				if(storeChaves.getCount() != 0)
				{
					storeChaves.each(
						function (record) {
							if (!record.data.Erro) {
								listaChave += record.data.Chave + ";";	
							}
						}
					);
				}else{
					listaChave = Ext.getCmp("filtro-Chave").getValue();	
				}

				params['filter[13].Campo'] = 'chave';
				params['filter[13].Valor'] = listaChave;
				params['filter[13].FormaPesquisa'] = 'Start';
                
                
                ////window.alert(Ext.util.JSON.encode(params));
                ////window.alert(params);
            });

         var reeviarEmailAction = new Ext.ux.grid.RowActions({
		    // dataIndex: '',
            
		    header: '...',
		    actions: [{
                iconCls: 'icon-arrow_redo',
                tooltip: 'Reenviar'
            },{
                iconCls: 'icon-delete',
                tooltip: 'Excluir e-mail'
            }],
		    callbacks: {
		            'icon-arrow_redo': function (grid, record, action, rowIndex, col) {
                         var mensagem = "";
                         /*
						 if(record.data.Erro)
                         {
							mensagem = "Cte possui log erro!<br/>";
                         }
						 */
                        if (Ext.Msg.confirm("Reenvio de E-mail", mensagem + "Você deseja realmente reenviar o e-mail para " + record.data.Email + " ?", function (btn, text) {
		                    if (btn == 'yes') {
                               var Id               = grid.getStore().getAt(rowIndex).get('Id');                               
                               var pdf              = grid.getStore().getAt(rowIndex).get('pdf');                               
                               var xml              = grid.getStore().getAt(rowIndex).get('xml');                               
                               var TpArquivoDefault = grid.getStore().getAt(rowIndex).get('TpArquivo');
                               ReenviarEmail(Id, xml, pdf, TpArquivoDefault);
		                    }
		                }));
		            },
					'icon-delete': function (grid, record, action, rowIndex, col) {
						
						executarPesquisa = false;						

						if (Ext.Msg.confirm("Exclusão", "Você deseja realmente excluir o e-mail " + record.data.Email + " ?", function (btn, text) {
							if (btn == 'yes') {								
								var idXmlEnvio = grid.getStore().getAt(rowIndex).get('Id');
							   
								Ext.Ajax.request({
									url: "<% =Url.Action("ExcluirEmail") %>",
									method: "POST",                        
									params: {  idXmlEnvio: idXmlEnvio},
									success: function(response) {
										
										var idCteAux = idCte;
										var result = Ext.util.JSON.decode(response.responseText);
										if(!result.Erro) {
											
											Ext.Msg.alert('Mensagem de Informação', "Excluído e-mail com sucesso!", 
												 function(){ 
													executarPesquisa = true;
												});
											
											//	Pesquisar();
										}
										else {
											Ext.Msg.alert('Mensagem de Informação','Erro na exclusão do e-mail!',
												 function(){ 
													executarPesquisa = true;
												});
											
										}
										idCte = idCteAux;
										dsEmail.load({ params: { idcte: idCte} });
									},
									failure: function(conn, data){
											Ext.Msg.alert('Erro na exclusão do e-mail!', result.Message);
									}
								});

		                    }
		                }));
		            }
		        }
		});

    /* var excluirEmailAction = new Ext.ux.grid.RowActions({
		    // dataIndex: '',
            
		    header: '...',
		    actions: [{
                iconCls: 'icon-delete',
                tooltip: 'Excluir e-mail'
            }],
		    callbacks: {
		            
		        }
		});
		*/
	var gridEmail = new Ext.grid.GridPanel({
		stripeRows: true,
		height: 293,
		width: 450,
		region: 'center',
		store: dsEmail,
		plugins: [reeviarEmailAction],
		loadMask: { msg: App.Resources.Web.Carregando },
		tbar: [{
			id: 'btnNovo',
			text: 'Adicionar e-mail',
			tooltip: 'Adicionar e-mail',
			iconCls: 'icon-new',
			handler: function (c) {
				if(idCte == 0) {
					Ext.Msg.show({
						title: "Mensagem de Informação",
						msg: "Favor selecionar um CTe",
						buttons: Ext.Msg.OK,
						icon: Ext.MessageBox.INFO,
						minWidth: 220
					});
					return; 
				}

				ExibirFormNovoEmail();
				executarPesquisa = false;

			}
		}],
		colModel: new Ext.grid.ColumnModel({
			defaults: {
				sortable: false
				},
			columns: [
				new Ext.grid.RowNumberer(),
				reeviarEmailAction,
				{ header: 'E-mail', dataIndex: "Email", width: 210, sortable: false },
				{ header: 'Tipo Envio', dataIndex: "TpEnvio", width: 60, sortable: false, renderer:envioRenderer  },
				{ header: 'Tipo Arquivo', dataIndex: "TpArquivo", width: 70, sortable: false, renderer:arquivoRenderer  },
				{ header: 'Xml',  dataIndex: "Id", width: 30, sortable: false, renderer:chkXmlRenderer  },
				{ header: 'Pdf',  dataIndex: "Id", width: 30, sortable: false, renderer:chkPdfRenderer  }
			]
		})
	});


    function chkXmlRenderer(val)
    {
        return '<input type="checkbox" name="opt_Xml_'+val+'" value="" onclick="clickCheckBox(this);">';
    }
    
    function chkPdfRenderer(val)
    {
         return '<input type="checkbox" name="opt_Pdf_'+val+'" value="" onclick="clickCheckBox(this);">';
    }

    function arquivoRenderer(val)
      {
        switch(val){
            case "PDF":
                return "<img src='<%=Url.Images("Icons/pdf.png") %>' alt='PDF'>";
                break;
            case "XML":
                return "<img src='<%=Url.Images("Icons/xml.png") %>' alt='XML'>";
                break;
            case "PEX":
                return "<img src='<%=Url.Images("Icons/pdf.png") %>' alt='PDF e XML'>&nbsp;&nbsp;<img src='<%=Url.Images("Icons/xml.png") %>' alt='PDF e XML'>";
                break;
            default:
                return "";
                break;
        }
      }      

	
	function SomenteLetras(campo){
		var digits="abcdefghijklmnopqrstuvwyxz";
		var digito;
		var valor = "";
		var novoValor = "";

		for (var i=0;i<campo.getValue().length;i++){
			
			valor = campo.getValue();
			digito = valor.substring(i,i+1).toLowerCase();

			if (digits.indexOf(digito) != -1){
				novoValor += digito;
			}
		}
		campo.setValue(novoValor);
	}

	function SomenteNumerico(campo)
	{
		var value = campo.getValue();
		var newValue = "";
                    
		for(var i=0; i < value.length; i++)
		{
			if(!isNaN(value.charAt(i)))
			{
				newValue += value.charAt(i);
			}
		}
		campo.setValue(newValue);
	}

      function envioRenderer(val)
      {
        switch(val){
            case "EMA":
                return "Envio por email";
                break;
            case "ECC":
                return " Envio por email como cópia";
                break;
            case "ECO":
                return "Envio por email por cópia oculta";
                break;
            default:
                return "";
                break;
        }
      }                

           var colunaDet1 = {
                title: 'Cte',
                width: 451,
                height: 320,
                layout: 'form',
                border: true,
                items: [grid]
            };

            var colunaDet2 = {
                title: 'E-mail',
                width: 450,
                height: 320,
                layout: 'form',
                border: true,
                items: [gridEmail]
            };

            var columns = {
                layout: 'column',
                border: false,
                autoScroll: true,
                region: 'center',
                items: [colunaDet1, colunaDet2]
            };

	 document.onkeydown = function (e) {
	 
		var keyID = event.keyCode;
		
		if (keyID == 13) 
		{
			if(windowNovoEmail != null && windowNovoEmail.isVisible() && executarPesquisa)
			{
				Salvar(); 
				return;
			}
			else if(executarPesquisa)
			{
				Pesquisar();
				return;
			}
		}	
	
	 }

  $(function () {
            new Ext.Viewport({
                layout: 'border',
                margins: 10,
                items: [
					{
					    region: 'north',
					    height: 230,
					    width: 700,
					    items: [{
					        region: 'center',
					        applyTo: 'header-content'
					    }, filters]
					},
					columns
			]
            });
        });


    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div id="header-content">
        <h1>
            CTe - Reenvio de E-mail</h1>
        <small>Você está em CTe > Reenvio de E-mail</small>
        <br />
    </div>
</asp:Content>
