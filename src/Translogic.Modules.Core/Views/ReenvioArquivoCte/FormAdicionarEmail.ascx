﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%@ Import Namespace="Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico" %>
<div id="div-form-novo-email">
</div>
<script type="text/javascript">

    var idCte = '<%=ViewData["idCte"] %>';

    var dsTipoEnvio = new Ext.data.ArrayStore({
        fields: ['Id', 'Descricao'],
        data: [
                ['EMA', 'Por email'],
				['ECC', 'Como cópia'],
				['ECO', 'Cópia oculta']
			]
    });
    
    var dsTipoArquivo = new Ext.data.ArrayStore({
        fields: ['Id', 'Descricao'],
        data: [
                ['PDF', 'Pdf'],
				['XML', 'Xml'],
				['PEX', 'Pdf e Xml']
			]
    });

    var fieldEmail = {
        xtype: 'textfield',
        id: 'field-Email',
        fieldLabel: 'E-mail',
        name: 'fieldEmail',
        allowBlank: false,
        maxLength: 100,
        width: 195,
        hiddenName: 'fieldEmail',
		listeners:
        {
			specialKey: function (field, e) {
				if (e.getKey() == e.ENTER) 
				{
					 Salvar();
				}
			}
		}
    };

    var cboTipoEnvio = {
        xtype: 'combo',
        store: dsTipoEnvio,
        allowBlank: false,
        // lazyInit: false,
        // lazyRender: false,
        mode: 'local',
        typeAhead: false,
        triggerAction: 'all',
        fieldLabel: 'Tipo  Envio',
        name: 'field-tpEnvio',
        id: 'field-tpEnvio',
        hiddenName: 'filtro-tpEnvio',
        displayField: 'Descricao',
        forceSelection: true,
        width: 100,
        valueField: 'Id',
        emptyText: 'Selecione...',
		listeners:
        {
			specialKey: function (field, e) {
				if (e.getKey() == e.ENTER) 
				{
					 Salvar();
				}
			}
		}
    };


    var cbotpArquivo = {
        xtype: 'combo',
        store: dsTipoArquivo,
        allowBlank: false,
        // lazyInit: false,
        // lazyRender: false,
        mode: 'local',
        typeAhead: false,
        triggerAction: 'all',
        fieldLabel: 'Tipo de Arquivo',
        name: 'filtro-tpArquivo',
        id: 'filtro-tpArquivo',
        hiddenName: 'filtro-tpArquivo',
        displayField: 'Descricao',
        forceSelection: true,
        width: 90,
        valueField: 'Id',
        emptyText: 'Selecione...',
		listeners:
        {
			specialKey: function (field, e) {
				if (e.getKey() == e.ENTER) 
				{
					 Salvar();
				}
			}
		}
    };

    var colEmail = {
        width: 200,
        layout: 'form',
        border: false,
        items: [fieldEmail]
    };

    var colCboTpEnvio = {
        width: 105,
        layout: 'form',
        border: false,
        items: [cboTipoEnvio]
    };

    var colCboTpArquivo = {
        width: 95,
        layout: 'form',
        border: false,
        items: [cbotpArquivo]
    };

    var arrlinha1 = {
        layout: 'column',
        border: false,
        items: [colEmail, colCboTpEnvio, colCboTpArquivo]
    };


    var arrCampos = new Array();
    arrCampos.push(arrlinha1);

    var fieldsEmail = new Ext.form.FormPanel({
        id: 'fields-Email',
        title: "Filtros",
        region: 'center',
        bodyStyle: 'padding: 15px',
        labelAlign: 'top',
        items: [arrCampos],
        buttonAlign: "center",
        buttons:
	            [
                 {
                     text: 'Salvar',
                     type: 'submit',
                     iconCls: 'icon-save',
                     handler: function (b, e) {
                         Salvar(); //AdicionarEmail    Salvar();
                     }
                 },
                 {
                     text: 'Cancelar',
                     handler: function (b, e) {
						windowNovoEmail.close();
                        // Ext.getCmp("fields-Email").getForm().reset();
                     },
                     scope: this
                 }
                ]
    });

		function Salvar() {
			var email = Ext.getCmp("field-Email").getValue().trim();
			var tpEnvio = Ext.getCmp("field-tpEnvio").getValue().trim();
			var tpArquivo = Ext.getCmp("filtro-tpArquivo").getValue().trim();

			if (email == '') {
				Ext.Msg.show({
				title: "Erro",
				msg: "Preencha o e-mail a ser cadastrado!",
				buttons: Ext.Msg.OK,
				icon: Ext.MessageBox.INFO,
				minWidth: 200
				});
			} 
			else if (tpEnvio == '') {
				Ext.Msg.show({
				title: "Erro",
				msg: "Preencha o tipo de envio!",
				buttons: Ext.Msg.OK,
				icon: Ext.MessageBox.INFO,
				minWidth: 200
				});
			} 
			else if (tpArquivo == '') {
				Ext.Msg.show({
				title: "Erro",
				msg: "Preencha o tipo de arquivo!",
				buttons: Ext.Msg.OK,
				icon: Ext.MessageBox.INFO,
				minWidth: 200
				});
			}
			else if (validaEmail(email) == '') {
				Ext.Msg.show({
				title: "Erro",
				msg: "O e-mail informado não é válido!",
				buttons: Ext.Msg.OK,
				icon: Ext.MessageBox.INFO,
				minWidth: 200
				});
			}
			else {
				var mensagem = "Você deseja adicionar o novo e-mail?";

				if (Ext.Msg.confirm("Confirmação", mensagem, function (btn, text) {
					if (btn == 'yes') {
					Ext.Ajax.request({    
					url: "<% =Url.Action("AdicionarEmail") %>",
					success: function(response) {
						var result = Ext.decode(response.responseText);
						if(! result.Erro) {
							
							Ext.Msg.alert('Informação', "Cadastro efetuado com sucesso!", 
								 function(){ 
									executarPesquisa = true;
									
									if (idCte != 0) {
										dsEmail.load({ params: { idcte: idCte} });
									}
								});

							// Ext.Msg.alert('Alerta','Cadastro efetuado com sucesso!');
						}
						else {

							 Ext.Msg.alert('Aviso', "Erro no cadastro do Email <br/>" + result.Mensagem, 
								 function(){ 
									executarPesquisa = true;
								});

							

							// Ext.Msg.alert('Alerta','Erro na Cadastro do Email!');
						}
						windowNovoEmail.close();
						

						// Chama a atualização do grid da página principal
						dsEmail.load({ params: { idcte: idCte} });

					},
					failure: function(conn, data) {
						Ext.Msg.alert('Erro no cadastro do Email!', result.Message);
					},
					method: "POST",
					params: {  idCte: idCte, email: email, tipoEnvio: tpEnvio, tipoArquivo: tpArquivo}
					});
					}
				}));
			}
		}

	function validaEmail(mail) {

        var er = RegExp(/^[A-Za-z0-9_\-\.]+@[A-Za-z0-9_\-\.]{2,}\.[A-Za-z0-9]{2,}(\.[A-Za-z0-9])?/);

        if (mail == "") {
            return false;
        } else if (er.test(mail) == false) {
            return false;
        }
        return true;
    }


   fieldsEmail.render("div-form-novo-email");
	 
	 $(function () { 
		Ext.getCmp("field-Email").focus();
	});

</script>

