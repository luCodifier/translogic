﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%@ Import Namespace="System.Globalization" %>
<%@ Import Namespace="Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico" %>
<div id="div-form-status-Chaves">
</div>
<script type="text/javascript">

		var gridChaves = new Ext.grid.GridPanel({
			viewConfig: {
				forceFit: true
			},
			height: 270,
			width: 800,
			stripeRows: true,
			region: 'center',
			store: storeChaves,
			autoScroll: true,
			loadMask: { msg: App.Resources.Web.Carregando },
			colModel: new Ext.grid.ColumnModel({
				defaults: {
					sortable: false
				},
				columns: [
							new Ext.grid.RowNumberer(),
							{ header: 'Status', dataIndex: "Erro", sortable: false, width: 50,renderer:statusRenderer },
							{ header: 'Chave', dataIndex: "Chave", sortable: false, width: 300 },
							{ header: 'Mensagem', dataIndex: "Mensagem", sortable: false, width: 450}
						 ]
			})
		});

		function statusRenderer(val)
		{
			switch(val){
				case false:
					return "<img src='<%=Url.Images("Icons/tick.png") %>' >";
					break;
				default:
					return "<img src='<%=Url.Images("Icons/cancel.png") %>' >";
					break;
			}
		}  

		
		/* ------------- Ação CTRL+C nos campos da Grid ------------- */
		var isCtrl = false;
		document.onkeyup = function (e) {
		
			var keyID = event.keyCode;

			// 17 = tecla CTRL
			if (keyID == 17) {
				isCtrl = false; //libera tecla CTRL
			}
		}

		document.onkeydown = function (e) {

			var keyID = event.keyCode;

			// verifica se CTRL esta acionado
			if (keyID == 17) {
				isCtrl = true;
			}

			// 67 = tecla 'v'
			if (keyID == 86 && isCtrl == true) {				
				
				var retorno = window.clipboardData.getData('Text');
				
				var rePattern = new RegExp('(?:[0-9]{44})', 'gi');
				var arrChave = new Array();
				
				while (arrMatch = rePattern.exec(retorno)){
					arrChave.push(arrMatch[0]);
				}	

				var dados = $.toJSON(arrChave);
				$.ajax({
					url: "<%= Url.Action("ValidarChaves") %>",
					type: "POST",
					beforeSend: function() { Ext.getCmp("formChavesNfe").getEl().mask("Validando Chave dos CTe.", "x-mask-loading"); },
					dataType: 'json',
					data: dados,
					contentType: "application/json; charset=utf-8",
					success: function(result) {						
                        storeChaves.loadData(result);
						Ext.getCmp("formChavesNfe").getEl().unmask();
					},
					failure: function(result) {
						Ext.Msg.alert('Erro na Inutilização do(s) CTE(s)!', result.Message);
					}
				});

			}
		}


		/* ------------------------------------------------------- */

		var formChavesNfe = new Ext.form.FormPanel
	    ({
	        id: 'formChavesNfe',
	        labelWidth: 80,
	        width: 830,
	        bodyStyle: 'padding: 15px',
	        labelAlign: 'top',
	        items: [gridChaves],
	        buttonAlign: "center",
	        buttons:
		    [{
		        text: "Confirmar",
				id: 'btConfirmar',
		        handler: function () {
					
					if(storeChaves.getCount() != 0)
					{
						storeChaves.each(
							function (record) {
								if (record.data.Erro) {
									storeChaves.remove(record);
								}
							}
						);
								
						if(storeChaves.getCount() == 0)
						{
							Ext.Msg.alert('Informação', 'Favor informar uma chave válida!');
							return;
						}

						Ext.getCmp("grid-filtros").getForm().reset();
						Ext.getCmp("filtro-Chave").setValue(storeChaves.getCount() + ' chaves selecionadas');
						HabilitarCampos(true);

					}else{
						HabilitarCampos(false);
					}

					windowStatusNFe.close();					
		        }
		    },{
		        text: "Cancelar",
		        handler: function () {
					windowStatusNFe.close();
		        }
		    }]
	        });

		 formChavesNfe.render("div-form-status-Chaves");
</script>
