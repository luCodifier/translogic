﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%@ Import Namespace="System.Globalization" %>
<%@ Import Namespace="Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico" %>
<div id="div-form-alterar-email">
</div>
<script type="text/javascript">
   
    var EmailFiltro = '<%=ViewData["Email"] %>';
    var FluxoFiltro = '<%=ViewData["Fluxo"] %>'; 

    var fieldFluxo = {
        xtype: 'numberfield',
		vtype: 'ctefluxovtype', 
        style: 'text-transform: uppercase',
        id: 'field-fluxo',
        fieldLabel: 'Fluxo',
        name: 'fieldfluxo',
        allowBlank: true,
        autoCreate: { tag: 'input', type: 'text', maxlength: '5', autocomplete: 'off' },
        maxLength: 20,
        width: 45,
        value: FluxoFiltro,
        hiddenName: 'fieldfluxo',
		listeners:
        {
			specialKey: function (field, e) {
				if (e.getKey() == e.ENTER) 
				{
					  Salvar();
				}
			}
		}
    };

    var fieldEmail = {
        xtype: 'textfield',
        id: 'field-Email',
        fieldLabel: 'E-mail',
        name: 'fieldEmail',
        allowBlank: true,
        maxLength: 100,
        width: 195,
        value: EmailFiltro,
        hiddenName: 'fieldEmail',
		listeners:
        {
			specialKey: function (field, e) {
				if (e.getKey() == e.ENTER) 
				{
					  Salvar();
				}
			}
		}
    };

    var fieldNovoEmail = {
        xtype: 'textfield',
        id: 'field-NovoEmail',
        fieldLabel: 'E-mail Correto',
        name: 'fieldNovoEmail',
        allowBlank: true,
        maxLength: 100,
        width: 195,
        hiddenName: 'fieldNovoEmail',
		listeners:
        {
			specialKey: function (field, e) {
				if (e.getKey() == e.ENTER) 
				{
					  Salvar();
				}
			}
		}
    };

    var colFluxo = {
        width: 50,
        layout: 'form',
        border: false,
        items: [fieldFluxo]
    };

    var colEmail = {
        width: 200,
        layout: 'form',
        border: false,
        items: [fieldEmail]
    };

    var colNovoEmail = {
        width: 200,
        layout: 'form',
        border: false,
        items: [fieldNovoEmail]
    };

    var arrlinha1 = {
        layout: 'column',
        border: false,
        items: [colFluxo, colEmail, colNovoEmail]
    };

    var arrCampos = new Array();
    arrCampos.push(arrlinha1);

    var fieldsEmail = new Ext.form.FormPanel({
        id: 'fields-Email',
        title: "Filtros",
        region: 'center',
        bodyStyle: 'padding: 15px',
        labelAlign: 'top',
        items: [arrCampos],
        buttonAlign: "center",
        buttons:
	            [
                 {
                     text: 'Salvar',
                     type: 'submit',
                     iconCls: 'icon-save',
                     handler: function (b, e) {
                         Salvar();
                     }
                 },
                 {
                     text: 'Cancelar',
                     handler: function (b, e) {
						 windowAlterarEmail.close();
                     },
                     scope: this
                 }
                ]
    });

             function Salvar() {
                 var emailAntigo = Ext.getCmp("field-Email").getValue().trim();
                 var emailNovo = Ext.getCmp("field-NovoEmail").getValue().trim();
                 var fluxoCte = Ext.getCmp("field-fluxo").getValue();

                 if (fluxoCte == '') {
                     Ext.Msg.show({
                         title: "Erro",
                         msg: "Preencha o fluxo do e-mail a ser alterado!",
                         buttons: Ext.Msg.OK,
                         icon: Ext.MessageBox.INFO,
                         minWidth: 200,
						 fn: function (){ Ext.getCmp("field-fluxo").focus(); }
                     });
                 } else if (emailAntigo == '') {
                     Ext.Msg.show({
                         title: "Erro",
                         msg: "Preencha o email a ser alterado!",
                         buttons: Ext.Msg.OK,
                         icon: Ext.MessageBox.INFO,
                         minWidth: 200,
						 fn: function (){ Ext.getCmp("field-Email").focus(); }
                     });
                 } else if (emailNovo == '') {
                     Ext.Msg.show({
                         title: "Erro",
                         msg: "Preencha o email correto a ser enviado os arquivos!",
                         buttons: Ext.Msg.OK,
                         icon: Ext.MessageBox.INFO,
                         minWidth: 200,
						 fn: function (){ Ext.getCmp("field-NovoEmail").focus(); }
                     });
                 } else if (validaEmail(emailNovo) == '') {
                     Ext.Msg.show({
                         title: "Erro",
                         msg: "O novo e-mail informado não é válido!",
                         buttons: Ext.Msg.OK,
                         icon: Ext.MessageBox.INFO,
                         minWidth: 200,
						 fn: function (){ Ext.getCmp("field-NovoEmail").focus(); }
                     });
                 } else {
                     var mensagem = "Você deseja alterar todos os e-mail de " + emailAntigo + " para " + emailNovo + " do fluxo " + fluxoCte + " ?";

                     if (Ext.Msg.confirm("Confirmação", mensagem, function (btn, text) {
                         if (btn == 'yes') {
                             Ext.Ajax.request({    
                               url: "<% =Url.Action("Salvar") %>",
			                    success: function(response) {
                                    var result = Ext.decode(response.responseText);
                                   
                                    if(!result.Erro) {
                                            Ext.Msg.alert('Alerta','Alteração efetuada com sucesso!');
                                    }else{
                                            Ext.Msg.alert('Alerta','Erro na Alteração do(s) Email(s)!');
                                    }

                                    windowAlterarEmail.close();
			                    },
                                failure: function(conn, data){
                                     Ext.Msg.alert('Erro na Alteração do(s) Email(s)!', result.Message);
                                },
			                    method: "POST",
			                    params: {  email: emailAntigo, novoEmail: emailNovo, fluxo: fluxoCte}
		                    });
                         }
                     }));
                 }
             }


    function validaEmail(mail) {

        var er = RegExp(/^[A-Za-z0-9_\-\.]+@[A-Za-z0-9_\-\.]{2,}\.[A-Za-z0-9]{2,}(\.[A-Za-z0-9])?/);

        if (mail == "") {
            return false;
        } else if (er.test(mail) == false) {
            return false;
        }
        return true;
    }

    fieldsEmail.render("div-form-alterar-email");
	
	$(function () { 
		Ext.getCmp("field-fluxo").focus();
	});

</script>
