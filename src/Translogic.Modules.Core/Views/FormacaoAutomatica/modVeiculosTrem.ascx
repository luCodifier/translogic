﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div id="div-modal-VeiculosTrem"></div>
<%
    var OS = ViewData["OS"];
    var Trem = ViewData["Trem"];
    var LocalParada = ViewData["LocalParada"];
    var dataProximaExecucao = ViewData["DataProximaExecucao"];
    var horaProximaExecucao = ViewData["HoraProximaExecucao"];
    var Acao = ViewData["Acao"];
    var IdMensagem = ViewData["IdMensagem"];
%>
<script type="text/javascript">
    var gridVeiculos;
    var OS = '<%=OS %>';
    var IdMensagem = '<%=IdMensagem %>';
    var Trem = '<%=Trem %>';
    var LocalParada = '<%=LocalParada %>';
    var dataProximaExecucao = '<%=dataProximaExecucao %>';
    var horaProximaExecucao = '<%=horaProximaExecucao %>';
    var Acao = '<%=Acao %>';

    //JAVASCRIPT

    //STORES
    var gridVeiculosTremStore = new Ext.data.JsonStore({
        root: "Items",
        totalProperty: 'Total',
        id: 'gridVeiculosTremStore',
        name: 'gridVeiculosTremStore',
        autoLoad: true,
        proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("ObterConsultaVeiculosTrem","FormacaoAutomatica") %>', timeout: 600000 }),
        paramNames: {
            sort: "detalhesPaginacaoWeb.Sort",
            dir: "detalhesPaginacaoWeb.Dir",
            start: "detalhesPaginacaoWeb.Start",
            limit: "detalhesPaginacaoWeb.Limit"
        },
        fields: ['VagaoLocomotiva', 'StatusProcessamento', 'Cad', 'Pedido', 'Localizacao', 'Situacao', 'Lotacao', 'Recomendacao', 'LocalParada', 'DescErro']
    });

    var txtTrem = {
        xtype: 'textfield',
        name: 'txtTrem',
        id: 'txtTrem',
        fieldLabel: 'Trem',
        maskRe: /[a-zA-Z0-9]/,
        autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '3' },
        allowBlank: false,
        width: 35,
        style: 'text-transform:uppercase;',
        disabled: true,
        value: Trem
    };

    var txtLocalParada = {
        xtype: 'textfield',
        name: 'txtLocalParada',
        id: 'txtLocalParada',
        fieldLabel: 'Local/Parada',
        maskRe: /[a-zA-Z0-9]/,
        autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '3' },
        allowBlank: false,
        width: 35,
        style: 'text-transform:uppercase;',
        disabled: true,
        value: LocalParada
    };

    var txtteste = {
        xtype: 'textfield',
        name: 'txtteste',
        id: 'txtteste',
        fieldLabel: 'Ltxtteste',
        maskRe: /[a-zA-Z0-9]/,
        autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '3' },
        allowBlank: false,
        width: 50,
        style: 'text-transform:uppercase;'
    };

    var lblDataHora = {
        xtype: 'label',
        id: 'lblDataHora',
        text: 'Data e Hora: '
    };

    var dpData = new Ext.form.DateField({
        fieldLabel: 'Data',
        id: 'dpData',
        name: 'dpData',
        dateFormat: 'd/n/Y',
        width: 83,
        plugins: [new Ext.ux.InputTextMask('99/99/9999', true)],
        disabled: true,
        value: dataProximaExecucao
    });

    var tpTime = new Ext.form.TimeField({
        name: 'tpTime',
        id: 'tpTime',
        format: 'H:i',
        increment: 1,
        fieldLabel: 'Hora',
        maxlength: 5,
        plugins: [new Ext.ux.InputTextMask('99:99', true)],
        width: 60,
        editable: true,
        disabled: true,
        value: horaProximaExecucao
    });

    var frmtxtTrem = {
        //bodyStyle: 'padding-top:5px',
        layout: 'form',
        border: false,
        labelAlign: 'right',
		items: [txtTrem]
	};

    var frmtxtLocalParada = {
        layout: 'form',
        labelAlign: 'right',
		border: false,
		items: [txtLocalParada]
    };

    var frmdpData = {
        layout: 'form',
        labelAlign: 'right',
        border: false,
        items: [dpData]
    };
    var frmtpTime = {
        layout: 'form',
        labelAlign: 'right',
        border: false,
        items: [tpTime]
    };

    var linhaCampos = {
        layout: 'column',
        border: false,
        items: [frmtxtTrem, frmtxtLocalParada, frmdpData, frmtpTime]
    };


    var fsCampos = {
        xtype: 'fieldset',
        id: 'fsCampos',
        name: 'fsCampos',
        height: 50,
        autowidth: true,
        items: [linhaCampos]
    };

    //GRID Veiculos
    var cm = new Ext.grid.ColumnModel({
        defaults: {
            sortable: true
        },
        columns: [
                    new Ext.grid.RowNumberer(),
                    { header: 'Vagão/Locomotiva', dataIndex: "VagaoLocomotiva", sortable: false, width: 30 },
                    { header: 'Status', dataIndex: "StatusProcessamento", sortable: false, width:13,  
                        renderer: function(value) 
                        {                           
                            if (value == 'S')
                                return "<img src='/Get.efh?file={Translogic.Core.Infrastructure.Web;}/Content/Images/Icons/go.png' qtip='Sim' alt='Sim' />";
                            else 
                                return "<img src='/Get.efh?file={Translogic.Core.Infrastructure.Web;}/Content/Images/Icons/stop.png' qtip='Não' alt='Não' />";
                        }
                    },
                    { header: 'Cad', dataIndex: "Cad", sortable: false, width: 10 },
                    { header: 'Pedido', dataIndex: "Pedido", sortable: false, width: 14 },
                    { header: 'Localização', dataIndex: "Localizacao", sortable: false, width: 20 },               
                    { header: 'Situação', dataIndex: "Situacao", sortable: false, width: 20 },
                    { header: 'Lotação', dataIndex: "Lotacao", sortable: false, width: 20 },
                    { header: 'Recomendação', dataIndex: "Recomendacao", sortable: false, width: 25 },
                    { header: 'Descrição/Erro', dataIndex: "DescErro", sortable: false, width: 80 }
                ]
    });

    gridVeiculos = new Ext.grid.EditorGridPanel({
        id: 'gridVeiculos',
        name: 'gridVeiculos',
        autoLoadGrid: true,
        height: 300,
        limit: 10,
        width: 960,
        stripeRows: true,
        cm: cm,
        region: 'center',
        viewConfig: {
            forceFit: true,
            emptyText: 'Não possui dado(s) para exibição.'
        },
        autoScroll: true,
        loadMask: { msg: App.Resources.Web.Carregando },
        store: gridVeiculosTremStore
    });

    gridVeiculos.getStore().proxy.on('beforeload', function (p, params) {
        //Parametros de pesquisa(filtros) grid
        //params['os'] = OS;
        params['IdMensagem'] = IdMensagem;
    });


    var frmgridVeiculos = {
        layout: 'form',
        border: false,
        autoScroll: true,
        items: [{ layout: 'column', title: 'Veículos', border: false, items: [gridVeiculos]}]
    };

    var formVeiculosTrem = new Ext.form.FormPanel
    ({
        id: 'formVeiculosTrem',
        //labelWidth: 80,
        //labelAlign: 'top',
        height: 450,
        bodyStyle: 'padding: 5px',
        items:
        [
            fsCampos,
            frmgridVeiculos
        ],
        buttonAlign: "center",
        buttons:
        [
            {
                id: 'btnSair',
                text: "Sair",
                handler: function () {
                    modVeiculosTrem.close();
                }
            }
        ]
    });

    /******************************* RENDER *******************************/
    Ext.onReady(function () {
        formVeiculosTrem.render("div-modal-VeiculosTrem");
        //Ext.getCmp('txtValorMercadoria').setReadOnly(true);
    });

</script>