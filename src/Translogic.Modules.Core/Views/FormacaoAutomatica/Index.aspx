﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Interna.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content runat="server" ID="HeadContent" ContentPlaceHolderID="HeadContent">
<script src="/GetFile.efh?file={Translogic.Core.Infrastructure.Web;}/Content/Javascript/Utilfunctions.js"> </script>

    <script type="text/javascript">
        var OS;
        var IdMensagem;
        var modVeiculosTrem;

        //#Region Storegrid

        var gridStore = new Ext.data.JsonStore({
            root: "Items",
            totalProperty: 'Total',
            id: 'gridStore',
            name: 'gridStore',
            proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("ObterConsultaFormacaoAutomatica","FormacaoAutomatica") %>', timeout: 600000 }),
            paramNames: {
                sort: "detalhesPaginacaoWeb.Sort",
                dir: "detalhesPaginacaoWeb.Dir",
                start: "detalhesPaginacaoWeb.Start",
                limit: "detalhesPaginacaoWeb.Limit"
            },
            fields: ['LfaId', 'LIdMensagem', 'Operacao', 'OS', 'Trem', 'LfaOrigem', 'LfaDestino', 'StatusProcessamento', 'StatusLiberacao', 'LfaTentativas', 'DtProximaExecucao', 'Obs']
        });

        var statusProcessamentoStore = new Ext.data.ArrayStore({
            id: 'statusProcessamentoStore',
            fields: [
            'Id',
            'Descricao'
            ],
            data: [
                ['S', 'Processados'],
                ['N', 'Não Processados']
            ]
        });

        //#endRegion

        //Functions
        function showModalVeiculosTrem(record) {
            modVeiculosTrem = new Ext.Window({
                name: 'formModalVeiculosTrem',
                id: 'formModalVeiculosTrem',
                title: 'Trem',
                modal: true,
                width: 970,
                height: 485,
                closable: true,
                autoLoad:
				    {
                    url: '<%= Url.Action("modVeiculosTrem","FormacaoAutomatica") %>',
                    params: { os: record.data.OS, trem: record.data.Trem, localParada: record.data.LfaDestino, dtProximaExecucao: record.data.DtProximaExecucao, IdMensagem: record.data.LIdMensagem },
				        text: "Carregando Veiculos do Trem...",
				        scripts: true,
				        callback: function (el, sucess) {
				            if (!sucess) {
				                el.close();
				            }
				        }
				    },
                listeners: {
                    resize: function (win, width, height, eOpts) {
                        win.center();
                    }
                }
            });
            modVeiculosTrem.show();
        };

        function ZerarTentativa(idMensagem) {
            loader("Processando...");
                $.ajax
                ({
                    url: '<%= Url.Action("ZerarTentativa","FormacaoAutomatica") %>',
                    type: "POST",
                    dataType: 'json',
                    data: $.toJSON(
                        {
                            IdMensagem: idMensagem
                        }),
                    contentType: "application/json; charset=utf-8",
                    async: true,
                    success: function (result) {

                        if (result.Result) {

                            closeLoader();

                            Ext.Msg.show(
                                {
                                    title: "",
                                    msg: "Tentativas zeradas com sucesso!",
                                    buttons: Ext.Msg.OK,
                                    icon: Ext.MessageBox.INFO,
                                    fn: function (buttonValue) {
                                        if (buttonValue == "ok") {
                                            //Atualizar grid da tela de pesquisa
                                            $("#btnPesquisar").trigger("click");
                                        }
                                    }
                                });
                        } else {

                            closeLoader();

                            Ext.Msg.show(
                                {
                                    title: "Erro",
                                    msg: result.Message,
                                    buttons: Ext.Msg.OK,
                                    icon: Ext.MessageBox.INFO
                                });
                        }

                    },
                    error: function (result) {

                        closeLoader();

                        var mensagem = "Ocorreu erro na trava do Trem";

                        if (result != null) {
                            if (result.Message != null) {
                                mensagem = result.Message;
                            }
                        }

                        Ext.Msg.show(
                            {
                                title: "Erro",
                                msg: mensagem,
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.ERROR
                            });
                    }
                });
        };

        //OBJETO TELA
        function criarObjFormacaoAutomatica() {
            var dateParts;
            var formacaoAutoFiltroDto = new Object();

            //Converte data inicial para formato MM/DD/YYYY para controller conseguir identificar
            dateParts = (Ext.getCmp("txtDataInicial").getRawValue()).split("/");
            if (dateParts.length > 1) {
                formacaoAutoFiltroDto.dtInicial = dateParts[1] + "/" + dateParts[0] + "/" + dateParts[2];
            } else {
                formacaoAutoFiltroDto.dtInicial = null;
            }

            //Converte data final para formato MM/DD/YYYY para controller conseguir identificar
            dateParts = (Ext.getCmp("txtDataFinal").getRawValue()).split("/");
            if (dateParts.length > 1) {
                formacaoAutoFiltroDto.dtFinal = dateParts[1] + "/" + dateParts[0] + "/" + dateParts[2];
            } else {
                formacaoAutoFiltroDto.dtFinal = null;
            }

            formacaoAutoFiltroDto.statusProcessamento = Ext.getCmp("ddlStatusProcessamento").getValue();

            formacaoAutoFiltroDto.os = Ext.getCmp("txtNumOs").getValue();
            formacaoAutoFiltroDto.tremPrefixo = Ext.getCmp("txtTremPrefixo").getValue();
            formacaoAutoFiltroDto.origem = Ext.getCmp("txtOrigem").getValue();
            formacaoAutoFiltroDto.destino = Ext.getCmp("txtDestino").getValue();

//            formacaoAutoFiltroDto.local = Ext.getCmp("txtLocal").getValue();

            return formacaoAutoFiltroDto;
        };

        function pesquisar() {
            if (ValidarFiltros()) {
                gridStore.load({ params: { start: 0, limit: 50} });
            }
        };

        function limpar() {
            ColocarHojeEmDatas();
            Ext.getCmp('ddlStatusProcessamento').reset();
            Ext.getCmp('txtNumOs').setValue("");
            Ext.getCmp('txtTremPrefixo').setValue("");
            Ext.getCmp('txtOrigem').setValue("");
            Ext.getCmp('txtDestino').setValue("");
            //Ext.getCmp('txtLocal').setValue("");
            gridStore.removeAll();        
        };

        function exportarExcel(url) {
            if (ValidarFiltros()) {
                loader("Processando dados...");

                url += "?formacaoAutoFiltroDto=" + $.toJSON(criarObjFormacaoAutomatica());

                window.open(url, "");

                closeLoader();
            }
        };

        function ValidarFiltros() {
            var dtI = Ext.getCmp("txtDataInicial").getRawValue();
            var dtF = Ext.getCmp("txtDataFinal").getRawValue();
            var numOs = Ext.getCmp("txtNumOs").getRawValue();

            if (dtI == "" &&
	            dtF == "" &&
	            numOs == "") {
                Ext.Msg.show({
                    title: 'Aviso',
                    msg: 'Selecione ou preencha no mínimo um filtro de pesquisa.!',
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.ERROR
                });
                return false;
            }

            //Valida data inicial é nula
            if (dtI == "") {
                Ext.Msg.show({
                    title: 'Aviso',
                    msg: 'A Data Inicial é obrigatória.',
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.ERROR
                });
                return false;
            }
//            //Valida data final é nula
//            if (dtF == "") {
//                Ext.Msg.show({
//                    title: 'Aviso',
//                    msg: 'A Data Final é obrigatória.',
//                    buttons: Ext.Msg.OK,
//                    icon: Ext.MessageBox.ERROR
//                });
//                return false;
            //            }
            //Valida datas final maior que inicial
            if (dtI != "" && dtF != "") {
                if (!comparaDatas(dtI, dtF)) {
                    Ext.Msg.show({
                        title: 'Aviso',
                        msg: ' Data final deve ser superior a data inicial nos filtros.',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.ERROR
                    });
                    return false;
                }
            }
            return true;
        };

        function comparaDatas(dataIni, dataFim) {
            var arrDtIni = dataIni.split('/');
            var arrDtFim = dataFim.split('/');

            var dtIni = new Date(arrDtIni[2] + '/' + arrDtIni[1] + '/' + arrDtIni[0]);
            var dtFim = new Date(arrDtFim[2] + '/' + arrDtFim[1] + '/' + arrDtFim[0]);

            return dtIni <= dtFim;
        };

        var ColocarHojeEmDatas = function () {
            var today = new Date();
            var dd = today.getDate();
            var dAnterior = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();

            if (dd < 10) {
                dd = '0' + dd
            }

            if (dAnterior < 10) {
                dAnterior = '0' + dAnterior;
            }

            if (mm < 10) {
                mm = '0' + mm;
            }

            today = dd + '/' + mm + '/' + yyyy;
            var yesterday = dAnterior + '/' + mm + '/' + yyyy;
            Ext.getCmp("txtDataInicial").setValue(yesterday);
            Ext.getCmp("txtDataFinal").setValue(today);
        }

        //#Region PainelChecklist GRID

        $(function () 
        {

            var btnDetail = new Ext.ux.grid.RowActions({
                dataIndex: '',
                header: 'Ações',
                align: 'center',
                autoWidth: false,
                width: 12,
                actions: [
                            { iconCls: 'icon-detail', tooltip: 'Abrir Registro' }
                         ],
                callbacks: {

                    'icon-detail': function (grid, record, action, row, col) 
                    {
                        showModalVeiculosTrem(record);
                    }
                }
            });

            // BOTÃO Zerar Tentativas
            var btnZerarTentativa = new Ext.ux.grid.RowActions({
                dataIndex: '',
                autoWidth: false,
                width: 1,
                header: '',
                align: 'center',
                actions: [
                            { iconCls: 'icon-arrow_refresh', tooltip: 'Zerar tentativas' }
                         ],
                callbacks: {
                    'icon-arrow_refresh': function (grid, record, action, row, col) {
                        if (Ext.Msg.confirm("Tentativas", "Deseja realmente zerar o contador?", function (btn, text) {
                            if (btn == 'yes') {
                                ZerarTentativa(record.data.LIdMensagem);
                            }
                        }));
                        return false;
                    }
                }
            });

            // Toolbar da Grid
            var pagingToolbar = new Ext.PagingToolbar({
                pageSize: 50,
                store: gridStore,
                displayInfo: true,
                displayMsg: App.Resources.Web.GridExibindoRegistros,
                emptyMsg: App.Resources.Web.GridSemRegistros,
                paramNames: {
                    start: "detalhesPaginacaoWeb.Start",
                    limit: "detalhesPaginacaoWeb.Limit"
                }
            });

            var cm = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true
                },
                columns: [
                    new Ext.grid.RowNumberer(),
                    { header: 'LfaId', dataIndex: "LfaId", sortable: false, hidden: true, width: 20 },
                    { header: 'IdMensagem', dataIndex: "LIdMensagem", sortable: false, hidden: true, width: 20 },
                    { header: 'Operação', dataIndex: "Operacao", sortable: false, width: 20 },
                    btnDetail,
                    { header: 'OS', dataIndex: "OS", sortable: false, width: 15 },
                    { header: 'Trem', dataIndex: "Trem", sortable: false, width: 10 },
                    { header: 'Origem', dataIndex: "LfaOrigem", sortable: false, width: 12 },
                    { header: 'Destino', dataIndex: "LfaDestino", sortable: false, width: 12 },
                    { header: 'Status Processamento', dataIndex: "StatusProcessamento", sortable: false, width: 32,
                        renderer: function (value) {

                            if (value == 'Verde')
                                return "<img src='/Get.efh?file={Translogic.Core.Infrastructure.Web;}/Content/Images/Icons/go.png' qtip='Sim' alt='Sim' />";
                            else
                                return "<img src='/Get.efh?file={Translogic.Core.Infrastructure.Web;}/Content/Images/Icons/stop.png' qtip='Não' alt='Não' />";
                        }
                    },
                    { header: 'Status Liberação', dataIndex: "StatusLiberacao", sortable: false, width: 22,
                        renderer: function (value) {

                            if (value == 'Verde')
                                return "<img src='/Get.efh?file={Translogic.Core.Infrastructure.Web;}/Content/Images/Icons/go.png' qtip='Fechada' alt='Fechada' />";
                            else
                                return "<img src='/Get.efh?file={Translogic.Core.Infrastructure.Web;}/Content/Images/Icons/stop.png' qtip='Aberta' alt='Aberta' />";
                        }
                    },
                    { header: 'Tentativas', dataIndex: "LfaTentativas", sortable: false, width: 17 },
                    btnZerarTentativa,
                    { header: 'Data próxima execução', dataIndex: "DtProximaExecucao", sortable: false, width: 33 },
                    { header: 'Obs', dataIndex: "Obs", sortable: false, width: 57 }
                ]
            });
            var grid = new Ext.grid.EditorGridPanel({
                id: 'grid',
                name: 'grid',
                autoLoadGrid: true,
                height: 360,
                //limit: 10,
                width: 870,
                stripeRows: true,
                cm: cm,
                region: 'center',
                viewConfig: {
                    forceFit: true,
                    emptyText: 'A pesquisa não retornou nenhum resultado.'
                },
                autoScroll: true,
                loadMask: { msg: App.Resources.Web.Carregando },
                store: gridStore,
                bbar: pagingToolbar,
                plugins: [btnDetail, btnZerarTentativa]
            });

            grid.getStore().proxy.on('beforeload', function (p, params) {
                //Parametros de pesquisa(filtros) grid
                params['formacaoAutoFiltroDto'] = $.toJSON(criarObjFormacaoAutomatica());
            });

            //#endRegion

            //#Region PesquisaFiltros
            var txtDataInicial = new Ext.form.DateField({
                fieldLabel: 'Data Inicial',
                id: 'txtDataInicial',
                name: 'txtDataInicial',
                dateFormat: 'd/n/Y',
                width: 83,
                plugins: [new Ext.ux.InputTextMask('99/99/9999', true)],
                value: new Date()
            });

            var txtDataFinal = new Ext.form.DateField({
                fieldLabel: 'Data Final',
                id: 'txtDataFinal',
                name: 'txtDataFinal',
                dateFormat: 'd/n/Y',
                width: 83,
                plugins: [new Ext.ux.InputTextMask('99/99/9999', true)],
                value: new Date()
            });

            var txtNumOs = {
                xtype: 'numberfield',
                name: 'txtNumOs',
                id: 'txtNumOs',
                fieldLabel: 'OS',
                allowDecimals: false,
                decimalSeparator: ',',
                autoCreate: {
                    tag: 'input',
                    type: 'text',
                    autocomplete: 'off',
                    maxlength: '7'
                },
                width: 85,
                enableKeyEvents: true,
                listeners: {
                    keyup: function (field, e) {
                        field.setValue(field.getRawValue().replace("'", ""));
                    }
                }
            };

            var ddlStatusProcessamento = new Ext.form.ComboBox({
                id: 'ddlStatusProcessamento',
                typeAhead: false,
                editable: false,
                triggerAction: 'all',
                lazyRender: true,
                mode: 'local',
                valueField: 'Descricao',
                displayField: 'Descricao',
                fieldLabel: 'Status processamento',
                width: 150,
                store: statusProcessamentoStore,
                value: 'Não Processados'
            });

            var txtTremPrefixo = {
                xtype: 'textfield',
                name: 'txtTremPrefixo',
                id: 'txtTremPrefixo',
                fieldLabel: 'Trem/Prefixo',
                maskRe: /[a-zA-Z0-9]/,
                autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '3' },
                allowBlank: true,
                width: 50,
                style: 'text-transform:uppercase;'
            };

            var txtOrigem = {
                id: 'txtOrigem',
                xtype: 'textfield',
                vtype: 'cteestacaovtype',
                style: 'text-transform: uppercase',
                fieldLabel: 'Origem',
                name: 'Origem',
                allowBlank: true,
                maxLength: 3,
                width: 50,
                hiddenName: 'Origem',
                autoCreate: { tag: 'input', type: 'text', maxlength: '3', autocomplete: 'off' }
            };

            var txtDestino = {
                id: 'txtDestino',
                xtype: 'textfield',
                vtype: 'cteestacaovtype',
                style: 'text-transform: uppercase',
                fieldLabel: 'Destino',
                name: 'Destino',
                allowBlank: true,
                maxLength: 3,
                width: 50,
                hiddenName: 'Destino',
                autoCreate: { tag: 'input', type: 'text', maxlength: '3', autocomplete: 'off' }
            };

//            var txtLocal = {
//                xtype: 'textfield',
//                name: 'txtLocal',
//                id: 'txtLocal',
//                fieldLabel: 'Local',
//                maskRe: /[a-zA-Z]/,
//                autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '3' },
//                allowBlank: true,
//                width: 50,
//                style: 'text-transform:uppercase;'
//            };

            //Item do Array de Componentes (1 por cada componente criado)
            var formDataInicial = {
                width: 100,
                layout: 'form',
                border: false,
                items: [txtDataInicial]
            };

            var formDataFinal = {
                width: 100,
                layout: 'form',
                border: false,
                items: [txtDataFinal]
            };

            var formddlStatusProcessamento = {
                layout: 'form',
                width: 165,
                border: false,
                items: [ddlStatusProcessamento]
            };

            var formtxtNumOs = {
                width: 100,
                layout: 'form',
                border: false,
                items: [txtNumOs]
            };

            var formtxtTremPrefixo = {
                width: 80,
                layout: 'form',
                border: false,
                items: [txtTremPrefixo]
            };

            var formtxtOrigem = {
                width: 65,
                layout: 'form',
                border: false,
                items: [txtOrigem]
            };

            var formtxtDestino = {
                width: 65,
                layout: 'form',
                border: false,
                items: [txtDestino]
            };

//            var formtxtLocal = {
//                width: 65,
//                layout: 'form',
//                border: false,
//                items: [txtLocal]
//            };

            //Array de componentes do form 
            var linha1Campos = {
                layout: 'column',
                border: false,
                items: [formDataInicial, formDataFinal, formddlStatusProcessamento, formtxtNumOs, formtxtTremPrefixo, formtxtOrigem, formtxtDestino]
            };

            //Conteudo do Form que sera criado
            var filtros = new Ext.form.FormPanel({
                id: 'grid-filtros',
                title: "Filtros",
                region: 'center',
                bodyStyle: 'padding:15px;',
                layout: 'form',
                labelAlign: 'top',
                items: [linha1Campos],
                buttonAlign: "center",
                buttons:
                [{
                    text: 'Pesquisar',
                    id: 'btnPesquisar',
                    iconCls: 'icon-find',
                    handler: function (b, e) {
                        pesquisar();
                    }
                },
                {
                    text: 'Limpar',
                    id: 'btnLimpar',
                    handler: function (b, e) {
                        limpar();
                    }
                },
                {
                    id: 'btnExportarExcel',
                    text: 'Exportar',
                    tooltip: 'Exportar para Excel',
                    iconCls: 'icon-page-excel',
                    handler: function (b, e) {
                        exportarExcel('<%= Url.Action("ExportarXlsFormacaoAutomatica", "FormacaoAutomatica") %>');
                    }

                }]
            });

            new Ext.Viewport({
                layout: 'border',
                margins: 10,
                items: [
                {
                    region: 'north',
                    height: 185,
                    autoScroll: false,
                    items: [{
                        region: 'center',
                        applyTo: 'header-content'
                    },
                        filtros]
                },
                grid
            ]
            });

        });
    </script>
</asp:Content>
<asp:Content runat="server" ID="Content2" ContentPlaceHolderID="Content">
    <div id="header-content">
        <h1>
            Formação Automática</h1>
        <small>Você está em Operação > Circulação > Formação Automática</small>
        <br />
    </div>
</asp:Content>