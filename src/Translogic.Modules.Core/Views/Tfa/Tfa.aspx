﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Interna.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content runat="server" ID="HeadContent" ContentPlaceHolderID="HeadContent">
    <script type="text/javascript">
        var processosSelecionados = [];
    </script>
    <%Html.RenderPartial("ModalFecharLote"); %>
    <% 
        var anexos = ViewData["Anexos"] as List<Translogic.Modules.Core.Domain.Model.Dto.AnexoLaudoDto>;
        var valoresAnexo = Html.Raw(Json.Encode(anexos));
        var erros = ViewData["Erros"] as StringCollection;
        var valoresErros = Html.Raw(Json.Encode(erros));
    %>
    <script type="text/javascript">

		var valoresAnexos = <%= valoresAnexo %> || [];
		var anexos = [];
		var statusAnexos = [];
		var temAnexo = '<%= ViewData["TemAnexos"] %>';   
		var erros = <%= valoresErros %>;

		//*****STORES*****
		var gridStore = new Ext.data.JsonStore({
			root: "Items",
			totalProperty: 'Total',
			id: 'gridStore',
			name: 'gridStore',
			proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("ObterConsulta", "Tfa") %>', timeout: 600000 }),
			paramNames: {
				sort: "detalhesPaginacaoWeb.Sort",
				dir: "detalhesPaginacaoWeb.Dir",
				start: "detalhesPaginacaoWeb.Start",
				limit: "detalhesPaginacaoWeb.Limit"
			},
			fields: ['IdProcesso','NumTermo', 'NumProcesso', 'DataProcesso','Situacao','UnidEnvolvida', 'OrdemServico', 'DataProvisao', 'TfaAnexo', 'StatusCiente', 'PermiteExcluir']
		});

		var unidadesStore = new Ext.data.JsonStore({
			root: "Items",
			autoLoad: true,
			proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("ObterUN", "Tfa") %>', timeout: 600000 }),
			id: 'unidadesStore',
			fields: [
						 'IdUnidade',
						 'Descricao'
						]
		});

		var causaStore = new Ext.data.JsonStore({
			root: "Items",
			autoLoad: true,
			proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("ObterCausa", "Tfa") %>', timeout: 600000 }),
			id: 'causaStore',
			fields: [
						 'IdCausa',
						 'Descricao'
						]
		});

	   var situacaoStore = new Ext.data.JsonStore({
			root: "Items",
			autoLoad: true,
			proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("ObterSituacao", "Tfa") %>', timeout: 600000 }),
			id: 'situacaoStore',
			fields: [
						 'Id',
						 'Descricao'
						]
		});

		var anexoTfaStore = new Ext.data.JsonStore({
			root: "Items",
			autoLoad: true,
			proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("ObterAnexoTfa", "Tfa") %>', timeout: 600000 }),
			id: 'anexoTfaStore',
			fields: [
						 'Id',
						 'Descricao'
						]
		});
		
		var sm = new Ext.grid.CheckboxSelectionModel({
			renderer: function(v, p, record){
				
				if (record.data.Situacao.toUpperCase() == "PREPARADO P/ PAGAMENTO" || record.data.TfaAnexo == 0)
					return '<div class="x-grid3-row-checker">&#160;</div>';
				else
					return '<div>&#160;</div>';

			},
			selectAll: function(){

				var rowIndex=0;
				while(typeof(this.grid.getStore().getAt(rowIndex))!='undefined') {

					var record = this.grid.getStore().getAt(rowIndex);
					
					if (record.data.Situacao.toUpperCase() != "PREPARADO P/ PAGAMENTO")
						habilitarFechamento = false;

                    if (record.data.TfaAnexo == 1)
						habilitarDossie = false;

					if (record.data.Situacao.toUpperCase() == "PREPARADO P/ PAGAMENTO" || record.data.TfaAnexo == 0)
						this.grid.getSelectionModel().selectRow(rowIndex, true);
					else
						this.grid.getSelectionModel().deselectRow(rowIndex, true);

					rowIndex++;
				}
				
			}
		});


		// Busca e armazena ID linhas selecionadas
		function BuscaIDProcessosSelecionados() {

			var selection = sm.getSelections();
			var items = [];

			if (selection && selection.length > 0) {

				for (i = 0; i < selection.length; i++) {
					items.push(selection[i].data.NumProcesso);
				}

			}

			return items;
		};

		function alerta(msg) {
			Ext.Msg.show({
				title: "Aviso",
				msg: msg,
				buttons: Ext.Msg.OK,
				icon: Ext.MessageBox.QUESTION,
				minWidth: 200
			});
		}

		Ext.onReady(function () {

		
		windowModalFechamento = new Ext.Window({
			id: 'windowModalFechamento',
			title: 'Fechar Lote',
			modal: true,
			width: 605,
			closeAction:'hide',
			resizable: false,
			height: 515,
			items:[formModalFechamento],
			listeners: {				
				beforehide: function(){					
					VerificarSelecionadosHabilitaBtnFecharLote();
				

				}
			}
		});

		//*****JANELA MODAL PARA UPLOAD DE ARQUIVOS*****
		var formModal = new Ext.Window({
				id: 'modalArquivo',
				title: 'Importar',
				modal: true,
				width: 400,
				height: 300,
				resizable: true,
				autoScroll: true,
				autoLoad:
				{
					url: '<%=Url.Action("ModalStatus")%>',
					params: { anexos : anexos, statusAnexos : statusAnexos },
					text: "Buscando arquivos...",
					scripts: true,
					callback: function(el, sucess) {
						if (!sucess) {
							formModal.close();
						}
					}
				},
				listeners: {  
						resize: function(win, width, height, eOpts) {
							win.center();
						}
				}
			});

			if (temAnexo == 'True') {
				
				getAnexos();

				if (anexos.length > 0){
					var heightModal = 150 * anexos.length;
					if (heightModal > 450)
						heightModal = 450;
					//formModal.setHeight( heightModal );
					formModal.setSize(390, heightModal);
					formModal.show();
				}
			}

			if (erros != null && erros != "") {

				var mensagem = "";
				for(var i = 0; i < erros.length;i++) {
					mensagem += erros[i] + "<br>";
				}
				alerta(mensagem);
			}

			//****CAMPOS*****

			var txtInicioProcessos = new Ext.form.DateField({
				fieldLabel: 'Início Processos',
				id: 'txtInicioProcessos',
				name: 'txtInicioProcessos',
				dateFormat: 'd/n/Y',
				width: 83,
				plugins: [new Ext.ux.InputTextMask('99/99/9999', true)]
			});

			var txtFimProcessos = new Ext.form.DateField({
				fieldLabel: 'Fim  Processos',
				id: 'txtFimProcessos',
				name: 'txtFimProcessos',
				dateFormat: 'd/n/Y',
				width: 83,
				plugins: [new Ext.ux.InputTextMask('99/99/9999', true)]
			});

			var txtVagao = {
				xtype: 'textfield',
				name: 'txtVagao',
				id: 'txtVagao',
				fieldLabel: 'Vagão',
				autoCreate: {
					tag: 'input',
					type: 'text',
					autocomplete: 'off'                   
				},
				maskRe: /[0-9,]/,
				style: 'text-transform:uppercase;',
				width: 200
			};

			var ddlUn = new Ext.form.ComboBox({
				editable: true,
				typeAhead: true,
				forceSelection: true,
				disableKeyFilter: true,
				triggerAction: 'all',
				lazyRender: true,
				mode: 'local',
				valueField: 'IdUnidade',
				displayField: 'Descricao',
				fieldLabel: 'UN',
				id: 'ddlUn',
				name: 'ddlUn',
				width: 70,
				store: unidadesStore,
				value: ''
			});

			var ddlCausa = new Ext.form.ComboBox({
				editable: true,
				typeAhead: true,
				forceSelection: true,
				disableKeyFilter: true,
				triggerAction: 'all',
				lazyRender: true,
				mode: 'local',
				valueField: 'IdCausa',
				displayField: 'Descricao',
				fieldLabel: 'Causa',
				id: 'ddlCausa',
				name: 'ddlCausa',
				width: 245,
				store: causaStore,
				value: ''
			});

			var txtInicioSinistros = new Ext.form.DateField({
				fieldLabel: 'Início Sinistros',
				id: 'txtInicioSinistros',
				name: 'txtInicioSinistros',
				dateFormat: 'd/n/Y',
				width: 83,
				plugins: [new Ext.ux.InputTextMask('99/99/9999', true)]
			});

			var txtFimSinistros = new Ext.form.DateField({
				fieldLabel: 'Fim  Sinistros',
				id: 'txtFimSinistros',
				name: 'txtFimSinistros',
				dateFormat: 'd/n/Y',
				width: 83,
				plugins: [new Ext.ux.InputTextMask('99/99/9999', true)]
			});

			var txtNumProcesso = {
				xtype: 'textfield',
				name: 'txtNumProcesso',
				id: 'txtNumProcesso',
				fieldLabel: 'Num. Processo',
				autoCreate: {
					tag: 'input',
					type: 'text',
					autocomplete: 'off'                    
				},
				maskRe: /[0-9,]/,
				style: 'text-transform:uppercase;',
				width: 200
			};

			var txtOs = {
				xtype: 'textfield',
				name: 'txtOs',
				id: 'txtOs',
				fieldLabel: 'Os',
				autoCreate: {
					tag: 'input',
					type: 'text',
					autocomplete: 'off'
				},
				maskRe: /[0-9,]/,
				style: 'text-transform:uppercase;',
				width: 80
			};

			var ddlSituacao = new Ext.form.ComboBox({
				typeAhead: true,
				forceSelection: true,
				disableKeyFilter: true,
				triggerAction: 'all',
				lazyRender: true,
				mode: 'local',
				valueField: 'Id',
				displayField: 'Descricao',
				fieldLabel: 'Situação',
				id: 'ddlSituacao',
				name: 'ddlSituacao',
				width: 175,
				store: situacaoStore,
				value: ''
			});

			var txtNumTermo = {
				xtype: 'textfield',
				name: 'txtNumTermo',
				id: 'txtNumTermo',
				fieldLabel: 'Num. Termo',
				autoCreate: {
					tag: 'input',
					type: 'text',
					autocomplete: 'off'                    
				},
				maskRe: /[0-9,]/,
				style: 'text-transform:uppercase;',
				width: 200
			};

			var txtSindicancia = {
				xtype: 'textfield',
				name: 'txtSindicancia',
				id: 'txtSindicancia',
				fieldLabel: 'Sindicância',
				autoCreate: {
					tag: 'input',
					type: 'text',
					autocomplete: 'off'                    
				},
				maskRe: /[0-9,]/,
				style: 'text-transform:uppercase;',
				width: 200
			};

			var ddlAnexado = new Ext.form.ComboBox({
				editable: true,
				typeAhead: true,
				forceSelection: true,
				disableKeyFilter: true,
				triggerAction: 'all',
				lazyRender: true,
				mode: 'local',
				valueField: 'Id',
				displayField: 'Descricao',
				fieldLabel: 'TFA Anexado',
				id: 'ddlAnexado',
				name: 'ddlAnexado',
				width: 100,
				store: anexoTfaStore,
				value: ''
			});


			//***** Form de campos da linha inicial *****//
			var formtxtInicioProcessos = {
				width: 93,
				layout: 'form',
				border: false,
				items: [txtInicioProcessos]
			};

			var formtxtFimProcessos = {
				width: 93,
				layout: 'form',
				border: false,
				items: [txtFimProcessos]
			};

			var formtxtVagao = {
				width: 210,
				layout: 'form',
				border: false,
				items: [txtVagao]
			};

			var formddlUn = {
				width: 80,
				layout: 'form',
				border: false,
				items: [ddlUn]
			};

		   var formddlSituacao = {
				width: 185,
				layout: 'form',
				border: false,
				items: [ddlSituacao]
			};

			var formddlCausa = {
				width: 250,
				layout: 'form',
				border: false,
				items: [ddlCausa]
			};

			var lineTop = {
				layout: 'column',
				border: false,
				items: [formtxtInicioProcessos, formtxtFimProcessos, formtxtVagao, formddlUn, formddlSituacao, formddlCausa]
			};

			//***** Form de campos da linha do meio *****//
			var formtxtInicioSinistros = {
				width: 93,
				layout: 'form',
				border: false,
				items: [txtInicioSinistros]
			};

			var formtxtFimSinistros = {
				width: 93,
				layout: 'form',
				border: false,
				items: [txtFimSinistros]
			};

			var formtxtNumProcessos = {
				width: 210,
				layout: 'form',
				border: false,
				items: [txtNumProcesso]
			};

			var formtxtOs = {
				width: 90,
				layout: 'form',
				border: false,
				items: [txtOs]
			};

			var formtxtNumTermo = {
				width: 210,
				layout: 'form',
				border: false,
				items: [txtNumTermo]
			};
		
			var formtxtSindicancia = {
				width: 210,
				layout: 'form',
				border: false,
				items: [txtSindicancia]
			};         

			var formddlAnexado = {
				width: 110,
				layout: 'form',
				border: false,
				items: [ddlAnexado]
			};

			var lineMiddle = {
				layout: 'column',
				border: false,
				items: [formtxtInicioSinistros, formtxtFimSinistros, formtxtNumProcessos, formtxtOs, formtxtNumTermo, formtxtSindicancia, formddlAnexado]
			};

			var btnAnexar = new Ext.ux.form.FileUploadField({

					id: 'btnAnexar',
					buttonOnly: true,
					buttonText: 'Anexar TFA',
					tooltip: 'Selecionar arquivo',
					listeners: {
					
						afterrender: function () {
						
							$("#btnAnexar-file").change(function (event) {

								var file = event.target.value;
								
								if (validateFileExtension(file)) {
										Ext.getBody().mask("Carregando arquivo, isso pode levar alguns minutos, por favor aguarde a finalização", "x-mask-loading");
										var url = '<%= Url.Action("upload", "Tfa") %>';
										$(".x-form").attr("action", url);
										$(".x-form").attr("encoding", "multipart/form-data");
										painelFiltros.getForm().submit();
								}
								else {
									alerta("Favor adicionar um arquivo .zip contendo os anexos");
								}
						});
					}
				}
			});

			var fieldset = {
				xtype: 'fieldset',
				border: false,
				items: [lineTop, lineMiddle, btnAnexar]
			};


			//***** GRID *****//

			// Toolbar da Grid
			var pagingToolbar = new Ext.PagingToolbar({
				pageSize: 200,
				store: gridStore,
				displayInfo: true,
				displayMsg: App.Resources.Web.GridExibindoRegistros,
				emptyMsg: App.Resources.Web.GridSemRegistros,
				paramNames: {
					start: "detalhesPaginacaoWeb.Start",
					limit: "detalhesPaginacaoWeb.Limit"
				}
			});

			var Actions = new Ext.ux.grid.RowActions({
				id: 'colTfaAnexos',
				dataIndex: '',
				header: 'TFA Anexo',
				align: 'center',
				autoWidth: false,
				width: 25,
				actions: [
							{ iconCls: 'icon-del', tooltip: 'Excluir' , hideIndex: 'PermiteExcluir' },
							{ iconCls: 'icon-find', tooltip: 'Vizualisar', hideIndex: 'TfaAnexo' }
						 ],
						callbacks: {

								'icon-del': function (grid, record, action, row, col) 
								{

									if (record.data.NumProcesso) {
										if (Ext.Msg.confirm("Excluir", "Confirma a exclusão de TFA?", function (btn, text) {
											if (btn == 'yes') {
												
												ExcluirArquivoTFA(record.data.NumProcesso);

												pesquisar();
											}

										}));
									}
								}
												,
								'icon-find': function (grid, record, action, row, col) 
								{
									AbrirArquivoTFA(record.data.NumProcesso);
								}
							}
			});

			var cm = new Ext.grid.ColumnModel({
				defaults: {
					sortable: true
				},
				plugins: Actions,
				columns: [
					new Ext.grid.RowNumberer({width: 30}),
					sm,
					Actions,
					{ header: 'Num. Termo', dataIndex: "NumTermo", sortable: false, width: 40 },
					{ header: 'Num. Processo', dataIndex: "NumProcesso", sortable: false, width: 40 },
					{ header: 'Data', dataIndex: "DataProcesso", sortable: false, width: 60 },
					{ header: 'Situacao', dataIndex: "Situacao", sortable: false, width: 60 },
					{ header: 'Ciente', dataIndex: "StatusCiente", sortable: false, width: 60 },
					{ header: 'Unidades Envolvidas', dataIndex: "UnidEnvolvida", sortable: false, width: 100 },
					{ header: 'Os', dataIndex: "OrdemServico", sortable: false, width: 40 },
					{ header: 'Data Provisão', dataIndex: "DataProvisao", sortable: false, width: 50 },
					{ header: 'TfaAnexo', dataIndex: "TfaAnexo", sortable: false, hidden:true ,width: 50 }
				]
			});

			var grid = new Ext.grid.EditorGridPanel({
				id: 'grid',
				name: 'grid',
				autoLoadGrid: false,
				//limit: 10,
				height: 450,
				stripeRows: true,
				cm: cm,
				region: 'center',
				viewConfig: {
					forceFit: true,
					emptyText: 'Não possui dado(s) para exibição.'
				},
				//autoScroll: true,
				loadMask: { msg: App.Resources.Web.Carregando },
				store: gridStore,
				plugins: [Actions],
				bbar: pagingToolbar,
				sm: sm,
				tbar: [
					{
						id: 'btnExportarDossie',
						text: 'Exportar Dossiê',
						tooltip: 'Exportar Dossiê',
						type: 'submit',
						disabled: true,                        
						iconCls: 'icon-folder',
						handler: function (c) {
							limpaCamposDownloadDossie();
							popupDownloadDossie.show();
						}
					 }
				],
				listeners:
				{
					cellclick: function ( this_grid, rowIndex, columnIndex, e )
					{
//						var record = grid.getStore().getAt(rowIndex);  
//						if (record.data.TfaAnexo == 0){
//							VerificarSelecionadosHabilitaBtnExportarDossie();
//						}
                        VerificarSelecionadosHabilitaBtnExportarDossie();
						VerificarSelecionadosHabilitaBtnFecharLote();
					}
				}                
			});

			grid.getStore().proxy.on('beforeload', function (p, params) {

				var obj = criarObjetoTfa();
				params['dtInicioProcesso'] = obj.dtInicioProcesso;
				params['dtFinalProcesso'] = obj.dtFinalProcesso;
				params['vagao'] = obj.vagao;
				params['un'] = obj.un;
				params['dtInicioSinistros'] = obj.dtInicioSinistro
				params['dtFinalSinistros'] = obj.dtFinalSinistros;
				params['numProcesso'] = obj.numProcesso;
				params['os'] = obj.os;
				params['situacao'] = obj.situacao;
				params['causa'] = obj.causa;
				params['numTermo'] = obj.numTermo;
				params['sindicancia'] = obj.sindicancia;
				params['anexado'] = obj.anexado;
			});

			 var painelFiltros = new Ext.form.FormPanel({
				id: 'painelFiltro',
				layout: 'form',
				labelAlign: 'top',
				standardSubmit: true,
				url: '<%= Url.Action("upload", "Tfa") %>',
				border: false,
				autoHeight: true,
				buttonAlign: "center",
				region: 'north',
				title: "",              
				bodyStyle: 'padding: 5px 10px 0px 10px',
				items: [fieldset],
				buttons:
				[{
					id: 'btnPesquisar',
					text: 'Pesquisar',
					tooltip: 'Pesquisar',
					 region: 'south',
					iconCls: 'icon-find',
					handler: function (c) {             
						if(validaCamposNumericos())
						{
							if (validarFiltros()) 
							{
						 
								pesquisar();
							}
						}
					}
				},
				{
					id: 'btnExportarExcel',
					text: 'Exportar para Excel',
					tooltip: 'Exportar para Excel',
					type: 'submit',
					 region: 'north',
					iconCls: 'icon-page-excel',
					handler: function (c) {
					   if (validarFiltros()) 
						{
							ExportarExcel();
						}
					}
				 },
				 {
					id: 'btnFecharLote',
					text: 'Fechar lote',
					tooltip: 'Fechar lote',
					type: 'submit',
					region: 'north',
					disabled: true,
					iconCls: 'icon-computer-delete', 
					handler: function (c) {
					
						processosSelecionados = [];

						var selection = sm.getSelections();           
						if (selection.length > 0) {
				
							for(var i=0;i<selection.length;i++){
							 
								processosSelecionados.push({ "NumProcesso" : selection[i].data.NumProcesso, "DataProcesso" : selection[i].data.DataProcesso, "Situacao" : selection[i].data.Situacao} );
							}
					   }
					   
					   storeModal.loadData(processosSelecionados, false);

					   windowModalFechamento.show();
					   Ext.getCmp("btnFecharLote").setDisabled(true);                       
					   
					}
				  }
				]
			});

			var painelGrid = new Ext.form.FormPanel({
				id: 'painelGrid',
				layout: 'form',
				labelAlign: 'top',
				border: false,
				autoHeight: true,
				title: "",
				region: 'center',
				bodyStyle: 'padding: 0px 10px 0px 10px',
				items: [grid]
			});

		   painelFiltros.render("divFiltros");
		   painelGrid.render("divGrid");

			// *****javascript*****

			 // Validar se extensão do arquivo é um .zip
			function validateFileExtension(file) {

				var extension = file.split('.').pop();
				if (extension == "zip")
					return true;
				else
					return false;
			}

		   function getAnexos(){
		   
				for(var i = 0; i < valoresAnexos.length;i++){
					anexos.push(valoresAnexos[i].NumProcesso);
					statusAnexos.push(valoresAnexos[i].Status);
				}
			}

			function comparaDatas(dataIni, dataFim) {

				var arrDtIni = dataIni.split('/');
				var arrDtFim = dataFim.split('/');

				var dtIni = new Date(arrDtIni[2] + '/' + arrDtIni[1] + '/' + arrDtIni[0]);
				var dtFim = new Date(arrDtFim[2] + '/' + arrDtFim[1] + '/' + arrDtFim[0]);

				return dtIni > dtFim;
			}

			function validarPeriodoUmAno(dtIni, dtFim) {
			   
				var diasAno = 365;
				var arrDtIni = dtIni.split('/');
				var arrDtFim = dtFim.split('/');

				var anoIni = arrDtIni[2];
				var anoFim = arrDtFim[2];

				var dtIni = new Date(arrDtIni[2] + '/' + arrDtIni[1] + '/' + arrDtIni[0]);
				var dtFim = new Date(arrDtFim[2] + '/' + arrDtFim[1] + '/' + arrDtFim[0]);
				
				var diferenca = Math.abs(dtIni.getTime() - dtFim.getTime());
				var dia = 1000 * 60 * 60 * 24;
				var total = Math.round(diferenca / dia);

				//verifica se é ano bissexto
				if ((anoIni % 4 == 0) && ((anoIni % 100 != 0) || (anoIni % 400 == 0))) 
					diasAno = 366;
				
				if ((anoFim % 4 == 0) && ((anoFim % 100 != 0) || (anoFim % 400 == 0))) 
					diasAno = 366;
			   //fim de verificação de ano bissexto

				if (total > diasAno) {
					return true;
				}
				return false;
			}

			function pesquisar() {
				sm.clearSelections();
				VerificarSelecionadosHabilitaBtnExportarDossie();
				VerificarSelecionadosHabilitaBtnFecharLote();
				gridStore.load({ params: { start: 0, limit: 200} });
			}

			
			function validarFiltros() {
				var tfa = criarObjetoTfa();

				/* 
				•	Início Processos ( Deve aceitar até 1 ano entre o início e o Fim)
				•	Fim Processos (Deve aceitar até 1 ano entre o início e o Fim)
				*Ambos são obrigatórios quando um for preenchido
				*Data Fim não pode ser Inferior a data de Início
				*Obrigatórios quando (Inícios e Fim Sinsitros , ou Num Termo, ou Num Processo) não preenchidos*/

				if (tfa.dtInicioProcesso == ""
					&& tfa.dtFinalProcesso == ""
					&& tfa.dtInicioSinistro == ""
					&& tfa.dtFinalSinistros == ""
					&& tfa.numProcesso == ""
					&& tfa.vagao == ""
					&& tfa.os == ""
					&& tfa.numTermo == ""
					&& tfa.anexado == ""
					&& tfa.un == ""
					&& tfa.causa == ""
					&& tfa.situacao == ""
					&& tfa.sindicancia == "")
					{
						alerta("Selecione ou preencha no mínimo um filtro de pesquisa");
						return false;
					}
				   
					if (tfa.numTermo != ""
					&& tfa.dtInicioSinistro == ""
					&& tfa.dtFinalSinistros == ""
					&& tfa.numProcesso == ""
					&& tfa.vagao == ""
					&& tfa.os == ""
					&& tfa.anexado == ""
					&& tfa.un == ""
					&& tfa.causa == ""
					&& tfa.situacao == ""
					&& tfa.sindicancia == "")
					{
						if (tfa.dtInicioProcesso == "" && tfa.dtFinalProcesso == "")
						{
							alerta("Para pesquisa por numTermo(s) é obrigatório informar Inicio Processo e Fim Processo");
							return false;
						}
					}

					if (tfa.numTermo == ""
					&& tfa.dtInicioSinistro == ""
					&& tfa.dtFinalSinistros == ""
					&& tfa.numProcesso == ""
					&& tfa.vagao == ""
					&& tfa.os == ""
					&& tfa.anexado == ""
					&& tfa.un == ""
					&& tfa.causa == ""
					&& tfa.situacao == ""
					&& tfa.sindicancia != "")
					{
						if (tfa.dtInicioProcesso == "" && tfa.dtFinalProcesso == "")
						{
							alerta("Para pesquisa por Sindicância(s) é obrigatório informar Inicio Processo e Fim Processo");
							return false;
						}
					}

				   

				/*
				•	Início Sinistros ( Deve aceitar até 1 ano entre o início e o Fim)
				•	Fim Sinistros    ( Deve aceitar até 1 ano entre o início e o Fim)
				*Ambos são obrigatórios quando um for preenchido
				*Data Fim não pode ser Inferior a data de Início
				*Obrigatórios quando (Inícios e Fim Processos , ou Num Termo, ou Num Processo) não preenchidos*/
				if ( (tfa.dtInicioProcesso == "" || tfa.dtFinalProcesso == "")
					&& tfa.dtInicioSinistro == ""
					&& tfa.dtFinalSinistros == ""
					&& tfa.numProcesso == ""
					&& tfa.numTermo == ""
					&& tfa.os == ""
					&& tfa.sindicancia == "")
					{
						alerta("Ambos os filtros de Data de Processos devem ser preenchidos");
						return false;
				}

				 if ( tfa.dtInicioProcesso != "" || tfa.dtFinalProcesso != "") {

						if (validarPeriodoUmAno(tfa.dtInicioProcesso, tfa.dtFinalProcesso)){
							alerta("Filtro de data não pode ser maior que 1 ano");
							return false;
						}

						if (comparaDatas(tfa.dtInicioProcesso, tfa.dtFinalProcesso)){
							alerta("Data Posterior deve ser superior a data anterior nos filtros");
							return false;
						}
					}

				if (tfa.dtInicioProcesso == "" 
					&& tfa.dtFinalProcesso == ""
					&& tfa.dtInicioSinistro == ""
					&& tfa.dtFinalSinistros == ""
					&& tfa.numProcesso == ""
					&& tfa.numTermo == ""
					&& tfa.os == ""
					&& tfa.sindicancia == "")
					{
						alerta("Ambos os filtros de Data de Sinistros devem ser preenchidos");
						return false;
					}

				if ( (tfa.dtInicioSinistro == "" || tfa.dtFinalSinistros == "")
					&& tfa.dtInicioProcesso == ""
					&& tfa.dtFinalProcesso == ""
					&& tfa.numProcesso == ""
					&& tfa.numTermo == ""
					&& tfa.os == ""
					&& tfa.sindicancia == "")
					{
						alerta("Ambos os filtros de Data de Sinistros devem ser preenchidos");
						return false;
				}

				if ( tfa.dtInicioSinistro != "" || tfa.dtFinalSinistros != "") {

					if (validarPeriodoUmAno(tfa.dtInicioSinistro, tfa.dtFinalSinistros)){
						alerta("Filtro de data não pode ser maior que 1 ano");
						return false;
					}

					if (comparaDatas(tfa.dtInicioSinistro, tfa.dtFinalSinistros)){
						alerta("Data Posterior deve ser superior a data anterior nos filtros");
						return false;
					}
				}

				/*Num Processo (Até 300 Registros separados por vírgula) *Obrigatório quando (Inícios e Fim Processos , ou Inícios e Fim Sinistros, ou Num Termo) não preenchidos*/
				if (tfa.dtInicioProcesso == "" 
				&& tfa.dtFinalProcesso == ""
				&& tfa.dtInicioSinistro == ""
				&& tfa.dtFinalSinistros == ""
				&& tfa.numProcesso == ""
				&& tfa.numTermo == ""
				&& tfa.os == ""
				&& tfa.sindicancia == "")
				{
					alerta("Num. do processo obrigatório");
					return false;
				}

				
				if (tfa.vagao != "") 
				{
					var vagao = tfa.vagao;
					var quantidade = vagao.split(",");
					if (quantidade.length > 300)
					{
						alerta("Vagões permitidos até 300 Registros separados por vírgula");
						return false;
					}
				}

				if (tfa.dtInicioProcesso == "" 
				&& tfa.dtFinalProcesso == ""
				&& tfa.dtInicioSinistro == ""
				&& tfa.dtFinalSinistros == ""
				&& tfa.numTermo == ""
				&& tfa.numProcesso != ""
				&& tfa.sindicancia == "")
				{
					var processos = tfa.numProcesso;
					var quantidade = processos.split(",");
					if (quantidade.length > 300)
					{
						alerta("Num. do processo permitido até 300 Registros separados por vírgula");
						return false;
					}
				}

				if (tfa.os != "") 
				{
					var os = tfa.os;
					var quantidade = os.split(",");
					if (quantidade.length > 300)
					{
						alerta("Os. permitido até 300 Registros separados por vírgula");
						return false;
					}
				}

				/*Num Termo *Obrigatório quando (Inícios e Fim Processos , ou Inícios e Fim Sinistros, ou Num Processo) não preenchidos*/
				if (tfa.dtInicioProcesso == "" 
				&& tfa.dtFinalProcesso == ""
				&& tfa.dtInicioSinistro == ""
				&& tfa.dtFinalSinistros == ""
				&& tfa.numProcesso == ""
				&& tfa.numTermo == ""
				&& tfa.os == ""
				&& tfa.sindicancia == "")
				{
					alerta("Num. do termo obrigatório");
					return false;
				}

				return true;
			}

			function criarObjetoTfa() {
				var tfa = new Object();
				tfa.dtInicioProcesso = Ext.getCmp("txtInicioProcessos").getRawValue();
				tfa.dtFinalProcesso = Ext.getCmp("txtFimProcessos").getRawValue();
				tfa.dtInicioSinistro = Ext.getCmp("txtInicioSinistros").getRawValue();
				tfa.dtFinalSinistros = Ext.getCmp("txtFimSinistros").getRawValue();
				tfa.numProcesso = Ext.getCmp("txtNumProcesso").getValue().trim();
				tfa.vagao = Ext.getCmp("txtVagao").getValue().trim();
				tfa.os = Ext.getCmp("txtOs").getValue().trim();
				tfa.numTermo = Ext.getCmp("txtNumTermo").getValue().trim();
				tfa.sindicancia = Ext.getCmp("txtSindicancia").getValue();
				/*DDLS*/
				tfa.anexado = Ext.getCmp("ddlAnexado").getValue();
				tfa.un = Ext.getCmp("ddlUn").getValue();
				tfa.causa = Ext.getCmp("ddlCausa").getValue();
				tfa.situacao = Ext.getCmp("ddlSituacao").getValue();
				return tfa;
			}

			function AbrirArquivoTFA(Id) {

				var url = '<%= Url.Action("AbrirArquivo") %>';
				url += "?NumProcesso=" + Id;

				window.open(url, "");

			};

			function ExcluirArquivoTFA(Id) {

				var url = '<%= Url.Action("ExcluiArquivoTFA") %>';
				url += "?NumProcesso=" + Id;

				$.ajax({
					url: url,
					type: "POST",
					dataType: 'json',
					async: false,
					timeout: 300000,
					contentType: "application/json; charset=utf-8",
					failure: function (conn, data) {
						Ext.Msg.alert("Mensagem de Erro", "Ocorreu um erro inesperado");
						Ext.getBody().unmask();
					},
					success: function (data) {
						if (data != null){
							if (data.sucesso){
								gridStore.load({ params: { start: 0, limit: 200} });
							}
							else{
								Ext.Msg.alert("Atenção", data.message);
							}
						}

						Ext.getBody().unmask();
					}
				});
			};
			

			function ExportarExcel() {
				if(validaCamposNumericos())
				{
					var obj = criarObjetoTfa();

					document.cookie = "numProcesso=" + obj.numProcesso;
					document.cookie = "numTermo=" + obj.numTermo;
					document.cookie = "os=" + obj.os;
					document.cookie = "sindicancia=" + obj.sindicancia;
					document.cookie = "vagao=" + obj.vagao;
					
					//URL
					var url = '<%= Url.Action("ObterConsultaTFAExportar", "Tfa") %>';
					//Parametros
					url += "?dtInicioProcesso=" + obj.dtInicioProcesso; ;
					url += '&dtFinalProcesso=' + obj.dtFinalProcesso;
					//url += '&vagao=' + obj.vagao;
					url += '&un=' + obj.un;
					url += '&dtInicioSinistros=' + obj.dtInicioSinistro;
					url += '&dtFinalSinistros=' + obj.dtFinalSinistros; 
					//url += '&numProcesso=' + obj.numProcesso;
					//url += '&os=' + obj.os;
					url += '&situacao=' + obj.situacao;
					url += '&causa=' + obj.causa;
					//url += '&numTermo=' + obj.numTermo;
					//url += '&sindicancia=' + obj.sindicancia;
					url += '&anexado=' + obj.anexado;

					window.open(url, "");
				}
			};

			
			// Ao clicar em checbox todos validar se habilita ou nao o botão
			$(".x-grid3-hd-checker").click(function () {
				VerificarSelecionadosHabilitaBtnExportarDossie();
				VerificarSelecionadosHabilitaBtnFecharLote();
			});
		});
		
		

		function SomenteNumeros(valor){
		   var regex = '^[0-9]+$';
		   if (valor.match(regex)) {
				return false;
			}
			return true;
		}

		function isNumber(n) 
		{
			return !isNaN(parseFloat(n)) && isFinite(n);
		};

		function validaCamposNumericos()
		{
			var valor;
			var arrayLength;
			//Vagão
			//valor = Ext.getCmp("txtVagao").getRawValue().replace(new RegExp(',', 'g'), '');
			valor = Ext.getCmp("txtVagao").getRawValue().split(",");
			if(valor != "")
			{
				arrayLength = valor.length;
				for (var i = 0; i < arrayLength; i++) 
				{                   
					if(!isNumber(valor[i]))
					{
						Ext.Msg.show({
						title: "Alerta",
						msg: "Campo Vagão apenas números separados por virgula (EX:999,999,999)",
						buttons: Ext.Msg.OK,
						icon: Ext.MessageBox.ERROR
						});
						return;
					}
				}
			}
			//Num. Processo
			//valor = Ext.getCmp("txtNumProcesso").getRawValue().replace(new RegExp(',', 'g'), '');
			valor = Ext.getCmp("txtNumProcesso").getRawValue().split(","); 
			if(valor != "")
			{
				arrayLength = valor.length;
				for (var i = 0; i < arrayLength; i++) 
				{                   
					if(!isNumber(valor[i]))
					{
							Ext.Msg.show({
							title: "Alerta",
							msg: "Campo Num. Processo apenas números separados por virgula (EX:999,999,999)",
							buttons: Ext.Msg.OK,
							icon: Ext.MessageBox.ERROR
						});
						return;
					}
				}
			}
			//OS
			//valor = Ext.getCmp("txtOs").getRawValue().replace(new RegExp(',', 'g'), '');
			valor = Ext.getCmp("txtOs").getRawValue().split(","); 
			if(valor[0] != "")
			{
				arrayLength = valor.length;
				for (var i = 0; i < arrayLength; i++) {                   
					if(!isNumber(valor[i]))
					{
						Ext.Msg.show({
							title: "Alerta",
							msg: "Campo OS apenas números separados por virgula (EX:999,999,999)",
							buttons: Ext.Msg.OK,
							icon: Ext.MessageBox.ERROR
						});
						return;
					}
				}
			}
			//Num. Termo
			//valor = Ext.getCmp("txtNumTermo").getRawValue().replace(new RegExp(',', 'g'), '');
			valor = Ext.getCmp("txtNumTermo").getRawValue().split(",");
			if(valor != "")
			{
				arrayLength = valor.length;
				for (var i = 0; i < arrayLength; i++) 
				{                   
					if(!isNumber(valor[i]))
					{
						Ext.Msg.show({
							title: "Alerta",
							msg: "Campo Num. Termo apenas números separados por virgula (EX:999,999,999)",
							buttons: Ext.Msg.OK,
							icon: Ext.MessageBox.ERROR
						});
						return;
					}
				}
			}
			//Sindicância
			//valor = Ext.getCmp("txtSindicancia").getRawValue().replace(new RegExp(',', 'g'), '');
			valor = Ext.getCmp("txtSindicancia").getRawValue().split(",");
			if(valor != "")
			{
				 arrayLength = valor.length;
				for (var i = 0; i < arrayLength; i++) 
				{                   
					if(!isNumber(valor[i]))
					{
						Ext.Msg.show({
							title: "Alerta",
							msg: "Campo Sindicância apenas números separados por virgula (EX:999,999,999)",
							buttons: Ext.Msg.OK,
							icon: Ext.MessageBox.ERROR
						});
						return;
					}
				}
			}      
			return true;
		};

		 //******modal para download de dossiê********
		var chkDownloadTFA = {
			xtype: 'checkbox',
			fieldLabel: 'TFA',
			id: 'chkDownloadTFA',
			cls: 'checkBoxDownloadDossie',
			name: 'chkDownloadTFA',
			validateField: true,
			value: false
		};


		var chkDownloadCTe = {
			xtype: 'checkbox',
			fieldLabel: 'CT-e',
			id: 'chkDownloadCTe',
			cls: 'checkBoxDownloadDossie',
			name: 'chkDownloadCTe',
			validateField: true,
			value: false
		};


		var chkDownloadNFe = {
			xtype: 'checkbox',
			fieldLabel: 'NF-e',
			id: 'chkDownloadNFe',
			cls: 'checkBoxDownloadDossie',
			name: 'chkDownloadNFe',
			validateField: true,
			value: false
		};


		var chkDownloadTicket = {
			xtype: 'checkbox',
			fieldLabel: 'Ticket',
			id: 'chkDownloadTicket',
			cls: 'checkBoxDownloadDossie',
			name: 'chkDownloadTicket',
			validateField: true,
			value: false
		};

		var chkDownloadFotos = {
			xtype: 'checkbox',
			fieldLabel: 'Fotos',
			id: 'chkDownloadFotos',
			cls: 'checkBoxDownloadDossie',
			name: 'chkDownloadFotos',
			validateField: true,
			value: false
		};


		var formchkDownloadTFA = {
			width: 50,
			layout: 'form',
			border: false,
			//labelWidth: 30,
			labelAlign: 'top',
			items: [chkDownloadTFA]
		};

		var formchkDownloadCTe = {
			width: 50,
			layout: 'form',
			border: false,
			labelAlign: 'top',
			items: [chkDownloadCTe]
		};

		var formchkDownloadNFe = {
			width: 50,
			layout: 'form',
			border: false,
			labelAlign: 'top',
			items: [chkDownloadNFe]
		};

		var formchkDownloadTicket = {
			width: 50,
			layout: 'form',
			border: false,
			labelAlign: 'top',
			items: [chkDownloadTicket]
		};

		var formchkDownloadFotos = {
			width: 50,
			layout: 'form',
			border: false,
			labelWidth: 40,
			labelAlign: 'top',
			items: [chkDownloadFotos]
		};


		var lineTitleDownloadDossie = {
			layout: 'column',
			border: false,
			region: 'center',
			items: [{
				text: 'Selecione o(s) tipos de documentos para Download do Dossiê:',
				name: 'lblTitulo',
				id: 'lblTitulo',
				labelStyle: 'font: 10px ',
				style: 'font-weight: bold',
				hidden: false,
				height: 20,
				xtype: 'label'
			}]
		};

		var lineDownloadDossie = {
			layout: 'column',
			border: false,
			region: 'center',
			width: '100%',
			bodyStyle: 'margin-left: 70px',
			items: [formchkDownloadTFA, formchkDownloadCTe, formchkDownloadNFe, formchkDownloadTicket, formchkDownloadFotos]
		};


		var formDownloadDossie = new Ext.form.FormPanel({
			id: 'formDownloadDossie',
			width: '100%',
			//height: 100,
			region: 'center',
			autoScroll: true,
			labelAlign: 'left',
			items: [
				lineTitleDownloadDossie,
				lineDownloadDossie
			],
			buttonAlign: "center",
			buttons: [{
					text: "OK",
					handler: function() {
						if (!(Ext.getCmp("chkDownloadTFA").getValue() ||
								Ext.getCmp("chkDownloadCTe").getValue() ||
								Ext.getCmp("chkDownloadNFe").getValue() ||
								Ext.getCmp("chkDownloadTicket").getValue() ||
								Ext.getCmp("chkDownloadFotos").getValue())) {

							alerta("Favor selecionar no mínimo um tipo de documento");
							return;
						}

						var options = 0;

						//ligação de bits
						//TFA = 1,
						//CTE = 2,
						//NFE = 4,
						//TKT = 8,
						//Foto = 16

						if (Ext.getCmp("chkDownloadTFA").getValue())
							options += 1;

						if (Ext.getCmp("chkDownloadCTe").getValue())
							options += 2;

						if (Ext.getCmp("chkDownloadNFe").getValue())
							options += 4;

						if (Ext.getCmp("chkDownloadTicket").getValue())
							options += 8;

						if (Ext.getCmp("chkDownloadFotos").getValue())
							options += 16;


						var url = '<%= Url.Action("BaixaZIPDossie") %>';
						url += "?items=" + BuscaIDProcessosSelecionados();
						url += "&options=" + options;
						window.open(url, "");
						//popupDownloadDossie.hide();
					}
				},
				{
					text: "Cancelar",
					handler: function() {
						popupDownloadDossie.hide();
					}
				}
			]
		});



		var popupDownloadDossie = new Ext.Window({
			id: 'popupDownloadDossie',
			title: 'Download de Dossiê',
			modal: true,
			width: 400,
			closeAction: 'hide',
			height: 140,
			items: [formDownloadDossie],
			listeners: {
				'hide': function() {
					limpaCamposDownloadDossie();
				}
			}
		});

		//popupDownloadDossie.show();
		function limpaCamposDownloadDossie() {
			Ext.getCmp("chkDownloadTFA").setValue(false);
			Ext.getCmp("chkDownloadCTe").setValue(false);
			Ext.getCmp("chkDownloadNFe").setValue(false);
			Ext.getCmp("chkDownloadTicket").setValue(false);
			Ext.getCmp("chkDownloadFotos").setValue(false);
		}

		function VerificarSelecionadosHabilitaBtnFecharLote() {

			var selection = sm.getSelections();
			var selecionado = selection.length > 0 ? true : false;

			if (selecionado == true) {

				var habilitar = true;
				for (var i = 0; i < selection.length; i++) {

					if (selection[i].data.Situacao != "PREPARADO P/ PAGAMENTO") {
						habilitar = false;
					}
				}

				if (habilitar) {
					Ext.getCmp("btnFecharLote").setDisabled(false);
				}
				else {
					Ext.getCmp("btnFecharLote").setDisabled(true);
				}

			}
			else {
				Ext.getCmp("btnFecharLote").setDisabled(true);
			}
		};

        function VerificarSelecionadosHabilitaBtnExportarDossie() {
        
			var selection = sm.getSelections();
			var selecionado = selection.length > 0 ? true : false;

            if (selecionado == true) {

				var habilitar = true;
				for (var i = 0; i < selection.length; i++) {

					if (selection[i].data.TfaAnexo == 1) {
						habilitar = false;
					}
				}

				if (habilitar) {
					Ext.getCmp("btnExportarDossie").setDisabled(false);
				}
				else {
					Ext.getCmp("btnExportarDossie").setDisabled(true);
				}

			}
			else {
				Ext.getCmp("btnExportarDossie").setDisabled(true);
			}
		};


    </script>
</asp:Content>
<asp:Content runat="server" ID="Content2" ContentPlaceHolderID="Content">
    <div id="header-content">
        <h1>
            Consulta TFA</h1>
        <small>Você está em Operação > Seguro > Consulta de TFA</small>
        <br />
    </div>
    <div id="divFiltros">
    </div>
    <div id="divGrid">
    </div>
</asp:Content>
