﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%@ Import Namespace="Translogic.Modules.Core.Domain.Model.Tfa" %>
<div id="div-form-novo-email">
</div>
<script type="text/javascript">

    var idTerminal = '<%=ViewData["idTerminal"] %>';

    var fieldEmail = {
        xtype: 'textfield',
        id: 'txtEmail',
        fieldLabel: 'E-mail',
        name: 'fieldEmail',
        allowBlank: false,
        maxLength: 50,
        autoCreate: { 
            tag: "input",
            maxlength: 50,
            type: "text",
            size: "50",
            autocomplete: "off"
        }, 
        width: 240,
        hiddenName: 'fieldEmail',
        style: 'text-transform: lowercase;',
        listeners:
        {
            specialKey: function (field, e) {
                if (e.getKey() == e.ENTER) {
                    Salvar();
                }
            }
        }
    };


    var colEmail = {
        width: 250,
        layout: 'form',
        border: false,
        items: [fieldEmail]
    };

    
    var arrlinha1 = {
        layout: 'column',
        border: false,
        items: [colEmail]
    };


    var arrCampos = new Array();
    arrCampos.push(arrlinha1);

    var fieldsEmail = new Ext.form.FormPanel({
        id: 'fields-Email',
        title: "",
        region: 'center',
        bodyStyle: 'padding: 15px',
        labelAlign: 'top',
        items: [arrCampos],
        buttonAlign: "center",
        buttons:
	            [
                 {
                     text: 'Salvar',
                     type: 'submit',
                     iconCls: 'icon-save',
                     handler: function (b, e) {
                         Salvar(); //AdicionarEmail    Salvar();
                     }
                 },
                 {
                     text: 'Cancelar',
                     handler: function (b, e) {
						windowNovoEmail.close();
                        // Ext.getCmp("fields-Email").getForm().reset();
                     },
                     scope: this
                 }
                ]
    });

		function Salvar() {
			var email = Ext.getCmp("txtEmail").getValue().trim().toLowerCase();

			if (email == '') {
				Ext.Msg.show({
				title: "Erro",
				msg: "Preencha o e-mail a ser cadastrado!",
				buttons: Ext.Msg.OK,
				icon: Ext.MessageBox.INFO,
				minWidth: 200
				});
			} 			
			else if (validaEmail(email) == '') {
				Ext.Msg.show({
				title: "Erro",
				msg: "O e-mail informado não é válido!",
				buttons: Ext.Msg.OK,
				icon: Ext.MessageBox.INFO,
				minWidth: 200
				});
			}
			else {
				var mensagem = "Você deseja adicionar o novo e-mail?";

				if (Ext.Msg.confirm("Confirmação", mensagem, function (btn, text) {
				    if (btn == 'yes') {
				        Ext.Ajax.request({
				            url: '<%= Url.Action("IncluirEmailTerminal", "TerminalTfa") %>',
				            success: function (response) {
				                var result = Ext.decode(response.responseText);
				                if (result.Success) {

				                    Ext.Msg.alert('Informação', "Cadastro efetuado com sucesso!");
				                    windowNovoEmail.close();
				                    grid.getStore().reload();
				                }
				                else {

				                    Ext.Msg.alert('Aviso', result.Message);
				                    Ext.getCmp("txtEmail").focus();
				                }
				            },
				            failure: function (conn, data) {
				                Ext.Msg.alert('Erro no cadastro do Email!');
				            },
				            method: "POST",
				            params: { idTerminal: idTerminal, email: email }
				        });
				    }
				}));
			}
		}

	function validaEmail(mail) {

        var er = RegExp(/^[A-Za-z0-9_\-\.]+@[A-Za-z0-9_\-\.]{2,}\.[A-Za-z0-9]{2,}(\.[A-Za-z0-9])?/);

        if (mail == "") {
            return false;
        } else if (er.test(mail) == false) {
            return false;
        }
        return true;
    }


   fieldsEmail.render("div-form-novo-email");
	 
	 $(function () { 
		Ext.getCmp("txtEmail").focus();
	});

</script>

