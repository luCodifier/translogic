﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>

<script type="text/javascript">

    //*****STORES*****
    var storeModal = new Ext.data.JsonStore({
        //data: processosSelecionados,
        autoLoad: true,
        fields: ['NumProcesso', 'DataProcesso', 'Situacao']
    });

    var formaPagamentoStore = new Ext.data.ArrayStore({
        id: 0,
        fields: [
            'Id',
            'FormaPagamento'
            ],
        data: [['D', 'D-DEPÓSITO'], ['C', 'C-ENCONTRO DE CONTAS'], ['E', 'E-ENCERRADO SEM PAGAMENTO']]
    });

    var sm = new Ext.grid.CheckboxSelectionModel({
        singleSelect: true,
        header: ''
    });

//    var pagingToolbar = new Ext.PagingToolbar({
//        pageSize: 1000,
//        store: storeModal,
//        displayInfo: true,
//        displayMsg: App.Resources.Web.GridExibindoRegistros,
//        emptyMsg: App.Resources.Web.GridSemRegistros,
//        paramNames: {
//            start: "detalhesPaginacaoWeb.Start",
//            limit: "detalhesPaginacaoWeb.Limit"
//        }
//    });

    var cm = new Ext.grid.ColumnModel({
        defaults: {
            sortable: true
        },
        columns: [
                    new Ext.grid.RowNumberer({ width: 30 }),
                    { header: 'NumProcesso', dataIndex: "NumProcesso", sortable: false, width: 50 },
                    { header: 'Data Processo', dataIndex: "DataProcesso", sortable: false, width: 60 },
                    { header: 'Situação', dataIndex: "Situacao", sortable: false, width: 120 }
                ]
    });

    var grid = new Ext.grid.EditorGridPanel({
        id: 'grid',
        name: 'grid',
        autoLoadGrid: true,
        height: 250,
        width: 550,
        autoLoadGrid: true,
        stripeRows: true,
        cm: cm,
        region: 'center',
        viewConfig: {
            forceFit: true,
            emptyText: 'Não possui registros para exibição.'
        },
        autoScroll: true,
        loadMask: { msg: App.Resources.Web.Carregando },
        store: storeModal,
        //bbar: pagingToolbar,
        sm: sm
    });

    var formgrid = {
        layout: 'form',
        width: 500,
        border: false,
        bodyStyle: 'padding: 5px',
        autoWidth: true,
        items: [grid]
    };

    var txtAnaliseProcesso = {
        xtype: 'textarea',
        name: 'txtAnaliseProcesso',
        id: 'txtAnaliseProcesso',
        fieldLabel: 'Analise do Processo (4000/4000)',
        maxlength: '1000',
        maxLengthText: '1000',
        width: 550,
        height: 60,
        enableKeyEvents: true,
        listeners: {
            keyUp: function () {
                contaCaracter();
            }
        }
    };

    var formtxtAnaliseProcesso = {
        layout: 'form',
        width: 200,
        border: false,
        bodyStyle: 'padding: 5px',
        autoWidth: true,
        items: [txtAnaliseProcesso]
    };

    var dtAnalise = new Ext.form.DateField({
        fieldLabel: 'Data Análise',
        id: 'dtAnalise',
        name: 'dtAnalise',
        dateFormat: 'd/n/Y',
        width: 90,
        maxLength: 10,
        value: new Date(),
        plugins: [new Ext.ux.InputTextMask('99/99/9999', false)]
    });

    var formdtAnalise = {
        layout: 'form',
        width: 92,
        border: false,
        bodyStyle: 'padding: 5px',
        autoWidth: true,
        items: [dtAnalise]
    };

    var dtPrepPagamento = new Ext.form.DateField({
        fieldLabel: 'Data Prep.Pagto',
        id: 'dtPrepPagamento',
        name: 'dtPrepPagamento',
        dateFormat: 'd/n/Y',
        width: 90,
        maxLength: 10,
        value: new Date(),
        plugins: [new Ext.ux.InputTextMask('99/99/9999', false)]
    });

    var formdtPrepPagamento = {
        layout: 'form',
        width: 92,
        border: false,
        bodyStyle: 'padding: 5px',
        autoWidth: true,
        items: [dtPrepPagamento]
    };

    var ddlFormaPagamento = new Ext.form.ComboBox({
        typeAhead: false,
        triggerAction: 'all',
        lazyRender: true,
        allowBlank: false,
        forceSelection: true,
        mode: 'local',
        store: formaPagamentoStore,
        valueField: 'Id',
        editable: false,
        forceSelection: false,
        displayField: 'FormaPagamento',
        fieldLabel: 'Forma de Pagamento',
        id: 'ddlFormaPagamento',
        width: 110
    });

    var formddlFormaPagamento = {
        layout: 'form',
        width: 102,
        border: false,
        bodyStyle: 'padding: 5px',
        autoWidth: true,
        items: [ddlFormaPagamento]
    };

    var dtPagamento = new Ext.form.DateField({
        fieldLabel: 'Data de Pagamento',
        id: 'dtPagamento',
        name: 'dtPagamento',
        dateFormat: 'd/n/Y',
        width: 90,
        maxLength: 10,
        value: new Date(),
        plugins: [new Ext.ux.InputTextMask('99/99/9999', false)]
    });

    var formdtPagamento = {
        layout: 'form',
        width: 92,
        border: false,
        bodyStyle: 'padding: 5px',
        autoWidth: true,
        items: [dtPagamento]
    };

    var dtEncerramento = new Ext.form.DateField({
        fieldLabel: 'Data de Encerramento',
        id: 'dtEncerramento',
        name: 'dtEncerramento',
        dateFormat: 'd/n/Y',
        width: 90,
        maxLength: 10,
        value: new Date(),
        plugins: [new Ext.ux.InputTextMask('99/99/9999', false)]
    });

    var formdtEncerramento = {
        layout: 'form',
        width: 92,
        border: false,
        bodyStyle: 'padding: 5px',
        autoWidth: true,
        items: [dtEncerramento]
    };

    //Linha Campos 1
    var arrCampos1 = {
        layout: 'column',
        border: false,
        items: [formtxtAnaliseProcesso]
    };
    //Linha Campos 2
    var arrCampos2 = {
        layout: 'column',
        border: false,
        items: [formdtAnalise, formdtPrepPagamento, formddlFormaPagamento, formdtPagamento, formdtEncerramento]
    };

    var formModalFechamento = new Ext.form.FormPanel
	({
	    id: 'formModalFechamento',
	    labelWidth: 80,
	    width: 605,
	    height: 485,
	    autoScroll: true,
	    bodyStyle: 'padding: 15px',
	    labelAlign: 'top',
	    items:
		[
            formgrid,
            arrCampos1,
            arrCampos2
		],
	    buttonAlign: "center",
	    listeners: {
	        beforehide: function () {
	            //gridStore.load({ params: { start: 0, limit: 200} });
	            VerificarSelecionadosHabilitaBtnFecharLote();
	            Ext.getCmp("ddlFormaPagamento").setValue("");
	        },
	        hide: function () {
	            LimpaCampos();
	        },
	        render: function () {
	            $('.x-tool-close').hide();
	        }
	    },
	    buttons: [
                            {
                                text: 'Sair',
                                type: 'submit',
                                id: 'btnSair',
                                //disabled: true,
                                iconCls: 'icon-cancel',
                                handler: function (b, e) {
                                    Ext.getCmp("txtAnaliseProcesso").label.dom.innerText = "Analise do Processo(4000/4000 ):";
                                    LimpaCampos();
                                    windowModalFechamento.hide();
                                }
                            },
                            {
                                text: 'Salvar',
                                type: 'submit',
                                id: 'btnSalvar',
                                //disabled: true,
                                iconCls: 'icon-save',
                                handler: function (b, e) {

                                    //Ativa GIF Loading
                                    Ext.getBody().mask("Processando dados...", "x-mask-loading");
                                    //Executa processo Atualizar Status
                                    Salvar();
                                    //Desativa GIF Loading
                                    Ext.getBody().unmask();
                                }
                            }
                ]
	});                    

    //Executa processo ATUALIZAR STATUS dos processos
    function Salvar() {
        
        if (ValidaCamposObrigatorios()) {
           
            var processos = GeraArrayProcessos();
            var dtPrepPagamento = Ext.getCmp("dtPrepPagamento").getValue();
            var ddlFormaPagamento = Ext.getCmp("ddlFormaPagamento").getValue();
            var dtPagamento = Ext.getCmp("dtPagamento").getValue();
            var dtEncerramento = Ext.getCmp("dtEncerramento").getValue();
            var analiseProcesso = Ext.getCmp("txtAnaliseProcesso").getValue();
            var jsonRequest = $.toJSON({ processos: processos,
                dtPrepPagamento: dtPrepPagamento,
                formaPagamento: ddlFormaPagamento,
                dtPagamento: dtPagamento,
                dtEncerramento: dtEncerramento,
                analiseProcesso: analiseProcesso
            });
            $.ajax({
                url: '<%= Url.Action("FecharLote", "Tfa") %>',
                type: "POST",
                dataType: 'json',
                data: jsonRequest,
                timeout: 300000,
                contentType: "application/json; charset=utf-8",
                failure: function (conn, data) {
                    Ext.Msg.alert("Mensagem de Erro", "Ocorreu um erro inesperado");
                    Ext.getBody().unmask();
                },
                success: function (result) {
                  
                    if (!result.sucesso) {
                        Ext.Msg.show({ title: 'Aviso', msg: result.Message, buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                        Ext.getBody().unmask();
                    }
                    else {
                        Ext.getBody().unmask();
                        Ext.Msg.show({
                            title: 'Aviso',
                            msg: result.message,
                            buttons: Ext.Msg.OK,
                            fn: Sair(),
                            animEl: 'elId',
                            icon: Ext.MessageBox.INFO
                        });
                    }
                }
            });
        }
    }

    function ValidaCamposObrigatorios() {
        
        var qtd = grid.getStore().getRange().length;
        var data = new Date();
        var txtAnaliseProcesso = Ext.getCmp("txtAnaliseProcesso").getValue();
        var dtAnalise = Ext.getCmp("dtAnalise").getValue();
        var dtPrePgto = Ext.getCmp("dtPrepPagamento").getValue();
        var dtPagamento = Ext.getCmp("dtPagamento").getValue();
        var dtEncerramento = Ext.getCmp("dtEncerramento").getValue();
        var formaPagamento = Ext.getCmp("ddlFormaPagamento").getValue();

       
        //Validação Comum, tanto para fechamento de processos em lote como individual
        if (formaPagamento == "") {
            formModalFechamento.getForm().isValid();
            Ext.Msg.show({ title: 'Aviso', msg: 'Forma de pagamento é obrigatório.', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
            return false;
        }

        if (dtPagamento == "") {
            Ext.Msg.show({ title: 'Aviso', msg: 'Data de pagamento é obrigatório.', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
            return false;
        }

        if (dtPrePgto == "") {
            Ext.Msg.show({ title: 'Aviso', msg: 'Data de Preparação do pagamento é obrigatório.', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
            return false;
        }

        if (dtEncerramento == "") {
            Ext.Msg.show({ title: 'Aviso', msg: 'Data de encerramento é obrigatório.', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
            return false;
        }

        //se NÃO for fechamento em lote
        if (grid.getStore().getRange().length == 1) {

            if (dtAnalise == "") {
                Ext.Msg.show({ title: 'Aviso', msg: 'Campo Data Análise é obrigatório.', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                return false;
            }
        }
        else {
            if (dtAnalise =="" ) {
                dtAnalise = new Date().toDateString();
            }
        }
        //dtPrePgto = new Date().toDateString();
        return true;
    }

    //Fecha tela modal   
    function Sair() {
 
        Ext.getCmp("txtAnaliseProcesso").label.dom.innerText = "Analise do Processo(4000/4000 ):";      
        LimpaCampos();
        gridStore.load({ params: { start: 0, limit: 200} });
        windowModalFechamento.hide();
    }

    function LimpaCampos() {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!

        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd;
        }
        if (mm < 10) {
            mm = '0' + mm;
        }
        var today = dd + '/' + mm + '/' + yyyy;

        Ext.getCmp("txtAnaliseProcesso").setValue("");
        Ext.getCmp("dtPrepPagamento").setValue(today);
        Ext.getCmp("dtAnalise").setValue(today);
        Ext.getCmp("dtPagamento").setValue(today);
        Ext.getCmp("dtEncerramento").setValue(today);
        Ext.getCmp("ddlFormaPagamento").setValue("");
    }


    //Gera Array com todos os numeros de processos selecionados pelo usuario
    function GeraArrayProcessos() {
        var processos = [];
        var selection = sm.getSelections();
        if (selection && selection.length > 0) {
            for (var i = 0; i < selection.length; i++) {
                processos.push(selection[i].data.NumProcesso);
            }
        }

        return processos;
    }
    var contaCaracter = function () {

        var limite = 4000;
        var analiseProcesso = $("#txtAnaliseProcesso").val();
        if (analiseProcesso.length > limite) {
            document.getElementById("txtAnaliseProcesso").value = document.getElementById("txtAnaliseProcesso").value.substr(0, limite);
            return;
        }
        var total = limite - analiseProcesso.length;
        Ext.getCmp("txtAnaliseProcesso").label.dom.innerText = "Analise do Processo(" + total + "/4000 ):";
    }

    function alerta(msg) {
        Ext.Msg.show({
            title: "Aviso",
            msg: msg,
            buttons: Ext.Msg.OK,
            icon: Ext.MessageBox.QUESTION,
            minWidth: 200
        });
    }
</script>
