﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Simples.Master" Inherits="System.Web.Mvc.ViewPage" %>


<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server">
<%using (var combiner = new ResourceCombiner("error"))
 {
	// CSS
	combiner.Add(Url.CSS("Common.css"));
 } %>
 <style>
 	BODY{padding:20px;}
 </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
	<div style="margin: 7em;TEXT-ALIGN: center">
		<div style="margin:20px 0 0 20px; color:Gray;">
			<h2><img src="<%=Url.Images("Messages/warning.gif")%>" alt="<%=Resources.Geral.Translogic%>" align="top" /> <%=Resources.Web.MensagemErroInesperado %></h2>
            <br /><h3>Nenhum arquivo de Dossiê encontrado para Download!</h3>
            <br />
			<em><%=Resources.Web.MensagemErroPersistir %></em>
		</div>
	</div>
</asp:Content>