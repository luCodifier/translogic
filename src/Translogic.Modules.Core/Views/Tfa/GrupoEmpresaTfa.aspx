﻿<%@ Page Title="Grupo de Empresas" Language="C#" MasterPageFile="~/Views/Shared/Interna.Master"
    Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">

        //*****VARIÁVEIS*****
        var windowNovoGrupoEmpresaTfa = null;
        

        //*****STORES*****
        var gridStore = new Ext.data.JsonStore({
            root: "Items",
            totalProperty: 'Total',
            id: 'gridStore',
            name: 'gridStore',
            proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("PesquisarGruposEmpresas","GrupoEmpresaTfa") %>', timeout: 600000 }),
            paramNames: {
                sort: "detalhesPaginacaoWeb.Sort",
                dir: "detalhesPaginacaoWeb.Dir",
                start: "detalhesPaginacaoWeb.Start",
                limit: "detalhesPaginacaoWeb.Limit"
            },
            fields: ['Id', 'NomeGrupo']
        });


        //*****JANELA MODAL PARA INCLUIR EMPRESAS*****

        function MostrarFormCadastro(id) {
            windowNovoGrupoEmpresaTfa = null;
            windowNovoGrupoEmpresaTfa = new Ext.Window({
                name: 'formCadastro',
                id: 'formCadastro',
                title: 'Cadastro de Grupo de Empresas',
                expandonShow: true,
                modal: true,
                width: 330,
                height: 150,
                closable: true,
                autoLoad: {
                    url: '<%= Url.Action("NovoGrupoEmpresaTfa", "GrupoEmpresaTfa") %>',
                    params: {
                        idGrupoEmpresa: id
                    },
                    scripts: true
                }
            });
            windowNovoGrupoEmpresaTfa.show();
        }


        //****CAMPOS*****
        var painelFiltros = new Ext.form.FormPanel(
        {
            id: 'painelFiltro',
            title: 'Filtros de Pesquisa',
            name: 'filtros',
            width: "100%",
            region: 'center',
            bodyStyle: 'padding: 10px',
            layout: 'column',
            labelAlign: 'top',
            align: 'left',
            border: false,
            items:
            [
                {
                    width: 800,
                    border: false,
                    layout: 'column',
                    align: 'left',
                    buttonAlign: 'center',
                    items:
                    [
                        {
                            width: 260,
                            layout: 'form',
                            border: false,
                            items:
                            [
                                {
                                    border: false,
                                    layout: 'form',
                                    items:
                                    [
                                        {
                                            xtype: 'textfield',
                                            name: 'txtNomeGrupo',
                                            id: 'txtNomeGrupo',
                                            fieldLabel: 'Nome do Grupo de Empresas',
                                            maxLength: 30,
                                            autoCreate:
                                            { 
                                                tag: "input",
                                                maxlength: 50,
                                                type: "text",
                                                size: "50",
                                                autocomplete: "off"
                                            }, 
                                            style: 'text-transform:uppercase;',
                                            width: 200,
                                            listeners:
                                            {
                                                specialKey: function (field, e) {
                                                    if (e.getKey() == e.ENTER) {
                                                        Pesquisar();
                                                    }
                                                }
                                            }
                                        }
                                    ]
                                }
                            ]
                        }
                    ],
                    buttons:
                    [
                        {
                            name: 'btnPesquisar',
                            id: 'btnPesquisar',
                            text: 'Pesquisar',
                            iconCls: 'icon-find',
                            handler: function () 
                            {
                                Pesquisar();
                            }
                        },
                        {
                            text: 'Limpar',
                            handler: function (b, e) 
                            {
                                limparCamposGeral();
                            },
                            scope: this
                        }
                    ]
                }
            ]
        });

        //***** GRID *****//
        var botaoGridExcluir = new Ext.ux.grid.RowActions({
            dataIndex: '',
            header: '',
            align: 'center',
            actions: [{
                iconCls: 'icon-del',
                tooltip: 'Deletar Registro'
            }],
            callbacks: {
                'icon-del': function (grid, record, action, row, col) {
                    if (Ext.Msg.confirm("Exclusão", "Deseja excluir o Grupo de Empresas?", function (btn, text) {
                        if (btn == 'yes') {
                            grid.setDisabled(true);
                            var idGrupoEmpresa = grid.getStore().getAt(row).get('Id');
                            Ext.Ajax.request({
                                url: '<%= Url.Action("ExcluirGrupoEmpresa","GrupoEmpresaTfa") %>',
                                params: {
                                    idGrupoEmpresa: idGrupoEmpresa
                                },
                                success: function (response) {
                                    var result = Ext.decode(response.responseText);

                                    grid.setDisabled(false);
                                    //grid.getStore().load();
                                    Pesquisar();
                                    alerta(result.Message);

                                },
                                erro: function () {
                                    grid.setDisabled(false);
                                }
                            });
                        }
                    }));
                }
            }
        });

        var botaoGridEditar = new Ext.ux.grid.RowActions({
            dataIndex: '',
            header: '',
            align: 'center',
            actions: [{
                iconCls: 'icon-edit',
                tooltip: 'Editar Registro'
            }],
            callbacks: {
                'icon-edit': function (grid, record, action, row, col) {
                    id = grid.getStore().getAt(row).get('Id');
                    MostrarFormCadastro(id);
                }
            }
        });


        // Toolbar da Grid
        var pagingToolbar = new Ext.PagingToolbar({
            pageSize: 50,
            store: gridStore,
            displayInfo: true,
            displayMsg: App.Resources.Web.GridExibindoRegistros,
            emptyMsg: App.Resources.Web.GridSemRegistros,
            paramNames: {
                start: "detalhesPaginacaoWeb.Start",
                limit: "detalhesPaginacaoWeb.Limit"
            }            
        });


        var grid = new Ext.grid.EditorGridPanel({
            id: 'grid',
            name: 'grid',
            autoLoadGrid: false,
            height: 450,
            limit: 10,
            //width: 870,
            stripeRows: true,
            region: 'center',
            viewConfig: {
                forceFit: true,
                emptyText: 'Não possui dado(s) para exibição.'
            },
            plugins: [botaoGridExcluir, botaoGridEditar],
            autoScroll: true,
            loadMask: { msg: App.Resources.Web.Carregando },
            store: gridStore,
            tbar: [
                    {
                        xtype: 'button',
                        name: 'btnNovo',
                        id: 'btnNovo',
                        text: 'Novo Grupo de Empresas',
                        iconCls: 'icon-new',
                        handler: function () {
                            MostrarFormCadastro(0);
                        }
                    }
                ],
            bbar: pagingToolbar,
            columns: [new Ext.grid.RowNumberer(),
                        botaoGridExcluir,
                        botaoGridEditar,                      
                        {
                            header: 'Nome do Grupo',
                            width: 100,
                            autoExpandColumn: true,
                            dataIndex: "NomeGrupo",
                            sortable: true
                        }
                    ],
            filteringToolbar: [{
                xtype: 'button',
                name: 'btnNovo',
                id: 'btnNovo',
                text: 'Novo Grupo',
                iconCls: 'icon-new',
                handler: function () {
                    MostrarFormCadastro(0);
                }
            }]

        });


        var painelGrid = new Ext.form.FormPanel({
            id: 'painelGrid',
            layout: 'form',
            labelAlign: 'top',
            border: false,
            autoHeight: true,
            title: "",
            region: 'center',
            bodyStyle: 'padding: 0px 10px 0px 10px',
            items: [grid]
        });


        //***** FUNÇÕES *****//
        function alerta(msg) {
            Ext.Msg.show({
                title: "Aviso",
                msg: msg,
                buttons: Ext.Msg.OK,
                icon: Ext.MessageBox.QUESTION,
                minWidth: 200
            });
        }


        function limparCamposGeral() {            
            grid.getStore().removeAll();
            grid.getStore().totalLength = 0;
            grid.getBottomToolbar().bind(grid.getStore());
            grid.getBottomToolbar().updateInfo();
            Ext.getCmp("painelFiltro").getForm().reset();
        }

        function Pesquisar() {
            grid.getStore().load({ params: { start: 0, limit: 50} });
        }



        //***** inicializa tela ******
        Ext.onReady(function () {
            painelFiltros.render("divFiltros");
            painelGrid.render("divGrid");
            limparCamposGeral();
        });

        grid.store.proxy.on('beforeload', function (p, params) {
            if(Ext.getCmp('txtNomeGrupo').getValue())
                params['nomeGrupo'] = Ext.getCmp('txtNomeGrupo').getValue().toUpperCase();
        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div id="header-content">
        <h1>
            Seguro</h1>
        <small>Você está em Cadastro > TFA > Grupo de Empresas</small>
        <br />
    </div>
    <div id="divFiltros">
    </div>
     <div id="divGrid">
    </div>
</asp:Content>
