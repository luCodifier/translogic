﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% 
    if (ViewData["Anexos"] == null)
    {
        Response.End();
    }

    var anexos = ViewData["Anexos"] as string[];
    var status = ViewData["StatusAnexos"] as string[];
    
%>


<div style="padding: 15px">
    <table border="1" cellpadding="2" cellspacing="2">
        <thead>
            <tr>
                <td style="width: 250px; font-weight: bold; font-size: 12px" align="center">
                    Número do Processo
                </td>
                <td style="width: 250px; font-weight: bold; font-size: 12px" align="center">
                    Status
                </td>
            </tr>
        </thead>
        <tbody>
            <% if (anexos != null)
               {
                   for (var i = 0; i < anexos.Length; i++)
                   { %>
            <tr>
                <td align="center">                   
                    <%= anexos[i]%>
                </td>
                <td align="center">
                    <% if (status[i] == "carregado")
                       { %>
                    <img src="/Get.efh?file={Translogic.Core.Infrastructure.Web;}/Content/Images/Icons/accept.png" qtip="Carregado"
                        alt="Carregado" />
                    <% }
                       else if (status[i] == "atualizado")
                       {%>
                    <img src="/Get.efh?file={Translogic.Core.Infrastructure.Web;}/Content/Images/Icons/alert.png"
                        alt="Atualizado" qtip="Atualizado" />
                    <% }
                       else if (status[i] == "inexistente")
                       {%>
                    <img src="/Get.efh?file={Translogic.Core.Infrastructure.Web;}/Content/Images/Icons/delete.png"
                        alt="Inválido" qtip="Inválido"/>
                     <% }
                       else if (status[i] == "origemapp")
                       {%>
                    <img src="/Get.efh?file={Translogic.Core.Infrastructure.Web;}/Content/Images/Icons/delete.png"
                        alt="Arquivo carregado pelo app" qtip="Arquivo carregado pelo app"/>
                    <% } %>
                </td>
            </tr>
            <% }
               } %>
        </tbody>
    </table>
    <br />
    <br />

   
</div>