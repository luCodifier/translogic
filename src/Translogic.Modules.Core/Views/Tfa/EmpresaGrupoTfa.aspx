﻿<%@ Page Title="Gestão de Empresas" Language="C#" MasterPageFile="~/Views/Shared/Interna.Master"
    Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">

        //*****VARIÁVEIS*****
        var windowNovoEmpresaGrupoTfa = null;
        var idGrupoEmpresa = 0;
        

        //*****STORES*****
        var gridStore = new Ext.data.JsonStore({
            root: "Items",
            totalProperty: 'Total',
            id: 'gridStore',
            name: 'gridStore',
            proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("ObterListaEmpresaGrupoTfa","EmpresaGrupoTfa") %>', timeout: 600000 }),
            paramNames: {
                sort: "detalhesPaginacaoWeb.Sort",
                dir: "detalhesPaginacaoWeb.Dir",
                start: "detalhesPaginacaoWeb.Start",
                limit: "detalhesPaginacaoWeb.Limit"
            },
            fields: ['Id', 'Cgc', 'RazaoSocial']
        });

        var grupoEmpresaStore = new Ext.data.JsonStore({
            root: "Items",
            autoLoad: true,
            proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("ObterListaGrupoEmpresa", "EmpresaGrupoTfa") %>', timeout: 600000 }),
            id: 'grupoEmpresaStore',
            fields: ['Id', 'NomeGrupo'],
            listeners: {
                load: function (store, records) {
                    var rt = store.recordType;
                    store.insert(0, new rt({}));
                }
            }
        });


        //*****JANELA MODAL PARA INCLUIR EMPRESAS*****

        function MostrarFormCadastro() {
            windowNovoEmpresaGrupoTfa = null;
            windowNovoEmpresaGrupoTfa = new Ext.Window({
                name: 'formCadastro',
                id: 'formCadastro',
                title: 'Cadastro de Empresa',
                expandonShow: true,
                modal: true,
                width: 330,
                height: 200,
                closable: true,
                autoLoad: {
                    url: '<%= Url.Action("NovoEmpresaGrupoTfa", "EmpresaGrupoTfa") %>',
                    params: {
                        idGrupoEmpresa: Ext.getCmp("ddlGrupoEmpresa").getValue()
                    },
                    scripts: true
                }
            });
            windowNovoEmpresaGrupoTfa.show();
        }


        //****CAMPOS*****

        var ddlGrupoEmpresa = new Ext.form.ComboBox({
            editable: false,
            typeAhead: true,
            forceSelection: true,
            disableKeyFilter: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            valueField: 'Id',
            displayField: 'NomeGrupo',
            fieldLabel: 'Grupo de Empresas',
            id: 'ddlGrupoEmpresa',
            name: 'ddlGrupoEmpresa',
            width: 245,
            tpl: '<tpl for="."><div class="x-combo-list-item">{NomeGrupo}&nbsp;</div></tpl>',
            store: grupoEmpresaStore,
            value: '',
            listeners: {
                select: {
                    fn: function () {
                        idGrupoEmpresa = this.getValue();
                        if(this.getValue()){
                            Ext.getCmp("btnNovo").setDisabled(false);
                            Pesquisar();
                        }
                        else{
                            limparCamposGeral();
                        }
                    }
                }
            }
        });


        var painelFiltros = new Ext.form.FormPanel({
            id: 'painelFiltro',
            title: 'Filtros de Pesquisa',
            name: 'filtros',
            width: "100%",
            region: 'center',
            bodyStyle: 'padding: 10px',
            standardSubmit: true,
            url: '<%= Url.Action("EmpresaGrupoTfa","EmpresaGrupoTfa") %>',
            initEl: function (el) {
                this.el = Ext.get(el);
                this.id = this.el.id || Ext.id();
                if (this.standardSubmit) {
                    this.el.dom.action = this.url;
                } else {
                    this.el.on('submit', this.onSubmit, this);
                }
                this.el.addClass('x-form');
            },
            layout: 'column',
            labelAlign: 'top',
            align: 'left',
            border: false,
            items: [{
                width: 800,
                border: false,
                layout: 'column',
                align: 'left',
                items: [{
                    width: 260,
                    layout: 'form',
                    border: false,
                    items: [{
                        border: false,
                        layout: 'form',
                        items: [ddlGrupoEmpresa]
                    }]
                }            
            ]
            }],
            buttonAlign: 'center',
            buttons: [{
                name: 'btnPesquisar',
                id: 'btnPesquisar',
                text: 'Pesquisar',
                iconCls: 'icon-find',
                handler: function () {                    
                    Pesquisar();                    
                }
            }, {
                text: 'Limpar',
                handler: function (b, e) {
                    limparCamposGeral();
                },
                scope: this
            }]
        });


        //***** GRID *****//
        var botaoGridExcluir = new Ext.ux.grid.RowActions({
            dataIndex: '',
            header: '',
            align: 'center',
            actions: [{
                iconCls: 'icon-del',
                tooltip: 'Deletar Registro'
            }],
            callbacks: {
                'icon-del': function (grid, record, action, row, col) {
                    if (Ext.Msg.confirm("Exclusão", "Deseja excluir a Empresa do Grupo de Empresas?", function (btn, text) {
                        if (btn == 'yes') {                            
                            grid.setDisabled(true);
                            var idEmpresaGrupoTfa = grid.getStore().getAt(row).get('Id');
                            Ext.Ajax.request({
                                url: '<%= Url.Action("ExcluirEmpresaGrupoTfa","EmpresaGrupoTfa") %>',
                                params: {
                                    idEmpresaGrupoTfa: idEmpresaGrupoTfa
                                },
                                success: function (response) {
                                    var result = Ext.decode(response.responseText);

                                    grid.setDisabled(false);
                                    grid.getStore().load();
                                    alerta(result.Message);

                                },
                                erro: function () {
                                    grid.setDisabled(false);
                                }
                            });
                        }
                    }));
                }
            }
        });

        

        // Toolbar da Grid
        var pagingToolbar = new Ext.PagingToolbar({
            pageSize: 50,
            store: gridStore,
            displayInfo: true,
            displayMsg: App.Resources.Web.GridExibindoRegistros,
            emptyMsg: App.Resources.Web.GridSemRegistros,
            paramNames: {
                start: "detalhesPaginacaoWeb.Start",
                limit: "detalhesPaginacaoWeb.Limit"
            },
            doRefresh: function () {
                if (!Ext.getCmp("ddlGrupoEmpresa").getValue()) {
                    return false;
                }
                Pesquisar();
            }
        });




        var grid = new Ext.grid.EditorGridPanel({
            id: 'grid',
            name: 'grid',
            autoLoadGrid: false,
            height: 450,
            limit: 10,
            //width: 870,
            stripeRows: true,
            region: 'center',
            viewConfig: {
                forceFit: true,
                emptyText: 'Não possui dado(s) para exibição.'
            },
            plugins: [botaoGridExcluir],
            autoScroll: true,
            loadMask: { msg: App.Resources.Web.Carregando },
            store: gridStore,
            tbar: [
                    {
                        xtype: 'button',
                        name: 'btnNovo',
                        id: 'btnNovo',
                        text: 'Vincular Nova Empresa',
                        iconCls: 'icon-new',
                        handler: function () {
                            MostrarFormCadastro();
                        }
                    }
                ],
            bbar: pagingToolbar,
            columns: [new Ext.grid.RowNumberer(),
                        botaoGridExcluir,
                        {
                            header: 'CNPJ',
                            width: 30,
                            autoExpandColumn: false,
                            dataIndex: "Cgc",
                            sortable: true,
                            renderer: function (value, metaData, record) {
                                return value.replace(/^(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/, "$1.$2.$3/$4-$5");
                            }
                        },
                        {
                            header: 'Razão Social',
                            width: 100,
                            autoExpandColumn: true,
                            dataIndex: "RazaoSocial",
                            sortable: true
                        }
                    ],
            filteringToolbar: [{
                xtype: 'button',
                name: 'btnNovo',
                id: 'btnNovo',
                text: 'Vincular Nova Empresa',
                iconCls: 'icon-new',
                handler: function () {
                    MostrarFormCadastro();
                }
            }]

        });

        
        var painelGrid = new Ext.form.FormPanel({
            id: 'painelGrid',
            layout: 'form',
            labelAlign: 'top',
            border: false,
            autoHeight: true,
            title: "",
            region: 'center',
            bodyStyle: 'padding: 0px 10px 0px 10px',
            items: [grid]
        });


        //***** FUNÇÕES *****//
        function alerta(msg) {
            Ext.Msg.show({
                title: "Aviso",
                msg: msg,
                buttons: Ext.Msg.OK,
                icon: Ext.MessageBox.QUESTION,
                minWidth: 200
            });
        }

        
        function limparCamposGeral() {
            Ext.getCmp("btnNovo").setDisabled(true);
            grid.getStore().removeAll();
            grid.getStore().totalLength = 0;
            grid.getBottomToolbar().bind(grid.getStore());
            grid.getBottomToolbar().updateInfo();
            Ext.getCmp("painelFiltro").getForm().reset();
        }

        function Pesquisar() {
            if (Ext.getCmp("ddlGrupoEmpresa").getValue()) {
                grid.getStore().load({ params: { start: 0, limit: 50} });
            }
            else {
                alerta("Selecione um Grupo de Empresas");
            }
                        
        }

        

        //***** inicializa tela ******
        Ext.onReady(function () {
            painelFiltros.render("divFiltros");
            painelGrid.render("divGrid");
            limparCamposGeral();            
        });

        grid.store.proxy.on('beforeload', function (p, params) {            
            params['idGrupoEmpresa'] = Ext.getCmp('ddlGrupoEmpresa').getValue();
        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div id="header-content">
        <h1>
            Seguro</h1>
        <small>Você está em Cadastro > TFA > Gestão de Empresas</small>
        <br />
    </div>
    <div id="divFiltros">
    </div>
     <div id="divGrid">
    </div>
</asp:Content>
