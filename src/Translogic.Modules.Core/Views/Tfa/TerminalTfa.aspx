﻿<%@ Page Title="Cadastro de E-mails de Terminais" Language="C#" MasterPageFile="~/Views/Shared/Interna.Master"
    Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server">      
    <script type="text/javascript">

        //*****VARIÁVEIS*****
        var windowNovoEmail = null;
        var idTerminal = null;

        //*****STORES*****
        var gridStore = new Ext.data.JsonStore({
            root: "Items",
            totalProperty: 'Total',
            id: 'gridStore',
            name: 'gridStore',
            proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("ObterEmailsTerminalTfa","TerminalTfa") %>', timeout: 600000 }),
            paramNames: {
                sort: "detalhesPaginacaoWeb.Sort",
                dir: "detalhesPaginacaoWeb.Dir",
                start: "detalhesPaginacaoWeb.Start",
                limit: "detalhesPaginacaoWeb.Limit"
            },
            fields: ['Id', 'Email']
        });


        var terminalTfaStore = new Ext.data.JsonStore({
            root: "Items",
            autoLoad: true,
            proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("ObterListaTerminalTfa", "TerminalTfa") %>', timeout: 600000 }),
            id: 'grupoEmpresaStore',
            fields: ['IdTerminal', 'DescricaoTerminal'],
            listeners: {
                load: function (store, records) {
                    var rt = store.recordType;
                    store.insert(0, new rt({}));
                    ddlTerminalTfa.setValue(null);
                }
            }
        });



        //*****JANELA MODAL PARA INCLUIR EMAILS*****

        function MostrarFormCadastro() {
            windowNovoEmail = null;
            windowNovoEmail = new Ext.Window({
                name: 'formCadastro',
                id: 'formCadastro',
                title: 'Cadastro de e-mail de Terminal',
                expandonShow: true,
                modal: true,
                width: 300,
                height: 150,
                closable: true,
                autoLoad: {
                    url: '<%= Url.Action("NovoEmailTerminal", "TerminalTfa") %>',
                    params: {
                        idTerminal: ddlTerminalTfa.getValue()
                    },
                    scripts: true
                }
            });
            windowNovoEmail.show();
        }



        //****CAMPOS*****
        var ddlTerminalTfa = new Ext.form.ComboBox({
            editable: false,
            typeAhead: true,
            forceSelection: true,
            disableKeyFilter: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            valueField: 'IdTerminal',
            displayField: 'DescricaoTerminal',
            fieldLabel: 'Estação - Terminal',
            id: 'ddlTerminalTfa',
            name: 'ddlTerminalTfa',
            width: 245,
            tpl: '<tpl for="."><div class="x-combo-list-item">{DescricaoTerminal}&nbsp;</div></tpl>',
            store: terminalTfaStore,
            value: '',
            listeners: {
                select: {
                    fn: function () {
                        IdTerminal = this.getValue();
                        if (this.getValue()) {
                            Ext.getCmp("btnNovo").setDisabled(false);
                            grid.getStore().load();
                        }
                        else {
                            limparCampos();
                        }
                    }
                }
            }
        });



        var painelFiltros = new Ext.form.FormPanel({
            id: 'painelFiltro',
            title: 'Filtros de Pesquisa',
            name: 'filtros',
            width: "100%",
            region: 'center',
            bodyStyle: 'padding: 10px',
            standardSubmit: true,
            url: '<%= Url.Action("ObterTerminalTfa","TerminalTfa") %>',
            initEl: function (el) {
                this.el = Ext.get(el);
                this.id = this.el.id || Ext.id();
                if (this.standardSubmit) {
                    this.el.dom.action = this.url;
                } else {
                    this.el.on('submit', this.onSubmit, this);
                }
                this.el.addClass('x-form');
            },
            layout: 'column',
            labelAlign: 'top',
            align: 'left',
            border: false,
            items: [{
                width: 800,
                border: false,
                layout: 'column',
                align: 'left',
                items: [{
                    width: 250,
                    layout: 'form',
                    border: false,
                    items: [{
                        border: false,
                        layout: 'form',
                        items: [ddlTerminalTfa]
                    }]
                }
            ]
            }]
        });


        //***** GRID *****//
        var botaoGridExcluir = new Ext.ux.grid.RowActions({
            dataIndex: '',
            header: '',
            align: 'center',
            actions: [{
                iconCls: 'icon-del',
                tooltip: 'Deletar Registro'
            }],
            callbacks: {
                'icon-del': function (grid, record, action, row, col) {
                    if (Ext.Msg.confirm("Exclusão", "Deseja excluir o e-mail " + grid.getStore().getAt(row).get('Email') + "?", function (btn, text) {
                        if (btn == 'yes') {
                            grid.setDisabled(true);
                            id = grid.getStore().getAt(row).get('Id');
                            Ext.Ajax.request({
                                url: '<%= Url.Action("ExcluirTerminalTfaEmail","TerminalTfa") %>',
                                params: {
                                    id: id
                                },
                                success: function (response) {
                                    var result = Ext.decode(response.responseText);

                                    grid.setDisabled(false);
                                    grid.getStore().load();
                                    alerta(result.Message);

                                },
                                erro: function () {
                                    grid.setDisabled(false);
                                }
                            });
                        }
                    }));
                }
            }
        });

        
        // Toolbar da Grid
        var pagingToolbar = new Ext.PagingToolbar({
            pageSize: 50,
            store: gridStore,
            displayInfo: true,
            displayMsg: App.Resources.Web.GridExibindoRegistros,
            emptyMsg: App.Resources.Web.GridSemRegistros,
            paramNames: {
                start: "detalhesPaginacaoWeb.Start",
                limit: "detalhesPaginacaoWeb.Limit"
            }
        });




        var grid = new Ext.grid.EditorGridPanel({
            id: 'grid',
            name: 'grid',
            autoLoadGrid: false,
            height: 450,
            limit: 10,
            //width: 870,
            stripeRows: true,
            region: 'center',
            viewConfig: {
                forceFit: true,
                emptyText: 'Não possui dado(s) para exibição.'
            },
            plugins: [botaoGridExcluir],
            autoScroll: true,
            loadMask: { msg: App.Resources.Web.Carregando },
            store: gridStore,
            tbar: [
                    {
                        xtype: 'button',
                        name: 'btnNovo',
                        id: 'btnNovo',
                        text: 'Novo e-mail',
                        iconCls: 'icon-new',
                        handler: function () {
                            MostrarFormCadastro();
                        }
                    }
                ],
            bbar: pagingToolbar,
            columns: [new Ext.grid.RowNumberer(),
                        botaoGridExcluir,
                        {
                            header: 'Email',
                            width: 100,
                            autoExpandColumn: true,
                            dataIndex: "Email",
                            sortable: true
                        }
                    ],
            filteringToolbar: [{
                xtype: 'button',
                name: 'btnNovo',
                id: 'btnNovo',
                text: 'Novo e-mail',
                iconCls: 'icon-new',
                handler: function () {
                    MostrarFormCadastro();
                }
            }]

        });

       
        var painelGrid = new Ext.form.FormPanel({
            id: 'painelGrid',
            layout: 'form',
            labelAlign: 'top',
            border: false,
            autoHeight: true,
            title: "",
            region: 'center',
            bodyStyle: 'padding: 0px 10px 0px 10px',
            items: [grid]
        });


        //***** FUNÇÕES *****//
        function alerta(msg) {
            Ext.Msg.show({
                title: "Aviso",
                msg: msg,
                buttons: Ext.Msg.OK,
                icon: Ext.MessageBox.QUESTION,
                minWidth: 200
            });
        }

        function limparCampos() {
            idTerminal = null;
            Ext.getCmp("btnNovo").setDisabled(true);
            grid.getStore().removeAll();
            grid.getStore().totalLength = 0;
            grid.getBottomToolbar().bind(grid.getStore());
            grid.getBottomToolbar().updateInfo();
            //Ext.getCmp("ddlTerminalTfa").selectByValue(terminalTfaStore.getAt(0));
        }


        //***** inicializa tela ******
        Ext.onReady(function () {
            painelFiltros.render("divFiltros");
            painelGrid.render("divGrid");
            //Ext.getCmp("ddlTerminalTfa").render();
            limparCampos();
        });

        grid.store.proxy.on('beforeload', function (p, params) {
            params['idTerminal'] = ddlTerminalTfa.getValue();
        });

    </script>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">    
    <div id="header-content">
        <h1>
            Seguro</h1>
        <small>Você está em Cadastro > TFA > E-mails de Terminais</small>
        <br />
    </div>
    <div id="divFiltros">
    </div>
     <div id="divGrid">
    </div>
</asp:Content>
