﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%@ Import Namespace="Translogic.Modules.Core.Domain.Model.Tfa" %>
<div id="div-form-novoGrupoEmpresaTfa">
</div>
<script type="text/javascript">

    var idGrupoEmpresa = '<%=ViewData["idGrupoEmpresa"] %>';
    var nomeGrupo = '<%=ViewData["nomeGrupo"] %>';



    var fieldNomeGrupo = {
        xtype: 'textfield',
        name: 'txtNome',
        id: 'txtNome',
        fieldLabel: 'Nome do Grupo de Empresas',
        maxLength: 30,
        autoCreate: { 
            tag: "input",
            maxlength: 30,
            type: "text",
            size: "30",
            autocomplete: "off"
        },
        style: 'text-transform:uppercase;',
        width: 200,
        listeners:
        {
            specialKey: function (field, e) {
                if (e.getKey() == e.ENTER) {
                    Salvar();
                }
            }
        }
    };

    var colNomeGrupo = {
        width: 300,
        layout: 'form',
        border: false,
        items: [fieldNomeGrupo]
    };


    var arrlinha1 = {
        layout: 'column',
        border: false,
        items: [colNomeGrupo]
    };


    var arrCampos = new Array();
    arrCampos.push(arrlinha1);

    var fields = new Ext.form.FormPanel({
        id: 'fields',
        title: "",
        region: 'center',
        bodyStyle: 'padding: 15px',
        labelAlign: 'top',
        items: [arrCampos],
        buttonAlign: "center",
        buttons:
	            [
                 {
                     text: 'Salvar',
                     name: 'btnSalvar',
                     id: 'btnSalvar',
                     type: 'submit',
                     iconCls: 'icon-save',
                     handler: function (b, e) {
                         Salvar();
                     }
                 },
                 {
                     text: 'Cancelar',
                     handler: function (b, e) {
                         windowNovoGrupoEmpresaTfa.close();
                     },
                     scope: this
                 }
                ]
    });

    function Salvar() {
        var fieldNomeGrupo = Ext.getCmp("txtNome").getValue().trim().toUpperCase();

        if (fieldNomeGrupo == '') {
            Ext.Msg.show({
                title: "Aviso",
                msg: "Informe o Nome do Grupo!",
                buttons: Ext.Msg.OK,
                icon: Ext.MessageBox.INFO,
                minWidth: 200
            });
            
            return;
        }
        if (idGrupoEmpresa == 0) {
            Ext.Ajax.request({
                url: '<%= Url.Action("IncluirGrupoEmpresa", "GrupoEmpresaTfa") %>',
                success: function (response) {
                    var result = Ext.decode(response.responseText);
                    if (result.Success) {

                        Ext.Msg.alert('Informação', "Cadastro efetuado com sucesso!");
                        windowNovoGrupoEmpresaTfa.close();
                        //grid.getStore().load();
                        Pesquisar();
                    }
                    else {

                        Ext.Msg.alert('Aviso', result.Message);
                        Ext.getCmp("txtNome").focus();
                    }
                },
                failure: function (conn, data) {
                    Ext.Msg.alert('Erro no cadastro do Grupo de Empresas!');
                },
                method: "POST",
                params: { nomeGrupo: fieldNomeGrupo }
            });
        }
        else {
            Ext.Ajax.request({
                url: '<%= Url.Action("AlterarGrupoEmpresa", "GrupoEmpresaTfa") %>',
                success: function (response) {
                    var result = Ext.decode(response.responseText);
                    if (result.Success) {

                        Ext.Msg.alert('Informação', "Alteração realizada com sucesso!");
                        windowNovoGrupoEmpresaTfa.close();
                        //grid.getStore().reload();
                        Pesquisar();
                    }
                    else {

                        Ext.Msg.alert('Aviso', result.Message);
                        Ext.getCmp("txtNome").focus();
                    }
                },
                failure: function (conn, data) {
                    Ext.Msg.alert('Erro na edição do Grupo de Empresas!');
                },
                method: "POST",
                params: { idGrupoEmpresa: idGrupoEmpresa, nomeGrupo: fieldNomeGrupo }
            });
        }
    }

    

    function limparCampos() {
        Ext.getCmp("txtNome").setValue('');
    }

   
    function alerta(msg) {
        Ext.Msg.show({
            title: "Aviso",
            msg: msg,
            buttons: Ext.Msg.OK,
            icon: Ext.MessageBox.QUESTION,
            minWidth: 200
        });
    }

    
    fields.render("div-form-novoGrupoEmpresaTfa");

    $(function () {
        limparCampos();
        Ext.getCmp("txtNome").setValue(nomeGrupo);
        Ext.getCmp("txtNome").focus();
    });

</script>

