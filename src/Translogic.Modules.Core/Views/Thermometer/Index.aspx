﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Interna.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content runat="server" ID="HeadContent" ContentPlaceHolderID="HeadContent">
    
    <!--[if IE]><script language="javascript" type="text/javascript" src="/GetFile.efh?file={Translogic.Core.Infrastructure.Web;}/Content/Javascript/jqPlot/excanvas.min.js"> </script><![endif]-->
    <script src="/GetFile.efh?file={Translogic.Core.Infrastructure.Web;}/Content/Javascript/jQuery/jquery1.9.1.min.js"> </script>
   
    <% using (var combiner = new ResourceCombiner("termometro"))
       {
           // CSS
           combiner.Add(Url.CSS("jquery.jqplot.min.css"));
           combiner.Add(Url.Javascript("jqPlot/jquery.jqplot.min.js"));
           combiner.Add(Url.Javascript("jqPlot/jqplot.dateAxisRenderer.min.js"));
           combiner.Add(Url.Javascript("jqPlot/jqplot.canvasAxisTickRenderer.min.js"));
           combiner.Add(Url.Javascript("jqPlot/jqplot.canvasTextRenderer.min.js"));
           combiner.Add(Url.Javascript("jqPlot/jqplot.cursor.min.js"));
           combiner.Add(Url.Javascript("jqPlot/jqplot.highlighter.min.js"));
       } %>
    
    <style type="text/css">
        table.jqplot-table-legend, table.jqplot-cursor-legend {
            font-size: 8pt;
        }
        
        #ext-comp-1005 {
            margin-top: 14px;
        }
        
         #ext-comp-1006 {
            margin-top: 14px;
        }
        
        #ext-comp-1008 {
            margin-top: 14px;
        }
    </style>
    

    <script type="text/javascript">

        var plot1;
        var $jq191 = jQuery.noConflict(true);

        function ValidarPesquisa() {
            var executarPesquisa = true;
            var diferenca = Ext.getCmp("filtro-data-final").getValue() - Ext.getCmp("filtro-data-inicial").getValue();
            diferenca = Math.floor(diferenca / (1000 * 60 * 60 * 24));

            if (Ext.getCmp("filtro-data-inicial").getValue() == '' || Ext.getCmp("filtro-data-final").getValue() == '') {
                executarPesquisa = false;
                Ext.Msg.show({
                    title: "Mensagem de Informação",
                    msg: "Preencha os filtro de datas!",
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.INFO,
                    minWidth: 200
                });
            } else if (diferenca > 30) {
                executarPesquisa = false;
                Ext.Msg.show({
                    title: "Mensagem de Informação",
                    msg: "O período da pesquisa não deve ultrapassar 30 dias",
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.INFO,
                    minWidth: 200
                });
            } else if (Ext.getCmp("cboTermometro").getValue() == '') {
                executarPesquisa = false;
                Ext.Msg.show({
                    title: "Mensagem de Informação",
                    msg: "Selecione o Termômetro!",
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.INFO,
                    minWidth: 200
                });
            }

            return executarPesquisa;
        }

        function ExportarRelatorio() {

            if (!ValidarPesquisa()) {
                return false;
            }

            var idTermometro = Ext.getCmp("cboTermometro").getValue();
            var dataInicial = Ext.getCmp("filtro-data-inicial").getValue().format('Y/m/d') + 'T' + Ext.getCmp("filtro-data-inicial").getValue().format('H:i:s');
            var dataFinal = Ext.getCmp("filtro-data-final").getValue().format('Y/m/d') + 'T' + Ext.getCmp("filtro-data-final").getValue().format('H:i:s');

            var url = "<%= Url.Action("ExportarLeiturasTermometro") %>";

            url += "?idTermometro=" + idTermometro;
            url += "&dataInicial=" + dataInicial;
            url += "&dataFinal=" + dataFinal;

            window.open(url, "");
        }

        function CarregarGrafico() {

            if (!ValidarPesquisa()) {
                return false;
            }

            var idTermometro = Ext.getCmp("cboTermometro").getValue();
            var dataInicial = Ext.getCmp("filtro-data-inicial").getValue().format('Y/m/d') + 'T' + Ext.getCmp("filtro-data-inicial").getValue().format('H:i:s');
            var dataFinal = Ext.getCmp("filtro-data-final").getValue().format('Y/m/d') + 'T' + Ext.getCmp("filtro-data-final").getValue().format('H:i:s');

            $.ajax({
                url: "<%= Url.Action("ObterLeiturasTermometro") %>",
                type: "POST",
                beforeSend: function() { Ext.getCmp("grid-filtros").getEl().mask("Obtendo medições.", "x-mask-loading"); },
                async: true,
                dataType: 'json',
                data: ({ idTermometro: idTermometro, dataInicial: dataInicial, dataFinal: dataFinal }),
                success: function(result) {
                    Ext.getCmp("grid-filtros").getEl().unmask();

                    if (result.Erro) {
                        Ext.Msg.alert('Mensagem de Erro', result.Mensagem);
                    } else {

                        var arr = new Array();
                        var arr2 = new Array();
                        var arr3 = new Array();
                        var arr4 = new Array();
                        var arr5 = new Array();
                        var arr6 = new Array();
                        var arr7 = new Array();

                        if (result.Items.length == 0) {
                            $('#chart').empty();
                            Ext.Msg.alert('Mensagem', 'A consulta não retornou registros!');
                            return false;
                        }

                        for (var i = 0; i < result.Items.length; i++) {
                            var date = result.Items[i].DataLeitura;
                            var value = result.Items[i].TemperaturaTermometro;
                            arr.push([date, value]);
                            arr2.push([date, result.MinimaRonda]);
                            arr3.push([date, result.MinimaRestricao]);
                            arr4.push([date, result.MaximaRonda]);
                            arr5.push([date, result.MaximaRestricao]);
                            arr6.push([date, result.MinimaInterdicao]);
                            arr7.push([date, result.MaximaInterdicao]);
                        }

                        $jq191.jqplot.config.enablePlugins = true;

                        // for 2 digit years, set the default centry to 2000.
                        $jq191.jsDate.config.defaultCentury = 2000;

                        plot1 = $jq191.jqplot('chart', [arr, arr2, arr3, arr4, arr5, arr6, arr7], {
                            title: Ext.getCmp("cboTermometro").getRawValue(),
                            animate: true,
                            animateReplot: true,
                            legend: {
                                show: true,
                                placement: 'outside',
                                fontSize: 'larger'
                            },
                            series: [
                                {
                                    showMarker: false,
                                    lineWidth: 2,
                                    showLabel: false
                                },
                                {
                                    show: result.MinimaRonda != null ? true: false,
                                    showMarker: false,
                                    lineWidth: 1,
                                    color: 'red',
                                    label: 'RONDA'
                                },
                                {
                                    show: result.MinimaRestricao != null ? true: false,
                                    showMarker: false,
                                    lineWidth: 1,
                                    color: 'yellow',
                                    label: 'RESTRIÇÃO'
                                },
                                {
                                    show: result.MaximaRonda != null ? true: false,
                                    showMarker: false,
                                    lineWidth: 1,
                                    color: 'red',
                                    showLabel: result.MinimaRonda == null && result.MaximaRonda != null ? true : false,
                                    label: 'RONDA'
                                },
                                {
                                    show: result.MaximaRestricao != null ? true: false,
                                    showMarker: false,
                                    lineWidth: 1,
                                    color: 'yellow',
                                    showLabel: result.MinimaRestricao == null && result.MaximaRestricao != null ? true : false,
                                    label: 'RESTRIÇÃO'
                                    
                                },
                                {
                                    show: result.MinimaInterdicao != null ? true: false,
                                    showMarker: false,
                                    lineWidth: 1,
                                    color: 'black',
                                    label: 'INTERDIÇÃO'
                                },
                                {
                                    show: result.MaximaInterdicao != null ? true: false,
                                    showMarker: false,
                                    showLabel: result.MinimaInterdicao == null && result.MaximaInterdicao != null ? true : false,
                                    label: 'INTERDIÇÃO',
                                    lineWidth: 1,
                                    color: 'black'
                                    
                                },
                                {
                                    rendererOptions: {
                                        animation: {
                                            speed: 2000
                                        }
                                    }
                                }
                            ],
                            axes: {
                                xaxis: {
                                    tickRenderer: $jq191.jqplot.CanvasAxisTickRenderer,
                                    renderer: $jq191.jqplot.DateAxisRenderer,
                                    tickOptions: {
                                        angle: -90,
                                        fontSize: '11pt',
                                        formatString: '%d/%m - %H:%M',
                                        textColor: 'black'
                                    }
                                },
                                yaxis: {
                                    tickOptions: {
                                        formatString: '%d'
                                    },
                                    ticks: [0, 10, 20, 30, 40, 50, 60, 70]
                                }
                            },
                            highlighter: {
                                show: false
                            },
                            cursor: {
                                show: true,
                                tooltipAxesGroups: ['xaxis', 'yaxis'],
                                zoom: true
                            }
                        });
                        plot1.redraw();
                    }
                },
                failure: function(result) {
                    Ext.Msg.alert('Erro ao obter medições.', result.Message);
                    Ext.getCmp("grid-filtros").getEl().unmask();
                }
            });
        }

        var arrDetalhesFormulario = new Array();
        var dataAtual = new Date();

        var storeTermometros = new Ext.data.JsonStore({
            url: '<%= Url.Action("ObterTermometros") %>',
            root: "Items",
            autoLoad: true,
            fields: [
                { name: 'Id' },
                { name: 'NomeTermometro' }
            ]
        });

        var label = {
            xtype: 'label',
            html: "<div id='chart' style='height:410px;width:92%'></div>"
        };

        var fsChart = {
            xtype: 'fieldset',
            id: 'fsChart',
            name: 'fsChart',
            autoHeight: true,
            //bodyStyle: 'padding: 5px',
            width: '100%',
            height: '400px',
            title: 'Gráfico',
            items: [label]
        };

        var cboTermometro = {
            xtype: 'combo',
            store: storeTermometros,
            allowBlank: true,
            lazyInit: false,
            lazyRender: false,
            mode: 'local',
            typeAhead: false,
            triggerAction: 'all',
            fieldLabel: 'Termômetro',
            name: 'cboTermometro',
            id: 'cboTermometro',
            hiddenName: 'cboTermometro',
            displayField: 'NomeTermometro',
            forceSelection: true,
            width: 200,
            valueField: 'Id',
            emptyText: 'Selecione...',
            editable: false
        };

        var dataInicial = {
            xtype: 'xdatetime',
            id: 'filtro-data-inicial',
            fieldLabel: 'Data início',
            anchor: '-18',
            hiddenFormat: 'c',
            timeFormat: 'H:i:s',
            timeConfig: {
                altFormats: 'H:i:s',
                allowBlank: true,
                plugins: [new Ext.ux.InputTextMask('99:99:99', true)]
            },
            dateFormat: 'd/n/Y',
            dateConfig: {
                altFormats: 'Y-m-d|Y-n-d',
                allowBlank: true,
                plugins: [new Ext.ux.InputTextMask('99/99/9999', true)]
            },
            name: 'filtro-data-inicial'
        };

        var dataFinal = {
            xtype: 'xdatetime',
            id: 'filtro-data-final',
            fieldLabel: 'Data Fim',
            anchor: '-18',
            hiddenFormat: 'c',
            timeFormat: 'H:i:s',
            timeConfig: {
                altFormats: 'H:i:s',
                allowBlank: true,
                plugins: [new Ext.ux.InputTextMask('99:99:99', true)]
            },
            dateFormat: 'd/n/Y',
            dateConfig: {
                altFormats: 'Y-m-d|Y-n-d',
                allowBlank: true,
                plugins: [new Ext.ux.InputTextMask('99/99/9999', true)]
            },
            name: 'filtro-data-final'
        };

        var botaoCarregarGrafico = {
            xtype: 'button',
            iconCls: 'icon-find',
            id: 'botaoCarregarGrafico',
            text: 'Carregar',
            handler: function() {
                CarregarGrafico();
            }
        };
        
        var botaoExportarRelatorio = {
            text: 'Exportar',
            xtype: 'button',
            iconCls: 'icon-page-excel',
            handler: function() {
                ExportarRelatorio();
            }
        };
        
        var botaoLimparFormulario = {
            xtype: 'button',
            text: 'Limpar',
            width: 70,
            handler: function() {
                $('#chart').empty();
                Ext.getCmp("grid-filtros").getForm().reset();
            }
        };

        var linha1_coluna1 = { width: 220, layout: 'form', border: false, items: [cboTermometro] };
        var linha1_coluna2 = { width: 210, layout: 'form', border: false, items: [dataInicial] };
        var linha1_coluna3 = { width: 210, layout: 'form', border: false, items: [dataFinal] };
        var linha1_coluna4 = { width: 80, layout: 'form', border: false, items: [botaoCarregarGrafico] };
        var linha1_coluna5 = { width: 80, layout: 'form', border: false, items: [botaoExportarRelatorio] };
        var linha1_coluna6 = { width: 85, layout: 'form', border: false, items: [botaoLimparFormulario] };
        
        var linha1 = {
            layout: 'column',
            border: false,
            items: [linha1_coluna1, linha1_coluna2, linha1_coluna3, linha1_coluna4, linha1_coluna5,linha1_coluna6]
        };

        arrDetalhesFormulario.push(linha1);

        var filters = new Ext.form.FormPanel({
            id: 'grid-filtros',
            title: "Filtros",
            region: 'center',
            bodyStyle: 'padding: 15px',
            labelAlign: 'top',
            items:
                [arrDetalhesFormulario, fsChart]
        });

        $(function() {

            var view = new Ext.Viewport({
                margins: 10,
                items: [
                    {
                        region: 'north',
                        height: 1000,
                        items: [{
                                region: 'center',
                                applyTo: 'header-content'
                            },
                            filters]
                    }
                ]
            });
        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div id="header-content">
        <h1>
            Tela de Termômetro</h1>
        <small>Você está em Termometro > Painel Termômetro</small>
    </div>
</asp:Content>