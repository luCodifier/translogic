﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div id="div-form-add-conf">
</div>
<script type="text/javascript">

    /* region :: Functions */

    function Confirmar() {
        var chave = Ext.getCmp("txtChave").getValue();
        if (chave == "") {
            alert("Obrigatório preenchimento do campo Chave CT-e para Fluxo com tipo de serviço diferente de normal");
            return;
        }

        Ext.getCmp("modalChaveCte").close();

        var cte  = Ext.getCmp("cpHiddenCte").getValue();

        if (telaOrigem == 'Index') {
            PodeReenviar(cte, chave);

        } else {
            SalvarAlteracoesCte('yes', chave);
        }
        
        
    }

    function Cancelar() {
        Ext.getCmp("modalChaveCte").close();

    }

    /* endregion :: Functions */

    var txtChave = {
        xtype: 'textfield',
        name: 'txtChave',
        id: 'txtChave',
        fieldLabel: 'Chave CT-e',
        autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '44' },
        style: 'text-transform: uppercase',
        value: '<%= ViewData["ChaveCteOrigem"] %>',
        width: 300
    };

    var cpHiddenCte = {
        name: 'cpHiddenCte',
        id: 'cpHiddenCte',
        xtype: 'hidden',
        value: '<%= ViewData["CTE"] %>'
    };
    

    /* region :: Modal */

    var modal = new Ext.form.FormPanel({
        id: 'form-add-conf-modal',
        labelWidth: 50,
        width: 400,
        resizable: false,
        bodyStyle: 'padding: 15px',
        items: [
            txtChave, cpHiddenCte
        ],
        buttonAlign: "center",
        buttons: [
            {
                text: "Confirmar",
                id: "btnConfirmar",
                iconCls: 'icon-save',
                handler: Confirmar
            }
                    ,
                    {
                        text: "Cancelar",
                        id: "btnCancelar",
                        iconCls: 'icon-cancel',
                        handler: Cancelar
                    }
        ]
    });

    /* endregion :: Modal*/
    modal.render("div-form-add-conf");
</script>
