﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%@ Import Namespace="System.Globalization" %>
<div id="div-grid-nota-fiscal"></div>
<script type="text/javascript">
    function InserirLinhaNfVazia(){
        var ChaveNfe = "";
        var Conteiner = "";
        var SerieNotaFiscal = "";
        var NumeroNotaFiscal = "";
        var PesoTotal = null;
        var PesoRateio = null;
        var ValorNotaFiscal = 0;
        var ValorTotalNotaFiscal = 0;
        var Remetente = "";
        var Destinatario = "";
        var CnpjRemetente = "";
        var CnpjDestinatario = "";
        var TIF = "";
        var DataNotaFiscal = "";
        var SiglaRemetente = "";
        var SiglaDestinatario = "";
        var UfRemetente = "";
        var UfDestinatario = "";
        var InscricaoEstadualRemetente = "";
        var InscricaoEstadualDestinatario = "";
        var pesoDclVagao = null;
        var multiploDespacho = "";
        var volumeNotaFiscal = null;

        InsereLinhaNotaFiscal(ChaveNfe, Conteiner, SerieNotaFiscal, NumeroNotaFiscal, PesoTotal, PesoRateio, ValorNotaFiscal, ValorTotalNotaFiscal, Remetente, Destinatario, CnpjRemetente, CnpjDestinatario, TIF, DataNotaFiscal, SiglaRemetente, SiglaDestinatario, UfRemetente, UfDestinatario, InscricaoEstadualRemetente, InscricaoEstadualDestinatario, pesoDclVagao, multiploDespacho, volumeNotaFiscal);
    }

    function InsereLinhaNotaFiscal(ChaveNfe, Conteiner, SerieNotaFiscal, NumeroNotaFiscal, PesoTotal, PesoRateio, ValorNotaFiscal, ValorTotalNotaFiscal, Remetente, Destinatario, CnpjRemetente, CnpjDestinatario, TIF, DataNotaFiscal, SiglaRemetente, SiglaDestinatario, UfRemetente, UfDestinatario, InscricaoEstadualRemetente, InscricaoEstadualDestinatario, pesoDclVagao, multiploDespacho, volumeNotaFiscal) 
    {
        var notaFiscalType = gridNotasFiscais.getStore().recordType;
        var notaFiscal = new notaFiscalType({
            ChaveNfe: ChaveNfe,
            Conteiner: Conteiner,
            SerieNotaFiscal: SerieNotaFiscal,
            NumeroNotaFiscal: NumeroNotaFiscal,
            PesoTotal: PesoTotal,
            PesoRateio: PesoRateio,
            ValorNotaFiscal: ValorNotaFiscal,
            ValorTotalNotaFiscal: ValorTotalNotaFiscal,
            Remetente: Remetente,
            Destinatario: Destinatario,
            CnpjRemetente: CnpjRemetente,
            CnpjDestinatario: CnpjDestinatario,
            TIF: TIF,
            DataNotaFiscal: DataNotaFiscal,
            SiglaRemetente: SiglaRemetente,
            SiglaDestinatario: SiglaDestinatario,
            UfRemetente: UfRemetente,
            UfDestinatario: UfDestinatario,
            InscricaoEstadualRemetente: InscricaoEstadualRemetente,
            InscricaoEstadualDestinatario: InscricaoEstadualDestinatario,
            IndObtidoAutomatico: false,
            IndMultiploDespacho: multiploDespacho,
            PesoDclVagao: pesoDclVagao,
            VolumeNotaFiscal: volumeNotaFiscal
        });

        gridNotasFiscais.stopEditing();
        gridNotasFiscais.getStore().add(notaFiscal);
        var total = gridNotasFiscais.getStore().getCount();
        gridNotasFiscais.startEditing(total - 1, 1); //(total - 1) devido ao indice da grid
    }

    function RemoverLinhasVazias() {
        gridNotasFiscais.getStore().each(
			function (record) {
			    if (record.data.ChaveNfe == ''
                    && record.data.SerieNotaFiscal == ''
                    && record.data.NumeroNotaFiscal == ''
                    && record.data.PesoTotal == null
                    && record.data.PesoRateio == null
                    && record.data.ValorNotaFiscal == 0) {

			        gridNotasFiscais.getStore().remove(record);

			    }
			}
		);
    }

    function SalvarNotas() {
        RemoverLinhasVazias();
        var valido = ValidarDadosNotasVagao();
        if (valido)
        {
            RemoverNotasVagao();
            var tara = null;
            var volume = null;
            var pesoDclVagao = Ext.getCmp("pesoDclVagao").getValue();
            if (indPreenchimentoTara) tara = Ext.getCmp("taraVagao").getValue();
            if (indPreenchimentoVolume) volume = Ext.getCmp("volumeVagao").getValue();
            
            gridNotasFiscais.getStore().each(
			    function (record) {

			        ImportarRegistroParaTelaPrincipal(record, tara, volume, pesoDclVagao);
			    }
		    );
		    
            Ext.getCmp("btnSalvar").enable();
            windowNotasFiscais.close();
        }else{
            var mensagemErro = "";
            Ext.getCmp("formNotasFiscais").getForm().items.each(function(f){
               if(f.activeError!= undefined){
                   mensagemErro += " - " + f.fieldLabel + ": " + f.activeError + "<br />";
               }
            });

            if (mensagemErro != "")
            {
                Ext.Msg.alert('Erro de validação', "Foram encontrados os erros abaixo:<br />" + mensagemErro);
            }
        }
    }

    function ValidarDadosNotasVagao(){
        if (!Ext.getCmp("formNotasFiscais").getForm().isValid()) {
            return false;
        }

        if (parseFloat(Ext.getCmp("pesoDclVagao").getValue()) <= 0 || parseFloat(Ext.getCmp("pesoDclVagao").getValue()) >= 120){
            Ext.getCmp("pesoDclVagao").markInvalid("Peso inválido.");
            return false;
        }

        if (indPreenchimentoVolume)
        {
            if (parseFloat(Ext.getCmp("volumeVagao").getValue()) <= 0 || parseFloat(Ext.getCmp("volumeVagao").getValue()) > 120){
                Ext.getCmp("volumeVagao").markInvalid("Volume do vagão inválido.");
                return false;
            }
        }

        if (indPreenchimentoTara && indTaraObrigatorio){
            if (Ext.getCmp("taraVagao").getValue() <= 0 || Ext.getCmp("taraVagao").getValue() > 120){
                Ext.getCmp("taraVagao").markInvalid("Tara do vagão inválida.");
                return false;
            }
        }

        var mensagemErro = "";
        var contadorNotas = 0;
        var contadorIteracoes = 0;
        var contadorConteiners = 0;
        var somaPesoNotas = 0;
        var somaVolumeNotas = 0;    
        var contadorNotasManuais = 0;
        var contadorNotasEletronicas = 0;
        gridNotasFiscais.getStore().each(
			function (record) {
			    if (record.data.ChaveNfe != "" && record.data.ChaveNfe != null && record.data.ChaveNfe != undefined) {
			        contadorNotasEletronicas++;
			    } else {
			        contadorNotasManuais++;
			    }

			    if (record.data.SerieNotaFiscal != "" && record.data.NumeroNotaFiscal != "") {
			        contadorNotas++;

			        var retorno = ValidaRegistro(record);
			        if (retorno != null) {
			            for (var i = 0; i < retorno.length; i++) {
			                Ext.fly(gridNotasFiscais.getView().getCell(contadorIteracoes, retorno[i])).setStyle({ "background-color": "#E5B9B7" });
			                mensagemErro = mensagemErro + "Erro3";
			            }
			        }

			        if (record.data.Conteiner != null && record.data.Conteiner != "") {
			            contadorConteiners++;
			        }

			        if (!isNaN(record.data.PesoRateio)) {
			            var pesoRateioTmp = RoundWithDecimals(parseFloat(record.data.PesoRateio), 3);
			            somaPesoNotas += pesoRateioTmp;
			            // alert(pesoRateioTmp);
			        }

			        if (!isNaN(record.data.VolumeNotaFiscal)) {
			            somaVolumeNotas += RoundWithDecimals(parseFloat(record.data.VolumeNotaFiscal), 3);
			        }

			        contadorIteracoes++;
			    }
			}
		);

        if (mensagemErro != ""){
            Ext.Msg.alert('Erro de validação', 'Os campos marcados em vermelho estão com valores inválidos, corrija antes de continuar.');
            return false;
        }

        if (contadorConteiners > 0 && contadorConteiners < contadorNotas){
            Ext.Msg.alert('Erro de validação', 'Necessário preencher os conteiners de todas notas quando houver uma nota com conteiner.');
            return false;
           }

        if (indVerificarConteiner && contadorConteiners < contadorNotas) {
            Ext.Msg.alert('Erro de validação', 'Necessário preencher os conteiners de todas notas.');
            return false;
        }

        if (contadorNotas == 0){
            Ext.Msg.alert('Erro de validação', 'Necessário inserir pelo menos uma nota para o vagão.');
            return false;
        }

        if (indPreenchimentoVolume){
            if (RoundWithDecimals(parseFloat(Ext.getCmp("volumeVagao").getValue()), 3) != RoundWithDecimals(parseFloat(somaVolumeNotas),3)){
                Ext.Msg.alert('Erro de validação', 'A soma dos volumes das notas é diferente do volume do vagão informado.');
                Ext.getCmp("volumeVagao").markInvalid('A soma dos volumes das notas é diferente do volume do vagão informado.');
                return false;
            }
        }else{

            // alert("peso dcl = " + Ext.getCmp("pesoDclVagao").getValue() + " | soma = " + somaPesoNotas + " | margem = " + percentualTolerancial + " | soma+percentual = " + somaPesoNotasComPercentual);
            
            if (RoundWithDecimals(parseFloat(Ext.getCmp("pesoDclVagao").getValue()), 3) != RoundWithDecimals(parseFloat(somaPesoNotas), 3))
            {
                Ext.Msg.alert('Erro de validação', 'A soma dos pesos das notas é diferente do peso informado.');
                Ext.getCmp("pesoDclVagao").markInvalid('A soma dos pesos das notas é diferente do peso informado.');
                return false;
            }
        }

        if (contadorNotasManuais > 0 && contadorNotasEletronicas > 0){
            Ext.Msg.alert('Erro de validação', 'Todas as notas do vagão devem ser do mesmo tipo, ou NF-e ou de preenchimento manual.');
            return false;
        }

        return true;
    }

    function ValidaRegistro(rec){
        var erros = new Array();

        if (indPreenchimentoVolume){
            var idxVolumeNotaFsical = 4;
            if (indFluxoInternacional) idxVolumeNotaFsical = idxVolumeNotaFsical+1;

            // O VOLUME DEVE SER PREENCHIDO
            if (rec.data.VolumeNotaFiscal == null || rec.data.VolumeNotaFiscal == "" || rec.data.VolumeNotaFiscal == 0){
                erros.push(idxVolumeNotaFsical);
            }

        }else{
            var idxPeso = 4;
            if (indFluxoInternacional) idxPeso = idxPeso+1;

            // O PESO DO RATEIO DEVE SER PREENCHIDO
            if (rec.data.PesoRateio == null || rec.data.PesoRateio == "" || rec.data.PesoRateio == 0){
                erros.push(idxPeso);
            }
           
            var pesoTotal = RoundWithDecimals(parseFloat(rec.data.PesoTotal), 3);

            // O PESO DO RATEIO DEVE SER MENOR OU IGUAL AO PESO DA NOTA
            if (rec.data.PesoTotal != null && rec.data.PesoTotal > 0) {
                if (pesoTotal < RoundWithDecimals(parseFloat(rec.data.PesoRateio), 3)) {
                    erros.push(idxPeso);
                }
            }
        }

        // QUANDO O FLUXO FOR DE CONTEINER É NECESSÁRIO MARCAR O CAMPO COMO OBRIGATÓRIO
        if (indFluxoConteiner)
        {
            var idxConteiner = 3;
            if (indFluxoInternacional) idxConteiner = idxConteiner+1;

            // O CONTEINER DEVE SER PREENCHIDO
            if (rec.data.Conteiner == null || rec.data.Conteiner == ""){
                erros.push(idxConteiner);
            }
        }


        // QUANDO FOR FLUXO INTERNACIONAL DEVE-SE VERIFICAR O SE O TIF FOI PREENCHIDO
        if (indFluxoInternacional && indFluxoInternacionalDestinoTifObrigatorio){
            var idxTif = 3
            if (rec.data.TIF == null || rec.data.TIF == "" || rec.data.TIF == 0){
                erros.push(idxTif);
            }
        }

        if (erros.length > 0){
            return erros;
        }

        return null;
    }
    
    /**********************************************************
	STORE DE NOTAS FISCAIS - INICIO
	**********************************************************/
	var notasFiscaisStore = new Ext.data.JsonStore({

        fields: [
				'ChaveNfe',
                'Conteiner',
				'SerieNotaFiscal',
				'NumeroNotaFiscal',
				'PesoTotal',
                'PesoRateio',
                'ValorNotaFiscal',
                'ValorTotalNotaFiscal',
                'Remetente',
                'Destinatario',
                'CnpjRemetente',
                'CnpjDestinatario',
                'TIF',
                'SiglaRemetente',
                'SiglaDestinatario',
                'DataNotaFiscal',
				'VolumeNotaFiscal',
                'UfRemetente',
                'UfDestinatario',
                'InscricaoEstadualRemetente',
                'InscricaoEstadualDestinatario',
                'IndObtidoAutomatico',
                'MultiploDespacho',
                'PesoDclVagao',
                'VolumeNotaFiscal'
				]
	});
	
	// notasFiscaisStore.load();
	/**********************************************************
	STORE DE NOTAS FISCAIS - FIM
	**********************************************************/
	
	/**********************************************************
	GRID DE NOTAS FISCAIS - INICIO
	**********************************************************/
	var detalheActionNotasFiscais = new Ext.ux.grid.RowActions({
	    dataIndex: '',
	    header: '',
	    align: 'center',
	    actions: [
            {
                iconCls: 'icon-detail',
                tooltip: 'Detalhes'
            },
            {
                iconCls: 'icon-del',
                tooltip: 'Excluir'
            }],
	    callbacks: {
	        'icon-detail': function (grid, record, action, row, col) {
	            EditarDadosNota(record);
	        },
	        'icon-del': function (grid, record, action, row, col) {
	            if (Ext.Msg.confirm("Excluir", "Deseja realmente excluir esta Nota Fiscal?", function (btn, text) {
	                if (btn == 'yes') {
	                    notasFiscaisStore.remove(record);
	                }
	            }));
	        }
	    }
	});
    
    function getNumberField(){
        return { 
            xtype: 'masktextfield',
            allowblank: false, 
            mask: '990,000',
		    money: true,
            maxLength: 7,
            value: "0,000",
            width: 70
             };
    }

    var arrColunasNf = new Array();
    arrColunasNf.push(new Ext.grid.RowNumberer());
    arrColunasNf.push(detalheActionNotasFiscais);
    // arrColunasNf.push({ header: 'Chave', width: 260, dataIndex: "ChaveNfe", sortable: false, editor: CriarCampoNfe() });
    arrColunasNf.push({ header: 'Chave', width: 260, dataIndex: "ChaveNfe", sortable: false, editor: new Ext.form.TextField({ vtype: 'ctevtype', autoCreate: { tag: 'input', type: 'text', maxlength: '44', autocomplete: 'off' }, allowblank: false }) });

    if (indFluxoInternacional){
        arrColunasNf.push({ header: 'TIF', width: 100, dataIndex: "TIF", sortable: false, editor: new Ext.form.TextField({ allowblank: false }) });
    }

       arrColunasNf.push({ header: 'Conteiner', width: 100, dataIndex: "Conteiner", sortable: false, editor: new Ext.form.TextField({ allowblank: !indFluxoConteiner, autoCreate: { tag: 'input', type: 'text', maxlength: '11', autocomplete: 'off' }, maskRe: /[a-zA-Z0-9]/ }) });

    if (indPreenchimentoVolume){
        arrColunasNf.push({ header: 'Volume', dataIndex: "VolumeNotaFiscal", sortable: false, renderer: VolumeRenderer });
    }else{
        arrColunasNf.push({ header: 'Peso Rateio', dataIndex: "PesoRateio", sortable: false, renderer: PesoRenderer, editor: getNumberField() });
        arrColunasNf.push({ header: 'Peso Total', dataIndex: "PesoTotal", sortable: false, renderer: PesoRenderer });
    }

    
    arrColunasNf.push({ header: 'Série', width: 50, dataIndex: "SerieNotaFiscal", sortable: false });
    arrColunasNf.push({ header: 'Nro.', width: 50, dataIndex: "NumeroNotaFiscal", sortable: false });
    arrColunasNf.push({ header: 'Valor', dataIndex: "ValorTotalNotaFiscal", sortable: false, renderer: MonetarioRenderer });
    arrColunasNf.push({ header: 'Data', dataIndex: "DataNotaFiscal", sortable: false, renderer: DataRenderer });
    arrColunasNf.push({ header: 'Remetente', dataIndex: "Remetente", sortable: false });
    arrColunasNf.push({ header: 'Sigla Rem.', dataIndex: "SiglaRemetente", sortable: false});
    arrColunasNf.push({ header: 'CNPJ Rem.', dataIndex: "CnpjRemetente", sortable: false});
    arrColunasNf.push({ header: 'UF Rem.', dataIndex: "UfRemetente", sortable: false});
    arrColunasNf.push({ header: 'Insc. Est. Rem.', dataIndex: "InscricaoEstadualRemetente", sortable: false});
    arrColunasNf.push({ header: 'Destinatário', dataIndex: "Destinatario", sortable: false });
    arrColunasNf.push({ header: 'Sigla Dest.', dataIndex: "SiglaDestinatario", sortable: false });
    arrColunasNf.push({ header: 'CNPJ Dest.', dataIndex: "CnpjDestinatario", sortable: false });
    arrColunasNf.push({ header: 'UF Dest.', dataIndex: "UfDestinatario", sortable: false });
    arrColunasNf.push({ header: 'Insc. Est. Dest.', dataIndex: "InscricaoEstadualDestinatario", sortable: false });
    

	var gridNotasFiscais = new Ext.grid.EditorGridPanel({
	    id: 'gridNotasFiscais',
	    name: 'gridNotasFiscais',
	    store: notasFiscaisStore,
	    clicksToEdit: 1,
	    columns: arrColunasNf,
	    plugins: [detalheActionNotasFiscais],
	    colModel: new Ext.grid.ColumnModel({
	        defaults: {
	            sortable: false
	        },
	        columns: this.columns
	    }),
	    stripeRows: false,
	    height: 230,
	    width: 600,
	    tbar: [{
	        id: 'btnNovaNF',
            text: 'Novo',
	        tooltip: 'Adicionar Nota Fiscal',
	        iconCls: 'icon-new',
	        handler: function (c) {
                InserirLinhaNfVazia();
	        }
	    }]
	});

    gridNotasFiscais.on('validateedit', afterEditNotaFiscal, this );


    var theSelectionModel = gridNotasFiscais.getSelectionModel();
    theSelectionModel.onEditorKey = function(field, e)
    {
      var k = e.getKey(), newCell, g = theSelectionModel.grid, ed = g.activeEditor;
      if(k == e.TAB){
            e.stopEvent();
            ed.completeEdit();
      }
    };

	/**********************************************************
	GRID DE NOTAS FISCAIS - FIM
	**********************************************************/
	
	/**********************************************************
	FORM DADOS DAS NOTAS FISCAIS - INICIO
	**********************************************************/
    var arrCamposFormVagao = new Array();
    
    var txtPesoDclVagao = 
    {
		xtype: 'masktextfield',
		id: 'pesoDclVagao',
        fieldLabel: 'Peso',
        name: 'pesoDclVagao',
        allowBlank: false,
		mask: '990,000',
		money: true,
        maxLength: 7,
        value: "0,000",
        width: 50
    };

    var colunaPesoDclVagao = { width: 70, layout: 'form', border: false, items: [txtPesoDclVagao] };
    arrCamposFormVagao.push(colunaPesoDclVagao);
    
    if (indPreenchimentoVolume){
        var txtVolumeVagao = {
            xtype: 'masktextfield',
            id: 'volumeVagao',
            fieldLabel: 'Volume',
            name: 'volumeVagao',
            allowBlank: false,
            width: 50,
            mask: '990,000',
		    money: true,
            maxLength: 7,
            enableKeyEvents: true,
            listeners:{
                'blur': function (field) {
                    if (indVerificaPesoPorVolume)
                    {
                        var valorPeso = CalcularPesoPorDensidade(field.getValue());
                        if (valorPeso != null){
                            Ext.getCmp("pesoDclVagao").setValue(valorPeso);
                        }
                    }
                }
            }
        };
        var colunaVolumeVagao = { width: 70, layout: 'form', border: false, items: [txtVolumeVagao] };
        arrCamposFormVagao.push(colunaVolumeVagao);
    }

    if (indPreenchimentoTara){
        var txtTaraVagao = {
            xtype: 'masktextfield',
            id: 'taraVagao',
            fieldLabel: 'Tara',
            name: 'taraVagao',
            allowBlank: false,
            width: 50,
            mask: '990,000',
		    money: true,
            maxLength: 7,
            value: "0,000"
        };
        var colunaTaraVagao = { width: 110, layout: 'form', border: false, items: [txtTaraVagao] };
        arrCamposFormVagao.push(colunaTaraVagao);
    }

    var linha1 = {
        layout: 'column',
        border: false,
        items: arrCamposFormVagao
    };

    var label = {
		xtype: 'label',
		html: '<div>Clique nos campos para editar os valores dos campos: Chave NF-e, Conteiner e Peso Rateio.</div>',
		height: 15
	};

    var linha2 = {
        layout: 'column',
        border: false,
        items: label
    };

	var formNotasFiscais = new Ext.form.FormPanel
	({
        id: 'formNotasFiscais',
		labelWidth: 80,
		width: 630,
		bodyStyle: 'padding: 15px',
        labelAlign: 'top',
		items: [linha1, linha2, gridNotasFiscais],
		buttonAlign: "center",
		buttons: 
		[{
			text: "Confirmar",
			handler: function() {
				SalvarNotas();
			}
		}, 
		{
			text: "Cancelar",
			handler: function() {
				windowNotasFiscais.close();
			}
		}]
	});

	var cont = 0;
	var taraTemp = "";
	var volumeTemp = "";
    dsNotasVagoes.each(
		function (record) {
			ImportarRegistroParaPopupNotas(record);

			if (indPreenchimentoTara) taraTemp = record.data.TaraVagao;
			if (indPreenchimentoVolume) volumeTemp = record.data.VolumeVagao;
            cont++;
		}
	);

    if (indPreenchimentoTara) Ext.getCmp("taraVagao").setValue(taraTemp);
    if (indPreenchimentoVolume) Ext.getCmp("volumeVagao").setValue(volumeTemp);
    Ext.getCmp("pesoDclVagao").setValue(toneladaUtil);
    

    // INSERE LINHAS VAZIAS
    var dif = 1-cont;
    if (dif > 0){
        for (var i=0; i<dif; i++){
            InserirLinhaNfVazia();
        }    
    }else{
        InserirLinhaNfVazia();
    }

	/**********************************************************
	FORM DADOS DAS NOTAS FISCAIS - FIM
	**********************************************************/	
	formNotasFiscais.render("div-grid-nota-fiscal");

</script>
