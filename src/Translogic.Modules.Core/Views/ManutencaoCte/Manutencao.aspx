<%@ Page Title="" Language="C#" Inherits="System.Web.Mvc.ViewPage" MasterPageFile="~/Views/Shared/Interna.master" %>

<%@ Import Namespace="System.Globalization" %>
<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .vagao-invalido
        {
            color: red;
        }
        
        .campo-numero-internacional
        {
            text-align: right;
        }
    </style>
    <%
        var idFluxoComercial = (int)ViewData["ID_FLUXO_COMERCIAL"];
        var codigoFluxo = (string)ViewData["CODIGO_FLUXO"];
        var indFluxoCte = (bool)ViewData["IND_FLUXO_CTE"];
        var indFluxoInternacional = (bool)ViewData["IND_FLUXO_INTERNACIONAL"];
        var indFluxoInternacionalDestinoTifObrigatorio = (bool)ViewData["IND_FLUXO_INTERNACIONAL_TIF_OBRIGATORIO"];
        var indFluxoOrigemIntercambio = (bool)ViewData["IND_FLUXO_ORIGEM_INTERCAMBIO"];
        var indPreenchimentoVolume = (bool)ViewData["IND_PREENCHIMENTO_VOLUME"];
        var indPreenchimentoTara = (bool)ViewData["IND_PREENCHIMENTO_TARA"];
        var indTaraObrigatorio = (bool)ViewData["IND_TARA_OBRIGATORIO"];
        var indNfeObrigatorio = (bool)ViewData["IND_NFE_OBRIGATORIO"];
        var indFluxoConteiner = (bool)ViewData["IND_FLUXO_CONTEINER"];
        var indFluxoPallet = (bool)ViewData["IND_FLUXO_PALLETS"];
        var cnpjRemetenteFiscal = (string)ViewData["CNPJ_REMETENTE_FISCAL"];
        var cnpjDestinatarioFiscal = (string)ViewData["CNPJ_DESTINATARIO_FISCAL"];
        var cfopContrato = (string)ViewData["CFOP_CONTRATO"];

        var dataLimiteNf = (DateTime)ViewData["LIMITADOR_DATA_NF"];
        var mesesRetroativos = (int)ViewData["MESES_RETROATIVOS"];
        var idOrigem = (int)ViewData["ID_ESTACAO_ORIGEM"];
        var codigoOrigem = (string)ViewData["CODIGO_ESTACAO_ORIGEM"];
        var idDestino = (int)ViewData["ID_ESTACAO_DESTINO"];
        var codigoDestino = (string)ViewData["CODIGO_ESTACAO_DESTINO"];

        var idMercadoria = (int)ViewData["ID_MERCADORIA"];
        var descricaoMercadoria = (string)ViewData["DESCRICAO_MERCADORIA"];
        var codigoMercadoria = (string)ViewData["CODIGO_MERCADORIA"];

        var indVerificaPesoPorVolume = (bool)ViewData["IND_VERIFICA_PESO_POR_VOLUME"];
        var indValidarCfopContrato = (bool)ViewData["IND_VALIDAR_CFOP_CONTRATO"];
        var indValidarCnpjRemetente = (bool)ViewData["IND_VALIDAR_CNPJ_REMETENTE"];
        var indValidarCnpjDestinatario = (bool)ViewData["IND_VALIDAR_CNPJ_DESTINATARIO"];

        var cteId = (int)ViewData["ID_CTE_SELECIONADO"];
        var vagaoCodOrig = (string)ViewData["VAGAO_COD"];
        var vagaoIdOrig = (int)ViewData["VAGAO_ID"];
        var volume = (double)ViewData["VOLUME"];
        var toneladaUtil = (double)ViewData["TONELADA_UTIL"];
        var ufRemetenteFiscal = (string)ViewData["UF_REMETENTE_FISCAL"];
        var mostraCampoAlteraTomador = (string)ViewData["MOSTRAR_ALTERA_TOMADOR"];
    %>
    <script type="text/javascript" language="javascript">
        var modalChaveCte = null;
        function VoltarTela() {
            if (Ext.Msg.confirm("Informa��o", "Voc� deseja realmente sair?", function(btn, text) {
                if (btn == 'yes') {
                    var url = '<%= Url.Action("Index") %>';
                    document.location.href = url;
                }
            })) ;
        }

        var mostraCampoAlteraTomador = "<%= mostraCampoAlteraTomador %>";
        var ufRemetenteFiscal = "<%= ufRemetenteFiscal %>";
        var indFluxoCte = <%= indFluxoCte ? "true" : "false" %>;
        var indFluxoInternacionalDestinoTifObrigatorio = <%= indFluxoInternacionalDestinoTifObrigatorio ? "true" : "false" %>;
        var indFluxoOrigemIntercambio = <%= indFluxoOrigemIntercambio ? "true" : "false" %>;
        var indFluxoInternacional = <%= indFluxoInternacional ? "true" : "false" %>;
        var indPreenchimentoVolume = <%= indPreenchimentoVolume ? "true" : "false" %>;
        var indPreenchimentoTara = <%= indPreenchimentoTara ? "true" : "false" %>;
        var indTaraObrigatorio = <%= indTaraObrigatorio ? "true" : "false" %>;
        var indNfeObrigatorio = <%= indNfeObrigatorio ? "true" : "false" %>;

        var indVerificaPesoPorVolume = <%= indVerificaPesoPorVolume ? "true" : "false" %>;
        var indVerificarConteiner = false;

        var indValidarCfopContrato = <%= indValidarCfopContrato ? "true" : "false" %>;
        var indValidarCnpjRemetente = <%= indValidarCnpjRemetente ? "true" : "false" %>;
        var indValidarCnpjDestinatario = <%= indValidarCnpjDestinatario ? "true" : "false" %>;

        var indFluxoConteiner = <%= indFluxoConteiner ? "true" : "false" %>;
        var indFluxoPallet = <%= indFluxoPallet ? "true" : "false" %>;
        var cnpjRemetenteFiscal = "<%= cnpjRemetenteFiscal %>";
        var cnpjDestinatarioFiscal = "<%= cnpjDestinatarioFiscal %>";
        var codigoMercadoria = "<%= codigoMercadoria %>";

        var cfopContrato = "<%= cfopContrato %>";

        var dataLimiteNf = new Date(<%= dataLimiteNf.Year %>, <%= dataLimiteNf.Month - 1 %>, <%= dataLimiteNf.Day %>, 0, 0, 0, 0);
        var mesesRetroativos = <%= mesesRetroativos %>;

        var idOrigem = <%= idOrigem %>;
        var idDestino = <%= idDestino %>;

        var cteId = "<%= cteId %>";
        var vagaoCodigoOrig = "<%= vagaoCodOrig %>";
        var vagaoIdOrig = "<%= vagaoIdOrig %>";
        var volumeOrig = <%= volume.ToString(CultureInfo.GetCultureInfo("en-US")) %>;
        var toneladaUtilOrig = <%= toneladaUtil.ToString(CultureInfo.GetCultureInfo("en-US")) %>;
        var codigoFluxoOrig = "<%= codigoFluxo %>";
        var idFluxoComercialOrig = <%= idFluxoComercial %>;

        var vagaoCodigo = "<%= vagaoCodOrig %>";
        var vagaoId = "<%= vagaoIdOrig %>";
        var volumeTemp = <%= volume.ToString(CultureInfo.GetCultureInfo("en-US")) %>;
        var toneladaUtil = <%= toneladaUtil.ToString(CultureInfo.GetCultureInfo("en-US")) %>;
        var codigoFluxo = "<%= codigoFluxo %>";
        var idFluxoComercial = <%= idFluxoComercial %>;

        var salvarHabilitadoVagao = false;
        var salvarHabilitadoFluxo = false;
        
        var telaOrigem = 'Manutencao';

        function getAlteraTomadorSelecionado() {
            var campoChkAlteraTomador = Ext.getCmp("chkAlteraTomador").getValue()[0];
            if (campoChkAlteraTomador == undefined) {

                return false;
            } else {

                if (campoChkAlteraTomador.checked) {
                    return true;
                }
            }
            return false;
        }

        function PesoRenderer(val) {
            return Ext.util.Format.number(val, '0.000,000/i');
        }

        function VolumeRenderer(val) {
            return Ext.util.Format.number(val, '0.000,000/i');
        }

        function RendererFloat(val) {
            return Ext.util.Format.number(val, '0.000,000/i');
        }

        function MonetarioRenderer(val) {
            if (val == null || val == "") {
                return "";
            }

            return "R$ " + Ext.util.Format.number(val, '0.000,00/i');
        }

        function DataRenderer(val) {
            if (val == null || val == "") {
                return "";
            }

            return Ext.util.Format.date(val, "d/m/Y");
        }

        function ValidarCampoMaiorZero(nomeCampo) {
            var erroMenorZero = false;

            if (Ext.getCmp(nomeCampo).isValid()) {
                var valorCampo = Ext.getCmp(nomeCampo).getValue();
                if (isNaN(valorCampo)) {
                    Ext.getCmp(nomeCampo).markInvalid("O campo deve ser um valor num�rico.");
                    erroMenorZero = true;
                } else {
                    if (valorCampo <= 0) {
                        Ext.getCmp(nomeCampo).markInvalid("O campo deve ser maior que zero.");
                        erroMenorZero = true;
                    }
                }
            }

            return !erroMenorZero;
        }

        /**
        FUN��ES CHAMADAS DA WINDOW DE NOTAS - INICIO
        */

        function RoundWithDecimals(val, dec) {
            var decimals = Math.pow(10, dec);
            var result = Math.round(val * decimals) / decimals;
            return result;
        }

        function CalcularPesoPorDensidade(volume) {
            if (volume != "" && volume > 0) {
                var responseText = $.ajax({
                    url: "<%= Url.Action("ObterPesoPorVolume") %>",
                    type: "POST",
                    async: false,
                    dataType: 'json',
                    data: ({
                        volume: volume,
                        codigoMercadoria: codigoMercadoria
                    })
                }).responseText;

                var result = Ext.util.JSON.decode(responseText);
                // var result = eval(tmp);
                if (result.Erro) {
                    Ext.Msg.alert('Mensagem de Erro', result.Mensagem);
                    return null;
                } else {
                    return result.Peso;
                }
            }

        }

        function VerificarConteiner(codigoConteiner) {
            if (codigoConteiner != "") {
                var responseText = $.ajax({
                    url: "<%= Url.Action("ObterDadosConteiner") %>",
                    type: "POST",
                    async: false,
                    dataType: 'json',
                    data: ({ codigoConteiner: codigoConteiner })
                }).responseText;

                var result = Ext.util.JSON.decode(responseText);
                // var result = eval(tmp);
                if (result.Erro) {
                    Ext.Msg.alert('Mensagem de Erro', result.Mensagem);
                }
                return !result.Erro;
                // return true;
            }
        }

        var recordEdit = null;

        function EditarDadosNota(record) {
            recordEdit = record;
           
            if (indNfeObrigatorio) {
                if (!record.data.IndObtidoAutomatico) {
                    Ext.Msg.alert('Mensagem de Informa��o', 'Todas as notas fiscais deste fluxo devem ser via NFe');
                    return;
                }
            }

            windowNotaFiscal = new Ext.Window({
                id: 'windowNotaFiscal',
                title: 'Editar dados da Nota Fiscal',
                modal: true,
                width: 444,
                height: 550,
                autoLoad: {
                    url: '<%= Url.Action("FormNotaFiscal") %>',
                    scripts: true
                }
            });
            windowNotaFiscal    .show();
        }

        function RemoverNotasVagao() {
            //remove do store as notas
            dsNotasVagoes.removeAll();
        }

        function afterEditNotaFiscal(e) {
            /*
            e.grid - This grid
            e.record - The record being edited
            e.field - The field name being edited
            e.value - The value being set
            e.originalValue - The original value for the field, before the edit.
            e.row - The grid row index
            e.column - The grid column index
            */
            // execute an XHR to send/commit data to the server, in callback do (if successful):
            if (e.field == "ChaveNfe") {
                // var w = Ext.getCmp('gridNotasFiscais');
                e.grid.getEl().mask();

                ObterDadosNfe(e.value, e, e.grid);
            }

            if (e.field == "Conteiner") {
                e.grid.getEl().mask();

                if (!VerificarConteiner(e.value)) {
                    e.grid.getEl().unmask();
                    e.cancel = true;
                } else {
                    e.grid.getEl().unmask();
                }
            }
        }

        function CriarCampoNfe() {
            return new Ext.form.TextField({
                allowblank: false
            });
        }

        function ImportarRegistroParaTelaPrincipal(recordPopup, tara, volume, pesoDcl) {
            var rec = new dsNotasVagoes.recordType({
                ChaveNfe: recordPopup.data.ChaveNfe,
                SerieNotaFiscal: recordPopup.data.SerieNotaFiscal,
                NumeroNotaFiscal: recordPopup.data.NumeroNotaFiscal,
                PesoTotal: recordPopup.data.PesoTotal,
                PesoRateio: recordPopup.data.PesoRateio,
                ValorNotaFiscal: recordPopup.data.ValorNotaFiscal,
                ValorTotalNotaFiscal: recordPopup.data.ValorTotalNotaFiscal,
                Remetente: recordPopup.data.Remetente,
                Destinatario: recordPopup.data.Destinatario,
                CnpjRemetente: recordPopup.data.CnpjRemetente,
                CnpjDestinatario: recordPopup.data.CnpjDestinatario,
                SiglaRemetente: recordPopup.data.SiglaRemetente,
                SiglaDestinatario: recordPopup.data.SiglaDestinatario,
                UfRemetente: recordPopup.data.UfRemetente,
                UfDestinatario: recordPopup.data.UfDestinatario,
                InscricaoEstadualRemetente: recordPopup.data.InscricaoEstadualRemetente,
                InscricaoEstadualDestinatario: recordPopup.data.InscricaoEstadualDestinatario,
                DataNotaFiscal: recordPopup.data.DataNotaFiscal,
                Conteiner: recordPopup.data.Conteiner,
                TIF: recordPopup.data.TIF,
                VolumeNotaFiscal: recordPopup.data.VolumeNotaFiscal,
                IndObtidoAutomatico: recordPopup.data.IndObtidoAutomatico
            });
            rec.commit();
            dsNotasVagoes.add(rec);
            toneladaUtil = pesoDcl;
            // tara;
            volumeTemp = volume;
        }

        function ImportarRegistroParaPopupNotas(record) {
            var rec = new notasFiscaisStore.recordType({
                ChaveNfe: record.data.ChaveNfe,
                SerieNotaFiscal: record.data.SerieNotaFiscal,
                NumeroNotaFiscal: record.data.NumeroNotaFiscal,
                PesoTotal: record.data.PesoTotal,
                PesoRateio: record.data.PesoRateio,
                ValorNotaFiscal: record.data.ValorNotaFiscal,
                ValorTotalNotaFiscal: record.data.ValorTotalNotaFiscal,
                Remetente: record.data.Remetente,
                Destinatario: record.data.Destinatario,
                CnpjRemetente: record.data.CnpjRemetente,
                CnpjDestinatario: record.data.CnpjDestinatario,
                SiglaRemetente: record.data.SiglaRemetente,
                SiglaDestinatario: record.data.SiglaDestinatario,
                UfRemetente: record.data.UfRemetente,
                UfDestinatario: record.data.UfDestinatario,
                InscricaoEstadualRemetente: record.data.InscricaoEstadualRemetente,
                InscricaoEstadualDestinatario: record.data.InscricaoEstadualDestinatario,
                DataNotaFiscal: record.data.DataNotaFiscal,
                Conteiner: record.data.Conteiner,
                TIF: record.data.TIF,
                VolumeNotaFiscal: record.data.VolumeNotaFiscal,
                IndObtidoAutomatico: record.data.IndObtidoAutomatico,
                PesoDclVagao: record.data.PesoDclVagao
            });
            rec.commit();
            gridNotasFiscais.getStore().add(rec);
        }

        function ConverteDataStr(data) {
            return Date.parseDate(data, "d/m/Y");
        }

        function ObterDadosNfe(chaveNfe, e, w) {

            if (chaveNfe == null || chaveNfe == "") {
                e.record.data.SerieNotaFiscal = "";
                e.record.data.NumeroNotaFiscal = "";
                e.record.data.PesoTotal = "";
                e.record.data.VolumeNotaFiscal = "";
                e.record.data.ValorNotaFiscal = "";
                e.record.data.ValorTotalNotaFiscal = "";
                e.record.data.Remetente = "";
                e.record.data.Destinatario = "";
                e.record.data.CnpjRemetente = "";
                e.record.data.CnpjDestinatario = "";
                e.record.data.SiglaRemetente = "";
                e.record.data.SiglaDestinatario = "";
                e.record.data.DataNotaFiscal = "";
                e.record.data.UfRemetente = "";
                e.record.data.UfDestinatario = "";
                e.record.data.InscricaoEstadualRemetente = "";
                e.record.data.InscricaoEstadualDestinatario = "";
                e.record.data.IndObtidoAutomatico = false;
                e.record.commit();
                e.cancel = true;
                w.getEl().unmask();
                return;
            }

            Ext.Ajax.request({
                url: "<% = Url.Action("ObterDadosNfe") %>",
                timeout: 150000,
                success: function(response) {
                    w.getEl().unmask();
                    var data = Ext.decode(response.responseText);
                    if (!data.Erro) {
                        if (indValidarCnpjRemetente) {
                            if (ufRemetenteFiscal == "EX" && data.DadosNfe.TipoNotaFiscal == "0") {
                                if (data.DadosNfe.CnpjRemetente != cnpjDestinatarioFiscal) {
                                    Ext.Msg.alert('Mensagem de Informa��o', "CNPJ do Remetente da NFe(" + data.DadosNfe.CnpjRemetente + ") n�o � o mesmo do Destinat�rio Fiscal do Fluxo(" + cnpjDestinatarioFiscal + ").");
                                    e.record.data.ChaveNfe = '';
                                    e.record.commit();
                                    return;
                                }
                            } else {
                                if (cnpjRemetenteFiscal != data.DadosNfe.CnpjRemetente) {
                                    Ext.Msg.alert('Mensagem de Informa��o', "O CNPJ do Remetente da NFe n�o � o mesmo do Remetente Fiscal do Fluxo.");
                                    e.record.data.ChaveNfe = '';
                                    e.record.commit();
                                    return;
                                }
                            }
                        }
                        
                        if (indValidarCnpjDestinatario) {
                            if (ufRemetenteFiscal == "EX" && data.DadosNfe.TipoNotaFiscal == "0") {
                                if (data.DadosNfe.CnpjDestinatario != cnpjRemetenteFiscal) {
                                    Ext.Msg.alert('Mensagem de Informa��o', "CNPJ do Destinat�rio da NFe(" + data.DadosNfe.CnpjDestinatario + ") n�o � o mesmo do Remetente Fiscal do Fluxo(" + cnpjRemetenteFiscal + ").");
                                    e.record.data.ChaveNfe = '';
                                    e.record.commit();
                                    return;
                                }
                            } else {
                                if (cnpjDestinatarioFiscal != data.DadosNfe.CnpjDestinatario) {
                                    Ext.Msg.alert('Mensagem de Informa��o', "O CNPJ do Destinat�rio da NFe n�o � o mesmo do Destinat�rio Fiscal do Fluxo.");
                                    e.record.data.ChaveNfe = '';
                                    e.record.commit();
                                    return;
                                }
                            }
                        }

                        if (indValidarCfopContrato) {
                            var regexCfop = new RegExp(cfopContrato);
                            var matchCfop = regexCfop.exec(data.DadosNfe.Cfop);
                            if (matchCfop == null) {
                                Ext.Msg.alert('Mensagem de Informa��o', "O CFOP da NF-e n�o � o mesmo do contrato do Fluxo.");
                                e.record.data.ChaveNfe = '';
                                e.record.commit();
                                return;
                            }
                        }


                        var dataNf = ConverteDataStr(data.DadosNfe.DataNotaFiscal);
                        if (dataNf < dataLimiteNf) {
                            Ext.Msg.alert('Mensagem de Informa��o', "A Data Nota Fiscal n�o pode ultrapassar de " + mesesRetroativos + " meses.");
                            e.record.data.ChaveNfe = '';
                            e.record.commit;
                            return;
                        }

                        e.record.data.SerieNotaFiscal = data.DadosNfe.SerieNotaFiscal;
                        e.record.data.NumeroNotaFiscal = data.DadosNfe.NumeroNotaFiscal;
                        e.record.data.VolumeNotaFiscal = data.DadosNfe.Volume;
                        e.record.data.PesoTotal = data.DadosNfe.Peso;
                        if (indPreenchimentoVolume) {
                            e.record.data.ValorTotalNotaFiscal = data.DadosNfe.Valor;
                        }
                        e.record.data.ValorTotalNotaFiscal = data.DadosNfe.Valor;
                        e.record.data.Remetente = data.DadosNfe.Remetente;
                        e.record.data.Destinatario = data.DadosNfe.Destinatario;
                        e.record.data.CnpjRemetente = data.DadosNfe.CnpjRemetente;
                        e.record.data.CnpjDestinatario = data.DadosNfe.CnpjDestinatario;
                        e.record.data.SiglaRemetente = data.DadosNfe.SiglaRemetente;
                        e.record.data.SiglaDestinatario = data.DadosNfe.SiglaDestinatario;
                        e.record.data.DataNotaFiscal = ConverteDataStr(data.DadosNfe.DataNotaFiscal);
                        e.record.data.UfRemetente = data.DadosNfe.UfRemetente;
                        e.record.data.UfDestinatario = data.DadosNfe.UfDestinatario;
                        e.record.data.InscricaoEstadualRemetente = data.DadosNfe.InscricaoEstadualRemetente;
                        e.record.data.InscricaoEstadualDestinatario = data.DadosNfe.InscricaoEstadualDestinatario;
                        e.record.data.IndObtidoAutomatico = true;
                        e.record.commit();
                    } else {
                        Ext.Msg.alert('Mensagem de Erro', data.Mensagem);
                        e.record.data.ChaveNfe = '';
                        e.record.commit();
                    }
                },
                failure: function(conn, data) {
                    alert("Ocorreu um erro inesperado.");
                    e.record.data.ChaveNfe = '';
                    e.record.commit();
                },
                method: "POST",
                params: { chaveNfe: chaveNfe, codigoFluxo: Ext.getCmp("fluxoManutencao").getValue() }
            });
        }

/**
        FUN��ES CHAMADAS DA WINDOW DE NOTAS - FIM
        */

        function VerificarMudancaVagao(codVagao, campo) {
            if (codVagao != "" && codVagao != vagaoCodigo) {
                var responseText = $.ajax({
                    url: "<%= Url.Action("ObterDadosVagao") %>",
                    type: "POST",
                    async: false,
                    dataType: 'json',
                    data: ({ codigoVagao: codVagao })
                }).responseText;

                var result = Ext.util.JSON.decode(responseText);
                // var result = eval(tmp);
                if (result.Erro) {
                    Ext.Msg.alert('Mensagem de Erro', result.Mensagem);
                    campo.markInvalid(result.Mensagem);
                    campo.setValue(vagaoCodigoOrig);
                } else {
                    vagaoId = result.IdVagao;
                    vagaoCodigo = codVagao;
                    Ext.getCmp('btnSalvar').enable();
                    salvarHabilitadoVagao = true;
                }
            }
        }

        function VerificarMudancaFluxoComercial(codigoFluxoComercial, campo) {

            if (codigoFluxoComercial != "" && codigoFluxoComercial != codigoFluxoOrig.substring(2) && codigoFluxoComercial != codigoFluxo) {
                var responseText = $.ajax({
                    url: "<%= Url.Action("ObterDadosFluxoComercial") %>",
                    type: "POST",
                    async: false,
                    dataType: 'json',
                    data: ({ codigoFluxoComercial: codigoFluxoComercial, idCte: cteId })
                }).responseText;

                var result = Ext.util.JSON.decode(responseText);
                // var result = eval(tmp);
                if (result.Erro) {
                    Ext.Msg.alert('Mensagem de Erro', result.Mensagem);
                    campo.markInvalid(result.Mensagem);
                    campo.setValue(codigoFluxoOrig);
                } else {
                    var cnpjRemetenteFiscalAnterior = cnpjRemetenteFiscal;
                    var cnpjDestinatarioFiscalAnterio = cnpjDestinatarioFiscal;

                    var indFluxoPalletAnterior = indFluxoPallet;
                    var indFluxoConteinerAnterior = indFluxoConteiner;

                    indValidarCfopContrato = result.IndValidarCfopContrato;
                    indValidarCnpjRemetente = result.IndValidarRemetente;
                    indValidarCnpjDestinatario = result.IndValidarCnpjDestinatario;
                    indFluxoCte = result.IndFluxoCte;
                    idFluxoComercial = result.IdFluxoComercial;
                    indPreenchimentoTara = result.IndPreenchimentoTara;
                    indTaraObrigatorio = result.IndTaraObrigatorio;
                    indNfeObrigatorio = result.IndNfeObrigatorio;
                    cfopContrato = result.CfopContrato;
                    cnpjRemetenteFiscal = result.CnpjRemetenteFiscal;
                    cnpjDestinatarioFiscal = result.CnpjDestinatarioFiscal;
                    indFluxoOrigemIntercambio = result.IndFluxoOrigemIntercambio;
                    indFluxoInternacional = result.IndFluxoInternacional;
                    indPreenchimentoVolume = result.IndPreenchimentoVolume;
                    indFluxoInternacionalDestinoTifObrigatorio = result.IndFluxoInternacionalTifObrigatorio;
                    idOrigem = result.IdEstacaoOrigem;
                    codigoOrigem = result.CodigoEstacaoOrigem;
                    idDestino = result.IdEstacaoDestino;
                    codigoDestino = result.CodigoEstacaoDestino;
                    idMercadoria = result.IdMercadoria;
                    descricaoMercadoria = result.DescricaoMercadoria;
                    indFluxoConteiner = result.IndFluxoConteiner;
                    indFluxoPallet = result.IndFluxoPallet;
                    codigoMercadoria = result.CodigoMercadoria;
                    indVerificaPesoPorVolume = result.IndVerificaPesoPorVolume;

                    codigoFluxo = codigoFluxoComercial;

                    //***********************************************************//
                    // Na troca de fluxo, quando o fluxo antigo for PALLET       //
                    // e o atual for CONTEINER, obrigar o lan�amento dos         // 
                    // conteineres, para que o DACTe seja cobrado por conteiner  //
                    // e n�o por tonelada.                                       //
                    //***********************************************************//
                    if (indFluxoPalletAnterior && indFluxoConteiner) {
                        indVerificarConteiner = true;
                    } else {
                        indVerificarConteiner = false;
                    }

                    if (cnpjRemetenteFiscalAnterior != cnpjRemetenteFiscal ||
                        cnpjDestinatarioFiscalAnterio != cnpjDestinatarioFiscal) {
                        dsNotasVagoes.removeAll();
                    }

                    Ext.getCmp('btnSalvar').enable();
                    salvarHabilitadoFluxo = true;
                }
            }
        }

        var arrDetalhesFormulario = new Array();
        /****
        INICIO LINHA 1
        ****/
        var dsSimNao = new Ext.data.ArrayStore({
            fields: ['value', 'text'],
            data: [[true, 'Sim'], [false, 'N�o']]
        });

        var txtVagao = {
            xtype: 'textfield',
            vtype: 'ctevagaovtype',
            id: 'vagaoManutencao',
            fieldLabel: 'Vag�o',
            name: 'manutencao.Vagao',
            allowBlank: false,
            autoCreate: { tag: 'input', type: 'text', maxlength: '7', autocomplete: 'off' },
            maxLength: 20,
            width: 80,
            value: vagaoCodigo,
            listeners: {
                'blur': function(field) {
                    VerificarMudancaVagao(field.getValue(), field);
                }
            }
        };

        var txtFluxo = {
            xtype: 'textfield',
            vtype: 'ctefluxovtype',
            id: 'fluxoManutencao',
            fieldLabel: 'Fluxo',
            name: 'manutencao.Fluxo',
            allowBlank: false,
            maxLength: 20,
            width: 80,
            value: codigoFluxo,
            style: 'text-transform: uppercase',
            autoCreate: { tag: 'input', type: 'text', maxlength: '5', autocomplete: 'off' },
            listeners:
            {
                'blur': function(campo) {
                    var value = campo.getValue();
                    var newValue = "";

                    for (var i = 0; i < value.length; i++) {
                        if (!isNaN(value.charAt(i))) {
                            newValue += value.charAt(i);
                        }
                    }
                    campo.setValue(newValue);

                    VerificarMudancaFluxoComercial(campo.getValue(), campo);
                }
            }
        };

        var chkAlteraTomador = {
            xtype: 'checkboxgroup',
            fieldLabel: 'Alterar Tomador',
            id: 'chkAlteraTomador',
            width: 200,
            name: 'chkAlteraTomador',
            style: 'margin-top:5px; margin-left:30px;',
            listeners:
            {
                'change': function(campo) {

                    var bAlteraTomador = getAlteraTomadorSelecionado();


                    ////var campoChkAlteraTomador = campo.getValue()[0];
                    
                    
                    if (bAlteraTomador === false) {

                        if ((salvarHabilitadoVagao !== true) && (salvarHabilitadoFluxo !== true)){
                            Ext.getCmp('btnSalvar').disable();     
                        }
                    } else {

////                        if (campoChkAlteraTomador.checked) {
                            Ext.getCmp('btnSalvar').enable();                                                
////                        }
                    }
                }
            },
            items: [
		     { name: 'chkAlteraTomador', inputValue: 1}
		    ]
        };

        var coluna1 = { width: 100, layout: 'form', border: false, items: [txtVagao] };
        var coluna2 = { width: 100, layout: 'form', border: false, items: [txtFluxo] };
        //var coluna3 = null;
        var linha1 = null;

        if (mostraCampoAlteraTomador === "S") {
            //coluna3 = { width: 100, layout: 'form', border: false, items: [chkAlteraTomador] };
            linha1 =
            {
                layout: 'column',
                border: false,
                items: [coluna1, coluna2, { width: 100, layout: 'form', border: false, items: [chkAlteraTomador] }]
            };

        } else {
            linha1 =
            {
                layout: 'column',
                border: false,
                items: [coluna1, coluna2]
            };
        }

        arrDetalhesFormulario.push(linha1);

        /***
        FIM DA LINHA 1
        ****/
        
        /****
        INICIO GRID VAGOES
        *****/

        function onEditNotasFiscaisVagao(idVagao, codigoVagao) {

            windowNotasFiscais = new Ext.Window({
                id: 'windowNotasFiscais',
                title: 'Notas Fiscais',
                modal: true,
                width: 644,
                height: 390,
                autoLoad: {
                    url: '<%= Url.Action("GridNotaFiscal") %>',
                    scripts: true,
                    params: { idVagao: idVagao, codigoVagao: codigoVagao }
                }
            });

            windowNotasFiscais.show();
        }

        /****
        INICIO GRID VAGOES NOTAS
        *****/
        var dsNotasVagoes = new Ext.data.JsonStore({
            url: '<%= Url.Action("ObterNotaCte") %>',
            root: "Items",
            fields: [
                'IdVagao',
                'ChaveNfe',
                'SerieNotaFiscal',
                'NumeroNotaFiscal',
                'PesoTotal',
                'PesoRateio',
                'ValorNotaFiscal',
                'ValorTotalNotaFiscal',
                'Remetente',
                'Destinatario',
                'CnpjRemetente',
                'CnpjDestinatario',
                'Mercadoria',
                'SiglaRemetente',
                'SiglaDestinatario',
                { name: 'DataNotaFiscal', type: 'date', dateFormat: 'M$' },
                'TIF',
                'UfRemetente',
                'UfDestinatario',
                'InscricaoEstadualRemetente',
                'InscricaoEstadualDestinatario',
                'VolumeNotaFiscal',
                'IndObtidoAutomatico',
                'Conteiner',
                'PesoDcl'],
            baseParams: { id: cteId }
        });


        var gridNotasVagoes = new Ext.grid.GridPanel({
            viewConfig: {
                forceFit: true
            },
            // title: 'Vag�es � despachar',
            height: 200,
            region: 'center',
            stripeRows: true,
            store: dsNotasVagoes,
            loadMask: { msg: App.Resources.Web.Carregando },
            colModel: new Ext.grid.ColumnModel({
                defaults: {
                    sortable: false
                },
                columns: [
                    new Ext.grid.RowNumberer(),
                    //detalheVagaoDespachoAction,
                    { header: 'Vag�o', width: 50, dataIndex: "CodigoVagao", sortable: false, menuDisabled: true, hidden: true },
                    { header: 'Chave', width: 250, dataIndex: "ChaveNfe", sortable: false },
                    { header: 'S�rie', width: 50, dataIndex: "SerieNotaFiscal", sortable: false },
                    { header: 'Nro.', width: 50, dataIndex: "NumeroNotaFiscal", sortable: false },
                    { header: 'Peso Rateio', dataIndex: "PesoRateio", sortable: false, renderer: RendererFloat },
                    { header: 'Peso Total', dataIndex: "PesoTotal", sortable: false, renderer: RendererFloat }
                ]
            }),
            tbar: [{
                text: 'Editar Notas',
                type: 'button',
                iconCls: 'icon-page-white',
                handler: function(b, e) {
                    onEditNotasFiscaisVagao(vagaoId, vagaoCodigo);
                }
            }]
        });

        statusCte = function(status) {
            if (status == 0) {
                return "Ativo";
            } else {
                return "Inativo";
            }
        };
        var dsCte = new Ext.data.JsonStore({
            url: '<%= Url.Action("ObterCteSelecionado") %>',
            root: "Items",
            fields: [
                'CteId',
                'IdDespacho',
                'SerieDesp5',
                'NumDesp5',
                'SerieDesp6',
                'NumDesp6',
                'CodVagao',
                'Fluxo',
                'ToneladaUtil',
                'Volume',
                'NroCte',
                'SerieCte',
                'DataEmissao',
                'Status'
            ],
            baseParams: { id: cteId }
        });


        var gridCte = new Ext.grid.GridPanel({
            viewConfig: {
                forceFit: true
            },
            stripeRows: true,
            region: 'center',
            store: dsCte,
            autoHeight: true,
            autoScroll: true,
            columnLines: true,
            // title: 'CTE:',
            loadMask: { msg: App.Resources.Web.Carregando },
            colModel: new Ext.grid.ColumnModel({
                defaults: {
                    sortable: false
                },
                columns: [
                    { dataIndex: "CteId", hidden: true },
                    { header: 'S�rie Desp5', dataIndex: "SerieDesp5", sortable: false },
                    { header: 'Num Desp5', dataIndex: "NumDesp5", sortable: false },
                    { header: 'S�rie Desp6', dataIndex: "SerieDesp6", sortable: false },
                    { header: 'Num Desp6', dataIndex: "NumDesp6", sortable: false },
                    { header: 'Vag�o', dataIndex: "CodVagao", sortable: false },
                    { header: 'Fluxo', dataIndex: "Fluxo", sortable: false },
                    { header: 'TU', dataIndex: "ToneladaUtil", sortable: false, renderer: Ext.util.Format.numberRenderer('0.000,00/i') },
                    { header: 'Volume', dataIndex: "Volume", sortable: false, renderer: Ext.util.Format.numberRenderer('0.000,00/i') },
                    { header: 'Nro CTe', dataIndex: "NroCte", sortable: false },
                    { header: 'S�rie CTe', dataIndex: "SerieCte", sortable: false },
                    { header: 'Data Emiss�o', dataIndex: "DataEmissao", sortable: false },
                    { header: 'Status', dataIndex: "Status", sortable: false, renderer: statusCte }
                ]
            })
        });


        /****
        FIM GRID VAGOES NOTAS
        *****/
        var formDetalhes = new Ext.FormPanel({
            id: 'formDetalhes',
            title: "Manuten��o de CT-e",
            autoScroll: true,
            region: 'center',
            bodyStyle: 'padding: 15px',
            items: [{
                    xtype: 'fieldset',
                    autoHeight: true,
                    title: 'CT-e',
                    items: [gridCte]
                },
                arrDetalhesFormulario,
                {
                    xtype: 'fieldset',
                    autoHeight: true,
                    title: 'Notas Fiscais',
                    items: [gridNotasVagoes]
                }],
            buttonAlign: 'center',
            labelAlign: 'top',
            buttons: [
                {
                    id: 'btnSalvar',
                    name: 'btnSalvar',
                    text: 'Salvar',
                    type: 'submit',
                    disabled: true,
                    iconCls: 'icon-save',
                    handler: function(b, e) {
                        Ext.Msg.show({
                            title: 'Mensagem de Informa��o',
                            msg: 'Voc� realmente deseja salvar as altera��es?',
                            buttons: Ext.Msg.YESNO,
                            fn: SalvarAlteracoesCte,
                            icon: Ext.MessageBox.QUESTION
                        });
                    }
                },
                {
                    text: 'Sair',
                    type: 'button',
                    handler: function(b, e) {
                        VoltarTela();
                    }
                }
            ]
        });

        function ValidarCarregamento() {
            if (indFluxoOrigemIntercambio) {
                if (!Ext.getCmp("formDetalhes").getForm().isValid()) {
                    return false;
                }
            }

            return true;
        }
        
        function PrecisaInformarChaveCte(cte)
        {
            var retorno = false;
            
            $.ajax({
                url: "<%= Url.Action("VerificarNecessidadeChave") %>",
                type: "POST",
                async:false,
                cache:false,
                data: { cteId : cte },
                timeout: 120000,
                dataType: "json",
                success: function (result) {
                    if (result.PrecisaChave) {
                        //alert("Obrigat�rio preenchimento do campo Chave CT-e para Fluxo com tipo de servi�o: " + result.TipoServicoCte);
                        var urlChaveCte = '<%= Url.Action("ChaveCte") %>';
                        urlChaveCte += '?chaveCteOrigem=' + result.ChaveCteOrigem;

                       var modalChaveCte = new Ext.Window({
                            id: 'modalChaveCte',
                           title: 'Confirmar chave CTe Origem (ou informar nova chave)',
                            modal: true,
                            width: 399,
                            autoLoad: {
                                url: urlChaveCte,
                                scripts: true
                            }
                        });
                        
                        modalChaveCte.show();
                        retorno = true;
                    } 
                    else if(result.Erro){
                        alert(result.Mensagem);
                        retorno = true;
                    }
                },
                error: function(result) {
                    alert("Ocorreu um erro inesperado: " + result.Mensagem);
                    retorno = true;
                }
            });
            
            return retorno;
        }

        function SalvarAlteracoesCte(btn,novaChaveCte) {
            if (btn == 'no') {
                return;
            }

            if(novaChaveCte == null || novaChaveCte == "" ) {
                if(PrecisaInformarChaveCte(cteId)) {
                    return;
                }    
            }

            if (!ValidarCarregamento()) {
                return;
            }

            var listaNotas = new Array();
            dsNotasVagoes.each(
                function(record) {
                    var nota = record.data;
                    nota.IdVagao = parseInt(vagaoId);
                    nota.DataNotaFiscalString = nota.DataNotaFiscal.format('d/m/Y');

                    if (indPreenchimentoVolume) {
                        nota.PesoTotalNotaFiscal = 0;
                        nota.PesoNotaFiscal = null;
                        nota.VolumeNotaFiscal = parseFloat(nota.VolumeNotaFiscal);
                    } else {
                        nota.PesoTotalNotaFiscal = parseFloat(nota.PesoTotal);
                        nota.PesoNotaFiscal = parseFloat(nota.PesoRateio);
                        nota.VolumeNotaFiscal = null;
                    }

                    if (indFluxoInternacional) {
                        nota.NumeroTif = nota.TIF;
                    }

                    nota.ConteinerNotaFiscal = nota.Conteiner;

                    listaNotas.push(nota);
                }
            );

            var indAlteraTomador = 0;

            if (mostraCampoAlteraTomador === "S") {
                var bAlteraTomador = getAlteraTomadorSelecionado();
                if (bAlteraTomador === true) {
                    indAlteraTomador = 1;
                } 
            }

            var cteTemp = null;
            if (indPreenchimentoVolume) {
                cteTemp = {
                    Id: parseInt(vagaoId),
                    CodigoVagao: vagaoCodigo,
                    IdCteOrigem: parseInt(cteId),
                    IdFluxoComercial: idFluxoComercial,
                    IdDespacho: dsCte.data.items[0].data.IdDespacho,
                    PesoVagao: parseFloat(toneladaUtil),
                    VolumeVagao: parseFloat(volumeTemp),
                    IndAltToma: indAlteraTomador
                };
            } else {
                cteTemp = {
                    Id: parseInt(vagaoId),
                    CodigoVagao: vagaoCodigo,
                    IdCteOrigem: parseInt(cteId),
                    IdFluxoComercial: idFluxoComercial,
                    IdDespacho: dsCte.data.items[0].data.IdDespacho,
                    PesoVagao: parseFloat(toneladaUtil),
                    VolumeVagao: null,
                    IndAltToma: indAlteraTomador
                };
            }
            
            var manutencaoCte = {
                NovaChaveCte: novaChaveCte,
                CteTemp: cteTemp,
                ListaDetalhes: listaNotas
            };
            
            var manutencaoCteDto = $.toJSON(manutencaoCte);

            Ext.getBody().mask();
            $.ajax({
                url: "<%= Url.Action("SalvarManutencaoCte") %>",
                type: "POST",
                dataType: 'json',
                data: manutencaoCteDto,
                timeout: 120000,
                contentType: "application/json; charset=utf-8",
                success: function(result) {
                    Ext.getBody().unmask();

                    if (!result.Erro) {
                        Ext.Msg.show({
                            title: 'Mensagem de Informa��o',
                            msg: 'Dados alterados com sucesso.',
                            buttons: Ext.Msg.OK,
                            fn: function() {
                                document.location.href = '<%= Url.Action("Index") %>';
                            }
                        });
                    } else {
                        // Ext.Msg.alert('Aviso', result.Mensagem); 
                        MostrarWindowErros(result.Mensagem);
                    }
                },
                error: function(result) {
                    alert("Ocorreu um erro inesperado;");
                    Ext.getBody().unmask();
                }
            });
        }

        $(function() {
            dsCte.load();
            dsNotasVagoes.load();

            new Ext.Viewport({
                layout: 'border',
                items: [
                    {
                        region: 'north',
                        height: 40,
                        items: [{
                            region: 'center',
                            applyTo: 'header-content'
                        }]
                    },
                    {
                        region: 'center',
                        autoScroll: true,
                        items: [formDetalhes]
                    }
                ]
            });
        });

        function MostrarWindowErros(mensagensErro) {
            var textarea = new Ext.form.TextArea({
                xtype: 'textarea',
                fieldLabel: 'Foram encontrados os seguintes erros',
                value: mensagensErro,
                readOnly: true,
                height: 220,
                width: 395
            });

            var formErros = new Ext.FormPanel({
                id: 'formErros',
                bodyStyle: 'padding: 15px',
                labelAlign: 'top',
                items: [textarea],
                buttonAlign: 'center',
                buttons: [{
                    text: 'Fechar',
                    type: 'button',
                    handler: function(b, e) {
                        windowErros.close();
                    }
                }]
            });

            windowErros = new Ext.Window({
                id: 'windowErros',
                title: 'Erros',
                modal: true,
                width: 444,
                height: 350,
                items: [formErros]
            });
            windowErros.show();
        }
    </script>
</asp:Content>
<asp:Content ID="MainContent" ContentPlaceHolderID="Content" runat="server">
    <div id="header-content">
        <h1>
            CTe - Manuten��o</h1>
        <small>Voc� est� em CTe > Manuten��o</small>
    </div>
    <div id="divFiltros">
    </div>
</asp:Content>
