﻿<%@ Page Title="" Language="C#" Inherits="System.Web.Mvc.ViewPage" MasterPageFile="~/Views/Shared/Interna.master" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
    
        var vid = '<%=ViewData["id"] %>'; 
        var vMotivo = '<%=ViewData["Motivo"]%>';
        var vDataChegada = '<%= ViewData["DataChegada"]%>';
        var vHraChegada = '<%=ViewData["HraChegada"]%>';
        var vSolicitante = '<%=ViewData["Solicitante"]%>';
        var gLoginSolicitante = '<%=ViewData["LoginSolicitante"]%>';
        var gPatioSolicitante = '<%=ViewData["PatioSolicitante"]%>';
        var vAreaResponsavel = '<%=ViewData["AreaResponsavel"]%>';

        var gtipoAtivo = '<%=ViewData["tipoAtivo"] %>'; 
        var descricaoTipoAtivo = '<%=ViewData["descricaoTipoAtivo"] %>';
        var gPatioDestino = '<%=ViewData["PatioDestino"] %>';

        var motivoStore = new Ext.data.JsonStore({
            root: "Items",
            autoLoad: false,
            proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("BuscarMotivos", "VooAtivo") %>', timeout: 600000 }),
            id: 'motivoStore',
            fields: ['Id', 'Descricao']
        });

        //******************************************************************//
        //***************************** FUNÇÕES*****************************//
        //******************************************************************//
        function PatioDestino() {
                
                if (gtipoAtivo == 4) {
                    Ext.getCmp("lblPatioDestino").setVisible(true);
                    $("label[for='lblPatioDestino']").show();
                    $("#lblPatioDestino").show();
                }
                else {
                Ext.getCmp("lblPatioDestino").setVisible(false);
                    $("label[for='lblPatioDestino']").hide();
                    $("#lblPatioDestino").hide();
                }
            }
        
        function atualizarGridPainelVooAtivos()
        {
            // Carregar a grid
            gridVooAtivoStore.load({ params: { start: 0, limit: 50} });

        };

        var sm = new Ext.grid.CheckboxSelectionModel({

        });

        var gridVooAtivoStore = new Ext.data.JsonStore({
            root: "Items",
            totalProperty: 'Total',
            id: 'gridVooAtivoStore',
            name: 'gridVooAtivoStore',
            proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("ConsultaVooAtivosItems", "VooAtivo") %>', timeout: 600000 }),
            paramNames: {
                sort: "detalhesPaginacaoWeb.Sort",
                dir: "detalhesPaginacaoWeb.Dir",
                start: "detalhesPaginacaoWeb.Start",
                limit: "detalhesPaginacaoWeb.LiConsultaVooAtivosItemsmit"
            },
            fields: ['Id', 'StatusItem', 'Ativo', 'TipoAtivo', 'dtEvento', 'Origem', 'Destino', 'Motivo', 'CondUso', 'Situacao', 'Local', 'Responsavel', 'Lotacao', 'Intercambio', 'EstadoFuturo', 'dtDespacho']
        });        

       $(function () {

            // Toolbar da Grid
            var pagingToolbar = new Ext.PagingToolbar({
                pageSize: 50,
                store: gridVooAtivoStore,
                displayInfo: true,
                displayMsg: App.Resources.Web.GridExibindoRegistros,
                emptyMsg: App.Resources.Web.GridSemRegistros,
                paramNames: {
                    start: "detalhesPaginacaoWeb.Start",
                    limit: "detalhesPaginacaoWeb.Limit"
                }
            });

            var cm = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true
                },
                columns: 
                [
                    { header: 'Status Item', dataIndex: "StatusItem", sortable: false, width: 50 },
                    { header: 'Ativo', dataIndex: "Ativo", sortable: false, width: 40 },
                    { header: 'Tipo Ativo', dataIndex: "TipoAtivo", sortable: false, width: 55 },
                    { header: 'Data Evento', dataIndex: "dtEvento", sortable: false, width: 85 },
                    { header: 'Origem', dataIndex: "Origem", sortable: false, width: 40 },
                    { header: 'Destino', dataIndex: "Destino", sortable: false, width: 50, hidden: gtipoAtivo == 4 ? true : false },
                    { header: 'Cond. de Uso', dataIndex: "CondUso", sortable: false, width: 70 },
                    { header: 'Situação', dataIndex: "Situacao", sortable: false, width: 50 },
                    { header: 'Local', dataIndex: "Local", sortable: false, width: 60 },
                    { header: 'Lotação', dataIndex: "Lotacao", sortable: false, width: 100 },
                    { header: 'Intercâmbio', dataIndex: "Intercambio", sortable: false, width: 60, hidden: true},
                    { header: 'Estado Futuro', dataIndex: "EstadoFuturo", sortable: false, width: 100 },
                    { header: 'Data Despacho', dataIndex: "dtDespacho", sortable: false, width: 100, hidden: gtipoAtivo == 4 ? false : true }
                ]
            });

            var grid = new Ext.grid.EditorGridPanel({
                id: 'grid',
                name: 'grid',
                height: 230,
                autoLoadGrid: true,
                stripeRows: true,
                viewConfig: 
                {
                    forceFit: true,
                    emptyText: 'Não possui dado(s) para exibição.'
                },
                cm: cm,
                region: 'center',
                autoScroll: true,
                loadMask: { msg: App.Resources.Web.Carregando },
                store: gridVooAtivoStore,
                bbar: pagingToolbar,
                sm: sm
            });

            //Carregar combo de motivos
            motivoStore.load({ params: { tipoAtivo: gtipoAtivo } });
            
            grid.getStore().proxy.on('beforeload', function (p, params) {
                params['Id'] = vid;
                params['tipoAtivo'] = gtipoAtivo;
            });

             var lblIDSolicitacao = {
                xtype: 'label',
                name: 'lblIDSolicitacao',
                id: 'lblIDSolicitacao',
                fieldLabel: 'ID Solicitação',
                autoCreate: {
                    tag: 'input',
                    type: 'text',
                    autocomplete: 'off'
                },
                width: 120,
                labelStyle:'font-weight: bold;'
            };

            var lblTipoSolicitacao = {
                xtype: 'label',
                name: 'lblTipoSolicitacao',
                id: 'lblTipoSolicitacao',
                fieldLabel: 'Tipo solicitação',
                autoCreate: {
                    tag: 'input',
                    type: 'text',
                    autocomplete: 'off'
                },
                width: 120,
                labelStyle: 'font-weight: bold;'
            };

            var lblMotivo = {
                xtype: 'label',
                name: 'lblMotivo',
                id: 'lblMotivo',
                fieldLabel: 'Motivo',
                autoCreate: {
                    tag: 'input',
                    type: 'text',
                    autocomplete: 'off'
                },
                width: 350,
                labelStyle:'font-weight: bold;'   
            };

            var fMotivo = {
                layout: 'form',
                width: 350,
                border: false,
                items: [lblMotivo]
            };

            var fIDSolicitacao = {
                layout: 'form',
                width: 150,
                border: false,
                items: [lblIDSolicitacao]
            };
            

            var cIDSolicitacao = {
                width: 100,
                layout: 'column',
                border: false,
                items: [fIDSolicitacao]
            };

            var fTipoSolicitacao = {
                layout: 'form',
                width: 150,
                border: false,
                items: [lblTipoSolicitacao]
            };

             var cTipoSolicitacao = {
                width: 110,
                layout: 'column',
                border: false,
                items: [fTipoSolicitacao]
            };

            var cMotivo = {
                width: 350,
                layout: 'column',
                border: false,
                items: [fMotivo]
            };

            var lblJustificativa = {
                xtype: 'label',
                name: 'lblJustificativa',
                id: 'lblJustificativa',
                fieldLabel: 'Justificativa',
                autoCreate: {
                    tag: 'input',
                    type: 'text',
                    autocomplete: 'off'
                },
                width: 100,
                labelStyle:'font-weight: bold;'
            };


            var lblPatioDestino = {
                xtype: 'label',
                name: 'lblPatioDestino',
                id: 'lblPatioDestino',
                fieldLabel: 'Local Destino (Retorno)',
                autoCreate: {
                    tag: 'input',
                    type: 'text',
                    autocomplete: 'off'
                },
                width: 150,
                labelStyle:'font-weight: bold;'
            };

            var flblPatioDestino = {
                layout: 'form',
                width: 150,
                border: false,
                items: [lblPatioDestino]
            };

             var lblDataHoraChegada = {
                xtype: 'label',
                name: 'lblDataHoraChegada',
                id: 'lblDataHoraChegada',
                fieldLabel: 'Data Hora Chegada',
                autoCreate: {
                    tag: 'input',
                    type: 'text',
                    autocomplete: 'off'
                },
                width: 130,
                labelStyle:'font-weight: bold;'
            };

           
             var fJustificativa = {
                layout: 'form',
                width: '95%',
                height: 95,
                border: false,
                items: [lblJustificativa]
            };

            //AGRUPA CAMPOS DATA HORA CHEGADA
            var fDT = {
                layout: 'form',
                border: false,
                items: [lblDataHoraChegada]
            };

            var cDTHM = {
                width: 180,
                layout: 'column',
                border: false,
                items: [fDT]
            };

            var lblSolicitante = {
                xtype: 'label',
                name: 'lblSolicitante',
                id: 'lblSolicitante',
                fieldLabel: 'Solicitante',
                autoCreate: {
                    tag: 'input',
                    type: 'text',
                    autocomplete: 'off'
                },
                width: 100,
                labelStyle:'font-weight: bold;'
            };

            var lblLoginSolicitante = {
                xtype: 'label',
                name: 'lblLoginSolicitante',
                id: 'lblLoginSolicitante',
                fieldLabel: 'Matricula',
                autoCreate: {
                    tag: 'input',
                    type: 'text',
                    autocomplete: 'off'
                },
                width: 100,
                labelStyle: 'font-weight: bold;'
            };

            var lblPatioSolicitante = {
                xtype: 'label',
                name: 'lblPatioSolicitante',
                id: 'lblPatioSolicitante',
                fieldLabel: 'Pátio',
                autoCreate: {
                    tag: 'input',
                    type: 'text',
                    autocomplete: 'off'
                },
                width: 100,
                labelStyle: 'font-weight: bold;'
            };

            var fSolicitante = {
                layout: 'form',
                width: 150,
                border: false,
                items: [lblSolicitante]
            };

            var flblLoginSolicitante = {
                layout: 'form',
                //width: 150,
                border: false,
                items: [lblLoginSolicitante]
            };

            var flblPatioSolicitante = {
                layout: 'form',
                //width: 150,
                border: false,
                items: [lblPatioSolicitante]
            };

            var fieldsetSolicitante = {
                xtype: 'fieldset',
                fieldLabel: 'Solicitante',
                width: '50%',
                //bodyStyle: 'padding:15px; border-bottom:0px solid; ',
                items: [flblLoginSolicitante, fSolicitante, flblPatioSolicitante],
                labelStyle: 'font-weight: bold;'
            };

            var cAreaSoclicitanteSolicitante = {
                width: 155,
                layout: 'column',
                border: false,
                items: [fSolicitante]
            };
            
            //COLUNA DIREITA
            var lblAreaResponsavel = {
                xtype: 'label',
                name: 'lblAreaResponsavel',
                id: 'lblAreaResponsavel',
                fieldLabel: 'Área Responsável',
                autoCreate: {
                    tag: 'input',
                    type: 'text',
                    autocomplete: 'off'
                },
                width: 100,
                labelStyle:'font-weight: bold;'
            };

            //AGRUPA CAMPOS Area Responsavel e ID
            var fAreaResponsavel = {
                layout: 'form',
                width: 200,
                border: false,
                items: [lblAreaResponsavel]
            };

            var cAreaResponsavel = {
                width: 350,
                layout: 'column',
                border: false,
                items: [fAreaResponsavel]
            };

            var lblJustificativaCCO = {
                xtype: 'label',
                name: 'lblJustificativaCCO',
                id: 'lblJustificativaCCO',
                fieldLabel: 'Justificativa CCO',
                autoCreate: {
                    tag: 'input',
                    type: 'text',
                    autocomplete: 'off'
                },
                width: 120,
                labelStyle:'font-weight: bold;'
            };

            var lblObservacaoCCO = {
                xtype: 'label',
                name: 'lblObservacaoCCO',
                id: 'lblObservacaoCCO',
                fieldLabel: 'Observação ',
                autoCreate: {
                    tag: 'input',
                    type: 'text',
                    autocomplete: 'off'
                },
                width: 120,
                labelStyle:'font-weight: bold;'
            };

            var fieldsetEsquerda = {
                xtype: 'fieldset',
                 width: '50%',
                bodyStyle: 'padding:15px;',
                items: [cIDSolicitacao, cTipoSolicitacao, cMotivo, cDTHM, fieldsetSolicitante]
            };
            
            var fieldsetDireita = {
                xtype: 'fieldset',
                height: 320,
                width: '48.7%',
                bodyStyle: 'padding:15px;',
                items: [ lblAreaResponsavel, lblJustificativaCCO, lblObservacaoCCO, fJustificativa, flblPatioDestino]        
            };

            var fieldsetGrid = {
                xtype: 'fieldset',
                width: '98.7%',
                items: [ grid ]        
            };


            //Conteudo do Form que sera criado
            var panelForm = new Ext.form.FormPanel({
                id: 'grid-filtros',
                title: "Solicitação",
                region: 'center',
                bodyStyle: 'padding:15px 15px 0px 15px;',
                layout: 'column',
                labelAlign: 'top',
                items: [fieldsetEsquerda, fieldsetDireita, fieldsetGrid],
                buttonAlign: "center"
            });
             panelForm.render("divContent");
        });

        //Verificação de checkboxtodos (Habilita e Desabilita Botão Confirmar)
        Ext.onReady(function () {

                Ext.getCmp("lblIDSolicitacao").setText(vid);
                Ext.getCmp("lblTipoSolicitacao").setText(descricaoTipoAtivo);
                Ext.getCmp("lblMotivo").setText(vMotivo);
                Ext.getCmp("lblDataHoraChegada").setText(vDataChegada +" " + vHraChegada);                
                Ext.getCmp("lblSolicitante").setText(vSolicitante);
                Ext.getCmp("lblLoginSolicitante").setText(gLoginSolicitante);
                Ext.getCmp("lblPatioSolicitante").setText(gPatioSolicitante);
                Ext.getCmp("lblAreaResponsavel").setText( vAreaResponsavel );
                Ext.getCmp("lblPatioDestino").setText(gPatioDestino);

                // Traquitana pra pegar valores quebrados por caracteres de Enter, tab, etc.
                var ObservacaoCCO = document.getElementById("ObservacaoCCO");
                Ext.getCmp("lblObservacaoCCO").setText(ObservacaoCCO.innerHTML);

                var Justificativa = document.getElementById("Justificativa");
                Ext.getCmp("lblJustificativa").setText(Justificativa.innerHTML);
                
                var JustificativaCCO = document.getElementById("JustificativaCCO");
                Ext.getCmp("lblJustificativaCCO").setText(JustificativaCCO.innerHTML);

                // Carregar a grid
                gridVooAtivoStore.load({ params: { start: 0, limit: 50} });

                PatioDestino();

         });
    </script>
</asp:Content>
<asp:Content ID="MainContent" ContentPlaceHolderID="Content" runat="server">
    <div id="header-content">
        <h1>
            Visualizar Voos de Ativos</h1>
        <small>Você está em Operações > Manobra > Visualizar Voos de Ativos</small>
        <br />
    </div>
    <div id="divContent">
    </div>
    <div id="ObservacaoCCO" style="display:none">
    <%=ViewData["ObservacaoCCO"]%></div>

    <div id="JustificativaCCO" style="display:none">
    <%=ViewData["JustificativaCCO"]%></div>

    <div id="Justificativa" style="display:none">
    <%=ViewData["Justificativa"]%></div>
</asp:Content>
