﻿<%@ Page Title="" Language="C#" Inherits="System.Web.Mvc.ViewPage" MasterPageFile="~/Views/Shared/Interna.master" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .item-em-solicitacao
        {
            color: red;
        }
    </style>
    <script type="text/javascript">

        /* Tela 1407 - Solicitação de Voo de ativos - resultado da pesquisa */

        //***************************** PARÂMETROS **************************//
        var tipoAtivo = '<%=ViewData["tipoAtivo"] %>';
        var ativos = '<%=ViewData["ativos"] %>';
        var patio = '<%=ViewData["patio"] %>';
        var linha = '<%=ViewData["linha"] %>';
        var os = '<%=ViewData["os"] %>';

        var arrGridItens = [];

        var ativosGeral = [];

        //***************************** STORES *****************************//

        var motivoStore = new Ext.data.JsonStore({
            root: "Items",
            autoLoad: false,
            proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("BuscarMotivos", "VooAtivo") %>', timeout: 600000 }),
            id: 'motivoStore',
            fields: ['Descricao', 'Descricao']
        });

        var gridStore = new Ext.data.JsonStore({
            root: "Items",
            totalProperty: 'Total',
            id: 'gridStore',
            name: 'gridStore',
            proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("PesquisarAtivosSolicitar", "VooAtivo") %>', timeout: 600000 }),
            paramNames: {
                sort: "detalhesPaginacaoWeb.Sort",
                dir: "detalhesPaginacaoWeb.Dir",
                start: "detalhesPaginacaoWeb.Start",
                limit: "detalhesPaginacaoWeb.Limit"
            },
            fields: ['IdAtivo', 'Selecionavel', 'Linha', 'Sequencia', 'Serie', 'Ativo', 'Bitola', 'TipoAtivo', 'DataEvento', 'PatioOrigem', 'Situacao', 'CondicaoUso', 'Local', 'Responsavel', 'Lotacao', 'Intercambio'],
            listeners: {
                exception: function (proxy, response, operation) {
                    Ext.Msg.show({ title: 'Aviso', msg: 'Número do Ativo informado não existe!', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                }
            }
        });

        Ext.onReady(function () {

            //***************************** CAMPOS *****************************//

            var txtJustificativa = new Ext.form.TextArea({
                id: 'txtJustificativa',
                name: 'txtJustificativa',
                fieldLabel: 'Justificativa (*)',
                enableKeyEvents: true,
                autoCreate: {
                    maxlength: '500',
                    tag: 'textarea',
                    rows: '4',
                    cols: '140'
                },
                listeners: {
                    keyUp: function (field, e) {
                        if (field.getRawValue().length >= 250) {
                            $("#txtJustificativa").val($("#txtJustificativa").val().substring(0, 500));
                        }
                    },
                    blur: function (field) {
                        habilitaEnvio();
                    }
                }
            });

            var ddlMotivo = new Ext.form.ComboBox({
                editable: false,
                typeAhead: true,
                forceSelection: true,
                disableKeyFilter: true,
                triggerAction: 'all',
                lazyRender: true,
                mode: 'local',
                store: motivoStore,
                valueField: 'Descricao',
                displayField: 'Descricao',
                fieldLabel: 'Motivo (*)',
                id: 'ddlMotivo',
                width: 300,
                alowBlank: false,
                listeners: {
                    select: function () {
                        habilitaEnvio();
                    }
                }
            });

            var txtPatio = {
                xtype: 'textfield',
                name: 'txtPatio',
                id: 'txtPatio',
                fieldLabel: 'Pátio Destino (*)',
                autoCreate: {
                    tag: 'input',
                    type: 'text',
                    autocomplete: 'off',
                    maxlength: '3'
                },
                maskRe: /[A-Za-z]/,
                style: 'text-transform:uppercase;',
                width: 100,
                enableKeyEvents: true,
                listeners: {
                    blur: function (field) {

                        ValidarPatio(this.id, "Pátio Destino");
                    },
                    keyup: function (obj, e) {
                        var value = Ext.getCmp("txtPatio").getValue();
                        if (value.length == 3) {
                            ValidarPatio(this.id, "Pátio Destino");
                        }
                    }
                }
            };

            var lblDestino = {
                xtype: 'label',
                name: 'lblDestino',
                id: 'lblDestino',
                fieldLabel: 'Linha Destino = (L999)',
                autoCreate: {
                    tag: 'input',
                    type: 'text',
                    autocomplete: 'off'
                },
                width: 32
            };

            var txtSolicitante = {
                xtype: 'textfield',
                name: 'txtSolicitante',
                id: 'txtSolicitante',
                fieldLabel: 'Solicitante (*)',
                autoCreate: {
                    tag: 'input',
                    type: 'text',
                    autocomplete: 'off',
                    maxlength: '30'
                },
                width: 120,
                listeners: {
                    blur: function () {
                        habilitaEnvio();
                    }
                }
            };

            var txtPatioSolicitante = {
                xtype: 'textfield',
                name: 'txtPatioSolicitante',
                id: 'txtPatioSolicitante',
                fieldLabel: 'Pátio Solicitante (*)',
                autoCreate: {
                    tag: 'input',
                    type: 'text',
                    autocomplete: 'off',
                    maxlength: '3'
                },
                maskRe: /[A-Za-z]/,
                style: 'text-transform:uppercase;',
                width: 100,
                enableKeyEvents: true,
                listeners: {
                    blur: function (field) {
                        ValidarPatio(this.id, "Pátio Solicitante");
                    },
                    keyup: function (obj, e) {
                        var value = Ext.getCmp("txtPatioSolicitante").getValue();
                        if (value.length == 3) {
                            ValidarPatio(this.id, "Pátio Solicitante");
                        }
                    }
                }
            };

            var txtDataChegada = new Ext.form.DateField({
                fieldLabel: 'Data Chegada (*)',
                id: 'txtDataChegada',
                name: 'txtDataChegada',
                dateFormat: 'd/n/Y',
                width: 83,
                plugins: [new Ext.ux.InputTextMask('99/99/9999', true)],
                listeners: {
                    blur: function () {
                        habilitaEnvio();
                    }
                }
            });

            var txtHoraChegada = new Ext.form.TimeField({
                name: 'txtHoraChegada',
                id: 'txtHoraChegada',
                format: 'H:i',
                increment: 1,
                fieldLabel: 'Hora (*)',
                maxlength: 5,
                plugins: [new Ext.ux.InputTextMask('99:99', true)],
                width: 65,
                listeners: {
                    blur: function () {
                        habilitaEnvio();
                    }
                }
            });

            var formData = {
                layout: 'form',
                border: false,
                items: [txtDataChegada]
            };

            var formHora = {
                layout: 'form',
                border: false,
                items: [txtHoraChegada]
            };

            var columnsData = {
                layout: 'column',
                border: false,
                width: 300,
                items: [formData, formHora]
            };

            var lblLegenda = {
                xtype: 'label',
                name: 'lblLegenda',
                id: 'lblLegenda',
                text: '*Itens em vermelho estão pendentes em Requisições em Aberto',
                style: 'color: red',
                autoCreate: {
                    tag: 'input',
                    type: 'text',
                    autocomplete: 'off'
                }
            };

            var fieldset = {
                xtype: 'fieldset',
                items: [txtJustificativa, ddlMotivo, txtPatio, lblDestino, txtSolicitante, txtPatioSolicitante, columnsData]
            };

            var txtAtivo = {
                xtype: 'textfield',
                name: 'txtAtivo',
                id: 'txtAtivo',
                fieldLabel: 'Número do ativo',
                autoCreate: {
                    tag: 'input',
                    type: 'text',
                    autocomplete: 'off'
                },
                maskRe: /[0-9;]/,
                width: 120,
                listeners: {
                    blur: function (field) {
                        var ativo = field.getValue();
                        atualizarGrid(ativo);
                    }
                }
            };

            //***************************** GRID ********************************//

            // Toolbar da Grid
            var pagingToolbar = new Ext.PagingToolbar({
                pageSize: 50000,
                store: gridStore,
                displayInfo: true,
                displayMsg: App.Resources.Web.GridExibindoRegistros,
                emptyMsg: App.Resources.Web.GridSemRegistros,
                paramNames: {
                    start: "detalhesPaginacaoWeb.Start",
                    limit: "detalhesPaginacaoWeb.Limit"
                }
            });

            var sm = new Ext.grid.CheckboxSelectionModel({
                listeners: {
                    rowdeselect: function (componente, rowIndex, record) {

                        for (i = 0; i < arrGridItens.length; i++) {
                            if (arrGridItens[i] == record.data.Ativo) {
                                arrGridItens.splice(i, 1);
                                ativosGeral.splice(i, 1);
                            }
                        }
                    }
                },
                renderer: function (v, p, record) {
                    if (record.data.Selecionavel == 0) {
                        return '<div>&#160;</div>';
                    } else {
                        return '<div class="x-grid3-row-checker">&#160;</div>';
                    }
                },
                selectAll: function () {
                    habilitaEnvio();
                    var rowIndex = 0;
                    while (typeof (this.grid.getStore().getAt(rowIndex)) != 'undefined') {
                        var record = this.grid.getStore().getAt(rowIndex);
                        if (record.data.Selecionavel == 0) {
                            this.grid.getSelectionModel().deselectRow(rowIndex, true);
                        }
                        else {
                            this.grid.getSelectionModel().selectRow(rowIndex, true);
                        }

                        rowIndex++;
                    }

                    arrGridItens = [];
                    ativosGeral = [];

                    // Adiciona item selecionado ao grid de itens selecionados somente se estiver selecionado
                    var selection = sm.getSelections();
                    var selecionado = selection.length > 0 ? true : false;
                    if (selecionado == true) {
                        for (i = 0; i < selection.length; i++) {
                            arrGridItens.push(selection[i].data.Ativo);
                            //Adiciona ARRAY GLOBAL GRID Ativos
                            ativosGeral.push(selection[i]);
                        }
                    }
                }
            });

            var cm = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: false,
                    resizable: false,
                    menuDisabled: true
                },
                columns: [
                    new Ext.grid.RowNumberer(),
                    sm,
                //                    { header: 'Pátio Origem', dataIndex: "PatioOrigem", sortable: true, menuDisabled: true, resizable: false, width: 40 },
                //                    { header: 'Linha', dataIndex: "Linha", sortable: true, menuDisabled: true, resizable: false, width: 25, hidden: tipoAtivo == 1 ? true : false },
                //                    { header: 'Seq', dataIndex: "Sequencia", sortable: true, menuDisabled: true, resizable: false, width: 25, hidden: tipoAtivo == 1 ? true : false },
                //                    { header: 'Série', dataIndex: "Serie", sortable: true, menuDisabled: true, resizable: false, width: 25 , hidden: tipoAtivo == 1 ? true : false },
                //                    { header: 'Ativos', dataIndex: "Ativo", sortable: true, menuDisabled: true, resizable: false, width: 30 },
                //                    { header: 'Bitola', dataIndex: "Bitola", sortable: true, menuDisabled: true, resizable: false, width: 40, hidden: tipoAtivo == 1 ? true : false },
                //                    { header: 'Tipo Ativo', dataIndex: "TipoAtivo", sortable: true, menuDisabled: true, resizable: false, width: 80, hidden: true },
                //                    { header: 'Data Evento', dataIndex: "DataEvento", sortable: true, menuDisabled: true, resizable: false, width: 80 },
                //                    { header: 'Situação', dataIndex: "Situacao", sortable: true, menuDisabled: true, resizable: false, width: 100 },
                //                    { header: 'Condição', dataIndex: "CondicaoUso", sortable: true, menuDisabled: true, resizable: false, width: 90 },
                //                    { header: 'Local', dataIndex: "Local", sortable: true, menuDisabled: true, resizable: false, width: 50 },
                //                    { header: 'Lotação', dataIndex: "Lotacao", sortable: true, menuDisabled: true, resizable: false, width: 100 },
                //                    { header: 'Intercâmbio', dataIndex: "Intercambio", sortable: true, menuDisabled: true, resizable: false, width: 100, hidden: true }
                    {header: 'Pátio Origem', dataIndex: "PatioOrigem", sortable: true, menuDisabled: true, resizable: false, width: 70 },
                    { header: 'Linha', dataIndex: "Linha", sortable: true, menuDisabled: true, resizable: false, width: 60, hidden: tipoAtivo == 1 ? true : false },
                    { header: 'Seq', dataIndex: "Sequencia", sortable: true, menuDisabled: true, resizable: false, width: 60, hidden: tipoAtivo == 1 ? true : false },
                    { header: 'Série', dataIndex: "Serie", sortable: true, menuDisabled: true, resizable: false, width: 60, hidden: tipoAtivo == 1 ? true : false },
                    { header: 'Ativos', dataIndex: "Ativo", sortable: true, menuDisabled: true, resizable: false, autowidth: true },
                    { header: 'Bitola', dataIndex: "Bitola", sortable: true, menuDisabled: true, resizable: false, width: 60, hidden: tipoAtivo == 1 ? true : false },
                    { header: 'Tipo Ativo', dataIndex: "TipoAtivo", sortable: true, menuDisabled: true, resizable: false, autowidth: true, hidden: true },
                    { header: 'Data Evento', dataIndex: "DataEvento", sortable: true, menuDisabled: true, resizable: false, width: 100 },
                    { header: 'Situação', dataIndex: "Situacao", sortable: true, menuDisabled: true, resizable: false, autowidth: true },
                    { header: 'Condição', dataIndex: "CondicaoUso", sortable: true, menuDisabled: true, resizable: false, autowidth: true },
                    { header: 'Local', dataIndex: "Local", sortable: true, menuDisabled: true, resizable: false, autowidth: true },
                    { header: 'Lotação', dataIndex: "Lotacao", sortable: true, menuDisabled: true, resizable: false, autowidth: true },
                    { header: 'Intercâmbio', dataIndex: "Intercambio", sortable: true, menuDisabled: true, resizable: false, autowidth: true, hidden: true }
                ]
            });

            var grid = new Ext.grid.EditorGridPanel({
                id: 'grid',
                name: 'grid',
                autoLoadGrid: true,
                height: 150,
                stripeRows: true,
                cm: cm,
                region: 'center',
                viewConfig: {
                    forceFit: false,
                    emptyText: 'Não possui dado(s) para exibição.'
                },
                autoScroll: true,
                loadMask: { msg: App.Resources.Web.Carregando },
                store: gridStore,
                bbar: pagingToolbar,
                sm: sm,
                viewConfig: {
                    forceFit: true,
                    getRowClass: function (row, index) {
                        if (row.data.Selecionavel == 0) {
                            return 'item-em-solicitacao';
                        }
                    }
                },
                listeners: {
                    cellclick: function (grid, rowIndex, columnIndex, e) {
                        var record = grid.getStore().getAt(rowIndex);  // Get the Record
                        var fieldName = grid.getColumnModel().getDataIndex(columnIndex); // Get field name
                        var data = record.get(fieldName);
                    },
                    rowclick: function (this_grid, rowIndex, e) {
                        var record = this_grid.getStore().getAt(rowIndex);
                        
                        if (record.data.Selecionavel == 0) {
                            this_grid.getSelectionModel().deselectRow(rowIndex);
                        }
                        habilitaEnvio();
                        var ativo = record.data.Ativo

                        // Remove item selecionado do array de itens selecionados
                        var indexRemove = -1;
                        for (i = 0; i < arrGridItens.length; i++) {
                            if (arrGridItens[i] == ativo) {
                                indexRemove = i;
                            }
                        }
                        if (indexRemove > -1) {
                            arrGridItens.splice(indexRemove, 1);
                            ativosGeral.splice(indexRemove, 1);
                        }

                        // Adiciona item selecionado ao array de itens selecionados somente se estiver selecionado
                        var selection = sm.getSelections();
                        var selecionado = selection.length > 0 ? true : false;
                        if (selecionado == true) {
                            for (i = 0; i < selection.length; i++) {
                                if (selection[i].data.Ativo == ativo) {
                                    arrGridItens.push(ativo);
                                    geraListaAtivos(record.data);
                                }
                            }
                        }
                    },
                    headerclick: function () {
                        habilitaEnvio();
                    },
                    afterrender: function () {
                        var this_grid = this;
                        gridStore.on('load', function () {

                            var data = this_grid.getStore().data.items;
                            var recs = [];
                            Ext.each(data, function (item, index) {

                                for (i = 0; i < arrGridItens.length; i++) {
                                    if (item.data.Ativo == arrGridItens[i])
                                        recs.push(index);
                                }

                            });
                            this_grid.getSelectionModel().selectRows(recs);
                        });
                    }
                }
            });

            grid.getStore().proxy.on('beforeload', function (p, params) {
                params['tipoAtivo'] = tipoAtivo;
                params['ativos'] = ativos;
                params['patio'] = patio;
                params['linha'] = linha;
                params['os'] = os;
            });

            var painel = new Ext.form.FormPanel({
                id: 'painel',
                layout: 'form',
                labelAlign: 'top',
                border: false,
                autoHeight: true,
                buttonAlign: "center",
                title: "",
                region: 'center',
                bodyStyle: 'padding: 15px',
                items: [fieldset, lblLegenda, grid, txtAtivo],
                buttons:
                [{
                    id: 'btnVoltar',
                    text: 'Voltar',
                    tooltip: 'Voltar',
                    iconCls: 'icon-left',
                    handler: function (c) {
                        voltar();
                    }
                },
                {
                    id: 'btnEnviar',
                    text: 'Enviar para Aprovação',
                    tooltip: 'Enviar para Aprovação',
                    type: 'submit',
                    iconCls: 'icon-tick',
                    disabled: true,
                    handler: function (c) {

                        if (verificaMesmaOrigem()) {
                            Ext.Msg.show({ title: 'Aviso', msg: 'Não é possível solicitar voo de ativos com locais de origem diferentes em uma mesmoa requisição.. Deve ser criada mais de uma para cada local de origem!', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                        } else if (!validarCamposObrigatorios()) {
                            Ext.Msg.show({ title: 'Aviso', msg: 'Preencha os campos obrigatórios em destaque.', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                        }
                        else if (!validarItensSelecionados()) {
                            Ext.Msg.show({ title: 'Aviso', msg: 'Ao menos um ativo deve ser selecionado para que a solicitação de aprovação possa ser enviada.', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                        }
                        else {

                            var dataChegadaSuperior = validarDataChegada();
                            if (dataChegadaSuperior) {
                                Ext.Msg.show({ title: 'Aviso', msg: 'Data de Chegada não pode ser superior a data e hora atual.', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                            }
                            else {

                                var dataEventos = validarDataVentos();
                                if (dataEventos) {
                                    Ext.Msg.show({ title: 'Aviso', msg: 'Data de Chegada não pode ser inferior a data de evento de envio de algum dos itens selecionados.', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                                }
                                else {

                                    Ext.Msg.show({
                                        title: 'Envio para Aprovação',
                                        msg: 'Você deseja confirmar a solicitação de voo para os ativos selecionados?',
                                        buttons: Ext.Msg.YESNO,
                                        fn: enviarParaAprovacao,
                                        animEl: 'elId',
                                        icon: Ext.MessageBox.QUESTION
                                    });

                                }
                            }
                        }
                    }
                }]
            });

            painel.render("divContent");
            //Verifica o tipo de ativo e exibe o patio destino
            HabilitaPatioDestino();

            // Carregar combo de motivos
            motivoStore.load({ params: { tipoAtivo: tipoAtivo} });

            // Atualizar dados do grid
            atualizarGrid(ativos);

            // Setar a data e hora atual
            setNow();

            //***************************** FUNÇÕES E MÉTODOS *******************//
            function geraListaAtivos(data) {
                var patio = Ext.getCmp("txtPatio").getValue().toUpperCase();

                var ativo = new SolicitacaoVooAtivoItem();
                ativo.IdAtivo = data.IdAtivo;
                ativo.Ativo = data.Ativo;
                ativo.DataEvento = data.DataEvento;
                ativo.PatioOrigem = data.PatioOrigem;
                ativo.PatioDestino = patio;
                ativo.Situacao = data.Situacao;
                ativo.CondicaoUso = data.CondicaoUso;
                ativo.Local = data.Local;
                ativo.Responsavel = data.Responsavel;
                ativo.Lotacao = data.Lotacao;
                ativo.Intercambio = data.Intercambio;
                ativo.NumSeq = data.Sequencia;

                ativosGeral.push(ativo);
            }

            function HabilitaPatioDestino() {
                if (tipoAtivo == 4) {
                    $("label[for='txtPatio']").hide();
                    $("#txtPatio").hide();
                }
                else {
                    $("label[for='txtPatio']").show();
                    $("#txtPatio").show();
                }
            }


            function voltar() {
                var url = '<%= Url.Action("BuscarAtivosSolicitar", "VooAtivo") %>';
                window.location = url + "?tipoAtivo=" + tipoAtivo + "&ativos=" + ativos + "&patio=" + patio + "&linha=" + linha + "&os=" + os;
            }

            function atualizarGrid(filtroAtivo) {

                Ext.getBody().mask("Buscando por ativo...", "x-mask-loading");

                var filtrarEsseAtivo = filtroAtivo;

                if (filtroAtivo == "")
                    filtrarEsseAtivo = ativos;

                grid.getStore().proxy.on('beforeload', function (p, params) {
                    params['tipoAtivo'] = tipoAtivo;
                    params['ativos'] = filtrarEsseAtivo;
                    params['patio'] = patio;
                    params['linha'] = linha;
                    params['os'] = os;
                });

                // Carregar a grid
                gridStore.load({ params: { start: 0, limit: 50000} });

                Ext.getBody().unmask();

                habilitaEnvio();

            }

            function validarCamposObrigatorios() {
                var justificativa = Ext.getCmp("txtJustificativa").getValue();
                var motivo = Ext.getCmp("ddlMotivo").getRawValue();
                var patio = Ext.getCmp("txtPatio").getValue();
                var solicitante = Ext.getCmp("txtSolicitante").getValue();
                var dataChegada = Ext.getCmp("txtDataChegada").getValue();
                var horaChegada = Ext.getCmp("txtHoraChegada").getValue();
                var patioSolicitante = Ext.getCmp("txtPatioSolicitante").getValue();


                if (justificativa.trim() == ""
                || (motivo == "" || motivo == "Selecione")
                || solicitante == ""
                || dataChegada == ""
                || horaChegada == ""
                || patioSolicitante == ""
                || (tipoAtivo != 4 && patio == "")) {
                    return false;
                }
                else {
                    return true;
                }
            }

            // Verificar pátio destino ou pátio solicitante
            function ValidarPatio(txt, label) {
                var patio = Ext.getCmp(txt).getValue().toUpperCase();
                var jsonRequest = $.toJSON({ patio: patio });

                $.ajax({
                    url: '<%= Url.Action("ValidarPatio", "VooAtivo") %>',
                    type: "POST",
                    dataType: 'json',
                    data: jsonRequest,
                    timeout: 300000,
                    contentType: "application/json; charset=utf-8",
                    failure: function (conn, data) {
                        Ext.Msg.alert("Mensagem de Erro", "Ocorreu um erro inesperado");
                        Ext.getBody().unmask();
                    },
                    success: function (result) {
                        if (!result.Result) {
                            var message = result.Message.replace("#", label);
                            Ext.Msg.show({ title: 'Aviso', msg: message, buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                            $("#" + txt).val("");
                            Ext.getCmp("btnEnviar").setDisabled(true);
                        }
                        else {
                            habilitaEnvio();
                        }
                    }
                });
            }

            function verificaMesmaOrigem() {
                var selection = sm.getSelections();
                var origem = selection[0].data.PatioOrigem;

                if (selection && selection.length > 0) {

                    for (var i = 1; i < selection.length; i++) {

                        if (selection[i].data.PatioOrigem != origem) {
                            return true;
                        }
                    }
                }
                return false;
            }

            // Validar se existem itens selecionados
            function validarItensSelecionados() {
                var selection = sm.getSelections();
                var selecionado = selection.length > 0 ? true : false;
                return selecionado;
            }

            // Habilitar botão de envio
            function habilitaEnvio() {
                // Habilitar se houver pelo menos um item selecionado.
                if (validarItensSelecionados()) {
                    if (validarCamposObrigatorios()) {
                        Ext.getCmp("btnEnviar").setDisabled(false);
                    }
                    else {
                        Ext.getCmp("btnEnviar").setDisabled(true);
                    }
                }
                else {
                    Ext.getCmp("btnEnviar").setDisabled(true);
                }
            }

            // Comparação entre duas datas e horários
            function comparaDatas(dataIni, dataFim) {

                var arrDtIni = dataIni.split('/');
                var arrHoraIni = dataIni.split(' ');
                var arrDtFim = dataFim.split('/');
                var arrHoraFim = dataFim.split(' ');

                var dtIni = new Date(arrDtIni[2].replace(" " + arrHoraIni[1], "") + '/' + arrDtIni[1] + '/' + arrDtIni[0] + " " + arrHoraIni[1]);
                var dtFim = new Date(arrDtFim[2].replace(" " + arrHoraFim[1], "") + '/' + arrDtFim[1] + '/' + arrDtFim[0] + " " + arrHoraFim[1]);

                return dtIni.getTime() < dtFim.getTime();
            }

            // Validar data de eventos dos itens selecionados com a data de chegada.
            function validarDataVentos() {

                var dataChegada = Ext.getCmp("txtDataChegada").getRawValue();
                var horaChegada = Ext.getCmp("txtHoraChegada").getRawValue();
                var dataHoraChegada = dataChegada + ' ' + horaChegada;
                var selection = sm.getSelections();
                var dataInferior = false;

                if (selection && selection.length > 0) {
                    $.each(selection, function (i, e) {
                        var dataEvento = e.data.DataEvento;
                        var ativo = e.data.Ativo;

                        if (comparaDatas(dataHoraChegada, dataEvento)) {
                            dataInferior = true;
                        }
                    });
                }

                return dataInferior;
            }

            // Verificar se data de chegada é maior que data atual
            function validarDataChegada() {

                var dataChegada = Ext.getCmp("txtDataChegada").getRawValue();
                var horaChegada = Ext.getCmp("txtHoraChegada").getRawValue();
                var dataHoraChegada = dataChegada + ' ' + horaChegada;

                var today = getToday();
                var time = getTime();
                var dataAtual = today + ' ' + time;

                return comparaDatas(dataAtual, dataHoraChegada);
            }

            // Buscar data atual em formato esperado
            function getToday() {

                var today = new Date();
                var dd = today.getDate();
                var mm = today.getMonth() + 1; //January is 0!
                var yyyy = today.getFullYear();

                if (dd < 10) {
                    dd = '0' + dd
                }

                if (mm < 10) {
                    mm = '0' + mm
                }

                today = dd + '/' + mm + '/' + yyyy;

                return today;
            }

            function getTime() {

                var date = new Date();
                var h = date.getHours();
                var m = date.getMinutes();

                if (h < 10) {
                    h = '0' + h
                }

                if (m < 10) {
                    m = '0' + m
                }

                var time = h + ":" + m;

                return time;
            }

            // Definir data e hora de chegado com data hora atual
            function setNow() {

                var today = getToday();
                Ext.getCmp("txtDataChegada").setValue(today);

                var time = getTime();
                Ext.getCmp("txtHoraChegada").setValue(time);

            }

            // Envio da solicitação
            function enviarParaAprovacao(btn) {

                if (btn == 'yes') {

                    Ext.getBody().mask("Enviando para aprovação...", "x-mask-loading");

                    // Campos
                    var justificativa = Ext.getCmp("txtJustificativa").getValue();
                    var motivo = Ext.getCmp("ddlMotivo").getRawValue();
                    var patio = Ext.getCmp("txtPatio").getValue().toUpperCase();
                    var solicitante = Ext.getCmp("txtSolicitante").getValue();
                    var patioSolicitante = Ext.getCmp("txtPatioSolicitante").getValue().toUpperCase();
                    var dataChegada = Ext.getCmp("txtDataChegada").getRawValue();
                    var horaChegada = Ext.getCmp("txtHoraChegada").getRawValue();
                    var dataHoraChegada = dataChegada + ' ' + horaChegada;

                    // Itens pegando somente selecionados na visualização atual da grid
                    var ativos = [];
                    var selection = sm.getSelections();
                    if (selection && selection.length > 0) {

                        for (var i = 0; i < selection.length; i++) {

                            var ativo = new SolicitacaoVooAtivoItem();
                            ativo.IdAtivo = selection[i].data.IdAtivo;
                            ativo.Ativo = selection[i].data.Ativo;
                            ativo.DataEvento = selection[i].data.DataEvento;
                            ativo.PatioOrigem = selection[i].data.PatioOrigem;
                            ativo.PatioDestino = patio;
                            ativo.Situacao = selection[i].data.Situacao;
                            ativo.CondicaoUso = selection[i].data.CondicaoUso;
                            ativo.Local = selection[i].data.Local;
                            ativo.Responsavel = selection[i].data.Responsavel;
                            ativo.Lotacao = selection[i].data.Lotacao;
                            ativo.Intercambio = selection[i].data.Intercambio;
                            ativo.NumSeq = selection[i].data.Sequencia;

                            ativos.push(ativo);
                        }
                    }

                    var jsonRequest = $.toJSON({ tipoAtivo: tipoAtivo, justificativa: justificativa, motivo: motivo, patio: patio, solicitante: solicitante, patioSolicitante: patioSolicitante, dataHoraChegada: dataHoraChegada, ativos: ativos });

                    $.ajax({
                        url: '<%= Url.Action("EnviarParaAprovacao", "VooAtivo") %>',
                        type: "POST",
                        dataType: 'json',
                        data: jsonRequest,
                        timeout: 300000,
                        contentType: "application/json; charset=utf-8",
                        failure: function (conn, data) {
                            Ext.Msg.alert("Mensagem de Erro", "Ocorreu um erro inesperado");
                            Ext.getBody().unmask();
                        },
                        success: function (result) {

                            if (!result.Result) {
                                Ext.Msg.show({ title: 'Aviso', msg: result.Message, buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                                Ext.getBody().unmask();
                            }
                            else {
                                Ext.getBody().unmask();
                                Ext.Msg.show({
                                    title: 'Aviso',
                                    msg: result.Message,
                                    buttons: Ext.Msg.OK,
                                    fn: voltar,
                                    animEl: 'elId',
                                    icon: Ext.MessageBox.INFO
                                });
                            }
                        }
                    });
                }
            }

            //***************************** CLASSES *********************//

            function SolicitacaoVooAtivoItem() {
                var IdAtivo;
                var Ativo;
                var DataEvento;
                var PatioOrigem;
                var PatioDestino;
                var CondicaoUso;
                var Situacao;
                var Local;
                var Responsavel;
                var Lotacao;
                var Intercambio;
            }

            function GridItem() {
                var Ativo;
                var Selected;
            }

        });       


    </script>
</asp:Content>
<asp:Content ID="MainContent" ContentPlaceHolderID="Content" runat="server">
    <div id="header-content">
        <h1>
            Solicitar Voo de Ativos</h1>
        <small>Você está em Operações > Manobra > Solicitar Voo de ativos</small>
        <br />
    </div>
    <div id="divContent">
    </div>
</asp:Content>
