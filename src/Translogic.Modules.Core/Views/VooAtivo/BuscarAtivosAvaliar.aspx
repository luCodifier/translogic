﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Interna.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content runat="server" ID="HeadContent" ContentPlaceHolderID="HeadContent">
    <script type="text/javascript">

        //***************************** STORES *****************************//
        var storeStatusChamado = new Ext.data.ArrayStore({
            id: 0,
            fields: [
            'IdStatusChamado',
            'StatusChamado'
            ],
            data: [[0, 'Todos'], [1, 'Aberto'], [2, 'Encerrado']]
        });

        var storeTipoSolicitacao = new Ext.data.ArrayStore({
            id: 0,
            fields: [
            'IdTipoSolicitacao',
            'TipoSolicitacao'
            ],
            data: [[0, 'Todos'], [1, 'EOT'], [2, 'Locomotiva'], [3, 'Vagão'], [4, 'Vagão Refaturamento']]
        });

        var gridStore = new Ext.data.JsonStore({
            root: "Items",
            totalProperty: 'Total',
            id: 'gridStore',
            name: 'gridStore',
            proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("ConsultaVooAtivos", "VooAtivo") %>', timeout: 600000 }),
            paramNames: {
                sort: "detalhesPaginacaoWeb.Sort",
                dir: "detalhesPaginacaoWeb.Dir",
                start: "detalhesPaginacaoWeb.Start",
                limit: "detalhesPaginacaoWeb.Limit"
            },
            fields: ['Id', 'DtSolicitacao', 'TipoAtivo', 'Solicitante', 'AreaSolicitante', 'Motivo', 'Status']
        });

        Ext.onReady(function () {

            //***************************** GRID *****************************//

            //var sm = new Ext.grid.CheckboxSelectionModel();

            // Toolbar da Grid
            var pagingToolbar = new Ext.PagingToolbar({
                pageSize: 50,
                store: gridStore,
                displayInfo: true,
                displayMsg: App.Resources.Web.GridExibindoRegistros,
                emptyMsg: App.Resources.Web.GridSemRegistros,
                paramNames: {
                    start: "detalhesPaginacaoWeb.Start",
                    limit: "detalhesPaginacaoWeb.Limit"
                }
            });

            var cm = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true
                },
                columns:
                [
                    { hideable: false, width: 5, renderer: function (val) { return '<div class="ux-row-action-item icon-find" data-id="' + val + '"></div>'; }, dataIndex: 'Id' },
                    { header: 'ID', dataIndex: "Id", sortable: false, width: 20 },
                    { header: 'Data Solicitação', dataIndex: "DtSolicitacao", sortable: false, width: 80 },
                    { header: 'Tipo do Ativo', dataIndex: "TipoAtivo", sortable: false, width: 40 },
                    { header: 'Solicitante', dataIndex: "Solicitante", sortable: false, width: 100 },
                    { header: 'Pátio Solicitante', dataIndex: "AreaSolicitante", sortable: false, width: 60 },
                    { header: 'Motivo', dataIndex: "Motivo", sortable: false, width: 250 },
                    { header: 'Status', dataIndex: "Status", sortable: false, width: 50 }
                ]
            });

            var grid = new Ext.grid.EditorGridPanel({
                id: 'grid',
                name: 'grid',
                height: 360,
                width: 870,
                autoLoadGrid: false,
                stripeRows: true,
                viewConfig: {
                    forceFit: true,
                    emptyText: 'Nenhum registro encontrado para os Filtros Selecionados.'
                },
                cm: cm,
                region: 'center',
                autoScroll: true,
                loadMask: { msg: App.Resources.Web.Carregando },
                store: gridStore,
                bbar: pagingToolbar,
                sm: new Ext.grid.CheckboxSelectionModel(),
                listeners:
                 {
                     cellclick: function (e, rowIndex, columnIndex) {
                         if (columnIndex == 0) {
                             var id = e.selModel.selections.items[0].data.Id;
                             AbreEmNovaAba(id, "Avaliar itens do voo");
                         }
                     }
                 }
            });

            grid.getStore().proxy.on('beforeload', function (p, params) {

                var Id = Ext.getCmp("txtId").getValue();
                var txtDataInicial = Ext.getCmp("txtDataInicial").getValue();
                var txtDataFinal = Ext.getCmp("txtDataFinal").getValue();
                var solicitante = Ext.getCmp("txtSolicitante").getValue();
                var status = Ext.getCmp("cmbStatusChamado").getValue();
                var tipoSolicitacao = Ext.getCmp("cmbTipoSolicitacao").getValue();


                if (Id != "") {
                    params['Id'] = Id;
                }

                if (txtDataInicial != "") {
                    params['dtPeriodoInicio'] = txtDataInicial;
                }

                if (txtDataFinal != "") {
                    params['dtPeriodoFinal'] = txtDataFinal;
                }

                if (solicitante != "") {
                    params['solicitante'] = solicitante;
                }

                if (status != "") {
                    if (status == "Todos")
                        status = 0;
                  
                    params['status'] = status;
                }

                if (tipoSolicitacao != "") {

                    if (tipoSolicitacao == "Todos")
                        tipoSolicitacao = 0;
                   
                    params['tipoSolicitacao'] = tipoSolicitacao;
                }

            });

            //***************************** CAMPOS *****************************//

            var txtId = {
                xtype: 'textfield',
                name: 'txtId',
                id: 'txtId',
                fieldLabel: 'ID',
                autoCreate: {
                    tag: 'input',
                    type: 'text',
                    autocomplete: 'off',
                    maxlength: '24'
                },
                maskRe: /[0-9+;]/,
                style: 'text-transform:uppercase;',
                width: 50
            };

            var txtDataInicial = new Ext.form.DateField({
                fieldLabel: 'Data Inicial',
                id: 'txtDataInicial',
                name: 'txtDataInicial',
                dateFormat: 'd/n/Y',
                width: 90,
                plugins: [new Ext.ux.InputTextMask('99/99/9999', true)]

            });

            var txtDataFinal = new Ext.form.DateField({
                fieldLabel: 'Data Final',
                id: 'txtDataFinal',
                name: 'txtDataFinal',
                dateFormat: 'd/n/Y',
                width: 90,
                plugins: [new Ext.ux.InputTextMask('99/99/9999', true)]

            });

            var txtSolicitante = {
                xtype: 'textfield',
                name: 'txtSolicitante',
                id: 'txtSolicitante',
                fieldLabel: 'Solicitante',
                autoCreate: {
                    tag: 'input',
                    type: 'text',
                    autocomplete: 'off',
                    maxlength: '256'
                },
                width: 100
            };

            var cmbStatusChamado = new Ext.form.ComboBox({
                editable: false,
                typeAhead: true,
                forceSelection: false,
                disableKeyFilter: false,
                triggerAction: 'all',
                lazyRender: true,
                mode: 'local',
                valueField: 'IdStatusChamado',
                displayField: 'StatusChamado',
                fieldLabel: 'Status',
                id: 'cmbStatusChamado',
                name: 'cmbStatusChamado',
                width: 80,
                store: storeStatusChamado,
                value: 'Todos'
            });

            var cmbTipoSolicitacao = new Ext.form.ComboBox({
                editable: false,
                typeAhead: true,
                forceSelection: false,
                disableKeyFilter: false,
                triggerAction: 'all',
                lazyRender: true,
                mode: 'local',
                valueField: 'IdTipoSolicitacao',
                displayField: 'TipoSolicitacao',
                fieldLabel: 'Tipo de Solicitação',
                id: 'cmbTipoSolicitacao',
                name: 'cmbTipoSolicitacao',
                width: 155,
                store: storeTipoSolicitacao,
                value: 'Todos'
            });

            //***************************** FORMS DE CAMPOS *****************************//
            var formId = {
                width: 60,
                layout: 'form',
                border: false,
                items: [txtId]
            };

            var formDataInicial = {
                width: 100,
                layout: 'form',
                border: false,
                items: [txtDataInicial]
            };

            var formDataFinal = {
                width: 100,
                layout: 'form',
                border: false,
                items: [txtDataFinal]
            };

            var formSolicitante = {
                width: 110,
                layout: 'form',
                border: false,
                items: [txtSolicitante]
            };

            var formStatusChamado = {
                width: 90,
                layout: 'form',
                border: false,
                items: [cmbStatusChamado]
            };

            var formTipoSolicitacao = {
                width: 155,
                layout: 'form',
                border: false,
                items: [cmbTipoSolicitacao]
            };

            //Conteudo do Form que sera criado
            var painel = new Ext.form.FormPanel({
                id: 'painel',
                title: "Filtros",
                region: 'center',
                bodyStyle: 'padding:15px;',
                layout: 'form',
                labelAlign: 'top',
                items: {
                    layout: 'column',
                    border: false,
                    items: [formId, formDataInicial, formDataFinal, formSolicitante, formStatusChamado, formTipoSolicitacao]
                },
                buttonAlign: "center",
                buttons:
                    [{
                        text: 'Pesquisar',
                        type: 'submit',
                        iconCls: 'icon-find',
                        handler: function (b, e) {
                            PesquisarVooAtivos();
                        }
                    },
                        {
                            text: 'Limpar',
                            type: 'submit',
                            handler: function (b, e) {
                                LimpaCampos();
                            }
                        }
                    ]
            });

            new Ext.Viewport({
                layout: 'border',
                margins: 10,
                items: [
                    {
                        region: 'north',
                        height: 190,
                        autoScroll: true,
                        items: [{
                            region: 'center',
                            applyTo: 'header-content'
                        },
                            painel]
                    },
                    grid
                ]
            });

        });

        //***************************** FUNÇÕES E MÉTODOS *******************//
        function PesquisarVooAtivos() {
            var Id = Ext.getCmp("txtId").getValue();
           
            var txtDataInicial = Ext.getCmp("txtDataInicial").getRawValue();
            var txtDataFinal = Ext.getCmp("txtDataFinal").getRawValue();
            var solicitante = Ext.getCmp("txtSolicitante").getValue();
            var status = Ext.getCmp("cmbStatusChamado").getRawValue();
            var tipoSolicitacao = Ext.getCmp("cmbTipoSolicitacao").getValue();
            var valoresCampos = Id + txtDataInicial + txtDataFinal + solicitante;

            if (valoresCampos == "" && status == "Todos") {
                Ext.Msg.show({ title: 'Aviso', msg: 'Informe ao menos um filtro de busca na Pesquisa!', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                return;
            }

            if (txtDataInicial != "" && txtDataFinal != "") {
                if (!comparaDatas(txtDataInicial, txtDataFinal)) {
                    Ext.Msg.show({ title: 'Aviso', msg: 'Data inicial não pode ser maior que a data final!', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                    return;
                }
            }

            gridStore.load({ params: { start: 0, limit: 50} });
        }

        function LimpaCampos() {
            //Id
            Ext.getCmp('txtId').setValue("");
            //txtDataInicial
            Ext.getCmp('txtDataInicial').setValue("");
            //txtDataFinal
            Ext.getCmp('txtDataFinal').setValue("");
            //txtSolicitante
            Ext.getCmp('txtSolicitante').setValue("");
            //cmbStatusChamado
            Ext.getCmp('cmbStatusChamado').setValue("Todos");
            //cmbTipoSolicitacao
            Ext.getCmp('cmbTipoSolicitacao').setValue("Todos");

            gridStore.removeAll();
        }

        // Abrir nova aba
        function AbreEmNovaAba(id, nomeAba) {
            var tituloAba = nomeAba + " - " + id;
            var linkAba = '<%= Url.Action("Avaliar") %>?indice=' + id;

            var tabs = parent.Ext.getCmp("conteudo-principal");
            var tab = new parent.Ext.ux.ManagedIFrame.Panel({
                autoScroll: true,
                closable: true,
                title: tituloAba,
                loadMask: { msg: tituloAba },
                defaultSrc: linkAba,
                listeners: {
                    documentloaded: function (frame) {
                        parent.layoutPrincipal.menuUsuario.ativaContador();
                        parent.layoutPrincipal.doLayout(false);
                    }
                }
            });
            tabs.insert(tabs.items.length - 1, tab);
            tab.show();
            tabs.setActiveTab(tabs.items.length - 2);
        }

        // Comparação entre duas datas e horários
        function comparaDatas(dataIni, dataFim) {

            var arrDtIni = dataIni.split('/');
            var arrDtFim = dataFim.split('/');

            var dtIni = new Date(arrDtIni[2] + '/' + arrDtIni[1] + '/' + arrDtIni[0]);
            var dtFim = new Date(arrDtFim[2] + '/' + arrDtFim[1] + '/' + arrDtFim[0]);

            return dtIni.getTime() <= dtFim.getTime();
        }

      

    </script>
</asp:Content>

<asp:Content runat="server" ID="Content2" ContentPlaceHolderID="Content">
    <div id="header-content">
        <h1>
            Busca de Ativos para Voo Avaliar</h1>
        <small>Você está em Operação > Manobra > Buscar Ativos Avaliar</small>
        <br />
    </div>
</asp:Content>

