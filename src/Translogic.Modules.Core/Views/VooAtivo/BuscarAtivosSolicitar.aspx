﻿<%@ Page Title="" Language="C#" Inherits="System.Web.Mvc.ViewPage" MasterPageFile="~/Views/Shared/Interna.master" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        /* Tela 1407 - Solicitação de Voo de ativos */
        

        //***************************** STORES *****************************//

        var linhaStore = new Ext.data.JsonStore({
            root: "Items",
            autoLoad: false,
            proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("ObterLinhas", "VooAtivo") %>', timeout: 600000 }),
            id: 'linhasStore',
            fields: ['Descricao', 'Descricao']
        });

        Ext.onReady(function () {

            //***************************** PARÂMETROS **************************//

            var tipoAtivo = '<%=ViewData["tipoAtivo"] %>';
            var ativos = '<%=ViewData["ativos"] %>';
            var patio = '<%=ViewData["patio"] %>';
            var linha = '<%=ViewData["linha"] %>';
            var os = '<%=ViewData["os"] %>';

            //***************************** CAMPOS *****************************//

            var ddlLinha = new Ext.form.ComboBox({
                editable: false,
                typeAhead: true,
                forceSelection: true,
                disableKeyFilter: true,
                triggerAction: 'all',
                lazyRender: true,
                mode: 'local',
                store: linhaStore,
                valueField: 'Descricao',
                displayField: 'Descricao',
                fieldLabel: 'Linha Origem',
                id: 'ddlLinha',
                width: 235,
                alowBlank: false,
                value: 'Todas'
            });

            var fieldset = {
                xtype: 'fieldset',
                title: 'Filtros',

                items: [{

                    xtype: 'radiogroup',
                    fieldLabel: 'Tipo de Ativo',
                    name: 'TipoAtivo',
                    id: 'TipoAtivo',
                    width: 500,
                    cls: 'x-check-group-alt',
                    items: [
                            { boxLabel: 'EOT', name: 'TipoAtivo', inputValue: 1 },
                            { boxLabel: 'Locomotiva', name: 'TipoAtivo', inputValue: 2 },
                            { boxLabel: 'Vagão', name: 'TipoAtivo', inputValue: 3 }
                            //,{ boxLabel: 'Vagão refaturamento', id: 'rbtVagaoRefaturamento' , name: 'TipoAtivo', inputValue: 4 }
                        ],
                    listeners: {
                        change: function (field, checked) {
                            var idTipo = field.getValue().inputValue;
                            
                            // Deixar todos os campos visíveis
                            ddlLinha.setVisible(true);
                            $("#ddlLinha").show();
                            $("label[for='ddlLinha']").show();

                            Ext.getCmp("txtAtivos").setVisible(true);
                            $("#txtAtivos").show();
                            $("label[for='txtAtivos']").show();

                            Ext.getCmp("lblPrefixos").setVisible(true);

                            Ext.getCmp("txtPatio").setVisible(true);
                            $("#txtPatio").show();
                            $("label[for='txtPatio']").show();


                            if (idTipo == 1 || idTipo == 4) {
                                ddlLinha.setVisible(false);
                                $("#ddlLinha").hide();
                                $("label[for='ddlLinha']").hide();

                                if (idTipo == 4) {
                                    Ext.getCmp("txtAtivos").setVisible(false);
                                    $("#txtAtivos").hide();
                                    $("label[for='txtAtivos']").hide();

                                    Ext.getCmp("lblPrefixos").setVisible(false);

                                    Ext.getCmp("txtPatio").setVisible(false);
                                    $("#txtPatio").hide();
                                    $("label[for='txtPatio']").hide();
                                }

                            }
                        }
                    }
                },

                {
                    layout: 'column',
                    border: false,
                    items: [{
                        layout: 'form',
                        border: false,
                        items: [{
                            xtype: 'textfield',
                            name: 'txtAtivos',
                            id: 'txtAtivos',
                            fieldLabel: 'Ativos (prefixo)',
                            autoCreate: {
                                tag: 'input',
                                type: 'text',
                                autocomplete: 'off',
                                maxlength: '256'
                            },
                            width: 300
                        }]
                    },
                    {
                        xtype: 'label',
                        id: 'lblPrefixos',
                        style: 'color: red; padding-top: 20px; padding-left:10px',
                        text: 'Informe os prefixos separados por vírgula. Ex: 4415, 55551, 33354'
                    }]
                },

                {
                    xtype: 'textfield',
                    name: 'txtPatio',
                    id: 'txtPatio',
                    fieldLabel: 'Pátio Origem',
                    autoCreate: {
                        tag: 'input',
                        type: 'text',
                        autocomplete: 'off',
                        maxlength: '3'
                    },
                    maskRe: /[A-Za-z]/,
                    style: 'text-transform:uppercase;',
                    width: 100,
                    listeners: {
                        change: function (field, newValue, oldValue) {
                            ObterLinhas(newValue);
                        }
                    }
                },

                ddlLinha,
                {
                    xtype: 'textfield',
                    name: 'txtOs',
                    id: 'txtOs',
                    fieldLabel: 'Os (Trem)',
                    autoCreate: {
                        tag: 'input',
                        type: 'text',
                        autocomplete: 'off',
                        maxlength: 12
                    },
                    maskRe: /[0-9]/,
                    width: 200
                }]

            };

            var panelForm = new Ext.form.FormPanel({
                id: 'panelForm',
                layout: 'form',
                labelAlign: 'top',
                border: false,
                autoHeight: true,
                title: "",
                region: 'center',
                bodyStyle: 'padding: 15px',
                items: [fieldset],
                buttonAlign: "center",
                buttons: [{

                    text: 'Pesquisar',
                    type: 'submit',
                    iconCls: 'icon-find',
                    handler: function (b, e) {
                        Pesquisa();
                    }
                }]
            });

            panelForm.render("divContent");

            if (tipoAtivo != "") {
                Ext.getCmp("TipoAtivo").setValue(tipoAtivo);
            }
            if (ativos != "") {
                Ext.getCmp("txtAtivos").setValue(ativos);
            }
            if (patio != "") {
                Ext.getCmp("txtPatio").setValue(patio);
                ObterLinhas(patio);
            }
            if (linha != "") {
                Ext.getCmp("ddlLinha").setValue(linha);
            }
            if (os != "") {
                Ext.getCmp("txtOs").setValue(os);
            }

        });

        function Pesquisa() {

            Ext.getBody().mask("Estamos pesquisando...", "x-mask-loading");

            var tipo = Ext.getCmp('TipoAtivo').getValue();
            var ativos = Ext.getCmp('txtAtivos').getValue();
            var patio = Ext.getCmp('txtPatio').getValue();
            var linha = Ext.getCmp('ddlLinha').getValue();
            
            if (linha == "Todas" || linha == "0")
                linha = "";
            patio = patio.toUpperCase();

            var os = Ext.getCmp('txtOs').getValue().replace("''", "");

            if (camposValidos(tipo, ativos, patio, linha, os)) {

                function detalhesPaginacaoWeb() {
                    sort = "detalhesPaginacaoWeb.Sort";
                    dir = "detalhesPaginacaoWeb.Dir";
                    start = "detalhesPaginacaoWeb.Start";
                    limit = "detalhesPaginacaoWeb.Limit";
                }

                var jsonRequest = $.toJSON({ detalhesPaginacaoWeb: detalhesPaginacaoWeb, tipoAtivo: tipo.inputValue, ativos: ativos, patio: patio, linha: linha, os: os });

                $.ajax({
                    url: '<%= Url.Action("PesquisarAtivosSolicitar", "VooAtivo") %>',
                    type: "POST",
                    dataType: 'json',
                    data: jsonRequest,
                    timeout: 300000,
                    contentType: "application/json; charset=utf-8",
                    failure: function (conn, data) {
                        Ext.Msg.alert("Mensagem de Erro", "Ocorreu um erro inesperado");
                        Ext.getBody().unmask();
                    },
                    success: function (result) {

                        if (!result.Result) {
                            Ext.Msg.show({ title: 'Aviso', msg: result.Message, buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                        }
                        else {

                            if (result.Total > 0) {

                                var url = '<%= Url.Action("Solicitar", "VooAtivo") %>';
                                window.location = url + "?tipoAtivo=" + tipo.inputValue + "&ativos=" + ativos + "&patio=" + patio + "&linha=" + linha + "&os=" + os;
                            }
                            else {
                                Ext.Msg.show({ title: 'Aviso', msg: result.Message, buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                            }
                        }

                        Ext.getBody().unmask();
                    }
                });
            }            
        }

        // Validação de campos
        function camposValidos(tipo, ativos, patio, linha, os) {

            if (tipo == "" || tipo == null) {
                Ext.Msg.show({ title: 'Aviso', msg: 'Selecione um tipo de ativo para filtrar', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                Ext.getBody().unmask();
                return false;
            }
            
            if (ativos.length > 0) {
                var validos = validarSeparadoresAtivos(ativos);
                if (validos == false)
                    return false;
            }

            if (ativos == "" && patio == "" && os == "" && (linha == "" || linha == "Todas")) {
                Ext.Msg.show({ title: 'Aviso', msg: 'Informe ao menos um filtro de busca', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                Ext.getBody().unmask();
                return false;
            }
            
            if (Ext.getCmp('TipoAtivo').getValue().inputValue == 4 && os == "") {
                Ext.Msg.show({ title: 'Aviso', msg: 'Informe Os (Trem)', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                Ext.getBody().unmask();
                return false;
            }

            return true;
        }

        function validarSeparadoresAtivos(ativos) {
            if (ativos.lastIndexOf('.') > 0 || ativos.lastIndexOf(';') > 0) {
                Ext.Msg.show({ title: 'Aviso', msg: 'Informe os prefixos separados por vírgula. Ex: 4415, 55551, 33354', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                Ext.getBody().unmask();
                return false;
            }
            return true;
        }

        // Buscar linhas do pátio
        function ObterLinhas(patio) {

            Ext.getBody().mask("Carregando linhas...", "x-mask-loading");

            Ext.getCmp("ddlLinha").clearValue();

            linhaStore.load({ params: { patio: patio} });

            Ext.getCmp("ddlLinha").setValue("Todas");

            Ext.getBody().unmask();
        }
    </script>
</asp:Content>
<asp:Content ID="MainContent" ContentPlaceHolderID="Content" runat="server">
    <div id="header-content">
        <h1>
            Busca de Ativos para Voo</h1>
        <small>Você está em Operações > Manobra > Solicitar Voo de Ativos</small>
        <br />
    </div>
    <div id="divContent">
    </div>
</asp:Content>