﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Interna.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content runat="server" ID="HeadContent" ContentPlaceHolderID="HeadContent">
    <style type="text/css">
        .data-despacho-desatualizada
        {
            color: red;
        }
    </style>

    <script type="text/javascript">

        var gid = '<%=ViewData["id"] %>';
        var gMotivo = '<%=ViewData["Motivo"]%>';
        
        var gDataChegada = '<%=ViewData["DataChegada"]%>';
        var gHraChegada = '<%=ViewData["HraChegada"]%>';
        var gSolicitante = '<%=ViewData["Solicitante"]%>';
        var gLoginSolicitante = '<%=ViewData["LoginSolicitante"]%>';
        var gPatioSolicitante = '<%=ViewData["PatioSolicitante"]%>';
        var gAreaResponsavel = '<%=ViewData["AreaResponsavel"]%>';
        
        var gtipoAtivo = '<%=ViewData["tipoAtivo"] %>';
        var descricaoTipoAtivo = '<%=ViewData["descricaoTipoAtivo"] %>';
        var gPatioDestino = '<%=ViewData["PatioDestino"] %>';
        var gStatus = '<%=ViewData["Status"] %>';
        var vAprovar = false;

        var motivoStore = new Ext.data.JsonStore({
            root: "Items",
            autoLoad: false,
            proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("BuscarMotivos", "VooAtivo") %>', timeout: 600000 }),
            id: 'motivoStore',
            fields: ['Descricao', 'Descricao']
        });

        var gridStore = new Ext.data.JsonStore({
            root: "Items",
            totalProperty: 'Total',
            id: 'gridStore',
            name: 'gridStore',
            proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("ConsultaVooAtivosItems", "VooAtivo") %>', timeout: 600000 }),
            paramNames: {
                sort: "detalhesPaginacaoWeb.Sort",
                dir: "detalhesPaginacaoWeb.Dir",
                start: "detalhesPaginacaoWeb.Start",
                limit: "detalhesPaginacaoWeb.LiConsultaVooAtivosItemsmit"
            },
            fields: ['Id', 'StatusItem', 'Origem', 'Linha', 'Sequencia', 'Serie',  'Ativo', 'TipoAtivo', 'Bitola', 'dtEvento', 'Destino', 'Motivo', 'CondUso', 'Situacao', 'Local', 'Responsavel', 'Lotacao', 'Intercambio', 'EstadoFuturo', 'OlvTimestamp', 'dtDespacho']
        });

        Ext.onReady(function () {

            // Toolbar da Grid
            var pagingToolbar = new Ext.PagingToolbar({
                pageSize: 50000,
                store: gridStore,
                displayInfo: true,
                displayMsg: App.Resources.Web.GridExibindoRegistros,
                emptyMsg: App.Resources.Web.GridSemRegistros,
                paramNames: {
                    start: "detalhesPaginacaoWeb.Start",
                    limit: "detalhesPaginacaoWeb.Limit"
                }
            });

            var sm = new Ext.grid.CheckboxSelectionModel({
                renderer: function (v, p, record) {
                    if (record.data.StatusItem != 'Pendente') {
                        return '<div>&#160;</div>';
                    } else {
                        return '<div class="x-grid3-row-checker">&#160;</div>';
                    }
                },
                selectAll: function () {
                    //will prevent to select all rows
                    var rowIndex = 0;
                    while (typeof (this.grid.getStore().getAt(rowIndex)) != 'undefined') {
                        var record = this.grid.getStore().getAt(rowIndex);
                        if (record.data.StatusItem != 'Pendente') {
                            this.grid.getSelectionModel().deselectRow(rowIndex, true);
                        }
                        else {
                            this.grid.getSelectionModel().selectRow(rowIndex, true);
                        }
                        rowIndex++;
                    }
                }
            });

            var cm = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true
                },
                columns:
                [
                    sm,
                    { header: 'Status Item', dataIndex: "StatusItem", sortable: false, width: 50 },
                    { header: 'Origem', dataIndex: "Origem", sortable: false, width: 40 },
                    { header: 'Linha', dataIndex: "Linha", sortable: false, width: 40, hidden: gtipoAtivo == 1 ? true : false },
                    { header: 'Seq', dataIndex: "Sequencia", sortable: false, width: 40, hidden: gtipoAtivo == 1 ? true : false },
                    { header: 'Série', dataIndex: "Serie", sortable: false, width: 40, hidden: gtipoAtivo == 1 ? true : false },
                    { header: 'Ativo', dataIndex: "Ativo", sortable: false, width: 40 },
                    { header: 'Tipo Ativo', dataIndex: "TipoAtivo", sortable: false, width: 55, hidden: true },
                    { header: 'Bitola', dataIndex: "Bitola", sortable: false, width: 55, hidden: gtipoAtivo == 1 ? true : false },
                    { header: 'Data Evento', dataIndex: "dtEvento", sortable: false, width: 85 },
                    { header: 'Destino', dataIndex: "Destino", sortable: false, width: 50, hidden: gtipoAtivo == 4 ? true : false },
                    { header: 'Cond. de Uso', dataIndex: "CondUso", sortable: false, width: 70 },
                    { header: 'Situação', dataIndex: "Situacao", sortable: false, width: 50 },
                    { header: 'Local', dataIndex: "Local", sortable: false, width: 60 },
                    { header: 'Lotação', dataIndex: "Lotacao", sortable: false, width: 100 },
                    { header: 'Intercambio', dataIndex: "Intercambio", sortable: false, width: 60, hidden: true },
                    { header: 'Estado Futuro', dataIndex: "EstadoFuturo", sortable: false, width: 100 },
                    { header: 'Data Despacho', dataIndex: "dtDespacho", sortable: false, width: 100, hidden: gtipoAtivo == 4 ? false : true }
                ]
            });

            var grid = new Ext.grid.EditorGridPanel({
                id: 'grid',
                name: 'grid',
                height: 230,
                //width: 870,
                autoLoadGrid: true,
                stripeRows: true,
                viewConfig:
                {
                    forceFit: true,
                    emptyText: 'Não possui dado(s) para exibição.',
                    getRowClass: function (row, index) {

                        var data = comparaDatas(row.data.dtDespacho, row.data.OlvTimestamp);
                        if (row.data.StatusItem == 'Refaturando' && data == true) {
                            //Se encontrou data maior que data da solicitação

                            //Ext.getCmp("btnAprovarTodos").setDisabled(true);
                            return 'data-despacho-desatualizada';
                        }
                    }
                },
                cm: cm,
                region: 'center',
                autoScroll: true,
                loadMask: { msg: App.Resources.Web.Carregando },
                store: gridStore,
                bbar: pagingToolbar,
                sm: sm,
                tbar: [
                    {
                        id: 'btnAprovar',
                        text: 'Aprovar',
                        tooltip: 'Aprovar',
                        hidden: true,
                        disabled: true,
                        iconCls: 'icon-accept',
                        handler: function (c) {
                            if (validaCamposObrigatorios()) {

                                if (gtipoAtivo == 4 && localDestino == "") {
                                    Ext.Msg.show({ title: 'Aviso', msg: 'Campo Local Destino (Retorno) deve ser preenchido.', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                                    return false;
                                }

                                if (verificaEstadoNaoOK() == true) {
                                    Ext.Msg.show({ title: 'Aviso', msg: "Existe Item(s) selecionado(s) com estado futuro não permitido!", buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                                    return;
                                }
                                else {
                                    vAprovar = true;
                                    Ext.Msg.show
                                    ({
                                        title: 'Envio para Aprovação',
                                        msg: 'Você deseja confirmar a aprovação de voo para os ativos selecionados?',
                                        buttons: Ext.Msg.YESNO,
                                        fn: executar,
                                        animEl: 'elId',
                                        icon: Ext.MessageBox.QUESTION
                                    });
                                }
                            };
                        }
                    }
                , {
                    id: 'btnReprovar',
                    text: 'Reprovar',
                    tooltip: 'Reprovar',
                    hidden: true,
                    disabled: true,
                    iconCls: 'icon-cancel',
                    handler: function (c) {
                        if (validaCamposObrigatorios()) {
                            vAprovar = false;
                            Ext.Msg.show
                                ({
                                    title: 'Envio para Reprovação',
                                    msg: 'Você deseja confirmar a reprovação de voo para os ativos selecionados?',
                                    buttons: Ext.Msg.YESNO,
                                    fn: executar,
                                    animEl: 'elId',
                                    icon: Ext.MessageBox.QUESTION
                                });
                        };
                    }
                },
                {
                    id: 'btnReprovarTodos',
                    text: 'Reprovar Todos',
                    tooltip: 'Reprovar Todos',
                    iconCls: 'icon-cancel',
                    hidden: true,
                    disabled: true,
                    handler: function (c) {
                        if (validaCamposObrigatorios()) {
                            vAprovar = false;
                            Ext.Msg.show
                                ({
                                    title: 'Envio para Reprovação',
                                    msg: 'Você deseja confirmar a reprovação de voo para todos os ativos?',
                                    buttons: Ext.Msg.YESNO,
                                    fn: executar,
                                    animEl: 'elId',
                                    icon: Ext.MessageBox.QUESTION
                                });
                        };
                    }
                },
                    {
                        id: 'btnPreparar',
                        text: 'Preparar para Refaturamento',
                        tooltip: 'Preparar para Refaturamento',
                        iconCls: 'icon-accept',
                        hidden: true,
                        disabled: true,
                        handler: function (c) {
                            vAprovar = null;
                            if (validaCamposObrigatorios()) {
                                Ext.Msg.show
                                ({
                                    title: 'Preparar para Refaturamento',
                                    msg: 'Você deseja preparar ativos para faturamento?',
                                    buttons: Ext.Msg.YESNO,
                                    fn: executar,
                                    animEl: 'elId',
                                    icon: Ext.MessageBox.QUESTION
                                });
                            };
                        }
                    }

                     ,
                    {
                        id: 'btnAtualizar',
                        text: 'Atualizar despacho',
                        tooltip: 'Atualizar despacho',
                        iconCls: 'icon-arrow_refresh',
                        hidden: true,
                        disabled: true,
                        handler: function (c) {
                            atualizargrid();
                        }
                    }
                    ,
                    {
                        id: 'btnAprovarTodos',
                        text: 'Aprovar todos',
                        tooltip: 'Aprovar todos',
                        iconCls: 'icon-accept',
                        hidden: true,
                        disabled: true,
                        handler: function (c) {
                            vAprovar = true;
                            if (validaCamposObrigatorios()) {
                                // Validar data de despacho
                                if (verificaDataDespachoMenor()) {
                                    Ext.Msg.show({ title: 'Aviso', msg: "A data do despacho não pode ser menor que a data da solicitação", buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                                }
                                else {
                                    Ext.Msg.show
                                    ({
                                        title: 'Envio para aprovação',
                                        msg: 'Você deseja aprovar todos os ativos para refaturamento?',
                                        buttons: Ext.Msg.YESNO,
                                        fn: executar,
                                        animEl: 'elId',
                                        icon: Ext.MessageBox.QUESTION
                                    });
                                }
                            };
                        }
                    }

                ],
                listeners: {
                    cellclick: function (in_this, rowIndex, e) {
                        var record = in_this.getStore().getAt(rowIndex);
                        if (record.data.StatusItem != 'Pendente') {
                            in_this.getSelectionModel().deselectRow(rowIndex);
                        }
                        else {
                            VerificarSelecionarHabilitaBotoes();
                        }
                    },
                    rowclick: function (in_this, rowIndex, e) {
                        var record = in_this.getStore().getAt(rowIndex);
                        if (record.data.StatusItem != 'Pendente') {
                            in_this.getSelectionModel().deselectRow(rowIndex);
                        }
                    },
                    afterrender: function (this_grid) {

                        $("label[for='txtLocalDestino']").hide();
                        $("#txtLocalDestino").hide();
                        Ext.getCmp("txtLocalDestino").setVisible(false);

                        //Ext.getCmp("btnAprovarTodos").setDisabled(true);

                        if (gtipoAtivo == 4) {

                            //$("label[for='txtLocalDestino']").show();
                            //$("#txtLocalDestino").show();
                            //Ext.getCmp("txtLocalDestino").setVisible(true);

                            // Esconder coluna com CheckBox
                            var colModel = grid.getColumnModel().setHidden(0, true);

                            // Se status == Aberto
                            if (gStatus == 1) {

                                // Validar quais botões apresesentar. Se houver algum item pendente ainda não foi enviado para aprovação corretamente
                                gridStore.on('load', function () {
                                    gridStore.data.each(function (item) {
                                        if (item.data['StatusItem'].toUpperCase() == 'PENDENTE') {
                                            Ext.getCmp("btnReprovarTodos").setVisible(true);
                                            Ext.getCmp("btnPreparar").setVisible(true);
                                            Ext.getCmp("btnReprovarTodos").setDisabled(false);
                                            Ext.getCmp("btnPreparar").setDisabled(false);
                                        }
                                        else if (item.data['StatusItem'].toUpperCase() == 'REPROVADO') {
                                            Ext.getCmp("btnReprovarTodos").setVisible(false);
                                            Ext.getCmp("btnPreparar").setVisible(false);
                                            Ext.getCmp("btnAprovarTodos").setVisible(false);
                                            Ext.getCmp("btnAtualizar").setVisible(false);

                                        }
                                        else if (item.data['StatusItem'].toUpperCase() == 'REFATURANDO') {
                                            Ext.getCmp("btnReprovarTodos").setVisible(false);
                                            Ext.getCmp("btnPreparar").setVisible(false);

                                            Ext.getCmp("btnAprovarTodos").setVisible(true);
                                            Ext.getCmp("btnAtualizar").setVisible(true);

                                            //Se items com data despacho menor que data timestamp ok desabilita botão aprovar todos e campo LOCAL
                                            if (verificaDataDespachoMenor()) {

                                                $("label[for='txtLocalDestino']").hide();
                                                $("#txtLocalDestino").hide();
                                                Ext.getCmp("txtLocalDestino").setVisible(false);

                                                Ext.getCmp("btnAprovarTodos").setDisabled(true);
                                            }
                                            else {

                                                Ext.getCmp("txtLocalDestino").setVisible(true);
                                                $("label[for='txtLocalDestino']").show();
                                                $("#txtLocalDestino").show();

                                                Ext.getCmp("btnAprovarTodos").setDisabled(false);
                                            }


                                            Ext.getCmp("btnAtualizar").setDisabled(false);
                                            //Ext.getCmp("txtLocalDestino").setVisible(true);
                                            //$("label[for='txtLocalDestino']").show();
                                            //$("#txtLocalDestino").show();
                                        }
                                        else if (item.data['StatusItem'].toUpperCase() == 'APROVADO') {

                                            Ext.getCmp("btnReprovarTodos").setVisible(false);
                                            Ext.getCmp("btnPreparar").setVisible(false);
                                            Ext.getCmp("btnAprovarTodos").setVisible(false);
                                            Ext.getCmp("btnAtualizar").setVisible(false);

                                            //Ext.getCmp("txtLocalDestino").setVisible(true);
                                            //$("label[for='txtLocalDestino']").show();
                                            //$("#txtLocalDestino").show();
                                        }
                                    });
                                });
                            }
                            else {
                                //Ext.getCmp("txtLocalDestino").setVisible(true);
                                //$("label[for='txtLocalDestino']").show();
                                //$("#txtLocalDestino").show();
                            }
                        }
                        else {

                            Ext.getCmp("btnReprovar").setVisible(true);
                            Ext.getCmp("btnAprovar").setVisible(true);

                            if (gStatus == 2) {

                                Ext.getCmp("btnReprovar").setDisabled(true);
                                Ext.getCmp("btnAprovar").setDisabled(true);

                                // Esconder coluna com CheckBox
                                var colModel = grid.getColumnModel().setHidden(0, true);

                            }
                        }
                    }
                }
            });

            grid.getStore().proxy.on('beforeload', function (p, params) {
                params['Id'] = gid;
                params['tipoAtivo'] = gtipoAtivo;
            });

            //***************************** CAMPOS *****************************//

            var cmbMotivo = new Ext.form.ComboBox({
                editable: true,
                typeAhead: true,
                forceSelection: true,
                disableKeyFilter: true,
                triggerAction: 'all',
                lazyRender: true,
                mode: 'local',
                valueField: 'Descricao',
                displayField: 'Descricao',
                fieldLabel: 'Motivo (*)',
                id: 'cmbMotivo',
                name: 'cmbMotivo',
                width: 350,
                store: motivoStore,
                value: 'Todos',
                labelStyle: 'font-weight: bold;'
            });

            var lblIDSolicitacao = {
                xtype: 'label',
                name: 'lblIDSolicitacao',
                id: 'lblIDSolicitacao',
                fieldLabel: 'ID Solicitação',
                autoCreate: {
                    tag: 'input',
                    type: 'text',
                    autocomplete: 'off'
                },
                width: 120,
                labelStyle: 'font-weight: bold;'
            };

            var lblTipoSolicitacao = {
                xtype: 'label',
                name: 'lblTipoSolicitacao',
                id: 'lblTipoSolicitacao',
                fieldLabel: 'Tipo solicitação',
                autoCreate: {
                    tag: 'input',
                    type: 'text',
                    autocomplete: 'off'
                },
                width: 120,
                labelStyle: 'font-weight: bold;'
            };

            var fMotivo = {
                layout: 'form',
                width: 350,
                border: false,
                items: [cmbMotivo]
            };

            var fIDSolicitacao = {
                layout: 'form',
                width: 150,
                border: false,
                items: [lblIDSolicitacao]
            };

            var cIDSolicitacao = {
                width: 100,
                layout: 'column',
                border: false,
                items: [fIDSolicitacao]
            };


            var fTipoSolicitacao = {
                layout: 'form',
                width: 150,
                border: false,
                items: [lblTipoSolicitacao]
            };

            var cTipoSolicitacao = {
                width: 110,
                layout: 'column',
                border: false,
                items: [fTipoSolicitacao]
            };

            var cMotivo = {
                width: 350,
                layout: 'column',
                border: false,
                items: [fMotivo]
            };

            var lblJustificativa = {
                xtype: 'label',
                name: 'lblJustificativa',
                id: 'lblJustificativa',
                fieldLabel: 'Justificativa',
                autoCreate: {
                    tag: 'input',
                    type: 'text',
                    autocomplete: 'off'
                },
                width: 100,
                labelStyle: 'font-weight: bold;'
            };

            var fJustificativa = {
                layout: 'form',
                width: '95%',
                height: 95,
                border: false,
                items: [lblJustificativa]
            };

            var dtDataChegada = new Ext.form.DateField({
                fieldLabel: gtipoAtivo == 4 ? "" : 'Data Chegada (*)',
                id: 'dtDataChegada',
                hidden: gtipoAtivo == 4 ? true : false,
                name: 'dtDataChegada',
                dateFormat: 'd/n/Y',
                width: 83,
                labelStyle: 'font-weight: bold;',
                plugins: [new Ext.ux.InputTextMask('99/99/9999', true)]
            });

            var hmHoraMinutoChegada = new Ext.form.TimeField({
                name: 'hmHoraMinutoChegada',
                id: 'hmHoraMinutoChegada',
                hidden: gtipoAtivo == 4 ? true : false,
                format: 'H:i',
                increment: 1,
                fieldLabel: gtipoAtivo == 4 ? "" : 'Hora (*)',
                maxlength: 5,
                plugins: [new Ext.ux.InputTextMask('99:99', true)],
                width: 65,
                labelStyle: 'font-weight: bold;'
            });

            //AGRUPA CAMPOS DATA HORA CHEGADA
            var fDT = {
                layout: 'form',
                border: false,
                items: [dtDataChegada]
            };

            var fHM = {
                layout: 'form',
                border: false,
                items: [hmHoraMinutoChegada]
            };

            var cDTHM = {
                width: 180,
                layout: 'column',
                border: false,
                items: [fDT, fHM]
            };

            var lblSolicitante = {
                xtype: 'label',
                name: 'lblSolicitante',
                id: 'lblSolicitante',
                fieldLabel: 'Nome',
                autoCreate: {
                    tag: 'input',
                    type: 'text',
                    autocomplete: 'off'
                },
                width: 100,
                labelStyle: 'font-weight: bold;'
            };

            var lblLoginSolicitante = {
                xtype: 'label',
                name: 'lblLoginSolicitante',
                id: 'lblLoginSolicitante',
                fieldLabel: 'Matricula',
                autoCreate: {
                    tag: 'input',
                    type: 'text',
                    autocomplete: 'off'
                },
                width: 100,
                labelStyle: 'font-weight: bold;'
            };

            var lblPatioSolicitante = {
                xtype: 'label',
                name: 'lblPatioSolicitante',
                id: 'lblPatioSolicitante',
                fieldLabel: 'Pátio',
                autoCreate: {
                    tag: 'input',
                    type: 'text',
                    autocomplete: 'off'
                },
                width: 100,
                labelStyle: 'font-weight: bold;'
            };

            var fSolicitante = {
                layout: 'form',
                //width: 150,
                border: false,
                items: [lblSolicitante]
            };

            var flblLoginSolicitante = {
                layout: 'form',
                //width: 150,
                border: false,
                items: [lblLoginSolicitante]
            };

            var flblPatioSolicitante = {
                layout: 'form',
                //width: 150,
                border: false,
                items: [lblPatioSolicitante]
            };

            var fieldsetSolicitante = {
                xtype: 'fieldset',
                fieldLabel: 'Solicitante',
                width: '50%',
                //bodyStyle: 'padding:15px; border-bottom:0px solid; ',
                items: [flblLoginSolicitante, fSolicitante, flblPatioSolicitante],
                labelStyle: 'font-weight: bold;'
            };

            //COLUNA DIREITA 
            var txtAreaResponsavel = {
                xtype: 'textfield',
                name: 'txtAreaResponsavel',
                id: 'txtAreaResponsavel',
                fieldLabel: 'Área Responsável (*)',
                autoCreate: {
                    tag: 'input',
                    type: 'text',
                    autocomplete: 'off',
                    maxlength: '255'
                },
                style: 'text-transform:uppercase;',
                width: 200,
                labelStyle: 'font-weight: bold;'
            };

            //AGRUPA CAMPOS Area Responsavel e ID
            var fAreaResponsavel = {
                layout: 'form',
                width: 205,
                border: false,
                items: [txtAreaResponsavel]
            };

            var cAreaResponsavel = {
                width: 350,
                layout: 'column',
                border: false,
                items: [fAreaResponsavel]
            };

            var txtJustificativaCCO = new Ext.form.TextArea({
                id: 'txtJustificativaCCO',
                name: 'txtJustificativaCCO',
                fieldLabel: 'Justificativa CCO (*)',
                enableKeyEvents: true,
                autoCreate: {
                    maxlength: '500',
                    tag: 'textarea',
                    rows: '3',
                    cols: '65'
                },
                listeners: {
                    keyUp: function (field, e) {
                        if (field.getRawValue().length >= 500) {
                            $("#txtJustificativaCCO").val($("#txtJustificativaCCO").val().substring(0, 500));
                        }

                    }
                },
                labelStyle: 'font-weight: bold;'
            });

            var txtLocalDestino = {
                xtype: 'textfield',
                name: 'txtLocalDestino',
                id: 'txtLocalDestino',
                fieldLabel: 'Local Destino (Retorno)',
                autoCreate: {
                    tag: 'input',
                    type: 'text',
                    autocomplete: 'off',
                    maxlength: '4'
                },
                hidden: true,
                maskRe: /[A-Za-z]/,
                style: 'text-transform:uppercase;',
                width: 195,
                labelStyle: 'font-weight: bold;'
            };

            var fPatioDestino = {
                layout: 'form',
                width: 200,
                border: false,
                items: [txtLocalDestino]
            };

            var cPatioDestino = {
                width: 350,
                layout: 'column',
                border: false,
                items: [fPatioDestino]
            };

            var txtObservacaoCCO = new Ext.form.TextArea({
                id: 'txtObservacaoCCO',
                name: 'txtObservacaoCCO',
                fieldLabel: 'Observação (*)',
                enableKeyEvents: true,
                autoCreate: {
                    maxlength: '255',
                    tag: 'textarea',
                    rows: '3',
                    cols: '65'
                },
                listeners: {
                    keyUp: function (field, e) {
                        if (field.getRawValue().length >= 255) {
                            $("#txtObservacaoCCO").val($("#txtObservacaoCCO").val().substring(0, 255));
                        }
                    }
                },
                labelStyle: 'font-weight: bold;'
            });

            var fieldsetEsquerda = {
                xtype: 'fieldset',
                width: '50%',
                bodyStyle: 'padding:15px; border-bottom:0px solid; ',
                items: [cIDSolicitacao, cTipoSolicitacao, cMotivo, cDTHM, fieldsetSolicitante]
            };

            var fieldsetDireita = {
                xtype: 'fieldset',
                height: 348,
                width: '48.7%',
                bodyStyle: 'padding:15px;',
                items: [cAreaResponsavel, txtJustificativaCCO, txtObservacaoCCO, cPatioDestino, fJustificativa]
            };

            var fieldsetGrid = {
                xtype: 'fieldset',
                width: '98.7%',
                items: [grid]
            };

            //Conteudo do Form que sera criado
            var panelForm = new Ext.form.FormPanel({
                id: 'grid-filtros',
                title: "Solicitação",
                region: 'center',
                bodyStyle: 'padding:15px 15px 0px 15px;',
                layout: 'column',
                labelAlign: 'top',
                items: [fieldsetEsquerda, fieldsetDireita, fieldsetGrid],
                buttonAlign: "center"
            });

            panelForm.render("divContent");

            //Carregar combo de motivos
            motivoStore.load({ params: { tipoAtivo: gtipoAtivo} });

            // Carregar a grid
            gridStore.load({ params: { start: 0, limit: 50000} });

            // Setar para readonly os campos se já estiver encerrado
            setFieldToReadOnly();

            // Setar campos     
            Ext.getCmp("lblIDSolicitacao").setText(gid);
            Ext.getCmp("lblTipoSolicitacao").setText(descricaoTipoAtivo);
            Ext.getCmp("cmbMotivo").setValue(gMotivo);

            Ext.getCmp("dtDataChegada").setValue(gDataChegada);
            Ext.getCmp("hmHoraMinutoChegada").setValue(gHraChegada);
            Ext.getCmp("lblSolicitante").setText(gSolicitante);
            Ext.getCmp("lblLoginSolicitante").setText(gLoginSolicitante);
            Ext.getCmp("lblPatioSolicitante").setText(gPatioSolicitante);
            Ext.getCmp("txtAreaResponsavel").setValue(gAreaResponsavel);

            Ext.getCmp("txtLocalDestino").setValue(gPatioDestino);

            // Traquitana pra pegar valores quebrados por caracteres de Enter, tab, etc.
            var ObservacaoCCO = document.getElementById("ObservacaoCCO");
            ObservacaoCCO = (ObservacaoCCO.innerHTML).slice(5);
            Ext.getCmp("txtObservacaoCCO").setRawValue(ObservacaoCCO);

            var Justificativa = document.getElementById("Justificativa");
            Justificativa = (Justificativa.innerHTML).slice(5);
            Ext.getCmp("lblJustificativa").setText(Justificativa);

            var JustificativaCCO = document.getElementById("JustificativaCCO");
            JustificativaCCO = (JustificativaCCO.innerHTML).slice(5);
            Ext.getCmp("txtJustificativaCCO").setValue(JustificativaCCO);

            //Responsavel pelo CHECKBOX TODOS DA GRID
            $(".x-grid3-hd-checker").click(function () {
                VerificarSelecionarHabilitaBotoes();
            });

            //***************************** FUNÇÕES E MÉTODOS *******************//
            function EscondePatioEColunaDestino() {
                if (gtipoAtivo == 4) {
                    return false;
                }
                else {
                    return true;
                }
            }

            // Setar campos como readonly quando já estiver encerrado
            function setFieldToReadOnly() {

                if (gStatus == 2) {

                    Ext.getCmp("cmbMotivo").setDisabled(true);
                    Ext.getCmp("dtDataChegada").setDisabled(true);
                    Ext.getCmp("hmHoraMinutoChegada").setDisabled(true);
                    Ext.getCmp("txtAreaResponsavel").setDisabled(true);
                    Ext.getCmp("txtJustificativaCCO").setDisabled(true);
                    Ext.getCmp("txtObservacaoCCO").setDisabled(true);
                    Ext.getCmp("txtLocalDestino").setDisabled(true);
                }

            }

            function comparaDatas(dataIni, dataFim) {

                var arrDtIni = dataIni.split('/');
                var arrDtFim = dataFim.split('/');

                var dtIni = new Date(arrDtIni[2] + '/' + arrDtIni[1] + '/' + arrDtIni[0]);
                var dtFim = new Date(arrDtFim[2] + '/' + arrDtFim[1] + '/' + arrDtFim[0]);

                return dtIni <= dtFim;
            }

            function verificaDataDespachoMenor() {
                var grid = gridStore;

                for (var i = 0; i < grid.totalLength; i++) {
                    if (comparaDatas(grid.data.items[i].data.dtDespacho, grid.data.items[i].data.OlvTimestamp)) {
                        return true;
                    }
                }
                //Se não encontrou data maior que data da solicitação retorna false
                return false;
            };


            function verificaEstadoNaoOK() {
                var existe = true;
                var selection = sm.getSelections();
                var item;

                if (selection && selection.length > 0) {

                    for (var i = 0; i < selection.length; i++) {

                        if (selection[i].data.EstadoFuturo == "Estado nao permitido" || selection[i].data.EstadoFuturo == "Estado Futuro Duplicado") {
                            return true;
                        }
                    }
                }
                return false;
            };

            function atualizargrid() {
                //Limpa GRID
                LimpaGrid();
                // Carregar a grid
                gridStore.load({ params: { start: 0, limit: 50000} });

            };

            // Executar aprovação e reprovação (param. aprovar = true)
            function executar(btn) {

                var url = '<%= Url.Action("Reprovar", "VooAtivo") %>';
                if (vAprovar) {
                    url = '<%= Url.Action("Aprovar", "VooAtivo") %>';
                }
                else if (vAprovar == null) {
                    url = '<%= Url.Action("Preparar", "VooAtivo") %>';
                }

                // Verifica se usuario clicou em sim
                if (btn == 'yes') {
                    // Campos
                    var Id = Ext.getCmp("lblIDSolicitacao").text;
                    var Motivo = Ext.getCmp("cmbMotivo").getValue();
                    var Justificativa = Ext.getCmp("lblJustificativa").text;
                    var DataChegada = Ext.getCmp("dtDataChegada").getRawValue();
                    var HoraMinutoChegada = Ext.getCmp("hmHoraMinutoChegada").getRawValue();
                    var Solicitante = Ext.getCmp("lblSolicitante").text;
                    var AreaResponsavel = Ext.getCmp("txtAreaResponsavel").getValue();
                    var JustificativaCCO = Ext.getCmp("txtJustificativaCCO").getValue();
                    var ObservacaoCCO = Ext.getCmp("txtObservacaoCCO").getValue();
                    var dataHoraChegada = DataChegada + ' ' + HoraMinutoChegada;

                    // Itens
                    var items = [];
                    if (gtipoAtivo != 4) {
                        var item;
                        var selection = sm.getSelections();
                        if (selection && selection.length > 0) {

                            for (var i = 0; i < selection.length; i++) {

                                item = selection[i].data.Id;

                                items.push(item);
                            }
                        }
                    }
                    else {
                        // Para tipo de ativo 4 (Vagao refaturamento) pegar todos os itens do grid
                        var store = grid.getStore();
                        for (var i = 0; i < store.data.length; i++) {
                            var record = store.data.items[i].data;
                            var item = record["Id"];
                            items.push(item);
                        }
                    }

                    var jsonRequest = $.toJSON
                    ({ id: Id,
                        motivo: Motivo,
                        justificativa: Justificativa,
                        dataHoraChegada: dataHoraChegada,
                        solicitante: Solicitante,
                        areaResponsavel: AreaResponsavel,
                        justificativaCCO: JustificativaCCO,
                        observacaoCCO: ObservacaoCCO,
                        items: items,
                        patioDestino: gPatioDestino
                    });

                    if (vAprovar) {
                        Ext.getBody().mask("Aprovando...", "x-mask-loading");
                    } else if (vAprovar == false) {
                        Ext.getBody().mask("Reprovando...", "x-mask-loading");
                    } else if (vAprovar == null) {
                        Ext.getBody().mask("Preparando...", "x-mask-loading");
                    }

                    $.ajax({
                        url: url,
                        type: "POST",
                        dataType: 'json',
                        data: jsonRequest,
                        timeout: 300000,
                        contentType: "application/json; charset=utf-8",
                        failure: function (conn, data) {
                            Ext.getBody().unmask();
                            Ext.Msg.alert("Mensagem de Erro", "Ocorreu um erro inesperado");
                        },
                        success: function (result) {

                            Ext.getBody().unmask();

                            Ext.Msg.show
                            ({
                                title: 'Aviso',
                                msg: result.Message,
                                buttons: Ext.Msg.OK,
                                fn: reloadPage,
                                animEl: 'elId',
                                icon: Ext.MessageBox.INFO
                            });
                        }
                    });
                };
            }

            function reloadPage() {
                location.reload();
            }

            function validaCamposObrigatorios() {
                var ID = Ext.getCmp("lblIDSolicitacao").text;
                var Motivo = Ext.getCmp("cmbMotivo").getValue();
                var Justificativa = Ext.getCmp("lblJustificativa").text;
                var dtDataChegada = Ext.getCmp("dtDataChegada").getValue();
                var HoraMinChegada = Ext.getCmp("hmHoraMinutoChegada").getValue();
                var Solicitante = Ext.getCmp("lblSolicitante").text;
                var AreaResponsavel = Ext.getCmp("txtAreaResponsavel").getValue();
                var JustificativaCCO = Ext.getCmp("txtJustificativaCCO").getValue();
                var ObservacaoCCO = Ext.getCmp("txtObservacaoCCO").getValue();
                var localDestino = Ext.getCmp("txtLocalDestino").getRawValue();

                if (dtDataChegada == "" || HoraMinChegada == "") {
                    Ext.Msg.show({ title: 'Aviso', msg: 'Campo data e hora da chegada é obrigatório.', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                    return false;
                }

                //Validação data chegada com datas das linhas selecionadas da grid
                var dataInferior = validarDataVentos();

                if (dataInferior) {
                    Ext.Msg.show({ title: 'Aviso', msg: 'Data de Chegada não pode ser inferior a data de evento de envio de algum dos itens selecionados.', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                    return false;
                }

                // Tamanhos
                if (JustificativaCCO.length > 500) {
                    Ext.Msg.show({ title: 'Aviso', msg: 'Campo Justificativa CCO não pode ter mais que 500 caracteres.', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                    return false;
                }

                if (ObservacaoCCO.length > 255) {
                    Ext.Msg.show({ title: 'Aviso', msg: 'Campo Observação CCO não pode ter mais que 255 caracteres.', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                    return false;
                }

                //Campos Obrigatorios para aprovação e reprovação!
                if (AreaResponsavel.trim() == "") {
                    Ext.Msg.show({ title: 'Aviso', msg: 'Campo Área Responsável deve ser preenchido para aprovação e reprovação.', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                    return false;
                } else if (JustificativaCCO.trim() == "") {
                    Ext.Msg.show({ title: 'Aviso', msg: 'Campo Justificativa CCO deve ser preenchido para aprovação e reprovação.', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                    return false;
                } else if (ObservacaoCCO.trim() == "") {
                    Ext.Msg.show({ title: 'Aviso', msg: 'Campo Observação deve ser preenchido para aprovação e reprovação.', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                    return false;
                }

                return true;
            };


            // Verificar se tem linhas selecionadas
            function VerificaLinhasSelecionadas() {
                var resultado = false;
                var selection = sm.getSelections();
                var iCont = 0;

                if (selection && selection.length > 0) {
                    var id = selection[0].data.IdSituacao;

                    selection = sm.getSelections();

                    $.each(selection, function (i, e) {
                        if (id == e.data.id) {
                            iCont++;
                        }
                    });

                    resultado = selection.length == iCont ? true : false;
                }

                return resultado;
            }

            // Busca e armazena lista de ids de vagões
            function BuscaIDLinhasSelecionadas() {
                var selection = sm.getSelections();

                if (selection && selection.length > 0) {
                    var numVagao = selection[0].data.Id;
                    var listaIDS = [];

                    selection = sm.getSelections();

                    $.each(selection, function (i, e) {
                        listaIDS[i] = e.data.NumeroVagao;
                    });
                }

                return listaIDS;
            }

            function VerificaAtivosSelecionados() {
                var resultado = false;
                var selection = sm.getSelections();
                var iCont = 0;
                if (selection && selection.length > 0) {

                    $.each(selection, function (i, e) {
                        iCont++;
                    });

                    if (iCont > 1) {
                        resultado = true;
                    }
                    else {
                        resultado = false;
                    }
                }
                return resultado;
            }

            function VerificarSelecionarHabilitaBotoes() {

                var selection = sm.getSelections();
                var selecionado = selection.length > 0 ? true : false;

                if (selecionado == true) {
                    Ext.getCmp("btnAprovar").setDisabled(false);
                    Ext.getCmp("btnReprovar").setDisabled(false);
                } else {
                    Ext.getCmp("btnAprovar").setDisabled(true);
                    Ext.getCmp("btnReprovar").setDisabled(true);
                }
            }

            function LimpaGrid() {
                gridStore.removeAll();

                //Reseta botom checkboxtodos
                if ($(".x-grid3-hd-checker").hasClass('x-grid3-hd-checker-on')) {
                    $(".x-grid3-hd-checker").removeClass('x-grid3-hd-checker-on')
                }
            };

            // Comparação entre duas datas e horários
            function comparaDatas(dataIni, dataFim) {
                var arrDtIni = dataIni.split('/');
                var arrHoraIni = dataIni.split(' ');
                var arrDtFim = dataFim.split('/');
                var arrHoraFim = dataFim.split(' ');

                var dtIni = new Date(arrDtIni[2].replace(" " + arrHoraIni[1], "") + '/' + arrDtIni[1] + '/' + arrDtIni[0] + " " + arrHoraIni[1]);
                var dtFim = new Date(arrDtFim[2].replace(" " + arrHoraFim[1], "") + '/' + arrDtFim[1] + '/' + arrDtFim[0] + " " + arrHoraFim[1]);

                return dtIni < dtFim;
            };

            // Validar data de eventos dos itens selecionados com a data de chegada.
            function validarDataVentos() {
                var dataChegada = Ext.getCmp("dtDataChegada").getRawValue();
                var horaChegada = Ext.getCmp("hmHoraMinutoChegada").getRawValue();
                var dataHoraChegada = dataChegada + ' ' + horaChegada;
                var selection = sm.getSelections();
                var dataInferior = false;

                if (selection && selection.length > 0) {
                    $.each(selection, function (i, e) {
                        var dataEvento = e.data.dtEvento;
                        var ativo = e.data.Ativo;

                        if (comparaDatas(dataHoraChegada, dataEvento)) {
                            dataInferior = true;
                        }
                    });
                }

                return dataInferior;
            };

            //            if (verificaDataDespachoMenor()) {
            //                Ext.getCmp("btnAprovarTodos").setDisabled(false);
            //            }
            //            else {
            //                Ext.getCmp("btnAprovarTodos").setDisabled(true);
            //            }

        });

    </script>
</asp:Content>
<asp:Content runat="server" ID="Content2" ContentPlaceHolderID="Content">
    <div id="header-content">
        <h1>
            Avaliação de Itens Voo de Ativos</h1>
        <small>Você está em Operação > Manobra > Avaliação de Itens Voo de Ativos </small>
        <br />
    </div>
      <div id="divContent">
    </div>

     <div id="ObservacaoCCO" style="display:none">
    <%=ViewData["ObservacaoCCO"]%></div>

    <div id="JustificativaCCO" style="display:none">
    <%=ViewData["JustificativaCCO"]%></div>

    <div id="Justificativa" style="display:none">
    <%=ViewData["Justificativa"]%></div>
</asp:Content>
