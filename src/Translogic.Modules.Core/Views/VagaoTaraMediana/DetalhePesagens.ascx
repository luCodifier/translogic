﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div id="div-form-detalhe-pesagens">
</div>
<%
    string codVagao = ViewData["CODIGO_VAGAO"] != null ? ViewData["CODIGO_VAGAO"].ToString() : string.Empty;
%>
<script type="text/javascript">

    var dsPesagens = new Ext.data.JsonStore({
        url: '<%= Url.Action("ObterPesagensPlanilha") %>',
        root: "Items",
        remoteSort: true,
        fields: [
                    'Id',
                    'CodVagao',
                    'Tara',
                    'DataCadastro',
                    'Marcar'
			    ],
        listeners: {
        load: function() {
//            for (var j = 0; j <= 5; j++) 
//            {
//                gridPesagens.getSelectionModel().selectRow(j,true);
//            }
        },
        scope: this
        }
    });

    var arraySelectItens = new Array();
    var selectModel = new Ext.grid.CheckboxSelectionModel({singleSelect:false});

    var gridPesagens = new Ext.grid.EditorGridPanel({
        viewConfig: {
            forceFit: true,
            getRowClass: MudaCor
        },
        id: 'grdDados',
        sm: selectModel,
        stripeRows: true,
        region: 'center',
        autoLoadGrid: true,
        store: dsPesagens,
        height: 350,
        loadMask: { msg: App.Resources.Web.Carregando },
        colModel: new Ext.grid.ColumnModel({
            defaults: {
                sortable: false
            },
            columns: [
						selectModel,
                        { dataIndex: "Id", hidden: true },
                        { header: 'Codigo', dataIndex: "CodVagao", sortable: false },
						{ header: 'DataCadastro', dataIndex: "DataCadastro", sortable: false },
                        { header: 'Tara', dataIndex: "Tara", sortable: false }
					],
            isCellEditable: function (col, row) {

                arraySelectItens.push(row);

                selectModel.selectRows(arraySelectItens, false);
            }

        }),
        listeners: {
            render: function(gridData) {
                gridData.store.on('load', function(store, records, options) {                    
                    var j = 0;
                    $.each(store.data.items, function(index, rec) {                        
                        if (rec.data.Marcar) {
                            if (rec.data.Marcar === true) {
                                gridData.getSelectionModel().selectRow(j, true);
                            }
                        }
                        j = j + 1;
                    });
                });
            }
        },
        columnLines: true,
        buttonAlign: 'center'
    });

    gridPesagens.on('render', function () {
        if (gridPesagens.autoLoadGrid)
            gridPesagens.getStore().load({ params: { 'codVagao': '<%=codVagao %>'} });
    });

    var formPesagens = new Ext.form.FormPanel
	({
	    id: 'formPesagens',
	    labelWidth: 80,
	    height: 320,
	    bodyStyle: 'padding: 15px',
	    labelAlign: 'top',
	    items: [gridPesagens],
	    buttonAlign: "center",
	    buttons: [{
	        text: "Sair",
	        handler: function () {
	            windowPesagens.close();
	        }
	    },
		   {
		       text: "Salvar",
		       handler: function () {
		           var idvagoes = "";
		           var listaSelecionados = selectModel.getSelections();
                    
                   for (var i = 0; i < listaSelecionados.length; i++) 
                   {
                       idvagoes += listaSelecionados[i].data.Id + "x";
                   }

		           if (Ext.Msg.confirm("Calculo da Mediana", "A mediana será calculada com base nos registros selecionados, deseja prosseguir ?", function(btn, text) {
		               if (btn == 'yes') {
		                   
                          var responseText = $.ajax({
                                url: "<%= Url.Action("SalvarNovaTara") %>",
                                type: "POST",
                                async: false,
                                dataType: 'json',
                                data: ({
                                    idvagoes: idvagoes,
                                    codvagao: '<%=codVagao %>'
                                })
                            }).responseText;

                            var result = Ext.util.JSON.decode(responseText);
                          
                          if (result.success) {
                              Pesquisar();
                          } else {
                               Ext.Msg.show({
								    title: "Mensagem de Erro",
								    msg: result.mensagem,
								    buttons: Ext.Msg.OK,
								    icon: Ext.MessageBox.ERROR,
								    width: 300,
								    minWidth: 250
							    });
                          }
		               }
		           }));
                          
		           windowPesagens.close();
		       }
		   }
        ]
	});

    function MudaCor(row, index) {

        if (row.data.Complementado || row.data.DataComplemento != null) {
            return 'corRed';
        }
    }

	/**********************************************************
	FORM DETALHES FLUXO - FIM
	**********************************************************/
    formPesagens.render("div-form-detalhe-pesagens");

</script>
