﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Interna.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content runat="server" ID="HeadContent" ContentPlaceHolderID="HeadContent">
    <script type="text/javascript">

    var formModal = null;
    var grid = null;
	var arraySelectItens = new Array();
	var lastRowSelected = -1;
	var executarPesquisa = false;

	var selectModelVagao = new Ext.grid.CheckboxSelectionModel();

    function FormSuccess(form, action) {
        ds.removeAll();
        ds.loadData(action.result, true);
    }

    function Pesquisar() {
        arraySelectItens = new Array();
        executarPesquisa = true;
        grid.getStore().load();
        /*
        if (grid.getStore().data.length > 0) {
              Ext.Msg.show({
				msg: "Nenhum Registro Encontrado",
				buttons: Ext.Msg.OK,
				icon: Ext.MessageBox.ERROR,
				minWidth: 200
			});
        }*/
        


        var allStores = [ds],
            len = allStores.length,
            loadedStores = 0,
            i = 0;

        for (; i < len; ++i) {
            allStores[i].on('load', check, null, {single: true});
        }

        function check() {
            if (++loadedStores === len) {
                AllStoresLoaded();
            }
        }

        function AllStoresLoaded() {
             if (ds.data.length == 0) {
                    Ext.Msg.show({
				    msg: "Nenhum Registro Encontrado",
				    buttons: Ext.Msg.OK,
				    icon: Ext.MessageBox.ERROR,
				    minWidth: 200
			    });
            }
        }
    }

    function FormError(form, action) {
            Ext.Msg.show({
				title: "Mensagem de Erro",
				msg: action.result.Message,
				buttons: Ext.Msg.OK,
				icon: Ext.MessageBox.ERROR,
				minWidth: 200
			});

        ds.removeAll();
    } 

    var ds = new Ext.data.JsonStore({
        url: '<%= Url.Action("ObterVagoes") %>',
        root: "Items",
        totalProperty: 'Total',
				remoteSort: true,
                paramNames: {
				        sort: "pagination.Sort",
				        dir: "pagination.Dir",
				        start: "pagination.Start",
				        limit: "pagination.Limit"
		        },
                fields: [
                    'Id',
                    'Codigo',
                    'PesoTara',
                    'PesoTotal'
			    ]
    });
   
/* ------------------------------------------------------- */

    Contains = function(arr, obj) {
        for (var i = 0; i < arr.length; i++) {
            if (arr[i] === obj) {
                return true;
            }
        }
        return false;
    }

    $(function () {

        var editarTaraAction = new Ext.ux.grid.RowActions({
            dataIndex: '',
            header: '',
            align: 'center',
            actions: [
		{
		    iconCls: 'icon-detail',
		    tooltip: 'Selecionar pesagens para cálculo da mediana'
		}],
            callbacks: {
                'icon-detail': function (grid, record, action, row, col) {

                    windowPesagens = new Ext.Window({
                        id: 'windowPesagens',
                        title: 'Detalhes das Pesagens',
                        modal: true,
                        width: 500,
                        height: 350,
                        autoLoad: {
                            url: '<%= Url.Action("DetalhePesagens") %>',
                            params: { codVagao: record.data.Codigo },
                            scripts: true
                        }

                    });
                    windowPesagens.show();
                    // alert(record.data.Codigo);
                }
            }
        });

        var pagingToolbar = new Ext.PagingToolbar({
            pageSize: 50,
            store: ds,
            displayInfo: true,
            displayMsg: App.Resources.Web.GridExibindoRegistros,
            emptyMsg: App.Resources.Web.GridSemRegistros,
            paramNames: {
                start: "pagination.Start",
                limit: "pagination.Limit"
            }
        });

        grid = new Ext.grid.EditorGridPanel({
            viewConfig: {
                forceFit: true,
                getRowClass: MudaCor
            },
            sm: selectModelVagao,
            id: 'grdDados',
            stripeRows: true,
            region: 'center',
            autoLoadGrid: true,
            store: ds,
            loadMask: { msg: App.Resources.Web.Carregando },
            colModel: new Ext.grid.ColumnModel({
                defaults: {
                    sortable: false
                },
                columns: [

                        { dataIndex: "Id", hidden: true },
                        selectModelVagao,
                        editarTaraAction,
						{ header: 'Codigo', dataIndex: "Codigo", sortable: false },
						{ header: 'Tara Cadastrada', dataIndex: "PesoTara", sortable: false },
                        { header: 'Tara (Mediana)', dataIndex: "PesoTotal", sortable: false }
					]
            }),
            
            plugins: [editarTaraAction],
            columnLines: true,
            bbar: pagingToolbar,
            buttonAlign: 'center'
        });
     
        grid.getStore().proxy.on('beforeload', function (p, params) {
        
            params['filter[0].Campo'] = 'local';
            params['filter[0].Valor'] = Ext.getCmp("filtro-local").getValue();
            params['filter[0].FormaPesquisa'] = 'Start';

            params['filter[1].Campo'] = 'vagoes';
            params['filter[1].Valor'] = Ext.getCmp("filtro-vagoes").getRawValue();
            params['filter[1].FormaPesquisa'] = 'Start';

        });
         
        function MudaCor(row, index) {

            if (row.data.Complementado || row.data.DataComplemento != null) {
                return 'corRed';
            }
        }
       
        var local = {
            xtype: 'textfield',
            vtype: 'cteestacaovtype',
            id: 'filtro-local',
            fieldLabel: 'Local',
            name: 'local',
            style: { textTransform: "uppercase" },
            allowBlank: true,
            maxLength: 3,
            width: 50,
            hiddenName: 'local',
            autoCreate: { tag: 'input', type: 'text', maxlength: '3', autocomplete: 'off' }
        };

        var vagoes = {
            xtype: 'textfield',
            style: 'text-transform: uppercase',
            id: 'filtro-vagoes',
            fieldLabel: 'Vagoes',
            name: 'vagoes',
            allowBlank: true,
            autoCreate: { tag: 'input', type: 'text', maxlength: '100', autocomplete: 'off' },
            maxLength: 300,
            width: 500,
            hiddenName: 'vagoes'
        };

        var linha1col1 = {
            width: 120,
            layout: 'form',
            border: false,
            items:
				[local]
        };

        var linha1col2 = {
            width: 600,
            layout: 'form',
            border: false,
            items: [vagoes]
        };


        var arrlinha1 = {
            layout: 'column',
            border: false,
            items: [linha1col1, linha1col2]
        };

        var arrCampos = new Array();
        arrCampos.push(arrlinha1);

        filters = new Ext.form.FormPanel({
            id: 'grid-filtros',
            title: "Filtros",
            region: 'center',
            bodyStyle: 'padding: 15px',
            labelAlign: 'top',
            items:
			[arrCampos],
            buttonAlign: "center",
            buttons:
	        [
                {
                    text: 'Pesquisar',
                    type: 'submit',
                    iconCls: 'icon-find',
                    handler: function (b, e) {
                        if (Ext.getCmp("filtro-local").getValue() == "" && Ext.getCmp("filtro-vagoes").getValue() == "") {
                            Ext.Msg.alert('', 'Informar valores para pesquisa');
                        } else {
                            Pesquisar();
                        }
                    }
                    
                },
	            {
	                text: 'Limpar',
	                handler: function (b, e) {
	                    arraySelectItens = new Array();
	                    grid.getStore().removeAll();
	                    grid.getStore().totalLength = 0;
	                    grid.getBottomToolbar().bind(grid.getStore());
	                    grid.getBottomToolbar().updateInfo();
	                    Ext.getCmp("grid-filtros").getForm().reset();

	                },
	                scope: this
	            },
	            {
	                text: 'Salvar',
	                handler: function (b, e) {
                    var idvagoes = "";
	                    
		            var listaSelecionados = selectModelVagao.getSelections();
                    for (var i = 0; i < listaSelecionados.length; i++) 
                    {
                        idvagoes += parseFloat(listaSelecionados[i].data.Id + ",");
                    }


                            if (Ext.Msg.confirm("Confirmação", "Você deseja realmente alterar a tara dos vagões selecionados?", function(btn, text) {
                            if (btn == 'yes') {
                            $.ajax({
                            url: "<%= Url.Action("SalvarTarasAlteradas") %>" + "?idvagoes=" + idvagoes,
                            type: "POST", 
                            dataType: 'json',
                            success: function(result) {
                            if (result.success) {
                            Pesquisar();
							                alert('Valores alterados com sucesso');
							            }
							            else {
								            alert('erro');
							            }
						            },
						            failure: function(result) {
							            Ext.Msg.show({
								            title: "Mensagem de Erro",
								            msg: result.Message,
								            buttons: Ext.Msg.OK,
								            icon: Ext.MessageBox.ERROR,
								            width: 300,
								            minWidth: 250
							            });
						            }
					            });
	                        }
	                    }));
	                },
	                scope: this
	            }
            ]
        });

        new Ext.Viewport({
            layout: 'border',
            margins: 10,
            items: [
			{
			    region: 'north',
			    height: 230,
			    items: [{
			        region: 'center',
			        applyTo: 'header-content'
			    },
				filters]
			},
			grid
		]
        });
    });
    </script>
    <style>
        .corRed
        {
            background-color: #FFEEDD;
        }
        .corOrange
        {
            background-color: #FFC58A;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div id="header-content">
        <h1>
            Vagão Tara Mediana</h1>
        <small>Você está em Veículo > Vagão Tara Mediana</small>
        <br />
    </div>
</asp:Content>