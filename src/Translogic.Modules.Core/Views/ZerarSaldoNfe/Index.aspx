﻿<%@ Page Title="" Language="C#" Inherits="System.Web.Mvc.ViewPage" MasterPageFile="~/Views/Shared/Interna.master" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server">
<% Html.RenderPartial("Autorizacao"); %>

<style type="text/css">
    .x-grid3-cell-inner, .x-grid3-hd-inner {
        white-space: normal; 
    }
</style>

	<script type="text/javascript">
    // **************************** TELA ********************************//
    var verificadorFaltantes;
    var windowZerarAutorizacao = null;
    var bCancelouZerarSaldoNfe;
    var autorizadoPorEstorno = "";
    var justificativaEstorno = "";
    
    var storeNfeBaixando = new Ext.data.JsonStore({
        id: 'storeNfeBaixando',
        name: 'storeNfeBaixando',
        url: '<%=Url.Action("Zerar") %>',
        fields: ['Chave', 'Mensagem', 'Status']
    });

    var txtChavesParaBaixar = new Ext.form.TextArea ({
        fieldLabel: 'Informe as chaves das NFes separados por ; ou linhas',
        maxLength: 6000,
        width: 320,
        height: 280,
        name: 'txtChavesParaBaixar',
        id: 'txtChavesParaBaixar',
        allowBlank: false
    });

    var gridNfeBaixando = new Ext.grid.GridPanel({
		height: 360,
		width: 670,
		stripeRows: true,
		region: 'center',
		store: storeNfeBaixando,
		autoScroll: true,
		colModel: new Ext.grid.ColumnModel({
			columns: [
				new Ext.grid.RowNumberer(),
				{ header: 'NFes', dataIndex: "Chave", sortable: false, width: 300 },
			    { header: 'Status', dataIndex: "Status", sortable: false, width: 25, renderer: StatusRenderer },
			    { header: 'Erro', dataIndex: "Mensagem", sortable: false, width: 290 }
			]
		})
	});
    
    gridNfeBaixando.getStore().proxy.on('beforeload', function (p, params) {
        params['chaves'] = Ext.getCmp("txtChavesParaBaixar").getValue();
        params['autorizadopor'] = autorizadoPorEstorno;
        params['justificativa'] = justificativaEstorno;
    });
    
    function StatusRenderer(val)
    {
        switch (val) {
            case "OK":
                return "<img src='<%=Url.Images("Icons/tick.png") %>' >";
            case "ERRO":
                return "<img src='<%=Url.Images("Icons/cross.gif") %>' >";
            default:
                return "<img src='<%=Url.Images("loading.gif") %>' >";
        }
    }
    
    function LerErrosDaGrid() {
        var result = [];
        
        gridNfeBaixando.getStore().each(function (record) {
            if (record.data.Mensagem != null && record.data.Mensagem != "")
                result.push(record.data.Chave + "|" +  record.data.Mensagem);
        });

        return result;
    }

    var btnZerarNfes = {
        xtype: "button",
        name: 'btnZerarNfes',
        id: 'btnZerarNfes',
        text: 'Zerar NFes',
        width: 100,
        handler: function (b, e) {
            fctZerarNfes();
        }
    };
    
    var btnLimparTelaBaixarNfes = {
        xtype: "button",
        name: 'btnLimparTelaBaixarNfes',
        id: 'btnLimparTelaBaixarNfes',
        text: 'Limpar',
        width: 100,
        handler: LimparTelaBaixarNfes
    };
    
    function Ajax(url, params, funcao) {
	    Ext.Ajax.request(
        {
            url: url,
            params: params,
            timeout: 300000,
            failure: function(conn, data){
                Ext.Msg.show({
                    title: 'Ops...',
                    msg: "Ocorreu um erro inesperado.",
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.ERROR
                });
			},
            success: function (response) {
                var result = Ext.decode(response.responseText);
                 
                if (result.success == false){
                    Ext.Msg.show({
                        title: 'Ops...',
                        msg: result.Message,
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.ERROR
                    });
                }
                else {
                    funcao(result);
                }
            }
        });
    }

    function fctZerarNfes() {           
        if (Ext.Msg.confirm("Alteração", "Sua matrícula está sendo vinculada na auditoria da alteração dessa NFe! Deseja prosseguir com a alteração?", function(btn, text) {
			if (btn == 'yes') {
				fctCarregarTelaAutorizacao();
			}
		}));
    }

    function fctCarregarTelaAutorizacao() {            
        windowZerarAutorizacao = null;

        windowZerarAutorizacao = new Ext.Window({
            name: 'windowZerarAutorizacao',
            id: 'windowZerarAutorizacao',
            title: "Zerar Saldo de NFes",
            expandonShow: true,
            modal: true,
            width: 400,
            height: 500,
            closable: true,
            closeAction: 'hide',
                items: [painelAutorizadoPor, painelJustificativa],
                listeners: {
                    'beforeshow': function() {
                        Ext.getCmp("txtAutorizadoPor").setValue("");
                        Ext.getCmp("txtJustificativa").setValue("");
                    },
                    'hide': function() {
                        bCancelouZerarSaldoNfe = flagCancelouZerarSaldo;
                        if (bCancelouZerarSaldoNfe == false) {
                            // Recuperar o campo Autorizado Por e a Justificativa
                            autorizadoPorEstorno = Ext.getCmp("txtAutorizadoPor").getValue();
                            justificativaEstorno = Ext.getCmp("txtJustificativa").getValue();
                            
                            Ext.getCmp("txtChavesParaBaixar").disable();
                            Ext.getCmp("btnZerarNfes").disable();
                            gridNfeBaixando.getStore().load();
                        }
                    }
                }
        });

        windowZerarAutorizacao.show();
    }

    /*function ProcessarNotas() {
        Ext.getCmp("txtChavesParaBaixar").disable();
        Ext.getCmp("btnZerarNfes").disable();
        
        gridNfeBaixando.getStore().load();
    }*/

    function LimparTelaBaixarNfes() {
        Ext.getCmp("txtChavesParaBaixar").setValue("");
        Ext.getCmp("txtChavesParaBaixar").enable();
        Ext.getCmp("btnZerarNfes").enable();
        gridNfeBaixando.getStore().removeAll();
    }
    
    var epacamento = { xtype: 'label', html: '&nbsp;&nbsp;' };
    
    var c1BaixarNfe = {
        layout: 'form',
        bodyStyle: 'padding: 5px',
        border: false,
        items: [txtChavesParaBaixar]
    };
    
    var lBotoesBaixarNfe = {
	    layout: 'column',
        bodyStyle: 'padding: 5px',
	    border: false,
	    items: [btnZerarNfes, epacamento, btnLimparTelaBaixarNfes]
	};
    
    var fsC1BaixarNfe = {
        xtype: 'fieldset',
        layout: 'form',
        title: 'Chaves das NFes que serão zeradas',
        width: 350,
        items: [c1BaixarNfe, lBotoesBaixarNfe]
    };
    
    var c2BaixarNfe = {
	    layout: 'form',
	    border: false,
	    bodyStyle: 'padding: 5px',
	    items: [gridNfeBaixando]
	};
    
    var fsC2BaixarNfe = {
        xtype: 'fieldset',
	    layout: 'form',
        title: 'Situação das NFes zeradas',
        width: 680,
	    items: [c2BaixarNfe]
	};
    
    var l1BaixarNfe = {
	    layout: 'column',
	    border: false,
	    items: [fsC1BaixarNfe, epacamento, fsC2BaixarNfe]
	};
    
    /******************************************************************/
    
    var filtros = new Ext.form.FormPanel({
        id: 'panel-notas',
        region: 'center',
        bodyStyle: 'padding: 5px',
        labelAlign: 'top',
        width: 1020,
        items: [l1BaixarNfe],
        buttonAlign: "center"          
    });

	//***************************** RENDER *****************************//
	Ext.onReady(function () {
	    filtros.render("divContent");
	});
		
	</script>
</asp:Content>
<asp:Content ID="MainContent" ContentPlaceHolderID="Content" runat="server">
	<div id="header-content">
		<h1>
			CTe - Zerar Saldo da NFe</h1>
		<small>Você está em CTe > Zerar Saldo da NFe</small>
		<br />
	</div>
	<div id="divContent">
	</div>
</asp:Content>
