﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div id="div-form-autorizacao-zerar-saldo-nfe" style="display: none;">
</div>
<script type="text/javascript">

    var flagCancelouZerarSaldo = true;

    var txtAutorizadoPor = {
        xtype: 'textfield',
        id: 'txtAutorizadoPor',
        name: 'txtAutorizadoPor',
        typeAhead: true,
        fieldLabel: 'Autorizado por',
        width: 280,
        enableKeyEvents: true,
        allowBlank: false,
        listeners: {
            change: function () {
                //
            }
        }
    };

    var coluna1Linha1 = {
        layout: 'form',
        border: false,
        bodyStyle: 'padding-right:10px',
        items: [txtAutorizadoPor]
    };

    var linha1 = {
        layout: 'column',
        border: false,
        items: [coluna1Linha1]
    };

    var painelAutorizadoPor = new Ext.form.FormPanel({
        id: 'painelAutorizadoPor',
        title: '',
        name: 'painelAutorizadoPor',
        bodyStyle: 'padding: 10px',
        labelAlign: 'top',
        items: [linha1],
        buttonAlign: 'center'
    });

    var txtJustificativa = new Ext.form.TextArea({
        fieldLabel: 'Informe a justificativa de zerar o saldo da nfe',
        maxLength: 1000,
        width: 320,
        height: 280,
        name: 'txtJustificativa',
        id: 'txtJustificativa',
        allowBlank: false
    });

    var coluna1Linha2 = {
        layout: 'form',
        border: false,
        bodyStyle: 'padding-right:10px',
        items: [txtJustificativa]
    };

    var linha2 = {
        layout: 'column',
        border: false,
        items: [coluna1Linha2]
    };

    var btnSalvarAutorizacao = {
        name: 'btnSalvarAutorizacao',
        id: 'btnSalvarAutorizacao',
        text: 'Salvar',
        handler: function () {
            var autorizadoPorInformado = Ext.getCmp("txtAutorizadoPor").getValue();
            if (autorizadoPorInformado == "") {
                Ext.Msg.show({
                    title: 'Zerar Saldo',
                    msg: 'O campo "Autorizado Por" é obrigatório!',
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.ERROR
                });
                return false;
            }
            
            var justificativaInformada = Ext.getCmp("txtJustificativa").getValue();
            if (justificativaInformada == "") {
                Ext.Msg.show({
                    title: 'Zerar Saldo',
                    msg: 'O campo "Justificativa" é obrigatório e deve conter no mínimo 15 caracteres!',
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.ERROR
                });
                return false;
            } else {
                if (justificativaInformada.length < 15) {
                    Ext.Msg.show({
                        title: 'Zerar Saldo',
                        msg: 'O campo "Justificativa" é obrigatório e deve conter no mínimo 15 caracteres!',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.ERROR
                    });
                    return false;
                }
            }

            flagCancelouZerarSaldo = false;
            windowZerarAutorizacao.hide();
        }
    };

    var btnCancelarAutorizacao = {
        name: 'btnCancelarAutorizacao',
        id: 'btnCancelarAutorizacao',
        text: 'Cancelar',
        handler: function () {
            flagCancelouZerarSaldo = true;
            windowZerarAutorizacao.hide();
        }
    };

    var painelJustificativa = new Ext.form.FormPanel({
        id: 'painelJustificativa',
        title: 'Justificativa',
        name: 'painelJustificativa',
        bodyStyle: 'padding: 10px',
        labelAlign: 'top',
        items: [linha2],
        buttonAlign: 'center',
        buttons: [btnSalvarAutorizacao, btnCancelarAutorizacao]
    });

    /******************************* RENDER *******************************/
    Ext.onReady(function () {
        painelAutorizadoPor.render("div-form-autorizacao-zerar-saldo-nfe");
        painelJustificativa.render("div-form-autorizacao-zerar-saldo-nfe");
    });

    
</script>