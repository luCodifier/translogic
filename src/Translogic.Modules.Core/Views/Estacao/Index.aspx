<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Interna.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server">

    <script type="text/javascript">
    	$(function()
    	{
    		this.action = new Ext.ux.grid.RowActions({
    			dataIndex: "",
    			actions: [{
    				iconCls: 'icon-del',
    				tooltip: 'dd'
    			}, {
    				iconCls: 'icon-edit',
    				tooltip: 'Editar'
				}],
				callbacks: {
					'icon-del': function(grid, record, action, row, col)
					{
						/*
						console.info(action);
						console.info(record.data);
						*/
					},
					'icon-edit': function(grid, record, action, row, col)
					{
						/*
						console.info(action);
						console.info(record.data);
						*/
					}
				}
    		});

    			var grid = new Translogic.PaginatedGrid({
    				width: 800,
    				height: 400,
    				url: '<%= Url.Action("GetEstacoes") %>',
    				fields: [
						'Id',
						'Altitude',
						'Codigo',
						'Direcao',
						'Km',
						'Latitude',
						'Longitude',
						'PontosNotaveis',
						'Raio',
						'SBDesviadaDireita',
						'SBDesviadaEsquerda',
						'SubdivisaoLinhaDesviada',
						'SubdivisaoLinhaPrincipal',
						'TempoDescida',
						'TempoSubida',
						'VelocidadeCrescente',
						'VelocidadeDecrescente'
    				],
    				columns: [
						new Ext.grid.RowNumberer(),
						{ id: "Id", header: "Id", sortable: true, width: 70, dataIndex: "Id" },
						{ header: 'Altitude', sortable: true, width: 70, dataIndex: "Altitude" },
						{ header: 'Direcao', sortable: true, width: 70, dataIndex: "Direcao" },
						{ header: 'Km', sortable: true, width: 70, dataIndex: "Km" },
						{ header: 'Latitude', sortable: true, width: 70, dataIndex: "Latitude" },
						{ header: 'Longitude', sortable: true, width: 70, dataIndex: "Longitude" },
						{ header: 'PontosNotaveis', sortable: true, width: 70, dataIndex: "PontosNotaveis" },
						{ header: 'Raio', sortable: true, width: 70, dataIndex: "Raio" },
						{ header: 'Direita', sortable: true, width: 70, dataIndex: "SBDesviadaDireita" },
						{ header: 'Esquerda', sortable: true, width: 70, dataIndex: "SBDesviadaEsquerda" },
						{ header: 'Desviada', sortable: true, width: 70, dataIndex: "SubdivisaoLinhaDesviada" },
						{ header: 'Principal', sortable: true, width: 70, dataIndex: "SubdivisaoLinhaPrincipal" },
						{ header: 'Descida', sortable: true, width: 70, dataIndex: "TempoDescida" },
						{ header: 'Subida', sortable: true, width: 70, dataIndex: "TempoSubida" },
						{ header: 'Crescente', sortable: true, width: 70, dataIndex: "VelocidadeCrescente" },
						{ header: 'Decrescente', sortable: true, width: 70, dataIndex: "VelocidadeDecrescente" },
						this.action
					],
					plugins: [
						this.action,
						new Ext.ux.plugins.GroupHeaderGrid({
							rows: [
								[
									{},
									{},
									{ header: 'Localizacao', colspan: 5, align: 'center', dataIndex: 'Id' },
									{ header: 'Outros', colspan: 2, align: 'center', dataIndex: 'Id' },
									{ header: 'Desviada', colspan: 2, align: 'center', dataIndex: 'Id' },
									{ header: 'Subdivisao Linha', colspan: 2, align: 'center', dataIndex: 'Id' },
									{ header: 'Tempo', colspan: 2, align: 'center', dataIndex: 'Id' },
									{ header: 'Velocidade', colspan: 2, align: 'center', dataIndex: 'Id' },
									{}
								]
							],
							hierarchicalColMenu: true
						})
					]
    			});

    			grid.render('grid-example');
    		});
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">

	<h1>Ext.JS</h1>
	<div id="grid-example"></div>

</asp:Content>