﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<style type="text/css">
</style>

<div style="padding:10px 10px 10px 10px; margin-top:10px">
    <%--<p>CONSULTA POR TREM - Ficha de Recomendações</p>--%>
    <table cellpadding="0" cellspacing="0" style="font-size: 11px; border-top-width: 1px; border-left-width: 1px; border-left-style: solid; border-left-color: #000000; border-top-style: solid; border-top-color: #000000; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #000000; border-right-width: 1px; border-right-style: solid; border-right-color: #000000; width: 100%;">
                    <tr>
                <td colspan="14">
                    <span style="font-weight: bold;">CONSULTA POR TREM - Ficha de Recomendações</span>
                </td>
            </tr>
        <tr>
            <td colspan="4" class="style15" style="border-top-width: 1px; border-top-style: solid; border-top-color: #000000; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #000000; border-left-width: 1px; border-left-style: solid; border-left-color: #000000;">
                <span style="font-weight: bold;">Trem: </span>
                <%=ViewData["Trem"]%>
                &nbsp;
            </td>
            <td colspan="2" class="style1" style="border-top-width: 1px; border-top-style: solid; border-top-color: #000000; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #000000; border-left-width: 1px; border-left-style: solid; border-left-color: #000000;">
                <span style="font-weight: bold;">OS: </span>
                <%=ViewData["OS"]%>
                &nbsp;
            </td>
            <td colspan="7" class="style15" style="border-left-width: 1px; border-left-style: solid; border-left-color: #000000; border-top-width: 1px; border-top-style: solid; border-top-color: #000000; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #000000;">
                <span style="font-weight: bold;">Data/Hora Relatório: </span>
                <%=ViewData["Data"]%>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
                    <tr  class = "styleColunaCabecalho" style="font-weight: bold; text-align: center;">
                        <th style="border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #000000; border-top-width: 1px; border-top-style: solid; border-top-color: #000000;">
                            SEQ
                        </th>
                        <th style="border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #000000; border-left-width: 1px; border-left-style: solid; border-left-color: #000000; border-top-width: 1px; border-top-style: solid; border-top-color: #000000;">
                            ID_TREM
                        </th>
                        <th style="border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #000000; border-left-width: 1px; border-left-style: solid; border-left-color: #000000; border-top-width: 1px; border-top-style: solid; border-top-color: #000000;">
                            TREM
                        </th>
                        <th style="border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #000000; border-left-width: 1px; border-left-style: solid; border-left-color: #000000; border-top-width: 1px; border-top-style: solid; border-top-color: #000000;">
                            OS
                        </th>
                        <th style="border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #000000; border-left-width: 1px; border-left-style: solid; border-left-color: #000000; border-top-width: 1px; border-top-style: solid; border-top-color: #000000;">
                            VAGAO
                        </th>
                        <th style="border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #000000; border-left-width: 1px; border-left-style: solid; border-left-color: #000000; border-top-width: 1px; border-top-style: solid; border-top-color: #000000;">
                            RECOMENDAÇÃO
                        </th>
                        <th style="border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #000000; border-left-width: 1px; border-left-style: solid; border-left-color: #000000; border-top-width: 1px; border-top-style: solid; border-top-color: #000000;">
                            AVARIA FREIO
                        </th>
                        <th style="border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #000000; border-left-width: 1px; border-left-style: solid; border-left-color: #000000; border-top-width: 1px; border-top-style: solid; border-top-color: #000000;">
                            ORI
                        </th>
                        <th style="border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #000000; border-left-width: 1px; border-left-style: solid; border-left-color: #000000; border-top-width: 1px; border-top-style: solid; border-top-color: #000000;">
                            DST
                        </th>
                        <th style="border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #000000; border-left-width: 1px; border-left-style: solid; border-left-color: #000000; border-top-width: 1px; border-top-style: solid; border-top-color: #000000;">
                            MOTIVO RECOMENDAÇÃO
                        </th>
                        <th style="border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #000000; border-left-width: 1px; border-left-style: solid; border-left-color: #000000; border-top-width: 1px; border-top-style: solid; border-top-color: #000000;">
                            FREIO_SIT
                        </th>
                    </tr>
                    <%
                        var list = (List<Translogic.Modules.Core.Domain.Model.Dto.RelatorioFichaRecomendacaoExportDto>)ViewData["ListaRelatorioFichaRecomendacao"];

                        foreach (var item in list)
                        {%>
                        <% if (item.Recomendacao.Equals("Rec Oficina"))
                           { %>
                    <tr>
                        <td style="border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #9C9C9C; font-weight:bold;">
                            <%=item.SEQ%>
                        </td>
                        <td style="border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #9C9C9C; border-left-width: 1px; border-left-style: solid; border-left-color: #000000; font-weight:bold;">
                            <%=item.IdTrem%>
                        </td>
                        <td style="border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #9C9C9C; border-left-width: 1px; border-left-style: solid; border-left-color: #000000; font-weight:bold;">
                            <%=item.Trem%>
                        </td>
                        <td style="border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #9C9C9C; border-left-width: 1px; border-left-style: solid; border-left-color: #000000; font-weight:bold;">
                            <%=item.OS%>
                        </td>
                        <td style="border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #9C9C9C; border-left-width: 1px; border-left-style: solid; border-left-color: #000000; font-weight:bold;">
                            <%=item.Vagao%>
                        </td>
                        <td style="border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #9C9C9C; border-left-width: 1px; border-left-style: solid; border-left-color: #000000; font-weight:bold;">
                            <%=item.Recomendacao%>
                        </td>
                        <td style="border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #9C9C9C; border-left-width: 1px; border-left-style: solid; border-left-color: #000000; font-weight:bold;">
                            <%=item.AvariaFreio%>
                        </td>
                        <td style="border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #9C9C9C; border-left-width: 1px; border-left-style: solid; border-left-color: #000000; text-align:left; font-weight:bold;">
                            <%=item.Origem%>
                        </td>
                        <td style="border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #9C9C9C; border-left-width: 1px; border-left-style: solid; border-left-color: #000000; text-align:left; font-weight:bold;">
                            <%=item.Destino%>
                        </td>
                        <td style="border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #9C9C9C; border-left-width: 1px; border-left-style: solid; border-left-color: #000000; font-weight:bold;">
                            <%=item.MotivoRecomendacao%>
                        </td>
                         <%if (item.FreioSituacao == "RUIM")
                           {%>
                        <td style="border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #9C9C9C; border-left-width: 1px; border-left-style: solid; border-left-color: #000000; color: Red; font-weight:bold;">
                            <%=item.FreioSituacao%>
                        </td>
                        <%}%>
                        <%else
                           {%>
                        <td style="border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #9C9C9C; border-left-width: 1px; border-left-style: solid; border-left-color: #000000; font-weight:bold;">
                            <%=item.FreioSituacao%>
                        </td>
                        <%}%>
                    </tr>
                    <% } %>
                    <%else
                      {%>
                           <tr>
                        <td style="border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #9C9C9C;">
                            <%=item.SEQ%>
                        </td>
                        <td style="border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #9C9C9C; border-left-width: 1px; border-left-style: solid; border-left-color: #000000;">
                            <%=item.IdTrem%>
                        </td>
                        <td style="border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #9C9C9C; border-left-width: 1px; border-left-style: solid; border-left-color: #000000;">
                            <%=item.Trem%>
                        </td>
                        <td style="border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #9C9C9C; border-left-width: 1px; border-left-style: solid; border-left-color: #000000;">
                            <%=item.OS%>
                        </td>
                        <td style="border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #9C9C9C; border-left-width: 1px; border-left-style: solid; border-left-color: #000000;">
                            <%=item.Vagao%>
                        </td>
                        <td style="border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #9C9C9C; border-left-width: 1px; border-left-style: solid; border-left-color: #000000;">
                            <%=item.Recomendacao%>
                        </td>
                        <td style="border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #9C9C9C; border-left-width: 1px; border-left-style: solid; border-left-color: #000000;">
                            <%=item.AvariaFreio%>
                        </td>
                        <td style="border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #9C9C9C; border-left-width: 1px; border-left-style: solid; border-left-color: #000000; text-align:left;">
                            <%=item.Origem%>
                        </td>
                        <td style="border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #9C9C9C; border-left-width: 1px; border-left-style: solid; border-left-color: #000000; text-align:left;">
                            <%=item.Destino%>
                        </td>
                        <td style="border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #9C9C9C; border-left-width: 1px; border-left-style: solid; border-left-color: #000000;">
                            <%=item.MotivoRecomendacao%>
                        </td>
                         <%if (item.FreioSituacao == "RUIM")
                           {%>
                        <td style="border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #9C9C9C; border-left-width: 1px; border-left-style: solid; border-left-color: #000000; color: Red">
                            <%=item.FreioSituacao%>
                        </td>
                        <%}%>
                        <%else
                           {%>
                        <td style="border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #9C9C9C; border-left-width: 1px; border-left-style: solid; border-left-color: #000000;">
                            <%=item.FreioSituacao%>
                        </td>
                        <%}%>
                    </tr>
                      <%} %>
                   <% } %>   
                </table>
</div>




