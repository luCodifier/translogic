﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Interna.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content runat="server" ID="HeadContent" ContentPlaceHolderID="HeadContent">
    <script type="text/javascript">

        //*****STORES*****
        var gridStore = new Ext.data.JsonStore({
            root: "Items",
            totalProperty: 'Total',
            id: 'gridStore',
            name: 'gridStore',
            proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("ConsultaRelatorioFichaRecomendacao", "RelatorioFichaRecomendacao") %>', timeout: 600000 }),
            paramNames: {
                sort: "detalhesPaginacaoWeb.Sort",
                dir: "detalhesPaginacaoWeb.Dir",
                start: "detalhesPaginacaoWeb.Start",
                limit: "detalhesPaginacaoWeb.Limit"
            },
            fields: ['NumeroOS', 'Prefixo', 'Origem', 'Destino', 'Local', 'DtChegadaPrevista', 'QtdAvaria', 'QtdVagao']
        });

        var avariaStore = new Ext.data.ArrayStore({
            fields: ['Descricao'],
            data: [['Todos'], ['Sim'], ['Não']]
        });

        Ext.onReady(function () {

            var txtTrem = {
                xtype: 'textfield',
                name: 'txtTrem',
                id: 'txtTrem',
                fieldLabel: 'Trem',
                autoCreate: {
                    tag: 'input',
                    type: 'text',
                    autocomplete: 'off',
                    maxlength: '12'
                },
                //maskRe: /[0-9+;]/,
                style: 'text-transform:uppercase;',
                width: 200
            };

            var txtNumOS = {
                xtype: 'textfield',
                name: 'txtNumOS',
                id: 'txtNumOS',
                fieldLabel: 'Nr.OS',
                autoCreate: {
                    tag: 'input',
                    type: 'text',
                    autocomplete: 'off',
                    maxlength: '12'
                },
                maskRe: /^[0-9]+$/,
                style: 'text-transform:uppercase;',
                width: 200
            };

            var ddlAvaria = new Ext.form.ComboBox({
                editable: false,
                typeAhead: true,
                forceSelection: true,
                disableKeyFilter: true,
                triggerAction: 'all',
                lazyRender: true,
                mode: 'local',
                valueField: 'Descricao',
                displayField: 'Descricao',
                fieldLabel: 'Avaria',
                id: 'ddlAvaria',
                name: 'ddlAvaria',
                width: 200,
                store: avariaStore,
                value: 'Todos'
            });

            //***** Form de campos da linha inicial *****//
            var formtxtTrem = {
                width: 210,
                layout: 'form',
                border: false,
                items: [txtTrem]
            };

            var formtxtNumOS = {
                width: 210,
                layout: 'form',
                border: false,
                items: [txtNumOS]
            };

            var formddlAvaria = {
                width: 210,
                layout: 'form',
                border: false,
                items: [ddlAvaria]
            };

            var lineTop = {
                layout: 'column',
                border: false,
                items: [formtxtTrem, formtxtNumOS, formddlAvaria]
            };

            var fieldset = {
                xtype: 'fieldset',
                border: false,
                items: [lineTop]
            };

            //***** GRID *****//

            // Toolbar da Grid
            var pagingToolbar = new Ext.PagingToolbar({
                pageSize: 50,
                store: gridStore,
                displayInfo: true,
                displayMsg: App.Resources.Web.GridExibindoRegistros,
                emptyMsg: App.Resources.Web.GridSemRegistros,
                paramNames: {
                    start: "detalhesPaginacaoWeb.Start",
                    limit: "detalhesPaginacaoWeb.Limit"
                }
            });

            var Actions = new Ext.ux.grid.RowActions({
                id: 'colFind',
                dataIndex: '',
                header: '',
                align: 'center',
                autoWidth: true,
                width: 5,
                actions: [
                            { iconCls: 'icon-find', tooltip: 'Visualizar' }
                         ],
                callbacks: {
                    'icon-find': function (grid, record, action, row, col) {
                        //Chama controller IMPRIMIR PDF
                        ImprimirPDF(record.data.NumeroOS);
                    }
                }
            });

            var cm = new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true
                },
                plugins: Actions,
                columns: [
                    new Ext.grid.RowNumberer({ width: 25 }),
                    Actions,
                    { header: 'Numero OS', dataIndex: "NumeroOS", sortable: false, width: 40 },
                    { header: 'Prefixo', dataIndex: "Prefixo", sortable: false, width: 40 },
                    { header: 'Origem', dataIndex: "Origem", sortable: false, width: 60 },
                    { header: 'Destino', dataIndex: "Destino", sortable: false, width: 60 },
                    { header: 'Local', dataIndex: "Local", sortable: false, width: 40 },
                    { header: 'Chegada Prevista', dataIndex: "DtChegadaPrevista", sortable: false, width: 50 },
                    { header: 'Avarias', dataIndex: "QtdAvaria", sortable: false, width: 50 },
                    { header: 'Vagões', dataIndex: "QtdVagao", sortable: false, width: 50 }
                ]
            });

            var grid = new Ext.grid.EditorGridPanel({
                id: 'grid',
                name: 'grid',
                autoLoadGrid: false,
                limit: 10,
                height: 360,
                width: 870,
                stripeRows: true,
                cm: cm,
                region: 'center',
                viewConfig: {
                    forceFit: true,
                    emptyText: 'Nenhum Registro encontrado.'
                },
                autoScroll: true,
                loadMask: { msg: App.Resources.Web.Carregando },
                store: gridStore,
                plugins: [Actions],
                bbar: pagingToolbar
            });

            grid.getStore().proxy.on('beforeload', function (p, params) {
                var obj = GeraObjeto();
                params['trem'] = obj.trem;
                params['numOS'] = obj.numOS;
                params['avaria'] = obj.avaria;
            });

            var painel = new Ext.form.FormPanel({
                id: 'painel',
                layout: 'form',
                labelAlign: 'top',
                standardSubmit: true,
                url: '<%= Url.Action("upload", "Tfa") %>',
                border: false,
                autoHeight: true,
                buttonAlign: "center",
                title: "",
                bodyStyle: 'padding: 15px',
                items: [fieldset, grid],
                buttons:
                [{
                    id: 'btnPesquisar',
                    text: 'Pesquisar',
                    tooltip: 'Pesquisar',
                    region: 'south',
                    iconCls: 'icon-find',
                    handler: function (c) {
                        Pesquisar();
                    }
                }]
            });

            new Ext.Viewport({
                layout: 'border',
                margins: 10,
                items: [
                    {
                        region: 'north',
                        height: 190,
                        autoScroll: true,
                        items: [{
                            region: 'center',
                            applyTo: 'header-content'
                        },
                            painel]
                    },
                    grid
                ]
            });

            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////// *****JAVASCRIPT*****//////////////////////////////////////////////////////////////
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            function Pesquisar() {
                if (ValidaCamposObrigatorios()) {
                    gridStore.load({ params: { start: 0, limit: 50} });
                }
            }

            function ValidaCamposObrigatorios() {

                var obj = GeraObjeto();

                //R1
                if (obj.trem == "" && obj.numOS == "") {
                    ExtAlert("É necessário informar o Trem ou a OS", "ERRO");
                    return false;
                }
                return true;
            }

            function GeraObjeto() {

                var obj = new Object();
                obj.trem = Ext.getCmp("txtTrem").getValue();
                obj.numOS = Ext.getCmp("txtNumOS").getValue();
                /*DDLS*/
                obj.avaria = Ext.getCmp("ddlAvaria").getRawValue();
                return obj;

            }

            function ExtAlert(msg, QUESTAOERRO) {

                var tipoMensagemAux = QUESTAOERRO.toUpperCase();

                switch (tipoMensagemAux) {
                    case "QUESTAO":
                        Ext.Msg.show
                            ({
                                title: "Aviso",
                                msg: msg,
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.QUESTION,
                                minWidth: 200
                            });
                        break;
                    case "ERRO":
                        Ext.Msg.show
                            ({
                                title: 'Aviso',
                                msg: msg,
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.ERROR
                            });
                        break;
                    default:
                }
            }

            function ImprimirPDF(numOS) {
                Ext.getBody().mask("Processando dados...", "x-mask-loading");

                // Busca idOS linha selecionada
                //var idOs = BuscaIDOrdemServicoSelecionado();

                //var url = '<%= Url.Action("Imprimir", "RelatorioFichaRecomendacao") %>?numOS=1';
                var url = '<%= Url.Action("Imprimir", "RelatorioFichaRecomendacao") %>?numOS=' + numOS;
                window.open(url, "");

                Ext.getBody().unmask();

            }
        });
    </script>
</asp:Content>
<asp:Content runat="server" ID="Content2" ContentPlaceHolderID="Content">
    <div id="header-content">
        <h1>
            Relatorio Ficha Recomendação</h1>
        <br />
        <small>Você está em Consultas > Trem > Trens Recomendados</small>
        <p>
        </p>
    </div>
   
</asp:Content>
