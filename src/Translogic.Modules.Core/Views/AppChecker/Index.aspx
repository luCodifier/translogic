<%@ Import Namespace="System.Threading"%>
<%@ Page Title="AppChecker" Language="C#" MasterPageFile="~/Views/Shared/Interna.Master" Inherits="Translogic.Modules.Core.Views.AppChecker.Index" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server">
	<style>
		h1 
		{
			font-size: 20px;
		}
		strong 
		{
			font-weight: bold;
		}
	</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
	<div style="padding: 10px;">
		<h1>Base de Dados 
		<%if (ViewData.Model.DatabaseIsOk){ %>
			<span style="color: Green; font-weight: bolder;">OK</span>
			<%}else{ %>
			<span style="color: Red; font-weight: bolder;">Com Problemas</span>
			<%}%>
		</h1>
		<ul style="margin: 10px;">
			<li><strong>ConnectionString</strong>: <%= ViewData.Model.ConnectionString %></li>
		</ul>
		
		<h1>M�dulos</h1>
		<ul style="margin: 10px;">
			<%foreach (var module in ViewData.Model.Modules){%>
				<li style="padding: 5px 0;">
					<strong><%= module.Name %></strong><br />
					<%= module %>
				</li>
			<%} %>
		</ul>
		
		<h1>Cultura</h1>
		<ul style="margin: 10px;">
			<li style="padding: 5px 0;">
				<strong>Current Culture</strong>:<%= string.Format("{0} ({1})", Thread.CurrentThread.CurrentCulture.Name, Thread.CurrentThread.CurrentCulture.LCID)%>
			</li>
			<li style="padding: 5px 0;">
				<strong>Current UI Culture</strong>:<%= string.Format("{0} ({1})", Thread.CurrentThread.CurrentUICulture.Name, Thread.CurrentThread.CurrentUICulture.LCID)%>
			</li>
			<li style="padding: 5px 0;">
				<strong>Date Time</strong>: <%= DateTime.Now %>
			</li>
			<li style="padding: 5px 0;">
				<strong>Currency</strong>: <%= string.Format("{0:c}", 12345.6789)%>
			</li>
			<li style="padding: 5px 0;">
				<strong>Culturas dispon�veis</strong>: <%
              foreach (var cultura in ViewData.Model.Culturas)
              {
              	%><%= string.Format("{0} ({1}) / ", cultura.Name, cultura.DisplayName) %><%
              } %>
				
			</li>
		</ul>
		
		<h1>Server Variables</h1>
		<ul style="margin: 10px;">
			<%foreach (string variable in Request.ServerVariables){%>
				<li style="padding: 5px 0;">
					<strong><%= variable %></strong>:<%= Request.ServerVariables[variable] %>
				</li>
			<%} %>
		</ul>
		
	</div>
</asp:Content>
