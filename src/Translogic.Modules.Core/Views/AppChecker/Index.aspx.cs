namespace Translogic.Modules.Core.Views.AppChecker
{
	using Domain.Model.Acesso;
	// using MvcContrib.UI.ASPXViewEngine;
	using Translogic.Core.Infrastructure.Modules;

	/// <summary>
	/// view do Index tipado
	/// </summary>
	public class Index : System.Web.Mvc.ViewPage<EnvironmentInfo>
	{
	}

	/// <summary>
	/// Informações de ambiente
	/// </summary>
	public class EnvironmentInfo
	{
		#region PROPRIEDADES

		/// <summary>
		/// Gets or sets ConnectionString.
		/// </summary>
		public string ConnectionString { get; set; }

		/// <summary>
		/// Gets or sets Culturas.
		/// </summary>
		public Cultura[] Culturas { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether DatabaseIsOk.
		/// </summary>
		public bool DatabaseIsOk { get; set; }

		/// <summary>
		/// Gets or sets Modules.
		/// </summary>
		public IModuleInstaller[] Modules { get; set; }

		#endregion
	}
}