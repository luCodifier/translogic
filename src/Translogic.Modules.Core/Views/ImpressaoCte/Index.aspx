﻿<%@ Import Namespace="System.Security.Policy" %>
<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Interna.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content runat="server" ID="HeadContent" ContentPlaceHolderID="HeadContent">

<script type="text/javascript">

    var formModal = null;
    var grid = null;
    // var dataAtual = new String('<%= String.Format("{0:dd/MM/yyyy}", DateTime.Now) %>');

    function FormSuccess(form, action) {
        ds.removeAll();
        ds.loadData(action.result, true);
    }

    function FormError(form, action) {
        Ext.Msg.alert('Ops...', action.result.Message);
        ds.removeAll();
    }

    var ds = new Ext.data.JsonStore({
        root: "Items",
        fields: [
                    'CteId',
                    'Fluxo',
                    'Origem',
                    'Cte',
                    'Chave',
                    'SerieDesp5',
                    'NumDesp5',
                    'SerieDesp6',
                    'NumDesp6',
                    'DateEmissao',
                    'Impresso'
			    ]
    });

/* ------------- Ação CTRL+C nos campos da Grid ------------- */
    var isCtrl = false;
    document.onkeyup = function (e) {

        var keyID = event.keyCode;

        // 17 = tecla CTRL
        if (keyID == 17) {
            isCtrl = false; //libera tecla CTRL
        }
    }

    document.onkeydown = function (e) {

        var keyID = event.keyCode;

        // verifica se CTRL esta acionado
        if (keyID == 17) {
            isCtrl = true;
        }

        // 67 = tecla 'c'
        if (keyID == 67 && isCtrl == true) {

            // verifica se há selecão na tabela
            if (grid.getSelectionModel().hasSelection()) {
                // captura todas as linhas selecionadas
                var recordList = grid.getSelectionModel().getSelections();

                var a = [];
                var separator = '\t'; // separador para colunas no excel
                for (var i = 0; i < recordList.length; i++) {
                    var s = '';
                    var item = recordList[i].data;
                    for (key in item) {
                        if (key != 'Id') { //elimina o campo id da linha
                            s = s + item[key] + separator;
                        }
                    }
                    s = s.substr(0, s.length - 1); //retira o ultimo separador '\t' da linha
                    a.push(s);
                }
                window.clipboardData.setData('Text', (a.join('\n'))); //insere linhas no clipBoard
            }
        }
    }
/* ------------------------------------------------------- */

    $(function () {

        sm2 = new Ext.grid.CheckboxSelectionModel({
            listeners: {
                // On selection change, set enabled state of the removeButton
                // which was placed into the GridPanel using the ref config
                selectionchange: function (sm) {

                    if (sm.getCount()) {
                        Ext.getCmp("imprimir").enable();
                    } else {
                        Ext.getCmp("imprimir").disable();
                    }
                }
            }
        });

        grid = new Ext.grid.GridPanel({
            viewConfig: {
                forceFit: true
            },
            stripeRows: true,
            region: 'center',
            store: ds,
            loadMask: { msg: App.Resources.Web.Carregando },
            colModel: new Ext.grid.ColumnModel({
                defaults: {
                    sortable: false
                },
                columns: [
						new Ext.grid.RowNumberer(),
                        sm2,
                        { dataIndex: "CteId", hidden: true },
						{ header: 'Fluxo', dataIndex: "Fluxo", sortable: false },
						{ header: 'Origem', dataIndex: "Origem", sortable: false },
				        { header: 'CTE', dataIndex: "Cte", sortable: false },
				        { header: 'Chave', dataIndex: "Chave", sortable: false, width: 200 },
                        { header: 'Série Desp5', dataIndex: "SerieDesp5", sortable: false },
				        { header: 'Num Desp5', dataIndex: "NumDesp5", sortable: false },
				        { header: 'Série Desp6', dataIndex: "SerieDesp6", sortable: false },
				        { header: 'Num Desp6', dataIndex: "NumDesp6", sortable: false },
				        { header: 'Data Emissão', dataIndex: "DateEmissao", sortable: false },
				        { header: 'Impresso', dataIndex: "Impresso", sortable: false }
					]
            }),
            sm: sm2
        });




        filters = new Ext.form.FormPanel({
            id: 'grid-filtros',
            title: "Filtros",
            region: 'center',
            bodyStyle: 'padding: 15px',
            labelAlign: 'top',
            items:
			[{
			    layout: 'column',
			    border: false,
			    items: [{
			        width: 130,
			        layout: 'form',
			        border: false,
			        items:
						[
							{
							    xtype: 'datefield',
							    fieldLabel: 'Data Inicial',
							    id: 'filtro-data-inicial',
							    name: 'dataInicial',
							    width: 100,
							    allowBlank: false,
							    vtype: 'daterange',
							    endDateField: 'filtro-data-final',
							    hiddenName: 'dataInicial'
							    // ,value: dataAtual
							}
                            ,
                            {
                                xtype: 'textfield',
                                style: 'text-transform: uppercase',
							    id: 'filtro-fluxo',
							    fieldLabel: 'Fluxo',
							    name: 'fluxo',
							    allowBlank: true,
							    maxLength: 20,
							    width: 100,
							    hiddenName: 'fluxo'
							}
						]
			    },
				{
				    width: 130,
				    layout: 'form',
				    border: false,
				    items:
						[
							{
							    xtype: 'datefield',
							    fieldLabel: 'Data Final',
							    id: 'filtro-data-final',
							    name: 'dataFinal',
							    width: 100,
							    allowBlank: false,
							    vtype: 'daterange',
							    startDateField: 'filtro-data-inicial',
							    hiddenName: 'dataFinal'
							    // ,value: dataAtual
							}
                            ,
                            {
							    xtype: 'numberfield',
							    id: 'filtro-nro-cte',
							    fieldLabel: 'Nro CTE',
							    name: 'nroCte',
							    allowBlank: true,
							    maxLength: 20,
							    width: 100,
							    hiddenName: 'nroCte'
							}
						]
				},
                {
                    width: 130,
                    layout: 'form',
                    border: false,
                    items:
						[
							{
							    xtype: 'numberfield',
							    id: 'filtro-serie',
							    fieldLabel: 'Série',
							    name: 'serie',
							    allowBlank: true,
							    maxLength: 20,
							    width: 100,
							    hiddenName: 'serie'
							}
                            ,
                            {
                                autoCreate: { tag: 'input', type: 'text', maxlength: '3', autocomplete: 'off' },
							    xtype: 'textfield',
                                style: 'text-transform: uppercase',
							    id: 'filtro-estacao',
							    fieldLabel: 'Estação Origem',
							    name: 'estacao',
							    allowBlank: true,
							    maxLength: 20,
							    width: 50,
                                maxLength: 3,
							    hiddenName: 'estacao'
                            }
						]
                },
				{
				    width: 130,
				    layout: 'form',
				    border: false,
				    items:
						[
							{
							    xtype: 'numberfield',
							    id: 'filtro-despacho',
							    fieldLabel: 'Despacho',
							    name: 'despacho',
							    allowBlank: true,
							    maxLength: 20,
							    width: 100,
							    hiddenName: 'despacho'
							}
                            ,
                            {
                                xtype: 'checkbox',
				                fieldLabel: 'Impresso',
				                id: 'impresso',
				                name: 'impresso',
                                hiddenName: 'impresso'
                            }
                            
						]
				}
                ,
                {
                    width: 230,
                    layout: 'form',
                    border: false,
                    items:
						[
                            {  
                                xtype: 'textfield',
							    id: 'filtro-chave',
							    fieldLabel: 'Chave',
							    name: 'chave',
							    allowBlank: true,
							    maxLength: 44,
							    width: 200,
							    hiddenName: 'chave'
				            }
						]
                }
                ]

			}],
            buttonAlign: "center",
            buttons:
	        [
                {
                    text: 'Imprimir',
                    id: 'imprimir',
                    iconCls: 'icon-printer',
                    disabled: true,
                    handler: function (b, e) {

                        var listaEnvio = Array();

                        var selected = sm2.getSelections();

                        for (var i = 0; i < selected.length; i++) {
                            listaEnvio.push(selected[i].data.CteId);
                        }

                        var dados = $.toJSON(listaEnvio);

                        $.ajax({
                            url: "<%= Url.Action("Imprimir") %>",
                            type: "POST",
                            dataType: 'json',
                            data: dados,
                            contentType: "application/json; charset=utf-8",
                            success: function(result) {
                                
                                if(result.success) {
                                    Ext.Msg.alert('Sucesso!', result.Message);
                                }
                                else {
                                    Ext.Msg.alert('OPS...', result.Message);
                                }
                            },
                            failure: function(result) {
                                Ext.Msg.alert('OPS...', result.Message);
                            }
                        });
                    },
                    scope: this
                },
                {
                    text: 'Pesquisar',
                    type: 'submit',
                    iconCls: 'icon-find',
                    handler: function (b, e) {

                        if (Ext.getCmp("filtro-serie").getValue() != '' && Ext.getCmp("filtro-despacho").getValue() == '') {
                            Ext.Msg.show({
					            title: "Erro",
					            msg: "Preencha o filtro Despacho!",
					            buttons: Ext.Msg.OK,
					            icon: Ext.MessageBox.ERROR,
					            minWidth: 200
				            });
                        }
                        else if (Ext.getCmp("filtro-despacho").getValue() != '' && Ext.getCmp("filtro-serie").getValue() == '') {
                            Ext.Msg.show({
					            title: "Erro",
					            msg: "Preencha o filtro Série!",
					            buttons: Ext.Msg.OK,
					            icon: Ext.MessageBox.ERROR,
					            minWidth: 200
				            });
                        }
                        else {

                            if (filters.form.isValid()) {
                                filters.getForm().submit({
                                    url: '<%= Url.Action("ObterCtes") %>',
                                    waitMsg: 'Aguarde, processando...',
                                    timeout: 300000,
                                    reset: false,
                                    success: function (filters, action) {
                                        FormSuccess(filters, action);
                                    },
                                    failure: function (filters, action) {
                                        if (action.result == null) {
                                            var jsonData = Ext.util.JSON.decode(action.response.responseText);
                                            var msg = jsonData.message;
                                            Ext.Msg.alert('Ops...', msg);
                                        }
                                        else {
                                            FormError(filters, action);
                                        }
                                    }
                                })
                            }
                        }
                    }
                },
	            {
	                text: 'Limpar',
	                handler: function (b, e) {
	                    ds.removeAll();
	                    Ext.getCmp("grid-filtros").getForm().reset();
	                },
	                scope: this
	            }
            ]
        });

        new Ext.Viewport({
            layout: 'border',
            margins: 10,
            items: [
			{
			    region: 'north',
			    height: 230,
			    items: [{
			        region: 'center',
			        applyTo: 'header-content'
			    },
				filters]
			},
			grid
		]

        });



    });


</script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div id="header-content">
        <h1>Impressão Cte</h1>
        <small>Você está em Cte > Impressão Cte</small>
        <br />
    </div>
</asp:Content>
