﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<script type="text/javascript">
    
    var gridResultadoMdfe = null;
    var gridResultadoMdfeDet = null;
    var linhaTabMdfe = null;
    var idMdfeSelecionadoOld = null;
    var idMdfeSelecionado = null;

    /**********************************FUNÇÕES DE APOIO*************************************/

        function printRenderer(val, metadata, record) {         	
		    if (val == 'S') 
		    {
			    metadata.style = 'cursor: pointer;';
			    var urlPrint = '<%= Url.Action("Imprimir") %>' +"?idMdfe="+ record.data.Id;
			    return "<a href='"+urlPrint+"'><img src='<%=Url.Images("Icons/pdf.png") %>' alt='Imprimir Mdfe'></a>";
		    } else {
			    return "";
		    }
        };
        var dsStatusMdfe = new Ext.data.JsonStore({
            root: "Items",
            proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("ObterStatusMdfePorIdMdfe") %>', timeout: 600000 }),
            fields:
                [
                    'DataHora',
                    'StatusMdfe',
                    'Usuario'
                ]
        });

        /********************************************GRIDS***************************************************/

        gridResultadoMdfe = new Translogic.PaginatedGrid({
            id: 'gridResultadoMdfe',
            name: 'gridResultadoMdfe',
            autoLoadGrid: false,
            width: 600,
            height: 230,
            viewConfig: {
                emptyText: 'Não possui Mdfe(s) para exibição.'
            },
            fields:
                [
                    'Id',
                    'TremId',
                    'PdfGerado',
                    'Chave',
                    'Serie',
                    'Numero',
                    'SituacaoAtual',
                    'DataHora'
                ],
            url: '<%= Url.Action("DetalhesMdfe") %>',
            stripeRows: true,
            loadMask: { msg: App.Resources.Web.Carregando },
            columns: [
					{ header: 'Id', dataIndex: "Id", hidden: true },
                    { header: 'TremId', dataIndex: "TremId", hidden: true },
                    { header: 'Status', dataIndex: 'SituacaoAtual', width: 60, renderer:statusRenderer },    
                    { header: '...', dataIndex: 'PdfGerado', width: 40, sortable: false, renderer:printRenderer },
                    { header: 'Serie', dataIndex: 'Serie', width: 40, sortable: false },
                    { header: 'Numero', dataIndex: 'Numero', width: 100, sortable: false },
					{ header: 'Data', dataIndex: 'DataHora', sortable: false }
				],
            listeners: {
                rowClick: function (grid, rowIndex, record, e) {
                    var sel = gridResultadoMdfe.getSelectionModel().getSelected();
                    idMdfeSelecionado = sel.data.Id;
                    idTremSelecionado = sel.data.TremId;
                    
                    if (grid.getStore().getAt(rowIndex).data.SituacaoAtualEnum == 'PME' ||
				              grid.getStore().getAt(rowIndex).data.SituacaoAtualEnum == 'EAR' ||
				              grid.getStore().getAt(rowIndex).data.SituacaoAtualEnum == 'ERR') 
                    {
				        Ext.getCmp('btnReenviarMdfe').setDisabled(false);
				    }
                    else
                    {
				        Ext.getCmp('btnReenviarMdfe').setDisabled(true);
				    }
					    
                    if (idMdfeSelecionadoOld != idMdfeSelecionado) {
                        idMdfeSelecionadoOld = idMdfeSelecionado;
                        gridResultadoMdfeDet.getStore().load();
                    }
                },
                rowdblclick: function (grid, rowIndex, record, e) 
                {
                    tabResultados.setActiveTab(2);
                    idDespachoSelecionado = 0;

                    gridResultadoComp.getStore().load();
                    gridResultadoTrem.getStore().load();

                    idTremSelecionado = 0;
                    
                }
            }
        });

        var colunaGridMdfe = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding-right:5px',
            items: [gridResultadoMdfe]
        };

        gridResultadoMdfe.getStore().proxy.on('beforeload', function (p, params) {
            params['filter[0].Campo'] = 'idTrem';
            params['filter[0].Valor'] = idTremSelecionado;
            params['filter[0].FormaPesquisa'] = 'Start';

            params['filter[1].Campo'] = 'semaforo';
            params['filter[1].Valor'] = semaforo;
            params['filter[1].FormaPesquisa'] = 'Start';
            
            params['filter[2].Campo'] = 'UF';
            params['filter[2].Valor'] = siglaUfSemaforo;
            params['filter[2].FormaPesquisa'] = 'Start';
            
            params['filter[3].Campo'] = 'periodoHoras';
            params['filter[3].Valor'] = periodoHoraSemaforo;
            params['filter[3].FormaPesquisa'] = 'Start';
            
            params['filter[4].Campo'] = 'idDespacho';
            params['filter[4].Valor'] = idDespachoSelecionado;
            params['filter[4].FormaPesquisa'] = 'Start';
            
            idMdfeSelecionadoOld = null;
            idMdfeSelecionado = null;

            gridResultadoMdfeDet.getStore().removeAll();
            Ext.getCmp('btnReenviarMdfe').setDisabled(true);
        });

        gridResultadoMdfeDet = new Ext.grid.GridPanel({
            id: 'gridResultadoMdfeDet',
            name: 'gridResultadoMdfeDet',
            autoLoadGrid: false,
            width: 500,
            height: 230,
            store: dsStatusMdfe,
            loadMask: { msg: App.Resources.Web.Carregando },
            columns: [
						{ header: 'Data', dataIndex: 'DataHora', width: 110, sortable: false },
						{ header: 'Status', dataIndex: 'StatusMdfe', width: 230, sortable: false },
						{ header: 'Usuario', dataIndex: 'Usuario', width: 150, sortable: false }
					]
        });

        gridResultadoMdfeDet.getStore().proxy.on('beforeload', function (p, params) {
            params['filter[0].Campo'] = 'idMdfe';
            params['filter[0].Valor'] = idMdfeSelecionado;
            params['filter[0].FormaPesquisa'] = 'Start';
        });

        var colunaGridMdfeDef = {
            layout: 'form',
            //width: '40%',
            bodyStyle: 'padding-left:5px',
            border: false,
            items: [gridResultadoMdfeDet]
        };

        var linhaResultadoMdfe = {
            layout: 'column',
            border: false,
            items: [colunaGridMdfe, colunaGridMdfeDef]
        };

        var btnReenviarMdfe = {
            name: 'btnReenviarMdfe',
            id: 'btnReenviarMdfe',
            text: 'Reenviar',
            disabled: true,
            handler: function() {
                if (Ext.Msg.confirm("Aprovação", "Você deseja realmente enviar o MDF-e para aprovação?", function(btn, text) {
                    if (btn == 'yes') {
                        Ext.Ajax.request({
                          url: "<% =Url.Action("EnviarAprovacao") %>",
                          method: "POST",
                          params: { idMdfe: idMdfeSelecionado },
                          success: function(response) {
                              var result = Ext.util.JSON.decode(response.responseText);

                              if (result.success) {
                                  Ext.Msg.show({
                                      title: "Mensagem de Informação",
                                      msg: "Mdf-e enviado para aprovação!",
                                      buttons: Ext.Msg.OK,
                                      minWidth: 200
                                  });
                              } else {
                                  Ext.Msg.show({
                                      title: "Mensagem de Erro",
                                      msg: "Erro ao enviar o Mdf-e para aprovação!</BR>" + result.Message,
                                      buttons: Ext.Msg.OK,
                                      icon: Ext.MessageBox.ERROR,
                                      minWidth: 200
                                  });
                              }
                          },
                          failure: function(conn, data) {
                              Ext.Msg.show({
                                  title: "Mensagem de Erro",
                                  msg: "Erro ao enviar o Mdf-e para aprovação!",
                                  buttons: Ext.Msg.OK,
                                  icon: Ext.MessageBox.ERROR,
                                  minWidth: 200
                              });
                          }
                      });
                    }
                }));
            }
        };

        var btnReimprimirRecibo = {
            name: 'btnReimprimirRecibo',
            id: 'btnReimprimirRecibo',
            text: 'Reimprimir recibo de entrega',
            handler: TratarTeclaEnter
        };

        var linhaBotoesMdfe = {
            layout: 'column',
            border: false,
            bodyStyle: 'padding-bottom:0px;',
            buttons: [btnReenviarMdfe],
            buttonAlign: 'center'
        };

        var fieldSetResultadoMdfe = {
            xtype: 'fieldset',
            id: 'fieldSetResultadoMdfe',
            name: 'fieldSetResultadoMdfe',
            title: 'Mdfe Trem:[] - OS:[]',
            width: '100%',
            bodyStyle: 'padding:5px',
            border: true,
            items: [linhaResultadoMdfe, linhaBotoesMdfe]
        };

        var coluna1LinhaTabMdfe = {
            layout: 'form',
            border: false,
            width: '100%',
            items: [fieldSetResultadoMdfe]
        };

        linhaTabMdfe = {
            layout: 'column',
            border: false,
            width: '100%',
            items: [coluna1LinhaTabMdfe]
        };
    
</script>
