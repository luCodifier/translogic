﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%Html.RenderPartial("DiarioDeBordo"); %>
<script type="text/javascript" language="javascript">
    
    var gridResultadoStatusCte = null;
    var gridNotasVagoes = null;
    var fieldSetResultadoCte = null;
    var cteId = "";
    var alteracaoCfop = false;
    var tabDetalhesCte = null;
    
    function getDadosCte(idCteSelecionada, idDespachoSelecionado) 
    {
        tabResultados.setActiveTab(3);
        tabDetalhesCte.setActiveTab(0);
        
        Ext.getCmp("fsResultadoBuscaCte").getEl().mask("Obtendo dados da Cte ...", "x-mask-loading");
        Ext.Ajax.request(
        {
            url: '<%= Url.Action("DetalheCte") %>',
            params: { idCte: idCteSelecionada },
            timeout: 300000,
            failure: function (conn, data) {
                Ext.Msg.show({
                    title: 'Ops...',
                    msg: "Ocorreu um erro inesperado.",
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.ERROR
                });
            },
            success: function (response) {
                var result = Ext.decode(response.responseText);

                if (result.Success == false) {
                    Ext.getCmp("fsResultadoBuscaCte").getEl().unmask();
                    Ext.Msg.show({
                        title: 'Ops...',
                        msg: result.Message,
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.ERROR
                    });
                    tabResultados.setActiveTab(2);
                }
                else {
                    
                    var cteDados = result.Item;
                    cteId = idCteSelecionada;

                    Ext.getCmp('fsEnviarDiarioBordo').setTitle("Enviar diário de Bordo Cte:"+cteDados.Numero);
                    Ext.getCmp('fsListarDiarioBordo').setTitle("Diário de Bordo Cte:"+cteDados.Numero);
                    
                    Ext.getCmp("lblSerieCte").setValue(cteDados.Serie);
                    Ext.getCmp("lblNumeroCte").setValue(cteDados.Numero);
                    Ext.getCmp("lblDataEmissaoCte").setValue(cteDados.DataEmissao);
                    Ext.getCmp("lblChaveCte").setValue(cteDados.Chave);
                    Ext.getCmp("fluxoCte").setValue(cteDados.FluxoComercial);
                    Ext.getCmp("vagaoCte").setValue(cteDados.Vagao);
                    Ext.getCmp("lblCFOPCte").setValue(cteDados.Cfop);
                    Ext.getCmp("txtObservacaoCte").setValue(cteDados.Observacao);
                    Ext.getCmp("lblNomeRemetente").setValue(cteDados.Remetente.Nome);
                    Ext.getCmp("lblEnderecoRemetente").setValue(cteDados.Remetente.Endereco);
                    Ext.getCmp("lblMunicipioRemetente").setValue(cteDados.Remetente.Cidade);
                    Ext.getCmp("lblCEPRemetente").setValue(cteDados.Remetente.Cep);
                    Ext.getCmp("lblDocumentoRemetente").setValue(cteDados.Remetente.Documento);
                    Ext.getCmp("lblInscEstRemetente").setValue(cteDados.Remetente.InscEstadual);
                    Ext.getCmp("lblUFRemetente").setValue(cteDados.Remetente.UF);
                    Ext.getCmp("lblPaisRemetente").setValue(cteDados.Remetente.Pais);
                    Ext.getCmp("lblFoneRemetente").setValue(cteDados.Remetente.Fone);
                    Ext.getCmp("lblNomeDestinatario").setValue(cteDados.Destinatario.Nome);
                    Ext.getCmp("lblEnderecoDestinatario").setValue(cteDados.Destinatario.Endereco);
                    Ext.getCmp("lblMunicipioDestinatario").setValue(cteDados.Destinatario.Cidade);
                    Ext.getCmp("lblCEPDestinatario").setValue(cteDados.Destinatario.Cep);
                    Ext.getCmp("lblDocumentoDestinatario").setValue(cteDados.Destinatario.Documento);
                    Ext.getCmp("lblInscEstDestinatario").setValue(cteDados.Destinatario.InscEstadual);
                    Ext.getCmp("lblUFDestinatario").setValue(cteDados.Destinatario.UF);
                    Ext.getCmp("lblPaisDestinatario").setValue(cteDados.Destinatario.Pais);
                    Ext.getCmp("lblFoneDestinatario").setValue(cteDados.Destinatario.Fone);
                    Ext.getCmp("lblNomeTomador").setValue(cteDados.Tomador.Nome);
                    Ext.getCmp("lblEnderecoTomador").setValue(cteDados.Tomador.Endereco);
                    Ext.getCmp("lblMunicipioTomador").setValue(cteDados.Tomador.Cidade);
                    Ext.getCmp("lblCEPTomador").setValue(cteDados.Tomador.Cep);
                    Ext.getCmp("lblDocumentoTomador").setValue(cteDados.Tomador.Documento);
                    Ext.getCmp("lblInscEstTomador").setValue(cteDados.Tomador.InscEstadual);
                    Ext.getCmp("lblUFTomador").setValue(cteDados.Tomador.UF);
                    Ext.getCmp("lblPaisTomador").setValue(cteDados.Tomador.Pais);
                    Ext.getCmp("lblFoneTomador").setValue(cteDados.Tomador.Fone);
                    Ext.getCmp("lblNomeExpedidor").setValue(cteDados.Expedidor.Nome);
                    Ext.getCmp("lblEnderecoExpedidor").setValue(cteDados.Expedidor.Endereco);
                    Ext.getCmp("lblMunicipioExpedidor").setValue(cteDados.Expedidor.Cidade);
                    Ext.getCmp("lblCEPExpedidor").setValue(cteDados.Expedidor.Cep);
                    Ext.getCmp("lblDocumentoExpedidor").setValue(cteDados.Expedidor.Documento);
                    Ext.getCmp("lblInscEstExpedidor").setValue(cteDados.Expedidor.InscEstadual);
                    Ext.getCmp("lblUFExpedidor").setValue(cteDados.Expedidor.UF);
                    Ext.getCmp("lblPaisExpedidor").setValue(cteDados.Expedidor.Pais);
                    Ext.getCmp("lblFoneExpedidor").setValue(cteDados.Expedidor.Fone);
                    Ext.getCmp("lblNomeRecebedor").setValue(cteDados.Recebedor.Nome);
                    Ext.getCmp("lblEnderecoRecebedor").setValue(cteDados.Recebedor.Endereco);
                    Ext.getCmp("lblMunicipioRecebedor").setValue(cteDados.Recebedor.Cidade);
                    Ext.getCmp("lblCEPRecebedor").setValue(cteDados.Recebedor.Cep);
                    Ext.getCmp("lblDocumentoRecebedor").setValue(cteDados.Recebedor.Documento);
                    Ext.getCmp("lblInscEstRecebedor").setValue(cteDados.Recebedor.InscEstadual);
                    Ext.getCmp("lblUFRecebedor").setValue(cteDados.Recebedor.UF);
                    Ext.getCmp("lblPaisRecebedor").setValue(cteDados.Recebedor.Pais);
                    Ext.getCmp("lblFoneRecebedor").setValue(cteDados.Recebedor.Fone);
                    Ext.getCmp("lblPesoVagaoCte").setValue(cteDados.PrestacaoServico.PesoVagao);
                    Ext.getCmp("lblBaseCalculo").setValue(cteDados.PrestacaoServico.BaseCalculo);
                    Ext.getCmp("lblAliqIcms").setValue(cteDados.PrestacaoServico.AliqIcms);
                    Ext.getCmp("lblVlrIcms").setValue(cteDados.PrestacaoServico.VlrIcms);
                    Ext.getCmp("lblVlrDesconto").setValue(cteDados.PrestacaoServico.VlrDesconto);
                    Ext.getCmp("lblVlrTotal").setValue(cteDados.PrestacaoServico.VlrTotal);
                    Ext.getCmp("lblVlrReceber").setValue(cteDados.PrestacaoServico.VlrReceber);
                    Ext.getCmp("lblTarifaCte").setValue(cteDados.PrestacaoServico.VlrTarifa);
                    Ext.getCmp("lblPesoVagaoCte").setValue(cteDados.PrestacaoServico.PesoVagao);

                    gridResultadoStatusCte.getStore().load();
                    gridNotasVagoes.getStore().load({ params: { 'idDespacho': idDespachoSelecionado } });
                    gridResultadoDiarioDeBordo.getStore().load({ params: { 'idCte': cteId } });
                    
                    Ext.getCmp("fsResultadoBuscaCte").getEl().unmask();
                }
            }
        });
    }

    $(function() {
        var dsStatusCte = new Ext.data.JsonStore({
                root: "Items",
                proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("ObterStatusCtePorIdCte") %>', timeout: 600000 }),
                fields:
                    [
                        'DataHora',
                        'StatusCte',
                        'StatusSefaz',
                        'Usuario',
                        'Procedimentos'
                    ]
            });

        var linhaDetalhesCte = new Array();

        var lblSerie = {
            xtype: 'textfield',
            fieldLabel: 'Série',
            id: 'lblSerieCte',
            name: 'lblSerieCte',
            value: '',
            width: 30,
            readOnly: true
        };

        var lblNumeroCte = {
            xtype: 'textfield',
            fieldLabel: 'Número',
            id: 'lblNumeroCte',
            name: 'lblNumeroCte',
            value: '',
            width: 80,
            readOnly: true
        };

        var lblDataEmissao = {
            xtype: 'textfield',
            fieldLabel: 'Data de Emissão',
            id: 'lblDataEmissaoCte',
            name: 'lblDataEmissaoCte',
            value: '',
            width: 125,
            readOnly: false
        };

        var txtFluxo = {
            xtype: 'textfield',
            id: 'fluxoCte',
            fieldLabel: 'Fluxo',
            name: 'fluxoCte',
            width: 80,
            value: '',
            readOnly: true
        };

        var txtVagao = {
            xtype: 'textfield',
            vtype: 'ctevagaovtype',
            id: 'vagaoCte',
            fieldLabel: 'Vagão',
            name: 'vagaoCte',
            maxLength: 20,
            width: 80,
            value: '',
            readOnly: true
        };

        var clsCfop = alteracaoCfop == "true" ? 'x-item-disabled campoCorrigido' : 'x-item-disabled';

        var lblCFOP = {
            xtype: 'textfield',
            fieldLabel: 'CFOP',
            id: 'lblCFOPCte',
            name: 'lblCFOPCte',
            value: '',
            width: 80,
            readOnly: true
        };

        var linha1TabCte_coluna1 = { width: 35, layout: 'form', border: false, items: [lblSerie] };
        var linha1TabCte_coluna2 = { width: 85, layout: 'form', border: false, items: [lblNumeroCte] };
        var linha1TabCte_coluna3 = { width: 130, layout: 'form', border: false, items: [lblDataEmissao] };
        var linha1TabCte_coluna4 = { width: 85, layout: 'form', border: false, items: [txtFluxo] };
        var linha1TabCte_coluna5 = { width: 85, layout: 'form', border: false, items: [txtVagao] };
        var linha1TabCte_coluna6 = { width: 85, layout: 'form', border: false, items: [lblCFOP] };
        
        var linha1TabCte = {
            layout: 'column',
            border: false,
            items: [linha1TabCte_coluna1, linha1TabCte_coluna2, linha1TabCte_coluna3, linha1TabCte_coluna4, linha1TabCte_coluna5, linha1TabCte_coluna6]
        };
        
        var lblChaveCte = {
            xtype: 'textfield',
            fieldLabel: 'Chave',
            id: 'lblChaveCte',
            name: 'lblChaveCte',
            value: '',
            width: 500,
            readOnly: true
        };
        
        var linha2TabCte_coluna1 = { width: 600, layout: 'form', border: false, items: [lblChaveCte] };
        
        

        var linha2TabCte = {
            layout: 'column',
            border: false,
            items: [linha2TabCte_coluna1]
        };

        var txtObservacao = new Ext.form.TextArea({
                xtype: 'textarea',
                fieldLabel: 'Observação',
                id: 'txtObservacaoCte',
                name: 'txtObservacaoCte',
                height: 80,
                width: 500,
                value: '',
                readOnly: true
            });

        var linha3TabCte_coluna = { width: 540, layout: 'form', border: false, items: [txtObservacao] };

        var linha3TabCte = {
            layout: 'column',
            border: false,
            items: [linha3TabCte_coluna]
        };

        var linhasTabCte = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            width: '100%',
            items: [linha1TabCte, linha2TabCte, linha3TabCte]
        };

        var lblRemetente = {
            xtype: 'textfield',
            fieldLabel: 'Remetente',
            id: 'lblNomeRemetente',
            name: 'lblNomeRemetente',
            value: '',
            readOnly: true,
            width: 280
        };

        var lblCPF_CNPJ_Remetente = {
            xtype: 'textfield',
            fieldLabel: 'CPF/CNPJ',
            id: 'lblDocumentoRemetente',
            name: 'lblDocumentoRemetente',
            value: '',
            width: 110,
            readOnly: true
        };

        var lblInscEstRemetente = {
            xtype: 'textfield',
            fieldLabel: 'Insc. Est',
            id: 'lblInscEstRemetente',
            name: 'lblInscEstRemetente',
            value: '',
            width: 80,
            readOnly: true
        };

        var lblUFRemetente = {
            xtype: 'textfield',
            fieldLabel: 'UF',
            id: 'lblUFRemetente',
            name: 'lblUFRemetente',
            value: '',
            width: 30,
            readOnly: true
        };

        var linha1_coluna1FsRemetente = { width: 285, layout: 'form', border: false, items: [lblRemetente] };
        var linha1_coluna2FsRemetente = { width: 115, layout: 'form', border: false, items: [lblCPF_CNPJ_Remetente] };
        var linha1_coluna3FsRemetente = { width: 85, layout: 'form', border: false, items: [lblInscEstRemetente] };
        var linha1_coluna4FsRemetente = { width: 30, layout: 'form', border: false, items: [lblUFRemetente] };

        var lblEnderecoRemetente = {
            xtype: 'textfield',
            fieldLabel: 'Endereço',
            id: 'lblEnderecoRemetente',
            name: 'lblEnderecoRemetente',
            value: '',
            readOnly: true,
            width: 200
        };

        var lblMunicipioRemetente = {
            xtype: 'textfield',
            fieldLabel: 'Município',
            id: 'lblMunicipioRemetente',
            name: 'lblMunicipioRemetente',
            value: '',
            readOnly: true,
            width: 110
        };

        var lblCEPRemetente = {
            xtype: 'textfield',
            fieldLabel: 'CEP',
            id: 'lblCEPRemetente',
            name: 'lblCEPRemetente',
            value: '',
            width: 65,
            readOnly: true
        };

        var lblPaisRemetente = {
            xtype: 'textfield',
            fieldLabel: 'País',
            id: 'lblPaisRemetente',
            name: 'lblPaisRemetente',
            value: '',
            width: 30,
            readOnly: true
        };

        var lblFoneRemetente = {
            xtype: 'textfield',
            fieldLabel: 'Fone',
            id: 'lblFoneRemetente',
            name: 'lblFoneRemetente',
            value: '',
            width: 90,
            readOnly: true
        };

        var linha2_coluna1FsRemetente = { width: 205, layout: 'form', border: false, items: [lblEnderecoRemetente] };
        var linha2_coluna2FsRemetente = { width: 115, layout: 'form', border: false, items: [lblMunicipioRemetente] };
        var linha2_coluna3FsRemetente = { width: 70, layout: 'form', border: false, items: [lblCEPRemetente] };
        var linha2_coluna4FsRemetente = { width: 35, layout: 'form', border: false, items: [lblPaisRemetente] };
        var linha2_coluna5FsRemetente = { width: 91, layout: 'form', border: false, items: [lblFoneRemetente] };

        var linha1fsRemetente = {
            layout: 'column',
            border: false,
            items: [linha1_coluna1FsRemetente, linha1_coluna2FsRemetente, linha1_coluna3FsRemetente, linha1_coluna4FsRemetente]
        };

        var linha2fsRemetente = {
            layout: 'column',
            border: false,
            items: [linha2_coluna1FsRemetente, linha2_coluna2FsRemetente, linha2_coluna3FsRemetente, linha2_coluna4FsRemetente, linha2_coluna5FsRemetente]
        };

        var fsRemetente = {
            xtype: 'fieldset',
            id: 'fsRemetente',
            name: 'fsRemetente',
            height: 130,
            width: 540,
            title: 'Remetente',
            items: [linha1fsRemetente, linha2fsRemetente]
        };

        var lblDestinatario = {
            xtype: 'textfield',
            fieldLabel: 'Destinatário',
            id: 'lblNomeDestinatario',
            name: 'lblNomeDestinatario',
            value: '',
            readOnly: true,
            width: 280
        };

        var lblCPF_CNPJ_Destinatario = {
            xtype: 'textfield',
            fieldLabel: 'CPF/CNPJ',
            id: 'lblDocumentoDestinatario',
            name: 'lblDocumentoDestinatario',
            value: '',
            width: 110,
            readOnly: true
        };

        var lblInscEstDestinatario = {
            xtype: 'textfield',
            fieldLabel: 'Insc. Est',
            id: 'lblInscEstDestinatario',
            name: 'lblInscEstDestinatario',
            value: '',
            width: 80,
            readOnly: true
        };

        var lblUFDestinatario = {
            xtype: 'textfield',
            fieldLabel: 'UF',
            id: 'lblUFDestinatario',
            name: 'lblUFDestinatario',
            value: '',
            width: 30,
            readOnly: true
        };

        var linha1_coluna1fsDestinatario = { width: 285, layout: 'form', border: false, items: [lblDestinatario] };
        var linha1_coluna2fsDestinatario = { width: 115, layout: 'form', border: false, items: [lblCPF_CNPJ_Destinatario] };
        var linha1_coluna3fsDestinatario = { width: 85, layout: 'form', border: false, items: [lblInscEstDestinatario] };
        var linha1_coluna4fsDestinatario = { width: 30, layout: 'form', border: false, items: [lblUFDestinatario] };

        var lblEnderecoDestinatario = {
            xtype: 'textfield',
            fieldLabel: 'Endereço',
            id: 'lblEnderecoDestinatario',
            name: 'lblEnderecoDestinatario',
            value: '',
            readOnly: true,
            width: 200
        };

        var lblMunicipioDestinatario = {
            xtype: 'textfield',
            fieldLabel: 'Município',
            id: 'lblMunicipioDestinatario',
            name: 'lblMunicipioDestinatario',
            value: '',
            readOnly: true,
            width: 110
        };

        var lblCEPDestinatario = {
            xtype: 'textfield',
            fieldLabel: 'CEP',
            id: 'lblCEPDestinatario',
            name: 'lblCEPDestinatario',
            value: '',
            width: 65,
            readOnly: true
        };

        var lblPaisDestinatario = {
            xtype: 'textfield',
            fieldLabel: 'País',
            id: 'lblPaisDestinatario',
            name: 'lblPaisDestinatario',
            value: '',
            width: 30,
            readOnly: true
        };

        var lblFoneDestinatario = {
            xtype: 'textfield',
            fieldLabel: 'Fone',
            id: 'lblFoneDestinatario',
            name: 'lblFoneDestinatario',
            value: '',
            width: 90,
            readOnly: true
        };

        var linha2_coluna1fsDestinatario = { width: 205, layout: 'form', border: false, items: [lblEnderecoDestinatario] };
        var linha2_coluna2fsDestinatario = { width: 115, layout: 'form', border: false, items: [lblMunicipioDestinatario] };
        var linha2_coluna3fsDestinatario = { width: 70, layout: 'form', border: false, items: [lblCEPDestinatario] };
        var linha2_coluna4fsDestinatario = { width: 35, layout: 'form', border: false, items: [lblPaisDestinatario] };
        var linha2_coluna5fsDestinatario = { width: 91, layout: 'form', border: false, items: [lblFoneDestinatario] };

        var linha1fsDestinatario = {
            layout: 'column',
            border: false,
            items: [linha1_coluna1fsDestinatario, linha1_coluna2fsDestinatario, linha1_coluna3fsDestinatario, linha1_coluna4fsDestinatario]
        };

        var linha2fsDestinatario = {
            layout: 'column',
            border: false,
            items: [linha2_coluna1fsDestinatario, linha2_coluna2fsDestinatario, linha2_coluna3fsDestinatario, linha2_coluna4fsDestinatario, linha2_coluna5fsDestinatario]
        };

        var fsDestinatario = {
            xtype: 'fieldset',
            id: 'fsDestinatario',
            name: 'fsDestinatario',
            height: 130,
            width: 540,
            title: 'Destinatário',
            items: [linha1fsDestinatario, linha2fsDestinatario]
        };

        var lblTomador = {
            xtype: 'textfield',
            fieldLabel: 'Tomador',
            id: 'lblNomeTomador',
            name: 'lblNomeTomador',
            value: '',
            readOnly: true,
            width: 265
        };

        var lblEnderecoTomador = {
            xtype: 'textfield',
            fieldLabel: 'Endereço',
            id: 'lblEnderecoTomador',
            name: 'lblEnderecoTomador',
            value: '',
            readOnly: true,
            width: 230
        };

        var lblMunicipioTomador = {
            xtype: 'textfield',
            fieldLabel: 'Município',
            id: 'lblMunicipioTomador',
            name: 'lblMunicipioTomador',
            value: '',
            readOnly: true,
            width: 100
        };

        var lblCEPTomador = {
            xtype: 'textfield',
            fieldLabel: 'CEP',
            id: 'lblCEPTomador',
            name: 'lblCEPTomador',
            value: '',
            width: 80,
            readOnly: true
        };

        var lblCPF_CNPJ_Tomador = {
            xtype: 'textfield',
            fieldLabel: 'CPF/CNPJ',
            id: 'lblDocumentoTomador',
            name: 'lblDocumentoTomador',
            value: '',
            width: 110,
            readOnly: true
        };

        var lblInscEstTomador = {
            xtype: 'textfield',
            fieldLabel: 'Insc. Est',
            id: 'lblInscEstTomador',
            name: 'lblInscEstTomador',
            value: '',
            width: 80,
            readOnly: true
        };

        var lblUFTomador = {
            xtype: 'textfield',
            fieldLabel: 'UF',
            id: 'lblUFTomador',
            name: 'lblUFTomador',
            value: '',
            width: 30,
            readOnly: true
        };

        var lblPaisTomador = {
            xtype: 'textfield',
            fieldLabel: 'País',
            id: 'lblPaisTomador',
            name: 'lblPaisTomador',
            value: '',
            width: 30,
            readOnly: true
        };

        var lblFoneTomador = {
            xtype: 'textfield',
            fieldLabel: 'Fone',
            id: 'lblFoneTomador',
            name: 'lblFoneTomador',
            value: '',
            width: 90,
            readOnly: true
        };

        var linha1_coluna1fsTomador = { width: 270, layout: 'form', border: false, items: [lblTomador] };
        var linha1_coluna2fsTomador = { width: 115, layout: 'form', border: false, items: [lblCPF_CNPJ_Tomador] };
        var linha1_coluna3fsTomador = { width: 85, layout: 'form', border: false, items: [lblInscEstTomador] };
        var linha1_coluna4fsTomador = { width: 235, layout: 'form', border: false, items: [lblEnderecoTomador] };
        var linha1_coluna5fsTomador = { width: 85, layout: 'form', border: false, items: [lblCEPTomador] };
        var linha1_coluna6fsTomador = { width: 105, layout: 'form', border: false, items: [lblMunicipioTomador] };
        var linha1_coluna7fsTomador = { width: 35, layout: 'form', border: false, items: [lblUFTomador] };
        var linha1_coluna8fsTomador = { width: 35, layout: 'form', border: false, items: [lblPaisTomador] };
        var linha1_coluna9fsTomador = { layout: 'form', border: false, items: [lblFoneTomador] };

        var linha1fsTomador = {
            layout: 'column',
            border: false,
            items: [linha1_coluna1fsTomador, linha1_coluna2fsTomador, linha1_coluna3fsTomador, linha1_coluna4fsTomador, linha1_coluna5fsTomador,
                linha1_coluna6fsTomador, linha1_coluna7fsTomador, linha1_coluna8fsTomador, linha1_coluna9fsTomador]
        };

        var fsTomador = {
            xtype: 'fieldset',
            id: 'fsTomador',
            name: 'fsTomador',
        //autoHeight: true,
            width: 1081,
            title: 'Tomador',
            items: [linha1fsTomador]
        };


        var linha1DadosFiscais_coluna1 = { width: 542.5, layout: 'form', border: false, items: [fsRemetente] };
        var linha1DadosFiscais_coluna2 = { width: 542.5, layout: 'form', border: false, items: [fsDestinatario] };

        var linha1DadosFiscais = {
            layout: 'column',
            border: false,
            items: [linha1DadosFiscais_coluna1, linha1DadosFiscais_coluna2]
        };

        var linha2DadosFiscais_coluna1 = { width: 1090, layout: 'form', border: false, items: [fsTomador] };

        var linha2DadosFiscais = {
            layout: 'column',
            border: false,
            items: [linha2DadosFiscais_coluna1]
        };

        var linhaTabDadosFiscais = {
            layout: 'form',
            height: 230,
            autoScroll: true,
            border: false,
            bodyStyle: 'padding: 5px',
            width: 1100,
            items: [linha1DadosFiscais, linha2DadosFiscais]
        };


        var lblExpedidor = {
            xtype: 'textfield',
            fieldLabel: 'Expedidor',
            id: 'lblNomeExpedidor',
            name: 'lblNomeExpedidor',
            value: '',
            readOnly: true,
            width: 280
        };

        var lblCPF_CNPJ_Expedidor = {
            xtype: 'textfield',
            fieldLabel: 'CPF/CNPJ',
            id: 'lblDocumentoExpedidor',
            name: 'lblDocumentoExpedidor',
            value: '',
            width: 110,
            readOnly: true
        };

        var lblInscEstExpedidor = {
            xtype: 'textfield',
            fieldLabel: 'Insc. Est',
            id: 'lblInscEstExpedidor',
            name: 'lblInscEstExpedidor',
            value: '',
            width: 80,
            readOnly: true
        };

        var lblUFExpedidor = {
            xtype: 'textfield',
            fieldLabel: 'UF',
            id: 'lblUFExpedidor',
            name: 'lblUFExpedidor',
            value: '',
            width: 30,
            readOnly: true
        };

        var linha1_coluna1fsExpedidor = { width: 285, layout: 'form', border: false, items: [lblExpedidor] };
        var linha1_coluna2fsExpedidor = { width: 115, layout: 'form', border: false, items: [lblCPF_CNPJ_Expedidor] };
        var linha1_coluna3fsExpedidor = { width: 85, layout: 'form', border: false, items: [lblInscEstExpedidor] };
        var linha1_coluna4fsExpedidor = { width: 30, layout: 'form', border: false, items: [lblUFExpedidor] };

        var lblEnderecoExpedidor = {
            xtype: 'textfield',
            fieldLabel: 'Endereço',
            id: 'lblEnderecoExpedidor',
            name: 'lblEnderecoExpedidor',
            value: '',
            readOnly: true,
            width: 200
        };

        var lblMunicipioExpedidor = {
            xtype: 'textfield',
            fieldLabel: 'Município',
            id: 'lblMunicipioExpedidor',
            name: 'lblMunicipioExpedidor',
            value: '',
            readOnly: true,
            width: 110
        };

        var lblCEPExpedidor = {
            xtype: 'textfield',
            fieldLabel: 'CEP',
            id: 'lblCEPExpedidor',
            name: 'lblCEPExpedidor',
            value: '',
            width: 65,
            readOnly: true
        };

        var lblPaisExpedidor = {
            xtype: 'textfield',
            fieldLabel: 'País',
            id: 'lblPaisExpedidor',
            name: 'lblPaisExpedidor',
            value: '',
            width: 30,
            readOnly: true
        };

        var lblFoneExpedidor = {
            xtype: 'textfield',
            fieldLabel: 'Fone',
            id: 'lblFoneExpedidor',
            name: 'lblFoneExpedidor',
            value: '',
            width: 90,
            readOnly: true
        };

        var linha2_coluna1fsExpedidor = { width: 205, layout: 'form', border: false, items: [lblEnderecoExpedidor] };
        var linha2_coluna2fsExpedidor = { width: 115, layout: 'form', border: false, items: [lblMunicipioExpedidor] };
        var linha2_coluna3fsExpedidor = { width: 70, layout: 'form', border: false, items: [lblCEPExpedidor] };
        var linha2_coluna4fsExpedidor = { width: 35, layout: 'form', border: false, items: [lblPaisExpedidor] };
        var linha2_coluna5fsExpedidor = { width: 91, layout: 'form', border: false, items: [lblFoneExpedidor] };

        var linha1fsExpedidor = {
            layout: 'column',
            border: false,
            items: [linha1_coluna1fsExpedidor, linha1_coluna2fsExpedidor, linha1_coluna3fsExpedidor, linha1_coluna4fsExpedidor]
        };

        var linha2fsExpedidor = {
            layout: 'column',
            border: false,
            items: [linha2_coluna1fsExpedidor, linha2_coluna2fsExpedidor, linha2_coluna3fsExpedidor, linha2_coluna4fsExpedidor, linha2_coluna5fsExpedidor]
        };

        var clsExpedidor = 'none'; //= alteracaoExpedidor == "true" ? 'campoCorrigido' : 'none';

        /*if(validacaoExpedidor == "false") {
        clsExpedidor = 'campoInconforme';
    }*/

        var fsExpedidor = {
            xtype: 'fieldset',
            id: 'fsExpedidor',
            name: 'fsExpedidor',
            autoHeight: true,
            width: 540,
            title: 'Expedidor',
            cls: clsExpedidor,
            items: [linha1fsExpedidor, linha2fsExpedidor]
        };

        var lblRecebedor = {
            xtype: 'textfield',
            fieldLabel: 'Recebedor',
            id: 'lblNomeRecebedor',
            name: 'lblNomeRecebedor',
            value: '',
            readOnly: true,
            width: 280
        };

        var lblCPF_CNPJ_Recebedor = {
            xtype: 'textfield',
            fieldLabel: 'CPF/CNPJ',
            id: 'lblDocumentoRecebedor',
            name: 'lblDocumentoRecebedor',
            value: '',
            width: 110,
            readOnly: true
        };

        var lblInscEstRecebedor = {
            xtype: 'textfield',
            fieldLabel: 'Insc. Est',
            id: 'lblInscEstRecebedor',
            name: 'lblInscEstRecebedor',
            value: '',
            width: 80,
            readOnly: true
        };

        var lblUFRecebedor = {
            xtype: 'textfield',
            fieldLabel: 'UF',
            id: 'lblUFRecebedor',
            name: 'lblUFRecebedor',
            value: '',
            width: 30,
            readOnly: true
        };

        var linha1_coluna1fsRecebedor = { width: 285, layout: 'form', border: false, items: [lblRecebedor] };
        var linha1_coluna2fsRecebedor = { width: 115, layout: 'form', border: false, items: [lblCPF_CNPJ_Recebedor] };
        var linha1_coluna3fsRecebedor = { width: 85, layout: 'form', border: false, items: [lblInscEstRecebedor] };
        var linha1_coluna4fsRecebedor = { width: 30, layout: 'form', border: false, items: [lblUFRecebedor] };

        var lblEnderecoRecebedor = {
            xtype: 'textfield',
            fieldLabel: 'Endereço',
            id: 'lblEnderecoRecebedor',
            name: 'lblEnderecoRecebedor',
            value: '',
            readOnly: true,
            width: 200
        };

        var lblMunicipioRecebedor = {
            xtype: 'textfield',
            fieldLabel: 'Município',
            id: 'lblMunicipioRecebedor',
            name: 'lblMunicipioRecebedor',
            value: '',
            readOnly: true,
            width: 110
        };

        var lblCEPRecebedor = {
            xtype: 'textfield',
            fieldLabel: 'CEP',
            id: 'lblCEPRecebedor',
            name: 'lblCEPRecebedor',
            value: '',
            width: 65,
            readOnly: true
        };

        var lblPaisRecebedor = {
            xtype: 'textfield',
            fieldLabel: 'País',
            id: 'lblPaisRecebedor',
            name: 'lblPaisRecebedor',
            value: '',
            width: 30,
            readOnly: true
        };

        var lblFoneRecebedor = {
            xtype: 'textfield',
            fieldLabel: 'Fone',
            id: 'lblFoneRecebedor',
            name: 'lblFoneRecebedor',
            value: '',
            width: 90,
            readOnly: true
        };

        var linha2_coluna1fsRecebedor = { width: 205, layout: 'form', border: false, items: [lblEnderecoRecebedor] };
        var linha2_coluna2fsRecebedor = { width: 115, layout: 'form', border: false, items: [lblMunicipioRecebedor] };
        var linha2_coluna3fsRecebedor = { width: 70, layout: 'form', border: false, items: [lblCEPRecebedor] };
        var linha2_coluna4fsRecebedor = { width: 35, layout: 'form', border: false, items: [lblPaisRecebedor] };
        var linha2_coluna5fsRecebedor = { width: 91, layout: 'form', border: false, items: [lblFoneRecebedor] };

        var linha1fsRecebedor = {
            layout: 'column',
            border: false,
            items: [linha1_coluna1fsRecebedor, linha1_coluna2fsRecebedor, linha1_coluna3fsRecebedor, linha1_coluna4fsRecebedor]
        };

        var linha2fsRecebedor = {
            layout: 'column',
            border: false,
            items: [linha2_coluna1fsRecebedor, linha2_coluna2fsRecebedor, linha2_coluna3fsRecebedor, linha2_coluna4fsRecebedor, linha2_coluna5fsRecebedor]
        };

        var clsRecebedor = 'none'; // alteracaoRecebedor == "true" ? 'campoCorrigido' : 'none';

        /*if(validacaoRecebedor == "false") {
        clsRecebedor = 'campoInconforme';
    }*/

        var fsRecebedor = {
            xtype: 'fieldset',
            id: 'fsRecebedor',
            name: 'fsRecebedor',
            autoHeight: true,
            width: 540,
            title: 'Recebedor',
            cls: clsRecebedor,
            items: [linha1fsRecebedor, linha2fsRecebedor]
        };

        var linha1DadosOperacionais_coluna1 = { width: 542.5, layout: 'form', border: false, items: [fsExpedidor] };
        var linha1DadosOperacionais_coluna2 = { width: 542.5, layout: 'form', border: false, items: [fsRecebedor] };

        var linha1DadosOperacionais = {
            layout: 'column',
            border: false,
            items: [linha1DadosOperacionais_coluna1, linha1DadosOperacionais_coluna2]
        };

        var linhaTabDadosOperacionais = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            width: 1100,
            items: [linha1DadosOperacionais]
        };

        var lblBaseCalculo = {
            xtype: 'textfield',
            fieldLabel: 'Base de Cálculo',
            id: 'lblBaseCalculo',
            name: 'lblBaseCalculo',
            value: '',
            readOnly: true,
            width: 100
        };

        var lblAliqIcms = {
            xtype: 'textfield',
            fieldLabel: 'Alíq. ICMS',
            id: 'lblAliqIcms',
            name: 'lblAliqIcms',
            value: '',
            readOnly: true,
            width: 100
        };

        var lblValorIcms = {
            xtype: 'textfield',
            fieldLabel: 'Valor ICMS',
            id: 'lblVlrIcms',
            name: 'lblVlrIcms',
            value: '',
            readOnly: true,
            width: 100
        };

        var lblDesconto = {
            xtype: 'textfield',
            fieldLabel: 'Desconto',
            id: 'lblVlrDesconto',
            name: 'lblVlrDesconto',
            value: '',
            width: 100,
            readOnly: true
        };

        var lblValorTotal = {
            xtype: 'textfield',
            fieldLabel: 'Valor Total',
            id: 'lblVlrTotal',
            name: 'lblVlrTotal',
            value: '',
            width: 100,
            readOnly: true
        };

        var lblValorReceber = {
            xtype: 'textfield',
            fieldLabel: 'Valor a Receber',
            id: 'lblVlrReceber',
            name: 'lblVlrReceber',
            value: '',
            width: 100,
            readOnly: true
        };

        var lblPesoVagaoCte = {
            xtype: 'textfield',
            fieldLabel: 'Peso do vagão',
            id: 'lblPesoVagaoCte',
            name: 'lblPesoVagaoCte',
            value: '',
            width: 100,
            readOnly: true
        };

        var lblTarifaCte = {
            xtype: 'textfield',
            fieldLabel: 'Valor da Tarifa',
            id: 'lblTarifaCte',
            name: 'lblTarifaCte',
            value: '',
            width: 100,
            readOnly: true
        };

        var linha1_coluna1fsPrestacaoServicos = { width: 105, layout: 'form', border: false, items: [lblPesoVagaoCte] };
        var linha1_coluna2fsPrestacaoServicos = { width: 105, layout: 'form', border: false, items: [lblBaseCalculo] };
        var linha1_coluna3fsPrestacaoServicos = { width: 105, layout: 'form', border: false, items: [lblAliqIcms] };
        var linha1_coluna4fsPrestacaoServicos = { width: 105, layout: 'form', border: false, items: [lblValorIcms] };
        var linha1_coluna5fsPrestacaoServicos = { width: 105, layout: 'form', border: false, items: [lblTarifaCte] };
        var linha1_coluna6fsPrestacaoServicos = { width: 105, layout: 'form', border: false, items: [lblDesconto] };
        var linha1_coluna7fsPrestacaoServicos = { width: 105, layout: 'form', border: false, items: [lblValorTotal] };
        var linha1_coluna8fsPrestacaoServicos = { width: 100, layout: 'form', border: false, items: [lblValorReceber] };

        var linha1fsPrestacaoServicos = {
            layout: 'column',
            border: false,
            items: [linha1_coluna1fsPrestacaoServicos, linha1_coluna2fsPrestacaoServicos, linha1_coluna3fsPrestacaoServicos,
                linha1_coluna4fsPrestacaoServicos, linha1_coluna5fsPrestacaoServicos, linha1_coluna6fsPrestacaoServicos,
                linha1_coluna7fsPrestacaoServicos, linha1_coluna8fsPrestacaoServicos]
        };

        var fsValoresDaPrestacaoDeServico = {
            xtype: 'fieldset',
            id: 'fsValoresDaPrestacaoDeServico',
            name: 'fsValoresDaPrestacaoDeServico',
            autoHeight: true,
            width: '100%',
            title: 'Valores da Prestação de Serviço',
            items: [linha1fsPrestacaoServicos]
        };

        var linhaTabValores = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding: 5px',
            width: 870,
            items: [fsValoresDaPrestacaoDeServico]
        };

        var dsNotasVagoes = new Ext.data.JsonStore({
                url: '<%= Url.Action("ObterNota") %>',
                root: "Items",
                fields: [
                    'Id',
                    'TipoDocumento',
                    'SerieNotaFiscal',
                    'NumeroNotaFiscal',
                    'PesoTotal',
                    'PesoRateio',
                    'CnpjRemetente',
                    'ChaveNfe',
                    'Conteiner'
                ]
            });

        gridNotasVagoes = new Ext.grid.GridPanel({
                autoHeight: true,
                autoLoadGrid: false,
                width: 1040,
                columnLines: true,
                stripeRows: true,
                loadMask: { msg: App.Resources.Web.Carregando },
                viewConfig: {
                    forceFit: true,
                    emptyText: 'Não há vagões(s) para exibição.'
                },
                store: dsNotasVagoes,
                columns: [
                    { dataIndex: "Id", hidden: true },
                    { header: 'ChaveNfe', dataIndex: 'ChaveNfe', hidden: true },
                    { header: 'TIPO DOCUMENTO', width: 60, dataIndex: "TipoDocumento", sortable: false },
                    { header: 'CPF/CNPJ EMITENTE', width: 100, dataIndex: "CnpjRemetente", sortable: false },
                    { header: 'SÉRIE', width: 50, dataIndex: "SerieNotaFiscal", sortable: false },
                    { header: 'DOCUMENTO', width: 50, dataIndex: "NumeroNotaFiscal", sortable: false },
                    { header: 'PESO REAL', dataIndex: "PesoTotal", sortable: false, renderer: RendererFloat },
                    { header: 'PESO RATEADO', dataIndex: "PesoRateio", sortable: false, renderer: RendererFloat },
                    { header: 'CONTEINER', dataindex: 'Conteiner', sortable: false }
                ],
                listeners: {
                    rowdblclick: function(grid, rowIndex, e) {

                        semaforo = null;
                        siglaUfSemaforo = null;
                        periodoHoraSemaforo = null;
                        var sel = gridNotasVagoes.getSelectionModel().getSelected();
                        var idNf = sel.data.Id;
                        ObterNfe(idNf);
                    }
                }
            });

        var clFsDocumentosOriginariosCte = {
            layout: 'form',
            id: 'clFsDocumentosOriginariosCte',
            name: 'clFsDocumentosOriginariosCte',
            autoWidth: true,
            height: 175,
            border: false,
            autoScroll: true,
            items: [gridNotasVagoes]
        };

        var fsDocumentosOriginarios = {
            xtype: 'fieldset',
            id: 'fsDocumentosOriginarios',
            name: 'fsDocumentosOriginarios',
            height: 200,
            width: 1080,
            title: 'Documentos Originários',
            items: [clFsDocumentosOriginariosCte]
        };

        var linhaTabDocumentos = {
            layout: 'form',
            border: false,
            width: 1100,
            bodyStyle: 'padding:5px',
            items: [fsDocumentosOriginarios]
        };

        function rendererProcedimentoCteStatus(value, metaData, record, rowIndex, colIndex, store) 
        {
            var links = "";
            
            if(record.data.Procedimentos != null) 
            {
                for (var i = 0; i < record.data.Procedimentos.length; i++) {
                 
                    if (record.data.Procedimentos[i] != null && record.data.Procedimentos[i] != undefined && $.trim(record.data.Procedimentos[i].LinkProcedimento) != "") 
                    {
                        var tagA = "<a href='"+record.data.Procedimentos[i].LinkProcedimento+"'>"+record.data.Procedimentos[i].NomeProcedimento+" | "+record.data.Procedimentos[i].ResponsavelProcedimento+"</a>";
                        
                        if ($.trim(links) == "") {
                            links = tagA;
                        }
                        else {
                            links += "<br/>" + tagA;
                        }
                    }
                }
            }
            return links;
        }
        
        gridResultadoStatusCte = new Ext.grid.GridPanel({
                id: 'gridResultadoStatusCte',
                name: 'gridResultadoStatusCte',
                autoLoadGrid: false,
                autoScroll: true,
                height: 200,
                store: dsStatusCte,
                stripeRows: true,
                loadMask: { msg: App.Resources.Web.Carregando },
                columns: [
                    { header: 'Data e Hora', dataIndex: 'DataHora', width: 110, sortable: false },
                    { header: 'Status', dataIndex: 'StatusCte', width: 300, sortable: false },
                    { header: 'Status Sefaz', dataIndex: 'StatusSefaz', width: 300, sortable: false },
                    { header: 'Usuario', dataIndex: 'Usuario', width: 200, sortable: false },
                    { header: 'Procedimentos', dataIndex: 'Procedimentos', width: 250, sortable: false, renderer: rendererProcedimentoCteStatus }
                ]
            });

        gridResultadoStatusCte.getStore().proxy.on('beforeload', function(p, params) {
            params['filter[0].Campo'] = 'idCte';
            params['filter[0].Valor'] = idCteSelecionada;
            params['filter[0].FormaPesquisa'] = 'Start';
        });

        var linhaTabStatusCte = {
            layout: 'form',
            width: '100%',
            bodyStyle: 'padding:5px',
            border: false,
            items: [gridResultadoStatusCte]
        };

        tabDetalhesCte = new Ext.TabPanel({
                activeTab: 0,
                frame: false,
                width: 1100,
                height: 230,
                plain: true,
                items: [{
                    title: 'CT-e',
                    layout: 'vbox',
                    items: [linhasTabCte]
                }, {
                    title: 'Dados Fiscais',
                    layout: 'vbox',
                    items: [linhaTabDadosFiscais]
                }, {
                    title: 'Dados Operacionais',
                    layout: 'vbox',
                    items: [linhaTabDadosOperacionais]
                }, {
                    title: 'Valores',
                    layout: 'vbox',
                    items: [linhaTabValores]
                }, {
                    title: 'Documentos originários',
                    layout: 'vbox',
                    items: [linhaTabDocumentos]
                }, {
                    title: 'Status',
                    layout: 'vbox',
                    items: [linhaTabStatusCte]
                }, {
                    title: 'Diário de Bordo',
                    layout: 'vbox',
                    items: [linhaTabDiarioBordo]
                }]
            });

        linhaDetalhesCte.push(tabDetalhesCte);

        var botaoGerarPdfCte = {
            text: 'Gerar Pdf',
            id: 'gerarPdfCte',
            iconCls: 'icon-printer',
            name: 'gerarPdfCte',
            handler: function() {
                var url = "<%= Url.Action("GerarZipPdf") %> ?ids=" + idCteSelecionada
                window.open(url, "Pdf");
            }
        };
        var botaoGerarXmlCte = {
            text: 'Gerar Xml',
            id: 'gerarXmlCte',
            iconCls: 'icon-printer',
            name: 'gerarXmlCte',
            handler: function() {
                var url = "<%= Url.Action("GerarZipXml") %> ?ids=" + idCteSelecionada
                window.open(url, "Xml");
            }
        };

        var linhaBotoesCte = {
            layout: 'column',
            border: false,
            bodyStyle: 'padding-bottom:0px;',
            buttons: [botaoGerarPdfCte, botaoGerarXmlCte],
            buttonAlign: 'center'
        };

        fieldSetResultadoCte = {
            xtype: 'fieldset',
            id: 'fsResultadoBuscaCte',
            name: 'fsResultadoBuscaCte',
            title: 'Detalhe Cte',
            width: 1130,
            bodyStyle: 'padding:5px',
            border: true,
            items: [linhaDetalhesCte, linhaBotoesCte]
        };
    });
    </script>

   