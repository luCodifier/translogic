﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<script type="text/javascript">
    var linhaTabNfe = null;
    var gridDocsNfes = null;

    function fctLimparCampos()
	{
        Ext.getCmp("lblChaveNfe").setValue("");
		Ext.getCmp("lblSerieNfe").setValue("");
        Ext.getCmp("lblNumeroNfe").setValue("");
        Ext.getCmp("lblDataEmissaoNfe").setValue("");
        Ext.getCmp("lblValorTotalNfe").setValue("");
        Ext.getCmp("lblOrigemlNfe").setValue("");
        Ext.getCmp("lblCNPJEmit").setValue("");
        Ext.getCmp("lblRazaoEmit").setValue("");
        Ext.getCmp("lblInscEmit").setValue("");
        Ext.getCmp("lblUFEmit").setValue("");
        Ext.getCmp("lblCNPJDest").setValue("");
        Ext.getCmp("fieldRazaoDest").setValue("");
        Ext.getCmp("lblInscDest").setValue("");
        Ext.getCmp("lblUFDest").setValue("");
        /*Ext.getCmp("lblCNPJTrans").setValue("");
        Ext.getCmp("lblRazaoTrans").setValue("");
        Ext.getCmp("lblMunicipioTrans").setValue("");
        Ext.getCmp("lblUFTrans").setValue("");*/
        Ext.getCmp("lblPesoBruto").setValue("");
        Ext.getCmp("lblVolume").setValue("");
        Ext.getCmp("lblPesoDisponivel").setValue("");
        Ext.getCmp("lblVolumeDisponivel").setValue("");
	}
    
    function fctPreencherCampos(store)
	{				
        Ext.getCmp("lblChaveNfe").setValue(store.ChaveNfe);
		Ext.getCmp("lblSerieNfe").setValue(store.SerieNotaFiscal);
        Ext.getCmp("lblNumeroNfe").setValue(store.NumeroNotaFiscal);
        Ext.getCmp("lblDataEmissaoNfe").setValue(store.DataEmissao);
        Ext.getCmp("lblValorTotalNfe").setValue(store.Valor);
        Ext.getCmp("lblOrigemlNfe").setValue(store.Origem);
        Ext.getCmp("lblCNPJEmit").setValue(store.CnpjEmitente);
        Ext.getCmp("lblRazaoEmit").setValue(store.RazaoSocialEmitente);
        Ext.getCmp("lblInscEmit").setValue(store.InscricaoEstadualEmitente);
        Ext.getCmp("lblUFEmit").setValue(store.UfEmitente);
        Ext.getCmp("lblCNPJDest").setValue(store.CnpjDestinatario);
        Ext.getCmp("fieldRazaoDest").setValue(store.RazaoSocialDestinatario);
        Ext.getCmp("lblInscDest").setValue(store.InscricaoEstadualDestinatario);
        Ext.getCmp("lblUFDest").setValue(store.UfDestinatario);
        /*Ext.getCmp("lblCNPJTrans").setValue(store.CnpjTransportadora);
        Ext.getCmp("lblRazaoTrans").setValue(store.NomeTransportadora);
        Ext.getCmp("lblMunicipioTrans").setValue(store.MunicipioTransportadora);
        Ext.getCmp("lblUFTrans").setValue(store.UfTransportadora);*/
        Ext.getCmp("lblPesoBruto").setValue(store.PesoBruto.toString().replace(",", "."));
        Ext.getCmp("lblVolume").setValue(store.Volume.toString().replace(",", "."));
        Ext.getCmp("lblPesoDisponivel").setValue(store.PesoDisponivel);
        Ext.getCmp("lblVolumeDisponivel").setValue(store.VolumeDisponivel);	    
	}
    
    function ObterNfe(idNotaFiscal) 
    {
        tabResultados.setActiveTab(4);
        
        Ext.getCmp("tabResNfe").getEl().mask("Buscando NFe na base de dados ...", "x-mask-loading");

			Ext.Ajax.request({ 
				url: "<% =Url.Action("ObterDadosNfe") %>",
				success: function(response, options) {
						var result = Ext.decode(response.responseText);
							 
						fctLimparCampos();

						if(result.Erro)
						{
							Ext.Msg.alert("Ocorreu um erro ao buscar os dados da nota",result.Mensagem);
						}
						else{
							fctPreencherCampos(result);
						}
						Ext.getCmp("tabResNfe").getEl().unmask();
				},
				dataType: 'json',		        
				failure: function(conn, data){
					Ext.Msg.alert("Mensagem de Erro","Ocorreu um erro inesperado.");
					Ext.getCmp("tabResNfe").getEl().unmask();
				},
				method: "POST",
				params: { idNota: idNotaFiscal }
			});   
    }
    
    /*********************** GRID **************************/
        
        var dsDocsNfes = new Ext.data.JsonStore({
            url: '<%= Url.Action("ObterNota") %>',
            root: "Items",
            fields: [
                'Id',
                'TipoDocumento',
                'SerieNotaFiscal',
                'NumeroNotaFiscal',
                'Vagao',
                'PesoTotal',
                'PesoRateio',
                'CnpjRemetente',
                'ChaveNfe'
            ]
        });

        gridDocsNfes = new Ext.grid.GridPanel({
            autoHeight: true,
            width: 955,
            autoScroll: true,
            autoLoadGrid: false,
            columnLines: true,
            //region: 'center',
            stripeRows: true,
            loadMask: { msg: App.Resources.Web.Carregando },
                viewConfig: {
                forceFit: true,
                emptyText: 'Não há Nfe(s) para exibição.'
            },
            store: dsDocsNfes,
            columns: [
                    { dataIndex: "Id", hidden: true},
                    { header: 'TP DOC', width: 70, dataIndex: "TipoDocumento", sortable: false },
                    { header: 'CPF/CNPJ EMIT.', width: 120, dataIndex: "CnpjRemetente", sortable: false },
                    { header: 'SÉRIE', width: 60, dataIndex: "SerieNotaFiscal", sortable: false },
                    { header: 'DOCUMENTO', width: 110, dataIndex: "NumeroNotaFiscal", sortable: false },
                    { header: 'CHAVE', width: 290, dataIndex: 'ChaveNfe', sortable: false },
                    { header: 'VAGÃO', width: 80, dataIndex: "Vagao", sortable: false },
                    { header: 'PESO REAL', width: 110, dataIndex: "PesoTotal", sortable: false, renderer: RendererFloat },
                    { header: 'PESO RAT.', width: 110, dataIndex: "PesoRateio", sortable: false, renderer: RendererFloat }
                ],
            listeners: {
                rowdblclick: function (grid, rowIndex, e) {

                    var sel = gridDocsNfes.getSelectionModel().getSelected();
                    var idNf = sel.data.Id;
                    ObterNfe(idNf);
                }
            }
        });
        /*********************** FIELDSET Documentos Originários - Linha 1 Coluna 1 ***********************************/

        var clFsDocumentosOriginarios = {
            layout: 'form',
            id: 'clFsDocumentosOriginarios',
            name: 'clFsDocumentosOriginarios',
            autoWidth: true, 
            height: 270,
            border: false,
            autoScroll: true,
            items: [gridDocsNfes]
        };
        
        var fsDocumentosOriginariosNfe = {
            xtype: 'fieldset',
            id: 'fsDocumentosOriginariosNfe',
            name: 'fsDocumentosOriginariosNfe',
            height: 370,
            width: 570,
            title: 'Documentos Originários',
            items: [clFsDocumentosOriginarios]
        };

        /************************************** FIELDSET Dados Nfe - Linha 1 Coluna 2 *************************************/
        var lblChaveNfe = {
            xtype: 'textfield',
            fieldLabel: 'Chave Nfe',
            id: 'lblChaveNfe',
            name: 'lblChaveNfe',
            value: '',
            width: 440,
            readOnly: true
        };
        
        var lblSerieNfe = {
            xtype: 'textfield',
            fieldLabel: 'Série',
            id: 'lblSerieNfe',
            name: 'lblSerieNfe',
            value: '',
            width: 50,
            readOnly: true
        };

        var lblNumeroNfe = {
            xtype: 'textfield',
            fieldLabel: 'Número',
            id: 'lblNumeroNfe',
            name: 'lblNumeroNfe',
            value: '',
            width: 70,
            readOnly: true
        };

        var lblDataEmissaoNfe = {
            xtype: 'textfield',
            fieldLabel: 'Data de Emissão',
            id: 'lblDataEmissaoNfe',
            name: 'lblDataEmissaoNfe',
            value: '',
            width: 100,
            readOnly: false
        };

        var lblValorTotalNfe = {
            xtype: 'textfield',
            fieldLabel: 'Valor Total',
            id: 'lblValorTotalNfe',
            name: 'lblValorTotalNfe',
            value: '',
            width: 100,
            readOnly: true
        };

        var lblOrigemlNfe = {
            xtype: 'textfield',
            fieldLabel: 'Origem',
            id: 'lblOrigemlNfe',
            name: 'lblOrigemlNfe',
            value: '',
            width: 100,
            readOnly: true
        };

        var fsDadosNfeLinha1_Coluna1 = { width: 450, layout: 'form', border: false, items: [lblChaveNfe] };
        
        var linha1fsDadosNfe = {
            layout: 'column',
            border: false,
            height: 40,
            items: [fsDadosNfeLinha1_Coluna1]
        };
        
        var fsDadosNfeLinha2_Coluna1 = { width: 75, layout: 'form', border: false, items: [lblNumeroNfe] };
        var fsDadosNfeLinha2_Coluna2 = { width: 55, layout: 'form', border: false, items: [lblSerieNfe] };
        var fsDadosNfeLinha2_Coluna3 = { width: 105, layout: 'form', border: false, items: [lblDataEmissaoNfe] };
        var fsDadosNfeLinha2_Coluna4 = { width: 105, layout: 'form', border: false, items: [lblValorTotalNfe] };
        var fsDadosNfeLinha2_Coluna5 = { width: 105, layout: 'form', border: false, items: [lblOrigemlNfe] };

        var linha2fsDadosNfe = {
            layout: 'column',
            border: false,
            height: 40,
            items: [fsDadosNfeLinha2_Coluna1, fsDadosNfeLinha2_Coluna2, fsDadosNfeLinha2_Coluna3, fsDadosNfeLinha2_Coluna4, fsDadosNfeLinha2_Coluna5]
        };
        
        var fsDadosNfe = {
            xtype: 'fieldset',
            id: 'fsDocumentosDadosNfe',
            name: 'fsDocumentosDadosNfe',
            autoHeight: true,
            width: 535,
            title: 'Dados Nfe',
            items: [linha1fsDadosNfe, linha2fsDadosNfe]
        };

        /************************************** FIELDSET Emitente - Linha 2 Coluna 2 *************************************/

        var lblCNPJEmit = new Ext.form.TextField({
            id: 'lblCNPJEmit',
            fieldLabel: 'CNPJ',
            name: 'lblCNPJEmit',
            readOnly: true,
            width: 120,
            hiddenName: 'lblCNPJEmit'
        });

        var lblRazaoEmit = new Ext.form.TextField({
            id: 'lblRazaoEmit',
            fieldLabel: 'Nome/Razão Social',
            name: 'lblRazaoEmit',
            readOnly: true,
            width: 240,
            hiddenName: 'lblRazaoEmit'
        });

        var lblInscEmit = new Ext.form.TextField({
            id: 'lblInscEmit',
            fieldLabel: 'Inscrição Estadual',
            name: 'lblInscEmit',
            readOnly: true,
            width: 100,
            hiddenName: 'lblInscEmit'
        });

        var lblUFEmit = new Ext.form.TextField({
            id: 'lblUFEmit',
            fieldLabel: 'UF',
            name: 'lblUFEmit',
            readOnly: true,
            width: 30,
            hiddenName: 'lblUFEmit'
        });

        var fsEmitenteDadosNfeColuna1 = { width: 125, layout: 'form', border: false, items: [lblCNPJEmit] };
        var fsEmitenteDadosNfeColuna2 = { width: 245, layout: 'form', border: false, items: [lblRazaoEmit] };
        var fsEmitenteDadosNfeColuna3 = { width: 105, layout: 'form', border: false, items: [lblInscEmit] };
        var fsEmitenteDadosNfeColuna4 = { width: 35, layout: 'form', border: false, items: [lblUFEmit] };

        var linha1fsEmitenteDadosNfe = {
            layout: 'column',
            border: false,
            height: 40,
            items: [fsEmitenteDadosNfeColuna1, fsEmitenteDadosNfeColuna2, fsEmitenteDadosNfeColuna3, fsEmitenteDadosNfeColuna4]
        };

        var fsEmitenteDadosNfe = {
            xtype: 'fieldset',
            id: 'fsEmitenteDadosNfe',
            name: 'fsEmitenteDadosNfe',
            autoHeight: true,
            width: 535,
            title: 'Emitente',
            items: [linha1fsEmitenteDadosNfe]
        };

        /************************************** FIELDSET Destinatário - Linha 3 Coluna 2 *************************************/

        var lblCNPJDest = new Ext.form.TextField({
            id: 'lblCNPJDest',
            fieldLabel: 'CNPJ',
            name: 'lblCNPJDest',
            readOnly: true,
            width: 120,
            hiddenName: 'lblCNPJDest'
        });

        var fieldRazaoDest = new Ext.form.TextField({
            id: 'fieldRazaoDest',
            fieldLabel: 'Nome/Razão Social',
            name: 'fieldRazaoDest',
            readOnly: true,
            width: 240,
            hiddenName: 'fieldRazaoDest'
        });

        var lblInscDest = new Ext.form.TextField({
            id: 'lblInscDest',
            fieldLabel: 'Inscrição Estadual',
            name: 'lblInscDest',
            readOnly: true,
            width: 100,
            hiddenName: 'lblInscDest'
        });

        var lblUFDest = new Ext.form.TextField({
            id: 'lblUFDest',
            fieldLabel: 'UF',
            name: 'lblUFDest',
            readOnly: true,
            width: 30,
            hiddenName: 'lblUFDest'
        });

        var fsDestinatarioDadosNfeColuna1 = { width: 125, layout: 'form', border: false, items: [lblCNPJDest] };
        var fsDestinatarioDadosNfeColuna2 = { width: 245, layout: 'form', border: false, items: [fieldRazaoDest] };
        var fsDestinatarioDadosNfeColuna3 = { width: 105, layout: 'form', border: false, items: [lblInscDest] };
        var fsDestinatarioDadosNfeColuna4 = { width: 35, layout: 'form', border: false, items: [lblUFDest] };

        var linha1fsDestinatarioDadosNfe = {
            layout: 'column',
            border: false,
            height: 40,
            items: [fsDestinatarioDadosNfeColuna1, fsDestinatarioDadosNfeColuna2, fsDestinatarioDadosNfeColuna3, fsDestinatarioDadosNfeColuna4]
        };

        var fsDestinatarioDadosNfe = {
            xtype: 'fieldset',
            id: 'fsDestinatarioDadosNfe',
            name: 'fsDestinatarioDadosNfe',
            autoHeight: true,
            width: 535,
            title: 'Destinatario',
            items: [linha1fsDestinatarioDadosNfe]
        };

        /************************************** FIELDSET Transporte - Linha 4 Coluna 2 *************************************/

        /*
        var lblCNPJTrans = new Ext.form.TextField({
            id: 'lblCNPJTrans',
            fieldLabel: 'CNPJ',
            name: 'lblCNPJTrans',
            readOnly: true,
            width: 120,
            hiddenName: 'lblCNPJTrans'
        });
        
        var lblRazaoTrans = new Ext.form.TextField({
            id: 'lblRazaoTrans',
            fieldLabel: 'Nome/Razão Social',
            name: 'lblRazaoTrans',
            readOnly: true,
            width: 240,
            hiddenName: 'lblRazaoTrans'
        });

        var lblMunicipioTrans = new Ext.form.TextField({
            id: 'lblMunicipioTrans',
            fieldLabel: 'Município',
            name: 'lblMunicipioTrans',
            readOnly: true,
            width: 100,
            hiddenName: 'lblMunicipioTrans'
        });

        var lblUFTrans = new Ext.form.TextField({
            id: 'lblUFTrans',
            fieldLabel: 'UF',
            name: 'lblUFTrans',
            readOnly: true,
            width: 30,
            hiddenName: 'lblUFTrans'
        });*/

        var lblPesoBruto = {
            xtype: 'masktextfield',
            id: 'lblPesoBruto',
            fieldLabel: 'Peso Bruto',
            name: 'lblPesoBruto',
            readOnly: true,
            width: 120,
            allowBlank: false,
            mask: '990,000',
            money: true,
            hiddenName: 'lblPesoBruto'
        };

        var lblVolume = {
            xtype: 'masktextfield',
            id: 'lblVolume',
            fieldLabel: 'Volume ',
            name: 'lblVolume',
            readOnly: true,
            width: 117.5,
            allowBlank: false,
            mask: '990,000',
            money: true,
            hiddenName: 'lblVolume'
        };

        var lblPesoDisponivel = new Ext.form.TextField({
            id: 'lblPesoDisponivel',
            fieldLabel: 'Peso Disponível ',
            name: 'lblPesoDisponivel',
            readOnly: true,
            width: 117.5,
            hiddenName: 'lblPesoDisponivel'
        });

        var lblVolumeDisponivel = new Ext.form.TextField({
            id: 'lblVolumeDisponivel',
            fieldLabel: 'Vol. Disponível ',
            name: 'lblVolumeDisponivel',
            readOnly: true,
            width: 135,
            hiddenName: 'lblVolumeDisponivel'
        });
/*
        var linha1fsTransporteDadosNfeColuna1 = { width: 125, layout: 'form', border: false, items: [lblCNPJTrans] };
        var linha1fsTransporteDadosNfeColuna2 = { width: 245, layout: 'form', border: false, items: [lblRazaoTrans] };
        var linha1fsTransporteDadosNfeColuna3 = { width: 105, layout: 'form', border: false, items: [lblMunicipioTrans] };
        var linha1fsTransporteDadosNfeColuna4 = { width: 35, layout: 'form', border: false, items: [lblUFTrans] };
        */
        var linha2fsTransporteDadosNfeColuna1 = { width: 125, layout: 'form', border: false, items: [lblPesoBruto] };
        var linha2fsTransporteDadosNfeColuna2 = { width: 122.5, layout: 'form', border: false, items: [lblPesoDisponivel] };
        var linha2fsTransporteDadosNfeColuna3 = { width: 122.5, layout: 'form', border: false, items: [lblVolume] };
        var linha2fsTransporteDadosNfeColuna4 = { width: 140, layout: 'form', border: false, items: [lblVolumeDisponivel] };
        
        /*var linha1fsTransporteDadosNfe = {
            layout: 'column',
            border: false,
            height: 40,
            bodyStyle: 'padding-bottom: 10px',
            items: [linha1fsTransporteDadosNfeColuna1, linha1fsTransporteDadosNfeColuna2, linha1fsTransporteDadosNfeColuna3, linha1fsTransporteDadosNfeColuna4]
        };*/

        var linha2fsTransporteDadosNfe = {
            layout: 'column',
            border: false,
            height: 40,
            items: [linha2fsTransporteDadosNfeColuna1, linha2fsTransporteDadosNfeColuna2, linha2fsTransporteDadosNfeColuna3, linha2fsTransporteDadosNfeColuna4]
        };
        
        var fsTransporteDadosNfe = {
            xtype: 'fieldset',
            autoHeight: true,
            width: 535,
            title: 'Transporte',
            items: [linha2fsTransporteDadosNfe]
        };


        /***************************************************LAYOUT**********************************************************/
        
        var linha1DadosNfe_coluna1 = { width: 575, layout: 'form', border: false, items: [fsDocumentosOriginariosNfe] };
        var linha1DadosNfe_coluna2 = { width: 535, layout: 'form', border: false, items: [fsDadosNfe] };
        var linha2DadosNfe_coluna2 = { width: 535, layout: 'form', border: false, items: [fsEmitenteDadosNfe] };
        var linha3DadosNfe_coluna2 = { width: 535, layout: 'form', border: false, items: [fsDestinatarioDadosNfe] };
        var linha4DadosNfe_coluna2 = { width: 535, layout: 'form', border: false, items: [fsTransporteDadosNfe] };

        var linha1DadosNfe = {
            layout: 'column',
            border: false,
            items: [linha1DadosNfe_coluna1, linha1DadosNfe_coluna2, linha2DadosNfe_coluna2, linha3DadosNfe_coluna2, linha4DadosNfe_coluna2]
        };

        linhaTabNfe = {
            layout:'form',
            border: false,
            items: [linha1DadosNfe]
        };
        
</script>