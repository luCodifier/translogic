﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Interna.Master" Inherits="System.Web.Mvc.ViewPage" %>
<%@ Import Namespace="System.Security.Policy" %>
<%@ Import Namespace="Translogic.Modules.Core.Domain.Model.Diversos.Mdfe" %>

<asp:Content runat="server" ID="HeadContent" ContentPlaceHolderID="HeadContent">

<script type="text/javascript">
    /************************* STORE **************************/
    var storeChaves = new Ext.data.JsonStore({
        remoteSort: true,
        root: "Items",
        fields: [
            'Chave',
            'Erro',
            'Mensagem'
        ]
    });

    function TratarTeclaEnter(f, e) {
        if (e.getKey() == e.ENTER) {
            Pesquisar();
        }
    }

    function RendererFloat(val) {
        return Ext.util.Format.number(val, '0.000,000/i');
    }
    
    function statusRenderer(val)
    {
        switch(val){
        case "AUT":
            return "<img src='<%=Url.Images("Icons/tick.png") %>' alt='MDF-e Aprovado'>";
            break;
        case "AGE":
        case "AGC":
            return "<img src='<%=Url.Images("Icons/application_go.png") %>' alt='Aguardando Cancelamento/Encerramento'>";
            break;
        case "EAE":
            return "<img src='<%=Url.Images("Icons/email_go.png") %>' alt='Enviado arquivo MDF-e para Sefaz.'>";
            break;
        case "EAR":
            return "<img src='<%=Url.Images("Icons/arrow_redo_yellow.png") %>' alt='Erro Autorizado reenvio do MDF-e.'>";
            break;            
        case "CAN":
            return "<img src='<%=Url.Images("Icons/cancel.png") %>' alt='MDF-e Cancelado '>";
            break;                        
        case "ENC":
            return "<img src='<%=Url.Images("Icons/delete.png") %>' alt='MDF-e Encerrado'>";
            break;                                                                                 
        case "ERR":
            return "<img src='<%=Url.Images("Icons/cross.png") %>' alt='Erro na geração do MDF-e.'>";
            break; 
        case "PGC":
            return "<img src='<%=Url.Images("Icons/information.png") %>' alt='MDf-e aguardando a geração automática da numeração / chave.'>";
            break; 	           
        default:
            return "<img src='<%=Url.Images("Icons/alert.png") %>' alt='MDF-e em processamento.'>";
            break;
        }
    }
</script>
<!-- User Controls componentes de Layout -->

<%Html.RenderPartial("Semaforos"); %>
<%Html.RenderPartial("GridsResultados"); %>
<%Html.RenderPartial("Filtros"); %>

<!-- Fim User Controls componentes de Layout -->

<style type="text/css">
.x-panel-btns
{
    padding: 0px 5px !important;
}
#idFsFiltros
{
    padding-bottom: 0px !important;
}
</style>
<script type="text/javascript">
    $(function () {

        /************************* FUNCOES DE APOIO **************************/
        /*
    linhaSemaforos = Semaforos.ascx
    linhaFiltros = Filtros.ascx
    linhaResultados = GridResultados.ascx
    */
        new Ext.form.FormPanel({
            id: 'mainPanel',
            name: 'mainPanel',
            region: 'center',
            autoScroll: true,
            height: 560,
            width: 1150,
            bodyStyle: 'padding: 5px;',    
            labelAlign: 'top',
            items: [linhaSemaforos]
        }).render(document.body);
    
    });       
        
</script>
</asp:Content>
<asp:Content runat="server" ID="Content2" ContentPlaceHolderID="Content">
    <div id="header-content">
        <h1>
            CTE - Acompanhamentos Fiscal Simplificado</h1>
        <small>Você está em CTE > Acompanhamento de Documentos Fiscais Simplificado</small>
        <br />
    </div>
</asp:Content>
