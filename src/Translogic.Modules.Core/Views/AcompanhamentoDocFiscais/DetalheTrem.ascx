﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<script type="text/javascript">
    var gridResultadoTrem = null;
    var linhaTabTrem = null;
    var idTremSelecionado = null;
    var idOsTremSelecionado = null;

    /******************************************** DETALHES ********************************************/

    function MostrarDetalhes(idTrem, idOrdemServico) {

        idTremSelecionado = idTrem;
        idOsTremSelecionado = idOrdemServico;
        idDespachoSelecionado = 0;
        tabResultados.setActiveTab(1);
        Ext.getCmp('fieldSetResultadoMdfe').setTitle("Mdfe Trem:[" + idTrem + "] - OS:[" + idOrdemServico + "]");
        gridResultadoMdfe.getStore().load();
        gridResultadoMdfeDet.getStore().removeAll();
        gridResultadoComp.getStore().load();
    }

    /********************************************GRIDS************************************************/

    gridResultadoTrem = new Translogic.PaginatedGrid({
        id: 'gridResultadoTrem',
        name: 'gridResultadoTrem',
        autoLoadGrid: false,
        width: '100%',
        height: 260,
        viewConfig: {
            emptyText: 'Não há trem(s) para exibição.'
        },
        fields: [
                    'Id',
                    'Prefixo',
                    'Origem',
                    'Destino',
                    'OS'
                ],
        url: '<%= Url.Action("Pesquisar") %>',
        stripeRows: true,
        loadMask: { msg: App.Resources.Web.Carregando },
        columns: [
                    { dataIndex: "Id", hidden: true },
                    { header: 'Prefixo', dataIndex: 'Prefixo', width: 80, sortable: false },
                    { header: 'Origem', dataIndex: 'Origem', width: 200, sortable: false },
                    { header: 'Destino', dataIndex: 'Destino', width: 200, sortable: false },
                    { header: 'OS', dataIndex: 'OS', width: 80, sortable: false }
                ],
        listeners: {
            rowdblclick: function (grid, rowIndex, record, e) {
                MostrarDetalhes(gridResultadoTrem.getStore().getAt(rowIndex).get('Id'), gridResultadoTrem.getStore().getAt(rowIndex).get('OS'));
            }
        }
    });

    gridResultadoTrem.getStore().proxy.on('beforeload', function (p, params) {
        var dataInicial = Ext.getCmp("txtFiltroTremDataInicial").getValue();
        var dataFinal = Ext.getCmp("txtFiltroTremDataFinal").getValue();
        var chave = Ext.getCmp("txtFiltroChaveMdfe").getValue();
        var prefixo = Ext.getCmp("txtFiltroTrem").getValue();
        var os = Ext.getCmp("txtFiltroOS").getValue();
        var origem = Ext.getCmp("filtroOriTrem").getValue();
        var destino = Ext.getCmp("filtroDestTrem").getValue();

        params['filter[0].Campo'] = 'dataInicial';
        params['filter[0].Valor'] = new Array(dataInicial.format('d/m/Y') + " " + dataInicial.format('H:i:s'));
        params['filter[0].FormaPesquisa'] = 'Start';

        params['filter[1].Campo'] = 'dataFinal';
        params['filter[1].Valor'] = new Array(dataFinal.format('d/m/Y') + " " + dataFinal.format('H:i:s'));
        params['filter[1].FormaPesquisa'] = 'Start';

        params['filter[2].Campo'] = 'chave';
        params['filter[2].Valor'] = chave;
        params['filter[2].FormaPesquisa'] = 'Start';

        params['filter[3].Campo'] = 'prefixo';
        params['filter[3].Valor'] = prefixo;
        params['filter[3].FormaPesquisa'] = 'Start';

        params['filter[4].Campo'] = 'os';
        params['filter[4].Valor'] = os;
        params['filter[4].FormaPesquisa'] = 'Start';

        params['filter[5].Campo'] = 'origem';
        params['filter[5].Valor'] = origem;
        params['filter[5].FormaPesquisa'] = 'Start';

        params['filter[6].Campo'] = 'destino';
        params['filter[6].Valor'] = destino;
        params['filter[6].FormaPesquisa'] = 'Start';

        params['filter[7].Campo'] = 'idTrem';
        params['filter[7].Valor'] = idTremSelecionado;
        params['filter[7].FormaPesquisa'] = 'Start';

        params['filter[8].Campo'] = 'idDespacho';
        params['filter[8].Valor'] = idDespachoSelecionado;
        params['filter[8].FormaPesquisa'] = 'Start';

    });

    /************************************************Layout************************************************/

    var fieldSetResultadoTrem = {
        xtype: 'fieldset',
        title: 'Trem',
        width: '98%',
        bodyStyle: 'padding:5px',
        border: true,
        items: [gridResultadoTrem]
    };

    var coluna1LinhaTabTrem = {
        layout: 'form',
        border: false,
        width: '100%',
        items: [fieldSetResultadoTrem]
    };

    linhaTabTrem = {
        layout: 'column',
        border: false,
        width: '100%',
        items: [coluna1LinhaTabTrem]
    };
    
</script>
