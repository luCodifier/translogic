﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<script type="text/javascript">
    var filtros = null;
    var tabsFiltros = null;
    var linhaFiltros = null;

    function Pesquisar() {
        semaforo = null;
        siglaUfSemaforo = null;
        periodoHoraSemaforo = null;
        idTremSelecionado = 0;
        idMdfeSelecionado = 0;
        idCteSelecionada = 0;
        idDespachoSelecionado = 0;
        
        var activeTabFiltros = tabsFiltros.getActiveTab();
        var activeTabFiltrosIndex = tabsFiltros.items.findIndex('id', activeTabFiltros.id);

        if (activeTabFiltrosIndex == 0) {
            tabResultados.setActiveTab(2);

            var diferenca = Ext.getCmp("txtFiltroVagaoDataFinal").getValue() - Ext.getCmp("txtFiltroVagaoDataInicial").getValue();
            diferenca = Math.floor(diferenca / (1000 * 60 * 60 * 24));

            if (Ext.getCmp("txtFiltroVagaoDataInicial").getValue() == '' || Ext.getCmp("txtFiltroVagaoDataFinal").getValue() == '') {
                Ext.Msg.show({
                    title: "Mensagem de Informação",
                    msg: "Preencha os filtros de datas!",
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.INFO,
                    minWidth: 200
                });
            }
            else if (diferenca > 30) {
                Ext.Msg.show({
                    title: "Mensagem de Informação",
                    msg: "O período da pesquisa não deve ultrapassar 30 dias",
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.INFO,
                    minWidth: 200
                });
            }
            else {
                gridResultadoComp.getStore().load();
            }
        }
        else {
            tabResultados.setActiveTab(0);
            var diferenca = Ext.getCmp("txtFiltroTremDataFinal").getValue() - Ext.getCmp("txtFiltroTremDataInicial").getValue();
            diferenca = Math.floor(diferenca / (1000 * 60 * 60 * 24));

            if (Ext.getCmp("txtFiltroTremDataInicial").getValue() == '' || Ext.getCmp("txtFiltroTremDataFinal").getValue() == '') {
                Ext.Msg.show({
                    title: "Mensagem de Informação",
                    msg: "Preencha os filtros de datas!",
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.INFO,
                    minWidth: 200
                });
            }
            else if (diferenca > 30) {
                Ext.Msg.show({
                    title: "Mensagem de Informação",
                    msg: "O período da pesquisa não deve ultrapassar 30 dias",
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.INFO,
                    minWidth: 200
                });
            }
            else {
                gridResultadoTrem.getStore().load();
            }
        }

    }

    $(function () {

        var dsUFvagao = new Ext.data.JsonStore({
            root: "Items",
            autoLoad: false,
            url: '<%= Url.Action("ObterUFs") %>',
            fields: [
                'Uf'
            ]
        });

        var dsTiposCte = new Ext.data.JsonStore({
            root: "Items",
            autoLoad: false,
            url: '<%= Url.Action("ObterTiposCte") %>',
            fields: [
                'TipoCte',
                'TipoCteLabel'
            ]
        });


        /********************************************FILTROS VAGAO************************************************/
        /*LINHA 1*/
        var txtFiltroVagaoDataInicial = {
            xtype: 'datefield',
            fieldLabel: 'Data Inicial',
            id: 'txtFiltroVagaoDataInicial',
            name: 'txtFiltroVagaoDataInicial',
            width: 83,
            allowBlank: false,
            vtype: 'daterange',
            endDateField: 'txtFiltroVagaoDataFinal',
            value: new Date(),
            enableKeyEvents: true,
            listeners: {
                specialkey: TratarTeclaEnter
            }
        };

        var txtFiltroVagaoDataFinal = {
            xtype: 'datefield',
            fieldLabel: 'Data Final',
            id: 'txtFiltroVagaoDataFinal',
            name: 'txtFiltroVagaoDataFinal',
            width: 83,
            allowBlank: false,
            vtype: 'daterange',
            startDateField: 'txtFiltroVagaoDataInicial',
            value: new Date(),
            enableKeyEvents: true,
            listeners: {
                specialkey: TratarTeclaEnter
            }
        };

        var txtFiltroChaveNfe = {
            xtype: 'textfield',
            maskRe: /[0-9]/,
            autoCreate: { tag: 'input', type: 'text', maxlength: '44', autocomplete: 'off' },
            width: 280,
            name: 'txtFiltroChaveNfe',
            allowBlank: true,
            id: 'txtFiltroChaveNfe',
            fieldLabel: 'Chave da Nfe',
            enableKeyEvents: true,
            listeners: {
                specialkey: TratarTeclaEnter
            }
        };

        var txtFiltroFluxo = {
            xtype: 'textfield',
            maskRe: /[a-zA-Z0-9]/,
            autoCreate: { tag: 'input', type: 'text', maxlength: '44', autocomplete: 'off' },
            width: 90,
            name: 'txtFiltroFluxo',
            allowBlank: true,
            id: 'txtFiltroFluxo',
            fieldLabel: 'Fluxo',
            enableKeyEvents: true,
            listeners: {
                specialkey: TratarTeclaEnter
            }
        };

        var cboUfVagao = {
            xtype: 'combo',
            id: 'filtroUf',
            name: 'filtroUf',
            forceSelection: true,
            store: dsUFvagao,
            triggerAction: 'all',
            mode: 'remote',
            typeAhead: true,
            fieldLabel: 'UF',
            displayField: 'Uf',
            width: 40,
            valueField: 'Uf',
            emptyText: '--',
            editable: false,
            tpl: '<tpl for="."><div class="x-combo-list-item">{Uf}&nbsp;</div></tpl>',
            listeners: {
                specialkey: TratarTeclaEnter
            }
        };

        var cboTipoCte = {
            xtype: 'combo',
            id: 'filtroTipoCte',
            name: 'filtroTipoCte',
            forceSelection: false,
            store: dsTiposCte,
            triggerAction: 'all',
            mode: 'remote',
            typeAhead: true,
            fieldLabel: 'Tipo Cte',
            displayField: 'TipoCteLabel',
            width: 115,
            valueField: 'TipoCte',
            emptyText: '--',
            editable: false,
            tpl: '<tpl for="."><div class="x-combo-list-item">{TipoCteLabel}&nbsp;</div></tpl>',
            listeners: {
                specialkey: TratarTeclaEnter
            }
        };
        var coluna1Linha1 = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding-right:10px',
            items: [txtFiltroVagaoDataInicial]
        };

        var coluna2Linha1 = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding-right:10px',
            items: [txtFiltroVagaoDataFinal]
        };

        var coluna3Linha1 = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding-right:10px',
            items: [txtFiltroChaveNfe]
        };

        var coluna4Linha1 = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding-right:10px',
            items: [cboTipoCte]
        };
        
        var coluna5Linha1 = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding-right:10px',
            items: [txtFiltroFluxo]
        };

        var coluna6Linha1 = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding-right:10px',
            items: [cboUfVagao]
        };

        var linha1 = {
            layout: 'column',
            border: false,
            width: '100%',
            height: 50,
            bodyStyle: 'padding:5px',
            items: [coluna1Linha1, coluna2Linha1, coluna3Linha1, coluna4Linha1, coluna5Linha1, coluna6Linha1]
        };

        /*LINHA 2*/

        var txtFiltroOrigem = {
            xtype: 'textfield',
            vtype: 'cteestacaovtype',
            style: 'text-transform: uppercase',
            id: 'filtroOri',
            fieldLabel: 'Origem',
            autoCreate: { tag: 'input', type: 'text', maxlength: '3', autocomplete: 'off' },
            name: 'filtroOri',
            allowBlank: true,
            maxLength: 3,
            width: 35,
            hiddenName: 'Origem',
            listeners: {
                specialkey: TratarTeclaEnter
            }
        };

        var txtFiltroDestino = {
            xtype: 'textfield',
            vtype: 'cteestacaovtype',
            style: 'text-transform: uppercase',
            id: 'filtroDest',
            fieldLabel: 'Destino',
            autoCreate: { tag: 'input', type: 'text', maxlength: '3', autocomplete: 'off' },
            name: 'filtroDest',
            allowBlank: true,
            maxLength: 3,
            width: 35,
            hiddenName: 'Destino',
            listeners: {
                specialkey: TratarTeclaEnter
            }
        };

        var txtFiltroSerieDesp = {
            xtype: 'textfield',
            maskRe: /[0-9]/,
            autoCreate: { tag: 'input', type: 'text', maxlength: '3', autocomplete: 'off' },
            width: 40,
            name: 'txtFiltroSerie',
            allowBlank: true,
            id: 'txtFiltroSerie',
            fieldLabel: 'Série',
            enableKeyEvents: true,
            listeners: {
                specialkey: TratarTeclaEnter
            }
        };

        var txtFiltroNumeroDesp = {
            xtype: 'textfield',
            maskRe: /[0-9]/,
            autoCreate: { tag: 'input', type: 'text', maxlength: '9', autocomplete: 'off' },
            width: 70,
            name: 'txtFiltroNumero',
            allowBlank: true,
            id: 'txtFiltroNumero',
            fieldLabel: 'Número',
            enableKeyEvents: true,
            listeners: {
                specialkey: TratarTeclaEnter
            }
        };

        var txtFiltroChaveCte = {
            xtype: 'textfield',
            maskRe: /[0-9]/,
            autoCreate: { tag: 'input', type: 'text', maxlength: '44', autocomplete: 'off' },
            width: 280,
            name: 'txtFiltroChaveCte',
            allowBlank: true,
            id: 'txtFiltroChaveCte',
            fieldLabel: 'Chave da Cte',
            enableKeyEvents: true,
            listeners: {
                specialkey: TratarTeclaEnter
            }
        };

        var txtFiltroVagao = {
            xtype: 'textfield',
            maskRe: /[a-zA-Z0-9]/,
            autoCreate: { tag: 'input', type: 'text', maxlength: '44', autocomplete: 'off' },
            width: 120,
            name: 'txtFiltroVagao',
            allowBlank: true,
            id: 'txtFiltroVagao',
            fieldLabel: 'Vagão',
            enableKeyEvents: true,
            listeners: {
                specialkey: TratarTeclaEnter
            }
        };

        var btnLoteChaveCte = {
            text: 'Informar Chaves',
            xtype: 'button',
            iconCls: 'icon-find',
            handler: function (b, e) {

                windowStatusCte = new Ext.Window({
                    id: 'windowStatusCte',
                    title: 'Informar chaves',
                    modal: true,
                    width: 855,
                    resizable: false,
                    height: 380,
                    autoLoad: {
                        url: '<%= Url.Action("FormInformarChaves") %>',
                        scripts: true
                    }
                });

                windowStatusCte.show();

            }
        };

        var coluna1Linha2 = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding-right:10px',
            items: [txtFiltroOrigem]
        };

        var coluna2Linha2 = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding-right:10px',
            items: [txtFiltroDestino]
        };

        var coluna3Linha2 = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding-right:10px',
            items: [txtFiltroSerieDesp]
        };

        var coluna4Linha2 = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding-right:10px',
            items: [txtFiltroNumeroDesp]
        };

        var coluna5Linha2 = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding-right:10px',
            items: [txtFiltroChaveCte]
        };

        var coluna6Linha2 = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding-right:10px',
            items: [txtFiltroVagao]
        };

        var coluna7Linha2 = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding-right:10px; padding-top:17px;',
            items: [btnLoteChaveCte]
        };

        var linha2 = {
            layout: 'column',
            border: false,
            width: '100%',
            height: 50,
            bodyStyle: 'padding:5px',
            items: [coluna1Linha2, coluna2Linha2, coluna3Linha2, coluna4Linha2, coluna5Linha2, coluna6Linha2, coluna7Linha2]
        };

        /********************************************FILTROS TREM************************************************/
        /*LINHA 1*/
        var txtFiltroTremDataInicial = {
            xtype: 'datefield',
            fieldLabel: 'Data Inicial',
            id: 'txtFiltroTremDataInicial',
            name: 'txtFiltroTremDataInicial',
            width: 83,
            allowBlank: false,
            vtype: 'daterange',
            endDateField: 'txtFiltroTremDataFinal',
            value: new Date(),
            enableKeyEvents: true,
            listeners: {
                specialkey: TratarTeclaEnter
            }
        };

        var txtFiltroTremDataFinal = {
            xtype: 'datefield',
            fieldLabel: 'Data Final',
            id: 'txtFiltroTremDataFinal',
            name: 'txtFiltroTremDataFinal',
            width: 83,
            allowBlank: false,
            vtype: 'daterange',
            startDateField: 'txtFiltroTremDataInicial',
            value: new Date(),
            enableKeyEvents: true,
            listeners: {
                specialkey: TratarTeclaEnter
            }
        };

        var txtFiltroTrem = {
            xtype: 'textfield',
            maskRe: /[a-zA-Z0-9]/,
            autoCreate: { tag: 'input', type: 'text', maxlength: '44', autocomplete: 'off' },
            width: 90,
            name: 'txtFiltroTrem',
            allowBlank: true,
            id: 'txtFiltroTrem',
            fieldLabel: 'Trem',
            enableKeyEvents: true,
            listeners: {
                specialkey: TratarTeclaEnter
            }
        };

        var txtFiltroOS = {
            xtype: 'textfield',
            maskRe: /[a-zA-Z0-9]/,
            autoCreate: { tag: 'input', type: 'text', maxlength: '44', autocomplete: 'off' },
            width: 90,
            name: 'txtFiltroOS',
            allowBlank: true,
            id: 'txtFiltroOS',
            fieldLabel: 'OS',
            enableKeyEvents: true,
            listeners: {
                specialkey: TratarTeclaEnter
            }
        };

        var coluna1Linha1Trem = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding-right:10px',
            items: [txtFiltroTremDataInicial]
        };

        var coluna2Linha1Trem = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding-right:10px',
            items: [txtFiltroTremDataFinal]
        };

        var coluna3Linha1Trem = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding-right:10px',
            items: [txtFiltroTrem]
        };

        var coluna4Linha1Trem = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding-right:10px',
            items: [txtFiltroOS]
        };

        var linha1Trem = {
            layout: 'column',
            border: false,
            width: '100%',
            height: 50,
            bodyStyle: 'padding:5px',
            items: [coluna1Linha1Trem, coluna2Linha1Trem, coluna3Linha1Trem, coluna4Linha1Trem]
        };

        /*LINHA 2*/

        var txtFiltroOrigemTrem = {
            xtype: 'textfield',
            vtype: 'cteestacaovtype',
            style: 'text-transform: uppercase',
            id: 'filtroOriTrem',
            fieldLabel: 'Origem',
            autoCreate: { tag: 'input', type: 'text', maxlength: '3', autocomplete: 'off' },
            name: 'filtroOriTrem',
            allowBlank: true,
            maxLength: 12,
            width: 90,
            hiddenName: 'Origem',
            listeners: {
                specialkey: TratarTeclaEnter
            }
        };

        var txtFiltroDestinoTrem = {
            xtype: 'textfield',
            vtype: 'cteestacaovtype',
            style: 'text-transform: uppercase',
            id: 'filtroDestTrem',
            fieldLabel: 'Destino',
            autoCreate: { tag: 'input', type: 'text', maxlength: '3', autocomplete: 'off' },
            name: 'filtroDestTrem',
            allowBlank: true,
            maxLength: 12,
            width: 90,
            hiddenName: 'Destino',
            listeners: {
                specialkey: TratarTeclaEnter
            }
        };

        var txtFiltroChaveMdfe = {
            xtype: 'textfield',
            maskRe: /[0-9]/,
            autoCreate: { tag: 'input', type: 'text', maxlength: '44', autocomplete: 'off' },
            width: 280,
            name: 'txtFiltroChaveMdfe',
            allowBlank: true,
            id: 'txtFiltroChaveMdfe',
            fieldLabel: 'Chave da Mdfe',
            enableKeyEvents: true,
            listeners: {
                specialkey: TratarTeclaEnter
            }
        };

        var coluna1Linha2Trem = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding-right:10px',
            items: [txtFiltroOrigemTrem]
        };

        var coluna2Linha2Trem = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding-right:10px',
            items: [txtFiltroDestinoTrem]
        };

        var coluna3Linha2Trem = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding-right:10px',
            items: [txtFiltroChaveMdfe]
        };

        var linha2Trem = {
            layout: 'column',
            border: false,
            width: '100%',
            height: 50,
            bodyStyle: 'padding:5px',
            items: [coluna1Linha2Trem, coluna2Linha2Trem, coluna3Linha2Trem]
        };

        var btnPesquisar = {
            name: 'btnPesquisar',
            id: 'btnPesquisar',
            text: 'Pesquisar',
            iconCls: 'icon-find',
            handler: Pesquisar
        };

        var btnLimpar = {
            name: 'btnLimpar',
            id: 'btnLimpar',
            text: 'Limpar',
            handler: Limpar
        };

        /********************************************TABS***************************************************/

        tabsFiltros = new Ext.TabPanel({
            activeTab: 0,
            frame: false,
            height: 130,
            plain: true,
            defaults: {
                autoScroll: true,
                bodyPadding: 10
            },
            items: [{
                    title: 'Consulta por Vagão',
                    layout: 'vbox',
                    items: [linha1, linha2]
                },
                {
                    title: 'Consulta por Trem',
                    layout: 'vbox',
                    items: [linha1Trem, linha2Trem]
                }
            ]
        });


        /********************************************Layout***********************************************/

        var coluna1Filtros = {
            layout: 'form',
            border: false,
            items: [tabsFiltros]
        };

        var linhaFieldSets = {
            layout: 'column',
            width: '100%',
            border: false,
            items: [coluna1Filtros]
        };

        var linhaBotoes = {
            layout: 'column',
            border: false,
            bodyStyle: 'padding-bottom:0px;',
            buttons: [btnPesquisar, btnLimpar],
            buttonAlign: 'center'
        };

        var fieldSetFiltros = {
            id: 'idFsFiltros',
            xtype: 'fieldset',
            title: 'Filtros',
            width: '99.8%',
            height: 190,
            collapsible: true,
            collapsed: false,
            border: true,
            items: [linhaFieldSets, linhaBotoes]
        };

        linhaFiltros = {
            layout: 'column',
            border: false,
            bodyStyle: 'padding-top:5px;',
            items: [fieldSetFiltros]
        };

        function Limpar() {
            gridResultadoTrem.getStore().removeAll();
            gridResultadoComp.getStore().removeAll();
            gridResultadoStatusCte.getStore().removeAll();
            gridResultadoMdfe.getStore().removeAll();
            gridResultadoMdfeDet.getStore().removeAll();
            gridNotasVagoes.getStore().removeAll();
            gridDocsNfes.getStore().removeAll();
            gridResultadoDiarioDeBordo.getStore().removeAll();
            semaforo = null;
            siglaUfSemaforo = null;
            periodoHoraSemaforo = null;
            idTremSelecionado = 0;
            idMdfeSelecionado = 0;
            idCteSelecionada = 0;
            idDespachoSelecionado = 0;
            Ext.getCmp("mainPanel").getForm().reset();
            Ext.getCmp('fieldSetResultadoMdfe').setTitle("Mdfe Trem:[] - OS:[]");
            Ext.getCmp('fsEnviarDiarioBordo').setTitle("Enviar diário de Bordo Cte:{0}");
            Ext.getCmp('fsListarDiarioBordo').setTitle("Diário de Bordo Cte:{0}");

            Ext.getCmp("lblSerieCte").setValue("");
            Ext.getCmp("lblNumeroCte").setValue("");
            Ext.getCmp("lblDataEmissaoCte").setValue("");
            Ext.getCmp("lblChaveCte").setValue("");
            Ext.getCmp("fluxoCte").setValue("");
            Ext.getCmp("vagaoCte").setValue("");
            Ext.getCmp("lblCFOPCte").setValue("");
            Ext.getCmp("txtObservacaoCte").setValue("");
            Ext.getCmp("lblNomeRemetente").setValue("");
            Ext.getCmp("lblEnderecoRemetente").setValue("");
            Ext.getCmp("lblMunicipioRemetente").setValue("");
            Ext.getCmp("lblCEPRemetente").setValue("");
            Ext.getCmp("lblDocumentoRemetente").setValue("");
            Ext.getCmp("lblInscEstRemetente").setValue("");
            Ext.getCmp("lblUFRemetente").setValue("");
            Ext.getCmp("lblFoneRemetente").setValue("");
            Ext.getCmp("lblPaisRemetente").setValue("");
            Ext.getCmp("lblNomeDestinatario").setValue("");
            Ext.getCmp("lblEnderecoDestinatario").setValue("");
            Ext.getCmp("lblMunicipioDestinatario").setValue("");
            Ext.getCmp("lblCEPDestinatario").setValue("");
            Ext.getCmp("lblDocumentoDestinatario").setValue("");
            Ext.getCmp("lblInscEstDestinatario").setValue("");
            Ext.getCmp("lblUFDestinatario").setValue("");
            Ext.getCmp("lblPaisDestinatario").setValue("");
            Ext.getCmp("lblFoneDestinatario").setValue("");
            Ext.getCmp("lblNomeTomador").setValue("");
            Ext.getCmp("lblEnderecoTomador").setValue("");
            Ext.getCmp("lblMunicipioTomador").setValue("");
            Ext.getCmp("lblCEPTomador").setValue("");
            Ext.getCmp("lblDocumentoTomador").setValue("");
            Ext.getCmp("lblInscEstTomador").setValue("");
            Ext.getCmp("lblUFTomador").setValue("");
            Ext.getCmp("lblPaisTomador").setValue("");
            Ext.getCmp("lblFoneTomador").setValue("");
            Ext.getCmp("lblNomeExpedidor").setValue("");
            Ext.getCmp("lblEnderecoExpedidor").setValue("");
            Ext.getCmp("lblMunicipioExpedidor").setValue("");
            Ext.getCmp("lblCEPExpedidor").setValue("");
            Ext.getCmp("lblDocumentoExpedidor").setValue("");
            Ext.getCmp("lblInscEstExpedidor").setValue("");
            Ext.getCmp("lblUFExpedidor").setValue("");
            Ext.getCmp("lblPaisExpedidor").setValue("");
            Ext.getCmp("lblFoneExpedidor").setValue("");
            Ext.getCmp("lblNomeRecebedor").setValue("");
            Ext.getCmp("lblEnderecoRecebedor").setValue("");
            Ext.getCmp("lblMunicipioRecebedor").setValue("");
            Ext.getCmp("lblCEPRecebedor").setValue("");
            Ext.getCmp("lblDocumentoRecebedor").setValue("");
            Ext.getCmp("lblInscEstRecebedor").setValue("");
            Ext.getCmp("lblUFRecebedor").setValue("");
            Ext.getCmp("lblPaisRecebedor").setValue("");
            Ext.getCmp("lblFoneRecebedor").setValue("");
            Ext.getCmp("lblBaseCalculo").setValue("");
            Ext.getCmp("lblAliqIcms").setValue("");
            Ext.getCmp("lblVlrIcms").setValue("");
            Ext.getCmp("lblVlrDesconto").setValue("");
            Ext.getCmp("lblVlrTotal").setValue("");
            Ext.getCmp("lblVlrReceber").setValue("");
            Ext.getCmp("lblSerieNfe").setValue("");
            Ext.getCmp("lblNumeroNfe").setValue("");
            Ext.getCmp("lblDataEmissaoNfe").setValue("");
            Ext.getCmp("lblValorTotalNfe").setValue("");
            Ext.getCmp("lblOrigemlNfe").setValue("");
            Ext.getCmp("lblCNPJEmit").setValue("");
            Ext.getCmp("lblRazaoEmit").setValue("");
            Ext.getCmp("lblInscEmit").setValue("");
            Ext.getCmp("lblUFEmit").setValue("");
            Ext.getCmp("lblCNPJDest").setValue("");
            Ext.getCmp("fieldRazaoDest").setValue("");
            Ext.getCmp("lblInscDest").setValue("");
            Ext.getCmp("lblUFDest").setValue("");
            //Ext.getCmp("lblCNPJTrans").setValue("");
            //Ext.getCmp("lblRazaoTrans").setValue("");
            //Ext.getCmp("lblMunicipioTrans").setValue("");
            //Ext.getCmp("lblUFTrans").setValue("");
            Ext.getCmp("lblPesoBruto").setValue("");
            Ext.getCmp("lblVolume").setValue("");
            Ext.getCmp("lblPesoDisponivel").setValue("");
            Ext.getCmp("lblVolumeDisponivel").setValue("");
            Ext.getCmp("lblPesoVagaoCte").setValue("");
            Ext.getCmp("lblTarifaCte").setValue("");
            Ext.getCmp("txtAcaoDiarioBordo").setValue("");
        }
    });
</script>
