﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<!-- User Controls componentes de Layout -->
<%Html.RenderPartial("DetalheTrem"); %>
<%Html.RenderPartial("DetalheMdfe"); %>
<%Html.RenderPartial("DetalheVagao"); %>
<%Html.RenderPartial("DetalheCte"); %>
<%Html.RenderPartial("DetalheNfe"); %>
<!-- Fim User Controls componentes de Layout -->

<script type="text/javascript">

    var tabResultados = null;
    var linhaResultados = null;
    $(function() {
        /********************************************TABS***************************************************/
        tabResultados = new Ext.TabPanel({
                activeTab: 0,
                width: '100%',
                height: 320,
                plain: true,
                defaults: {
                    autoScroll: true
                },
                items: [{
                    title: 'Trem',
                    layout: 'vbox',
                    items: [linhaTabTrem]
                }, {
                    title: 'Mdfe',
                    layout: 'vbox',
                    items: [linhaTabMdfe]
                }, {
                    title: 'Composição',
                    layout: 'vbox',
                    items: [linhaTabComp]
                },{
                    title: 'Cte',
                    layout: 'vbox',
                    items: [fieldSetResultadoCte]
                },{
                    title: 'Nfes',
                    id: 'tabResNfe',
                    name: 'tabResNfe',
                    items: [linhaTabNfe]
                }]
            });

        linhaResultados = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding-top:5px;',
            items: [tabResultados]
        };
    });   
</script>
