﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%@ Import Namespace="System.Globalization" %>
<%@ Import Namespace="Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico" %>
<div id="div-form-status-impressao">
</div>
<script type="text/javascript">
    var arrayCtes = new Array();
    var arrayCtesOk = new Array();
    var arrayCtesNok = new Array();
    var arrayCtesNaoImpressao = new Array();
    
    <%
        var arrayCtes = (int[]) (ViewData["ID_CTE_IMPRESSAO"]);

        foreach (int cteId in arrayCtes)
        {
            %> arrayCtes.push('<%= cteId %>' ); <%
        }

    %>
   

 var storeImpressao = new Ext.data.JsonStore({
        url: '<%= Url.Action("ObterCteImpressao") %>',        
        //autoLoad: false,
        remoteSort: true,
        root: "Items",
        fields: [
                    'CteId',
                    'NroCte',
                    'Status',
                    'Imprime'
			    ]
    });
    
    function fctObterRegistrosAprovados()
    {

        storeImpressao.each(
			    function (record) 
                {
			        switch (record.data.Imprime) {
			        case "S":
			            arrayCtesOk.push(record.data.CteId);
			            break;
			        case "N":
			            arrayCtesNok.push(record.data.CteId);
			            break;
			        default:
			            arrayCtesNaoImpressao.push(record.data.CteId);
			            break;
			        }
			    }
		    );
      return arrayCtesOk;
    }

    var gridStatusImpressao = new Ext.grid.GridPanel({
                                viewConfig: {
                                    forceFit: true
                                },
                                height: 250,
                                stripeRows: true,
                                autoLoad: false,
                                region: 'center',
                                store: storeImpressao ,
                               loadMask: { msg: App.Resources.Web.Carregando },
                                colModel: new Ext.grid.ColumnModel({
                                    defaults: {
                                        sortable: false
                                    },
                                    columns: [
						                        new Ext.grid.RowNumberer(),
                                                { dataIndex: "CteId", hidden: true },
						                        { header: 'Nro Cte', dataIndex: "NroCte", sortable: false, width: 100 },
				                                { header: 'Status', dataIndex: "Imprime", sortable: false, width: 50, renderer:statusRenderer }
					                        ]
                                })                               
                            });


function statusRenderer(val)
      {
        switch(val){
            case "S":
                return "<img src='<%=Url.Images("Icons/tick.png") %>' >";
                break;
            case "NP":
                return "<img src='<%=Url.Images("Icons/printer_error.png") %>' >";
                break;
            default:
                return "<img src='<%=Url.Images("Icons/cross.gif") %>' >";
                break;
        }
      }

var legenda = new Ext.Toolbar({
        layout: 'column',
        items: [
            {
                xtype: 'button',
                iconCls: 'icon-tick',
                id: 'btnAprovados',
                text: '<br/>'
            }, {
                xtype: 'button',
                iconCls: 'icon-delete',
                id: 'btnNaoAprovados',
                text: ''
            },
            {
                xtype: 'button',
                iconCls: 'icon-printer-error',
                id: 'btnNaoImpressos',
                text: ''
            }
        ]
    });

  var formStatusImpressao = new Ext.form.FormPanel
	        ({
	            id: 'formStatusImpressao',
	            labelWidth: 80,
	            width: 400,
	            bodyStyle: 'padding: 15px',
	            labelAlign: 'top',
	            items: [gridStatusImpressao],
	            buttonAlign: "center",
	            buttons:
		        [{
		            text: "Confirmar",
		            handler: function () {

                    var arrAprovados = fctObterRegistrosAprovados();
                    var stringAprovados = "";
                    var count = 0;
                    
                     storeImpressao.each(
			                function (record) {
                                if(record.data.Imprime == "S")
                                { 
                                     stringAprovados +=  record.data.CteId +"$";
                                     count++;
                                }
			                }
		                );

                    if(count == 0)
                    {
                            Ext.Msg.alert('Alerta','Não foi encontrado nenhum arquivo para impressão.');
                            return;
                    }

                     var url = '<%=Url.Action("ImprimirCte") %>';
					 url = url + "?idCte=" + stringAprovados;
					 window.location = url;

					}					
		        },
		        {
		            text: "Cancelar",
		            handler: function () {
		                windowStatusImpressao.close();
						
		            }
		        }],
                bbar: legenda       
	        });

    formStatusImpressao.render("div-form-status-impressao");
    
    storeImpressao.load({
        params: {'arrayCtes': arrayCtes },
        callback: function() { 
            var arrRegistroAprovados =  fctObterRegistrosAprovados();
            var totalAprovados = arrRegistroAprovados.length;
            var total = arrayCtes.length;
            var msg = 'CTe para impressão: ' + totalAprovados +' CTe de ' + total + ' selecionados.';
            var msgNok = 'Cte que não tiveram arquivos gerados: ' + arrayCtesNok.length;
            var msgNoPrint = 'Cte que não podem ser impressas: ' + arrayCtesNaoImpressao.length;
            
            Ext.getCmp('btnAprovados').setText(msg);
            Ext.getCmp('btnNaoAprovados').setText(msgNok);
			Ext.getCmp('btnNaoImpressos').setText(msgNoPrint);
		}
    });

</script>
