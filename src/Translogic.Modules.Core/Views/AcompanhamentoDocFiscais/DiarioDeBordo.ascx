﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<script type="text/javascript">

    var gridResultadoDiarioDeBordo = null;
    var linhaTabDiarioBordo = null;

    $(function () {

        /******************************************************STORE******************************************************/
        var dsDiarioDeBordo = new Ext.data.JsonStore({
            root: "Items",
            autoLoad: false,
            proxy: new Ext.data.HttpProxy({ url: '<%= Url.Action("ObterDiarioBordoCte") %>', timeout: 600000 }),
            fields:
                [
                    'Id',
                    'DataHora',
                    'Acao',
                    'Usuario'
                ]
        });

        /******************************************************GRID******************************************************/
        gridResultadoDiarioDeBordo = new Ext.grid.GridPanel({
            id: 'gridResultadoDiarioDeBordo',
            name: 'gridResultadoDiarioDeBordo',
            autoLoadGrid: false,
            autoScroll: true,
            height: 175,
            store: dsDiarioDeBordo,
            stripeRows: true,
            loadMask: { msg: App.Resources.Web.Carregando },
            columns: [
                    { header: 'Ação', dataIndex: 'Acao', width: 350, sortable: false },
                    { header: 'Data e Hora', dataIndex: 'DataHora', width: 110, sortable: false },
                    { header: 'Usuario', dataIndex: 'Usuario', width: 200, sortable: false }
                ]
        });

        /******************************************************Campos******************************************************/

        var txtAcaoDiarioBordo = {
            xtype: 'textarea',
            fieldLabel: 'Ação',
            id: 'txtAcaoDiarioBordo',
            name: 'txtAcaoDiarioBordo',
            height: 100,
            maxLength: 600,
            autoCreate: { tag: 'textarea', type: 'textarea', maxlength: '600', autocomplete: 'off' },
            width: 368,
            value: ''
        };

        var btnEnviarDiario = {
            name: 'btnEnviarDiario',
            id: 'btnEnviarDiario',
            text: 'Enviar',
            iconCls: 'icon-save',
            handler: function () {
                var msgmAcao = Ext.getCmp("txtAcaoDiarioBordo").getValue();

                var semCte = (idCteSelecionada == null || idCteSelecionada == undefined || idCteSelecionada <= 0);
                
                if($.trim(msgmAcao) == "" || semCte) {
                    Ext.Msg.show({
                        title: "Mensagem de Erro",
                        msg: semCte ? "Não há cte selecionada!" : "Insira algo no campo Ação",
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.ERROR,
                        minWidth: 200
                    });
                }
                else {
                    Ext.Ajax.request({
                        url: "<%= Url.Action("GravarDiarioDeBordoCte") %>",
                        method: "POST",
                        params: { acao: msgmAcao , idCte: idCteSelecionada },
                        success: function(resultSalvar)
                        {
                            var result = Ext.util.JSON.decode(resultSalvar.responseText);
                            
                            Ext.Msg.show({
                                title: "Mensagem",
                                msg: result.Message,
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.INFO,
                                minWidth: 200
                            }); 
                            
                            if(result.Success) {
                                Ext.getCmp("txtAcaoDiarioBordo").setValue("");
                                gridResultadoDiarioDeBordo.getStore().load({ params: { 'idCte': idCteSelecionada } });
                            }
                        },
                        failure: function(resVerificarFail)
                        {
                            var resFail = Ext.util.JSON.decode(resVerificarFail.responseText);

                            Ext.Msg.show({
                                title: "Mensagem de Erro",
                                msg: resFail.Message != null && resFail.Message != undefined ? resFail.Message : resFail.message,
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.ERROR,
                                minWidth: 200
                            });
                        }
                    });
                }
            }
        };

        var linha1FsEnviarDiarioBordo = {
            layout: 'form',
            border: false,
            items: [txtAcaoDiarioBordo]
        };

        var linha2EnviarDiarioBordo = {
            layout: 'form',
            border: false,
            bodyStyle: 'padding-bottom:0px;',
            buttons: [btnEnviarDiario],
            buttonAlign: 'center'
        };

        var fsEnviarDiarioBordo = {
            xtype: 'fieldset',
            id: 'fsEnviarDiarioBordo',
            name: 'fsEnviarDiarioBordo',
            height: 200,
            width: 390,
            title: 'Enviar diário de Bordo Cte:{0}',
            items: [linha1FsEnviarDiarioBordo, linha2EnviarDiarioBordo]
        };

        var linhaFsListarDiarioBordo = {
            layout: 'form',
            border: false,
            items: [gridResultadoDiarioDeBordo]
        };
        
        var fsListarDiarioBordo = {
            xtype: 'fieldset',
            id: 'fsListarDiarioBordo',
            name: 'fsListarDiarioBordo',
            height: 200,
            width: 690,
            title: 'Diário de Bordo Cte:{0}',
            items: [linhaFsListarDiarioBordo]
        };

        var coluna1linhaTabDiarioBordo = {
            layout: 'form',
            width: 395,
            border: false,
            items: [fsEnviarDiarioBordo]
        };
        var coluna2linhaTabDiarioBordo = {
            layout: 'form',
            border: false,
            items: [fsListarDiarioBordo]
        };

        linhaTabDiarioBordo = {
            layout: 'column',
            border: false,
            width: '100%',
            bodyStyle: 'padding: 5px',
            items: [coluna1linhaTabDiarioBordo, coluna2linhaTabDiarioBordo]
        };
    });

</script>
