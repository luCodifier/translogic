﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<script type="text/javascript">
    var gridResultadoComp = null;
    var linhaTabComp = null;
    var idCteSelecionada = 0;
    var idCteSelecionadaOld = 0;
    var idDespachoSelecionado = 0;
    var idDespachoSelecionadoOld = 0;

    var smGridComp = new Ext.grid.CheckboxSelectionModel({
        singleSelect: false,
        listeners: {
            selectionchange: function(sm) {

                var recLen = Ext.getCmp('gridResultadoComp').store.getRange().length;
                var selectedLen = this.selections.items.length;
                if (selectedLen == recLen) {
                    var view = Ext.getCmp('gridResultadoComp').getView();
                    var chkdiv = Ext.fly(view.innerHd).child(".x-grid3-hd-checker");
                    chkdiv.addClass("x-grid3-hd-checker-on");
                }
            },
            beforerowselect: function(sm, rowIndex, keep, rec) {
                var sitgrid = Ext.getCmp("gridResultadoComp").getStore().getAt(rowIndex).get('Status');
                if (sitgrid != "EAR" &&
                    sitgrid != "ERR" &&
                    sitgrid != "EAC" &&
                    sitgrid != "INV" &&
                    sitgrid != "AUT" &&
                    sitgrid != "CAN" &&
                    sitgrid != "ANL" &&
                    sitgrid != "AGA" &&
                    sitgrid != "PAE") {
                    return false;
                }
            }
        },
        rowdeselect: function(sm, rowIndex, record) {
            var view = Ext.getCmp('gridResultadoComp').getView();
            var chkdiv = Ext.fly(view.innerHd).child(".x-grid3-hd-checker");
            chkdiv.removeClass('x-grid3-hd-checker-on');
        },
        renderer: function(v, p, record) {
            if (record.data.Status != "EAR" &&
                record.data.Status != "ERR" &&
                record.data.Status != "INV" &&
                record.data.Status != "AUT" &&
                record.data.Status != "CAN" &&
                record.data.Status != "ANL" &&
                record.data.Status != "EAC" &&
                record.data.Status != "AGA" &&
                record.data.Status != "PAE") {
                return '';
            }
            return '<div class="x-grid3-row-checker">&#160;</div>';
        }
    });

    gridResultadoComp = new Translogic.PaginatedGrid({
        id: 'gridResultadoComp',
        name: 'gridResultadoComp',
        autoLoadGrid: false,
        autoScroll: true,
        height: 220,
        width: '100%',
        viewConfig: {
            forceFit: true,
            emptyText: 'Não há vagões(s) para exibição.'
        },
        fields: [
            'IdCte',
            'IdDespacho',
            'Status',
            'Vagao',
            'Fluxo',
            'DataCarregamento',
            'SerieDesp',
            'NumeroDesp',
            'Serie',
            'Numero',
            'DataEmissao',
            'Chave',
            'Baldeio'
        ],
        filteringToolbar: [
            {
                id: 'btnExportar',
                text: 'Exportar',
                tooltip: 'Exportar',
                iconCls: 'icon-page-excel',
                handler: function(c) {

                    if (gridResultadoComp.getStore().getCount() > 0) {

                        var listaChave = "";
                        if (storeChaves.getCount() != 0) {
                            storeChaves.each(
                                function(record) {
                                    if (!record.data.Erro) {
                                        listaChave += record.data.Chave + ";";
                                    }
                                }
                            );
                        } else {
                            listaChave = Ext.getCmp("txtFiltroChaveCte").getValue() == "null" ? "" : Ext.getCmp("txtFiltroChaveCte").getValue();
                        }

                        var dataInicial = Ext.getCmp("txtFiltroVagaoDataInicial").getValue();
                        var dataFinal = Ext.getCmp("txtFiltroVagaoDataFinal").getValue();
                        var chaveNfe = Ext.getCmp("txtFiltroChaveNfe").getValue() == "null" ? "" :  Ext.getCmp("txtFiltroChaveNfe").getValue();
                        var fluxo = Ext.getCmp("txtFiltroFluxo").getValue() == "null" ? "" : Ext.getCmp("txtFiltroFluxo").getValue();
                        var uf = Ext.getCmp("filtroUf").getValue() == "null" ? "" : Ext.getCmp("filtroUf").getValue();
                        var origem = Ext.getCmp("filtroOri").getValue() == "null" ? "" : Ext.getCmp("filtroOri").getValue();
                        var destino = Ext.getCmp("filtroDest").getValue() == "null" ? "" : Ext.getCmp("filtroDest").getValue();
                        var serieDesp = Ext.getCmp("txtFiltroSerie").getValue() == "null" ? "" : Ext.getCmp("txtFiltroSerie").getValue();
                        var numeroDesp = Ext.getCmp("txtFiltroNumero").getValue() == "null" ? "" : Ext.getCmp("txtFiltroNumero").getValue();
                        //var chaveCte = Ext.getCmp("txtFiltroChaveCte").getValue() == "null" ? "" : Ext.getCmp("txtFiltroChaveCte").getValue();
                        var chaveCte = listaChave;
                        var codVagao = Ext.getCmp("txtFiltroVagao").getValue() == "null" ? "" : Ext.getCmp("txtFiltroVagao").getValue();
                        var idMdfe = idMdfeSelecionado != null && idMdfeSelecionado != null ? idMdfeSelecionado : 0;
                        var idTrem = idTremSelecionado != null && idTremSelecionado != null ? idTremSelecionado : 0;
                        var tipoCte = Ext.getCmp("filtroTipoCte").getValue() == "null" ? "" : Ext.getCmp("filtroTipoCte").getValue();

                        var url = "<%= this.Url.Action("Exportar") %>";

                        url += String.format("?dataInicial={0}", new Array(dataInicial.format('d/m/Y') + " " + dataInicial.format('H:i:s')));
                        url += String.format("&dataFinal={0}", new Array(dataFinal.format('d/m/Y') + " " + dataInicial.format('H:i:s')));
                        url += String.format("&chaveNfe={0}", chaveNfe);
                        url += String.format("&fluxo={0}", fluxo);
                        url += String.format("&uf={0}", uf);
                        url += String.format("&origem={0}", origem);
                        url += String.format("&destino={0}", destino);
                        url += String.format("&serieDesp={0}", serieDesp);
                        url += String.format("&numeroDesp={0}", numeroDesp);
                        url += String.format("&chaveCte={0}", chaveCte);
                        url += String.format("&codVagao={0}", codVagao);
                        url += String.format("&idMdfe={0}", idMdfe);
                        url += String.format("&idTrem={0}", idTrem);
                        url += String.format("&tipoCte={0}", tipoCte);
                        url += String.format("&semaforo={0}", semaforo == null ? "" : semaforo);
                        url += String.format("&siglaUfSemaforo={0}", siglaUfSemaforo == null ? "" : siglaUfSemaforo);
                        url += String.format("&periodoSemaforo={0}", periodoHoraSemaforo == null ? "" : periodoHoraSemaforo);

                        window.open(url, "");
                    }
                }
            }
        ],
        url: '<%= this.Url.Action("ObterVagoesPorFiltro") %>',
        stripeRows: true,
        loadMask: { msg: App.Resources.Web.Carregando },
        columns: [
            smGridComp,
            { header: 'IdCte', dataIndex: 'IdCte', hidden: true },
            { header: 'IdDespacho', dataIndex: 'IdDespacho', hidden: true },
            { header: 'Status', dataIndex: 'Status', width: 50, renderer: statusRenderer, sortable: false },
            { header: 'Cód. Vg.', dataIndex: 'Vagao', width: 60, sortable: false },
            { header: 'Fluxo', dataIndex: 'Fluxo', width: 65, sortable: false },
            { header: 'Dt. Carregamento', dataIndex: 'DataCarregamento', width: 110, sortable: false },
            { header: 'Ser. Desp', dataIndex: 'SerieDesp', width: 60, sortable: false },
            { header: 'Num. Desp', dataIndex: 'NumeroDesp', width: 65, sortable: false },
            { header: 'Ser. Cte', dataIndex: 'Serie', width: 50, sortable: false },
            { header: 'Nr. Cte', dataIndex: 'Numero', width: 65, sortable: false },
            { header: 'Dt. Emissão', dataIndex: 'DataEmissao', width: 110, sortable: false },
            { header: 'Chave Cte', dataIndex: 'Chave', width: 290, sortable: false },
            { header: 'Transbordo', dataIndex: 'Baldeio', width: 65, sortable: false }
        ],
        sm: smGridComp,
        listeners: {
            rowdblclick: function(grid, rowIndex, e) {

                var sel = gridResultadoComp.getSelectionModel().getSelected();

                idCteSelecionada = sel.data.IdCte != null && sel.data.IdCte != undefined ? sel.data.IdCte : 0;
                idDespachoSelecionado = sel.data.IdDespacho != null && sel.data.IdDespacho != undefined ? sel.data.IdDespacho : 0;

                if (idCteSelecionadaOld != idCteSelecionada) {
                    idCteSelecionadaOld = idCteSelecionada;
                }

                if (idDespachoSelecionado != idDespachoSelecionadoOld) {
                    idDespachoSelecionadoOld = idDespachoSelecionado;
                }

                if (idCteSelecionada > 0) {
                    getDadosCte(idCteSelecionada, idDespachoSelecionado);
                }

                Ext.getCmp('fieldSetResultadoMdfe').setTitle("Mdfe's");

                idOsTremSelecionado = null;
                idTremSelecionado = null;
                idMdfeSelecionado = null;
                idMdfeSelecionadoOld = null;

                gridDocsNfes.getStore().load({ params: { 'idDespacho': idDespachoSelecionado } });
                gridResultadoTrem.getStore().load();
                gridResultadoMdfe.getStore().load();
            }
        }
    });

    gridResultadoComp.getStore().proxy.conn = { timeout: 300000 };

    gridResultadoComp.getStore().proxy.on('beforeload', function(p, params) {
        var listaChave = "";
        if (storeChaves.getCount() != 0) {
            storeChaves.each(
                function(record) {
                    if (!record.data.Erro) {
                        listaChave += record.data.Chave + ";";
                    }
                }
            );
        } else {
            listaChave = Ext.getCmp("txtFiltroChaveCte").getValue() == "null" ? "" : Ext.getCmp("txtFiltroChaveCte").getValue();
        }

        var dataInicial = Ext.getCmp("txtFiltroVagaoDataInicial").getValue();
        var dataFinal = Ext.getCmp("txtFiltroVagaoDataFinal").getValue();
        var chaveNfe = Ext.getCmp("txtFiltroChaveNfe").getValue();
        var fluxo = Ext.getCmp("txtFiltroFluxo").getValue();
        var uf = Ext.getCmp("filtroUf").getValue();
        var origem = Ext.getCmp("filtroOri").getValue();
        var destino = Ext.getCmp("filtroDest").getValue();
        var serieDesp = Ext.getCmp("txtFiltroSerie").getValue();
        var numeroDesp = Ext.getCmp("txtFiltroNumero").getValue();
        //var chaveCte = Ext.getCmp("txtFiltroChaveCte").getValue();
        var chaveCte = listaChave;
        var codVagao = Ext.getCmp("txtFiltroVagao").getValue();
        var idMdfe = idMdfeSelecionado != null && idMdfeSelecionado != null ? idMdfeSelecionado : 0;
        var idTrem = idTremSelecionado != null && idTremSelecionado != null ? idTremSelecionado : 0;
        var tipoCte = Ext.getCmp("filtroTipoCte").getValue();

        params['filter[0].Campo'] = 'dataInicial';
        params['filter[0].Valor'] = new Array(dataInicial.format('d/m/Y') + " " + dataInicial.format('H:i:s'));
        params['filter[0].FormaPesquisa'] = 'Start';

        params['filter[1].Campo'] = 'dataFinal';
        params['filter[1].Valor'] = new Array(dataFinal.format('d/m/Y') + " " + dataFinal.format('H:i:s'));
        params['filter[1].FormaPesquisa'] = 'Start';

        params['filter[2].Campo'] = 'chaveNfe';
        params['filter[2].Valor'] = chaveNfe;
        params['filter[2].FormaPesquisa'] = 'Start';

        params['filter[3].Campo'] = 'fluxo';
        params['filter[3].Valor'] = fluxo;
        params['filter[3].FormaPesquisa'] = 'Start';

        params['filter[4].Campo'] = 'uf';
        params['filter[4].Valor'] = uf;
        params['filter[4].FormaPesquisa'] = 'Start';

        params['filter[5].Campo'] = 'origem';
        params['filter[5].Valor'] = origem;
        params['filter[5].FormaPesquisa'] = 'Start';

        params['filter[6].Campo'] = 'destino';
        params['filter[6].Valor'] = destino;
        params['filter[6].FormaPesquisa'] = 'Start';

        params['filter[7].Campo'] = 'serieDesp';
        params['filter[7].Valor'] = serieDesp;
        params['filter[7].FormaPesquisa'] = 'Start';

        params['filter[8].Campo'] = 'numeroDesp';
        params['filter[8].Valor'] = numeroDesp;
        params['filter[8].FormaPesquisa'] = 'Start';

        params['filter[9].Campo'] = 'chaveCte';
        params['filter[9].Valor'] = chaveCte;
        params['filter[9].FormaPesquisa'] = 'Start';

        params['filter[10].Campo'] = 'codVagao';
        params['filter[10].Valor'] = codVagao;
        params['filter[10].FormaPesquisa'] = 'Start';

        params['filter[11].Campo'] = 'idMdfe';
        params['filter[11].Valor'] = idMdfe;
        params['filter[11].FormaPesquisa'] = 'Start';

        params['filter[12].Campo'] = 'idTrem';
        params['filter[12].Valor'] = idTrem;
        params['filter[12].FormaPesquisa'] = 'Start';

        params['filter[13].Campo'] = 'semaforo';
        params['filter[13].Valor'] = semaforo;
        params['filter[13].FormaPesquisa'] = 'Start';

        params['filter[14].Campo'] = 'UF';
        params['filter[14].Valor'] = siglaUfSemaforo;
        params['filter[14].FormaPesquisa'] = 'Start';

        params['filter[15].Campo'] = 'periodoHoras';
        params['filter[15].Valor'] = periodoHoraSemaforo;
        params['filter[15].FormaPesquisa'] = 'Start';

        params['filter[16].Campo'] = 'tipoCte';
        params['filter[16].Valor'] = tipoCte;
        params['filter[16].FormaPesquisa'] = 'Start';

    });

    var colunaGridComp = {
        layout: 'form',
        height: 230,
        width: '100%',
        bodyStyle: 'padding-right:5px',
        border: false,
        autoScroll: true,
        items: [gridResultadoComp]
    };

    var linhaResultadoComp = {
        layout: 'column',
        border: false,
        items: [colunaGridComp]
    };

    var btnReenviarComp = {
        name: 'btnReenviarComp',
        id: 'btnReenviarComp',
        text: 'Reenviar',
        iconCls: 'icon-save',
        handler: function(b, e) {

            var listaEnvio = Array();

            var selected = smGridComp.getSelections();

            for (var i = 0; i < selected.length; i++) {
                if (selected[i].get('Status') == 'EAR')
                    listaEnvio.push(selected[i].data.IdCte);
            }

            if (listaEnvio.length > 0) {
                if (Ext.Msg.confirm("Reemissão", "Você possui " + listaEnvio.length + " para reemissão, deseja realmente Reemitir?", function(btn, text) {
                        if (btn == 'yes') {
                            var dados = $.toJSON(listaEnvio);
                            $.ajax({
                                url: "<%= this.Url.Action("ReenviarCte") %>",
                                type: "POST",
                                dataType: 'json',
                                data: dados,
                                contentType: "application/json; charset=utf-8",
                                success: function(result) {
                                    if (result.success) {
                                        Ext.Msg.show({
                                            title: "Mensagem de Informação",
                                            msg: "Sucesso na Reemissão do(s) CTE(s)!",
                                            buttons: Ext.Msg.OK,
                                            minWidth: 200
                                        });
                                        gridResultadoComp.getStore().removeAll();
                                        gridResultadoComp.getStore().load();
                                    } else {
                                        Ext.Msg.show({
                                            title: "Mensagem de Erro",
                                            msg: "Erro na Reemissão do(s) CTe(s)!</BR>" + result.Message,
                                            buttons: Ext.Msg.OK,
                                            icon: Ext.MessageBox.ERROR,
                                            minWidth: 200
                                        });
                                    }
                                },
                                failure: function(result) {
                                    Ext.Msg.show({
                                        title: "Mensagem de Erro",
                                        msg: "Erro na Reemissão do(s) CTe(s)!</BR>" + result.Message,
                                        buttons: Ext.Msg.OK,
                                        icon: Ext.MessageBox.ERROR,
                                        minWidth: 200
                                    });
                                }
                            });
                        }
                    })
                );
            } else {
                Ext.Msg.show({
                    title: "Atenção",
                    msg: "Apesar de selecionados, os ctes não estão autorizados para reenvio.",
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.INFO,
                    minWidth: 200
                });
            }
        }
    };

    var btnInutilizarComp = {
        name: 'btnInutilizarComp',
        id: 'btnInutilizarComp',
        text: 'Inutilizar',
        iconCls: 'icon-save',
        handler: function(b, e) {
            var listaEnvio = Array();

            var selected = smGridComp.getSelections();

            for (var i = 0; i < selected.length; i++) {
                if (selected[i].get('Status') != 'AUT')
                    listaEnvio.push(selected[i].data.IdCte);
            }
            if (listaEnvio.length > 0) {

                if (Ext.Msg.confirm("Inutilização", "Você possui " + listaEnvio.length + " para inutilização, deseja realmente inutilizar?", function(btn, text) {
                    if (btn == 'yes') {
                        var dados = $.toJSON(listaEnvio);
                        $.ajax({
                            url: "<%= this.Url.Action("InutilizarCte") %>",
                            type: "POST",
                            dataType: 'json',
                            data: dados,
                            contentType: "application/json; charset=utf-8",
                            success: function(result) {
                                if (result.success) {
                                    var ctesNaoInutilizados = "";

                                    for (var i = 0; i < result.NaoPodeInutilizar.length; i++) {
                                        if (i + 1 < result.NaoPodeInutilizar.length) {
                                            ctesNaoInutilizados += result.NaoPodeInutilizar[i] + ", ";
                                        } else {
                                            ctesNaoInutilizados += result.NaoPodeInutilizar[i];
                                        }
                                    }

                                    var msgm = ctesNaoInutilizados != "" ?
                                        "Nem todos os ctes puderam ser inutilizados: " + ctesNaoInutilizados + ", os demais foram inutilizados com sucesso!" :
                                        "Sucesso na Inutilização do(s) CTe(s)!";

                                    Ext.Msg.show({
                                        title: "Mensagem de Informação",
                                        msg: msgm,
                                        buttons: Ext.Msg.OK,
                                        minWidth: 200
                                    });

                                    gridResultadoComp.getStore().removeAll();
                                    gridResultadoComp.getStore().load();
                                } else {
                                    Ext.Msg.show({
                                        title: "Mensagem de Erro",
                                        msg: "Erro na Inutilização do(s) CTe(s)!</BR>" + result.Message,
                                        buttons: Ext.Msg.OK,
                                        icon: Ext.MessageBox.ERROR,
                                        minWidth: 200
                                    });
                                }
                            },
                            failure: function(result) {
                                Ext.Msg.show({
                                    title: "Mensagem de Erro",
                                    msg: "Erro na Inutilização do(s) CTe(s)!</BR>" + result.Message,
                                    buttons: Ext.Msg.OK,
                                    icon: Ext.MessageBox.ERROR,
                                    minWidth: 200
                                });
                            }
                        });
                    }
                }));
            } else {
                Ext.Msg.show({
                    title: "Atenção",
                    msg: "Apesar de selecionados, os ctes não podem ser inutilizados.",
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.INFO,
                    minWidth: 200
                });
            }
        }
    };

    var btnCancelarComp = {
        name: 'btnCancelarComp',
        id: 'btnCancelarComp',
        text: 'Cancelar',
        iconCls: 'icon-save',
        handler: function(b, e) {
            var listaEnvio = new Array();
            var selected = smGridComp.getSelections();

            for (var i = 0; i < selected.length; i++) {
                listaEnvio.push(selected[i].data.CteId);
            }

            Ext.Msg.confirm("Reemissão Cancelamento", "Você possui " + listaEnvio.length + " para reemitir o cancelamento, deseja realmente Reemitir?", function(btn, text) {

                if (btn != 'yes')
                    return;

                var dados = $.toJSON(listaEnvio);

                $.ajax({
                    url: "<%= this.Url.Action("CancelamentoCte") %>",
                    type: "POST",
                    dataType: 'json',
                    data: dados,
                    contentType: "application/json; charset=utf-8",
                    success: function(result) {
                        if (result.success) {

                            var ctesNaoCancelados = "";

                            for (var i = 0; i < result.NaoPodeCancelar.length; i++) {
                                if (i + 1 < result.NaoPodeCancelar.length) {
                                    ctesNaoCancelados += result.NaoPodeCancelar[i] + ", ";
                                } else {
                                    ctesNaoCancelados += result.NaoPodeCancelar[i];
                                }
                            }

                            var msgm = ctesNaoCancelados != "" ?
                                "Nem todos os ctes puderam ser cancelador: " + ctesNaoCancelados + ", os demais foram cancelados com sucesso!" :
                                "Sucesso na Reemissão do Cancelamento do(s) CTE(s)!";

                            Ext.Msg.show({ title: "Mensagem de Informação", msg: msgm, buttons: Ext.Msg.OK });

                            gridResultadoComp.getStore().removeAll();
                            gridResultadoComp.getStore().load();

                        } else {
                            Ext.Msg.show({ title: "Mensagem de Erro", msg: "Erro na Reemissão do Cancelamento do(s) CTe(s)!</BR>" + result.Message, buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                        }
                    },
                    failure: function(result) {
                        Ext.Msg.show({ title: "Mensagem de Erro", msg: "Erro na Reemissão do Cancelamento do(s) CTe(s)!</BR>" + result.Message, buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                    }
                });
            });
        }
    };

    var btnImprimirComp = {
        name: 'btnImprimirComp',
        id: 'btnImprimirComp',
        text: 'Imprimir',
        iconCls: 'icon-printer',
        handler: function(b, e) {
            var selected = smGridComp.getSelections();
            var arrCte = new Array();
            for (var i = 0; i < selected.length; i++) {
                arrCte.push(selected[i].data.IdCte);
            }

            windowStatusImpressao = new Ext.Window({
                id: 'windowStatusImpressao',
                title: 'Status da impressão',
                modal: true,
                width: 414,
                height: 430,
                resizable: false,
                autoLoad: {
                    url: '<%= this.Url.Action("FormStatusImpressao") %>',
                    params: { 'arrayCtes': arrCte },
                    scripts: true,
                    callback: function(el, sucess) {
                        if (!sucess) {
                            windowStatusImpressao.close();
                        }
                    }
                }
            });
            windowStatusImpressao.show();
        }
    };

    var linhaBotoesComp = {
        layout: 'column',
        border: false,
        bodyStyle: 'padding-bottom:0px;',
        buttons: [btnReenviarComp, btnInutilizarComp, btnImprimirComp],
        buttonAlign: 'center'
    };
    var fieldSetResultadoComp = {
        xtype: 'fieldset',
        title: 'Vagões',
        width: '100%',
        bodyStyle: 'padding:5px',
        border: true,
        items: [linhaResultadoComp, linhaBotoesComp]
    };

    var coluna1LinhaTabComp = {
        layout: 'form',
        border: false,
        width: '100%',
        items: [fieldSetResultadoComp]
    };

    linhaTabComp = {
        layout: 'column',
        border: false,
        width: '100%',
        items: [coluna1LinhaTabComp]
    };

</script>