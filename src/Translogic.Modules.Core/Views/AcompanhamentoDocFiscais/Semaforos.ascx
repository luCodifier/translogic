﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<script type="text/javascript">
    var gridSemaforoCte = null;
    var gridSemaforoMdfe = null;
    var grids = null;
    var linhaSemaforos = null;
    var semaforo = null;
    var siglaUfSemaforo = null;
    var periodoHoraSemaforo = null;

    $(function () {

        function semaforoVerdeRenderer(value, meta) {
            meta.style += " background-color:#33BB00; opacity:0.8;";
            return value;
        }

        function semaforoAmareloRenderer(value, meta) {
            meta.style += " background-color:#FFEE33; opacity:0.8;";
            return value;
        }

        function semaforoVermelhoRenderer(value, meta) {
            meta.style += " background-color:#FF3322; opacity:0.8;";
            return value;
        }

        var dsSemaforoCte = new Ext.data.JsonStore({
            id: 'dsSemaforoCte',
            name: 'dsSemaforoCte',
            root: 'SemaforoCtes',
            url: '<% =Url.Action("ObterSemaforoCte") %>',
            fields: [
                'Uf',
                'ZeroDoze',
                'DozeVinteQuatro',
                'MaisQueVinteCinco'
            ]
        });

        var dsSemaforoMdfe = new Ext.data.JsonStore({
            id: 'dsSemaforoMdfe',
            name: 'dsSemaforoMdfe',
            root: 'SemaforoMdfes',
            url: '<% =Url.Action("ObterSemaforoMdfe") %>',
            fields: [
                'Uf',
                'ZeroDoze',
                'DozeVinteQuatro',
                'MaisQueVinteCinco'
            ]
        });

        /********************************************GRIDS***************************************************/
        gridSemaforoCte = new Ext.grid.GridPanel({
            id: 'gridSemaforoCte',
            name: 'gridSemaforoCte',
            stripeRows: true,
            autoLoadGrid: false,
            width: 520,
            autoScroll: true,
            ds: dsSemaforoCte,
            loadMask: { msg: App.Resources.Web.Carregando },
            columnLines: true,
            columns:
            [
                { header: 'Estado', dataIndex: 'Uf', width: 280, sortable: false },
                { header: '0-12hrs', dataIndex: 'ZeroDoze', width: 75, sortable: false, renderer: semaforoVerdeRenderer },
                { header: '12-24hrs', dataIndex: 'DozeVinteQuatro', width: 75, sortable: false, renderer: semaforoAmareloRenderer },
                { header: '+24hrs', dataIndex: 'MaisQueVinteCinco', width: 75, sortable: false, renderer: semaforoVermelhoRenderer }
            ]
        });

        gridSemaforoCte.on({
            celldblclick: function (grid, rowIndex, columnIndex, e) {

                tabResultados.setActiveTab(2);

                idTremSelecionado = 0;
                idMdfeSelecionado = 0;
                idDespachoSelecionado = 0;
                
                semaforo = "CTE";
                var sel = gridSemaforoCte.getSelectionModel().getSelected();
                siglaUfSemaforo = sel.data.Uf;

                switch (columnIndex) {
                    case 1:
                        periodoHoraSemaforo = "0-12";
                        break;
                    case 2:
                        periodoHoraSemaforo = "12-24";
                        break;
                    case 3:
                        periodoHoraSemaforo = "+24";
                        break;
                    default:
                        periodoHoraSemaforo = "Todas";
                        break;
                }

                gridResultadoComp.getStore().load();

            }
        });

        gridSemaforoMdfe = new Ext.grid.GridPanel({
            id: 'gridSemaforoMdfe',
            name: 'gridSemaforoMdfe',
            stripeRows: true,
            width: 520,
            autoLoadGrid: false,
            autoScroll: true,
            ds: dsSemaforoMdfe,
            loadMask: { msg: App.Resources.Web.Carregando },
            columns:
            [
                { header: 'Estado', dataIndex: 'Uf', width: 280, sortable: false },
                { header: '0-12hrs', dataIndex: 'ZeroDoze', width: 75, sortable: false, renderer: semaforoVerdeRenderer },
                { header: '12-24hrs', dataIndex: 'DozeVinteQuatro', width: 75, sortable: false, renderer: semaforoAmareloRenderer },
                { header: '+24hrs', dataIndex: 'MaisQueVinteCinco', width: 75, sortable: false, renderer: semaforoVermelhoRenderer }
            ]
        });

        gridSemaforoMdfe.on({
            celldblclick: function (grid, rowIndex, columnIndex, e) {

                tabResultados.setActiveTab(1);

                idTremSelecionado = 0;
                idMdfeSelecionado = 0;
                idDespachoSelecionado = 0;
                
                semaforo = "MDFE";
                var sel = gridSemaforoMdfe.getSelectionModel().getSelected();
                siglaUfSemaforo = sel.data.Uf;

                switch (columnIndex) {
                    case 1:
                        periodoHoraSemaforo = "0-12";
                        break;
                    case 2:
                        periodoHoraSemaforo = "12-24";
                        break;
                    case 3:
                        periodoHoraSemaforo = "+24";
                        break;
                    default:
                        periodoHoraSemaforo = "Todas";
                        break;
                }

                gridResultadoMdfe.getStore().load();

            }
        });

        /**********************************LAYOUT***********************************/
        var fieldSetSemaforoCte = {
            xtype: 'fieldset',
            title: 'Cte',
            width: 550,
            border: true,
            items: [gridSemaforoCte]
        };
        var fieldSetSemaforoMdfe = {
            xtype: 'fieldset',
            width: 550,
            title: 'MDFe',
            border: true,
            items: [gridSemaforoMdfe]
        };

        var coluna1Semaforos = {
            layout: 'form',
            bodyStyle: 'padding: 0px 5px 0px 5px',
            border: false,
            items: [fieldSetSemaforoCte]
        };
        var coluna2Semaforos = {
            layout: 'form',
            bodyStyle: 'padding: 0px 5px 0px 5px',
            border: false,
            items: [fieldSetSemaforoMdfe]
        };

        var linhaFieldSets = {
            layout: 'column',
            autoWidth: true,
            border: false,
            items: [coluna1Semaforos, coluna2Semaforos]
        };

        var fieldSets = {
            xtype: 'fieldset',
            width: '99.8%',
            title: 'Semáforos',
            border: true,
            items: [linhaFieldSets],
            collapsible: true,
            collapsed: true
        };

        linhaSemaforos = {
            layout: 'column',
            width: '100%',
            border: false,
            items: [fieldSets]
        };

        gridSemaforoCte.getStore().load();
        gridSemaforoMdfe.getStore().load();

    });
</script>
