﻿<%@ Import Namespace="System.Security.Policy" %>

<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Interna.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content runat="server" ID="HeadContent" ContentPlaceHolderID="HeadContent">
    <script type="text/javascript">

        var formModal = null;
        var grid = null;
        var windCanc;
        var lastRowSelected = -1;
        
        // Flag para garantir que uma tela de mensagem que está sobreposta não chame novamente a tela de pesquisa
        var executarPesquisa = true;

        function showResult(btn) {  
            executarPesquisa = true;
        }

        function FormError(form, action) {
            Ext.Msg.show({
                title: "Mensagem de Erro",
                msg: action.result.Message,
                buttons: Ext.Msg.OK,
                icon: Ext.MessageBox.ERROR,
                minWidth: 200
            });
            grid.getStore().removeAll();
        }

        function Pesquisar() {
            var diferenca = Ext.getCmp("filtro-data-final").getValue() - Ext.getCmp("filtro-data-inicial").getValue();
            diferenca = Math.floor(diferenca / (1000 * 60 * 60 * 24));

            if (Ext.getCmp("filtro-data-inicial").getValue() == '' || Ext.getCmp("filtro-data-final").getValue() == '') {
                executarPesquisa = false;
                Ext.Msg.show({
                    title: "Mensagem de Erro",
                    msg: "Preencha os filtro datas!",
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.ERROR,
                    minWidth: 200,
                    fn: showResult
                });
            }
            else if (diferenca > 30) {
                executarPesquisa = false;
                Ext.Msg.show({
                    title: "Mensagem de Erro",
                    msg: "O período da pesquisa não deve ultrapassar 30 dias",
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.ERROR,
                    minWidth: 200,
                    fn: showResult
                });
            }
            else if (Ext.getCmp("filtro-serie").getValue() != '' && Ext.getCmp("filtro-despacho").getValue() == '') {
                executarPesquisa = false;
                Ext.Msg.show({
                    title: "Mensagem de Erro",
                    msg: "Não é possível filtrar apenas pela a Série do Despacho.<br/>Também é necessário preencher o número do Despacho.",
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.ERROR,
                    minWidth: 200,
                    fn: showResult
                });
            }
            else if (Ext.getCmp("filtro-despacho").getValue() != '' && Ext.getCmp("filtro-serie").getValue() == '') {
                executarPesquisa = false;
                Ext.Msg.show({
                    title: "Mensagem de Erro",
                    msg: "Não é possível filtrar apenas pelo número do Despacho.<br/>Também é necessário preencher a Série do Despacho.",
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.ERROR,
                    minWidth: 200,
                    fn: showResult
                });
            }
            else {
                executarPesquisa = true;
                grid.getStore().load();
            }
        }

        /* ------------- Ação CTRL+C nos campos da Grid ------------- */
        var isCtrl = false;
        document.onkeyup = function (e) {
            var keyID = event.keyCode;

            // 17 = tecla CTRL
            if (keyID == 17) {
                isCtrl = false; //libera tecla CTRL
            }
        }

        document.onkeydown = function (e) {

            var keyID = event.keyCode;

            // verifica se CTRL esta acionado
            if (keyID == 17) {
                isCtrl = true;
            }

            if ((keyID == 13) && (executarPesquisa)) {
                Pesquisar();
                return;
            }

            // 67 = tecla 'c'
            if (keyID == 67 && isCtrl == true) {

                // verifica se há selecão na tabela
                if (grid.getSelectionModel().hasSelection()) {
                    // captura todas as linhas selecionadas
                    var recordList = grid.getSelectionModel().getSelections();

                    var a = [];
                    var separator = '\t'; // separador para colunas no excel
                    for (var i = 0; i < recordList.length; i++) {
                        var s = '';
                        var item = recordList[i].data;
                        for (key in item) {
                            if (key != 'Id') { //elimina o campo id da linha
                                s = s + item[key] + separator;
                            }
                        }
                        s = s.substr(0, s.length - 1); //retira o ultimo separador '\t' da linha
                        a.push(s);
                    }
                    window.clipboardData.setData('Text', (a.join('\n'))); //insere linhas no clipBoard
                }
            }
        }
        /* ------------------------------------------------------- */

        $(function () {

            sm2 = new Ext.grid.CheckboxSelectionModel({
                listeners: {
                    selectionchange: function (sm) {

                        if (sm.getCount()) {
                            Ext.getCmp("salvar").enable();
                        } else {
                            Ext.getCmp("salvar").disable();
                        }
                    }
                }
            });

            var dsCodigoControle = new Ext.data.JsonStore({
                root: "Items",
                autoLoad: true,
                url: '<%= Url.Action("ObterCodigoSerieDesp") %>',
                fields: [
                        'Id',
                        'CodigoControle'
			        ]
            });

            grid = new Translogic.PaginatedGrid({
                autoLoadGrid: false,
                id: "gridCte",
                url: '<%= Url.Action("ObterCtes") %>',
                stripeRows: false,
                region: 'center',
                loadMask: { msg: App.Resources.Web.Carregando },
                viewConfig: {
                    forceFit: true,
                    getRowClass: MudaCor
                },
                fields: [
                    'CteId',
                    'Fluxo',
                    'Origem',
                    'Destino',
                    'Mercadoria',
                    'ClienteFatura',
                    'Cte',
					'CodigoVagao',
                    'Serie',
                    'Despacho',
                    'DateEmissao',
                    'CteComAgrupamentoNaoAutorizado',
                    'CtePago',
                    'ForaDoTempoCancelamento'
					],
                columns: [
						new Ext.grid.RowNumberer(),
                        sm2,
                        { dataIndex: "CteId", hidden: true },
                        { header: 'Num Vagão', dataIndex: "CodigoVagao", width: 100, sortable: false },
						{ header: 'Fluxo', dataIndex: "Fluxo", sortable: false },
						{ header: 'Origem', dataIndex: "Origem", sortable: false },
						{ header: 'Destino', dataIndex: "Destino", sortable: false },
				        { header: 'CTe', dataIndex: "Cte", sortable: false },
                        { header: 'Série', dataIndex: "Serie", sortable: false },
				        { header: 'Despacho', dataIndex: "Despacho", sortable: false },
				        { header: 'Data Emissão', dataIndex: "DateEmissao", sortable: false },
                        { dataIndex: "CteComAgrupamentoNaoAutorizado", hidden: true },
                        { dataIndex: "CtePago", hidden: true },
                        { dataIndex: "ForaDoTempoCancelamento", hidden: true }
					],
                sm: sm2,
                listeners: {
                    rowclick: function (grid, rowIndex, e) {

                        if (sm2.isSelected(rowIndex)) {
                            if (rowIndex == lastRowSelected) {
                                grid.getSelectionModel().deselectRow(rowIndex);
                                lastRowSelected = -1;
                            }
                            else {
                                lastRowSelected = rowIndex;
                            }
                        }
                    }
                }
            });

            function MudaCor(row, index) {
                if (row.data.CteComAgrupamentoNaoAutorizado) {
                    return 'corRed';
                }
                else if (row.data.CtePago) {
                    return 'corOrange';
                }
                else if (row.data.ForaDoTempoCancelamento) {
                    return 'corYellow';
                }
            }

            var cboCteCodigoControle = {
                xtype: 'combo',
                store: dsCodigoControle,
                allowBlank: true,
                lazyInit: false,
                lazyRender: false,
                mode: 'local',
                typeAhead: false,
                triggerAction: 'all',
                fieldLabel: 'UF DCL',
                name: 'filtro-UfDcl',
                id: 'filtro-UfDcl',
                hiddenName: 'filtro-UfDcl',
                displayField: 'CodigoControle',
                forceSelection: true,
                width: 70,
                valueField: 'Id',
                emptyText: 'Selecione...',
                editable: false,
                tpl: '<tpl for="."><div class="x-combo-list-item">{CodigoControle}&nbsp;</div></tpl>'
            };

            var dataAtual = new Date();

            var dataInicial = {
                xtype: 'datefield',
                fieldLabel: 'Data Inicial',
                id: 'filtro-data-inicial',
                name: 'dataInicial',
                width: 83,
                allowBlank: false,
                vtype: 'daterange',
                endDateField: 'filtro-data-final',
                hiddenName: 'dataInicial',
                value: dataAtual
            };

            var dataFinal = {
                xtype: 'datefield',
                fieldLabel: 'Data Final',
                id: 'filtro-data-final',
                name: 'dataFinal',
                width: 83,
                allowBlank: false,
                vtype: 'daterange',
                startDateField: 'filtro-data-inicial',
                hiddenName: 'dataFinal',
                value: dataAtual
            };

            var origem = {
                xtype: 'textfield',
                vtype: 'cteestacaovtype',
                style: 'text-transform: uppercase',
                id: 'filtro-Ori',
                fieldLabel: 'Origem',
                autoCreate: { tag: 'input', type: 'text', maxlength: '3', autocomplete: 'off' },
                name: 'Origem',
                allowBlank: true,
                maxLength: 3,
                width: 35,
                hiddenName: 'Origem'
            };

            var destino = {
                xtype: 'textfield',
                vtype: 'cteestacaovtype',
                style: 'text-transform: uppercase',
                id: 'filtro-Dest',
                fieldLabel: 'Destino',
                autoCreate: { tag: 'input', type: 'text', maxlength: '3', autocomplete: 'off' },
                name: 'Destino',
                allowBlank: true,
                maxLength: 3,
                width: 35,
                hiddenName: 'Destino'
            };

            var serie = {
                xtype: 'textfield',
                vtype: 'ctesdvtype',
                style: 'text-transform: uppercase',
                id: 'filtro-serie',
                fieldLabel: 'Série',
                name: 'serie',
                allowBlank: true,
                autoCreate: { tag: 'input', type: 'text', maxlength: '3', autocomplete: 'off' },
                maxLength: 20,
                width: 30,
                hiddenName: 'serie'
            };

            var despacho = {
                xtype: 'textfield',
                vtype: 'ctedespachovtype',
                id: 'filtro-despacho',
                fieldLabel: 'Despacho',
                name: 'despacho',
                allowBlank: true,
                maxLength: 20,
                autoCreate: { tag: 'input', type: 'text', maxlength: '6', autocomplete: 'off' },
                width: 55,
                hiddenName: 'despacho'
            };

            var fluxo = {
                xtype: 'textfield',
                vtype: 'ctefluxovtype',
                style: 'text-transform: uppercase',
                id: 'filtro-fluxo',
                fieldLabel: 'Fluxo',
                name: 'fluxo',
                allowBlank: true,
                autoCreate: { tag: 'input', type: 'text', maxlength: '5', autocomplete: 'off' },
                maxLength: 20,
                width: 45,
                minValue: 0,
                maxValue: 99999,
                hiddenName: 'fluxo'
            };

            var Vagao = {
                xtype: 'textfield',
                vtype: 'ctevagaovtype',
                style: 'text-transform: uppercase',
                id: 'filtro-numVagao',
                fieldLabel: 'Vagão',
                name: 'numVagao',
                allowBlank: true,
                autoCreate: { tag: 'input', type: 'text', maxlength: '7', autocomplete: 'off' },
                maxLength: 20,
                width: 55,
                hiddenName: 'numVagao'
            };

            var chave = {
                xtype: 'textfield',
                vtype: 'ctevtype',
                id: 'filtro-Chave',
                fieldLabel: 'Chave CTe',
                name: 'Chave',
                autoCreate: { tag: 'input', type: 'text', maxlength: '44', autocomplete: 'off' },
                allowBlank: true,
                maxLength: 50,
                width: 275,
                hiddenName: 'Chave  '
            };

            var arrDataIni = {
                width: 87,
                layout: 'form',
                border: false,
                items:
				[dataInicial]
            };

            var arrDataFim = {
                width: 87,
                layout: 'form',
                border: false,
                items:
						[dataFinal]
            };

            var arrOrigem = {
                width: 43,
                layout: 'form',
                border: false,
                items:
						[origem]
            };
            var arrDestino = {
                width: 43,
                layout: 'form',
                border: false,
                items:
						[destino]
            };

            var arrSerie = {
                width: 37,
                layout: 'form',
                border: false,
                items:
								[serie]
            };

            var arrDespacho = {
                width: 60,
                layout: 'form',
                border: false,
                items:
						[despacho]
            };

            var arrFluxo = {
                width: 50,
                layout: 'form',
                border: false,
                items:
						[fluxo]
            };

            var arrVagao = {
                width: 60,
                layout: 'form',
                border: false,
                items:
				[Vagao]
            };

            var arrChave = {
                width: 280,
                layout: 'form',
                border: false,
                items:
						[chave]
            };

           var arrCodigoDcl = {
                width: 75,
                layout: 'form',
                border: false,
                items:
					[cboCteCodigoControle]
            };


            var arrlinha1 = {
                layout: 'column',
                border: false,
                items: [arrDataIni, arrDataFim, arrFluxo, arrChave]
            };

            var arrlinha2 = {
                layout: 'column',
                border: false,
                items: [arrCodigoDcl, arrSerie, arrDespacho, arrOrigem, arrDestino, arrVagao]
            };

            var arrCampos = new Array();
            arrCampos.push(arrlinha1);
            arrCampos.push(arrlinha2);

            filters = new Ext.form.FormPanel({
                id: 'grid-filtros',
                title: "Filtros",
                region: 'center',
                bodyStyle: 'padding: 15px',
                labelAlign: 'top',
                items:
			[arrCampos],
                buttonAlign: "center",
                buttons:
	        [
                {
                    text: 'Inutilizar',
                    id: 'salvar',
                    iconCls: 'icon-save',
                    disabled: true,
                    handler: function (b, e) {

                        SalvarInutilizacao();
                    },
                    scope: this
                },
                {
                    text: 'Pesquisar',
                    type: 'submit',
                    iconCls: 'icon-find',
                    handler: function (b, e) {
                        Pesquisar();
                    }
                },
	            {
	                text: 'Limpar',
	                handler: function (b, e) {

	                    grid.getStore().removeAll();
	                    grid.getStore().totalLength = 0;
	                    grid.getBottomToolbar().bind(grid.getStore());
	                    grid.getBottomToolbar().updateInfo();
	                    Ext.getCmp("grid-filtros").getForm().reset();

	                },
	                scope: this
	            }
            ]
            });

            grid.getStore().proxy.on('beforeload', function (p, params) {

                params['filter[0].Campo'] = 'dataInicial';
                params['filter[0].Valor'] = new Array(Ext.getCmp("filtro-data-inicial").getValue().format('d/m/Y') + " " + Ext.getCmp("filtro-data-inicial").getValue().format('H:i:s'));
                params['filter[0].FormaPesquisa'] = 'Start';

                params['filter[1].Campo'] = 'dataFinal';
                params['filter[1].Valor'] = new Array(Ext.getCmp("filtro-data-final").getValue().format('d/m/Y') + " " + Ext.getCmp("filtro-data-inicial").getValue().format('H:i:s'));
                params['filter[1].FormaPesquisa'] = 'Start';

                params['filter[2].Campo'] = 'serie';
                params['filter[2].Valor'] = Ext.getCmp("filtro-serie").getValue();
                params['filter[2].FormaPesquisa'] = 'Start';

                params['filter[3].Campo'] = 'despacho';
                params['filter[3].Valor'] = Ext.getCmp("filtro-despacho").getValue();
                params['filter[3].FormaPesquisa'] = 'Start';

                params['filter[4].Campo'] = 'fluxo';
                params['filter[4].Valor'] = Ext.getCmp("filtro-fluxo").getValue();
                params['filter[4].FormaPesquisa'] = 'Start';

                params['filter[5].Campo'] = 'chave';
                params['filter[5].Valor'] = Ext.getCmp("filtro-Chave").getValue();
                params['filter[5].FormaPesquisa'] = 'Start';

                params['filter[6].Campo'] = 'Origem';
                params['filter[6].Valor'] = Ext.getCmp("filtro-Ori").getValue();
                params['filter[6].FormaPesquisa'] = 'Start';

                params['filter[7].Campo'] = 'Destino';
                params['filter[7].Valor'] = Ext.getCmp("filtro-Dest").getValue();
                params['filter[7].FormaPesquisa'] = 'Start';

                params['filter[8].Campo'] = 'Vagao';
                params['filter[8].Valor'] = Ext.getCmp("filtro-numVagao").getValue();
                params['filter[8].FormaPesquisa'] = 'Start';

                params['filter[9].Campo'] = 'UfDcl';
                params['filter[9].Valor'] = Ext.getCmp("filtro-UfDcl").getRawValue();
                params['filter[9].FormaPesquisa'] = 'Start';
            });

            new Ext.Viewport({
                layout: 'border',
                margins: 10,
                items: [
			{
			    region: 'north',
			    height: 240,
			    autoScroll: true,
			    items: [{
			        region: 'center',
			        applyTo: 'header-content'
			    },
				filters]
			},
			grid
		]
            });

        });

        function onCancelamentoSuccess() {
            grid.getStore().removeAll();
            Ext.Msg.alert('Cancelamento', 'CTe Cancelado com sucesso!');

            Pesquisar();
            grid.getStore().load();

        }

        function SalvarInutilizacao() {
            
            var listaEnvio = Array();
         
			var selected = sm2.getSelections();

			for (var i = 0; i < selected.length; i++) {
                if(selected[i].get('SituacaoCte') != 'AUT')
					listaEnvio.push(selected[i].data.CteId);
            }

	        if (Ext.Msg.confirm("Inutilização", "Você possui " + listaEnvio.length + " para inutilização, deseja realmente inutilizar?", function(btn, text) {
		        if (btn == 'yes') 
		        {
			        var dados = $.toJSON(listaEnvio);
			        $.ajax({
				        url: "<%= Url.Action("Inutilizacao") %>",
				        type: "POST",
				        dataType: 'json',
				        data: dados,
				        contentType: "application/json; charset=utf-8",
				        success: function(result) {
					        if(result.success) {
						        Ext.Msg.show({
							        title: "Mensagem de Informação",
							        msg: "Sucesso na Inutilização do(s) CTe(s)!",
							        buttons: Ext.Msg.OK,
							        minWidth: 200
						        });
                                Pesquisar();
						       
					        } 
					        else {
						        Ext.Msg.show({
								        title: "Mensagem de Erro",
								        msg: "Erro na Inutilização do(s) CTe(s)!</BR>" + result.Message,
								        buttons: Ext.Msg.OK,
								        icon: Ext.MessageBox.ERROR,
								        minWidth: 200
							        });
					        }
				        },
				        failure: function(result) {
					        Ext.Msg.show({
						        title: "Mensagem de Erro",
						        msg: "Erro na Inutilização do(s) CTe(s)!</BR>" + result.Message,
						        buttons: Ext.Msg.OK,
						        icon: Ext.MessageBox.ERROR,
						        minWidth: 200
					        });
				        }
			        });
			        }
		        }));
        }

        function MostrarWindowErros(mensagensErro) {
            var textarea = new Ext.form.TextArea({
                xtype: 'textarea',
                fieldLabel: 'Foram encontrados os seguintes erros',
                value: mensagensErro,
                readOnly: true,
                height: 220,
                width: 395
            });

            var formErros = new Ext.FormPanel({
                id: 'formErros',
                bodyStyle: 'padding: 15px',
                labelAlign: 'top',
                items: [textarea],
                buttonAlign: 'center',
                buttons: [{
                    text: 'Fechar',
                    type: 'button',
                    handler: function (b, e) {
                        windowErros.close();
                    }
                }]
            });

            windowErros = new Ext.Window({
                id: 'windowErros',
                title: 'Erros',
                modal: true,
                width: 444,
                height: 350,
                items: [formErros]
            });
            windowErros.show();
        }
    
    </script>
    <style>
        .corRed
        {
            background-color: #FFEEDD;
        }
        .corOrange
        {
            background-color: #FFC58A;
        }
        .corYellow
        {
            background-color: #FFFF80;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div id="header-content">
        <h1>
            CTe - Inutilização</h1>
        <small>Você está em CTe > Inutilização</small>
        <br />
    </div>
</asp:Content>
