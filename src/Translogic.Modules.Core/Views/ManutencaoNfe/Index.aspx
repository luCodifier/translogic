﻿<%@ Page Title="" Language="C#" Inherits="System.Web.Mvc.ViewPage" MasterPageFile="~/Views/Shared/Interna.master" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server">

	<script type="text/javascript">
        //***************************** VARIÁVEIS *****************************//
        var nfe = null;

	    //***************************** FUNCOES *****************************//
	    function GetTextUpperField(id, fieldLabel, maxLength, width, allowBlank) {

	        maxLength = maxLength || 50;
	        width = width || 100;

	        if (allowBlank == null)
	            allowBlank = true;

	        var text = {
	            xtype: 'textfield',
	            maxLength: maxLength,
	            width: width,
	            name: id,
	            id: id,
	            allowBlank: allowBlank,
	            fieldLabel: fieldLabel,
	            style: { textTransform: "uppercase" },
	            listeners:
                {
                    change: Upper
                }
	        };

	        return text;
	    }

	    function Upper(field, newValue, oldValue) {
	        field.setValue(newValue.toUpperCase());
	    }

	    function Ajax(url, params, funcao) {
	        Ext.getBody().mask("Processando dados...", "x-mask-loading");

	        Ext.Ajax.request(
            {
                url: url,
                params: params,
                timeout: 300000,
                failure: function(conn, data){
					Ext.Msg.alert("Mensagem de Erro","Ocorreu um erro inesperado.");
					Ext.getBody().unmask();
				},
                success: function (response) {
                    var result = Ext.decode(response.responseText);
                    
                    if (result.success == true) {
                        Ext.Msg.show({
                            title: 'Aviso',
                            msg: result.Message,
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.INFO
                        });

                        funcao(result);
                    }
                    else if (result.success == false){
                        Ext.Msg.show({
                            title: 'Ops...',
                            msg: result.Message,
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.ERROR
                        });
                    }
                    else {
                        funcao(result);
                    }

                    Ext.getBody().unmask();
                }
            });
	    }
	    
	    String.prototype.trim = function() {
            return this.replace(/^\s+|\s+$/g, "");
        };


	    function Salvar() {
            if(nfe == null)
                return;
	        
	        // -> Validações        
            if (!painel.form.isValid()) {
                Ext.Msg.show({ title: 'Aviso', msg: 'Por favor preencha os campos corretamente!', buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR });
                return;
            }
	        
	        Ext.getBody().mask("Processando dados...", "x-mask-loading");

	        for (var key in nfe) {
                if (nfe.hasOwnProperty(key)) {
                    var field = Ext.getCmp(key);
                        
                    if(field != null) {
                        var value = field.getValue();
                        if(value) {
                            value = value.trim();
                            nfe[key] = value;
                        }
                    }
                }
            }

	        nfe.CepDestinatario = nfe.CepDestinatario == null ? null : nfe.CepDestinatario.replace(/[^\d]+/g,'');
	        nfe.CepEmitente = nfe.CepEmitente == null ? null : nfe.CepEmitente.replace(/[^\d]+/g,'');
	        nfe.CnpjDestinatario = nfe.CnpjDestinatario.replace(/[^\d]+/g,'');
	        nfe.CnpjEmitente = nfe.CnpjEmitente.replace(/[^\d]+/g,'');
	        
	        $.ajax({
		        url: "<%= Url.Action("Salvar") %>",
		        type: "POST",
		        dataType: 'json',
		        data: $.toJSON(nfe),
	            timeout: 300000,
		        contentType: "application/json; charset=utf-8",
	            failure: function(conn, data){
					Ext.Msg.alert("Mensagem de Erro","Ocorreu um erro inesperado.");
					Ext.getBody().unmask();
				},
	            success: function (result) {
                    if (result.success == true) {
                        Ext.Msg.show({
                            title: 'Aviso',
                            msg: result.Message,
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.INFO
                        });

                        Cancelar();
                    }
                    else {
                        Ext.Msg.show({
                            title: 'Ops...',
                            msg: result.Message,
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.ERROR
                        });
                    }

                    Ext.getBody().unmask();
                }
	        });
	    }

	    function Cancelar() {
	        painel.getForm().items.each(function (item) {
	            item.disable();
	            item.setValue("");
	        });

	        Ext.getCmp("Chave").enable();
	        Ext.getCmp("btnPesquisar").enable();
	        nfe = null;
	    }

	    function Pesquisar() {
	        var chave = Ext.getCmp("Chave");
	        var valorChave = chave.getValue();

	        Ajax("<%= Url.Action("Carregar") %>", { chave: valorChave }, function (dados) {
	            painel.getForm().items.each(function (item) {
	                item.enable();
	            });

	            chave.disable();
	            Ext.getCmp("btnPesquisar").disable();
	            nfe = dados;
	            
	            for (var key in dados) {
                    if (dados.hasOwnProperty(key)) {
                        var field = Ext.getCmp(key);
                        
                        if(field != null)
                            field.setValue(dados[key]);
                    }
                }
	        });
	    }
	    
	    //***************************** BOTOES *****************************//
	    var btnSalvar = {
	        name: 'btnSalvar',
	        id: 'btnSalvar',
	        text: 'Salvar',
	        iconCls: 'icon-save',
	        handler: Salvar
	    };

	    var btnCancelar = {
	        name: 'btnCancelar',
	        id: 'btnCancelar',
	        text: 'Cancelar',
	        handler: Cancelar
	    };

	    var btnPesquisar = {
	        xtype: "button",
	        name: 'btnPesquisar',
	        id: 'btnPesquisar',
	        iconCls: 'icon-find',
	        handler: Pesquisar
	    };

	    //***************************** CAMPOS *****************************//
	    var Chave = GetTextUpperField('Chave', 'Chave NF-e', 44, 300, false);
	    
	    var CnpjDestinatario = GetTextUpperField('CnpjDestinatario', 'CNPJ', 20, 120, false);
	    var CnpjEmitente = GetTextUpperField('CnpjEmitente', 'CNPJ', 20, 120, false);

	    var BairroDestinatario = GetTextUpperField('BairroDestinatario', 'Bairro', 50, 130, false);
	    var BairroEmitente = GetTextUpperField('BairroEmitente', 'Bairro', 50, 130, false);

	    var CepDestinatario = GetTextUpperField('CepDestinatario', 'CEP', 15, 120, true);
	    var CepEmitente = GetTextUpperField('CepEmitente', 'CEP', 15, 120, true);

	    var CodigoMunicipioDestinatario = GetTextUpperField('CodigoMunicipioDestinatario', 'Cod. Município', 8, 80, false);
	    var CodigoMunicipioEmitente = GetTextUpperField('CodigoMunicipioEmitente', 'Cod. Município', 8, 80, false);

	    var CodigoPaisDestinatario = GetTextUpperField('CodigoPaisDestinatario', 'Cod. País', 8, 80, false);
	    var CodigoPaisEmitente = GetTextUpperField('CodigoPaisEmitente', 'Cod. País', 8, 80, false);

	    var ComplementoDestinatario = GetTextUpperField('ComplementoDestinatario', 'Complemento', 150, 220, true);
	    var ComplementoEmitente = GetTextUpperField('ComplementoEmitente', 'Complemento', 150, 220, true);

	    var InscricaoEstadualDestinatario = GetTextUpperField('InscricaoEstadualDestinatario', 'IE', 20, 140, true);
	    var InscricaoEstadualEmitente = GetTextUpperField('InscricaoEstadualEmitente', 'IE', 20, 140, true);

	    var LogradouroDestinatario = GetTextUpperField('LogradouroDestinatario', 'Logradouro', 200, 260, false);
	    var LogradouroEmitente = GetTextUpperField('LogradouroEmitente', 'Logradouro', 200, 260, false);

	    var MunicipioDestinatario = GetTextUpperField('MunicipioDestinatario', 'Município', 50, 140, false);
	    var MunicipioEmitente = GetTextUpperField('MunicipioEmitente', 'Município', 50, 140, false);

	    var NomeFantasiaDestinatario = GetTextUpperField('NomeFantasiaDestinatario', 'Nome Fantasia', 100, 240, true);
	    var NomeFantasiaEmitente = GetTextUpperField('NomeFantasiaEmitente', 'Nome Fantasia', 100, 240, true);

	    var NumeroDestinatario = GetTextUpperField('NumeroDestinatario', 'Número', 10, 100, true);
	    var NumeroEmitente = GetTextUpperField('NumeroEmitente', 'Número', 10, 100, true);

	    var PaisDestinatario = GetTextUpperField('PaisDestinatario', 'País', 50, 120, false);
	    var PaisEmitente = GetTextUpperField('PaisEmitente', 'País', 50, 120, false);

	    var RazaoSocialDestinatario = GetTextUpperField('RazaoSocialDestinatario', 'Razão Social', 100, 240, false);
	    var RazaoSocialEmitente = GetTextUpperField('RazaoSocialEmitente', 'Razão Social', 100, 240, false);

	    var TelefoneDestinatario = GetTextUpperField('TelefoneDestinatario', 'Fone', 20, 100, true);
	    var TelefoneEmitente = GetTextUpperField('TelefoneEmitente', 'Fone', 20, 100, true);

	    var UfDestinatario = GetTextUpperField('UfDestinatario', 'UF', 2, 40, false);
	    var UfEmitente = GetTextUpperField('UfEmitente', 'UF', 2, 40, false);

	    //***************************** LAYOUT *****************************//
	    var c1Chave = {
	        layout: 'form',
	        border: false,
	        bodyStyle: 'padding: 5px',
	        items: [Chave]
	    };

	    var c2Chave = {
	        layout: 'form',
	        border: false,
	        bodyStyle: 'padding: 5px',
	        items: [btnPesquisar]
	    };

	    var l1Chave = {
	        layout: 'column',
	        border: false,
	        items: [c1Chave, c2Chave]
	    };

	    var fieldsetChave = {
	        xtype: 'fieldset',
	        title: 'NF-e',
	        autoHeight: true,
	        width: 833,
	        items: [l1Chave]
	    };
	    
	    
	    var c1Dest = {
	        layout: 'form',
	        border: false,
	        bodyStyle: 'padding: 5px',
	        items: [CnpjDestinatario]
	    };

	    var c2Dest = {
	        layout: 'form',
	        border: false,
	        bodyStyle: 'padding: 5px',
	        items: [NomeFantasiaDestinatario]
	    };

	    var c3Dest = {
	        layout: 'form',
	        border: false,
	        bodyStyle: 'padding: 5px',
	        items: [RazaoSocialDestinatario]
	    };

	    var c4Dest = {
	        layout: 'form',
	        border: false,
	        bodyStyle: 'padding: 5px',
	        items: [CepDestinatario]
	    };

	    var c5Dest = {
	        layout: 'form',
	        border: false,
	        bodyStyle: 'padding: 5px',
	        items: [LogradouroDestinatario]
	    };

	    var c6Dest = {
	        layout: 'form',
	        border: false,
	        bodyStyle: 'padding: 5px',
	        items: [NumeroDestinatario]
	    };

	    var c7Dest = {
	        layout: 'form',
	        border: false,
	        bodyStyle: 'padding: 5px',
	        items: [ComplementoDestinatario]
	    };

	    var c8Dest = {
	        layout: 'form',
	        border: false,
	        bodyStyle: 'padding: 5px',
	        items: [InscricaoEstadualDestinatario]
	    };

	    var c9Dest = {
	        layout: 'form',
	        border: false,
	        bodyStyle: 'padding: 5px',
	        items: [BairroDestinatario]
	    };

	    var c10Dest = {
	        layout: 'form',
	        border: false,
	        bodyStyle: 'padding: 5px',
	        items: [CodigoMunicipioDestinatario]
	    };

	    var c11Dest = {
	        layout: 'form',
	        border: false,
	        bodyStyle: 'padding: 5px',
	        items: [MunicipioDestinatario]
	    };

	    var c12Dest = {
	        layout: 'form',
	        border: false,
	        bodyStyle: 'padding: 5px',
	        items: [CodigoPaisDestinatario]
	    };

	    var c13Dest = {
	        layout: 'form',
	        border: false,
	        bodyStyle: 'padding: 5px',
	        items: [PaisDestinatario]
	    };

	    var c14Dest = {
	        layout: 'form',
	        border: false,
	        bodyStyle: 'padding: 5px',
	        items: [UfDestinatario]
	    };

	    var c15Dest = {
	        layout: 'form',
	        border: false,
	        bodyStyle: 'padding: 5px',
	        items: [TelefoneDestinatario]
	    };
	    

	    var l1Dest = {
	        layout: 'column',
	        border: false,
	        items: [c1Dest, c2Dest]
	    };

	    var l2Dest = {
	        layout: 'column',
	        border: false,
	        items: [c3Dest, c4Dest]
	    };

	    var l3Dest = {
	        layout: 'column',
	        border: false,
	        items: [c5Dest, c6Dest]
	    };

	    var l4Dest = {
	        layout: 'column',
	        border: false,
	        items: [c7Dest, c8Dest]
	    };

	    var l5Dest = {
	        layout: 'column',
	        border: false,
	        items: [c9Dest, c10Dest, c11Dest]
	    };

	    var l6Dest = {
	        layout: 'column',
	        border: false,
	        items: [c15Dest, c12Dest, c13Dest, c14Dest]
	    };
	    

	    var fieldsetDestinatario = {
	        xtype: 'fieldset',
	        title: 'Destinatário',
	        autoHeight: true,
	        width: 420,
	        items: [l1Dest, l2Dest, l3Dest, l4Dest, l5Dest, l6Dest]
	    };


	    var c1Emit = {
	        layout: 'form',
	        border: false,
	        bodyStyle: 'padding: 5px',
	        items: [CnpjEmitente]
	    };

	    var c2Emit = {
	        layout: 'form',
	        border: false,
	        bodyStyle: 'padding: 5px',
	        items: [NomeFantasiaEmitente]
	    };

	    var c3Emit = {
	        layout: 'form',
	        border: false,
	        bodyStyle: 'padding: 5px',
	        items: [RazaoSocialEmitente]
	    };

	    var c4Emit = {
	        layout: 'form',
	        border: false,
	        bodyStyle: 'padding: 5px',
	        items: [CepEmitente]
	    };

	    var c5Emit = {
	        layout: 'form',
	        border: false,
	        bodyStyle: 'padding: 5px',
	        items: [LogradouroEmitente]
	    };

	    var c6Emit = {
	        layout: 'form',
	        border: false,
	        bodyStyle: 'padding: 5px',
	        items: [NumeroEmitente]
	    };

	    var c7Emit = {
	        layout: 'form',
	        border: false,
	        bodyStyle: 'padding: 5px',
	        items: [ComplementoEmitente]
	    };

	    var c8Emit = {
	        layout: 'form',
	        border: false,
	        bodyStyle: 'padding: 5px',
	        items: [InscricaoEstadualEmitente]
	    };

	    var c9Emit = {
	        layout: 'form',
	        border: false,
	        bodyStyle: 'padding: 5px',
	        items: [BairroEmitente]
	    };

	    var c10Emit = {
	        layout: 'form',
	        border: false,
	        bodyStyle: 'padding: 5px',
	        items: [CodigoMunicipioEmitente]
	    };

	    var c11Emit = {
	        layout: 'form',
	        border: false,
	        bodyStyle: 'padding: 5px',
	        items: [MunicipioEmitente]
	    };

	    var c12Emit = {
	        layout: 'form',
	        border: false,
	        bodyStyle: 'padding: 5px',
	        items: [CodigoPaisEmitente]
	    };

	    var c13Emit = {
	        layout: 'form',
	        border: false,
	        bodyStyle: 'padding: 5px',
	        items: [PaisEmitente]
	    };

	    var c14Emit = {
	        layout: 'form',
	        border: false,
	        bodyStyle: 'padding: 5px',
	        items: [UfEmitente]
	    };

	    var c15Emit = {
	        layout: 'form',
	        border: false,
	        bodyStyle: 'padding: 5px',
	        items: [TelefoneEmitente]
	    };


	    var l1Emit = {
	        layout: 'column',
	        border: false,
	        items: [c1Emit, c2Emit]
	    };

	    var l2Emit = {
	        layout: 'column',
	        border: false,
	        items: [c3Emit, c4Emit]
	    };

	    var l3Emit = {
	        layout: 'column',
	        border: false,
	        items: [c5Emit, c6Emit]
	    };

	    var l4Emit = {
	        layout: 'column',
	        border: false,
	        items: [c7Emit, c8Emit]
	    };

	    var l5Emit = {
	        layout: 'column',
	        border: false,
	        items: [c9Emit, c10Emit, c11Emit]
	    };

	    var l6Emit = {
	        layout: 'column',
	        border: false,
	        items: [c15Emit, c12Emit, c13Emit, c14Emit]
	    };


	    var fieldsetEmitente = {
	        xtype: 'fieldset',
	        title: 'Emitente',
	        autoHeight: true,
	        width: 420,
	        items: [l1Emit, l2Emit, l3Emit, l4Emit, l5Emit, l6Emit]
	    };
	    

	    var container = {
	        border: false,
	        layout: 'column',
	        id: 'container',
	        name: 'container',
	        bodyStyle: 'padding: 10px',
	        labelAlign: 'top',
	        items: [fieldsetDestinatario, { xtype: 'label', html: '&nbsp;&nbsp;&nbsp;' }, fieldsetEmitente]
	    };

	    var container2 = {
	        border: false,
	        layout: 'column',
	        id: 'container2',
	        name: 'container2',
	        bodyStyle: 'padding: 10px',
	        items: [fieldsetChave]
	    };

	    //***************************** PAINEL *****************************//
	    var painel = new Ext.form.FormPanel({
	        id: 'painel',
	        name: 'painel',
	        autoHeight: true,
	        buttonAlign: 'center',
	        items: [container2, container],
	        buttons: [btnSalvar, btnCancelar]
	    });

	    //***************************** RENDER *****************************//
	    Ext.onReady(function () {
	        painel.render("divContent");
	        Cancelar();
	    });
	</script>

</asp:Content>
<asp:Content ID="MainContent" ContentPlaceHolderID="Content" runat="server">
	<div id="header-content">
		<h1>CTe - Manutenção de NFe</h1>
		<small>Você está em CTe > Manutenção de NFe</small>
		<br />
	</div>
	<div id="divContent">
	</div>
</asp:Content>
