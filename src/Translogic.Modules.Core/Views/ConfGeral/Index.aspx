﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Interna.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content runat="server" ID="HeadContent" ContentPlaceHolderID="HeadContent">
    <div id="header-content">
        <h1>
            Administração</h1>
        <small>Parametrização</small>
        <br />
    </div>
    <script type="text/javascript" language="javascript">
        var formModal = null;
        var grid = null;
        var posicaoRow = -1;

        /* region :: Functions */

        function Pesquisar() {
            grid.getStore().load();
        }

        function Limpar() {
            grid.getStore().removeAll();
            Ext.getCmp("filtros").getForm().reset();
        }

        function DeletarConfiguracao(id) {
            if(!confirm("Confirma exclusão deste registro?"))
                return;
            
            Ext.Ajax.request({
                    url: '<%= Url.Action("DeletarConfiguracao", "ConfGeral") %>',
                    method: "POST",
                    params: { chave: id },
                    success: function(response, options) {
                        var result = Ext.decode(response.responseText);
                        
                        if (result.Success == true) {
                            Pesquisar();

                            Ext.Msg.show({
                                title: 'Aviso',
                                msg: result.Message,
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.INFO
                            });
                        } else {
                            Ext.Msg.show({
                                title: 'Aviso',
                                msg: result.Message,
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.ERROR
                            });
                        }

                        Ext.getBody().unmask();
                    },
                    failure: function(response, options) {
                        Ext.Msg.show({
                            title: 'Aviso',
                            msg: response.Message,
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.ERROR
                        });

                        Ext.getBody().unmask();
                    }
                });
        }
        
        function AdicionarConfiguracao() {
            posicaoRow = 1;
            formModal = new Ext.Window({
                id: 'form-add-conf',
                title: 'Adicionar Configuração',
                modal: true,
                resizable: false,
                width: 400,
                height: 220,
                autoLoad:
                    {
                        url: '<%= Url.Action("AdicionarConfiguracao", "ConfGeral") %>',
                        scripts: true,
                        callback: function (el, success, c) {
                            posicaoRow = -1;
                            if (!success) {
                                 formModal.close();
                            }
                        }
                    },
                listeners: {
                    destroy: function () {
                        if (grid) {
                            grid.getStore().reload();
                        }
                    }
                }
            });

            formModal.show(this);
        }
        
        function EditarConfiguracao(id) {
            posicaoRow = 1;
            formModal = new Ext.Window({
                id: 'form-add-conf',
                title: 'Editar Configuração',
                modal: true,
                resizable: false,
                width: 400,
                height: 220,
                autoLoad:
                    {
                        url: '<%= Url.Action("EditarConfiguracao", "ConfGeral") %>',
                        params  : { id: id },
                        scripts: true,
                        callback: function (el, success, c) {
                            posicaoRow = -1;
                            if (!success) {
                                 formModal.close();
                            }
                        }
                    },
                listeners: {
                    destroy: function () {
                        if (grid) {
                            grid.getStore().reload();
                        }
                    }
                }
            });

            formModal.show(this);
        }
        
        function EditarConfiguracaoRender(id) {
            return "<img style=\"cursor: pointer; cursor: hand;\" title=\"Editar\" src=\"<%=Url.Images("Icons/email_edit.png") %>\" alt=\"Editar\" onclick=\"EditarConfiguracao('" + id + "')\">";
        }
        
        function DeletarConfiguracaoRenderer(id) {
            return "<img style=\"cursor: pointer; cursor: hand;\" title=\"Deletar\" src=\"<%=Url.Images("Icons/delete.png") %>\" alt=\"Deletar\" onclick=\"DeletarConfiguracao('" + id + "')\">";
        }

        function GerenciarRenderer(id) {
            var html = '';
            var espaco = '&nbsp';
            
            html += EditarConfiguracaoRender(id);
            //html += (espaco + DeletarConfiguracaoRenderer(id));

            return html;
        }

        /* endregion :: Functions */

        /* region :: Filtros */

        var txtChaveConfiguracao = {
            xtype: 'textfield',
            name: 'txtChaveConfiguracao',
            id: 'txtChaveConfiguracao',
            fieldLabel: 'Chave de configuração',
            autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '256' },
            style: 'text-transform: uppercase',
            width: 400
        };

        var arrChaveConfiguracao = {
            width: 400,
            layout: 'form',
            border: false,
            items: [txtChaveConfiguracao]
        };

        var arrlinha1 = {
            layout: 'column',
            border: false,
            items: [arrChaveConfiguracao]
        };

        var arrCampos = new Array();
        arrCampos.push(arrlinha1);

        var filtros = new Ext.form.FormPanel({
            id: 'filtros',
            title: 'Filtros',
            name: 'filtros',
            region: 'center',
            bodyStyle: 'padding: 15px',
            initEl: function(el) {
                this.el = Ext.get(el);
                this.id = this.el.id || Ext.id();
                if (this.standardSubmit) {
                    this.el.dom.action = this.url;
                } else {
                    this.el.on('submit', this.onSubmit, this);
                }
                this.el.addClass('x-form');
            },
            labelAlign: 'top',
            border: false,
            items: [arrCampos],
            buttonAlign: 'center',
            buttons: [
                {
                    name: 'btnPesquisar',
                    id: 'btnPesquisar',
                    text: 'Pesquisar',
                    iconCls: 'icon-find',
                    handler: Pesquisar
                },
                {
                    name: 'btnLimpar',
                    id: 'btnLimpar',
                    text: 'Limpar',
                    handler: Limpar
                }
            ]
        });

        /* endregion :: Filtros */

        /* region :: Grid */

        var cm = new Ext.grid.ColumnModel({
            defaults: {
                sortable: true
            },
            columns: [
                {
                    dataIndex: "Id", 
                    hidden: true
                },
                new Ext.grid.RowNumberer(),
                {
                    header: 'Ações',
                    dataIndex: 'Id',
                    width: 65,
                    align: "center",
                    sortable: false,
                    renderer: function (value, metaData, record, rowIndex, colIndex, store) {
                        return GerenciarRenderer(record.data.Id);
                    }
                },
                {
                    header: 'Chave',
                    dataIndex: 'Id',
                    width: 150
                },
                {
                    header: 'Valor',
                    dataIndex: 'Valor',
                    width: 200
                },
                {
                    header: 'Descrição',
                    dataIndex: 'Descricao',
                    width: 300
                },
                {
                    header: 'Data',
                    dataIndex: 'Data',
                    width: 100
                }
            ]
        });
        
        var btnAdicionarConfiguracoes = new Ext.Button({
            id: 'btnAdicionarConfiguracoes',
            name: 'btnAdicionarConfiguracoes',
            text: 'Adicionar Configuração',
            iconCls: 'icon-new',
            handler: AdicionarConfiguracao
        });

        grid = new Translogic.PaginatedGrid({
            id: 'grid',
            name: 'grid',
            autoLoadGrid: true,
            region: 'center',
            viewConfig: {
                emptyText: 'Não possui dado(s) para exibição.'
            },
            filteringToolbar: [btnAdicionarConfiguracoes],
            loadMask: { msg: App.Resources.Web.Carregando },
            fields: [
                'Id',
                'Valor',
                'Descricao',
                'Data'
            ],
            cm: cm,
            stripeRows: true,
            url: '<%= Url.Action("ObterConfiguracoes", "ConfGeral") %>'
        });

        grid.getStore().proxy.on('beforeload', function (p, params) {
            params['chave'] = Ext.getCmp("txtChaveConfiguracao").getValue();
        });

        /* endregion :: Grid */

        /* region :: Render */
        
        new Ext.Viewport({
            layout: 'border',
            margins: 10,
            items: [{
                    region: 'north',
                    height: 200,
                    autoScroll: true,
                    items: [{
                            region: 'center',
                            applyTo: 'header-content'
                        },
                        filtros]
                },
                grid]
        });

        /* endregion :: Render */
        
    </script>
</asp:Content>