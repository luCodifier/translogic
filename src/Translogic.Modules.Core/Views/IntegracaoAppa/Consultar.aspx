﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Interna.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content runat="server" ID="HeadContent" ContentPlaceHolderID="HeadContent">
    <meta http-equiv="x-ua-compatible" content="IE=7" />
    <div id="header-content">
        <h1>
            Consulta de Envios da Integração APPA</h1>
        <br />
        <small>Consulta dos registros enviados à integração APPA</small>
        <br />
    </div>
    <script type="text/javascript" language="javascript">

        /* region :: Váriaveis */
        
        var gridResultados = null;

        var statusEnvioStore = new Ext.data.ArrayStore({
            id: 'statusEnvioStore',
            fields: [ 'ID', 'Text' ],
            data: [ [-1, 'Todos'], [0, 'Erros'], [1, 'Pendentes'], [2, 'Processadas'] ]
        });
        
        var statusRecebimentoStore = new Ext.data.ArrayStore({
            id: 'statusRecebimentoStore',
            fields: [ 'ID', 'Text' ],
            data: [ [-1, 'Todos'], [0, 'Erros'], [2, 'Processadas'] ]
        });

        /* endregion :: Váriaveis */

        /* region :: Functions */

        function Pesquisar() {
            gridResultados.getStore().load();
        }

        function Excel() {

            var dataInicial = Ext.getCmp("dataInicial").getValue();
            var dataFinal = Ext.getCmp("dataFinal").getValue();

            var ddlSituacaoEnvioStatus = Ext.getCmp("ddlSituacaoEnvioStatus");
            var situacaoEnvioStatus = ddlSituacaoEnvioStatus.getValue();

            var ddlSituacaoRecebimentoStatus = Ext.getCmp("ddlSituacaoRecebimentoStatus");
            var situacaoRecebimentoStatus = ddlSituacaoRecebimentoStatus.getValue();

            var listaVagoes = '';
            if (storeVagoes.getCount() != 0) {
                storeVagoes.each(
                    function (record) {
                        if (!record.data.Erro) {
                            listaVagoes += record.data.Vagao + ";";
                        }
                    }
                );
            } else {
                listaVagoes = Ext.getCmp("txtVagoes").getValue();
            }

            url = "<%= Url.Action("ExportarExcel") %>";

            url += String.format("?dataInicial={0}", dataInicial.format('d/m/Y'));
            url += String.format("&dataFinal={0}", dataFinal.format('d/m/Y'));
            url += String.format("&origem={0}", Ext.getCmp("txtOrigem").getValue());
            url += String.format("&destino={0}", Ext.getCmp("txtDestino").getValue());
            url += String.format("&vagoes={0}", listaVagoes);
            url += String.format("&situacaoEnvioStatus={0}", situacaoEnvioStatus);
            url += String.format("&situacaoRecebimentoStatus={0}", situacaoRecebimentoStatus);
            url += String.format("&lote={0}", Ext.getCmp("txtLote").getValue());

            window.open(url, "");

        }


        function Limpar() {
            gridResultados.getStore().removeAll();
            gridVagoes.getStore().removeAll();
            Ext.getCmp("filtros").getForm().reset();
        }

        function Reprocessar(id) {
            Ext.getBody().mask("Salvando as alterações.", "x-mask-loading");
            
            Ext.Ajax.request({
                url: "<%= Url.Action("ReprocessarRegistro") %>",
                method: "POST",
                params: { id: id },
                success: function(response) {
                    var result = Ext.decode(response.responseText);
                    if (result.Success === true) {
                        Ext.Msg.show({
                            title: "Mensagem de Informação",
                            msg: "Registro enviado para processamento novamente. Aguarde uns instantes para obter a resposta da integração.",
                            buttons: Ext.Msg.OK,
                            minWidth: 200
                        });
                        
                        Pesquisar();
                    } else {
                        Ext.Msg.show({
                            title: "Mensagem de Erro",
                            msg: result.Message,
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.ERROR,
                            minWidth: 200
                        });
                    }
                    
                    Ext.getBody().unmask();
                }
            });
        }
        
        function ObterXmlEnvio(id) {
            Ext.Msg.show({
                title: 'Atenção',
                msg: 'Deseja visualizar o XML de envio?',
                buttons: Ext.Msg.YESNO,
                fn: function (btn) {
                    if (btn == 'yes') {
                        var url = "<%= Url.Action("ObterXmlEnvio") %>?ids=" + id;
                        window.open(url, "");
                    }
                },
                icon: Ext.MessageBox.QUESTION
            });
        }

        function ActionsRenderer(id, adicionaXml, sucessoRetorno) {

            var xmlAction = '';
            var espaco = '';
            if (adicionaXml === true) {
                xmlAction = XmlRenderer(id);
                espaco = '&nbsp';
            }
            else {
                xmlAction = '';
            }

            var reprocessarAction = '';
            if (sucessoRetorno === false) {
                
                reprocessarAction = ReprocessarRenderer(id);
            }
            else {
                espaco = '';
            }
            
            var actions = xmlAction + espaco + reprocessarAction;
            return actions;
        }
        
        function XmlRenderer(id) {
            return "<img id='img-xml' style='cursor: pointer; cursor: hand;' title='Obter XML' src='<%=Url.Images("Icons/xml.png") %>' alt='XML' onclick='ObterXmlEnvio(" + id + ")'>";
        }
        
        function ReprocessarRenderer(id) {
            return "<img id='img-reprocessar' style='cursor: pointer; cursor: hand;' title='Reprocessar' src='<%=Url.Images("Icons/arrow_redo.png") %>' alt='Reprocessar' onclick='Reprocessar(" + id + ")'>";
        }

        function StatusRenderer(status) {
            if (status == 0 || status == "0") {
                return "<img src='/Get.efh?file={Translogic.Modules.Core;}/circle-red.png' alt='Erro'>";
            } else if (status == 1 || status == "1") {
                return "<img src='/Get.efh?file={Translogic.Modules.Core;}/circle-yellow.png' alt='Pendente de Processamento'>";
            } else {
                return "<img src='/Get.efh?file={Translogic.Modules.Core;}/circle-green.png' alt='Processada'>";
            }
        }

        /* endregion Functions */

        /* region :: Filtros */

        var dataInicial = {
            xtype: 'datefield',
            fieldLabel: 'Data Inicial',
            id: 'dataInicial',
            name: 'dataInicial',
            width: 83,
            allowBlank: false,
            value: new Date()
        };

        var arrDataInicial = {
            width: 87,
            layout: 'form',
            border: false,
            items: [dataInicial]
        };

        var dataFinal = {
            xtype: 'datefield',
            fieldLabel: 'Data Final',
            id: 'dataFinal',
            name: 'dataFinal',
            width: 83,
            allowBlank: false,
            value: new Date()
        };

        var arrDataFinal = {
            width: 87,
            layout: 'form',
            border: false,
            items: [dataFinal]
        };
        
        var txtLote = {
            xtype: 'textfield',
            name: 'txtLote',
            id: 'txtLote',
            fieldLabel: 'Lote',
            autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '5' },
            maskRe: /[0-9]/,
            width: 50
        };
        
        var arrLote = {
            width: 55,
            layout: 'form',
            border: false,
            items: [txtLote]
        };

        var txtOrigem = {
            xtype: 'textfield',
            name: 'txtOrigem',
            id: 'txtOrigem',
            fieldLabel: 'Origem',
            autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '3' },
            maskRe: /[a-zA-Z]/,
            style: 'text-transform: uppercase',
            width: 50
        };

        var arrOrigem = {
            width: 55,
            layout: 'form',
            border: false,
            items: [txtOrigem]
        };
        
        var txtDestino = {
            xtype: 'textfield',
            name: 'txtDestino',
            id: 'txtDestino',
            fieldLabel: 'Destino',
            autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '3' },
            maskRe: /[a-zA-Z]/,
            style: 'text-transform: uppercase',
            width: 50
        };
        
        var arrDestino = {
            width: 55,
            layout: 'form',
            border: false,
            items: [txtDestino]
        };
        
        var txtVagoes = {
            xtype: 'textfield',
            name: 'txtVagoes',
            id: 'txtVagoes',
            fieldLabel: 'Vagão',
            autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '7' },
            //maxLength: 500,
            maskRe: /[a-zA-Z0-9\;]/,
            width: 80,
            enableKeyEvents: true,
            listeners: {
                specialkey: function (f, e) {
                    if (e.getKey() == e.ENTER) {
                        Pesquisar();
                    }
                }
            }
        };

        var btnLoteVagao = {
            text: 'Informar Vagões',
            xtype: 'button',
            iconCls: 'icon-find',
            handler: function (b, e) {
                windowStatusVagoes = new Ext.Window({
                    id: 'windowStatusVagoes',
                    title: 'Informar vagões',
                    modal: true,
                    width: 855,
                    resizable: false,
                    height: 380,
                    autoLoad: {
                        url: '<%= Url.Action("FormInformarVagoes") %>',
                        scripts: true
                    }
                });

                 windowStatusVagoes.show();
             }
         };
        
        var arrVagoes = {
            width: 85,
            layout: 'form',
            border: false,
            items: [txtVagoes]
        };

        var arrBtnLoteVagoes = {
            width: 120,
            layout: 'form',
            style: 'padding: 17px 0px 0px 0px;',
            border: false,
            items: [btnLoteVagao]
        };

        
        var ddlSituacaoEnvioStatus = new Ext.form.ComboBox({
            id: 'ddlSituacaoEnvioStatus',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            store: statusEnvioStore,
            valueField: 'ID',
            displayField: 'Text',
            value: -1,
            width: 110,
            fieldLabel: 'Situação Envio'
        });
        
        var arrSituacaoEnvioStatus = {
            layout: 'form',
            border: false,
            width: 120,
            items: [ddlSituacaoEnvioStatus]
        };
        
        var ddlSituacaoRecebimentoStatus = new Ext.form.ComboBox({
            id: 'ddlSituacaoRecebimentoStatus',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            store: statusRecebimentoStore,
            valueField: 'ID',
            displayField: 'Text',
            value: -1,
            width: 110,
            fieldLabel: 'Situação Recebimento'
        });
        
        var arrSituacaoRecebimentoStatus = {
            layout: 'form',
            border: false,
            width: 120,
            items: [ddlSituacaoRecebimentoStatus]
        };

        var btnPesquisar = {
            xtype: "button",
            name: 'btnPesquisar',
            id: 'btnPesquisar',
            text: 'Pesquisar',
            fieldLabel: '&nbsp;',
            labelSeparator: ' ',
            iconCls: 'icon-find',
            handler: Pesquisar,
            width: 70
        };

        var arrBtnPesquisar = {
            width: 110,
            layout: 'form',
            border: false,
            items: [btnPesquisar]
        };

        var btnLimpar = {
            xtype: "button",
            name: 'btnLimpar',
            id: 'btnLimpar',
            text: 'Limpar',
            fieldLabel: '&nbsp;',
            labelSeparator: ' ',
            width: 55,
            handler: Limpar
        };

        var arrBtnLimpar = {
            width: 65,
            layout: 'form',
            border: false,
            items: [btnLimpar]
        };

        var btnExcel = {
            xtype: "button",
            name: 'btnExcel',
            id: 'btnExcel',
            text: 'Exportar',
            fieldLabel: '&nbsp;',
            labelSeparator: ' ',
            iconCls: 'icon-page-excel',
            width: 55,
            handler: Excel
        };

        var arrbtnExportar = {
            width: 100,
            layout: 'form',
            border: false,
            items: [btnExcel]
        };


        var arrlinha1 = {
            layout: 'column',
            border: false,
            width: 1250,
            items: [arrDataInicial, arrDataFinal, arrLote, arrOrigem, arrDestino, arrVagoes, arrBtnLoteVagoes, arrSituacaoEnvioStatus, arrSituacaoRecebimentoStatus, arrBtnPesquisar, arrBtnLimpar, arrbtnExportar]
        };


        /* endregion :: Filtros */

        /* region :: Grid */

        var gridResultadosCheckboxSel = new Ext.grid.CheckboxSelectionModel({
            singleSelect: false,
            renderer: function (value, metadata, record) {

                if ((record.data !== undefined || record.data !== null) && record.data.SucessoRetorno != 0 && record.data.SucessoRetorno != '0') {
                    this.lock();
                    return '';
                }
                else {
                    return '<div class="x-grid3-row-checker">&#160;</div>';
                }
            }
        });

        var cm = new Ext.grid.ColumnModel({
            defaults: {
                sortable: true
            },
            columns: [
                gridResultadosCheckboxSel,
                {
                    dataIndex: "Id", hidden: true
                },
                {
                    dataIndex: "PossuiXml", hidden: true
                },
                {
                    header: 'Reprocessar',
                    dataIndex: 'Id',
                    width: 15,
                    align: "center",
                    sortable: false,
                    renderer: function(value, metaData, record, rowIndex, colIndex, store) {
                        var sucessoRetorno = false;
                        if (record.data !== undefined || record.data !== null) {
                            sucessoRetorno = record.data.SucessoRetorno != 0 && record.data.SucessoRetorno != '0';
                        }
                        
                        return ActionsRenderer(record.data.Id, null, sucessoRetorno);
                    }
                },
                {
                    header: 'Download',
                    dataIndex: 'Id',
                    width: 10,
                    align: "center",
                    sortable: false,
                    renderer: function (value, metaData, record, rowIndex, colIndex, store) {
                        var sucessoRetorno = false;
                        if (record.data !== undefined || record.data !== null) {
                            sucessoRetorno = record.data.SucessoRetorno != 0 && record.data.SucessoRetorno != '0';
                        }

                        return ActionsRenderer(record.data.Id, record.data.PossuiXml, null);
                    }
                },
                {
                    header: 'Lote',
                    dataIndex: 'Lote',
                    width: 5,
                    align: "center"
                },
                {
                    header: 'Data Geração',
                    dataIndex: 'DataGeracao',
                    width: 20
                },
                {
                    header: 'Status Envio',
                    dataIndex: 'SucessoEnvio',
                    width: 10,
                    align: "center",
                    sortable: false,
                    renderer: StatusRenderer
                },
                {
                    header: 'Data Envio',
                    dataIndex: 'DataEnvio',
                    width: 20
                },
                {
                    header: 'Status Receb',
                    dataIndex: 'SucessoRetorno',
                    width: 15,
                    align: "center",
                    sortable: false,
                    renderer: StatusRenderer
                },
                {
                    header: 'Vagão',
                    dataIndex: 'Vagao',
                    align: "center",
                    width: 18
                },
                {
                    header: 'Fluxo',
                    dataIndex: 'FluxoComercial',
                    align: "center",
                    width: 10
                },
                {
                    header: 'Origem',
                    dataIndex: 'Origem',
                    align: "center",
                    width: 8
                },
                {
                    header: 'Destino',
                    dataIndex: 'Destino',
                    align: "center",
                    width: 8
                },
                {
                    header: 'Prev. Chegada',
                    dataIndex: 'DataPrevistaChegada',
                    align: "center",
                    width: 17
                },
                {
                    header: 'Mensagem',
                    dataIndex: 'MensagemRetorno',
                    align: "left",
                    width: 50
                }
            ]
        });

        var storeVagoes = new Ext.data.JsonStore({
            remoteSort: true,
            root: "Items",
            fields: [
                'Vagao',
                'Erro',
                'Mensagem'
            ]
        });



        gridResultados = new Translogic.PaginatedGrid({
            id: 'gridResultados',
            name: 'gridResultados',
            cm: cm,
            sm: gridResultadosCheckboxSel,
            autoLoadGrid: false,
            region: 'center',
            viewConfig: {
                forceFit: true,
                emptyText: 'Não possui dado(s) para exibição.'
            },
            loadMask: { msg: App.Resources.Web.Carregando },
            fields: [
                'Id',
                'Lote',
                'PossuiXml',
                'DataGeracao',
                'DataEnvio',
                'SucessoEnvio',
                'SucessoRetorno',
                'Vagao',
                'FluxoComercial',
                'Origem',
                'Destino',
                'DataPrevistaChegada',
                'MensagemRetorno'
            ],
            stripeRows: true,
            url: '<%= Url.Action("ObterIntegracoesConsulta", "IntegracaoAppa") %>',
            filteringToolbar: {
                items: [
                    {
                        id: 'ReprocessarSelecionados',
                        text: 'Reprocessar Selecionados',
                        cls: 'x-btn-text-icon',
                        icon: '<%=Url.Images("Icons/arrow_redo.png") %>',
                        handler: function () {
                            var allSelected = gridResultadosCheckboxSel.getSelections();
                            
                            selectedIds = [];
                            Ext.each(allSelected, function (item) {
                                selectedIds.push(item.data.Id);
                            });

                            var postData = { values: selectedIds };

                            Ext.getBody().mask("Salvando as alterações.", "x-mask-loading");

                            Ext.Ajax.request({
                                url: "<%= Url.Action("ReprocessarRegistros") %>",
                                method: "POST",
                                params: { itens: selectedIds },
                                success: function (response) {
                                    var result = Ext.decode(response.responseText);
                                    if (result.Success === true) {
                                        Ext.Msg.show({
                                            title: "Mensagem de Informação",
                                            msg: "Resultado do processamento: Aguarde uns instantes para obter a resposta da integração.",
                                            buttons: Ext.Msg.OK,
                                            minWidth: 200
                                        });

                                        Pesquisar();
                                    } else {
                                        Ext.Msg.show({
                                            title: "Mensagem de Erro",
                                            msg: result.Message,
                                            buttons: Ext.Msg.OK,
                                            icon: Ext.MessageBox.ERROR,
                                            minWidth: 200
                                        });
                                    }

                                    Ext.getBody().unmask();
                                }
                            });

                        }
                    }
                ]
            }
        });

        gridResultados.getStore().proxy.on('beforeload', function (p, params) {
            var dataInicial = Ext.getCmp("dataInicial").getValue();
            var dataFinal = Ext.getCmp("dataFinal").getValue();
            
            var ddlSituacaoEnvioStatus = Ext.getCmp("ddlSituacaoEnvioStatus");
            var situacaoEnvioStatus = ddlSituacaoEnvioStatus.getValue();

            var ddlSituacaoRecebimentoStatus = Ext.getCmp("ddlSituacaoRecebimentoStatus");
            var situacaoRecebimentoStatus = ddlSituacaoRecebimentoStatus.getValue();

            var listaVagoes = '';
            if (storeVagoes.getCount() != 0) {
                storeVagoes.each(
                    function (record) {
                        if (!record.data.Erro) {
                            listaVagoes += record.data.Vagao + ";";
                        }
                    }
                );
            } else {
                listaVagoes = Ext.getCmp("txtVagoes").getValue();
            }


            params['dataInicial'] = dataInicial.format('d/m/Y');
            params['dataFinal'] = dataFinal.format('d/m/Y');
            params['origem'] = Ext.getCmp("txtOrigem").getValue();
            params['destino'] = Ext.getCmp("txtDestino").getValue();
            params['vagoes'] = listaVagoes;
            params['situacaoEnvioStatus'] = situacaoEnvioStatus;
            params['situacaoRecebimentoStatus'] = situacaoRecebimentoStatus;
            params['lote'] = Ext.getCmp("txtLote").getValue();
        });

        var arrCampos = new Array();
        arrCampos.push(arrlinha1);
        //arrCampos.push(gridResultados);

        var filtros = new Ext.form.FormPanel({
            id: 'filtros',
            title: 'Filtros',
            name: 'filtros',
            region: 'center',
            bodyStyle: 'padding: 15px',
            initEl: function (el) {
                this.el = Ext.get(el);
                this.id = this.el.id || Ext.id();
                if (this.standardSubmit) {
                    this.el.dom.action = this.url;
                } else {
                    this.el.on('submit', this.onSubmit, this);
                }
                this.el.addClass('x-form');
            },
            labelAlign: 'top',
            border: false,
            items: [arrCampos]
        });

        /* endregion :: Grid */
        new Ext.Viewport({
            layout: 'border',
            margins: 10,
            items: [
			{
			    region: 'north',
			    height: 150,
			    autoScroll: true,
			    items: [{
			        region: 'center',
			        applyTo: 'header-content'
			    },
				filtros]
			},
			gridResultados
		]
        });
    </script>
</asp:Content>
