﻿<%@ Page Title="Cadastro de atrasos" Language="C#" MasterPageFile="~/Views/Shared/Interna.Master"
    Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">

        var formModal = null;
        var grid = null;
        var viewPort = null;
        var posicaoRow = -1;

        function FormSuccess(form, action) {
            formModal.close();
        }

        function FormError(form, action) {
            Ext.Msg.alert('Ops...', action.result.Message);
        }

        function onAdd() {
            posicaoRow = -1;
            formModal = new Ext.Window({
                id: 'formDetalhe',
                title: 'Novo Atraso',
                modal: true,
                resizable: false,
                width: 340,
                height: 435, //373
                autoLoad:
                {
                    url: '<%= Url.Action("Edit") %>',
                    scripts: true,
                    callback: function (el, success, c) {
                        posicaoRow = -1;
                        if (!success) { formModal.close(); }
                    }
                },
                listeners: {
                    destroy: function () {
                        if (grid) {
                            grid.getStore().reload();
                        }
                    }
                }
            });
            //window.wFormEdicao = formModal;
            formModal.show(this);
        }

        function onEdit(id, idLinha) {
            formModal = new Ext.Window({
                id: 'formDetalhe',
                title: 'Detalhe Atraso',
                modal: true,
                resizable: false,
                width: 340,
                height: 435, //373
                autoLoad:
			        {
			            url: '<%= Url.Action("Edit") %>',
			            scripts: true,
			            params: { Id: id, IdLinha: idLinha },
			            callback: function (el, success, c) {
			                if (!success) { formModal.close(); }
			            }
			        }
			    });
            
            formModal.show(this);
        }
        /* ------------------------------------------------------- */

        function tooltip(value, metaData) {
            metaData.attr = 'ext:qtip="' + value + '"';
            return value;
        }

        $(function () {
            var detalheAction = new Ext.ux.grid.RowActions({
                dataIndex: '',
                header: '',
                align: 'center',
                actions:
				[{
				    iconCls: "icon-detail",
				    tooltip: "Detalhes"
				}],
                callbacks: {
                    'icon-details': function (grid, record, action, row, col) {
                        onEdit(record.data.Id);
                    }
                }
            });

            grid = new Translogic.PaginatedGrid({
                name: 'grid',
                id: 'grid',
                autoLoad: false,
                region: 'center',
                url: '<%= Url.Action("ObterAtrasos") %>',
                //                viewConfig: {
                //                    forceFit: true
                //                },
                fields:
			[
					'Id',
					'QtdeVagoes',
			        'Vagoes',
					'Cliente',
					'Motivo',
			        'Estacao',
			        'Mercadoria',
					'TU',
					'Data',
					'TempoEmHrs',
					'Responsavel',
					'Obs',
			        'Tipo'
			],
                columns: [
				new Ext.grid.RowNumberer(),
				detalheAction,
				{ Id: 'Id', hidden: true },
				{ header: 'Tipo', dataIndex: "Tipo", sortable: true, renderer: tooltip },
				{ header: 'Motivo', dataIndex: "Motivo", sortable: true, renderer: tooltip },
                { header: 'Estação', dataIndex: "Estacao", sortable: true, renderer: tooltip },
                { header: 'Mercadoria', dataIndex: "Mercadoria", sortable: true, renderer: tooltip },
				{ header: 'Qtd. Vagões', dataIndex: "QtdeVagoes", sortable: true, renderer: tooltip },
                { header: 'Vagões', dataIndex: "Vagoes", sortable: true, renderer: tooltip },
				{ header: 'Cliente', dataIndex: "Cliente", sortable: true, renderer: tooltip },
				{ header: 'TU', dataIndex: "TU", sortable: true, renderer: tooltip },
				{ header: 'Data', dataIndex: "Data", sortable: true, renderer: tooltip },
				{ header: 'Tempo (h)', dataIndex: "TempoEmHrs", sortable: true, renderer: tooltip },
				{ header: 'Responsável', dataIndex: "Responsavel", sortable: true, renderer: tooltip },
				{ header: 'Obs', dataIndex: "Obs", sortable: true, renderer: tooltip }
			],
                plugins: [detalheAction],
                filteringToolbar: [{
                    text: 'Novo',
                    tooltip: 'Adicionar novo registro',
                    iconCls: 'icon-new',
                    handler: onAdd
                }]
            });

            grid.on("rowdblclick", function (g, i, e) {
                var record = grid.getSelectionModel().getSelected();
                posicaoRow = i;
                onEdit(record.data.Id, i);
            });

            var dsTiposAtrasos = new Ext.data.JsonStore({
                autoLoad: true,
                url: '<%= Url.Action("ObterTipos") %>',
                fields: [
                            'Tipo',
                            'TipoLabel'
                        ],
                listeners: {
                    load: function (store, records) {
                        var rt = store.recordType;
                        store.insert(0, new rt({}));
                    }
                }
            });

            var dsMotivos = new Ext.data.JsonStore({
                autoLoad: true,
                url: '<%= Url.Action("ObterMotivos") %>',
                fields: [
                            'Motivo',
                            'MotivoLabel'
                        ],
                listeners: {
                    load: function (store, records) {
                        var rt = store.recordType;
                        store.insert(0, new rt({}));
                    }
                }
            });
            window.dsMotivos = dsMotivos;

            var dsResponsaveis = new Ext.data.JsonStore({
                autoLoad: true,
                url: '<%= Url.Action("ObterResponsaveis") %>',
                fields: [
                            'Responsavel',
                            'ResponsavelLabel'
                        ],
                listeners: {
                    load: function (store, records) {
                        var rt = store.recordType;
                        store.insert(0, new rt({}));
                    }
                }
            });

            var dsClientes = new window.Ext.data.JsonStore({
                id: 'dsClientes',
                name: 'dsClientes',
                root: 'Items',
                url: '<%=Url.Action("ListClientes", "CadastroAtrasos") %>',
                fields: [
                    'DescResumida'
                ],
                listeners: {
                    load: function (store, records) {
                        var rt = store.recordType;
                        store.insert(0, new rt({}));
                    }
                }
            });
            window.dsClientes = dsClientes;

            var dsMercadorias = new window.Ext.data.JsonStore({
                id: 'dsMercadorias',
                name: 'dsMercadorias',
                root: 'Items',
                url: '<%=Url.Action("ListMercadorias", "CadastroAtrasos") %>',
                fields: [
                    'CodigoNome'
                ],
                listeners: {
                    load: function (store, records) {
                        var rt = store.recordType;
                        store.insert(0, new rt({}));
                    }
                }
            });
            window.dsMercadorias = dsMercadorias;

            var dataAtual = new Date();

            var dataInicial = {
                xtype: 'datefield',
                fieldLabel: 'Data Inicial',
                id: 'filtro-data-inicial',
                name: 'dataInicial',
                width: 83,
                allowBlank: false,
                vtype: 'daterange',
                endDateField: 'filtro-data-final',
                hiddenName: 'dataInicial',
                value: dataAtual
            };

            //            var dataInicial = new Ext.form.DateField({
            //                fieldLabel: 'Data Inicial',
            //                id: 'filtro-data-inicial',
            //                name: 'dataInicial',
            //                width: 80,
            //                allowBlank: false,
            //                vtype: 'daterange'
            //            });

            var dataFinal = {
                xtype: 'datefield',
                fieldLabel: 'Data Final',
                id: 'filtro-data-final',
                name: 'dataFinal',
                width: 83,
                allowBlank: false,
                vtype: 'daterange',
                startDateField: 'filtro-data-inicial',
                hiddenName: 'dataFinal',
                value: dataAtual
            };
            //            var dataFinal = new Ext.form.DateField({
            //                fieldLabel: 'Data Final',
            //                id: 'filtro-data-final',
            //                name: 'dataFinal',
            //                width: 80,
            //                allowBlank: false,
            //                vtype: 'daterange'
            //            });

            window.dsTiposAtrasos = dsTiposAtrasos;
            var cboTipo = {
                xtype: 'combo',
                id: 'filtroTipo',
                name: 'filtroTipo',
                forceSelection: false,
                store: dsTiposAtrasos,
                triggerAction: 'all',
                mode: 'local',
                typeAhead: true,
                fieldLabel: 'Tipo',
                displayField: 'TipoLabel',
                width: 115,
                valueField: 'Tipo',
                emptyText: '-- TODOS --',
                editable: false//,
                //tpl: '<tpl for="."><div class="x-combo-list-item">{TipoLabel}&nbsp;</div></tpl>'
            };

            window.dsResponsaveis = dsResponsaveis;
            var cboResponsavel = {
                xtype: 'combo',
                id: 'filtroResponsavel',
                name: 'filtroResponsavel',
                forceSelection: false,
                store: dsResponsaveis,
                triggerAction: 'all',
                mode: 'local',
                typeAhead: true,
                fieldLabel: 'Responsável',
                displayField: 'ResponsavelLabel',
                width: 115,
                valueField: 'Responsavel',
                emptyText: '-- TODOS --',
                editable: false//,
                //tpl: '<tpl for="."><div class="x-combo-list-item">{ResponsavelLabel}&nbsp;</div></tpl>'
            };

            var arrDataIni = {
                width: 87,
                layout: 'form',
                border: false,
                items: [dataInicial]
            };

            var arrDataFim = {
                width: 87,
                layout: 'form',
                border: false,
                items: [dataFinal]
            };

            var arrTipo = {
                width: 117,
                layout: 'form',
                border: false,
                items: [cboTipo]
            };

            var arrResponsavel = {
                width: 117,
                layout: 'form',
                border: false,
                items: [cboResponsavel]
            };

            var arrlinha1 = {
                layout: 'column',
                border: false,
                items: [arrDataIni, arrDataFim, arrTipo, arrResponsavel]
            };
            var arrCampos = new Array();
            arrCampos.push(arrlinha1);

            var filters = new Ext.FormPanel({
                id: 'grid-filtros',
                title: "Filtros",
                region: 'center',
                bodyStyle: 'padding: 15px',
                labelAlign: 'top',
                items: [arrCampos],
                buttonAlign: "center",
                buttons:
			[{
			    text: 'Pesquisar',
			    type: 'submit',
			    iconCls: 'icon-find',
			    handler: function (b, e) {
			        if (filters.form.isValid()) {
			            grid.getStore().load();
			        }
			    }
			},
			{
			    text: 'Limpar',
			    handler: function (b, e) {
			        grid.overviewStore.removeAll();
			        Ext.getCmp("grid-filtros").getForm().reset();
			    },
			    scope: this
			}]
            });

            grid.getStore().proxy.on('beforeload', function (p, params) {
                var dataInicial2 = Ext.getCmp("filtro-data-inicial").getValue();
                var dataFinal2 = Ext.getCmp("filtro-data-final").getValue();
                var tipo = Ext.getCmp("filtroTipo").getValue();
                var resp = Ext.getCmp("filtroResponsavel").getValue();

                params['filter[0].Campo'] = 'dataInicial';
                params['filter[0].Valor'] = new Array(dataInicial2.format('d/m/Y') + " " + dataInicial2.format('H:i:s'));
                params['filter[0].FormaPesquisa'] = 'Start';

                params['filter[1].Campo'] = 'dataFinal';
                params['filter[1].Valor'] = new Array(dataFinal2.format('d/m/Y') + " " + dataFinal2.format('H:i:s'));
                params['filter[1].FormaPesquisa'] = 'Start';

                params['filter[2].Campo'] = 'tipo';
                params['filter[2].Valor'] = tipo;
                params['filter[2].FormaPesquisa'] = 'Start';

                params['filter[3].Campo'] = 'responsavel';
                params['filter[3].Valor'] = resp;
                params['filter[3].FormaPesquisa'] = 'Start';
            });

            viewPort = new Ext.Viewport({
                layout: 'border',
                margins: 10,
                items: [
				{
				    region: 'north',
				    height: 185,
				    items: [{
				        region: 'center',
				        applyTo: 'header-content'
				    }, filters]
				},
				grid,
				{
				    id: 'detailPanel',
				    region: 'south',
				    autoScroll: true,
				    bodyStyle: {
				        background: '#CCCCCC',
				        padding: '5px'
				    }
				}
			]
            });
        });

Ext.onReady(function () {
    //filters.render(document.body);
    //card.render(document.body);
    //footerPanel.render(document.body);
    Ext.getCmp("filtro-data-inicial").setValue('<%=DateTime.Now.AddDays(-1).ToString("dd/MM/yyyy") %>');
    Ext.getCmp("filtro-data-final").setValue('<%=DateTime.Now.AddDays(-1).ToString("dd/MM/yyyy") %>');
    Ext.getCmp("filtro-data-inicial").focus();
    dsTiposAtrasos.load();
    dsResponsaveis.load();
});
	
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div id="header-content">
        <h1>
            Operação</h1>
        <small>Você está em Operação > Atrasos</small>
        <br />
    </div>
</asp:Content>
