﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%@ Import namespace="Translogic.Modules.Core.Domain.Model.FluxosComerciais.Atrasos" %>

<div id="div-form-atraso"></div>
<script type="text/javascript">
    var itemStore = 0;

    function FormError(form, action)
    {
        var responseObj = Ext.decode(action.response.responseText);
        Ext.Msg.alert('Ops...', responseObj.msg);
    }
    
    function limpaCampos(){
        Ext.getCmp("txtEstacao").setValue("");
        Ext.getCmp("txtObs").setValue("");
        Ext.getCmp("txtQtdeVagoes").setValue("");
        Ext.getCmp("txtTU").setValue("");
        Ext.getCmp("txtVagoes").setValue("");
        Ext.getCmp("dataAtraso").setValue("");
        Ext.getCmp("txtTempo").setValue("");
        Ext.getCmp("cadCliente").setValue("");
        Ext.getCmp("cadMercadoria").setValue("");
        Ext.getCmp("cadMotivo").setValue("");
        Ext.getCmp("cadResponsavel").setValue("");
	}

    function onInsertEdit(itemStore) {
        if (itemStore >= 0) {
            //Ext.getCmp("Id").setValue(grid.getStore().getAt(itemStore).get('id'));
        	Ext.getCmp("txtEstacao").setValue(grid.getStore().getAt(itemStore).get('Estacao'));
        	Ext.getCmp("txtObs").setValue(grid.getStore().getAt(itemStore).get('Obs'));
        	Ext.getCmp("txtQtdeVagoes").setValue(grid.getStore().getAt(itemStore).get('QtdeVagoes'));
        	Ext.getCmp("txtTU").setValue(grid.getStore().getAt(itemStore).get('TU'));  
        	Ext.getCmp("txtVagoes").setValue(grid.getStore().getAt(itemStore).get('Vagoes'));  
        	Ext.getCmp("dataAtraso").setValue(grid.getStore().getAt(itemStore).get('Data'));
            Ext.getCmp("txtTempo").setValue(grid.getStore().getAt(itemStore).get('TempoEmHrs'));
            Ext.getCmp("cadCliente").setValue(grid.getStore().getAt(itemStore).get('Cliente'));
        }
    }

//    var hdnIdAtraso = {
//        xtype: 'hidden',
//        name: 'Id',
//        id: 'Id',
//        hiddenName: 'Id'
//    };

    var cboTipoCad = {
        xtype: 'combo',
        id: 'cadTipo',
        name: 'cadTipo',
        forceSelection: true,
        store: window.dsTiposAtrasos,
        triggerAction: 'all',
        mode: 'remote',
        typeAhead: true,
        fieldLabel: 'Tipo',
        displayField: 'TipoLabel',
        width: 200,
        valueField: 'Tipo',
        editable: false,
        tpl: '<tpl for="."><div class="x-combo-list-item">{TipoLabel}&nbsp;</div></tpl>',
        listeners: {
            select: function (thisCb, rec, idx) {
                if (rec.data.Tipo == "CRG") {
                    Ext.getCmp("ltVAGOES").hide();
                    Ext.getCmp("ltMercadoria").hide(); 
                    Ext.getCmp("ltTU").show();
                    Ext.getCmp("txtQtdeVagoes").enable();
                }
                if (rec.data.Tipo == "DSC") {
                    Ext.getCmp("ltVAGOES").show();
                    Ext.getCmp("ltMercadoria").show(); 
                    Ext.getCmp("ltTU").hide();
                    Ext.getCmp("txtQtdeVagoes").disable();
                }
                Ext.getCmp("txtQtdeVagoes").setValue('');
                Ext.getCmp("txtVagoes").setValue('');
                Ext.getCmp("txtTU").setValue('');
            }
        }
    };

    var cboClienteCad = {
        xtype: 'combo',
        id: 'cadCliente',
        name: 'cadCliente',
        forceSelection: true,
        store: window.dsClientes,
        triggerAction: 'all',
        mode: 'remote',
        typeAhead: true,
        minChars: 3,
        fieldLabel: 'Cliente',
        displayField: 'DescResumida',
        valueField: 'DescResumida',
        width: 200,
        editable: true,
        tpl: '<tpl for="."><div class="x-combo-list-item">{DescResumida}&nbsp;</div></tpl>'
    };
    
    var cboMercadoriaCad = {
        xtype: 'combo',
        id: 'cadMercadoria',
        name: 'cadMercadoria',
        forceSelection: true,
        store: window.dsMercadorias,
        triggerAction: 'all',
        mode: 'remote',
        typeAhead: true,
        minChars: 3,
        fieldLabel: 'Mercadoria',
        displayField: 'CodigoNome',
        valueField: 'CodigoNome',
        width: 200,
        editable: true,
        tpl: '<tpl for="."><div class="x-combo-list-item">{CodigoNome}&nbsp;</div></tpl>'
    };
    
    var cboMotivoCad = {
        xtype: 'combo',
        id: 'cadMotivo',
        name: 'cadMotivo',
        forceSelection: true,
        store: window.dsMotivos,
        triggerAction: 'all',
        mode: 'remote',
        typeAhead: true,
        fieldLabel: 'Motivo',
        displayField: 'MotivoLabel',
        width: 200,
        valueField: 'Motivo',
        editable: false,
        tpl: '<tpl for="."><div class="x-combo-list-item">{MotivoLabel}&nbsp;</div></tpl>'
    };

    var cboResponsavelCad = {
        xtype: 'combo',
        id: 'cadResponsavel',
        name: 'cadResponsavel',
        forceSelection: true,
        store: window.dsResponsaveis,
        triggerAction: 'all',
        mode: 'remote',
        typeAhead: true,
        fieldLabel: 'Responsável',
        displayField: 'ResponsavelLabel',
        width: 200,
        valueField: 'Responsavel',
        editable: false,
        tpl: '<tpl for="."><div class="x-combo-list-item">{ResponsavelLabel}&nbsp;</div></tpl>'
    };

    var txtTempo = {
        xtype: 'timefield',
        id: 'txtTempo',
        name: 'txtTempo',
        typeAhead: true,
        fieldLabel: 'Tempo (hrs)',
        width: 80,
        editable: false,
        format: 'H:i',
        increment: 15
    };

    var txtEstacao = {
        xtype: 'textfield',
        id: 'txtEstacao',
        name: 'txtEstacao',
        typeAhead: true,
        fieldLabel: 'Estação',
        width: 80,
        enableKeyEvents: true,
        style: {
            textTransform: 'uppercase'
        },
        listeners: {
            keypress: function(thisTxt, e) {
                var regEst = /[a-zA-Z]/;
                var charDig = String.fromCharCode(e.getCharCode());
                if (!regEst.test(charDig)) {
                    e.preventDefault();
                }
                else if (thisTxt.getValue().length >= 3) {
                    e.preventDefault();
                }
            }
        }
    };

    var txtQtdeVagoes = {
        xtype: 'numberfield',
        allowDecimals: false,
        id: 'txtQtdeVagoes',
        name: 'txtQtdeVagoes',
        typeAhead: true,
        fieldLabel: 'Qtde. Vagões',
        width: 80
    };

    var txtTU = {
        xtype: 'numberfield',
        id: 'txtTU',
        name: 'txtTU',
        typeAhead: true,
        fieldLabel: 'TU',
        decimalSeparator: ',',
        width: 80
    };

    var txtVagoes = {
        xtype: 'textfield',
        id: 'txtVagoes',
        name: 'txtVagoes',
        typeAhead: true,
        fieldLabel: 'Vagões',
        width: 200,
        enableKeyEvents: true,
        listeners: {
            keypress: function (thisTxt, e) {
                var charDig = String.fromCharCode(e.getCharCode());
                if (charDig == ',') {
                    var str = thisTxt.getValue();
                    if (str.length > 0) {
                        if (str[str.length - 1] == ',') {
                            e.preventDefault();
                        }
                    }
                }
                else if (isNaN(parseInt(charDig))) {
                    e.preventDefault();
                }
            },
            change: function (thisTxt, nValue, oValue) {
                var vagoes = nValue.split(',');
                var arrayVagoes = new Array();
                var regVagao = /^[0-9]{5,}$/;
                for (var i = 0, item; item = vagoes[i]; i++) {
                    var vg = item.trim();
                    if (regVagao.test(vg)) {
                        arrayVagoes.push(vg);
                    }
                }

                Ext.getCmp("txtQtdeVagoes").setValue(vagoes.length);
            }
        }
    };

    var txtObs = {
        xtype: 'textarea',
        id: 'txtObs',
        name: 'txtObs',
        typeAhead: true,
        fieldLabel: 'Observação',
        width: 200
    };

    var dataAtraso = {
        xtype: 'datefield',
        fieldLabel: 'Data',
        id: 'dataAtraso',
        name: 'dataAtraso',
        width: 83,
        allowBlank: false,
        hiddenName: 'dataAtraso'
    };

    var formAtrasos = new Ext.form.FormPanel
    ({
        id: 'formAtrasos',
        labelWidth: 80,
        width: 325,
        resizable: false,
        height: 405,
        bodyStyle: 'padding: 15px',
        items:
            [
                cboTipoCad,
                txtEstacao,
                cboMotivoCad,
                {
                    id: 'ltVAGOES',
                    layout: 'form',
                    border: false,
                    hidden: true,
                    items: [txtVagoes]
                },
                {
                    id: 'ltMercadoria',
                    layout: 'form',
                    border: false,
                    hidden: true,
                    items: [cboMercadoriaCad]
                },
                {
                    id: 'ltTU',
                    layout: 'form',
                    border: false,
                    hidden: true,
                    items: [txtTU]
                },
                txtQtdeVagoes,
                dataAtraso,
                txtTempo,
                cboClienteCad,
                cboResponsavelCad,
                txtObs
            ],
        buttonAlign: "center",
        buttons:
            [{
                text: "Salvar",
                id: "btnSalvar",
                iconCls: 'icon-save',
                handler: function () {
                    Ext.getCmp("btnSalvar").setDisabled(true);
                    var dtAtraso = Ext.getCmp("dataAtraso").getValue()? new Array(Ext.getCmp("dataAtraso").getValue().format('d/m/Y') + " " + Ext.getCmp("dataAtraso").getValue().format('H:i:s')): '';
                    window.jQuery.ajax({
                        url: '<%= Url.Action("Salvar") %>',
                        type: "POST",
                        data: {
                            id: <%= Request["ID"] ?? "null" %>,
                            tipo: Ext.getCmp("cadTipo").getValue(),
                            estacao: Ext.getCmp("txtEstacao").getValue(),
                            tu: Ext.getCmp("txtTU").getValue().toString().replace('.', ','),
                            vagoes: Ext.getCmp("txtVagoes").getValue(),
                            qtdeVagoes: Ext.getCmp("txtQtdeVagoes").getValue(),
                            data: dtAtraso,
                            horasAtraso: Ext.getCmp("txtTempo").getValue(),
                            cliente: Ext.getCmp("cadCliente").getValue(),
                            mercadoria: Ext.getCmp("cadMercadoria").getValue(),
                            motivo: Ext.getCmp("cadMotivo").getValue(),
                            responsavel: Ext.getCmp("cadResponsavel").getValue(),
                            observacao: Ext.getCmp("txtObs").getValue()
                        },
                        dataType: "json",
                        success: function (data) {
                            var iconCad = window.Ext.MessageBox.ERROR;
                            if (data.sucesso) {
                                iconCad = window.Ext.MessageBox.SUCCESS;
                            }else {
                                Ext.getCmp("btnSalvar").setDisabled(false);
                            }
                            window.Ext.Msg.show({
                                title: "Salvar Aviso de Atraso",
                                msg: data.message,
                                buttons: window.Ext.Msg.OK,
                                icon: iconCad,
                                minWidth: 200,
                                fn: function () {
                                    if (data.sucesso) {
                                        //window.wFormEdicao.close();
                                        Ext.getCmp("formDetalhe").close();
                                        limpaCampos();
                                        posicaoRow = -1;
                                    } else {
                                        Ext.getCmp("btnSalvar").setDisabled(false);
                                    }
                                },
                                close: function () {
                                    if (data.sucesso) {
                                        //window.wFormEdicao.close();
                                        Ext.getCmp("formDetalhe").close();
                                        limpaCampos();
                                        posicaoRow = -1;
                                    }
                                }
                            });

                        },
                        error: function (jqXHR, textStatus) {
                            window.Ext.Msg.show({
                                title: "Erro no Servidor",
                                msg: 'Ocorreu um erro no servidor, por favor, tente novamente mais tarde.',
                                buttons: window.Ext.Msg.OK,
                                icon: window.Ext.MessageBox.ERROR,
                                minWidth: 200
                            });
                        }
                    });
                }
            }]
    });
    
    Ext.onReady(function () {
        itemStore = posicaoRow;
        if (itemStore >= 0) {
            onInsertEdit(itemStore);

            window.dsTiposAtrasos.load({
                callback: function(records) {
                    if (itemStore >= 0) {
                        Ext.getCmp("cadTipo").setValue(grid.getStore().getAt(itemStore).get('Tipo'));

                        if (grid.getStore().getAt(itemStore).get('Tipo') == "Carga") {
                            Ext.getCmp("ltVAGOES").hide();
                            Ext.getCmp("ltMercadoria").hide(); 
                            Ext.getCmp("ltTU").show();
                            Ext.getCmp("txtQtdeVagoes").enable();
                        }
                        if (grid.getStore().getAt(itemStore).get('Tipo') == "Descarga") {
                            Ext.getCmp("ltVAGOES").show();
                            Ext.getCmp("ltMercadoria").show(); 
                            Ext.getCmp("ltTU").hide();
                            Ext.getCmp("txtQtdeVagoes").disable();
                        }
                    }
                }
            });

            window.dsResponsaveis.load({
                callback: function(records) {
                    if (itemStore >= 0) {
                        Ext.getCmp("cadResponsavel").setValue(grid.getStore().getAt(itemStore).get('Responsavel'));
                    }
                }
            });

            window.dsMotivos.load({
                callback: function(records) {
                    if (itemStore >= 0) {
                        Ext.getCmp("cadMotivo").setValue(grid.getStore().getAt(itemStore).get('Motivo'));
                    }
                }
            });

            window.dsClientes.load({
                callback: function(records) {
                    if (itemStore >= 0) {
                        Ext.getCmp("cadCliente").setValue(grid.getStore().getAt(itemStore).get('Cliente'));
                    }
                }
            });

            window.dsMercadorias.load({
                callback: function (records) {
                    if (itemStore >= 0) {
                        Ext.getCmp("cadMercadoria").setValue(grid.getStore().getAt(itemStore).get('Mercadoria'));
                    }
                }
            });
        } else {
            window.dsTiposAtrasos.load();
            window.dsResponsaveis.load();
            window.dsMotivos.load();
            //window.dsClientes.load();
            //window.dsMercadorias.load();
        }
    });

    formAtrasos.render("div-form-atraso");
</script>

