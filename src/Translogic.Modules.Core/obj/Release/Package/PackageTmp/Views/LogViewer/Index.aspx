﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Interna.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content runat="server" ID="HeadContent" ContentPlaceHolderID="HeadContent">

    <div id="header-content">
        <h1>Logs do Sistema</h1>
        <small>Logs do Sistema</small>
        <br />
    </div>

    <style>
        .x-grid3-row {
            /*height: 40px;*/
        }

        .x-grid3-cell-inner {
            overflow: hidden;
            padding: 2px 2px;
            white-space: pre-line;
            text-wrap: normal;
        }
    </style>

    <script type="text/javascript" language="javascript">
        var formModal = null;
        var grid = null;
        var posicaoRow = -1;

        $(document).ready(function () {
            // alert('1');
            $('.x-tbar-page-last').hide();
            $('.xtb-text').hide();
            $('.x-tbar-page-number').hide();
        });

        /* region :: Functions */

        function ValidarFiltros() {
            var dtI = Ext.getCmp("txtDataInicial").getRawValue();
            var dtF = Ext.getCmp("txtDataFinal").getRawValue();

            if (dtI == "" &&
                dtF == "") {
                Ext.Msg.show({
                    title: 'Aviso',
                    msg: 'Entrada obrigatória das datas inicial e final',
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.ERROR
                });
                return false;
            }

            //Valida data inicial é nula
            if (dtI == "") {
                Ext.Msg.show({
                    title: 'Aviso',
                    msg: 'A Data Inicial é obrigatória.',
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.ERROR
                });
                return false;
            }
            //Valida datas final maior que inicial
            if (dtI != "" && dtF != "") {
                if (!comparaDatas(dtI, dtF)) {
                    Ext.Msg.show({
                        title: 'Aviso',
                        msg: ' Data final deve ser superior a data inicial nos filtros.',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.ERROR
                    });
                    return false;
                }
            }
            return true;
        };

        function comparaDatas(dataIni, dataFim) {
            var arrDtIni = dataIni.split('/');
            var arrDtFim = dataFim.split('/');

            var dtIni = new Date(arrDtIni[2] + '/' + arrDtIni[1] + '/' + arrDtIni[0]);
            var dtFim = new Date(arrDtFim[2] + '/' + arrDtFim[1] + '/' + arrDtFim[0]);

            return dtIni <= dtFim;
        };

        function Pesquisar() {
            if (ValidarFiltros()) {
                grid.getStore().load({ params: { start: 0, limit: 50 } });
                //gridStore.load({ params: { start: 0, limit: 50 } });
            }
        }

        function Limpar() {
            grid.getStore().removeAll();
            Ext.getCmp("filtros").getForm().reset();
        }

        function DeletarConfiguracao(id) {
            if (!confirm("Confirma exclusão deste registro?"))
                return;

            Ext.Ajax.request({
                url: '<%= Url.Action("DeletarConfiguracao", "LogViewer") %>',
                method: "POST",
                params: { chave: id },
                success: function (response, options) {
                    var result = Ext.decode(response.responseText);

                    if (result.Success == true) {
                        Pesquisar();

                        Ext.Msg.show({
                            title: 'Aviso',
                            msg: result.Message,
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.INFO
                        });
                    } else {
                        Ext.Msg.show({
                            title: 'Aviso',
                            msg: result.Message,
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.ERROR
                        });
                    }

                    Ext.getBody().unmask();
                },
                failure: function (response, options) {
                    Ext.Msg.show({
                        title: 'Aviso',
                        msg: response.Message,
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.ERROR
                    });

                    Ext.getBody().unmask();
                }
            });
        }

        function AdicionarConfiguracao() {
            posicaoRow = 1;
            formModal = new Ext.Window({
                id: 'form-add-conf',
                title: 'Adicionar Configuração',
                modal: true,
                resizable: false,
                width: 400,
                height: 220,
                autoLoad:
                {
                    url: '<%= Url.Action("AdicionarConfiguracao", "LogViewer") %>',
                    scripts: true,
                    callback: function (el, success, c) {
                        posicaoRow = -1;
                        if (!success) {
                            formModal.close();
                        }
                    }
                },
                listeners: {
                    destroy: function () {
                        if (grid) {
                            grid.getStore().reload();
                        }
                    }
                }
            });

            formModal.show(this);
        }

        function EditarConfiguracao(id) {
            posicaoRow = 1;
            formModal = new Ext.Window({
                id: 'form-add-conf',
                title: 'Editar Configuração',
                modal: true,
                resizable: false,
                width: 400,
                height: 220,
                autoLoad:
                {
                    url: '<%= Url.Action("EditarConfiguracao", "LogViewer") %>',
                    params: { id: id },
                    scripts: true,
                    callback: function (el, success, c) {
                        posicaoRow = -1;
                        if (!success) {
                            formModal.close();
                        }
                    }
                },
                listeners: {
                    destroy: function () {
                        if (grid) {
                            grid.getStore().reload();
                        }
                    }
                }
            });

            formModal.show(this);
        }

        function EditarConfiguracaoRender(id) {
            return "<img style=\"cursor: pointer; cursor: hand;\" title=\"Editar\" src=\"<%=Url.Images("Icons/email_edit.png") %>\" alt=\"Editar\" onclick=\"EditarConfiguracao('" + id + "')\">";
        }

        function DeletarConfiguracaoRenderer(id) {
            return "<img style=\"cursor: pointer; cursor: hand;\" title=\"Deletar\" src=\"<%=Url.Images("Icons/delete.png") %>\" alt=\"Deletar\" onclick=\"DeletarConfiguracao('" + id + "')\">";
        }

        function GerenciarRenderer(id) {
            var html = '';
            var espaco = '&nbsp';

            html += EditarConfiguracaoRender(id);
            //html += (espaco + DeletarConfiguracaoRenderer(id));

            return html;
        }

        /* endregion :: Functions */

        /* region :: Filtros */

        var txtDataInicial = new Ext.form.DateField({
            fieldLabel: 'Data Inicial',
            id: 'txtDataInicial',
            name: 'txtDataInicial',
            dateFormat: 'd/n/Y',
            width: 110,
            plugins: [new Ext.ux.InputTextMask('99/99/9999', true)],
            value: new Date()
        });

        var txtDataFinal = new Ext.form.DateField({
            fieldLabel: 'Data Final',
            id: 'txtDataFinal',
            name: 'txtDataFinal',
            dateFormat: 'd/n/Y',
            width: 110,
            plugins: [new Ext.ux.InputTextMask('99/99/9999', true)],
            value: new Date()
        });

        var formDataInicial = {
            width: 120,
            layout: 'form',
            border: false,
            items: [txtDataInicial]
        };

        var formDataFinal = {
            width: 120,
            layout: 'form',
            border: false,
            items: [txtDataFinal]
        };

        var segsTipo = new Ext.data.ArrayStore({
            fields: ['tipo'],
            data: [['Debug'], ['Error'], ['Exception'], ['Info'], ['Message'], ['Trace'], ['Warning'], ['WebException']]
        });
        window.dsTipos = segsTipo;
        var cboTipo = {
            xtype: 'combo',
            id: 'cboTipo',
            name: 'cboTipo',
            forceSelection: true,
            store: segsTipo,
            triggerAction: 'all',
            mode: 'local',
            typeAhead: true,
            minChars: 3,
            fieldLabel: 'Tipo',
            displayField: 'tipo',
            valueField: 'tipo',
            width: 200,
            editable: true,
            tpl: '<tpl for="."><div class="x-combo-list-item">{tipo}&nbsp;</div></tpl>'
        };
        var arrTipo = {
            width: 215,
            layout: 'form',
            border: false,
            items: [cboTipo]
        };

        var sistStore = new Ext.data.ArrayStore({
            fields: ['sistema'],
            data: [['CAALL'], ['CteRunner'], ['DbTranslogic'], ['EdiRunnerAcompanhamentoTremProcessor'], ['EdiRunnerAcompanhamentoTremSender'], ['EdiRunnerNotaExpedicao'], ['EdiRunnerShippingNote'], ['EdiRunnerShippingNoteJ'], ['EdiSoaWebService'], ['EdiWebSite'], ['Indefinido'], ['JobRunner'], ['Testes'], ['Translogic'], ['TranslogicSoaWebService']]
        });
        window.dsSistemas = sistStore;
        var cboSistema = {
            xtype: 'combo',
            id: 'cboSistema',
            name: 'cboSistema',
            forceSelection: true,
            store: sistStore,
            triggerAction: 'all',
            mode: 'local',
            typeAhead: true,
            minChars: 3,
            fieldLabel: 'Sistema',
            displayField: 'sistema',
            valueField: 'sistema',
            width: 200,
            editable: true,
            tpl: '<tpl for="."><div class="x-combo-list-item">{sistema}&nbsp;</div></tpl>'
        };
        var arrSistema = {
            width: 215,
            layout: 'form',
            border: false,
            items: [cboSistema]
        };

        var txtMensagemConfiguracao = {
            xtype: 'textfield',
            name: 'txtMensagemConfiguracao',
            id: 'txtMensagemConfiguracao',
            fieldLabel: 'Mensagem',
            autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '256' },
            width: 300
        };
        var arrMensagemConfiguracao = {
            width: 315,
            layout: 'form',
            border: false,
            items: [txtMensagemConfiguracao]
        };

        var txtExceptionConfiguracao = {
            xtype: 'textfield',
            name: 'txtExceptionConfiguracao',
            id: 'txtExceptionConfiguracao',
            fieldLabel: 'Exception',
            autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '256' },
            width: 400
        };
        var arrExceptionConfiguracao = {
            width: 300,
            layout: 'form',
            border: false,
            items: [txtExceptionConfiguracao]
        };

        var arrlinha1 = {
            layout: 'column',
            border: false,
            items: [formDataInicial, formDataFinal, arrTipo, arrSistema, arrMensagemConfiguracao, arrExceptionConfiguracao]
        };

        var arrCampos = new Array();
        arrCampos.push(arrlinha1);

        var filtros = new Ext.form.FormPanel({
            id: 'filtros',
            title: 'Filtros',
            name: 'filtros',
            region: 'center',
            bodyStyle: 'padding: 15px',
            initEl: function (el) {
                this.el = Ext.get(el);
                this.id = this.el.id || Ext.id();
                if (this.standardSubmit) {
                    this.el.dom.action = this.url;
                } else {
                    this.el.on('submit', this.onSubmit, this);
                }
                this.el.addClass('x-form');
            },
            labelAlign: 'top',
            border: false,
            items: [arrCampos],
            buttonAlign: 'center',
            buttons: [
                {
                    name: 'btnPesquisar',
                    id: 'btnPesquisar',
                    text: 'Pesquisar',
                    iconCls: 'icon-find',
                    handler: Pesquisar
                },
                {
                    name: 'btnLimpar',
                    id: 'btnLimpar',
                    text: 'Limpar',
                    handler: Limpar
                }
            ]
        });

        /* endregion :: Filtros */

        /* region :: Grid */

        var cm = new Ext.grid.ColumnModel({
            defaults: {
                sortable: true
            },
            columns: [
                {
                    header: 'LogId',
                    dataIndex: 'LogId',
                    width: 80
                },
                //new Ext.grid.RowNumberer(),
                //{
                //    header: 'Ações',
                //    dataIndex: 'LogId',
                //    width: 65,
                //    align: "center",
                //    sortable: false,
                //    renderer: function (value, metaData, record, rowIndex, colIndex, store) {
                //        return GerenciarRenderer(record.data.Id);
                //    }
                //},
                {
                    header: 'Sistema',
                    dataIndex: 'SistemaNome',
                    width: 150
                },
                {
                    header: 'Tipo',
                    dataIndex: 'TipoNome',
                    width: 70
                },
                {
                    header: 'Data',
                    dataIndex: 'Data',
                    width: 100
                },
                {
                    header: 'Mensagem',
                    dataIndex: 'Mensagem',
                    width: 500
                },
                {
                    header: 'Exception',
                    dataIndex: 'Exception',
                    width: 300
                },
                {
                    header: 'Tag',
                    dataIndex: 'Tag',
                    // width: 400
                }
            ]
        });

        var btnAdicionarConfiguracoes = new Ext.Button({
            id: 'btnAdicionarConfiguracoes',
            name: 'btnAdicionarConfiguracoes',
            text: 'Adicionar Configuração',
            iconCls: 'icon-new',
            handler: AdicionarConfiguracao
        });

        grid = new Translogic.PaginatedGrid({
            id: 'grid',
            name: 'grid',
            autoLoadGrid: true,
            region: 'center',
            timeout: 300,
            viewConfig: {
                emptyText: 'Não possui dado(s) para exibição.'
            },
            filteringToolbar: [btnAdicionarConfiguracoes],
            loadMask: { msg: App.Resources.Web.Carregando },
            fields: [
                'LogId',
                'Data',
                'SistemaNome',
                'TipoNome',
                'Mensagem',
                'Tag',
                'Exception'
            ],
            cm: cm,
            stripeRows: true,
            url: '<%= Url.Action("Select", "LogViewer") %>'
        });

        grid.getStore().proxy.on('beforeload', function (p, params) {
            params['dataIni'] = Ext.getCmp("txtDataInicial").getValue();
            params['dataFim'] = Ext.getCmp("txtDataFinal").getValue();
            params['mensagem'] = Ext.getCmp("txtMensagemConfiguracao").getValue();
            params['exception'] = Ext.getCmp("txtExceptionConfiguracao").getValue();
            params['sistema'] = Ext.getCmp("cboSistema").getValue();
            params['tipo'] = Ext.getCmp("cboTipo").getValue();
            
        });

        /* endregion :: Grid */

        /* region :: Render */

        new Ext.Viewport({
            layout: 'border',
            margins: 10,
            items: [{
                region: 'north',
                height: 200,
                autoScroll: true,
                items: [{
                    region: 'center',
                    applyTo: 'header-content'
                },
                    filtros]
            },
                grid]
        });

        /* endregion :: Render */

    </script>
</asp:Content>
