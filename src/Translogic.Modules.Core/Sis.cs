﻿using Speed.Common;
using Speed.Data;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.ServiceModel;
using System.Transactions;
using System.Web;
using Translogic.Core.Spd;
using Translogic.Modules.Core.Spd.BLL;
using Translogic.Modules.Core.Spd.Data;
using Translogic.Modules.Core.Spd.Data.Enums;

/// <summary>
/// Classe pai do Sistema
/// Propositalmente sem namespace, senão eu teria que incluir using em trocentos arquivos já existentes
/// </summary>
public class Sis
{

    #region Properties

    /// <summary>
    /// Sistema
    /// </summary>
    public static EnumSistema Sistema { get; set; }

    /// <summary>
    /// Usuário rodando o sistema
    /// </summary>
    public static string Usuario { get; set; }

    #endregion Properties

    #region Constructor

    static Sis()
    {
        // Inicializa aqui, pq se um sistema não chamar o Inicializar, não vai gravar log
        Inicializar(EnumSistema.Indefinido, "Sys");
    }

    #endregion Constructor

    #region Methods

    /// <summary>
    /// Inicializa o sistema
    /// </summary>
    /// <param name="sistema"></param>
    /// <param name="usuario"></param>
    public static void Inicializar(EnumSistema sistema, string usuario)
    {
        Sistema = sistema;
        Usuario = usuario;

        // Sempre usa o translogic como connection default do speed
        Sys.ConnectionString = Dbs.ConnectionString(Db.Translogic);
        Sys.ProviderType = EnumDbProviderType.Oracle;

        // Cadastra os valores de EnumSistema que ainda nao estão na base de dados
        var sistemas = BL_SisSistema.Select().ToDictionary(p => p.SistemaId);
        foreach (EnumSistema sis in Enum.GetValues(typeof(EnumSistema)))
        {
            var id = (decimal)sis;
            SisSistema rec = sistemas.GetValue(id);
            if (rec == null)
            {
                rec = new SisSistema { SistemaId = id, SistemaNome = sis.ToString(), Ativo = 1 };
                BL_SisSistema.Insert(rec);
            }
        }

        LogTrace("Iniciando sistema: " + Sistema);

        // Cadastra os valores de Tipos de Logs que ainda nao estão na base de dados
        var logTipos = BL_SisLogTipo.Select().ToDictionary(p => p.TipoId);
        foreach (EnumLogTipo tipo in Enum.GetValues(typeof(EnumLogTipo)))
        {
            var id = (short)tipo;
            SisLogTipo rec = logTipos.GetValue(id);
            if (rec == null)
            {
                rec = new SisLogTipo { TipoId = id, TipoNome = tipo.ToString() };
                BL_SisLogTipo.Insert(rec);
            }
        }
    }

    public static void ChecarConexao()
    {
        if (Sys.ProviderType != EnumDbProviderType.Oracle || string.IsNullOrEmpty(Sys.ConnectionString))
        {
            Sistema = EnumSistema.Translogic;
            Usuario = "Web";
            // Sempre usa o translogic como connection default do speed
            Sys.ConnectionString = Dbs.ConnectionString(Db.Translogic);
            Sys.ProviderType = EnumDbProviderType.Oracle;
        }
    }

    /// <summary>
    /// Verifica se o sistema está ativo. Usado para jobs
    /// </summary>
    /// <returns></returns>
    public static bool IstAtivo()
    {
        var rec = BL_SisSistema.SelectByPk((decimal)Sistema);
        return rec != null ? rec.Ativo.ToBoolean() : true;
    }

    #endregion Methods

    #region Log

    /// <summary>
    /// Método geral de gravação de Log
    /// </summary>
    /// <param name="mensagem"></param>
    /// <param name="tipoLog"></param>
    /// <param name="metodo"></param>
    /// <param name="linha"></param>
    /// <param name="tag"></param>
    /// <param name="exception"></param>
    /// <param name="stack"></param>
    /// <returns></returns>
    public static void Log(string mensagem, EnumLogTipo tipoLog = EnumLogTipo.Message, string metodo = null,
        int? linha = null, string tag = null, string exception = null, string stack = null, string usuario = null)
    {
        var log = new SisLog
        {
            Mensagem = mensagem.Left(4000),
            Tipo = tipoLog,
            Usuario = usuario ?? Usuario,
            Data = DateTime.Now,
            Metodo = metodo.Left(200),
            Linha = linha,
            Tag = string.IsNullOrEmpty(tag) ? Environment.MachineName : (tag + " -- " + Environment.MachineName).Left(4000),
            Exception = exception.Left(4000),
            Stack = stack.Left(4000),
            Sistema = Sistema
        };

        // trata erro para não parar o sistema por causa do log
        try
        {
            if (Transaction.Current == null)
            {
                BL_SisLog.Insert(log);
            }
            else
            {
                using (var tr = new TransactionScope(TransactionScopeOption.Suppress))
                {
                    BL_SisLog.Insert(log);
                }
            }
        }
        catch
        {
        }
    }

    /// <summary>
    /// Log de trace
    /// </summary>
    /// <param name="mensagem"></param>
    /// <param name="tag"></param>
    public static void LogTrace(string mensagem = null, string tag = null)
    {
        Log(mensagem, EnumLogTipo.Trace);
    }

    /// <summary>
    /// Log de trace
    /// </summary>
    /// <param name="mensagem"></param>
    /// <param name="tag"></param>
    public static void LogWarning(string mensagem = null, string tag = null)
    {
        Log(mensagem, EnumLogTipo.Warning);
    }

    /// <summary>
    /// Log de exception
    /// </summary>
    /// <param name="ex"></param>
    /// <param name="mensagem"></param>
    /// <param name="tag"></param>
    public static void LogException(Exception ex, string mensagem = null, string tag = null)
    {
        try
        {
            StackTrace stackTrace = new StackTrace();
            StackFrame stackFrame = stackTrace.GetFrame(1);
            MethodBase method = stackFrame.GetMethod();

            Log(
                mensagem ?? ex.Message,
                EnumLogTipo.Exception,
                string.Format("{0}.{1}({2})", method.ReflectedType.FullName, method.Name, string.Join(",", method.GetParameters().Select(o => string.Format("{0} {1}", o.ParameterType, o.Name)).ToArray())),
                stackFrame.GetFileLineNumber(),
                tag,
                ex.Message,
                Conv.GetErrorMessage(ex, true),
                Usuario);
        }
        catch
        {
        }
    }

    /// <summary>
    /// Log de exception, mas ao invés de usar GetFrame(1) usa GetFrame(2)
    /// Usado para sbrepor métodos de log legados
    /// </summary>
    /// <param name="ex"></param>
    /// <param name="mensagem"></param>
    /// <param name="tag"></param>
    public static void LogException2(Exception ex, string mensagem = null, string tag = null)
    {
        try
        {
            StackTrace stackTrace = new StackTrace();
            StackFrame stackFrame = stackTrace.GetFrame(2);
            MethodBase method = stackFrame.GetMethod();

            Log(
                mensagem ?? ex.Message,
                EnumLogTipo.Exception,
                string.Format("{0}.{1}({2})", method.ReflectedType.FullName, method.Name, string.Join(",", method.GetParameters().Select(o => string.Format("{0} {1}", o.ParameterType, o.Name)).ToArray())),
                stackFrame.GetFileLineNumber(),
                tag,
                ex.Message,
                Conv.GetErrorMessage(ex, true),
                Usuario);
        }
        catch
        {
        }
    }

    /// <summary>
    /// Método útil pra substituir o 
    /// </summary>
    /// <param name="message"></param>
    /// <param name="type"></param>
    public static void WriteEntry(string message, EventLogEntryType type)
    {
        if (type == EventLogEntryType.Error)
            Sis.Log(message, EnumLogTipo.Exception);
        else
            Sis.Log(message, EnumLogTipo.Message);
    }

    /// <summary>
    /// Método útil pra substituir o 
    /// </summary>
    /// <param name="message"></param>
    /// <param name="type"></param>
    public static void WriteEntry(Exception ex, string message)
    {
        LogException2(ex, message);
    }

    #endregion Log

}
