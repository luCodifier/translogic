namespace Translogic.Modules.Core
{
    using System;
    using System.Web.Mvc;
    using System.Web.Routing;
    using ALL.Core.AcessoDados;
    using AutoMapper;
    using Castle.MicroKernel.Registration;
    using Castle.Windsor;
    using Domain.DataAccess.Mappings.Diversos.ConfigCADE;
    using Domain.DataAccess.Repositories.Diversos.ConfigCADE;
    using Domain.Jobs.Petrobras;
    using Domain.Model.Diversos.ConfigCADE.Repositories;
    using Domain.Services;
    using Domain.Services.FluxosComerciais.NfePetrobras;
    using Translogic.Core.Infrastructure;
    using Translogic.Core.Infrastructure.DataAccess;
    using Translogic.Core.Infrastructure.Modules;
    using Translogic.Core.Infrastructure.Web;
    using Translogic.Core.Infrastructure.Web.ControllerExtensions;
    using Translogic.Modules.Core.Domain.DataAccess.Listeners;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories.Acesso;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories.Appa;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories.AvisoFat;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories.Bolar;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories.CadastroUnicoEstacoes;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories.Codificador;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories.Consultas;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories.Diversos;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories.Diversos.Bacen;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories.Diversos.CodigoBarras;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories.Diversos.Ibge;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories.Documentacao;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories.Edi;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories.Estrutura;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Atrasos;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Carregamentos;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Dcls;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.DocumentosVagao;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.InterfaceSap;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Mdfes;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Mdfes.TriangulusConfig;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Ndd;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Nfes;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Pedidos;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Pedidos.Despachos;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Refaturamento;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Simconsultas;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Simconsultas.Ctes;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.TriangulusConfig;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories.Fornecedor;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories.IntegracaoSiv;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories.Intercambios;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories.LaudoMercadoria;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories.LocaisFornecedor;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories.MotivoSituacaoVagao;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories.OSLimpezaVagao;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories.OSRevistamentoVagao;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories.PainelExpedicao;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories.PainelExpedicao.Parametrizacao;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories.RelatorioFaturamento;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories.Seguro;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories.Thermometer;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories.TipoServico;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories.Trem;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories.Trem.MovimentacaoTrem;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories.Trem.OrdemServico;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories.Trem.Veiculo.Conteiner;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories.Trem.Veiculo.Locomotiva;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories.Trem.Veiculo.Vagao;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories.Trem.Veiculo.Vagao.QuadroEstados;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories.Via;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories.Via.Circulacao;
    using Translogic.Modules.Core.Domain.Jobs;
    using Translogic.Modules.Core.Domain.Jobs.AcompanhamentoFat;
    using Translogic.Modules.Core.Domain.Jobs.AvisoFat;
    using Translogic.Modules.Core.Domain.Jobs.Edi;
    using Translogic.Modules.Core.Domain.Jobs.EnvioDocumentacao;
    using Translogic.Modules.Core.Domain.Jobs.FluxosComerciais;
    using Translogic.Modules.Core.Domain.Jobs.FluxosComerciais.ConhecimentoTransporteEletronico;
    using Translogic.Modules.Core.Domain.Jobs.SefazNfe;
    using Translogic.Modules.Core.Domain.Jobs.Trem;
    using Translogic.Modules.Core.Domain.Model.Acesso;
    using Translogic.Modules.Core.Domain.Model.Acesso.Repositories;
    using Translogic.Modules.Core.Domain.Model.Appa;
    using Translogic.Modules.Core.Domain.Model.Bolar.Repositories;
    using Translogic.Modules.Core.Domain.Model.CadastroUnicoEstacoes;
    using Translogic.Modules.Core.Domain.Model.CadastroUnicoEstacoes.Repositories;
    using Translogic.Modules.Core.Domain.Model.Codificador.Repositories;
    using Translogic.Modules.Core.Domain.Model.Consultas.Repositories;
    using Translogic.Modules.Core.Domain.Model.Diversos;
    using Translogic.Modules.Core.Domain.Model.Diversos.Bacen.Repositories;
    using Translogic.Modules.Core.Domain.Model.Diversos.CodigoBarras.Repositories;
    using Translogic.Modules.Core.Domain.Model.Diversos.Ibge.Repositories;
    using Translogic.Modules.Core.Domain.Model.Diversos.Repositories;
    using Translogic.Modules.Core.Domain.Model.Documentacao.Repositories;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Modules.Core.Domain.Model.Edi;
    using Translogic.Modules.Core.Domain.Model.Edi.Repositories;
    using Translogic.Modules.Core.Domain.Model.Estrutura;
    using Translogic.Modules.Core.Domain.Model.Estrutura.Repositories;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Atrasos.Repositories;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Carregamentos.Repositories;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Dcls.Repositories;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.DocumentosVagao.Repositories;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.InterfaceSap.Repositories;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes.Repositories;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes.Repositories.TriangulusConfig;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Ndd.Repositories;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Nfes.Repositories;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos.Repositories;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Repositories;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Refaturamento.Repositories;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Repositories;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Simconsultas.Ctes.Repositories;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Simconsultas.Repositories;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.TriangulusConfig.Repositories;
    using Translogic.Modules.Core.Domain.Model.Fornecedor;
    using Translogic.Modules.Core.Domain.Model.Fornecedor.Repositories;
    using Translogic.Modules.Core.Domain.Model.IntegracaoSIV.Repositories;
    using Translogic.Modules.Core.Domain.Model.Intercambios.Repositories;
    using Translogic.Modules.Core.Domain.Model.LaudoMercadoria.Repositories;
    using Translogic.Modules.Core.Domain.Model.LocaisFornecedor.Repositories;
    using Translogic.Modules.Core.Domain.Model.MotivoSituacaoVagao.Repositories;
    using Translogic.Modules.Core.Domain.Model.OSLimpezaVagao;
    using Translogic.Modules.Core.Domain.Model.OSLimpezaVagao.Repositories;
    using Translogic.Modules.Core.Domain.Model.OSRevistamentoVagao;
    using Translogic.Modules.Core.Domain.Model.OSRevistamentoVagao.Repositories;
    using Translogic.Modules.Core.Domain.Model.PainelExpedicao;
    using Translogic.Modules.Core.Domain.Model.PainelExpedicao.Repositories;
    using Translogic.Modules.Core.Domain.Model.RelatorioFaturamento.Repositories;
    using Translogic.Modules.Core.Domain.Model.Seguro.Repositories;
    using Translogic.Modules.Core.Domain.Model.Thermometer.Repositories;
    using Translogic.Modules.Core.Domain.Model.TipoServicos;
    using Translogic.Modules.Core.Domain.Model.TipoServicos.Repositories;
    using Translogic.Modules.Core.Domain.Model.Trem;
    using Translogic.Modules.Core.Domain.Model.Trem.MovimentacaoTrem.Repositories;
    using Translogic.Modules.Core.Domain.Model.Trem.OrdemServico;
    using Translogic.Modules.Core.Domain.Model.Trem.OrdemServico.Repositories;
    using Translogic.Modules.Core.Domain.Model.Trem.Repositories;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Conteiner.Repositories;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Locomotiva;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Locomotiva.Repositories;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao.QuadroEstado.Repositories;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao.Repositories;
    using Translogic.Modules.Core.Domain.Model.Via;
    using Translogic.Modules.Core.Domain.Model.Via.Circulacao;
    using Translogic.Modules.Core.Domain.Model.Via.Circulacao.Repositories;
    using Translogic.Modules.Core.Domain.Model.Via.Repositories;
    using Translogic.Modules.Core.Domain.Services.Acesso;
    using Translogic.Modules.Core.Domain.Services.AcompanhamentoFat;
    using Translogic.Modules.Core.Domain.Services.Appa;
    using Translogic.Modules.Core.Domain.Services.AvisoFat;
    using Translogic.Modules.Core.Domain.Services.CadastroUnicoEstacoes;
    using Translogic.Modules.Core.Domain.Services.Consulta;
    using Translogic.Modules.Core.Domain.Services.Ctes;
    using Translogic.Modules.Core.Domain.Services.Ctes.Interface;
    using Translogic.Modules.Core.Domain.Services.Documentacao;
    using Translogic.Modules.Core.Domain.Services.Edi;
    using Translogic.Modules.Core.Domain.Services.Estrutura;
    using Translogic.Modules.Core.Domain.Services.FluxosComerciais;
    using Translogic.Modules.Core.Domain.Services.Fornecedor;
    using Translogic.Modules.Core.Domain.Services.Fornecedor.Interface;
    using Translogic.Modules.Core.Domain.Services.GestaoDocumentos;
    using Translogic.Modules.Core.Domain.Services.LaudoMercadoria;
    using Translogic.Modules.Core.Domain.Services.LocaisFornecedor;
    using Translogic.Modules.Core.Domain.Services.LocaisFornecedor.Interface;
    using Translogic.Modules.Core.Domain.Services.ManutencaoDespacho;
    using Translogic.Modules.Core.Domain.Services.Mdfes;
    using Translogic.Modules.Core.Domain.Services.Mdfes.Interface;
    using Translogic.Modules.Core.Domain.Services.MotivoSituacaoVagao;
    using Translogic.Modules.Core.Domain.Services.OSLimpezaVagao;
    using Translogic.Modules.Core.Domain.Services.OSLimpezaVagao.Interface;
    using Translogic.Modules.Core.Domain.Services.OSRevistamentoVagao;
    using Translogic.Modules.Core.Domain.Services.OSRevistamentoVagao.Interface;
    using Translogic.Modules.Core.Domain.Services.PainelExpedicao;
    using Translogic.Modules.Core.Domain.Services.RelatorioFaturamento;
    using Translogic.Modules.Core.Domain.Services.RelatorioFaturamento.Interface;
    using Translogic.Modules.Core.Domain.Services.Seguro;
    using Translogic.Modules.Core.Domain.Services.Thermometer;
    using Translogic.Modules.Core.Domain.Services.TipoServico;
    using Translogic.Modules.Core.Domain.Services.TipoServico.Interface;
    using Translogic.Modules.Core.Domain.Services.Trem;
    using Translogic.Modules.Core.Domain.Services.Via;
    using Translogic.Modules.Core.Domain.Services.Via.Interface;
    using Translogic.Modules.Core.Filters;
    using Translogic.Modules.Core.Infrastructure.Services;
    using Translogic.Modules.Core.Interfaces.Acesso;
    using Translogic.Modules.Core.Interfaces.DadosFiscais.Interfaces;
    using Translogic.Modules.Core.Interfaces.GestaoDocumentos;
    using Translogic.Modules.Core.Interfaces.Jobs;
    using Translogic.Modules.Core.Interfaces.Jobs.Service;
    using Translogic.Modules.Core.Interfaces.Trem;
    using Translogic.Modules.Core.Interfaces.Trem.Estrutura;
    using Translogic.Modules.Core.Interfaces.Trem.OrdemServico;
    using Translogic.Modules.Core.Interfaces.Trem.Veiculo.Locomotiva;
    using Translogic.Modules.Core.Tests.Domain.Services.Trem.OrdemService;
    using Translogic.Modules.EDI.Domain.DataAccess.Repositories.Nfes;
    using Translogic.Modules.EDI.Domain.Jobs;
    using Translogic.Modules.EDI.Domain.Models.Nfes.Repositories;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories.Administracao;
    using Translogic.Modules.Core.Domain.Services.Administracao;
    using Translogic.Modules.Core.Domain.Model.LiberacaoFormacaoTrem.Repositories;
    using Translogic.Modules.Core.Domain.Model.QuadroEstados.Repositories;
    using Translogic.Modules.Core.Domain.Services.LiberacaoFormacaoTrem;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories.QuadroEstados;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories.LiberacaoFormacaoTrem;
    using Translogic.Modules.Core.Domain.Services.Seguro.Interface;
    using Translogic.Modules.Core.Domain.Model.Tfa.Repositories;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories;
    using Translogic.Modules.Core.Domain.Services.Tfa.Interface;
    using Translogic.Modules.Core.Domain.Services.Tfa;
    using Translogic.Modules.Core.Domain.Model.Tfa;
    using Translogic.Modules.Core.Domain.Model.RelatorioFichaRecomendacao.Repositories;
    using Translogic.Modules.Core.Domain.Services.RelatorioFichaRecomendacao.Interface;
    using Translogic.Modules.Core.Domain.Services.RelatorioFichaRecomendacao;
    using Translogic.Modules.Core.Domain.Model.Despacho.Repositories;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories.Despacho;
    using Translogic.Modules.Core.Domain.Services.VooAtivo.Interface;
    using Translogic.Modules.Core.Domain.Model.Vagoes.Repositories;
    using Translogic.Modules.Core.Domain.Services.VooAtivo;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories.Vagoes;
    using Translogic.Modules.Core.Domain.Model.VooAtivo;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories.VooAtivo;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories.Tfa;
    using Translogic.Modules.Core.Domain.Model.ChecklistPassageiros.Repositories;
    using Translogic.Modules.Core.Domain.Services.ChecklistPassageiro;
    using Translogic.Modules.Core.Domain.Services.ChecklistPassageiro.Interface;
    using Translogic.Modules.Core.Domain.Services.Baldeio.Interface;
    using Translogic.Modules.Core.Domain.Services.Baldeio;
    using Translogic.Modules.Core.Domain.Model.ArquivoBaldeio.Repositorio;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories.ArquivoBaldeio;
    using Translogic.Modules.Core.Domain.Services.ArquivoBaldeio.Interface;
    using Translogic.Modules.Core.Domain.Services.ArquivoBaldeio;
    using Translogic.Modules.Core.Interfaces.ControlePerdas;
    using Translogic.Modules.Core.Domain.Services.ControlePerdas;
    using Translogic.Modules.Core.Domain.Services.ControlePerdas.Interface;
    using Translogic.Modules.Core.Domain.Model.ControlePerdas.Repositories;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories.ControlePerdas;
    using Translogic.Modules.Core.Domain.Model.ConsultaTrem.Repositories;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories.ConsultaTrem;
    using Translogic.Modules.Core.Domain.Services.ConsultaTrem;
    using Translogic.Modules.Core.Domain.Services.ConsultaTrem.Interface;
    using Translogic.Modules.Core.Domain.Model.ConsultaTrem;
    using Translogic.Modules.Core.Domain.Model.Dto.ConsultaTrem;
    using Translogic.Modules.Core.Domain.Model.AnexacaoDesanexacao.Repositories;
    using Translogic.Modules.Core.Domain.Services.AnexacaoDesanexacao;
    using Translogic.Modules.Core.Domain.Services.AnexacaoDesanexacao.Interface;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories.AnexacaoDesanexacao;
    using Translogic.Modules.Core.Domain.Services.LogAnxHistTrem;
    using Translogic.Modules.Core.Domain.Services.LogAnxHistVeiculo;
    using Translogic.Modules.Core.Util;
    using Translogic.Modules.Core.Domain.Services.Terminal.Interface;
    using Translogic.Modules.Core.Domain.Services.Terminal;
    using Translogic.Modules.Core.Domain.Model.FormacaoAutomatica.Repositories;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories.FormacaoAutomatica;
    using Translogic.Modules.Core.Domain.Services.FormacaoAutomatica.Interface;
    using Translogic.Modules.EDI.Domain.Models.Edi.Repositories;
    using Translogic.Modules.EDI.Domain.DataAccess.Repositories.Edi;
    using Translogic.Modules.Core.Domain.Services.Tolerancia.Interface;
    using Translogic.Modules.Core.Domain.Model.Tolerancia.Repositories;
    using Translogic.Modules.Core.Domain.Services.Tolerancia;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories.Tolerancia;
    using Translogic.Modules.Core.Domain.Services.Rota;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories.Rota;
    using Translogic.Modules.Core.Domain.Model.Rota.Repositories;
    using Translogic.Modules.Core.Domain.Services.Rota.Interface;

    /// <summary>
    ///     Configurador de m�dulo ao ser adicionado na aplica��o
    /// </summary>
    public class CoreInstaller : IModuleInstaller
    {
        #region Public Properties

        /// <summary>
        ///     Nome do m�dulo
        /// </summary>
        public string Name
        {
            get
            {
                return "Core";
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     Efetua o mapeamento utilizando o AutoMapper
        /// </summary>
        public static void RegistrarAutoMapper()
        {
            Mapper.CreateMap<OrdemServico, OrdemServicoDto>()
                .ForMember(os => os.Situacao, s => s.MapFrom(si => (SituacaoOrdemServicoEnum)si.Situacao.Id))
                .ForMember(os => os.Usuario, s => s.MapFrom(si => si.UsuarioCadastro));

            Mapper.CreateMap<OrdemServicoDto, OrdemServico>()
                .ForMember(os => os.Situacao, s => s.MapFrom(si => new SituacaoOrdemServico { Id = (int)si.Situacao }))
                .ForMember(os => os.UsuarioCadastro, s => s.MapFrom(si => si.Usuario));

            Mapper.CreateMap<SerieLocomotiva, SerieLocomotivaDto>();
            Mapper.CreateMap<SerieLocomotivaDto, SerieLocomotiva>();

            Mapper.CreateMap<ParadaOrdemServicoProgramada, ParadaOrdemServicoProgramadaDto>();
            Mapper.CreateMap<ParadaOrdemServicoProgramadaDto, ParadaOrdemServicoProgramada>();

            Mapper.CreateMap<PlanejamentoSerieLocomotiva, PlanejamentoSerieLocomotivaDto>()
                .ForMember(d => d.IdOrdemServico, s => s.MapFrom(sb => sb.OrdemServico.Id));
            Mapper.CreateMap<PlanejamentoSerieLocomotivaDto, PlanejamentoSerieLocomotiva>()
                .ForMember(s => s.OrdemServico, s => s.MapFrom(sb => new OrdemServico { Id = sb.IdOrdemServico }));

            Mapper.CreateMap<DuracaoAtividadeEstacao, DuracaoAtividadeEstacaoDto>();
            Mapper.CreateMap<DuracaoAtividadeEstacaoDto, DuracaoAtividadeEstacao>();

            Mapper.CreateMap<Atividade, AtividadeDto>();
            Mapper.CreateMap<AtividadeDto, Atividade>();

            Mapper.CreateMap<Estacao, LocalDto>();
            Mapper.CreateMap<LocalDto, Estacao>();

            Mapper.CreateMap<EstacaoMae, LocalDto>();
            Mapper.CreateMap<LocalDto, EstacaoMae>();

            Mapper.CreateMap<PatioCirculacao, LocalDto>();
            Mapper.CreateMap<LocalDto, PatioCirculacao>();

            Mapper.CreateMap<EstacaoMae, EstacaoMaeDto>();
            Mapper.CreateMap<EstacaoMaeDto, EstacaoMae>();

            Mapper.CreateMap<Usuario, UsuarioDto>();
            Mapper.CreateMap<UsuarioDto, Usuario>();

            Mapper.CreateMap<EmpresaFerrovia, EmpresaFerroviaDto>();
            Mapper.CreateMap<EmpresaFerroviaDto, EmpresaFerrovia>();

            Mapper.CreateMap<ConferenciaArquivos, PainelExpedicaoConferenciaArquivosDto>();
            Mapper.CreateMap<PainelExpedicaoConferenciaArquivosDto, ConferenciaArquivos>();
            Mapper.CreateMap<EdiErroResultPesquisa, EdiErroResultPesquisaDto>();

            Mapper.CreateMap<Rota, RotaDto>()
                .ForMember(rt => rt.Origem, o => o.MapFrom(ori => new LocalDto { Codigo = ori.Origem.Codigo }))
                .ForMember(rt => rt.Destino, o => o.MapFrom(dst => new LocalDto { Codigo = dst.Destino.Codigo }));
            Mapper.CreateMap<RotaDto, Rota>()
                .ForMember(rt => rt.Origem, o => o.MapFrom(ori => new PatioCirculacao { Codigo = ori.Origem.Codigo }))
                .ForMember(rt => rt.Destino, o => o.MapFrom(dst => new PatioCirculacao { Codigo = dst.Destino.Codigo }));

            Mapper.CreateMap<IAreaOperacional, LocalDto>();
            Mapper.CreateMap<LocalDto, IAreaOperacional>();

            Mapper.CreateMap<RotaSentido, RotaSentidoDto>();
            Mapper.CreateMap<RotaSentidoDto, RotaSentido>();

            Mapper.CreateMap<AreaOperacionalRotaDto, AreaOperacionalRota>();
            Mapper.CreateMap<AreaOperacionalRota, AreaOperacionalRotaDto>();

            Mapper.CreateMap<AtividadesParadaOrdemServicoProgramada, AtividadesParadaOrdemServicoProgramadaDto>();
            Mapper.CreateMap<AtividadesParadaOrdemServicoProgramadaDto, AtividadesParadaOrdemServicoProgramada>();

            //Mapeamento para OS Limpeza
            Mapper.CreateMap<OSStatus, OSStatusDto>();
            Mapper.CreateMap<OSTipoServico, OSTipoDto>();
            Mapper.CreateMap<OSLimpezaVagao, OSLimpezaVagaoDto>();
            Mapper.CreateMap<OSRevistamentoVagao, OSRevistamentoVagaoDto>();
            Mapper.CreateMap<OSRevistamentoVagaoItem, OSRevistamentoVagaoItemDto>();

            //Mapeamento para fornecedor(de model para DTO)
            Mapper.CreateMap<FornecedorOs, FornecedorOsDto>()
                .ForMember(dest => dest.DtRegistro, ori => ori.MapFrom(l => l.DtRegistro))
                .ForMember(dest => dest.IdFornecedorOs, ori => ori.MapFrom(l => Convert.ToDecimal(l.Id)))
                .ForMember(dest => dest.Login, ori => ori.MapFrom(l => l.Login))
                .ForMember(dest => dest.Nome, ori => ori.MapFrom(l => l.Nome))
                .ForMember(dest => dest.Ativo, ori => ori.MapFrom(l => l.Ativo));

            //               origem-->destino(de model para dto)
            Mapper.CreateMap<TipoServico, TipoServicoDto>()
                .ForMember(dto => dto.DescricaoServico, model => model.MapFrom(l => l.Servico))
                .ForMember(dto => dto.IdTipoServico, model => model.MapFrom(l => l.Id));

            //               origem-->destino(de dto para model)
            Mapper.CreateMap<TipoServicoDto, TipoServico>()
                .ForMember(model => model.Id, dto => dto.MapFrom(l => l.IdTipoServico))
                .ForMember(model => model.Servico, dto => dto.MapFrom(l => l.DescricaoServico));


            Mapper.CreateMap<FornecedorOsDto, FornecedorOs>()
                .ForMember(os => os.Nome, dto => dto.MapFrom(to => to.Nome))
                .ForMember(os => os.Id, dto => dto.MapFrom(to => to.IdFornecedorOs))
                .ForMember(os => os.Login, dto => dto.MapFrom(to => to.Login))
                .ForMember(os => os.DtRegistro, dto => dto.MapFrom(to => to.DtRegistro));

            //origem-->destino de model para dto           
            Mapper.CreateMap<PerguntaChecklist, PerguntaCheckListDto>()
                .ForMember(dto => dto.Pergunta, model => model.MapFrom(m => m.Descricao))
                .ForMember(dto => dto.OrdemPergunta, model => model.MapFrom(m => m.Ordem))
                .ForMember(dto => dto.IdPergunta, model => model.MapFrom(m => m.Id))
                .ForMember(dto => dto.IdAgrupador, model => model.MapFrom(m => m.Id));


            //origem-->destino de dto para model           
            Mapper.CreateMap<PerguntaCheckListDto, PerguntaChecklist>()
                .ForMember(model => model.Descricao, dto => dto.MapFrom(d => d.Pergunta))
                .ForMember(model => model.Id, dto => dto.MapFrom(d => d.IdPergunta))
                .ForMember(model => model.Ordem, dto => dto.MapFrom(d => d.OrdemPergunta))
                .ForMember(model => model.Agrupador, dto => dto.MapFrom(d => d.IdAgrupador));

        }

        /// <summary>
        ///     Instala os componentes e as rotas
        /// </summary>
        /// <param name="container">Container do windsor</param>
        public void InstallComponents(IWindsorContainer container)
        {
            container.RegisterControllers(typeof(CoreInstaller).Assembly);
            this.InstallDomainComponents(container);
        }

        /// <summary>
        ///     Registra as rotas do m�dulo
        /// </summary>
        /// <param name="routes">Cole��o de rotas do MVC</param>
        public void RegisterRoutes(RouteCollection routes)
        {
            ModelBinders.Binders.Remove(typeof(decimal));
            ModelBinders.Binders.Remove(typeof(decimal?));
            ModelBinders.Binders.Add(typeof(decimal), new DecimalModelBinder());
            ModelBinders.Binders.Add(typeof(decimal?), new DecimalModelBinder());

            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("{*favicon}", new { favicon = @"(.*/)?favicon.ico(/.*)?" });

            routes.MapRoute(
                "resources",
                "resx/{key}/{resourcetype}.all",
                new { controller = "Resources", action = "index", module = "core" });

            routes.MapRoute(
                "appChecker",
                "AppChecker.all",
                new { controller = "AppChecker", action = "index", module = "core" });

            routes.MapRoute(
                "tela_all",
                "{codigoTela}.all",
                new { controller = "home", action = "index", module = "core" },
                new { codigoTela = new IntegerRouteConstraint() });

            routes.MapRoute(
                "tela",
                "{codigoTela}",
                new { controller = "home", action = "index", module = "core" },
                new { codigoTela = new IntegerRouteConstraint() });

            routes.MapRoute("home_all", "home.all", new { controller = "home", action = "index", module = "core" });

            routes.MapRoute("homeController", "home/{action}.all", new { controller = "home", module = "core" });

            routes.MapRoute("home", "home", new { controller = "home", action = "index", module = "core" });

            routes.MapRoute("acesso", "acesso/{action}.all", new { controller = "acesso", module = "core" });

            routes.MapRoute("Default", string.Empty, new { controller = "home", action = "index", module = "core" });
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Instala os componentes no container
        /// </summary>
        /// <param name="container">Windsor Container inicializado na aplica��o</param>
        private void InstallDomainComponents(IWindsorContainer container)
        {
            RegistrarAutoMapper();

            container.Register(Component.For<IRepository>().ImplementedBy<NHRepository>());

            container.Register(
                Component.For<AuditoriaListener>(),
                Component.For<ILogAuditoriaRepository>().ImplementedBy<LogAuditoriaRepository>(),
                Component.For<AuditoriaService>());

            container.Register(
                Component.For<IJobRunnerService>().ImplementedBy<JobRunnerService>().Named("core.jobrunnerservice"));

            container.Register(Component.For<IJob>().ImplementedBy<TesteJob>().Named("core.job.teste"));

            container.Register(
                Component.For<IJob>().ImplementedBy<CorrecaoNotasFiscaisJob>().Named("core.job.correcaoNotasFiscais"));

            container.Register(
                Component.For<DfeSefazService>(),
                Component.For<ICertificadosRepository>().ImplementedBy<CertificadosRepository>(),
                Component.For<INfeDistribuicaoReadonlyRepository>().ImplementedBy<NfeDistribuicaoReadonlyRepository>(),
                Component.For<IDFeConsultaRepository>().ImplementedBy<DFeConsultaRepository>(),
                Component.For<ILogDfeSefazRepository>().ImplementedBy<LogDfeSefazRepository>(),
                Component.For<INfeProdutoDistribuicaoRepository>().ImplementedBy<NfeProdutoDistribuicaoRepository>(),
                Component.For<IJob>().ImplementedBy<DFeServiceJob>().Named("core.job.dfeservice"));

            container.Register(
                Component.For<AvisoVagoesNaoFaturadosService>(),
                Component.For<IContatoAvisoFatRepository>().ImplementedBy<ContatoAvisoFatRepository>(),
                Component.For<IJob>().ImplementedBy<AvisoVagoesNaoFaturadosJob>().Named("core.job.avisovagoesfat"));

            container.Register(
             Component.For<GerarArquivosXmlService>(),
             Component.For<IJob>().ImplementedBy<GerarArquivosXmlJob>().Named("core.job.gerararquivosxmlservice"));

            container.Register(
            Component.For<AvisoTaraMedianaService>(),
            Component.For<IJob>().ImplementedBy<AvisoTaraMedianaJob>().Named("core.job.enviarAvisoTarasMedianas"));

            container.Register(
            Component.For<GerarArquivosPdfService>(),
            Component.For<IJob>().ImplementedBy<GerarArquivosPdfJob>().Named("core.job.gerararquivospdfservice"));

            container.Register(
                Component.For<AcompanhamentoFatService>(),
                Component.For<AcompanhamentoFatMalhaNorteService>(),
                Component.For<IDeParaOperacaoRepository>().ImplementedBy<DeParaOperacaoRepository>(),
                Component.For<IJob>().ImplementedBy<AcompanhamentoFatJob>().Named("core.job.acompanhamentofat"));

            container.Register(
                Component.For<IJob>().ImplementedBy<NotasPetrobrasServiceJob>().Named("core.job.notaspetrobras"));

            container.Register(
                Component.For<IJob>()
                    .ImplementedBy<NfeBoProcessarPoolingJob>()
                    .DependsOn(new { UfIbge = "32" })
                    .Named("core.job.NfeBoProcessarPooling32"),
                Component.For<IJob>()
                    .ImplementedBy<NfeBoProcessarPoolingJob>()
                    .DependsOn(new { UfIbge = "35" })
                    .Named("core.job.NfeBoProcessarPooling35"),
                Component.For<IJob>()
                    .ImplementedBy<NfeBoProcessarPoolingJob>()
                    .DependsOn(new { UfIbge = "41" })
                    .Named("core.job.NfeBoProcessarPooling41"),
                Component.For<IJob>()
                    .ImplementedBy<NfeBoProcessarPoolingJob>()
                    .DependsOn(new { UfIbge = "42" })
                    .Named("core.job.NfeBoProcessarPooling42"),
                Component.For<IJob>()
                    .ImplementedBy<NfeBoProcessarPoolingJob>()
                    .DependsOn(new { UfIbge = "43" })
                    .Named("core.job.NfeBoProcessarPooling43"),
                Component.For<IJob>()
                    .ImplementedBy<NfeBoProcessarPoolingJob>()
                    .DependsOn(new { UfIbge = "50" })
                    .Named("core.job.NfeBoProcessarPooling50"),
                Component.For<IJob>()
                    .ImplementedBy<NfeBoProcessarPoolingJob>()
                    .DependsOn(new { UfIbge = "51" })
                    .Named("core.job.NfeBoProcessarPooling51"),
                Component.For<IJob>()
                    .ImplementedBy<NfeBoProcessarPoolingJob>()
                    .DependsOn(new { UfIbge = "52" })
                    .Named("core.job.NfeBoProcessarPooling52"),
                Component.For<IJob>()
                    .ImplementedBy<NfeBoProcessarPoolingJob>()
                    .DependsOn(new { UfIbge = "OE" })
                    .Named("core.job.NfeBoProcessarPoolingOE"));

            container.Register(
                Component.For<IJob>().ImplementedBy<NfeBoInserirPoolingJob>().Named("core.job.NfeBoInserirPooling"));

            container.Register(
                Component.For<IJob>().ImplementedBy<CorrecaoNfeEdiJob>().Named("core.job.correcaoNfeEdiJob"));

            container.Register(
                Component.For<IJob>().ImplementedBy<AtualizacaoNfePdfId>().Named("core.job.AtualizacaoNfePdfId"));

            container.Register(
                Component.For<DocumentacaoVagaoService>(),
                Component.For<IJob>().ImplementedBy<GerenciamentoDocumentacaoJob>().Named("core.job.GerenciamentoDocumentacao"),
                Component.For<IJob>().ImplementedBy<EnvioDocumentacaoJob>().Named("core.job.EnvioDocumentacaoEmail"));

            container.Register(
                Component.For<IJob>().ImplementedBy<MovimentacaoTremRealJob>().Named("core.job.MovimentacaoTremRealJob"));

            container.Register(
                Component.For<IJob>()
                    .ImplementedBy<NfeEmailEdiJob>()
                    .DependsOn(new { Hora = 1, Order = "ASC" })
                    .Named("core.job.NfeEmailEdiJob1Asc"));

            container.Register(
                Component.For<IJob>()
                    .ImplementedBy<NfeEmailEdiJob>()
                    .DependsOn(new { Hora = 2, Order = "ASC" })
                    .Named("core.job.NfeEmailEdiJob2Asc"));

            container.Register(
                Component.For<IJob>()
                    .ImplementedBy<NfeEmailEdiJob>()
                    .DependsOn(new { Hora = 3, Order = "ASC" })
                    .Named("core.job.NfeEmailEdiJob3Asc"));

            container.Register(
                Component.For<IJob>()
                    .ImplementedBy<NfeEmailEdiJob>()
                    .DependsOn(new { Hora = 1, Order = "DESC" })
                    .Named("core.job.NfeEmailEdiJob1Desc"));

            container.Register(
                Component.For<IJob>()
                    .ImplementedBy<NfeEmailEdiJob>()
                    .DependsOn(new { Hora = 2, Order = "DESC" })
                    .Named("core.job.NfeEmailEdiJob2Desc"));

            container.Register(
                Component.For<IJob>()
                    .ImplementedBy<NfeEmailEdiJob>()
                    .DependsOn(new { Hora = 3, Order = "DESC" })
                    .Named("core.job.NfeEmailEdiJob3Desc"));

            container.Register(
                Component.For<IJob>()
                    .ImplementedBy<NfeEmailEdiJob>()
                    .DependsOn(new { Hora = 99, Order = "ASC" })
                    .Named("core.job.NfeEmailEdiJob99Asc"));

            container.Register(
                Component.For<IJob>()
                    .ImplementedBy<NfeEmailEdiJob>()
                    .DependsOn(new { Hora = 99, Order = "DESC" })
                    .Named("core.job.NfeEmailEdiJob99Desc"));

            container.Register(
                Component.For<IJob>()
                    .ImplementedBy<EnvioCarregamentoFerroviarioJob>()
                    .Named("edi.job.EnvioCarregamentoFerroviario"));

            container.Register(
            Component.For<IJob>()
                .ImplementedBy<EnvioDescargaRodoviariaJob>()
                .Named("edi.job.EnvioDescargaRodoviaria"));

            container.Register(
                Component.For<IJob>()
                    .ImplementedBy<EnvioNotaFaturamentoJob>()
                    .Named("edi.job.EnvioNotaFaturamento"));

            container.Register(
                Component.For<IJob>()
                    .ImplementedBy<ExclusaoEmailEdiJob>()
                    .DependsOn(new { PastaExclusaoEmails = "Processadas" })
                    .Named("core.job.ExclusaoEmailEdiJob"));

            container.Register(
                Component.For<IJob>()
                    .ImplementedBy<ExclusaoEmailEdiJob>()
                    .DependsOn(new { PastaExclusaoEmails = "Erro" })
                    .Named("core.job.ExclusaoEmailEdiJobPastaErro"));

            container.Register(
                Component.For<IJob>()
                    .ImplementedBy<ExclusaoEmailEdiJob>()
                    .DependsOn(new { PastaExclusaoEmails = "SemXML" })
                    .Named("core.job.ExclusaoEmailEdiJobPastaSemXML"));

            container.Register(
                Component.For<IJob>()
                    .ImplementedBy<ExclusaoEmailEdiJob>()
                    .DependsOn(new { PastaExclusaoEmails = "Eventos_NFe" })
                    .Named("core.job.ExclusaoEmailEdiJobPastaEventos_NFe"));


            container.Register(
                Component.For<ICteCanceladoRefaturamentoConfigRepository>()
                    .ImplementedBy<CteCanceladoRefaturamentoConfigRepository>(),
                Component.For<ICteCanceladoNotasConfigRepository>().ImplementedBy<CteCanceladoNotasConfigRepository>(),
                Component.For<CteCanceladoRefaturamentoService>(),
                Component.For<ICteCanceladoRefaturamentoRepository>()
                    .ImplementedBy<CteCanceladoRefaturamentoRepository>(),
                Component.For<ICteCanceladoNotasRepository>().ImplementedBy<CteCanceladoNotasRepository>(),
                Component.For<ICteCanceladoNotasService>().ImplementedBy<CteCanceladoNotasService>(),
                Component.For<IJob>()
                    .ImplementedBy<CteCanceladoRefaturamentoJob>()
                    .Named("core.job.CteCanceladoRefaturamentoJob"));

            container.Register(
                Component.For<ICteInutilizacaoConfigRepository>().ImplementedBy<CteInutilizacaoConfigRepository>(),
                Component.For<CteInutilizacaoConfigService>(),
                Component.For<IJob>()
                    .ImplementedBy<InsereCtesInutilizacaoConfigJob>()
                    .Named("core.job.InsereCtesInutilizacaoConfigJob"),
                Component.For<IJob>()
                    .ImplementedBy<StatusCteInutilizacaoConfigJob>()
                    .Named("core.job.StatusCteInutilizacaoConfigJob"));

            container.Register(
                Component.For<IMenuRepository>().ImplementedBy<MenuRepository>(),
                Component.For<MenuService>());

            container.Register(
                Component.For<ICteFerroviarioRepository>().ImplementedBy<CteFerroviarioRepository>(),
                Component.For<ICteFerroviarioService>()
                    .ImplementedBy<CteFerroviarioService>()
                    .Named("core.cteferroviarioservice"));

            container.Register(
                Component.For<ICteFerroviarioCanceladoRepository>().ImplementedBy<CteFerroviarioCanceladoRepository>(),
                Component.For<ICteFerroviarioCanceladoService>()
                    .ImplementedBy<CteFerroviarioCanceladoService>()
                    .Named("core.cteferroviariocanceladoservice"));

            container.Register(
                Component.For<ICteConclusaoExportacaoRepository>().ImplementedBy<CteConclusaoExportacaoRepository>(),
                Component.For<ICteConclusaoExportacaoService>()
                    .ImplementedBy<CteConclusaoExportacaoService>()
                    .Named("core.cteconclusaoexportacaoservice"));

            container.Register(
                Component.For<ICteRodoviarioRepository>().ImplementedBy<CteRodoviarioRepository>(),
                Component.For<ICteRodoviarioNotasRepository>().ImplementedBy<CteRodoviarioNotasRepository>(),
                Component.For<ICteRodoviarioService>()
                    .ImplementedBy<CteRodoviarioService>()
                    .Named("core.cterodoviarioservice"));

            container.Register(
                Component.For<IMdfeFerroviarioRepository>().ImplementedBy<MdfeFerroviarioRepository>(),
                Component.For<IMdfeFerroviarioService>()
                    .ImplementedBy<MdfeFerroviarioService>()
                    .Named("core.mdfeferroviarioservice"));

            container.Register(
                Component.For<IConfiguracaoTranslogicRepository>().ImplementedBy<ConfiguracaoTranslogicRepository>());

            container.Register(
                Component.For<IEstacaoRepository>().ImplementedBy<EstacaoRepository>(),
                Component.For<EstacaoService>());

            container.Register(
                Component.For<IUsuarioRepository>().ImplementedBy<UsuarioRepository>(),
                Component.For<ITerminalService>().ImplementedBy<TerminalService>(),
                Component.For<IGrupoUsuarioRepository>().ImplementedBy<GrupoUsuarioRepository>(),
                Component.For<IEventoUsuarioRepository>().ImplementedBy<EventoUsuarioRepository>(),
                Component.For<ILogUsuarioRepository>().ImplementedBy<LogUsuarioRepository>(),
                Component.For<ITokenAcessoRepository>().ImplementedBy<TokenAcessoRepository>(),
                Component.For<IMeuTranslogicRepository>().ImplementedBy<MeuTranslogicRepository>(),
                Component.For<ISessaoExpiradaRepository>().ImplementedBy<SessaoExpiradaRepository>(),
                Component.For<ControleAcessoService>(),
                Component.For<AutenticacaoWebService>(),
                Component.For<UsuarioService>(),
                Component.For<IControleAcessoService>()
                    .ImplementedBy<ControleAcessoService>()
                    .Named("controle-acesso-service"),
                Component.For<IPlanejamentoSerieLocomotivaRepository>()
                    .ImplementedBy<PlanejamentoSerieLocomotivaRepository>(),
                Component.For<IPlanejamentoQuantidadeLocomotivaRepository>()
                    .ImplementedBy<PlanejamentoQuantidadeLocomotivaRepository>(),
                Component.For<IDuracaoAtividadeEstacaoRepository>().ImplementedBy<DuracaoAtividadeEstacaoRepository>(),
                Component.For<IParadaOrdemServicoProgramadaRepository>()
                    .ImplementedBy<ParadaOrdemServicoProgramadaRepository>(),
                Component.For<ParadaOrdemServicoProgramadaService>()
                    .ImplementedBy<ParadaOrdemServicoProgramadaService>(),
                Component.For<IAtividadeParadaOrdemServicoProgramadaRepository>()
                    .ImplementedBy<AtividadeParadaOrdemServicoProgramadaRepository>(),
                Component.For<ISerieLocomotivaRepository>().ImplementedBy<SerieLocomotivaRepository>());

            container.Register(
                Component.For<IEmpresaRepository>().ImplementedBy<EmpresaRepository>(),
                Component.For<IEmpresaClienteRepository>().ImplementedBy<EmpresaClienteRepository>(),
                Component.For<IEmpresaInterfaceCteRepository>().ImplementedBy<EmpresaInterfaceCteRepository>(),
                Component.For<IEmpresaFerroviaRepository>().ImplementedBy<EmpresaFerroviaRepository>(),
                Component.For<IEmpresaClienteRegulatorioRepository>()
                    .ImplementedBy<EmpresaClienteRegulatorioRepository>());

            container.Register(Component.For<EmpresaClienteRegulatorioService>());

            container.Register(
                Component.For<IOrdemServicoRepository>().ImplementedBy<OrdemServicoRepository>(),
                Component.For<ICentroTremService>().ImplementedBy<CentroTremService>().Named("core.centrotremservice"));

            container.Register(
                Component.For<IComposicaoRepository>().ImplementedBy<ComposicaoRepository>(),
                Component.For<IComposicaoVagaoRepository>().ImplementedBy<ComposicaoVagaoRepository>(),
                Component.For<ISerieVagaoRepository>().ImplementedBy<SerieVagaoRepository>(),
                Component.For<IComposicaoLocomotivaRepository>().ImplementedBy<ComposicaoLocomotivaRepository>(),
                Component.For<IComposicaoMaquinistaRepository>().ImplementedBy<ComposicaoMaquinistaRepository>(),
                Component.For<IComposicaoEquipamentoRepository>().ImplementedBy<ComposicaoEquipamentoRepository>(),
                Component.For<IEventoLocomotivaRepository>().ImplementedBy<EventoLocomotivaRepository>(),
                Component.For<ITrechoTkbRepository>().ImplementedBy<TrechoTkbRepository>(),
                Component.For<TrechoTkbService>().ImplementedBy<TrechoTkbService>());

            container.Register(
                Component.For<IRotaRepository>().ImplementedBy<RotaRepository>(),
                Component.For<IUnidadeProducaoRepository>().ImplementedBy<UnidadeProducaoRepository>(),
                Component.For<IAreaOperacionalRepository>().ImplementedBy<AreaOperacionalRepository>(),
                Component.For<IEstacaoMaeRepository>().ImplementedBy<EstacaoMaeRepository>(),
                Component.For<ISituacaoOrdemServicoRepository>().ImplementedBy<SituacaoOrdemServicoRepository>(),
                Component.For<ITremRepository>().ImplementedBy<TremRepository>(),
                Component.For<ITremRapidoRepository>().ImplementedBy<TremRapidoRepository>(),
                Component.For<IDesempenhoCausaRepository>().ImplementedBy<DesempenhoCausaRepository>(),
                Component.For<IMovimentacaoTremRepository>().ImplementedBy<MovimentacaoTremRepository>(),
                Component.For<IMovimentacaoTremRealRepository>().ImplementedBy<MovimentacaoTremRealRepository>(),
                Component.For<ILocomotivaRepository>().ImplementedBy<Translogic.Modules.Core.Domain.DataAccess.Repositories.Trem.Veiculo.Locomotiva.LocomotivaRepository>(),
                Component.For<ISituacaoComandoLocomotivaRepository>()
                    .ImplementedBy<SituacaoComandoLocomotivaRepository>(),
                Component.For<IVagaoRepository>().ImplementedBy<VagaoRepository>(),
                Component.For<IVagaoLimitePesoRepository>().ImplementedBy<VagaoLimitePesoRepository>(),
                Component.For<IVagaoLimitePesoVigenteRepository>().ImplementedBy<VagaoLimitePesoVigenteRepository>(),
                Component.For<ISituacaoTracaoLocomotivaRepository>().ImplementedBy<SituacaoTracaoLocomotivaRepository>(),
                Component.For<ISituacaoTracaoLocomotivaVigenteRepository>()
                    .ImplementedBy<SituacaoTracaoLocomotivaVigenteRepository>(),
                Component.For<ISituacaoPosicaoLocomotivaRepository>()
                    .ImplementedBy<SituacaoPosicaoLocomotivaRepository>(),
                Component.For<ISituacaoPosicaoLocomotivaVigenteRepository>()
                    .ImplementedBy<SituacaoPosicaoLocomotivaVigenteRepository>(),
                Component.For<ILocalizacaoRepository>()
                    .ImplementedBy<LocalizacaoRepository>());

            container.Register(Component.For<NfePetrobrasService>());

            container.Register(Component.For<ComposicaoService>());

            container.Register(Component.For<ISmsRepository>().ImplementedBy<SmsRepository>());

            container.Register(Component.For<ManutencaoDespachoService>().ImplementedBy<ManutencaoDespachoService>());

            container.Register(Component.For<CteService>().ImplementedBy<CteService>());
            container.Register(Component.For<DclService>().ImplementedBy<DclService>());
            container.Register(Component.For<LaudoMercadoriaService>().ImplementedBy<LaudoMercadoriaService>());
            container.Register(Component.For<ILaudoMercadoriaEmpresaLogoRepository>().ImplementedBy<LaudoMercadoriaEmpresaLogoRepository>());

            container.Register(Component.For<RefaturamentoService>().ImplementedBy<RefaturamentoService>());

            container.Register(Component.For<NfeBoService>().ImplementedBy<NfeBoService>());
            container.Register(Component.For<INfePoolingRepository>().ImplementedBy<NfePoolingRepository>());

            container.Register(
                Component.For<IEstacaoMaeService>().ImplementedBy<EstacaoMaeService>(),
                Component.For<IAssociaFluxoRepository>().ImplementedBy<AssociaFluxoRepository>(),
                Component.For<IAssociaFluxoVigenteRepository>().ImplementedBy<AssociaFluxoVigenteRepository>(),
                Component.For<IBaldeioRepository>().ImplementedBy<BaldeioRepository>(),
                Component.For<ICarregamentoRepository>().ImplementedBy<CarregamentoRepository>(),
                Component.For<IDescarregamentoRepository>().ImplementedBy<DescarregamentoRepository>(),
                Component.For<IDespachoTranslogicRepository>().ImplementedBy<DespachoTranslogicRepository>(),
                Component.For<IDetalheCarregamentoRepository>().ImplementedBy<DetalheCarregamentoRepository>(),
                Component.For<IDetalheIntercambioRepository>().ImplementedBy<DetalheIntercambioRepository>(),
                Component.For<IGrupoDespachoRepository>().ImplementedBy<GrupoDespachoRepository>(),
                Component.For<IItemDespachoRepository>().ImplementedBy<ItemDespachoRepository>(),
                Component.For<INotaFiscalTranslogicRepository>().ImplementedBy<NotaFiscalTranslogicRepository>(),
                Component.For<IPedidoDerivadoRepository>().ImplementedBy<PedidoDerivadoRepository>(),
                Component.For<IRegistroIntercambioRepository>().ImplementedBy<RegistroIntercambioRepository>(),
                Component.For<ISerieDespachoRepository>().ImplementedBy<SerieDespachoRepository>(),
                Component.For<IVagaoPedidoRepository>().ImplementedBy<VagaoPedidoRepository>(),
                Component.For<IVagaoPedidoVigenteRepository>().ImplementedBy<VagaoPedidoVigenteRepository>(),
                Component.For<IFluxoInternacionalRepository>().ImplementedBy<FluxoInternacionalRepository>(),
                Component.For<ISerieDespachoUfRepository>().ImplementedBy<SerieDespachoUfRepository>(),
                Component.For<IPedidoTransporteRepository>().ImplementedBy<PedidoTransporteRepository>(),
                Component.For<IPedidoRealizadoRepository>().ImplementedBy<PedidoRealizadoRepository>(),
                Component.For<IEmpresaMargemLiberacaoFatRepository>()
                    .ImplementedBy<EmpresaMargemLiberacaoFatRepository>(),
                Component.For<IPedidoRepository>().ImplementedBy<PedidoRepository>());

            container.Register(Component.For<IIntegracaoAppaRepository>().ImplementedBy<IntegracaoAppaRepository>());
            container.Register(Component.For<IDespachoLocalBloqueioRepository>().ImplementedBy<DespachoLocalBloqueioRepository>());
            container.Register(Component.For<IClienteTipoIntegracaoRepository>().ImplementedBy<ClienteTipoIntegracaoRepository>());
            container.Register(Component.For<IAgrupamentoUsuarioRepository>().ImplementedBy<AgrupamentoUsuarioRepository>());
            container.Register(Component.For<IFluxoComercialRepository>().ImplementedBy<FluxoComercialRepository>());
            container.Register(Component.For<IAtividadeRepository>().ImplementedBy<AtividadeRepository>());


            //container.Register(Component.For<IOperacaoMalhaRepository>().ImplementedBy<OperacaoMalhaRepository>());


            // Alguem colocou o register do MalhaRepository (que � do Modules.Core) em outro Installer e 
            // n�o estou conseguindo localizar qual installer esta o MalhaRepository.
            // Preciso por o if DEBUG pois para executar o teste do Job.Runner localmente e est� dando erro 
            // dizendo que preciso do register.
#if DEBUG
            //container.Register(Component.For<IMalhaRepository>().ImplementedBy<MalhaRepository>());
#endif
            try
            {
                // container.Register(Component.For<IMalhaRepository>().ImplementedBy<MalhaRepository>());
            }
            catch { }

            container.Register(Component.For<IPainelExpedicaoRepository>().ImplementedBy<PainelExpedicaoRepository>(),
                Component.For<IOrigemRepository>().ImplementedBy<OrigemRepository>(),
                Component.For<IPainelExpedicaoIndicadoresRepository>().ImplementedBy<PainelExpedicaoIndicadoresRepository>(),
                //Component.For<IOperacaoMalhaUFRepository>().ImplementedBy<OperacaoMalhaUFRepository>(),
                //Component.For<IRegiaoRepository>().ImplementedBy<RegiaoRepository>(),
                //Component.For<IRegraEmailsRepository>().ImplementedBy<RegraEmailsRepository>(),
                Component.For<PainelExpedicaoService>());

            container.Register(Component.For<IntegracaoAppaService>());
            container.Register(Component.For<ICadCorredorRepository>().ImplementedBy<CadCorredorRepository>());
            container.Register(Component.For<ISituacaoRepository>().ImplementedBy<SituacaoRepository>());

            container.Register(Component.For<IEdiErroNfRepository>().ImplementedBy<EdiErroNfRepository>());
            container.Register(Component.For<IEdiErroRepository>().ImplementedBy<EdiErroRepository>(),
                                    Component.For<EdiErroService>());

            container.Register(
                Component.For<IBolarBoRepository>().ImplementedBy<BolarBoRepository>(),
                Component.For<IBolarVagaoRepository>().ImplementedBy<BolarVagaoRepository>(),
                Component.For<IBolarNfRepository>().ImplementedBy<BolarNfRepository>(),
                Component.For<IBolarCteRepository>().ImplementedBy<BolarCteRepository>(),
                Component.For<IBolarBoBolarCteRepository>().ImplementedBy<BolarBoBolarCteRepository>(),
                Component.For<IBoBolarControleRepository>().ImplementedBy<BoBolarControleRepository>());

            container.Register(
                Component.For<IMenuSivRepository>().ImplementedBy<MenuSivRepository>(),
                Component.For<ISubMenuSivRepository>().ImplementedBy<SubMenuSivRepository>(),
                Component.For<IItemMenuSivRepository>().ImplementedBy<ItemMenuSivRepository>(),
                Component.For<IDivisionSivRepository>().ImplementedBy<DivisionSivRepository>());

            container.Register(
                Component.For<ILocalTaraPlacaRepository>().ImplementedBy<LocalTaraPlacaRepository>(),
                Component.For<IElementoViaRepository>().ImplementedBy<ElementoViaRepository>(),
                Component.For<IVagaoPatioRepository>().ImplementedBy<VagaoPatioRepository>(),
                Component.For<IVagaoPatioVigenteRepository>().ImplementedBy<VagaoPatioVigenteRepository>(),
                Component.For<IDclTempRepository>().ImplementedBy<DclTempRepository>(),
                Component.For<IDclTempVagaoRepository>().ImplementedBy<DclTempVagaoRepository>(),
                Component.For<IDclTempVagaoConteinerRepository>().ImplementedBy<DclTempVagaoConteinerRepository>(),
                Component.For<IDclTempVagaoNotaFiscalRepository>().ImplementedBy<DclTempVagaoNotaFiscalRepository>(),
                Component.For<IDclErroGravacaoRepository>().ImplementedBy<DclErroGravacaoRepository>(),
                Component.For<IGrupoFluxoComercialRepository>().ImplementedBy<GrupoFluxoComercialRepository>(),
                Component.For<IAssociaFluxoInternacionalRepository>()
                    .ImplementedBy<AssociaFluxoInternacionalRepository>(),
                Component.For<NfeService>(),
                Component.For<NfePdfService>(),
                Component.For<EdiReadNfeService>(),
                Component.For<NfeConfiguracaoEmpresaService>(),
                Component.For<NfeAnulacaoService>(),
                Component.For<NfeInLoteService>(),
                Component.For<BaixarNfeService>(),
                Component.For<CarregamentoService>(),
                Component.For<DescarregamentoService>(),
                Component.For<AnulacaoService>(),
                Component.For<NotaFiscalService>(),
                Component.For<VagaoLimitePesoService>(),
                Component.For<RelatorioAmaggiService>());

            container.Register(
                Component.For<INfeSimconsultasRepository>().ImplementedBy<NfeSimconsultasRepository>(),
                Component.For<INfeProdutoSimconsultasRepository>().ImplementedBy<NfeProdutoSimconsultasRepository>(),
                Component.For<INfeReadonlyRepository>().ImplementedBy<NfeReadonlyRepository>());

            container.Register(
                Component.For<INfeAnulacaoSimconsultasRepository>().ImplementedBy<NfeAnulacaoSimconsultasRepository>(),
                Component.For<INfeAnulacaoProdutoSimconsultasRepository>()
                    .ImplementedBy<NfeAnulacaoProdutoSimconsultasRepository>(),
                Component.For<IStatusNfeAnulacaoRepository>().ImplementedBy<StatusNfeAnulacaoRepository>());

            container.Register(
                Component.For<IEstadoRepository>().ImplementedBy<EstadoRepository>(),
                Component.For<IMunicipioRepository>().ImplementedBy<MunicipioRepository>());

            container.Register(Component.For<IRelatorioAmaggiRepository>().ImplementedBy<RelatorioAmaggiRepository>());

            container.Register(
                Component.For<ICteStatusRetornoRepository>().ImplementedBy<CteStatusRetornoRepository>(),
                Component.For<ICteStatusRepository>().ImplementedBy<CteStatusRepository>(),
                Component.For<ICteAgrupamentoRepository>().ImplementedBy<CteAgrupamentoRepository>(),
                Component.For<ICteRepository>().ImplementedBy<CteRepository>(),
                Component.For<ICteVersaoRepository>().ImplementedBy<CteVersaoRepository>(),
                Component.For<ICteOrigemRepository>().ImplementedBy<CteOrigemRepository>(),
                Component.For<ICteTempRepository>().ImplementedBy<CteTempRepository>(),
                Component.For<ICteDetalheRepository>().ImplementedBy<CteDetalheRepository>(),
                Component.For<ICorrecaoCteRepository>().ImplementedBy<CorrecaoCteRepository>(),
                Component.For<ICteAnuladoRepository>().ImplementedBy<CteAnuladoRepository>(),
                Component.For<ICorrecaoConteinerCteRepository>().ImplementedBy<CorrecaoConteinerCteRepository>(),
                Component.For<ICteComplementadoRepository>().ImplementedBy<CteComplementadoRepository>(),
                Component.For<IUfIbgeRepository>().ImplementedBy<UfIbgeRepository>(),
                Component.For<ICodigoBarrasRepository>().ImplementedBy<CodigoBarrasRepository>(),
                Component.For<IPaisBacenRepository>().ImplementedBy<PaisBacenRepository>(),
                Component.For<IComposicaoFreteContratoRepository>().ImplementedBy<ComposicaoFreteContratoRepository>(),
                Component.For<ICteArvoreRepository>().ImplementedBy<CteArvoreRepository>(),
                Component.For<ICteEmpresasRepository>().ImplementedBy<CteEmpresasRepository>(),
                Component.For<ICteLogRepository>().ImplementedBy<CteLogRepository>(),
                Component.For<ICteEnvioPoolingRepository>().ImplementedBy<CteEnvioPoolingRepository>(),
                Component.For<ICteRecebimentoPoolingRepository>().ImplementedBy<CteRecebimentoPoolingRepository>(),
                Component.For<ITbDatabaseInputRepository>()
                    .ImplementedBy<TbDatabaseInputRepository>()
                    .DependsOn(new { Alias = "NDD" }),
                ////Component.For<ICteRunnerLogRepository>().ImplementedBy<CteRunnerLogRepository>(),
                Component.For<ICteInterfaceRecebimentoConfigRepository>()
                    .ImplementedBy<CteInterfaceRecebimentoConfigRepository>(),
                Component.For<ICteInterfaceEnvioSapRepository>().ImplementedBy<CteInterfaceEnvioSapRepository>(),
                Component.For<ICteArquivoRepository>().ImplementedBy<CteArquivoRepository>(),
                Component.For<ICteComunicadoRepository>().ImplementedBy<CteComunicadoRepository>(),
                Component.For<IRefaturamentoDestinoPermRepository>().ImplementedBy<RefaturamentoDestinoPermRepository>(),
                Component.For<ICteEnvioSeguradoraHistRepository>().ImplementedBy<CteEnvioSeguradoraHistRepository>());
            container.Register(
                Component.For<GerarArquivoCteEnvioService>(),
                Component.For<GerarArquivoCteCancelamentoService>(),
                Component.For<GerarArquivoCteInutilizacaoService>(),
                Component.For<CteLogService>(),
                Component.For<CodigoBarrasService>());

            container.Register(
                Component.For<ICondicaoUsoVagaoRepository>().ImplementedBy<CondicaoUsoVagaoRepository>(),
                Component.For<ICondicaoUsoVagaoVigenteRepository>().ImplementedBy<CondicaoUsoVagaoVigenteRepository>(),
                Component.For<IIntercambioVagaoRepository>().ImplementedBy<IntercambioVagaoRepository>(),
                Component.For<IIntercambioVagaoVigenteRepository>().ImplementedBy<IntercambioVagaoVigenteRepository>(),
                Component.For<ILocalizacaoVagaoRepository>().ImplementedBy<LocalizacaoVagaoRepository>(),
                Component.For<ILocalizacaoVagaoVigenteRepository>().ImplementedBy<LocalizacaoVagaoVigenteRepository>(),
                Component.For<ILotacaoVagaoRepository>().ImplementedBy<LotacaoVagaoRepository>(),
                Component.For<ILotacaoVagaoVigenteRepository>().ImplementedBy<LotacaoVagaoVigenteRepository>(),
                Component.For<IResponsavelVagaoRepository>().ImplementedBy<ResponsavelVagaoRepository>(),
                Component.For<IResponsavelVagaoVigenteRepository>().ImplementedBy<ResponsavelVagaoVigenteRepository>(),
                Component.For<ISituacaoVagaoRepository>().ImplementedBy<SituacaoVagaoRepository>(),
                Component.For<ISituacaoVagaoVigenteRepository>().ImplementedBy<SituacaoVagaoVigenteRepository>(),
                Component.For<IMargemTrechoRepository>().ImplementedBy<MargemTrechoRepository>(),
                Component.For<IVagoesTravadosAnxRepository>().ImplementedBy<VagoesTravadosAnxRepository>());

            container.Register(
                Component.For<IComposicaoVagaoVigenteRepository>().ImplementedBy<ComposicaoVagaoVigenteRepository>(),
                Component.For<IComposicaoLocomotivaVigenteRepository>()
                    .ImplementedBy<ComposicaoLocomotivaVigenteRepository>(),
                Component.For<IMercadoriaRepository>().ImplementedBy<MercadoriaRepository>(),
                Component.For<IConteinerRepository>().ImplementedBy<ConteinerRepository>(),
                Component.For<IVagaoConteinerRepository>().ImplementedBy<VagaoConteinerRepository>(),
                Component.For<IVagaoConteinerVigenteRepository>().ImplementedBy<VagaoConteinerVigenteRepository>(),
                Component.For<IDensidadeMercadoriaRepository>().ImplementedBy<DensidadeMercadoriaRepository>(),
                Component.For<INfeProdutoReadonlyRepository>().ImplementedBy<NfeProdutoReadonlyRepository>(),
                Component.For<ICteSerieNumeroEmpresaUfRepository>().ImplementedBy<CteSerieNumeroEmpresaUfRepository>(),
                Component.For<ICteArquivoFtpRepository>().ImplementedBy<CteArquivoFtpRepository>(),
                Component.For<IComposicaoFreteCteRepository>().ImplementedBy<ComposicaoFreteCteRepository>(),
                Component.For<ITempCompRepository>().ImplementedBy<TempCompRepository>(),
                Component.For<ICidadeIbgeRepository>().ImplementedBy<CidadeIbgeRepository>(),
                Component.For<ICotacaoRepository>().ImplementedBy<CotacaoRepository>(),
                Component.For<ISapDespCteRepository>().ImplementedBy<SapDespCteRepository>(),
                Component.For<ISapCteAnuladoRepository>().ImplementedBy<SapCteAnuladoRepository>(),
                Component.For<ISapNotasCteRepository>().ImplementedBy<SapNotasCteRepository>(),
                Component.For<ISapConteinerCteRepository>().ImplementedBy<SapConteinerCteRepository>(),
                Component.For<ISapDocsoutrasferCteRepository>().ImplementedBy<SapDocsoutrasferCteRepository>(),
                Component.For<ISapCteComplementadoRepository>().ImplementedBy<SapCteComplementadoRepository>(),
                Component.For<ISapVagaoCteRepository>().ImplementedBy<SapVagaoCteRepository>(),
                Component.For<ISapChaveCteRepository>().ImplementedBy<SapChaveCteRepository>(),
                Component.For<ISapComposicaoFreteCteRepository>().ImplementedBy<SapComposicaoFreteCteRepository>(),
                Component.For<ICteInterfacePdfConfigRepository>().ImplementedBy<CteInterfacePdfConfigRepository>(),
                Component.For<IVSapZSDV0203VRepository>().ImplementedBy<VSapZSDV0203VRepository>());

            container.Register(
                Component.For<TestarGravacaoConfigService>(),
                Component.For<GravarConfigCteEnvioService>(),
                Component.For<GravarConfigCorrecaoCteService>(),
                Component.For<GravarConfigCteCancelamentoService>(),
                Component.For<GravarConfigCteAnulacaoContribuinteService>(),
                Component.For<GravarConfigCteComplementoService>(),
                Component.For<GravarConfigCteInutilizacaoService>(),
                Component.For<GravarConfigCteAnulacaoNContribuinteService>(),
                Component.For<FilaProcessamentoService>(),
                Component.For<GravarSapCteEnvioService>(),
                Component.For<EfetuarLimpezaDiretorioCteService>(),
                Component.For<GerarArquivoCteComplementoService>());

            container.Register(
                Component.For<ICte01Repository>().ImplementedBy<Cte01Repository>().DependsOn(new { Alias = "CONFIG" }),
                Component.For<ICte02Repository>().ImplementedBy<Cte02Repository>().DependsOn(new { Alias = "CONFIG" }),
                Component.For<ICte03Repository>().ImplementedBy<Cte03Repository>().DependsOn(new { Alias = "CONFIG" }),
                Component.For<ICte04Repository>().ImplementedBy<Cte04Repository>().DependsOn(new { Alias = "CONFIG" }),
                Component.For<ICte05Repository>().ImplementedBy<Cte05Repository>().DependsOn(new { Alias = "CONFIG" }),
                Component.For<ICte06Repository>().ImplementedBy<Cte06Repository>().DependsOn(new { Alias = "CONFIG" }),
                Component.For<ICte07Repository>().ImplementedBy<Cte07Repository>().DependsOn(new { Alias = "CONFIG" }),
                Component.For<ICte08Repository>().ImplementedBy<Cte08Repository>().DependsOn(new { Alias = "CONFIG" }),
                Component.For<ICte09Repository>().ImplementedBy<Cte09Repository>().DependsOn(new { Alias = "CONFIG" }),
                Component.For<ICte10Repository>().ImplementedBy<Cte10Repository>().DependsOn(new { Alias = "CONFIG" }),
                Component.For<ICte11Repository>().ImplementedBy<Cte11Repository>().DependsOn(new { Alias = "CONFIG" }),
                Component.For<ICte12Repository>().ImplementedBy<Cte12Repository>().DependsOn(new { Alias = "CONFIG" }),
                Component.For<ICte13Repository>().ImplementedBy<Cte13Repository>().DependsOn(new { Alias = "CONFIG" }),
                Component.For<ICte14Repository>().ImplementedBy<Cte14Repository>().DependsOn(new { Alias = "CONFIG" }),
                Component.For<ICte15Repository>().ImplementedBy<Cte15Repository>().DependsOn(new { Alias = "CONFIG" }),

                //// n�o existe mais devido ao CTE3.0 - 7/04/2017
                ////Component.For<ICte16Repository>().ImplementedBy<Cte16Repository>().DependsOn(new { Alias = "CONFIG" }),

                Component.For<ICte17Repository>().ImplementedBy<Cte17Repository>().DependsOn(new { Alias = "CONFIG" }),
                Component.For<ICte23Repository>().ImplementedBy<Cte23Repository>().DependsOn(new { Alias = "CONFIG" }),
                Component.For<ICte24Repository>().ImplementedBy<Cte24Repository>().DependsOn(new { Alias = "CONFIG" }),
                Component.For<ICte32Repository>().ImplementedBy<Cte32Repository>().DependsOn(new { Alias = "CONFIG" }),

                //// n�o existe mais devido ao CTE3.0 - 7/04/2017  
                ////Component.For<ICte33Repository>().ImplementedBy<Cte33Repository>().DependsOn(new { Alias = "CONFIG" }),

                Component.For<ICte34Repository>().ImplementedBy<Cte34Repository>().DependsOn(new { Alias = "CONFIG" }),
                Component.For<ICte35Repository>().ImplementedBy<Cte35Repository>().DependsOn(new { Alias = "CONFIG" }),
                Component.For<ICte36Repository>().ImplementedBy<Cte36Repository>().DependsOn(new { Alias = "CONFIG" }),
                Component.For<ICte37Repository>().ImplementedBy<Cte37Repository>().DependsOn(new { Alias = "CONFIG" }),
                Component.For<ICte38Repository>().ImplementedBy<Cte38Repository>().DependsOn(new { Alias = "CONFIG" }),
                Component.For<ICte50Repository>().ImplementedBy<Cte50Repository>().DependsOn(new { Alias = "CONFIG" }),
                Component.For<ICte51Repository>().ImplementedBy<Cte51Repository>().DependsOn(new { Alias = "CONFIG" }),
                Component.For<ICte60Repository>().ImplementedBy<Cte60Repository>().DependsOn(new { Alias = "CONFIG" }),
                Component.For<ICte61Repository>().ImplementedBy<Cte61Repository>().DependsOn(new { Alias = "CONFIG" }),
                Component.For<ICte62Repository>().ImplementedBy<Cte62Repository>().DependsOn(new { Alias = "CONFIG" }),
                Component.For<ICte67Repository>().ImplementedBy<Cte67Repository>().DependsOn(new { Alias = "CONFIG" }),
                Component.For<ICte63Repository>().ImplementedBy<Cte63Repository>().DependsOn(new { Alias = "CONFIG" }),
                Component.For<ICte64Repository>().ImplementedBy<Cte64Repository>().DependsOn(new { Alias = "CONFIG" }),
                Component.For<ICte66Repository>().ImplementedBy<Cte66Repository>().DependsOn(new { Alias = "CONFIG" }),
                Component.For<ICte68Repository>().ImplementedBy<Cte68Repository>().DependsOn(new { Alias = "CONFIG" }),
                Component.For<ICte69Repository>().ImplementedBy<Cte69Repository>().DependsOn(new { Alias = "CONFIG" }),
                Component.For<ICte70Repository>().ImplementedBy<Cte70Repository>().DependsOn(new { Alias = "CONFIG" }),
                Component.For<ICte100Repository>().ImplementedBy<Cte100Repository>().DependsOn(new { Alias = "CONFIG" }),
                Component.For<ICte01ListaRepository>()
                    .ImplementedBy<Cte01ListaRepository>()
                    .DependsOn(new { Alias = "CONFIG" }),
                Component.For<INfe19ErroClassRepository>()
                    .ImplementedBy<Nfe19ErroClassRepository>()
                    .DependsOn(new { Alias = "CONFIG" }),
                Component.For<INfe20ErroLogRepository>()
                    .ImplementedBy<Nfe20ErroLogRepository>()
                    .DependsOn(new { Alias = "CONFIG" }),
                Component.For<IVw209ConsultaCteRepository>()
                    .ImplementedBy<Vw209ConsultaCteRepository>()
                    .DependsOn(new { Alias = "CONFIG" }),
                Component.For<INfe03FilialRepository>()
                    .ImplementedBy<Nfe03FilialRepository>()
                    .DependsOn(new { Alias = "CONFIG" }),
                Component.For<ICte101Repository>().ImplementedBy<Cte101Repository>().DependsOn(new { Alias = "CONFIG" }),
                Component.For<ICte102Repository>().ImplementedBy<Cte102Repository>().DependsOn(new { Alias = "CONFIG" }),
                Component.For<ICte103Repository>().ImplementedBy<Cte103Repository>().DependsOn(new { Alias = "CONFIG" }),
                Component.For<ICte105Repository>().ImplementedBy<Cte105Repository>().DependsOn(new { Alias = "CONFIG" }),
                Component.For<ICte107Repository>().ImplementedBy<Cte107Repository>().DependsOn(new { Alias = "CONFIG" }),
                Component.For<ICte109Repository>().ImplementedBy<Cte109Repository>().DependsOn(new { Alias = "CONFIG" }),
                Component.For<ICte111Repository>().ImplementedBy<Cte111Repository>().DependsOn(new { Alias = "CONFIG" }),
                Component.For<ICte113Repository>().ImplementedBy<Cte113Repository>().DependsOn(new { Alias = "CONFIG" }),
                Component.For<ICte115Repository>().ImplementedBy<Cte115Repository>().DependsOn(new { Alias = "CONFIG" }),
                Component.For<ICte117Repository>().ImplementedBy<Cte117Repository>().DependsOn(new { Alias = "CONFIG" }),
                Component.For<ICte119Repository>().ImplementedBy<Cte119Repository>().DependsOn(new { Alias = "CONFIG" }),
                Component.For<ICte121Repository>().ImplementedBy<Cte121Repository>().DependsOn(new { Alias = "CONFIG" }));

            container.Register(Component.For<ICteTempDetalheRepository>().ImplementedBy<CteTempDetalheRepository>());

            container.Register(
                Component.For<IEmpresaClientePaisRepository>().ImplementedBy<EmpresaClientePaisRepository>());

            container.Register(Component.For<UnidadeProducaoService>());

            container.Register(Component.For<ILogSimconsultasRepository>().ImplementedBy<LogSimconsultasRepository>());
            container.Register(Component.For<ICfopAgrupamentoRepository>().ImplementedBy<CfopAgrupamentoRepository>());
            container.Register(
                Component.For<IConteinerLocalVigenteRepository>().ImplementedBy<ConteinerLocalVigenteRepository>());

            container.Register(
                Component.For<ILogTempoCarregamentoRepository>().ImplementedBy<LogTempoCarregamentoRepository>(),
                Component.For<ILogTempoCarregamentoVagaoRepository>()
                    .ImplementedBy<LogTempoCarregamentoVagaoRepository>(),
                Component.For<ILogTempoCarregamentoVagaoNotaFiscalRepository>()
                    .ImplementedBy<LogTempoCarregamentoVagaoNotaFiscalRepository>(),
                Component.For<ILogCarregamentoNfeRepository>().ImplementedBy<LogCarregamentoNfeRepository>());

            container.Register(
                Component.For<IStatusRetornoNfeRepository>().ImplementedBy<StatusRetornoNfeRepository>(),
                Component.For<IStatusNfeRepository>().ImplementedBy<StatusNfeRepository>(),
                Component.For<IStatusNfeLogRepository>().ImplementedBy<StatusNfeLogRepository>(),
                Component.For<IHistoricoNfeRepository>().ImplementedBy<HistoricoNfeRepository>(),
                Component.For<ILogStatusNfeRepository>().ImplementedBy<LogStatusNfeRepository>(),
                Component.For<IAssociaNotaFiscalStatusNfeRepository>()
                    .ImplementedBy<AssociaNotaFiscalStatusNfeRepository>(),
                Component.For<ICteSapPoolingRepository>().ImplementedBy<CteSapPoolingRepository>(),
                Component.For<ICteSapHistoricoRepository>().ImplementedBy<CteSapHistoricoRepository>(),
                Component.For<ICteEnvioXmlRepository>().ImplementedBy<CteEnvioXmlRepository>());

            container.Register(
                Component.For<IFluxoExcluirPortoferRepository>().ImplementedBy<FluxoExcluirPortoferRepository>());
            container.Register(Component.For<IFluxoExcluirCteRepository>().ImplementedBy<FluxoExcluirCteRepository>());

            container.Register(
                Component.For<INfeConfiguracaoEmpresaUnidadeMedidaRepository>()
                    .ImplementedBy<NfeConfiguracaoEmpresaUnidadeMedidaRepository>(),
                Component.For<INfeConfiguracaoEmpresaRepository>().ImplementedBy<NfeConfiguracaoEmpresaRepository>(),
                Component.For<ICteInterfaceEnvioEmailRepository>().ImplementedBy<CteInterfaceEnvioEmailRepository>(),
                Component.For<ICteEnvioEmailHistoricoRepository>().ImplementedBy<CteEnvioEmailHistoricoRepository>(),
                Component.For<ICteInterfaceXmlConfigRepository>().ImplementedBy<CteInterfaceXmlConfigRepository>(),
                Component.For<ICteEnvioEmailErroRepository>().ImplementedBy<CteEnvioEmailErroRepository>());

            container.Register(
                Component.For<ICteMotivoCancelamentoRepository>().ImplementedBy<CteMotivoCancelamentoRepository>());
            container.Register(Component.For<IContratoRepository>().ImplementedBy<ContratoRepository>());
            container.Register(
                Component.For<IContratoHistoricoRepository>().ImplementedBy<ContratoHistoricoRepository>());

            container.Register(
                Component.For<IUsuarioConfiguracaoRepository>().ImplementedBy<UsuarioConfiguracaoRepository>());

            container.Register(
                    Component.For<IModalSeguroRepository>().ImplementedBy<ModalSeguroRepository>());
            container.Register(
                    Component.For<IProcessoSeguroCacheRepository>().ImplementedBy<ProcessoSeguroCacheRepository>());
            container.Register(Component.For<ProcessoSeguroCacheService>().ImplementedBy<ProcessoSeguroCacheService>());
            container.Register(
                    Component.For<IUnidadeSeguroRepository>().ImplementedBy<UnidadeSeguroRepository>());
            container.Register(Component.For<UnidadeSeguroService>().ImplementedBy<UnidadeSeguroService>());
            container.Register(
                    Component.For<ICausaSeguroRepository>().ImplementedBy<CausaSeguroRepository>());
            container.Register(Component.For<CausaSeguroService>().ImplementedBy<CausaSeguroService>());
            container.Register(
                    Component.For<IRateioSeguroRepository>().ImplementedBy<RateioSeguroRepository>());
            container.Register(
                    Component.For<IAnaliseSeguroRepository>().ImplementedBy<AnaliseSeguroRepository>());
            container.Register(
                    Component.For<IContaContabilSeguroRepository>().ImplementedBy<ContaContabilSeguroRepository>());
            container.Register(
                    Component.For<IHistoricoSeguroRepository>().ImplementedBy<HistoricoSeguroRepository>());
            container.Register(
                    Component.For<IClienteSeguroRepository>().ImplementedBy<ClienteSeguroRepository>());
            container.Register(Component.For<ClienteSeguroService>().ImplementedBy<ClienteSeguroService>());

            container.Register(Component.For<IBolarBoImpCfgRepository>().ImplementedBy<BolarBoImpCfgRepository>());

            container.Register(Component.For<IEmailEnvioDocumentacaoRepository>().ImplementedBy<EmailEnvioDocumentacaoRepository>());

            container.Register(
                Component.For<IConfiguracaoEnvioDocumentacaoRepository>().ImplementedBy<ConfiguracaoEnvioDocumentacaoRepository>(),
                Component.For<ConfiguracaoEnvioDocumentacaoService>().ImplementedBy<ConfiguracaoEnvioDocumentacaoService>());

            container.Register(
                Component.For<IConfGeralRepository>().ImplementedBy<ConfGeralRepository>(),
                Component.For<ConfGeralService>().ImplementedBy<ConfGeralService>(),
                Component.For<ILogConfGeralRepository>().ImplementedBy<LogConfGeralRepository>());

            container.Register(Component.For<IHistoricoEmailEnvioDocumentacaoRepository>().ImplementedBy<HistoricoEmailEnvioDocumentacaoRepository>());

            container.Register(
                Component.For<IHistoricoEnvioDocumentacaoRepository>().ImplementedBy<HistoricoEnvioDocumentacaoRepository>(),
                Component.For<HistoricoEnvioDocumentacaoService>().ImplementedBy<HistoricoEnvioDocumentacaoService>());

            container.Register(
                Component.For<IGerenciamentoDocumentacaoRepository>().ImplementedBy<GerenciamentoDocumentacaoRepository>(),
                Component.For<IGerenciamentoDocumentacaoCompVagaoRepository>().ImplementedBy<GerenciamentoDocumentacaoCompVagaoRepository>(),
                Component.For<IGerenciamentoDocumentacaoRefaturamentoRepository>().ImplementedBy<GerenciamentoDocumentacaoRefaturamentoRepository>(),
                Component.For<IGerenciamentoDocumentacaoRefaturamentoVagaoRepository>().ImplementedBy<GerenciamentoDocumentacaoRefaturamentoVagaoRepository>(),
                Component.For<GerenciamentoDocumentacaoRefaturamentoService>().ImplementedBy<GerenciamentoDocumentacaoRefaturamentoService>());

            container.Register(
                Component.For<IDocumentacaoMotivoRefaturamentoRepository>().ImplementedBy<DocumentacaoMotivoRefaturamentoRepository>(),
                Component.For<DocumentacaoMotivoRefaturamentoService>().ImplementedBy<DocumentacaoMotivoRefaturamentoService>());

            container.Register(Component.For<IDocumentacaoResponsavelRefaturamentoRepository>().ImplementedBy<DocumentacaoResponsavelRefaturamentoRepository>());

            container.Register(Component.For<GravarConfigDadosEnvioMdfeService>());

            container.Register(
                Component.For<IMdfe01ListaRepository>()
                    .ImplementedBy<Mdfe01ListaRepository>()
                    .DependsOn(new { Alias = "CONFIG" }),
                Component.For<IMdfe01InfRepository>()
                    .ImplementedBy<Mdfe01InfRepository>()
                    .DependsOn(new { Alias = "CONFIG" }),
                Component.For<IMdfe02InfMunCarregaRepository>()
                    .ImplementedBy<Mdfe02InfMunCarregaRepository>()
                    .DependsOn(new { Alias = "CONFIG" }),
                Component.For<IMdfe03InfPercursoRepository>()
                    .ImplementedBy<Mdfe03InfPercursoRepository>()
                    .DependsOn(new { Alias = "CONFIG" }),
                Component.For<IMdfe09FerroRepository>()
                    .ImplementedBy<Mdfe09FerroRepository>()
                    .DependsOn(new { Alias = "CONFIG" }),
                Component.For<IMdfe10FerroVagRepository>()
                    .ImplementedBy<Mdfe10FerroVagRepository>()
                    .DependsOn(new { Alias = "CONFIG" }),
                Component.For<IMdfe11InfDocRepository>()
                    .ImplementedBy<Mdfe11InfDocRepository>()
                    .DependsOn(new { Alias = "CONFIG" }),
                Component.For<IMdfe12InfCteRepository>()
                    .ImplementedBy<Mdfe12InfCteRepository>()
                    .DependsOn(new { Alias = "CONFIG" }),
                Component.For<IMdfe13InfCtRepository>()
                    .ImplementedBy<Mdfe13InfCtRepository>()
                    .DependsOn(new { Alias = "CONFIG" }),
                Component.For<IMdfe14InfNfeRepository>()
                    .ImplementedBy<Mdfe14InfNfeRepository>()
                    .DependsOn(new { Alias = "CONFIG" }),
                Component.For<IMdfe15InfNfRepository>()
                    .ImplementedBy<Mdfe15InfNfRepository>()
                    .DependsOn(new { Alias = "CONFIG" }),
                Component.For<IMdfe16LacresRepository>()
                    .ImplementedBy<Mdfe16LacresRepository>()
                    .DependsOn(new { Alias = "CONFIG" }),
                Component.For<IMdfe50ImpRepository>()
                    .ImplementedBy<Mdfe50ImpRepository>()
                    .DependsOn(new { Alias = "CONFIG" }),
                Component.For<IVw509ConsultaMdfeRepository>()
                    .ImplementedBy<Vw509ConsultaMdfeRepository>()
                    .DependsOn(new { Alias = "CONFIG" }),
                Component.For<IMdfe06AgrupImpRepository>()
                    .ImplementedBy<Mdfe06AgrupImpRepository>()
                    .DependsOn(new { Alias = "CONFIG" }));

            container.Register(
                Component.For<IMdfeRepository>().ImplementedBy<MdfeRepository>(),
                Component.For<IMdfeArvoreRepository>().ImplementedBy<MdfeArvoreRepository>(),
                Component.For<IMdfeStatusRepository>().ImplementedBy<MdfeStatusRepository>(),
                Component.For<IMdfeStatusRetornoRepository>().ImplementedBy<MdfeStatusRetornoRepository>(),
                Component.For<IMdfeVersaoRepository>().ImplementedBy<MdfeVersaoRepository>(),
                Component.For<IMdfeRecebimentoPoolingRepository>().ImplementedBy<MdfeRecebimentoPoolingRepository>(),
                Component.For<IMdfeEnvioPoolingRepository>().ImplementedBy<MdfeEnvioPoolingRepository>(),
                Component.For<IMdfeInterfaceRecebimentoConfigRepository>()
                    .ImplementedBy<MdfeInterfaceRecebimentoConfigRepository>(),
                Component.For<IMdfeLogRepository>().ImplementedBy<MdfeLogRepository>(),
                Component.For<IMdfeRunnerLogRepository>().ImplementedBy<MdfeRunnerLogRepository>(),
                Component.For<IMdfeComposicaoRepository>().ImplementedBy<MdfeComposicaoRepository>(),
                Component.For<IMdfeEstacaoGeracaoRepository>().ImplementedBy<MdfeEstacaoGeracaoRepository>(),
                Component.For<IMdfeArquivoRepository>().ImplementedBy<MdfeArquivoRepository>(),
                Component.For<IMdfeInterfacePdfConfigRepository>().ImplementedBy<MdfeInterfacePdfConfigRepository>(),
                Component.For<IMdfeSerieEmpresaUfRepository>().ImplementedBy<MdfeSerieEmpresaUfRepository>(),
                Component.For<MdfeService>().ImplementedBy<MdfeService>(),
                Component.For<MdfeLogService>().ImplementedBy<MdfeLogService>(),
                Component.For<FilaProcessamentoMdfeService>(),
                Component.For<GravarConfigDadosCancelamentoMdfeService>(),
                Component.For<ILogImpressaoReciboRepository>().ImplementedBy<LogImpressaoReciboRepository>());

            container.Register(
                Component.For<ICteDocumentoRemetenteSimconsultasRepository>()
                    .ImplementedBy<CteDocumentoRemetenteSimconsultasRepository>(),
                Component.For<ICteEmpresaSimconsultasRepository>().ImplementedBy<CteEmpresaSimconsultasRepository>(),
                Component.For<ICteComponentePrestacaoSimconsultasRepository>()
                    .ImplementedBy<CteComponentePrestacaoSimconsultasRepository>(),
                Component.For<ICteSimconsultasRepository>().ImplementedBy<CteSimconsultasRepository>(),
                Component.For<ICteConfigEmailRepository>().ImplementedBy<CteConfigEmailRepository>(),
                Component.For<CteSimconsultasService>());

            container.Register(
                Component.For<IGestaoDocumentosService>()
                    .ImplementedBy<GestaoDocumentosService>()
                    .Named("SOA.GestaoDocumentos"),
                Component.For<RelatorioDocumentosService>().ImplementedBy<RelatorioDocumentosService>(),
                Component.For<ITicketBalancaRepository>().ImplementedBy<TicketBalancaRepository>(),
                Component.For<IVagaoTicketRepository>().ImplementedBy<VagaoTicketRepository>(),
                Component.For<INotaTicketVagaoRepository>().ImplementedBy<NotaTicketVagaoRepository>(),
                Component.For<IWsTicketControleRecebimentoRepository>()
                    .ImplementedBy<WsTicketControleRecebimentoRepository>());

            container.Register(
                Component.For<ISispatRepository>().ImplementedBy<SispatRepository>().DependsOn(new { Alias = "SISPAT" }),
                Component.For<IDadosFiscaisService>().ImplementedBy<DadosFiscaisService>(),
                Component.For<IWsTicketLogRecebimentoRepository>().ImplementedBy<WsTicketLogRecebimentoRepository>());

            container.Register(Component.For<OnTimeService>());

            container.Register(
                Component.For<IAvisoAtrasoRepository>().ImplementedBy<AvisoAtrasoRepository>(),
                Component.For<AtrasosService>());

            container.Register(
                Component.For<IReservedMapping>().ImplementedBy<ConfigCADEReservedMapping>(),
                Component.For<IConfigCADERepository>().ImplementedBy<ConfigCADERepository>().DependsOn(new { Alias = "SADE" }),
                Component.For<PedraIntegracaoService>(),
                Component.For<CADEConfigService>()
                );

            container.Register(
                Component.For<INfeEdiRepository>().ImplementedBy<NfeEdiRepository>(),
                Component.For<INfePdfEdiRepository>().ImplementedBy<NfePdfEdiRepository>(),
                Component.For<INfeProdutoEdiRepository>().ImplementedBy<NfeProdutoEdiRepository>());

            container.Register(Component.For<IVagaoLaudoRepository>().ImplementedBy<VagaoLaudoRepository>());

            container.Register(
                Component.For<IFrotaVagaoRepository>().ImplementedBy<FrotaVagaoRepository>(),
                Component.For<ILimiteExcecaoFrotaRepository>().ImplementedBy<LimiteExcecaoFrotaRepository>(),
                Component.For<ILimiteExcecaoVagaoRepository>().ImplementedBy<LimiteExcecaoVagaoRepository>(),
                Component.For<ILimiteMinimoBaseRepository>().ImplementedBy<LimiteMinimoBaseRepository>());

            container.Register(
                Component.For<IDiarioBordoCteRepository>().ImplementedBy<DiarioBordoCteRepository>(),
                Component.For<ICteResponsavelProcedimentoRepository>()
                    .ImplementedBy<CteResponsavelProcedimentoRepository>());

            // Termometro
            container.Register(Component.For<ThermometerService>().ImplementedBy<ThermometerService>());
            container.Register(
                Component.For<IThermometerRepository>()
                    .ImplementedBy<ThermometerRepository>()
                    .DependsOn(new { Alias = "ACT" }),
                Component.For<IReadThermometerRepository>()
                    .ImplementedBy<ReadThermometerRepository>()
                    .DependsOn(new { Alias = "ACT" }));

            container.Register(Component.For<IConfNfePoolingRepository>().ImplementedBy<ConfNfePoolingRepository>());

            container.Register(
                Component.For<IMonitorEmailJobRunnerRepository>().ImplementedBy<MonitorEmailJobRunnerRepository>());

            container.Register(
                Component.For<IParametrosPetrobrasRepository>().ImplementedBy<ParametrosPetrobrasRepository>());

            container.Register(
               Component.For<IControleCpbrRepository>().ImplementedBy<ControleCpbrRepository>());

            container.Register(
                Component.For<VagoesTravadosAnxService>());

            container.Register(
               Component.For<IVagaoTaraEdiDescargaRepository>().ImplementedBy<VagaoTaraEdiDescargaRepository>());

            container.Register(
              Component.For<IVagaoLogTaraRepository>().ImplementedBy<VagaoLogTaraRepository>());

            container.Register(
              Component.For<IVagaoMedianaTempRepository>().ImplementedBy<VagaoMedianaTempRepository>());

            container.Register(
              Component.For<IVagaoMedianaTempPlanilhaRepository>().ImplementedBy<VagaoMedianaTempPlanilhaRepository>());

            container.Register(
              Component.For<IVagaoPesagemPlanilhaRepository>().ImplementedBy<VagaoPesagemPlanilhaRepository>());

            container.Register(
                   Component.For<IMotivoSituacaoVagaoRepository>().ImplementedBy<MotivoSituacaoVagaoRepository>(),
                   Component.For<MotivoSituacaoVagaoService>().ImplementedBy<MotivoSituacaoVagaoService>());

            container.Register(Component.For<IVagaoHistoricoTaraRepository>().ImplementedBy<VagaoHistoricoTaraRepository>());
            container.Register(Component.For<VagaoHistoricoTaraService>().ImplementedBy<VagaoHistoricoTaraService>());
            container.Register(Component.For<ICteConteinerRepository>().ImplementedBy<CteConteinerRepository>());
            container.Register(Component.For<INfeEstornoSaldoRepository>().ImplementedBy<NfeEstornoSaldoRepository>());

            #region Inje��o de objetos usados no TPCCO
            //container.Register(
            //     Component.For<IOrdemServicoService>().ImplementedBy<OrdemServicoService>(),
            //     Component.For<ILocaisRepository>().ImplementedBy<LocaisRepository>(),
            //     Component.For<IFalhasRepository>().ImplementedBy<FalhasRepository>(),
            //     Component.For<IEquipeToRepository>().ImplementedBy<EquipeToRepository>(),
            //     Component.For<IOrdemServicosRepository>().ImplementedBy<OrdemServicosRepository>(),
            //     Component.For<IOsDescricoesRepository>().ImplementedBy<OsDescricoesRepository>(),
            //     Component.For<ITipoEquipamentosRepository>().ImplementedBy<TipoEquipamentosRepository>(),
            //     Component.For<TecnicosService>().ImplementedBy<TecnicosService>(),
            //     Component.For<IHorariosTecRepository>().ImplementedBy<HorariosTecRepository>(),
            //     Component.For<ITecnicosRepository>().ImplementedBy<TecnicosRepository>(),
            //     Component.For<IRotasRepository>().ImplementedBy<RotasRepository>(),
            //     Component.For<IEquipamentosRepository>().ImplementedBy<EquipamentosRepository>(),
            //     Component.For<IPesquisaOSRepository>().ImplementedBy<PesquisaOSRepository>());
            #endregion

            #region Inje��o de objetos OS Limpeza/Revistamento

            container.Register(
                Component.For<IOSLimpezaVagaoRepository>().ImplementedBy<OSLimpezaVagaoRepository>(),
                Component.For<IOSLimpezaVagaoItemRepository>().ImplementedBy<OSLimpezaVagaoItemRepository>(),
                Component.For<IOSStatusRepository>().ImplementedBy<OSStatusRepository>(),
                Component.For<IOSRevistamentoVagaoRepository>().ImplementedBy<OSRevistamentoVagaoRepository>(),
                Component.For<IOSRevistamentoVagaoItemRepository>().ImplementedBy<OSRevistamentoVagaoItemRepository>(),
                Component.For<IOSTipoRepository>().ImplementedBy<OSTipoRepository>());

            container.Register(
                Component.For<IOSStatusService>().ImplementedBy<OSStatusService>(),
                Component.For<IOSLimpezaVagaoService>().ImplementedBy<OSLimpezaVagaoService>(),
                Component.For<IOSRevistamentoVagaoService>().ImplementedBy<OSRevistamentoVagaoService>(),
                Component.For<IOSTipoService>().ImplementedBy<OSTipoService>());

            #endregion

            #region Inje��o de objetos Fornecedor

            container.Register(Component.For<IFornecedorOsRepository>().ImplementedBy<FornecedorOsRepository>());
            container.Register(Component.For<IFornecedorOsService>().ImplementedBy<FornecedorOsService>());
            container.Register(Component.For<ITipoServicoRepository>().ImplementedBy<TipoServicoRepository>());
            container.Register(Component.For<ITipoServicoService>().ImplementedBy<TipoServicoService>());
            container.Register(Component.For<ILocalFornecedorRepository>().ImplementedBy<LocalFornecedorRepository>());
            container.Register(Component.For<ILocalFornecedorService>().ImplementedBy<LocalFornecedorService>());

            #endregion

            #region Inje��o de Objetos Manga

            container.Register(Component.For<IMangaRepository>().ImplementedBy<MangaRepository>());
            container.Register(Component.For<MangaService>().ImplementedBy<MangaService>());

            #endregion

            #region Inje��o de Objetos Relat�rio Faturamento

            container.Register(Component.For<IRelatorioFaturamentoRepository>().ImplementedBy<RelatorioFaturamentoRepository>());
            container.Register(Component.For<IRelatorioFaturamentoService>().ImplementedBy<RelatorioFaturamentoService>());

            #endregion

            #region Inje��o de Objetos Liberacao Formacao Trem

            container.Register(Component.For<ILiberacaoFormacaoTremRepository>().ImplementedBy<LiberacaoFormacaoTremRepository>());
            container.Register(Component.For<ILiberacaoFormacaoTremService>().ImplementedBy<LiberacaoFormacaoTremService>());

            #endregion

            #region Inje��o de Objetos MalhaSeguro

            container.Register(Component.For<IMalhaSeguroRepository>().ImplementedBy<MalhaSeguroRepository>());
            container.Register(Component.For<IMalhaSeguroService>().ImplementedBy<MalhaSeguroService>());

            #endregion

            #region Inje��o de Objetos Controle de Perdas
            container.Register(Component.For<ITfaRepository>().ImplementedBy<TfaRepository>());
            container.Register(Component.For<ITfaService>().ImplementedBy<TfaService>());

            container.Register(Component.For<ITerminalTfaService>().ImplementedBy<TerminalTfaService>());
            container.Register(Component.For<ITerminalTfaRepository>().ImplementedBy<TerminalTfaRepository>());

            container.Register(Component.For<ITerminalTfaEmailService>().ImplementedBy<TerminalTfaEmailService>());
            container.Register(Component.For<ITerminalTfaEmailRepository>().ImplementedBy<TerminalTfaEmailRepository>());

            container.Register(Component.For<IGrupoEmpresaTfaService>().ImplementedBy<GrupoEmpresaTfaService>());
            container.Register(Component.For<IGrupoEmpresaTfaRepository>().ImplementedBy<GrupoEmpresaTfaRepository>());

            container.Register(Component.For<IEmpresaGrupoTfaService>().ImplementedBy<EmpresaGrupoTfaService>());
            container.Register(Component.For<IEmpresaGrupoTfaRepository>().ImplementedBy<EmpresaGrupoTfaRepository>());

            container.Register(Component.For<IProcessoSeguroIntegracaoService>().ImplementedBy<ProcessoSeguroIntegracaoService>());

            container.Register(Component.For<ITfaCacheRepository>().ImplementedBy<TfaCacheRepository>());
            container.Register(Component.For<ILogTfaRepository>().ImplementedBy<LogTfaRepository>());

            container.Register(Component.For<IControlePerdasService>().ImplementedBy<ControlePerdasService>());
            container.Register(Component.For<IRecomendacaoVagaoRepository>().ImplementedBy<RecomendacaoVagaoRepository>());
            container.Register(Component.For<IRecomendacaoToleranciaRepository>().ImplementedBy<RecomendacaoToleranciaRepository>());

            #endregion

            #region Inje��o de Objetos TFAAnexo
            container.Register(Component.For<ITfaAnexoRepository>().ImplementedBy<TfaAnexoRepository>());
            container.Register(Component.For<ITfaAnexoService>().ImplementedBy<TfaAnexoService>());

            #endregion

            #region Inje��o de Objetos Relat�rio Ficha Recomenda��o
            container.Register(Component.For<IRelatorioFichaRecomendacaoRepository>().ImplementedBy<RelatorioFichaRecomendacaoRepository>());
            container.Register(Component.For<IRelatorioFichaRecomendacaoService>().ImplementedBy<RelatorioFichaRecomendacaoService>());
            #endregion

            container.Register(Component.For<IConfiguracaoDespachoAutomaticoRepository>().ImplementedBy<ConfiguracaoDespachoAutomaticoRepository>());
            container.Register(Component.For<IExcessaoVagaoPesoRepository>().ImplementedBy<ExcessaoVagaoPesoRepository>());
            #region VooAtivos

            container.Register(Component.For<ISolicitacaoVooRepository>().ImplementedBy<SolicitacaoVooRepository>());
            container.Register(Component.For<ISolicitacaoVooItemRepository>().ImplementedBy<SolicitacaoVooItemRepository>());
            container.Register(Component.For<ISolicitacaoVooService>().ImplementedBy<SolicitacaoVooService>());
            container.Register(Component.For<IElementoViaService>().ImplementedBy<ElementoViaService>());
            container.Register(Component.For<IVooMotivoRepository>().ImplementedBy<VooMotivoRepository>());
            container.Register(Component.For<IMotivoVooService>().ImplementedBy<MotivoVooService>());
            container.Register(Component.For<ISolicitacaoTipoAtivoRepository>().ImplementedBy<SolicitacaoTipoAtivoRepository>());
            container.Register(Component.For<ISolicitacaoVooAtivoStatusRepository>().ImplementedBy<SolicitacaoVooAtivoStatusRepository>());
            container.Register(Component.For<ISolicitacaoVooAtivoStatusItemRepository>().ImplementedBy<SolicitacaoVooAtivoStatusItemRepository>());

            #endregion

            #region Inje��o de Processo Seguro
            container.Register(Component.For<IProcessoSeguroRepository>().ImplementedBy<ProcessoSeguroRepository>());
            container.Register(Component.For<IProcessoSeguroService>().ImplementedBy<ProcessoSeguroService>());
            #endregion

            #region Inje��o de Objetos ChecklistPassageiros
            container.Register(Component.For<IOsChecklistAprovacaoLogRepository>().ImplementedBy<OsChecklistAprovacaoLogRepository>());
            container.Register(Component.For<IOsChecklistAprovacaoRepository>().ImplementedBy<OsChecklistAprovacaoRepository>());
            container.Register(Component.For<IOsChecklistImportacaoRepository>().ImplementedBy<OsChecklistImportacaoRepository>());
            container.Register(Component.For<IOsChecklistLocoRepository>().ImplementedBy<OsChecklistLocoRepository>());
            container.Register(Component.For<IOsChecklistLogRepository>().ImplementedBy<OsChecklistLogRepository>());
            container.Register(Component.For<IOsChecklistPassageiroLogRepository>().ImplementedBy<OsChecklistPassageiroLogRepository>());
            container.Register(Component.For<IOsChecklistPassageiroRepository>().ImplementedBy<OsChecklistPassageiroRepository>());
            container.Register(Component.For<IOsChecklistRepository>().ImplementedBy<OsChecklistRepository>());
            container.Register(Component.For<IOsChecklistService>().ImplementedBy<OsChecklistService>());
            #endregion

            #region Injecao de Objetos Baldeio
            container.Register(Component.For<IBaldeioService>().ImplementedBy<BaldeioService>());
            container.Register(Component.For<ICartaBaldeioRepository>().ImplementedBy<CartaBaldeioRepository>());
            container.Register(Component.For<ICartaBaldeioService>().ImplementedBy<CartaBaldeioService>());
            #endregion

            #region Inje��o de ConsultaTrem
            container.Register(Component.For<IConsultaTremRepository>().ImplementedBy<ConsultaTremRepository>());
            container.Register(Component.For<IConsultaTremService>().ImplementedBy<ConsultaTremService>());
            #endregion

            #region Inje��o de ChecklistFichaTrem

            //Repositorio            
            container.Register(Component.For<IPerguntaChecklistRepository>().ImplementedBy<PerguntaChecklistRepository>());

            // Servi�o
            container.Register(Component.For<IChecklistFichaTremService>().ImplementedBy<ChecklistFichaTremService>());

            #endregion

            #region Inje��o de Anexacao e Desanexacao

            //Repositorio            
            container.Register(Component.For<IAnexacaoDesanexacaoRepository>().ImplementedBy<AnexacaoDesanexacaoRepository>());
            container.Register(Component.For<ILogAnxHistTremRepository>().ImplementedBy<LogAnxHistTremRepository>());
            container.Register(Component.For<ILogAnxHistVeiculoRepository>().ImplementedBy<LogAnxHistVeiculoRepository>());

            // Servi�o
            container.Register(Component.For<IAnexacaoDesanexacaoService>().ImplementedBy<AnexacaoDesanexacaoService>());
            container.Register(Component.For<ILogAnxHistTremService>().ImplementedBy<LogAnxHistTremService>());
            container.Register(Component.For<ILogAnxHistVeiculoService>().ImplementedBy<LogAnxHistVeiculoService>());

            #endregion

            #region Inje��o de WebService
            // Servi�o
            container.Register(Component.For<IWebService>().ImplementedBy<WebService>());





            #endregion

            #region Inje��o de Forma��o Automatica

            //Repositorio            
            container.Register(Component.For<ILogFormacaoAutoRepository>().ImplementedBy<LogFormacaoAutoRepository>());
            container.Register(Component.For<ILogFormacaoAutoVeiculoRepository>().ImplementedBy<LogFormacaoAutoVeiculoRepository>());

            // Servi�o
            container.Register(Component.For<ILogFormacaoAutoService>().ImplementedBy<LogFormacaoAutomaticaService>());


            #endregion

            #region ToleranciaMercadoria
            container.Register(Component.For<IToleranciaService>().ImplementedBy<ToleranciaService>());
            container.Register(Component.For<IToleranciaMercadoriaRepository>().ImplementedBy<ToleranciaMercadoriaRepository>());

            #endregion

            #region GrupoRota
            container.Register(Component.For<IGrupoRotaService>().ImplementedBy<GrupoRotaService>());
            container.Register(Component.For<IGrupoRotaRepository>().ImplementedBy<GrupoRotaRepository>());
            container.Register(Component.For<IGrupoRotaRotaRepository>().ImplementedBy<GrupoRotaRotaRepository>());
            container.Register(Component.For<IGrupoRotaLimiteRepository>().ImplementedBy<GrupoRotaLimiteRepository>());
            #endregion

        }

#endregion

    }
}