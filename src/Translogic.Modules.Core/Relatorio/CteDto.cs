﻿namespace Translogic.Modules.Core.Relatorio
{
    using System;
    using System.Data;
    using System.Globalization;

    /// <summary>
    /// Dto com todos os dados do Cte
    /// </summary>
    public class CteDto : DataSet
    {
        /// <summary>
        /// Gets or sets CodigoTransacao.
        /// </summary>
        public string ChaveCte { get; set; }

        /// <summary>
        /// Gets or sets bytes da imagem gerada com o código de barras
        /// </summary>
        public byte[] CodigoBarra { get; set; }
    }
}