﻿var filtrosTrem, gridTrem,
    filtrosVagao, gridVagao,
    tabControl,
    urlDetalhes, urlPesquisar, urlPesquisarVagoes, urlObterLinhas, urlImpressaoVagoes;

function iniciarTabs() {
    var width = jQuery(window).width();
    var height = jQuery(window).height();
    tabControl = new Ext.TabPanel({
        activeTab: 0,
        frame: false,
        height: 700,
        margins: '0 0 0 0',
        defaults: { autoScroll: true, autoHeight: true },
        items: [{
            title: 'Trem',
            layout: 'vbox',
            items: [filtrosTrem, gridTrem]
        }, {
            title: 'Vagao',
            layout: 'vbox',
            items: [{
                height: 130,
                width: width,
                items: [filtrosVagao]
            },
                {
                    flex: 1,
                    width: width,
                    items: [gridVagao]
                }]
        }]
    });
}

function iniciarGridTrem() {
    var detalheAction = new Ext.ux.grid.RowActions({
        dataIndex: '',
        header: '',
        align: 'center',
        actions: [{
            iconCls: 'icon-detail',
            tooltip: 'Exibir Detalhes'
        }],
        callbacks: {
            'icon-detail': function (grid, record, action, row, col) {
                Detalhe(record.data.Id);
            }
        }
    });

    gridTrem = new Translogic.PaginatedGrid({
        id: 'grid',
        name: 'grid',
        autoLoadGrid: false,
        height: 500,
        plugins: [detalheAction],
        fields: [
                'Id',
                'Prefixo',
                'Origem',
                'Destino',
                'NumeroOS',
				'DataRealizadaPartida'
            ],
        url: urlPesquisar,
        columns: [
				detalheAction,
                { header: 'Prefixo', width: 80, dataIndex: "Prefixo", sortable: false },
                { header: 'Origem', width: 80, dataIndex: "Origem", sortable: false },
                { header: 'Destino', width: 80, dataIndex: "Destino", sortable: false },
                { header: 'OS', width: 80, dataIndex: "NumeroOS", sortable: false },
                { header: 'Data Partida', width: 80, dataIndex: "DataRealizadaPartida", sortable: false }
            ],
        listeners: {
            rowdblclick: function (grid, rowIndex, record, e) {
                //  MostrarDetalhes(grid.getStore().getAt(rowIndex).get('Id'));
            }
        }
    });

    gridTrem.getStore().proxy.on('beforeload', function (p, params) {
        var dataInicial = Ext.getCmp("txtFiltroDataInicial").getValue();
        var dataFinal = Ext.getCmp("txtFiltroDataFinal").getValue();
        var prefixo = Ext.getCmp("txtFiltroPrefixo").getValue();
        var os = Ext.getCmp("txtFiltroOS").getValue();
        var origem = Ext.getCmp("filtro-Ori").getValue();
        var destino = Ext.getCmp("filtro-Dest").getValue();

        params['filter[0].Campo'] = 'dataInicial';
        params['filter[0].Valor'] = new Array(dataInicial.format('d/m/Y') + " " + dataInicial.format('H:i:s'));
        params['filter[0].FormaPesquisa'] = 'Start';

        params['filter[1].Campo'] = 'dataFinal';
        params['filter[1].Valor'] = new Array(dataFinal.format('d/m/Y') + " " + dataFinal.format('H:i:s'));
        params['filter[1].FormaPesquisa'] = 'Start';

        params['filter[2].Campo'] = 'prefixo';
        params['filter[2].Valor'] = prefixo;
        params['filter[2].FormaPesquisa'] = 'Start';

        params['filter[3].Campo'] = 'os';
        params['filter[3].Valor'] = os;
        params['filter[3].FormaPesquisa'] = 'Start';

        params['filter[4].Campo'] = 'origem';
        params['filter[4].Valor'] = origem;
        params['filter[4].FormaPesquisa'] = 'Start';

        params['filter[5].Campo'] = 'destino';
        params['filter[5].Valor'] = destino;
        params['filter[5].FormaPesquisa'] = 'Start';
    });
}

function iniciarGridVagao() {
    var smTemp = new Ext.grid.CheckboxSelectionModel();

    var storeTemp = new Ext.data.JsonStore({
        root: "Items",
        autoLoad: false,
        url: urlPesquisarVagoes,
        fields: [
                'Id',
                'IdItemDespacho',
                'Sequencia',
                'Vagao',
                'Origem',
                'Destino',
                'Mercadoria',
                'TU',
                'TB',
                'Correntista',
                'Tara',
                'Destinatario',
                'PossuiTicket',
                'DataCarregamento',
                'DataDescarregamento'
            ]
    });

    gridVagao = new Ext.grid.GridPanel({
        id: 'gridVagoes',
        name: 'gridVagoes',
        autoLoadGrid: false,
        autoScroll: true,
        sm: smTemp,
        height: 500,
        stripeRows: true,
        region: 'center',
        store: storeTemp,
        loadMask: { msg: App.Resources.Web.Carregando },
        colModel: new Ext.grid.ColumnModel({
            defaults: {
                sortable: false
            },
            columns: [
                    smTemp,
                    { header: 'Ticket', width: 35, dataIndex: "PossuiTicket", sortable: false, renderer: TicketPrintRenderer },
                    { header: 'NF-e', width: 35, sortable: false, renderer: NfePrintRenderer },
                    { header: 'Vagão', width: 80, dataIndex: "Vagao", sortable: false },
                    { header: 'Origem', width: 50, dataIndex: "Origem", sortable: false },
                    { header: 'Destino', width: 50, dataIndex: "Destino", sortable: false },
                    { header: 'Mercadoria', width: 200, dataIndex: "Mercadoria", sortable: false },
                    { header: 'TU', width: 50, dataIndex: "TU", sortable: false },
                    { header: 'TB', width: 50, dataIndex: "TB", sortable: false },
                    { header: 'Tara', width: 50, dataIndex: "Tara", sortable: false },
                    { header: 'Correntista', width: 150, dataIndex: "Correntista", sortable: false },
                    { header: 'Destinatario', width: 150, dataIndex: "Destinatario", sortable: false },
                    { header: 'Carregamento', width: 120, dataIndex: "DataCarregamento", sortable: false },
                    { header: 'Descarregamento', width: 120, dataIndex: "DataDescarregamento", sortable: false }
                ]
        })
    });

    function TicketPrintRenderer(val, metaData, record, rowIndex, colIndex, store) {

        var id = record.get('IdItemDespacho');
        var url = urlImpressaoVagoes;
        url += "?itensDespacho=" + id;
        url = url + "&tipoImpressao=1";

        switch (val) {
            case true:
                return "<a href='" + url + "'><img src='/Content/Images/Icons/printer.png' alt='Imprimir Ticket' ></a><br />";
            default:
                return "";
        }
    }

    function NfePrintRenderer(val, metaData, record, rowIndex, colIndex, store) {

        var id = record.get('IdItemDespacho');
        var url = urlImpressaoVagoes;
        url += "?itensDespacho=" + id;
        url = url + "&tipoImpressao=0";

        return "<a href='" + url + "'><img src='/Content/Images/Icons/printer.png' alt='Imprimir Nf-e' ></a><br />";
    }

    gridVagao.getStore().proxy.on('beforeload', function (p, params) {
        var dataInicial = Ext.getCmp("txtFiltroDataInicialVagao").getValue();
        var dataFinal = Ext.getCmp("txtFiltroDataFinalVagao").getValue();

        params['filter[0].Campo'] = 'dataInicial';
        params['filter[0].Valor'] = new Array(dataInicial.format('d/m/Y') + " " + dataInicial.format('H:i:s'));
        params['filter[0].FormaPesquisa'] = 'Start';

        params['filter[1].Campo'] = 'dataFinal';
        params['filter[1].Valor'] = new Array(dataFinal.format('d/m/Y') + " " + dataFinal.format('H:i:s'));
        params['filter[1].FormaPesquisa'] = 'Start';

        params['filter[2].Campo'] = 'estacoes';
        params['filter[2].Valor'] = Ext.getCmp("txtFiltroLocalVagao").getValue();
        params['filter[2].FormaPesquisa'] = 'Start';

        params['filter[3].Campo'] = 'vagoes';
        params['filter[3].Valor'] = Ext.getCmp("txtFiltroVagoes").getValue();
        params['filter[3].FormaPesquisa'] = 'Start';

        params['filter[4].Campo'] = 'idLinha';
        params['filter[4].Valor'] = Ext.getCmp("cbLinha").getValue();
        params['filter[4].FormaPesquisa'] = 'Start';
    });
}

function iniciarFiltrosTrem() {
    /************************* FILTROS **************************/
    var txtFiltroDataInicial = {
        xtype: 'datefield',
        fieldLabel: 'Data Inicial',
        id: 'txtFiltroDataInicial',
        name: 'txtFiltroDataInicial',
        width: 90,
        allowBlank: false,
        vtype: 'daterange',
        endDateField: 'txtFiltroDataFinal',
        value: new Date(),
        enableKeyEvents: true,
        listeners: {
            specialkey: TratarTeclaEnter
        }
    };

    var txtFiltroDataFinal = {
        xtype: 'datefield',
        fieldLabel: 'Data Final',
        id: 'txtFiltroDataFinal',
        name: 'txtFiltroDataFinal',
        width: 90,
        allowBlank: false,
        vtype: 'daterange',
        startDateField: 'txtFiltroDataInicial',
        value: new Date(),
        enableKeyEvents: true,
        listeners: {
            specialkey: TratarTeclaEnter
        }
    };

    var txtFiltroOS = {
        xtype: 'textfield',
        autoCreate: { tag: 'input', type: 'text', maxlength: '10', autocomplete: 'off' },
        width: 70,
        maskRe: /[0-9]/,
        name: 'txtFiltroOS',
        allowBlank: true,
        id: 'txtFiltroOS',
        fieldLabel: 'OS',
        enableKeyEvents: true,
        listeners: {
            specialkey: TratarTeclaEnter
        }
    };

    var txtFiltroPrefixo = {
        xtype: 'textfield',
        style: 'text-transform: uppercase',
        autoCreate: { tag: 'input', type: 'text', maxlength: '3', autocomplete: 'off' },
        maskRe: /[a-zA-Z0-9]/,
        width: 60,
        name: 'txtFiltroPrefixo',
        allowBlank: true,
        id: 'txtFiltroPrefixo',
        fieldLabel: 'Prefixo',
        enableKeyEvents: true,
        listeners: {
            specialkey: TratarTeclaEnter
        }
    };

    var txtOrigem = {
        xtype: 'textfield',
        vtype: 'cteestacaovtype',
        style: 'text-transform: uppercase',
        id: 'filtro-Ori',
        fieldLabel: 'Origem',
        autoCreate: { tag: 'input', type: 'text', maxlength: '3', autocomplete: 'off' },
        name: 'Origem',
        allowBlank: true,
        maxLength: 3,
        width: 35,
        hiddenName: 'Origem',
        enableKeyEvents: true,
        listeners: {
            specialkey: TratarTeclaEnter
        }
    };

    var txtDestino = {
        xtype: 'textfield',
        vtype: 'cteestacaovtype',
        style: 'text-transform: uppercase',
        id: 'filtro-Dest',
        fieldLabel: 'Destino',
        autoCreate: { tag: 'input', type: 'text', maxlength: '3', autocomplete: 'off' },
        name: 'Destino',
        allowBlank: true,
        maxLength: 3,
        width: 35,
        hiddenName: 'Destino',
        enableKeyEvents: true,
        listeners: {
            specialkey: TratarTeclaEnter
        }
    };

    var btnPesquisar = {
        name: 'btnPesquisar',
        id: 'btnPesquisar',
        text: 'Pesquisar',
        iconCls: 'icon-find',
        handler: CarregarGrid
    };

    var btnLimpar = {
        name: 'btnLimpar',
        id: 'btnLimpar',
        text: 'Limpar',
        handler: Limpar
    };

    var coluna1Linha1 = {
        layout: 'form',
        border: false,
        bodyStyle: 'padding-right:10px',
        items: [txtFiltroDataInicial]
    };

    var coluna2Linha1 = {
        layout: 'form',
        border: false,
        bodyStyle: 'padding-right:10px',
        items: [txtFiltroDataFinal]
    };

    var coluna3Linha1 = {
        layout: 'form',
        border: false,
        bodyStyle: 'padding-right:10px',
        items: [txtFiltroPrefixo]
    };

    var coluna4Linha1 = {
        layout: 'form',
        border: false,
        bodyStyle: 'padding-right:10px',
        items: [txtFiltroOS]
    };

    var coluna5Linha1 = {
        layout: 'form',
        border: false,
        bodyStyle: 'padding-right:10px',
        items: [txtOrigem]
    };

    var coluna6Linha1 = {
        layout: 'form',
        border: false,
        bodyStyle: 'padding-right:10px',
        items: [txtDestino]
    };

    var linha1 = {
        layout: 'column',
        border: false,

        items: [coluna1Linha1, coluna2Linha1, coluna3Linha1, coluna4Linha1, coluna5Linha1, coluna6Linha1]
    };

    filtrosTrem = new Ext.form.FormPanel({
        id: 'filtros',
        name: 'filtros',
        bodyStyle: 'padding: 10px',
        labelAlign: 'top',
        width: jQuery(window).width(),
        items: [linha1],
        buttonAlign: 'center',
        buttons: [btnPesquisar, btnLimpar]
    });
}

function iniciarFiltrosVagao() {
    /************************* FILTROS **************************/
    var txtFiltroDataInicialVagao = {
        xtype: 'datefield',
        fieldLabel: 'Data Inicial',
        id: 'txtFiltroDataInicialVagao',
        name: 'txtFiltroDataInicialVagao',
        width: 90,
        allowBlank: false,
        vtype: 'daterange',
        endDateField: 'txtFiltroDataFinalVagao',
        value: new Date(),
        enableKeyEvents: true,
        listeners: {
            specialkey: TratarTeclaEnterVagao
        }
    };

    var txtFiltroDataFinalVagao = {
        xtype: 'datefield',
        fieldLabel: 'Data Final',
        id: 'txtFiltroDataFinalVagao',
        name: 'txtFiltroDataFinalVagao',
        width: 90,
        allowBlank: false,
        vtype: 'daterange',
        startDateField: 'txtFiltroDataInicialVagao',
        value: new Date(),
        enableKeyEvents: true,
        listeners: {
            specialkey: TratarTeclaEnterVagao
        }
    };

    var txtFiltroVagoes = {
        xtype: 'textfield',
        style: 'text-transform: uppercase',
        autoCreate: { tag: 'input', type: 'text', autocomplete: 'off' },
        maskRe: /[a-zA-Z0-9]/,
        width: 200,
        name: 'txtFiltroVagoes',
        allowBlank: true,
        id: 'txtFiltroVagoes',
        fieldLabel: 'Lista de Vagões',
        enableKeyEvents: true,
        listeners: {
            specialkey: TratarTeclaEnterVagao
        }
    };

    var txtLocal = {
        xtype: 'textfield',
        vtype: 'cteestacaovtype',
        style: 'text-transform: uppercase',
        id: 'txtFiltroLocalVagao',
        fieldLabel: 'Local',
        autoCreate: { tag: 'input', type: 'text', maxlength: '3', autocomplete: 'off' },
        name: 'txtFiltroLocalVagao',
        allowBlank: true,
        maxLength: 3,
        width: 35,
        hiddenName: 'txtFiltroLocalVagao',
        enableKeyEvents: true,
        listeners: {
            specialkey: TratarTeclaEnterVagao,
            'change': function () {
                window.Ext.getCmp('cbLinha').clearValue();
                var tmpStore = window.Ext.getCmp('cbLinha').getStore();
                tmpStore.setBaseParam('areaOper', window.Ext.getCmp('txtFiltroLocalVagao').getValue());
                tmpStore.removeAll();
                tmpStore.reload({
                    'params': {
                        areaOper: window.Ext.getCmp('txtFiltroLocalVagao').getValue()
                    }
                });
            }
        }
    };

    var cbLinha = {
        xtype: 'combo',
        id: 'cbLinha',
        name: 'cbLinha',
        forceSelection: true,
        store: new window.Ext.data.JsonStore({
            id: 'dsLinhas',
            name: 'dsLinhas',
            root: 'Items',
            url: urlObterLinhas,
            fields: ['Id', 'Codigo']
        }),
        triggerAction: 'all',
        mode: 'remote',
        typeAhead: true,
        displayField: 'Codigo',
        valueField: 'Id',
        fieldLabel: 'Linhas',
        width: 100,
        minChars: 1,
        listeners: {
            specialkey: TratarTeclaEnterVagao
        }
    };

    var rdTipoImpressaoVagao = {
        xtype: 'radiogroup',
        fieldLabel: 'Tipo Impressão',
        id: 'rdTipoImpressaoVagao',
        width: 170,
        name: 'rdTipoImpressaoVagao',
        items: [
	{ boxLabel: 'NF-e', name: 'tipoImpressao', inputValue: 0, checked: true },
	{ boxLabel: 'Ticket', name: 'tipoImpressao', inputValue: 1 },
	{ boxLabel: 'Ambos', name: 'tipoImpressao', inputValue: 2 }
	]
    };

    var btnPesquisarVagoes = {
        name: 'btnPesquisarVagoes',
        id: 'btnPesquisarVagoes',
        text: 'Pesquisar',
        iconCls: 'icon-find',
        handler: CarregarGridVagao
    };

    var btnLimparVagoes = {
        name: 'btnLimparVagoes',
        id: 'btnLimparVagoes',
        text: 'Limpar',
        handler: Limpar
    };

    var btnImprimirVagoes = {
        name: 'btnImprimirVagoes',
        id: 'btnImprimirVagoes',
        text: 'Imprimir',
        iconCls: 'icon-printer',
        handler: ImprimirVagoes
    };

    var coluna1Linha1 = {
        layout: 'form',
        border: false,
        bodyStyle: 'padding-right:10px',
        items: [txtFiltroDataInicialVagao]
    };

    var coluna2Linha1 = {
        layout: 'form',
        border: false,
        bodyStyle: 'padding-right:10px',
        items: [txtFiltroDataFinalVagao]
    };

    var coluna3Linha1 = {
        layout: 'form',
        border: false,
        bodyStyle: 'padding-right:10px',
        items: [txtFiltroVagoes]
    };

    var coluna4Linha1 = {
        layout: 'form',
        border: false,
        bodyStyle: 'padding-right:10px',
        items: [txtLocal]
    };

    var coluna5Linha1 = {
        layout: 'form',
        border: false,
        bodyStyle: 'padding-right:10px',
        items: [cbLinha]
    };

    var coluna6Linha1 = {
        layout: 'form',
        border: false,
        bodyStyle: 'padding-right:10px',
        items: [rdTipoImpressaoVagao]
    };

    var linha1 = {
        layout: 'column',
        border: false,

        items: [coluna1Linha1, coluna2Linha1, coluna3Linha1, coluna4Linha1, coluna5Linha1, coluna6Linha1]
    };

    filtrosVagao = new Ext.form.FormPanel({
        id: 'filtrosVagoes',
        name: 'filtrosVagoes',
        bodyStyle: 'padding: 10px',
        labelAlign: 'top',
        width: jQuery(window).width(),
        items: [linha1],
        buttonAlign: 'center',
        buttons: [btnPesquisarVagoes, btnLimparVagoes, btnImprimirVagoes]
    });
}

/************************* DETALHES **************************/
var formDetalhes;

function Detalhe(id) {
   
    var url = urlDetalhes + "?idTrem=" + id;
    document.location.href = url;
}


function TratarTeclaEnter(f, e) {
    if (e.getKey() === e.ENTER) {
        CarregarGrid();
    }
}

function TratarTeclaEnterVagao(f, e) {
    if (e.getKey() === e.ENTER) {
        CarregarGridVagao();
    }
}

function Limpar() {
    gridTrem.getStore().removeAll();
    filtrosTrem.getForm().reset();
    gridVagao.getStore().removeAll();
    filtrosVagao.getForm().reset();
}

function CarregarGrid() {
    gridTrem.getStore().load();
}

function CarregarGridVagao() {
    gridVagao.getStore().load();
}


var changeCursorToHand = function (val, metadata, record) {
    metadata.style = 'cursor: pointer;';
    return val;
};

function ImprimirVagoes() {
    if (gridVagao.getSelectionModel().hasSelection()) {
        var selected = gridVagao.getSelectionModel().getSelections();
        var listaEnvio = new Array();
        
        var possuiTck = false;
        
        for (var i = 0; i < selected.length; i++) {
            listaEnvio.push(selected[i].data.IdItemDespacho);
            if (selected[i].data.PossuiTicket) {
                possuiTck = true;
            }
        }
        
        if (Ext.getCmp('rdTipoImpressaoVagao').getValue().getRawValue() === 0 && !possuiTck) {
            Ext.Msg.show({
                title: "Mensagem de Informação",
                msg: "Favor selecionar pelo menos um vagão que possua ticket.",
                buttons: Ext.Msg.OK,
                minWidth: 200
            });
        }
        
        var url = urlImpressaoVagoes;
        url += "?itensDespacho=" + listaEnvio;
        url += "&tipoImpressao=" + Ext.getCmp('rdTipoImpressaoVagao').getValue().getRawValue();

        window.open(url, "");
    } else {
        Ext.Msg.show({
            title: "Mensagem de Informação",
            msg: "Favor selecionar pelo menos um vagão.",
            buttons: Ext.Msg.OK,
            minWidth: 200
        });
    }
}

function statusRenderer(val) {
    switch (val) {
        case "AUT":
            return "<img src='/GetFile.efh?file={Translogic.Core.Infrastructure.Web;}/Content/Images/Icons/tick.png' alt='MDF-e Aprovado'>";
        case "AGE":
        case "AGC":
            return "<img src='/GetFile.efh?file={Translogic.Core.Infrastructure.Web;}/Content/Images/Icons/application_go.png' alt='Aguardando Cancelamento/Encerramento'>";
        case "EAE":
            return "<img src='/GetFile.efh?file={Translogic.Core.Infrastructure.Web;}/Content/Images/Icons/email_go.png' alt='Enviado arquivo MDF-e para Sefaz.'>";
        case "EAR":
            return "<img src='/GetFile.efh?file={Translogic.Core.Infrastructure.Web;}/Content/Images/Icons/arrow_redo_yellow.png' alt='Erro Autorizado reenvio do MDF-e.'>";
        case "CAN":
            return "<img src='/GetFile.efh?file={Translogic.Core.Infrastructure.Web;}/Content/Images/Icons/cancel.png' alt='MDF-e Cancelado '>";
        case "ENC":
            return "<img src='/GetFile.efh?file={Translogic.Core.Infrastructure.Web;}/Content/Images/Icons/delete.png' alt='MDF-e Encerrado'>";
        case "ERR":
            return "<img src='/GetFile.efh?file={Translogic.Core.Infrastructure.Web;}/Content/Images/Icons/cross.png' alt='Erro na geração do MDF-e.'>";
        case "PGC":
            return "<img src='/GetFile.efh?file={Translogic.Core.Infrastructure.Web;}/Content/Images/Icons/information.png' alt='MDf-e aguardando a geração automática da numeração / chave.'>";
        default:
            return "<img src='/GetFile.efh?file={Translogic.Core.Infrastructure.Web;}/Content/Images/Icons/alert.png' alt='MDF-e em processamento.'>";
    }
}