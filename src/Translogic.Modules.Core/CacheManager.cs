﻿using ALL.Core.IoC;
using Speed;
using Speed.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Translogic.Modules.Core.Domain.Model.Estrutura;
using Translogic.Modules.Core.Spd.BLL;
using Translogic.Modules.Core.Spd.Data;

namespace Translogic.Modules.Core
{

    /// <summary>
    /// Classe central de cache. Dever ser usado apenas para pequenos volumes de dados e que sejam muito acessados
    /// </summary>
    public static class CacheManager
    {

        static class Time
        {
            public const int Min1 = 1000;
            public const int Min5 = 5 * Min1;
            public const int Min30 = 30 * Min1;
            public const int Hora1 = 2 * Min30;
            public const int Dia = 24 * Hora1;
        }

        #region ConfigGeral

        public static DataTimer<ConfGeralInfo> ConfigGeral = new DataTimer<ConfGeralInfo>(Time.Min5,
                        () => BL_ConfGeral.GetInfo());

        #endregion ConfigGeral

        #region Empresas

        //public static DataTimer<List<Cliente>> EmpresaClientes = new DataTimer<List<CaallEmpresaClienteDto>>(Time.Min5,
        //                () => GetService<ITranslogicService>().ObterListaEmpresasPorCnpj("", "").ToList() );

        #endregion Empresas

        public static T GetService<T>() where T : class
        {
            return ApplicationContext.Container.GetService<T>();
        }

    }
}