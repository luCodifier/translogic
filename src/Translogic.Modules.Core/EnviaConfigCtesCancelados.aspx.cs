﻿namespace Translogic.Modules.Core
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Web;
    using System.Web.UI;

    using Translogic.Core.Infrastructure.Web;
    using Translogic.Modules.Core.Domain.Model.Diversos.Cte;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;

    public partial class EnviaConfigCtesCancelados : Page
    {
        #region Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                var _cteRepository = ((AbstractGlobalApplication)HttpContext.Current.ApplicationInstance).Container
                                     .Resolve<ICteRepository>();

                var _cteCancelRefatConfigRepository = ((AbstractGlobalApplication)HttpContext.Current.ApplicationInstance).Container
                                                .Resolve<ICteCanceladoRefaturamentoConfigRepository>();

                IList<CteCanceladoRefaturamentoConfig> listaCtesCancelRefatConfig = _cteCancelRefatConfigRepository.ObterTodos().Where(crf => !crf.Processado).ToList();
                IList<int?> listaIdCtes = new List<int?>();

                foreach (CteCanceladoRefaturamentoConfig cteCanceladoRefaturamentoConfig in listaCtesCancelRefatConfig)
                {
                    if (listaIdCtes.Count < 10)
                    {
                        Cte cte = _cteRepository.ObterPorId((int?)cteCanceladoRefaturamentoConfig.CteCanceladoId);
                        cte.SituacaoAtual = SituacaoCteEnum.AguardandoCancelamento;
                        cteCanceladoRefaturamentoConfig.Processado = true;

                        _cteRepository.Atualizar(cte);
                        _cteCancelRefatConfigRepository.Atualizar(cteCanceladoRefaturamentoConfig);

                        listaIdCtes.Add(cte.Id);
                    }
                    else
                    {
                        bool todosCtesCancelados = false;
                        do
                        {
                            Thread.Sleep(5000);

                            foreach (var idCte in listaIdCtes)
                            {
                                Cte cte = _cteRepository.ObterPorId(idCte);

                                if (cte.SituacaoAtual != SituacaoCteEnum.Cancelado)
                                {
                                    todosCtesCancelados = false;
                                    break;
                                }
                                todosCtesCancelados = true;
                            }
                            if (todosCtesCancelados)
                            {
                                listaIdCtes = new List<int?>();
                            }
                        }
                        while (!todosCtesCancelados);
                    }
                }
            }
            catch (Exception ex)
            {
                var erro = ex.Message;
            }
        }

        #endregion
    }
}