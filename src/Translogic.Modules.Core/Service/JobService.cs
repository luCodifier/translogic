﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;
using Translogic.Core;
using Translogic.Modules.Core.Domain.Model.Diversos;

namespace Translogic.Modules.Core.Service
{

    /// <summary>
    /// Classe base de serviços
    /// </summary>
    public class JobService
    {
        private Timer _timer;
        public bool IsRunning { get; private set; }

        /// <summary>
        /// Inicializa a classe
        /// </summary>
        public JobService()
        {
            IsRunning = false;
            _timer = new Timer(1000 * 5) { AutoReset = true };
            _timer.Elapsed += (sender, eventArgs) =>
            {
                //if (!IsRunning && Sis.IstAtivo())
                if (!IsRunning)
                {
                    IsRunning = true;
                    try
                    {
                        Execute();
                    }
                    catch (Exception ex)
                    {
                        Sis.LogException(ex, "Erro não tratado no serviço");
                    }
                    IsRunning = false;
                }
            };
        }

        /// <summary>
        /// Método chamado na inicialização do serviço
        /// </summary>
        /// <param name="startTimer">Se inicia  o timer</param>
        public void Start(bool startTimer = true)
        {
            try
            {
                TranslogicStarter.Initialize();
                TranslogicStarter.SetupForJobRunner();
                TranslogicContainer container = TranslogicStarter.Container;

                OnStart(TranslogicStarter.Container);

                if (startTimer)
                {
                    _timer.Start();
                }
            }
            catch (Exception ex)
            {
                Sis.LogException(ex);
            }
        }

        /// <summary>
        /// Método chamado no término do serviço
        /// </summary>
        public void Stop()
        {
            OnStop();
        }

        /// <summary>
        /// Para implementar lógica quando o serviço for iniciado
        /// </summary>
        protected virtual void OnStart(TranslogicContainer container)
        {
        }

        /// <summary>
        /// Para implementar lógica quando o serviço for parado
        /// </summary>
        protected virtual void OnStop()
        {
            _timer.Stop();
        }

        /// <summary>
        /// Executa o processamento. Obrgiatório sobrepor esse método
        /// </summary>
        protected virtual void OnExecute()
        {
            throw new NotImplementedException("É necessário override o método Execute");
        }

        /// <summary>
        /// Executa o processamento
        /// </summary>
        public void Execute()
        {
            try
            {
                IsRunning = true;
                //Sis.LogTrace("Processando ...");
                OnExecute();
            }
            catch (Exception ex)
            {
                Sis.LogException(ex, "Erro no Serviço");
            }
        }
    }
}