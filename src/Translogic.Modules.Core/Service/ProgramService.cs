﻿namespace Translogic.Modules.Core.Service
{
    using Speed;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Text;


    /// <summary>
    /// Classe base de Program para um Serviço
    /// </summary>
    public class ProgramService
    {

        /// <summary>
        /// Nome do Serviço
        /// </summary>
        public static string ServiceName { get; set; }

        public ProgramService()
        {
            ServiceName = "Translogic";
        }

    }
}
