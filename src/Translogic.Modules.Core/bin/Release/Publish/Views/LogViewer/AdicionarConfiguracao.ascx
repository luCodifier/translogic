﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div id="div-form-add-conf">
</div>
<script type="text/javascript">

    /* region :: Functions */
    
    function limpaCampos() {
        Ext.getCmp("txtChave").setValue("");
        Ext.getCmp("txtValor").setValue("");
        Ext.getCmp("txtDescricao").setValue("");
    }

    /* endregion :: Functions */

    /* region :: Filtros */

    var txtChave = {
        xtype: 'textfield',
        name: 'txtChave',
        id: 'txtChave',
        fieldLabel: 'Chave',
        autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '70' },
        style: 'text-transform: uppercase',
        width: 200
    };

    var txtValor = {
        xtype: 'textfield',
        name: 'txtValor',
        id: 'txtValor',
        fieldLabel: 'Valor',
        autoCreate: { tag: 'input', type: 'text', autocomplete: 'off', maxlength: '500' },
        width: 200
    };

    var txtDescricao = new Ext.form.TextArea({
        xtype: 'textarea',
        fieldLabel: 'Descrição',
        id: 'txtDescricao',
        name: 'txtDescricao',
        height: 60,
        width: 200
    });
    /* endregion :: Filtros */

    /* region :: Modal */

    var modal = new Ext.form.FormPanel({
        id: 'form-add-conf-modal',
        labelWidth: 150,
        width: 400,
        resizable: false,
        bodyStyle: 'padding: 15px',
        items: [
            txtChave,
            txtValor,
            txtDescricao
        ],
        buttonAlign: "center",
        buttons: [
            {
                text: "Salvar",
                id: "btnSalvar",
                iconCls: 'icon-save',
                handler: function () {
                    Ext.getCmp("btnSalvar").setDisabled(true);

                    window.jQuery.ajax({
                        url: '<%= Url.Action("SalvarConfiguracao") %>',
                        type: "POST",
                        data: {
                            chave: Ext.getCmp("txtChave").getValue(),
                            valor: Ext.getCmp("txtValor").getValue(),
                            descricao: Ext.getCmp("txtDescricao").getValue(),
                            edicao: false
                        },
                        dataType: "json",
                        success: function (data) {
                            var iconeCadastro = window.Ext.MessageBox.ERROR;
                            if (data.Success == true) {
                                iconeCadastro = window.Ext.MessageBox.SUCCESS;
                            } else {
                                Ext.getCmp("btnSalvar").setDisabled(false);
                            }

                            window.Ext.Msg.show({
                                title: "Salvar Configuração",
                                msg: data.Message,
                                buttons: window.Ext.Msg.OK,
                                icon: iconeCadastro,
                                minWidth: 200,
                                fn: function () {
                                    if (data.Success) {
                                        Ext.getCmp("form-add-conf").close();
                                        limpaCampos();
                                        posicaoRow = -1;
                                    } else {
                                        Ext.getCmp("btnSalvar").setDisabled(false);
                                    }
                                },
                                close: function () {
                                    if (data.Success) {
                                        Ext.getCmp("form-add-conf").close();
                                        limpaCampos();
                                        posicaoRow = -1;
                                    }
                                }
                            });
                        },
                        error: function (jqXHR, textStatus) {
                            window.Ext.Msg.show({
                                title: "Erro no Servidor",
                                msg: 'Ocorreu um erro no servidor, por favor, tente novamente mais tarde.',
                                buttons: window.Ext.Msg.OK,
                                icon: window.Ext.MessageBox.ERROR,
                                minWidth: 200
                            });
                        }
                    });
                }
            }
        ]
    });

    /* endregion :: Modal*/
    modal.render("div-form-add-conf");
</script>
