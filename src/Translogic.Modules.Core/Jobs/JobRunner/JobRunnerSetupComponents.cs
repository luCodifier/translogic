﻿namespace Translogic.Modules.Core.Jobs.JobRunner
{
    using System;
    using System.Diagnostics;
    using System.ServiceModel;
    using Castle.Facilities.WcfIntegration;
    using Castle.MicroKernel.Registration;
    using Translogic.Core;
    using Translogic.Modules.Core.Interfaces.Jobs;
    using Translogic.Modules.EDI.Domain.DataAccess.Repositories.Edi;
    using Translogic.Modules.EDI.Domain.Jobs;
    using Translogic.Modules.EDI.Domain.Models.Edi.Repositories;

    /// <summary>
    /// Serviço de configuração do jobrunner
    /// </summary>
    public class JobRunnerSetupComponents
    {
        /// <summary>
        /// Método chamado na inicialização do serviço
        /// </summary>
        public static void Start()
        {
            TranslogicStarter.Initialize();
            TranslogicStarter.SetupForJobRunner();

            TranslogicContainer container = TranslogicStarter.Container;
            RegistrarComponentes(TranslogicStarter.Container);
        }

        private static void RegistrarComponentes(TranslogicContainer container)
        {
            try
            {
                container.Register(Component.For<IConfiguracaoEmpresaEdiRepository>().ImplementedBy<ConfiguracaoEmpresaEdiRepository>());
            }
            catch { }
            try
            {
                Component.For<EDI.Domain.Models.Nfes.Repositories.INfeEdiRepository>().ImplementedBy<EDI.Domain.Models.Nfes.Repositories.INfeEdiRepository>();
            }
            catch { }
        }
    }
}