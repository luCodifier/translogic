﻿namespace Translogic.Modules.Core.Jobs.JobRunner
{
    using Speed.Common;
    using System;
    using System.Linq;
    using System.Threading;
    using Translogic.Core;
    using Translogic.Modules.Core.Interfaces.Jobs;
    using Translogic.Modules.Core.Spd;
    using Translogic.Modules.Core.Spd.Data.Enums;

    /// <summary>
    /// Classe que inicia o programa
    /// </summary>
    public static class JobRunnerProgram
    {
        /// <summary>
        /// Execute um IJob
        /// </summary>
        public static int Execute(EnumSistema sistema, string jobKey, string[] args)
        {
            // Não permite executar se chamar este programa sem parametros e se existe outra insância, independente da outra possuir parâmetros ou não
            if (args.Length == 0)
            {
                if (Helper.IsRunningInstance())
                {
                    Console.WriteLine("Já existe outra instânca rodando sem parâmetros. Saindo");
                    return 0;
                }
            }

            try
            {
                Sis.Inicializar(sistema, "Job: " + string.Join(", ", args), true);
                // var code = Spd.BLL.BL_ConfGeral.GenerateClass();
            }
            catch (Exception ex)
            {
                Sis.LogException(ex, "Sis.Inicializar");
                return 1;
            }

            try
            {
                Sis.LogTrace("SetupComponents.Start");
                JobRunnerSetupComponents.Start();
            }
            catch (Exception ex)
            {
                Sis.LogException(ex, "Erro em SetupComponents.Start");
                return 1;
            }

            try
            {
                Sis.LogTrace("Executando " + jobKey);

                var job = TranslogicStarter.Container.Resolve<IJob>(jobKey);

                args = Helper.ParseCommandLine(args);

                job.Execute(args);

                Sis.LogTrace("Fim do processamento");
            }
            catch (Exception ex)
            {
                Sis.LogException(ex, "Erro ao executar o Job");
                Sis.LogTrace("Fim do processamento");
                return 1;
            }

            return 0;
        }

        /// <summary>
        /// Execute uma action
        /// </summary>
        public static int Execute<T>(EnumSistema sistema, Action<T> actionExecute, string[] args)
        {
            return Execute<T>(sistema, null, actionExecute, args);
        }

            /// <summary>
            /// Execute uma action
            /// </summary>
            public static int Execute<T>(EnumSistema sistema, Action actionStart, Action<T> actionExecute, string[] args)
        {
            // Não permite executar se chamar este programa sem parametros e se existe outra insância, independente da outra possuir parâmetros ou não
            if (args.Length == 0)
            {
                if (Helper.IsRunningInstance())
                {
                    Console.WriteLine("Já existe outra instânca rodando sem parâmetros. Saindo");
                    return 0;
                }
            }

            try
            {
                Sis.Inicializar(sistema, "Job: " + string.Join(", ", args), true);
            }
            catch (Exception ex)
            {
                Sis.LogException(ex, "Erro ao conectar na base de dados do Translogic");
                return 1;
            }

            try
            {
                Sis.LogTrace("SetupComponents.Start");
                JobRunnerSetupComponents.Start();
            }
            catch (Exception ex)
            {
                Sis.LogException(ex, "Erro em SetupComponents.Start");
                return 1;
            }

            try
            {
                if (actionStart != null)
                {
                    actionStart();
                }

                var job = TranslogicStarter.Container.Resolve<T>();

                Sis.LogTrace("Executando " + job.GetType().Name);
                
                args = Helper.ParseCommandLine(args);

                if (actionExecute == null)
                    throw new ArgumentNullException("action");

                actionExecute(job);

                Sis.LogTrace("Fim do processamento");
            }
            catch (Exception ex)
            {
                Sis.LogException(ex, "Erro ao executar o Job");
                Sis.LogTrace("Fim do processamento");
                return 1;
            }

            return 0;
        }

    }
}
