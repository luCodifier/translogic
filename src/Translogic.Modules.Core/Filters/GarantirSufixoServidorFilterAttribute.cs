namespace Translogic.Modules.Core.Filters
{
	using System.Web;
	using System.Web.Mvc;
	using Microsoft.Practices.ServiceLocation;
	using Translogic.Core.Infrastructure.Settings;

	/// <summary>
	/// Utilizado para garantir que sempre seja utilizado o sufixo do servidor
	/// </summary>
	public class GarantirSufixoServidorFilterAttribute : ActionFilterAttribute
	{
		#region ATRIBUTOS READONLY & EST�TICOS

		private readonly ServerSettings settings = ServiceLocator.Current.GetInstance<ServerSettings>();

		#endregion

		#region M�TODOS

		/// <summary>
		/// Verifica��o se � necess�rio adicionar o sufixo
		/// </summary>
		/// <param name="filterContext">Contexto do filtro</param>
		public override void OnActionExecuting(ActionExecutingContext filterContext)
		{
			base.OnActionExecuting(filterContext);

			HttpRequestBase request = filterContext.RequestContext.HttpContext.Request;
			HttpResponseBase response = filterContext.RequestContext.HttpContext.Response;

			if (request.Url.IsLoopback)
			{
				return;
			}

			if (!settings.GarantirUsoDoSufixoServidor)
			{
				return;
			}

			if (request.IsAjaxRequest())
			{
				return;
			}

			if (!request.RequestType.Equals("GET"))
			{
				return;
			}

			if (request.Url.Host.Equals(request.ServerVariables["LOCAL_ADDR"]))
			{
				return;
			}

			if (!request.Url.Host.EndsWith(settings.SufixoServidor))
			{
				response.Write(string.Format(
				               	"<script>window.location.href='{0}';</script>",
				               	request.Url.AbsoluteUri.Replace(
									"//" + request.Url.Host,
				               	    "//" + request.Url.Host + settings.SufixoServidor)));
				response.End();
			}
		}

		#endregion
	}
}