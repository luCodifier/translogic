﻿namespace Translogic.Modules.Core.Filters
{
    using System;
    using System.Globalization;
    using System.Web.Mvc;

    /// <summary>
    /// Model binder custom para decimais
    /// </summary>
    public class DecimalModelBinder : DefaultModelBinder
    {
        /// <summary>
        /// Conversão de valor decimal
        /// </summary>
        /// <param name="controllerContext">Context do controller</param>
        /// <param name="bindingContext">Context do binder</param>
        /// <returns>Valor resultante</returns>
        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            ValueProviderResult valueResult = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);
            ModelState modelState = new ModelState { Value = valueResult };
            object actualValue = null;
            try
            {
                if (valueResult != null)
                {
                    actualValue = Convert.ToDecimal(valueResult.AttemptedValue, CultureInfo.CurrentCulture);
                }
            }
            catch (FormatException e)
            {
                modelState.Errors.Add(e);
            }

            bindingContext.ModelState.Add(bindingContext.ModelName, modelState);
            return actualValue;
        }
    }
}