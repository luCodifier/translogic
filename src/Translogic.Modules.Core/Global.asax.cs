﻿namespace Translogic.Modules.Core
{
    using Speed.Data;
	using System;
	using System.Globalization;
	using System.Security.Principal;
    using System.ServiceModel;
    using System.Threading;
	using System.Web;
	using System.Web.Hosting;
	using System.Web.Mvc;
    using System.Web.Routing;
	using System.Web.Security;
	using Translogic.Core.Infrastructure.Web;
	using Translogic.Modules.Core.Domain.Model.Acesso;
    using Translogic.Modules.Core.Spd.Data.Enums;

    /// <summary>
    /// Global.asax herdando do abstrato
    /// </summary>
    public class Global : AbstractGlobalApplication
	{
		#region MÉTODOS

		/// <summary>
		/// Inicializador para fazer a configuração dos componentes
		/// </summary>
		protected override void InstallWebComponents()
		{
            Sis.Inicializar(EnumSistema.Translogic, "Web");
            RouteTable.Routes.Clear();
			AreaRegistration.RegisterAllAreas();
			TranslogicStarterWeb.Setup();
			ControllerBuilder.Current.SetControllerFactory(Container.Resolve<IControllerFactory>());
			HostingEnvironment.RegisterVirtualPathProvider(new ModuleVirtualPathProvider());
			ViewEngines.Engines.Clear();
			ViewEngines.Engines.Add(new CustomViewEngine());
			//HibernatingRhinos.Profiler.Appender.NHibernate.NHibernateProfiler.Initialize();
		}

		/// <summary>
		/// Evento disparado quando for realizar a autenticação
		/// </summary>
		/// <param name="sender">Objeto que está disparando o evento</param>
		/// <param name="e">Argumentos do evento</param>
		protected void Application_AuthenticateRequest(object sender, EventArgs e)
		{
			HttpCookie authCookie = Request.Cookies[FormsAuthentication.FormsCookieName];

			if (authCookie != null)
			{
				FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(authCookie.Value);

				HttpContext.Current.User = new GenericPrincipal(new UsuarioIdentity(authTicket.Name), new string[0]);

				string culture = authTicket.UserData;

				Thread.CurrentThread.CurrentUICulture = GetCultureFromStringOrDefault(culture);
				Thread.CurrentThread.CurrentCulture = Thread.CurrentThread.CurrentUICulture;
			}
		}

		#endregion

		#region MÉTODOS ESTÁTICOS

		private static CultureInfo GetCultureFromStringOrDefault(string culture)
		{
			try
			{
				return new CultureInfo(culture);
			}
			catch (Exception)
			{
				return Cultura.Default.Culture;
			}
		}

		#endregion
	}
}