﻿using ALL.Core.Dominio;
using Translogic.Core.Infrastructure.Web;
using System.Collections.Generic;
using Translogic.Modules.Core.Domain.Model.Dto.AnexacaoDesanexacao;
using Translogic.Modules.Core.Domain.Model.AnexacaoDesanexacao;
namespace Translogic.Modules.Core.Domain.Services.AnexacaoDesanexacao.Interface
{
    public interface IAnexacaoDesanexacaoService
    {
        /// <summary>
        /// Retorna lista de registro  com carta de baldeio anexo conforme filtro informado pelo usuario
        /// </summary>
        /// <param name="detalhesPaginacaoWeb"></param>
        /// <param name="anexacaoDesanexacaoRequestDto"> Filtros pesquisa</param>
        /// <returns></returns>
        ResultadoPaginado<AnexacaoDesanexacaoDto> ObterConsultaAnexacaoDesanexacao(DetalhesPaginacaoWeb detalhesPaginacaoWeb, AnexacaoDesanexacaoRequestDto anexacaoDesanexacaoRequestDto);

        /// <summary>
        /// Retorna lista Anexação e Desanexação para exportação
        /// </summary>
        /// <param name="anexacaoDesanexacaoRequestDto"> Filtros exportação</param>
        /// <returns> Lista Anexação Desanexação</returns>
        IEnumerable<AnexacaoDesanexacaoDto> ObterConsultaAnexacaoDesanexacaoExportar(AnexacaoDesanexacaoRequestDto anexacaoDesanexacaoRequestDto);

        /// <summary>
        /// Lista Veiculos do Trem
        /// </summary>
        /// <param name="detalhesPaginacaoWeb"></param>
        /// <param name="os"> OS TREM</param>
        /// <returns> Lista Veiculos do trem</returns>
        ResultadoPaginado<VeiculoTremDto> ObterConsultaVeiculosTrem(DetalhesPaginacaoWeb detalhesPaginacaoWeb, decimal os, string estacao, string acao);

        AnexacaoDesanexacaoDto ObterPorOS(decimal os, string numeroVeiculo);

        ResultTravaTremDto EnviaSOAP(string operacao, decimal os, string usuario, string localParada);

        void ZeraTentativas(string operacao, decimal os);
    }
}