﻿namespace Translogic.Modules.Core.Domain.Services.LogAnxHistTrem
{
    using Translogic.Modules.Core.Domain.Model.AnexacaoDesanexacao.Repositories;
    using Translogic.Modules.Core.Domain.Services.AnexacaoDesanexacao.Interface;


    public class LogAnxHistTremService : ILogAnxHistTremService
    {

        private readonly ILogAnxHistTremRepository _LogAnxHistTremRepository;

        public LogAnxHistTremService(ILogAnxHistTremRepository LogAnxHistTremRepository)
        {
            _LogAnxHistTremRepository = LogAnxHistTremRepository;
        }
    }
}