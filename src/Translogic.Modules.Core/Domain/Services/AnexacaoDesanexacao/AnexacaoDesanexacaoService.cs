﻿namespace Translogic.Modules.Core.Domain.Services.AnexacaoDesanexacao
{
    using Translogic.Modules.Core.Domain.Services.AnexacaoDesanexacao.Interface;
    using Translogic.Modules.Core.Domain.Model.AnexacaoDesanexacao.Repositories;
    using Translogic.Core.Infrastructure.Web;
    using ALL.Core.Dominio;
    using System.Collections.Generic;
    using Translogic.Modules.Core.Domain.Model.Dto.AnexacaoDesanexacao;
    using Translogic.Modules.Core.Domain.Model.AnexacaoDesanexacao;
    using Translogic.Modules.Core.Util;
    using System;
    using System.Text;
    using System.Xml;

    public class AnexacaoDesanexacaoService : IAnexacaoDesanexacaoService
    {

        private readonly IAnexacaoDesanexacaoRepository _anexacaoDesanexacaoRepository;
        private readonly IWebService _webService;

        public AnexacaoDesanexacaoService(IAnexacaoDesanexacaoRepository anexacaoDesanexacaoRepository, IWebService webService)
        {
            _anexacaoDesanexacaoRepository = anexacaoDesanexacaoRepository;
            _webService = webService;
        }

        public ResultadoPaginado<AnexacaoDesanexacaoDto> ObterConsultaAnexacaoDesanexacao(DetalhesPaginacaoWeb detalhesPaginacaoWeb, AnexacaoDesanexacaoRequestDto anexacaoDesanexacaoRequestDto)
        {
            var result = _anexacaoDesanexacaoRepository.ObterConsultaAnexacaoDesanexacao(detalhesPaginacaoWeb, anexacaoDesanexacaoRequestDto);
            
            return result;
        }

        public IEnumerable<AnexacaoDesanexacaoDto> ObterConsultaAnexacaoDesanexacaoExportar(AnexacaoDesanexacaoRequestDto anexacaoDesanexacaoRequestDto)
        {
            var result = _anexacaoDesanexacaoRepository.ObterConsultaAnexacaoDesanexacaoExportar(anexacaoDesanexacaoRequestDto);

            return result;
        }

        public ResultadoPaginado<VeiculoTremDto> ObterConsultaVeiculosTrem(DetalhesPaginacaoWeb detalhesPaginacaoWeb, decimal os, string estacao, string acao)
        {
            var result = _anexacaoDesanexacaoRepository.ObterConsultaVeiculosTrem(detalhesPaginacaoWeb, os, estacao, acao);

            return result;
        }
        
        //public BaldeioPermissaoDto BuscarPermissaoUsuario(Usuario usuario)
        //{
        //    var permissao = new BaldeioPermissaoDto();
        //    var acao = "CONSULTABALDEIO";

        //    permissao.Pesquisar = ServiceLocator.Current.GetInstance<ControleAcessoService>().Autorizar(acao, "Pesquisar", usuario);
        //    permissao.Importar = ServiceLocator.Current.GetInstance<ControleAcessoService>().Autorizar(acao, "Importar", usuario);
        //    permissao.ExcluirCarta = ServiceLocator.Current.GetInstance<ControleAcessoService>().Autorizar(acao, "ExcluirCarta", usuario);

        //    return permissao;
        //}
        public AnexacaoDesanexacaoDto ObterPorOS(decimal os, string numeroVeiculo)
        {
            return  _anexacaoDesanexacaoRepository.ObterPorOS(os, numeroVeiculo);        
        }

        public ResultTravaTremDto EnviaSOAP(string operacao, decimal os, string usuario, string localParada) 
        {
            var result = new ResultTravaTremDto();
            //ResultTravaTremDto result;
            result =  _webService.ExecutarTravaTrem(os, usuario);

            if (result.conformidade.Equals("true"))
            {
                decimal idTrem = _anexacaoDesanexacaoRepository.ObterIdTrem(os);
                decimal idLocal = _anexacaoDesanexacaoRepository.ObterIdLocal(localParada);
                decimal idParada = _anexacaoDesanexacaoRepository.ObterIdParada(os, idLocal);
                string xml = MontaXMLEncerrarParada(usuario, idParada, idTrem);

                string resultEncerrarParadaPKG = _anexacaoDesanexacaoRepository.EncerrarParadaPKG(xml);

                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(resultEncerrarParadaPKG);

                XmlNodeList nodeXML = xmlDoc.GetElementsByTagName("XML");
                string retornoXML = nodeXML[0].InnerText;
                if (retornoXML == "")
                {
                    //var objLog = _anexacaoDesanexacaoRepository.ObterPorOperacaoOsLocalParada(operacao, os,localParada);
                    //objLog.StatusParada = "FECHADA";
                    //_anexacaoDesanexacaoRepository.Atualizar(objLog);
                    _anexacaoDesanexacaoRepository.UpdateSatusParada(operacao, os, localParada);

                }
                else {
                    XmlNodeList nodeERRO = xmlDoc.GetElementsByTagName("XML");
                    result.retornoPKGEncerrarParada = nodeERRO[0].InnerText;
                }
            }
            //var objOS = _anexacaoDesanexacaoRepository.ObterPorOS(os);

            return result; 
        }

        public void ZeraTentativas(string operacao, decimal os) 
        {
            _anexacaoDesanexacaoRepository.ZeraTentativas(operacao, os);
        }

        private string MontaXMLEncerrarParada(string usuario, decimal idParada, decimal idTrem)
        {

            #region Monta XML
            string xml;
            StringBuilder sb = new StringBuilder();

            sb.Append("<?xml version=\"1.0\" encoding=\"iso-8859-1\"?>");
            sb.Append("<XML>");
            sb.Append("<ENCERRA_PARADA");
            sb.Append(" USR=\":USUARIO\"");
            sb.Append(" ID_PARADA=\":ID_PARADA\"");
            sb.Append(" ID_TREM=\":ID_TREM\"");
            sb.Append(" />");
            sb.Append("</XML>");

            xml = sb.ToString().Replace(":USUARIO", usuario)
            .Replace(":ID_PARADA", idParada.ToString())
            .Replace(":ID_TREM", idTrem.ToString());

            #endregion

            return xml;
        }
    }
}