﻿namespace Translogic.Modules.Core.Domain.Services.VooAtivo.Interface
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Translogic.Core.Infrastructure.Web;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Modules.Core.Util;
    using ALL.Core.Dominio;
    using Translogic.Modules.Core.Domain.Model.VooAtivo;

    public interface ISolicitacaoVooService
    {
        /// <summary>
        /// Pesquisar ativos disponíveis de acordo com o tipo e demais filtros
        /// </summary>
        /// <param name="detalhesPaginacaoWeb"></param>
        /// <param name="ativos"></param>
        /// <param name="tipo"></param>
        /// <param name="patio"></param>
        /// <param name="linha"></param>
        /// <param name="os"></param>
        /// <returns></returns>
        ResultadoPaginado<SolicitacaoVooAtivoItemDto> PesquisaAtivosDisponiveis(DetalhesPaginacaoWeb detalhesPaginacaoWeb, string ativos, EnumTipoAtivo tipo, string patio, string linha, string os);

        /// <summary>
        /// Pesquisar ativos avaliar
        /// </summary>
        /// <param name="detalhesPaginacaoWeb"></param>
        /// <param name="id"></param>
        /// <param name="dtPeriodoInicio"></param>
        /// <param name="dtPeriodoFinal"></param>
        /// <param name="Solicitante"></param>
        /// <param name="StatusChamado"></param>
        /// <param name="tipoSolicitacao"></param>
        /// <returns></returns>
        ResultadoPaginado<SolicitacaoVooAtivosAvaliarDto> BuscarAtivosVooAvaliar(DetalhesPaginacaoWeb detalhesPaginacaoWeb, int? id, DateTime? dtPeriodoInicio, DateTime? dtPeriodoFinal, string Solicitante, int? idStatusChamado, int? idTipoSolicitacao);
   

        /// <summary>
        /// Salvar as solicitação com status aberto e todos os itens com status pendente enviando assim para aprovação.
        /// </summary>
        /// <param name="tipoAtivo"></param>
        /// <param name="justificativa"></param>
        /// <param name="motivo"></param>
        /// <param name="patio"></param>
        /// <param name="solicitante"></param>
        /// <param name="dataHoraChegada"></param>
        /// <param name="ativos"></param>
        /// <param name="usuario"></param>
        void EnviarAprovacao(int tipoAtivo, string justificativa, string motivo, string patio, string solicitante, string patioSolicitante, DateTime dataHoraChegada, List<SolicitacaoVooAtivoItem> ativos, string usuario);

        /// <summary>
        /// Busca dados Item ativo
        /// </summary>
        /// <param name="id"></param>
        SolicitacaoVooAtivoDto BuscaDadosAtivosVooAvaliarPorID(int id);

        /// <summary>
        /// Retorna items da solicitação
        /// </summary>
        /// <param name="detalhesPaginacaoWeb"></param>
        /// <param name="id"></param>
        ResultadoPaginado<SolicitacaoVooAtivoItemDto> BuscarItemsAtivosVooAvaliarPorID(DetalhesPaginacaoWeb detalhesPaginacaoWeb, int id, EnumTipoAtivo tipoAtivo);

        /// <summary>
        /// Aprova items da solicitação .
        /// </summary>
        /// <param name="id"></param>
        /// <param name="justificativa"></param>
        /// <param name="dataHoraChegada"></param>
        /// <param name="solicitante"></param>
        /// <param name="areaResponsavel"></param>
        /// <param name="justificativaCCO"></param>
        /// <param name="observacaoCCO"></param>
        /// <param name="items"></param>
        string Aprovar(int id, string motivo, string justificativa, DateTime dataHoraChegada, string solicitante, string areaResponsavel, string justificativaCCO, string observacaoCCO, List<int> items, string usuario, string patioDestino);
        /// <summary>
        /// Reprova items da solicitação .
        /// </summary>
        /// <param name="id"></param>
        /// <param name="justificativa"></param>
        /// <param name="dataHoraChegada"></param>
        /// <param name="solicitante"></param>
        /// <param name="areaResponsavel"></param>
        /// <param name="justificativaCCO"></param>
        /// <param name="observacaoCCO"></param>
        /// <param name="items"></param>
        void Reprovar(int id, string motivo, string justificativa, DateTime dataHoraChegada, string solicitante, string areaResponsavel, string justificativaCCO, string observacaoCCO, List<int> items, string usuario, string patioDestino);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="motivo"></param>
        /// <param name="justificativa"></param>
        /// <param name="dataHoraChegada"></param>
        /// <param name="solicitante"></param>
        /// <param name="areaResponsavel"></param>
        /// <param name="justificativaCCO"></param>
        /// <param name="observacaoCCO"></param>
        /// <param name="items"></param>
        /// <param name="usuario"></param>
        /// <param name="patioDestino"></param>
        /// <returns></returns>
        string Preparar(int id, string motivo, string justificativa, DateTime dataHoraChegada, string solicitante, string areaResponsavel, string justificativaCCO, string observacaoCCO, List<int> items, string usuario, string patioDestino);
    }
}
