﻿namespace Translogic.Modules.Core.Domain.Services.VooAtivo.Interface
{
    using System.Collections.Generic;
    using Translogic.Modules.Core.Domain.Model.VooAtivo;
    using Translogic.Modules.Core.Util;

    public interface IMotivoVooService
    {
        /// <summary>
        /// Buscar motivos de voos de acordo com o tipo
        /// </summary>
        /// <param name="tipo"></param>
        /// <returns></returns>
        IList<SolicitacaoVooMotivo> BuscarMotivos(EnumTipoAtivo tipo);
    }
}
