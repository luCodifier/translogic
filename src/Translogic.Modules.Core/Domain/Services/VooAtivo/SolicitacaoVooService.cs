﻿namespace Translogic.Modules.Core.Domain.Services.VooAtivo
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using Translogic.Modules.Core.Domain.Services.VooAtivo.Interface;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Core.Infrastructure.Web;
    using Translogic.Modules.Core.Util;
    using Translogic.Modules.Core.Domain.Model.VooAtivo;
    using ALL.Core.Dominio;
    using System.Text;
using System.Text.RegularExpressions;

    public class SolicitacaoVooService : ISolicitacaoVooService
    {

        #region Repositórios

        private readonly ISolicitacaoVooRepository _solicitacaoVooRepository;
        private readonly ISolicitacaoVooItemRepository _solicitacaoVooItemRepository;
        private readonly ISolicitacaoTipoAtivoRepository _solicitacaoTipoAtivoRepository;
        private readonly ISolicitacaoVooAtivoStatusRepository _solicitacaoVooAtivoStatusRepository;
        private readonly ISolicitacaoVooAtivoStatusItemRepository _solicitacaoVooAtivoStatusItemRepository;
        private readonly IVooMotivoRepository _vooMotivoRepository;

        #endregion

        #region Construtores

        public SolicitacaoVooService(ISolicitacaoVooRepository solicitacaoVooRepository,
            ISolicitacaoVooItemRepository solicitacaoVooItemRepository,
            ISolicitacaoTipoAtivoRepository solicitacaoTipoAtivoRepository,
            ISolicitacaoVooAtivoStatusRepository solicitacaoVooAtivoStatusRepository,
            ISolicitacaoVooAtivoStatusItemRepository solicitacaoVooAtivoStatusItemRepository,
            IVooMotivoRepository vooMotivoRepository)
        {
            _solicitacaoVooRepository = solicitacaoVooRepository;
            _solicitacaoVooItemRepository = solicitacaoVooItemRepository;
            _solicitacaoTipoAtivoRepository = solicitacaoTipoAtivoRepository;
            _solicitacaoVooAtivoStatusRepository = solicitacaoVooAtivoStatusRepository;
            _solicitacaoVooAtivoStatusItemRepository = solicitacaoVooAtivoStatusItemRepository;
            _vooMotivoRepository = vooMotivoRepository;
        }
      
        #endregion

        #region Métodos
        
        public ResultadoPaginado<SolicitacaoVooAtivoItemDto> PesquisaAtivosDisponiveis(DetalhesPaginacaoWeb detalhesPaginacaoWeb, string ativos, EnumTipoAtivo tipo, string patio, string linha, string os)
        {
            /* User Story 4771:R18: Solicitação do Voo já existente e pendente para item da Busca:
             * Após o usuário clicar em buscar registros o sistem deve validar na tabela SOLICITACAO_VOO_ATIVOS_ITEM se o item requisitado não está pendente em alguma solicitação de voo
             */

            switch (tipo)
            {
                case EnumTipoAtivo.Eot: 
                    {
                        return _solicitacaoVooItemRepository.BuscarEotDisponiveisVoo(detalhesPaginacaoWeb, ativos, patio, os);
                    }
                case EnumTipoAtivo.Locomotiva:
                    {
                        return _solicitacaoVooItemRepository.BuscarLocomotivasDisponiveisVoo(detalhesPaginacaoWeb, ativos, patio, linha, os);
                    }
                case EnumTipoAtivo.Vagao:
                    {
                        return _solicitacaoVooItemRepository.BuscarVagoesDisponiveisVoo(detalhesPaginacaoWeb, ativos, patio, linha, os);
                    }
                case EnumTipoAtivo.VagaoRefaturamento:
                    {
                        return _solicitacaoVooItemRepository.BuscarVagoesRefaturamentoDisponiveisVoo(detalhesPaginacaoWeb, ativos, os);
                    }
                default:
                    return _solicitacaoVooItemRepository.BuscarVagoesDisponiveisVoo(detalhesPaginacaoWeb, ativos, patio, linha, os);
            }
        }

        public ResultadoPaginado<SolicitacaoVooAtivosAvaliarDto> BuscarAtivosVooAvaliar(DetalhesPaginacaoWeb detalhesPaginacaoWeb, int? id, DateTime? dtPeriodoInicio, DateTime? dtPeriodoFinal, string solicitante, int? idStatusChamado, int? idTipoSolicitacao)
        {
            return _solicitacaoVooRepository.BuscarAtivosVooAvaliar(detalhesPaginacaoWeb, id, dtPeriodoInicio, dtPeriodoFinal, solicitante, idStatusChamado, idTipoSolicitacao);
        }

        public ResultadoPaginado<SolicitacaoVooAtivoItemDto> BuscarItemsAtivosVooAvaliarPorID(DetalhesPaginacaoWeb detalhesPaginacaoWeb, int id, EnumTipoAtivo tipoAtivo)
        {
            switch (tipoAtivo)
            {
                case EnumTipoAtivo.Vagao:
                    {
                        return _solicitacaoVooItemRepository.BuscarItemsAtivosVooAvaliarVagaoPorID(detalhesPaginacaoWeb, id);
                    }
                case EnumTipoAtivo.Locomotiva:
                    {
                        return _solicitacaoVooItemRepository.BuscarItemsAtivosVooAvaliarLocomotivaPorID(detalhesPaginacaoWeb, id);
                    }
                case EnumTipoAtivo.Eot:
                    {
                        return _solicitacaoVooItemRepository.BuscarItemsAtivosVooAvaliarEotPorID(detalhesPaginacaoWeb, id);
                    }
                case EnumTipoAtivo.VagaoRefaturamento:
                    {
                        return _solicitacaoVooItemRepository.BuscarItemsAtivosVooAvaliarVagaoRefaturamentoPorID(detalhesPaginacaoWeb, id);
                    }
                default:
                    return _solicitacaoVooItemRepository.BuscarItemsAtivosVooAvaliarVagaoPorID(detalhesPaginacaoWeb, id);
            }
        }

        public SolicitacaoVooAtivoDto BuscaDadosAtivosVooAvaliarPorID(int id)
        {
            var item = _solicitacaoVooRepository.ObterPorId(id);

            var itemAtivoVoo = new SolicitacaoVooAtivoDto();

            itemAtivoVoo.Id = item.Id;
            itemAtivoVoo.Motivo = item.Motivo;
            itemAtivoVoo.Justificativa = item.Justificativa;
            itemAtivoVoo.dtHrChegada = item.DataHoraChegada;
            itemAtivoVoo.Solicitante = item.Solicitante;
            itemAtivoVoo.AreaResponsavel = item.AreaResponsavel;
            itemAtivoVoo.JustificativaCCO = item.JustificativaCCO;
            itemAtivoVoo.ObservacaoCCO = item.ObservacaoCCO;
            itemAtivoVoo.tipoAtivo = item.SolicitacaoTipoAtivo.Id;
            itemAtivoVoo.PatioDestino = item.PatioDestino;
            itemAtivoVoo.PatioSolicitante = item.PatioSolicitante;
            itemAtivoVoo.Status = item.SolicitacaoVooAtivoStatus.Id;

            return itemAtivoVoo;
        }

        public void EnviarAprovacao(int tipoAtivo, string justificativa, string motivo, string patio, string solicitante, string patioSolicitante, DateTime dataHoraChegada, List<SolicitacaoVooAtivoItem> ativos, string usuario)
        {
            var solicitacaoVooAtivoStatus = ObterStatusAtivo((int)EnumStatusAtivo.Aberto);
            var solicitacaoTipoAtivo = ObterTipoAtivo(tipoAtivo);

            var vooAtivo = new SolicitacaoVooAtivo() { 
                Justificativa = StringUtil.RemoveAspas(justificativa),
                Motivo = motivo,
                PatioDestino = patio.ToUpper(),
                Solicitante = solicitante,
                PatioSolicitante = patioSolicitante.ToUpper(),
                DataHoraChegada = dataHoraChegada,
                DataSolicitacao = DateTime.Now,
                DataUltimaAlteracao = DateTime.Now,
                Usuario = usuario,
                LinhaDestino = Constantes.LINHA_DESTINO_SOLICITACAO_VOO_DEFAULT,
                SolicitacaoVooAtivoStatus = solicitacaoVooAtivoStatus,
                SolicitacaoTipoAtivo = solicitacaoTipoAtivo,
                VersionDate = DateTime.Now
            };

            _solicitacaoVooRepository.Inserir(vooAtivo);

            // Inserir os itens do ativo
            var solicitacaoVooAtivoStatusItem = ObterStatusAtivoItem((int)EnumStatusAtivoItem.Pendente);
            
            foreach(var ativo in ativos){

                ativo.SolicitacaoVooAtivo = vooAtivo;
                ativo.SolicitacaoTipoAtivo = solicitacaoTipoAtivo;
                ativo.SolicitacaoVooAtivoStatusItem = solicitacaoVooAtivoStatusItem;
                ativo.VersionDate = DateTime.Now;

                _solicitacaoVooItemRepository.Inserir(ativo);
            }
        }

        public string Aprovar(int id, string motivo, string justificativa, DateTime dataHoraChegada, string solicitante, string areaResponsavel, string justificativaCCO, string observacaoCCO, List<int> items, string usuario, string patioDestino)
        {
            var objVooAtivo = _solicitacaoVooRepository.ObterPorId(id);
            // Inserir os itens do ativo
            var solicitacaoVooAtivoStatusItem = ObterStatusAtivoItem((int)EnumStatusAtivoItem.Aprovado);

            var tipoAtivo = (EnumTipoAtivo)objVooAtivo.SolicitacaoTipoAtivo.Id;


            var executouTodasAsPkgs = true;

            string mensagem = "";
            var quantAtualizados = 0;

            var countItens = items.Count;
            
            for (int i = items.Count - 1; i < items.Count && i >= 0; i--)
            {
                var objVooAtivoItem = _solicitacaoVooItemRepository.ObterPorId(items[i]);
                try
                {
                    mensagem = AprovarPKG(dataHoraChegada, usuario, tipoAtivo, patioDestino, justificativa, objVooAtivoItem.Ativo, solicitante, motivo);
                    //Se retornou estado futuro não permitido na execução de qualquer item, pula os outros
                    if (mensagem == "Estado futuro não permitido!")
                    {
                        break;
                    }

                    if (mensagem == "<XML><SUCESSO>OK</SUCESSO></XML>" || string.IsNullOrEmpty(mensagem))
                    {
                        objVooAtivoItem.SolicitacaoVooAtivoStatusItem = solicitacaoVooAtivoStatusItem;
                        objVooAtivoItem.VersionDate = DateTime.Now;

                        // TODO: Atualizar data ultimo evento

                        _solicitacaoVooItemRepository.Atualizar(objVooAtivoItem);
                        quantAtualizados++;
                    }
                }
                catch (Exception ex)
                {
                    executouTodasAsPkgs = false;
                    mensagem = "Erro ao executar Voo. Erro: " + ex.Message;
                    break;
                }
            }

            // Se não houver erro na mensagem se todos os itens recebidos foram atualizados
            if (items.Count == quantAtualizados)
            {
                objVooAtivo.Motivo = motivo;
                objVooAtivo.DataHoraChegada = dataHoraChegada;
                objVooAtivo.AreaResponsavel = areaResponsavel;
                objVooAtivo.JustificativaCCO = justificativaCCO;
                objVooAtivo.ObservacaoCCO = observacaoCCO;
                objVooAtivo.DataUltimaAlteracao = DateTime.Now;
                objVooAtivo.VersionDate = DateTime.Now;

                // Se não houver itens pendentes atualizar status para Encerrado
                if (SemItensPendentes(id, tipoAtivo) && executouTodasAsPkgs)
                    objVooAtivo.SolicitacaoVooAtivoStatus = ObterStatusAtivo((int)EnumStatusAtivo.Encerrado);

                _solicitacaoVooRepository.Atualizar(objVooAtivo);
            }

            //Se deu erro em algum item da aprovação retorna erro
            if (mensagem == "<XML><SUCESSO>OK</SUCESSO></XML>" && items.Count > quantAtualizados)
            {
                mensagem = "Item(s) da lista possui estado futuro não permitido!";
            }

            return mensagem;
        }

        public void Reprovar(int id, string motivo, string justificativa, DateTime dataHoraChegada, string solicitante, string areaResponsavel, string justificativaCCO, string observacaoCCO, List<int> items, string usuario, string patioDestino)
        {
            var objVooAtivo = _solicitacaoVooRepository.ObterPorId(id);
            // Inserir os itens do ativo
            var solicitacaoVooAtivoStatusItem = ObterStatusAtivoItem((int)EnumStatusAtivoItem.Reprovado);
            
            foreach (var ativo in items)
            {
                var objVooAtivoItem = _solicitacaoVooItemRepository.ObterPorId(ativo);

                objVooAtivoItem.SolicitacaoVooAtivoStatusItem = solicitacaoVooAtivoStatusItem;
                objVooAtivoItem.VersionDate = DateTime.Now;

                _solicitacaoVooItemRepository.Atualizar(objVooAtivoItem);         
            }

            objVooAtivo.Motivo = motivo;
            objVooAtivo.DataHoraChegada = dataHoraChegada;
            objVooAtivo.AreaResponsavel = areaResponsavel;
            objVooAtivo.JustificativaCCO = justificativaCCO;
            objVooAtivo.ObservacaoCCO = observacaoCCO;
            objVooAtivo.DataUltimaAlteracao = DateTime.Now;
            objVooAtivo.VersionDate = DateTime.Now;

            var tipoAtivo = (EnumTipoAtivo)objVooAtivo.SolicitacaoTipoAtivo.Id;
            if (tipoAtivo == EnumTipoAtivo.VagaoRefaturamento)
                objVooAtivo.SolicitacaoVooAtivoStatus = ObterStatusAtivo((int)EnumStatusAtivo.Encerrado);
            else if (SemItensPendentes(id, tipoAtivo)) // Se não houver itens pendentes atualizar status para Encerrado
                objVooAtivo.SolicitacaoVooAtivoStatus = ObterStatusAtivo((int)EnumStatusAtivo.Encerrado);

            _solicitacaoVooRepository.Atualizar(objVooAtivo);
        }

        public string AprovarPKG(DateTime dtHoraChegada, string usuario, EnumTipoAtivo tipoAtivo, string patioDestino, string justificativa, string ativo, string solicitante, string motivo)
        {
            var solicitacaoItem = new SolicitacaoVooAtivoItemPkgDto();
            var xml = string.Empty;

            //string xml = MontaXML(id,motivo,justificativa,dataHoraChegada,solicitante,areaResponsavel,justificativaCCO,observacaoCCO,item);
            switch (tipoAtivo)
            {
                case EnumTipoAtivo.Eot:
                    {
                        solicitacaoItem = _solicitacaoVooItemRepository.BuscarAtivosEotsParaPkgs(ativo, patioDestino);
                            if (!solicitacaoItem.IdEstadoFuturo.HasValue)
                            {
                                return "Item(s) com Estado futuro não permitido!";
                            }
                            xml = MontaXMLEot(dtHoraChegada, usuario, motivo, StringUtil.RemoveAspas(justificativa), solicitacaoItem, tipoAtivo);
                            return _solicitacaoVooItemRepository.AprovarEotPKG(xml);
                    }
                case EnumTipoAtivo.Locomotiva:
                    {
                        solicitacaoItem = _solicitacaoVooItemRepository.BuscarAtivosLocomotivasParaPkgs(ativo, patioDestino);
                            if (!solicitacaoItem.IdEstadoFuturo.HasValue)
                            {
                                return "Item(s) com Estado futuro não permitido!";
                            }
                            xml = MontaXMLLocomotiva(dtHoraChegada, usuario, motivo, StringUtil.RemoveAspas(justificativa), solicitacaoItem, tipoAtivo);
                        return _solicitacaoVooItemRepository.AprovarLocomotivaPKG(xml); 
                    }
                case EnumTipoAtivo.Vagao:
                    {
                        solicitacaoItem = _solicitacaoVooItemRepository.BuscarAtivosVagoesParaPkgs(ativo, patioDestino);
                            if (!solicitacaoItem.IdEstadoFuturo.HasValue)
                            {
                                return "Item(s) com Estado futuro não permitido!";
                            }
                            xml = MontaXMLVagao(dtHoraChegada, usuario, motivo, StringUtil.RemoveAspas(justificativa), solicitacaoItem, tipoAtivo);
                        return _solicitacaoVooItemRepository.AprovarVagaoPKG(xml);
                    }
                case EnumTipoAtivo.VagaoRefaturamento:
                    {
                        solicitacaoItem = _solicitacaoVooItemRepository.BuscarAtivosVagoesRefaturamentoParaPkgs(ativo, patioDestino);
                        if (!solicitacaoItem.IdEstadoFuturo.HasValue)
                        {
                            return "Item(s) com Estado futuro não permitido!";
                        }
                        xml = MontaXMLVagao(dtHoraChegada, usuario, motivo, StringUtil.RemoveAspas(justificativa), solicitacaoItem, tipoAtivo);
                        return _solicitacaoVooItemRepository.AprovarVagaoPKG(xml);
                    }
            }
            return null;
        }

        public string Preparar(int id, string motivo, string justificativa, DateTime dataHoraChegada, string solicitante, string areaResponsavel, string justificativaCCO, string observacaoCCO, List<int> items, string usuario, string patioDestino)
        {
            var objVooAtivo = _solicitacaoVooRepository.ObterPorId(id);

            var tipoAtivo = (EnumTipoAtivo)objVooAtivo.SolicitacaoTipoAtivo.Id;
            var solicitacaoVooAtivoStatusItem = ObterStatusAtivoItem((int)EnumStatusAtivoItem.Refaturando);

            var dadosPrepararRefaturamento = _solicitacaoVooItemRepository.BuscarDadosPrepararRefaturamento(id);

            RefatVagoesDto refatVagoesDto = new RefatVagoesDto();
            refatVagoesDto.ListaVagao = new List<RefatVagaoDto>();

            refatVagoesDto.IdTrem = dadosPrepararRefaturamento.Rows[0]["idTrem"].ToString();
            refatVagoesDto.IdComposicao = dadosPrepararRefaturamento.Rows[0]["idComposicao"].ToString();
            refatVagoesDto.Usuario = usuario;
            refatVagoesDto.IdMotivo = motivo;
            refatVagoesDto.Solicitante = solicitante;
            refatVagoesDto.Justificativa = justificativa;

            string mensagem = "";

            //Monta Lista Vagoes DTO para execução PKG posteriormente
            for (int row = 0; row < dadosPrepararRefaturamento.Rows.Count; row++)
            {
                refatVagoesDto.ListaVagao.Add(new RefatVagaoDto()
                {
                    IdVagao = dadosPrepararRefaturamento.Rows[row]["IdVagao"].ToString(),
                    IdDespacho = dadosPrepararRefaturamento.Rows[row]["IdDespacho"].ToString()
                });                                                                             
            }
                            
            //Executa PKG
            mensagem = _solicitacaoVooItemRepository.PrepararVagaoRefaturamentoPKG(MontaXMLVagaoRefaturamento(refatVagoesDto,tipoAtivo));

            //Se deu erro em algum item em preparação retorna erro
            if (!string.IsNullOrEmpty(mensagem) && !mensagem.Contains("ERRO"))
            {
                //Se processo PKG rodou 100% atualiza dados grid e tela
                foreach (var ativo in items)
                {
                    var objVooAtivoItem = _solicitacaoVooItemRepository.ObterPorId(ativo);

                    objVooAtivoItem.SolicitacaoVooAtivoStatusItem = solicitacaoVooAtivoStatusItem;
                    objVooAtivoItem.VersionDate = DateTime.Now;
                    objVooAtivoItem.DataEvento = _solicitacaoVooItemRepository.BuscarDataUltimoEventoVagao(objVooAtivoItem.Ativo);

                    _solicitacaoVooItemRepository.Atualizar(objVooAtivoItem);

                }

                objVooAtivo.Motivo = motivo;
                objVooAtivo.DataHoraChegada = dataHoraChegada;
                objVooAtivo.AreaResponsavel = areaResponsavel;
                objVooAtivo.JustificativaCCO = justificativaCCO;
                objVooAtivo.ObservacaoCCO = observacaoCCO;
                objVooAtivo.DataUltimaAlteracao = DateTime.Now;
                objVooAtivo.VersionDate = DateTime.Now;

                _solicitacaoVooRepository.Atualizar(objVooAtivo);              
            }

            return mensagem;
        }

        /// <summary>
        /// Verificar se existem itens pendentes
        /// </summary>
        /// <param name="id"></param>
        /// <param name="tipoAtivo"></param>
        /// <returns></returns>
        private bool SemItensPendentes(int id, EnumTipoAtivo tipoAtivo)
        {
            decimal count = 0;
            switch (tipoAtivo)
            {
                case EnumTipoAtivo.Vagao:
                    {
                        count = _solicitacaoVooItemRepository.BuscarItemsVagoesPendentes(id);
                        break;
                    }
                case EnumTipoAtivo.Locomotiva:
                    {
                        count = _solicitacaoVooItemRepository.BuscarItemsLocomotivasPendentes(id);
                        break;
                    }

                case EnumTipoAtivo.Eot:
                    {
                        count = _solicitacaoVooItemRepository.BuscarItemsEotsPendentes(id);
                        break;
                    }
            }
            if (count > 0)
                return false;
            else
                return true;
        }
        
        private string MontaXMLLocomotiva(DateTime dataHoraChegada, string usuario, string motivo, string justificativa, SolicitacaoVooAtivoItemPkgDto solicitacaoItem, EnumTipoAtivo tipoAtivo)
        {
            #region Monta XML
            string xml;
            StringBuilder sb = new StringBuilder();

            sb.Append("<?xml version=\"1.0\" encoding=\"iso-8859-1\"?>");
            sb.Append("<ATRIBUTOS ");
            sb.Append(" DATA_HORA_SAIDA=\":DATA_HORA_SAIDA\"");
            sb.Append(" DATA_HORA_ENTRADA=\":DATA_HORA_ENTRADA\"");
            sb.Append(" DATA_HORA_ENTRADA_PREVISTA=\":DATA_HORA_PREVISTA\"");
            sb.Append(" ID_LOCAL=\":ID_LOCAL\"");
            sb.Append(" ID_RESPONSAVEL=\":ID_RESPONSAVEL\"");
            sb.Append(" ID_SITUACAO=\":ID_SITUACAO\"");
            sb.Append(" ID_INTERCAMBIO=\":ID_INTERCAMBIO\"");
            sb.Append(" ID_COND_USO=\":ID_COND_USO\"");
            sb.Append(" AO_ID_DESTINO=\":AO_ID_DESTINO\"");
            sb.Append(" ID_LINHA_DESTINO=\":ID_LINHA_DESTINO\"");
            sb.Append(" US_USR_IDU=\":USUARIO\">");
            sb.Append(" <MOTIVO_VOADOR ID_MOTIVO=\":ID_MOTIVO\" SOLICITANTE=\":SOLICITANTE\" JUSTIFICATIVA=\":JUSTIFICATIVA\"  />");
            sb.Append(" <LISTA_LOCOS>");
            sb.Append(" <LOCO ID_LOCO=\":ID_LOCO\" COD_LOCO=\":COD_LOCO\" NUM_SEQ=\":NUM_SEQ\" AO_ID_ORIGEM=\":AO_ID_ORIGEM\" ID_LINHA_ORIGEM=\":ID_LINHA_ORIGEM\" />");
            sb.Append(" </LISTA_LOCOS>");
            sb.Append(" </ATRIBUTOS>");

            xml = sb.ToString().Replace(":DATA_HORA_SAIDA", dataHoraChegada.AddDays(-1).ToString("dd/MM/yyyy HH:mm:ss"))
                .Replace(":DATA_HORA_ENTRADA", dataHoraChegada.ToString("dd/MM/yyyy HH:mm:ss"))
                .Replace(":DATA_HORA_PREVISTA", dataHoraChegada.ToString("dd/MM/yyyy HH:mm:ss"))
                .Replace(":ID_LOCAL", solicitacaoItem.IdLocal.ToString())
                .Replace(":ID_RESPONSAVEL", solicitacaoItem.IdResponsavel.ToString())
                .Replace(":ID_SITUACAO", solicitacaoItem.IdSituacao.ToString())
                .Replace(":ID_LOTACAO", solicitacaoItem.IdLotacao.ToString())
                .Replace(":ID_INTERCAMBIO", solicitacaoItem.IdIntercambio.ToString())
                .Replace(":ID_COND_USO", solicitacaoItem.IdCondUso.ToString())
                .Replace(":AO_ID_DESTINO", solicitacaoItem.IdPatioDestino.ToString())
                .Replace(":ID_LINHA_DESTINO", solicitacaoItem.IdLinhaDestino.ToString())
                .Replace(":USUARIO", usuario)
                .Replace(":ID_MOTIVO", _vooMotivoRepository.BuscaIDPorDescricaoMotivo(tipoAtivo,motivo).ToString())
                .Replace(":SOLICITANTE", usuario)
                .Replace(":JUSTIFICATIVA", justificativa)
                .Replace(":ID_LOCO", solicitacaoItem.IdAtivo.ToString())
                .Replace(":NUM_SEQ", "1")
                .Replace(":COD_LOCO", solicitacaoItem.Ativo.ToString())
                .Replace(":AO_ID_ORIGEM", solicitacaoItem.IdPatioOrigem.ToString())
                .Replace(":ID_LINHA_ORIGEM", solicitacaoItem.IdLinhaOrigem.ToString());
            #endregion

            return xml;
        }

        private string MontaXMLVagao(DateTime dataHoraChegada, string usuario, string motivo, string justificativa, SolicitacaoVooAtivoItemPkgDto solicitacaoItem, EnumTipoAtivo tipoAtivo)
        {

            #region Monta XML
            string xml;
            StringBuilder sb = new StringBuilder();
           
            sb.Append("<?xml version=\"1.0\" encoding=\"iso-8859-1\"?>");
            sb.Append("<ATRIBUTOS ");
            sb.Append(" DATA_HORA_SAIDA=\":DATA_HORA_SAIDA\"");
            sb.Append(" DATA_HORA_ENTRADA=\":DATA_HORA_ENTRADA\"");
            sb.Append(" DATA_HORA_ENTRADA_PREVISTA=\":DATA_HORA_PREVISTA\"");
            sb.Append(" ID_LOCAL=\":ID_LOCAL\"");
            sb.Append(" ID_RESPONSAVEL=\":ID_RESPONSAVEL\"");
            sb.Append(" ID_SITUACAO=\":ID_SITUACAO\"");
            sb.Append(" ID_LOTACAO=\":ID_LOTACAO\"");
            sb.Append(" ID_INTERCAMBIO=\":ID_INTERCAMBIO\"");
            sb.Append(" ID_COND_USO=\":ID_COND_USO\"");
            sb.Append(" AO_ID_DESTINO=\":AO_ID_DESTINO\"");
            sb.Append(" ID_LINHA_DESTINO=\":ID_LINHA_DESTINO\"");
            sb.Append(" US_USR_IDU=\":USUARIO\">");
            sb.Append(" <MOTIVO_VOADOR ID_MOTIVO=\":ID_MOTIVO\" SOLICITANTE=\":SOLICITANTE\" JUSTIFICATIVA=\":JUSTIFICATIVA\"  />");
            sb.Append(" <LISTA_VAGOES>");
            sb.Append(" <VAGAO ID_VAGAO=\":ID_VAGAO\" COD_VAGAO=\":COD_VAGAO\" AO_ID_ORIGEM=\":AO_ID_ORIGEM\" NUM_SEQ=\":NUM_SEQ\" ID_LINHA_ORIGEM=\":ID_LINHA_ORIGEM\"  />");
            sb.Append(" </LISTA_VAGOES>");
            sb.Append(" </ATRIBUTOS>");
    
        xml = sb.ToString().Replace(":DATA_HORA_SAIDA", dataHoraChegada.AddDays(-1).ToString("dd/MM/yyyy HH:mm:ss"))
            .Replace(":DATA_HORA_ENTRADA", dataHoraChegada.ToString("dd/MM/yyyy HH:mm:ss"))
            .Replace(":DATA_HORA_PREVISTA", dataHoraChegada.ToString("dd/MM/yyyy HH:mm:ss"))
            .Replace(":ID_LOCAL", solicitacaoItem.IdLocal.ToString())
            .Replace(":ID_RESPONSAVEL", solicitacaoItem.IdResponsavel.ToString())
            .Replace(":ID_SITUACAO", solicitacaoItem.IdSituacao.ToString())
            .Replace(":ID_LOTACAO", solicitacaoItem.IdLotacao.ToString())
            .Replace(":ID_INTERCAMBIO", solicitacaoItem.IdIntercambio.ToString())
            .Replace(":ID_COND_USO", solicitacaoItem.IdCondUso.ToString())
            .Replace(":AO_ID_DESTINO", solicitacaoItem.IdPatioDestino.ToString())
            .Replace(":ID_LINHA_DESTINO", solicitacaoItem.IdLinhaDestino.ToString())
            .Replace(":USUARIO", usuario)
            .Replace(":ID_MOTIVO", _vooMotivoRepository.BuscaIDPorDescricaoMotivo(tipoAtivo, motivo).ToString())
            .Replace(":SOLICITANTE", usuario)
            .Replace(":JUSTIFICATIVA", justificativa)
            .Replace(":ID_VAGAO", solicitacaoItem.IdAtivo.ToString())
            .Replace(":COD_VAGAO", solicitacaoItem.Ativo.ToString())
            .Replace(":AO_ID_ORIGEM", solicitacaoItem.IdPatioOrigem.ToString())
            .Replace(":NUM_SEQ", "1")
            .Replace(":ID_LINHA_ORIGEM", solicitacaoItem.IdLinhaOrigem.ToString());

            #endregion

            return xml;
        }

        private string MontaXMLEot(DateTime dataHoraChegada, string usuario, string motivo, string justificativa, SolicitacaoVooAtivoItemPkgDto solicitacaoItem, EnumTipoAtivo tipoAtivo)
        {

            #region Monta XML
            string xml;
            StringBuilder sb = new StringBuilder();

            sb.Append("<?xml version=\"1.0\" encoding=\"iso-8859-1\"?>");
            sb.Append("<ATRIBUTOS ");
            sb.Append(" DATA_HORA_ENTRADA=\":DATA_HORA_ENTRADA\"");
            sb.Append(" AO_ID_DESTINO=\":AO_ID_DESTINO\"");
            sb.Append(" US_USR_IDU=\":USUARIO\">");
            sb.Append(" <MOTIVO_VOADOR ID_MOTIVO=\":ID_MOTIVO\" SOLICITANTE=\":SOLICITANTE\" JUSTIFICATIVA=\":JUSTIFICATIVA\"  />");
            sb.Append(" <LISTA_EQUIPAMENTOS>");
            sb.Append(" <EQUIPAMENTO ID_EQUIPAMENTO=\":ID_EQUIPAMENTO\" COD_EQUIPAMENTO=\":COD_EQUIPAMENTO\" AO_ID_ORIGEM=\":AO_ID_ORIGEM\" />");
            sb.Append(" </LISTA_EQUIPAMENTOS>");
            sb.Append(" </ATRIBUTOS>");

            xml = sb.ToString().Replace(":DATA_HORA_ENTRADA", dataHoraChegada.ToString("dd/MM/yyyy HH:mm:ss"))
            .Replace(":AO_ID_DESTINO", solicitacaoItem.IdPatioDestino.ToString())
            .Replace(":USUARIO", usuario)
            .Replace(":ID_MOTIVO", _vooMotivoRepository.BuscaIDPorDescricaoMotivo(tipoAtivo, motivo).ToString())
            .Replace(":SOLICITANTE", usuario)
            .Replace(":JUSTIFICATIVA", justificativa)
            .Replace(":ID_EQUIPAMENTO", solicitacaoItem.IdAtivo.ToString())
            .Replace(":COD_EQUIPAMENTO", solicitacaoItem.Ativo.ToString())
            .Replace(":AO_ID_ORIGEM", solicitacaoItem.IdPatioOrigem.ToString());

            #endregion

            return xml;
        }


        private string MontaXMLVagaoRefaturamento(RefatVagoesDto refatVagoesDto, EnumTipoAtivo tipoAtivo)
        {
            #region Monta XML

            string xml;
            StringBuilder sb = new StringBuilder();

            sb.Append("<?xml version=\"1.0\" encoding=\"iso-8859-1\"?>");
            sb.Append("<ATRIBUTOS ");
            sb.Append(" ID_TREM=\":ID_TREM\"");
            sb.Append(" ID_COMPOSICAO=\":ID_COMPOSICAO\"");
            sb.Append(" US_USR_IDU=\":USUARIO\">");
            sb.Append(" <MOTIVO_REFATURAMENTO ID_MOTIVO=\":ID_MOTIVO\" SOLICITANTE=\":SOLICITANTE\" JUSTIFICATIVA=\":JUSTIFICATIVA\"  />");
            sb.Append(" <LISTA_VAGOES>");   

            #endregion

            foreach (var vagao in refatVagoesDto.ListaVagao)
            {
                sb.Append(string.Format(@"<VAGAO ID_VAGAO=""{0}"" ID_DESPACHO=""{1}"" CONTRATO=""{2}"" />", vagao.IdVagao, vagao.IdDespacho, vagao.Contrato));
            }
            //Loop para percorrer lista de tras para frente
            //for (int i = refatVagoesDto.ListaVagao.Count - 1; i < refatVagoesDto.ListaVagao.Count && i >= 0; i--)
            //{
            //    sb.Append(string.Format(@"<VAGAO ID_VAGAO=""{0}"" ID_DESPACHO=""{1}"" CONTRATO=""{2}"" />", refatVagoesDto.ListaVagao[i].IdVagao, refatVagoesDto.ListaVagao[i].IdDespacho, refatVagoesDto.ListaVagao[i].Contrato));
            //}

            sb.Append(" </LISTA_VAGOES>");
            sb.Append(" </ATRIBUTOS>");

            xml = sb.ToString().Replace(":ID_TREM", refatVagoesDto.IdTrem.ToString())
            .Replace(":ID_COMPOSICAO", refatVagoesDto.IdComposicao.ToString())
            .Replace(":USUARIO", refatVagoesDto.Usuario)
            .Replace(":ID_MOTIVO", _vooMotivoRepository.BuscaIDPorDescricaoMotivo(tipoAtivo, refatVagoesDto.IdMotivo).ToString())
            .Replace(":SOLICITANTE", refatVagoesDto.Solicitante)
            .Replace(":JUSTIFICATIVA", refatVagoesDto.Justificativa);

            return xml;
        }

        public SolicitacaoVooAtivoStatus ObterStatusAtivo(int id)
        {
            return _solicitacaoVooAtivoStatusRepository.ObterPorId(id);
        }

        public SolicitacaoTipoAtivo ObterTipoAtivo(int id)
        {
            return _solicitacaoTipoAtivoRepository.ObterPorId(id);
        }

        public SolicitacaoVooAtivoStatusItem ObterStatusAtivoItem(int id)
        {
            return _solicitacaoVooAtivoStatusItemRepository.ObterPorId(id);
        }

        #endregion
    }
}