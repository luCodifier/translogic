﻿
namespace Translogic.Modules.Core.Domain.Services.VooAtivo
{
    using System.Collections.Generic;
    using Translogic.Modules.Core.Domain.Model.VooAtivo;
    using Translogic.Modules.Core.Util;
    using Translogic.Modules.Core.Domain.Services.VooAtivo.Interface;

    public class MotivoVooService : IMotivoVooService
    {
        #region Repositórios

        private readonly IVooMotivoRepository _vooMotivoRepository;

        #endregion

        #region Construtores

        public MotivoVooService(IVooMotivoRepository vooMotivoRepository)
        {
            _vooMotivoRepository = vooMotivoRepository;
        }
      
        #endregion

        #region Métodos
        
        public IList<SolicitacaoVooMotivo> BuscarMotivos(EnumTipoAtivo tipo) 
        {
            return _vooMotivoRepository.BuscarMotivos(tipo);
        }

        #endregion
    }
}