﻿namespace Translogic.Modules.Core.Domain.Services
{
    using System;
    using System.IO;
    using System.Threading;
    using Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;
    using Util;

    public class GerarArquivosPdfService
    {
        private readonly ICteArquivoRepository _cteArquivoRepository;

        public GerarArquivosPdfService(ICteArquivoRepository cteArquivoRepository)
        {
            _cteArquivoRepository = cteArquivoRepository;
        }

        public void GerarArquivos()
        {
            string[] lines = File.ReadAllLines(@"D:\Temp\Gerar Arquivos Pdf\listaGeracao.txt");
            var file = new StreamWriter(@"D:\Temp\Gerar Arquivos Pdf\logGeracao.txt");
                
            foreach (string line in lines)
            {
                Thread.Sleep(1000);
                try
                {
                    var arquivo = _cteArquivoRepository.ObterArqPorId(line);
                    string fileName = @"D:\Temp\Gerar Arquivos Pdf\Arquivos\" + line + ".pdf";
                    Tools.SaveByteArrayToFile(fileName, arquivo);
                    file.WriteLine(line + ";" + "OK");
                    
                }
                catch (Exception e)
                {
                    file.WriteLine(line + ";" + "ERRO");
                }
                finally
                {
                    System.GC.Collect();
                    Thread.Sleep(1000);
                }
            }
            file.Close();
        }
    }
}