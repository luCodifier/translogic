
namespace Translogic.Modules.Core.Domain.Services.Estrutura
{
	using System.Collections.Generic;
	using Model.Estrutura.Repositories;
	using Translogic.Modules.Core.Domain.Model.Diversos;
	using Translogic.Modules.Core.Domain.Model.Estrutura;

    /// <summary>
    /// Classe de Servicos
    /// </summary>
    public class UnidadeProducaoService
    {
        private IUnidadeProducaoRepository _unidadeProducaoRepository;
        
        /// <summary>
        /// Contrutor Injetado
        /// </summary>
        /// <param name="unidadeProducaoRepository">Interface de Unidade de Producao</param>
        public UnidadeProducaoService(IUnidadeProducaoRepository unidadeProducaoRepository)
        {
            _unidadeProducaoRepository = unidadeProducaoRepository;
        }

		/// <summary>
		/// Obtem todas as unidades de produ��o.
		/// </summary>
		/// <param name="malha"> A malha onde est� o ponto de abastecimento </param>			
		/// <returns>Lista de Unidade de produ��o</returns>
		public IList<UnidadeProducao> ObterUnidadeProducao(Malha malha)
		{
			return _unidadeProducaoRepository.ObterPorMalha(malha);
		}

		#region M�TODOS PRIVADOS

		private string ClassificarMalha(string malhaId)
		{
			if ("Larga".Equals(malhaId))
			{
				malhaId = "LG";
			}
			else if ("MetricaSul".Equals(malhaId))
			{
				malhaId = "MS";
			}
			else if ("MetricaNorte".Equals(malhaId))
			{
				malhaId = "MN";
			}
			else
			{
				malhaId = string.Empty;
			}

			return malhaId;
		}

		#endregion
    }
}