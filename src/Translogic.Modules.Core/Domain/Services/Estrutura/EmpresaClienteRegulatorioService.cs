﻿namespace Translogic.Modules.Core.Domain.Services.Estrutura
{
    using System.Collections.Generic;
    using ALL.Core.Dominio;
    using ALL.Core.Dominio.Services;
    using Model.Diversos;
    using Model.Diversos.Ibge;
    using Translogic.Core.Infrastructure.Web;
    using Translogic.Modules.Core.Domain.Model.Estrutura;
    using Translogic.Modules.Core.Domain.Model.Estrutura.Repositories;

    /// <summary>
    /// Classe de serviços de Cliente
    /// </summary>
    public class EmpresaClienteRegulatorioService : BaseCrudService<Empresa, int?>
    {
        /// <summary>
        /// Inicializa uma nova instância da classe <see cref="EmpresaClienteRegulatorioService"/>.
        /// </summary>
        /// <param name="clienteRepository"> Repositório de Clientes</param>
        public EmpresaClienteRegulatorioService(IEmpresaClienteRegulatorioRepository clienteRepository)
            : base(clienteRepository)
        {
        }

        /// <summary>
        /// Lista os clientes
        /// </summary>
        /// <returns>Lista de clientes</returns>
        public IEnumerable<Empresa> ListarClientes()
        {
            (this.Repository as IEmpresaClienteRegulatorioRepository).ListarPorRazaoSocial(" ");
            return this.Repository.ObterTodos();
        }

        /// <summary>
        /// Método para Obter as categorias
        /// </summary>
        /// <param name="pagination">
        /// Parametro Paginação da Grid
        /// </param>
        /// <param name="razaoSocial">
        /// Parametro Razão Social
        /// </param>
        /// <param name="nomeFantasia">
        /// Nome Fantasia
        /// </param>
        /// <param name="cnpj">
        /// Cnpj da empresa
        /// </param>               
        /// <returns>
        /// Retorna Ilist de categorias
        /// </returns>
        public ResultadoPaginado<Empresa> ObterPorParametro(DetalhesPaginacaoWeb pagination, string razaoSocial, string nomeFantasia, string cnpj)
        {
            return (this.Repository as IEmpresaClienteRegulatorioRepository).ObterPorParametro(pagination, razaoSocial, nomeFantasia, cnpj);
        }

        /// <summary>
        /// Lista todos os estados (UFs)
        /// </summary>
        /// <returns>Listagem dos estados</returns>
        public IEnumerable<Estado> ListarEstados()
        {
            return (this.Repository as IEmpresaClienteRegulatorioRepository).ListarEstados();
        }

        /// <summary>
        /// Lista todas as cidades de um estado
        /// </summary>
        /// <param name="sigla">Estado desejado</param>
        /// <returns>Listagem dos estados</returns>
        public IEnumerable<CidadeIbge> ListarCidades(string sigla)
        {
            return (this.Repository as IEmpresaClienteRegulatorioRepository).ListarCidades(sigla);
        }

        /// <summary>
        /// Obtem o estado (UF)
        /// </summary>
        /// <param name="sigla">Estado desejado</param>
        /// <returns>Obtem o estado desejado</returns>
        public Estado ObterEstado(string sigla)
        {
            return (this.Repository as IEmpresaClienteRegulatorioRepository).ObterEstado(sigla);
        }

        /// <summary>
        /// Obtem a cidade
        /// </summary>
        /// <param name="cidadeId">Id da cidade desejada</param>
        /// <returns>Obtem a cidade desejada</returns>
        public CidadeIbge ObterCidade(int cidadeId)
        {
            return (this.Repository as IEmpresaClienteRegulatorioRepository).ObterCidade(cidadeId);
        }
    }
}
