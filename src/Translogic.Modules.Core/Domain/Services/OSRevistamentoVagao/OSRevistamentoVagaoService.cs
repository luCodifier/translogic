﻿// -----------------------------------------------------------------------
// <copyright file="OSRevistamentoVagaoService.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace Translogic.Modules.Core.Domain.Services.OSRevistamentoVagao
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Translogic.Modules.Core.Domain.Services.OSRevistamentoVagao.Interface;
    using ALL.Core.Dominio;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Core.Infrastructure.Web;
    using Translogic.Modules.Core.Domain.Model.OSRevistamentoVagao.Repositories;
    using Translogic.Modules.Core.Domain.Model.OSLimpezaVagao.Repositories;
    using Translogic.Modules.Core.Domain.Model.OSRevistamentoVagao;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao.Repositories;
    
    using Translogic.Modules.Core.Domain.Model.Fornecedor.Repositories;
    using Translogic.Modules.Core.Domain.Model.Via.Repositories;
    using Translogic.Modules.Core.Helpers;
    using Translogic.Modules.Core.Util;
    using Translogic.Modules.Core.Domain.Model.Codificador.Repositories;
    using System.Configuration;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class OSRevistamentoVagaoService : IOSRevistamentoVagaoService
    {
        #region Constantes
        public const int IDTIPOREVISTAMENTO = 3;
        #endregion

        #region Repositórios

        private readonly IOSRevistamentoVagaoRepository _osRevistamentoVagaoRepository;
        private readonly IOSRevistamentoVagaoItemRepository _osRevistamentoVagaoItemRepository;
        private readonly IOSStatusRepository _statusRepository;
        private readonly IVagaoRepository _vagaoRepository;
        private readonly IFornecedorOsRepository _fornecedorRepository;
        private readonly IAreaOperacionalRepository _areaOperacional;
        private readonly IOSTipoRepository _tipoRepository;
        private readonly IConfiguracaoTranslogicRepository _configuracaoTranslogicRepository;

        #endregion

        #region Construtores
        public OSRevistamentoVagaoService(IOSRevistamentoVagaoRepository osRevistamentoVagaoRepository, 
            IOSRevistamentoVagaoItemRepository osRevistamentoVagaoItemRepository, 
            IOSStatusRepository statusRepository,
            IVagaoRepository vagaoRepository,
            IFornecedorOsRepository fornecedorRepository,
            IAreaOperacionalRepository areaOperacional,
            IOSTipoRepository tipoRepository,
            IConfiguracaoTranslogicRepository configuracaoTranslogicRepository)
        {
            _osRevistamentoVagaoRepository = osRevistamentoVagaoRepository;
            _osRevistamentoVagaoItemRepository = osRevistamentoVagaoItemRepository;
            _statusRepository = statusRepository;
            _vagaoRepository = vagaoRepository;
            _fornecedorRepository = fornecedorRepository;
            _areaOperacional = areaOperacional;
            _tipoRepository = tipoRepository;
            _configuracaoTranslogicRepository = configuracaoTranslogicRepository;
        }
      
        #endregion

        #region Método privados

        private void SendMail(int idOs, string type)
        {
            var sendoTo = "";
            sendoTo = _configuracaoTranslogicRepository.ObterPorId("EMAIL_NOTIFICA_REVISTAMENTO_EXECUCAO").Valor;
                
            var subject = "";
            var body = "";

            switch (type)
            {

                case "gerada":
                    {
                        subject = string.Format("Revistamento – OS Gerada Nº {0}", idOs);
                        body = string.Format("<p>Mensagem automática.</p> <p>Foi gerada a OS de Revistamento Nº {0}.</p> <p>Favor tomar as devidas providências.</p>", idOs); ;
                        break;
                    }
                case "fechada":
                    {
                        subject = string.Format("Revistamento – OS Fechada Nº {0}", idOs);
                        body = string.Format("<p>Mensagem automática.</p> <p>Foi fechada a OS de Revistamento Nº {0}.</p> <p>Favor tomar as devidas providências.</p>", idOs); ;
                        break;
                    }
                case "parcial":
                    {
                        subject = string.Format("Revistamento – OS Parcialmente Fechada Nº {0}", idOs);
                        body = string.Format("<p>Mensagem automática.</p> <p>Foi parcialmente fechada a OS de Revistamento Nº {0}.</p> <p>Favor tomar as devidas providências.</p>", idOs); ;
                        break;
                    }
            }

            EmailUtil.Send(sendoTo, subject, body);
        }

        #endregion

        #region Métodos

        public int InserirAtualizarVagoes(int? idOs, IList<int> idsVagoes, string usuario, string local)
        {
            //Salvar OS sem dados para gerar ID
            var osRevistamentoVagao = new OSRevistamentoVagao();
            var status = _statusRepository.ObterPorId((int)EnumOSLimpezaVagaoStatus.Aberta);
            osRevistamentoVagao.Status = status;
            osRevistamentoVagao.Usuario = usuario;
            
            var tipoServico = _tipoRepository.ObterPorId(IDTIPOREVISTAMENTO); //Uso do id do tipo Revistamento
            osRevistamentoVagao.TipoServico = tipoServico;

            osRevistamentoVagao.Local = _areaOperacional.ObterPorCodigo(local.ToUpper());


            if (idOs.HasValue)
            {
                osRevistamentoVagao.Id = idOs.Value;
                osRevistamentoVagao = _osRevistamentoVagaoRepository.Atualizar(osRevistamentoVagao);

                //Remover vagoes que já existem
                var vagoesRemover = _osRevistamentoVagaoItemRepository.ObterVagoesDaOs(idOs.Value);
                _osRevistamentoVagaoItemRepository.Remover(vagoesRemover);
            }
            else
                osRevistamentoVagao = _osRevistamentoVagaoRepository.Inserir(osRevistamentoVagao);


            //Salvar vagões da OS
            var itens = new List<OSRevistamentoVagaoItem>();
            foreach (var idVagao in idsVagoes)
            {
                var vagao = _vagaoRepository.ObterPorCodigo(idVagao.ToString());

                itens.Add(new OSRevistamentoVagaoItem()
                {
                    Vagao = vagao,
                    OSRevistamentoVagao = osRevistamentoVagao,
                    VersionDate = DateTime.Now
                });
            }
            _osRevistamentoVagaoItemRepository.Inserir(itens);

            return osRevistamentoVagao.Id;
        }

        public ResultadoPaginado<OSRevistamentoVagaoDto> ObterConsultaListaOs(DetalhesPaginacaoWeb detalhesPaginacaoWeb, string dataInicial, string dataFinal,
        string local, string numOs, string idTipo, string idStatus, bool vagaoRetirado, bool problemaVedacao, bool vagaoCarregado)
        {
            DateTime? dtInicial = null;
            DateTime? dtFinal = null;

            if (!string.IsNullOrEmpty(dataInicial))
            {
                dtInicial = Convert.ToDateTime(dataInicial);
            }

            if (!string.IsNullOrEmpty(dataFinal))
            {
                dtFinal = Convert.ToDateTime(dataFinal);
            }

            if (idStatus == "99")
                idStatus = "1,2"; //Caso seja Aberta/Parcial

            return _osRevistamentoVagaoRepository.ObterConsultaListaOs(detalhesPaginacaoWeb, dtInicial, dtFinal, local, numOs, idTipo, idStatus, vagaoRetirado, problemaVedacao, vagaoCarregado);
        }

        public ResultadoPaginado<OSRevistamentoVagaoItemDto> ObterVagoesOS(DetalhesPaginacaoWeb detalhesPaginacaoWeb, int idOs)
        {
            return _osRevistamentoVagaoItemRepository.ObterVagoesDaOs(detalhesPaginacaoWeb, idOs);
        }

        public IList<OSVagaoItemLotacaoDto> ObterLotacoes()
        {
            return _osRevistamentoVagaoItemRepository.ObterLotacoes();
        }

        public ResultadoPaginado<OSRevistamentoVagaoItemDto> ObterConsultaDeVagoesParaOs(DetalhesPaginacaoWeb detalhesPaginacaoWeb, int? numOs,
          string listaDeVagoes, string serie, string local, string linha, string idLotacao, int sequencioInicio, int sequencioFim)
        {
            if (string.IsNullOrEmpty(local))
                throw new Exception("Campo local é obrigatório");

            return _osRevistamentoVagaoItemRepository.ObterConsultaDeVagoesParaOs(detalhesPaginacaoWeb, numOs, listaDeVagoes, serie, local, linha, idLotacao, sequencioInicio, sequencioFim);
        }

        public OSRevistamentoVagaoDto ObterOS(int idOs)
        {
            if (idOs == 0)
                throw new Exception("ID da OS é obrigatório");

            var os = _osRevistamentoVagaoRepository.ObterPorId(idOs);

            var osDto = new OSRevistamentoVagaoDto();
            osDto.IdOs = os.Id;
            osDto.IdOsParcial = (os.OsRevistamentoParcial != null ? os.OsRevistamentoParcial.Id : 0);
            osDto.Data = os.Data.Value;
            osDto.IdFornecedor = (os.Fornecedor != null ? os.Fornecedor.Id : 0);
            osDto.Fornecedor = (os.Fornecedor != null ? os.Fornecedor.Nome : "");
            osDto.idLocal = (os.Local != null ? os.Local.Id.Value : 0);
            osDto.LocalServico = (os.Local != null ? os.Local.Codigo.ToUpper() : "");
            osDto.IdTipo = (os.TipoServico != null ? os.TipoServico.Id : 0);
            osDto.Tipo = (os.TipoServico != null ? os.TipoServico.Descricao : "");
            osDto.IdStatus = os.Status.Id;
            osDto.Status = os.Status.Descricao;
            osDto.Convocacao = os.Convocacao;
            osDto.HoraInicio = os.DataHoraInicio;
            osDto.HoraTermino = os.DataHoraInicio;
            osDto.HoraEntrega = os.DataHoraTermino;
            osDto.Linha = os.Linha;
            osDto.NomeEstacao = os.NomeEstacao;
            osDto.MatriculaEstacao = os.MatriculaEstacao;
            osDto.NomeManutencao = os.NomeManutencao;
            osDto.MatriculaManutencao = os.MatriculaManutencao;
            osDto.QtdeVagoes = os.QtdVagoes;
            osDto.QtdeVagoesGambitados = os.QtdVagoesGambitados;
            osDto.QtdeVagoesVazios = os.QtdEntreVazios;
            osDto.QtdeVagoesVedados = os.QtdVagoesVedados;
            osDto.UltimaAlteracao = os.DataUltimaAlteracao;
            osDto.Usuario = os.Usuario;
            osDto.JustificativaCancelamento = os.JustificativaCancelamento;            

            return osDto;
        }

        public void AtualizarOSeVagoes(int idOs, int idFornecedor, string local)
        {
            var osRevistamentoVagao = _osRevistamentoVagaoRepository.ObterPorId(idOs);
            var fornecedor = _fornecedorRepository.ObterPorId(idFornecedor);
            osRevistamentoVagao.Fornecedor = fornecedor;
            var localAreaOperacional = _areaOperacional.ObterPorCodigo(local.ToUpper());
            osRevistamentoVagao.Local = localAreaOperacional;

            var tipoServico = _tipoRepository.ObterPorId(IDTIPOREVISTAMENTO); //Uso do id do tipo Revistamento
            osRevistamentoVagao.TipoServico = tipoServico;

            osRevistamentoVagao.DataUltimaAlteracao = DateTime.Now;
            osRevistamentoVagao.VersionDate = DateTime.Now;
            osRevistamentoVagao.Status = _statusRepository.ObterPorId((int)EnumOSLimpezaVagaoStatus.Aberta);

            try
            {
                _osRevistamentoVagaoRepository.Atualizar(osRevistamentoVagao);

                #region Enviando e-mail de notificação.

                try
                {
                    SendMail(idOs, "gerada");
                }
                catch 
                {
                    //// Ignorando problemas no envio de e-mail.
                }

                #endregion
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possível salvar a OS. Erro: " + ex.Message);

                try
                {
                    osRevistamentoVagao.Status = _statusRepository.ObterPorId((int)EnumOSLimpezaVagaoStatus.Aberta);
                    _osRevistamentoVagaoRepository.Atualizar(osRevistamentoVagao);
                }
                catch { }

            }
        }

        public void EditarOs(int idOs, int idFornecedor)
        {
            var os = _osRevistamentoVagaoRepository.ObterPorId(idOs);
            os.Fornecedor = _fornecedorRepository.ObterPorId(idFornecedor);
            _osRevistamentoVagaoRepository.Atualizar(os);
        }

        public int CancelarOs(int idOs)
        {
            var os = _osRevistamentoVagaoRepository.ObterPorId(idOs);
            os.Status = _statusRepository.ObterPorId((int)EnumOSLimpezaVagaoStatus.Cancelada);
            _osRevistamentoVagaoRepository.Atualizar(os);

            return idOs;
        }

        public void CancelarOs(int idOs, string justificativa)
        {
            var os = _osRevistamentoVagaoRepository.ObterPorId(idOs);
            os.Status = _statusRepository.ObterPorId((int)EnumOSLimpezaVagaoStatus.Cancelada);
            os.Data = DateTime.Now;
            os.JustificativaCancelamento = justificativa;
            _osRevistamentoVagaoRepository.Atualizar(os);
        }
        
        public int FecharOs(int idOs, string horaInicio, string horaTermino, string horaEntrega, string linha, string convocacao, int quantVagoes,
            int idFornecedor, string local, int quantVagoesVedados, int quantVagoesGambitados, int quantVagoesVazios,
            string nomeManutencao, string matriculaManutencao, string nomeEstacao, string matriculaEstacao, string codigoUsuario,
            IList<OSRevistamentoVagaoItemDto> vagoes)
        {

            var osRevistamentoVagao = _osRevistamentoVagaoRepository.ObterPorId(idOs);
            if (osRevistamentoVagao == null)
                throw new Exception("OS " + idOs + " não encontrada");

            #region Conversão das datas
            DateTime dateHoraInicial;  
            string[] splHoraInicial = horaInicio.Split(':');
            if (splHoraInicial.Length > 0)
            {
                dateHoraInicial = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, Convert.ToInt32(splHoraInicial[0]), Convert.ToInt32(splHoraInicial[1]), 0);
                osRevistamentoVagao.DataHoraInicio = dateHoraInicial;
            }

            DateTime dateHoraFinal;
            string[] splHoraFinal = horaTermino.Split(':');
            if (splHoraFinal.Length > 0)
            {
                dateHoraFinal = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, Convert.ToInt32(splHoraFinal[0]), Convert.ToInt32(splHoraFinal[1]), 0);
                osRevistamentoVagao.DataHoraTermino = dateHoraFinal;
            }

            DateTime dateHoraEntrega;
            string[] splHoraEntrega = horaEntrega.Split(':');
            if (splHoraEntrega.Length > 0)
            {
                dateHoraEntrega = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, Convert.ToInt32(splHoraEntrega[0]), Convert.ToInt32(splHoraEntrega[1]), 0);
                osRevistamentoVagao.DataHoraEntrega = dateHoraEntrega;
            }
            #endregion

            osRevistamentoVagao.Linha = linha;
            osRevistamentoVagao.Convocacao = convocacao;

            var fornecedor = _fornecedorRepository.ObterPorId(idFornecedor);
            osRevistamentoVagao.Fornecedor = fornecedor;

            osRevistamentoVagao.QtdVagoes = quantVagoes;
            osRevistamentoVagao.QtdVagoesGambitados = quantVagoesGambitados;
            osRevistamentoVagao.QtdVagoesVedados = quantVagoesVedados;
            osRevistamentoVagao.QtdEntreVazios = quantVagoesVazios;
            osRevistamentoVagao.NomeManutencao = nomeManutencao;
            osRevistamentoVagao.MatriculaManutencao = matriculaManutencao;
            osRevistamentoVagao.NomeEstacao = nomeEstacao;
            osRevistamentoVagao.MatriculaEstacao = matriculaEstacao;

            var tipoServico = _tipoRepository.ObterPorId(IDTIPOREVISTAMENTO); //Uso do id do tipo Revistamento
            osRevistamentoVagao.TipoServico = tipoServico;
            
            osRevistamentoVagao.Usuario = codigoUsuario;

            osRevistamentoVagao.DataUltimaAlteracao = DateTime.Now;
            osRevistamentoVagao.VersionDate = DateTime.Now;
            osRevistamentoVagao.Status = _statusRepository.ObterPorId((int)EnumOSLimpezaVagaoStatus.Fechada);

            //Salvar vagões da OS
            var itens = new List<OSRevistamentoVagaoItem>();
            var itensParciais = new List<Int32>();
            foreach (var vagao in vagoes)
            {
                var item = _osRevistamentoVagaoItemRepository.ObterPorId((int)vagao.IdItem);
                if (item != null)
                {
                    item.Concluido = (vagao.Concluido.HasValue ? (vagao.Concluido.Value ? 1 : 0) : 0);
                    item.Retirar = (vagao.Retirar.HasValue ? (vagao.Retirar.Value ? 1 : 0) : 0);
                    item.Comproblema = (vagao.Comproblema.HasValue ? (vagao.Comproblema.Value ? 1 : 0) : 0);
                    item.Observacao = (vagao.Observacao != null ? vagao.Observacao.Replace(">", "").Replace("<", "") : "");
                    item.VersionDate = DateTime.Now;
                    itens.Add(item);

                    if (item.Concluido == 0) //se houver itens não marcados como concluídos, adiciona à lista de id de vagão
                    {
                        itensParciais.Add(Convert.ToInt32(item.Vagao.Codigo));
                    }
                }
            }

            //Atualizar os vagões da OS
            _osRevistamentoVagaoItemRepository.Atualizar(itens);

            //Se houver vagões não marcados, criar os Parcial
            if (itensParciais.Count > 0)
            {
                var idOSParcial = InserirAtualizarVagoes(null, itensParciais, codigoUsuario, osRevistamentoVagao.Local.Codigo);

                //Atualizar OS parcial para Status Parcial
                var osParcial = _osRevistamentoVagaoRepository.ObterPorId(idOSParcial);
                osParcial.Status = _statusRepository.ObterPorId((int)EnumOSLimpezaVagaoStatus.Parcial);
                osParcial.Fornecedor = osRevistamentoVagao.Fornecedor;
                osParcial.TipoServico = osRevistamentoVagao.TipoServico;
                osParcial.OsRevistamentoParcial = osRevistamentoVagao;
                _osRevistamentoVagaoRepository.Atualizar(osParcial);

                #region Enviando e-mail de notificação.

                try
                {
                    SendMail(idOSParcial, "parcial");
                }
                catch
                {
                    //// Ignorando problemas no envio de e-mail.
                }

                #endregion
            }

            try
            {
                _osRevistamentoVagaoRepository.Atualizar(osRevistamentoVagao);

                #region Enviando e-mail de notificação.

                try
                {
                    SendMail(idOs, "fechada");
                }
                catch
                {
                    //// Ignorando problemas no envio de e-mail.
                }

                #endregion
            }
            catch (Exception ex)
            {
                try
                {
                    osRevistamentoVagao.Status = _statusRepository.ObterPorId((int)EnumOSLimpezaVagaoStatus.Aberta);
                    _osRevistamentoVagaoRepository.Atualizar(osRevistamentoVagao);
                }
                catch { }

                throw new Exception("Não foi possível salvar a OS. Erro: " + ex.Message);
            }


            return idOs;
        }

        public IList<OSRevistamentoVagaoExportaDto> ObterConsultaOsRevistamentoExportar(string dataInicial, string dataFinal,
        string local, string numOs, string idTipo, string idStatus, bool vagaoRetirado, bool problemaVedacao, bool vagaoCarregado)
        {
            DateTime? dtInicial = null;
            DateTime? dtFinal = null;

            if (!string.IsNullOrEmpty(dataInicial))
            {
                dtInicial = Convert.ToDateTime(dataInicial);
            }

            if (!string.IsNullOrEmpty(dataFinal))
            {
                dtFinal = Convert.ToDateTime(dataFinal);
            }

            if (idStatus == "99")
                idStatus = "1,2"; //Caso seja Aberta/Parcial

            return _osRevistamentoVagaoRepository.ObterConsultaOsParaExportar(dtInicial, dtFinal, local, numOs, idTipo, idStatus, vagaoRetirado, problemaVedacao, vagaoCarregado);
        }

        #endregion
    }
}
