﻿// -----------------------------------------------------------------------
// <copyright file="IOSRevistamentoVagaoService.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace Translogic.Modules.Core.Domain.Services.OSRevistamentoVagao.Interface
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using ALL.Core.Dominio;
    using Translogic.Core.Infrastructure.Web;
    using Translogic.Modules.Core.Domain.Model.Dto;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public interface IOSRevistamentoVagaoService
    {

        /// <summary>
        /// Atualizar a OS com seus dados e atualizar a lista de vagões da OS
        /// </summary>
        /// <param name="idOs"></param>
        /// <param name="idFornecedor"></param>
        /// <param name="local"></param>
        /// <param name="vagoes"></param>
        void AtualizarOSeVagoes(int idOs, int idFornecedor, string local);
        
        /// <summary>
        /// Inserir os vagoes
        /// </summary>
        /// /// <param name="idOs">Id OS em caso de edição</param>
        /// <param name="idsVagoes"></param>
        /// <param name="usuario"></param>
        /// <param name="local"></param>
        /// <returns></returns>
        int InserirAtualizarVagoes(int? idOs, IList<int> idsVagoes, string usuario, string local);

        /// <summary>
        /// Pesquisa de OS
        /// </summary>
        /// <param name="detalhesPaginacaoWeb"></param>
        /// <param name="dataInicial"></param>
        /// <param name="dataFinal"></param>
        /// <param name="local"></param>
        /// <param name="numOs"></param>
        /// <param name="idTipo"></param>
        /// <param name="idStatus"></param>
        /// <param name="vagaoRetirado"></param>
        /// <param name="problemaVedacao"></param>
        /// <param name="vagaoCarregado"></param>
        /// <returns></returns>
        ResultadoPaginado<OSRevistamentoVagaoDto> ObterConsultaListaOs(DetalhesPaginacaoWeb detalhesPaginacaoWeb, string dataInicial, string dataFinal,
        string local, string numOs, string idTipo, string idStatus, bool vagaoRetirado, bool problemaVedacao, bool vagaoCarregado);

        /// <summary>
        /// Lista de lotações dos vagões da OS
        /// </summary>
        /// <returns></returns>
        IList<OSVagaoItemLotacaoDto> ObterLotacoes();

        /// <summary>
        /// Buscar os vagões da OS em resultado paginado
        /// </summary>
        /// <param name="detalhesPaginacaoWeb"></param>
        /// <param name="idOs"></param>
        /// <returns></returns>
        ResultadoPaginado<OSRevistamentoVagaoItemDto> ObterVagoesOS(DetalhesPaginacaoWeb detalhesPaginacaoWeb, int idOs);

        /// <summary>
        /// Listagem de vagões para a OS
        /// </summary>
        /// <param name="detalhesPaginacaoWeb"></param>
        ///   /// <param name="numOs">ID da OS se ja estiver criada</param>
        /// <param name="listaDeVagoes"></param>
        /// <param name="serie"></param>
        /// <param name="local"></param>
        /// <param name="linha"></param>
        /// <param name="idLotacao"></param>
        /// <param name="sequenciaInicio"></param>
        /// <param name="sequenciaFim"></param>
        /// <returns></returns>
        ResultadoPaginado<OSRevistamentoVagaoItemDto> ObterConsultaDeVagoesParaOs(DetalhesPaginacaoWeb detalhesPaginacaoWeb, int? numOs,
            string listaDeVagoes, string serie, string local, string linha, string idLotacao, int sequenciaInicio, int sequenciaFim);

        /// <summary>
        /// Buscar dados da OS
        /// </summary>
        /// <param name="idOs"></param>
        /// <returns></returns>
        OSRevistamentoVagaoDto ObterOS(int idOs);

        /// <summary>
        /// Realizar atualização do fornecedor
        /// </summary>
        /// <param name="idOs"></param>
        /// <param name="idFornecedor"></param>
        void EditarOs(int idOs, int idFornecedor);

        /// <summary>
        /// Cancelamento de OS
        /// </summary>
        /// <param name="idOs"></param>
        /// <param name="justificativa"></param>
        void CancelarOs(int idOs, string justificativa);

        /// <summary>
        /// Cancelar OS fornecida
        /// </summary>
        /// <param name="idOs"></param>
        /// <returns></returns>
        int CancelarOs(int idOs);

        /// <summary>
        /// Fechamento da OS
        /// </summary>
        /// <param name="idOs"></param>
        /// <param name="horaInicio"></param>
        /// <param name="horaTermino"></param>
        /// <param name="horaEntrega"></param>
        /// <param name="linha"></param>
        /// <param name="convocacao"></param>
        /// <param name="quantVagoes"></param>
        /// <param name="idFornecedor"></param>
        /// <param name="local"></param>
        /// <param name="quantVagoesVedados"></param>
        /// <param name="quantVagoesGambitados"></param>
        /// <param name="quantVagoesVazios"></param>
        /// <param name="nomeManutencao"></param>
        /// <param name="matriculaManutencao"></param>
        /// <param name="nomeEstacao"></param>
        /// <param name="matriculaEstacao"></param>
        /// <param name="codigoUsuario"></param>
        /// <param name="vagoes"></param>
        /// <returns></returns>
        int FecharOs(int idOs, string horaInicio, string horaTermino, string horaEntrega, string linha, string convocacao, int quantVagoes,
            int idFornecedor, string local, int quantVagoesVedados, int quantVagoesGambitados, int quantVagoesVazios,
            string nomeManutencao, string matriculaManutencao, string nomeEstacao, string matriculaEstacao, string codigoUsuario,
            IList<OSRevistamentoVagaoItemDto> vagoes);

        /// <summary>
        /// Retornar lista de OS com vagões sem paginação para exportação.
        /// </summary>
        /// <param name="dataInicial"></param>
        /// <param name="dataFinal"></param>
        /// <param name="local"></param>
        /// <param name="numOs"></param>
        /// <param name="idTipo"></param>
        /// <param name="idStatus"></param>
        /// <param name="vagaoRetirado"></param>
        /// <param name="problemaVedacao"></param>
        /// <param name="vagaoCarregado"></param>
        /// <returns></returns>
        IList<OSRevistamentoVagaoExportaDto> ObterConsultaOsRevistamentoExportar(string dataInicial, string dataFinal,
        string local, string numOs, string idTipo, string idStatus, bool vagaoRetirado, bool problemaVedacao, bool vagaoCarregado);
    }
}
