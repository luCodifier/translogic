﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Translogic.Modules.Core.Domain.Model.Via;
using Translogic.Modules.Core.Domain.Model.Via.Repositories;
using Translogic.Modules.Core.Domain.Services.Via.Interface;
using Translogic.Modules.Core.Domain.Model.Dto;

namespace Translogic.Modules.Core.Domain.Services.Via
{
    public class ElementoViaService : IElementoViaService
    {
        private readonly IElementoViaRepository _IElementoViaRepository;

        public ElementoViaService(IElementoViaRepository IElementoViaRepository)
        {
            _IElementoViaRepository = IElementoViaRepository;
        }
        
        /// <summary>
        /// Obter as vias (Linhas) por código da estação Mãe (Pátio Origem)
        /// </summary>
        /// <param name="idAreaOperacional"></param>
        /// <returns></returns>
        public IList<ElementoViaDto> ObterPorCodigoEstacaoMae(string patio)
        {
            return _IElementoViaRepository.ObterPorPatio(patio);
        }
    }
}