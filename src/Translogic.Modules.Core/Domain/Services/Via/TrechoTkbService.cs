﻿namespace Translogic.Modules.Core.Domain.Services.Via
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using DataAccess.Repositories.Via;
    using Model.Estrutura;
    using Model.Estrutura.Repositories;
    using Model.Via;
    using Model.Via.Repositories;

    /// <summary>
    /// Serviço Trecho Tkb
    /// </summary>
    public class TrechoTkbService
    {
        private readonly ITrechoTkbRepository _trechoTkbRepository;
        private readonly IUnidadeProducaoRepository _unidadeProducaoRepository;

        /// <summary>
        /// Método Construtor
        /// </summary>
        /// <param name="trechoTkbRepository">Repositório Trecho Tkb</param>
        /// <param name="unidadeProducaoRepository">Repositório Up</param>
        public TrechoTkbService(ITrechoTkbRepository trechoTkbRepository, IUnidadeProducaoRepository unidadeProducaoRepository)
        {
            _trechoTkbRepository = trechoTkbRepository;
            _unidadeProducaoRepository = unidadeProducaoRepository;
        }

        /// <summary>
        /// Método para obter os trechos TKBs
        /// </summary>
        /// <param name="trechoTkb"> Parametro trecho Tkb. </param>
        /// <param name="trechoDiesel"> Parametro trecho Diesel. </param>
        /// <param name="corredor"> Parametro corredor. </param>
        /// <param name="up"> Parametro Up </param>
        /// <param name="idUp"> Id da Up (classe)</param>
        /// <returns> Retorna lista de Trechos </returns>
        public IList<TrechoTkb> ObterTodosPorParametros(string trechoTkb, string trechoDiesel, string corredor, string up, int? idUp)
        {
            return _trechoTkbRepository.ObterTodosPorParametro(trechoTkb, trechoDiesel, corredor, up, idUp);
        }

        /// <summary>Método para Adicionar um Registro </summary>
        /// <param name="trecho"> O registro a ser adicionado</param>
        public void Adicionar(TrechoTkb trecho)
        {
            _trechoTkbRepository.Inserir(trecho);
        }

        /// <summary>
        /// Método para Obter as ups
        /// </summary>
        /// <returns>Retorna as Ups</returns>
        public IList<UnidadeProducao> ObterListaUps()
        {
            return _unidadeProducaoRepository.ObterTodos();
        }

        /// <summary>
        /// Método para Obter up por Id
        /// </summary>
        /// <param name="idUp">Parametro id da Up</param>
        /// <returns>Retorna a UP</returns>
        public UnidadeProducao ObterUpPorId(int idUp)
        {
            return _unidadeProducaoRepository.ObterPorId(idUp);
        }

        /// <summary>
        /// Método para Excluir um registro por Id
        /// </summary>
        /// <param name="id">id do registro</param>
        public void ExcluirPorId(int id)
        {
            _trechoTkbRepository.Remover(id);
        }

        /// <summary>
        /// Método para obter por id
        /// </summary>
        /// <param name="id">id do registro</param>
        /// <returns>retorna TrechoTkb</returns>
        public TrechoTkb ObterPorId(int id)
        {
            return _trechoTkbRepository.ObterPorId(id);
        }

        /// <summary>
        /// Método para alterar o registro
        /// </summary>
        /// <param name="trecho">Parametro Classe</param>
        public void Alterar(TrechoTkb trecho)
        {
            _trechoTkbRepository.Atualizar(trecho);
        }
    }
}
