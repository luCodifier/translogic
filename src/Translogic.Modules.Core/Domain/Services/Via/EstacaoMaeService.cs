﻿using System;
using System.Collections.Generic;
using Translogic.Modules.Core.Domain.Model.Fornecedor;
using Translogic.Modules.Core.Domain.Model.LocaisFornecedor;
using Translogic.Modules.Core.Domain.Model.LocaisFornecedor.Repositories;
using Translogic.Modules.Core.Domain.Services.Fornecedor.Interface;
using Translogic.Modules.Core.Domain.Services.TipoServico.Interface;
using Translogic.Modules.Core.Domain.Model.Fornecedor.Repositories;
using Translogic.Modules.Core.Domain.Model.Dto;
using Translogic.Modules.Core.Util;
using Translogic.Core.Infrastructure.Web;
using ALL.Core.Dominio;
using AutoMapper;
using Translogic.Modules.Core.Domain.Model.TipoServicos.Repositories;
using Translogic.Modules.Core.Domain.Model.Via.Repositories;
using Translogic.Modules.Core.Domain.Model.Via;
using Translogic.Modules.Core.Domain.Services.Via.Interface;
namespace Translogic.Modules.Core.Domain.Services.Via
{
    public class EstacaoMaeService:IEstacaoMaeService
    {
        private readonly IEstacaoMaeRepository _IEstacaoMaeRepository;
        private readonly IAreaOperacionalRepository _areaOperacional;
        public EstacaoMaeService(IEstacaoMaeRepository IEstacaoMaeRepository,
                                 IAreaOperacionalRepository areaOperacional )
        {
            _IEstacaoMaeRepository = IEstacaoMaeRepository;
            _areaOperacional = areaOperacional;
        }

        public EstacaoMae ObterPorCodigo(string codigo)
        {
            var obj = _IEstacaoMaeRepository.ObterEstMaePorCodigo(codigo);
            //var obj =(EstacaoMae) _areaOperacional.ObterPorCodigo(codigo);
            return obj;
        }   


        public EstacaoMae ObterIdEstacaoMaePorCodigoDoLocal(string codigo)
        {
            throw new NotImplementedException();
        }
    }
}