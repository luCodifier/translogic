namespace Translogic.Modules.Core.Domain.Services.Via
{
    using Model.Via.Circulacao;
    using Model.Via.Circulacao.Repositories;

    /// <summary>
    /// Servi�o de gerenciamento de rotas
    /// </summary>
    public class RoteadorService
    {
        private readonly IRotaRepository _rotaRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="RoteadorService"/> class.
        /// </summary>
        /// <param name="rotaRepository"> Reposit�rio de rotas injetado. </param>
        public RoteadorService(IRotaRepository rotaRepository)
        {
            _rotaRepository = rotaRepository;
        }

        /// <summary>
        /// Obt�m a rota de menor dist�ncia dada uma origem/destino
        /// </summary>
        /// <param name="codigoOrigem"> Codigo da esta��o de origem. </param>
        /// <param name="codigoDestino"> Codigo da esta��o de destino. </param>
        /// <returns> Objeto Rota </returns>
        public Rota ObterRotaPorTrechoMenorDistancia(string codigoOrigem, string codigoDestino)
        {
            return _rotaRepository.ObterPorTrechoMenorDistancia(codigoOrigem, codigoDestino);
        }
    }
}