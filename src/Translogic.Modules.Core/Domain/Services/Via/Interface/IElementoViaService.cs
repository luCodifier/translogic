﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Translogic.Modules.Core.Domain.Model.Via;
using Translogic.Modules.Core.Domain.Model.Dto;

namespace Translogic.Modules.Core.Domain.Services.Via.Interface
{
    public interface IElementoViaService
    {
        /// <summary>
        /// Obter as vias (Linhas) por código da estação Mãe (Pátio Origem)
        /// </summary>
        /// <param name="idAreaOperacional"></param>
        /// <returns></returns>
        IList<ElementoViaDto> ObterPorCodigoEstacaoMae(string patio);
    }
}
