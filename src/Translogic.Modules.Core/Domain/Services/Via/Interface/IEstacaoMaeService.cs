﻿

namespace Translogic.Modules.Core.Domain.Services.Via.Interface
{
    using Translogic.Modules.Core.Domain.Model.Via;

    public interface IEstacaoMaeService
    {
        /// <summary>
        /// Obter a area operacional com o código informado
        /// </summary>
        /// <param name="codigo">Código da area operacional</param>
        /// <returns>Retorna a area operacional que tenha o código informado</returns>
        EstacaoMae ObterPorCodigo(string codigo);

        /// <summary>
        /// Retorna o Id EstacaoMae, pelo codigo do local
        /// </summary>
        /// <param name="codigo"></param>
        /// <returns></returns>
        //EstacaoMae ObterIdEstacaoMaePorCodigoDoLocal(string codigo);


    }
}
