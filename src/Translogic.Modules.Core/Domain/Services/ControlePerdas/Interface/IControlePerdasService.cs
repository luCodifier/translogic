﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Translogic.Modules.Core.Domain.Model.ControlePerdas;

namespace Translogic.Modules.Core.Domain.Services.ControlePerdas.Interface
{
    /// <summary>
    /// Interface de Serviço de Controle de Perdas
    /// </summary>
	public interface IControlePerdasService
	{
        /// <summary>
        /// Obtêm a listagem de Recomendação de Vagões
        /// </summary>
        /// <returns>Retorna Lista de Recomendação de Vagões</returns>
        IList<RecomendacaoVagao> ObterListagemRecomendacaoVagao();

        /// <summary>
        /// Obtêm a listagem de Recomendação de Tolerância
        /// </summary>
        /// <returns>Retorna Lista de Recomendação de Tolerância</returns>
        IList<RecomendacaoTolerancia> ObterListagemRecomendacaoTolerancia();

        /// <summary>
        /// Atualiza Lista Recomendações de Vagões e Tolerância
        /// </summary>
        /// <param name="listaRecVagao">Lista Recomendações de Vagões</param>
        /// <param name="listaRecTolerancia">Lista Recomendações de Tolerância</param>
        void SalvarRecomendacoes(IList<RecomendacaoVagao> listaRecVagao, IList<RecomendacaoTolerancia> listaRecTolerancia);
	}
}