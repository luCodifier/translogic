﻿using Translogic.Modules.Core.Domain.Services.ControlePerdas.Interface;
using Translogic.Modules.Core.Domain.Model.ControlePerdas.Repositories;
using Translogic.Modules.Core.Domain.Model.ControlePerdas;
using ALL.Core.Dominio;
using Translogic.Core.Infrastructure.Web;
using System.Collections.Generic;
using System;
using Translogic.Modules.Core.Domain.Model.Seguro.Repositories;
using Translogic.Modules.Regulatorio.Domains.Models.Repositories;
using Translogic.Modules.Core.Domain.Model.Seguro;
using Translogic.Modules.Core.Domain.Model.Estrutura;
using Castle.Services.Transaction;

namespace Translogic.Modules.Core.Domain.Services.ControlePerdas
{
    /// <summary>
    /// Classe de Serviço de Controle de Perdas
    /// </summary>
    public class ControlePerdasService : IControlePerdasService
    {
        private readonly IRecomendacaoVagaoRepository _recomendacaoVagaoRepository;
        private readonly IRecomendacaoToleranciaRepository _recomendacaoToleranciaRepository;
        private readonly ICausaSeguroRepository _causaSeguroRepository;
        private readonly IUnidadeNegocioRepository _unidadeNegocioRepository;

        public ControlePerdasService(IRecomendacaoVagaoRepository recomendacaoVagaoRepository,
                                    IRecomendacaoToleranciaRepository recomendacaoToleranciaRepository,
                                    ICausaSeguroRepository causaSeguroRepository,
                                    IUnidadeNegocioRepository unidadeNegocioRepository)
        {
            _recomendacaoVagaoRepository = recomendacaoVagaoRepository;
            _recomendacaoToleranciaRepository = recomendacaoToleranciaRepository;
            _causaSeguroRepository = causaSeguroRepository;
            _unidadeNegocioRepository = unidadeNegocioRepository;
        }

        /// <summary>
        /// Obtêm a listagem de Recomendação de Vagões
        /// </summary>
        /// <returns>Retorna Lista de Recomendação de Vagões</returns>
        public IList<RecomendacaoVagao> ObterListagemRecomendacaoVagao()
        {
            var listaRecVagao = _recomendacaoVagaoRepository.ObterTodos() as List<RecomendacaoVagao>;
            var listaCausaSeguro = _causaSeguroRepository.ObterTodos();
            foreach (CausaSeguro causaSeguro in listaCausaSeguro)
            {
                Boolean registroEncontrado = listaRecVagao.Exists(x => (x.CausaSeguro != null && x.CausaSeguro.Id.Equals(causaSeguro.Id)));
                if (!registroEncontrado && causaSeguro.Situacao == "S")
                {
                    listaRecVagao.Add(
                        new RecomendacaoVagao
                        {
                            CausaSeguro = causaSeguro,
                            Recomendacao = false,
                            Tolerancia = false
                        }
                    );
                }
            }

            listaRecVagao.Sort((x, y) => x.CausaSeguro.Descricao.CompareTo(y.CausaSeguro.Descricao));
            return listaRecVagao;
        }

        /// <summary>
        /// Obtêm a listagem de Recomendação de Tolerância
        /// </summary>
        /// <returns>Retorna Lista de Recomendação de Tolerância</returns>
        public IList<RecomendacaoTolerancia> ObterListagemRecomendacaoTolerancia()
        {
            return _recomendacaoToleranciaRepository.ObterTodos();
        }

        /// <summary>
        /// Atualiza Lista Recomendações de Vagões e Tolerância
        /// </summary>
        /// <param name="listaRecVagao">Lista Recomendações de Vagões</param>
        /// <param name="listaRecTolerancia">Lista Recomendações de Tolerância</param>
        [Transaction]
        public void SalvarRecomendacoes(IList<RecomendacaoVagao> listaRecomendacaoVagao, IList<RecomendacaoTolerancia> listaRecomendacaoTolerancia)
        {
            foreach (RecomendacaoVagao recVagao in listaRecomendacaoVagao)
            {
                _recomendacaoVagaoRepository.InserirOuAtualizar(recVagao);                
            }

            foreach (RecomendacaoTolerancia recTolerancia in listaRecomendacaoTolerancia)
            {
                _recomendacaoToleranciaRepository.InserirOuAtualizar(recTolerancia);
            }
        }
    }
}