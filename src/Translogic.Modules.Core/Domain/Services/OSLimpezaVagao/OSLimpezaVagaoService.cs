﻿using Translogic.Modules.Core.Domain.Model.Dto;

namespace Translogic.Modules.Core.Domain.Services.OSLimpezaVagao
{    
    using System.Collections.Generic;
    using Translogic.Modules.Core.Domain.Services.OSLimpezaVagao.Interface;
    using Translogic.Modules.Core.Domain.Model.OSLimpezaVagao.Repositories;
    using ALL.Core.Dominio;
    using Translogic.Core.Infrastructure.Web;
    using System;
    using Translogic.Modules.Core.Domain.Model.OSLimpezaVagao;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao.Repositories;
    using Translogic.Modules.Core.Domain.Model.Fornecedor.Repositories;
    using Translogic.Modules.Core.Domain.Model.Via.Repositories;
    using Translogic.Modules.Core.Util;
    using System.IO;
    using Translogic.Core.Infrastructure.FileSystem.ExtensionMethods;
    using ICSharpCode.SharpZipLib.Zip;
    using ICSharpCode.SharpZipLib.Core;
    using Translogic.Modules.Core.Domain.Services.MotivoSituacaoVagao;
    using Translogic.Modules.Core.Domain.Model.MotivoSituacaoVagao;
    using Translogic.Modules.Core.Domain.Model.Via;

    public class OSLimpezaVagaoService : IOSLimpezaVagaoService
    {
        private const int IDMOTIVO = 2;

        #region Repositórios
        private readonly IOSLimpezaVagaoRepository _osLimpezaVagaoRepository;
        private readonly IOSLimpezaVagaoItemRepository _osLimpezaVagaoItemRepository;
        private readonly IVagaoRepository _vagaoRepository;
        private readonly IFornecedorOsRepository _fornecedorOsRepository;
        private readonly IAreaOperacionalRepository _areaOperacional;
        private readonly IOSTipoRepository _tipoRepository;
        private readonly IOSStatusRepository _statusRepository;
        #endregion

        #region Services
        private readonly MotivoSituacaoVagaoService _motivoSituacaoVagaoService;
        #endregion

        #region Construtores
        public OSLimpezaVagaoService(IOSLimpezaVagaoRepository osLimpezaVagaoRepository,
           IOSLimpezaVagaoItemRepository osLimpezaVagaoItemRepository,
           IVagaoRepository vagaoRepository,
           IFornecedorOsRepository fornecedorOsRepository,
           IAreaOperacionalRepository areaOperacional,
           IOSTipoRepository tipoRepository,
           IOSStatusRepository statusRepository,
           MotivoSituacaoVagaoService motivoSituacaoVagaoService)
        {
            _osLimpezaVagaoRepository = osLimpezaVagaoRepository;
            _osLimpezaVagaoItemRepository = osLimpezaVagaoItemRepository;
            _vagaoRepository = vagaoRepository;
            _fornecedorOsRepository = fornecedorOsRepository;
            _areaOperacional = areaOperacional;
            _tipoRepository = tipoRepository;
            _statusRepository = statusRepository;
            _motivoSituacaoVagaoService = motivoSituacaoVagaoService;
        }
        #endregion

        #region Métodos
        public ResultadoPaginado<OSLimpezaVagaoDto> ObterConsultaListaOs(DetalhesPaginacaoWeb detalhesPaginacaoWeb, string dataInicial, string dataFinal, 
            string local, string numOs, string idTipo, string idStatus, string retrabalho)
        {
            DateTime? dtInicial = null;
            DateTime? dtFinal = null;
            bool? temRetrabalho = null; 

            if (retrabalho == "1" || retrabalho.ToLower() == "sim")
                temRetrabalho = true;
            else if (retrabalho == "0" || retrabalho.ToLower() == "não")
                temRetrabalho = false;

            if (!string.IsNullOrEmpty(dataInicial) && dataInicial != "__/__/____") 
            {
                dtInicial = Convert.ToDateTime(dataInicial);
            }

            if (!string.IsNullOrEmpty(dataFinal) && dataInicial != "__/__/____")
            {
                dtFinal = Convert.ToDateTime(dataFinal);
            }

            if (idStatus == "99")
                idStatus = "1,2"; //Caso seja Aberta/Parcial

            return _osLimpezaVagaoRepository.ObterConsultaListaOs(detalhesPaginacaoWeb, dtInicial, dtFinal, local, numOs, idTipo, idStatus, temRetrabalho);
        }

        public ResultadoPaginado<OSLimpezaVagaoItemDto> ObterConsultaDeVagoesParaOs(DetalhesPaginacaoWeb detalhesPaginacaoWeb, int? numOs,
          string listaDeVagoes, string serie, string local, string linha, string idLotacao, int sequenciaInicio, int sequenciaFim)
        {
            if (string.IsNullOrEmpty(local))
                throw new Exception("Campo local é obrigatório");

            return _osLimpezaVagaoItemRepository.ObterConsultaDeVagoesParaOs(detalhesPaginacaoWeb, numOs, listaDeVagoes, serie, local, linha, idLotacao, sequenciaInicio, sequenciaFim);
        }

        public IList<OSVagaoItemLotacaoDto> ObterLotacoes()
        {
            return _osLimpezaVagaoItemRepository.ObterLotacoes();
        }

        public int InserirAtualizarVagoes(int? idOs, IList<int> idsVagoes, string usuario, string local)
        {
            //Salvar OS sem dados para gerar ID, se não houver ID caso contrário atualizar
            var osLimpezaVagao = new OSLimpezaVagao();
            
            var status = _statusRepository.ObterPorId((int)EnumOSLimpezaVagaoStatus.Aberta);
            osLimpezaVagao.Status = status;
            osLimpezaVagao.Usuario = usuario;
            osLimpezaVagao.Local = _areaOperacional.ObterPorCodigo(local.ToUpper());
           
            if (idOs.HasValue){
                osLimpezaVagao.Id = idOs.Value;
                osLimpezaVagao = _osLimpezaVagaoRepository.Atualizar(osLimpezaVagao);

                //Remover vagoes que já existem
                var vagoesRemover = _osLimpezaVagaoItemRepository.ObterVagoesDaOs(idOs.Value);
                _osLimpezaVagaoItemRepository.Remover(vagoesRemover);
            }
            else
                osLimpezaVagao = _osLimpezaVagaoRepository.Inserir(osLimpezaVagao);

            //Salvar vagões da OS
            var itens = new List<OSLimpezaVagaoItem>();
            foreach (var idVagao in idsVagoes)
            {
                var vagao = _vagaoRepository.ObterPorCodigo(idVagao.ToString());

                itens.Add(new OSLimpezaVagaoItem()
                {
                    Vagao = vagao,
                    OSLimpezaVagao = osLimpezaVagao,
                    VersionDate = DateTime.Now
                });
            }
            
            _osLimpezaVagaoItemRepository.Inserir(itens);

            return osLimpezaVagao.Id;
        }

        public ResultadoPaginado<OSLimpezaVagaoItemDto> ObterVagoesOS(DetalhesPaginacaoWeb detalhesPaginacaoWeb, int idOs)
        {
            return _osLimpezaVagaoItemRepository.ObterVagoesDaOs(detalhesPaginacaoWeb, idOs);
        }

        public void AtualizarOSeVagoes(int idOs, int idFornecedor, string local) 
        {
            var osLimpezaVagao = _osLimpezaVagaoRepository.ObterPorId(idOs);
            var fornecedor = _fornecedorOsRepository.ObterPorId(idFornecedor);
            osLimpezaVagao.Fornecedor = fornecedor;
            var localAreaOperacional = _areaOperacional.ObterPorCodigo(local.ToUpper());
            osLimpezaVagao.Local = localAreaOperacional;

            osLimpezaVagao.DataUltimaAlteracao = DateTime.Now;
            osLimpezaVagao.VersionDate = DateTime.Now;
            osLimpezaVagao.Status = _statusRepository.ObterPorId((int)EnumOSLimpezaVagaoStatus.Aberta);

            try
            {
                _osLimpezaVagaoRepository.Atualizar(osLimpezaVagao);
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possível salvar a OS. Erro: " + ex.Message);

                try
                {
                    osLimpezaVagao.Status = _statusRepository.ObterPorId((int)EnumOSLimpezaVagaoStatus.Aberta);
                    _osLimpezaVagaoRepository.Atualizar(osLimpezaVagao);
                }
                catch { }

            }
        }

        public OSLimpezaVagaoDto ObterOS(int idOs) 
        {
            var os = _osLimpezaVagaoRepository.ObterPorId(idOs);

            var osDto = new OSLimpezaVagaoDto();
            osDto.IdOs = os.Id;
            osDto.IdOsParcial = (os.OsLimpezaParcial != null ? os.OsLimpezaParcial.Id : 0);
            osDto.Data = os.Data.Value;
            osDto.IdFornecedor = (os.Fornecedor != null ? os.Fornecedor.Id : 0);
            osDto.Fornecedor = (os.Fornecedor != null ? os.Fornecedor.Nome : "");
            osDto.idLocal  = (os.Local != null ? os.Local.Id.Value: 0);
            osDto.LocalServico = (os.Local != null ? os.Local.Codigo.ToUpper() : "");
            osDto.IdTipo = (os.TipoServico != null ? os.TipoServico.Id : 0);
            osDto.Tipo = (os.TipoServico != null ? os.TipoServico.Descricao : "");
            osDto.IdStatus = os.Status.Id;
            osDto.Status = os.Status.Descricao;
            osDto.DataHoraDevolucaoOS = os.DataHoraDevolucaoOS;
            osDto.DataHoraEntregaOSFornecedor = os.DataHoraEntregaOSFornecedor;
            osDto.NomeAprovadorFornecedor = os.NomeAprovadorFornecedor;
            osDto.NomeAprovadorRumo = os.NomeAprovadorRumo;
            osDto.MatriculaAprovadorRumo = os.MatriculaAprovadorRumo;
            osDto.MatriculaAprovadorFornecedor = os.MatriculaAprovadorFornecedor;
            osDto.Usuario = os.Usuario;
            osDto.UltimaAlteracao = os.DataUltimaAlteracao;
            osDto.JustificativaCancelamento = os.JustificativaCancelamento;

            return osDto;
        }

        public void EditarOs(int idOs, int idFornecedor)
        {
            var os = _osLimpezaVagaoRepository.ObterPorId(idOs);
            os.Fornecedor = _fornecedorOsRepository.ObterPorId(idFornecedor);
            _osLimpezaVagaoRepository.Atualizar(os);
        }

        public int CancelarOs(int idOs)
        {
            var os = _osLimpezaVagaoRepository.ObterPorId(idOs);
            os.Status = _statusRepository.ObterPorId((int)EnumOSLimpezaVagaoStatus.Cancelada);
            _osLimpezaVagaoRepository.Atualizar(os);

            return idOs;
        }

        public void CancelarOs(int idOs, string justificativa)
        {
            var os = _osLimpezaVagaoRepository.ObterPorId(idOs);
            os.Status = _statusRepository.ObterPorId((int)EnumOSLimpezaVagaoStatus.Cancelada);
            os.DataUltimaAlteracao = DateTime.Now;
            os.JustificativaCancelamento = justificativa;
            _osLimpezaVagaoRepository.Atualizar(os);
         }

        public int FecharOs(int idOs, int idFornecedor, int idTipo, string dataHoraEntregaOsFornecedor, string horaEntregaOsFornecedor,
            string dataHoraDevolucaoOSRumo, string horaDevolucaoOSRumo, string nomeAprovadorRumo, string matriculaAprovadorRumo,
            string nomeAprovadorFornecedor, string matriculaAprovadorFornecedor, string codigoUsuario, IList<OSLimpezaVagaoItemDto> vagoes)
        {

            var osLimpezaVagao = _osLimpezaVagaoRepository.ObterPorId(idOs);
            if (osLimpezaVagao == null)
                throw new Exception("OS " + idOs + " não encontrada");

            var fornecedor = _fornecedorOsRepository.ObterPorId(idFornecedor);
            osLimpezaVagao.Fornecedor = fornecedor;

            var tipoServico = _tipoRepository.ObterPorId(idTipo);
            osLimpezaVagao.TipoServico = tipoServico;

            DateTime DataHoraEntregaOSFornecedor;
            DateTime.TryParse(dataHoraEntregaOsFornecedor + " " + horaEntregaOsFornecedor, out DataHoraEntregaOSFornecedor);
            osLimpezaVagao.DataHoraEntregaOSFornecedor = DataHoraEntregaOSFornecedor;

            DateTime DataHoraDevolucaoOS;
            DateTime.TryParse(dataHoraDevolucaoOSRumo + " " + horaDevolucaoOSRumo, out DataHoraDevolucaoOS);
            osLimpezaVagao.DataHoraDevolucaoOS = DataHoraDevolucaoOS;

            osLimpezaVagao.NomeAprovadorRumo = nomeAprovadorRumo;
            osLimpezaVagao.NomeAprovadorFornecedor = nomeAprovadorFornecedor;
            osLimpezaVagao.MatriculaAprovadorRumo = matriculaAprovadorRumo;
            osLimpezaVagao.MatriculaAprovadorFornecedor = matriculaAprovadorFornecedor;

            osLimpezaVagao.DataUltimaAlteracao = DateTime.Now;
            osLimpezaVagao.VersionDate = DateTime.Now;
            osLimpezaVagao.Status = _statusRepository.ObterPorId((int)EnumOSLimpezaVagaoStatus.Fechada);

            //Salvar vagões da OS
            var itens = new List<OSLimpezaVagaoItem>();
            var itensParciais = new List<Int32>();
            foreach (var vagao in vagoes)
            {
                var item = _osLimpezaVagaoItemRepository.ObterPorId((int)vagao.IdItem);
                if (item != null)
                {
                    item.Concluido = (vagao.Concluido.HasValue ? (vagao.Concluido.Value ? 1 : 0) : 0);
                    item.Retrabalho = (vagao.Retrabalho.HasValue ? (vagao.Retrabalho.Value ? 1 : 0) : 0);
                    item.Observacao = (vagao.Observacao != null  ? vagao.Observacao.Replace(">", "").Replace("<", "") : null);
                    item.VersionDate = DateTime.Now;
                    itens.Add(item);

                    if (item.Concluido == 0) //se houver itens não marcados como concluídos, adiciona à lista de id de vagão
                    {
                        itensParciais.Add(Convert.ToInt32(item.Vagao.Codigo));
                    }

                }
            }

            //Atualizar os vagões da OS
            _osLimpezaVagaoItemRepository.Atualizar(itens);

            //Se houver vagões não marcados, criar OS Parcial
            if (itensParciais.Count > 0) {

                var idOSParcial = InserirAtualizarVagoes(null, itensParciais, codigoUsuario, osLimpezaVagao.Local.Codigo);

                //Atualizar OS parcial para Status Parcial
                var osParcial = _osLimpezaVagaoRepository.ObterPorId(idOSParcial);
                osParcial.OsLimpezaParcial = osLimpezaVagao;
                osParcial.Fornecedor = osLimpezaVagao.Fornecedor;
                osParcial.TipoServico = osLimpezaVagao.TipoServico;
                osParcial.Status = _statusRepository.ObterPorId((int)EnumOSLimpezaVagaoStatus.Parcial);
                _osLimpezaVagaoRepository.Atualizar(osParcial);
            }
            

            try
            {
                _osLimpezaVagaoRepository.Atualizar(osLimpezaVagao);
            }
            catch (Exception ex)
            {
                try
                {
                    osLimpezaVagao.Status = _statusRepository.ObterPorId((int)EnumOSLimpezaVagaoStatus.Cancelada);
                    _osLimpezaVagaoRepository.Atualizar(osLimpezaVagao);
                }
                catch { }

                throw new Exception("Não foi possível salvar a OS. Erro: " + ex.Message);
            }


            return idOs;
        }

        private MemoryStream CreateToMemoryStream(Dictionary<string, Stream> arquivos)
        {
            MemoryStream outputMemStream = new MemoryStream();
            ZipOutputStream zipStream = new ZipOutputStream(outputMemStream);

            zipStream.SetLevel(9); // 0-9, 9 being the highest level of compression

            foreach (KeyValuePair<string, Stream> arquivo in arquivos)
            {
                ZipEntry newEntry = new ZipEntry(arquivo.Key);
                newEntry.DateTime = DateTime.Now;

                zipStream.PutNextEntry(newEntry);
                StreamUtils.Copy(arquivo.Value, zipStream, new byte[4096]);
                zipStream.CloseEntry();
            }

            zipStream.IsStreamOwner = false;	// False stops the Close also Closing the underlying stream.
            zipStream.Close();			// Must finish the ZipOutputStream before using outputMemStream.

            outputMemStream.Position = 0;
            return outputMemStream;
        }        

        /// <summary>
        /// Atualizar a observação do Vagão
        /// </summary>
        /// <param name="idItem"></param>
        /// <param name="observacao"></param>
        /// <returns></returns>
        public int SalvarObservacaoVagao(int idItem, string observacao) {

            var item = _osLimpezaVagaoItemRepository.ObterPorId(idItem);
            item.Observacao = observacao;
            item.VersionDate = DateTime.Now;

            _osLimpezaVagaoItemRepository.Atualizar(item);

            return idItem;
        }

        public IList<OSLimpezaVagaoExportaDto> ObterConsultaOsLimpezaExportar(string dataInicial, string dataFinal,
            string local, string numOs, string idTipo, string idStatus, string retrabalho)
        {
            DateTime? dtInicial = null;
            DateTime? dtFinal = null;
            bool? temRetrabalho = null;

            if (retrabalho == "1" || retrabalho.ToLower() == "sim")
                temRetrabalho = true;
            else if (retrabalho == "0" || retrabalho.ToLower() == "não")
                temRetrabalho = false;

            if (!string.IsNullOrEmpty(dataInicial) && dataInicial != "__/__/____")
            {
                dtInicial = Convert.ToDateTime(dataInicial);
            }

            if (!string.IsNullOrEmpty(dataFinal) && dataInicial != "__/__/____")
            {
                dtFinal = Convert.ToDateTime(dataFinal);
            }

            if (idStatus == "99")
                idStatus = "1,2"; //Caso seja Aberta/Parcial

            return _osLimpezaVagaoRepository.ObterConsultaOsParaExportar(dtInicial, dtFinal, local, numOs, idTipo, idStatus, temRetrabalho);
        }

        #endregion
    }
}