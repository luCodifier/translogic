﻿using System.Collections.Generic;
using Translogic.Modules.Core.Domain.Services.OSLimpezaVagao.Interface;
using Translogic.Modules.Core.Domain.Model.Dto;
using Translogic.Modules.Core.Domain.Model.OSLimpezaVagao.Repositories;
using Translogic.Modules.Core.Domain.Model.OSLimpezaVagao;

namespace Translogic.Modules.Core.Domain.Services.OSLimpezaVagao
{
    public class OSTipoService : IOSTipoService
    {
        #region Repositórios

        private readonly IOSTipoRepository _osTipoRepository;

	    #endregion
         
        #region Construtores

        public OSTipoService(IOSTipoRepository osTipoRepository)
        {
            _osTipoRepository = osTipoRepository;
        }

	    #endregion

        #region Métodos

        public IList<OSTipoDto> ObterTiposOSLimpeza()
        {
            return _osTipoRepository.ObterTiposOSLimpeza();
        }

        public IList<OSTipoDto> ObterTiposOSRevistamento()
        {
            return _osTipoRepository.ObterTiposOSRevistamento();
        }
		 
	    #endregion
    }
}