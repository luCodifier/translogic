﻿
namespace Translogic.Modules.Core.Domain.Services.OSLimpezaVagao.Interface
{
    using System.Collections.Generic;
    using Translogic.Modules.Core.Domain.Model.Dto;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public interface IOSTipoService
    {
        /// <summary>
        /// Tipos de OS de limpeza
        /// </summary>
        /// <returns></returns>
        IList<OSTipoDto> ObterTiposOSLimpeza();

        /// <summary>
        /// Tipos de OS de Revistamento
        /// </summary>
        /// <returns></returns>
        IList<OSTipoDto> ObterTiposOSRevistamento();
    }
}
