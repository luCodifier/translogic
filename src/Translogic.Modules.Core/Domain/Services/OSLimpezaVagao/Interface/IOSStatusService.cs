﻿namespace Translogic.Modules.Core.Domain.Services.OSLimpezaVagao.Interface
{
    using System.Collections.Generic;
    using Translogic.Modules.Core.Domain.Model.Dto;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public interface IOSStatusService
    {
        IList<OSStatusDto> ObterStatus();
    }
}
