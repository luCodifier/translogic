﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ALL.Core.Dominio;
using Translogic.Core.Infrastructure.Web;
using Translogic.Modules.Core.Domain.Model.Dto;
using Translogic.Modules.Core.Domain.Model.MotivoSituacaoVagao;
using Translogic.Modules.Core.Domain.Model.Via;

namespace Translogic.Modules.Core.Domain.Services.OSLimpezaVagao.Interface
{
    public interface IOSLimpezaVagaoService
    {
        /// <summary>
        /// Listagem de Ordens de limpeza
        /// </summary>
        /// <param name="detalhesPaginacaoWeb"></param>
        /// <param name="dataInicial"></param>
        /// <param name="dataFinal"></param>
        /// <param name="idLocal"></param>
        /// <param name="numOs"></param>
        /// <param name="idTipo"></param>
        /// <param name="idStatus"></param>
        /// <param name="retrabalho"></param>
        /// <returns></returns>
        ResultadoPaginado<OSLimpezaVagaoDto> ObterConsultaListaOs(DetalhesPaginacaoWeb detalhesPaginacaoWeb, string dataInicial, string dataFinal,
              string local, string numOs, string idTipo, string idStatus, string retrabalho);

        /// <summary>
        /// Lista de lotações dos vagões da OS
        /// </summary>
        /// <returns></returns>
        IList<OSVagaoItemLotacaoDto> ObterLotacoes();

        /// <summary>
        /// Listagem de vagões para a OS
        /// </summary>
        /// <param name="detalhesPaginacaoWeb"></param>
        /// <param name="numOs">ID da OS se ja estiver criada</param>
        /// <param name="listaDeVagoes"></param>
        /// <param name="serie"></param>
        /// <param name="local"></param>
        /// <param name="linha"></param>
        /// <param name="idLotacao"></param>
        /// <param name="sequenciaInicio"></param>
        /// <param name="sequenciaFim"></param>
        /// <returns></returns>
        ResultadoPaginado<OSLimpezaVagaoItemDto> ObterConsultaDeVagoesParaOs(DetalhesPaginacaoWeb detalhesPaginacaoWeb, int? numOs,
            string listaDeVagoes, string serie, string local, string linha, string idLotacao, int sequenciaInicio, int sequenciaFim);

        /// <summary>
        /// Buscar os vagões da OS em resultado paginado
        /// </summary>
        /// <param name="detalhesPaginacaoWeb"></param>
        /// <param name="idOs"></param>
        /// <returns></returns>
        ResultadoPaginado<OSLimpezaVagaoItemDto> ObterVagoesOS(DetalhesPaginacaoWeb detalhesPaginacaoWeb, int idOs);

        /// <summary>
        /// Criar uma nova OS vazia e criar os vagões
        /// </summary>
        /// <param name="idOs">Id OS em caso de edição</param>
        /// <param name="idsVagoes">Lista de Ids dos vagões</param>
        /// <param name="usuario">Usuário que criou</param>
        /// <param name="local">Usuário que criou</param>
        /// <returns></returns>
        int InserirAtualizarVagoes(int? idOs, IList<int> idsVagoes, string usuario, string local);

        /// <summary>
        /// Atualizar a OS com seus dados e atualizar a lista de vagões da OS
        /// </summary>
        /// <param name="idOs"></param>
        /// <param name="idFornecedor"></param>
        /// <param name="local"></param>
        /// <param name="vagoes"></param>
        void AtualizarOSeVagoes(int idOs, int idFornecedor, string local);

        /// <summary>
        /// Buscar dados da OS
        /// </summary>
        /// <param name="idOs"></param>
        /// <returns></returns>
        OSLimpezaVagaoDto ObterOS(int idOs);

        /// <summary>
        /// Edição de OS
        /// </summary>
        /// <param name="idOs"></param>
        /// <param name="idFornecedor"></param>
        void EditarOs(int idOs, int idFornecedor);

        /// <summary>
        /// Cancelar OS fornecida
        /// </summary>
        /// <param name="idOs"></param>
        /// <returns></returns>
        int CancelarOs(int idOs);

        /// <summary>
        /// Cancelar uma OS
        /// </summary>
        /// <param name="idOs"></param>
        /// <param name="justificativa"></param>
        void CancelarOs(int idOs, string justificativa);

        /// <summary>
        /// Fechar a OS com dados de fechamento
        /// </summary>
        /// <param name="idOs"></param>
        /// <param name="dataHoraEntregaOsFornecedor"></param>
        /// <param name="dataHoraDevolucaoOSRumo"></param>
        /// <param name="nomeAprovadorRumo"></param>
        /// <param name="matriculaAprovadorRumo"></param>
        /// <param name="nomeAprovadorFornecedor"></param>
        /// <param name="matriculaAprovadorFornecedor"></param>
        int FecharOs(int idOs, int idFornecedor, int idTipo, string dataHoraEntregaOsFornecedor, string horaEntregaOsFornecedor,
            string dataHoraDevolucaoOSRumo, string horaDevolucaoOSRumo, string nomeAprovadorRumo, string matriculaAprovadorRumo, 
            string nomeAprovadorFornecedor, string matriculaAprovadorFornecedor, string codigoUsuario, IList<OSLimpezaVagaoItemDto> vagoes);

        /// <summary>
        /// Salvar observação do Vagão
        /// </summary>
        /// <param name="idItem"></param>
        /// <param name="observacao"></param>
        /// <returns></returns>
        int SalvarObservacaoVagao(int idItem, string observacao);

        /// <summary>
        /// Retornar listagem de OS e vagões para exportação
        /// </summary>
        /// <param name="dataInicial"></param>
        /// <param name="dataFinal"></param>
        /// <param name="local"></param>
        /// <param name="numOs"></param>
        /// <param name="idTipo"></param>
        /// <param name="idStatus"></param>
        /// <param name="retrabalho"></param>
        /// <returns></returns>
        IList<OSLimpezaVagaoExportaDto> ObterConsultaOsLimpezaExportar(string dataInicial, string dataFinal,
            string local, string numOs, string idTipo, string idStatus, string retrabalho);
    }
}
        