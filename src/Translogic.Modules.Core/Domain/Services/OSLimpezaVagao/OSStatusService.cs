﻿using System.Collections.Generic;
using Translogic.Modules.Core.Domain.Services.OSLimpezaVagao.Interface;
using Translogic.Modules.Core.Domain.Model.Dto;
using Translogic.Modules.Core.Domain.Model.OSLimpezaVagao.Repositories;

namespace Translogic.Modules.Core.Domain.Services.OSLimpezaVagao
{
    public class OSStatusService : IOSStatusService
    {
        private readonly IOSStatusRepository _ostatusRepository;

        public OSStatusService(IOSStatusRepository ostatusRepository)
        {
            _ostatusRepository = ostatusRepository;
        }

        public IList<OSStatusDto> ObterStatus()
        {
            return _ostatusRepository.ObterStatus();
        }
    }
}