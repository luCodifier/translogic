﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using ALL.Core.Dominio;
using Translogic.Core.Infrastructure.Web;
using Translogic.Modules.Core.Domain.Model.Appa;
using Translogic.Modules.Core.Domain.Model.Diversos.Cte;
using Translogic.Modules.Core.Domain.Model.Dto;
using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao;
using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao.Repositories;
using Translogic.Modules.Core.Util;

namespace Translogic.Modules.Core.Domain.Services.Appa
{
    public class IntegracaoAppaService
    {
        private readonly IIntegracaoAppaRepository _integracaoAppaRepository;
        private readonly IVagaoRepository _vagaoRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="IntegracaoAppaService"/> class.
        /// </summary>
        /// <param name="integracaoAppaRepository">Repositório da integração da APPA</param>
        public IntegracaoAppaService(IIntegracaoAppaRepository integracaoAppaRepository, IVagaoRepository vagaoRepository)
        {
            _integracaoAppaRepository = integracaoAppaRepository;
            _vagaoRepository = vagaoRepository;
    }

        public ResultadoPaginado<IntegracaoAppaDto> ObterIntegracoesConsulta(DetalhesPaginacaoWeb pagination, DateTime dataInicial, DateTime dataFinal, string origem, string destino, string vagoes, int situacaoEnvioStatus, int situacaoRecebimentoStatus, string lote)
        {
            vagoes = vagoes ?? string.Empty;
            string[] codigosDosVagoesSeparados =
                vagoes.ToUpper()
                    .Trim()
                    .Replace(" ", string.Empty)
                    .Replace(",", ";")
                    .Replace(".", ";")
                    .Split(';')
                    .Where(e => !string.IsNullOrWhiteSpace(e))
                    .ToArray();

            int? loteId = null;
            if (!String.IsNullOrEmpty(lote))
            {
                int loteTemp;
                if (int.TryParse(lote, out loteTemp) && loteTemp > 0)
                {
                    loteId = loteTemp;
                }
            }

            var resultados = _integracaoAppaRepository.ObterIntegracoesConsulta(pagination, dataInicial, dataFinal, origem, destino, codigosDosVagoesSeparados, situacaoEnvioStatus, situacaoRecebimentoStatus, loteId);
             return resultados;
        }

        public IList<IntegracaoAppaDto> ObterIntegracoesExcel(DateTime dataInicial, DateTime dataFinal, string origem, string destino, string vagoes, int situacaoEnvioStatus, int situacaoRecebimentoStatus, string lote)
        {
            vagoes = vagoes ?? string.Empty;
            string[] codigosDosVagoesSeparados =
                vagoes.ToUpper()
                    .Trim()
                    .Replace(" ", string.Empty)
                    .Replace(",", ";")
                    .Replace(".", ";")
                    .Split(';')
                    .Where(e => !string.IsNullOrWhiteSpace(e))
                    .ToArray();

            int? loteId = null;
            if (!String.IsNullOrEmpty(lote))
            {
                int loteTemp;
                if (int.TryParse(lote, out loteTemp) && loteTemp > 0)
                {
                    loteId = loteTemp;
                }
            }

            var resultados = _integracaoAppaRepository.ObterIntegracoesExcel(dataInicial, dataFinal, origem, destino, codigosDosVagoesSeparados, situacaoEnvioStatus, situacaoRecebimentoStatus, loteId);

            return resultados;
        }

        public Stream ObterXmlEnvio(int id)
        {
            try
            {
                Stream arquivo = new MemoryStream();
            
                // Obtem o Xml
                var registro = _integracaoAppaRepository.ObterIntegracaoPorId(id);
                if (registro != null && !String.IsNullOrEmpty(registro.XmlEnviado))
                {
                    arquivo = Tools.StringToStream(registro.XmlEnviado);
                }

                return arquivo;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public void ReprocessarRegistro(int id)
        {
            #region Validações

            var registro = _integracaoAppaRepository.ObterIntegracaoPorId(id);
            if (registro == null)
                throw new Exception("Integração não encontrada.");

            if (!registro.DataEnvio.HasValue)
                throw new Exception("A integração já está pendente de processamento. Aguarde um instante.");

            if (registro.SucessoEnvioBool && registro.SucessoRetornoBool)
                throw new Exception("A integração foi processada com sucesso anteriormente.");

            #endregion

            _integracaoAppaRepository.ReprocessarRegistro(id);
        }

        /// <summary>
        /// Verifica vagões
        /// </summary>
        /// <param name="listaVagoes">Lista de vagões</param>
        /// <returns>Retorna false quando o vagão for valido</returns>
        public Dictionary<string, KeyValuePair<bool, string>> VerificarListaVagoes(List<string> listaVagoes)
        {
            var listaRetorno = new Dictionary<string, KeyValuePair<bool, string>>();

            if (listaVagoes != null)
            {
                foreach (string vagao in listaVagoes)
                {
                    KeyValuePair<bool, string> statusRetorno = VerificarVagao(vagao);
                    listaRetorno.Add(vagao, statusRetorno);
                }
            }

            return listaRetorno;
        }

        /// <summary>
        /// Verifica se o vagão é válido
        /// </summary>
        /// <param name="codigo">Código do vagão</param>
        /// <returns>Retorna false for um vagão válido - true quando encontrar algum erro</returns>
        public KeyValuePair<bool, string> VerificarVagao(string codigo)
        {
            Vagao vagaoRegistro = _vagaoRepository.ObterPorCodigo(codigo);

            if (codigo.Length != 7)
            {
                return new KeyValuePair<bool, string>(true, "Código do Vagão deve conter 7 dígitos.");
            }

            if (vagaoRegistro == null)
            {
                return new KeyValuePair<bool, string>(true, "Vagão não encontrado na base de dados.");
            }

            return new KeyValuePair<bool, string>(false, "Vagão válido.");
        }

        public Dictionary<string, string> ReprocessarRegistros(List<string> itens)
        {

            Dictionary<string, string> retorno = new Dictionary<string, string>();

            foreach (var item in itens)
            {
                var mensagem = "Sucesso";

                var registro = _integracaoAppaRepository.ObterIntegracaoPorId(Convert.ToInt32(item, CultureInfo.InvariantCulture));
                if (registro == null)
                    mensagem = "Integração não encontrada.";

                if (!registro.DataEnvio.HasValue)
                    mensagem = "A integração já está pendente de processamento. Aguarde um instante.";

                if (registro.SucessoEnvioBool && registro.SucessoRetornoBool)
                    mensagem = "A integração foi processada com sucesso anteriormente.";


                _integracaoAppaRepository.ReprocessarRegistro(Convert.ToInt32(item, CultureInfo.InvariantCulture));

                retorno.Add(item, mensagem);
            }

            return retorno;

        }
    }
}