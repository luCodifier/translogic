﻿
namespace Translogic.Modules.Core.Domain.Services.Tolerancia.Interface
{
    using ALL.Core.Dominio;
    using System.Collections.Generic;
    using Translogic.Core.Infrastructure.Web;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais;
    using Translogic.Modules.Core.Domain.Model.Tolerancia;

    public interface IToleranciaService
    {
        ResultadoPaginado<ToleranciaMercadoriaDto> ObterConsulta(DetalhesPaginacaoWeb detalhesPaginacaoWeb, string dataInicial, string dataFinal, string mercadoria);
        IList<Mercadoria> ListMercadorias();
        ToleranciaMercadoria Obter(int id);

        void Salvar(ToleranciaMercadoria toleranciaMercadoria);

        void Excluir(int id);

        ToleranciaMercadoria ObterInstancia(int id);
    }
}
