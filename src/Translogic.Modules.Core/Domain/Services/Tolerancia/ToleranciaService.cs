﻿
namespace Translogic.Modules.Core.Domain.Services.Tolerancia
{
    using ALL.Core.Dominio;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Translogic.Core.Infrastructure.Web;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Repositories;
    using Translogic.Modules.Core.Domain.Model.Tolerancia;
    using Translogic.Modules.Core.Domain.Model.Tolerancia.Repositories;
    using Translogic.Modules.Core.Domain.Services.Tolerancia.Interface;

    public class ToleranciaService : IToleranciaService
    {
        #region Repositórios
        private readonly IToleranciaMercadoriaRepository _toleranciaRepository;
        private readonly IMercadoriaRepository _mercadoriaRepository;
        #endregion

        #region Construtores
        public ToleranciaService(
            IToleranciaMercadoriaRepository toleranciaRepository, 
            IMercadoriaRepository mercadoriaRepository)
        {
            _toleranciaRepository = toleranciaRepository;
            _mercadoriaRepository = mercadoriaRepository;
        }
        #endregion

        #region Métodos
        public ResultadoPaginado<ToleranciaMercadoriaDto> ObterConsulta(DetalhesPaginacaoWeb detalhesPaginacaoWeb, string dataInicial, string dataFinal, string mercadoria)
        {
            DateTime? dtInicial = null;
            DateTime? dtFinal = null;

            if (!string.IsNullOrEmpty(dataInicial) && dataInicial != "__/__/____")
            {
                dtInicial = Convert.ToDateTime(dataInicial);
            }

            if (!string.IsNullOrEmpty(dataFinal) && dataInicial != "__/__/____")
            {
                dtFinal = Convert.ToDateTime(dataFinal);
            }

            if (mercadoria.ToLower() == "todos")
            {
                mercadoria = null;
            }

            return _toleranciaRepository.ObterConsulta(detalhesPaginacaoWeb, dtInicial, dtFinal, mercadoria);
        }

        public ToleranciaMercadoria Obter(int id)
        {
            return _toleranciaRepository.ObterPorId(id);
        }

        public IList<Mercadoria> ListMercadorias()
        {
            var list = _mercadoriaRepository.ObterTodasMercadorias();

            return list.OrderBy(x => x.DescricaoResumida).ToList();
        }

        public void Salvar(ToleranciaMercadoria toleranciaMercadoria)
        {
            //toleranciaMercadoria.Mercadoria = ObterMercadoria(toleranciaMercadoria.Mercadoria.Id);
            _toleranciaRepository.Salvar(toleranciaMercadoria);
        }

        private Mercadoria ObterMercadoria(int id)
        {
            return _mercadoriaRepository.ObterPorId(id);
        }

        public void Excluir(int id)
        {
            var toleranciaMercadoria = this.Obter(id);
            _toleranciaRepository.Excluir(toleranciaMercadoria);
        }

        public ToleranciaMercadoria ObterInstancia(int id)
        {
            if (id > 0)
                return _toleranciaRepository.ObterPorId(id);
            else
            {
              return new ToleranciaMercadoria() { Mercadoria = new Mercadoria() };                
            }
        }
        #endregion

    }
}