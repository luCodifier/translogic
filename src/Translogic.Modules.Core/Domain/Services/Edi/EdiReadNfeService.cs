﻿namespace Translogic.Modules.Core.Domain.Services.Edi
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Xml;
    using Castle.Services.Transaction;
    using FluxosComerciais;
    using Model.Codificador.Repositories;
    using Speed.Common;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Nfes.Repositories;
    using Translogic.Modules.Core.Spd;
    using Translogic.Modules.EDI.Domain.Models.Nfes;
    using Translogic.Modules.EDI.Domain.Models.Nfes.Repositories;
    using Util;

    /// <summary>
    /// Serviço para busca e processamentos dos emails recebidos pelo Edi
    /// </summary>
    [Transactional]
    public class EdiReadNfeService
    {
        private readonly Email _emailUtil;
        private readonly string _enderecoEmail;
        private readonly string _senhaEmail;
        private readonly string _pastaEmail;
        private readonly int _qtdEmail;
        private readonly int _qtdDiasExclusaoProcessadas;
        private readonly int _qtdDiasExclusaoOutras;
        private readonly NfeService _nfeService;
        private readonly IConfiguracaoTranslogicRepository _configuracaoTranslogicRepository;
        private readonly ILogReadEmailEdiRepository _logReadEmailEdiRepository;
        private readonly ILogReadEmailEdiNfeRepository _logReadEmailEdiNfeRepository;
        private readonly IMonitorEmailJobRunnerRepository _logPrtg;

        /// <summary>
        /// Inicializa uma instância de EdiReadNfeService <see cref="EdiReadNfeService"/>
        /// </summary>
        /// <param name="nfeService">Serviço de ações referentes a Notas Fiscais</param>
        /// <param name="configuracaoTranslogicRepository">Repositório de informações de configuração</param>
        /// <param name="logReadEmailEdiRepository">Repositório de informações de log de leitura de email</param>
        /// <param name="logReadEmailEdiNfeRepository">Repositório de informações de log de leitura de anexo do email</param>
        public EdiReadNfeService(NfeService nfeService, IConfiguracaoTranslogicRepository configuracaoTranslogicRepository, ILogReadEmailEdiRepository logReadEmailEdiRepository, ILogReadEmailEdiNfeRepository logReadEmailEdiNfeRepository, IMonitorEmailJobRunnerRepository logPrtg)
        {
            _nfeService = nfeService;
            _configuracaoTranslogicRepository = configuracaoTranslogicRepository;
            _logReadEmailEdiRepository = logReadEmailEdiRepository;
            _logReadEmailEdiNfeRepository = logReadEmailEdiNfeRepository;
            _enderecoEmail = CacheManager.ConfigGeral.Value.EdiNfeContaEmail;
            _senhaEmail = CacheManager.ConfigGeral.Value.EdiNfeSenhaEmail;
            _pastaEmail = CacheManager.ConfigGeral.Value.EdiNfePastaEmail;
            _qtdEmail = CacheManager.ConfigGeral.Value.EdiNfeQtdaEmail.ToInt32();

            _qtdDiasExclusaoProcessadas = CacheManager.ConfigGeral.Value.EdiNfeDiasRetroativosExclusaoProcessadas.ToInt32();

            Sis.LogTrace("_enderecoEmail; " + _enderecoEmail);

            var _qtdDiasExclusaoOutras = CacheManager.ConfigGeral.Value.EdiNfeDiasRetroativosExclusaoOutras.ToInt32();

            _logPrtg = logPrtg;
            _emailUtil = new Email(_enderecoEmail, _senhaEmail, _pastaEmail, _qtdEmail, _logPrtg, _qtdDiasExclusaoProcessadas, _qtdDiasExclusaoOutras);
        }

        /// <summary>
        /// Método responsável por realizar a busca e invokar o método para processamento.
        /// </summary>
        public void ObterEmailsRealizarProcessamento(int hora, string order)
        {
            _emailUtil.ObterEmailsNaoLidosRealizarProcessamento(ProcessarAnexoXmlEmail, GravarLogLeituraEmail, GravarLogLeituraAnexoEmail, hora, order, SalvarAnexoPdfEmail);
        }

        /// <summary>
        /// Método responsável por realizar a busca e invokar o método para processamento.
        /// </summary>
        public void ProcessarArquivos()
        {
            var dir = CacheManager.ConfigGeral.Value.EdiNfeDiretorio;
            var dirDownload = Path.Combine(dir, "EmailDownload");
            var dirProcessados = Path.Combine(dir, "Processados");
            var dirErros = Path.Combine(dir, "Erros");

            if (!Directory.Exists(dir))
            {
                throw new Exception("Diretório de arquivos não encontrado. Verifica o valor de conf_geral chave EDI_NFE_DIRETORIO: " + dir);
            }
            else
            {
                Sis.LogTrace("Procesando diretório: " + dir);
            }

            Helper.CreateDirectory(dir, dirDownload, dirProcessados, dirErros);

            var fileNames = Directory.GetFiles(dirDownload, "*.*", SearchOption.AllDirectories);

            var total = fileNames.Length;
            var xmls = fileNames.Count(p => Path.GetExtension(p).ToLower() == ".xml");
            var pdfs = fileNames.Count(p => Path.GetExtension(p).ToLower() == ".pdf");

            Sis.LogTrace("Diretório de download: " + dirDownload);
            Sis.LogTrace(string.Format("Arquivos à processar: {0} - XML: {1} - PDF: {2}", total, xmls, pdfs));

            int count = 0;

            // ordena pra processar primeiro os xmls, que inserem na edi2_nfe, e depois o pdf

            fileNames = fileNames.OrderByDescending(p => Path.GetExtension(p).ToLower()).ToArray();

            foreach (var fileName in fileNames)
            {
                count++;
                try
                {
                    Sis.LogTrace(string.Format("Processando {0} de {1}", count, total), fileName);

                    string ext = Path.GetExtension(fileName).ToUpperInvariant();

                    if (ext == ".PDF")
                    {
                        ProcessarPdf(fileName);
                    }
                    else if (ext == ".XML")
                    {
                        ProcessarXmlSeguro(fileName);
                    }

                    MoveFile(dirDownload, dirProcessados, fileName);
                }
                catch (Exception ex)
                {
                    Sis.LogException(ex, "Erro ao processar o arquivo: " + fileName);

                    // deixa na fila, por 5 dias, em caso do erro: ChaveNfe não encontrada em VW_NFE
                    if (!ManterNaFila(fileName, ex))
                    {
                        MoveFile(dirDownload, dirErros, fileName);
                    }
                }
            }

            Helper.ApagarSubdiretoriosVazios(dirDownload);
        }

        /// <summary>
        /// No caso de processamento com o erro "ChaveNfe não encontrada em VW_NFE", verifica se mantém na fila de processamento
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        private bool ManterNaFila(string fileName, Exception ex)
        {
            /*
            int numDias = 5;

            try
            {
                if (ex.Message.Contains("ChaveNfe não encontrada em VW_NFE"))
                {
                    if (DateTime.Today.Date.Subtract(File.GetLastWriteTime(fileName)).Days <= numDias)
                    {
                        return true;
                    }
                }
            }
            catch { }
            */

            return false;
        }

        void ProcessarPdf(string fileName)
        {
            SalvarAnexoPdfEmail(File.ReadAllBytes(fileName));
        }

        void ProcessarXmlSeguro(string fileName)
        {
            try
            {
                ProcessarXml(fileName);
            }
            catch
            {
                var xml = File.ReadAllText(fileName);
                if (!string.IsNullOrWhiteSpace(xml) && !xml.StartsWith("<?xml"))
                {
                    xml = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\r\n" + xml;
                    File.WriteAllText(fileName, xml);
                    ProcessarXml(fileName);
                }
            }
        }

        void ProcessarXml(string fileName)
        {
            var doc = new XmlDocument();
            using (var stream = File.OpenRead(fileName))
            {
                doc.Load(stream);

                var root = doc.DocumentElement;
                var nsmgr = new XmlNamespaceManager(doc.NameTable);
                nsmgr.AddNamespace("d", root.NamespaceURI);

                /* desprexa e-mails de cancelamento */
                if (!root.Name.Equals("procEventoNFe"))
                {
                    /* esse If trata o XML novo da Brado */
                    if (root.Name.Equals("soap:Envelope"))
                    {
                        nsmgr.AddNamespace("soap", root.NamespaceURI);

                        var xmlBrado = new StringBuilder();
                        xmlBrado.Append(
                            "<nfeProc xmlns:ns2=\"http://www.w3.org/2000/09/xmldsig#\" xmlns=\"http://www.portalfiscal.inf.br/nfe\" versao=\"3.10\">");

                        xmlBrado.Append(
                            (((root.SelectSingleNode("//soap:Body", nsmgr)).FirstChild).
                                FirstChild).InnerXml.ToString());

                        xmlBrado.Append("</nfeProc>");

                        /* recarrega o XML em um formato decente */
                        doc.LoadXml(xmlBrado.ToString());
                        root = doc.DocumentElement;
                        nsmgr = new XmlNamespaceManager(doc.NameTable);
                        nsmgr.AddNamespace("d", root.NamespaceURI);
                    }
                    //var noInfNfe = root.Name.Equals("soap:Envelope") ? (((root.SelectSingleNode("//soap:Body", nsmgr)).FirstChild).FirstChild).FirstChild.FirstChild : root.SelectSingleNode("//d:NFe/d:infNFe", nsmgr);

                    var noInfNfe = root.SelectSingleNode("//d:NFe/d:infNFe", nsmgr);
                    var chaveNfe = noInfNfe != null && noInfNfe.Attributes != null && noInfNfe.Attributes["Id"] != null
                                       ? noInfNfe.Attributes["Id"].Value
                                       : null;

                    ProcessarAnexoXmlEmail(doc);
                }
            }
        }

        void MoveFile(string oldDir, string newDir, string fileName)
        {
            try
            {
                var target = Helper.ChangePath(oldDir, newDir, fileName);

                string dir = Path.GetDirectoryName(target);
                if (!Directory.Exists(dir))
                    Directory.CreateDirectory(dir);

                if (File.Exists(target))
                {
                    File.Delete(target);
                }

                File.Move(fileName, target);
                File.Delete(fileName);
            }
            catch { }
        }

        /// <summary>
        /// Método responsável por excluir os e-mails antigos das caixas secundárias
        /// </summary>
        public void ExcluirEmailsMovidosCaixasSecundarias(string pastaExclusaoEmails)
        {
            int diasExclusao = this.ObterDiasExclusaoPastaSecundaria(pastaExclusaoEmails);
            _emailUtil.ExcluirEmailsMovidosCaixasSecundarias(GravarLogLeituraEmail, pastaExclusaoEmails, diasExclusao);
        }

        /// <summary>
        /// Realiza o processamento dos anexos recebidos por email
        /// </summary>
        /// <param name="anexo">Anexo recebido por email</param>
        private void ProcessarAnexoXmlEmail(XmlDocument anexo)
        {
            if (anexo != null)
            {
                _nfeService.ProcessarDadosNfeEdi(anexo);
            }
        }

        /// <summary>
        /// Salva o pdf dos anexos recebidos por email
        /// </summary>
        /// <param name="anexo">Anexo recebido por email</param>
        private void SalvarAnexoPdfEmail(byte[] anexo)
        {
            if (anexo != null)
            {
                _nfeService.SalvarPdfNfeEdi(anexo);
            }
        }

        /// <summary>
        /// Grava o log da leitura do email
        /// </summary>
        /// <param name="logEmail">Objeto com as informações do Log de Email</param>
        private void GravarLogLeituraEmail(LogReadEmailEdi logEmail)
        {
            try
            {
                Sis.LogTrace(string.Format("{0} - {1} - {2}", logEmail.Remetente, logEmail.AssuntoEmail, logEmail.MensagemErro));

                if (logEmail.Id.HasValue)
                {
                    logEmail.DataFimLog = DateTime.Now;
                    _logReadEmailEdiRepository.AtualizarLog(logEmail);
                }
                else
                {
                    _logReadEmailEdiRepository.InserirOuAtualizar(logEmail);
                }
            }
            catch { }
        }

        /// <summary>
        /// Grava o log do processamento do anexo do email
        /// </summary>
        /// <param name="logEmailNfe">Objeto com as informações do Log do anexo de Email</param>
        private void GravarLogLeituraAnexoEmail(LogReadEmailEdiNfe logEmailNfe)
        {
            try
            {
                var nfe = _nfeService.ObterNfeEdiPorChave(logEmailNfe.NfeChave);

                logEmailNfe.NfeEdiBanco = nfe;

                _logReadEmailEdiNfeRepository.InserirOuAtualizar(logEmailNfe);
            }
            catch { }
        }

        private int ObterDiasExclusaoPastaSecundaria(string pastaSecundaria)
        {
            pastaSecundaria = pastaSecundaria.ToLower();
            switch (pastaSecundaria)
            {
                case "processadas":
                    return _qtdDiasExclusaoProcessadas;
                case "erro":
                    return _qtdDiasExclusaoOutras;
                case "semxml":
                    return _qtdDiasExclusaoOutras;
                case "eventos_nfe":
                    return _qtdDiasExclusaoOutras;
            }

            return 1000;
        }

        /// <summary>
        /// Método que vai receber os dados da Nfe que sta sendo inserida na base para conferido se existe um Fluxo válido para a mesma
        /// </summary>
        public void ValidaExistenciaFluxoNota()
        {
            _nfeService.ValidaExistenciaFluxoNota();
        }

    }
}