﻿namespace Translogic.Modules.Core.Domain.Services.Edi
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text.RegularExpressions;
    using ALL.Core.Dominio;
    using Translogic.Core.Infrastructure;
    using Translogic.Core.Infrastructure.Web;
    using Translogic.Modules.Core.Domain.Model.Bolar.Repositories;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Modules.Core.Domain.Model.Edi;
    using Translogic.Modules.Core.Domain.Model.Edi.Enum;
    using Translogic.Modules.Core.Domain.Model.Edi.Repositories;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Nfes.Repositories;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Simconsultas.Repositories;
    using Translogic.Modules.EDI.Domain.Models.Nfes.Repositories;

    public class EdiErroService
    {
        private readonly IEdiErroRepository _ediErroRepository;
        private readonly IEdiErroNfRepository _ediErroNfRepository;
        private readonly INfeSimconsultasRepository _nfeSimconsultasRepository;
        private readonly INfeDistribuicaoReadonlyRepository _nfeDistribuicaoReadonlyRepository;
        private readonly IBolarBoImpCfgRepository _bolarBoImpCfgRepository;

        /// <summary>
        ///  Initializes a new instance of the <see cref="EdiErroService"/> class.
        /// </summary>
        /// <param name="painelExpedicaoRepository">Repositório do Painel de Expedição</param>
        public EdiErroService(IEdiErroRepository ediErroRepository,
                              INfeSimconsultasRepository nfeSimconsultasRepository,
                              INfeDistribuicaoReadonlyRepository nfeDistribuicaoReadonlyRepository,
                              IEdiErroNfRepository ediErroNfRepository,
                              IBolarBoImpCfgRepository bolarBoImpCfgRepository)
        {
            this._ediErroRepository = ediErroRepository;
            this._nfeSimconsultasRepository = nfeSimconsultasRepository;
            this._nfeDistribuicaoReadonlyRepository = nfeDistribuicaoReadonlyRepository;
            this._ediErroNfRepository = ediErroNfRepository;
            this._bolarBoImpCfgRepository = bolarBoImpCfgRepository;
        }

        #region Carregar Erros

        public bool CarregarErros(DateTime dataInicial, DateTime dataFinal)
        {
            dataFinal = dataFinal.AddDays(1);

            // Adquire todas as mensagens de erros do EDI - Shipping Note
            var mensagensErro = _ediErroRepository.ObterMensagensRecebimentoErro(dataInicial, dataFinal);
            //var mensagemErro = this.ObterErrosPendentesEDI(dataInicial, dataFinal);

            // Para cada mensagem encontrada, devemos verificar se as notas da mensagem já se encontram no banco.
            foreach (var mensagem in mensagensErro)
            {
                // Verificando se já foi inserido o erro na tabela de erros
                //var registroEdiErro = _ediErroRepository.ObterEdiErro((int)mensagem.Id);
                if (!mensagem.EdiErroId.HasValue)
                {
                    // Ainda não foi inserido
                    // Verificando se as notas da mensagem estão no banco, se não estiverem, devemos registrar o erro de NFe faltante
                    if (!VerificaRecebimentoNFe(mensagem))
                    {
                        // As notas não foram recebidas. Agora devemos inserir na tabela de erros
                        InsereRegistroEdiErroNfe(mensagem);
                    }

                    // Outros erros podem ser inseridos aqui
                }
                else
                {
                    // Já foi inserido anteriormente. Devemos apenas atualizar o registro
                    var registroEdiErro = _ediErroRepository.ObterEdiErro((int)mensagem.Id);
                    AtualizaRegistroEdiErroNfe(mensagem, registroEdiErro);
                }
            }

            return true;
        }

        /// <summary>
        /// Verifica se as notas da mensagem foram recebidas por algum meio (SimConsulta, e-mail, etc)
        /// </summary>
        /// <param name="mensagem">A mensagem do EDI Shipping Note</param>
        /// <returns>True se todas as NFs da requisição foram recebidas</returns>
        private bool VerificaRecebimentoNFe(MensagemRecebimentoDto mensagem)
        {
            var chaves = ObtemChavesNfsMensagemEdi(mensagem);
            var chavesOtherXml = ObtemChavesOtherXml(mensagem);

            foreach (var chave in chaves)
            {
                if (!chavesOtherXml.Contains(chave))
                {
                    /* Procura no banco pela chave se não encontrar retorno = false */
                    if (!VerificaNfeRecebida(chave))
                        return false;
                }
            }

            return true;
        }

        private bool VerificaRecebimentoNFe(MensagemRecebimentoDto mensagem, string chaveNfe)
        {
            var chavesOtherXml = ObtemChavesOtherXml(mensagem);
            if (chavesOtherXml != null)
            {
                if (chavesOtherXml.Contains(chaveNfe))
                    return true;
            }

            return VerificaNfeRecebida(chaveNfe);
        }

        private bool VerificaNfeRecebida(string chaveNfe)
        {
            // Verifica se encontra no banco do EDI
            var nfe = _ediErroNfRepository.ExisteChaveNfeEdi(chaveNfe);
            if (nfe)
                return true;

            var nfeSimConsultas = _nfeSimconsultasRepository.ExisteChaveNfe(chaveNfe);
            if (nfeSimConsultas)
                return true;

            var nfeDistribuicao = _nfeDistribuicaoReadonlyRepository.ObterPorChaveNfe(chaveNfe);
            if (nfeDistribuicao != null)
                return true;

            return false;
        }

        /// <summary>
        /// Retorna todas as mensagens do EDI - Shipping Note que tiveram erro de processamento
        /// </summary>
        public List<MensagemRecebimentoDto> ObterErrosPendentesEDI(DateTime dataInicial, DateTime dataFinal)
        {
            dataFinal = dataFinal.AddDays(1);

            var resultados = new List<MensagemRecebimentoDto>();

            // Adquirindo as mensagens com erros
            var mensagensComErros = _ediErroRepository.ObterMensagensRecebimentoErro(dataInicial, dataFinal);

            foreach (var mensagem in mensagensComErros)
            {
                // Adquirindo apenas as mensagens do EDI - Shipping Notes
                if (VerificaMensagemEdi(mensagem))
                {
                    // Verificando se a mensagem já não foi inserida anteriormente
                    if (!resultados.Any(r => r.Id == mensagem.Id))
                    {
                        resultados.Add(mensagem);
                    }
                }
            }

            return resultados;
        }

        /// <summary>
        /// Verifica se a mensagem é do EDI - Shipping Notes
        /// </summary>
        /// <param name="mensagem">A mensagem a ser validada</param>
        /// <returns>True se a mensagem é do EDI - Shipping Notes</returns>
        private bool VerificaMensagemEdi(MensagemRecebimentoDto mensagem)
        {
            // Mensagens do EDI - Shipping Note possuem na tag MessageType a informação NOTA_EXP_FERR
            bool resultado = mensagem.ArquivoMensagem.Contains("<MessageType>NOTA_EXP_FERR</MessageType>");

            return resultado;
        }

        private void InsereRegistroEdiErroNfe(MensagemRecebimentoDto mensagem)
        {
            var vagao = ObtemVagaoMensagemEdi(mensagem);
            var fluxo = ObtemFluxoMensagemEdi(mensagem);
            var responsavel = ObtemResponsavelMensagemEdi(mensagem);
            var dataErro = mensagem.DataCadastro;
            var dataEnvio = mensagem.DataCadastro;

            var registro = new EdiErro();
            registro.TipoErro = (int)EdiTipoErroEnum.NotaFiscalNaoRecebida;
            registro.IdMensagemRecebimento = (int)mensagem.Id;
            registro.Vagao = vagao;
            registro.Fluxo = fluxo;
            registro.DataErro = dataErro;
            registro.DataEnvio = dataEnvio;
            registro.ResponsavelEnvio = responsavel;
            registro.Reprocessado = "N";

            _ediErroRepository.Inserir(registro);

            var chavesNfs = ObtemChavesNfsMensagemEdi(mensagem);

            foreach (var chave in chavesNfs)
            {
                var registroNfe = new EdiErroNf();
                registroNfe.EdiErro = registro;
                registroNfe.ChaveNfe = chave;

                bool nfEncontrada = VerificaRecebimentoNFe(mensagem, chave);
                if (nfEncontrada)
                {
                    registroNfe.DataRegularizacao = DateTime.Now;
                }
                else
                {
                    registroNfe.DataRegularizacao = null;
                }

                _ediErroNfRepository.Inserir(registroNfe);
            }
        }

        private void AtualizaRegistroEdiErroNfe(MensagemRecebimentoDto mensagem, EdiErro ediErro)
        {
            if (ediErro.TipoErro == (int)EdiTipoErroEnum.NotaFiscalNaoRecebida)
            {
                bool reprocessado = true;
                var ediErroNfs = _ediErroNfRepository.ObterPorIdEdiErro(ediErro.Id);
                foreach (var erroNf in ediErroNfs)
                {
                    if (!erroNf.DataRegularizacao.HasValue)
                    {
                        var nfRecebida = VerificaRecebimentoNFe(mensagem, erroNf.ChaveNfe);
                        if (nfRecebida)
                        {
                            erroNf.DataRegularizacao = DateTime.Now;
                            _ediErroNfRepository.Atualizar(erroNf);
                        }
                    }
                    else
                    {
                        reprocessado = false;
                    }
                }

                if (!reprocessado)
                {
                    ediErro.Reprocessado = "N";
                    _ediErroRepository.Atualizar(ediErro);
                }
            }
        }

        private string ObtemVagaoMensagemEdi(MensagemRecebimentoDto mensagem)
        {
            if (mensagem == null)
                return String.Empty;

            string pattern = "<WagonCode>[0-9]{7}</WagonCode>";
            string input = mensagem.ArquivoMensagem;
            string vagao = String.Empty;

            foreach (Match match in Regex.Matches(input, pattern))
            {
                vagao = match.Value.Replace("<WagonCode>", String.Empty).Replace("</WagonCode>", String.Empty);
            }

            return vagao.Trim();
        }

        private string ObtemFluxoMensagemEdi(MensagemRecebimentoDto mensagem)
        {
            if (mensagem == null)
                return String.Empty;

            string pattern = "<FlowId>[0-9]{5}</FlowId>";
            string input = mensagem.ArquivoMensagem;
            string fluxo = String.Empty;

            foreach (Match match in Regex.Matches(input, pattern))
            {
                fluxo = match.Value.Replace("<FlowId>", String.Empty).Replace("</FlowId>", String.Empty);
            }

            return fluxo.Trim();
        }

        private string ObtemResponsavelMensagemEdi(MensagemRecebimentoDto mensagem)
        {
            if (mensagem == null)
                return String.Empty;

            string pattern = "<BusinessIdentifier>[0-9]{4,14}</BusinessIdentifier>";
            string input = mensagem.ArquivoMensagem;
            string cnpj = String.Empty;

            foreach (Match match in Regex.Matches(input, pattern))
            {
                if (String.IsNullOrEmpty(cnpj))
                    cnpj = match.Value.Replace("<BusinessIdentifier>", String.Empty).Replace("</BusinessIdentifier>", String.Empty);
            }

            var responsavelFaturamento = String.Empty;
            if (!String.IsNullOrEmpty(cnpj))
            {
                var cfg = _bolarBoImpCfgRepository.ObterPorCnpjAtivo(cnpj);
                if (cfg == null)
                {
                    /* Pegar os dados da BolarBoImpCfgFilial */
                    responsavelFaturamento = cnpj;
                }
                else
                {
                    responsavelFaturamento = cfg.DescricaoEmpresa;
                }
            }

            return responsavelFaturamento;
        }

        private List<string> ObtemChavesOtherXml(MensagemRecebimentoDto mensagem)
        {
            var chaves = new List<string>();

            if (mensagem == null)
                return chaves;

            string pattern = "NFe[0-9]{44}";
            string input = mensagem.ArquivoMensagem;

            foreach (Match match in Regex.Matches(input, pattern))
            {
                var chave = match.Value.Replace("NFe", String.Empty);
                if (!chaves.Contains(chave))
                    chaves.Add(chave);
            }

            return chaves;
        }

        private List<string> ObtemChavesNfsMensagemEdi(MensagemRecebimentoDto mensagem)
        {
            var chaves = new List<string>();

            if (mensagem == null)
                return chaves;

            string pattern = "<WaybillInvoiceKey>[0-9]{44}</WaybillInvoiceKey>";
            string input = mensagem.ArquivoMensagem;

            foreach (Match match in Regex.Matches(input, pattern))
            {
                var chave = match.Value.Replace("<WaybillInvoiceKey>", String.Empty).Replace("</WaybillInvoiceKey>", String.Empty);
                if (!chaves.Contains(chave))
                    chaves.Add(chave);
            }

            return chaves;
        }

        #endregion

        #region Pesquisa Erros

        public ResultadoPaginado<EdiErroResultPesquisaDto> ObterEdiErrosPesquisa(
            DetalhesPaginacaoWeb pagination,
            DateTime dataInicial,
            DateTime dataFinal,
            string fluxo,
            string vagoes,
            string responsavelEnvio)
        {
            CarregarErros(dataInicial, dataFinal);

            vagoes = vagoes ?? string.Empty;

            string[] codigosDosVagoesSeparados =
                vagoes.ToUpper()
                    .Trim()
                    .Replace(" ", string.Empty)
                    .Replace(",", ";")
                    .Replace(".", ";")
                    .Split(';')
                    .Where(e => !string.IsNullOrWhiteSpace(e))
                    .ToArray();

            var result = this._ediErroRepository.ObterEdiErrosPesquisa(pagination,
                                                                       dataInicial,
                                                                       dataFinal,
                                                                       fluxo,
                                                                       codigosDosVagoesSeparados,
                                                                       responsavelEnvio);
            return result;
        }

        #endregion

        #region Regularizar Notas

        public List<int> RegularizarNotas(List<EdiErroResultPesquisaDto> listaEdiErros)
        {
            var notasNaoRegularizadasTela1154 = new List<int>();

            foreach (var ediErro in listaEdiErros)
            {
                if (!ediErro.DataRegularizacao.HasValue)
                {
                    if (ediErro.DataRegularizacao == null)
                    {
                        // Regularizar caso a data de regularização esteja nula

                        var nfe = _ediErroNfRepository.ExisteChaveNfeEdi(ediErro.ChaveNfe);
                        if (!nfe)
                        {
                            bool bExisteNota = false;

                            // Verifica se existe na SimConsultas
                            var existeNfeSimConsultas = _nfeSimconsultasRepository.ExisteChaveNfe(ediErro.ChaveNfe);
                            if (existeNfeSimConsultas)
                            {
                                bExisteNota = true;
                            }

                            // Verifica se existe na NfeDistribuicao
                            if (!(bExisteNota))
                            {
                                var nfeDistribuicao = _nfeDistribuicaoReadonlyRepository.ObterPorChaveNfe(ediErro.ChaveNfe);
                                if (nfeDistribuicao != null)
                                {
                                    bExisteNota = true;
                                }
                            }

                            if (bExisteNota)
                            {
                                var ediErroNf = _ediErroNfRepository.ObterPorId((int)ediErro.IdErroEdiNfe);
                                if (ediErroNf != null && ediErroNf.DataRegularizacao.HasValue == false)
                                {
                                    ediErroNf.DataRegularizacao = DateTime.Now;
                                    _ediErroNfRepository.Atualizar(ediErroNf);
                                }
                            }
                            else
                            {
                                // Nota não regularizada - Adiciona na lista para redirecionar
                                // para a tela 1154 (Baixar Notas)
                                notasNaoRegularizadasTela1154.Add(int.Parse(ediErro.IdErroEdiNfe.ToString()));
                            }
                        }
                    }
                }
            }

            return notasNaoRegularizadasTela1154;
        }

        /// <summary>
        /// Retorna as chaves dos ids solicitados
        /// </summary>
        /// <param name="ids">IDs em formato de string, separados por ';'</param>
        /// <returns>Retorna as chaves</returns>
        public string RetornaChaves(string ids)
        {
            var resultadoAgrupado = String.Empty;

            if (String.IsNullOrEmpty(ids))
                return String.Empty;

            ids = ids.Replace(';', ',');

            var resultados = _ediErroRepository.RetornarChaves(ids);

            foreach (var resultado in resultados)
            {
                if (String.IsNullOrEmpty(resultadoAgrupado))
                {
                    resultadoAgrupado = resultado;
                }
                else
                {
                    resultadoAgrupado = String.Format("{0};{1}", resultadoAgrupado, resultado);
                }
            }

            return resultadoAgrupado;
        }

        #endregion

        #region Reprocessar Notas

        public bool ReprocessarNotas(List<EdiErroResultPesquisaDto> listaNotasReprocessar)
        {
            // Verifica Mensagens recebimento com critério para reprocessamento
            //   - Requisito 1: Deve estar como MRE na mensagemRecebimento
            //   - Requisito 2: Todas as notas devem estar regularizadas na EdiErroNf

            List<decimal> listaMensagensDistintas = listaNotasReprocessar.Select(i => i.IdMensagemRecebimento).Distinct().ToList();

            List<MensagemRecebimentoDto> listaMensagensRecebimentoParaReprocessar =
                _ediErroRepository.ObterMensagensRecebimentoParaReprocessar(listaMensagensDistintas);

            var mensagensComErro = new List<decimal>();
            var mensagensProcessadas = new List<decimal>();
            var mensagensSemNf = new List<decimal>();
            if (listaMensagensRecebimentoParaReprocessar != null)
            {
                foreach (var mensagem in listaMensagensRecebimentoParaReprocessar)
                {
                    if (mensagem.HaPendenciaNotas)
                    {
                        mensagensSemNf.Add(mensagem.Id);
                    }
                    else if (mensagem.Situacao == "MRE")
                    {
                        mensagensComErro.Add(mensagem.Id);
                    }
                    else if (mensagem.Situacao == "MRP" || mensagem.Situacao == "MRR")
                    {
                        mensagensProcessadas.Add(mensagem.Id);
                    }
                }
            }

            if (mensagensSemNf.Count > 0)
            {
                throw new TranslogicException("Algumas notas estão pendentes de baixa da NF-e. Por favor, elimine-as do reprocessamento ou realize as suas baixas.");
            }

            if (mensagensComErro.Count > 0)
            {
                _ediErroRepository.AtualizarMensagensRecebimentoParaReprocessar(mensagensComErro);
                _ediErroRepository.AtualizarEdiErrosProcessados(mensagensComErro);
            }

            if (mensagensProcessadas.Count > 0)
            {
                _ediErroRepository.AtualizarEdiErrosProcessados(mensagensProcessadas);
            }

            return true;
        }

        #endregion
    }
}