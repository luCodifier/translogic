﻿namespace Translogic.Modules.Core.Domain.Services.ChecklistPassageiro
{
    using Translogic.Modules.Core.Domain.Services.ChecklistPassageiro.Interface;
    using ALL.Core.Dominio;
    using Translogic.Modules.Core.Domain.Model.Dto.ChecklistPassageiro;
    using Translogic.Core.Infrastructure.Web;
    using Translogic.Modules.Core.Domain.Model.ChecklistPassageiros.Repositories;
    using System;
    using Translogic.Modules.Core.Domain.Model.ChecklistPassageiros;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Modules.Core.Domain.Model.Trem.OrdemServico.Repositories;
    using Microsoft.Practices.ServiceLocation;
    using Translogic.Modules.Core.Domain.Services.Acesso;
    using Translogic.Modules.Core.Domain.Model.Acesso;
    using System.Collections.Generic;
    using Translogic.Modules.Core.Util;

    public class OsChecklistService : IOsChecklistService
    {
        private readonly IOsChecklistRepository _osCheckListRepository;
        private readonly IOsChecklistLogRepository _osCheckListLogRepository;
        private readonly IOsChecklistAprovacaoRepository _osChecklistAprovacaoRepository;
        private readonly IOsChecklistAprovacaoLogRepository _osChecklistAprovacaoLogRepository;
        private readonly IOsChecklistPassageiroRepository _osChecklistPassageiroRepository;
        private readonly IOsChecklistPassageiroLogRepository _osChecklistPassageiroLogRepository;
        private readonly IOrdemServicoRepository _ordemServicoRepository;
        private readonly IOsChecklistImportacaoRepository _osChecklistImportacaoRepository;

        public OsChecklistService(IOsChecklistRepository osCheckListRepository,
            IOsChecklistLogRepository osCheckListLogRepository,
            IOsChecklistAprovacaoRepository osChecklistAprovacaoRepository,
            IOsChecklistAprovacaoLogRepository osChecklistAprovacaoLogRepository,
            IOsChecklistPassageiroRepository osChecklistPassageiroRepository,
            IOsChecklistPassageiroLogRepository osChecklistPassageiroLogRepository,
            IOrdemServicoRepository ordemServicoRepository,
            IOsChecklistImportacaoRepository osChecklistImportacaoRepository)
        {
            _osCheckListRepository = osCheckListRepository;
            _osCheckListLogRepository = osCheckListLogRepository;
            _osChecklistAprovacaoRepository = osChecklistAprovacaoRepository;
            _osChecklistAprovacaoLogRepository = osChecklistAprovacaoLogRepository;
            _osChecklistPassageiroRepository = osChecklistPassageiroRepository;
            _osChecklistPassageiroLogRepository = osChecklistPassageiroLogRepository;
            _ordemServicoRepository = ordemServicoRepository;
            _osChecklistImportacaoRepository = osChecklistImportacaoRepository;
        }

        public ResultadoPaginado<OsChecklistDto> ObterConsultaOsCheckList(DetalhesPaginacaoWeb detalhesPaginacaoWeb, string dataInicial, string dataFinal, string numOs)
        {
            string dataInicio = "";
            string dataFim = "";
            var os = 0;

            if (!string.IsNullOrEmpty(numOs))
            {
                os = Convert.ToInt32(numOs);

                if (!_osCheckListRepository.OsDePassageiro(os))
                {
                    throw new Exception("A OS informada não foi encontrada");
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(dataInicial) && dataInicial != "__/__/____")
                {
                    dataInicio = dataInicial;
                }

                if (!string.IsNullOrEmpty(dataFinal) && dataFinal != "__/__/____")
                {
                    dataFim = dataFinal;
                }
            }

            var result = _osCheckListRepository.BuscarOsCheklist(detalhesPaginacaoWeb, dataInicio, dataFim, os);

            if (result.Total == 0 && !string.IsNullOrEmpty(numOs))
            {
                throw new Exception("A OS informada não é válida");
            }

            return result;
        }

        public int SalvarAprovacoes(OsChecklistAprovacaoDto osChecklistAprovacaoDto)
        {
            var osChecklist = new Os_Checklist();
            var obsChecklistLog = string.Empty;
            var estaConcluido = false;
            var obsChecklistAprovacaoLog = string.Empty;
            var osChecklistAprovacao = new Os_Checklist_Aprovacao();

            var CheckList = _osCheckListRepository.ObterOSCheckListPorIdOS(osChecklistAprovacaoDto.idOs).Id;

            osChecklistAprovacaoDto.idOsChecklist = CheckList;

            if (osChecklistAprovacaoDto.idOsChecklist == 0)
            {
                osChecklist = new Os_Checklist(osChecklistAprovacaoDto.idOsChecklist,
                osChecklistAprovacaoDto.idOs,
                osChecklistAprovacaoDto.Ida ? 1 : 0,
                osChecklistAprovacaoDto.Ida ? 0 : 1,
                estaConcluido ? 1 : 0,
                osChecklistAprovacaoDto.Matricula);

                obsChecklistLog = string.Format("Novo Checklist para OS {0}", osChecklistAprovacaoDto.idOs);
                obsChecklistAprovacaoLog = string.Format("Nova aprovação de {0} para OS {1}", osChecklistAprovacaoDto.TipoAprovacao, osChecklistAprovacaoDto.idOs);
            }
            else
            {
                estaConcluido = _osChecklistAprovacaoRepository.Concluido(osChecklistAprovacaoDto.idOsChecklist);

                try
                {
                    osChecklist = _osCheckListRepository.ObterPorId(osChecklistAprovacaoDto.idOsChecklist);
                }
                catch (Exception ex)
                {

                    obsChecklistLog = string.Format("Erro ao buscar ao buscar Checklist {0}. Erro: {1}", osChecklistAprovacaoDto.idOsChecklist, ex.Message.Replace("'", ""));

                    throw new Exception(obsChecklistLog);
                }

                osChecklist.Concluido = estaConcluido ? 1 : 0;
                osChecklist.Ida = osChecklistAprovacaoDto.Ida ? 1 : 0;
                osChecklist.Volta = osChecklistAprovacaoDto.Ida ? 0 : 1;

                obsChecklistLog = string.Format("Atualização do Checklist {0} para OS {1}", osChecklistAprovacaoDto.idOsChecklist, osChecklistAprovacaoDto.idOs);
                obsChecklistAprovacaoLog = string.Format("Atualização de aprovação de {0} para OS {1}. Status: {2}", osChecklistAprovacaoDto.TipoAprovacao, osChecklistAprovacaoDto.idOs, osChecklistAprovacaoDto.Aprovado ? "Aprovado" : "Reprovado");

                try
                {
                    osChecklistAprovacao = _osChecklistAprovacaoRepository.ObterPorOsChecklist(osChecklistAprovacaoDto.idOsChecklist);
                }
                catch (Exception ex)
                {
                    obsChecklistAprovacaoLog = string.Format("Erro ao buscar aprovações do Checklist {0}. Erro: {1}", osChecklistAprovacaoDto.idOsChecklist, ex.Message.Replace("'", ""));

                    throw new Exception(obsChecklistAprovacaoLog);
                }
            }

            try
            {
                osChecklist = _osCheckListRepository.InserirOuAtualizar(osChecklist);
            }
            catch (Exception ex)
            {
                obsChecklistLog = string.Format("Erro ao inserir Checklist para os {0}. Erro: {1}", osChecklistAprovacaoDto.idOs, ex.Message.Replace("'", ""));

                throw new Exception(obsChecklistLog);
            }

                var osChecklistLog = new Os_Checklist_Log(osChecklist, osChecklistAprovacaoDto.Matricula, obsChecklistLog);
                _osCheckListLogRepository.Inserir(osChecklistLog);

            if (osChecklistAprovacao == null)
            {
                osChecklistAprovacao = new Os_Checklist_Aprovacao();
            }

            osChecklistAprovacao = SetarPropriedadesAprovacaoPorTipo(
                   osChecklistAprovacao,
                   osChecklistAprovacaoDto.TipoAprovacao,
                   osChecklistAprovacaoDto.DataHoraAprocacao,
                   osChecklistAprovacaoDto.Usuario,
                   osChecklistAprovacaoDto.Matricula,
                   osChecklistAprovacaoDto.Aprovado ? 'A' : 'R',
                   'U',
                   osChecklistAprovacaoDto.Observacao);

            osChecklistAprovacao.OsChecklist = osChecklist;

            try
            {
                osChecklistAprovacao = _osChecklistAprovacaoRepository.InserirOuAtualizar(osChecklistAprovacao);
            }
            catch (Exception ex)
            {
                obsChecklistAprovacaoLog = string.Format("Erro ao inserir ou atualizar aprovações do Checklist {0}. Erro: {1}", osChecklistAprovacaoDto.idOsChecklist, ex.Message.Replace("'", ""));

                throw new Exception(obsChecklistAprovacaoLog);
            }
            
            //GRAVA LOG APROVAÇÃO
            var osChecklistAprovacaoLog = new Os_Checklist_Aprovacao_Log(osChecklistAprovacao, osChecklistAprovacaoDto.Matricula, obsChecklistAprovacaoLog, osChecklistAprovacaoDto.Aprovado ? 'A' : 'R', 'U');
                _osChecklistAprovacaoLogRepository.Inserir(osChecklistAprovacaoLog);
            
            return osChecklist.Id;
        }

        public OsChecklistPassageiroDto SalvarCheckListPassageiro(OsChecklistPassageiroDto osChecklistPassageiroDto)
        {
            var osChecklist = new Os_Checklist();
            var obsChecklistLog = string.Empty;
            var estaConcluido = false;
            var obsChecklistAprovacaoLog = string.Empty;
            var osChecklistAprovacao = new Os_Checklist_Aprovacao();

            //var numOS = Convert.ToInt32(_ordemServicoRepository.ObterPorId(osChecklistPassageiroDto.idOs).Numero) ;

            var CheckList = _osCheckListRepository.ObterOSCheckListPorIdOS(osChecklistPassageiroDto.idOs).Id;

            osChecklistPassageiroDto.idOsChecklist = CheckList;

            if (osChecklistPassageiroDto.idOsChecklist == 0)
            {
                osChecklist = new Os_Checklist(
                osChecklistPassageiroDto.idOsChecklist,
                osChecklistPassageiroDto.idOs,
                osChecklistPassageiroDto.Ida == true ? 1 : 0,
                osChecklistPassageiroDto.Volta == true ? 1 : 0,
                estaConcluido ? 1 : 0,
                osChecklistPassageiroDto.Matricula);

                obsChecklistLog = string.Format("Salvou Checklist para OS {0}", osChecklistPassageiroDto.idOs);

            }
            else
            {
                estaConcluido = _osChecklistAprovacaoRepository.Concluido(osChecklistPassageiroDto.idOsChecklist);

                try
                {
                    osChecklist = _osCheckListRepository.ObterPorId(osChecklistPassageiroDto.idOsChecklist);
                }
                catch (Exception ex)
                {

                    obsChecklistLog = string.Format("Erro ao buscar ao buscar Checklist {0}. Erro: {1}", osChecklistPassageiroDto.idOsChecklist, ex.Message.Replace("'", ""));

                    throw new Exception(obsChecklistLog);
                }

                osChecklist.Concluido = estaConcluido ? 1 : 0;
                osChecklist.Ida = osChecklistPassageiroDto.Ida ? 1 : 0;
                osChecklist.Volta = osChecklistPassageiroDto.Ida ? 0 : 1;

                obsChecklistLog = string.Format("Atualização do Checklist {0}", osChecklistPassageiroDto.idOsChecklist);

                try
                {
                    osChecklistAprovacao = _osChecklistAprovacaoRepository.ObterPorOsChecklist(osChecklistPassageiroDto.idOsChecklist);
                }
                catch (Exception ex)
                {
                    obsChecklistAprovacaoLog = string.Format("Erro ao buscar aprovações do Checklist {0}. Erro: {1}", osChecklistPassageiroDto.idOsChecklist, ex.Message.Replace("'", ""));

                    throw new Exception(obsChecklistAprovacaoLog);
                }
            }

            try
            {
                osChecklist = _osCheckListRepository.InserirOuAtualizar(osChecklist);
            }
            catch (Exception ex)
            {
                obsChecklistLog = string.Format("Erro ao inserir Checklist para os {0}. Erro: {1}", osChecklistPassageiroDto.idOs, ex.Message.Replace("'", ""));

                throw new Exception(obsChecklistLog);
            }
            
            //Grava LOG salvar CHECKLIST PASSAGEIRO
            var osChecklistLog = new Os_Checklist_Log(osChecklist, osChecklistPassageiroDto.Matricula, obsChecklistLog);
            _osCheckListLogRepository.Inserir(osChecklistLog);

            var osChecklistPassageiro = new Os_Checklist_Passageiro();
            if (osChecklistPassageiroDto.idOsChecklistPassageiro != 0)
            {
                osChecklistPassageiro = _osChecklistPassageiroRepository.ObterPorId(osChecklistPassageiroDto.idOsChecklistPassageiro);

            }

            osChecklistPassageiro.OsChecklist = osChecklist;
            //osChecklistPassageiro.Id = osChecklistPassageiroDto.idOsChecklistPassageiro;
            //Status
            osChecklistPassageiro.Status = 'A';
            // CCO
            osChecklistPassageiro.CcoSupervisorNome = osChecklistPassageiroDto.CCOtxtSupervisorCCO;
            osChecklistPassageiro.CcoSupervisorMatricula = osChecklistPassageiroDto.CCOtxtMatriculaSupervisorCCO;
            osChecklistPassageiro.CtrSupervisorNome = osChecklistPassageiroDto.CCOtxtControlador;
            osChecklistPassageiro.CtrSupervisorMatricula = osChecklistPassageiroDto.CCOtxtMatriculaControlador;
            // Equipagem
            osChecklistPassageiro.MaqEquipSupervisorNome = osChecklistPassageiroDto.EquiptxtMaquinista;
            osChecklistPassageiro.MaqEquipSupervisorMatricula = osChecklistPassageiroDto.EquiptxtMatriculaMaquinista;
            osChecklistPassageiro.AuxEquipSupervisorNome = osChecklistPassageiroDto.EquiptxtAuxiliar;
            osChecklistPassageiro.AuxEquipSupervisorMatricula = osChecklistPassageiroDto.EquiptxtMatriculaAuxiliar;
            osChecklistPassageiro.EquipSupervisorNome = osChecklistPassageiroDto.EquiptxtSupervisorEquipagem;
            osChecklistPassageiro.EquipSupervisorMatricula = osChecklistPassageiroDto.EquiptxtMatriculaSupervisorEquipagem;
            //Comentários adicionais
            osChecklistPassageiro.ComentAdicionais = osChecklistPassageiroDto.txtComentAdicionais != null ? StringUtil.RemoveAspas(osChecklistPassageiroDto.txtComentAdicionais) : string.Empty;
            // CheckList
            osChecklistPassageiro.Resposta1 = (osChecklistPassageiroDto.rdbListrdbTesteGradiente == true ? 'S' : osChecklistPassageiroDto.rdbListrdbTesteGradiente == false ? 'N' : 'X');
            osChecklistPassageiro.Resposta2 = (osChecklistPassageiroDto.rdbListrdbTesteVazamento == true ? 'S' : osChecklistPassageiroDto.rdbListrdbTesteVazamento == false ? 'N' : 'X');
            osChecklistPassageiro.Resposta3 = (osChecklistPassageiroDto.rdbListrdbTesteResposta == true ? 'S' : osChecklistPassageiroDto.rdbListrdbTesteResposta == false ? 'N' : 'X');
            osChecklistPassageiro.Resposta4 = (osChecklistPassageiroDto.rdbListrdbListaVagoes == true ? 'S' : osChecklistPassageiroDto.rdbListrdbListaVagoes == false ? 'N' : 'X');
            osChecklistPassageiro.Resposta5NumCruzamentos = osChecklistPassageiroDto.ListtxtNCruzamentosForaPO;
            osChecklistPassageiro.Resposta5Supervisor = osChecklistPassageiroDto.ListtxtSupervisorNCruzamentoForaPO;
            osChecklistPassageiro.Resposta5Matricula = osChecklistPassageiroDto.ListtxtMatriculaNCruzamentoForaPO;
            osChecklistPassageiro.Resposta6TotalCruzamentos = osChecklistPassageiroDto.ListtxtTotalCruzamentos;
            osChecklistPassageiro.Resposta7QtdePararam = osChecklistPassageiroDto.ListtxtQuantCruzamentosPararam;
            // Vagões
            osChecklistPassageiro.QtdeVagao = osChecklistPassageiroDto.VagoestxtQTDVagoes;
            osChecklistPassageiro.Tb = osChecklistPassageiroDto.VagoestxtTB;
            // Locomotivas
            //(1)
            osChecklistPassageiro.Locomotiva1 = osChecklistPassageiroDto.LocotxtLoco1;
            osChecklistPassageiro.Diesel1 = osChecklistPassageiroDto.LocotxtDiesel1;
            osChecklistPassageiro.DataVencimento1 = osChecklistPassageiroDto.LocodtVencimento1;
            osChecklistPassageiro.Cativas1 = (osChecklistPassageiroDto.LocordbCativas1 == true ? 'S' : osChecklistPassageiroDto.LocordbCativas1 == false ? 'N' : 'X');
            osChecklistPassageiro.Observacao1 = osChecklistPassageiroDto.LocotxtOBSCativas1;
            //(2)
            osChecklistPassageiro.Locomotiva2 = osChecklistPassageiroDto.LocotxtLoco2;
            osChecklistPassageiro.Diesel2 = osChecklistPassageiroDto.LocotxtDiesel2;
            osChecklistPassageiro.DataVencimento2 = osChecklistPassageiroDto.LocodtVencimento2;
            osChecklistPassageiro.Cativas2 = (osChecklistPassageiroDto.LocordbCativas2 == true ? 'S' : osChecklistPassageiroDto.LocordbCativas2 == false ? 'N' : 'X');
            osChecklistPassageiro.Observacao2 = osChecklistPassageiroDto.LocotxtOBSCativas2;
            //(3)
            osChecklistPassageiro.Locomotiva3 = osChecklistPassageiroDto.LocotxtLoco3;
            osChecklistPassageiro.Diesel3 = osChecklistPassageiroDto.LocotxtDiesel3;
            osChecklistPassageiro.DataVencimento3 = osChecklistPassageiroDto.LocodtVencimento3;
            osChecklistPassageiro.Cativas3 = (osChecklistPassageiroDto.LocordbCativas3 == true ? 'S' : osChecklistPassageiroDto.LocordbCativas3 == false ? 'N' : 'X');
            osChecklistPassageiro.Observacao3 = osChecklistPassageiroDto.LocotxtOBSCativas3;
            //(4)
            osChecklistPassageiro.Locomotiva4 = osChecklistPassageiroDto.LocotxtLoco4;
            osChecklistPassageiro.Diesel4 = osChecklistPassageiroDto.LocotxtDiesel4;
            osChecklistPassageiro.DataVencimento4 = osChecklistPassageiroDto.LocodtVencimento4;
            osChecklistPassageiro.Cativas4 = (osChecklistPassageiroDto.LocordbCativas4 == true ? 'S' : osChecklistPassageiroDto.LocordbCativas4 == false ? 'N' : 'X');
            osChecklistPassageiro.Observacao4 = osChecklistPassageiroDto.LocotxtOBSCativas4;
            // Previsão
            osChecklistPassageiro.DataChegada = osChecklistPassageiroDto.PrevdtHoraChegadaLocoOrigem;
            osChecklistPassageiro.DataPrevisto = osChecklistPassageiroDto.PrevtxtPrevisto;
            osChecklistPassageiro.DataReal = osChecklistPassageiroDto.PrevtxtReal;
            osChecklistPassageiro.DataChegadaReal = osChecklistPassageiroDto.DataChegadaReal;

            try
            {
                osChecklistPassageiro = _osChecklistPassageiroRepository.InserirOuAtualizar(osChecklistPassageiro);
            }
            catch (Exception ex)
            {
                obsChecklistLog = string.Format("Erro ao inserir Checklist para os {0}. Erro: {1}", osChecklistPassageiroDto.idOs, ex.Message.Replace("'", ""));

                throw new Exception(obsChecklistLog);
            }

            var osChecklistPassageiroLog = new Os_Checklist_Passageiro_Log(osChecklistPassageiro,
                                                                            osChecklistPassageiroDto.Matricula,
                                                                            obsChecklistLog);

            _osChecklistPassageiroLogRepository.Inserir(osChecklistPassageiroLog);

            osChecklistPassageiroDto.idOsChecklistPassageiro = osChecklistPassageiro.Id;
            //osChecklistPassageiroDto.numOs = numOS;

            return osChecklistPassageiroDto;
        }

        private Os_Checklist_Aprovacao SetarPropriedadesAprovacaoPorTipo(Os_Checklist_Aprovacao osChecklistAprovacao, string tipo, DateTime? dataHora, string usuario, string matricula, char status, char importado, string obs)
        {
            switch (tipo)
            {

                case "via":
                    {

                        osChecklistAprovacao.ViaDataAprovacao = dataHora != null ? dataHora : null;
                        osChecklistAprovacao.ViaUsuarioNome = usuario;
                        osChecklistAprovacao.ViaUsuarioMatricula = matricula;
                        osChecklistAprovacao.ViaStatus = status;
                        osChecklistAprovacao.ViaImportacao = importado;
                        osChecklistAprovacao.ViaObs = obs;
                        break;
                    }
                case "loco":
                    {
                        osChecklistAprovacao.LocoDataAprovacao = dataHora != null ? dataHora : null;
                        osChecklistAprovacao.LocoUsuarioNome = usuario;
                        osChecklistAprovacao.LocoUsuarioMatricula = matricula;
                        osChecklistAprovacao.LocoStatus = status;
                        osChecklistAprovacao.LocoImportacao = importado;
                        osChecklistAprovacao.LocoObs = obs;
                        break;
                    }
                case "tracao":
                    {
                        osChecklistAprovacao.TracDataAprovacao = dataHora != null ? dataHora : null;
                        osChecklistAprovacao.TracUsuarioNome = usuario;
                        osChecklistAprovacao.TracUsuarioMatricula = matricula;
                        osChecklistAprovacao.TracStatus = status;
                        osChecklistAprovacao.TracImportacao = importado;
                        osChecklistAprovacao.TracObs = obs;
                        break;
                    }
                case "vagao":
                    {
                        osChecklistAprovacao.VagDataAprovacao = dataHora != null ? dataHora : null;
                        osChecklistAprovacao.VagUsuarioNome = usuario;
                        osChecklistAprovacao.VagUsuarioMatricula = matricula;
                        osChecklistAprovacao.VagStatus = status;
                        osChecklistAprovacao.VagImportacao = importado;
                        osChecklistAprovacao.VagObs = obs;
                        break;
                    }
            }
            return osChecklistAprovacao;
        }

        public int BuscarNumOs(int idOs)
        {

            if (idOs > 0)
            {
                var ordemServico = _ordemServicoRepository.ObterPorId(idOs);
                if (ordemServico.Numero.HasValue)
                    return ordemServico.Numero.Value;
                else
                    return 0;
            }

            return 0;
        }

        public OsChecklistAprovadaDto BuscarDadosAprovados(int idOsChecklist)
        {
            var aprovacoes = _osChecklistAprovacaoRepository.ObterPorOsChecklist(idOsChecklist);

            var osAprovadaDto = new OsChecklistAprovadaDto();

            if (aprovacoes != null)
            {
                osAprovadaDto.idOs = aprovacoes.OsChecklist.IdOs;
                osAprovadaDto.idOsChecklist = aprovacoes.OsChecklist.Id;
                osAprovadaDto.idOsChecklistAprovacao = aprovacoes.Id;

                ///Propriedades da via
                osAprovadaDto.ViaUsuarioMatricula = aprovacoes.ViaUsuarioMatricula;
                osAprovadaDto.ViaUsuarioNome = aprovacoes.ViaUsuarioNome;
                osAprovadaDto.ViaDataAprovacao = aprovacoes.ViaDataAprovacao;
                osAprovadaDto.ViaStatus = aprovacoes.ViaStatus;
                osAprovadaDto.ViaImportacao = aprovacoes.ViaImportacao;
                osAprovadaDto.ViaObs = !string.IsNullOrEmpty(aprovacoes.ViaObs) ? StringUtil.RemoveAspas(aprovacoes.ViaObs.Replace("\n1.\t", ".").Replace("\n2.\t", ".").Replace("\n3.\t", ".").Replace("\n", "").Replace(@"\x", "").Replace("\r", " ")) : aprovacoes.ViaObs;

                //// Propriedades de aprovações da área Locoomitiva
                osAprovadaDto.LocoUsuarioMatricula = aprovacoes.LocoUsuarioMatricula;
                osAprovadaDto.LocoUsuarioNome = aprovacoes.LocoUsuarioNome;
                osAprovadaDto.LocoDataAprovacao = aprovacoes.LocoDataAprovacao;
                osAprovadaDto.LocoStatus = aprovacoes.LocoStatus;
                osAprovadaDto.LocoImportacao = aprovacoes.LocoImportacao;
                osAprovadaDto.LocoObs = !string.IsNullOrEmpty(aprovacoes.LocoObs) ? StringUtil.RemoveAspas(aprovacoes.LocoObs.Replace("\n1.\t", ".").Replace("\n2.\t", ".").Replace("\n3.\t", ".").Replace("\n", "").Replace(@"\x", "").Replace("\r", " ")) : aprovacoes.LocoObs;

                //// Propriedades de aprovações da área Tração
                osAprovadaDto.TracUsuarioMatricula = aprovacoes.TracUsuarioMatricula;
                osAprovadaDto.TracUsuarioNome = aprovacoes.TracUsuarioNome;
                osAprovadaDto.TracDataAprovacao = aprovacoes.TracDataAprovacao;
                osAprovadaDto.TracStatus = aprovacoes.TracStatus;
                osAprovadaDto.TracImportacao = aprovacoes.TracImportacao;
                osAprovadaDto.TracObs = !string.IsNullOrEmpty(aprovacoes.TracObs) ? StringUtil.RemoveAspas(aprovacoes.TracObs.Replace("\n1.\t", ".").Replace("\n2.\t", ".").Replace("\n3.\t", ".").Replace("\n", "").Replace(@"\x", "").Replace("\r", " ")) : aprovacoes.TracObs;

                //// Propriedades de aprovações da área Vagão
                osAprovadaDto.VagUsuarioMatricula = aprovacoes.VagUsuarioMatricula;
                osAprovadaDto.VagUsuarioNome = aprovacoes.VagUsuarioNome;
                osAprovadaDto.VagDataAprovacao = aprovacoes.VagDataAprovacao;
                osAprovadaDto.VagStatus = aprovacoes.VagStatus;
                osAprovadaDto.VagImportacao = aprovacoes.VagImportacao;
                osAprovadaDto.VagObs = !string.IsNullOrEmpty(aprovacoes.VagObs) ? StringUtil.RemoveAspas(aprovacoes.VagObs.Replace("\n1.\t", ".").Replace("\n2.\t", ".").Replace("\n3.\t", ".").Replace("\n", "").Replace(@"\x", "").Replace("\r", " ")) : aprovacoes.VagObs;
            }
            return osAprovadaDto;
        }

        public OsChecklistPermissaoDto BuscarPermissaoUsuario(Usuario usuario)
        {
            var permissao = new OsChecklistPermissaoDto();
            var acao = "CHECKLISTPASS";

            permissao.AlteraStatusCheckList = ServiceLocator.Current.GetInstance<ControleAcessoService>().Autorizar(acao, "AlteraStatusCheckList", usuario);
            permissao.Pesquisar = ServiceLocator.Current.GetInstance<ControleAcessoService>().Autorizar(acao, "Pesquisar", usuario);
            permissao.Gerar = ServiceLocator.Current.GetInstance<ControleAcessoService>().Autorizar(acao, "Gerar", usuario);
            permissao.Visualizar = ServiceLocator.Current.GetInstance<ControleAcessoService>().Autorizar(acao, "Visualizar", usuario);
            permissao.Importar = ServiceLocator.Current.GetInstance<ControleAcessoService>().Autorizar(acao, "Importar", usuario);
            permissao.Exportar = ServiceLocator.Current.GetInstance<ControleAcessoService>().Autorizar(acao, "Exportar", usuario);
            permissao.SalvarChecklist = ServiceLocator.Current.GetInstance<ControleAcessoService>().Autorizar(acao, "SalvarChecklist", usuario);
            permissao.ConcluirChecklist = ServiceLocator.Current.GetInstance<ControleAcessoService>().Autorizar(acao, "ConcluirChecklist", usuario);
            permissao.EditarChecklist = ServiceLocator.Current.GetInstance<ControleAcessoService>().Autorizar(acao, "EditarChecklist", usuario);

            permissao.AprovarLoco = ServiceLocator.Current.GetInstance<ControleAcessoService>().Autorizar(acao, "AprovarLoco", usuario);
            permissao.ReprovarLoco = ServiceLocator.Current.GetInstance<ControleAcessoService>().Autorizar(acao, "ReprovarLoco", usuario);

            permissao.AprovarTracao = ServiceLocator.Current.GetInstance<ControleAcessoService>().Autorizar(acao, "AprovarTracao", usuario);
            permissao.ReprovarTracao = ServiceLocator.Current.GetInstance<ControleAcessoService>().Autorizar(acao, "ReprovarTracao", usuario);

            permissao.AprovarVagao = ServiceLocator.Current.GetInstance<ControleAcessoService>().Autorizar(acao, "AprovarVagao", usuario);
            permissao.ReprovarVagao = ServiceLocator.Current.GetInstance<ControleAcessoService>().Autorizar(acao, "ReprovarVagao", usuario);

            permissao.AprovarVia = ServiceLocator.Current.GetInstance<ControleAcessoService>().Autorizar(acao, "AprovarVia", usuario);
            permissao.ReprovarVia = ServiceLocator.Current.GetInstance<ControleAcessoService>().Autorizar(acao, "ReprovarVia", usuario);

            return permissao;
        }

        public void Importar(int idOs, int idOsChecklist, string area, bool aprovado, Usuario usuario, string nomeArquivo, byte[] arquivo)
        {
            var osChecklist = new Os_Checklist();
            var obsChecklistLog = string.Empty;
            var estaConcluido = false;
            var obsChecklistAprovacaoLog = string.Empty;
            var osChecklistAprovacao = new Os_Checklist_Aprovacao();

            var CheckList = _osCheckListRepository.ObterOSCheckListPorIdOS(idOs).Id;

            idOsChecklist = CheckList;

            var novoNomeArquivo = nomeArquivo.Split('\\');
            var nomeDoArquivo = novoNomeArquivo.GetValue(novoNomeArquivo.Length - 1).ToString();

            /*x.GetValue(x.Length-1);"OS1735513_vag.msg"*/

            if (idOsChecklist == 0)
            {
                osChecklist = new Os_Checklist(0,
                idOs,
                1, 0, 0, usuario.Codigo);

                obsChecklistLog = string.Format("Novo Checklist para OS {0}", idOs);
                obsChecklistAprovacaoLog = string.Format("Nova aprovação de {0} para OS {1}", area, idOs);
            }
            else
            {
                estaConcluido = _osChecklistAprovacaoRepository.Concluido(idOsChecklist);

                try
                {
                    osChecklist = _osCheckListRepository.ObterPorId(idOsChecklist);
                }
                catch (Exception ex)
                {

                    obsChecklistLog = string.Format("Erro ao buscar ao buscar Checklist {0}. Erro: {1}", idOsChecklist, ex.Message.Replace("'", ""));

                    throw new Exception(obsChecklistLog);
                }

                osChecklist.Concluido = estaConcluido ? 1 : 0;
                osChecklist.Ida = 1;
                osChecklist.Volta = 0;

                obsChecklistLog = string.Format("Atualização do Checklist {0} para OS {1}", idOsChecklist, idOs);
                obsChecklistAprovacaoLog = string.Format("Atualização de aprovação de {0} para OS {1}. Status: {2}", area, idOs, aprovado ? "Aprovado" : "Reprovado");

                try
                {
                    osChecklistAprovacao = _osChecklistAprovacaoRepository.ObterPorOsChecklist(idOsChecklist);
                }
                catch (Exception ex)
                {
                    obsChecklistAprovacaoLog = string.Format("Erro ao buscar aprovações do Checklist {0}. Erro: {1}", idOsChecklist, ex.Message.Replace("'", ""));

                    throw new Exception(obsChecklistAprovacaoLog);
                }
            }

            try
            {
                osChecklist = _osCheckListRepository.InserirOuAtualizar(osChecklist);
            }
            catch (Exception ex)
            {
                obsChecklistLog = string.Format("Erro ao inserir Checklist para os {0}. Erro: {1}", idOs, ex.Message.Replace("'", ""));

                throw new Exception(obsChecklistLog);
            }

            var osChecklistLog = new Os_Checklist_Log(osChecklist, usuario.Codigo, obsChecklistLog);
            _osCheckListLogRepository.Inserir(osChecklistLog);


            if (area.ToLower() == "via")
            {
                osChecklistAprovacao.ViaDataAprovacao = DateTime.Now;
                osChecklistAprovacao.ViaImportacao = 'I';
                osChecklistAprovacao.ViaStatus = aprovado ? 'A' : 'R';
                osChecklistAprovacao.ViaUsuarioMatricula = usuario.Codigo;
                osChecklistAprovacao.ViaUsuarioNome = usuario.Nome;
            }

            if (area.Contains("Loc"))
            {
                area = "loc";
                osChecklistAprovacao.LocoDataAprovacao = DateTime.Now;
                osChecklistAprovacao.LocoImportacao = 'I';
                osChecklistAprovacao.LocoStatus = aprovado ? 'A' : 'R';
                osChecklistAprovacao.LocoUsuarioMatricula = usuario.Codigo;
                osChecklistAprovacao.LocoUsuarioNome = usuario.Nome;
            }

            if (area.Contains("Tra"))
            {
                area = "tra";
                osChecklistAprovacao.TracDataAprovacao = DateTime.Now;
                osChecklistAprovacao.TracImportacao = 'I';
                osChecklistAprovacao.TracStatus = aprovado ? 'A' : 'R';
                osChecklistAprovacao.TracUsuarioMatricula = usuario.Codigo;
                osChecklistAprovacao.TracUsuarioNome = usuario.Nome;
            }

            if (area.Contains("Vag"))
            {
                area = "vag";
                osChecklistAprovacao.VagDataAprovacao = DateTime.Now;
                osChecklistAprovacao.VagImportacao = 'I';
                osChecklistAprovacao.VagStatus = aprovado ? 'A' : 'R';
                osChecklistAprovacao.VagUsuarioMatricula = usuario.Codigo;
                osChecklistAprovacao.VagUsuarioNome = usuario.Nome;
            }

            osChecklistAprovacao.OsChecklist = osChecklist;

            try
            {
                osChecklistAprovacao = _osChecklistAprovacaoRepository.InserirOuAtualizar(osChecklistAprovacao);

                obsChecklistAprovacaoLog = string.Format("{0} para área {1} por importação.", aprovado ? "Aprovação" : "Reprovação", area);

                var osChecklistAprovacaoLog = new Os_Checklist_Aprovacao_Log(osChecklistAprovacao, usuario.Codigo, obsChecklistAprovacaoLog, aprovado ? 'A' : 'R', 'U');
                _osChecklistAprovacaoLogRepository.Inserir(osChecklistAprovacaoLog);
            }
            catch (Exception ex)
            {
                obsChecklistAprovacaoLog = string.Format("Erro ao inserir ou atualizar aprovações do Checklist {0}. Erro: {1}", idOsChecklist, ex.Message.Replace("'", ""));

                throw new Exception(obsChecklistAprovacaoLog);
            }

            var osChecklistImportacao = _osChecklistImportacaoRepository.ObterPorIdEArea(osChecklistAprovacao.Id, area);
            if (osChecklistImportacao.Id == 0)
                osChecklistImportacao = new Os_Checklist_Importacao(osChecklistAprovacao, area, nomeDoArquivo, arquivo);

            try
            {
                _osChecklistImportacaoRepository.InserirOuAtualizar(osChecklistImportacao);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Os_Checklist_Passageiro ObterOSPassageiro(int idOs)
        {
            var osChecklistPassageiroDto = _osChecklistPassageiroRepository.ObterPorId(idOs);

            return osChecklistPassageiroDto;
        }

        public Os_Checklist_Passageiro ObterOSPassageiroPorIDCheckList(int idOsCheckList)
        {
            int totalLoco = 0;
            var osChecklistPassageiroDto = _osChecklistPassageiroRepository.ObterOSPassageiroPorIDCheckList(idOsCheckList);

            if (osChecklistPassageiroDto != null)
            {
                if (!string.IsNullOrWhiteSpace(osChecklistPassageiroDto.Locomotiva1))
                {
                    totalLoco++;
                }
                if (!string.IsNullOrWhiteSpace(osChecklistPassageiroDto.Locomotiva2))
                {
                    totalLoco++;
                }

                if (!string.IsNullOrWhiteSpace(osChecklistPassageiroDto.Locomotiva3))
                {
                    totalLoco++;
                }

                if (!string.IsNullOrWhiteSpace(osChecklistPassageiroDto.Locomotiva4))
                {
                    totalLoco++;
                }
                osChecklistPassageiroDto.TotalLoco = totalLoco;
            }
            return osChecklistPassageiroDto;
        }

        public Os_Checklist ObterOSCheckListPassageiroPorIDCheckList(int idOsCheckList)
        {
            var osCheckListDto = _osCheckListRepository.ObterPorId(idOsCheckList);

            return osCheckListDto;
        }

        public Os_Checklist_Passageiro ObterOSPassageiroCheckList(int idOsCheckListPassageiro)
        {
            var osCheckListPassageiro = _osChecklistPassageiroRepository.ObterOSPassageiroCheckList(idOsCheckListPassageiro);

            return osCheckListPassageiro;
        }

        public Os_Checklist ObterOSCheckListPorIdOS(int idOs)
        {
            var osCheckListDto = _osCheckListRepository.ObterOSCheckListPorIdOS(idOs);

            return osCheckListDto;
        }

        public void ConcluirCheckListPassageiro(OsChecklistPassageiroDto osChecklistPassageiroDto)
        {
            var osChecklistPassageiroLog = new Os_Checklist_Passageiro_Log();
            var obsChecklistPassageiroLog = string.Empty;
            var osChecklistPassageiro = _osChecklistPassageiroRepository.ObterPorId(osChecklistPassageiroDto.idOsChecklistPassageiro);

            try
            {
                //Altera status checklist passageiro para concluido
                osChecklistPassageiro.Status = 'C';
                _osChecklistPassageiroRepository.Atualizar(osChecklistPassageiro);

                //Historico alteração para concluido
                obsChecklistPassageiroLog = string.Format("Mudança de status para Concluido do checklistpassageiro {0}", osChecklistPassageiroDto.idOsChecklistPassageiro);
                //obsChecklistLog = string.Format("Atualização do Checklist {0} para OS {1}", idOsChecklist, idOs);
                //Popula objeto check list passageiro log
                osChecklistPassageiroLog.OsChecklistPassageiro = osChecklistPassageiro;
                osChecklistPassageiroLog.Observacao = obsChecklistPassageiroLog;
                osChecklistPassageiroLog.Usuario = osChecklistPassageiroDto.Matricula;
                //Efetua inclusão tabela check list passageiro LOG
                _osChecklistPassageiroLogRepository.Inserir(osChecklistPassageiroLog);

            }
            catch (Exception ex)
            {
                throw new Exception();
            }
        }

        public OsCheckListImportacaoDto ObterArquivo(int idCheckListAprovado, string quadro)
        {
            return _osChecklistImportacaoRepository.ObterArquivo(idCheckListAprovado, quadro);
        }

        public OsDadosTremDto BuscaDadosTremCheckList(int idOs)
        {
            var dadosTremDto = _osChecklistPassageiroRepository.BuscaDadosTremPorIdOs(idOs);
            var osLocomotivaDto = _osChecklistPassageiroRepository.BuscaLocomotivasTremPorIdOs(idOs);
            
            if (dadosTremDto != null)
            {
                dadosTremDto.listLocomotiva = osLocomotivaDto != null ? osLocomotivaDto : new List<LocomotivaDto>();
            }

            return dadosTremDto;
        }

        public bool ValidaDataValida(DateTime data)
        {
            DateTime dataValida;
            if (DateTime.TryParse(data.ToString(), out dataValida))
            {
                //Se a data for valida
                return true;
            }
            else
            {
                //Se a data for invalida
                return false;
            }
        }

        public OsChecklistExportacaoDto ObterConsultaExportacaoPDF(string idOs)
        {
            var osChecklistExportacaoDto = _osCheckListRepository.ObterConsultaExportacaoPDF(idOs);

            osChecklistExportacaoDto.ComentAdicionais = osChecklistExportacaoDto.ComentAdicionais = !string.IsNullOrEmpty(osChecklistExportacaoDto.ComentAdicionais) ? StringUtil.RemoveAspas(osChecklistExportacaoDto.ComentAdicionais.Replace("\n1.\t", ".").Replace("\n2.\t", ".").Replace("\n3.\t", ".").Replace("\n", "").Replace(@"\x", "").Replace("\r", " ")) : osChecklistExportacaoDto.ComentAdicionais;

            return osChecklistExportacaoDto;
        }


        public IList<OsCheckListImportacaoDto> ObterImportacaoPorNumOS(int numOs)
        {
            return _osChecklistImportacaoRepository.ObterImportacaoPorNumOS(numOs);
        }


        public IList<LocomotivaDto> BuscaLocomotivasTremPorIdOs(int idOs)
        {
            return _osChecklistPassageiroRepository.BuscaLocomotivasTremPorIdOs(idOs);
        }
    }
}