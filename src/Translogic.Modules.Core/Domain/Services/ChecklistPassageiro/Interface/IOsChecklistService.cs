﻿
namespace Translogic.Modules.Core.Domain.Services.ChecklistPassageiro.Interface
{
    using ALL.Core.Dominio;
    using Translogic.Modules.Core.Domain.Model.Dto.ChecklistPassageiro;
    using Translogic.Core.Infrastructure.Web;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Modules.Core.Domain.Model.Acesso;
    using Translogic.Modules.Core.Domain.Model.ChecklistPassageiros;
    using System.Collections.Generic;

    public interface IOsChecklistService
    {
        /// <summary>
        /// Pesquisar OS de trens com prefixo P comparado com Checklist de passageiros
        /// </summary>
        /// <param name="detalhesPaginacaoWeb">Paginação</param>
        /// <param name="dataInicial">Filtro data inicial</param>
        /// <param name="dataFinal">Filtro data final</param>
        /// <param name="numOs">Filtro OS</param>
        /// <returns>Lista de dados para GRID</returns>
        ResultadoPaginado<OsChecklistDto> ObterConsultaOsCheckList(DetalhesPaginacaoWeb detalhesPaginacaoWeb, string dataInicial, string dataFinal, string numOs);

        /// <summary>
        /// Salvar aprovações do checklist
        /// </summary>
        /// <param name="entity"></param>
        int SalvarAprovacoes(OsChecklistAprovacaoDto osChecklistAprovacaoDto);

        /// <summary>
        /// Salvar CheckList do Passageiro
        /// </summary>
        /// <param name="entity"></param>
        OsChecklistPassageiroDto SalvarCheckListPassageiro(OsChecklistPassageiroDto osChecklistPassageiroDto);

        /// <summary>
        /// Buscar número da OS pelo ID
        /// </summary>
        /// <param name="idOs">ID da OS</param>
        /// <returns></returns>
        int BuscarNumOs(int idOs);

        /// <summary>
        /// Buscar as aprovações existentes de uma OsChecklist
        /// </summary>
        /// <param name="idOsChecklist">ID OsChecklist</param>
        /// <returns></returns>
        OsChecklistAprovadaDto BuscarDadosAprovados(int idOsChecklist);

        /// <summary>
        /// Buscar permissões de usuário para aprovações
        /// </summary>
        /// <param name="usuario"></param>
        /// <returns></returns>
        OsChecklistPermissaoDto BuscarPermissaoUsuario(Usuario usuario);

        /// <summary>
        /// Realizar o salvamento da importação
        /// </summary>
        /// <param name="idOs"></param>
        /// <param name="idOsChecklist"></param>
        /// <param name="area"></param>
        /// <param name="aprovado"></param>
        /// <param name="usuario"></param>
        /// <param name="nomeArquivo"></param>
        /// <param name="arquivo"></param>
        void Importar(int idOs, int idOsChecklist, string area, bool aprovado, Usuario usuario, string nomeArquivo, byte[] arquivo);

        /// <summary>
        /// Buscar OS CheckList Passageiro pelo ID
        /// </summary>
        /// <param name="idOs"></param>
        /// <returns> Retorna OBJETO CheckList Passageiro</returns>
        Os_Checklist_Passageiro ObterOSPassageiro(int idOs);

        /// <summary>
        /// Buscar OS CheckList Passageiro pelo id DO CHECKLIST
        /// </summary>
        /// <param name="idOs"></param>
        /// <returns> Retorna OBJETO CheckList Passageiro</returns>
        Os_Checklist_Passageiro ObterOSPassageiroPorIDCheckList(int idOsCheckList);

        /// <summary>
        /// Buscar OS_CHECKLIST_PASSAGEIRO pelo id DO CHECKLIST_PASSAGEIRO
        /// </summary>
        /// <param name="idOs"></param>
        /// <returns> Retorna OBJETO CheckList Passageiro</returns>
        Os_Checklist_Passageiro ObterOSPassageiroCheckList(int idOsCheckListPassageiro);

        /// <summary>
        /// Buscar OS_CHECKLIST pelo id DA OS
        /// </summary>
        /// <param name="idOs"></param>
        /// <returns></returns>
        Os_Checklist ObterOSCheckListPorIdOS(int idOs);

        /// <summary>
        /// Conclui CheckList
        /// </summary>
        /// <param name="idOsChecklistPassageiro"></param>
        /// <returns></returns>
        void ConcluirCheckListPassageiro(OsChecklistPassageiroDto osChecklistPassageiroDto);
        /// <summary>
        /// Busca dados trem Quantidade de vagões, TB e lista de Locomotivas do trem
        /// </summary>
        /// <param name="idOs"></param>
        /// <returns>QTD Vagões, TB e Lista Locomotivas do trem</returns>
        OsDadosTremDto BuscaDadosTremCheckList(int idOs);
        /// <summary>
        /// Obeter Arquivo de importação
        /// </summary>
        /// <param name="idOsChecklistPassageiro"></param>
        /// <returns></returns>
        OsCheckListImportacaoDto ObterArquivo(int idCheckListAprovado, string quadro);

        /// <summary>
        /// Pesquisa dados trem por OS para exporatação PDF
        /// </summary>
        /// <param name="idOs">Filtro OS</param>
        /// <returns>Lista de dados para Exportação PDF</returns>
        OsChecklistExportacaoDto ObterConsultaExportacaoPDF(string idOs);

        /// <summary>
        /// Obter Importação por numero da OS
        /// </summary>
        /// <param name="idCheckListAprovado"></param>
        /// <param name="quadro"></param>
        /// <returns></returns>
        IList<OsCheckListImportacaoDto> ObterImportacaoPorNumOS(int numOs);

        /// <summary>
        /// Busca Locomotivas do Trem pelo id da OS da (tabela T2)
        /// </summary>
        /// <param name="idOs"></param>
        /// <returns>Lista de Locomotivas</returns>
        IList<LocomotivaDto> BuscaLocomotivasTremPorIdOs(int idOs);
    }
}