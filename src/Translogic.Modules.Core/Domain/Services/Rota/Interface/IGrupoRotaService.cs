﻿namespace Translogic.Modules.Core.Domain.Services.Rota.Interface
{
    using ALL.Core.Dominio;
    using System.Collections.Generic;
    using Translogic.Core.Infrastructure.Web;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais;
    using Translogic.Modules.Core.Domain.Model.Rota;

    public interface IGrupoRotaService
    {

        ResultadoPaginado<GrupoRotaDto> ObterConsultaGrupoRota(DetalhesPaginacaoWeb detalhesPaginacaoWeb, string dataInicial, string dataFinal);

        IList<GrupoRotaDto> ObterGrupoRotas();
        IList<RotaGrupoRotaDto> ObterRotas();
        GrupoRota Obter(int id);

        void Salvar(GrupoRota grupoRota);

        void Excluir(int id);

        GrupoRota ObterInstancia(int id);

        ResultadoPaginado<GrupoRotaRotaDto>  ObterAssociacoes(DetalhesPaginacaoWeb detalhesPaginacaoWeb, string dataInicial, string dataFinal, string grupoRota, string origem, string destino, string rota);
        GrupoRotaRota ObterInstanciaAssociar(GrupoRotaRotaDto model);
        void SalvarAssociacao(GrupoRotaRota grupoRotaRota);
        void ExcluirAssociar(int id);

        IList<Mercadoria> ListMercadorias();
        IList<FrotaDto> ObtemFrotas();
        ResultadoPaginado<LimiteDto> ObterLimites(DetalhesPaginacaoWeb detalhesPaginacaoWeb, string dataInicial, string dataFinal, string idGrupoRota, string frota, string idMercadoria);
        void SalvarLimite(Limite limite);
        void ExcluirLimite(Limite limite);
        Limite ObterLimite(int id);
    }
}
