﻿
namespace Translogic.Modules.Core.Domain.Services.Rota
{
    using ALL.Core.Dominio;
    using Org.BouncyCastle.Asn1.Cmp;
    using System;
    using System.Collections.Generic;
    using Translogic.Core.Infrastructure.Web;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais;
    using Translogic.Modules.Core.Domain.Model.Rota;
    using Translogic.Modules.Core.Domain.Model.Rota.Repositories;
    using Translogic.Modules.Core.Domain.Services.Rota.Interface;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Repositories;

    public class GrupoRotaService : IGrupoRotaService
    {
        #region Repositórios
        private readonly IGrupoRotaRepository _grupoRotaRepository;
        private readonly IGrupoRotaRotaRepository _grupoRotaRotaRepository;
        private readonly IMercadoriaRepository _mercadoriaRepository;
        private readonly IGrupoRotaLimiteRepository _grupoRotaLimiteRepository;
        #endregion

        #region Construtores
        public GrupoRotaService(
            IGrupoRotaRepository grupoRotaRepository,
            IGrupoRotaRotaRepository grupoRotaRotaRepository, 
            IMercadoriaRepository mercadoriaRepository,
            IGrupoRotaLimiteRepository grupoRotaLimiteRepository)
       
        {
            _grupoRotaRepository = grupoRotaRepository;
            _grupoRotaRotaRepository = grupoRotaRotaRepository;
             _mercadoriaRepository = mercadoriaRepository;
            _grupoRotaLimiteRepository = grupoRotaLimiteRepository;
        }
        #endregion

        #region Métodos

        #region Grupo
        public ResultadoPaginado<GrupoRotaDto> ObterConsultaGrupoRota(DetalhesPaginacaoWeb detalhesPaginacaoWeb, string dataInicial, string dataFinal)
        {
            DateTime? dtInicial = null;
            DateTime? dtFinal = null;

            if (!string.IsNullOrEmpty(dataInicial) && dataInicial != "__/__/____")
            {
                dtInicial = Convert.ToDateTime(dataInicial);
            }

            if (!string.IsNullOrEmpty(dataFinal) && dataInicial != "__/__/____")
            {
                dtFinal = Convert.ToDateTime(dataFinal);
            }


            return _grupoRotaRepository.ObterConsulta(detalhesPaginacaoWeb, dtInicial, dtFinal);
        }

        public IList<GrupoRotaDto> ObterGrupoRotas()
        {
            return _grupoRotaRepository.ObterGrupoRotas();
        }

        public IList<RotaGrupoRotaDto> ObterRotas()
        {
            return _grupoRotaRepository.ObterRotas();
        }

        public GrupoRota Obter(int id)
        {
            return _grupoRotaRepository.ObterPorId(id);
        }

        public void Salvar(GrupoRota grupoRota)
        {
            _grupoRotaRepository.Salvar(grupoRota);
        }

        public void Excluir(int id)
        {
            var grupoRota = this.Obter(id);
            _grupoRotaRepository.Excluir(grupoRota);
        }

        public GrupoRota ObterInstancia(int id)
        {
            if (id > 0)
                return Obter(id);
            else
            {
                return new GrupoRota() { };
            }
        }
        #endregion

        #region Associacao Grupo Rota
        public ResultadoPaginado<GrupoRotaRotaDto> ObterAssociacoes(DetalhesPaginacaoWeb detalhesPaginacaoWeb, string dataInicial, string dataFinal, string grupoRota, string origem, string destino, string rota)
        {
            DateTime? dtInicial = null;
            DateTime? dtFinal = null;

            if (!string.IsNullOrEmpty(dataInicial) && dataInicial != "__/__/____")
            {
                dtInicial = Convert.ToDateTime(dataInicial);
            }

            if (!string.IsNullOrEmpty(dataFinal) && dataInicial != "__/__/____")
            {
                dtFinal = Convert.ToDateTime(dataFinal);
            }

            return _grupoRotaRotaRepository.ObterAssociacoes(detalhesPaginacaoWeb, dtInicial, dtFinal, grupoRota, origem, destino, rota);
        }


        public void SalvarAssociacao(GrupoRotaRota grupoRotaRota)
        {
            _grupoRotaRotaRepository.Salvar(grupoRotaRota);
        }
        public GrupoRotaRota ObterAssociar(int id)
        {
            return _grupoRotaRotaRepository.ObterPorId(id);
        }
        public GrupoRotaRota ObterInstanciaAssociar(GrupoRotaRotaDto model)
        {
            var instancia = new GrupoRotaRota();
            if (model.IdGrupoRotaRota > 0)
                instancia =  ObterAssociar(Convert.ToInt32(model.IdGrupoRotaRota));

            instancia.IdGrupo = Convert.ToInt32(model.IdGrupo);
            instancia.IdRota = Convert.ToInt32(model.IdRota);

            return instancia;
        }
        public void ExcluirAssociar(int id)
        {
            var grupoRotaRota = this.ObterAssociar(id);
            _grupoRotaRotaRepository.Excluir(grupoRotaRota);
        }
        #endregion

        #region Limites
        public IList<Mercadoria> ListMercadorias()
        {
            return _mercadoriaRepository.ObterTodasMercadorias();
        }

        public IList<FrotaDto> ObtemFrotas()
        {
            return _grupoRotaLimiteRepository.ObterFrotas();
        }

        public ResultadoPaginado<LimiteDto> ObterLimites(DetalhesPaginacaoWeb detalhesPaginacaoWeb, string dataInicial, string dataFinal, string idGrupoRota, string frota, string idMercadoria)
        {
            DateTime? dtInicial = null;
            DateTime? dtFinal = null; 

            if (!string.IsNullOrEmpty(dataInicial) && dataInicial != "__/__/____")
            {
                dtInicial = Convert.ToDateTime(dataInicial);
            }

            if (!string.IsNullOrEmpty(dataFinal) && dataInicial != "__/__/____")
            {
                dtFinal = Convert.ToDateTime(dataFinal);
            }

            return _grupoRotaLimiteRepository.ObterConsulta(detalhesPaginacaoWeb, dtInicial, dtFinal, idGrupoRota, frota, idMercadoria);
        }

        public void SalvarLimite(Limite limite)
        {
            _grupoRotaLimiteRepository.Salvar(limite);
        }
        public void ExcluirLimite(Limite limite)
        {
            _grupoRotaLimiteRepository.Excluir(limite);
        }

        public Limite ObterLimite(int id)
        {
            return _grupoRotaLimiteRepository.ObterLimite(id);
        }
        #endregion
        #endregion
    }
}