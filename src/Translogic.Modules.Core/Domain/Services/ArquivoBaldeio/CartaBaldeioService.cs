﻿namespace Translogic.Modules.Core.Domain.Services.ArquivoBaldeio
{
    using System;
    using Translogic.Modules.Core.Domain.Services.ArquivoBaldeio.Interface;
    using Translogic.Modules.Core.Domain.Model.ArquivoBaldeio.Repositorio;
    using System.Web.Mvc;
    using System.IO;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Repositories;
    using Translogic.Modules.Core.Domain.Model.ArquivoBaldeio;
    using Translogic.Modules.Core.Domain.Model.Acesso;
    using Microsoft.Practices.ServiceLocation;
    using Translogic.Modules.Core.Domain.Services.Acesso;

    public class CartaBaldeioService : ICartaBaldeioService
    {
        private readonly ICartaBaldeioRepository _arquivoBaldeioRepository;
        private readonly IBaldeioRepository _baldeioRepository;

        public CartaBaldeioService(ICartaBaldeioRepository arquivoBaldeioRepository, IBaldeioRepository baldeioRepository)
        {
            _arquivoBaldeioRepository = arquivoBaldeioRepository;
            _baldeioRepository = baldeioRepository;
        }

        public FileStreamResult ObterArquivoBaldeio(int idArquivoBaldeio)
        {
            var arquivoBaldeio = _arquivoBaldeioRepository.ObterArquivoBaldeio(idArquivoBaldeio);
            FileStreamResult fsr = null;

            if (arquivoBaldeio == null)
                return fsr;
            else
            {
                MemoryStream ms = new MemoryStream(arquivoBaldeio.ArquivoPdf);
                if (ms != null)
                {
                    fsr = new FileStreamResult(ms, "application/pdf");
                    fsr.FileDownloadName = string.Format("{0}.pdf", arquivoBaldeio.NomeArquivo);
                }
            }
            return fsr;
        }

        public void AnexarCarta(int idBaldeio, byte[] arquivo, string nomeArquivo, string usuario)
        {
            if (idBaldeio > 0)
            {
                var baldeio = _baldeioRepository.ObterPorId(idBaldeio);
                if (baldeio == null)
                {
                    throw new Exception("Baldeio não encontrado");
                }
                else
                {
                    if (arquivo == null)
                    {
                        throw new Exception("Arquivo do Baldeio não fornecido");
                    }

                    if (ExistePorIdBaldeio(idBaldeio))
                    {
                        throw new Exception("Já existe carta para este baldeio");
                    }

                    var cartaBaldeio = new CartaBaldeio()
                    {
                        Baldeio = baldeio,
                        ArquivoPdf = arquivo,
                        DataAtualizacao = DateTime.Now,
                        NomeArquivo = nomeArquivo,
                        Usuario = usuario,
                        VersionDate = DateTime.Now
                    };

                    _arquivoBaldeioRepository.Inserir(cartaBaldeio);
                }
            }
            else
            {
                throw new Exception("Id do baldeio não encontrado");
            }
        }

        public bool ExistePorIdBaldeio(int idBaldeio)
        {
            return _arquivoBaldeioRepository.ObterPorBaldeio(idBaldeio) != null;
        }

        public void ExcluirArquivo(int idArquivoBaldeio, Usuario usuario)
        {
            if (TerPermissaoParaExcluir(usuario))
            {
                var arquivoBaldeio = _arquivoBaldeioRepository.ObterPorId(idArquivoBaldeio);
                _arquivoBaldeioRepository.Remover(arquivoBaldeio);
            }
            else
            {

                throw new Exception("Usuário não tem permissão para excluir o arquivo");
            }
        }

        private bool TerPermissaoParaExcluir(Usuario usuario)
        {
            var acao = "CONSULTABALDEIO";

            return ServiceLocator.Current.GetInstance<ControleAcessoService>().Autorizar(acao, "ExcluirCarta", usuario);
        }
    }
}