﻿namespace Translogic.Modules.Core.Domain.Services.ArquivoBaldeio.Interface
{
    using System.Web.Mvc;
    using System.IO;
    using Translogic.Modules.Core.Domain.Model.Acesso;

    public interface ICartaBaldeioService
    {
        /// <summary>
        /// Retorna a Carta de baldeio em formato PDF
        /// </summary>
        /// <param name="idArquivoBaldeio"></param>
        /// <returns></returns>
        FileStreamResult ObterArquivoBaldeio(int idArquivoBaldeio);

         /// <summary>
         /// Subir arquivo de carda do baldeio
         /// </summary>
         /// <param name="idBaldeio"></param>
         /// <param name="arquivo"></param>
         /// <param name="nomeArquivo"></param>
         /// <param name="usuario"></param>
        void AnexarCarta(int idBaldeio, byte[] arquivo, string nomeArquivo, string usuario);

        /// <summary>
        /// Verificar se existe arquivo pelo ID do Baldeio
        /// </summary>
        /// <param name="idBaldeio"></param>
        /// <returns></returns>
        bool ExistePorIdBaldeio(int idBaldeio);

        /// <summary>
        /// Excluir arquivo e registro pelo ID do Arquivo
        /// </summary>
        /// <param name="idArquivoBaldeio"></param>
        /// <param name="usuario"></param>
        void ExcluirArquivo(int idArquivoBaldeio, Usuario usuario);
    }
}
