﻿namespace Translogic.Modules.Core.Domain.Services
{
    using Translogic.Modules.Core.Domain.Model.Dto.AnexacaoDesanexacao;

    public interface IWebService
    {
        /// <summary>
        /// Executa trava trem
        /// </summary>
        /// <param name="os"> OS Trem</param>
        /// /// <param name="usuario"> usuario sistema</param>
        /// <returns></returns>
        ResultTravaTremDto ExecutarTravaTrem(decimal os, string usuario);
    }
}
