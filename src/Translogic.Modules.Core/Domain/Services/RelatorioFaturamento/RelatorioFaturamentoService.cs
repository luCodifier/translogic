﻿using Translogic.Modules.Core.Domain.Model.RelatorioFaturamento.Repositories;
using Translogic.Modules.Core.Domain.Services.RelatorioFaturamento.Interface;
using ALL.Core.Dominio;
using Translogic.Modules.Core.Domain.Model.Dto;
using System;
using Translogic.Core.Infrastructure.Web;
using System.Collections.Generic;

namespace Translogic.Modules.Core.Domain.Services.RelatorioFaturamento
{
    public class RelatorioFaturamentoService : IRelatorioFaturamentoService
    {
        private readonly IRelatorioFaturamentoRepository  _relatorioFaturamentoRepository;

        public RelatorioFaturamentoService(IRelatorioFaturamentoRepository  relatorioFaturamentoRepository)
        {
            _relatorioFaturamentoRepository = relatorioFaturamentoRepository;
        }

        public ResultadoPaginado<RelatorioFaturamentoDto> ObterRelatorioFaturamento(
                                                                 DetalhesPaginacaoWeb detalhesPaginacao,
                                                                 DateTime dataInicio,
                                                                 DateTime dataFinal,
                                                                 string fluxo,
                                                                 string origem,
                                                                 string destino,
                                                                 string[] vagoes,
                                                                 string situacao,
                                                                 string cliente,
                                                                 string expeditor,
                                                                 string segmento,
                                                                 string prefixo,
                                                                 string Os,
                                                                 string malha,
                                                                 string uf,
                                                                 string estacao)
        {
            return _relatorioFaturamentoRepository.ObterRelatorioFaturamento(detalhesPaginacao,
                                                                 dataInicio,
                                                                 dataFinal,
                                                                 fluxo,
                                                                 origem,
                                                                 destino,
                                                                 vagoes,
                                                                 situacao,
                                                                 cliente,
                                                                 expeditor,
                                                                 segmento,
                                                                 prefixo,
                                                                 Os,
                                                                 malha,
                                                                 uf,
                                                                 estacao);
            
        }
                

        public IList<RelatorioFaturamentoExportDto> ObterRelatorioFaturamentoExportar(
                                                                 DateTime dataInicio,
                                                                 DateTime dataFinal,
                                                                 string fluxo,
                                                                 string origem,
                                                                 string destino,
                                                                 string[] vagoes,
                                                                 string situacao,
                                                                 string cliente,
                                                                 string expeditor,
                                                                 string segmento,
                                                                 string prefixo,
                                                                 string Os,
                                                                 string malha,
                                                                 string uf,
                                                                 string estacao)
        {
            return _relatorioFaturamentoRepository.ObterRelatorioFaturamentoExportar(
                                                               dataInicio,
                                                               dataFinal,
                                                               fluxo,
                                                               origem,
                                                               destino,
                                                               vagoes,
                                                               situacao,
                                                               cliente,
                                                               expeditor,
                                                               segmento,
                                                               prefixo,
                                                               Os,
                                                               malha,
                                                               uf,
                                                               estacao);
        }

        public IList<ClienteTipoIntegracaoDto> ObterClientes()
        {
            return _relatorioFaturamentoRepository.ObterClientes();
        }
    }
}