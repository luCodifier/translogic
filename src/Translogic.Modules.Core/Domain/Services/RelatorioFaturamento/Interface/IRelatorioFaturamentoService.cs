﻿using System;
using ALL.Core.Dominio;
using Translogic.Modules.Core.Domain.Model.Dto;
using Translogic.Core.Infrastructure.Web;
using System.Collections.Generic;

namespace Translogic.Modules.Core.Domain.Services.RelatorioFaturamento.Interface
{
    public interface IRelatorioFaturamentoService
    {

        /// <summary>
        /// Retornar a consulta para relatório de faturamento
        /// </summary>
        /// <param name="detalhesPaginacao"></param>
        /// <param name="dataInicio"></param>
        /// <param name="dataFinal"></param>
        /// <param name="fluxo"></param>
        /// <param name="origem"></param>
        /// <param name="destino"></param>
        /// <param name="vagoes"></param>
        /// <param name="situacao"></param>
        /// <param name="cliente"></param>
        /// <param name="expeditor"></param>
        /// <param name="segmento"></param>
        /// <param name="prefixo"></param>
        /// <param name="Os"></param>
        /// <param name="malha"></param>
        /// <param name="uf"></param>
        /// <param name="estacao"></param>
        /// <returns></returns>
        ResultadoPaginado<RelatorioFaturamentoDto> ObterRelatorioFaturamento(
                                                                  DetalhesPaginacaoWeb detalhesPaginacao,
                                                                  DateTime dataInicio,
                                                                  DateTime dataFinal,
                                                                  string fluxo,
                                                                  string origem,
                                                                  string destino,
                                                                  string[] vagoes,
                                                                  string situacao,
                                                                  string cliente,
                                                                  string expeditor,
                                                                  string segmento,
                                                                  string prefixo,
                                                                  string Os,
                                                                  string malha,
                                                                  string uf,
                                                                  string estacao);

        /// <summary>
        /// Retornar a consulta para relatório de faturamento para exportação e impressão
        /// </summary>
        /// <param name="dataInicio"></param>
        /// <param name="dataFinal"></param>
        /// <param name="fluxo"></param>
        /// <param name="origem"></param>
        /// <param name="destino"></param>
        /// <param name="vagoes"></param>
        /// <param name="situacao"></param>
        /// <param name="cliente"></param>
        /// <param name="expeditor"></param>
        /// <param name="segmento"></param>
        /// <param name="prefixo"></param>
        /// <param name="Os"></param>
        /// <param name="malha"></param>
        /// <param name="uf"></param>
        /// <param name="estacao"></param>
        /// <returns></returns>
        IList<RelatorioFaturamentoExportDto> ObterRelatorioFaturamentoExportar(
                                        DateTime dataInicio,
                                        DateTime dataFinal,
                                        string fluxo,
                                        string origem,
                                        string destino,
                                        string[] vagoes,
                                        string situacao,
                                        string cliente,
                                        string expeditor,
                                        string segmento,
                                        string prefixo,
                                        string Os,
                                        string malha,
                                        string uf,
                                        string estacao);

        /// <summary>
        /// Retornar a Lista Clientes
        /// </summary>
        /// <returns>Clientes</returns>
        IList<ClienteTipoIntegracaoDto> ObterClientes();
    }
}