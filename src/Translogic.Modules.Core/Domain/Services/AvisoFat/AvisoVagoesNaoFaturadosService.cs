﻿namespace Translogic.Modules.Core.Domain.Services.AvisoFat
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Net.Mail;
    using System.Net.Mime;
    using System.Text;
    using Model.Codificador.Repositories;
    using Model.Diversos;
    using Model.Diversos.AvisoFat;
    using Model.Diversos.Repositories;

    public class AvisoVagoesNaoFaturadosService
    {
        private readonly IContatoAvisoFatRepository _contatoAvisoFatRepository;
        private readonly IConfiguracaoTranslogicRepository _configuracaoTranslogicRepository;
        private readonly ISmsRepository _smsRepository;

        private readonly string _cteEmailServer = "CTE_EMAIL_SERVER";
        private readonly string _cteEmailRemetente = "CTE_EMAIL_REMETENTE";
        private readonly string _cteEmailSenhaSmtp = "CTE_EMAIL_SMTP_SENHA";

        public AvisoVagoesNaoFaturadosService(IContatoAvisoFatRepository contatoAvisoFatRepository, IConfiguracaoTranslogicRepository configuracaoTranslogicRepository, ISmsRepository smsRepository)
        {
            _contatoAvisoFatRepository = contatoAvisoFatRepository;
            _configuracaoTranslogicRepository = configuracaoTranslogicRepository;
            _smsRepository = smsRepository;
        }

        public void EnviarAvisos()
        {
            var listaEnvio = _contatoAvisoFatRepository.CarregaObjetosEnvio();

            foreach (var item in _contatoAvisoFatRepository.ObterTodos().Where(x => x.EnviaEmail == EnviaMensagemAvisoEnum.Envia))
            {
                string texto = listaEnvio.Where(y => y.ContatoId == item.Id).Aggregate(string.Empty, (current, t) => current + ("<p>" + t.AreaOp + " : " + t.CountVagoes + "</p>"));
                if(texto.Length > 1)
                {
                    EnviaEmail(texto, item.Email);
                }
            }

            foreach (var item in _contatoAvisoFatRepository.ObterTodos().Where(x => x.EnviaSms == EnviaMensagemAvisoEnum.Envia))
            {
                string texto = listaEnvio.Where(y => y.ContatoId == item.Id).Aggregate(string.Empty, (current, t) => current + (t.AreaOp + " : " + t.CountVagoes + ". "));
                if (texto.Length > 1)
                {
                    EnviaSms(item.Telefone.ToString(), texto);
                }
            }
        }

        public void EnviaEmail(string texto, string email)
        {
            var mask = ";,: ";

            var configuracaoTranslogic = _configuracaoTranslogicRepository.ObterPorId(_cteEmailServer);
            var valorArray = configuracaoTranslogic.Valor.Split(mask.ToCharArray());

            var emailServer = valorArray[0];
            var emailPort = valorArray[1];

            var configuracaoRemetente = _configuracaoTranslogicRepository.ObterPorId(_cteEmailRemetente);
            string remetente = "noreply@all-logistica.com";

            var configuracaocteEmailSenhaSmtp = _configuracaoTranslogicRepository.ObterPorId(_cteEmailSenhaSmtp);
            var senhaSmtp = configuracaocteEmailSenhaSmtp.Valor;

            var client = new SmtpClient(emailServer, Convert.ToInt32(emailPort));

            if (!string.IsNullOrEmpty(senhaSmtp))
            {
                client.EnableSsl = true;
                client.Credentials = new NetworkCredential(remetente, senhaSmtp);
            }

            var mailMessage = new MailMessage();
            mailMessage.BodyEncoding = Encoding.UTF8;
            mailMessage.Subject = "Aviso de Faturamento";
            mailMessage.From = new MailAddress(remetente);

            mailMessage.To.Add(email);

            var corpo = new StringBuilder();
            corpo.AppendFormat(@"<p>Segue relação de faturamentos pendentes: </p>{0}",texto);
            var html = AlternateView.CreateAlternateViewFromString(corpo.ToString(), null, MediaTypeNames.Text.Html);
            mailMessage.AlternateViews.Add(html);
            mailMessage.IsBodyHtml = true;
            mailMessage.Body = corpo.ToString();

            client.Send(mailMessage);
        }

        public void EnviaSms(string telefone,string texto)
        {
            var corpo = new StringBuilder();
            corpo.AppendFormat(@"Segue relação de faturamentos pendentes: {0}", texto);

            var sms = new Sms
                          {
                              From = "TL",
                              DataAdd = DateTime.Now,
                              To = telefone,
                              Mensagem = corpo.ToString()
                          };

            _smsRepository.Inserir(sms);
        }
    }
}