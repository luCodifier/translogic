﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using Microsoft.Exchange.WebServices.Data;

namespace Translogic.Modules.Core.Domain.Services
{

    public static class Sispat
    {

        public static void EnviarEmail(string subject, string body, IList<string> toRecipients, IList<string> bccRecipients = null,
        IList<string> listFileAttachments = null)
        {
            ExchangeService exchange = new ExchangeService(ExchangeVersion.Exchange2007_SP1);

            exchange.Credentials = new NetworkCredential("sispat@all-logistica.com", "All@1234!");
            exchange.Url = new Uri("https://outlook.office365.com/EWS/Exchange.asmx");

            EmailMessage message = new EmailMessage(exchange);

            message.Subject = subject;
            message.Body = body;

            if (toRecipients != null)
            {
                foreach (var email in toRecipients)
                {
                    message.ToRecipients.Add(email);
                }
            }

            if (bccRecipients != null)
            {
                foreach (var email in bccRecipients)
                {
                    message.BccRecipients.Add(email);
                }
            }

            if (listFileAttachments != null)
            {
                foreach (var arquivo in listFileAttachments)
                {
                    message.Attachments.AddFileAttachment(arquivo);
                }
            }

            message.Send();
        }
    }
}
