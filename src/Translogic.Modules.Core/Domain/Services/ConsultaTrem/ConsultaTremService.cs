﻿using Translogic.Modules.Core.Domain.Services.ConsultaTrem.Interface;
using ALL.Core.Dominio;
using Translogic.Core.Infrastructure.Web;
using Translogic.Modules.Core.Domain.Model.Dto.ConsultaTrem;
using Translogic.Modules.Core.Domain.Model.ConsultaTrem.Repositories;
using System.Collections.Generic;
using System.Linq;

namespace Translogic.Modules.Core.Domain.Services.ConsultaTrem
{
    public class ConsultaTremService : IConsultaTremService
    {
        private readonly IConsultaTremRepository _consultaTremRepository;

        public ConsultaTremService(IConsultaTremRepository consultaTremRepository)
        {
            _consultaTremRepository = consultaTremRepository;
        }

        public ResultadoPaginado<ConsultaTremDto> ObterConsultaTrem(DetalhesPaginacaoWeb detalhesPaginacaoWeb, string prefixo, string os, string situacao, string origem, string destino, string tempoAPT, string prefixosExcluir)
        {
            //string dataInicio = "";
            //string dataFim = "";
            //var os = 0;

            //if (!string.IsNullOrEmpty(numOs))
            //{
            //    os = Convert.ToInt32(numOs);

            //    if (!_osCheckListRepository.OsDePassageiro(os))
            //    {
            //        throw new Exception("A OS informada não foi encontrada");
            //    }
            //}
            //else
            //{
            //    if (!string.IsNullOrEmpty(dataInicial) && dataInicial != "__/__/____")
            //    {
            //        dataInicio = dataInicial;
            //    }

            //    if (!string.IsNullOrEmpty(dataFinal) && dataFinal != "__/__/____")
            //    {
            //        dataFim = dataFinal;
            //    }
            //}

            var result = _consultaTremRepository.ObterConsultaTrem(detalhesPaginacaoWeb, prefixo, os, situacao, origem, destino, tempoAPT, prefixosExcluir);

            //if (result.Total == 0 && !string.IsNullOrEmpty(numOs))
            //{
            //    throw new Exception("A OS informada não é válida");
            //}

            return result;
        }

        public IEnumerable<ConsultaTremDto> ObterConsultaTremExportar(string prefixo, string os, string situacao, string origem, string destino, string tempoAPT, string prefixosExcluir)
        {

            var result = _consultaTremRepository.ObterConsultaTremExportar(prefixo, os, situacao, origem, destino, tempoAPT, prefixosExcluir);

            return result;

        }


        public IEnumerable<ConsultaTremDto> ObterTotalMercadarioOnu(List<string> listaOs)
        {
            var OsConcat = FormataParametroStringArray(listaOs);
            return _consultaTremRepository.ObterTotalMercadarioOnu(OsConcat);
        }

        private string FormataParametroStringArray(List<string> parametro)
        {
            var retorno = "";
            foreach (var valor in parametro)
            {
                if (!string.IsNullOrWhiteSpace(valor))
                    retorno += "'" + valor + "'" + ",";
            }

            return retorno.Length > 0 ? retorno.Substring(0, retorno.LastIndexOf(",")) : string.Empty;
        }

        public IList<ConsultaTremDto> AtritbuiTotalOnu(IEnumerable<ConsultaTremDto> resultado, IEnumerable<ConsultaTremDto> listaONU)
        {
            var lista = new List<ConsultaTremDto>();

            foreach (var item in resultado)
            {
                var retorno = listaONU.Where(onu => onu.NumOs == item.NumOs).FirstOrDefault();
                if (retorno != null)
                {
                    item.TotalOnu = retorno.TotalOnu;
                    lista.Add(item);
                }
                else
                {
                    lista.Add(item);
                }
            }
            return lista;
        }
    }
}