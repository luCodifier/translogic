﻿
namespace Translogic.Modules.Core.Domain.Services.ConsultaTrem
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using Translogic.Modules.Core.Domain.Services.ConsultaTrem.Interface;
    using Translogic.Modules.Core.Domain.Model.Dto.ConsultaTrem;
    using ALL.Core.Dominio;
    using Translogic.Modules.Core.Domain.Model.ConsultaTrem.Repositories;
    using AutoMapper;
    using Translogic.Modules.Core.Domain.Model.ConsultaTrem;
    using Translogic.Modules.Core.Domain.Model.Dto.ConsultaTrem.Enumeradores;
    using Translogic.Core.Infrastructure.Web;

    public class ChecklistFichaTremService : IChecklistFichaTremService
    {
        private readonly IPerguntaChecklistRepository _perguntachecklistRepository;

        public ChecklistFichaTremService(IPerguntaChecklistRepository perguntachecklistRepository)
        {
            _perguntachecklistRepository = perguntachecklistRepository;

        }

        public PerguntaCheckListDto ObterPerguntaPorId(int id)
        {
            var dto = Mapper.Map<PerguntaChecklist, PerguntaCheckListDto>(_perguntachecklistRepository.ObterPorId(id));
            return dto;
        }

        public ResultadoPaginado<PerguntaCheckListDto> ObterTodasPerguntas()
        {
            throw new NotImplementedException();

        }

        public void SalvarPergunta(PerguntaCheckListDto perguntaDto)
        {
            var entity = Mapper.Map<PerguntaCheckListDto, PerguntaChecklist>(perguntaDto);
            //entity.Tipo = (Translogic.Core.Commons.Enum<TipoPerguntaCheckList>.GetDescriptionOf(perguntaDto.TipoPerguntaCheckList)).ToString();
            entity.Tipo = perguntaDto.TipoPerguntaCheckList.ToString();
            _perguntachecklistRepository.InserirOuAtualizar(entity);
        }

        public void ExcluirPergunta(int idPergunta)
        {
            _perguntachecklistRepository.Remover(idPergunta);
        }

        public ResultadoPaginado<PerguntaCheckListDto> ObterConsultaPerguntasChecklist(DetalhesPaginacaoWeb detalhesPaginacaoWeb, int? idAgrupador, string tipoPergunta)
        {
            return _perguntachecklistRepository.ObterConsultaPerguntasChecklist(detalhesPaginacaoWeb, idAgrupador, tipoPergunta);
        }


        public IList<PerguntaCheckListDto> ObterPerguntasChecklistPorTipo(string tipo)
        {
            return _perguntachecklistRepository.ObterPerguntasChecklistPorTipo(tipo.ToUpper());
        }
    }
}