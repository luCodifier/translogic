﻿
namespace Translogic.Modules.Core.Domain.Services.ConsultaTrem.Interface
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using ALL.Core.Dominio;
    using Translogic.Modules.Core.Domain.Model.Dto.ConsultaTrem;
    using Translogic.Core.Infrastructure.Web;

    public interface IChecklistFichaTremService
    {

        //metodos Perguntas
        /// <summary>
        /// Retorna objeto, pela busca do id do registro
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        PerguntaCheckListDto ObterPerguntaPorId(int id);

        /// <summary>
        /// Retorna lista de todos os agrupadores
        /// </summary>
        /// <returns></returns>
        ResultadoPaginado<PerguntaCheckListDto> ObterTodasPerguntas();

        /// <summary>
        /// Salva registro de perguntas
        /// </summary>
        /// <param name="agrupador"></param>
        /// <returns></returns>
        void SalvarPergunta(PerguntaCheckListDto pergunta);

        /// <summary>
        /// Excluir registro
        /// </summary>
        /// <param name="agrupador"></param>
        void ExcluirPergunta(int idPergunta);

        /// <summary>
        /// Retorna as perguntas 
        /// </summary>
        /// <param name="detalhesPaginacaoWeb"></param>
        /// <param name="idAgrupador"></param>
        /// <param name="tipoPergunta"></param>
        /// <returns></returns>
        ResultadoPaginado<PerguntaCheckListDto> ObterConsultaPerguntasChecklist(DetalhesPaginacaoWeb detalhesPaginacaoWeb, int? idAgrupador, string tipoPergunta);

        /// <summary>
        /// Retorna lista de perguntas por tipo
        /// </summary>
        /// <param name="tipo"></param>
        /// <returns></returns>
        IList<PerguntaCheckListDto> ObterPerguntasChecklistPorTipo(string tipo);
    }
}
