﻿using ALL.Core.Dominio;
using Translogic.Core.Infrastructure.Web;
using Translogic.Modules.Core.Domain.Model.Dto.ConsultaTrem;
using System.Collections.Generic;
namespace Translogic.Modules.Core.Domain.Services.ConsultaTrem.Interface
{
    public interface IConsultaTremService
    {
        /// <summary>
        /// Pesquisar trens
        /// </summary>
        /// <param name="detalhesPaginacaoWeb">Paginação</param>
        /// <param name="dataInicial">Filtro data Inicial</param>
        /// <param name="dataFinal">Filtro data Final</param>
        /// <param name="numOs"> Filtro NUM OS</param>
        /// <returns>Lista de dados para GRID</returns>
        ResultadoPaginado<ConsultaTremDto> ObterConsultaTrem(DetalhesPaginacaoWeb detalhesPaginacaoWeb, string prefixo, string os, string situacao, string origem, string destino, string tempoAPT, string prefixosExcluir);

        /// <summary>
        /// Pesquisar trens
        /// </summary>
        /// <param name="detalhesPaginacaoWeb">Paginação</param>
        /// <param name="dataInicial">Filtro data Inicial</param>
        /// <param name="dataFinal">Filtro data Final</param>
        /// <param name="numOs"> Filtro NUM OS</param>
        /// <returns>Lista de dados para EXPORTAÇÃO EXCEL</returns>
        IEnumerable<ConsultaTremDto> ObterConsultaTremExportar(string prefixo, string os, string situacao, string origem, string destino, string tempoAPT, string prefixosExcluir);

        /// <summary>
        /// Verificar se existe mercadoria com codigo ONU na composição
        /// </summary>
        /// <param name="os"></param>
        /// <returns></returns>
        IEnumerable<ConsultaTremDto> ObterTotalMercadarioOnu(List<string> listaOs);

        /// <summary>
        /// Retorna lista com o total de numero ONU
        /// </summary>
        /// <param name="resultado"></param>
        /// <param name="listaONU"></param>
        /// <returns></returns>
        IList<ConsultaTremDto> AtritbuiTotalOnu(IEnumerable<ConsultaTremDto> resultado, IEnumerable<ConsultaTremDto> listaONU);

    }
}