namespace Translogic.Modules.Core.Domain.Services.FluxosComerciais
{
	using System;
	using System.Collections.Generic;
	using System.Net;
	using System.Threading;
	using Model.Bolar.Repositories;
	using Model.Codificador;
	using Model.Codificador.Repositories;
	using Model.Dto;
	using Model.FluxosComerciais;
	using Model.FluxosComerciais.Nfes;
	using Model.FluxosComerciais.Nfes.Repositories;
	using Model.FluxosComerciais.Repositories;
	using Translogic.Core.Infrastructure;
	using Translogic.Modules.Core.Domain.Model.Diversos.Cte;

	/// <summary>
	/// Classe NfeBoService
	/// </summary>
	public class NfeBoService
	{
		// Vari�veis Globais
		private readonly IBolarNfRepository _bolarNfRepository;
		private readonly INfePoolingRepository _nfePoolingRepository;
		private readonly NfeService _nfeService;
		private readonly IConfiguracaoTranslogicRepository _configuracaoTranslogicRepository;
		private readonly string _chaveMaxThread = "CTE_MAX_THREAD_SIMCONSULTA";
		private readonly string _chaveQtdeDiasConsulta = "CTE_QTD_DIAS_SIMCONSULTA";
		private readonly string _chaveNumMaxTentativas = "CTE_MAX_TENTATIVAS";
		private readonly string _chaveTempoProcessamento = "CTE_TEMPO_PROCESSAMENTO";
		private readonly DateTime _dataConsulta;
		private readonly int _numMaxThread;
		private readonly int _numMaxTentativas;
		private readonly int _numTempoProcessamento;
		
		/// <summary>
		///  Contrutor no NfeBoService
		/// </summary>
		/// <param name="nfeService">service nfeService injetado</param>
		/// <param name="bolarNfRepository">Reposit�rio do bolarNfRepository injetado</param>
		/// <param name="nfePoolingRepository">Reposit�rio do nfePoolingRepository injetado</param>
		/// <param name="configuracaoTranslogicRepository"> Configura��o Translogic repository</param>
		public NfeBoService(NfeService nfeService, IBolarNfRepository bolarNfRepository, INfePoolingRepository nfePoolingRepository, IConfiguracaoTranslogicRepository configuracaoTranslogicRepository)
		{
			_nfeService = nfeService;
			_configuracaoTranslogicRepository = configuracaoTranslogicRepository;
			_nfePoolingRepository = nfePoolingRepository;
			_bolarNfRepository = bolarNfRepository;

			// Recupera da Conf_Geral a quantidade de dias para consulta
			ConfiguracaoTranslogic qtdeDiasConsulta = _configuracaoTranslogicRepository.ObterPorId(_chaveQtdeDiasConsulta);

			if (qtdeDiasConsulta != null && !string.IsNullOrEmpty(qtdeDiasConsulta.Valor))
			{
				int dias = int.Parse(qtdeDiasConsulta.Valor);
				_dataConsulta = DateTime.Now.AddDays(-dias);
			}
			else
			{
				// Seta como default 10 execu��es simultaneas.
				_dataConsulta = DateTime.Now.AddDays(-20);
			}

			// Recupera da Conf_Geral a quantidade de threads a serem executadas 
			ConfiguracaoTranslogic maxThread = _configuracaoTranslogicRepository.ObterPorId(_chaveMaxThread);

			// Seta a quantidade de threads que v�o ser executas 
			if (maxThread != null && !string.IsNullOrEmpty(maxThread.Valor))
			{
				_numMaxThread = int.Parse(maxThread.Valor);
			}
			else
			{
				// Seta como default 10 execu��es simultaneas.
				_numMaxThread = 10;
			}

			// Recupera da Conf_Geral a quantidade de tentativas de reenvio da NFe.
			ConfiguracaoTranslogic maxTentativas = _configuracaoTranslogicRepository.ObterPorId(_chaveNumMaxTentativas);

			// Seta a quantidade de tentativas que v�o ser executas 
			if (maxTentativas != null && !string.IsNullOrEmpty(maxTentativas.Valor))
			{
				_numMaxTentativas = int.Parse(maxTentativas.Valor);
			}
			else
			{
				// Seta como default 5 tentativas.
				_numMaxTentativas = 5;
			}

			// Recupera da Conf_Geral a diferenca de tempo para as tentavias de reenvio. 
			ConfiguracaoTranslogic tempoProcessamento = _configuracaoTranslogicRepository.ObterPorId(_chaveTempoProcessamento);

			// Seta a quantidade de tempo para reenvio.
			if (tempoProcessamento != null && !string.IsNullOrEmpty(tempoProcessamento.Valor))
			{
				_numTempoProcessamento = int.Parse(tempoProcessamento.Valor);
			}
			else
			{
				// Seta como default 5 minutos.
				_numTempoProcessamento = 5;
			}
		}

		/// <summary>
		/// Enum utilizado para Origem da Nfe
		/// </summary>
		public enum OrigemNfe
		{
			/// <summary>
			/// Origem da Nfe Sispat. 
			/// </summary>
			SISPAT,

			/// <summary>
			/// Origem da Nfe BO
			/// </summary>
			BO,
		}

		/// <summary>
		/// Recupera as Nfe que ainda n�o foram processadas no BO e Inseri na tabela de Pooling
		/// </summary>
		public void InserirNfeBoPooling()
		{
			// Declara��o de objetos
			IList<string> listaNfeNaoProcessadas = null;

			try
			{
				// Recupera a lista de Nfe n�o processadas
				listaNfeNaoProcessadas = _bolarNfRepository.ObterNfeNaoProcessadas(_dataConsulta) as List<string>;
				
				// Insere as nfe de origem do Bo no pooling
				InserirNfePooling(listaNfeNaoProcessadas, OrigemNfe.BO, TelaProcessamentoNfe.Pooling);

				// Recupera a lista de nfe do SISPAT que n�o foram processadas
				listaNfeNaoProcessadas = _bolarNfRepository.ObterNfeNaoProcessadasSispat(_dataConsulta);

				// Insere as nfe de origem do SISPAT no pooling
				InserirNfePooling(listaNfeNaoProcessadas, OrigemNfe.SISPAT, TelaProcessamentoNfe.Pooling);
			}
			finally
			{
				// Finaliza os objetos
				listaNfeNaoProcessadas = null;
			}

			// ProcessarNfePooling();
		}

		/// <summary>
		/// Processa as nfe que est�o na tabela de Pooling
		/// </summary>
		/// <param name="codigoUfIbge">C�digo da UF do ibge</param>
		public void ProcessarNfePooling(string codigoUfIbge)
		{
			int index = 0;

			// Remove as Notas Antigas
			if (codigoUfIbge != "OE")
			{
				// _nfePoolingRepository.RemoverNotasAntigasPooling(_dataConsulta, _numMaxTentativas, codigoUfIbge);

				// Remove notas que estao no pooling mas j� foram processadas na tabela nfe_simconsultas.
				_nfePoolingRepository.RemoverNotasExistentesPooling(codigoUfIbge);
			}
			
            /* colocar aqui para verificar a quantidade de "processando" */

            if (_nfePoolingRepository.AtingiuLimiteProcessamento(codigoUfIbge))
            {

                // Obtem todas as Nfe a serem processadas
                IList<NfePooling> listaNfePooling = _nfePoolingRepository.ObterNotasPooling(_numMaxTentativas,
                                                                                            codigoUfIbge);

                // Seta o MaxThread
                // Comentado para que o ThreadPool fa�a aloca��o do n�mero de thread automaticamente.
                // ThreadPool.SetMaxThreads(_numMaNxThread, listaNfePooling.Count);

                if (listaNfePooling != null && listaNfePooling.Count > 0)
                {
                    ServicePointManager.DefaultConnectionLimit = 200;
                    ServicePointManager.Expect100Continue = true;
                    // ServicePointManager.MaxServicePointIdleTime = 10000;

                    //int minWorker, minCompletionPortThreads;
                    //ThreadPool.GetMinThreads(out minWorker, out minCompletionPortThreads);
                    // without setting the thread pool up, there was enough of a delay to cause timeouts!
                    //ThreadPool.SetMinThreads(listaNfePooling.Count, minCompletionPortThreads);

                    // Inst�ncia ManualResetEvent com a quantidade de notas
                    //ManualResetEvent[] doneEvents = new ManualResetEvent[listaNfePooling.Count];

                    var tasks = new List<System.Threading.Tasks.Task>();

                    foreach (NfePooling nfePooling in listaNfePooling)
                    {
                        // inst�ncia o ManualResetEvent
                        //doneEvents[index] = new ManualResetEvent(false);
                        nfePooling.IndEmProcessamento = true;
                        _nfePoolingRepository.Atualizar(nfePooling);

                        // Inst�ncia a NfeBoAsync passando os objetos necess�rios para consultar a Nfe
                        NfeBoAsync nfeBoAsync = new NfeBoAsync
                                                    {
                                                        //ManualResetEvent = doneEvents[index], 
                                                        IdNfePooling = nfePooling.Id
                                                    };

                        // insere a execu��o no pooling
                        //ThreadPool.QueueUserWorkItem(ProcessarThread, nfeBoAsync);
                        tasks.Add(new System.Threading.Tasks.Task(() => ProcessarThread(nfeBoAsync)));

                        // Incrementa o index do array de ManualResetEvent
                        index++;
                    }

                    tasks.ForEach(a => a.Start());
                    // Aguarda todas as notas serem processadas
                    // WaitHandle.WaitAll(doneEvents);

                    System.Threading.Tasks.Task.WaitAll(tasks.ToArray());
                    tasks.Clear();
                }
            }
		}

		/// <summary>
		/// Processar tread
		/// </summary>
		/// <param name="stateInfo">informa��o de estado da thread</param>
		protected void ProcessarThread(object stateInfo)
		{
			NfeBoAsync nfeBoAsync = stateInfo as NfeBoAsync;

			NfePooling nfePooling = _nfePoolingRepository.ObterPorId(nfeBoAsync.IdNfePooling);

			try
			{
				// Obtem os dados da Nfe no SimConsulta e Grava na base.
				StatusNfeDto status = _nfeService.ObterDadosNfe(nfePooling.ChaveNfe, TelaProcessamentoNfe.Pooling, true, false);

				if (!status.ObtidaComSucesso)
				{
					if (status.IndPermiteReenvio)
					{
						nfePooling.PermiteReenvio = status.IndPermiteReenvio;

						nfePooling.NumTentativas++;

						// Seta a hora do proximo processamento
						// tenta progressivo por exemplo se for 5min a config entao
						// ser� 5, 10, 15, 20, 25, 30, 30, 30, 30....
						int minutos = nfePooling.NumTentativas > 6 ? 30 : _numTempoProcessamento * nfePooling.NumTentativas;
						nfePooling.DataProxProcessamento = DateTime.Now.AddMinutes(minutos);
						nfePooling.IndEmProcessamento = false;

						// Atualiza na base
						_nfePoolingRepository.Atualizar(nfePooling);
					}
					else
					{
						_nfePoolingRepository.Remover(nfePooling);
					}
				}
				else
				{
					// Codigo 0 - Sucesso, ent�o remove a nota do pooling
					_nfePoolingRepository.Remover(nfePooling);
				}
			}
			catch (NHibernate.StaleStateException)
			{
			}
			catch (Exception)
			{
				nfePooling.PermiteReenvio = true;
				nfePooling.IndEmProcessamento = false;

				// Seta o n�mero de tentativas
				nfePooling.NumTentativas++;

				// Seta a hora do proximo processamento
				int minutos = nfePooling.NumTentativas > 6 ? 30 : _numTempoProcessamento * nfePooling.NumTentativas;
				nfePooling.DataProxProcessamento = DateTime.Now.AddMinutes(minutos);

				// Atualiza na base
				_nfePoolingRepository.Atualizar(nfePooling);
			}
            //finally
            //{
            //    // Sinaliza que o processo est� conclu�do.
            //    nfeBoAsync.ManualResetEvent.Set();
            //}
		}

		/// <summary>
		///  Insere a nfe no pooling de processamento
		/// </summary>
		/// <param name="listaNfeNaoProcessadas">Lista de Nfe a ser inserida no pooling</param>
		/// <param name="origemNfe">Base de origem da nfe</param>
		/// <param name="origemConsulta">Origem da consulta</param>
		private void InserirNfePooling(IList<string> listaNfeNaoProcessadas, OrigemNfe origemNfe, TelaProcessamentoNfe origemConsulta)
		{
			NfePooling nfePooling;
			foreach (string chaveNfe in listaNfeNaoProcessadas)
			{
				try
				{
					// Valida se a chave nfe � v�lida.
					_nfeService.ValidarChaveNfe(chaveNfe, origemConsulta);

					// Inst�ncia um novo objeto de NfePooling
					nfePooling = new NfePooling();

					// Inseri a nfe na tabela de pooling para ser processada.
					nfePooling.ChaveNfe = chaveNfe;
					nfePooling.DataNfe = DateTime.Now;
					nfePooling.DataProxProcessamento = DateTime.Now;
					nfePooling.OrigemNfe = origemNfe.ToString();
					nfePooling.IndEmProcessamento = false;
					_nfePoolingRepository.Inserir(nfePooling);
				}
				catch (TranslogicValidationException)
				{
					// Se a chave for inv�lida, ent�o � descartada e processa a pr�xima.
				}
			}
		}

		#region "Classe NfeBoAsync - Respons�vel por executar a consulta da Nfe"

		/// <summary>
		/// Classe Interna para executar a thread
		/// </summary>
		protected class NfeBoAsync
		{
			/// <summary>
			/// Manual Reset Event
			/// </summary>
			public ManualResetEvent ManualResetEvent { get; set; }

			/// <summary>
			/// Id do pooling da nfe
			/// </summary>
			public int IdNfePooling { get; set; }
		}

		#endregion
	}
}