﻿namespace Translogic.Modules.Core.Domain.Services.FluxosComerciais
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Model.Diversos.ConfigCADE;
    using Model.Diversos.ConfigCADE.Repositories;
    using Model.FluxosComerciais.Repositories;

    public class CADEConfigService
    {
        private readonly IConfigCADERepository _repository;
        private readonly IFluxoComercialRepository _fluxosRepository;

        public CADEConfigService(IConfigCADERepository repository, IFluxoComercialRepository fluxosRepository)
        {
            _repository = repository;
            _fluxosRepository = fluxosRepository;
        }

        public IList<AjusteTUMensal> ObterAjustesTU(DateTime mesReferencia, List<string> fluxo, string cliente, string segmento)
        {
            List<string> lifluxo = fluxo;
            cliente = String.IsNullOrWhiteSpace(cliente) ? null : cliente;
            segmento = String.IsNullOrWhiteSpace(segmento) ? null : segmento;
            return _repository.ListarAjustesTU(mesReferencia, lifluxo, cliente, segmento);
        }

        public IList<TremTipo> ObterTrensTipo(DateTime mesReferencia, string origem, string destino, string cliente, string segmento)
        {
            origem = String.IsNullOrWhiteSpace(origem) ? null : origem;
            destino = String.IsNullOrWhiteSpace(destino) ? null : destino;
            cliente = String.IsNullOrWhiteSpace(cliente) ? null : cliente;
            segmento = String.IsNullOrWhiteSpace(segmento) ? null : segmento;
            var trensTipo = _repository.ListarTrensTipo(mesReferencia, origem, destino, cliente, segmento);
            if (!String.IsNullOrWhiteSpace(cliente))
            {
                var fluxos = _fluxosRepository.ObterFluxosResumidosPorDescResumida(cliente);
                foreach (var fluxo in fluxos)
                {
                    if (trensTipo.Any(tt => tt.Cliente == fluxo.Cliente && tt.Origem == fluxo.Origem && tt.Destino == fluxo.Destino))
                    {
                        continue;
                    }

                    trensTipo.Add(new TremTipo
                    {
                        Cliente = fluxo.Cliente,
                        Origem = fluxo.Origem,
                        Destino = fluxo.Destino,
                        Ano = mesReferencia.Year,
                        Mes = mesReferencia.Month,
                        Segmento = segmento
                    });
                }
            }

            return trensTipo
                .Where(t =>
                    (t.Origem == origem || origem == null) ||
                    (t.Destino == destino || destino == null))
                .ToList();
        }

        public void SalvarAjustes(IEnumerable<AjusteTUMensal> ajustes)
        {
            _repository.SalvarAjustes(ajustes);
        }

        public void SalvarTrensTipo(IEnumerable<TremTipo> trensTipo)
        {
            _repository.SalvarTrensTipo(trensTipo);
        }
    }
}