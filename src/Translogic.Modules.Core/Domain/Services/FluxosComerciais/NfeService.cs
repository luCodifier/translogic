using System.Net;

namespace Translogic.Modules.Core.Domain.Services.FluxosComerciais
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Net.Mail;
    using System.Reflection;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Threading;
    using System.Xml;
    using System.Xml.Schema;
    using System.Diagnostics;
    using ALL.Core.Dominio;
    using Castle.Services.Transaction;
    using Translogic.Core.Commons;
    using Translogic.Core.Infrastructure;
    using Translogic.Core.Infrastructure.Web;
    using Translogic.Modules.Core.Domain.Model.Acesso;
    using Translogic.Modules.Core.Domain.Model.Acesso.Repositories;
    using Translogic.Modules.Core.Domain.Model.Codificador;
    using Translogic.Modules.Core.Domain.Model.Codificador.Repositories;
    using Translogic.Modules.Core.Domain.Model.Diversos;
    using Translogic.Modules.Core.Domain.Model.Diversos.Cte;
    using Translogic.Modules.Core.Domain.Model.Diversos.Ibge;
    using Translogic.Modules.Core.Domain.Model.Diversos.Ibge.Repositories;
    using Translogic.Modules.Core.Domain.Model.Diversos.Simconsultas;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Nfes;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Nfes.Repositories;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos.Repositories;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Repositories;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Simconsultas;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Simconsultas.Repositories;
    using Translogic.Modules.Core.Interfaces.Simconsultas;
    using Translogic.Modules.Core.Interfaces.Simconsultas.Interfaces;
    using Translogic.Modules.Core.Util;
    using Translogic.Modules.EDI.Domain.Models.Nfes;
    using Translogic.Modules.EDI.Domain.Models.Nfes.Repositories;
    using Translogic.Modules.EDI.Infrastructure;
    using Speed;
    using Speed.Common;
    using Translogic.Modules.EDI.Spd.BLL;
    using Translogic.Modules.Core.Spd.BLL;
    using Speed.Data;
    using SpdT = Translogic.Modules.Core.Spd.Data;
    using SpdE = Translogic.Modules.EDI.Spd.Data;
    using NHibernate;

    /// <summary>
    ///     Servi�o da NFe
    /// </summary>
    [Transactional]
    public class NfeService
    {
        #region Fields

        private readonly IAgrupamentoUsuarioRepository _agrupamentoUsuarioRepository;

        private readonly IAssociaNotaFiscalStatusNfeRepository _associaNotaFiscalStatusNfeRepository;

        private readonly IConfiguracaoTranslogicRepository _configuracaoTranslogicRepository;

        private readonly ICteSerieNumeroEmpresaUfRepository _cteSerieNumeroEmpresaUfRepository;

        private readonly IUfIbgeRepository _estadoIbgeRepository;

        private readonly IHistoricoNfeRepository _historicoNfeRepository;

        private readonly ILogSimconsultasRepository _logSimconsultasRepository;

        private readonly ILogStatusNfeRepository _logStatusNfeRepository;

        private readonly NfeConfiguracaoEmpresaService _nfeConfiguracaoEmpresaService;

        private readonly INfeConfiguracaoEmpresaUnidadeMedidaRepository _nfeConfiguracaoEmpresaUnidadeMedidaRepository;

        private readonly INfeEdiRepository _nfeEdiRepository;

        private readonly INfePoolingRepository _nfePoolingRepository;

        private readonly INfeProdutoEdiRepository _nfeProdutoEdiRepository;

        private readonly INfeProdutoReadonlyRepository _nfeProdutoReadonlyRepository;

        private readonly INfeProdutoSimconsultasRepository _nfeProdutoSimconsultasRepository;

        private readonly INfeReadonlyRepository _nfeReadonlyRepository;

        private readonly INfeSimconsultasRepository _nfeSimconsultasRepository;

        private readonly INotaFiscalTranslogicRepository _notaFiscalTranslogicRepository;

        private readonly ISimconsultasService _simconsultasService;

        private readonly IStatusNfeLogRepository _statusNfeLogRepository;

        private readonly IStatusNfeRepository _statusNfeRepository;

        private readonly IStatusRetornoNfeRepository _statusRetornoNfeRepository;

        private StringBuilder _errosValidacaoXml;

        private readonly INfePdfEdiRepository _nfePdfEdiRepository;

        private readonly INfeDistribuicaoReadonlyRepository _nfeDistribuicaoReadonlyRepository;

        private readonly IUsuarioRepository _usuarioRepository;

        private readonly INfeEstornoSaldoRepository _estornoSaldoRepository;

        private readonly IFluxoComercialRepository _fluxoComercialRepository;

        private readonly string _source = "Translogic.JobRunner";
        private readonly string _logName = "Envio de Documenta��o";

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="NfeService" /> class.
        /// </summary>
        /// <param name="nfeSimconsultasRepository"> The nfe simconsultas repository. </param>
        /// <param name="nfeReadonlyRepository"> The nfe readonly repository. </param>
        /// <param name="simconsultasService"> The simconsultas service. </param>
        /// <param name="configuracaoTranslogicRepository"> The configuracao translogic repository. </param>
        /// <param name="nfeProdutoSimconsultasRepository"> Reposit�rio de produto de nfe do simconsultas injetado</param>
        /// <param name="nfeProdutoReadonlyRepository"> Reposit�rio de produto de nfe readonly injetado </param>
        /// <param name="logSimconsultasRepository">Repositorio de log do simconsultas injetado</param>
        /// <param name="estadoIbgeRepository"> Repositorio de UfIbge injetado</param>
        /// <param name="statusRetornoNfeRepository"> Repositorio de status de retorno de nfe injetado</param>
        /// <param name="statusNfeRepository"> Reposit�rio de status de nfe injetado</param>
        /// <param name="logStatusNfeRepository">Repositorio de Log injetado</param>
        /// <param name="notaFiscalTranslogicRepository">Repositorio de nota fiscal do translogic injetado</param>
        /// <param name="associaNotaFiscalStatusNfeRepository">Repositorio de associacao de nota fiscal com status nfe injetado </param>
        /// <param name="cteSerieNumeroEmpresaUfRepository"> Repositorio da cte serie empresa Uf injetado</param>
        /// <param name="nfePoolingRepository">Reposit�rio do nfe pooling injetado</param>
        /// <param name="nfeConfiguracaoEmpresaUnidadeMedidaRepository">
        ///     Repositorio de configura��o da empresa x unidade medida
        ///     injetado
        /// </param>
        /// <param name="historicoNfeRepository">Repositorio do historico da nfe injetado</param>
        /// <param name="agrupamentoUsuarioRepository">Repositorio de agrupamento de usuario injetado</param>
        /// <param name="statusNfeLogRepository">Repositorio statusNfeLogRepository injetado</param>
        /// <param name="nfeConfiguracaoEmpresaService">Service nfeConfiguracaoEmpresaRepository injetado</param>
        /// <param name="nfeEdiRepository">Reposit�rio do Edi</param>
        /// <param name="nfeProdutoEdiRepository"> Reposit�rio de produtos do Edi</param>
        /// <param name="nfePdfEdiRepository">Reposit�rio dos produtos das nfes do EDI</param>
        /// <param name="nfeDistribuicaoReadonlyRepository">Reposit�rio das nfes de distribui��o</param>
        /// <param name="usuarioRepository">Reposit�rio de Usu�rios</param>
        /// <param name="estornoSaldoRepository">Reposit�rio de Estornos de Saldo</param>
        /// <param name="fluxoComercialRepository">Reposit�rio de Fluxo Comercial</param>
        public NfeService(
            INfeSimconsultasRepository nfeSimconsultasRepository,
            INfeReadonlyRepository nfeReadonlyRepository,
            ISimconsultasService simconsultasService,
            IConfiguracaoTranslogicRepository configuracaoTranslogicRepository,
            INfeProdutoSimconsultasRepository nfeProdutoSimconsultasRepository,
            INfeProdutoReadonlyRepository nfeProdutoReadonlyRepository,
            ILogSimconsultasRepository logSimconsultasRepository,
            IUfIbgeRepository estadoIbgeRepository,
            IStatusRetornoNfeRepository statusRetornoNfeRepository,
            IStatusNfeRepository statusNfeRepository,
            ILogStatusNfeRepository logStatusNfeRepository,
            INotaFiscalTranslogicRepository notaFiscalTranslogicRepository,
            IAssociaNotaFiscalStatusNfeRepository associaNotaFiscalStatusNfeRepository,
            ICteSerieNumeroEmpresaUfRepository cteSerieNumeroEmpresaUfRepository,
            INfePoolingRepository nfePoolingRepository,
            INfeConfiguracaoEmpresaUnidadeMedidaRepository nfeConfiguracaoEmpresaUnidadeMedidaRepository,
            IHistoricoNfeRepository historicoNfeRepository,
            IAgrupamentoUsuarioRepository agrupamentoUsuarioRepository,
            IStatusNfeLogRepository statusNfeLogRepository,
            NfeConfiguracaoEmpresaService nfeConfiguracaoEmpresaService,
            INfeEdiRepository nfeEdiRepository,
            INfeProdutoEdiRepository nfeProdutoEdiRepository,
            INfePdfEdiRepository nfePdfEdiRepository,
            INfeDistribuicaoReadonlyRepository nfeDistribuicaoReadonlyRepository,
            IUsuarioRepository usuarioRepository,
            INfeEstornoSaldoRepository estornoSaldoRepository,
            IFluxoComercialRepository fluxoComercialRepository
            )
        {
            this._nfeSimconsultasRepository = nfeSimconsultasRepository;
            this._nfeConfiguracaoEmpresaUnidadeMedidaRepository = nfeConfiguracaoEmpresaUnidadeMedidaRepository;
            this._nfePoolingRepository = nfePoolingRepository;
            this._cteSerieNumeroEmpresaUfRepository = cteSerieNumeroEmpresaUfRepository;
            this._associaNotaFiscalStatusNfeRepository = associaNotaFiscalStatusNfeRepository;
            this._notaFiscalTranslogicRepository = notaFiscalTranslogicRepository;
            this._logStatusNfeRepository = logStatusNfeRepository;
            this._statusNfeRepository = statusNfeRepository;
            this._statusRetornoNfeRepository = statusRetornoNfeRepository;
            this._estadoIbgeRepository = estadoIbgeRepository;
            this._logSimconsultasRepository = logSimconsultasRepository;
            this._nfeProdutoReadonlyRepository = nfeProdutoReadonlyRepository;
            this._nfeProdutoSimconsultasRepository = nfeProdutoSimconsultasRepository;
            this._configuracaoTranslogicRepository = configuracaoTranslogicRepository;
            this._simconsultasService = simconsultasService;
            this._nfeReadonlyRepository = nfeReadonlyRepository;
            this._historicoNfeRepository = historicoNfeRepository;
            this._agrupamentoUsuarioRepository = agrupamentoUsuarioRepository;
            this._statusNfeLogRepository = statusNfeLogRepository;
            this._nfeConfiguracaoEmpresaService = nfeConfiguracaoEmpresaService;
            this._nfeEdiRepository = nfeEdiRepository;
            this._nfeProdutoEdiRepository = nfeProdutoEdiRepository;
            this._nfePdfEdiRepository = nfePdfEdiRepository;
            this._nfeDistribuicaoReadonlyRepository = nfeDistribuicaoReadonlyRepository;
            this._usuarioRepository = usuarioRepository;
            this._estornoSaldoRepository = estornoSaldoRepository;
            this._fluxoComercialRepository = fluxoComercialRepository;
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     extrai um double de um xml
        /// </summary>
        /// <param name="xmlNode">n� do xml informado</param>
        /// <param name="tagNo">tag do xml informado</param>
        /// <param name="nsmgr">namespace manager</param>
        /// <returns>double extraido</returns>
        public static double ObterDoubleDoXML(XmlNode xmlNode, string tagNo, XmlNamespaceManager nsmgr)
        {
            double result = 0;
            string text = ObterTextoNoXml(xmlNode, tagNo, nsmgr);

            if (!string.IsNullOrWhiteSpace(text))
            {
                text = TratarFormatoNumerico(text);
                result = Convert.ToDouble(text, CultureInfo.GetCultureInfo("en-US"));
            }

            return result;
        }

        /// <summary>
        ///     obterm um texto do xml
        /// </summary>
        /// <param name="xmlNode">no do xml informado</param>
        /// <param name="tagNo">tag do xml informado</param>
        /// <param name="nsmgr">namespace manager</param>
        /// <returns>texto extraido</returns>
        public static string ObterTextoNoXml(XmlNode xmlNode, string tagNo, XmlNamespaceManager nsmgr)
        {
            XmlNode no = xmlNode.SelectSingleNode(".//d:" + tagNo, nsmgr);

            if (no == null)
            {
                return null;
            }

            return no.InnerText;
        }

        /// <summary>
        ///     Tratar Formato Numerico
        /// </summary>
        /// <param name="value">valor para tratar</param>
        /// <returns>texto tratado</returns>
        public static string TratarFormatoNumerico(string value)
        {
            int indexPeriod = value.IndexOf(".");
            int indexComma = value.IndexOf(",");

            if (indexPeriod < indexComma)
            {
                value = value.Replace(".", string.Empty).Replace(",", ".");
            }
            else if (indexComma < indexPeriod)
            {
                value = value.Replace(",", string.Empty);
            }

            return value;
        }

        /// <summary>
        ///     Altera os dados de uma nfe
        /// </summary>
        /// <param name="nfe">nfe com os novos dados</param>
        /// <param name="matriculaUsuario">matricula do usuario que e sta fazendo a alteracao</param>
        [Transaction]
        public virtual void AlterarNfe(NfeSimconsultas nfe, string matriculaUsuario)
        {
            string tabelaOrigem = this._historicoNfeRepository.ObterTabelaOrigemNfe(nfe.ChaveNfe);

            ////if (!this.PodeAlterarNfe(tabelaOrigem))
            ////{
            ////    throw new Exception("Essa nota n�o pode ser alterada, pois n�o foi obtida pelo SIMCONSULTAS.");
            ////}
            var nfeReadonly = _nfeReadonlyRepository.ObterPorChaveNfe(nfe.ChaveNfe);
            ////var statusNfe = _statusNfeRepository.ObterPorChaveNfe(nfe.ChaveNfe);

            var historicoNfe = new HistoricoNfe
            {
                ChaveNfe = nfe.ChaveNfe,
                DataAlteracao = DateTime.Now,
                MatriculaUsuario = matriculaUsuario,
                NaturezaDaOperacao = NaturezaDaOperacaoEnum.Manual,
                Origem = tabelaOrigem,
                StatusEnvioEmail = StatusEnvioEmailEnum.NaoEnviado,
                ////PesoAntigo = nfeReadonly.PesoBruto,
                ////VolumeAntigo = nfeReadonly.Volume,
                ////PesoAtual = nfe.PesoBruto,
                ////VolumeAtual = nfe.Volume ?? 0,

                BairroDestinoAntigo = nfeReadonly.BairroDestinatario,
                BairroDestinoNovo = nfe.BairroDestinatario,
                BairroOrigemAntigo = nfeReadonly.BairroEmitente,
                BairroOrigemNovo = nfe.BairroEmitente,
                CepDestinoAntigo = nfeReadonly.CepDestinatario,
                CepDestinoNovo = nfe.CepDestinatario,
                CepOrigemAntigo = nfeReadonly.CepEmitente,
                CepOrigemNovo = nfe.CepEmitente,
                CnpjDestinoAntigo = nfeReadonly.CnpjDestinatario,
                CnpjDestinoNovo = nfe.CnpjDestinatario,
                CnpjOrigemAntigo = nfeReadonly.CnpjEmitente,
                CnpjOrigemNovo = nfe.CnpjEmitente,
                CodMunicipioDestinoAntigo = nfeReadonly.CodigoMunicipioDestinatario,
                CodMunicipioDestinoNovo = nfe.CodigoMunicipioDestinatario,
                CodMunicipioOrigemAntigo = nfeReadonly.CodigoMunicipioEmitente,
                CodMunicipioOrigemNovo = nfe.CodigoMunicipioEmitente,
                CodPaisDestinoAntigo = nfeReadonly.CodigoPaisDestinatario,
                CodPaisDestinoNovo = nfe.CodigoPaisDestinatario,
                CodPaisOrigemAntigo = nfeReadonly.CodigoPaisEmitente,
                CodPaisOrigemNovo = nfe.CodigoPaisEmitente,
                ComplementoDestinoAntigo = nfeReadonly.ComplementoDestinatario,
                ComplementoDestinoNovo = nfe.ComplementoDestinatario,
                ComplementoOrigemAntigo = nfeReadonly.ComplementoEmitente,
                ComplementoOrigemNovo = nfe.ComplementoEmitente,
                FoneDestinoAntigo = nfeReadonly.TelefoneDestinatario,
                FoneDestinoNovo = nfe.TelefoneDestinatario,
                FoneOrigemAntigo = nfeReadonly.TelefoneEmitente,
                FoneOrigemNovo = nfe.TelefoneEmitente,
                IeDestinoAntigo = nfeReadonly.InscricaoEstadualDestinatario,
                IeDestinoNovo = nfe.InscricaoEstadualDestinatario,
                IeOrigemAntigo = nfeReadonly.InscricaoEstadualEmitente,
                IeOrigemNovo = nfe.InscricaoEstadualEmitente,
                LogradouroDestinoAntigo = nfeReadonly.LogradouroDestinatario,
                LogradouroDestinoNovo = nfe.LogradouroDestinatario,
                LogradouroOrigemAntigo = nfeReadonly.LogradouroEmitente,
                LogradouroOrigemNovo = nfe.LogradouroEmitente,
                MunicipioDestinoAntigo = nfeReadonly.MunicipioDestinatario,
                MunicipioDestinoNovo = nfe.MunicipioDestinatario,
                MunicipioOrigemAntigo = nfeReadonly.MunicipioEmitente,
                MunicipioOrigemNovo = nfe.MunicipioEmitente,
                NomeFantasiaDestinoAntigo = nfeReadonly.NomeFantasiaDestinatario,
                NomeFantasiaDestinoNovo = nfe.NomeFantasiaDestinatario,
                NomeFantasiaOrigemAntigo = nfeReadonly.NomeFantasiaEmitente,
                NomeFantasiaOrigemNovo = nfe.NomeFantasiaEmitente,
                NumeroDestinoAntigo = nfeReadonly.NumeroDestinatario,
                NumeroDestinoNovo = nfe.NumeroDestinatario,
                NumeroOrigemAntigo = nfeReadonly.NumeroEmitente,
                NumeroOrigemNovo = nfe.NumeroEmitente,
                PaisDestinoAntigo = nfeReadonly.PaisDestinatario,
                PaisDestinoNovo = nfe.PaisDestinatario,
                PaisOrigemAntigo = nfeReadonly.PaisEmitente,
                PaisOrigemNovo = nfe.PaisEmitente,
                RazaoSocialDestinoAntigo = nfeReadonly.RazaoSocialDestinatario,
                RazaoSocialDestinoNovo = nfe.RazaoSocialDestinatario,
                RazaoSocialOrigemAntigo = nfeReadonly.RazaoSocialEmitente,
                RazaoSocialOrigemNovo = nfe.RazaoSocialEmitente,
                UfDestinoAntigo = nfeReadonly.UfDestinatario,
                UfDestinoNovo = nfe.UfDestinatario,
                UfOrigemAntigo = nfeReadonly.UfEmitente,
                UfOrigemNovo = nfe.UfEmitente,
            };

            this._historicoNfeRepository.InserirOuAtualizar(historicoNfe);

            ////nfeReadonly.PesoBruto = nfe.PesoBruto;
            ////nfeReadonly.Volume = nfe.Volume ?? 0;

            ////statusNfe.Peso = nfe.PesoBruto / 1000;
            ////statusNfe.Volume = nfe.Volume ?? 0;

            switch (nfeReadonly.Tipo)
            {
                case "SCO":
                    NfeSimconsultas nfeSimconsultas = this._nfeSimconsultasRepository.ObterPorChaveNfe(nfe.ChaveNfe);

                    if (nfeSimconsultas != null)
                    {
                        nfeSimconsultas.BairroDestinatario = nfe.BairroDestinatario;
                        nfeSimconsultas.BairroEmitente = nfe.BairroEmitente;
                        nfeSimconsultas.CepDestinatario = nfe.CepDestinatario;
                        nfeSimconsultas.CepEmitente = nfe.CepEmitente;
                        nfeSimconsultas.CnpjDestinatario = nfe.CnpjDestinatario;
                        nfeSimconsultas.CnpjEmitente = nfe.CnpjEmitente;
                        nfeSimconsultas.CodigoMunicipioDestinatario = nfe.CodigoMunicipioDestinatario;
                        nfeSimconsultas.CodigoMunicipioEmitente = nfe.CodigoMunicipioEmitente;
                        nfeSimconsultas.CodigoPaisDestinatario = nfe.CodigoPaisDestinatario;
                        nfeSimconsultas.CodigoPaisEmitente = nfe.CodigoPaisEmitente;
                        nfeSimconsultas.ComplementoDestinatario = nfe.ComplementoDestinatario;
                        nfeSimconsultas.ComplementoEmitente = nfe.ComplementoEmitente;
                        nfeSimconsultas.InscricaoEstadualDestinatario = nfe.InscricaoEstadualDestinatario;
                        nfeSimconsultas.InscricaoEstadualEmitente = nfe.InscricaoEstadualEmitente;
                        nfeSimconsultas.LogradouroDestinatario = nfe.LogradouroDestinatario;
                        nfeSimconsultas.LogradouroEmitente = nfe.LogradouroEmitente;
                        nfeSimconsultas.MunicipioDestinatario = nfe.MunicipioDestinatario;
                        nfeSimconsultas.MunicipioEmitente = nfe.MunicipioEmitente;
                        nfeSimconsultas.NomeFantasiaDestinatario = nfe.NomeFantasiaDestinatario;
                        nfeSimconsultas.NomeFantasiaEmitente = nfe.NomeFantasiaEmitente;
                        nfeSimconsultas.NumeroDestinatario = nfe.NumeroDestinatario;
                        nfeSimconsultas.NumeroEmitente = nfe.NumeroEmitente;
                        nfeSimconsultas.PaisDestinatario = nfe.PaisDestinatario;
                        nfeSimconsultas.PaisEmitente = nfe.PaisEmitente;
                        nfeSimconsultas.RazaoSocialDestinatario = nfe.RazaoSocialDestinatario;
                        nfeSimconsultas.RazaoSocialEmitente = nfe.RazaoSocialEmitente;
                        nfeSimconsultas.TelefoneDestinatario = nfe.TelefoneDestinatario;
                        nfeSimconsultas.TelefoneEmitente = nfe.TelefoneEmitente;
                        nfeSimconsultas.UfDestinatario = nfe.UfDestinatario;
                        nfeSimconsultas.UfEmitente = nfe.UfEmitente;

                        this._nfeSimconsultasRepository.InserirOuAtualizar(nfeSimconsultas);
                    }
                    break;
                case "EDI":
                    var nfeDistribuicao = _nfeDistribuicaoReadonlyRepository.ObterPorChaveNfe(nfe.ChaveNfe);

                    if (nfeDistribuicao != null)
                    {
                        nfeDistribuicao.BairroDestinatario = nfe.BairroDestinatario;
                        nfeDistribuicao.BairroEmitente = nfe.BairroEmitente;
                        nfeDistribuicao.CepDestinatario = nfe.CepDestinatario;
                        nfeDistribuicao.CepEmitente = nfe.CepEmitente;
                        nfeDistribuicao.CnpjDestinatario = nfe.CnpjDestinatario;
                        nfeDistribuicao.CnpjEmitente = nfe.CnpjEmitente;
                        nfeDistribuicao.CodigoMunicipioDestinatario = nfe.CodigoMunicipioDestinatario;
                        nfeDistribuicao.CodigoMunicipioEmitente = nfe.CodigoMunicipioEmitente;
                        nfeDistribuicao.CodigoPaisDestinatario = nfe.CodigoPaisDestinatario;
                        nfeDistribuicao.CodigoPaisEmitente = nfe.CodigoPaisEmitente;
                        nfeDistribuicao.ComplementoDestinatario = nfe.ComplementoDestinatario;
                        nfeDistribuicao.ComplementoEmitente = nfe.ComplementoEmitente;
                        nfeDistribuicao.InscricaoEstadualDestinatario = nfe.InscricaoEstadualDestinatario;
                        nfeDistribuicao.InscricaoEstadualEmitente = nfe.InscricaoEstadualEmitente;
                        nfeDistribuicao.LogradouroDestinatario = nfe.LogradouroDestinatario;
                        nfeDistribuicao.LogradouroEmitente = nfe.LogradouroEmitente;
                        nfeDistribuicao.MunicipioDestinatario = nfe.MunicipioDestinatario;
                        nfeDistribuicao.MunicipioEmitente = nfe.MunicipioEmitente;
                        nfeDistribuicao.NomeFantasiaDestinatario = nfe.NomeFantasiaDestinatario;
                        nfeDistribuicao.NomeFantasiaEmitente = nfe.NomeFantasiaEmitente;
                        nfeDistribuicao.NumeroDestinatario = nfe.NumeroDestinatario;
                        nfeDistribuicao.NumeroEmitente = nfe.NumeroEmitente;
                        nfeDistribuicao.PaisDestinatario = nfe.PaisDestinatario;
                        nfeDistribuicao.PaisEmitente = nfe.PaisEmitente;
                        nfeDistribuicao.RazaoSocialDestinatario = nfe.RazaoSocialDestinatario;
                        nfeDistribuicao.RazaoSocialEmitente = nfe.RazaoSocialEmitente;
                        nfeDistribuicao.TelefoneDestinatario = nfe.TelefoneDestinatario;
                        nfeDistribuicao.TelefoneEmitente = nfe.TelefoneEmitente;
                        nfeDistribuicao.UfDestinatario = nfe.UfDestinatario;
                        nfeDistribuicao.UfEmitente = nfe.UfEmitente;

                        this._nfeDistribuicaoReadonlyRepository.InserirOuAtualizar(nfeDistribuicao);
                    }
                    else
                    {
                        var nfeEdi = _nfeEdiRepository.ObterPorChaveNfe(nfe.ChaveNfe);

                        if (nfeEdi != null)
                        {
                            nfeEdi.BairroDestinatario = nfe.BairroDestinatario;
                            nfeEdi.BairroEmitente = nfe.BairroEmitente;
                            nfeEdi.CepDestinatario = nfe.CepDestinatario;
                            nfeEdi.CepEmitente = nfe.CepEmitente;
                            nfeEdi.CnpjDestinatario = nfe.CnpjDestinatario;
                            nfeEdi.CnpjEmitente = nfe.CnpjEmitente;
                            nfeEdi.CodigoMunicipioDestinatario = nfe.CodigoMunicipioDestinatario;
                            nfeEdi.CodigoMunicipioEmitente = nfe.CodigoMunicipioEmitente;
                            nfeEdi.CodigoPaisDestinatario = nfe.CodigoPaisDestinatario;
                            nfeEdi.CodigoPaisEmitente = nfe.CodigoPaisEmitente;
                            nfeEdi.ComplementoDestinatario = nfe.ComplementoDestinatario;
                            nfeEdi.ComplementoEmitente = nfe.ComplementoEmitente;
                            nfeEdi.InscricaoEstadualDestinatario = nfe.InscricaoEstadualDestinatario;
                            nfeEdi.InscricaoEstadualEmitente = nfe.InscricaoEstadualEmitente;
                            nfeEdi.LogradouroDestinatario = nfe.LogradouroDestinatario;
                            nfeEdi.LogradouroEmitente = nfe.LogradouroEmitente;
                            nfeEdi.MunicipioDestinatario = nfe.MunicipioDestinatario;
                            nfeEdi.MunicipioEmitente = nfe.MunicipioEmitente;
                            nfeEdi.NomeFantasiaDestinatario = nfe.NomeFantasiaDestinatario;
                            nfeEdi.NomeFantasiaEmitente = nfe.NomeFantasiaEmitente;
                            nfeEdi.NumeroDestinatario = nfe.NumeroDestinatario;
                            nfeEdi.NumeroEmitente = nfe.NumeroEmitente;
                            nfeEdi.PaisDestinatario = nfe.PaisDestinatario;
                            nfeEdi.PaisEmitente = nfe.PaisEmitente;
                            nfeEdi.RazaoSocialDestinatario = nfe.RazaoSocialDestinatario;
                            nfeEdi.RazaoSocialEmitente = nfe.RazaoSocialEmitente;
                            nfeEdi.TelefoneDestinatario = nfe.TelefoneDestinatario;
                            nfeEdi.TelefoneEmitente = nfe.TelefoneEmitente;
                            nfeEdi.UfDestinatario = nfe.UfDestinatario;
                            nfeEdi.UfEmitente = nfe.UfEmitente;

                            Conv.TrimObject(nfeEdi);
                            _nfeEdiRepository.InserirOuAtualizar(nfeEdi);
                        }
                    }
                    break;
            }
            ////_statusNfeRepository.InserirOuAtualizar(statusNfe);

            this.EnviarEmailAlteracaoNfe(historicoNfe);
        }

        /// <summary>
        ///     Altera o volume e o peso de uma nfe
        /// </summary>
        /// <param name="chaveNfe">chave da nfe alterada</param>
        /// <param name="novoVolume">valor do novo volume</param>
        /// <param name="novoPeso">valor do novo peso</param>
        /// <param name="matriculaUsuario">matricula do usuario que e sta fazendo a alteracao</param>
        [Transaction]
        public virtual void AlterarVolumePesoNfe(
            string chaveNfe,
            double novoVolume,
            double novoPeso,
            string matriculaUsuario,
            Usuario usuarioAtual,
            double novoVolumeLiberar,
            double novoPesoLiberar,
            bool teveLiberacaoSaldoNfe,
            string autorizadoPor,
            string justificativa)
        {
            string tabelaOrigem = this._historicoNfeRepository.ObterTabelaOrigemNfe(chaveNfe);

            ////if (!this.PodeAlterarNfe(tabelaOrigem))
            ////{
            ////    throw new Exception("Essa nota n�o pode ser alterada, pois n�o foi obtida pelo SIMCONSULTAS.");
            ////}
            var nfeReadonly = _nfeReadonlyRepository.ObterPorChaveNfe(chaveNfe);

            var pesoAntigo = nfeReadonly.PesoBruto;
            var volumeAntigo = nfeReadonly.Volume.HasValue ? nfeReadonly.Volume : 0;
            StatusNfe statusNfe = this._statusNfeRepository.ObterPorChaveNfe(chaveNfe);

            var historicoNfe = new HistoricoNfe
            {
                ChaveNfe = chaveNfe,
                DataAlteracao = DateTime.Now,
                MatriculaUsuario = matriculaUsuario,
                NaturezaDaOperacao = NaturezaDaOperacaoEnum.Manual,
                Origem = tabelaOrigem,
                StatusEnvioEmail = StatusEnvioEmailEnum.NaoEnviado,
                PesoAntigo = pesoAntigo, ////nfeSimconsultas.PesoBruto,
                VolumeAntigo = volumeAntigo, ////nfeSimconsultas.Volume,
                PesoAtual = novoPeso,
                VolumeAtual = novoVolume
            };

            this._historicoNfeRepository.InserirOuAtualizar(historicoNfe);

            //
            // Insere na tabela de estorno de saldo de nfe (Nova tabela a pedido da Priscila)
            // Motivo: registrar a justificativa e o nome de quem autorizou o estorno de saldo
            //
            var pesoUtilizado = novoPesoLiberar > 0 ? (novoPeso - novoPesoLiberar) / 1000 : statusNfe.PesoUtilizado;
            var volumeUtilizado = novoVolumeLiberar > 0 ? (novoVolume - novoVolumeLiberar) : statusNfe.VolumeUtilizado;

            if (teveLiberacaoSaldoNfe)
            {
                novoPesoLiberar = novoPesoLiberar / 1000;

                var estornoSaldoNfe = new NfeEstornoSaldo
                {
                    ChaveNfe = chaveNfe,
                    Justificativa = justificativa,
                    Aprovador = autorizadoPor,
                    PesoUtilizado = pesoUtilizado,
                    PesoEstornado = novoPesoLiberar,
                    VolumeUtilizado = volumeUtilizado,
                    VolumeEstornado = novoVolumeLiberar,
                    DataEstorno = DateTime.Now,
                    UsuarioEstorno = usuarioAtual,
                    TransacaoEstorno = "ESTORNOSALDONOTA"
                };
                this._estornoSaldoRepository.InserirOuAtualizar(estornoSaldoNfe);
            }

            statusNfe.Peso = novoPeso / 1000;
            statusNfe.PesoUtilizado = pesoUtilizado;
            statusNfe.Volume = novoVolume;
            statusNfe.VolumeUtilizado = volumeUtilizado;

            this._statusNfeRepository.InserirOuAtualizar(statusNfe);
            this.EnviarEmailAlteracaoNfe(historicoNfe);

            switch (nfeReadonly.Tipo)
            {
                case "SCO":
                    NfeSimconsultas nfeSimconsultas = this._nfeSimconsultasRepository.ObterPorChaveNfe(chaveNfe);

                    if (nfeSimconsultas != null)
                    {
                        nfeSimconsultas.PesoBruto = novoPeso;
                        nfeSimconsultas.Volume = novoVolume;
                        this._nfeSimconsultasRepository.InserirOuAtualizar(nfeSimconsultas);
                    }
                    break;
                case "EDI":
                    var nfeDistribuicao = _nfeDistribuicaoReadonlyRepository.ObterPorChaveNfe(chaveNfe);

                    if (nfeDistribuicao != null)
                    {
                        nfeDistribuicao.PesoBruto = novoPeso;
                        nfeDistribuicao.Volume = novoVolume;
                        _nfeDistribuicaoReadonlyRepository.InserirOuAtualizar(nfeDistribuicao);
                    }
                    else
                    {
                        var nfeEdi = _nfeEdiRepository.ObterPorChaveNfe(chaveNfe);

                        if (nfeEdi != null)
                        {
                            nfeEdi.PesoBruto = novoPeso;
                            nfeEdi.Volume = novoVolume;
                            Conv.TrimObject(nfeEdi);
                            _nfeEdiRepository.InserirOuAtualizar(nfeEdi);
                        }
                    }
                    break;
            }
        }

        /// <summary>
        ///     Atualiza os dados da nfe Edi
        /// </summary>
        public void AtualizarDadosNfeEdi()
        {
            IList<string> nfeForaEdi = this._nfeReadonlyRepository.ObterNfeForaEdi();

            foreach (string chaveNfe in nfeForaEdi)
            {
                try
                {
                    this.ObterDadosNfe(chaveNfe, TelaProcessamentoNfe.CorrecaoNfeEdi, true, false);
                }
                catch (Exception ex)
                {
                    Sis.LogException(ex, "Erro ao ObterDadosNfe: " + chaveNfe);
                }
            }
        }

        /// <summary>
        ///     Atualiza os dados de peso da NF-e
        /// </summary>
        /// <param name="pesoLiquido">Peso L�quido</param>
        /// <param name="pesoBruto">Peso Bruto</param>
        /// <param name="chaveNfe">Chave da nfe</param>
        /// <param name="indVolume">Indicador se � volume</param>
        public void AtualizarPesosNfe(double pesoLiquido, double pesoBruto, string chaveNfe, bool indVolume)
        {
            NfeReadonly nfe = this.ObterDadosNfeBancoDados(chaveNfe);
            StatusNfe statusNfe = this._statusNfeRepository.ObterPorChaveNfe(chaveNfe);

            if (indVolume)
            {
                nfe.Volume = pesoBruto;
                statusNfe.Volume = pesoBruto / 1000;
            }
            else
            {
                nfe.Peso = pesoLiquido;
                nfe.PesoBruto = pesoBruto;
                statusNfe.Peso = pesoBruto / 1000;
            }

            this._nfeReadonlyRepository.AtualizarPesosNfe(nfe);
            this._statusNfeRepository.Atualizar(statusNfe);
        }

        /// <summary>
        /// Extorna saldo da nota no nfe status
        /// </summary>
        /// <param name="statusNfe">Objeto status nfe</param>
        /// <param name="peso">peso que ser� extornado</param>
        public void RetornarSaldoNfe(StatusNfe statusNfe, double peso)
        {
            try
            {
                statusNfe.PesoUtilizado = (statusNfe.PesoUtilizado - peso);
                this._statusNfeRepository.Atualizar(statusNfe);
            }
            catch (Exception ex)
            {
                Sis.LogException(ex, "Erro em RetornarSaldoNfe");
                Console.Write(ex);
            }
        }

        /// <summary>
        ///     Da como conferido uma alteracao da NFe
        /// </summary>
        /// <param name="idHistoricoNfe">id do historico de alteracao da nfe</param>
        public void DarComoConferidoVolumePesoNfe(int idHistoricoNfe)
        {
            HistoricoNfe historicoNfeOriginal = this._historicoNfeRepository.ObterPorId(idHistoricoNfe);
            historicoNfeOriginal.AlteracaoConferida = true;
            this._historicoNfeRepository.InserirOuAtualizar(historicoNfeOriginal);
        }

        /// <summary>
        ///     Grava o log de status da nfe e retorna o ultimo status
        /// </summary>
        /// <param name="nfe">Objeto da Nfe caso haja sucesso no parser</param>
        /// <param name="statusRetorno">Status de retorno do simconsultas</param>
        /// <param name="chaveNfe">Chave de acesso da Nfe</param>
        /// <param name="origem"> Tela de origem de nfe</param>
        /// <param name="notasBrado"> Indica Notas Clientes Brado</param>
        /// <param name="peso_bruto">peso_bruto da StatusNFe</param>
        /// <returns>Status da Nfe</returns>
        public SpdT.StatusNfe GravarLogStatusNfe(
            NotaFiscalEletronica nfe,
            StatusRetornoNfe statusRetorno,
            string chaveNfe,
            TelaProcessamentoNfe origem,
            bool notasBrado,
            ref double peso_bruto)
        {
            decimal peso = 0;
            decimal volume = 0;

            var tc = new TimerCount();

            var logStatusNfe = new LogStatusNfe
            {
                ChaveNfe = chaveNfe,
                DataCadastro = DateTime.Now,
                StatusRetornoNfe = statusRetorno
            };
            tc.Next("_logStatusNfeRepository.Inserir");
            this._logStatusNfeRepository.Inserir(logStatusNfe);

            if (nfe != null)
            {
                tc.Next("_configuracaoTranslogicRepository.ObterPorId");
                ConfiguracaoTranslogic configuracaoTranslogic =
                    this._configuracaoTranslogicRepository.ObterPorId("SIGLAS_UNIDADE_TONELADA");

                string[] arraySiglaUnidadeTonelada = null;
                if (configuracaoTranslogic != null && !string.IsNullOrEmpty(configuracaoTranslogic.Valor))
                {
                    arraySiglaUnidadeTonelada = configuracaoTranslogic.Valor.Split(',');
                }

                if (nfe.ListaProdutos != null)
                {
                    if (
                        nfe.ListaProdutos.Any(
                            p =>
                            !String.IsNullOrEmpty(p.UnidadeComercial)
                            && (p.UnidadeComercial.ToUpperInvariant().StartsWith("L")
                                || p.UnidadeComercial.ToUpperInvariant().StartsWith("M"))) && nfe.Volume.HasValue)
                    {
                        volume = (decimal)Tools.TruncateValue(nfe.Volume.Value, TipoTruncateValueEnum.UnidadeMilhar);
                    }

                    if (
                        nfe.ListaProdutos.Any(
                            p =>
                            !String.IsNullOrEmpty(p.UnidadeComercial) && arraySiglaUnidadeTonelada != null
                            && arraySiglaUnidadeTonelada.Contains(p.UnidadeComercial)))
                    {
                        // � necess�rio realizar esta valida��o para salvar como toneladas pois algumas NFs enviam na lista de produtos como sendo toneladas (condi��o deste IF)
                        // mas o peso do transporte (nfe.Peso) em Kgs. Na teoria poderia eliminar o IF, mas irei deixar por ser menos risco de erro
                        peso = nfe.Peso > 150 ? (decimal)Tools.TruncateValue(nfe.Peso / 1000, TipoTruncateValueEnum.UnidadeMilhar) : (decimal)nfe.Peso;
                    }
                    else
                    {
                        peso = (decimal)Tools.TruncateValue(nfe.Peso / 1000, TipoTruncateValueEnum.UnidadeMilhar);
                    }
                }
            }

            tc.Next("_statusNfeRepository.ObterPorChaveNfe");
            //StatusNfe statusNfe = this._statusNfeRepository.ObterPorChaveNfe(chaveNfe);
            var statusNfe = BL_StatusNfe.ObterPorChaveNfe(chaveNfe);

            //var statusNfe = Spd.BLL.BL_StatusNfe.ObterPorChaveNfe(chaveNfe);
            if (statusNfe != null)
            {
                if (statusNfe.Peso == 0 && peso > 0)
                {
                    statusNfe.Peso = (decimal?)peso;
                }

                if (statusNfe.PesoBruto == 0 && peso_bruto > 0)
                {
                    statusNfe.PesoBruto = (decimal?)Tools.TruncateValue((double?)(nfe.PesoBruto < 150 ? nfe.PesoBruto : nfe.PesoBruto / 1000), TipoTruncateValueEnum.UnidadeMilhar);
                }

                if (statusNfe.Volume == 0 && volume > 0)
                {
                    statusNfe.Volume = (decimal?)volume;
                }

                if (notasBrado)
                {
                    statusNfe.Peso = statusNfe.PesoBruto ?? 0M;
                }

                statusNfe.IdStatusRetornoNfe = statusRetorno.Id;
                statusNfe.Origem = origem.ToString();
                tc.Next("_statusNfeRepository.Atualizar");

                //this._statusNfeRepository.Atualizar(statusNfe);
                BL_StatusNfe.Update(statusNfe, EnumSaveMode.Requery);
            }
            else
            {
                statusNfe = new Spd.Data.StatusNfe
                {
                    ChaveNfe = chaveNfe,
                    DataCadastro = DateTime.Now,
                    PesoUtilizado = 0,
                    VolumeUtilizado = 0,
                    IdStatusRetornoNfe = statusRetorno.Id,
                    Peso = notasBrado ? (decimal?)Tools.TruncateValue((double?)(nfe.PesoBruto < 150 ? nfe.PesoBruto : nfe.PesoBruto / 1000), TipoTruncateValueEnum.UnidadeMilhar) : peso,
                    Volume = volume,
                    PesoBruto = (decimal)Tools.TruncateValue(nfe.PesoBruto < 150 ? nfe.PesoBruto : nfe.PesoBruto / 1000, TipoTruncateValueEnum.UnidadeMilhar),
                    Origem = origem.ToString()
                };

                tc.Next("_statusNfeRepository.Inserir");
                //this._statusNfeRepository.Inserir(statusNfe);
                BL_StatusNfe.Insert(statusNfe, EnumSaveMode.Requery);
            }

            var time = tc.ToString();
            return statusNfe;
        }

        /// <summary>
        ///     Retorna o estado atraves da chave nfe
        /// </summary>
        /// <param name="chaveNfe"> Chave da Nfe a ser inserida no pooling</param>
        /// <param name="sistemaOrigem"> Sistema de Origem da Nfe</param>
        /// <returns> Retorna a NfeStatus</returns>
        public StatusNfeDto InserirNfePooling(string chaveNfe, string sistemaOrigem)
        {
            // Inst�ncia um novo objeto de NfePooling
            var nfePooling = new NfePooling();

            // Inseri a nfe na tabela de pooling para ser processada.
            nfePooling.ChaveNfe = chaveNfe;
            nfePooling.DataNfe = DateTime.Now;
            nfePooling.DataProxProcessamento = DateTime.Now;
            nfePooling.OrigemNfe = sistemaOrigem;
            nfePooling.PermiteReenvio = true;
            this._nfePoolingRepository.Inserir(nfePooling);

            return new StatusNfeDto
            {
                ObtidaComSucesso = false,
                IndPermiteReenvio = false,
                IndLiberaDigitacao = true,
                NotaFiscalEletronica = null,
                MensagemErro = string.Empty,
                StackTrace = string.Empty
            };
        }

        /// <summary>
        ///     Insere o status da nfe
        /// </summary>
        /// <param name="chaveNfe">Chave da Nfe</param>
        /// <param name="codigoStatusRetorno">Codigo do status de retorno da nfe</param>
        /// <param name="pesoNota">Peso Total da Nfe</param>
        /// <param name="pesoUtilizado">Peso Utilizado da Nfe</param>
        /// <param name="volumeNota">Volume Total da Nfe</param>
        /// <param name="volumeUtilizado">Volume utilizado na nfe</param>
        /// <param name="origem">Origem consulta</param>
        /// <param name="pesoBruto">Peso Bruto da Nfe</param>
        /// <returns>Objeto StatusNfe</returns>
        public StatusNfe InserirStatusNfe(
            string chaveNfe,
            string codigoStatusRetorno,
            double pesoNota,
            double pesoUtilizado,
            double? volumeNota,
            double volumeUtilizado,
            TelaProcessamentoNfe origem,
            double? pesoBruto)
        {
            StatusNfe statusNfe = this._statusNfeRepository.ObterPorChaveNfe(chaveNfe);
            if (statusNfe == null)
            {
                StatusRetornoNfe statusRetorno = this._statusRetornoNfeRepository.ObterPorCodigo(codigoStatusRetorno);
                statusNfe = new StatusNfe
                {
                    ChaveNfe = chaveNfe,
                    DataCadastro = DateTime.Now,
                    PesoUtilizado = pesoUtilizado,
                    VolumeUtilizado = volumeUtilizado,
                    StatusRetornoNfe = statusRetorno,
                    Peso = pesoNota,
                    Volume = volumeNota.HasValue ? volumeNota.Value : 0,
                    Origem = origem.ToString(),
                    PesoBruto = pesoBruto
                };
                this._statusNfeRepository.Inserir(statusNfe);
            }

            return statusNfe;
        }

        /// <summary>
        ///     Lista somento historicos para aprova��o
        /// </summary>
        /// <returns>lista de historicos de altera��o da nfe n�o aprovados ainda</returns>
        public ResultadoPaginado<HistoricoNfe> ListarHistoricoNfe()
        {
            List<HistoricoNfe> itens = this._historicoNfeRepository.ListarHistoricoNfe();
            var result = new ResultadoPaginado<HistoricoNfe> { Items = itens, Total = itens.Count };
            return result;
        }

        /// <summary>
        ///     Obt�m os dados da NF-e
        /// </summary>
        /// <param name="chaveNfe">Chave da NF-e</param>
        /// <param name="origemPesquisa">Tela que efetuou a pesquisa.</param>
        /// <param name="indProcessamentoBackground"> Indica se o processamento � em background</param>
        /// <param name="notasBrado"> Indica se � uma nota de um cliente da Brado</param>
        /// <returns>Dto de status da nfe</returns>
        public StatusNfeDto ObterDadosNfe(
            string chaveNfe,
            TelaProcessamentoNfe origemPesquisa,
            bool indProcessamentoBackground, bool? notasBrado)
        {
            notasBrado = notasBrado ?? false;
            StatusNfe statusNfe = this._statusNfeRepository.ObterPorChaveNfe(chaveNfe);
            ConfiguracaoTranslogic configuracao =
                this._configuracaoTranslogicRepository.ObterPorId("LIBERACAO_DT_180_CGTO_ERRO_V07");
            if (configuracao != null && configuracao.Valor.Equals("S"))
            {
                if (!indProcessamentoBackground && statusNfe != null && statusNfe.StatusRetornoNfe != null
                    && statusNfe.StatusRetornoNfe.Codigo.Equals("V07"))
                {
                    return new StatusNfeDto
                    {
                        ObtidaComSucesso = false,
                        IndPermiteReenvio =
                            indProcessamentoBackground
                                ? statusNfe.StatusRetornoNfe.IndPermiteReenvioBo
                                : statusNfe.StatusRetornoNfe.IndPermiteReenvioTela,
                        IndLiberaDigitacao = statusNfe.StatusRetornoNfe.IndLiberarDigitacao,
                        NotaFiscalEletronica = null,
                        MensagemErro =
                            string.Format(statusNfe.StatusRetornoNfe.Mensagem, chaveNfe),
                        CodigoStatusRetorno = statusNfe.StatusRetornoNfe.Codigo
                    };
                }
            }

            var statusNfeDto = this.ObterStatusNfeBancoDados(chaveNfe, origemPesquisa, (bool)notasBrado);
            if (statusNfe != null && statusNfeDto != null)
            {
                // Verifica se conseguiu recuperar do banco de Dados
                if (!statusNfe.StatusRetornoNfe.IndPermiteReenvioTela
                    || (statusNfe.Peso > 0 || statusNfe.Volume > 0))
                {
                    return statusNfeDto;
                }
            }

            if (statusNfe == null || statusNfe.StatusRetornoNfe.IndPermiteReenvioTela)
            {
                return this.ObterNfeSimConsultas(chaveNfe, indProcessamentoBackground, origemPesquisa, (bool)notasBrado);
            }

            if ((indProcessamentoBackground && statusNfe.StatusRetornoNfe.IndPermiteReenvioBo)
                || (!indProcessamentoBackground && statusNfe.StatusRetornoNfe.IndPermiteReenvioTela))
            {
                return this.ObterNfeSimConsultas(chaveNfe, indProcessamentoBackground, origemPesquisa, (bool)notasBrado);
            }

            return new StatusNfeDto
            {
                ObtidaComSucesso = false,
                IndPermiteReenvio =
                    indProcessamentoBackground
                        ? statusNfe.StatusRetornoNfe.IndPermiteReenvioBo
                        : statusNfe.StatusRetornoNfe.IndPermiteReenvioTela,
                IndLiberaDigitacao = statusNfe.StatusRetornoNfe.IndLiberarDigitacao,
                NotaFiscalEletronica = null,
                MensagemErro = string.Format(statusNfe.StatusRetornoNfe.Mensagem, chaveNfe),
                CodigoStatusRetorno = statusNfe.StatusRetornoNfe.Codigo
            };
        }

        /*/// <summary>
        /// Obt�m os dados da NFe
        /// </summary>
        /// <param name="chaveNfe"> Chave da nfe. </param>
        /// <returns> Objeto de nota fiscal eletronica </returns>
        public INotaFiscalEletronica ObterDadosNfe(string chaveNfe)
        {
            int codigoErro = 0;
            ValidarChaveNfe(chaveNfe);
            return ObterDadosNfeBancoDados(chaveNfe) ?? ObterNfeSimConsultas(chaveNfe, out codigoErro);
        }*/

        /// <summary>
        ///     Obt�m os dados da NFe da base de dados
        /// </summary>
        /// <param name="chaveNfe">Chave da Nfe</param>
        /// <returns> Objeto de nota fiscal </returns>
        public NfeReadonly ObterDadosNfeBancoDados(string chaveNfe)
        {
            return this._nfeReadonlyRepository.ObterPorChaveNfe(chaveNfe);
        }

        /// <summary>'
        ///     Obt�m os dados das NFes da base de dados
        /// </summary>
        /// <param name="listaChavesNfe">Lista de chaves da nfes</param>
        /// <returns> Lista de NfeReadonly </returns>
        public IList<NfeReadonly> ObterDadosNfesBancoDados(string listaChavesNfe)
        {
            if (String.IsNullOrEmpty(listaChavesNfe))
                throw new Exception("Nenhuma chave informada.");

            var chavesNfeLista =
                listaChavesNfe.ToUpper()
                    .Trim()
                    .Replace(" ", string.Empty)
                    .Replace(",", ";")
                    .Replace(".", ";")
                    .Split(';')
                    .Where(e => !string.IsNullOrWhiteSpace(e))
                    .ToList();

            return this._nfeReadonlyRepository.ObterPorChavesNfe(chavesNfeLista);
        }

        /// <summary>
        ///     Obt�m os dados da NFe da base de dados
        /// </summary>
        /// <param name="idNotaFiscal">Id da Nota fiscal</param>
        /// <returns> Objeto de nota fiscal </returns>
        public NotaFiscalDto ObterDadosNfeBancoDadosPorId(int idNotaFiscal)
        {
            return this._nfeReadonlyRepository.ObterNotaFiscalPorIdNota(idNotaFiscal);
        }

        /// <summary>
        ///     Obtem os dados da Nfe no SimConsulta
        /// </summary>
        /// <param name="chaveNfe">Chave da Nfe</param>
        /// <returns>Retorno o objeto nota RetornoSimconsultas</returns>
        public RetornoSimconsultas ObterDadosNfeSimConsulta(string chaveNfe)
        {
            // Declara��o de vari�veis
            ConfiguracaoTranslogic configuracao;
            DateTime dataInicioProcesso = DateTime.Now;
            RetornoSimconsultas dadosNota = null;

            try
            {
                // Recupera a chave de acesso do Sim Consulta
                configuracao = this._configuracaoTranslogicRepository.ObterPorId("CHAVE_ACESSO_SIMCONSULTAS");

                // Obtem os dados da Nfe
                dadosNota = this._simconsultasService.ObterDadosNfe(configuracao.Valor, chaveNfe);

                // Grava o Log do processo
                this.GravarLogProcesso(dataInicioProcesso, chaveNfe, dadosNota);

                return dadosNota;
            }
            catch (Exception ex)
            {
                Sis.LogException(ex, "Erro em ObterDadosNfeSimConsulta");
                if (dadosNota == null)
                {
                    dadosNota = new RetornoSimconsultas { MensagemErro = ex.Message };
                }
                // seta o tipo Erro
                dadosNota.Erro = true;

                // Grava o Log do processo
                this.GravarLogProcesso(dataInicioProcesso, chaveNfe, dadosNota);
            }
            finally
            {
                // Finaliza os objetos
                dadosNota = null;
            }

            return dadosNota;
        }

        /// <summary>
        ///     Obtem os dados da Nfe no SimConsulta pelo portal nacional prioritariamente
        /// </summary>
        /// <param name="chaveNfe">Chave da Nfe</param>
        /// <returns>Retorno o objeto nota RetornoSimconsultas</returns>
        public RetornoSimconsultas ObterDadosNfeSimConsultaNacional(string chaveNfe)
        {
            // Declara��o de vari�veis
            ConfiguracaoTranslogic configuracao;
            DateTime dataInicioProcesso = DateTime.Now;
            RetornoSimconsultas dadosNota = null;

            try
            {
                // Recupera a chave de acesso do Sim Consulta
                configuracao = this._configuracaoTranslogicRepository.ObterPorId("CHAVE_ACESSO_SIMCONSULTAS");

                // Obtem os dados da Nfe
                dadosNota = this._simconsultasService.ObterDadosNfeNacional(configuracao.Valor, chaveNfe);

                // Grava o Log do processo
                this.GravarLogProcesso(dataInicioProcesso, chaveNfe, dadosNota);

                return dadosNota;
            }
            catch (Exception ex)
            {
                Sis.LogException(ex, "Erro em ObterDadosNfeSimConsultaNacional");
                if (dadosNota == null)
                {
                    dadosNota = new RetornoSimconsultas { MensagemErro = ex.Message };
                }
                // seta o tipo Erro
                dadosNota.Erro = true;

                // Grava o Log do processo
                this.GravarLogProcesso(dataInicioProcesso, chaveNfe, dadosNota);
            }
            finally
            {
                // Finaliza os objetos
                dadosNota = null;
            }

            return dadosNota;
        }

        /// <summary>
        ///     Obt�m os dados da NFe da base de dados
        /// </summary>
        /// <param name="idDespacho">Id do despacho da composi��o</param>
        /// <returns> Objeto de nota fiscal </returns>
        public IList<NotaFiscalDto> ObterDadosNotaFiscal(int idDespacho)
        {
            return this._nfeReadonlyRepository.ObterNotaFiscalPorDespachoId(idDespacho);
        }

        /// <summary>
        ///     Obt�m os dados do banco de dados pela chave
        /// </summary>
        /// <param name="chave">Chave da Nfe</param>
        /// <returns>Objeto NfeSimconsultas</returns>
        public NfeSimconsultas ObterDbNfeSimconsultasPorChave(string chave)
        {
            return this._nfeSimconsultasRepository.ObterPorChaveNfe(chave);
        }

        /// <summary>
        ///     Obt�m os dados de status da NFe da base de dados
        /// </summary>
        /// <param name="chaveNfe">Chave da Nfe</param>
        /// <returns> Objeto de Status da nota fiscal </returns>
        public IList<DespachosNfeDto> ObterDespachosPorNfe(string chaveNfe)
        {
            return this._statusNfeRepository.ObterDespachosPorChaveNfe(chaveNfe);
        }

        /// <summary>
        ///     Obt�m o Dto por nfe baixada
        /// </summary>
        /// <param name="nota">Objeto Nota fiscal</param>
        /// <param name="statusNfe">Objeto status nfe</param>
        /// <param name="origemPesquisa">Tela que efetuou a pesquisa</param>
        /// <param name="notasBrado"> Indica Notas Clientes Brado</param>
        /// <returns>Dto de sucesso</returns>
        public StatusNfeDto ObterDtoPorNfeBaixada(
            INotaFiscalEletronica nota,
            StatusNfe statusNfe,
            TelaProcessamentoNfe origemPesquisa,
            bool notasBrado)
        {
            if (statusNfe == null)
            {
                IList<NotaFiscalTranslogic> listaNotas =
                    this._notaFiscalTranslogicRepository.ObterPorChaveNfeSemDespachosCancelados(nota.ChaveNfe);
                double pesoUtilizado = 0;
                double volumeUtilizado = 0;

                if (listaNotas != null && listaNotas.Count > 0)
                {
                    pesoUtilizado = listaNotas.Sum(c => c.PesoNotaFiscal ?? 0);
                    volumeUtilizado = listaNotas.Sum(c => c.VolumeNotaFiscal ?? 0);

                    if (pesoUtilizado > 0)
                    {
                        pesoUtilizado = Tools.TruncateValue(pesoUtilizado, TipoTruncateValueEnum.UnidadeMilhar);
                    }

                    if (volumeUtilizado > 0)
                    {
                        volumeUtilizado = Tools.TruncateValue(volumeUtilizado, TipoTruncateValueEnum.UnidadeMilhar);
                    }
                }

                double? volumeNota = Tools.TruncateValue(nota.Volume, TipoTruncateValueEnum.UnidadeMilhar);
                double pesoNota = Tools.TruncateValue(nota.Peso / 1000, TipoTruncateValueEnum.UnidadeMilhar);
                double pesoBruto = Tools.TruncateValue(nota.PesoBruto / 1000, TipoTruncateValueEnum.UnidadeMilhar);
                string chaveNfe = nota.ChaveNfe;

                if (notasBrado)
                {
                    pesoNota = pesoBruto;
                }

                statusNfe = this.InserirStatusNfe(
                    chaveNfe,
                    "0",
                    pesoNota,
                    pesoUtilizado,
                    volumeNota,
                    volumeUtilizado,
                    origemPesquisa,
                    pesoBruto);

                // INSERE AS ASSOCIA��ES ENTRE O STATUS E AS NOTAS FISCAIS
                if (listaNotas != null)
                {
                    foreach (NotaFiscalTranslogic notaFiscalTranslogic in listaNotas)
                    {
                        if (notaFiscalTranslogic.Id.HasValue && statusNfe.Id > 0)
                        {
                            var associa = new AssociaNotaFiscalStatusNfe
                            {
                                DataCadastro = DateTime.Now,
                                IdNotaFiscal = notaFiscalTranslogic.Id.Value,
                                IdStatusNfe = statusNfe.Id
                            };
                            this._associaNotaFiscalStatusNfeRepository.Inserir(associa);
                        }
                    }
                }
            }

            if (statusNfe.Peso == 0 && statusNfe.Volume == 0)
            {
                double? volumeNota = Tools.TruncateValue(nota.Volume, TipoTruncateValueEnum.UnidadeMilhar);
                double pesoNota = Tools.TruncateValue(nota.Peso / 1000, TipoTruncateValueEnum.UnidadeMilhar);

                StatusRetornoNfe statusRetornoNfe = this._statusRetornoNfeRepository.ObterPorCodigo("0");

                if (notasBrado && nota.PesoBruto > 0)
                {
                    pesoNota = Tools.TruncateValue(nota.PesoBruto / 1000, TipoTruncateValueEnum.UnidadeMilhar);
                }

                statusNfe.Peso = pesoNota;
                statusNfe.Volume = volumeNota ?? 0;
                statusNfe.StatusRetornoNfe = statusRetornoNfe;
                this._statusNfeRepository.Atualizar(statusNfe);
            }

            // return VerificarExisteSaldoNfe(statusNfe, nota);
            return new StatusNfeDto
            {
                Peso = statusNfe.Peso,
                PesoBruto = statusNfe.PesoBruto,
                Volume = statusNfe.Volume,
                PesoUtilizado = statusNfe.PesoUtilizado,
                VolumeUtilizado = statusNfe.VolumeUtilizado,
                ObtidaComSucesso = true,
                IndPermiteReenvio = false,
                NotaFiscalEletronica = nota,
                MensagemErro = string.Empty,
                StackTrace = string.Empty
            };
        }

        /// <summary>
        ///     Obter uma NfeEdi dada a chave informada
        /// </summary>
        /// <param name="chaveNfe">Chave para se obter a NfeEdi gravada no banco de dados</param>
        /// <returns>NfeEdi de acordo com a chave informada</returns>
        public NfeEdi ObterNfeEdiPorChave(string chaveNfe)
        {
            return this._nfeEdiRepository.ObterPorChaveNfe(chaveNfe);
        }

        /// <summary>
        ///     Tenta obter os dados da NFe pelo webservice do Simconsultas
        /// </summary>
        /// <param name="chaveNfe"> The chave nfe. </param>
        /// <param name="indProcessoBackground">Indica se � um processo em background</param>
        /// <param name="origem"> Origem da consulta</param>
        /// <param name="notasBrado"> Notas da Brado</param>
        /// <returns> Objeto DTO de status da nfe</returns>
        public StatusNfeDto ObterNfeSimConsultas(
            string chaveNfe,
            bool indProcessoBackground,
            TelaProcessamentoNfe origem,
            bool notasBrado)
        {
            string mensagemErro = null;
            for (int i = 0; i < 3; i++)
            {
                RetornoSimconsultas dadosNota = i % 2 == 0
                                                    ? this.ObterDadosNfeSimConsultaNacional(chaveNfe)
                                                    : this.ObterDadosNfeSimConsulta(chaveNfe);

                if (!dadosNota.Erro)
                {
                    try
                    {
                        StatusNfe statusNfe = this.ProcessarRetornoSimconsultas(dadosNota, chaveNfe, origem, notasBrado);
                        // Status de ok, pode-se pegar os dados do banco
                        if (statusNfe.StatusRetornoNfe.Codigo.Equals("000"))
                        {
                            NfeReadonly notaBanco = this.ObterDadosNfeBancoDados(chaveNfe);
                            if (notaBanco != null)
                            {
                                return this.ObterDtoPorNfeBaixada(notaBanco, statusNfe, origem, notasBrado);
                            }
                        }

                        // Status de cancelamento
                        if (statusNfe.StatusRetornoNfe.Codigo.Equals("100"))
                        {
                            return new StatusNfeDto
                            {
                                IndPermiteReenvio = false,
                                CodigoStatusRetorno = statusNfe.StatusRetornoNfe.Codigo,
                                ObtidaComSucesso = false,
                                NotaFiscalEletronica = null,
                                MensagemErro =
                                    string.Format(statusNfe.StatusRetornoNfe.Mensagem, chaveNfe),
                                IndLiberaDigitacao = false,
                                StackTrace = string.Empty
                            };
                        }

                        // quando for processo em background deve-se verificar pelo flag IndPermiteReenvioBo
                        // quando n�o for processo em background deve-se verficar pelo flag IndPermiteReenvioTela
                        if ((indProcessoBackground && statusNfe.StatusRetornoNfe.IndPermiteReenvioBo)
                            || (!indProcessoBackground && statusNfe.StatusRetornoNfe.IndPermiteReenvioTela))
                        {
                            Thread.Sleep(500);
                            continue;
                        }

                        return new StatusNfeDto
                        {
                            IndPermiteReenvio =
                                indProcessoBackground
                                    ? statusNfe.StatusRetornoNfe.IndPermiteReenvioBo
                                    : statusNfe.StatusRetornoNfe.IndPermiteReenvioTela,
                            IndLiberaDigitacao = statusNfe.StatusRetornoNfe.IndLiberarDigitacao,
                            CodigoStatusRetorno = statusNfe.StatusRetornoNfe.Codigo,
                            ObtidaComSucesso = false,
                            NotaFiscalEletronica = null,
                            MensagemErro =
                                string.Format(statusNfe.StatusRetornoNfe.Mensagem, chaveNfe),
                            StackTrace = string.Empty
                        };
                    }
                    catch (Exception ex)
                    {
                        Sis.LogException(ex, "Erro em ObterNfeSimConsultas");
                        string stack = "EX01 - " + ex.Message + "\n" + ex.StackTrace;
                        return new StatusNfeDto
                        {
                            IndPermiteReenvio = true,
                            IndLiberaDigitacao = true,
                            CodigoStatusRetorno = "EX01",
                            ObtidaComSucesso = false,
                            NotaFiscalEletronica = null,
                            MensagemErro =
                                string.Format(
                                    "Ocorreu um erro ao processar os dados da NF-e({0}).",
                                    chaveNfe),
                            StackTrace = stack
                        };
                    }
                }

                mensagemErro = dadosNota.MensagemErro;
            }

            return new StatusNfeDto
            {
                IndPermiteReenvio = true,
                IndLiberaDigitacao = true,
                CodigoStatusRetorno = "EX02",
                ObtidaComSucesso = false,
                NotaFiscalEletronica = null,
                MensagemErro =
                    string.Format("Ocorreu um erro ao obter os dados da NF-e({0}).", chaveNfe),
                StackTrace = "EX02 - " + mensagemErro
            };
        }

        /// <summary>
        ///     Obt�m os produtos dada uma nfe identificando pelo tipo e ID
        /// </summary>
        /// <param name="notaFiscalEletronica">Nota fiscal eletronica</param>
        /// <returns>Lista de produtos de nfe</returns>
        public IList<NfeProdutoReadonly> ObterProdutosNfe(INotaFiscalEletronica notaFiscalEletronica)
        {
            return this._nfeProdutoReadonlyRepository.ObterPorTipoId(
                notaFiscalEletronica.Id.Value,
                notaFiscalEletronica.Tipo);
        }

        /// <summary>
        ///     Retorna o estado atraves da chave nfe
        /// </summary>
        /// <param name="siglaEstado">Chave Nfe para consulta</param>
        /// <returns> Retorna a serie emp Uf</returns>
        public CteSerieNumeroEmpresaUf ObterSerieEmpUfPorEstado(string siglaEstado)
        {
            return this._cteSerieNumeroEmpresaUfRepository.ObterPorUf(siglaEstado);
        }

        /// <summary>
        ///     Obt�m os dados de status da NFe da base de dados
        /// </summary>
        /// <param name="chaveNfe">Chave da Nfe</param>
        /// <returns> Objeto de Status da nota fiscal </returns>
        public StatusNfe ObterStatusNfe(string chaveNfe)
        {
            return this._statusNfeRepository.ObterPorChaveNfe(chaveNfe);
        }

        /// <summary>
        ///     Retorna os dados do Banco de dados
        /// </summary>
        /// <param name="chaveNfe">Chave Nfe para consulta</param>
        /// <param name="origem">Origem da consulta</param>
        /// <returns>Retorna o status da nfe</returns>
        public StatusNfeDto ObterStatusNfeBancoDados(string chaveNfe, TelaProcessamentoNfe origem, bool notasBrado)
        {
            try
            {
                this.ValidarChaveNfe(chaveNfe, origem);
            }
            catch (Exception ex)
            {
                Sis.LogException(ex, "Erro em ObterStatusNfeBancoDados");
                return new StatusNfeDto
                {
                    ObtidaComSucesso = false,
                    IndPermiteReenvio = false,
                    IndLiberaDigitacao = false,
                    NotaFiscalEletronica = null,
                    MensagemErro = ex.Message,
                    StackTrace = ex.StackTrace
                };
            }

            StatusNfe statusNfe = this._statusNfeRepository.ObterPorChaveNfe(chaveNfe);
            NfeReadonly notaBanco = this.ObterDadosNfeBancoDados(chaveNfe);

            if (notaBanco != null)
            {
                return this.ObterDtoPorNfeBaixada(notaBanco, statusNfe, origem, notasBrado);
            }

            return null;
        }

        /// <summary>
        ///     Verifica se pode alterar a nfe
        /// </summary>
        /// <param name="nomeTabela">nome da tabela de origem da nfe</param>
        /// <returns>boolean value</returns>
        public bool PodeAlterarNfe(string nomeTabela)
        {
            return nomeTabela == "nfe_simconsultas";
        }

        /// <summary>
        ///     Verifica se, ap�s ocorrer um erro ao carregar a NFe, a mesma deve ser liberada para digita��o.
        /// </summary>
        /// <param name="codigoErro">c�digo do erro ao carregar</param>
        /// <returns>valor boleano</returns>
        public bool PodeLiberarDigitacaoNfe(string codigoErro)
        {
            StatusRetornoNfe entity = this._statusRetornoNfeRepository.ObterPorCodigo(codigoErro);

            if (entity == null)
            {
                return false;
            }

            return entity.IndLiberarDigitacao;
        }

        /// <summary>
        ///     Realiza o processamento do xml para obter os valores dos campos
        /// </summary>
        /// <param name="docXml">O documento xml</param>
        public void ProcessarDadosNfeEdi(XmlDocument docXml)
        {
            string codigoErro = "000";
            string mensagemErro = String.Empty;
            double peso_bruto = 0;

            XmlElement root = docXml.DocumentElement;
            var nsmgr = new XmlNamespaceManager(docXml.NameTable);
            nsmgr.AddNamespace("d", root.NamespaceURI);

            /* esse If trata o XML novo da Brado */
            if (root.Name.Equals("soap:Envelope"))
            {
                nsmgr.AddNamespace("soap", root.NamespaceURI);

                var xmlBrado = new StringBuilder();
                xmlBrado.Append("<nfeProc xmlns:ns2=\"http://www.w3.org/2000/09/xmldsig#\" xmlns=\"http://www.portalfiscal.inf.br/nfe\" versao=\"3.10\">");

                xmlBrado.Append(
                    (((root.SelectSingleNode("//soap:Body", nsmgr)).FirstChild).
                        FirstChild).InnerXml.ToString());

                xmlBrado.Append("</nfeProc>");
                /* recarrega o XML em mem�ria */
                docXml.LoadXml(xmlBrado.ToString());
                root = docXml.DocumentElement;
                nsmgr = new XmlNamespaceManager(docXml.NameTable);
                nsmgr.AddNamespace("d", root.NamespaceURI);
            }

            var noInfNfe = root.SelectSingleNode("//d:NFe/d:infNFe", nsmgr);

            var nfeLido = (NotaFiscalEletronica)this.ProcessarNfeXml(docXml);

            if (nfeLido != null)
            {
                // Caso a nfe tenha sido processada simultaneamente, ent�o retorna a nfe j� processada.
                //NfeEdi nfeEdi = this._nfeEdiRepository.ObterPorChaveNfe(nfeLido.ChaveNfe);
                var nfeEdi = BL_Edi2Nfe.ObterPorChaveNfe(nfeLido.ChaveNfe);
                NfeEdi nfe = null;

                if (nfeEdi == null)
                {
                    try
                    {
                        nfe = new NfeEdi(nfeLido);

                        nfe.Xml = docXml.OuterXml;

                        Conv.TrimObject(nfeEdi);
                        this._nfeEdiRepository.Inserir(nfe);

                        XmlNode noTransp = noInfNfe.SelectSingleNode("//d:transp", nsmgr);
                        var docTransp = new XmlDocument();
                        docTransp.LoadXml(noTransp.OuterXml);

                        IEnumerable<INotaFiscalEletronicaProduto> listaProdutosXml =
                            this.ProcessarProdutosNfeXml(noInfNfe.SelectNodes("//d:det", nsmgr), nsmgr);

                        nfe.ListaProdutos = this.ProcessarProdutosNfeEdi(nfe, listaProdutosXml);

                        XmlNodeList listaNoVolume = docTransp.SelectNodes("//d:vol", nsmgr);

                        var nfeParam = (NotaFiscalEletronica)nfe;

                        nfeParam.ListaProdutos = new List<NotaFiscalEletronicaProduto>();

                        foreach (NfeProdutoEdi produto in nfe.ListaProdutos)
                        {
                            nfeParam.ListaProdutos.Add(produto);
                        }

                        this._nfeConfiguracaoEmpresaService.ObterPesoVolumeNfe(listaNoVolume, nsmgr, ref nfeParam, ref peso_bruto);

                        nfe.Peso = nfeParam.Peso;
                        nfe.PesoBruto = nfeParam.PesoBruto;
                        nfe.Volume = nfeParam.Volume;

                        this._nfeEdiRepository.Atualizar(nfe);
                    }
                    catch (Exception ex)
                    {
                        Sis.LogException(ex, "Erro em ProcessarDadosNfeEdi. CodigoErro=EDI1");
                        codigoErro = "EDI1";
                        mensagemErro = ex.Message;
                    }
                    finally
                    {
                        StatusRetornoNfe statusRetorno = this._statusRetornoNfeRepository.ObterPorCodigo(codigoErro);
                        if (statusRetorno == null)
                        {
                            statusRetorno = new StatusRetornoNfe
                            {
                                Codigo = codigoErro,
                                DataCadastro = DateTime.Now,
                                IndPermiteReenvioTela = false,
                                IndPermiteReenvioBo = false,
                                IndLiberarDigitacao = true,
                                Mensagem = mensagemErro
                            };
                            this._statusRetornoNfeRepository.Inserir(statusRetorno);
                        }

                        if (nfe != null)
                        {
                            this.GravarLogStatusNfe(
                                nfe,
                                statusRetorno,
                                nfe.ChaveNfe,
                                TelaProcessamentoNfe.CorrecaoNfeEdi,
                                false,
                                ref peso_bruto);
                        }
                    }
                }
                /* valida se existe um fluxo valido para essa nota que esta entrando na base */
                ////this.ValidaExistenciaFluxoNota(nfeEdi);
            }
        }

        /// <summary>
        /// M�todo que vai receber os dados da Nfe que sta sendo inserida na base para conferido se existe um Fluxo v�lido para a mesma
        /// </summary>
        //// <param name="nfe">Objeto com dados da Nfe</param>
        [Transaction]
        public virtual void ValidaExistenciaFluxoNota()
        {
            try
            {
                LogHelper.Log("In�cio processo ValidaExistenciaFluxoNota;", EventLogEntryType.Information, _source,
                              _logName);
                var pagination = new DetalhesPaginacaoWeb();
                pagination.Limit = 0;
                pagination.Limite = 0;

                var datafiltro = new List<DateTime>();
                datafiltro.Add(DateTime.Now.AddMinutes(-15));
                datafiltro.Add(DateTime.Now);
                var liNfe = this._nfeEdiRepository.ObterNfePorRangeDataHqlList(datafiltro.ToArray());

                LogHelper.Log(string.Format("ValidaExistenciaFluxoNota:: Encontradas {0} notas.", liNfe.Count.ToString()),
                    EventLogEntryType.Information, _source, _logName);

                #region Vari�veis de configura��o de e-mail
                var remetente = "noreply@rumolog.com";
                var remetenteConf = _configuracaoTranslogicRepository.ObterPorId("GERENCIAMENTO_DOCUMENTACAO_EMAIL_REMETENTE");
                if (remetenteConf != null && !string.IsNullOrEmpty(remetenteConf.Valor))
                {
                    remetente = remetenteConf.Valor;
                }

                ConfiguracaoTranslogic configuracaoTranslogic = this._configuracaoTranslogicRepository.ObterPorId("EDI_EMAIL_REMETENTE");
                var smtpServer = _configuracaoTranslogicRepository.ObterPorId("GERENCIAMENTO_DOCUMENTACAO_EMAIL_SERVER");
                var emailServer = smtpServer.Valor;
                var smtpPort = _configuracaoTranslogicRepository.ObterPorId("GERENCIAMENTO_DOCUMENTACAO_EMAIL_PORTA");
                int? emailPort = null;

                if (!string.IsNullOrEmpty(smtpPort.Valor))
                {
                    emailPort = int.Parse(smtpPort.Valor);
                }

                var smtpSenha = _configuracaoTranslogicRepository.ObterPorId("GERENCIAMENTO_DOCUMENTACAO_EMAIL_SMTP_SENHA");
                var emailSenha = smtpSenha.Valor;

                #endregion Vari�veis de configura��o de e-mail

                foreach (var nfe in liNfe)
                {
                    IList<NotaFiscalFluxoAssociacaoDto> listaFluxosAssociadosNfe =
                        this.ObterFluxosAssociadosChaveNfe(pagination, nfe.ChaveNfe, true);

                    if (!listaFluxosAssociadosNfe.Any())
                    {
                        string body =
                            @"
                    <p>Foi cadastrada a NFe de chave ([chave]) via ADMNFE, e foi verificado que a mesma
                    n�o possui um fluxo cadastrado vig�nte.</p><br/>
                    <p>Favor providenciar Fluxo Comercial para a mesma.</p>
                    <p>Dados da Nota:</p>
                    <p>Emitente: [cnpjEmitente] - [nomeEmitente]</p>
                    <p>Destinat�rio: [cnpjDestinatario] - [nomeDestinatario]</p>
                    "
                                .Replace("[chave]", nfe.ChaveNfe)
                                .Replace("[cnpjEmitente]", nfe.CnpjEmitente)
                                .Replace("[nomeEmitente]", nfe.RazaoSocialEmitente)
                                .Replace("[cnpjDestinatario]", nfe.CnpjDestinatario)
                                .Replace("[nomeDestinatario]", nfe.RazaoSocialDestinatario);

                        var pdfanexo = this._nfePdfEdiRepository.ObterPorChavesNfe(new List<string> { nfe.ChaveNfe }).FirstOrDefault();

                        try
                        {
                            var mailMessage = new MailMessage();
                            mailMessage.From = new MailAddress(remetente);
                            mailMessage.IsBodyHtml = true;
                            if (pdfanexo != null && pdfanexo.Pdf != null)
                            {
                                var stream = new MemoryStream(pdfanexo.Pdf);
                                var anexo = new Attachment(stream, nfe.ChaveNfe, "application/pdf");
                                mailMessage.Attachments.Add(anexo);
                            }
                            mailMessage.Body = body;
                            mailMessage.Subject = "Solicita��o de Cadastro/Ativa��o de Fluxo (ADMNFE)";
                            mailMessage.SubjectEncoding = Encoding.UTF8;
                            mailMessage.BodyEncoding = Encoding.UTF8;

                            configuracaoTranslogic =
                                this._configuracaoTranslogicRepository.ObterPorId("EDI_EMAIL_GESTAO_FLUXO");

                            foreach (var email in configuracaoTranslogic.Valor.Replace(",", ";").Split(';'))
                            {
                                if (!string.IsNullOrEmpty(email))
                                    mailMessage.To.Add(email);
                            }

                            using (var client = new SmtpClient(emailServer, emailPort.HasValue ? emailPort.Value : 25))
                            {
                                if (!string.IsNullOrEmpty(emailSenha))
                                {
                                    client.EnableSsl = true;
                                    client.Credentials = new NetworkCredential(remetente, emailSenha);
                                }
                                LogHelper.Log("Faz o Send no SmtpClient para envio dos e-mails.", EventLogEntryType.Information, _source, _logName);
                                client.Send(mailMessage);
                            }

                            LogHelper.Log("ValidaExistenciaFluxoNota :: Email enviado com sucesso;", EventLogEntryType.Information, _source,
                              _logName);
                        }
                        catch (SmtpException sex)
                        {
                            LogHelper.Log(string.Format("ValidaExistenciaFluxoNota:: SmtpInnerException: {0}, Message: {1} ", sex.InnerException, sex.Message),
                                EventLogEntryType.Error, _source, _logName);
                        }
                    }
                }
                LogHelper.Log("Fim processo ValidaExistenciaFluxoNota;", EventLogEntryType.Information, _source,
                              _logName);
            }
            catch (Exception ex)
            {
                LogHelper.Log(string.Format("ValidaExistenciaFluxoNota:: InnerException: {0}, Message: {1} ", ex.InnerException, ex.Message),
                    EventLogEntryType.Error, _source, _logName);
            }
        }

        /// <summary>
        ///     Processa os dados da Nfe
        /// </summary>
        /// <param name="doc"> Documento xml da NFe. </param>
        //// <param name="peso_bruto">peso_bruto da StatusNFe</param>
        /// <param name="notasBrado">Indica Clientes da Brado</param>
        /// <returns> Objeto de NFe da simconsultas persistido </returns>
        // [Transaction(TransactionMode.RequiresNew, IsolationMode.ReadCommitted, Distributed = true)]
        public virtual NfeSimconsultas ProcessarDadosNfeSimConsultas(XmlDocument doc, bool notasBrado, ref double peso_bruto)
        {
            XmlElement root = doc.DocumentElement;
            var nsmgr = new XmlNamespaceManager(doc.NameTable);
            nsmgr.AddNamespace("d", root.NamespaceURI);

            XmlNode noInfNfe = root.SelectSingleNode("//d:NFe/d:infNFe", nsmgr);

            var nfeLido = (NotaFiscalEletronica)this.ProcessarNfeXml(doc);

            if (nfeLido != null)
            {
                var nfe = new NfeSimconsultas(nfeLido);

                nfe.Xml = doc.OuterXml;

                // Caso a nfe tenha sido processada simultaneamente, ent�o retorna a nfe j� processada.
                NfeSimconsultas nfeSimconsultas = this._nfeSimconsultasRepository.ObterPorChaveNfe(nfe.ChaveNfe);

                if (nfeSimconsultas != null)
                {
                    var nfeRetorno = (NotaFiscalEletronica)nfeSimconsultas;
                    this.ParseNfes(nfe, ref nfeRetorno);

                    nfeSimconsultas = (NfeSimconsultas)nfeRetorno;

                    this._nfeSimconsultasRepository.Atualizar(nfeSimconsultas);

                    nfe = nfeSimconsultas;

                    nfeSimconsultas.ListaProdutos =
                        this._nfeProdutoSimconsultasRepository.ObterPorNfeId(nfeSimconsultas.Id.Value);
                }
                else
                {
                    this._nfeSimconsultasRepository.Inserir(nfe);
                }

                XmlNode noTransp = noInfNfe.SelectSingleNode("//d:transp", nsmgr);
                var docTransp = new XmlDocument();
                docTransp.LoadXml(noTransp.OuterXml);

                IEnumerable<INotaFiscalEletronicaProduto> listaProdutosXml =
                    this.ProcessarProdutosNfeXml(noInfNfe.SelectNodes("//d:det", nsmgr), nsmgr);

                if (listaProdutosXml == null || listaProdutosXml.Count() == 0)
                {
                    throw new NfeException("Xml corrompido, Nota Fiscal n�o possui produtos!");
                }

                nfe.ListaProdutos = this.ProcessarProdutosNfeSimConsultas(nfe, listaProdutosXml);

                XmlNodeList listaNoVolume = docTransp.SelectNodes("//d:vol", nsmgr);

                var nfeParam = (NotaFiscalEletronica)nfe;

                nfeParam.ListaProdutos = new List<NotaFiscalEletronicaProduto>();

                foreach (NfeProdutoSimconsultas produto in nfe.ListaProdutos)
                {
                    nfeParam.ListaProdutos.Add(produto);
                }

                this._nfeConfiguracaoEmpresaService.ObterPesoVolumeNfe(listaNoVolume, nsmgr, ref nfeParam, ref peso_bruto);

                if (notasBrado)
                {
                    nfe.Peso = nfeParam.PesoBruto;
                    nfe.PesoBruto = nfeParam.PesoBruto;
                    nfe.Volume = nfeParam.Volume;
                }
                else
                {
                    nfe.Peso = nfeParam.Peso;
                    nfe.PesoBruto = nfeParam.PesoBruto;
                    nfe.Volume = nfeParam.Volume;
                }
                this._nfeSimconsultasRepository.Atualizar(nfe);

                return nfe;
            }

            return null;
        }

        /// <summary>
        ///     Recebe o documento xml e realiza o parser
        /// </summary>
        /// <param name="doc">Documento Xml</param>
        /// <returns>Interface Nota Fiscal Eletr�nica</returns>
        public INotaFiscalEletronica ProcessarNfeXml(XmlDocument doc)
        {
            string[] datePatterns = { "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd", "dd/MM/yyyy", "dd/MM/yyyy HH:mm:ss", "yyyy-MM-ddTHH:mm:sszzz" };

            XmlElement root = doc.DocumentElement;
            var nsmgr = new XmlNamespaceManager(doc.NameTable);
            nsmgr.AddNamespace("d", root.NamespaceURI);

            /* esse If trata o XML novo da Brado */
            if (root.Name.Equals("soap:Envelope"))
            {
                nsmgr.AddNamespace("soap", root.NamespaceURI);

                var xmlBrado = new StringBuilder();
                xmlBrado.Append("<nfeProc xmlns:ns2=\"http://www.w3.org/2000/09/xmldsig#\" xmlns=\"http://www.portalfiscal.inf.br/nfe\" versao=\"3.10\">");

                xmlBrado.Append(
                    (((root.SelectSingleNode("//soap:Body", nsmgr)).FirstChild).
                        FirstChild).InnerXml.ToString());

                xmlBrado.Append("</nfeProc>");

                /* recarrega o XML em mem�ria */
                doc.LoadXml(xmlBrado.ToString());
                root = doc.DocumentElement;
                nsmgr = new XmlNamespaceManager(doc.NameTable);
                nsmgr.AddNamespace("d", root.NamespaceURI);
            }

            var noInfNfe = root.SelectSingleNode("//d:NFe/d:infNFe", nsmgr);

            // Verificar se a NFe � v�lida
            if (noInfNfe != null && noInfNfe.Attributes != null)
            {
                string versaoNfe = noInfNfe.Attributes["versao"] == null
                                       ? string.Empty
                                       : noInfNfe.Attributes["versao"].Value;
                string chaveNfe = noInfNfe != null && noInfNfe.Attributes != null && noInfNfe.Attributes["Id"] != null
                                      ? noInfNfe.Attributes["Id"].Value
                                      : null;

                string noDataEmissao = !string.IsNullOrEmpty(versaoNfe) && versaoNfe.Equals("3.10") ? "dhEmi" : "dEmi";
                string noDataSaida = !string.IsNullOrEmpty(versaoNfe) && versaoNfe.Equals("3.10")
                                         ? "dhSaiEnt"
                                         : "dSaiEnt";

                if (!String.IsNullOrEmpty(chaveNfe) && !String.IsNullOrWhiteSpace(chaveNfe))
                {
                    XmlNode noIde = noInfNfe.SelectSingleNode("//d:ide", nsmgr);
                    string dataEmissao = string.IsNullOrEmpty(ObterTextoNoXml(noIde, "dhEmi", nsmgr)) ? ObterTextoNoXml(noIde, "dEmi", nsmgr) : ObterTextoNoXml(noIde, "dhEmi", nsmgr);
                    string dataSaida = string.IsNullOrEmpty(ObterTextoNoXml(noIde, "dhSaiEnt", nsmgr)) ? ObterTextoNoXml(noIde, "dSaiEnt", nsmgr) : ObterTextoNoXml(noIde, "dhSaiEnt", nsmgr);

                    var nfe = new NotaFiscalEletronica();
                    // DADOS DE IDENTIFICA��O DA NFE
                    nfe.ChaveNfe = chaveNfe.Substring(3);
                    nfe.CodigoUfIbge = Convert.ToInt32(ObterTextoNoXml(noIde, "cUF", nsmgr));
                    nfe.CodigoChaveAcesso = Convert.ToInt32(ObterTextoNoXml(noIde, "cNF", nsmgr));
                    nfe.NaturezaOperacao = ObterTextoNoXml(noIde, "natOp", nsmgr);
                    string indPag = ObterTextoNoXml(noIde, "indPag", nsmgr);
                    if (!string.IsNullOrEmpty(indPag))
                    {
                        nfe.FormaPagamento = this.ObterFormaPagamento(indPag);
                    }

                    nfe.ModeloNota = ObterTextoNoXml(noIde, "mod", nsmgr);
                    nfe.SerieNotaFiscal = ObterTextoNoXml(noIde, "serie", nsmgr);
                    nfe.NumeroNotaFiscal = Convert.ToInt32(ObterTextoNoXml(noIde, "nNF", nsmgr));

                    if (!string.IsNullOrEmpty(dataEmissao))
                    {
                        if (!string.IsNullOrEmpty(versaoNfe) && versaoNfe.Equals("3.10"))
                        {
                            dataEmissao = dataEmissao.Substring(0, 10);
                            nfe.DataEmissao = DateTime.ParseExact(
                                dataEmissao,
                                datePatterns,
                                CultureInfo.InvariantCulture,
                                DateTimeStyles.None);
                        }
                        else
                        {
                            nfe.DataEmissao = DateTime.ParseExact(
                                string.IsNullOrEmpty(ObterTextoNoXml(noIde, "dhEmi", nsmgr)) ? ObterTextoNoXml(noIde, "dEmi", nsmgr) : ObterTextoNoXml(noIde, "dhEmi", nsmgr),
                                datePatterns,
                                CultureInfo.InvariantCulture,
                                DateTimeStyles.None);


                        }
                    }
                    else
                    {
                        dataEmissao = DateTime.Now.ToShortDateString();
                    }

                    if (!string.IsNullOrEmpty(dataSaida))
                    {
                        if (!string.IsNullOrEmpty(versaoNfe) && versaoNfe.Equals("3.10"))
                        {
                            dataSaida = dataSaida.Substring(0, 10);
                            nfe.DataSaida = DateTime.ParseExact(
                                dataSaida,
                                datePatterns,
                                CultureInfo.InvariantCulture,
                                DateTimeStyles.None);
                        }
                        else
                        {
                            string horaSaida = ObterTextoNoXml(noIde, "hSaiEnt", nsmgr);
                            if (!string.IsNullOrEmpty(horaSaida))
                            {
                                dataSaida = string.Concat(dataSaida, " ", horaSaida);
                            }

                            try
                            {
                                dataSaida = dataSaida.Replace("�s", string.Empty);
                                nfe.DataSaida = DateTime.ParseExact(
                                    dataSaida,
                                    datePatterns,
                                    CultureInfo.InvariantCulture,
                                    DateTimeStyles.None);
                            }
                            catch
                            {
                            }
                        }
                    }
                    else
                    {
                        dataSaida = DateTime.Now.ToShortDateString();
                    }

                    nfe.TipoNotaFiscal = this.ObterTipoNotaFiscal(ObterTextoNoXml(noIde, "tpNF", nsmgr));
                    nfe.CodigoIbge = Convert.ToInt32(ObterTextoNoXml(noIde, "cMunFG", nsmgr));
                    nfe.FormatoImpressao = this.ObterFormatoImpressao(ObterTextoNoXml(noIde, "tpImp", nsmgr));
                    nfe.TipoEmissao = this.ObterTipoEmissao(ObterTextoNoXml(noIde, "tpEmis", nsmgr));
                    nfe.DigitoVerificadorChave = string.IsNullOrEmpty(ObterTextoNoXml(noIde, "cDV", nsmgr))
                                                     ? 0
                                                     : Convert.ToInt32(ObterTextoNoXml(noIde, "cDV", nsmgr));
                    nfe.TipoAmbiente = this.ObterTipoAmbiente(ObterTextoNoXml(noIde, "tpAmb", nsmgr));
                    nfe.FinalidadeEmissao = this.ObterFinalidadeEmissao(ObterTextoNoXml(noIde, "finNFe", nsmgr));
                    nfe.ProcessoEmissao = this.ObterProcessoEmissao(ObterTextoNoXml(noIde, "procEmi", nsmgr));
                    nfe.VersaoProcesso = ObterTextoNoXml(noIde, "verProc", nsmgr);

                    // DADOS DE EMITENTE
                    XmlNode noEmit = noInfNfe.SelectSingleNode("//d:emit", nsmgr);
                    DadosEmpresaTemp dadosEmpresaEmitente = this.RecuperarDadosEmpresa(noEmit, true, nsmgr);
                    this.ProcessarDadosEmitente(ref nfe, dadosEmpresaEmitente);

                    // DADOS DE DESTINATARIO
                    XmlNode noDest = noInfNfe.SelectSingleNode("//d:dest", nsmgr);
                    DadosEmpresaTemp dadosEmpresaDestinataria = this.RecuperarDadosEmpresa(noDest, false, nsmgr);
                    this.ProcessarDadosDestinatario(ref nfe, dadosEmpresaDestinataria);

                    // DADOS DE IMPOSTOS DA NFE
                    XmlNode noIcmsTotal = noInfNfe.SelectSingleNode("//d:total/d:ICMSTot", nsmgr);

                    nfe.ValorBaseCalculoIcms = ObterDoubleDoXML(noIcmsTotal, "vBC", nsmgr);
                    nfe.ValorIcms = ObterDoubleDoXML(noIcmsTotal, "vICMS", nsmgr);
                    nfe.ValorBaseCalculoSubTributaria = ObterDoubleDoXML(noIcmsTotal, "vBCST", nsmgr);
                    nfe.ValorSubTributaria = ObterDoubleDoXML(noIcmsTotal, "vST", nsmgr);
                    nfe.ValorProduto = ObterDoubleDoXML(noIcmsTotal, "vProd", nsmgr);
                    nfe.ValorTotalFrete = ObterDoubleDoXML(noIcmsTotal, "vFrete", nsmgr);
                    nfe.ValorSeguro = ObterDoubleDoXML(noIcmsTotal, "vSeg", nsmgr);
                    nfe.ValorDesconto = ObterDoubleDoXML(noIcmsTotal, "vDesc", nsmgr);
                    nfe.ValorImpostoImportacao = ObterDoubleDoXML(noIcmsTotal, "vII", nsmgr);
                    nfe.ValorIpi = ObterDoubleDoXML(noIcmsTotal, "vIPI", nsmgr);
                    nfe.ValorPis = ObterDoubleDoXML(noIcmsTotal, "vPIS", nsmgr);
                    nfe.ValorCofins = ObterDoubleDoXML(noIcmsTotal, "vCOFINS", nsmgr);
                    nfe.ValorOutro = ObterDoubleDoXML(noIcmsTotal, "vOutro", nsmgr);
                    nfe.Valor = ObterDoubleDoXML(noIcmsTotal, "vNF", nsmgr);
                    nfe.Volume = 0;

                    // DADOS DE TRANSPORTE
                    XmlNode noTransp = noInfNfe.SelectSingleNode("//d:transp", nsmgr);
                    var docTransp = new XmlDocument();
                    docTransp.LoadXml(noTransp.OuterXml);
                    string modFrete = ObterTextoNoXml(docTransp, "modFrete", nsmgr);
                    modFrete = string.IsNullOrEmpty(modFrete) || string.IsNullOrWhiteSpace(modFrete) ? "0" : modFrete;
                    nfe.ModeloFrete = this.ObterModeloFrete(modFrete);

                    XmlNode noTransportadora = docTransp.SelectSingleNode("//d:transporta", nsmgr);
                    if (noTransportadora != null)
                    {
                        string cnpj = ObterTextoNoXml(noTransportadora, "CNPJ", nsmgr);
                        if (!string.IsNullOrEmpty(cnpj))
                        {
                            nfe.CnpjTransportadora = cnpj;
                        }

                        nfe.NomeTransportadora = ObterTextoNoXml(noTransportadora, "xNome", nsmgr);
                        nfe.EnderecoTransportadora = ObterTextoNoXml(noTransportadora, "xEnder", nsmgr);
                        nfe.MunicipioTransportadora = ObterTextoNoXml(noTransportadora, "xMun", nsmgr);
                        nfe.UfTransportadora = ObterTextoNoXml(noTransportadora, "UF", nsmgr);
                    }

                    XmlNode noVeicTransp = docTransp.SelectSingleNode("//d:veicTransp", nsmgr);
                    if (noVeicTransp != null)
                    {
                        nfe.Placa = ObterTextoNoXml(noVeicTransp, "placa", nsmgr);
                    }

                    nfe.CanceladoEmitente = false;
                    nfe.DataHoraGravacao = DateTime.Now;

                    // DADOS DO LOCAL DA RETIRADA
                    var noRetirada = noInfNfe.SelectSingleNode("//d:retirada", nsmgr);
                    if (noRetirada != null)
                    {
                        nfe.CnpjRetirada = ObterTextoNoXml(noRetirada, "CNPJ", nsmgr);
                        nfe.CpfRetirada = ObterTextoNoXml(noRetirada, "CPF", nsmgr);
                        nfe.LogradouroRetirada = ObterTextoNoXml(noRetirada, "xLgr", nsmgr);
                        nfe.NumeroRetirada = ObterTextoNoXml(noRetirada, "nro", nsmgr);
                        nfe.ComplementoRetirada = ObterTextoNoXml(noRetirada, "xCpl", nsmgr).Left(50);
                        nfe.BairroRetirada = ObterTextoNoXml(noRetirada, "xBairro", nsmgr);
                        nfe.CodigoMunicipioRetirada = ObterTextoNoXml(noRetirada, "cMun", nsmgr);
                        nfe.MunicipioRetirada = ObterTextoNoXml(noRetirada, "xMun", nsmgr);
                        nfe.UfRetirada = ObterTextoNoXml(noRetirada, "UF", nsmgr);
                    }

                    // DADOS DO LOCAL DA DESCARGA
                    var noEntrega = noInfNfe.SelectSingleNode("//d:entrega", nsmgr);
                    if (noEntrega != null)
                    {
                        nfe.CnpjEntrega = ObterTextoNoXml(noEntrega, "CNPJ", nsmgr);
                        nfe.CpfEntrega = ObterTextoNoXml(noEntrega, "CPF", nsmgr);
                        nfe.LogradouroEntrega = ObterTextoNoXml(noEntrega, "xLgr", nsmgr);
                        nfe.NumeroEntrega = ObterTextoNoXml(noEntrega, "nro", nsmgr);
                        nfe.ComplementoEntrega = ObterTextoNoXml(noEntrega, "xCpl", nsmgr);
                        nfe.BairroEntrega = ObterTextoNoXml(noEntrega, "xBairro", nsmgr);
                        nfe.CodigoMunicipioEntrega = ObterTextoNoXml(noEntrega, "cMun", nsmgr);
                        nfe.MunicipioEntrega = ObterTextoNoXml(noEntrega, "xMun", nsmgr);
                        nfe.UfEntrega = ObterTextoNoXml(noEntrega, "UF", nsmgr);
                    }

                    Conv.TrimObject(nfe);
                    return nfe;
                }
            }

            return null;
        }

        /// <summary>
        ///     Processa os produtos lidos no xml do Edi
        /// </summary>
        /// <param name="nfeEdi">NF do Edi realizada a leitura</param>
        /// <param name="listaProdutos">Lista de Produtos</param>
        /// <returns>Lista de produtos processados</returns>
        public IList<NfeProdutoEdi> ProcessarProdutosNfeEdi(
            NfeEdi nfeEdi,
            IEnumerable<INotaFiscalEletronicaProduto> listaProdutos)
        {
            var list = new List<NfeProdutoEdi>();

            double pesoTotalNota = 0;

            if (listaProdutos != null && listaProdutos.Count() > 0)
            {
                int i = 0;

                foreach (INotaFiscalEletronicaProduto produto in listaProdutos)
                {
                    var nfeProduto = new NfeProdutoEdi((NotaFiscalEletronicaProduto)produto);
                    nfeProduto.NfeEdi = nfeEdi;
                    //pesoTotalNota += nfeProduto.QuantidadeComercial;
                    nfeProduto.DataHoraGravacao = DateTime.Now;

                    if (nfeProduto.UnidadeComercial == "PC")
                    {
                        pesoTotalNota += 0;
                    }
                    else if (nfeProduto.UnidadeComercial == "S50")
                    {
                        pesoTotalNota += nfeProduto.QuantidadeComercial * 50;
                    }
                    else if (nfeProduto.UnidadeComercial == "S60")
                    {
                        pesoTotalNota += nfeProduto.QuantidadeComercial * 60;
                    }
                    else
                    {
                        pesoTotalNota += nfeProduto.QuantidadeComercial;
                    }

                    if (nfeEdi.ListaProdutos != null && nfeEdi.ListaProdutos.Count > 0)
                    {
                        var prod =
                            (NotaFiscalEletronicaProduto)
                            this._nfeProdutoSimconsultasRepository.ObterPorId(nfeEdi.ListaProdutos[i].Id);

                        this.ParseProdutosNfes(nfeProduto, ref prod);

                        this._nfeProdutoEdiRepository.Atualizar(nfeProduto);
                    }
                    else
                    {
                        this._nfeProdutoEdiRepository.Inserir(nfeProduto);
                    }

                    list.Add(nfeProduto);
                    i++;
                }

                nfeEdi.Peso = pesoTotalNota;
                nfeEdi.PesoBruto = pesoTotalNota;
            }

            if (this.VerificarPorUnidadeComercial(listaProdutos))
            {
                nfeEdi.Peso = 0;
                nfeEdi.PesoBruto = 0;
            }

            return list;
        }

        /// <summary>
        ///     Obt�m os produtos da NFE
        /// </summary>
        /// <param name="listaProdutos">Lista de produtos no xml</param>
        /// <param name="nsmgr">Namespace do xml</param>
        /// <returns>Lista de produtos realizada parser</returns>
        public IEnumerable<INotaFiscalEletronicaProduto> ProcessarProdutosNfeXml(
            XmlNodeList listaProdutos,
            XmlNamespaceManager nsmgr)
        {
            var listaDeProdutosNoXml = new List<INotaFiscalEletronicaProduto>();

            foreach (XmlNode produto in listaProdutos)
            {
                XmlNode noProduto = produto.SelectSingleNode(".//d:prod", nsmgr);

                listaDeProdutosNoXml.Add(
                    new NotaFiscalEletronicaProduto
                    {
                        CodigoProduto = ObterTextoNoXml(noProduto, "cProd", nsmgr),
                        DescricaoProduto = ObterTextoNoXml(noProduto, "xProd", nsmgr),
                        CodigoNcm = ObterTextoNoXml(noProduto, "NCM", nsmgr),
                        Cfop = ObterTextoNoXml(noProduto, "CFOP", nsmgr),
                        UnidadeComercial = ObterTextoNoXml(noProduto, "uCom", nsmgr),
                        QuantidadeComercial =
                            Convert.ToDouble(
                                ObterTextoNoXml(noProduto, "qCom", nsmgr),
                                CultureInfo.GetCultureInfo("en-US")),
                        ValorUnitarioComercializacao =
                            this.ParseDoubleField(
                                ObterTextoNoXml(noProduto, "vUnCom", nsmgr)),
                        ValorProduto =
                            this.ParseDoubleField(
                                ObterTextoNoXml(noProduto, "vProd", nsmgr)),
                        UnidadeTributavel = ObterTextoNoXml(noProduto, "uTrib", nsmgr),
                        QuantidadeTributavel =
                            Convert.ToDouble(
                                ObterTextoNoXml(noProduto, "qTrib", nsmgr),
                                CultureInfo.GetCultureInfo("en-US")),
                        ValorUnitarioTributacao =
                            this.ParseDoubleField(
                                ObterTextoNoXml(noProduto, "vUnTrib", nsmgr)),
                        DataHoraGravacao = DateTime.Now
                    });
            }

            return listaDeProdutosNoXml;
        }

        /// <summary>
        ///     Processa os dados da NFe
        /// </summary>
        /// <param name="dadosNfe">Dados retornados da NFe</param>
        /// <param name="chaveNfe">Chave da Nfe</param>
        /// <param name="origem"> Origem da consulta</param>
        /// <param name="notasBrado"> Indica Notas Clientes Brado</param>
        /// <returns> Objeto de StatusNfeDto </returns>
        [Transaction]
        public virtual StatusNfe ProcessarRetornoSimconsultas(
            RetornoSimconsultas dadosNfe,
            string chaveNfe,
            TelaProcessamentoNfe origem,
            bool notasBrado)
        {
            string codigoErro = string.Empty;
            string mensagemErro = string.Empty;
            double peso_bruto = 0;
            NfeSimconsultas nfe = null;
            var doc = new XmlDocument();
            doc.LoadXml(dadosNfe.Nfe.OuterXml);

            if (doc.DocumentElement != null)
            {
                if (doc.DocumentElement.Name.Equals("NFe") || doc.DocumentElement.Name.ToUpper().Equals("NFEPROC"))
                {
                    /*if (!ValidarXml(doc, out mensagemErro))
                    {
                        return InserirStatusNfe(chaveNfe, "V05", 0, 0, 0, 0);
                    }*/
                    try
                    {
                        nfe = this.ProcessarDadosNfeSimConsultas(doc, notasBrado, ref peso_bruto);
                        codigoErro = "000";
                    }
                    catch (NfeException ex)
                    {
                        Sis.LogException(ex, "Erro em ProcessarRetornoSimconsultas - CodigoErro: S99");
                        codigoErro = "S999";
                        mensagemErro = ex.Message;
                    }
                    catch (Exception ex)
                    {
                        Sis.LogException(ex, "Erro em ProcessarRetornoSimconsultas - CodigoErro: S999");
                        codigoErro = "S999";
                    }
                }

                if (doc.DocumentElement.Name.Equals("InfConsulta"))
                {
                    XmlElement root = doc.DocumentElement;
                    var nsmgr = new XmlNamespaceManager(doc.NameTable);
                    nsmgr.AddNamespace("d", root.NamespaceURI);

                    XmlNode noCodigo = root.SelectSingleNode("//d:InfConsulta/d:StatusConsulta", nsmgr);
                    codigoErro = noCodigo.InnerText;

                    int codigoRetornoTmp;
                    if (!int.TryParse(codigoErro, out codigoRetornoTmp))
                    {
                        codigoErro = "S999";
                    }

                    XmlNode noDescricao = root.SelectSingleNode("//d:InfConsulta/d:StatusDescricao", nsmgr);
                    mensagemErro = noDescricao.InnerText;
                }
            }


            StatusRetornoNfe statusRetorno = this._statusRetornoNfeRepository.ObterPorCodigo(codigoErro);
            if (statusRetorno == null)
            {
                statusRetorno = new StatusRetornoNfe
                {
                    Codigo = codigoErro,
                    DataCadastro = DateTime.Now,
                    IndPermiteReenvioTela = false,
                    IndPermiteReenvioBo = false,
                    IndLiberarDigitacao = true,
                    Mensagem = mensagemErro
                };
                this._statusRetornoNfeRepository.Inserir(statusRetorno);
            }

            SpdT.StatusNfe statusNfe = this.GravarLogStatusNfe(nfe, statusRetorno, chaveNfe, origem, notasBrado, ref peso_bruto);

            /* if (statusNfe.Peso == 0 && statusNfe.PesoUtilizado == 0 && statusNfe.Volume == 0 && statusNfe.VolumeUtilizado == 0)
            {
                throw new Exception("Erro ao obter dados da NFe.");
            } */

            return _statusNfeRepository.ObterPorId(statusNfe.IdStatusNfe.ToInt32());
            // return statusNfe;
            // return new StatusNfeDto { CodigoStatusRetorno = statusRetorno.Codigo, ObtidaComSucesso = false, NotaFiscalEletronica = null, MensagemErro = statusRetorno.Mensagem, IndPermiteReenvio = statusRetorno.IndPermiteReenvio };
        }

        /// <summary>
        ///     Rollback da Nfe
        /// </summary>
        /// <param name="idHistoricoNfe">id do historico de referencia</param>
        /// <param name="matriculaUsuario">usu�rio que est� fazendo o rollback</param>
        [Transaction]
        public virtual void RestaurarVolumePesoNfe(int idHistoricoNfe, string matriculaUsuario)
        {
            HistoricoNfe historicoNfeOriginal = this._historicoNfeRepository.ObterPorId(idHistoricoNfe);
            string chaveNfe = historicoNfeOriginal.ChaveNfe;
            double? novoPeso = historicoNfeOriginal.PesoAntigo;
            double? novoVolume = historicoNfeOriginal.VolumeAntigo;
            string origem = historicoNfeOriginal.Origem;

            NfeSimconsultas nfeSimconsultas = this._nfeSimconsultasRepository.ObterPorChaveNfe(chaveNfe);
            StatusNfe statusNfe = this._statusNfeRepository.ObterPorChaveNfe(chaveNfe);

            var historicoNfe = new HistoricoNfe
            {
                ChaveNfe = chaveNfe,
                DataAlteracao = DateTime.Now,
                MatriculaUsuario = matriculaUsuario,
                NaturezaDaOperacao = NaturezaDaOperacaoEnum.Restaurado,
                Origem = origem,
                StatusEnvioEmail = StatusEnvioEmailEnum.NaoEnviado,
                PesoAntigo = nfeSimconsultas.PesoBruto,
                VolumeAntigo = nfeSimconsultas.Volume,
                PesoAtual = novoPeso ?? 0,
                VolumeAtual = novoVolume ?? 0
            };

            nfeSimconsultas.PesoBruto = novoPeso ?? 0;
            nfeSimconsultas.Volume = novoVolume;

            statusNfe.Peso = novoPeso ?? 0;
            statusNfe.Volume = novoVolume ?? 0;

            this._historicoNfeRepository.InserirOuAtualizar(historicoNfe);
            this._nfeSimconsultasRepository.InserirOuAtualizar(nfeSimconsultas);
            this._statusNfeRepository.InserirOuAtualizar(statusNfe);

            this.EnviarEmailAlteracaoNfe(historicoNfe);
        }

        /// <summary>
        ///     Valida a chave NFe
        /// </summary>
        /// <param name="chaveNfe"> The chave nfe. </param>
        /// <param name="origem">Origem da consulta</param>
        public void ValidarChaveNfe(string chaveNfe, TelaProcessamentoNfe origem)
        {
            StatusNfe statusNfeErro;
            int[] pesos =
            {
                4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2, 9, 8, 7, 6,
                5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2
            };
            if (string.IsNullOrEmpty(chaveNfe))
            {
                statusNfeErro = this.InserirStatusNfe(chaveNfe, "V01", 0, 0, 0, 0, origem, 0);
                throw new TranslogicValidationException(statusNfeErro.StatusRetornoNfe.Mensagem);
            }

            if (chaveNfe.Length != 44)
            {
                statusNfeErro = this.InserirStatusNfe(chaveNfe, "V02", 0, 0, 0, 0, origem, 0);
                throw new TranslogicValidationException(
                    string.Format(statusNfeErro.StatusRetornoNfe.Mensagem, chaveNfe));
            }

            int somatorio = 0;
            for (int i = 0; i <= chaveNfe.Length - 2; i++)
            {
                int digito;
                try
                {
                    digito = int.Parse(chaveNfe[i].ToString());
                }
                catch (Exception)
                {
                    statusNfeErro = this.InserirStatusNfe(chaveNfe, "V04", 0, 0, 0, 0, origem, 0);
                    throw new TranslogicValidationException(
                        string.Format(statusNfeErro.StatusRetornoNfe.Mensagem, chaveNfe));
                }

                somatorio += digito * pesos[i];
            }

            int idEstado;
            if (!int.TryParse(chaveNfe.Substring(0, 2), out idEstado))
            {
                statusNfeErro = this.InserirStatusNfe(chaveNfe, "V02", 0, 0, 0, 0, origem, 0);
                throw new TranslogicValidationException(
                    string.Format(statusNfeErro.StatusRetornoNfe.Mensagem, chaveNfe));
            }

            UfIbge estado = this._estadoIbgeRepository.ObterPorId(idEstado);
            if (estado == null)
            {
                statusNfeErro = this.InserirStatusNfe(chaveNfe, "V03", 0, 0, 0, 0, origem, 0);
                throw new TranslogicValidationException(
                    string.Format(statusNfeErro.StatusRetornoNfe.Mensagem, chaveNfe));
            }

            int modeloNota;
            if (int.TryParse(chaveNfe.Substring(20, 2), out modeloNota))
            {
                if (modeloNota != 55)
                {
                    statusNfeErro = this.InserirStatusNfe(chaveNfe, "55", 0, 0, 0, 0, origem, 0);
                    throw new TranslogicValidationException(
                        string.Format(statusNfeErro.StatusRetornoNfe.Mensagem, chaveNfe));
                }
            }

            int ano, mes;
            if (int.TryParse(chaveNfe.Substring(2, 2), out ano) && int.TryParse(chaveNfe.Substring(4, 2), out mes))
            {
                if (mes <= 0 || mes >= 13)
                {
                    statusNfeErro = this.InserirStatusNfe(chaveNfe, "V02", 0, 0, 0, 0, origem, 0);
                    throw new TranslogicValidationException(
                        string.Format(statusNfeErro.StatusRetornoNfe.Mensagem, chaveNfe));
                }

                ano += 2000;
                var dataChave = new DateTime(ano, mes, 1);
                TimeSpan timestamp = DateTime.Now - dataChave;

                ConfiguracaoTranslogic configuracao =
                    this._configuracaoTranslogicRepository.ObterPorId("LIBERACAO_DT_180_CGTO_ERRO_V07");

                if (configuracao.Valor == "N" && timestamp.Days > 210)
                {
                    this.InserirStatusNfe(chaveNfe, "31", 0, 0, 0, 0, origem, 0);
                    StatusRetornoNfe statusRetorno = this._statusRetornoNfeRepository.ObterPorCodigo("31");
                    throw new TranslogicValidationException(string.Format(statusRetorno.Mensagem, chaveNfe));
                }
            }

            int mod = somatorio % 11;
            int dv = 11 - mod;
            if (mod.Equals(0) || mod.Equals(1))
            {
                dv = 0;
            }

            string stringUltimoDigito = chaveNfe.Substring(43);
            int ultimoDigito;
            int.TryParse(stringUltimoDigito, out ultimoDigito);
            if (!ultimoDigito.Equals(dv))
            {
                statusNfeErro = this.InserirStatusNfe(chaveNfe, "25", 0, 0, 0, 0, origem, 0);
                throw new TranslogicValidationException(
                    string.Format(statusNfeErro.StatusRetornoNfe.Mensagem, chaveNfe));
            }
        }

        /// <summary>
        ///     Valida a estrutura do XMl
        /// </summary>
        /// <param name="xmlDocument"> Xml a ser validado </param>
        /// <param name="erros"> The erros de valida��o. </param>
        /// <returns> Retorna true se a estrutura do xml estiver valida. </returns>
        public bool ValidarXml(XmlDocument xmlDocument, out string erros)
        {
            this._errosValidacaoXml = new StringBuilder();
            // xmlDocument = new XmlDocument();
            // xmlDocument.Load(@"C:\Temp\NFE.xml");

            // Remove o No LogTrace para validar o xml
            this.RemoveNoLogTrace(xmlDocument);

            // xmlDocument.ReadNode()
            // Define o tipo de valida��o
            var settings = new XmlReaderSettings();
            settings.ValidationType = ValidationType.Schema;

            // Carrega o arquivo de esquema
            var schemas = new XmlSchemaSet();
            settings.Schemas = schemas;

            // Carrega o arquivo Xsd do assembly
            // Quando carregar o eschema, especificar o namespace que ele valida
            // e a localiza��o do arquivo 
            // arquivo Base da Nfe 2.0 que possui includo arquivo leiauteNFe_v2.00.xsd.
            Stream arquivoXsd = this.LoadFileFromResouce("Translogic.Modules.Core.Resources.xsd.nfe_v2.00.xsd");
            schemas.Add(null, XmlReader.Create(arquivoXsd));

            // arquivo com o layout da Nfe 2.0 que possui include arquivo xmldsig-core-schema_v1.01.xsd e tiposBasico_v1.03.xsd.
            arquivoXsd = this.LoadFileFromResouce("Translogic.Modules.Core.Resources.xsd.leiauteNFe_v2.00.xsd");
            schemas.Add(null, XmlReader.Create(arquivoXsd));

            // arquivo com a estrutura da Nfe 2.0.
            arquivoXsd = this.LoadFileFromResouce("Translogic.Modules.Core.Resources.xsd.xmldsig-core-schema_v1.01.xsd");
            schemas.Add(null, XmlReader.Create(arquivoXsd));

            // arquivo com os tipos complexos criados na estrutura.
            arquivoXsd = this.LoadFileFromResouce("Translogic.Modules.Core.Resources.xsd.tiposBasico_v1.03.xsd");
            schemas.Add(null, XmlReader.Create(arquivoXsd));

            // Especifica o tratamento de evento para os erros de validacao
            settings.ValidationEventHandler += this.ValidationEventHandler;

            // cria um leitor para valida��o
            XmlReader xmlReader = XmlReader.Create(new StringReader(xmlDocument.InnerXml), settings);

            try
            {
                // Faz a leitura de todos os dados XML
                while (xmlReader.Read())
                {
                }
            }
            catch (XmlException err)
            {
                // Um erro ocorre se o documento XML inclui caracteres ilegais
                // ou tags que n�o est�o aninhadas corretamente
                if (this._errosValidacaoXml == null)
                {
                    this._errosValidacaoXml = new StringBuilder();
                }

                this._errosValidacaoXml.AppendLine("Ocorreu um erro critico durante a validacao XML.");
                this._errosValidacaoXml.AppendLine("Erro: " + err.Message);
            }
            finally
            {
                xmlReader.Close();
            }

            erros = this._errosValidacaoXml.ToString();

            return this._errosValidacaoXml.Length.Equals(0);
        }

        /// <summary>
        ///     Verifica existe saldo na NF-e
        /// </summary>
        /// <param name="statusNfeDto">Objeto StatusNfeDto</param>
        /// <param name="fluxoComercial">Objeto fluxo Comercial</param>
        /// <returns>Objeto StatusNfeDto validado</returns>
        public StatusNfeDto VerificarExisteSaldoNfe(StatusNfeDto statusNfeDto, FluxoComercial fluxoComercial)
        {
            bool indVerificarSaldo = false;
            double margemPercentual = 0;
            bool indFluxoVolume = false;
            ConfiguracaoTranslogic config = this._configuracaoTranslogicRepository.ObterPorId("NFE_TRAVA_SALDO");
            if (config != null)
            {
                indVerificarSaldo = config.Valor.Equals("S");
            }

            config = this._configuracaoTranslogicRepository.ObterPorId("NFE_TRAVA_SALDO_PORCENTAGEM");
            if (config != null)
            {
                margemPercentual = double.Parse(config.Valor);
            }

            // Verifica se o Fluxo � de volume 
            indFluxoVolume = fluxoComercial.Contrato.CodigoUnidadeMedida.Equals("M3");

            if (indVerificarSaldo)
            {
                bool saldoZerado = false;
                string tipoValor = string.Empty;

                if (indFluxoVolume)
                {
                    if ((statusNfeDto.Volume + (statusNfeDto.Volume * (margemPercentual / 100)))
                        <= statusNfeDto.VolumeUtilizado)
                    {
                        saldoZerado = true;
                        tipoValor = "volume";
                    }
                }

                if (!indFluxoVolume)
                {
                    if ((statusNfeDto.Peso + (statusNfeDto.Peso * (margemPercentual / 100)))
                        <= statusNfeDto.PesoUtilizado)
                    {
                        saldoZerado = true;
                        tipoValor = "peso";
                    }
                }

                if (saldoZerado)
                {
                    string mensagemErro =
                        string.Format(
                            "N�o � poss�vel utilizar a NF-e({0}), pois todo o {1} j� foi utilizado.",
                            statusNfeDto.NotaFiscalEletronica.ChaveNfe,
                            tipoValor);
                    return new StatusNfeDto
                    {
                        Peso = statusNfeDto.Peso,
                        Volume = statusNfeDto.Volume,
                        PesoBruto = statusNfeDto.PesoBruto,
                        PesoUtilizado = statusNfeDto.PesoUtilizado,
                        VolumeUtilizado = statusNfeDto.VolumeUtilizado,
                        IndLiberaDigitacao = false,
                        ObtidaComSucesso = false,
                        IndPermiteReenvio = false,
                        NotaFiscalEletronica = null,
                        MensagemErro = mensagemErro,
                        StackTrace = string.Empty
                    };
                }
            }

            return new StatusNfeDto
            {
                Peso = statusNfeDto.Peso,
                Volume = statusNfeDto.Volume,
                PesoBruto = statusNfeDto.PesoBruto,
                PesoUtilizado = statusNfeDto.PesoUtilizado,
                VolumeUtilizado = statusNfeDto.VolumeUtilizado,
                ObtidaComSucesso = true,
                IndPermiteReenvio = false,
                NotaFiscalEletronica = statusNfeDto.NotaFiscalEletronica,
                MensagemErro = string.Empty,
                StackTrace = string.Empty
            };
        }

        /// <summary>
        ///     Verifica se um determinado usu�rio
        /// </summary>
        /// <param name="usuario">Usuario a ser verificado</param>
        /// <returns>Valor booleano</returns>
        public bool VerificarUsuarioPodeAlterarPesoNfe(Usuario usuario)
        {
            IList<GrupoUsuario> listaGrupos = this._agrupamentoUsuarioRepository.ObterGruposPorUsuario(usuario);
            if (listaGrupos == null || listaGrupos.Count == 0)
            {
                return false;
            }

            IEnumerable<GrupoUsuario> grupo =
                listaGrupos.Where(c => c.Codigo.Equals("CTE_ALT") || c.Codigo.Equals("MASTE"));
            return grupo.Any();
        }

        /// <summary>
        ///     Zerar Saldo do status da nfe
        /// </summary>
        /// <param name="usuario">usuario que esta fazendo a operacao</param>
        /// <param name="chaves">chaves para zerar</param>
        [Transaction]
        public virtual void ZerarSaldo(Usuario usuario, string autorizadoPor, string justificativa, params string[] chaves)
        {
            foreach (string chave in chaves)
            {
                StatusNfe statusNfe = this._statusNfeRepository.ObterPorChaveNfe(chave);

                if (statusNfe == null)
                {
                    continue;
                }

                var log = new StatusNfeLog
                {
                    VersionDate = DateTime.Now,
                    Usuario = usuario,
                    ChaveNfe = statusNfe.ChaveNfe,
                    PesoUtilizadoAntigo = statusNfe.PesoUtilizado,
                    VolumeUtilizadoAntigo = statusNfe.VolumeUtilizado,
                    StatusNfe = statusNfe
                };

                statusNfe.PesoUtilizado = 0;
                statusNfe.VolumeUtilizado = 0;

                this._statusNfeRepository.InserirOuAtualizar(statusNfe);
                this._statusNfeLogRepository.InserirOuAtualizar(log);

                //
                // Insere na tabela de estorno de saldo de nfe (Nova tabela a pedido da Priscila)
                // Motivo: registrar a justificativa e o nome de quem autorizou o estorno de saldo
                //
                var estornoSaldoNfe = new NfeEstornoSaldo
                {
                    ChaveNfe = chave,
                    Justificativa = justificativa,
                    Aprovador = autorizadoPor,
                    PesoUtilizado = 0,
                    PesoEstornado = statusNfe.Peso, // Liberou/Estornou o valor cheio correspondente ao peso da nfe, pois o peso utilizado ficou 0
                    VolumeUtilizado = 0,
                    VolumeEstornado = statusNfe.Volume, // Liberou/Estornou o valor cheio correspondente ao volume da nfe, pois o volume utilizado ficou 0
                    DataEstorno = DateTime.Now,
                    UsuarioEstorno = usuario,
                    TransacaoEstorno = "ZERARSALDONFE"
                };
                this._estornoSaldoRepository.InserirOuAtualizar(estornoSaldoNfe);
            }
        }

        /// <summary>
        ///     Verifica se um determinado usu�rio possui permiss�o de estorno de saldo da nfe
        /// </summary>
        /// <param name="usuario">Usuario a ser verificado</param>
        /// <returns>Valor booleano</returns>
        public bool VerificarUsuarioPossuiPermissaoEstornoSaldoNfe(Usuario usuario)
        {
            IList<GrupoUsuario> listaGrupos = this._agrupamentoUsuarioRepository.ObterGruposPorUsuario(usuario);
            if (listaGrupos != null)
            {
                if (listaGrupos.Count > 0)
                {
                    GrupoUsuario grupoMaster = listaGrupos.Where(c => c.Codigo.Equals("MASTE")).FirstOrDefault();
                    if (grupoMaster != null)
                    {
                        if (grupoMaster.Id > 0)
                        {
                            return true;
                        }
                    }
                }
            }

            var permissaoEstornoSaldo = _usuarioRepository.ObterPermissoes("ESTORNOSALDONOTA", usuario);
            if (permissaoEstornoSaldo != null)
            {
                if (permissaoEstornoSaldo.Count > 0)
                {
                    object[] permissaoObj = (object[])permissaoEstornoSaldo[0];
                    if (permissaoObj[1] != null)
                        return true;
                }
            }

            return false;
        }

        /// <summary>
        ///     Salva o PDF dos emails para Shirata
        /// </summary>
        /// <param name="docPdf">O documento pdf</param>
        public void SalvarPdfNfeEdi(byte[] docPdf)
        {
            var pdfValido = Tools.VerificaPdfValido(docPdf);
            if (pdfValido == false)
            {
                throw new Exception("PDF Inv�lido");
            }

            // Buscando a chave da NFe dentro do PDF
            var chaveNfe = Tools.BuscarChaveNfeDoArquivoPDF(docPdf);
            string chaveNfeValida = null;
            NfePdfEdi arquivoPdf = null;

            SpdT.VwNfe nfe = null;
            int? ifNfe = null;
            int? ifNfeSco = null;

            if (!String.IsNullOrEmpty(chaveNfe))
            {
                nfe = BL_VwNfe.ObterPorChaveNfe(chaveNfe);

                if (nfe != null)
                {
                    if (nfe.Id.HasValue && nfe.Id.Value != 0)
                    {
                        chaveNfeValida = chaveNfe;
                        switch (nfe.Tipo)
                        {
                            case "SCO":
                                // Procurando pelo PDF na coluna do SimConsultas
                                ifNfeSco = nfe.Id.ToInt32();
                                arquivoPdf = this._nfePdfEdiRepository.ObterPorNfeSimConsultasId((int)nfe.Id);
                                break;
                            case "EDI":
                                // Procurando pelo PDF na coluna do EDI
                                ifNfe = nfe.Id.ToInt32();
                                arquivoPdf = this._nfePdfEdiRepository.ObterPorNfeId((int)nfe.Id);
                                // var pdf = Translogic.Modules.Core.Spd.Dbs.ExecuteEdi((db) => BL_Edi2NfePdf.SelectSingle(db, new SpdE.Edi2NfePdf { NfeIdNfe = nfe.Id }));
                                break;
                        }
                    }
                }
            }
            else
            {
                throw new Exception("ChaveNfe n�o encontrada no arquivo");
                // throw new Exception("ChaveNfe n�o encontrada em VW_NFE: " + chaveNfe);
            }

            // Se foi encontrado o PDF, devemos atualizar o PDF do registro
            if (arquivoPdf != null)
            {
                arquivoPdf.Pdf = docPdf;
                arquivoPdf.NfeChave = chaveNfeValida;
                this._nfePdfEdiRepository.Atualizar(arquivoPdf);
            }
            // N�o foi encontrado o registro, ent�o inserimos um novo registro
            else
            {
                arquivoPdf = new NfePdfEdi()
                {
                    Id_Nfe = ifNfe.GetValueOrDefault(),
                    IdNfeSimConsultas = ifNfeSco,
                    Data = DateTime.Now,
                    Pdf = docPdf,
                    NfeChave = chaveNfeValida
                };

                this._nfePdfEdiRepository.Inserir(arquivoPdf);
            }
        }

        /// <summary>
        ///      Obt�m os fluxos associados aos dados da nota fiscal (remetente e destinatario fiscal)
        /// </summary>
        /// <param name="chaveNfe">Chave da Nfe</param>
        /// <returns> Retorna lista de Objetos do tipo NotaFiscalFluxoAssociacaoDto </returns>
        public IList<NotaFiscalFluxoAssociacaoDto> ObterFluxosAssociadosChaveNfe(DetalhesPaginacaoWeb pagination,
                                                                                 string listaChavesNfe,
                                                                                 bool somenteVigentes)
        {
            var chavesNfeLista =
                listaChavesNfe.ToUpper()
                    .Trim()
                    .Replace(" ", string.Empty)
                    .Replace(",", ";")
                    .Replace(".", ";")
                    .Split(';')
                    .Where(e => !string.IsNullOrWhiteSpace(e))
                    .ToList();

            return this._nfeReadonlyRepository.ObterFluxosAssociadosChaveNfe(pagination,
                                                                             chavesNfeLista,
                                                                             somenteVigentes);
        }

        #endregion

        #region Methods

        private void ApagarNotaNaoFaturada(string chaveNfe, ref StatusNfe nfe)
        {
            FaturamentosNfeDto faturamentos = this._statusNfeRepository.VerificarQuantidadeFaturamento(chaveNfe);

            if (faturamentos.FaturamentosCte == 0 && faturamentos.FaturamentosFiscais == 0)
            {
                this._statusNfeRepository.ApagarRegistrosNFe(chaveNfe);
                nfe = null;
            }
        }

        private void EnviarEmailAlteracaoNfe(HistoricoNfe historicoNfe)
        {
            List<string> destinatarios = this._historicoNfeRepository.ObterListaDeEmailsQuandoHaAlteracao();

            try
            {
                if (destinatarios != null && destinatarios.Count > 0)
                {
                    string body =
                        @"
                                <p>Houve altera��o na NFe [chave] feita pelo usu�rio de matr�cula [matricula] �s [dataHora].</p>
                                <p>Peso Antigo: [pesoAntigo] -> Novo Peso: [novoPeso]</p>
                                <p>Volume Antigo: [volumeAntigo] -> Novo Volume: [volumePeso]</p>
                                <p>([naturezaOperacao])</p>   
                                ".Replace("[chave]", historicoNfe.ChaveNfe)
                            .Replace("[matricula]", historicoNfe.MatriculaUsuario)
                            .Replace("[dataHora]", historicoNfe.DataAlteracao.ToString())
                            .Replace("[pesoAntigo]", historicoNfe.PesoAntigo.ToString())
                            .Replace("[novoPeso]", historicoNfe.PesoAtual.ToString())
                            .Replace("[volumeAntigo]", historicoNfe.VolumeAntigo.ToString())
                            .Replace("[volumePeso]", historicoNfe.VolumeAtual.ToString())
                            .Replace(
                                "[naturezaOperacao]",
                                historicoNfe.NaturezaDaOperacao == NaturezaDaOperacaoEnum.Manual
                                    ? "Alterado Manualmente"
                                    : "Valor Restaurado");

                    ConfiguracaoTranslogic configuracaoTranslogic =
                        this._configuracaoTranslogicRepository.ObterPorId("EDI_EMAIL_REMETENTE");

                    var mailMessage = new MailMessage();
                    mailMessage.From = new MailAddress("noreply@all-logistica.com");
                    mailMessage.IsBodyHtml = true;
                    mailMessage.Body = body;
                    mailMessage.Subject = "Altera��o de NFe";
                    mailMessage.SubjectEncoding = Encoding.GetEncoding("ISO-8859-1");
                    mailMessage.BodyEncoding = Encoding.GetEncoding("ISO-8859-1");
                    mailMessage.To.Add(string.Join(",", destinatarios));

                    var client = new SmtpClient();
                    client.Send(mailMessage);
                }

                historicoNfe.StatusEnvioEmail = StatusEnvioEmailEnum.Sim;
            }
            catch
            {
                historicoNfe.StatusEnvioEmail = StatusEnvioEmailEnum.Erro;
            }

            this._historicoNfeRepository.InserirOuAtualizar(historicoNfe);
        }

        private void GravarLogProcesso(DateTime dataInicioProcesso, string chaveNfe, RetornoSimconsultas dadosNota)
        {
            var log = new LogSimconsultas
            {
                DataInicio = dataInicioProcesso,
                DataTermino = DateTime.Now,
                ChaveNfe = chaveNfe,
                Erro = dadosNota.Erro,
                MensagemErro = dadosNota.MensagemErro,
                DataHoraGravacao = DateTime.Now,
                Retorno = dadosNota.Erro ? string.Empty : dadosNota.Nfe.OuterXml
            };
            this._logSimconsultasRepository.Inserir(log);
        }

        private string JustNumbers(string value)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                return string.Empty;
            }

            try
            {
                string result = Regex.Replace(value, @"[^\d]", string.Empty);
                return result;
            }
            catch
            {
                return string.Empty;
            }
        }

        /// <summary>
        ///     Carrega o arquivo do assembly
        /// </summary>
        /// <param name="strFileName">Nome do arquivo</param>
        /// <returns>Retorna o arquivo</returns>
        private Stream LoadFileFromResouce(string strFileName)
        {
            // Recupera o Assebly.
            // var assembly = Assembly.GetExecutingAssembly();
            Assembly assembly = Assembly.Load("Translogic.Modules.Core");
            // Recupera o arquivo do assembly
            Stream stream = assembly.GetManifestResourceStream(strFileName);

            try
            {
                // Verifica se o arquivo foi encontrado
                if (stream == null)
                {
                    throw new FileNotFoundException(
                        "N�o foi poss�vel localizar o arquivo " + strFileName + " no resouce.",
                        strFileName);
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return stream;
        }

        private FinalidadeEmissaoEnum ObterFinalidadeEmissao(string textoFinalidadeEmissao)
        {
            /*
             1 - NFe normal
             2 - NFe complementar
             3 - NFe de ajuste
             */
            return Enum<FinalidadeEmissaoEnum>.From(this.JustNumbers(textoFinalidadeEmissao));
        }

        private FormaPagamentoEnum? ObterFormaPagamento(string numeroFormaPagamento)
        {
            var retorno = this.JustNumbers(numeroFormaPagamento);
            if (!string.IsNullOrEmpty(retorno))
                return Enum<FormaPagamentoEnum>.From(retorno);
            else
                return
                    null;
        }

        private FormatoImpressaoEnum ObterFormatoImpressao(string textoFormatoImpressao)
        {
            if (string.IsNullOrEmpty(textoFormatoImpressao))
            {
                return FormatoImpressaoEnum.Retrato;
            }

            return Enum<FormatoImpressaoEnum>.From(this.JustNumbers(textoFormatoImpressao));
        }

        private ModeloFreteEnum? ObterModeloFrete(string textoModeloFrete)
        {
            return Enum<ModeloFreteEnum>.From(this.JustNumbers(textoModeloFrete));
        }

        private ProcessoEmissaoEnum ObterProcessoEmissao(string textoProcessoEmissao)
        {
            /*0 - emiss�o de NF-e com aplicativo do contribuinte;
            1 - emiss�o de NF-e avulsa pelo Fisco;
            2 - emiss�o de NF-e avulsa, pelo contribuinte com seu certificado digital, atrav�s do site
            do Fisco;
            3- emiss�o de NF-e pelo contribuinte com aplicativo fornecido pelo Fisco.*/
            return Enum<ProcessoEmissaoEnum>.From(this.JustNumbers(textoProcessoEmissao));
        }

        private TipoAmbienteEnum ObterTipoAmbiente(string textoTipoAmbiente)
        {
            return Enum<TipoAmbienteEnum>.From(this.JustNumbers(textoTipoAmbiente));
        }

        private TipoEmissaoEnum ObterTipoEmissao(string textoTipoEmissao)
        {
            /*1 - Normal;
            2 - Conting�ncia FS
            3 - Conting�ncia SCAN
            4 - Conting�ncia DPEC
            5 - Conting�ncia FSDA*/
            if (string.IsNullOrEmpty(textoTipoEmissao))
            {
                return TipoEmissaoEnum.Normal;
            }

            return Enum<TipoEmissaoEnum>.From(this.JustNumbers(textoTipoEmissao));
        }

        private TipoNotaFiscalEnum ObterTipoNotaFiscal(string textoTipoNotaFiscal)
        {
            return Enum<TipoNotaFiscalEnum>.From(this.JustNumbers(textoTipoNotaFiscal));
        }

        private double ParseDoubleField(string valor)
        {
            double retorno = 0;
            if (Double.TryParse(valor, NumberStyles.Currency, CultureInfo.GetCultureInfo("en-US"), out retorno))
            {
                return retorno;
            }

            if (Double.TryParse(valor, NumberStyles.Currency, CultureInfo.GetCultureInfo("pt-BR"), out retorno))
            {
                return retorno;
            }

            return 0;
        }

        /// <summary>
        ///     Realiza o parse de Nfes
        /// </summary>
        /// <param name="nfe">Nfe que recebeu o xml</param>
        /// <param name="nfeRetorno">nfe a ser atualizada com os novos dados</param>
        private void ParseNfes(NotaFiscalEletronica nfe, ref NotaFiscalEletronica nfeRetorno)
        {
            foreach (FieldInfo field in nfe.GetType().GetFields())
            {
                if (field.Name != "Tipo" && field.Name != "ListaProdutos" && field.Name != "Id")
                {
                    typeof(NotaFiscalEletronica).GetField(field.Name).SetValue(nfeRetorno, field.GetValue(nfe));
                }
            }

            foreach (PropertyInfo prop in nfe.GetType().GetProperties())
            {
                if (prop.Name != "Tipo" && prop.Name != "ListaProdutos" && prop.Name != "Id")
                {
                    typeof(NotaFiscalEletronica).GetProperty(prop.Name)
                        .SetValue(nfeRetorno, prop.GetValue(nfe, null), null);
                }
            }
        }

        /// <summary>
        ///     Realiza o parse de NfesProdutos
        /// </summary>
        /// <param name="nfe">NfeProduto que recebeu o xml</param>
        /// <param name="nfeRetorno">nfeproduto a ser atualizada com os novos dados</param>
        private void ParseProdutosNfes(NotaFiscalEletronicaProduto nfe, ref NotaFiscalEletronicaProduto nfeRetorno)
        {
            foreach (FieldInfo field in nfe.GetType().GetFields())
            {
                if (field.Name != "Id" && field.Name.ToUpper() != "NFESIMCONSULTAS")
                {
                    typeof(NotaFiscalEletronicaProduto).GetField(field.Name).SetValue(nfeRetorno, field.GetValue(nfe));
                }
            }

            foreach (PropertyInfo prop in nfe.GetType().GetProperties())
            {
                if (prop.Name != "Id" && prop.Name.ToUpper() != "NFESIMCONSULTAS")
                {
                    typeof(NotaFiscalEletronicaProduto).GetProperty(prop.Name)
                        .SetValue(nfeRetorno, prop.GetValue(nfe, null), null);
                }
            }
        }

        private void ProcessarDadosDestinatario(ref NotaFiscalEletronica nfe, DadosEmpresaTemp dadosEmpresa)
        {
            if (!string.IsNullOrEmpty(dadosEmpresa.Cnpj))
                nfe.CnpjDestinatario = this.JustNumbers(dadosEmpresa.Cnpj);
            else if (!string.IsNullOrEmpty(dadosEmpresa.Cpf))
                nfe.CnpjDestinatario = this.JustNumbers(dadosEmpresa.Cpf);

            nfe.RazaoSocialDestinatario = dadosEmpresa.RazaoSocial;
            nfe.NomeFantasiaDestinatario = dadosEmpresa.NomeFantasia;
            nfe.LogradouroDestinatario = dadosEmpresa.Logradouro;
            nfe.NumeroDestinatario = dadosEmpresa.Numero;
            nfe.ComplementoDestinatario = dadosEmpresa.Complemento;
            nfe.BairroDestinatario = dadosEmpresa.Bairro;
            nfe.CodigoMunicipioDestinatario = dadosEmpresa.CodigoMunicipioIbge;
            nfe.MunicipioDestinatario = dadosEmpresa.Municipio;
            nfe.UfDestinatario = dadosEmpresa.SiglaUf;
            nfe.CepDestinatario = dadosEmpresa.Cep;
            nfe.CodigoPaisDestinatario = dadosEmpresa.CodigoPais;
            nfe.PaisDestinatario = dadosEmpresa.Pais;
            nfe.TelefoneDestinatario = dadosEmpresa.Telefone;
            nfe.InscricaoEstadualDestinatario = dadosEmpresa.InscricaoEstadual;
        }

        private void ProcessarDadosEmitente(ref NotaFiscalEletronica nfe, DadosEmpresaTemp dadosEmpresa)
        {
            if (!string.IsNullOrEmpty(dadosEmpresa.Cnpj))
                nfe.CnpjEmitente = this.JustNumbers(dadosEmpresa.Cnpj);
            else if (!string.IsNullOrEmpty(dadosEmpresa.Cpf))
                nfe.CnpjEmitente = this.JustNumbers(dadosEmpresa.Cpf);

            nfe.RazaoSocialEmitente = dadosEmpresa.RazaoSocial;
            nfe.NomeFantasiaEmitente = dadosEmpresa.NomeFantasia;
            nfe.LogradouroEmitente = dadosEmpresa.Logradouro;
            nfe.NumeroEmitente = dadosEmpresa.Numero;
            nfe.ComplementoEmitente = dadosEmpresa.Complemento;
            nfe.BairroEmitente = dadosEmpresa.Bairro;
            nfe.CodigoMunicipioEmitente = dadosEmpresa.CodigoMunicipioIbge;
            nfe.MunicipioEmitente = dadosEmpresa.Municipio;
            nfe.UfEmitente = dadosEmpresa.SiglaUf;
            nfe.CepEmitente = dadosEmpresa.Cep;
            nfe.CodigoPaisEmitente = dadosEmpresa.CodigoPais;
            nfe.PaisEmitente = dadosEmpresa.Pais;
            nfe.TelefoneEmitente = dadosEmpresa.Telefone;
            nfe.InscricaoEstadualEmitente = dadosEmpresa.InscricaoEstadual;
        }

        private IList<NfeProdutoSimconsultas> ProcessarProdutosNfeSimConsultas(
            NfeSimconsultas nfe,
            IEnumerable<INotaFiscalEletronicaProduto> listaProdutos)
        {
            var list = new List<NfeProdutoSimconsultas>();

            double pesoTotalNota = 0;

            if (listaProdutos != null)
            {
                int i = 0;

                foreach (INotaFiscalEletronicaProduto produto in listaProdutos)
                {
                    var nfeProduto = new NfeProdutoSimconsultas((NotaFiscalEletronicaProduto)produto);
                    nfeProduto.NfeSimconsultas = nfe;
                    nfeProduto.DataHoraGravacao = DateTime.Now;

                    if (nfeProduto.UnidadeComercial == "PC")
                    {
                        pesoTotalNota += 0;
                    }
                    else if (nfeProduto.UnidadeComercial == "S50")
                    {
                        pesoTotalNota += nfeProduto.QuantidadeComercial * 50;
                    }
                    else if (nfeProduto.UnidadeComercial == "S60")
                    {
                        pesoTotalNota += nfeProduto.QuantidadeComercial * 60;
                    }
                    else
                    {
                        pesoTotalNota += nfeProduto.QuantidadeVendida;
                    }

                    if (nfe.ListaProdutos != null && nfe.ListaProdutos.Count > 0)
                    {
                        var prod =
                            (NotaFiscalEletronicaProduto)
                            this._nfeProdutoSimconsultasRepository.ObterPorId(nfe.ListaProdutos[i].Id);

                        this.ParseProdutosNfes(nfeProduto, ref prod);

                        this._nfeProdutoSimconsultasRepository.Atualizar((NfeProdutoSimconsultas)prod);
                    }
                    else
                    {
                        this._nfeProdutoSimconsultasRepository.Inserir(nfeProduto);
                    }

                    list.Add(nfeProduto);

                    i++;
                }

                nfe.Peso = pesoTotalNota;
                nfe.PesoBruto = pesoTotalNota;
            }

            if (this.VerificarPorUnidadeComercial(list))
            {
                nfe.Peso = 0;
                nfe.PesoBruto = 0;
            }

            return list;
        }

        private DadosEmpresaTemp RecuperarDadosEmpresa(XmlNode noXml, bool indEmitente, XmlNamespaceManager nsmgr)
        {
            var docEmp = new XmlDocument();
            docEmp.LoadXml(noXml.OuterXml);

            XmlNode noEndereco = docEmp.SelectSingleNode(indEmitente ? "//d:enderEmit" : "//d:enderDest", nsmgr);

            var dados = new DadosEmpresaTemp
            {
                RazaoSocial = ObterTextoNoXml(docEmp, "xNome", nsmgr),
                NomeFantasia = ObterTextoNoXml(docEmp, "xFant", nsmgr),
                Logradouro = ObterTextoNoXml(noEndereco, "xLgr", nsmgr),
                Numero = ObterTextoNoXml(noEndereco, "nro", nsmgr),
                Complemento = ObterTextoNoXml(noEndereco, "xCpl", nsmgr),
                Bairro = ObterTextoNoXml(noEndereco, "xBairro", nsmgr),
                CodigoMunicipioIbge = ObterTextoNoXml(noEndereco, "cMun", nsmgr),
                Municipio = ObterTextoNoXml(noEndereco, "xMun", nsmgr),
                SiglaUf = ObterTextoNoXml(noEndereco, "UF", nsmgr),
                Cep = ObterTextoNoXml(noEndereco, "CEP", nsmgr),
                CodigoPais = ObterTextoNoXml(noEndereco, "cPais", nsmgr),
                Pais = ObterTextoNoXml(noEndereco, "xPais", nsmgr),
                Telefone = ObterTextoNoXml(noEndereco, "fone", nsmgr),
                InscricaoEstadual = ObterTextoNoXml(docEmp, "IE", nsmgr)
            };
            string cnpj = ObterTextoNoXml(docEmp, "CNPJ", nsmgr);
            if (!string.IsNullOrEmpty(dados.SiglaUf) && dados.SiglaUf.Equals("EX"))
            {
                dados.Cnpj = "00000000000000";
            }
            else
            {
                if (!string.IsNullOrEmpty(cnpj))
                {
                    dados.Cnpj = cnpj;
                }
            }

            string cpf = ObterTextoNoXml(docEmp, "CPF", nsmgr);
            if (!string.IsNullOrEmpty(cpf))
            {
                dados.Cpf = cpf;
            }

            return dados;
        }

        /*
		/// <summary>
		/// Efetua a valida��o de um documento Xml
		/// </summary>
		/// <param name="xmlDocument">Xml a ser validado</param>
		/// <param name="xsdPath">Caminho do Xsd</param>
		/// <returns>Retorna true se o schema estiver certo</returns>
		private bool ValidarDocumentoXml(XmlDocument xmlDocument, string xsdPath)
		{
			// Declara��o de vari�veis.
			XmlReaderSettings settings = null;
			XmlReader xmlReader = null;

			try
			{
				// Verifica se o Xml n�o � nulo.
				if (xmlDocument == null)
				{
					throw new Exception("N�o foi poss�vel carregar o xml.");
				}

				// Int�ncia de Objeto.
				settings = new XmlReaderSettings();

				// Seta o Schema XSD.
				settings.Schemas.Add(null, xsdPath);

				// Seta o tipo de valida��o.
				settings.ValidationType = ValidationType.Schema;

				// Cria o XmlReader para validar o Schema.
				xmlReader = XmlReader.Create(new StringReader(xmlDocument.InnerXml), settings);

				// Quando � efetua a leitura do xml � validado se o Schema(settings) � v�lido.
				// Caso o schema esteja errado, ent�o lan�a um exception.
				while (xmlReader.Read());

				// Retorna true se o schema estiver certo.
				return true;
			}
			catch (Exception ex)
			{
				return false;
			}
		}
		*/

        /// <summary>
        ///     Remove o n� logTrace do xml
        /// </summary>
        /// <param name="xmlDocument">Documento xml</param>
        private void RemoveNoLogTrace(XmlDocument xmlDocument)
        {
            // Cria o namespace
            XmlElement root = xmlDocument.DocumentElement;
            var nsmgr = new XmlNamespaceManager(xmlDocument.NameTable);
            nsmgr.AddNamespace("d", root.NamespaceURI);

            // Recupera o no INfNfe
            XmlNode noinfNFe = xmlDocument.SelectSingleNode("//d:infNFe", nsmgr);

            // Recupera o no LogTrace
            XmlNode noLogTrace = xmlDocument.SelectSingleNode("//d:LOGTrace", nsmgr);

            if (noinfNFe != null && noLogTrace != null)
            {
                // Remove o N� LogTrace do xml
                noinfNFe.RemoveChild(noLogTrace);
            }

            // Recupera o no OrigemNfe
            XmlNode noOrigemNfe = xmlDocument.SelectSingleNode("//d:OrigemNFe", nsmgr);
            if (noinfNFe != null && noOrigemNfe != null)
            {
                // Remove o N� LogTrace do xml
                noinfNFe.RemoveChild(noOrigemNfe);
            }
        }

        /// <summary>
        ///     Evento disparato quando ocorre erro de valida��o no xml
        /// </summary>
        /// <param name="sender">Objeto que disparou o evento</param>
        /// <param name="args">dados da exception</param>
        private void ValidationEventHandler(object sender, ValidationEventArgs args)
        {
            // Exibe o erro da valida��o
            if (this._errosValidacaoXml == null)
            {
                this._errosValidacaoXml = new StringBuilder();
            }

            this._errosValidacaoXml.AppendLine(args.Message);
        }

        /// <summary>
        ///     Verificar o tipo de unidade comercial
        /// </summary>
        /// <param name="listaProdutos">Lista de produtos a verificar</param>
        /// <returns>Verdadeiro se algum produto atender a alguma das condi��es testadas</returns>
        private bool VerificarPorUnidadeComercial(IEnumerable<INotaFiscalEletronicaProduto> listaProdutos)
        {
            return
                listaProdutos.Any(
                    p =>
                    p.UnidadeComercial.Trim().ToUpper().Contains("SAC")
                    || p.UnidadeComercial.Trim().ToUpper().Contains("SC")
                    || p.UnidadeComercial.Trim().ToUpper().Contains("SCS"));
        }



        #endregion

        /// <summary>
        ///     Classe de empresa
        /// </summary>
        protected class DadosEmpresaTemp
        {
            #region Public Properties

            /// <summary>
            ///     Gets or sets Bairro.
            /// </summary>
            public string Bairro { get; set; }

            /// <summary>
            ///     Gets or sets Cep.
            /// </summary>
            public string Cep { get; set; }

            /// <summary>
            ///     Gets or sets Cnpj.
            /// </summary>
            public string Cnpj { get; set; }

            /// <summary>
            ///     Gets or sets CodigoMunicipioIbge.
            /// </summary>
            public string CodigoMunicipioIbge { get; set; }

            /// <summary>
            ///     Gets or sets CodigoPais.
            /// </summary>
            public string CodigoPais { get; set; }

            /// <summary>
            ///     Gets or sets Complemento.
            /// </summary>
            public string Complemento { get; set; }

            /// <summary>
            ///     Gets or sets Cpf.
            /// </summary>
            public string Cpf { get; set; }

            /// <summary>
            ///     Gets or sets InscricaoEstadual.
            /// </summary>
            public string InscricaoEstadual { get; set; }

            /// <summary>
            ///     Gets or sets Logradouro.
            /// </summary>
            public string Logradouro { get; set; }

            /// <summary>
            ///     Gets or sets Municipio.
            /// </summary>
            public string Municipio { get; set; }

            /// <summary>
            ///     Gets or sets NomeFantasia.
            /// </summary>
            public string NomeFantasia { get; set; }

            /// <summary>
            ///     Gets or sets Numero.
            /// </summary>
            public string Numero { get; set; }

            /// <summary>
            ///     Gets or sets Pais.
            /// </summary>
            public string Pais { get; set; }

            /// <summary>
            ///     Gets or sets RazaoSocial.
            /// </summary>
            public string RazaoSocial { get; set; }

            /// <summary>
            ///     Gets or sets SiglaUf.
            /// </summary>
            public string SiglaUf { get; set; }

            /// <summary>
            ///     Gets or sets Telefone.
            /// </summary>
            public string Telefone { get; set; }

            /// <summary>
            ///     Gets or sets Uf.
            /// </summary>
            public string Uf { get; set; }

            #endregion
        }
    }
}