﻿namespace Translogic.Modules.Core.Domain.Services.FluxosComerciais
{
    using System;
    using System.Collections.Generic;
    using ALL.Core.Dominio;
    using Castle.Services.Transaction;
    using Model.FluxosComerciais.Atrasos;
    using Model.FluxosComerciais.Atrasos.Repositories;

    [Transactional]
    public class AtrasosService
    {
        private readonly IAvisoAtrasoRepository _avisoAtrasoRepository;
        
        public AtrasosService(IAvisoAtrasoRepository avisoAtrasoRepository)
        {
            _avisoAtrasoRepository = avisoAtrasoRepository;
        }

        public ResultadoPaginado<AvisoAtraso> ObterAtrasos(DetalhesPaginacao pagination, DateTime dataInicial,
            DateTime dataFinal, TipoAtraso tipo, ResponsavelAtraso responsavel)
        {
            return _avisoAtrasoRepository.ObterAtrasos(pagination, dataInicial, dataFinal, tipo, responsavel);
        }

        public AvisoAtraso ObterAvisoAtrasoPorId(int id)
        {
            return _avisoAtrasoRepository.ObterPorId(id);
        }

        public AvisoAtraso Salvar(AvisoAtraso avisoAtraso)
        {
            return _avisoAtrasoRepository.Salvar(avisoAtraso);
        }

        public IEnumerable<TipoAtraso> ObterTipoAtrasos()
        {
            return _avisoAtrasoRepository.ObterTipos();
        }

        public IEnumerable<MotivoAtraso> ObterMotivoAtrasos()
        {
            return _avisoAtrasoRepository.ObterMotivos();
        }

        public IEnumerable<ResponsavelAtraso> ObterResponsavelAtrasos()
        {
            return _avisoAtrasoRepository.ObterResponsaveis();
        }
    }
}