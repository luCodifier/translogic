namespace Translogic.Modules.Core.Domain.Services.FluxosComerciais
{
	using System.Collections.Generic;
	using System.Linq;
	using ALL.Core.Dominio;
	using Castle.Services.Transaction;
	using Translogic.Core.Infrastructure.Web;
	using Translogic.Modules.Core.Domain.Model.Dto;
	using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;
	using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Refaturamento;
	using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Refaturamento.Repositories;
	using Translogic.Modules.Core.Domain.Model.Trem.Repositories;

	/// <summary>
	/// Servi�o de Refaturamento
	/// </summary>
	public class RefaturamentoService
	{
		private readonly ICteRepository _cteRepository;
		private readonly IRefaturamentoDestinoPermRepository _refaturamentoDestinoPermRepository;

		/// <summary>
		/// Construtor da classe de Servi�o de Refaturamento
		/// </summary>
		/// <param name="cteRepository">Reposit�rio do Cte.</param>
		/// <param name="refaturamentoDestinoPermRepository">Reposit�rio de Destino Permitido para Refaturamento.</param>
		public RefaturamentoService(ICteRepository cteRepository, IRefaturamentoDestinoPermRepository refaturamentoDestinoPermRepository)
		{
			_cteRepository = cteRepository;
			_refaturamentoDestinoPermRepository = refaturamentoDestinoPermRepository;
		}

		/// <summary>
		/// Obt�m os refaturamentos
		/// </summary>
		/// /// <param name="prefixo"> prefixo do trem</param>
		/// <param name="ordemServico"> ordem de Servi�o</param>
		/// <returns> Dto de Refaturamento</returns>
		public IList<RefaturamentoDto>	ObterCteRefaturamento(string prefixo, string ordemServico)
		{
			return _cteRepository.ObterParaRefaturamento(ordemServico.ToUpperInvariant(), prefixo.ToUpperInvariant());
		}

		/// <summary>
		/// Salva os refaturamentos
		/// </summary>
		/// /// <param name="listaVagoes"> Lista de Vag�es</param>
		[Transaction]
		public virtual void SalvarCteRefaturamento(RefatVagoesDto listaVagoes)
		{
			_cteRepository.SalvarRefaturamento(listaVagoes);
		}

		/// <summary>
		/// Verifica se os Destinos Permitem Refaturamento
		/// </summary>
		/// <param name="listaCteRefaturamento">Lista de Cte a ser validada.</param>
		/// <returns>Retorna a lista Filtrada.</returns>
		public IList<RefaturamentoDto> VerificarDestinosPerm(IList<RefaturamentoDto> listaCteRefaturamento)
		{
			IList<RefaturamentoDestinoPerm> refaturamentoDestinoPermitidos = _refaturamentoDestinoPermRepository.ObterTodosAtivos();

			return
				(from c in listaCteRefaturamento
				 where (from b in refaturamentoDestinoPermitidos select b.CodigoAreaOperacional).Contains(c.Destino)
				 select c).ToList();
		}

		/// <summary>
		/// Verifica se os Destinos Permitem Refaturamento
		/// </summary>
		/// <param name="contrato"> N�mero do contrato.</param>
		/// <returns>Retorna saldo disponivel..</returns>
		public decimal ObterSaldoContratoDisponivel(string contrato)
		{
			return _cteRepository.ObterSaldoContratoDisponivel(contrato);
		}
	}
}