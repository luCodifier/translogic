﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Translogic.Modules.Core.Domain.Model.Estrutura;
using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Dcls;
using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Repositories;
using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos;
using Translogic.Modules.Core.Domain.Model.Estrutura.Repositories;
using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Repositories;
using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos.Repositories;
using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;
using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Nfes.Repositories;
using Translogic.Modules.Core.Domain.Model.PainelExpedicao.Documentos.Dto;
using Speed.Common;
using Translogic.Modules.Core.Spd.BLL;

namespace Translogic.Modules.Core.Domain.Services.FluxosComerciais
{
    public class DclService
    {
        private readonly IPedidoDerivadoRepository _pedidoDerivadoRepository;
        private readonly IPedidoTransporteRepository _pedidoTransporteRepository;
        private readonly IEmpresaRepository _empresaRepository;
        private readonly IEmpresaClienteRepository _empresaClienteRepository;
        private readonly IFluxoComercialRepository _fluxoComercialRepository;
        private readonly IMercadoriaRepository _mercadoriaRepository;
        private readonly IItemDespachoRepository _itemDespachoRepository;
        private readonly INotaFiscalTranslogicRepository _notaFiscalRepository;
        private readonly ICteRepository _cteRepository;
        private readonly INfeReadonlyRepository _nfeReadonlyRepository;

        public DclService(IPedidoDerivadoRepository pedidoDerivadoRepository,
                          IPedidoTransporteRepository pedidoTransporteRepository,
                          IEmpresaRepository empresaRepository,
                          IEmpresaClienteRepository empresaClienteRepository,
                          IFluxoComercialRepository fluxoComercialRepository,
                          IMercadoriaRepository mercadoriaRepository,
                          IItemDespachoRepository itemDespachoRepository,
                          INotaFiscalTranslogicRepository notaFiscalRepository,
                          ICteRepository cteRepository,
                          INfeReadonlyRepository nfeReadonlyRepository)
        {
            this._pedidoDerivadoRepository = pedidoDerivadoRepository;
            this._pedidoTransporteRepository = pedidoTransporteRepository;
            this._empresaRepository = empresaRepository;
            this._empresaClienteRepository = empresaClienteRepository;
            this._fluxoComercialRepository = fluxoComercialRepository;
            this._mercadoriaRepository = mercadoriaRepository;
            this._itemDespachoRepository = itemDespachoRepository;
            this._notaFiscalRepository = notaFiscalRepository;
            this._cteRepository = cteRepository;
            this._nfeReadonlyRepository = nfeReadonlyRepository;
        }

        /// <summary>
        /// Obtem Dcl para impressao
        /// </summary>
        /// <param name="despachos">ids de despacho</param>
        /// <returns>Retorna lista de Dcls para Impressão</returns>
        public IList<DclImpressao> ObterDclImpressao(List<Spd.Data.Despacho> despachos)
        {
            var retornoDclsImpressao = new List<DclImpressao>();
            Spd.Data.Vagao vagao = null;

            var sdEmpresas = new SmartDictionary<decimal?, IEmpresa>();

            foreach (var despacho in despachos)
            {
                var dclImp = new DclImpressao();

                var serie = BL_SerieDespacho.SelectByPk(despacho.IdSerieDespacho);
                var serieUf = BL_SerieDespUf.SelectByPk(despacho.IdSerieDespachoUf);
                var pedidoDerivado = BL_PedidoDerivado.SelectByPk(despacho.IdPedidoDerivado);

                dclImp.IdDespacho = despacho.IdDespacho.ToInt32();

                // TODO: PERFORMANCE
                dclImp.Emitente = BL_Empresa.SelectByPk(despacho.IdEmpresa); // sdEmpresas.Find(despacho.IdEmpresa, () => _empresaClienteRepository.ObterPorId(despacho.IdEmpresa.ToInt32()));

                dclImp.Numero = (despacho.NumeroDespachoUf.HasValue ? despacho.NumeroDespachoUf.Value.ToString() :
                                        (despacho.NumDespIntercambio.HasValue ? despacho.NumDespIntercambio.Value.ToString()
                                            : despacho.NumeroDespacho.Value.ToString()));

                // Alteração de regra no número do despacho no DCL
                // Verificar se o despacho possui CTE
                // Caso possua CTE, então atribuir o Número do Despacho de 5 dígitos
                var ctes = BL_Cte.ObterCtesPorDespacho(despacho.IdDespacho);
                if (ctes.Any())
                {
                    dclImp.Numero = (despacho.NumeroDespacho.HasValue ? despacho.NumeroDespacho.Value.ToString() : dclImp.Numero);
                }

                dclImp.Serie = (serieUf != null && (!String.IsNullOrEmpty(serieUf.CodigoSerieDespacho)) ? serieUf.CodigoSerieDespacho
                                        : (!String.IsNullOrEmpty(despacho.SerieDespachoSdi) ? despacho.SerieDespachoSdi :
                                            serie.SerieDespachoNum));

                dclImp.Mercadoria = "";
                dclImp.SerieVagao = "";
                dclImp.CodigoVagao = "";
                dclImp.TaraVagao = 0.0;
                dclImp.PesoBrutoVagao = 0.0;
                dclImp.PesoLiquido = 0.0;
                dclImp.Observacao = despacho.Observacao;
                // colocar na observação o número de despacho de 6 dígitos
                if (ctes.Any())
                {
                    dclImp.Observacao += " / Número Despacho 6 dígitos: " + (despacho.NumeroDespachoUf.HasValue ? despacho.NumeroDespachoUf.Value.ToString() : "");
                }

                // TODO: PERFORMANCE
                var itemDespacho = BL_ItemDespacho.SelectSingle(new Spd.Data.ItemDespacho { IdDespacho = despacho.IdDespacho });
                if (itemDespacho != null)
                {
                    if (itemDespacho.IdVagao != null)
                    {
                        vagao = BL_Vagao.SelectByPk(itemDespacho.IdVagao);
                        if (vagao != null)
                        {
                            dclImp.CodigoVagao = vagao.Codigo;
                            //dclImp.TaraVagao = (vagao.PesoTara.HasValue ? vagao.PesoTara.Value : 0.00);
                            dclImp.TaraVagao = 0.00;
                            //dclImp.PesoBrutoVagao = (itemDespacho.PesoToneladaUtil.HasValue ? itemDespacho.PesoToneladaUtil.Value : (vagao.PesoTotal.HasValue ? vagao.PesoTotal.Value : 0.00)) + dclImp.TaraVagao;
                            dclImp.PesoBrutoVagao = 0.00;
                            var serieVagao = BL_SerieVagao.SelectByPk(vagao.IdSerie);
                            if (serieVagao != null)
                            {
                                dclImp.SerieVagao = serieVagao.Codigo;
                            }
                        }
                    }
                }

                // Obtém Empresa Remetente, Destinatario, Expedidor, Recebedor
                // através do Pedido Derivado
                int? pedidoDerivadoId = -1;
                if (pedidoDerivado != null)
                {
                    pedidoDerivadoId = pedidoDerivado.Id.ToInt32();
                    if (pedidoDerivadoId > 0)
                    {
                        var pedDerivado = _pedidoDerivadoRepository.ObterPorId(pedidoDerivadoId);

                        // Remetente e Destinatario Fiscal
                        var cnpjRemetenteFiscal = pedDerivado.FluxoComercial.CnpjRemetenteFiscal;
                        var cnpjDestinatarioFiscal = pedDerivado.FluxoComercial.CnpjDestinatarioFiscal;

                        if (!(String.IsNullOrEmpty(cnpjRemetenteFiscal)))
                        {
                            dclImp.Remetente = _empresaRepository.ObterPorCnpj(cnpjRemetenteFiscal);
                        }
                        else
                            dclImp.Remetente = pedDerivado.FluxoComercial.EmpresaRemetente;

                        if (!(String.IsNullOrEmpty(cnpjDestinatarioFiscal)))
                        {
                            dclImp.Destinatario = _empresaRepository.ObterPorCnpj(cnpjDestinatarioFiscal);
                        }
                        else
                            dclImp.Destinatario = pedDerivado.FluxoComercial.EmpresaDestinataria;

                        // Remetente e Destinatario Operacional
                        dclImp.Expedidor = pedDerivado.FluxoComercial.EmpresaRemetente;
                        dclImp.Recebedor = pedDerivado.FluxoComercial.EmpresaDestinataria;
                        dclImp.FluxoComercial = pedDerivado.FluxoComercial;
                        dclImp.Origem = pedDerivado.FluxoComercial.Origem;
                        dclImp.Destino = pedDerivado.FluxoComercial.Destino;
                        dclImp.Mercadoria = pedDerivado.Mercadoria.DescricaoResumida;
                    }
                }

                // Documentos
                dclImp.ListaDocs = new List<DclImpressaoDocumento>();

                // TODO: PERFORMANCE
                var notas = Spd.BLL.BL_NotaFiscal.SelectPorDespacho(despacho);

                if (notas != null)
                {
                    if (notas.Any())
                    {
                        foreach (Spd.Data.NotaFiscal nf in notas)
                        {
                            var dclImpDoc = new DclImpressaoDocumento();
                            dclImpDoc.Numero = nf.NumNota;
                            dclImpDoc.ChaveNfe = nf.ChaveNotaFiscalEletronica;
                            dclImpDoc.Conteiner = nf.ConteinerNotaFiscal;
                            dclImpDoc.Volume = (double)nf.VolumeNotaFiscal.GetValueOrDefault();
                            dclImpDoc.Peso = (double)nf.PesoNotaFiscal.GetValueOrDefault();

                            var cgcRemetente = nf.CgcRemetente;
                            long cgcRemetenteNumero = 0;
                            if (long.TryParse(cgcRemetente, out cgcRemetenteNumero))
                            {
                                if (cgcRemetenteNumero == 0)
                                {
                                    var vwNfe = _nfeReadonlyRepository.ObterPorChaveNfe(nf.ChaveNotaFiscalEletronica);
                                    if (vwNfe != null)
                                        cgcRemetente = vwNfe.CnpjEmitente;
                                }
                            }

                            dclImpDoc.CnpjEmitente = cgcRemetente;
                            dclImp.ListaDocs.Add(dclImpDoc);
                        }
                    }
                }

                dclImp.PesoLiquido = dclImp.ListaDocs.Sum(docs => docs.Peso);

                // Ctes
                dclImp.Ctes = new List<Spd.Data.Cte>();
                if (ctes != null)
                {
                    foreach (var cc in ctes)
                    {
                        dclImp.Ctes.Add(cc);
                    }
                }

                retornoDclsImpressao.Add(dclImp);
            }

            return retornoDclsImpressao;
        }

        /// <summary>
        /// Obtem Dcl para impressao
        /// </summary>
        /// <param name="itemsDespachos">ids de despacho</param>
        /// <returns>Retorna lista de Dcls para Impressão</returns>
        public IList<DclImpressao> ObterDclImpressao(IList<Spd.Data.ItemDespacho> itemsDespachos)
        {
            var retornoDclsImpressao = new List<DclImpressao>();

            var itensDespachosIds = itemsDespachos.Select(d => int.Parse(d.IdItemDespacho.ToString())).ToList();
            var despachoIds = itemsDespachos.Select(d => d.IdDespacho).ToList();

            var despachos = Spd.BLL.BL_Despacho.ObterDespachosPorItensDespacho(itemsDespachos.Select(p => p.IdItemDespacho.ToInt32()).ToArray());

            var pedidosDerivadosIds = itemsDespachos.Select(p => despachos.First(q => q.IdDespacho == p.IdDespacho).IdPedidoDerivado.GetValueOrDefault().ToInt32())
                                        .Distinct(p => p).ToList();

            var itensDespachos = BL_ItemDespacho.ObterDespachosPorItensDespacho(itensDespachosIds.ToArray());

            var ctesDespachos = BL_Cte.ObterCtesPorDespachos(despachoIds);

            var notasFiscaisDespachos = BL_NotaFiscal.SelectPorDespachos(despachoIds);

            // TODO: PERFORMANCE
            var pedidosDerivados = _pedidoDerivadoRepository.ObterPorIds(pedidosDerivadosIds);

            var empresas = new List<IEmpresa>();
            var sdEmpresas = new SmartDictionary<decimal?, IEmpresa>();

            int count = 0;
            foreach (var d in itemsDespachos)
            {
                count++;
                // Sis.LogTrace("Processnado despacho: " + count + " de " + itemsDespachos.Count);

                var despacho = despachos.First(p => p.IdDespacho == d.IdDespacho);
                var serie = despacho.IdSerieDespacho != null ? BL_SerieDespacho.SelectByPk(despacho.IdSerieDespacho) : null;
                var serieUf = despacho.IdSerieDespachoUf != null ? BL_SerieDespUf.SelectByPk(despacho.IdSerieDespachoUf) : null;
                var pedidoDerivado = despacho.IdPedidoDerivado != null ? BL_PedidoDerivado.SelectByPk(despacho.IdPedidoDerivado) : null;

                var dclImp = new DclImpressao();

                dclImp.IdDespacho = despacho.IdDespacho.GetValueOrDefault().ToInt32();

                // TODO: PERFORMANCE
                dclImp.Emitente = BL_Empresa.SelectByPk(despacho.IdEmpresa); // sdEmpresas.Find(despacho.IdEmpresa, () => _empresaRepository.ObterPorId(despacho.IdEmpresa.ToInt32()));

                dclImp.Numero = (despacho.NumeroDespachoUf.HasValue ? despacho.NumeroDespachoUf.Value.ToString() :
                                        (despacho.NumDespIntercambio.HasValue ? despacho.NumDespIntercambio.Value.ToString()
                                            : despacho.NumeroDespacho.Value.ToString()));

                // Alteração de regra no número do despacho no DCL
                // Verificar se o despacho possui CTE
                // Caso possua CTE, então atribuir o Número do Despacho de 5 dígitos
                var ctes = ctesDespachos.Where(cte => cte.IdDespacho == despacho.IdDespacho).ToList();
                if (ctes.Any())
                {
                    dclImp.Numero = (despacho.NumeroDespacho.HasValue ? despacho.NumeroDespacho.Value.ToString() : dclImp.Numero);
                }

                dclImp.Serie = (serieUf != null && (!String.IsNullOrEmpty(serieUf.CodigoSerieDespacho)) ? serieUf.CodigoSerieDespacho
                                        : (!String.IsNullOrEmpty(despacho.SerieDespachoSdi) ? despacho.SerieDespachoSdi :
                                            serie.SerieDespachoNum));

                dclImp.Mercadoria = "";
                dclImp.SerieVagao = "";
                dclImp.CodigoVagao = "";
                dclImp.TaraVagao = 0.0;
                dclImp.PesoBrutoVagao = 0.0;
                dclImp.PesoLiquido = 0.0;
                dclImp.Observacao = despacho.Observacao;
                // colocar na observação o número de despacho de 6 dígitos
                if (ctes.Any())
                {
                    dclImp.Observacao += " / Número Despacho 6 dígitos: " + (despacho.NumeroDespachoUf.HasValue ? despacho.NumeroDespachoUf.Value.ToString() : "");
                }

                var itemDespacho = itensDespachos.SingleOrDefault(i => i.IdItemDespacho == int.Parse(d.IdItemDespacho.ToString()));
                if (itemDespacho != null)
                {
                    if (itemDespacho.IdVagao != null)
                    {
                        var vagao = BL_Vagao.SelectByPk(itemDespacho.IdVagao);
                        if (vagao != null)
                        {
                            dclImp.CodigoVagao = vagao.Codigo;
                            //dclImp.TaraVagao = (vagao.PesoTara.HasValue ? vagao.PesoTara.Value : 0.00);
                            dclImp.TaraVagao = 0.00;
                            //dclImp.PesoBrutoVagao = (itemDespacho.PesoToneladaUtil.HasValue ? itemDespacho.PesoToneladaUtil.Value : (vagao.PesoTotal.HasValue ? vagao.PesoTotal.Value : 0.00)) + dclImp.TaraVagao;
                            dclImp.PesoBrutoVagao = 0.00;
                            var serieVagao = BL_SerieVagao.SelectByPk(vagao.IdSerie);
                            if (serieVagao != null)
                            {
                                dclImp.SerieVagao = serieVagao.Codigo;
                            }
                        }
                    }
                }

                // Obtém Empresa Remetente, Destinatario, Expedidor, Recebedor
                // através do Pedido Derivado
                int? pedidoDerivadoId = -1;
                if (pedidoDerivado != null)
                {
                    pedidoDerivadoId = pedidoDerivado.Id.ToInt32();
                    if (pedidoDerivadoId > 0)
                    {
                        var pedDerivado = pedidosDerivados.SingleOrDefault(p => p.Id == pedidoDerivadoId);

                        // Remetente e Destinatario Fiscal
                        var cnpjRemetenteFiscal = pedDerivado.FluxoComercial.CnpjRemetenteFiscal;
                        var cnpjDestinatarioFiscal = pedDerivado.FluxoComercial.CnpjDestinatarioFiscal;

                        if (!(String.IsNullOrEmpty(cnpjRemetenteFiscal)))
                        {
                            var empresa = empresas.SingleOrDefault(e => e.Cgc == cnpjRemetenteFiscal);
                            if (empresa == null)
                            {
                                empresa = _empresaRepository.ObterPorCnpj(cnpjRemetenteFiscal);
                                empresas.Add(empresa);
                            }

                            dclImp.Remetente = empresa;
                        }
                        else
                        {
                            dclImp.Remetente = pedDerivado.FluxoComercial.EmpresaRemetente;
                        }

                        if (!(String.IsNullOrEmpty(cnpjDestinatarioFiscal)))
                        {
                            var empresa = empresas.SingleOrDefault(e => e.Cgc == cnpjDestinatarioFiscal);
                            if (empresa == null)
                            {
                                empresa = _empresaRepository.ObterPorCnpj(cnpjDestinatarioFiscal);
                                empresas.Add(empresa);
                            }

                            dclImp.Destinatario = empresa;
                        }
                        else
                        {
                            dclImp.Destinatario = pedDerivado.FluxoComercial.EmpresaDestinataria;
                        }

                        // Remetente e Destinatario Operacional
                        dclImp.Expedidor = pedDerivado.FluxoComercial.EmpresaRemetente;
                        dclImp.Recebedor = pedDerivado.FluxoComercial.EmpresaDestinataria;
                        dclImp.FluxoComercial = pedDerivado.FluxoComercial;
                        dclImp.Origem = pedDerivado.FluxoComercial.Origem;
                        dclImp.Destino = pedDerivado.FluxoComercial.Destino;
                        dclImp.Mercadoria = pedDerivado.Mercadoria.DescricaoResumida;
                    }
                }

                // Documentos
                dclImp.ListaDocs = new List<DclImpressaoDocumento>();

                var notas = notasFiscaisDespachos.Where(nf => nf.IdDespacho == despacho.IdDespacho).ToList();
                if (notas != null)
                {
                    if (notas.Any())
                    {
                        foreach (var nf in notas)
                        {
                            var dclImpDoc = new DclImpressaoDocumento();
                            dclImpDoc.Numero = nf.NumNota;
                            dclImpDoc.ChaveNfe = nf.ChaveNotaFiscalEletronica;
                            dclImpDoc.Conteiner = nf.ConteinerNotaFiscal;
                            dclImpDoc.Volume = (double)(nf.VolumeNotaFiscal.HasValue ? nf.VolumeNotaFiscal.Value : 0M);
                            dclImpDoc.Peso = (double)(nf.PesoNotaFiscal.HasValue ? nf.PesoNotaFiscal.Value : 0M);

                            var cgcRemetente = nf.CgcRemetente;
                            long cgcRemetenteNumero = 0;
                            if (long.TryParse(cgcRemetente, out cgcRemetenteNumero))
                            {
                                if (cgcRemetenteNumero == 0)
                                {
                                    // TODO: PERFORMANCE
                                    var vwNfe = _nfeReadonlyRepository.ObterPorChaveNfe(nf.ChaveNotaFiscalEletronica);
                                    if (vwNfe != null)
                                        cgcRemetente = vwNfe.CnpjEmitente;
                                }
                            }

                            dclImpDoc.CnpjEmitente = cgcRemetente;
                            dclImp.ListaDocs.Add(dclImpDoc);
                        }
                    }
                }

                dclImp.PesoLiquido = dclImp.ListaDocs.Sum(docs => docs.Peso);

                // Ctes
                dclImp.Ctes = new List<Spd.Data.Cte>();
                if (ctes != null)
                {
                    foreach (var cc in ctes)
                    {
                        dclImp.Ctes.Add(cc);
                    }
                }

                retornoDclsImpressao.Add(dclImp);
            }

            return retornoDclsImpressao;
        }
    }
}