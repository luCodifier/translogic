﻿using System.Diagnostics;

namespace Translogic.Modules.Core.Domain.Services.FluxosComerciais
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using Castle.Services.Transaction;
    using Model.Acesso;
    using Model.Codificador;
    using Model.Codificador.Repositories;
    using Model.Diversos;
    using Model.Dto;
    using Model.Estrutura;
    using Model.Estrutura.Repositories;
    using Model.FluxosComerciais;
    using Model.FluxosComerciais.Carregamentos;
    using Model.FluxosComerciais.Carregamentos.Repositories;
    using Model.FluxosComerciais.ConhecimentoTransporteEletronico;
    using Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;
    using Model.FluxosComerciais.Dcls;
    using Model.FluxosComerciais.Dcls.Repositories;
    using Model.FluxosComerciais.Pedidos.Despachos;
    using Model.FluxosComerciais.Pedidos.Despachos.Repositories;
    using Model.FluxosComerciais.Repositories;
    using Model.Trem.Veiculo.Conteiner;
    using Model.Trem.Veiculo.Conteiner.Repositories;
    using Model.Trem.Veiculo.Vagao;
    using Model.Trem.Veiculo.Vagao.QuadroEstado;
    using Model.Trem.Veiculo.Vagao.QuadroEstado.Repositories;
    using Model.Trem.Veiculo.Vagao.Repositories;
    using Model.Via;
    using Model.Via.Circulacao;
    using Model.Via.Circulacao.Repositories;
    using Model.Via.Repositories;
    using Speed.Common;
    using Translogic.Core.Infrastructure;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Nfes;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Nfes.Repositories;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Simconsultas;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Simconsultas.Repositories;
    using Translogic.Modules.Core.Domain.Services.Ctes;

    /// <summary>
    /// Serviço de carregamento
    /// </summary>
    [Transactional]
    public class CarregamentoService
    {

        #region Declarations

        private const string ConfGeralCteVerificarCnpjRemetenteFiscal = "CTE_VERIFICAR_CNPJ_REMENTE_FISCAL";
        private const string NfeTravaSaldoPorcentagem = "NFE_TRAVA_SALDO_PORCENTAGEM";
        private const string ConfGeralCteVerificarCnpjDestinatarioFiscal = "CTE_VERIFICAR_CNPJ_DESTINATARIO_FISCAL";
        private const string ConfGeralCteVerificarCfop = "CTE_VERIFICAR_CFOP";
        private const string ConfGeralCteMesesRetroativosNf = "CTE_MESES_RETROATIVOS_NF";
        private const string ConfGeralNfeTravaSaldo = "NFE_TRAVA_SALDO";
        private readonly IConfiguracaoTranslogicRepository _configuracaoTranslogicRepository;
        private readonly IFluxoComercialRepository _fluxoComercialRepository;
        private readonly IAssociaFluxoInternacionalRepository _associaFluxoInternacionalRepository;
        private readonly IEmpresaFerroviaRepository _empresaFerroviaRepository;
        private readonly IEmpresaClienteRepository _empresaClienteRepository;
        private readonly IElementoViaRepository _elementoViaRepository;
        private readonly IVagaoPatioVigenteRepository _vagaoPatioVigenteRepository;
        private readonly IDclTempRepository _dclTempRepository;
        private readonly IDclTempVagaoConteinerRepository _dclTempVagaoConteinerRepository;
        private readonly IDclTempVagaoNotaFiscalRepository _dclTempVagaoNotaFiscalRepository;
        private readonly IDclTempVagaoRepository _dclTempVagaoRepository;
        private readonly IDclErroGravacaoRepository _dclErroGravacaoRepository;
        private readonly IAssociaFluxoVigenteRepository _associaFluxoVigenteRepository;
        private readonly ILocalTaraPlacaRepository _localTaraPlacaRepository;
        private readonly ILocalizacaoVagaoVigenteRepository _localizacaoVagaoVigenteRepository;
        private readonly IComposicaoVagaoVigenteRepository _composicaoVagaoVigenteRepository;
        private readonly ISituacaoVagaoVigenteRepository _situacaoVagaoVigenteRepository;
        private readonly IVagaoRepository _vagaoRepository;
        private readonly IEstacaoMaeRepository _estacaoMaeRepository;
        private readonly IMercadoriaRepository _mercadoriaRepository;
        private readonly IConteinerRepository _conteinerRepository;
        private readonly IVagaoConteinerVigenteRepository _vagaoConteinerVigenteRepository;
        private readonly IDensidadeMercadoriaRepository _densidadeMercadoriaRepository;
        private readonly ICteSerieNumeroEmpresaUfRepository _cteSerieNumeroEmpresaUfRepository;
        private readonly ISerieDespachoUfRepository _serieDespachoUfRepository;
        private readonly IItemDespachoRepository _itemDespachoRepository;
        private readonly IEmpresaClientePaisRepository _empresaClientePaisRepository;
        private readonly ICfopAgrupamentoRepository _cfopAgrupamentoRepository;
        private readonly IConteinerLocalVigenteRepository _conteinerLocalVigRepository;
        private readonly IRotaRepository _rotaRepository;
        private readonly CteService _cteService;
        private readonly ILogTempoCarregamentoRepository _logTempoCarregamentoRepository;
        private readonly ILogTempoCarregamentoVagaoRepository _logTempoCarregamentoVagaoRepository;
        private readonly ILogTempoCarregamentoVagaoNotaFiscalRepository _logTempoCarregamentoVagaoNotaFiscalRepository;
        private readonly ILogCarregamentoNfeRepository _logCarregamentoNfeRepository;
        private readonly IFluxoExcluirPortoferRepository _fluxoExcluirPortoferRepository;
        private readonly ICteRepository _cteRepository;
        private readonly IContratoRepository _contratoRepository;
        private readonly IEmpresaMargemLiberacaoFatRepository _empresaMargemLiberacaoFatRepository;
        private readonly IStatusNfeRepository _statusNfeRepository;
        private readonly INfeSimconsultasRepository _nfeSimconsultasRepository;
        private readonly IStatusRetornoNfeRepository _statusRetornoNfeRepository;
        private static int id = 0;

        #endregion Declarations

        /// <summary>
        /// Initializes a new instance of the <see cref="CarregamentoService"/> class.
        /// </summary>
        /// <param name="fluxoComercialRepository"> Repositório de fluxo comercial injetado. </param>
        /// <param name="empresaFerroviaRepository"> Repositório de empresa de ferrovia injetado </param>
        /// <param name="elementoViaRepository"> Repositório de elemento de via injetado</param>
        /// <param name="vagaoPatioVigenteRepository"> Repositório de vagão patio vigente injetado </param>
        /// <param name="dclTempRepository">Repositório de DCL Temp Injetado </param>
        /// <param name="associaFluxoInternacionalRepository">Repositório de associação de fluxo internacional</param>
        /// <param name="empresaClienteRepository"> Repositório de empresa cliente injetado </param>
        /// <param name="dclTempVagaoConteinerRepository">Repositorio de DclTempVagaoConteiner injetado </param>
        /// <param name="dclTempVagaoNotaFiscalRepository"> Repositorio de DclTempVagaoNotaFiscal Injetado</param>
        /// <param name="dclTempVagaoRepository"> Repositório de DCL Temp Vagao Injetado</param>
        /// <param name="dclErroGravacaoRepository"> Repositorio de Erro de Gravacao Injetado </param>
        /// <param name="associaFluxoVigenteRepository"> Repositorio de associação de fluxo vigente injetado </param>
        /// <param name="localTaraPlacaRepository"> Repositorio de local tara placa injetado </param>
        /// <param name="localizacaoVagaoVigenteRepository">Repositorio de localização de vagão vigente injetado</param>
        /// <param name="composicaoVagaoVigenteRepository"> Repositório de composicao de vagao vigente injetado</param>
        /// <param name="situacaoVagaoVigenteRepository"> Repositório de situação de vagão vigente injetado </param>
        /// <param name="vagaoRepository">Repositório de vagão injetado </param>
        /// <param name="configuracaoTranslogicRepository">Repositório de configuração do translogic injetado </param>
        /// <param name="estacaoMaeRepository"> Repositório de estação mãe injetado</param>
        /// <param name="mercadoriaRepository">Repositório mercadoria injetado</param>
        /// <param name="conteinerRepository"> Repositório de conteiner injetado</param>
        /// <param name="vagaoConteinerVigenteRepository">Repositório de vagão conteiner vigente injetado</param>
        /// <param name="densidadeMercadoriaRepository"> Repositório de densidade de mercadoria injetado </param>
        /// <param name="cteSerieNumeroEmpresaUfRepository"> Repositorio injetado de serie numero empresa uf</param>
        /// <param name="serieDespachoUfRepository"> Repositório injetado de série despacho uf</param>
        /// <param name="itemDespachoRepository">Repositório injetado de item de despacho</param>
        /// <param name="empresaClientePaisRepository">Repositório de empresa cliente pais injetado</param>
        /// <param name="cfopAgrupamentoRepository">Repositorio de CfopAgrupamento injetado</param>
        /// <param name="conteinerlocalVigRepository">Repositorio de Local Vigente do Conteiner injetado</param>
        /// <param name="rotaRepository">Repositorio de Rota injetado</param> 
        /// <param name="cteService">Serviço do Cte injetado</param>
        /// <param name="logTempoCarregamentoRepository">Repositorio de log do tempo de carregamento injetado</param>
        /// <param name="logTempoCarregamentoVagaoRepository">Repositorio do log do tempo de carregamento do vagão injetado</param>
        /// <param name="logTempoCarregamentoVagaoNotaFiscalRepository">Repositorio do log do tempo de carregamento da nf do vagão injetado</param>
        /// <param name="logCarregamentoNfeRepository"> Repositorio do log do tempo de consulta de nfe no carregamento injetado</param>
        /// <param name="fluxoExcluirPortoferRepository">Repositorio de FluxoExcluirPortofer injetado</param>
        /// <param name="cteRepository"> Repositorio de Cte Injetado </param>
        /// <param name="contratoRepository">Repositorio de contrato injetado</param>
        /// <param name="empresaMargemLiberacaoFatRepository">Repositório de empresa Margem liberação faturamento injetado</param>
        /// <param name="statusNfeRepository"> Repositório da Status Nfe.</param>
        /// <param name="nfeSimconsultasRepository">Repositório da NfeSimConsultas</param>
        /// <param name="statusRetornoNfeRepository">Repositório da Status Retorno Nfe</param>
        public CarregamentoService(IFluxoComercialRepository fluxoComercialRepository, IEmpresaFerroviaRepository empresaFerroviaRepository, IElementoViaRepository elementoViaRepository, IVagaoPatioVigenteRepository vagaoPatioVigenteRepository, IDclTempRepository dclTempRepository, IAssociaFluxoInternacionalRepository associaFluxoInternacionalRepository, IEmpresaClienteRepository empresaClienteRepository, IDclTempVagaoConteinerRepository dclTempVagaoConteinerRepository, IDclTempVagaoNotaFiscalRepository dclTempVagaoNotaFiscalRepository, IDclTempVagaoRepository dclTempVagaoRepository, IDclErroGravacaoRepository dclErroGravacaoRepository, IAssociaFluxoVigenteRepository associaFluxoVigenteRepository, ILocalTaraPlacaRepository localTaraPlacaRepository, ILocalizacaoVagaoVigenteRepository localizacaoVagaoVigenteRepository, IComposicaoVagaoVigenteRepository composicaoVagaoVigenteRepository, ISituacaoVagaoVigenteRepository situacaoVagaoVigenteRepository, IVagaoRepository vagaoRepository, IConfiguracaoTranslogicRepository configuracaoTranslogicRepository, IEstacaoMaeRepository estacaoMaeRepository, IMercadoriaRepository mercadoriaRepository, IConteinerRepository conteinerRepository, IVagaoConteinerVigenteRepository vagaoConteinerVigenteRepository, IDensidadeMercadoriaRepository densidadeMercadoriaRepository, ICteSerieNumeroEmpresaUfRepository cteSerieNumeroEmpresaUfRepository, ISerieDespachoUfRepository serieDespachoUfRepository, IItemDespachoRepository itemDespachoRepository, IEmpresaClientePaisRepository empresaClientePaisRepository, ICfopAgrupamentoRepository cfopAgrupamentoRepository, IConteinerLocalVigenteRepository conteinerlocalVigRepository, IRotaRepository rotaRepository, CteService cteService, ILogTempoCarregamentoRepository logTempoCarregamentoRepository, ILogTempoCarregamentoVagaoRepository logTempoCarregamentoVagaoRepository, ILogTempoCarregamentoVagaoNotaFiscalRepository logTempoCarregamentoVagaoNotaFiscalRepository, ILogCarregamentoNfeRepository logCarregamentoNfeRepository, IFluxoExcluirPortoferRepository fluxoExcluirPortoferRepository, ICteRepository cteRepository, IContratoRepository contratoRepository, IEmpresaMargemLiberacaoFatRepository empresaMargemLiberacaoFatRepository, IStatusNfeRepository statusNfeRepository, INfeSimconsultasRepository nfeSimconsultasRepository, IStatusRetornoNfeRepository statusRetornoNfeRepository)
        {
            _fluxoComercialRepository = fluxoComercialRepository;
            _statusRetornoNfeRepository = statusRetornoNfeRepository;
            _nfeSimconsultasRepository = nfeSimconsultasRepository;
            _statusNfeRepository = statusNfeRepository;
            _empresaMargemLiberacaoFatRepository = empresaMargemLiberacaoFatRepository;
            _contratoRepository = contratoRepository;
            _cteRepository = cteRepository;
            _fluxoExcluirPortoferRepository = fluxoExcluirPortoferRepository;
            _logCarregamentoNfeRepository = logCarregamentoNfeRepository;
            _logTempoCarregamentoVagaoNotaFiscalRepository = logTempoCarregamentoVagaoNotaFiscalRepository;
            _logTempoCarregamentoVagaoRepository = logTempoCarregamentoVagaoRepository;
            _logTempoCarregamentoRepository = logTempoCarregamentoRepository;
            _cteService = cteService;
            _rotaRepository = rotaRepository;
            _cfopAgrupamentoRepository = cfopAgrupamentoRepository;
            _empresaClientePaisRepository = empresaClientePaisRepository;
            _itemDespachoRepository = itemDespachoRepository;
            _serieDespachoUfRepository = serieDespachoUfRepository;
            _cteSerieNumeroEmpresaUfRepository = cteSerieNumeroEmpresaUfRepository;
            _densidadeMercadoriaRepository = densidadeMercadoriaRepository;
            _vagaoConteinerVigenteRepository = vagaoConteinerVigenteRepository;
            _conteinerRepository = conteinerRepository;
            _mercadoriaRepository = mercadoriaRepository;
            _estacaoMaeRepository = estacaoMaeRepository;
            _configuracaoTranslogicRepository = configuracaoTranslogicRepository;
            _vagaoRepository = vagaoRepository;
            _situacaoVagaoVigenteRepository = situacaoVagaoVigenteRepository;
            _composicaoVagaoVigenteRepository = composicaoVagaoVigenteRepository;
            _localizacaoVagaoVigenteRepository = localizacaoVagaoVigenteRepository;
            _localTaraPlacaRepository = localTaraPlacaRepository;
            _associaFluxoVigenteRepository = associaFluxoVigenteRepository;
            _dclErroGravacaoRepository = dclErroGravacaoRepository;
            _dclTempVagaoRepository = dclTempVagaoRepository;
            _dclTempVagaoNotaFiscalRepository = dclTempVagaoNotaFiscalRepository;
            _dclTempVagaoConteinerRepository = dclTempVagaoConteinerRepository;
            _empresaClienteRepository = empresaClienteRepository;
            _associaFluxoInternacionalRepository = associaFluxoInternacionalRepository;
            _dclTempRepository = dclTempRepository;
            _vagaoPatioVigenteRepository = vagaoPatioVigenteRepository;
            _elementoViaRepository = elementoViaRepository;
            _empresaFerroviaRepository = empresaFerroviaRepository;
            _conteinerLocalVigRepository = conteinerlocalVigRepository;
        }

        private int GetNewId()
        {
            return ++id;
        }

        /// <summary>
        /// Calcula o peso do vagão pelo volume informado e mercadoria
        /// </summary>
        /// <param name="codigoMercadoria">Código da mercadoria</param>
        /// <param name="volume">Volume informado</param>
        /// <returns>Peso do vagão</returns>
        public double CalcularPesoPorVolume(string codigoMercadoria, double volume)
        {
            DensidadeMercadoria densidadeMercadoria = _densidadeMercadoriaRepository.ObterPorId(codigoMercadoria);
            if (densidadeMercadoria == null)
            {
                throw new TranslogicException("Não existe Densidade cadastrada para a mercadoria.");
            }

            if (densidadeMercadoria.Densidade <= 0 || densidadeMercadoria.Densidade == 1)
            {
                throw new TranslogicException("Densidade da mercadoria é inválida");
            }

            double peso = volume * densidadeMercadoria.Densidade;
            return Math.Round(peso, 3);
        }

        /// <summary>
        /// Obtém lista de empresas ferroviárias
        /// </summary>
        /// <returns>Lista de empresas</returns>
        public IList<EmpresaFerrovia> ObterFerrovias()
        {
            return _empresaFerroviaRepository.ObterTodos().OrderBy(c => c.Sigla).ThenBy(c => c.DescricaoResumida).ToList();
        }

        /// <summary>
        /// Verifica o Conteiner Vazio
        /// </summary>
        /// <param name="fluxo">Fluxo Comercial</param>
        /// <returns>Retorna o peso do conteiner ou vazio</returns>
        public string VerificarConteinerVazio(FluxoComercial fluxo)
        {
            // Regra para conteiner vazio.
            if (!string.IsNullOrEmpty(fluxo.ContainerVazio) && fluxo.ContainerVazio.ToUpper().Equals("S") && fluxo.Codigo != "99999")
            {
                // Caso conteiner vazio de 20t, então seta os dados de série, número, data e Peso
                Regex regex = new Regex(@"^CONT.*20");

                if (regex.Match(fluxo.Mercadoria.DescricaoResumida).Success)
                {
                    ConfiguracaoTranslogic pesoConteiner = _configuracaoTranslogicRepository.ObterPorId("PESO_CONT_20");

                    return pesoConteiner.Valor;
                }

                // Caso conteiner vazio de 40t, então seta os dados de série, número, data e Peso
                regex = new Regex(@"^CONT.*40");

                if (regex.Match(fluxo.Mercadoria.DescricaoResumida).Success)
                {
                    ConfiguracaoTranslogic pesoConteiner = _configuracaoTranslogicRepository.ObterPorId("PESO_CONT_40");

                    return pesoConteiner.Valor;
                }
            }

            return string.Empty;
        }

        /// <summary>
        /// Verifica se o fluxo é de conteiner vazio
        /// </summary>
        /// <param name="fluxo">Fluxo Comercial</param>
        /// <returns>Retorna o peso do conteiner ou vazio</returns>
        public bool VerificarFluxoDeConteinerVazio(FluxoComercial fluxo)
        {
            // Regra para conteiner vazio.
            if (!string.IsNullOrEmpty(fluxo.ContainerVazio) && fluxo.ContainerVazio.ToUpper().Equals("S") && fluxo.Codigo != "99999")
            {
                // Caso conteiner vazio de 20t, então seta os dados de série, número, data e Peso
                Regex regex = new Regex(@"^CONT.*20");

                if (regex.Match(fluxo.Mercadoria.DescricaoResumida).Success)
                {
                    return true;
                }

                // Caso conteiner vazio de 40t, então seta os dados de série, número, data e Peso
                regex = new Regex(@"^CONT.*40");

                if (regex.Match(fluxo.Mercadoria.DescricaoResumida).Success)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Obtém as linhas de recebimento de intercâmbio
        /// </summary>
        /// <param name="idAreaOperacional">Id da AreaOperacional</param>
        /// <returns>Lista de linhas</returns>
        public IList<ElementoVia> ObterLinhasRecebimentoIntercambio(int idAreaOperacional)
        {
            return _elementoViaRepository.ObterPorIdEstacaoMae(idAreaOperacional).OrderBy(c => c.Codigo).ToList();
        }

        /// <summary>
        /// Obtém os vagões de carregamento
        /// </summary>
        /// <param name="idEstacao">Id da estação de origem</param>
        /// <param name="idFluxo">Id do fluxo comercial</param>
        /// <param name="codigoVagao">Codigo do Vagão </param>
        /// <returns>Lista de vagões para carregamento</returns>
        public IList<VagaoCarregamentoDto> ObterVagoesCarregamento(int idEstacao, int idFluxo, string codigoVagao)
        {
            return _vagaoPatioVigenteRepository.ObterParaCarregamentoPorIdEstacaoMae(idEstacao, idFluxo, codigoVagao);
        }

        /// <summary>
        /// Obtém os vagões de carregamento por codigo de vagão
        /// </summary>
        /// <param name="codigoVagao">Código do vagão</param>
        /// <returns>Lista de vagões para carregamento</returns>
        public IList<VagaoCarregamentoDto> ObterVagaoCarregamentoIntercambio(string codigoVagao)
        {
            IList<LocalizacaoVagaoVigente> listVagaoLocal = _localizacaoVagaoVigenteRepository.ObterPorCodigoVagao(codigoVagao);
            return ConverterVagaoCarregamentoDto(listVagaoLocal);
        }

        /// <summary>
        /// Obtém a empresa pelo CNPJ
        /// </summary>
        /// <param name="cnpj"> Cnpj da Empresa Cliente. </param>
        /// <param name="codigoFluxo">Codigo do fluxo comercial</param>
        /// <param name="idAreaOperacional"> Id da AreaOperacional</param>
        /// <returns> Empresa Cliente </returns>
        public IEmpresa ObterEmpresaPorCnpj(string cnpj, string codigoFluxo, int? idAreaOperacional)
        {
            if (string.IsNullOrEmpty(cnpj))
            {
                return null;
            }

            if (codigoFluxo.Equals("99999"))
            {
                EstacaoMae estacaoMae = _estacaoMaeRepository.ObterPorId(idAreaOperacional);
                return _empresaClienteRepository.ObterPorCnpj(cnpj, estacaoMae.Municipio.Estado.Sigla);
            }

            IList<EmpresaCliente> listaEmpresas = _empresaClienteRepository.ObterEmpresasPorCnpj(cnpj);
            if (listaEmpresas.Count == 0)
            {
                return null;
            }

            if (listaEmpresas.Count == 1)
            {
                return listaEmpresas[0];
            }

            foreach (EmpresaCliente empresaCliente in listaEmpresas)
            {
                var sigla = empresaCliente.Sigla;
                if (!string.IsNullOrEmpty(sigla))
                {
                    if (sigla.Substring(sigla.Length - 2).StartsWith("-"))
                    {
                        return empresaCliente;
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Obtém a empresa pelo CNPJ
        /// </summary>
        /// <param name="cnpj"> Cnpj da Empresa Cliente. </param>
        /// <param name="codigoFluxo">Codigo do fluxo comercial</param>
        /// <param name="siglaUf">Sigla da UF</param>
        /// <returns> Empresa Cliente </returns>
        public IEmpresa ObterEmpresaPorCnpj(string cnpj, string codigoFluxo, string siglaUf)
        {
            if (string.IsNullOrEmpty(cnpj))
            {
                return null;
            }

            if (codigoFluxo.Equals("99999"))
            {
                return _empresaClienteRepository.ObterPorCnpj(cnpj, siglaUf);
            }

            return _empresaClienteRepository.ObterPorCnpj(cnpj);
        }

        /// <summary>
        /// Obtém a empresa pelo CNPJ
        /// </summary>
        /// <param name="cnpj"> Cnpj da Empresa Cliente. </param>
        /// <param name="razaoSocial">Razão social da empresa</param>
        /// <returns> Empresa Cliente </returns>
        public IList<EmpresaCliente> ObterEmpresaPorCnpjRazaoSocial(string cnpj, string razaoSocial)
        {
            return _empresaClienteRepository.ObterEmpresaClientePorCnpjRazaoSocialDaFerrovia(cnpj, razaoSocial);
        }

        /// <summary>
        /// Grava os dados da nota quando é obtida manualmente.
        /// </summary>
        /// <param name="chave"> Chave da nota</param>
        /// <param name="conteiner">Numero do Conteiner</param>
        /// <param name="serie">Serie da Nfe</param>
        /// <param name="numero">Numero da Nfe</param>
        /// <param name="valorNota">Valor da Nota</param>
        /// <param name="valorTotal">Valor Total da Nota</param>
        /// <param name="remetente">Remetente da nota</param>
        /// <param name="destinatario">Destinatario da nota</param>
        /// <param name="cnpjRemetente">Cnpj do remetente da nota</param>
        /// <param name="cnpjDestinatario">Cnpj do Destinatario da nota</param>
        /// <param name="dataNotaFiscal">Data da Nota</param>
        /// <param name="siglaRemetente">Sigla do Remetente</param>
        /// <param name="siglaDestinatario">Sigla do Destinatario</param>
        /// <param name="estadoRemetente"> UF Remetente da Nota</param>
        /// <param name="estadoDestinatario"> UF Destinatario da nota</param>
        /// <param name="inscricaoEstadualRemetente">IE do Remetente</param>
        /// <param name="inscricaoEstadualDestinatario">IE do Destinatario</param>
        /// <param name="volumeNotaFiscal">Volume da Nota</param>
        /// <param name="pesoTotal"> Peso Total da nota</param>
        /// <param name="pesoRateio">Peso Rateio da nota</param>
        /// <param name="tif">tif da nota fiscal</param>
        /// <param name="placaCavalo">Placa Cavalo da nota</param>
        /// <param name="placaCarreta">Placa Carreta da nota</param>
        /// <param name="usuario">Usuario que realizou a ação</param>
        [Transaction]
        public virtual void ProcessarNfeManual(string chave, string conteiner, string serie, string numero, string valorNota, string valorTotal, string remetente, string destinatario, string cnpjRemetente, string cnpjDestinatario, string dataNotaFiscal, string siglaRemetente, string siglaDestinatario, string estadoRemetente, string estadoDestinatario, string inscricaoEstadualRemetente, string inscricaoEstadualDestinatario, string volumeNotaFiscal, string pesoTotal, string pesoRateio, string tif, string placaCavalo, string placaCarreta, Usuario usuario)
        {
            StatusNfe statusNfe = _statusNfeRepository.ObterPorChaveNfe(chave);

            if (statusNfe == null)
            {
                statusNfe = new StatusNfe();
                statusNfe.ChaveNfe = chave;
                statusNfe.StatusRetornoNfe = _statusRetornoNfeRepository.ObterPorCodigo("NM1");
            }
            else
            {
                if (statusNfe.StatusRetornoNfe != null && !statusNfe.StatusRetornoNfe.Codigo.Equals("V07"))
                {
                    statusNfe.StatusRetornoNfe = _statusRetornoNfeRepository.ObterPorCodigo("NM1");
                }
            }

            statusNfe.Peso = string.IsNullOrEmpty(pesoTotal) ? 0 : double.Parse(pesoTotal) / 1000;
            statusNfe.Volume = string.IsNullOrEmpty(volumeNotaFiscal) ? 0 : double.Parse(volumeNotaFiscal) / 1000;

            statusNfe.DataCadastro = DateTime.Now;
            statusNfe.Origem = "NfeManual";
            statusNfe.UsuarioCadastro = usuario;

            _statusNfeRepository.InserirOuAtualizar(statusNfe);

            NfeSimconsultas nfeSimconsultas = _nfeSimconsultasRepository.ObterPorChaveNfe(chave);

            if (nfeSimconsultas == null)
            {
                nfeSimconsultas = new NfeSimconsultas();
                nfeSimconsultas.ChaveNfe = chave;
            }

            EmpresaCliente empresaEmit = _empresaClienteRepository.ObterPorSiglaSap(siglaRemetente);
            EmpresaCliente empresaDst = _empresaClienteRepository.ObterPorSiglaSap(siglaDestinatario);

            if (empresaEmit != null)
            {
                // Dados do emitente
                nfeSimconsultas.NomeFantasiaEmitente = empresaEmit.NomeFantasia;
                nfeSimconsultas.RazaoSocialEmitente = empresaEmit.RazaoSocial;
                nfeSimconsultas.UfEmitente = empresaEmit.Estado.Sigla;
                nfeSimconsultas.CepEmitente = empresaEmit.Cep.Replace(".", string.Empty).Replace("-", string.Empty).Trim();
                nfeSimconsultas.CnpjEmitente = empresaEmit.Cgc.Replace(".", string.Empty).Replace("-", string.Empty).Replace("/", string.Empty).Trim();

                nfeSimconsultas.CodigoMunicipioEmitente = empresaEmit.CidadeIbge.CodigoIbge.ToString();
                nfeSimconsultas.CodigoPaisEmitente = empresaEmit.Estado.Pais.Codigo;
                nfeSimconsultas.InscricaoEstadualEmitente = empresaEmit.InscricaoEstadual.Replace(".", string.Empty).Replace("-", string.Empty).Trim();

                nfeSimconsultas.LogradouroEmitente = empresaEmit.Endereco;
                nfeSimconsultas.MunicipioEmitente = empresaEmit.CidadeIbge.Descricao;
                nfeSimconsultas.PaisEmitente = empresaEmit.Estado.Pais.DescricaoResumida;
                nfeSimconsultas.TelefoneEmitente = empresaEmit.Telefone1;
            }

            if (empresaDst != null)
            {
                // Dados do Destinatario
                nfeSimconsultas.NomeFantasiaDestinatario = empresaDst.NomeFantasia;
                nfeSimconsultas.RazaoSocialDestinatario = empresaDst.RazaoSocial;
                nfeSimconsultas.UfDestinatario = empresaDst.Estado.Sigla;
                nfeSimconsultas.CepDestinatario = empresaDst.Cep.Replace(".", string.Empty).Replace("-", string.Empty).Trim();
                nfeSimconsultas.CnpjDestinatario = empresaDst.Cgc.Replace(".", string.Empty).Replace("-", string.Empty).Replace("/", string.Empty).Trim();

                nfeSimconsultas.CodigoMunicipioDestinatario = empresaDst.CidadeIbge.CodigoIbge.ToString();
                nfeSimconsultas.CodigoPaisDestinatario = empresaDst.Estado.Pais.Codigo;
                nfeSimconsultas.InscricaoEstadualDestinatario = empresaDst.InscricaoEstadual.Replace(".", string.Empty).Replace("-", string.Empty).Trim();

                nfeSimconsultas.LogradouroDestinatario = empresaDst.Endereco;
                nfeSimconsultas.MunicipioDestinatario = empresaDst.CidadeIbge.Descricao;
                nfeSimconsultas.PaisDestinatario = empresaDst.Estado.Pais.DescricaoResumida;
                nfeSimconsultas.TelefoneDestinatario = empresaDst.Telefone1;
            }

            // Dados da Nota
            nfeSimconsultas.SerieNotaFiscal = serie;
            nfeSimconsultas.NumeroNotaFiscal = int.Parse(numero);
            nfeSimconsultas.Valor = double.Parse(valorTotal) / 100;
            nfeSimconsultas.DataEmissao = DateTime.Parse(dataNotaFiscal);
            nfeSimconsultas.Volume = string.IsNullOrEmpty(volumeNotaFiscal) ? 0 : double.Parse(volumeNotaFiscal);
            nfeSimconsultas.Peso = string.IsNullOrEmpty(pesoRateio) ? 0 : double.Parse(pesoRateio);
            nfeSimconsultas.PesoBruto = string.IsNullOrEmpty(pesoTotal) ? 0 : double.Parse(pesoTotal);
            nfeSimconsultas.Placa = placaCarreta;
            nfeSimconsultas.PlacaReboque = placaCavalo;

            _nfeSimconsultasRepository.InserirOuAtualizar(nfeSimconsultas);
        }

        /// <summary>
        /// Obtém os dados do vagão, verificando se está em trem, patio ou intercambio
        /// </summary>
        /// <param name="codigoVagao"> The codigo vagao. </param>
        /// <returns>Dto com os dados do vagão </returns>
        public DadosVagaoForaCarregamentoDto ObterDadosVagao(string codigoVagao)
        {
            Vagao vagao = _vagaoRepository.ObterPorCodigo(codigoVagao);
            if (vagao == null)
            {
                throw new TranslogicException("O vagão {0} não está cadastrado.", codigoVagao);
            }

            DadosVagaoForaCarregamentoDto dados = new DadosVagaoForaCarregamentoDto();
            dados.Intercambio = false;

            SituacaoVagaoVigente situacaoVagaoVigente = _situacaoVagaoVigenteRepository.ObterPorVagao(vagao);
            if (situacaoVagaoVigente == null)
            {
                throw new TranslogicException("O vagão {0} não possui situação vigente cadastrada", codigoVagao);
            }

            dados.Situacao = situacaoVagaoVigente.Situacao.Descricao;

            ComposicaoVagaoVigente composicaoVagaoVigente = _composicaoVagaoVigenteRepository.ObterPorVagao(vagao);
            if (composicaoVagaoVigente != null)
            {
                dados.PrefixoTrem = composicaoVagaoVigente.Composicao.Trem.Prefixo;
                return dados;
            }

            VagaoPatioVigente vagaoPatioVigente = _vagaoPatioVigenteRepository.ObterPorVagao(vagao);
            if (vagaoPatioVigente != null)
            {
                dados.Patio = vagaoPatioVigente.Patio.EstacaoMae == null ? vagaoPatioVigente.Patio.Codigo : vagaoPatioVigente.Patio.EstacaoMae.Codigo;
            }

            LocalizacaoVagaoVigente localizacaoVagaoVigente = _localizacaoVagaoVigenteRepository.ObterEmOutraFerroviaPorVagao(vagao);
            if (localizacaoVagaoVigente != null)
            {
                dados.Intercambio = true;
                return dados;
            }

            return dados;
        }

        /// <summary>
        /// Obtém a mercadoria pelo código
        /// </summary>
        /// <param name="codigoMercadoria">Código da mercadoria</param>
        /// <returns>Objeto mercadoria</returns>
        public Mercadoria ObterMercadoriaPorCodigo(string codigoMercadoria)
        {
            return _mercadoriaRepository.ObterPorCodigo(codigoMercadoria);
        }

        /// <summary>
        /// Obtém a estação mãe pelo código
        /// </summary>
        /// <param name="codigo">Código da estaçao mãe</param>
        /// <returns>Objeto estação mãe</returns>
        public EstacaoMae ObterEstacaoMaePorCodigo(string codigo)
        {
            return _estacaoMaeRepository.ObterPorCodigo(codigo) as EstacaoMae;
        }

        /// <summary>
        /// Obtém os meses retroativos limite da nota fiscal
        /// </summary>
        /// <returns>Meses retroativos</returns>
        public int ObterMesesRetroativos()
        {
            ConfiguracaoTranslogic configuracao = _configuracaoTranslogicRepository.ObterPorId(ConfGeralCteMesesRetroativosNf);
            int mesesRetroativos;
            try
            {
                mesesRetroativos = int.Parse(configuracao.Valor);
            }
            catch (Exception)
            {
                mesesRetroativos = 6;
            }

            return mesesRetroativos;
        }

        /// <summary>
        /// Obtém o cnpj fiscal pela estação
        /// </summary>
        /// <param name="estacao">Estação a ser obtido o cnpj fiscal</param>
        /// <returns>Cnpj da Empresa</returns>
        public IEmpresa ObterEmpresaPorEstacao(EstacaoMae estacao)
        {
            SerieDespachoUf serieDespachoUf = _serieDespachoUfRepository.ObterPorEmpresaUf(estacao.EmpresaConcessionaria, estacao.Municipio.Estado.Sigla);
            if (serieDespachoUf != null)
            {
                return _empresaClienteRepository.ObterPorCnpj(serieDespachoUf.CnpjFerrovia, estacao.Municipio.Estado.Sigla);
            }

            return null;
        }

        /// <summary>
        /// Obtém o cnpj fiscal pela estação
        /// </summary>
        /// <param name="estacao">Estação a ser obtido o cnpj fiscal</param>
        /// <returns>Cnpj da Empresa</returns>
        public string ObterCnpjFiscalPorEstacao(EstacaoMae estacao)
        {
            SerieDespachoUf serieDespachoUf = _serieDespachoUfRepository.ObterPorEmpresaUf(estacao.EmpresaConcessionaria, estacao.Municipio.Estado.Sigla);
            if (serieDespachoUf != null)
            {
                return serieDespachoUf.CnpjFerrovia;
            }

            return string.Empty;
        }

        /// <summary>
        /// Obtém as SerieDespachoUf
        /// </summary>
        /// <returns>lista de SerieDespachoUf</returns>
        public IList<SerieDespachoUf> ObterCodigoSerie()
        {
            return _serieDespachoUfRepository.ObterTodos();
        }

        /// <summary>
        /// Obtém o valor booleano da conf geral pela chave
        /// </summary>
        /// <param name="chave">Chave da confgeral</param>
        /// <returns>Valor booleano</returns>
        public bool ObterBooleanoConfGeral(string chave)
        {
            ConfiguracaoTranslogic configuracaoTranslogic = _configuracaoTranslogicRepository.ObterPorId(chave);
            try
            {
                string valor = configuracaoTranslogic.Valor;
                if (valor.Equals("S"))
                {
                    return true;
                }

                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Obtém o Peso total do vagão quando multiplo despacho
        /// </summary>
        /// <param name="idVagao">Id do vagão</param>
        /// <param name="pesoTotal">Peso Total</param>
        /// <param name="pesoRemanescente"> Peso Remanescente</param>
        /// <returns>Peso total do vagão se já estiver despachado uma vez como múltiplo despacho</returns>
        public bool ObterPesosMultiploDespacho(int? idVagao, out double pesoTotal, out double pesoRemanescente)
        {
            Vagao vagao = new Vagao { Id = idVagao };
            IList<ItemDespacho> lista = _itemDespachoRepository.ObterPorVagaoComPedidoVigente(vagao);
            if (lista.Count > 0)
            {
                pesoTotal = lista.Select(c => c.PesoTotalVagao).First().Value * 1000;
                pesoRemanescente = pesoTotal - lista.Sum(c => c.PesoToneladaUtil.Value * 1000);

                pesoTotal = pesoTotal / 1000;
                pesoRemanescente = pesoRemanescente / 1000;
                return true;
            }

            pesoRemanescente = 0;
            pesoTotal = 0;
            return false;
        }

        /// <summary>
        /// Obtém o Peso total do vagão quando multiplo despacho
        /// </summary>
        /// <param name="idVagao">Id do vagão</param>
        /// <returns>Peso total do vagão se já tiver na vagao pedido vig</returns>
        public double ObterPesoTotalCarregamentoPorVagaoVigente(string chaveCte)
        {
            return (Double)_cteRepository.ObterSomaPesoNotasCtesPorVagaoPedidoVigente(chaveCte);
        }

        /// <summary>
        /// Obtém o Peso total do vagão e fluxo quando multiplo despacho
        /// </summary>
        /// <param name="idVagao">Id do vagão</param>
        /// <returns>Peso total do vagão se já tiver na vagao pedido vig</returns>
        public double ObterPesoTotalCarregamentoPorVagaoVigenteFluxo(string chaveCte)
        {
            return (Double)_cteRepository.ObterSomaPesoNotasCtesPorVagaoPedidoVigenteFluxo(chaveCte);
        }

        /// <summary>
        /// Obtém o Peso total do vagão quando multiplo despacho
        /// </summary>
        /// <param name="chaveCte">Id do vagão</param>
        /// <returns>Verifica se foi finalizado o múltiplo despacho do vagão pelo flag do cte.</returns>
        public bool VerificaMultiploDespachoFinalizado(string chaveCte)
        {
            return _cteRepository.ExisteCteFinalizaMultiploDespachoVagaoVigente(chaveCte);
        }

        /// <summary>
        /// Obtém o Peso total do vagão quando multiplo despacho
        /// </summary>
        /// <param name="chaveCte">Id do vagão</param>
        /// <returns>Verifica se o vagão possui algum fluxo que é rateio de CTE.</returns>
        public bool VerificaPossuiFluxoRateioCte(string chaveCte)
        {
            return _cteRepository.ExistemCtesRateioVagaoVigente(chaveCte);
        }

        /// <summary>
        /// Obtém o Peso total do vagão quando multiplo despacho
        /// </summary>
        /// <param name="idVagao">Id do vagão</param>
        /// <returns>Verifica o número de conteiners do Vagão vigente.</returns>
        public int ObterNumeroConteinersPorVagaoVigente(string chaveCte)
        {
            return _conteinerRepository.ObterTotalConteinersVagaoVigente(chaveCte);
        }

        /// <summary>
        /// Obtém o Número de conteiners por vagão vigente e fluxo
        /// </summary>
        /// <param name="idVagao">Id do vagão</param>
        /// <returns>Verifica o número de conteiners do Vagão vigente.</returns>
        public int ObterNumeroConteinersPorVagaoVigenteFluxo(string chaveCte)
        {
            return _conteinerRepository.ObterTotalConteinersVagaoVigenteFluxo(chaveCte);
        }

        public decimal ObterPesoCalculoSapDespCte(string chaveCte)
        {
            // Tarifa contrato no fluxo do cte
            var cte = _cteRepository.ObterPorChave(chaveCte);
            var tarifa = cte.FluxoComercial.Contrato.TarifaTotal ?? 0;

            ////var nroConteiners = ObterNumeroConteinersPorVagaoVigenteFluxo(chaveCte);

            ////tarifa = tarifa * nroConteiners;

            // valor do cte a receber
            var valorCte = cte.ValorCte;
            
            var valorSemArred = valorCte/tarifa;           
            
            var valorFinal = Math.Round(valorSemArred, 3);
            
            return (decimal)valorFinal;
        }

        public decimal ObterQtdeCtePorVagaoVigente(string chaveCte)
        {
            //// pega o(s) conteiner(s) do CTE no vagao vigente.
            decimal qtde = 0;
            double valorResultadoRateio = 0;
            int contCtes = 0;
            var conteiners = _conteinerRepository.ObterPorVagaoVigenteCte(chaveCte);

            //// Faz um loop nos conteiner q o cte referencia, e verifica se o cte ocupa o conteiner inteiro. se ocupar 
            //// inteiro, considera 1 unidade cheia. Se não ocupar inteiro, verifica quanto do conteiner o cte ocupa 
            //// (pegando o codigo do conteiner e verificando o numero de ctes no conteiner.)
            foreach (var conteiner in conteiners)
            {
                var ctes = _cteRepository.ObterCtesPorVagaoPedidoVigenteConteiner(chaveCte, conteiner.Nro).OrderBy(p => p.Chave);
                contCtes = ctes.Count();
                if (contCtes == 1){
                    qtde = qtde + 1;
                }
                else
                    if (contCtes > 1)
                    {
                        double pesoCte = 0;

                        // pega o peso total dos ctes do conteiner.
                        var pesoTotalConteiner = (double)ctes.Sum(p => p.PesoVagaoDec);

                        // cria uma nova lista de Ctes
                        IList<CteDto> ctesParaRateio = new List<CteDto>();
                        
                        // Para cada cte do conteiner, faz o rateio e guarda na lista.
                        foreach (var cteNoConteiner in ctes)
                        {
                            pesoCte = cteNoConteiner == null ? 0 : (double)cteNoConteiner.PesoVagaoDec;
                            
                            cteNoConteiner.Qtde = Math.Round((pesoCte * 1) / pesoTotalConteiner, 4);
                            
                            //cteNoConteiner.Qtde = (pesoCte * 1) / pesoTotalConteiner;
                            ctesParaRateio.Add(cteNoConteiner);
                        }

                        //// Se é o CTE que fecha o conteiner verifica se tem que acertar diferença de centavos.
                        var cteUltimo = ctesParaRateio.OrderBy(p => p.Chave).LastOrDefault();
                        if (cteUltimo.Chave == chaveCte)
                        {
                            // pega o valor dos outros CTES do conteiner pra ratear
                            double valorAnteriorsCalculado = 0;
                            foreach (var cteParaRateio in ctesParaRateio)
                            {
                                if (cteParaRateio.Chave != chaveCte)
                                {
                                    valorAnteriorsCalculado += cteParaRateio.Qtde ?? 0;
                                }
                                else
                                {
                                    valorResultadoRateio = cteParaRateio.Qtde ?? 0;
                                }
                            }

                            double valorFinalSomado = valorResultadoRateio + valorAnteriorsCalculado;

                            // se valor final é diferente do valor total do carregamento, acerta a diferença
                            if (valorFinalSomado != 1.0)
                            {
                                double diferenca = 1.0 - valorFinalSomado;
                                valorResultadoRateio = diferenca + valorResultadoRateio;
                            }

                        }
                        else
                        {
                            var cteFinal = ctesParaRateio.FirstOrDefault(p => p.Chave == chaveCte);
                            valorResultadoRateio = cteFinal == null ? 0 : cteFinal.Qtde ?? 0; 
                        }

                        qtde += (Decimal)valorResultadoRateio;
                    }
            }
            return qtde;
        }

        /// <summary>
        /// Persiste o carregamento pelo dto enviado
        /// </summary>
        /// <param name="carregamentoDto"> The carregamento dto. </param>
        /// <param name="usuario"> Usuario que está salvando o carregamento</param>
        /// <returns> Lista de dtos de carregamento de vagão</returns>
        public List<CarregamentoVagaoDto> SalvarCarregamento(CarregamentoDto carregamentoDto, Usuario usuario)
        {
            FluxoComercial fluxo = VerificarValidadeFluxoCarregamento(carregamentoDto.IdFluxoComercial);
            PopularDadosDtoPeloFluxo(fluxo, ref carregamentoDto);
            List<CarregamentoVagaoDto> listaVagoes = SepararDadosParaGerarNovoDto(carregamentoDto);
            CarregarVagaoPorVagao(listaVagoes, carregamentoDto, fluxo, usuario);
            return listaVagoes;
        }

        /// <summary>
        /// Faz o carregamento vagão à vagão
        /// </summary>
        /// <param name="listaVagoes">Lista de vagões a serem carregados</param>
        /// <param name="carregamentoDto">Dto de carregamento já pré populado</param>
        /// <param name="fluxo">Flxuo comercial</param>
        /// <param name="usuario">Usuario que está fazendo a operação</param>
        public void CarregarVagaoPorVagao(List<CarregamentoVagaoDto> listaVagoes, CarregamentoDto carregamentoDto, FluxoComercial fluxo, Usuario usuario)
        {
            List<NotaFiscalVagaoCarregamentoDto> listaNotasFiscais = carregamentoDto.ListaNotasFiscais.ToList();

            foreach (CarregamentoVagaoDto vagaoTemp in listaVagoes)
            {
                List<NotaFiscalVagaoCarregamentoDto> listaNotasTemp = listaNotasFiscais.Where(c => c.IdVagao.Equals(vagaoTemp.IdVagao)).ToList();
                carregamentoDto.ListaNotasFiscais = listaNotasTemp;
                try
                {
                    SalvarCarregamentoPorTemporarias(carregamentoDto, fluxo, usuario);
                    vagaoTemp.IndErro = false;
                    vagaoTemp.Excecao = null;
                }
                catch (Exception ex)
                {
                    vagaoTemp.IndErro = true;
                    vagaoTemp.Excecao = ex;
                }
            }
        }

        /// <summary>
        /// Separa os dados
        /// </summary>
        /// <param name="carregamentoDto">Dto de carregmaento</param>
        /// <returns>Lista de vagões</returns>
        public List<CarregamentoVagaoDto> SepararDadosParaGerarNovoDto(CarregamentoDto carregamentoDto)
        {
            List<CarregamentoVagaoDto> listaVagoes = carregamentoDto.ListaNotasFiscais.GroupBy(g => new { g.IdVagao, g.CodigoVagao, g.MultiploDespacho, g.PesoDclVagao, g.PesoTotalVagao }).Select(
                s => new CarregamentoVagaoDto
                {
                    IdVagao = s.Key.IdVagao,
                    CodigoVagao = s.Key.CodigoVagao
                }).ToList();
            return listaVagoes;
        }

        /// <summary>
        /// Salva o carregamento pelas tabelas temporárias
        /// </summary>
        /// <param name="carregamentoDto">Dto de carregamento</param>	
        /// <param name="fluxoComercial"> Fluxo comercial</param>
        /// <param name="usuario"> Usuario que está salvando o carregamento </param>
        [Transaction(TransactionMode.RequiresNew)]
        public virtual void SalvarCarregamentoPorTemporarias(CarregamentoDto carregamentoDto, FluxoComercial fluxoComercial, Usuario usuario)
        {
            try
            {
                DclTemp dclTemp = GerarDclTemp(carregamentoDto, usuario);
                IList<DclTempVagao> dclTempVagao = GerarDclTempVagao(carregamentoDto.ListaNotasFiscais, fluxoComercial);
                IList<DclTempVagaoConteiner> dclTempVagaoConteiner = GerarDclTempVagaoConteiner(carregamentoDto.ListaNotasFiscais);
                IList<DclTempVagaoNotaFiscal> dclTempVagaoNotaFiscal = GerarDclTempVagaoNotaFiscal(carregamentoDto.ListaNotasFiscais);
                decimal idControleDclTemp = _dclTempRepository.ObterIdControle();
                _dclTempRepository.Inserir(dclTemp);
                _dclTempVagaoConteinerRepository.Inserir(dclTempVagaoConteiner);
                _dclTempVagaoRepository.Inserir(dclTempVagao);
                _dclTempVagaoNotaFiscalRepository.Inserir(dclTempVagaoNotaFiscal);
                try
                {
#if (!DEBUG2)
                    _dclTempRepository.GerarDespachos(idControleDclTemp.ToString("0"), carregamentoDto.CodigoTransacao);
#endif
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                    LimparTabelasTemporarias();
                    throw;
                }

                IList<DclErroGravacao> listaErros = _dclErroGravacaoRepository.ObterPorIdControle(idControleDclTemp);
                if (listaErros.Count > 0)
                {
                    StringBuilder sb = new StringBuilder();
                    foreach (DclErroGravacao dclErroGravacao in listaErros)
                    {
                        sb.AppendLine(dclErroGravacao.Mensagem);
                    }

                    throw new TranslogicException(sb.ToString());
                }
#if (!DEBUG2)
                LimparTabelasTemporarias();
#endif
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                LimparTabelasTemporarias();
                throw;
            }
        }

        /// <summary>
        /// Limpa os dados 
        /// </summary>
        public void LimparTabelasTemporarias()
        {
            _dclTempRepository.RemoverTodos();
            _dclErroGravacaoRepository.RemoverTodos();
            _dclTempVagaoConteinerRepository.RemoverTodos();
            _dclTempVagaoNotaFiscalRepository.RemoverTodos();
            _dclTempVagaoRepository.RemoverTodos();
            // _dclTempRepository.LimparDados(dclTemp, dclTempVagaoConteiner, dclTempVagao, dclTempVagaoNotaFiscal, idControleDclTemp);
        }

        /// <summary>
        /// Verifica se é para validar o cnpj do destinatario fiscal
        /// </summary>
        /// <returns>Valor booleano</returns>
        public bool VerificarCnpjDestinatarioFiscal()
        {
            return ObterBooleanoConfGeral(ConfGeralCteVerificarCnpjDestinatarioFiscal);
        }

        /// <summary>
        /// Verifica se é para validar o cnpj do destinatario fiscal
        /// </summary>
        /// <returns>Valor booleano</returns>
        public bool VerificarCnpjRemetenteFiscal()
        {
            return ObterBooleanoConfGeral(ConfGeralCteVerificarCnpjRemetenteFiscal);
        }

        /// <summary>
        /// Verifica se é para validar o cnpj do destinatario fiscal
        /// </summary>
        /// <returns>Valor booleano</returns>
        public bool VerificarCfopContrato()
        {
            return ObterBooleanoConfGeral(ConfGeralCteVerificarCfop);
        }

        /// <summary>
        /// Verifica se é para validar o saldo da NFe
        /// </summary>
        /// <param name="fluxo">Fluxo comercial</param>
        /// <param name="statusNfeDto">Status Nfe Dto</param>
        /// <returns>Valor booleano</returns>
        public bool VerificarTravaSaldoNfe(FluxoComercial fluxo, StatusNfeDto statusNfeDto)
        {
            if (fluxo.Codigo.Equals("99999"))
            {
                return false;
            }

            if (fluxo != null)
            {
                // recupera a sigla da ferrovia
                string siglaFerrovia = ALL.Core.Util.Enum<FerroviaFaturamentoEnum>.GetDescriptionOf(fluxo.Contrato.FerroviaFaturamento.Value);

                // Obtem a ferrovia atraves da sigla
                IEmpresa empresaFerrovia = _empresaFerroviaRepository.ObterPorSigla(siglaFerrovia);

                // Verifica se está na tabela excluir portofer
                FluxoExcluirPortofer fluxoExcluirPortofer = _fluxoExcluirPortoferRepository.ObterPorId(fluxo.Codigo.Substring(2));

                // Se for indempresaAll e estiver na tabela de fluxosExcluirPortofer, então não valida saldo
                if (empresaFerrovia.IndEmpresaALL.Value && fluxoExcluirPortofer != null)
                {
                    return false;
                }
            }

            if (statusNfeDto != null)
            {
                StatusNfe statusNfe = _statusNfeRepository.ObterPorChaveNfe(statusNfeDto.NotaFiscalEletronica.ChaveNfe);

                if (statusNfe.StatusRetornoNfe.Codigo == "19")
                {
                    return false;
                }
            }

            return ObterBooleanoConfGeral(ConfGeralNfeTravaSaldo);
        }

        /// <summary>
        /// Verifica se o fluxo comercial é para gerar CTe
        /// </summary>
        /// <param name="fluxo">Fluxo comercial</param>
        /// <returns>Valor booleano</returns>
        public bool VerificarFluxoCte(FluxoComercial fluxo)
        {
            if (fluxo.Codigo.Equals("99999"))
            {
                return false;
            }

            FluxoExcluirPortofer fluxoExcluirPortofer = _fluxoExcluirPortoferRepository.ObterPorId(fluxo.Codigo.Substring(2));
            if (fluxoExcluirPortofer != null)
            {
                return false;
            }

            if (fluxo.OrigemIntercambio != null && fluxo.Contrato.FerroviaFaturamento.HasValue)
            {
                if (fluxo.Origem.EmpresaConcessionaria.Sigla == ALL.Core.Util.Enum<FerroviaFaturamentoEnum>.GetDescriptionOf(fluxo.Contrato.FerroviaFaturamento.Value))
                {
                    return false;
                }
            }

            EstacaoMae areaOrigem = fluxo.OrigemIntercambio ?? fluxo.Origem;
            EstacaoMae areaDestino = fluxo.DestinoIntercambio ?? fluxo.Destino;

            bool indOrigemPortofer = VerificaEmpresaPortoFer(areaOrigem);
            bool indDestinoPortofer = VerificaEmpresaPortoFer(areaDestino);

            if (indOrigemPortofer && indDestinoPortofer)
            {
                return false;
            }

            if (!string.IsNullOrEmpty(fluxo.ContainerVazio))
            {
                if (fluxo.ContainerVazio.Trim() == "S")
                {
                    return false;
                }
            }

            IEmpresa empresa = areaOrigem.EmpresaConcessionaria;
            string uf = areaOrigem.Municipio.Estado.Sigla;
            CteSerieNumeroEmpresaUf serieNumeroEmpresaUf = _cteSerieNumeroEmpresaUfRepository.ObterPorEmpresaUf(empresa, uf);
            return serieNumeroEmpresaUf == null ? false : true;
        }

        /// <summary>
        /// Verifica se o fluxo é para liberar a data do despacho
        /// </summary>
        /// <param name="fluxo">Fluxo comercial</param>
        /// <returns>Valor booleano</returns>
        public bool VerificarLiberacaoDataDespacho(FluxoComercial fluxo)
        {
            if (fluxo.Codigo.Equals("99999"))
            {
                return true;
            }

            EstacaoMae areaOrigem = fluxo.OrigemIntercambio ?? fluxo.Origem;
            IEmpresa empresa = areaOrigem.EmpresaConcessionaria;
            string uf = areaOrigem.Municipio.Estado.Sigla;
            CteSerieNumeroEmpresaUf serieNumeroEmpresaUf = _cteSerieNumeroEmpresaUfRepository.ObterPorEmpresaUf(empresa, uf);
            if (serieNumeroEmpresaUf == null)
            {
                return true;
            }

            return serieNumeroEmpresaUf.DataInicioVigenciaTravaDataDespacho >= DateTime.Now;
        }

        /// <summary>
        /// Verifica se o fluxo comercial pode carregar
        /// </summary>
        /// <param name="codigoFluxoComercial"> The codigo fluxo comercial. </param>
        /// <param name="codigoOrigemFluxo"> The codigo origem fluxo. </param>
        /// <param name="codigoDestinoFluxo"> The codigo destino fluxo. </param>
        /// <param name="codigoMercadoria"> The codigo mercadoria. </param>
        /// <returns> Fluxo Comercial </returns>
        public FluxoComercial VerificarValidadeFluxoCarregamento(string codigoFluxoComercial, string codigoOrigemFluxo, string codigoDestinoFluxo, string codigoMercadoria)
        {
            if (codigoFluxoComercial.Equals("99999"))
            {
                return VerificarDadosFluxoPadrao(codigoFluxoComercial, codigoOrigemFluxo, codigoDestinoFluxo, codigoMercadoria);
            }

            return VerificarValidadeFluxoCarregamento(codigoFluxoComercial);
        }

        /// <summary>
        /// Verifica se o fluxo comercial é válido
        /// </summary>
        /// <param name="codigoFluxo">Código do fluxo comercial</param>
        /// <returns> Se o fluxo comercial for válido o mesmo será retornado </returns>
        public FluxoComercial VerificarValidadeFluxoCarregamento(string codigoFluxo)
        {
            FluxoComercial fluxoComercial = _fluxoComercialRepository.ObterFerroviarioPorCodigo(codigoFluxo);

            if (fluxoComercial == null)
            {
                throw new TranslogicException("Fluxo não cadastrado.");
            }

            VerificarFluxoParaCarregamento(fluxoComercial);

            return fluxoComercial;
        }

        /// <summary>
        /// Verifica se o fluxo comercial é válido
        /// </summary>
        /// <param name="codigoFluxo">Código do fluxo comercial</param>
        /// <returns> Se o fluxo comercial for válido o mesmo será retornado </returns>
        public FluxoComercial VerificarValidadeFluxoManutencao(string codigoFluxo)
        {
            FluxoComercial fluxoComercial = _fluxoComercialRepository.ObterFerroviarioPorCodigo(codigoFluxo);
            StringBuilder mensagemErro = new StringBuilder();

            if (fluxoComercial == null)
            {
                throw new TranslogicException("Fluxo não cadastrado.");
            }

            mensagemErro.Append(VerificarFluxoSaneado(fluxoComercial));

            if (mensagemErro.ToString().Trim() != string.Empty)
            {
                throw new TranslogicException(string.Concat("Foram encontrados os erros abaixo:<br/>", mensagemErro.ToString()));
            }

            return fluxoComercial;
        }

        /// <summary>
        /// Verifica se o fluxo comercial é válido
        /// </summary>
        /// <param name="idFluxo">Id do fluxo comercial</param>
        /// <returns> Se o fluxo comercial for válido o mesmo será retornado </returns>
        public FluxoComercial VerificarValidadeFluxoCarregamento(int idFluxo)
        {
            FluxoComercial fluxoComercial = _fluxoComercialRepository.ObterPorId(idFluxo);

            VerificarFluxoParaCarregamento(fluxoComercial);

            return fluxoComercial;
        }

        /// <summary>
        /// Verifica se o fluxo é válido para carregamento
        /// </summary>
        /// <param name="fluxo">Objeto fluxo comercial</param>
        public void VerificarFluxoParaCarregamento(FluxoComercial fluxo)
        {
            StringBuilder mensagemErro = new StringBuilder();

            if (!fluxo.Codigo.Equals("99999"))
            {
                if (fluxo.DataVigencia < DateTime.Now)
                {
                    mensagemErro.AppendLine("<br/> - Fluxo não está vigente.");
                }

                mensagemErro.Append(VerificarFluxoSaneado(fluxo));

                if (mensagemErro.ToString().Trim() != string.Empty)
                {
                    throw new TranslogicException(string.Concat("Foram encontrados os erros abaixo:<br/>", mensagemErro.ToString()));
                }
            }
        }

        /// <summary>
        /// Verifica se o fluxo é válido para carregamento
        /// </summary>
        /// <param name="fluxo">Objeto fluxo comercial</param>
        /// <returns> Retorna a string com os erros encontrados</returns>
        public string VerificarFluxoSaneado(FluxoComercial fluxo)
        {
            StringBuilder mensagemErro = new StringBuilder();

            if (!fluxo.Codigo.Equals("99999"))
            {
                if (fluxo.Contrato == null)
                {
                    mensagemErro.AppendLine("<br/> - Fluxo não possui contrato.");
                }
                else
                {
                    if (!_cteService.VerificarLiberacaoTravasCorrentista(fluxo))
                    {
                        if (VerificarCnpjRemetenteFiscal())
                        {
                            if (fluxo.Contrato.EmpresaRemetenteFiscal == null)
                            {
                                mensagemErro.AppendLine("<br/> - Fluxo não possui Remetente Fiscal.");
                            }
                            else
                            {
                                if (fluxo.Contrato.EmpresaRemetenteFiscal.TipoPessoa.Value.Equals(TipoPessoaEnum.Juridica))
                                {
                                    if (VerificarEmpresaNacional(fluxo.Contrato.EmpresaRemetenteFiscal))
                                    {
                                        if (string.IsNullOrEmpty(fluxo.Contrato.EmpresaRemetenteFiscal.Cgc) ||
                                            fluxo.Contrato.EmpresaRemetenteFiscal.Cgc.Trim().Equals(string.Empty))
                                        {
                                            mensagemErro.AppendLine("<br/> - Fluxo não possui o CNPJ do Remetente Fiscal.");
                                        }
                                    }
                                }
                            }
                        }

                        if (VerificarCnpjDestinatarioFiscal())
                        {
                            if (fluxo.Contrato.EmpresaDestinatariaFiscal == null)
                            {
                                mensagemErro.AppendLine("<br/> - Fluxo não possui Destinatário Fiscal.");
                            }
                            else
                            {
                                if (fluxo.Contrato.EmpresaDestinatariaFiscal.TipoPessoa.Value.Equals(TipoPessoaEnum.Juridica))
                                {
                                    if (VerificarEmpresaNacional(fluxo.Contrato.EmpresaDestinatariaFiscal))
                                    {
                                        if (string.IsNullOrEmpty(fluxo.Contrato.EmpresaDestinatariaFiscal.Cgc) ||
                                            fluxo.Contrato.EmpresaDestinatariaFiscal.Cgc.Trim().Equals(string.Empty))
                                        {
                                            mensagemErro.AppendLine("<br/> - Fluxo não possui o CNPJ do Destinatário Fiscal.");
                                        }
                                    }
                                }
                            }
                        }

                        if (VerificarCfopContrato())
                        {
                            if (!VerificarFluxoInternacional(fluxo))
                            {
                                if (string.IsNullOrEmpty(fluxo.Contrato.CfopProduto)
                                    || fluxo.Contrato.CfopProduto.Trim().Equals(string.Empty))
                                {
                                    mensagemErro.AppendLine("<br/> - Contrato do Fluxo não possui o CFOP cadastrado.");
                                }
                            }
                        }
                    }
                }

                if (fluxo.Contrato != null)
                {
                    if (!fluxo.Contrato.ValorAliquotaIcmsProduto.HasValue || !fluxo.Contrato.ValorBaseCalculo.HasValue
                        || string.IsNullOrEmpty(fluxo.Contrato.Cfop) || fluxo.Contrato.Cfop.Trim().Equals(string.Empty))
                    {
                        mensagemErro.AppendLine("<br/> - Fluxo não está liberado no SAP.");
                    }
                }

                AssociaFluxoVigente associaFluxo = _associaFluxoVigenteRepository.ObterPorFluxoAtivo(fluxo);

                if (associaFluxo == null)
                {
                    mensagemErro.AppendLine("<br/> - Fluxo não possui um Grupo de fluxo ativo.");
                }
            }

            return mensagemErro.ToString();
        }

        /// <summary>
        /// Verifica se o fluxo comercial está com as pre-condições para gerar o CT-e 
        /// </summary>
        /// <param name="fluxo">Fluxo comercial</param>
        public void VerificarDadosFluxoCte(FluxoComercial fluxo)
        {
            EstacaoMae estacaoMaeOrigem;
            EstacaoMae estacaoMaeDestino;

            // Verifica se é origem Intercambio
            if (fluxo.OrigemIntercambio != null)
            {
                estacaoMaeOrigem = fluxo.OrigemIntercambio;
            }
            else
            {
                estacaoMaeOrigem = fluxo.Origem;
            }

            // Verifica se é destino Intercambio
            if (fluxo.DestinoIntercambio != null)
            {
                estacaoMaeDestino = fluxo.DestinoIntercambio;
            }
            else
            {
                estacaoMaeDestino = fluxo.Destino;
            }

            // Verifica se existe a rota cadastrada
            Rota rota = _rotaRepository.ObterPorOrigemDestino(estacaoMaeOrigem, estacaoMaeDestino);

            if (rota == null)
            {
                throw new TranslogicException("Rota não cadastrada.");
            }
        }

        /// <summary>
        /// Verifica os dados de fluxo padrão
        /// </summary>
        /// <param name="codigoFluxoComercial"> The codigo fluxo comercial. </param>
        /// <param name="codigoOrigem"> The codigo origem. </param>
        /// <param name="codigoDestino"> The codigo destino. </param>
        /// <param name="codigoMercadoria"> The codigo mercadoria. </param>
        /// <returns> Objeto Fluxo comercial </returns>
        public FluxoComercial VerificarDadosFluxoPadrao(string codigoFluxoComercial, string codigoOrigem, string codigoDestino, string codigoMercadoria)
        {
            FluxoComercial fluxoComercial = _fluxoComercialRepository.ObterPorCodigo(codigoFluxoComercial);
            codigoOrigem = codigoOrigem.ToUpperInvariant().Trim();
            EstacaoMae origem = _estacaoMaeRepository.ObterPorCodigo(codigoOrigem) as EstacaoMae;
            if (origem == null)
            {
                throw new TranslogicValidationException("Estação de origem inválido");
            }

            codigoDestino = codigoDestino.ToUpperInvariant().Trim();
            EstacaoMae destino = _estacaoMaeRepository.ObterPorCodigo(codigoDestino) as EstacaoMae;
            if (destino == null)
            {
                throw new TranslogicValidationException("Estação de destino inválido");
            }

            codigoMercadoria = codigoMercadoria.ToUpperInvariant().Trim();
            Mercadoria mercadoria = _mercadoriaRepository.ObterPorCodigo(codigoMercadoria);
            if (mercadoria == null)
            {
                throw new TranslogicValidationException("Código de mercadoria inválido.");
            }

            return fluxoComercial;
        }

        /// <summary>
        /// Verifica se o fluxo é um fluxo internacional
        /// </summary>
        /// <param name="fluxo"> Objeto FluxoComercial. </param>
        /// <returns> Valor booleano </returns>
        public bool VerificarFluxoInternacional(FluxoComercial fluxo)
        {
            AssociaFluxoInternacional fluxoInternacional = _associaFluxoInternacionalRepository.ObterPorFluxoComercial(fluxo);
            return fluxoInternacional != null;
        }

        /// <summary>
        /// Verifica se o fluxo é um fluxo intercambio
        /// </summary>
        /// <param name="fluxo"> Objeto fluxo comercial. </param>
        /// <returns> Valor booleano </returns>
        public bool VerificarFluxoIntercambio(FluxoComercial fluxo)
        {
            return fluxo.OrigemIntercambio != null || fluxo.DestinoIntercambio != null;
        }

        /// <summary>
        /// Verifica se o fluxo é um fluxo de origem de intercambio
        /// </summary>
        /// <param name="fluxo"> Objeto fluxo comercial. </param>
        /// <returns> Valor booleano </returns>
        public bool VerificarFluxoOrigemIntercambio(FluxoComercial fluxo)
        {
            return fluxo.OrigemIntercambio != null;
        }

        /// <summary>
        /// Verifica se o fluxo é um fluxo de rateio de cte
        /// </summary>
        /// <param name="fluxo"> Objeto fluxo comercial. </param>
        /// <returns> Valor booleano </returns>
        public bool VerificarFluxoRateioCte(FluxoComercial fluxo)
        {
            return fluxo.IndRateioCte == true;
        }

        /// <summary>
        /// Verifica se o fluxo é de preenchimento obrigatório de NFe
        /// </summary>
        /// <param name="fluxo">Fluxo comercial</param>
        /// <returns>Valor Booleano</returns>
        public bool VerificarNfeObrigatorio(FluxoComercial fluxo)
        {
            if (string.IsNullOrEmpty(fluxo.ModeloNotaFiscal))
            {
                return false;
            }

            return fluxo.ModeloNotaFiscal.Equals("55");
        }

        /// <summary>
        /// Verifica se deve ser preenchido o volume do vagão
        /// </summary>
        /// <param name="fluxo"> Fluxo comercial. </param>
        /// <returns> Valor booleano </returns>
        public bool VerificarPreenchimentoVolume(FluxoComercial fluxo)
        {
            return fluxo.Contrato.CodigoUnidadeMedida.Equals("M3");
        }

        /// <summary>
        /// Verifica se deve ser preenchido o volume do vagão
        /// </summary>
        /// <remarks>Este metodo só será chamado se o fluxo for 99999</remarks>
        /// <param name="mercadoria"> Objeto da mercadoria. </param>
        /// <returns> Valor booleano </returns>
        public bool VerificarPreenchimentoVolume(Mercadoria mercadoria)
        {
            return mercadoria.UnidadeMedida.Id.Equals("M3");
        }

        /// <summary>
        /// Verifica se o fluxo é necessário o preenchimento da tara e das placas
        /// </summary>
        /// <param name="fluxo">Fluxo comercial</param>
        /// <returns> Valor booleano </returns>
        public bool VerificarPreenchimentoTara(FluxoComercial fluxo)
        {
            if (fluxo.DestinoIntercambio != null)
            {
                bool ret = _localTaraPlacaRepository.VerificarLocalExiste(fluxo.DestinoIntercambio);
                if (ret)
                {
                    return true;
                }
            }

            return _localTaraPlacaRepository.VerificarLocalExiste(fluxo.Destino);
        }

        /// <summary>
        /// Verifica se o conteiner é válido
        /// </summary>
        /// <param name="codigoConteiner">Código do conteiner</param>
        /// <returns>Objeto conteiner</returns>
        public Conteiner ObterConteinerPorCodigo(string codigoConteiner)
        {
            Conteiner conteiner = _conteinerRepository.ObterPorCodigo(codigoConteiner);
            if (conteiner == null)
            {
                string mensagemErro = string.Format("Não foi encontrado o conteiner {0}.", codigoConteiner);
                throw new TranslogicException(mensagemErro);
            }

            return conteiner;
        }

        /// <summary>
        /// Verifica se o conteiner é válido
        /// </summary>
        /// <param name="codigoConteiner">Código do conteiner</param>
        /// <param name="codAoOrigem">Código da AO origem do Fluxo Comercial</param> 		
        /// <returns>Objeto conteiner</returns>
        public Conteiner VerificarConteinerPorCodigo(string codigoConteiner, int codAoOrigem)
        {
            Conteiner conteiner = _conteinerRepository.ObterPorCodigo(codigoConteiner);
            if (conteiner == null)
            {
                string mensagemErro = string.Format("Não foi encontrado o conteiner {0}.", codigoConteiner);
                throw new TranslogicException(mensagemErro);
            }

            ////VagaoConteinerVigente vagaoConteiner = _vagaoConteinerVigenteRepository.ObterPorConteiner(conteiner);
            ////if (vagaoConteiner != null)
            ////{
            ////    string mensagemErro = string.Format("O conteiner {0} já está no vagão {1}", codigoConteiner, vagaoConteiner.Vagao.Codigo);
            ////    throw new TranslogicException(mensagemErro);
            ////}

            return conteiner;
        }

        /// <summary>
        /// Verifica se o conteiner está em um local diferente
        /// </summary>
        /// <param name="conteiner"> Conteiner a ser verificado</param>
        /// <param name="idAoOrigem">Cod da AO de origem</param>
        /// <returns>Retorna o Conteiner Local Vigente</returns>
        public ConteinerLocalVigente VerificarLocalConteiner(Conteiner conteiner, int idAoOrigem)
        {
            ConteinerLocalVigente localConteiner = _conteinerLocalVigRepository.ObterPorCodConteiner(conteiner.Id);
            if (localConteiner != null)
            {
                if (localConteiner.EstacaoOrigem.Id != idAoOrigem)
                {
                    return localConteiner;
                }
            }

            return null;
        }

        /// <summary>
        /// Voa o vagão para a estação
        /// </summary>
        /// <param name="idConteiner">Número do conteiner</param>
        /// <param name="idAoOri"> Id da área operacional mãe do destino do contêiner</param>
        /// <param name="usuario">Usuário logado no sistema</param>
        public void VoarConteiner(string idConteiner, int idAoOri, string usuario)
        {
            _conteinerRepository.VoarConteiner(idConteiner, idAoOri, usuario);
        }

        /// <summary>
        /// Verifica se é um fluxo comercial de conteiner
        /// </summary>
        /// <param name="fluxo">Objeto fluxo comercial</param>
        /// <returns>Valor booleano</returns>
        public bool VerificarFluxoConteiner(FluxoComercial fluxo)
        {
            return VerificarFluxoConteiner(fluxo.Mercadoria);
        }

        /// <summary>
        /// Verifica se é um fluxo comercial de conteiner
        /// </summary>
        /// <param name="fluxo">Objeto fluxo comercial</param>
        /// <returns>Valor booleano</returns>
        public bool VerificarFluxoPallet(FluxoComercial fluxo)
        {
            return VerificarFluxoPallet(fluxo.Mercadoria);
        }

        /// <summary>
        /// Verficia se é um fluxo de PALLETS pela mercadoria
        /// </summary>
        /// <param name="mercadoria">Mercadoria informada no fluxo</param>
        /// <returns>Valor booleano</returns>
        public bool VerificarFluxoPallet(Mercadoria mercadoria)
        {
            Regex regex = new Regex(@"^PALLETS$");
            return regex.Match(mercadoria.DescricaoResumida).Success;
        }

        /// <summary>
        /// Verficia se é um fluxo de conteiner pela mercadoria
        /// </summary>
        /// <param name="mercadoria">Mercadoria informada no fluxo</param>
        /// <returns>Valor booleano</returns>
        public bool VerificarFluxoConteiner(Mercadoria mercadoria)
        {
            Regex regex = new Regex(@"^CONT.*");
            return regex.Match(mercadoria.DescricaoResumida).Success;
            // return mercadoria.UnidadeMedida.Id.Equals("CON");
        }

        /// <summary>
        /// Verifica se o fluxo comercial é necessário o preenchimento do tif
        /// </summary>
        /// <param name="fluxo">Fluxo comercial</param>
        /// <returns>Valor booleano</returns>
        public bool VerificarTifObrigatorio(FluxoComercial fluxo)
        {
            if (fluxo.DestinoIntercambio != null)
            {
                return fluxo.DestinoIntercambio.Codigo.Equals("NUG");
            }

            return false;
        }

        /// <summary>
        /// Verifica se o dada a origem é para calcular o peso do vagão pelo volume
        /// </summary>
        /// <param name="codigoOrigem">Código da origem do fluxo</param>
        /// <returns>Valor booleano</returns>
        public bool VerificarPesoPorVolume(string codigoOrigem)
        {
            // Alterado para calcular o peso sempre, não apenas para REPLAN
            // return codigoOrigem.Equals("ZZZ");
            return true;
        }

        /// <summary>
        /// Verifica se a empresa é do nacional ou do exterior
        /// </summary>
        /// <param name="empresa">Empresa a ser verificada</param>
        /// <returns>Valor booleano</returns>
        public bool VerificarEmpresaNacional(EmpresaCliente empresa)
        {
            EmpresaClientePais empresaClientePais = _empresaClientePaisRepository.ObterPorEmpresa(empresa);
            if (empresaClientePais == null)
            {
                throw new TranslogicException("A os dados do País da empresa não cadastrados.");
            }

            return empresaClientePais.Pais.ToUpperInvariant().Equals("BRASIL");
        }

        /// <summary>
        /// Verifica se a area operacional é da portofer
        /// </summary>
        /// <param name="estacao">Estação Mãe</param>
        /// <returns>Valor booleano</returns>
        public bool VerificaEmpresaPortoFer(EstacaoMae estacao)
        {
            return estacao.Codigo.StartsWith("P") && estacao.EmpresaConcessionaria.Id.Equals(68588);
        }

        /// <summary>
        /// Busca os dados do Fluxo Comercial
        /// </summary>
        /// <param name="codigoFluxoComercial"> The codigo fluxo comercial. </param>
        /// <returns> Objeto Fluxo comercial </returns>
        public FluxoComercial BuscarDadosFluxoComercial(string codigoFluxoComercial)
        {
            return _fluxoComercialRepository.ObterFerroviarioPorCodigo(codigoFluxoComercial.ToString());
        }

        /// <summary>
        /// Obtém as variações do CFop
        /// </summary>
        /// <param name="cfopProduto">Cfop do produto</param>
        /// <returns>String das variações</returns>
        public string ObterVariacoesCfop(string cfopProduto)
        {
            string variacoes = string.Empty;
            CfopAgrupamento agrupamento = _cfopAgrupamentoRepository.ObterPorCfopExemplo(cfopProduto);
            if (agrupamento != null)
            {
                variacoes = string.Concat(agrupamento.PrefixoCfop, ";", agrupamento.InicioIntervalo, ";", agrupamento.FimInteravalo);
            }

            return variacoes;
        }

        /// <summary>
        /// Loga os eventos do carregamento
        /// </summary>
        /// <param name="logDespacho">Log do despacho</param>
        /// <param name="usuarioAtual">Usuário que está logado</param>
        [Transaction]
        public virtual void LogarEventosCarregamento(LogTempoCarregamento logDespacho, Usuario usuarioAtual)
        {
            logDespacho.DataInicioPersistenciaDespacho = DateTime.Now;
            logDespacho.DataCadastro = DateTime.Now;
            logDespacho.CodigoUsuario = usuarioAtual.Codigo;
            logDespacho.MensagemErroDespacho = string.Empty;
            _logTempoCarregamentoRepository.Inserir(logDespacho);

            if (logDespacho.ListaVagoes != null)
            {
                foreach (LogTempoCarregamentoVagao vagao in logDespacho.ListaVagoes)
                {
                    vagao.DataCadastro = DateTime.Now;
                    vagao.LogTempoCarregamento = logDespacho;
                    _logTempoCarregamentoVagaoRepository.Inserir(vagao);

                    if (vagao.ListaNotas != null)
                    {
                        foreach (LogTempoCarregamentoVagaoNotaFiscal notaFiscal in vagao.ListaNotas)
                        {
                            notaFiscal.LogTempoCarregamentoVagao = vagao;
                            notaFiscal.DataCadastro = DateTime.Now;
                            _logTempoCarregamentoVagaoNotaFiscalRepository.Inserir(notaFiscal);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Grava o fechamento de eventos do carregamento
        /// </summary>
        /// <param name="logDespacho">Objeto logDespacho</param>
        /// <param name="indErro">Indica se deu erro</param>
        /// <param name="mensagemErro">Mensagem de Erro</param>
        [Transaction]
        public virtual void LogarFechamentoEventosCarregamento(LogTempoCarregamento logDespacho, bool indErro, string mensagemErro)
        {
            logDespacho.DataTerminoPersistenciaDespacho = DateTime.Now;
            logDespacho.IndDespachadoSucesso = !indErro;
            logDespacho.MensagemErroDespacho = mensagemErro;
            _logTempoCarregamentoRepository.Atualizar(logDespacho);
        }

        /// <summary>
        /// Salva o log de tempo de consulta da nfe no carregamento
        /// </summary>
        /// <param name="log">Log carregamento nfe</param>
        /// <param name="usuarioAtual">Usuario logado</param>
        public void SalvarLogCarregamentoNfe(LogCarregamentoNfe log, Usuario usuarioAtual)
        {
            log.CodigoUsuario = usuarioAtual.Codigo;
            log.DataCadastro = DateTime.Now;
            _logCarregamentoNfeRepository.Inserir(log);
        }

        /// <summary>
        /// Verifica se a descrição de mercadoria é de pallets
        /// </summary>
        /// <param name="descricaoMercadoria">Descrição da mercadoria</param>
        /// <returns>Valor booleano</returns>
        public bool VerificarMercadoriaPallets(string descricaoMercadoria)
        {
            return descricaoMercadoria.ToUpperInvariant().Contains("PALL");
        }

        /// <summary>
        /// Verifica se o contrato do fluxo possui vigência dado um fluxo e um CT-e
        /// </summary>
        /// <param name="fluxo">Objeto fluxo comercial</param>
        /// <param name="idCte">Id do cte a ser obtido a data do despacho</param>
        public void VerificarContratoFluxoPossuiVigenciaPorCte(FluxoComercial fluxo, int idCte)
        {
            Cte cte = _cteRepository.ObterPorId(idCte);
            DateTime dataDespacho = cte.Despacho.DataDespacho;
            Contrato contrato = _contratoRepository.ObterPorFluxoData(fluxo.Codigo, dataDespacho);
            if (contrato == null)
            {
                throw new TranslogicException("O fluxo comercial({0}) não possui um contrato vigente na data do carregamento({1}).", fluxo.Codigo, dataDespacho.ToString("dd/MM/yyyy"));
            }
        }

        /// <summary>
        /// Verifica se possui margem de liberacao para faturamento
        /// </summary>
        /// <param name="empresaCliente">Objeto Empresa Cliente</param>
        /// <returns>Retorna o valor da margem de liberacao ou caso não houver retorna zero</returns>
        public double VerificarMargemLiberacaoFaturamento(EmpresaCliente empresaCliente)
        {
            var empresa = _empresaMargemLiberacaoFatRepository.ObterPorEmpresa(empresaCliente);

            if (empresa != null)
            {
                return empresa.MargemPercentual;
            }

            ConfiguracaoTranslogic configuracao = _configuracaoTranslogicRepository.ObterPorId(NfeTravaSaldoPorcentagem);

            if (configuracao != null)
            {
                double margem;
                double.TryParse(configuracao.Valor, out margem);
                return (margem / 100) + 1;
            }

            return 1;
        }

        private DclTemp GerarDclTemp(CarregamentoDto carregamentoDto, Usuario usuario)
        {
            DclTemp dclTemp = new DclTemp();
            dclTemp.Id = GetNewId();
            dclTemp.CodigoUsuario = usuario.Codigo;
            dclTemp.CodigoTransacao = carregamentoDto.CodigoTransacao;
            dclTemp.IdFluxoComercial = carregamentoDto.IdFluxoComercial;
            dclTemp.IdFluxoInternacional = carregamentoDto.IdFluxoArgentina;
            dclTemp.DataDespacho = carregamentoDto.DataDespacho.Value;
            dclTemp.DataCarregamento = carregamentoDto.DataCarregamento.Value;
            dclTemp.DclPorVagao = carregamentoDto.DclPorVagao;
            dclTemp.NumeroSerieIntercambio = carregamentoDto.SerieIntercambio;
            dclTemp.NumeroDespachoIntercambio = carregamentoDto.NumeroIntercambio;
            dclTemp.IdFerroviaIntercambio = carregamentoDto.IdFerroviaIntercambio;
            dclTemp.RegimeIntercambio = carregamentoDto.RegimeIntercambio;
            dclTemp.IdLinhaIntercambio = carregamentoDto.IdLinhaIntercambio;
            dclTemp.IdEmpresaDespacho = carregamentoDto.IdEmpresa.Value;
            dclTemp.IdAreaOperacionalSede = carregamentoDto.IdAreaOperacionalSede;
            dclTemp.IdAreaOperacionalDestino = carregamentoDto.IdAreaOperacionalDestino;
            dclTemp.IdMercadoria = carregamentoDto.IdMercadoria;
            dclTemp.IndOrigemRpl = carregamentoDto.IndRpl;
            dclTemp.DataIntercambio = carregamentoDto.DataIntercambio;
            dclTemp.PrefixoDespacho = null;
            dclTemp.SerieManutencao = null;
            dclTemp.NumeroDclManutencao = null;
            dclTemp.IndRebatimento = false;
            dclTemp.IndPrecarregamento = false;
            dclTemp.Observacao = string.Empty;
            dclTemp.NumeroCtfc = null;
            dclTemp.SerieCtfc = null;

            // dclTemp.IdGrupoFluxo = carregamentoDto.IdGrupoFluxo;

            return dclTemp;
        }

        private void PopularDadosDtoPeloFluxo(FluxoComercial fluxo, ref CarregamentoDto carregamentoDto)
        {
            // carregamentoDto.DataCarregamento = DateTime.Now.AddSeconds(-10);
            // carregamentoDto.DataDespacho = DateTime.Now.AddSeconds(-10);
            // carregamentoDto.IdAreaOperacionalSede = fluxo.OrigemIntercambio != null ? fluxo.OrigemIntercambio.Id.Value : fluxo.Origem.Id.Value;
            // carregamentoDto.IdAreaOperacionalDestino = fluxo.DestinoIntercambio != null ? fluxo.DestinoIntercambio.Id.Value : fluxo.Destino.Id.Value;
            // carregamentoDto.IdMercadoria = fluxo.Mercadoria.Id;
            // carregamentoDto.DclPorVagao = true;

            if (fluxo.Codigo.Equals("99999"))
            {
                EstacaoMae estacao = _estacaoMaeRepository.ObterPorId(carregamentoDto.IdAreaOperacionalSede);
                carregamentoDto.IdEmpresa = estacao.EmpresaOperadora.Id;
                carregamentoDto.IndRpl = estacao.Codigo.Equals("ZZZ");
            }
            else
            {
                carregamentoDto.IndRpl = fluxo.Origem.Codigo.Equals("ZZZ");
                carregamentoDto.IdEmpresa = fluxo.Contrato.EmpresaRemetente.Id;
            }
        }

        private IList<VagaoCarregamentoDto> ConverterVagaoCarregamentoDto(IList<LocalizacaoVagaoVigente> listVagaoLocal)
        {
            return listVagaoLocal.Select(vagaoLocalizacao => new VagaoCarregamentoDto
                                                                 {
                                                                     IdVagao = vagaoLocalizacao.Vagao.Id.Value,
                                                                     CodigoVagao = vagaoLocalizacao.Vagao.Codigo,
                                                                     QuantidadeCarregadoMesmoFluxo = 0,
                                                                     Linha = string.Empty,
                                                                     Sequencia = null,
                                                                     SerieVagao = vagaoLocalizacao.Vagao.Serie.Codigo
                                                                 }).ToList();
        }

        private IList<DclTempVagaoNotaFiscal> GerarDclTempVagaoNotaFiscal(IList<NotaFiscalVagaoCarregamentoDto> listaNotasFiscais)
        {
            List<DclTempVagaoNotaFiscal> list = new List<DclTempVagaoNotaFiscal>();
            int contador = 1;
            foreach (NotaFiscalVagaoCarregamentoDto dto in listaNotasFiscais)
            {
                double valorRateio;
                double? pesoRateio;
                if (dto.VolumeNotaFiscal.HasValue && dto.VolumeVagao.HasValue)
                {
                    pesoRateio = Math.Round(dto.PesoDclVagao * dto.VolumeNotaFiscal.Value / dto.VolumeVagao.Value, 3);
                    valorRateio = dto.ValorTotalNotaFiscal;
                }
                else
                {
                    pesoRateio = dto.PesoRateio;
                    valorRateio = !string.IsNullOrEmpty(dto.ChaveNfe) ? Math.Round(dto.ValorTotalNotaFiscal * dto.PesoRateio.Value / dto.PesoTotal.Value, 2) : dto.ValorNotaFiscal;
                }

                DclTempVagaoNotaFiscal nota = new DclTempVagaoNotaFiscal
                {
                    Id = contador,
                    Identificacao = string.Concat(dto.NumeroNotaFiscal, dto.SerieNotaFiscal),
                    ChaveNfe = dto.ChaveNfe,
                    CodigoConteiner = dto.Conteiner,
                    CodigoVagao = dto.CodigoVagao,
                    DataNotaFiscal = dto.DataNotaFiscal.Value,
                    NumeroNotaFiscal = dto.NumeroNotaFiscal,
                    SerieNotaFiscal = dto.SerieNotaFiscal,
                    Valor = valorRateio,
                    ValorTotal = dto.ValorTotalNotaFiscal,
                    Peso = pesoRateio,
                    PesoTotal = dto.PesoTotal,
                    NumeroTif = dto.TIF,
                    CodigoEmpresaDestinataria = dto.SiglaDestinatario,
                    CodigoEmpresaRemetente = dto.SiglaRemetente,
                    UfEmpresaDestinataria = dto.UfDestinatario,
                    UfEmpresaRemetente = dto.UfRemetente,
                    CnpjEmpresaDestinataria = dto.CnpjDestinatario,
                    CnpjEmpresaRemetente = dto.CnpjRemetente,
                    InscricaoEstadualEmpresaDestinataria = dto.InscricaoEstadualDestinatario,
                    InscricaoEstadualEmpresaRemetente = dto.InscricaoEstadualRemetente,
                    Volume = dto.VolumeNotaFiscal,
                    ChaveCte = dto.ChaveCte
                };

                list.Add(nota);
                contador++;
            }

            return list;
        }

        private int TratarTipoNotaFiscal(object tipoNotaFiscal)
        {
            if (tipoNotaFiscal == null)
            {
                return -1;
            }

            try
            {
                int result;
                var valorConvetido = int.TryParse(tipoNotaFiscal.ToString(), out result);

                return result;
            }
            catch
            {
            }

            return -1;
        }

        private void PopularRemetente(DclTempVagaoNotaFiscal nota, string sigla, string uf, string cnpj, string ie)
        {
            nota.CodigoEmpresaRemetente = sigla;
            nota.UfEmpresaRemetente = uf;
            nota.CnpjEmpresaRemetente = cnpj;
            nota.InscricaoEstadualEmpresaRemetente = ie;
        }

        private void PopularDestinatario(DclTempVagaoNotaFiscal nota, string sigla, string uf, string cnpj, string ie)
        {
            nota.CodigoEmpresaDestinataria = sigla;
            nota.UfEmpresaDestinataria = uf;
            nota.CnpjEmpresaDestinataria = cnpj;
            nota.InscricaoEstadualEmpresaDestinataria = ie;
        }

        private IList<DclTempVagaoConteiner> GerarDclTempVagaoConteiner(IList<NotaFiscalVagaoCarregamentoDto> listaNotasFiscais)
        {
            List<DclTempVagaoConteiner> listAlterada = listaNotasFiscais.GroupBy(g => new { g.Conteiner, g.CodigoVagao }).Select(
                s => new DclTempVagaoConteiner
                {
                    CodigoVagao = s.Key.CodigoVagao,
                    Id = s.Key.Conteiner
                }).ToList();

            List<DclTempVagaoConteiner> list = listaNotasFiscais.GroupBy(g => new { g.Conteiner, g.CodigoVagao }).Select(
                s => new DclTempVagaoConteiner
                {
                    CodigoVagao = s.Key.CodigoVagao,
                    Id = s.Key.Conteiner
                }).ToList();

            foreach (DclTempVagaoConteiner dclTempVagaoConteiner in list)
            {
                if (string.IsNullOrEmpty(dclTempVagaoConteiner.Id))
                {
                    listAlterada.Remove(dclTempVagaoConteiner);
                }
            }

            return listAlterada;
        }

        private IList<DclTempVagao> GerarDclTempVagao(IList<NotaFiscalVagaoCarregamentoDto> listaNotasFiscais, FluxoComercial fluxoComercial)
        {
            List<DclTempVagao> listaVagoes = listaNotasFiscais.GroupBy(g => new { g.IdVagao, g.CodigoVagao, g.MultiploDespacho, g.PesoDclVagao, g.PesoTotalVagao }).Select(
                s => new DclTempVagao
                    {
                        Id = s.Key.IdVagao,
                        CodigoVagao = s.Key.CodigoVagao,
                        IndMultiploDespacho = s.Key.MultiploDespacho,
                        PesoTotal = s.Key.PesoTotalVagao,
                        PesoDcl = s.Key.PesoDclVagao,
                        QuantidadeConteiners = s.Where(d => !string.IsNullOrEmpty(d.Conteiner)).Select(d => d.Conteiner).Distinct().Count(), // s.Count(v => !string.IsNullOrEmpty(v.Conteiner)),
                        Tara = s.Select(d => d.TaraVagao ?? 0).FirstOrDefault(),
                        Volume = s.Select(d => d.VolumeVagao ?? 0).FirstOrDefault()
                    })
                    .ToList();

            if (fluxoComercial.Codigo != "99999")
            {
                // if (!fluxoComercial.Mercadoria.UnidadeMedida.Id.Equals("CON"))
                Regex regex = new Regex(@"^CONT.*");

                if (!regex.Match(fluxoComercial.Mercadoria.DescricaoResumida).Success)
                {
                    for (int i = 0; i < listaVagoes.Count; i++)
                    {
                        listaVagoes[i].QuantidadeConteiners = null;
                    }
                }
            }

            return listaVagoes;
        }
    }

    /// <summary>
    /// Dto de carregamento de vagão
    /// </summary>
    public class CarregamentoVagaoDto
    {
        /// <summary>
        /// Id do vagão
        /// </summary>
        public int? IdVagao { get; set; }

        /// <summary>
        /// Código do vagão
        /// </summary>
        public string CodigoVagao { get; set; }

        /// <summary>
        /// Indica se deu erro ou não no vagão
        /// </summary>
        public bool IndErro { get; set; }

        /// <summary>
        /// Excessao do vagão
        /// </summary>
        public Exception Excecao { get; set; }

        /// <summary>
        /// Mensagem de erro
        /// </summary>
        public string MensagemErro { get; set; }
    }
}
