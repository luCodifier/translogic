namespace Translogic.Modules.Core.Domain.Services.FluxosComerciais
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Xml;
    using Interfaces.Simconsultas;
    using Interfaces.Simconsultas.Interfaces;
    using Model.Codificador;
    using Model.Codificador.Repositories;
    using Model.FluxosComerciais.Pedidos.Despachos;
    using Model.FluxosComerciais.Pedidos.Despachos.Dtos;
    using Model.FluxosComerciais.Pedidos.Despachos.Repositories;
    using Model.FluxosComerciais.Simconsultas;
    using Translogic.Core.Infrastructure;

    /// <summary>
    /// Servi�o de nota fiscal
    /// </summary>
    public class NotaFiscalService
    {
        private readonly INotaFiscalTranslogicRepository _notaFiscalTranslogicRepository;
        private readonly IConfiguracaoTranslogicRepository _configuracaoTranslogicRepository;
        private readonly ISimconsultasService _simconsultasService;

        /// <summary>
        /// Initializes a new instance of the <see cref="NotaFiscalService"/> class.
        /// </summary>
        /// <param name="notaFiscalTranslogicRepository"> The nota fiscal translogic repository. </param>
        /// <param name="configuracaoTranslogicRepository">Reposit�rio de configura��o translogic injetado</param>
        /// <param name="simconsultasService">Servi�o do SimConsultas injetado</param>
        public NotaFiscalService(INotaFiscalTranslogicRepository notaFiscalTranslogicRepository, IConfiguracaoTranslogicRepository configuracaoTranslogicRepository, ISimconsultasService simconsultasService)
        {
            _notaFiscalTranslogicRepository = notaFiscalTranslogicRepository;
            _simconsultasService = simconsultasService;
            _configuracaoTranslogicRepository = configuracaoTranslogicRepository;
        }

        /// <summary>
        /// Atualiza os dados das notas fiscais
        /// </summary>
        public void AtualizarDadosNotasFiscais()
        {
            string chaveSimConsultas = string.Empty;
            ConfiguracaoTranslogic configuracao = null;
            try
            {
                configuracao = _configuracaoTranslogicRepository.ObterPorId("CHAVE_ACESSO_SIMCONSULTAS");
                chaveSimConsultas = configuracao.Valor;
            }
            catch (Exception)
            {
                chaveSimConsultas = "6E07B8D5-0644-46F7-9BAE-3DDA70A8AC06";
            }
            
            IList<NotaFiscalLdpDto> notasFiscais = _notaFiscalTranslogicRepository.ObterNotasCorrecaoDomPedro();
            for (int i = 0; i < notasFiscais.Count; i++)
            {
                NotaFiscalLdpDto notaFiscalLdp = notasFiscais[i];
                notaFiscalLdp.Processado = false;

                try
                {
                    NfeSimconsultas nfe = ObterDadosPorWebService(chaveSimConsultas, notaFiscalLdp);
                    if (nfe != null)
                    {
                        if (ValidarPlaca(nfe.Placa))
                        {
                            notaFiscalLdp.PesoTotal = (nfe.Peso > 0) ? nfe.Peso / 1000 : 0;
                            notaFiscalLdp.PlacaCavalo = nfe.Placa;
                            notaFiscalLdp.MensagemErro = string.Empty;
                            notaFiscalLdp.Processado = true;
                        }
                        else
                        {
                            notaFiscalLdp.MensagemErro = "Placa do caminh�o inv�lida.";
                            notaFiscalLdp.Processado = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    notaFiscalLdp.MensagemErro = ex.Message;
                    notaFiscalLdp.Processado = true;
                }

                if (notaFiscalLdp.Processado.Value)
                {
                    _notaFiscalTranslogicRepository.AtualizarPlacaPeso(notaFiscalLdp);
                }
            }
        }

        /// <summary>
        /// Valida se a placa do caminh�o � inv�lida
        /// </summary>
        /// <param name="placa">Placa do caminh�o</param>
        /// <returns>Valor booleano</returns>
        public bool ValidarPlaca(string placa)
        {
            Regex regex = new Regex(@"^[A-Z][A-Z][A-Z]\-?\d{4}$", RegexOptions.None);
            return regex.IsMatch(placa);
        }

        private NfeSimconsultas ObterDadosPorWebService(string chaveSimConsultas, NotaFiscalLdpDto notaFiscalLdp)
        {
            NfeSimconsultas nfe;

            nfe = ObterDadosSimConsultas(chaveSimConsultas, notaFiscalLdp.ChaveNfe);
            if (nfe == null)
            {
                nfe = ObterDadosSimConsultas(chaveSimConsultas, notaFiscalLdp.ChaveNfe);
            }

            return nfe;
        }

        private NfeSimconsultas ObterDadosSimConsultas(string chaveSimconsultas, string chaveNfe)
        {
            RetornoSimconsultas dadosNota = _simconsultasService.ObterDadosNfe(chaveSimconsultas, chaveNfe);
            if (dadosNota.Erro)
            {
                return null;
            }

            return ProcessarRetornoSimconsultas(dadosNota);
        }

        private NfeSimconsultas ProcessarRetornoSimconsultas(RetornoSimconsultas dadosNfe)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(dadosNfe.Nfe.OuterXml);

            if (doc.DocumentElement != null)
            {
                if (doc.DocumentElement.Name.Equals("NFe"))
                {
                    return ProcessarDadosNfe(doc);
                }

                if (doc.DocumentElement.Name.Equals("InfConsulta"))
                {
                    XmlElement root = doc.DocumentElement;
                    XmlNamespaceManager nsmgr = new XmlNamespaceManager(doc.NameTable);
                    nsmgr.AddNamespace("d", root.NamespaceURI);

                    XmlNode noCodigo = root.SelectSingleNode("//d:InfConsulta/d:StatusConsulta", nsmgr);
                    string codigoErro = noCodigo.InnerText;

                    XmlNode noDescricao = root.SelectSingleNode("//d:InfConsulta/d:StatusDescricao", nsmgr);
                    string mensagemErro = noDescricao.InnerText;

                    if (codigoErro.Equals("3") ||
                        codigoErro.Equals("7") ||
                        codigoErro.Equals("25") ||
                        codigoErro.Equals("31") ||
                        codigoErro.Equals("55"))
                    {
                        throw new TranslogicException(mensagemErro);
                    }

                    if (codigoErro.Equals("19"))
                    {
                        throw new TranslogicException("Nota Fiscal Eletr�nica emitida em conting�ncia e ainda n�o autorizada pela SEFAZ.");
                    }

                    if (codigoErro.Equals("100"))
                    {
                        throw new TranslogicException("Nota Fiscal Eletr�nica Cancelada pelo Emitente.");
                    }

                    if (codigoErro.Equals("4") || codigoErro.Equals("98"))
                    {
                        return null;
                    }
                }
            }

            return null;
        }

        private NfeSimconsultas ProcessarDadosNfe(XmlDocument doc)
        {
            XmlElement root = doc.DocumentElement;
            XmlNamespaceManager nsmgr = new XmlNamespaceManager(doc.NameTable);
            nsmgr.AddNamespace("d", root.NamespaceURI);

            XmlNode noInfNfe = root.SelectSingleNode("//d:NFe/d:infNFe", nsmgr);
            string chaveNfe = noInfNfe.Attributes["Id"].Value;

            XmlNode noIde = noInfNfe.SelectSingleNode("//d:ide", nsmgr);

            NfeSimconsultas nfe = new NfeSimconsultas();
            // DADOS DE IDENTIFICA��O DA NFE
            nfe.ChaveNfe = chaveNfe.Substring(3);
            nfe.CodigoUfIbge = Convert.ToInt32(ObterTextoNoXml(noIde, "cUF", nsmgr));
            nfe.CodigoChaveAcesso = Convert.ToInt32(ObterTextoNoXml(noIde, "cNF", nsmgr));
            nfe.NaturezaOperacao = ObterTextoNoXml(noIde, "natOp", nsmgr);
            // nfe.FormaPagamento = ObterFormaPagamento(ObterTextoNoXml(noIde, "indPag", nsmgr));
            nfe.ModeloNota = ObterTextoNoXml(noIde, "mod", nsmgr);
            nfe.SerieNotaFiscal = ObterTextoNoXml(noIde, "serie", nsmgr);
            nfe.NumeroNotaFiscal = Convert.ToInt32(ObterTextoNoXml(noIde, "nNF", nsmgr));
            nfe.DataEmissao = DateTime.ParseExact(ObterTextoNoXml(noIde, "dEmi", nsmgr), "yyyy-MM-dd", CultureInfo.InvariantCulture);
            string dataSaida = ObterTextoNoXml(noIde, "dSaiEnt", nsmgr);
            if (!string.IsNullOrEmpty(dataSaida))
            {
                nfe.DataSaida = DateTime.ParseExact(dataSaida + " " + ObterTextoNoXml(noIde, "hSaiEnt", nsmgr), "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);
            }

            /*nfe.TipoNotaFiscal = ObterTipoNotaFiscal(ObterTextoNoXml(noIde, "tpNF", nsmgr));
            nfe.CodigoIbge = Convert.ToInt32(ObterTextoNoXml(noIde, "cMunFG", nsmgr));
            nfe.FormatoImpressao = ObterFormatoImpressao(ObterTextoNoXml(noIde, "tpImp", nsmgr));
            nfe.TipoEmissao = ObterTipoEmissao(ObterTextoNoXml(noIde, "tpEmis", nsmgr));
            nfe.DigitoVerificadorChave = Convert.ToInt32(ObterTextoNoXml(noIde, "cDV", nsmgr));
            nfe.TipoAmbiente = ObterTipoAmbiente(ObterTextoNoXml(noIde, "tpAmb", nsmgr));
            nfe.FinalidadeEmissao = ObterFinalidadeEmissao(ObterTextoNoXml(noIde, "finNFe", nsmgr));
            nfe.ProcessoEmissao = ObterProcessoEmissao(ObterTextoNoXml(noIde, "procEmi", nsmgr));
            nfe.VersaoProcesso = ObterTextoNoXml(noIde, "verProc", nsmgr);

            /*
            // DADOS DE EMITENTE
            XmlNode noEmit = noInfNfe.SelectSingleNode("//d:emit", nsmgr);
            DadosEmpresaTemp dadosEmpresaEmitente = RecuperarDadosEmpresa(noEmit, true, nsmgr);
            ProcessarDadosEmitente(ref nfe, dadosEmpresaEmitente);

            // DADOS DE DESTINATARIO
            XmlNode noDest = noInfNfe.SelectSingleNode("//d:dest", nsmgr);
            DadosEmpresaTemp dadosEmpresaDestinataria = RecuperarDadosEmpresa(noDest, false, nsmgr);
            ProcessarDadosDestinatario(ref nfe, dadosEmpresaDestinataria);*/

            // DADOS DE IMPOSTOS DA NFE
            XmlNode noIcmsTotal = noInfNfe.SelectSingleNode("//d:total/d:ICMSTot", nsmgr);
            nfe.ValorBaseCalculoIcms = Convert.ToDouble(ObterTextoNoXml(noIcmsTotal, "vBC", nsmgr), CultureInfo.GetCultureInfo("en-US"));
            nfe.ValorIcms = Convert.ToDouble(ObterTextoNoXml(noIcmsTotal, "vICMS", nsmgr), CultureInfo.GetCultureInfo("en-US"));
            nfe.ValorBaseCalculoSubTributaria = Convert.ToDouble(ObterTextoNoXml(noIcmsTotal, "vBCST", nsmgr), CultureInfo.GetCultureInfo("en-US"));
            nfe.ValorSubTributaria = Convert.ToDouble(ObterTextoNoXml(noIcmsTotal, "vST", nsmgr), CultureInfo.GetCultureInfo("en-US"));
            nfe.ValorProduto = Convert.ToDouble(ObterTextoNoXml(noIcmsTotal, "vProd", nsmgr), CultureInfo.GetCultureInfo("en-US"));
            nfe.ValorTotalFrete = Convert.ToDouble(ObterTextoNoXml(noIcmsTotal, "vFrete", nsmgr), CultureInfo.GetCultureInfo("en-US"));
            nfe.ValorSeguro = Convert.ToDouble(ObterTextoNoXml(noIcmsTotal, "vSeg", nsmgr), CultureInfo.GetCultureInfo("en-US"));
            nfe.ValorDesconto = Convert.ToDouble(ObterTextoNoXml(noIcmsTotal, "vDesc", nsmgr), CultureInfo.GetCultureInfo("en-US"));
            nfe.ValorImpostoImportacao = Convert.ToDouble(ObterTextoNoXml(noIcmsTotal, "vII", nsmgr), CultureInfo.GetCultureInfo("en-US"));
            nfe.ValorIpi = Convert.ToDouble(ObterTextoNoXml(noIcmsTotal, "vIPI", nsmgr), CultureInfo.GetCultureInfo("en-US"));
            nfe.ValorPis = Convert.ToDouble(ObterTextoNoXml(noIcmsTotal, "vPIS", nsmgr), CultureInfo.GetCultureInfo("en-US"));
            nfe.ValorCofins = Convert.ToDouble(ObterTextoNoXml(noIcmsTotal, "vCOFINS", nsmgr), CultureInfo.GetCultureInfo("en-US"));
            nfe.ValorOutro = Convert.ToDouble(ObterTextoNoXml(noIcmsTotal, "vOutro", nsmgr), CultureInfo.GetCultureInfo("en-US"));
            nfe.Valor = Convert.ToDouble(ObterTextoNoXml(noIcmsTotal, "vNF", nsmgr), CultureInfo.GetCultureInfo("en-US"));

            // DADOS DE TRANSPORTE
            XmlNode noTransp = noInfNfe.SelectSingleNode("//d:transp", nsmgr);
            // nfe.ModeloFrete = ObterModeloFrete(ObterTextoNoXml(noTransp, "modFrete", nsmgr));

            XmlNode noTransportadora = noTransp.SelectSingleNode("//d:transporta", nsmgr);
            if (noTransportadora != null)
            {
                string cnpj = ObterTextoNoXml(noTransportadora, "CNPJ", nsmgr);
                if (!string.IsNullOrEmpty(cnpj))
                {
                    nfe.CnpjTransportadora = cnpj;
                }

                nfe.NomeTransportadora = ObterTextoNoXml(noTransportadora, "xNome", nsmgr);
                nfe.EnderecoTransportadora = ObterTextoNoXml(noTransportadora, "xEnder", nsmgr);
                nfe.MunicipioTransportadora = ObterTextoNoXml(noTransportadora, "xMun", nsmgr);
                nfe.UfTransportadora = ObterTextoNoXml(noTransportadora, "UF", nsmgr);
            }

            XmlNode noVeicTransp = noTransp.SelectSingleNode("//d:veicTransp", nsmgr);
            if (noVeicTransp != null)
            {
                nfe.Placa = ObterTextoNoXml(noVeicTransp, "placa", nsmgr);
            }

            nfe.CanceladoEmitente = false;
            nfe.DataHoraGravacao = DateTime.Now;
            // nfe.Xml = doc.OuterXml;
            
            ProcessarProdutosNfe(nfe, noInfNfe.SelectNodes("//d:det", nsmgr), nsmgr);
            return nfe;
        }

        private void ProcessarProdutosNfe(NfeSimconsultas nfe, XmlNodeList listaProdutos, XmlNamespaceManager nsmgr)
        {
            double pesoTotalNota = 0;
            foreach (XmlNode produto in listaProdutos)
            {
                NfeProdutoSimconsultas nfeProduto = ProcessarProdutoNfe(nfe, produto, nsmgr);
                pesoTotalNota += nfeProduto.QuantidadeComercial;
            }

            nfe.Peso = pesoTotalNota;
            nfe.PesoBruto = pesoTotalNota;
        }

        private NfeProdutoSimconsultas ProcessarProdutoNfe(NfeSimconsultas nfe, XmlNode noDetProduto, XmlNamespaceManager nsmgr)
        {
            NfeProdutoSimconsultas nfeProduto = new NfeProdutoSimconsultas();
            XmlNode noProduto = noDetProduto.SelectSingleNode("//d:prod", nsmgr);
            nfeProduto.NfeSimconsultas = nfe;
            nfeProduto.CodigoProduto = ObterTextoNoXml(noProduto, "cProd", nsmgr);
            nfeProduto.DescricaoProduto = ObterTextoNoXml(noProduto, "xProd", nsmgr);
            nfeProduto.CodigoNcm = ObterTextoNoXml(noProduto, "NCM", nsmgr);
            // nfeProduto.Genero = ObterTextoNoXml(noProduto, "", nsmgr); //verificar onde est� no xml da nfe
            nfeProduto.Cfop = ObterTextoNoXml(noProduto, "CFOP", nsmgr);
            nfeProduto.UnidadeComercial = ObterTextoNoXml(noProduto, "uCom", nsmgr);
            nfeProduto.QuantidadeComercial = Convert.ToDouble(ObterTextoNoXml(noProduto, "qCom", nsmgr), CultureInfo.GetCultureInfo("en-US"));
            nfeProduto.ValorUnitarioComercializacao = Convert.ToDouble(ObterTextoNoXml(noProduto, "vUnCom", nsmgr), CultureInfo.GetCultureInfo("en-US"));
            nfeProduto.ValorProduto = Convert.ToDouble(ObterTextoNoXml(noProduto, "vProd", nsmgr), CultureInfo.GetCultureInfo("en-US"));
            nfeProduto.UnidadeTributavel = ObterTextoNoXml(noProduto, "uTrib", nsmgr);
            nfeProduto.QuantidadeTributavel = Convert.ToDouble(ObterTextoNoXml(noProduto, "qTrib", nsmgr), CultureInfo.GetCultureInfo("en-US"));
            nfeProduto.ValorUnitarioTributacao = Convert.ToDouble(ObterTextoNoXml(noProduto, "vUnTrib", nsmgr), CultureInfo.GetCultureInfo("en-US"));
            nfeProduto.DataHoraGravacao = DateTime.Now;
            return nfeProduto;
        }

        private string ObterTextoNoXml(XmlNode xmlNode, string tagNo, XmlNamespaceManager nsmgr)
        {
            XmlNode no = xmlNode.SelectSingleNode("//d:" + tagNo, nsmgr);
            if (no == null)
            {
                return null;
            }

            return no.InnerText;
        }
    }
}