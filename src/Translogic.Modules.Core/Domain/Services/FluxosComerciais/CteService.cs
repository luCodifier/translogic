﻿namespace Translogic.Modules.Core.Domain.Services.Ctes
{
	using System;
	using System.Collections;
	using System.Collections.Generic;
	using System.Globalization;
	using System.IO;
	using System.IO.Compression;
	using System.Linq;
	using System.Net;
	using System.Net.Mail;
	using System.Net.Mime;
	using System.Text;
	using System.Text.RegularExpressions;
	using System.Threading;
	using ALL.Core.Dominio;
	using Castle.Services.Transaction;
	using Ctes;
	using ICSharpCode.SharpZipLib.Core;
	using ICSharpCode.SharpZipLib.Zip;
	using Model.Acesso;
	using Model.Acesso.Repositories;
	using Model.Codificador;
	using Model.Codificador.Repositories;
	using Model.Diversos.Cte;
	using Model.Dto;
	using Model.Estrutura;
	using Model.FluxosComerciais;
	using Model.FluxosComerciais.ConhecimentoTransporteEletronico;
	using Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;
	using Model.FluxosComerciais.InterfaceSap.Repositories;
	using Model.FluxosComerciais.Ndd.Repositories;
	using Model.FluxosComerciais.Pedidos.Despachos;
	using Model.FluxosComerciais.Pedidos.Despachos.Repositories;
	using Model.FluxosComerciais.Repositories;
	using Model.FluxosComerciais.TriangulusConfig;
	using Model.FluxosComerciais.TriangulusConfig.Repositories;
	using Model.Trem.Veiculo.Vagao;
	using Model.Trem.Veiculo.Vagao.Repositories;
	using Model.Via;
	using Translogic.Core.Infrastructure;
	using Translogic.Core.Infrastructure.FileSystem.ExtensionMethods;
	using Translogic.Core.Infrastructure.Web;
	using Util;

	/// <summary>
	/// Serviço de Cte
	/// </summary>
	[Transactional]
	public class CteService
	{
		private readonly ICteRepository _cteRepository;
		private readonly ICteArquivoFtpRepository _cteArquivoFtpRepository;
		private readonly IFluxoComercialRepository _fluxoComercialRepository;
		private readonly ICteComplementadoRepository _cteComplementadoRepository;
		private readonly IItemDespachoRepository _itemDespachoRepository;
		private readonly ICteEnvioPoolingRepository _cteEnvioPoolingRepository;
		private readonly ICteRecebimentoPoolingRepository _cteRecebimentoPoolingRepository;
		private readonly ICteSapPoolingRepository _cteSapPoolingRepository;
		private readonly ICteStatusRepository _cteStatusRepository;
		private readonly ITbDatabaseInputRepository _databaseInputRepository;
		private readonly CteLogService _cteLogService;
		private readonly ICteArvoreRepository _cteArvoreRepository;
		private readonly ICteStatusRetornoRepository _cteStatusRetornoRepository;
		private readonly IUsuarioRepository _usuarioRepository;
		private readonly ICteDetalheRepository _cteDetalheRepository;
		private readonly ICteAgrupamentoRepository _cteAgrupamentoRepository;
		private readonly IVagaoRepository _vagaoRepository;
		private readonly ICteTempDetalheRepository _cteTempDetalheRepository;
		private readonly ICteTempRepository _cteTempRepository;
		private readonly FilaProcessamentoService _filaProcessamentoService;
		private readonly IConfiguracaoTranslogicRepository _configuracaoTranslogicRepository;
		private readonly ICteInterfaceRecebimentoConfigRepository _cteInterfaceRecebimentoConfigRepository;
		private readonly ICteInterfaceEnvioSapRepository _cteInterfaceEnvioSapRepository;
		private readonly ICteArquivoRepository _cteArquivoRepository;
		private readonly ICteInterfacePdfConfigRepository _cteInterfacePdfRepository;
		private readonly ICteInterfaceXmlConfigRepository _cteInterfaceXmlRepository;
		private readonly IVSapZSDV0203VRepository _vsapZSDV0203VRepository;
		private readonly ICteInterfaceEnvioEmailRepository _cteInterfaceEnvioEmailRepository;
		private readonly ICteEnvioXmlRepository _cteEnvioXmlRepository;
		private readonly ICteEnvioEmailHistoricoRepository _cteEnvioEmailHistoricoRepository;
		private readonly INfe03FilialRepository _nfe03FilialRepository;
		private readonly ICteEnvioEmailErroRepository _cteEnvioEmailErroRepository;
		private readonly ICteMotivoCancelamentoRepository _cteMotivoCancelamentoRepository;
		private readonly IAgrupamentoUsuarioRepository _agrupamentoUsuarioRepository;
		private readonly ISerieDespachoUfRepository _serieDespachoUfRepository;
		private readonly ICteComunicadoRepository _cteComunicadoRepository;
		private readonly IControleAcessoService _controleAcessoService;

		private readonly string _chaveCteRobo = "CTE_USUARIO_ROBO";
		private readonly string _chaveCteCodigoInternoFilial = "CTE_CODIGO_INTERNO_FILIAL_DEFAULT";
		private readonly string _chavePdfPath = "CTE_PDF_PATH";
		private readonly string _chaveDataMaximaCancelamento = "CTE_DATA_MAXIMA_CANCELAMENTO";
		private readonly string _chaveCteTentativaReenvio = "CTE_TENTATIVA_REENVIO";
		private readonly string _chaveHostReenvioEmail = "CTE_HOST_REENVIO_EMAIL";
		private readonly string _cteEmailServer = "CTE_EMAIL_SERVER";
		private readonly string _cteEmailRemetente = "CTE_EMAIL_REMETENTE";
		private readonly string _chaveDiasCancelamentoCte = "CTE_DIAS_CANCELAMENTO_CTE";
		private readonly string _chaveCodigoGrupoFiscal = "CTE_CODIGO_GRUPO_FISCAL";
		private readonly string _chaveTravaCancelamento = "CTE_HABILITAR_TRAVA_CANCELAMENTO_CTE";

		/// <summary>
		/// Initializes a new instance of the <see cref="CteService"/> class.
		/// </summary>
		/// <param name="cteRepository">Repositório do Cte injetado</param>
		/// <param name="fluxoComercialRepository">Repositório do fluxo comercial injetado</param>
		/// <param name="cteComplementadoRepository">Repositório do cte complementado injetado</param>
		/// <param name="itemDespachoRepository">Repositório do item de despacho injetado</param>
		/// <param name="cteEnvioPoolingRepository">Repositório do envio para o pooling injetado</param>
		/// <param name="cteRecebimentoPoolingRepository">Repositório do recebimento do pooling injetado</param>
		/// <param name="cteLogService"> Serviço do log do cte injetado</param>
		/// <param name="cteStatusRepository">Repositório do status do cte injetado</param>
		/// <param name="databaseInputRepository">Repositório do tbDatabaseInput injetado</param>
		/// <param name="cteArquivoFtpRepository">Repositório de CteArquivoFtp injetado</param>
		/// <param name="cteStatusRetornoRepository">Repositorio de CteStatusRetorno injetado</param>
		/// <param name="usuarioRepository">Repositorio de Usuario injetado</param>
		/// <param name="cteDetalheRepository">Repositório do Cte Detalhe injetad</param>
		/// <param name="cteArvoreRepository">Repositório do Cte Arvore injetado</param>
		/// <param name="cteAgrupamentoRepository">Repositório do Cte Agrupamento injetado</param>
		/// <param name="cteTempRepository">Repositório do Cte Temp injetado</param>
		/// <param name="vagaoRepository">Repotório de Vagão Injetado</param>
		/// <param name="cteTempDetalheRepository">Repositório de CteTempDetalhe injetado</param>
		/// <param name="filaProcessamentoService">Serviço para gravar na fila de processamento da Config</param>
		/// <param name="configuracaoTranslogicRepository">Repositório de configuração do translogic injetado</param>
		/// <param name="cteInterfaceRecebimentoConfigRepository">Repositório de interface de recebimento dos arquivos da config injetado</param>
		/// <param name="cteInterfaceEnvioSapRepository">Repositório de interface de envio de cte para o SAP injetado</param>
		/// <param name="cteArquivoRepository">Repositório do cte arquivo injetado</param>
		/// <param name="cteInterfacePdfRepository">Repositório da interface de importação do Pdf</param>
		/// <param name="vsapZSDV0203VRepository">Repositório da interface de vSapZSDV0203V (Verifica cte pago)</param>
		/// <param name="cteSapPoolingRepository">Repositório do pooling de envio para o SAP</param>
		/// <param name="cteInterfaceXmlRepository">Repositório do pooling de geração do XML</param>
		/// <param name="cteInterfaceEnvioEmailRepository">Repositoório de interface de envio de email</param>
		/// <param name="cteEnvioXmlRepository">Repositório do envio de xml injetado</param>
		/// <param name="cteEnvioEmailHistoricoRepository">Repositório de envio de email historico xml</param>
		/// <param name="nfe03FilialRepository">Filial da nota fiscal eletronica injetado</param>
		/// <param name="cteEnvioEmailErroRepository">Repositório de envio de email com erro</param>
		/// <param name="cteMotivoCancelamentoRepository">Repositório de motivo de cancelamento injetado</param>
		/// <param name="agrupamentoUsuarioRepository"> Repositório de agrupamento de usuário injetado</param>
		/// <param name="serieDespachoUfRepository">Repositório de Serie Despacho UF</param>
		/// <param name="cteComunicadoRepository"> Repositório Comunicado injetado</param>
		/// <param name="controleAcessoService">  Serviço Controle de acesso </param>
		public CteService(ICteRepository cteRepository, IFluxoComercialRepository fluxoComercialRepository, ICteComplementadoRepository cteComplementadoRepository, IItemDespachoRepository itemDespachoRepository, ICteEnvioPoolingRepository cteEnvioPoolingRepository, ICteRecebimentoPoolingRepository cteRecebimentoPoolingRepository, CteLogService cteLogService, ICteStatusRepository cteStatusRepository, ITbDatabaseInputRepository databaseInputRepository, ICteArquivoFtpRepository cteArquivoFtpRepository, ICteStatusRetornoRepository cteStatusRetornoRepository, IUsuarioRepository usuarioRepository, ICteDetalheRepository cteDetalheRepository, ICteArvoreRepository cteArvoreRepository, ICteAgrupamentoRepository cteAgrupamentoRepository, ICteTempRepository cteTempRepository, IVagaoRepository vagaoRepository, ICteTempDetalheRepository cteTempDetalheRepository, FilaProcessamentoService filaProcessamentoService, IConfiguracaoTranslogicRepository configuracaoTranslogicRepository, ICteInterfaceRecebimentoConfigRepository cteInterfaceRecebimentoConfigRepository, ICteInterfaceEnvioSapRepository cteInterfaceEnvioSapRepository, ICteArquivoRepository cteArquivoRepository, ICteInterfacePdfConfigRepository cteInterfacePdfRepository, IVSapZSDV0203VRepository vsapZSDV0203VRepository, ICteSapPoolingRepository cteSapPoolingRepository, ICteInterfaceXmlConfigRepository cteInterfaceXmlRepository, ICteInterfaceEnvioEmailRepository cteInterfaceEnvioEmailRepository, ICteEnvioXmlRepository cteEnvioXmlRepository, ICteEnvioEmailHistoricoRepository cteEnvioEmailHistoricoRepository, INfe03FilialRepository nfe03FilialRepository, ICteEnvioEmailErroRepository cteEnvioEmailErroRepository, ICteMotivoCancelamentoRepository cteMotivoCancelamentoRepository, IAgrupamentoUsuarioRepository agrupamentoUsuarioRepository, ISerieDespachoUfRepository serieDespachoUfRepository, ICteComunicadoRepository cteComunicadoRepository, IControleAcessoService controleAcessoService)
		{
			_cteRepository = cteRepository;
			_controleAcessoService = controleAcessoService;
			_cteComunicadoRepository = cteComunicadoRepository;
			_serieDespachoUfRepository = serieDespachoUfRepository;
			_agrupamentoUsuarioRepository = agrupamentoUsuarioRepository;
			_cteMotivoCancelamentoRepository = cteMotivoCancelamentoRepository;
			_cteEnvioEmailErroRepository = cteEnvioEmailErroRepository;
			_nfe03FilialRepository = nfe03FilialRepository;
			_cteEnvioEmailHistoricoRepository = cteEnvioEmailHistoricoRepository;
			_cteEnvioXmlRepository = cteEnvioXmlRepository;
			_cteInterfaceEnvioEmailRepository = cteInterfaceEnvioEmailRepository;
			_cteInterfaceXmlRepository = cteInterfaceXmlRepository;
			_cteSapPoolingRepository = cteSapPoolingRepository;
			_cteInterfacePdfRepository = cteInterfacePdfRepository;
			_cteArquivoRepository = cteArquivoRepository;
			_cteInterfaceEnvioSapRepository = cteInterfaceEnvioSapRepository;
			_cteInterfaceRecebimentoConfigRepository = cteInterfaceRecebimentoConfigRepository;
			_configuracaoTranslogicRepository = configuracaoTranslogicRepository;
			_cteTempRepository = cteTempRepository;
			_cteTempDetalheRepository = cteTempDetalheRepository;
			_vagaoRepository = vagaoRepository;
			_filaProcessamentoService = filaProcessamentoService;
			_usuarioRepository = usuarioRepository;
			_cteStatusRetornoRepository = cteStatusRetornoRepository;
			_cteArquivoFtpRepository = cteArquivoFtpRepository;
			_databaseInputRepository = databaseInputRepository;
			_cteStatusRepository = cteStatusRepository;
			_cteLogService = cteLogService;
			_cteRecebimentoPoolingRepository = cteRecebimentoPoolingRepository;
			_cteEnvioPoolingRepository = cteEnvioPoolingRepository;
			_fluxoComercialRepository = fluxoComercialRepository;
			_cteComplementadoRepository = cteComplementadoRepository;
			_itemDespachoRepository = itemDespachoRepository;
			_cteDetalheRepository = cteDetalheRepository;
			_cteArvoreRepository = cteArvoreRepository;
			_cteAgrupamentoRepository = cteAgrupamentoRepository;
			_vsapZSDV0203VRepository = vsapZSDV0203VRepository;
		}

		/// <summary>
		/// Retorna a lista de Cte com a flag de Pago preenchida
		/// </summary> 
		/// <param name="dtos">lista de cte dto</param>
		/// <returns>Retorna a lista dos ctes com a flag de Pago preenchida</returns>
		public IList<CteDto> RetornaCtePagos(CteDto[] dtos)
		{
			List<int> listaIdCte = new List<int>();

			foreach (CteDto dto in dtos)
			{
				listaIdCte.Add(dto.CteId);
			}

			IList<CteDto> retorno = _cteRepository.ObterCtesPago(listaIdCte);

			return retorno.Where(c => c.CtePago).OrderBy(c => c.Cte).ToList();
		}

		/// <summary>
		/// Retorna todos os Ctes para cancelamento
		/// </summary>
		/// <param name="pagination"> Paginacao pesquisa</param>
		/// <param name="filtros"> Filtros para pesquisa</param>
		/// <returns>lista de Cte</returns>
		public ResultadoPaginado<CteDto> RetornaCteParaCancelamento(DetalhesPaginacaoWeb pagination, DetalhesFiltro<object>[] filtros)
		{
			DateTime dataIni = DateTime.Now;
			DateTime dataFim = DateTime.Now;
			string serie = string.Empty;
			int numDespacho = 0;
			string erro = string.Empty;
			string fluxo = string.Empty;
			string origem = string.Empty;
			string destino = string.Empty;
			string numVagao = string.Empty;
			string chaveCte = string.Empty;
			int diasForaData = 0;
			bool filtroForaData = false;
			string codigoUfDcl = string.Empty;

			ConfiguracaoTranslogic dataMaximaCancelamento = _configuracaoTranslogicRepository.ObterPorId(_chaveDataMaximaCancelamento);

			diasForaData = int.Parse(dataMaximaCancelamento.Valor);

			if (filtros != null)
			{
				foreach (var detalheFiltro in filtros)
				{
					if (detalheFiltro.Campo.Equals("dataInicial") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						string texto = detalheFiltro.Valor[0].ToString();

						dataIni = DateTime.ParseExact(texto, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
					}

					if (detalheFiltro.Campo.Equals("dataFinal") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						string texto = detalheFiltro.Valor[0].ToString();

						dataFim = DateTime.ParseExact(texto, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);

						dataFim = dataFim.AddDays(1).AddSeconds(-1);
					}

					if (detalheFiltro.Campo.Equals("serie") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						serie = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
					}

					if (detalheFiltro.Campo.Equals("despacho") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						numDespacho = Convert.ToInt32(detalheFiltro.Valor[0].ToString());
					}

					if (detalheFiltro.Campo.Equals("fluxo") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						fluxo = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
					}

					if (detalheFiltro.Campo.Equals("Origem") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						origem = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
					}

					if (detalheFiltro.Campo.Equals("UfDcl") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						codigoUfDcl = detalheFiltro.Valor[0].ToString().ToUpperInvariant().Trim();
					}

					if (detalheFiltro.Campo.Equals("Destino") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						destino = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
					}

					if (detalheFiltro.Campo.Equals("chave") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						chaveCte = detalheFiltro.Valor[0].ToString();
					}

					if (detalheFiltro.Campo.Equals("errocte") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						erro = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
					}

					if (detalheFiltro.Campo.Equals("Vagao") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						numVagao = detalheFiltro.Valor[0].ToString();
					}

					if (detalheFiltro.Campo.Equals("ForaDataCancelamento") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						bool.TryParse(detalheFiltro.Valor[0].ToString(), out filtroForaData);
					}
				}
			}

			ResultadoPaginado<CteDto> listaDto = new ResultadoPaginado<CteDto>();

			listaDto = _cteRepository.ObterCteCancelamento(pagination, dataIni, dataFim, serie, numDespacho, fluxo, chaveCte, erro, origem, destino, numVagao, diasForaData, codigoUfDcl);

			if (filtroForaData)
			{
				listaDto.Items = listaDto.Items.Where(c => c.ForaDoTempoCancelamento).ToList();
			}

			/*
			IList<CteDto> listaDto = new List<CteDto>();

			CteDto dto = null;

			ConfiguracaoTranslogic dataMaximaCancelamento = _configuracaoTranslogicRepository.ObterPorId(_chaveDataMaximaCancelamento);

			DespachoTranslogic despacho = null;
			FluxoComercial fluxoComercial = null;
			Mercadoria mercadoria = null;
			EmpresaCliente empresaClientePagadora = null;
			EstacaoMae estacaoMaeOrigem = null;
			EstacaoMae estacaoMaeDestino = null;
			SerieDespacho serieDespacho = null;
			SerieDespachoUf serieDespachoUf = null;

			IList<Cte> listaCte = _cteRepository.Obter(dataIni, dataFim, serie, numDespacho, fluxo.ToUpper(), string.Empty, 0);

			foreach (Cte cte in listaCte)
			{
																																																																																																																																																																																																																																																																			bool cteComAgrupamentoNaoAutorizado = false;

																																																																																																																																																																																																																																																																			bool ctePago = false;

																																																																																																																																																																																																																																																																			bool foraDoTempoCancelamento = false;

																																																																																																																																																																																																																																																																			IList<CteArvore> listaArvore = _cteArvoreRepository.ObterArvorePorCteFilho(cte);

																																																																																																																																																																																																																																																																			CteArvore cteArvore = listaArvore.OrderByDescending(c => c.Id).FirstOrDefault();

																																																																																																																																																																																																																																																																			if (cteArvore == null)
																																																																																																																																																																																																																																																																			{
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			continue;
																																																																																																																																																																																																																																																																			}

																																																																																																																																																																																																																																																																			IList<CteAgrupamento> listaCteAgrupamento = _cteAgrupamentoRepository.ObterAgrupamentoPorCteFilho(cteArvore.CteFilho);

																																																																																																																																																																																																																																																																			foreach (CteAgrupamento cteAgrupamento in listaCteAgrupamento)
																																																																																																																																																																																																																																																																			{
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			if (cteAgrupamento.CteFilho.SituacaoAtual != SituacaoCteEnum.Autorizado)
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			{
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			cteComAgrupamentoNaoAutorizado = true;
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			}
																																																																																																																																																																																																																																																																			}

																																																																																																																																																																																																																																																																			Cte cteRaiz = _cteAgrupamentoRepository.ObterCteRaizPorCteFilho(cteArvore.CteFilho);

																																																																																																																																																																																																																																																																			if (cteRaiz != null)
																																																																																																																																																																																																																																																																			{
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			if (ContainCteDto(listaDto.ToList(), typeof(IList<CteDto>), (int)cteRaiz.Id))
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			{
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			continue;
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			}

																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			despacho = cteRaiz.Despacho;

																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			// para ser listado tem que ter despacho e ser autorizado no SEFAZ
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			if (despacho != null && cteRaiz.SituacaoAtual == SituacaoCteEnum.Autorizado)
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			{
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			serieDespacho = despacho.SerieDespacho;
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			serieDespachoUf = despacho.SerieDespachoUf;

																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			int serieDesp5 = serieDespacho != null ? Convert.ToInt32(serieDespacho.SerieDespachoNum) : 0;
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			int numDesp5 = despacho.NumeroDespacho ?? 0;

																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			// Verifica se o CTE já foi pago
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			VSapZSDV0203V vsapZSDV0203V = _vsapZSDV0203VRepository.ObterPorSerieDespachoTipo(serieDesp5, numDesp5, "DES");
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			if (vsapZSDV0203V != null)
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			{
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			if (vsapZSDV0203V.Compensacao.Trim() != string.Empty)
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			{
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			ctePago = true;
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			}
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			}

																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			// Verifica se o Cte está fora do tempo de cancelamento (7 dias após autorização)
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			DateTime dataAutorizacaoCte = cteRaiz.StatusVigente.DataHora;
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			if (DateTime.Now.Subtract(dataAutorizacaoCte).Days > Convert.ToInt32(dataMaximaCancelamento.Valor))
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																		{
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			foraDoTempoCancelamento = true;
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			}

																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			fluxoComercial = cteRaiz.FluxoComercial;

																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			if (fluxoComercial != null)
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			{
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			mercadoria = fluxoComercial.Mercadoria;
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			empresaClientePagadora = fluxoComercial.EmpresaPagadora;
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			estacaoMaeOrigem = fluxoComercial.Origem;
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			estacaoMaeDestino = fluxoComercial.Destino;

																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			dto = new CteDto();
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			dto.CteId = cteRaiz.Id ?? 0;
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			dto.Fluxo = fluxoComercial.Codigo;
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			dto.Origem = estacaoMaeOrigem != null ? estacaoMaeOrigem.Codigo : string.Empty;
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			dto.Destino = estacaoMaeDestino != null ? estacaoMaeDestino.Codigo : string.Empty;
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			dto.Mercadoria = mercadoria != null ? mercadoria.DescricaoResumida : string.Empty;
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			dto.ClienteFatura = empresaClientePagadora != null ? empresaClientePagadora.NomeFantasia : string.Empty;
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			dto.Cte = cteRaiz.Serie + "-" + cteRaiz.Numero;
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			dto.SerieDesp5 = serieDespacho.SerieDespachoNum;
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			dto.NumDesp5 = numDesp5;
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			dto.SerieDesp6 = serieDespachoUf != null ? serieDespachoUf.NumeroSerieDespacho : 0;
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			dto.NumDesp6 = despacho.NumeroDespacho ?? 0;
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			dto.DataEmissao = cteRaiz.DataHora;
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			dto.CteComAgrupamentoNaoAutorizado = cteComAgrupamentoNaoAutorizado;
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			dto.CtePago = ctePago;
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			dto.ForaDoTempoCancelamento = foraDoTempoCancelamento;

																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			if (foraDataCancelamento)
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			{
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			if (foraDoTempoCancelamento)
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			{
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			listaDto.Add(dto);
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			}
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			}
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			else
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			{
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			if (!foraDoTempoCancelamento)
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			{
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			listaDto.Add(dto);
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			}
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			}
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			}
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			}
																																																																																																																																																																																																																																																																			}
			}
			*/
			return listaDto;
		}

		/// <summary>
		/// Retorna todos os Ctes para cancelamento
		/// </summary>
		/// <param name="pagination"> Paginacao pesquisa</param>
		/// <param name="filtros"> Filtros para pesquisa</param>
		/// <returns>lista de Cte</returns>
		public ResultadoPaginado<CteDto> RetornaCteParaComplemento(DetalhesPaginacaoWeb pagination, DetalhesFiltro<object>[] filtros)
		{
			DateTime dataIni = DateTime.Now;
			DateTime dataFim = DateTime.Now;
			string serie = string.Empty;
			int numDespacho = 0;
			string erro = string.Empty;
			string fluxo = string.Empty;
			string origem = string.Empty;
			string destino = string.Empty;
			string numVagao = string.Empty;
			string chaveCte = string.Empty;
			int diasForaData = 0;
			bool filtroForaData = false;
			string codigoDcl = string.Empty;

			ConfiguracaoTranslogic dataMaximaCancelamento = _configuracaoTranslogicRepository.ObterPorId(_chaveDataMaximaCancelamento);

			diasForaData = int.Parse(dataMaximaCancelamento.Valor);

			if (filtros != null)
			{
				foreach (var detalheFiltro in filtros)
				{
					if (detalheFiltro.Campo.Equals("dataInicial") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						string texto = detalheFiltro.Valor[0].ToString();

						dataIni = DateTime.ParseExact(texto, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
					}

					if (detalheFiltro.Campo.Equals("dataFinal") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						string texto = detalheFiltro.Valor[0].ToString();

						dataFim = DateTime.ParseExact(texto, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);

						dataFim = dataFim.AddDays(1).AddSeconds(-1);
					}

					if (detalheFiltro.Campo.Equals("serie") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						serie = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
					}

					if (detalheFiltro.Campo.Equals("despacho") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						numDespacho = Convert.ToInt32(detalheFiltro.Valor[0].ToString());
					}

					if (detalheFiltro.Campo.Equals("fluxo") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						fluxo = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
					}

					if (detalheFiltro.Campo.Equals("Origem") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						origem = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
					}

					if (detalheFiltro.Campo.Equals("Destino") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						destino = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
					}

					if (detalheFiltro.Campo.Equals("chave") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						chaveCte = detalheFiltro.Valor[0].ToString();
					}

					if (detalheFiltro.Campo.Equals("errocte") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						erro = detalheFiltro.Valor[0].ToString();
					}

					if (detalheFiltro.Campo.Equals("Vagao") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						numVagao = detalheFiltro.Valor[0].ToString();
					}

					if (detalheFiltro.Campo.Equals("UfDcl") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						codigoDcl = detalheFiltro.Valor[0].ToString().ToUpperInvariant().Trim();
					}
				}
			}

			ResultadoPaginado<CteDto> listaDto = new ResultadoPaginado<CteDto>();

			listaDto = _cteRepository.ObterCteComplemento(pagination, dataIni, dataFim, serie, numDespacho, fluxo, chaveCte, erro, origem, destino, numVagao, codigoDcl);

			return listaDto;
		}

		/// <summary>
		/// Retorna todos os Ctes para cancelamento
		/// </summary>
		/// <param name="pagination"> Paginacao pesquisa</param>
		/// <param name="filtros"> Filtros para pesquisa</param>
		/// <returns>lista de Cte</returns>
		public ResultadoPaginado<CteDto> RetornaCteParaTakeOrPay(DetalhesPaginacaoWeb pagination, DetalhesFiltro<object>[] filtros)
		{
			DateTime dataIni = DateTime.Now;
			DateTime dataFim = DateTime.Now;
			string serie = string.Empty;
			int numDespacho = 0;
			string erro = string.Empty;
			string fluxo = string.Empty;
			string origem = string.Empty;
			string destino = string.Empty;
			string numVagao = string.Empty;
			string chaveCte = string.Empty;
			string codigoDcl = string.Empty;

			if (filtros != null)
			{
				foreach (var detalheFiltro in filtros)
				{
					if (detalheFiltro.Campo.Equals("dataInicial") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						string texto = detalheFiltro.Valor[0].ToString();

						dataIni = DateTime.ParseExact(texto, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
					}

					if (detalheFiltro.Campo.Equals("dataFinal") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						string texto = detalheFiltro.Valor[0].ToString();

						dataFim = DateTime.ParseExact(texto, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);

						dataFim = dataFim.AddDays(1).AddSeconds(-1);
					}

					if (detalheFiltro.Campo.Equals("serie") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						serie = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
					}

					if (detalheFiltro.Campo.Equals("despacho") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						numDespacho = Convert.ToInt32(detalheFiltro.Valor[0].ToString());
					}

					if (detalheFiltro.Campo.Equals("fluxo") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						fluxo = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
					}

					if (detalheFiltro.Campo.Equals("Origem") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						origem = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
					}

					if (detalheFiltro.Campo.Equals("Destino") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						destino = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
					}

					if (detalheFiltro.Campo.Equals("chave") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						chaveCte = detalheFiltro.Valor[0].ToString();
					}

					if (detalheFiltro.Campo.Equals("errocte") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						erro = detalheFiltro.Valor[0].ToString();
					}

					if (detalheFiltro.Campo.Equals("Vagao") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						numVagao = detalheFiltro.Valor[0].ToString();
					}

					if (detalheFiltro.Campo.Equals("UfDcl") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						codigoDcl = detalheFiltro.Valor[0].ToString().ToUpperInvariant().Trim();
					}
				}
			}

			ResultadoPaginado<CteDto> listaDto = new ResultadoPaginado<CteDto>();

			listaDto = _cteRepository.ObterCteTakeOrPay(pagination, dataIni, dataFim, serie, numDespacho, fluxo, chaveCte, erro, origem, destino, numVagao, codigoDcl);

			return listaDto;
		}

		/// <summary>
		/// Realiza o Cancelamento dos Ctes
		/// </summary>
		/// <param name="dtos">dtos dos ctes</param>
		/// <param name="idMotivoCancelamento"> Id do motivo de cancelamento</param>
		/// <param name="usuario">Usuário atual</param>
		/// <returns>resultado do cancelamento</returns>
		public KeyValuePair<bool, string> SalvarCancelamentoCtes(CteDto[] dtos, int idMotivoCancelamento, Usuario usuario)
		{
			IList<CteDto> listaPagos = null;
			IList<CteDto> listaDtos = null;
			StringBuilder mensagemErro = null;
			StringBuilder mensagemErroPrazoCancelamento = null;
			bool sucesso = true;
			bool travarCancelamneto = false;

			try
			{
				// instância de objeto
				mensagemErro = new StringBuilder();
				mensagemErroPrazoCancelamento = new StringBuilder();

				travarCancelamneto = RecuperarValorConfGeral(_chaveTravaCancelamento).Equals("S");

				// Seta a mensagem de erro
				// mensagemErro.AppendLine("Falha no Cancelamento!  <br/>");

				// Recupera a lista de Ctes
				listaPagos = RetornaCtePagos(dtos);
				listaDtos = dtos.OrderBy(c => c.Cte).ToList();

				// Verifica se possui ctes que já foram pagos
				if (listaPagos.Count > 0)
				{
					sucesso = false;
					// mensagemErro.AppendLine("O(s) Cte(s) abaixo já estão pago(s) e não podem ser cancelados <br/>");
				}

				// Remove os Ctes pagos
				foreach (CteDto cteDto in listaPagos)
				{
					mensagemErro.AppendLine(string.Concat("Impossível cancelar o CTE (", cteDto.Cte, ") CTE já está pago!"));
					CteDto dto = listaDtos.Where(c => c.CteId == cteDto.CteId).FirstOrDefault();
					listaDtos.Remove(dto);
				}

				CteMotivoCancelamento cteMotivoCancelamento = _cteMotivoCancelamentoRepository.ObterPorId(idMotivoCancelamento);

				foreach (CteDto dto in listaDtos)
				{
					Cte cte = _cteRepository.ObterPorId(dto.CteId);
					if (!cte.SituacaoAtual.Equals(SituacaoCteEnum.Autorizado))
					{
						continue;
					}

					if (travarCancelamneto && !VerificarPrazoCancelamento(cte.DataHora, usuario))
					{
						sucesso = false;

						if (mensagemErroPrazoCancelamento.Length > 0)
						{
							mensagemErroPrazoCancelamento.Append(", ");
						}

						mensagemErroPrazoCancelamento.Append(cte.Numero);

						// mensagemErro.AppendLine(string.Concat("Impossível cancelar o CTE (", cte.Numero, ") CTE está fora do prazo de cancelamento. \n Favor Entrar em contato com o Fiscal !"));
						continue;
					}

					cte.CteMotivoCancelamento = cteMotivoCancelamento;
					_cteRepository.Atualizar(cte);

					IList<CteAgrupamento> listaCteAgrupamento = _cteAgrupamentoRepository.ObterAgrupamentoPorCteFilho(cte);

					if (dto.ForaDoTempoCancelamento)
					{
						//	string hostName = Dns.GetHostName();

						// Se for fora do tempo de cancelamento
						// muda a situação para cancelado, 
						// e manda direto para a interface SAP
						foreach (CteAgrupamento cteAgrupamento in listaCteAgrupamento)
						{
							// mudando Status CTE para Cancelado (13)
							MudarSituacaoCte(cteAgrupamento.CteFilho, SituacaoCteEnum.Cancelado, usuario, new CteStatusRetorno { Id = 13 }, "Arquivo Cte alterado cancelado", string.Empty);

							CteInterfaceEnvioSap cteInterfaceEnvioSap = new CteInterfaceEnvioSap();
							cteInterfaceEnvioSap.Cte = cteAgrupamento.CteFilho;
							cteInterfaceEnvioSap.DataGravacaoRetorno = DateTime.Now;
							cteInterfaceEnvioSap.DataUltimaLeitura = DateTime.Now;
							cteInterfaceEnvioSap.DataEnvioSap = null;
							cteInterfaceEnvioSap.HostName = null;
							cteInterfaceEnvioSap.TipoOperacao = TipoOperacaoCteEnum.CancelamentoRejeitado;

							_cteInterfaceEnvioSapRepository.Inserir(cteInterfaceEnvioSap);
						}
					}
					else
					{
						foreach (CteAgrupamento cteAgrupamento in listaCteAgrupamento)
						{
							// mudando Status CTE para Aguardando Cancelamento (19)
							MudarSituacaoCte(cteAgrupamento.CteFilho, SituacaoCteEnum.AguardandoCancelamento, usuario, new CteStatusRetorno { Id = 19 }, "Arquivo Cte alterado para aguardando cancelamento", string.Empty);
						}
					}
				}
			}
			catch (Exception ex)
			{
				mensagemErro.AppendLine(ex.Message);
				return new KeyValuePair<bool, string>(false, mensagemErro.ToString());
			}

			if (!string.IsNullOrEmpty(mensagemErroPrazoCancelamento.ToString()))
			{
				mensagemErro.AppendLine(string.Format("Os CTes ({0}) são de meses anteriores e só poderão ser cancelados pelo fiscal.", mensagemErroPrazoCancelamento.ToString()));
			}

			return new KeyValuePair<bool, string>(sucesso, sucesso ? "Cancelado com Sucesso!" : mensagemErro.ToString());
		}

		/// <summary>
		/// Retorna todos os Cte para Complemento
		/// </summary>
		/// <param name="dataInicial">Data inicial de pesquisa</param>
		/// <param name="dataFinal">Data final de pesquisa</param>
		/// <param name="serie">Número da série</param>
		/// <param name="numDespacho">Número do Despacho</param>
		/// <param name="fluxo">Número do Fluxo</param>
		/// <returns>lista de Cte</returns>
		public IList<CteDto> RetornaCteParaComplemento(DateTime dataInicial, DateTime dataFinal, int serie, int numDespacho, string fluxo)
		{
			DateTime dataIni = new DateTime(dataInicial.Year, dataInicial.Month, dataInicial.Day);

			DateTime dataFim = new DateTime(dataFinal.Year, dataFinal.Month, dataFinal.Day, 23, 59, 59);

			IList<CteDto> listaDto = new List<CteDto>();

			CteDto dto = null;

			DespachoTranslogic despacho = null;
			FluxoComercial fluxoComercial = null;
			Mercadoria mercadoria = null;
			EmpresaCliente empresaClientePagadora = null;
			EstacaoMae estacaoMaeOrigem = null;
			EstacaoMae estacaoMaeDestino = null;

			IList<Cte> listaCte = _cteRepository.Obter(dataIni, dataFim, serie, numDespacho);

			foreach (Cte cte in listaCte)
			{
				bool cteComAgrupamentoNaoAutorizado = false;

				IList<CteArvore> listaArvore = _cteArvoreRepository.ObterArvorePorCteFilho(cte);

				CteArvore cteArvore = listaArvore.OrderByDescending(c => c.Id).FirstOrDefault();

				if (cteArvore == null)
				{
					continue;
				}

				IList<CteAgrupamento> listaCteAgrupamento = _cteAgrupamentoRepository.ObterAgrupamentoPorCteFilho(cteArvore.CteFilho);

				foreach (CteAgrupamento cteAgrupamento in listaCteAgrupamento)
				{
					if (cteAgrupamento.CteFilho.SituacaoAtual != SituacaoCteEnum.Autorizado)
					{
						cteComAgrupamentoNaoAutorizado = true;
					}
				}

				foreach (CteAgrupamento cteAgrupamento in listaCteAgrupamento)
				{
					Cte cteTmp = cteAgrupamento.CteFilho;

					if (ContainCteDto(listaDto.ToList(), typeof(IList<CteDto>), (int)cteTmp.Id))
					{
						continue;
					}

					despacho = cteTmp.Despacho;

					// para ser listado tem que ter despacho, ser autorizado no SEFAZ
					if (despacho != null && cteTmp.SituacaoAtual == SituacaoCteEnum.Autorizado)
					{
						fluxoComercial = cteTmp.FluxoComercial;

						if (fluxoComercial != null)
						{
							mercadoria = fluxoComercial.Mercadoria;
							empresaClientePagadora = fluxoComercial.EmpresaPagadora;
							estacaoMaeOrigem = fluxoComercial.Origem;
							estacaoMaeDestino = fluxoComercial.Destino;

							dto = new CteDto();
							dto.CteId = cteTmp.Id ?? 0;
							dto.Fluxo = fluxoComercial.Codigo;
							dto.Origem = estacaoMaeOrigem != null ? estacaoMaeOrigem.Codigo : string.Empty;
							dto.Destino = estacaoMaeDestino != null ? estacaoMaeDestino.Codigo : string.Empty;
							dto.Mercadoria = mercadoria != null ? mercadoria.DescricaoResumida : string.Empty;
							dto.ClienteFatura = empresaClientePagadora != null ? empresaClientePagadora.NomeFantasia : string.Empty;
							dto.Cte = cteTmp.Serie + "-" + cteTmp.Numero;
							dto.SerieDesp5 = despacho.SerieDespacho != null ? despacho.SerieDespacho.SerieDespachoNum : string.Empty;
							dto.NumDesp5 = despacho.NumeroDespacho ?? 0;
							dto.SerieDesp6 = despacho.SerieDespachoUf != null ? despacho.SerieDespachoUf.NumeroSerieDespacho : 0;
							dto.NumDesp6 = despacho.NumeroDespacho ?? 0;
							dto.DataEmissao = cteTmp.DataHora;
							dto.Complementado = cteTmp.Complemento != null ? true : false;
							dto.ValorCte = cteTmp.ValorCte != 0 ? cteTmp.ValorCte : 0;
							dto.ValorDiferencaComplemento = cteTmp.Complemento != null ? cteTmp.Complemento.ValorDiferenca : 0;
							dto.PesoVagao = cteTmp.PesoVagao ?? 0;
							dto.CodigoUsuario = cteTmp.Complemento != null ? cteTmp.Complemento.Usuario.Codigo : string.Empty;
							// dto.DataComplemento = cteTmp.Complemento != null ? cteTmp.Complemento.DataHora.ToString("dd/MM/yyyy HH:mm:ss") : string.Empty;
							dto.CteComAgrupamentoNaoAutorizado = cteComAgrupamentoNaoAutorizado;
							dto.CteRaiz = cteTmp.Id == cteAgrupamento.CteRaiz.Id ? true : false;
							listaDto.Add(dto);
						}
					}
				}
			}

			return listaDto;
		}

		/// <summary>
		/// Retorna todos os Cte para Take Or Pay
		/// </summary>
		/// <param name="dataInicial">Data inicial de pesquisa</param>
		/// <param name="dataFinal">Data final de pesquisa</param>
		/// <param name="serie">Número da série</param>
		/// <param name="numDespacho">Número do Despacho</param>
		/// <param name="fluxo">Número do Fluxo</param>
		/// <param name="serieCte">Série do CTE</param>
		/// <param name="nroCte">Número do CTE</param>
		/// <returns>lista de Cte</returns>
		public IList<CteDto> RetornaCteParaTakeOrPay(DateTime dataInicial, DateTime dataFinal, int serie, int numDespacho, string fluxo, int serieCte, int nroCte)
		{
			DateTime dataIni = new DateTime(dataInicial.Year, dataInicial.Month, dataInicial.Day);

			DateTime dataFim = new DateTime(dataFinal.Year, dataFinal.Month, dataFinal.Day, 23, 59, 59);

			IList<CteDto> listaDto = new List<CteDto>();

			CteDto dto = null;

			DespachoTranslogic despacho = null;
			FluxoComercial fluxoComercial = null;
			Mercadoria mercadoria = null;
			EmpresaCliente empresaClientePagadora = null;
			EstacaoMae estacaoMaeOrigem = null;
			EstacaoMae estacaoMaeDestino = null;

			IList<Cte> listaCte = _cteRepository.Obter(dataIni, dataFim, serie, numDespacho, string.Empty, string.Empty, serieCte, nroCte);

			foreach (Cte cte in listaCte)
			{
				bool cteComAgrupamentoNaoAutorizado = false;

				IList<CteArvore> listaArvore = _cteArvoreRepository.ObterArvorePorCteFilho(cte);

				CteArvore cteArvore = listaArvore.OrderByDescending(c => c.Id).FirstOrDefault();

				if (cteArvore == null)
				{
					continue;
				}

				IList<CteAgrupamento> listaCteAgrupamento = _cteAgrupamentoRepository.ObterAgrupamentoPorCteFilho(cteArvore.CteFilho);

				foreach (CteAgrupamento cteAgrupamento in listaCteAgrupamento)
				{
					if (cteAgrupamento.CteFilho.SituacaoAtual != SituacaoCteEnum.Autorizado)
					{
						cteComAgrupamentoNaoAutorizado = true;
					}
				}

				foreach (CteAgrupamento cteAgrupamento in listaCteAgrupamento)
				{
					Cte cteTmp = cteAgrupamento.CteFilho;

					if (ContainCteDto(listaDto.ToList(), typeof(IList<CteDto>), (int)cteTmp.Id))
					{
						continue;
					}

					despacho = cteTmp.Despacho;

					// para ser listado tem que ter despacho, ser autorizado no SEFAZ
					if (despacho != null && cteTmp.SituacaoAtual == SituacaoCteEnum.Autorizado)
					{
						fluxoComercial = cteTmp.FluxoComercial;

						if (fluxoComercial != null)
						{
							mercadoria = fluxoComercial.Mercadoria;
							empresaClientePagadora = fluxoComercial.EmpresaPagadora;
							estacaoMaeOrigem = fluxoComercial.Origem;
							estacaoMaeDestino = fluxoComercial.Destino;

							dto = new CteDto();
							dto.CteId = cteTmp.Id ?? 0;
							dto.Fluxo = fluxoComercial.Codigo;
							dto.Origem = estacaoMaeOrigem != null ? estacaoMaeOrigem.Codigo : string.Empty;
							dto.Destino = estacaoMaeDestino != null ? estacaoMaeDestino.Codigo : string.Empty;
							dto.Mercadoria = mercadoria != null ? mercadoria.DescricaoResumida : string.Empty;
							dto.ClienteFatura = empresaClientePagadora != null ? empresaClientePagadora.NomeFantasia : string.Empty;
							dto.Cte = cteTmp.Serie + "-" + cteTmp.Numero;
							dto.SerieDesp5 = despacho.SerieDespacho != null ? despacho.SerieDespacho.SerieDespachoNum : string.Empty;
							dto.NumDesp5 = despacho.NumeroDespacho ?? 0;
							dto.SerieDesp6 = despacho.SerieDespachoUf != null ? despacho.SerieDespachoUf.NumeroSerieDespacho : 0;
							dto.NumDesp6 = despacho.NumeroDespacho ?? 0;
							dto.DataEmissao = cteTmp.DataHora;
							dto.Complementado = cteTmp.Complemento != null ? true : false;
							dto.ValorCte = cteTmp.ValorCte != 0 ? cteTmp.ValorCte : 0;
							dto.ValorDiferencaComplemento = cteTmp.Complemento != null ? cteTmp.Complemento.ValorDiferenca : 0;
							dto.PesoVagao = cteTmp.PesoVagao ?? 0;
							dto.CodigoUsuario = cteTmp.Complemento != null ? cteTmp.Complemento.Usuario.Codigo : string.Empty;
							// dto.DataComplemento = cteTmp.Complemento != null ? cteTmp.Complemento.DataHora.ToString("dd/MM/yyyy HH:mm:ss") : string.Empty;
							dto.CteComAgrupamentoNaoAutorizado = cteComAgrupamentoNaoAutorizado;
							listaDto.Add(dto);
						}
					}
				}
			}

			return listaDto;
		}

		/// <summary>
		/// Realiza o salvamento dos complementos dos Ctes Selecionados
		/// </summary>
		/// <param name="dtos">dtos dos ctes</param>
		/// <param name="usuario">usuario logado</param>
		/// <param name="mensagem">Mensagem de status do salvamento</param>
		/// <returns>resultado do salvamento</returns>
		[Transaction]
		public virtual bool SalvarComplementoCtes(CteDto[] dtos, Usuario usuario, out string mensagem)
		{
			try
			{
				// Valida os CT-es verificando se tem algum fluxo de subst. tributária
				// caso sim não deixa realizar o complemento
				if (dtos.Length > 0 && usuario != null)
				{
					StringBuilder builder = new StringBuilder();
					foreach (CteDto dto in dtos)
					{
						FluxoComercial fluxo = _fluxoComercialRepository.ObterPorCodigo(dto.Fluxo);
						if (fluxo.SubstituicaoTributaria != null && fluxo.SubstituicaoTributaria.Value)
						{
							string serie = dto.SerieDesp6 == 0 && dto.NumDesp6 == 0
									? dto.SerieDesp5.ToString().PadLeft(3, '0')
									: string.Concat(dto.CodigoControle, "-", dto.SerieDesp6.ToString().PadLeft(3, '0'));

							string despacho = dto.SerieDesp6 == 0 && dto.NumDesp6 == 0
												? dto.NumDesp5.ToString().PadLeft(3, '0')
												: dto.NumDesp6.ToString().PadLeft(3, '0');

							string aux = string.Format("CT-e numero: {0} Fluxo: {1} Série: {2} Despacho: {3} <BR/>", dto.Cte, fluxo.Codigo, serie, despacho);
							builder.Append(aux);
						}
					}

					if (builder.Length > 0)
					{
						builder.Insert(0, "Não é permitido realizar complemento de Cte de Substituição Tributária para o(s) seguinte(s) CT-e(s):</BR>");
						mensagem = builder.ToString();
						return false;
					}
				}

				int idCteComplementar = 0;

				if (dtos.Length > 0 && usuario != null)
				{
					foreach (CteDto dto in dtos)
					{
						FluxoComercial fluxoComercial = _fluxoComercialRepository.ObterPorCodigo(dto.Fluxo);
						CteTemp cteTemp = new CteTemp();
						cteTemp.IdFluxoComercial = fluxoComercial.Id;
						cteTemp.CodigoVagao = _cteRepository.ObterPorId(dto.CteId).Vagao.Codigo;
						_cteTempRepository.InserirOuAtualizar(cteTemp);

						_cteRepository.GerarCteComplementar(usuario, out idCteComplementar);
						if (idCteComplementar != 0)
						{
							CteComplementado cteComplementado = null;
							cteComplementado = new CteComplementado();
							cteComplementado.Cte = _cteRepository.ObterPorId(dto.CteId);
							cteComplementado.ValorDiferenca = dto.ValorDiferencaComplemento;
							cteComplementado.DataHora = DateTime.Now;
							cteComplementado.Usuario = usuario;
							cteComplementado.CteComplementar = _cteRepository.ObterPorId(idCteComplementar);

							_cteComplementadoRepository.Inserir(cteComplementado);
						}

						_cteTempRepository.Remover(cteTemp);
					}
				}
				else
				{
					mensagem = "Cte Complementar não foi criado!";
					return false;
				}
			}
			catch (Exception e)
			{
				mensagem = e.Message;
				return false;
			}

			mensagem = "Ação efetuada com Sucesso!";
			return true;

			/*
			try
			{
				int idCteComplementar = 0;

				if (dtos.Length > 0 && usuario != null)
				{
					FluxoComercial fluxoComercial = _fluxoComercialRepository.ObterPorCodigo(dtos[0].Fluxo);
					CteTemp cteTemp = new CteTemp();
					cteTemp.IdFluxoComercial = fluxoComercial.Id;
					cteTemp.CodigoVagao = _cteRepository.ObterPorId(dtos[0].CteId).Vagao.Codigo;
					_cteTempRepository.InserirOuAtualizar(cteTemp);

					_cteRepository.GerarCteComplementar(usuario, out idCteComplementar);
				}

				if (idCteComplementar != 0)
				{
					CteComplementado cteComplementado = null;

					foreach (CteDto dto in dtos)
					{
						cteComplementado = new CteComplementado();
						cteComplementado.Cte = _cteRepository.ObterPorId(dto.CteId);
						cteComplementado.ValorDiferenca = dto.ValorDiferencaComplemento;
						cteComplementado.DataHora = DateTime.Now;
						cteComplementado.Usuario = usuario;
						cteComplementado.CteComplementar = _cteRepository.ObterPorId(idCteComplementar);

						_cteComplementadoRepository.Inserir(cteComplementado);
					}
				}
				else
				{
					mensagem = "Cte Complementar não foi criado!";
					return false;
				}
			}
			catch (Exception e)
			{
				mensagem = e.Message;
				return false;
			}

			mensagem = "Ação efetuada com Sucesso!";
			return true;
			 */
		}

		/// <summary>
		/// Realiza o salvamento dos complementos Take or Pay dos Ctes Selecionados
		/// </summary>
		/// <param name="dtos">dtos dos ctes</param>
		/// <param name="usuario">usuario logado</param>
		/// <param name="mensagem">mensagem de status do salvamento</param>
		/// <returns>resultado do salvamento</returns>
		[Transaction]
		public virtual bool SalvarComplementoTakeOrPayCtes(CteDto[] dtos, Usuario usuario, out string mensagem)
		{
			try
			{
				if (dtos.Length > 0)
				{
					double valorTotal = dtos[0].ValorTotal;
					string codigoFluxoReferencia = dtos[0].FluxoReferencia.ToUpperInvariant();

					// FluxoComercial fluxoComercialReferencia = _fluxoComercialRepository.ObterPorCodigo(codigoFluxoReferencia);
					FluxoComercial fluxoComercialReferencia = _fluxoComercialRepository.ObterFerroviarioPorCodigo(codigoFluxoReferencia);

					if (fluxoComercialReferencia == null)
					{
						mensagem = "Fluxo Referência não encontrado!";
						return false;
					}

					Mercadoria mercadoriaReferencia = fluxoComercialReferencia.Mercadoria;

					EstacaoMae estacaoMaeOrigemReferencia = fluxoComercialReferencia.Origem;
					EstacaoMae estacaoMaeDestinoReferencia = fluxoComercialReferencia.Destino;

					EmpresaCliente empresaClienteRemetenteReferencia = fluxoComercialReferencia.EmpresaRemetente;
					EmpresaCliente empresaClienteDestinatariaReferencia = fluxoComercialReferencia.EmpresaDestinataria;
					EmpresaCliente empresaClientePagadoraReferencia = fluxoComercialReferencia.EmpresaPagadora;

					foreach (CteDto dto in dtos)
					{
						Cte cte = _cteRepository.ObterPorId(dto.CteId);
						FluxoComercial fluxoComercial = cte.FluxoComercial;

						Mercadoria mercadoria = fluxoComercial.Mercadoria;

						EstacaoMae estacaoMaeOrigem = fluxoComercial.Origem;
						EstacaoMae estacaoMaeDestino = fluxoComercial.Destino;

						EmpresaCliente empresaClienteRemetente = fluxoComercial.EmpresaRemetente;
						EmpresaCliente empresaClienteDestinataria = fluxoComercial.EmpresaDestinataria;
						EmpresaCliente empresaClientePagadora = fluxoComercial.EmpresaPagadora;

						// Verifica se o fluxo referencia tem mesma Origem, mesmo Destino, mesma Mercadoria
						// também os clientes devem ser igual, sendo cliente remetente, destinatario e correntista;
						/*    if (mercadoriaReferencia.Id != mercadoria.Id)
{
mensagem = "Mercadoria do Cte: " + cte.Numero + " está diferente ao do Fluxo Referência!";
return false;
}

if (estacaoMaeOrigemReferencia.Codigo != estacaoMaeOrigem.Codigo)
{
mensagem = "Origem do Cte: " + cte.Numero + " está diferente ao do Fluxo Referência!";
return false;
}
if (estacaoMaeDestinoReferencia.Codigo != estacaoMaeDestino.Codigo)
{
mensagem = "Destino do Cte: " + cte.Numero + " está diferente ao do Fluxo Referência!";
return false;
}

if (empresaClienteRemetenteReferencia.Id != empresaClienteRemetente.Id)
{
mensagem = "Empresa Remetente do Cte: " + cte.Numero + " está diferente ao do Fluxo Referência!";
return false;
}

if (empresaClienteDestinatariaReferencia.Id != empresaClienteDestinataria.Id)
{
mensagem = "Empresa Destinatária do Cte: " + cte.Numero + " está diferente ao do Fluxo Referência!";
return false;
}

if (empresaClientePagadoraReferencia.Id != empresaClientePagadora.Id)
{
mensagem = "Empresa Pagadora do Cte: " + cte.Numero + " está diferente ao do Fluxo Referência!";
return false;
} */
					}

					// Divide o valor total entre os ctes
					double valorRateado = valorTotal / dtos.Length;

					int idCteComplementar = 0;

					CteTemp cteTemp = new CteTemp();
					// cteTemp.IdFluxoComercial = _fluxoComercialRepository.ObterPorCodigo(codigoFluxoReferencia).Id;
					cteTemp.IdFluxoComercial = _fluxoComercialRepository.ObterFerroviarioPorCodigo(codigoFluxoReferencia).Id;
					cteTemp.CodigoVagao = _cteRepository.ObterPorId(dtos[0].CteId).Vagao.Codigo;
					_cteTempRepository.InserirOuAtualizar(cteTemp);
					_cteRepository.GerarCteComplementar(usuario, out idCteComplementar);

					if (idCteComplementar != 0)
					{
						CteComplementado cteComplementado = null;

						foreach (CteDto dto in dtos)
						{
							cteComplementado = new CteComplementado();
							cteComplementado.Cte = _cteRepository.ObterPorId(dto.CteId);
							cteComplementado.ValorDiferenca = valorRateado;
							cteComplementado.DataHora = DateTime.Now;
							cteComplementado.Usuario = usuario;
							cteComplementado.CteComplementar = _cteRepository.ObterPorId(idCteComplementar);

							_cteComplementadoRepository.InserirOuAtualizar(cteComplementado);
						}
					}
					else
					{
						mensagem = "Cte Complementar não foi criado!";
						return false;
					}
				}
			}
			catch (Exception e)
			{
				mensagem = e.Message;
				return false;
			}

			mensagem = "Ação efetuada com Sucesso!";
			return true;
		}

		/// <summary>
		/// Realiza a reemissão do cte
		/// </summary>
		/// <param name="listaCte">lista de Id do cte a ser emitido</param>
		/// <param name="usuario"> Usuario que fez a manutencao</param>
		public virtual void SalvarLoteManutencaoCte(List<int> listaCte, Usuario usuario)
		{
			foreach (int idCte in listaCte)
			{
				SalvarManutencaoCte(idCte, usuario);
			}
		}

		/// <summary>
		/// Realiza a reemissão do cte
		/// </summary>
		/// <param name="idCte">Id do cte a ser emitido</param>
		/// <param name="usuario"> Usuario que fez a manutencao</param>
		[Transaction]
		public virtual void SalvarManutencaoCte(int idCte, Usuario usuario)
		{
			Cte cte = _cteRepository.ObterPorId(idCte);
			string hostName = Dns.GetHostName();

			try
			{
				// Insere no log do cte
				_cteLogService.InserirLogInfo(cte, "CteService", string.Format("Gravando cte {0} nas tabelas temporárias. - HostName:{1} - IdUsuario:{2}", cte.Id, hostName, usuario.Id));

				// Caso o Cte seja EAR, então altera a situação para INV.
				AtualizarStatusCteManutInvalida(idCte);

				CteTemp cteTemp = new CteTemp();
				int index = 0;

				cteTemp.Id = cte.Vagao.Id.HasValue ? cte.Vagao.Id.Value : 0;
				cteTemp.CodigoVagao = cte.Vagao.Codigo;
				cteTemp.IdCteOrigem = idCte;
				cteTemp.IdFluxoComercial = cte.FluxoComercial.Id;
				cteTemp.IdDespacho = cte.Despacho.Id.HasValue ? cte.Despacho.Id.Value : 0;
				cteTemp.PesoVagao = cte.PesoVagao.HasValue ? cte.PesoVagao.Value : 0;
				cteTemp.VolumeVagao = cte.VolumeVagao;

				_cteTempRepository.Inserir(cteTemp);
				IList<CteDetalhe> listaDetalhes = _cteDetalheRepository.ObterPorCte(cte);

				// Regra para validar se possui nota fiscal duplicada.
				// É possível usar a mesma Nota Fiscal 2 ou mais vezes no vagão apenas em casos de conteiners diferentes.
				foreach (CteDetalhe detalhe in listaDetalhes)
				{
					List<CteDetalhe> retorno = listaDetalhes.Where(c => c.NumeroNota == detalhe.NumeroNota).ToList();

					if (retorno.Count > 1)
					{
						foreach (CteDetalhe cteDetalhe in retorno)
						{
							List<CteDetalhe> countConteiner = retorno.Where(c => c.ConteinerNotaFiscal == cteDetalhe.ConteinerNotaFiscal).ToList();

							if (countConteiner.Count > 1)
							{
								throw new Exception("É possível usar a mesma Nota Fiscal 2 ou mais vezes no vagão apenas em casos de conteiners diferentes.");
							}
						}
					}
				}

				_cteLogService.InserirLogInfo(cte, "CteService", string.Format("Gravando {0} Detalhes na CteTempDetalhe do cte {1} - HostName:{2} - IdUsuario:{3} ", listaDetalhes.Count, cte.Id, hostName, usuario.Id));

				foreach (CteDetalhe detalhe in listaDetalhes)
				{
					CteTempDetalhe cteTempDetalhe = new CteTempDetalhe();
					cteTempDetalhe.Id = (++index).ToString();
					cteTempDetalhe.CodigoNfe = string.Concat(detalhe.NumeroNota, detalhe.SerieNota);
					cteTempDetalhe.PesoNotaFiscal = detalhe.PesoNotaFiscal;
					cteTempDetalhe.ValorNotaFiscal = detalhe.ValorNotaFiscal;
					cteTempDetalhe.PesoTotalNotaFiscal = detalhe.PesoTotal;
					cteTempDetalhe.TipoMoeda = "BRL";
					cteTempDetalhe.ValorTotalNotaFiscal = detalhe.ValorTotalNotaFiscal;

					cteTempDetalhe.ChaveNfe = detalhe.ChaveNfe;
					cteTempDetalhe.CnpjDestinatario = detalhe.CgcDestinatario;
					cteTempDetalhe.CnpjRemetente = detalhe.CgcRemetente;
					cteTempDetalhe.ConteinerNotaFiscal = detalhe.ConteinerNotaFiscal;
					cteTempDetalhe.DataCadastro = detalhe.DataCadastro;
					cteTempDetalhe.DataNotaFiscal = detalhe.DataNotaFiscal;
					cteTempDetalhe.IdEmpresaDestinatario = detalhe.IdDestinatario;
					cteTempDetalhe.IdEmpresaRemetente = detalhe.IdRementente;
					cteTempDetalhe.NumeroNotaFiscal = detalhe.NumeroNota;
					cteTempDetalhe.IdVagao = cte.Vagao.Id.Value;
					cteTempDetalhe.InscricaoEstadualDestinatario = detalhe.InsEstadualDestinatario;
					cteTempDetalhe.InscricaoEstadualRemetente = detalhe.InsEstadualRemetente;
					cteTempDetalhe.NumeroTif = detalhe.NumeroTif;
					cteTempDetalhe.SerieNotaFiscal = detalhe.SerieNota;
					cteTempDetalhe.SiglaDestinatario = detalhe.UfDestinatario;
					cteTempDetalhe.SiglaRemetente = detalhe.UfRemetente;
					cteTempDetalhe.UfDestinatario = detalhe.UfDestinatario;
					cteTempDetalhe.UfRemetente = detalhe.UfRemetente;
					cteTempDetalhe.VolumeNotaFiscal = detalhe.VolumeNotaFiscal;

					_cteTempDetalheRepository.Inserir(cteTempDetalhe);
				}

				_cteTempRepository.GerarCteManutencaoPorTemporarias(usuario);

				// limpa a tabela temporaria
				_cteTempDetalheRepository.RemoverTodos();
				_cteTempRepository.RemoverTodos();
			}
			catch (Exception ex)
			{
				_cteLogService.InserirLogErro(cte, "CteService", string.Format("Erro na geração do cte: {0} - Erro: {1} - HostName:{2} - IdUsuario:{3} ", cte.Id, ex.Message, hostName, usuario.Id), ex);
				throw ex;
			}
		}

		/// <summary>
		/// Realiza o salvamento do status dos Ctes em manutenção
		/// </summary>
		/// <param name="dto">dtos do cte a ser feito manutencao</param>
		/// <param name="usuario">usuario logado</param>
		[Transaction]
		public virtual void SalvarManutencaoCte(ManutencaoCteTempDto dto, Usuario usuario)
		{
			try
			{
				int index = 0;
				bool existeAlteracao = VerificarExisteAlteracaoParaManutencao(dto);
				if (!existeAlteracao)
				{
					throw new TranslogicException("Não foram encontradas alterações no CT-e.");
				}

				// Caso o Cte seja EAR, então altera a situação para INV.
				AtualizarStatusCteManutInvalida(dto.CteTemp.IdCteOrigem.Value);

				CteTemp cteTemp = dto.CteTemp;
				_cteTempRepository.Inserir(cteTemp);
				foreach (CteTempDetalheViewModel cteTempDetalheViewModel in dto.ListaDetalhes)
				{
					/* double valorRateio;
					 double? pesoRateio;
					 double pesoTotal;

					 if (cteTempDetalheViewModel.VolumeNotaFiscal.HasValue && cteTemp.VolumeVagao.HasValue)
					 {
						pesoRateio = Math.Round(cteTemp.PesoVagao * cteTempDetalheViewModel.VolumeNotaFiscal.Value / cteTemp.VolumeVagao.Value, 3);
						pesoTotal = pesoRateio.Value;
						valorRateio = cteTempDetalheViewModel.ValorTotalNotaFiscal;
					 }
					 else
					 {
						pesoRateio = cteTempDetalheViewModel.PesoNotaFiscal;
						pesoTotal = cteTempDetalheViewModel.PesoTotalNotaFiscal;
						valorRateio = !string.IsNullOrEmpty(cteTempDetalheViewModel.ChaveNfe) ? Math.Round(cteTempDetalheViewModel.ValorTotalNotaFiscal * cteTempDetalheViewModel.PesoNotaFiscal.Value / cteTempDetalheViewModel.PesoTotalNotaFiscal, 2) : cteTempDetalheViewModel.ValorNotaFiscal.Value;
					 }
					 */

					CteTempDetalhe cteTempDetalhe = new CteTempDetalhe();
					cteTempDetalhe.Id = (++index).ToString();
					cteTempDetalhe.CodigoNfe = string.Concat(cteTempDetalheViewModel.NumeroNotaFiscal, cteTempDetalheViewModel.SerieNotaFiscal);
					cteTempDetalhe.PesoNotaFiscal = cteTempDetalheViewModel.PesoNotaFiscal;
					cteTempDetalhe.ValorNotaFiscal = cteTempDetalheViewModel.ValorNotaFiscal;
					cteTempDetalhe.PesoTotalNotaFiscal = cteTempDetalheViewModel.PesoTotalNotaFiscal;
					cteTempDetalhe.TipoMoeda = "BRL";
					cteTempDetalhe.ValorTotalNotaFiscal = cteTempDetalheViewModel.ValorTotalNotaFiscal;

					cteTempDetalhe.ChaveNfe = cteTempDetalheViewModel.ChaveNfe;
					cteTempDetalhe.CnpjDestinatario = cteTempDetalheViewModel.CnpjDestinatario;
					cteTempDetalhe.CnpjRemetente = cteTempDetalheViewModel.CnpjRemetente;
					cteTempDetalhe.ConteinerNotaFiscal = cteTempDetalheViewModel.ConteinerNotaFiscal;
					cteTempDetalhe.DataCadastro = cteTempDetalheViewModel.DataCadastro ?? DateTime.Now;
					cteTempDetalhe.DataNotaFiscal = cteTempDetalheViewModel.DataNotaFiscal;
					cteTempDetalhe.IdEmpresaDestinatario = cteTempDetalheViewModel.IdEmpresaDestinatario;
					cteTempDetalhe.IdEmpresaRemetente = cteTempDetalheViewModel.IdEmpresaRemetente;
					cteTempDetalhe.NumeroNotaFiscal = cteTempDetalheViewModel.NumeroNotaFiscal;
					cteTempDetalhe.IdVagao = cteTempDetalheViewModel.IdVagao;
					cteTempDetalhe.InscricaoEstadualDestinatario = cteTempDetalheViewModel.InscricaoEstadualDestinatario;
					cteTempDetalhe.InscricaoEstadualRemetente = cteTempDetalheViewModel.InscricaoEstadualRemetente;
					cteTempDetalhe.NumeroTif = cteTempDetalheViewModel.NumeroTif;
					cteTempDetalhe.SerieNotaFiscal = cteTempDetalheViewModel.SerieNotaFiscal;
					cteTempDetalhe.SiglaDestinatario = cteTempDetalheViewModel.SiglaDestinatario;
					cteTempDetalhe.SiglaRemetente = cteTempDetalheViewModel.SiglaRemetente;
					cteTempDetalhe.UfDestinatario = cteTempDetalheViewModel.UfDestinatario;
					cteTempDetalhe.UfRemetente = cteTempDetalheViewModel.UfRemetente;
					cteTempDetalhe.VolumeNotaFiscal = cteTempDetalheViewModel.VolumeNotaFiscal;

					_cteTempDetalheRepository.Inserir(cteTempDetalhe);
				}

				_cteTempRepository.GerarCteManutencaoPorTemporarias(usuario);
			}
			catch (Exception ex)
			{
				throw;
			}
		}

		/// <summary>
		/// Retorna todos os Cte Autorizados para listagem de Impressão
		/// </summary>
		/// <param name="dataInicial">Data inicial de pesquisa</param>
		/// <param name="dataFinal">Data final de pesquisa</param>
		/// <param name="serie">Número da série</param>
		/// <param name="numDespacho">Número do Despacho</param>
		/// <param name="chave">chave do cte</param>
		/// <param name="codigoEstacaoOrigem">Estação de Origem</param>
		/// <param name="numeroCte">número do cte</param>
		/// <param name="fluxo">Número do Fluxo</param>
		/// <param name="impresso">opção de já impresso ou não</param>
		/// <returns>lista de Cte</returns>
		public IList<CteDto> RetornaCteImpressao(DateTime dataInicial, DateTime dataFinal, int serie, int numDespacho, string chave, string codigoEstacaoOrigem, int numeroCte, string fluxo, bool impresso)
		{
			DateTime dataIni = new DateTime(dataInicial.Year, dataInicial.Month, dataInicial.Day);

			DateTime dataFim = new DateTime(dataFinal.Year, dataFinal.Month, dataFinal.Day, 23, 59, 59);

			IList<CteDto> listaDto = new List<CteDto>();

			CteDto dto = null;

			DespachoTranslogic despacho = null;
			ItemDespacho itemDespacho = null;
			DetalheCarregamento detalheCarregamento = null;
			Carregamento carregamento = null;
			FluxoComercial fluxoComercial = null;
			EstacaoMae estacaoMaeOrigem = null;

			IList<Cte> listaCte = _cteRepository.Obter(dataIni, dataFim, serie, numDespacho, chave, codigoEstacaoOrigem, numeroCte, fluxo, impresso);

			foreach (Cte cte in listaCte)
			{
				IList<CteArvore> listaArvore = _cteArvoreRepository.ObterArvorePorCteFilho(cte);

				CteArvore cteArvore = listaArvore.OrderByDescending(c => c.Id).FirstOrDefault();

				if (cteArvore == null)
				{
					continue;
				}

				IList<CteAgrupamento> listaCteAgrupamento = _cteAgrupamentoRepository.ObterAgrupamentoPorCteFilho(cteArvore.CteFilho);

				foreach (CteAgrupamento cteAgrupamento in listaCteAgrupamento)
				{
					Cte cteTmp = cteAgrupamento.CteFilho;

					if (ContainCteDto(listaDto.ToList(), typeof(IList<CteDto>), (int)cteTmp.Id))
					{
						continue;
					}

					despacho = cteTmp.Despacho;

					// para ser listado tem que ter despacho e ser autorizado no SEFAZ
					if (despacho != null && cteTmp.SituacaoAtual == SituacaoCteEnum.Autorizado)
					{
						itemDespacho = _itemDespachoRepository.ObterItemDespachoPorDespacho(despacho);

						if (itemDespacho != null)
						{
							detalheCarregamento = itemDespacho.DetalheCarregamento;

							if (detalheCarregamento != null)
							{
								carregamento = detalheCarregamento.Carregamento;

								if (carregamento != null)
								{
									fluxoComercial = carregamento.FluxoComercialCarregamento;
								}
							}
						}

						if (fluxoComercial != null)
						{
							estacaoMaeOrigem = fluxoComercial.Origem;

							dto = new CteDto();
							dto.CteId = cteTmp.Id ?? 0;
							dto.Fluxo = fluxoComercial.Codigo;
							dto.Origem = estacaoMaeOrigem != null ? estacaoMaeOrigem.Codigo : string.Empty;
							dto.Cte = cteTmp.Serie + "-" + cteTmp.Numero;
							dto.Chave = cteTmp.Chave;
							dto.SerieDesp5 = despacho.SerieDespacho != null ? despacho.SerieDespacho.SerieDespachoNum : string.Empty;
							dto.NumDesp5 = despacho.NumeroDespacho ?? 0;
							dto.SerieDesp6 = despacho.SerieDespachoUf != null ? despacho.SerieDespachoUf.NumeroSerieDespacho : 0;
							dto.NumDesp6 = despacho.NumeroDespacho ?? 0;
							dto.DataEmissao = cteTmp.DataHora;
							dto.Impresso = cteTmp.Impresso;

							listaDto.Add(dto);
						}
					}
				}
			}

			return listaDto;
		}

		/// <summary>
		/// Retorna todos os Cte Cancelados ou invalidados para realizar manutenção
		/// </summary>
		/// <param name="pagination"> Paginacao pesquisa</param>
		/// <param name="filtros"> Filtros para pesquisa</param>
		/// <returns>lista de Cte</returns>
		public ResultadoPaginado<ManutencaoCteDto> RetornaListaCteManutencao(DetalhesPaginacaoWeb pagination, DetalhesFiltro<object>[] filtros)
		{
			DateTime dataIni = DateTime.Now;
			DateTime dataFim = DateTime.Now;
			string serie = string.Empty;
			int numDespacho = 0;
			string erro = string.Empty;
			string fluxo = string.Empty;
			string origem = string.Empty;
			string destino = string.Empty;
			string numVagao = string.Empty;
			string chaveCte = string.Empty;
			string codigoUfDcl = string.Empty;

			if (filtros != null)
			{
				foreach (var detalheFiltro in filtros)
				{
					if (detalheFiltro.Campo.Equals("dataInicial") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						string texto = detalheFiltro.Valor[0].ToString();

						dataIni = DateTime.ParseExact(texto, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
					}

					if (detalheFiltro.Campo.Equals("dataFinal") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						string texto = detalheFiltro.Valor[0].ToString();

						dataFim = DateTime.ParseExact(texto, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);

						dataFim = dataFim.AddDays(1).AddSeconds(-1);
					}

					if (detalheFiltro.Campo.Equals("serie") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						serie = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
					}

					if (detalheFiltro.Campo.Equals("despacho") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						numDespacho = Convert.ToInt32(detalheFiltro.Valor[0].ToString());
					}

					if (detalheFiltro.Campo.Equals("UfDcl") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						codigoUfDcl = detalheFiltro.Valor[0].ToString().ToUpperInvariant().Trim();
					}

					if (detalheFiltro.Campo.Equals("fluxo") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						fluxo = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
					}

					if (detalheFiltro.Campo.Equals("Origem") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						origem = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
					}

					if (detalheFiltro.Campo.Equals("Destino") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						destino = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
					}

					if (detalheFiltro.Campo.Equals("chave") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						chaveCte = detalheFiltro.Valor[0].ToString();
					}

					if (detalheFiltro.Campo.Equals("errocte") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						erro = detalheFiltro.Valor[0].ToString();
					}

					if (detalheFiltro.Campo.Equals("Vagao") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						numVagao = detalheFiltro.Valor[0].ToString();
					}
				}
			}

			ResultadoPaginado<ManutencaoCteDto> listaDto = new ResultadoPaginado<ManutencaoCteDto>();

			listaDto = _cteRepository.ObterCteManutencao(pagination, dataIni, dataFim, serie, numDespacho, fluxo, chaveCte, erro, origem, destino, numVagao, codigoUfDcl);

			/*
			DateTime dataIni = new DateTime(dataInicial.Year, dataInicial.Month, dataInicial.Day);

			DateTime dataFim = new DateTime(dataFinal.Year, dataFinal.Month, dataFinal.Day, 23, 59, 59);

			IList<ManutencaoCteDto> listaManutencaoDto = new List<ManutencaoCteDto>();

			ManutencaoCteDto manutencaoDto = null;

			DespachoTranslogic despacho = null;
			FluxoComercial fluxoComercial = null;
			Mercadoria mercadoria = null;
			EstacaoMae estacaoMaeOrigem = null;
			EstacaoMae estacaoMaeDestino = null;
			SerieDespacho serieDespacho = null;
			SerieDespachoUf serieDespachoUf = null;
			Vagao vagao = null;
			*/

			/*// Lista os Ctes Cancelados ou os Invalidados
			SituacaoCteEnum situacaoCte = SituacaoCteEnum.Cancelado;
			if (status == StatusCteEnum.Inativo)
			{
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			situacaoCte = SituacaoCteEnum.Invalidado;
			}*/
			/*

			IList<Cte> listaCte = _cteRepository.Obter(dataIni, dataFim, serie, numDespacho, codVagao, chave, serieCte, nroCte);

			foreach (Cte cte in listaCte)
			{
																																																																																																																																																																																																																																																																			bool cteComAgrupamentoNaoCancelado = false;

																																																																																																																																																																																																																																																																			IList<CteArvore> listaArvore = _cteArvoreRepository.ObterArvorePorCteFilho(cte);

																																																																																																																																																																																																																																																																			CteArvore cteArvore = listaArvore.OrderByDescending(c => c.Id).FirstOrDefault();

																																																																																																																																																																																																																																																																			if (cteArvore == null)
																																																																																																																																																																																																																																																																			{
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			continue;
																																																																																																																																																																																																																																																																			}

																																																																																																																																																																																																																																																																			IList<CteAgrupamento> listaCteAgrupamento = _cteAgrupamentoRepository.ObterAgrupamentoPorCteFilho(cteArvore.CteFilho);

																																																																																																																																																																																																																																																																			foreach (CteAgrupamento cteAgrupamento in listaCteAgrupamento)
																																																																																																																																																																																																																																																																			{
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			if (cteAgrupamento.CteFilho.SituacaoAtual != SituacaoCteEnum.Cancelado)
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			{
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			cteComAgrupamentoNaoCancelado = true;
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			}
																																																																																																																																																																																																																																																																			}

																																																																																																																																																																																																																																																																			Cte cteRaiz = _cteAgrupamentoRepository.ObterCteRaizPorCteFilho(cteArvore.CteFilho);

																																																																																																																																																																																																																																																																			if (ContainCteDto(listaManutencaoDto.ToList(), typeof(IList<ManutencaoCteDto>), (int)cteRaiz.Id))
																																																																																																																																																																																																																																																																			{
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			continue;
																																																																																																																																																																																																																																																																			}

																																																																																																																																																																																																																																																																			despacho = cteRaiz.Despacho;
																																																																																																																																																																																																																																																																			vagao = cteRaiz.Vagao;

																																																																																																																																																																																																																																																																			if (despacho != null)
																																																																																																																																																																																																																																																																			{
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			serieDespacho = despacho.SerieDespacho;
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			serieDespachoUf = despacho.SerieDespachoUf;

																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			fluxoComercial = cteRaiz.FluxoComercial;

																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			if (fluxoComercial != null)
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			{
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			mercadoria = fluxoComercial.Mercadoria;
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			estacaoMaeOrigem = fluxoComercial.Origem;
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			estacaoMaeDestino = fluxoComercial.Destino;

																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			manutencaoDto = new ManutencaoCteDto();
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			manutencaoDto.IdDespacho = despacho.Id.Value;
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			manutencaoDto.CteId = cteRaiz.Id ?? 0;
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			manutencaoDto.SerieDesp5 = serieDespacho != null ? Convert.ToInt32(serieDespacho.SerieDespachoNum) : 0;
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			manutencaoDto.NumDesp5 = despacho.NumeroDespacho ?? 0;
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			manutencaoDto.SerieDesp6 = serieDespachoUf != null ? serieDespachoUf.NumeroSerieDespacho : 0;
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			manutencaoDto.NumDesp6 = despacho.NumeroDespacho ?? 0;
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			manutencaoDto.VagaoId = vagao.Id.HasValue ? vagao.Id.Value : 0;
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			manutencaoDto.CodVagao = vagao.Codigo;
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			manutencaoDto.Fluxo = fluxoComercial.Codigo;
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			manutencaoDto.CodigoOrigem = estacaoMaeOrigem != null ? estacaoMaeOrigem.Codigo : string.Empty;
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			manutencaoDto.CodigoDestino = estacaoMaeDestino != null ? estacaoMaeDestino.Codigo : string.Empty;
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			manutencaoDto.CodigoMercadoria = mercadoria != null ? mercadoria.Codigo : string.Empty;
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			manutencaoDto.ToneladaUtil = cteRaiz.PesoVagao ?? 0;
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			manutencaoDto.Volume = cteRaiz.VolumeVagao;
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			manutencaoDto.NroCte = cteRaiz.Numero;
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			manutencaoDto.SerieCte = cteRaiz.Serie;
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			manutencaoDto.ChaveCte = cteRaiz.Chave;
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			manutencaoDto.DataEmissao = cteRaiz.DataHora.ToString("dd/MM/yyyy");
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			// manutencaoDto.Status = cte.SituacaoAtual == SituacaoCteEnum.Invalidado ? StatusCteEnum.Inativo : StatusCteEnum.Ativo;
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			manutencaoDto.CteComAgrupamentoNaoCancelado = cteComAgrupamentoNaoCancelado;

																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			listaManutencaoDto.Add(manutencaoDto);
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			}
																																																																																																																																																																																																																																																																			}
			}

			return listaManutencaoDto;
			 */
			return listaDto;
		}

		/// <summary>
		/// Retorna o Cte Selecionado na tela de manutenção
		/// </summary>
		/// <param name="idCte">id do Cte Selecionado</param>
		/// <returns>Cte selecionado</returns>
		public ManutencaoCteDto RetornaCteManutencao(int idCte)
		{
			ManutencaoCteDto manutencaoDto = new ManutencaoCteDto();

			DespachoTranslogic despacho = null;
			FluxoComercial fluxoComercial = null;
			SerieDespacho serieDespacho = null;
			SerieDespachoUf serieDespachoUf = null;
			Vagao vagao = null;

			Cte cte = _cteRepository.ObterPorIdCte(idCte);

			despacho = cte.Despacho;
			vagao = cte.Vagao;

			if (despacho != null)
			{
				serieDespacho = despacho.SerieDespacho;
				serieDespachoUf = despacho.SerieDespachoUf;

				fluxoComercial = cte.FluxoComercial;

				if (fluxoComercial != null)
				{
					manutencaoDto.CteId = cte.Id ?? 0;
					manutencaoDto.IdDespacho = despacho.Id.Value;
					manutencaoDto.SerieDesp5 = despacho.SerieDespachoSdi ?? (serieDespacho != null ? serieDespacho.SerieDespachoNum : string.Empty);
					manutencaoDto.NumDesp5 = despacho.NumDespIntercambio ?? (despacho.NumeroDespacho ?? 0);
					manutencaoDto.SerieDesp6 = serieDespachoUf != null ? serieDespachoUf.NumeroSerieDespacho : 0;
					manutencaoDto.NumDesp6 = despacho.NumeroDespacho ?? 0;
					manutencaoDto.VagaoId = vagao.Id.HasValue ? vagao.Id.Value : 0;
					manutencaoDto.CodVagao = vagao.Codigo;
					manutencaoDto.Fluxo = fluxoComercial.Codigo;
					manutencaoDto.ToneladaUtil = cte.PesoVagao ?? 0.0;
					manutencaoDto.Volume = cte.VolumeVagao;
					manutencaoDto.NroCte = cte.Numero;
					manutencaoDto.SerieCte = cte.Serie;
					manutencaoDto.DataEmissao = cte.DataHora;
					// manutencaoDto.Status = cte.SituacaoAtual == SituacaoCteEnum.Invalidado ? StatusCteEnum.Inativo : StatusCteEnum.Ativo;
				}
			}

			return manutencaoDto;
		}

		/// <summary>
		/// Realiza a Impressão dos Ctes
		/// </summary>
		/// <param name="ids">ids dos ctes</param>
		/// <returns>resultado da Impressão</returns>
		public bool ImprimirCtes(List<int> ids)
		{
			try
			{
				Cte cte = null;

				foreach (int id in ids)
				{
					cte = _cteRepository.ObterPorId(id);
					cte.Impresso = true;
					_cteRepository.Atualizar(cte);
				}
			}
			catch (Exception)
			{
				return false;
			}

			return true;
		}

		/// <summary>
		/// Obtem Detalhe do Cte (nota) por id do Cte
		/// </summary>
		/// <param name="cteId">id do Cte Selecionado</param>
		/// <returns>Retorna lista de Detalhe Cte</returns>
		public IList<CteDetalhe> ObterCteDetalhePorIdCte(int cteId)
		{
			IList<CteDetalhe> lista = new List<CteDetalhe>();

			if (cteId != 0)
			{
				// Cte cte = _cteRepository.ObterPorId(cteId);
				Cte cte = new Cte { Id = cteId };
				lista = _cteDetalheRepository.ObterPorCte(cte);
			}

			return lista;
		}

		/// <summary>
		/// Obtem Detalhe do Cte (nota) por id do Cte
		/// </summary>
		/// <param name="arrayCtes">id do Cte Selecionado</param>
		/// <returns>Retorna lista de Detalhe Cte</returns>
		public IList<Cte> ObterCteImpressao(int[] arrayCtes)
		{
			return _cteRepository.ObterTodosPorId(arrayCtes);
		}

		/// <summary>
		/// Insere os ctes da lista no pool de processamento do servidor
		/// </summary>
		/// <param name="listaCtesNaoProcessados">Lista de ctes</param>
		/// <param name="hostName">Ip do Servidor</param>
		public void InserirPoolingEnvioCte(IList<Cte> listaCtesNaoProcessados, string hostName)
		{
			foreach (Cte cteAux in listaCtesNaoProcessados)
			{
				Cte cte = cteAux;
				try
				{
					CteEnvioPooling cep = new CteEnvioPooling();
					cep.Id = cte.Id;
					cep.DataHora = DateTime.Now;
					cep.HostName = hostName;

					Usuario usuario = ObterUsuarioRobo();

					// Insere o cte no pooling
					_cteEnvioPoolingRepository.Inserir(cep);

					switch (cte.SituacaoAtual)
					{
						case SituacaoCteEnum.PendenteArquivoEnvio:

							// Caso seja um cte complementar
							if (cte.Complementar)
							{
								cte.SituacaoAtual = SituacaoCteEnum.EnviadoFilaArquivoComplemento;
								// Insere no log do cte
								_cteLogService.InserirLogInfo(cte, "CteService", "Adicionado o arquivo de complemento no pooling");
							}
							else
							{
								cte.SituacaoAtual = SituacaoCteEnum.EnviadoFilaArquivoEnvio;
								// Insere no log do cte
								_cteLogService.InserirLogInfo(cte, "CteService", "Adicionado o arquivo de envio no pooling");
							}

							// Insere o status do cte
							_cteStatusRepository.InserirCteStatus(cte, usuario, new CteStatusRetorno { Id = 21 });
							break;
						case SituacaoCteEnum.PendenteArquivoCancelamento:
						case SituacaoCteEnum.AguardandoCancelamento:

							cte.SituacaoAtual = SituacaoCteEnum.EnviadoFilaArquivoCancelamento;
							// Insere o status do cte
							_cteStatusRepository.InserirCteStatus(cte, usuario, new CteStatusRetorno { Id = 21 });

							// Insere no log do cte
							_cteLogService.InserirLogInfo(cte, "CteService", "Adicionado o arquivo de cancelamento no pooling");
							break;
						case SituacaoCteEnum.PendenteArquivoInutilizacao:
						case SituacaoCteEnum.AguardandoInutilizacao:
							cte.SituacaoAtual = SituacaoCteEnum.EnviadoFilaArquivoInutilizacao;
							// Insere o status do cte
							_cteStatusRepository.InserirCteStatus(cte, usuario, new CteStatusRetorno { Id = 21 });

							// Insere no log do cte
							_cteLogService.InserirLogInfo(cte, "CteService", "Adicionado o arquivo de inutilização no pooling");
							break;
					}

					// Altera o status do CTE
					// _cteRepository.Atualizar(cte);
					_cteRepository.AtualizarSituacao(cte);
				}
				catch (Exception ex)
				{
					// Insere no log do cte
					_cteLogService.InserirLogErro(cte, "CteService", string.Format("InserirPoolingEnvioCte: erro para inserir no pooling - {0}", ex.Message), ex);

					// Limpa a sessão para processar o próximo
					_cteEnvioPoolingRepository.ClearSession();
				}
			}
		}

		/// <summary>
		/// Obtém a Lista de Ctes serem processados para envio
		/// </summary>
		/// <param name="indAmbienteSefaz">Indica qual ambiente do SEFAZ</param>
		/// <returns>Lista de ctes</returns> 
		public IList<Cte> ObterCtesProcessarEnvio(int indAmbienteSefaz)
		{
			try
			{
				return _cteRepository.ObterStatusProcessamentoEnvio(indAmbienteSefaz);
			}
			catch (Exception ex)
			{
				_cteLogService.InserirLogErro(null, "CteService", string.Format("ObterCtesProcessarEnvio: {0}", ex.Message), ex);
				throw ex;
			}
		}

		/// <summary>
		/// Insere os dados do arquivo de envio na tabela de NDD
		/// </summary>
		/// <param name="cte">Cte a ser inerido</param>
		/// <param name="arquivo">Dados do arquivo da NDD</param>
		public void InserirArquivoEnvioNdd(Cte cte, StringBuilder arquivo)
		{
			// Insere na tabela da NDD
			_databaseInputRepository.Inserir(cte.Numero, null, arquivo);

			// Altera o status do CTE
			cte.SituacaoAtual = SituacaoCteEnum.EnviadoArquivoEnvio;
			_cteRepository.Atualizar(cte);

			Usuario usuario = ObterUsuarioRobo();

			// Insere o status do cte
			_cteStatusRepository.InserirCteStatus(cte, usuario, new CteStatusRetorno { Id = 11 });

			// Insere no log do cte
			_cteLogService.InserirLogInfo(cte, "CteService", "Arquivo enviado para a NDD");
		}

		/// <summary>
		/// Insere o arquivo de Cte na fila de processamento da config
		/// </summary>
		/// <param name="cte">Cte a ser inserido na fila</param>
		public void InserirConfigCteEnvio(Cte cte)
		{
			try
			{
				int idListaConfig = 0;
				// Insere na fila de processamento da config
				idListaConfig = _filaProcessamentoService.InserirCteEnvioFilaProcessamento(cte);

				// Insere na fila a geração do Pdf
				_filaProcessamentoService.InserirGeracaArquivosCteNoServidorFtp(cte);

				// Gera o PDF pela impressão
				_filaProcessamentoService.InserirGeracaoPdfPorImpressao(cte);

				// Insere na fila a geração do xml
				_filaProcessamentoService.InserirGeracaoArquivosCteNoServidorEmail(cte);

				// Insere no pooling de recebimento (insere na tabela informando que foi colocado na fila o cte)
				InserirInterfaceRecebimentoConfig(cte, idListaConfig);

				// Altera o status do CTE
				cte.SituacaoAtual = SituacaoCteEnum.EnviadoArquivoEnvio;
				_cteRepository.Atualizar(cte);

				// Pega o usuario default da conf geral
				Usuario usuario = ObterUsuarioRobo();

				// Insere o status do cte
				_cteStatusRepository.InserirCteStatus(cte, usuario, new CteStatusRetorno { Id = 11 });

				// Insere no log do cte
				_cteLogService.InserirLogInfo(cte, "CteService", "Cte enviado para a Config");
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		/// <summary>
		/// Insere o Cte de complemento na fila de processamento da config
		/// </summary>
		/// <param name="cte">Cte de complemento</param>
		public void InserirConfigCteComplemento(Cte cte)
		{
			int idListaConfig = 0;
			// Insere na fila de processamento da config
			idListaConfig = _filaProcessamentoService.InserirCteComplementoFilaProcessamento(cte);

			// Insere na fila a geração do Pdf
			_filaProcessamentoService.InserirGeracaArquivosCteNoServidorFtp(cte);

			// Gera o PDF pela impressão
			_filaProcessamentoService.InserirGeracaoPdfPorImpressao(cte);

			// Insere na fila a geração do xml
			_filaProcessamentoService.InserirGeracaoArquivosCteNoServidorEmail(cte);

			// Insere no pooling de recebimento (insere na tabela informando que foi colocado na fila o cte)
			InserirInterfaceRecebimentoConfig(cte, idListaConfig);

			// Altera o status do CTE
			cte.SituacaoAtual = SituacaoCteEnum.EnviadoArquivoComplemento;
			_cteRepository.Atualizar(cte);

			Usuario usuario = ObterUsuarioRobo();

			// Insere o status do cte
			_cteStatusRepository.InserirCteStatus(cte, usuario, new CteStatusRetorno { Id = 11 });

			// Insere no log do cte
			_cteLogService.InserirLogInfo(cte, "CteService", "Cte de complemento enviado para a Config");
		}

		/// <summary>
		/// Insere os dados do arquivo de cancelamento na tabela de NDD
		/// </summary>
		/// <param name="cte">Cte a ser inserido</param>
		/// <param name="arquivo">Dados do arquivo da NDD</param>
		public void InserirArquivoCancelamentoNdd(Cte cte, StringBuilder arquivo)
		{
			// Insere na tabela da NDD
			_databaseInputRepository.Inserir(cte.Numero, null, arquivo);

			// Altera o status do CTE
			cte.SituacaoAtual = SituacaoCteEnum.EnviadoArquivoCancelamento;
			_cteRepository.Atualizar(cte);

			Usuario usuario = ObterUsuarioRobo();

			// Insere o status do cte
			_cteStatusRepository.InserirCteStatus(cte, usuario, new CteStatusRetorno { Id = 24 });

			// Insere no log do cte
			_cteLogService.InserirLogInfo(cte, "CteService", "Arquivo de cancelamento enviado para a NDD");
		}

		/// <summary>
		/// Insere o Cte de cancelamento na fila de processamento da config
		/// </summary>
		/// <param name="cte">Cte a ser inserido</param>
		public void InserirConfigCteCancelamento(Cte cte)
		{
			try
			{
				int idListaConfig = 0;
				// Insere na fila de processamento da config
				idListaConfig = _filaProcessamentoService.InserirCteCancelamentoFilaProcessamento(cte);

				// Insere no pooling de recebimento (insere na tabela informando que foi colocado na fila o cte)
				InserirInterfaceRecebimentoConfig(cte, idListaConfig);

				_filaProcessamentoService.InserirGeracaoArquivosCteNoServidorEmailCancelamento(cte);

				// Altera o status do CTE
				cte.SituacaoAtual = SituacaoCteEnum.EnviadoArquivoCancelamento;
				// _cteRepository.Atualizar(cte);
				_cteRepository.AtualizarSituacao(cte);

				Usuario usuario = ObterUsuarioRobo();

				// Insere o status do cte
				_cteStatusRepository.InserirCteStatus(cte, usuario, new CteStatusRetorno { Id = 24 });

				// Insere no log do cte
				_cteLogService.InserirLogInfo(cte, "CteService", "Cte de cancelamento enviado para a Config");
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		/// <summary>
		/// Insere os dados do arquivo de inutilização na tabela de NDD
		/// </summary>
		/// <param name="cte">Cte a ser inerido</param>
		/// <param name="arquivo">Dados do arquivo da NDD</param>
		public void InserirArquivoInutilizacaoNdd(Cte cte, StringBuilder arquivo)
		{
			// Insere na tabela da NDD
			_databaseInputRepository.Inserir(cte.Numero, null, arquivo);

			// Altera o status do CTE
			cte.SituacaoAtual = SituacaoCteEnum.EnviadoArquivoInutilização;
			_cteRepository.Atualizar(cte);

			Usuario usuario = ObterUsuarioRobo();

			// Insere o status do cte
			_cteStatusRepository.InserirCteStatus(cte, usuario, new CteStatusRetorno { Id = 25 });

			// Insere no log do cte
			_cteLogService.InserirLogInfo(cte, "CteService", "Arquivo de inutilização enviado para a NDD");
		}

		/// <summary>
		/// Insere o Cte de inutilização na fila de processamento da config
		/// </summary>
		/// <param name="cte">Cte a ser inutilizado</param>
		public void InserirConfigCteInutilizacao(Cte cte)
		{
			int idListaConfig = 0;
			// Insere na fila de processamento da config
			idListaConfig = _filaProcessamentoService.InserirCteInutilizacaoFilaProcessamento(cte);

			// Insere no pooling de recebimento (insere na tabela informando que foi colocado na fila o cte)
			InserirInterfaceRecebimentoConfig(cte, idListaConfig);

			// Altera o status do CTE
			cte.SituacaoAtual = SituacaoCteEnum.EnviadoArquivoInutilização;
			_cteRepository.Atualizar(cte);

			Usuario usuario = ObterUsuarioRobo();

			// Insere o status do cte
			_cteStatusRepository.InserirCteStatus(cte, usuario, new CteStatusRetorno { Id = 25 });

			// Insere no log do cte
			_cteLogService.InserirLogInfo(cte, "CteService", "Cte de inutilização enviado para a Config");
		}

		/// <summary>
		/// Obtém os ctes do pool pelo ip do servStatusProcessamentoEnvio
		/// </summary>
		/// <param name="hostName">Nome do host</param>
		/// <returns>Lista de Ctes</returns>
		public IList<Cte> ObterPoolingEnvioPorServidor(string hostName)
		{
			try
			{
				return _cteRepository.ObterListaEnvioPorHost(hostName);
			}
			catch (Exception ex)
			{
				_cteLogService.InserirLogErro(null, "CteService", string.Format("ObterPoolingEnvioPorServidor({0}): {1}", hostName, ex.Message), ex);
				throw ex;
			}
		}

		/// <summary>
		/// Obtém os ctes complementado do pool pelo ip do servStatusProcessamentoEnvio
		/// </summary>
		/// <param name="hostName">Nome do host</param>
		/// <returns>Lista de Ctes</returns>
		public IList<Cte> ObterPoolingEnvioComplementadoPorServidor(string hostName)
		{
			try
			{
				return _cteRepository.ObterListaEnvioComplementarPorHost(hostName);
			}
			catch (Exception ex)
			{
				_cteLogService.InserirLogErro(null, "CteService", string.Format("ObterPoolingEnvioComplementadoPorServidor({0}): {1}", hostName, ex.Message), ex);
				throw ex;
			}
		}

		/// <summary>
		/// Remove os ctes do pool
		/// </summary>
		/// <param name="lista">Lista de ctes processados</param>
		public void RemoverPoolingEnvioCte(IList<Cte> lista)
		{
			_cteEnvioPoolingRepository.RemoverPorLista(lista);
		}

		/// <summary>
		/// Retorna todos os Cte para monitoramento
		/// </summary>
		/// <param name="pagination">Detalhes da Paginação</param>
		/// <param name="dataInicial">Data inicial de pesquisa</param>
		/// <param name="dataFinal">Data final de pesquisa</param>
		/// <param name="serie">Número da série</param>
		/// <param name="despacho">Número do Despacho</param>
		/// <param name="codFluxo">Código do fluxo comercial</param>
		/// <param name="chaveCte">Chave do Cte informada</param>
		/// <param name="erro">Status de erro Cte</param>
		/// <param name="origem"> Origem do Cte</param>
		/// <param name="destino"> Destino do Cte</param>
		/// <param name="numVagao"> Numero do Vagao</param>b
		/// <param name="impresso"> Indicador se cte impresso</param>
		/// <param name="codigoUfDcl">Codigo controle do despacho</param>
		/// <returns>lista de Cte</returns>
		public ResultadoPaginado<CteDto> RetornaCteMonitoramento(DetalhesPaginacao pagination, DateTime dataInicial, DateTime dataFinal, string serie, int despacho, string codFluxo, string chaveCte, string erro, string origem, string destino, string numVagao, bool? impresso, string codigoUfDcl)
		{
			ResultadoPaginado<CteDto> listaDto = new ResultadoPaginado<CteDto>();

			listaDto = _cteRepository.ObterCteMonitoramento(pagination, dataInicial, dataFinal, serie, despacho, codFluxo, chaveCte, erro, origem, destino, numVagao, impresso, codigoUfDcl);
			listaDto.Items = listaDto.Items.OrderByDescending(c => c.Numero).ToList();

			return listaDto;
		}

		/// <summary>
		/// Retorna todos os Cte para monitoramento
		/// </summary>
		/// <param name="pagination">Detalhes da Paginação</param>
		/// <param name="dataInicial">Data inicial de pesquisa</param>
		/// <param name="dataFinal">Data final de pesquisa</param>
		/// <param name="codFluxo">Código do fluxo comercial</param>
		/// <param name="origem"> Origem do Cte</param>
		/// <param name="destino"> Destino do Cte</param>
		/// <returns>lista de Cte</returns>
		public ResultadoPaginado<CteDto> RetornaCteRelatorioErro(DetalhesPaginacao pagination, DateTime dataInicial, DateTime dataFinal, string codFluxo, string origem, string destino)
		{
			ResultadoPaginado<CteDto> listaDto = new ResultadoPaginado<CteDto>();

			listaDto = _cteRepository.ObterCteRelatorioErro(pagination, dataInicial, dataFinal, codFluxo, origem, destino);
			listaDto.Items = listaDto.Items.OrderByDescending(c => c.Numero).ToList();

			return listaDto;
		}

		/// <summary>
		/// Retorna todos os Cte Autorizados para listagem de Impressão
		/// </summary>
		/// <param name="pagination"> Paginacao pesquisa</param>
		/// <param name="filtros"> Filtros para pesquisa</param>
		/// <returns>lista de Cte</returns>
		public ResultadoPaginado<CteDto> RetornaCteMonitoramento(DetalhesPaginacaoWeb pagination, DetalhesFiltro<object>[] filtros)
		{
			DateTime dataIni = DateTime.Now;
			DateTime dataFim = DateTime.Now;
			string serie = string.Empty;
			int numDespacho = 0;
			string numVagao = string.Empty;
			string erro = string.Empty;
			string fluxo = string.Empty;
			string origem = string.Empty;
			string destino = string.Empty;
			string chaveCte = string.Empty;
			string codigoUfDcl = string.Empty;
			bool? impresso = null;

			if (filtros != null)
			{
				foreach (var detalheFiltro in filtros)
				{
					if (detalheFiltro.Campo.Equals("dataInicial") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						string texto = detalheFiltro.Valor[0].ToString();

						dataIni = DateTime.ParseExact(texto, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
					}

					if (detalheFiltro.Campo.Equals("dataFinal") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						string texto = detalheFiltro.Valor[0].ToString();

						dataFim = DateTime.ParseExact(texto, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);

						dataFim = dataFim.AddDays(1).AddSeconds(-1);
					}

					if (detalheFiltro.Campo.Equals("serie") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						serie = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
					}

					if (detalheFiltro.Campo.Equals("despacho") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						numDespacho = Convert.ToInt32(detalheFiltro.Valor[0].ToString());
					}

					if (detalheFiltro.Campo.Equals("fluxo") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						fluxo = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
					}

					if (detalheFiltro.Campo.Equals("Origem") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						origem = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
					}

					if (detalheFiltro.Campo.Equals("UfDcl") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						codigoUfDcl = detalheFiltro.Valor[0].ToString().ToUpperInvariant().Trim();
					}

					if (detalheFiltro.Campo.Equals("Destino") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						destino = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
					}

					if (detalheFiltro.Campo.Equals("chave") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						chaveCte = detalheFiltro.Valor[0].ToString();
					}

					if (detalheFiltro.Campo.Equals("errocte") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						erro = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
					}

					if (detalheFiltro.Campo.Equals("Vagao") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						numVagao = detalheFiltro.Valor[0].ToString();
					}

					if (detalheFiltro.Campo.Equals("Impresso") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						string value = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
						if (value.Equals("T"))
						{
							impresso = null;
						}
						else if (value.Equals("S"))
						{
							impresso = true;
						}
						else
						{
							impresso = false;
						}
					}
				}
			}

			ResultadoPaginado<CteDto> listaDto = new ResultadoPaginado<CteDto>();

			listaDto = _cteRepository.ObterCteMonitoramento(pagination, dataIni, dataFim, serie, numDespacho, fluxo, chaveCte, erro, origem, destino, numVagao, impresso, codigoUfDcl);
			listaDto.Items = listaDto.Items.OrderByDescending(c => c.Numero).ToList();

			return listaDto;
		}

		/// <summary>
		/// Retorna todos os Cte Autorizados para listagem de Impressão
		/// </summary>
		/// <param name="pagination"> Paginacao pesquisa</param>
		/// <param name="filtros"> Filtros para pesquisa</param>
		/// <returns>lista de Cte</returns>
		public ResultadoPaginado<CteDto> RetornaCteManutencaoPendente(DetalhesPaginacaoWeb pagination, DetalhesFiltro<object>[] filtros)
		{
			DateTime dataIni = DateTime.Now;
			DateTime dataFim = DateTime.Now;
			string serie = string.Empty;
			int numDespacho = 0;
			string numVagao = string.Empty;
			string erro = string.Empty;
			string fluxo = string.Empty;
			string origem = string.Empty;
			string destino = string.Empty;
			string chaveCte = string.Empty;
			string codigoUfDcl = string.Empty;
			bool? impresso = null;

			if (filtros != null)
			{
				foreach (var detalheFiltro in filtros)
				{
					if (detalheFiltro.Campo.Equals("dataInicial") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						string texto = detalheFiltro.Valor[0].ToString();

						dataIni = DateTime.ParseExact(texto, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
					}

					if (detalheFiltro.Campo.Equals("dataFinal") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						string texto = detalheFiltro.Valor[0].ToString();

						dataFim = DateTime.ParseExact(texto, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);

						dataFim = dataFim.AddDays(1).AddSeconds(-1);
					}

					if (detalheFiltro.Campo.Equals("serie") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						serie = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
					}

					if (detalheFiltro.Campo.Equals("despacho") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						numDespacho = Convert.ToInt32(detalheFiltro.Valor[0].ToString());
					}

					if (detalheFiltro.Campo.Equals("fluxo") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						fluxo = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
					}

					if (detalheFiltro.Campo.Equals("Origem") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						origem = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
					}

					if (detalheFiltro.Campo.Equals("UfDcl") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						codigoUfDcl = detalheFiltro.Valor[0].ToString().ToUpperInvariant().Trim();
					}

					if (detalheFiltro.Campo.Equals("Destino") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						destino = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
					}

					if (detalheFiltro.Campo.Equals("chave") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						chaveCte = detalheFiltro.Valor[0].ToString();
					}

					if (detalheFiltro.Campo.Equals("Vagao") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						numVagao = detalheFiltro.Valor[0].ToString();
					}
				}
			}

			ResultadoPaginado<CteDto> listaDto = new ResultadoPaginado<CteDto>();

			listaDto = _cteRepository.ObterCteManutencaoPendente(pagination, dataIni, dataFim, serie, numDespacho, fluxo, chaveCte, origem, destino, numVagao, codigoUfDcl);
			listaDto.Items = listaDto.Items.OrderByDescending(c => c.Numero).ToList();

			return listaDto;
		}

		/// <summary>
		/// Retorna todos os Cte para monitoramento
		/// </summary>
		/// <param name="pagination">Detalhes da Paginação</param>
		/// <param name="dataInicial">Data inicial de pesquisa</param>
		/// <param name="dataFinal">Data final de pesquisa</param>
		/// <param name="serie">Número da série</param>
		/// <param name="despacho">Número do Despacho</param>
		/// <param name="codFluxo">Código do fluxo comercial</param>
		/// <param name="chaveCte">Chave do Cte informada</param>
		/// <param name="origem"> Origem do Cte</param>
		/// <param name="destino"> Destino do Cte</param>
		/// <param name="numVagao"> Numero do Vagao</param>b
		/// <param name="codigoUfDcl">Codigo controle do despacho</param>
		/// <returns>lista de Cte</returns>
		public ResultadoPaginado<CteDto> RetornaCteManutencaoPendente(DetalhesPaginacao pagination, DateTime dataInicial, DateTime dataFinal, string serie, int despacho, string codFluxo, string chaveCte, string origem, string destino, string numVagao, string codigoUfDcl)
		{
			ResultadoPaginado<CteDto> listaDto = new ResultadoPaginado<CteDto>();

			listaDto = _cteRepository.ObterCteManutencaoPendente(pagination, dataInicial, dataFinal, serie, despacho, codFluxo, chaveCte, origem, destino, numVagao, codigoUfDcl);
			listaDto.Items = listaDto.Items.OrderByDescending(c => c.Numero).ToList();

			return listaDto;
		}

		/// <summary>
		/// Retorna todos os Cte Autorizados para listagem de Impressão
		/// </summary>
		/// <param name="pagination"> Paginacao pesquisa</param>
		/// <param name="filtros"> Filtros para pesquisa</param>
		/// <returns>lista de Cte</returns>
		public ResultadoPaginado<CteDto> RetornaCteRelatorioErro(DetalhesPaginacaoWeb pagination, DetalhesFiltro<object>[] filtros)
		{
			DateTime dataIni = DateTime.Now;
			DateTime dataFim = DateTime.Now;
			string fluxo = string.Empty;
			string origem = string.Empty;
			string destino = string.Empty;

			if (filtros != null)
			{
				foreach (var detalheFiltro in filtros)
				{
					if (detalheFiltro.Campo.Equals("dataInicial") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						string texto = detalheFiltro.Valor[0].ToString();

						dataIni = DateTime.ParseExact(texto, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
					}

					if (detalheFiltro.Campo.Equals("dataFinal") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						string texto = detalheFiltro.Valor[0].ToString();

						dataFim = DateTime.ParseExact(texto, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);

						dataFim = dataFim.AddDays(1).AddSeconds(-1);
					}

					if (detalheFiltro.Campo.Equals("fluxo") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						fluxo = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
					}

					if (detalheFiltro.Campo.Equals("Origem") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						origem = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
					}

					if (detalheFiltro.Campo.Equals("Destino") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						destino = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
					}
				}
			}

			ResultadoPaginado<CteDto> listaDto = new ResultadoPaginado<CteDto>();

			listaDto = _cteRepository.ObterCteRelatorioErro(pagination, dataIni, dataFim, fluxo, origem, destino);
			listaDto.Items = listaDto.Items.OrderByDescending(c => c.Numero).ToList();

			return listaDto;
		}

		/// <summary>
		/// Retorna todos os Cte Autorizados para listagem de Impressão
		/// </summary>
		/// <param name="pagination"> Paginacao pesquisa</param>
		/// <param name="filtros"> Filtros para pesquisa</param>
		/// <returns>lista de Cte</returns>
		public ResultadoPaginado<CteDto> RetornaCteGerarArquivoLote(DetalhesPaginacaoWeb pagination, DetalhesFiltro<object>[] filtros)
		{
			DateTime dataIni = DateTime.Now;
			DateTime dataFim = DateTime.Now;
			string serie = string.Empty;
			int numDespacho = 0;
			string numVagao = string.Empty;
			string fluxo = string.Empty;
			string origem = string.Empty;
			string destino = string.Empty;
			string[] chaveCte = null;
			string codigoUfDcl = string.Empty;

			if (filtros != null)
			{
				foreach (var detalheFiltro in filtros)
				{
					if (detalheFiltro.Campo.Equals("dataInicial") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						string texto = detalheFiltro.Valor[0].ToString();

						dataIni = DateTime.ParseExact(texto, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
					}

					if (detalheFiltro.Campo.Equals("dataFinal") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						string texto = detalheFiltro.Valor[0].ToString();

						dataFim = DateTime.ParseExact(texto, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);

						dataFim = dataFim.AddDays(1).AddSeconds(-1);
					}

					if (detalheFiltro.Campo.Equals("serie") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						serie = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
					}

					if (detalheFiltro.Campo.Equals("despacho") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						numDespacho = Convert.ToInt32(detalheFiltro.Valor[0].ToString());
					}

					if (detalheFiltro.Campo.Equals("fluxo") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						fluxo = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
					}

					if (detalheFiltro.Campo.Equals("Origem") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						origem = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
					}

					if (detalheFiltro.Campo.Equals("UfDcl") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						codigoUfDcl = detalheFiltro.Valor[0].ToString().ToUpperInvariant().Trim();
					}

					if (detalheFiltro.Campo.Equals("Destino") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						destino = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
					}

					if (detalheFiltro.Campo.Equals("chave") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						chaveCte = detalheFiltro.Valor[0].ToString().Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
					}

					if (detalheFiltro.Campo.Equals("Vagao") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						numVagao = detalheFiltro.Valor[0].ToString();
					}
				}
			}

			ResultadoPaginado<CteDto> listaDto = new ResultadoPaginado<CteDto>();

			listaDto = _cteRepository.ObterCteGerarArquivoLote(pagination, dataIni, dataFim, serie, numDespacho, fluxo, chaveCte, origem, destino, numVagao, codigoUfDcl);

			return listaDto;
		}

		/// <summary>
		/// Retorna todos os Cte Autorizados para listagem de Impressão
		/// </summary>
		/// <param name="pagination"> Paginacao pesquisa</param>
		/// <param name="filtros"> Filtros para pesquisa</param>
		/// <returns>lista de Cte</returns>
		public ResultadoPaginado<CteDto> RetornaCteImpressao(DetalhesPaginacaoWeb pagination, DetalhesFiltro<object>[] filtros)
		{
			DateTime dataIni = DateTime.Now;
			DateTime dataFim = DateTime.Now;
			int serie = 0;
			int numDespacho = 0;
			string numVagao = string.Empty;
			string erro = string.Empty;
			string fluxo = string.Empty;
			string origem = string.Empty;
			string destino = string.Empty;
			string chaveCte = string.Empty;

			if (filtros != null)
			{
				foreach (var detalheFiltro in filtros)
				{
					if (detalheFiltro.Campo.Equals("dataInicial") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						string texto = detalheFiltro.Valor[0].ToString();

						dataIni = DateTime.ParseExact(texto, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
					}

					if (detalheFiltro.Campo.Equals("dataFinal") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						string texto = detalheFiltro.Valor[0].ToString();

						dataFim = DateTime.ParseExact(texto, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);

						dataFim = dataFim.AddDays(1).AddSeconds(-1);
					}

					if (detalheFiltro.Campo.Equals("serie") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						serie = Convert.ToInt32(detalheFiltro.Valor[0].ToString());
					}

					if (detalheFiltro.Campo.Equals("despacho") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						numDespacho = Convert.ToInt32(detalheFiltro.Valor[0].ToString());
					}

					if (detalheFiltro.Campo.Equals("fluxo") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						fluxo = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
					}

					if (detalheFiltro.Campo.Equals("Origem") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						origem = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
					}

					if (detalheFiltro.Campo.Equals("Destino") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						destino = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
					}

					if (detalheFiltro.Campo.Equals("chave") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						chaveCte = detalheFiltro.Valor[0].ToString();
					}

					if (detalheFiltro.Campo.Equals("errocte") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						erro = detalheFiltro.Valor[0].ToString();
					}

					if (detalheFiltro.Campo.Equals("Vagao") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						numVagao = detalheFiltro.Valor[0].ToString();
					}
				}
			}

			ResultadoPaginado<CteDto> listaDto = new ResultadoPaginado<CteDto>();

			listaDto = _cteRepository.ObterCteImpressao(pagination, dataIni, dataFim, serie, numDespacho, fluxo, chaveCte, erro, origem, destino, numVagao);

			return listaDto;
		}

		/// <summary>
		/// Retorna todos os Cte filhos do cte atual
		/// </summary>
		/// <param name="cteVigente">Id do Cte vigente</param>
		/// <returns>lista de Cte agrupamento</returns>
		public IList<CteDto> RetornaCteMonitoramentoAgrupamento(int cteVigente)
		{
			IList<CteDto> listaDto = new List<CteDto>();

			CteDto dto = null;

			DespachoTranslogic despacho = null;
			ItemDespacho itemDespacho = null;
			DetalheCarregamento detalheCarregamento = null;
			Carregamento carregamento = null;
			FluxoComercial fluxoComercial = null;
			EstacaoMae estacaoMaeOrigem = null;
			EstacaoMae estacaoMaeDestino = null;
			SerieDespacho serieDespacho = null;
			SerieDespachoUf serieDespachoUf = null;

			Cte cteFilho = _cteRepository.ObterPorId(cteVigente);

			IList<CteAgrupamento> listaCte = _cteAgrupamentoRepository.ObterAgrupamentoPorCteFilho(cteFilho);

			// Remove o cte raiz da lista
			listaCte = listaCte.Where(c => c.CteRaiz != c.CteFilho).ToList();

			IList<CteStatus> v = null;
			Cte cteUltimo = null;

			foreach (CteAgrupamento cteagrupamento in listaCte)
			{
				cteUltimo = _cteRepository.ObterPorId(cteagrupamento.CteFilho.Id);

				CteStatus status = cteUltimo.ListaStatus.OrderByDescending(c => c.Id).FirstOrDefault();

				despacho = cteUltimo.Despacho;

				// para ser listado tem que ter despacho 
				if (despacho != null)
				{
					serieDespacho = despacho.SerieDespacho;
					serieDespachoUf = despacho.SerieDespachoUf;

					itemDespacho = _itemDespachoRepository.ObterItemDespachoPorDespacho(despacho);

					if (itemDespacho != null)
					{
						detalheCarregamento = itemDespacho.DetalheCarregamento;

						if (detalheCarregamento != null)
						{
							carregamento = detalheCarregamento.Carregamento;

							if (carregamento != null)
							{
								fluxoComercial = carregamento.FluxoComercialCarregamento;
							}
						}
					}

					if (fluxoComercial != null)
					{
						estacaoMaeOrigem = fluxoComercial.Origem;
						estacaoMaeDestino = fluxoComercial.Destino;

						dto = new CteDto();
						dto.CteId = cteUltimo.Id ?? 0;
						dto.Fluxo = fluxoComercial.Codigo;
						dto.Origem = estacaoMaeOrigem != null ? estacaoMaeOrigem.Codigo : string.Empty;
						dto.Destino = estacaoMaeDestino != null ? estacaoMaeDestino.Codigo : string.Empty;
						dto.Mercadoria = fluxoComercial.Mercadoria.Apelido;
						dto.Cte = cteUltimo.Serie + "-" + cteUltimo.Numero;
						dto.Chave = cteUltimo.Chave;
						dto.SerieDesp5 = serieDespacho != null ? serieDespacho.SerieDespachoNum : string.Empty;
						dto.NumDesp5 = despacho.NumeroDespacho ?? 0;
						dto.SerieDesp6 = serieDespachoUf != null ? serieDespachoUf.NumeroSerieDespacho : 0;
						dto.NumDesp6 = despacho.NumeroDespacho ?? 0;
						dto.DataEmissao = cteUltimo.DataHora;
						dto.CodigoUsuario = status.Usuario != null ? status.Usuario.Codigo : "...";
						if (estacaoMaeOrigem != null)
						{
							dto.FerroviaOrigem = estacaoMaeOrigem.EmpresaConcessionaria.DescricaoResumida;
							if (estacaoMaeOrigem.Municipio.Estado != null)
							{
								dto.UfOrigem = estacaoMaeOrigem.Municipio.Estado.Sigla;
							}
						}

						if (estacaoMaeDestino != null)
						{
							dto.FerroviaDestino = estacaoMaeDestino.EmpresaConcessionaria.DescricaoResumida;
							if (estacaoMaeDestino.Municipio.Estado != null)
							{
								dto.UfDestino = estacaoMaeDestino.Municipio.Estado.Sigla;
							}
						}

						dto.Impresso = cteUltimo.Impresso;
						dto.SituacaoCte = cteUltimo.SituacaoAtual;
						dto.CodigoErro = string.Format("{0} - {1}", status.CteStatusRetorno.Id, status.CteStatusRetorno.Id == 14 ? status.XmlRetorno : status.CteStatusRetorno.Id == 4001 ? status.Cte.XmlAutorizacao : status.CteStatusRetorno.Descricao);
						dto.StatusCte = status.CteStatusRetorno.Id ?? 0;
						dto.ArquivoPdfGerado = cteUltimo.ArquivoPdfGerado;
						listaDto.Add(dto);
					}
				}
			}

			return listaDto;
		}

		/// <summary>
		/// Retorna todos os Cte filhos do cte atual
		/// </summary>
		/// <param name="cte">Id do Cte para retorno do status</param>
		/// <returns>lista de Cte agrupamento</returns>
		public IList<CteStatus> RetornaCteMonitoramentoStatusCte(int cte)
		{
			IList<CteStatus> cteStatus = _cteRepository.ObterPorId(cte).ListaStatus.OrderByDescending(g => g.DataHora).ToList();

			return cteStatus;
		}

		/// <summary>
		/// Realiza o Reemissão dos Ctes selecionados
		/// </summary>
		/// <param name="ids">ids dos ctes</param>
		/// <param name="usuario">Usuário atual</param>
		/// <param name="mensagemErro">Mensagem de erro utilizados para informar na tela </param>
		/// <returns>resultado do cancelamento</returns>
		public bool ReemissaoCtes(int[] ids, Usuario usuario, out string mensagemErro)
		{
			mensagemErro = string.Empty;
			try
			{
				IList<Cte> listaCte = new List<Cte>();

				foreach (int id in ids)
				{
					listaCte.Add(_cteRepository.ObterPorId(id));
				}

				foreach (Cte cte in listaCte)
				{
					cte.SituacaoAtual = SituacaoCteEnum.PendenteArquivoEnvio;
					_cteRepository.InserirOuAtualizar(cte);
					_cteStatusRepository.InserirCteStatus(cte, usuario, new CteStatusRetorno { Id = 26 });
					// Insere no log do cte
					_cteLogService.InserirLogInfo(cte, "CteService", "Reemissão do cte");
				}
			}
			catch (Exception ex)
			{
				mensagemErro = ex.Message;
				return false;
			}

			return true;
		}

		/// <summary>
		/// Realiza a geração dos arquivos .zip
		/// </summary>
		/// <param name="arrIds">ids dos ctes</param>
		/// <param name="tipoArquivoEnvio"> Tipo do arquivo</param>
		/// <returns>resultado do cancelamento</returns>
		public MemoryStream GerarArquivoZipLote(string[] arrIds, TipoArquivoEnvioEnum tipoArquivoEnvio)
		{
			Stream arquivoPdf;
			CteArquivo file;
			Cte cte;

			try
			{
				Dictionary<string, Stream> arquivos = new Dictionary<string, Stream>();

				// Zip dos arquivos Pdf
				if (tipoArquivoEnvio == TipoArquivoEnvioEnum.Pdf)
				{
					arquivoPdf = ObterArquivoPdf(arrIds);

					arquivos.Add("Dacte.pdf", arquivoPdf);
				}

				// Zip dos arquivos Xml
				if (tipoArquivoEnvio == TipoArquivoEnvioEnum.Xml)
				{
					// percorre os Ctes
					foreach (string id in arrIds)
					{
						// Obtem o Arquivo
						file = ObterArquivoCte(int.Parse(id));
						cte = _cteRepository.ObterPorId(int.Parse(id));

						if (file.ArquivoXml != null)
						{
							arquivos.Add(string.Concat(cte.Chave, ".xml"), Tools.StringToStream(file.ArquivoXml));
						}
					}
				}

				return CreateToMemoryStream(arquivos);
			}
			catch (Exception)
			{
				return null;
			}
		}

		/// <summary>
		/// Realiza o Cancelamento dos Ctes
		/// </summary>
		/// <param name="ids">ids dos ctes</param>
		/// <param name="usuario">Usuário atual</param>
		/// <returns>resultado do cancelamento</returns>
		public bool SalvarInutilizacaoCtes(int[] ids, Usuario usuario)
		{
            IList<Cte> listaCte = new List<Cte>();
            var ctesNaoInutilizados = new List<string>();

            foreach (int id in ids)
            {
                listaCte.Add(_cteRepository.ObterPorId(id));
            }

            foreach (Cte cte in listaCte)
            {
                IList<CteAgrupamento> listaCteAgrupamento = _cteAgrupamentoRepository.ObterAgrupamentoPorCteFilho(cte);

                foreach (CteAgrupamento cteAgrupamento in listaCteAgrupamento)
                {
                    // -> Se o CTE já foi autorizado em algum momento, não pode ser invalidado.
                    if (!JaFoiAutorizadoAlgumaVez(cteAgrupamento.CteFilho))
                    {
                        // mudando Status CTE para Aguardando Inutilização (18)
                        MudarSituacaoCte(cteAgrupamento.CteFilho, SituacaoCteEnum.AguardandoInutilizacao, usuario, new CteStatusRetorno { Id = 18 }, "Arquivo Cte alterado para aguardando inutilização", string.Empty);
                    }
                    else
                    {
                        ctesNaoInutilizados.Add(cteAgrupamento.CteFilho.Numero);
                    }
                }
            }

            if (ctesNaoInutilizados.Count > 0)
            {
                throw new TransactionException(string.Format("Os seguintes CTEs não foram inutilizados, pois já foram autorizados em algum momento: {0}. Primeiramente devem ser cancelados.", string.Join(",", ctesNaoInutilizados)));
            }
		}

	    /// <summary>
		/// Obtém a lista de Ctes a serem processados para recebimento (ctes que estão na interface e não foram processados)
		/// </summary>
		/// <param name="indAmbienteSefaz">Indica qual ambiente da SEFAZ 1-produção / 2-homologação</param>
		/// <returns>Lista de Ctes</returns>
		public IList<CteInterfaceRecebimentoConfig> ObterCtesProcessarRecebimento(int indAmbienteSefaz)
		{
			try
			{
				IList<CteInterfaceRecebimentoConfig> listaPooling = _cteInterfaceRecebimentoConfigRepository.ObterNaoProcessados(indAmbienteSefaz);
				foreach (CteInterfaceRecebimentoConfig itemInterface in listaPooling)
				{
					itemInterface.DataUltimaLeitura = DateTime.Now;
					_cteInterfaceRecebimentoConfigRepository.Atualizar(itemInterface);
				}

				return listaPooling;
			}
			catch (Exception ex)
			{
				_cteLogService.InserirLogErro(null, "CteService", string.Format("ObterCtesProcessarRecebimento: {0}", ex.Message), ex);
				throw ex;
			}
		}

		/// <summary>
		/// Obter os Ctes para serem enviados para o SAP
		/// </summary>
		/// <param name="indAmbienteSefaz">Indica qual ambiente da SEFAZ 1-produção / 2-homologação</param>
		/// <returns>Retorna a lista de ctes a serem enviados para o sap</returns>
		public IList<CteInterfaceEnvioSap> ObterCtesProcessarEnvioSap(int indAmbienteSefaz)
		{
			try
			{
				IList<CteInterfaceEnvioSap> listaPooling = _cteInterfaceEnvioSapRepository.ObterNaoProcessados(indAmbienteSefaz);
				foreach (CteInterfaceEnvioSap itemInterface in listaPooling)
				{
					itemInterface.DataUltimaLeitura = DateTime.Now;
					_cteInterfaceEnvioSapRepository.Atualizar(itemInterface);
				}

				return listaPooling;
			}
			catch (Exception ex)
			{
				_cteLogService.InserirLogErro(null, "CteService", string.Format("ObterCtesProcessarEnvioSap: {0}", ex.Message), ex);
				throw ex;
			}
		}

        /// <summary>
        /// verifica no historico de status do CTE se em algum momento ele já foi autorizado
        /// </summary>
        /// <param name="cte">Cte para ser verificado</param>
        /// <returns>valor boleano</returns>
        public bool JaFoiAutorizadoAlgumaVez(Cte cte)
        {
            return _cteStatusRepository.TemStatusAutorizado(cte);
        }

        /// <summary>
        /// verifica no historico de status do CTE (EAR) se em algum momento ele já foi autorizado
        /// </summary>
        /// <param name="cte">Cte para ser verificado</param>
        /// <returns>valor boleano</returns>
        public bool EARJaFoiAutorizadoAlgumaVez(Cte cte)
        {
            var cteOriginal = _cteRepository.ObterPorIdCte(cte.Id.Value);

            if (cteOriginal.SituacaoAtual == SituacaoCteEnum.ErroAutorizadoReEnvio)
            {
                return _cteStatusRepository.TemStatusAutorizado(cte);
            }

            return false;
        }

		/// <summary>
		/// Dummy testar o insert no TbDatabaseInput
		/// </summary>
		/// <param name="cte">Cte de teste</param>
		public void Dummy(Cte cte)
		{
			// Atualiza o flag de PDF gerado
			// cte.ArquivoXmlGerado = true;
			_cteRepository.AtualizarArquivoGeradoCte(cte, TipoArquivoEnvioEnum.Pdf);
			// Insere na interface de envio de email
			InserirInterfaceEnvioEmail(cte);

			// InserirInterfaceArquivoPdf(cte);

			// Insere na interface de envio para o SAP
			// InserirInterfaceEnvioSap(cte);
			// string valor = GerarHtmlEmail(cte);
			/*
			IList<string> lista = new List<string>();
			lista.Add("marcelo");
			lista.Add("carolina");
			lista.Add("gabriela");
			lista.Add("juliana");

			IList<string> listaAux = new List<string>();
			listaAux.Add("MARCELO");
			listaAux.Add("livia");
			listaAux.Add("juliana");

			// Encontra os nomes comuns
			var nomes = lista.Select(g => g.ToUpper()).Intersect(listaAux.Select(s => s.ToUpper()));
			foreach (string nome in nomes)
			{
											string teste = nome;
			}

			var nomeAux = lista.Except(listaAux);

			foreach (string s in nomeAux)
			{
											string teste = s;
			}

			var nomeDif = listaAux.Except(lista);

			foreach (string s in nomeDif)
			{
											string teste = s;
			}
			*/
		}

		/// <summary>
		/// Insere no pooling de recebimento do Cte
		/// </summary>
		/// <param name="listaCtesNaoProcessados"> The lista ctes nao processados. </param>
		/// <param name="hostName"> The host name. </param>
		public void InserirPoolingRecebimentoCte(IList<CteInterfaceRecebimentoConfig> listaCtesNaoProcessados, string hostName)
		{
			foreach (CteInterfaceRecebimentoConfig itemRecebimento in listaCtesNaoProcessados)
			{
				try
				{
					Cte cte = itemRecebimento.Cte;
					CteRecebimentoPooling crp = new CteRecebimentoPooling();
					crp.Id = cte.Id;
					crp.IdListaConfig = itemRecebimento.IdListaConfig;
					crp.DataHora = DateTime.Now;
					crp.HostName = hostName;

					// Insere o cte no pooling
					_cteRecebimentoPoolingRepository.Inserir(crp);

					// Insere no log do cte
					_cteLogService.InserirLogInfo(cte, "CteService", "Adicionado no pooling de recebimento");
				}
				catch (Exception e)
				{
					// Limpa a sessão para processar o próximo
					_cteEnvioPoolingRepository.ClearSession();
				}
			}
		}

		/// <summary>
		/// Insere o Cte no pooling de envio para SAP
		/// </summary>
		/// <param name="listaCtesNaoProcessados"> The lista ctes nao processados</param>
		/// <param name="hostName"> The host name. </param>
		public void InserirPoolingEnvioSapCte(IList<CteInterfaceEnvioSap> listaCtesNaoProcessados, string hostName)
		{
			foreach (CteInterfaceEnvioSap itemEnvio in listaCtesNaoProcessados)
			{
				try
				{
					Cte cte = itemEnvio.Cte;
					CteSapPooling csp = new CteSapPooling();
					csp.Id = cte.Id;
					csp.DataHora = DateTime.Now;
					csp.HostName = hostName;
					csp.TipoOperacao = itemEnvio.TipoOperacao;

					// Insere o cte no pooling
					_cteSapPoolingRepository.Inserir(csp);

					// Insere no log do cte
					_cteLogService.InserirLogInfo(cte, "CteService", "Adicionado no pooling de envio para o SAP");
				}
				catch (Exception e)
				{
					// Limpa a sessão para processar o próximo
					_cteSapPoolingRepository.ClearSession();
				}
			}
		}

		/// <summary>
		/// Obtém os ctes que estão no pool de recebimento pelo hostname
		/// </summary>
		/// <param name="hostName">Nome do Host</param>
		/// <returns>Lista de Ctes</returns>
		public IList<CteRecebimentoPooling> ObterPoolingRecebimentoPorServidor(string hostName)
		{
			try
			{
				return _cteRecebimentoPoolingRepository.ObterListaRecebimentoPorHost(hostName);
			}
			catch (Exception ex)
			{
				_cteLogService.InserirLogErro(null, "CteService", string.Format("ObterPoolingRecebimentoPorServidor ({0}): {1}", hostName, ex.Message), ex);
				throw ex;
			}
		}

		/// <summary>
		/// Obtém os ctes que estão no pool de envio para o SAP pelo hostname
		/// </summary>
		/// <param name="hostName">Nome do Host</param>
		/// <returns>Lista de Ctes</returns>
		public IList<CteSapPooling> ObterPoolingEnvioSapPorServidor(string hostName)
		{
			try
			{
				return _cteSapPoolingRepository.ObterListaRecebimentoPorHost(hostName);
			}
			catch (Exception ex)
			{
				_cteLogService.InserirLogErro(null, "CteService", string.Format("ObterPoolingSapPorServidor ({0}): {1}", hostName, ex.Message), ex);
				throw ex;
			}
		}

		/// <summary>
		/// Muda a situacao do cte e insere o status retorno e grava o log
		/// </summary>
		/// <param name="cte">cte que será atualizado</param>
		/// <param name="situacao">nova situacao do cte</param>
		/// <param name="usuario">usuário que está mudando a situação</param>
		/// <param name="statusRetorno">status retorno</param>
		/// <param name="mensagemLog">mensagem de log que será gravada no log do Cte</param>
		/// <param name="mensagemStatusRetorno">Mensagem do status de retorno</param>
		public void MudarSituacaoCte(Cte cte, SituacaoCteEnum situacao, Usuario usuario, CteStatusRetorno statusRetorno, string mensagemLog, string mensagemStatusRetorno)
		{
			cte.SituacaoAtual = situacao;
			_cteRepository.Atualizar(cte);

			if (usuario == null)
			{
				// Pega o usuario default da conf geral
				usuario = ObterUsuarioRobo();
			}

			_cteStatusRepository.InserirCteStatus(cte, usuario, statusRetorno, mensagemStatusRetorno);

			// Insere no log do cte
			_cteLogService.InserirLogInfo(cte, "CteService", string.Format("MudarSituacaoCte: {0}", mensagemLog));
		}

		/// <summary>
		/// Processa o retorno do Cte pelo item do pooling
		/// </summary>
		/// <param name="itemRecebimento">Item do pooling</param>
		/// <returns>Verdadeiro se conseguiu processar o retorno</returns>
		public bool ProcessarCteRetorno(CteRecebimentoPooling itemRecebimento)
		{
			try
			{
				Vw209ConsultaCte consultaCte = _filaProcessamentoService.ObterRetornoCteProcessadoPorIdLista(itemRecebimento.IdListaConfig);

				if (consultaCte != null)
				{
					Cte cteAux = _cteRepository.ObterPorId(itemRecebimento.Id);

					AtualizarCteRetorno(cteAux, consultaCte);
					return true;
				}

				return false;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		/// <summary>
		/// Verifica Cte Arvore
		/// </summary>
		/// <param name="listaCte">Lista de Cte a serem verificados</param>
		public void VerificaCteArvorePorChave(List<string> listaCte)
		{
			foreach (string chave in listaCte)
			{
				VerificarCteFolhaArvorePorChave(chave);
			}
		}

		/// <summary>
		/// Verifica Cte Arvore pela Chave do Cte
		/// </summary>
		/// <param name="listaChave">Lista de chave do Cte</param>
		/// <returns>Retorna true quando for o ultimo cte da arvore</returns>
		public Dictionary<string, KeyValuePair<bool, string>> VerificarCteFolhaArvorePorListaChave(List<string> listaChave)
		{
			Dictionary<string, KeyValuePair<bool, string>> listaRetorno = new Dictionary<string, KeyValuePair<bool, string>>();

			if (listaChave != null)
			{
				foreach (string chave in listaChave)
				{
					KeyValuePair<bool, string> statusRetorno = VerificarCteFolhaArvorePorChave(chave);
					listaRetorno.Add(chave, statusRetorno);
				}
			}

			return listaRetorno;
		}

		/// <summary>
		/// Verifica Cte Arvore pela Chave do Cte
		/// </summary>
		/// <param name="chave">Chave do Cte</param>
		/// <returns>Retorna false quando for o ultimo cte da arvore - true quando encontrar algum erro</returns>
		public KeyValuePair<bool, string> VerificarCteFolhaArvorePorChave(string chave)
		{
			Cte cte = _cteRepository.ObterPorChave(chave);

			if (chave.Length != 44)
			{
				return new KeyValuePair<bool, string>(true, "Chave CTe deve conter 44 caracteres.");
			}

			if (!VerificarDigitoChaveCte(chave))
			{
				return new KeyValuePair<bool, string>(true, "Chave Cte inválida.");
			}

			if (cte == null)
			{
				return new KeyValuePair<bool, string>(true, "Chave Cte não encontrada na base");
			}

			IList<CteArvore> listaArvore = _cteArvoreRepository.ObterArvorePorCteFilho(cte);
			CteArvore cteArvore = listaArvore.OrderByDescending(c => c.Sequencia).FirstOrDefault();

			if (cte.SituacaoAtual != SituacaoCteEnum.Autorizado)
			{
				return new KeyValuePair<bool, string>(true, "Cte cancelado");
			}

			if (cteArvore == null)
			{
				return new KeyValuePair<bool, string>(true, "Cte arvore não encontrado.");
			}

			if (cteArvore.CteFilho.Chave == chave)
			{
				return new KeyValuePair<bool, string>(false, "Chave Cte válida.");
			}

			return new KeyValuePair<bool, string>(true, "Existe um novo Cte gerado a partir da manutenção.");
		}

		/// <summary>
		/// Verifica se é o ultimo cte da arvore
		/// </summary>
		/// <param name="id">id do cte a ser validado</param>
		/// <returns>Retorna true se for o ultimo cte da arvore</returns>
		public bool VerificarCteFolha(int id)
		{
			var cte = _cteRepository.ObterPorId(id);

			IList<CteArvore> listaArvore = _cteArvoreRepository.ObterArvorePorCteFilho(cte);
			CteArvore cteArvore = listaArvore.OrderByDescending(c => c.Sequencia).FirstOrDefault();

			return cteArvore.CteFilho.Chave == cte.Chave;
		}

		/// <summary>
		/// Verificar se o digito verificador é valido
		/// </summary>
		/// <param name="chave">Chave do Cte</param>
		/// <returns>Retorna true se a chave for verdadeira</returns>
		public bool VerificarDigitoChaveCte(string chave)
		{
			char[] pesoCte = "4329876543298765432987654329876543298765432".ToCharArray();
			char[] chaveCte = chave.ToCharArray();
			int soma = 0;
			int digito = 0;
			int diferenca = 0;
			int sum = 0;

			for (int i = 0; i < 43; i++)
			{
				sum = int.Parse(chaveCte[i].ToString()) * int.Parse(pesoCte[i].ToString());
				soma += sum;
			}

			diferenca = soma % 11;
			digito = 11 - diferenca;

			if (diferenca <= 1)
			{
				digito = 0;
			}

			if (int.Parse(chaveCte[43].ToString()) == digito)
			{
				return true;
			}

			return false;
		}

		/// <summary>
		/// Atualiza o Status do Cte de retorno
		/// </summary>
		/// <param name="cte">Cte de retorno</param>
		/// <param name="consultaCte">Objeto de consulta do Cte</param>
		public void AtualizarCteRetorno(Cte cte, Vw209ConsultaCte consultaCte)
		{
			try
			{
				Usuario usuario = ObterUsuarioRobo();

				// Insere no log do cte
				_cteLogService.InserirLogInfo(cte, "CteService", "Processando o cte de retorno");

				CteStatusRetorno cteStatusRetorno = _cteStatusRetornoRepository.ObterPorId(consultaCte.Stat);
				if (cteStatusRetorno == null)
				{
					cteStatusRetorno = new CteStatusRetorno
					{
						Descricao = consultaCte.DescStat,
						Id = consultaCte.Stat,
						InformadoPelaReceita = false,
						PermiteReenvio = false,
						RemoveFilaInterface = true
					};

					_cteStatusRetornoRepository.Inserir(cteStatusRetorno);
				}

				// Processa o status de retono
				ProcessarCteStatusRetorno(cte, cteStatusRetorno, consultaCte);

				if ((cte.SituacaoAtual == SituacaoCteEnum.Autorizado) || (cte.SituacaoAtual == SituacaoCteEnum.AguardandoCancelamento) || (cte.SituacaoAtual == SituacaoCteEnum.AguardandoInutilizacao))
				{
					// Item da fila de processamento (utilizado para pegar o XML de retorno)
					Cte01Lista retornoConfig = _filaProcessamentoService.ObterItemFilaProcessamentoPorConsultaRetorno(consultaCte);
					_cteStatusRepository.InserirCteStatus(cte, usuario, cteStatusRetorno, retornoConfig.XmlRetorno);
				}
				else
				{
					_cteStatusRepository.InserirCteStatus(cte, usuario, cteStatusRetorno, cteStatusRetorno.Descricao);
				}

				cte.NumeroProtocolo = string.Format("{0}", consultaCte.Protocolo);
				_cteRepository.Atualizar(cte);

				// Verifica se pode ser removido da fila da interface
				if (cteStatusRetorno.RemoveFilaInterface)
				{
					// Remove o cte da interface
					_cteInterfaceRecebimentoConfigRepository.RemoverPorCte(cte);
				}

				// Verifica se pode ser inserido na interface de geração do PDF
				if (cte.SituacaoAtual == SituacaoCteEnum.Autorizado)
				{
					// Insere na interface de geração arquivo PDF
					InserirInterfaceArquivoPdf(cte);

					// Insere na interface de geração arquivo XML
					InserirInterfaceArquivoXml(cte);

					// Insere na interface de envio para o SAP
					InserirInterfaceEnvioSap(cte, cteStatusRetorno);
				}

				if (cte.SituacaoAtual == SituacaoCteEnum.AutorizadoCancelamento)
				{
					// Verificar se todos os cte do agrupamento estao autorizados
					// caso todos estejam autorizado cancelamento -> mudar a situacao para CAN (Cancelado)
					// Enviar informações para o SAP
					bool cteComAgrupamentoNaoAutorizado = false;

					IList<CteAgrupamento> listaCteAgrupamento = _cteAgrupamentoRepository.ObterAgrupamentoPorCteFilho(cte);

					foreach (CteAgrupamento cteAgrupamento in listaCteAgrupamento)
					{
						if (cteAgrupamento.CteFilho.SituacaoAtual != SituacaoCteEnum.AutorizadoCancelamento)
						{
							cteComAgrupamentoNaoAutorizado = true;
						}
					}

					if (!cteComAgrupamentoNaoAutorizado)
					{
						foreach (CteAgrupamento cteAgrupamento in listaCteAgrupamento)
						{
							MudarSituacaoCte(cteAgrupamento.CteFilho, SituacaoCteEnum.Cancelado, usuario, new CteStatusRetorno { Id = 13 }, "Arquivo Cte alterado para cancelado", string.Empty);

							// Insere na interface de envio para o SAP
							InserirInterfaceEnvioSap(cteAgrupamento.CteFilho, cteStatusRetorno);
						}
					}
				}

				if (cte.SituacaoAtual == SituacaoCteEnum.Invalidado)
				{
					_cteStatusRepository.InserirCteStatus(cte, usuario, new CteStatusRetorno { Id = 15 });
				}
			}
			catch (Exception ex)
			{
				_cteLogService.InserirLogErro(cte, "CteService", string.Format("AtualizarCteRetorno: {0}", ex.Message), ex);
				throw ex;
			}
		}

		/// <summary>
		/// Processa o arquivo de retorno e insere o status de retorno caso não exista
		/// </summary>
		/// <param name="conteudoArquivo">Conteúdo do arquivo de retorno do SEFAZ</param>
		/// <returns>Objeto CteStatusRetorno</returns>
		public CteStatusRetorno ProcessarConteudoArquivoRetorno(string conteudoArquivo)
		{
			string[] split = conteudoArquivo.Split(';');
			string codigoRetornoString = split[7];
			int codigoRetorno;
			int.TryParse(codigoRetornoString, out codigoRetorno);

			CteStatusRetorno cteStatusRetorno = _cteStatusRetornoRepository.ObterPorId(codigoRetorno);
			if (cteStatusRetorno == null)
			{
				string mensagemRetorno = split[8];
				cteStatusRetorno = new CteStatusRetorno
				{
					Descricao = mensagemRetorno,
					Id = codigoRetorno,
					InformadoPelaReceita = true
				};
				_cteStatusRetornoRepository.Inserir(cteStatusRetorno);
			}

			return cteStatusRetorno;
		}

		/// <summary>
		/// Remove da tabela de Pool de recebimento de da tabela de FTP
		/// </summary>
		/// <param name="listaProcessamento">Lista de arquivos a serem removidos</param>
		public void RemoverPoolingRecebimentoCte(IList<CteRecebimentoPooling> listaProcessamento)
		{
			_cteRecebimentoPoolingRepository.RemoverPorLista(listaProcessamento);
		}

		/// <summary>
		/// Remove da tabela de Pool de envio para SAP
		/// </summary>
		/// <param name="listaProcessamento">Lista de arquivos a serem removidos</param>
		public void RemoverPoolingEnvioSapCte(IList<CteSapPooling> listaProcessamento)
		{
			_cteSapPoolingRepository.RemoverPorLista(listaProcessamento);
		}

		/// <summary>
		/// Obtém o vagão pelo código
		/// </summary>
		/// <param name="codigoVagao">Código do vagão</param>
		/// <returns>Objeto Vagao</returns>
		public Vagao ObterVagaoPorCodigo(string codigoVagao)
		{
			Vagao vagao = _vagaoRepository.ObterPorCodigo(codigoVagao);
			if (vagao != null)
			{
				if (!vagao.IndAtivo)
				{
					throw new TranslogicException("O vagão não está ativo no sistema.");
				}
			}

			return vagao;
		}

		/// <summary>
		/// Obtém a lista de ctes que devem ser enviado para o SAP
		/// </summary>
		/// <returns>Retorna uma lista com os ctes a serem enviados</returns>
		public IList<CteInterfaceEnvioSap> ObterDadosParaEnvioSap()
		{
			string hostName = Dns.GetHostName();
			return _cteInterfaceEnvioSapRepository.ObterNaoProcessadosPorHost(hostName);
		}

		/// <summary>
		/// Envia dados para a interface do SAP
		/// </summary>
		/// <param name="itemPooling">Item do Pooling</param>
		/*
				public void EnviarInterfaceSap(CteSapPooling itemPooling)
				{
					string hostName = Dns.GetHostName();
					Cte cteAux = _cteRepository.ObterPorId(itemPooling.Id);

					try
					{
						Usuario usuario = ObterUsuarioRobo();

						// Serviço de gravação nas tabelas do SAP
						_gravarSapCteEnvioService.Executar(cteAux, usuario);
					}
					catch (Exception ex)
					{
						_cteLogService.InserirLogErro(cteAux, "CteService", string.Format("EnviarInterfaceSap ({0}): {1}", hostName, ex.Message), ex);
						throw ex;
					}
				}
		*/

		/// <summary>
		/// Grava na lista de envio do CT-e para o SAP o HOST 
		/// </summary>
		/// <param name="hostName">Nome do host</param>
		[Transaction]
		public virtual void GravarHostListaNaoProcessados(string hostName)
		{
			IList<CteInterfaceEnvioSap> list = _cteInterfaceEnvioSapRepository.ObterNaoProcessadosSemHost();
			foreach (CteInterfaceEnvioSap cteInterfaceEnvioSap in list)
			{
				_cteInterfaceEnvioSapRepository.GravarHostNaoProcessados(hostName, cteInterfaceEnvioSap);
			}
		}

		/// <summary>
		/// Atualiza a tabela de envio para a interface de envio para o SAP
		/// </summary>
		/// <param name="listaCteEnvioSap">Lista com os ctes a serem atualizados</param>
		public void AtualizarEnvioInterfaceSap(IList<CteInterfaceEnvioSap> listaCteEnvioSap)
		{
			foreach (CteInterfaceEnvioSap interfaceEnvioSap in listaCteEnvioSap)
			{
				_cteInterfaceEnvioSapRepository.AtualizarEnvioSap(interfaceEnvioSap.Cte);
			}
		}

		/// <summary>
		/// Atualiza a tabela de envio para a interface de envio para o SAP
		/// </summary>
		/// <param name="interfaceEnvioSap">Interface para ser atualizada</param>
		public void AtualizarEnvioInterfaceSap(CteInterfaceEnvioSap interfaceEnvioSap)
		{
			_cteInterfaceEnvioSapRepository.AtualizarEnvioSap(interfaceEnvioSap.Cte);
		}

		/// <summary>
		/// Obtém uma lista com os ctes para importação
		/// </summary>
		/// <param name="hostName">Nome do servidor</param>
		/// <returns>Retorna uma lista com os ctes para serem importados</returns>
		public IList<CteInterfacePdfConfig> ObterCtesImportarPdf(string hostName)
		{
			try
			{
				IList<CteInterfacePdfConfig> listaInterface = _cteInterfacePdfRepository.ObterListaInterfacePorHost(hostName);
				foreach (CteInterfacePdfConfig itemInterface in listaInterface)
				{
					itemInterface.DataUltimaLeitura = DateTime.Now;
					itemInterface.DataUltimaLeitura = DateTime.Now;
					_cteInterfacePdfRepository.Atualizar(itemInterface);
				}

				return listaInterface;
			}
			catch (Exception ex)
			{
				_cteLogService.InserirLogErro(null, "CteService", string.Format("ObterCtesImportarPdf ({0}): {1}", hostName, ex.Message), ex);
				throw ex;
			}
		}

		/// <summary>
		/// Obtém uma lista com os ctes para importação
		/// </summary>
		/// <param name="hostName">Nome do servidor</param>
		/// <returns>Retorna uma lista com os ctes para serem importados</returns>
		public IList<CteInterfaceXmlConfig> ObterCtesImportarXml(string hostName)
		{
			try
			{
				IList<CteInterfaceXmlConfig> listaInterface = _cteInterfaceXmlRepository.ObterListaInterfacePorHost(hostName);
				foreach (CteInterfaceXmlConfig itemInterface in listaInterface)
				{
					itemInterface.DataUltimaLeitura = DateTime.Now;
					_cteInterfaceXmlRepository.Atualizar(itemInterface);
				}

				return listaInterface;
			}
			catch (Exception ex)
			{
				_cteLogService.InserirLogErro(null, "CteService", string.Format("ObterCtesImportarPdf ({0}): {1}", hostName, ex.Message), ex);
				throw ex;
			}
		}

		/// <summary>
		/// Obtém os Ctes que devem ser enviado email
		/// </summary>
		/// <param name="hostName">Nome do servidor</param>
		/// <returns>Retorna uma lista com os dados da interface de envio de email</returns>
		public IList<CteInterfaceEnvioEmail> ObterCteEnviarEmail(string hostName)
		{
			IList<CteInterfaceEnvioEmail> lista = _cteInterfaceEnvioEmailRepository.ObterListaInterfacePorHost(hostName);
			foreach (CteInterfaceEnvioEmail cteInterfaceEnvioEmail in lista)
			{
				cteInterfaceEnvioEmail.DataUltimaLeitura = DateTime.Now;
				_cteInterfaceEnvioEmailRepository.Atualizar(cteInterfaceEnvioEmail);
			}

			return lista;
		}

		/// <summary>
		/// Importa o pdf para dentro do banco
		/// </summary>
		/// <param name="cteInterfacePdf">Interface pdf do Cte</param>
		/// <param name="pdfPah">Caminho do pdf</param>
		public void ImportarArquivoPdf(CteInterfacePdfConfig cteInterfacePdf, string pdfPah)
		{
			Cte cte = _cteRepository.ObterPorId(cteInterfacePdf.Cte.Id);

			string nomeArquivo = pdfPah + cte.Chave + ".pdf";

			try
			{
				// Verifica se o arquivo existe
				if (File.Exists(nomeArquivo))
				{
					// Insere o arquivo pdf no banco de dados
					_cteArquivoRepository.InserirOuAtualizarPdf(cte, nomeArquivo);
					_cteLogService.InserirLogInfo(cte, "CteService", "Pdf inserido na base (CTE_ARQUIVO)");

					// string processado = pdfPah + "Processados\\" + cte.Chave + ".pdf";
					string processado = ObterDiretorioProcessado(pdfPah, TipoArquivoEnvioEnum.Pdf, cte.DataHora) + cte.Chave + ".pdf";

					// Move o arquivo para o dir de processados
					File.Move(nomeArquivo, processado);
					_cteLogService.InserirLogInfo(cte, "CteService", string.Format("Movendo arquivo de: {0} para: {1}", nomeArquivo, processado));

					_cteLogService.InserirLogInfo(cte, "CteService", "Removendo da interface de geração do Pdf");

					// Remove da lista da interface
					_cteInterfacePdfRepository.Remover(cteInterfacePdf);

					// Atualiza o flag de PDF gerado
					// cte.ArquivoPdfGerado = true;
					_cteRepository.AtualizarArquivoGeradoCte(cte, TipoArquivoEnvioEnum.Pdf);

					// Insere na interface de envio de email
					InserirInterfaceEnvioEmail(cte);
				}
				else
				{
					_cteLogService.InserirLogErro(cte, "CteService", string.Format("Não encontrado o arquivo: {0} ", nomeArquivo));
				}
			}
			catch (Exception ex)
			{
				_cteLogService.InserirLogErro(cte, "CteService", string.Format("ImportarArquivoPdf: Erro na importacao do arquivo em ImportarArquivoPdf: {0}", ex.Message), ex);
			}
		}

		/// <summary>
		/// Importa o xml para dentro do banco
		/// </summary>
		/// <param name="cteInterfaceXml">Interface xml do Cte</param>
		/// <param name="xmlPah">Caminho do xml</param>
		public void ImportarArquivoXml(CteInterfaceXmlConfig cteInterfaceXml, string xmlPah)
		{
			Cte cte = _cteRepository.ObterPorId(cteInterfaceXml.Cte.Id);

			string nomeArquivo = xmlPah + cte.Chave + ".xml";

			try
			{
				// Verifica se o arquivo existe
				if (File.Exists(nomeArquivo))
				{
					// Insere o arquivo xml no banco de dados
					_cteArquivoRepository.InserirOuAtualizarXml(cte, nomeArquivo);
					_cteLogService.InserirLogInfo(cte, "CteService", "XML inserido na base (CTE_ARQUIVO)");

					// string processado = xmlPah + "XmlProcessados\\" + cte.Chave + ".xml";
					string processado = ObterDiretorioProcessado(xmlPah, TipoArquivoEnvioEnum.Xml, cte.DataHora) + cte.Chave + ".xml";

					// Move o arquivo para o dir de processados
					File.Move(nomeArquivo, processado);
					_cteLogService.InserirLogInfo(cte, "CteService", string.Format("Movendo arquivo de: {0} para: {1}", nomeArquivo, processado));

					_cteLogService.InserirLogInfo(cte, "CteService", "Removendo da interface de geração do Xml");

					// Remove da lista da interface
					_cteInterfaceXmlRepository.Remover(cteInterfaceXml);

					// Atualiza o flag de PDF gerado
					// cte.ArquivoXmlGerado = true;
					_cteRepository.AtualizarArquivoGeradoCte(cte, TipoArquivoEnvioEnum.Xml);

					// Insere na interface de envio de email
					InserirInterfaceEnvioEmail(cte);
				}
				else
				{
					_cteLogService.InserirLogErro(cte, "CteService", string.Format("Não encontrado o arquivo: {0} ", nomeArquivo));
				}
			}
			catch (Exception ex)
			{
				_cteLogService.InserirLogErro(cte, "CteService", string.Format("ImportarArquivoXml: Erro na importacao do arquivo em ImportarArquivoXml: {0}", ex.Message), ex);
			}
		}

		/// <summary>
		/// Salva os e-mail
		/// </summary>
		/// <param name="email"> e-mail a ser alterado</param>
		/// <param name="novoEmail"> Novo e-mail a ser alterado</param>
		/// <param name="fluxo"> Fluxo informado</param>
		public void SalvarEmailCteReenvioArquivo(string email, string novoEmail, string fluxo)
		{
			_cteRepository.SalvarEmailCteReenvioArquivo(email, novoEmail, fluxo);
		}

		/// <summary>
		/// Adiciona um email no cte reenvio arquivo
		/// </summary>
		/// <param name="idCte">Id Cte para cadastro do email</param>
		/// <param name="email">novo email a ser cadastrado</param>
		/// <param name="tipoEnvioCte">Tipo de envio do email</param>
		/// <param name="tipoArquivoEnvio">tipo do Arquivo a ser enviado</param>
		public void AdicionarEmailCteReenvioArquivo(int idCte, string email, TipoEnvioCteEnum tipoEnvioCte, TipoArquivoEnvioEnum tipoArquivoEnvio)
		{
			CteEnvioXml cteEnvioXml = null;
			try
			{
				Cte cte = new Cte { Id = idCte };
				IList<CteEnvioXml> listaEmail = _cteEnvioXmlRepository.ObterTodosPorCte(cte);
				if ((listaEmail != null) && (listaEmail.Count > 0))
				{
					cteEnvioXml = listaEmail.Where(g => g.EnvioPara.ToUpper() == email.ToUpper()).FirstOrDefault();
				}

				if (cteEnvioXml != null)
				{
					throw new Exception(string.Format("Email: {0}<br/> já cadastrado para o Cte!", email));
				}

				cteEnvioXml = new CteEnvioXml();
				cteEnvioXml.EnvioPara = email;
				cteEnvioXml.TipoArquivoEnvio = tipoArquivoEnvio;
				cteEnvioXml.TipoEnvioCte = tipoEnvioCte;
				cteEnvioXml.Cte = _cteRepository.ObterPorId(idCte);
				_cteEnvioXmlRepository.Inserir(cteEnvioXml);
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		/// <summary>
		/// Envia os emails com o Cte
		/// </summary>
		/// <param name="idXmlEnvio">Id do xml Envio</param>
		/// <param name="tipoEnvio">Tipo de Envio de arquivo</param>
		public void EnviarEmail(int idXmlEnvio, TipoArquivoEnvioEnum tipoEnvio)
		{
			string mask = ":";
			string emailServer = string.Empty;
			string remetente = string.Empty;
			string[] valorArray = null;
			int emailPort = 0;
			ConfiguracaoTranslogic configuracaoTranslogic = null;
			ConfiguracaoTranslogic configuracaoRemetente = null;
			Stream arquivoPdf = null;
			Stream arquivoXml = null;
			IList<CteEnvioXml> lista = null;
			CteEnvioXml auxCteEnvioXml = null;
			CteEnvioXml cteEnvioXml = null;

			try
			{
				lista = new List<CteEnvioXml>();

				auxCteEnvioXml = _cteEnvioXmlRepository.ObterPorId(idXmlEnvio);
				
				cteEnvioXml = new CteEnvioXml();
				cteEnvioXml.Ativo = true;
				cteEnvioXml.Cte = auxCteEnvioXml.Cte;
				cteEnvioXml.EnvioPara = auxCteEnvioXml.EnvioPara;
				cteEnvioXml.TipoEnvioCte = auxCteEnvioXml.TipoEnvioCte;
				cteEnvioXml.TipoArquivoEnvio = tipoEnvio;

				lista.Add(cteEnvioXml);

				// Recupera as informações do Cte
				CteArquivo cteArquivo = _cteArquivoRepository.ObterPorId(cteEnvioXml.Cte.Id);

				if (tipoEnvio == TipoArquivoEnvioEnum.Pdf || tipoEnvio == TipoArquivoEnvioEnum.PdfXml)
				{
					arquivoPdf = Tools.ByteArrayToStream(cteArquivo.ArquivoPdf);
				}

				if (tipoEnvio == TipoArquivoEnvioEnum.Xml || tipoEnvio == TipoArquivoEnvioEnum.PdfXml)
				{
					arquivoXml = Tools.StringToStream(cteArquivo.ArquivoXml);
				}

				configuracaoTranslogic = _configuracaoTranslogicRepository.ObterPorId(_cteEmailServer);
				valorArray = configuracaoTranslogic.Valor.Split(mask.ToCharArray());

				emailServer = valorArray[0];
				emailPort = Convert.ToInt32(valorArray[1]);

				configuracaoRemetente = _configuracaoTranslogicRepository.ObterPorId(_cteEmailRemetente);
				remetente = configuracaoRemetente.Valor;

				EnviarEmail(cteEnvioXml.Cte, emailServer, emailPort, remetente, lista, tipoEnvio, arquivoPdf, arquivoXml);
			}
			finally
			{
				mask = ":";
				emailServer = string.Empty;
				remetente = string.Empty;
				valorArray = null;
				emailPort = 0;
				configuracaoTranslogic = null;
				configuracaoRemetente = null;
				arquivoPdf = null;
				arquivoXml = null;
				lista = null;
				cteEnvioXml = null;
			}
		}

		/// <summary>
		/// Envia os emails com o Cte
		/// </summary>
		/// <param name="cteInterfaceEnvioEmail">Interface de envio de email</param>
		/// <param name="emailServer">Servidor de email</param>
		/// <param name="emailPort">Porta do servidor de email</param>
		/// <param name="remetente">Remetente do email do Cte</param>
		public void EnviarEmail(CteInterfaceEnvioEmail cteInterfaceEnvioEmail, string emailServer, int emailPort, string remetente)
		{
			Cte cte = cteInterfaceEnvioEmail.Cte;
			Stream arquivoPdf = null;
			Stream arquivoXml = null;

			try
			{
				// Lista os envios
				IList<CteEnvioXml> listaEnvio = _cteEnvioXmlRepository.ObterPorCte(cte);

				// Recupera as informações do Cte
				CteArquivo cteArquivo = _cteArquivoRepository.ObterPorId(cte.Id);
				if (cteArquivo == null)
				{
					// Grava no status do Cte
					_cteStatusRepository.InserirCteStatus(cte, ObterUsuarioRobo(), new CteStatusRetorno { Id = 29 });

					// Insere no log do cte
					_cteLogService.InserirLogErro(cte, "CteService", string.Format("EnviarEmail: cte {0} não encontrado os arquivos de envio na base de dados - HostName:{1}", cte.Id, Dns.GetHostName()));

					// Remove da interface
					_cteInterfaceEnvioEmailRepository.Remover(cteInterfaceEnvioEmail);

					return;
				}

				arquivoPdf = Tools.ByteArrayToStream(cteArquivo.ArquivoPdf);
				arquivoXml = Tools.StringToStream(cteArquivo.ArquivoXml);
				if ((arquivoPdf == null) || (arquivoXml == null))
				{
					// Grava no status do Cte
					_cteStatusRepository.InserirCteStatus(cte, ObterUsuarioRobo(), new CteStatusRetorno { Id = 29 });

					// Insere no log do cte
					_cteLogService.InserirLogErro(cte, "CteService", string.Format("EnviarEmail: cte {0} não encontrado os arquivos de envio na base de dados - HostName:{1}", cte.Id, Dns.GetHostName()));

					// Remove da interface
					_cteInterfaceEnvioEmailRepository.Remover(cteInterfaceEnvioEmail);

					return;
				}

				// #########################################
				// Gera email que envia o PDF e o XML
				// #########################################
				if (arquivoPdf != null)
				{
					arquivoPdf.Seek(0, SeekOrigin.Begin);
				}

				if (arquivoXml != null)
				{
					arquivoXml.Seek(0, SeekOrigin.Begin);
				}

				IList<CteEnvioXml> listaEnvioPdfXml = listaEnvio.Where(g => g.TipoArquivoEnvio == TipoArquivoEnvioEnum.PdfXml).ToList();
				if (listaEnvioPdfXml.Count > 0)
				{
					EnviarEmail(cte, emailServer, emailPort, remetente, listaEnvioPdfXml, TipoArquivoEnvioEnum.PdfXml, arquivoPdf, arquivoXml);
				}

				// #########################################
				// Gera email que envia o PDF
				// #########################################
				IList<CteEnvioXml> listaEnvioPdf = listaEnvio.Where(g => g.TipoArquivoEnvio == TipoArquivoEnvioEnum.Pdf).ToList();
				if (listaEnvioPdf.Count > 0)
				{
					EnviarEmail(cte, emailServer, emailPort, remetente, listaEnvioPdf, TipoArquivoEnvioEnum.Pdf, arquivoPdf, null);
				}

				// #########################################
				// Gera email que envia XML
				// #########################################
				IList<CteEnvioXml> listaEnvioXml = listaEnvio.Where(g => g.TipoArquivoEnvio == TipoArquivoEnvioEnum.Xml).ToList();
				if (listaEnvioXml.Count > 0)
				{
					EnviarEmail(cte, emailServer, emailPort, remetente, listaEnvioXml, TipoArquivoEnvioEnum.Xml, null, arquivoXml);
				}

				// Grava no status do Cte
				_cteStatusRepository.InserirCteStatus(cte, ObterUsuarioRobo(), new CteStatusRetorno { Id = 28 });

				// Remove da interface
				_cteInterfaceEnvioEmailRepository.Remover(cteInterfaceEnvioEmail);
			}
			catch (Exception ex)
			{
				_cteLogService.InserirLogErro(cteInterfaceEnvioEmail.Cte, "CteService", ex.Message, ex);
				throw ex;
			}
		}

		/*
		public void EnviarEmail(CteInterfaceEnvioEmail cteInterfaceEnvioEmail, string emailServer, int emailPort, string remetente)
		{
						Cte cte = cteInterfaceEnvioEmail.Cte;
						List<string> listaEmailInvalido;
						try
						{
										string diretorio = Directory.GetCurrentDirectory();
										string caminhoLogoAll = diretorio + "\\" + "Logo-All.jpg";
										string emailPara = string.Empty;
										string emailCopia = string.Empty;
										string emailCopiaOculta = string.Empty;
										listaEmailInvalido = new List<string>();
										SmtpClient client;
										MailMessage mailMessage;
										AlternateView html;
										LinkedResource logo;
										Regex regexEmail = new Regex("^([0-9a-zA-Z]+([_.-]?[0-9a-zA-Z]+)*@[0-9a-zA-Z]+[0-9,a-z,A-Z,.,-]*(.){1}[a-zA-Z]{2,4})+$");
										Stream arquivoPdf = null;
										Stream arquivoXml = null;
				
										string chave = cteInterfaceEnvioEmail.Chave;

										// Lista os envios
										IList<CteEnvioXml> listaEnvio = _cteEnvioXmlRepository.ObterPorCte(cte);

										// Caso não tenha email para enviar, 
										if (listaEnvio.Count == 0)
										{
														// Remove o item da interface
														_cteInterfaceEnvioEmailRepository.Remover(cteInterfaceEnvioEmail);

														return;
										}

										// Recupera as informações do Cte
										CteArquivo cteArquivo = _cteArquivoRepository.ObterPorId(cte.Id);

										arquivoPdf = Tools.ByteArrayToStream(cteArquivo.ArquivoPdf);
										arquivoXml = Tools.StringToStream(cteArquivo.ArquivoXml);

										// #########################################
										// Gera email que envia o PDF e o XML
										// #########################################
										IList<CteEnvioXml> listaEnvioPdfXml = listaEnvio.Where(g => g.TipoArquivoEnvio == TipoArquivoEnvioEnum.PdfXml).ToList();
										emailPara = string.Empty;
										emailCopia = string.Empty;
										emailCopiaOculta = string.Empty;

										if (listaEnvioPdfXml.Count > 0)
										{
														// Envio de email
														client = new SmtpClient(emailServer, emailPort);
														listaEmailInvalido = new List<string>();
														mailMessage = new MailMessage();
														mailMessage.BodyEncoding = Encoding.UTF8;
														mailMessage.Subject = "Envio Cte - " + chave;
														mailMessage.From = new MailAddress(remetente);

														// Lista dos emails para ser enviado
														foreach (CteEnvioXml envio in listaEnvioPdfXml)
														{
																		if (!regexEmail.Match(envio.EnvioPara).Success)
																		{
																						listaEmailInvalido.Add(envio.EnvioPara);
																						continue;
																		}

																		if (envio.TipoEnvioCte == TipoEnvioCteEnum.Email)
																		{
																						mailMessage.To.Add(envio.EnvioPara);
																						emailPara += envio.EnvioPara + ";";
																		}
																		else if (envio.TipoEnvioCte == TipoEnvioCteEnum.EmailCopia)
																		{
																						mailMessage.CC.Add(envio.EnvioPara);
																						emailCopia += envio.EnvioPara + ";";
																		}
																		else
																		{
																						mailMessage.Bcc.Add(envio.EnvioPara);
																						emailCopiaOculta += envio.EnvioPara + ";";
																		}
														}

														// mailMessage.Body = GerarHtmlEmail(cte);
														html = AlternateView.CreateAlternateViewFromString(GerarHtmlEmail(cte), null, MediaTypeNames.Text.Html);
														logo = new LinkedResource(caminhoLogoAll, MediaTypeNames.Image.Jpeg);
														logo.ContentId = "Logo";
														html.LinkedResources.Add(logo);
														mailMessage.AlternateViews.Add(html);
														mailMessage.IsBodyHtml = true;

														// Anexar os arquivos
														mailMessage.Attachments.Add(new Attachment(arquivoPdf, string.Concat(chave, ".pdf")));
														mailMessage.Attachments.Add(new Attachment(arquivoXml, string.Concat(chave, ".xml")));

														if (listaEmailInvalido.Count > 0)
														{
																		client.Send(GerarEmailCentralFluxo(cte, remetente, listaEmailInvalido));
														}

														client.Send(mailMessage);
														// Insere na tabela histórica
														InserirHistoricoEnvioEmail(cteInterfaceEnvioEmail, emailPara, emailCopia, emailCopiaOculta, TipoArquivoEnvioEnum.PdfXml);
										}

										// #########################################
										// Gera email que envia o PDF
										// #########################################
										IList<CteEnvioXml> listaEnvioPdf = listaEnvio.Where(g => g.TipoArquivoEnvio == TipoArquivoEnvioEnum.Pdf).ToList();
										emailPara = string.Empty;
										emailCopia = string.Empty;
										emailCopiaOculta = string.Empty;

										if (listaEnvioPdf.Count > 0)
										{
														// Envio de email
														client = new SmtpClient(emailServer, emailPort);
														listaEmailInvalido = new List<string>();
														mailMessage = new MailMessage();
														mailMessage.BodyEncoding = Encoding.UTF8;
														mailMessage.Subject = "Envio Cte-" + chave;
														mailMessage.From = new MailAddress(remetente);

														// Lista dos emails para ser enviado
														foreach (CteEnvioXml envio in listaEnvioPdf)
														{
																		if (!regexEmail.Match(envio.EnvioPara).Success)
																		{
																						listaEmailInvalido.Add(envio.EnvioPara);
																						continue;
																		}

																		if (envio.TipoEnvioCte == TipoEnvioCteEnum.Email)
																		{
																						mailMessage.To.Add(envio.EnvioPara);
																						emailPara += envio.EnvioPara + ";";
																		}
																		else if (envio.TipoEnvioCte == TipoEnvioCteEnum.EmailCopia)
																		{
																						mailMessage.CC.Add(envio.EnvioPara);
																						emailCopia += envio.EnvioPara + ";";
																		}
																		else
																		{
																						mailMessage.Bcc.Add(envio.EnvioPara);
																						emailCopiaOculta += envio.EnvioPara + ";";
																		}
														}

														// mailMessage.Body = GerarHtmlEmail(cte);
														html = AlternateView.CreateAlternateViewFromString(GerarHtmlEmail(cte), null, MediaTypeNames.Text.Html);
														logo = new LinkedResource(caminhoLogoAll, MediaTypeNames.Image.Jpeg);
														logo.ContentId = "Logo";
														html.LinkedResources.Add(logo);
														mailMessage.AlternateViews.Add(html);
														mailMessage.IsBodyHtml = true;

														// Anexar os arquivos
														// mailMessage.Attachments.Add(new Attachment(arquivoPdf));

														// Anexar os arquivos
														mailMessage.Attachments.Add(new Attachment(arquivoPdf, string.Concat(chave, ".pdf")));

														if (listaEmailInvalido.Count > 0)
														{
																		client.Send(GerarEmailCentralFluxo(cte, remetente, listaEmailInvalido));
														}

														client.Send(mailMessage);
														// Insere na tabela histórica
														InserirHistoricoEnvioEmail(cteInterfaceEnvioEmail, emailPara, emailCopia, emailCopiaOculta, TipoArquivoEnvioEnum.Pdf);
										}

										// #########################################
										// Gera email que envia XML
										// #########################################
										IList<CteEnvioXml> listaEnvioXml = listaEnvio.Where(g => g.TipoArquivoEnvio == TipoArquivoEnvioEnum.Xml).ToList();
										emailPara = string.Empty;
										emailCopia = string.Empty;
										emailCopiaOculta = string.Empty;

										if (listaEnvioXml.Count > 0)
										{
														// Envio de email
														client = new SmtpClient(emailServer, emailPort);
														listaEmailInvalido = new List<string>();
														mailMessage = new MailMessage();
														mailMessage.BodyEncoding = Encoding.UTF8;
														mailMessage.Subject = "Envio Cte-" + chave;
														mailMessage.From = new MailAddress(remetente);

														// Lista dos emails para ser enviado
														foreach (CteEnvioXml envio in listaEnvioXml)
														{
																		if (!regexEmail.Match(envio.EnvioPara).Success)
																		{
																						listaEmailInvalido.Add(envio.EnvioPara);
																						continue;
																		}

																		if (envio.TipoEnvioCte == TipoEnvioCteEnum.Email)
																		{
																						mailMessage.To.Add(envio.EnvioPara);
																						emailPara += envio.EnvioPara + ";";
																		}
																		else if (envio.TipoEnvioCte == TipoEnvioCteEnum.EmailCopia)
																		{
																						mailMessage.CC.Add(envio.EnvioPara);
																						emailCopia += envio.EnvioPara + ";";
																		}
																		else
																		{
																						mailMessage.Bcc.Add(envio.EnvioPara);
																						emailCopiaOculta += envio.EnvioPara + ";";
																		}
														}

														// mailMessage.Body = GerarHtmlEmail(cte);
														html = AlternateView.CreateAlternateViewFromString(GerarHtmlEmail(cte), null, MediaTypeNames.Text.Html);
														logo = new LinkedResource(caminhoLogoAll, MediaTypeNames.Image.Jpeg);
														logo.ContentId = "Logo";
														html.LinkedResources.Add(logo);
														mailMessage.AlternateViews.Add(html);
														mailMessage.IsBodyHtml = true;

														// Anexar os arquivos
														// mailMessage.Attachments.Add(new Attachment(arquivoXml));
														mailMessage.Attachments.Add(new Attachment(arquivoXml, string.Concat(chave, ".xml")));

														if (listaEmailInvalido.Count > 0)
														{
																		client.Send(GerarEmailCentralFluxo(cte, remetente, listaEmailInvalido));
														}

														client.Send(mailMessage);

														// Insere na tabela histórica
														InserirHistoricoEnvioEmail(cteInterfaceEnvioEmail, emailPara, emailCopia, emailCopiaOculta, TipoArquivoEnvioEnum.Xml);
										}

										// Grava no status do Cte
										_cteStatusRepository.InserirCteStatus(cte, ObterUsuarioRobo(), new CteStatusRetorno { Id = 28 });

										// Remove o item da interface
										_cteInterfaceEnvioEmailRepository.Remover(cteInterfaceEnvioEmail);
						}
						catch (Exception ex)
						{
										_cteLogService.InserirLogErro(cte, "CteService", string.Format("EnviarEmail: Erro no envio de e-mail: {0}", ex.Message), ex);
						}
		}
		 */

		/// <summary>
		/// Gera e-mail para central de Fluxo
		/// </summary>
		/// <param name="cte">Cte que ocorreu o problema</param>
		/// <param name="remetente">remetende da mensagem</param>
		/// <param name="listaEmail">lista de e-mail inválidos</param>
		/// <returns>Retorna a mensagem para envio</returns>
		public MailMessage GerarEmailCentralFluxo(Cte cte, string remetente, List<string> listaEmail)
		{
			MailMessage mailMessage = null;
			StringBuilder mensagemBody;

			try
			{
				/*
				// Envio de email
				mailMessage = new MailMessage();
				mensagemBody = new StringBuilder();

				mailMessage.BodyEncoding = Encoding.UTF8;
				mailMessage.Subject = "CTe - Endereço de e-mail do tomador inválido.";
				mailMessage.From = new MailAddress(remetente);
				mailMessage.To.Add("central.fluxos@all-logistica.com");
				// mailMessage.CC.Add("robson.schmidt@all-logistica.com");

				mensagemBody.AppendLine("Verificar cadastro de e-mail do Tomador do Cte.");
				mensagemBody.AppendLine("<br/>Série: " + cte.Serie + " - Número: " + cte.Numero + " - UF: " + cte.SiglaUfFerrovia);
				mensagemBody.AppendLine("<br/><br/>E-mail(s) inválido(s): ");

				foreach (string email in listaEmail)
				{
												mensagemBody.AppendLine("<br/>" + email);
												_cteLogService.InserirLogErro(cte, "CteService", string.Format("GerarEmailCentralFluxo: Email invalido: {0}", email));
				}

				mailMessage.Body = mensagemBody.ToString();
				mailMessage.IsBodyHtml = true;
				 */

				foreach (string email in listaEmail)
				{
					_cteLogService.InserirLogErro(cte, "CteService", string.Format("GerarEmailCentralFluxo: Remetente: {0} Email invalido: {1}", remetente, email));
				}
			}
			catch (Exception ex)
			{
				_cteLogService.InserirLogErro(cte, "CteService", string.Format("EnviarEmailCentralFluxo: Erro no envio de e-mail: {0}", ex.Message), ex);
			}

			return mailMessage;
		}

		/// <summary>
		/// Grava as informações na tabela de envio de email
		/// </summary>
		/// <param name="cte">Cte de referência para gravar as informações</param>
		/// <param name="emailNfe">emails para ser enviado o arquivo NFE</param>
		/// <param name="emailXml">emails para ser enviado o arquivo PDF</param>
		public void GravarInformacoesEnvioEmail(Cte cte, string emailNfe, string emailXml)
		{
			try
			{
				string[] emailsNfe = ParserEmail(emailNfe);
				string[] emailsXml = ParserEmail(emailXml);

				// Remove itens por Cte
				_cteEnvioXmlRepository.RemoverPorCte(cte);

				// Cria a lista de emails que envia os arquivos Pdf e Xml
				// Encontra os emails comuns
				var emailsArquivoPdfXml = emailsNfe.Select(g => g.Trim()).Intersect(emailsXml.Select(s => s.Trim()));
				foreach (string email in emailsArquivoPdfXml)
				{
					CteEnvioXml envioXml = new CteEnvioXml();
					envioXml.Cte = cte;
					envioXml.EnvioPara = email;
					envioXml.TipoEnvioCte = TipoEnvioCteEnum.Email;
					envioXml.TipoArquivoEnvio = TipoArquivoEnvioEnum.PdfXml;
					envioXml.Ativo = true;
					_cteEnvioXmlRepository.Inserir(envioXml);
				}

				// Cria a lista de emails que envia apenas o pdf
				var emailsArquivoPdf = emailsNfe.Except(emailsXml);
				foreach (string email in emailsArquivoPdf)
				{
					CteEnvioXml envioXml = new CteEnvioXml();
					envioXml.Cte = cte;
					envioXml.EnvioPara = email;
					envioXml.TipoEnvioCte = TipoEnvioCteEnum.Email;
					envioXml.TipoArquivoEnvio = TipoArquivoEnvioEnum.Pdf;
					envioXml.Ativo = true;
					_cteEnvioXmlRepository.Inserir(envioXml);
				}

				// Cria a lista de emails que envia apenas o pdf
				var emailsArquivoXml = emailsXml.Except(emailsNfe);
				foreach (string email in emailsArquivoXml)
				{
					CteEnvioXml envioXml = new CteEnvioXml();
					envioXml.Cte = cte;
					envioXml.EnvioPara = email;
					envioXml.TipoEnvioCte = TipoEnvioCteEnum.Email;
					envioXml.TipoArquivoEnvio = TipoArquivoEnvioEnum.Xml;
					envioXml.Ativo = true;
					_cteEnvioXmlRepository.Inserir(envioXml);
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		/// <summary>
		/// Obtem o arquivo cte
		/// </summary>
		/// <param name="cteId">Codigo do Cte a ser localizado</param>
		/// <returns>Retorna o arquivo cte</returns>
		public CteArquivo ObterArquivoCte(int cteId)
		{
			return _cteArquivoRepository.ObterPorId(cteId);
		}

		/// <summary>
		/// Verifica se o cliente correntista do fluxo é para liberar as travas
		/// </summary>
		/// <param name="fluxo">Fluxo comercial</param>
		/// <returns>Valor booleano</returns>
		public bool VerificarLiberacaoTravasCorrentista(FluxoComercial fluxo)
		{
			if (fluxo.Codigo == "99999")
			{
				return false;
			}

			if (fluxo.Contrato.EmpresaPagadora.DescricaoResumida.ToUpperInvariant().StartsWith("BRADO")
				&& fluxo.Mercadoria.UnidadeMedida.Id.Equals("CON")
				&& fluxo.ModeloNotaFiscal.Equals("55"))
			{
				return true;
			}

			return false;
		}

		/// <summary>
		/// Pelo fluxo comercial verifica se é conteiner da Brado
		/// </summary>
		/// <param name="fluxo">Fluxo comercial de referência</param>
		/// <returns>Verdadeiro caso seja Brado, senão retorna falso</returns>
		public bool VerificarConteinerBrado(FluxoComercial fluxo)
		{
			if (fluxo.Contrato.EmpresaPagadora.DescricaoResumida.ToUpperInvariant().StartsWith("BRADO")
																																																																																																																																																																																																																																																																			&& fluxo.Mercadoria.UnidadeMedida.Id.Equals("CON"))
			{
				return true;
			}

			return false;
		}

		/// <summary>
		/// Obtem o diretório dos arquivos processados
		/// </summary>
		/// <param name="pathInicial">Caminho Inicial</param>
		/// <param name="tipoArquivo">Tipo do arquivo</param>
		/// <param name="dataCte">Data do Cte</param>
		/// <returns>Retorna caminho</returns>
		public string ObterDiretorioProcessado(string pathInicial, TipoArquivoEnvioEnum tipoArquivo, DateTime dataCte)
		{
			string nomeDiretorio = string.Empty;
			if (tipoArquivo == TipoArquivoEnvioEnum.Pdf)
			{
				nomeDiretorio = pathInicial + @"PDF_PROCESSADOS\";
			}
			else if (tipoArquivo == TipoArquivoEnvioEnum.Xml)
			{
				nomeDiretorio = pathInicial + @"XML_PROCESSADOS\";
			}
			else
			{
				nomeDiretorio = pathInicial + @"PROCESSADOS\";
			}

			nomeDiretorio += String.Format("{0:yyyyMM}", dataCte);
			nomeDiretorio += @"\";

			if (!Directory.Exists(nomeDiretorio))
			{
				Directory.CreateDirectory(nomeDiretorio);
			}

			return nomeDiretorio;
		}

		/// <summary>
		/// Retorna todos os Email do cte
		/// </summary>
		/// <param name="idCte">Id do Cte para retorno do status</param>
		/// <returns>lista de Cte para envio de xml</returns>
		public IList<ReenvioEmailDto> RetornaEmailReenvioArquivo(int idCte)
		{
			// Cte cte = _cteRepository.ObterPorId(idCte);
			return _cteEnvioXmlRepository.ObterPorCte(idCte);
		}

		/// <summary>
		/// Retorna todos os Cte Autorizados para listagem de Impressão
		/// </summary>
		/// <param name="pagination"> Paginacao pesquisa</param>
		/// <param name="filtros"> Filtros para pesquisa</param>
		/// <returns>lista de Cte</returns>
		public ResultadoPaginado<CteDto> RetornaCteReenvioArquivo(DetalhesPaginacaoWeb pagination, DetalhesFiltro<object>[] filtros)
		{
			DateTime dataIni = DateTime.Now;
			DateTime dataFim = DateTime.Now;
			string fluxo = string.Empty;
			string email = string.Empty;
			bool emailErro = false;
			string serie = string.Empty;
			string destino = string.Empty;
			string origem = string.Empty;
			string numVagao = string.Empty;
			string codigoUfDcl = string.Empty;
			int numDespacho = 0;
			string numeroCte = string.Empty;
			string[] chaveCte = null;

			if (filtros != null)
			{
				foreach (var detalheFiltro in filtros)
				{
					if (detalheFiltro.Campo.Equals("dataInicial") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						string texto = detalheFiltro.Valor[0].ToString();

						dataIni = DateTime.ParseExact(texto, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
					}

					if (detalheFiltro.Campo.Equals("dataFinal") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						string texto = detalheFiltro.Valor[0].ToString();

						dataFim = DateTime.ParseExact(texto, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);

						dataFim = dataFim.AddDays(1).AddSeconds(-1);
					}

					if (detalheFiltro.Campo.Equals("serie") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						serie = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
					}

					if (detalheFiltro.Campo.Equals("despacho") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						numDespacho = Convert.ToInt32(detalheFiltro.Valor[0].ToString());
					}

					if (detalheFiltro.Campo.Equals("fluxo") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						fluxo = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
					}

					if (detalheFiltro.Campo.Equals("Origem") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						origem = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
					}

					if (detalheFiltro.Campo.Equals("UfDcl") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						codigoUfDcl = detalheFiltro.Valor[0].ToString().ToUpperInvariant().Trim();
					}

					if (detalheFiltro.Campo.Equals("Destino") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						destino = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
					}

					if (detalheFiltro.Campo.Equals("Vagao") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						numVagao = detalheFiltro.Valor[0].ToString();
					}

					if (detalheFiltro.Campo.Equals("email") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						email = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
					}

					if (detalheFiltro.Campo.Equals("emailErro") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						emailErro = bool.Parse(detalheFiltro.Valor[0].ToString());
					}

					if (detalheFiltro.Campo.Equals("NroCte") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						numeroCte = detalheFiltro.Valor[0].ToString().PadLeft(9, '0');
					}

					if (detalheFiltro.Campo.Equals("chave") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
					{
						chaveCte = detalheFiltro.Valor[0].ToString().Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
					}
				}
			}

			ResultadoPaginado<CteDto> listaDto = new ResultadoPaginado<CteDto>();

			listaDto = _cteRepository.ObterCteReenvioEmail(pagination, dataIni, dataFim, fluxo, email, emailErro, serie, numDespacho, destino, origem, numVagao, codigoUfDcl, numeroCte, chaveCte);
			listaDto.Items = listaDto.Items.OrderByDescending(c => c.Numero).ToList();
			IEnumerable<CteDto> lst = listaDto.Items.Distinct();

			return listaDto;
		}

		/// <summary>
		/// Efetua o reenvio de e-mail
		/// </summary>
		/// <param name="idCtes">Id dos Ctes que serão reenviados</param>
		public void ReenviarEmailporCte(int[] idCtes)
		{
			// Remove o CTe do log de erro
			_cteEnvioEmailErroRepository.RemoverTodosPorCte(idCtes);

			// Obtem a lista de Cte do Repositório
			IList<CteEnvioXml> listaCte = _cteEnvioXmlRepository.ObterTodosPorCte(idCtes);

			// Percorre a lista e inseri no pooling
			foreach (CteEnvioXml cteEnvioXml in listaCte)
			{
				cteEnvioXml.Ativo = true;
				_cteEnvioXmlRepository.Atualizar(cteEnvioXml);
			}

			ConfiguracaoTranslogic hostReenvio = _configuracaoTranslogicRepository.ObterPorId(_chaveHostReenvioEmail);
			foreach (var idCte in idCtes)
			{
				var cteInterfaceEnvioEmail = _cteInterfaceEnvioEmailRepository.ObterInterfacePorCte(idCte);

				if (cteInterfaceEnvioEmail == null)
				{
					Cte cte = _cteRepository.ObterPorId(idCte);
					CteInterfaceEnvioEmail interfaceEnvioEmail = new CteInterfaceEnvioEmail();
					interfaceEnvioEmail.Cte = cte;
					interfaceEnvioEmail.Chave = cte.Chave;
					interfaceEnvioEmail.DataHora = DateTime.Now;
					interfaceEnvioEmail.DataUltimaLeitura = DateTime.Now;
					interfaceEnvioEmail.HostName = hostReenvio.Valor;
					_cteInterfaceEnvioEmailRepository.Inserir(interfaceEnvioEmail);
				}
			}
		}

		/// <summary>
		/// Retorna os Estados da Serie Desp Uf
		/// </summary>
		/// <returns>Lista de estados da Serie Desp Uf</returns>
		public IList<string> ObterEstadosSerieDespUf()
		{
			IList<SerieDespachoUf> retorno = _serieDespachoUfRepository.ObterTodos();
			return retorno.Select(e => e.CodUnidadeFederativaFerrovia).Distinct().ToList();
		}

		/// <summary>
		/// Salva o novo comunicado
		/// </summary>
		/// <param name="arquivo">Comunicado a ser salvo</param>
		/// <param name="uf"> Estado do comunicado</param>
		[Transaction]
		public virtual void SalvarComunicado(Stream arquivo, string uf)
		{
			// Inativa o CteComunicado Anterior
			CteComunicado comunicadoAnterior = _cteComunicadoRepository.ObterPorSiglaUf(uf);

			if (comunicadoAnterior != null)
			{
				comunicadoAnterior.Ativo = false;
				_cteComunicadoRepository.Atualizar(comunicadoAnterior);
			}

			// Salva o novo comunicado
			CteComunicado cteComunicado = new CteComunicado();
			cteComunicado.ComunicadoPdf = Tools.ConverterStreamToArray(arquivo);
			cteComunicado.SiglaUf = uf.ToUpperInvariant();
			cteComunicado.Ativo = true;
			cteComunicado.DataCadastro = DateTime.Now;
			_cteComunicadoRepository.Inserir(cteComunicado);
		}

		/// <summary>
		/// Retorno a lista de Comunicados Ativo
		/// </summary>
		/// <returns> lista de Comunicados Ativo</returns>
		public IList<CteComunicado> ObterComunicados()
		{
			return _cteComunicadoRepository.ObterTodosAtivos();
		}

		/// <summary>
		/// Apaga o Comunicados
		/// </summary>
		/// <param name="idCteComunicado">Id do comunicado a ser excluido</param>
		public void ExcluirComunicado(int idCteComunicado)
		{
			// Inativa o CteComunicado Anterior
			CteComunicado comunicadoAnterior = _cteComunicadoRepository.ObterPorId(idCteComunicado);

			if (comunicadoAnterior != null)
			{
				comunicadoAnterior.Ativo = false;
				_cteComunicadoRepository.Atualizar(comunicadoAnterior);
			}
		}

		/// <summary>
		/// Efetua o reenvio de e-mail
		/// </summary>
		/// <param name="idXmlEnvio">Id dos Xml de Envio</param>
		/// <param name="tipoEnvio"> Tipo de envio de e-mail</param>
		public void ReenviarEmail(int idXmlEnvio, TipoArquivoEnvioEnum tipoEnvio)
		{
			// Obtem a lista de Cte do Repositório
			CteEnvioXml cteEnvioXml = _cteEnvioXmlRepository.ObterPorId(idXmlEnvio);
			IList<CteEnvioXml> listaXmlEnvio = _cteEnvioXmlRepository.ObterTodosPorCte(cteEnvioXml.Cte);

			foreach (CteEnvioXml envioXml in listaXmlEnvio)
			{
				if (envioXml.Id == idXmlEnvio)
				{
					envioXml.Ativo = true;
				}
				else
				{
					envioXml.Ativo = false;
				}
				// 874383

				_cteEnvioXmlRepository.Atualizar(cteEnvioXml);
			}

			EnviarEmail(idXmlEnvio, tipoEnvio);
		}

		/// <summary>
		/// Exclui o e-mail do cadastro do cte
		/// </summary>
		/// <param name="idXmlEnvio">Id dos Xml de Envio</param>
		public void ExcluirEmailCteReenvioArquivo(int idXmlEnvio)
		{
			CteEnvioXml xmlEnvio = _cteEnvioXmlRepository.ObterPorId(idXmlEnvio);

			if (xmlEnvio == null)
			{
				throw new Exception("Não foi localizado o e-mail para exclusão");
			}

			_cteEnvioXmlRepository.Remover(xmlEnvio);
		}

		/// <summary>
		/// Obtém os motivos de cancelamento ativos
		/// </summary>
		/// <returns>Lista de motivos de cancelamento</returns>
		public IList<CteMotivoCancelamento> ObterMotivosCancelamentoAtivos()
		{
			return _cteMotivoCancelamentoRepository.ObterAtivos();
		}

		/// <summary>
		/// Obtem o usuário do Robo
		/// </summary>
		/// <returns>Retorna o usuário do Robo</returns>
		public Usuario ObterUsuarioRobo()
		{
			ConfiguracaoTranslogic usuarioRobo = _configuracaoTranslogicRepository.ObterPorId(_chaveCteRobo);
			return _usuarioRepository.ObterPorId(Convert.ToInt32(usuarioRobo.Valor));
		}

		private string[] ParserEmail(string emails)
		{
			string mask = ";,: ";
			return emails.Split(mask.ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
		}

		/// <summary>
		/// Verifica se houveram alterações no Cte de manutenção com o original
		/// </summary>
		/// <param name="dto">Dto de manutenção</param>
		/// <returns>True se houve alteração</returns>
		private bool VerificarExisteAlteracaoParaManutencao(ManutencaoCteTempDto dto)
		{
			Cte cteOriginal = _cteRepository.ObterPorId(dto.CteTemp.IdCteOrigem.Value);
			IList<CteDetalhe> listaDetalhesOriginais = _cteDetalheRepository.ObterPorCte(cteOriginal);

			CteTemp cteAlterado = dto.CteTemp;
			IList<CteTempDetalheViewModel> listaDetalhesAlterados = dto.ListaDetalhes;

			bool alteradoCte = VerificarAlteracaoCte(cteOriginal, cteAlterado);
			if (alteradoCte)
			{
				return true;
			}

			return VerificarAlteracaoDetalhesCte(listaDetalhesOriginais, listaDetalhesAlterados);
		}

		private bool VerificarAlteracaoCte(Cte cteOriginal, CteTemp cteAlterado)
		{
			if (!cteOriginal.Vagao.Id.Equals(cteAlterado.Id))
			{
				return true;
			}

			if (!cteOriginal.FluxoComercial.Id.Equals(cteAlterado.IdFluxoComercial))
			{
				return true;
			}

			if (!cteOriginal.PesoVagao.Equals(cteAlterado.PesoVagao))
			{
				return true;
			}

			if (!cteAlterado.VolumeVagao.HasValue)
			{
				cteAlterado.VolumeVagao = 0;
			}

			if (!cteOriginal.VolumeVagao.Equals(cteAlterado.VolumeVagao.Value))
			{
				return true;
			}

			if (!cteAlterado.TaraVagao.HasValue)
			{
				cteAlterado.TaraVagao = 0;
			}

			if (!cteOriginal.TaraVagao.Equals(cteAlterado.TaraVagao.Value))
			{
				return true;
			}

			return false;
		}

		private bool VerificarAlteracaoDetalhesCte(IList<CteDetalhe> listaDetalhesOriginais, IList<CteTempDetalheViewModel> listaDetalhesAlterados)
		{
			if (listaDetalhesAlterados.Count != listaDetalhesOriginais.Count)
			{
				return true;
			}

			foreach (CteDetalhe detalheOriginal in listaDetalhesOriginais)
			{
				CteTempDetalheViewModel alterado =
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																				listaDetalhesAlterados.FirstOrDefault(
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																				c =>
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																				c.SerieNotaFiscal.Equals(detalheOriginal.SerieNota) &&
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																				c.NumeroNotaFiscal.Equals(detalheOriginal.NumeroNota) &&
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																				c.ChaveNfe.Equals(detalheOriginal.ChaveNfe));
				if (alterado == null)
				{
					return true;
				}

				if (VerificarAlteracaoDetalheCte(detalheOriginal, alterado))
				{
					return true;
				}
			}

			return false;
		}

		private bool VerificarAlteracaoDetalheCte(CteDetalhe detalheOriginal, CteTempDetalheViewModel alterado)
		{
			if (!detalheOriginal.PesoNotaFiscal.Equals(alterado.PesoNotaFiscal))
			{
				return true;
			}

			if (!alterado.VolumeNotaFiscal.HasValue)
			{
				alterado.VolumeNotaFiscal = 0;
			}

			if (!detalheOriginal.VolumeNotaFiscal.Equals(alterado.VolumeNotaFiscal))
			{
				return true;
			}

			if (detalheOriginal.ConteinerNotaFiscal != alterado.ConteinerNotaFiscal)
			{
				return true;
			}

			if (detalheOriginal.NumeroTif != alterado.NumeroTif)
			{
				return true;
			}

			if (detalheOriginal.ValorNotaFiscal != alterado.ValorNotaFiscal)
			{
				return true;
			}

			if (detalheOriginal.ValorTotalNotaFiscal != alterado.ValorTotalNotaFiscal)
			{
				return true;
			}

			return false;
		}

		/// <summary>
		/// Verifica se na lista já contem o cte
		/// </summary>
		/// <param name="lista">lista de Dto Cte que será analisada</param>
		/// <param name="tipo">tipo da lista</param>
		/// <param name="cteId">id do cte que irá ser procurado na lista</param>
		/// <returns>retorna verdadeiro se o dto cte já estiver na lista</returns>
		private bool ContainCteDto(IList lista, Type tipo, int cteId)
		{
			if (tipo == typeof(IList<CteDto>))
			{
				IList<CteDto> listaTmp = (IList<CteDto>)lista;
				return listaTmp.Any(cteDto => cteDto.CteId == cteId);
			}
			else if (tipo == typeof(IList<ManutencaoCteDto>))
			{
				IList<ManutencaoCteDto> listaTmp = (IList<ManutencaoCteDto>)lista;
				return listaTmp.Any(cteDto => cteDto.CteId == cteId);
			}

			return false;
		}

		/// <summary>
		/// Processa o status de retorno conforme o codigo de retorno da SEFAZ
		/// </summary>
		/// <param name="cte">CTE que está sendo processado</param>
		/// <param name="cteStatusRetorno">Status de retorno do CTE</param>
		/// <param name="consultaCte">Retorno da config</param>
		private void ProcessarCteStatusRetorno(Cte cte, CteStatusRetorno cteStatusRetorno, Vw209ConsultaCte consultaCte)
		{
			// Insere no log do cte
			_cteLogService.InserirLogInfo(cte, "CteService", string.Format("Status de retorno do Cte: {0}", cteStatusRetorno.Id));

			if (cteStatusRetorno.Id == 100)
			{
				// Item da fila de processamento (utilizado para pegar o XML de retorno)
				Cte01Lista retornoConfig = _filaProcessamentoService.ObterItemFilaProcessamentoPorConsultaRetorno(consultaCte);

				// Cte Autorizado
				cte.SituacaoAtual = SituacaoCteEnum.Autorizado;
				cte.XmlAutorizacao = retornoConfig.XmlRetorno;
			}
			else if (cteStatusRetorno.Id == 101)
			{
				// Item da fila de processamento (utilizado para pegar o XML de retorno)
				Cte01Lista retornoConfig = _filaProcessamentoService.ObterItemFilaProcessamentoPorConsultaRetorno(consultaCte);

				// Cancelamento de CT-e homologado
				cte.SituacaoAtual = SituacaoCteEnum.AutorizadoCancelamento;
				cte.XmlCancelamento = retornoConfig.XmlRetorno;
			}
			else if (cteStatusRetorno.Id == 102)
			{
				// Item da fila de processamento (utilizado para pegar o XML de retorno)
				Cte01Lista retornoConfig = _filaProcessamentoService.ObterItemFilaProcessamentoPorConsultaRetorno(consultaCte);

				// Inutilização de número homologado
				// cte.SituacaoAtual = SituacaoCteEnum.AutorizadoInutilizacao;
				cte.XmlInutilizacao = retornoConfig.XmlRetorno;
				cte.SituacaoAtual = SituacaoCteEnum.Invalidado;
			}
			else if ((cteStatusRetorno.Id >= 103) && (cteStatusRetorno.Id <= 109))
			{
				// Sefaz processando o Lote (Nenhuma ação)
			}
			else if (cteStatusRetorno.Id == 110)
			{
				cte.SituacaoAtual = SituacaoCteEnum.UsoDenegado;
			}
			else if ((cteStatusRetorno.Id >= 111) && (cteStatusRetorno.Id <= 199))
			{
				cte.SituacaoAtual = SituacaoCteEnum.Erro;
			}
			else if ((cteStatusRetorno.Id >= 200) && (cteStatusRetorno.Id <= 299))
			{
				// Rejeição
				if (cteStatusRetorno.PermiteReenvio)
				{
					cte.SituacaoAtual = SituacaoCteEnum.ErroAutorizadoReEnvio;
				}
				else
				{
					cte.SituacaoAtual = SituacaoCteEnum.Erro;
				}
			}
			else if ((cteStatusRetorno.Id >= 301) && (cteStatusRetorno.Id <= 306))
			{
				cte.SituacaoAtual = SituacaoCteEnum.UsoDenegado;
			}
			else if ((cteStatusRetorno.Id >= 401) && (cteStatusRetorno.Id <= 999))
			{
				// Rejeição
				if (cteStatusRetorno.PermiteReenvio)
				{
					cte.SituacaoAtual = SituacaoCteEnum.ErroAutorizadoReEnvio;
				}
				else
				{
					cte.SituacaoAtual = SituacaoCteEnum.Erro;
				}
			}
			else if (cteStatusRetorno.Id == 2011)
			{
				// Verifica se excedeu o numero máximo de tentativas 
				if (ValidarTentativasExcedentesPorStatusRetorno(cte, 2011))
				{
					cte.SituacaoAtual = SituacaoCteEnum.ErroAutorizadoReEnvio;
					_cteStatusRepository.InserirCteStatus(cte, ObterUsuarioRobo(), new CteStatusRetorno { Id = 14 }, cteStatusRetorno.Descricao);
				}
				else
				{
					// Erro Retornado pela Config quando a Sefaz está OFF, então é enviado novamente até que a Sefaz volte.
					if (cteStatusRetorno.PermiteReenvio)
					{
						// Se o Reenvio estiver habilitado, então coloca o arquivo na fila novamente.
						cte.SituacaoAtual = SituacaoCteEnum.PendenteArquivoEnvio;
						_cteStatusRepository.InserirCteStatus(cte, ObterUsuarioRobo(), new CteStatusRetorno { Id = 26 });
					}
					else
					{
						cte.SituacaoAtual = SituacaoCteEnum.Erro;
					}
				}
			}
			else if (cteStatusRetorno.Id == 4001)
			{
				// Erro Log (utilizado para pegar a descrição completa do erro)
				Nfe20ErroLog erroLog = _filaProcessamentoService.ObterLogErroPorConsultaRetorno(consultaCte);

				cte.SituacaoAtual = SituacaoCteEnum.ErroAutorizadoReEnvio;
				if (erroLog != null)
				{
					cte.XmlAutorizacao = erroLog.DescricaoCompleta;
				}
			}
			else if (cteStatusRetorno.Id >= 1000)
			{
				if (cteStatusRetorno.PermiteReenvio)
				{
					cte.SituacaoAtual = SituacaoCteEnum.ErroAutorizadoReEnvio;
				}
				else
				{
					cte.SituacaoAtual = SituacaoCteEnum.Erro;
				}
			}

			// Regra cancelamento 220
			// Caso seja um cancelamento rejeitado pela Sefaz. Deverá ser enviado as informações para o SAP
			// como "CR" - Cancelamento Rejeitado e mudar o Status do CT-e para cancelado
			if (cteStatusRetorno.Id == 220)
			{
				_cteStatusRepository.InserirCteStatus(cte, ObterUsuarioRobo(), new CteStatusRetorno { Id = 30 });
				cte.SituacaoAtual = SituacaoCteEnum.Cancelado;

				// Insere na interface de envio para o SAP
				InserirInterfaceEnvioSap(cte, cteStatusRetorno);
			}

			// Insere no log do cte
			_cteLogService.InserirLogInfo(cte, "CteService", string.Format("Atualizando a situação atual do cte para: {0}", cte.SituacaoAtual));
			_cteRepository.InserirOuAtualizar(cte);
		}

		private void InserirInterfaceRecebimentoConfig(Cte cte, int idListaConfig)
		{
			int numeroCte = 0;
			int.TryParse(cte.Numero, out numeroCte);

			CteInterfaceRecebimentoConfig interfaceConfig = new CteInterfaceRecebimentoConfig();
			interfaceConfig.Cte = cte;
			interfaceConfig.IdListaConfig = idListaConfig;
			interfaceConfig.CfgUn = ObterIdentificadorFilial(cte);
			interfaceConfig.CfgDoc = numeroCte;
			interfaceConfig.CfgSerie = cte.Serie;
			interfaceConfig.DataHora = DateTime.Now;
			interfaceConfig.DataUltimaLeitura = DateTime.Now;

			_cteInterfaceRecebimentoConfigRepository.Inserir(interfaceConfig);
		}

		private int ObterIdentificadorFilial(Cte cte)
		{
			int idFilial = 0;
			if (cte.FluxoComercial != null)
			{
				idFilial = cte.FluxoComercial.Origem.EmpresaOperadora.Id ?? 0;
			}

			if (idFilial == 0)
			{
				ConfiguracaoTranslogic filial = _configuracaoTranslogicRepository.ObterPorId(_chaveCteCodigoInternoFilial);
				idFilial = System.Convert.ToInt32(filial.Valor);
			}

			return idFilial;
		}

		private void InserirInterfaceArquivoPdf(Cte cte)
		{
			string hostName = Dns.GetHostName();
			CteInterfacePdfConfig interfacePdf = new CteInterfacePdfConfig();
			interfacePdf.Cte = cte;
			interfacePdf.Chave = cte.Chave;
			interfacePdf.HostName = hostName;
			interfacePdf.DataHora = DateTime.Now;
			interfacePdf.DataUltimaLeitura = DateTime.Now;

			_cteInterfacePdfRepository.Inserir(interfacePdf);
		}

		private void InserirInterfaceArquivoXml(Cte cte)
		{
			string hostName = Dns.GetHostName();
			CteInterfaceXmlConfig interfaceXml = new CteInterfaceXmlConfig();
			interfaceXml.Cte = cte;
			interfaceXml.Chave = cte.Chave;
			interfaceXml.HostName = hostName;
			interfaceXml.DataHora = DateTime.Now;
			interfaceXml.DataUltimaLeitura = DateTime.Now;

			_cteInterfaceXmlRepository.Inserir(interfaceXml);
		}

		private void InserirInterfaceEnvioSap(Cte cte, CteStatusRetorno cteStatusRetorno)
		{
			try
			{
				TipoOperacaoCteEnum tipoOperacao = TipoOperacaoCteEnum.Invalida;

				/*  
					I=Inclusão, 
					E=Exclusão (CANCELADO AUTORIZADO), 
					C=Complemento, IN=Inutilizado, 
					CR=Cancelamento Rejeitado código do SEFAZ 220,
					D=Denegado 
				 */
				switch (cte.SituacaoAtual)
				{
					case SituacaoCteEnum.Autorizado:
						if (cte.Complementar)
						{
							tipoOperacao = TipoOperacaoCteEnum.Complemento;
						}
						else
						{
							tipoOperacao = TipoOperacaoCteEnum.Inclusao;
						}

						break;
					case SituacaoCteEnum.Cancelado:
						if (cteStatusRetorno.Id == 220)
						{
							tipoOperacao = TipoOperacaoCteEnum.CancelamentoRejeitado;
						}
						else
						{
							tipoOperacao = TipoOperacaoCteEnum.Exclusao;
						}

						break;
					case SituacaoCteEnum.EnviadoArquivoComplemento:
						tipoOperacao = TipoOperacaoCteEnum.Complemento;
						break;
					case SituacaoCteEnum.Erro:
						if (cteStatusRetorno.Id == 220)
						{
							tipoOperacao = TipoOperacaoCteEnum.CancelamentoRejeitado;
						}

						break;
					case SituacaoCteEnum.UsoDenegado:
						tipoOperacao = TipoOperacaoCteEnum.Denegado;
						break;
				}

				CteInterfaceEnvioSap interfaceEnvioSap = new CteInterfaceEnvioSap();
				interfaceEnvioSap.Cte = cte;
				interfaceEnvioSap.DataGravacaoRetorno = DateTime.Now;
				interfaceEnvioSap.DataUltimaLeitura = DateTime.Now;
				interfaceEnvioSap.TipoOperacao = tipoOperacao;
				interfaceEnvioSap.DataEnvioSap = null;

				_cteInterfaceEnvioSapRepository.Inserir(interfaceEnvioSap);
			}
			catch (Exception ex)
			{
				_cteLogService.InserirLogErro(cte, "CteService", string.Format("Erro na geração dos dados para o SAP em InserirInterfaceEnvioSap: {0}", ex.Message), ex);
			}
		}

		private void InserirInterfaceEnvioEmail(Cte cte)
		{
			CteArquivo cteArquivo = _cteArquivoRepository.ObterPorId(cte.Id.Value);

			try
			{
				if ((cteArquivo != null) && (cteArquivo.TamanhoPdf != null) && (cteArquivo.TamanhoXml != null))
				{
					CteInterfaceEnvioEmail interfaceEnvioEmail = new CteInterfaceEnvioEmail();
					interfaceEnvioEmail.Cte = cte;
					interfaceEnvioEmail.Chave = cte.Chave;
					interfaceEnvioEmail.DataHora = DateTime.Now;
					interfaceEnvioEmail.DataUltimaLeitura = DateTime.Now;
					interfaceEnvioEmail.HostName = Dns.GetHostName();
					_cteInterfaceEnvioEmailRepository.Inserir(interfaceEnvioEmail);
				}
			}
			catch (Exception)
			{
			}
		}

		private bool ValidarTentativasExcedentesPorStatusRetorno(Cte cte, int codigoStatusRetorno)
		{
			try
			{
				int numeroMaximoTentativas = 0;
				int numeroAtual = 0;
				ConfiguracaoTranslogic configuracao = _configuracaoTranslogicRepository.ObterPorId(_chaveCteTentativaReenvio);
				if (configuracao != null)
				{
					IList<CteStatus> listaAux = _cteStatusRepository.ObterPorCte(cte);

					numeroMaximoTentativas = Convert.ToInt32(configuracao.Valor);
					numeroAtual = listaAux.Where(g => g.CteStatusRetorno.Id == codigoStatusRetorno).Count();
					if (numeroAtual >= numeroMaximoTentativas)
					{
						return true;
					}
				}

				return false;
			}
			catch (Exception ex)
			{
				return false;
			}
		}

		private void InserirHistoricoEnvioEmail(Cte envioEmail, string emailPara, string emailCopia, string emailCopiaOculta, TipoArquivoEnvioEnum tipoArquivoEnvio)
		{
			try
			{
				CteEnvioEmailHistorico envioEmailHistorico = new CteEnvioEmailHistorico();
				envioEmailHistorico.Cte = envioEmail;
				envioEmailHistorico.EmailPara = emailPara;
				envioEmailHistorico.EmailCopia = emailCopia;
				envioEmailHistorico.EmailCopiaOculta = emailCopiaOculta;
				envioEmailHistorico.TipoArquivoEnvio = tipoArquivoEnvio;
				envioEmailHistorico.DataHora = DateTime.Now;
				_cteEnvioEmailHistoricoRepository.Inserir(envioEmailHistorico);
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		private void InserirEnvioEmailErro(Cte envioEmail, string emailPara, string emailCopia, string emailCopiaOculta, string descricaoErro, TipoArquivoEnvioEnum tipoArquivoEnvio)
		{
			try
			{
				CteEnvioEmailErro envioEmailErro = new CteEnvioEmailErro();
				envioEmailErro.Cte = envioEmail;
				envioEmailErro.EmailPara = emailPara;
				envioEmailErro.EmailCopia = emailCopia;
				envioEmailErro.EmailCopiaOculta = emailCopiaOculta;
				envioEmailErro.DescricaoErro = descricaoErro;
				envioEmailErro.TipoArquivoEnvio = tipoArquivoEnvio;
				envioEmailErro.DataHora = DateTime.Now;
				envioEmailErro.HostName = Dns.GetHostName();
				_cteEnvioEmailErroRepository.Inserir(envioEmailErro);
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		private string GerarHtmlEmail(Cte cte)
		{
			string conteudo = string.Empty;
			long cnpj;
			Int64.TryParse(cte.CnpjFerrovia, out cnpj);

			Nfe03Filial filial = _nfe03FilialRepository.ObterFilialPorCnpj(cnpj);

			// StringBuilder buider = new StringBuilder();
			// string buffer = string.Empty;

			string arquivo = Directory.GetCurrentDirectory() + "\\" + "GenericoCTe.htm";
			string valor;
			using (StreamReader reader = new StreamReader(arquivo))
			{
				// string line = string.Empty;
				conteudo = reader.ReadToEnd();
				conteudo = conteudo.Replace("#PROTOCOLO#", cte.NumeroProtocolo);
				conteudo = conteudo.Replace("#NUMERO_NFE#", cte.Numero);
				conteudo = conteudo.Replace("#SERIE_NFE#", cte.Serie);
				conteudo = conteudo.Replace("#RAZ_SOC#", filial.RazSoc);
			}

			return conteudo;
		}

		private MemoryStream CreateToMemoryStream(Dictionary<string, Stream> arquivos)
		{
			MemoryStream outputMemStream = new MemoryStream();
			ZipOutputStream zipStream = new ZipOutputStream(outputMemStream);

			zipStream.SetLevel(9); // 0-9, 9 being the highest level of compression

			foreach (KeyValuePair<string, Stream> arquivo in arquivos)
			{
				ZipEntry newEntry = new ZipEntry(arquivo.Key);
				newEntry.DateTime = DateTime.Now;

				zipStream.PutNextEntry(newEntry);
				StreamUtils.Copy(arquivo.Value, zipStream, new byte[4096]);
				zipStream.CloseEntry();
			}

			zipStream.IsStreamOwner = false;	// False stops the Close also Closing the underlying stream.
			zipStream.Close();			// Must finish the ZipOutputStream before using outputMemStream.

			outputMemStream.Position = 0;
			return outputMemStream;
		}

		private Stream ObterArquivoPdf(string[] arrIds)
		{
			// Instância de objeto
			PdfMerge merge = new PdfMerge();
			CteArquivo file;

			// percorre os Ctes
			foreach (string id in arrIds)
			{
				// Obtem o Arquivo
				file = ObterArquivoCte(int.Parse(id));

				// Se possuir arquio na base, então faz o merge do Pdf
				if (file != null)
				{
					if (file.ArquivoPdf != null)
					{
						merge.AddFile(file.ArquivoPdf);
					}
				}
			}

			return merge.Execute();
		}

		private void AtualizarStatusCteManutInvalida(int idCte)
		{
			Cte cteOriginal = _cteRepository.ObterPorId(idCte);

			if (cteOriginal.SituacaoAtual == SituacaoCteEnum.ErroAutorizadoReEnvio)
			{
				cteOriginal.SituacaoAtual = SituacaoCteEnum.Invalidado;
				_cteRepository.Atualizar(cteOriginal);
			}
		}

		/// <summary>
		/// Função para verificar se o Cte está no prazo de cancelamento
		/// </summary>
		/// <param name="dataCte">Data do Cte</param>
		/// <param name="usuario"> Usuario que executou </param>
		/// <returns>Retorna true se puder efetuar o cancelamento</returns>
		private bool VerificarPrazoCancelamento(DateTime dataCte, Usuario usuario)
		{
			int diasCancelamento;
			DateTime novaDataCte;
			string grupoFiscal = string.Empty;
			bool sucesso = true;
			/*
				Cancelamentos de CTe do mês anterior poderão ser realizados até o 3º dia útil + 7 dias(Configurável pela Conf_Geral) dia do mês corrente.
			*/
			grupoFiscal = RecuperarValorConfGeral(_chaveCodigoGrupoFiscal);
			int.TryParse(RecuperarValorConfGeral(_chaveDiasCancelamentoCte), out diasCancelamento);

			novaDataCte = dataCte.AddMonths(1);
			novaDataCte = new DateTime(novaDataCte.Year, novaDataCte.Month, diasCancelamento);

			bool ret = _controleAcessoService.VerificarUsuarioPertenceGrupo(usuario.Codigo, grupoFiscal);
			
			if (DateTime.Now.Subtract(novaDataCte).Days > 0)
			{
				IList<GrupoUsuario> grupoUsuarios = _agrupamentoUsuarioRepository.ObterGruposPorUsuario(usuario);

				foreach (GrupoUsuario grupoUsuario in grupoUsuarios)
				{
					if (grupoUsuario.Codigo.ToUpper().Trim().Equals(grupoFiscal))
					{
						return true;
					}
				}

				return false;
			}

			return true;
		}

		/// <summary>
		/// Recupera o valor da con`figuração cadastrado na Conf Geral
		/// </summary>
		/// <param name="chave">Chave da confGeral</param>
		/// <returns>Retorna o valor cadastrado na tabela confGeral</returns>
		private string RecuperarValorConfGeral(string chave)
		{
			ConfiguracaoTranslogic pathDiretorio = _configuracaoTranslogicRepository.ObterPorId(chave);

			return pathDiretorio != null ? pathDiretorio.Valor : string.Empty;
		}

		private bool EnviarEmail(Cte cte, string emailServer, int emailPort, string remetente, IList<CteEnvioXml> listaEnvio, TipoArquivoEnvioEnum tipoArquivoEnvio, Stream arquivoPdf, Stream arquivoXml)
		{
			// mailMessage.To.Add(envio.EnvioPara);
			IList<string> listaEmailTo = new List<string>();
			IList<string> listaEmailCc = new List<string>();
			IList<string> listaEmailCo = new List<string>();

			string emailPara = string.Empty;
			string emailCopia = string.Empty;
			string emailCopiaOculta = string.Empty;

			string emailInvalidoPara = string.Empty;
			string emailInvalidoCopia = string.Empty;
			string emailInvalidoCopiaOculta = string.Empty;
			bool result = false;

			try
			{
				string diretorio = Directory.GetCurrentDirectory();	
				string caminhoLogoAll = diretorio + "\\" + "Logo-All.jpg";

				SmtpClient client;
				MailMessage mailMessage;
				AlternateView html;
				LinkedResource logo;

				string chave = cte.Chave;

				Regex regexEmail = new Regex(@"^([\w\-\.]+)@((\[([0-9]{1,3}\.){3}[0-9]{1,3}\])|(([\w\-]+\.)+)([a-zA-Z]{2,4}))$");

				// Caso não tenha email para enviar, 
				if (listaEnvio.Count == 0)
				{
					return false;
				}

				if (listaEnvio.Count > 0)
				{
					foreach (CteEnvioXml envio in listaEnvio)
					{
						if (!regexEmail.Match(envio.EnvioPara).Success)
						{
							if (envio.TipoEnvioCte == TipoEnvioCteEnum.Email)
							{
								emailInvalidoPara += envio.EnvioPara;
								emailInvalidoPara += ";";
							}
							else if (envio.TipoEnvioCte == TipoEnvioCteEnum.EmailCopia)
							{
								emailInvalidoCopia += envio.EnvioPara;
								emailInvalidoCopia += ";";
							}
							else if (envio.TipoEnvioCte == TipoEnvioCteEnum.EmailCopiaOculta)
							{
								emailInvalidoCopiaOculta += envio.EnvioPara;
								emailInvalidoCopiaOculta += ";";
							}

							continue;
						}

						if (envio.TipoEnvioCte == TipoEnvioCteEnum.Email)
						{
							listaEmailTo.Add(envio.EnvioPara);
						}
						else if (envio.TipoEnvioCte == TipoEnvioCteEnum.EmailCopia)
						{
							listaEmailCc.Add(envio.EnvioPara);
						}
						else
						{
							listaEmailCo.Add(envio.EnvioPara);
						}
					}

					if (listaEmailTo.Count > 0 || listaEmailCc.Count > 0 || listaEmailCo.Count > 0)
					{
						// Envio de email
						client = new SmtpClient(emailServer, emailPort);
						mailMessage = new MailMessage();
						mailMessage.BodyEncoding = Encoding.UTF8;
						mailMessage.Subject = "Envio Cte - " + chave;
						mailMessage.From = new MailAddress(remetente);

						foreach (string email in listaEmailTo)
						{
							mailMessage.To.Add(email);
							emailPara += email + ";";
						}

						foreach (string email in listaEmailCc)
						{
							mailMessage.CC.Add(email);
							emailCopia += email + ";";
						}

						foreach (string email in listaEmailCo)
						{
							mailMessage.Bcc.Add(email);
							emailCopiaOculta += email + ";";
						}

						html = AlternateView.CreateAlternateViewFromString(GerarHtmlEmail(cte), null, MediaTypeNames.Text.Html);
						logo = new LinkedResource(caminhoLogoAll, MediaTypeNames.Image.Jpeg);
						logo.ContentId = "Logo";
						html.LinkedResources.Add(logo);
						mailMessage.AlternateViews.Add(html);
						mailMessage.IsBodyHtml = true;

						// Anexar os arquivos
						if (arquivoPdf != null)
						{
							// Posiciona no inicio o arquivo
							arquivoPdf.Seek(0, SeekOrigin.Begin);

							Stream arquivoPdfComunicado = AnexarComunicadoDacte(cte, arquivoPdf);
							mailMessage.Attachments.Add(new Attachment(arquivoPdfComunicado, string.Concat(chave, ".pdf")));
							// mailMessage.Attachments.Add(new Attachment(arquivoPdf, string.Concat(chave, ".pdf")));
						}

						if (arquivoXml != null)
						{
							// Posiciona no inicio o arquivo
							arquivoXml.Seek(0, SeekOrigin.Begin);

							mailMessage.Attachments.Add(new Attachment(arquivoXml, string.Concat(chave, ".xml")));
						}

						client.Send(mailMessage);
						result = true;

						// Insere na tabela histórica
						InserirHistoricoEnvioEmail(cte, emailPara, emailCopia, emailCopiaOculta, tipoArquivoEnvio);

						// Loga o envio de email
						string buffer = string.Format("Enviado email para: {0} - Cópia: {1} - Copia Oculta {2}", emailPara, emailCopia, emailCopiaOculta);
						_cteLogService.InserirLogInfo(cte, "CteService", buffer);
					}

					if ((!String.IsNullOrEmpty(emailInvalidoCopia)) || (!String.IsNullOrEmpty(emailInvalidoPara)) || (!String.IsNullOrEmpty(emailInvalidoCopiaOculta)))
					{
						InserirEnvioEmailErro(cte, emailInvalidoPara, emailInvalidoCopia, emailInvalidoCopiaOculta, "Email inválido", tipoArquivoEnvio);

						string buffer = string.Format("Erro envio email para: {0} - Cópia: {1} - Copia Oculta {2}", emailInvalidoPara, emailInvalidoCopia, emailInvalidoCopiaOculta);
						_cteLogService.InserirLogErro(cte, buffer);
					}
				}
			}
			catch (Exception ex)
			{
				InserirEnvioEmailErro(cte, emailPara, emailCopia, emailCopiaOculta, ex.Message, tipoArquivoEnvio);

				string buffer = string.Format("Enviado email para: {0} - Cópia: {1} - Copia Oculta {2}", emailPara, emailCopia, emailCopiaOculta);
				_cteLogService.InserirLogInfo(cte, buffer);
			}

			return result;
		}

		private Stream AnexarComunicadoDacte(Cte cte, Stream arquivoPdf)
		{
			CteComunicado cteComunicado = _cteComunicadoRepository.ObterPorSiglaUf(cte.SiglaUfFerrovia);

			PdfMerge merge = new PdfMerge();
			merge.AddFile(arquivoPdf);

			if (cteComunicado != null)
			{
				merge.AddFile(cteComunicado.ComunicadoPdf);

				// Grava no status do Cte
				_cteStatusRepository.InserirCteStatus(cte, ObterUsuarioRobo(), new CteStatusRetorno { Id = 31 });

				string buffer = string.Format("Adicionado o arquivo  de comunicado no envio do DACTE. Sigla Uf Ferrovia: {0}", cte.SiglaUfFerrovia);
				_cteLogService.InserirLogInfo(cte, "CteService", buffer);
			}

			return merge.Execute();
		}

		/*
		 * /// <summary>
		/// Salva os e-mail
		/// </summary>
		/// <param name="email"> e-mail a ser alterado</param>
		/// <param name="novoEmail"> Novo e-mail a ser alterado</param>
		/// <param name="fluxo"> Fluxo informado</param>
		public void SalvarEmailCteReenvioArquivo(string email, string novoEmail, string fluxo)
		{
										_cteRepository.SalvarEmailCteReenvioArquivo(email, novoEmail, fluxo);
		}
		*/
	}
}
