namespace Translogic.Modules.Core.Domain.Services.FluxosComerciais
{
	using System;
	using System.Collections.Generic;
	using System.Globalization;
	using System.IO;
	using System.Linq;
	using System.Reflection;
	using System.Text;
	using System.Text.RegularExpressions;
	using System.Threading;
	using System.Xml;
	using System.Xml.Schema;
	using Castle.Services.Transaction;
	using Interfaces.Simconsultas;
	using Interfaces.Simconsultas.Interfaces;
	using Model.Codificador;
	using Model.Codificador.Repositories;
	using Model.Diversos.Cte;
	using Model.Diversos.Ibge;
	using Model.Diversos.Ibge.Repositories;
	using Model.Diversos.Simconsultas;
	using Model.Dto;
	using Model.FluxosComerciais.Nfes;
	using Model.FluxosComerciais.Nfes.Repositories;
	using Model.FluxosComerciais.Simconsultas;
	using Model.FluxosComerciais.Simconsultas.Repositories;
	using Translogic.Core.Infrastructure;
	using Util;

	/// <summary>
	/// Servi�o da NFe
	/// </summary>
	[Transactional]
	public class NfeAnulacaoService
	{
		private readonly IConfiguracaoTranslogicRepository _configuracaoTranslogicRepository;
		private readonly ISimconsultasService _simconsultasService;
		private readonly INfeAnulacaoSimconsultasRepository _nfeAnulacaoSimconsultasRepository;
		private readonly INfeAnulacaoProdutoSimconsultasRepository _nfeAnulacaoProdutoSimconsultasRepository;
		private readonly ILogSimconsultasRepository _logSimconsultasRepository;
		private readonly IUfIbgeRepository _estadoIbgeRepository;
		private readonly IStatusRetornoNfeRepository _statusRetornoNfeRepository;
		private readonly IStatusNfeAnulacaoRepository _statusNfeAnulacaoRepository;
		private readonly ILogStatusNfeRepository _logStatusNfeRepository;
		private readonly INfeConfiguracaoEmpresaUnidadeMedidaRepository _nfeConfiguracaoEmpresaUnidadeMedidaRepository;

		private StringBuilder _errosValidacaoXml;

		/// <summary>
		/// Initializes a new instance of the <see cref="NfeService"/> class.
		/// </summary>
		/// <param name="configuracaoTranslogicRepository"> The configuracao translogic repository. </param>
		/// <param name="simconsultasService"> The simconsultas service. </param>
		/// <param name="nfeSimconsultasRepository"> The nfe simconsultas repository. </param>
		/// <param name="nfeProdutoSimconsultasRepository"> Reposit�rio de produto de nfe do simconsultas injetado</param>
		/// <param name="logSimconsultasRepository">Repositorio de log do simconsultas injetado</param>
		/// <param name="estadoIbgeRepository"> Repositorio de UfIbge injetado</param>
		/// <param name="statusRetornoNfeRepository"> Repositorio de status de retorno de nfe injetado</param>
		/// <param name="statusNfeAnulacaoRepository">Repositorio de Status de Nfe Anula��o injetado</param>
		/// <param name="logStatusNfeRepository">Repositorio de Log injetado</param>
		/// <param name="nfeConfiguracaoEmpresaUnidadeMedidaRepository">Repositorio de configura��o da empresa x unidade medida injetado</param>
		public NfeAnulacaoService(IConfiguracaoTranslogicRepository configuracaoTranslogicRepository, ISimconsultasService simconsultasService, INfeAnulacaoSimconsultasRepository nfeSimconsultasRepository, INfeAnulacaoProdutoSimconsultasRepository nfeProdutoSimconsultasRepository, ILogSimconsultasRepository logSimconsultasRepository, IUfIbgeRepository estadoIbgeRepository, IStatusRetornoNfeRepository statusRetornoNfeRepository, IStatusNfeAnulacaoRepository statusNfeAnulacaoRepository, ILogStatusNfeRepository logStatusNfeRepository, INfeConfiguracaoEmpresaUnidadeMedidaRepository nfeConfiguracaoEmpresaUnidadeMedidaRepository)
		{
			_configuracaoTranslogicRepository = configuracaoTranslogicRepository;
			_nfeConfiguracaoEmpresaUnidadeMedidaRepository = nfeConfiguracaoEmpresaUnidadeMedidaRepository;
			_logStatusNfeRepository = logStatusNfeRepository;
			_statusNfeAnulacaoRepository = statusNfeAnulacaoRepository;
			_statusRetornoNfeRepository = statusRetornoNfeRepository;
			_estadoIbgeRepository = estadoIbgeRepository;
			_logSimconsultasRepository = logSimconsultasRepository;
			_nfeAnulacaoProdutoSimconsultasRepository = nfeProdutoSimconsultasRepository;
			_nfeAnulacaoSimconsultasRepository = nfeSimconsultasRepository;
			_simconsultasService = simconsultasService;
		}

		/// <summary>
		/// Valida a chave NFe
		/// </summary>
		/// <param name="chaveNfe"> The chave nfe. </param>
		/// <param name="origem">Origem da consulta</param>
		public void ValidarChaveNfe(string chaveNfe, TelaProcessamentoNfe origem)
		{
			StatusNfeAnulacao statusNfeErro;
			int[] pesos = new int[]
                            {
                                4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2
                            };
			if (string.IsNullOrEmpty(chaveNfe))
			{
				statusNfeErro = InserirStatusNfe(chaveNfe, "V01", 0, 0, 0, 0, origem);
				throw new TranslogicValidationException(statusNfeErro.StatusRetornoNfe.Mensagem);
			}

			if (chaveNfe.Length != 44)
			{
				statusNfeErro = InserirStatusNfe(chaveNfe, "V02", 0, 0, 0, 0, origem);
				throw new TranslogicValidationException(string.Format(statusNfeErro.StatusRetornoNfe.Mensagem, chaveNfe));
			}

			int somatorio = 0;
			for (int i = 0; i <= chaveNfe.Length - 2; i++)
			{
				int digito;
				try
				{
					digito = int.Parse(chaveNfe[i].ToString());
				}
				catch (Exception)
				{
					statusNfeErro = InserirStatusNfe(chaveNfe, "V04", 0, 0, 0, 0, origem);
					throw new TranslogicValidationException(string.Format(statusNfeErro.StatusRetornoNfe.Mensagem, chaveNfe));
				}

				somatorio += digito * pesos[i];
			}

			int idEstado;
			if (!int.TryParse(chaveNfe.Substring(0, 2), out idEstado))
			{
				statusNfeErro = InserirStatusNfe(chaveNfe, "V02", 0, 0, 0, 0, origem);
				throw new TranslogicValidationException(string.Format(statusNfeErro.StatusRetornoNfe.Mensagem, chaveNfe));
			}

			UfIbge estado = _estadoIbgeRepository.ObterPorId(idEstado);
			if (estado == null)
			{
				statusNfeErro = InserirStatusNfe(chaveNfe, "V03", 0, 0, 0, 0, origem);
				throw new TranslogicValidationException(string.Format(statusNfeErro.StatusRetornoNfe.Mensagem, chaveNfe));
			}

			int modeloNota;
			if (int.TryParse(chaveNfe.Substring(20, 2), out modeloNota))
			{
				if (modeloNota != 55)
				{
					statusNfeErro = InserirStatusNfe(chaveNfe, "55", 0, 0, 0, 0, origem);
					throw new TranslogicValidationException(string.Format(statusNfeErro.StatusRetornoNfe.Mensagem, chaveNfe));
				}
			}

			int ano, mes;
			if (int.TryParse(chaveNfe.Substring(2, 2), out ano) && int.TryParse(chaveNfe.Substring(4, 2), out mes))
			{
				if (mes <= 0 || mes >= 13)
				{
					statusNfeErro = InserirStatusNfe(chaveNfe, "V02", 0, 0, 0, 0, origem);
					throw new TranslogicValidationException(string.Format(statusNfeErro.StatusRetornoNfe.Mensagem, chaveNfe));
				}

				ano += 2000;
				DateTime dataChave = new DateTime(ano, mes, 1);
				TimeSpan timestamp = DateTime.Now - dataChave;

				if (timestamp.Days > 210)
				{
					InserirStatusNfe(chaveNfe, "31", 0, 0, 0, 0, origem);
					StatusRetornoNfe statusRetorno = _statusRetornoNfeRepository.ObterPorCodigo("31");
					throw new TranslogicValidationException(string.Format(statusRetorno.Mensagem, chaveNfe));
				}
			}

			int mod = somatorio % 11;
			int dv = 11 - mod;
			if (mod.Equals(0) || mod.Equals(1))
			{
				dv = 0;
			}

			string stringUltimoDigito = chaveNfe.Substring(43);
			int ultimoDigito;
			int.TryParse(stringUltimoDigito, out ultimoDigito);
			if (!ultimoDigito.Equals(dv))
			{
				statusNfeErro = InserirStatusNfe(chaveNfe, "25", 0, 0, 0, 0, origem);
				throw new TranslogicValidationException(string.Format(statusNfeErro.StatusRetornoNfe.Mensagem, chaveNfe));
			}
		}

		/// <summary>
		/// Retorna os dados do Banco de dados
		/// </summary>
		/// <param name="chaveNfe">Chave Nfe para consulta</param>
		/// <param name="origem">Origem da consulta</param>
		/// <returns>Retorna o status da nfe</returns>
		public StatusNfeDto ObterStatusNfeBancoDados(string chaveNfe, TelaProcessamentoNfe origem)
		{
			try
			{
				ValidarChaveNfe(chaveNfe, origem);
			}
			catch (Exception ex)
			{
				return new StatusNfeDto
				{
					ObtidaComSucesso = false,
					IndPermiteReenvio = false,
					IndLiberaDigitacao = false,
					NotaFiscalEletronica = null,
					MensagemErro = ex.Message,
					StackTrace = ex.StackTrace
				};
			}

			StatusNfeAnulacao statusNfe = _statusNfeAnulacaoRepository.ObterPorChaveNfe(chaveNfe);
			NotaFiscalAnulacao notaBanco = ObterDadosNfeBancoDados(chaveNfe);

			if (notaBanco != null)
			{
				return ObterDtoPorNfeBaixada(notaBanco, statusNfe, origem);
			}

			return null;
		}

		/// <summary>
		/// Verifica se, ap�s ocorrer um erro ao carregar a NFe, a mesma deve ser liberada para digita��o.
		/// </summary>
		/// <param name="codigoErro">c�digo do erro ao carregar</param>
		/// <returns>valor boleano</returns>
		public bool PodeLiberarDigitacaoNfe(string codigoErro)
		{
			var entity = _statusRetornoNfeRepository.ObterPorCodigo(codigoErro);

			if (entity == null)
			{
				return false;
			}

			return entity.IndLiberarDigitacao;
		}

		/// <summary>
		/// Obt�m os dados da NF-e
		/// </summary>
		/// <param name="chaveNfe">Chave da NF-e</param>
		/// <param name="origemPesquisa">Tela que efetuou a pesquisa.</param>
		/// <param name="indProcessamentoBackground"> Indica se o processamento � em background</param>
		/// <returns>Dto de status da nfe</returns>
		public StatusNfeDto ObterDadosNfe(string chaveNfe, TelaProcessamentoNfe origemPesquisa, bool indProcessamentoBackground)
		{
			StatusNfeAnulacao statusNfe = _statusNfeAnulacaoRepository.ObterPorChaveNfe(chaveNfe);
			var configuracao = _configuracaoTranslogicRepository.ObterPorId("LIBERACAO_DT_180_CGTO_ERRO_V07");
			if (configuracao != null && configuracao.Valor.Equals("S"))
			{
				if (!indProcessamentoBackground
					&& statusNfe != null
					&& statusNfe.StatusRetornoNfe != null
					&& statusNfe.StatusRetornoNfe.Codigo.Equals("V07"))
				{
					return new StatusNfeDto
					{
						ObtidaComSucesso = false,
						IndPermiteReenvio = indProcessamentoBackground ? statusNfe.StatusRetornoNfe.IndPermiteReenvioBo : statusNfe.StatusRetornoNfe.IndPermiteReenvioTela,
						IndLiberaDigitacao = statusNfe.StatusRetornoNfe.IndLiberarDigitacao,
						NotaFiscalEletronica = null,
						MensagemErro = string.Format(statusNfe.StatusRetornoNfe.Mensagem, chaveNfe),
						CodigoStatusRetorno = statusNfe.StatusRetornoNfe.Codigo
					};
				}
			}

			StatusNfeDto statusNfeDto = ObterStatusNfeBancoDados(chaveNfe, origemPesquisa);

			// Verifica se conseguiu recuperar do banco de Dados
			if (statusNfeDto != null)
			{
				return statusNfeDto;
			}

			if (statusNfe == null || statusNfe.StatusRetornoNfe.IndPermiteReenvioTela)
			{
				return ObterNfeSimConsultas(chaveNfe, indProcessamentoBackground, origemPesquisa);
			}

			if ((indProcessamentoBackground && statusNfe.StatusRetornoNfe.IndPermiteReenvioBo) ||
				(!indProcessamentoBackground && statusNfe.StatusRetornoNfe.IndPermiteReenvioTela)
				)
			{
				return ObterNfeSimConsultas(chaveNfe, indProcessamentoBackground, origemPesquisa);
			}

			return new StatusNfeDto
			{
				ObtidaComSucesso = false,
				IndPermiteReenvio = indProcessamentoBackground ? statusNfe.StatusRetornoNfe.IndPermiteReenvioBo : statusNfe.StatusRetornoNfe.IndPermiteReenvioTela,
				IndLiberaDigitacao = statusNfe.StatusRetornoNfe.IndLiberarDigitacao,
				NotaFiscalEletronica = null,
                MensagemErro = string.Format(statusNfe.StatusRetornoNfe.Mensagem ?? string.Empty, chaveNfe),
                CodigoStatusRetorno = statusNfe.StatusRetornoNfe.Codigo ?? string.Empty
			};
		}

		/// <summary>
		/// Insere o status da nfe
		/// </summary>
		/// <param name="chaveNfe">Chave da Nfe</param>
		/// <param name="codigoStatusRetorno">Codigo do status de retorno da nfe</param>
		/// <param name="pesoNota">Peso Total da Nfe</param>
		/// <param name="pesoUtilizado">Peso Utilizado da Nfe</param>
		/// <param name="volumeNota">Volume Total da Nfe</param>
		/// <param name="volumeUtilizado">Volume utilizado na nfe</param>
		/// <param name="origem">Origem consulta</param>
		/// <returns>Objeto StatusNfe</returns>
		public StatusNfeAnulacao InserirStatusNfe(string chaveNfe, string codigoStatusRetorno, double pesoNota, double pesoUtilizado, double? volumeNota, double volumeUtilizado, TelaProcessamentoNfe origem)
		{
			StatusNfeAnulacao statusNfe = _statusNfeAnulacaoRepository.ObterPorChaveNfe(chaveNfe);
			if (statusNfe == null)
			{
				StatusRetornoNfe statusRetorno = _statusRetornoNfeRepository.ObterPorCodigo(codigoStatusRetorno);
				statusNfe = new StatusNfeAnulacao
								{
									ChaveNfe = chaveNfe,
									DataCadastro = DateTime.Now,
									PesoUtilizado = pesoUtilizado,
									VolumeUtilizado = volumeUtilizado,
									StatusRetornoNfe = statusRetorno,
									Peso = pesoNota,
									Volume = volumeNota.HasValue ? volumeNota.Value : 0,
									Origem = origem.ToString()
								};

				_statusNfeAnulacaoRepository.Inserir(statusNfe);
			}

			return statusNfe;
		}

		/// <summary>
		/// Obt�m o Dto por nfe baixada
		/// </summary>
		/// <param name="nota">Objeto Nota fiscal</param>
		/// <param name="statusNfe">Objeto status nfe</param>
		/// <param name="origemPesquisa">Tela que efetuou a pesquisa</param>
		/// <returns>Dto de sucesso</returns>
		public StatusNfeDto ObterDtoPorNfeBaixada(INotaFiscalEletronica nota, StatusNfeAnulacao statusNfe, TelaProcessamentoNfe origemPesquisa)
		{
			if (statusNfe == null)
			{
				double pesoUtilizado = nota.Peso;
				double volumeUtilizado = nota.Volume.HasValue ? nota.Volume.Value : 0;

				double? volumeNota = Tools.TruncateValue(nota.Volume, TipoTruncateValueEnum.UnidadeMilhar);
				double pesoNota = Tools.TruncateValue(nota.Peso / 1000, TipoTruncateValueEnum.UnidadeMilhar);
				string chaveNfe = nota.ChaveNfe;

				statusNfe = InserirStatusNfe(chaveNfe, "0", pesoNota, pesoUtilizado, volumeNota, volumeUtilizado, origemPesquisa);
			}

			if (statusNfe.Peso == 0 && statusNfe.Volume == 0)
			{
				double? volumeNota = Tools.TruncateValue(nota.Volume, TipoTruncateValueEnum.UnidadeMilhar);
				double pesoNota = Tools.TruncateValue(nota.Peso / 1000, TipoTruncateValueEnum.UnidadeMilhar);

				StatusRetornoNfe statusRetornoNfe = _statusRetornoNfeRepository.ObterPorCodigo("0");

				statusNfe.Peso = pesoNota;
				statusNfe.Volume = volumeNota ?? 0;
				statusNfe.StatusRetornoNfe = statusRetornoNfe;
				_statusNfeAnulacaoRepository.Atualizar(statusNfe);
			}

			// return VerificarExisteSaldoNfe(statusNfe, nota);
			return new StatusNfeDto
			{
				Peso = statusNfe.Peso,
				Volume = statusNfe.Volume,
				PesoUtilizado = statusNfe.PesoUtilizado,
				VolumeUtilizado = statusNfe.VolumeUtilizado,
				ObtidaComSucesso = true,
				IndPermiteReenvio = false,
				NotaFiscalEletronica = nota,
				MensagemErro = string.Empty,
				StackTrace = string.Empty
			};
		}

		/// <summary>
		/// Obt�m os dados da NFe da base de dados
		/// </summary>
		/// <param name="chaveNfe">Chave da Nfe</param>
		/// <returns> Objeto de nota fiscal </returns>
		public NotaFiscalAnulacao ObterDadosNfeBancoDados(string chaveNfe)
		{
			return _nfeAnulacaoSimconsultasRepository.ObterPorChaveNfe(chaveNfe);
		}

		/// <summary>
		/// Obt�m os dados de status da NFe da base de dados
		/// </summary>
		/// <param name="chaveNfe">Chave da Nfe</param>
		/// <returns> Objeto de Status da nota fiscal </returns>
		public StatusNfeAnulacao ObterStatusNfe(string chaveNfe)
		{
			return _statusNfeAnulacaoRepository.ObterPorChaveNfe(chaveNfe);
		}

		/// <summary>
		/// Processa os dados da NFe
		/// </summary>
		/// <param name="dadosNfe">Dados retornados da NFe</param>
		/// <param name="chaveNfe">Chave da Nfe</param>
		/// <param name="origem"> Origem da consulta</param>
		/// <returns> Objeto de StatusNfeDto </returns>
		public StatusNfeAnulacao ProcessarRetornoSimconsultas(RetornoSimconsultas dadosNfe, string chaveNfe, TelaProcessamentoNfe origem)
		{
			string codigoErro = string.Empty;
			string mensagemErro = string.Empty;
			NotaFiscalAnulacao nfe = null;
			XmlDocument doc = new XmlDocument();
			doc.LoadXml(dadosNfe.Nfe.OuterXml);

			if (doc.DocumentElement != null)
			{
				if (doc.DocumentElement.Name.Equals("nfeProc"))
				{
					/*if (!ValidarXml(doc, out mensagemErro))
					{
						return InserirStatusNfe(chaveNfe, "V05", 0, 0, 0, 0);
					}*/
					try
					{
						nfe = ProcessarDadosNfe(doc);
						codigoErro = "000";
					}
					catch (Exception)
					{
						codigoErro = "S999";
					}
				}

				else if (doc.DocumentElement.Name.Equals("NFe"))
				{
					/*if (!ValidarXml(doc, out mensagemErro))
					{
						return InserirStatusNfe(chaveNfe, "V05", 0, 0, 0, 0);
					}*/
					try
					{
						nfe = ProcessarDadosNfe(doc);
						codigoErro = "000";
					}
					catch (Exception)
					{
						codigoErro = "S999";
					}
				}

				else if (doc.DocumentElement.Name.Equals("InfConsulta"))
				{
					XmlElement root = doc.DocumentElement;
					XmlNamespaceManager nsmgr = new XmlNamespaceManager(doc.NameTable);
					nsmgr.AddNamespace("d", root.NamespaceURI);

					XmlNode noCodigo = root.SelectSingleNode("//d:InfConsulta/d:StatusConsulta", nsmgr);
					codigoErro = noCodigo.InnerText;

					int codigoRetornoTmp;
					if (!int.TryParse(codigoErro, out codigoRetornoTmp))
					{
						codigoErro = "S999";
					}

					XmlNode noDescricao = root.SelectSingleNode("//d:InfConsulta/d:StatusDescricao", nsmgr);
					mensagemErro = noDescricao.InnerText;
				}
			}

			StatusRetornoNfe statusRetorno = _statusRetornoNfeRepository.ObterPorCodigo(codigoErro);
			if (statusRetorno == null)
			{
				statusRetorno = new StatusRetornoNfe
				{
					Codigo = codigoErro,
					DataCadastro = DateTime.Now,
					IndPermiteReenvioTela = false,
					IndPermiteReenvioBo = false,
					IndLiberarDigitacao = true,
					Mensagem = mensagemErro
				};
				_statusRetornoNfeRepository.Inserir(statusRetorno);
			}

			StatusNfeAnulacao statusNfe = GravarLogStatusNfe(nfe, statusRetorno, chaveNfe, origem);
			return statusNfe;
			// return new StatusNfeDto { CodigoStatusRetorno = statusRetorno.Codigo, ObtidaComSucesso = false, NotaFiscalEletronica = null, MensagemErro = statusRetorno.Mensagem, IndPermiteReenvio = statusRetorno.IndPermiteReenvio };
		}

		/// <summary>
		/// Grava o log de status da nfe e retorna o ultimo status
		/// </summary>
		/// <param name="nfe">Objeto da Nfe caso haja sucesso no parser</param>
		/// <param name="statusRetorno">Status de retorno do simconsultas</param>
		/// <param name="chaveNfe">Chave de acesso da Nfe</param>
		/// <param name="origem"> Tela de origem de nfe</param>
		/// <returns>Status da Nfe</returns>
		public StatusNfeAnulacao GravarLogStatusNfe(NotaFiscalAnulacao nfe, StatusRetornoNfe statusRetorno, string chaveNfe, TelaProcessamentoNfe origem)
		{
			double peso = 0;
			double volume = 0;

			LogStatusNfe logStatusNfe = new LogStatusNfe
								{
									ChaveNfe = chaveNfe,
									DataCadastro = DateTime.Now,
									StatusRetornoNfe = statusRetorno
								};
			_logStatusNfeRepository.Inserir(logStatusNfe);

			if (nfe != null)
			{
				NotaFiscalAnulacaoProduto produto = nfe.ListaProdutos.FirstOrDefault();
				if (produto != null)
				{
					string unid = produto.UnidadeComercial.ToUpperInvariant();
					if (!string.IsNullOrEmpty(unid) && (unid.StartsWith("L") || unid.StartsWith("M")) && nfe.Volume.HasValue)
					{
						volume = Tools.TruncateValue(nfe.Volume.Value, TipoTruncateValueEnum.UnidadeMilhar);
					}
					// else
					// {
					peso = Tools.TruncateValue(nfe.Peso / 1000, TipoTruncateValueEnum.UnidadeMilhar);
					// }
				}
			}

			StatusNfeAnulacao statusNfe = _statusNfeAnulacaoRepository.ObterPorChaveNfe(chaveNfe);
			if (statusNfe != null)
			{
				if (statusNfe.Peso == 0 && peso > 0)
				{
					statusNfe.Peso = peso;
				}

				if (statusNfe.Volume == 0 && volume > 0)
				{
					statusNfe.Volume = volume;
				}

				statusNfe.StatusRetornoNfe = statusRetorno;
				statusNfe.Origem = origem.ToString();
				_statusNfeAnulacaoRepository.Atualizar(statusNfe);
			}
			else
			{
				statusNfe = new StatusNfeAnulacao
								{
									ChaveNfe = chaveNfe,
									DataCadastro = DateTime.Now,
									PesoUtilizado = 0,
									VolumeUtilizado = 0,
									StatusRetornoNfe = statusRetorno,
									Peso = peso,
									Volume = volume,
									Origem = origem.ToString()
								};

				_statusNfeAnulacaoRepository.Inserir(statusNfe);
			}

			return statusNfe;
		}

		/// <summary>
		/// Tenta obter os dados da NFe pelo webservice do Simconsultas
		/// </summary>
		/// <param name="chaveNfe"> The chave nfe. </param>
		/// <param name="indProcessoBackground">Indica se � um processo em background</param>
		/// <param name="origem"> Origem da consulta</param>
		/// <returns> Objeto DTO de status da nfe</returns>
		public StatusNfeDto ObterNfeSimConsultas(string chaveNfe, bool indProcessoBackground, TelaProcessamentoNfe origem)
		{
			string mensagemErro = string.Empty;
			for (int i = 0; i < 3; i++)
			{
				RetornoSimconsultas dadosNota = i % 2 == 0 ? ObterDadosNfeSimConsultaNacional(chaveNfe) : ObterDadosNfeSimConsulta(chaveNfe);

				if (!dadosNota.Erro)
				{
					try
					{
						StatusNfeAnulacao statusNfe = ProcessarRetornoSimconsultas(dadosNota, chaveNfe, origem);
						// Status de ok, pode-se pegar os dados do banco
						if (statusNfe.StatusRetornoNfe.Codigo.Equals("000"))
						{
							var notaBanco = ObterDadosNfeBancoDados(chaveNfe);
							if (notaBanco != null)
							{
								return ObterDtoPorNfeBaixada(notaBanco, statusNfe, origem);
							}
						}

						// Status de cancelamento
						if (statusNfe.StatusRetornoNfe.Codigo.Equals("100"))
						{
							return new StatusNfeDto
									{
										IndPermiteReenvio = false,
										CodigoStatusRetorno = statusNfe.StatusRetornoNfe.Codigo,
										ObtidaComSucesso = false,
										NotaFiscalEletronica = null,
										MensagemErro = string.Format(statusNfe.StatusRetornoNfe.Mensagem, chaveNfe),
										IndLiberaDigitacao = false,
										StackTrace = string.Empty
									};
						}

						// quando for processo em background deve-se verificar pelo flag IndPermiteReenvioBo
						// quando n�o for processo em background deve-se verficar pelo flag IndPermiteReenvioTela
						if ((indProcessoBackground && statusNfe.StatusRetornoNfe.IndPermiteReenvioBo) ||
							(!indProcessoBackground && statusNfe.StatusRetornoNfe.IndPermiteReenvioTela))
						{
							Thread.Sleep(2000);
							continue;
						}

						return new StatusNfeDto
								{
									IndPermiteReenvio = indProcessoBackground ? statusNfe.StatusRetornoNfe.IndPermiteReenvioBo : statusNfe.StatusRetornoNfe.IndPermiteReenvioTela,
									IndLiberaDigitacao = statusNfe.StatusRetornoNfe.IndLiberarDigitacao,
									CodigoStatusRetorno = statusNfe.StatusRetornoNfe.Codigo,
									ObtidaComSucesso = false,
									NotaFiscalEletronica = null,
									MensagemErro = string.Format(statusNfe.StatusRetornoNfe.Mensagem, chaveNfe),
									StackTrace = string.Empty
								};
					}
					catch (Exception ex)
					{
						string stack = "EX01 - " + ex.Message + "\n" + ex.StackTrace;
					    mensagemErro += "" + ex.StackTrace;

						return new StatusNfeDto
								{
									IndPermiteReenvio = true,
									IndLiberaDigitacao = true,
									CodigoStatusRetorno = "EX01",
									ObtidaComSucesso = false,
									NotaFiscalEletronica = null,
                                    MensagemErro = string.Format("Ocorreu um erro ao processar os dados da NF-e({0}).", chaveNfe),
									StackTrace = stack
								};
					}
				}
			}

			return new StatusNfeDto
					{
						IndPermiteReenvio = true,
						IndLiberaDigitacao = true,
                        CodigoStatusRetorno = "EX02",
						ObtidaComSucesso = false,
						NotaFiscalEletronica = null,
						MensagemErro = string.Format("Ocorreu um erro ao obter os dados da NF-e({0}).", chaveNfe),
						StackTrace = "EX02 - " + mensagemErro
					};
		}

		/// <summary>
		/// Obt�m os dados do banco de dados pela chave
		/// </summary>
		/// <param name="chave">Chave da Nfe</param>
		/// <returns>Objeto NfeSimconsultas</returns>
		public NotaFiscalAnulacao ObterDbNfeSimconsultasPorChave(string chave)
		{
			return _nfeAnulacaoSimconsultasRepository.ObterPorChaveNfe(chave);
		}

		/// <summary>
		/// Processa os dados da Nfe
		/// </summary>
		/// <param name="doc"> Documento xml da NFe. </param>
		/// <returns> Objeto de NFe da simconsultas persistido </returns>
		public NotaFiscalAnulacao ProcessarDadosNfe(XmlDocument doc)
		{
            string[] datePatterns = { "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd", "dd/MM/yyyy", "dd/MM/yyyy HH:mm:ss", "yyyy-MM-ddTHH:mm:sszzz" };

			XmlElement root = doc.DocumentElement;
			XmlNamespaceManager nsmgr = new XmlNamespaceManager(doc.NameTable);
			nsmgr.AddNamespace("d", root.NamespaceURI);

            /* esse If trata o XML novo da Brado */
            if (root.Name.Equals("soap:Envelope"))
            {
                nsmgr.AddNamespace("soap", root.NamespaceURI);

                var xmlBrado = new StringBuilder();
                xmlBrado.Append("<nfeProc xmlns:ns2=\"http://www.w3.org/2000/09/xmldsig#\" xmlns=\"http://www.portalfiscal.inf.br/nfe\" versao=\"3.10\">");

                xmlBrado.Append(
                    (((root.SelectSingleNode("//soap:Body", nsmgr)).FirstChild).
                        FirstChild).InnerXml.ToString());

                xmlBrado.Append("</nfeProc>");

                /* recarrega o XML em mem�ria */
                doc.LoadXml(xmlBrado.ToString());
                root = doc.DocumentElement;
                nsmgr = new XmlNamespaceManager(doc.NameTable);
                nsmgr.AddNamespace("d", root.NamespaceURI);
            }

            var noInfNfe = root.SelectSingleNode("//d:NFe/d:infNFe", nsmgr);

            // Verificar se a NFe � v�lida
            if (noInfNfe != null && noInfNfe.Attributes != null)
            {
                string versaoNfe = noInfNfe.Attributes["versao"] == null
                                       ? string.Empty
                                       : noInfNfe.Attributes["versao"].Value;
                string chaveNfe = noInfNfe.Attributes["Id"] == null
                                      ? noInfNfe.Attributes["id"].Value
                                      : noInfNfe.Attributes["Id"].Value;

                string noDataEmissao = !string.IsNullOrEmpty(versaoNfe) && versaoNfe.Equals("3.10") ? "dhEmi" : "dEmi";
                string noDataSaida = !string.IsNullOrEmpty(versaoNfe) && versaoNfe.Equals("3.10")
                                         ? "dhSaiEnt"
                                         : "dSaiEnt";

                if (!String.IsNullOrEmpty(chaveNfe) && !String.IsNullOrWhiteSpace(chaveNfe))
                {
                    XmlNode noIde = noInfNfe.SelectSingleNode("//d:ide", nsmgr);
                    string dataEmissao = string.IsNullOrEmpty(ObterTextoNoXml(noIde, "dhEmi", nsmgr)) ? ObterTextoNoXml(noIde, "dEmi", nsmgr) : ObterTextoNoXml(noIde, "dhEmi", nsmgr);
                    string dataSaida = string.IsNullOrEmpty(ObterTextoNoXml(noIde, "dhSaiEnt", nsmgr)) ? ObterTextoNoXml(noIde, "dSaiEnt", nsmgr) : ObterTextoNoXml(noIde, "dhSaiEnt", nsmgr);

                    NotaFiscalAnulacao nfe = new NotaFiscalAnulacao();
                    // DADOS DE IDENTIFICA��O DA NFE
                    nfe.ChaveNfe = chaveNfe.Substring(3);
                    nfe.CodigoUfIbge = Convert.ToInt32(ObterTextoNoXml(noIde, "cUF", nsmgr));
                    nfe.CodigoChaveAcesso = Convert.ToInt32(ObterTextoNoXml(noIde, "cNF", nsmgr));
                    nfe.NaturezaOperacao = ObterTextoNoXml(noIde, "natOp", nsmgr);
                    string indPag = ObterTextoNoXml(noIde, "indPag", nsmgr);
                    if (!string.IsNullOrEmpty(indPag))
                    {
                        nfe.FormaPagamento = this.ObterFormaPagamento(indPag);
                    }
                    nfe.ModeloNota = ObterTextoNoXml(noIde, "mod", nsmgr);
                    nfe.SerieNotaFiscal = ObterTextoNoXml(noIde, "serie", nsmgr);
                    nfe.NumeroNotaFiscal = Convert.ToInt32(ObterTextoNoXml(noIde, "nNF", nsmgr));
                    ////nfe.DataEmissao = !string.IsNullOrEmpty(versaoNfe) && versaoNfe.Equals("3.10") ? "dhEmi" : "dEmi";
                    if (!string.IsNullOrEmpty(dataSaida))
                    {
                        string horaSaida = ObterTextoNoXml(noIde, "hSaiEnt", nsmgr);
                        if (!string.IsNullOrEmpty(horaSaida))
                        {
                            dataSaida = string.Concat(dataSaida, " ", horaSaida);
                        }

                        nfe.DataSaida = DateTime.ParseExact(dataSaida, datePatterns, CultureInfo.InvariantCulture, DateTimeStyles.None);
                    }
                    if (!string.IsNullOrEmpty(dataEmissao))
                    {
                        if (!string.IsNullOrEmpty(versaoNfe) && versaoNfe.Equals("3.10"))
                        {
                            dataEmissao = dataEmissao.Substring(0, 10);
                            nfe.DataEmissao = DateTime.ParseExact(
                                dataEmissao,
                                datePatterns,
                                CultureInfo.InvariantCulture,
                                DateTimeStyles.None);
                        }
                        else
                        {
                            nfe.DataEmissao = DateTime.ParseExact(
                                string.IsNullOrEmpty(ObterTextoNoXml(noIde, "dhEmi", nsmgr)) ? ObterTextoNoXml(noIde, "dEmi", nsmgr) : ObterTextoNoXml(noIde, "dhEmi", nsmgr),
                                datePatterns,
                                CultureInfo.InvariantCulture,
                                DateTimeStyles.None);
                        }
                    }
                    else
                    {
                        dataEmissao = DateTime.Now.ToShortDateString();
                    }

                    if (!string.IsNullOrEmpty(dataSaida))
                    {
                        if (!string.IsNullOrEmpty(versaoNfe) && versaoNfe.Equals("3.10"))
                        {
                            dataSaida = dataSaida.Substring(0, 10);
                            nfe.DataSaida = DateTime.ParseExact(
                                dataSaida,
                                datePatterns,
                                CultureInfo.InvariantCulture,
                                DateTimeStyles.None);
                        }
                        else
                        {
                            string horaSaida = ObterTextoNoXml(noIde, "hSaiEnt", nsmgr);
                            if (!string.IsNullOrEmpty(horaSaida))
                            {
                                dataSaida = string.Concat(dataSaida, " ", horaSaida);
                            }

                            try
                            {
                                dataSaida = dataSaida.Replace("�s", string.Empty);
                                nfe.DataSaida = DateTime.ParseExact(
                                    dataSaida,
                                    datePatterns,
                                    CultureInfo.InvariantCulture,
                                    DateTimeStyles.None);
                            }
                            catch
                            {
                            }
                        }
                    }
                    else
                    {
                        dataSaida = DateTime.Now.ToShortDateString();
                    }

                    nfe.TipoNotaFiscal = this.ObterTipoNotaFiscal(ObterTextoNoXml(noIde, "tpNF", nsmgr));
                    nfe.CodigoIbge = Convert.ToInt32(ObterTextoNoXml(noIde, "cMunFG", nsmgr));
                    nfe.FormatoImpressao = this.ObterFormatoImpressao(ObterTextoNoXml(noIde, "tpImp", nsmgr));
                    nfe.TipoEmissao = this.ObterTipoEmissao(ObterTextoNoXml(noIde, "tpEmis", nsmgr));
                    nfe.DigitoVerificadorChave = string.IsNullOrEmpty(ObterTextoNoXml(noIde, "cDV", nsmgr))
                                                     ? 0
                                                     : Convert.ToInt32(ObterTextoNoXml(noIde, "cDV", nsmgr));
                    nfe.TipoAmbiente = this.ObterTipoAmbiente(ObterTextoNoXml(noIde, "tpAmb", nsmgr));
                    nfe.FinalidadeEmissao = this.ObterFinalidadeEmissao(ObterTextoNoXml(noIde, "finNFe", nsmgr));
                    nfe.ProcessoEmissao = this.ObterProcessoEmissao(ObterTextoNoXml(noIde, "procEmi", nsmgr));
                    nfe.VersaoProcesso = ObterTextoNoXml(noIde, "verProc", nsmgr);

                    // DADOS DE EMITENTE
                    XmlNode noEmit = noInfNfe.SelectSingleNode("//d:emit", nsmgr);
                    DadosEmpresaTemp dadosEmpresaEmitente = this.RecuperarDadosEmpresa(noEmit, true, nsmgr);
                    this.ProcessarDadosEmitente(ref nfe, dadosEmpresaEmitente);

                    // DADOS DE DESTINATARIO
                    XmlNode noDest = noInfNfe.SelectSingleNode("//d:dest", nsmgr);
                    DadosEmpresaTemp dadosEmpresaDestinataria = this.RecuperarDadosEmpresa(noDest, false, nsmgr);
                    this.ProcessarDadosDestinatario(ref nfe, dadosEmpresaDestinataria);

                    // DADOS DE IMPOSTOS DA NFE
                    XmlNode noIcmsTotal = noInfNfe.SelectSingleNode("//d:total/d:ICMSTot", nsmgr);

                    nfe.ValorBaseCalculoIcms = ObterDoubleDoXML(noIcmsTotal, "vBC", nsmgr);
                    nfe.ValorIcms = ObterDoubleDoXML(noIcmsTotal, "vICMS", nsmgr);
                    nfe.ValorBaseCalculoSubTributaria = ObterDoubleDoXML(noIcmsTotal, "vBCST", nsmgr);
                    nfe.ValorSubTributaria = ObterDoubleDoXML(noIcmsTotal, "vST", nsmgr);
                    nfe.ValorProduto = ObterDoubleDoXML(noIcmsTotal, "vProd", nsmgr);
                    nfe.ValorTotalFrete = ObterDoubleDoXML(noIcmsTotal, "vFrete", nsmgr);
                    nfe.ValorSeguro = ObterDoubleDoXML(noIcmsTotal, "vSeg", nsmgr);
                    nfe.ValorDesconto = ObterDoubleDoXML(noIcmsTotal, "vDesc", nsmgr);
                    nfe.ValorImpostoImportacao = ObterDoubleDoXML(noIcmsTotal, "vII", nsmgr);
                    nfe.ValorIpi = ObterDoubleDoXML(noIcmsTotal, "vIPI", nsmgr);
                    nfe.ValorPis = ObterDoubleDoXML(noIcmsTotal, "vPIS", nsmgr);
                    nfe.ValorCofins = ObterDoubleDoXML(noIcmsTotal, "vCOFINS", nsmgr);
                    nfe.ValorOutro = ObterDoubleDoXML(noIcmsTotal, "vOutro", nsmgr);
                    nfe.Valor = ObterDoubleDoXML(noIcmsTotal, "vNF", nsmgr);
                    nfe.Volume = 0;

                    // DADOS DE TRANSPORTE
                    XmlNode noTransp = noInfNfe.SelectSingleNode("//d:transp", nsmgr);
                    var docTransp = new XmlDocument();
                    docTransp.LoadXml(noTransp.OuterXml);
                    string modFrete = ObterTextoNoXml(docTransp, "modFrete", nsmgr);
                    modFrete = string.IsNullOrEmpty(modFrete) || string.IsNullOrWhiteSpace(modFrete) ? "0" : modFrete;
                    nfe.ModeloFrete = this.ObterModeloFrete(modFrete);

                    XmlNode noTransportadora = docTransp.SelectSingleNode("//d:transporta", nsmgr);
                    if (noTransportadora != null)
                    {
                        string cnpj = ObterTextoNoXml(noTransportadora, "CNPJ", nsmgr);
                        if (!string.IsNullOrEmpty(cnpj))
                        {
                            nfe.CnpjTransportadora = cnpj;
                        }

                        nfe.NomeTransportadora = ObterTextoNoXml(noTransportadora, "xNome", nsmgr);
                        nfe.EnderecoTransportadora = ObterTextoNoXml(noTransportadora, "xEnder", nsmgr);
                        nfe.MunicipioTransportadora = ObterTextoNoXml(noTransportadora, "xMun", nsmgr);
                        nfe.UfTransportadora = ObterTextoNoXml(noTransportadora, "UF", nsmgr);
                    }

                    XmlNode noVeicTransp = docTransp.SelectSingleNode("//d:veicTransp", nsmgr);
                    if (noVeicTransp != null)
                    {
                        nfe.Placa = ObterTextoNoXml(noVeicTransp, "placa", nsmgr);
                    }

                    nfe.CanceladoEmitente = false;
                    nfe.DataHoraGravacao = DateTime.Now;
                    nfe.Xml = doc.OuterXml;

                    // Caso a nfe tenha sido processada simultaneamente, ent�o retorna a nfe j� processada.
                    NotaFiscalAnulacao nfeSimconsultas = _nfeAnulacaoSimconsultasRepository.ObterPorChaveNfe(nfe.ChaveNfe);

                    if (nfeSimconsultas != null)
                    {
                        return nfeSimconsultas;
                    }

                    _nfeAnulacaoSimconsultasRepository.Inserir(nfe);

                    IList<NotaFiscalAnulacaoProduto> listaProdutos = ProcessarProdutosNfe(nfe, noInfNfe.SelectNodes("//d:det", nsmgr), nsmgr);

                    // XmlNode noVolume = docTransp.SelectSingleNode("//d:vol", nsmgr);
                    XmlNodeList listaNoVolume = docTransp.SelectNodes("//d:vol", nsmgr);
                    bool alterado = false;
                    double pesoBrutoTotal = 0;
                    double pesoLiquidoTotal = 0;
                    double volumeTotal = 0;

                    bool? indPesoToneladas = VerificarEmpresaPesoTonelada(nfe.CnpjEmitente);

                    foreach (XmlNode noVolume in listaNoVolume)
                    {
                        // string pesoB = ObterTextoNoXml(noVolume, "pesoB", nsmgr);
                        string pesoB = noVolume["pesoB"] != null ? noVolume["pesoB"].InnerText : string.Empty;

                        if (!string.IsNullOrEmpty(pesoB))
                        {
                            double pesoBruto = Convert.ToDouble(pesoB, CultureInfo.GetCultureInfo("en-US"));
                            if (indPesoToneladas.HasValue)
                            {
                                if (indPesoToneladas.Value)
                                {
                                    pesoBruto = pesoBruto * 1000;
                                }
                            }
                            else
                            {
                                if (pesoBruto > 0 && pesoBruto < 150)
                                {
                                    pesoBruto = pesoBruto * 1000;
                                }
                            }

                            // nfe.PesoBruto += pesoBruto;
                            pesoBrutoTotal += pesoBruto;
                            alterado = true;
                        }

                        // string pesoL = ObterTextoNoXml(noVolume, "pesoL", nsmgr);
                        string pesoL = noVolume["pesoL"] != null ? noVolume["pesoL"].InnerText : string.Empty;
                        if (!string.IsNullOrEmpty(pesoL))
                        {
                            double pesoLiquido = Convert.ToDouble(pesoL, CultureInfo.GetCultureInfo("en-US"));
                            if (indPesoToneladas.HasValue)
                            {
                                if (indPesoToneladas.Value)
                                {
                                    pesoLiquido = pesoLiquido * 1000;
                                }
                            }
                            else
                            {
                                if (pesoLiquido > 0 && pesoLiquido < 150)
                                {
                                    pesoLiquido = pesoLiquido * 1000;
                                }
                            }

                            // nfe.Peso += pesoLiquido;
                            pesoLiquidoTotal += pesoLiquido;
                            alterado = true;
                        }

                        // string volume = ObterTextoNoXml(noTransportadora, "qVol", nsmgr);
                        string volume = noVolume["qVol"] != null ? noVolume["qVol"].InnerText : string.Empty;
                        if (!string.IsNullOrEmpty(volume))
                        {
                            double vol = Convert.ToDouble(volume, CultureInfo.GetCultureInfo("en-US"));
                            if (vol > 1000)
                            {
                                vol = vol / 1000;
                            }

                            // nfe.Volume += vol;
                            volumeTotal += vol;
                            alterado = true;
                        }
                    }

                    if (alterado)
                    {
                        nfe.PesoBruto = pesoBrutoTotal;
                        nfe.Peso = pesoLiquidoTotal;
                        nfe.Volume = volumeTotal;

                        _nfeAnulacaoSimconsultasRepository.Atualizar(nfe);
                    }

                    if (nfe.PesoBruto.Equals(0))
                    {
                        double? pesoProdutos = ObterPesoTotalToneladas(listaProdutos) * 1000;
                        if (pesoProdutos.HasValue)
                        {
                            nfe.PesoBruto = pesoProdutos.Value;
                            nfe.Peso = pesoProdutos.Value;
                            _nfeAnulacaoSimconsultasRepository.Atualizar(nfe);
                        }
                    }

                    if (!nfe.Volume.HasValue || (nfe.Volume.HasValue && nfe.Volume.Value.Equals(0)))
                    {
                        double? volumeProdutos = ObterVolume(listaProdutos);
                        if (volumeProdutos.HasValue)
                        {
                            nfe.Volume = volumeProdutos.Value;
                            _nfeAnulacaoSimconsultasRepository.Atualizar(nfe);
                        }
                    }

                    nfe.ListaProdutos = listaProdutos;

                    return nfe;
                }
            }
            return null;
		}

        /// <summary>
        ///     extrai um double de um xml
        /// </summary>
        /// <param name="xmlNode">n� do xml informado</param>
        /// <param name="tagNo">tag do xml informado</param>
        /// <param name="nsmgr">namespace manager</param>
        /// <returns>double extraido</returns>
        public static double ObterDoubleDoXML(XmlNode xmlNode, string tagNo, XmlNamespaceManager nsmgr)
        {
            double result = 0;
            string text = ObterTextoNoXml(xmlNode, tagNo, nsmgr);

            if (!string.IsNullOrWhiteSpace(text))
            {
                text = TratarFormatoNumerico(text);
                result = Convert.ToDouble(text, CultureInfo.GetCultureInfo("en-US"));
            }

            return result;
        }

        /// <summary>
        ///     obterm um texto do xml
        /// </summary>
        /// <param name="xmlNode">no do xml informado</param>
        /// <param name="tagNo">tag do xml informado</param>
        /// <param name="nsmgr">namespace manager</param>
        /// <returns>texto extraido</returns>
        public static string ObterTextoNoXml(XmlNode xmlNode, string tagNo, XmlNamespaceManager nsmgr)
        {
            XmlNode no = xmlNode.SelectSingleNode(".//d:" + tagNo, nsmgr);

            if (no == null)
            {
                return null;
            }

            return no.InnerText;
        }

        /// <summary>
        ///     Tratar Formato Numerico
        /// </summary>
        /// <param name="value">valor para tratar</param>
        /// <returns>texto tratado</returns>
        public static string TratarFormatoNumerico(string value)
        {
            int indexPeriod = value.IndexOf(".");
            int indexComma = value.IndexOf(",");

            if (indexPeriod < indexComma)
            {
                value = value.Replace(".", string.Empty).Replace(",", ".");
            }
            else if (indexComma < indexPeriod)
            {
                value = value.Replace(",", string.Empty);
            }

            return value;
        }

		/// <summary>
		/// Obt�m o peso total dos produtos em toneladas
		/// </summary>
		/// <param name="produtos">Lista de produtos da nota</param>
		/// <returns>Peso total em toneladas</returns>
		public double? ObterPesoTotalToneladas(IList<NotaFiscalAnulacaoProduto> produtos)
		{
			if (produtos.Count > 0)
			{
				string unidadeComercial = produtos[0].UnidadeComercial;
				UnidadeComercialNfeEnum unidComercialEnum = ObterUnidadeComercialNfe(unidadeComercial);
				switch (unidComercialEnum)
				{
					case UnidadeComercialNfeEnum.Toneladas:
						return produtos.Sum(c => c.QuantidadeComercial);
					case UnidadeComercialNfeEnum.Quilos:
						return produtos.Sum(c => c.QuantidadeComercial) / 1000;
					case UnidadeComercialNfeEnum.Litros:
					case UnidadeComercialNfeEnum.MetrosCubicos:
					case UnidadeComercialNfeEnum.Outros:
						return null;
				}
			}

			return null;
		}

		/// <summary>
		/// Obt�m o volume na nota em milhares de litros
		/// </summary>
		/// <param name="produtos">Lista de produtos da nfe</param>
		/// <returns>Volume em milhares de litros</returns>
		public double? ObterVolume(IList<NotaFiscalAnulacaoProduto> produtos)
		{
			if (produtos.Count > 0)
			{
				string unidadeComercial = produtos[0].UnidadeComercial;
				UnidadeComercialNfeEnum unidComercialEnum = ObterUnidadeComercialNfe(unidadeComercial);
				switch (unidComercialEnum)
				{
					case UnidadeComercialNfeEnum.Toneladas:
					case UnidadeComercialNfeEnum.Quilos:
						return null;
					case UnidadeComercialNfeEnum.Litros:
						return produtos.Sum(c => c.QuantidadeComercial) / 1000;
					case UnidadeComercialNfeEnum.MetrosCubicos:
						return produtos.Sum(c => c.QuantidadeComercial);
					case UnidadeComercialNfeEnum.Outros:
						return null;
				}
			}

			return null;
		}

		/// <summary>
		/// Valida a estrutura do XMl
		/// </summary>
		/// <param name="xmlDocument"> Xml a ser validado </param>
		/// <param name="erros"> The erros de valida��o. </param>
		/// <returns> Retorna true se a estrutura do xml estiver valida. </returns>
		public bool ValidarXml(XmlDocument xmlDocument, out string erros)
		{
			_errosValidacaoXml = new StringBuilder();
			// xmlDocument = new XmlDocument();
			// xmlDocument.Load(@"C:\Temp\NFE.xml");

			// Remove o No LogTrace para validar o xml
			RemoveNoLogTrace(xmlDocument);

			// xmlDocument.ReadNode()
			// Define o tipo de valida��o
			XmlReaderSettings settings = new XmlReaderSettings();
			settings.ValidationType = ValidationType.Schema;

			// Carrega o arquivo de esquema
			XmlSchemaSet schemas = new XmlSchemaSet();
			settings.Schemas = schemas;

			// Carrega o arquivo Xsd do assembly
			// Quando carregar o eschema, especificar o namespace que ele valida
			// e a localiza��o do arquivo 
			// arquivo Base da Nfe 2.0 que possui includo arquivo leiauteNFe_v2.00.xsd.
			Stream arquivoXsd = LoadFileFromResouce("Translogic.Modules.Core.Resources.xsd.nfe_v2.00.xsd");
			schemas.Add(null, XmlReader.Create(arquivoXsd));

			// arquivo com o layout da Nfe 2.0 que possui include arquivo xmldsig-core-schema_v1.01.xsd e tiposBasico_v1.03.xsd.
			arquivoXsd = LoadFileFromResouce("Translogic.Modules.Core.Resources.xsd.leiauteNFe_v2.00.xsd");
			schemas.Add(null, XmlReader.Create(arquivoXsd));

			// arquivo com a estrutura da Nfe 2.0.
			arquivoXsd = LoadFileFromResouce("Translogic.Modules.Core.Resources.xsd.xmldsig-core-schema_v1.01.xsd");
			schemas.Add(null, XmlReader.Create(arquivoXsd));

			// arquivo com os tipos complexos criados na estrutura.
			arquivoXsd = LoadFileFromResouce("Translogic.Modules.Core.Resources.xsd.tiposBasico_v1.03.xsd");
			schemas.Add(null, XmlReader.Create(arquivoXsd));

			// Especifica o tratamento de evento para os erros de validacao
			settings.ValidationEventHandler += ValidationEventHandler;

			// cria um leitor para valida��o
			XmlReader xmlReader = XmlReader.Create(new StringReader(xmlDocument.InnerXml), settings);

			try
			{
				// Faz a leitura de todos os dados XML
				while (xmlReader.Read())
				{
				}
			}
			catch (XmlException err)
			{
				// Um erro ocorre se o documento XML inclui caracteres ilegais
				// ou tags que n�o est�o aninhadas corretamente
				if (_errosValidacaoXml == null)
				{
					_errosValidacaoXml = new StringBuilder();
				}

				_errosValidacaoXml.AppendLine("Ocorreu um erro critico durante a validacao XML.");
				_errosValidacaoXml.AppendLine("Erro: " + err.Message);
			}
			finally
			{
				xmlReader.Close();
			}

			erros = _errosValidacaoXml.ToString();

			return _errosValidacaoXml.Length.Equals(0);
		}

		/// <summary>
		/// Obtem os dados da Nfe no SimConsulta pelo portal nacional prioritariamente
		/// </summary>
		/// <param name="chaveNfe">Chave da Nfe</param>
		/// <returns>Retorno o objeto nota RetornoSimconsultas</returns>
		public RetornoSimconsultas ObterDadosNfeSimConsultaNacional(string chaveNfe)
		{
			// Declara��o de vari�veis
			ConfiguracaoTranslogic configuracao;
			DateTime dataInicioProcesso = DateTime.Now;
			RetornoSimconsultas dadosNota = null;

			try
			{
				// Recupera a chave de acesso do Sim Consulta
				configuracao = _configuracaoTranslogicRepository.ObterPorId("CHAVE_ACESSO_SIMCONSULTAS");

				// Obtem os dados da Nfe
				dadosNota = _simconsultasService.ObterDadosNfeNacional(configuracao.Valor, chaveNfe);

				// Grava o Log do processo
				GravarLogProcesso(dataInicioProcesso, chaveNfe, dadosNota);

				return dadosNota;
			}
			catch (Exception ex)
			{
				if (dadosNota == null)
				{
					dadosNota = new RetornoSimconsultas { MensagemErro = ex.Message };
				}
				// seta o tipo Erro
				dadosNota.Erro = true;

				// Grava o Log do processo
				GravarLogProcesso(dataInicioProcesso, chaveNfe, dadosNota);
			}
			finally
			{
				// Finaliza os objetos
				dadosNota = null;
			}

			return dadosNota;
		}

		/// <summary>
		/// Obtem os dados da Nfe no SimConsulta
		/// </summary>
		/// <param name="chaveNfe">Chave da Nfe</param>
		/// <returns>Retorno o objeto nota RetornoSimconsultas</returns>
		public RetornoSimconsultas ObterDadosNfeSimConsulta(string chaveNfe)
		{
			// Declara��o de vari�veis
			ConfiguracaoTranslogic configuracao;
			DateTime dataInicioProcesso = DateTime.Now;
			RetornoSimconsultas dadosNota = null;

			try
			{
				// Recupera a chave de acesso do Sim Consulta
				configuracao = _configuracaoTranslogicRepository.ObterPorId("CHAVE_ACESSO_SIMCONSULTAS");

				// Obtem os dados da Nfe
				dadosNota = _simconsultasService.ObterDadosNfe(configuracao.Valor, chaveNfe);

				// Grava o Log do processo
				GravarLogProcesso(dataInicioProcesso, chaveNfe, dadosNota);

				return dadosNota;
			}
			catch (Exception ex)
			{
				if (dadosNota == null)
				{
					dadosNota = new RetornoSimconsultas { MensagemErro = ex.Message };
				}
				// seta o tipo Erro
				dadosNota.Erro = true;

				// Grava o Log do processo
				GravarLogProcesso(dataInicioProcesso, chaveNfe, dadosNota);
			}
			finally
			{
				// Finaliza os objetos
				dadosNota = null;
			}

			return dadosNota;
		}

		private TipoNotaFiscalEnum ObterTipoNotaFiscal(string textoTipoNotaFiscal)
		{
			return Translogic.Core.Commons.Enum<TipoNotaFiscalEnum>.From(GetInteger(textoTipoNotaFiscal).ToString());
		}

		private ModeloFreteEnum? ObterModeloFrete(string textoModeloFrete)
		{
            return Translogic.Core.Commons.Enum<ModeloFreteEnum>.From(GetInteger(textoModeloFrete).ToString());
		}

		private void ProcessarDadosDestinatario(ref NotaFiscalAnulacao nfe, DadosEmpresaTemp dadosEmpresa)
		{
			nfe.CnpjDestinatario = dadosEmpresa.Cnpj;
			nfe.RazaoSocialDestinatario = dadosEmpresa.RazaoSocial;
			nfe.NomeFantasiaDestinatario = dadosEmpresa.NomeFantasia;
			nfe.LogradouroDestinatario = dadosEmpresa.Logradouro;
			nfe.NumeroDestinatario = dadosEmpresa.Numero;
			nfe.ComplementoDestinatario = dadosEmpresa.Complemento;
			nfe.BairroDestinatario = dadosEmpresa.Bairro;
			nfe.CodigoMunicipioDestinatario = dadosEmpresa.CodigoMunicipioIbge;
			nfe.MunicipioDestinatario = dadosEmpresa.Municipio;
			nfe.UfDestinatario = dadosEmpresa.SiglaUf;
			nfe.CepDestinatario = dadosEmpresa.Cep;
			nfe.CodigoPaisDestinatario = dadosEmpresa.CodigoPais;
			nfe.PaisDestinatario = dadosEmpresa.Pais;
			nfe.TelefoneDestinatario = dadosEmpresa.Telefone;
			nfe.InscricaoEstadualDestinatario = dadosEmpresa.InscricaoEstadual;
		}

		private void ProcessarDadosEmitente(ref NotaFiscalAnulacao nfe, DadosEmpresaTemp dadosEmpresa)
		{
			nfe.CnpjEmitente = dadosEmpresa.Cnpj;
			nfe.RazaoSocialEmitente = dadosEmpresa.RazaoSocial;
			nfe.NomeFantasiaEmitente = dadosEmpresa.NomeFantasia;
			nfe.LogradouroEmitente = dadosEmpresa.Logradouro;
			nfe.NumeroEmitente = dadosEmpresa.Numero;
			nfe.ComplementoEmitente = dadosEmpresa.Complemento;
			nfe.BairroEmitente = dadosEmpresa.Bairro;
			nfe.CodigoMunicipioEmitente = dadosEmpresa.CodigoMunicipioIbge;
			nfe.MunicipioEmitente = dadosEmpresa.Municipio;
			nfe.UfEmitente = dadosEmpresa.SiglaUf;
			nfe.CepEmitente = dadosEmpresa.Cep;
			nfe.CodigoPaisEmitente = dadosEmpresa.CodigoPais;
			nfe.PaisEmitente = dadosEmpresa.Pais;
			nfe.TelefoneEmitente = dadosEmpresa.Telefone;
			nfe.InscricaoEstadualEmitente = dadosEmpresa.InscricaoEstadual;
		}

		private DadosEmpresaTemp RecuperarDadosEmpresa(XmlNode noXml, bool indEmitente, XmlNamespaceManager nsmgr)
		{
			XmlDocument docEmp = new XmlDocument();
			docEmp.LoadXml(noXml.OuterXml);

			XmlNode noEndereco = docEmp.SelectSingleNode(indEmitente ? "//d:enderEmit" : "//d:enderDest", nsmgr);

			DadosEmpresaTemp dados = new DadosEmpresaTemp
										 {
											 RazaoSocial = ObterTextoNoXml(docEmp, "xNome", nsmgr),
											 NomeFantasia = ObterTextoNoXml(docEmp, "xFant", nsmgr),
											 Logradouro = ObterTextoNoXml(noEndereco, "xLgr", nsmgr),
											 Numero = ObterTextoNoXml(noEndereco, "nro", nsmgr),
											 Complemento = ObterTextoNoXml(noEndereco, "xCpl", nsmgr),
											 Bairro = ObterTextoNoXml(noEndereco, "xBairro", nsmgr),
											 CodigoMunicipioIbge = ObterTextoNoXml(noEndereco, "cMun", nsmgr),
											 Municipio = ObterTextoNoXml(noEndereco, "xMun", nsmgr),
											 SiglaUf = ObterTextoNoXml(noEndereco, "UF", nsmgr),
											 Cep = ObterTextoNoXml(noEndereco, "CEP", nsmgr),
											 CodigoPais = ObterTextoNoXml(noEndereco, "cPais", nsmgr),
											 Pais = ObterTextoNoXml(noEndereco, "xPais", nsmgr),
											 Telefone = ObterTextoNoXml(noEndereco, "fone", nsmgr),
											 InscricaoEstadual = ObterTextoNoXml(docEmp, "IE", nsmgr)
										 };
			string cnpj = ObterTextoNoXml(docEmp, "CNPJ", nsmgr);
			if (!string.IsNullOrEmpty(dados.SiglaUf) && dados.SiglaUf.Equals("EX"))
			{
				dados.Cnpj = "00000000000000";
			}
			else
			{
				if (!string.IsNullOrEmpty(cnpj))
				{
					dados.Cnpj = cnpj;
				}
			}

			string cpf = ObterTextoNoXml(docEmp, "CPF", nsmgr);
			if (!string.IsNullOrEmpty(cpf))
			{
				dados.Cpf = cpf;
			}

			return dados;
		}
        
		private FormaPagamentoEnum ObterFormaPagamento(string numeroFormaPagamento)
		{
            return Translogic.Core.Commons.Enum<FormaPagamentoEnum>.From(GetInteger(numeroFormaPagamento).ToString());
		}

		private ProcessoEmissaoEnum ObterProcessoEmissao(string textoProcessoEmissao)
		{
			/*0 - emiss�o de NF-e com aplicativo do contribuinte;
			1 - emiss�o de NF-e avulsa pelo Fisco;
			2 - emiss�o de NF-e avulsa, pelo contribuinte com seu certificado digital, atrav�s do site
			do Fisco;
			3- emiss�o de NF-e pelo contribuinte com aplicativo fornecido pelo Fisco.*/
            return Translogic.Core.Commons.Enum<ProcessoEmissaoEnum>.From(GetInteger(textoProcessoEmissao).ToString());
		}

		private FinalidadeEmissaoEnum ObterFinalidadeEmissao(string textoFinalidadeEmissao)
		{
			/*
			 1 - NFe normal
			 2 - NFe complementar
			 3 - NFe de ajuste
			 */
            return Translogic.Core.Commons.Enum<FinalidadeEmissaoEnum>.From(GetInteger(textoFinalidadeEmissao).ToString());
		}

        private string TratarDecimal(XmlNode xmlNode, string tagNo, XmlNamespaceManager nsmgr)
        {
            var result = ObterTextoNoXml(xmlNode, "vNF", nsmgr);

            if (result.IndexOf(",") != -1)
            {
                result = result.Replace(".", string.Empty).Replace(",", ".");
            }

            return result;
        }

	    private TipoAmbienteEnum ObterTipoAmbiente(string textoTipoAmbiente)
		{
            return Translogic.Core.Commons.Enum<TipoAmbienteEnum>.From(GetInteger(textoTipoAmbiente).ToString());
		}

		private TipoEmissaoEnum ObterTipoEmissao(string textoTipoEmissao)
		{
			/*1 - Normal;
			2 - Conting�ncia FS
			3 - Conting�ncia SCAN
			4 - Conting�ncia DPEC
			5 - Conting�ncia FSDA*/
            return Translogic.Core.Commons.Enum<TipoEmissaoEnum>.From(GetInteger(textoTipoEmissao).ToString());
		}

		private FormatoImpressaoEnum ObterFormatoImpressao(string textoFormatoImpressao)
		{
			if (string.IsNullOrEmpty(textoFormatoImpressao))
			{
				return FormatoImpressaoEnum.Retrato;
			}

            return Translogic.Core.Commons.Enum<FormatoImpressaoEnum>.From(GetInteger(textoFormatoImpressao).ToString());
		}

        private int GetInteger(string value)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                throw new Exception("N�mero inv�lido");
            }

            var result = Int32.Parse(Regex.Replace(value, @"[^\d]", string.Empty));
            return result;
        }

		private IList<NotaFiscalAnulacaoProduto> ProcessarProdutosNfe(NotaFiscalAnulacao nfe, XmlNodeList listaProdutos, XmlNamespaceManager nsmgr)
		{
			List<NotaFiscalAnulacaoProduto> list = new List<NotaFiscalAnulacaoProduto>();
			// double pesoTotalNota = 0;
			foreach (XmlNode produto in listaProdutos)
			{
				NotaFiscalAnulacaoProduto nfeProduto = ProcessarProdutoNfe(nfe, produto, nsmgr);
				list.Add(nfeProduto);
				// pesoTotalNota += nfeProduto.QuantidadeComercial;
			}

			// nfe.Peso = pesoTotalNota;
			// nfe.PesoBruto = pesoTotalNota;
			// _nfeSimconsultasRepository.Atualizar(nfe);
			return list;
		}

		private NotaFiscalAnulacaoProduto ProcessarProdutoNfe(NotaFiscalAnulacao nfe, XmlNode noDetProduto, XmlNamespaceManager nsmgr)
		{
			XmlDocument docProd = new XmlDocument();
			docProd.LoadXml(noDetProduto.OuterXml);

			NotaFiscalAnulacaoProduto nfeProduto = new NotaFiscalAnulacaoProduto();
			XmlNode noProduto = docProd.SelectSingleNode("//d:prod", nsmgr);
			nfeProduto.NotaFiscalAnulacao = nfe;
			nfeProduto.CodigoProduto = ObterTextoNoXml(noProduto, "cProd", nsmgr);
			nfeProduto.DescricaoProduto = ObterTextoNoXml(noProduto, "xProd", nsmgr);
			nfeProduto.CodigoNcm = ObterTextoNoXml(noProduto, "NCM", nsmgr);
			// nfeProduto.Genero = ObterTextoNoXml(noProduto, "", nsmgr); //verificar onde est� no xml da nfe
			nfeProduto.Cfop = ObterTextoNoXml(noProduto, "CFOP", nsmgr);
			nfeProduto.UnidadeComercial = ObterTextoNoXml(noProduto, "uCom", nsmgr);
			nfeProduto.QuantidadeComercial = Convert.ToDouble(ObterTextoNoXml(noProduto, "qCom", nsmgr), CultureInfo.GetCultureInfo("en-US"));
			nfeProduto.ValorUnitarioComercializacao = Convert.ToDouble(ObterTextoNoXml(noProduto, "vUnCom", nsmgr), CultureInfo.GetCultureInfo("en-US"));
			nfeProduto.ValorProduto = Convert.ToDouble(ObterTextoNoXml(noProduto, "vProd", nsmgr), CultureInfo.GetCultureInfo("en-US"));
			nfeProduto.UnidadeTributavel = ObterTextoNoXml(noProduto, "uTrib", nsmgr);
			nfeProduto.QuantidadeTributavel = Convert.ToDouble(ObterTextoNoXml(noProduto, "qTrib", nsmgr), CultureInfo.GetCultureInfo("en-US"));
			nfeProduto.ValorUnitarioTributacao = Convert.ToDouble(ObterTextoNoXml(noProduto, "vUnTrib", nsmgr), CultureInfo.GetCultureInfo("en-US"));

			/*
			// DADOS DE IMPOSTOS - COMENTADO POIS N�O � IMPORTANTE PARA O PROJETO DO CTE
			XmlNode noImposto = noDetProduto.SelectSingleNode("//d:imposto", nsmgr);
			// DADOS DE ICMS DO PRODUTO
			// nfeProduto.IcmsEmitente // <column name="ICMS_ORIG" />
			// nfeProduto.IcmsCst // <column name="ICMS_CST" />
			// nfeProduto.ModalidadeDeterminacaoBaseCalculoIcms //<column name="ICMS_MODBC" />
			// nfeProduto.ValorBaseCalculoIcms // <column name="ICMS_VBC " />
			// nfeProduto.AliquotaIcms // <column name="ICMS_PICMS" />
			// nfeProduto.ValorIcms // <column name="ICMS_VICMS" />
            
			// DADOS DE PIS DO PRODUTO
			XmlNode noPis = noImposto.SelectSingleNode("//d:PIS", nsmgr);
			nfeProduto.AliquotaPis = Convert.ToDouble(ObterTextoNoXml(noPis, "PISAliq", nsmgr), CultureInfo.GetCultureInfo("en-US"));
			nfeProduto.QuantidadeVendida = Convert.ToDouble(ObterTextoNoXml(noPis, "PISQtde", nsmgr), CultureInfo.GetCultureInfo("en-US"));
			// nfeProduto.PisCst =  // <column name="PIS_CST" />
			// 
			// nfeProduto.ValorPis //<column name="PIS_VPIS" />
			// 
            
			// DADOS DE COFINS DO PRODUTO
			// nfeProduto.CofinsCst //<column name="CONFINS_CST" />
			// nfeProduto.QuantidadeVendidaCofins //<column name="COFINS_QBCPROD" />
			// nfeProduto.AliquotaCofins // <column name="COFINS_VALIQPROD" />
			// nfeProduto.ValorCofins // <column name="COFINS_VCOFINS" />

			// DADOS DE IPI DO PRODUTO
			// nfeProduto.ValorBaseCalculoIpi // <column name="IPI_VBC" />
			// nfeProduto.PercentualIpi //<column name="IPI_PIPI" />
			// nfeProduto.ValorIpi // <column name="IPI_VIPI" />
			// nfeProduto.IpiCst // <column name="IPI_CST" />

			// DADOS DE IMPOSTO DE IMPORTA��O
			// nfeProduto.ValorBaseCalculoImpostoImportacao // <column name="II_VBC" />
			// nfeProduto.ValorDespesaAduaneira //<column name="II_VDESPADU" />
			// nfeProduto.ValorIof //<column name="II_VIOF" />
			// nfeProduto.ValorImpostoImportacao //<column name="II_VII" />
			 */
			nfeProduto.DataHoraGravacao = DateTime.Now;

			_nfeAnulacaoProdutoSimconsultasRepository.Inserir(nfeProduto);
			return nfeProduto;
		}

		private UnidadeComercialNfeEnum ObterUnidadeComercialNfe(string unidadeComercial)
		{
			unidadeComercial = unidadeComercial.ToUpperInvariant();
			if (unidadeComercial.StartsWith("K"))
			{
				return UnidadeComercialNfeEnum.Quilos;
			}

			if (unidadeComercial.StartsWith("T"))
			{
				return UnidadeComercialNfeEnum.Toneladas;
			}

			if (unidadeComercial.StartsWith("L"))
			{
				return UnidadeComercialNfeEnum.Litros;
			}

			if (unidadeComercial.Equals("M3"))
			{
				return UnidadeComercialNfeEnum.MetrosCubicos;
			}

			return UnidadeComercialNfeEnum.Outros;
		}

		private void GravarLogProcesso(DateTime dataInicioProcesso, string chaveNfe, RetornoSimconsultas dadosNota)
		{
			LogSimconsultas log = new LogSimconsultas
									{
										DataInicio = dataInicioProcesso,
										DataTermino = DateTime.Now,
										ChaveNfe = chaveNfe,
										Erro = dadosNota.Erro,
										MensagemErro = dadosNota.MensagemErro,
										DataHoraGravacao = DateTime.Now,
										Retorno = dadosNota.Erro ? string.Empty : dadosNota.Nfe.OuterXml
									};
			_logSimconsultasRepository.Inserir(log);
		}

		/// <summary>
		/// Remove o n� logTrace do xml
		/// </summary>
		/// <param name="xmlDocument">Documento xml</param>
		private void RemoveNoLogTrace(XmlDocument xmlDocument)
		{
			// Cria o namespace
			XmlElement root = xmlDocument.DocumentElement;
			XmlNamespaceManager nsmgr = new XmlNamespaceManager(xmlDocument.NameTable);
			nsmgr.AddNamespace("d", root.NamespaceURI);

			// Recupera o no INfNfe
			XmlNode noinfNFe = xmlDocument.SelectSingleNode("//d:infNFe", nsmgr);

			// Recupera o no LogTrace
			XmlNode noLogTrace = xmlDocument.SelectSingleNode("//d:LOGTrace", nsmgr);

			if (noinfNFe != null && noLogTrace != null)
			{
				// Remove o N� LogTrace do xml
				noinfNFe.RemoveChild(noLogTrace);
			}

			// Recupera o no OrigemNfe
			XmlNode noOrigemNfe = xmlDocument.SelectSingleNode("//d:OrigemNFe", nsmgr);
			if (noinfNFe != null && noOrigemNfe != null)
			{
				// Remove o N� LogTrace do xml
				noinfNFe.RemoveChild(noOrigemNfe);
			}
		}

		/// <summary>
		/// Evento disparato quando ocorre erro de valida��o no xml
		/// </summary>
		/// <param name="sender">Objeto que disparou o evento</param>
		/// <param name="args">dados da exception</param>
		private void ValidationEventHandler(object sender, ValidationEventArgs args)
		{
			// Exibe o erro da valida��o
			if (_errosValidacaoXml == null)
			{
				_errosValidacaoXml = new StringBuilder();
			}

			_errosValidacaoXml.AppendLine(args.Message);
		}

		/// <summary>
		/// Carrega o arquivo do assembly
		/// </summary>
		/// <param name="strFileName">Nome do arquivo</param>
		/// <returns>Retorna o arquivo</returns>
		private Stream LoadFileFromResouce(string strFileName)
		{
			// Recupera o Assebly.
			// var assembly = Assembly.GetExecutingAssembly();
			var assembly = Assembly.Load("Translogic.Modules.Core");
			// Recupera o arquivo do assembly
			var stream = assembly.GetManifestResourceStream(strFileName);

			try
			{
				// Verifica se o arquivo foi encontrado
				if (stream == null)
				{
					throw new FileNotFoundException("N�o foi poss�vel localizar o arquivo " + strFileName + " no resouce.", strFileName);
				}
			}
			catch (Exception ex)
			{
				throw;
			}

			return stream;
		}

		private bool? VerificarEmpresaPesoTonelada(string cnpjEmitente)
		{
			NfeConfiguracaoEmpresaUnidadeMedida nfeConfiguracaoEmpresaUnidadeMedida = _nfeConfiguracaoEmpresaUnidadeMedidaRepository.ObterPorCnpj(cnpjEmitente);
			if (nfeConfiguracaoEmpresaUnidadeMedida == null)
			{
				return null;
			}

			return nfeConfiguracaoEmpresaUnidadeMedida.UnidadeMedida.Equals("TO");
		}

		/// <summary>
		/// Classe de empresa
		/// </summary>
		protected class DadosEmpresaTemp
		{
			/// <summary>
			/// Gets or sets Cnpj.
			/// </summary>
			public string Cnpj { get; set; }

			/// <summary>
			/// Gets or sets Cpf.
			/// </summary>
			public string Cpf { get; set; }

			/// <summary>
			/// Gets or sets RazaoSocial.
			/// </summary>
			public string RazaoSocial { get; set; }

			/// <summary>
			/// Gets or sets NomeFantasia.
			/// </summary>
			public string NomeFantasia { get; set; }

			/// <summary>
			/// Gets or sets Logradouro.
			/// </summary>
			public string Logradouro { get; set; }

			/// <summary>
			/// Gets or sets Numero.
			/// </summary>
			public string Numero { get; set; }

			/// <summary>
			/// Gets or sets Complemento.
			/// </summary>
			public string Complemento { get; set; }

			/// <summary>
			/// Gets or sets Bairro.
			/// </summary>
			public string Bairro { get; set; }

			/// <summary>
			/// Gets or sets CodigoMunicipioIbge.
			/// </summary>
			public string CodigoMunicipioIbge { get; set; }

			/// <summary>
			/// Gets or sets Municipio.
			/// </summary>
			public string Municipio { get; set; }

			/// <summary>
			/// Gets or sets SiglaUf.
			/// </summary>
			public string SiglaUf { get; set; }

			/// <summary>
			/// Gets or sets Uf.
			/// </summary>
			public string Uf { get; set; }

			/// <summary>
			/// Gets or sets Cep.
			/// </summary>
			public string Cep { get; set; }

			/// <summary>
			/// Gets or sets CodigoPais.
			/// </summary>
			public string CodigoPais { get; set; }

			/// <summary>
			/// Gets or sets Pais.
			/// </summary>
			public string Pais { get; set; }

			/// <summary>
			/// Gets or sets Telefone.
			/// </summary>
			public string Telefone { get; set; }

			/// <summary>
			/// Gets or sets InscricaoEstadual.
			/// </summary>
			public string InscricaoEstadual { get; set; }
		}
	}
}
