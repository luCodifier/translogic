﻿namespace Translogic.Modules.Core.Domain.Services.FluxosComerciais
{
    using System;
    using System.Collections.Generic;
    using Castle.Services.Transaction;
    using Model.Acesso;
    using Model.Dto;
    using Model.Estrutura;
    using Model.FluxosComerciais.Pedidos.Despachos.Repositories;
    using Model.Via.Repositories;

    /// <summary>
    /// Service para controle dos descarregamentos
    /// </summary>
    [Transactional]
    public class DescarregamentoService
    {
        private readonly IDescarregamentoRepository _descarregamentoRep;
        private readonly IAreaOperacionalRepository _areaOperacionalRep;

        /// <summary>
        /// Construtor do serviço
        /// </summary>
        /// <param name="descarregamentoRepository">Repositório de descarregamentos</param>
        /// <param name="aopRepo">Repositório de áreas operacionais</param>
        public DescarregamentoService(IDescarregamentoRepository descarregamentoRepository, IAreaOperacionalRepository aopRepo)
        {
            _descarregamentoRep = descarregamentoRepository;
            _areaOperacionalRep = aopRepo;
        }

        /// <summary>
        /// Recupera a lista de vagões a serem descarregados
        /// </summary>
        /// <param name="codAO">Código da estação para filtro</param>
        /// <param name="codLinha">Código da linha</param>
        /// <param name="codVagao">Código do vagão</param>
        /// <param name="codMerc">Código da mercadoria</param>
        /// <param name="codFluxo">Código do fluxo</param>
        /// <param name="codPedido">Código do Pedido</param>
        /// <param name="identCliente">Id do Cliente  </param>
        /// <param name="dataInicio">Data de início da pesquisa</param>
        /// <param name="dataFim">Data de fim da pesquisa</param>
        /// <returns>Lista de vagões para descarregamento</returns>
        public IList<VagaoDescarregamentoDto> ObterVagoesParaDescarregamento(
            string codAO, string codLinha, string codVagao,
            string codMerc, string codFluxo, string codPedido,
            string identCliente, DateTime dataInicio, DateTime dataFim)
        {
            return _descarregamentoRep.ObterVagoesParaDescarregamento(
                codAO == null ? null : codAO.ToUpper(),
                codLinha == null ? null : codLinha.ToUpper(),
                codVagao == null ? null : codVagao.ToUpper(),
                codMerc == null ? null : codMerc.ToUpper(),
                codFluxo == null ? null : codFluxo.ToUpper(),
                codPedido == null ? null : codPedido.ToUpper(),
                identCliente, dataInicio, dataFim);
        }

        /// <summary>
        /// Recupera a lista de vagões já descarregados
        /// </summary>
        /// <param name="codAO">Código da estação para filtro</param>
        /// <param name="codLinha">Código da linha</param>
        /// <param name="codVagao">Código do vagão</param>
        /// <param name="codMerc">Código da mercadoria</param>
        /// <param name="codFluxo">Código do fluxo</param>
        /// <param name="codPedido">Código do Pedido</param>
        /// <param name="identCliente">Id do Cliente  </param>
        /// <param name="dataInicio">Data de início da pesquisa</param>
        /// <param name="dataFim">Data de fim da pesquisa</param>
        /// <returns>Lista de vagões para descarregamento</returns>
        public IList<VagaoDescarregamentoDto> ObterVagoesDescarregados(
            string codAO, string codLinha, string codVagao,
            string codMerc, string codFluxo, string codPedido,
            string identCliente, DateTime dataInicio, DateTime dataFim)
        {
            return _descarregamentoRep.ObterVagoesDescarregados(
                codAO == null ? null : codAO.ToUpper(),
                codLinha == null ? null : codLinha.ToUpper(),
                codVagao == null ? null : codVagao.ToUpper(),
                codMerc == null ? null : codMerc.ToUpper(),
                codFluxo == null ? null : codFluxo.ToUpper(),
                codPedido == null ? null : codPedido.ToUpper(),
                identCliente, dataInicio, dataFim);
        }

        /// <summary>
        /// Recupera a lista de vagões não descarregados por erros
        /// </summary>
        /// <param name="codAO">Código da estação para filtro</param>
        /// <param name="codLinha">Código da linha</param>
        /// <param name="codVagao">Código do vagão</param>
        /// <param name="codMerc">Código da mercadoria</param>
        /// <param name="codFluxo">Código do fluxo</param>
        /// <param name="codPedido">Código do Pedido</param>
        /// <param name="identCliente">Id do cliente</param>
        /// <param name="dataInicio">Data de início da pesquisa</param>
        /// <param name="dataFim">Data de fim da pesquisa</param>
        /// <returns>Lista de vagões para descarregamento</returns>
        public IList<VagaoDescarregamentoDto> ObterVagoesComErro(string codAO, string codLinha, string codVagao, string codMerc, string codFluxo, string codPedido, string identCliente, DateTime dataInicio, DateTime dataFim)
        {
            return _descarregamentoRep.ObterVagoesComErro(
                codAO == null ? null : codAO.ToUpper(),
                codLinha == null ? null : codLinha.ToUpper(),
                codVagao == null ? null : codVagao.ToUpper(),
                codMerc == null ? null : codMerc.ToUpper(),
                codFluxo == null ? null : codFluxo.ToUpper(),
                codPedido == null ? null : codPedido.ToUpper(),
                identCliente, dataInicio, dataFim);
        }

        /// <summary>
        /// Efetuar descarregamento de vagões com base nas TUs da Tabela de PESAGEM_ESTATICA_FERRO
        /// </summary>
        /// <param name="usuAcao">Usuário que efetuou a ação</param>
        /// <param name="codAO">Identificador da Área Operacional</param>
        /// <param name="vagaoEmCarreg">Indica se é para mudar os vagões para "EM CARREGAMENTO"</param>
        /// <param name="vagoes">Lista de vagões</param>
        [Transaction]
        public virtual void DescarregarVagoes(Usuario usuAcao, string codAO, bool vagaoEmCarreg, List<VagaoDescarregamentoDto> vagoes)
        {
            var estacao = _areaOperacionalRep.ObterPorCodigo(codAO.ToUpper());
            _descarregamentoRep.DescarregarVagoes(usuAcao, estacao.Id.Value, vagaoEmCarreg, vagoes);
        }

        /// <summary>
        /// Obter clientes com vagões em condição de descarregamento
        /// </summary>
        /// <param name="codAO">Identificador a Area Operacional</param>
        /// <returns>Lista de empresas com vagões para descarregamento</returns>
        public IList<string> ObterClientesComDescarregamentos(string codAO)
        {
            var estacao = _areaOperacionalRep.ObterPorCodigo(codAO.ToUpper());
            if (estacao == null)
            {
                return new List<string>();
            }

            return _descarregamentoRep.ObterClientesComDescarregamentos(estacao.Id.Value);
        }
    }
}
