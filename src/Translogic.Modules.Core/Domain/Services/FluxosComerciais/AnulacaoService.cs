﻿namespace Translogic.Modules.Core.Domain.Services.FluxosComerciais
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Web;
    using Castle.Services.Transaction;
    using Translogic.Modules.Core.Domain.Model.Acesso;
    using Translogic.Modules.Core.Domain.Model.Codificador;
    using Translogic.Modules.Core.Domain.Model.Codificador.Repositories;
    using Translogic.Modules.Core.Domain.Model.Diversos.Cte;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Modules.Core.Domain.Model.Estrutura;
    using Translogic.Modules.Core.Domain.Model.Estrutura.Repositories;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Nfes;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Nfes.Repositories;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Simconsultas;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Simconsultas.Repositories;
    using Translogic.Modules.Core.Domain.Services.Ctes;

    /// <summary>
    /// Serviço de Anulação
    /// </summary>
    [Transactional]
    public class AnulacaoService
    {
        private readonly IEmpresaClienteRepository _empresaClienteRepository;
        private readonly IStatusNfeAnulacaoRepository _statusNfeAnulacaoRepository;
        private readonly INfeAnulacaoSimconsultasRepository _nfeAnulacaoSimconsultasRepository;
        private readonly IStatusRetornoNfeRepository _statusRetornoNfeRepository;
        private readonly ICteRepository _cteRepository;
        private readonly CteService _cteService;
        private readonly ICteEmpresasRepository _cteEmpresasRepository;
        private readonly IConfiguracaoTranslogicRepository _configuracaoTranslogicRepository;
        private readonly ICteStatusRepository _cteStatusRepository;
        private readonly CteLogService _cteLogService;
        private readonly ICteConteinerRepository _cteConteinerRepository;

        private const string ConfGeralMargemLibNotaAnulacao = "CTE_MARGEM_LIB_ANULACAO";

        /// <summary>
        /// Initializes a new instance of the <see cref="AnulacaoService"/> class.
        /// </summary>
        /// <param name="empresaClienteRepository"> Repositório de empresa cliente injetado </param>
        /// <param name="statusNfeAnulacaoRepository">Repositório de status de nfe de anulação injetado </param>
        /// <param name="statusRetornoNfeRepository">Repositório de status de retorno de nfe injetado </param>
        /// <param name="nfeAnulacaoSimconsultasRepository">Repositório de Nfe Anulacao SimConsultas injetado </param>
        /// <param name="cteRepository"> Repositório do Cte injetado.</param>
        /// <param name="cteService">Serviço do Cte injetado.</param>
        /// <param name="cteEmpresasRepository">Cte Empresas Repositorio injetado.</param>
        /// <param name="configuracaoTranslogicRepository">Configuracao Repositorio injetado.</param>
        /// <param name="cteStatusRepository">Repositório do status do cte injetado</param>
        public AnulacaoService(IEmpresaClienteRepository empresaClienteRepository, IStatusNfeAnulacaoRepository statusNfeAnulacaoRepository, IStatusRetornoNfeRepository statusRetornoNfeRepository, INfeAnulacaoSimconsultasRepository nfeAnulacaoSimconsultasRepository, ICteRepository cteRepository, CteService cteService, ICteEmpresasRepository cteEmpresasRepository, IConfiguracaoTranslogicRepository configuracaoTranslogicRepository, ICteStatusRepository cteStatusRepository, CteLogService cteLogService, ICteConteinerRepository cteConteinerRepository)
        {
            _empresaClienteRepository = empresaClienteRepository;
            _cteService = cteService;
            _cteEmpresasRepository = cteEmpresasRepository;
            _configuracaoTranslogicRepository = configuracaoTranslogicRepository;
            _cteRepository = cteRepository;
            _nfeAnulacaoSimconsultasRepository = nfeAnulacaoSimconsultasRepository;
            _statusRetornoNfeRepository = statusRetornoNfeRepository;
            _statusNfeAnulacaoRepository = statusNfeAnulacaoRepository;
            _cteStatusRepository = cteStatusRepository;
            _cteLogService = cteLogService;
            _cteConteinerRepository = cteConteinerRepository;
        }

        /// <summary>
        /// Obtém a empresa pelo CNPJ
        /// </summary>
        /// <param name="cnpj"> Cnpj da Empresa Cliente. </param>
        /// <returns> Empresa Cliente </returns>
        public IEmpresa ObterEmpresaPorCnpj(string cnpj)
        {
            if (string.IsNullOrEmpty(cnpj))
            {
                return null;
            }

            IList<EmpresaCliente> listaEmpresas = _empresaClienteRepository.ObterEmpresasPorCnpj(cnpj);
            if (listaEmpresas.Count == 0)
            {
                return null;
            }

            if (listaEmpresas.Count == 1)
            {
                return listaEmpresas[0];
            }

            foreach (EmpresaCliente empresaCliente in listaEmpresas)
            {
                var sigla = empresaCliente.Sigla;
                if (!string.IsNullOrEmpty(sigla))
                {
                    if (sigla.Substring(sigla.Length - 2).StartsWith("-"))
                    {
                        return empresaCliente;
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Obtém a empresa pelo CNPJ
        /// </summary>
        /// <param name="cnpj"> Cnpj da Empresa Cliente. </param>
        /// <param name="razaoSocial">Razão social da empresa</param>
        /// <returns> Empresa Cliente </returns>
        public IList<EmpresaCliente> ObterEmpresaPorCnpjRazaoSocial(string cnpj, string razaoSocial)
        {
            return _empresaClienteRepository.ObterEmpresaClientePorCnpjRazaoSocialDaFerrovia(cnpj, razaoSocial);
        }

        /// <summary>
        /// Efetua a anulação do cte
        /// </summary>
        /// <param name="ctes">Cte a ser anulado</param>
        /// <param name="chaveNfeAnulacao">chave da nfe de anulação</param>
        /// <param name="protocolo">protocolo de autenticação da nfe na sefaz</param>
        /// <param name="usuario"> Usuario que realizou a anulação</param>
        [Transaction]
        public virtual void ProcessarAnulacaoCte(List<int> ctes, string chaveNfeAnulacao, string protocolo, Usuario usuario)
        {
            StringBuilder erros = new StringBuilder();
            NotaFiscalAnulacao notaAnulacao = null;
            Cte cte = null;
            List<CteDto> ctesProcessar = _cteRepository.ObterCtesPorListaIds(ctes);
            List<decimal> ctesAnular = _cteService.ObterCtesAgrupadosRateio(ctesProcessar.ToArray(), true);

            foreach (var id in ctesAnular)
            {
                cte = _cteRepository.ObterPorId((int)id);
                SalvarAnulacaoCte(chaveNfeAnulacao, protocolo, usuario, cte.Id ?? 0, erros);
            }

            /*
            Grava apenas 1 registro na interface do sap, 
            pois quando é enviado para o sap o CT-e de anulação 
            é recuperado todos os Ctes anulados com a mesma nota.
            */
            if (cte != null)
            {
                // Insere na interface de envio para o SAP
                _cteService.EnviarNotaAnulacaoSAP(cte, usuario);
            }
        }

        private void SalvarAnulacaoCte(string chaveNfeAnulacao, string protocolo, Usuario usuario, ////Cte cte, 
            int idCte,
            StringBuilder erros)
        {
            NotaFiscalAnulacao notaAnulacao;
            Cte cte = null;
            try
            {
                cte = _cteRepository.ObterPorId(idCte);
                notaAnulacao = _nfeAnulacaoSimconsultasRepository.ObterPorChaveNfe(chaveNfeAnulacao);
                notaAnulacao.Protocolo = protocolo;
                _nfeAnulacaoSimconsultasRepository.InserirOuAtualizar(notaAnulacao);

                cte.NfeAnulacao = notaAnulacao;


                var verificaEventoDesacordo = ObterValorConfGeral("CTE_EVENTO_DESACORDO");
                if (verificaEventoDesacordo == "S")
                {
                    VerificarEventoAcordo();
                }

                var enviaSefaz = ObterValorConfGeral("CTE_ANUL_CTR_ENVIA_SEFAZ");
                if (enviaSefaz == "S")
                {
                    cte.SituacaoAtual = SituacaoCteEnum.PendenteArquivoAnulacaoContribuinte;

                    _cteStatusRepository.InserirCteStatus(cte, usuario, new CteStatusRetorno { Id = 38 });

                    // Insere no log do cte
                    _cteLogService.InserirLogInfo(cte, "CteService", "CTE pedente para envio do arquivo de anulação sefaz. ");
                }
                else
                {
                    cte.SituacaoAtual = SituacaoCteEnum.Anulado;
                }

                _cteRepository.Atualizar(cte);
                _cteStatusRepository.InserirCteStatus(cte, usuario, new CteStatusRetorno { Id = 36 });
            }
            catch (Exception ex)
            {
                erros.AppendLine(string.Format("Erro ao processar o Ct-e ({0}). Mensagem: ",
                    cte != null ? cte.Numero : string.Empty, ex.Message));
            }
            ////return cte;
        }

        private void VerificarEventoAcordo()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        ///    Efetua a anulação do Cte enviando para a sefaz.
        /// </summary>
        /// <param name="ctes">Ctes a serem anuladoa</param>
        [Transaction]
        public virtual void ProcessarAnulacaoCteEnvioSefaz(List<CteDto> ctes, Usuario usuario)
        {
            var msgFinal = new StringBuilder();
            string mensagem = "";

            // Para cada um dos ctes atualizar o Status.
            foreach (var cte in ctes)
            {
                try
                {
                    Cte cteAnulado = _cteRepository.ObterPorChave(cte.Chave.Trim());

                    cteAnulado.DataAnulacao = DateTime.Now.Date;

                    // tomador 9 - não contribuinte
                    if (cteAnulado.FluxoComercial.Contrato.IndIeToma == 9)
                    {
                        cteAnulado.DataAnulacao = DateTime.Parse(cte.DataAnulacao);
                    }

                    cteAnulado.SituacaoAtual = SituacaoCteEnum.AnulacaoConfig;

                    _cteRepository.Atualizar(cteAnulado);
                    _cteStatusRepository.InserirCteStatus(cteAnulado, usuario, new CteStatusRetorno { Id = 38 });

                    // Insere no log do cte
                    _cteLogService.InserirLogInfo(cteAnulado, "CteService", "CTE pedente para envio do arquivo de anulação sefaz. ");
                    msgFinal.AppendLine(string.Format("Processado Ct-e ({0}), série {1}", cte != null ? cte.Numero : string.Empty, cte != null ? cte.SerieCte : string.Empty) + "</BR>");

                    /*
                    Grava apenas 1 registro na interface do sap, 
                    pois quando é enviado para o sap o CT-e de anulação 
                    é recuperado todos os Ctes anulados com a mesma nota.
                    */
                    ////if (cteAnulado != null)
                    ////{
                    // Insere na interface de envio para o SAP
                    ////    _cteService.EnviarNotaAnulacaoSAP(cteAnulado, usuario);
                    ////}
                }
                catch (Exception ex)
                {
                    msgFinal.AppendLine(string.Format("Erro ao processar o Ct-e ({0}), série {1}, Mensagem: {2}", cte != null ? cte.Numero : string.Empty, cte != null ? cte.SerieCte : string.Empty, ex.Message) + "</BR>");
                    throw new Exception(msgFinal.ToString());
                }
            }

            // Salva os ctes de anulação
            _cteService.SalvarAnulacaoCtes(ctes.ToArray(), usuario, out mensagem);
        }

        /// <summary>
        /// Verifica se a nfe já foi utilizada
        /// </summary>
        /// <param name="chaveNfe">chave da nfe de anulação</param>
        /// <returns>Retorna true se a nfe já foi utilizada</returns>
        public bool ValidaNfeJaUtilizada(string chaveNfe)
        {
            NotaFiscalAnulacao nfeAnulacao = _nfeAnulacaoSimconsultasRepository.ObterPorChaveNfe(chaveNfe);

            IList<Cte> ctes = _cteRepository.ObterPorChaveAnulacao(nfeAnulacao);

            return ctes.Count > 0;
        }

        /// <summary>
        /// Valida Valor total das ctes
        /// </summary>
        /// <param name="chaveNfe">chave da nfe de anulação</param>
        /// <param name="listaIdCte">Lista de Id dos ctes</param>
        /// <returns>Retorna true quando o valor das ctes somadas nao ultrapassar o valor da nota </returns>
        public bool ValidaValorCtes(string chaveNfe, List<int> listaIdCte)
        {
            string margem = ObterValorConfGeral(ConfGeralMargemLibNotaAnulacao);
            NotaFiscalAnulacao nfeAnulacao = _nfeAnulacaoSimconsultasRepository.ObterPorChaveNfe(chaveNfe);
            List<Cte> listaCtes = new List<Cte>();
            double somatoria = 0;

            foreach (var idCte in listaIdCte)
            {
                Cte cte = _cteRepository.ObterPorId(idCte);
                somatoria += cte.ValorCte;
            }

            if (Math.Abs(nfeAnulacao.Valor - somatoria) > Convert.ToDouble(margem))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// Verifica se os ctes são válidos para anulação.
        /// </summary>
        /// <param name="chaveNfe">chave da nfe de anulação</param>
        /// <param name="listaIdCte">Lista de Id dos ctes</param>
        /// <returns>Retorna true os ctes forem válidos.</returns>

        public bool ValidaAnulacaoCte(string chaveNfe, List<int> ctes)
        {
            string trava = ObterValorConfGeral("VALIDAR_FLUXO_TOMADOR_ANULACAO");

            if (trava.Equals("S"))
            {
                NotaFiscalAnulacao nfeAnulacao = _nfeAnulacaoSimconsultasRepository.ObterPorChaveNfe(chaveNfe);
                List<Cte> listaCtes = new List<Cte>();
                List<CteEmpresas> listaCteEmpresas = new List<CteEmpresas>();


                foreach (var cteTemp in ctes)
                {

                    Cte cte = _cteRepository.ObterPorId(cteTemp);
                    CteEmpresas cteEmpresas = _cteEmpresasRepository.ObterPorCte(cte);
                    listaCtes.Add(cte);
                    listaCteEmpresas.Add(cteEmpresas);
                }

                int countCteEmpresas = listaCteEmpresas.Where(ce => ce.EmpresaTomadora.Cgc == nfeAnulacao.CnpjEmitente).Count();

                if (countCteEmpresas < listaCtes.Count)
                {
                    return false;
                }

                /*
                    Solicitação do dia 24/03/2015
                    CT-es selecionados não precisam ser do mesmo fluxo.   
                    int countFluxos = listaCtes.Select(c => c.FluxoComercial.Codigo).Distinct().Count();

                    if (countFluxos != 1)
                    {
                        return false;
                    }
                */
            }

            return true;
        }

        /// <summary>
        /// Verifica se os ctes são válidos para anulação.
        /// </summary>
        /// <param name="chaveNfe">chave da nfe de anulação</param>
        /// <param name="listaIdCte">Lista de Id dos ctes</param>
        /// <returns>Retorna true os ctes forem válidos.</returns>
        public bool ValidaAnulacaoCteSefaz(List<CteDto> ctes)
        {
            bool result = true;
            foreach (var cte in ctes)
            {
                if (result == false) continue;

                var cteAnulacao = _cteRepository.ObterPorChave(cte.Chave);

                if (cteAnulacao.SituacaoAtual == SituacaoCteEnum.Anulado)
                {
                    result = false;
                }
                else
                    if (cteAnulacao.SituacaoAtual == SituacaoCteEnum.PendenteArquivoAnulacaoContribuinte)
                    {
                        result = false;
                    }
                    else
                        if (cteAnulacao.SituacaoAtual == SituacaoCteEnum.PendenteArquivoAnulacaoNContribuinte)
                        {
                            result = false;
                        }
            }
            return result;
        }

        /// <summary>
        /// Retorna a margem de liberacao permitida 
        /// </summary>
        /// <returns>Valor da margem</returns>
        public double ObterMargemNotaAnulacao()
        {
            double margem = 0;
            double.TryParse(ObterValorConfGeral(ConfGeralMargemLibNotaAnulacao), out margem);

            return margem;
        }

        /// <summary>
        /// Grava os dados da nota quando é obtida manualmente.
        /// </summary>
        /// <param name="chave"> Chave da nota</param>
        /// <param name="serie">Serie da Nfe</param>
        /// <param name="numero">Numero da Nfe</param>
        /// <param name="valorTotal">Valor Total da Nota</param>
        /// <param name="remetente">Remetente da nota</param>
        /// <param name="destinatario">Destinatario da nota</param>
        /// <param name="cnpjRemetente">Cnpj do remetente da nota</param>
        /// <param name="cnpjDestinatario">Cnpj do Destinatario da nota</param>
        /// <param name="dataNotaFiscal">Data da Nota</param>
        /// <param name="siglaRemetente">Sigla do Remetente</param>
        /// <param name="siglaDestinatario">Sigla do Destinatario</param>
        /// <param name="estadoRemetente"> UF Remetente da Nota</param>
        /// <param name="estadoDestinatario"> UF Destinatario da nota</param>
        /// <param name="inscricaoEstadualRemetente">IE do Remetente</param>
        /// <param name="inscricaoEstadualDestinatario">IE do Destinatario</param>
        /// <param name="protocolo">numero do protocolo de autorizacao na sefaz</param>
        /// <param name="usuario">Usuario que realizou a ação</param>
        public void ProcessarNfeManual(string chave, string serie, string numero, string valorTotal, string remetente, string destinatario, string cnpjRemetente, string cnpjDestinatario, string dataNotaFiscal, string siglaRemetente, string siglaDestinatario, string estadoRemetente, string estadoDestinatario, string inscricaoEstadualRemetente, string inscricaoEstadualDestinatario, string protocolo, Usuario usuario)
        {
            // Se for nota eletronica, então grava na tabela status nfe
            if (!string.IsNullOrEmpty(chave))
            {
                StatusNfeAnulacao statusNfe = _statusNfeAnulacaoRepository.ObterPorChaveNfe(chave);

                if (statusNfe == null)
                {
                    statusNfe = new StatusNfeAnulacao();
                    statusNfe.ChaveNfe = chave;
                    statusNfe.StatusRetornoNfe = _statusRetornoNfeRepository.ObterPorCodigo("NM1");
                }
                else
                {
                    if (statusNfe.StatusRetornoNfe != null && !statusNfe.StatusRetornoNfe.Codigo.Equals("V07"))
                    {
                        statusNfe.StatusRetornoNfe = _statusRetornoNfeRepository.ObterPorCodigo("NM1");
                    }
                }

                statusNfe.DataCadastro = DateTime.Now;
                statusNfe.Origem = "NfeManual";
                statusNfe.UsuarioCadastro = usuario;

                _statusNfeAnulacaoRepository.InserirOuAtualizar(statusNfe);
            }

            NotaFiscalAnulacao nfeSimconsultas = _nfeAnulacaoSimconsultasRepository.ObterPorChaveNfe(chave);

            if (nfeSimconsultas == null)
            {
                nfeSimconsultas = new NotaFiscalAnulacao();
                nfeSimconsultas.ChaveNfe = chave;
                nfeSimconsultas.ModeloNota = string.IsNullOrEmpty(chave) ? "1" : "55";
            }

            EmpresaCliente empresaEmit = _empresaClienteRepository.ObterPorSiglaSap(siglaRemetente);
            EmpresaCliente empresaDst = _empresaClienteRepository.ObterPorSiglaSap(siglaDestinatario);

            if (empresaEmit != null)
            {
                // Dados do emitente
                nfeSimconsultas.NomeFantasiaEmitente = empresaEmit.NomeFantasia;
                nfeSimconsultas.RazaoSocialEmitente = empresaEmit.RazaoSocial;
                nfeSimconsultas.UfEmitente = empresaEmit.Estado.Sigla;
                nfeSimconsultas.CepEmitente = empresaEmit.Cep.Replace(".", string.Empty).Replace("-", string.Empty).Trim();
                nfeSimconsultas.CnpjEmitente = empresaEmit.Cgc.Replace(".", string.Empty).Replace("-", string.Empty).Replace("/", string.Empty).Trim();

                nfeSimconsultas.CodigoMunicipioEmitente = empresaEmit.CidadeIbge.CodigoIbge.ToString();
                nfeSimconsultas.CodigoPaisEmitente = empresaEmit.Estado.Pais.Codigo;
                nfeSimconsultas.InscricaoEstadualEmitente = empresaEmit.InscricaoEstadual.Replace(".", string.Empty).Replace("-", string.Empty).Trim();

                nfeSimconsultas.LogradouroEmitente = empresaEmit.Endereco;
                nfeSimconsultas.MunicipioEmitente = empresaEmit.CidadeIbge.Descricao;
                nfeSimconsultas.PaisEmitente = empresaEmit.Estado.Pais.DescricaoResumida;
                nfeSimconsultas.TelefoneEmitente = empresaEmit.Telefone1;
            }

            if (empresaDst != null)
            {
                // Dados do Destinatario
                nfeSimconsultas.NomeFantasiaDestinatario = empresaDst.NomeFantasia;
                nfeSimconsultas.RazaoSocialDestinatario = empresaDst.RazaoSocial;
                nfeSimconsultas.UfDestinatario = empresaDst.Estado.Sigla;
                nfeSimconsultas.CepDestinatario = empresaDst.Cep.Replace(".", string.Empty).Replace("-", string.Empty).Trim();
                nfeSimconsultas.CnpjDestinatario = empresaDst.Cgc.Replace(".", string.Empty).Replace("-", string.Empty).Replace("/", string.Empty).Trim();

                nfeSimconsultas.CodigoMunicipioDestinatario = empresaDst.CidadeIbge.CodigoIbge.ToString();
                nfeSimconsultas.CodigoPaisDestinatario = empresaDst.Estado.Pais.Codigo;
                nfeSimconsultas.InscricaoEstadualDestinatario = empresaDst.InscricaoEstadual.Replace(".", string.Empty).Replace("-", string.Empty).Trim();

                nfeSimconsultas.LogradouroDestinatario = empresaDst.Endereco;
                nfeSimconsultas.MunicipioDestinatario = empresaDst.CidadeIbge.Descricao;
                nfeSimconsultas.PaisDestinatario = empresaDst.Estado.Pais.DescricaoResumida;
                nfeSimconsultas.TelefoneDestinatario = empresaDst.Telefone1;
            }

            // Dados da Nota
            nfeSimconsultas.SerieNotaFiscal = serie;
            nfeSimconsultas.Protocolo = protocolo;
            nfeSimconsultas.NumeroNotaFiscal = int.Parse(numero);
            nfeSimconsultas.Valor = double.Parse(valorTotal) / 100;
            nfeSimconsultas.DataEmissao = DateTime.Parse(dataNotaFiscal);

            _nfeAnulacaoSimconsultasRepository.InserirOuAtualizar(nfeSimconsultas);
        }

        private string ObterValorConfGeral(string chave)
        {
            ConfiguracaoTranslogic configuracaoTranslogic = _configuracaoTranslogicRepository.ObterPorId(chave);

            try
            {
                return configuracaoTranslogic.Valor;
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }
    }
}