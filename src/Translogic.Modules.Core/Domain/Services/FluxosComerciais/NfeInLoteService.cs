namespace Translogic.Modules.Core.Domain.Services.FluxosComerciais
{
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Net;
	using System.Threading;
	using Model.Dto;
	using Translogic.Modules.Core.Domain.Model.Diversos.Cte;

	/// <summary>
	/// Classe NfeInLoteService
	/// </summary>
	public class NfeInLoteService
	{
		private readonly NfeService _nfeService;
		private static object syncRoot = new object();

		/// <summary>
		/// Construtor da Classe
		/// </summary>
		/// <param name="nfeService">Servico de Nfe</param>
		public NfeInLoteService(NfeService nfeService)
		{
			if (StatusProcessamento == null)
			{
				StatusProcessamento = new Dictionary<string, KeyValuePair<int, string>>();
			}

			_nfeService = nfeService;
		}

		/// <summary>
		/// Status do processamento
		/// </summary>
		private static IDictionary<string, KeyValuePair<int, string>> StatusProcessamento { get; set; }

		/// <summary>
		/// Adiciona novo processamento.
		/// </summary>
		/// <param name="listaChaveNfe">Codigo do usuario.</param>
		public void AdicionarProcessamentoLote(List<string> listaChaveNfe)
		{
			foreach (string chave in listaChaveNfe)
			{
				AdicionarProcessamento(99, chave);
			}
		}

		/// <summary>
		/// Adiciona novo processamento.
		/// </summary>
		/// <param name="codErro"> C�digo de erro. </param>
		/// <param name="chaveNfe">Codigo do usuario.</param>
		public void AdicionarProcessamento(int codErro, string chaveNfe)
		{
			lock (syncRoot)
			{
				StatusProcessamento.Add(chaveNfe, new KeyValuePair<int, string>(codErro, "Processando..."));
			}
		}

		/// <summary>
		/// Atualiza status do processamento.
		/// </summary>
		/// <param name="chave"> chave da Nfe. </param>
		/// <param name="codErro"> C�digo de erro. </param>
		/// <param name="status"> Status do processamento. </param>
		public void AtualizarStatusProcessamento(string chave, int codErro, string status)
		{
			lock (syncRoot)
			{
				StatusProcessamento[chave] = new KeyValuePair<int, string>(codErro, status);
			}
		}

		/// <summary>
		/// Remove a chave Nfe especificado.
		/// </summary>
		/// <param name="chave">C�digo da chave Nfe.</param>
		public void RemoverProcessamento(string chave)
		{
			lock (syncRoot)
			{
				StatusProcessamento.Remove(chave);
			}
		}

		/// <summary>
		/// Remove a chave Nfe especificado.
		/// </summary>
		public void RemoverTodosProcessamento()
		{
			StatusProcessamento = new Dictionary<string, KeyValuePair<int, string>>();
		}

		/// <summary>
		/// Verifica se existe algum processamento rodando.
		/// </summary>
		/// <returns> Valor booleano </returns>
		public bool VerificarExisteProcessamento()
		{
			lock (syncRoot)
			{
				return StatusProcessamento.Count > 0;
			}
		}

		/// <summary>
		/// Obt�m o usu�rio que est� processando.
		/// </summary>
		/// <returns> String do c�digo do usuario </returns>
		public string ObterChaveProcessamento()
		{
			lock (syncRoot)
			{
				return StatusProcessamento.Keys.First();
			}
		}

		/// <summary>
		/// Obt�m o status do processamento
		/// </summary>
		/// <returns> Status do processamento existente. </returns>
		public IDictionary<string, KeyValuePair<int, string>> ObterStatusProcessamento()
		{
			lock (syncRoot)
			{
				/*Dictionary<string, KeyValuePair<int, string>> retorno = new Dictionary<string, KeyValuePair<int, string>>();
				
				StatusProcessamento.

				foreach (string chaveNfe in listaChave)
				{
					if (StatusProcessamento.Keys.Count(x => x == chaveNfe) == 1)
					{
						retorno.Add(chaveNfe, StatusProcessamento[chaveNfe]);
					}
				}
				*/
				return StatusProcessamento;
			}
		}

		/// <summary>
		/// Inicia o processamento de consolida��o
		/// </summary>
		/// <param name="chaveNfe"> Codigo da chave Nfe.  </param>
		/// <returns> C�digo da chave. </returns>
		public List<string> IniciarProcessamento(List<string> chaveNfe)
		{
			ProcessarNfe(chaveNfe);
			return chaveNfe;
		}

		/// <summary>
		/// Processar tread
		/// </summary>
		/// <param name="stateInfo">informa��o de estado da thread</param>
		protected void ProcessarThread(object stateInfo)
		{
			NfeBoAsync nfeBoAsync = stateInfo as NfeBoAsync;

			try
			{
                if (nfeBoAsync.ChaveNfe.Length < 44 || nfeBoAsync.ChaveNfe.Substring(20, 2) != "55")
                {
                    throw new Exception("Chave de NFe inv�lida, favor informar uma chave de modelo 55!");
                }

			    int codigoRetorno;
				// Obtem os dados da Nfe no SimConsulta e Grava na base.
				StatusNfeDto status = _nfeService.ObterDadosNfe(nfeBoAsync.ChaveNfe, TelaProcessamentoNfe.PreCarregamento, true, false);

				if (!status.ObtidaComSucesso)
				{
					AtualizarStatusProcessamento(nfeBoAsync.ChaveNfe, 1, string.Concat(status.CodigoStatusRetorno, status.MensagemErro));
				}
				else
				{
					// Codigo 0 - Sucesso, ent�o remove a nota do pooling
					AtualizarStatusProcessamento(nfeBoAsync.ChaveNfe, 0, "NFe processada com sucesso.");
					// RemoverProcessamento(nfeBoAsync.chaveNfe);
				}
			}
			catch (NHibernate.StaleStateException ex)
			{
				AtualizarStatusProcessamento(nfeBoAsync.ChaveNfe, 8, ex.Message);
			}
			catch (Exception ex)
			{
				AtualizarStatusProcessamento(nfeBoAsync.ChaveNfe, 9, ex.Message);
			}
			finally
			{
				// Sinaliza que o processo est� conclu�do.
				nfeBoAsync.ManualResetEvent.Set();
			}
		}

		/// <summary>
		/// Processar Nfes
		/// </summary>
		/// <param name="listaNFe">Lista de Chave a serem processadas</param>
		private void ProcessarNfe(List<string> listaNFe)
		{
			int index = 0;

			if (listaNFe != null && listaNFe.Count > 0)
			{
				ServicePointManager.DefaultConnectionLimit = 200;
				// ServicePointManager.MaxServicePointIdleTime = 10000;

				int minWorker, minCompletionPortThreads;
				ThreadPool.GetMinThreads(out minWorker, out minCompletionPortThreads);
				// without setting the thread pool up, there was enough of a delay to cause timeouts!
				ThreadPool.SetMinThreads(listaNFe.Count, minCompletionPortThreads);

				// Inst�ncia ManualResetEvent com a quantidade de notas
				ManualResetEvent[] doneEvents = new ManualResetEvent[listaNFe.Count];

				foreach (string chave in listaNFe)
				{
					// inst�ncia o ManualResetEvent
					doneEvents[index] = new ManualResetEvent(false);
					// nfePooling.IndEmProcessamento = true;
					// _nfePoolingRepository.Atualizar(nfePooling);

					// Inst�ncia a NfeBoAsync passando os objetos necess�rios para consultar a Nfe
					NfeBoAsync nfeBoAsync = new NfeBoAsync
					{
						ManualResetEvent = doneEvents[index],
						ChaveNfe = chave
					};

					// insere a execu��o no pooling
					ThreadPool.QueueUserWorkItem(ProcessarThread, nfeBoAsync);

					// Incrementa o index do array de ManualResetEvent
					index++;
				}

				// Aguarda todas as notas serem processadas
				WaitHandle.WaitAll(doneEvents);
			}
		}

		#region "Classe NfeBoAsync - Respons�vel por executar a consulta da Nfe"

		/// <summary>
		/// Classe Interna para executar a thread
		/// </summary>
		protected class NfeBoAsync
		{
			/// <summary>
			/// Manual Reset Event
			/// </summary>
			public ManualResetEvent ManualResetEvent { get; set; }

			/// <summary>
			/// Chave da nfe
			/// </summary>
			public string ChaveNfe { get; set; }
		}

		#endregion
	}
}