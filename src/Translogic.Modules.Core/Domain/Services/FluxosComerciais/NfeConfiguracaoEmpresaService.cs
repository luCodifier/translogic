﻿namespace Translogic.Modules.Core.Domain.Services.FluxosComerciais
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml;
    using ALL.Core.Dominio;
    using Translogic.Core.Infrastructure;
    using Translogic.Core.Infrastructure.Web;
    using Translogic.Modules.Core.Domain.Model.Acesso;
    using Translogic.Modules.Core.Domain.Model.Codificador;
    using Translogic.Modules.Core.Domain.Model.Codificador.Repositories;
    using Translogic.Modules.Core.Domain.Model.Diversos.Cte;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Nfes;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Nfes.Repositories;

    /// <summary>
    /// NfeConfiguracaoEmpresa Service
    /// </summary>
    public class NfeConfiguracaoEmpresaService
    {
        private readonly INfeConfiguracaoEmpresaRepository _nfeConfiguracaoEmpresaRepository;
        private readonly INfeConfiguracaoEmpresaUnidadeMedidaRepository _nfeConfiguracaoEmpresaUnidadeMedidaRepository;
        private readonly IConfiguracaoTranslogicRepository _configuracaoTranslogicRepository;

        /// <summary>
        /// Construtor Padraoi
        /// </summary>
        /// <param name="nfeConfiguracaoEmpresaRepository">nfeConfiguracaoEmpresa Repository</param>
        /// <param name="nfeConfiguracaoEmpresaUnidadeMedidaRepository">nfeConfiguracaoEmpresaUnidadeMedida Repository</param>
        /// <param name="configuracaoTranslogicRepository"> The configuracao translogic repository. </param>
        public NfeConfiguracaoEmpresaService(INfeConfiguracaoEmpresaRepository nfeConfiguracaoEmpresaRepository, INfeConfiguracaoEmpresaUnidadeMedidaRepository nfeConfiguracaoEmpresaUnidadeMedidaRepository, IConfiguracaoTranslogicRepository configuracaoTranslogicRepository)
        {
            _nfeConfiguracaoEmpresaRepository = nfeConfiguracaoEmpresaRepository;
            _nfeConfiguracaoEmpresaUnidadeMedidaRepository = nfeConfiguracaoEmpresaUnidadeMedidaRepository;
            _configuracaoTranslogicRepository = configuracaoTranslogicRepository;
        }

        /// <summary>
        /// ObterPesoVolume Nfe
        /// </summary>
        /// <param name="listaDeVolumes">lista De Volumes</param>
        /// <param name="namespaceManager">namespace Manager</param>
        /// <param name="nfe">nfe simconsultas</param>
        /// <param name="peso_bruto">peso Bruto</param>
        public void ObterPesoVolumeNfe(XmlNodeList listaDeVolumes, XmlNamespaceManager namespaceManager, ref NotaFiscalEletronica nfe, ref double peso_bruto)
        {
            ConfiguracaoTranslogic configuracaoTranslogic = _configuracaoTranslogicRepository.ObterPorId("TRANSPORTE_UNIDADE_COMERCIAL");
            NfeConfiguracaoEmpresa configuracaoEmpresa = _nfeConfiguracaoEmpresaRepository.ObterPorCnpj(nfe.CnpjEmitente);

            var funcoesDePesoEVolume = new List<ProcessamentoNfeDto>
                {
                    new ProcessamentoNfeDto { FuncaoSimconsultas = ObterPesoBrutoDoTransporte, PriorizarVolume = false, PriorizarProdutos = false },
                    new ProcessamentoNfeDto { FuncaoSimconsultas = ObterPesoLiquidoDoTransporte, PriorizarVolume = false, PriorizarProdutos = false },
                    new ProcessamentoNfeDto { FuncaoSimconsultas = ObterVolumeDoTransporte, PriorizarVolume = true, PriorizarProdutos = false },
                    new ProcessamentoNfeDto { FuncaoSimconsultas = ObterPesoBrutoDosProdutos, PriorizarVolume = false, PriorizarProdutos = true },
                    new ProcessamentoNfeDto { FuncaoSimconsultas = ObterPesoLiquidoDosProdutos, PriorizarVolume = false, PriorizarProdutos = true },
                    new ProcessamentoNfeDto { FuncaoSimconsultas = ObterVolumeDosProdutos, PriorizarVolume = true, PriorizarProdutos = true },
                };

            string[] arraySiglasUnidadesComerciais = null;
            if (configuracaoTranslogic != null && !string.IsNullOrEmpty(configuracaoTranslogic.Valor))
            {
                arraySiglasUnidadesComerciais = configuracaoTranslogic.Valor.Split(',');
            }

            // Se a unidade comercial for igual a UN ou FD, zera o peso e obtém do transporte
            if (nfe.ListaProdutos.Any(x => arraySiglasUnidadesComerciais != null && arraySiglasUnidadesComerciais.Contains(x.UnidadeComercial)))
            {
                nfe.PesoBruto = 0;
                nfe.Peso = 0;
            }

            if (configuracaoEmpresa != null)
            {
                IOrderedEnumerable<ProcessamentoNfeDto> funcoesOrdenadas;

                if (configuracaoEmpresa.PriorizarProdutos)
                {
                    funcoesOrdenadas = funcoesDePesoEVolume.OrderByDescending(f => f.PriorizarProdutos);
                }
                else
                {
                    funcoesOrdenadas = funcoesDePesoEVolume.OrderBy(f => f.PriorizarProdutos);
                }

                if (configuracaoEmpresa.PriorizarVolume)
                {
                    funcoesOrdenadas = funcoesOrdenadas.ThenByDescending(f => f.PriorizarVolume);
                }
                else
                {
                    funcoesOrdenadas = funcoesOrdenadas.ThenBy(f => f.PriorizarVolume);
                }

                funcoesDePesoEVolume = funcoesOrdenadas.ToList();
            }

            foreach (var funcao in funcoesDePesoEVolume)
            {
                funcao.FuncaoSimconsultas.Invoke(listaDeVolumes, namespaceManager, ref nfe, configuracaoEmpresa, ref peso_bruto);
            }
        }

        /// <summary>
        /// Método para Obter as entidades
        /// </summary>
        /// <param name="pagination">
        /// Paginação da Grid
        /// </param>
        /// <param name="value">
        /// Descrição do Registro
        /// </param>
        /// <returns>
        /// lista paginada
        /// </returns>
        public ResultadoPaginado<NfeConfiguracaoEmpresa> ListarPorTrechoDeCnpj(DetalhesPaginacaoWeb pagination, string value)
        {
            return _nfeConfiguracaoEmpresaRepository.ListarPorTrechoDeCnpj(pagination, value);
        }

        /// <summary>
        /// Obter Por Id
        /// </summary>
        /// <param name="id">id da entidade</param>
        /// <returns>entidade populada</returns>
        public NfeConfiguracaoEmpresa ObterPorId(int id)
        {
            return _nfeConfiguracaoEmpresaRepository.ObterPorId(id);
        }

        /// <summary>
        /// Salva a entidade
        /// </summary>
        /// <param name="id">id da entidade</param>
        /// <param name="cnpj">cnpj da entidade</param>
        /// <param name="usuario">usuario da operacao</param>
        /// <param name="priorizarVolume">priorizar Volume</param>
        /// <param name="priorizarProdutos">priorizar Produtos</param>
        public void Salvar(int id, string cnpj, Usuario usuario, bool priorizarVolume, bool priorizarProdutos)
        {
            NfeConfiguracaoEmpresa registro;

            if (id == 0)
            {
                registro = new NfeConfiguracaoEmpresa();
            }
            else
            {
                registro = _nfeConfiguracaoEmpresaRepository.ObterPorId(id);
            }

            registro.Cnpj = cnpj.Trim();
            registro.PriorizarVolume = priorizarVolume;
            registro.PriorizarProdutos = priorizarProdutos;

            // -> Verificar se já existe pelo cnpj
            if (_nfeConfiguracaoEmpresaRepository.CnpjJaExiste(registro.Cnpj, registro.Id))
            {
                throw new TranslogicException("Já existe o Cnpj cadastrado!");
            }

            _nfeConfiguracaoEmpresaRepository.InserirOuAtualizar(registro);
        }

        /// <summary>
        /// Método para Excluir o registro
        /// </summary>
        /// <param name="id">
        /// Parametro o Id do registro
        /// </param>
        public void Excluir(int id)
        {
            _nfeConfiguracaoEmpresaRepository.Remover(id);
        }

        /// <summary>bruto do transporte
        /// obtem peso 
        /// </summary>
        /// <param name="listaDeVolumes">lista De Volumes</param>
        /// <param name="namespaceManager">namespace Manager</param>
        /// <param name="nfe">nfe sim consultas</param>
        /// <param name="nfeConfiguracaoEmpresa">Configuração da empresa emitente</param>
        /// <param name="peso_bruto">peso_bruto da StatusNFe</param>
        private void ObterPesoBrutoDoTransporte(XmlNodeList listaDeVolumes, XmlNamespaceManager namespaceManager, ref NotaFiscalEletronica nfe, NfeConfiguracaoEmpresa nfeConfiguracaoEmpresa, ref double peso_bruto)
        {
            double result = 0;
            var atualizaPeso = true;

            if ((!nfe.PesoBruto.Equals(0) || listaDeVolumes == null) && !(nfeConfiguracaoEmpresa != null && nfeConfiguracaoEmpresa.PriorizarVolume))
            {
                nfe.PesoBruto = nfe.PesoBruto < 150 ? nfe.PesoBruto * 1000 : nfe.PesoBruto;
                atualizaPeso = false;
            }


            if (listaDeVolumes != null)
            {
                if (atualizaPeso)
                {
                    nfe.PesoBruto = nfe.PesoBruto < 150 ? nfe.PesoBruto * 1000 : nfe.PesoBruto;
                }

                bool? indPesoToneladas = VerificarEmpresaPesoTonelada(nfe.CnpjEmitente);

                foreach (XmlNode noVolume in listaDeVolumes)
                {
                    var pesoBruto = NfeService.ObterDoubleDoXML(noVolume, "pesoB", namespaceManager);

                    // -> Peso Bruto do Transporte
                    if (atualizaPeso)
                    {
                        if (indPesoToneladas == true || pesoBruto < 150)
                        {
                            pesoBruto = pesoBruto * 1000;
                        }
                    }

                    result += pesoBruto;
                }
            }


            if ((result > nfe.PesoBruto) && atualizaPeso)
            {
                nfe.PesoBruto = result;
            }

            peso_bruto = result / 1000;
        }

        /// <summary>
        /// obtem peso liquido do transporte
        /// </summary>
        /// <param name="listaDeVolumes">lista De Volumes</param>
        /// <param name="namespaceManager">namespace Manager</param>
        /// <param name="nfe">nfe sim consultas</param>
        /// <param name="nfeConfiguracaoEmpresa">Configuração da empresa emitente</param>
        /// <param name="peso_bruto">peso_bruto da StatusNFe</param>
        private void ObterPesoLiquidoDoTransporte(XmlNodeList listaDeVolumes, XmlNamespaceManager namespaceManager, ref NotaFiscalEletronica nfe, NfeConfiguracaoEmpresa nfeConfiguracaoEmpresa, ref double peso_bruto)
        {
            double result = 0;

            if ((!nfe.Peso.Equals(0) || listaDeVolumes == null) && !(nfeConfiguracaoEmpresa != null && nfeConfiguracaoEmpresa.PriorizarVolume))
            {
                nfe.Peso = nfe.Peso < 150 ? nfe.Peso * 1000 : nfe.Peso;
                return;
            }

            if (listaDeVolumes != null)
            {
                nfe.Peso = nfe.Peso < 150 ? nfe.Peso * 1000 : nfe.Peso;

                bool? indPesoToneladas = VerificarEmpresaPesoTonelada(nfe.CnpjEmitente);

                foreach (XmlNode noVolume in listaDeVolumes)
                {
                    var peso = NfeService.ObterDoubleDoXML(noVolume, "pesoL", namespaceManager);

                    // -> Peso Bruto do Transporte
                    if (indPesoToneladas == true || peso < 150)
                    {
                        peso = peso * 1000;
                    }

                    result += peso;
                }
            }

            if (result > nfe.Peso)
            {
                nfe.Peso = result;
            }
        }

        /// <summary>
        /// obtem volume do transporte
        /// </summary>
        /// <param name="listaDeVolumes">lista De Volumes</param>
        /// <param name="namespaceManager">namespace Manager</param>
        /// <param name="nfe">nfe sim consultas</param>
        /// <param name="nfeConfiguracaoEmpresa">Configuração da empresa emitente</param>
        /// <param name="peso_bruto">peso_bruto da StatusNFe</param>
        private void ObterVolumeDoTransporte(XmlNodeList listaDeVolumes, XmlNamespaceManager namespaceManager, ref NotaFiscalEletronica nfe, NfeConfiguracaoEmpresa nfeConfiguracaoEmpresa, ref double peso_bruto)
        {
            double result = 0;

            if ((nfe.Volume.HasValue && nfe.Volume.Value > 0) || listaDeVolumes == null)
            {
                nfe.Volume = nfe.Volume.HasValue ? (nfe.Volume > 1000 ? nfe.Volume / 1000 : nfe.Volume) : 0;
                return;
            }

            foreach (XmlNode noVolume in listaDeVolumes)
            {
                var volume = NfeService.ObterDoubleDoXML(noVolume, "qVol", namespaceManager);

                // -> Volume do Transporte
                if (volume > 1000)
                {
                    volume = volume / 1000;
                }

                result += volume;
            }

            nfe.Volume = result;
        }

        /// <summary>
        /// obtem peso bruto dos produtos
        /// </summary>
        /// <param name="listaDeVolumes">lista De Volumes</param>
        /// <param name="namespaceManager">namespace Manager</param>
        /// <param name="nfe">nfe sim consultas</param>
        /// <param name="nfeConfiguracaoEmpresa">Configuração da empresa emitente</param>
        /// <param name="peso_bruto">peso_bruto da StatusNFe</param>
        private void ObterPesoBrutoDosProdutos(XmlNodeList listaDeVolumes, XmlNamespaceManager namespaceManager, ref NotaFiscalEletronica nfe, NfeConfiguracaoEmpresa nfeConfiguracaoEmpresa, ref double peso_bruto)
        {
            // -> Verifica se precisará calcular os Pesos pela somatória dos produtos
            //    (Nessa caso, assume-se que o peso bruto e liquido sejam os mesmos)
            if (nfe.PesoBruto.Equals(0))
            {
                nfe.PesoBruto = ObterPesoTotalToneladas(nfe.ListaProdutos) * 1000;
            }
        }

        /// <summary>
        /// obtem peso liquido dos produtos
        /// </summary>
        /// <param name="listaDeVolumes">lista De Volumes</param>
        /// <param name="namespaceManager">namespace Manager</param>
        /// <param name="nfe">nfe sim consultas</param>
        /// <param name="nfeConfiguracaoEmpresa">Configuração da empresa emitente</param>
        /// <param name="peso_bruto">peso_bruto da StatusNFe</param>
        private void ObterPesoLiquidoDosProdutos(XmlNodeList listaDeVolumes, XmlNamespaceManager namespaceManager, ref NotaFiscalEletronica nfe, NfeConfiguracaoEmpresa nfeConfiguracaoEmpresa, ref double peso_bruto)
        {
            // -> Verifica se precisará calcular os Pesos pela somatória dos produtos
            //    (Nessa caso, assume-se que o peso bruto e liquido sejam os mesmos)
            if (nfe.Peso.Equals(0))
            {
                nfe.Peso = ObterPesoTotalToneladas(nfe.ListaProdutos) * 1000;
            }
        }

        /// <summary>
        /// obtem volume dos produtos 
        /// </summary>
        /// <param name="listaDeVolumes">lista De Volumes</param>
        /// <param name="namespaceManager">namespace Manager</param>
        /// <param name="nfe">nfe sim consultas</param>
        /// <param name="nfeConfiguracaoEmpresa">Configuração da empresa emitente</param>
        /// <param name="peso_bruto">peso_bruto da StatusNFe</param>
        private void ObterVolumeDosProdutos(XmlNodeList listaDeVolumes, XmlNamespaceManager namespaceManager, ref NotaFiscalEletronica nfe, NfeConfiguracaoEmpresa nfeConfiguracaoEmpresa, ref double peso_bruto)
        {
            // -> Verifica se precisará calcular o Volume pela somatória dos produtos
            if (!nfe.Volume.HasValue || nfe.Volume.Value <= 0)
            {
                nfe.Volume = ObterVolume(nfe.ListaProdutos);
            }
        }

        private bool? VerificarEmpresaPesoTonelada(string cnpjEmitente)
        {
            NfeConfiguracaoEmpresaUnidadeMedida nfeConfiguracaoEmpresaUnidadeMedida = _nfeConfiguracaoEmpresaUnidadeMedidaRepository.ObterPorCnpj(cnpjEmitente);
            if (nfeConfiguracaoEmpresaUnidadeMedida == null)
            {
                return null;
            }

            return nfeConfiguracaoEmpresaUnidadeMedida.UnidadeMedida.Equals("TO");
        }

        /// <summary>
        /// Obtém o peso total dos produtos em toneladas
        /// </summary>
        /// <param name="produtos">Lista de produtos da nota</param>
        /// <returns>Peso total em toneladas</returns>
        private double ObterPesoTotalToneladas(IList<NotaFiscalEletronicaProduto> produtos)
        {
            if (produtos.Count > 0)
            {
                string unidadeComercial = produtos[0].UnidadeComercial;
                UnidadeComercialNfeEnum unidComercialEnum = ObterUnidadeComercialNfe(unidadeComercial);
                switch (unidComercialEnum)
                {
                    case UnidadeComercialNfeEnum.Toneladas:
                        return produtos.Sum(c => c.QuantidadeComercial);
                    case UnidadeComercialNfeEnum.Quilos:
                        return produtos.Sum(c => c.QuantidadeComercial) / 1000;
                    case UnidadeComercialNfeEnum.Litros:
                    case UnidadeComercialNfeEnum.MetrosCubicos:
                    case UnidadeComercialNfeEnum.Outros:
                        return 0;
                }
            }

            return 0;
        }

        private UnidadeComercialNfeEnum ObterUnidadeComercialNfe(string unidadeComercial)
        {
            unidadeComercial = unidadeComercial.ToUpperInvariant();
            if (unidadeComercial.StartsWith("K"))
            {
                return UnidadeComercialNfeEnum.Quilos;
            }

            if (unidadeComercial.StartsWith("T"))
            {
                return UnidadeComercialNfeEnum.Toneladas;
            }

            if (unidadeComercial.StartsWith("L"))
            {
                return UnidadeComercialNfeEnum.Litros;
            }

            if (unidadeComercial.Equals("M3"))
            {
                return UnidadeComercialNfeEnum.MetrosCubicos;
            }

            return UnidadeComercialNfeEnum.Outros;
        }

        /// <summary>
        /// Obtém o volume na nota em milhares de litros
        /// </summary>
        /// <param name="produtos">Lista de produtos da nfe</param>
        /// <returns>Volume em milhares de litros</returns>
        private double ObterVolume(IList<NotaFiscalEletronicaProduto> produtos)
        {
            if (produtos.Count > 0)
            {
                string unidadeComercial = produtos[0].UnidadeComercial;
                UnidadeComercialNfeEnum unidComercialEnum = ObterUnidadeComercialNfe(unidadeComercial);
                switch (unidComercialEnum)
                {
                    case UnidadeComercialNfeEnum.Litros:
                        return produtos.Sum(c => c.QuantidadeComercial) / 1000;
                    case UnidadeComercialNfeEnum.MetrosCubicos:
                        return produtos.Sum(c => c.QuantidadeComercial);
                    case UnidadeComercialNfeEnum.Toneladas:
                    case UnidadeComercialNfeEnum.Quilos:
                    case UnidadeComercialNfeEnum.Outros:
                        return 0;
                }
            }

            return 0;
        }
    }
}