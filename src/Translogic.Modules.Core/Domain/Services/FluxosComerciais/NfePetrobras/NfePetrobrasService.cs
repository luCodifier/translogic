﻿namespace Translogic.Modules.Core.Domain.Services.FluxosComerciais.NfePetrobras
{
    using System;
    using System.IO;
    using System.Net;
    using System.Text;
    using System.Xml;
    using System.Xml.Linq;
    using System.Xml.Serialization;
    using EDI.Domain.Models.Nfes;
    using EDI.Domain.Models.Nfes.Repositories;
    using Model.FluxosComerciais.NfePetrobras.Outputs;

    public class NfePetrobrasService
    {
        private readonly INfeEdiRepository _nfeEdiRepository;

        /// <summary>
        /// Serviço que recebe as notas da Petrobras
        /// </summary>
        /// <param name="nfeEdiRepository">Repositorio de notas do EDI</param>
        public NfePetrobrasService(INfeEdiRepository nfeEdiRepository)
        {
            _nfeEdiRepository = nfeEdiRepository;
        }

        /// <summary>
        /// Metodo que chama o serviço de notas da petrobras
        /// </summary>
        /// <param name="input">dados de entrada para a requisicao</param>
        public void ObterNotasBr(NfePetrobrasRequest input)
        {
            string urlRequest =
                string.Format("{0}?cnpj={1}&dataEmissao={2}&horaEmissaoInicial={3}&horaEmissaoFinal={4}&token={5}",
                              input.Url, input.Cnpj, input.DataEmissao, input.HoraEmissaoInicial, input.HoraEmissaoFinal,
                              input.Token);

            var request = (HttpWebRequest) WebRequest.Create(urlRequest);

            /*XDocument doc;
            using (WebResponse response = request.GetResponse())
            {
                using (Stream stream = response.GetResponseStream())
                {
                    doc = XDocument.Load(stream);
                }
            }
            Console.WriteLine(doc);*/

            Stream x = request.GetResponse().GetResponseStream();

            Encoding encoding = Encoding.ASCII;

            if (x == null) return;
            using (var reader = new StreamReader(x, encoding))
            {
                var serializer = new XmlSerializer(typeof (NFesBr));
                var notas = (NFesBr) serializer.Deserialize(reader);

                Console.Write(reader);

                reader.Close();

                foreach (DadosNfe nota in notas.NFes)
                {
                    SalvarNota(nota);
                }
            }
        }

        /// <summary>
        /// Salva as notas da petrobras na base do Edi
        /// </summary>
        /// <param name="nfePetrobras">Objeto parseado do xml</param>
        public void SalvarNota(DadosNfe nfePetrobras)
        {
            var nfe = new NfeEdi();
            nfe.ChaveNfe = nfePetrobras.IdentificadorNotaFiscal;

            // DADOS DESTINATARIO 
            try
            {
                nfe.CnpjDestinatario = nfePetrobras.DadosDestinatario.CNPJ == null ? null : nfePetrobras.DadosDestinatario.CNPJ;
                nfe.BairroDestinatario = nfePetrobras.DadosDestinatario.Bairro == null ? null : nfePetrobras.DadosDestinatario.Bairro;
                nfe.CepDestinatario = nfePetrobras.DadosDestinatario.CEP == null ? null : nfePetrobras.DadosDestinatario.CEP;
                nfe.CodigoMunicipioDestinatario = nfePetrobras.DadosDestinatario.CodigoCidade == null ? null : nfePetrobras.DadosDestinatario.CodigoCidade;
                nfe.CodigoPaisDestinatario = nfePetrobras.DadosDestinatario.ChavePais == null ? null : nfePetrobras.DadosDestinatario.ChavePais;
                nfe.LogradouroDestinatario = nfePetrobras.DadosDestinatario.Logradouro == null ? null : nfePetrobras.DadosDestinatario.Logradouro;
                nfe.MunicipioDestinatario = nfePetrobras.DadosDestinatario.NomeCidade == null ? null : nfePetrobras.DadosDestinatario.NomeCidade;
                nfe.NumeroDestinatario = nfePetrobras.DadosDestinatario.Numero == null ? null : nfePetrobras.DadosDestinatario.Numero;
                nfe.RazaoSocialDestinatario = nfePetrobras.DadosDestinatario.NomeEmpresa == null ? null : nfePetrobras.DadosDestinatario.NomeEmpresa;
                nfe.UfDestinatario = nfePetrobras.DadosDestinatario.UF == null ? null : nfePetrobras.DadosDestinatario.UF;
            }
            catch (Exception e)
            {
                Console.Write(e);
            }

            // DADOS EMITENTE
            try
            {
                nfe.CnpjEmitente = nfePetrobras.DadosEmissor.CNPJ == null ? null : nfePetrobras.DadosEmissor.CNPJ;
                nfe.BairroEmitente = nfePetrobras.DadosEmissor.Bairro == null ? null : nfePetrobras.DadosEmissor.Bairro;
                nfe.CepEmitente = nfePetrobras.DadosEmissor.CEP == null ? null : nfePetrobras.DadosEmissor.CEP;
                nfe.CodigoMunicipioEmitente = nfePetrobras.DadosEmissor.CodigoCidade == null ? null : nfePetrobras.DadosEmissor.CodigoCidade;
                nfe.CodigoPaisEmitente = nfePetrobras.DadosEmissor.ChavePais == null ? null : nfePetrobras.DadosEmissor.ChavePais;
                nfe.LogradouroEmitente = nfePetrobras.DadosEmissor.Logradouro == null ? null : nfePetrobras.DadosEmissor.Logradouro;
                nfe.MunicipioEmitente = nfePetrobras.DadosEmissor.NomeCidade == null ? null : nfePetrobras.DadosEmissor.NomeCidade;
                nfe.NumeroEmitente = nfePetrobras.DadosEmissor.Numero == null ? null : nfePetrobras.DadosEmissor.Numero;
                nfe.RazaoSocialEmitente = nfePetrobras.DadosEmissor.NomeEmpresa == null ? null : nfePetrobras.DadosEmissor.NomeEmpresa;
                nfe.UfEmitente = nfePetrobras.DadosEmissor.UF == null ? null : nfePetrobras.DadosEmissor.UF;
            }
            catch (Exception e)
            {
                Console.Write(e);
            }

            // DADOS TRANSPORTADORA
            try
            {
                nfe.CnpjTransportadora = nfePetrobras.DadosTransporte.Transportador.CNPJ == null ? null : nfePetrobras.DadosTransporte.Transportador.CNPJ;
                nfe.EnderecoTransportadora = nfePetrobras.DadosTransporte.Transportador.EnderecoCompleto == null ? null : nfePetrobras.DadosTransporte.Transportador.EnderecoCompleto;
                nfe.NomeTransportadora = nfePetrobras.DadosTransporte.Transportador.Nome == null ? null : nfePetrobras.DadosTransporte.Transportador.Nome;
                nfe.UfTransportadora = nfePetrobras.DadosTransporte.Transportador.UF == null ? null : nfePetrobras.DadosTransporte.Transportador.UF;
            }
            catch (Exception e)
            {
                Console.Write(e);
            }

            // DADOS NOTA FISCAL
            try
            {
                nfe.DataEmissao = nfePetrobras.DadosIdentificacao.Etapa.DadosEtapa.DataEmissao;
                nfe.DataHoraGravacao = DateTime.Now;
                nfe.NumeroNotaFiscal = nfePetrobras.DadosIdentificacao.Etapa.DadosEtapa.NumeroNFe == null ? 0 : Convert.ToInt32(nfePetrobras.DadosIdentificacao.Etapa.DadosEtapa.NumeroNFe);
                nfe.SerieNotaFiscal = nfePetrobras.DadosIdentificacao.Etapa.DadosEtapa.Serie == null ? null : nfePetrobras.DadosIdentificacao.Etapa.DadosEtapa.Serie;
                
            }
            catch (Exception e)
            {
                Console.Write(e);
            }

            // DADOS VEICULO
            try
            {
                nfe.Placa = nfePetrobras.DadosTransporte.Placa == null ? null : nfePetrobras.DadosTransporte.Placa;
                nfe.PlacaReboque = nfePetrobras.DadosTransporte.Reboque.Placa == null ? null : nfePetrobras.DadosTransporte.Reboque.Placa;
            }
            catch (Exception e)
            {
                Console.Write(e);
            }

            // VALORES DA NOTA
            try
            {
                if(nfePetrobras.DadosIdentificacao.Etapa.Valor != null)
                {
                    nfe.Valor = Convert.ToDouble(nfePetrobras.DadosIdentificacao.Etapa.Valor);
                }
                if (nfePetrobras.DadosFrete.ValorBaseICMS != null)
                {
                    nfe.ValorBaseCalculoIcms = Convert.ToDouble(nfePetrobras.DadosFrete.ValorBaseICMS);
                }
                if (nfePetrobras.DadosFrete.Valor != null)
                {
                    nfe.ValorFrete = Convert.ToDouble(nfePetrobras.DadosFrete.Valor);
                }
                if (nfePetrobras.DadosFrete.ValorICMS != null)
                {
                    nfe.ValorIcms = Convert.ToDouble(nfePetrobras.DadosFrete.ValorICMS);
                }
            }
            catch (Exception e)
            {
                Console.Write(e);
            }

            _nfeEdiRepository.Inserir(nfe);
        }
    }

    /// <summary>
    /// Objeto para montar requisicao do servico
    /// </summary>
    public class NfePetrobrasRequest
    {
        /// <summary>
        /// Cnpj cadastrado na base da petrobras para a ALL
        /// </summary>
        public virtual string Cnpj { get; set; }

        /// <summary>
        /// Data da consulta
        /// </summary>
        public virtual string DataEmissao { get; set; }

        /// <summary>
        /// Hora inicial da consulta
        /// </summary>
        public virtual string HoraEmissaoInicial { get; set; }

        /// <summary>
        /// Hora final da consulta
        /// </summary>
        public virtual string HoraEmissaoFinal { get; set; }

        /// <summary>
        /// Token gerado e cadastrado na base da petrobras para a ALL (canal de negocios)
        /// </summary>
        public virtual string Token { get; set; }

        /// <summary>
        /// Url do servico
        /// </summary>
        public virtual string Url { get; set; }
    }
}