﻿namespace Translogic.Modules.Core.Domain.Services.FluxosComerciais
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Net.Mail;
    using System.Text;
    using System.Threading;
    using Translogic.Modules.Core.Domain.Model.Acesso;
    using Translogic.Modules.Core.Domain.Model.Codificador.Repositories;
    using Translogic.Modules.Core.Domain.Model.Diversos.Cte;

    /// <summary>
    /// Serviço que baixa as notas do simconsultas
    /// </summary>
    public class BaixarNfeService
    {
        #region Variáveis locais

            private readonly NfeService _nfeService;
            private readonly IConfiguracaoTranslogicRepository _configuracaoTranslogicRepository;

        #endregion

        #region Variáveis estáticas

            private static List<Thread> threads;
            private static Dictionary<string, string> erros;
            private class Param
            {
                public string Chave { get; set; }
                public bool NotasBrado { get; set; }
            };

        #endregion

        #region Construtor

            /// <summary>
            /// Construtor padrão
            /// </summary>
            /// <param name="nfeService">servico da nfe</param>
            /// <param name="configuracaoTranslogicRepository">configuracao Translogic Repository</param>
            public BaixarNfeService(NfeService nfeService, IConfiguracaoTranslogicRepository configuracaoTranslogicRepository)
            {
                if (threads == null)
                {
                    threads = new List<Thread>();
                }

                if (erros == null)
                {
                    erros = new Dictionary<string, string>();
                }

                _nfeService = nfeService;
                _configuracaoTranslogicRepository = configuracaoTranslogicRepository;
            }

        #endregion

        #region Métodos públicos

            /// <summary>
            /// Inicia as threads de processamentos das NFes
            /// </summary>
            /// <param name="chaves">chaves que serão consultadas (separadas por ;)</param>
            /// <param name="notasBrado">notas de clientes da Brado</param>
            /// <returns>Chaves que estão em processamento</returns>
            public List<string> IniciarProcessoNfe(string chaves, bool notasBrado)
            {
                var chavesSeparadas = RecuperarChaves(chaves);
                var chavesValidas = chavesSeparadas.Where(e => !threads.Select(t => t.Name).Contains(e)).ToList();

                chavesValidas.ForEach(e =>
                {
                    var param = new Param();
                    param.Chave = e;
                    param.NotasBrado = notasBrado;
                    var thread = new Thread(ProcessarNfe);
                    thread.Name = e;
                    thread.Start(param);
                    threads.Add(thread);
                });

                return chavesValidas;
            }

            /// <summary>
            /// Consulta o processamento de uma NFe
            /// </summary>
            /// <param name="chaves">chaves que serão consultadas (separadas por ;)</param>
            /// <returns>Chaves que estão em processamento</returns>
            public List<string> ConsultarProcessoNfe(string chaves)
            {
                var result = new List<string>();

                RecuperarChaves(chaves).ForEach(e =>
                {
                    if (EmProcessamento(e))
                    {
                        result.Add(e);
                    }
                });

                return result;
            }

            /// <summary>
            /// Consulta NFes que deram erro no processamento
            /// </summary>
            /// <param name="chaves">chaves que serão consultadas (separadas por ;)</param>
            /// <returns>Chave e descrição do erro</returns>
            public Dictionary<string, string> ConsultarErrosNfe(string chaves)
            {
                var chaversCortadas = RecuperarChaves(chaves);
                var result = erros.Where(e => chaversCortadas.Contains(e.Key)).ToDictionary(e => e.Key, e => e.Value);

                foreach (var keyValuePair in result)
                {
                    erros.Remove(keyValuePair.Key);
                }

                return result;
            }

            /// <summary>
            /// Recuperar Chaves
            /// </summary>
            /// <param name="chaves">string com as chaves</param>
            /// <returns>chaves separadas</returns>
            public List<string> RecuperarChaves(string chaves)
            {
                var chavesCortadas = chaves
                    .Replace(" ", ";")
                    .Replace(",", ";")
                    .Replace("\n", ";")
                    .Replace("\t", ";")
                    .Replace("\r", ";")
                    .Replace("\v", ";")
                    .Replace("-", string.Empty)
                    .Replace(".", string.Empty)
                    .Replace("\\", string.Empty)
                    .Replace("/", string.Empty)
                    .Split(';')
                    .Where(e => !string.IsNullOrWhiteSpace(e))
                    .Distinct()
                    .ToList();

                return chavesCortadas;
            }

            /// <summary>
            /// Enviar Email Erros
            /// </summary>
            /// <param name="chavesComErros">chaves das nfes com os erros separados por |</param>
            /// <param name="usuario">usuario ativo</param>
            /// <param name="titulo">titulo do email</param>
            public void EnviarEmail(string[] chavesComErros, Usuario usuario, string titulo)
            {
                if (chavesComErros != null && chavesComErros.Length > 0 && chavesComErros[0] != string.Empty)
                {
                    var body = new StringBuilder();

                    foreach (var item in chavesComErros)
                    {
                        if (item.IndexOf("|") != -1)
                        {
                            body.Append(item.Split('|')[0]);
                            body.Append(": ");
                            body.Append(item.Split('|')[1]);
                        }
                        else
                        {
                            body.Append(item);
                        }

                        body.Append("<br />");
                    }

                    body.Append("<br />");
                    body.Append("<br />");
                    body.Append("Usuário que reportou: ");
                    body.Append(usuario.Nome);
                    body.Append(" (");
                    body.Append(usuario.Codigo);
                    body.Append(")");

                    var mailMessage = new MailMessage();
                    mailMessage.From = new MailAddress("noreply@all-logistica.com");
                    mailMessage.IsBodyHtml = true;
                    mailMessage.Body = body.ToString();
                    mailMessage.Subject = titulo;
                    mailMessage.SubjectEncoding = Encoding.GetEncoding("ISO-8859-1");
                    mailMessage.BodyEncoding = Encoding.GetEncoding("ISO-8859-1");
                    mailMessage.To.Add(_configuracaoTranslogicRepository.ObterPorId("EMAILS_ALTERACAO_NFE").Valor);

                    new SmtpClient().Send(mailMessage);
                }
            }

        #endregion

        #region Métodos privados

            private void ProcessarNfe(object chaves)
            {
                var chaveConvertida = ((Param)chaves).Chave;

                try
                {
                    if (chaveConvertida.Length < 44 || chaveConvertida.Substring(20, 2) != "55")
                    {
                        throw new Exception("Chave de NFe inválida! Favor informar uma chave de modelo 55.");
                    }

                    // Obtem os dados da Nfe no SimConsulta e Grava na base.
                    var status = _nfeService.ObterDadosNfe(chaveConvertida, TelaProcessamentoNfe.PreCarregamento, true, ((Param)chaves).NotasBrado);

                    if (!status.ObtidaComSucesso)
                    {
                        throw new Exception(status.MensagemErro);
                    }
                }
                catch (Exception ex)
                {
                    erros.Add(chaveConvertida, FormatarErro(ex, 0));
                }
            }

            private bool EmProcessamento(string chave)
            {
                var thread = threads.Where(e => e.Name == chave).FirstOrDefault();

                if (thread != null && thread.IsAlive)
                {
                    return true;
                }

                threads.RemoveAll(e => !e.IsAlive);

                return false;
            }

            private string FormatarErro(Exception ex, int level)
            {
                var result = string.Empty;

                if (ex == null)
                {
                    return result;
                }

                result = string.Format("{0}{1}", string.Empty.PadLeft(level, '\t'), ex.Message);

                return string.Format("{0}{1}\n", result, FormatarErro(ex.InnerException, ++level));
            }

        #endregion
    }
}