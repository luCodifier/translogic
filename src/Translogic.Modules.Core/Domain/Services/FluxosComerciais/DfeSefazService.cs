﻿namespace Translogic.Modules.Core.Domain.Services.FluxosComerciais
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.IO.Compression;
    using System.Linq;
    using System.Net;
    using System.Reflection;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Xml;
    using Castle.Services.Transaction;
    using Interfaces.SefazNfe;
    using Interfaces.SefazNfe.Interfaces;
    using Model.Codificador;
    using Model.Codificador.Repositories;
    using Model.Diversos;
    using Model.Diversos.Cte;
    using Model.Diversos.Repositories;
    using Model.Diversos.Simconsultas;
    using Model.FluxosComerciais.Nfes;
    using Model.FluxosComerciais.Nfes.Repositories;
    using Translogic.Core.Commons;
    using Util;

    public class DfeSefazService
    {
        private readonly ICertificadosRepository _cetificadosRepository;
        private readonly IConfiguracaoTranslogicRepository _configuracaoTranslogicRepository;
        private readonly IDFeConsultaRepository _dFeConsultaRepository;
        private readonly IDFeService _dfeService;
        private readonly ILogDfeSefazRepository _logDfeSefazRepository;
        private readonly ILogStatusNfeRepository _logStatusNfeRepository;
        private readonly NfeConfiguracaoEmpresaService _nfeConfiguracaoEmpresaService;
        private readonly INfeDistribuicaoReadonlyRepository _nfeDistribuicaoReadonlyRepository;
        private readonly INfeProdutoDistribuicaoRepository _nfeProdutoDistribuicaoRepository;
        private readonly IStatusNfeRepository _statusNfeRepository;
        private readonly IStatusRetornoNfeRepository _statusRetornoNfeRepository;

        public DfeSefazService(IDFeService dfeService,
                               INfeDistribuicaoReadonlyRepository nfeDistribuicaoReadonlyRepository,
                               IDFeConsultaRepository dFeConsultaRepository,
                               ILogDfeSefazRepository logDfeSefazRepository,
                               ICertificadosRepository certificadosRepository,
                               NfeConfiguracaoEmpresaService nfeConfiguracaoEmpresaService,
                               INfeProdutoDistribuicaoRepository nfeProdutoDistribuicaoRepository,
                               IStatusRetornoNfeRepository statusRetornoNfeRepository,
                               ILogStatusNfeRepository logStatusNfeRepository,
                               IConfiguracaoTranslogicRepository configuracaoTranslogicRepository,
                               IStatusNfeRepository statusNfeRepository)
        {
            _dfeService = dfeService;
            _nfeDistribuicaoReadonlyRepository = nfeDistribuicaoReadonlyRepository;
            _dFeConsultaRepository = dFeConsultaRepository;
            _logDfeSefazRepository = logDfeSefazRepository;
            _cetificadosRepository = certificadosRepository;
            _nfeConfiguracaoEmpresaService = nfeConfiguracaoEmpresaService;
            _nfeProdutoDistribuicaoRepository = nfeProdutoDistribuicaoRepository;
            _statusRetornoNfeRepository = statusRetornoNfeRepository;
            _logStatusNfeRepository = logStatusNfeRepository;
            _configuracaoTranslogicRepository = configuracaoTranslogicRepository;
            _statusNfeRepository = statusNfeRepository;
        }

        public void ProcessarDfe()
        {
            IList<Certificado> certificados = _cetificadosRepository.ObterTodos();
            foreach (Certificado certificado in certificados)
            {
                try
                {
                    DFeConsulta ultimaConsulta = _dFeConsultaRepository.ObterUltimoRegistro(certificado.CnpjBusca, "11");
                    DFeConsultaDtoRequest dfeServiceRequest;
                    
                    if (ultimaConsulta == null)
                    {
                        dfeServiceRequest = new DFeConsultaDtoRequest
                        {
                            CnpjBusca = certificado.CnpjBusca,
                            Uf = "11",
                            UltimoRegistro = "000000000000000",
                            CnpjCertificado = certificado.CnpjCertificado
                        };
                    }
                    else
                    {
                        dfeServiceRequest = new DFeConsultaDtoRequest
                        {
                            CnpjBusca = certificado.CnpjBusca,
                            Uf = "11",
                            UltimoRegistro = ultimaConsulta.UltimoRegistro,
                            CnpjCertificado = certificado.CnpjCertificado
                        };
                    }
                     
                    XmlDocument retorno = _dfeService.ObterDadosDFe(dfeServiceRequest);

                    string strMaxNsu;
                    try
                    {
                        strMaxNsu = retorno.GetElementsByTagName("maxNSU").Item(0).InnerText;
                    }
                    catch
                    {
                        strMaxNsu = "000000000000000";
                    }

                    decimal ultimoNsu = Convert.ToDecimal(dfeServiceRequest.UltimoRegistro);
                    decimal maxNsu = Convert.ToDecimal(strMaxNsu);

                    ServicePointManager.DefaultConnectionLimit = 200;
                    ServicePointManager.Expect100Continue = true;

                    var tasks = new List<System.Threading.Tasks.Task>();
                    char pad = '0';

                    if (maxNsu > ultimoNsu)
                    {
                        do
                        {
                            string strUltimoNsu = ultimoNsu.ToString().PadLeft(15, pad);

                            dfeServiceRequest = new DFeConsultaDtoRequest
                            {
                                CnpjBusca = certificado.CnpjBusca,
                                Uf = "11",
                                UltimoRegistro = strUltimoNsu,
                                CnpjCertificado = certificado.CnpjCertificado
                            };

                            var nfeBoAsync = new DfeSefazAsync
                            {
                                certificado = certificado,
                                uf = "11",
                                DFeRequest = dfeServiceRequest
                            };

                            ultimoNsu += 50;

                            tasks.Add(new System.Threading.Tasks.Task(() => ObtemPorEstadoCnpj(nfeBoAsync)));

                        } while (ultimoNsu <= maxNsu);

                        // FAZ UMA REQUISIÇÃO ADICIONAL CASO NESTE MEIO TEMPO TENHAM ENTRADO MAIS NOTAS 
                        string strUltimoNsu2 = maxNsu.ToString().PadLeft(15, pad);
                        dfeServiceRequest = new DFeConsultaDtoRequest
                        {
                            CnpjBusca = certificado.CnpjBusca,
                            Uf = "11",
                            UltimoRegistro = strUltimoNsu2,
                            CnpjCertificado = certificado.CnpjCertificado
                        };

                        var nfeBoAsync2 = new DfeSefazAsync
                        {
                            certificado = certificado,
                            uf = "11",
                            DFeRequest = dfeServiceRequest
                        };

                        tasks.Add(new System.Threading.Tasks.Task(() => ObtemPorEstadoCnpjAdicional(nfeBoAsync2)));

                        tasks.ForEach(a => a.Start());
                        System.Threading.Tasks.Task.WaitAll(tasks.ToArray());
                        tasks.Clear();
                    }
                }
                catch(Exception e)
                {
                    // todo implementar log de erros
                }
            }
        }

        public void ObtemPorEstadoCnpj(object stateInfo)
        {
            var dados = stateInfo as DfeSefazAsync;
            try
            {
                var retorno = _dfeService.ObterDadosDFe(dados.DFeRequest);

                string strUltNsu;
                try
                {
                    strUltNsu = retorno.GetElementsByTagName("ultNSU").Item(0).InnerText.Equals("000000000000000") ? dados.DFeRequest.UltimoRegistro : retorno.GetElementsByTagName("ultNSU").Item(0).InnerText;
                }
                catch
                {
                    strUltNsu = "000000000000000";
                }

                XmlNodeList notasXml = retorno.GetElementsByTagName("docZip");

                for (int i = 0; i < notasXml.Count; i++)
                {
                    var convertenota = Convert.FromBase64String(notasXml[i].InnerText);

                    using (var ms = new MemoryStream(convertenota))
                    {
                        var gZip = new GZipStream(ms, CompressionMode.Decompress);
                        var msDesc = new MemoryStream();
                        gZip.CopyTo(msDesc);
                        msDesc.Seek(0, SeekOrigin.Begin);

                        var ut = Encoding.UTF8.GetString(msDesc.ToArray());
                        XmlDocument xDoc = new XmlDocument();
                        xDoc.LoadXml(ut);
                        try
                        {
                            if (notasXml[i].Attributes["schema"].Value.ToLower().Contains("procnfe"))
                            {
                                ProcessarNota(xDoc);
                            }
                        }
                        catch(Exception e)
                        {
                            // todo implementar log de erros
                        }
                    }
                }
                var novoNsu = new DFeConsulta
                {
                    Cnpj = dados.DFeRequest.CnpjBusca,
                    TimeStamp = DateTime.Now,
                    Uf = "11",
                    UltimoRegistro = strUltNsu
                };
                _dFeConsultaRepository.Inserir(novoNsu);

            }
            catch (Exception e)
            {
                // todo implementar log de erros
                // Console.Write(e);
            }
        }

        public void ObtemPorEstadoCnpjAdicional(object stateInfo)
        {
            var dados = stateInfo as DfeSefazAsync;
            try
            {
                var retorno = _dfeService.ObterDadosDFe(dados.DFeRequest);

                XmlNodeList notasXml = retorno.GetElementsByTagName("docZip");

                for (int i = 0; i < notasXml.Count; i++)
                {
                    var convertenota = Convert.FromBase64String(notasXml[i].InnerText);

                    using (var ms = new MemoryStream(convertenota))
                    {
                        var gZip = new GZipStream(ms, CompressionMode.Decompress);
                        var msDesc = new MemoryStream();
                        gZip.CopyTo(msDesc);
                        msDesc.Seek(0, SeekOrigin.Begin);

                        var ut = Encoding.UTF8.GetString(msDesc.ToArray());
                        XmlDocument xDoc = new XmlDocument();
                        xDoc.LoadXml(ut);
                        try
                        {
                            if (notasXml[i].Attributes["schema"].Value.ToLower().Contains("procnfe"))
                            {
                                ProcessarNota(xDoc);
                            }
                        }
                        catch (Exception e)
                        {
                            // todo implementar log de erros
                        }
                    }
                }
            }
            catch (Exception e)
            {
                // todo implementar log de erros
                // Console.Write(e);
            }
        }

        [Transaction]
        public void ProcessarNota(XmlDocument xDoc)
        {
            string codigoErro = "000";
            string mensagemErro = String.Empty;
            double peso_bruto = 0;

            XmlElement root = xDoc.DocumentElement;
            var nsmgr = new XmlNamespaceManager(xDoc.NameTable);
            nsmgr.AddNamespace("d", root.NamespaceURI);

            XmlNode noInfNfe = root.SelectSingleNode("//d:NFe/d:infNFe", nsmgr);

            XmlNode noProtNFe = root.SelectSingleNode("//d:protNFe/d:infProt", nsmgr);

            NfeDistribuicaoReadonly nfeLido = ProcessarNfeXml(xDoc);

            if (nfeLido != null)
            {
                // Caso a nfe tenha sido processada simultaneamente, então retorna a nfe já processada.
                NfeDistribuicaoReadonly nfeEdi = _nfeDistribuicaoReadonlyRepository.ObterPorChaveNfe(nfeLido.ChaveNfe);

                NfeDistribuicaoReadonly nfe = null;

                if (nfeEdi == null)
                {
                    try
                    {
                        nfe = new NfeDistribuicaoReadonly(nfeLido);

                        nfe.Xml = xDoc.OuterXml;

                        string dataRecebimento = ObterTextoNoXml(noProtNFe, "dhRecbto", nsmgr);
                        
                        if (!string.IsNullOrEmpty(dataRecebimento))
                        {
                            try
                            {
                                nfe.DataRecebimento = Convert.ToDateTime(dataRecebimento);
                            }
                            catch(Exception e)
                            {
                                // novo campo NFE_DT_RECEBIMENTO da tabela nfe_distribuicao 
                            }
                        }

                        _nfeDistribuicaoReadonlyRepository.Inserir(nfe);

                        XmlNode noTransp = noInfNfe.SelectSingleNode("//d:transp", nsmgr);
                        var docTransp = new XmlDocument();
                        docTransp.LoadXml(noTransp.OuterXml);

                        IEnumerable<INotaFiscalEletronicaProduto> listaProdutosXml =
                            ProcessarProdutosNfeXml(noInfNfe.SelectNodes("//d:det", nsmgr), nsmgr);

                        nfe.ListaProdutos = ProcessarProdutosNfeDfe(nfe, listaProdutosXml);

                        XmlNodeList listaNoVolume = docTransp.SelectNodes("//d:vol", nsmgr);

                        var nfeParam = (NotaFiscalEletronica) nfe;

                        nfeParam.ListaProdutos = new List<NotaFiscalEletronicaProduto>();

                        foreach (NfeProdutoDistribuicao produto in nfe.ListaProdutos)
                        {
                            nfeParam.ListaProdutos.Add(produto);
                        }

                        _nfeConfiguracaoEmpresaService.ObterPesoVolumeNfe(listaNoVolume, nsmgr, ref nfeParam, ref peso_bruto);

                        nfe.Peso = nfeParam.Peso;
                        nfe.PesoBruto = nfeParam.PesoBruto;
                        nfe.Volume = nfeParam.Volume;

                        _nfeDistribuicaoReadonlyRepository.Atualizar(nfe);
                    }
                    catch (Exception ex)
                    {
                        codigoErro = "EDI1";
                        mensagemErro = ex.Message;
                    }
                    finally
                    {
                        StatusRetornoNfe statusRetorno = _statusRetornoNfeRepository.ObterPorCodigo(codigoErro);
                        if (statusRetorno == null)
                        {
                            statusRetorno = new StatusRetornoNfe
                                                {
                                                    Codigo = codigoErro,
                                                    DataCadastro = DateTime.Now,
                                                    IndPermiteReenvioTela = false,
                                                    IndPermiteReenvioBo = false,
                                                    IndLiberarDigitacao = true,
                                                    Mensagem = mensagemErro
                                                };
                            _statusRetornoNfeRepository.Inserir(statusRetorno);
                        }

                        if (nfe != null)
                        {
                            GravarLogStatusNfe(nfe, statusRetorno, nfe.ChaveNfe, TelaProcessamentoNfe.DFeSefaz);
                        }
                    }
                }
            }
        }


        /// <summary>
        /// Processa os produtos lidos no xml do Edi
        /// </summary>
        /// <param name="nfeDfe">NF do Edi realizada a leitura</param>
        /// <param name="listaProdutos">Lista de Produtos</param>
        /// <returns>Lista de produtos processados</returns>
        public IList<NfeProdutoDistribuicao> ProcessarProdutosNfeDfe(NfeDistribuicaoReadonly nfeDfe,
                                                                     IEnumerable<INotaFiscalEletronicaProduto>
                                                                         listaProdutos)
        {
            var list = new List<NfeProdutoDistribuicao>();

            double pesoTotalNota = 0;

            if (listaProdutos != null && listaProdutos.Count() > 0)
            {
                int i = 0;

                foreach (INotaFiscalEletronicaProduto produto in listaProdutos)
                {
                    var nfeProduto = new NfeProdutoDistribuicao((NotaFiscalEletronicaProduto) produto);
                    nfeProduto.NfeDistribuicaoReadonly = nfeDfe;
                    //pesoTotalNota += nfeProduto.QuantidadeComercial;
                    nfeProduto.DataHoraGravacao = DateTime.Now;

                    if (nfeProduto.UnidadeComercial == "PC")
                    {
                        pesoTotalNota += 0;
                    }
                    else if (nfeProduto.UnidadeComercial == "S50")
                    {
                        pesoTotalNota += nfeProduto.QuantidadeComercial*50;
                    }
                    else if (nfeProduto.UnidadeComercial == "S60")
                    {
                        pesoTotalNota += nfeProduto.QuantidadeComercial*60;
                    }
                    else
                    {
                        pesoTotalNota += nfeProduto.QuantidadeComercial;
                    }

                    if (nfeDfe.ListaProdutos != null && nfeDfe.ListaProdutos.Count > 0)
                    {
                        var prod =
                            (NotaFiscalEletronicaProduto)
                            _nfeProdutoDistribuicaoRepository.ObterPorId(nfeDfe.ListaProdutos[i].Id);

                        ParseProdutosNfes(nfeProduto, ref prod);

                        _nfeProdutoDistribuicaoRepository.Atualizar(nfeProduto);
                    }
                    else
                    {
                        _nfeProdutoDistribuicaoRepository.Inserir(nfeProduto);
                    }

                    list.Add(nfeProduto);
                    i++;
                }

                nfeDfe.Peso = pesoTotalNota;
                nfeDfe.PesoBruto = pesoTotalNota;
            }

            if (VerificarPorUnidadeComercial(listaProdutos))
            {
                nfeDfe.Peso = 0;
                nfeDfe.PesoBruto = 0;
            }

            return list;
        }

        /// <summary>
        ///     Realiza o parse de NfesProdutos
        /// </summary>
        /// <param name="nfe">NfeProduto que recebeu o xml</param>
        /// <param name="nfeRetorno">nfeproduto a ser atualizada com os novos dados</param>
        private void ParseProdutosNfes(NotaFiscalEletronicaProduto nfe, ref NotaFiscalEletronicaProduto nfeRetorno)
        {
            foreach (FieldInfo field in nfe.GetType().GetFields())
            {
                if (field.Name != "Id" && field.Name.ToUpper() != "NFESIMCONSULTAS")
                {
                    typeof (NotaFiscalEletronicaProduto).GetField(field.Name).SetValue(nfeRetorno, field.GetValue(nfe));
                }
            }

            foreach (PropertyInfo prop in nfe.GetType().GetProperties())
            {
                if (prop.Name != "Id" && prop.Name.ToUpper() != "NFESIMCONSULTAS")
                {
                    typeof (NotaFiscalEletronicaProduto).GetProperty(prop.Name)
                        .SetValue(nfeRetorno, prop.GetValue(nfe, null), null);
                }
            }
        }


        /// <summary>
        /// Obtém os produtos da NFE
        /// </summary>
        /// <param name="listaProdutos">Lista de produtos no xml</param>
        /// <param name="nsmgr">Namespace do xml</param>
        /// <returns>Lista de produtos realizada parser</returns>
        public IEnumerable<INotaFiscalEletronicaProduto> ProcessarProdutosNfeXml(XmlNodeList listaProdutos,
                                                                                 XmlNamespaceManager nsmgr)
        {
            var listaDeProdutosNoXml = new List<INotaFiscalEletronicaProduto>();

            foreach (XmlNode produto in listaProdutos)
            {
                XmlNode noProduto = produto.SelectSingleNode(".//d:prod", nsmgr);

                listaDeProdutosNoXml.Add(new NotaFiscalEletronicaProduto
                                             {
                                                 CodigoProduto = ObterTextoNoXml(noProduto, "cProd", nsmgr),
                                                 DescricaoProduto = ObterTextoNoXml(noProduto, "xProd", nsmgr),
                                                 CodigoNcm = ObterTextoNoXml(noProduto, "NCM", nsmgr),
                                                 Cfop = ObterTextoNoXml(noProduto, "CFOP", nsmgr),
                                                 UnidadeComercial = ObterTextoNoXml(noProduto, "uCom", nsmgr),
                                                 QuantidadeComercial =
                                                     Convert.ToDouble(ObterTextoNoXml(noProduto, "qCom", nsmgr),
                                                                      CultureInfo.GetCultureInfo("en-US")),
                                                 ValorUnitarioComercializacao =
                                                     ParseDoubleField(ObterTextoNoXml(noProduto, "vUnCom", nsmgr)),
                                                 ValorProduto =
                                                     ParseDoubleField(ObterTextoNoXml(noProduto, "vProd", nsmgr)),
                                                 UnidadeTributavel = ObterTextoNoXml(noProduto, "uTrib", nsmgr),
                                                 QuantidadeTributavel =
                                                     Convert.ToDouble(ObterTextoNoXml(noProduto, "qTrib", nsmgr),
                                                                      CultureInfo.GetCultureInfo("en-US")),
                                                 ValorUnitarioTributacao =
                                                     ParseDoubleField(ObterTextoNoXml(noProduto, "vUnTrib", nsmgr)),
                                                 DataHoraGravacao = DateTime.Now
                                             });
            }

            return listaDeProdutosNoXml;
        }

        /// <summary>
        /// Recebe o documento xml e realiza o parser
        /// </summary>
        /// <param name="doc">Documento Xml</param>
        /// <returns>Interface Nota Fiscal Eletrônica</returns>
        public NfeDistribuicaoReadonly ProcessarNfeXml(XmlDocument doc)
        {
            var logDfe = new LogDfeSefaz();
            logDfe.Retorno = doc.OuterXml;

            var nfe = new NfeDistribuicaoReadonly();

            var datePatterns = new[] {"yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd", "dd/MM/yyyy", "dd/MM/yyyy HH:mm:ss"};

            XmlElement root = doc.DocumentElement;
            var nsmgr = new XmlNamespaceManager(doc.NameTable);
            nsmgr.AddNamespace("d", root.NamespaceURI);

            XmlNode noInfNfe = root.SelectSingleNode("//d:NFe/d:infNFe", nsmgr);

            // Verificar se a NFe é válida
            if (noInfNfe != null && noInfNfe.Attributes != null)
            {
                string versaoNfe = noInfNfe.Attributes["versao"] == null
                                       ? string.Empty
                                       : noInfNfe.Attributes["versao"].Value;
                string chaveNfe = noInfNfe.Attributes["Id"] == null
                                      ? noInfNfe.Attributes["id"].Value
                                      : noInfNfe.Attributes["Id"].Value;

                string noDataEmissao = !string.IsNullOrEmpty(versaoNfe) && versaoNfe.Equals("3.10") ? "dhEmi" : "dEmi";
                string noDataSaida = !string.IsNullOrEmpty(versaoNfe) && versaoNfe.Equals("3.10")
                                         ? "dhSaiEnt"
                                         : "dSaiEnt";

                try
                {
                    logDfe.DataInicio = DateTime.Now;
                    XmlNode noIde = noInfNfe.SelectSingleNode("//d:ide", nsmgr);
                    string dataEmissao = ObterTextoNoXml(noIde, noDataEmissao, nsmgr);
                    string dataSaida = ObterTextoNoXml(noIde, noDataSaida, nsmgr);

                    // DADOS DE IDENTIFICAÇÃO DA NFE
                    logDfe.ChaveNfe = nfe.ChaveNfe = chaveNfe.Substring(3);
                    logDfe.DataHoraGravacao = DateTime.Now;

                    nfe.CodigoUfIbge = Convert.ToInt32(ObterTextoNoXml(noIde, "cUF", nsmgr));
                    nfe.CodigoChaveAcesso = Convert.ToInt32(ObterTextoNoXml(noIde, "cNF", nsmgr));
                    nfe.NaturezaOperacao = ObterTextoNoXml(noIde, "natOp", nsmgr);
                    string indPag = ObterTextoNoXml(noIde, "indPag", nsmgr);
                    if (!string.IsNullOrEmpty(indPag))
                    {
                        nfe.FormaPagamento = ObterFormaPagamento(indPag);
                    }

                    nfe.ModeloNota = ObterTextoNoXml(noIde, "mod", nsmgr);
                    nfe.SerieNotaFiscal = ObterTextoNoXml(noIde, "serie", nsmgr);
                    nfe.NumeroNotaFiscal = Convert.ToInt32(ObterTextoNoXml(noIde, "nNF", nsmgr));

                    if (!string.IsNullOrEmpty(dataEmissao))
                    {
                        if (!string.IsNullOrEmpty(versaoNfe) && versaoNfe.Equals("3.10"))
                        {
                            dataEmissao = dataEmissao.Substring(0, 10);
                            nfe.DataEmissao = DateTime.ParseExact(dataEmissao, datePatterns,
                                                                  CultureInfo.InvariantCulture, DateTimeStyles.None);
                        }
                        else
                        {
                            nfe.DataEmissao = DateTime.ParseExact(ObterTextoNoXml(noIde, noDataEmissao, nsmgr),
                                                                  datePatterns, CultureInfo.InvariantCulture,
                                                                  DateTimeStyles.None);
                        }
                    }

                    if (!string.IsNullOrEmpty(dataSaida))
                    {
                        if (!string.IsNullOrEmpty(versaoNfe) && versaoNfe.Equals("3.10"))
                        {
                            dataSaida = dataSaida.Substring(0, 10);
                            nfe.DataSaida = DateTime.ParseExact(dataSaida, datePatterns, CultureInfo.InvariantCulture,
                                                                DateTimeStyles.None);
                        }
                        else
                        {
                            string horaSaida = ObterTextoNoXml(noIde, "hSaiEnt", nsmgr);
                            if (!string.IsNullOrEmpty(horaSaida))
                            {
                                dataSaida = string.Concat(dataSaida, " ", horaSaida);
                            }

                            try
                            {
                                dataSaida = dataSaida.Replace("ás", string.Empty);
                                nfe.DataSaida = DateTime.ParseExact(dataSaida, datePatterns,
                                                                    CultureInfo.InvariantCulture, DateTimeStyles.None);
                            }
                            catch
                            {
                            }
                        }
                    }

                    nfe.TipoNotaFiscal = ObterTipoNotaFiscal(ObterTextoNoXml(noIde, "tpNF", nsmgr));
                    nfe.CodigoIbge = Convert.ToInt32(ObterTextoNoXml(noIde, "cMunFG", nsmgr));
                    nfe.FormatoImpressao = ObterFormatoImpressao(ObterTextoNoXml(noIde, "tpImp", nsmgr));
                    nfe.TipoEmissao = ObterTipoEmissao(ObterTextoNoXml(noIde, "tpEmis", nsmgr));
                    nfe.DigitoVerificadorChave = string.IsNullOrEmpty(ObterTextoNoXml(noIde, "cDV", nsmgr))
                                                     ? 0
                                                     : Convert.ToInt32(ObterTextoNoXml(noIde, "cDV", nsmgr));
                    nfe.TipoAmbiente = ObterTipoAmbiente(ObterTextoNoXml(noIde, "tpAmb", nsmgr));
                    nfe.FinalidadeEmissao = ObterFinalidadeEmissao(ObterTextoNoXml(noIde, "finNFe", nsmgr));
                    nfe.ProcessoEmissao = ObterProcessoEmissao(ObterTextoNoXml(noIde, "procEmi", nsmgr));
                    nfe.VersaoProcesso = ObterTextoNoXml(noIde, "verProc", nsmgr);

                    // DADOS DE EMITENTE
                    XmlNode noEmit = noInfNfe.SelectSingleNode("//d:emit", nsmgr);
                    DadosEmpresaTemp dadosEmpresaEmitente = RecuperarDadosEmpresa(noEmit, true, nsmgr);
                    ProcessarDadosEmitente(ref nfe, dadosEmpresaEmitente);

                    // DADOS DE DESTINATARIO
                    XmlNode noDest = noInfNfe.SelectSingleNode("//d:dest", nsmgr);
                    DadosEmpresaTemp dadosEmpresaDestinataria = RecuperarDadosEmpresa(noDest, false, nsmgr);
                    ProcessarDadosDestinatario(ref nfe, dadosEmpresaDestinataria);

                    // DADOS DE IMPOSTOS DA NFE
                    XmlNode noIcmsTotal = noInfNfe.SelectSingleNode("//d:total/d:ICMSTot", nsmgr);

                    nfe.ValorBaseCalculoIcms = ObterDoubleDoXML(noIcmsTotal, "vBC", nsmgr);
                    nfe.ValorIcms = ObterDoubleDoXML(noIcmsTotal, "vICMS", nsmgr);
                    nfe.ValorBaseCalculoSubTributaria = ObterDoubleDoXML(noIcmsTotal, "vBCST", nsmgr);
                    nfe.ValorSubTributaria = ObterDoubleDoXML(noIcmsTotal, "vST", nsmgr);
                    nfe.ValorProduto = ObterDoubleDoXML(noIcmsTotal, "vProd", nsmgr);
                    nfe.ValorTotalFrete = ObterDoubleDoXML(noIcmsTotal, "vFrete", nsmgr);
                    nfe.ValorSeguro = ObterDoubleDoXML(noIcmsTotal, "vSeg", nsmgr);
                    nfe.ValorDesconto = ObterDoubleDoXML(noIcmsTotal, "vDesc", nsmgr);
                    nfe.ValorImpostoImportacao = ObterDoubleDoXML(noIcmsTotal, "vII", nsmgr);
                    nfe.ValorIpi = ObterDoubleDoXML(noIcmsTotal, "vIPI", nsmgr);
                    nfe.ValorPis = ObterDoubleDoXML(noIcmsTotal, "vPIS", nsmgr);
                    nfe.ValorCofins = ObterDoubleDoXML(noIcmsTotal, "vCOFINS", nsmgr);
                    nfe.ValorOutro = ObterDoubleDoXML(noIcmsTotal, "vOutro", nsmgr);
                    nfe.Valor = ObterDoubleDoXML(noIcmsTotal, "vNF", nsmgr);
                    // nfe.Volume = 0;

                    // DADOS DE TRANSPORTE
                    XmlNode noTransp = noInfNfe.SelectSingleNode("//d:transp", nsmgr);
                    var docTransp = new XmlDocument();
                    docTransp.LoadXml(noTransp.OuterXml);
                    string modFrete = ObterTextoNoXml(docTransp, "modFrete", nsmgr);
                    modFrete = string.IsNullOrEmpty(modFrete) || string.IsNullOrWhiteSpace(modFrete) ? "0" : modFrete;
                    nfe.ModeloFrete = ObterModeloFrete(modFrete);

                    XmlNode noTransportadora = docTransp.SelectSingleNode("//d:transporta", nsmgr);
                    if (noTransportadora != null)
                    {
                        string cnpj = ObterTextoNoXml(noTransportadora, "CNPJ", nsmgr);
                        if (!string.IsNullOrEmpty(cnpj))
                        {
                            nfe.CnpjTransportadora = cnpj;
                        }

                        nfe.NomeTransportadora = ObterTextoNoXml(noTransportadora, "xNome", nsmgr);
                        nfe.EnderecoTransportadora = ObterTextoNoXml(noTransportadora, "xEnder", nsmgr);
                        nfe.MunicipioTransportadora = ObterTextoNoXml(noTransportadora, "xMun", nsmgr);
                        nfe.UfTransportadora = ObterTextoNoXml(noTransportadora, "UF", nsmgr);
                    }

                    XmlNode noVeicTransp = docTransp.SelectSingleNode("//d:veicTransp", nsmgr);
                    if (noVeicTransp != null)
                    {
                        nfe.Placa = ObterTextoNoXml(noVeicTransp, "placa", nsmgr);
                    }

                    XmlNode noVolume = docTransp.SelectSingleNode("//d:vol", nsmgr);
                    {
                        nfe.Volume = ObterDoubleDoXML(noVolume, "qVol", nsmgr);
                        nfe.Peso = ObterDoubleDoXML(noVolume, "pesoL", nsmgr);
                        nfe.PesoBruto = ObterDoubleDoXML(noVolume, "pesoB", nsmgr);
                    }

                    nfe.CanceladoEmitente = false;
                    nfe.DataHoraGravacao = DateTime.Now;
                    nfe.Xml = doc.OuterXml;

                    logDfe.DataTermino = DateTime.Now;
                    logDfe.Erro = false;
                    logDfe.MensagemErro = string.Empty;
                }
                catch (Exception erro)
                {
                    logDfe.Erro = true;
                    logDfe.MensagemErro = erro.ToString();
                    nfe = null;
                }
                finally
                {
                    _logDfeSefazRepository.Inserir(logDfe);
                }
            }
            return nfe;
        }

        /// <summary>
        /// Grava o log de status da nfe e retorna o ultimo status
        /// </summary>
        /// <param name="nfe">Objeto da Nfe caso haja sucesso no parser</param>
        /// <param name="statusRetorno">Status de retorno do simconsultas</param>
        /// <param name="chaveNfe">Chave de acesso da Nfe</param>
        /// <param name="origem"> Tela de origem de nfe</param>
        /// <returns>Status da Nfe</returns>
        public StatusNfe GravarLogStatusNfe(NotaFiscalEletronica nfe, StatusRetornoNfe statusRetorno, string chaveNfe,
                                            TelaProcessamentoNfe origem)
        {
            double peso = 0;
            double volume = 0;

            var logStatusNfe = new LogStatusNfe
                                   {
                                       ChaveNfe = chaveNfe,
                                       DataCadastro = DateTime.Now,
                                       StatusRetornoNfe = statusRetorno
                                   };
            _logStatusNfeRepository.Inserir(logStatusNfe);

            if (nfe != null)
            {
                ConfiguracaoTranslogic configuracaoTranslogic =
                    _configuracaoTranslogicRepository.ObterPorId("SIGLAS_UNIDADE_TONELADA");

                string[] arraySiglaUnidadeTonelada = null;
                if (configuracaoTranslogic != null && !string.IsNullOrEmpty(configuracaoTranslogic.Valor))
                {
                    arraySiglaUnidadeTonelada = configuracaoTranslogic.Valor.Split(',');
                }

                if (nfe.ListaProdutos != null)
                {
                    if (nfe.ListaProdutos.Any(p => !String.IsNullOrEmpty(p.UnidadeComercial) &&
                                                   (p.UnidadeComercial.ToUpperInvariant().StartsWith("L") ||
                                                    p.UnidadeComercial.ToUpperInvariant().StartsWith("M")))
                        && nfe.Volume.HasValue)
                    {
                        volume = Tools.TruncateValue(nfe.Volume.Value, TipoTruncateValueEnum.UnidadeMilhar);
                    }

                    if (
                        nfe.ListaProdutos.Any(
                            p =>
                            !String.IsNullOrEmpty(p.UnidadeComercial) && arraySiglaUnidadeTonelada != null &&
                            arraySiglaUnidadeTonelada.Contains(p.UnidadeComercial)))
                    {
                        peso = nfe.Peso;
                    }
                    else
                    {
                        peso = Tools.TruncateValue(nfe.Peso/1000, TipoTruncateValueEnum.UnidadeMilhar);
                    }
                }
            }

            StatusNfe statusNfe = _statusNfeRepository.ObterPorChaveNfe(chaveNfe);
            
            if (statusNfe != null)
            {
                if (statusNfe.Peso == 0 && peso > 0)
                {
                    statusNfe.Peso = peso;
                }

                if (statusNfe.Volume == 0 && volume > 0)
                {
                    statusNfe.Volume = volume;
                }

                statusNfe.StatusRetornoNfe = statusRetorno;
                statusNfe.Origem = origem.ToString();
                _statusNfeRepository.Atualizar(statusNfe);
            }
            else
            {
                statusNfe = new StatusNfe
                                {
                                    ChaveNfe = chaveNfe,
                                    DataCadastro = DateTime.Now,
                                    PesoUtilizado = 0,
                                    VolumeUtilizado = 0,
                                    StatusRetornoNfe = statusRetorno,
                                    Peso = peso,
                                    Volume = volume,
                                    Origem = origem.ToString()
                                };

                _statusNfeRepository.Inserir(statusNfe);
            }

            return statusNfe;
        }

        /// <summary>
        /// obterm um texto do xml
        /// </summary>
        /// <param name="xmlNode">no do xml informado</param>
        /// <param name="tagNo">tag do xml informado</param>
        /// <param name="nsmgr">namespace manager</param>
        /// <returns>texto extraido</returns>
        public static string ObterTextoNoXml(XmlNode xmlNode, string tagNo, XmlNamespaceManager nsmgr)
        {
            XmlNode no = xmlNode.SelectSingleNode(".//d:" + tagNo, nsmgr);

            if (no == null)
            {
                return null;
            }

            return no.InnerText;
        }

        /// <summary>
        /// Tratar Formato Numerico
        /// </summary>
        /// <param name="value">valor para tratar</param>
        /// <returns>texto tratado</returns>
        public static string TratarFormatoNumerico(string value)
        {
            int indexPeriod = value.IndexOf(".");
            int indexComma = value.IndexOf(",");

            if (indexPeriod < indexComma)
            {
                value = value.Replace(".", string.Empty).Replace(",", ".");
            }
            else if (indexComma < indexPeriod)
            {
                value = value.Replace(",", string.Empty);
            }

            return value;
        }

        /// <summary>
        /// extrai um double de um xml
        /// </summary>
        /// <param name="xmlNode">nó do xml informado</param>
        /// <param name="tagNo">tag do xml informado</param>
        /// <param name="nsmgr">namespace manager</param>
        /// <returns>double extraido</returns>
        public static double ObterDoubleDoXML(XmlNode xmlNode, string tagNo, XmlNamespaceManager nsmgr)
        {
            try
            {
                double result = 0;
                string text = ObterTextoNoXml(xmlNode, tagNo, nsmgr);

                if (!string.IsNullOrWhiteSpace(text))
                {
                    text = TratarFormatoNumerico(text);
                    result = Convert.ToDouble(text, CultureInfo.GetCultureInfo("en-US"));
                }
                return result;
            }
            catch
            {
                return 0;
            }
        }

        private TipoNotaFiscalEnum ObterTipoNotaFiscal(string textoTipoNotaFiscal)
        {
            return Enum<TipoNotaFiscalEnum>.From(JustNumbers(textoTipoNotaFiscal));
        }

        private ModeloFreteEnum? ObterModeloFrete(string textoModeloFrete)
        {
            return Enum<ModeloFreteEnum>.From(JustNumbers(textoModeloFrete));
        }

        private FormaPagamentoEnum ObterFormaPagamento(string numeroFormaPagamento)
        {
            return Enum<FormaPagamentoEnum>.From(JustNumbers(numeroFormaPagamento));
        }

        private ProcessoEmissaoEnum ObterProcessoEmissao(string textoProcessoEmissao)
        {
            // 0 - emissão de NF-e com aplicativo do contribuinte;
            // 1 - emissão de NF-e avulsa pelo Fisco;
            // 2 - emissão de NF-e avulsa, pelo contribuinte com seu certificado digital, através do site
            // do Fisco;
            // 3- emissão de NF-e pelo contribuinte com aplicativo fornecido pelo Fisco.
            return Enum<ProcessoEmissaoEnum>.From(JustNumbers(textoProcessoEmissao));
        }

        private FinalidadeEmissaoEnum ObterFinalidadeEmissao(string textoFinalidadeEmissao)
        {
            // 1 - NFe normal
            // 2 - NFe complementar
            // 3 - NFe de ajuste

            return Enum<FinalidadeEmissaoEnum>.From(JustNumbers(textoFinalidadeEmissao));
        }

        private TipoAmbienteEnum ObterTipoAmbiente(string textoTipoAmbiente)
        {
            return Enum<TipoAmbienteEnum>.From(JustNumbers(textoTipoAmbiente));
        }

        private TipoEmissaoEnum ObterTipoEmissao(string textoTipoEmissao)
        {
            /*
            1 - Normal;
            2 - Contingência FS
            3 - Contingência SCAN
            4 - Contingência DPEC
            5 - Contingência FSDA
            */
            if (string.IsNullOrEmpty(textoTipoEmissao))
            {
                return TipoEmissaoEnum.Normal;
            }

            return Enum<TipoEmissaoEnum>.From(JustNumbers(textoTipoEmissao));
        }

        private FormatoImpressaoEnum ObterFormatoImpressao(string textoFormatoImpressao)
        {
            if (string.IsNullOrEmpty(textoFormatoImpressao))
            {
                return FormatoImpressaoEnum.Retrato;
            }

            return Enum<FormatoImpressaoEnum>.From(JustNumbers(textoFormatoImpressao));
        }

        private DadosEmpresaTemp RecuperarDadosEmpresa(XmlNode noXml, bool indEmitente, XmlNamespaceManager nsmgr)
        {
            var docEmp = new XmlDocument();
            docEmp.LoadXml(noXml.OuterXml);

            XmlNode noEndereco = docEmp.SelectSingleNode(indEmitente ? "//d:enderEmit" : "//d:enderDest", nsmgr);

            var dados = new DadosEmpresaTemp
                            {
                                RazaoSocial = ObterTextoNoXml(docEmp, "xNome", nsmgr),
                                NomeFantasia = ObterTextoNoXml(docEmp, "xFant", nsmgr),
                                Logradouro = ObterTextoNoXml(noEndereco, "xLgr", nsmgr),
                                Numero = ObterTextoNoXml(noEndereco, "nro", nsmgr),
                                Complemento = ObterTextoNoXml(noEndereco, "xCpl", nsmgr),
                                Bairro = ObterTextoNoXml(noEndereco, "xBairro", nsmgr),
                                CodigoMunicipioIbge = ObterTextoNoXml(noEndereco, "cMun", nsmgr),
                                Municipio = ObterTextoNoXml(noEndereco, "xMun", nsmgr),
                                SiglaUf = ObterTextoNoXml(noEndereco, "UF", nsmgr),
                                Cep = ObterTextoNoXml(noEndereco, "CEP", nsmgr),
                                CodigoPais = ObterTextoNoXml(noEndereco, "cPais", nsmgr),
                                Pais = ObterTextoNoXml(noEndereco, "xPais", nsmgr),
                                Telefone = ObterTextoNoXml(noEndereco, "fone", nsmgr),
                                InscricaoEstadual = ObterTextoNoXml(docEmp, "IE", nsmgr)
                            };
            string cnpj = ObterTextoNoXml(docEmp, "CNPJ", nsmgr);
            if (!string.IsNullOrEmpty(dados.SiglaUf) && dados.SiglaUf.Equals("EX"))
            {
                dados.Cnpj = "00000000000000";
            }
            else
            {
                if (!string.IsNullOrEmpty(cnpj))
                {
                    dados.Cnpj = cnpj;
                }
            }

            string cpf = ObterTextoNoXml(docEmp, "CPF", nsmgr);
            if (!string.IsNullOrEmpty(cpf))
            {
                dados.Cpf = cpf;
            }

            return dados;
        }

        private void ProcessarDadosDestinatario(ref NfeDistribuicaoReadonly nfe, DadosEmpresaTemp dadosEmpresa)
        {
            nfe.CnpjDestinatario = JustNumbers(dadosEmpresa.Cnpj);
            nfe.RazaoSocialDestinatario = dadosEmpresa.RazaoSocial;
            nfe.NomeFantasiaDestinatario = dadosEmpresa.NomeFantasia;
            nfe.LogradouroDestinatario = dadosEmpresa.Logradouro;
            nfe.NumeroDestinatario = dadosEmpresa.Numero;
            nfe.ComplementoDestinatario = dadosEmpresa.Complemento;
            nfe.BairroDestinatario = dadosEmpresa.Bairro;
            nfe.CodigoMunicipioDestinatario = dadosEmpresa.CodigoMunicipioIbge;
            nfe.MunicipioDestinatario = dadosEmpresa.Municipio;
            nfe.UfDestinatario = dadosEmpresa.SiglaUf;
            nfe.CepDestinatario = dadosEmpresa.Cep;
            nfe.CodigoPaisDestinatario = dadosEmpresa.CodigoPais;
            nfe.PaisDestinatario = dadosEmpresa.Pais;
            nfe.TelefoneDestinatario = dadosEmpresa.Telefone;
            nfe.InscricaoEstadualDestinatario = dadosEmpresa.InscricaoEstadual;
        }

        private void ProcessarDadosEmitente(ref NfeDistribuicaoReadonly nfe, DadosEmpresaTemp dadosEmpresa)
        {
            nfe.CnpjEmitente = JustNumbers(dadosEmpresa.Cnpj);
            nfe.RazaoSocialEmitente = dadosEmpresa.RazaoSocial;
            nfe.NomeFantasiaEmitente = dadosEmpresa.NomeFantasia;
            nfe.LogradouroEmitente = dadosEmpresa.Logradouro;
            nfe.NumeroEmitente = dadosEmpresa.Numero;
            nfe.ComplementoEmitente = dadosEmpresa.Complemento;
            nfe.BairroEmitente = dadosEmpresa.Bairro;
            nfe.CodigoMunicipioEmitente = dadosEmpresa.CodigoMunicipioIbge;
            nfe.MunicipioEmitente = dadosEmpresa.Municipio;
            nfe.UfEmitente = dadosEmpresa.SiglaUf;
            nfe.CepEmitente = dadosEmpresa.Cep;
            nfe.CodigoPaisEmitente = dadosEmpresa.CodigoPais;
            nfe.PaisEmitente = dadosEmpresa.Pais;
            nfe.TelefoneEmitente = dadosEmpresa.Telefone;
            nfe.InscricaoEstadualEmitente = dadosEmpresa.InscricaoEstadual;
        }

        private string JustNumbers(string value)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                return string.Empty;
            }

            try
            {
                string result = Regex.Replace(value, @"[^\d]", string.Empty);
                return result;
            }
            catch
            {
                return string.Empty;
            }
        }

        private double ParseDoubleField(string valor)
        {
            double retorno = 0;
            if (Double.TryParse(valor, NumberStyles.Currency, CultureInfo.GetCultureInfo("en-US"), out retorno))
            {
                return retorno;
            }

            if (Double.TryParse(valor, NumberStyles.Currency, CultureInfo.GetCultureInfo("pt-BR"), out retorno))
            {
                return retorno;
            }

            return 0;
        }

        /// <summary>
        /// Verificar o tipo de unidade comercial
        /// </summary>
        /// <param name="listaProdutos">Lista de produtos a verificar</param>
        /// <returns>Verdadeiro se algum produto atender a alguma das condições testadas</returns>
        private bool VerificarPorUnidadeComercial(IEnumerable<INotaFiscalEletronicaProduto> listaProdutos)
        {
            return listaProdutos.Any(p => p.UnidadeComercial.Trim().ToUpper().Contains("SAC") ||
                                          p.UnidadeComercial.Trim().ToUpper().Contains("SC") ||
                                          p.UnidadeComercial.Trim().ToUpper().Contains("SCS"));
        }

        #region Nested type: DadosEmpresaTemp

        /// <summary>
        /// Classe de empresa
        /// </summary>
        protected class DadosEmpresaTemp
        {
            /// <summary>
            /// Gets or sets Cnpj.
            /// </summary>
            public string Cnpj { get; set; }

            /// <summary>
            /// Gets or sets Cpf.
            /// </summary>
            public string Cpf { get; set; }

            /// <summary>
            /// Gets or sets RazaoSocial.
            /// </summary>
            public string RazaoSocial { get; set; }

            /// <summary>
            /// Gets or sets NomeFantasia.
            /// </summary>
            public string NomeFantasia { get; set; }

            /// <summary>
            /// Gets or sets Logradouro.
            /// </summary>
            public string Logradouro { get; set; }

            /// <summary>
            /// Gets or sets Numero.
            /// </summary>
            public string Numero { get; set; }

            /// <summary>
            /// Gets or sets Complemento.
            /// </summary>
            public string Complemento { get; set; }

            /// <summary>
            /// Gets or sets Bairro.
            /// </summary>
            public string Bairro { get; set; }

            /// <summary>
            /// Gets or sets CodigoMunicipioIbge.
            /// </summary>
            public string CodigoMunicipioIbge { get; set; }

            /// <summary>
            /// Gets or sets Municipio.
            /// </summary>
            public string Municipio { get; set; }

            /// <summary>
            /// Gets or sets SiglaUf.
            /// </summary>
            public string SiglaUf { get; set; }

            /// <summary>
            /// Gets or sets Uf.
            /// </summary>
            public string Uf { get; set; }

            /// <summary>
            /// Gets or sets Cep.
            /// </summary>
            public string Cep { get; set; }

            /// <summary>
            /// Gets or sets CodigoPais.
            /// </summary>
            public string CodigoPais { get; set; }

            /// <summary>
            /// Gets or sets Pais.
            /// </summary>
            public string Pais { get; set; }

            /// <summary>
            /// Gets or sets Telefone.
            /// </summary>
            public string Telefone { get; set; }

            /// <summary>
            /// Gets or sets InscricaoEstadual.
            /// </summary>
            public string InscricaoEstadual { get; set; }
        }

        #endregion

        #region Nested type: DfeSefazAsync

        /// <summary>
        /// Classe Interna para executar a thread
        /// </summary>
        protected class DfeSefazAsync
        {
            /// <summary>
            /// Certificado
            /// </summary>
            public Certificado certificado { get; set; }

            /// <summary>
            /// UfIbge
            /// </summary>
            public string uf { get; set; }

            /// <summary>
            /// Objeto do Request
            /// </summary>
            public DFeConsultaDtoRequest DFeRequest { get; set; }
        }

        #endregion
    }
}