﻿namespace Translogic.Modules.Core.Domain.Services.FluxosComerciais
{
    using System;
    using System.Collections.Generic;
    using Castle.Services.Transaction;
    using Translogic.Modules.Core.Domain.Model.Codificador.Repositories;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Simconsultas.Repositories;
    using Translogic.Modules.Core.Util;
    using Translogic.Modules.EDI.Domain.Models.Nfes;
    using Translogic.Modules.EDI.Domain.Models.Nfes.Repositories;

    /// <summary>
    ///     Serviço da NFe PDF
    /// </summary>
    [Transactional]
    public class NfePdfService
    {
        private readonly INfePdfEdiRepository _nfePdfEdiRepository;
        private readonly INfeEdiRepository _nfeEdiRepository;
        private readonly INfeSimconsultasRepository _nfeSimConsultasRepository;
        private readonly IConfiguracaoTranslogicRepository _confGeralRepository;

        public NfePdfService(INfePdfEdiRepository nfePdfEdiRepository,
                             INfeEdiRepository nfeEdiRepository,
                             INfeSimconsultasRepository nfeSimConsultasRepository,
                             IConfiguracaoTranslogicRepository confGeralRepository)
        {
            this._nfePdfEdiRepository = nfePdfEdiRepository;
            this._nfeEdiRepository = nfeEdiRepository;
            this._nfeSimConsultasRepository = nfeSimConsultasRepository;
            this._confGeralRepository = confGeralRepository;
        }

        public NfeEdi ObterNfePorChaveEdi(string chaveNfe)
        {
            return _nfeEdiRepository.ObterPorChaveNfe(chaveNfe);
        }

        private int? ObterIdNotaFiscalEdi(string chaveNfe)
        {
            int? id = null;
            var nfe = _nfeEdiRepository.ObterPorChaveNfe(chaveNfe);
            if (nfe != null)
            {
                id = nfe.Id;
            }

            return id;
        }

        private int? ObterIdNotaFiscalSimConsultas(string chaveNfe)
        {
            int? id = null;
            var nfe = _nfeSimConsultasRepository.ObterPorChaveNfe(chaveNfe);
            if (nfe != null)
            {
                id = nfe.Id;
            }

            return id;
        }

        /// <summary>
        ///     Atualiza os dados da nfe Edi
        /// </summary>
        public void AtualizacaoNfePdfSemIdNota()
        {
            var limite = 100;

            #region Limite de Registros

            // Obtendo da configuração geral o valor parametrizado
            var configuracaoLimite = _confGeralRepository.ObterPorId("ATUALIZACAO_PDF_NFE_LIMITE");

            if (configuracaoLimite != null)
            {
                int limiteConf;
                if (Int32.TryParse(configuracaoLimite.Valor, out limiteConf))
                {
                    limite = limiteConf > 0 ? limiteConf : 150;
                }
            }

            #endregion

            var nfePdfEdiSemIdNota = this._nfePdfEdiRepository.ObterNfePdfEdiSemIdNota(limite);
            foreach (var nfePdf in nfePdfEdiSemIdNota)
            {
                try
                {
                    var pdf = nfePdf.Pdf;
                    var pdfValido = Tools.VerificaPdfValido(pdf);
                    if (pdfValido)
                    {
                        // Ler o Arquivo PDF da Nota e recuperar a chave da nota
                        // Caso encontre alguma chave no arquivo PDF, então buscar o ID da Nota no banco de dados
                        IList<string> chavesNfe = null;
                        if (pdf != null)
                        {
                            chavesNfe = Tools.BuscarChavesNfeDoArquivoPDF(pdf);
                        }
                        
                        foreach (var chaveNfe in chavesNfe)
                        {
                            var encontrado = false;

                            #region EDI

                            if (!encontrado)
                            {
                                var idNfeEdi = ObterIdNotaFiscalEdi(chaveNfe);
                                if (idNfeEdi.HasValue)
                                {
                                    nfePdf.Id_Nfe = idNfeEdi.Value;
                                    encontrado = true;
                                }
                            }

                            #endregion EDI

                            #region SIM Consultas

                            if (!encontrado)
                            {
                                var idNfeSimConsultas = ObterIdNotaFiscalSimConsultas(chaveNfe);
                                if (idNfeSimConsultas.HasValue)
                                {
                                    nfePdf.IdNfeSimConsultas = idNfeSimConsultas.Value;
                                    encontrado = true;
                                }
                            }

                            #endregion

                            #region Atualização da chave

                            if (nfePdf.NfeChave != chaveNfe && chavesNfe.Count == 1)
                            {
                                nfePdf.NfeChave = chaveNfe;
                                encontrado = true;
                            }

                            #endregion

                            if (encontrado)
                            {
                                this._nfePdfEdiRepository.Atualizar(nfePdf);
                            }
                        }
                    }
                    else
                    {
                        throw new Exception("PDF corrompido.");
                    }
                }
                catch (Exception ex)
                {
                    var mensagemException = ex.Message;
                }
            }
        }
    }
}