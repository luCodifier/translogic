﻿namespace Translogic.Modules.Core.Domain.Services.Baldeio.Interface
{
    using Translogic.Modules.Core.Domain.Model.Dto.Baldeio;
    using ALL.Core.Dominio;
    using Translogic.Core.Infrastructure.Web;
    using System.Collections.Generic;
    using Translogic.Modules.Core.Domain.Model.Acesso;

    public interface IBaldeioService
    {
        /// <summary>
        /// Retorna lista de registro  com carta de baldeio anexo conforme filtro informado pelo usuario
        /// </summary>
        /// <param name="detalhesPaginacaoWeb"></param>
        /// <param name="dataInicial"></param>
        /// <param name="dataFinal"></param>
        /// <param name="local"></param>
        /// <param name="vagaoCedente"></param>
        /// <param name="vagaoRecebedor"></param>
        /// <returns></returns>
        ResultadoPaginado<BaldeioDto> ObterConsultaBaldeio(DetalhesPaginacaoWeb detalhesPaginacaoWeb, string dataInicial, string dataFinal, string local, string vagaoCedente, string vagaoRecebedor);

        /// <summary>
        /// Retorna lista baldeio para exportação
        /// </summary>
        /// <param name="dataInicial"></param>
        /// <param name="dataFinal"></param>
        /// <param name="local"></param>
        /// <param name="vagaoCedente"></param>
        /// <param name="vagaoRecebedor"></param>
        /// <returns> Lista Baleio</returns>
        IEnumerable<BaldeioDto> ObterConsultaBaldeioExportar(string dataInicial, string dataFinal, string local, string vagaoCedente, string vagaoRecebedor);

        /// <summary>
        /// Buscar permissões de usuário para baldeio
        /// </summary>
        /// <param name="usuario"></param>
        /// <returns></returns>
        BaldeioPermissaoDto BuscarPermissaoUsuario(Usuario usuario);
    }
}
