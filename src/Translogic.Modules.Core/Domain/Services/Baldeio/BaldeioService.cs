﻿namespace Translogic.Modules.Core.Domain.Services.Baldeio
{
    using System;
    using Translogic.Modules.Core.Domain.Services.Baldeio.Interface;
    using Translogic.Core.Infrastructure.Web;
    using ALL.Core.Dominio;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Repositories;
    using Translogic.Modules.Core.Domain.Model.Dto.Baldeio;
    using System.Collections.Generic;
    using Translogic.Modules.Core.Domain.Model.Acesso;
    using Microsoft.Practices.ServiceLocation;
    using Translogic.Modules.Core.Domain.Services.Acesso;

    public class BaldeioService : IBaldeioService
    {

        private readonly IBaldeioRepository _baldeioRepository;

        public BaldeioService(IBaldeioRepository baldeioRepository)
        {
            _baldeioRepository = baldeioRepository;
        }

        public ResultadoPaginado<BaldeioDto> ObterConsultaBaldeio(DetalhesPaginacaoWeb detalhesPaginacaoWeb,
                                                                                                     string dataInicial,
                                                                                                     string dataFinal,
                                                                                                     string local,
                                                                                                     string vagaoCedente,
                                                                                          string vagaoRecebedor)
        {
            string dataInicio = string.Empty;
            string dataFim = string.Empty;

            if (!string.IsNullOrEmpty(dataInicial) && dataInicial != "__/__/____")
            {
                dataInicio = dataInicial;
            }

            if (!string.IsNullOrEmpty(dataFinal) && dataFinal != "__/__/____")
            {
                dataFim = dataFinal;
            }

            var result = _baldeioRepository.ObterConsultaBaldeio(detalhesPaginacaoWeb, dataInicio, dataFim, local, vagaoCedente, vagaoRecebedor);

            if (result.Total == 0)
            {
                //throw new Exception("Nenhum registro encontrado");
            }
            return result;
        }

        public IEnumerable<BaldeioDto> ObterConsultaBaldeioExportar(string dataInicial,
                                                                            string dataFinal,
                                                                            string local,
                                                                            string vagaoCedente,
                                                                            string vagaoRecebedor)
        {
            string dataInicio = string.Empty;
            string dataFim = string.Empty;

            if (!string.IsNullOrEmpty(dataInicial) && dataInicial != "__/__/____")
            {
                dataInicio = dataInicial;
            }

            if (!string.IsNullOrEmpty(dataFinal) && dataFinal != "__/__/____")
            {
                dataFim = dataFinal;
            }

            var result = _baldeioRepository.ObterConsultaBaldeioExportar(dataInicio, dataFim, local, vagaoCedente, vagaoRecebedor);

            return result;
        }

        public BaldeioPermissaoDto BuscarPermissaoUsuario(Usuario usuario)
        {
            var permissao = new BaldeioPermissaoDto();
            var acao = "CONSULTABALDEIO";

            permissao.Pesquisar = ServiceLocator.Current.GetInstance<ControleAcessoService>().Autorizar(acao, "Pesquisar", usuario);
            permissao.Importar = ServiceLocator.Current.GetInstance<ControleAcessoService>().Autorizar(acao, "Importar", usuario);
            permissao.ExcluirCarta = ServiceLocator.Current.GetInstance<ControleAcessoService>().Autorizar(acao, "ExcluirCarta", usuario);

            return permissao;
        }
    }
}