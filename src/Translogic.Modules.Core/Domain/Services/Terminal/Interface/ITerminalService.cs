﻿namespace Translogic.Modules.Core.Domain.Services.Terminal.Interface
{
    using Translogic.Modules.Core.Domain.Model.Via;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;

    public interface ITerminalService
    {
        /// <summary>
        /// Obter lista de terminais
        /// </summary>
        /// <returns>Retorna a de terminais cadastrados</returns>
        List<TerminalSeguroDto> ObterTerminais();

        List<TerminalSiglaDto> ObterListaTerminalSigla();
    }
}