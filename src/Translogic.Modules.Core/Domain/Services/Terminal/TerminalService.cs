﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Translogic.Modules.Core.Domain.Services.Terminal.Interface;
using Translogic.Modules.Core.Domain.Model.Via.Repositories;
using Translogic.Modules.Core.Domain.Model.Dto;

namespace Translogic.Modules.Core.Domain.Services.Terminal
{
    public class TerminalService : ITerminalService
    {
        private readonly IAreaOperacionalRepository _areaOperacional;

        public TerminalService(IAreaOperacionalRepository areaOperacionalRepo)
        {
            _areaOperacional = areaOperacionalRepo;
        }

        public List<TerminalSeguroDto> ObterTerminais()
        {
            var obj = _areaOperacional.ObterTerminais();
            return obj;
        }

        public TerminalSeguroDto Atualizar(TerminalSeguroDto entity)
        {
            throw new NotImplementedException();
        }

        public TerminalSeguroDto Inserir(TerminalSeguroDto entity)
        {
            throw new NotImplementedException();
        }

        public TerminalSeguroDto InserirOuAtualizar(TerminalSeguroDto entity)
        {
            throw new NotImplementedException();
        }

        public TerminalSeguroDto ObterPorId(int? id)
        {
            throw new NotImplementedException();
        }

        public IList<TerminalSeguroDto> ObterTodos()
        {
            throw new NotImplementedException();
        }

        public ALL.Core.Dominio.ResultadoPaginado<TerminalSeguroDto> ObterTodosPaginado(ALL.Core.Dominio.DetalhesPaginacao detalhes, ALL.Core.Dominio.IDetalhesFiltro<object>[] filterDetails)
        {
            throw new NotImplementedException();
        }

        public ALL.Core.Dominio.ResultadoPaginado<TerminalSeguroDto> ObterTodosPaginado(ALL.Core.Dominio.DetalhesPaginacao detalhes)
        {
            throw new NotImplementedException();
        }

        public void Remover(params int?[] id)
        {
            throw new NotImplementedException();
        }

        public void Remover(TerminalSeguroDto entity)
        {
            throw new NotImplementedException();
        }

        public void RemoverTodos()
        {
            throw new NotImplementedException();
        }


        public List<TerminalSiglaDto> ObterListaTerminalSigla()
        {
            return _areaOperacional.ObterListaTerminalSigla();
        }
    }
}