﻿namespace Translogic.Modules.Core.Domain.Services.Ctes
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Translogic.Modules.Core.Domain.Model.Codificador.Repositories;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.TriangulusConfig;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.TriangulusConfig.Repositories;
    using Translogic.Modules.Core.Domain.Services.Ctes.Interface;

    public class CteInutilizacaoConfigService : ICteInutilizacaoConfigService
    {
        #region Fields

        private readonly IConfiguracaoTranslogicRepository _configuracaoTranslogicRepository;

        private readonly ICte01ListaRepository _cte01ListaRepository;

        private readonly ICteInutilizacaoConfigRepository _cteInutilizacaoConfigRepository;

        private readonly FilaProcessamentoService _filaProcessamentoService;

        #endregion

        #region Constructors and Destructors

        public CteInutilizacaoConfigService(
            ICte01ListaRepository cte01ListaRepository,
            ICteInutilizacaoConfigRepository cteInutilizacaoConfigRepository,
            FilaProcessamentoService filaProcessamentoService,
            IConfiguracaoTranslogicRepository configuracaoTranslogicRepository)
        {
            this._cte01ListaRepository = cte01ListaRepository;
            this._cteInutilizacaoConfigRepository = cteInutilizacaoConfigRepository;
            this._filaProcessamentoService = filaProcessamentoService;
            this._configuracaoTranslogicRepository = configuracaoTranslogicRepository;
        }

        #endregion

        #region Public Methods and Operators

        public bool AtualizarStatusCtesInutilizacaoConfig()
        {
            try
            {
                IList<CteInutilizacaoConfig> listaCtesInutilizacaoConfigProcessado =
                    this._cteInutilizacaoConfigRepository.ObterTodosProcessados(false).Where(c => c.StatusCte == null || c.StatusCte == 0).ToList();

                foreach (CteInutilizacaoConfig cteInutilizacao in listaCtesInutilizacaoConfigProcessado)
                {
                    if (cteInutilizacao.StatusCte != 102)
                    {
                        Cte01Lista cte01Lista = this._cte01ListaRepository.ObterPorId(cteInutilizacao.IdListaConfig);
                        cteInutilizacao.StatusCte = cte01Lista.Status;
                        cteInutilizacao.Protocolo = cte01Lista.Protocolo;
                        this._cteInutilizacaoConfigRepository.Atualizar(cteInutilizacao);
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }

            return true;
        }

        public bool InserirCtesInutilizacaoConfig()
        {
            try
            {
                IList<CteInutilizacaoConfig> listaCtesInutilizacaoConfig =
                    this._cteInutilizacaoConfigRepository.ObterTodosProcessados(false).Where(c => c.StatusCte == 0 || c.StatusCte == null).Take(Convert.ToInt32(this._configuracaoTranslogicRepository.ObterPorId("NUMERO_CTES_PARA_INUTILIZACAO").Valor))
                        .ToList();

                foreach (CteInutilizacaoConfig cteInutilizacao in listaCtesInutilizacaoConfig)
                {
                    cteInutilizacao.IdListaConfig =
                        this._filaProcessamentoService.InserirCteInutilizacaoFilaProcessamentoPorNumeroSerie(
                            cteInutilizacao.Numero,
                            cteInutilizacao.Serie,
                            cteInutilizacao.Cnpj);

                    Cte01Lista cte01Lista = this._cte01ListaRepository.ObterPorId(cteInutilizacao.IdListaConfig);
                    cteInutilizacao.Protocolo = cte01Lista.Protocolo;

                    cteInutilizacao.Processado = false;

                    this._cteInutilizacaoConfigRepository.Atualizar(cteInutilizacao);
                }
            }
            catch (Exception ex)
            {
                return false;
            }

            return true;
        }

        #endregion
    }
}