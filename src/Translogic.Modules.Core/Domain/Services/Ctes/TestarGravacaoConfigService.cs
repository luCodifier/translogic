namespace Translogic.Modules.Core.Domain.Services.Ctes
{
	using System;
	using Model.FluxosComerciais.TriangulusConfig;
	using Model.FluxosComerciais.TriangulusConfig.Repositories;

	/// <summary>
	/// Servi�o de teste de grava��o da config
	/// </summary>
	public class TestarGravacaoConfigService
	{
		private readonly ICte04Repository _cte04Repository;
		private readonly ICte01Repository _ctr01Repository;

		/// <summary>
		/// Construtor do servico de teste
		/// </summary>
		/// <param name="cte04Repository">Reposit�rio do Cte04 injetado</param>
		/// <param name="ctr01Repository">Reposit�rio do Cte01 injetado</param>
		public TestarGravacaoConfigService(ICte04Repository cte04Repository, ICte01Repository ctr01Repository)
		{
			_cte04Repository = cte04Repository;
			_ctr01Repository = ctr01Repository;
		}

		/// <summary>
		/// Testa a grava��o dos dados
		/// </summary>
		public void TestarGravacao()
		{
			Cte04 cte04 = new Cte04();
			cte04.CfgUn = 1;
			cte04.CfgDoc = 1;
			cte04.CfgSerie = "XXX";
			cte04.IdObsFisco = 3;
			cte04.ObsFiscoXcampo = "EFETIVA��O DOS";
			cte04.ObsFiscoXtexto = "����� ������";
			_cte04Repository.Inserir(cte04);
		}

		/// <summary>
		/// Gravar dados Iniciais
		/// </summary>
		public void GravarDadosIniciais()
		{
			Cte01 cte01 = new Cte01();
			cte01.CfgDoc = 10;
			cte01.CfgUn = 10;
			cte01.CfgSerie = "XYZ";
			_ctr01Repository.Inserir(cte01);
		}
	}
}