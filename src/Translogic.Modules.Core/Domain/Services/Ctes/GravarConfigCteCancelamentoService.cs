namespace Translogic.Modules.Core.Domain.Services.Ctes
{
	using System;
	using System.Text;
	using Model.FluxosComerciais.ConhecimentoTransporteEletronico;
	using Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;

	/// <summary>
	/// Classe para gravar na config os CT-e de cancelamento
	/// </summary>
	public class GravarConfigCteCancelamentoService
	{
		private readonly ICteRepository _cteRepository;
		private CteLogService _cteLogService;
		private Cte _cte;
		private string _justificativa;

		/// <summary>
		/// Initializes a new instance of the <see cref="GravarConfigCteCancelamentoService"/> class.
		/// </summary>
		/// <param name="cteRepository">Reposit�rio do Cte injetado</param>
		/// <param name="cteLogService">Servico de log do cte injetado</param>
		public GravarConfigCteCancelamentoService(ICteRepository cteRepository, CteLogService cteLogService)
		{
			_cteRepository = cteRepository;
			_cteLogService = cteLogService;
		}

		/// <summary>
		/// Executa a grava��o dos dados de cancelamento
		/// </summary>
		/// <param name="cte">Cte que ser� gerado o arquivo de cancelamento</param>
		/// <param name="justificativa">Justificativa do cancelamento</param>
		public void Executar(Cte cte, string justificativa)
		{
			try
			{
				_cte = cte;
				_justificativa = justificativa;

				CarregarDados();

				// Todo: To Implement
			}
			catch (Exception ex)
			{
				_cteLogService.InserirLogErro(_cte, "GravarConfigCteCancelamentoService", string.Format("Executar: {0}", ex.Message), ex);
				throw;
			}
		}

		private void CarregarDados()
		{
		}
	}
}
