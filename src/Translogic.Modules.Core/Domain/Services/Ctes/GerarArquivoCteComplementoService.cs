namespace Translogic.Modules.Core.Domain.Services.Ctes
{
	using System;
	using System.Collections.Generic;
	using System.Globalization;
	using System.IO;
	using System.Linq;
	using System.Text;
	using Model.FluxosComerciais; 
	using Model.FluxosComerciais.ConhecimentoTransporteEletronico;
	using Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;
	using Model.FluxosComerciais.Pedidos.Despachos;
	using Model.FluxosComerciais.Pedidos.Despachos.Repositories;
	using Model.Trem.Veiculo.Vagao;
	using Util;

	/// <summary>
	/// Classe para gera��o dos arquivos do CT-e de complemento
	/// </summary>
	public class GerarArquivoCteComplementoService
	{
		private readonly string SEPARADOR_COLUNA = ";";
		private readonly ICteRepository _cteRepository;
		private readonly IItemDespachoRepository _itemDespachoRepository;
		private readonly IComposicaoFreteContratoRepository _composicaoFreteContratoRepository;
		private readonly INotaFiscalTranslogicRepository _notaFiscalTranslogicRepository;
		private readonly ICteDetalheRepository _cteDetalheRepository;
		private readonly ICteArvoreRepository _cteArvoreRepository;
		private readonly IComposicaoFreteCteRepository _composicaoFreteCteRepository;
		private readonly ICteComplementadoRepository _cteComplementadoRepository;
		private IList<CteDetalhe> _listaCteDetalhe;

		private Cte _cteComplementar;
		private Cte _cteRaiz;
		private IList<CteComplementado> _listaCteComplementado;

		private ItemDespacho _itemDespacho;
		private FluxoComercial _fluxoComercial;
		// private DetalheCarregamento _detalheCarregamento;
		// private Carregamento _carregamento;
		private ContratoHistorico _contratoHistorico;
		private Contrato _contrato;
		private Vagao _vagao;
		private CteLogService _cteLogService;

		/// <summary>
		/// Initializes a new instance of the <see cref="GerarArquivoCteComplementoService"/> class.
		/// </summary>
		/// <param name="cteRepository">Reposit�rio do Cte injetado</param>
		/// <param name="itemDespachoRepository">Reposit�rio do item de despacho injetado</param>
		/// <param name="composicaoFreteContratoRepository">Reposit�rio da composi��o do frete de contrato injetado</param>
		/// <param name="notaFiscalTranslogicRepository">Reposit�rio da nota fiscal do translogic injetado</param>
		/// <param name="cteDetalheRepository">Reposit�rio do detalhe do Cte injetado</param>
		/// <param name="cteLogService">Servico de log do cte injetado</param>
		/// <param name="cteArvoreRepository">Reposit�rio de cte arvore injetado</param>
		/// <param name="composicaoFreteCteRepository">Reposit�rio da composicao frete cte injetado</param>
		/// <param name="cteComplementadoRepository">Reposit�rio do cte complementado injetado</param>
		public GerarArquivoCteComplementoService(ICteRepository cteRepository, IItemDespachoRepository itemDespachoRepository, IComposicaoFreteContratoRepository composicaoFreteContratoRepository, INotaFiscalTranslogicRepository notaFiscalTranslogicRepository, ICteDetalheRepository cteDetalheRepository, CteLogService cteLogService, ICteArvoreRepository cteArvoreRepository, IComposicaoFreteCteRepository composicaoFreteCteRepository, ICteComplementadoRepository cteComplementadoRepository)
		{
			_cteRepository = cteRepository;
			_cteComplementadoRepository = cteComplementadoRepository;
			_composicaoFreteCteRepository = composicaoFreteCteRepository;
			_cteArvoreRepository = cteArvoreRepository;
			_cteLogService = cteLogService;
			_cteDetalheRepository = cteDetalheRepository;
			_notaFiscalTranslogicRepository = notaFiscalTranslogicRepository;
			_composicaoFreteContratoRepository = composicaoFreteContratoRepository;
			_itemDespachoRepository = itemDespachoRepository;
		}

		/// <summary>
		/// Executa a gera��o do arquivo de envio
		/// </summary>
		/// <param name="cteComplementar">Cte complementar que ser� gerado o arquivo de complemento</param>
		/// <returns>Retorna o StringBuilder com o arquivo gerado</returns>
		public StringBuilder Executar(Cte cteComplementar)
		{
			StringBuilder builder = new StringBuilder();
			try
			{
				_cteComplementar = cteComplementar;
				
				CarregarDados();

				// U - CT-e Complementados
				builder.Append(GerarRegistro23000());
				builder.Append(GerarRegistro23100());
				builder.Append(GerarRegistro23110());
				builder.Append(GerarRegistro24000());
				builder.Append(GerarRegistro24100());
				builder.Append(GerarRegistro24145());
				builder.Append(GerarRegistro24180());
				builder.Append(GerarRegistro24181());
				builder.Append(GerarRegistro24190());
				// Todo: Verificar como pegar as informa��es complementares
				// builder.Append(GerarRegistro24300());

				InserirComposicaoFreteCte();
			}
			catch (Exception ex)
			{
				// Limpa o string builder
				builder.Remove(0, builder.Length);
				_cteLogService.InserirLogErro(_cteComplementar, "GerarArquivoCteComplementoService", string.Format("Executar: {0}", ex.Message), ex);

				throw;
			}

			return builder;
		}

		private string GerarRegistro24300()
		{
			StringBuilder buffer = new StringBuilder();

			try
			{
				string informacoes = String.Empty;

				buffer.Append("24300");
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(informacoes);
			}
			catch (Exception ex)
			{
				_cteLogService.InserirLogErro(_cteComplementar, "GerarArquivoCteComplementoService", string.Format("GerarRegistro24300: {0}", ex.Message), ex);
				throw;
			}

			return buffer.ToString();
		}

		private string GerarRegistro24190()
		{
			StringBuilder buffer = new StringBuilder();

			try
			{
				double baseCalculoIcms = CalcularBaseCalculoIcms();
				double aliquota = _contratoHistorico.PercentualAliquotaIcms ?? 0.0;
				double valorTotalIcms = baseCalculoIcms * aliquota;
				double valorCredito = 0.0;

				buffer.Append("24190");
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append("90");
				buffer.Append(SEPARADOR_COLUNA);
				// Percentual de reducao - N�o tem esse campo no SAP (N�o utilizado)
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(baseCalculoIcms.ToString("0.00", CultureInfo.GetCultureInfo("en-US").NumberFormat));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(aliquota.ToString("0.00", CultureInfo.GetCultureInfo("en-US").NumberFormat));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(valorTotalIcms.ToString("0.00", CultureInfo.GetCultureInfo("en-US").NumberFormat));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(valorCredito.ToString("0.00", CultureInfo.GetCultureInfo("en-US").NumberFormat));
			}
			catch (Exception ex)
			{
				_cteLogService.InserirLogErro(_cteComplementar, "GerarArquivoCteComplementoService", string.Format("GerarRegistro24190: {0}", ex.Message), ex);
				throw;
			}

			return buffer.ToString();
		}

		private string GerarRegistro24181()
		{
			StringBuilder buffer = new StringBuilder();

			try
			{
				double baseCalculoIcms = CalcularBaseCalculoIcms();
				double aliquota = _contratoHistorico.PercentualAliquotaIcms ?? 0.0;
				double valorTotalIcms = baseCalculoIcms * aliquota;

				buffer.Append("24181");
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append("81");
				buffer.Append(SEPARADOR_COLUNA);
				// Percentual de reducao - N�o tem esse campo no SAP (N�o utilizado)
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(baseCalculoIcms.ToString("0.00", CultureInfo.GetCultureInfo("en-US").NumberFormat));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(aliquota.ToString("0.00", CultureInfo.GetCultureInfo("en-US").NumberFormat));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(valorTotalIcms.ToString("0.00", CultureInfo.GetCultureInfo("en-US").NumberFormat));
			}
			catch (Exception ex)
			{
				_cteLogService.InserirLogErro(_cteComplementar, "GerarArquivoCteComplementoService", string.Format("GerarRegistro24181: {0}", ex.Message), ex);
				throw;
			}

			return buffer.ToString();
		}

		private string GerarRegistro24180()
		{
			StringBuilder buffer = new StringBuilder();

			try
			{
				double baseCalculoIcms = CalcularBaseCalculoIcms();
				double aliquota = _contratoHistorico.PercentualAliquotaIcms ?? 0.0;
				double valorTotalIcms = baseCalculoIcms * aliquota;
				double valorCredito = 0.0;

				buffer.Append("24180");
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append("80");
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(baseCalculoIcms.ToString("0.00", CultureInfo.GetCultureInfo("en-US").NumberFormat));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(aliquota.ToString("0.00", CultureInfo.GetCultureInfo("en-US").NumberFormat));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(valorTotalIcms.ToString("0.00", CultureInfo.GetCultureInfo("en-US").NumberFormat));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(valorCredito.ToString("0.00", CultureInfo.GetCultureInfo("en-US").NumberFormat));
			}
			catch (Exception ex)
			{
				_cteLogService.InserirLogErro(_cteComplementar, "GerarArquivoCteComplementoService", string.Format("GerarRegistro24180: {0}", ex.Message), ex);
				throw;
			}

			return buffer.ToString();
		}

		private string GerarRegistro24145()
		{
			StringBuilder buffer = new StringBuilder();

			try
			{
				string tributacaoServico = "40";
				buffer.Append("24145");
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(tributacaoServico);
			}
			catch (Exception ex)
			{
				_cteLogService.InserirLogErro(_cteComplementar, "GerarArquivoCteComplementoService", string.Format("GerarRegistro24145: {0}", ex.Message), ex);
				throw;
			}

			return buffer.ToString();
		}

		private string GerarRegistro24120()
		{
			return string.Empty;
		}

		private string GerarRegistro24100()
		{
			StringBuilder buffer = new StringBuilder();

			try
			{
				double baseCalculoIcms = CalcularBaseCalculoIcms();
				double aliquota = _contratoHistorico.PercentualAliquotaIcms ?? 0.0;
				double valorTotalIcms = baseCalculoIcms * aliquota;

				buffer.Append("24100");
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append("00");
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(baseCalculoIcms.ToString("0.00", CultureInfo.GetCultureInfo("en-US").NumberFormat));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(aliquota.ToString("0.00", CultureInfo.GetCultureInfo("en-US").NumberFormat));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(valorTotalIcms.ToString("0.00", CultureInfo.GetCultureInfo("en-US").NumberFormat));
			}
			catch (Exception ex)
			{
				_cteLogService.InserirLogErro(_cteComplementar, "GerarArquivoCteComplementoService", string.Format("GerarRegistro24100: {0}", ex.Message), ex);
				throw;
			}

			return buffer.ToString();
		}

		private string GerarRegistro24000()
		{
			StringBuilder buffer = new StringBuilder();

			try
			{
				buffer.Append("24000");
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append("INFORMA�OES COMPLEMENTARES");
			}
			catch (Exception ex)
			{
				_cteLogService.InserirLogErro(_cteComplementar, "GerarArquivoCteComplementoService", string.Format("GerarRegistro24000: {0}", ex.Message), ex);
				throw;
			}

			return buffer.ToString();
		}

		private string GerarRegistro23110()
		{
			StringBuilder buffer = new StringBuilder();

			try
			{
				string nomeComponente = "FRETE";
				double valorComponente = _listaCteComplementado.Sum(g => g.ValorDiferenca);

				buffer.Append("23110");
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(nomeComponente, 15));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(valorComponente.ToString("0.00", CultureInfo.GetCultureInfo("en-US").NumberFormat));
			}
			catch (Exception ex)
			{
				_cteLogService.InserirLogErro(_cteComplementar, "GerarArquivoCteComplementoService", string.Format("GerarRegistro23110: {0}", ex.Message), ex);
				throw;
			}

			return buffer.ToString();
		}

		private string GerarRegistro23100()
		{
			StringBuilder buffer = new StringBuilder();

			try
			{
				double valorPrestacao = _listaCteComplementado.Sum(g => g.ValorDiferenca); 

				buffer.Append("23100");
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(valorPrestacao.ToString("0.00", CultureInfo.GetCultureInfo("en-US").NumberFormat));
			}
			catch (Exception ex)
			{
				_cteLogService.InserirLogErro(_cteComplementar, "GerarArquivoCteComplementoService", string.Format("GerarRegistro23100: {0}", ex.Message), ex);
				throw;
			}

			return buffer.ToString();
		}

		private string GerarRegistro23000()
		{
			StringBuilder buffer = new StringBuilder();

			try
			{
				string chaveCteComplementado = _cteComplementar.Chave;

				buffer.Append("23000");
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(chaveCteComplementado, 44));
			}
			catch (Exception ex)
			{
				_cteLogService.InserirLogErro(_cteComplementar, "GerarArquivoCteComplementoService", string.Format("GerarRegistro23000: {0}", ex.Message), ex);
				throw;
			}

			return buffer.ToString();
		}

		private void CarregarDados()
		{
			try
			{
				// Carregar os dados do cte raiz
				CteArvore cteArvore = _cteArvoreRepository.ObterCteArvorePorCteFilho(_cteComplementar);
				_cteRaiz = cteArvore.CteRaiz;

				// Carregar dados do CTE complementado
				_listaCteComplementado = _cteComplementadoRepository.ObterCteComplementadoPorCteComplementar(_cteComplementar);

				// Carregar os dados do Item de Despacho
				// _itemDespacho = _itemDespachoRepository.ObterItemDespachoPorDespacho(_cte.Despacho);

				// Carregar as informa��es do detalhe carregamento
				// _detalheCarregamento = _itemDespacho.DetalheCarregamento;

				// Carregar as informa��es de carregamento
				// _carregamento = _detalheCarregamento.Carregamento;

				// Informa��es do fluxo comercial
				_fluxoComercial = _cteComplementar.FluxoComercial;

				// Contrato hist�rico
                _contratoHistorico = _cteComplementar.ContratoHistorico ?? (_listaCteComplementado.Count() > 0 ? _listaCteComplementado[0].Cte.ContratoHistorico : null);

				// Dados de contrato
				// _contrato = _contratoHistorico.Contrato;

				// Dados do vag�o (pega o vagao do 1 complemento)
				if (_listaCteComplementado.Count > 0)
				{
					_vagao = _listaCteComplementado[0].Cte.Vagao;	
				}
				
				// Notas fiscais do despacho
				// _listaCteDetalhe = _cteDetalheRepository.ObterPorCte(_cte);
			}
			catch (Exception ex)
			{
				_cteLogService.InserirLogErro(_cteComplementar, "GerarArquivoCteComplementoService", string.Format("CarregarDados: {0}", ex.Message), ex);
				throw;
			}
		}

		private double CalcularBaseCalculoIcms()
		{
			double valorBaseCalculo = 0.0;

			try
			{
				int tipoTributacao = _contratoHistorico.TipoTributacao ?? 0;

				string letraFerrovia = _cteComplementar.Despacho.SerieDespachoUf.LetraFerrovia;
				double toneladaUtil = _cteComplementar.PesoVagao ?? 0.0;
				double aliquotaIcms = _contratoHistorico.PercentualAliquotaIcms ?? 0.0;
				double coeficiente = 100 - (100 / aliquotaIcms);

				IList<ComposicaoFreteContrato> listaComposicaoFreteContrato = _composicaoFreteContratoRepository.ObterPorNumeroContrato(_contratoHistorico.NumeroContrato, _cteRaiz.DataHora, letraFerrovia, true);
				IList<ComposicaoFreteContrato> listaComposicaoFreteContratoVg = _composicaoFreteContratoRepository.ObterPorNumeroContrato(_contratoHistorico.NumeroContrato, _cteRaiz.DataHora, "VG", true);

				double? valorTotal = 0.0;

				if (tipoTributacao == 0)
				{
					valorTotal = listaComposicaoFreteContrato.Sum(g => g.ValorSemIcms);
					if ((listaComposicaoFreteContratoVg.Count > 0) && (_vagao.EmpresaProprietaria.Sigla == "L"))
					{
						valorTotal += listaComposicaoFreteContratoVg.Sum(g => g.ValorSemIcms);
					}
				}
				else
				{
					valorTotal = listaComposicaoFreteContrato.Sum(g => g.ValorComIcms);
					if ((listaComposicaoFreteContratoVg.Count > 0) && (_vagao.EmpresaProprietaria.Sigla == "L"))
					{
						valorTotal += listaComposicaoFreteContratoVg.Sum(g => g.ValorComIcms);
					}
				}

				valorTotal *= toneladaUtil;

				valorBaseCalculo = (valorTotal * coeficiente).Value;
			}
			catch (Exception ex)
			{
				_cteLogService.InserirLogErro(_cteComplementar, "GerarArquivoCteComplementoService", string.Format("CalcularBaseCalculoIcms: {0}", ex.Message), ex);
				throw;
			}

			return valorBaseCalculo;
		}

		private void InserirComposicaoFreteCte()
		{
			ComposicaoFreteCte composicaoFreteCte;
			double valorComIcms;
			double valorSemIcms;

			double totalComIcms = 0.0;
			double totalSemIcms = 0.0;

			string letraFerrovia = _cteComplementar.Despacho.SerieDespachoUf.LetraFerrovia;
			double toneladaUtil = _cteComplementar.PesoVagao ?? 0.0;
			IList<ComposicaoFreteContrato> listaComposicaoFreteContrato = _composicaoFreteContratoRepository.ObterPorNumeroContrato(_contratoHistorico.NumeroContrato, _cteRaiz.DataHora, letraFerrovia, true);
			IList<ComposicaoFreteContrato> listaComposicaoFreteContratoVg = _composicaoFreteContratoRepository.ObterPorNumeroContrato(_contratoHistorico.NumeroContrato, _cteRaiz.DataHora, "VG", true);

			// Dados da composicao frete contrato
			foreach (ComposicaoFreteContrato composicaoFreteContrato in listaComposicaoFreteContrato)
			{
				valorComIcms = composicaoFreteContrato.ValorComIcms ?? 0;
				valorSemIcms = composicaoFreteContrato.ValorSemIcms ?? 0;

				// Insere na composi��o frete cte
				composicaoFreteCte = new ComposicaoFreteCte();
				composicaoFreteCte.Cte = _cteComplementar;
				composicaoFreteCte.CondicaoTarifa = composicaoFreteContrato.CondicaoTarifa;
				composicaoFreteCte.Ferrovia = composicaoFreteContrato.Ferrovia;
				composicaoFreteCte.ValorComIcms = valorComIcms * toneladaUtil;
				composicaoFreteCte.ValorSemIcms = valorSemIcms * toneladaUtil;
				_composicaoFreteCteRepository.Inserir(composicaoFreteCte);

				totalComIcms += composicaoFreteCte.ValorComIcms.Value;
				totalSemIcms += composicaoFreteCte.ValorSemIcms.Value;
			}

			// Dados da composicao frete contrato VG
			foreach (ComposicaoFreteContrato composicaoFreteContrato in listaComposicaoFreteContratoVg)
			{
				if (_vagao.EmpresaProprietaria.Sigla == "L")
				{
					valorComIcms = composicaoFreteContrato.ValorComIcms ?? 0;
					valorSemIcms = composicaoFreteContrato.ValorSemIcms ?? 0;

					// Insere na composi��o frete cte
					composicaoFreteCte = new ComposicaoFreteCte();
					composicaoFreteCte.Cte = _cteComplementar;
					composicaoFreteCte.CondicaoTarifa = composicaoFreteContrato.CondicaoTarifa;
					composicaoFreteCte.Ferrovia = composicaoFreteContrato.Ferrovia;
					composicaoFreteCte.ValorComIcms = valorComIcms * toneladaUtil;
					composicaoFreteCte.ValorSemIcms = valorSemIcms * toneladaUtil;
					_composicaoFreteCteRepository.Inserir(composicaoFreteCte);

					totalComIcms += composicaoFreteCte.ValorComIcms.Value;
					totalSemIcms += composicaoFreteCte.ValorSemIcms.Value;
				}
			}
			// Grava a totaliza��o dos dados
			// Insere na composi��o frete cte
			composicaoFreteCte = new ComposicaoFreteCte();
			composicaoFreteCte.Cte = _cteComplementar;
			composicaoFreteCte.CondicaoTarifa = "ICMI";
			composicaoFreteCte.Ferrovia = letraFerrovia;
			composicaoFreteCte.ValorComIcms = totalComIcms;
			composicaoFreteCte.ValorSemIcms = totalSemIcms;
			_composicaoFreteCteRepository.Inserir(composicaoFreteCte);
		}
	}
}
