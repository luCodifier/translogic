using Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico;

namespace Translogic.Modules.Core.Domain.Services.Ctes
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.Mail;
    using System.Net.Mime;
    using System.Reflection;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Threading;
    using System.Xml;
    using ALL.Core.Dominio;
    using Castle.Services.Transaction;
    using ICSharpCode.SharpZipLib.Core;
    using ICSharpCode.SharpZipLib.Zip;
    using Model.Acesso;
    using Model.Acesso.Repositories;
    using Model.Codificador;
    using Model.Codificador.Repositories;
    using Model.Diversos.Cte;
    using Model.Dto;
    using Model.Estrutura;
    using Model.FluxosComerciais;
    using Model.FluxosComerciais.ConhecimentoTransporteEletronico;
    using Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;
    using Model.FluxosComerciais.InterfaceSap.Repositories;
    using Model.FluxosComerciais.Ndd.Repositories;
    using Model.FluxosComerciais.Pedidos.Despachos;
    using Model.FluxosComerciais.Pedidos.Despachos.Repositories;
    using Model.FluxosComerciais.Repositories;
    using Model.FluxosComerciais.TriangulusConfig;
    using Model.FluxosComerciais.TriangulusConfig.Repositories;
    using Model.Trem.Veiculo.Vagao;
    using Model.Trem.Veiculo.Vagao.Repositories;
    using Model.Via;
    using Model.Via.Repositories;

    using NHibernate.Criterion;
    using NHibernate.Util;

    using Translogic.Core.Infrastructure;
    using Translogic.Core.Infrastructure.FileSystem.ExtensionMethods;
    using Translogic.Core.Infrastructure.Web;
    using Translogic.Modules.Core.Domain.Model.Diversos;
    using Translogic.Modules.Core.Domain.Model.Diversos.Bacen;
    using Translogic.Modules.Core.Domain.Model.Diversos.Bacen.Repositories;
    using Translogic.Modules.Core.Domain.Model.Diversos.Ibge;
    using Translogic.Modules.Core.Domain.Model.Diversos.Ibge.Repositories;
    using Translogic.Modules.Core.Domain.Model.Estrutura.Repositories;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Nfes;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Simconsultas.Ctes.Repositories;
    using Translogic.Modules.Core.Domain.Services.FluxosComerciais;
    using Translogic.Modules.Core.Domain.Services.Mdfes;
    using Translogic.Modules.ServicesClients.Domain.Models.cte;
    using Translogic.Modules.ServicesClients.Domain.ServicesClients.cte.Interfaces;
    using Util;
    using Translogic.Modules.Core.Domain.Model.PainelExpedicao.Documentos.Dto;
    using Translogic.Modules.Core.Spd.BLL;
    using Spd_Data = Translogic.Modules.Core.Spd.Data;
    using Translogic.Modules.Core.Spd;
    using Speed.Common;
    using Speed.Data;

    /// <summary>
    /// Servi�o de Cte
    /// </summary>
    [Transactional]
    public class CteService
    {
        private readonly ICteCanceladoRefaturamentoConfigRepository _cteCanceladoRefaturamentoConfigRepository;
        private readonly ICteInutilizacaoConfigRepository _cteInutilizacaoConfigRepository;
        private readonly ICteRepository _cteRepository;
        private readonly ICteVersaoRepository _cteVersaoRepository;
        private readonly ICteOrigemRepository _cteOrigemRepository;
        private readonly ICteConteinerRepository _cteConteinerRepository;
        private readonly ICteEmpresasRepository _cteEmpresaRepository;
        private readonly ICteArquivoFtpRepository _cteArquivoFtpRepository;
        private readonly IFluxoComercialRepository _fluxoComercialRepository;
        private readonly ICteComplementadoRepository _cteComplementadoRepository;
        private readonly IItemDespachoRepository _itemDespachoRepository;
        private readonly ICteEnvioPoolingRepository _cteEnvioPoolingRepository;
        private readonly ICteRecebimentoPoolingRepository _cteRecebimentoPoolingRepository;
        private readonly ICteSapPoolingRepository _cteSapPoolingRepository;
        private readonly ICteStatusRepository _cteStatusRepository;
        private readonly ITbDatabaseInputRepository _databaseInputRepository;
        private readonly CteLogService _cteLogService;
        private readonly ICteArvoreRepository _cteArvoreRepository;
        private readonly ICteStatusRetornoRepository _cteStatusRetornoRepository;
        private readonly IUsuarioRepository _usuarioRepository;
        private readonly ICteDetalheRepository _cteDetalheRepository;
        private readonly ICteAgrupamentoRepository _cteAgrupamentoRepository;
        private readonly IVagaoRepository _vagaoRepository;
        private readonly ICteTempDetalheRepository _cteTempDetalheRepository;
        private readonly ICteTempRepository _cteTempRepository;
        private readonly ICteAnuladoRepository _cteAnuladoRepository;
        private readonly FilaProcessamentoService _filaProcessamentoService;
        private readonly IConfiguracaoTranslogicRepository _configuracaoTranslogicRepository;
        private readonly ICteInterfaceRecebimentoConfigRepository _cteInterfaceRecebimentoConfigRepository;
        private readonly ICteInterfaceEnvioSapRepository _cteInterfaceEnvioSapRepository;
        private readonly ICteArquivoRepository _cteArquivoRepository;
        private readonly ICteInterfacePdfConfigRepository _cteInterfacePdfRepository;
        private readonly ICteInterfaceXmlConfigRepository _cteInterfaceXmlRepository;
        private readonly ICteInterfaceEnvioEmailRepository _cteInterfaceEnvioEmailRepository;
        private readonly ICteEnvioXmlRepository _cteEnvioXmlRepository;
        private readonly ICteEnvioEmailHistoricoRepository _cteEnvioEmailHistoricoRepository;
        private readonly INfe03FilialRepository _nfe03FilialRepository;
        private readonly ICteEnvioEmailErroRepository _cteEnvioEmailErroRepository;
        private readonly ICteMotivoCancelamentoRepository _cteMotivoCancelamentoRepository;
        private readonly IAgrupamentoUsuarioRepository _agrupamentoUsuarioRepository;
        private readonly ISerieDespachoUfRepository _serieDespachoUfRepository;
        private readonly ICteComunicadoRepository _cteComunicadoRepository;
        private readonly IControleAcessoService _controleAcessoService;
        private readonly NfeService _nfeService;
        private readonly ICidadeIbgeRepository _cidadeIbgeRepository;
        private readonly IPaisBacenRepository _paisBacenRepository;
        private readonly ICorrecaoCteRepository _correcaoCteRepository;
        private readonly ICorrecaoConteinerCteRepository _correcaoConteinerCteRepository;
        private readonly IAssociaFluxoVigenteRepository _associaFluxoVigenteRepository;
        private readonly IEmpresaClientePaisRepository _empresaClientePaisRepository;
        private readonly IAssociaFluxoInternacionalRepository _associaFluxoInternacionalRepository;
        private readonly IMargemTrechoRepository _margemTrechoRepository;
        private readonly ICteConfigEmailRepository _configEmailRepository;
        private readonly MdfeService _mdfeService;
        private readonly ICteSerieNumeroEmpresaUfRepository _cteSerieNumeroEmpresaUfRepository;
        private readonly IComposicaoFreteContratoRepository _composicaoFreteContratoRepository;
        private readonly IDiarioBordoCteRepository _diarioBordoCteRepository;
        private readonly ICteResponsavelProcedimentoRepository _cteResponsavelProcedimentoRepository;
        private readonly IReceiveFile _receiveFileSeguroBradesco;
        private readonly ICteEnvioSeguradoraHistRepository _cteEnvioSeguradoraHistRepository;
        private readonly IEmpresaRepository _empresaRepository;
        //private readonly ICteStatusRetornoRepository _cteStatusRetornoRepository;

        private readonly string _chaveCteRobo = "CTE_USUARIO_ROBO";
        private readonly string _chaveCteCodigoInternoFilial = "CTE_CODIGO_INTERNO_FILIAL_DEFAULT";
        private readonly string _chavePdfPath = "CTE_PDF_PATH";
        private readonly string _chaveDataMaximaCancelamento = "CTE_DATA_MAXIMA_CANCELAMENTO";
        private readonly string _chaveDiasInutilizacao = "CTE_DIAS_INUTILIZACAO";
        private readonly string _chaveMudancaMargemVagao = "TRAVA_MUDANCA_MARGEM_VAGAO";
        private readonly string _chaveCteTentativaReenvio = "CTE_TENTATIVA_REENVIO";
        private readonly string _chaveHostReenvioEmail = "CTE_HOST_REENVIO_EMAIL";
        private readonly string _cteEmailServer = "CTE_EMAIL_SERVER";
        private readonly string _cteEmailRemetente = "CTE_EMAIL_REMETENTE";
        private readonly string _cteEmailSenhaSmtp = "CTE_EMAIL_SMTP_SENHA";
        private readonly string _chaveDiasCancelamentoCte = "CTE_DIAS_CANCELAMENTO_CTE";
        private readonly string _chaveCodigoGrupoFiscal = "CTE_CODIGO_GRUPO_FISCAL";
        private readonly string _chaveTravaCancelamento = "CTE_HABILITAR_TRAVA_CANCELAMENTO_CTE";
        private readonly string _chaveTravaValidacaoAlteracaoFluxo = "CTE_HABILITAR_TRAVA_VALIDACAO_FLUXO_COMERCIAL";
        private const string ConfGeralCteVerificarCnpjRemetenteFiscal = "CTE_VERIFICAR_CNPJ_REMENTE_FISCAL";
        private const string ConfGeralCteVerificarCnpjDestinatarioFiscal = "CTE_VERIFICAR_CNPJ_DESTINATARIO_FISCAL";
        private const string ConfGeralCteVerificarCfop = "CTE_VERIFICAR_CFOP";
        private readonly string _chaveHorasCancelamentoCte = "CTE_HORAS_CANCELAMENTO_CTE";
        private readonly string _chaveEmailSeguradoraBradesco = "CTE_EMAIL_SEGURADORA_BRADESCO";
        private readonly string _chaveRemtenteEmailSeguradoraBradesco = "CTE_REMETENTE_EMAIL_SEGURADORA_BRADESCO";
        private readonly string _chaveSeguradoraBradesco = "CTE_SEGURADORA_ENVIO";
        private readonly string _cteAtivaEnvioSeguradora = "CTE_ATIVA_ENVIO_SEGURADORA";

        /// <summary>
        /// Initializes a new instance of the <see cref="CteService"/> class.
        /// </summary>
        /// <param name="cteRepository">Reposit�rio do Cte injetado</param>
        /// <param name="cteVersaoRepository">Reposit�rio do CteVersao injetado</param>
        /// <param name="fluxoComercialRepository">Reposit�rio do fluxo comercial injetado</param>
        /// <param name="cteComplementadoRepository">Reposit�rio do cte complementado injetado</param>
        /// <param name="itemDespachoRepository">Reposit�rio do item de despacho injetado</param>
        /// <param name="cteEnvioPoolingRepository">Reposit�rio do envio para o pooling injetado</param>
        /// <param name="cteRecebimentoPoolingRepository">Reposit�rio do recebimento do pooling injetado</param>
        /// <param name="cteLogService"> Servi�o do log do cte injetado</param>
        /// <param name="cteStatusRepository">Reposit�rio do status do cte injetado</param>
        /// <param name="databaseInputRepository">Reposit�rio do tbDatabaseInput injetado</param>
        /// <param name="cteArquivoFtpRepository">Reposit�rio de CteArquivoFtp injetado</param>
        /// <param name="cteStatusRetornoRepository">Repositorio de CteStatusRetorno injetado</param>
        /// <param name="usuarioRepository">Repositorio de Usuario injetado</param>
        /// <param name="cteDetalheRepository">Reposit�rio do Cte Detalhe injetad</param>
        /// <param name="cteArvoreRepository">Reposit�rio do Cte Arvore injetado</param>
        /// <param name="cteAgrupamentoRepository">Reposit�rio do Cte Agrupamento injetado</param>
        /// <param name="cteTempRepository">Reposit�rio do Cte Temp injetado</param>
        /// <param name="cteAnuladoRepository">Reposit�rio do Cte Temp injetado</param>
        /// <param name="vagaoRepository">Repot�rio de Vag�o Injetado</param>
        /// <param name="cteTempDetalheRepository">Reposit�rio de CteTempDetalhe injetado</param>
        /// <param name="filaProcessamentoService">Servi�o para gravar na fila de processamento da Config</param>
        /// <param name="configuracaoTranslogicRepository">Reposit�rio de configura��o do translogic injetado</param>
        /// <param name="cteInterfaceRecebimentoConfigRepository">Reposit�rio de interface de recebimento dos arquivos da config injetado</param>
        /// <param name="cteInterfaceEnvioSapRepository">Reposit�rio de interface de envio de cte para o SAP injetado</param>
        /// <param name="cteArquivoRepository">Reposit�rio do cte arquivo injetado</param>
        /// <param name="cteInterfacePdfRepository">Reposit�rio da interface de importa��o do Pdf</param>
        /// <param name="vsapZSDV0203VRepository">Reposit�rio da interface de vSapZSDV0203V (Verifica cte pago)</param>
        /// <param name="cteSapPoolingRepository">Reposit�rio do pooling de envio para o SAP</param>
        /// <param name="cteInterfaceXmlRepository">Reposit�rio do pooling de gera��o do XML</param>
        /// <param name="cteInterfaceEnvioEmailRepository">Reposito�rio de interface de envio de email</param>
        /// <param name="cteEnvioXmlRepository">Reposit�rio do envio de xml injetado</param>
        /// <param name="cteEnvioEmailHistoricoRepository">Reposit�rio de envio de email historico xml</param>
        /// <param name="nfe03FilialRepository">Filial da nota fiscal eletronica injetado</param>
        /// <param name="cteEnvioEmailErroRepository">Reposit�rio de envio de email com erro</param>
        /// <param name="cteMotivoCancelamentoRepository">Reposit�rio de motivo de cancelamento injetado</param>
        /// <param name="agrupamentoUsuarioRepository"> Reposit�rio de agrupamento de usu�rio injetado</param>
        /// <param name="serieDespachoUfRepository">Reposit�rio de Serie Despacho UF</param>
        /// <param name="cteComunicadoRepository"> Reposit�rio Comunicado injetado</param>
        /// <param name="controleAcessoService">  Servi�o Controle de acesso </param>
        /// <param name="cteEmpresasRepository"> Reposit�rio de CteEmpresa injetado </param>
        /// <param name="nfeService">Servi�o do Nfe Service</param>
        /// <param name="paisBacenRepository">Reposit�rio do codigo do pais no BACEN injetado</param>
        /// <param name="cidadeIbgeRepository">Reposit�rio da cidade IBGE injetado</param>
        /// <param name="correcaoCteRepository">Reposit�rio da Corre��o do Cte injetado</param>
        /// <param name="correcaoConteinerCteRepository">Reposit�rio da Corre��o do Conteiner Cte injetado</param>
        /// <param name="associaFluxoVigenteRepository"> Repositorio de associa��o de fluxo vigente injetado </param>
        /// <param name="associaFluxoInternacionalRepository">Reposit�rio de associa��o de fluxo internacional</param>
        /// <param name="empresaClientePaisRepository">Reposit�rio de empresa cliente pais injetado</param>
        /// <param name="margemTrechoRepository">Reposit�rio de Margens de Trecho</param>
        /// <param name="configEmailRepository">Reposit�rio de Configura��o email CTE</param>
        /// <param name="cteSerieNumeroEmpresaUfRepository">Reposit�rio para obter a lista de UF's</param>
        /// <param name="mdfeService">Servi�o de processamento de MDFE's</param>
        /// <param name="composicaoFreteContratoRepository">Reposit�rio de composicao Frete Contrato </param>
        /// <param name="diarioBordoCteRepository">Reposit�rio para <see cref="DiarioBordoCte"/></param>
        /// <param name="cteResponsavelProcedimentoRepository">Reposit�rio para <see cref="CteResponsavelProcedimento"/></param>
        /// <param name="receiveFileSeguroBradesco">Servico envio seguro bradesco </param>
        /// <param name="cteEnvioSeguradoraHistRepository">Servico envio seguradora historico </param>
        /// <param name="empresaRepository">Reposit�rio de empresa injetado</param>
        /// <param name="cteCanceladoRefaturamentoConfigRepository">Reposit�rio de Ctes Cancelados enviados para refaturamento</param>
        /// <param name="cteInutilizacaoConfigRepository">Reposit�rio de Ctes Inutilizados enviados para config</param>
        public CteService(ICteRepository cteRepository, ICteVersaoRepository cteVersaoRepository, ICteOrigemRepository cteOrigemRepository, ICteConteinerRepository cteConteinerRepository, IFluxoComercialRepository fluxoComercialRepository, ICteComplementadoRepository cteComplementadoRepository, IItemDespachoRepository itemDespachoRepository, ICteEnvioPoolingRepository cteEnvioPoolingRepository, ICteRecebimentoPoolingRepository cteRecebimentoPoolingRepository, CteLogService cteLogService, ICteStatusRepository cteStatusRepository, ITbDatabaseInputRepository databaseInputRepository, ICteArquivoFtpRepository cteArquivoFtpRepository, ICteStatusRetornoRepository cteStatusRetornoRepository, IUsuarioRepository usuarioRepository, ICteDetalheRepository cteDetalheRepository, ICteArvoreRepository cteArvoreRepository, ICteAgrupamentoRepository cteAgrupamentoRepository, ICteTempRepository cteTempRepository, ICteAnuladoRepository cteAnuladoRepository, IVagaoRepository vagaoRepository, ICteTempDetalheRepository cteTempDetalheRepository, FilaProcessamentoService filaProcessamentoService, IConfiguracaoTranslogicRepository configuracaoTranslogicRepository, ICteInterfaceRecebimentoConfigRepository cteInterfaceRecebimentoConfigRepository, ICteInterfaceEnvioSapRepository cteInterfaceEnvioSapRepository, ICteArquivoRepository cteArquivoRepository, ICteInterfacePdfConfigRepository cteInterfacePdfRepository, IVSapZSDV0203VRepository vsapZSDV0203VRepository, ICteSapPoolingRepository cteSapPoolingRepository, ICteInterfaceXmlConfigRepository cteInterfaceXmlRepository, ICteInterfaceEnvioEmailRepository cteInterfaceEnvioEmailRepository, ICteEnvioXmlRepository cteEnvioXmlRepository, ICteEnvioEmailHistoricoRepository cteEnvioEmailHistoricoRepository, INfe03FilialRepository nfe03FilialRepository, ICteEnvioEmailErroRepository cteEnvioEmailErroRepository, ICteMotivoCancelamentoRepository cteMotivoCancelamentoRepository, IAgrupamentoUsuarioRepository agrupamentoUsuarioRepository, ISerieDespachoUfRepository serieDespachoUfRepository, ICteComunicadoRepository cteComunicadoRepository, IControleAcessoService controleAcessoService, ICteEmpresasRepository cteEmpresasRepository, NfeService nfeService, IPaisBacenRepository paisBacenRepository, ICidadeIbgeRepository cidadeIbgeRepository, ICorrecaoCteRepository correcaoCteRepository, ICorrecaoConteinerCteRepository correcaoConteinerCteRepository, IAssociaFluxoVigenteRepository associaFluxoVigenteRepository, IAssociaFluxoInternacionalRepository associaFluxoInternacionalRepository, IEmpresaClientePaisRepository empresaClientePaisRepository, IMargemTrechoRepository margemTrechoRepository, ICteConfigEmailRepository configEmailRepository, ICteSerieNumeroEmpresaUfRepository cteSerieNumeroEmpresaUfRepository, MdfeService mdfeService, IComposicaoFreteContratoRepository composicaoFreteContratoRepository, IDiarioBordoCteRepository diarioBordoCteRepository, ICteResponsavelProcedimentoRepository cteResponsavelProcedimentoRepository, IReceiveFile receiveFileSeguroBradesco, ICteEnvioSeguradoraHistRepository cteEnvioSeguradoraHistRepository, IEmpresaRepository empresaRepository, ICteCanceladoRefaturamentoConfigRepository cteCanceladoRefaturamentoConfigRepository, ICteInutilizacaoConfigRepository cteInutilizacaoConfigRepository)
        {
            _cteRepository = cteRepository;
            _cteVersaoRepository = cteVersaoRepository;
            _cteOrigemRepository = cteOrigemRepository;
            _cteConteinerRepository = cteConteinerRepository;
            _controleAcessoService = controleAcessoService;
            _cteComunicadoRepository = cteComunicadoRepository;
            _serieDespachoUfRepository = serieDespachoUfRepository;
            _agrupamentoUsuarioRepository = agrupamentoUsuarioRepository;
            _cteMotivoCancelamentoRepository = cteMotivoCancelamentoRepository;
            _cteEnvioEmailErroRepository = cteEnvioEmailErroRepository;
            _nfe03FilialRepository = nfe03FilialRepository;
            _cteEnvioEmailHistoricoRepository = cteEnvioEmailHistoricoRepository;
            _cteEnvioXmlRepository = cteEnvioXmlRepository;
            _cteInterfaceEnvioEmailRepository = cteInterfaceEnvioEmailRepository;
            _cteInterfaceXmlRepository = cteInterfaceXmlRepository;
            _cteSapPoolingRepository = cteSapPoolingRepository;
            _cteInterfacePdfRepository = cteInterfacePdfRepository;
            _cteArquivoRepository = cteArquivoRepository;
            _cteInterfaceEnvioSapRepository = cteInterfaceEnvioSapRepository;
            _cteInterfaceRecebimentoConfigRepository = cteInterfaceRecebimentoConfigRepository;
            _configuracaoTranslogicRepository = configuracaoTranslogicRepository;
            _cteTempRepository = cteTempRepository;
            _cteAnuladoRepository = cteAnuladoRepository;
            _cteTempDetalheRepository = cteTempDetalheRepository;
            _vagaoRepository = vagaoRepository;
            _filaProcessamentoService = filaProcessamentoService;
            _usuarioRepository = usuarioRepository;
            _cteStatusRetornoRepository = cteStatusRetornoRepository;
            _cteArquivoFtpRepository = cteArquivoFtpRepository;
            _databaseInputRepository = databaseInputRepository;
            _cteStatusRepository = cteStatusRepository;
            _cteLogService = cteLogService;
            _cteRecebimentoPoolingRepository = cteRecebimentoPoolingRepository;
            _cteEnvioPoolingRepository = cteEnvioPoolingRepository;
            _fluxoComercialRepository = fluxoComercialRepository;
            _cteComplementadoRepository = cteComplementadoRepository;
            _itemDespachoRepository = itemDespachoRepository;
            _cteDetalheRepository = cteDetalheRepository;
            _cteArvoreRepository = cteArvoreRepository;
            _cteAgrupamentoRepository = cteAgrupamentoRepository;
            _cteEmpresaRepository = cteEmpresasRepository;
            _nfeService = nfeService;
            _cidadeIbgeRepository = cidadeIbgeRepository;
            _paisBacenRepository = paisBacenRepository;
            _correcaoCteRepository = correcaoCteRepository;
            _correcaoConteinerCteRepository = correcaoConteinerCteRepository;
            _associaFluxoVigenteRepository = associaFluxoVigenteRepository;
            _empresaClientePaisRepository = empresaClientePaisRepository;
            _associaFluxoInternacionalRepository = associaFluxoInternacionalRepository;
            _margemTrechoRepository = margemTrechoRepository;
            _configEmailRepository = configEmailRepository;
            _mdfeService = mdfeService;
            _composicaoFreteContratoRepository = composicaoFreteContratoRepository;
            _diarioBordoCteRepository = diarioBordoCteRepository;
            _cteResponsavelProcedimentoRepository = cteResponsavelProcedimentoRepository;
            _receiveFileSeguroBradesco = receiveFileSeguroBradesco;
            _cteEnvioSeguradoraHistRepository = cteEnvioSeguradoraHistRepository;
            _empresaRepository = empresaRepository;
            _cteCanceladoRefaturamentoConfigRepository = cteCanceladoRefaturamentoConfigRepository;
            _cteInutilizacaoConfigRepository = cteInutilizacaoConfigRepository;
            _cteSerieNumeroEmpresaUfRepository = cteSerieNumeroEmpresaUfRepository;
        }

        /// <summary>
        /// Retorna a lista de Cte com a flag de Pago preenchida
        /// </summary> 
        /// <param name="dtos">lista de cte dto</param>
        /// <returns>Retorna a lista dos ctes com a flag de Pago preenchida</returns>
        public IList<CteDto> RetornaCtePagos(CteDto[] dtos)
        {
            List<int> listaIdCte = new List<int>();

            foreach (CteDto dto in dtos)
            {
                listaIdCte.Add(dto.CteId);
            }

            IList<CteDto> retorno = _cteRepository.ObterCtesPago(listaIdCte);

            return retorno.Where(c => c.CtePago).OrderBy(c => c.Cte).ToList();
        }

        /// <summary>
        /// Retorna a lista de Ctes que n�o constam na tabela Cte_Empresas
        /// </summary> 
        /// <returns>Retorna a lista dos ctes com a flag de Pago preenchida</returns>
        public IList<CteDto> RetornaCtesNaoConstamCteEmpresas()
        {
            var retorno = _cteRepository.RetornaCtesNaoConstamCteEmpresas();

            return retorno.ToList();
        }

        /// <summary>
        /// Retorna todos os Ctes para Inutilizacao
        /// </summary>
        /// <param name="pagination"> Paginacao pesquisa</param>
        /// <param name="filtros"> Filtros para pesquisa</param>
        /// <returns>lista de Cte</returns>
        public ResultadoPaginado<CteDto> RetornaCteParaInutilizacao(DetalhesPaginacaoWeb pagination, DetalhesFiltro<object>[] filtros)
        {
            DateTime dataIni = DateTime.Now;
            DateTime dataFim = DateTime.Now;
            string serie = string.Empty;
            int numDespacho = 0;
            string erro = string.Empty;
            string fluxo = string.Empty;
            string origem = string.Empty;
            string destino = string.Empty;
            string numVagao = string.Empty;
            string chaveCte = string.Empty;
            int diasInutilizacao = 0;
            bool filtroForaData = false;
            string codigoUfDcl = string.Empty;

            ConfiguracaoTranslogic confDiasInutilizacao = _configuracaoTranslogicRepository.ObterPorId(_chaveDiasInutilizacao);

            diasInutilizacao = int.Parse(confDiasInutilizacao.Valor);

            if (filtros != null)
            {
                foreach (var detalheFiltro in filtros)
                {
                    if (detalheFiltro.Campo.Equals("dataInicial") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        string texto = detalheFiltro.Valor[0].ToString();

                        dataIni = DateTime.ParseExact(texto, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                    }

                    if (detalheFiltro.Campo.Equals("dataFinal") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        string texto = detalheFiltro.Valor[0].ToString();

                        dataFim = DateTime.ParseExact(texto, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);

                        dataFim = dataFim.AddDays(1).AddSeconds(-1);
                    }

                    if (detalheFiltro.Campo.Equals("serie") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        serie = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
                    }

                    if (detalheFiltro.Campo.Equals("despacho") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        numDespacho = Convert.ToInt32(detalheFiltro.Valor[0].ToString());
                    }

                    if (detalheFiltro.Campo.Equals("fluxo") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        fluxo = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
                    }

                    if (detalheFiltro.Campo.Equals("Origem") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        origem = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
                    }

                    if (detalheFiltro.Campo.Equals("UfDcl") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        codigoUfDcl = detalheFiltro.Valor[0].ToString().ToUpperInvariant().Trim();
                    }

                    if (detalheFiltro.Campo.Equals("Destino") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        destino = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
                    }

                    if (detalheFiltro.Campo.Equals("chave") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        chaveCte = detalheFiltro.Valor[0].ToString();
                    }

                    if (detalheFiltro.Campo.Equals("errocte") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        erro = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
                    }

                    if (detalheFiltro.Campo.Equals("Vagao") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        numVagao = detalheFiltro.Valor[0].ToString();
                    }

                    if (detalheFiltro.Campo.Equals("ForaDataCancelamento") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        bool.TryParse(detalheFiltro.Valor[0].ToString(), out filtroForaData);
                    }
                }
            }

            ResultadoPaginado<CteDto> listaDto = new ResultadoPaginado<CteDto>();

            listaDto = _cteRepository.ObterCteInutilizacao(pagination, dataIni, dataFim, serie, numDespacho, fluxo, chaveCte, erro, origem, destino, numVagao, diasInutilizacao, codigoUfDcl);

            return listaDto;
        }

        /// <summary>
        /// Retorna todos os Ctes para cancelamento
        /// </summary>
        /// <param name="pagination"> Paginacao pesquisa</param>
        /// <param name="filtros"> Filtros para pesquisa</param>
        /// <returns>lista de Cte</returns>
        public ResultadoPaginado<CteDto> RetornaCteParaCancelamento(DetalhesPaginacaoWeb pagination, DetalhesFiltro<object>[] filtros)
        {
            DateTime dataIni = DateTime.Now;
            DateTime dataFim = DateTime.Now;
            string serie = string.Empty;
            int numDespacho = 0;
            string erro = string.Empty;
            string fluxo = string.Empty;
            string origem = string.Empty;
            string destino = string.Empty;
            string[] numVagao = null;
            string[] chaveCte = null;
            int diasForaData = 0;
            bool filtroForaData = false;
            string codigoUfDcl = string.Empty;
            int horasCancelamento = 0;

            ConfiguracaoTranslogic dataMaximaCancelamento = _configuracaoTranslogicRepository.ObterPorId(_chaveDataMaximaCancelamento);

            diasForaData = int.Parse(dataMaximaCancelamento.Valor);

            if (filtros != null)
            {
                foreach (var detalheFiltro in filtros)
                {
                    if (detalheFiltro.Campo.Equals("dataInicial") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        string texto = detalheFiltro.Valor[0].ToString();

                        dataIni = DateTime.ParseExact(texto, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                    }

                    if (detalheFiltro.Campo.Equals("dataFinal") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        string texto = detalheFiltro.Valor[0].ToString();

                        dataFim = DateTime.ParseExact(texto, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);

                        dataFim = dataFim.AddDays(1).AddSeconds(-1);
                    }

                    if (detalheFiltro.Campo.Equals("serie") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        serie = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
                    }

                    if (detalheFiltro.Campo.Equals("despacho") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        numDespacho = Convert.ToInt32(detalheFiltro.Valor[0].ToString());
                    }

                    if (detalheFiltro.Campo.Equals("fluxo") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        fluxo = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
                    }

                    if (detalheFiltro.Campo.Equals("Origem") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        origem = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
                    }

                    if (detalheFiltro.Campo.Equals("UfDcl") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        codigoUfDcl = detalheFiltro.Valor[0].ToString().ToUpperInvariant().Trim();
                    }

                    if (detalheFiltro.Campo.Equals("Destino") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        destino = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
                    }

                    if (detalheFiltro.Campo.Equals("chave") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        chaveCte = detalheFiltro.Valor[0].ToString().Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                    }

                    if (detalheFiltro.Campo.Equals("errocte") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        erro = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
                    }

                    if (detalheFiltro.Campo.Equals("Vagao") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        numVagao = detalheFiltro.Valor[0].ToString().Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                    }

                    if (detalheFiltro.Campo.Equals("ForaDataCancelamento") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        bool.TryParse(detalheFiltro.Valor[0].ToString(), out filtroForaData);
                    }

                    int.TryParse(RecuperarValorConfGeral(_chaveHorasCancelamentoCte), out horasCancelamento);
                }
            }

            ResultadoPaginado<CteDto> listaDto = new ResultadoPaginado<CteDto>();

            listaDto = _cteRepository.ObterCteCancelamento(pagination, dataIni, dataFim, serie, numDespacho, fluxo, chaveCte, erro, origem, destino, numVagao, diasForaData, codigoUfDcl, horasCancelamento);

            if (filtroForaData)
            {
                listaDto.Items = listaDto.Items.Where(c => c.ForaDoTempoCancelamento).ToList();
            }

            return listaDto;
        }

        /// <summary>
        /// Realiza o salvamento dos complementos dos Ctes Selecionados
        /// </summary>
        /// <param name="dtos">dtos dos ctes</param>
        /// <param name="usuario">usuario logado</param>
        /// <param name="mensagem">Mensagem de status do salvamento</param>
        /// <returns>resultado do salvamento</returns>
        [Transaction]
        public virtual bool SalvarComplementoPesoCtes(CteDto[] dtos, Usuario usuario, out string mensagem)
        {
            try
            {
                var estaHabilitadaValidacaoSubsTrib = _configuracaoTranslogicRepository.ObterPorId("CTE_HABILITAR_VERIFICACAO_SUBS_TRIB").Valor;

                // Valida os CT-es verificando se tem algum fluxo de subst. tribut�ria
                // caso sim n�o deixa realizar o complemento
                if (dtos.Length > 0 && usuario != null)
                {
                    StringBuilder builder = new StringBuilder();
                    foreach (CteDto dto in dtos)
                    {
                        FluxoComercial fluxo = _fluxoComercialRepository.ObterPorCodigo(dto.Fluxo);
                        if (fluxo.SubstituicaoTributaria != null && fluxo.SubstituicaoTributaria.Value && estaHabilitadaValidacaoSubsTrib == "S")
                        {
                            string serie = dto.SerieDesp6 == 0 && dto.NumDesp6 == 0
                                    ? dto.SerieDesp5.ToString().PadLeft(3, '0')
                                    : string.Concat(dto.CodigoControle, "-", dto.SerieDesp6.ToString().PadLeft(3, '0'));

                            string despacho = dto.SerieDesp6 == 0 && dto.NumDesp6 == 0
                                                ? dto.NumDesp5.ToString().PadLeft(3, '0')
                                                : dto.NumDesp6.ToString().PadLeft(3, '0');

                            string aux = string.Format("CT-e numero: {0} Fluxo: {1} S�rie: {2} Despacho: {3} <BR/>", dto.Cte, fluxo.Codigo, serie, despacho);
                            builder.Append(aux);
                        }
                    }

                    if (builder.Length > 0)
                    {
                        builder.Insert(0, "N�o � permitido realizar complemento de Cte de Substitui��o Tribut�ria para o(s) seguinte(s) CT-e(s):</BR>");
                        mensagem = builder.ToString();
                        return false;
                    }
                }

                int idCteComplementar = 0;

                if (dtos.Length > 0 && usuario != null)
                {
                    foreach (CteDto dto in dtos)
                    {
                        FluxoComercial fluxoComercial = _fluxoComercialRepository.ObterPorCodigo(dto.Fluxo);
                        CteTemp cteTemp = new CteTemp();
                        cteTemp.IdFluxoComercial = fluxoComercial.Id;
                        cteTemp.CodigoVagao = _cteRepository.ObterPorId(dto.CteId).Vagao.Codigo;
                        _cteTempRepository.InserirOuAtualizar(cteTemp);

                        _cteRepository.GerarCteComplementar(usuario, out idCteComplementar);
                        if (idCteComplementar != 0)
                        {
                            CteComplementado cteComplementado = null;
                            cteComplementado = new CteComplementado();
                            cteComplementado.Cte = _cteRepository.ObterPorId(dto.CteId);
                            cteComplementado.ValorDiferenca = dto.ValorDiferencaComplemento;
                            cteComplementado.DataHora = DateTime.Now;
                            cteComplementado.Usuario = usuario;
                            cteComplementado.TipoComplemento = TipoOperacaoCteEnum.Complemento;
                            cteComplementado.CteComplementar = _cteRepository.ObterPorId(idCteComplementar);
                            cteComplementado.PesoDiferenca = dto.PesoDiferencaComplemento;

                            _cteComplementadoRepository.Inserir(cteComplementado);
                        }

                        _cteTempRepository.Remover(cteTemp);
                    }
                }
                else
                {
                    mensagem = "Cte Complementar n�o foi criado!";
                    return false;
                }
            }
            catch (Exception e)
            {
                mensagem = e.Message;
                return false;
            }

            mensagem = "A��o efetuada com Sucesso!";
            return true;
        }

        /// <summary>
        /// Retorna todos os Ctes para cancelamento
        /// </summary>
        /// <param name="pagination"> Paginacao pesquisa</param>
        /// <param name="filtros"> Filtros para pesquisa</param>
        /// <returns>lista de Cte</returns>
        public ResultadoPaginado<CteDto> RetornaCteParaComplemento(DetalhesPaginacaoWeb pagination, DetalhesFiltro<object>[] filtros)
        {
            DateTime dataIni = DateTime.Now;
            DateTime dataFim = DateTime.Now;
            string serie = string.Empty;
            int numDespacho = 0;
            string erro = string.Empty;
            string fluxo = string.Empty;
            string origem = string.Empty;
            string destino = string.Empty;
            string numVagao = string.Empty;
            string chaveCte = string.Empty;
            int diasForaData = 0;
            bool filtroForaData = false;
            string codigoDcl = string.Empty;

            ConfiguracaoTranslogic dataMaximaCancelamento = _configuracaoTranslogicRepository.ObterPorId(_chaveDataMaximaCancelamento);

            diasForaData = int.Parse(dataMaximaCancelamento.Valor);

            if (filtros != null)
            {
                foreach (var detalheFiltro in filtros)
                {
                    if (detalheFiltro.Campo.Equals("dataInicial") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        string texto = detalheFiltro.Valor[0].ToString();

                        dataIni = DateTime.ParseExact(texto, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                    }

                    if (detalheFiltro.Campo.Equals("dataFinal") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        string texto = detalheFiltro.Valor[0].ToString();

                        dataFim = DateTime.ParseExact(texto, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);

                        dataFim = dataFim.AddDays(1).AddSeconds(-1);
                    }

                    if (detalheFiltro.Campo.Equals("serie") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        serie = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
                    }

                    if (detalheFiltro.Campo.Equals("despacho") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        numDespacho = Convert.ToInt32(detalheFiltro.Valor[0].ToString());
                    }

                    if (detalheFiltro.Campo.Equals("fluxo") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        fluxo = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
                    }

                    if (detalheFiltro.Campo.Equals("Origem") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        origem = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
                    }

                    if (detalheFiltro.Campo.Equals("Destino") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        destino = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
                    }

                    if (detalheFiltro.Campo.Equals("chave") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        chaveCte = detalheFiltro.Valor[0].ToString();
                    }

                    if (detalheFiltro.Campo.Equals("errocte") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        erro = detalheFiltro.Valor[0].ToString();
                    }

                    if (detalheFiltro.Campo.Equals("Vagao") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        numVagao = detalheFiltro.Valor[0].ToString();
                    }

                    if (detalheFiltro.Campo.Equals("UfDcl") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        codigoDcl = detalheFiltro.Valor[0].ToString().ToUpperInvariant().Trim();
                    }
                }
            }

            ResultadoPaginado<CteDto> listaDto = new ResultadoPaginado<CteDto>();

            listaDto = _cteRepository.ObterCteComplemento(pagination, dataIni, dataFim, serie, numDespacho, fluxo, chaveCte, erro, origem, destino, numVagao, codigoDcl);

            return listaDto;
        }

        /// <summary>
        /// Retorna todos os Ctes para complemento de ICMS
        /// </summary>
        /// <param name="pagination"> Paginacao pesquisa</param>
        /// <param name="filtros"> Filtros para pesquisa</param>
        /// <returns>lista de Cte</returns>
        public ResultadoPaginado<CteDto> RetornaCteParaComplementoIcms(DetalhesPaginacaoWeb pagination, DetalhesFiltro<object>[] filtros)
        {
            DateTime dataIni = DateTime.Now;
            DateTime dataFim = DateTime.Now;
            string serie = string.Empty;
            int numDespacho = 0;
            string erro = string.Empty;
            string fluxo = string.Empty;
            string origem = string.Empty;
            string destino = string.Empty;
            string numVagao = string.Empty;
            string chaveCte = string.Empty;
            int diasForaData = 0;
            bool filtroForaData = false;
            string codigoDcl = string.Empty;

            ConfiguracaoTranslogic dataMaximaCancelamento = _configuracaoTranslogicRepository.ObterPorId(_chaveDataMaximaCancelamento);

            diasForaData = int.Parse(dataMaximaCancelamento.Valor);

            if (filtros != null)
            {
                foreach (var detalheFiltro in filtros)
                {
                    if (detalheFiltro.Campo.Equals("dataInicial") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        string texto = detalheFiltro.Valor[0].ToString();

                        dataIni = DateTime.ParseExact(texto, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                    }

                    if (detalheFiltro.Campo.Equals("dataFinal") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        string texto = detalheFiltro.Valor[0].ToString();

                        dataFim = DateTime.ParseExact(texto, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);

                        dataFim = dataFim.AddDays(1).AddSeconds(-1);
                    }

                    if (detalheFiltro.Campo.Equals("serie") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        serie = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
                    }

                    if (detalheFiltro.Campo.Equals("despacho") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        numDespacho = Convert.ToInt32(detalheFiltro.Valor[0].ToString());
                    }

                    if (detalheFiltro.Campo.Equals("fluxo") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        fluxo = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
                    }

                    if (detalheFiltro.Campo.Equals("Origem") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        origem = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
                    }

                    if (detalheFiltro.Campo.Equals("Destino") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        destino = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
                    }

                    if (detalheFiltro.Campo.Equals("chave") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        chaveCte = detalheFiltro.Valor[0].ToString();
                    }

                    if (detalheFiltro.Campo.Equals("errocte") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        erro = detalheFiltro.Valor[0].ToString();
                    }

                    if (detalheFiltro.Campo.Equals("Vagao") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        numVagao = detalheFiltro.Valor[0].ToString();
                    }

                    if (detalheFiltro.Campo.Equals("UfDcl") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        codigoDcl = detalheFiltro.Valor[0].ToString().ToUpperInvariant().Trim();
                    }
                }
            }

            ResultadoPaginado<CteDto> listaDto = new ResultadoPaginado<CteDto>();

            listaDto = _cteRepository.ObterCteComplementoIcms(pagination, dataIni, dataFim, serie, numDespacho, fluxo, chaveCte, erro, origem, destino, numVagao, codigoDcl);

            return listaDto;
        }

        /// <summary>
        ///    Retorna todos os Ctes para cancelamento
        /// </summary>
        /// <param name="pagination"> Paginacao pesquisa</param>
        /// <param name="filtros"> Filtros para pesquisa</param>
        /// <returns>lista de Cte</returns>
        public ResultadoPaginado<CteDto> RetornaCteParaTakeOrPay(DetalhesPaginacaoWeb pagination, DetalhesFiltro<object>[] filtros)
        {
            DateTime dataIni = DateTime.Now;
            DateTime dataFim = DateTime.Now;
            string serie = string.Empty;
            int numDespacho = 0;
            string erro = string.Empty;
            string fluxo = string.Empty;
            string origem = string.Empty;
            string destino = string.Empty;
            string numVagao = string.Empty;
            string chaveCte = string.Empty;
            string codigoDcl = string.Empty;

            if (filtros != null)
            {
                foreach (var detalheFiltro in filtros)
                {
                    if (detalheFiltro.Campo.Equals("dataInicial") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        string texto = detalheFiltro.Valor[0].ToString();

                        dataIni = DateTime.ParseExact(texto, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                    }

                    if (detalheFiltro.Campo.Equals("dataFinal") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        string texto = detalheFiltro.Valor[0].ToString();

                        dataFim = DateTime.ParseExact(texto, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);

                        dataFim = dataFim.AddDays(1).AddSeconds(-1);
                    }

                    if (detalheFiltro.Campo.Equals("serie") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        serie = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
                    }

                    if (detalheFiltro.Campo.Equals("despacho") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        numDespacho = Convert.ToInt32(detalheFiltro.Valor[0].ToString());
                    }

                    if (detalheFiltro.Campo.Equals("fluxo") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        fluxo = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
                    }

                    if (detalheFiltro.Campo.Equals("Origem") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        origem = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
                    }

                    if (detalheFiltro.Campo.Equals("Destino") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        destino = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
                    }

                    if (detalheFiltro.Campo.Equals("chave") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        chaveCte = detalheFiltro.Valor[0].ToString();
                    }

                    if (detalheFiltro.Campo.Equals("errocte") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        erro = detalheFiltro.Valor[0].ToString();
                    }

                    if (detalheFiltro.Campo.Equals("Vagao") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        numVagao = detalheFiltro.Valor[0].ToString();
                    }

                    if (detalheFiltro.Campo.Equals("UfDcl") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        codigoDcl = detalheFiltro.Valor[0].ToString().ToUpperInvariant().Trim();
                    }
                }
            }

            ResultadoPaginado<CteDto> listaDto = new ResultadoPaginado<CteDto>();

            listaDto = _cteRepository.ObterCteTakeOrPay(pagination, dataIni, dataFim, serie, numDespacho, fluxo, chaveCte, erro, origem, destino, numVagao, codigoDcl);

            return listaDto;
        }

        /////// <summary>
        ///////    Salvar as anula��es dos Ctes
        /////// </summary>
        /////// <param name="dtos">Paginacao pesquisa</param>
        /////// <param name="usuario">Usu�rio atual</param>
        /////// <returns>lista de Cte</returns>
        ////public KeyValuePair<bool, string> SalvarAnulacaoCtes(CteDto[] dtos, Usuario usuario)
        ////{
        ////    //MudarSituacaoCte(cte, SituacaoCteEnum.AguardandoAnulacao, usuario, new CteStatusRetorno { Id = 35 }, "Arquivo Cte alterado para aguardando anulacao", string.Empty);

        ////    //MudarSituacaoCte(cte, SituacaoCteEnum.AguardandoAnulacao, usuario, new CteStatusRetorno { Id = 35 }, "Arquivo Cte alterado para aguardando anulacao", string.Empty);

        ////    return null;
        ////}

        /// <summary>
        /// Realiza o Cancelamento dos Ctes
        /// </summary>
        /// <param name="dtos">dtos dos ctes</param>
        /// <param name="idMotivoCancelamento"> Id do motivo de cancelamento</param>
        /// <param name="usuario">Usu�rio atual</param>
        /// <returns>resultado do cancelamento</returns>
        public KeyValuePair<bool, string> SalvarCancelamentoCtes(CteDto[] dtos, int idMotivoCancelamento, Usuario usuario)
        {
            ////IList<CteDto> listaPagos = new List<CteDto>();
            var mensagemErro = new StringBuilder();
            var mensagemErroPrazoCancelamento = new StringBuilder();
            var ctesCancelar = ObterCtesAgrupadosRateio(dtos, true);
            var sucesso = true;

            try
            {
                var travarCancelamento = RecuperarValorConfGeral(_chaveTravaCancelamento).Equals("S");
                var travarMudancaMargem = RecuperarValorConfGeral(_chaveMudancaMargemVagao).Equals("S");

                // Recupera a lista de Ctes
                var cteMotivoCancelamento = _cteMotivoCancelamentoRepository.ObterPorId(idMotivoCancelamento);

                foreach (var id in ctesCancelar)
                {
                    var cte = _cteRepository.ObterPorId((int)id);
                    SalvarCancelamentoCte(usuario, cte, travarMudancaMargem, ref sucesso, mensagemErro, cteMotivoCancelamento, travarCancelamento);
                }
            }
            catch (Exception ex)
            {
                mensagemErro.AppendLine(ex.Message);
                return new KeyValuePair<bool, string>(false, mensagemErro.ToString());
            }

            if (!string.IsNullOrEmpty(mensagemErroPrazoCancelamento.ToString()))
            {
                mensagemErro.AppendLine(string.Format("Os CTes ({0}) s�o de meses anteriores e s� poder�o ser cancelados pelo fiscal.", mensagemErroPrazoCancelamento.ToString()));
            }

            return new KeyValuePair<bool, string>(sucesso, sucesso ? "Cancelado com Sucesso!" : mensagemErro.ToString());
        }

        public List<decimal> ObterCtesAgrupadosRateio(CteDto[] dtos, bool agrupCarga = false)
        {
            List<decimal> ctesProcessar = new List<decimal>();
            foreach (CteDto dto in dtos)
            {
                //// se � rateio cte, busca os ctes do conteiner/carga para processar.
                if (dto.RateioCte)
                {
                    var ctes = ObterCteConteinersAgrupados(dto.CteId, agrupCarga);

                    if (ctes.Count > 0)
                    {
                        foreach (var cteConteiner in ctes)
                        {
                            if (!(ctesProcessar.Any(p => p == cteConteiner.Cte.Id)))
                            {
                                ctesProcessar.Add(cteConteiner.Cte.Id ?? 0);
                            }
                        }
                    }
                    else
                    {
                        if (!(ctesProcessar.Any(p => p == dto.CteId)))
                        {
                            ctesProcessar.Add(dto.CteId);
                        }
                    }
                }
                else
                {
                    if (!(ctesProcessar.Any(p => p == dto.CteId)))
                    {
                        ctesProcessar.Add(dto.CteId);
                    }
                }
            }
            return ctesProcessar;
        }

        public bool SalvarCancelamentoCte(Usuario usuario, Cte cteCancelamento, bool travarMudancaMargem, ref bool sucesso,
            StringBuilder mensagemErro, CteMotivoCancelamento cteMotivoCancelamento, bool travarCancelamento)
        {
            if (!cteCancelamento.SituacaoAtual.Equals(SituacaoCteEnum.Autorizado))
            {
                return sucesso;
            }

            // Validar Mudan�a de margem (S� � permitido uma mudan�a de margem por despacho)
            if (travarMudancaMargem)
            {
                var margensFluxo = _margemTrechoRepository.ObterPorDestinoFluxo(cteCancelamento.FluxoComercial);
                var cteParaComparacao = _cteRepository.ObterCteAnteriorVagao(cteCancelamento);

                if (cteParaComparacao != null)
                {
                    var idAoDestino = cteParaComparacao.FluxoComercial.Destino.Id;
                    if (cteCancelamento.FluxoComercial.Destino.Id != idAoDestino &&
                        margensFluxo.Any(a => a.MargemDireita.Id == idAoDestino || a.MargemEsquerda.Id == idAoDestino))
                    {
                        sucesso = false;
                        mensagemErro.AppendLine(string.Concat("Imposs�vel cancelar o CTE (", cteCancelamento.Chave,
                            "), Mudan�a de margem � permitida apenas 1 vez!"));
                        return sucesso;
                    }
                }
            }

            var mdfe = _mdfeService.ObterMdfePorCte(cteCancelamento.Id ?? 0);
            // TRATAMENTO PARA OS CASOS EM QUE O CTE TEM VINCULO COM UM MDFE AUTORIZADO OU ENCERRADO
            if (mdfe != null)
            {
                var now = DateTime.Now;
                if (mdfe.DataRecibo < now.AddHours(-24))
                {
                    // somente grupo fiscal pode prosseguir no processo caso o mdfe esteaja com prazo acima de 24 horas
                    var grupoFiscal = RecuperarValorConfGeral(_chaveCodigoGrupoFiscal);
                    if (_controleAcessoService.VerificarUsuarioPertenceGrupo(usuario.Codigo, grupoFiscal))
                    {
                        cteCancelamento.CteMotivoCancelamento = cteMotivoCancelamento;
                        _cteRepository.Atualizar(cteCancelamento);

                        if (cteMotivoCancelamento.EnviaSefaz == false)
                        {
                            MudarSituacaoCte(cteCancelamento, SituacaoCteEnum.AguardandoAnulacao, usuario, new CteStatusRetorno { Id = 19 },
                                "Arquivo Cte alterado para aguardando anulacao", null);
                            sucesso = false;
                            mensagemErro.AppendLine(string.Concat("AVISO - CTE (", cteCancelamento.Chave, ") enviado para anula��o"));
                        }
                        else if (travarCancelamento)
                        {
                            if (VerificarPrazoCancelamento(cteCancelamento))
                            {
                                MudarSituacaoCte(cteCancelamento, SituacaoCteEnum.AguardandoCancelamento, usuario,
                                    new CteStatusRetorno { Id = 19 }, "Arquivo Cte alterado para aguardando cancelamento",
                                    null);
                            }
                            else
                            {
                                MudarSituacaoCte(cteCancelamento, SituacaoCteEnum.AguardandoAnulacao, usuario,
                                    new CteStatusRetorno { Id = 35 }, "Arquivo Cte alterado para aguardando anulacao",
                                    null);
                                sucesso = false;
                                mensagemErro.AppendLine(string.Concat("AVISO - CTE (", cteCancelamento.Chave, ") enviado para anula��o"));
                            }
                        }
                        else
                        {
                            // mudando Status CTE para Aguardando Cancelamento (19)
                            MudarSituacaoCte(cteCancelamento, SituacaoCteEnum.AguardandoCancelamento, usuario,
                                new CteStatusRetorno { Id = 19 }, "Arquivo Cte alterado para aguardando cancelamento",
                                null);
                        }
                    }
                    else
                    {
                        sucesso = false;
                        mensagemErro.AppendLine(string.Concat("Usu�rio n�o tem permiss�o para cancelar o CTE (", cteCancelamento.Chave,
                            "), Mdfe (" + mdfe.Numero + ") com mais de 24 horas."));
                    }
                }
                else
                {
                    sucesso = false;
                    mensagemErro.AppendLine(string.Concat("Imposs�vel cancelar o CTE (", cteCancelamento.Chave,
                        "), Cancelar primeiro o Mdfe (" + mdfe.Numero + ")."));
                }
            }
            // TRATAMENTO PARA OS CASOS EM QUE O CTE N�O EST� VINCULADO A UM MDFE
            else
            {
                cteCancelamento.CteMotivoCancelamento = cteMotivoCancelamento;
                _cteRepository.Atualizar(cteCancelamento);

                if (cteMotivoCancelamento.EnviaSefaz == false)
                {
                    MudarSituacaoCte(cteCancelamento, SituacaoCteEnum.AguardandoAnulacao, usuario, new CteStatusRetorno { Id = 19 },
                        "Arquivo Cte alterado para aguardando anulacao", null);
                    sucesso = false;
                    mensagemErro.AppendLine(string.Concat("AVISO - CTE (", cteCancelamento.Chave, ") enviado para anula��o"));
                }
                else if (travarCancelamento)
                {
                    if (VerificarPrazoCancelamento(cteCancelamento))
                    {
                        MudarSituacaoCte(cteCancelamento, SituacaoCteEnum.AguardandoCancelamento, usuario, new CteStatusRetorno { Id = 19 },
                            "Arquivo Cte alterado para aguardando cancelamento", null);
                    }
                    else
                    {
                        MudarSituacaoCte(cteCancelamento, SituacaoCteEnum.AguardandoAnulacao, usuario, new CteStatusRetorno { Id = 35 },
                            "Arquivo Cte alterado para aguardando anulacao", null);
                        sucesso = false;
                        mensagemErro.AppendLine(string.Concat("AVISO - CTE (", cteCancelamento.Chave, ") enviado para anula��o"));
                    }
                }
                else
                {
                    // mudando Status CTE para Aguardando Cancelamento (19)
                    MudarSituacaoCte(cteCancelamento, SituacaoCteEnum.AguardandoCancelamento, usuario, new CteStatusRetorno { Id = 19 },
                        "Arquivo Cte alterado para aguardando cancelamento", null);
                }
            }

            return sucesso;
        }

        /// <summary>
        /// Retorna todos os Cte para Complemento
        /// </summary>
        /// <param name="dataInicial">Data inicial de pesquisa</param>
        /// <param name="dataFinal">Data final de pesquisa</param>
        /// <param name="serie">N�mero da s�rie</param>
        /// <param name="numDespacho">N�mero do Despacho</param>
        /// <param name="fluxo">N�mero do Fluxo</param>
        /// <returns>lista de Cte</returns>
        public IList<CteDto> RetornaCteParaComplemento(DateTime dataInicial, DateTime dataFinal, int serie, int numDespacho, string fluxo)
        {
            DateTime dataIni = new DateTime(dataInicial.Year, dataInicial.Month, dataInicial.Day);

            DateTime dataFim = new DateTime(dataFinal.Year, dataFinal.Month, dataFinal.Day, 23, 59, 59);

            IList<CteDto> listaDto = new List<CteDto>();

            CteDto dto = null;

            DespachoTranslogic despacho = null;
            FluxoComercial fluxoComercial = null;
            Mercadoria mercadoria = null;
            EmpresaCliente empresaClientePagadora = null;
            EstacaoMae estacaoMaeOrigem = null;
            EstacaoMae estacaoMaeDestino = null;

            IList<Cte> listaCte = _cteRepository.Obter(dataIni, dataFim, serie, numDespacho);

            foreach (Cte cte in listaCte)
            {
                bool cteComAgrupamentoNaoAutorizado = false;

                IList<CteArvore> listaArvore = _cteArvoreRepository.ObterArvorePorCteFilho(cte);

                CteArvore cteArvore = listaArvore.OrderByDescending(c => c.Id).FirstOrDefault();

                if (cteArvore == null)
                {
                    continue;
                }

                IList<CteAgrupamento> listaCteAgrupamento = _cteAgrupamentoRepository.ObterAgrupamentoPorCteFilho(cteArvore.CteFilho);

                foreach (CteAgrupamento cteAgrupamento in listaCteAgrupamento)
                {
                    if (cteAgrupamento.CteFilho.SituacaoAtual != SituacaoCteEnum.Autorizado)
                    {
                        cteComAgrupamentoNaoAutorizado = true;
                    }
                }

                foreach (CteAgrupamento cteAgrupamento in listaCteAgrupamento)
                {
                    Cte cteTmp = cteAgrupamento.CteFilho;

                    if (ContainCteDto(listaDto.ToList(), typeof(IList<CteDto>), (int)cteTmp.Id))
                    {
                        continue;
                    }

                    despacho = cteTmp.Despacho;

                    // para ser listado tem que ter despacho, ser autorizado no SEFAZ
                    if (despacho != null && cteTmp.SituacaoAtual == SituacaoCteEnum.Autorizado)
                    {
                        fluxoComercial = cteTmp.FluxoComercial;

                        if (fluxoComercial != null)
                        {
                            mercadoria = fluxoComercial.Mercadoria;
                            empresaClientePagadora = fluxoComercial.EmpresaPagadora;
                            estacaoMaeOrigem = fluxoComercial.Origem;
                            estacaoMaeDestino = fluxoComercial.Destino;

                            if (cteTmp.ListaComplemento.Any())
                            {
                                foreach (var cteComplementado in cteTmp.ListaComplemento)
                                {
                                    dto = new CteDto();
                                    dto.CteId = cteTmp.Id ?? 0;
                                    dto.Fluxo = fluxoComercial.Codigo;
                                    dto.Origem = estacaoMaeOrigem != null ? estacaoMaeOrigem.Codigo : string.Empty;
                                    dto.Destino = estacaoMaeDestino != null ? estacaoMaeDestino.Codigo : string.Empty;
                                    dto.Mercadoria = mercadoria != null ? mercadoria.DescricaoResumida : string.Empty;
                                    dto.ClienteFatura = empresaClientePagadora != null ? empresaClientePagadora.NomeFantasia : string.Empty;
                                    dto.Cte = cteTmp.Serie + "-" + cteTmp.Numero;
                                    dto.SerieDesp5 = despacho.SerieDespacho != null ? despacho.SerieDespacho.SerieDespachoNum : string.Empty;
                                    dto.NumDesp5 = despacho.NumeroDespacho ?? 0;
                                    dto.SerieDesp6 = despacho.SerieDespachoUf != null ? despacho.SerieDespachoUf.NumeroSerieDespacho : 0;
                                    dto.NumDesp6 = despacho.NumeroDespacho ?? 0;
                                    dto.DataEmissao = cteTmp.DataHora;
                                    dto.TipoCte = cteTmp.TipoCte;
                                    // dto.Complementado = cteTmp.Complemento != null ? true : false;
                                    dto.ValorCte = cteTmp.ValorCte != 0 ? cteTmp.ValorCte : 0;
                                    dto.ValorDiferencaComplemento = cteComplementado != null ? cteComplementado.ValorDiferenca : 0;
                                    dto.PesoVagao = cteTmp.PesoVagao ?? 0;
                                    dto.CodigoUsuario = cteComplementado != null ? cteComplementado.Usuario.Codigo : string.Empty;
                                    // dto.DataComplemento = cteTmp.Complemento != null ? cteTmp.Complemento.DataHora.ToString("dd/MM/yyyy HH:mm:ss") : string.Empty;
                                    dto.CteComAgrupamentoNaoAutorizado = cteComAgrupamentoNaoAutorizado;
                                    dto.CteRaiz = cteTmp.Id == cteAgrupamento.CteRaiz.Id ? true : false;
                                    listaDto.Add(dto);
                                }
                            }
                            else
                            {
                                dto = new CteDto();
                                dto.CteId = cteTmp.Id ?? 0;
                                dto.Fluxo = fluxoComercial.Codigo;
                                dto.Origem = estacaoMaeOrigem != null ? estacaoMaeOrigem.Codigo : string.Empty;
                                dto.Destino = estacaoMaeDestino != null ? estacaoMaeDestino.Codigo : string.Empty;
                                dto.Mercadoria = mercadoria != null ? mercadoria.DescricaoResumida : string.Empty;
                                dto.ClienteFatura = empresaClientePagadora != null ? empresaClientePagadora.NomeFantasia : string.Empty;
                                dto.Cte = cteTmp.Serie + "-" + cteTmp.Numero;
                                dto.SerieDesp5 = despacho.SerieDespacho != null ? despacho.SerieDespacho.SerieDespachoNum : string.Empty;
                                dto.NumDesp5 = despacho.NumeroDespacho ?? 0;
                                dto.SerieDesp6 = despacho.SerieDespachoUf != null ? despacho.SerieDespachoUf.NumeroSerieDespacho : 0;
                                dto.NumDesp6 = despacho.NumeroDespacho ?? 0;
                                dto.DataEmissao = cteTmp.DataHora;
                                dto.TipoCte = cteTmp.TipoCte;
                                // dto.Complementado = cteTmp.Complemento != null ? true : false;
                                dto.ValorCte = cteTmp.ValorCte != 0 ? cteTmp.ValorCte : 0;
                                dto.ValorDiferencaComplemento = 0;
                                dto.PesoVagao = cteTmp.PesoVagao ?? 0;
                                dto.CodigoUsuario = string.Empty;
                                // dto.DataComplemento = cteTmp.Complemento != null ? cteTmp.Complemento.DataHora.ToString("dd/MM/yyyy HH:mm:ss") : string.Empty;
                                dto.CteComAgrupamentoNaoAutorizado = cteComAgrupamentoNaoAutorizado;
                                dto.CteRaiz = cteTmp.Id == cteAgrupamento.CteRaiz.Id ? true : false;
                                listaDto.Add(dto);
                            }
                        }
                    }
                }
            }

            return listaDto;
        }

        /// <summary>
        /// Retorna todos os Cte para Take Or Pay
        /// </summary>
        /// <param name="dataInicial">Data inicial de pesquisa</param>
        /// <param name="dataFinal">Data final de pesquisa</param>
        /// <param name="serie">N�mero da s�rie</param>
        /// <param name="numDespacho">N�mero do Despacho</param>
        /// <param name="fluxo">N�mero do Fluxo</param>
        /// <param name="serieCte">S�rie do CTE</param>
        /// <param name="nroCte">N�mero do CTE</param>
        /// <returns>lista de Cte</returns>
        public IList<CteDto> RetornaCteParaTakeOrPay(DateTime dataInicial, DateTime dataFinal, int serie, int numDespacho, string fluxo, int serieCte, int nroCte)
        {
            DateTime dataIni = new DateTime(dataInicial.Year, dataInicial.Month, dataInicial.Day);

            DateTime dataFim = new DateTime(dataFinal.Year, dataFinal.Month, dataFinal.Day, 23, 59, 59);

            IList<CteDto> listaDto = new List<CteDto>();

            CteDto dto = null;

            DespachoTranslogic despacho = null;
            FluxoComercial fluxoComercial = null;
            Mercadoria mercadoria = null;
            EmpresaCliente empresaClientePagadora = null;
            EstacaoMae estacaoMaeOrigem = null;
            EstacaoMae estacaoMaeDestino = null;

            IList<Cte> listaCte = _cteRepository.Obter(dataIni, dataFim, serie, numDespacho, string.Empty, string.Empty, serieCte, nroCte);

            foreach (Cte cte in listaCte)
            {
                bool cteComAgrupamentoNaoAutorizado = false;

                IList<CteArvore> listaArvore = _cteArvoreRepository.ObterArvorePorCteFilho(cte);

                CteArvore cteArvore = listaArvore.OrderByDescending(c => c.Id).FirstOrDefault();

                if (cteArvore == null)
                {
                    continue;
                }

                IList<CteAgrupamento> listaCteAgrupamento = _cteAgrupamentoRepository.ObterAgrupamentoPorCteFilho(cteArvore.CteFilho);

                foreach (CteAgrupamento cteAgrupamento in listaCteAgrupamento)
                {
                    if (cteAgrupamento.CteFilho.SituacaoAtual != SituacaoCteEnum.Autorizado)
                    {
                        cteComAgrupamentoNaoAutorizado = true;
                    }
                }

                foreach (CteAgrupamento cteAgrupamento in listaCteAgrupamento)
                {
                    Cte cteTmp = cteAgrupamento.CteFilho;

                    if (ContainCteDto(listaDto.ToList(), typeof(IList<CteDto>), (int)cteTmp.Id))
                    {
                        continue;
                    }

                    despacho = cteTmp.Despacho;

                    // para ser listado tem que ter despacho, ser autorizado no SEFAZ
                    if (despacho != null && cteTmp.SituacaoAtual == SituacaoCteEnum.Autorizado)
                    {
                        fluxoComercial = cteTmp.FluxoComercial;

                        if (fluxoComercial != null)
                        {
                            mercadoria = fluxoComercial.Mercadoria;
                            empresaClientePagadora = fluxoComercial.EmpresaPagadora;
                            estacaoMaeOrigem = fluxoComercial.Origem;
                            estacaoMaeDestino = fluxoComercial.Destino;


                            if (cteTmp.ListaComplemento.Any())
                            {
                                foreach (var cteComplementado in cteTmp.ListaComplemento)
                                {
                                    dto = new CteDto();
                                    dto.CteId = cteTmp.Id ?? 0;
                                    dto.Fluxo = fluxoComercial.Codigo;
                                    dto.Origem = estacaoMaeOrigem != null ? estacaoMaeOrigem.Codigo : string.Empty;
                                    dto.Destino = estacaoMaeDestino != null ? estacaoMaeDestino.Codigo : string.Empty;
                                    dto.Mercadoria = mercadoria != null ? mercadoria.DescricaoResumida : string.Empty;
                                    dto.ClienteFatura = empresaClientePagadora != null
                                                            ? empresaClientePagadora.NomeFantasia
                                                            : string.Empty;
                                    dto.Cte = cteTmp.Serie + "-" + cteTmp.Numero;
                                    dto.SerieDesp5 = despacho.SerieDespacho != null
                                                         ? despacho.SerieDespacho.SerieDespachoNum
                                                         : string.Empty;
                                    dto.NumDesp5 = despacho.NumeroDespacho ?? 0;
                                    dto.SerieDesp6 = despacho.SerieDespachoUf != null
                                                         ? despacho.SerieDespachoUf.NumeroSerieDespacho
                                                         : 0;
                                    dto.NumDesp6 = despacho.NumeroDespacho ?? 0;
                                    dto.DataEmissao = cteTmp.DataHora;
                                    dto.TipoCte = cteTmp.TipoCte;
                                    dto.ValorCte = cteTmp.ValorCte != 0 ? cteTmp.ValorCte : 0;
                                    dto.ValorDiferencaComplemento = cteComplementado != null
                                                                        ? cteComplementado.ValorDiferenca
                                                                        : 0;
                                    dto.PesoVagao = cteTmp.PesoVagao ?? 0;
                                    dto.CodigoUsuario = cteComplementado != null
                                                            ? cteComplementado.Usuario.Codigo
                                                            : string.Empty;
                                    // dto.DataComplemento = cteTmp.Complemento != null ? cteTmp.Complemento.DataHora.ToString("dd/MM/yyyy HH:mm:ss") : string.Empty;
                                    dto.CteComAgrupamentoNaoAutorizado = cteComAgrupamentoNaoAutorizado;
                                    listaDto.Add(dto);
                                }
                            }
                            else
                            {
                                dto = new CteDto();
                                dto.CteId = cteTmp.Id ?? 0;
                                dto.Fluxo = fluxoComercial.Codigo;
                                dto.Origem = estacaoMaeOrigem != null ? estacaoMaeOrigem.Codigo : string.Empty;
                                dto.Destino = estacaoMaeDestino != null ? estacaoMaeDestino.Codigo : string.Empty;
                                dto.Mercadoria = mercadoria != null ? mercadoria.DescricaoResumida : string.Empty;
                                dto.ClienteFatura = empresaClientePagadora != null
                                                        ? empresaClientePagadora.NomeFantasia
                                                        : string.Empty;
                                dto.Cte = cteTmp.Serie + "-" + cteTmp.Numero;
                                dto.SerieDesp5 = despacho.SerieDespacho != null
                                                     ? despacho.SerieDespacho.SerieDespachoNum
                                                     : string.Empty;
                                dto.NumDesp5 = despacho.NumeroDespacho ?? 0;
                                dto.SerieDesp6 = despacho.SerieDespachoUf != null
                                                     ? despacho.SerieDespachoUf.NumeroSerieDespacho
                                                     : 0;
                                dto.NumDesp6 = despacho.NumeroDespacho ?? 0;
                                dto.DataEmissao = cteTmp.DataHora;
                                dto.TipoCte = cteTmp.TipoCte;
                                dto.ValorCte = cteTmp.ValorCte != 0 ? cteTmp.ValorCte : 0;
                                dto.ValorDiferencaComplemento = 0;
                                dto.PesoVagao = cteTmp.PesoVagao ?? 0;
                                dto.CodigoUsuario = string.Empty;
                                // dto.DataComplemento = cteTmp.Complemento != null ? cteTmp.Complemento.DataHora.ToString("dd/MM/yyyy HH:mm:ss") : string.Empty;
                                dto.CteComAgrupamentoNaoAutorizado = cteComAgrupamentoNaoAutorizado;
                                listaDto.Add(dto);
                            }
                        }
                    }
                }
            }

            return listaDto;
        }

        /// <summary>
        /// Obt�m as UF's dos estados
        /// </summary>
        /// <returns>Lista de Estados</returns>
        public IList<UFDto> ObterUFs()
        {
            var ufs = _cteSerieNumeroEmpresaUfRepository.ObterTodos();

            var listaUfs = new List<UFDto>();

            foreach (var uf in ufs.Where(uf => !listaUfs.Any(u => u.Uf.Equals(uf.Uf))))
            {
                listaUfs.Add(new UFDto()
                {
                    Uf = uf.Uf
                });
            }

            return listaUfs.OrderBy(u => u.Uf).ToList();
        }

        public CteOrigem ObterChaveCteOrigemPorCte(Cte cte)
        {
            var ctesOrigem = _cteOrigemRepository.ObterPorCte(cte);
            CteOrigem cteOrigem = null;

            if (ctesOrigem != null)
            {
                var cteOrigemId = ctesOrigem.Min(c => c.Id);
                cteOrigem = ctesOrigem.Where(c => c.Id == cteOrigemId).FirstOrDefault();
            }

            return cteOrigem;
        }

        /// <summary>
        /// Realiza o salvamento dos complementos dos Ctes Selecionados
        /// </summary>
        /// <param name="dtos">dtos dos ctes</param>
        /// <param name="usuario">usuario logado</param>
        /// <param name="mensagem">Mensagem de status do salvamento</param>
        /// <returns>resultado do salvamento</returns>
        [Transaction]
        public virtual bool SalvarComplementoIcms(CteDto[] dtos, Usuario usuario, out string mensagem)
        {
            try
            {
                var estaHabilitadaValidacaoSubsTrib = _configuracaoTranslogicRepository.ObterPorId("CTE_HABILITAR_VERIFICACAO_SUBS_TRIB").Valor;

                // Valida os CT-es verificando se tem algum fluxo de subst. tribut�ria
                // caso sim n�o deixa realizar o complemento
                if (dtos.Length > 0 && usuario != null)
                {
                    StringBuilder builder = new StringBuilder();
                    foreach (CteDto dto in dtos)
                    {
                        FluxoComercial fluxo = _fluxoComercialRepository.ObterPorCodigo(dto.Fluxo);
                        if (fluxo.SubstituicaoTributaria != null && fluxo.SubstituicaoTributaria.Value && estaHabilitadaValidacaoSubsTrib == "S")
                        {
                            string serie = dto.SerieDesp6 == 0 && dto.NumDesp6 == 0
                                    ? dto.SerieDesp5.ToString().PadLeft(3, '0')
                                    : string.Concat(dto.CodigoControle, "-", dto.SerieDesp6.ToString().PadLeft(3, '0'));

                            string despacho = dto.SerieDesp6 == 0 && dto.NumDesp6 == 0
                                                ? dto.NumDesp5.ToString().PadLeft(3, '0')
                                                : dto.NumDesp6.ToString().PadLeft(3, '0');

                            string aux = string.Format("CT-e numero: {0} Fluxo: {1} S�rie: {2} Despacho: {3} <BR/>", dto.Cte, fluxo.Codigo, serie, despacho);
                            builder.Append(aux);
                        }
                    }

                    if (builder.Length > 0)
                    {
                        builder.Insert(0, "N�o � permitido realizar complemento de Cte de Substitui��o Tribut�ria para o(s) seguinte(s) CT-e(s):</BR>");
                        mensagem = builder.ToString();
                        return false;
                    }
                }

                int idCteComplementar = 0;

                if (dtos.Length > 0 && usuario != null)
                {
                    foreach (CteDto dto in dtos)
                    {
                        FluxoComercial fluxoComercial = _fluxoComercialRepository.ObterPorCodigo(dto.Fluxo);
                        CteTemp cteTemp = new CteTemp();
                        cteTemp.IdFluxoComercial = fluxoComercial.Id;
                        cteTemp.CodigoVagao = _cteRepository.ObterPorId(dto.CteId).Vagao.Codigo;
                        _cteTempRepository.InserirOuAtualizar(cteTemp);

                        _cteRepository.GerarCteComplementar(usuario, out idCteComplementar);
                        if (idCteComplementar != 0)
                        {
                            CteComplementado cteComplementado = null;
                            cteComplementado = new CteComplementado();
                            cteComplementado.Cte = _cteRepository.ObterPorId(dto.CteId);
                            cteComplementado.ValorDiferenca = dto.ValorDiferencaComplemento;
                            cteComplementado.DataHora = DateTime.Now;
                            cteComplementado.Usuario = usuario;
                            cteComplementado.CteComplementar = _cteRepository.ObterPorId(idCteComplementar);
                            cteComplementado.TipoComplemento = TipoOperacaoCteEnum.ComplementoIcms;
                            _cteComplementadoRepository.Inserir(cteComplementado);
                        }

                        _cteTempRepository.Remover(cteTemp);
                    }
                }
                else
                {
                    mensagem = "Cte Complementar n�o foi criado!";
                    return false;
                }
            }
            catch (Exception e)
            {
                mensagem = e.Message;
                return false;
            }

            mensagem = "A��o efetuada com Sucesso!";
            return true;
        }

        /// <summary>
        /// Realiza o salvamento dos complementos dos Ctes Selecionados
        /// </summary>
        /// <param name="dtos">dtos dos ctes</param>
        /// <param name="usuario">usuario logado</param>
        /// <param name="mensagem">Mensagem de status do salvamento</param>
        /// <returns>resultado do salvamento</returns>
        [Transaction]
        public virtual bool SalvarComplementoCtes(CteDto[] dtos, Usuario usuario, out string mensagem)
        {
            try
            {
                var estaHabilitadaValidacaoSubsTrib = _configuracaoTranslogicRepository.ObterPorId("CTE_HABILITAR_VERIFICACAO_SUBS_TRIB").Valor;

                // Valida os CT-es verificando se tem algum fluxo de subst. tribut�ria
                // caso sim n�o deixa realizar o complemento
                if (dtos.Length > 0 && usuario != null)
                {
                    StringBuilder builder = new StringBuilder();
                    foreach (CteDto dto in dtos)
                    {
                        FluxoComercial fluxo = _fluxoComercialRepository.ObterPorCodigo(dto.Fluxo);
                        if (fluxo.SubstituicaoTributaria != null && fluxo.SubstituicaoTributaria.Value && estaHabilitadaValidacaoSubsTrib == "S")
                        {
                            string serie = dto.SerieDesp6 == 0 && dto.NumDesp6 == 0
                                    ? dto.SerieDesp5.ToString().PadLeft(3, '0')
                                    : string.Concat(dto.CodigoControle, "-", dto.SerieDesp6.ToString().PadLeft(3, '0'));

                            string despacho = dto.SerieDesp6 == 0 && dto.NumDesp6 == 0
                                                ? dto.NumDesp5.ToString().PadLeft(3, '0')
                                                : dto.NumDesp6.ToString().PadLeft(3, '0');

                            string aux = string.Format("CT-e numero: {0} Fluxo: {1} S�rie: {2} Despacho: {3} <BR/>", dto.Cte, fluxo.Codigo, serie, despacho);
                            builder.Append(aux);
                        }
                    }

                    if (builder.Length > 0)
                    {
                        builder.Insert(0, "N�o � permitido realizar complemento de Cte de Substitui��o Tribut�ria para o(s) seguinte(s) CT-e(s):</BR>");
                        mensagem = builder.ToString();
                        return false;
                    }
                }

                int idCteComplementar = 0;

                if (dtos.Length > 0 && usuario != null)
                {
                    foreach (CteDto dto in dtos)
                    {
                        FluxoComercial fluxoComercial = _fluxoComercialRepository.ObterPorCodigo(dto.Fluxo);
                        CteTemp cteTemp = new CteTemp();
                        cteTemp.IdFluxoComercial = fluxoComercial.Id;
                        cteTemp.CodigoVagao = _cteRepository.ObterPorId(dto.CteId).Vagao.Codigo;
                        _cteTempRepository.InserirOuAtualizar(cteTemp);

                        _cteRepository.GerarCteComplementar(usuario, out idCteComplementar);
                        if (idCteComplementar != 0)
                        {
                            CteComplementado cteComplementado = null;
                            cteComplementado = new CteComplementado();
                            cteComplementado.Cte = _cteRepository.ObterPorId(dto.CteId);
                            cteComplementado.ValorDiferenca = dto.ValorDiferencaComplemento;
                            cteComplementado.DataHora = DateTime.Now;
                            cteComplementado.Usuario = usuario;
                            cteComplementado.TipoComplemento = TipoOperacaoCteEnum.Complemento;
                            cteComplementado.CteComplementar = _cteRepository.ObterPorId(idCteComplementar);

                            _cteComplementadoRepository.Inserir(cteComplementado);
                        }

                        _cteTempRepository.Remover(cteTemp);
                    }
                }
                else
                {
                    mensagem = "Cte Complementar n�o foi criado!";
                    return false;
                }
            }
            catch (Exception e)
            {
                mensagem = e.Message;
                return false;
            }

            mensagem = "A��o efetuada com Sucesso!";
            return true;

            /*
            try
            {
                int idCteComplementar = 0;

                if (dtos.Length > 0 && usuario != null)
                {
                    FluxoComercial fluxoComercial = _fluxoComercialRepository.ObterPorCodigo(dtos[0].Fluxo);
                    CteTemp cteTemp = new CteTemp();
                    cteTemp.IdFluxoComercial = fluxoComercial.Id;
                    cteTemp.CodigoVagao = _cteRepository.ObterPorId(dtos[0].CteId).Vagao.Codigo;
                    _cteTempRepository.InserirOuAtualizar(cteTemp);

                    _cteRepository.GerarCteComplementar(usuario, out idCteComplementar);
                }

                if (idCteComplementar != 0)
                {
                    CteComplementado cteComplementado = null;

                    foreach (CteDto dto in dtos)
                    {
                        cteComplementado = new CteComplementado();
                        cteComplementado.Cte = _cteRepository.ObterPorId(dto.CteId);
                        cteComplementado.ValorDiferenca = dto.ValorDiferencaComplemento;
                        cteComplementado.DataHora = DateTime.Now;
                        cteComplementado.Usuario = usuario;
                        cteComplementado.CteComplementar = _cteRepository.ObterPorId(idCteComplementar);

                        _cteComplementadoRepository.Inserir(cteComplementado);
                    }
                }
                else
                {
                    mensagem = "Cte Complementar n�o foi criado!";
                    return false;
                }
            }
            catch (Exception e)
            {
                mensagem = e.Message;
                return false;
            }

            mensagem = "A��o efetuada com Sucesso!";
            return true;
             */
        }

        /// <summary>
        /// Realiza o salvamento das anula��es dos Ctes Selecionados
        /// </summary>
        /// <param name="dtos">dtos dos ctes</param>
        /// <param name="usuario">usuario logado</param>
        /// <param name="mensagem">Mensagem de status do salvamento</param>
        /// <returns>resultado do salvamento</returns>
        [Transaction]
        public virtual bool SalvarAnulacaoCtes(CteDto[] dtos, Usuario usuario, out string mensagem)
        {
            try
            {
                int idCteAnulado = 0;

                if (dtos.Length > 0 && usuario != null)
                {
                    foreach (CteDto dto in dtos)
                    {
                        Cte cte = _cteRepository.ObterPorChave(dto.Chave);
                        FluxoComercial fluxoComercial = _fluxoComercialRepository.ObterPorId(cte.FluxoComercial.Id);

                        CteTemp cteTemp = new CteTemp();
                        ////cteTemp.Id = cte.Vagao.Id ?? 0;
                        cteTemp.IdFluxoComercial = fluxoComercial.Id;
                        cteTemp.IdCteOrigem = cte.Id;
                        cteTemp.CodigoVagao = cte.Vagao.Codigo;

                        _cteTempRepository.InserirOuAtualizar(cteTemp);
                        _cteRepository.GerarCteAnulacao(usuario, out idCteAnulado);

                        if (idCteAnulado != 0)
                        {
                            CteAnulado cteAnulado = null;
                            cteAnulado = new CteAnulado();
                            cteAnulado.Cte = cte;
                            cteAnulado.DataHora = DateTime.Now;
                            cteAnulado.Usuario = usuario;
                            cteAnulado.TipoAnulacao = dto.IndIeToma == 1 ? "AC" : "AN";
                            cteAnulado.CteAnulacao = _cteRepository.ObterPorId(idCteAnulado);

                            _cteAnuladoRepository.Inserir(cteAnulado);
                        }

                        _cteTempRepository.Remover(cteTemp);
                    }
                }
                else
                {
                    mensagem = "Cte de anula��o n�o foi criado!";
                    return false;
                }
            }
            catch (Exception e)
            {
                mensagem = e.Message;
                return false;
            }

            mensagem = "A��o efetuada com Sucesso!";
            return true;
        }


        /// <summary>
        /// Realiza o salvamento dos complementos Take or Pay dos Ctes Selecionados
        /// </summary>
        /// <param name="dtos">dtos dos ctes</param>
        /// <param name="usuario">usuario logado</param>
        /// <param name="mensagem">mensagem de status do salvamento</param>
        /// <returns>resultado do salvamento</returns>
        [Transaction]
        public virtual bool SalvarComplementoTakeOrPayCtes(CteDto[] dtos, Usuario usuario, out string mensagem)
        {
            try
            {
                if (dtos.Length > 0)
                {
                    double valorTotal = dtos[0].ValorTotal;
                    string codigoFluxoReferencia = dtos[0].FluxoReferencia.ToUpperInvariant();

                    // FluxoComercial fluxoComercialReferencia = _fluxoComercialRepository.ObterPorCodigo(codigoFluxoReferencia);
                    FluxoComercial fluxoComercialReferencia = _fluxoComercialRepository.ObterFerroviarioPorCodigo(codigoFluxoReferencia);

                    if (fluxoComercialReferencia == null)
                    {
                        mensagem = "Fluxo Refer�ncia n�o encontrado!";
                        return false;
                    }

                    Mercadoria mercadoriaReferencia = fluxoComercialReferencia.Mercadoria;

                    EstacaoMae estacaoMaeOrigemReferencia = fluxoComercialReferencia.Origem;
                    EstacaoMae estacaoMaeDestinoReferencia = fluxoComercialReferencia.Destino;

                    EmpresaCliente empresaClienteRemetenteReferencia = fluxoComercialReferencia.EmpresaRemetente;
                    EmpresaCliente empresaClienteDestinatariaReferencia = fluxoComercialReferencia.EmpresaDestinataria;
                    EmpresaCliente empresaClientePagadoraReferencia = fluxoComercialReferencia.EmpresaPagadora;

                    foreach (CteDto dto in dtos)
                    {
                        Cte cte = _cteRepository.ObterPorId(dto.CteId);
                        FluxoComercial fluxoComercial = cte.FluxoComercial;

                        Mercadoria mercadoria = fluxoComercial.Mercadoria;

                        EstacaoMae estacaoMaeOrigem = fluxoComercial.Origem;
                        EstacaoMae estacaoMaeDestino = fluxoComercial.Destino;

                        EmpresaCliente empresaClienteRemetente = fluxoComercial.EmpresaRemetente;
                        EmpresaCliente empresaClienteDestinataria = fluxoComercial.EmpresaDestinataria;
                        EmpresaCliente empresaClientePagadora = fluxoComercial.EmpresaPagadora;

                        // Verifica se o fluxo referencia tem mesma Origem, mesmo Destino, mesma Mercadoria
                        // tamb�m os clientes devem ser igual, sendo cliente remetente, destinatario e correntista;
                        /*    if (mercadoriaReferencia.Id != mercadoria.Id)
{
mensagem = "Mercadoria do Cte: " + cte.Numero + " est� diferente ao do Fluxo Refer�ncia!";
return false;
}

if (estacaoMaeOrigemReferencia.Codigo != estacaoMaeOrigem.Codigo)
{
mensagem = "Origem do Cte: " + cte.Numero + " est� diferente ao do Fluxo Refer�ncia!";
return false;
}
if (estacaoMaeDestinoReferencia.Codigo != estacaoMaeDestino.Codigo)
{
mensagem = "Destino do Cte: " + cte.Numero + " est� diferente ao do Fluxo Refer�ncia!";
return false;
}

if (empresaClienteRemetenteReferencia.Id != empresaClienteRemetente.Id)
{
mensagem = "Empresa Remetente do Cte: " + cte.Numero + " est� diferente ao do Fluxo Refer�ncia!";
return false;
}

if (empresaClienteDestinatariaReferencia.Id != empresaClienteDestinataria.Id)
{
mensagem = "Empresa Destinat�ria do Cte: " + cte.Numero + " est� diferente ao do Fluxo Refer�ncia!";
return false;
}

if (empresaClientePagadoraReferencia.Id != empresaClientePagadora.Id)
{
mensagem = "Empresa Pagadora do Cte: " + cte.Numero + " est� diferente ao do Fluxo Refer�ncia!";
return false;
} */
                    }

                    // Divide o valor total entre os ctes
                    double valorRateado = valorTotal / dtos.Length;

                    int idCteComplementar = 0;

                    CteTemp cteTemp = new CteTemp();
                    // cteTemp.IdFluxoComercial = _fluxoComercialRepository.ObterPorCodigo(codigoFluxoReferencia).Id;
                    cteTemp.IdFluxoComercial = _fluxoComercialRepository.ObterFerroviarioPorCodigo(codigoFluxoReferencia).Id;
                    cteTemp.CodigoVagao = _cteRepository.ObterPorId(dtos[0].CteId).Vagao.Codigo;
                    _cteTempRepository.InserirOuAtualizar(cteTemp);
                    _cteRepository.GerarCteComplementar(usuario, out idCteComplementar);

                    if (idCteComplementar != 0)
                    {
                        CteComplementado cteComplementado = null;

                        foreach (CteDto dto in dtos)
                        {
                            cteComplementado = new CteComplementado();
                            cteComplementado.Cte = _cteRepository.ObterPorId(dto.CteId);
                            cteComplementado.ValorDiferenca = valorRateado;
                            cteComplementado.DataHora = DateTime.Now;
                            cteComplementado.Usuario = usuario;
                            cteComplementado.TipoComplemento = TipoOperacaoCteEnum.Complemento;
                            cteComplementado.CteComplementar = _cteRepository.ObterPorId(idCteComplementar);

                            _cteComplementadoRepository.InserirOuAtualizar(cteComplementado);
                        }
                    }
                    else
                    {
                        mensagem = "Cte Complementar n�o foi criado!";
                        return false;
                    }
                }
            }
            catch (Exception e)
            {
                mensagem = e.Message;
                return false;
            }

            mensagem = "A��o efetuada com Sucesso!";
            return true;
        }

        /// <summary>
        /// Realiza a reemiss�o do cte
        /// </summary>
        /// <param name="listaCte">lista de Id do cte a ser emitido</param>
        /// <param name="usuario"> Usuario que fez a manutencao</param>
        public virtual void SalvarLoteManutencaoCte(List<int> listaCte, Usuario usuario)
        {
            foreach (int idCte in listaCte)
            {
                SalvarManutencaoCte(idCte, usuario);
            }
        }

        /// <summary>
        /// Realiza a reemiss�o do cte
        /// </summary>
        /// <param name="idCte">Id do cte a ser emitido</param>
        /// <param name="usuario"> Usuario que fez a manutencao</param>
        [Transaction]
        public virtual void SalvarManutencaoCte(int idCte, Usuario usuario)
        {
            Cte cte = _cteRepository.ObterPorId(idCte);
            string hostName = Dns.GetHostName();

            try
            {
                // Insere no log do cte
                _cteLogService.InserirLogInfo(cte, "CteService", string.Format("Gravando cte {0} nas tabelas tempor�rias. - HostName:{1} - IdUsuario:{2}", cte.Id, hostName, usuario.Id));

                if (cte.Despacho.DataCancelamento.HasValue)
                {
                    throw new Exception("N�o pode ser realizada manuten��o desse CT-e pois o despacho est� cancelado.");
                }

                // Caso o Cte seja EAR, ent�o altera a situa��o para INV.
                AtualizarStatusCteManutInvalida(idCte);

                CteTemp cteTemp = new CteTemp();
                int index = 0;

                cteTemp.Id = cte.Vagao.Id.HasValue ? cte.Vagao.Id.Value : 0;
                cteTemp.CodigoVagao = cte.Vagao.Codigo;
                cteTemp.IdCteOrigem = idCte;
                cteTemp.IdFluxoComercial = cte.FluxoComercial.Id;
                cteTemp.IdDespacho = cte.Despacho.Id.HasValue ? cte.Despacho.Id.Value : 0;
                cteTemp.PesoVagao = cte.PesoVagao.HasValue ? cte.PesoVagao.Value : 0;

                cteTemp.VolumeVagao = cte.VolumeVagao;

                _cteTempRepository.Inserir(cteTemp);
                IList<CteDetalhe> listaDetalhes = _cteDetalheRepository.ObterPorCte(cte);

                // Regra para validar se possui nota fiscal duplicada.
                // � poss�vel usar a mesma Nota Fiscal 2 ou mais vezes no vag�o apenas em casos de conteiners diferentes.
                foreach (CteDetalhe detalhe in listaDetalhes)
                {
                    List<CteDetalhe> retorno = listaDetalhes.Where(c => c.NumeroNota == detalhe.NumeroNota).ToList();

                    if (retorno.Count > 1)
                    {
                        foreach (CteDetalhe cteDetalhe in retorno)
                        {
                            List<CteDetalhe> countConteiner = retorno.Where(c => c.ConteinerNotaFiscal == cteDetalhe.ConteinerNotaFiscal).ToList();

                            if (countConteiner.Count > 1)
                            {
                                throw new Exception("� poss�vel usar a mesma Nota Fiscal 2 ou mais vezes no vag�o apenas em casos de conteiners diferentes.");
                            }
                        }
                    }
                }

                _cteLogService.InserirLogInfo(cte, "CteService", string.Format("Gravando {0} Detalhes na CteTempDetalhe do cte {1} - HostName:{2} - IdUsuario:{3} ", listaDetalhes.Count, cte.Id, hostName, usuario.Id));

                foreach (CteDetalhe detalhe in listaDetalhes)
                {
                    CteTempDetalhe cteTempDetalhe = new CteTempDetalhe();
                    cteTempDetalhe.Id = (++index).ToString();
                    cteTempDetalhe.CodigoNfe = string.Concat(detalhe.NumeroNota, detalhe.SerieNota);
                    cteTempDetalhe.PesoNotaFiscal = detalhe.PesoNotaFiscal;
                    cteTempDetalhe.ValorNotaFiscal = detalhe.ValorNotaFiscal;
                    cteTempDetalhe.PesoTotalNotaFiscal = detalhe.PesoTotal;
                    cteTempDetalhe.TipoMoeda = "BRL";
                    cteTempDetalhe.ValorTotalNotaFiscal = detalhe.ValorTotalNotaFiscal;

                    cteTempDetalhe.ChaveNfe = detalhe.ChaveNfe;
                    cteTempDetalhe.CnpjDestinatario = detalhe.CgcDestinatario;
                    cteTempDetalhe.CnpjRemetente = detalhe.CgcRemetente;
                    cteTempDetalhe.ConteinerNotaFiscal = detalhe.ConteinerNotaFiscal;
                    cteTempDetalhe.DataCadastro = detalhe.DataCadastro;
                    cteTempDetalhe.DataNotaFiscal = detalhe.DataNotaFiscal;
                    cteTempDetalhe.IdEmpresaDestinatario = detalhe.IdDestinatario;
                    cteTempDetalhe.IdEmpresaRemetente = detalhe.IdRementente;
                    cteTempDetalhe.NumeroNotaFiscal = detalhe.NumeroNota;
                    cteTempDetalhe.IdVagao = cte.Vagao.Id.Value;
                    cteTempDetalhe.InscricaoEstadualDestinatario = detalhe.InsEstadualDestinatario;
                    cteTempDetalhe.InscricaoEstadualRemetente = detalhe.InsEstadualRemetente;
                    cteTempDetalhe.NumeroTif = detalhe.NumeroTif;
                    cteTempDetalhe.SerieNotaFiscal = detalhe.SerieNota;
                    cteTempDetalhe.SiglaDestinatario = detalhe.UfDestinatario;
                    cteTempDetalhe.SiglaRemetente = detalhe.UfRemetente;
                    cteTempDetalhe.UfDestinatario = detalhe.UfDestinatario;
                    cteTempDetalhe.UfRemetente = detalhe.UfRemetente;
                    cteTempDetalhe.VolumeNotaFiscal = detalhe.VolumeNotaFiscal;

                    _cteTempDetalheRepository.Inserir(cteTempDetalhe);
                }

                _cteTempRepository.GerarCteManutencaoPorTemporarias(usuario);

                // limpa a tabela temporaria
                _cteTempDetalheRepository.RemoverTodos();
                _cteTempRepository.RemoverTodos();
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(cte, "CteService", string.Format("Erro na gera��o do cte: {0} - Erro: {1} - HostName:{2} - IdUsuario:{3} ", cte.Id, ex.Message, hostName, usuario.Id), ex);
                throw;
            }
        }

        /// <summary>
        /// Realiza o salvamento do status dos Ctes em manuten��o
        /// </summary>
        /// <param name="dto">dtos do cte a ser feito manutencao</param>
        /// <param name="usuario">usuario logado</param>
        [Transaction]
        public virtual void SalvarManutencaoCte(ManutencaoCteTempDto dto, Usuario usuario)
        {
            try
            {
                int index = 0;

                /*
                 bool existeAlteracao = VerificarExisteAlteracaoParaManutencao(dto);
                if (!existeAlteracao)
                {
                    throw new TranslogicException("N�o foram encontradas altera��es no CT-e.");
                }

                // Caso o Cte seja EAR, ent�o altera a situa��o para INV.
                AtualizarStatusCteManutInvalida(dto.CteTemp.IdCteOrigem.Value);
                */
                bool isCteSubstituto = false;
                //CteArquivo cteArquivo = _cteArquivoRepository.ObterPorId(dto.CteTemp.IdCteOrigem.Value);

                /* colocar if para verificar se � menos de 90 dias para fazer os IFs abaixo...se for maior � normal direto */
                //if (cteArquivo != null && dto.CteTemp.IdCteOrigem != null)
                if (dto.CteTemp.IdCteOrigem != null)
                {
                    Cte cte = ObterCtePorId(dto.CteTemp.IdCteOrigem.Value);
                    FluxoComercial fluxoComercial = ObterFluxoComercialPorId(dto.CteTemp.IdFluxoComercial);

                    if (!ValidarSeOcorreuAlteracaoDeExpedidor(cte, fluxoComercial))
                    {
                        if (!ValidarSeOcorreuAlteracaoDeRecebedor(cte, fluxoComercial))
                        {
                            if (!ValidarSeOcorreuAlteracaoDeRemetente(cte, fluxoComercial))
                            {
                                if (!ValidarSeOcorreuAlteracaoDeTomador(cte, fluxoComercial))
                                {
                                    if (!ValidarSeOcorreuAlteracaoDeDestinatario(cte, fluxoComercial))
                                    {
                                        if (ValidarSeOcorreuAlteracaoDaNota(dto, cte))
                                        {
                                            isCteSubstituto = true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                CteTemp cteTemp = dto.CteTemp;

                if (isCteSubstituto)
                {
                    cteTemp.TipoCte = TipoCteEnum.Substituto;
                }
                else
                {
                    cteTemp.TipoCte = TipoCteEnum.Normal;
                }

                //// Grava a informa��o de altera��o do tomador 
                cteTemp.IndAltToma = dto.CteTemp.IndAltToma;

                Cte _cte = ObterCtePorId(dto.CteTemp.IdCteOrigem.Value);

                _cteTempRepository.Inserir(cteTemp);

                if (dto.ListaDetalhes == null)
                {
                    IList<CteDetalhe> listaDetalhes = _cteDetalheRepository.ObterPorCte(_cte);

                    foreach (CteDetalhe detalhe in listaDetalhes)
                    {
                        CteTempDetalhe cteTempDetalhe = new CteTempDetalhe();
                        cteTempDetalhe.Id = (++index).ToString();
                        cteTempDetalhe.CodigoNfe = string.Concat(detalhe.NumeroNota, detalhe.SerieNota);
                        cteTempDetalhe.PesoNotaFiscal = detalhe.PesoNotaFiscal;
                        cteTempDetalhe.ValorNotaFiscal = detalhe.ValorNotaFiscal;
                        cteTempDetalhe.PesoTotalNotaFiscal = detalhe.PesoTotal;
                        cteTempDetalhe.TipoMoeda = "BRL";
                        cteTempDetalhe.ValorTotalNotaFiscal = detalhe.ValorTotalNotaFiscal;

                        cteTempDetalhe.ChaveNfe = detalhe.ChaveNfe;
                        cteTempDetalhe.CnpjDestinatario = detalhe.CgcDestinatario;
                        cteTempDetalhe.CnpjRemetente = detalhe.CgcRemetente;
                        cteTempDetalhe.ConteinerNotaFiscal = detalhe.ConteinerNotaFiscal;
                        cteTempDetalhe.DataCadastro = detalhe.DataCadastro != null ? detalhe.DataCadastro : DateTime.Now;
                        cteTempDetalhe.DataNotaFiscal = detalhe.DataNotaFiscal;
                        cteTempDetalhe.IdEmpresaDestinatario = detalhe.IdDestinatario;
                        cteTempDetalhe.IdEmpresaRemetente = detalhe.IdRementente;
                        cteTempDetalhe.NumeroNotaFiscal = detalhe.NumeroNota;
                        cteTempDetalhe.IdVagao = detalhe.Cte.Vagao.Id.HasValue ? detalhe.Cte.Vagao.Id.Value : int.Parse(detalhe.Cte.Numero);
                        cteTempDetalhe.InscricaoEstadualDestinatario = detalhe.InsEstadualDestinatario;
                        cteTempDetalhe.InscricaoEstadualRemetente = detalhe.InsEstadualRemetente;
                        cteTempDetalhe.NumeroTif = detalhe.NumeroTif;
                        cteTempDetalhe.SerieNotaFiscal = detalhe.SerieNota;
                        cteTempDetalhe.SiglaDestinatario = detalhe.UfDestinatario;
                        cteTempDetalhe.SiglaRemetente = detalhe.UfRemetente;
                        cteTempDetalhe.UfDestinatario = detalhe.UfDestinatario;
                        cteTempDetalhe.UfRemetente = detalhe.UfRemetente;
                        cteTempDetalhe.VolumeNotaFiscal = detalhe.VolumeNotaFiscal;

                        if (!string.IsNullOrEmpty(dto.NovaChaveCte))
                        {
                            cteTempDetalhe.ChaveCte = dto.NovaChaveCte;
                        }

                        _cteTempDetalheRepository.Inserir(cteTempDetalhe);
                    }
                }
                else
                {
                    foreach (CteTempDetalheViewModel detalhe in dto.ListaDetalhes)
                    {
                        CteTempDetalhe cteTempDetalhe = new CteTempDetalhe();
                        cteTempDetalhe.Id = (++index).ToString();
                        cteTempDetalhe.CodigoNfe = string.Concat(detalhe.NumeroNotaFiscal, detalhe.SerieNotaFiscal);
                        cteTempDetalhe.PesoNotaFiscal = detalhe.PesoNotaFiscal ?? null;
                        cteTempDetalhe.ValorNotaFiscal = detalhe.ValorNotaFiscal ?? null;
                        cteTempDetalhe.PesoTotalNotaFiscal = detalhe.PesoNotaFiscal ?? 0;
                        cteTempDetalhe.TipoMoeda = "BRL";
                        cteTempDetalhe.ValorTotalNotaFiscal = detalhe.ValorTotalNotaFiscal;

                        cteTempDetalhe.ChaveNfe = detalhe.ChaveNfe;
                        cteTempDetalhe.CnpjDestinatario = detalhe.CnpjDestinatario;
                        cteTempDetalhe.CnpjRemetente = detalhe.CnpjRemetente;
                        cteTempDetalhe.ConteinerNotaFiscal = detalhe.ConteinerNotaFiscal;
                        cteTempDetalhe.DataCadastro = detalhe.DataCadastro.HasValue ? detalhe.DataCadastro.Value : DateTime.Now;
                        cteTempDetalhe.DataNotaFiscal = detalhe.DataNotaFiscal;
                        cteTempDetalhe.IdEmpresaDestinatario = detalhe.IdEmpresaDestinatario ?? null;
                        cteTempDetalhe.IdEmpresaRemetente = detalhe.IdEmpresaRemetente ?? null;
                        cteTempDetalhe.NumeroNotaFiscal = detalhe.NumeroNotaFiscal;
                        cteTempDetalhe.IdVagao = detalhe.IdVagao;
                        cteTempDetalhe.InscricaoEstadualDestinatario = detalhe.InscricaoEstadualDestinatario;
                        cteTempDetalhe.InscricaoEstadualRemetente = detalhe.InscricaoEstadualRemetente;
                        cteTempDetalhe.NumeroTif = detalhe.NumeroTif ?? "";
                        cteTempDetalhe.SerieNotaFiscal = detalhe.SerieNotaFiscal;
                        cteTempDetalhe.SiglaDestinatario = detalhe.UfDestinatario;
                        cteTempDetalhe.SiglaRemetente = detalhe.UfRemetente;
                        cteTempDetalhe.UfDestinatario = detalhe.UfDestinatario;
                        cteTempDetalhe.UfRemetente = detalhe.UfRemetente;
                        cteTempDetalhe.VolumeNotaFiscal = detalhe.VolumeNotaFiscal ?? null;

                        if (!string.IsNullOrEmpty(dto.NovaChaveCte))
                        {
                            cteTempDetalhe.ChaveCte = dto.NovaChaveCte;
                        }

                        _cteTempDetalheRepository.Inserir(cteTempDetalhe);
                    }
                }

                _cteTempRepository.GerarCteManutencaoPorTemporarias(usuario);

                /* Limpa a temp para n�o afetar o processo em Lote */
                _cteTempDetalheRepository.RemoverTodos();
                _cteTempRepository.RemoverTodos();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Realiza o salvamento do status dos Ctes em manuten��o
        /// </summary>
        /// <param name="dto">dtos do cte a ser feito manutencao</param>
        /// <param name="usuario">usuario logado</param>
        [Transaction]
        public virtual void SalvarCteVirtual(ManutencaoCteTempDto dto, Usuario usuario)
        {
            try
            {
                int index = 0;

                CteTemp cteTemp = dto.CteTemp;
                dto.CteTemp.TipoCte = TipoCteEnum.Virtual;
                _cteTempRepository.Inserir(cteTemp);

                foreach (CteTempDetalheViewModel cteTempDetalheViewModel in dto.ListaDetalhes)
                {
                    CteTempDetalhe cteTempDetalhe = new CteTempDetalhe();
                    cteTempDetalhe.Id = (++index).ToString();
                    cteTempDetalhe.CodigoNfe = string.Concat(cteTempDetalheViewModel.NumeroNotaFiscal, cteTempDetalheViewModel.SerieNotaFiscal);
                    cteTempDetalhe.PesoNotaFiscal = cteTempDetalheViewModel.PesoNotaFiscal;
                    cteTempDetalhe.ValorNotaFiscal = cteTempDetalheViewModel.ValorNotaFiscal;
                    cteTempDetalhe.PesoTotalNotaFiscal = cteTempDetalheViewModel.PesoTotalNotaFiscal;
                    cteTempDetalhe.TipoMoeda = "BRL";
                    cteTempDetalhe.ValorTotalNotaFiscal = cteTempDetalheViewModel.ValorTotalNotaFiscal;

                    cteTempDetalhe.ChaveNfe = cteTempDetalheViewModel.ChaveNfe;
                    cteTempDetalhe.CnpjDestinatario = cteTempDetalheViewModel.CnpjDestinatario;
                    cteTempDetalhe.CnpjRemetente = cteTempDetalheViewModel.CnpjRemetente;
                    cteTempDetalhe.ConteinerNotaFiscal = cteTempDetalheViewModel.ConteinerNotaFiscal;
                    cteTempDetalhe.DataCadastro = cteTempDetalheViewModel.DataCadastro ?? DateTime.Now;
                    cteTempDetalhe.DataNotaFiscal = cteTempDetalheViewModel.DataNotaFiscal;
                    cteTempDetalhe.IdEmpresaDestinatario = cteTempDetalheViewModel.IdEmpresaDestinatario;
                    cteTempDetalhe.IdEmpresaRemetente = cteTempDetalheViewModel.IdEmpresaRemetente;
                    cteTempDetalhe.NumeroNotaFiscal = cteTempDetalheViewModel.NumeroNotaFiscal;
                    cteTempDetalhe.IdVagao = cteTempDetalheViewModel.IdVagao;
                    cteTempDetalhe.InscricaoEstadualDestinatario = cteTempDetalheViewModel.InscricaoEstadualDestinatario;
                    cteTempDetalhe.InscricaoEstadualRemetente = cteTempDetalheViewModel.InscricaoEstadualRemetente;
                    cteTempDetalhe.NumeroTif = cteTempDetalheViewModel.NumeroTif;
                    cteTempDetalhe.SerieNotaFiscal = cteTempDetalheViewModel.SerieNotaFiscal;
                    cteTempDetalhe.SiglaDestinatario = cteTempDetalheViewModel.SiglaDestinatario;
                    cteTempDetalhe.SiglaRemetente = cteTempDetalheViewModel.SiglaRemetente;
                    cteTempDetalhe.UfDestinatario = cteTempDetalheViewModel.UfDestinatario;
                    cteTempDetalhe.UfRemetente = cteTempDetalheViewModel.UfRemetente;
                    cteTempDetalhe.VolumeNotaFiscal = cteTempDetalheViewModel.VolumeNotaFiscal;

                    _cteTempDetalheRepository.Inserir(cteTempDetalhe);
                }

                _cteTempRepository.GerarCteManutencaoPorTemporarias(usuario);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Retorna todos os Cte Autorizados para listagem de Impress�o
        /// </summary>
        /// <param name="dataInicial">Data inicial de pesquisa</param>
        /// <param name="dataFinal">Data final de pesquisa</param>
        /// <param name="serie">N�mero da s�rie</param>
        /// <param name="numDespacho">N�mero do Despacho</param>
        /// <param name="chave">chave do cte</param>
        /// <param name="codigoEstacaoOrigem">Esta��o de Origem</param>
        /// <param name="numeroCte">n�mero do cte</param>
        /// <param name="fluxo">N�mero do Fluxo</param>
        /// <param name="impresso">op��o de j� impresso ou n�o</param>
        /// <returns>lista de Cte</returns>
        public IList<CteDto> RetornaCteImpressao(DateTime dataInicial, DateTime dataFinal, int serie, int numDespacho, string chave, string codigoEstacaoOrigem, int numeroCte, string fluxo, bool impresso)
        {
            DateTime dataIni = new DateTime(dataInicial.Year, dataInicial.Month, dataInicial.Day);

            DateTime dataFim = new DateTime(dataFinal.Year, dataFinal.Month, dataFinal.Day, 23, 59, 59);

            IList<CteDto> listaDto = new List<CteDto>();

            CteDto dto = null;

            DespachoTranslogic despacho = null;
            ItemDespacho itemDespacho = null;
            DetalheCarregamento detalheCarregamento = null;
            Carregamento carregamento = null;
            FluxoComercial fluxoComercial = null;
            EstacaoMae estacaoMaeOrigem = null;

            IList<Cte> listaCte = _cteRepository.Obter(dataIni, dataFim, serie, numDespacho, chave, codigoEstacaoOrigem, numeroCte, fluxo, impresso);

            foreach (Cte cte in listaCte)
            {
                IList<CteArvore> listaArvore = _cteArvoreRepository.ObterArvorePorCteFilho(cte);

                CteArvore cteArvore = listaArvore.OrderByDescending(c => c.Id).FirstOrDefault();

                if (cteArvore == null)
                {
                    continue;
                }

                IList<CteAgrupamento> listaCteAgrupamento = _cteAgrupamentoRepository.ObterAgrupamentoPorCteFilho(cteArvore.CteFilho);

                foreach (CteAgrupamento cteAgrupamento in listaCteAgrupamento)
                {
                    Cte cteTmp = cteAgrupamento.CteFilho;

                    if (ContainCteDto(listaDto.ToList(), typeof(IList<CteDto>), (int)cteTmp.Id))
                    {
                        continue;
                    }

                    despacho = cteTmp.Despacho;

                    // para ser listado tem que ter despacho e ser autorizado no SEFAZ
                    if (despacho != null && cteTmp.SituacaoAtual == SituacaoCteEnum.Autorizado)
                    {
                        itemDespacho = _itemDespachoRepository.ObterItemDespachoPorDespacho(despacho);

                        if (itemDespacho != null)
                        {
                            detalheCarregamento = itemDespacho.DetalheCarregamento;

                            if (detalheCarregamento != null)
                            {
                                carregamento = detalheCarregamento.Carregamento;

                                if (carregamento != null)
                                {
                                    fluxoComercial = carregamento.FluxoComercialCarregamento;
                                }
                            }
                        }

                        if (fluxoComercial != null)
                        {
                            estacaoMaeOrigem = fluxoComercial.Origem;

                            dto = new CteDto();
                            dto.CteId = cteTmp.Id ?? 0;
                            dto.Fluxo = fluxoComercial.Codigo;
                            dto.Origem = estacaoMaeOrigem != null ? estacaoMaeOrigem.Codigo : string.Empty;
                            dto.Cte = cteTmp.Serie + "-" + cteTmp.Numero;
                            dto.Chave = cteTmp.Chave;
                            dto.SerieDesp5 = despacho.SerieDespacho != null ? despacho.SerieDespacho.SerieDespachoNum : string.Empty;
                            dto.NumDesp5 = despacho.NumeroDespacho ?? 0;
                            dto.SerieDesp6 = despacho.SerieDespachoUf != null ? despacho.SerieDespachoUf.NumeroSerieDespacho : 0;
                            dto.NumDesp6 = despacho.NumeroDespacho ?? 0;
                            dto.DataEmissao = cteTmp.DataHora;
                            dto.Impresso = cteTmp.Impresso;

                            listaDto.Add(dto);
                        }
                    }
                }
            }

            return listaDto;
        }

        /// <summary>
        /// Retorna todos os Cte Cancelados ou invalidados para realizar manuten��o
        /// </summary>
        /// <param name="pagination"> Paginacao pesquisa</param>
        /// <param name="filtros"> Filtros para pesquisa</param>
        /// <returns>lista de Cte</returns>
        public ResultadoPaginado<ManutencaoCteDto> RetornaListaCteManutencao(DetalhesPaginacaoWeb pagination, DetalhesFiltro<object>[] filtros)
        {
            DateTime dataIni = DateTime.Now;
            DateTime dataFim = DateTime.Now;
            string serie = string.Empty;
            int numDespacho = 0;
            string erro = string.Empty;
            string fluxo = string.Empty;
            string origem = string.Empty;
            string destino = string.Empty;
            string numVagao = string.Empty;
            string chaveCte = string.Empty;
            string codigoUfDcl = string.Empty;

            if (filtros != null)
            {
                foreach (var detalheFiltro in filtros)
                {
                    if (detalheFiltro.Campo.Equals("dataInicial") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        string texto = detalheFiltro.Valor[0].ToString();

                        dataIni = DateTime.ParseExact(texto, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                    }

                    if (detalheFiltro.Campo.Equals("dataFinal") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        string texto = detalheFiltro.Valor[0].ToString();

                        dataFim = DateTime.ParseExact(texto, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);

                        dataFim = dataFim.AddDays(1).AddSeconds(-1);
                    }

                    if (detalheFiltro.Campo.Equals("serie") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        serie = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
                    }

                    if (detalheFiltro.Campo.Equals("despacho") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        numDespacho = Convert.ToInt32(detalheFiltro.Valor[0].ToString());
                    }

                    if (detalheFiltro.Campo.Equals("UfDcl") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        codigoUfDcl = detalheFiltro.Valor[0].ToString().ToUpperInvariant().Trim();
                    }

                    if (detalheFiltro.Campo.Equals("fluxo") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        fluxo = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
                    }

                    if (detalheFiltro.Campo.Equals("Origem") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        origem = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
                    }

                    if (detalheFiltro.Campo.Equals("Destino") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        destino = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
                    }

                    if (detalheFiltro.Campo.Equals("chave") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        chaveCte = detalheFiltro.Valor[0].ToString();
                    }

                    if (detalheFiltro.Campo.Equals("errocte") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        erro = detalheFiltro.Valor[0].ToString();
                    }

                    if (detalheFiltro.Campo.Equals("Vagao") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        numVagao = detalheFiltro.Valor[0].ToString();
                    }
                }
            }

            ResultadoPaginado<ManutencaoCteDto> listaDto = new ResultadoPaginado<ManutencaoCteDto>();

            listaDto = _cteRepository.ObterCteManutencao(pagination, dataIni, dataFim, serie, numDespacho, fluxo, chaveCte, erro, origem, destino, numVagao, codigoUfDcl);

            /*
            DateTime dataIni = new DateTime(dataInicial.Year, dataInicial.Month, dataInicial.Day);

            DateTime dataFim = new DateTime(dataFinal.Year, dataFinal.Month, dataFinal.Day, 23, 59, 59);

            IList<ManutencaoCteDto> listaManutencaoDto = new List<ManutencaoCteDto>();

            ManutencaoCteDto manutencaoDto = null;

            DespachoTranslogic despacho = null;
            FluxoComercial fluxoComercial = null;
            Mercadoria mercadoria = null;
            EstacaoMae estacaoMaeOrigem = null;
            EstacaoMae estacaoMaeDestino = null;
            SerieDespacho serieDespacho = null;
            SerieDespachoUf serieDespachoUf = null;
            Vagao vagao = null;
            */

            /*// Lista os Ctes Cancelados ou os Invalidados
            SituacaoCteEnum situacaoCte = SituacaoCteEnum.Cancelado;
            if (status == StatusCteEnum.Inativo)
            {
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            situacaoCte = SituacaoCteEnum.Invalidado;
            }*/
            /*

            IList<Cte> listaCte = _cteRepository.Obter(dataIni, dataFim, serie, numDespacho, codVagao, chave, serieCte, nroCte);

            foreach (Cte cte in listaCte)
            {
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            bool cteComAgrupamentoNaoCancelado = false;

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            IList<CteArvore> listaArvore = _cteArvoreRepository.ObterArvorePorCteFilho(cte);

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            CteArvore cteArvore = listaArvore.OrderByDescending(c => c.Id).FirstOrDefault();

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            if (cteArvore == null)
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            {
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            continue;
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            }

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            IList<CteAgrupamento> listaCteAgrupamento = _cteAgrupamentoRepository.ObterAgrupamentoPorCteFilho(cteArvore.CteFilho);

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            foreach (CteAgrupamento cteAgrupamento in listaCteAgrupamento)
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            {
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            if (cteAgrupamento.CteFilho.SituacaoAtual != SituacaoCteEnum.Cancelado)
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            {
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            cteComAgrupamentoNaoCancelado = true;
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            }
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            }

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            Cte cteRaiz = _cteAgrupamentoRepository.ObterCteRaizPorCteFilho(cteArvore.CteFilho);

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            if (ContainCteDto(listaManutencaoDto.ToList(), typeof(IList<ManutencaoCteDto>), (int)cteRaiz.Id))
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            {
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            continue;
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            }

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            despacho = cteRaiz.Despacho;
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            vagao = cteRaiz.Vagao;

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            if (despacho != null)
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            {
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            serieDespacho = despacho.SerieDespacho;
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            serieDespachoUf = despacho.SerieDespachoUf;

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            fluxoComercial = cteRaiz.FluxoComercial;

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            if (fluxoComercial != null)
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            {
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            mercadoria = fluxoComercial.Mercadoria;
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            estacaoMaeOrigem = fluxoComercial.Origem;
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            estacaoMaeDestino = fluxoComercial.Destino;

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            manutencaoDto = new ManutencaoCteDto();
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            manutencaoDto.IdDespacho = despacho.Id.Value;
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            manutencaoDto.CteId = cteRaiz.Id ?? 0;
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            manutencaoDto.SerieDesp5 = serieDespacho != null ? Convert.ToInt32(serieDespacho.SerieDespachoNum) : 0;
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            manutencaoDto.NumDesp5 = despacho.NumeroDespacho ?? 0;
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            manutencaoDto.SerieDesp6 = serieDespachoUf != null ? serieDespachoUf.NumeroSerieDespacho : 0;
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            manutencaoDto.NumDesp6 = despacho.NumeroDespacho ?? 0;
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            manutencaoDto.VagaoId = vagao.Id.HasValue ? vagao.Id.Value : 0;
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            manutencaoDto.CodVagao = vagao.Codigo;
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            manutencaoDto.Fluxo = fluxoComercial.Codigo;
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            manutencaoDto.CodigoOrigem = estacaoMaeOrigem != null ? estacaoMaeOrigem.Codigo : string.Empty;
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            manutencaoDto.CodigoDestino = estacaoMaeDestino != null ? estacaoMaeDestino.Codigo : string.Empty;
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            manutencaoDto.CodigoMercadoria = mercadoria != null ? mercadoria.Codigo : string.Empty;
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            manutencaoDto.ToneladaUtil = cteRaiz.PesoVagao ?? 0;
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            manutencaoDto.Volume = cteRaiz.VolumeVagao;
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            manutencaoDto.NroCte = cteRaiz.Numero;
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            manutencaoDto.SerieCte = cteRaiz.Serie;
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            manutencaoDto.ChaveCte = cteRaiz.Chave;
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            manutencaoDto.DataEmissao = cteRaiz.DataHora.ToString("dd/MM/yyyy");
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            // manutencaoDto.Status = cte.SituacaoAtual == SituacaoCteEnum.Invalidado ? StatusCteEnum.Inativo : StatusCteEnum.Ativo;
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            manutencaoDto.CteComAgrupamentoNaoCancelado = cteComAgrupamentoNaoCancelado;

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            listaManutencaoDto.Add(manutencaoDto);
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            }
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            }
            }

            return listaManutencaoDto;
             */
            return listaDto;
        }

        /// <summary>
        /// Retorna uma lista de Cte's para corre��o
        /// </summary>
        /// <param name="pagination"> Paginacao pesquisa</param>
        /// <param name="filtros"> Filtros para pesquisa</param>
        /// <returns>lista de Cte</returns>
        public ResultadoPaginado<CorrecaoCteDto> ObterListaCteParaCorrecao(DetalhesPaginacaoWeb pagination, DetalhesFiltro<object>[] filtros)
        {
            DateTime dataIni = DateTime.Now;
            DateTime dataFim = DateTime.Now;
            string serie = string.Empty;
            int numDespacho = 0;
            string erro = string.Empty;
            string fluxo = string.Empty;
            string origem = string.Empty;
            string destino = string.Empty;
            string numVagao = string.Empty;
            string chaveCte = string.Empty;
            string codigoUfDcl = string.Empty;

            if (filtros != null)
            {
                foreach (var detalheFiltro in filtros)
                {
                    if (detalheFiltro.Campo.Equals("dataInicial") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        string texto = detalheFiltro.Valor[0].ToString();

                        dataIni = DateTime.ParseExact(texto, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                    }

                    if (detalheFiltro.Campo.Equals("dataFinal") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        string texto = detalheFiltro.Valor[0].ToString();

                        dataFim = DateTime.ParseExact(texto, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);

                        dataFim = dataFim.AddDays(1).AddSeconds(-1);
                    }

                    if (detalheFiltro.Campo.Equals("serie") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        serie = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
                    }

                    if (detalheFiltro.Campo.Equals("despacho") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        numDespacho = Convert.ToInt32(detalheFiltro.Valor[0].ToString());
                    }

                    if (detalheFiltro.Campo.Equals("UfDcl") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        codigoUfDcl = detalheFiltro.Valor[0].ToString().ToUpperInvariant().Trim();
                    }

                    if (detalheFiltro.Campo.Equals("fluxo") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        fluxo = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
                    }

                    if (detalheFiltro.Campo.Equals("Origem") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        origem = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
                    }

                    if (detalheFiltro.Campo.Equals("Destino") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        destino = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
                    }

                    if (detalheFiltro.Campo.Equals("chave") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        chaveCte = detalheFiltro.Valor[0].ToString();
                    }

                    if (detalheFiltro.Campo.Equals("errocte") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        erro = detalheFiltro.Valor[0].ToString();
                    }

                    if (detalheFiltro.Campo.Equals("Vagao") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        numVagao = detalheFiltro.Valor[0].ToString();
                    }
                }
            }

            ResultadoPaginado<CorrecaoCteDto> listaDto = new ResultadoPaginado<CorrecaoCteDto>();

            listaDto = _cteRepository.ObterListaCteParaCorrecao(pagination, dataIni, dataFim, serie, numDespacho, fluxo, chaveCte, erro, origem, destino, numVagao, codigoUfDcl);

            return listaDto;
        }

        /// <summary>
        /// Retorna todos os Cte Cancelados ou invalidados para realizar manuten��o
        /// </summary>
        /// <param name="pagination"> Paginacao pesquisa</param>
        /// <param name="filtros"> Filtros para pesquisa</param>
        /// <returns>lista de Cte</returns>
        public ResultadoPaginado<ManutencaoCteDto> RetornaListaCteVirtual(DetalhesPaginacaoWeb pagination, DetalhesFiltro<object>[] filtros)
        {
            DateTime dataIni = DateTime.Now;
            DateTime dataFim = DateTime.Now;
            string serie = string.Empty;
            int numDespacho = 0;
            string erro = string.Empty;
            string fluxo = string.Empty;
            string origem = string.Empty;
            string destino = string.Empty;
            string numVagao = string.Empty;
            string chaveCte = string.Empty;
            string codigoUfDcl = string.Empty;

            if (filtros != null)
            {
                foreach (var detalheFiltro in filtros)
                {
                    if (detalheFiltro.Campo.Equals("dataInicial") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        string texto = detalheFiltro.Valor[0].ToString();

                        dataIni = DateTime.ParseExact(texto, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                    }

                    if (detalheFiltro.Campo.Equals("dataFinal") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        string texto = detalheFiltro.Valor[0].ToString();

                        dataFim = DateTime.ParseExact(texto, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);

                        dataFim = dataFim.AddDays(1).AddSeconds(-1);
                    }

                    if (detalheFiltro.Campo.Equals("serie") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        serie = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
                    }

                    if (detalheFiltro.Campo.Equals("despacho") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        numDespacho = Convert.ToInt32(detalheFiltro.Valor[0].ToString());
                    }

                    if (detalheFiltro.Campo.Equals("UfDcl") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        codigoUfDcl = detalheFiltro.Valor[0].ToString().ToUpperInvariant().Trim();
                    }

                    if (detalheFiltro.Campo.Equals("fluxo") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        fluxo = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
                    }

                    if (detalheFiltro.Campo.Equals("Origem") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        origem = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
                    }

                    if (detalheFiltro.Campo.Equals("Destino") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        destino = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
                    }

                    if (detalheFiltro.Campo.Equals("chave") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        chaveCte = detalheFiltro.Valor[0].ToString();
                    }

                    if (detalheFiltro.Campo.Equals("errocte") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        erro = detalheFiltro.Valor[0].ToString();
                    }

                    if (detalheFiltro.Campo.Equals("Vagao") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        numVagao = detalheFiltro.Valor[0].ToString();
                    }
                }
            }

            ResultadoPaginado<ManutencaoCteDto> listaDto = new ResultadoPaginado<ManutencaoCteDto>();

            listaDto = _cteRepository.ObterCteVirtual(pagination, dataIni, dataFim, serie, numDespacho, fluxo, chaveCte, erro, origem, destino, numVagao, codigoUfDcl);

            return listaDto;
        }

        /// <summary>
        /// Retorna o Cte Selecionado na tela de manuten��o
        /// </summary>
        /// <param name="idCte">id do Cte Selecionado</param>
        /// <returns>Cte selecionado</returns>
        public ManutencaoCteDto RetornaCteManutencao(int idCte)
        {
            ManutencaoCteDto manutencaoDto = new ManutencaoCteDto();

            DespachoTranslogic despacho = null;
            FluxoComercial fluxoComercial = null;
            SerieDespacho serieDespacho = null;
            SerieDespachoUf serieDespachoUf = null;
            Vagao vagao = null;

            Cte cte = _cteRepository.ObterPorIdCte(idCte);

            despacho = cte.Despacho;
            vagao = cte.Vagao;

            if (despacho != null)
            {
                serieDespacho = despacho.SerieDespacho;
                serieDespachoUf = despacho.SerieDespachoUf;

                fluxoComercial = cte.FluxoComercial;

                if (fluxoComercial != null)
                {
                    manutencaoDto.CteId = cte.Id ?? 0;
                    manutencaoDto.IdDespacho = despacho.Id.Value;
                    manutencaoDto.SerieDesp5 = despacho.SerieDespachoSdi ?? (serieDespacho != null ? serieDespacho.SerieDespachoNum : string.Empty);
                    manutencaoDto.NumDesp5 = despacho.NumDespIntercambio ?? (despacho.NumeroDespacho ?? 0);
                    manutencaoDto.SerieDesp6 = serieDespachoUf != null ? serieDespachoUf.NumeroSerieDespacho : 0;
                    manutencaoDto.NumDesp6 = despacho.NumeroDespacho ?? 0;
                    manutencaoDto.VagaoId = vagao.Id.HasValue ? vagao.Id.Value : 0;
                    manutencaoDto.CodVagao = vagao.Codigo;
                    manutencaoDto.Fluxo = fluxoComercial.Codigo;
                    manutencaoDto.ToneladaUtil = cte.PesoVagao ?? 0.0;
                    manutencaoDto.Volume = cte.VolumeVagao;
                    manutencaoDto.NroCte = cte.Numero;
                    manutencaoDto.SerieCte = cte.Serie;
                    manutencaoDto.DataEmissao = cte.DataHora;
                    manutencaoDto.ChaveCte = cte.Chave;
                    manutencaoDto.Status = cte.SituacaoAtual == SituacaoCteEnum.Invalidado ? StatusCteEnum.Inativo : StatusCteEnum.Ativo;
                    manutencaoDto.Situacao = cte.SituacaoAtual;
                    manutencaoDto.IdNfeAnulacao = cte.NfeAnulacao == null ? null : cte.NfeAnulacao.Id;
                    manutencaoDto.DataAnulacao = cte.DataAnulacao;
                }
            }

            return manutencaoDto;
        }

        /// <summary>
        /// Realiza a Impress�o dos Ctes
        /// </summary>
        /// <param name="ids">ids dos ctes</param>
        /// <returns>resultado da Impress�o</returns>
        public bool ImprimirCtes(List<int> ids)
        {
            try
            {
                Cte cte = null;

                foreach (int id in ids)
                {
                    cte = _cteRepository.ObterPorId(id);
                    cte.Impresso = true;
                    _cteRepository.Atualizar(cte);
                }
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Obtem Detalhe do Cte (nota) por id do Cte
        /// </summary>
        /// <param name="cteId">id do Cte Selecionado</param>
        /// <returns>Retorna lista de Detalhe Cte</returns>
        public IList<CteDetalhe> ObterCteDetalhePorIdCte(int cteId)
        {
            IList<CteDetalhe> lista = new List<CteDetalhe>();

            if (cteId != 0)
            {
                // Cte cte = _cteRepository.ObterPorId(cteId);
                Cte cte = new Cte { Id = cteId };
                lista = _cteDetalheRepository.ObterPorCte(cte);
            }

            return lista;
        }

        /// <summary>
        /// Obtem Detalhe do Cte (nota) por id do Cte
        /// </summary>
        /// <param name="arrayCtes">id do Cte Selecionado</param>
        /// <returns>Retorna lista de Detalhe Cte</returns>
        public IList<Cte> ObterCteImpressao(int[] arrayCtes)
        {
            return _cteRepository.ObterTodosPorId(arrayCtes);
        }

        /// <summary>
        /// Insere os ctes da lista no pool de processamento do servidor
        /// </summary>
        /// <param name="listaCtesNaoProcessados">Lista de ctes</param>
        /// <param name="hostName">Ip do Servidor</param>
        public void InserirPoolingEnvioCte(IList<Cte> listaCtesNaoProcessados, string hostName)
        {
            foreach (Cte cteAux in listaCtesNaoProcessados)
            {
                Cte cte = cteAux;
                try
                {
                    CteEnvioPooling cep = new CteEnvioPooling();
                    cep.Id = cte.Id;
                    cep.DataHora = DateTime.Now;
                    cep.HostName = hostName;

                    Usuario usuario = ObterUsuarioRobo();

                    // Insere o cte no pooling
                    _cteEnvioPoolingRepository.Inserir(cep);

                    switch (cte.SituacaoAtual)
                    {
                        case SituacaoCteEnum.PendenteArquivoEnvio:

                            // Caso seja um cte complementar
                            if (cte.TipoCte == TipoCteEnum.Complementar)
                            {
                                cte.SituacaoAtual = SituacaoCteEnum.EnviadoFilaArquivoComplemento;
                                // Insere no log do cte
                                _cteLogService.InserirLogInfo(cte, "CteService", "Adicionado o arquivo de complemento no pooling");
                            }
                            else
                            {
                                cte.SituacaoAtual = SituacaoCteEnum.EnviadoFilaArquivoEnvio;
                                // Insere no log do cte
                                _cteLogService.InserirLogInfo(cte, "CteService", "Adicionado o arquivo de envio no pooling");
                            }

                            // Insere o status do cte
                            _cteStatusRepository.InserirCteStatus(cte, usuario, new CteStatusRetorno { Id = 21 });
                            break;
                        case SituacaoCteEnum.PendenteArquivoCancelamento:
                        case SituacaoCteEnum.AguardandoCancelamento:

                            cte.SituacaoAtual = SituacaoCteEnum.EnviadoFilaArquivoCancelamento;
                            // Insere o status do cte
                            _cteStatusRepository.InserirCteStatus(cte, usuario, new CteStatusRetorno { Id = 21 });

                            // Insere no log do cte
                            _cteLogService.InserirLogInfo(cte, "CteService", "Adicionado o arquivo de cancelamento no pooling");
                            break;
                        case SituacaoCteEnum.PendenteArquivoInutilizacao:
                        case SituacaoCteEnum.AguardandoInutilizacao:
                            cte.SituacaoAtual = SituacaoCteEnum.EnviadoFilaArquivoInutilizacao;
                            // Insere o status do cte
                            _cteStatusRepository.InserirCteStatus(cte, usuario, new CteStatusRetorno { Id = 21 });

                            // Insere no log do cte
                            _cteLogService.InserirLogInfo(cte, "CteService", "Adicionado o arquivo de inutiliza��o no pooling");
                            break;

                        case SituacaoCteEnum.PendenteArquivoAnulacaoContribuinte:
                            cte.SituacaoAtual = SituacaoCteEnum.EnviadoFilaArquivoAnulacaoContribuinte;
                            // Insere o status do cte
                            _cteStatusRepository.InserirCteStatus(cte, usuario, new CteStatusRetorno { Id = 21 });
                            // Insere no log do cte
                            _cteLogService.InserirLogInfo(cte, "CteService", "Adicionado o arquivo de anula��o contribuinte no pooling.");

                            break;

                        case SituacaoCteEnum.PendenteArquivoAnulacaoNContribuinte:
                            cte.SituacaoAtual = SituacaoCteEnum.EnviadoFilaArquivoAnulacaoNContribuinte;
                            // Insere o status do cte
                            _cteStatusRepository.InserirCteStatus(cte, usuario, new CteStatusRetorno { Id = 21 });
                            // Insere no log do cte
                            _cteLogService.InserirLogInfo(cte, "CteService", "Adicionado o arquivo de anula��o n�o contribuinte no pooling");

                            break;


                        case SituacaoCteEnum.PendenteCorrecaoEnvio:
                            cte.SituacaoAtual = SituacaoCteEnum.EnviadoFilaArquivoCorrecao;
                            // Insere o status do cte
                            _cteStatusRepository.InserirCteStatus(cte, usuario, new CteStatusRetorno { Id = 21 });

                            // Insere no log do cte
                            _cteLogService.InserirLogInfo(cte, "CteService", "Adicionado o arquivo de corre��o no pooling");
                            break;
                    }

                    // Altera o status do CTE
                    // _cteRepository.Atualizar(cte);
                    _cteRepository.AtualizarSituacao(cte);

                    _cteLogService.InserirLogInfo(cte, "CteService", "Cte com Status atualizado e inserido no Pooling.");
                }
                catch (Exception ex)
                {
                    // Insere no log do cte
                    _cteLogService.InserirLogErro(cte, "CteService", string.Format("InserirPoolingEnvioCte: erro para inserir no pooling - {0}", ex.Message), ex);

                    // Limpa a sess�o para processar o pr�ximo
                    _cteEnvioPoolingRepository.ClearSession();
                }
            }
        }

        /// <summary>
        /// Obt�m a Lista de Ctes serem processados para envio
        /// </summary>
        /// <param name="indAmbienteSefaz">Indica qual ambiente do SEFAZ</param>
        /// <returns>Lista de ctes</returns> 
        public IList<Cte> ObterCtesProcessarEnvio(int indAmbienteSefaz)
        {
            try
            {
                return _cteRepository.ObterStatusProcessamentoEnvio(indAmbienteSefaz);
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(null, "CteService", string.Format("ObterCtesProcessarEnvio: {0}", ex.Message), ex);
                throw;
            }
        }

        /// <summary>
        /// Insere os dados do arquivo de envio na tabela de NDD
        /// </summary>
        /// <param name="cte">Cte a ser inerido</param>
        /// <param name="arquivo">Dados do arquivo da NDD</param>
        public void InserirArquivoEnvioNdd(Cte cte, StringBuilder arquivo)
        {
            // Insere na tabela da NDD
            _databaseInputRepository.Inserir(cte.Numero, null, arquivo);

            // Altera o status do CTE
            cte.SituacaoAtual = SituacaoCteEnum.EnviadoArquivoEnvio;
            _cteRepository.Atualizar(cte);

            Usuario usuario = ObterUsuarioRobo();

            // Insere o status do cte
            _cteStatusRepository.InserirCteStatus(cte, usuario, new CteStatusRetorno { Id = 11 });

            // Insere no log do cte
            _cteLogService.InserirLogInfo(cte, "CteService", "Arquivo enviado para a NDD");
        }

        /// <summary>
        /// Insere o arquivo de Cte na fila de processamento da config
        /// </summary>
        /// <param name="cte">Cte a ser inserido na fila</param>
        public void InserirConfigCteEnvio(Cte cte)
        {
            try
            {
                int idListaConfig = 0;
                // Insere na fila de processamento da config - Cte01 - Lista
                idListaConfig = _filaProcessamentoService.InserirCteEnvioFilaProcessamento(cte);

                // Insere na fila a gera��o do Pdf
                _filaProcessamentoService.InserirGeracaArquivosCteNoServidorFtp(cte);

                // Gera o PDF pela impress�o
                _filaProcessamentoService.InserirGeracaoPdfPorImpressao(cte);

                // Insere na fila a gera��o do xml
                _filaProcessamentoService.InserirGeracaoArquivosCteNoServidorEmail(cte);

                // Insere no pooling de recebimento (insere na tabela informando que foi colocado na fila o cte)
                InserirInterfaceRecebimentoConfig(cte, idListaConfig);

                // Altera o status do CTE - Para status EAE
                if (cte.TipoCte == TipoCteEnum.Anulacao)
                {
                    if (cte.FluxoComercial.Contrato.IndIeToma == 9)
                    {
                        cte.SituacaoAtual = SituacaoCteEnum.EnviadoArquivoAnulacaoNc;
                    }
                    else
                    {
                        cte.SituacaoAtual = SituacaoCteEnum.EnviadoArquivoAnulacao;
                    }
                }
                else
                {
                    cte.SituacaoAtual = SituacaoCteEnum.EnviadoArquivoEnvio;
                }

                _cteRepository.Atualizar(cte);

                // Pega o usuario default da conf geral
                Usuario usuario = ObterUsuarioRobo();

                // Insere o status do cte
                _cteStatusRepository.InserirCteStatus(cte, usuario, new CteStatusRetorno { Id = 11 });

                // Insere no log do cte
                _cteLogService.InserirLogInfo(cte, "CteService", "Cte enviado para a Config");
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Insere o arquivo de Cte na fila de processamento da config
        /// </summary>
        /// <param name="cte">Cte a ser inserido na fila</param>
        public void InserirConfigCteCorrecao(Cte cte)
        {
            try
            {
                int idListaConfig = 0;
                // Insere na fila de processamento da config
                idListaConfig = _filaProcessamentoService.InserirCteCorrecaEnvioFilaProcessamento(cte);

                // Insere na fila a gera��o do Pdf
                _filaProcessamentoService.InserirGeracaArquivosCteNoServidorFtp(cte);

                // Gera o PDF pela impress�o
                _filaProcessamentoService.InserirGeracaoPdfPorImpressao(cte);

                // Insere na fila a gera��o do xml
                _filaProcessamentoService.InserirGeracaoArquivosCteNoServidorEmail(cte);

                // Insere no pooling de recebimento (insere na tabela informando que foi colocado na fila o cte)
                InserirInterfaceRecebimentoConfig(cte, idListaConfig);

                // Altera o status do CTE
                cte.SituacaoAtual = SituacaoCteEnum.EnviadoArquivoEnvio;
                _cteRepository.Atualizar(cte);

                // Pega o usuario default da conf geral
                Usuario usuario = ObterUsuarioRobo();

                // Insere o status do cte
                _cteStatusRepository.InserirCteStatus(cte, usuario, new CteStatusRetorno { Id = 11 });

                // Insere no log do cte
                _cteLogService.InserirLogInfo(cte, "CteService", "Cte enviado para a Config");
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Insere o Cte de complemento na fila de processamento da config
        /// </summary>
        /// <param name="cte">Cte de complemento</param>
        public void InserirConfigCteComplemento(Cte cte)
        {
            int idListaConfig = 0;
            // Insere na fila de processamento da config
            idListaConfig = _filaProcessamentoService.InserirCteComplementoFilaProcessamento(cte);

            // Insere na fila a gera��o do Pdf
            _filaProcessamentoService.InserirGeracaArquivosCteNoServidorFtp(cte);

            // Gera o PDF pela impress�o
            _filaProcessamentoService.InserirGeracaoPdfPorImpressao(cte);

            // Insere na fila a gera��o do xml
            _filaProcessamentoService.InserirGeracaoArquivosCteNoServidorEmail(cte);

            // Insere no pooling de recebimento (insere na tabela informando que foi colocado na fila o cte)
            InserirInterfaceRecebimentoConfig(cte, idListaConfig);

            // Altera o status do CTE
            cte.SituacaoAtual = SituacaoCteEnum.EnviadoArquivoComplemento;
            _cteRepository.Atualizar(cte);

            Usuario usuario = ObterUsuarioRobo();

            // Insere o status do cte
            _cteStatusRepository.InserirCteStatus(cte, usuario, new CteStatusRetorno { Id = 11 });

            // Insere no log do cte
            _cteLogService.InserirLogInfo(cte, "CteService", "Cte de complemento enviado para a Config");
        }

        /// <summary>
        /// Insere os dados do arquivo de cancelamento na tabela de NDD
        /// </summary>
        /// <param name="cte">Cte a ser inserido</param>
        /// <param name="arquivo">Dados do arquivo da NDD</param>
        public void InserirArquivoCancelamentoNdd(Cte cte, StringBuilder arquivo)
        {
            // Insere na tabela da NDD
            _databaseInputRepository.Inserir(cte.Numero, null, arquivo);

            // Altera o status do CTE
            cte.SituacaoAtual = SituacaoCteEnum.EnviadoArquivoCancelamento;
            _cteRepository.Atualizar(cte);

            Usuario usuario = ObterUsuarioRobo();

            // Insere o status do cte
            _cteStatusRepository.InserirCteStatus(cte, usuario, new CteStatusRetorno { Id = 24 });

            // Insere no log do cte
            _cteLogService.InserirLogInfo(cte, "CteService", "Arquivo de cancelamento enviado para a NDD");
        }

        /// <summary>
        /// Insere os dados do arquivo de cancelamento na tabela de NDD
        /// </summary>
        /// <param name="cte">Cte a ser inserido</param>
        /// <param name="arquivo">Dados do arquivo da NDD</param>
        public void InserirConfigCteAnulacao(Cte cte)
        {
            // Insere na tabela da NDD
            ////_databaseInputRepository.Inserir(cte.Numero, null, arquivo);

            // Altera o status do CTE
            cte.SituacaoAtual = SituacaoCteEnum.EnviadoArquivoAnulacao;
            _cteRepository.Atualizar(cte);

            Usuario usuario = ObterUsuarioRobo();

            // Insere o status do cte
            _cteStatusRepository.InserirCteStatus(cte, usuario, new CteStatusRetorno { Id = 38 });

            // Insere no log do cte
            _cteLogService.InserirLogInfo(cte, "CteService", "Arquivo de anula��o enviado para a Config");
        }

        /// <summary>
        /// Insere os dados do arquivo de cancelamento na tabela de NDD
        /// </summary>
        /// <param name="cte">Cte a ser inserido</param>
        /// <param name="arquivo">Dados do arquivo da NDD</param>
        public void InserirConfigCteAnulacaoNc(Cte cte)
        {
            // Insere na tabela da NDD
            ////_databaseInputRepository.Inserir(cte.Numero, null, arquivo);

            // Altera o status do CTE
            cte.SituacaoAtual = SituacaoCteEnum.EnviadoArquivoAnulacaoNc;
            _cteRepository.Atualizar(cte);

            Usuario usuario = ObterUsuarioRobo();

            // Insere o status do cte
            _cteStatusRepository.InserirCteStatus(cte, usuario, new CteStatusRetorno { Id = 38 });

            // Insere no log do cte
            _cteLogService.InserirLogInfo(cte, "CteService", "Arquivo de anula��o n�o contribuinte enviado para a Config");
        }



        /// <summary>
        /// Insere o Cte de cancelamento na fila de processamento da config
        /// </summary>
        /// <param name="cte">Cte a ser inserido</param>
        public void InserirConfigCteCancelamento(Cte cte)
        {
            try
            {
                // Insere no log do cte
                _cteLogService.InserirLogInfo(cte, "CteService - CA", "Cte de cancelamento enviado para a Config");

                // Altera o status do CTE
                cte.SituacaoAtual = SituacaoCteEnum.EnviadoArquivoCancelamento;
                _cteRepository.AtualizarSituacao(cte);

                Usuario usuario = ObterUsuarioRobo();
                int idListaConfig = 0;

                // Insere o status do cte
                _cteStatusRepository.InserirCteStatus(cte, usuario, new CteStatusRetorno { Id = 24 });

                // Insere na fila de processamento da config
                idListaConfig = _filaProcessamentoService.InserirCteCancelamentoFilaProcessamento(cte);

                // Insere no pooling de recebimento (insere na tabela informando que foi colocado na fila o cte)
                InserirInterfaceRecebimentoConfig(cte, idListaConfig);

                _filaProcessamentoService.InserirGeracaoArquivosCteNoServidorEmailCancelamento(cte);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Insere os dados do arquivo de inutiliza��o na tabela de NDD
        /// </summary>
        /// <param name="cte">Cte a ser inerido</param>
        /// <param name="arquivo">Dados do arquivo da NDD</param>
        public void InserirArquivoInutilizacaoNdd(Cte cte, StringBuilder arquivo)
        {
            // Insere na tabela da NDD
            _databaseInputRepository.Inserir(cte.Numero, null, arquivo);

            // Altera o status do CTE
            cte.SituacaoAtual = SituacaoCteEnum.EnviadoArquivoInutiliza��o;
            _cteRepository.Atualizar(cte);

            Usuario usuario = ObterUsuarioRobo();

            // Insere o status do cte
            _cteStatusRepository.InserirCteStatus(cte, usuario, new CteStatusRetorno { Id = 25 });

            // Insere no log do cte
            _cteLogService.InserirLogInfo(cte, "CteService", "Arquivo de inutiliza��o enviado para a NDD");
        }

        /// <summary>
        /// Insere o Cte de inutiliza��o na fila de processamento da config
        /// </summary>
        /// <param name="cte">Cte a ser inutilizado</param>
        public void InserirConfigCteInutilizacao(Cte cte)
        {
            int idListaConfig = 0;
            // Insere na fila de processamento da config
            idListaConfig = _filaProcessamentoService.InserirCteInutilizacaoFilaProcessamento(cte);

            // Insere no pooling de recebimento (insere na tabela informando que foi colocado na fila o cte)
            InserirInterfaceRecebimentoConfig(cte, idListaConfig);

            // Altera o status do CTE
            cte.SituacaoAtual = SituacaoCteEnum.EnviadoArquivoInutiliza��o;
            _cteRepository.Atualizar(cte);

            Usuario usuario = ObterUsuarioRobo();

            // Insere o status do cte
            _cteStatusRepository.InserirCteStatus(cte, usuario, new CteStatusRetorno { Id = 25 });

            // Insere no log do cte
            _cteLogService.InserirLogInfo(cte, "CteService", "Cte de inutiliza��o enviado para a Config");
        }

        /// <summary>
        /// Obt�m os ctes do pool pelo ip do servStatusProcessamentoEnvio
        /// </summary>
        /// <param name="hostName">Nome do host</param>
        /// <returns>Lista de Ctes</returns>
        public IList<Cte> ObterPoolingEnvioPorServidor(string hostName)
        {
            try
            {
                return _cteRepository.ObterListaEnvioPorHost(hostName);
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogInfo(new Cte { Id = 5508027 }, "CteService", string.Format("ObterPoolingEnvioPorServidor({0}): {1}", hostName, ex.StackTrace));
                _cteLogService.InserirLogErro(null, "CteService", string.Format("ObterPoolingEnvioPorServidor({0}): {1}", hostName, ex.Message), ex);
                throw;
            }
        }

        /// <summary>
        /// Obt�m os ctes complementado do pool pelo ip do servStatusProcessamentoEnvio
        /// </summary>
        /// <param name="hostName">Nome do host</param>
        /// <returns>Lista de Ctes</returns>
        public IList<Cte> ObterPoolingEnvioComplementadoPorServidor(string hostName)
        {
            try
            {
                return _cteRepository.ObterListaEnvioComplementarPorHost(hostName);
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(null, "CteService", string.Format("ObterPoolingEnvioComplementadoPorServidor({0}): {1}", hostName, ex.Message), ex);
                throw;
            }
        }

        /// <summary>
        /// Remove os ctes do pool
        /// </summary>
        /// <param name="lista">Lista de ctes processados</param>
        public void RemoverPoolingEnvioCte(IList<Cte> lista)
        {
            foreach (var cte in lista)
            {
                _cteLogService.InserirLogInfo(cte, "CteService", "RemoverPoolingEnvioCte");
            }

            _cteEnvioPoolingRepository.RemoverPorLista(lista);
        }

        /// <summary>
        /// Retorna todos os Cte para monitoramento
        /// </summary>
        /// <param name="pagination">Detalhes da Pagina��o</param>
        /// <param name="dataInicial">Data inicial de pesquisa</param>
        /// <param name="dataFinal">Data final de pesquisa</param>
        /// <param name="serie">N�mero da s�rie</param>
        /// <param name="despacho">N�mero do Despacho</param>
        /// <param name="codFluxo">C�digo do fluxo comercial</param>
        /// <param name="chaveCte">Chave do Cte informada</param>
        /// <param name="erro">Status de erro Cte</param>
        /// <param name="origem"> Origem do Cte</param>
        /// <param name="destino"> Destino do Cte</param>
        /// <param name="numVagao"> Numero do Vagao</param>b
        /// <param name="impresso"> Indicador se cte impresso</param>
        /// <param name="codigoUfDcl">Codigo controle do despacho</param>
        /// <param name="incluirNfe">deve incluir nfe</param>
        /// <returns>lista de Cte</returns>
        public ResultadoPaginado<CteDto> RetornaCteMonitoramento(DetalhesPaginacao pagination, DateTime dataInicial, DateTime dataFinal, string serie, int despacho, string codFluxo, string chaveCte, string erro, string origem, string destino, string numVagao, bool? impresso, string codigoUfDcl, bool incluirNfe)
        {
            ResultadoPaginado<CteDto> listaDto = new ResultadoPaginado<CteDto>();

            listaDto = _cteRepository.ObterCteMonitoramento(pagination, dataInicial, dataFinal, serie, despacho, codFluxo, chaveCte, erro, origem, destino, numVagao, impresso, codigoUfDcl, incluirNfe);
            listaDto.Items = listaDto.Items.OrderByDescending(c => c.Numero).ToList();

            return listaDto;
        }

        /// <summary>
        /// Retorna todos os Cte para monitoramento
        /// </summary>
        /// <param name="pagination">Detalhes da Pagina��o</param>
        /// <param name="dataInicial">Data inicial de pesquisa</param>
        /// <param name="dataFinal">Data final de pesquisa</param>
        /// <param name="codFluxo">C�digo do fluxo comercial</param>
        /// <param name="origem"> Origem do Cte</param>
        /// <param name="destino"> Destino do Cte</param>
        /// <returns>lista de Cte</returns>
        public ResultadoPaginado<CteDto> RetornaCteRelatorioErro(DetalhesPaginacao pagination, DateTime dataInicial, DateTime dataFinal, string codFluxo, string origem, string destino)
        {
            ResultadoPaginado<CteDto> listaDto = new ResultadoPaginado<CteDto>();

            listaDto = _cteRepository.ObterCteRelatorioErro(pagination, dataInicial, dataFinal, codFluxo, origem, destino);
            listaDto.Items = listaDto.Items.OrderByDescending(c => c.Numero).ToList();

            return listaDto;
        }

        /// <summary>
        /// Retorna a lista de Ctes com erro para montar o sem�foro
        /// </summary>
        /// <returns>Lista de Ctes com erro</returns>
        public IList<SemaforoCteDto> RetornaListaCtesErro()
        {
            return _cteRepository.ObterCtesErro();
        }

        /// <summary>
        /// Obter os vag�es de acordo com os filtros informados na tela
        /// </summary>
        /// <param name="pagination">Detalhes de pagina��o de resultados</param>
        /// <param name="dataInicial">Data inicial do Cte</param>
        /// <param name="dataFinal">Data final do Cte</param>
        /// <param name="chaveNfe">Chave da Nfe</param>
        /// <param name="fluxo">O fluxo do cte</param>
        /// <param name="siglaUfFerrovia">a UF da ferrovia do Cte</param>
        /// <param name="origem">A Origem do cte</param>
        /// <param name="destino">O Destino do cte</param>
        /// <param name="serieDesp">O n�mero de s�rie do despacho</param>
        /// <param name="numeroDesp">O n�mero do despacho</param>
        /// <param name="chaveCte">O n�mero da chave do cte</param>
        /// <param name="codVagao">O c�digo do vag�o</param>
        /// <param name="tipoCte">Tipo de cte para selecionar</param>
        /// <returns>Lista de vag�es de acordo com os filtros informados</returns>
        public ResultadoPaginado<CteDto> ObterComposicoesFiltros(DetalhesPaginacao pagination, DateTime dataInicial, DateTime dataFinal, string chaveNfe, string fluxo, string siglaUfFerrovia, string origem, string destino, int serieDesp, int numeroDesp, string[] chaveCte, string codVagao, string tipoCte)
        {
            return _cteRepository.ObterComposicaoPorFiltros(pagination, dataInicial, dataFinal, chaveNfe, fluxo, siglaUfFerrovia, origem, destino, serieDesp, numeroDesp, chaveCte, codVagao, tipoCte);
        }

        /// <summary>
        /// Obt�m os vag�es de acordo como o Trem informado
        /// </summary>
        /// <param name="pagination">Detalhes de pagina��o de resultados</param>
        /// <param name="idTrem">Id do trem selecionado</param>
        /// <returns>Lista de vag�es de acordo com os filtros informados</returns>
        public ResultadoPaginado<CteDto> ObterComposicaoPorTrem(DetalhesPaginacao pagination, int idTrem)
        {
            return _cteRepository.ObterComposicaoPorTrem(pagination, idTrem);
        }

        /// <summary>
        /// Obt�m os vag�es de acordo com o Mdfe selecionado
        /// </summary>
        /// <param name="pagination">Detalhes de pagina��o de resultados</param>
        /// <param name="idMdfe">Id da Mdfe selecionada</param>
        /// <returns>Lista de vag�es de acordo com os filtros informados</returns>
        public ResultadoPaginado<CteDto> ObterComposicaoPorMdfe(DetalhesPaginacao pagination, int idMdfe)
        {
            return _cteRepository.ObterComposicaoPorMdfe(pagination, idMdfe);
        }

        /// <summary>
        /// Obt�m os vag�es de acordo com o sem�foro
        /// </summary>
        /// <param name="pagination">Detalhes de pagina��o de resultados</param>
        /// <param name="semaforo">Semaforo que foi selecionado para o filtro</param>
        /// <param name="siglaUfSemaforo">Sigla da UF selecionada no sem�foro</param>
        /// <param name="periodoHorasErroSemaforo">O per�odo de tempo selecionado no sem�foro</param>
        /// <returns>Lista de vag�es de acordo com os filtros informados</returns>
        public ResultadoPaginado<CteDto> ObterComposicaoPorSemaforo(DetalhesPaginacao pagination, string semaforo, string siglaUfSemaforo, HorasErroEnum periodoHorasErroSemaforo)
        {
            return _cteRepository.ObterComposicaoPorSemaforo(pagination, semaforo, siglaUfSemaforo, periodoHorasErroSemaforo);
        }

        /// <summary>
        /// Obt�m os status de uma Cte
        /// </summary>
        /// <param name="idCte">Id da Cte que se deseja obter os status</param>
        /// <returns>Lista de Status da Cte</returns>
        public IList<CteStatus> ObterStatusCtePorIdCte(int idCte)
        {
            return _cteRepository.ObterStatusCtePorIdCte(idCte);
        }

        /// <summary>
        /// Obt�m os status de uma Cte
        /// </summary>
        /// <param name="idCte">Id do Cte </param>
        /// <param name="idConteiner">Id do conteiner que se deseja obter os status</param
        /// <returns>C�digo do agrupamento</returns>
        public long ObterAgrupamentoContainer(int idCte, int idConteiner)
        {
            var cteConteiner = _cteConteinerRepository.ObterPorCteConteinerSemEstado(idCte, idConteiner);
            return cteConteiner == null ? 0 : cteConteiner.Agrupamento;
        }

        /// <summary>
        /// Obt�m os status de uma Cte
        /// </summary>
        /// <param name="idCte">Id do Cte </param>
        /// <param name="chavesMsgGeradas">chavesMsgGeradas </param>
        /// <param name="agrupCarga">Agrupa pela carga ao inves de agrupar pelo conteiner do cte</param>
        /// <returns>C�digo do agrupamento</returns>
        public string ObterMsgCtesAgrupados(int idCte, ref List<string> chavesMsgGeradas, bool agrupCarga = false)
        {
            string mensagem = string.Empty;
            var cte = _cteRepository.ObterPorId(idCte);
            if (cte.FluxoComercial.IndRateioCte ?? false)
            {
                IList<CteConteiner> listAgrupamentos;
                if (agrupCarga == false)
                {
                    listAgrupamentos = _cteConteinerRepository.ObterPorCte(idCte);
                }
                else
                {
                    listAgrupamentos = _cteConteinerRepository.ObterPorCteAgrupCarga(idCte);
                }

                if (listAgrupamentos.Count > 0)
                {
                    foreach (var agrupamento in listAgrupamentos)
                    {
                        if (chavesMsgGeradas.All(p => p != agrupamento.Cte.Chave))
                        {
                            mensagem += "CT-e: " + agrupamento.Cte.Numero + ", conteiner: " + agrupamento.Conteiner.Numero + "</br>";
                            chavesMsgGeradas.Add(agrupamento.Cte.Chave);
                        }
                    }
                }
                else
                {
                    if (chavesMsgGeradas.All(p => p != cte.Chave))
                    {
                        mensagem += "CT-e: " + cte.Numero + "</br>";
                        chavesMsgGeradas.Add(cte.Chave);
                    }
                }
            }
            else
            {
                if (chavesMsgGeradas.All(p => p != cte.Chave))
                {
                    mensagem += "CT-e: " + cte.Numero + "</br>";
                    chavesMsgGeradas.Add(cte.Chave);
                }
            }
            return mensagem;
        }

        /// <summary>
        /// Obt�m os status de uma Cte
        /// </summary>
        /// <param name="idCte">Id do Cte </param>
        /// <param name="agrupCarga">Agrupa por carga, se informado true, agrupa pela carga ao inv�s do conteiner </param>
        /// <returns>Lista co os registros do conteiners</returns>
        public IList<CteConteiner> ObterCteConteinersAgrupados(int idCte, bool agrupCarga = false)
        {
            if (!(agrupCarga))
            {
                return _cteConteinerRepository.ObterPorCte(idCte);
            }
            else
            {
                return _cteConteinerRepository.ObterPorCteAgrupCarga(idCte);
            }
        }

        /// <summary>
        /// Obt�m os status de uma Cte
        /// </summary>
        /// <param name="idCte">Id do Cte </param>
        /// <returns>Lista co os registros do conteiners</returns>
        public IList<Cte> ObterCteAgrupados(string chaveCte)
        {
            return _cteRepository.ObterCtesAgrupamentoConteiner(chaveCte);
        }

        /// <summary>
        /// Retorna todos os Cte Autorizados para listagem de Impress�o
        /// </summary>
        /// <param name="pagination"> Paginacao pesquisa</param>
        /// <param name="filtros"> Filtros para pesquisa</param>
        /// <returns>lista de Cte</returns>
        public ResultadoPaginado<CteDto> RetornaCteMonitoramento(DetalhesPaginacaoWeb pagination, DetalhesFiltro<object>[] filtros)
        {
            DateTime dataIni = DateTime.Now;
            DateTime dataFim = DateTime.Now;
            string serie = string.Empty;
            int numDespacho = 0;
            string[] numVagao = null;
            string erro = string.Empty;
            string fluxo = string.Empty;
            string origem = string.Empty;
            string destino = string.Empty;
            string chaveCte = string.Empty;
            string codigoUfDcl = string.Empty;
            bool? impresso = null;
            var incluirNfe = false;

            if (filtros != null)
            {
                foreach (var detalheFiltro in filtros)
                {
                    if (detalheFiltro.Campo.Equals("dataInicial") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        string texto = detalheFiltro.Valor[0].ToString();

                        dataIni = DateTime.ParseExact(texto, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                    }

                    if (detalheFiltro.Campo.Equals("dataFinal") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        string texto = detalheFiltro.Valor[0].ToString();

                        dataFim = DateTime.ParseExact(texto, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);

                        dataFim = dataFim.AddDays(1).AddSeconds(-1);
                    }

                    if (detalheFiltro.Campo.Equals("serie") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        serie = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
                    }

                    if (detalheFiltro.Campo.Equals("despacho") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        numDespacho = Convert.ToInt32(detalheFiltro.Valor[0].ToString());
                    }

                    if (detalheFiltro.Campo.Equals("fluxo") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        fluxo = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
                    }

                    if (detalheFiltro.Campo.Equals("Origem") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        origem = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
                    }

                    if (detalheFiltro.Campo.Equals("UfDcl") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        codigoUfDcl = detalheFiltro.Valor[0].ToString().ToUpperInvariant().Trim();
                    }

                    if (detalheFiltro.Campo.Equals("Destino") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        destino = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
                    }

                    if (detalheFiltro.Campo.Equals("chave") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        chaveCte = detalheFiltro.Valor[0].ToString();
                    }

                    if (detalheFiltro.Campo.Equals("errocte") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        erro = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
                    }

                    if (detalheFiltro.Campo.Equals("Vagao") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        numVagao = detalheFiltro.Valor[0].ToString().Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                    }

                    if (detalheFiltro.Campo.Equals("IncluirNfe") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        incluirNfe = detalheFiltro.Valor[0].ToString().ToUpperInvariant().Equals("S");
                    }

                    if (detalheFiltro.Campo.Equals("Impresso") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        string value = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
                        if (value.Equals("T"))
                        {
                            impresso = null;
                        }
                        else if (value.Equals("S"))
                        {
                            impresso = true;
                        }
                        else
                        {
                            impresso = false;
                        }
                    }
                }
            }

            ResultadoPaginado<CteDto> listaDto = new ResultadoPaginado<CteDto>();

            listaDto = _cteRepository.ObterCteMonitoramento(pagination, dataIni, dataFim, serie, numDespacho, fluxo, chaveCte, erro, origem, destino, numVagao, impresso, codigoUfDcl, incluirNfe);
            listaDto.Items = listaDto.Items.OrderByDescending(c => c.Numero).ToList();

            return listaDto;
        }

        /// <summary>
        /// Autoriza o CR e envia os dados do cte para o sap
        /// </summary>
        /// <param name="listaCte">Lista com Ids dos ctes</param>
        /// <param name="usuario">usuario que autorizou</param>
        /// <returns>Retorna string de erros</returns>
        public StringBuilder AutorizarCancelamentoRejeitado(List<CteDto> listaCte, Usuario usuario)
        {
            StringBuilder erros = new StringBuilder();
            foreach (var ct in listaCte)
            {
                Cte cte = null;
                cte = _cteRepository.ObterPorId(Convert.ToInt32(ct.CteId.ToString()));
                try
                {
                    if (cte.SituacaoAtual != SituacaoCteEnum.AguardandoAnulacao && cte.SituacaoAtual != SituacaoCteEnum.Autorizado)
                    {
                        throw new Exception("Situa��o do CT-e diferente de Aguardando Anula��o/ou cancelamento acima de 168 horas");
                    }

                    if (!VerificarHorasCancelamento(cte, usuario, ct))
                    {
                        if (ct.Descarregado)
                        {
                            throw new Exception("Usu�rio n�o possui permiss�o para cancelar(Vag�o Descarregado).");
                        }
                        else
                        {
                            throw new Exception("Usuario n�o tem permiss�o para executar essa a��o.");
                        }
                    }

                    AutorizarCancelamentoRejeitado(cte, usuario);
                }
                catch (Exception ex)
                {
                    erros.AppendLine("Erro ao cancelar o cte:" + cte.Numero + "  Erro:" + ex.Message + "<br />");
                }
            }

            return erros;
        }

        /// <summary>
        /// Autoriza o CR e envia os dados do cte para o sap
        /// </summary>
        /// <param name="cte">Cte que foi autorizado o CR</param>
        /// <param name="usuario">usuario que autorizou</param>
        public void AutorizarCancelamentoRejeitado(Cte cte, Usuario usuario)
        {
            try
            {
                _cteStatusRepository.InserirCteStatus(cte, usuario, new CteStatusRetorno { Id = 30 });
                cte.SituacaoAtual = SituacaoCteEnum.Cancelado;

                // Insere na interface de envio para o SAP
                InserirInterfaceEnvioSap(cte, new CteStatusRetorno { Id = 220 });
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(cte, "CteService", string.Format("AutorizarCR: {0}", ex.Message), ex);
                throw;
            }
        }

        /// <summary>
        /// Retorna Saldo das Notas
        /// </summary>
        /// <param name="cte">Cte referencia</param>
        public void RetornarSaldoDasNotas(Cte cte)
        {
            var listaDet = _cteDetalheRepository.ObterPorCte(cte);
            foreach (var det in listaDet)
            {
                try
                {
                    var statusNfe = _nfeService.ObterStatusNfe(det.ChaveNfe);
                    if (statusNfe.PesoUtilizado - det.PesoNotaFiscal >= 0)
                    {
                        _nfeService.RetornarSaldoNfe(statusNfe, det.PesoNotaFiscal);
                    }
                    else
                    {
                        throw new TransactionException(string.Format("Peso Utilizado a ser extornado (nota {0}) � maior do que o peso da nota.", det.ChaveNfe));
                    }
                }
                catch (Exception ex)
                {
                    _cteLogService.InserirLogErro(cte, "CteService", string.Format("RetornarSaldoDasNotas: {0}", ex.Message), ex);
                    throw;
                }
            }
        }

        /// <summary>
        /// Autoriza o CR e envia os dados do cte para o sap
        /// </summary>
        /// <param name="cte">Cte que foi autorizado o CR</param>
        /// <param name="usuario">usuario que autorizou</param>
        public void EnviarNotaAnulacaoSAP(Cte cte, Usuario usuario)
        {
            try
            {
                _cteStatusRepository.InserirCteStatus(cte, usuario, new CteStatusRetorno { Id = 36 });

                // Insere na interface de envio para o SAP
                InserirInterfaceEnvioSap(cte, new CteStatusRetorno { Id = 36 });
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(cte, "CteService", string.Format("AutorizarCR: {0}", ex.Message), ex);
                throw;
            }
        }

        /// <summary>
        /// Retorna todos os Cte Autorizados para listagem de Impress�o
        /// </summary>
        /// <param name="pagination"> Paginacao pesquisa</param>
        /// <param name="filtros"> Filtros para pesquisa</param>
        /// <returns>lista de Cte</returns>
        public ResultadoPaginado<CteDto> RetornaCteAnulacao(DetalhesPaginacaoWeb pagination, DetalhesFiltro<object>[] filtros)
        {
            DateTime dataIni = DateTime.Now;
            DateTime dataFim = DateTime.Now;
            string serie = string.Empty;
            int numDespacho = 0;
            string numVagao = string.Empty;
            string erro = string.Empty;
            string fluxo = string.Empty;
            string origem = string.Empty;
            string destino = string.Empty;
            string[] chaveCte = null;
            string codigoUfDcl = string.Empty;
            bool? impresso = null;
            int horasCancelamento = 0;

            if (filtros != null)
            {
                foreach (var detalheFiltro in filtros)
                {
                    if (detalheFiltro.Campo.Equals("dataInicial") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        string texto = detalheFiltro.Valor[0].ToString();

                        dataIni = DateTime.ParseExact(texto, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                    }

                    if (detalheFiltro.Campo.Equals("dataFinal") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        string texto = detalheFiltro.Valor[0].ToString();

                        dataFim = DateTime.ParseExact(texto, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);

                        dataFim = dataFim.AddDays(1).AddSeconds(-1);
                    }

                    if (detalheFiltro.Campo.Equals("serie") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        serie = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
                    }

                    if (detalheFiltro.Campo.Equals("despacho") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        numDespacho = Convert.ToInt32(detalheFiltro.Valor[0].ToString());
                    }

                    if (detalheFiltro.Campo.Equals("fluxo") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        fluxo = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
                    }

                    if (detalheFiltro.Campo.Equals("Origem") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        origem = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
                    }

                    if (detalheFiltro.Campo.Equals("UfDcl") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        codigoUfDcl = detalheFiltro.Valor[0].ToString().ToUpperInvariant().Trim();
                    }

                    if (detalheFiltro.Campo.Equals("Destino") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        destino = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
                    }

                    if (detalheFiltro.Campo.Equals("chave") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        chaveCte = detalheFiltro.Valor[0].ToString().Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                    }

                    if (detalheFiltro.Campo.Equals("Vagao") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        numVagao = detalheFiltro.Valor[0].ToString();
                    }

                    int.TryParse(RecuperarValorConfGeral(_chaveHorasCancelamentoCte), out horasCancelamento);
                }
            }

            ResultadoPaginado<CteDto> listaDto = new ResultadoPaginado<CteDto>();

            listaDto = _cteRepository.ObterCteAnulacao(pagination, dataIni, dataFim, serie, numDespacho, fluxo, chaveCte, origem, destino, numVagao, horasCancelamento);
            listaDto.Items = listaDto.Items.OrderByDescending(c => c.Numero).ToList();

            return listaDto;
        }

        /// <summary>
        /// Retorna todos os Cte Autorizados para listagem de Impress�o
        /// </summary>
        /// <param name="pagination"> Paginacao pesquisa</param>
        /// <param name="filtros"> Filtros para pesquisa</param>
        /// <returns>lista de Cte</returns>
        public ResultadoPaginado<CteDto> RetornaCteManutencaoPendente(DetalhesPaginacaoWeb pagination, DetalhesFiltro<object>[] filtros)
        {
            DateTime dataIni = DateTime.Now;
            DateTime dataFim = DateTime.Now;
            string serie = string.Empty;
            int numDespacho = 0;
            string numVagao = string.Empty;
            string erro = string.Empty;
            string fluxo = string.Empty;
            string origem = string.Empty;
            string destino = string.Empty;
            string chaveCte = string.Empty;
            string codigoUfDcl = string.Empty;
            bool? impresso = null;

            if (filtros != null)
            {
                foreach (var detalheFiltro in filtros)
                {
                    if (detalheFiltro.Campo.Equals("dataInicial") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        string texto = detalheFiltro.Valor[0].ToString();

                        dataIni = DateTime.ParseExact(texto, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                    }

                    if (detalheFiltro.Campo.Equals("dataFinal") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        string texto = detalheFiltro.Valor[0].ToString();

                        dataFim = DateTime.ParseExact(texto, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);

                        dataFim = dataFim.AddDays(1).AddSeconds(-1);
                    }

                    if (detalheFiltro.Campo.Equals("serie") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        serie = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
                    }

                    if (detalheFiltro.Campo.Equals("despacho") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        numDespacho = Convert.ToInt32(detalheFiltro.Valor[0].ToString());
                    }

                    if (detalheFiltro.Campo.Equals("fluxo") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        fluxo = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
                    }

                    if (detalheFiltro.Campo.Equals("Origem") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        origem = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
                    }

                    if (detalheFiltro.Campo.Equals("UfDcl") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        codigoUfDcl = detalheFiltro.Valor[0].ToString().ToUpperInvariant().Trim();
                    }

                    if (detalheFiltro.Campo.Equals("Destino") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        destino = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
                    }

                    if (detalheFiltro.Campo.Equals("chave") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        chaveCte = detalheFiltro.Valor[0].ToString();
                    }

                    if (detalheFiltro.Campo.Equals("Vagao") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        numVagao = detalheFiltro.Valor[0].ToString();
                    }
                }
            }

            ResultadoPaginado<CteDto> listaDto = new ResultadoPaginado<CteDto>();

            listaDto = _cteRepository.ObterCteManutencaoPendente(pagination, dataIni, dataFim, serie, numDespacho, fluxo, chaveCte, origem, destino, numVagao, codigoUfDcl);
            listaDto.Items = listaDto.Items.OrderByDescending(c => c.Numero).ToList();

            return listaDto;
        }

        /// <summary>
        /// Retorna todos os Cte para monitoramento
        /// </summary>
        /// <param name="pagination">Detalhes da Pagina��o</param>
        /// <param name="dataInicial">Data inicial de pesquisa</param>
        /// <param name="dataFinal">Data final de pesquisa</param>
        /// <param name="serie">N�mero da s�rie</param>
        /// <param name="despacho">N�mero do Despacho</param>
        /// <param name="codFluxo">C�digo do fluxo comercial</param>
        /// <param name="chaveCte">Chave do Cte informada</param>
        /// <param name="origem"> Origem do Cte</param>
        /// <param name="destino"> Destino do Cte</param>
        /// <param name="numVagao"> Numero do Vagao</param>b
        /// <param name="codigoUfDcl">Codigo controle do despacho</param>
        /// <returns>lista de Cte</returns>
        public ResultadoPaginado<CteDto> RetornaCteManutencaoPendente(DetalhesPaginacao pagination, DateTime dataInicial, DateTime dataFinal, string serie, int despacho, string codFluxo, string chaveCte, string origem, string destino, string numVagao, string codigoUfDcl)
        {
            ResultadoPaginado<CteDto> listaDto = new ResultadoPaginado<CteDto>();

            listaDto = _cteRepository.ObterCteManutencaoPendente(pagination, dataInicial, dataFinal, serie, despacho, codFluxo, chaveCte, origem, destino, numVagao, codigoUfDcl);
            listaDto.Items = listaDto.Items.OrderByDescending(c => c.Numero).ToList();

            return listaDto;
        }

        /// <summary>
        /// Retorna todos os Cte Autorizados para listagem de Impress�o
        /// </summary>
        /// <param name="pagination"> Paginacao pesquisa</param>
        /// <param name="filtros"> Filtros para pesquisa</param>
        /// <returns>lista de Cte</returns>
        public ResultadoPaginado<CteDto> RetornaCteRelatorioErro(DetalhesPaginacaoWeb pagination, DetalhesFiltro<object>[] filtros)
        {
            DateTime dataIni = DateTime.Now;
            DateTime dataFim = DateTime.Now;
            string fluxo = string.Empty;
            string origem = string.Empty;
            string destino = string.Empty;

            if (filtros != null)
            {
                foreach (var detalheFiltro in filtros)
                {
                    if (detalheFiltro.Campo.Equals("dataInicial") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        string texto = detalheFiltro.Valor[0].ToString();

                        dataIni = DateTime.ParseExact(texto, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                    }

                    if (detalheFiltro.Campo.Equals("dataFinal") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        string texto = detalheFiltro.Valor[0].ToString();

                        dataFim = DateTime.ParseExact(texto, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);

                        dataFim = dataFim.AddDays(1).AddSeconds(-1);
                    }

                    if (detalheFiltro.Campo.Equals("fluxo") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        fluxo = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
                    }

                    if (detalheFiltro.Campo.Equals("Origem") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        origem = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
                    }

                    if (detalheFiltro.Campo.Equals("Destino") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        destino = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
                    }
                }
            }

            ResultadoPaginado<CteDto> listaDto = new ResultadoPaginado<CteDto>();

            listaDto = _cteRepository.ObterCteRelatorioErro(pagination, dataIni, dataFim, fluxo, origem, destino);
            listaDto.Items = listaDto.Items.OrderByDescending(c => c.Numero).ToList();

            return listaDto;
        }

        /// <summary>
        /// Retorna todos os Cte Autorizados para listagem de Impress�o
        /// </summary>
        /// <param name="pagination"> Paginacao pesquisa</param>
        /// <param name="filtros"> Filtros para pesquisa</param>
        /// <returns>lista de Cte</returns>
        public ResultadoPaginado<CteDto> RetornaCteGerarArquivoLote(DetalhesPaginacaoWeb pagination, DetalhesFiltro<object>[] filtros)
        {
            DateTime dataIni = DateTime.Now;
            DateTime dataFim = DateTime.Now;
            string serie = string.Empty;
            int numDespacho = 0;
            string[] numVagao = null;
            string fluxo = string.Empty;
            string origem = string.Empty;
            string destino = string.Empty;
            string[] chaveCte = null;
            string codigoUfDcl = string.Empty;

            if (filtros != null)
            {
                foreach (var detalheFiltro in filtros)
                {
                    if (detalheFiltro.Campo.Equals("dataInicial") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        string texto = detalheFiltro.Valor[0].ToString();

                        dataIni = DateTime.ParseExact(texto, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                    }

                    if (detalheFiltro.Campo.Equals("dataFinal") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        string texto = detalheFiltro.Valor[0].ToString();

                        dataFim = DateTime.ParseExact(texto, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);

                        dataFim = dataFim.AddDays(1).AddSeconds(-1);
                    }

                    if (detalheFiltro.Campo.Equals("serie") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        serie = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
                    }

                    if (detalheFiltro.Campo.Equals("despacho") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        numDespacho = Convert.ToInt32(detalheFiltro.Valor[0].ToString());
                    }

                    if (detalheFiltro.Campo.Equals("fluxo") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        fluxo = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
                    }

                    if (detalheFiltro.Campo.Equals("Origem") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        origem = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
                    }

                    if (detalheFiltro.Campo.Equals("UfDcl") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        codigoUfDcl = detalheFiltro.Valor[0].ToString().ToUpperInvariant().Trim();
                    }

                    if (detalheFiltro.Campo.Equals("Destino") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        destino = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
                    }

                    if (detalheFiltro.Campo.Equals("chave") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        chaveCte = detalheFiltro.Valor[0].ToString().Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                    }

                    if (detalheFiltro.Campo.Equals("Vagao") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        numVagao = detalheFiltro.Valor[0].ToString().Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                    }
                }
            }

            ResultadoPaginado<CteDto> listaDto = new ResultadoPaginado<CteDto>();

            listaDto = _cteRepository.ObterCteGerarArquivoLote(pagination, dataIni, dataFim, serie, numDespacho, fluxo, chaveCte, origem, destino, numVagao, codigoUfDcl);

            return listaDto;
        }

        /// <summary>
        /// Retorna todos os Cte Autorizados para listagem de Impress�o
        /// </summary>
        /// <param name="pagination"> Paginacao pesquisa</param>
        /// <param name="filtros"> Filtros para pesquisa</param>
        /// <returns>lista de Cte</returns>
        public ResultadoPaginado<CteDto> RetornaCteImpressao(DetalhesPaginacaoWeb pagination, DetalhesFiltro<object>[] filtros)
        {
            DateTime dataIni = DateTime.Now;
            DateTime dataFim = DateTime.Now;
            int serie = 0;
            int numDespacho = 0;
            string numVagao = string.Empty;
            string erro = string.Empty;
            string fluxo = string.Empty;
            string origem = string.Empty;
            string destino = string.Empty;
            string chaveCte = string.Empty;

            if (filtros != null)
            {
                foreach (var detalheFiltro in filtros)
                {
                    if (detalheFiltro.Campo.Equals("dataInicial") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        string texto = detalheFiltro.Valor[0].ToString();

                        dataIni = DateTime.ParseExact(texto, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                    }

                    if (detalheFiltro.Campo.Equals("dataFinal") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        string texto = detalheFiltro.Valor[0].ToString();

                        dataFim = DateTime.ParseExact(texto, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);

                        dataFim = dataFim.AddDays(1).AddSeconds(-1);
                    }

                    if (detalheFiltro.Campo.Equals("serie") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        serie = Convert.ToInt32(detalheFiltro.Valor[0].ToString());
                    }

                    if (detalheFiltro.Campo.Equals("despacho") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        numDespacho = Convert.ToInt32(detalheFiltro.Valor[0].ToString());
                    }

                    if (detalheFiltro.Campo.Equals("fluxo") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        fluxo = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
                    }

                    if (detalheFiltro.Campo.Equals("Origem") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        origem = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
                    }

                    if (detalheFiltro.Campo.Equals("Destino") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        destino = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
                    }

                    if (detalheFiltro.Campo.Equals("chave") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        chaveCte = detalheFiltro.Valor[0].ToString();
                    }

                    if (detalheFiltro.Campo.Equals("errocte") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        erro = detalheFiltro.Valor[0].ToString();
                    }

                    if (detalheFiltro.Campo.Equals("Vagao") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        numVagao = detalheFiltro.Valor[0].ToString();
                    }
                }
            }

            ResultadoPaginado<CteDto> listaDto = new ResultadoPaginado<CteDto>();

            listaDto = _cteRepository.ObterCteImpressao(pagination, dataIni, dataFim, serie, numDespacho, fluxo, chaveCte, erro, origem, destino, numVagao);

            return listaDto;
        }

        /// <summary>
        /// Retorna todos os Cte filhos do cte atual
        /// </summary>
        /// <param name="cteVigente">Id do Cte vigente</param>
        /// <returns>lista de Cte agrupamento</returns>
        public IList<CteDto> RetornaCteMonitoramentoAgrupamento(int cteVigente)
        {
            IList<CteDto> listaDto = new List<CteDto>();

            CteDto dto = null;

            DespachoTranslogic despacho = null;
            ItemDespacho itemDespacho = null;
            DetalheCarregamento detalheCarregamento = null;
            Carregamento carregamento = null;
            FluxoComercial fluxoComercial = null;
            EstacaoMae estacaoMaeOrigem = null;
            EstacaoMae estacaoMaeDestino = null;
            SerieDespacho serieDespacho = null;
            SerieDespachoUf serieDespachoUf = null;

            Cte cteFilho = _cteRepository.ObterPorId(cteVigente);

            IList<CteAgrupamento> listaCte = _cteAgrupamentoRepository.ObterAgrupamentoPorCteFilho(cteFilho);

            // Remove o cte raiz da lista
            listaCte = listaCte.Where(c => c.CteRaiz != c.CteFilho).ToList();

            IList<CteStatus> v = null;
            Cte cteUltimo = null;

            foreach (CteAgrupamento cteagrupamento in listaCte)
            {
                cteUltimo = _cteRepository.ObterPorId(cteagrupamento.CteFilho.Id);

                CteStatus status = cteUltimo.ListaStatus.OrderByDescending(c => c.Id).FirstOrDefault();

                despacho = cteUltimo.Despacho;

                // para ser listado tem que ter despacho 
                if (despacho != null)
                {
                    serieDespacho = despacho.SerieDespacho;
                    serieDespachoUf = despacho.SerieDespachoUf;

                    itemDespacho = _itemDespachoRepository.ObterItemDespachoPorDespacho(despacho);

                    if (itemDespacho != null)
                    {
                        detalheCarregamento = itemDespacho.DetalheCarregamento;

                        if (detalheCarregamento != null)
                        {
                            carregamento = detalheCarregamento.Carregamento;

                            if (carregamento != null)
                            {
                                fluxoComercial = carregamento.FluxoComercialCarregamento;
                            }
                        }
                    }

                    if (fluxoComercial != null)
                    {
                        estacaoMaeOrigem = fluxoComercial.Origem;
                        estacaoMaeDestino = fluxoComercial.Destino;

                        dto = new CteDto();
                        dto.CteId = cteUltimo.Id ?? 0;
                        dto.Fluxo = fluxoComercial.Codigo;
                        dto.Origem = estacaoMaeOrigem != null ? estacaoMaeOrigem.Codigo : string.Empty;
                        dto.Destino = estacaoMaeDestino != null ? estacaoMaeDestino.Codigo : string.Empty;
                        dto.Mercadoria = fluxoComercial.Mercadoria.Apelido;
                        dto.Cte = cteUltimo.Serie + "-" + cteUltimo.Numero;
                        dto.Chave = cteUltimo.Chave;
                        dto.SerieDesp5 = serieDespacho != null ? serieDespacho.SerieDespachoNum : string.Empty;
                        dto.NumDesp5 = despacho.NumeroDespacho ?? 0;
                        dto.SerieDesp6 = serieDespachoUf != null ? serieDespachoUf.NumeroSerieDespacho : 0;
                        dto.NumDesp6 = despacho.NumeroDespacho ?? 0;
                        dto.DataEmissao = cteUltimo.DataHora;
                        dto.CodigoUsuario = status.Usuario != null ? status.Usuario.Codigo : "...";
                        if (estacaoMaeOrigem != null)
                        {
                            dto.FerroviaOrigem = estacaoMaeOrigem.EmpresaConcessionaria.DescricaoResumida;
                            if (estacaoMaeOrigem.Municipio.Estado != null)
                            {
                                dto.UfOrigem = estacaoMaeOrigem.Municipio.Estado.Sigla;
                            }
                        }

                        if (estacaoMaeDestino != null)
                        {
                            dto.FerroviaDestino = estacaoMaeDestino.EmpresaConcessionaria.DescricaoResumida;
                            if (estacaoMaeDestino.Municipio.Estado != null)
                            {
                                dto.UfDestino = estacaoMaeDestino.Municipio.Estado.Sigla;
                            }
                        }

                        dto.Impresso = cteUltimo.Impresso;
                        dto.SituacaoCte = cteUltimo.SituacaoAtual;
                        dto.CodigoErro = string.Format("{0} - {1}", status.CteStatusRetorno.Id, status.CteStatusRetorno.Id == 14 ? status.XmlRetorno : status.CteStatusRetorno.Id == 4001 ? status.Cte.XmlAutorizacao : status.CteStatusRetorno.Descricao);
                        dto.StatusCte = status.CteStatusRetorno.Id ?? 0;
                        dto.ArquivoPdfGerado = cteUltimo.ArquivoPdfGerado;
                        listaDto.Add(dto);
                    }
                }
            }

            return listaDto;
        }

        /// <summary>
        /// Retorna todos os Cte filhos do cte atual
        /// </summary>
        /// <param name="cte">Id do Cte para retorno do status</param>
        /// <returns>lista de Cte agrupamento</returns>
        public IList<CteStatus> RetornaCteMonitoramentoStatusCte(int cte)
        {
            IList<CteStatus> cteStatus = _cteRepository.ObterPorId(cte).ListaStatus.OrderByDescending(g => g.DataHora).ToList();

            return cteStatus;
        }

        /// <summary>
        /// Realiza o Reemiss�o dos Ctes selecionados
        /// </summary>
        /// <param name="ids">ids dos ctes</param>
        /// <param name="usuario">Usu�rio atual</param>
        /// <param name="mensagemErro">Mensagem de erro utilizados para informar na tela </param>
        /// <returns>resultado do cancelamento</returns>
        public bool ReemissaoCtes(int[] ids, Usuario usuario, out string mensagemErro)
        {
            mensagemErro = string.Empty;
            try
            {
                IList<Cte> listaCte = new List<Cte>();

                foreach (int id in ids)
                {
                    listaCte.Add(_cteRepository.ObterPorId(id));
                }

                foreach (Cte cte in listaCte)
                {
                    var arrayStatus = new int[] { 24, 25, 37 };
                    IList<CteStatus> listaStatus = _cteStatusRepository.ObterPorCte(cte);
                    int qtdeCancelado = listaStatus.Count(c => c.CteStatusRetorno.Id == 24);
                    int qtdeInutilizado = listaStatus.Count(c => c.CteStatusRetorno.Id == 25);

                    int? ultimoStatus = listaStatus.OrderByDescending(x => x.DataHora).Select(x => x.CteStatusRetorno.Id).FirstOrDefault(x => arrayStatus.Contains(x.HasValue ? x.Value : 0));

                    if (ultimoStatus.HasValue && ultimoStatus == 37)
                    {
                        cte.SituacaoAtual = SituacaoCteEnum.PendenteCorrecaoEnvio;
                        _cteStatusRepository.InserirCteStatus(cte, usuario, new CteStatusRetorno { Id = 37 });
                    }
                    else if (qtdeCancelado > 0)
                    {
                        cte.SituacaoAtual = SituacaoCteEnum.AguardandoCancelamento;
                        _cteStatusRepository.InserirCteStatus(cte, usuario, new CteStatusRetorno { Id = 19 });
                    }
                    else if (qtdeInutilizado > 0)
                    {
                        cte.SituacaoAtual = SituacaoCteEnum.AguardandoInutilizacao;
                        _cteStatusRepository.InserirCteStatus(cte, usuario, new CteStatusRetorno { Id = 18 });
                    }
                    else
                    {
                        cte.SituacaoAtual = SituacaoCteEnum.PendenteArquivoEnvio;
                        _cteStatusRepository.InserirCteStatus(cte, usuario, new CteStatusRetorno { Id = 10 });
                    }

                    _cteRepository.InserirOuAtualizar(cte);

                    // Insere no log do cte
                    _cteLogService.InserirLogInfo(cte, "CteService", "Reemiss�o do cte");
                }
            }
            catch (Exception ex)
            {
                mensagemErro = ex.Message;
                return false;
            }

            return true;
        }

        /// <summary>
        /// Verifica se a Cte pode ser cancelada
        /// </summary>
        /// <param name="idCte">Id da Cte para verificar</param>
        /// <returns>Verdadeiro se a Cte pode ser cancelada, sen�o falso</returns>
        public bool PodeCancelar(int idCte)
        {
            return _cteRepository.PodeCancelar(idCte);
        }

        /// <summary>
        /// Realiza o Reemiss�o dos Ctes selecionados
        /// </summary>
        /// <param name="ids">ids dos ctes</param>
        /// <param name="usuario">Usu�rio atual</param>
        public void ReemissaoCancelamentoCtes(int[] ids, Usuario usuario)
        {
            IList<Cte> listaCte = new List<Cte>();

            foreach (int id in ids)
            {
                listaCte.Add(_cteRepository.ObterPorId(id));
            }

            foreach (Cte cte in listaCte)
            {
                cte.SituacaoAtual = SituacaoCteEnum.AguardandoCancelamento;
                _cteRepository.InserirOuAtualizar(cte);
                _cteStatusRepository.InserirCteStatus(cte, usuario, new CteStatusRetorno { Id = 26 });
                // Insere no log do cte
                _cteLogService.InserirLogInfo(cte, "CteService", "Reemiss�o do Cancelamento do cte");
            }
        }

        /// <summary>
        /// Realiza a gera��o dos arquivos .zip
        /// </summary>
        /// <param name="arrIds">ids dos ctes</param>
        /// <param name="tipoArquivoEnvio"> Tipo do arquivo</param>
        /// <returns>resultado do cancelamento</returns>
        public MemoryStream GerarArquivoZipLote(string[] arrIds, TipoArquivoEnvioEnum tipoArquivoEnvio)
        {
            try
            {
                Dictionary<string, Stream> arquivos = new Dictionary<string, Stream>();

                // Zip dos arquivos Pdf
                if (tipoArquivoEnvio == TipoArquivoEnvioEnum.Pdf)
                {
                    Stream arquivoPdf = ObterArquivoPdf(arrIds);

                    arquivos.Add("Dacte.pdf", arquivoPdf);
                }

                // Zip dos arquivos Xml
                if (tipoArquivoEnvio == TipoArquivoEnvioEnum.Xml)
                {
                    // percorre os Ctes
                    foreach (string id in arrIds)
                    {
                        // Obtem o Arquivo
                        CteArquivo file = ObterArquivoCte(int.Parse(id));
                        Cte cte = _cteRepository.ObterPorId(int.Parse(id));

                        if (file != null && file.ArquivoXml != null)
                        {
                            arquivos.Add(string.Concat(cte.Chave, ".xml"), Tools.StringToStream(file.ArquivoXml));
                        }
                    }
                }

                return CreateToMemoryStream(arquivos);
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Verifica se uma Cte pode ser inutilizada
        /// </summary>
        /// <param name="idCte">Id da Cte para inutilizar</param>
        /// <returns>Verdadeiro se pode inutilizar, falso se n�o pode</returns>
        public bool PodeInutilzarCte(int idCte)
        {
            return _cteRepository.PodeInutilizar(idCte);
        }

        /// <summary>
        /// Realiza o Cancelamento dos Ctes
        /// </summary>
        /// <param name="ids">ids dos ctes</param>
        /// <param name="usuario">Usu�rio atual</param>
        /// <returns>resultado do cancelamento</returns>
        public bool SalvarInutilizacaoCtes(int[] ids, Usuario usuario)
        {
            var ctesNaoInutilizados = new List<string>();

            foreach (int id in ids)
            {
                Cte cte = _cteRepository.ObterPorId(id);
                IList<CteAgrupamento> listaCteAgrupamento = _cteAgrupamentoRepository.ObterAgrupamentoPorCteFilho(cte);

                foreach (CteAgrupamento cteAgrupamento in listaCteAgrupamento)
                {
                    // -> Se o CTE j� foi autorizado em algum momento, n�o pode ser invalidado.
                    if (!JaFoiAutorizadoAlgumaVez(cteAgrupamento.CteFilho))
                    {
                        // mudando Status CTE para Aguardando Inutiliza��o (18)
                        MudarSituacaoCte(cteAgrupamento.CteFilho, SituacaoCteEnum.AguardandoInutilizacao, usuario, new CteStatusRetorno { Id = 18 }, "Arquivo Cte alterado para aguardando inutiliza��o", null);
                    }
                    else
                    {
                        ctesNaoInutilizados.Add(cteAgrupamento.CteFilho.Numero);
                    }
                }
            }

            if (ctesNaoInutilizados.Count > 0)
            {
                throw new TransactionException(string.Format("Os seguintes CTEs n�o foram inutilizados, pois j� foram autorizados em algum momento: {0}. Primeiramente devem ser cancelados.", string.Join(",", ctesNaoInutilizados)));
            }

            return true;
        }

        /// <summary>
        /// Obt�m a lista de Ctes a serem processados para recebimento (ctes que est�o na interface e n�o foram processados)
        /// </summary>
        /// <param name="indAmbienteSefaz">Indica qual ambiente da SEFAZ 1-produ��o / 2-homologa��o</param>
        /// <returns>Lista de Ctes</returns>
        public IList<CteInterfaceRecebimentoConfig> ObterCtesProcessarRecebimento(int indAmbienteSefaz)
        {
            try
            {
                IList<CteInterfaceRecebimentoConfig> listaPooling = _cteInterfaceRecebimentoConfigRepository.ObterNaoProcessados(indAmbienteSefaz);
                foreach (CteInterfaceRecebimentoConfig itemInterface in listaPooling)
                {
                    itemInterface.DataUltimaLeitura = DateTime.Now;
                    _cteInterfaceRecebimentoConfigRepository.Atualizar(itemInterface);
                }

                return listaPooling;
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(null, "CteService", string.Format("ObterCtesProcessarRecebimento: {0}", ex.Message), ex);
                throw;
            }
        }

        /// <summary>
        /// Obter os Ctes para serem enviados para o SAP
        /// </summary>
        /// <param name="indAmbienteSefaz">Indica qual ambiente da SEFAZ 1-produ��o / 2-homologa��o</param>
        /// <returns>Retorna a lista de ctes a serem enviados para o sap</returns>
        public IList<CteInterfaceEnvioSap> ObterCtesProcessarEnvioSap(int indAmbienteSefaz)
        {
            try
            {
                IList<CteInterfaceEnvioSap> listaPooling = _cteInterfaceEnvioSapRepository.ObterNaoProcessados(indAmbienteSefaz);
                foreach (CteInterfaceEnvioSap itemInterface in listaPooling)
                {
                    itemInterface.DataUltimaLeitura = DateTime.Now;
                    _cteInterfaceEnvioSapRepository.Atualizar(itemInterface);
                }

                return listaPooling;
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(null, "CteService", string.Format("ObterCtesProcessarEnvioSap: {0}", ex.Message), ex);
                throw;
            }
        }

        /// <summary>
        /// Obt�m o di�rio de bordo da Cte por id de Cte
        /// </summary>
        /// <param name="pagination">Detalhes da pagina��o</param>
        /// <param name="idCte">Id da Cte para se obter o di�rio de bordo</param>
        /// <returns>Lista de di�rio de bordo para a Cte</returns>
        public ResultadoPaginado<DiarioBordoCte> ObterDiarioBordoCte(DetalhesPaginacaoWeb pagination, int idCte)
        {
            return _diarioBordoCteRepository.ObterDiarioBordoCte(pagination, idCte);
        }

        /// <summary>
        /// Grava um novo registro no di�rio de bordo para a Cte selecionada
        /// </summary>
        /// <param name="acao">A��o ou mensagem informada</param>
        /// <param name="idCte">Id da Cte selecionada</param>
        /// <param name="usuario">Usuario que est� inserindo o registro no di�rio</param>
        /// <returns>Resultado com o sucesso ou n�o da opera��o</returns>
        public bool GravarDiarioBordoCte(string acao, int idCte, Usuario usuario)
        {
            var cte = ObterCtePorId(idCte);

            if (cte == null)
            {
                throw new Exception("Cte n�o foi localizada");
            }

            return _diarioBordoCteRepository.GravarDiarioBordoCte(acao, cte, usuario);
        }

        /// <summary>
        /// verifica no historico de status do CTE se em algum momento ele j� foi autorizado
        /// </summary>
        /// <param name="cte">Cte para ser verificado</param>
        /// <returns>valor boleano</returns>
        public bool JaFoiAutorizadoAlgumaVez(Cte cte)
        {
            return _cteStatusRepository.TemStatusAutorizado(cte);
        }

        /// <summary>
        /// verifica no historico de status do CTE (EAR) se em algum momento ele j� foi autorizado
        /// </summary>
        /// <param name="cte">Cte para ser verificado</param>
        /// <returns>valor boleano</returns>
        public bool EARJaFoiAutorizadoAlgumaVez(Cte cte)
        {
            var cteOriginal = _cteRepository.ObterPorIdCte(cte.Id.Value);

            if (cteOriginal.SituacaoAtual == SituacaoCteEnum.ErroAutorizadoReEnvio)
            {
                return _cteStatusRepository.TemStatusAutorizado(cte);
            }

            return false;
        }

        /// <summary>
        /// Dummy testar o insert no TbDatabaseInput
        /// </summary>
        /// <param name="cte">Cte de teste</param>
        public void Dummy(Cte cte)
        {
            // Atualiza o flag de PDF gerado
            // cte.ArquivoXmlGerado = true;
            _cteRepository.AtualizarArquivoGeradoCte(cte, TipoArquivoEnvioEnum.Pdf);
            // Insere na interface de envio de email
            InserirInterfaceEnvioEmail(cte, null);

            // InserirInterfaceArquivoPdf(cte);

            // Insere na interface de envio para o SAP
            // InserirInterfaceEnvioSap(cte);
            // string valor = GerarHtmlEmail(cte);
            /*
            IList<string> lista = new List<string>();
            lista.Add("marcelo");
            lista.Add("carolina");
            lista.Add("gabriela");
            lista.Add("juliana");

            IList<string> listaAux = new List<string>();
            listaAux.Add("MARCELO");
            listaAux.Add("livia");
            listaAux.Add("juliana");

            // Encontra os nomes comuns
            var nomes = lista.Select(g => g.ToUpper()).Intersect(listaAux.Select(s => s.ToUpper()));
            foreach (string nome in nomes)
            {
                                            string teste = nome;
            }

            var nomeAux = lista.Except(listaAux);

            foreach (string s in nomeAux)
            {
                                            string teste = s;
            }

            var nomeDif = listaAux.Except(lista);

            foreach (string s in nomeDif)
            {
                                            string teste = s;
            }
            */
        }

        /// <summary>
        /// Insere no pooling de recebimento do Cte
        /// </summary>
        /// <param name="listaCtesNaoProcessados"> The lista ctes nao processados. </param>
        /// <param name="hostName"> The host name. </param>
        public void InserirPoolingRecebimentoCte(IList<CteInterfaceRecebimentoConfig> listaCtesNaoProcessados, string hostName)
        {
            foreach (CteInterfaceRecebimentoConfig itemRecebimento in listaCtesNaoProcessados)
            {
                try
                {
                    Cte cte = itemRecebimento.Cte;
                    CteRecebimentoPooling crp = new CteRecebimentoPooling();
                    crp.Id = cte.Id;
                    crp.IdListaConfig = itemRecebimento.IdListaConfig;
                    crp.DataHora = DateTime.Now;
                    crp.HostName = hostName;

                    // Insere o cte no pooling
                    _cteRecebimentoPoolingRepository.Inserir(crp);

                    // Insere no log do cte
                    _cteLogService.InserirLogInfo(cte, "CteService", "Adicionado no pooling de recebimento");
                }
                catch (Exception e)
                {
                    // Limpa a sess�o para processar o pr�ximo
                    _cteEnvioPoolingRepository.ClearSession();
                }
            }
        }

        /// <summary>
        /// Insere o Cte no pooling de envio para SAP
        /// </summary>
        /// <param name="listaCtesNaoProcessados"> The lista ctes nao processados</param>
        /// <param name="hostName"> The host name. </param>
        public void InserirPoolingEnvioSapCte(IList<CteInterfaceEnvioSap> listaCtesNaoProcessados, string hostName)
        {
            foreach (CteInterfaceEnvioSap itemEnvio in listaCtesNaoProcessados)
            {
                try
                {
                    Cte cte = itemEnvio.Cte;
                    CteSapPooling csp = new CteSapPooling();
                    csp.Id = cte.Id;
                    csp.DataHora = DateTime.Now;
                    csp.HostName = hostName;
                    csp.TipoOperacao = itemEnvio.TipoOperacao;

                    // Insere o cte no pooling
                    _cteSapPoolingRepository.Inserir(csp);

                    // Insere no log do cte
                    _cteLogService.InserirLogInfo(cte, "CteService", "Adicionado no pooling de envio para o SAP");
                }
                catch (Exception e)
                {
                    // Limpa a sess�o para processar o pr�ximo
                    _cteSapPoolingRepository.ClearSession();
                }
            }
        }

        /// <summary>
        /// Obt�m os ctes que est�o no pool de recebimento pelo hostname
        /// </summary>
        /// <param name="hostName">Nome do Host</param>
        /// <returns>Lista de Ctes</returns>
        public IList<CteRecebimentoPooling> ObterPoolingRecebimentoPorServidor(string hostName)
        {
            try
            {
                return _cteRecebimentoPoolingRepository.ObterListaRecebimentoPorHost(hostName);
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(null, "CteService", string.Format("ObterPoolingRecebimentoPorServidor ({0}): {1}", hostName, ex.Message), ex);
                throw;
            }
        }

        /// <summary>
        /// Obt�m os ctes que est�o no pool de envio para o SAP pelo hostname
        /// </summary>
        /// <param name="hostName">Nome do Host</param>
        /// <returns>Lista de Ctes</returns>
        public IList<CteSapPooling> ObterPoolingEnvioSapPorServidor(string hostName)
        {
            try
            {
                return _cteSapPoolingRepository.ObterListaRecebimentoPorHost(hostName);
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(null, "CteService", string.Format("ObterPoolingSapPorServidor ({0}): {1}", hostName, ex.Message), ex);
                throw;
            }
        }

        /// <summary>
        /// Muda a situacao do cte e insere o status retorno e grava o log
        /// </summary>
        /// <param name="cte">cte que ser� atualizado</param>
        /// <param name="situacao">nova situacao do cte</param>
        /// <param name="usuario">usu�rio que est� mudando a situa��o</param>
        /// <param name="statusRetorno">status retorno</param>
        /// <param name="mensagemLog">mensagem de log que ser� gravada no log do Cte</param>
        /// <param name="mensagemStatusRetorno">Mensagem do status de retorno</param>
        public void MudarSituacaoCte(Cte cte, SituacaoCteEnum situacao, Usuario usuario, CteStatusRetorno statusRetorno, string mensagemLog, Exception mensagemStatusRetorno)
        {
            cte.SituacaoAtual = situacao;
            _cteRepository.Atualizar(cte);

            if (usuario == null)
            {
                // Pega o usuario default da conf geral
                usuario = ObterUsuarioRobo();
            }

            _cteStatusRepository.InserirCteStatus(cte, usuario, statusRetorno, mensagemStatusRetorno == null ? string.Empty : mensagemStatusRetorno.Message);

            // Insere no log do cte
            _cteLogService.InserirLogInfo(cte, "CteService", string.Format("MudarSituacaoCte: {0}", mensagemLog));
        }

        /// <summary>
        /// Atualiza as colunas de ICMS na tabela do CTE para um CTE complementar
        /// </summary>
        /// <param name="cte">cte a ser atualizado</param>
        /// <param name="aliquota">valor da aliquota</param>
        /// <param name="somaBaseCalculoIcms">soma da base da c�uculo do ICMS</param>
        /// <param name="somaValorIcms">Soma dos valores do ICMS</param>
        /// <param name="valorDiferenca">valor Diferenca dos valores do Cte</param>
        public void AtualizaIcmsCte(Cte cte, double aliquota, double somaBaseCalculoIcms, double somaValorIcms, double valorDiferenca)
        {
            try
            {
                cte.BaseCalculoIcms = somaBaseCalculoIcms;
                cte.ValorIcms = somaValorIcms;
                cte.PercentualAliquotaIcms = aliquota;
                cte.ValorCte = valorDiferenca;
                _cteRepository.Atualizar(cte);

                // Insere no log do cte
                _cteLogService.InserirLogErro(cte, "CteService", string.Format("AtualizaIcmsCte: {0}", "Atualizado Icms do Cte"));
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(cte, "AtualizaIcmsCte", string.Format("AtualizaIcmsCte: {0}", ex.Message), ex);
            }
        }

        /// <summary>
        /// Processa o retorno do Cte pelo item do pooling
        /// </summary>
        /// <param name="itemRecebimento">Item do pooling</param>
        /// <returns>Verdadeiro se conseguiu processar o retorno</returns>
        public bool ProcessarCteRetorno(CteRecebimentoPooling itemRecebimento)
        {
            try
            {
                Vw209ConsultaCte consultaCte = _filaProcessamentoService.ObterRetornoCteProcessadoPorIdLista(itemRecebimento.IdListaConfig);

                if (consultaCte != null)
                {
                    Cte cteAux = _cteRepository.ObterCtePorIdHql(itemRecebimento.Id.Value);

                    if (cteAux == null)
                    {
                        Sis.LogTrace("CTE n�o encontrado. ID_CTE = " + itemRecebimento.Id.Value);
                    }

                    if (consultaCte.Stat == 2010)
                    {
                        var chaveSvc = string.Concat(consultaCte.ChaveAcesso.Substring(0, 34), "8", consultaCte.ChaveAcesso.Substring(35, 8));
                        var consultaCteSvc = _filaProcessamentoService.ObterRetornoCteProcessadoPorChave(chaveSvc);

                        if (consultaCteSvc != null)
                        {
                            cteAux.Chave = consultaCteSvc.ChaveAcesso;
                            _cteRepository.Atualizar(cteAux);

                            itemRecebimento.IdListaConfig = consultaCteSvc.IdLista;
                            _cteRecebimentoPoolingRepository.Atualizar(itemRecebimento);

                            consultaCte = consultaCteSvc;
                        }
                    }

                    // n�o autorizado devido ao vinculo a um Mdfe -> muda status do cte para AguardandoAnulacao (AGA)
                    if (consultaCte.Stat == 528)
                    {
                        cteAux.SituacaoAtual = SituacaoCteEnum.AguardandoAnulacao;
                        _cteRepository.Atualizar(cteAux);
                    }

                    if ((consultaCte.Tipo == 0 && consultaCte.Stat < 999 && consultaCte.Stat != 100) || (consultaCte.Tipo == 0 && consultaCte.Stat == 2013))
                    {
                        // Tenta encontrar o registro de autoriza��o pela chave de acesso do cte
                        Vw209ConsultaCte retorno = _filaProcessamentoService.ObterRetornoCteAutorizadoPorChave(consultaCte.ChaveAcesso);
                        if (retorno != null)
                        {
                            consultaCte = retorno;
                        }
                        else
                        {
                            // Se o cte foi autorizado em contig�ncia, tenta encontrar o registro pelo n�mero do cte, filial, s�rie e tipo de emiss�o 
                            string chaveSvc = string.Concat(consultaCte.ChaveAcesso.Substring(0, 34), "8", consultaCte.ChaveAcesso.Substring(35, 8));
                            retorno = _filaProcessamentoService.ObterRetornoCteAutorizadoPorChave(chaveSvc);

                            if (retorno != null)
                            {
                                cteAux.Chave = retorno.ChaveAcesso;
                                _cteRepository.Atualizar(cteAux);

                                consultaCte = retorno;
                            }
                            else
                            {
                                int numeroCteAux;

                                int.TryParse(cteAux.Numero, out numeroCteAux);
                                Nfe03Filial filial = _filaProcessamentoService.ObterIdentificadorFilial(cteAux);

                                retorno = _filaProcessamentoService.ObterCteJaAutorizadoComDiferencaNaChaveDeAcesso(numeroCteAux, (int)filial.IdFilial, int.Parse(cteAux.Serie));

                                if (retorno != null)
                                {
                                    cteAux.Chave = retorno.ChaveAcesso;
                                    _cteRepository.Atualizar(cteAux);

                                    consultaCte = retorno;
                                }
                            }
                        }
                    }

                    AtualizarCteRetorno(cteAux, consultaCte);
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Verifica Cte Arvore
        /// </summary>
        /// <param name="listaCte">Lista de Cte a serem verificados</param>
        public void VerificaCteArvorePorChave(List<string> listaCte)
        {
            foreach (string chave in listaCte)
            {
                VerificarCteFolhaArvorePorChave(chave);
            }
        }

        /// <summary>
        /// Verifica Cte Arvore pela Chave do Cte
        /// </summary>
        /// <param name="listaChave">Lista de chave do Cte</param>
        /// <param name="anulacao">Flag para verificar se o Cte est� com status diferente de Aguardando Anula��o</param>
        /// <returns>Retorna true quando for o ultimo cte da arvore</returns>
        public Dictionary<string, KeyValuePair<bool, string>> VerificarCtePorListaChave(List<string> listaChave, bool anulacao)
        {
            Dictionary<string, KeyValuePair<bool, string>> listaRetorno = new Dictionary<string, KeyValuePair<bool, string>>();

            if (listaChave != null)
            {
                foreach (string chave in listaChave)
                {
                    KeyValuePair<bool, string> statusRetorno = VerificarCtePorChave(chave, anulacao);
                    listaRetorno.Add(chave, statusRetorno);
                }
            }

            return listaRetorno;
        }

        /// <summary>
        /// Verifica Cte Arvore pela Chave do Cte
        /// </summary>
        /// <param name="listaChave">Lista de chave do Cte</param>
        /// <returns>Retorna true quando for o ultimo cte da arvore</returns>
        public Dictionary<string, KeyValuePair<bool, string>> VerificarCteFolhaArvorePorListaChave(List<string> listaChave)
        {
            Dictionary<string, KeyValuePair<bool, string>> listaRetorno = new Dictionary<string, KeyValuePair<bool, string>>();

            if (listaChave != null)
            {
                foreach (string chave in listaChave)
                {
                    KeyValuePair<bool, string> statusRetorno = VerificarCteFolhaArvorePorChave(chave);
                    listaRetorno.Add(chave, statusRetorno);
                }
            }

            return listaRetorno;
        }

        /// <summary>
        /// Verifica vag�es
        /// </summary>
        /// <param name="listaVagoes">Lista de vag�es</param>
        /// <returns>Retorna false quando o vag�o for valido</returns>
        public Dictionary<string, KeyValuePair<bool, string>> VerificarListaVagoes(List<string> listaVagoes)
        {
            var listaRetorno = new Dictionary<string, KeyValuePair<bool, string>>();

            if (listaVagoes != null)
            {
                foreach (string vagao in listaVagoes)
                {
                    KeyValuePair<bool, string> statusRetorno = VerificarVagao(vagao);
                    listaRetorno.Add(vagao, statusRetorno);
                }
            }

            return listaRetorno;
        }

        /// <summary>
        /// Verifica Cte Arvore pela Chave do Cte
        /// </summary>
        /// <param name="chave">Chave do Cte</param>
        /// <returns>Retorna false quando for o ultimo cte da arvore - true quando encontrar algum erro</returns>
        public KeyValuePair<bool, string> VerificarCteFolhaArvorePorChave(string chave)
        {
            Cte cte = _cteRepository.ObterPorChave(chave);

            if (chave.Length != 44)
            {
                return new KeyValuePair<bool, string>(true, "Chave CTe deve conter 44 caracteres.");
            }

            if (!VerificarDigitoChaveCte(chave))
            {
                return new KeyValuePair<bool, string>(true, "Chave Cte inv�lida.");
            }

            if (cte == null)
            {
                return new KeyValuePair<bool, string>(true, "Chave Cte n�o encontrada na base");
            }

            IList<CteArvore> listaArvore = _cteArvoreRepository.ObterArvorePorCteFilho(cte);
            CteArvore cteArvore = listaArvore.OrderByDescending(c => c.Sequencia).FirstOrDefault();

            if (cte.SituacaoAtual != SituacaoCteEnum.Autorizado)
            {
                return new KeyValuePair<bool, string>(true, "Cte cancelado");
            }

            if (cteArvore == null)
            {
                return new KeyValuePair<bool, string>(true, "Cte arvore n�o encontrado.");
            }

            if (cteArvore.CteFilho.Chave == chave)
            {
                return new KeyValuePair<bool, string>(false, "Chave Cte v�lida.");
            }

            return new KeyValuePair<bool, string>(true, "Existe um novo Cte gerado a partir da manuten��o.");
        }

        /// <summary>
        /// Verifica se o vag�o � v�lido
        /// </summary>
        /// <param name="codigo">C�digo do vag�o</param>
        /// <returns>Retorna false for um vag�o v�lido - true quando encontrar algum erro</returns>
        public KeyValuePair<bool, string> VerificarVagao(string codigo)
        {
            Vagao vagaoRegistro = _vagaoRepository.ObterPorCodigo(codigo);

            if (codigo.Length != 7)
            {
                return new KeyValuePair<bool, string>(true, "C�digo do Vag�o deve conter 7 d�gitos.");
            }

            if (vagaoRegistro == null)
            {
                return new KeyValuePair<bool, string>(true, "Vag�o n�o encontrado na base de dados.");
            }

            return new KeyValuePair<bool, string>(false, "Vag�o v�lido.");
        }

        /// <summary>
        /// Verifica Cte Arvore pela Chave do Cte
        /// </summary>
        /// <param name="chave">Chave do Cte</param>
        /// <param name="anulacao">Flag para verificar se o Cte est� com status diferente de Aguardando Anula��o</param>
        /// <returns>Retorna false quando for o ultimo cte da arvore - true quando encontrar algum erro</returns>
        public KeyValuePair<bool, string> VerificarCtePorChave(string chave, bool anulacao)
        {
            Cte cte = _cteRepository.ObterPorChave(chave);

            if (chave.Length != 44)
            {
                return new KeyValuePair<bool, string>(true, "Chave CTe deve conter 44 caracteres.");
            }

            if (!VerificarDigitoChaveCte(chave))
            {
                return new KeyValuePair<bool, string>(true, "Chave Cte inv�lida.");
            }

            if (cte == null)
            {
                return new KeyValuePair<bool, string>(true, "Chave Cte n�o encontrada na base");
            }

            IList<CteArvore> listaArvore = _cteArvoreRepository.ObterArvorePorCteFilho(cte);
            CteArvore cteArvore = listaArvore.OrderByDescending(c => c.Sequencia).FirstOrDefault();

            if (anulacao && cte.SituacaoAtual != SituacaoCteEnum.AguardandoAnulacao)
            {
                return new KeyValuePair<bool, string>(true, "Cte com status diferente de Aguardando Anula��o");
            }

            if (cteArvore == null)
            {
                return new KeyValuePair<bool, string>(true, "Cte arvore n�o encontrado.");
            }

            if (cteArvore.CteFilho.Chave == chave)
            {
                return new KeyValuePair<bool, string>(false, "Chave Cte v�lida.");
            }

            return new KeyValuePair<bool, string>(true, "Existe um novo Cte gerado a partir da manuten��o.");
        }

        /// <summary>
        /// Verifica se deve gerar fluxo para o CT-e.
        /// </summary>
        /// <param name="fluxo">C�digo do Fluxo</param>
        /// <returns>Retorna true for para gerar CT-e</returns>
        public bool VerificarFLuxoGerarCte(string fluxo)
        {
            return _cteRepository.VerificarFLuxoGerarCte(fluxo);
        }

        /// <summary>
        /// Verifica se � o ultimo cte da arvore
        /// </summary>
        /// <param name="id">id do cte a ser validado</param>
        /// <returns>Retorna true se for o ultimo cte da arvore</returns>
        public bool VerificarCteFolha(int id)
        {
            var cte = _cteRepository.ObterPorId(id);

            IList<CteArvore> listaArvore = _cteArvoreRepository.ObterArvorePorCteFilho(cte);
            CteArvore cteArvore = listaArvore.OrderByDescending(c => c.Sequencia).FirstOrDefault();

            return cteArvore.CteFilho.Chave == cte.Chave;
        }

        /// <summary>
        /// Verificar se o digito verificador � valido
        /// </summary>
        /// <param name="chave">Chave do Cte</param>
        /// <returns>Retorna true se a chave for verdadeira</returns>
        public bool VerificarDigitoChaveCte(string chave)
        {
            char[] pesoCte = "4329876543298765432987654329876543298765432".ToCharArray();
            char[] chaveCte = chave.ToCharArray();
            int soma = 0;
            int digito = 0;
            int diferenca = 0;
            int sum = 0;

            for (int i = 0; i < 43; i++)
            {
                sum = int.Parse(chaveCte[i].ToString()) * int.Parse(pesoCte[i].ToString());
                soma += sum;
            }

            diferenca = soma % 11;
            digito = 11 - diferenca;

            if (diferenca <= 1)
            {
                digito = 0;
            }

            if (int.Parse(chaveCte[43].ToString()) == digito)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Atualiza o Status do Cte de retorno
        /// </summary>
        /// <param name="cte">Cte de retorno</param>
        /// <param name="consultaCte">Objeto de consulta do Cte</param>
        public void AtualizarCteRetorno(Cte cte, Vw209ConsultaCte consultaCte)
        {
            try
            {
                Usuario usuario = ObterUsuarioRobo();

                // Insere no log do cte
                _cteLogService.InserirLogInfo(cte, "CteService", "Processando o cte de retorno");

                CteStatusRetorno cteStatusRetorno = _cteStatusRetornoRepository.ObterPorId(consultaCte.Stat);
                if (cteStatusRetorno == null)
                {
                    cteStatusRetorno = new CteStatusRetorno
                    {
                        Descricao = consultaCte.DescStat,
                        Id = consultaCte.Stat,
                        InformadoPelaReceita = false,
                        PermiteReenvio = false,
                        RemoveFilaInterface = true
                    };

                    _cteStatusRetornoRepository.Inserir(cteStatusRetorno);
                }

                // Processa o status de retono
                ProcessarCteStatusRetorno(cte, cteStatusRetorno, consultaCte);

                if ((cte.SituacaoAtual == SituacaoCteEnum.Autorizado) || (cte.SituacaoAtual == SituacaoCteEnum.AguardandoCancelamento) || (cte.SituacaoAtual == SituacaoCteEnum.AguardandoInutilizacao))
                {
                    // Item da fila de processamento (utilizado para pegar o XML de retorno)
                    Cte01Lista retornoConfig = _filaProcessamentoService.ObterItemFilaProcessamentoPorConsultaRetorno(consultaCte);
                    _cteStatusRepository.InserirCteStatus(cte, usuario, cteStatusRetorno, consultaCte, retornoConfig.XmlRetorno);
                }
                else
                {
                    _cteStatusRepository.InserirCteStatus(cte, usuario, cteStatusRetorno, consultaCte, cteStatusRetorno.Descricao);
                }

                cte.NumeroProtocolo = string.Format("{0}", consultaCte.Protocolo);
                _cteRepository.Atualizar(cte);

                // Verifica se pode ser removido da fila da interface
                if (cteStatusRetorno.RemoveFilaInterface)
                {
                    // Remove o cte da interface
                    _cteInterfaceRecebimentoConfigRepository.RemoverPorCte(cte);
                }

                // Verifica se pode ser inserido na interface de gera��o do PDF
                if (cte.SituacaoAtual == SituacaoCteEnum.Autorizado)
                {
                    // Item da fila de processamento (utilizado para pegar o XML de retorno)
                    Cte01Lista retornoConfig = _filaProcessamentoService.ObterItemFilaProcessamentoPorConsultaRetorno(consultaCte);
                    bool cartaDeCorrecao = cteStatusRetorno.Id == 135 && retornoConfig.Tipo == 6;
                    TipoOperacaoCteEnum tipoOperacao = cartaDeCorrecao ? TipoOperacaoCteEnum.CartaDeCorrecao : TipoOperacaoCteEnum.Inclusao;

                    // Insere na interface de gera��o arquivo PDF
                    InserirInterfaceArquivoPdf(cte, tipoOperacao);

                    // Insere na interface de gera��o arquivo XML
                    InserirInterfaceArquivoXml(cte, tipoOperacao);

                    if (!cartaDeCorrecao)
                    {
                        // Insere na interface de envio para o SAP
                        InserirInterfaceEnvioSap(cte, cteStatusRetorno);
                    }

                    try
                    {
                        _mdfeService.VerificarComposicaoMdfeEnviarAprovacao(cte.Id.Value, usuario);
                    }
                    catch (Exception exception)
                    {
                        _cteLogService.InserirLogErro(cte, exception.Message);
                    }
                }

                if (cte.SituacaoAtual == SituacaoCteEnum.Inutilizado)
                {
                    MudarSituacaoCte(cte, SituacaoCteEnum.Inutilizado, usuario, new CteStatusRetorno { Id = 13 }, "Arquivo Cte alterado para cancelado", null);

                    // Insere na interface de envio para o SAP
                    InserirInterfaceEnvioSap(cte, cteStatusRetorno);
                }

                if (cte.SituacaoAtual == SituacaoCteEnum.AutorizadoCancelamento)
                {
                    MudarSituacaoCte(cte, SituacaoCteEnum.Cancelado, usuario, new CteStatusRetorno { Id = 13 }, "Arquivo Cte alterado para cancelado", null);

                    // Insere na interface de envio para o SAP
                    CancelarCarregamentoCteConteiner(cte);

                    // Insere na interface de envio para o SAP
                    InserirInterfaceEnvioSap(cte, cteStatusRetorno);
                }

                if (cte.SituacaoAtual == SituacaoCteEnum.Inutilizado)
                {
                    // Insere na interface de envio para o SAP
                    InserirInterfaceEnvioSap(cte, cteStatusRetorno);

                    _cteStatusRepository.InserirCteStatus(cte, usuario, new CteStatusRetorno { Id = 15 });
                }

                if (cte.SituacaoAtual == SituacaoCteEnum.UsoDenegado)
                {
                    // Insere na interface de envio para o SAP
                    InserirInterfaceEnvioSap(cte, cteStatusRetorno);

                    _cteStatusRepository.InserirCteStatus(cte, usuario, new CteStatusRetorno { Id = 16 });
                }

                if (cte.TipoCte == TipoCteEnum.Anulacao && cte.SituacaoAtual == SituacaoCteEnum.Autorizado)
                {
                    try
                    {
                        var cteAnulado = _cteAnuladoRepository.ObterPorIdCteAnulacao(cte.Id.Value);
                        var ctePaiParaAnular = _cteRepository.ObterCtePorIdHql(cteAnulado.Cte.Id.Value);
                        ctePaiParaAnular.SituacaoAtual = SituacaoCteEnum.Anulado;
                        ctePaiParaAnular.DataAnulacao = DateTime.Now;
                        _cteRepository.AtualizarSemSessao(ctePaiParaAnular);
                        _cteStatusRepository.InserirCteStatus(ctePaiParaAnular, usuario, new CteStatusRetorno { Id = 39 });
                        _cteLogService.InserirLogInfo(cte, "CteService", "Atualiza a SituacaoAtual do Cte Pai desta Anula��o para Anulado");
                    }
                    catch (Exception ex)
                    {
                        _cteLogService.InserirLogErro(cte, "CteService", string.Format("AtualizarCteRetorno=>AtualizarCtePaiDoAnulacao: {0}", ex.Message), ex);
                    }
                }


                //// Inserre na interface de envio de Email os CTEs de Complemento
                if (cte.TipoCte == TipoCteEnum.Complementar)
                {
                    var cteInterfaceEnvioEmail = _cteInterfaceEnvioEmailRepository.ObterInterfacePorCte(cte.Id.Value);

                    if (cteInterfaceEnvioEmail == null)
                    {
                        try
                        {
                            ConfiguracaoTranslogic hostReenvio = _configuracaoTranslogicRepository.ObterPorId(_chaveHostReenvioEmail);

                            CteInterfaceEnvioEmail interfaceEnvioEmail = new CteInterfaceEnvioEmail();
                            interfaceEnvioEmail.Cte = cte;
                            interfaceEnvioEmail.Chave = cte.Chave;
                            interfaceEnvioEmail.DataHora = DateTime.Now;
                            interfaceEnvioEmail.DataUltimaLeitura = DateTime.Now;
                            interfaceEnvioEmail.HostName = hostReenvio.Valor;
                            _cteInterfaceEnvioEmailRepository.Inserir(interfaceEnvioEmail);
                        }
                        catch
                        {
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(cte, "CteService", string.Format("AtualizarCteRetorno: {0}", ex.Message), ex);
                throw;
            }
        }

        /// <summary>
        ///    Cancela o carregamento da carga na tabela CTE_CONTEINER.
        /// </summary>
        /// <param name="cte">Conte�do do arquivo de retorno do SEFAZ</param>
        private void CancelarCarregamentoCteConteiner(Cte cte)
        {
            _cteConteinerRepository.CancelarCarregPorChaveCte(cte.Chave);
        }

        /// <summary>
        /// Processa o arquivo de retorno e insere o status de retorno caso n�o exista
        /// </summary>
        /// <param name="conteudoArquivo">Conte�do do arquivo de retorno do SEFAZ</param>
        /// <returns>Objeto CteStatusRetorno</returns>
        public CteStatusRetorno ProcessarConteudoArquivoRetorno(string conteudoArquivo)
        {
            string[] split = conteudoArquivo.Split(';');
            string codigoRetornoString = split[7];
            int codigoRetorno;
            int.TryParse(codigoRetornoString, out codigoRetorno);

            CteStatusRetorno cteStatusRetorno = _cteStatusRetornoRepository.ObterPorId(codigoRetorno);
            if (cteStatusRetorno == null)
            {
                string mensagemRetorno = split[8];
                cteStatusRetorno = new CteStatusRetorno
                {
                    Descricao = mensagemRetorno,
                    Id = codigoRetorno,
                    InformadoPelaReceita = true
                };
                _cteStatusRetornoRepository.Inserir(cteStatusRetorno);
            }

            return cteStatusRetorno;
        }

        /// <summary>
        /// Remove da tabela de Pool de recebimento de da tabela de FTP
        /// </summary>
        /// <param name="listaProcessamento">Lista de arquivos a serem removidos</param>
        public void RemoverPoolingRecebimentoCte(IList<CteRecebimentoPooling> listaProcessamento)
        {
            _cteRecebimentoPoolingRepository.RemoverPorLista(listaProcessamento);
        }

        /// <summary>
        /// Remove da tabela de Pool de envio para SAP
        /// </summary>
        /// <param name="listaProcessamento">Lista de arquivos a serem removidos</param>
        public void RemoverPoolingEnvioSapCte(IList<CteSapPooling> listaProcessamento)
        {
            _cteSapPoolingRepository.RemoverPorLista(listaProcessamento);
        }

        /// <summary>
        /// Obt�m o vag�o pelo c�digo
        /// </summary>
        /// <param name="codigoVagao">C�digo do vag�o</param>
        /// <returns>Objeto Vagao</returns>
        public Vagao ObterVagaoPorCodigo(string codigoVagao)
        {
            Vagao vagao = _vagaoRepository.ObterPorCodigo(codigoVagao);
            if (vagao != null)
            {
                if (!vagao.IndAtivo)
                {
                    throw new TranslogicException("O vag�o n�o est� ativo no sistema.");
                }
            }

            return vagao;
        }

        /// <summary>
        /// Obt�m a lista de ctes que devem ser enviado para o SAP
        /// </summary>
        /// <returns>Retorna uma lista com os ctes a serem enviados</returns>
        public IList<CteInterfaceEnvioSap> ObterDadosParaEnvioSap()
        {
            string hostName = Dns.GetHostName();
            return _cteInterfaceEnvioSapRepository.ObterNaoProcessadosPorHost(hostName);
        }

        /// <summary>
        /// Envia dados para a interface do SAP
        /// </summary>
        /// <param name="itemPooling">Item do Pooling</param>
        /*
                public void EnviarInterfaceSap(CteSapPooling itemPooling)
                {
                    string hostName = Dns.GetHostName();
                    Cte cteAux = _cteRepository.ObterPorId(itemPooling.Id);

                    try
                    {
                        Usuario usuario = ObterUsuarioRobo();

                        // Servi�o de grava��o nas tabelas do SAP
                        _gravarSapCteEnvioService.Executar(cteAux, usuario);
                    }
                    catch (Exception ex)
                    {
                        _cteLogService.InserirLogErro(cteAux, "CteService", string.Format("EnviarInterfaceSap ({0}): {1}", hostName, ex.Message), ex);
                        throw;
                    }
                }
        */

        /// <summary>
        /// Grava na lista de envio do CT-e para o SAP o HOST 
        /// </summary>
        /// <param name="hostName">Nome do host</param>
        [Transaction]
        public virtual void GravarHostListaNaoProcessados(string hostName)
        {
            IList<CteInterfaceEnvioSap> list = _cteInterfaceEnvioSapRepository.ObterNaoProcessadosSemHost();
            foreach (CteInterfaceEnvioSap cteInterfaceEnvioSap in list)
            {
                _cteInterfaceEnvioSapRepository.GravarHostNaoProcessados(hostName, cteInterfaceEnvioSap);
            }
        }

        /// <summary>
        /// Atualiza a tabela de envio para a interface de envio para o SAP
        /// </summary>
        /// <param name="listaCteEnvioSap">Lista com os ctes a serem atualizados</param>
        public void AtualizarEnvioInterfaceSap(IList<CteInterfaceEnvioSap> listaCteEnvioSap)
        {
            foreach (CteInterfaceEnvioSap interfaceEnvioSap in listaCteEnvioSap)
            {
                _cteInterfaceEnvioSapRepository.AtualizarEnvioSap(interfaceEnvioSap.Cte);
            }
        }

        /// <summary>
        /// Atualiza a tabela de envio para a interface de envio para o SAP
        /// </summary>
        /// <param name="interfaceEnvioSap">Interface para ser atualizada</param>
        public void AtualizarEnvioInterfaceSap(CteInterfaceEnvioSap interfaceEnvioSap)
        {
            _cteInterfaceEnvioSapRepository.AtualizarEnvioSap(interfaceEnvioSap.Cte);
        }

        /// <summary>
        /// Obt�m uma lista com os ctes para importa��o
        /// </summary>
        /// <param name="hostName">Nome do servidor</param>
        /// <returns>Retorna uma lista com os ctes para serem importados</returns>
        public IList<CteInterfacePdfConfig> ObterCtesImportarPdf(string hostName)
        {
            try
            {
                IList<CteInterfacePdfConfig> listaInterface = _cteInterfacePdfRepository.ObterListaInterfacePorHost(hostName);
                foreach (CteInterfacePdfConfig itemInterface in listaInterface)
                {
                    itemInterface.DataUltimaLeitura = DateTime.Now;
                    itemInterface.DataUltimaLeitura = DateTime.Now;
                    _cteInterfacePdfRepository.Atualizar(itemInterface);
                }

                return listaInterface;
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(null, "CteService", string.Format("ObterCtesImportarPdf ({0}): {1}", hostName, ex.Message), ex);
                throw;
            }
        }

        /// <summary>
        /// Obt�m uma lista com os ctes para importa��o
        /// </summary>
        /// <param name="hostName">Nome do servidor</param>
        /// <returns>Retorna uma lista com os ctes para serem importados</returns>
        public IList<CteInterfaceXmlConfig> ObterCtesImportarXml(string hostName)
        {
            try
            {
                IList<CteInterfaceXmlConfig> listaInterface = _cteInterfaceXmlRepository.ObterListaInterfacePorHost(hostName);
                foreach (CteInterfaceXmlConfig itemInterface in listaInterface)
                {
                    itemInterface.DataUltimaLeitura = DateTime.Now;
                    _cteInterfaceXmlRepository.Atualizar(itemInterface);
                }

                return listaInterface;
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(null, "CteService", string.Format("ObterCtesImportarPdf ({0}): {1}", hostName, ex.Message), ex);
                throw;
            }
        }

        /// <summary>
        /// Obt�m os Ctes que devem ser enviado email
        /// </summary>
        /// <param name="hostName">Nome do servidor</param>
        /// <returns>Retorna uma lista com os dados da interface de envio de email</returns>
        public IList<CteInterfaceEnvioEmail> ObterCteEnviarEmail(string hostName)
        {
            IList<CteInterfaceEnvioEmail> lista = _cteInterfaceEnvioEmailRepository.ObterListaInterfacePorHost(hostName);
            foreach (CteInterfaceEnvioEmail cteInterfaceEnvioEmail in lista)
            {
                try
                {
                    cteInterfaceEnvioEmail.DataUltimaLeitura = DateTime.Now;
                    _cteInterfaceEnvioEmailRepository.Atualizar(cteInterfaceEnvioEmail);
                }
                catch { }
            }

            return lista;
        }

        /// <summary>
        /// Importa o pdf para dentro do banco
        /// </summary>
        /// <param name="cteInterfacePdf">Interface pdf do Cte</param>
        /// <param name="pdfPah">Caminho do pdf</param>
        public void ImportarArquivoPdf(CteInterfacePdfConfig cteInterfacePdf, string pdfPah)
        {
#if DEBUG
            pdfPah = @"D:\temp\Pdf\";
#endif
            Cte cte = _cteRepository.ObterPorId(cteInterfacePdf.Cte.Id);
            RegistrarLogDetalhado(cte, string.Format("Procurando PDF '{0}'", cte.Chave));

            string nomeArquivo = Path.Combine(pdfPah, cte.Chave + ".pdf");
            RegistrarLogDetalhado(cte, string.Format("Caminho para procurar PDF '{0}'", nomeArquivo));

            try
            {
                // Verifica se o arquivo existe
                if (File.Exists(nomeArquivo))
                {
                    bool cartaDeCorrecao = cteInterfacePdf.TipoOperacao != null && cteInterfacePdf.TipoOperacao == TipoOperacaoCteEnum.CartaDeCorrecao;

                    if (cartaDeCorrecao)
                    {
                        // Insere o arquivo pdf no banco de dados
                        _cteArquivoRepository.AtualizarPdfCartaDeCorrecao(cte, nomeArquivo);
                    }
                    else
                    {
                        // Insere o arquivo pdf no banco de dados
                        _cteArquivoRepository.InserirOuAtualizarPdf(cte, nomeArquivo);
                    }

                    _cteLogService.InserirLogInfo(cte, "CteService", "Pdf inserido na base (CTE_ARQUIVO)");

                    // string processado = pdfPah + "Processados\\" + cte.Chave + ".pdf";
                    string processado = ObterDiretorioProcessado(pdfPah, TipoArquivoEnvioEnum.Pdf, cte.DataHora) + cte.Chave + ".pdf";

                    if (File.Exists(processado))
                    {
                        File.Delete(processado);
                    }

                    // Move o arquivo para o dir de processados
                    File.Move(nomeArquivo, processado);

                    _cteLogService.InserirLogInfo(cte, "CteService", string.Format("Movendo arquivo de: {0} para: {1}", nomeArquivo, processado));

                    _cteLogService.InserirLogInfo(cte, "CteService", "Removendo da interface de gera��o do Pdf");

                    // Remove da lista da interface
                    _cteInterfacePdfRepository.Remover(cteInterfacePdf);

                    // Atualiza o flag de PDF gerado
                    // cte.ArquivoPdfGerado = true;
                    _cteRepository.AtualizarArquivoGeradoCte(cte, TipoArquivoEnvioEnum.Pdf);

                    // Insere na interface de envio de email
                    InserirInterfaceEnvioEmail(cte, cteInterfacePdf.TipoOperacao);
                }
                else
                {
                    _cteLogService.InserirLogErro(cte, "CteService", string.Format("N�o encontrado o arquivo: {0} ", nomeArquivo));
                }
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(cte, "CteService", string.Format("ImportarArquivoPdf: Erro na importacao do arquivo em ImportarArquivoPdf: {0}", ex.Message), ex);
            }
        }

        /// <summary>
        /// Importa o xml para dentro do banco
        /// </summary>
        /// <param name="cteInterfaceXml">Interface xml do Cte</param>
        /// <param name="xmlPath">Caminho do xml</param>
        public void ImportarArquivoXml(CteInterfaceXmlConfig cteInterfaceXml, string xmlPath)
        {
            Cte cte = _cteRepository.ObterCtePorHql(cteInterfaceXml.Cte);

            if (cte == null)
            {
                try
                {
                    int? idCte = cteInterfaceXml.Cte == null ? 0 : cteInterfaceXml.Cte.Id;
                    Sis.LogError(string.Format("Cte n�o encontrado � partir do CteInterfaceXmlConfig. IdCte={0} - cteInterfaceXml.Id={1} - xmlPath= {2}", idCte, cteInterfaceXml.Id, xmlPath));
                }
                catch (Exception ex)
                {
                    Sis.LogException(ex);
                }

                return;
            }

            RegistrarLogDetalhado(cte, string.Format("Procurando XML '{0}'", cte.Chave));

            string nomeArquivo = xmlPath + cte.Chave + ".xml";
            RegistrarLogDetalhado(cte, string.Format("Caminho para procurar XML '{0}'", nomeArquivo));

            try
            {
                // Verifica se o arquivo existe
                if (File.Exists(nomeArquivo))
                {
                    bool cartaDeCorrecao = cteInterfaceXml.TipoOperacao != null && cteInterfaceXml.TipoOperacao == TipoOperacaoCteEnum.CartaDeCorrecao;

                    if (cartaDeCorrecao)
                    {
                        // Insere o arquivo pdf no banco de dados
                        _cteArquivoRepository.AtualizarXmlCartaDeCorrecao(cte, nomeArquivo);
                    }
                    else
                    {
                        try
                        {
                            /*
                            * Solicita��o do dia 08/04/2015 para remover valida��es e enviar independente da condi��o, salvando um status novo para as ctes sem valor
                            * if (cte.FluxoComercial != null && !VerificarConteinerBrado(cte.FluxoComercial) && cte.ValorTotalMercadoria > 100)
                            */
                            var statusEnvio = StatusEnvioSeguradora.Validada;
                            var _listaCteDetalhe = _cteDetalheRepository.ObterPorCte(cte);

                            if (cte.FluxoComercial.UnidadeMedida.Id.Equals("M3"))
                            {
                                _cteLogService.InserirLogInfo(cte, "CteService", "Seguro - VOLUME");
                                if (_listaCteDetalhe.Any(cteDetalhe => cteDetalhe.VolumeNotaFiscal == 0.0 || cteDetalhe.PesoTotal == 0.0 || cteDetalhe.ValorTotalNotaFiscal < 100 || cte.ValorTotalMercadoria < 100))
                                {
                                    statusEnvio = StatusEnvioSeguradora.SemVolume;
                                }
                            }
                            else
                            {
                                _cteLogService.InserirLogInfo(cte, "CteService", "Seguro - PESO");
                                if (_listaCteDetalhe.Any(cteDetalhe => cteDetalhe.PesoNotaFiscal == 0.0 || cteDetalhe.PesoTotal == 0.0 || cteDetalhe.ValorTotalNotaFiscal < 100 || cte.ValorTotalMercadoria < 100))
                                {
                                    statusEnvio = StatusEnvioSeguradora.SemPeso;
                                }
                            }

                            if (_configuracaoTranslogicRepository.ObterPorId(_cteAtivaEnvioSeguradora).Valor.Equals("S"))
                            {
                                if (_listaCteDetalhe.Any(c => c.ChaveNfe != null))
                                {
                                    EnviarSeguradora(nomeArquivo, cte, statusEnvio);
                                }
                                else if (_listaCteDetalhe.Any(
                                    c => c.Cte.FluxoComercial.Mercadoria.CodigoMercadoriaSAP.Equals("011")))
                                {
                                    EnviarSeguradora(nomeArquivo, cte, statusEnvio);
                                }
                            }
                            else
                            {
                                _cteLogService.InserirLogInfo(cte, "CteService", "Seguro - Nota fiscal inv�lida");
                            }
                        }
                        catch (Exception ex)
                        {
                            _cteLogService.InserirLogInfo(cte, "CteService", string.Format("ERRO AO ENVIAR PARA O SEGURO - {0}", ex.Message));
                        }

                        // Insere o arquivo xml no banco de dados
                        _cteArquivoRepository.InserirOuAtualizarXml(cte, nomeArquivo);
                    }

                    _cteLogService.InserirLogInfo(cte, "CteService", "XML inserido na base (CTE_ARQUIVO)");

                    // string processado = xmlPah + "XmlProcessados\\" + cte.Chave + ".xml";
                    string processado = ObterDiretorioProcessado(xmlPath, TipoArquivoEnvioEnum.Xml, cte.DataHora) + cte.Chave + ".xml";

                    if (File.Exists(processado))
                    {
                        File.Delete(processado);
                    }

                    // Move o arquivo para o dir de processados
                    File.Move(nomeArquivo, processado);

                    _cteLogService.InserirLogInfo(cte, "CteService", string.Format("Movendo arquivo de: {0} para: {1}", nomeArquivo, processado));

                    _cteLogService.InserirLogInfo(cte, "CteService", "Removendo da interface de gera��o do Xml");

                    // Remove da lista da interface
                    _cteInterfaceXmlRepository.Remover(cteInterfaceXml);

                    // Atualiza o flag de PDF gerado
                    // cte.ArquivoXmlGerado = true;
                    _cteRepository.AtualizarArquivoGeradoCte(cte, TipoArquivoEnvioEnum.Xml);

                    // Insere na interface de envio de email
                    InserirInterfaceEnvioEmail(cte, cteInterfaceXml.TipoOperacao);
                }
                else
                {
                    _cteLogService.InserirLogErro(cte, "CteService", string.Format("N�o encontrado o arquivo: {0} ", nomeArquivo));
                }
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(cte, "CteService", string.Format("ImportarArquivoXml: Erro na importacao do arquivo em ImportarArquivoXml: {0}", ex.Message), ex);
            }
        }

        /// <summary>
        /// Rotina para Enviar o vagao para Seguradora.
        /// </summary>
        /// <param name="nomeArquivo">Caminho do Arquivo XML do CT-e</param>
        /// <param name="cte">Ct-e</param>
        /// <param name="statusEnvio">Status do Envio</param>
        public void EnviarSeguradora(string nomeArquivo, Cte cte, StatusEnvioSeguradora statusEnvio)
        {
            TipoEnvioSeguradora tipoEnvio;

            var requisicao = new UploadFileRequest()
            {
                Email =
                    _configuracaoTranslogicRepository.ObterPorId(
                        _chaveRemtenteEmailSeguradoraBradesco).Valor,
                Seguradora =
                    _configuracaoTranslogicRepository.ObterPorId(_chaveSeguradoraBradesco).Valor,
                XMLData = Tools.ConvertFileToString(nomeArquivo)
            };

            UploadFileResponse resultadoUploadFile = null;
            try
            {
                _cteLogService.InserirLogInfo(cte, "CteService", "Seguro - ENVIO POR WS");

                resultadoUploadFile = _receiveFileSeguroBradesco.UploadFile(requisicao);
                tipoEnvio = TipoEnvioSeguradora.WebService;
                if (resultadoUploadFile.@return.Contains("ERROR"))
                {
                    throw new Exception(resultadoUploadFile.@return);
                }
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogInfo(cte, "CteService", string.Format("Seguro - ENVIO POR EMAIL - {0}", ex.Message));

                // grava informa��es para envio do email
                GravarInformacaoEnvioEmail(cte,
                    _configuracaoTranslogicRepository.ObterPorId(_chaveEmailSeguradoraBradesco)
                        .Valor);

                tipoEnvio = TipoEnvioSeguradora.Email;
            }

            _cteEnvioSeguradoraHistRepository.Inserir(new CteEnvioSeguradoraHist()
            {
                Cte = cte,
                Seguradora = requisicao.Seguradora,
                Email = requisicao.Email,
                Retorno = resultadoUploadFile != null ? resultadoUploadFile.@return : null,
                TipoEnvio = ALL.Core.Util.Enum<TipoEnvioSeguradora>.GetDescriptionOf(tipoEnvio),
                StatusEnvio =
                    ALL.Core.Util.Enum<StatusEnvioSeguradora>.GetDescriptionOf(statusEnvio)
            });
        }

        /// <summary>
        /// Salva os e-mail
        /// </summary>
        /// <param name="email"> e-mail a ser alterado</param>
        /// <param name="novoEmail"> Novo e-mail a ser alterado</param>
        /// <param name="fluxo"> Fluxo informado</param>
        public void SalvarEmailCteReenvioArquivo(string email, string novoEmail, string fluxo)
        {
            _cteRepository.SalvarEmailCteReenvioArquivo(email, novoEmail, fluxo);
        }

        /// <summary>
        /// Adiciona um email no cte reenvio arquivo
        /// </summary>
        /// <param name="idCte">Id Cte para cadastro do email</param>
        /// <param name="email">novo email a ser cadastrado</param>
        /// <param name="tipoEnvioCte">Tipo de envio do email</param>
        /// <param name="tipoArquivoEnvio">tipo do Arquivo a ser enviado</param>
        public void AdicionarEmailCteReenvioArquivo(int idCte, string email, TipoEnvioCteEnum tipoEnvioCte, TipoArquivoEnvioEnum tipoArquivoEnvio)
        {
            CteEnvioXml cteEnvioXml = null;
            try
            {
                Cte cte = new Cte { Id = idCte };
                IList<CteEnvioXml> listaEmail = _cteEnvioXmlRepository.ObterTodosPorCte(cte);
                if ((listaEmail != null) && (listaEmail.Count > 0))
                {
                    cteEnvioXml = listaEmail.Where(g => g.EnvioPara.ToUpper() == email.ToUpper()).FirstOrDefault();
                }

                if (cteEnvioXml != null)
                {
                    throw new Exception(string.Format("Email: {0}<br/> j� cadastrado para o Cte!", email));
                }

                cteEnvioXml = new CteEnvioXml();
                cteEnvioXml.EnvioPara = email;
                cteEnvioXml.TipoArquivoEnvio = tipoArquivoEnvio;
                cteEnvioXml.TipoEnvioCte = tipoEnvioCte;
                cteEnvioXml.Cte = _cteRepository.ObterPorId(idCte);
                _cteEnvioXmlRepository.Inserir(cteEnvioXml);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Envia os emails com o Cte
        /// </summary>
        /// <param name="idXmlEnvio">Id do xml Envio</param>
        /// <param name="tipoEnvio">Tipo de Envio de arquivo</param>
        public void EnviarEmail(int idXmlEnvio, TipoArquivoEnvioEnum tipoEnvio)
        {
            string mask = ":";
            string emailServer = string.Empty;
            string remetente = string.Empty;
            string cteEmailSenhaSmtp = string.Empty;
            string[] valorArray = null;
            int emailPort = 0;
            ConfiguracaoTranslogic configuracaoTranslogic = null;
            ConfiguracaoTranslogic configuracaoRemetente = null;
            ConfiguracaoTranslogic configuracaocteEmailSenhaSmtp = null;
            Stream arquivoPdf = null;
            Stream arquivoXml = null;
            IList<CteEnvioXml> lista = null;
            CteEnvioXml auxCteEnvioXml = null;
            CteEnvioXml cteEnvioXml = null;

            try
            {
                lista = new List<CteEnvioXml>();

                auxCteEnvioXml = _cteEnvioXmlRepository.ObterPorId(idXmlEnvio);

                cteEnvioXml = new CteEnvioXml();
                cteEnvioXml.Ativo = true;
                cteEnvioXml.Cte = auxCteEnvioXml.Cte;
                cteEnvioXml.EnvioPara = auxCteEnvioXml.EnvioPara;
                cteEnvioXml.TipoEnvioCte = auxCteEnvioXml.TipoEnvioCte;
                cteEnvioXml.TipoArquivoEnvio = tipoEnvio;

                lista.Add(cteEnvioXml);

                // Recupera as informa��es do Cte
                CteArquivo cteArquivo = _cteArquivoRepository.ObterPorId(cteEnvioXml.Cte.Id);

                if (tipoEnvio == TipoArquivoEnvioEnum.Pdf || tipoEnvio == TipoArquivoEnvioEnum.PdfXml)
                {
                    arquivoPdf = Tools.ByteArrayToStream(cteArquivo.ArquivoPdf);
                }

                if (tipoEnvio == TipoArquivoEnvioEnum.Xml || tipoEnvio == TipoArquivoEnvioEnum.PdfXml)
                {
                    arquivoXml = Tools.StringToStream(cteArquivo.ArquivoXml);
                }

                configuracaoTranslogic = _configuracaoTranslogicRepository.ObterPorId(_cteEmailServer);
                valorArray = configuracaoTranslogic.Valor.Split(mask.ToCharArray());

                emailServer = valorArray[0];
                emailPort = Convert.ToInt32(valorArray[1]);

                configuracaoRemetente = _configuracaoTranslogicRepository.ObterPorId(_cteEmailRemetente);
                remetente = configuracaoRemetente.Valor;

                configuracaocteEmailSenhaSmtp = _configuracaoTranslogicRepository.ObterPorId(_cteEmailSenhaSmtp);
                cteEmailSenhaSmtp = configuracaocteEmailSenhaSmtp.Valor;

                EnviarEmail(cteEnvioXml.Cte, emailServer, emailPort, remetente, lista, tipoEnvio, arquivoPdf, arquivoXml, cteEmailSenhaSmtp);
            }
            finally
            {
                mask = ":";
                emailServer = string.Empty;
                remetente = string.Empty;
                valorArray = null;
                emailPort = 0;
                configuracaoTranslogic = null;
                configuracaoRemetente = null;
                arquivoPdf = null;
                arquivoXml = null;
                lista = null;
                cteEnvioXml = null;
            }
        }

        /// <summary>
        /// Envia os emails com o Cte
        /// </summary>
        /// <param name="cteInterfaceEnvioEmail">Interface de envio de email</param>
        /// <param name="emailServer">Servidor de email</param>
        /// <param name="emailPort">Porta do servidor de email</param>
        /// <param name="remetente">Remetente do email do Cte</param>
        /// <param name="senhaSmtp">Senha do smtp caso necessite de autentica��o</param>
        public void EnviarEmail(CteInterfaceEnvioEmail cteInterfaceEnvioEmail, string emailServer, int emailPort, string remetente, string senhaSmtp)
        {
            Cte cte = cteInterfaceEnvioEmail.Cte;
            Stream arquivoPdf = null;
            Stream arquivoXml = null;

            try
            {
                // Lista os envios
                IList<CteEnvioXml> listaEnvio = _cteEnvioXmlRepository.ObterPorCte(cte);

                // Recupera as informa��es do Cte
                CteArquivo cteArquivo = _cteArquivoRepository.ObterPorId(cte.Id);
                if (cteArquivo == null)
                {
                    // Grava no status do Cte
                    _cteStatusRepository.InserirCteStatus(cte, ObterUsuarioRobo(), new CteStatusRetorno { Id = 29 });

                    // Insere no log do cte
                    _cteLogService.InserirLogErro(cte, "CteService", string.Format("EnviarEmail: cte {0} n�o encontrado os arquivos de envio na base de dados - HostName:{1}", cte.Id, Dns.GetHostName()));

                    // Remove da interface
                    _cteInterfaceEnvioEmailRepository.Remover(cteInterfaceEnvioEmail);

                    return;
                }

                if (cteInterfaceEnvioEmail.TipoOperacao != null && cteInterfaceEnvioEmail.TipoOperacao == TipoOperacaoCteEnum.CartaDeCorrecao)
                {
                    arquivoPdf = Tools.ByteArrayToStream(cteArquivo.ArquivoPdfCartaDeCorrecao);
                    arquivoXml = Tools.StringToStream(cteArquivo.ArquivoXmlCartaDeCorrecao);
                }
                else
                {
                    arquivoPdf = Tools.ByteArrayToStream(cteArquivo.ArquivoPdf);
                    arquivoXml = Tools.StringToStream(cteArquivo.ArquivoXml);
                }

                if ((arquivoPdf == null) || (arquivoXml == null))
                {
                    // Grava no status do Cte
                    _cteStatusRepository.InserirCteStatus(cte, ObterUsuarioRobo(), new CteStatusRetorno { Id = 29 });

                    // Insere no log do cte
                    _cteLogService.InserirLogErro(cte, "CteService", string.Format("EnviarEmail: cte {0} n�o encontrado os arquivos de envio na base de dados - HostName:{1}", cte.Id, Dns.GetHostName()));

                    // Remove da interface
                    _cteInterfaceEnvioEmailRepository.Remover(cteInterfaceEnvioEmail);

                    return;
                }

                // #########################################
                // Gera email que envia o PDF e o XML
                // #########################################
                if (arquivoPdf != null)
                {
                    arquivoPdf.Seek(0, SeekOrigin.Begin);
                }

                if (arquivoXml != null)
                {
                    arquivoXml.Seek(0, SeekOrigin.Begin);
                }

                IList<CteEnvioXml> listaEnvioPdfXml = listaEnvio.Where(g => g.TipoArquivoEnvio == TipoArquivoEnvioEnum.PdfXml).ToList();
                if (listaEnvioPdfXml.Count > 0)
                {
                    EnviarEmail(cte, emailServer, emailPort, remetente, listaEnvioPdfXml, TipoArquivoEnvioEnum.PdfXml, arquivoPdf, arquivoXml, senhaSmtp);
                }

                // #########################################
                // Gera email que envia o PDF
                // #########################################
                IList<CteEnvioXml> listaEnvioPdf = listaEnvio.Where(g => g.TipoArquivoEnvio == TipoArquivoEnvioEnum.Pdf).ToList();
                if (listaEnvioPdf.Count > 0)
                {
                    EnviarEmail(cte, emailServer, emailPort, remetente, listaEnvioPdf, TipoArquivoEnvioEnum.Pdf, arquivoPdf, null, senhaSmtp);
                }

                // #########################################
                // Gera email que envia XML
                // #########################################
                IList<CteEnvioXml> listaEnvioXml = listaEnvio.Where(g => g.TipoArquivoEnvio == TipoArquivoEnvioEnum.Xml).ToList();
                if (listaEnvioXml.Count > 0)
                {
                    EnviarEmail(cte, emailServer, emailPort, remetente, listaEnvioXml, TipoArquivoEnvioEnum.Xml, null, arquivoXml, senhaSmtp);
                }

                // Grava no status do Cte
                _cteStatusRepository.InserirCteStatus(cte, ObterUsuarioRobo(), new CteStatusRetorno { Id = 28 });

                // Remove da interface
                _cteInterfaceEnvioEmailRepository.Remover(cteInterfaceEnvioEmail);
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(cteInterfaceEnvioEmail.Cte, "CteService", ex.Message, ex);
                throw;
            }
        }

        /// <summary>
        /// Gera e-mail para central de Fluxo
        /// </summary>
        /// <param name="cte">Cte que ocorreu o problema</param>
        /// <param name="remetente">remetende da mensagem</param>
        /// <param name="listaEmail">lista de e-mail inv�lidos</param>
        /// <returns>Retorna a mensagem para envio</returns>
        public MailMessage GerarEmailCentralFluxo(Cte cte, string remetente, List<string> listaEmail)
        {
            MailMessage mailMessage = null;
            StringBuilder mensagemBody;

            try
            {
                /*
                // Envio de email
                mailMessage = new MailMessage();
                mensagemBody = new StringBuilder();

                mailMessage.BodyEncoding = Encoding.UTF8;
                mailMessage.Subject = "CTe - Endere�o de e-mail do tomador inv�lido.";
                mailMessage.From = new MailAddress(remetente);
                mailMessage.To.Add("central.fluxos@all-logistica.com");
                // mailMessage.CC.Add("robson.schmidt@all-logistica.com");

                mensagemBody.AppendLine("Verificar cadastro de e-mail do Tomador do Cte.");
                mensagemBody.AppendLine("<br/>S�rie: " + cte.Serie + " - N�mero: " + cte.Numero + " - UF: " + cte.SiglaUfFerrovia);
                mensagemBody.AppendLine("<br/><br/>E-mail(s) inv�lido(s): ");

                foreach (string email in listaEmail)
                {
                                                mensagemBody.AppendLine("<br/>" + email);
                                                _cteLogService.InserirLogErro(cte, "CteService", string.Format("GerarEmailCentralFluxo: Email invalido: {0}", email));
                }

                mailMessage.Body = mensagemBody.ToString();
                mailMessage.IsBodyHtml = true;
                 */

                foreach (string email in listaEmail)
                {
                    _cteLogService.InserirLogErro(cte, "CteService", string.Format("GerarEmailCentralFluxo: Remetente: {0} Email invalido: {1}", remetente, email));
                }
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(cte, "CteService", string.Format("EnviarEmailCentralFluxo: Erro no envio de e-mail: {0}", ex.Message), ex);
            }

            return mailMessage;
        }

        /// <summary>
        /// Grava as informa��es na tabela de envio de email
        /// </summary>
        /// <param name="cte">Cte de refer�ncia para gravar as informa��es</param>
        /// <param name="emailNfe">emails para ser enviado o arquivo NFE</param>
        /// <param name="emailXml">emails para ser enviado o arquivo PDF</param>
        public void GravarInformacoesEnvioEmail(Cte cte, string emailNfe, string emailXml)
        {
            try
            {
                string[] emailsNfe = ParserEmail(emailNfe);
                string[] emailsXml = ParserEmail(emailXml);

                // Remove itens por Cte
                _cteEnvioXmlRepository.RemoverPorCte(cte);

                // Cria a lista de emails que envia os arquivos Pdf e Xml
                // Encontra os emails comuns
                var emailsArquivoPdfXml = emailsNfe.Select(g => g.Trim()).Intersect(emailsXml.Select(s => s.Trim()));
                foreach (string email in emailsArquivoPdfXml)
                {
                    CteEnvioXml envioXml = new CteEnvioXml();
                    envioXml.Cte = cte;
                    envioXml.EnvioPara = email;
                    envioXml.TipoEnvioCte = TipoEnvioCteEnum.Email;
                    envioXml.TipoArquivoEnvio = TipoArquivoEnvioEnum.PdfXml;
                    envioXml.Ativo = true;
                    _cteEnvioXmlRepository.Inserir(envioXml);
                }

                // Cria a lista de emails que envia apenas o pdf
                var emailsArquivoPdf = emailsNfe.Except(emailsXml);
                foreach (string email in emailsArquivoPdf)
                {
                    CteEnvioXml envioXml = new CteEnvioXml();
                    envioXml.Cte = cte;
                    envioXml.EnvioPara = email;
                    envioXml.TipoEnvioCte = TipoEnvioCteEnum.Email;
                    envioXml.TipoArquivoEnvio = TipoArquivoEnvioEnum.Pdf;
                    envioXml.Ativo = true;
                    _cteEnvioXmlRepository.Inserir(envioXml);
                }

                // Cria a lista de emails que envia apenas o pdf
                var emailsArquivoXml = emailsXml.Except(emailsNfe);
                foreach (string email in emailsArquivoXml)
                {
                    CteEnvioXml envioXml = new CteEnvioXml();
                    envioXml.Cte = cte;
                    envioXml.EnvioPara = email;
                    envioXml.TipoEnvioCte = TipoEnvioCteEnum.Email;
                    envioXml.TipoArquivoEnvio = TipoArquivoEnvioEnum.Xml;
                    envioXml.Ativo = true;
                    _cteEnvioXmlRepository.Inserir(envioXml);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Grava as informa��es na tabela de envio de email
        /// </summary>
        /// <param name="cte">Cte de refer�ncia para gravar as informa��es</param>
        /// <param name="email">emails para ser enviado o arquivo NFE</param>
        public void GravarInformacaoEnvioEmail(Cte cte, string email)
        {
            try
            {
                CteEnvioXml envioXml = new CteEnvioXml();
                envioXml.Cte = cte;
                envioXml.EnvioPara = email;
                envioXml.TipoEnvioCte = TipoEnvioCteEnum.Email;
                envioXml.TipoArquivoEnvio = TipoArquivoEnvioEnum.Xml;
                envioXml.Ativo = true;
                _cteEnvioXmlRepository.Inserir(envioXml);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Obtem o arquivo cte
        /// </summary>
        /// <param name="cteId">Codigo do Cte a ser localizado</param>
        /// <returns>Retorna o arquivo cte</returns>
        public CteArquivo ObterArquivoCte(int cteId)
        {
            return _cteArquivoRepository.ObterPorId(cteId);
        }

        /// <summary>
        /// Obtem o arquivo cte pelo Id do despacho
        /// </summary>
        /// <param name="idDespacho">Id do Despacho vinculado ao CTE a ser localizado</param>
        /// <returns>Retorna o arquivo cte</returns>
        public IList<CteArquivo> ObterArquivoCtePorDespacho(int idDespacho)
        {
            return _cteArquivoRepository.ObterPorIdDespacho(idDespacho);
        }


        /// <summary>
        /// Obtem uma lista de arquivos cte
        /// </summary>
        /// <param name="cteId">Codigos dos Ctes a serem localizados</param>
        /// <returns>Retorna os arquivos cte</returns>
        public IList<CteArquivo> ObterArquivoCteLista(List<int> cteIds)
        {
            return _cteArquivoRepository.ObterPorIds(cteIds);
        }

        /// <summary>
        /// Verifica se o cliente correntista do fluxo � para liberar as travas
        /// </summary>
        /// <param name="fluxo">Fluxo comercial</param>
        /// <returns>Valor booleano</returns>
        public bool VerificarLiberacaoTravasCorrentista(FluxoComercial fluxo)
        {
            if (fluxo.Codigo == "99999")
            {
                return false;
            }

            if (fluxo.Contrato.EmpresaPagadora.DescricaoResumida.ToUpperInvariant().StartsWith("BRADO"))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Pelo fluxo comercial verifica se � conteiner da Brado
        /// </summary>
        /// <param name="fluxo">Fluxo comercial de refer�ncia</param>
        /// <returns>Verdadeiro caso seja Brado, sen�o retorna falso</returns>
        public bool VerificarConteinerBrado(FluxoComercial fluxo)
        {
            // -> Entrou Pallets de acordo com o e-mail "CT-e 595270" (22/11/2013) acordado entre Joel e Shirata
            if (fluxo.Contrato.EmpresaPagadora.DescricaoResumida.ToUpperInvariant().StartsWith("BRADO") && (fluxo.Mercadoria.UnidadeMedida.Id.Equals("CON") || fluxo.Mercadoria.DescricaoResumida.ToUpper().Contains("PALLETS")))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Obtem o diret�rio dos arquivos processados
        /// </summary>
        /// <param name="pathInicial">Caminho Inicial</param>
        /// <param name="tipoArquivo">Tipo do arquivo</param>
        /// <param name="dataCte">Data do Cte</param>
        /// <returns>Retorna caminho</returns>
        public string ObterDiretorioProcessado(string pathInicial, TipoArquivoEnvioEnum tipoArquivo, DateTime dataCte)
        {
            string nomeDiretorio = string.Empty;
            if (tipoArquivo == TipoArquivoEnvioEnum.Pdf)
            {
                nomeDiretorio = pathInicial + @"PDF_PROCESSADOS\";
            }
            else if (tipoArquivo == TipoArquivoEnvioEnum.Xml)
            {
                nomeDiretorio = pathInicial + @"XML_PROCESSADOS\";
            }
            else
            {
                nomeDiretorio = pathInicial + @"PROCESSADOS\";
            }

            nomeDiretorio += String.Format("{0:yyyyMM}", dataCte);
            nomeDiretorio += @"\";

            if (!Directory.Exists(nomeDiretorio))
            {
                Directory.CreateDirectory(nomeDiretorio);
            }

            return nomeDiretorio;
        }

        /// <summary>
        /// Retorna todos os Email do cte
        /// </summary>
        /// <param name="idCte">Id do Cte para retorno do status</param>
        /// <returns>lista de Cte para envio de xml</returns>
        public IList<ReenvioEmailDto> RetornaEmailReenvioArquivo(int idCte)
        {
            // Cte cte = _cteRepository.ObterPorId(idCte);
            return _cteEnvioXmlRepository.ObterPorCte(idCte);
        }

        /// <summary>
        /// Retorna todos os Cte Autorizados para listagem de Impress�o
        /// </summary>
        /// <param name="pagination"> Paginacao pesquisa</param>
        /// <param name="filtros"> Filtros para pesquisa</param>
        /// <returns>lista de Cte</returns>
        public ResultadoPaginado<CteDto> RetornaCteReenvioArquivo(DetalhesPaginacaoWeb pagination, DetalhesFiltro<object>[] filtros)
        {
            DateTime dataIni = DateTime.Now;
            DateTime dataFim = DateTime.Now;
            string fluxo = string.Empty;
            string email = string.Empty;
            bool emailErro = false;
            string serie = string.Empty;
            string destino = string.Empty;
            string origem = string.Empty;
            string numVagao = string.Empty;
            string codigoUfDcl = string.Empty;
            int numDespacho = 0;
            string numeroCte = string.Empty;
            string[] chaveCte = null;

            if (filtros != null)
            {
                foreach (var detalheFiltro in filtros)
                {
                    if (detalheFiltro.Campo.Equals("dataInicial") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        string texto = detalheFiltro.Valor[0].ToString();

                        dataIni = DateTime.ParseExact(texto, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                    }

                    if (detalheFiltro.Campo.Equals("dataFinal") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        string texto = detalheFiltro.Valor[0].ToString();

                        dataFim = DateTime.ParseExact(texto, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);

                        dataFim = dataFim.AddDays(1).AddSeconds(-1);
                    }

                    if (detalheFiltro.Campo.Equals("serie") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        serie = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
                    }

                    if (detalheFiltro.Campo.Equals("despacho") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        numDespacho = Convert.ToInt32(detalheFiltro.Valor[0].ToString());
                    }

                    if (detalheFiltro.Campo.Equals("fluxo") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        fluxo = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
                    }

                    if (detalheFiltro.Campo.Equals("Origem") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        origem = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
                    }

                    if (detalheFiltro.Campo.Equals("UfDcl") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        codigoUfDcl = detalheFiltro.Valor[0].ToString().ToUpperInvariant().Trim();
                    }

                    if (detalheFiltro.Campo.Equals("Destino") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        destino = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
                    }

                    if (detalheFiltro.Campo.Equals("Vagao") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        numVagao = detalheFiltro.Valor[0].ToString();
                    }

                    if (detalheFiltro.Campo.Equals("email") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        email = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
                    }

                    if (detalheFiltro.Campo.Equals("emailErro") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        emailErro = bool.Parse(detalheFiltro.Valor[0].ToString());
                    }

                    if (detalheFiltro.Campo.Equals("NroCte") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        numeroCte = detalheFiltro.Valor[0].ToString().PadLeft(9, '0');
                    }

                    if (detalheFiltro.Campo.Equals("chave") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        chaveCte = detalheFiltro.Valor[0].ToString().Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                    }
                }
            }

            ResultadoPaginado<CteDto> listaDto = new ResultadoPaginado<CteDto>();

            listaDto = _cteRepository.ObterCteReenvioEmail(pagination, dataIni, dataFim, fluxo, email, emailErro, serie, numDespacho, destino, origem, numVagao, codigoUfDcl, numeroCte, chaveCte);
            listaDto.Items = listaDto.Items.OrderByDescending(c => c.Numero).ToList();
            IEnumerable<CteDto> lst = listaDto.Items.Distinct();

            return listaDto;
        }

        /// <summary>
        /// Efetua o reenvio de e-mail
        /// </summary>
        /// <param name="idCtes">Id dos Ctes que ser�o reenviados</param>
        public void ReenviarEmailporCte(int[] idCtes)
        {
            // vers�o Speed
            using (var db = Dbs.NewDb(Db.Translogic))
            {
                db.BeginTransaction();
                var ids = string.Join(",", idCtes);

                BL_CteEnvioEmailErro.Delete(db, "ID_CTE in ({0})", ids);

                db.ExecuteNonQuery("update CTE_ENVIO_XML set IND_ATIVO = 'S' where ID_CTE in ({0})", ids);

                var _hostReenvio = CacheManager.ConfigGeral.Value.CteHostReenvioEmail;

                var ctes = BL_Cte.Select(db, "ID_CTE in ({0})", ids).ToDictionary(p => p.IdCte);

                foreach (var idCte in idCtes)
                {
                    if (ctes.ContainsKey(idCte))
                    {
                        var cte = ctes[idCte];
                        var cteInterfaceEnvioEmail = BL_CteInterfEnvioEmail.SelectSingle(db, new Spd.Data.CteInterfEnvioEmail { IdCte = idCte });

                        if (cteInterfaceEnvioEmail == null)
                        {
                            Spd.Data.CteInterfEnvioEmail interfaceEnvioEmail = new Spd.Data.CteInterfEnvioEmail();
                            interfaceEnvioEmail.IdCte = idCte;
                            interfaceEnvioEmail.ChaveCte = cte.ChaveCte;
                            interfaceEnvioEmail.DataHora = DateTime.Now;
                            interfaceEnvioEmail.DataUltimaLeitura = DateTime.Now;
                            interfaceEnvioEmail.HostName = _hostReenvio;
                            interfaceEnvioEmail.TipoOperacao = TipoOperacaoCteEnum.Inclusao.Desc();
                            BL_CteInterfEnvioEmail.Insert(db, interfaceEnvioEmail);
                        }
                    }
                }
                db.Commit();
            }

            /*
            // Remove o CTe do log de erro
            _cteEnvioEmailErroRepository.RemoverTodosPorCte(idCtes);

            // Obtem a lista de Cte do Reposit�rio
            IList<CteEnvioXml> listaCte = _cteEnvioXmlRepository.ObterTodosPorCte(idCtes);

            // Percorre a lista e inseri no pooling
            foreach (CteEnvioXml cteEnvioXml in listaCte)
            {
                cteEnvioXml.Ativo = true;
                _cteEnvioXmlRepository.Atualizar(cteEnvioXml);
            }

            ConfiguracaoTranslogic hostReenvio = _configuracaoTranslogicRepository.ObterPorId(_chaveHostReenvioEmail);
            foreach (var idCte in idCtes)
            {
                var cteInterfaceEnvioEmail = _cteInterfaceEnvioEmailRepository.ObterInterfacePorCte(idCte);

                if (cteInterfaceEnvioEmail == null)
                {
                    Cte cte = _cteRepository.ObterPorId(idCte);
                    CteInterfaceEnvioEmail interfaceEnvioEmail = new CteInterfaceEnvioEmail();
                    interfaceEnvioEmail.Cte = cte;
                    interfaceEnvioEmail.Chave = cte.Chave;
                    interfaceEnvioEmail.DataHora = DateTime.Now;
                    interfaceEnvioEmail.DataUltimaLeitura = DateTime.Now;
                    interfaceEnvioEmail.HostName = hostReenvio.Valor;
                    _cteInterfaceEnvioEmailRepository.Inserir(interfaceEnvioEmail);
                }
            }
            */
        }

        /// <summary>
        /// Retorna os Estados da Serie Desp Uf
        /// </summary>
        /// <returns>Lista de estados da Serie Desp Uf</returns>
        public IList<string> ObterEstadosSerieDespUf()
        {
            IList<SerieDespachoUf> retorno = _serieDespachoUfRepository.ObterTodos();
            return retorno.Select(e => e.CodUnidadeFederativaFerrovia).Distinct().ToList();
        }

        /// <summary>
        /// Salva o novo comunicado
        /// </summary>
        /// <param name="arquivo">Comunicado a ser salvo</param>
        /// <param name="uf"> Estado do comunicado</param>
        [Transaction]
        public virtual void SalvarComunicado(Stream arquivo, string uf)
        {
            // Inativa o CteComunicado Anterior
            CteComunicado comunicadoAnterior = _cteComunicadoRepository.ObterPorSiglaUf(uf);

            if (comunicadoAnterior != null)
            {
                comunicadoAnterior.Ativo = false;
                _cteComunicadoRepository.Atualizar(comunicadoAnterior);
            }

            // Salva o novo comunicado
            CteComunicado cteComunicado = new CteComunicado();
            cteComunicado.ComunicadoPdf = Tools.ConverterStreamToArray(arquivo);
            cteComunicado.SiglaUf = uf.ToUpperInvariant();
            cteComunicado.Ativo = true;
            cteComunicado.DataCadastro = DateTime.Now;
            _cteComunicadoRepository.Inserir(cteComunicado);
        }

        /// <summary>
        /// Retorno a lista de Comunicados Ativo
        /// </summary>
        /// <returns> lista de Comunicados Ativo</returns>
        public IList<CteComunicado> ObterComunicados()
        {
            return _cteComunicadoRepository.ObterTodosAtivos();
        }

        /// <summary>
        /// Apaga o Comunicados
        /// </summary>
        /// <param name="idCteComunicado">Id do comunicado a ser excluido</param>
        public void ExcluirComunicado(int idCteComunicado)
        {
            // Inativa o CteComunicado Anterior
            CteComunicado comunicadoAnterior = _cteComunicadoRepository.ObterPorId(idCteComunicado);

            if (comunicadoAnterior != null)
            {
                comunicadoAnterior.Ativo = false;
                _cteComunicadoRepository.Atualizar(comunicadoAnterior);
            }
        }

        /// <summary>
        /// Efetua o reenvio de e-mail
        /// </summary>
        /// <param name="idXmlEnvio">Id dos Xml de Envio</param>
        /// <param name="tipoEnvio"> Tipo de envio de e-mail</param>
        public void ReenviarEmail(int idXmlEnvio, TipoArquivoEnvioEnum tipoEnvio)
        {
            // Obtem a lista de Cte do Reposit�rio
            CteEnvioXml cteEnvioXml = _cteEnvioXmlRepository.ObterPorId(idXmlEnvio);
            IList<CteEnvioXml> listaXmlEnvio = _cteEnvioXmlRepository.ObterTodosPorCte(cteEnvioXml.Cte);

            foreach (CteEnvioXml envioXml in listaXmlEnvio)
            {
                if (envioXml.Id == idXmlEnvio)
                {
                    envioXml.Ativo = true;
                }
                else
                {
                    envioXml.Ativo = false;
                }
                // 874383

                _cteEnvioXmlRepository.Atualizar(cteEnvioXml);
            }

            EnviarEmail(idXmlEnvio, tipoEnvio);
        }

        /// <summary>
        /// Exclui o e-mail do cadastro do cte
        /// </summary>
        /// <param name="idXmlEnvio">Id dos Xml de Envio</param>
        public void ExcluirEmailCteReenvioArquivo(int idXmlEnvio)
        {
            CteEnvioXml xmlEnvio = _cteEnvioXmlRepository.ObterPorId(idXmlEnvio);

            if (xmlEnvio == null)
            {
                throw new Exception("N�o foi localizado o e-mail para exclus�o");
            }

            _cteEnvioXmlRepository.Remover(xmlEnvio);
        }

        /// <summary>
        /// Obt�m os motivos de cancelamento ativos
        /// </summary>
        /// <returns>Lista de motivos de cancelamento</returns>
        public IList<CteMotivoCancelamento> ObterMotivosCancelamentoAtivos()
        {
            return _cteMotivoCancelamentoRepository.ObterAtivos();
        }

        /// <summary>
        /// Obtem o usu�rio do Robo
        /// </summary>
        /// <returns>Retorna o usu�rio do Robo</returns>
        public Usuario ObterUsuarioRobo()
        {
            ConfiguracaoTranslogic usuarioRobo = _configuracaoTranslogicRepository.ObterPorId(_chaveCteRobo);
            return _usuarioRepository.ObterPorId(Convert.ToInt32(usuarioRobo.Valor));
        }

        /// <summary>
        /// Obt�m lista de empresas utilizadas para o cte.
        /// </summary>
        /// <param name="cte">Cte a ser pesquisado</param>
        /// <returns>Lista dos elementos da arvore</returns>
        public CteEmpresas ObterListaDeEmpresas(Cte cte)
        {
            return _cteEmpresaRepository.ObterPorCte(cte);
        }

        /// <summary>
        /// Realiza a valida��o da troca do fluxo comercial com o objetivo de realizar uma corre��o em Cte.
        /// Ao trocar o fluxo, n�o deve ser poss�vel ser alterado o expedidor e o recebedor.  
        /// </summary>
        /// <param name="fluxoComercial">Novo fluxo comercial</param>
        /// <param name="idCte">Identificador do Cte</param>
        /// <returns> True se n�o for encontrado inconsist�ncias </returns>
        public bool VerificarInconsistenciasNaAlteracaoDoFluxoComercial(FluxoComercial fluxoComercial, int idCte)
        {
            Cte cte = ObterCtePorId(idCte);
            CteEmpresas empresas = ObterListaDeEmpresas(cte);
            bool alteracaoRemetente = ValidarSeOcorreuAlteracaoDeRemetente(cte, fluxoComercial);
            bool alteracaoDestinatario = ValidarSeOcorreuAlteracaoDeDestinatario(cte, fluxoComercial);
            bool alteracaoTomador = ValidarSeOcorreuAlteracaoDeTomador(cte, fluxoComercial);
            bool alteracaoExpedidor = ValidarSeOcorreuAlteracaoDeExpedidor(cte, fluxoComercial);
            bool alteracaoRecebedor = ValidarSeOcorreuAlteracaoDeRecebedor(cte, fluxoComercial);
            bool validaAlteracaoCfop;

            ConfiguracaoTranslogic chave = _configuracaoTranslogicRepository.ObterPorId(_chaveTravaValidacaoAlteracaoFluxo);

            bool alteracaoCfop = ValidarSeOcorreuAlteracaoCfop(cte, fluxoComercial, out validaAlteracaoCfop);

            // Os tr�s primeiros dig�tos do Cfop n�o podem ser alterados
            if (alteracaoCfop && !validaAlteracaoCfop)
            {
                throw new TranslogicException("Cfop n�o pode ser alterado");
            }

            // Al�quota n�o pode ser alterada
            if (cte.PercentualAliquotaIcms != fluxoComercial.Contrato.PercentualAliquotaIcms)
            {
                throw new TranslogicException("Al�quota ICMS n�o pode ser alterada");
            }

            if (chave.Valor.Equals("S"))
            {
                // Verifica se o remetente foi alterado.
                if (alteracaoRemetente)
                {
                    throw new TranslogicException("Remetente fiscal do Cte n�o pode ser alterado!!!");
                }

                // Verifica se o destinat�rio foi alterado.
                if (alteracaoDestinatario)
                {
                    throw new TranslogicException("Destinat�rio fiscal do Cte n�o pode ser alterado!!!");
                }

                // Tomador n�o pode ser alterado
                if (empresas.CodigoTomador == 4)
                {
                    if (alteracaoTomador)
                    {
                        throw new TranslogicException("Tomador do Cte n�o pode ser alterado");
                    }
                }

                // Se o expedidor for o tomador, o mesmo n�o pode ser alterado
                if (empresas.CodigoTomador == 1)
                {
                    if (alteracaoExpedidor)
                    {
                        throw new TranslogicException("Expedidor n�o pode ser alterado, pois o mesmo � o Tomador do Cte");
                    }
                }

                // Se o recebedor for o tomador, o mesmo n�o pode ser alterado
                if (empresas.CodigoTomador == 2)
                {
                    if (alteracaoRecebedor)
                    {
                        throw new TranslogicException("Recebedor n�o pode ser alterado, pois o mesmo � o Tomador do Cte");
                    }
                }
            }

            // Verifica se foi corrigido alguma informa��o
            if (alteracaoExpedidor || alteracaoRecebedor || alteracaoCfop)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Realiza a valida��o da troca do fluxo comercial com o objetivo de realizar uma corre��o em Cte.
        /// Ao trocar o fluxo, n�o deve ser poss�vel ser alterado o expedidor e o recebedor.  
        /// </summary>
        /// <param name="codigoFluxoComercial">C�digo do fluxo comercial</param>
        /// <param name="idCte">Identificador do Cte</param>
        /// <param name="mensagem">Mensagem de valida��o</param>
        /// <returns> Se o fluxo comercial for v�lido o mesmo ser� retornado </returns>
        public bool ValidarCorrecaoDoFluxoComercial(string codigoFluxoComercial, int idCte, out string mensagem)
        {
            FluxoComercial fluxoComercial = _fluxoComercialRepository.ObterPorCodigoSimplificado(codigoFluxoComercial);
            Cte cte = ObterCtePorId(idCte);
            CteEmpresas empresas = ObterListaDeEmpresas(cte);
            IEmpresa remetente = ObterEmpresaRemetenteFiscal(cte.FluxoComercial.Codigo.Substring(2), cte.Id.Value);
            IEmpresa destinatario = ObterEmpresaDestinatariaFiscal(cte.FluxoComercial.Codigo.Substring(2), cte.Id.Value);
            StringBuilder mensagemErro = new StringBuilder();
            mensagem = string.Empty;

            ConfiguracaoTranslogic chave = _configuracaoTranslogicRepository.ObterPorId(_chaveTravaValidacaoAlteracaoFluxo);

            // Verifica se o remetente foi alterado.
            if (remetente.TipoPessoa == TipoPessoaEnum.Fisica)
            {
                if (remetente.Cpf != fluxoComercial.Contrato.EmpresaRemetenteFiscal.Cpf)
                {
                    mensagemErro.Append("<br/> - Remetente fiscal do Cte n�o pode ser alterado.");
                }
            }
            else
            {
                if (remetente.Cgc != fluxoComercial.Contrato.EmpresaRemetenteFiscal.Cgc)
                {
                    mensagemErro.Append("<br/> - Remetente fiscal do Cte n�o pode ser alterado.");
                }
            }

            // Verifica se o destinat�rio foi alterado.
            if (remetente.TipoPessoa == TipoPessoaEnum.Fisica)
            {
                if (destinatario.Cpf != fluxoComercial.Contrato.EmpresaDestinatariaFiscal.Cpf)
                {
                    mensagemErro.Append("<br/> - Destinat�rio fiscal do Cte n�o pode ser alterado.");
                }
            }
            else
            {
                if (destinatario.Cgc != fluxoComercial.Contrato.EmpresaDestinatariaFiscal.Cgc)
                {
                    mensagemErro.Append("<br/> - Destinat�rio fiscal do Cte n�o pode ser alterado.");
                }
            }

            // Tomador n�o pode ser alterado
            if (empresas.CodigoTomador == 4)
            {
                if (empresas.EmpresaTomadora.Cgc != fluxoComercial.Contrato.EmpresaPagadora.Cgc)
                {
                    mensagemErro.Append("<br/> - Tomador do Cte n�o pode ser alterado.");
                }
            }

            // Al�quota n�o pode ser alterada
            if (cte.PercentualAliquotaIcms != fluxoComercial.Contrato.PercentualAliquotaIcms)
            {
                mensagemErro.Append("<br/> - Al�quota ICMS n�o pode ser alterada.");
            }

            // Os tr�s primeiros dig�tos do Cfop n�o podem ser alterados
            if (cte.Cfop.Substring(0, 3) != fluxoComercial.Contrato.Cfop.Substring(0, 3))
            {
                mensagemErro.Append("<br/> - Cfop n�o pode ser alterado");
            }

            if (chave.Valor.Equals("S"))
            {
                // Se o expedidor for o tomador, o mesmo n�o pode ser alterado
                if (empresas.CodigoTomador == 1)
                {
                    if (empresas.EmpresaExpedidor.Cgc != fluxoComercial.EmpresaRemetente.Cgc)
                    {
                        mensagemErro.Append("<br/> - Expedidor n�o pode ser alterado, pois o mesmo � o Tomador do Cte.");
                    }
                }

                // Se o recebedor for o tomador, o mesmo n�o pode ser alterado
                if (empresas.CodigoTomador == 2)
                {
                    if (empresas.EmpresaRecebedor.Cgc != fluxoComercial.EmpresaDestinataria.Cgc)
                    {
                        mensagemErro.Append("<br/> - Recebedor n�o pode ser alterado, pois o mesmo � o Tomador do Cte.");
                    }
                }
            }

            if (!string.IsNullOrEmpty(mensagemErro.ToString()))
            {
                mensagem = mensagemErro.ToString();
                return false;
            }

            if (empresas.EmpresaExpedidor.Cgc != fluxoComercial.EmpresaRemetente.Cgc)
            {
                return true;
            }

            if (empresas.EmpresaRecebedor.Cgc != fluxoComercial.EmpresaDestinataria.Cgc)
            {
                return true;
            }

            if (cte.ContratoHistorico.Cfop != fluxoComercial.Contrato.Cfop)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Salva uma entidade do tipo CorrecaoCte no banco de dados
        /// </summary>
        /// <param name="cteDto">Entidade cteDto</param>
        /// <param name="usuario">usuario logado</param>
        [Transaction]
        public virtual void SalvarCorrecaoCte(CorrecaoCteDto cteDto, Usuario usuario)
        {
            try
            {
                if (cteDto.CorrecaoCte || cteDto.CorrecaoConteinerCte)
                {
                    if (cteDto.CorrecaoCte)
                    {
                        var correcaoCte = new CorrecaoCte { Cte = new Cte { Id = cteDto.CteId }, Usuario = usuario, DataCorrecao = DateTime.Now };

                        if (!string.IsNullOrEmpty(cteDto.CodFluxo))
                        {
                            var fluxo = VerificarValidadeFluxo(cteDto.CodFluxo);
                            correcaoCte.FluxoComercial = fluxo;
                        }

                        if (!string.IsNullOrEmpty(cteDto.CodigoVagao))
                        {
                            correcaoCte.Vagao = ObterVagaoPorCodigo(cteDto.CodigoVagao);
                        }

                        if (!string.IsNullOrEmpty(cteDto.Observacao))
                        {
                            correcaoCte.Observacao = cteDto.Observacao;
                        }

                        _correcaoCteRepository.Inserir(correcaoCte);
                    }

                    if (cteDto.CorrecaoConteinerCte)
                    {
                        foreach (var conteiner in cteDto.CorrecaoConteiner)
                        {
                            var correcaoConteinerCte = new CorrecaoConteinerCte() { Cte = new Cte { Id = cteDto.CteId }, Usuario = usuario, DataCorrecao = DateTime.Now };
                            correcaoConteinerCte.ConteinerNotaFiscal = conteiner.ConteinerNfe;
                            correcaoConteinerCte.ConteinerCorrigidoNotaFiscal = conteiner.ConteinerCorrigido;
                            correcaoConteinerCte.ChaveNfe = conteiner.ChaveNfe;
                            _correcaoConteinerCteRepository.Inserir(correcaoConteinerCte);
                        }
                    }

                    Cte cte = ObterCtePorId(cteDto.CteId);
                    MudarSituacaoCte(cte, SituacaoCteEnum.PendenteCorrecaoEnvio, usuario, new CteStatusRetorno { Id = 37 }, "Arquivo Cte alterado para aguardando corre��o", null);
                }
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(null, "CteService", string.Format("SalvarCorrecaoCte: {0}", ex.Message), ex);
                throw;
            }
        }

        /// <summary>
        /// Retorna um Cte pelo Identificador
        /// </summary>
        /// <param name="cteId">Identificador do Cte</param>
        /// <returns>Retorna o Cte</returns>
        public Cte ObterCtePorId(int cteId)
        {
            return _cteRepository.ObterPorId(cteId);
        }

        /// <summary>
        /// Retorna a lista de Cte pelo Id do despacho
        /// </summary>
        /// <param name="despachoId">Identificador do Despacho</param>
        /// <returns>Retorna a lista de Ctes</returns>
        public IList<Cte> ObterCtesPorDespacho(int despachoId)
        {
            return _cteRepository.ObterCtesPorDespacho(despachoId);
        }

        /// <summary>
        /// Verifica se existe o status de retorno informado para o cte
        /// </summary>
        /// <param name="cteId">Identificador do Cte</param>
        /// <param name="codigoStatusRetorno">C�digo de Status do Retorno</param>
        /// <returns>Retorna a lista de Ctes</returns>
        public bool ExisteStatusRetornoCte(int cteId, int codigoStatusRetorno)
        {
            return _cteRepository.ExisteStatusRetornoCte(cteId, codigoStatusRetorno);
        }

        /// <summary>
        /// Retorna um Fluxo Comercial pelo Identificador
        /// </summary>
        /// <param name="fluxoId">Identificador do Cte</param>
        /// <returns>Retorna o Cte</returns>
        public FluxoComercial ObterFluxoComercialPorId(int fluxoId)
        {
            return _fluxoComercialRepository.ObterPorId(fluxoId);
        }

        /// <summary>
        /// Obtem os dados da empresa remetente fiscal
        /// </summary>
        /// <param name="codFluxoComercial">Fluxo comercial</param>
        /// <param name="cteId">Cte comercial</param>
        /// <returns> Empresa Remetente </returns>
        public IEmpresa ObterEmpresaRemetenteFiscal(string codFluxoComercial, int cteId)
        {
            IList<CteDetalhe> _listaCteDetalhe = ObterCteDetalhePorIdCte(cteId);
            FluxoComercial fluxoComercial = _fluxoComercialRepository.ObterPorCodigoSimplificado(codFluxoComercial);

            if ((_listaCteDetalhe == null) || (_listaCteDetalhe.Count == 0))
            {
                return null;
            }

            IEmpresa empresaRemetente;
            CidadeIbge _cidadeIbgeRemetenteFiscal;
            Contrato _contrato = fluxoComercial.Contrato;
            int aux;

            // Verifica se � um Fluxo da Brado
            if (VerificarLiberacaoTravasCorrentista(fluxoComercial))
            {
                // Caso seja fluxo brado, ent�o recupera o REMETENTE e DESTINATARIO da nota fiscal.
                INotaFiscalEletronica nota = _nfeService.ObterDadosNfeBancoDados(_listaCteDetalhe[0].ChaveNfe);

                if (nota != null)
                {
                    // Configura o objeto Empresa Remetente
                    empresaRemetente = new EmpresaCliente();
                    empresaRemetente.TipoPessoa = TipoPessoaEnum.Juridica;
                    empresaRemetente.Cgc = nota.CnpjEmitente;
                    empresaRemetente.InscricaoEstadual = nota.InscricaoEstadualEmitente;
                    empresaRemetente.RazaoSocial = nota.RazaoSocialEmitente;
                    empresaRemetente.NomeFantasia = nota.NomeFantasiaEmitente;
                    empresaRemetente.Telefone1 = nota.TelefoneEmitente;
                    empresaRemetente.Endereco = nota.LogradouroEmitente;
                    empresaRemetente.Cep = nota.CepEmitente;

                    // Tenta pega pelo codigo do municipio, caso nao venha, entao recupera pelo nome do municipio
                    if (int.TryParse(nota.CodigoMunicipioEmitente, out aux))
                    {
                        empresaRemetente.CidadeIbge = _cidadeIbgeRepository.ObterPorCodigoMunicipioIbge(aux);
                        _cidadeIbgeRemetenteFiscal = empresaRemetente.CidadeIbge;
                        if (_cidadeIbgeRemetenteFiscal.CodigoIbge == 9999999)
                        {
                            PaisBacen paisAux = _paisBacenRepository.ObterPorCodigoPais(nota.CodigoPaisEmitente);
                            if (paisAux == null)
                            {
                                if (int.TryParse(nota.PaisEmitente, out aux))
                                {
                                    paisAux = _paisBacenRepository.ObterPorCodigoPais(nota.PaisEmitente);
                                }
                                else
                                {
                                    paisAux = _paisBacenRepository.ObterPorDescricaoPais(nota.PaisEmitente);
                                }
                            }

                            if (paisAux == null)
                            {
                                return null;
                            }

                            empresaRemetente.EnderecoSap = paisAux.SiglaResumida;
                        }
                    }
                    else
                    {
                        if (nota.MunicipioDestinatario != null)
                        {
                            empresaRemetente.CidadeIbge = _cidadeIbgeRepository.ObterPorDescricaoUf(nota.MunicipioDestinatario.ToUpper(), nota.UfDestinatario);
                        }
                        else
                        {
                            if (nota.UfDestinatario == "EX")
                            {
                                _cidadeIbgeRemetenteFiscal = _cidadeIbgeRepository.ObterPorCodigoMunicipioIbge(9999999);
                                empresaRemetente.CidadeIbge = _cidadeIbgeRemetenteFiscal;
                            }
                        }
                    }

                    return empresaRemetente;
                }

                return _contrato.EmpresaRemetenteFiscal;
            }

            return _contrato.EmpresaRemetenteFiscal;
        }

        /// <summary>
        /// Obtem os dados da empresa destinat�ria fiscal
        /// </summary>
        /// <param name="codFluxoComercial">Fluxo comercial</param>
        /// <param name="cteId">Cte comercial</param>
        /// <returns> Empresa Remetente </returns>
        public IEmpresa ObterEmpresaDestinatariaFiscal(string codFluxoComercial, int cteId)
        {
            IEmpresa empresaDestinataria;
            CidadeIbge _cidadeIbgeDestinatariaFiscal;
            IList<CteDetalhe> _listaCteDetalhe = ObterCteDetalhePorIdCte(cteId);

            if ((_listaCteDetalhe == null) || (_listaCteDetalhe.Count == 0))
            {
                return null;
            }

            FluxoComercial fluxoComercial = _fluxoComercialRepository.ObterPorCodigoSimplificado(codFluxoComercial);
            Contrato _contrato = fluxoComercial.Contrato;
            int aux;

            // Verifica se � um Fluxo da Brado
            if (VerificarLiberacaoTravasCorrentista(fluxoComercial))
            {
                // Caso seja fluxo brado, ent�o recupera o REMETENTE e DESTINATARIO da nota fiscal.
                INotaFiscalEletronica nota = _nfeService.ObterDadosNfeBancoDados(_listaCteDetalhe[0].ChaveNfe);

                if (nota != null)
                {
                    // Configura o objeto Empresa Destinatario
                    empresaDestinataria = new EmpresaCliente();
                    empresaDestinataria.TipoPessoa = TipoPessoaEnum.Juridica;
                    empresaDestinataria.Cgc = nota.CnpjDestinatario;
                    empresaDestinataria.InscricaoEstadual = nota.InscricaoEstadualDestinatario;
                    empresaDestinataria.RazaoSocial = nota.RazaoSocialDestinatario;
                    empresaDestinataria.NomeFantasia = nota.NomeFantasiaDestinatario;
                    empresaDestinataria.Telefone1 = nota.TelefoneDestinatario;
                    empresaDestinataria.Endereco = nota.LogradouroDestinatario;
                    empresaDestinataria.Cep = nota.CepDestinatario;

                    // Tenta pega pelo codigo do municipio, caso nao venha, entao recupera pelo nome do municipio
                    if (int.TryParse(nota.CodigoMunicipioDestinatario, out aux))
                    {
                        empresaDestinataria.CidadeIbge = _cidadeIbgeRepository.ObterPorCodigoMunicipioIbge(aux);
                        _cidadeIbgeDestinatariaFiscal = empresaDestinataria.CidadeIbge;
                        if (_cidadeIbgeDestinatariaFiscal.CodigoIbge == 9999999)
                        {
                            PaisBacen paisAux = _paisBacenRepository.ObterPorCodigoPais(nota.CodigoPaisDestinatario);
                            if (paisAux == null)
                            {
                                if (int.TryParse(nota.PaisDestinatario, out aux))
                                {
                                    paisAux = _paisBacenRepository.ObterPorCodigoPais(nota.PaisDestinatario);
                                }
                                else
                                {
                                    paisAux = _paisBacenRepository.ObterPorDescricaoPais(nota.PaisDestinatario);
                                }
                            }

                            if (paisAux == null)
                            {
                                return null;
                            }

                            empresaDestinataria.EnderecoSap = paisAux.SiglaResumida;
                        }
                    }
                    else
                    {
                        if (nota.MunicipioDestinatario != null)
                        {
                            empresaDestinataria.CidadeIbge = _cidadeIbgeRepository.ObterPorDescricaoUf(nota.MunicipioDestinatario.ToUpper(), nota.UfDestinatario);
                        }
                        else
                        {
                            if (nota.UfDestinatario == "EX")
                            {
                                _cidadeIbgeDestinatariaFiscal = _cidadeIbgeRepository.ObterPorCodigoMunicipioIbge(9999999);
                                empresaDestinataria.CidadeIbge = _cidadeIbgeDestinatariaFiscal;
                            }
                        }
                    }

                    return empresaDestinataria;
                }

                return _contrato.EmpresaDestinatariaFiscal;
            }

            return _contrato.EmpresaDestinatariaFiscal;
        }

        /// <summary>
        /// Verifica se o fluxo comercial � v�lido
        /// </summary>
        /// <param name="codigoFluxo">C�digo do fluxo comercial</param>
        /// <returns> Se o fluxo comercial for v�lido o mesmo ser� retornado </returns>
        public FluxoComercial VerificarValidadeFluxo(string codigoFluxo)
        {
            FluxoComercial fluxoComercial = _fluxoComercialRepository.ObterPorCodigoSimplificado(codigoFluxo);
            var mensagemErro = new StringBuilder();

            if (fluxoComercial == null)
            {
                throw new TranslogicException("Fluxo n�o cadastrado.");
            }

            mensagemErro.Append(VerificarFluxoSaneado(fluxoComercial));

            if (mensagemErro.ToString().Trim() != string.Empty)
            {
                throw new TranslogicException(string.Concat("Foram encontrados os erros abaixo:<br/>", mensagemErro.ToString()));
            }

            return fluxoComercial;
        }

        /// <summary>
        /// Verifica se o fluxo da Cte est� sofrendo altera��es que n�o podem ocorrer na troca de Fluxo da Cte.
        /// Para Cte's com nota de anula��o, algumas informa��es do fluxo da Cte n�o podem ser alteradas,
        /// sendo assumido que somente dados relacionados a valores podem sofrer altera��es.
        /// </summary>
        /// <param name="novoFluxoComercial">Fluxo informado pelo usu�rio</param>
        /// <param name="idCte">Cte selecionada pelo usu�rio</param>
        public void ValidarAlteracaoFluxoComercialCteComNotaAnulacao(FluxoComercial novoFluxoComercial, int idCte)
        {
            var indGeracaoSubstituto = _configuracaoTranslogicRepository.ObterPorId("CTE_IND_GERACAO_SUBSTITUTO");

            if (indGeracaoSubstituto != null && indGeracaoSubstituto.Valor.Trim().ToUpper() == "S")
            {
                Cte cte = ObterCtePorId(idCte);

                var mensagemValidacao = new StringBuilder();

                if (cte != null)
                {
                    if (cte.NfeAnulacao != null)
                    {
                        CteEmpresas empresas = ObterListaDeEmpresas(cte);
                        IEmpresa remetente = ObterEmpresaRemetenteFiscal(cte.FluxoComercial.Codigo.Substring(2), cte.Id.Value);
                        IEmpresa destinatario = ObterEmpresaDestinatariaFiscal(cte.FluxoComercial.Codigo.Substring(2), cte.Id.Value);

                        // Verifica se o remetente foi alterado.
                        if (remetente.TipoPessoa == TipoPessoaEnum.Fisica)
                        {
                            if (remetente.Cpf != novoFluxoComercial.Contrato.EmpresaRemetenteFiscal.Cpf)
                            {
                                mensagemValidacao.AppendLine("Remetente fiscal do Cte n�o pode ser alterado!!!");
                            }
                        }
                        else
                        {
                            if (remetente.Cgc != novoFluxoComercial.Contrato.EmpresaRemetenteFiscal.Cgc)
                            {
                                mensagemValidacao.AppendLine("Remetente fiscal do Cte n�o pode ser alterado!!!");
                            }
                        }

                        // Verifica se o destinat�rio foi alterado.
                        if (destinatario.TipoPessoa == TipoPessoaEnum.Fisica)
                        {
                            if (destinatario.Cpf != novoFluxoComercial.Contrato.EmpresaDestinatariaFiscal.Cpf)
                            {
                                if (mensagemValidacao.Length > 0)
                                {
                                    mensagemValidacao.Append("<br />");
                                }

                                mensagemValidacao.AppendLine("Destinat�rio fiscal do Cte n�o pode ser alterado!!!");
                            }
                        }
                        else
                        {
                            if (destinatario.Cgc != novoFluxoComercial.Contrato.EmpresaDestinatariaFiscal.Cgc)
                            {
                                if (mensagemValidacao.Length > 0)
                                {
                                    mensagemValidacao.Append("<br />");
                                }

                                mensagemValidacao.AppendLine("Destinat�rio fiscal do Cte n�o pode ser alterado!!!");
                            }
                        }

                        // Tomador n�o pode ser alterado
                        // if (empresas.CodigoTomador == 4)
                        // {
                        //     if (empresas.EmpresaTomadora.Cgc != novoFluxoComercial.Contrato.EmpresaPagadora.Cgc)
                        //     {
                        //         throw new TranslogicException("Tomador do Cte n�o pode ser alterado");
                        //     }
                        // }

                        if (empresas.EmpresaExpedidor.Cgc != novoFluxoComercial.EmpresaRemetente.Cgc)
                        {
                            if (mensagemValidacao.Length > 0)
                            {
                                mensagemValidacao.Append("<br />");
                            }

                            mensagemValidacao.AppendLine("Expedidor do Cte n�o pode ser alterado!!!");
                        }

                        if (empresas.EmpresaRecebedor.Cgc != novoFluxoComercial.EmpresaDestinataria.Cgc)
                        {
                            if (mensagemValidacao.Length > 0)
                            {
                                mensagemValidacao.Append("<br />");
                            }

                            mensagemValidacao.AppendLine("Expedidor do Cte n�o pode ser alterado!!!");
                        }

                        if (mensagemValidacao.Length > 0)
                        {
                            throw new TransactionException(mensagemValidacao.ToString());
                        }
                    }
                }
                else
                {
                    throw new TransactionException(string.Format("N�o foi poss�vel obter a CT-e pelo Id: {0}", idCte));
                }
            }
        }

        /// <summary>
        /// Obt�m o remetente do xml de envio
        /// </summary>
        /// <param name="cteid">Identificador do Cte</param>
        /// <returns>Retorna a o remetente do Cte</returns>
        public IEmpresa ObterEmpresaRemetenteXmlEnvio(int cteid)
        {
            CteArquivo cteArquivo = _cteArquivoRepository.ObterPorId(cteid);

            if (cteArquivo != null)
            {
                IEmpresa empresaXml = new EmpresaCliente();
                empresaXml.CidadeIbge = new CidadeIbge();

                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(cteArquivo.ArquivoXml);
                XmlElement root = xmlDoc.DocumentElement;
                XmlNamespaceManager nsmgr = new XmlNamespaceManager(xmlDoc.NameTable);
                nsmgr.AddNamespace("cte", root.NamespaceURI);

                XmlNode infCte = root.SelectSingleNode("//cte:CTe/cte:infCte/cte:rem", nsmgr);

                XmlDocument docEmp = new XmlDocument();
                docEmp.LoadXml(infCte.OuterXml);
                empresaXml.Cgc = ObterTextoNoXml(docEmp, "CNPJ", nsmgr);
                empresaXml.InscricaoEstadual = ObterTextoNoXml(docEmp, "IE", nsmgr);
                empresaXml.RazaoSocial = ObterTextoNoXml(docEmp, "xNome", nsmgr);
                empresaXml.NomeFantasia = ObterTextoNoXml(docEmp, "xFant", nsmgr);
                empresaXml.Telefone1 = ObterTextoNoXml(docEmp, "fone", nsmgr);

                infCte = root.SelectSingleNode("//cte:CTe/cte:infCte/cte:rem/cte:enderReme", nsmgr);
                XmlDocument docEnd = new XmlDocument();
                docEnd.LoadXml(infCte.OuterXml);
                empresaXml.Endereco = ObterTextoNoXml(docEnd, "xLgr", nsmgr);
                empresaXml.Cep = ObterTextoNoXml(docEnd, "CEP", nsmgr);
                empresaXml.CidadeIbge.Descricao = ObterTextoNoXml(docEnd, "xMun", nsmgr);
                empresaXml.CidadeIbge.SiglaEstado = ObterTextoNoXml(docEnd, "UF", nsmgr);
                empresaXml.CidadeIbge.SiglaPais = ObterTextoNoXml(docEnd, "xPais", nsmgr);

                return empresaXml;
            }
            else
                return null;
        }

        /// <summary>
        /// Obt�m o destinat�rio xml de envio
        /// </summary>
        /// <param name="cteid">Identificador do Cte</param>
        /// <returns>Retorna a o destinat�rio do Cte</returns>
        public IEmpresa ObterEmpresaDestinatarioXmlEnvio(int cteid)
        {
            CteArquivo cteArquivo = _cteArquivoRepository.ObterPorId(cteid);
            if (cteArquivo == null)
                return null;

            IEmpresa empresaXml = new EmpresaCliente();
            empresaXml.CidadeIbge = new CidadeIbge();

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(cteArquivo.ArquivoXml);
            XmlElement root = xmlDoc.DocumentElement;
            XmlNamespaceManager nsmgr = new XmlNamespaceManager(xmlDoc.NameTable);
            nsmgr.AddNamespace("cte", root.NamespaceURI);

            XmlNode infCte = root.SelectSingleNode("//cte:CTe/cte:infCte/cte:dest", nsmgr);

            XmlDocument docEmp = new XmlDocument();
            docEmp.LoadXml(infCte.OuterXml);
            empresaXml.Cgc = ObterTextoNoXml(docEmp, "CNPJ", nsmgr);
            empresaXml.InscricaoEstadual = ObterTextoNoXml(docEmp, "IE", nsmgr);
            empresaXml.RazaoSocial = ObterTextoNoXml(docEmp, "xNome", nsmgr);
            empresaXml.NomeFantasia = ObterTextoNoXml(docEmp, "xFant", nsmgr);
            empresaXml.Telefone1 = ObterTextoNoXml(docEmp, "fone", nsmgr);

            infCte = root.SelectSingleNode("//cte:CTe/cte:infCte/cte:dest/cte:enderDest", nsmgr);
            XmlDocument docEnd = new XmlDocument();
            docEnd.LoadXml(infCte.OuterXml);
            empresaXml.Endereco = ObterTextoNoXml(docEnd, "xLgr", nsmgr);
            empresaXml.Cep = ObterTextoNoXml(docEnd, "CEP", nsmgr);
            empresaXml.CidadeIbge.Descricao = ObterTextoNoXml(docEnd, "xMun", nsmgr);
            empresaXml.CidadeIbge.SiglaEstado = ObterTextoNoXml(docEnd, "UF", nsmgr);
            empresaXml.CidadeIbge.SiglaPais = ObterTextoNoXml(docEnd, "xPais", nsmgr);

            return empresaXml;
        }

        /// <summary>
        /// Obt�m o expedidor do xml de envio
        /// </summary>
        /// <param name="cteid">Identificador do Cte</param>
        /// <returns>Retorna o expedidor do Cte</returns>
        public IEmpresa ObterEmpresaExpedidorXmlEnvio(int cteid)
        {
            CteArquivo cteArquivo = _cteArquivoRepository.ObterPorId(cteid);

            if (cteArquivo == null)
                return null;

            IEmpresa empresaXml = new EmpresaCliente();
            empresaXml.CidadeIbge = new CidadeIbge();

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(cteArquivo.ArquivoXml);
            XmlElement root = xmlDoc.DocumentElement;
            XmlNamespaceManager nsmgr = new XmlNamespaceManager(xmlDoc.NameTable);
            nsmgr.AddNamespace("cte", root.NamespaceURI);

            XmlNode infCte = root.SelectSingleNode("//cte:CTe/cte:infCte/cte:exped", nsmgr);

            XmlDocument docEmp = new XmlDocument();
            docEmp.LoadXml(infCte.OuterXml);
            empresaXml.Cgc = ObterTextoNoXml(docEmp, "CNPJ", nsmgr);
            empresaXml.InscricaoEstadual = ObterTextoNoXml(docEmp, "IE", nsmgr);
            empresaXml.RazaoSocial = ObterTextoNoXml(docEmp, "xNome", nsmgr);
            empresaXml.NomeFantasia = ObterTextoNoXml(docEmp, "xFant", nsmgr);
            empresaXml.Telefone1 = ObterTextoNoXml(docEmp, "fone", nsmgr);

            infCte = root.SelectSingleNode("//cte:CTe/cte:infCte/cte:exped/cte:enderExped", nsmgr);
            XmlDocument docEnd = new XmlDocument();
            docEnd.LoadXml(infCte.OuterXml);
            empresaXml.Endereco = ObterTextoNoXml(docEnd, "xLgr", nsmgr);
            empresaXml.Cep = ObterTextoNoXml(docEnd, "CEP", nsmgr);
            empresaXml.CidadeIbge.Descricao = ObterTextoNoXml(docEnd, "xMun", nsmgr);
            empresaXml.CidadeIbge.SiglaEstado = ObterTextoNoXml(docEnd, "UF", nsmgr);
            empresaXml.CidadeIbge.SiglaPais = ObterTextoNoXml(docEnd, "xPais", nsmgr);

            return empresaXml;
        }

        /// <summary>
        /// Obt�m o recebedor do xml de envio
        /// </summary>
        /// <param name="cteid">Identificador do Cte</param>
        /// <returns>Retorna o recebedor do Cte</returns>
        public IEmpresa ObterEmpresaRecebedorXmlEnvio(int cteid)
        {
            CteArquivo cteArquivo = _cteArquivoRepository.ObterPorId(cteid);
            if (cteArquivo == null)
                return null;

            IEmpresa empresaXml = new EmpresaCliente();
            empresaXml.CidadeIbge = new CidadeIbge();

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(cteArquivo.ArquivoXml);
            XmlElement root = xmlDoc.DocumentElement;
            XmlNamespaceManager nsmgr = new XmlNamespaceManager(xmlDoc.NameTable);
            nsmgr.AddNamespace("cte", root.NamespaceURI);

            XmlNode infCte = root.SelectSingleNode("//cte:CTe/cte:infCte/cte:receb", nsmgr);

            XmlDocument docEmp = new XmlDocument();
            docEmp.LoadXml(infCte.OuterXml);
            empresaXml.Cgc = ObterTextoNoXml(docEmp, "CNPJ", nsmgr);
            empresaXml.InscricaoEstadual = ObterTextoNoXml(docEmp, "IE", nsmgr);
            empresaXml.RazaoSocial = ObterTextoNoXml(docEmp, "xNome", nsmgr);
            empresaXml.NomeFantasia = ObterTextoNoXml(docEmp, "xFant", nsmgr);
            empresaXml.Telefone1 = ObterTextoNoXml(docEmp, "fone", nsmgr);

            infCte = root.SelectSingleNode("//cte:CTe/cte:infCte/cte:receb/cte:enderReceb", nsmgr);
            XmlDocument docEnd = new XmlDocument();
            docEnd.LoadXml(infCte.OuterXml);
            empresaXml.Endereco = ObterTextoNoXml(docEnd, "xLgr", nsmgr);
            empresaXml.Cep = ObterTextoNoXml(docEnd, "CEP", nsmgr);
            empresaXml.CidadeIbge.Descricao = ObterTextoNoXml(docEnd, "xMun", nsmgr);
            empresaXml.CidadeIbge.SiglaEstado = ObterTextoNoXml(docEnd, "UF", nsmgr);
            empresaXml.CidadeIbge.SiglaPais = ObterTextoNoXml(docEnd, "xPais", nsmgr);

            return empresaXml;
        }

        /// <summary>
        /// Obt�m uma determinada empresa do xml de envio
        /// </summary>
        /// <param name="cteid">Identificador do Cte</param>
        /// <returns>Retorna a o destinat�rio do Cte</returns>
        public IEmpresa ObterEmpresaTomadorXmlEnvio(int cteid)
        {
            CteArquivo cteArquivo = _cteArquivoRepository.ObterPorId(cteid);
            if (cteArquivo == null)
                return null;

            IEmpresa empresaXml = new EmpresaCliente();
            empresaXml.CidadeIbge = new CidadeIbge();
            string empresa = string.Empty;
            string endEmpresa = string.Empty;

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(cteArquivo.ArquivoXml);
            XmlElement root = xmlDoc.DocumentElement;
            XmlNamespaceManager nsmgr = new XmlNamespaceManager(xmlDoc.NameTable);
            nsmgr.AddNamespace("cte", root.NamespaceURI);

            XmlNode infTomador = root.SelectSingleNode("//cte:CTe/cte:infCte/cte:ide/cte:toma03", nsmgr);
            XmlDocument docTomador = new XmlDocument();
            string tomador = string.Empty;

            if (infTomador == null)
            {
                infTomador = root.SelectSingleNode("//cte:CTe/cte:infCte/cte:ide/cte:toma3", nsmgr);
            }

            if (infTomador == null)
            {
                infTomador = root.SelectSingleNode("//cte:CTe/cte:infCte/cte:ide/cte:toma4", nsmgr);
            }

            if (infTomador == null)
            {
                throw new ArgumentException("N�o foi poss�vel localizar o tomador. Entre em contato com a �rea respos�vel");
            }

            docTomador.LoadXml(infTomador.OuterXml);
            tomador = ObterTextoNoXml(docTomador, "toma", nsmgr);

            switch (tomador)
            {
                case "0":
                    empresa = "rem";
                    endEmpresa = "enderReme";
                    break;
                case "1":
                    empresa = "exped";
                    endEmpresa = "enderExped";
                    break;
                case "2":
                    empresa = "receb";
                    endEmpresa = "enderReceb";
                    break;
                case "3":
                    empresa = "dest";
                    endEmpresa = "enderDest";
                    break;
                case "4":
                    empresa = "ide";
                    endEmpresa = "toma4/cte:enderToma";
                    break;
            }

            XmlNode infCte = root.SelectSingleNode(string.Format("//cte:CTe/cte:infCte/cte:{0}", empresa), nsmgr);

            XmlDocument docEmp = new XmlDocument();
            docEmp.LoadXml(infCte.OuterXml);
            empresaXml.Cgc = ObterTextoNoXml(docEmp, "CNPJ", nsmgr);
            empresaXml.InscricaoEstadual = ObterTextoNoXml(docEmp, "IE", nsmgr);
            empresaXml.RazaoSocial = ObterTextoNoXml(docEmp, "xNome", nsmgr);
            empresaXml.NomeFantasia = ObterTextoNoXml(docEmp, "xFant", nsmgr);
            empresaXml.Telefone1 = ObterTextoNoXml(docEmp, "fone", nsmgr);

            infCte = root.SelectSingleNode(string.Format("//cte:CTe/cte:infCte/cte:{0}/cte:{1}", empresa, endEmpresa), nsmgr);
            XmlDocument docEnd = new XmlDocument();
            docEnd.LoadXml(infCte.OuterXml);
            empresaXml.Endereco = ObterTextoNoXml(docEnd, "xLgr", nsmgr);
            empresaXml.Cep = ObterTextoNoXml(docEnd, "CEP", nsmgr);
            empresaXml.CidadeIbge.Descricao = ObterTextoNoXml(docEnd, "xMun", nsmgr);
            empresaXml.CidadeIbge.SiglaEstado = ObterTextoNoXml(docEnd, "UF", nsmgr);
            empresaXml.CidadeIbge.SiglaPais = ObterTextoNoXml(docEnd, "xPais", nsmgr);

            return empresaXml;
        }

        /// <summary>
        /// Obt�m o Cfop do xml de envio
        /// </summary>
        /// <param name="cteid">Identificador do Cte</param>
        /// <returns>Retorna o Cfop Cte</returns>
        public string ObterCfopXmlEnvio(int cteid)
        {
            CteArquivo cteArquivo = _cteArquivoRepository.ObterPorId(cteid);
            if (cteArquivo != null)
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(cteArquivo.ArquivoXml);

                XmlElement root = xmlDoc.DocumentElement;
                XmlNamespaceManager nsmgr = new XmlNamespaceManager(xmlDoc.NameTable);
                nsmgr.AddNamespace("cte", root.NamespaceURI);
                XmlNode infCte = root.SelectSingleNode("//cte:CTe/cte:infCte/cte:ide", nsmgr);
                return ObterTextoNoXml(infCte, "CFOP", nsmgr);
            }
            else
                return "";
        }

        /// <summary>
        /// Obt�m o CNPJ do Recebedor Cte do xml de envio
        /// </summary>
        /// <param name="cteid">Identificador do Cte</param>
        /// <returns>Retorna o CNPJ do Recebedor do Cte</returns>
        public string ObterRecebedorCteXmlEnvio(int cteid)
        {
            CteArquivo cteArquivo = _cteArquivoRepository.ObterPorId(cteid);
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(cteArquivo.ArquivoXml);

            XmlElement root = xmlDoc.DocumentElement;
            XmlNamespaceManager nsmgr = new XmlNamespaceManager(xmlDoc.NameTable);
            nsmgr.AddNamespace("cte", root.NamespaceURI);
            var cnpjReceb = root.SelectSingleNode("//cte:CTe/cte:infCte/cte:receb/cte:CNPJ", nsmgr) == null ? string.Empty : root.SelectSingleNode("//cte:CTe/cte:infCte/cte:receb/cte:CNPJ", nsmgr).InnerText;

            return cnpjReceb;
        }

        /// <summary>
        /// Insere o Cte Empresa por Cte
        /// </summary>
        /// <param name="cteId">Identificador do Cte</param>
        /// <returns>Retorna se foi poss�vel salvar o CteEmpresa</returns>
        public bool InserirCteEmpresaPorCte(int cteId)
        {
            try
            {
                string cteArquivo = _cteArquivoRepository.ObterArqPorId(cteId);
                XmlDocument xmlDoc = new XmlDocument();

                if (cteArquivo != null && !string.IsNullOrEmpty(cteArquivo))
                {
                    xmlDoc.LoadXml(cteArquivo);

                    XmlElement root = xmlDoc.DocumentElement;
                    XmlNamespaceManager nsmgr = new XmlNamespaceManager(xmlDoc.NameTable);
                    nsmgr.AddNamespace("cte", root.NamespaceURI);
                    var cnpjRem = root.SelectSingleNode("//cte:CTe/cte:infCte/cte:rem/cte:CNPJ", nsmgr) == null ? string.Empty : root.SelectSingleNode("//cte:CTe/cte:infCte/cte:rem/cte:CNPJ", nsmgr).InnerText;
                    var cnpjExped = root.SelectSingleNode("//cte:CTe/cte:infCte/cte:exped/cte:CNPJ", nsmgr) == null ? string.Empty : root.SelectSingleNode("//cte:CTe/cte:infCte/cte:exped/cte:CNPJ", nsmgr).InnerText;
                    var cnpjReceb = root.SelectSingleNode("//cte:CTe/cte:infCte/cte:receb/cte:CNPJ", nsmgr) == null ? string.Empty : root.SelectSingleNode("//cte:CTe/cte:infCte/cte:receb/cte:CNPJ", nsmgr).InnerText;
                    var cnpjDest = root.SelectSingleNode("//cte:CTe/cte:infCte/cte:dest/cte:CNPJ", nsmgr) == null ? string.Empty : root.SelectSingleNode("//cte:CTe/cte:infCte/cte:dest/cte:CNPJ", nsmgr).InnerText;
                    var cnpjToma = string.Empty;
                    XmlNode infCte = root.SelectSingleNode("//cte:CTe/cte:infCte", nsmgr);
                    if (infCte != null)
                    {
                        var ide = infCte.SelectSingleNode("//cte:ide", nsmgr);
                        var cnpjTomaAux = ide.SelectSingleNode("//cte:toma03", nsmgr);
                        int? codigoTomador = null;

                        if (cnpjTomaAux == null)
                        {
                            cnpjToma = ide.SelectSingleNode("//cte:toma4/cte:CNPJ", nsmgr) == null
                                           ? string.Empty
                                           : ide.SelectSingleNode("//cte:toma4/cte:CNPJ", nsmgr).InnerText;
                        }
                        else
                        {
                            var toma = ide.SelectSingleNode("//cte:toma03/cte:toma", nsmgr).InnerText;
                            switch (Convert.ToInt32(toma))
                            {
                                case 0:
                                    cnpjToma = cnpjRem;
                                    codigoTomador = 0;
                                    break;
                                case 1:
                                    cnpjToma = cnpjExped;
                                    codigoTomador = 1;
                                    break;
                                case 2:
                                    cnpjToma = cnpjReceb;
                                    codigoTomador = 2;
                                    break;
                                case 3:
                                    cnpjToma = cnpjDest;
                                    codigoTomador = 3;
                                    break;
                                default:
                                    return false;
                            }
                        }

                        CteEmpresas cteEmpresa = new CteEmpresas();
                        cteEmpresa.Cte = this.ObterCtePorId(cteId);
                        cteEmpresa.EmpresaDestinatario = cnpjDest == string.Empty
                                                             ? null
                                                             : _empresaRepository.ObterPorCnpj(cnpjDest);
                        cteEmpresa.EmpresaExpedidor = cnpjExped == string.Empty
                                                          ? null
                                                          : _empresaRepository.ObterPorCnpj(cnpjExped);
                        cteEmpresa.EmpresaRecebedor = cnpjReceb == string.Empty
                                                          ? null
                                                          : _empresaRepository.ObterPorCnpj(cnpjReceb);
                        cteEmpresa.EmpresaRemetente = cnpjRem == string.Empty
                                                          ? null
                                                          : _empresaRepository.ObterPorCnpj(cnpjRem);
                        cteEmpresa.EmpresaTomadora = cnpjToma == string.Empty
                                                         ? null
                                                         : _empresaRepository.ObterPorCnpj(cnpjToma);
                        cteEmpresa.CodigoTomador = codigoTomador;

                        _cteEmpresaRepository.InserirSemSessao(cteEmpresa);

                        cteArquivo = null;
                        xmlDoc = null;
                        root = null;
                        nsmgr = null;
                        cnpjRem = null;
                        cnpjExped = null;
                        cnpjReceb = null;
                        cnpjDest = null;
                        cnpjToma = null;
                        infCte = null;
                        ide = null;
                        cnpjTomaAux = null;
                        codigoTomador = null;
                        cteEmpresa = null;

                        return true;
                    }
                    else
                    {
                        CteEmpresas cteEmpresa = new CteEmpresas();
                        cteEmpresa.Cte = this.ObterCtePorId(cteId);
                        cteEmpresa.EmpresaDestinatario = cteEmpresa.Cte.ContratoHistorico.EmpresaDestinatariaFiscal;
                        cteEmpresa.EmpresaExpedidor = cteEmpresa.Cte.FluxoComercial.EmpresaRemetente;
                        cteEmpresa.EmpresaRecebedor = cteEmpresa.Cte.FluxoComercial.EmpresaDestinataria;
                        cteEmpresa.EmpresaRemetente = cteEmpresa.Cte.ContratoHistorico.EmpresaRemetenteFiscal;
                        cteEmpresa.EmpresaTomadora = cteEmpresa.Cte.FluxoComercial.EmpresaPagadora;

                        if ((cteEmpresa.EmpresaTomadora != null && cteEmpresa.EmpresaRemetente != null) && cteEmpresa.EmpresaTomadora.Id == cteEmpresa.EmpresaRemetente.Id)
                        {
                            cteEmpresa.CodigoTomador = 0;
                        }
                        else if ((cteEmpresa.EmpresaTomadora != null && cteEmpresa.EmpresaExpedidor != null) && cteEmpresa.EmpresaTomadora.Id == cteEmpresa.EmpresaExpedidor.Id)
                        {
                            cteEmpresa.CodigoTomador = 1;
                        }
                        else if ((cteEmpresa.EmpresaTomadora != null && cteEmpresa.EmpresaRecebedor != null) && cteEmpresa.EmpresaTomadora.Id == cteEmpresa.EmpresaRecebedor.Id)
                        {
                            cteEmpresa.CodigoTomador = 2;
                        }
                        else if ((cteEmpresa.EmpresaTomadora != null && cteEmpresa.EmpresaDestinatario != null) && cteEmpresa.EmpresaTomadora.Id == cteEmpresa.EmpresaDestinatario.Id)
                        {
                            cteEmpresa.CodigoTomador = 3;
                        }
                        else
                        {
                            cteEmpresa.CodigoTomador = 4;
                        }

                        _cteEmpresaRepository.InserirSemSessao(cteEmpresa);

                        cteArquivo = null;
                        xmlDoc = null;
                        cteEmpresa = null;

                        return true;
                    }

                }
                else
                {
                    CteEmpresas cteEmpresa = new CteEmpresas();
                    cteEmpresa.Cte = this.ObterCtePorId(cteId);
                    cteEmpresa.EmpresaDestinatario = cteEmpresa.Cte.FluxoComercial.EmpresaDestinataria;
                    cteEmpresa.EmpresaExpedidor = cteEmpresa.Cte.FluxoComercial.EmpresaRemetente;
                    cteEmpresa.EmpresaRecebedor = cteEmpresa.Cte.FluxoComercial.EmpresaDestinataria;
                    cteEmpresa.EmpresaRemetente = cteEmpresa.Cte.FluxoComercial.EmpresaRemetente;
                    cteEmpresa.EmpresaTomadora = cteEmpresa.Cte.FluxoComercial.EmpresaPagadora;

                    if ((cteEmpresa.EmpresaTomadora != null && cteEmpresa.EmpresaRemetente != null) && cteEmpresa.EmpresaTomadora.Id == cteEmpresa.EmpresaRemetente.Id)
                    {
                        cteEmpresa.CodigoTomador = 0;
                    }
                    else if ((cteEmpresa.EmpresaTomadora != null && cteEmpresa.EmpresaExpedidor != null) && cteEmpresa.EmpresaTomadora.Id == cteEmpresa.EmpresaExpedidor.Id)
                    {
                        cteEmpresa.CodigoTomador = 1;
                    }
                    else if ((cteEmpresa.EmpresaTomadora != null && cteEmpresa.EmpresaRecebedor != null) && cteEmpresa.EmpresaTomadora.Id == cteEmpresa.EmpresaRecebedor.Id)
                    {
                        cteEmpresa.CodigoTomador = 2;
                    }
                    else if ((cteEmpresa.EmpresaTomadora != null && cteEmpresa.EmpresaDestinatario != null) && cteEmpresa.EmpresaTomadora.Id == cteEmpresa.EmpresaDestinatario.Id)
                    {
                        cteEmpresa.CodigoTomador = 3;
                    }
                    else
                    {
                        cteEmpresa.CodigoTomador = 4;
                    }

                    _cteEmpresaRepository.InserirSemSessao(cteEmpresa);

                    cteArquivo = null;
                    xmlDoc = null;
                    cteEmpresa = null;

                    return true;
                }

                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Obt�m a lista de Nfe's do xml de envio
        /// </summary>
        /// <param name="cteid">Identificador do Cte</param>
        /// <returns>Retorna a lista de Nfe's</returns>
        public IList<string> ObterListaNfeXmlEnvio(int cteid)
        {
            CteArquivo cteArquivo = _cteArquivoRepository.ObterPorId(cteid);
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(cteArquivo.ArquivoXml);

            XmlElement root = xmlDoc.DocumentElement;
            XmlNamespaceManager nsmgr = new XmlNamespaceManager(xmlDoc.NameTable);
            nsmgr.AddNamespace("cte", root.NamespaceURI);
            var nodeList = root.SelectNodes("//cte:CTe/cte:infCte/cte:infCTeNorm/cte:infDoc/cte:infNFe/cte:chave", nsmgr);

            if (nodeList == null)
            {
                return null;
            }

            IList<string> listaNfe = (from XmlNode node in nodeList select node.InnerText).ToList();

            return listaNfe;
        }

        /// <summary>
        /// obt�m um texto do xml
        /// </summary>
        /// <param name="xmlNode">no do xml informado</param>
        /// <param name="tagNo">tag do xml informado</param>
        /// <param name="nsmgr">namespace manager</param>
        /// <returns>texto extraido</returns>
        public string ObterTextoNoXml(XmlNode xmlNode, string tagNo, XmlNamespaceManager nsmgr)
        {
            XmlNode no = xmlNode.SelectSingleNode("//cte:" + tagNo, nsmgr);
            if (no == null)
            {
                return null;
            }

            return no.InnerText;
        }

        /// <summary>
        /// Verifica se ocorreu altera��o de remetente
        /// </summary>
        /// <param name="cte">Objeto Cte</param>
        /// <param name="fluxoComercial">Objeto FluxoComercial</param>
        /// <returns>True se ocorreu altera��o</returns>
        public bool ValidarSeOcorreuAlteracaoDeRemetente(Cte cte, FluxoComercial fluxoComercial)
        {
            IEmpresa remetenteXml = ObterEmpresaRemetenteXmlEnvio(cte.Id.Value);
            if (remetenteXml != null)
            {
                IEmpresa remetente = ObterEmpresaRemetenteFiscal(fluxoComercial.Codigo.Substring(2), cte.Id.Value);
                return ValidarSeOcorreuAlteracaoDeEmpresa(remetenteXml, remetente);
            }
            return false;
        }

        /// <summary>
        /// Verifica se ocorreu altera��o de Cfop
        /// </summary>
        /// <param name="cte">Objeto Cte</param>
        /// <param name="fluxoComercial">Objeto FluxoComercial</param>
        /// <param name="validacaoAlteracaoCfop">Verifica se a altera��o de Cfop � v�lida</param>
        /// <returns>True se ocorreu altera��o</returns>
        public bool ValidarSeOcorreuAlteracaoCfop(Cte cte, FluxoComercial fluxoComercial, out bool validacaoAlteracaoCfop)
        {
            string cfop = ObterCfopXmlEnvio(cte.Id.Value);
            validacaoAlteracaoCfop = true;

            if ((cfop != "") && (cfop != fluxoComercial.Contrato.Cfop))
            {
                // Os tr�s primeiros dig�tos do Cfop n�o podem ser alterados
                if (cfop.Substring(0, 3) != fluxoComercial.Contrato.Cfop.Substring(0, 3))
                {
                    validacaoAlteracaoCfop = false;
                }

                return true;
            }

            return false;
        }

        /// <summary>
        /// Verifica se ocorreu altera��o de destinat�rio
        /// </summary>
        /// <param name="cte">Objeto Cte</param>
        /// <param name="fluxoComercial">Objeto FluxoComercial</param>
        /// <returns>True se ocorreu altera��o</returns>
        public bool ValidarSeOcorreuAlteracaoDeDestinatario(Cte cte, FluxoComercial fluxoComercial)
        {
            IEmpresa destinatarioXml = ObterEmpresaDestinatarioXmlEnvio(cte.Id.Value);
            if (destinatarioXml == null)
                return false;

            IEmpresa destinatario = ObterEmpresaDestinatariaFiscal(fluxoComercial.Codigo.Substring(2), cte.Id.Value);
            return ValidarSeOcorreuAlteracaoDeEmpresa(destinatarioXml, destinatario);
        }

        /// <summary>
        /// Verifica se ocorreu altera��o de tomador
        /// </summary>
        /// <param name="cte">Objeto Cte</param>
        /// <param name="fluxoComercial">Objeto FluxoComercial</param>
        /// <returns>True se ocorreu altera��o</returns>
        public bool ValidarSeOcorreuAlteracaoDeTomador(Cte cte, FluxoComercial fluxoComercial)
        {
            IEmpresa tomadorXml = ObterEmpresaTomadorXmlEnvio(cte.Id.Value);
            if (tomadorXml == null)
                return false;

            IEmpresa tomador = fluxoComercial.Contrato.EmpresaPagadora;
            return ValidarSeOcorreuAlteracaoDeEmpresa(tomadorXml, tomador);
        }

        /// <summary>
        /// Verifica se ocorreu altera��o de expedidor
        /// </summary>
        /// <param name="cte">Objeto Cte</param>
        /// <param name="fluxoComercial">Objeto FluxoComercial</param>
        /// <returns>True se ocorreu altera��o</returns>
        public bool ValidarSeOcorreuAlteracaoDeExpedidor(Cte cte, FluxoComercial fluxoComercial)
        {
            IEmpresa expedidorXml = ObterEmpresaExpedidorXmlEnvio(cte.Id.Value);

            if (expedidorXml == null)
                return false;

            IEmpresa expedidor = fluxoComercial.Contrato.EmpresaRemetente;
            return ValidarSeOcorreuAlteracaoDeEmpresa(expedidorXml, expedidor);
        }

        /// <summary>
        /// Verifica se ocorreu altera��o de Nota Fiscal
        /// </summary>
        /// <param name="cteTemp"></param>
        /// <param name="cte">Objeto Cte</param>
        /// <returns>True se ocorreu altera��o</returns>
        public bool ValidarSeOcorreuAlteracaoDaNota(ManutencaoCteTempDto cteTemp, Cte cte)
        {
            if (cteTemp.ListaDetalhes == null)
                return true;
            else
            {
                return cte.ListaDetalhes.ToList().TrueForAll(x => cteTemp.ListaDetalhes.Count(c => c.ChaveNfe == x.ChaveNfe) > 0) &&
                cteTemp.ListaDetalhes.ToList().TrueForAll(x => cte.ListaDetalhes.Count(c => c.ChaveNfe == x.ChaveNfe) > 0);
            }
        }

        /// <summary>
        /// Verifica se ocorreu altera��o de toamdor
        /// </summary>
        /// <param name="cte">Objeto Cte</param>
        /// <param name="fluxoComercial">Objeto FluxoComercial</param>
        /// <returns>True se ocorreu altera��o</returns>
        public bool ValidarSeOcorreuAlteracaoDeRecebedor(Cte cte, FluxoComercial fluxoComercial)
        {
            IEmpresa recebedorXml = ObterEmpresaRecebedorXmlEnvio(cte.Id.Value);
            if (recebedorXml == null)
                return false;

            IEmpresa recebedor = fluxoComercial.Contrato.EmpresaDestinataria;
            return ValidarSeOcorreuAlteracaoDeEmpresa(recebedorXml, recebedor);
        }

        /// <summary>
        /// Obt�m o valor da tarifa do Cte
        /// </summary>
        /// <param name="cte">Cte informada para se obter a tarifa</param>
        /// <returns>Retorna o valor da tarifa sem ICMS</returns>
        public double ObterValorTarifaSemIcms(Cte cte)
        {
            // Carregar os dados do cte raiz
            CteArvore cteArvore = _cteArvoreRepository.ObterCteArvorePorCteFilho(cte);
            var cteRaiz = cteArvore.CteRaiz;

            var serieDespachoUf = _serieDespachoUfRepository.ObterPorCnpjUf(cte.CnpjFerrovia, cte.SiglaUfFerrovia);
            double valorTarifa = 0.0;
            string letraFerrovia = serieDespachoUf.LetraFerrovia;
            IList<ComposicaoFreteContrato> listaComposicaoFreteContrato = _composicaoFreteContratoRepository.ObterPorNumeroContrato(cte.ContratoHistorico.NumeroContrato, cteRaiz.DataHora, letraFerrovia, true);
            IList<ComposicaoFreteContrato> listaComposicaoFreteContratoVg = _composicaoFreteContratoRepository.ObterPorNumeroContrato(cte.ContratoHistorico.NumeroContrato, cteRaiz.DataHora, "VG", true);
            valorTarifa = listaComposicaoFreteContrato.Sum(g => g.ValorSemIcms).Value;
            valorTarifa += listaComposicaoFreteContratoVg.Sum(g => g.ValorSemIcms).Value;

            return Math.Round(valorTarifa, 2, MidpointRounding.AwayFromZero);
        }

        /// <summary>
        /// Obt�m o valor da tarifa do Cte com ICMS
        /// </summary>
        /// <param name="cte">Cte selecionada</param>
        /// <returns>Valor da tarifa com ICMS</returns>
        public double ObterValorTarifaComIcms(Cte cte)
        {
            double valorTarifa = 0.0;
            var listaComposicaoFreteContrato = _composicaoFreteContratoRepository.ObterPorNumeroContrato(cte.ContratoHistorico.NumeroContrato);
            valorTarifa = listaComposicaoFreteContrato.Sum(g => g.ValorComIcms).Value;

            return Math.Round(valorTarifa, 2, MidpointRounding.AwayFromZero);
        }

        /// <summary>
        /// Obt�m os procedimentos para o Status retorno
        /// </summary>
        /// <param name="idCteStatusRetorno">Id do Cte Status Retorno</param>
        /// <returns>Lista de Procedimentos para o Status Retorno</returns>
        public IList<CteResponsavelProcedimento> ObterProcedimentosPorCteStatusRetorno(int idCteStatusRetorno)
        {
            return _cteResponsavelProcedimentoRepository.ObterPorCteStatusRetornoId(idCteStatusRetorno);
        }

        /// <summary>
        /// Obter os tipos de CTE
        /// </summary>
        /// <returns>Lista string de tipos de CTE do banco de dados</returns>
        public IList<string> ObterTiposCte()
        {
            return _cteRepository.ObterTiposCte();
        }

        /// <summary>
        /// Obt�m uma lista de Erros da Status Retorno
        /// </summary>
        /// <param name="statusRetornoId">Lista de Ids da CteStatusRetorno para se obter os status detalhados</param>
        /// <returns>Lista CteStatusRetorno</returns>
        public IList<CteStatusRetorno> ObterCteStatusRetornoPorId(string statusRetornoId)
        {
            return _cteStatusRetornoRepository.ObterCteStatusRetornoPorId(statusRetornoId);
        }

        private bool ValidarSeOcorreuAlteracaoDeEmpresa(IEmpresa empresaXml, IEmpresa empresaFluxoComercial)
        {
            if (empresaXml.TipoPessoa == TipoPessoaEnum.Fisica)
            {
                if (empresaXml.Cpf != empresaFluxoComercial.Cpf)
                {
                    return true;
                }
            }
            else
            {
                if (empresaXml.Cgc != empresaFluxoComercial.Cgc)
                {
                    return true;
                }
            }

            if (empresaXml.InscricaoEstadual != Tools.TruncateRemoveSpecialChar(empresaFluxoComercial.InscricaoEstadual, 14))
            {
                return true;
            }

            if (empresaXml.RazaoSocial != empresaFluxoComercial.RazaoSocial)
            {
                return true;
            }

            if (empresaXml.Telefone1 != null && empresaXml.Telefone1 != Tools.ValidarTelefone(empresaFluxoComercial.Telefone1))
            {
                return true;
            }

            if (empresaXml.Endereco != empresaFluxoComercial.Endereco)
            {
                return true;
            }

            if (empresaXml.CidadeIbge.Descricao != empresaFluxoComercial.CidadeIbge.Descricao)
            {
                return true;
            }

            if (empresaXml.CidadeIbge.SiglaEstado != empresaFluxoComercial.CidadeIbge.SiglaEstado)
            {
                return true;
            }

            return false;
        }

        private string[] ParserEmail(string emails)
        {
            string mask = ";,: ";
            return emails.Split(mask.ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
        }

        /// <summary>
        /// Verifica se o fluxo � v�lido para carregamento
        /// </summary>
        /// <param name="fluxo">Objeto fluxo comercial</param>
        /// <returns> Retorna a string com os erros encontrados</returns>
        private string VerificarFluxoSaneado(FluxoComercial fluxo)
        {
            StringBuilder mensagemErro = new StringBuilder();

            if (!fluxo.Codigo.Equals("99999"))
            {
                if (fluxo.Contrato == null)
                {
                    mensagemErro.AppendLine("<br/> - Fluxo n�o possui contrato.");
                }
                else
                {
                    if (VerificarLiberacaoTravasCorrentista(fluxo))
                    {
                        if (VerificarCnpjRemetenteFiscal())
                        {
                            if (fluxo.Contrato.EmpresaRemetenteFiscal == null)
                            {
                                mensagemErro.AppendLine("<br/> - Fluxo n�o possui Remetente Fiscal.");
                            }
                            else
                            {
                                if (fluxo.Contrato.EmpresaRemetenteFiscal.TipoPessoa.Value.Equals(TipoPessoaEnum.Juridica))
                                {
                                    if (VerificarEmpresaNacional(fluxo.Contrato.EmpresaRemetenteFiscal))
                                    {
                                        if (string.IsNullOrEmpty(fluxo.Contrato.EmpresaRemetenteFiscal.Cgc) ||
                                            fluxo.Contrato.EmpresaRemetenteFiscal.Cgc.Trim().Equals(string.Empty))
                                        {
                                            mensagemErro.AppendLine("<br/> - Fluxo n�o possui o CNPJ do Remetente Fiscal.");
                                        }
                                    }
                                }
                            }
                        }

                        if (VerificarCnpjDestinatarioFiscal())
                        {
                            if (fluxo.Contrato.EmpresaDestinatariaFiscal == null)
                            {
                                mensagemErro.AppendLine("<br/> - Fluxo n�o possui Destinat�rio Fiscal.");
                            }
                            else
                            {
                                if (fluxo.Contrato.EmpresaDestinatariaFiscal.TipoPessoa.Value.Equals(TipoPessoaEnum.Juridica))
                                {
                                    if (VerificarEmpresaNacional(fluxo.Contrato.EmpresaDestinatariaFiscal))
                                    {
                                        if (string.IsNullOrEmpty(fluxo.Contrato.EmpresaDestinatariaFiscal.Cgc) ||
                                            fluxo.Contrato.EmpresaDestinatariaFiscal.Cgc.Trim().Equals(string.Empty))
                                        {
                                            mensagemErro.AppendLine("<br/> - Fluxo n�o possui o CNPJ do Destinat�rio Fiscal.");
                                        }
                                    }
                                }
                            }
                        }

                        if (VerificarCfopContrato())
                        {
                            if (!VerificarFluxoInternacional(fluxo))
                            {
                                if (string.IsNullOrEmpty(fluxo.Contrato.CfopProduto)
                                    || fluxo.Contrato.CfopProduto.Trim().Equals(string.Empty))
                                {
                                    mensagemErro.AppendLine("<br/> - Contrato do Fluxo n�o possui o CFOP cadastrado.");
                                }
                            }
                        }
                    }
                }

                if (fluxo.Contrato != null)
                {
                    if (!fluxo.Contrato.ValorAliquotaIcmsProduto.HasValue || !fluxo.Contrato.ValorBaseCalculo.HasValue
                        || string.IsNullOrEmpty(fluxo.Contrato.Cfop) || fluxo.Contrato.Cfop.Trim().Equals(string.Empty))
                    {
                        mensagemErro.AppendLine("<br/> - Fluxo n�o est� liberado no SAP.");
                    }
                }

                AssociaFluxoVigente associaFluxo = _associaFluxoVigenteRepository.ObterPorFluxoAtivo(fluxo);

                if (associaFluxo == null)
                {
                    mensagemErro.AppendLine("<br/> - Fluxo n�o possui um Grupo de fluxo ativo.");
                }
            }

            return mensagemErro.ToString();
        }

        /// <summary>
        /// Verifica se � para validar o cnpj do destinatario fiscal
        /// </summary>
        /// <returns>Valor booleano</returns>
        private bool VerificarCnpjRemetenteFiscal()
        {
            return ObterBooleanoConfGeral(ConfGeralCteVerificarCnpjRemetenteFiscal);
        }

        /// <summary>
        /// Verifica se � para validar o cnpj do destinatario fiscal
        /// </summary>
        /// <returns>Valor booleano</returns>
        private bool VerificarCfopContrato()
        {
            return ObterBooleanoConfGeral(ConfGeralCteVerificarCfop);
        }

        /// <summary>
        /// Verifica se � para validar o cnpj do destinatario fiscal
        /// </summary>
        /// <returns>Valor booleano</returns>
        private bool VerificarCnpjDestinatarioFiscal()
        {
            return ObterBooleanoConfGeral(ConfGeralCteVerificarCnpjDestinatarioFiscal);
        }

        /// <summary>
        /// Verifica se a empresa � do nacional ou do exterior
        /// </summary>
        /// <param name="empresa">Empresa a ser verificada</param>
        /// <returns>Valor booleano</returns>
        private bool VerificarEmpresaNacional(EmpresaCliente empresa)
        {
            EmpresaClientePais empresaClientePais = _empresaClientePaisRepository.ObterPorEmpresa(empresa);
            if (empresaClientePais == null)
            {
                throw new TranslogicException("A os dados do Pa�s da empresa n�o cadastrados.");
            }

            return empresaClientePais.Pais.ToUpperInvariant().Equals("BRASIL");
        }

        /// <summary>
        /// Verifica se o fluxo � um fluxo internacional
        /// </summary>
        /// <param name="fluxo"> Objeto FluxoComercial. </param>
        /// <returns> Valor booleano </returns>
        private bool VerificarFluxoInternacional(FluxoComercial fluxo)
        {
            AssociaFluxoInternacional fluxoInternacional = _associaFluxoInternacionalRepository.ObterPorFluxoComercial(fluxo);
            return fluxoInternacional != null;
        }

        /// <summary>
        /// Obt�m o valor booleano da conf geral pela chave
        /// </summary>
        /// <param name="chave">Chave da confgeral</param>
        /// <returns>Valor booleano</returns>
        private bool ObterBooleanoConfGeral(string chave)
        {
            ConfiguracaoTranslogic configuracaoTranslogic = _configuracaoTranslogicRepository.ObterPorId(chave);
            try
            {
                string valor = configuracaoTranslogic.Valor;
                if (valor.Equals("S"))
                {
                    return true;
                }

                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Verifica se houveram altera��es no Cte de manuten��o com o original
        /// </summary>
        /// <param name="dto">Dto de manuten��o</param>
        /// <returns>True se houve altera��o</returns>
        private bool ValidarAlteracaoDeFluxoComercial(ManutencaoCteTempDto dto)
        {
            Cte cteOriginal = _cteRepository.ObterPorId(dto.CteTemp.IdCteOrigem.Value);
            IList<CteDetalhe> listaDetalhesOriginais = _cteDetalheRepository.ObterPorCte(cteOriginal);

            CteTemp cteAlterado = dto.CteTemp;
            IList<CteTempDetalheViewModel> listaDetalhesAlterados = dto.ListaDetalhes;

            bool alteradoCte = VerificarAlteracaoCte(cteOriginal, cteAlterado);
            if (alteradoCte)
            {
                return true;
            }

            return VerificarAlteracaoDetalhesCte(listaDetalhesOriginais, listaDetalhesAlterados);
        }

        private bool VerificarAlteracaoCte(Cte cteOriginal, CteTemp cteAlterado)
        {
            if (!cteOriginal.Vagao.Id.Equals(cteAlterado.Id))
            {
                return true;
            }

            if (!cteOriginal.FluxoComercial.Id.Equals(cteAlterado.IdFluxoComercial))
            {
                return true;
            }

            if (!cteOriginal.PesoVagao.Equals(cteAlterado.PesoVagao))
            {
                return true;
            }

            if (!cteAlterado.VolumeVagao.HasValue)
            {
                cteAlterado.VolumeVagao = 0;
            }

            if (!cteOriginal.VolumeVagao.Equals(cteAlterado.VolumeVagao.Value))
            {
                return true;
            }

            if (!cteAlterado.TaraVagao.HasValue)
            {
                cteAlterado.TaraVagao = 0;
            }

            if (!cteOriginal.TaraVagao.Equals(cteAlterado.TaraVagao.Value))
            {
                return true;
            }

            return false;
        }

        private bool VerificarAlteracaoDetalhesCte(IList<CteDetalhe> listaDetalhesOriginais, IList<CteTempDetalheViewModel> listaDetalhesAlterados)
        {
            if (listaDetalhesAlterados.Count != listaDetalhesOriginais.Count)
            {
                return true;
            }

            foreach (CteDetalhe detalheOriginal in listaDetalhesOriginais)
            {
                CteTempDetalheViewModel alterado =
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                listaDetalhesAlterados.FirstOrDefault(
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                c =>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                c.SerieNotaFiscal.Equals(detalheOriginal.SerieNota) &&
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                c.NumeroNotaFiscal.Equals(detalheOriginal.NumeroNota) &&
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                c.ChaveNfe.Equals(detalheOriginal.ChaveNfe));
                if (alterado == null)
                {
                    return true;
                }

                if (VerificarAlteracaoDetalheCte(detalheOriginal, alterado))
                {
                    return true;
                }
            }

            return false;
        }

        private bool VerificarAlteracaoDetalheCte(CteDetalhe detalheOriginal, CteTempDetalheViewModel alterado)
        {
            if (!detalheOriginal.PesoNotaFiscal.Equals(alterado.PesoNotaFiscal))
            {
                return true;
            }

            if (!alterado.VolumeNotaFiscal.HasValue)
            {
                alterado.VolumeNotaFiscal = 0;
            }

            if (!detalheOriginal.VolumeNotaFiscal.Equals(alterado.VolumeNotaFiscal))
            {
                return true;
            }

            if (detalheOriginal.ConteinerNotaFiscal != alterado.ConteinerNotaFiscal)
            {
                return true;
            }

            if (detalheOriginal.NumeroTif != alterado.NumeroTif)
            {
                return true;
            }

            if (detalheOriginal.ValorNotaFiscal != alterado.ValorNotaFiscal)
            {
                return true;
            }

            if (detalheOriginal.ValorTotalNotaFiscal != alterado.ValorTotalNotaFiscal)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Verifica se na lista j� contem o cte
        /// </summary>
        /// <param name="lista">lista de Dto Cte que ser� analisada</param>
        /// <param name="tipo">tipo da lista</param>
        /// <param name="cteId">id do cte que ir� ser procurado na lista</param>
        /// <returns>retorna verdadeiro se o dto cte j� estiver na lista</returns>
        private bool ContainCteDto(IList lista, Type tipo, int cteId)
        {
            if (tipo == typeof(IList<CteDto>))
            {
                IList<CteDto> listaTmp = (IList<CteDto>)lista;
                return listaTmp.Any(cteDto => cteDto.CteId == cteId);
            }
            else if (tipo == typeof(IList<ManutencaoCteDto>))
            {
                IList<ManutencaoCteDto> listaTmp = (IList<ManutencaoCteDto>)lista;
                return listaTmp.Any(cteDto => cteDto.CteId == cteId);
            }

            return false;
        }

        /// <summary>
        /// Processa o status de retorno conforme o codigo de retorno da SEFAZ
        /// </summary>
        /// <param name="cte">CTE que est� sendo processado</param>
        /// <param name="cteStatusRetorno">Status de retorno do CTE</param>
        /// <param name="consultaCte">Retorno da config</param>
        private void ProcessarCteStatusRetorno(Cte cte, CteStatusRetorno cteStatusRetorno, Vw209ConsultaCte consultaCte)
        {
            // Insere no log do cte
            _cteLogService.InserirLogInfo(cte, "CteService", string.Format("Status de retorno do Cte: {0}", cteStatusRetorno.Id));

            if (cteStatusRetorno.Id == 100)
            {
                // Item da fila de processamento (utilizado para pegar o XML de retorno)
                Cte01Lista retornoConfig = _filaProcessamentoService.ObterItemFilaProcessamentoPorConsultaRetorno(consultaCte);

                // Cte Autorizado
                cte.SituacaoAtual = SituacaoCteEnum.Autorizado;
                cte.XmlAutorizacao = retornoConfig.XmlRetorno;
            }
            else if (cteStatusRetorno.Id == 101 || cteStatusRetorno.Id == 135)
            {
                // Item da fila de processamento (utilizado para pegar o XML de retorno)
                Cte01Lista retornoConfig = _filaProcessamentoService.ObterItemFilaProcessamentoPorConsultaRetorno(consultaCte);

                // Corre��o de Cte
                if (cteStatusRetorno.Id == 135 && retornoConfig.Tipo == 6)
                {
                    // Cte Autorizado
                    cte.SituacaoAtual = SituacaoCteEnum.Autorizado;
                    cte.XmlAutorizacao = retornoConfig.XmlRetorno;
                }
                else
                {
                    // Cancelamento de CT-e homologado
                    cte.SituacaoAtual = SituacaoCteEnum.AutorizadoCancelamento;
                    cte.XmlCancelamento = retornoConfig.XmlRetorno;







                    /////* 
                    //// * Solicita��o 06/06/2016
                    //// * Extornar saldo das notas utilizadas no cte
                    //// */
                    ////var listaDet = _cteDetalheRepository.ObterPorCte(cte);
                    ////foreach (var det in listaDet)
                    ////{
                    ////    try
                    ////    {
                    ////        var statusNfe = _nfeService.ObterStatusNfe(det.ChaveNfe);
                    ////        if (statusNfe.PesoUtilizado - det.PesoNotaFiscal >= 0)
                    ////        {
                    ////            _nfeService.RetornarSaldoNfe(statusNfe, det.PesoNotaFiscal);
                    ////        }
                    ////        else
                    ////        {
                    ////            throw new TransactionException(string.Format("Peso Utilizado a ser extornado (nota {0}) � maior do que o peso da nota.", det.ChaveNfe));
                    ////        }
                    ////    }
                    ////    catch (Exception ex)
                    ////    {
                    ////        _cteLogService.InserirLogErro(cte, "CteService", string.Format("RetornarSaldoDasNotas: {0}", ex.Message), ex);
                    ////        throw;
                    ////    }
                    ////}








                    /*
                     * Solicita��o do dia 08/04/2015 para remover valida��es e enviar independente da condi��o, salvando um status novo para as ctes sem valor
                     * if (cte.FluxoComercial != null && !VerificarConteinerBrado(cte.FluxoComercial) && cte.ValorTotalMercadoria > 100)
                     */

                    var statusEnvio = StatusEnvioSeguradora.Validada;
                    var _listaCteDetalhe = _cteDetalheRepository.ObterPorCte(cte);

                    if (cte.FluxoComercial.UnidadeMedida.Id.Equals("M3"))
                    {
                        if (_listaCteDetalhe.Any(cteDetalhe => cteDetalhe.VolumeNotaFiscal == 0.0 || cteDetalhe.PesoTotal == 0.0 || cteDetalhe.ValorTotalNotaFiscal < 100 || cte.ValorTotalMercadoria < 100))
                        {
                            statusEnvio = StatusEnvioSeguradora.SemVolume;
                        }
                    }
                    else
                    {
                        if (_listaCteDetalhe.Any(cteDetalhe => cteDetalhe.PesoNotaFiscal == 0.0 || cteDetalhe.PesoTotal == 0.0 || cteDetalhe.ValorTotalNotaFiscal < 100 || cte.ValorTotalMercadoria < 100))
                        {
                            statusEnvio = StatusEnvioSeguradora.SemPeso;
                        }
                    }

                    if (_configuracaoTranslogicRepository.ObterPorId(_cteAtivaEnvioSeguradora).Valor.Equals("S"))
                    {
                        try
                        {
                            if (_listaCteDetalhe.Any(c => c.ChaveNfe != null))
                            {
                                TipoEnvioSeguradora tipoEnvio;

                                var requisicao = new UploadFileRequest()
                                {
                                    Email = _configuracaoTranslogicRepository.ObterPorId(_chaveRemtenteEmailSeguradoraBradesco).Valor,
                                    Seguradora = _configuracaoTranslogicRepository.ObterPorId(_chaveSeguradoraBradesco).Valor,
                                    XMLData = retornoConfig.XmlRetorno
                                };

                                UploadFileResponse resultadoUploadFile = null;
                                try
                                {
                                    resultadoUploadFile = _receiveFileSeguroBradesco.UploadFile(requisicao);
                                    tipoEnvio = TipoEnvioSeguradora.WebService;
                                    if (resultadoUploadFile.@return.Contains("ERROR"))
                                    {
                                        throw new Exception(resultadoUploadFile.@return);
                                    }
                                }
                                catch
                                {
                                    // grava informa��es para envio do email
                                    GravarInformacaoEnvioEmail(cte, _configuracaoTranslogicRepository.ObterPorId(_chaveEmailSeguradoraBradesco).Valor);

                                    tipoEnvio = TipoEnvioSeguradora.Email;
                                }

                                _cteEnvioSeguradoraHistRepository.Inserir(new CteEnvioSeguradoraHist()
                                {
                                    Cte = cte,
                                    Seguradora = requisicao.Seguradora,
                                    Email = requisicao.Email,
                                    Retorno = resultadoUploadFile != null ? resultadoUploadFile.@return : null,
                                    TipoEnvio = ALL.Core.Util.Enum<TipoEnvioSeguradora>.GetDescriptionOf(tipoEnvio),
                                    StatusEnvio = ALL.Core.Util.Enum<StatusEnvioSeguradora>.GetDescriptionOf(statusEnvio)
                                });
                            }
                        }
                        catch (Exception ex)
                        {
                            _cteLogService.InserirLogInfo(cte, "CteService", string.Format("ERRO AO ENVIAR PARA O SEGURO - {0}", ex.Message));
                        }
                    }
                }
            }
            else if (cteStatusRetorno.Id == 102)
            {
                // Item da fila de processamento (utilizado para pegar o XML de retorno)
                Cte01Lista retornoConfig = _filaProcessamentoService.ObterItemFilaProcessamentoPorConsultaRetorno(consultaCte);

                // Inutiliza��o de n�mero homologado
                // cte.SituacaoAtual = SituacaoCteEnum.AutorizadoInutilizacao;
                cte.XmlInutilizacao = retornoConfig.XmlRetorno;
                cte.SituacaoAtual = SituacaoCteEnum.Inutilizado;
            }
            else if ((cteStatusRetorno.Id >= 103) && (cteStatusRetorno.Id <= 107))
            {
                // Sefaz processando o Lote (Nenhuma a��o)
            }
            else if ((cteStatusRetorno.Id >= 108) && (cteStatusRetorno.Id <= 109))
            {
                // -> Ricardo Venanti pediu para deixar re-enviar esses casos.
                cte.SituacaoAtual = SituacaoCteEnum.ErroAutorizadoReEnvio;
            }
            else if (cteStatusRetorno.Id == 110)
            {
                cte.SituacaoAtual = SituacaoCteEnum.UsoDenegado;
            }
            else if ((cteStatusRetorno.Id >= 111) && (cteStatusRetorno.Id <= 199))
            {
                // Rejei��o
                if (cteStatusRetorno.PermiteReenvio)
                {
                    cte.SituacaoAtual = SituacaoCteEnum.ErroAutorizadoReEnvio;
                }
                else
                {
                    cte.SituacaoAtual = SituacaoCteEnum.Erro;
                }
            }
            else if ((cteStatusRetorno.Id >= 200) && (cteStatusRetorno.Id <= 299))
            {
                // Rejei��o
                if (cteStatusRetorno.PermiteReenvio)
                {
                    cte.SituacaoAtual = SituacaoCteEnum.ErroAutorizadoReEnvio;
                }
                else
                {
                    cte.SituacaoAtual = SituacaoCteEnum.Erro;
                }
            }
            else if ((cteStatusRetorno.Id >= 301) && (cteStatusRetorno.Id <= 306))
            {
                cte.SituacaoAtual = SituacaoCteEnum.UsoDenegado;
            }
            else if ((cteStatusRetorno.Id >= 401) && (cteStatusRetorno.Id <= 999))
            {
                // Rejei��o
                if (cteStatusRetorno.PermiteReenvio)
                {
                    cte.SituacaoAtual = SituacaoCteEnum.ErroAutorizadoReEnvio;
                }
                else
                {
                    cte.SituacaoAtual = SituacaoCteEnum.Erro;
                }
            }
            else if (cteStatusRetorno.Id == 2011)
            {
                // Verifica se excedeu o numero m�ximo de tentativas 
                if (ValidarTentativasExcedentesPorStatusRetorno(cte, 2011))
                {
                    cte.SituacaoAtual = SituacaoCteEnum.ErroAutorizadoReEnvio;
                    _cteStatusRepository.InserirCteStatus(cte, ObterUsuarioRobo(), new CteStatusRetorno { Id = 14 }, cteStatusRetorno.Descricao);
                }
                else
                {
                    // Erro Retornado pela Config quando a Sefaz est� OFF, ent�o � enviado novamente at� que a Sefaz volte.
                    if (cteStatusRetorno.PermiteReenvio)
                    {
                        // Se o Reenvio estiver habilitado, ent�o coloca o arquivo na fila novamente.
                        cte.SituacaoAtual = SituacaoCteEnum.PendenteArquivoEnvio;
                        _cteStatusRepository.InserirCteStatus(cte, ObterUsuarioRobo(), new CteStatusRetorno { Id = 26 });
                    }
                    else
                    {
                        cte.SituacaoAtual = SituacaoCteEnum.Erro;
                    }
                }
            }
            else if (cteStatusRetorno.Id == 4001)
            {
                // Erro Log (utilizado para pegar a descri��o completa do erro)
                Nfe20ErroLog erroLog = _filaProcessamentoService.ObterLogErroPorConsultaRetorno(consultaCte);

                cte.SituacaoAtual = SituacaoCteEnum.ErroAutorizadoReEnvio;
                if (erroLog != null)
                {
                    cte.XmlAutorizacao = erroLog.DescricaoCompleta;
                }
            }
            else if (cteStatusRetorno.Id >= 1000)
            {
                if (cteStatusRetorno.PermiteReenvio)
                {
                    cte.SituacaoAtual = SituacaoCteEnum.ErroAutorizadoReEnvio;
                }
                else
                {
                    cte.SituacaoAtual = SituacaoCteEnum.Erro;
                }
            }

            // Regra cancelamento 220
            // Caso seja um cancelamento rejeitado pela Sefaz. Dever� ser enviado as informa��es para o SAP
            // como "CR" - Cancelamento Rejeitado e mudar o Status do CT-e para cancelado
            if (cteStatusRetorno.Id == 220)
            {
                _cteStatusRepository.InserirCteStatus(cte, ObterUsuarioRobo(), new CteStatusRetorno { Id = 35 });
                cte.SituacaoAtual = SituacaoCteEnum.AguardandoAnulacao;

                // Insere na interface de envio para o SAP
                // InserirInterfaceEnvioSap(cte, cteStatusRetorno);
            }

            if (!cteStatusRetorno.RemoveFilaInterface)
            {
                cte.SituacaoAtual = SituacaoCteEnum.EmProcessamento;
            }

            // Insere no log do cte
            _cteLogService.InserirLogInfo(cte, "CteService", string.Format("Atualizando a situa��o atual do cte para: {0}", cte.SituacaoAtual));
            _cteRepository.InserirOuAtualizar(cte);
        }

        private void InserirInterfaceRecebimentoConfig(Cte cte, int idListaConfig)
        {
            int numeroCte = 0;
            int.TryParse(cte.Numero, out numeroCte);

            CteInterfaceRecebimentoConfig interfaceConfig = new CteInterfaceRecebimentoConfig();
            interfaceConfig.Cte = cte;
            interfaceConfig.IdListaConfig = idListaConfig;
            interfaceConfig.CfgUn = ObterIdentificadorFilial(cte);
            interfaceConfig.CfgDoc = numeroCte;
            interfaceConfig.CfgSerie = cte.Serie;
            interfaceConfig.DataHora = DateTime.Now;
            interfaceConfig.DataUltimaLeitura = DateTime.Now;

            _cteInterfaceRecebimentoConfigRepository.Inserir(interfaceConfig);
        }

        private int ObterIdentificadorFilial(Cte cte)
        {
            int idFilial = 0;
            if (cte.FluxoComercial != null)
            {
                idFilial = cte.FluxoComercial.Origem.EmpresaOperadora.Id ?? 0;
            }

            if (idFilial == 0)
            {
                ConfiguracaoTranslogic filial = _configuracaoTranslogicRepository.ObterPorId(_chaveCteCodigoInternoFilial);
                idFilial = System.Convert.ToInt32(filial.Valor);
            }

            return idFilial;
        }

        private void InserirInterfaceArquivoPdf(Cte cte, TipoOperacaoCteEnum tipoOperacao)
        {
            string hostName = Dns.GetHostName();
            CteInterfacePdfConfig interfacePdf = new CteInterfacePdfConfig();
            interfacePdf.Cte = cte;
            interfacePdf.Chave = cte.Chave;
            interfacePdf.HostName = hostName;
            interfacePdf.DataHora = DateTime.Now;
            interfacePdf.DataUltimaLeitura = DateTime.Now;
            interfacePdf.TipoOperacao = tipoOperacao;

            try
            {
                _cteInterfacePdfRepository.Inserir(interfacePdf);
            }
            catch
            {
            }
        }

        private void InserirInterfaceArquivoXml(Cte cte, TipoOperacaoCteEnum tipoOperacao)
        {
            string hostName = Dns.GetHostName();
            CteInterfaceXmlConfig interfaceXml = new CteInterfaceXmlConfig();
            interfaceXml.Cte = cte;
            interfaceXml.Chave = cte.Chave;
            interfaceXml.HostName = hostName;
            interfaceXml.DataHora = DateTime.Now;
            interfaceXml.DataUltimaLeitura = DateTime.Now;
            interfaceXml.TipoOperacao = tipoOperacao;

            try
            {
                _cteInterfaceXmlRepository.Inserir(interfaceXml);
            }
            catch
            {
            }
        }

        public void InserirInterfaceEnvioSap(Cte cte, CteStatusRetorno cteStatusRetorno)
        {
            try
            {
                TipoOperacaoCteEnum tipoOperacao = TipoOperacaoCteEnum.Invalida;

                /*  
                    I=Inclus�o, 
                    E=Exclus�o (CANCELADO AUTORIZADO), 
                    C=Complemento, IN=Inutilizado, 
                    CR=Cancelamento Rejeitado c�digo do SEFAZ 220,
                    A = Anula��o,
                    D=Denegado 
                 */
                CteComplementado cteComplemento;
                switch (cte.SituacaoAtual)
                {
                    case SituacaoCteEnum.Autorizado:
                        if (cte.TipoCte == TipoCteEnum.Complementar)
                        {
                            cteComplemento = _cteComplementadoRepository.ObterCteComplementadoPorCteComplementar(cte).FirstOrDefault();
                            tipoOperacao = cteComplemento.TipoComplemento;
                        }
                        else if (cte.TipoCte == TipoCteEnum.Substituto)
                        {
                            tipoOperacao = TipoOperacaoCteEnum.Substituicao;
                        }
                        else if (cte.TipoCte == TipoCteEnum.Virtual)
                        {
                            tipoOperacao = TipoOperacaoCteEnum.Virtual;
                        }
                        else
                        {
                            tipoOperacao = TipoOperacaoCteEnum.Inclusao;
                        }

                        break;

                    case SituacaoCteEnum.Anulado:
                        tipoOperacao = TipoOperacaoCteEnum.Anulacao;

                        break;

                    case SituacaoCteEnum.Cancelado:
                        if (cteStatusRetorno.Id == 220)
                        {
                            tipoOperacao = TipoOperacaoCteEnum.CancelamentoRejeitado;
                        }
                        else
                        {
                            tipoOperacao = TipoOperacaoCteEnum.Exclusao;
                        }

                        break;

                    case SituacaoCteEnum.EnviadoArquivoComplemento:
                        cteComplemento = _cteComplementadoRepository.ObterCteComplementadoPorCteComplementar(cte).FirstOrDefault();
                        tipoOperacao = cteComplemento.TipoComplemento;
                        break;

                    case SituacaoCteEnum.Erro:
                        if (cteStatusRetorno.Id == 220)
                        {
                            tipoOperacao = TipoOperacaoCteEnum.CancelamentoRejeitado;
                        }

                        break;

                    case SituacaoCteEnum.Inutilizado:
                        tipoOperacao = TipoOperacaoCteEnum.Inutilizado;
                        break;

                    case SituacaoCteEnum.UsoDenegado:
                        tipoOperacao = TipoOperacaoCteEnum.Denegado;
                        break;

                    case SituacaoCteEnum.PendenteArquivoAnulacaoContribuinte:
                        tipoOperacao = TipoOperacaoCteEnum.Anulacao; // para envio para o sap, o tipo operacao aqui � o mesmo da anula��o antiga.
                        break;

                    case SituacaoCteEnum.PendenteArquivoAnulacaoNContribuinte:
                        tipoOperacao = TipoOperacaoCteEnum.AnulacaoSefaz; // somente para nao contribuinte que muda o tipo opera��o.
                        break;
                }

                if (_cteCanceladoRefaturamentoConfigRepository.ObterPorId(cte.Id) == null && _cteInutilizacaoConfigRepository.ObterPorId(cte.Id) == null)
                {
                    // checa se j� existe, sen�o d� erro de duplicado pq id_cte � chave �nica
                    if (_cteInterfaceEnvioSapRepository.ObterPorCte(cte.Id.GetValueOrDefault()) == null)
                    {
                        CteInterfaceEnvioSap interfaceEnvioSap = new CteInterfaceEnvioSap();
                        interfaceEnvioSap.Cte = cte;
                        interfaceEnvioSap.DataGravacaoRetorno = DateTime.Now;
                        interfaceEnvioSap.DataUltimaLeitura = DateTime.Now;
                        interfaceEnvioSap.TipoOperacao = tipoOperacao;
                        interfaceEnvioSap.DataEnvioSap = null;

                        _cteInterfaceEnvioSapRepository.Inserir(interfaceEnvioSap);
                    }
                }
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(cte, "CteService", string.Format("Erro na gera��o dos dados para o SAP em InserirInterfaceEnvioSap: {0}", ex.Message), ex);
            }
        }

        private void InserirInterfaceEnvioEmail(Cte cte, TipoOperacaoCteEnum? tipoOperacao)
        {
            CteArquivo cteArquivo = _cteArquivoRepository.ObterPorId(cte.Id.Value);
            bool cartaDeCorrecao = tipoOperacao != null && tipoOperacao == TipoOperacaoCteEnum.CartaDeCorrecao;

            try
            {
                if (cartaDeCorrecao)
                {
                    if (cteArquivo.ArquivoPdfCartaDeCorrecao != null && cteArquivo.ArquivoXmlCartaDeCorrecao != null)
                    {
                        CteInterfaceEnvioEmail interfaceEnvioEmail = new CteInterfaceEnvioEmail();
                        interfaceEnvioEmail.Cte = cte;
                        interfaceEnvioEmail.Chave = cte.Chave;
                        interfaceEnvioEmail.DataHora = DateTime.Now;
                        interfaceEnvioEmail.DataUltimaLeitura = DateTime.Now;
                        interfaceEnvioEmail.HostName = Dns.GetHostName();
                        interfaceEnvioEmail.TipoOperacao = tipoOperacao;
                        _cteInterfaceEnvioEmailRepository.Inserir(interfaceEnvioEmail);
                    }
                }
                else if ((cteArquivo != null) && (cteArquivo.TamanhoPdf != null) && (cteArquivo.TamanhoXml != null))
                {
                    CteInterfaceEnvioEmail interfaceEnvioEmail = new CteInterfaceEnvioEmail();
                    interfaceEnvioEmail.Cte = cte;
                    interfaceEnvioEmail.Chave = cte.Chave;
                    interfaceEnvioEmail.DataHora = DateTime.Now;
                    interfaceEnvioEmail.DataUltimaLeitura = DateTime.Now;
                    interfaceEnvioEmail.HostName = Dns.GetHostName();
                    interfaceEnvioEmail.TipoOperacao = tipoOperacao;
                    _cteInterfaceEnvioEmailRepository.Inserir(interfaceEnvioEmail);
                }
            }
            catch (Exception)
            {
            }
        }

        private bool ValidarTentativasExcedentesPorStatusRetorno(Cte cte, int codigoStatusRetorno)
        {
            try
            {
                int numeroMaximoTentativas = 0;
                int numeroAtual = 0;
                ConfiguracaoTranslogic configuracao = _configuracaoTranslogicRepository.ObterPorId(_chaveCteTentativaReenvio);
                if (configuracao != null)
                {
                    IList<CteStatus> listaAux = _cteStatusRepository.ObterPorCte(cte);

                    numeroMaximoTentativas = Convert.ToInt32(configuracao.Valor);
                    numeroAtual = listaAux.Where(g => g.CteStatusRetorno.Id == codigoStatusRetorno).Count();
                    if (numeroAtual >= numeroMaximoTentativas)
                    {
                        return true;
                    }
                }

                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private void InserirHistoricoEnvioEmail(Cte envioEmail, string emailPara, string emailCopia, string emailCopiaOculta, TipoArquivoEnvioEnum tipoArquivoEnvio, string remetente, string smtp)
        {
            try
            {
                CteEnvioEmailHistorico envioEmailHistorico = new CteEnvioEmailHistorico();
                envioEmailHistorico.Cte = envioEmail;
                envioEmailHistorico.EmailPara = emailPara.Left(500);
                envioEmailHistorico.EmailCopia = emailCopia.Left(500);
                envioEmailHistorico.EmailCopiaOculta = emailCopiaOculta.Left(500);
                envioEmailHistorico.TipoArquivoEnvio = tipoArquivoEnvio;
                envioEmailHistorico.DataHora = DateTime.Now;
                envioEmailHistorico.HostName = Dns.GetHostName();
                envioEmailHistorico.Remetente = remetente;
                envioEmailHistorico.Smtp = smtp;
                _cteEnvioEmailHistoricoRepository.Inserir(envioEmailHistorico);
            }
            catch (Exception ex)
            {
                Sis.LogException(ex, string.Format("InserirHistoricoEnvioEmail. Cte: {0} - {1}", envioEmail.Id, envioEmail.Chave));
            }
        }

        private void InserirEnvioEmailErro(Cte envioEmail, string emailPara, string emailCopia, string emailCopiaOculta, string descricaoErro, TipoArquivoEnvioEnum tipoArquivoEnvio, string remetente, string smtp)
        {
            try
            {
                CteEnvioEmailErro envioEmailErro = new CteEnvioEmailErro();
                envioEmailErro.Cte = envioEmail;
                envioEmailErro.EmailPara = emailPara.Left(500);
                envioEmailErro.EmailCopia = emailCopia.Left(500);
                envioEmailErro.EmailCopiaOculta = emailCopiaOculta.Left(500);
                envioEmailErro.DescricaoErro = descricaoErro;
                envioEmailErro.TipoArquivoEnvio = tipoArquivoEnvio;
                envioEmailErro.DataHora = DateTime.Now;
                envioEmailErro.HostName = Dns.GetHostName();
                envioEmailErro.Remetente = remetente;
                envioEmailErro.Smtp = smtp;
                _cteEnvioEmailErroRepository.Inserir(envioEmailErro);

                if (envioEmailErro.EmailPara != null && envioEmailErro.EmailPara.Length > 500)
                {
                    envioEmailErro.EmailPara = envioEmailErro.EmailPara.Substring(0, 500);
                }
            }
            catch (Exception ex)
            {
                Sis.LogException(ex, string.Format("InserirHistoricoEnvioEmail. Cte: {0} - {1}", envioEmail.Id, envioEmail.Chave));
            }
        }

        private string GerarHtmlEmail(Cte cte)
        {
            string conteudo = string.Empty;
            long cnpj;
            Int64.TryParse(cte.CnpjFerrovia, out cnpj);

            Nfe03Filial filial = _nfe03FilialRepository.ObterFilialPorCnpj(cnpj);

            // StringBuilder buider = new StringBuilder();
            // string buffer = string.Empty;

            string arquivo = Directory.GetCurrentDirectory() + "\\" + "GenericoCTe.htm";

            if (!File.Exists(arquivo))
            {
                throw new Exception("Arquivo '" + arquivo + "' n�o encontrado. Esse arquivo � necess�rio para o envio de e-mails do Cte");
            }

            string valor;
            using (StreamReader reader = new StreamReader(arquivo))
            {
                // string line = string.Empty;
                conteudo = reader.ReadToEnd();
                conteudo = conteudo.Replace("#PROTOCOLO#", cte.NumeroProtocolo);
                conteudo = conteudo.Replace("#NUMERO_NFE#", cte.Numero);
                conteudo = conteudo.Replace("#SERIE_NFE#", cte.Serie);
                conteudo = conteudo.Replace("#RAZ_SOC#", filial.RazSoc);
            }

            return conteudo;
        }

        private MemoryStream CreateToMemoryStream(Dictionary<string, Stream> arquivos)
        {
            MemoryStream outputMemStream = new MemoryStream();
            ZipOutputStream zipStream = new ZipOutputStream(outputMemStream);

            zipStream.SetLevel(9); // 0-9, 9 being the highest level of compression

            foreach (KeyValuePair<string, Stream> arquivo in arquivos)
            {
                ZipEntry newEntry = new ZipEntry(arquivo.Key);
                newEntry.DateTime = DateTime.Now;

                zipStream.PutNextEntry(newEntry);
                StreamUtils.Copy(arquivo.Value, zipStream, new byte[4096]);
                zipStream.CloseEntry();
            }

            zipStream.IsStreamOwner = false;    // False stops the Close also Closing the underlying stream.
            zipStream.Close();          // Must finish the ZipOutputStream before using outputMemStream.

            outputMemStream.Position = 0;
            return outputMemStream;
        }

        public Stream ObterArquivoPdf(string[] arrIds)
        {
            // Inst�ncia de objeto
            PdfMerge merge = new PdfMerge();
            CteArquivo file;

            // percorre os Ctes
            foreach (string id in arrIds)
            {
                // Obtem o Arquivo
                file = ObterArquivoCte(int.Parse(id));

                // Se possuir arquio na base, ent�o faz o merge do Pdf
                if (file != null)
                {
                    if (file.ArquivoPdf != null)
                    {
                        merge.AddFile(file.ArquivoPdf);
                    }
                }
            }

            return merge.Execute();
        }

        private void AtualizarStatusCteManutInvalida(int idCte)
        {
            Cte cteOriginal = _cteRepository.ObterPorId(idCte);

            if (cteOriginal.SituacaoAtual == SituacaoCteEnum.AguardandoAnulacao)
            {
                if (cteOriginal.SituacaoAtual == SituacaoCteEnum.ErroAutorizadoReEnvio)
                {
                    cteOriginal.SituacaoAtual = SituacaoCteEnum.Invalidado;
                    _cteRepository.Atualizar(cteOriginal);
                }
            }
        }

        /// <summary>
        /// Fun��o para verificar se o Cte est� no prazo de cancelamento
        /// </summary>
        /// <param name="cte">Data do Cte</param>
        /// <param name="usuario"> Usuario que executou </param>
        /// <param name="cteDto"> verifica se esta descarregado </param>
        /// <returns>Retorna true se puder efetuar o cancelamento</returns>
        private bool VerificarHorasCancelamento(Cte cte, Usuario usuario, CteDto cteDto)
        {
            int horasCancelamento;
            DateTime novaDataCte;
            string grupoFiscal = string.Empty;

            grupoFiscal = RecuperarValorConfGeral(_chaveCodigoGrupoFiscal);
            int.TryParse(RecuperarValorConfGeral(_chaveHorasCancelamentoCte), out horasCancelamento);

            novaDataCte = cte.DataHora.AddHours(horasCancelamento);

            if (DateTime.Now.Subtract(novaDataCte).TotalHours > 0 || cte.CteMotivoCancelamento.EnviaSefaz == false || cteDto.Descarregado)
            {
                IList<GrupoUsuario> grupoUsuarios = _agrupamentoUsuarioRepository.ObterGruposPorUsuario(usuario);

                foreach (GrupoUsuario grupoUsuario in grupoUsuarios)
                {
                    if (grupoUsuario.Codigo.ToUpper().Trim().Equals(grupoFiscal))
                    {
                        return true;
                    }
                }

                return false;
            }

            return true;
        }

        /// <summary>
        /// Fun��o para verificar se o Cte est� no prazo de cancelamento
        /// </summary>
        /// <param name="dataCte">Data do Cte</param>
        /// <param name="usuario"> Usuario que executou </param>
        /// <returns>Retorna true se puder efetuar o cancelamento</returns>
        private bool VerificarPrazoCancelamento(DateTime dataCte, Usuario usuario)
        {
            int diasCancelamento;
            DateTime novaDataCte;
            string grupoFiscal = string.Empty;
            bool sucesso = true;
            /*
                Cancelamentos de CTe do m�s anterior poder�o ser realizados at� o 3� dia �til + 7 dias(Configur�vel pela Conf_Geral) dia do m�s corrente.
            */
            grupoFiscal = RecuperarValorConfGeral(_chaveCodigoGrupoFiscal);
            int.TryParse(RecuperarValorConfGeral(_chaveDiasCancelamentoCte), out diasCancelamento);

            novaDataCte = dataCte.AddMonths(1);
            novaDataCte = new DateTime(novaDataCte.Year, novaDataCte.Month, diasCancelamento);

            bool ret = _controleAcessoService.VerificarUsuarioPertenceGrupo(usuario.Codigo, grupoFiscal);

            if (DateTime.Now.Subtract(novaDataCte).Days > 0)
            {
                IList<GrupoUsuario> grupoUsuarios = _agrupamentoUsuarioRepository.ObterGruposPorUsuario(usuario);

                foreach (GrupoUsuario grupoUsuario in grupoUsuarios)
                {
                    if (grupoUsuario.Codigo.ToUpper().Trim().Equals(grupoFiscal))
                    {
                        return true;
                    }
                }

                return false;
            }

            return true;
        }

        /// <summary>
        /// Fun��o para verificar se o Cte est� no prazo de cancelamento
        /// </summary>
        /// <param name="cte">Objeto Cte</param>
        /// <returns>Retorna true se puder efetuar o cancelamento</returns>
        private bool VerificarPrazoCancelamento(Cte cte)
        {
            int diasCancelamento;
            int.TryParse(RecuperarValorConfGeral(_chaveDiasCancelamentoCte), out diasCancelamento);

            var statusCte = _cteStatusRepository.ObterCteStatusPorSituacaoCte(cte, 100);
            var dataLimiteCancelamento = statusCte.DataHora.AddDays(diasCancelamento);

            return DateTime.Now < dataLimiteCancelamento;
        }

        /// <summary>
        /// Recupera o valor da con`figura��o cadastrado na Conf Geral
        /// </summary>
        /// <param name="chave">Chave da confGeral</param>
        /// <returns>Retorna o valor cadastrado na tabela confGeral</returns>
        private string RecuperarValorConfGeral(string chave)
        {
            ConfiguracaoTranslogic pathDiretorio = _configuracaoTranslogicRepository.ObterPorId(chave);

            return pathDiretorio != null ? pathDiretorio.Valor : string.Empty;
        }

        private bool EnviarEmail(Cte cte, string emailServer, int emailPort, string remetente, IList<CteEnvioXml> listaEnvio, TipoArquivoEnvioEnum tipoArquivoEnvio, Stream arquivoPdf, Stream arquivoXml, string senhaSmtp)
        {
            // mailMessage.To.Add(envio.EnvioPara);
            IList<string> listaEmailTo = new List<string>();
            IList<string> listaEmailCc = new List<string>();
            IList<string> listaEmailCo = new List<string>();

            string emailPara = string.Empty;
            string emailCopia = string.Empty;
            string emailCopiaOculta = string.Empty;

            string emailInvalidoPara = string.Empty;
            string emailInvalidoCopia = string.Empty;
            string emailInvalidoCopiaOculta = string.Empty;
            bool result = false;

            // string emailServer, int emailPort, string remetente
            var valorArray = CacheManager.ConfigGeral.Value.CteEmailServer.Trim().Split(':');
            emailServer = valorArray[0];
            emailPort = Convert.ToInt32(valorArray[1]);
            remetente = CacheManager.ConfigGeral.Value.CteEmailRemetente.Trim();
            senhaSmtp = CacheManager.ConfigGeral.Value.CteEmailSmtpSenha;

            try
            {
                string diretorio = Directory.GetCurrentDirectory();
                string caminhoLogoAll = diretorio + "\\" + "Logo-All.jpg";

                SmtpClient client;
                MailMessage mailMessage;
                AlternateView html;
                LinkedResource logo;

                string chave = cte.Chave;

                // Caso n�o tenha email para enviar, 
                if (listaEnvio.Count == 0)
                {
                    return false;
                }

                if (listaEnvio.Count > 0)
                {
                    foreach (CteEnvioXml envio in listaEnvio)
                    {
                        if (!Validators.EmailIsValid(envio.EnvioPara))
                        {
                            if (envio.TipoEnvioCte == TipoEnvioCteEnum.Email)
                            {
                                emailInvalidoPara += envio.EnvioPara;
                                emailInvalidoPara += ";";
                            }
                            else if (envio.TipoEnvioCte == TipoEnvioCteEnum.EmailCopia)
                            {
                                emailInvalidoCopia += envio.EnvioPara;
                                emailInvalidoCopia += ";";
                            }
                            else if (envio.TipoEnvioCte == TipoEnvioCteEnum.EmailCopiaOculta)
                            {
                                emailInvalidoCopiaOculta += envio.EnvioPara;
                                emailInvalidoCopiaOculta += ";";
                            }

                            continue;
                        }

                        if (envio.TipoEnvioCte == TipoEnvioCteEnum.Email)
                        {
                            listaEmailTo.Add(envio.EnvioPara);
                        }
                        else if (envio.TipoEnvioCte == TipoEnvioCteEnum.EmailCopia)
                        {
                            listaEmailCc.Add(envio.EnvioPara);
                        }
                        else
                        {
                            listaEmailCo.Add(envio.EnvioPara);
                        }
                    }

                    if (listaEmailTo.Count > 0 || listaEmailCc.Count > 0 || listaEmailCo.Count > 0)
                    {
                        // Envio de email
                        client = new SmtpClient(emailServer, emailPort);

                        if (!string.IsNullOrEmpty(senhaSmtp))
                        {
                            client.EnableSsl = true;
                            client.Credentials = new NetworkCredential(remetente, senhaSmtp);
                        }

                        mailMessage = new MailMessage();
                        mailMessage.BodyEncoding = Encoding.UTF8;
                        mailMessage.Subject = "Envio Cte - " + chave;
                        mailMessage.From = new MailAddress(remetente);

                        foreach (string email in listaEmailTo)
                        {
                            mailMessage.To.Add(email);
                            emailPara += email + ";";
                        }

                        foreach (string email in listaEmailCc)
                        {
                            mailMessage.CC.Add(email);
                            emailCopia += email + ";";
                        }

                        foreach (string email in listaEmailCo)
                        {
                            mailMessage.Bcc.Add(email);
                            emailCopiaOculta += email + ";";
                        }

                        var cteConfEmail = _configEmailRepository.ObterPorCte(cte);

                        // N�o enviar corpo no email, somente o anexo
                        if (cteConfEmail != null)
                        {
                            if (!String.IsNullOrEmpty(cteConfEmail.AssuntoEmail) && !String.IsNullOrWhiteSpace(cteConfEmail.AssuntoEmail))
                            {
                                mailMessage.Subject = cteConfEmail.AssuntoEmail;
                            }
                        }
                        else
                        {
                            html = AlternateView.CreateAlternateViewFromString(GerarHtmlEmail(cte), null, MediaTypeNames.Text.Html);
                            logo = new LinkedResource(caminhoLogoAll, MediaTypeNames.Image.Jpeg);
                            logo.ContentId = "Logo";
                            html.LinkedResources.Add(logo);
                            mailMessage.AlternateViews.Add(html);
                            mailMessage.IsBodyHtml = true;
                        }

                        // Anexar os arquivos
                        if (arquivoPdf != null)
                        {
                            // Posiciona no inicio o arquivo
                            arquivoPdf.Seek(0, SeekOrigin.Begin);

                            Stream arquivoPdfComunicado = AnexarComunicadoDacte(cte, arquivoPdf);
                            mailMessage.Attachments.Add(new Attachment(arquivoPdfComunicado, string.Concat(chave, ".pdf")));
                            // mailMessage.Attachments.Add(new Attachment(arquivoPdf, string.Concat(chave, ".pdf")));
                        }

                        if (arquivoXml != null)
                        {
                            // Posiciona no inicio o arquivo
                            arquivoXml.Seek(0, SeekOrigin.Begin);

                            mailMessage.Attachments.Add(new Attachment(arquivoXml, string.Concat(chave, ".xml")));
                        }

                        client.Send(mailMessage);

                        result = true;

                        // Insere na tabela hist�rica
                        InserirHistoricoEnvioEmail(cte, emailPara, emailCopia, emailCopiaOculta, tipoArquivoEnvio, remetente, emailServer);

                        // Loga o envio de email
                        string buffer = string.Format("Enviado email de: {0} - para: {1} - C�pia: {2} - Copia Oculta {3}", remetente, emailPara, emailCopia, emailCopiaOculta);
                        _cteLogService.InserirLogInfo(cte, "CteService", buffer);
                    }

                    if ((!String.IsNullOrEmpty(emailInvalidoCopia)) || (!String.IsNullOrEmpty(emailInvalidoPara)) || (!String.IsNullOrEmpty(emailInvalidoCopiaOculta)))
                    {
                        InserirEnvioEmailErro(cte, emailInvalidoPara, emailInvalidoCopia, emailInvalidoCopiaOculta, "Email inv�lido", tipoArquivoEnvio, remetente, emailServer);

                        string buffer = string.Format("Erro envio email de: {0} - para: {1} - C�pia: {2} - Copia Oculta {3}", remetente, emailInvalidoPara, emailInvalidoCopia, emailInvalidoCopiaOculta);
                        _cteLogService.InserirLogErro(cte, buffer);
                    }
                }
            }
            catch (Exception ex)
            {
                Sis.LogException(ex, string.Format("Erro ao enviar e-mail do Cte: Id={0} - Chave={1} - De: {2} - Para: {3} - C�pia: {4} - Copia Oculta {5}", cte.Id, cte.Chave, remetente, emailPara, emailCopia, emailCopiaOculta));
                InserirEnvioEmailErro(cte, emailPara, emailCopia, emailCopiaOculta, ex.Message, tipoArquivoEnvio, remetente, emailServer);

                string buffer = string.Format("Enviado email de: {0} - para: {1} - C�pia: {2} - Copia Oculta {3}", remetente, emailPara, emailCopia, emailCopiaOculta);
                _cteLogService.InserirLogInfo(cte, buffer);
            }

            return result;
        }

        private void RegistrarLogDetalhado(Cte cte, string msg)
        {
            _cteLogService.InserirLogInfo(cte, "CteService", string.Format("{0} ({1} - {2})", msg, DateTime.Now, Dns.GetHostName()));
        }

        private Stream AnexarComunicadoDacte(Cte cte, Stream arquivoPdf)
        {
            CteComunicado cteComunicado = _cteComunicadoRepository.ObterPorSiglaUf(cte.SiglaUfFerrovia);

            PdfMerge merge = new PdfMerge();
            merge.AddFile(arquivoPdf);

            if (cteComunicado != null)
            {
                merge.AddFile(cteComunicado.ComunicadoPdf);

                // Grava no status do Cte
                _cteStatusRepository.InserirCteStatus(cte, ObterUsuarioRobo(), new CteStatusRetorno { Id = 31 });

                string buffer = string.Format("Adicionado o arquivo  de comunicado no envio do DACTE. Sigla Uf Ferrovia: {0}", cte.SiglaUfFerrovia);
                _cteLogService.InserirLogInfo(cte, "CteService", buffer);
            }

            return merge.Execute();
        }

        /*
         * /// <summary>
        /// Salva os e-mail
        /// </summary>
        /// <param name="email"> e-mail a ser alterado</param>
        /// <param name="novoEmail"> Novo e-mail a ser alterado</param>
        /// <param name="fluxo"> Fluxo informado</param>
        public void SalvarEmailCteReenvioArquivo(string email, string novoEmail, string fluxo)
        {
                                        _cteRepository.SalvarEmailCteReenvioArquivo(email, novoEmail, fluxo);
        }
        */

        public IList<HistoricoEnvioEmailDto> ObterConfirmacaoEnvioEmail(int[] idCtes)
        {
            return _cteEnvioEmailHistoricoRepository.ObterPor(idCtes);
        }

        public Cte Obter(int valor)
        {
            return _cteRepository.ObterPorId(valor);

        }

        public IList<Cte> ObterListaCte(List<int> listaCte)
        {
            return _cteRepository.ObterTodosPorId(listaCte.ToArray());
        }

        public ResultadoPaginado<CteVirtuaisSemVinculoMdfeDto> RetornaListaCteVirtuaisSemVinculoMdfe(DetalhesPaginacaoWeb pagination, DetalhesFiltro<object>[] filtros, bool isExportarExcel)
        {
            DateTime dataIni = DateTime.Now;
            DateTime dataFim = DateTime.Now;
            string fluxo = string.Empty;
            string[] numVagao = null;
            string[] chaveCte = null;

            if (filtros != null)
            {
                foreach (var detalheFiltro in filtros)
                {
                    if (detalheFiltro.Campo.Equals("dataInicial") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        string texto = detalheFiltro.Valor[0].ToString();
                        dataIni = DateTime.ParseExact(texto, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                    }

                    if (detalheFiltro.Campo.Equals("dataFinal") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        string texto = detalheFiltro.Valor[0].ToString();
                        dataFim = DateTime.ParseExact(texto, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                        dataFim = dataFim.AddDays(1).AddSeconds(-1);
                    }

                    if (detalheFiltro.Campo.Equals("fluxo") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        fluxo = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
                    }

                    if (detalheFiltro.Campo.Equals("chave") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        chaveCte = detalheFiltro.Valor[0].ToString().Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                    }

                    if (detalheFiltro.Campo.Equals("Vagao") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        numVagao = detalheFiltro.Valor[0].ToString().Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                    }
                }
            }
            return ListaCteVirtuaisSemVinculoMdfe(pagination, dataIni, dataFim, fluxo, chaveCte, numVagao, isExportarExcel);
        }

        public ResultadoPaginado<CteVirtuaisSemVinculoMdfeDto> ListaCteVirtuaisSemVinculoMdfe(DetalhesPaginacaoWeb pagination, DateTime dataInicial, DateTime dataFinal, string codFluxo, string[] chaveCte, string[] numVagao, bool isExportarExcel)
        {
            string[] numDespacho = null;
            var result = _cteRepository.CteVirtuaisSemVinculoMdfeDto(pagination, dataInicial, dataFinal, codFluxo, chaveCte, numVagao, isExportarExcel);

            var listIdDespachoVirtual = string.Empty;

            if (result.Items.Count > 0)
            {
                var listAux = new List<string>();
                foreach (var item in result.Items)
                {
                    if (!listAux.Any(x => x == item.ID_DESPACHO.ToString()))
                    {
                        listAux.Add(item.ID_DESPACHO.ToString());
                        listIdDespachoVirtual += item.ID_DESPACHO + ";";
                    }
                }

                if (!string.IsNullOrWhiteSpace(listIdDespachoVirtual))
                {
                    numDespacho = listIdDespachoVirtual.ToString().Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                }

                var listMdfePai = _cteRepository.CteVirtuaisMdfePai(numDespacho);

                foreach (var item in result.Items)
                {
                    var obj = listMdfePai.FirstOrDefault(x => x.ID_DESPACHO == item.ID_DESPACHO);
                    if (obj != null)
                    {
                        item.ID_AREA_OPERACIONAL = obj.ID_AREA_OPERACIONAL;
                        item.ID_TREM = obj.ID_TREM;
                        item.COMPOSICAO = obj.COMPOSICAO;
                        item.PREFIXO_TREM = obj.PREFIXO_TREM;
                        item.ID_DESPACHO = obj.ID_DESPACHO;
                        item.ID_MOVIMENTACAO = obj.ID_MOVIMENTACAO;
                    }
                    else
                    {
                        item.ID_AREA_OPERACIONAL = 0;
                        item.ID_TREM = 0;
                        item.COMPOSICAO = 0;
                        item.PREFIXO_TREM = "-";
                        item.ID_DESPACHO = 0;
                        item.ID_MOVIMENTACAO = 0;
                    }
                }
            }
            return result;
        }

        #region -- Retorna a lista sobre o Monitoramento Planejamento Descarga --

        public IList<MonitoramentoPlanejamentoDescargaDto> RetornaListaMonitoramentoPlanejamentoDescarga(DetalhesFiltro<object>[] filtros)
        {
            DateTime dataIni = DateTime.Now;
            DateTime dataFim = DateTime.Now;
            string prefixo = string.Empty;
            string os = string.Empty;
            string terminal = string.Empty;
            var vagao = string.Empty;
            if (filtros != null)
            {
                foreach (var detalheFiltro in filtros)
                {
                    if (detalheFiltro.Campo.Equals("dataInicial") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        string texto = detalheFiltro.Valor[0].ToString();
                        dataIni = DateTime.ParseExact(texto, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                    }
                    if (detalheFiltro.Campo.Equals("dataFinal") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        string texto = detalheFiltro.Valor[0].ToString();
                        dataFim = DateTime.ParseExact(texto, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                        dataFim = dataFim.AddDays(1).AddSeconds(-1);
                    }
                    if (detalheFiltro.Campo.Equals("prefixo") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        prefixo = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
                    }
                    if (detalheFiltro.Campo.Equals("os") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        os = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
                    }
                    if (detalheFiltro.Campo.Equals("terminal") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                    {
                        terminal = detalheFiltro.Valor[0].ToString().ToUpperInvariant();
                    }
                }
            }
            return ListaMonitoramentoPlanejamentoDescarga(dataIni, dataFim, prefixo, os, terminal, vagao);
        }

        public IList<MonitoramentoPlanejamentoDescargaDto> ListaMonitoramentoPlanejamentoDescarga(DateTime dataInicial, DateTime dataFinal, string prefixo, string os, string terminal, string vagao)
        {
            var result = _cteRepository.MonitoramentoPlanejamentoDescarga(dataInicial, dataFinal, prefixo, os, terminal, vagao);
            return result;
        }


        #endregion
    }
}
