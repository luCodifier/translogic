namespace Translogic.Modules.Core.Domain.Services.Ctes
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using Castle.Services.Transaction;
    using Model.Codificador;
    using Model.Codificador.Repositories;
    using Model.Diversos;
    using Model.Diversos.Bacen;
    using Model.Diversos.Bacen.Repositories;
    using Model.Diversos.Cte;
    using Model.Diversos.Ibge;
    using Model.Diversos.Ibge.Repositories;
    using Model.Estrutura;
    using Model.Estrutura.Repositories;
    using Model.FluxosComerciais;
    using Model.FluxosComerciais.ConhecimentoTransporteEletronico;
    using Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;
    using Model.FluxosComerciais.Nfes;
    using Model.FluxosComerciais.Pedidos.Despachos;
    using Model.FluxosComerciais.Pedidos.Despachos.Repositories;
    using Model.FluxosComerciais.Repositories;
    using Model.FluxosComerciais.TriangulusConfig;
    using Model.FluxosComerciais.TriangulusConfig.Repositories;
    using Model.Trem.Veiculo.Vagao;
    using Model.Via;
    using NHibernate.Validator.Engine;
    using Translogic.Core.Infrastructure.Validation;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.InterfaceSap.Repositories;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.TriangulusConfig.Interfaces;
    using Translogic.Modules.Core.Domain.Services.FluxosComerciais;
    using Util;

    /// <summary>
    /// Classe para grava��o dos dados na config do CT-e de complemento
    /// </summary>
    [Transactional]
    public class GravarConfigCteComplementoService
    {
        private readonly IUfIbgeRepository _unidadeFederativaIbgeRepository;
        private readonly IPaisBacenRepository _paisBacenRepository;
        private readonly IComposicaoFreteContratoRepository _composicaoFreteContratoRepository;
        private readonly IAssociaFluxoInternacionalRepository _associaFluxoInternacionalRepository;
        private readonly IConfiguracaoTranslogicRepository _configuracaoTranslogicRepository;
        private readonly ICteArvoreRepository _cteArvoreRepository;
        private readonly ICteAgrupamentoRepository _cteAgrupamentoRepository;
        private readonly IComposicaoFreteCteRepository _composicaoFreteCteRepository;
        private readonly ICteRepository _cteRepository;
        private readonly IContratoRepository _contratoRepository;
        private readonly ICteComplementadoRepository _cteComplementadoRepository;
        private readonly ICte01Repository _cte01Repository;
        private readonly ICte05Repository _cte05Repository;
        private readonly ICte06Repository _cte06Repository;
        private readonly ICte07Repository _cte07Repository;
        private readonly ICte08Repository _cte08Repository;
        private readonly ICte35Repository _cte35Repository;
        private readonly ICte36Repository _cte36Repository;
        private readonly ISerieDespachoUfRepository _serieDespachoUfRepository;
        private readonly INfe03FilialRepository _nfe03FilialRepository;
        private readonly IValidatorEngine _validatorEngine;
        private readonly ICidadeIbgeRepository _cidadeIbgeRepository;
        private readonly IEmpresaFerroviaRepository _empresaFerroviaRepository;
        private readonly IEmpresaClienteRepository _empresaFerroviaClienteRepository;
        private readonly ICteDetalheRepository _cteDetalheRepository;
        private readonly IEmpresaInterfaceCteRepository _empresaInterfaceCteRepository;
        private readonly ICteEmpresasRepository _cteEmpresasRepository;
        private readonly IContratoHistoricoRepository _contratoHistoricoRepository;
        private readonly CteService _cteService;
        private readonly NfeService _nfeService;
        private readonly IDespachoTranslogicRepository _despachoTranslogicRepository;
        private readonly ISapChaveCteRepository _sapChaveCteRepository;

        private readonly string _chaveCteFormaEmissao = "CTE_FORMA_EMISSAO";
        private readonly string _chaveCteTipoAmbiente = "CTE_TIPO_AMBIENTE";
        private readonly string _chaveCteVersaoProcesso = "CTE_VERSAO_PROCESSO";
        private readonly string _chaveCteCodigoInternoFilial = "CTE_CODIGO_INTERNO_FILIAL_DEFAULT";
        private readonly string _chaveVersaoSefaz = "CTE_VERSAO_SEFAZ";
        private readonly string _chaveCfopEstadual = "CTE_CFOP_COD_TRAFEGO_MUTUO_ESTADUAL";
        private readonly string _chaveCfopInterestadual = "CTE_CFOP_COD_TRAFEGO_MUTUO_INTERESTADUAL";
        private readonly string _chaveCfopInternacional = "CTE_CFOP_COD_TRAFEGO_MUTUO_INTERNACIONAL";

        private readonly ICte100Repository _cte100Repository;
        private readonly ICte101Repository _cte101Repository;
        private readonly ICte102Repository _cte102Repository;
        private readonly ICte103Repository _cte103Repository;
        private readonly ICte105Repository _cte105Repository;
        private readonly ICte107Repository _cte107Repository;
        private readonly ICte109Repository _cte109Repository;
        private readonly ICte111Repository _cte111Repository;
        private readonly ICte113Repository _cte113Repository;

        private CteEmpresas _cteEmpresas;
        private Cte _cteComplementar;
        private Cte _cteRaiz;
        private Cte _cteRaizAgrupamento;
        private IList<CteComplementado> _listaCteComplementado;
        private IList<CteDetalhe> _listaCteComplementadoDetalhe;
        private FluxoComercial _fluxoComercial;
        private StringBuilder _errosValidacao;
        private Vagao _vagao;
        private CteLogService _cteLogService;
        private CidadeIbge _cidadeIbgeEmpresaTomadora;
        private CidadeIbge _cidadeIbgeEstacaoMaeOrigemFluxoComercial;
        private CidadeIbge _cidadeIbgeRemetenteFiscal;
        private CidadeIbge _cidadeIbgeDestinatariaFiscal;
        private Municipio _municipioEstacaoMaeOrigemFluxoComercial;
        private Estado _estadoEstacaoMaeOrigemFluxoComercial;
        private Municipio _municipioEstacaoMaeDestinoFluxoComercial;
        private Estado _estadoEstacaoMaeDestinoFluxoComercial;
        private CidadeIbge _cidadeIbgeEstacaoMaeDestinoFluxoComercial;
        private IEmpresa _empresaOperadoraEstacaoMaeOrigemFluxoComercial;
        private CidadeIbge _cidadeIbgeEmpresaRemetenteContrato;
        private CidadeIbge _cidadeIbgeEmpresaFerrovia;
        private EstacaoMae _estacaoMaeOrigemFluxoComercial;
        private EstacaoMae _estacaoMaeDestinoFluxoComercial;
        private IEmpresa _empresaRemetenteFluxoComercial;
        private IEmpresa _empresaFerroviaCliente;
        private IEmpresa _empresaTomadora;
        private IEmpresa _empresaDestinatariaFluxoComercial;
        private IEmpresa _empresaContratoRemetenteFiscal;
        private IEmpresa _empresaContratoDestinatariaFiscal;

        private Nfe03Filial _nfe03Filial;
        private Estado _estadoEmpresaRemetenteFluxoComercial;
        private SerieDespachoUf _serieDespachoUfCteComplementado;
        private ContratoHistorico _contratoHistoricoCteComplementado;
        private Cte _cteCteComplementado;
        private TipoTrafegoMutuoEnum _tipoTrafegoMutuo;
        // private ConfiguracaoTranslogic _cfopEstadual;
        // private ConfiguracaoTranslogic _cfopInterestadual;
        // private ConfiguracaoTranslogic _cfopInternacional;
        private string _cfop;
        private string _cfopDescricao;
        private int codigoTomador;

        /// <summary>
        /// Initializes a new instance of the class.<see cref="GravarConfigCteComplementoService"/> 
        /// </summary>
        /// <param name="composicaoFreteContratoRepository">Reposit�rio da composi��o do frete de contrato injetado</param>
        /// <param name="cteDetalheRepository">Reposit�rio do detalhe do Cte injetado</param>
        /// <param name="cteArvoreRepository">Reposit�rio de cte arvore injetado</param>
        /// <param name="composicaoFreteCteRepository">Reposit�rio da composicao frete cte injetado</param>
        /// <param name="cteComplementadoRepository">Reposit�rio do cte complementado injetado</param>
        /// <param name="unidadeFederativaIbgeRepository">Reposit�rio do UF do IBGE injetado</param>
        /// <param name="paisBacenRepository">Reposit�rio do codigo do pais no BACEN injetado</param>
        /// <param name="configuracaoTranslogicRepository">Reposit�rio da configura��o do translogic injetado</param>
        /// <param name="cteAgrupamentoRepository">Reposit�rio de cte Agrupamento injetado</param>
        /// <param name="cte01Repository">Reposit�rio de cte01 dados principais</param>
        /// <param name="cte35Repository">Reposit�rio de cte35 dados cte complementar</param>
        /// <param name="cte36Repository">Reposit�rio de cte36 dados componentes cte complementar</param>
        /// <param name="serieDespachoUfRepository">Reposit�rio da serieDespachoUfRepository injetado</param>
        /// <param name="nfe03FilialRepository">Reposit�rio da nfe03FilialRepository injetado</param>
        /// <param name="validatorEngine">Validator Engine Injetado</param>
        /// <param name="cidadeIbgeRepository">Reposit�rio da cidade IBGE injetado</param>
        /// <param name="empresaFerroviaRepository">Reposit�rio da empresa ferrovia injetado</param>
        /// <param name="associaFluxoInternacionalRepository">Reposit�rio do associa fluxo internacional injetado</param>
        /// <param name="empresaFerroviaClienteRepository">Reposit�rio da empresa ferrovia Cliente injetado</param>
        /// <param name="cte05Repository">Reposit�rio de cte05 dados de informa��es do emitente</param>
        /// <param name="cte08Repository">Reposit�rio de cte08 dados de informa��es do valor da presta��o de servico</param>
        /// <param name="cte06Repository">Reposit�rio de cte06 dados de informa��es da nota fiscal</param>
        /// <param name="cte07Repository">Reposit�rio de cte07 dados de informa��es do expeditor</param>
        /// <param name="empresaInterfaceCteRepository">Reposit�rio da empresa Interface cte injetado</param>
        /// <param name="cteRepository">Reposit�rio do CTE Interface cte injetado</param>
        /// <param name="cteEmpresasRepository">Reposit�rio do CTE empresas injetado</param>
        /// <param name="cte100Repository">Reposit�rio de cte100 dados de informa��es de documentos</param>
        /// <param name="cte101Repository">Reposit�rio de cte101 dados de informa��es de documentos</param>
        /// <param name="cte102Repository">Reposit�rio de cte102 dados de informa��es de documentos</param>
        /// <param name="cte103Repository">Reposit�rio de cte103 dados de informa��es de documentos</param>
        /// <param name="cte105Repository">Reposit�rio de cte105 dados de informa��es de documentos</param>
        /// <param name="cte107Repository">Reposit�rio de cte107 dados de informa��es de documentos</param>
        /// <param name="cte109Repository">Reposit�rio de cte109 dados de informa��es de documentos</param>
        /// <param name="cte111Repository">Reposit�rio de cte111 dados de informa��es de documentos</param>
        /// <param name="cte113Repository">Reposit�rio de cte113 dados de informa��es de documentos</param>
        /// <param name="contratoHistoricoRepository">Reposit�rio do contrato historico</param>
        /// <param name="contratoRepository">Reposit�rio de do contrato historico</param>
        /// <param name="cteService">Servi�o do Cte</param>
        /// <param name="nfeService">Servi�o da NFe</param>
        public GravarConfigCteComplementoService(IComposicaoFreteContratoRepository composicaoFreteContratoRepository, ICteDetalheRepository cteDetalheRepository, ICteArvoreRepository cteArvoreRepository, IComposicaoFreteCteRepository composicaoFreteCteRepository, ICteComplementadoRepository cteComplementadoRepository, IUfIbgeRepository unidadeFederativaIbgeRepository, IPaisBacenRepository paisBacenRepository, IConfiguracaoTranslogicRepository configuracaoTranslogicRepository, ICteAgrupamentoRepository cteAgrupamentoRepository, ICte01Repository cte01Repository, ICte35Repository cte35Repository, ICte36Repository cte36Repository, ISerieDespachoUfRepository serieDespachoUfRepository, INfe03FilialRepository nfe03FilialRepository, IValidatorEngine validatorEngine, ICidadeIbgeRepository cidadeIbgeRepository, IEmpresaFerroviaRepository empresaFerroviaRepository, IAssociaFluxoInternacionalRepository associaFluxoInternacionalRepository, IEmpresaClienteRepository empresaFerroviaClienteRepository, ICte05Repository cte05Repository, ICte08Repository cte08Repository, ICte06Repository cte06Repository, ICte07Repository cte07Repository, IEmpresaInterfaceCteRepository empresaInterfaceCteRepository, ICteRepository cteRepository, ICteEmpresasRepository cteEmpresasRepository, ICte100Repository cte100Repository, ICte101Repository cte101Repository, ICte102Repository cte102Repository, ICte103Repository cte103Repository, ICte105Repository cte105Repository, ICte107Repository cte107Repository, ICte109Repository cte109Repository, ICte111Repository cte111Repository, ICte113Repository cte113Repository, IContratoHistoricoRepository contratoHistoricoRepository, IContratoRepository contratoRepository, CteService cteService, NfeService nfeService, IDespachoTranslogicRepository _despachoTranslogicRepository, ISapChaveCteRepository _sapChaveCteRepository)
        {
            _empresaFerroviaRepository = empresaFerroviaRepository;
            _cteEmpresasRepository = cteEmpresasRepository;
            _cte100Repository = cte100Repository;
            _cte101Repository = cte101Repository;
            _cte102Repository = cte102Repository;
            _cte103Repository = cte103Repository;
            _cte105Repository = cte105Repository;
            _cte107Repository = cte107Repository;
            _cte109Repository = cte109Repository;
            _cte111Repository = cte111Repository;
            _cte113Repository = cte113Repository;
            _contratoHistoricoRepository = contratoHistoricoRepository;
            _cteService = cteService;
            _empresaInterfaceCteRepository = empresaInterfaceCteRepository;
            _cte01Repository = cte01Repository;
            _cte05Repository = cte05Repository;
            _cte06Repository = cte06Repository;
            _cte07Repository = cte07Repository;
            _cte08Repository = cte08Repository;
            _cte35Repository = cte35Repository;
            _cte36Repository = cte36Repository;
            _validatorEngine = validatorEngine;
            _cidadeIbgeRepository = cidadeIbgeRepository;
            _cteComplementadoRepository = cteComplementadoRepository;
            _composicaoFreteCteRepository = composicaoFreteCteRepository;
            _empresaFerroviaClienteRepository = empresaFerroviaClienteRepository;
            _cteArvoreRepository = cteArvoreRepository;
            _composicaoFreteContratoRepository = composicaoFreteContratoRepository;
            _associaFluxoInternacionalRepository = associaFluxoInternacionalRepository;
            _unidadeFederativaIbgeRepository = unidadeFederativaIbgeRepository;
            _paisBacenRepository = paisBacenRepository;
            _configuracaoTranslogicRepository = configuracaoTranslogicRepository;
            _cteAgrupamentoRepository = cteAgrupamentoRepository;
            _serieDespachoUfRepository = serieDespachoUfRepository;
            _nfe03FilialRepository = nfe03FilialRepository;
            _cteDetalheRepository = cteDetalheRepository;
            _cteRepository = cteRepository;
            _contratoRepository = contratoRepository;
            _nfeService = nfeService;
            this._despachoTranslogicRepository = _despachoTranslogicRepository;
            this._sapChaveCteRepository = _sapChaveCteRepository;
        }

        /// <summary>
        /// Executa a grava��o na config do Cte complementar
        /// </summary>
        /// <param name="cteComplementar">Cte complementar que ser� gerado o arquivo de complemento</param>
        public void Executar(Cte cteComplementar)
        {
            _cteComplementar = cteComplementar;
            _cteEmpresas = new CteEmpresas();

            try
            {
                Executar();
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(cteComplementar, "GravarConfigCteComplementoService", string.Format("Executar: {0}", ex.Message), ex);
                throw;
            }
        }

        /// <summary>
        /// Executar a grava��o do cte em um m�todo transacional
        /// </summary>
        [Transaction]
        protected virtual void Executar()
        {
            // Limpa as variaveis globais
            LimparDados();

            // Carrega os Dados para gera��o do CTE
            CarregarDados();

            // Valida se possui os campos necess�rios para gerar o CTE
            ValidarDados();

            CalcularValorCte();

            // Limpa as tabelas, no caso de ser reenvio
            RemoverCteConfigParaReenvio();

            // Grava os Dados principais do CTE
            GravarDadosPrincipaisCte();

            // Informacao do emitente do CT-e
            GravarInformacoesEmitente();

            if (_cteComplementar.Versao.CodigoVersao.Equals("2.00"))
            {
                // Grava informacoes do documento
                GravarInformacoesDocumentos();
            }
            else
            {
                GravarInformacoesDocumentos1_04();
            }

            // Grava informa��es do Expedidor / Recebedor / Destinatario
            GravarInformacoesExpedidorRecebedorDestinatario();

            // Grava as informacoes do CTE complementado
            GravarInformacoesCteComplementar();

            // Gravar a informa�ao dos valores de presta��o de servico
            GravarValorPrestacaoServicoImposto();

            // Grava os dados das empresas utilizadas
            GravarCteEmpresas();
        }

        private void GravarCteEmpresas()
        {
            /*
                    0-Remetente
                    1-Expedidor
                    2-Recebedor
                    3-Destinat�rio
                    4-Outros
             */
            switch (codigoTomador)
            {
                case 0:
                    _cteEmpresas.EmpresaTomadora = _cteEmpresas.EmpresaRemetente;
                    break;
                case 1:
                    _cteEmpresas.EmpresaTomadora = _cteEmpresas.EmpresaExpedidor;
                    break;
                case 2:
                    _cteEmpresas.EmpresaTomadora = _cteEmpresas.EmpresaRecebedor;
                    break;
                case 3:
                    _cteEmpresas.EmpresaTomadora = _cteEmpresas.EmpresaDestinatario;
                    break;
            }

            _cteEmpresas.CodigoTomador = codigoTomador;

            if (_cteEmpresas.EmpresaRemetente != null && _cteEmpresas.EmpresaRemetente.Id == null)
            {
                _cteEmpresas.EmpresaRemetente = null;
            }

            if (_cteEmpresas.EmpresaDestinatario != null && _cteEmpresas.EmpresaDestinatario.Id == null)
            {
                _cteEmpresas.EmpresaDestinatario = null;
            }

            _cteEmpresas.Cte = _cteComplementar;
            _cteEmpresasRepository.InserirOuAtualizar(_cteEmpresas);
        }

        /// <summary>
        /// Remove o registro das tabelas caso j� exista, utilizado quando � reenvio
        /// </summary>
        private void RemoverCteConfigParaReenvio()
        {
            _cteEmpresasRepository.RemoverPorCte(_cteComplementar);
            _cte01Repository.Remover(ObterIdentificadorFilial(), int.Parse(_cteComplementar.Numero), int.Parse(_cteComplementar.Serie));
            _cte05Repository.Remover(ObterIdentificadorFilial(), int.Parse(_cteComplementar.Numero), int.Parse(_cteComplementar.Serie));
            _cte06Repository.Remover(ObterIdentificadorFilial(), int.Parse(_cteComplementar.Numero), int.Parse(_cteComplementar.Serie));
            _cte07Repository.Remover(ObterIdentificadorFilial(), int.Parse(_cteComplementar.Numero), int.Parse(_cteComplementar.Serie));
            _cte08Repository.Remover(ObterIdentificadorFilial(), int.Parse(_cteComplementar.Numero), int.Parse(_cteComplementar.Serie));
            _cte35Repository.Remover(ObterIdentificadorFilial(), int.Parse(_cteComplementar.Numero), int.Parse(_cteComplementar.Serie));
            _cte36Repository.Remover(ObterIdentificadorFilial(), int.Parse(_cteComplementar.Numero), int.Parse(_cteComplementar.Serie));

            _cte100Repository.Remover(ObterIdentificadorFilial(), int.Parse(_cteComplementar.Numero), int.Parse(_cteComplementar.Serie));
            _cte101Repository.Remover(ObterIdentificadorFilial(), int.Parse(_cteComplementar.Numero), int.Parse(_cteComplementar.Serie));
            _cte102Repository.Remover(ObterIdentificadorFilial(), int.Parse(_cteComplementar.Numero), int.Parse(_cteComplementar.Serie));
            _cte103Repository.Remover(ObterIdentificadorFilial(), int.Parse(_cteComplementar.Numero), int.Parse(_cteComplementar.Serie));
            _cte105Repository.Remover(ObterIdentificadorFilial(), int.Parse(_cteComplementar.Numero), int.Parse(_cteComplementar.Serie));
            _cte109Repository.Remover(ObterIdentificadorFilial(), int.Parse(_cteComplementar.Numero), int.Parse(_cteComplementar.Serie));
            _cte113Repository.Remover(ObterIdentificadorFilial(), int.Parse(_cteComplementar.Numero), int.Parse(_cteComplementar.Serie));
            _cte111Repository.Remover(ObterIdentificadorFilial(), int.Parse(_cteComplementar.Numero), int.Parse(_cteComplementar.Serie));
            _cte107Repository.Remover(ObterIdentificadorFilial(), int.Parse(_cteComplementar.Numero), int.Parse(_cteComplementar.Serie));
        }

        /// <summary>
        /// Tabela utilizada para dados principais do CT-e (1-1) - ICTE01_INF
        /// </summary>
        private void GravarDadosPrincipaisCte()
        {
            Cte01 cte01 = null;

            try
            {
                int intAux;

                ConfiguracaoTranslogic formaEmissao = _configuracaoTranslogicRepository.ObterPorId(_chaveCteFormaEmissao);
                // ConfiguracaoTranslogic tipoAmbiente = _configuracaoTranslogicRepository.ObterPorId(_chaveCteTipoAmbiente);
                ConfiguracaoTranslogic versaoProcesso = _configuracaoTranslogicRepository.ObterPorId(_chaveCteVersaoProcesso);
                ConfiguracaoTranslogic versaoSefaz = _configuracaoTranslogicRepository.ObterPorId(_chaveVersaoSefaz);

                CidadeIbge cidadeIbgeCalculoFrete;
                if ((_cteComplementar.Id == _cteRaizAgrupamento.Id) && (_tipoTrafegoMutuo == TipoTrafegoMutuoEnum.FaturamentoEmpresaGrupo))
                {
                    // TipoTrafegoMutuoEnum.FaturamentoEmpresaGrupo
                    cidadeIbgeCalculoFrete = _empresaRemetenteFluxoComercial.CidadeIbge;
                }
                else if (_tipoTrafegoMutuo == TipoTrafegoMutuoEnum.FaturamentoEmpresaForaGrupo)
                {
                    // TipoTrafegoMutuoEnum.FaturamentoEmpresaForaGrupo
                    cidadeIbgeCalculoFrete = _empresaRemetenteFluxoComercial.CidadeIbge;
                }
                else
                {
                    // Tipo CTE filho ou Trafego Mutuo IntercambioFaturamentoOrigem ou IntercambioFaturamentoDestino
                    cidadeIbgeCalculoFrete = _cidadeIbgeEmpresaFerrovia;
                }

                cte01 = new Cte01();

                // Codigo interno da filial (PK)
                cte01.CfgUn = ObterIdentificadorFilial();

                // Numero do documento CT-e (PK)
                int.TryParse(_cteComplementar.Numero, out intAux);
                cte01.CfgDoc = intAux;

                // Serie do documento CT-e (PK)
                // cte01.CfgSerie = _cte.Serie;
                cte01.CfgSerie = int.Parse(_cteComplementar.Serie).ToString();

                // Versao do CT-e
                cte01.InfCteVersao = double.Parse(_cteComplementar.Versao.CodigoVersao, CultureInfo.GetCultureInfo("en-US"));

                // Identificador da TAG a ser assinada
                cte01.InfCteId = _cteComplementar.Id.ToString();

                // cte01.IdeCuf = objUfIbge.Id;
                string codufFerrovia = _cteComplementar.Chave.Substring(0, 2);
                cte01.IdeCuf = Convert.ToInt32(codufFerrovia);

                // C�digo num�rico gerado aleatoriamente.
                string codVag = "9" + _vagao.Codigo;
                if (int.TryParse(codVag, out intAux))
                {
                    cte01.IdeCct = intAux;
                }

                // C�digo CFOP retirado a partir da tabela CONTRATO.
                if (int.TryParse(_cfop, out intAux))
                {
                    cte01.IdeCfOp = intAux;
                }

                // Natureza da opera��o
                cte01.IdeNatOp = _cfopDescricao;

                // Modelo do documento fiscal. Sempre fixo em 57.
                cte01.IdeMod = "57";

                // S�rie do CTE
                cte01.IdeSerie = int.TryParse(_cteComplementar.Serie, out intAux) ? intAux : 0;

                // N�mero do CTE
                cte01.IdeNct = int.TryParse(_cteComplementar.Numero, out intAux) ? intAux : 0;

                // Data e hora da emiss�o do CTE
                cte01.IdeDhEmi = GetDhEmissao();

                // Formato impressao 1 - Retrato
                cte01.IdeTpImp = 1;

                // Tipo da emiss�o se � normal ou conting�ncia.
                cte01.IdeTpEmis = int.TryParse(formaEmissao.Valor, out intAux) ? intAux : 0;

                // D�gito verificador da chave de acesso do CTE
                cte01.IdeCdv = Convert.ToInt32(_cteComplementar.Chave.Substring(_cteComplementar.Chave.Length - 1, 1));

                // Ambiente de homologa��o ou produ��o
                cte01.IdeTpAmb = _cteComplementar.AmbienteSefaz;

                // Tipo do CTE. (1-Complemento).
                cte01.IdeTpCte = 1;

                // Processo de emiss�o. Hoje � usado 0=Emiss�o com aplicativo do contribuinte.
                cte01.IdeProcEmi = 0;

                // Vers�o do processo de emiss�o.
                cte01.IdeVerProc = versaoProcesso.Valor;

                // C�digo do munic�pio de onde o CTE est� sendo emitido.
                CidadeIbge cidadeEmitente = _cidadeIbgeRepository.ObterPorDescricaoUf(_nfe03Filial.Cidade.ToUpper().Trim(), _cteComplementar.SiglaUfFerrovia);
                cte01.IdeCMunEmi = cidadeEmitente.CodigoIbge;

                // Nome do munic�pio de origem do fluxo
                if (cidadeEmitente != null)
                {
                    if (cidadeEmitente.CodigoIbge == 9999999)
                    {
                        cte01.IdeXMunEmi = "EXTERIOR";
                        cte01.IdeUfEmi = "EX";
                    }
                    else
                    {
                        cte01.IdeXMunEmi = cidadeEmitente.Descricao;
                        // C�digo UF do munic�pio de origem do fluxo.
                        cte01.IdeUfEmi = cidadeEmitente.SiglaEstado;
                    }
                }

                // Modal do transporte. Hoje � usado 04=Ferrovi�rio.
                cte01.IdeModal = 4;

                // Tipo de servi�o. Estava sendo usado 0. Mudamos para pgar o tipo de servi�o do CTE raiz.
                var cteAuxiliar = _cteRaiz;
                if (_cteRaiz.TipoCte == TipoCteEnum.Complementar)
                {
                    cteAuxiliar = _cteRepository.ObterCtePaiIdCteComplementar((int)_cteRaiz.Id);
                    // Tipo de servi�o.
                    cte01.IdeTpServ = cteAuxiliar == null || cteAuxiliar.TipoServico == null ? 0 : (int)cteAuxiliar.TipoServico;
                }
                else
                {
                    // Tipo de servi�o.
                    cte01.IdeTpServ = _cteRaiz == null ? 0 : _cteRaiz.ObterTipoServicoCte();
                }



                if (_cidadeIbgeEstacaoMaeOrigemFluxoComercial != null)
                {
                    // C�digo IBGE do munic�pio de origem do fluxo.
                    cte01.IdeCMunIni = _cidadeIbgeEstacaoMaeOrigemFluxoComercial.CodigoIbge;

                    if (_cidadeIbgeEstacaoMaeOrigemFluxoComercial.CodigoIbge == 9999999)
                    {
                        // Nome do munic�pio de origem do fluxo.
                        cte01.IdeXMunIni = "EXTERIOR";
                        // C�digo UF do munic�pio de origem do fluxo.
                        cte01.IdeUfIni = "EX";
                    }
                    else
                    {
                        // Nome do munic�pio de origem do fluxo.
                        cte01.IdeXMunIni = _municipioEstacaoMaeOrigemFluxoComercial.Descricao;
                        // C�digo UF do munic�pio de origem do fluxo.
                        cte01.IdeUfIni = _municipioEstacaoMaeOrigemFluxoComercial.CidadeIbge.SiglaEstado;
                    }
                }

                if (_cidadeIbgeEstacaoMaeDestinoFluxoComercial != null)
                {
                    // C�digo IBGE do munic�pio de destino do fluxo.
                    cte01.IdeCMunFim = _cidadeIbgeEstacaoMaeDestinoFluxoComercial.CodigoIbge;
                    if (_cidadeIbgeEstacaoMaeDestinoFluxoComercial.CodigoIbge == 9999999)
                    {
                        // Nome do munic�pio de destino do fluxo.
                        cte01.IdeXMunFim = "EXTERIOR";

                        // C�digo UF do munic�pio de destino do fluxo.
                        cte01.IdeUfFim = "EX";
                    }
                    else
                    {
                        // Nome do munic�pio de destino do fluxo.
                        cte01.IdeXMunFim = _municipioEstacaoMaeDestinoFluxoComercial.Descricao;

                        // C�digo UF do munic�pio de destino do fluxo.
                        cte01.IdeUfFim = _municipioEstacaoMaeDestinoFluxoComercial.CidadeIbge.SiglaEstado;
                    }
                }

                // Informa��es da retirada da carga. Hoje fixo 0.
                cte01.IdeRetira = 0;

                // Detalhes do retira
                cte01.IdeXDetRetira = string.Empty;

                /*
                Regra do frete 0-Pago e 1-A Pagar e 2-Outros
                Frete Pago: Quando o tomador de servico (FluxoComercial.EmpresaPagadora) igual ao Remetente (FluxoComercial.EmpresaRemetente)
                Frete a pagar: Quando o tomador de servico(FluxoComercial.EmpresaPagadora) igual ao Destinatario (FluxoComercial.EmpresaDestinataria)
                Caso o tomador seja diferente tanto do Remetente como do Destinatario o frete � Outros (03/05/2012)
                Obs: Leva 
                 */

                if (!_cteComplementar.Versao.CodigoVersao.Equals("3.00")) //// a tag IdeForPag se tornou obsoleta na vers�o 3.0
                {
                    if (_empresaTomadora.Id == _empresaRemetenteFluxoComercial.Id)
                    {
                        // Forma de pagamento. Quando o tomador de servi�o � Remetente, a Forma de Pagamento � Pago(0).
                        cte01.IdeForPag = "0";
                    }
                    else if (_empresaTomadora.Id == _empresaDestinatariaFluxoComercial.Id)
                    {
                        // Forma de pagamento. Quando o tomador de servi�o n�o � o Remetente, a Forma de Pagamento � A Pagar(1).
                        cte01.IdeForPag = "1";
                    }
                    else
                    {
                        // Forma de pagamento. Quando o tomador de servi�o � Remetente, a Forma de Pagamento � Pago(0).
                        cte01.IdeForPag = "2";
                    }
                }

                // if ((_cte.Id == _cteRaizAgrupamento.Id) && (_tipoTrafegoMutuo == TipoTrafegoMutuoEnum.FaturamentoEmpresaGrupo) && (! _cteService.VerificarLiberacaoTravasCorrentista(_fluxoComercial)))
                // 
                // {
                // Primeiro CTE do agrupamento e o tipo do trafego � FaturamentoEmpresaGrupo
                // Dados do tomador do servi�o. 0 - Remetente
                //	cte01.Toma03Toma = 0;
                // }
                // else
                // {
                // Nesse caso dever� ser preenchido os dados do Toma04 com os dados da ferrovia de origem
                // por ser um CTE filho ou o tipo de trafego � IntercambioFaturamentoOrigem ou IntercambioFaturamentoDestino


                // Pega a informa��o do tomador aqui.
                var toma = GetUtilizaToma();
                var utilizaToma3 = (toma >= 0);

                if (utilizaToma3) cte01.Toma03Toma = toma;

                ////if (!utilizaToma3 || (_carregamentoService.VerificarFluxoConteiner(_fluxoComercial) || VerificaEmpresaBrado(_contrato.EmpresaPagadora)))

                if (!utilizaToma3)
                {
                    cte01.Toma03Toma = null;
                    cte01.Toma4Toma = 4;
                    long cnpj = 0;

                    // Armazena qual a empresa tomadora.
                    _cteEmpresas.EmpresaTomadora = _empresaTomadora;

                    if (_empresaTomadora.TipoPessoa == TipoPessoaEnum.Juridica)
                    {
                        // CNPJ do tomador de servi�o. Se for pessoa jur�dica
                        if (Int64.TryParse(_empresaTomadora.Cgc, out cnpj))
                        {
                            cte01.Toma4Cnpj = cnpj;
                        }
                    }
                    else
                    {
                        // CPF do tomador de servi�o. Se for pessoa f�sica.
                        if (Int64.TryParse(_empresaTomadora.Cpf, out cnpj))
                        {
                            cte01.Toma4Cpf = cnpj;
                        }
                    }

                    // Inscri��o estadual do tomador. Campo opcional. -- descomentado em 21/11/18 devido que deve ir a info do Toma4 para a config
                    cte01.Toma4Ie = _cteComplementar.ObterTipoTomador() != 9 ? Tools.TruncateRemoveSpecialChar(_empresaTomadora.InscricaoEstadual, 14) : null;
                    Tools.TruncateRemoveSpecialChar(_empresaTomadora.InscricaoEstadual, 14);

                    if (!string.IsNullOrEmpty(_empresaTomadora.RazaoSocial))
                    {
                        // Raz�o social da empresa. 
                        cte01.Toma4xNome = _empresaTomadora.RazaoSocial.Trim();
                    }

                    // Nome fantasia da empresa
                    cte01.Toma4xFant = _empresaTomadora.NomeFantasia;

                    // Telefone da empresa
                    cte01.Toma4Fone = ValidarTelefone(_empresaTomadora.Telefone1);
                    if (string.IsNullOrEmpty(cte01.Toma4Fone))
                    {
                        cte01.Toma4Fone = null;
                    }
                }

                // Armazena qual o tomador utilizado
                codigoTomador = cte01.Toma03Toma.HasValue ? cte01.Toma03Toma.Value : cte01.Toma4Toma.Value;

                // Logradouro do tomador do servi�o
                cte01.EnderTomaxLgr = _empresaTomadora.Endereco.Trim();

                // N�mero.
                // cte01.EnderTomaNro = _empresaFerrovia.Numero;
                cte01.EnderTomaNro = "S/N";

                // Complemento do endere�o.
                cte01.EnderTomaXCpl = string.Empty;

                // Bairro do endere�o do tomador
                cte01.EnderTomaXBairro = "NAO INFORMADO";

                if (_cidadeIbgeEmpresaTomadora != null)
                {
                    // C�digo IBGE do munic�pio do tomador
                    cte01.EnderTomaCMun = _cidadeIbgeEmpresaTomadora.CodigoIbge;

                    if (_cidadeIbgeEmpresaTomadora.CodigoIbge == 9999999)
                    {
                        // Nome do munic�pio do tomador
                        cte01.EnderTomaXMun = "EXTERIOR";
                        cte01.EnderTomaUf = "EX";
                    }
                    else
                    {
                        // Nome do munic�pio do tomador
                        cte01.EnderTomaXMun = _cidadeIbgeEmpresaTomadora.Descricao;

                        // C�digo UF.
                        cte01.EnderTomaUf = _cidadeIbgeEmpresaTomadora.SiglaEstado;
                    }
                }

                try
                {
                    // CEP.( remove '-' caso tiver e converte para numerico
                    int index = _empresaTomadora.Cep.IndexOf('-');
                    cte01.EnderTomaCep = index > -1 ? int.Parse(_empresaTomadora.Cep.Remove(index, 1)) : int.Parse(_empresaTomadora.Cep);
                    if (cte01.EnderTomaCep > 99999999)
                    {
                        // Garante que o tamanho maximo do cep em 8 casas
                        cte01.EnderTomaCep = null;
                    }
                }
                catch (Exception)
                {
                    cte01.EnderTomaCep = null;
                }

                // Recupera o PaisBacen
                PaisBacen paisBacen = null;

                if (_cidadeIbgeEmpresaTomadora.CodigoIbge == 9999999)
                {
                    string siglaPaisAux = ObterSiglaPaisExteriorPeloSap(_empresaTomadora);
                    paisBacen = _paisBacenRepository.ObterPorSiglaResumida(siglaPaisAux);
                    cte01.Toma4Cnpj = 0;
                }
                else
                {
                    paisBacen = _paisBacenRepository.ObterPorSiglaResumida(_cidadeIbgeEmpresaTomadora.SiglaPais);
                }

                // C�digo BACEN do pa�s.
                if (int.TryParse(paisBacen.CodigoBacen.Substring(1, 4), out intAux))
                {
                    cte01.EnderTomaCPais = intAux;
                }

                // Nome do pa�s.
                cte01.EnderTomaXPais = paisBacen.Nome;
                // }

                // Texto livre. O SAP utiliza �ENTREGA�.
                cte01.ComplXCaracAd = "ENTREGA";

                // Texto livre para caracter�stica adicional do servi�o.
                cte01.ComplXCaracSer = "LOGISTICA";

                // Funcion�rio emissor do CTe
                /*CteStatus cteStatus = _cte.ListaStatus.Where(g => g.CteStatusRetorno.Id == 10).FirstOrDefault();
                cte01.ComplXEmi = cteStatus != null ? cteStatus.Usuario.Codigo : string.Empty;
                 */
                cte01.ComplXEmi = string.Empty;

                // Sigla ou c�digo interno da Filial/Porto/Esta��o/ Aeroporto de Origem
                cte01.FluxoXOrig = "ESTACAO";

                // Sigla ou c�digo interno da Filial/Porto/Esta��o/ Aeroporto de Destin
                cte01.FluxoXDest = "ESTACAO";

                // C�digo da Rota de EntregA
                cte01.FluxoXRota = string.Empty;

                // Tipo de data/per�odo programado para a entrega
                cte01.SemDataTpPer = string.Empty;

                // Tipo de data/per�odo programado para a entrega
                cte01.ComDataTpPer = string.Empty;

                // Data programada
                cte01.ComDataDProg = null;

                // Tipo de data/per�odo programado para a entrega
                cte01.NoPeriodoTpPer = string.Empty;

                // Data inicial
                cte01.NoPeriodoDIni = null;

                // Data final
                cte01.NoPeriodoDFim = null;

                // Tipo de hora/per�odo programado para a entrega
                cte01.SemHoraTpHor = string.Empty;

                // Tipo de data/per�odo programado para a entrega
                cte01.ComHoraTpHor = string.Empty;

                // Hora programada (HH:MM:SS)
                cte01.ComHoraHProg = string.Empty;

                // Tipo de data/per�odo programado para a entrega
                cte01.NoInterTpHor = string.Empty;

                // Hora inicial (HH:MM:SS)
                cte01.NoInterHIni = string.Empty;

                // Hora Final (HH:MM:SS)
                cte01.NoInterHFim = string.Empty;

                // munic�pio origem para efeito de c�lculo do frete
                cte01.ComplOrigCalc = cidadeIbgeCalculoFrete.Descricao ?? string.Empty;

                // munic�pio destino para efeito de c�lculo do frete
                cte01.ComplDestCalc = cidadeIbgeCalculoFrete.Descricao ?? string.Empty;

                string obs = ObterMensagemCteComplementado();

                if (_cteComplementar.FluxoComercial.Contrato.MensagemFiscal != null)
                {
                    obs += " ??";
                    obs += _cteComplementar.FluxoComercial.Contrato.MensagemFiscal.Trim();
                }

                /* inclupido a msg da situa��o tribut�ria */
                if (_cteComplementar.SiglaUfFerrovia == "MS")
                {
                    string obsms = CalcularObservacaoMS();
                    // substitui��o tribut�ria
                    if (!string.IsNullOrEmpty(obsms))
                    {
                        obs += obsms;
                    }
                }

                obs += ObterMensagemDataDescarga();

                _cteComplementar.Observacao = obs;
                cte01.ComplxObs = obs;
                // N�mero da fatura
                cte01.FatNFat = string.Empty;

                // Valor original da fatura
                cte01.FatVOrig = null;

                // Valor do desconto da fatura
                cte01.FatVDesc = null;

                // Valor l�quido da fatura
                cte01.FatVLiq = null;

                // Indicador do papel do tomador na presta��o de servi�o - pega do contrato do fluxo do cte.
                if (_cteComplementar.Versao.CodigoVersao.Equals("3.00")) //// a tag IdeIndIeToma s� mostra na vers�o 3.0
                {
                    cte01.IdeIndIeToma = _cteComplementar.FluxoComercial.Contrato.IndIeToma;
                }

                // Valida��o das regular expression
                InvalidValue[] ret = _validatorEngine.Validate(cte01);

                // Efetua o tratamento do retorno da valida��o das regular expression
                TratarRetornoRegexSefaz(ret);

                // Inserir
                _cte01Repository.InserirOuAtualizar(cte01);
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(_cteComplementar, "GravarConfigCteEnvioService", string.Format("GravarDadosPrincipaisCte: {0}", ex.Message), ex);
                throw;
            }
        }

        /// <summary>
        /// Calcula a Subistitui��o Tribut�ria para os Ctes do Mato Grosso do Sul
        /// </summary>
        /// <returns>Retorna uma string com os valores da substitui��o</returns>
        private string CalcularObservacaoMS()
        {
            string retorno = string.Empty;

            double aliquotaSt = _cteComplementar.FluxoComercial.Contrato.AliquotaIcmsSubstProduto ?? 0.0;
            if (aliquotaSt > 0.0)
            {
                double baseCalculoIcms = _cteComplementar.BaseCalculoIcms ?? 0.0;
                double valorImpostoRetido = Math.Round(baseCalculoIcms * (aliquotaSt / 100), 2);
                valorImpostoRetido *= 0.8;
                double desconto = _cteComplementar.DescontoPorPeso ?? 0.0;
                desconto += _cteComplementar.DescontoPorPercentual ?? 0.0;
                if (desconto < 0)
                {
                    desconto *= -1;
                }

                desconto = Math.Round(desconto, 2);
                double valorLiquido = Math.Round(baseCalculoIcms - valorImpostoRetido - desconto, 2);

                retorno = string.Format(new CultureInfo("pt-BR"), "??BASE DE CALCULO {0:C2} / VALOR DO IMPOSTO RETIDO {1:C2} / DESCONTO {2:C2} / VALOR LIQUIDO A PAGAR {3:C2}", baseCalculoIcms, valorImpostoRetido, desconto, valorLiquido);
                retorno += " ??";
                retorno += string.Format("O ICMS  ser� recolhido pelo remetente da mercadoria.");
            }

            return retorno;
        }

        /// <summary>
        /// Tabela de informacao do emitente do CT-e (1-1)
        /// </summary>
        private void GravarInformacoesEmitente()
        {
            try
            {
                IEmpresa empresaRemetente;
                CidadeIbge cidadeIbgeRemetente;

                var _cte = _listaCteComplementado[0].Cte;
                var _empresaPagadoraContrato = _cte.ContratoHistorico.EmpresaPagadora;
                var _cidadeIbgeEmpresaPagadoraContrato = _empresaPagadoraContrato.CidadeIbge;

                if ((_cteComplementar.Id == _cteRaizAgrupamento.Id) && (_tipoTrafegoMutuo == TipoTrafegoMutuoEnum.FaturamentoEmpresaGrupo))
                {
                    // TipoTrafegoMutuoEnum.FaturamentoEmpresaGrupo
                    //empresaRemetente = _cteComplementar.FluxoComercial.Contrato.EmpresaRemetenteFiscal;
                    //cidadeIbgeRemetente = _cteComplementar.FluxoComercial.Contrato.EmpresaRemetenteFiscal.CidadeIbge;

                    empresaRemetente = _empresaContratoRemetenteFiscal;
                    cidadeIbgeRemetente = _cidadeIbgeRemetenteFiscal;
                }
                else
                {
                    // Tipo CTE filho ou Trafego Mutuo IntercambioFaturamentoOrigem ou IntercambioFaturamentoDestino
                    empresaRemetente = _empresaFerroviaCliente;
                    cidadeIbgeRemetente = _cidadeIbgeEmpresaFerrovia;
                }

                // Armazena qual a empresa remetente utilizada.
                _cteEmpresas.EmpresaRemetente = empresaRemetente;

                Cte05 cte05 = new Cte05();
                int index, numConvertido;
                long cnpj;

                // Codigo interno da filial (PK)
                cte05.CfgUn = ObterIdentificadorFilial();

                // Numero do documento CT-e (PK)
                int.TryParse(_cteComplementar.Numero, out numConvertido);
                cte05.CfgDoc = numConvertido;

                // Serie do documento CT-e (PK)
                // cte05.CfgSerie = _cte.Serie;
                cte05.CfgSerie = int.Parse(_cteComplementar.Serie).ToString();

                // CNPJ
                cte05.EmitCnpj = _nfe03Filial.Cnpj;

                // Inscricao estadual   
                cte05.EmitIe = Tools.TruncateRemoveSpecialChar(_nfe03Filial.Ie, 14);

                // Nome
                cte05.EmitXnome = _nfe03Filial.RazSoc.Trim();

                // Nome Fantasia
                cte05.EmitXfant = _nfe03Filial.NomeReduzido;

                // Logradouro
                cte05.EnderEmitXlgr = _nfe03Filial.Endereco.Trim();

                // N�mero
                // cte05.EnderEmitNro = string.Empty;
                cte05.EnderEmitNro = _nfe03Filial.NrLogr.ToString();

                // Complemento do endere�o
                cte05.EnderEmitXcpl = _nfe03Filial.Compl;

                // Bairro do endere�o
                cte05.EnderEmitXbairro = _nfe03Filial.Bairro;

                CidadeIbge cidadeEmitente = _cidadeIbgeRepository.ObterPorDescricaoUf(_nfe03Filial.Cidade.ToUpper().Trim(), _cteComplementar.SiglaUfFerrovia);
                if (cidadeEmitente != null)
                {
                    // C�digo IBGE do munic�pio
                    cte05.EnderEmitCmun = cidadeEmitente.CodigoIbge;

                    // Nome do munic�pio 
                    cte05.EnderEmitXmun = cidadeEmitente.Descricao;

                    // Recupera o PaisBacen
                    PaisBacen paisBacenEmit = _paisBacenRepository.ObterPorSiglaResumida(cidadeEmitente.SiglaPais);

                    // C�digo BACEN do pa�s 
                    cte05.EnderEmitCpais = int.Parse(paisBacenEmit != null ? paisBacenEmit.CodigoBacen.Substring(1, 4) : string.Empty);

                    // Nome do pa�s. 
                    cte05.EnderEmitXpais = paisBacenEmit != null ? paisBacenEmit.Nome : string.Empty;
                }

                // Cep 
                cte05.EnderEmitCep = _nfe03Filial.Cep;

                // C�digo UF 
                cte05.EnderEmitUf = _cteComplementar.SiglaUfFerrovia;

                //// Telefone
                //if (_nfe03Filial.Telefone != null)
                //{
                //    cte05.EnderEmitFone = _nfe03Filial.Telefone.ToString();
                //}

                // Dados do remetente
                if (empresaRemetente.TipoPessoa == TipoPessoaEnum.Juridica)
                {
                    // CNPJ
                    if (Int64.TryParse(empresaRemetente.Cgc, out cnpj))
                    {
                        cte05.RemCnpj = cnpj;
                    }
                }
                else
                {
                    // CPF
                    if (Int64.TryParse(empresaRemetente.Cgc, out cnpj))
                    {
                        cte05.RemCpf = cnpj;
                    }
                    else
                    {
                        if (Int64.TryParse(empresaRemetente.Cpf, out cnpj))
                        {
                            cte05.RemCpf = cnpj;
                        }
                    }
                }

                // Inscricao estadual
                cte05.RemIe = Tools.TruncateRemoveSpecialChar(empresaRemetente.InscricaoEstadual, 14);

                // Nome
                cte05.RemXnome = empresaRemetente.RazaoSocial.Trim();

                // Nome Fantasia
                cte05.RemXfant = empresaRemetente.NomeFantasia;

                //// Telefone
                //cte05.EnderEmitFone = Tools.TruncateRemoveSpecialChar(empresaRemetente.Telefone1, 12, 7);

                // Telefone
                if (_nfe03Filial.Telefone != null)
                {
                    var ddd = _nfe03Filial.Ddd.HasValue ? _nfe03Filial.Ddd.ToString() : string.Empty;
                    cte05.EnderEmitFone = string.Concat(ddd, _nfe03Filial.Telefone.ToString());
                }

                // Logradouro
                cte05.EnderRemeXlgr = empresaRemetente.Endereco.Trim();

                // N�mero
                // cte05.EnderRemeNro = string.Empty;
                cte05.EnderRemeNro = "SN";

                // Complemento do endere�o
                cte05.EnderRemeXcpl = string.Empty;

                // Bairro do endere�o
                cte05.EnderRemeXbairro = "NAO INFORMADO";

                if (cidadeIbgeRemetente != null)
                {
                    // C�digo IBGE do munic�pio
                    cte05.EnderRemeCmun = cidadeIbgeRemetente.CodigoIbge;

                    // Nome do munic�pio 
                    cte05.EnderRemeXmun = cidadeIbgeRemetente.Descricao;

                    // C�digo UF 
                    cte05.EnderRemeUf = cidadeIbgeRemetente.SiglaEstado;

                    // Recupera o PaisBacen
                    PaisBacen paisBacenReme = _paisBacenRepository.ObterPorSiglaResumida(cidadeIbgeRemetente.SiglaPais);

                    // C�digo BACEN do pa�s 
                    cte05.EnderRemeCpais = paisBacenReme != null ? int.Parse(paisBacenReme.CodigoBacen.Substring(1, 4)) : default(int?);

                    // Nome do pa�s. 
                    cte05.EnderRemeXpais = paisBacenReme != null ? paisBacenReme.Nome : string.Empty;
                }

                // Cep 
                index = empresaRemetente.Cep.IndexOf('-');
                cte05.EnderRemeCep = index > 0 ? int.Parse(empresaRemetente.Cep.Remove(index, 1)) : int.Parse(empresaRemetente.Cep);

                if (!string.IsNullOrEmpty(_empresaPagadoraContrato.RazaoSocial))
                {
                    // Nome local de retirada
                    cte05.LocColetaxNome = _empresaPagadoraContrato.RazaoSocial.Trim();
                }

                if (!string.IsNullOrEmpty(_empresaPagadoraContrato.Endereco))
                {
                    // Logradouro do local de retirada
                    cte05.LocColetaxLgr = _empresaPagadoraContrato.Endereco.Trim();
                }

                // N�mero
                // cte06.LocRetNro = string.Empty;
                cte05.LocColetaNro = "0";

                // Complemento do complemento
                cte05.LocColetaxCpl = string.Empty;

                // Bairro do local de retirada 
                cte05.LocColetaxBairro = "NAO INFORMADO";

                // C�digo IBGE do munic�pio do local de retirada
                cte05.LocColetacMun = _cidadeIbgeEmpresaPagadoraContrato.CodigoIbge;
                if (_cidadeIbgeEmpresaPagadoraContrato.CodigoIbge == 9999999)
                {
                    // Nome do munic�pio do local de retirada
                    cte05.LocColetaxMun = "EXTERIOR";
                    // C�digo UF do local de retirada
                    cte05.LocColetaUf = "EX";
                }
                else
                {
                    // Nome do munic�pio do local de retirada
                    cte05.LocColetaxMun = _cidadeIbgeEmpresaPagadoraContrato.Descricao;
                    // C�digo UF do local de retirada
                    cte05.LocColetaUf = _cidadeIbgeEmpresaPagadoraContrato.SiglaEstado;
                }

                // Inserir
                _cte05Repository.Inserir(cte05);
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(_cteComplementar, "GravarConfigCteEnvioService", string.Format("GravarInformacoesEmitente: {0}", ex.Message), ex);
                throw;
            }
        }

        /// <summary>
        /// Carrega as variaveis usado para o Cte Complementado
        /// </summary>
        /// <param name="cteDetalhe"> Cte Complementado</param>
        private void CarregarDadosCteComplementado(CteComplementado cteDetalhe)
        {
            if (cteDetalhe != null)
            {
                _cteCteComplementado = cteDetalhe.Cte;

                if (_cteCteComplementado.Despacho != null)
                {
                    _serieDespachoUfCteComplementado = _serieDespachoUfRepository.ObterPorCnpjUf(cteDetalhe.CteComplementar.CnpjFerrovia, cteDetalhe.CteComplementar.SiglaUfFerrovia);
                }

                _contratoHistoricoCteComplementado = _cteCteComplementado.ContratoHistorico;
            }
        }

        /// <summary>
        /// Tabela de informacoes dos documentos (NF, Nfe e Outros) (1-N) - ICTE06_EMIT_INF
        /// </summary>
        private void GravarInformacoesDocumentos()
        {
            try
            {
                int seq = 0;

                foreach (var cteComplementado in _listaCteComplementado)
                {
                    foreach (CteDetalhe cteDetalhe in cteComplementado.Cte.ListaDetalhes.ToList())
                    {
                        seq++;

                        // Chave de acesso da NFE
                        if (!string.IsNullOrEmpty(cteDetalhe.ChaveNfe))
                        {
                            GravarDocumentoNfe(cteDetalhe, seq);
                        }
                        else
                        {
                            if (cteDetalhe.SerieNota == null)
                            {
                                cteDetalhe.SerieNota = string.Empty;
                            }

                            int serie;

                            // if ((cteDetalhe.SerieNota.ToUpper() != "V2") && (cteDetalhe.SerieNota.ToUpper() != "OU"))
                            if (int.TryParse(cteDetalhe.SerieNota.ToUpper(), out serie) && !_fluxoComercial.ModeloNotaFiscal.ToUpper().Equals("99"))
                            {
                                GravarDocumentoNf(cteDetalhe, seq, cteComplementado);
                            }
                            else
                            {
                                // Outros
                                GravarDocumentosOutros(cteDetalhe, seq);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(_cteComplementar, "GravarConfigCteEnvioService", string.Format("GravarInformacoesDocumentos: {0}", ex.Message), ex);
                throw;
            }
        }

        private void GravarDocumentoNf(CteDetalhe cteDetalhe, int seq, CteComplementado cteComplementado)
        {
            Cte100 cte100 = new Cte100();
            Cte103 cte103 = new Cte103();

            int intAux;
            double baseCalculoIcms = 0.0;
            double valorTotalIcms = 0.0;
            double baseCalculoIcmsSt = 0.0;
            double aliquotaSt = 0.0;
            double valorTotalIcmsSt = 0.0;
            Cte _cte;

            _cte = cteComplementado.Cte;

            // Recupera os dados para calculo do ICMS
            baseCalculoIcms = _cte.BaseCalculoIcms ?? 0.0;
            valorTotalIcms = _cte.ValorIcms ?? 0.0;

            baseCalculoIcmsSt = _cte.ContratoHistorico.ValorBaseCalculoSubstituicaoProduto ?? 0.0;
            valorTotalIcmsSt = baseCalculoIcmsSt * aliquotaSt;

            // Numero do documento CT-e (PK)
            int.TryParse(cteComplementado.CteComplementar.Numero, out intAux);
            cte100.CfgDoc = intAux;
            // S�rie
            cte100.CfgSerie = int.Parse(_cte.Serie).ToString();
            // Unidade
            cte100.CfgUn = ObterIdentificadorFilial();

            cte100.IdIcnInfNf = seq;

            // N�mero do romaneio
            cte100.InfNfNroma = null;

            // N�mero do pedido da NF
            cte100.InfNfNped = null;

            // S�rie da nota
            cte100.InfNfSerie = Tools.TruncateRemoveSpecialChar(cteDetalhe.SerieNota, 3);

            // N�mero da nota
            cte100.InfNfNdoc = cteDetalhe.NumeroNota.Trim();

            // Data da emiss�o da nota
            cte100.InfNfDemi = cteDetalhe.DataNotaFiscal;

            // Valor da base para c�lculo do ICMS
            cte100.InfNfVbc = baseCalculoIcms;

            // Valor do ICMS
            cte100.InfNfVicms = valorTotalIcms;

            // Valor da base de c�lculo de substitui��o
            cte100.InfNfVbcst = baseCalculoIcmsSt;

            // Valor total do ICMS de substitui��o calculado como vBCST * ALIQICMSST da tabela CONTRATO.
            cte100.InfNfVst = valorTotalIcmsSt;

            // Valor total dos produtos
            cte100.InfNfVprod = cteDetalhe.ValorNotaFiscal;

            // Valor total da nota
            cte100.InfNfVnf = CalcularValorTotalNotaFiscal();

            // CFOP predominante
            cte100.InfNfNcfop = null;

            if (_cte.ContratoHistorico.CfopProduto != null)
            {
                if (int.TryParse(_cte.ContratoHistorico.CfopProduto, out intAux))
                {
                    cte100.InfNfNcfop = intAux;
                }
            }
            else
            {
                if (int.TryParse(_cte.ContratoHistorico.Cfop, out intAux))
                {
                    cte100.InfNfNcfop = intAux;
                }
            }

            // Peso
            cte100.InfNfNpeso = cteDetalhe.PesoNotaFiscal;
            cte100.InfNfMod = 1;

            // Modelo da Nota Fiscal
            if (int.TryParse(_fluxoComercial.ModeloNotaFiscal, out intAux))
            {
                cte100.InfNfMod = intAux;
            }

            cte103.IdIcnInf = seq;
            cte103.IdInfUnidTransp = 1;

            PreencherDadosTransporte(cteDetalhe, cte103);

            // Inseri o registro
            _cte100Repository.Inserir(cte100);
            _cte103Repository.Inserir(cte103);

            if (!string.IsNullOrEmpty(cteDetalhe.ConteinerNotaFiscal))
            {
                Cte105 cte105 = new Cte105();
                cte105.IdIcnInf = seq;
                cte105.IdInfUnidTransp = 1;
                PreencherDadosCarga(cteDetalhe, cte105);

                _cte105Repository.Inserir(cte105);
            }
        }

        private void GravarDocumentoNfe(CteDetalhe cteDetalhe, int seq)
        {
            Cte101 cte101 = new Cte101();
            Cte107 cte107 = new Cte107();
            int intAux;

            // Numero do documento CT-e (PK)
            int.TryParse(_cteComplementar.Numero, out intAux);
            cte101.CfgDoc = intAux;
            // S�rie
            cte101.CfgSerie = int.Parse(_cteComplementar.Serie).ToString();
            // Unidade
            cte101.CfgUn = ObterIdentificadorFilial();

            cte101.IdIcnInfNfe = seq;

            cte101.InfNfeChave = cteDetalhe.ChaveNfe;

            // PIN atribu�do pela SUFRAMA para a opera��o
            cte101.InfNfePin = null;

            cte107.IdIcnInf = seq;
            cte107.IdInfUnidTransp = 1;

            PreencherDadosTransporte(cteDetalhe, cte107);

            // Inseri o registro
            _cte101Repository.Inserir(cte101);
            _cte107Repository.Inserir(cte107);

            if (!string.IsNullOrEmpty(cteDetalhe.ConteinerNotaFiscal))
            {
                Cte109 cte109 = new Cte109();
                cte109.IdIcnInf = seq;
                cte109.IdInfUnidTransp = 1;
                PreencherDadosCarga(cteDetalhe, cte109);

                _cte109Repository.Inserir(cte109);
            }
        }

        private void GravarDocumentosOutros(CteDetalhe cteDetalhe, int seq)
        {
            Cte102 cte102 = new Cte102();
            Cte111 cte111 = new Cte111();

            int intAux;

            // Numero do documento CT-e (PK)
            int.TryParse(_cteComplementar.Numero, out intAux);
            cte102.CfgDoc = intAux;
            // S�rie
            cte102.CfgSerie = int.Parse(_cteComplementar.Serie).ToString();
            // Unidade
            cte102.CfgUn = ObterIdentificadorFilial();

            cte102.IdIcnInfOutros = seq;

            // Utilizado nos casos da S�rie ser V2 ou OU
            cte102.InfOutrosTpDoc = 99;

            // Descri��o do documento
            cte102.InfOutrosDescOutros = "OUTROS";

            // N�mero do documento
            cte102.InfOutrosNdoc = cteDetalhe.NumeroNota.Trim();

            // Data da emiss�o do documento
            cte102.InfOutrosDemi = cteDetalhe.DataNotaFiscal;

            // Valor do documento
            cte102.InfOutrosVdocFisc = cteDetalhe.ValorNotaFiscal;

            cte111.IdIcnInf = seq;
            cte111.IdInfUnidTransp = 1;

            PreencherDadosTransporte(cteDetalhe, cte111);

            // Inseri o registro
            _cte102Repository.Inserir(cte102);
            _cte111Repository.Inserir(cte111);

            if (!string.IsNullOrEmpty(cteDetalhe.ConteinerNotaFiscal))
            {
                Cte113 cte113 = new Cte113();
                cte113.IdIcnInf = seq;
                cte113.IdInfUnidTransp = 1;
                PreencherDadosCarga(cteDetalhe, cte113);

                _cte113Repository.Inserir(cte113);
            }
        }

        private void PreencherDadosCarga(CteDetalhe cteDetalhe, IInfUnidCarga unidCarga)
        {
            int intAux;
            int.TryParse(_cteComplementar.Numero, out intAux);
            unidCarga.CfgDoc = intAux;
            // S�rie
            unidCarga.CfgSerie = int.Parse(_cteComplementar.Serie).ToString();
            // Unidade
            unidCarga.CfgUn = ObterIdentificadorFilial();

            unidCarga.IdInfUnidCarga = 1;

            unidCarga.IdUnidCarga = cteDetalhe.ConteinerNotaFiscal;
            /*
                1 - Container
                2 - ULD
                3 - Pallet
                4 - Outros
             */
            unidCarga.TpUnidCarga = 1;
            unidCarga.QtdRat = cteDetalhe.PesoNotaFiscal;
        }

        private void PreencherDadosTransporte(CteDetalhe cteDetalhe, IInfUnidTransp unidTransp)
        {
            int intAux;
            int.TryParse(_cteComplementar.Numero, out intAux);
            unidTransp.CfgDoc = intAux;
            // S�rie
            unidTransp.CfgSerie = int.Parse(_cteComplementar.Serie).ToString();
            // Unidade
            unidTransp.CfgUn = ObterIdentificadorFilial();

            unidTransp.IdInfUnidTransp = 1;

            // 1 - Rodovi�rio Tra��o 2 - Rodovi�rio Reboque 3 - Navio 4 - Balsa 5 - Aeronave 6 - Vag�o 7 - Outros
            unidTransp.TpUnidTransp = 6;
            unidTransp.IdUnidTransp = int.Parse(cteDetalhe.Cte.Vagao.Codigo);
            unidTransp.QtdRat = cteDetalhe.PesoNotaFiscal;
        }

        /// <summary>
        /// Tabela de informacoes do Expedidor / Recebedor / Destinatario (1-1) - ICTE07_EXPED
        /// </summary>
        private void GravarInformacoesExpedidorRecebedorDestinatario()
        {
            IEmpresa empresaExpedidor = _empresaRemetenteFluxoComercial;
            CidadeIbge cidadeIbgeExpedidor = _empresaRemetenteFluxoComercial.CidadeIbge;
            IEmpresa empresaRecebedor = _empresaDestinatariaFluxoComercial;
            CidadeIbge cidadeIbgeRecebedor = _empresaDestinatariaFluxoComercial.CidadeIbge;

            //IEmpresa empresaDestinataria = _cteComplementar.FluxoComercial.Contrato.EmpresaDestinatariaFiscal;
            IEmpresa empresaDestinataria = _empresaContratoDestinatariaFiscal;

            //CidadeIbge cidadeIbgeDestinataria = _cteComplementar.FluxoComercial.Contrato.EmpresaDestinatariaFiscal.CidadeIbge;
            CidadeIbge cidadeIbgeDestinataria = _cidadeIbgeDestinatariaFiscal;

            IEmpresa _empresaRemetenteContrato = _cteComplementar.FluxoComercial.Contrato.EmpresaRemetente;
            try
            {
                Cte07 cte07 = new Cte07();
                int numConvertido;
                int index;
                long cnpj = 0;

                // Armazena as empresas utilizadas
                _cteEmpresas.EmpresaExpedidor = empresaExpedidor;
                _cteEmpresas.EmpresaRecebedor = empresaRecebedor;
                _cteEmpresas.EmpresaDestinatario = empresaDestinataria;

                // Codigo interno da filial (PK)
                cte07.CfgUn = ObterIdentificadorFilial();

                // Numero do documento CT-e (PK)
                int.TryParse(_cteComplementar.Numero, out numConvertido);
                cte07.CfgDoc = numConvertido;

                // Serie do documento CT-e (PK)
                // cte07.CfgSerie = _cte.Serie;
                cte07.CfgSerie = int.Parse(_cteComplementar.Serie).ToString();

                // DADOS DO EXPEDIDOR

                if (empresaExpedidor.TipoPessoa == TipoPessoaEnum.Juridica)
                {
                    // CNPJ do expedidor
                    if (Int64.TryParse(empresaExpedidor.Cgc, out cnpj))
                    {
                        cte07.ExpedCnpj = cnpj;
                    }
                }
                else
                {
                    // CNPJ do expedidor
                    if (Int64.TryParse(empresaExpedidor.Cpf, out cnpj))
                    {
                        cte07.ExpedCpf = cnpj;
                    }
                }

                // Inscricao Estadual do expedidor
                cte07.ExpedIe = Tools.TruncateRemoveSpecialChar(empresaExpedidor.InscricaoEstadual, 14);

                // Nome do expedidor
                cte07.ExpedXNome = empresaExpedidor.RazaoSocial.Trim();

                // Telefone do expedidor
                cte07.ExpedFone = Tools.TruncateRemoveSpecialChar(empresaExpedidor.Telefone1, 12, 7);

                // Logradouro do tomador do servi�o
                cte07.EnderExpedXlgr = empresaExpedidor.Endereco.Trim();

                // N�mero
                // cte07.EnderExpedNro = string.Empty;
                cte07.EnderExpedNro = "0";

                // Complemento do endere�o
                cte07.EnderExpedXcpl = string.Empty;

                // Bairro do endere�o do tomador
                cte07.EnderExpedXBairro = "NAO INFORMADO";

                // C�digo IBGE do munic�pio do tomador
                cte07.EnderExpedCMun = cidadeIbgeExpedidor.CodigoIbge;

                if (cidadeIbgeExpedidor.CodigoIbge == 9999999)
                {
                    // Nome do munic�pio do tomador
                    cte07.EnderExpedXMun = "EXTERIOR";
                    cte07.EnderExpedUf = "EX";
                }
                else
                {
                    // Nome do munic�pio do 
                    cte07.EnderExpedXMun = cidadeIbgeExpedidor.Descricao;
                    // C�digo UF do expedidor
                    cte07.EnderExpedUf = cidadeIbgeExpedidor.SiglaEstado;
                }

                try
                {
                    // Cep do expedidor
                    index = _empresaRemetenteContrato.Cep.IndexOf('-');
                    cte07.EnderExpedCep = index > 0 ? int.Parse(empresaExpedidor.Cep.Remove(index, 1)) : int.Parse(empresaExpedidor.Cep);
                }
                catch (Exception)
                {
                    cte07.EnderExpedCep = null;
                }

                // Recupera o PaisBacen
                PaisBacen paisBacen = null;
                if (cidadeIbgeExpedidor.CodigoIbge == 9999999)
                {
                    string siglaPaisAux = ObterSiglaPaisExteriorPeloSap(empresaExpedidor);
                    paisBacen = _paisBacenRepository.ObterPorSiglaResumida(siglaPaisAux);
                    cte07.DestCnpj = 0;
                }
                else
                {
                    paisBacen = _paisBacenRepository.ObterPorSiglaResumida(cidadeIbgeExpedidor.SiglaPais);
                }

                // C�digo BACEN do pa�s do expedidor
                cte07.EnderExpedCpais = int.Parse(paisBacen.CodigoBacen.Substring(1, 4));

                // Nome do pa�s. 
                cte07.EnderExpedXPais = paisBacen.Nome;

                // DADOS DO RECEBEDOR

                if (empresaRecebedor.TipoPessoa == TipoPessoaEnum.Juridica)
                {
                    // CNPJ do recebedor de servi�o
                    if (Int64.TryParse(empresaRecebedor.Cgc, out cnpj))
                    {
                        cte07.RecebCnpj = cnpj;
                    }
                }
                else
                {
                    // CNPJ do recebedor de servi�o
                    if (Int64.TryParse(empresaRecebedor.Cpf, out cnpj))
                    {
                        cte07.RecebCpf = cnpj;
                    }
                }

                // Inscri��o estadual 
                cte07.RecebIe = Tools.TruncateRemoveSpecialChar(empresaRecebedor.InscricaoEstadual, 14);

                // Raz�o social da empresa
                cte07.RecebXNome = empresaRecebedor.RazaoSocial.Trim();

                // Telefone da empresa
                cte07.RecebFone = Tools.TruncateRemoveSpecialChar(empresaRecebedor.Telefone1, 12, 7);

                // Logradouro do recebedor do servi�o
                cte07.EnderRecebXlgr = empresaRecebedor.Endereco.Trim();

                // N�mero
                // cte07.EnderRecebNro = string.Empty;
                cte07.EnderRecebNro = "0";

                // Complemento do endere�o
                cte07.EnderRecebXcpl = string.Empty;

                // Bairro do endere�o do recebedor
                cte07.EnderRecebXbairro = "NAO INFORMADO";

                // C�digo IBGE do munic�pio do tomador
                cte07.EnderRecebCMun = cidadeIbgeRecebedor.CodigoIbge;

                if (cidadeIbgeRecebedor.CodigoIbge == 9999999)
                {
                    // Nome do munic�pio do tomador
                    cte07.EnderRecebXMun = "EXTERIOR";
                    cte07.EnderRecebUf = "EX";
                }
                else
                {
                    // Nome do munic�pio do tomador
                    cte07.EnderRecebXMun = cidadeIbgeRecebedor.Descricao;
                    // C�digo UF do recebedor
                    cte07.EnderRecebUf = cidadeIbgeRecebedor.SiglaEstado;
                }

                try
                {
                    // Cep do expedidor
                    index = empresaRecebedor.Cep.IndexOf('-');
                    cte07.EnderRecebCep = index > 0 ? int.Parse(empresaRecebedor.Cep.Remove(index, 1)) : int.Parse(empresaRecebedor.Cep);
                }
                catch (Exception)
                {
                    cte07.EnderRecebCep = null;
                }

                // Recupera o PaisBacen
                PaisBacen paisBacenReceb = null;
                if (cidadeIbgeRecebedor.CodigoIbge == 9999999)
                {
                    string siglaPaisAux = ObterSiglaPaisExteriorPeloSap(empresaRecebedor);
                    paisBacenReceb = _paisBacenRepository.ObterPorSiglaResumida(siglaPaisAux);
                    cte07.RecebCnpj = 0;
                }
                else
                {
                    paisBacenReceb = _paisBacenRepository.ObterPorSiglaResumida(cidadeIbgeRecebedor.SiglaPais);
                }

                // C�digo BACEN do pa�s do recebedor
                cte07.EnderRecebCpais = int.Parse(paisBacenReceb.CodigoBacen.Substring(1, 4));

                // Nome do pa�s. 
                cte07.EnderRecebXpais = paisBacen.Nome;

                // DADOS DO DESTINATARIO

                if (empresaDestinataria.TipoPessoa == TipoPessoaEnum.Juridica)
                {
                    // CNPJ do recebedor de servi�o
                    if (Int64.TryParse(empresaDestinataria.Cgc, out cnpj))
                    {
                        cte07.DestCnpj = cnpj;
                    }
                }
                else
                {
                    if (Int64.TryParse(empresaDestinataria.Cgc, out cnpj))
                    {
                        cte07.DestCpf = cnpj;
                    }
                }

                // Inscri��o estadual 
                cte07.DestIe = Tools.TruncateRemoveSpecialChar(empresaDestinataria.InscricaoEstadual, 14);

                // Raz�o social da empresa
                cte07.DestXNome = empresaDestinataria.RazaoSocial.Trim();

                // Telefone da empresa
                cte07.DestFone = Tools.TruncateRemoveSpecialChar(empresaDestinataria.Telefone1, 12, 7);

                // Inscri��o na SUFRAMA
                cte07.DestIsUf = string.Empty;

                // Logradouro do destinatario do servi�o
                cte07.EnderDestXlgr = empresaDestinataria.Endereco.Trim();

                // N�mero
                // cte07.EnderDestNro = string.Empty;
                cte07.EnderDestNro = "0";

                // Complemento do endere�o
                cte07.EnderDestXCpl = string.Empty;

                // Bairro do endere�o do destinatario
                cte07.EnderDestXBairro = "NAO INFORMADO";

                // C�digo IBGE do munic�pio do destinatario
                cte07.EnderDestCMun = cidadeIbgeDestinataria.CodigoIbge;

                if (cidadeIbgeDestinataria.CodigoIbge == 9999999)
                {
                    // Nome do munic�pio do tomador
                    cte07.EnderDestXMun = "EXTERIOR";
                    cte07.EnderDestUf = "EX";
                }
                else
                {
                    // Nome do munic�pio do destinatario
                    cte07.EnderDestXMun = cidadeIbgeDestinataria.Descricao;
                    // C�digo UF do destinatario
                    cte07.EnderDestUf = cidadeIbgeDestinataria.SiglaEstado;
                }

                // Cep do destinatario
                try
                {
                    index = empresaDestinataria.Cep.IndexOf('-');
                    cte07.EnderDestCep = index > 0 ? int.Parse(empresaDestinataria.Cep.Remove(index, 1)) : int.Parse(empresaDestinataria.Cep);
                }
                catch (Exception)
                {
                    cte07.EnderDestCep = null;
                }

                // Recupera o PaisBacen
                PaisBacen paisBacenDest = null;
                if (cidadeIbgeDestinataria.CodigoIbge == 9999999)
                {
                    string siglaPaisAux = ObterSiglaPaisExteriorPeloSap(empresaDestinataria);
                    paisBacenDest = _paisBacenRepository.ObterPorSiglaResumida(siglaPaisAux);
                    cte07.DestCnpj = 0;
                }
                else
                {
                    paisBacenDest = _paisBacenRepository.ObterPorSiglaResumida(cidadeIbgeDestinataria.SiglaPais);
                }

                // C�digo BACEN do pa�s do destinatario
                cte07.EnderDestCpais = int.Parse(paisBacenDest.CodigoBacen.Substring(1, 4));

                // Nome do pa�s. 
                cte07.EnderDestXpais = paisBacen.Nome;

                // DADOS DO LOCAL DE ENTREGA
                if (empresaDestinataria.TipoPessoa == TipoPessoaEnum.Juridica)
                {
                    // CNPJ do local de entrega
                    if (Int64.TryParse(empresaDestinataria.Cgc, out cnpj))
                    {
                        cte07.LocEntCnpj = cnpj;
                    }
                }
                else
                {
                    if (Int64.TryParse(empresaDestinataria.Cgc, out cnpj))
                    {
                        cte07.LocEntCpf = cnpj;
                    }
                }

                // Nome local de entrega
                cte07.LocEntXNome = empresaDestinataria.RazaoSocial.Trim();

                // Logradouro do local de entrega
                cte07.LocEntXlgr = empresaDestinataria.Endereco.Trim();

                // N�mero
                // cte07.LocEntNro = string.Empty;
                cte07.LocEntNro = "0";

                // Complemento do endere�o
                cte07.LocEntXcpl = string.Empty;

                // Bairro do local de enterga 
                cte07.EnderDestXBairro = "NAO INFORMADO";

                // C�digo IBGE do munic�pio do local de entrega
                cte07.LocEntCmun = cidadeIbgeDestinataria.CodigoIbge;

                if (cidadeIbgeDestinataria.CodigoIbge == 9999999)
                {
                    // Nome do munic�pio do tomador
                    cte07.LocEntXmun = "EXTERIOR";
                    cte07.LocEntUf = "EX";
                }
                else
                {
                    // Nome do munic�pio do local de entrega
                    cte07.LocEntXmun = cidadeIbgeDestinataria.Descricao;
                    // C�digo UF do local de entrega
                    cte07.LocEntUf = cidadeIbgeDestinataria.SiglaEstado;
                }

                cte07.LocEntXbairro = "NAO INFORMADO";

                // Inserir
                _cte07Repository.Inserir(cte07);
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(_cteComplementar, "GravarConfigCteEnvioService", string.Format("GravarInformacoesExpedidorRecebedorDestinatario: {0}", ex.Message), ex);
                throw;
            }
        }

        /// <summary>
        /// Valida os campos do Cte Complementado
        /// </summary>
        private void ValidarDadosCteComplementado()
        {
            StringBuilder buffer = new StringBuilder();

            if (_cteCteComplementado == null)
            {
                buffer.Append("_cteCteComplementado:null ");
            }

            if (_serieDespachoUfCteComplementado == null)
            {
                buffer.Append("_serieDespachoUfCteComplementado:null ");
            }

            if (_contratoHistoricoCteComplementado == null)
            {
                buffer.Append("_contratoHistoricoCteComplementado:null ");
            }

            if (buffer != null && buffer.Length > 0)
            {
                throw new Exception(buffer.ToString());
            }
        }

        /// <summary>
        /// Tabela de valores da prestacao do servico e impostos (1-1) - ICTE08_VALORES
        /// </summary>
        private void GravarValorPrestacaoServicoImposto()
        {
            try
            {
                Cte08 cte08 = new Cte08();

                int numConvertido;
                double baseCalculoIcms = 0.0;
                double aliquota = 0.0;
                double valorIcms = 0.0;

                double somaBaseCalculoIcms = 0.0;
                double somaValorIcms = 0.0;
                string cst = string.Empty;

                foreach (CteComplementado cteComplementado in _listaCteComplementado)
                {
                    if (cteComplementado.TipoComplemento == TipoOperacaoCteEnum.Complemento)
                    {
                        baseCalculoIcms = cteComplementado.ValorDiferenca;
                        //aliquota = cteComplementado.Cte.PercentualAliquotaIcms ?? 0.0;
                        /* n�o sabiamos qual campo usar da tabela de contrato hist�rico ent�o marreta */
                        aliquota = ((_cteComplementar.FluxoComercial.Contrato.AliquotaIcmsSubstProduto == 0 ? _cteComplementar.FluxoComercial.Contrato.PercentualAliquotaIcms : _cteComplementar.FluxoComercial.Contrato.AliquotaIcmsSubstProduto) ?? 0.0);
                        valorIcms = baseCalculoIcms * (aliquota / 100);

                        somaBaseCalculoIcms += baseCalculoIcms;
                        somaValorIcms += valorIcms;

                        if (String.IsNullOrEmpty(cst))
                        {
                            cst = cteComplementado.Cte.ContratoHistorico.Cst;
                        }
                    }
                    else
                    {
                        double valorSemIcms;
                        if (cteComplementado.Cte.ValorIcms.HasValue)
                        {
                            valorSemIcms = cteComplementado.Cte.ValorCte - cteComplementado.Cte.ValorIcms.Value;
                        }
                        else
                        {
                            valorSemIcms = cteComplementado.Cte.ValorCte;
                        }

                        aliquota = cteComplementado.TipoComplemento == TipoOperacaoCteEnum.ComplementoIcms ? cteComplementado.Cte.FluxoComercial.Contrato.PercentualAliquotaIcms ?? 0.0 :
                            cteComplementado.Cte.FluxoComercial.Contrato.AliquotaIcmsSubstProduto ?? 0.0;

                        baseCalculoIcms = valorSemIcms * (100 / (100 - aliquota));
                        valorIcms = cteComplementado.ValorDiferenca;
                        // valorIcms = baseCalculoIcms * (aliquota / 100);

                        somaBaseCalculoIcms += baseCalculoIcms;
                        somaValorIcms += valorIcms;

                        var contrato = _contratoHistoricoRepository.ObterPorContrato(cteComplementado.Cte.FluxoComercial.Contrato);

                        if (contrato == null || !contrato.Any())
                        {
                            throw new Exception("Contrato historico n�o localizado");
                        }

                        ContratoHistorico contratoHist = contrato.FirstOrDefault(x => x.Id == contrato.Max(f => f.Id));
                        cst = contratoHist.Cst;
                    }
                }

                // AtualizaIcmsCte(aliquota, somaBaseCalculoIcms, somaValorIcms);

                // Codigo interno da filial (PK)
                cte08.CfgUn = ObterIdentificadorFilial();

                // Numero do documento CT-e (PK)
                int.TryParse(_cteComplementar.Numero, out numConvertido);
                cte08.CfgDoc = numConvertido;

                // Serie do documento CT-e (PK)
                // cte08.CfgSerie = _cte.Serie;
                cte08.CfgSerie = int.Parse(_cteComplementar.Serie).ToString();

                if (_listaCteComplementado[0].TipoComplemento == TipoOperacaoCteEnum.Complemento)
                {
                    cte08.VprestVtprest = somaBaseCalculoIcms;
                    cte08.VprestVrec = somaBaseCalculoIcms;
                }
                else
                {
                    cte08.VprestVtprest = _listaCteComplementado[0].ValorDiferenca;
                    cte08.VprestVrec = _listaCteComplementado[0].ValorDiferenca;
                }

                // Classifica��o Tribut�ria do servi�o
                cte08.Cst20Cst = null;

                // Percentual de redu��o da BC
                cte08.Cst20Predbc = null;

                // Valor da BC do ICMS
                cte08.Cst20Vbc = null;

                // Al�quota do ICMS
                cte08.Cst20Picms = null;

                // Valor do ICMS
                cte08.Cst20Vicms = null;

                // Classifica��o Tribut�ria do servi�o
                cte08.Icms20Cst = null;

                // Valor da BC do ICMS
                cte08.Icms20Vbc = null;

                // Al�quota do ICMS
                cte08.Icms20Picms = null;

                // Valor do ICMS
                cte08.Icms20Vicms = null;

                // Percentual de redu��o da BC
                cte08.Icms20Predbc = null;

                // C�digo de Situa��o Tribut�ria
                cte08.Icms60Cst = null;

                // Al�quota do ICMS
                cte08.Icms60Picmsstret = null;

                // Valor da BC do ICMS ST retido
                cte08.Icms60Vbcstret = null;

                // Valor do Cr�dito outorgado/Presumido
                cte08.Icms60Vcred = null;

                // Valor do ICMS ST retido
                cte08.Icms60Vicmsstret = null;

                // Classifica��o Tribut�ria do Servi�o
                cte08.IcmsOutraufCst = null;

                // Percentual de redu��o da BC
                cte08.IcmsOutraUfPredbcOutraUf = null;

                // Valor da BC do ICMS
                cte08.IcmsOutraUfVbcOutraUf = null;

                // Al�quota do ICMS
                cte08.IcmsOutraUfPicmsOutraUf = null;

                // Valor do ICMS devido outra UF
                cte08.IcmsOutraUfVicmsOutraUf = null;

                // Indica se o contribuinte � Simples Nacional 1=Sim
                cte08.IcmsSnIndsn = null;

                // Verifica o tipo do CST
                if (cst == "00")
                {
                    // C�digo de Situa��o Tribut�ria
                    cte08.Cst00Cst = 00;

                    // Valor da BC do ICMS
                    cte08.Cst00Vbc = somaBaseCalculoIcms;

                    // Al�quota do ICMS
                    cte08.Cst00Picms = aliquota;

                    // Valor do ICMS
                    cte08.Cst00Vicms = somaValorIcms;

                    // classifica��o Tribut�ria do Servi�o
                    cte08.Icms00Cst = 00;

                    // Valor da BC do ICMS
                    cte08.Icms00Picms = aliquota;

                    // Al�quota do ICMS
                    cte08.Icms00Vbc = somaBaseCalculoIcms;

                    // Valor do ICMS
                    cte08.Icms00Vicms = somaValorIcms;
                }
                else if ((cst == "40") || (cst == "41") || (cst == "51"))
                {
                    // C�digo de Situa��o Tribut�ria
                    cte08.Cst45Cst = int.Parse(cst);

                    // classifica��o Tribut�ria do Servi�o
                    cte08.Icms45Cst = int.Parse(cst);
                }
                else if (cst == "60")
                {
                    double valorImpostoRetido = Math.Round(baseCalculoIcms * (aliquota / 100), 2);
                    // C�digo de Situa��o Tribut�ria
                    cte08.Icms60Cst = 60;

                    // Al�quota do ICMS
                    cte08.Icms60Picmsstret = aliquota;

                    // Valor da BC do ICMS ST retido
                    cte08.Icms60Vbcstret = baseCalculoIcms;

                    // Valor do ICMS ST retido
                    // cte08.Icms60Vicmsstret = valorTotalIcms;
                    cte08.Icms60Vicmsstret = valorImpostoRetido;

                    // Valor do Cr�dito outorgado/Presumido
                    cte08.Icms60Vcred = null;
                }
                else if (cst == "80")
                {
                    // C�digo de Situa��o Tribut�ria
                    cte08.Cst80Cst = 80;

                    // Al�quota do ICMS
                    cte08.Cst80Picms = aliquota;

                    // Valor da BC do ICMS
                    cte08.Cst80Vbc = somaBaseCalculoIcms;

                    // Valor do ICMS    
                    cte08.Cst80Vicms = somaValorIcms;

                    // Valor do Cr�dito outorgado/presumido
                    cte08.Cst80Vcred = null;
                }
                else if (cst == "81")
                {
                    // C�digo de Situa��o Tribut�ria
                    cte08.Cst81Cst = 81;

                    // Al�quota do ICMS
                    cte08.Cst81Picms = aliquota;

                    // Valor da BC do ICMS
                    cte08.Cst81Vbc = somaBaseCalculoIcms;

                    // Valor do ICMS
                    cte08.Cst81Vicms = somaValorIcms;

                    // Percentual de redu��o da BC do ICMS
                    cte08.Cst81Predbc = null;
                }
                else if (cst == "90")
                {
                    // C�digo de Situa��o Tribut�ria
                    cte08.Cst90Cst = 90;

                    // Al�quota do ICMS
                    cte08.Cst90Picms = aliquota;

                    // Valor da BC do ICMS
                    cte08.Cst90Vbc = somaBaseCalculoIcms;

                    // Valor do ICMS
                    cte08.Cst90Vicms = somaValorIcms;

                    // Percentual de redu��o da BC do ICMS
                    cte08.Cst90Predbc = null;

                    // Valor do Cr�dito outorgado/presumido
                    cte08.Cst90Vcred = null;

                    // C�digo de Situa��o Tribut�ria
                    cte08.Icms90Cst = 90;

                    // Al�quota do ICMS
                    cte08.Icms90Picms = aliquota;

                    // Valor da BC do ICMS
                    cte08.Icms90Vbc = somaBaseCalculoIcms;

                    // Valor do ICMS
                    cte08.Icms90Vicms = somaValorIcms;

                    // Percentual de redu��o da BC do ICMS
                    cte08.Icms90Predbc = null;

                    // Valor do Cr�dito outorgado/presumido
                    cte08.Icms90Vcred = null;
                }

                _cte08Repository.Inserir(cte08);
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(_cteComplementar, "GravarConfigCteComplementoService", string.Format("GravarValorPrestacaoServicoImposto: {0}", ex.Message), ex);
                throw;
            }
        }

        /// <summary>
        /// Tabelas de informacoes de CT-e complementado (0-N) - ICTE35_ICC
        /// </summary>
        private void GravarInformacoesCteComplementar()
        {
            try
            {
                Cte35 cte35 = null;

                int numConvertido;
                double valorPrestacao = 0.0;
                double baseCalculoIcms = 0.0;
                double aliquota = 0.0;
                double valorTotalIcms = 0.0;
                int numSequencial = 0;

                double pesoParaCalculo;

                IList<ComposicaoFreteCteInterno> listaComposicaoFreteCteInterno;

                foreach (CteComplementado cteDetalhe in _listaCteComplementado)
                {
                    listaComposicaoFreteCteInterno = null;

                    // Recupera os dados de calculo
                    CarregarDadosCteComplementado(cteDetalhe);

                    // Valida os dados
                    ValidarDadosCteComplementado();

                    pesoParaCalculo = 0.0;

                    listaComposicaoFreteCteInterno = ObterListaComposicaoFreteCte(cteDetalhe, ref pesoParaCalculo);

                    valorPrestacao = cteDetalhe.ValorDiferenca;

                    baseCalculoIcms = cteDetalhe.ValorDiferenca;
                    /* verificar */
                    aliquota = cteDetalhe.Cte.ContratoHistorico.PercentualAliquotaIcms ?? 0.0;
                    valorTotalIcms = baseCalculoIcms * (aliquota / 100);

                    cte35 = new Cte35();

                    // Codigo interno da filial (PK)
                    cte35.CfgUn = ObterIdentificadorFilial();

                    // Numero do documento CT-e (PK)
                    int.TryParse(_cteComplementar.Numero, out numConvertido);
                    cte35.CfgDoc = numConvertido;

                    // Serie do documento CT-e (PK)
                    cte35.CfgSerie = int.Parse(_cteComplementar.Serie).ToString();

                    // Numero Sequencial da nota
                    cte35.IdIcc = ++numSequencial;

                    // Chave do CT-e complementado
                    cte35.InfCteChave = cteDetalhe.Cte.Chave;

                    // Valor da Prestacao
                    cte35.VpresVtPrest = valorPrestacao;

                    // Classifica��o Tribut�ria do servi�o
                    // cte35.Cst20Cst = null;

                    //// Percentual de redu��o da BC
                    // cte35.Cst20PredBc = null;

                    //// Valor da BC do ICMS
                    // cte35.Cst20Vbc = null;

                    //// Al�quota do ICMS
                    // cte35.Cst20Picms = null;

                    //// Valor do ICMS
                    // cte35.Cst20Vicms = null;

                    //// Classifica��o Tribut�ria do servi�o
                    // cte35.Icms20Cst = null;

                    //// Valor da BC do ICMS
                    // cte35.Icms20Vbc = null;

                    //// Al�quota do ICMS
                    // cte35.Icms20Picms = null;

                    //// Valor do ICMS
                    // cte35.Icms20Vicms = null;

                    //// Percentual de redu��o da BC
                    // cte35.Icms20PredBc = null;

                    //// Classifica��o Tribut�ria do Servi�o
                    // cte35.IcmsOutraUfCst = null;

                    //// Percentual de redu��o da BC
                    // cte35.IcmsOutraUfPredBcOutraUf = null;

                    //// Valor da BC do ICMS
                    // cte35.IcmsOutraUfVbcOutraUf = null;

                    //// Al�quota do ICMS
                    // cte35.IcmsOutraUfPicmsOutrauf = null;

                    //// Valor do ICMS devido outra UF
                    // cte35.IcmsOutraUfVicmsOutraUf = null;

                    //// Indica se o contribuinte � Simples Nacional 1=Sim
                    // cte35.IcmsSnIndSn = null;

                    //// Informa��es adicionais de interesse do Fisco
                    // cte35.ImpCompInfAdFisco = string.Empty;

                    // Verifica o tipo do CST
                    if (cteDetalhe.Cte.ContratoHistorico.Cst == "00")
                    {
                        // C�digo de Situa��o Tribut�ria
                        cte35.Cst00Cst = "00";

                        // Valor da BC do ICMS
                        cte35.Cst00Vbc = baseCalculoIcms;

                        // Al�quota do ICMS
                        cte35.Cst00Picms = aliquota;

                        // Valor do ICMS
                        cte35.Cst00Vicms = valorTotalIcms;

                        // classifica��o Tribut�ria do Servi�o
                        cte35.Icms00Cst = "00";

                        // Valor da BC do ICMS
                        cte35.Icms00Picms = aliquota;

                        // Al�quota do ICMS
                        cte35.Icms00Vbc = baseCalculoIcms;

                        // Valor do ICMS
                        cte35.Icms00Vicms = valorTotalIcms;
                    }
                    else if ((cteDetalhe.Cte.ContratoHistorico.Cst == "40") || (cteDetalhe.Cte.ContratoHistorico.Cst == "41") ||
                                     (cteDetalhe.Cte.ContratoHistorico.Cst == "51"))
                    {
                        // C�digo de Situa��o Tribut�ria
                        cte35.Cst45Cst = cteDetalhe.Cte.ContratoHistorico.Cst;

                        // classifica��o Tribut�ria do Servi�o
                        cte35.Icms45Cst = cteDetalhe.Cte.ContratoHistorico.Cst;
                    }
                    else if (cteDetalhe.Cte.ContratoHistorico.Cst == "60")
                    {
                        // C�digo de Situa��o Tribut�ria
                        cte35.Icms60Cst = "60";

                        // Al�quota do ICMS
                        cte35.Icms90Picms = aliquota;

                        // Valor da BC do ICMS ST retido
                        cte35.Icms60VbcStRet = baseCalculoIcms;

                        // Valor do ICMS ST retido
                        cte35.Icms60VicmsStRet = valorTotalIcms;

                        // Valor do Cr�dito outorgado/Presumido
                        cte35.Icms60Vcred = null;
                    }
                    else if (cteDetalhe.Cte.ContratoHistorico.Cst == "80")
                    {
                        // C�digo de Situa��o Tribut�ria
                        cte35.Cst80Cst = "80";

                        // Al�quota do ICMS
                        cte35.Cst80Picms = aliquota;

                        // Valor da BC do ICMS
                        cte35.Cst80Vbc = baseCalculoIcms;

                        // Valor do ICMS    
                        cte35.Cst80Vicms = valorTotalIcms;
                    }
                    else if (cteDetalhe.Cte.ContratoHistorico.Cst == "81")
                    {
                        // C�digo de Situa��o Tribut�ria
                        cte35.Cst81Cst = "81";

                        // Al�quota do ICMS
                        cte35.Cst81Picms = aliquota;

                        // Valor da BC do ICMS
                        cte35.Cst81Vbc = baseCalculoIcms;

                        // Valor do ICMS
                        cte35.Cst81Vicms = valorTotalIcms;

                        // Percentual de redu��o da BC do ICMS
                        cte35.Cst81PredBc = null;
                    }
                    else if (cteDetalhe.Cte.ContratoHistorico.Cst == "90")
                    {
                        // C�digo de Situa��o Tribut�ria
                        cte35.Cst90Cst = "90";

                        // Al�quota do ICMS
                        cte35.Cst90Picms = aliquota;

                        // Valor da BC do ICMS
                        cte35.Cst90Vbc = baseCalculoIcms;

                        // Valor do ICMS
                        cte35.Cst90Vicms = valorTotalIcms;

                        // Percentual de redu��o da BC do ICMS
                        cte35.Cst90PredBc = null;

                        // Valor do Cr�dito outorgado/presumido
                        cte35.Cst90Vcred = null;

                        // C�digo de Situa��o Tribut�ria
                        cte35.Icms90Cst = "90";

                        // Al�quota do ICMS
                        cte35.Icms90Picms = aliquota;

                        // Valor da BC do ICMS
                        cte35.Icms90Vbc = baseCalculoIcms;

                        // Valor do ICMS
                        cte35.Icms90Vicms = valorTotalIcms;

                        // Percentual de redu��o da BC do ICMS
                        cte35.Icms90PredBc = null;

                        // Valor do Cr�dito outorgado/presumido
                        cte35.Icms90Vcred = null;
                    }

                    // Inserir
                    _cte35Repository.Inserir(cte35);

                    GravarInformacoesComponentesCteComplementar(listaComposicaoFreteCteInterno, numSequencial);
                    InserirComposicaoFreteCte(listaComposicaoFreteCteInterno);
                }
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(_cteComplementar, "GravarInformacoesCteComplementar", string.Format("Executar: {0}", ex.Message), ex);
                throw;
            }
        }

        /// <summary>
        /// Tabela de informacoes dos componestes de valor da prestacao do complementado(0-N) - ICTE36_ICC_COMPCOMP
        /// </summary>
        /// <param name="listaComposicao">Lista com as composi��es internas</param>
        /// <param name="index"> Chave sequencial da tabela icte35</param>
        private void GravarInformacoesComponentesCteComplementar(IList<ComposicaoFreteCteInterno> listaComposicao, int index)
        {
            ComposicaoFreteContrato composicaoFreteContrato;

            Cte36 cte36;
            int i = 1;

            foreach (ComposicaoFreteCteInterno composicaoFreteCteInterno in listaComposicao)
            {
                if (composicaoFreteCteInterno.ComposicaoFreteCte.CondicaoTarifa != "ICMI")
                {
                    composicaoFreteContrato = _composicaoFreteContratoRepository.ObterPorNumeroContratoCondicaoTarifa(_contratoHistoricoCteComplementado.NumeroContrato, composicaoFreteCteInterno.ComposicaoFreteCte.CondicaoTarifa);

                    if (composicaoFreteContrato != null)
                    {
                        if (composicaoFreteContrato.ValorSemIcms.Value > 0)
                        {
                            cte36 = new Cte36();

                            // Codigo interno da filial
                            cte36.CfgUn = ObterIdentificadorFilial();

                            // Numero do documento CT-e
                            cte36.CfgDoc = Convert.ToInt32(_cteComplementar.Numero);

                            // Serie do documento CT-e
                            cte36.CfgSerie = int.Parse(_cteComplementar.Serie).ToString();

                            // Numero sequecial da tabela
                            cte36.IdIccCompComp = i;

                            // Numero sequecial da tabela
                            cte36.IdIcc = index;

                            // Nome do componente
                            cte36.CompCompXnome = Tools.TruncateString(composicaoFreteContrato.Descricao, 13).Trim();

                            // Valor do componente
                            cte36.CompCompVcomp = composicaoFreteCteInterno.ValorRateado;

                            // Inserir Ou Atualizar	
                            _cte36Repository.Inserir(cte36);
                            i++;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Tabela de informacoes dos documentos (NF, Nfe e Outros) (1-N) - ICTE06_EMIT_INF
        /// </summary>
        private void GravarInformacoesDocumentos1_04()
        {
            try
            {
                int intAux;
                int numSequencialNota = 0;
                long cnpj;
                double baseCalculoIcms = 0.0;
                double aliquota = 0.0;
                double valorTotalIcms = 0.0;
                double baseCalculoIcmsSt = 0.0;
                double aliquotaSt = 0.0;
                double valorTotalIcmsSt = 0.0;
                Cte _cte;
                IEmpresa _empresaPagadoraContrato;
                CidadeIbge _cidadeIbgeEmpresaPagadoraContrato;
                Estado _estadoEmpresaPagadoraContrato;

                foreach (var cteComplementado in _listaCteComplementado)
                {
                    foreach (CteDetalhe cteDetalhe in cteComplementado.Cte.ListaDetalhes.ToList())
                    {
                        Cte06 cte06 = new Cte06();

                        _cte = cteComplementado.Cte;
                        _empresaPagadoraContrato = _cte.ContratoHistorico.EmpresaPagadora;
                        _cidadeIbgeEmpresaPagadoraContrato = _empresaPagadoraContrato.CidadeIbge;
                        _estadoEmpresaPagadoraContrato = _empresaPagadoraContrato.Estado;

                        // Recupera os dados para calculo do ICMS
                        baseCalculoIcms = _cte.BaseCalculoIcms ?? 0.0;
                        valorTotalIcms = _cte.ValorIcms ?? 0.0;

                        baseCalculoIcmsSt = _cte.ContratoHistorico.ValorBaseCalculoSubstituicaoProduto ?? 0.0;
                        valorTotalIcmsSt = baseCalculoIcmsSt * aliquotaSt;

                        // Codigo interno da filial (PK)
                        cte06.CfgUn = ObterIdentificadorFilial();

                        // Numero do documento CT-e (PK)
                        int.TryParse(_cteComplementar.Numero, out intAux);
                        cte06.CfgDoc = intAux;

                        // Serie do documento CT-e (PK)
                        // cte06.CfgSerie = _cte.Serie;
                        cte06.CfgSerie = int.Parse(_cteComplementar.Serie).ToString();

                        // Numero sequecial da tabela (PK)
                        cte06.IdEmitInf = ++numSequencialNota;

                        // CNPJ do local de retirada
                        if (Int64.TryParse(_empresaPagadoraContrato.Cgc, out cnpj))
                        {
                            cte06.LocRetCnpj = cnpj;
                        }

                        // Cpf local de retirada
                        // cte06.LocRetCpf = null;

                        // Nome local de retirada
                        cte06.LocRetXnome = _empresaPagadoraContrato.RazaoSocial.Trim();

                        // Logradouro do local de retirada
                        cte06.LocRetXlgr = _empresaPagadoraContrato.Endereco.Trim();

                        // N�mero
                        // cte06.LocRetNro = string.Empty;
                        cte06.LocRetNro = "0";

                        // Complemento do endere�o
                        cte06.LocRetXcpl = string.Empty;

                        // Bairro do local de retirada 
                        cte06.LocRetXbairro = "NAO INFORMADO";

                        // C�digo IBGE do munic�pio do local de retirada
                        cte06.LocRetCmun = _cidadeIbgeEmpresaPagadoraContrato.CodigoIbge;

                        // Nome do munic�pio do local de retirada
                        cte06.LocRetXmun = _cidadeIbgeEmpresaPagadoraContrato.Descricao;

                        // C�digo UF do local de retirada
                        cte06.LocRetUf = _estadoEmpresaPagadoraContrato.Sigla;

                        // Chave de acesso da NFE
                        if (!string.IsNullOrEmpty(cteDetalhe.ChaveNfe))
                        {
                            cte06.InfNfeChave = cteDetalhe.ChaveNfe;

                            // PIN atribu�do pela SUFRAMA para a opera��o
                            cte06.InfNfePin = null;
                        }
                        else
                        {
                            if ((cteDetalhe.SerieNota.ToUpper() != "V2") && (cteDetalhe.SerieNota.ToUpper() != "OU"))
                            {
                                // N�mero do romaneio
                                // cte06.InfNfNroma = null;

                                // N�mero do pedido da NF
                                // cte06.InfNfNped = null;

                                // S�rie da nota
                                cte06.InfNfSerie = cteDetalhe.SerieNota;

                                // N�mero da nota
                                cte06.InfNfNdoc = cteDetalhe.NumeroNota;

                                // Data da emiss�o da nota
                                cte06.InfNfDemi = cteDetalhe.DataNotaFiscal;

                                // Valor da base para c�lculo do ICMS
                                cte06.InfNfVbc = baseCalculoIcms;

                                // Valor do ICMS
                                cte06.InfNfVicms = valorTotalIcms;

                                // Valor da base de c�lculo de substitui��o
                                cte06.InfNfVbcst = baseCalculoIcmsSt;

                                // Valor total do ICMS de substitui��o calculado como vBCST * ALIQICMSST da tabela CONTRATO.
                                cte06.InfNfVst = valorTotalIcmsSt;

                                // Valor total dos produtos
                                cte06.InfNfVprod = cteDetalhe.ValorNotaFiscal;

                                // Valor total da nota
                                // cte06.InfNfVnf = CalcularValorTotalNotaFiscal();

                                // CFOP predominante
                                cte06.InfNfNcfop = null;
                                if (_cte.ContratoHistorico.CfopProduto != null)
                                {
                                    if (int.TryParse(_cte.ContratoHistorico.CfopProduto, out intAux))
                                    {
                                        cte06.InfNfNcfop = intAux;
                                    }
                                }
                                else
                                {
                                    if (int.TryParse(_cte.ContratoHistorico.Cfop, out intAux))
                                    {
                                        cte06.InfNfNcfop = intAux;
                                    }
                                }

                                // Peso
                                cte06.InfNfNpeso = cteDetalhe.PesoNotaFiscal;
                            }
                            else
                            {
                                // Tipo do documento outros
                                // Utilizado nos casos da S�rie ser V2 ou OU
                                cte06.InfOutrosTpDoc = 99;

                                // Descri��o do documento
                                cte06.InfOutrosDescOutros = "OUTROS";

                                // N�mero do documento
                                cte06.InfOutrosNdoc = cteDetalhe.NumeroNota;

                                // Data da emiss�o do documento
                                cte06.InfOutrosDemi = cteDetalhe.DataNotaFiscal;

                                // Valor do documento
                                cte06.InfOutrosVdocFisc = cteDetalhe.ValorNotaFiscal;
                            }

                            // Modelo da Nota Fiscal
                            if (int.TryParse(_fluxoComercial.ModeloNotaFiscal, out intAux))
                            {
                                cte06.InfNfMod = intAux;
                            }
                        }

                        // Inseri o registro
                        _cte06Repository.Inserir(cte06);
                    }
                }
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(_cteComplementar, "GravarConfigCteEnvioService", string.Format("GravarInformacoesDocumentos: {0}", ex.Message), ex);
                throw;
            }
        }

        /// <summary>
        /// Grava na tabela de composicao frete cte 
        /// </summary>
        /// <param name="listaComposicao">Lista com as composi��es internas</param>
        private void InserirComposicaoFreteCte(IList<ComposicaoFreteCteInterno> listaComposicao)
        {
            ComposicaoFreteCte composicaoFreteCte;

            foreach (ComposicaoFreteCteInterno composicaoFreteCteInterno in listaComposicao)
            {
                // Insere na composi��o frete cte
                composicaoFreteCte = new ComposicaoFreteCte();
                composicaoFreteCte.Cte = _cteComplementar;
                composicaoFreteCte.CondicaoTarifa = composicaoFreteCteInterno.ComposicaoFreteCte.CondicaoTarifa;
                composicaoFreteCte.Ferrovia = composicaoFreteCteInterno.ComposicaoFreteCte.Ferrovia;
                composicaoFreteCte.ValorComIcms = composicaoFreteCteInterno.ValorRateado;
                composicaoFreteCte.ValorSemIcms = composicaoFreteCteInterno.ValorRateado;

                composicaoFreteCte.ValorToneladaComIcms = composicaoFreteCteInterno.ComposicaoFreteCte.ValorToneladaComIcms ?? 0.0;
                composicaoFreteCte.ValorToneladaSemIcms = composicaoFreteCteInterno.ComposicaoFreteCte.ValorToneladaSemIcms ?? 0.0;
                composicaoFreteCte.Coeficiente = composicaoFreteCteInterno.ComposicaoFreteCte.Coeficiente;

                _composicaoFreteCteRepository.Inserir(composicaoFreteCte);
            }
        }

        /// <summary>
        /// Carrega os Dados para gera��o do CTE
        /// </summary>
        private void CarregarDados()
        {
            try
            {
                if (_cteComplementar != null)
                {
                    // Informa��es do fluxo comercial
                    _fluxoComercial = _cteComplementar.FluxoComercial;

                    // Carregar dados do CTE complementado
                    var retorno = _cteComplementadoRepository.ObterCteComplementadoPorCteComplementar(_cteComplementar);

                    _listaCteComplementado = new List<CteComplementado>();
                    if (retorno.Any())
                    {
                        _listaCteComplementado.Add(retorno.FirstOrDefault());
                    }

                    // Dados do vag�o (pega o vagao do 1 complemento)
                    if (_listaCteComplementado.Count > 0)
                    {
                        _vagao = _listaCteComplementado[0].Cte.Vagao;

                        //// Lista de detalhes do Cte complementado anteriormente.
                        if ((_listaCteComplementado[0].Cte.ListaDetalhes != null) && (_listaCteComplementado[0].Cte.ListaDetalhes.Count > 0))
                        {
                            _listaCteComplementadoDetalhe = _cteDetalheRepository.ObterPorCte(_listaCteComplementado[0].Cte);
                        }
                        //// Foi adicionado o valor do contrato historico em cima do cte pai deste cte complementar
                        if (_cteComplementar.ContratoHistorico == null)
                        {
                            _cteComplementar.ContratoHistorico = _listaCteComplementado[0].Cte.ContratoHistorico;
                        }
                    }
                }

                // Carregar os dados do cte raiz
                CteArvore cteArvore = _cteArvoreRepository.ObterCteArvorePorCteFilho(_cteComplementar);
                _cteRaiz = cteArvore.CteRaiz;

                CteAgrupamento cteAgrupamento = _cteAgrupamentoRepository.ObterAgrupamentoRaizPorCteFilho(_cteComplementar);
                _cteRaizAgrupamento = cteAgrupamento.CteRaiz;

                if (_fluxoComercial != null)
                {
                    // Dados da empresa remetente
                    _empresaRemetenteFluxoComercial = _fluxoComercial.EmpresaRemetente;

                    // Dados da empresa destinataria
                    _empresaDestinatariaFluxoComercial = _fluxoComercial.EmpresaDestinataria;

                    _estacaoMaeOrigemFluxoComercial = _fluxoComercial.Origem;
                    _estacaoMaeDestinoFluxoComercial = _fluxoComercial.Destino;
                }

                _empresaFerroviaCliente = _empresaFerroviaClienteRepository.ObterEmpresaClientePorCnpjDaFerrovia(_cteComplementar.CnpjFerrovia, _cteRaiz.SiglaUfFerrovia);

                if (_empresaFerroviaCliente == null)
                {
                    _empresaFerroviaCliente = ObterEmpresaFerrovia(_fluxoComercial.Contrato);
                }

                if (_empresaFerroviaCliente != null)
                {
                    _cidadeIbgeEmpresaFerrovia = _empresaFerroviaCliente.CidadeIbge;

                    // Verifica qual � o Tipo de Trafego Mutuo
                    _tipoTrafegoMutuo = ObterTipoTrafegoMutuo();
                }

                if (_cteComplementar.Id == _cteRaizAgrupamento.Id)
                {
                    _empresaTomadora = _fluxoComercial.EmpresaPagadora;

                    if (_empresaTomadora != null)
                    {
                        _cidadeIbgeEmpresaTomadora = _empresaTomadora.CidadeIbge;
                    }
                }
                else
                {
                    _empresaTomadora = _empresaFerroviaClienteRepository.ObterEmpresaClientePorCnpjDaFerrovia(_cteRaizAgrupamento.CnpjFerrovia, _cteRaizAgrupamento.SiglaUfFerrovia);
                }

                if (_empresaTomadora != null)
                {
                    _cidadeIbgeEmpresaTomadora = _empresaTomadora.CidadeIbge;
                }

                if (_estacaoMaeOrigemFluxoComercial != null)
                {
                    _municipioEstacaoMaeOrigemFluxoComercial = _estacaoMaeOrigemFluxoComercial.Municipio;

                    _empresaOperadoraEstacaoMaeOrigemFluxoComercial = _estacaoMaeOrigemFluxoComercial.EmpresaOperadora;
                }

                if (_municipioEstacaoMaeOrigemFluxoComercial != null)
                {
                    _estadoEstacaoMaeOrigemFluxoComercial = _municipioEstacaoMaeOrigemFluxoComercial.Estado;

                    _cidadeIbgeEstacaoMaeOrigemFluxoComercial = _municipioEstacaoMaeOrigemFluxoComercial.CidadeIbge;
                }

                if (_estacaoMaeDestinoFluxoComercial != null)
                {
                    _municipioEstacaoMaeDestinoFluxoComercial = _estacaoMaeDestinoFluxoComercial.Municipio;
                }

                if (_municipioEstacaoMaeDestinoFluxoComercial != null)
                {
                    _estadoEstacaoMaeDestinoFluxoComercial = _municipioEstacaoMaeDestinoFluxoComercial.Estado;

                    _cidadeIbgeEstacaoMaeDestinoFluxoComercial = _municipioEstacaoMaeDestinoFluxoComercial.CidadeIbge;
                }

                if (_empresaRemetenteFluxoComercial != null)
                {
                    _estadoEmpresaRemetenteFluxoComercial = _empresaRemetenteFluxoComercial.Estado;
                }

                ObterEmpresaRemetenteFiscal();
                ObterEmpresaDestinatariaFiscal();

                if (_listaCteComplementado.Any())
                {
                    _cfop = _listaCteComplementado[0].Cte.ContratoHistorico.Cfop;
                    _cfopDescricao = _listaCteComplementado[0].Cte.ContratoHistorico.DescricaoCfop;
                }
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(_cteComplementar, "GravarConfigCteEnvioService", string.Format("CarregarDados: {0}", ex.Message), ex);
                throw;
            }
        }

        /// <summary>
        /// Obtem o Identificador da Filial
        /// </summary>
        /// <returns>C�digo do indentificador da Filial</returns>
        private int ObterIdentificadorFilial()
        {
            if (_nfe03Filial == null)
            {
                long cnpj;
                Int64.TryParse(_cteComplementar.CnpjFerrovia, out cnpj);

                _nfe03Filial = _nfe03FilialRepository.ObterFilialPorCnpj(cnpj);

                if (_nfe03Filial != null)
                {
                    return (int)_nfe03Filial.IdFilial;
                }
                else
                {
                    return -1;
                }
            }

            return (int)_nfe03Filial.IdFilial;
        }

        /// <summary>
        /// Valida os Dados
        /// </summary>
        private void ValidarDados()
        {
            StringBuilder buffer = new StringBuilder();

            if (_cteComplementar == null)
            {
                buffer.Append("_cte:null ");
                // throw new Exception("Cte n�o econtrado");
            }

            // if (_listaCteDetalhe == null)
            // {
            //    buffer.Append("_listaCteDetalhe:null ");
            //    // throw new Exception("Lista de Detalhe do Cte n�o econtrado");
            // }

            if (_cteRaiz == null)
            {
                buffer.Append("_cteRaiz:null ");
                // throw new Exception("Cte Raiz n�o econtrado");
            }

            if (_cteRaizAgrupamento == null)
            {
                buffer.Append("_cteRaizAgrupamento:null ");
                // throw new Exception("Cte Raiz Agrupamento n�o econtrado");
            }

            if (_fluxoComercial == null)
            {
                buffer.Append("_fluxoComercial:null ");
                // throw new Exception("Fluxo Comercial n�o econtrado");
            }

            // if (_despachoTranslogic == null)
            // {
            //    buffer.Append("_despachoTranslogic:null ");
            //    // throw new Exception("Despacho n�o econtrado");
            // }

            // if (_serieDespachoUf == null)
            // {
            //    buffer.Append("_serieDespachoUf:null ");
            //    // throw new Exception("Serie Despacho UF n�o econtrado");
            // }

            if (_empresaRemetenteFluxoComercial == null)
            {
                buffer.Append("_empresaRemetenteFluxoComercial:null ");
                // new Exception("Empresa Remetente do Fluxo Comercial n�o econtrada");
            }

            if (_estadoEmpresaRemetenteFluxoComercial == null)
            {
                buffer.Append("_estadoEmpresaRemetenteFluxoComercial:null ");
                // new Exception("Estado da Empresa Remetente do Fluxo Comercial n�o econtrada");
            }

            // if (_contratoHistorico == null)
            // {
            //    buffer.Append("_contratoHistorico:null ");
            //    // new Exception("Contrato Historico n�o econtrado");
            // }

            // if (_empresaPagadoraContratoHistorico == null)
            // {
            //    buffer.Append("_empresaPagadoraContratoHistorico:null ");
            //    // new Exception("Empresa Pagadora do Contrato Historico n�o econtrada");
            // }

            // if (_empresaRemetenteContratoHistorico == null)
            // {
            //    buffer.Append("_empresaRemetenteContratoHistorico:null ");
            //    // new Exception("Empresa Remetente do Contrato Historico n�o econtrada");
            // }

            // if (_empresaDestinatariaContratoHistorico == null)
            // {
            //    buffer.Append("_empresaDestinatariaContratoHistorico:null ");
            //    // new Exception("Empresa Destinataria do Contrato Historico n�o econtrada");
            // }

            // if (_cidadeIbgeEmpresaPagadoraContratoHistorico == null)
            // {
            //    buffer.Append("_cidadeIbgeEmpresaPagadoraContratoHistorico:null ");
            //    // new Exception("Cidade IBGE da Empresa Pagadora do Contrato Historico n�o econtrada");
            // }

            // if (_cidadeIbgeEmpresaDestinatariaContratoHistorico == null)
            // {
            //    buffer.Append("_cidadeIbgeEmpresaDestinatariaContratoHistorico:null ");
            //    // new Exception("Cidade IBGE da Empresa Destinat�ria do Contrato Historico n�o econtrada");
            // }

            // if (_estadoEmpresaPagadoraContratoHistorico == null)
            // {
            //    buffer.Append("_estadoEmpresaPagadoraContratoHistorico:null ");
            //    // new Exception("Estado da Empresa Pagadora do Contrato Historicon�o econtrada");
            // }

            // if (_estadoEmpresaDestinatariaContratoHistorico == null)
            // {
            //    buffer.Append("_estadoEmpresaDestinatariaContratoHistorico:null ");
            //    // new Exception("Estado da Empresa Destinataria do Contrato Historico n�o econtrada");
            // }

            // if (_cidadeIbgeEmpresaRemetenteContratoHistorico == null)
            // {
            //    buffer.Append("_cidadeIbgeEmpresaRemetenteContratoHistorico:null ");
            //    // new Exception("Cidade IBGE da Empresa Remetente do Contrato Historico n�o econtrada");
            // }

            // if (_itemDespacho == null)
            // {
            //    buffer.Append("_itemDespacho:null ");
            //    // new Exception("Item Despacho n�o econtrado");
            // }

            // if (_detalheCarregamento == null)
            // {
            //    buffer.Append("_detalheCarregamento:null ");
            //    // new Exception("Detalhe do Carregamento n�o econtrado");
            // }

            // if (_carregamento == null)
            // {
            //    buffer.Append("_carregamento:null ");
            //    // new Exception("Carregamento n�o econtrado");
            // }

            if (_vagao == null)
            {
                buffer.Append("_vagao:null ");
                // new Exception("Vag�o n�o econtrado");
            }

            // if (_serieVagao == null)
            // {
            //    buffer.Append("_serieVagao:null ");
            //    // new Exception("S�rie do Vag�o n�o econtrado");
            // }

            // if (_tipoVagao == null)
            // {
            //    buffer.Append("_tipoVagao:null ");
            //    // new Exception("Tipo do Vag�o n�o econtrado");
            // }

            // if (_empresaProprietariaVagao == null)
            // {
            //    buffer.Append("_empresaProprietariaVagao:null ");
            //    // new Exception("Empresa Propriet�ria Vag�o n�o econtrado");
            // }

            if (_estacaoMaeOrigemFluxoComercial == null)
            {
                buffer.Append("_estacaoMaeOrigemFluxoComercial:null ");
                // new Exception("Esta��o M�e Origem do Fluxo Comercial n�o econtrada");
            }

            if (_estacaoMaeDestinoFluxoComercial == null)
            {
                buffer.Append("_estacaoMaeDestinoFluxoComercial:null ");
                // new Exception("Esta��o M�e Destino do Fluxo Comercial n�o econtrada");
            }

            if (_municipioEstacaoMaeOrigemFluxoComercial == null)
            {
                buffer.Append("_municipioEstacaoMaeOrigemFluxoComercial:null ");
                // new Exception("Municipio da Esta��o M�e Origem do Fluxo Comercial n�o econtrado");
            }

            if (_estadoEstacaoMaeOrigemFluxoComercial == null)
            {
                buffer.Append("_estadoEstacaoMaeOrigemFluxoComercial:null ");
                // new Exception("Estado da Esta��o M�e Origem do Fluxo Comercial n�o econtrado");
            }

            if (_municipioEstacaoMaeDestinoFluxoComercial == null)
            {
                buffer.Append("_municipioEstacaoMaeDestinoFluxoComercial:null ");
                // new Exception("Municipio da Esta��o M�e Destino do Fluxo Comercial n�o econtrado");
            }

            if (_estadoEstacaoMaeDestinoFluxoComercial == null)
            {
                buffer.Append("_estadoEstacaoMaeDestinoFluxoComercial:null ");
                // new Exception("Estado da Esta��o M�e Destino do Fluxo Comercial n�o econtrado");
            }

            if (_cidadeIbgeEstacaoMaeOrigemFluxoComercial == null)
            {
                buffer.Append("_cidadeIbgeEstacaoMaeOrigemFluxoComercial:null ");
                // new Exception("Cidade IBGE da Esta��o M�e Origem do Fluxo Comercial n�o econtrada");
            }

            if (_cidadeIbgeEstacaoMaeDestinoFluxoComercial == null)
            {
                buffer.Append("_cidadeIbgeEstacaoMaeDestinoFluxoComercial:null ");
                // new Exception("Cidade IBGE da Esta��o M�e Destino do Fluxo Comercial n�o econtrada");
            }

            if (_empresaOperadoraEstacaoMaeOrigemFluxoComercial == null)
            {
                buffer.Append("_empresaOperadoraEstacaoMaeOrigemFluxoComercial:null ");
                // new Exception("Empresa Operadora da Esta��o M�e Origem do Fluxo Comercial n�o econtrada");
            }

            if (buffer != null && buffer.Length > 0)
            {
                throw new Exception(buffer.ToString());
            }
        }

        /// <summary>
        /// Efetua o tratamento do retorno da validacao das regular expression
        /// </summary>
        /// <param name="ret">Lista de erros encontrados</param>
        private void TratarRetornoRegexSefaz(InvalidValue[] ret)
        {
            if (_errosValidacao == null)
            {
                _errosValidacao = new StringBuilder();
            }

            if (ret != null && ret.Count() > 0)
            {
                foreach (InvalidValue invalidValue in ret)
                {
                    _errosValidacao.AppendLine(invalidValue.Message);
                }
            }
        }

        /// <summary>
        /// Obt�m o tipo de trafego mutuo
        /// </summary>
        /// <returns>Retorna o tipo de trafego mutuo</returns>
        private TipoTrafegoMutuoEnum ObterTipoTrafegoMutuo()
        {
            if (!_listaCteComplementado.Any())
            {
                return TipoTrafegoMutuoEnum.Indefinido;
            }

            TipoTrafegoMutuoEnum tipoTrafegoMutuo = TipoTrafegoMutuoEnum.Indefinido;

            Contrato _contrato = _listaCteComplementado[0].Cte.FluxoComercial.Contrato;
            if (_contrato.FerroviaFaturamento == null)
            {
                return tipoTrafegoMutuo;
            }

            string siglaFerrovia = ALL.Core.Util.Enum<FerroviaFaturamentoEnum>.GetDescriptionOf(_contrato.FerroviaFaturamento.Value);

            // IEmpresa empresaFerrovia = _empresaFerroviaRepository.ObterPorSiglaUf(siglaFerrovia, _cteRaizAgrupamento.SiglaUfFerrovia);
            IEmpresa empresaFerrovia = ObterEmpresaFerrovia(siglaFerrovia);

            if (empresaFerrovia == null)
            {
                empresaFerrovia = _empresaFerroviaCliente;
            }

            if (empresaFerrovia.IndEmpresaALL == false)
            {
                if (_fluxoComercial.OrigemIntercambio != null)
                {
                    tipoTrafegoMutuo = TipoTrafegoMutuoEnum.IntercambioFaturamentoOrigem;
                }
                else if (_fluxoComercial.DestinoIntercambio != null)
                {
                    tipoTrafegoMutuo = TipoTrafegoMutuoEnum.IntercambioFaturamentoDestino;
                }
                else
                {
                    tipoTrafegoMutuo = TipoTrafegoMutuoEnum.Indefinido;
                }
            }
            else
            {
                tipoTrafegoMutuo = TipoTrafegoMutuoEnum.FaturamentoEmpresaGrupo;
            }

            return tipoTrafegoMutuo;
        }

        private IEmpresa ObterEmpresaFerrovia(string siglaFerrovia)
        {
            Contrato contrato = _listaCteComplementado[0].Cte.FluxoComercial.Contrato;

            IEmpresa empresaFerrovia = _empresaFerroviaRepository.ObterPorSiglaUf(siglaFerrovia, _cteRaizAgrupamento.SiglaUfFerrovia);

            if (empresaFerrovia == null)
            {
                empresaFerrovia = _empresaInterfaceCteRepository.ObterPorSiglaUF(siglaFerrovia, _cteRaizAgrupamento.SiglaUfFerrovia);
            }

            // Pega pela esta��o de Origem 
            if (empresaFerrovia == null)
            {
                if (contrato.FerroviaFaturamento.Value == FerroviaFaturamentoEnum.Argentina)
                {
                    empresaFerrovia = _fluxoComercial.Origem.EmpresaConcessionaria;
                }
            }

            return empresaFerrovia;
        }

        /// <summary>
        /// Verifica se o fluxo � um fluxo internacional
        /// </summary>
        /// <param name="fluxo"> Objeto FluxoComercial. </param>
        /// <returns> Valor booleano </returns>
        private string VerificarFluxoInternacional(FluxoComercial fluxo)
        {
            AssociaFluxoInternacional associaFluxoInternacional = _associaFluxoInternacionalRepository.ObterPorFluxoComercial(fluxo);

            if (associaFluxoInternacional != null)
            {
                FluxoInternacional fluxoInternacional = associaFluxoInternacional.FluxoInternacional;

                if (fluxoInternacional != null)
                {
                    return fluxoInternacional.CodigoFluxoInternacional;
                }
            }

            return null;
        }

        /// <summary>
        /// Calcula a base de calculo do icms quando o cte for filho (depois do primeiro no trafago mutuo)
        /// </summary>
        /// <returns>Retorna a base de calculo do ICMS</returns>
        private double CalcularBaseCalculoIcmsCteFilho()
        {
            double valorBaseCalculo = 0.0;
            try
            {
                // Todo: VERIFICAR INFORMACOES
                int tipoTributacao = _contratoHistoricoCteComplementado.TipoTributacao ?? 0;

                // string letraFerrovia = _serieDespachoUf.LetraFerrovia;
                string letraFerrovia = _serieDespachoUfCteComplementado.LetraFerrovia;
                // Todo: Complementar
                // double toneladaUtil = _cteComplementar.PesoParaCalculo ?? 0.0;
                double toneladaUtil = _cteCteComplementado.PesoParaCalculo ?? 0.0;
                double aliquotaIcms = _cteCteComplementado.PercentualAliquotaIcms ?? 0.0;
                double coeficiente = 100 / (100 - aliquotaIcms);
                coeficiente = Math.Round(coeficiente, 4);

                IList<ComposicaoFreteContrato> listaComposicaoFreteContrato = _composicaoFreteContratoRepository.ObterPorNumeroContrato(_contratoHistoricoCteComplementado.NumeroContrato, _cteRaiz.DataHora, letraFerrovia, true);
                IList<ComposicaoFreteContrato> listaComposicaoFreteContratoVg = _composicaoFreteContratoRepository.ObterPorNumeroContrato(_contratoHistoricoCteComplementado.NumeroContrato, _cteRaiz.DataHora, "VG", true);

                double? valorTotal = 0.0;

                if (tipoTributacao == 0)
                {
                    valorTotal = listaComposicaoFreteContrato.Sum(g => g.ValorSemIcms);
                    if ((listaComposicaoFreteContratoVg.Count > 0) && (_cteCteComplementado.Vagao.EmpresaProprietaria.Sigla == "L"))
                    {
                        valorTotal += listaComposicaoFreteContratoVg.Sum(g => g.ValorSemIcms);
                    }
                }
                else
                {
                    valorTotal = listaComposicaoFreteContrato.Sum(g => g.ValorComIcms);
                    if ((listaComposicaoFreteContratoVg.Count > 0) && (_cteCteComplementado.Vagao.EmpresaProprietaria.Sigla == "L"))
                    {
                        valorTotal += listaComposicaoFreteContratoVg.Sum(g => g.ValorComIcms);
                    }
                }

                valorTotal *= toneladaUtil;

                valorBaseCalculo = (valorTotal * coeficiente).Value;
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(_cteComplementar, "GravarConfigCteEnvioService", string.Format("CalcularBaseCalculoIcmsCteFilho: {0}", ex.Message), ex);
                throw;
            }

            if (valorBaseCalculo < 0)
            {
                valorBaseCalculo = 0;
            }

            return Math.Round(valorBaseCalculo, 2);
        }

        /// <summary>
        /// Calcula a base de calculo do icms quando o cte for raiz (primeiro cte do trafego mutuo)
        /// </summary>
        /// <returns>Retorna a base de calculo do ICMS</returns>
        private double CalcularBaseCalculoIcmsCteRaiz()
        {
            double valorTotal = 0.0;
            double valorCalculo = 0.0;
            double valorBaseCalculoIcms = 0.0;
            // Todo: COnfirmar
            double toneladaUtil = _cteComplementar.PesoParaCalculo ?? 0.0;
            double aliquotaIcms = _cteComplementar.PercentualAliquotaIcms ?? 0.0;
            double coeficiente = 100 / (100 - aliquotaIcms);
            coeficiente = Math.Round(coeficiente, 4);

            try
            {
                IList<ComposicaoFreteContrato> listaComposicaoFreteContrato = _composicaoFreteContratoRepository.ObterPorNumeroContrato(_contratoHistoricoCteComplementado.NumeroContrato);

                foreach (ComposicaoFreteContrato composicao in listaComposicaoFreteContrato)
                {
                    if (composicao.ValorSemIcms > 0)
                    {
                        valorCalculo = (composicao.ValorSemIcms.Value * toneladaUtil) * coeficiente;
                        valorCalculo = Math.Round(valorCalculo, 2);
                        valorTotal += valorCalculo;
                    }
                }

                valorBaseCalculoIcms = valorTotal;
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(_cteComplementar, "GravarConfigCteEnvioService", string.Format("CalcularBaseCalculoIcmsCteRaiz: {0}", ex.Message), ex);
                throw;
            }

            return Math.Round(valorBaseCalculoIcms, 2);
        }

        private string ValidarTelefone(string telefone)
        {
            if (telefone == null)
            {
                return string.Empty;
            }

            string retorno = Tools.TruncateRemoveSpecialChar(telefone, 12, 7);

            Regex regex = new Regex(ALL.Core.Util.Enum<RegexSefaz>.GetDescriptionOf(RegexSefaz.ER36));
            Match match = regex.Match(retorno);

            retorno = match.Success ? match.Value : string.Empty;

            return retorno;
        }


        /// <summary>
        /// Obtem os dados da empresa remetente fiscal
        /// </summary>
        private void ObterEmpresaRemetenteFiscal()
        {
            IEmpresa empresaRemetente;
            int aux;

            // Verifica se � um Fluxo da Brado
            if (_cteService.VerificarLiberacaoTravasCorrentista(_fluxoComercial))
            {
                // Caso seja fluxo brado, ent�o recupera o REMETENTE e DESTINATARIO da nota fiscal.
                INotaFiscalEletronica nota = _nfeService.ObterDadosNfeBancoDados(_listaCteComplementadoDetalhe[0].ChaveNfe);

                if (nota != null)
                {
                    // Configura o objeto Empresa Remetente
                    empresaRemetente = new EmpresaCliente();
                    empresaRemetente.TipoPessoa = TipoPessoaEnum.Juridica;
                    empresaRemetente.Cgc = nota.CnpjEmitente;
                    empresaRemetente.InscricaoEstadual = nota.InscricaoEstadualEmitente;
                    empresaRemetente.RazaoSocial = nota.RazaoSocialEmitente;
                    empresaRemetente.NomeFantasia = nota.NomeFantasiaEmitente;
                    empresaRemetente.Telefone1 = nota.TelefoneEmitente;
                    empresaRemetente.Endereco = nota.LogradouroEmitente;
                    empresaRemetente.Cep = nota.CepEmitente;

                    // Tenta pega pelo codigo do municipio, caso nao venha, entao recupera pelo nome do municipio
                    if (int.TryParse(nota.CodigoMunicipioEmitente, out aux))
                    {
                        empresaRemetente.CidadeIbge = _cidadeIbgeRepository.ObterPorCodigoMunicipioIbge(aux);
                        _cidadeIbgeRemetenteFiscal = empresaRemetente.CidadeIbge;
                        if (_cidadeIbgeRemetenteFiscal.CodigoIbge == 9999999)
                        {
                            PaisBacen paisAux = _paisBacenRepository.ObterPorCodigoPais(nota.CodigoPaisEmitente);
                            if (paisAux == null)
                            {
                                if (int.TryParse(nota.PaisEmitente, out aux))
                                {
                                    paisAux = _paisBacenRepository.ObterPorCodigoPais(nota.PaisEmitente);
                                }
                                else
                                {
                                    paisAux = _paisBacenRepository.ObterPorDescricaoPais(nota.PaisEmitente);
                                }
                            }

                            if (paisAux == null)
                            {
                                _empresaContratoRemetenteFiscal = null;
                            }
                            else
                            {
                                empresaRemetente.EnderecoSap = paisAux.SiglaResumida;
                            }
                        }
                    }
                    else
                    {
                        if (nota.MunicipioDestinatario != null)
                        {
                            empresaRemetente.CidadeIbge =
                                _cidadeIbgeRepository.ObterPorDescricaoUf(nota.MunicipioDestinatario.ToUpper(),
                                    nota.UfDestinatario);
                            _cidadeIbgeRemetenteFiscal = empresaRemetente.CidadeIbge;
                        }
                        else
                        {
                            if (nota.UfDestinatario == "EX")
                            {
                                _cidadeIbgeRemetenteFiscal = _cidadeIbgeRepository.ObterPorCodigoMunicipioIbge(9999999);
                                empresaRemetente.CidadeIbge = _cidadeIbgeRemetenteFiscal;
                            }
                        }
                    }

                    _empresaContratoRemetenteFiscal = empresaRemetente;
                }
                else
                {
                    _empresaContratoRemetenteFiscal = _cteComplementar.FluxoComercial.Contrato.EmpresaRemetenteFiscal;
                    _cidadeIbgeRemetenteFiscal = _cteComplementar.FluxoComercial.Contrato.EmpresaRemetenteFiscal.CidadeIbge;
                }
            }
            else
            {
                _empresaContratoRemetenteFiscal = _cteComplementar.FluxoComercial.Contrato.EmpresaRemetenteFiscal;
                _cidadeIbgeRemetenteFiscal = _cteComplementar.FluxoComercial.Contrato.EmpresaRemetenteFiscal.CidadeIbge;
            }
        }

        /// <summary>
        /// Obtem os dados da empresa destinataria fiscal
        /// </summary>
        private void ObterEmpresaDestinatariaFiscal()
        {
            IEmpresa empresaDestinataria;
            int aux;

            // Verifica se � um Fluxo da Brado
            if (_cteService.VerificarLiberacaoTravasCorrentista(_fluxoComercial))
            {
                // Caso seja fluxo brado, ent�o recupera o REMETENTE e DESTINATARIO da nota fiscal.
                INotaFiscalEletronica nota = _nfeService.ObterDadosNfeBancoDados(_listaCteComplementadoDetalhe[0].ChaveNfe);

                if (nota != null)
                {
                    // Configura o objeto Empresa Destinatario
                    empresaDestinataria = new EmpresaCliente();
                    empresaDestinataria.TipoPessoa = TipoPessoaEnum.Juridica;
                    empresaDestinataria.Cgc = nota.CnpjDestinatario;
                    empresaDestinataria.InscricaoEstadual = nota.InscricaoEstadualDestinatario;
                    empresaDestinataria.RazaoSocial = nota.RazaoSocialDestinatario;
                    empresaDestinataria.NomeFantasia = nota.NomeFantasiaDestinatario;
                    empresaDestinataria.Telefone1 = nota.TelefoneDestinatario;
                    empresaDestinataria.Endereco = nota.LogradouroDestinatario;
                    empresaDestinataria.Cep = nota.CepDestinatario;

                    // Tenta pega pelo codigo do municipio, caso nao venha, entao recupera pelo nome do municipio
                    if (int.TryParse(nota.CodigoMunicipioDestinatario, out aux))
                    {
                        empresaDestinataria.CidadeIbge = _cidadeIbgeRepository.ObterPorCodigoMunicipioIbge(aux);
                        _cidadeIbgeDestinatariaFiscal = empresaDestinataria.CidadeIbge;
                        if (_cidadeIbgeDestinatariaFiscal.CodigoIbge == 9999999)
                        {
                            PaisBacen paisAux = _paisBacenRepository.ObterPorCodigoPais(nota.CodigoPaisDestinatario);
                            if (paisAux == null)
                            {
                                if (int.TryParse(nota.PaisDestinatario, out aux))
                                {
                                    paisAux = _paisBacenRepository.ObterPorCodigoPais(nota.PaisDestinatario);
                                }
                                else
                                {
                                    paisAux = _paisBacenRepository.ObterPorDescricaoPais(nota.PaisDestinatario);
                                }
                            }

                            if (paisAux == null)
                            {
                                _empresaContratoDestinatariaFiscal = null;
                            }
                            else
                            {
                                empresaDestinataria.EnderecoSap = paisAux.SiglaResumida;
                            }
                        }
                    }
                    else
                    {
                        if (nota.MunicipioDestinatario != null)
                        {
                            empresaDestinataria.CidadeIbge = _cidadeIbgeRepository.ObterPorDescricaoUf(nota.MunicipioDestinatario.ToUpper(), nota.UfDestinatario);
                            _cidadeIbgeDestinatariaFiscal = empresaDestinataria.CidadeIbge;
                        }
                        else
                        {
                            if (nota.UfDestinatario == "EX")
                            {
                                _cidadeIbgeDestinatariaFiscal = _cidadeIbgeRepository.ObterPorCodigoMunicipioIbge(9999999);
                                empresaDestinataria.CidadeIbge = _cidadeIbgeDestinatariaFiscal;
                            }
                        }
                    }

                    _empresaContratoDestinatariaFiscal = empresaDestinataria;
                }
                else
                {
                    _empresaContratoDestinatariaFiscal = _cteComplementar.FluxoComercial.Contrato.EmpresaDestinatariaFiscal;
                    _cidadeIbgeDestinatariaFiscal = _cteComplementar.FluxoComercial.Contrato.EmpresaDestinatariaFiscal.CidadeIbge;
                }
            }
            else
            {
                _empresaContratoDestinatariaFiscal = _cteComplementar.FluxoComercial.Contrato.EmpresaDestinatariaFiscal;
                _cidadeIbgeDestinatariaFiscal = _cteComplementar.FluxoComercial.Contrato.EmpresaDestinatariaFiscal.CidadeIbge;
            }
        }

        private void LimparDados()
        {
            _cteRaiz = null;
            _cteRaizAgrupamento = null;
            _listaCteComplementado = null;
            _fluxoComercial = null;
            _vagao = null;
            _cteLogService = null;
            _cidadeIbgeEstacaoMaeOrigemFluxoComercial = null;
            _municipioEstacaoMaeOrigemFluxoComercial = null;
            _estadoEstacaoMaeOrigemFluxoComercial = null;
            _municipioEstacaoMaeDestinoFluxoComercial = null;
            _estadoEstacaoMaeDestinoFluxoComercial = null;
            _cidadeIbgeEstacaoMaeDestinoFluxoComercial = null;
            _empresaOperadoraEstacaoMaeOrigemFluxoComercial = null;
            _estacaoMaeOrigemFluxoComercial = null;
            _estacaoMaeDestinoFluxoComercial = null;
            _empresaRemetenteFluxoComercial = null;
            _empresaDestinatariaFluxoComercial = null;
            _nfe03Filial = null;
            _estadoEmpresaRemetenteFluxoComercial = null;
            _serieDespachoUfCteComplementado = null;
            _contratoHistoricoCteComplementado = null;
            _cteCteComplementado = null;
        }

        /// <summary>
        /// Obtem a sigla do pais pelo SAP
        /// </summary>
        /// <param name="empresa">Empresa utilizada para se obter o Pais</param>
        /// <returns>Retorna a sigla do Pais</returns>
        private string ObterSiglaPaisExteriorPeloSap(IEmpresa empresa)
        {
            if (empresa.EnderecoSap != null)
            {
                string mask = "- ";
                string linhaPais = empresa.EnderecoSap;
                string[] dados = linhaPais.Split(mask.ToCharArray());
                return dados[dados.Length - 1];
            }

            if (empresa.CidadeIbge != null)
            {
                CidadeIbge cidadeIbge = empresa.CidadeIbge;
                return cidadeIbge.SiglaPais;
            }

            return string.Empty;
        }

        private IList<ComposicaoFreteCteInterno> ObterListaComposicaoFreteCte(CteComplementado cteComplementado, ref double pesoParaCalculo)
        {
            ComposicaoFreteCteInterno composicaoInterno = null;

            IList<ComposicaoFreteCteInterno> listaRetorno = new List<ComposicaoFreteCteInterno>();

            IList<ComposicaoFreteCte> listaComposicaoFreteCte = _composicaoFreteCteRepository.ObterComposicaoFreteCtePorCte(cteComplementado.Cte);

            // Encontra o valor total por tonelada
            ComposicaoFreteCte totalPorTonelada = listaComposicaoFreteCte.FirstOrDefault(g => g.CondicaoTarifa == "ICMI");
            double coeficienteRateio = cteComplementado.ValorDiferenca / totalPorTonelada.ValorSemIcms.Value;

            pesoParaCalculo = coeficienteRateio * cteComplementado.Cte.PesoParaCalculo.Value;

            foreach (ComposicaoFreteCte composicaoFreteCte in listaComposicaoFreteCte)
            {
                composicaoInterno = new ComposicaoFreteCteInterno();
                composicaoInterno.ComposicaoFreteCte = composicaoFreteCte;
                composicaoInterno.ValorRateado = Math.Round(coeficienteRateio * composicaoFreteCte.ValorSemIcms.Value, 2);

                listaRetorno.Add(composicaoInterno);
            }

            return listaRetorno;
        }

        private int GetUtilizaToma()
        {
            // -> REGRA ANTIGA CANCELADA, CONFORME ATIVIDADE http://jira/browse/TL-1664 E ACORDADA EM E-MAIL PELO Jodicler Fistarol - Gerencia Tribut�ria - Superintend�ncia Financeira (28/12/2012)
            /////*
            ////Regra do frete 0-Pago, 1-A Pagar e 2-Outros
            ////Frete Pago: Quando o tomador de servico (FluxoComercial.EmpresaPagadora) igual ao Remetente (FluxoComercial.EmpresaRemetente)
            ////Frete a pagar: Quando o tomador de servico(FluxoComercial.EmpresaPagadora) igual ao Destinatario (FluxoComercial.EmpresaDestinataria)
            ////Caso o tomador seja diferente tanto do Remetente como do Destinatario o frete � Outros (03/05/2012)
            ////Obs:
            //// */
            ////if (_empresaTomadora.Id == _empresaRemetenteFluxoComercial.Id)
            ////{
            ////    // Forma de pagamento. Quando o tomador de servi�o � Remetente, a Forma de Pagamento � Pago(0).
            ////    cte01.IdeForPag = "0";
            ////}
            ////else if (_empresaTomadora.Id == _empresaDestinatariaFluxoComercial.Id)
            ////{
            ////    // Forma de pagamento. Quando o tomador de servi�o n�o � o Remetente, a Forma de Pagamento � A Pagar(1).
            ////    cte01.IdeForPag = "1";
            ////}
            ////else
            ////{
            ////    // Forma de pagamento. Quando o tomador de servi�o � Remetente, a Forma de Pagamento � Outros(2).
            ////    cte01.IdeForPag = "2";
            ////}

            /*
                -> NOVA REGRA, CONFORME ATIVIDADE http://jira/browse/TL-1664 E ACORDADA EM E-MAIL PELO Jodicler Fistarol - Gerencia Tribut�ria - Superintend�ncia Financeira (28/12/2012)
                Regra do frete 0-Pago, 1-A Pagar e 2-Outros
                Frete Pago: Quando o tomador de servico (FluxoComercial.EmpresaPagadora) igual ao Remetente FISCAL (FluxoComercial.EmpresaRemetente)
                Frete a pagar: Quando o tomador de servico(FluxoComercial.EmpresaPagadora) igual ao Destinatario FISCAL (FluxoComercial.EmpresaDestinataria)
                Caso o tomador seja diferente tanto do Remetente como do Destinatario o frete � Outros (03/05/2012)
                Obs:                
                 */

            //// a tag IdeForPag se tornou obsoleta na vers�o 3.0
            ////if (_empresaTomadora.Id == _empresaContratoRemetenteFiscal.Id)
            ////{
            ////    // Forma de pagamento. Quando o tomador de servi�o � Remetente, a Forma de Pagamento � Pago(0).
            ////    cte01.IdeForPag = "0";
            ////}
            ////else if (_empresaTomadora.Id == _empresaContratoDestinatariaFiscal.Id)
            ////{
            ////    // Forma de pagamento. Quando o tomador de servi�o n�o � o Remetente, a Forma de Pagamento � A Pagar(1).
            ////    cte01.IdeForPag = "1";
            ////}
            ////else
            ////{
            ////    // Forma de pagamento. Quando o tomador de servi�o � Remetente, a Forma de Pagamento � Outros(2).
            ////    cte01.IdeForPag = "2";
            ////}


            // if ((_cte.Id == _cteRaizAgrupamento.Id) && (_tipoTrafegoMutuo == TipoTrafegoMutuoEnum.FaturamentoEmpresaGrupo) && (! _cteService.VerificarLiberacaoTravasCorrentista(_fluxoComercial)))
            // 
            // {
            // Primeiro CTE do agrupamento e o tipo do trafego � FaturamentoEmpresaGrupo
            // Dados do tomador do servi�o. 0 - Remetente
            //	cte01.Toma03Toma = 0;
            // }
            // else
            // {
            // Nesse caso dever� ser preenchido os dados do Toma04 com os dados da ferrovia de origem
            // por ser um CTE filho ou o tipo de trafego � IntercambioFaturamentoOrigem ou IntercambioFaturamentoDestino

            /* Dados do tomador de servi�o
                Regra do tomador de servi�o caso o tomador seja:
                    0-Remetente
                    1-Expedidor
                    2-Recebedor
                    3-Destinat�rio
                    4-Outros
                 * 
                 * IEmpresa empresaRemetente = _empresaContratoRemetenteFiscal
                 * IEmpresa empresaExpedidor = _empresaRemetenteFluxoComercial;
                 * IEmpresa empresaRecebedor = _empresaDestinatariaFluxoComercial;
                 * IEmpresa empresaDestinataria = _empresaContratoDestinatariaFiscal;
                 * Regra Jodi: Quando o tomador for igual ao remetente fiscal e igual ao expedidor o valor do 
                 *             Toma03 � 0
                 *             Caso o tomador SEJA igual ao recebedor e igual ao destinatario 
                 *             Toma03 � 3
                 *             Brado: Quando for Brado utilizar cte01.Toma4Toma = 4;
                 *             Quandoo CTE tiver uma NFe exterior, n�o pode ser toma3 = 0 (primeiro "if")
             
                 OBS: dia 25/04/2017, foi alterada a regra Jodi para regra Josi. N�o existe mais partiularidade pra brado, conforme a valida��o acima.
                 a regra do tomador vale pra todos, ou seja, se n�o for toma3, entra no if do toma4
             
             */

            // Verifica se � um Fluxo da Brado

            /*	
                 * Regra antiga: Foi alterada para realizar a valida��o na PKG - Estava passando o tomador errado.
                 * if (
                        (_empresaTomadora.Id == _empresaContratoRemetenteFiscal.Id)
                        && (_empresaTomadora.Id == _empresaRemetenteFluxoComercial.Id)
                        && TemNotaExteriorDeEntrada(_cte)
                    )
                    {
                        // -> quando o cte tiver uma nota exterior de entrada, nao pode ser toma3 = 0; tem que ser tome3 = 3
                        cte01.Toma03Toma = 3;
                        utilizaToma3 = true;
                    }
                    else
                 */

            int resultado = -1;

            if ((_empresaTomadora.Id == _empresaContratoRemetenteFiscal.Id) &&
                (_empresaTomadora.Id == _empresaRemetenteFluxoComercial.Id))
            {
                resultado = 0;
            }
            else if ((_empresaTomadora.Id == _empresaDestinatariaFluxoComercial.Id) &&
                     (_empresaTomadora.Id == _empresaContratoDestinatariaFiscal.Id))
            {
                resultado = 3;
            }
            else if (_empresaTomadora.Id == _empresaContratoRemetenteFiscal.Id)
            {
                resultado = 0;
            }
            else if (_empresaTomadora.Id == _empresaContratoDestinatariaFiscal.Id)
            {
                resultado = 3;
            }
            else if (_empresaTomadora.Id == _empresaRemetenteFluxoComercial.Id)
            {
                resultado = 1;
            }
            else if (_empresaTomadora.Id == _empresaDestinatariaFluxoComercial.Id)
            {
                resultado = 2;
            }
            return resultado;
        }

        private void CalcularValorCte()
        {
            var cteComplementado = _listaCteComplementado[0];
            double baseCalculoIcms;
            double valorIcms;
            double aliquota;
            string cst;
            double valorSemIcms;
            if (cteComplementado.TipoComplemento == TipoOperacaoCteEnum.Complemento)
            {
                baseCalculoIcms = cteComplementado.ValorDiferenca;
                aliquota = cteComplementado.Cte.PercentualAliquotaIcms ?? 0.0;

                valorIcms = baseCalculoIcms * ((cteComplementado.Cte.PercentualAliquotaIcms ?? 0.0) / 100);
                cst = cteComplementado.Cte.ContratoHistorico.Cst;
            }
            else
            {
                if (cteComplementado.Cte.ValorIcms.HasValue)
                {
                    valorSemIcms = cteComplementado.Cte.ValorCte - cteComplementado.Cte.ValorIcms.Value;
                }
                else
                {
                    valorSemIcms = cteComplementado.Cte.ValorCte;
                }

                aliquota = cteComplementado.Cte.FluxoComercial.Contrato.PercentualAliquotaIcms ?? 0.0;

                baseCalculoIcms = valorSemIcms * (100 / (100 - aliquota));
                valorIcms = cteComplementado.ValorDiferenca;
                // valorIcms = baseCalculoIcms * (aliquota / 100);

                var contrato = _contratoHistoricoRepository.ObterPorContrato(cteComplementado.Cte.FluxoComercial.Contrato);

                if (contrato == null || !contrato.Any())
                {
                    throw new Exception("Contrato historico n�o localizado");
                }

                ContratoHistorico contratoHist = contrato.FirstOrDefault(x => x.Id == contrato.Max(f => f.Id));
                cst = contratoHist.Cst;
            }

            _cteService.AtualizaIcmsCte(_cteComplementar, aliquota, baseCalculoIcms, valorIcms, cteComplementado.ValorDiferenca);
        }

        private string ObterMensagemCteComplementado()
        {
            string retorno = string.Empty;
            // Verifica se � um Cte de complemento ou um Cte de TakeOrPay. Caso tenha apenas um item na lista � um Cte de complemento
            if (_listaCteComplementado.Count > 0)
            {
                // retorno = string.Format("Complemento de ICMS relativo ao Dacte n� {0}-{1} emitido em {2}, chave de acesso n�{3}??", _listaCteComplementado[0].Cte.Serie, _listaCteComplementado[0].Cte.Numero, _listaCteComplementado[0].Cte.DataHora, _listaCteComplementado[0].Cte.Chave);
                retorno = string.Format("Chave CT-e COMPLEMENTADOS:\\n{0}", _listaCteComplementado[0].Cte.Chave);
                if (_listaCteComplementado[0].PesoDiferenca > 0)
                {
                    retorno += String.Format(" CTE COMPLEMENTAR REFERENTE A DIFEREN�A DE: {0} TONELADAS, NO VALOR TOTAL DE: {1}", _listaCteComplementado[0].PesoDiferenca.ToString(), String.Format("{0:C}", _listaCteComplementado[0].ValorDiferenca));
                }
            }

            return retorno;
        }

        private IEmpresa ObterEmpresaFerrovia(Contrato contrato)
        {
            // Obter a sigla da empresa que fatura
            string siglaFerrovia = ALL.Core.Util.Enum<FerroviaFaturamentoEnum>.GetDescriptionOf(contrato.FerroviaFaturamento.Value);

            IEmpresa empresaFerrovia = _empresaFerroviaRepository.ObterPorSiglaUf(siglaFerrovia, _cteRaizAgrupamento.SiglaUfFerrovia);

            if (empresaFerrovia == null)
            {
                empresaFerrovia = _empresaInterfaceCteRepository.ObterPorSiglaUF(siglaFerrovia, _cteRaizAgrupamento.SiglaUfFerrovia);
            }

            // Pega pela esta��o de Origem 
            if (empresaFerrovia == null)
            {
                if (contrato.FerroviaFaturamento.Value == FerroviaFaturamentoEnum.Argentina)
                {
                    empresaFerrovia = _fluxoComercial.Origem.EmpresaConcessionaria;
                }
            }

            return empresaFerrovia;
        }

        private DateTime GetDhEmissao()
        {
            var restricoes = new[] { "MT", "MS" };

            if (restricoes.Contains(_cteComplementar.SiglaUfFerrovia))
            {
                return _cteComplementar.DataHora.AddHours(-1);
            }

            return _cteComplementar.DataHora;
        }

        private double CalcularValorTotalNotaFiscal()
        {
            return _listaCteComplementado[0].Cte.ListaDetalhes.Sum(g => g.ValorNotaFiscal);
        }

        /// <summary>
        /// Obt�m o numero e s�rie do despacho para os Ctes TakeOrPay ou Complementos - Utilizado no cancelamento
        /// </summary>
        /// <param name="chaveCte">N�mero da chave do Cte</param>
        /// <param name="serieDespachoSap">S�rie do despacho gerado pelo SAP</param>
        /// <param name="numeroDespachoSap">Numero do despacho gerado pelo SAP</param>
        private void ObterNumeroSerieDespachoGeradoPeloSap(string chaveCte, out string serieDespachoSap, out int numeroDespachoSap)
        {
            serieDespachoSap = string.Empty;
            numeroDespachoSap = 0;

            var sapChaveCteAux = _sapChaveCteRepository.ObterPorChaveCte(chaveCte);
            if (sapChaveCteAux != null)
            {
                serieDespachoSap = sapChaveCteAux.SapDespCte.SerieDespachoSap;
                numeroDespachoSap = sapChaveCteAux.SapDespCte.NumeroDespachoSap;
            }
        }

        private string ObterMensagemDataDescarga()
        {
            //if (_cteComplementar.ContratoHistorico == null)
            //{
            //    _cteComplementar.ContratoHistorico = _listaCteComplementado[0].Cte.ContratoHistorico;
            //}

            if (_despachoTranslogicRepository.VerificaPrecificacaoOrigem(_cteComplementar.ContratoHistorico.Id))
            {
                string serieDespachoSap = string.Empty;
                int numeroDespachoSap = 0;
                ObterNumeroSerieDespachoGeradoPeloSap(_cteComplementar.Chave, out serieDespachoSap, out numeroDespachoSap);
                return _despachoTranslogicRepository.ObterMensagemDataDespacho(numeroDespachoSap, serieDespachoSap);
            }
            return null;
        }

    }

    /// <summary>
    /// Classe interna de contrato frete cte responsavel pelo armazenamento das informa��es de contrato frete cte
    /// </summary>
    internal class ComposicaoFreteCteInterno
    {
        /// <summary>
        /// Composi��o do frete do CTE
        /// </summary>
        public ComposicaoFreteCte ComposicaoFreteCte { get; set; }

        /// <summary>
        /// Valor rateado
        /// </summary>
        public double ValorRateado { get; set; }
    }
}
