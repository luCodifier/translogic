﻿namespace Translogic.Modules.Core.Domain.Services.Ctes
{
    using System.Collections.Generic;
    using System.Linq;
    using System.ServiceModel;
    using System.ServiceModel.Activation;
    using Interface;
    using Model.CteRodoviario.Outputs;
    using Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;
    using Translogic.Modules.Core.Domain.Model.CteRodoviario;

    /// <summary>
    /// Servico de Ctes Rodoviarios
    /// </summary>
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceBehavior]
    public class CteRodoviarioService : ICteRodoviarioService
    {
        private readonly ICteRodoviarioRepository _cteRodoviarioRepository;
        private readonly ICteRodoviarioNotasRepository _cteRodoviarioNotasRepository;

        /// <summary>
        /// Construtor do Servico
        /// </summary>
        /// <param name="cteRodoviarioRepository">Repositorio de ctes rodoviarios</param>
        /// <param name="cteNotasRepository">Repositorio de nodas dos ctes rodoviarios</param>
        public CteRodoviarioService(ICteRodoviarioRepository cteRodoviarioRepository, ICteRodoviarioNotasRepository cteNotasRepository)
        {
            _cteRodoviarioRepository = cteRodoviarioRepository;
            _cteRodoviarioNotasRepository = cteNotasRepository;
        }

        /// <summary>
        /// Metodo que retorna os dados das ctes de acordo com o objeto de input
        /// </summary>
        /// <param name="request">Objeto do tipo InformationElectronicBillofLadingHighwayRequest com os dados de entrada</param>
        /// <returns>Retorna objeto de output InformationElectronicBillofLadingHighwayResponse</returns>
        public InformationElectronicBillofLadingHighwayResponse InformationCteRodoviario(InformationElectronicBillofLadingHighwayRequest request)
        {
            string mensagem;
            var dados = new List<ElectronicBillofLadingData>(); 

            if (request.InformatioElectronicBillofLadingHighway.InformationElectronicBillofLadingHighwayType.ElectronicBillofLadingHighwayDetail.StartDate > request.InformatioElectronicBillofLadingHighway.InformationElectronicBillofLadingHighwayType.ElectronicBillofLadingHighwayDetail.EndDate)
            {
                mensagem = "ERRO-003: Data Inicial deve ser menor que a data Final";
            }
            else
            {
                string dataFim;

                if ((request.InformatioElectronicBillofLadingHighway.InformationElectronicBillofLadingHighwayType.ElectronicBillofLadingHighwayDetail.EndDate - request.InformatioElectronicBillofLadingHighway.InformationElectronicBillofLadingHighwayType.ElectronicBillofLadingHighwayDetail.StartDate).TotalDays > 30)
                {
                    dataFim =
                       request.InformatioElectronicBillofLadingHighway.InformationElectronicBillofLadingHighwayType.ElectronicBillofLadingHighwayDetail.StartDate.
                            AddDays(30).ToShortDateString();
                }
                else
                {
                    dataFim =
                         request.InformatioElectronicBillofLadingHighway.InformationElectronicBillofLadingHighwayType.ElectronicBillofLadingHighwayDetail.EndDate.
                             ToShortDateString();
                }

                string dataIni =
                 request.InformatioElectronicBillofLadingHighway.InformationElectronicBillofLadingHighwayType.ElectronicBillofLadingHighwayDetail.StartDate.
                        ToShortDateString();

                mensagem = "PESQUISA REALIZADA ENTRE OS DIAS " + dataIni + " E " + dataFim;

                dados = _cteRodoviarioRepository.InformationCtesRodoviarios(request)
                    .GroupBy(a => a.DtoWaybillInvoiceKey)
                    .Select(a => new ElectronicBillofLadingData
                    {
                        ElectronicBillofLadingId = a.First().ElectronicBillofLadingId,
                        CustomsBase = a.First().CustomsBase,
                        DescriptionCustomsBase = a.First().DescriptionCustomsBase,
                        DateDischarge = a.First().DateDischarge,
                        ElectronicBillofLadingKey = a.First().ElectronicBillofLadingKey,
                        WaybillInformation = a.Select(b => new WaybillInformation
                        {
                            DeclaredWeight = b.DtoDeclaredWeight,
                            WaybillInvoiceKey = b.DtoWaybillInvoiceKey,
                            MeasuredWeight = b.DtoMeasuredWeight,
                            WeightDifference = b.DtoWeightDifference
                        }).ToList()
                    })
                     .ToList();
            }

            var response = new InformationElectronicBillofLadingHighwayResponse
            {
                InformationElectronicBillofLadingHighwayOutput = new InformationElectronicBillofLadingHighwayOutput
                {
                    InformationElectronicBillofLadingHighwayType = new InformationElectronicBillofLadingHighwayType
                    {
                        VersionIdentifier = "1.0",
                        ElectronicBillofLadingHighwayDetail = new ElectronicBillofLadingHighwayDetail
                        {
                            ElectronicBillofLadingData = dados
                        },  ReturnMessage = mensagem
                    }
                }
            };

            return response;
        }
    }
}