﻿namespace Translogic.Modules.Core.Domain.Services.Ctes
{
    using System;

    using Translogic.Modules.Core.Domain.Model.CteFerroviarioCancelado;
    using Translogic.Modules.Core.Domain.Model.CteFerroviarioCancelado.Inputs;
    using Translogic.Modules.Core.Domain.Model.CteFerroviarioCancelado.Outputs;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;
    using Translogic.Modules.Core.Domain.Model.WebServices.CteFerroviarioCancelado.Outputs;
    using Translogic.Modules.Core.Domain.Services.Ctes.Interface;

    public class CteCanceladoRefaturamentoService : ICteCanceladoRefaturamentoService
    {
        #region Fields

        private readonly ICteCanceladoNotasRepository _cteCanceladoNotasRepository;

        private readonly ICteCanceladoRefaturamentoRepository _cteCanceladoRefaturamentoRepository;

        private readonly ICteFerroviarioCanceladoService _cteFerroviarioCanceladoService;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///     contrutor da classe
        /// </summary>
        /// <param name="cteCanceladoRefaturamentoRepository">objeto instanciado</param>
        /// <param name="cteCanceladoNotasRepository">objeto instanciado</param>
        /// <param name="cteFerroviarioCanceladoService">objeto instanciado</param>
        public CteCanceladoRefaturamentoService(
            ICteCanceladoRefaturamentoRepository cteCanceladoRefaturamentoRepository,
            ICteCanceladoNotasRepository cteCanceladoNotasRepository,
            ICteFerroviarioCanceladoService cteFerroviarioCanceladoService)
        {
            this._cteCanceladoRefaturamentoRepository = cteCanceladoRefaturamentoRepository;
            this._cteCanceladoNotasRepository = cteCanceladoNotasRepository;
            this._cteFerroviarioCanceladoService = cteFerroviarioCanceladoService;
        }

        #endregion

        #region Public Methods and Operators

        public bool InserirCteCanceladoRefaturamento()
        {
            CteCanceladoRefaturamento ultimoCteCanceladoRefaturamento = this._cteCanceladoRefaturamentoRepository.ObterUltimoCteCanceladoRefaturamentoBaseConsolidada();

            DateTime dataInicio = ultimoCteCanceladoRefaturamento.Data.AddDays(1);
            DateTime dataFim = dataInicio.AddDays(1);

            while (DateTime.Today > dataInicio)
            {
                var request = new EletronicBillofLadingCancelledRailRequest
                                  {
                                      ElectronicBillofLadingCancelledRailType
                                          =
                                          new ElectronicBillofLadingCancelledRailType
                                              {
                                                  ElectronicBillofLadingCancelledRailDetail
                                                      =
                                                      new ElectronicBillofLadingCancelledRailDetail
                                                          {
                                                              EndDate
                                                                  =
                                                                  dataFim,
                                                              StartDate
                                                                  =
                                                                  dataInicio
                                                          }
                                              }
                                  };

                try
                {
                    EletronicBillofLadingCancelledRailResponse response = this._cteFerroviarioCanceladoService.ObterCtesCanceladosParaRefaturamento(request);

                    foreach (EletronicBillofLadingCancelledData reg in
                        response.EletronicBillofLadingCancelledRailType.EletronicBillOfLadingCancelledRailDetail
                            .EletronicBillofLadingCancelledData)
                    {
                        var cteCanceladoRefaturamento = new CteCanceladoRefaturamento();
                        cteCanceladoRefaturamento.CteCanceladoChave = reg.CteCancelledKey;
                        cteCanceladoRefaturamento.CteCanceladoId = reg.CteCancelledId;
                        cteCanceladoRefaturamento.CteNovoChave = reg.CteLadingKey;
                        cteCanceladoRefaturamento.CteNovoId = reg.CteLadingId;
                        cteCanceladoRefaturamento.Data = dataInicio;

                        this._cteCanceladoRefaturamentoRepository.InserirOuAtualizar(cteCanceladoRefaturamento);

                        foreach (WayBillsInfo notaCancelada in reg.WayBillsInfoCancelled)
                        {
                            var cteCanceladoNotas = new CteCanceladoNotas
                                                        {
                                                            CteCanceladoRefaturamento = cteCanceladoRefaturamento,
                                                            NotaCanceladaChave = notaCancelada.WaybillKey,
                                                            NotaCanceladaId = notaCancelada.WaybillId
                                                        };

                            this._cteCanceladoNotasRepository.InserirOuAtualizar(cteCanceladoNotas);
                        }

                        foreach (WayBillsInfo nota in reg.WayBillsInfoLading)
                        {
                            var cteNotas = new CteCanceladoNotas
                                               {
                                                   CteCanceladoRefaturamento = cteCanceladoRefaturamento,
                                                   NotaChave = nota.WaybillKey,
                                                   NotaId = nota.WaybillId
                                               };

                            this._cteCanceladoNotasRepository.InserirOuAtualizar(cteNotas);
                        }
                    }
                }
                catch (Exception ex)
                {
                    return false;
                }

                dataInicio = dataInicio.AddDays(1);
                dataFim = dataInicio.AddDays(1);
            }

            return true;
        }

        #endregion
    }
}