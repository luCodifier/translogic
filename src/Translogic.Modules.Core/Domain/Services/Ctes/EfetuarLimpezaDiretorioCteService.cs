namespace Translogic.Modules.Core.Domain.Services.Ctes
{
	using System;
	using System.IO;
	using Castle.Services.Transaction;
	using Model.Codificador;
	using Model.Codificador.Repositories;

	/// <summary>
	/// Classe para efetuar limpeza do diret�rio Cte
	/// </summary>
	[Transactional]
	public class EfetuarLimpezaDiretorioCteService
	{
		private readonly IConfiguracaoTranslogicRepository _configuracaoTranslogicRepository;

		private const string ChaveArquivosPdf = "CTE_CAMINHO_ARQUIVOS";
		private const string ChaveDiasExcluirArquivo = "CTE_DIAS_EXCLUIR_ARQUIVOS";
		private string _pathDiretorio = string.Empty;

		/// <summary>
		/// Construtor EfetuarLimpezaDiretorioCteService
		/// </summary>
		/// <param name="configuracaoTranslogicRepository"> Reposit�rio de Configura��es do Translogic</param>
		public EfetuarLimpezaDiretorioCteService(IConfiguracaoTranslogicRepository configuracaoTranslogicRepository)
		{
			_configuracaoTranslogicRepository = configuracaoTranslogicRepository;
		}

		/// <summary>
		/// Executa a grava��o do arquivo de envio
		/// </summary>
		public void Executar()
		{
			int numDias = 0;

			try
			{
				//// Recupera o diret�rio da Conf Geral.
				//_pathDiretorio = RecuperarValorConfGeral(ChaveArquivosPdf);

				//// Recupera o numero de dias que ir� manter o arquivo.
				//int.TryParse(RecuperarValorConfGeral(ChaveDiasExcluirArquivo), out numDias);

				//// Chama a rotina para limpar o diret�rio
				//LimparDiretorio(_pathDiretorio, numDias);
			}
			catch (Exception ex)
			{
				throw;
			}
		}

		/// <summary>
		/// M�todo recursivo para efetuar a limpeza dos arquivos PDF e XML dos Cte
		/// </summary>
		/// <param name="path">Caminho do Diret�rio a ser executada a limpeza</param>
		/// <param name="numDias"> Numero de dias</param>
		private void LimparDiretorio(string path, int numDias)
		{
			// Declara��o de Variaveis
			DirectoryInfo diretorio;
			DirectoryInfo[] listaDiretorio;
			FileInfo[] arquivosPdf;
			FileInfo[] arquivosXml;

			try
			{
				// Recupera os diretorios
				diretorio = new DirectoryInfo(path);

				// Recupera os subdiretorios
				listaDiretorio = diretorio.GetDirectories();

				// Recupera os Arquivos
				arquivosPdf = diretorio.GetFiles("*.pdf", SearchOption.TopDirectoryOnly);
				arquivosXml = diretorio.GetFiles("*.xml", SearchOption.TopDirectoryOnly);

				foreach (DirectoryInfo directoryInfo in listaDiretorio)
				{
					// Se possuir sub-diretorio, ent�o chama o metodo novamente para excluir os arquivos internos.
					LimparDiretorio(Path.Combine(path, directoryInfo.Name), numDias);
				}

				// Efetua a limpeza dos PDF
				VerificarArquivosDiretorios(arquivosPdf, numDias);

				// Efetua a limpeza dos XML
				VerificarArquivosDiretorios(arquivosXml, numDias);

				FileInfo[] files = diretorio.GetFiles();

				if (files.Length == 1)
				{
					if (files[0].Name.ToUpper().Equals("THUMBS.DB"))
					{
						File.Delete(files[0].FullName);
					}
				}

				// Exclui o diret�rio se n�o possuir mais arquivo
				if (diretorio.GetFiles().Length == 0 && diretorio.GetDirectories().Length == 0 && diretorio.FullName != _pathDiretorio)
				{
					Directory.Delete(diretorio.FullName);
				}
			}
			catch (Exception ex)
			{
				throw;
			}
		}

		/// <summary>
		/// Verifica o diret�rio e efetua a exclus�o dos arquivos
		/// </summary>
		/// <param name="arquivos">Array de arquivos</param>
		/// <param name="numDias"> Dias limite para exclus�o</param>
		private void VerificarArquivosDiretorios(FileInfo[] arquivos, int numDias)
		{
			// Percorre todos os arquivos da pasta
			foreach (FileInfo fileInfo in arquivos)
			{
				// Verifica se a diferen�a de dias � maior que a data configurada
				var diferenca = DateTime.Now.Subtract(fileInfo.CreationTime);

				if (diferenca.TotalDays > numDias)
				{
					fileInfo.Delete();
				}
			}
		}

		/// <summary>
		/// Recupera o valor da configura��o cadastrado na Conf Geral
		/// </summary>
		/// <param name="chave">Chave da confGeral</param>
		/// <returns>Retorna o valor cadastrado na tabela confGeral</returns>
		private string RecuperarValorConfGeral(string chave)
		{
			ConfiguracaoTranslogic pathDiretorio = _configuracaoTranslogicRepository.ObterPorId(chave);

			return pathDiretorio != null ? pathDiretorio.Valor : string.Empty;
		}
	}
}