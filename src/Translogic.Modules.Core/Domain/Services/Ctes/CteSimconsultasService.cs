﻿namespace Translogic.Modules.Core.Domain.Services.Ctes
{
	using System;
	using System.Collections.Generic;
	using System.Globalization;
	using System.Xml;

	using Castle.Services.Transaction;

	using Translogic.Modules.Core.Domain.Model.Codificador;
	using Translogic.Modules.Core.Domain.Model.Codificador.Repositories;
	using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Simconsultas.Ctes;
	using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Simconsultas.Ctes.Repositories;
	using Translogic.Modules.Core.Interfaces.Simconsultas;
	using Translogic.Modules.Core.Interfaces.Simconsultas.Interfaces;

	/// <summary>
	/// Classe de serviço do CTe baixado pelo simconsultas
	/// </summary>
	[Transactional]
	public class CteSimconsultasService
	{
		private readonly ISimconsultasService _simconsultasService;
		private readonly ICteSimconsultasRepository _cteSimconsultasRepository;
		private readonly ICteDocumentoRemetenteSimconsultasRepository _cteDocumentoRemetenteSimconsultasRepository;
		private readonly ICteComponentePrestacaoSimconsultasRepository _cteComponentePrestacaoSimconsultasRepository;
		private readonly ICteEmpresaSimconsultasRepository _cteEmpresaSimconsultasRepository;
		private readonly IConfiguracaoTranslogicRepository _configuracaoTranslogicRepository;

		/// <summary>
		/// Initializes a new instance of the <see cref="CteSimconsultasService"/> class.
		/// </summary>
		/// <param name="simconsultasService"> The simconsultas service. </param>
		/// <param name="cteSimconsultasRepository"> The cte simconsultas repository. </param>
		/// <param name="cteDocumentoRemetenteSimconsultasRepository"> The cte documento remetente simconsultas repository. </param>
		/// <param name="cteComponentePrestacaoSimconsultasRepository"> The cte componente prestacao simconsultas repository. </param>
		/// <param name="cteEmpresaSimconsultasRepository"> The cte empresa simconsultas repository. </param>
		/// <param name="configuracaoTranslogicRepository">Repositorio de configuracao translogic injetado</param>
		public CteSimconsultasService(ISimconsultasService simconsultasService, ICteSimconsultasRepository cteSimconsultasRepository, ICteDocumentoRemetenteSimconsultasRepository cteDocumentoRemetenteSimconsultasRepository, ICteComponentePrestacaoSimconsultasRepository cteComponentePrestacaoSimconsultasRepository, ICteEmpresaSimconsultasRepository cteEmpresaSimconsultasRepository, IConfiguracaoTranslogicRepository configuracaoTranslogicRepository)
		{
			_simconsultasService = simconsultasService;
			_cteSimconsultasRepository = cteSimconsultasRepository;
			_cteDocumentoRemetenteSimconsultasRepository = cteDocumentoRemetenteSimconsultasRepository;
			_cteComponentePrestacaoSimconsultasRepository = cteComponentePrestacaoSimconsultasRepository;
			_cteEmpresaSimconsultasRepository = cteEmpresaSimconsultasRepository;
			_configuracaoTranslogicRepository = configuracaoTranslogicRepository;
		}

		/// <summary>
		/// Obtém o xml do Cte do simconsultas
		/// </summary>
		/// <param name="chaveCte">Chave do cte</param>
		/// <returns>Objeto <see cref="CteSimconsultas"/></returns>
		public CteSimconsultas ObterXmlCteDoSimconsultas(string chaveCte)
		{
			if (chaveCte.Substring(20, 22).Substring(0, 2) != "57")
			{
				CteSimconsultas cte = new CteSimconsultas();
				cte.ChaveCte = chaveCte;
				cte.IndProcessado = false;
				cte.DataCadastro = DateTime.Now;
				cte.CteXml = "<InfConsulta xmlns=\"http://www.simconsultas.com.br/webservice\"><StatusConsulta>C1</StatusConsulta><StatusDescricao>Chave de acesso inválida. Modelo inválido (diferente de 57).</StatusDescricao></InfConsulta>";
				_cteSimconsultasRepository.Inserir(cte);
				return cte;
			}

			ConfiguracaoTranslogic confChaveAcesso = _configuracaoTranslogicRepository.ObterPorId("CHAVE_ACESSO_SIMCONSULTAS");
			string chaveAcesso = confChaveAcesso.Valor; // "195B98C3-7F0C-495D-8814-74210D41E01D";
			RetornoSimconsultas retornoSimconsultas = _simconsultasService.ObterDadosCte(chaveAcesso, chaveCte);
			if (!retornoSimconsultas.Erro)
			{
				if (retornoSimconsultas.Nfe.OuterXml.StartsWith("<CTe") 
					|| retornoSimconsultas.Nfe.OuterXml.Contains("<StatusConsulta>25</StatusConsulta>")
					|| retornoSimconsultas.Nfe.OuterXml.Contains("<StatusConsulta>3</StatusConsulta>")
					|| retornoSimconsultas.Nfe.OuterXml.Contains("<StatusConsulta>100</StatusConsulta>"))
				{
					CteSimconsultas cte = new CteSimconsultas();
					cte.ChaveCte = chaveCte;
					cte.IndProcessado = false;
					cte.DataCadastro = DateTime.Now;
					cte.CteXml = retornoSimconsultas.Nfe.OuterXml;
					_cteSimconsultasRepository.Inserir(cte);

					return cte;
				}
			}

			return null;
		}

		/// <summary>
		/// Obtém ctes que não foram processados ainda
		/// </summary>
		/// <returns>Lista de itens que não foram processados</returns>
		public IList<CteSimconsultas> ObterNaoProcessados()
		{
			return _cteSimconsultasRepository.ObterNaoProcessados();
		}

		/// <summary>
		/// Obtém ctes que não foram baixados ainda
		/// </summary>
		/// <returns>Lista de itens que não foram baixados</returns>
		public IList<string> ObterNaoBaixados()
		{
			return _cteSimconsultasRepository.ObterNaoBaixados();
		}

		/// <summary>
		/// Faz o parse do xml do CTe.
		/// </summary>
		/// <param name="cteSimconsultas"> Cte baixado do simconsultas a ser baixado. </param>
		[Transaction]
		public virtual void TransformarXmlCteSimconsultas(CteSimconsultas cteSimconsultas)
		{
			XmlDocument doc = new XmlDocument();
			doc.LoadXml(cteSimconsultas.CteXml);

			if (doc.DocumentElement != null)
			{
				if (doc.DocumentElement.Name.Equals("CTe"))
				{
					ProcessarDadosCte(doc, cteSimconsultas);
				}
				else if (doc.DocumentElement.Name.Equals("InfConsulta"))
				{
					XmlElement root = doc.DocumentElement;
					XmlNamespaceManager nsmgr = new XmlNamespaceManager(doc.NameTable);
					nsmgr.AddNamespace("d", root.NamespaceURI);
					XmlNode noInfConsulta = root.SelectSingleNode("//d:InfConsulta", nsmgr);
					cteSimconsultas.CodigoErro = ObterTextoNoXml(noInfConsulta, "StatusConsulta", nsmgr);
					cteSimconsultas.DescricaoErro = ObterTextoNoXml(noInfConsulta, "StatusDescricao", nsmgr);
					cteSimconsultas.IndProcessado = true;
					_cteSimconsultasRepository.Atualizar(cteSimconsultas);
				}
			}
		}

		private void ProcessarDadosCte(XmlDocument doc, CteSimconsultas cteSimconsultas)
		{
			string[] datePatterns = new string[] { "yyyy-MM-ddTHH:mm:ss", "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd" };
			XmlElement root = doc.DocumentElement;
			XmlNamespaceManager nsmgr = new XmlNamespaceManager(doc.NameTable);
			nsmgr.AddNamespace("d", root.NamespaceURI);

			XmlNode noInfCte = root.SelectSingleNode("//d:CTe/d:infCte", nsmgr);
			XmlNode noIde = noInfCte.SelectSingleNode("//d:ide", nsmgr);

			// DADOS DE IDENTIFICAÇÃO DA NFE
			cteSimconsultas.Cuf = ObterTextoNoXml(noIde, "cUF", nsmgr);
			cteSimconsultas.Cfop = ObterTextoNoXml(noIde, "CFOP", nsmgr);
			cteSimconsultas.NatOp = ObterTextoNoXml(noIde, "natOp", nsmgr);
			cteSimconsultas.ForPag = ObterTextoNoXml(noIde, "forPag", nsmgr);
			cteSimconsultas.Mod = ObterTextoNoXml(noIde, "mod", nsmgr);
			cteSimconsultas.Serie = ObterTextoNoXml(noIde, "serie", nsmgr);
			cteSimconsultas.Numero = ObterTextoNoXml(noIde, "nCT", nsmgr);
			var dataEmissao = ObterTextoNoXml(noIde, "dhEmi", nsmgr);
			if (dataEmissao != null)
			{
				cteSimconsultas.DhEmi = DateTime.ParseExact(dataEmissao, datePatterns, CultureInfo.InvariantCulture, DateTimeStyles.None);
			}

			cteSimconsultas.TpImp = ObterTextoNoXml(noIde, "tpImp", nsmgr);
			cteSimconsultas.TpEmis = ObterTextoNoXml(noIde, "tpEmis", nsmgr);
			cteSimconsultas.Cdv = ObterTextoNoXml(noIde, "cDV", nsmgr);
			cteSimconsultas.TpAmb = ObterTextoNoXml(noIde, "tpAmb", nsmgr);
			cteSimconsultas.TpCte = ObterTextoNoXml(noIde, "tpCTe", nsmgr);
			cteSimconsultas.ProcEmi = ObterTextoNoXml(noIde, "procEmi", nsmgr);
			cteSimconsultas.VerProc = ObterTextoNoXml(noIde, "verProc", nsmgr);
			cteSimconsultas.CmunEmi = ObterTextoNoXml(noIde, "cMunEnv", nsmgr);
			cteSimconsultas.XmunEmi = ObterTextoNoXml(noIde, "xMunEnv", nsmgr);
			cteSimconsultas.UfEmi = ObterTextoNoXml(noIde, "UFEnv", nsmgr);
			cteSimconsultas.Modal = ObterTextoNoXml(noIde, "modal", nsmgr);
			cteSimconsultas.TpServ = ObterTextoNoXml(noIde, "tpServ", nsmgr);
			cteSimconsultas.CmunIni = ObterTextoNoXml(noIde, "cMunIni", nsmgr);
			cteSimconsultas.XmunIni = ObterTextoNoXml(noIde, "xMunIni", nsmgr);
			cteSimconsultas.UfIni = ObterTextoNoXml(noIde, "UFIni", nsmgr);
			cteSimconsultas.CmunFim = ObterTextoNoXml(noIde, "cMunFim", nsmgr);
			cteSimconsultas.XmunFim = ObterTextoNoXml(noIde, "xMunFim", nsmgr);
			cteSimconsultas.UfFim = ObterTextoNoXml(noIde, "UFFim", nsmgr);
			cteSimconsultas.Retira = ObterTextoNoXml(noIde, "retira", nsmgr);
			
			XmlNode noToma03 = noIde.SelectSingleNode("//d:toma03", nsmgr);
			if (noToma03 != null && noToma03.HasChildNodes)
			{
				cteSimconsultas.TipoTomador = "TOMA03";
				cteSimconsultas.Toma = ObterTextoNoXml(noToma03, "toma", nsmgr);
			}

			XmlNode noToma4 = noIde.SelectSingleNode("//d:toma4", nsmgr);
			if (noToma4 != null && noToma4.HasChildNodes)
			{
				cteSimconsultas.TipoTomador = "TOMA4";
				cteSimconsultas.Toma = ObterTextoNoXml(noToma4, "toma", nsmgr);

				cteSimconsultas.Toma4 = ObterDadosEmpresa(noToma4, nsmgr, "enderToma");
			}

			XmlNode noEmpresa = noInfCte.SelectSingleNode("//d:emit", nsmgr);
			if (noEmpresa != null && noEmpresa.HasChildNodes)
			{
				cteSimconsultas.Emitente = this.ObterDadosEmpresa(noEmpresa, nsmgr, "enderEmit");
			}

			noEmpresa = noInfCte.SelectSingleNode("//d:rem", nsmgr);
			if (noEmpresa != null && noEmpresa.HasChildNodes)
			{
				cteSimconsultas.Remetente = this.ObterDadosEmpresa(noEmpresa, nsmgr, "enderReme");
			}

			noEmpresa = noInfCte.SelectSingleNode("//d:exped", nsmgr);
			if (noEmpresa != null && noEmpresa.HasChildNodes)
			{
				cteSimconsultas.Remetente = this.ObterDadosEmpresa(noEmpresa, nsmgr, "enderExped");
			}

			noEmpresa = noInfCte.SelectSingleNode("//d:receb", nsmgr);
			if (noEmpresa != null && noEmpresa.HasChildNodes)
			{
				cteSimconsultas.Recebedor = this.ObterDadosEmpresa(noEmpresa, nsmgr, "enderReceb");
			}

			noEmpresa = noInfCte.SelectSingleNode("//d:dest", nsmgr);
			if (noEmpresa != null && noEmpresa.HasChildNodes)
			{
				cteSimconsultas.Destinatario = this.ObterDadosEmpresa(noEmpresa, nsmgr, "enderDest");
			}

			var noVprest = noInfCte.SelectSingleNode("//d:vPrest", nsmgr);
			var valorPrestVrec = ObterTextoNoXml(noVprest, "vRec", nsmgr);
			if (valorPrestVrec != null)
			{
				cteSimconsultas.VprestVrec = Double.Parse(valorPrestVrec);
			}

			var valorPrestVtPrest = ObterTextoNoXml(noVprest, "vTPrest", nsmgr);
			if (valorPrestVtPrest != null)
			{
				cteSimconsultas.VprestVtPrest = Double.Parse(valorPrestVtPrest);
			}

			XmlNodeList listaNoComponentesPrestacao = noVprest.SelectNodes("//d:Comp", nsmgr);
			foreach (XmlNode noComponentePrestacao in listaNoComponentesPrestacao)
			{
				CteComponentePrestacaoSimconsultas componentePrestacao = new CteComponentePrestacaoSimconsultas();
				componentePrestacao.CteSimconsultas = cteSimconsultas;
				var valorComp = ObterTextoNoXml(noComponentePrestacao, "vComp", nsmgr);
				if (valorComp != null)
				{
					componentePrestacao.Vcomp = Double.Parse(valorComp);
				}

				componentePrestacao.Xnome = ObterTextoNoXml(noComponentePrestacao, "xNome", nsmgr);
				_cteComponentePrestacaoSimconsultasRepository.Inserir(componentePrestacao);
			}

			XmlNode noInformacaoCteNormal = noInfCte.SelectSingleNode("//d:infCTeNorm", nsmgr);
			if (noInformacaoCteNormal != null && noInformacaoCteNormal.HasChildNodes)
			{
				XmlNode noInformacaoCarga = noInformacaoCteNormal.SelectSingleNode("//d:infCarga", nsmgr);
				if (noInformacaoCarga != null && noInformacaoCarga.HasChildNodes)
				{
					XmlNodeList listaInfQ = noInformacaoCarga.SelectNodes("//d:infQ", nsmgr);
					double pesoTotalEmKg = 0;
					foreach (XmlNode noCarga in listaInfQ)
					{
						var unidadeMedidaCarga = ObterTextoNoXml(noCarga, "tpMed", nsmgr).ToUpperInvariant();
						var quantidadeCarga = ObterTextoNoXml(noCarga, "qCarga", nsmgr);
						var unidadeCarga = ObterTextoNoXml(noCarga, "cUnid", nsmgr);
						if (unidadeMedidaCarga.StartsWith("QUILO") || unidadeMedidaCarga.StartsWith("KG"))
						{
							double pesoParcial = int.Parse(unidadeCarga) * double.Parse(quantidadeCarga);
							pesoTotalEmKg += pesoParcial;
						} 
						
						if (unidadeMedidaCarga.StartsWith("TO") || unidadeMedidaCarga.StartsWith("TN"))
						{
							double pesoParcial = int.Parse(unidadeCarga) * double.Parse(quantidadeCarga) * 1000;
							pesoTotalEmKg += pesoParcial;
						}

						if (unidadeMedidaCarga.Contains("PESO"))
						{
							double pesoParcial;
							var quantidade = double.Parse(quantidadeCarga);

							if (quantidade < 100)
							{
								pesoParcial = double.Parse(quantidadeCarga) * 1000;
							}
							else
							{
								pesoParcial = double.Parse(quantidadeCarga);
							}
							
							pesoTotalEmKg += pesoParcial;
						}
					}

					cteSimconsultas.Peso = pesoTotalEmKg;
				}
			}

			cteSimconsultas.IndProcessado = true;
			_cteSimconsultasRepository.Atualizar(cteSimconsultas);
		}

		private CteEmpresaSimconsultas ObterDadosEmpresa(XmlNode noEmpresa, XmlNamespaceManager nsmgr, string nomeTagEndereco)
		{
			CteEmpresaSimconsultas cteEmp = new CteEmpresaSimconsultas();
			string cnpj = ObterTextoNoXml(noEmpresa, "CNPJ", nsmgr);
			if (cnpj != null)
			{
				cteEmp.TipoPessoa = "PJ";
				cteEmp.CnpjCpf = cnpj;
				cteEmp.InscricaoEstadual = ObterTextoNoXml(noEmpresa, "IE", nsmgr);
			}
			else
			{
				string cpf = ObterTextoNoXml(noEmpresa, "CPF", nsmgr);
				cteEmp.TipoPessoa = "PF";
				cteEmp.CnpjCpf = cpf;
			}

			cteEmp.Xnome = ObterTextoNoXml(noEmpresa, "xNome", nsmgr);
			cteEmp.Xfant = ObterTextoNoXml(noEmpresa, "xFant", nsmgr);
			cteEmp.Fone = ObterTextoNoXml(noEmpresa, "fone", nsmgr);
			cteEmp.Email = ObterTextoNoXml(noEmpresa, "email", nsmgr);

			XmlNode noEndereco = noEmpresa.SelectSingleNode("//d:" + nomeTagEndereco, nsmgr);

			cteEmp.Xlgr = ObterTextoNoXml(noEndereco, "xLgr", nsmgr);
			cteEmp.Nro = ObterTextoNoXml(noEndereco, "nro", nsmgr);
			cteEmp.Xcpl = ObterTextoNoXml(noEndereco, "xCpl", nsmgr);
			cteEmp.Xbairro = ObterTextoNoXml(noEndereco, "xBairro", nsmgr);
			cteEmp.Cmun = ObterTextoNoXml(noEndereco, "cMun", nsmgr);
			cteEmp.Xmun = ObterTextoNoXml(noEndereco, "xMun", nsmgr);
			cteEmp.Cep = ObterTextoNoXml(noEndereco, "CEP", nsmgr);
			cteEmp.Uf = ObterTextoNoXml(noEndereco, "UF", nsmgr);
			cteEmp.Cpais = ObterTextoNoXml(noEndereco, "cPais", nsmgr);
			cteEmp.Xpais = ObterTextoNoXml(noEndereco, "xPais", nsmgr);

			_cteEmpresaSimconsultasRepository.Inserir(cteEmp);

			if (nomeTagEndereco == "enderReme")
			{
				string[] datePatterns = new string[] { "yyyy-MM-ddTHH:mm:ss", "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd" };

				XmlNodeList listaDocNf = noEmpresa.SelectNodes("//d:infNF", nsmgr);
				if (listaDocNf != null && listaDocNf.Count > 0)
				{
					foreach (XmlNode noNf in listaDocNf)
					{
						CteDocumentoRemetenteSimconsultas doc = new CteDocumentoRemetenteSimconsultas();
						doc.NfNroma = ObterTextoNoXml(noNf, "nRoma", nsmgr);
						doc.NfNped = ObterTextoNoXml(noNf, "nPed", nsmgr);
						doc.NfSerie = ObterTextoNoXml(noNf, "serie", nsmgr);
						doc.NfNdoc = ObterTextoNoXml(noNf, "nDoc", nsmgr);
						var dataEmi = ObterTextoNoXml(noNf, "dEmi", nsmgr);
						if (dataEmi != null)
						{
							doc.NfDemi = DateTime.ParseExact(dataEmi, datePatterns, CultureInfo.InvariantCulture, DateTimeStyles.None);
						}

						doc.NfNCfop = ObterTextoNoXml(noNf, "nCFOP", nsmgr);

						var vBc = ObterTextoNoXml(noNf, "vBC", nsmgr);
						if (vBc != null)
						{
							doc.NfVbc = Double.Parse(vBc);
						}

						var valorIcms = ObterTextoNoXml(noNf, "vICMS", nsmgr);
						if (valorIcms != null)
						{
							doc.NfVicms = Double.Parse(valorIcms);
						}

						var valorbCst = ObterTextoNoXml(noNf, "vBCST", nsmgr);
						if (valorbCst != null)
						{
							doc.NfVbcst = Double.Parse(valorbCst);
						}
						
						var valorSt = ObterTextoNoXml(noNf, "vST", nsmgr);
						if (valorSt != null)
						{
							doc.NfVst = Double.Parse(valorSt);
						}

						var valorProd = ObterTextoNoXml(noNf, "vProd", nsmgr);
						if (valorProd != null)
						{
							doc.NfVprod = Double.Parse(valorProd);
						}

						var valorNf = ObterTextoNoXml(noNf, "vNF", nsmgr);
						if (valorNf != null)
						{
							doc.NfVnf = Double.Parse(valorNf);
						}

						var peso = ObterTextoNoXml(noNf, "nPeso", nsmgr);
						if (peso != null)
						{
							doc.NfNpeso = Double.Parse(peso);
						}

						doc.Pin = ObterTextoNoXml(noNf, "PIN", nsmgr);
						doc.CteEmpresaSimconsultas = cteEmp;
						_cteDocumentoRemetenteSimconsultasRepository.Inserir(doc);
					}
				}

				XmlNodeList listaDocNfe = noEmpresa.SelectNodes("//d:infNFe", nsmgr);
				if (listaDocNfe != null && listaDocNfe.Count > 0)
				{
					foreach (XmlNode noNfe in listaDocNfe)
					{
						CteDocumentoRemetenteSimconsultas doc = new CteDocumentoRemetenteSimconsultas();
						doc.NfeChave = ObterTextoNoXml(noNfe, "chave", nsmgr);
						doc.Pin = ObterTextoNoXml(noNfe, "PIN", nsmgr);
						doc.CteEmpresaSimconsultas = cteEmp;
						_cteDocumentoRemetenteSimconsultasRepository.Inserir(doc);
					}
				}

				XmlNodeList listaDocOutros = noEmpresa.SelectNodes("//d:infOutros", nsmgr);
				if (listaDocOutros != null && listaDocOutros.Count > 0)
				{
					foreach (XmlNode noOutros in listaDocOutros)
					{
						CteDocumentoRemetenteSimconsultas doc = new CteDocumentoRemetenteSimconsultas();
						doc.OutroTpDoc = ObterTextoNoXml(noOutros, "tpDoc", nsmgr);
						doc.OutroDescOutros = ObterTextoNoXml(noOutros, "descOutros", nsmgr);
						doc.OutroNdoc = ObterTextoNoXml(noOutros, "nDoc", nsmgr);
						doc.OutroDemi = DateTime.ParseExact(ObterTextoNoXml(noOutros, "dEmi", nsmgr), datePatterns, CultureInfo.InvariantCulture, DateTimeStyles.None);
						
						string valorDocFisc = ObterTextoNoXml(noOutros, "vDocFisc", nsmgr);
						if (valorDocFisc != null)
						{
							doc.OutroVdocFisc = Double.Parse(valorDocFisc);
						}
						
						doc.CteEmpresaSimconsultas = cteEmp;
						_cteDocumentoRemetenteSimconsultasRepository.Inserir(doc);
					}
				}
			}

			return cteEmp;
		}

		private string ObterTextoNoXml(XmlNode xmlNode, string tagNo, XmlNamespaceManager nsmgr)
		{
			XmlNode no = xmlNode.SelectSingleNode("./d:" + tagNo, nsmgr);
			if (no == null)
			{
				return null;
			}

			return no.InnerText;
		}
	}
}