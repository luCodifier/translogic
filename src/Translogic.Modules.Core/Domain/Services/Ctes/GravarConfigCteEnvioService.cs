namespace Translogic.Modules.Core.Domain.Services.Ctes
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq;
    using System.Net;
    using System.Reflection;
    using System.Text;
    using System.Text.RegularExpressions;
    using Castle.Services.Transaction;
    using FluxosComerciais;
    using Interfaces.Trem.OrdemServico;
    using Model.Codificador;
    using Model.Codificador.Repositories;
    using Model.Diversos;
    using Model.Diversos.Bacen;
    using Model.Diversos.Bacen.Repositories;
    using Model.Diversos.Cte;
    using Model.Diversos.Ibge;
    using Model.Diversos.Ibge.Repositories;
    using Model.Diversos.Repositories;
    using Model.Estrutura;
    using Model.Estrutura.Repositories;
    using Model.FluxosComerciais;
    using Model.FluxosComerciais.ConhecimentoTransporteEletronico;
    using Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;
    using Model.FluxosComerciais.Nfes;
    using Model.FluxosComerciais.Pedidos.Despachos;
    using Model.FluxosComerciais.Pedidos.Despachos.Repositories;
    using Model.FluxosComerciais.Repositories;
    using Model.FluxosComerciais.TriangulusConfig;
    using Model.FluxosComerciais.TriangulusConfig.Repositories;
    using Model.Trem.Veiculo.Conteiner.Repositories;
    using Model.Trem.Veiculo.Vagao;
    using Model.Via;
    using Model.Via.Circulacao;
    using Model.Via.Circulacao.Repositories;
    using NHibernate.Validator.Engine;
    using Translogic.Core.Infrastructure;
    using Translogic.Core.Infrastructure.Validation;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.InterfaceSap.Repositories;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.TriangulusConfig.Interfaces;
    using Translogic.Modules.Core.Spd;
    using Translogic.Modules.EDI.Domain.Models.NotasExpedicao.GestaoMensagens;
    using Translogic.Modules.EDI.Domain.Models.NotasExpedicao.GestaoMensagens.Repositories;
    using Util;

    /// <summary>
    /// Classe para gravar os dados do CT-e de envio na base da config
    /// </summary>
    [Transactional]
    public partial class GravarConfigCteEnvioService
    {
        private readonly ICteRepository _cteRepository;
        private readonly IDespachoTranslogicRepository _despachoTranslogicRepository;
        private readonly IItemDespachoRepository _itemDespachoRepository;
        private readonly IUfIbgeRepository _unidadeFederativaIbgeRepository;
        private readonly IAssociaFluxoInternacionalRepository _associaFluxoInternacionalRepository;
        private readonly IPaisBacenRepository _paisBacenRepository;
        private readonly IComposicaoFreteContratoRepository _composicaoFreteContratoRepository;
        private readonly IVagaoConteinerVigenteRepository _vagaoConteinerVigenteRepository;
        private readonly ICteConteinerRepository _cteConteinerRepository;
        private readonly ICteDetalheRepository _cteDetalheRepository;
        private readonly IConfiguracaoTranslogicRepository _configuracaoTranslogicRepository;
        private readonly IComposicaoFreteCteRepository _composicaoFreteCteRepository;
        private readonly ICteArvoreRepository _cteArvoreRepository;
        private readonly ICteAgrupamentoRepository _cteAgrupamentoRepository;
        private readonly ICteOrigemRepository _cteOrigemRepository;
        private readonly ISerieDespachoUfRepository _serieDespachoUfRepository;
        private readonly ICte01Repository _cte01Repository;
        private readonly ICte02Repository _cte02Repository;
        private readonly ICte03Repository _cte03Repository;
        private readonly ICte05Repository _cte05Repository;
        private readonly ICte06Repository _cte06Repository;
        private readonly ICte100Repository _cte100Repository;
        private readonly ICte07Repository _cte07Repository;
        private readonly ICte08Repository _cte08Repository;
        private readonly ICte09Repository _cte09Repository;
        private readonly ICte10Repository _cte10Repository;
        private readonly ICte11Repository _cte11Repository;
        private readonly ICte12Repository _cte12Repository;
        private readonly ICte14Repository _cte14Repository;
        private readonly ICte15Repository _cte15Repository;
        private readonly ICte37Repository _cte37Repository;
        private readonly ICte38Repository _cte38Repository;
        private readonly ICte60Repository _cte60Repository;
        private readonly ICte62Repository _cte62Repository;
        private readonly ICte67Repository _cte67Repository;
        private readonly INfe03FilialRepository _nfe03FilialRepository;
        private readonly ICidadeIbgeRepository _cidadeIbgeRepository;
        private readonly IValidatorEngine _validatorEngine;
        private readonly ICte51Repository _cte51Repository;
        private readonly IEmpresaClienteRepository _empresaFerroviaClienteRepository;
        private readonly IEmpresaFerroviaRepository _empresaFerroviaRepository;
        private readonly IEmpresaInterfaceCteRepository _empresaInterfaceCteRepository;
        private readonly CteService _cteService;
        private readonly NfeService _nfeService;
        private readonly ICteSerieNumeroEmpresaUfRepository _cteSerieNumeroEmpresaUfRepository;
        private readonly IRotaRepository _rotaRepository;
        private readonly ICteEnvioXmlRepository _cteEnvioXmlRepository;
        private readonly ICotacaoRepository _cotacaoRepository;
        private readonly CarregamentoService _carregamentoService;
        private readonly ICteEmpresasRepository _cteEmpresasRepository;
        private readonly IMensagemRecebimentoVagaoRepository _mensagemRecebimentoVagaoRepository;
        private readonly IControleCpbrRepository _controleCpbrRepository;
        private readonly IContratoRepository _contratoRepository;
        private readonly ICteAnuladoRepository _cteAnuladoRepository;
        private readonly ISapChaveCteRepository _sapChaveCteRepository;


        private readonly ICte101Repository _cte101Repository;
        private readonly ICte102Repository _cte102Repository;
        private readonly ICte103Repository _cte103Repository;
        private readonly ICte105Repository _cte105Repository;
        private readonly ICte107Repository _cte107Repository;
        private readonly ICte109Repository _cte109Repository;
        private readonly ICte111Repository _cte111Repository;
        private readonly ICte113Repository _cte113Repository;
        private readonly ICte63Repository _cte63Repository;
        private readonly ICte64Repository _cte64Repository;
        private readonly ICte66Repository _cte66Repository;
        private readonly ICte70Repository _cte70Repository;
        private readonly ICte69Repository _cte69Repository;
        private readonly ICte32Repository _cte32Repository;

        private readonly string _chaveCteFormaEmissao = "CTE_FORMA_EMISSAO";
        // private readonly string _chaveCteTipoAmbiente = "CTE_TIPO_AMBIENTE";
        private readonly string _chaveCteVersaoProcesso = "CTE_VERSAO_PROCESSO";
        private readonly string _chaveCteCodigoInternoFilial = "CTE_CODIGO_INTERNO_FILIAL_DEFAULT";
        private readonly string _chaveVersaoSefaz = "CTE_VERSAO_SEFAZ";
        private readonly string _chaveFtpHost = "CTE_FTP_HOST";
        private readonly string _chaveFtpPath = "CTE_FTP_PATH";
        private readonly string _chaveFtpUsuario = "CTE_FTP_USUARIO";
        private readonly string _chaveFtpSenha = "CTE_FTP_SENHA";
        private readonly string _chaveCfopEstadual = "CTE_CFOP_COD_TRAFEGO_MUTUO_ESTADUAL";
        private readonly string _chaveCfopInterestadual = "CTE_CFOP_COD_TRAFEGO_MUTUO_INTERESTADUAL";
        private readonly string _chaveCfopInternacional = "CTE_CFOP_COD_TRAFEGO_MUTUO_INTERNACIONAL";
        private readonly string _chaveExibirPisCofins = "CTE_EXIBIR_OBS_PIS_COFINS";
        private readonly string _chaveContainerVazioTipoFrigorifico = "CONTAINER_VAZIO_TIPO_FRIGORIFICO";

        private readonly string _condicaoTarifa = "ZFR";

        private Cte _cte;
        private CteAnulado _cteAnulado;
        private DespachoTranslogic _despachoTranslogic;
        private SerieDespachoUf _serieDespachoUf;
        private FluxoComercial _fluxoComercial;

        private IEmpresa _empresaRemetenteFluxoComercial;
        private IEmpresa _empresaDestinatariaFluxoComercial;
        private IEmpresa _empresaProprietariaVagao;
        private IEmpresa _empresaPagadoraContrato;
        private IEmpresa _empresaRemetenteContrato;
        private IEmpresa _empresaDestinatariaContrato;
        private IEmpresa _empresaContratoDestinatariaFiscal;
        private IEmpresa _empresaContratoRemetenteFiscal;
        private IEmpresa _empresaTomadora;
        private IEmpresa _empresaFerroviaCliente;
        private IEmpresa _empresaOperadoraEstacaoMaeOrigemFluxoComercial;

        private CteEmpresas _cteEmpresas;

        private Cte _cteRaiz;
        private Cte _cteRaizAgrupamento;
        private ItemDespacho _itemDespacho;
        private ContratoHistorico _contratoHistorico;
        private IContrato _contrato;
        private Vagao _vagao;
        private SerieVagao _serieVagao;
        private TipoVagao _tipoVagao;
        private CidadeIbge _cidadeIbgeEmpresaPagadoraContrato;
        private CidadeIbge _cidadeIbgeEmpresaRemetenteContrato;
        private CidadeIbge _cidadeIbgeEmpresaDestinatariaContrato;
        private IList<CteDetalhe> _listaCteDetalhe;
        private CteLogService _cteLogService;
        private EstacaoMae _estacaoMaeOrigemFluxoComercial;
        private EstacaoMae _estacaoMaeDestinoFluxoComercial;
        private Municipio _municipioEstacaoMaeOrigemFluxoComercial;
        private Municipio _municipioEstacaoMaeDestinoFluxoComercial;
        private CidadeIbge _cidadeIbgeEstacaoMaeOrigemFluxoComercial;
        private CidadeIbge _cidadeIbgeEstacaoMaeDestinoFluxoComercial;
        private Nfe03Filial _nfe03Filial;
        private CidadeIbge _cidadeIbgeEmpresaTomadora;
        private CidadeIbge _cidadeIbgeRemetenteFiscal;
        private CidadeIbge _cidadeIbgeDestinatariaFiscal;
        private StringBuilder _errosValidacao;
        private double _pesoParaCalculo;
        private TipoTrafegoMutuoEnum _tipoTrafegoMutuo;
        private TipoNotaCteEnum _tipoNotaCte;
        private CidadeIbge _cidadeIbgeEmpresaFerrovia;
        private ConfiguracaoTranslogic _cfopEstadual;
        private ConfiguracaoTranslogic _cfopInterestadual;
        private ConfiguracaoTranslogic _cfopInternacional;
        private string _cfop;
        private string _cfopDescricao;
        private bool _partilhaFerroeste;
        private bool _origemFerroeste;
        private StringBuilder _bufferMensagem = null;
        private double _valorImpostoRetidoSubstTributaria = 0.0;
        private int codigoTomador;
        private double _diferencaDeCentavosNaComposicaoFrete = 0.0;
        ////private decimal _despachoTranslogic_NumeroDespacho;
        ////private DateTime? _despachoTranslogic_DataDespacho;
        ////private int? _despachoTranslogic_NumDespIntercambio;
        ////private int? _despachoTranslogic_NumeroSerieDespacho;
        ////private int? _despachoTranslogic_SerieDespacho;
        ////private int? _despachoTranslogic_SerieDespachoNum;
        ////private string _despachoTranslogic_SerieDespachoSdi;
        ////private int? _despachoTranslogic_NumeroDespachoUf;
        ////private int? _despachoTranslogic_SerieDespachoUf; 


        /// <summary>
        /// Initializes a new instance of the <see cref="GravarConfigCteEnvioService"/> class.
        /// </summary>
        /// <param name="cteRepository">Reposit�rio do Cte injetado</param>
        /// <param name="itemDespachoRepository">Reposit�rio do ItemDespacho injetado</param>
        /// <param name="unidadeFederativaIbgeRepository">Reposit�rio do UF do IBGE injetado</param>
        /// <param name="paisBacenRepository">Reposit�rio do codigo do pais no BACEN injetado</param>
        /// <param name="composicaoFreteContratoRepository">Reposit�rio da composicao frete contrato injetado</param>
        /// <param name="vagaoConteinerVigenteRepository">Reposit�rio do vag�o conteiner injetado</param>
        /// <param name="cteDetalheRepository">Reposit�rio detalhe do cte injetado</param>
        /// <param name="configuracaoTranslogicRepository">Reposit�rio da configura��o do translogic injetado</param>
        /// <param name="composicaoFreteCteRepository">Reposit�rio de composi��o frete cte injetado</param>
        /// <param name="cteArvoreRepository">Reposit�rio de cte arvore injetado</param>
        /// <param name="cteAgrupamentoRepository">Reposit�rio de cte Agrupamento injetado</param>
        /// <param name="cteOrigemRepository">Reposit�rio de cte Origem injetado</param>
        /// <param name="cte01Repository">Reposit�rio de cte01 dados principais</param>
        /// <param name="cte02Repository">Reposit�rio de cte02 dados de pontos de passagem</param>
        /// <param name="cte05Repository">Reposit�rio de cte05 dados de informa��es do emitente</param>
        /// <param name="cte100Repository">Reposit�rio de cte100 dados de informa��es de documentos</param>
        /// <param name="cte07Repository">Reposit�rio de cte07 dados de informa��es do expeditor</param>
        /// <param name="cte08Repository">Reposit�rio de cte08 dados de valores de presta��o de servi�o</param>
        /// <param name="cte09Repository">Reposit�rio de cte09 dados de componentes do valor da prestacao</param>
        /// <param name="cte10Repository">Reposit�rio de cte10 Informacoes do CT-e Normal ou Anulacao</param>
        /// <param name="cte11Repository">Reposit�rio de cte11 Informacoes da qtde da carga</param>
        /// <param name="cte14Repository">Reposit�rio de cte14 Informacoes de emitente dos doc anteriores</param>
        /// <param name="cte15Repository">Reposit�rio de cte15 Informacoes basicas dois doc de transportes anteriores</param>
        /// <param name="cte60Repository">Reposit�rio de cte60 Informa��es do modal Ferrovi�rio</param>
        /// <param name="cte62Repository">Reposit�rio de cte62 Informa��es das Ferrovias Envolvidas</param>
        /// <param name="associaFluxoInternacionalRepository">Reposit�rio do associa fluxo internacional injetado</param>
        /// <param name="cteLogService">Servi�o de log do Cte injetado</param>
        /// <param name="serieDespachoUfRepository">Repositorio de serie de despacho uf</param>
        /// <param name="nfe03FilialRepository">Reposit�rio da Nfe03 injetado</param>
        /// <param name="cidadeIbgeRepository">Reposit�rio da cidade IBGE injetado</param>
        /// <param name="validatorEngine">Validator Engine Injetado</param>
        /// <param name="cte51Repository">Reposit�rio do cte51 dados de ftp para o PDF injetado</param>
        /// <param name="empresaFerroviaClienteRepository">Reposit�rio da empresa ferrovia Cliente injetado</param>
        /// <param name="empresaFerroviaRepository">Reposit�rio da empresa ferrovia injetado</param>
        /// <param name="empresaInterfaceCteRepository">Reposit�rio da empresa Interface cte injetado</param>
        /// <param name="cteService"> Servi�o do Cte Service</param>
        /// <param name="nfeService">Servi�o do Nfe Service</param>
        /// <param name="cteSerieNumeroEmpresaUfRepository"> Cte Serie Empresa Uf Reposit�rio injetado</param>
        /// <param name="rotaRepository"> Rota Reposit�rio injetado</param>
        /// <param name="cteEnvioXmlRepository">Reposit�rio do envio do arquivo xml injetado</param>
        /// <param name="cotacaoRepository">Reposit�rio de cota��o injetado</param>
        /// <param name="carregamentoService">Servi�o de carregamento injetado</param>
        /// <param name="cte03Repository">Reposit�rio cte03 injetado</param>
        /// <param name="cteEmpresasRepository">Reposit�rio Cte Empresas injetado</param>
        /// <param name="cte37Repository">Reposit�rio cte37 injetado</param>
        /// <param name="cte38Repository">Reposit�rio cte38 injetado</param>
        /// <param name="mensagemRecebimentoVagaoRepository">Reposit�rio MensagemRecebimentoVagaoRepository injetado</param>
        /// <param name="cte101Repository">Reposit�rio de cte101 dados de informa��es de documentos</param>
        /// <param name="cte102Repository">Reposit�rio de cte102 dados de informa��es de documentos</param>
        /// <param name="cte103Repository">Reposit�rio de cte103 dados de informa��es de documentos</param>
        /// <param name="cte105Repository">Reposit�rio de cte105 dados de informa��es de documentos</param>
        /// <param name="cte109Repository">Reposit�rio de cte109 dados de informa��es de documentos</param>
        /// <param name="cte113Repository">Reposit�rio de cte113 dados de informa��es de documentos</param>
        /// <param name="cte111Repository">Reposit�rio de cte111 dados de informa��es de documentos</param>
        /// <param name="cte107Repository">Reposit�rio de cte107 dados de informa��es de documentos</param>
        /// <param name="cte67Repository">Reposit�rio de cte67 dados de informa��es de documentos</param>
        /// <param name="cte06Repository">Reposit�rio de cte06 dados de informa��es de documentos</param>
        /// <param name="cte63Repository">Reposit�rio de cte63 dados de informa��es de documentos</param>
        /// <param name="cte64Repository">Reposit�rio de cte64 dados de informa��es de documentos</param>
        /// <param name="cte66Repository">Reposit�rio de cte66 dados de informa��es de documentos</param>
        /// <param name="cte70Repository">Reposit�rio de cte70 dados de informa��es de documentos</param>
        /// <param name="cte69Repository">Reposit�rio de cte69 dados de informa��es de documentos</param>
        /// <param name="cte12Repository">Reposit�rio de cte12 dados de informa��es de documentos</param>
        /// <param name="cte32Repository">Reposit�rio de cte32 dados de informa��es de documentos</param>
        /// <param name="controleCpbrRepository">Repositorio de controle de tributo cpbr</param>
        public GravarConfigCteEnvioService(ICteRepository cteRepository, IDespachoTranslogicRepository despachoTranslogicRepository, IItemDespachoRepository itemDespachoRepository, IUfIbgeRepository unidadeFederativaIbgeRepository, IPaisBacenRepository paisBacenRepository, IComposicaoFreteContratoRepository composicaoFreteContratoRepository, ICteConteinerRepository cteConteinerRepository, IVagaoConteinerVigenteRepository vagaoConteinerVigenteRepository, ICteDetalheRepository cteDetalheRepository, IConfiguracaoTranslogicRepository configuracaoTranslogicRepository, IComposicaoFreteCteRepository composicaoFreteCteRepository, ICteArvoreRepository cteArvoreRepository, ICteAgrupamentoRepository cteAgrupamentoRepository, ICteOrigemRepository cteOrigemRepository, ICte01Repository cte01Repository, ICte02Repository cte02Repository, ICte05Repository cte05Repository, ICte100Repository cte100Repository, ICte07Repository cte07Repository, ICte08Repository cte08Repository, ICte09Repository cte09Repository, ICte10Repository cte10Repository, ICte11Repository cte11Repository, ICte14Repository cte14Repository, ICte15Repository cte15Repository, ICte60Repository cte60Repository, ICte62Repository cte62Repository, IAssociaFluxoInternacionalRepository associaFluxoInternacionalRepository, CteLogService cteLogService, ISerieDespachoUfRepository serieDespachoUfRepository, INfe03FilialRepository nfe03FilialRepository, ICidadeIbgeRepository cidadeIbgeRepository, IValidatorEngine validatorEngine, ICte51Repository cte51Repository, IEmpresaClienteRepository empresaFerroviaClienteRepository, IEmpresaFerroviaRepository empresaFerroviaRepository, IEmpresaInterfaceCteRepository empresaInterfaceCteRepository, CteService cteService, NfeService nfeService, ICteSerieNumeroEmpresaUfRepository cteSerieNumeroEmpresaUfRepository, IRotaRepository rotaRepository, ICteEnvioXmlRepository cteEnvioXmlRepository, ICotacaoRepository cotacaoRepository, CarregamentoService carregamentoService, ICte03Repository cte03Repository, ICteEmpresasRepository cteEmpresasRepository, ICte37Repository cte37Repository, ICte38Repository cte38Repository, IMensagemRecebimentoVagaoRepository mensagemRecebimentoVagaoRepository, ICte101Repository cte101Repository, ICte102Repository cte102Repository, ICte103Repository cte103Repository, ICte105Repository cte105Repository, ICte109Repository cte109Repository, ICte113Repository cte113Repository, ICte111Repository cte111Repository, ICte107Repository cte107Repository, ICte67Repository cte67Repository, ICte06Repository cte06Repository, ICte63Repository cte63Repository, ICte64Repository cte64Repository, ICte66Repository cte66Repository, ICte70Repository cte70Repository, ICte69Repository cte69Repository, ICte12Repository cte12Repository, ICte32Repository cte32Repository, IControleCpbrRepository controleCpbrRepository, IContratoRepository contratoRepository, ICteAnuladoRepository cteAnuladoRepository, ISapChaveCteRepository _sapChaveCteRepository)
        {
            _cteSerieNumeroEmpresaUfRepository = cteSerieNumeroEmpresaUfRepository;
            _despachoTranslogicRepository = despachoTranslogicRepository;
            _cteEmpresasRepository = cteEmpresasRepository;
            _cte03Repository = cte03Repository;
            _carregamentoService = carregamentoService;
            _cotacaoRepository = cotacaoRepository;
            _cteEnvioXmlRepository = cteEnvioXmlRepository;
            _rotaRepository = rotaRepository;
            _validatorEngine = validatorEngine;
            _nfeService = nfeService;
            _cteService = cteService;
            _empresaInterfaceCteRepository = empresaInterfaceCteRepository;
            _empresaFerroviaRepository = empresaFerroviaRepository;
            _empresaFerroviaClienteRepository = empresaFerroviaClienteRepository;
            _cteRepository = cteRepository;
            _cidadeIbgeRepository = cidadeIbgeRepository;
            _nfe03FilialRepository = nfe03FilialRepository;
            _serieDespachoUfRepository = serieDespachoUfRepository;
            _cteLogService = cteLogService;
            _associaFluxoInternacionalRepository = associaFluxoInternacionalRepository;
            _cte01Repository = cte01Repository;
            _cte02Repository = cte02Repository;
            _cte05Repository = cte05Repository;
            _cte100Repository = cte100Repository;
            _cte07Repository = cte07Repository;
            _cte08Repository = cte08Repository;
            _cte09Repository = cte09Repository;
            _cte10Repository = cte10Repository;
            _cte11Repository = cte11Repository;
            _cte14Repository = cte14Repository;
            _cte15Repository = cte15Repository;
            _cte51Repository = cte51Repository;
            _cte60Repository = cte60Repository;
            _cte62Repository = cte62Repository;
            _cteArvoreRepository = cteArvoreRepository;
            _cteAgrupamentoRepository = cteAgrupamentoRepository;
            _cteOrigemRepository = cteOrigemRepository;
            _composicaoFreteCteRepository = composicaoFreteCteRepository;
            _configuracaoTranslogicRepository = configuracaoTranslogicRepository;
            _cteDetalheRepository = cteDetalheRepository;
            _vagaoConteinerVigenteRepository = vagaoConteinerVigenteRepository;
            _composicaoFreteContratoRepository = composicaoFreteContratoRepository;
            _paisBacenRepository = paisBacenRepository;
            _unidadeFederativaIbgeRepository = unidadeFederativaIbgeRepository;
            _itemDespachoRepository = itemDespachoRepository;
            _cte37Repository = cte37Repository;
            _cte38Repository = cte38Repository;
            _mensagemRecebimentoVagaoRepository = mensagemRecebimentoVagaoRepository;
            _cte101Repository = cte101Repository;
            _cte102Repository = cte102Repository;
            _cte103Repository = cte103Repository;
            _cte105Repository = cte105Repository;
            _cte109Repository = cte109Repository;
            _cte113Repository = cte113Repository;
            _cte111Repository = cte111Repository;
            _cte107Repository = cte107Repository;
            _cte67Repository = cte67Repository;
            _cte06Repository = cte06Repository;
            _cte63Repository = cte63Repository;
            _cte64Repository = cte64Repository;
            _cte66Repository = cte66Repository;
            _cte70Repository = cte70Repository;
            _cte69Repository = cte69Repository;
            _cte12Repository = cte12Repository;
            _cte32Repository = cte32Repository;
            _controleCpbrRepository = controleCpbrRepository;
            _cteConteinerRepository = cteConteinerRepository;
            _contratoRepository = contratoRepository;
            _cteAnuladoRepository = cteAnuladoRepository;
            this._sapChaveCteRepository = _sapChaveCteRepository;
        }

        /// <summary>
        /// Executa a grava��o do arquivo de envio - Quando o CTE est� em status PAE, processa aqui.
        /// </summary>
        /// <param name="cte">Cte que ser� gravado</param>
        public void Executar(Cte cte)
        {
            _cte = cte;
            _cteEmpresas = new CteEmpresas();
            try
            {
                RegistrarLogDetalhado("Inicio processo gravar dados na Config");
                // Busca as inform��es para gera��o do Cte
                CarregarDados();

                // Verifica se as informa��es est�o certas
                ValidarDados();

                // Limpa as tabelas, no caso de ser reenvio
                RemoverCteConfigParaReenvio();

                // Calcular o valor do Cte
                if (PartilhaFerroeste())
                {
                    RegistrarLogDetalhado("Realizando c�lculo do cte para a partilha da ferroeste");

                    CalcularValorCteFerroeste();
                }
                else
                {
                    RegistrarLogDetalhado("Realizando c�lculo normal do cte");

                    CalcularValorCte();
                }

                ValidarValorCte();

                Executar();
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(_cte, "GravarConfigCteEnvioService", string.Format("Executar:: {0}", ex.Message), ex);
                throw;
            }
        }

        private void ValidaCteRateioCarregamentoFinalizado()
        {
            // Verifica se no carregamento do vag�o existe algum fluxo marcado como rateio de CTE.
            if (_carregamentoService.VerificaPossuiFluxoRateioCte(_cte.Chave))
            {
                if (!(_carregamentoService.VerificaMultiploDespachoFinalizado(_cte.Chave)))
                {
                    var mensagem = string.Format("Carregamento do vag�o ainda n�o foi finalizado, n�o � possivel calcular o valor do CTE de rateio {0}", _cte.Chave);
                    RegistrarLogDetalhado(mensagem);
                    throw new Exception(mensagem);
                }
            }
        }

        /// <summary>
        /// Remove o registro das tabelas caso j� exista, utilizado quando � reenvio
        /// </summary>
        public void RemoverCteConfigParaReenvio()
        {
            var cteEmpresas = _cteEmpresasRepository.ObterPorCte(_cte);
            if (cteEmpresas != null)
                _cteEmpresasRepository.RemoverPorId(cteEmpresas.Id);

            _cte01Repository.Remover(ObterIdentificadorFilial(), int.Parse(_cte.Numero), int.Parse(_cte.Serie));
            _cte02Repository.Remover(ObterIdentificadorFilial(), int.Parse(_cte.Numero), int.Parse(_cte.Serie));
            _cte03Repository.Remover(ObterIdentificadorFilial(), int.Parse(_cte.Numero), int.Parse(_cte.Serie));
            _cte05Repository.Remover(ObterIdentificadorFilial(), int.Parse(_cte.Numero), int.Parse(_cte.Serie));
            _cte100Repository.Remover(ObterIdentificadorFilial(), int.Parse(_cte.Numero), int.Parse(_cte.Serie));
            _cte06Repository.Remover(ObterIdentificadorFilial(), int.Parse(_cte.Numero), int.Parse(_cte.Serie));
            _cte07Repository.Remover(ObterIdentificadorFilial(), int.Parse(_cte.Numero), int.Parse(_cte.Serie));
            _cte08Repository.Remover(ObterIdentificadorFilial(), int.Parse(_cte.Numero), int.Parse(_cte.Serie));
            _cte09Repository.Remover(ObterIdentificadorFilial(), int.Parse(_cte.Numero), int.Parse(_cte.Serie));
            _cte10Repository.Remover(ObterIdentificadorFilial(), int.Parse(_cte.Numero), int.Parse(_cte.Serie));
            _cte11Repository.Remover(ObterIdentificadorFilial(), int.Parse(_cte.Numero), int.Parse(_cte.Serie));
            _cte12Repository.Remover(ObterIdentificadorFilial(), int.Parse(_cte.Numero), int.Parse(_cte.Serie));
            _cte14Repository.Remover(ObterIdentificadorFilial(), int.Parse(_cte.Numero), int.Parse(_cte.Serie));
            _cte15Repository.Remover(ObterIdentificadorFilial(), int.Parse(_cte.Numero), int.Parse(_cte.Serie));
            _cte32Repository.Remover(ObterIdentificadorFilial(), int.Parse(_cte.Numero), int.Parse(_cte.Serie));
            _cte38Repository.Remover(ObterIdentificadorFilial(), int.Parse(_cte.Numero), int.Parse(_cte.Serie));
            _cte51Repository.Remover(ObterIdentificadorFilial(), int.Parse(_cte.Numero), int.Parse(_cte.Serie));
            _cte60Repository.Remover(ObterIdentificadorFilial(), int.Parse(_cte.Numero), int.Parse(_cte.Serie));
            _cte62Repository.Remover(ObterIdentificadorFilial(), int.Parse(_cte.Numero), int.Parse(_cte.Serie));
            _cte63Repository.Remover(ObterIdentificadorFilial(), int.Parse(_cte.Numero), int.Parse(_cte.Serie));
            _cte64Repository.Remover(ObterIdentificadorFilial(), int.Parse(_cte.Numero), int.Parse(_cte.Serie));
            _cte66Repository.Remover(ObterIdentificadorFilial(), int.Parse(_cte.Numero), int.Parse(_cte.Serie));
            _cte67Repository.Remover(ObterIdentificadorFilial(), int.Parse(_cte.Numero), int.Parse(_cte.Serie));
            _cte69Repository.Remover(ObterIdentificadorFilial(), int.Parse(_cte.Numero), int.Parse(_cte.Serie));
            _cte70Repository.Remover(ObterIdentificadorFilial(), int.Parse(_cte.Numero), int.Parse(_cte.Serie));
            _cte101Repository.Remover(ObterIdentificadorFilial(), int.Parse(_cte.Numero), int.Parse(_cte.Serie));
            _cte102Repository.Remover(ObterIdentificadorFilial(), int.Parse(_cte.Numero), int.Parse(_cte.Serie));
            _cte103Repository.Remover(ObterIdentificadorFilial(), int.Parse(_cte.Numero), int.Parse(_cte.Serie));
            _cte105Repository.Remover(ObterIdentificadorFilial(), int.Parse(_cte.Numero), int.Parse(_cte.Serie));
            _cte109Repository.Remover(ObterIdentificadorFilial(), int.Parse(_cte.Numero), int.Parse(_cte.Serie));
            _cte113Repository.Remover(ObterIdentificadorFilial(), int.Parse(_cte.Numero), int.Parse(_cte.Serie));
            _cte111Repository.Remover(ObterIdentificadorFilial(), int.Parse(_cte.Numero), int.Parse(_cte.Serie));
            _cte107Repository.Remover(ObterIdentificadorFilial(), int.Parse(_cte.Numero), int.Parse(_cte.Serie));
        }

        /// <summary>
        /// Metodo Dummy
        /// </summary>
        /// <param name="emails">linha com email para parser</param>
        /// <returns>Retorna uma lista com os emails</returns>
        public string[] Dummy(string emails)
        {
            return ParserEmail(emails);
        }

        /// <summary>
        /// Calcula o valor do cte baseado nos calculos
        /// </summary>
        /// <param name="cte">Cte que ter� o seu valor atualizado</param>
        public void CalcularValorCte(Cte cte)
        {
            try
            {
                _cte = cte;
                CarregarDados();

                ValidarDados();
                CalcularValorCte();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Executar a grava��o do cte em um m�todo transacional
        /// </summary>
        [Transaction]
        protected virtual void Executar()
        {
            try
            {
                // ICTE01_INF
                GravarDadosPrincipaisCte();

                // ICTE02_PASS
                GravarGrupoPontosPassagem();

                // ICTE03_OBSCONT
                GravarDadosObservacaoContribuinte();

                // ICTE05_EMIT 
                GravarInformacoesEmitente();

                // ICTE06_EMIT_INF da config - Dados dos documentos associados
                if (ValidarInformacoesDocumentos()) GravarInformacoesDocumentos();

                // ICTE06_EMIT_INF da config     
                if (ValidarInformacoesDocumentos_Cte1_04()) GravarInformacoesDocumentos_Cte1_04();

                // ICTE07_EXPED
                GravarInformacoesExpedidorRecebedorDestinatario();

                // ICTE08_VALORES
                GravarValorPrestacaoServicoImposto();

                // ICTE09_COMP
                GravarComponentesValorPrestacao();

                // ICTE10_ICN
                if (ValidarInformacoesCteNormalAnulacao()) GravarInformacoesCteNormalAnulacao();

                // ICTE11_ICN_INFQ
                if (ValidarInformacoesQtdeCarga()) GravarInformacoesQtdeCarga();

                if (ValidarDadosDocumentosAnterior()) GravarDadosDocumentosAnterior();

                // ICTE12_ICN_CONT da config     
                if (ValidarInformacoesContainerQtdeCarga()) GravarInformacoesContainerQtdeCarga();

                // ICTE14_ICN_DOC
                if (ValidarInformacoesEmitenteDocAnteriorPorCorrentista()) GravarInformacoesEmitenteDocAnteriorPorCorrentista();

                // ICTE15_ICN_DOC_ID
                if (ValidarInformacoesBasicasDocAnterior()) GravarInformacoesBasicasDocAnterior("", 0);

                // ICTE32_INFSERVVINC da config - Dados Vinculado Multimodal.
                if (ValidarInformacoesVinculadoMultimodal()) GravarInformacoesVinculadoMultimodal();

                // ICTE37_ICA
                if (ValidarInformacoesAnulacaoCte()) GravarInformacoesAnulacaoCte();

                // ICTE38_ICS
                if (ValidarInformacoesCteSubstituto()) GravarInformacoesCteSubstituto();

                // ICTE60_FERROV da config - Dados modal ferrovi�rio 
                if (ValidarInformacoesModalFerroviario()) GravarInformacoesModalFerroviario();

                // ICTE62_FERROENV da config - Informa��es das ferrovias envolvidas
                GravarInformacoesFerroviasEnvolvidas();

                // ICTE63_FERRO_DCL da config - 
                if (ValidarInformacoesDCL()) GravarInformacoesDCL();

                // ICTE64_FERRO_DCLDETVAG da config - 
                if (ValidarInformacoesDclDetVag()) GravarInformacoesDclDetVag();

                // ICTE66_FERRO_DCL_CONTVAG da config - 
                if (ValidarInformacoesContVag()) GravarInformacoesContVag();

                // ICTE67_FERRO_DETVAG da config - Dados de detalhes do vag�o 
                if (ValidarInformacoesDetVag()) GravarInformacoesDetVag();

                // ICTE69_FERRO_CONTVAG da config - 
                if (ValidarInformacoeContainerContidoVagao()) GravarInformacoeContainerContidoVagao();

                // ICTE70_FERRO_RATVAG da config - 
                if (ValidarRateioVagao()) GravarRateioVagao();

                GravarCteEmpresas();
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(_cte, "GravarConfigEnvioService", string.Format("Executar: {0}", ex.Message), ex);
                throw;
            }
        }

        /// <summary>
        ///  Valida a grava��o dos dados de multimodal na tabela ICTE32_INFSERVVINC. 
        ///  Somente CTE com tipo de servi�o vinculado a multi modal deve gerar essas informa��es. 
        /// </summary>
        private bool ValidarInformacoesAnulacaoCte()
        {
            try
            {
                return _cte.TipoCte == TipoCteEnum.Anulacao;
            }
            catch (Exception ex)
            {
                LogHelper.Log(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
                throw;
            }
        }

        /// <summary>
        ///  Valida a grava��o dos dados de multimodal na tabela ICTE32_INFSERVVINC. 
        ///  Somente CTE com tipo de servi�o vinculado a multi modal deve gerar essas informa��es. 
        /// </summary>
        private void GravarInformacoesAnulacaoCte()
        {
            try
            {
                Cte37 cte37 = new Cte37();

                // Identificador da filial do CT-e
                cte37.CfgUn = ObterIdentificadorFilial();

                // Numero do documento CT-e (PK)
                int numConvertido;
                int.TryParse(_cte.Numero, out numConvertido);
                cte37.CfgDoc = numConvertido;

                // S�rie do CT-e
                cte37.CfgSerie = int.Parse(_cte.Serie).ToString();

                // Chave do cte original a ser anulado - busca na tabela CTE_ANULADO, q a partir do cte de anula��om pega o cte pai anulado.
                cte37.InfCteAnuEntChCTe = _cteAnuladoRepository.ObterPorIdCteAnulacao(_cte.Id ?? 0).Cte.Chave;


                //Comentamos a condi��o abaixo porque a config est� exigindo a data de anula��o 
                //de n�o contribuinte para todos os tipos de anula��o.

                //Data da anula��o - S� informa quando o tomador for 9
                //if (_fluxoComercial.Contrato.IndIeToma == 9)
                cte37.InfCteAnuEntDemi = _cte.DataAnulacao;

                _cte37Repository.Inserir(cte37);
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(_cte, "GravarConfigCteEnvioService", string.Format("GravarInformacoesAnulacaoCte: {0}", ex.Message), ex);
                LogHelper.Log(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
                throw;
            }
        }

        /// <summary>
        ///  Valida a grava��o dos dados de multimodal na tabela ICTE32_INFSERVVINC. 
        ///  Somente CTE com tipo de servi�o vinculado a multi modal deve gerar essas informa��es. 
        /// </summary>
        private bool ValidarInformacoesVinculadoMultimodal()
        {
            try
            {
                if (_cte.TipoServico == (int)TipoServicoCteEnum.VinculadoMultimodal)
                {
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                LogHelper.Log(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
                throw;
            }
        }

        /// <summary>
        /// Executar a grava��o dos dados do cte de tipo vinculado multi modal na tabela ICTE32_INFSERVVINC
        /// </summary>
        private void GravarInformacoesVinculadoMultimodal()
        {
            try
            {
                Cte32 cte32 = new Cte32();
                int numConvertido;
                var cteOrigem = _cteOrigemRepository.ObterPorCte(_cte).FirstOrDefault();

                // Codigo interno da filial (PK) - ok
                cte32.CfgUn = ObterIdentificadorFilial();

                // Numero do documento CT-e (PK) - ok
                int.TryParse(_cte.Numero, out numConvertido);
                cte32.CfgDoc = numConvertido;

                // Serie do documento CT-e (PK) - ok
                cte32.CfgSerie = int.Parse(_cte.Serie).ToString();

                // Sequencial da tabela
                cte32.IdInfServVinc = 1;

                // se cte origem � nulo, lan�a uma exce�ao, n�o pode ser nulo
                if (cteOrigem == null)
                {
                    throw new Exception("Cte n�o possui cte de origem informado.");
                }

                // Chave do CTE  de origem
                cte32.InfCteMultiModalChcte = cteOrigem.ChaveCte;

                _cte32Repository.Inserir(cte32);
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(_cte, "GravarConfigEnvioService", string.Format("Executar: {0}", ex.Message), ex);
                LogHelper.Log(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
                throw;
            }
        }

        private bool ValidarInformacoesEmitenteDocAnteriorPorCorrentista()
        {
            try
            {
                if (
            ((_cte.TipoServico == (int)TipoServicoCteEnum.SubContratacao) ||
            (_cte.TipoServico == (int)TipoServicoCteEnum.Redespacho) ||
            (_cte.TipoServico == (int)TipoServicoCteEnum.RedespachoIntermediario)) &&
            (_cte.TipoCte != TipoCteEnum.Anulacao)
            )
                {
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                LogHelper.Log(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
                throw;
            }
        }

        private void GravarInformacoesEmitenteDocAnteriorPorCorrentista()
        {
            IEmpresa empresaDocAnterior = _contrato.EmpresaPagadora;

            try
            {
                Cte14 cte14 = new Cte14();
                int numConvertido;
                long cnpj = 0;

                // Codigo interno da filial (PK) - ok
                cte14.CfgUn = ObterIdentificadorFilial();

                // Numero do documento CT-e (PK) - ok
                int.TryParse(_cte.Numero, out numConvertido);
                cte14.CfgDoc = numConvertido;

                // Serie do documento CT-e (PK) - ok
                cte14.CfgSerie = int.Parse(_cte.Serie).ToString();

                // Numero sequecial da tabela (PK) - ok - verificar
                cte14.IdIcnDoc = 1;

                if (empresaDocAnterior.TipoPessoa == TipoPessoaEnum.Juridica)
                {
                    // Recupera o CNPJ - ok
                    if (Int64.TryParse(empresaDocAnterior.Cgc, out cnpj))
                    {
                        cte14.EmiDocAntCnpj = cnpj;
                    }
                    else
                    {
                        cte14.EmiDocAntCnpj = 0;
                    }
                }
                else
                {
                    // Recupera o CPF - ok
                    if (Int64.TryParse(empresaDocAnterior.Cpf, out cnpj))
                    {
                        cte14.EmiDocAntCpf = cnpj;
                    }
                }

                // Inscricao estadual
                cte14.EmiDocAntIe = Tools.TruncateRemoveSpecialChar(empresaDocAnterior.InscricaoEstadual, 14);

                // UF
                if (empresaDocAnterior.Estado != null)
                {
                    cte14.EmiDocAntUf = empresaDocAnterior.Estado.Sigla;
                }
                else
                {
                    cte14.EmiDocAntUf = "EX";
                }

                if (!string.IsNullOrEmpty(empresaDocAnterior.RazaoSocial))
                {
                    // Nome
                    cte14.EmiDocAntXnome = empresaDocAnterior.RazaoSocial.Trim();
                }

                _cte14Repository.Inserir(cte14);
            }
            catch (Exception ex)
            {
                // Limpa o string builder
                _cteLogService.InserirLogErro(_cte, "GravarInformacoesEmitenteDocAnterior", string.Format("Executar: {0}", ex.Message), ex);
                LogHelper.Log(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
                throw;
            }
        }

        private void GravarCteEmpresas()
        {
            try
            {
                /*
                0-Remetente
                1-Expedidor
                2-Recebedor
                3-Destinat�rio
                4-Outros
         */
                switch (codigoTomador)
                {
                    case 0:
                        _cteEmpresas.EmpresaTomadora = _cteEmpresas.EmpresaRemetente;
                        break;
                    case 1:
                        _cteEmpresas.EmpresaTomadora = _cteEmpresas.EmpresaExpedidor;
                        break;
                    case 2:
                        _cteEmpresas.EmpresaTomadora = _cteEmpresas.EmpresaRecebedor;
                        break;
                    case 3:
                        _cteEmpresas.EmpresaTomadora = _cteEmpresas.EmpresaDestinatario;
                        break;
                }

                _cteEmpresas.CodigoTomador = codigoTomador;

                if (_cteEmpresas.EmpresaRemetente != null && _cteEmpresas.EmpresaRemetente.Id == null)
                {
                    _cteEmpresas.EmpresaRemetente = null;
                }

                if (_cteEmpresas.EmpresaDestinatario != null && _cteEmpresas.EmpresaDestinatario.Id == null)
                {
                    _cteEmpresas.EmpresaDestinatario = null;
                }

                _cteEmpresas.Cte = _cte;
                _cteEmpresasRepository.InserirOuAtualizar(_cteEmpresas);
            }
            catch (Exception ex)
            {
                LogHelper.Log(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
                throw;
            }
        }

        private bool ValidarDadosDocumentosAnterior()
        {
            try
            {
                if (_cte.TipoCte == TipoCteEnum.Anulacao)
                {
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                LogHelper.Log(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
                throw;
            }
        }

        private void GravarDadosDocumentosAnterior()
        {
            try
            {
                string codigo = string.Empty;

                if ((_cte.Id == _cteRaizAgrupamento.Id) && (_tipoTrafegoMutuo == TipoTrafegoMutuoEnum.FaturamentoEmpresaGrupo))
                {
                    // Nessa situa��o � o primeiro CTE e o trafego � da propria empresa. N�o tem documentos anteriores
                    return;
                }

                if (_tipoTrafegoMutuo == TipoTrafegoMutuoEnum.FaturamentoEmpresaForaGrupo)
                {
                    return;
                }

                if (_tipoTrafegoMutuo == TipoTrafegoMutuoEnum.FaturamentoArgentina)
                {
                    return;
                }

                if (_tipoTrafegoMutuo == TipoTrafegoMutuoEnum.FaturamentoEmpresaGrupo)
                {
                    // Nessa situa��o � um CTE filho e o trafego � da propia empresa.
                    GravarInformacoesEmitenteDocAnterior();

                    // Recupera o n�mero do despacho e gera os documentos anteriores.
                    codigo = _despachoTranslogic.NumeroDespacho.ToString();
                    GravarInformacoesBasicasDocAnterior(codigo, 99);
                }
                else if (_tipoTrafegoMutuo == TipoTrafegoMutuoEnum.IntercambioFaturamentoOrigem)
                {
                    // Verifica se � o cte Raiz.
                    if (_cte.Id == _cteRaizAgrupamento.Id)
                    {
                        // Recupera os dados do emitente.
                        GravarInformacoesEmitenteDocAnterior();

                        // Verifica se � um fluxo Internacional.
                        codigo = VerificarFluxoInternacional(_fluxoComercial);

                        // Caso retorne diferente de vazio ou nulo, ent�o � fluxo internacional.
                        if (!string.IsNullOrEmpty(codigo))
                        {
                            // Se for fluxo internacional ent�o recupera os dados do cte.
                            GravarInformacoesBasicasDocAnterior(codigo, 12);
                        }
                        else
                        {
                            // Preenche as informa��es b�sicas do doc anterior.
                            codigo = _despachoTranslogic.NumeroDespacho.ToString();
                            if (String.IsNullOrEmpty(codigo))
                            {
                                codigo = _despachoTranslogic.NumDespIntercambio.ToString();
                            }

                            GravarInformacoesBasicasDocAnterior(codigo, 99);
                        }
                    }
                    else
                    {
                        // Recupera os dados do emitente
                        GravarInformacoesEmitenteDocAnterior();

                        // Preenche as informacoes basicas do doc anterior
                        codigo = _despachoTranslogic.NumeroDespacho.ToString();
                        GravarInformacoesBasicasDocAnterior(codigo, 99);
                    }
                }
                else if (_tipoTrafegoMutuo == TipoTrafegoMutuoEnum.IntercambioFaturamentoDestino)
                {
                    // Verifica se � o cte Raiz.
                    if (_cte.Id == _cteRaizAgrupamento.Id)
                    {
                        // Recupera os dados do emitente
                        GravarInformacoesEmitenteDocAnterior();

                        // Verifica se � um fluxo Internacional.
                        codigo = VerificarFluxoInternacional(_fluxoComercial);

                        // Caso retorne diferente de vazio ou nulo, ent�o � fluxo internacional.
                        if (!string.IsNullOrEmpty(codigo))
                        {
                            // Se for fluxo internacional ent�o recupera os dados do cte.
                            GravarInformacoesBasicasDocAnterior(codigo, 12);
                        }
                        else
                        {
                            // Preenche as informa��es b�sicas do doc anterior.
                            codigo = _listaCteDetalhe.FirstOrDefault().NumeroNota.Trim();
                            GravarInformacoesBasicasDocAnterior(codigo, 99);
                        }
                    }
                    else
                    {
                        // Recupera os dados do emitente
                        GravarInformacoesEmitenteDocAnterior();

                        // Preenche as informacoes basicas do doc anterior
                        codigo = _despachoTranslogic.NumeroDespacho.ToString();
                        GravarInformacoesBasicasDocAnterior(codigo, 99);
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.Log(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
                throw;
            }
        }

        /// <summary>
        /// Tabela de informacoes de emitente dos doc anteriores (0-N) - ICTE14_ICN_DOC
        /// </summary>
        private void GravarInformacoesEmitenteDocAnterior()
        {
            IEmpresa empresaDocAnterior = _empresaFerroviaCliente;

            try
            {
                Cte14 cte14 = new Cte14();
                int numConvertido;
                long cnpj = 0;

                // Codigo interno da filial (PK)
                cte14.CfgUn = ObterIdentificadorFilial();

                // Numero do documento CT-e (PK)
                int.TryParse(_cte.Numero, out numConvertido);
                cte14.CfgDoc = numConvertido;

                // Serie do documento CT-e (PK)
                cte14.CfgSerie = int.Parse(_cte.Serie).ToString();

                // Numero sequecial da tabela (PK)
                cte14.IdIcnDoc = 1;

                if (empresaDocAnterior.TipoPessoa == TipoPessoaEnum.Juridica)
                {
                    // Recupera o CNPJ
                    if (Int64.TryParse(empresaDocAnterior.Cgc, out cnpj))
                    {
                        cte14.EmiDocAntCnpj = cnpj;
                    }
                    else
                    {
                        cte14.EmiDocAntCnpj = 0;
                    }
                }
                else
                {
                    // Recupera o CPF
                    if (Int64.TryParse(empresaDocAnterior.Cpf, out cnpj))
                    {
                        cte14.EmiDocAntCpf = cnpj;
                    }
                }

                // Inscricao estadual
                cte14.EmiDocAntIe = Tools.TruncateRemoveSpecialChar(empresaDocAnterior.InscricaoEstadual, 14);

                // UF
                if (empresaDocAnterior.Estado != null)
                {
                    cte14.EmiDocAntUf = empresaDocAnterior.Estado.Sigla;
                }
                else
                {
                    cte14.EmiDocAntUf = "EX";
                }

                if (!string.IsNullOrEmpty(empresaDocAnterior.RazaoSocial))
                {
                    // Nome
                    cte14.EmiDocAntXnome = empresaDocAnterior.RazaoSocial.Trim();
                }

                _cte14Repository.Inserir(cte14);
            }
            catch (Exception exception)
            {
                // Limpa o string builder
                _cteLogService.InserirLogErro(_cte, "GravarInformacoesEmitenteDocAnterior", string.Format("Executar: {0}", exception.Message));
            }
        }

        /// <summary>
        /// Tabela de informacoes basicas dois doc de transportes anteriores (1,N) - ICTE15_ICN_DOC_ID
        /// </summary>
        /// <param name="numeroDoc">Numero do documento anterior</param>
        /// <param name="tipoDoc">Tipo do Documento</param>
        private bool ValidarInformacoesBasicasDocAnterior()
        {
            try
            {
                if (
            ((_cte.TipoServico == (int)TipoServicoCteEnum.SubContratacao) ||
             (_cte.TipoServico == (int)TipoServicoCteEnum.Redespacho) ||
             (_cte.TipoServico == (int)TipoServicoCteEnum.RedespachoIntermediario)) &&
            (_cte.TipoCte != TipoCteEnum.Anulacao)
        )
                {
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                LogHelper.Log(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
                throw;
            }
        }

        /// <summary>
        /// Tabela de informacoes basicas dois doc de transportes anteriores (1,N) - ICTE15_ICN_DOC_ID
        /// </summary>
        /// <param name="numeroDoc">Numero do documento anterior</param>
        /// <param name="tipoDoc">Tipo do Documento</param>
        private void GravarInformacoesBasicasDocAnterior(string numeroDoc, int tipoDoc)
        {
            try
            {
                Cte15 cte15 = new Cte15();
                int numConvertido;

                // Codigo interno da filial (PK)
                cte15.CfgUn = ObterIdentificadorFilial();

                // Numero do documento CT-e (PK)
                int.TryParse(_cte.Numero, out numConvertido);
                cte15.CfgDoc = numConvertido;

                // Serie do documento CT-e (PK)
                // cte15.CfgSerie = _cte.Serie;
                cte15.CfgSerie = int.Parse(_cte.Serie).ToString();

                // Numero sequecial da tabela (PK)
                cte15.IdIcnDoc = 1;

                // Numero sequecial da tabela (PK)
                cte15.IdIcnDocId = 1;
                // Se for manual passa o pab
                CteDetalhe cteDetalhe = _listaCteDetalhe[0];
                if (String.IsNullOrEmpty(cteDetalhe.ChaveNfe))
                {
                    // Tipo de documento de transporte anterior
                    cte15.IdDocAntPapTpdoc = tipoDoc;

                    // S�rie
                    if (_cte.Id != _cteRaiz.Id)
                    {
                        cte15.IdDocAntPapSerie = int.Parse(_cteRaizAgrupamento.Serie).ToString();

                        // Sub-s�rie
                        cte15.IdDocAntPapSubser = string.Empty;

                        // Data da emiss�o
                        cte15.IdDocAntPapDemi = _cteRaizAgrupamento.DataHora;
                    }
                    else
                    {
                        cte15.IdDocAntPapSerie = int.Parse(_cte.Serie).ToString();
                        // Data da emiss�o
                        cte15.IdDocAntPapDemi = _cte.DataHora;
                    }

                    // N�mero
                    cte15.IdDocAntPapNDoc = numeroDoc;
                }
                else
                {
                    if ((_cte.TipoServico == (int)TipoServicoCteEnum.SubContratacao) || (_cte.TipoServico == (int)TipoServicoCteEnum.Redespacho) || (_cte.TipoServico == (int)TipoServicoCteEnum.RedespachoIntermediario))
                    {
                        var cteOrigem = _cteOrigemRepository.ObterPorCte(_cte).FirstOrDefault();
                        if (cteOrigem != null)
                        {
                            cte15.IdDocAntEleChave = cteOrigem.ChaveCte ?? "";
                        }
                        else
                        {
                            cte15.IdDocAntEleChave = "";
                        }
                    }
                    else
                    {
                        // Chave de acesso do CT-e
                        if (!String.IsNullOrEmpty(_cteRaizAgrupamento.Chave))
                        {
                            cte15.IdDocAntEleChave = _cteRaizAgrupamento.Chave;
                        }
                    }
                }

                _cte15Repository.Inserir(cte15);
            }
            catch (Exception ex)
            {
                // Limpa o string builder
                _cteLogService.InserirLogErro(_cte, "GravarConfigCteEnvioService", string.Format("GravarInformacoesBasicasDocAnterior: {0}", ex.Message), ex);
                LogHelper.Log(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
                throw;
            }
        }

        /// <summary>
        /// Tabela de grupo dos pontos de passagem (0-N) - ICTE02_PASS
        /// </summary> 
        private void GravarGrupoPontosPassagem()
        {
            try
            {
                Cte02 cte02 = new Cte02();
                cte02.CfgUn = ObterIdentificadorFilial(); // Codigo interno da filial
                cte02.CfgDoc = Convert.ToInt32(_cte.Numero); // Numero do documento CT-e
                // cte02.CfgSerie = _cte.Serie; 
                cte02.CfgSerie = int.Parse(_cte.Serie).ToString(); // Serie do documento CT-e

                cte02.IdPass = 0; // Numero sequecial da tabela
                cte02.PassXpass = "ESTACAO"; // Sigla ou c�digo interno da Filial/Porto/Esta��o/Aeroporto de Passagem
                _cte02Repository.InserirOuAtualizar(cte02);
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(_cte, "GravarConfigCteEnvioService", string.Format("GravarGrupoPontosPassagem: {0}", ex.Message), ex);
                LogHelper.Log(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
                throw;
            }
        }

        /// <summary>
        /// Tabela utilizada para dados principais do CT-e (1-1) - ICTE01_INF
        /// </summary>
        private void GravarDadosPrincipaisCte()
        {
            Cte01 cte01 = null;

            try
            {
                int intAux;

                ConfiguracaoTranslogic formaEmissao = _configuracaoTranslogicRepository.ObterPorId(_chaveCteFormaEmissao);
                //ConfiguracaoTranslogic tipoAmbiente = _configuracaoTranslogicRepository.ObterPorId(_chaveCteTipoAmbiente);
                ConfiguracaoTranslogic versaoProcesso = _configuracaoTranslogicRepository.ObterPorId(_chaveCteVersaoProcesso);
                // ConfiguracaoTranslogic versaoSefaz = _configuracaoTranslogicRepository.ObterPorId(_chaveVersaoSefaz);

                CidadeIbge cidadeIbgeCalculoFrete;
                if ((_cte.Id == _cteRaizAgrupamento.Id) && (_tipoTrafegoMutuo == TipoTrafegoMutuoEnum.FaturamentoEmpresaGrupo))
                {
                    // TipoTrafegoMutuoEnum.FaturamentoEmpresaGrupo
                    cidadeIbgeCalculoFrete = _cidadeIbgeEmpresaRemetenteContrato;
                }
                else if (_tipoTrafegoMutuo == TipoTrafegoMutuoEnum.FaturamentoEmpresaForaGrupo)
                {
                    // TipoTrafegoMutuoEnum.FaturamentoEmpresaForaGrupo
                    cidadeIbgeCalculoFrete = _cidadeIbgeEmpresaRemetenteContrato;
                }
                else
                {
                    // Tipo CTE filho ou Trafego Mutuo IntercambioFaturamentoOrigem ou IntercambioFaturamentoDestino
                    cidadeIbgeCalculoFrete = _cidadeIbgeEmpresaFerrovia;
                }

                cte01 = new Cte01();

                // Codigo interno da filial (PK)
                cte01.CfgUn = ObterIdentificadorFilial();

                // Numero do documento CT-e (PK)
                int.TryParse(_cte.Numero, out intAux);
                cte01.CfgDoc = intAux;

                // Serie do documento CT-e (PK)
                // cte01.CfgSerie = _cte.Serie;
                cte01.CfgSerie = int.Parse(_cte.Serie).ToString();

                // Versao do CT-e
                cte01.InfCteVersao = double.Parse(_cte.Versao.CodigoVersao, CultureInfo.GetCultureInfo("en-US"));

                // Identificador da TAG a ser assinada
                cte01.InfCteId = _cte.Id.ToString();

                // cte01.IdeCuf = objUfIbge.Id;
                string codufFerrovia = _cte.Chave.Substring(0, 2);
                cte01.IdeCuf = Convert.ToInt32(codufFerrovia);

                // C�digo num�rico gerado aleatoriamente.
                string codVag = "9" + _vagao.Codigo;
                if (int.TryParse(codVag, out intAux))
                {
                    cte01.IdeCct = intAux;
                }

                // C�digo CFOP retirado a partir da tabela CONTRATO.
                if (int.TryParse(_cfop, out intAux))
                {
                    cte01.IdeCfOp = intAux;
                }

                // Natureza da opera��o
                cte01.IdeNatOp = _cfopDescricao;

                // Modelo do documento fiscal. Sempre fixo em 57.
                cte01.IdeMod = "57";

                // S�rie do CTE
                cte01.IdeSerie = int.TryParse(_cte.Serie, out intAux) ? intAux : 0;

                // N�mero do CTE
                cte01.IdeNct = int.TryParse(_cte.Numero, out intAux) ? intAux : 0;

                // Data e hora da emiss�o do CTE
                cte01.IdeDhEmi = GetDhEmissao();

                // Formato impressao 1 - Retrato
                cte01.IdeTpImp = 1;

                // Tipo da emiss�o se � normal ou conting�ncia.
                cte01.IdeTpEmis = int.TryParse(formaEmissao.Valor, out intAux) ? intAux : 0;

                // D�gito verificador da chave de acesso do CTE
                cte01.IdeCdv = Convert.ToInt32(_cte.Chave.Substring(_cte.Chave.Length - 1, 1));

                // Ambiente de homologa��o ou produ��o
                cte01.IdeTpAmb = _cte.AmbienteSefaz;

                if (_cte.TipoCte == TipoCteEnum.Substituto) // Tipo do CTE. (3-Substituto).
                {
                    cte01.IdeTpCte = 3;
                }
                else
                    if (_cte.TipoCte == TipoCteEnum.Anulacao) // Tipo do CTE. (3-Substituto).
                {
                    cte01.IdeTpCte = 2;
                }
                else // Tipo do CTE. (0-Normal).
                {
                    cte01.IdeTpCte = 0;
                }

                // Processo de emiss�o. Hoje � usado 0=Emiss�o com aplicativo do contribuinte.
                cte01.IdeProcEmi = 0;

                // Vers�o do processo de emiss�o.
                cte01.IdeVerProc = versaoProcesso.Valor;

                //// CTE referenciado
                //// if (_cte.TipoCte != TipoCteEnum.Normal)
                //// cte01.IdeRefcte = _cteRaizAgrupamento.Chave != _cte.Chave ? _cteRaizAgrupamento.Chave : string.Empty;

                // C�digo do munic�pio de onde o CTE est� sendo emitido.
                CidadeIbge cidadeEmitente = _cidadeIbgeRepository.ObterPorDescricaoUf(_nfe03Filial.Cidade.ToUpper().Trim(), _cte.SiglaUfFerrovia);
                if (cidadeEmitente == null) throw new Exception(string.Format("Cidade IBGE (Cidade: {0}, Uf: {1}) n�o cadastrada.", _nfe03Filial.Cidade.ToUpper().Trim(), _cte.SiglaUfFerrovia));
                cte01.IdeCMunEmi = cidadeEmitente.CodigoIbge;

                // Nome do munic�pio de origem do fluxo
                if (cidadeEmitente != null)
                {
                    if (cidadeEmitente.CodigoIbge == 9999999)
                    {
                        cte01.IdeXMunEmi = "EXTERIOR";
                        cte01.IdeUfEmi = "EX";
                    }
                    else
                    {
                        cte01.IdeXMunEmi = cidadeEmitente.Descricao;
                        // C�digo UF do munic�pio de origem do fluxo.
                        cte01.IdeUfEmi = cidadeEmitente.SiglaEstado;
                    }
                }

                // Modal do transporte. Hoje � usado 04=Ferrovi�rio.
                cte01.IdeModal = 4;

                // Tipo de servi�o.
                cte01.IdeTpServ = _cte.TipoServico == null ? 0 : (int)_cte.TipoServico;

                // Tipo de servi�o. Hoje � usado 0.
                // cte01.IdeTpServ = 0;

                if (_cidadeIbgeEstacaoMaeOrigemFluxoComercial != null)
                {
                    // C�digo IBGE do munic�pio de origem do fluxo.
                    cte01.IdeCMunIni = _cidadeIbgeEstacaoMaeOrigemFluxoComercial.CodigoIbge;

                    if (_cidadeIbgeEstacaoMaeOrigemFluxoComercial.CodigoIbge == 9999999)
                    {
                        // Nome do munic�pio de origem do fluxo.
                        cte01.IdeXMunIni = "EXTERIOR";
                        // C�digo UF do munic�pio de origem do fluxo.
                        cte01.IdeUfIni = "EX";
                    }
                    else
                    {
                        // Nome do munic�pio de origem do fluxo.
                        cte01.IdeXMunIni = _municipioEstacaoMaeOrigemFluxoComercial.Descricao;
                        // C�digo UF do munic�pio de origem do fluxo.
                        cte01.IdeUfIni = _municipioEstacaoMaeOrigemFluxoComercial.CidadeIbge.SiglaEstado;
                    }
                }

                if (_cidadeIbgeEstacaoMaeDestinoFluxoComercial != null)
                {
                    // C�digo IBGE do munic�pio de destino do fluxo.
                    cte01.IdeCMunFim = _cidadeIbgeEstacaoMaeDestinoFluxoComercial.CodigoIbge;
                    if (_cidadeIbgeEstacaoMaeDestinoFluxoComercial.CodigoIbge == 9999999)
                    {
                        // Nome do munic�pio de destino do fluxo.
                        cte01.IdeXMunFim = "EXTERIOR";

                        // C�digo UF do munic�pio de destino do fluxo.
                        cte01.IdeUfFim = "EX";
                    }
                    else
                    {
                        // Nome do munic�pio de destino do fluxo.
                        cte01.IdeXMunFim = _municipioEstacaoMaeDestinoFluxoComercial.Descricao;

                        // C�digo UF do munic�pio de destino do fluxo.
                        cte01.IdeUfFim = _municipioEstacaoMaeDestinoFluxoComercial.CidadeIbge.SiglaEstado;
                    }
                }

                // Informa��es da retirada da carga. Hoje fixo 0.
                cte01.IdeRetira = 0;

                // Detalhes do retira
                cte01.IdeXDetRetira = string.Empty;

                // Pega a informa��o do tomador aqui.
                var toma = GetUtilizaToma();
                var utilizaToma3 = (toma >= 0);

                if (utilizaToma3) cte01.Toma03Toma = toma;

                ////if (!utilizaToma3 || (_carregamentoService.VerificarFluxoConteiner(_fluxoComercial) || VerificaEmpresaBrado(_contrato.EmpresaPagadora)))

                if (!utilizaToma3)
                {
                    cte01.Toma03Toma = null;
                    cte01.Toma4Toma = 4;
                    long cnpj = 0;

                    // Armazena qual a empresa tomadora.
                    _cteEmpresas.EmpresaTomadora = _empresaTomadora;

                    if (_empresaTomadora.TipoPessoa == TipoPessoaEnum.Juridica)
                    {
                        // CNPJ do tomador de servi�o. Se for pessoa jur�dica
                        if (Int64.TryParse(_empresaTomadora.Cgc, out cnpj))
                        {
                            cte01.Toma4Cnpj = cnpj;
                        }
                    }
                    else
                    {
                        // CPF do tomador de servi�o. Se for pessoa f�sica.
                        if (Int64.TryParse(_empresaTomadora.Cpf, out cnpj))
                        {
                            cte01.Toma4Cpf = cnpj;
                        }
                    }

                    // Inscri��o estadual do tomador. Campo opcional.
                    //cte01.Toma4Ie = _cte.ObterTipoTomador() != 9 ? Tools.TruncateRemoveSpecialChar(_empresaTomadora.InscricaoEstadual, 14) : null;
                    cte01.Toma4Ie = Tools.TruncateRemoveSpecialChar(_empresaTomadora.InscricaoEstadual, 14);

                    if (!string.IsNullOrEmpty(_empresaTomadora.RazaoSocial))
                    {
                        // Raz�o social da empresa. 
                        cte01.Toma4xNome = _empresaTomadora.RazaoSocial.Trim();
                    }

                    // Nome fantasia da empresa
                    cte01.Toma4xFant = _empresaTomadora.NomeFantasia;

                    // Telefone da empresa
                    cte01.Toma4Fone = ValidarTelefone(_empresaTomadora.Telefone1);
                    if (string.IsNullOrEmpty(cte01.Toma4Fone))
                    {
                        cte01.Toma4Fone = null;
                    }
                }

                // Armazena qual o tomador utilizado
                codigoTomador = cte01.Toma03Toma.HasValue ? cte01.Toma03Toma.Value : cte01.Toma4Toma.Value;

                // Logradouro do tomador do servi�o
                if (!string.IsNullOrEmpty(_empresaTomadora.Endereco))
                {
                    cte01.EnderTomaxLgr = _empresaTomadora.Endereco.Trim();
                }

                // N�mero.
                // cte01.EnderTomaNro = _empresaFerrovia.Numero;
                cte01.EnderTomaNro = "S/N";

                // Complemento do endere�o.
                cte01.EnderTomaXCpl = string.Empty;

                // Bairro do endere�o do tomador
                cte01.EnderTomaXBairro = "NAO INFORMADO";

                if (_cidadeIbgeEmpresaTomadora != null)
                {
                    // C�digo IBGE do munic�pio do tomador
                    cte01.EnderTomaCMun = _cidadeIbgeEmpresaTomadora.CodigoIbge;

                    if (_cidadeIbgeEmpresaTomadora.CodigoIbge == 9999999)
                    {
                        // Nome do munic�pio do tomador
                        cte01.EnderTomaXMun = "EXTERIOR";
                        cte01.EnderTomaUf = "EX";
                    }
                    else
                    {
                        // Nome do munic�pio do tomador
                        cte01.EnderTomaXMun = _cidadeIbgeEmpresaTomadora.Descricao;

                        // C�digo UF.
                        cte01.EnderTomaUf = _cidadeIbgeEmpresaTomadora.SiglaEstado;
                    }
                }

                try
                {
                    // CEP.( remove '-' caso tiver e converte para numerico
                    int index = _empresaTomadora.Cep.IndexOf('-');
                    cte01.EnderTomaCep = index > -1 ? int.Parse(_empresaTomadora.Cep.Remove(index, 1)) : int.Parse(_empresaTomadora.Cep);
                    if (cte01.EnderTomaCep > 99999999)
                    {
                        // Garante que o tamanho maximo do cep em 8 casas
                        cte01.EnderTomaCep = null;
                    }
                }
                catch (Exception)
                {
                    cte01.EnderTomaCep = null;
                }

                // Recupera o PaisBacen
                PaisBacen paisBacen = null;

                if (_cidadeIbgeEmpresaTomadora.CodigoIbge == 9999999)
                {
                    string siglaPaisAux = ObterSiglaPaisExteriorPeloSap(_empresaTomadora);
                    paisBacen = _paisBacenRepository.ObterPorSiglaResumida(siglaPaisAux);
                    cte01.Toma4Cnpj = 0;
                }
                else
                {
                    paisBacen = _paisBacenRepository.ObterPorSiglaResumida(_cidadeIbgeEmpresaTomadora.SiglaPais);
                }

                // C�digo BACEN do pa�s.
                if (int.TryParse(paisBacen.CodigoBacen.Substring(1, 4), out intAux))
                {
                    cte01.EnderTomaCPais = intAux;
                }

                // Nome do pa�s.
                cte01.EnderTomaXPais = paisBacen.Nome;

                // Texto livre. O SAP utiliza �ENTREGA�.
                cte01.ComplXCaracAd = "ENTREGA";

                // Texto livre para caracter�stica adicional do servi�o.
                cte01.ComplXCaracSer = "LOGISTICA";

                // Funcion�rio emissor do CTe
                cte01.ComplXEmi = string.Empty;

                // Sigla ou c�digo interno da Filial/Porto/Esta��o/ Aeroporto de Origem
                cte01.FluxoXOrig = "ESTACAO";

                // Sigla ou c�digo interno da Filial/Porto/Esta��o/ Aeroporto de Destin
                cte01.FluxoXDest = "ESTACAO";

                // C�digo da Rota de EntregA
                cte01.FluxoXRota = string.Empty;

                // Tipo de data/per�odo programado para a entrega
                cte01.SemDataTpPer = string.Empty;

                // Tipo de data/per�odo programado para a entrega
                cte01.ComDataTpPer = string.Empty;

                // Data programada
                cte01.ComDataDProg = null;

                // Tipo de data/per�odo programado para a entrega
                cte01.NoPeriodoTpPer = string.Empty;

                // Data inicial
                cte01.NoPeriodoDIni = null;

                // Data final
                cte01.NoPeriodoDFim = null;

                // Tipo de hora/per�odo programado para a entrega
                cte01.SemHoraTpHor = string.Empty;

                // Tipo de data/per�odo programado para a entrega
                cte01.ComHoraTpHor = string.Empty;

                // Hora programada (HH:MM:SS)
                cte01.ComHoraHProg = string.Empty;

                // Tipo de data/per�odo programado para a entrega
                cte01.NoInterTpHor = string.Empty;

                // Hora inicial (HH:MM:SS)
                cte01.NoInterHIni = string.Empty;

                // Hora Final (HH:MM:SS)
                cte01.NoInterHFim = string.Empty;

                // munic�pio origem para efeito de c�lculo do frete
                cte01.ComplOrigCalc = cidadeIbgeCalculoFrete.Descricao ?? string.Empty;

                // munic�pio destino para efeito de c�lculo do frete
                cte01.ComplDestCalc = cidadeIbgeCalculoFrete.Descricao ?? string.Empty;

                // Observa��es Gerais
                string obs = string.Empty;

                if (_cte.TipoCte == TipoCteEnum.Anulacao)
                {
                    obs += ObterMensagemCteAnulando();
                }
                else
                {
                    obs = ObterMensagemVencimento();
                    obs += ObterMensagemSerieDespacho();

                    if (!string.IsNullOrEmpty(_contratoHistorico.UnidadeMonetaria))
                    {
                        if (_contratoHistorico.UnidadeMonetaria.ToUpper() == "USD")
                        {
                            obs += " ??";
                            obs += "MOEDA:" + _contratoHistorico.UnidadeMonetaria.ToUpper();
                        }
                    }

                    if (_contrato.MensagemFiscal != null)
                    {
                        obs += " ??";
                        obs += _contrato.MensagemFiscal.Trim();
                    }

                    if (_contrato.MensagemFiscalExp != null)
                    {
                        obs += " ??";
                        obs += _contrato.MensagemFiscalExp.Trim();
                    }

                    if (_contrato.MensagemFiscalExp != null)
                    {
                        obs += " ??";
                        obs += _contrato.MensagemFiscalExp.Trim();
                    }

                    obs += ObterMensagemNotasFiscais();

                    if (_cte.SiglaUfFerrovia == "MS")
                    {
                        string obsms = CalcularObservacaoMS();
                        // Se n�o for substitui��o tribut�ria, ent�o exibe os dados de desconto
                        if (!string.IsNullOrEmpty(obsms))
                        {
                            obs += obsms;
                        }
                        else
                        {
                            obs += ObterMensagemDesconto();
                        }
                    }
                    else
                    {
                        obs += ObterMensagemDesconto();
                    }

                    obs += ObterMensagemDocumentoCliente();
                    obs += ObterMensagemPisCofins();
                    obs += ObterMensagemCteSubstituto();
                    obs += ObterMensagemVagao();
                    obs += ObterMensagemIcmsSubst();

                    obs += ObterMensagemDataDescarga();
                }

                _cte.Observacao = obs;
                cte01.ComplxObs = obs;

                // N�mero da fatura
                cte01.FatNFat = string.Empty;

                // Valor original da fatura
                cte01.FatVOrig = null;

                // Valor do desconto da fatura
                cte01.FatVDesc = null;

                // Valor l�quido da fatura
                cte01.FatVLiq = null;

                // Indicador do papel do tomador na presta��o de servi�o - pega do contrato do fluxo do cte.
                Contrato contrato = _contratoRepository.ObterPorId(_fluxoComercial.Contrato.Id);
                cte01.IdeIndIeToma = contrato.IndIeToma;

                // Valida��o das regular expression
                InvalidValue[] ret = _validatorEngine.Validate(cte01);

                // Efetua o tratamento do retorno da valida��o das regular expression
                TratarRetornoRegexSefaz(ret);

                // Inserir
                _cte01Repository.InserirOuAtualizar(cte01);
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(_cte, "GravarConfigCteEnvioService", string.Format("GravarDadosPrincipaisCte: {0}", ex.Message), ex);
                LogHelper.Log(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
                throw;
            }
        }

        private string ObterMensagemVagao()
        {
            var valor = string.Empty;
            if (_cte != null)
            {
                valor = string.Format("VAG�O: {0} - {1} ??", _cte.ObterSerieVagao(), _cte.ObterCodigoVagao());
            }

            return valor;
        }

        private string ObterMensagemIcmsSubst()
        {
            string valor = null;

            var icms = Tools.ParseList(CacheManager.ConfigGeral.Value.EmpresasIcmsSt);

            // verifica se o cnpj est� na confura��o
            var valido = icms.Any(p => _contrato.EmpresaPagadora.Cgc.StartsWith(p));
#if DEBUG
            valido = true;
#endif
            if (valido)
            {
                var compFrete = Spd.BLL.BL_SapComposicaoFreteCte.SelectSingle(new Spd.Data.SapComposicaoFreteCte { IdSapDespCte = _cte.Id, CondicaoTarifa = "ZIC4" });
                // SAP_COMPOSICAO_FRETE_CTE CondicaoTarifa=ZIC4, pegar campo ValorComIcms 

                if (_contrato.TipoTributacao == 3)
                {
                    valor = $"Valor ICMS ST: {compFrete.ValorComIcms.GetValueOrDefault().ToString("N2")} ??";
                }
            }
            return valor;
        }

        private int GetUtilizaToma()
        {
            // -> REGRA ANTIGA CANCELADA, CONFORME ATIVIDADE http://jira/browse/TL-1664 E ACORDADA EM E-MAIL PELO Jodicler Fistarol - Gerencia Tribut�ria - Superintend�ncia Financeira (28/12/2012)
            /////*
            ////Regra do frete 0-Pago, 1-A Pagar e 2-Outros
            ////Frete Pago: Quando o tomador de servico (FluxoComercial.EmpresaPagadora) igual ao Remetente (FluxoComercial.EmpresaRemetente)
            ////Frete a pagar: Quando o tomador de servico(FluxoComercial.EmpresaPagadora) igual ao Destinatario (FluxoComercial.EmpresaDestinataria)
            ////Caso o tomador seja diferente tanto do Remetente como do Destinatario o frete � Outros (03/05/2012)
            ////Obs:
            //// */
            ////if (_empresaTomadora.Id == _empresaRemetenteFluxoComercial.Id)
            ////{
            ////    // Forma de pagamento. Quando o tomador de servi�o � Remetente, a Forma de Pagamento � Pago(0).
            ////    cte01.IdeForPag = "0";
            ////}
            ////else if (_empresaTomadora.Id == _empresaDestinatariaFluxoComercial.Id)
            ////{
            ////    // Forma de pagamento. Quando o tomador de servi�o n�o � o Remetente, a Forma de Pagamento � A Pagar(1).
            ////    cte01.IdeForPag = "1";
            ////}
            ////else
            ////{
            ////    // Forma de pagamento. Quando o tomador de servi�o � Remetente, a Forma de Pagamento � Outros(2).
            ////    cte01.IdeForPag = "2";
            ////}

            /*
                -> NOVA REGRA, CONFORME ATIVIDADE http://jira/browse/TL-1664 E ACORDADA EM E-MAIL PELO Jodicler Fistarol - Gerencia Tribut�ria - Superintend�ncia Financeira (28/12/2012)
                Regra do frete 0-Pago, 1-A Pagar e 2-Outros
                Frete Pago: Quando o tomador de servico (FluxoComercial.EmpresaPagadora) igual ao Remetente FISCAL (FluxoComercial.EmpresaRemetente)
                Frete a pagar: Quando o tomador de servico(FluxoComercial.EmpresaPagadora) igual ao Destinatario FISCAL (FluxoComercial.EmpresaDestinataria)
                Caso o tomador seja diferente tanto do Remetente como do Destinatario o frete � Outros (03/05/2012)
                Obs:                
                 */

            //// a tag IdeForPag se tornou obsoleta na vers�o 3.0
            ////if (_empresaTomadora.Id == _empresaContratoRemetenteFiscal.Id)
            ////{
            ////    // Forma de pagamento. Quando o tomador de servi�o � Remetente, a Forma de Pagamento � Pago(0).
            ////    cte01.IdeForPag = "0";
            ////}
            ////else if (_empresaTomadora.Id == _empresaContratoDestinatariaFiscal.Id)
            ////{
            ////    // Forma de pagamento. Quando o tomador de servi�o n�o � o Remetente, a Forma de Pagamento � A Pagar(1).
            ////    cte01.IdeForPag = "1";
            ////}
            ////else
            ////{
            ////    // Forma de pagamento. Quando o tomador de servi�o � Remetente, a Forma de Pagamento � Outros(2).
            ////    cte01.IdeForPag = "2";
            ////}


            // if ((_cte.Id == _cteRaizAgrupamento.Id) && (_tipoTrafegoMutuo == TipoTrafegoMutuoEnum.FaturamentoEmpresaGrupo) && (! _cteService.VerificarLiberacaoTravasCorrentista(_fluxoComercial)))
            // 
            // {
            // Primeiro CTE do agrupamento e o tipo do trafego � FaturamentoEmpresaGrupo
            // Dados do tomador do servi�o. 0 - Remetente
            //	cte01.Toma03Toma = 0;
            // }
            // else
            // {
            // Nesse caso dever� ser preenchido os dados do Toma04 com os dados da ferrovia de origem
            // por ser um CTE filho ou o tipo de trafego � IntercambioFaturamentoOrigem ou IntercambioFaturamentoDestino

            /* Dados do tomador de servi�o
                Regra do tomador de servi�o caso o tomador seja:
                    0-Remetente
                    1-Expedidor
                    2-Recebedor
                    3-Destinat�rio
                    4-Outros
                 * 
                 * IEmpresa empresaRemetente = _empresaContratoRemetenteFiscal
                 * IEmpresa empresaExpedidor = _empresaRemetenteFluxoComercial;
                 * IEmpresa empresaRecebedor = _empresaDestinatariaFluxoComercial;
                 * IEmpresa empresaDestinataria = _empresaContratoDestinatariaFiscal;
                 * Regra Jodi: Quando o tomador for igual ao remetente fiscal e igual ao expedidor o valor do 
                 *             Toma03 � 0
                 *             Caso o tomador SEJA igual ao recebedor e igual ao destinatario 
                 *             Toma03 � 3
                 *             Brado: Quando for Brado utilizar cte01.Toma4Toma = 4;
                 *             Quandoo CTE tiver uma NFe exterior, n�o pode ser toma3 = 0 (primeiro "if")
             
                 OBS: dia 25/04/2017, foi alterada a regra Jodi para regra Josi. N�o existe mais partiularidade pra brado, conforme a valida��o acima.
                 a regra do tomador vale pra todos, ou seja, se n�o for toma3, entra no if do toma4
             
             */

            // Verifica se � um Fluxo da Brado

            /*	
                 * Regra antiga: Foi alterada para realizar a valida��o na PKG - Estava passando o tomador errado.
                 * if (
                        (_empresaTomadora.Id == _empresaContratoRemetenteFiscal.Id)
                        && (_empresaTomadora.Id == _empresaRemetenteFluxoComercial.Id)
                        && TemNotaExteriorDeEntrada(_cte)
                    )
                    {
                        // -> quando o cte tiver uma nota exterior de entrada, nao pode ser toma3 = 0; tem que ser tome3 = 3
                        cte01.Toma03Toma = 3;
                        utilizaToma3 = true;
                    }
                    else
                 */

            int resultado = -1;

            if ((_empresaTomadora.Id == _empresaContratoRemetenteFiscal.Id) &&
                (_empresaTomadora.Id == _empresaRemetenteFluxoComercial.Id))
            {
                resultado = 0;
            }
            else if ((_empresaTomadora.Id == _empresaDestinatariaFluxoComercial.Id) &&
                     (_empresaTomadora.Id == _empresaContratoDestinatariaFiscal.Id))
            {
                resultado = 3;
            }
            else if (_empresaTomadora.Id == _empresaContratoRemetenteFiscal.Id)
            {
                resultado = 0;
            }
            else if (_empresaTomadora.Id == _empresaContratoDestinatariaFiscal.Id)
            {
                resultado = 3;
            }
            else if (_empresaTomadora.Id == _empresaRemetenteFluxoComercial.Id)
            {
                resultado = 1;
            }
            else if (_empresaTomadora.Id == _empresaDestinatariaFluxoComercial.Id)
            {
                resultado = 2;
            }
            return resultado;
        }

        /*	
            Regra antiga: Foi alterada para realizar a valida��o na PKG - Estava passando o tomador errado.
		
        private bool TemNotaExteriorDeEntrada(Cte _cte)
        {
            var detalhes = _cteDetalheRepository.ObterPorCte(_cte);

            if (detalhes != null && detalhes.Count > 0)
            {
                var detalhe = detalhes.FirstOrDefault(e => e.UfRemetente == "EX");

                if (detalhe != null && _tipoNotaCte == TipoNotaCteEnum.NotaEntrada)
                {
                    return true;
                }
            }

            return false;
        }
        */
        private void GravarDadosObservacaoContribuinte()
        {
            int intAux = 0;
            int idFilial = ObterIdentificadorFilial();
            int.TryParse(_cte.Numero, out intAux);
            int documento = intAux;
            string serie = int.Parse(_cte.Serie).ToString();

            string serieDespacho;
            string numeroDespacho;
            string dataDespacho;

            Cte03 cte03 = null;
            try
            {
                int itemObs = 0;

                // Campo Vencimento
                if (_cte.Vencimento != null)
                {
                    cte03 = new Cte03();
                    cte03.CfgUn = idFilial;
                    cte03.CfgDoc = documento;
                    cte03.CfgSerie = serie;

                    itemObs++;
                    cte03.IdObsCont = itemObs;
                    cte03.ObsContxCampo = "VENCIMENTO:";
                    cte03.ObsContxTexto = String.Format("{0:dd/MM/yyyy}", _cte.Vencimento.Value);
                    _cte03Repository.Inserir(cte03);
                }

                // Campo DCL
                cte03 = new Cte03();
                cte03.CfgUn = idFilial;
                cte03.CfgDoc = documento;
                cte03.CfgSerie = serie;

                itemObs++;
                cte03.IdObsCont = itemObs;
                cte03.ObsContxCampo = "DCL:";
                cte03.ObsContxTexto = _serieDespachoUf.CodigoControle;
                _cte03Repository.Inserir(cte03);

                // Obter informa��es do DCL
                if (ObterDadosDCL(out serieDespacho, out numeroDespacho, out dataDespacho))
                {
                    // Campo s�rie despacho
                    cte03 = new Cte03();
                    cte03.CfgUn = idFilial;
                    cte03.CfgDoc = documento;
                    cte03.CfgSerie = serie;

                    itemObs++;
                    cte03.IdObsCont = itemObs;
                    cte03.ObsContxCampo = "S�RIE:";
                    cte03.ObsContxTexto = serieDespacho;
                    _cte03Repository.Inserir(cte03);

                    // Campo n�mero despacho
                    cte03 = new Cte03();
                    cte03.CfgUn = idFilial;
                    cte03.CfgDoc = documento;
                    cte03.CfgSerie = serie;

                    itemObs++;
                    cte03.IdObsCont = itemObs;
                    cte03.ObsContxCampo = "N�MERO:";
                    cte03.ObsContxTexto = numeroDespacho;
                    _cte03Repository.Inserir(cte03);

                    // Campo data despacho
                    cte03 = new Cte03();
                    cte03.CfgUn = idFilial;
                    cte03.CfgDoc = documento;
                    cte03.CfgSerie = serie;

                    itemObs++;
                    cte03.IdObsCont = itemObs;
                    cte03.ObsContxCampo = "DATA:";
                    cte03.ObsContxTexto = dataDespacho;
                    _cte03Repository.Inserir(cte03);
                }

                //if (ValidarInformacoesVinculadoMultimodal())
                //{
                // Campo dados s�rie do vag�o
                cte03 = new Cte03();
                cte03.CfgUn = idFilial;
                cte03.CfgDoc = documento;
                cte03.CfgSerie = serie;

                itemObs++;
                cte03.IdObsCont = itemObs;
                cte03.ObsContxCampo = "S�RIE VAG�O:";
                cte03.ObsContxTexto = _cte.ObterSerieVagao();
                _cte03Repository.Inserir(cte03);

                // Campo dados c�digo do vag�o
                cte03 = new Cte03();
                cte03.CfgUn = idFilial;
                cte03.CfgDoc = documento;
                cte03.CfgSerie = serie;

                itemObs++;
                cte03.IdObsCont = itemObs;
                cte03.ObsContxCampo = "VAG�O:";
                cte03.ObsContxTexto = _cte.ObterCodigoVagao();
                _cte03Repository.Inserir(cte03);
                //}

            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(_cte, "GravarConfigCteEnvioService", string.Format("GravarDadosObservacaoContribuinte: {0}", ex.Message), ex);
                LogHelper.Log(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
                throw;
            }
        }

        /// <summary>
        /// Efetua o tratamento do retorno da validacao das regular expression
        /// </summary>
        /// <param name="ret">Lista de erros encontrados</param>
        private void TratarRetornoRegexSefaz(InvalidValue[] ret)
        {
            if (_errosValidacao == null)
            {
                _errosValidacao = new StringBuilder();
            }

            if (ret != null && ret.Count() > 0)
            {
                foreach (InvalidValue invalidValue in ret)
                {
                    _errosValidacao.AppendLine(invalidValue.Message);
                }
            }
        }

        /// <summary>
        /// Tabela de informacao do emitente do CT-e (1-1)
        /// </summary>
        private void GravarInformacoesEmitente()
        {
            try
            {
                IEmpresa empresaRemetente = _empresaContratoRemetenteFiscal;
                CidadeIbge cidadeIbgeRemetente = _cidadeIbgeRemetenteFiscal;

                /*
                if (_tipoNotaCte == TipoNotaCteEnum.NotaEntrada)
                {
                     Regra:
                     * Caso seja uma nota de entrada, deve trocar o remetente pelo destinat�rio
				
                    empresaRemetente = _empresaContratoDestinatariaFiscal;
                    cidadeIbgeRemetente = _empresaContratoDestinatariaFiscal.CidadeIbge;
                }
                */

                int aux;

                // Armazena qual a empresa remetente utilizada.
                _cteEmpresas.EmpresaRemetente = empresaRemetente;

                Cte05 cte05 = new Cte05();
                int index, numConvertido;
                long cnpj;

                // Codigo interno da filial (PK)
                cte05.CfgUn = ObterIdentificadorFilial();

                // Numero do documento CT-e (PK)
                int.TryParse(_cte.Numero, out numConvertido);
                cte05.CfgDoc = numConvertido;

                // Serie do documento CT-e (PK)
                // cte05.CfgSerie = _cte.Serie;
                cte05.CfgSerie = int.Parse(_cte.Serie).ToString();

                // CNPJ
                cte05.EmitCnpj = _nfe03Filial.Cnpj;

                // Inscricao estadual   
                cte05.EmitIe = Tools.TruncateRemoveSpecialChar(_nfe03Filial.Ie, 14);

                if (!string.IsNullOrEmpty(_nfe03Filial.RazSoc))
                {
                    // Nome
                    cte05.EmitXnome = _nfe03Filial.RazSoc.Trim();
                }

                // Nome Fantasia
                cte05.EmitXfant = _nfe03Filial.NomeReduzido;

                if (!string.IsNullOrEmpty(_nfe03Filial.Endereco))
                {
                    // Logradouro
                    cte05.EnderEmitXlgr = _nfe03Filial.Endereco.Trim();
                }

                // N�mero
                // cte05.EnderEmitNro = string.Empty;
                cte05.EnderEmitNro = _nfe03Filial.NrLogr.ToString();

                // Complemento do endere�o
                cte05.EnderEmitXcpl = _nfe03Filial.Compl;

                // Bairro do endere�o
                cte05.EnderEmitXbairro = _nfe03Filial.Bairro;

                CidadeIbge cidadeEmitente = _cidadeIbgeRepository.ObterPorDescricaoUf(_nfe03Filial.Cidade.ToUpper().Trim(), _cte.SiglaUfFerrovia);
                if (cidadeEmitente != null)
                {
                    // C�digo IBGE do munic�pio
                    cte05.EnderEmitCmun = cidadeEmitente.CodigoIbge;
                    if (cidadeEmitente.CodigoIbge == 9999999)
                    {
                        // Nome do munic�pio 
                        cte05.EnderEmitXmun = "EXTERIOR";
                        // C�digo UF 
                        cte05.EnderEmitUf = "EX";
                    }
                    else
                    {
                        // Nome do munic�pio 
                        cte05.EnderEmitXmun = cidadeEmitente.Descricao;
                        // C�digo UF 
                        cte05.EnderEmitUf = _cte.SiglaUfFerrovia;
                    }

                    // Recupera o PaisBacen
                    PaisBacen paisBacenEmit = _paisBacenRepository.ObterPorSiglaResumida(cidadeEmitente.SiglaPais);

                    // C�digo BACEN do pa�s 
                    cte05.EnderEmitCpais = int.Parse(paisBacenEmit != null ? paisBacenEmit.CodigoBacen.Substring(1, 4) : string.Empty);

                    // Nome do pa�s. 
                    cte05.EnderEmitXpais = paisBacenEmit != null ? paisBacenEmit.Nome : string.Empty;
                }

                // Cep 
                cte05.EnderEmitCep = _nfe03Filial.Cep;

                // Telefone
                if (_nfe03Filial.Telefone != null)
                {
                    var ddd = _nfe03Filial.Ddd.HasValue ? _nfe03Filial.Ddd.ToString() : string.Empty;
                    cte05.EnderEmitFone = string.Concat(ddd, _nfe03Filial.Telefone.ToString());
                }

                // Dados do remetente
                if (empresaRemetente.TipoPessoa == TipoPessoaEnum.Juridica)
                {
                    // CNPJ
                    if (Int64.TryParse(empresaRemetente.Cgc, out cnpj))
                    {
                        cte05.RemCnpj = cnpj;
                    }
                }
                else
                {
                    // CPF
                    if (Int64.TryParse(empresaRemetente.Cpf, out cnpj))
                    {
                        cte05.RemCpf = cnpj;
                    }
                }

                // Inscricao estadual
                cte05.RemIe = Tools.TruncateRemoveSpecialChar(empresaRemetente.InscricaoEstadual, 14);

                if (!string.IsNullOrEmpty(empresaRemetente.RazaoSocial))
                {
                    // Nome
                    cte05.RemXnome = empresaRemetente.RazaoSocial.Trim();
                }

                if (!string.IsNullOrEmpty(empresaRemetente.NomeFantasia))
                {
                    // Nome Fantasia
                    cte05.RemXfant = empresaRemetente.NomeFantasia.Trim();
                }

                // Telefone
                cte05.RemFone = ValidarTelefone(empresaRemetente.Telefone1);
                if (string.IsNullOrEmpty(cte05.RemFone))
                {
                    cte05.RemFone = null;
                }

                if (!string.IsNullOrEmpty(empresaRemetente.Endereco))
                {
                    // Logradouro
                    cte05.EnderRemeXlgr = empresaRemetente.Endereco.Trim();
                }

                // N�mero
                // cte05.EnderRemeNro = string.Empty;
                cte05.EnderRemeNro = "SN";

                // Complemento do endere�o
                cte05.EnderRemeXcpl = string.Empty;

                // Bairro do endere�o
                cte05.EnderRemeXbairro = "NAO INFORMADO";

                if (cidadeIbgeRemetente != null)
                {
                    // C�digo IBGE do munic�pio
                    cte05.EnderRemeCmun = cidadeIbgeRemetente.CodigoIbge;
                    if (cidadeIbgeRemetente.CodigoIbge == 9999999)
                    {
                        // Nome do munic�pio 
                        cte05.EnderRemeXmun = "EXTERIOR";

                        // C�digo UF 
                        cte05.EnderRemeUf = "EX";
                    }
                    else
                    {
                        // Nome do munic�pio 
                        cte05.EnderRemeXmun = cidadeIbgeRemetente.Descricao;

                        // C�digo UF 
                        cte05.EnderRemeUf = cidadeIbgeRemetente.SiglaEstado;
                    }

                    // Recupera o PaisBacen
                    PaisBacen paisBacenReme = null;
                    if (cidadeIbgeRemetente.CodigoIbge == 9999999)
                    {
                        string siglaPaisAux = ObterSiglaPaisExteriorPeloSap(empresaRemetente);
                        paisBacenReme = _paisBacenRepository.ObterPorSiglaResumida(siglaPaisAux);
                        cte05.RemCnpj = 0;
                    }
                    else
                    {
                        paisBacenReme = _paisBacenRepository.ObterPorSiglaResumida(cidadeIbgeRemetente.SiglaPais);
                    }

                    // C�digo BACEN do pa�s 
                    cte05.EnderRemeCpais = int.Parse(paisBacenReme != null ? paisBacenReme.CodigoBacen.Substring(1, 4) : string.Empty);

                    // Nome do pa�s. 
                    cte05.EnderRemeXpais = paisBacenReme != null ? paisBacenReme.Nome : string.Empty;
                }

                try
                {
                    // Cep 
                    index = empresaRemetente.Cep.IndexOf('-');
                    cte05.EnderRemeCep = index > 0 ? int.Parse(empresaRemetente.Cep.Remove(index, 1)) : int.Parse(empresaRemetente.Cep);
                    if (cte05.EnderRemeCep > 99999999)
                    {
                        // Garante que o tamanho maximo do cep em 8 casas 
                        cte05.EnderRemeCep = null;
                    }
                }
                catch (Exception ex)
                {
                    cte05.EnderRemeCep = null;
                }

                // CNPJ do local de retirada
                if (Int64.TryParse(_empresaPagadoraContrato.Cgc, out cnpj))
                {
                    cte05.LocColetaCnpj = cnpj;
                }

                // Cpf local de retirada
                // cte06.LocRetCpf = null;

                if (!string.IsNullOrEmpty(_empresaPagadoraContrato.RazaoSocial))
                {
                    // Nome local de retirada
                    cte05.LocColetaxNome = _empresaPagadoraContrato.RazaoSocial.Trim();
                }

                if (!string.IsNullOrEmpty(_empresaPagadoraContrato.Endereco))
                {
                    // Logradouro do local de retirada
                    cte05.LocColetaxLgr = _empresaPagadoraContrato.Endereco.Trim();
                }

                // N�mero
                // cte06.LocRetNro = string.Empty;
                cte05.LocColetaNro = "0";

                // Complemento do complemento
                cte05.LocColetaxCpl = string.Empty;

                // Bairro do local de retirada 
                cte05.LocColetaxBairro = "NAO INFORMADO";

                // C�digo IBGE do munic�pio do local de retirada
                cte05.LocColetacMun = _cidadeIbgeEmpresaPagadoraContrato.CodigoIbge;
                if (_cidadeIbgeEmpresaPagadoraContrato.CodigoIbge == 9999999)
                {
                    // Nome do munic�pio do local de retirada
                    cte05.LocColetaxMun = "EXTERIOR";
                    // C�digo UF do local de retirada
                    cte05.LocColetaUf = "EX";
                }
                else
                {
                    // Nome do munic�pio do local de retirada
                    cte05.LocColetaxMun = _cidadeIbgeEmpresaPagadoraContrato.Descricao;
                    // C�digo UF do local de retirada
                    cte05.LocColetaUf = _cidadeIbgeEmpresaPagadoraContrato.SiglaEstado;
                }

                // Inserir
                _cte05Repository.Inserir(cte05);
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(_cte, "GravarConfigCteEnvioService", string.Format("GravarInformacoesEmitente: {0}", ex.Message), ex);
                LogHelper.Log(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
                throw;
            }
        }

        /// <summary>
        /// Valida a grava��o das informa��es dos documentos (NF, Nfe e Outros) (1-N) - ICTE06_EMIT_INF
        ///  * Somente gera as informa��es nas vers�es 2.00 e 3.00
        ///  * Redespacho intermedi�rio e vinculado multi modal n�o geram informa��o de documentos..
        ///  * Cte de anula��o n�o gera informa��o de documentos..
        /// </summary>
        private bool ValidarInformacoesDocumentos()
        {
            try
            {
                if (_cte.Versao.CodigoVersao.Equals("2.00") || _cte.Versao.CodigoVersao.Equals("3.00"))
                {
                    if (
                        (_cte.TipoServico != (int)TipoServicoCteEnum.RedespachoIntermediario) &&
                        (_cte.TipoServico != (int)TipoServicoCteEnum.VinculadoMultimodal) &&
                        (_cte.TipoCte != TipoCteEnum.Anulacao)
                    )
                    {
                        return true;
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                LogHelper.Log(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
                throw;
            }
        }

        /// <summary>
        /// Tabela de informacoes dos documentos (NF, Nfe e Outros) (1-N) - ICTE06_EMIT_INF
        /// </summary>
        private void GravarInformacoesDocumentos()
        {
            try
            {
                int seq = 0;

                foreach (CteDetalhe cteDetalhe in _listaCteDetalhe)
                {
                    // Chave de acesso da NFE
                    if (!string.IsNullOrEmpty(cteDetalhe.ChaveNfe))
                    {
                        foreach (string strChave in _listaCteDetalhe.Select(x => x.ChaveNfe).Distinct())
                        {
                            seq++;

                            CteDetalhe cteDetalheNfe = _listaCteDetalhe.Where(det => det.ChaveNfe == strChave).Take(1).SingleOrDefault();
                            GravarDocumentoNfe(cteDetalheNfe, seq);
                        }

                        break;
                    }
                    else
                    {
                        if (cteDetalhe.SerieNota == null)
                        {
                            cteDetalhe.SerieNota = string.Empty;
                        }

                        int serie;

                        // if ((cteDetalhe.SerieNota.ToUpper() != "V2") && (cteDetalhe.SerieNota.ToUpper() != "OU"))
                        if (int.TryParse(cteDetalhe.SerieNota.ToUpper(), out serie) && !_fluxoComercial.ModeloNotaFiscal.ToUpper().Equals("99"))
                        {
                            GravarDocumentoNf(cteDetalhe, seq);
                        }
                        else
                        {
                            // Outros
                            GravarDocumentosOutros(cteDetalhe, seq);
                        }
                        seq++;
                    }
                }
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(_cte, "GravarConfigCteEnvioService", string.Format("GravarInformacoesDocumentos: {0}", ex.Message), ex);
                LogHelper.Log(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
                throw;
            }
        }

        /// <summary>
        /// Tabela de informacoes do Expedidor / Recebedor / Destinatario (1-1) - ICTE07_EXPED
        /// </summary>
        private void GravarInformacoesExpedidorRecebedorDestinatario()
        {
            IEmpresa empresaExpedidor = _empresaRemetenteFluxoComercial;
            CidadeIbge cidadeIbgeExpedidor = _empresaRemetenteFluxoComercial.CidadeIbge;
            IEmpresa empresaRecebedor = _empresaDestinatariaFluxoComercial;
            CidadeIbge cidadeIbgeRecebedor = _empresaDestinatariaFluxoComercial.CidadeIbge;
            IEmpresa empresaDestinataria = _empresaContratoDestinatariaFiscal;
            CidadeIbge cidadeIbgeDestinataria = _empresaContratoDestinatariaFiscal.CidadeIbge;

            /*
			 
            if (_tipoNotaCte == TipoNotaCteEnum.NotaEntrada)
            {
				
                Regra:
                 * Caso seja uma nota de entrada, deve trocar o remetente pelo destinat�rio
				
                empresaDestinataria = _empresaContratoRemetenteFiscal;
                cidadeIbgeDestinataria = _empresaContratoRemetenteFiscal.CidadeIbge;
            }
			
             */

            int aux;

            try
            {
                // Armazena as empresas utilizadas
                _cteEmpresas.EmpresaExpedidor = empresaExpedidor;
                _cteEmpresas.EmpresaRecebedor = empresaRecebedor;
                _cteEmpresas.EmpresaDestinatario = empresaDestinataria;

                Cte07 cte07 = new Cte07();
                int numConvertido;
                int index;
                long cnpj = 0;

                // Codigo interno da filial (PK)
                cte07.CfgUn = ObterIdentificadorFilial();

                // Numero do documento CT-e (PK)
                int.TryParse(_cte.Numero, out numConvertido);
                cte07.CfgDoc = numConvertido;

                // Serie do documento CT-e (PK)
                // cte07.CfgSerie = _cte.Serie;
                cte07.CfgSerie = int.Parse(_cte.Serie).ToString();

                // DADOS DO EXPEDIDOR

                if (empresaExpedidor.TipoPessoa == TipoPessoaEnum.Juridica)
                {
                    // CNPJ do expedidor
                    if (Int64.TryParse(empresaExpedidor.Cgc, out cnpj))
                    {
                        cte07.ExpedCnpj = cnpj;
                    }
                }
                else
                {
                    // CNPJ do expedidor
                    if (Int64.TryParse(empresaExpedidor.Cpf, out cnpj))
                    {
                        cte07.ExpedCpf = cnpj;
                    }
                }

                // Inscricao Estadual do expedidor
                cte07.ExpedIe = Tools.TruncateRemoveSpecialChar(empresaExpedidor.InscricaoEstadual, 14);

                if (!string.IsNullOrEmpty(empresaExpedidor.RazaoSocial))
                {
                    // Nome do expedidor
                    cte07.ExpedXNome = empresaExpedidor.RazaoSocial.Trim();
                }

                // Telefone do expedidor
                cte07.ExpedFone = ValidarTelefone(empresaExpedidor.Telefone1);
                if (string.IsNullOrEmpty(cte07.ExpedFone))
                {
                    cte07.ExpedFone = null;
                }

                if (!string.IsNullOrEmpty(empresaExpedidor.Endereco))
                {
                    // Logradouro do tomador do servi�o
                    cte07.EnderExpedXlgr = empresaExpedidor.Endereco.Trim();
                }

                // N�mero
                // cte07.EnderExpedNro = string.Empty;
                cte07.EnderExpedNro = "0";

                // Complemento do endere�o
                cte07.EnderExpedXcpl = string.Empty;

                // Bairro do endere�o do tomador
                cte07.EnderExpedXBairro = "NAO INFORMADO";

                // C�digo IBGE do munic�pio do tomador
                cte07.EnderExpedCMun = cidadeIbgeExpedidor.CodigoIbge;
                if (cidadeIbgeExpedidor.CodigoIbge == 9999999)
                {
                    // Nome do munic�pio do w
                    cte07.EnderExpedXMun = "EXTERIOR";
                    // C�digo UF do expedidor
                    cte07.EnderExpedUf = "EX";
                }
                else
                {
                    // Nome do munic�pio do w
                    cte07.EnderExpedXMun = cidadeIbgeExpedidor.Descricao;
                    // C�digo UF do expedidor
                    cte07.EnderExpedUf = cidadeIbgeExpedidor.SiglaEstado;
                }

                try
                {
                    // Cep do expedidor
                    index = _empresaRemetenteContrato.Cep.IndexOf('-');
                    cte07.EnderExpedCep = index > 0 ? int.Parse(empresaExpedidor.Cep.Remove(index, 1)) : int.Parse(empresaExpedidor.Cep);
                    if (cte07.EnderExpedCep > 99999999)
                    {
                        // Garante o tamanho maximo do cep em 8 casas
                        cte07.EnderExpedCep = null;
                    }
                }
                catch (Exception ex)
                {
                    cte07.EnderExpedCep = null;
                }

                // Recupera o PaisBacen
                PaisBacen paisBacen = null;
                if (cidadeIbgeExpedidor.CodigoIbge == 9999999)
                {
                    string siglaPaisAux = ObterSiglaPaisExteriorPeloSap(empresaExpedidor);
                    paisBacen = _paisBacenRepository.ObterPorSiglaResumida(siglaPaisAux);
                    cte07.ExpedCnpj = 0;
                }
                else
                {
                    paisBacen = _paisBacenRepository.ObterPorSiglaResumida(cidadeIbgeExpedidor.SiglaPais);
                }

                // C�digo BACEN do pa�s do expedidor
                cte07.EnderExpedCpais = int.Parse(paisBacen.CodigoBacen.Substring(1, 4));

                // Nome do pa�s. 
                cte07.EnderExpedXPais = paisBacen.Nome;

                // DADOS DO RECEBEDOR

                if (empresaRecebedor.TipoPessoa == TipoPessoaEnum.Juridica)
                {
                    // CNPJ do recebedor de servi�o
                    if (Int64.TryParse(empresaRecebedor.Cgc, out cnpj))
                    {
                        cte07.RecebCnpj = cnpj;
                    }
                }
                else
                {
                    // CNPJ do recebedor de servi�o
                    if (Int64.TryParse(empresaRecebedor.Cpf, out cnpj))
                    {
                        cte07.RecebCpf = cnpj;
                    }
                }

                // Inscri��o estadual 
                cte07.RecebIe = Tools.TruncateRemoveSpecialChar(empresaRecebedor.InscricaoEstadual, 14);

                if (!string.IsNullOrEmpty(empresaRecebedor.RazaoSocial))
                {
                    // Raz�o social da empresa
                    cte07.RecebXNome = empresaRecebedor.RazaoSocial.Trim();
                }

                // Telefone da empresa
                cte07.RecebFone = ValidarTelefone(empresaRecebedor.Telefone1);
                if (string.IsNullOrEmpty(cte07.RecebFone))
                {
                    cte07.RecebFone = null;
                }

                // Logradouro do recebedor do servi�o
                if (!String.IsNullOrEmpty(empresaRecebedor.Endereco))
                {
                    cte07.EnderRecebXlgr = empresaRecebedor.Endereco.Trim();
                }

                // N�mero
                // cte07.EnderRecebNro = string.Empty;
                cte07.EnderRecebNro = "0";

                // Complemento do endere�o
                cte07.EnderRecebXcpl = string.Empty;

                // Bairro do endere�o do recebedor
                cte07.EnderRecebXbairro = "NAO INFORMADO";

                // C�digo IBGE do munic�pio do tomador
                cte07.EnderRecebCMun = cidadeIbgeRecebedor.CodigoIbge;
                if (cidadeIbgeRecebedor.CodigoIbge == 9999999)
                {
                    // Nome do munic�pio do tomador
                    cte07.EnderRecebXMun = "EXTERIOR";
                    // C�digo UF do recebedor
                    cte07.EnderRecebUf = "EX";
                }
                else
                {
                    // Nome do munic�pio do tomador
                    cte07.EnderRecebXMun = cidadeIbgeRecebedor.Descricao;
                    // C�digo UF do recebedor
                    cte07.EnderRecebUf = cidadeIbgeRecebedor.SiglaEstado;
                }

                try
                {
                    // Cep do expedidor
                    index = empresaRecebedor.Cep.IndexOf('-');
                    cte07.EnderRecebCep = index > 0 ? int.Parse(empresaRecebedor.Cep.Remove(index, 1)) : int.Parse(empresaRecebedor.Cep);
                    if (cte07.EnderRecebCep > 99999999)
                    {
                        // Garante o tamanho maximo do cep em 8 casas
                        cte07.EnderRecebCep = null;
                    }
                }
                catch (Exception ex)
                {
                    cte07.EnderRecebCep = null;
                }

                // Recupera o PaisBacen
                PaisBacen paisBacenReceb = null;
                if (cidadeIbgeRecebedor.CodigoIbge == 9999999)
                {
                    string siglaPaisAux = ObterSiglaPaisExteriorPeloSap(empresaRecebedor);
                    paisBacenReceb = _paisBacenRepository.ObterPorSiglaResumida(siglaPaisAux);
                    cte07.RecebCnpj = 0;
                }
                else
                {
                    paisBacenReceb = _paisBacenRepository.ObterPorSiglaResumida(cidadeIbgeRecebedor.SiglaPais);
                }

                // C�digo BACEN do pa�s do recebedor
                cte07.EnderRecebCpais = int.Parse(paisBacenReceb.CodigoBacen.Substring(1, 4));

                // Nome do pa�s. 
                cte07.EnderRecebXpais = paisBacenReceb.Nome;

                // DADOS DO DESTINATARIO

                if (empresaDestinataria.TipoPessoa == TipoPessoaEnum.Juridica)
                {
                    // CNPJ do recebedor de servi�o
                    if (Int64.TryParse(empresaDestinataria.Cgc, out cnpj))
                    {
                        cte07.DestCnpj = cnpj;
                    }
                }
                else
                {
                    if (Int64.TryParse(empresaDestinataria.Cpf, out cnpj))
                    {
                        cte07.DestCpf = cnpj;
                    }
                }

                // Inscri��o estadual 
                cte07.DestIe = Tools.TruncateRemoveSpecialChar(empresaDestinataria.InscricaoEstadual, 14);

                if (!string.IsNullOrEmpty(empresaDestinataria.RazaoSocial))
                {
                    // Raz�o social da empresa
                    cte07.DestXNome = empresaDestinataria.RazaoSocial.Trim();
                }

                // Telefone da empresa
                cte07.DestFone = ValidarTelefone(empresaDestinataria.Telefone1);
                if (string.IsNullOrEmpty(cte07.DestFone))
                {
                    cte07.DestFone = null;
                }

                // Inscri��o na SUFRAMA
                cte07.DestIsUf = string.Empty;

                // Logradouro do destinatario do servi�o
                if (!String.IsNullOrEmpty(empresaDestinataria.Endereco))
                {
                    cte07.EnderDestXlgr = empresaDestinataria.Endereco.Trim();
                }

                // N�mero
                // cte07.EnderDestNro = string.Empty;
                cte07.EnderDestNro = "0";

                // Complemento do endere�o
                cte07.EnderDestXCpl = string.Empty;

                // Bairro do endere�o do destinatario
                cte07.EnderDestXBairro = "NAO INFORMADO";

                // C�digo IBGE do munic�pio do destinatario
                cte07.EnderDestCMun = cidadeIbgeDestinataria.CodigoIbge;
                if (cidadeIbgeDestinataria.CodigoIbge == 9999999)
                {
                    // Nome do munic�pio do destinatario
                    cte07.EnderDestXMun = "EXTERIOR";
                    // C�digo UF do destinatario
                    cte07.EnderDestUf = "EX";
                }
                else
                {
                    // Nome do munic�pio do destinatario
                    cte07.EnderDestXMun = cidadeIbgeDestinataria.Descricao;
                    // C�digo UF do destinatario
                    cte07.EnderDestUf = cidadeIbgeDestinataria.SiglaEstado;
                }

                // Cep do destinatario
                try
                {
                    index = empresaDestinataria.Cep.IndexOf('-');
                    cte07.EnderDestCep = index > 0 ? int.Parse(empresaDestinataria.Cep.Remove(index, 1)) : int.Parse(empresaDestinataria.Cep);
                    if (cte07.EnderDestCep > 99999999)
                    {
                        // Garante o tamanho maximo do cep em 8 casas
                        cte07.EnderDestCep = null;
                    }
                }
                catch (Exception ex)
                {
                    cte07.EnderDestCep = null;
                }

                // Recupera o PaisBacen
                PaisBacen paisBacenDest = null;
                if (cidadeIbgeDestinataria.CodigoIbge == 9999999)
                {
                    string siglaPaisAux = ObterSiglaPaisExteriorPeloSap(empresaDestinataria);
                    paisBacenDest = _paisBacenRepository.ObterPorSiglaResumida(siglaPaisAux);
                    cte07.DestCnpj = 0;
                }
                else
                {
                    paisBacenDest = _paisBacenRepository.ObterPorSiglaResumida(cidadeIbgeDestinataria.SiglaPais);
                }

                // C�digo BACEN do pa�s do destinatario
                cte07.EnderDestCpais = int.Parse(paisBacenDest.CodigoBacen.Substring(1, 4));

                // Nome do pa�s. 
                cte07.EnderDestXpais = paisBacenDest.Nome;

                // DADOS DO LOCAL DE ENTREGA
                if (empresaDestinataria.TipoPessoa == TipoPessoaEnum.Juridica)
                {
                    // CNPJ do local de entrega
                    if (Int64.TryParse(empresaDestinataria.Cgc, out cnpj))
                    {
                        cte07.LocEntCnpj = cnpj;
                    }
                }
                else
                {
                    if (Int64.TryParse(empresaDestinataria.Cpf, out cnpj))
                    {
                        cte07.LocEntCpf = cnpj;
                    }
                }

                if (!string.IsNullOrEmpty(empresaDestinataria.RazaoSocial))
                {
                    // Nome local de entrega
                    cte07.LocEntXNome = empresaDestinataria.RazaoSocial.Trim();
                }

                if (!string.IsNullOrEmpty(empresaDestinataria.Endereco))
                {
                    // Logradouro do local de entrega
                    cte07.LocEntXlgr = empresaDestinataria.Endereco.Trim();
                }

                // N�mero
                // cte07.LocEntNro = string.Empty;
                cte07.LocEntNro = "0";

                // Complemento do endere�o
                cte07.LocEntXcpl = string.Empty;

                // Bairro do local de enterga 
                cte07.EnderDestXBairro = "NAO INFORMADO";

                // C�digo IBGE do munic�pio do local de entrega
                cte07.LocEntCmun = cidadeIbgeDestinataria.CodigoIbge;
                if (cidadeIbgeDestinataria.CodigoIbge == 9999999)
                {
                    // Nome do munic�pio do local de entrega
                    cte07.LocEntXmun = "EXTERIOR";
                    // C�digo UF do local de entrega
                    cte07.LocEntUf = "EX";
                }
                else
                {
                    // Nome do munic�pio do local de entrega
                    cte07.LocEntXmun = cidadeIbgeDestinataria.Descricao;
                    // C�digo UF do local de entrega
                    cte07.LocEntUf = cidadeIbgeDestinataria.SiglaEstado;
                }

                cte07.LocEntXbairro = "NAO INFORMADO";

                // Inserir
                _cte07Repository.Inserir(cte07);

                // Grava email de envio do xml
                // GravarInformacoesEnvioXml(empresaDestinataria);
                GravarInformacoesEnvioXml(_empresaTomadora);
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(_cte, "GravarConfigCteEnvioService", string.Format("GravarInformacoesExpedidorRecebedorDestinatario: {0}", ex.Message), ex);
                LogHelper.Log(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
                throw;
            }
        }

        /// <summary>
        /// Tabela de valores da prestacao do servico e impostos (1-1) - ICTE08_VALORES
        /// </summary>
        private void GravarValorPrestacaoServicoImposto()
        {
            try
            {
                Cte08 cte08 = new Cte08();

                int numConvertido;

                double valorReceber = _cte.ValorReceber ?? 0.0;
                double baseCalculoIcms = _cte.BaseCalculoIcms ?? 0.0;
                double aliquota = _cte.PercentualAliquotaIcms ?? 0.0;
                double valorTotalIcms = _cte.ValorIcms ?? 0.0;

                if (_cte.TipoCte == TipoCteEnum.Anulacao)
                {
                    var cteAutorizado = _cteRepository.ObterCteAutorizadoByDespacho((int)_cte.Id);
                    if (cteAutorizado != null && cteAutorizado.ValorIcms != _cte.ValorIcms)
                    {
                        valorTotalIcms = cteAutorizado.ValorIcms != null ? (double)cteAutorizado.ValorIcms : 0.0;
                    }
                }
                // Codigo interno da filial (PK)
                cte08.CfgUn = ObterIdentificadorFilial();

                // Numero do documento CT-e (PK)
                int.TryParse(_cte.Numero, out numConvertido);
                cte08.CfgDoc = numConvertido;

                // Serie do documento CT-e (PK)
                // cte08.CfgSerie = _cte.Serie;
                cte08.CfgSerie = int.Parse(_cte.Serie).ToString();

                cte08.VprestVtprest = baseCalculoIcms;
                // cte08.VprestVrec = valorReceber;
                cte08.VprestVrec = baseCalculoIcms;

                // Classifica��o Tribut�ria do servi�o
                cte08.Cst20Cst = null;

                // Percentual de redu��o da BC
                cte08.Cst20Predbc = null;

                // Valor da BC do ICMS
                cte08.Cst20Vbc = null;

                // Al�quota do ICMS
                cte08.Cst20Picms = null;

                // Valor do ICMS
                cte08.Cst20Vicms = null;

                // Classifica��o Tribut�ria do servi�o
                cte08.Icms20Cst = null;

                // Valor da BC do ICMS
                cte08.Icms20Vbc = null;

                // Al�quota do ICMS
                cte08.Icms20Picms = null;

                // Valor do ICMS
                cte08.Icms20Vicms = null;

                // Percentual de redu��o da BC
                cte08.Icms20Predbc = null;

                // Classifica��o Tribut�ria do Servi�o
                cte08.IcmsOutraufCst = null;

                // Percentual de redu��o da BC
                cte08.IcmsOutraUfPredbcOutraUf = null;

                // Valor da BC do ICMS
                cte08.IcmsOutraUfVbcOutraUf = null;

                // Al�quota do ICMS
                cte08.IcmsOutraUfPicmsOutraUf = null;

                // Valor do ICMS devido outra UF
                cte08.IcmsOutraUfVicmsOutraUf = null;

                // Indica se o contribuinte � Simples Nacional 1=Sim
                cte08.IcmsSnIndsn = null;

                // Verifica o tipo do CST
                if (_contratoHistorico.Cst == "00")
                {
                    // C�digo de Situa��o Tribut�ria
                    cte08.Cst00Cst = 00;

                    // Valor da BC do ICMS
                    cte08.Cst00Vbc = baseCalculoIcms;

                    // Al�quota do ICMS
                    cte08.Cst00Picms = aliquota;

                    // Valor do ICMS
                    cte08.Cst00Vicms = valorTotalIcms;

                    // classifica��o Tribut�ria do Servi�o
                    cte08.Icms00Cst = 00;

                    // Valor da BC do ICMS
                    cte08.Icms00Picms = aliquota;

                    // Al�quota do ICMS
                    cte08.Icms00Vbc = baseCalculoIcms;

                    // Valor do ICMS
                    cte08.Icms00Vicms = valorTotalIcms;
                }
                else if ((_contratoHistorico.Cst == "40") || (_contratoHistorico.Cst == "41") || (_contratoHistorico.Cst == "51"))
                {
                    // C�digo de Situa��o Tribut�ria
                    cte08.Cst45Cst = int.Parse(_contratoHistorico.Cst);

                    // classifica��o Tribut�ria do Servi�o
                    cte08.Icms45Cst = int.Parse(_contratoHistorico.Cst);
                }
                else if (_contratoHistorico.Cst == "60")
                {
                    double aliquotaSt = _contratoHistorico.AliquotaIcmsSubstProduto ?? 0.0;
                    double valorImpostoRetido = Math.Round(baseCalculoIcms * (aliquotaSt / 100), 2);

                    // C�digo de Situa��o Tribut�ria
                    cte08.Icms60Cst = 60;

                    // Al�quota do ICMS
                    cte08.Icms60Picmsstret = aliquotaSt;

                    // Valor da BC do ICMS ST retido
                    cte08.Icms60Vbcstret = baseCalculoIcms;

                    // Valor do ICMS ST retido
                    // cte08.Icms60Vicmsstret = valorTotalIcms;
                    cte08.Icms60Vicmsstret = valorImpostoRetido;

                    // Valor do Cr�dito outorgado/Presumido
                    cte08.Icms60Vcred = null;
                }
                else if (_contratoHistorico.Cst == "80")
                {
                    // C�digo de Situa��o Tribut�ria
                    cte08.Cst80Cst = 80;

                    // Al�quota do ICMS
                    cte08.Cst80Picms = aliquota;

                    // Valor da BC do ICMS
                    cte08.Cst80Vbc = baseCalculoIcms;

                    // Valor do ICMS    
                    cte08.Cst80Vicms = valorTotalIcms;

                    // Valor do Cr�dito outorgado/presumido
                    cte08.Cst80Vcred = null;
                }
                else if (_contratoHistorico.Cst == "81")
                {
                    // C�digo de Situa��o Tribut�ria
                    cte08.Cst81Cst = 81;

                    // Al�quota do ICMS
                    cte08.Cst81Picms = aliquota;

                    // Valor da BC do ICMS
                    cte08.Cst81Vbc = baseCalculoIcms;

                    // Valor do ICMS
                    cte08.Cst81Vicms = valorTotalIcms;

                    // Percentual de redu��o da BC do ICMS
                    cte08.Cst81Predbc = null;
                }
                else if (_contratoHistorico.Cst == "90")
                {
                    // C�digo de Situa��o Tribut�ria
                    cte08.Cst90Cst = 90;

                    // Al�quota do ICMS
                    cte08.Cst90Picms = aliquota;

                    // Valor da BC do ICMS
                    cte08.Cst90Vbc = baseCalculoIcms;

                    // Valor do ICMS
                    cte08.Cst90Vicms = valorTotalIcms;

                    // Percentual de redu��o da BC do ICMS
                    cte08.Cst90Predbc = null;

                    // Valor do Cr�dito outorgado/presumido
                    cte08.Cst90Vcred = null;

                    // C�digo de Situa��o Tribut�ria
                    cte08.Icms90Cst = 90;

                    // Al�quota do ICMS
                    cte08.Icms90Picms = aliquota;

                    // Valor da BC do ICMS
                    cte08.Icms90Vbc = baseCalculoIcms;

                    // Valor do ICMS
                    cte08.Icms90Vicms = valorTotalIcms;

                    // Percentual de redu��o da BC do ICMS
                    cte08.Icms90Predbc = null;

                    // Valor do Cr�dito outorgado/presumido
                    cte08.Icms90Vcred = null;
                }

                cte08.ImpInfadfisco = _contratoHistorico.DescricaoTributacao;
                _cte08Repository.InserirOuAtualizar(cte08);
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(_cte, "GravarConfigCteEnvioService", string.Format("GravarValorPrestacaoServicoImposto: {0}", ex.Message), ex);
                LogHelper.Log(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
                throw;
            }
        }

        /// <summary>
        /// Tabela de componentes do valor da prestacao (0-N) - ICTE09_COMP
        /// </summary>
        private void GravarComponentesValorPrestacao()
        {
            try
            {
                if (_cte.Id == _cteRaizAgrupamento.Id)
                {
                    GravarComponentesValorPrestacaoRaiz();
                }
                else
                {
                    GravarComponentesValorPrestacaoFilho();
                }
            }
            catch (Exception ex)
            {
                LogHelper.Log(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
                throw;
            }
        }

        private void GravarComponentesValorPrestacaoRaiz()
        {
            double valorTarifa = 0.0;
            double valorDesconto = 0.0;
            double descontoPorPercentual = _cte.DescontoPorPercentual ?? 0.0;
            double descontoPorTonelada = _cte.ValorDescontoTonelada ?? 0.0;
            double percentualSeguro = _contratoHistorico.PercentualSeguro ?? 0.0;

            try
            {
                IList<ComposicaoFreteContrato> listaComposicaoFreteContrato = _composicaoFreteContratoRepository.ObterPorNumeroContrato(_contratoHistorico.NumeroContrato);

                if (PartilhaFerroeste())
                {
                    IList<ComposicaoFreteContrato> lista = listaComposicaoFreteContrato.Where(g => g.CondicaoTarifa != "ZDSL" && g.CondicaoTarifa != "ZDSS").ToList();
                    valorTarifa = lista.Sum(g => g.ValorComIcms).Value;
                }
                else
                {
                    valorTarifa = listaComposicaoFreteContrato.Sum(g => g.ValorComIcms).Value;
                }

                Cte09 cte09 = new Cte09();

                cte09.CfgUn = ObterIdentificadorFilial(); // Codigo interno da filial
                cte09.CfgDoc = Convert.ToInt32(_cte.Numero); // Numero do documento CT-e
                cte09.CfgSerie = int.Parse(_cte.Serie).ToString(); // Serie do documento CT-e
                cte09.IdComp = 1; // Numero sequecial da tabela
                cte09.CompXnome = "Tarifa";
                cte09.CompVcomp = valorTarifa;

                _cte09Repository.Inserir(cte09);

                /*
                valorDesconto = descontoPorPercentual + descontoPorTonelada;
                cte09 = new Cte09();
                cte09.CfgUn = ObterIdentificadorFilial(); // Codigo interno da filial
                cte09.CfgDoc = Convert.ToInt32(_cte.Numero); // Numero do documento CT-e
                cte09.CfgSerie = int.Parse(_cte.Serie).ToString(); // Serie do documento CT-e
                cte09.IdComp = 2; // Numero sequecial da tabela
                cte09.CompXnome = "Desconto";
                cte09.CompVcomp = Math.Abs(valorDesconto);
                _cte09Repository.Inserir(cte09);
                */
                double valorMercadoria;
                if (_fluxoComercial.Mercadoria.CodigoMercadoriaSAP == "011")
                {
                    valorMercadoria = this._configuracaoTranslogicRepository.ObterPorId(_chaveContainerVazioTipoFrigorifico) == null ? 0 : Convert.ToDouble(this._configuracaoTranslogicRepository.ObterPorId(_chaveContainerVazioTipoFrigorifico).Valor);
                }
                else
                {
                    valorMercadoria = _listaCteDetalhe.Sum(g => g.ValorTotalNotaFiscal);
                }
                cte09 = new Cte09();

                cte09.CfgUn = ObterIdentificadorFilial(); // Codigo interno da filial
                cte09.CfgDoc = Convert.ToInt32(_cte.Numero); // Numero do documento CT-e
                cte09.CfgSerie = int.Parse(_cte.Serie).ToString(); // Serie do documento CT-e
                cte09.IdComp = 3; // Numero sequecial da tabela
                cte09.CompXnome = "AdValorem";
                cte09.CompVcomp = valorMercadoria * percentualSeguro;

                _cte09Repository.Inserir(cte09);
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(_cte, "GravarConfigCteEnvioService", string.Format("GravarComponentesValorPrestacaoRaiz: {0}", ex.Message), ex);
                throw;
            }
        }

        private void GravarComponentesValorPrestacaoFilho()
        {
            try
            {
                double valorTarifa = 0.0;
                double valorDesconto = 0.0;
                double descontoPorPercentual = _cte.DescontoPorPercentual ?? 0.0;
                double descontoPorTonelada = _cte.ValorDescontoTonelada ?? 0.0;
                double percentualSeguro = _contratoHistorico.PercentualSeguro ?? 0.0;

                string letraFerrovia = _serieDespachoUf.LetraFerrovia;
                IList<ComposicaoFreteContrato> listaComposicaoFreteContrato = _composicaoFreteContratoRepository.ObterPorNumeroContrato(_contratoHistorico.NumeroContrato, _cteRaiz.DataHora, letraFerrovia, true);
                IList<ComposicaoFreteContrato> listaComposicaoFreteContratoVg = _composicaoFreteContratoRepository.ObterPorNumeroContrato(_contratoHistorico.NumeroContrato, _cteRaiz.DataHora, "VG", true);

                valorTarifa = listaComposicaoFreteContrato.Sum(g => g.ValorSemIcms).Value;
                valorTarifa += listaComposicaoFreteContratoVg.Sum(g => g.ValorSemIcms).Value;

                Cte09 cte09 = new Cte09();

                cte09.CfgUn = ObterIdentificadorFilial(); // Codigo interno da filial
                cte09.CfgDoc = Convert.ToInt32(_cte.Numero); // Numero do documento CT-e
                cte09.CfgSerie = int.Parse(_cte.Serie).ToString(); // Serie do documento CT-e
                cte09.IdComp = 1; // Numero sequecial da tabela
                cte09.CompXnome = "Tarifa";
                cte09.CompVcomp = valorTarifa;

                _cte09Repository.Inserir(cte09);

                valorDesconto = descontoPorPercentual + descontoPorTonelada;
                cte09 = new Cte09();
                cte09.CfgUn = ObterIdentificadorFilial(); // Codigo interno da filial
                cte09.CfgDoc = Convert.ToInt32(_cte.Numero); // Numero do documento CT-e
                cte09.CfgSerie = int.Parse(_cte.Serie).ToString(); // Serie do documento CT-e
                cte09.IdComp = 2; // Numero sequecial da tabela
                cte09.CompXnome = "Desconto";
                cte09.CompVcomp = Math.Abs(valorDesconto);

                _cte09Repository.Inserir(cte09);

                double valorMercadoria;
                if (_fluxoComercial.Mercadoria.CodigoMercadoriaSAP == "011")
                {
                    valorMercadoria = this._configuracaoTranslogicRepository.ObterPorId(_chaveContainerVazioTipoFrigorifico) == null ? 0 : Convert.ToDouble(this._configuracaoTranslogicRepository.ObterPorId(_chaveContainerVazioTipoFrigorifico).Valor);
                }
                else
                {
                    valorMercadoria = _listaCteDetalhe.Sum(g => g.ValorTotalNotaFiscal);
                }

                cte09 = new Cte09();

                cte09.CfgUn = ObterIdentificadorFilial(); // Codigo interno da filial
                cte09.CfgDoc = Convert.ToInt32(_cte.Numero); // Numero do documento CT-e
                cte09.CfgSerie = int.Parse(_cte.Serie).ToString(); // Serie do documento CT-e
                cte09.IdComp = 3; // Numero sequecial da tabela
                cte09.CompXnome = "AdValorem";
                cte09.CompVcomp = valorMercadoria * percentualSeguro;

                _cte09Repository.Inserir(cte09);
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(_cte, "GravarConfigCteEnvioService", string.Format("GravarComponentesValorPrestacaoFilho: {0}", ex.Message), ex);
                throw;
            }
        }

        private double ObterValorTarifa()
        {
            double valorTarifa = 0.0;
            string letraFerrovia = _serieDespachoUf.LetraFerrovia;
            IList<ComposicaoFreteContrato> listaComposicaoFreteContrato = _composicaoFreteContratoRepository.ObterPorNumeroContrato(_contratoHistorico.NumeroContrato, _cteRaiz.DataHora, letraFerrovia, true);
            IList<ComposicaoFreteContrato> listaComposicaoFreteContratoVg = _composicaoFreteContratoRepository.ObterPorNumeroContrato(_contratoHistorico.NumeroContrato, _cteRaiz.DataHora, "VG", true);
            valorTarifa = listaComposicaoFreteContrato.Sum(g => g.ValorSemIcms).Value;
            valorTarifa += listaComposicaoFreteContratoVg.Sum(g => g.ValorSemIcms).Value;

            return Math.Round(valorTarifa, 2, MidpointRounding.AwayFromZero);
        }

        private double ObterValorTarifaComICMS()
        {
            double valorTarifa = 0.0;
            string letraFerrovia = _serieDespachoUf.LetraFerrovia;
            ////IList<ComposicaoFreteContrato> listaComposicaoFreteContrato = _composicaoFreteContratoRepository.ObterPorNumeroContrato(_contratoHistorico.NumeroContrato, _cteRaiz.DataHora, letraFerrovia, true);
            ////IList<ComposicaoFreteContrato> listaComposicaoFreteContratoVg = _composicaoFreteContratoRepository.ObterPorNumeroContrato(_contratoHistorico.NumeroContrato, _cteRaiz.DataHora, "VG", true);
            ////valorTarifa = listaComposicaoFreteContrato.Sum(g => g.ValorComIcms).Value;
            ////valorTarifa += listaComposicaoFreteContratoVg.Sum(g => g.ValorComIcms).Value;
            var listaComposicaoFreteContrato = _composicaoFreteContratoRepository.ObterPorNumeroContrato(_contratoHistorico.NumeroContrato);
            valorTarifa = listaComposicaoFreteContrato.Sum(g => g.ValorComIcms).Value;

            return Math.Round(valorTarifa, 2, MidpointRounding.AwayFromZero);
        }

        private bool ValidarInformacoesCteNormalAnulacao()
        {
            try
            {
                if (_cte.TipoCte == TipoCteEnum.Anulacao)
                {
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                LogHelper.Log(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
                throw;
            }
        }

        /// <summary>
        /// Tabela de informacoes do CT-e Normal ou Anulacao (0-1) - ICTE10_ICN
        /// </summary>
        private void GravarInformacoesCteNormalAnulacao()
        {
            try
            {
                Cte10 cte10 = new Cte10();
                int numConvertido;

                // Codigo interno da filial (PK)
                cte10.CfgUn = ObterIdentificadorFilial();

                // Numero do documento CT-e (PK)
                int.TryParse(_cte.Numero, out numConvertido);
                cte10.CfgDoc = numConvertido;

                // Serie do documento CT-e (PK)
                cte10.CfgSerie = int.Parse(_cte.Serie).ToString();

                // Mercadoria
                string mercadoria = _fluxoComercial.Mercadoria.DescricaoDetalhada;

                // Soma os valores da nota fiscal                
                double totalMercadoria;
                if (_fluxoComercial.Mercadoria.CodigoMercadoriaSAP == "011")
                {
                    totalMercadoria = this._configuracaoTranslogicRepository.ObterPorId(_chaveContainerVazioTipoFrigorifico) == null ? 0 : Convert.ToDouble(this._configuracaoTranslogicRepository.ObterPorId(_chaveContainerVazioTipoFrigorifico).Valor);
                }
                else
                {
                    totalMercadoria = _cte.ValorTotalMercadoria ?? 0.0;
                }

                // Valor total da mercadoria
                cte10.InfCargaVmerc = totalMercadoria;

                // Produto predominante. 
                cte10.InfCargaPropred = Tools.TruncateString(mercadoria, 58);

                // Outras caracter�sticas da carga
                cte10.InfCargaXoutCat = string.Empty;

                _cte10Repository.InserirOuAtualizar(cte10);
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(_cte, "GravarConfigCteEnvioService", string.Format("GravarInformacoesCteNormalAnulacao: {0}", ex.Message), ex);
                LogHelper.Log(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
                throw;
            }
        }

        private bool ValidarInformacoesQtdeCarga()
        {
            try
            {
                if (_cte.TipoCte == TipoCteEnum.Anulacao)
                {
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                LogHelper.Log(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
                throw;
            }
        }

        /// <summary>
        /// Tabela de informacoes da qtde da carga (1-N) - ICTE11_ICN_INFQ
        /// </summary>
        private void GravarInformacoesQtdeCarga()
        {
            try
            {
                Cte11 cte11 = new Cte11();
                int numConvertido;
                double somaPeso = _cte.PesoTotalNf ?? 0.0;

                // Codigo interno da filial (PK)
                cte11.CfgUn = ObterIdentificadorFilial();

                // Numero do documento CT-e (PK)
                int.TryParse(_cte.Numero, out numConvertido);
                cte11.CfgDoc = numConvertido;

                // Serie do documento CT-e (PK)
                cte11.CfgSerie = int.Parse(_cte.Serie).ToString();

                // Numero sequecial da tabela (PK)
                cte11.IdIcnInfq = 1;

                // C�digo da unidade de medida
                switch (_fluxoComercial.UnidadeMedida.Id)
                {
                    case "M3":
                        cte11.InfqCunid = 00;
                        // Tipo da medida
                        cte11.InfqTpMed = "PESO CUBADO";
                        break;
                    case "KG":
                        cte11.InfqCunid = 01;
                        // Tipo da medida
                        cte11.InfqTpMed = "PESO BRUTO";
                        break;
                    case "TO":
                        cte11.InfqCunid = 02;
                        // Tipo da medida
                        cte11.InfqTpMed = "PESO BRUTO";
                        break;
                    case "UN":
                        cte11.InfqCunid = 03;
                        // Tipo da medida
                        cte11.InfqTpMed = "QUANTIDADE";
                        break;
                    case "L":
                        cte11.InfqCunid = 04;
                        // Tipo da medida
                        cte11.InfqTpMed = "LITRAGEM";
                        break;
                }

                // Quantidade da carga transportada
                if (cte11.InfqCunid == 02)
                {
                    // Se rateio de CTE
                    if (_fluxoComercial.IndRateioCte == true)
                    {
                        //cte11.InfqQcarga = (double)_carregamentoService.ObterQtdeCtePorVagaoVigente(_cte.Chave);

                        cte11.InfqQcarga = (double)_carregamentoService.ObterPesoCalculoSapDespCte(_cte.Chave);
                        cte11.InfqCunid = 03;
                        cte11.InfqTpMed = "QUANTIDADE";

                    }
                    else if (_carregamentoService.VerificarFluxoConteiner(_fluxoComercial) || VerificaEmpresaBrado(_contrato.EmpresaPagadora))
                    {
                        cte11.InfqQcarga = _cte.PesoParaCalculo;
                        cte11.InfqCunid = 03;
                        cte11.InfqTpMed = "QUANTIDADE";
                    }
                    else
                    {
                        cte11.InfqQcarga = _cte.PesoParaCalculo * 1000;
                        cte11.InfqCunid = 01;
                    }
                }
                else
                {
                    cte11.InfqQcarga = _cte.PesoParaCalculo;
                }

                _cte11Repository.Inserir(cte11);
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(_cte, "GravarConfigCteEnvioService", string.Format("GravarInformacoesQtdeCarga: {0}", ex.Message), ex);
                LogHelper.Log(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
                throw;
            }
        }

        /// <summary>
        /// Tabela de informacoes dos cointainers da qtde da carga (1-N) - ICTE12_ICN_CONT
        /// </summary>
        public bool ValidarInformacoesContainerQtdeCarga()
        {
            try
            {
                if (_cte.Versao.CodigoVersao.Equals("2.00") || _cte.Versao.CodigoVersao.Equals("3.00"))
                {
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                LogHelper.Log(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
                throw;
            }
        }

        /// <summary>
        /// Tabela de informacoes dos cointainers da qtde da carga (1-N) - ICTE12_ICN_CONT
        /// </summary>
        private void GravarInformacoesContainerQtdeCarga()
        {
            try
            {
                // Recupera as informa��es do container
                int index = 0;

                IList<CteDetalhe> listaCteDetalhe = _cteDetalheRepository.ObterPorCte(_cte);
                if (listaCteDetalhe.Count > 0)
                {
                    // Teste para verificar se � um CTE de conteiner
                    if (!string.IsNullOrEmpty(listaCteDetalhe[0].ConteinerNotaFiscal))
                    {
                        // Seleciona todos os conteiners dos detelhes agrupando por c�digo
                        List<string> listaConteiner = listaCteDetalhe.GroupBy(g => g.ConteinerNotaFiscal).Select(s => s.Key).ToList();

                        foreach (string identificacaoConteiner in listaConteiner)
                        {
                            Cte12 cte12 = new Cte12();
                            cte12.CfgUn = ObterIdentificadorFilial(); // Codigo interno da filial
                            cte12.CfgDoc = Convert.ToInt32(_cte.Numero); // Numero do documento CT-e
                            cte12.CfgSerie = int.Parse(_cte.Serie).ToString(); // Serie do documento CT-e
                            cte12.IdIcnCont = ++index; // Numero sequecial da tabela
                            cte12.ContQtNcont = identificacaoConteiner; // N�mero do container
                            cte12.ContQtDprev = null; // Data prevista da entrega

                            _cte12Repository.Inserir(cte12);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                // Limpa o string builder
                _cteLogService.InserirLogErro(_cte, "GravarConfigCteEnvioService", string.Format("GravarInformacoesContainerQtdeCarga: {0}", ex.Message), ex);
                LogHelper.Log(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
                throw;
            }
        }

        /// <summary>
        /// Recupera o valor total do cte
        /// </summary>
        private void CalcularValorCte()
        {
            try
            {
                /* Regra passada pelo fiscal:
                 *	Caso seja um CTE filho, n�o tem base calculo de icms e n�o tem o valor do ICMS.
                 *	No entanto tem o valor do CTE.
                 *	Regra: 02/02/2012
                 **/
                double baseCalculoIcms = 0;

                _cte.PesoParaCalculo = _pesoParaCalculo;
                _cte.PercentualAliquotaIcms = _contratoHistorico.PercentualAliquotaIcms ?? 0.0;

                if (_fluxoComercial.IndRateioCte ?? false)
                {
                    baseCalculoIcms = CalcularBaseCalculoIcmsCteRateado(_cte.Chave);
                }
                else
                {
                    baseCalculoIcms = CalcularBaseCalculoIcms();
                }

                double percentualAliquotaIcms = _cte.PercentualAliquotaIcms ?? 0.0;

                // -> faz a verifica��o de centavos no arredondamento 
                double tarifa = ObterValorTarifaComICMS();
                double tarifaVezesPeso = Math.Round(tarifa * _pesoParaCalculo, 2, MidpointRounding.AwayFromZero);

                RegistrarLogDetalhado("Realizando valida��o de centavos do c�lculo do cte");
                RegistrarLogDetalhado(string.Format("Valor do CTE = {0}", baseCalculoIcms));
                RegistrarLogDetalhado(string.Format("Valor Tarifa * Peso = {0}", tarifaVezesPeso));
                RegistrarLogDetalhado(string.Format("N�o � ferroeste = {0}", !OrigemFerroeste()));
                RegistrarLogDetalhado(string.Format("Empresa pagadora brado = {0}", VerificaEmpresaBrado(_contrato.EmpresaPagadora)));

                if (!(_fluxoComercial.IndRateioCte ?? false))
                {
                    if (!OrigemFerroeste() || VerificaEmpresaBrado(_contrato.EmpresaPagadora))
                    {
                        RegistrarLogDetalhado("Pr� requisito para verifica��o dos centavos aceitos");

                        if (baseCalculoIcms != tarifaVezesPeso)
                        {
                            _diferencaDeCentavosNaComposicaoFrete = Math.Round(tarifaVezesPeso - baseCalculoIcms, 2, MidpointRounding.AwayFromZero);
                            RegistrarLogDetalhado(string.Format("Corre��o de peso aplicada. Diferen�a de valor encontrada = {0}", _diferencaDeCentavosNaComposicaoFrete));
                            baseCalculoIcms = tarifaVezesPeso;
                        }
                    }
                }

                double valorIcms = baseCalculoIcms * (percentualAliquotaIcms / 100);
                double valorCte = baseCalculoIcms;

                // Regra do CTE filho que n�o tem valor do ICMS
                if (_cte.Id != _cteRaizAgrupamento.Id)
                {
                    baseCalculoIcms = 0.0;
                    valorIcms = 0.0;
                    _cte.PercentualAliquotaIcms = 0.0;
                }

                _cte.BaseCalculoIcms = baseCalculoIcms;
                _cte.ValorIcms = valorIcms;
                // _cte.ValorFrete = CalcularValorFrete();
                _cte.ValorFrete = valorCte;
                _cte.ValorSeguro = ObterValorSeguro();
                _cte.ValorTotalNf = CalcularValorTotalNotaFiscal();
                _cte.PesoTotalNf = CalcularPesoTotal();
                _cte.ValorCte = valorCte;
                _cte.DescontoPorPeso = (_contratoHistorico.DescontoPorPeso ?? 0.0) * _cte.PesoParaCalculo;

                _cte.ValorDescontoTonelada = CalcularDescontoPorTonelada();
                _cte.DescontoPorPercentual = CalcularDescontoPorPercentual();

                _cte.ValorReceber = (_cte.ValorCte - Math.Abs(_cte.ValorDescontoTonelada.Value)) - Math.Abs(_cte.DescontoPorPercentual.Value);

                // _cte.ValorTotalMercadoria = _listaCteDetalhe.Sum(g => g.ValorNotaFiscal);

                RegistrarLogDetalhado("Antes de entrar na unidade de medida ");

                if (_fluxoComercial.UnidadeMedida.Id.Equals("M3"))
                {
                    if (_listaCteDetalhe.Any(cteDetalhe => cteDetalhe.VolumeNotaFiscal == 0.0 || cteDetalhe.PesoTotal == 0.0 || cteDetalhe.ValorTotalNotaFiscal == 0.0))
                    {
                        if (_fluxoComercial.Mercadoria.CodigoMercadoriaSAP == "011")
                        {
                            _cte.ValorTotalMercadoria = this._configuracaoTranslogicRepository.ObterPorId(_chaveContainerVazioTipoFrigorifico) == null ? 0 : Convert.ToDouble(this._configuracaoTranslogicRepository.ObterPorId(_chaveContainerVazioTipoFrigorifico).Valor);
                        }
                        else
                        {
                            _cte.ValorTotalMercadoria = _listaCteDetalhe.Sum(g => g.ValorNotaFiscal);
                        }
                        // throw new Exception("Valor ou volume na nota fiscal inv�lido!");
                    }
                    else
                    {
                        if (_fluxoComercial.Mercadoria.CodigoMercadoriaSAP == "011")
                        {
                            _cte.ValorTotalMercadoria = this._configuracaoTranslogicRepository.ObterPorId(_chaveContainerVazioTipoFrigorifico) == null ? 0 : Convert.ToDouble(this._configuracaoTranslogicRepository.ObterPorId(_chaveContainerVazioTipoFrigorifico).Valor);
                        }
                        else
                        {
                            _cte.ValorTotalMercadoria = (from cteDetalhe in _listaCteDetalhe
                                                         let taxa =
                                                             (cteDetalhe.VolumeNotaFiscal * 100) / cteDetalhe.PesoTotal
                                                         select (cteDetalhe.ValorTotalNotaFiscal * taxa) / 100).Sum();
                        }
                    }
                }
                else
                {
                    if (_listaCteDetalhe.Any(cteDetalhe => cteDetalhe.PesoNotaFiscal == 0.0 || cteDetalhe.PesoTotal == 0.0 || cteDetalhe.ValorTotalNotaFiscal == 0.0))
                    {
                        if (_fluxoComercial.Mercadoria.CodigoMercadoriaSAP == "011")
                        {
                            _cte.ValorTotalMercadoria = this._configuracaoTranslogicRepository.ObterPorId(_chaveContainerVazioTipoFrigorifico) == null ? 0 : Convert.ToDouble(this._configuracaoTranslogicRepository.ObterPorId(_chaveContainerVazioTipoFrigorifico).Valor);
                        }
                        else
                        {
                            _cte.ValorTotalMercadoria = _listaCteDetalhe.Sum(g => g.ValorNotaFiscal);
                        }
                        // throw new Exception("Valor ou peso da nota fiscal inv�lido!");
                    }
                    else
                    {
                        if (_fluxoComercial.Mercadoria.CodigoMercadoriaSAP == "011")
                        {
                            _cte.ValorTotalMercadoria = this._configuracaoTranslogicRepository.ObterPorId(_chaveContainerVazioTipoFrigorifico) == null ? 0 : Convert.ToDouble(this._configuracaoTranslogicRepository.ObterPorId(_chaveContainerVazioTipoFrigorifico).Valor);
                        }
                        else
                        {
                            RegistrarLogDetalhado("Inicio linq do valor total da Mercadoria ");
                            _cte.ValorTotalMercadoria = (from cteDetalhe in _listaCteDetalhe
                                                         let taxa = (cteDetalhe.PesoNotaFiscal * 100) / cteDetalhe.PesoTotal
                                                         select (cteDetalhe.ValorTotalNotaFiscal * taxa) / 100).Sum();
                        }
                    }
                }

                _cte.Cfop = _cfop;
                // Arredondamento dos valores
                _cte.PesoParaCalculo = Math.Round(_cte.PesoParaCalculo.Value, 3);
                _cte.BaseCalculoIcms = Math.Round(baseCalculoIcms, 2);
                _cte.ValorIcms = Math.Round(valorIcms, 2);
                _cte.ValorFrete = Math.Round(_cte.ValorFrete.Value, 2);
                _cte.ValorSeguro = Math.Round(_cte.ValorSeguro.Value, 2);
                _cte.ValorTotalNf = Math.Round(_cte.ValorTotalNf.Value, 2);
                _cte.PesoTotalNf = Math.Round(_cte.PesoTotalNf.Value, 3);
                _cte.ValorCte = Math.Round(valorCte, 2);
                _cte.DescontoPorPeso = Math.Round(_cte.DescontoPorPeso.Value, 2);
                _cte.DescontoPorPercentual = Math.Round(_cte.DescontoPorPercentual.Value, 2);
                _cte.ValorDescontoTonelada = Math.Round(_cte.ValorDescontoTonelada.Value, 2);
                _cte.ValorReceber = Math.Round(_cte.ValorReceber.Value, 2);
                _cte.ValorTotalMercadoria = Math.Round(_cte.ValorTotalMercadoria.Value, 2);

                ////// Se rateio de CTE
                ////if (_cte.FluxoComercial.IndRateioCte == true)
                ////{
                ////    _cte.Qtde = (double)_carregamentoService.ObterPesoCalculoSapDespCte(_cte.Chave); 
                ////}

                if (_cte.SiglaUfFerrovia == "MS")
                {
                    // Verifica se existe a substitui��o tributaria, caso a aliquota for maior 0.0, calcula o valor liquido
                    double aliquotaSt = _contratoHistorico.AliquotaIcmsSubstProduto ?? 0.0;
                    if (aliquotaSt > 0.0)
                    {
                        RegistrarLogDetalhado("CalcularValorLiquidoSubstituicaoTributaria ");
                        _cte.ValorReceber = Math.Round(CalcularValorLiquidoSubstituicaoTributaria(), 2);
                    }
                }

                _cteRepository.AtualizarSemSessao(_cte);

                RegistrarLogDetalhado("InserirComposicaoFreteCte ");
                InserirComposicaoFreteCte(baseCalculoIcms);
            }
            catch (NHibernate.MappingException e)
            {
                _cteLogService.InserirLogErro(_cte, "GravarConfigCteEnvioService", string.Format("CalcularValorCte: {0}", e.StackTrace + " INNER: " + e));
                throw e;
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(_cte, "GravarConfigCteEnvioService", string.Format("CalcularValorCte: {0}", ex.StackTrace + " INNER: " + ex));
                throw;
            }

        }

        private double CalcularRateioCompFreteCte(string condTarifaContrato, string pChaveCte, double pValorTotalRateio = 0)
        {
            //// Obt�m o peso total do carregamento para o fluxo informado para efetuar o rateio na sequ�ncia com base nele
            double pesoTotal = _carregamentoService.ObterPesoTotalCarregamentoPorVagaoVigenteFluxo(pChaveCte);

            //// Obt�m o n�mero do conteiners na opera��o e fluxo
            int numConteiners = _carregamentoService.ObterNumeroConteinersPorVagaoVigenteFluxo(pChaveCte);

            //// Obter o peso total das notas do cte
            double pesoNotasCte = ObterPesoNotasCte();

            //// Valor total do rateio
            double valorTotalRateio = numConteiners * pValorTotalRateio;

            //// Guarda o valor final do cte
            double valorFinal = EfetuaRateioCte(valorTotalRateio, pesoTotal, pesoNotasCte);


            ////// pega a ultima condi��o da composi��o frete 
            ////var listaComposicaoFreteContratoTmp = _composicaoFreteContratoRepository.ObterPorNumeroContrato(_contratoHistorico.NumeroContrato).OrderBy(p => p.CondicaoTarifa).LastOrDefault();


            //////// Se � a condi��o frete que fecha, calcula a diferen�a de centavos.
            ////if  ((listaComposicaoFreteContratoTmp != null) && (listaComposicaoFreteContratoTmp.CondicaoTarifa == condTarifaContrato))
            ////{
            ////    // pega as outras condi��es
            ////    var ctesConteinerCarga = _cteConteinerRepository.ObterPorCteAgrupCarga(_cte.Id ?? 0);


            ////    var listaComposicaoFreteContratoTmp = _composicaoFreteContratoRepository.ObterPorNumeroContrato(_contratoHistorico.NumeroContrato)


            ////    double valorFinalCalculado = 0;
            ////    foreach (var cteConteiner in ctesConteinerCarga)
            ////    {
            ////        if (cteConteiner.Cte.Id != _cte.Id)
            ////        {
            ////            // pegar valor da composicao frete contrato deste cte, esta tarifa.
            ////            var compFretesCte =
            ////                _composicaoFreteCteRepository.ObterComposicaoFreteCtePorCte(cteConteiner.Cte);

            ////            if (compFretesCte.Any())
            ////            {
            ////                var compFreteCte = compFretesCte.FirstOrDefault(p => p.CondicaoTarifa == condTarifaContrato);
            ////                valorFinalCalculado += compFreteCte == null ? 0 : compFreteCte.ValorSemIcms ?? 0;
            ////            }
            ////        }
            ////    }

            ////    double valorFinalSomado = valorFinal + valorFinalCalculado;

            ////    // se valor final � diferente do valor total do carregamento, acerta a diferen�a
            ////    if (valorFinalSomado != valorTotalRateio)
            ////    {
            ////        double diferenca = valorTotalRateio - valorFinalSomado;
            ////        valorFinal = diferenca + valorFinal;
            ////    }
            ////}

            //// Efetua o rateio de cte
            return valorFinal;
        }


        private double CalcularBaseCalculoIcmsCteRateado(string pChaveCte)
        {
            //// Obt�m o peso total do carregamento para o fluxo informado para efetuar o rateio na sequ�ncia com base nele
            double pesoTotal = _carregamentoService.ObterPesoTotalCarregamentoPorVagaoVigenteFluxo(pChaveCte);

            //// Obt�m o n�mero do conteiners na opera��o e fluxo
            int numConteiners = _carregamentoService.ObterNumeroConteinersPorVagaoVigenteFluxo(pChaveCte);

            //// Obter o peso total das notas do cte
            double pesoNotasCte = ObterPesoNotasCte();

            //// obt�m o valor total do carregamento, com base no n�mero de conteiners X valores da composicao do frete.
            double valorTotalRateio = CalcularBaseCalculoIcmsCteConteiner(numConteiners);

            //// Guarda o valor final do cte
            double valorFinal = EfetuaRateioCte(valorTotalRateio, pesoTotal, pesoNotasCte);


            //// Se � o CTE que fecha a carga, verifica se tem que acertar diferen�a de centavos.
            if (_cte.IndMultiploDespacho == false)
            {
                // pega os outros ctes da carga
                var ctesConteinerCarga = _cteConteinerRepository.ObterPorCteAgrupamento(_cte.Id ?? 0);

                double valorFinalCalculado = 0;
                foreach (var cteConteiner in ctesConteinerCarga)
                {
                    if (cteConteiner.Cte.Id != _cte.Id)
                    {
                        valorFinalCalculado += _cteRepository.ObterPorIdCte(cteConteiner.Cte.Id ?? 0).ValorCte;
                    }
                }

                double valorFinalSomado = valorFinal + valorFinalCalculado;

                // se valor final � diferente do valor total do carregamento, acerta a diferen�a
                if (valorFinalSomado != valorTotalRateio)
                {
                    double diferenca = valorTotalRateio - valorFinalSomado;
                    valorFinal = diferenca + valorFinal;
                }
            }

            //// Efetua o rateio de cte
            return valorFinal;
        }


        ////private double CalcularBaseCalculoIcmsCteRateado(string condTarifaContrato, string pChaveCte, double pValorTotalRateio = 0)
        ////{
        ////    //// Obt�m o peso total do carregamento para o fluxo informado para efetuar o rateio na sequ�ncia com base nele
        ////    double pesoTotal = _carregamentoService.ObterPesoTotalCarregamentoPorVagaoVigenteFluxo(pChaveCte);

        ////    //// Obt�m o n�mero do conteiners na opera��o e fluxo
        ////    int numConteiners = _carregamentoService.ObterNumeroConteinersPorVagaoVigenteFluxo(pChaveCte);

        ////    //// Obter o peso total das notas do cte
        ////    double pesoNotasCte = ObterPesoNotasCte();

        ////    //// obt�m o valor total do carregamento, com base no n�mero de conteiners X valores da composicao do frete.
        ////    double valorTotalRateio = 0;
        ////    if (pValorTotalRateio == 0)
        ////    {
        ////        valorTotalRateio = CalcularBaseCalculoIcmsCteConteiner(numConteiners, condTarifaContrato);
        ////    }
        ////    else
        ////    {
        ////        valorTotalRateio = pValorTotalRateio;
        ////    }


        ////    /// Guarda o valor final do cte
        ////    double valorFinal = EfetuaRateioCte(valorTotalRateio, pesoTotal, pesoNotasCte);

        ////    //// Se � o CTE que fecha a carga, verifica se tem que acertar diferen�a de centavos.
        ////    if (_cte.IndMultiploDespacho == false)
        ////    {
        ////        // pega os outros ctes da carga
        ////        var ctesConteinerCarga = _cteConteinerRepository.ObterPorCteAgrupCarga(_cte.Id ?? 0);

        ////        double valorFinalCalculado = 0;
        ////        foreach (var cteConteiner in ctesConteinerCarga)
        ////        {
        ////            if (cteConteiner.Cte.Id != _cte.Id)
        ////            {
        ////                // pegar valor da composicao frete contrato deste cte, esta tarifa.
        ////                var compFretesCte =
        ////                    _composicaoFreteCteRepository.ObterComposicaoFreteCtePorCte(cteConteiner.Cte);

        ////                if (compFretesCte.Any())
        ////                {
        ////                    var compFreteCte = compFretesCte.FirstOrDefault(p => p.CondicaoTarifa == condTarifaContrato);
        ////                    valorFinalCalculado += compFreteCte == null ? 0 : compFreteCte.ValorSemIcms ?? 0;
        ////                }
        ////            }
        ////        }

        ////        double valorFinalSomado = valorFinal + valorFinalCalculado;

        ////        // se valor final � diferente do valor total do carregamento, acerta a diferen�a
        ////        if (valorFinalSomado != valorTotalRateio)
        ////        {
        ////            double diferenca = valorTotalRateio - valorFinalSomado;
        ////            valorFinal = diferenca + valorFinal;
        ////        }
        ////    }

        ////    //// Efetua o rateio de cte
        ////    return valorFinal;
        ////}

        private double ObterPesoNotasCte()
        {
            IList<CteDetalhe> listaCteDetalhe = _cteDetalheRepository.ObterPorCte(_cte);
            return listaCteDetalhe.Sum(p => p.PesoNotaFiscal);
        }

        private double CalcularBaseCalculoIcmsSemArredondar()
        {
            var valorCotacao = ObterValorCotacao();
            var toneladaUtil = _cte.PesoParaCalculo ?? 0.0;
            var coeficiente = Math.Round(100 / (100 - _cte.PercentualAliquotaIcms ?? 0.0), 6);

            var tarifaSomadaVezesPeso = Math.Round(
                _composicaoFreteContratoRepository
                    .ObterPorNumeroContrato(_contratoHistorico.NumeroContrato)
                    .Sum(e => e.ValorSemIcms * valorCotacao * coeficiente * toneladaUtil)
                    .Value,
                2,
                MidpointRounding.AwayFromZero
            );

            return tarifaSomadaVezesPeso;
        }

        private static double EfetuaRateioCte(double valorTotalRateio, double pesoTotal, double pesoNotasCte, int casasArred = 2)
        {
            double resultado = 0;
            if (casasArred == 0)
            {
                resultado = (pesoNotasCte * valorTotalRateio) / pesoTotal;
            }
            else
            {
                resultado = Math.Round((pesoNotasCte * valorTotalRateio) / pesoTotal, casasArred);
            }

            var result = resultado;
            return result;
        }

        /// <summary>
        /// Calcula o valor do Cte para a Ferroeste
        /// </summary>
        private void CalcularValorCteFerroeste()
        {
            try
            {
                double valorCotacao = ObterValorCotacao();
                IList<ComposicaoFreteContrato> listaComposicaoFreteContrato = _composicaoFreteContratoRepository.ObterPorNumeroContrato(_contratoHistorico.NumeroContrato);
                double freteComIcms = 0.0;
                double freteSemIcms = 0.0;

                double valorIcms = 0.0;

                double valorCarga = 0.0;
                double valorDescarga = 0.0;

                double zcal = 0.0;
                double zcas = 0.0;
                double zdcs = 0.0;
                double zdcl = 0.0;

                double valorVagao = 0.0;
                double zvgs = 0.0;
                double zvgl = 0.0;

                double partilhaFrete = 0.0;
                double distanciaTotal = 0.0;
                double distanciaAll = 0.0;
                double distanciaFerroeste = 0.0;
                double zfcl = 0.0;
                double zfcs = 0.0;

                double valorAll = 0.0;
                double zicl = 0.0;
                double zics = 0.0;

                double valorCte = 0.0;

                // 1 - Calcular o frete com ICMS e sem ICMS
                IList<ComposicaoFreteContrato> listaZFRL = listaComposicaoFreteContrato.Where(g => g.CondicaoTarifa == "ZFRL").ToList();
                freteComIcms = Math.Round(listaZFRL.Sum(g => g.ValorComIcms).Value * _pesoParaCalculo, 2, MidpointRounding.AwayFromZero);
                freteSemIcms = Math.Round(listaZFRL.Sum(g => g.ValorSemIcms).Value * _pesoParaCalculo, 2, MidpointRounding.AwayFromZero);

                // 2 - Calcular o valor do Icms
                // valorIcms = freteComIcms - freteSemIcms;

                // 3 - Calcular o valor da carga e 4 - Calcular o valor da descarga
                valorCarga = freteSemIcms * 0.05; // 5 %
                valorCarga = Math.Round(valorCarga, 2, MidpointRounding.AwayFromZero);

                valorDescarga = freteSemIcms * 0.05; // 5 %;
                valorDescarga = Math.Round(valorDescarga, 2, MidpointRounding.AwayFromZero);
                if (OrigemFerroeste())
                {
                    zcas = valorCarga;
                    zdcl = valorDescarga;
                }
                else
                {
                    zcal = valorCarga;
                    zdcs = valorDescarga;
                }

                // 5 - Calcular valor do vag�o
                valorVagao = freteSemIcms * 0.15; // 15 %
                valorVagao = (double)Math.Round((decimal)valorVagao, 2, MidpointRounding.AwayFromZero);
                if ((_empresaProprietariaVagao.Sigla == "S") || (_empresaProprietariaVagao.Sigla == "M"))
                {
                    // Verifica se o tomador de servi�o � a Bunge (RegraFerroesteBunge)
                    if (RegraFerroesteBunge())
                    {
                        zvgl = valorVagao;
                    }
                    else
                    {
                        zvgs = valorVagao;
                    }
                }
                else
                {
                    zvgl = valorVagao;
                }

                // 6 - Calcular a partilha do frete
                partilhaFrete = freteSemIcms - valorCarga - valorDescarga - valorVagao;
                partilhaFrete = Math.Round(partilhaFrete, 2, MidpointRounding.AwayFromZero);
                IList<ComposicaoFreteContrato> listaZDSL = listaComposicaoFreteContrato.Where(g => g.CondicaoTarifa == "ZDSL").ToList();
                IList<ComposicaoFreteContrato> listaZDSS = listaComposicaoFreteContrato.Where(g => g.CondicaoTarifa == "ZDSS").ToList();
                distanciaAll = listaZDSL.Sum(g => g.ValorSemIcms).Value;
                distanciaFerroeste = listaZDSS.Sum(g => g.ValorSemIcms).Value;
                distanciaTotal = distanciaAll + distanciaFerroeste;
                if (distanciaTotal > 0)
                {
                    zfcl = (partilhaFrete * distanciaAll) / distanciaTotal;
                    zfcl = Math.Round(zfcl, 2, MidpointRounding.AwayFromZero);
                    zfcs = partilhaFrete - zfcl;
                    zfcs = Math.Round(zfcs, 2, MidpointRounding.AwayFromZero);
                }
                else
                {
                    zfcl = 0.0;
                    zfcs = 0.0;
                }

                // 7 - Se o fluxo for tributadom, calcular o multiplicador do ICMS
                // Todo: verificar quando o fluxo � tributado
                int tipoTributacao = _contratoHistorico.TipoTributacao ?? 0;

                double aliquotaIcms = _contratoHistorico.PercentualAliquotaIcms ?? 0.0;
                double coeficiente = 100 / (100 - aliquotaIcms);
                _cte.PercentualAliquotaIcms = _contratoHistorico.PercentualAliquotaIcms ?? 0.0;
                coeficiente = Math.Round(coeficiente, 6);
                valorAll = zcal + zdcl + zvgl + zfcl;
                valorAll = Math.Round(valorAll, 2);

                valorIcms = (valorAll * coeficiente) - valorAll;

                zicl = (valorAll * coeficiente) - valorAll;
                zicl = Math.Round(zicl, 2);
                /* ICMS da Ferroeste - corrigido */
                zics = ((zfcs + zcas) * coeficiente) - (zfcs + zcas);
                zics = Math.Round(zics, 2);

                // 8 - Valor do CTE
                double valorTotalTarifa = 0.0;
                double valorTarifaCotacao = 0.0;
                IList<ComposicaoFreteContrato> listaValorAll = listaComposicaoFreteContrato.Where(g => g.Ferrovia != "S" && g.CondicaoTarifa != "ZFRL" && g.CondicaoTarifa != "ZDES" && g.CondicaoTarifa != "ZDSL" && g.CondicaoTarifa != "ZDEV").ToList();
                foreach (ComposicaoFreteContrato composicaoFreteContrato in listaValorAll)
                {
                    valorTarifaCotacao = composicaoFreteContrato.ValorComIcms.Value * valorCotacao;
                    valorTotalTarifa += Math.Round(valorTarifaCotacao * _pesoParaCalculo, 2, MidpointRounding.AwayFromZero);
                }

                valorCte = zcal + zdcl + zvgl + zfcl + zicl + valorTotalTarifa;
                // Verifica se � um fluxo da Brado / Conteiner
                if (_cteService.VerificarConteinerBrado(_fluxoComercial))
                {
                    valorCte += zcas + zdcs + zvgs + zfcs + zics;
                }

                valorCte = Math.Round(valorCte, 2);


                /* Corrigido o valor do ICMS para ser realmente 12% do valor total do CTE */
                valorIcms = (valorCte * (aliquotaIcms / 100));

                // Atualiza o valor do Cte da Ferroeste
                AtualizarValorCteFerroeste(valorCte, valorIcms);

                // Insere a composicao frete cte
                InserirComposicaoFreteCteFerroeste(
                        zcal,
                        zcas,
                        zdcs,
                        zdcl,
                        zvgs,
                        zvgl,
                        zfcl,
                        zfcs,
                        zicl,
                        zics,
                        valorCte,
                        coeficiente,
                        listaComposicaoFreteContrato);
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(_cte, "GravarConfigCteEnvioService", string.Format("CalcularValorCteFerroeste: {0}", ex.Message), ex);
                throw;
            }
        }

        /// <summary>
        /// Obtem o Ambiente da Sefaz
        /// (1) - Produ��o
        /// (2) - Homologa��o
        /// </summary>
        /// <returns>Retorna o ambiente da Sefaz</returns>
        private int ObterAmbienteSefaz()
        {
            EstacaoMae origem = _fluxoComercial.OrigemIntercambio ?? _fluxoComercial.Origem;

            CteSerieNumeroEmpresaUf serieEmpresaUf = _cteSerieNumeroEmpresaUfRepository.ObterPorEmpresaUf(origem.EmpresaConcessionaria, origem.Municipio.Estado.Sigla);

            if (serieEmpresaUf == null)
            {
                throw new Exception("N�o foi poss�vel recuperar o tipo de ambiente da Sefaz");
            }

            return serieEmpresaUf.ProducaoSefaz ? 1 : 2;
        }

        private void InserirComposicaoFreteCteFerroeste(double zcal, double zcas, double zdcs, double zdcl, double zvgs, double zvgl, double zfcl, double zfcs, double zicl, double zics, double valorCte, double coeficiente, IList<ComposicaoFreteContrato> listaComposicaoFreteContrato)
        {
            ComposicaoFreteCte composicaoFreteCte;

            composicaoFreteCte = new ComposicaoFreteCte();
            composicaoFreteCte.Cte = _cte;
            composicaoFreteCte.CondicaoTarifa = "ZCAL";
            // composicaoFreteCte.Ferrovia = letraFerrovia;
            composicaoFreteCte.ValorComIcms = Math.Round(zcal, 2);
            composicaoFreteCte.ValorSemIcms = Math.Round(zcal, 2);
            composicaoFreteCte.ValorToneladaComIcms = Math.Round(zcal, 2);
            composicaoFreteCte.ValorToneladaSemIcms = Math.Round(zcal, 2);
            composicaoFreteCte.Coeficiente = coeficiente;
            _composicaoFreteCteRepository.Inserir(composicaoFreteCte);

            composicaoFreteCte = new ComposicaoFreteCte();
            composicaoFreteCte.Cte = _cte;
            composicaoFreteCte.CondicaoTarifa = "ZCAS";
            // composicaoFreteCte.Ferrovia = letraFerrovia;
            composicaoFreteCte.ValorComIcms = Math.Round(zcas, 2);
            composicaoFreteCte.ValorSemIcms = Math.Round(zcas, 2);
            composicaoFreteCte.ValorToneladaComIcms = Math.Round(zcas, 2);
            composicaoFreteCte.ValorToneladaSemIcms = Math.Round(zcas, 2);
            composicaoFreteCte.Coeficiente = coeficiente;
            _composicaoFreteCteRepository.Inserir(composicaoFreteCte);

            composicaoFreteCte = new ComposicaoFreteCte();
            composicaoFreteCte.Cte = _cte;
            composicaoFreteCte.CondicaoTarifa = "ZDCS";
            // composicaoFreteCte.Ferrovia = letraFerrovia;
            composicaoFreteCte.ValorComIcms = Math.Round(zdcs, 2);
            composicaoFreteCte.ValorSemIcms = Math.Round(zdcs, 2);
            composicaoFreteCte.ValorToneladaComIcms = Math.Round(zdcs, 2);
            composicaoFreteCte.ValorToneladaSemIcms = Math.Round(zdcs, 2);
            composicaoFreteCte.Coeficiente = coeficiente;
            _composicaoFreteCteRepository.Inserir(composicaoFreteCte);

            composicaoFreteCte = new ComposicaoFreteCte();
            composicaoFreteCte.Cte = _cte;
            composicaoFreteCte.CondicaoTarifa = "ZDCL";
            // composicaoFreteCte.Ferrovia = letraFerrovia;
            composicaoFreteCte.ValorComIcms = Math.Round(zdcl, 2);
            composicaoFreteCte.ValorSemIcms = Math.Round(zdcl, 2);
            composicaoFreteCte.ValorToneladaComIcms = Math.Round(zdcl, 2);
            composicaoFreteCte.ValorToneladaSemIcms = Math.Round(zdcl, 2);
            composicaoFreteCte.Coeficiente = coeficiente;
            _composicaoFreteCteRepository.Inserir(composicaoFreteCte);

            composicaoFreteCte = new ComposicaoFreteCte();
            composicaoFreteCte.Cte = _cte;
            composicaoFreteCte.CondicaoTarifa = "ZVGS";
            // composicaoFreteCte.Ferrovia = letraFerrovia;
            composicaoFreteCte.ValorComIcms = Math.Round(zvgs, 2);
            composicaoFreteCte.ValorSemIcms = Math.Round(zvgs, 2);
            composicaoFreteCte.ValorToneladaComIcms = Math.Round(zvgs, 2);
            composicaoFreteCte.ValorToneladaSemIcms = Math.Round(zvgs, 2);
            composicaoFreteCte.Coeficiente = coeficiente;
            _composicaoFreteCteRepository.Inserir(composicaoFreteCte);

            composicaoFreteCte = new ComposicaoFreteCte();
            composicaoFreteCte.Cte = _cte;
            composicaoFreteCte.CondicaoTarifa = "ZVGL";
            // composicaoFreteCte.Ferrovia = letraFerrovia;
            composicaoFreteCte.ValorComIcms = Math.Round(zvgl, 2);
            composicaoFreteCte.ValorSemIcms = Math.Round(zvgl, 2);
            composicaoFreteCte.ValorToneladaComIcms = Math.Round(zvgl, 2);
            composicaoFreteCte.ValorToneladaSemIcms = Math.Round(zvgl, 2);
            composicaoFreteCte.Coeficiente = coeficiente;
            _composicaoFreteCteRepository.Inserir(composicaoFreteCte);

            composicaoFreteCte = new ComposicaoFreteCte();
            composicaoFreteCte.Cte = _cte;
            composicaoFreteCte.CondicaoTarifa = "ZFCL";
            // composicaoFreteCte.Ferrovia = letraFerrovia;
            composicaoFreteCte.ValorComIcms = Math.Round(zfcl, 2);
            composicaoFreteCte.ValorSemIcms = Math.Round(zfcl, 2);
            composicaoFreteCte.ValorToneladaComIcms = Math.Round(zfcl, 2);
            composicaoFreteCte.ValorToneladaSemIcms = Math.Round(zfcl, 2);
            composicaoFreteCte.Coeficiente = coeficiente;
            _composicaoFreteCteRepository.Inserir(composicaoFreteCte);

            composicaoFreteCte = new ComposicaoFreteCte();
            composicaoFreteCte.Cte = _cte;
            composicaoFreteCte.CondicaoTarifa = "ZFCS";
            // composicaoFreteCte.Ferrovia = letraFerrovia;
            composicaoFreteCte.ValorComIcms = Math.Round(zfcs, 2);
            composicaoFreteCte.ValorSemIcms = Math.Round(zfcs, 2);
            composicaoFreteCte.ValorToneladaComIcms = Math.Round(zfcs, 2);
            composicaoFreteCte.ValorToneladaSemIcms = Math.Round(zfcs, 2);
            composicaoFreteCte.Coeficiente = coeficiente;
            _composicaoFreteCteRepository.Inserir(composicaoFreteCte);

            composicaoFreteCte = new ComposicaoFreteCte();
            composicaoFreteCte.Cte = _cte;
            composicaoFreteCte.CondicaoTarifa = "ZICL";
            // composicaoFreteCte.Ferrovia = letraFerrovia;
            composicaoFreteCte.ValorComIcms = Math.Round(zicl, 2);
            composicaoFreteCte.ValorSemIcms = Math.Round(zicl, 2);
            composicaoFreteCte.ValorToneladaComIcms = Math.Round(zicl, 2);
            composicaoFreteCte.ValorToneladaSemIcms = Math.Round(zicl, 2);
            composicaoFreteCte.Coeficiente = coeficiente;
            _composicaoFreteCteRepository.Inserir(composicaoFreteCte);

            composicaoFreteCte = new ComposicaoFreteCte();
            composicaoFreteCte.Cte = _cte;
            composicaoFreteCte.CondicaoTarifa = "ZICS";
            // composicaoFreteCte.Ferrovia = letraFerrovia;
            composicaoFreteCte.ValorComIcms = Math.Round(zics, 2);
            composicaoFreteCte.ValorSemIcms = Math.Round(zics, 2);
            composicaoFreteCte.ValorToneladaComIcms = Math.Round(zics, 2);
            composicaoFreteCte.ValorToneladaSemIcms = Math.Round(zics, 2);
            composicaoFreteCte.Coeficiente = coeficiente;
            _composicaoFreteCteRepository.Inserir(composicaoFreteCte);

            // Grava a lista de composi��o frete contrato
            IList<ComposicaoFreteContrato> listaComposicao = listaComposicaoFreteContrato.Where(g => g.CondicaoTarifa != "ZFRL" && g.CondicaoTarifa != "ZDSS" && g.CondicaoTarifa != "ZDSL").ToList();
            foreach (ComposicaoFreteContrato cfc in listaComposicao)
            {
                // Insere na composi��o frete cte
                composicaoFreteCte = new ComposicaoFreteCte();
                composicaoFreteCte.Cte = _cte;
                composicaoFreteCte.CondicaoTarifa = cfc.CondicaoTarifa;
                composicaoFreteCte.Ferrovia = cfc.Ferrovia;
                composicaoFreteCte.ValorComIcms = Math.Round(cfc.ValorComIcms.Value * _pesoParaCalculo, 2);
                composicaoFreteCte.ValorSemIcms = Math.Round(cfc.ValorSemIcms.Value * _pesoParaCalculo, 2);
                composicaoFreteCte.ValorToneladaComIcms = Math.Round(cfc.ValorComIcms.Value * _pesoParaCalculo, 2);
                composicaoFreteCte.ValorToneladaSemIcms = Math.Round(cfc.ValorSemIcms.Value * _pesoParaCalculo, 2);
                composicaoFreteCte.Coeficiente = coeficiente;
                _composicaoFreteCteRepository.Inserir(composicaoFreteCte);
            }

            // Insere na composi��o frete cte
            composicaoFreteCte = new ComposicaoFreteCte();
            composicaoFreteCte.Cte = _cte;
            composicaoFreteCte.CondicaoTarifa = "ICMI";
            composicaoFreteCte.ValorComIcms = Math.Round(valorCte, 2);
            composicaoFreteCte.ValorSemIcms = Math.Round(valorCte, 2);
            composicaoFreteCte.ValorToneladaComIcms = Math.Round(valorCte, 2);
            composicaoFreteCte.ValorToneladaSemIcms = Math.Round(valorCte, 2);
            composicaoFreteCte.Coeficiente = Math.Round(coeficiente, 6);
            _composicaoFreteCteRepository.Inserir(composicaoFreteCte);
        }

        /// <summary>
        /// Atualiza o valor calculado do cte da ferroeste
        /// </summary>
        /// <param name="valorCte">Valor do CTE da ferroeste (partilha)</param>
        /// <param name="valorIcms">Valor do icms do CTE da ferroeste (partilha)</param>
        private void AtualizarValorCteFerroeste(double valorCte, double valorIcms)
        {
            _cte.PesoParaCalculo = _pesoParaCalculo;
            _cte.BaseCalculoIcms = valorCte;
            _cte.ValorIcms = valorIcms;
            // _cte.ValorFrete = CalcularValorFrete();
            _cte.ValorFrete = valorCte;
            _cte.ValorSeguro = ObterValorSeguro();
            _cte.ValorTotalNf = CalcularValorTotalNotaFiscal();
            _cte.PesoTotalNf = CalcularPesoTotal();
            _cte.ValorCte = valorCte;
            _cte.DescontoPorPeso = (_contratoHistorico.DescontoPorPeso ?? 0.0) * _cte.PesoParaCalculo;

            _cte.ValorDescontoTonelada = CalcularDescontoPorTonelada();
            _cte.DescontoPorPercentual = CalcularDescontoPorPercentual();

            _cte.ValorReceber = (_cte.ValorCte - Math.Abs(_cte.ValorDescontoTonelada.Value)) - Math.Abs(_cte.DescontoPorPercentual.Value);

            if (_fluxoComercial.Mercadoria.CodigoMercadoriaSAP == "011")
            {
                _cte.ValorTotalMercadoria = this._configuracaoTranslogicRepository.ObterPorId(_chaveContainerVazioTipoFrigorifico) == null ? 0 : Convert.ToDouble(this._configuracaoTranslogicRepository.ObterPorId(_chaveContainerVazioTipoFrigorifico).Valor);
            }
            else
            {
                _cte.ValorTotalMercadoria = _listaCteDetalhe.Sum(g => g.ValorNotaFiscal);
            }

            _cte.Cfop = _cfop;

            // Arredondamento dos valores
            _cte.PesoParaCalculo = Math.Round(_cte.PesoParaCalculo.Value, 3);
            _cte.BaseCalculoIcms = Math.Round(valorCte, 2);
            _cte.ValorIcms = Math.Round(valorIcms, 2);
            _cte.ValorFrete = Math.Round(_cte.ValorFrete.Value, 2);
            _cte.ValorSeguro = Math.Round(_cte.ValorSeguro.Value, 2);
            _cte.ValorTotalNf = Math.Round(_cte.ValorTotalNf.Value, 2);
            _cte.PesoTotalNf = Math.Round(_cte.PesoTotalNf.Value, 3);
            _cte.ValorCte = Math.Round(valorCte, 2);
            _cte.DescontoPorPeso = Math.Round(_cte.DescontoPorPeso.Value, 2);
            _cte.DescontoPorPercentual = Math.Round(_cte.DescontoPorPercentual.Value, 2);
            _cte.ValorDescontoTonelada = Math.Round(_cte.ValorDescontoTonelada.Value, 2);
            _cte.ValorReceber = Math.Round(_cte.ValorReceber.Value, 2);
            _cte.ValorTotalMercadoria = Math.Round(_cte.ValorTotalMercadoria.Value, 2);
            // _cte.AmbienteSefaz = ObterAmbienteSefaz();

            _cteRepository.Atualizar(_cte);
        }

        /// <summary>
        /// Tabela Utilizada para informa��es do Cte Substituto.
        /// </summary>
        private bool ValidarInformacoesCteSubstituto()
        {
            try
            {
                return _cte.TipoCte == TipoCteEnum.Substituto;
            }
            catch (Exception ex)
            {
                LogHelper.Log(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
                throw;
            }
        }

        /// <summary>
        /// Tabela Utilizada para informa��es do Cte Substituto.
        /// </summary>
        private void GravarInformacoesCteSubstituto()
        {
            try
            {
                Cte38 cte38 = new Cte38();
                cte38.CfgUn = ObterIdentificadorFilial(); // Codigo interno da filial
                cte38.CfgDoc = Convert.ToInt32(_cte.Numero); // Numero do documento CT-e
                cte38.CfgSerie = int.Parse(_cte.Serie).ToString(); // Serie do documento CT-e

                CteArvore cteArvore = _cteArvoreRepository.ObterCteArvorePorCteFilho(_cte);

                // se o cte pai foi anulado com uma nota de anula�ao, deve preencher os dados da nota fiscal.
                if (cteArvore.CtePai.NfeAnulacao != null)
                {
                    cte38.TomaIcmsRefNfe = cteArvore.CtePai.NfeAnulacao.ChaveNfe;
                    //cte38.RefNfCnpj = cteArvore.CtePai.NfeAnulacao.CnpjEmitente;

                    //// cte38.RefNfCpf = cteArvore.CtePai.NfeAnulacao - Essa informa��o n�o tem no sistema.

                    //cte38.RefNfMod = cteArvore.CtePai.NfeAnulacao.ModeloNota;
                    //cte38.RefNfSerie = int.Parse(cteArvore.CtePai.NfeAnulacao.SerieNotaFiscal);
                    //cte38.RefNfSubSerie = int.Parse(cteArvore.CtePai.NfeAnulacao.SerieNotaFiscal);
                    //cte38.RefNfNro = cteArvore.CtePai.NfeAnulacao.NumeroNotaFiscal;
                    //cte38.RefNfValor = cteArvore.CtePai.NfeAnulacao.Valor;
                    //cte38.RefNfDemi = cteArvore.CtePai.NfeAnulacao.DataEmissao;
                }
                else
                {
                    // Pegar da arvore - chave cte pai. - o tomador emite a cte de anula��o
                    //cte38.InfCteSubChCte = cteArvore.CtePai.Chave;
                }

                cte38.InfCteSubChCte = cteArvore.CtePai.Chave;

                //// Preencher campos seguintes 
                //cte38.TomaIcmsRefCte = cteArvore.CtePai.Chave;

                if (_cte.TipoCte == TipoCteEnum.Substituto)
                {
                    var listCteAnulacao = _cteRepository.ObterCtesPorDespacho((int)_cte.Despacho.Id).Where(x => x.TipoCte == TipoCteEnum.Anulacao);
                    if (listCteAnulacao != null)
                    {
                        var cteAnulacao = listCteAnulacao.OrderByDescending(x => x.DataHora).FirstOrDefault(x => x.TipoCte == TipoCteEnum.Anulacao);
                        if (cteAnulacao != null)
                            cte38.TomaINaoCmsRefCteAnu = cteAnulacao.Chave;
                    }
                }
                //cte38.TomaINaoCmsRefCteAnu = _cte.Chave;

                //// se o cte foi marcado pra alterar tomador
                if (_cte.IndAltToma == 1)
                {
                    //// Pega a informa��o do tomador aqui.
                    var toma = GetUtilizaToma();
                    var utilizaToma3 = (toma >= 0);

                    if (utilizaToma3) cte38.InfCteSubIndAlteraToma = toma;

                    //// Se for conteiner ou brado, coloca tomador como 4.
                    if (!utilizaToma3)
                    {
                        cte38.InfCteSubIndAlteraToma = 4;
                    }
                }

                _cte38Repository.Inserir(cte38);
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(_cte, "GravarConfigCteEnvioService", string.Format("GravarInformacoesCteSubstituto: {0}", ex.Message), ex);
                LogHelper.Log(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
                throw;
            }
        }

        /// <summary>
        ///  Valida a grava��o dos dados na tabela ICTE60_FERROV. 
        ///   * CTE de anula��o n�o pode gerar informa��es de modal ferrovi�rio. Se for o caso, retorna falso.
        /// </summary>
        private bool ValidarInformacoesModalFerroviario()
        {
            try
            {
                if (_cte.TipoCte == TipoCteEnum.Anulacao)
                {
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                LogHelper.Log(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
                throw;
            }
        }

        /// <summary>
        /// Tabela utilizada para Informa��es do modal Ferrovi�rio (1 - 1) - ICTE60_FERROV
        /// </summary>
        private void GravarInformacoesModalFerroviario()
        {
            try
            {
                // 1 Ferrovia de origem e 2 ferrovia destino
                // est� na Fluxo comercial
                // letra tem origem de quem fatura
                // qual a ferrovia de origem e destino ( pela letra consegue ver qual �)

                double valorFrete = _cte.ValorFrete ?? 0.0;

                Cte60 cte60 = new Cte60();
                cte60.CfgUn = ObterIdentificadorFilial(); // Codigo interno da filial
                cte60.CfgDoc = Convert.ToInt32(_cte.Numero); // Numero do documento CT-e
                cte60.CfgSerie = int.Parse(_cte.Serie).ToString(); // Serie do documento CT-e

                // Tipo de trafego
                if (_fluxoComercial.TipoTrafegoFluxo != null)
                {
                    if (_fluxoComercial.TipoTrafegoFluxo == TipoTrafegoFluxoEnum.Proprio)
                    {
                        cte60.FerrovTpTraf = 0; // Tipo de Tr�fego (Proprio)
                    }
                    else if (_fluxoComercial.TipoTrafegoFluxo == TipoTrafegoFluxoEnum.Mutuo)
                    {
                        cte60.FerrovTpTraf = 1; // Tipo de Tr�fego (Mutuo)
                    }
                }
                else
                {
                    if (_tipoTrafegoMutuo == TipoTrafegoMutuoEnum.FaturamentoEmpresaGrupo)
                    {
                        cte60.FerrovTpTraf = 0; // Tipo de Tr�fego (Proprio)
                    }
                    else if (_tipoTrafegoMutuo == TipoTrafegoMutuoEnum.FaturamentoEmpresaForaGrupo)
                    {
                        cte60.FerrovTpTraf = 0; // Tipo de Tr�fego (Proprio)
                    }
                    else
                    {
                        cte60.FerrovTpTraf = 1; // Tipo de Tr�fego (Mutuo)
                    }
                }

                // Responsavel pelo faturamento
                if (_tipoTrafegoMutuo == TipoTrafegoMutuoEnum.FaturamentoEmpresaGrupo)
                {
                    cte60.TrafMutRespFat = 1; // Respons�vel pelo Faturamento 1 Ferrovia de origem e 2 ferrovia destino
                }
                else if (_tipoTrafegoMutuo == TipoTrafegoMutuoEnum.FaturamentoEmpresaForaGrupo)
                {
                    cte60.TrafMutRespFat = 2; // Respons�vel pelo Faturamento 1 Ferrovia de origem e 2 ferrovia destino
                }
                else if (_tipoTrafegoMutuo == TipoTrafegoMutuoEnum.IntercambioFaturamentoOrigem)
                {
                    cte60.TrafMutRespFat = 1; // Respons�vel pelo Faturamento 1 Ferrovia de origem e 2 ferrovia destino
                }
                else if (_tipoTrafegoMutuo == TipoTrafegoMutuoEnum.IntercambioFaturamentoDestino)
                {
                    cte60.TrafMutRespFat = 2; // Respons�vel pelo Faturamento 1 Ferrovia de origem e 2 ferrovia destino
                }
                else if (_tipoTrafegoMutuo == TipoTrafegoMutuoEnum.FaturamentoArgentina)
                {
                    cte60.TrafMutRespFat = 1; // Respons�vel pelo Faturamento 1 Ferrovia de origem e 2 ferrovia destino
                }

                cte60.TrafMutFerrEmi = 1; // Ferrovia Emitente do CTe  1 Ferrovia de origem e 2 ferrovia destino
                cte60.FerrovFluxo = _fluxoComercial.Codigo; // Fluxo Ferrovi�rio

                //// Removida a tag na vers�o 3.0 - 07/04/2017 - comentado para 2.0 tb - 26/05/2017 - como essa informa��o nunca foi informada, 
                ///  o coment�rio foi mantido.
                ////cte60.FerrovIdTrem = null; // Identifica��o do trem. Campo opcional.

                cte60.FerrovValFrete = valorFrete; // Valor da participa��o do frete de outra ferrovia

                _cte60Repository.Inserir(cte60);
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(_cte, "GravarConfigCteEnvioService", string.Format("GravarInformacoesModalFerroviario: {0}", ex.Message), ex);
                LogHelper.Log(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
                throw;
            }
        }

        /// <summary>
        /// Tabela utilizada para Informa��es das Ferrovias Envolvidas (0 - n) - ICTE62_FERROENV
        /// </summary>
        private void GravarInformacoesFerroviasEnvolvidasOld()
        {
            try
            {
                IEmpresa empresaFerrovia;
                int index = 0;

                EstacaoMae origem = _fluxoComercial.OrigemIntercambio ?? _fluxoComercial.Origem;
                EstacaoMae destino = _fluxoComercial.DestinoIntercambio ?? _fluxoComercial.Destino;
                Rota rota = _rotaRepository.ObterPorTrechoMenorDistancia(origem.Codigo, destino.Codigo);
                IList<EmpresasEnvolvidasDto> empresasEnvolvidas = _rotaRepository.ObterFerroviasEnvolvidasPorIdRota(rota.Id);

                if ((_fluxoComercial.TipoTrafegoFluxo != null) && (_fluxoComercial.TipoTrafegoFluxo == TipoTrafegoFluxoEnum.Proprio))
                {
                    return;
                }

                // Obter a sigla da empresa que fatura
                string siglaFerrovia = ALL.Core.Util.Enum<FerroviaFaturamentoEnum>.GetDescriptionOf(_contrato.FerroviaFaturamento.Value);

                IEmpresa ferrovia = ObterEmpresaFerrovia(siglaFerrovia);

                if (ferrovia == null)
                {
                    return;
                }

                if (ferrovia.IndEmpresaALL == false)
                {
                    var listaEnvolvidas = empresasEnvolvidas.Select(g => g.IdEmpresa).Distinct().ToList();

                    // percorrer no agrupamento)
                    foreach (var empresa in listaEnvolvidas)
                    {
                        empresaFerrovia = ObterEmpresaFerrovia((int)empresa);

                        if (empresaFerrovia != null)
                        {
                            if (!VerificaEmpresaPortoFer(empresaFerrovia))
                            {
                                // Adiciona os dados da ferrovia envolvida
                                AdicionarEmpresaEnvolvida(empresaFerrovia, index);

                                index++;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(_cte, "GravarConfigCteEnvioService", string.Format("GravarInformacoesFerroviasEnvolvidas: {0}", ex.Message), ex);
                throw;
            }
        }

        /// <summary>
        /// Tabela utilizada para Informa��es das Ferrovias Envolvidas (0 - n) - ICTE62_FERROENV 
        /// </summary>
        private void GravarInformacoesFerroviasEnvolvidas()
        {
            try
            {
                IEmpresa empresaFerrovia = null;
                int index = 0;

                if ((_fluxoComercial.TipoTrafegoFluxo != null) && (_fluxoComercial.TipoTrafegoFluxo == TipoTrafegoFluxoEnum.Proprio))
                {
                    return;
                }

                // Obter a sigla da empresa que fatura
                string siglaFerrovia = ALL.Core.Util.Enum<FerroviaFaturamentoEnum>.GetDescriptionOf(_contrato.FerroviaFaturamento.Value);

                IEmpresa ferrovia = ObterEmpresaFerrovia(siglaFerrovia);

                if (ferrovia == null)
                {
                    return;
                }

                // Caso a empresa ferrovia 
                if (ferrovia.IndEmpresaALL == false)
                {
                    IList<ComposicaoFreteContrato> listaFreteContrato =
                            _composicaoFreteContratoRepository.ObterPorNumeroContrato(_contratoHistorico.NumeroContrato);

                    var listaFerroviasEnvolvidas = listaFreteContrato.Select(g => g.Ferrovia).Distinct().ToList();

                    foreach (string letraFerroviaEnvolvida in listaFerroviasEnvolvidas)
                    {
                        empresaFerrovia = _empresaFerroviaRepository.ObterPorSigla(letraFerroviaEnvolvida);

                        if (empresaFerrovia != null)
                        {
                            if (!VerificaEmpresaPortoFer(empresaFerrovia))
                            {
                                // Adiciona os dados da ferrovia envolvida
                                AdicionarEmpresaEnvolvida(empresaFerrovia, index);

                                index++;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(_cte, "GravarConfigCteEnvioService", string.Format("GravarInformacoesFerroviasEnvolvidas: {0}", ex.Message), ex);
                LogHelper.Log(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
                throw;
            }
        }

        /// <summary>
        /// Verifica se a Empresa � da portofer
        /// </summary>
        /// <param name="empresa">Empresa a ser verificada</param>
        /// <returns>Valor booleano</returns>
        private bool VerificaEmpresaPortoFer(IEmpresa empresa)
        {
            return empresa.NomeFantasia.StartsWith("P") && (empresa.Id.Equals(68957) || empresa.Id.Equals(68588));
        }

        /// <summary>
        /// Adiciona os dados da empresa envolvida na tabela - ICTE62_FERROENV 
        /// </summary>
        /// <param name="empresa">Empresa envolvida</param>
        /// <param name="idFerrov"> Numero sequencial da tabela</param>
        private void AdicionarEmpresaEnvolvida(IEmpresa empresa, int idFerrov)
        {
            Cte62 cte62;

            try
            {
                cte62 = new Cte62();
                cte62.CfgUn = ObterIdentificadorFilial(); // Codigo interno da filial
                cte62.CfgDoc = Convert.ToInt32(_cte.Numero); // Numero do documento CT-e
                cte62.CfgSerie = int.Parse(_cte.Serie).ToString(); // Serie do documento CT-e
                cte62.IdFerroEnv = idFerrov; // Numero sequencial da tabela

                cte62.FerroEnvCnpj = empresa.Cgc;

                cte62.FerroEnvCint = empresa.Sigla; // C�digo interno da ferrovia . Campo opcional.
                if (!string.IsNullOrEmpty(empresa.InscricaoEstadual))
                {
                    cte62.FerroEnvIe = Convert.ToInt64(Tools.TruncateRemoveSpecialChar(empresa.InscricaoEstadual, 14)); // Inscri��o estadual. Campo opcional.	
                }
                else
                {
                    cte62.FerroEnvIe = 0;
                }

                if (!String.IsNullOrEmpty(empresa.NomeFantasia))
                {
                    cte62.FerroEnvXnome = empresa.NomeFantasia.Trim(); // Nome da ferrovia. Campo opcional.
                }

                if (!string.IsNullOrEmpty(empresa.Endereco))
                {
                    cte62.EnderFerroXlgr = empresa.Endereco.Trim(); // Logradouro .Campo obrigat�rio.	
                }

                // cte62.EnderFerroNro = string.Empty; // N�mero. Campo obrigat�rio.
                cte62.EnderFerroNro = "0"; // N�mero. Campo obrigat�rio.
                cte62.EnderFerroXcpl = string.Empty; // Complemento do endere�o. Campo opcional
                cte62.EnderFerroXbairro = "NAO INFORMADO"; // Bairro do endere�o . Campo obrigat�rio.
                if (empresa.CidadeIbge != null)
                {
                    cte62.EnderFerroCMun = empresa.CidadeIbge.CodigoIbge; // C�digo IBGE do munic�pio. Campo obrigat�rio.
                    if (empresa.CidadeIbge.CodigoIbge == 9999999)
                    {
                        cte62.EnderFerroXmun = "EXTERIOR";
                        cte62.EnderFerroUf = "EX";
                    }
                    else
                    {
                        cte62.EnderFerroXmun = empresa.CidadeIbge.Descricao; // Nome do munic�pio. Campo obrigat�rio.
                        cte62.EnderFerroUf = empresa.CidadeIbge.SiglaEstado; // C�digo UF. Campo obrigat�rio.
                    }
                }
                else
                {
                    cte62.EnderFerroCMun = null;
                    cte62.EnderFerroXmun = string.Empty;
                }

                try
                {
                    int index = empresa.Cep.IndexOf('-');
                    cte62.EnderFerroCep = index > -1 ? int.Parse(empresa.Cep.Remove(index, 1)) : int.Parse(empresa.Cep); // CEP. Campo opcional.
                    if (cte62.EnderFerroCep > 99999999)
                    {
                        // Garante o tamanho maximo do cep em 8 casas
                        cte62.EnderFerroCep = null;
                    }
                }
                catch (Exception ex)
                {
                    cte62.EnderFerroCep = null;
                }

                _cte62Repository.Inserir(cte62);
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(_cte, "GravarConfigCteEnvioService", string.Format("AdicionarEmpresaEnvolvida: {0}", ex.Message), ex);
                throw;
            }
        }

        /// <summary>
        /// Informa��es da DCL (0 - n) - ICTE63_FERRO_DCL 
        /// </summary>
        private bool ValidarInformacoesDCL()
        {
            try
            {
                if (_cte.Versao.CodigoVersao.Equals("2.00") || _cte.Versao.CodigoVersao.Equals("3.00"))
                {
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                LogHelper.Log(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
                throw;
            }
        }

        /// <summary>
        /// Informa��es da DCL (0 - n) - ICTE63_FERRO_DCL 
        /// </summary>
        private void GravarInformacoesDCL()
        {
            try
            {
                Cte63 cte63 = new Cte63();
                int numConvertido;

                // Recupera os valores para calculos
                double baseCalculoIcms = _cte.BaseCalculoIcms ?? 0.0;
                double somaPeso = _cte.PesoTotalNf ?? 0.0;
                double valorSevicosAcessorios = 0.0;
                double valorFrete = _cte.ValorFrete ?? 0.0;
                double valorTotalServicos = valorFrete + valorSevicosAcessorios;

                // Codigo interno da filial (PK)
                cte63.CfgUn = ObterIdentificadorFilial();

                // Numero do documento CT-e (PK)
                int.TryParse(_cte.Numero, out numConvertido);
                cte63.CfgDoc = numConvertido;

                // Serie do documento CT-e (PK)
                // cte63.CfgSerie = _cte.Serie;
                cte63.CfgSerie = int.Parse(_cte.Serie).ToString();

                // Numero sequencial da tabela (PK)
                cte63.IdDcl = 1;

                // S�rie do DCL
                cte63.DclSerie = _despachoTranslogic.NumeroSerieDespacho.ToString();

                // N�mero do DCL
                cte63.DclNdcl = _despachoTranslogic.NumeroDespacho;

                // Data de emiss�o
                cte63.DclDemi = _despachoTranslogic.DataDespacho;

                // Quantidade de vag�es
                // cte63.DclQvag = _carregamento.NumeroVagoesCarregados;

                // Peso para c�lculo em Toneladas
                cte63.DclPcalc = somaPeso;

                // Valor da tarifa
                cte63.DclVtar = baseCalculoIcms;

                // Valor do frete
                cte63.DclVfrete = valorFrete;

                // Valor dos servi�os acess�rios
                cte63.DclVsacess = valorSevicosAcessorios;

                // Valor total do servi�o
                cte63.DclVtServ = valorTotalServicos;

                // Identifica��o do trem
                cte63.DclIdTrem = null;

                _cte63Repository.Inserir(cte63);
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(_cte, "GravarConfigCteEnvioService", string.Format("GravarInformacoesDCL: {0}", ex.Message), ex);
                LogHelper.Log(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
                throw;
            }
        }

        private void RegistrarLogDetalhado(string msg)
        {
            _cteLogService.InserirLogInfo(_cte, "GravarConfigCteEnvioService", string.Format("{0} ({1} - {2})", msg, DateTime.Now, Dns.GetHostName()));
        }

        /// <summary>
        /// Informa��es de detalhes dos Vag�es (1 - n) - ICTE64_FERRO_DCLDETVAG 
        /// </summary>
        private bool ValidarInformacoesDclDetVag()
        {
            try
            {
                if (_cte.Versao.CodigoVersao.Equals("2.00") || _cte.Versao.CodigoVersao.Equals("3.00"))
                {
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                LogHelper.Log(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
                throw;
            }
        }

        /// <summary>
        /// Informa��es de detalhes dos Vag�es (1 - n) - ICTE64_FERRO_DCLDETVAG 
        /// </summary>
        private void GravarInformacoesDclDetVag()
        {
            try
            {
                Cte64 cte64 = new Cte64();
                int numConvertido;

                // Codigo interno da filial (PK)
                cte64.CfgUn = ObterIdentificadorFilial();

                // Numero do documento CT-e (PK)
                int.TryParse(_cte.Numero, out numConvertido);
                cte64.CfgDoc = numConvertido;

                // Serie do documento CT-e (PK)
                cte64.CfgSerie = int.Parse(_cte.Serie).ToString();

                // Numero sequencial da tabela (PK)
                cte64.IdDcl = 1;

                // Numero sequencial da tabela (PK)
                cte64.IdDetVag = 1;

                // N�mero de identifica��o do vag�o
                cte64.DclDetVagNvag = int.Parse(_vagao.Codigo);

                // Capacidade em toneladas do vag�o
                cte64.DclDetVagCap = null;

                // Tipo de Vag�o
                // cte64.DclDetVagTpVag = _tipoVagao.Codigo;
                cte64.DclDetVagTpVag = _serieVagao.Codigo.Trim();

                // Peso real do vag�o
                cte64.DclDetVagPesoReal = Math.Round(_cte.PesoVagao ?? 0.0, 2);

                // Peso para c�lculo
                cte64.DclDetVagPesoBc = Math.Round(_cte.PesoParaCalculo ?? 0.0, 2);

                _cte64Repository.Inserir(cte64);
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(_cte, "GravarConfigCteEnvioService", string.Format("GravarInformacoesDclDetVag: {0}", ex.Message), ex);
                LogHelper.Log(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
                throw;
            }
        }

        /// <summary>
        /// informa��es dos containeres contidos no vag�o com DCL (0 - n) - ICTE66_FERRO_DCL_CONTVAG 
        /// </summary>
        private bool ValidarInformacoesContVag()
        {
            try
            {
                if (_cte.Versao.CodigoVersao.Equals("2.00") || _cte.Versao.CodigoVersao.Equals("3.00"))
                {
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                LogHelper.Log(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
                throw;
            }
        }

        /// <summary>
        /// informa��es dos containeres contidos no vag�o com DCL (0 - n) - ICTE66_FERRO_DCL_CONTVAG 
        /// </summary>
        private void GravarInformacoesContVag()
        {
            try
            {
                int index = 0;

                IList<CteDetalhe> listaCteDetalhe = _cteDetalheRepository.ObterPorCte(_cte);
                if (listaCteDetalhe.Count > 0)
                {
                    // Teste para verificar se � um CTE de conteiner
                    if (!string.IsNullOrEmpty(listaCteDetalhe[0].ConteinerNotaFiscal))
                    {
                        List<string> listaConteiner = listaCteDetalhe.GroupBy(g => g.ConteinerNotaFiscal).Select(s => s.Key).ToList();
                        foreach (string identificadorContainer in listaConteiner)
                        {
                            Cte66 cte66 = new Cte66();
                            cte66.CfgUn = ObterIdentificadorFilial(); // Codigo interno da filial
                            cte66.CfgDoc = Convert.ToInt32(_cte.Numero); // Numero do documento CT-e
                            cte66.CfgSerie = int.Parse(_cte.Serie).ToString();  // Serie do documento CT-e
                            cte66.IdDcl = 1; // Numero sequencial da tabela
                            cte66.IdDetVag = 1; // Numero sequencial da tabela
                            cte66.IdContVag = ++index; // Numero sequencial da tabela
                            cte66.ContVagNCont = identificadorContainer;
                            // Data prevista de entrega
                            cte66.ContVagDprev = null;

                            _cte66Repository.Inserir(cte66);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(_cte, "GravarInformacoesDclDetVag", string.Format("GravarInformacoesContVag: {0}", ex.Message), ex);
                LogHelper.Log(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
                throw;
            }
        }

        /// <summary>
        ///  Valida a grava��o das informa��es de detalhes dos vag�es na tabela ICTE67_FERRO_DETVAG.
        ///  Na vers�o 3.0 do cte, essas informa��es n�o devem ser mais geradas e enviadas para config.
        /// </summary>
        private bool ValidarInformacoesDetVag()
        {
            try
            {
                if (_cte.Versao.CodigoVersao.Equals("3.00"))
                {
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                LogHelper.Log(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
                throw;
            }
        }

        /// <summary>
        ///   Informa��es de detalhes dos Vag�es (1 - n) - ICTE67_FERRO_DETVAG 
        ///   OBS: Na vers�o 3.0, essa informa��o n�o deve ser integrada na config.
        /// </summary>
        private void GravarInformacoesDetVag()
        {
            try
            {
                Cte67 cte67 = new Cte67();
                int numConvertido;

                // Codigo interno da filial (PK)
                cte67.CfgUn = ObterIdentificadorFilial();

                // Numero do documento CT-e (PK)
                int.TryParse(_cte.Numero, out numConvertido);
                cte67.CfgDoc = numConvertido;

                // Serie do documento CT-e (PK)
                // cte67.CfgSerie = _cte.Serie;
                cte67.CfgSerie = int.Parse(_cte.Serie).ToString();

                // Numero sequencial da tabela (PK)
                // Fixo, pois � gerado 1 cte para cada vag�o
                cte67.IdDetVag = 1;

                // N�mero de identifica��o do vag�o
                cte67.DetVagNvag = int.Parse(_vagao.Codigo);

                // Capacidade em toneladas do vag�o
                cte67.DetVagCap = null;

                // Tipo de Vag�o
                // cte67.DetVagTpVag = _tipoVagao.Codigo;
                cte67.DetVagTpVag = _serieVagao.Codigo.Trim();

                // Peso real do vag�o
                cte67.DetVagPesoReal = _cte.PesoVagao;

                // Peso para c�lculo
                // cte67.DetVagPesoBc = _cte.PesoVagao;
                cte67.DetVagPesoBc = _cte.PesoParaCalculo;

                _cte67Repository.Inserir(cte67);
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(_cte, "GravarConfigCteEnvioService", string.Format("GravarInformacoesDetVag: {0}", ex.Message), ex);
                LogHelper.Log(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
                throw;
            }
        }

        /// <summary>
        /// informa��es de detalhes dos Vag�es (1 - n) - ICTE67_FERRO_DETVAG 
        /// OBS: Na vers�o 3.0, essa informa��o n�o deve ser integrada na config.
        /// </summary>
        private bool ValidarInformacoeContainerContidoVagao()
        {
            try
            {
                if (_cte.Versao.CodigoVersao.Equals("2.00") || _cte.Versao.CodigoVersao.Equals("3.00"))
                {
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                LogHelper.Log(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
                throw;
            }
        }

        /// <summary>
        /// Tabela utilizada para Informa��es dos containeres contidos no vag�o (0 - n) - ICTE69_FERRO_CONTVAG
        /// </summary>
        private void GravarInformacoeContainerContidoVagao()
        {
            try
            {
                int index = 0;

                IList<CteDetalhe> listaCteDetalhe = _listaCteDetalhe;
                if (listaCteDetalhe.Count > 0)
                {
                    // Teste para verificar se � um CTE de conteiner
                    if (!string.IsNullOrEmpty(listaCteDetalhe[0].ConteinerNotaFiscal))
                    {
                        List<string> listaConteiner = listaCteDetalhe.GroupBy(g => g.ConteinerNotaFiscal).Select(s => s.Key).ToList();

                        foreach (string identificadorContainer in listaConteiner)
                        {
                            if (!String.IsNullOrEmpty(identificadorContainer))
                            {
                                Cte69 cte69 = new Cte69();
                                cte69.CfgUn = ObterIdentificadorFilial(); // Codigo interno da filial
                                cte69.CfgDoc = Convert.ToInt32(_cte.Numero); // Numero do documento CT-e
                                // cte69.CfgSerie = _cte.Serie; 
                                cte69.CfgSerie = int.Parse(_cte.Serie).ToString();  // Serie do documento CT-e
                                cte69.IdDetVag = 1; // Numero sequencial da tabela
                                cte69.IdContVag = ++index; // Numero sequencial da tabela
                                cte69.ContVagNcont = identificadorContainer;  // Identifica��o do Container
                                cte69.ContVagDprev = null; // Data prevista da entrega

                                _cte69Repository.Inserir(cte69);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(_cte, "GravarConfigCteEnvioService", string.Format("GravarInformacoeContainerContidoVagao: {0}", ex.Message), ex);
                LogHelper.Log(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
                throw;
            }
        }

        /// <summary>
        /// Tabela utilizada para Rateio dos vag�es (0 - n) - ICTE70_FERRO_RATVAG 
        /// </summary>
        private bool ValidarRateioVagao()
        {
            try
            {
                if (_cte.Versao.CodigoVersao.Equals("2.00") || _cte.Versao.CodigoVersao.Equals("3.00"))
                {
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                LogHelper.Log(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
                throw;
            }
        }

        /// <summary>
        /// Tabela utilizada para Rateio dos vag�es (0 - n) - ICTE70_FERRO_RATVAG 
        /// </summary>
        private void GravarRateioVagao()
        {
            try
            {
                int index = 0;
                Cte70 cte70;
                if (_listaCteDetalhe.Count > 0)
                {
                    foreach (CteDetalhe cteDetalhe in _listaCteDetalhe)
                    {
                        cte70 = new Cte70();

                        cte70.CfgUn = ObterIdentificadorFilial(); // Codigo interno da filial
                        cte70.CfgDoc = Convert.ToInt32(_cte.Numero); // Numero do documento CT-e
                        // cte70.CfgSerie = _cte.Serie; 
                        cte70.CfgSerie = int.Parse(_cte.Serie).ToString();  // Serie do documento CT-e
                        cte70.IdDetVag = 1; // Numero sequencial da tabela
                        cte70.IdRatVag = index++; // Numero sequencial da tabela

                        if (string.IsNullOrEmpty(cteDetalhe.ChaveNfe))
                        {
                            cte70.RatNfPesoRat = cteDetalhe.PesoNotaFiscal; // Peso rateado
                            cte70.RatNfSerie = Tools.TruncateRemoveSpecialChar(cteDetalhe.SerieNota, 3); // S�rie
                            cte70.RatNfNdoc = cteDetalhe.NumeroNota.Trim(); // N�mero
                            // cte70.RatNfeChave = string.Empty; // Chave de acesso da NF-e
                            // cte70.RatNfePesoRat = 0; // Peso rateado
                        }
                        else
                        {
                            // cte70.RatNfPesoRat = 0; // Peso rateado

                            int intAux;
                            int.TryParse(_cte.Numero, out intAux);
                            cte70.RatNfeChave = cteDetalhe.ChaveNfe; // Chave de acesso da NF-e
                            cte70.RatNfePesoRat = cteDetalhe.PesoNotaFiscal; // Peso rateado
                        }

                        _cte70Repository.Inserir(cte70);
                    }
                }
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(_cte, "GravarConfigCteEnvioService", string.Format("GravarRateioVagao: {0}", ex.Message), ex);
                LogHelper.Log(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
                throw;
            }
        }

        private void LimparDados()
        {
            _valorImpostoRetidoSubstTributaria = 0.0;
            _despachoTranslogic = null;
            _serieDespachoUf = null;
            _fluxoComercial = null;
            _empresaRemetenteFluxoComercial = null;
            _empresaDestinatariaFluxoComercial = null;
            _cteRaiz = null;
            _cteRaizAgrupamento = null;
            _itemDespacho = null;
            _contratoHistorico = null;
            _contrato = null;
            _vagao = null;
            _serieVagao = null;
            _tipoVagao = null;
            _empresaProprietariaVagao = null;
            _empresaPagadoraContrato = null;
            _cidadeIbgeEmpresaPagadoraContrato = null;
            _empresaRemetenteContrato = null;
            _cidadeIbgeEmpresaRemetenteContrato = null;
            _empresaDestinatariaContrato = null;
            _cidadeIbgeEmpresaDestinatariaContrato = null;
            _listaCteDetalhe = null;
            _estacaoMaeOrigemFluxoComercial = null;
            _estacaoMaeDestinoFluxoComercial = null;
            _municipioEstacaoMaeOrigemFluxoComercial = null;
            _municipioEstacaoMaeDestinoFluxoComercial = null;
            _cidadeIbgeEstacaoMaeOrigemFluxoComercial = null;
            _cidadeIbgeEstacaoMaeDestinoFluxoComercial = null;
            _empresaOperadoraEstacaoMaeOrigemFluxoComercial = null;
            _nfe03Filial = null;
            _empresaContratoDestinatariaFiscal = null;
            _empresaContratoRemetenteFiscal = null;
            _cidadeIbgeRemetenteFiscal = null;
            _cidadeIbgeDestinatariaFiscal = null;
            _empresaFerroviaCliente = null;
            _tipoTrafegoMutuo = TipoTrafegoMutuoEnum.Indefinido;
            _cidadeIbgeEmpresaFerrovia = null;
            _cfop = string.Empty;
            _cfopDescricao = string.Empty;
            _bufferMensagem = null;
        }

        private DateTime GetDhEmissao()
        {
            var restricoes = new[] { "MT", "MS" };

            if (restricoes.Contains(_cte.SiglaUfFerrovia))
            {
                return _cte.DataHora.AddHours(-1);
            }

            return _cte.DataHora;
        }

        private void CarregarDados()
        {
            LimparDados();

            try
            {
                // Cria o buffer de mensagens
                _bufferMensagem = new StringBuilder();

                if (_cte != null)
                {
                    // Contrato hist�rico
                    _contratoHistorico = _cte.ContratoHistorico;

                    // Informa��es do fluxo comercial
                    _fluxoComercial = _cte.FluxoComercial;

                    // Contrato
                    _contrato = _fluxoComercial.Contrato;

                    // Dados do Vag�o
                    _vagao = _cte.Vagao;

                    _despachoTranslogic = _despachoTranslogicRepository.ObterPorIdSemEstado(_cte.Despacho.Id ?? 0);

                    // obt�m o registro da cteAnulado, para caso de ser um cte anula��o.
                    _cteAnulado = _cteAnuladoRepository.ObterPorIdCteAnulacao(_cte.Id ?? 0);

                    // Lista de detalhes do Cte
                    if (_cteAnulado != null)
                    {
                        _listaCteDetalhe = _cteDetalheRepository.ObterPorCte(_cteAnulado.Cte);
                    }
                    else
                        if ((_cte.ListaDetalhes != null) && (_cte.ListaDetalhes.Count > 0))
                    {
                        _listaCteDetalhe = _cteDetalheRepository.ObterPorCte(_cte);
                    }
                }

                // Carregar os dados do cte raiz
                CteArvore cteArvore = _cteArvoreRepository.ObterCteArvorePorCteFilho(_cte);
                _cteRaiz = cteArvore.CteRaiz;

                if (_cte.TipoCte == TipoCteEnum.Virtual)
                {
                    _contrato = _contratoHistorico;
                }

                CteAgrupamento cteAgrupamento = _cteAgrupamentoRepository.ObterAgrupamentoRaizPorCteFilho(_cte);
                _cteRaizAgrupamento = cteAgrupamento.CteRaiz;

                // Carregar os dados do Item de Despacho
                _itemDespacho = _itemDespachoRepository.ObterItemDespachoPorIdDespachoSemEstado(_despachoTranslogic.Id ?? 0);

                // Caso seja cte de manuten��o, pega o peso para calculo do proprio cte
                if (_cte.Manutencao)
                {
                    // Verifica se � um fluxo de conteiner ou se a mercadoria est� condicionada em conteiner ou se a empresa � BRADO
                    // Se a empresa � brado, e o fluxo � de rateio de CTE, pega como base o peso do cte, gerado no despacho, a partir do peso das notas.
                    var indRateio = _fluxoComercial.IndRateioCte ?? false;
                    if (VerificaEmpresaBrado(_contrato.EmpresaPagadora) && (indRateio))
                    {
                        ////_pesoParaCalculo = _itemDespacho.PesoCalculo ?? 0;

                        // Caso conteiner
                        IList<CteDetalhe> listaCteDetalhe = _cteDetalheRepository.ObterPorCte(_cte);
                        List<string> listaConteiner = listaCteDetalhe.GroupBy(g => g.ConteinerNotaFiscal).Select(s => s.Key).ToList();
                        _pesoParaCalculo = listaConteiner.Count;
                    }
                    else
                        if (_carregamentoService.VerificarFluxoConteiner(_fluxoComercial) || VerificaEmpresaBrado(_contrato.EmpresaPagadora))
                    {
                        // Caso conteiner
                        IList<CteDetalhe> listaCteDetalhe = _cteDetalheRepository.ObterPorCte(_cte);
                        List<string> listaConteiner = listaCteDetalhe.GroupBy(g => g.ConteinerNotaFiscal).Select(s => s.Key).ToList();
                        _pesoParaCalculo = listaConteiner.Count;
                    }
                    else
                    {
                        switch (_fluxoComercial.UnidadeMedida.Id)
                        {
                            case "M3":
                                _pesoParaCalculo = _cte.VolumeVagao;
                                break;
                            default:
                                _pesoParaCalculo = _cte.PesoVagao ?? 0;
                                break;
                        }
                    }
                }
                else
                {
                    // Se a empresa � brado, e o fluxo � de rateio de CTE, pega como base o peso do cte, gerado no despacho, a partir do peso das notas.
                    var indRateio = _fluxoComercial.IndRateioCte ?? false;
                    if (VerificaEmpresaBrado(_contrato.EmpresaPagadora) && (indRateio))
                    {
                        ////_pesoParaCalculo = _itemDespacho.PesoCalculo ?? 0;

                        // Caso conteiner
                        IList<CteDetalhe> listaCteDetalhe = _cteDetalheRepository.ObterPorCte(_cte);
                        List<string> listaConteiner = listaCteDetalhe.GroupBy(g => g.ConteinerNotaFiscal).Select(s => s.Key).ToList();
                        _pesoParaCalculo = listaConteiner.Count;
                    }
                    else

                        // Verifica se � um fluxo de conteiner ou se a mercadoria est� condicionada em conteiner ou se a empresa � BRADO
                        if (_carregamentoService.VerificarFluxoConteiner(_fluxoComercial) || VerificaEmpresaBrado(_contrato.EmpresaPagadora))
                    {
                        // Caso conteiner
                        //IList<CteDetalhe> listaCteDetalhe = _cteDetalheRepository.ObterPorCte(_cte);
                        List<string> listaConteiner = _listaCteDetalhe.GroupBy(g => g.ConteinerNotaFiscal).Select(s => s.Key).ToList();
                        _pesoParaCalculo = listaConteiner.Count;
                    }
                    else
                    {
                        _pesoParaCalculo = _itemDespacho.PesoCalculo ?? 0;
                    }
                }

                if (_contrato != null)
                {
                    // Dados da empresa pagadora
                    _empresaPagadoraContrato = _contrato.EmpresaPagadora;

                    // Dados da empresa remetente
                    _empresaRemetenteContrato = _contrato.EmpresaRemetente;

                    // Dados da empresa destinataria
                    _empresaDestinatariaContrato = _contrato.EmpresaDestinataria;

                    // Dados da empresa remetente fiscal
                    ObterEmpresaRemetenteFiscal();

                    // Dados da empresa destinataria fiscal
                    ObterEmpresaDestinatariaFiscal();
                }

                if (_empresaPagadoraContrato != null)
                {
                    _cidadeIbgeEmpresaPagadoraContrato = _empresaPagadoraContrato.CidadeIbge;
                }

                if (_empresaRemetenteContrato != null)
                {
                    _cidadeIbgeEmpresaRemetenteContrato = _empresaRemetenteContrato.CidadeIbge;
                }

                if (_empresaDestinatariaContrato != null)
                {
                    _cidadeIbgeEmpresaDestinatariaContrato = _empresaDestinatariaContrato.CidadeIbge;
                }

                if (_empresaContratoDestinatariaFiscal != null)
                {
                    _cidadeIbgeDestinatariaFiscal = _empresaContratoDestinatariaFiscal.CidadeIbge;
                }

                if (_empresaContratoRemetenteFiscal != null)
                {
                    _cidadeIbgeRemetenteFiscal = _empresaContratoRemetenteFiscal.CidadeIbge;
                }

                if (_vagao != null)
                {
                    _serieVagao = _vagao.Serie;

                    _empresaProprietariaVagao = _vagao.EmpresaProprietaria;
                }

                if (_serieVagao != null)
                {
                    _tipoVagao = _serieVagao.Tipo;
                }

                if (_fluxoComercial != null)
                {
                    _empresaRemetenteFluxoComercial = _fluxoComercial.EmpresaRemetente;
                    _empresaDestinatariaFluxoComercial = _fluxoComercial.EmpresaDestinataria;

                    _estacaoMaeOrigemFluxoComercial = _fluxoComercial.Origem;
                    _estacaoMaeDestinoFluxoComercial = _fluxoComercial.Destino;
                }

                _serieDespachoUf = _serieDespachoUfRepository.ObterPorCnpjUf(_cte.CnpjFerrovia, _cte.SiglaUfFerrovia);

                if (_estacaoMaeOrigemFluxoComercial != null)
                {
                    _municipioEstacaoMaeOrigemFluxoComercial = _estacaoMaeOrigemFluxoComercial.Municipio;

                    _empresaOperadoraEstacaoMaeOrigemFluxoComercial = _estacaoMaeOrigemFluxoComercial.EmpresaOperadora;
                }

                if (_municipioEstacaoMaeOrigemFluxoComercial != null)
                {
                    _cidadeIbgeEstacaoMaeOrigemFluxoComercial = _municipioEstacaoMaeOrigemFluxoComercial.CidadeIbge;
                }

                if (_estacaoMaeDestinoFluxoComercial != null)
                {
                    _municipioEstacaoMaeDestinoFluxoComercial = _estacaoMaeDestinoFluxoComercial.Municipio;
                }

                if (_municipioEstacaoMaeDestinoFluxoComercial != null)
                {
                    _cidadeIbgeEstacaoMaeDestinoFluxoComercial = _municipioEstacaoMaeDestinoFluxoComercial.CidadeIbge;
                }

                if (_contrato != null)
                {
                    if (_contrato.FerroviaFaturamento != null)
                    {
                        // Obter a sigla da empresa que fatura
                        string siglaFerrovia = ALL.Core.Util.Enum<FerroviaFaturamentoEnum>.GetDescriptionOf(_contrato.FerroviaFaturamento.Value);

                        _empresaFerroviaCliente = ObterEmpresaFerrovia(siglaFerrovia);
                    }
                }

                if (_tipoTrafegoMutuo == TipoTrafegoMutuoEnum.IntercambioFaturamentoDestino)
                {

                }

                if (_cte.Id == _cteRaizAgrupamento.Id)
                {
                    _empresaTomadora = _fluxoComercial.EmpresaPagadora;

                    if (_empresaTomadora != null)
                    {
                        _cidadeIbgeEmpresaTomadora = _empresaTomadora.CidadeIbge;
                    }
                }
                else
                {
                    _empresaTomadora = _empresaFerroviaClienteRepository.ObterEmpresaClientePorCnpjDaFerrovia(_cteRaizAgrupamento.CnpjFerrovia, _cteRaizAgrupamento.SiglaUfFerrovia);
                }

                if (_empresaTomadora != null)
                {
                    _cidadeIbgeEmpresaTomadora = _empresaTomadora.CidadeIbge;
                }

                if (_empresaFerroviaCliente != null)
                {
                    _cidadeIbgeEmpresaFerrovia = _empresaFerroviaCliente.CidadeIbge;

                    // Verifica qual � o Tipo de Trafego Mutuo
                    _tipoTrafegoMutuo = ObterTipoTrafegoMutuo();
                }

                /* 1) Regra: Quando a esta��o de origem e a esta��o de destino forem da ALL mas
                 * a empresa que fatura � fora da ALL. O tomador de servi�o passa a ser a ferrovia que fatura
                 * 2) Regra: Intercambio Faturamento Origem ou Destino
                 *	Para os fluxos que geram CTe, quando houver interc�mbio na origem ou destino, 
                 *	e a ferrovia que fatura for fora do grupo, o tomador de servi�o dever� ser a ferrovia que 
                 *	fatura.
                */
                if (_tipoTrafegoMutuo == TipoTrafegoMutuoEnum.FaturamentoEmpresaForaGrupo)
                {
                    // Obter os dados da Ferrovia
                    string siglaFerrovia = ALL.Core.Util.Enum<FerroviaFaturamentoEnum>.GetDescriptionOf(_contrato.FerroviaFaturamento.Value);
                    IEmpresa empresaFerrovia = ObterEmpresaFerrovia(siglaFerrovia);
                    _empresaTomadora = empresaFerrovia;
                    _cidadeIbgeEmpresaTomadora = _empresaTomadora.CidadeIbge;
                }
                else if (_tipoTrafegoMutuo == TipoTrafegoMutuoEnum.FaturamentoArgentina)
                {
                    string siglaFerrovia = ALL.Core.Util.Enum<FerroviaFaturamentoEnum>.GetDescriptionOf(_contrato.FerroviaFaturamento.Value);
                    IEmpresa empresaFerrovia = _empresaFerroviaRepository.ObterPorSigla(siglaFerrovia);
                    _empresaTomadora = empresaFerrovia;
                    _cidadeIbgeEmpresaTomadora = _empresaTomadora.CidadeIbge;
                }
                else if ((_tipoTrafegoMutuo == TipoTrafegoMutuoEnum.IntercambioFaturamentoOrigem) || (_tipoTrafegoMutuo == TipoTrafegoMutuoEnum.IntercambioFaturamentoDestino))
                {
                    string siglaFerrovia = ALL.Core.Util.Enum<FerroviaFaturamentoEnum>.GetDescriptionOf(_contrato.FerroviaFaturamento.Value);
                    IEmpresa empresaFerrovia = _empresaFerroviaRepository.ObterPorSigla(siglaFerrovia);
                    _empresaTomadora = empresaFerrovia;
                    _cidadeIbgeEmpresaTomadora = _empresaTomadora.CidadeIbge;
                }

                _cfopEstadual = _configuracaoTranslogicRepository.ObterPorId(_chaveCfopEstadual);
                _cfopInterestadual = _configuracaoTranslogicRepository.ObterPorId(_chaveCfopInterestadual);
                _cfopInternacional = _configuracaoTranslogicRepository.ObterPorId(_chaveCfopInternacional);

                if ((_cfopEstadual != null) && (_cfopInterestadual != null) && (_cfopInternacional != null))
                {
                    ObterCfopTrafegoMutuo();
                }

                /* Verifica se � uma partilha da ferroeste (Ferropar) */
                if ((_estacaoMaeOrigemFluxoComercial != null) && (_estacaoMaeOrigemFluxoComercial.EmpresaConcessionaria != null))
                {
                    _partilhaFerroeste = (_estacaoMaeOrigemFluxoComercial.EmpresaConcessionaria.Id == 49) ? true : false;

                    if (_partilhaFerroeste)
                    {
                        _origemFerroeste = true;
                    }
                }

                if ((_estacaoMaeDestinoFluxoComercial != null) && (_estacaoMaeDestinoFluxoComercial.EmpresaConcessionaria != null))
                {
                    if (!_partilhaFerroeste)
                    {
                        _partilhaFerroeste = (_estacaoMaeDestinoFluxoComercial.EmpresaConcessionaria.Id == 49) ? true : false;
                        if (_partilhaFerroeste)
                        {
                            _origemFerroeste = false;
                        }
                    }
                }

                _tipoNotaCte = ObterTipoNotaCte();
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(_cte, "GravarConfigCteEnvioService", string.Format("CarregarDados: {0}", ex.Message), ex);
                throw;
            }
        }

        private TipoNotaCteEnum ObterTipoNotaCte()
        {
            if (!string.IsNullOrWhiteSpace(_contratoHistorico.CfopProduto))
            {
                if (_contratoHistorico.CfopProduto[0] == '3')
                {
                    return TipoNotaCteEnum.NotaEntrada;
                }
            }

            return TipoNotaCteEnum.NotaSaida;
        }

        private int ObterNumeroConteiners()
        {
            IList<CteDetalhe> listaCteDetalhe = _cteDetalheRepository.ObterPorCte(_cte);
            List<string> listaConteiner = listaCteDetalhe.GroupBy(g => g.ConteinerNotaFiscal).Select(s => s.Key).ToList();
            return listaConteiner.Count;
        }

        private void ValidarValorCte()
        {
            try
            {
                if (_cte.ValorCte == 0.0)
                {
                    _bufferMensagem.Append(" N�o foi poss�vel calcular o valor do Cte. (favor verificar composi��o de frete no SAP)");
                }

                if (_bufferMensagem.Length > 0)
                {
                    throw new Exception(_bufferMensagem.ToString());
                }
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(_cte, "GravarConfigCteEnvioService", string.Format("ValidarValorCte: {0}", ex.Message), ex);
                throw;
            }
        }

        private void ValidarDados()
        {
            try
            {
                if (_cte == null)
                {
                    _bufferMensagem.Append("9001,");
                }

                if (((_listaCteDetalhe == null) || (_listaCteDetalhe.Count == 0)) && (_cte.TipoCte != TipoCteEnum.Complementar) && (_cte.TipoCte != TipoCteEnum.Anulacao))
                {
                    _bufferMensagem.Append("9002,");
                }

                // if ((_listaCteDetalhe != null) && (_cte.Complementar == false) && (_listaCteDetalhe.Count == 0))
                // {
                // 	_bufferMensagem.Append(" _listaCteDetalhe:N�o encontrado os detalhes das notas do CTE ");
                // }

                if (_cteRaiz == null)
                {
                    _bufferMensagem.Append("9003,");
                }

                if (_cteRaizAgrupamento == null)
                {
                    _bufferMensagem.Append("9004,");
                }

                if (_fluxoComercial == null)
                {
                    _bufferMensagem.Append("9005,");
                }

                if (_despachoTranslogic == null)
                {
                    _bufferMensagem.Append("9006,");
                }

                if (_serieDespachoUf == null)
                {
                    _bufferMensagem.Append("9007,");
                }

                if (_empresaRemetenteFluxoComercial == null)
                {
                    _bufferMensagem.Append("9008,");
                }

                if (_empresaDestinatariaFluxoComercial == null)
                {
                    _bufferMensagem.Append("9009,");
                }

                if (_empresaRemetenteFluxoComercial != null)
                {
                    if (_empresaRemetenteFluxoComercial.CidadeIbge == null)
                    {
                        _bufferMensagem.Append("9010,");
                    }
                }

                if (_empresaDestinatariaFluxoComercial != null)
                {
                    if (_empresaDestinatariaFluxoComercial.CidadeIbge == null)
                    {
                        _bufferMensagem.Append("9011,");
                    }
                }

                if (_contratoHistorico == null)
                {
                    _bufferMensagem.Append("9012,");
                }

                if (_contratoHistorico != null)
                {
                    if (_contratoHistorico.Cst == null || !new Regex(ALL.Core.Util.Enum<RegexSefaz>.GetDescriptionOf(RegexSefaz.CST)).Match(_contratoHistorico.Cst).Success)
                    {
                        //_bufferMensagem.Append(String.Format("Cst do Contrato Historico inv�lido - cst: {0}, contrato: {1} ", _contratoHistorico.Cst, _contratoHistorico.NumeroContrato.ToString()));
                        _bufferMensagem.Append("9013,");
                    }
                }

                if (_empresaPagadoraContrato == null)
                {
                    _bufferMensagem.Append("9014,");
                }

                if (_empresaRemetenteContrato == null)
                {
                    _bufferMensagem.Append("9015,");
                }

                if (_empresaDestinatariaContrato == null)
                {
                    _bufferMensagem.Append("9016,");
                }

                if (_empresaContratoDestinatariaFiscal == null)
                {
                    _bufferMensagem.Append("9017,");
                }

                if (_empresaContratoRemetenteFiscal == null)
                {
                    _bufferMensagem.Append("9018,");
                }

                if (_cidadeIbgeDestinatariaFiscal == null)
                {
                    _bufferMensagem.Append("9019,");
                }

                if (_cidadeIbgeRemetenteFiscal == null)
                {
                    _bufferMensagem.Append("9020,");
                }

                if (_cidadeIbgeEmpresaPagadoraContrato == null)
                {
                    _bufferMensagem.Append("9021,");
                }

                if (_cidadeIbgeEmpresaDestinatariaContrato == null)
                {
                    _bufferMensagem.Append("9022,");
                }

                if (_cidadeIbgeEmpresaRemetenteContrato == null)
                {
                    _bufferMensagem.Append("9023,");
                }

                if (_itemDespacho == null)
                {
                    _bufferMensagem.Append("9024,");
                }

                if (_vagao == null)
                {
                    _bufferMensagem.Append("9025,");
                }

                if (_serieVagao == null)
                {
                    _bufferMensagem.Append("9026,");
                }

                if (_tipoVagao == null)
                {
                    _bufferMensagem.Append("9027,");
                }

                if (_empresaProprietariaVagao == null)
                {
                    _bufferMensagem.Append("9028,");
                }

                if (_estacaoMaeOrigemFluxoComercial == null)
                {
                    _bufferMensagem.Append("9029,");
                }

                if (_estacaoMaeDestinoFluxoComercial == null)
                {
                    _bufferMensagem.Append("9030,");
                }

                if (_municipioEstacaoMaeOrigemFluxoComercial == null)
                {
                    _bufferMensagem.Append("9031,");
                }

                if (_municipioEstacaoMaeDestinoFluxoComercial == null)
                {
                    _bufferMensagem.Append("9032,");
                }

                if (_cidadeIbgeEstacaoMaeOrigemFluxoComercial == null)
                {
                    _bufferMensagem.Append("9033,");
                }
                else
                {
                    if (_paisBacenRepository.ObterPorSiglaResumida(_cidadeIbgeEstacaoMaeOrigemFluxoComercial.SiglaPais) == null)
                    {
                        _bufferMensagem.Append("9034,");
                    }
                }

                if (_cidadeIbgeEstacaoMaeDestinoFluxoComercial == null)
                {
                    _bufferMensagem.Append("9035,");
                }
                else
                {
                    if (_paisBacenRepository.ObterPorSiglaResumida(_cidadeIbgeEstacaoMaeDestinoFluxoComercial.SiglaPais) == null)
                    {
                        _bufferMensagem.Append("9036,");
                    }
                }

                if (_empresaOperadoraEstacaoMaeOrigemFluxoComercial == null)
                {
                    _bufferMensagem.Append("9037,");
                }

                if (_empresaFerroviaCliente == null)
                {
                    _bufferMensagem.Append("9038,");
                }

                if (_tipoTrafegoMutuo == TipoTrafegoMutuoEnum.Indefinido)
                {
                    _bufferMensagem.Append("9039,");
                }

                if (_cidadeIbgeEmpresaFerrovia == null)
                {
                    _bufferMensagem.Append("9040,");
                }
                else
                {
                    if (_paisBacenRepository.ObterPorSiglaResumida(_cidadeIbgeEmpresaFerrovia.SiglaPais) == null)
                    {
                        _bufferMensagem.Append("9041,");
                    }
                }

                if (String.IsNullOrEmpty(_cfop))
                {
                    _bufferMensagem.Append("9042,");
                }

                if (String.IsNullOrEmpty(_cfopDescricao))
                {
                    _bufferMensagem.Append("9043,");
                }

                if ((ObterIdentificadorFilial() == -1) || (_nfe03Filial == null))
                {
                    //_bufferMensagem.Append(String.Format("N�o encontrado a filial para o cnpj: {0}", _cte.CnpjFerrovia));
                    _bufferMensagem.Append("9044,");
                }

                if (_tipoTrafegoMutuo == TipoTrafegoMutuoEnum.IntercambioFaturamentoDestino)
                {
                    if (_fluxoComercial.DestinoIntercambio.EmpresaConcessionaria != null)
                    {
                        if (_fluxoComercial.DestinoIntercambio.EmpresaConcessionaria.CidadeIbge == null)
                        {
                            _bufferMensagem.Append("9045,");
                        }
                    }
                    else
                    {
                        _bufferMensagem.Append("9046,");
                    }
                }

                if (_bufferMensagem.Length > 0)
                {
                    throw new Exception(_bufferMensagem.ToString().Substring(0, _bufferMensagem.ToString().Length - 1));
                }


            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(_cte, "GravarConfigCteEnvioService", string.Format("ValidarDados: {0}", ex.Message), ex);
                throw;
            }
        }

        public void InserirComposicaoFreteCteTeste(Cte cte)
        {
            _cte = cte;
            CarregarDados();
            InserirComposicaoFreteCte(_cte.BaseCalculoIcms);
        }

        private void InserirComposicaoFreteCte(double? valorCte)
        {
            try
            {
                double valorCotacao = ObterValorCotacao();
                double aliquotaIcms = _cte.PercentualAliquotaIcms ?? 0.0;
                double coeficiente = 100 / (100 - aliquotaIcms);
                coeficiente = Math.Round(coeficiente, 6);

                ComposicaoFreteCte composicaoFreteCte;
                double valorComIcms;
                double valorSemIcms;

                double totalComIcms = 0.0;
                double totalSemIcms = 0.0;
                double totalSomaValorSemIcmsRateado = 0.0;
                double totalToneladaComIcms = 0.0;
                double totalToneladaSemIcms = 0.0;

                string letraFerrovia = _serieDespachoUf.LetraFerrovia;
                double toneladaUtil = _cte.PesoParaCalculo ?? 0.0;
                var listaComposicaoFreteContrato = _composicaoFreteContratoRepository
                    .ObterPorNumeroContrato(_contratoHistorico.NumeroContrato)
                    .OrderBy(p => p.CondicaoTarifa);
                var listaComposicaoFreteContratoVg = _composicaoFreteContratoRepository
                    .ObterPorNumeroContrato(_contratoHistorico.NumeroContrato, _cteRaiz.DataHora, "VG", true)
                    .OrderBy(p => p.CondicaoTarifa);

                // Dados da composicao frete contrato
                foreach (ComposicaoFreteContrato composicaoFreteContrato in listaComposicaoFreteContrato)
                {
                    // se for rateio de cte, pega o valor rateado.
                    if (_fluxoComercial.IndRateioCte ?? false)
                    {
                        valorSemIcms = CalcularRateioCompFreteCte(composicaoFreteContrato.CondicaoTarifa, _cte.Chave,
                            composicaoFreteContrato.ValorSemIcms ?? 0);
                    }
                    else
                    {
                        valorSemIcms = composicaoFreteContrato.ValorSemIcms ?? 0;
                    }
                    valorSemIcms = Math.Round(valorSemIcms * valorCotacao, 2);


                    // se for rateio de cte, calcula a diferne�a de centavos para o ultimo
                    if (_fluxoComercial.IndRateioCte ?? false)
                    {
                        // pega a ultima condi��o da composi��o frete 
                        var ultimaComposicaoFrete = _composicaoFreteContratoRepository
                            .ObterPorNumeroContrato(_contratoHistorico.NumeroContrato)
                            .OrderBy(p => p.CondicaoTarifa)
                            .LastOrDefault();
                        if (composicaoFreteContrato.CondicaoTarifa == ultimaComposicaoFrete.CondicaoTarifa)
                        {
                            double totalCompFreteContrato = valorCte ?? 0;

                            // calcula a diferen�a
                            var diferenca = totalCompFreteContrato - (totalSomaValorSemIcmsRateado + valorSemIcms);

                            // soma a diferne�a no valor
                            valorSemIcms = valorSemIcms + diferenca;
                        }
                    }
                    totalSomaValorSemIcmsRateado += valorSemIcms;

                    valorComIcms = valorSemIcms * coeficiente;
                    valorComIcms = Math.Round(valorComIcms, 2);

                    composicaoFreteCte = new ComposicaoFreteCte();

                    if (composicaoFreteContrato.CondicaoTarifa != "ZDES")
                    {
                        // Insere na composi��o frete cte
                        composicaoFreteCte.Cte = _cte;
                        composicaoFreteCte.CondicaoTarifa = composicaoFreteContrato.CondicaoTarifa;
                        composicaoFreteCte.Ferrovia = composicaoFreteContrato.Ferrovia;

                        // se for rateio de cte, pega o valor rateado.
                        if (_fluxoComercial.IndRateioCte ?? false)
                        {
                            composicaoFreteCte.ValorComIcms =
                                Math.Round(valorComIcms, 2, MidpointRounding.AwayFromZero);
                            composicaoFreteCte.ValorSemIcms =
                                Math.Round(valorSemIcms, 2, MidpointRounding.AwayFromZero);
                        }
                        else
                        {
                            composicaoFreteCte.ValorComIcms = Math.Round(valorComIcms * toneladaUtil, 2,
                                MidpointRounding.AwayFromZero);
                            composicaoFreteCte.ValorSemIcms = Math.Round(valorSemIcms * toneladaUtil, 2,
                                MidpointRounding.AwayFromZero);
                        }

                        composicaoFreteCte.ValorToneladaComIcms = composicaoFreteContrato.ValorComIcms ?? 0.0;
                        composicaoFreteCte.ValorToneladaSemIcms = composicaoFreteContrato.ValorSemIcms ?? 0.0;
                        composicaoFreteCte.Coeficiente = coeficiente;

                        if (composicaoFreteCte.ValorSemIcms.Value > 0)
                        {
                            totalSemIcms += composicaoFreteCte.ValorSemIcms.Value;
                            totalToneladaSemIcms += composicaoFreteContrato.ValorSemIcms ?? 0.0;
                        }

                        if (composicaoFreteCte.ValorComIcms.Value > 0)
                        {
                            totalComIcms += composicaoFreteCte.ValorComIcms.Value;
                            totalToneladaComIcms += composicaoFreteContrato.ValorComIcms ?? 0.0;
                        }
                    }
                    else
                    {
                        // Insere na composi��o frete cte
                        composicaoFreteCte.Cte = _cte;
                        composicaoFreteCte.CondicaoTarifa = composicaoFreteContrato.CondicaoTarifa;
                        composicaoFreteCte.Ferrovia = composicaoFreteContrato.Ferrovia;
                        composicaoFreteCte.ValorComIcms = _cte.DescontoPorPercentual ?? 0.0;
                        composicaoFreteCte.ValorSemIcms = _cte.DescontoPorPercentual ?? 0.0;

                        composicaoFreteCte.ValorToneladaComIcms = composicaoFreteContrato.ValorComIcms ?? 0.0;
                        composicaoFreteCte.ValorToneladaSemIcms = composicaoFreteContrato.ValorSemIcms ?? 0.0;
                        composicaoFreteCte.Coeficiente = coeficiente;
                    }

                    /*
                    caso for uma das condi��es abaixo, acrescentar a diferen�a dos centavos, no valor com ICMS
                    ZFRL � Malha Sul
                    ZFRZ � Malha Paulista
                    ZFRN � Malha Norte
                    ZFRJ � Malha Oeste
                    */
                    var malhasAll = new string[] { "ZFRL", "ZFRZ", "ZFRN", "ZFRJ" };
                    if (malhasAll.Contains(composicaoFreteContrato.CondicaoTarifa))
                    {
                        composicaoFreteCte.ValorComIcms =
                            Math.Round(composicaoFreteCte.ValorComIcms.Value + _diferencaDeCentavosNaComposicaoFrete, 2,
                                MidpointRounding.AwayFromZero);
                        totalComIcms = Math.Round(totalComIcms + _diferencaDeCentavosNaComposicaoFrete, 2,
                            MidpointRounding.AwayFromZero);
                        _diferencaDeCentavosNaComposicaoFrete = 0;
                    }

                    _composicaoFreteCteRepository.Inserir(composicaoFreteCte);
                }

                // Dados da composicao frete contrato VG
                foreach (ComposicaoFreteContrato composicaoFreteContrato in listaComposicaoFreteContratoVg)
                {
                    if (composicaoFreteContrato.ValorSemIcms > 0)
                    {
                        if (_empresaProprietariaVagao.Sigla == "L")
                        {
                            if (composicaoFreteContrato.CondicaoTarifa != "ZDES")
                            {
                                // se for rateio de cte, pega o valor rateado.
                                if (_fluxoComercial.IndRateioCte ?? false)
                                {
                                    valorSemIcms = CalcularRateioCompFreteCte(composicaoFreteContrato.CondicaoTarifa,
                                        _cte.Chave, composicaoFreteContrato.ValorSemIcms ?? 0);
                                }
                                else
                                {
                                    valorSemIcms = composicaoFreteContrato.ValorSemIcms ?? 0;
                                }
                                valorSemIcms = Math.Round(valorSemIcms * valorCotacao, 2);


                                // se for rateio de cte, calcula a diferne�a de centavos para o ultimo
                                if (_fluxoComercial.IndRateioCte ?? false)
                                {
                                    // pega a ultima condi��o da composi��o frete 
                                    var ultimaComposicaoFrete =
                                        _composicaoFreteContratoRepository
                                            .ObterPorNumeroContrato(_contratoHistorico.NumeroContrato)
                                            .OrderBy(p => p.CondicaoTarifa)
                                            .LastOrDefault();
                                    if (composicaoFreteContrato.CondicaoTarifa == ultimaComposicaoFrete.CondicaoTarifa)
                                    {
                                        double totalCompFreteContrato = valorCte ?? 0;

                                        // calcula a diferen�a
                                        var diferenca = totalCompFreteContrato -
                                                        (totalSomaValorSemIcmsRateado + valorSemIcms);

                                        // soma a diferne�a no valor
                                        valorSemIcms = valorSemIcms + diferenca;
                                    }
                                }
                                totalSomaValorSemIcmsRateado += valorSemIcms;

                                valorComIcms = valorSemIcms * coeficiente;
                                valorComIcms = Math.Round(valorComIcms, 2);

                                // Insere na composi��o frete cte
                                composicaoFreteCte = new ComposicaoFreteCte();
                                composicaoFreteCte.Cte = _cte;
                                composicaoFreteCte.CondicaoTarifa = composicaoFreteContrato.CondicaoTarifa;
                                composicaoFreteCte.Ferrovia = composicaoFreteContrato.Ferrovia;
                                composicaoFreteCte.ValorComIcms = Math.Round(valorComIcms * toneladaUtil, 2);
                                composicaoFreteCte.ValorSemIcms = Math.Round(valorSemIcms * toneladaUtil, 2);
                                composicaoFreteCte.ValorToneladaComIcms = composicaoFreteContrato.ValorComIcms ?? 0.0;
                                composicaoFreteCte.ValorToneladaSemIcms = composicaoFreteContrato.ValorSemIcms ?? 0.0;
                                composicaoFreteCte.Coeficiente = coeficiente;

                                _composicaoFreteCteRepository.Inserir(composicaoFreteCte);

                                if (composicaoFreteCte.ValorSemIcms.Value > 0)
                                {
                                    totalSemIcms += composicaoFreteCte.ValorSemIcms.Value;
                                    totalToneladaSemIcms += composicaoFreteContrato.ValorSemIcms ?? 0.0;
                                }

                                if (composicaoFreteCte.ValorComIcms.Value > 0)
                                {
                                    totalComIcms += composicaoFreteCte.ValorComIcms.Value;
                                    totalToneladaComIcms += composicaoFreteContrato.ValorComIcms ?? 0.0;
                                }
                            }
                            else
                            {
                                // Insere na composi��o frete cte
                                composicaoFreteCte = new ComposicaoFreteCte();
                                composicaoFreteCte.Cte = _cte;
                                composicaoFreteCte.CondicaoTarifa = composicaoFreteContrato.CondicaoTarifa;
                                composicaoFreteCte.Ferrovia = composicaoFreteContrato.Ferrovia;
                                composicaoFreteCte.ValorComIcms = _cte.DescontoPorPercentual ?? 0.0;
                                composicaoFreteCte.ValorSemIcms = _cte.DescontoPorPercentual ?? 0.0;

                                composicaoFreteCte.ValorToneladaComIcms = composicaoFreteContrato.ValorComIcms ?? 0.0;
                                composicaoFreteCte.ValorToneladaSemIcms = composicaoFreteContrato.ValorSemIcms ?? 0.0;
                                composicaoFreteCte.Coeficiente = coeficiente;

                                _composicaoFreteCteRepository.Inserir(composicaoFreteCte);
                            }
                        }
                    }
                }

                // Caso seja MS verifica se � uma substitui��o tribut�ria
                if (_cte.SiglaUfFerrovia == "MS")
                {
                    // Verifica se existe a substitui��o tributaria, caso a aliquota for maior 0.0, calcula o valor liquido
                    double aliquotaSt = _contratoHistorico.AliquotaIcmsSubstProduto ?? 0.0;
                    if (aliquotaSt > 0.0)
                    {
                        composicaoFreteCte = new ComposicaoFreteCte();
                        composicaoFreteCte.Cte = _cte;
                        composicaoFreteCte.CondicaoTarifa = "ZIC4";
                        composicaoFreteCte.Ferrovia = letraFerrovia;
                        composicaoFreteCte.ValorComIcms = _valorImpostoRetidoSubstTributaria;
                        composicaoFreteCte.ValorSemIcms = _valorImpostoRetidoSubstTributaria;
                        composicaoFreteCte.ValorToneladaComIcms = _valorImpostoRetidoSubstTributaria;
                        composicaoFreteCte.ValorToneladaSemIcms = _valorImpostoRetidoSubstTributaria;
                        composicaoFreteCte.Coeficiente = coeficiente;

                        _composicaoFreteCteRepository.Inserir(composicaoFreteCte);
                    }
                }

                // -> Aplica corre��o para problema de arredondamentos nos centavos.
                if (valorCte.HasValue && totalComIcms > valorCte)
                {
                    totalComIcms = valorCte.Value;
                }

                // Grava a totaliza��o dos dados
                // Insere na composi��o frete cte
                composicaoFreteCte = new ComposicaoFreteCte();
                composicaoFreteCte.Cte = _cte;
                composicaoFreteCte.CondicaoTarifa = "ICMI";
                composicaoFreteCte.Ferrovia = letraFerrovia;
                composicaoFreteCte.ValorComIcms = Math.Round(totalComIcms, 2);
                composicaoFreteCte.ValorSemIcms = Math.Round(totalSemIcms, 2);
                composicaoFreteCte.ValorToneladaComIcms = totalToneladaComIcms;
                composicaoFreteCte.ValorToneladaSemIcms = totalToneladaSemIcms;
                composicaoFreteCte.Coeficiente = coeficiente;

                _composicaoFreteCteRepository.Inserir(composicaoFreteCte);
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(_cte, "GravarConfigCteEnvioService", string.Format("InserircomposicaoFrete: {0}", ex.Message), ex);
                throw;
            }
        }

        private double CalcularBaseCalculoIcms()
        {
            if (_cte.Id == _cteRaizAgrupamento.Id)
            {
                return CalcularBaseCalculoIcmsCteRaiz();
            }

            return CalcularBaseCalculoIcmsCteFilho();
        }

        /// <summary>
        /// Calcula a base de calculo do icms quando o cte for filho (depois do primeiro no trafago mutuo)
        /// </summary>
        /// <returns>Retorna a base de calculo do ICMS</returns>
        private double CalcularBaseCalculoIcmsCteFilho()
        {
            double valorCotacao = ObterValorCotacao();
            double valorBaseCalculo = 0.0;
            try
            {
                int tipoTributacao = _contratoHistorico.TipoTributacao ?? 0;

                string letraFerrovia = _serieDespachoUf.LetraFerrovia;
                double toneladaUtil = _cte.PesoParaCalculo ?? 0.0;
                double aliquotaIcms = _cte.PercentualAliquotaIcms ?? 0.0;
                double coeficiente = 100 / (100 - aliquotaIcms);
                coeficiente = Math.Round(coeficiente, 6);

                IList<ComposicaoFreteContrato> listaComposicaoFreteContrato = _composicaoFreteContratoRepository.ObterPorNumeroContrato(_contratoHistorico.NumeroContrato, _cteRaiz.DataHora, letraFerrovia, true);
                IList<ComposicaoFreteContrato> listaComposicaoFreteContratoVg = _composicaoFreteContratoRepository.ObterPorNumeroContrato(_contratoHistorico.NumeroContrato, _cteRaiz.DataHora, "VG", true);

                double? valorTotal = 0.0;

                if (tipoTributacao == 0)
                {
                    valorTotal = listaComposicaoFreteContrato.Sum(g => g.ValorSemIcms) * valorCotacao;

                    if ((listaComposicaoFreteContratoVg.Count > 0) && (_empresaProprietariaVagao.Sigla == "L"))
                    {
                        valorTotal += listaComposicaoFreteContratoVg.Sum(g => g.ValorSemIcms) * valorCotacao;
                    }
                }
                else
                {
                    valorTotal = listaComposicaoFreteContrato.Sum(g => g.ValorComIcms) * valorCotacao;
                    if ((listaComposicaoFreteContratoVg.Count > 0) && (_empresaProprietariaVagao.Sigla == "L"))
                    {
                        valorTotal += listaComposicaoFreteContratoVg.Sum(g => g.ValorComIcms) * valorCotacao;
                    }
                }

                valorTotal *= toneladaUtil;

                valorBaseCalculo = valorTotal.Value;
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(_cte, "GravarConfigCteEnvioService", string.Format("CalcularBaseCalculoIcmsCteFilho: {0}", ex.Message), ex);
                throw;
            }

            if (valorBaseCalculo < 0)
            {
                valorBaseCalculo = 0;
            }

            return Math.Round(valorBaseCalculo, 2);
        }

        /// <summary>
        /// Calcula a base de calculo do icms quando o cte for raiz (primeiro cte do trafego mutuo)
        /// </summary>
        /// <returns>Retorna a base de calculo do ICMS</returns>
        private double CalcularBaseCalculoIcmsCteRaiz()
        {
            double valorCotacao = ObterValorCotacao();
            double valorTotal = 0.0;
            double valorCalculo = 0.0;
            double valorBaseCalculoIcms = 0.0;
            double toneladaUtil = _cte.PesoParaCalculo ?? 0.0;
            double aliquotaIcms = _cte.PercentualAliquotaIcms ?? 0.0;
            double coeficiente = 100 / (100 - aliquotaIcms);
            coeficiente = Math.Round(coeficiente, 6);

            try
            {
                IList<ComposicaoFreteContrato> listaComposicaoFreteContrato = _composicaoFreteContratoRepository.ObterPorNumeroContrato(_contratoHistorico.NumeroContrato);

                foreach (ComposicaoFreteContrato composicao in listaComposicaoFreteContrato)
                {
                    if (composicao.ValorSemIcms > 0)
                    {
                        valorCalculo = Math.Round((composicao.ValorSemIcms.Value * valorCotacao), 2);
                        valorCalculo = Math.Round((valorCalculo * coeficiente), 2, MidpointRounding.AwayFromZero);
                        valorCalculo = Math.Round(valorCalculo * toneladaUtil, 2, MidpointRounding.AwayFromZero);
                        valorTotal += valorCalculo;
                    }
                }

                valorBaseCalculoIcms = valorTotal;
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(_cte, "GravarConfigCteEnvioService", string.Format("CalcularBaseCalculoIcmsCteRaiz: {0}", ex.Message), ex);
                throw;
            }

            return Math.Round(valorBaseCalculoIcms, 2);
        }

        /// <summary>
        /// Calcula a base de calculo do icms quando o cte for raiz (primeiro cte do trafego mutuo)
        /// </summary>
        /// <returns>Retorna a base de calculo do ICMS</returns>
        private double CalcularBaseCalculoIcmsCteConteiner(int numConteiners, string condTarifa = "")
        {
            double valorCotacao = ObterValorCotacao();
            double valorTotal = 0.0;
            double valorCalculo = 0.0;
            double valorBaseCalculoIcms = 0.0;
            double toneladaUtil = numConteiners;
            double aliquotaIcms = _cte.PercentualAliquotaIcms ?? 0.0;
            double coeficiente = 100 / (100 - aliquotaIcms);
            coeficiente = Math.Round(coeficiente, 6);

            bool bFiltraConfTarifa = !string.IsNullOrEmpty(condTarifa);
            try
            {
                IList<ComposicaoFreteContrato> listaComposicaoFreteContrato = _composicaoFreteContratoRepository.ObterPorNumeroContrato(_contratoHistorico.NumeroContrato);

                foreach (ComposicaoFreteContrato composicao in listaComposicaoFreteContrato)
                {
                    if ((bFiltraConfTarifa && condTarifa == composicao.CondicaoTarifa) || !bFiltraConfTarifa)
                    {
                        if (composicao.ValorSemIcms > 0)
                        {
                            valorCalculo = Math.Round((composicao.ValorSemIcms.Value * valorCotacao), 2);
                            valorCalculo = Math.Round((valorCalculo * coeficiente), 2, MidpointRounding.AwayFromZero);
                            valorCalculo = Math.Round(valorCalculo * toneladaUtil, 2, MidpointRounding.AwayFromZero);
                            valorTotal += valorCalculo;
                        }
                    }
                }

                valorBaseCalculoIcms = valorTotal;
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(_cte, "GravarConfigCteEnvioService", string.Format("CalcularBaseCalculoIcmsCteRaiz: {0}", ex.Message), ex);
                throw;
            }

            return Math.Round(valorBaseCalculoIcms, 2);
        }

        private double CalcularDescontoPorTonelada()
        {
            IList<ComposicaoFreteContrato> listaComposicaoFreteContrato = _composicaoFreteContratoRepository.ObterPorNumeroContrato(_contratoHistorico.NumeroContrato);
            double totalTarifa = listaComposicaoFreteContrato.Where(g => g.CondicaoTarifa == "ZDEV").Sum(h => h.ValorSemIcms).Value;
            return totalTarifa * _pesoParaCalculo;
        }

        private double CalcularDescontoPorPercentual()
        {
            IList<ComposicaoFreteContrato> listaComposicaoFreteContrato = _composicaoFreteContratoRepository.ObterPorNumeroContrato(_contratoHistorico.NumeroContrato);
            double tarifaDesconto = listaComposicaoFreteContrato.Where(g => g.CondicaoTarifa == "ZDES").Sum(h => h.ValorSemIcms).Value;
            double percentualTarifa = tarifaDesconto / 100;

            return percentualTarifa * (_cte.BaseCalculoIcms ?? 0.0);
        }

        private double CalcularValorFrete()
        {
            if (_cte.Id == _cteRaizAgrupamento.Id)
            {
                return CalcularValorFreteCteRaiz();
            }

            return CalcularValorFreteCteFilho();
        }

        private double CalcularValorFreteCteRaiz()
        {
            double valorFrete = 0.0;
            double toneladaUtil = _cte.PesoParaCalculo ?? 0.0;
            try
            {
                IList<ComposicaoFreteContrato> listaComposicaoFreteContrato = _composicaoFreteContratoRepository.ObterPorNumeroContrato(_contratoHistorico.NumeroContrato, _condicaoTarifa);
                valorFrete = listaComposicaoFreteContrato.Sum(g => g.ValorComIcms).Value * toneladaUtil;
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(_cte, "GravarConfigCteEnvioService", string.Format("CalcularBaseCalculoIcmsCteRaiz: {0}", ex.Message), ex);
                throw;
            }

            return valorFrete;
        }

        private double CalcularValorFreteCteFilho()
        {
            double valorFrete = 0.0;
            try
            {
                int tipoTributacao = _contratoHistorico.TipoTributacao ?? 0;

                string letraFerrovia = _serieDespachoUf.LetraFerrovia;
                double toneladaUtil = _cte.PesoParaCalculo ?? 0.0;
                double aliquotaIcms = _cte.PercentualAliquotaIcms ?? 0.0;

                IList<ComposicaoFreteContrato> listaComposicaoFreteContrato = _composicaoFreteContratoRepository.ObterPorNumeroContrato(_contratoHistorico.NumeroContrato, _cteRaiz.DataHora, letraFerrovia, true, _condicaoTarifa);
                IList<ComposicaoFreteContrato> listaComposicaoFreteContratoVg = _composicaoFreteContratoRepository.ObterPorNumeroContrato(_contratoHistorico.NumeroContrato, _cteRaiz.DataHora, "VG", true, _condicaoTarifa);

                double? valorTotal = 0.0;

                if (tipoTributacao == 0)
                {
                    valorTotal = listaComposicaoFreteContrato.Sum(g => g.ValorSemIcms);
                    if ((listaComposicaoFreteContratoVg.Count > 0) && (_empresaProprietariaVagao.Sigla == "L"))
                    {
                        valorTotal += listaComposicaoFreteContratoVg.Sum(g => g.ValorSemIcms);
                    }
                }
                else
                {
                    valorTotal = listaComposicaoFreteContrato.Sum(g => g.ValorComIcms);
                    if ((listaComposicaoFreteContratoVg.Count > 0) && (_empresaProprietariaVagao.Sigla == "L"))
                    {
                        valorTotal += listaComposicaoFreteContratoVg.Sum(g => g.ValorComIcms);
                    }
                }

                valorTotal *= toneladaUtil;

                // Como � CTE filho, n�o tem ICMS
                valorFrete = valorTotal.Value;
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(_cte, "GravarConfigCteEnvioService", string.Format("CalcularValorFreteCteFilho: {0}", ex.Message), ex);
                throw;
            }

            if (valorFrete < 0)
            {
                valorFrete = 0;
            }

            return valorFrete;
        }

        private double CalcularValorTotalNotaFiscal()
        {
            return _listaCteDetalhe.Sum(g => g.ValorNotaFiscal);
        }

        private double CalcularPesoTotal()
        {
            return _listaCteDetalhe.Sum(g => g.PesoNotaFiscal);
        }

        private int ObterIdentificadorFilial()
        {
            if (_nfe03Filial == null)
            {
                long cnpj;
                Int64.TryParse(_cte.CnpjFerrovia, out cnpj);

                _nfe03Filial = _nfe03FilialRepository.ObterFilialPorCnpj(cnpj);

                if (_nfe03Filial != null)
                {
                    return (int)_nfe03Filial.IdFilial;
                }

                // Caso n�o tenha encontrado a filial
                return -1;
            }

            return (int)_nfe03Filial.IdFilial;
        }

        /// <summary>
        /// Verifica se o fluxo � um fluxo internacional
        /// </summary>
        /// <param name="fluxo"> Objeto FluxoComercial. </param>
        /// <returns> Valor booleano </returns>
        private string VerificarFluxoInternacional(FluxoComercial fluxo)
        {
            AssociaFluxoInternacional associaFluxoInternacional = _associaFluxoInternacionalRepository.ObterPorFluxoComercial(fluxo);

            if (associaFluxoInternacional != null)
            {
                FluxoInternacional fluxoInternacional = associaFluxoInternacional.FluxoInternacional;

                if (fluxoInternacional != null)
                {
                    return fluxoInternacional.CodigoFluxoInternacional;
                }
            }

            return null;
        }

        private IEmpresa ObterDadosEmpresaRemetente(Cte cte)
        {
            IEmpresa empresa = _serieDespachoUfRepository.ObterEmpresaPorCnpjDoGrupo(cte.CnpjFerrovia);
            return empresa;
        }

        /// <summary>
        /// Obt�m o tipo de trafego mutuo
        /// </summary>
        /// <returns>Retorna o tipo de trafego mutuo</returns>
        private TipoTrafegoMutuoEnum ObterTipoTrafegoMutuo()
        {
            TipoTrafegoMutuoEnum tipoTrafegoMutuo = TipoTrafegoMutuoEnum.Indefinido;
            if ((_contrato.FerroviaFaturamento == null) || (_fluxoComercial == null))
            {
                return tipoTrafegoMutuo;
            }

            // Obter a sigla da empresa que fatura
            string siglaFerrovia = ALL.Core.Util.Enum<FerroviaFaturamentoEnum>.GetDescriptionOf(_contrato.FerroviaFaturamento.Value);

            if (_contrato.FerroviaFaturamento.Value == FerroviaFaturamentoEnum.Argentina)
            {
                tipoTrafegoMutuo = TipoTrafegoMutuoEnum.FaturamentoArgentina;
                return tipoTrafegoMutuo;
            }

            IEmpresa empresaFerrovia = ObterEmpresaFerrovia(siglaFerrovia);

            if (empresaFerrovia == null)
            {
                return TipoTrafegoMutuoEnum.Indefinido;
            }

            if (empresaFerrovia.IndEmpresaALL == false)
            {
                if (_fluxoComercial.OrigemIntercambio != null)
                {
                    tipoTrafegoMutuo = TipoTrafegoMutuoEnum.IntercambioFaturamentoOrigem;
                }
                else if (_fluxoComercial.DestinoIntercambio != null)
                {
                    tipoTrafegoMutuo = TipoTrafegoMutuoEnum.IntercambioFaturamentoDestino;
                }
                else
                {
                    /* Regra: Quando a origem e destino s�o do grupo, mas que fatura � uma empresa fora do grupo */
                    EstacaoMae origem = _fluxoComercial.Origem;
                    EstacaoMae destino = _fluxoComercial.Destino;

                    if ((origem.EmpresaConcessionaria != null) && (destino.EmpresaConcessionaria != null))
                    {
                        if (origem.EmpresaConcessionaria.IndEmpresaALL.Value || destino.EmpresaConcessionaria.IndEmpresaALL.Value)
                        {
                            tipoTrafegoMutuo = TipoTrafegoMutuoEnum.FaturamentoEmpresaForaGrupo;
                        }
                    }
                    else
                    {
                        tipoTrafegoMutuo = TipoTrafegoMutuoEnum.Indefinido;
                    }
                }
            }
            else
            {
                tipoTrafegoMutuo = TipoTrafegoMutuoEnum.FaturamentoEmpresaGrupo;
            }

            return tipoTrafegoMutuo;
        }

        private void ObterCfopTrafegoMutuo()
        {
            string cfop = string.Empty;
            string cfopDescricao = string.Empty;

            string codigoInternacional = string.Empty;

            // Quando for cte de anula��o, gera fixo a partir do campo CfopAnulado do contrato 
            if (_cte.TipoCte == TipoCteEnum.Anulacao)
            {
                cfop = _contrato.CfopAnulacao;
                cfopDescricao = _contrato.DescCfopAnulacao;
            }
            else  // Trafego mutuo empresas do Grupo
                if (_tipoTrafegoMutuo == TipoTrafegoMutuoEnum.FaturamentoEmpresaGrupo || _tipoTrafegoMutuo == TipoTrafegoMutuoEnum.FaturamentoEmpresaForaGrupo)
            {
                if (_cte.Id != _cteRaizAgrupamento.Id)
                {
                    if (_cte.SiglaUfFerrovia.Equals(_cteRaizAgrupamento.SiglaUfFerrovia))
                    {
                        cfop = _cfopEstadual.Valor;
                        cfopDescricao = _cfopEstadual.Descricao;
                    }
                    else
                    {
                        cfop = _cfopInterestadual.Valor;
                        cfopDescricao = _cfopInterestadual.Descricao;
                    }
                }
                else
                {
                    cfop = _contrato.Cfop;
                    cfopDescricao = _contrato.DescricaoCfop;
                }
            }
            else if (_tipoTrafegoMutuo == TipoTrafegoMutuoEnum.IntercambioFaturamentoOrigem)
            {
                // Verifica se � um fluxo Internacional.
                codigoInternacional = VerificarFluxoInternacional(_fluxoComercial);

                // Caso retorne diferente de vazio ou nulo, ent�o � fluxo internacional.
                if (string.IsNullOrEmpty(codigoInternacional))
                {
                    if (_empresaFerroviaCliente.Estado.Sigla.Equals(_cte.SiglaUfFerrovia))
                    {
                        cfop = _cfopEstadual.Valor;
                        cfopDescricao = _cfopEstadual.Descricao;
                    }
                    else
                    {
                        cfop = _cfopInterestadual.Valor;
                        cfopDescricao = _cfopInterestadual.Descricao;
                    }
                }
                else
                {
                    cfop = _cfopInternacional.Valor;
                    cfopDescricao = _cfopInternacional.Descricao;
                }
            }
            else if (_tipoTrafegoMutuo == TipoTrafegoMutuoEnum.IntercambioFaturamentoDestino)
            {
                codigoInternacional = VerificarFluxoInternacional(_fluxoComercial);
                if (!string.IsNullOrEmpty(codigoInternacional))
                {
                    cfop = _cfopInternacional.Valor;
                    cfopDescricao = _cfopInternacional.Descricao;
                }
                else
                {
                    cfop = _contrato.Cfop;
                    cfopDescricao = _contrato.DescricaoCfop;
                }
            }
            else if (_tipoTrafegoMutuo == TipoTrafegoMutuoEnum.FaturamentoArgentina)
            {
                cfop = _cfopInternacional.Valor;
                cfopDescricao = _cfopInternacional.Descricao;
            }
            else
            {
                // Tipo Trafego mutuo indefinido
                _cfop = string.Empty;
                _cfopDescricao = string.Empty;
            }

            _cfop = cfop;
            _cfopDescricao = cfopDescricao;
        }

        /// <summary>
        /// Obtem o Valor do Seguro do vagao
        /// </summary>
        /// <returns>Valor do Seguro</returns>
        private double ObterValorSeguro()
        {
            // Todo: Validar se o seguro j� vem como decimal ou com valor inteiro (verificar se precisa dividir por 100)
            double prcSeguro = _contrato.PercentualSeguro ?? 0.0;
            prcSeguro = prcSeguro / 100;
            double valorSeguro = 0;

            /*
                                            1- PrcSeguro - no contrato
                                            Prcseg * valorNota (soma, percorrer os detalhes e pegar todas as notas)

                                            2 - PrcSeguro null ou 0 ou vazio
                                            composicaoFreteContrato
                                            pegar a condicaotarifa = 'ZSG' + letra da ferrovia
                                            valor com ICMs * Peso total vag�o
             */
            if (prcSeguro != 0)
            {
                foreach (CteDetalhe cteDetalhe in _listaCteDetalhe)
                {
                    valorSeguro += prcSeguro * cteDetalhe.ValorNotaFiscal;
                }
            }
            else
            {
                string letraFerrovia = _serieDespachoUf.LetraFerrovia;
                IList<ComposicaoFreteContrato> listaComposicaoFreteContrato = _composicaoFreteContratoRepository.ObterPorNumeroContrato(_contratoHistorico.NumeroContrato, _cteRaiz.DataHora, letraFerrovia, true);

                foreach (ComposicaoFreteContrato composicaoFreteContrato in listaComposicaoFreteContrato)
                {
                    if (composicaoFreteContrato.CondicaoTarifa.Equals("ZSG" + letraFerrovia))
                    {
                        double valorComIcms = composicaoFreteContrato.ValorComIcms != null ? composicaoFreteContrato.ValorComIcms.Value : 0;
                        double pesoVagaoCte = _cte.PesoVagao != null ? _cte.PesoVagao.Value : 0;
                        valorSeguro += valorComIcms * pesoVagaoCte;
                    }
                }
            }

            return valorSeguro;
        }

        /// <summary>
        /// Obtem os dados da empresa remetente fiscal
        /// </summary>
        private void ObterEmpresaRemetenteFiscal()
        {
            if ((_listaCteDetalhe == null) || (_listaCteDetalhe.Count == 0))
            {
                return;
            }

            IEmpresa empresaRemetente;
            int aux;

            // Verifica se � um Fluxo da Brado
            if (_cteService.VerificarLiberacaoTravasCorrentista(_fluxoComercial))
            {
                // Caso seja fluxo brado, ent�o recupera o REMETENTE e DESTINATARIO da nota fiscal.
                INotaFiscalEletronica nota = _nfeService.ObterDadosNfeBancoDados(_listaCteDetalhe[0].ChaveNfe);

                if (nota != null)
                {
                    // Configura o objeto Empresa Remetente
                    empresaRemetente = new EmpresaCliente();
                    empresaRemetente.TipoPessoa = TipoPessoaEnum.Juridica;
                    empresaRemetente.Cgc = nota.CnpjEmitente;
                    empresaRemetente.InscricaoEstadual = nota.InscricaoEstadualEmitente;
                    empresaRemetente.RazaoSocial = nota.RazaoSocialEmitente;
                    empresaRemetente.NomeFantasia = nota.NomeFantasiaEmitente;
                    empresaRemetente.Telefone1 = nota.TelefoneEmitente;
                    empresaRemetente.Endereco = nota.LogradouroEmitente;
                    empresaRemetente.Cep = nota.CepEmitente;

                    // Tenta pega pelo codigo do municipio, caso nao venha, entao recupera pelo nome do municipio
                    if (int.TryParse(nota.CodigoMunicipioEmitente, out aux))
                    {
                        empresaRemetente.CidadeIbge = _cidadeIbgeRepository.ObterPorCodigoMunicipioIbge(aux);
                        _cidadeIbgeRemetenteFiscal = empresaRemetente.CidadeIbge;
                        if (_cidadeIbgeRemetenteFiscal.CodigoIbge == 9999999)
                        {
                            PaisBacen paisAux = _paisBacenRepository.ObterPorCodigoPais(nota.CodigoPaisEmitente);
                            if (paisAux == null)
                            {
                                if (int.TryParse(nota.PaisEmitente, out aux))
                                {
                                    paisAux = _paisBacenRepository.ObterPorCodigoPais(nota.PaisEmitente);
                                }
                                else
                                {
                                    paisAux = _paisBacenRepository.ObterPorDescricaoPais(nota.PaisEmitente);
                                }
                            }

                            if (paisAux == null)
                            {
                                _empresaContratoRemetenteFiscal = null;
                            }
                            else
                            {
                                empresaRemetente.EnderecoSap = paisAux.SiglaResumida;
                            }
                        }
                    }
                    else
                    {
                        if (nota.MunicipioDestinatario != null)
                        {
                            empresaRemetente.CidadeIbge = _cidadeIbgeRepository.ObterPorDescricaoUf(nota.MunicipioDestinatario.ToUpper(), nota.UfDestinatario);
                            _cidadeIbgeRemetenteFiscal = empresaRemetente.CidadeIbge;
                        }
                        else
                        {
                            if (nota.UfDestinatario == "EX")
                            {
                                _cidadeIbgeRemetenteFiscal = _cidadeIbgeRepository.ObterPorCodigoMunicipioIbge(9999999);
                                empresaRemetente.CidadeIbge = _cidadeIbgeRemetenteFiscal;
                            }
                        }
                    }

                    _empresaContratoRemetenteFiscal = empresaRemetente;
                }
                else
                {
                    _empresaContratoRemetenteFiscal = _contrato.EmpresaRemetenteFiscal;
                    // _empresaContratoRemetenteFiscal = null;
                }
            }
            else
            {
                // Dados da empresa remetente fiscal
                _empresaContratoRemetenteFiscal = _contrato.EmpresaRemetenteFiscal;
                if (_empresaContratoRemetenteFiscal != null)
                {
                    _cidadeIbgeRemetenteFiscal = _empresaContratoRemetenteFiscal.CidadeIbge;
                }
            }
        }

        /// <summary>
        /// Obtem os dados da empresa destinataria fiscal
        /// </summary>
        private void ObterEmpresaDestinatariaFiscal()
        {
            if ((_listaCteDetalhe == null) || (_listaCteDetalhe.Count == 0))
            {
                return;
            }

            IEmpresa empresaDestinataria;
            int aux;

            // Verifica se � um Fluxo da Brado
            if (_cteService.VerificarLiberacaoTravasCorrentista(_fluxoComercial))
            {
                // Caso seja fluxo brado, ent�o recupera o REMETENTE e DESTINATARIO da nota fiscal.
                INotaFiscalEletronica nota = _nfeService.ObterDadosNfeBancoDados(_listaCteDetalhe[0].ChaveNfe);

                if (nota != null)
                {
                    // Configura o objeto Empresa Destinatario
                    empresaDestinataria = new EmpresaCliente();
                    empresaDestinataria.TipoPessoa = TipoPessoaEnum.Juridica;
                    empresaDestinataria.Cgc = nota.CnpjDestinatario;
                    empresaDestinataria.InscricaoEstadual = nota.InscricaoEstadualDestinatario;
                    empresaDestinataria.RazaoSocial = nota.RazaoSocialDestinatario;
                    empresaDestinataria.NomeFantasia = nota.NomeFantasiaDestinatario;
                    empresaDestinataria.Telefone1 = nota.TelefoneDestinatario;
                    empresaDestinataria.Endereco = nota.LogradouroDestinatario;
                    empresaDestinataria.Cep = nota.CepDestinatario;

                    // Tenta pega pelo codigo do municipio, caso nao venha, entao recupera pelo nome do municipio
                    if (int.TryParse(nota.CodigoMunicipioDestinatario, out aux))
                    {
                        empresaDestinataria.CidadeIbge = _cidadeIbgeRepository.ObterPorCodigoMunicipioIbge(aux);
                        _cidadeIbgeDestinatariaFiscal = empresaDestinataria.CidadeIbge;
                        if (_cidadeIbgeDestinatariaFiscal.CodigoIbge == 9999999)
                        {
                            PaisBacen paisAux = _paisBacenRepository.ObterPorCodigoPais(nota.CodigoPaisDestinatario);
                            if (paisAux == null)
                            {
                                if (int.TryParse(nota.PaisDestinatario, out aux))
                                {
                                    paisAux = _paisBacenRepository.ObterPorCodigoPais(nota.PaisDestinatario);
                                }
                                else
                                {
                                    paisAux = _paisBacenRepository.ObterPorDescricaoPais(nota.PaisDestinatario);
                                }
                            }

                            if (paisAux == null)
                            {
                                _empresaContratoDestinatariaFiscal = null;
                            }
                            else
                            {
                                empresaDestinataria.EnderecoSap = paisAux.SiglaResumida;
                            }
                        }
                    }
                    else
                    {
                        if (nota.MunicipioDestinatario != null)
                        {
                            empresaDestinataria.CidadeIbge = _cidadeIbgeRepository.ObterPorDescricaoUf(nota.MunicipioDestinatario.ToUpper(), nota.UfDestinatario);
                            _cidadeIbgeDestinatariaFiscal = empresaDestinataria.CidadeIbge;
                        }
                        else
                        {
                            if (nota.UfDestinatario == "EX")
                            {
                                _cidadeIbgeDestinatariaFiscal = _cidadeIbgeRepository.ObterPorCodigoMunicipioIbge(9999999);
                                empresaDestinataria.CidadeIbge = _cidadeIbgeDestinatariaFiscal;
                            }
                        }
                    }

                    _empresaContratoDestinatariaFiscal = empresaDestinataria;
                }
                else
                {
                    _empresaContratoDestinatariaFiscal = _contrato.EmpresaDestinatariaFiscal;
                }
            }
            else
            {
                _empresaContratoDestinatariaFiscal = _contrato.EmpresaDestinatariaFiscal;
            }
        }

        private IEmpresa ObterEmpresaFerrovia(string siglaFerrovia)
        {
            IEmpresa empresaFerrovia = _empresaFerroviaRepository.ObterPorSiglaUf(siglaFerrovia, _cteRaizAgrupamento.SiglaUfFerrovia);

            if (empresaFerrovia == null)
            {
                empresaFerrovia = _empresaInterfaceCteRepository.ObterPorSiglaUF(siglaFerrovia, _cteRaizAgrupamento.SiglaUfFerrovia);
            }

            // Pega pela esta��o de Origem 
            if (empresaFerrovia == null)
            {
                if (_contrato.FerroviaFaturamento.Value == FerroviaFaturamentoEnum.Argentina)
                {
                    empresaFerrovia = _fluxoComercial.Origem.EmpresaConcessionaria;
                }
            }

            return empresaFerrovia;
        }

        private IEmpresa ObterEmpresaFerrovia(int idEmpresa)
        {
            IEmpresa empresaFerrovia = _empresaFerroviaRepository.ObterPorId(idEmpresa);

            if (empresaFerrovia == null)
            {
                empresaFerrovia = _empresaInterfaceCteRepository.ObterPorId(idEmpresa);
            }

            return empresaFerrovia;
        }

        private bool PartilhaFerroeste()
        {
            return _partilhaFerroeste;
        }

        private bool OrigemFerroeste()
        {
            return _origemFerroeste;
        }

        private string ValidarTelefone(string telefone)
        {
            if (telefone == null)
            {
                return string.Empty;
            }

            string retorno = Tools.TruncateRemoveSpecialChar(telefone, 12, 7);

            Regex regex = new Regex(ALL.Core.Util.Enum<RegexSefaz>.GetDescriptionOf(RegexSefaz.ER36));
            Match match = regex.Match(retorno);

            retorno = match.Success ? match.Value : string.Empty;

            return retorno;
        }

        private string ObterMensagemSerieDespacho()
        {
            string retorno = string.Empty;
            retorno = "DCL:";
            retorno += _serieDespachoUf.CodigoControle;
            retorno += " ";

            if (_tipoTrafegoMutuo == TipoTrafegoMutuoEnum.IntercambioFaturamentoOrigem || PartilhaFerroeste())
            {
                string serieDesp5 = string.Empty;
                if (_despachoTranslogic.SerieDespacho != null)
                {
                    serieDesp5 = _despachoTranslogic.SerieDespacho.SerieDespachoNum;
                }

                string serie = string.Concat(_despachoTranslogic.SerieDespachoSdi ?? serieDesp5);
                serie = serie.PadLeft(3, '0');
                retorno += "S�RIE:";
                retorno += serie;
                retorno += " N�M:";
                retorno += string.Concat(_despachoTranslogic.NumDespIntercambio ?? _despachoTranslogic.NumeroDespacho);
            }
            else
            {
                if (_despachoTranslogic.SerieDespachoUf != null)
                {
                    string serie = string.Concat(_despachoTranslogic.SerieDespachoUf.NumeroSerieDespacho);
                    serie = serie.PadLeft(3, '0');
                    retorno += "S�RIE:";
                    retorno += serie;
                    retorno += " N�M:";
                    retorno += string.Concat(_despachoTranslogic.NumeroDespachoUf);
                }
                else
                {
                    if (_despachoTranslogic.SerieDespacho != null)
                    {
                        string serie = string.Concat(_despachoTranslogic.SerieDespacho.SerieDespachoNum);
                        serie = serie.PadLeft(3, '0');
                        retorno += "S�RIE:";
                        retorno += serie;
                        retorno += " N�M:";
                        retorno += string.Concat(_despachoTranslogic.NumeroDespacho);
                    }
                }
            }

            retorno += " DATA:" + String.Format("{0:dd/MM/yyyy}", _despachoTranslogic.DataDespacho);

            return retorno;
        }

        private string ObterMensagemVencimento()
        {
            string retorno = string.Empty;

            if (_cte.Vencimento != null)
            {
                retorno = "VENC:" + String.Format("{0:dd/MM/yyyy}", _cte.Vencimento.Value);
                retorno += " ??";
            }

            return retorno;
        }

        private string ObterMensagemPisCofins()
        {
            var chave = _configuracaoTranslogicRepository.ObterPorId(_chaveExibirPisCofins);
            string retorno = string.Empty;

            if (chave.Valor == "S")
            {
                // Calculo Cofins
                var percCofins = _contratoHistorico.PercentualCofins ?? 0.0;
                var valorCofins = Math.Round(_cte.ValorCte * (percCofins / 100), 2, MidpointRounding.AwayFromZero);

                // Calculo Pis
                var percPis = _contratoHistorico.PercentualPis ?? 0.0;
                var valorPis = Math.Round(_cte.ValorCte * (percPis / 100), 2, MidpointRounding.AwayFromZero);

                // Calculo Cprb
                var percCprb = _contratoHistorico.PercentualCprb ?? 0.0;

                try
                {
                    // Nova valida��o para os novos percentuais de cprb por ferrovia (in�cio em 01/11/2015)
                    var controleCprb = _controleCpbrRepository.ObterTodos().First(c => c.CnpjFerrovia == _cteRaizAgrupamento.CnpjFerrovia);
                    if (controleCprb != null)
                    {
                        // Verifica 
                        // se o cte � de refaturamento
                        // se a data do refaturamento(cte raiz) � anterior a nova vigencia
                        // se a data atual j� est� dentro da nova vigencia do tributo
                        // se o controle do tributo est� ativo 
                        // se o percentual que veio do contrato � diferente do novo cadastrado no controle
                        if (_cte.Id != _cteRaiz.Id && _cteRaiz.DataHora < controleCprb.InicioVigencia && DateTime.Now > controleCprb.InicioVigencia && controleCprb.FlagAtivo.Equals("S") && percCprb != Convert.ToDouble(controleCprb.PercentualTributo))
                        {
                            percCprb = Convert.ToDouble(controleCprb.PercentualTributo);
                        }
                    }
                }
                catch
                {
                    // Controle tempor�rio, remover ap�s valida��es dos novos fluxos
                }

                var valorCprb = Math.Round(_cte.ValorCte * (percCprb / 100), 2, MidpointRounding.AwayFromZero);

                var valorImpostoRetido = valorCprb + valorPis + valorCofins + _cte.ValorIcms;
                if (valorImpostoRetido > 0)
                {
                    retorno = " ??";
                    retorno += string.Format(new CultureInfo("pt-BR"), "VALOR APROXIMADO DOS IMPOSTOS {0:C2}", valorImpostoRetido);
                    retorno += " ??";
                }
            }

            return retorno;
        }

        private string ObterMensagemCteSubstituto()
        {
            string retorno = string.Empty;

            // Verifica se � um Cte de complemento ou um Cte de TakeOrPay. Caso tenha apenas um item na lista � um Cte de complemento
            if (_cte.TipoCte == TipoCteEnum.Substituto)
            {
                IList<CteArvore> arvoreCte = _cteArvoreRepository.ObterArvorePorCteFilho(_cte);

                var cteSubstituto = arvoreCte.SingleOrDefault(f => f.CteFilho.Id != null && f.CteFilho.Id.Value.Equals(_cte.Id));
                retorno = "CHAVE CT-e SUBSTITUIDO:??";

                if (cteSubstituto != null)
                {
                    retorno += cteSubstituto.CtePai.Chave;
                }

                retorno += "??";
            }

            return retorno;
        }

        private string ObterMensagemNotasFiscais()
        {
            string retorno = string.Empty;
            foreach (CteDetalhe detalhe in _listaCteDetalhe)
            {
                if (!string.IsNullOrEmpty(detalhe.ChaveNfe))
                {
                    retorno += "??";
                    retorno += "N�M NFE:";
                    retorno += detalhe.NumeroNota.Trim();
                    retorno += " S�R:";
                    retorno += detalhe.SerieNota;
                    retorno += " ";
                }
            }

            return retorno;
        }

        private string ObterMensagemCteAnulando()
        {
            return string.Format("Chave CT-e ANULADO: {0} {1}", _cteAnulado.Cte.Chave, "CTE DE ANULACAO REFERENTE A ANULACAO DE VALORES");
        }

        private void GravarInformacoesEnvioXml(IEmpresa empresa)
        {
            try
            {
                // Ambiente Sefaz 1 - Produ��o e 2 - Homologa��o
                if (_cte.AmbienteSefaz == 1)
                {
                    string[] emailsNfe = ParserEmail(empresa.EmailNfe);
                    string[] emailsXml = ParserEmail(empresa.EmailXml);

                    // Remove itens por Cte
                    _cteEnvioXmlRepository.RemoverPorCte(_cte);

                    // Cria a lista de emails que envia os arquivos Pdf e Xml
                    // Encontra os emails comuns
                    var emailsArquivoPdfXml = emailsNfe.Select(g => g.Trim()).Intersect(emailsXml.Select(s => s.Trim()));
                    foreach (string email in emailsArquivoPdfXml)
                    {
                        CteEnvioXml envioXml = new CteEnvioXml();
                        envioXml.Cte = _cte;
                        envioXml.EnvioPara = email;
                        envioXml.TipoEnvioCte = TipoEnvioCteEnum.Email;
                        envioXml.TipoArquivoEnvio = TipoArquivoEnvioEnum.PdfXml;
                        envioXml.Ativo = true;
                        _cteEnvioXmlRepository.Inserir(envioXml);
                    }

                    // Cria a lista de emails que envia apenas o pdf
                    var emailsArquivoPdf = emailsNfe.Except(emailsXml);
                    foreach (string email in emailsArquivoPdf)
                    {
                        CteEnvioXml envioXml = new CteEnvioXml();
                        envioXml.Cte = _cte;
                        envioXml.EnvioPara = email;
                        envioXml.TipoEnvioCte = TipoEnvioCteEnum.Email;
                        envioXml.TipoArquivoEnvio = TipoArquivoEnvioEnum.Pdf;
                        envioXml.Ativo = true;
                        _cteEnvioXmlRepository.Inserir(envioXml);
                    }

                    // Cria a lista de emails que envia apenas o pdf
                    var emailsArquivoXml = emailsXml.Except(emailsNfe);
                    foreach (string email in emailsArquivoXml)
                    {
                        CteEnvioXml envioXml = new CteEnvioXml();
                        envioXml.Cte = _cte;
                        envioXml.EnvioPara = email;
                        envioXml.TipoEnvioCte = TipoEnvioCteEnum.Email;
                        envioXml.TipoArquivoEnvio = TipoArquivoEnvioEnum.Xml;
                        envioXml.Ativo = true;
                        _cteEnvioXmlRepository.Inserir(envioXml);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private string[] ParserEmail(string emails)
        {
            if (String.IsNullOrEmpty(emails))
            {
                return new string[0];
            }

            string mask = ";,: ";
            return emails.Split(mask.ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
        }

        /// <summary>
        /// Obtem a sigla do pais pelo SAP
        /// </summary>
        /// <param name="empresa">Empresa utilizada para se obter o Pais</param>
        /// <returns>Retorna a sigla do Pais</returns>
        private string ObterSiglaPaisExteriorPeloSap(IEmpresa empresa)
        {
            if (empresa.EnderecoSap != null)
            {
                string mask = "- ";
                string linhaPais = empresa.EnderecoSap;
                string[] dados = linhaPais.Split(mask.ToCharArray());
                return dados[dados.Length - 1];
            }

            if (empresa.CidadeIbge != null)
            {
                CidadeIbge cidadeIbge = empresa.CidadeIbge;
                return cidadeIbge.SiglaPais;
            }

            return string.Empty;
        }

        private double ObterValorCotacao()
        {
            double? valorCotacao = 1.0;
            string unidade = _contratoHistorico.UnidadeMonetaria ?? string.Empty;

            if (!string.IsNullOrEmpty(unidade))
            {
                try
                {
                    var data = _despachoTranslogicRepository.ObterDataDespachoPorId(_despachoTranslogic.Id ?? 0);
                    if (data != null)
                    {
                        var cotacao = _cotacaoRepository.ObterCotacaoPorData(data ?? DateTime.Now, unidade);
                        if (cotacao != null)
                        {
                            valorCotacao = cotacao.Valor;
                        }
                    }
                }
                catch (Exception)
                {
                    valorCotacao = 1.0;
                }
            }

            return valorCotacao ?? 1.0;
        }

        /// <summary>
        /// Calcula a Subistitui��o Tribut�ria para os Ctes do Mato Grosso do Sul
        /// </summary>
        /// <returns>Retorna uma string com os valores da substitui��o</returns>
        private string CalcularObservacaoMS()
        {
            string retorno = string.Empty;

            double aliquotaSt = _contratoHistorico.AliquotaIcmsSubstProduto ?? 0.0;
            if (aliquotaSt > 0.0)
            {
                double baseCalculoIcms = _cte.BaseCalculoIcms ?? 0.0;
                double valorImpostoRetido = Math.Round(baseCalculoIcms * (aliquotaSt / 100), 2);
                valorImpostoRetido *= 0.8;
                double desconto = _cte.DescontoPorPeso ?? 0.0;
                desconto += _cte.DescontoPorPercentual ?? 0.0;
                if (desconto < 0)
                {
                    desconto *= -1;
                }

                desconto = Math.Round(desconto, 2);
                double valorLiquido = Math.Round(baseCalculoIcms - valorImpostoRetido - desconto, 2);

                retorno = string.Format(new CultureInfo("pt-BR"), "??BASE DE CALCULO {0:C2} / VALOR DO IMPOSTO RETIDO {1:C2} / DESCONTO {2:C2} / VALOR LIQUIDO A PAGAR {3:C2}", baseCalculoIcms, valorImpostoRetido, desconto, valorLiquido);
                retorno += " ??";
                retorno += string.Format("O ICMS  ser� recolhido pelo remetente da mercadoria.");
            }

            return retorno;
        }

        /// <summary>
        /// Calcula o valor liquido quando houver Substitui��o tributaria.
        /// valorLiquido = Math.Round(baseCalculoIcms - valorImpostoRetido - desconto, 2);
        /// </summary>
        /// <returns>Retorna o valor liquido do Cte quando houver substitui��o tributaria</returns>
        private double CalcularValorLiquidoSubstituicaoTributaria()
        {
            double valorLiquido = 0.0;
            double aliquotaSt = _contratoHistorico.AliquotaIcmsSubstProduto ?? 0.0;

            if (aliquotaSt > 0.0)
            {
                double baseCalculoIcms = _cte.BaseCalculoIcms ?? 0.0;
                double valorImpostoRetido = Math.Round(baseCalculoIcms * (aliquotaSt / 100), 2);
                double desconto = _cte.DescontoPorPeso ?? 0.0;
                desconto += _cte.DescontoPorPercentual ?? 0.0;

                if (desconto < 0)
                {
                    desconto *= -1;
                }

                desconto = Math.Round(desconto, 2);
                valorLiquido = Math.Round(baseCalculoIcms - valorImpostoRetido - desconto, 2);
                // Grava na variavel para mandar para o sap posteriormente
                _valorImpostoRetidoSubstTributaria = valorImpostoRetido;
            }

            return valorLiquido;
        }

        /// <summary>
        /// Verifica se a empresa � Brado
        /// </summary>
        /// <param name="empresa">Empresa para ser verificada</param>
        /// <returns>Retorna verdadeiro caso seja a empresa Brado, falso caso contr�rio</returns>
        private bool VerificaEmpresaBrado(IEmpresa empresa)
        {
            if (empresa == null)
            {
                return false;
            }

            string descricao = empresa.DescricaoResumida.ToUpper();
            return descricao.IndexOf("BRADO") > -1 ? true : false;
        }

        private bool ObterDadosDCL(out string serie, out string numero, out string data)
        {
            bool retorno = true;
            serie = string.Empty;
            numero = string.Empty;
            data = string.Empty;

            if (_tipoTrafegoMutuo == TipoTrafegoMutuoEnum.IntercambioFaturamentoOrigem || PartilhaFerroeste())
            {
                string serieDesp5 = string.Empty;
                if (_despachoTranslogic.SerieDespacho != null)
                {
                    serieDesp5 = _despachoTranslogic.SerieDespacho.SerieDespachoNum;
                }

                serie = string.Concat(_despachoTranslogic.SerieDespachoSdi ?? serieDesp5);
                numero = string.Concat(_despachoTranslogic.NumDespIntercambio ?? _despachoTranslogic.NumeroDespacho);
            }
            else
            {
                if (_despachoTranslogic.SerieDespachoUf != null)
                {
                    serie = string.Concat(_despachoTranslogic.SerieDespachoUf.NumeroSerieDespacho);
                    numero = string.Concat(_despachoTranslogic.NumeroDespachoUf);
                }
                else
                {
                    if (_despachoTranslogic.SerieDespacho != null)
                    {
                        serie = string.Concat(_despachoTranslogic.SerieDespacho.SerieDespachoNum);
                        numero = string.Concat(_despachoTranslogic.NumeroDespacho);
                    }
                }
            }

            if (string.IsNullOrEmpty(serie.Trim()) || string.IsNullOrEmpty(numero.Trim()))
            {
                retorno = false;
            }

            serie = serie.PadLeft(3, '0');
            data = String.Format("{0:dd/MM/yyyy}", _despachoTranslogic.DataDespacho);

            return retorno;
        }

        /// <summary>
        /// H� uma exce��o no c�lculo de partilha da Ferroeste.
        /// Se a propriet�ria do vag�o for Ferroeste (S ou M) e o cliente de faturamento (tomador) for 7230 ou 7875(cliente Bunge � desprezando o 5� d�gito), 
        /// o valor da partilha referente � propriet�ria do vag�o (15%) deve ser considerado para a ALL.
        /// Isso se deve a um acordo feito entre Bunge, ALL e Ferroeste, onde a Bunge afirmou que n�o tem como controlar o 
        /// c�lculo dos valores, utilizando a propriet�ria do vag�o, onde acertaram de deixar 
        /// esse valor sempre para a ALL e depois a ALL faz o acerto com a Ferroeste.
        /// </summary>
        /// <returns>Retorna true caso seja acordo com a Bunge e false caso contrario</returns>
        private bool RegraFerroesteBunge()
        {
            string siglaEmpresa = _empresaTomadora.Sigla;
            if (!string.IsNullOrEmpty(siglaEmpresa))
            {
                if (siglaEmpresa.Length >= 4)
                {
                    // Pela sigla da empresa verifica se � a Bunge 7230 e 7875
                    if ((siglaEmpresa.Substring(0, 4) == "7230") || (siglaEmpresa.Substring(0, 4) == "7875"))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        private string ObterMensagemDesconto()
        {
            string retorno = string.Empty;
            double descontoPorPercentual = _cte.DescontoPorPercentual ?? 0.0;
            double descontoPorTonelada = _cte.ValorDescontoTonelada ?? 0.0;
            double valorDesconto = descontoPorPercentual + descontoPorTonelada;

            if (valorDesconto != 0)
            {
                retorno = " ??";
                retorno += "DESCONTO:" + Math.Abs(valorDesconto);
                retorno += " ??";
                retorno += "VALOR A RECEBER(R$" + _cte.ValorCte + "-R$" + Math.Abs(valorDesconto) + "): R$" + _cte.ValorReceber;
                retorno += " ??";
            }

            return retorno;
        }

        private string ObterMensagemDocumentoCliente()
        {
            string retorno = string.Empty;
            IList<MensagemRecebimentoVagao> mensagem = _mensagemRecebimentoVagaoRepository.ObterPorDespacho(_despachoTranslogic.Id.Value);

            if (mensagem != null && mensagem.Count > 0)
            {
                var clientId = mensagem.FirstOrDefault(x => !string.IsNullOrEmpty(x.ClientReferenceId));

                if (clientId != null && !string.IsNullOrEmpty(clientId.ClientReferenceId))
                {
                    retorno = " ??";
                    retorno += "Documento do Cliente:" + clientId.ClientReferenceId;
                    retorno += " ??";
                }
            }

            return retorno;
        }

        /// <summary>
        /// Tabela de informacoes dos documentos (NF, Nfe e Outros) (1-N) - ICTE06_EMIT_INF
        /// </summary>
        public bool ValidarInformacoesDocumentos_Cte1_04()
        {
            try
            {
                if (_cte.Versao.CodigoVersao.Equals("2.00") || _cte.Versao.CodigoVersao.Equals("3.00"))
                {
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                LogHelper.Log(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
                throw;
            }
        }

        /// <summary>
        /// Tabela de informacoes dos documentos (NF, Nfe e Outros) (1-N) - ICTE06_EMIT_INF
        /// </summary>
        private void GravarInformacoesDocumentos_Cte1_04()
        {
            try
            {
                int intAux;
                int numSequencialNota = 0;
                long cnpj;
                double baseCalculoIcms = 0.0;
                double aliquota = 0.0;
                double valorTotalIcms = 0.0;
                double baseCalculoIcmsSt = 0.0;
                double aliquotaSt = 0.0;
                double valorTotalIcmsSt = 0.0;

                foreach (CteDetalhe cteDetalhe in _listaCteDetalhe)
                {
                    Cte06 cte06 = new Cte06();
                    // Recupera os dados para calculo do ICMS
                    baseCalculoIcms = _cte.BaseCalculoIcms ?? 0.0;
                    valorTotalIcms = _cte.ValorIcms ?? 0.0;

                    baseCalculoIcmsSt = _contratoHistorico.ValorBaseCalculoSubstituicaoProduto ?? 0.0;
                    valorTotalIcmsSt = baseCalculoIcmsSt * aliquotaSt;

                    // Codigo interno da filial (PK)
                    cte06.CfgUn = ObterIdentificadorFilial();

                    // Numero do documento CT-e (PK)
                    int.TryParse(_cte.Numero, out intAux);
                    cte06.CfgDoc = intAux;

                    // Serie do documento CT-e (PK)
                    // cte06.CfgSerie = _cte.Serie;
                    cte06.CfgSerie = int.Parse(_cte.Serie).ToString();

                    // Numero sequecial da tabela (PK)
                    cte06.IdEmitInf = ++numSequencialNota;

                    // CNPJ do local de retirada
                    if (Int64.TryParse(_empresaPagadoraContrato.Cgc, out cnpj))
                    {
                        cte06.LocRetCnpj = cnpj;
                    }

                    // Cpf local de retirada
                    // cte06.LocRetCpf = null;

                    if (!string.IsNullOrEmpty(_empresaPagadoraContrato.RazaoSocial))
                    {
                        // Nome local de retirada
                        cte06.LocRetXnome = _empresaPagadoraContrato.RazaoSocial.Trim();
                    }

                    if (!string.IsNullOrEmpty(_empresaPagadoraContrato.Endereco))
                    {
                        // Logradouro do local de retirada
                        cte06.LocRetXlgr = _empresaPagadoraContrato.Endereco.Trim();
                    }

                    // N�mero
                    // cte06.LocRetNro = string.Empty;
                    cte06.LocRetNro = "0";

                    // Complemento do endere�o
                    cte06.LocRetXcpl = string.Empty;

                    // Bairro do local de retirada 
                    cte06.LocRetXbairro = "NAO INFORMADO";

                    // C�digo IBGE do munic�pio do local de retirada
                    cte06.LocRetCmun = _cidadeIbgeEmpresaPagadoraContrato.CodigoIbge;
                    if (_cidadeIbgeEmpresaPagadoraContrato.CodigoIbge == 9999999)
                    {
                        // Nome do munic�pio do local de retirada
                        cte06.LocRetXmun = "EXTERIOR";
                        // C�digo UF do local de retirada
                        cte06.LocRetUf = "EX";
                    }
                    else
                    {
                        // Nome do munic�pio do local de retirada
                        cte06.LocRetXmun = _cidadeIbgeEmpresaPagadoraContrato.Descricao;
                        // C�digo UF do local de retirada
                        cte06.LocRetUf = _cidadeIbgeEmpresaPagadoraContrato.SiglaEstado;
                    }

                    // Chave de acesso da NFE
                    if (!string.IsNullOrEmpty(cteDetalhe.ChaveNfe))
                    {
                        cte06.InfNfeChave = cteDetalhe.ChaveNfe;

                        // PIN atribu�do pela SUFRAMA para a opera��o
                        cte06.InfNfePin = null;

                        // Modelo da Nota Fiscal
                        if (int.TryParse(_fluxoComercial.ModeloNotaFiscal, out intAux))
                        {
                            cte06.InfNfMod = intAux;
                        }
                    }
                    else
                    {
                        if (cteDetalhe.SerieNota == null)
                        {
                            cteDetalhe.SerieNota = string.Empty;
                        }

                        int serie;

                        // if ((cteDetalhe.SerieNota.ToUpper() != "V2") && (cteDetalhe.SerieNota.ToUpper() != "OU"))
                        if (int.TryParse(cteDetalhe.SerieNota.ToUpper(), out serie) && !_fluxoComercial.ModeloNotaFiscal.ToUpper().Equals("99"))
                        {
                            // N�mero do romaneio
                            // cte06.InfNfNroma = null;

                            // N�mero do pedido da NF
                            // cte06.InfNfNped = null;

                            // S�rie da nota
                            cte06.InfNfSerie = Tools.TruncateRemoveSpecialChar(cteDetalhe.SerieNota, 3);

                            // N�mero da nota
                            cte06.InfNfNdoc = cteDetalhe.NumeroNota.Trim();

                            // Data da emiss�o da nota
                            cte06.InfNfDemi = cteDetalhe.DataNotaFiscal;

                            // Valor da base para c�lculo do ICMS
                            cte06.InfNfVbc = baseCalculoIcms;

                            // Valor do ICMS
                            cte06.InfNfVicms = valorTotalIcms;

                            // Valor da base de c�lculo de substitui��o
                            cte06.InfNfVbcst = baseCalculoIcmsSt;

                            // Valor total do ICMS de substitui��o calculado como vBCST * ALIQICMSST da tabela CONTRATO.
                            cte06.InfNfVst = valorTotalIcmsSt;

                            // Valor total dos produtos
                            cte06.InfNfVprod = cteDetalhe.ValorNotaFiscal;

                            // Valor total da nota
                            // cte06.InfNfVnf = CalcularValorTotalNotaFiscal();

                            // CFOP predominante
                            intAux = 0;
                            cte06.InfNfNcfop = null;
                            if (_contratoHistorico.CfopProduto != null)
                            {
                                if (int.TryParse(_contratoHistorico.CfopProduto, out intAux))
                                {
                                    cte06.InfNfNcfop = intAux;
                                }
                            }
                            else
                            {
                                if (int.TryParse(_contratoHistorico.Cfop, out intAux))
                                {
                                    cte06.InfNfNcfop = intAux;
                                }
                            }

                            // Peso
                            cte06.InfNfNpeso = cteDetalhe.PesoNotaFiscal;
                            cte06.InfNfMod = 1;
                        }
                        else
                        {
                            // Tipo do documento outros
                            // Utilizado nos casos da S�rie ser V2 ou OU
                            cte06.InfOutrosTpDoc = 99;

                            // Descri��o do documento
                            cte06.InfOutrosDescOutros = "OUTROS";

                            // N�mero do documento
                            cte06.InfOutrosNdoc = cteDetalhe.NumeroNota.Trim();

                            // Data da emiss�o do documento
                            cte06.InfOutrosDemi = cteDetalhe.DataNotaFiscal;

                            // Valor do documento
                            cte06.InfOutrosVdocFisc = cteDetalhe.ValorNotaFiscal;

                            // Modelo da Nota Fiscal
                            if (int.TryParse(_fluxoComercial.ModeloNotaFiscal, out intAux))
                            {
                                cte06.InfNfMod = intAux;
                            }
                        }
                    }

                    // Inseri o registro
                    _cte06Repository.Inserir(cte06);
                }
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(_cte, "GravarConfigCteEnvioService", string.Format("GravarInformacoesDocumentos: {0}", ex.Message), ex);
                LogHelper.Log(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
                throw;
            }
        }

        private void GravarDocumentoNf(CteDetalhe cteDetalhe, int seq)
        {
            Cte100 cte100 = new Cte100();
            Cte103 cte103 = new Cte103();

            int intAux;
            double baseCalculoIcms = 0.0;
            double valorTotalIcms = 0.0;
            double baseCalculoIcmsSt = 0.0;
            double aliquotaSt = 0.0;
            double valorTotalIcmsSt = 0.0;

            // Recupera os dados para calculo do ICMS
            baseCalculoIcms = _cte.BaseCalculoIcms ?? 0.0;
            valorTotalIcms = _cte.ValorIcms ?? 0.0;

            baseCalculoIcmsSt = _contratoHistorico.ValorBaseCalculoSubstituicaoProduto ?? 0.0;
            valorTotalIcmsSt = baseCalculoIcmsSt * aliquotaSt;

            // Numero do documento CT-e (PK)
            int.TryParse(_cte.Numero, out intAux);
            cte100.CfgDoc = intAux;
            // S�rie
            cte100.CfgSerie = int.Parse(_cte.Serie).ToString();
            // Unidade
            cte100.CfgUn = ObterIdentificadorFilial();

            cte100.IdIcnInfNf = seq;

            // N�mero do romaneio
            cte100.InfNfNroma = null;

            // N�mero do pedido da NF
            cte100.InfNfNped = null;

            // S�rie da nota
            cte100.InfNfSerie = Tools.TruncateRemoveSpecialChar(cteDetalhe.SerieNota, 3);

            // N�mero da nota
            cte100.InfNfNdoc = cteDetalhe.NumeroNota.Trim();

            // Data da emiss�o da nota
            cte100.InfNfDemi = cteDetalhe.DataNotaFiscal;

            // Valor da base para c�lculo do ICMS
            cte100.InfNfVbc = baseCalculoIcms;

            // Valor do ICMS
            cte100.InfNfVicms = valorTotalIcms;

            // Valor da base de c�lculo de substitui��o
            cte100.InfNfVbcst = baseCalculoIcmsSt;

            // Valor total do ICMS de substitui��o calculado como vBCST * ALIQICMSST da tabela CONTRATO.
            cte100.InfNfVst = valorTotalIcmsSt;

            // Valor total dos produtos
            cte100.InfNfVprod = cteDetalhe.ValorNotaFiscal;

            // Valor total da nota
            cte100.InfNfVnf = CalcularValorTotalNotaFiscal();

            // CFOP predominante
            cte100.InfNfNcfop = null;

            if (_contratoHistorico.CfopProduto != null)
            {
                if (int.TryParse(_contratoHistorico.CfopProduto, out intAux))
                {
                    cte100.InfNfNcfop = intAux;
                }
            }
            else
            {
                if (int.TryParse(_contratoHistorico.Cfop, out intAux))
                {
                    cte100.InfNfNcfop = intAux;
                }
            }

            // Peso
            cte100.InfNfNpeso = cteDetalhe.PesoNotaFiscal;

            // Modelo da Nota Fiscal
            cte100.InfNfMod = 1;

            /*
              if (int.TryParse(_fluxoComercial.ModeloNotaFiscal, out intAux))
            {
                cte100.InfNfMod = intAux;
            } */

            cte103.IdIcnInf = seq;
            cte103.IdInfUnidTransp = 1;

            PreencherDadosTransporte(cteDetalhe, cte103);

            // Inseri o registro
            _cte100Repository.Inserir(cte100);
            _cte103Repository.Inserir(cte103);

            if (!string.IsNullOrEmpty(cteDetalhe.ConteinerNotaFiscal))
            {
                Cte105 cte105 = new Cte105();
                cte105.IdIcnInf = seq;
                cte105.IdInfUnidTransp = 1;
                PreencherDadosCarga(cteDetalhe, cte105);

                _cte105Repository.Inserir(cte105);
            }
        }

        private void GravarDocumentoNfe(CteDetalhe cteDetalhe, int seq)
        {
            Cte101 cte101 = new Cte101();
            Cte107 cte107 = new Cte107();
            int intAux;

            // Numero do documento CT-e (PK)
            int.TryParse(_cte.Numero, out intAux);
            cte101.CfgDoc = intAux;
            // S�rie
            cte101.CfgSerie = int.Parse(_cte.Serie).ToString();
            // Unidade
            cte101.CfgUn = ObterIdentificadorFilial();

            cte101.IdIcnInfNfe = seq;

            cte101.InfNfeChave = cteDetalhe.ChaveNfe;

            // PIN atribu�do pela SUFRAMA para a opera��o
            cte101.InfNfePin = null;

            cte107.IdIcnInf = seq;
            cte107.IdInfUnidTransp = 1;

            PreencherDadosTransporte(cteDetalhe, cte107);

            // Inseri o registro
            _cte101Repository.Inserir(cte101);
            _cte107Repository.Inserir(cte107);

            int seqCont = 1;
            foreach (var detalhe in _listaCteDetalhe.Where(c => c.ChaveNfe == cteDetalhe.ChaveNfe))
            {
                if (!string.IsNullOrEmpty(detalhe.ConteinerNotaFiscal))
                {
                    Cte109 cte109 = new Cte109();
                    cte109.IdIcnInf = seq;
                    cte109.IdInfUnidTransp = 1;
                    PreencherDadosCarga(detalhe, cte109);

                    cte109.IdInfUnidCarga = seqCont;

                    _cte109Repository.Inserir(cte109);
                }

                seqCont++;
            }
        }

        private void GravarDocumentosOutros(CteDetalhe cteDetalhe, int seq)
        {
            Cte102 cte102 = new Cte102();
            Cte111 cte111 = new Cte111();

            int intAux;

            // Numero do documento CT-e (PK)
            int.TryParse(_cte.Numero, out intAux);
            cte102.CfgDoc = intAux;
            // S�rie
            cte102.CfgSerie = int.Parse(_cte.Serie).ToString();
            // Unidade
            cte102.CfgUn = ObterIdentificadorFilial();

            cte102.IdIcnInfOutros = seq;

            // Utilizado nos casos da S�rie ser V2 ou OU
            cte102.InfOutrosTpDoc = 99;

            // Descri��o do documento
            cte102.InfOutrosDescOutros = "OUTROS";

            // N�mero do documento
            cte102.InfOutrosNdoc = cteDetalhe.NumeroNota.Trim();

            // Data da emiss�o do documento
            cte102.InfOutrosDemi = cteDetalhe.DataNotaFiscal;

            // Valor do documento
            cte102.InfOutrosVdocFisc = cteDetalhe.ValorNotaFiscal;

            cte111.IdIcnInf = seq;
            cte111.IdInfUnidTransp = 1;

            PreencherDadosTransporte(cteDetalhe, cte111);

            // Inseri o registro
            _cte102Repository.Inserir(cte102);
            _cte111Repository.Inserir(cte111);

            if (!string.IsNullOrEmpty(cteDetalhe.ConteinerNotaFiscal))
            {
                Cte113 cte113 = new Cte113();
                cte113.IdIcnInf = seq;
                cte113.IdInfUnidTransp = 1;
                PreencherDadosCarga(cteDetalhe, cte113);

                _cte113Repository.Inserir(cte113);
            }
        }

        private void PreencherDadosCarga(CteDetalhe cteDetalhe, IInfUnidCarga unidCarga)
        {
            int intAux;
            int.TryParse(_cte.Numero, out intAux);
            unidCarga.CfgDoc = intAux;
            // S�rie
            unidCarga.CfgSerie = int.Parse(_cte.Serie).ToString();
            // Unidade
            unidCarga.CfgUn = ObterIdentificadorFilial();

            unidCarga.IdInfUnidCarga = 1;

            unidCarga.IdUnidCarga = cteDetalhe.ConteinerNotaFiscal;
            /*
                1 - Container
                2 - ULD
                3 - Pallet
                4 - Outros
             */
            unidCarga.TpUnidCarga = 1;
            unidCarga.QtdRat = cteDetalhe.PesoNotaFiscal;
        }

        private void PreencherDadosTransporte(CteDetalhe cteDetalhe, IInfUnidTransp unidTransp)
        {
            int intAux;
            int.TryParse(_cte.Numero, out intAux);
            unidTransp.CfgDoc = intAux;
            // S�rie
            unidTransp.CfgSerie = int.Parse(_cte.Serie).ToString();
            // Unidade
            unidTransp.CfgUn = ObterIdentificadorFilial();

            unidTransp.IdInfUnidTransp = 1;

            // 1 - Rodovi�rio Tra��o 2 - Rodovi�rio Reboque 3 - Navio 4 - Balsa 5 - Aeronave 6 - Vag�o 7 - Outros
            unidTransp.TpUnidTransp = 6;
            unidTransp.IdUnidTransp = int.Parse(cteDetalhe.Cte.Vagao.Codigo);
            unidTransp.QtdRat = _listaCteDetalhe.Where(c => c.ChaveNfe == cteDetalhe.ChaveNfe).Sum(s => s.PesoNotaFiscal); //cteDetalhe.PesoNotaFiscal;
        }


        /// <summary>
        /// Obt�m o numero e s�rie do despacho para os Ctes TakeOrPay ou Complementos - Utilizado no cancelamento
        /// </summary>
        /// <param name="chaveCte">N�mero da chave do Cte</param>
        /// <param name="serieDespachoSap">S�rie do despacho gerado pelo SAP</param>
        /// <param name="numeroDespachoSap">Numero do despacho gerado pelo SAP</param>
        private void ObterNumeroSerieDespachoGeradoPeloSap(string chaveCte, out string serieDespachoSap, out int numeroDespachoSap)
        {
            serieDespachoSap = string.Empty;
            numeroDespachoSap = 0;

            var sapChaveCteAux = _sapChaveCteRepository.ObterPorChaveCte(chaveCte);
            if (sapChaveCteAux != null)
            {
                serieDespachoSap = sapChaveCteAux.SapDespCte.SerieDespachoSap;
                numeroDespachoSap = sapChaveCteAux.SapDespCte.NumeroDespachoSap;
            }
        }

        private string ObterMensagemDataDescarga()
        {
            return _despachoTranslogicRepository.ObterMensagemDataDespacho(_cte.Id);
            /*
            string serieDespachoSap = string.Empty;
            int numeroDespachoSap = 0;

            ObterNumeroSerieDespachoGeradoPeloSap(_cte.Chave, out serieDespachoSap, out numeroDespachoSap);
            return _despachoTranslogicRepository.ObterMensagemDataDespacho(numeroDespachoSap, serieDespachoSap);
            */
        }

    }

    /// <summary>
    ///   Log Helper
    /// </summary>
    public class LogHelper
    {
        private static string source = "TL.CteRunner";

        /// <summary>
        ///   Enviar dados de acompanhamento de trem por tipo de ambiente
        /// </summary>
        /// <param name="ex">exce��o disparada</param>
        /// <param name="metodo"> metodo da classe </param>     
        /// <param name="type"> Tipo do log </param>
        public static void Log(Exception ex, MethodBase metodo, EventLogEntryType type)
        {
            using (var evento = new EventLog())
            {
                if (!EventLog.SourceExists(source))
                {
                    EventLog.CreateEventSource(source, source);
                }

                evento.Source = source;
                evento.WriteEntry(string.Format("Classe: {0}, M�todo: {1}, Erro: {2}, erro interno: {3} ", metodo.DeclaringType == null ? String.Empty : metodo.DeclaringType.Name, metodo.Name, ex.Message, ex.InnerException == null ? String.Empty : ex.InnerException.Message));
            }
        }

        /// <summary>
        ///   Enviar dados de acompanhamento de trem por tipo de ambiente
        /// </summary>
        /// <param name="message"> Mensagem de erro </param>     
        /// <param name="type"> Tipo do log </param>
        public static void Log(string message, EventLogEntryType type)
        {
            //// var source = "Translogic.EdiRunner";

            if (!EventLog.SourceExists(source))
            {
                EventLog.CreateEventSource(source, source);
            }

            EventLog.WriteEntry(source, message, type);
        }
    }
}
