namespace Translogic.Modules.Core.Domain.Services.Ctes
{
    using System;
    using System.Text;
	using Model.FluxosComerciais.ConhecimentoTransporteEletronico;
	using Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;
	using NHibernate;

	/// <summary>
	/// Classe para grava��o dos logs do Ct-e
	/// </summary>
	public class CteLogService
	{
		private readonly ICteLogRepository _cteLogRepository;

		/// <summary>
		///  Initializes a new instance of the <see cref="CteLogService"/> class.
		/// </summary>
		/// <param name="cteLogRepository">Reposit�rio Cte log injetado</param>
		public CteLogService(ICteLogRepository cteLogRepository)
		{
			_cteLogRepository = cteLogRepository;
		}

		/// <summary>
		/// Insere o log do Cte
		/// </summary>
		/// <param name="cte">Cte referente ao log</param>
		/// <param name="mensagem">mensagem de log</param>
		public void InserirLogInfo(Cte cte, string mensagem)
		{
			_cteLogRepository.InserirLogInfo(cte, mensagem);
		}

		/// <summary>
		/// Insere o log do Cte
		/// </summary>
		/// <param name="cte">Cte referente ao log</param>
		/// <param name="nomeServico">Nome do servico ou metodo que esta sendo logado</param>
		/// <param name="mensagem">mensagem de log</param>
		public void InserirLogInfo(Cte cte, string nomeServico, string mensagem)
		{
			_cteLogRepository.InserirLogInfo(cte, nomeServico, mensagem);
		}

		/// <summary>
		/// Insere o log do Cte
		/// </summary>
		/// <param name="cte">Cte referente ao log</param>
		/// <param name="builder">mensagem de log</param>
		public void InserirLogInfo(Cte cte, StringBuilder builder)
		{
			_cteLogRepository.InserirLogInfo(cte, builder);
		}

		/// <summary>
		/// Insere o log de erro do Cte
		/// </summary>
		/// <param name="cte">Cte referente ao log</param>
		/// <param name="mensagem">mensagem de log</param>
		/// <param name="ex">Exception do erro</param>
		public void InserirLogErro(Cte cte, string mensagem, Exception ex = null)
		{
			_cteLogRepository.InserirLogErro(cte, mensagem, ex);
		}

		/// <summary>
		/// Insere o log de erro do Cte
		/// </summary>
		/// <param name="cte">Cte referente ao log</param>
		/// <param name="nomeServico">Nome do servico ou m�todo que esta sendo logado</param>
		/// <param name="mensagem">mensagem de log</param>
		/// <param name="ex">Exception do erro</param>
		public void InserirLogErro(Cte cte, string nomeServico, string mensagem, Exception ex = null)
		{
			_cteLogRepository.InserirLogErro(cte, nomeServico, mensagem, ex);
		}

		/// <summary>
		/// Insere o log de erro do Cte
		/// </summary>
		/// <param name="cte">Cte referente ao log</param>
		/// <param name="builder">mensagem de log</param>
		public void InserirLogErro(Cte cte, StringBuilder builder)
		{
			_cteLogRepository.InserirLogErro(cte, builder);
		}
	}
}