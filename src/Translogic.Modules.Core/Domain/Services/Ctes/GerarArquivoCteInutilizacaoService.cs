namespace Translogic.Modules.Core.Domain.Services.Ctes
{
	using System;
	using System.Collections.Generic;
	using System.IO;
	using System.Text;
	using Model.Estrutura;
	using Model.FluxosComerciais;
	using Model.FluxosComerciais.ConhecimentoTransporteEletronico;
	using Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;
	using Model.FluxosComerciais.Pedidos.Despachos;
	using Model.FluxosComerciais.Pedidos.Despachos.Repositories;
	using Util;

	/// <summary>
	/// Classe para geração dos arquivos do CT-e de inutilização
	/// </summary>
	public class GerarArquivoCteInutilizacaoService
	{
		private readonly string SEPARADOR_COLUNA = ";";
		private readonly ICteRepository _cteRepository;
		private readonly IItemDespachoRepository _itemDespachoRepository;

		private Cte _cte;
		private Contrato _contrato;
		private ItemDespacho _itemDespacho;
		private FluxoComercial _fluxoComercial;
		private DetalheCarregamento _detalheCarregamento;
		private Carregamento _carregamento;
		private ContratoHistorico _contratoHistorico;
		private CteLogService _cteLogService;

		private string _justificativa;

		/// <summary>
		/// Initializes a new instance of the <see cref="GerarArquivoCteInutilizacaoService"/> class.
		/// </summary>
		/// <param name="cteRepository">Repositório do Cte injetado</param>
		/// <param name="itemDespachoRepository">Repositório do ItemDespacho injetado</param>
		/// <param name="cteLogService">Serviço de log do cte injetado</param>
		public GerarArquivoCteInutilizacaoService(ICteRepository cteRepository, IItemDespachoRepository itemDespachoRepository, CteLogService cteLogService)
		{
			_cteRepository = cteRepository;
			_cteLogService = cteLogService;
			_itemDespachoRepository = itemDespachoRepository;
		}

		/// <summary>
		/// Executa a geração do arquivo de inutilizacao
		/// </summary>
		/// <param name="cte">Cte para gerar o arquivo de inutilização</param>
		/// <param name="justificativa">Justificativa da inutilização</param>
		/// <returns>Retorna o StringBuilder com o arquivo gerado</returns>
		public StringBuilder Executar(Cte cte, string justificativa)
		{
			StringBuilder builder = new StringBuilder();

			try
			{
				_justificativa = justificativa;
				_cte = cte;

				CarregarDados();

				builder.Append(GerarRegistro00000());
				builder.Append(GerarRegistro00010());
			}
			catch (Exception ex)
			{
				_cteLogService.InserirLogErro(_cte, "GerarArquivoCteInutilizacaoService", string.Format("Executar: {0}", ex.Message), ex);
				throw;
			}

			return builder;
		}

		private string GerarRegistro00010()
		{
			StringBuilder buffer = new StringBuilder();

			try
			{
				string cnpjSolicitante = _contrato.EmpresaRemetente.Cgc ?? string.Empty;
				string cnpjEmitente = _contrato.EmpresaRemetente.Cgc ?? string.Empty;
				EmpresaCliente empresaRemetente = _contrato.EmpresaRemetente;
				string siglaEstadoSolicitante = empresaRemetente.CidadeIbge.SiglaEstado ?? string.Empty;
				string identificador = siglaEstadoSolicitante + cnpjSolicitante;
				string anoInutilizacao = DateTime.Now.ToString("yyyy");
				string serieCte = _cte.Serie;
				string numeroInicial = _cte.Numero;
				string numeroFinal = _cte.Numero;
				string justificativa = _justificativa;

				buffer.Append("00010");
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(identificador, 39));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append("1"); // Homologacao
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append("INUTILIZAR");
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(siglaEstadoSolicitante);
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(anoInutilizacao);
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(cnpjEmitente, 14));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append("57"); // Modelo do CT-e
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(serieCte, 3));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(numeroInicial, 9));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(numeroFinal, 9));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(justificativa, 255));
				buffer.Append(SEPARADOR_COLUNA);
			}
			catch (Exception ex)
			{
				_cteLogService.InserirLogErro(_cte, "GerarArquivoCteInutilizacaoService", string.Format("GerarRegistro00010: {0}", ex.Message), ex);
				throw;
			}

			return buffer.ToString();
		}

		private string GerarRegistro00000()
		{
			StringBuilder buffer = new StringBuilder();
			try
			{
				buffer.Append("00000");
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append("1.04");
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append("INUTILIZACAO");
			}
			catch (Exception ex)
			{
				_cteLogService.InserirLogErro(_cte, "GerarArquivoCteInutilizacaoService", string.Format("GerarRegistro00000: {0}", ex.Message), ex);
				throw;
			}

			return buffer.ToString();
		}

		private void CarregarDados()
		{
			try
			{
				// Carregar os dados do Item de Despacho
				_itemDespacho = _itemDespachoRepository.ObterItemDespachoPorDespacho(_cte.Despacho);

				// Carregar as informações do detalhe carregamento
				_detalheCarregamento = _itemDespacho.DetalheCarregamento;

				// Carregar as informações de carregamento
				_carregamento = _detalheCarregamento.Carregamento;

				// Informações do fluxo comercial
				_fluxoComercial = _cte.FluxoComercial;

				// Contrato histórico
				_contratoHistorico = _cte.ContratoHistorico;

				// Dados de contrato
				// _contrato = _contratoHistorico.Contrato;
			}
			catch (Exception ex)
			{
				_cteLogService.InserirLogErro(_cte, "GerarArquivoCteInutilizacaoService", string.Format("CarregarDados: {0}", ex.Message), ex);
				throw;
			}
		}
	}
}
