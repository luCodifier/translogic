﻿namespace Translogic.Modules.Core.Domain.Services.Ctes
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text.RegularExpressions;
    using ALL.Core.Util;
    using Castle.Services.Transaction;
    using Model.Diversos;
    using Model.Diversos.Bacen;
    using Model.Diversos.Bacen.Repositories;
    using Model.Diversos.Cte;
    using Model.Diversos.Ibge;
    using Model.Estrutura;
    using Model.FluxosComerciais;
    using Model.FluxosComerciais.ConhecimentoTransporteEletronico;
    using Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;
    using Model.FluxosComerciais.TriangulusConfig;
    using Model.FluxosComerciais.TriangulusConfig.Repositories;
    using Util;

    /// <summary>
    /// Classe para gravar na config as informações de correção de Cte
    /// </summary>
    [Transactional]
    public class GravarConfigCorrecaoCteService
    {
        private readonly ICorrecaoCteRepository _correcaoCteRepository;
        private readonly ICorrecaoConteinerCteRepository _correcaoConteinerCteRepository;
        private readonly INfe03FilialRepository _nfe03FilialRepository;
        private readonly IPaisBacenRepository _paisBacenRepository;
        private readonly ICte121Repository _cte121Repository;
        private readonly CteService _cteService;
        private Nfe03Filial _nfe03Filial;
        private string _cfgSerie;
        private int _contCorrecao;
        private int _cfgDoc;
        private int _cfgUn;

        /// <summary>
        /// Initializes a new instance of the <see cref="GravarConfigCorrecaoCteService"/> class.
        /// </summary>
        /// <param name="correcaoCteRepository">Repositório da Correção do Cte injetado</param>
        /// <param name="correcaoConteinerCteRepository">Repositório da Correção de Conteiner do Cte injetado</param>
        /// <param name="cteService"> Serviço do Cte Service</param>
        /// <param name="nfe03FilialRepository">Repositório da Nfe03 injetado</param>
        /// <param name="cte121Repository">Repositório da entidade cte121 injetado</param>
        /// <param name="paisBacenRepository">Repositório do codigo do pais no BACEN injetado</param>
        public GravarConfigCorrecaoCteService(ICorrecaoCteRepository correcaoCteRepository, ICorrecaoConteinerCteRepository correcaoConteinerCteRepository, CteService cteService, INfe03FilialRepository nfe03FilialRepository, ICte121Repository cte121Repository, IPaisBacenRepository paisBacenRepository)
        {
            _correcaoCteRepository = correcaoCteRepository;
            _correcaoConteinerCteRepository = correcaoConteinerCteRepository;
            _cteService = cteService;
            _nfe03FilialRepository = nfe03FilialRepository;
            _cte121Repository = cte121Repository;
            _paisBacenRepository = paisBacenRepository;
        }

        /// <summary>
        /// Executa a gravação dos dados da carta de correção
        /// </summary>
        /// <param name="cte">Cte que será processado</param>
        [Transaction]
        public virtual void Executar(Cte cte)
        {
            try
            {
                LimparDados();
                _contCorrecao = 0;
                CorrecaoCte cteCorrecao = _correcaoCteRepository.ObterPorCte(cte);
                IList<CorrecaoConteinerCte> listaConteiner = _correcaoConteinerCteRepository.ObterPorCte(cte);
                _cte121Repository.Remover(ObterIdentificadorFilial(cte), int.Parse(cte.Numero), int.Parse(cte.Serie));

                CarregarDados(cte);

                if (cteCorrecao != null)
                {
                    GravarInformacoesCorrecaoCte(cteCorrecao);
                }

                if (listaConteiner.Any())
                {
                    GravarInformacoesAlteradasConteiner(listaConteiner);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private void LimparDados()
        {
            _nfe03Filial = null;
            _contCorrecao = 0;
            _cfgDoc = 0;
            _cfgSerie = string.Empty;
            _cfgUn = 0;
        }

        private void CarregarDados(Cte cte)
        {
            int.TryParse(cte.Numero, out _cfgDoc);
            _cfgSerie = int.Parse(cte.Serie).ToString();
            _cfgUn = ObterIdentificadorFilial(cte);
        }

        private void GravarInformacoesCorrecaoCte(CorrecaoCte cteCorrecao)
        {
            bool validaAlteracaoCfop;
            bool alteracaoExpedidor = false;
            bool alteracaoRecebedor = false;
            bool alteracaoCfop = false;

            if (cteCorrecao.FluxoComercial != null)
            {
                alteracaoExpedidor = _cteService.ValidarSeOcorreuAlteracaoDeExpedidor(cteCorrecao.Cte, cteCorrecao.FluxoComercial);
                alteracaoRecebedor = _cteService.ValidarSeOcorreuAlteracaoDeRecebedor(cteCorrecao.Cte, cteCorrecao.FluxoComercial);
                alteracaoCfop = _cteService.ValidarSeOcorreuAlteracaoCfop(cteCorrecao.Cte, cteCorrecao.FluxoComercial, out validaAlteracaoCfop);

                if (cteCorrecao.Cte.FluxoComercial.Codigo != cteCorrecao.FluxoComercial.Codigo)
                {
                    GravarAlteracaoFluxoComercial(cteCorrecao.FluxoComercial);
                }

                if (alteracaoExpedidor)
                {
                    GravarInformacoesAlteradasExpedidor(cteCorrecao.FluxoComercial.Contrato.EmpresaRemetente);
                }

                if (alteracaoRecebedor)
                {
                    GravarInformacoesAlteradasRecebedor(cteCorrecao.FluxoComercial.Contrato.EmpresaDestinataria);
                }

                if (alteracaoCfop && validaAlteracaoCfop)
                {
                    GravarInformacoesAlteradasCfop(cteCorrecao.FluxoComercial);
                }
            }

            if (!string.IsNullOrEmpty(cteCorrecao.Observacao))
            {
                GravarAlteracaoObservacao(cteCorrecao.Observacao);
            }

            if (cteCorrecao.Vagao != null)
            {
                GravarAlteracaoVagao(cteCorrecao);
            }
        }

        private void GravarInformacoesAlteradasCfop(FluxoComercial fluxoComercial)
        {
            var cte121 = new Cte121
            {
                CfgUn = _cfgUn,
                CfgDoc = _cfgDoc,
                CfgSerie = _cfgSerie,
                GrupoAlterado = Enum<CorrecaoCteGrupoAlteradoEnum>.GetDescriptionOf(CorrecaoCteGrupoAlteradoEnum.IdentificadorDoCte),
                CampoAlterado = Enum<CorrecaoCteCampoAlteradoEnum>.GetDescriptionOf(CorrecaoCteCampoAlteradoEnum.CodigoFiscalDeOperacoesEPrestacoes),
                IdCceInfCorrecao = ++_contCorrecao,
                ValorAlterado = fluxoComercial.Contrato.Cfop
            };
            _cte121Repository.Inserir(cte121);

            cte121 = new Cte121
            {
                CfgUn = _cfgUn,
                CfgDoc = _cfgDoc,
                CfgSerie = _cfgSerie,
                GrupoAlterado = Enum<CorrecaoCteGrupoAlteradoEnum>.GetDescriptionOf(CorrecaoCteGrupoAlteradoEnum.IdentificadorDoCte),
                CampoAlterado = Enum<CorrecaoCteCampoAlteradoEnum>.GetDescriptionOf(CorrecaoCteCampoAlteradoEnum.NaturezaDaOperacao),
                IdCceInfCorrecao = ++_contCorrecao,
                ValorAlterado = fluxoComercial.Contrato.DescricaoCfop
            };
            _cte121Repository.Inserir(cte121);
        }

        private void GravarAlteracaoFluxoComercial(FluxoComercial fluxoComercial)
        {
            var cte121 = new Cte121
            {
                CfgUn = _cfgUn,
                CfgDoc = _cfgDoc,
                CfgSerie = _cfgSerie,
                GrupoAlterado = Enum<CorrecaoCteGrupoAlteradoEnum>.GetDescriptionOf(CorrecaoCteGrupoAlteradoEnum.Ferroviario),
                CampoAlterado = Enum<CorrecaoCteCampoAlteradoEnum>.GetDescriptionOf(CorrecaoCteCampoAlteradoEnum.FluxoComercial),
                IdCceInfCorrecao = ++_contCorrecao,
                ValorAlterado = fluxoComercial.Codigo
            };
            _cte121Repository.Inserir(cte121);
        }

        private void GravarInformacoesAlteradasExpedidor(IEmpresa expedidor)
        {
            string cidadeExpedidor;
            string expedidorUf;
            int codigoPais;
            string cnpjExpedidor;

            if (expedidor.CidadeIbge.CodigoIbge == 9999999)
            {
                cidadeExpedidor = "EXTERIOR";
                expedidorUf = "EX";
            }
            else
            {
                cidadeExpedidor = expedidor.CidadeIbge.Descricao;
                expedidorUf = expedidor.CidadeIbge.SiglaEstado;
            }

            var telefoneExpedidor = ValidarTelefone(expedidor.Telefone1);

            PaisBacen paisBacen;
            if (expedidor.CidadeIbge.CodigoIbge == 9999999)
            {
                string siglaPaisAux = ObterSiglaPaisExteriorPeloSap(expedidor);
                paisBacen = _paisBacenRepository.ObterPorSiglaResumida(siglaPaisAux);
                cnpjExpedidor = "0";
            }
            else
            {
                paisBacen = _paisBacenRepository.ObterPorSiglaResumida(expedidor.CidadeIbge.SiglaPais);
                cnpjExpedidor = expedidor.Cgc;
            }

            // Código BACEN do país.
            int.TryParse(paisBacen.CodigoBacen.Substring(1, 4), out codigoPais);

            // Razão Social
            var cte121 = new Cte121
            {
                CfgUn = _cfgUn,
                CfgDoc = _cfgDoc,
                CfgSerie = _cfgSerie,
                GrupoAlterado = Enum<CorrecaoCteGrupoAlteradoEnum>.GetDescriptionOf(CorrecaoCteGrupoAlteradoEnum.Expedidor),
                CampoAlterado = Enum<CorrecaoCteCampoAlteradoEnum>.GetDescriptionOf(CorrecaoCteCampoAlteradoEnum.Nome),
                IdCceInfCorrecao = ++_contCorrecao,
                ValorAlterado = expedidor.RazaoSocial
            };
            _cte121Repository.Inserir(cte121);

            // CPF ou CNPJ
            if (expedidor.TipoPessoa == TipoPessoaEnum.Juridica)
            {
                cte121 = new Cte121
                {
                    CfgUn = _cfgUn,
                    CfgDoc = _cfgDoc,
                    CfgSerie = _cfgSerie,
                    GrupoAlterado = Enum<CorrecaoCteGrupoAlteradoEnum>.GetDescriptionOf(CorrecaoCteGrupoAlteradoEnum.Expedidor),
                    CampoAlterado = Enum<CorrecaoCteCampoAlteradoEnum>.GetDescriptionOf(CorrecaoCteCampoAlteradoEnum.Cnpj),
                    IdCceInfCorrecao = ++_contCorrecao,
                    ValorAlterado = cnpjExpedidor
                };

                _cte121Repository.Inserir(cte121);
            }
            else
            {
                cte121 = new Cte121
                {
                    CfgUn = _cfgUn,
                    CfgDoc = _cfgDoc,
                    CfgSerie = _cfgSerie,
                    GrupoAlterado = Enum<CorrecaoCteGrupoAlteradoEnum>.GetDescriptionOf(CorrecaoCteGrupoAlteradoEnum.Expedidor),
                    CampoAlterado = Enum<CorrecaoCteCampoAlteradoEnum>.GetDescriptionOf(CorrecaoCteCampoAlteradoEnum.Cpf),
                    IdCceInfCorrecao = ++_contCorrecao,
                    ValorAlterado = expedidor.Cpf
                };

                _cte121Repository.Inserir(cte121);
            }

            // Inscrição Estadual
            cte121 = new Cte121
            {
                CfgUn = _cfgUn,
                CfgDoc = _cfgDoc,
                CfgSerie = _cfgSerie,
                GrupoAlterado = Enum<CorrecaoCteGrupoAlteradoEnum>.GetDescriptionOf(CorrecaoCteGrupoAlteradoEnum.Expedidor),
                CampoAlterado = Enum<CorrecaoCteCampoAlteradoEnum>.GetDescriptionOf(CorrecaoCteCampoAlteradoEnum.InscEst),
                IdCceInfCorrecao = ++_contCorrecao,
                ValorAlterado = expedidor.InscricaoEstadual
            };
            _cte121Repository.Inserir(cte121);

            if (!string.IsNullOrEmpty(telefoneExpedidor))
            {
                // Telefone
                cte121 = new Cte121
                {
                    CfgUn = _cfgUn,
                    CfgDoc = _cfgDoc,
                    CfgSerie = _cfgSerie,
                    GrupoAlterado = Enum<CorrecaoCteGrupoAlteradoEnum>.GetDescriptionOf(CorrecaoCteGrupoAlteradoEnum.Expedidor),
                    CampoAlterado = Enum<CorrecaoCteCampoAlteradoEnum>.GetDescriptionOf(CorrecaoCteCampoAlteradoEnum.Telefone),
                    IdCceInfCorrecao = ++_contCorrecao,
                    ValorAlterado = telefoneExpedidor
                };
                _cte121Repository.Inserir(cte121);
            }

            // Logradouro Expedidor
            cte121 = new Cte121
            {
                CfgUn = _cfgUn,
                CfgDoc = _cfgDoc,
                CfgSerie = _cfgSerie,
                GrupoAlterado = Enum<CorrecaoCteGrupoAlteradoEnum>.GetDescriptionOf(CorrecaoCteGrupoAlteradoEnum.EnderecoExpedidor),
                CampoAlterado = Enum<CorrecaoCteCampoAlteradoEnum>.GetDescriptionOf(CorrecaoCteCampoAlteradoEnum.Logradouro),
                IdCceInfCorrecao = ++_contCorrecao,
                ValorAlterado = expedidor.Endereco
            };
            _cte121Repository.Inserir(cte121);

            // CEP Expedidor
            cte121 = new Cte121
            {
                CfgUn = _cfgUn,
                CfgDoc = _cfgDoc,
                CfgSerie = _cfgSerie,
                GrupoAlterado = Enum<CorrecaoCteGrupoAlteradoEnum>.GetDescriptionOf(CorrecaoCteGrupoAlteradoEnum.EnderecoExpedidor),
                CampoAlterado = Enum<CorrecaoCteCampoAlteradoEnum>.GetDescriptionOf(CorrecaoCteCampoAlteradoEnum.Cep),
                IdCceInfCorrecao = ++_contCorrecao,
                ValorAlterado = expedidor.Cep
            };
            _cte121Repository.Inserir(cte121);

            // Código Município
            cte121 = new Cte121
            {
                CfgUn = _cfgUn,
                CfgDoc = _cfgDoc,
                CfgSerie = _cfgSerie,
                GrupoAlterado = Enum<CorrecaoCteGrupoAlteradoEnum>.GetDescriptionOf(CorrecaoCteGrupoAlteradoEnum.EnderecoExpedidor),
                CampoAlterado = Enum<CorrecaoCteCampoAlteradoEnum>.GetDescriptionOf(CorrecaoCteCampoAlteradoEnum.CodMunicipio),
                IdCceInfCorrecao = ++_contCorrecao,
                ValorAlterado = expedidor.CidadeIbge.CodigoIbge.ToString()
            };
            _cte121Repository.Inserir(cte121);

            // Município
            cte121 = new Cte121
            {
                CfgUn = _cfgUn,
                CfgDoc = _cfgDoc,
                CfgSerie = _cfgSerie,
                GrupoAlterado = Enum<CorrecaoCteGrupoAlteradoEnum>.GetDescriptionOf(CorrecaoCteGrupoAlteradoEnum.EnderecoExpedidor),
                CampoAlterado = Enum<CorrecaoCteCampoAlteradoEnum>.GetDescriptionOf(CorrecaoCteCampoAlteradoEnum.Municipio),
                IdCceInfCorrecao = ++_contCorrecao,
                ValorAlterado = cidadeExpedidor
            };
            _cte121Repository.Inserir(cte121);

            // UF Expedidor
            cte121 = new Cte121
            {
                CfgUn = _cfgUn,
                CfgDoc = _cfgDoc,
                CfgSerie = _cfgSerie,
                GrupoAlterado = Enum<CorrecaoCteGrupoAlteradoEnum>.GetDescriptionOf(CorrecaoCteGrupoAlteradoEnum.EnderecoExpedidor),
                CampoAlterado = Enum<CorrecaoCteCampoAlteradoEnum>.GetDescriptionOf(CorrecaoCteCampoAlteradoEnum.Uf),
                IdCceInfCorrecao = ++_contCorrecao,
                ValorAlterado = expedidorUf
            };
            _cte121Repository.Inserir(cte121);

            // Código Pais Expedidor
            cte121 = new Cte121
            {
                CfgUn = _cfgUn,
                CfgDoc = _cfgDoc,
                CfgSerie = _cfgSerie,
                GrupoAlterado = Enum<CorrecaoCteGrupoAlteradoEnum>.GetDescriptionOf(CorrecaoCteGrupoAlteradoEnum.EnderecoExpedidor),
                CampoAlterado = Enum<CorrecaoCteCampoAlteradoEnum>.GetDescriptionOf(CorrecaoCteCampoAlteradoEnum.CodigoPais),
                IdCceInfCorrecao = ++_contCorrecao,
                ValorAlterado = codigoPais.ToString()
            };
            _cte121Repository.Inserir(cte121);

            // País Expedidor
            cte121 = new Cte121
            {
                CfgUn = _cfgUn,
                CfgDoc = _cfgDoc,
                CfgSerie = _cfgSerie,
                GrupoAlterado = Enum<CorrecaoCteGrupoAlteradoEnum>.GetDescriptionOf(CorrecaoCteGrupoAlteradoEnum.EnderecoExpedidor),
                CampoAlterado = Enum<CorrecaoCteCampoAlteradoEnum>.GetDescriptionOf(CorrecaoCteCampoAlteradoEnum.NomePais),
                IdCceInfCorrecao = ++_contCorrecao,
                ValorAlterado = paisBacen.Nome
            };
            _cte121Repository.Inserir(cte121);
        }

        private void GravarInformacoesAlteradasRecebedor(IEmpresa recebedor)
        {
            string cidadeRecebedor;
            string recebedorUf;
            string cnpjRecebedor;
            int codigoPais;

            if (recebedor.CidadeIbge.CodigoIbge == 9999999)
            {
                cidadeRecebedor = "EXTERIOR";
                recebedorUf = "EX";
            }
            else
            {
                cidadeRecebedor = recebedor.CidadeIbge.Descricao;
                recebedorUf = recebedor.CidadeIbge.SiglaEstado;
            }

            var telefoneRecebedor = ValidarTelefone(recebedor.Telefone1);

            PaisBacen paisBacen;
            if (recebedor.CidadeIbge.CodigoIbge == 9999999)
            {
                string siglaPaisAux = ObterSiglaPaisExteriorPeloSap(recebedor);
                paisBacen = _paisBacenRepository.ObterPorSiglaResumida(siglaPaisAux);
                cnpjRecebedor = "0";
            }
            else
            {
                paisBacen = _paisBacenRepository.ObterPorSiglaResumida(recebedor.CidadeIbge.SiglaPais);
                cnpjRecebedor = recebedor.Cgc;
            }

            // Código BACEN do país.
            int.TryParse(paisBacen.CodigoBacen.Substring(1, 4), out codigoPais);

            // Razão Social
            var cte121 = new Cte121
            {
                CfgUn = _cfgUn,
                CfgDoc = _cfgDoc,
                CfgSerie = _cfgSerie,
                GrupoAlterado = Enum<CorrecaoCteGrupoAlteradoEnum>.GetDescriptionOf(CorrecaoCteGrupoAlteradoEnum.Recebedor),
                CampoAlterado = Enum<CorrecaoCteCampoAlteradoEnum>.GetDescriptionOf(CorrecaoCteCampoAlteradoEnum.Nome),
                IdCceInfCorrecao = ++_contCorrecao,
                ValorAlterado = recebedor.RazaoSocial
            };
            _cte121Repository.Inserir(cte121);

            // CPF ou CNPJ
            if (recebedor.TipoPessoa == TipoPessoaEnum.Juridica)
            {
                cte121 = new Cte121
                {
                    CfgUn = _cfgUn,
                    CfgDoc = _cfgDoc,
                    CfgSerie = _cfgSerie,
                    GrupoAlterado = Enum<CorrecaoCteGrupoAlteradoEnum>.GetDescriptionOf(CorrecaoCteGrupoAlteradoEnum.Recebedor),
                    CampoAlterado = Enum<CorrecaoCteCampoAlteradoEnum>.GetDescriptionOf(CorrecaoCteCampoAlteradoEnum.Cnpj),
                    IdCceInfCorrecao = ++_contCorrecao,
                    ValorAlterado = cnpjRecebedor
                };
                _cte121Repository.Inserir(cte121);
            }
            else
            {
                cte121 = new Cte121
                {
                    CfgUn = _cfgUn,
                    CfgDoc = _cfgDoc,
                    CfgSerie = _cfgSerie,
                    GrupoAlterado = Enum<CorrecaoCteGrupoAlteradoEnum>.GetDescriptionOf(CorrecaoCteGrupoAlteradoEnum.Recebedor),
                    CampoAlterado = Enum<CorrecaoCteCampoAlteradoEnum>.GetDescriptionOf(CorrecaoCteCampoAlteradoEnum.Cpf),
                    IdCceInfCorrecao = ++_contCorrecao,
                    ValorAlterado = recebedor.Cpf
                };
                _cte121Repository.Inserir(cte121);
            }

            // Inscrição Estadual
            cte121 = new Cte121
            {
                CfgUn = _cfgUn,
                CfgDoc = _cfgDoc,
                CfgSerie = _cfgSerie,
                GrupoAlterado = Enum<CorrecaoCteGrupoAlteradoEnum>.GetDescriptionOf(CorrecaoCteGrupoAlteradoEnum.Recebedor),
                CampoAlterado = Enum<CorrecaoCteCampoAlteradoEnum>.GetDescriptionOf(CorrecaoCteCampoAlteradoEnum.InscEst),
                IdCceInfCorrecao = ++_contCorrecao,
                ValorAlterado = recebedor.InscricaoEstadual
            };
            _cte121Repository.Inserir(cte121);

            if (!string.IsNullOrEmpty(telefoneRecebedor))
            {
                // Telefone
                cte121 = new Cte121
                {
                    CfgUn = _cfgUn,
                    CfgDoc = _cfgDoc,
                    CfgSerie = _cfgSerie,
                    GrupoAlterado = Enum<CorrecaoCteGrupoAlteradoEnum>.GetDescriptionOf(CorrecaoCteGrupoAlteradoEnum.Recebedor),
                    CampoAlterado = Enum<CorrecaoCteCampoAlteradoEnum>.GetDescriptionOf(CorrecaoCteCampoAlteradoEnum.Telefone),
                    IdCceInfCorrecao = ++_contCorrecao,
                    ValorAlterado = telefoneRecebedor
                };
                _cte121Repository.Inserir(cte121);
            }

            // Logradouro
            cte121 = new Cte121
            {
                CfgUn = _cfgUn,
                CfgDoc = _cfgDoc,
                CfgSerie = _cfgSerie,
                GrupoAlterado = Enum<CorrecaoCteGrupoAlteradoEnum>.GetDescriptionOf(CorrecaoCteGrupoAlteradoEnum.EnderecoRecebedor),
                CampoAlterado = Enum<CorrecaoCteCampoAlteradoEnum>.GetDescriptionOf(CorrecaoCteCampoAlteradoEnum.Logradouro),
                IdCceInfCorrecao = ++_contCorrecao,
                ValorAlterado = recebedor.Endereco
            };
            _cte121Repository.Inserir(cte121);

            // CEP
            cte121 = new Cte121
            {
                CfgUn = _cfgUn,
                CfgDoc = _cfgDoc,
                CfgSerie = _cfgSerie,
                GrupoAlterado = Enum<CorrecaoCteGrupoAlteradoEnum>.GetDescriptionOf(CorrecaoCteGrupoAlteradoEnum.EnderecoRecebedor),
                CampoAlterado = Enum<CorrecaoCteCampoAlteradoEnum>.GetDescriptionOf(CorrecaoCteCampoAlteradoEnum.Cep),
                IdCceInfCorrecao = ++_contCorrecao,
                ValorAlterado = recebedor.Cep
            };
            _cte121Repository.Inserir(cte121);

            // Código Município
            cte121 = new Cte121
            {
                CfgUn = _cfgUn,
                CfgDoc = _cfgDoc,
                CfgSerie = _cfgSerie,
                GrupoAlterado = Enum<CorrecaoCteGrupoAlteradoEnum>.GetDescriptionOf(CorrecaoCteGrupoAlteradoEnum.EnderecoRecebedor),
                CampoAlterado = Enum<CorrecaoCteCampoAlteradoEnum>.GetDescriptionOf(CorrecaoCteCampoAlteradoEnum.CodMunicipio),
                IdCceInfCorrecao = ++_contCorrecao,
                ValorAlterado = recebedor.CidadeIbge.CodigoIbge.ToString()
            };
            _cte121Repository.Inserir(cte121);

            // Município
            cte121 = new Cte121
            {
                CfgUn = _cfgUn,
                CfgDoc = _cfgDoc,
                CfgSerie = _cfgSerie,
                GrupoAlterado = Enum<CorrecaoCteGrupoAlteradoEnum>.GetDescriptionOf(CorrecaoCteGrupoAlteradoEnum.EnderecoRecebedor),
                CampoAlterado = Enum<CorrecaoCteCampoAlteradoEnum>.GetDescriptionOf(CorrecaoCteCampoAlteradoEnum.Municipio),
                IdCceInfCorrecao = ++_contCorrecao,
                ValorAlterado = cidadeRecebedor
            };
            _cte121Repository.Inserir(cte121);

            // UF
            cte121 = new Cte121
            {
                CfgUn = _cfgUn,
                CfgDoc = _cfgDoc,
                CfgSerie = _cfgSerie,
                GrupoAlterado = Enum<CorrecaoCteGrupoAlteradoEnum>.GetDescriptionOf(CorrecaoCteGrupoAlteradoEnum.EnderecoRecebedor),
                CampoAlterado = Enum<CorrecaoCteCampoAlteradoEnum>.GetDescriptionOf(CorrecaoCteCampoAlteradoEnum.Uf),
                IdCceInfCorrecao = ++_contCorrecao,
                ValorAlterado = recebedorUf
            };
            _cte121Repository.Inserir(cte121);

            // Código Pais
            cte121 = new Cte121
            {
                CfgUn = _cfgUn,
                CfgDoc = _cfgDoc,
                CfgSerie = _cfgSerie,
                GrupoAlterado = Enum<CorrecaoCteGrupoAlteradoEnum>.GetDescriptionOf(CorrecaoCteGrupoAlteradoEnum.EnderecoRecebedor),
                CampoAlterado = Enum<CorrecaoCteCampoAlteradoEnum>.GetDescriptionOf(CorrecaoCteCampoAlteradoEnum.CodigoPais),
                IdCceInfCorrecao = ++_contCorrecao,
                ValorAlterado = codigoPais.ToString()
            };
            _cte121Repository.Inserir(cte121);

            // País
            cte121 = new Cte121
            {
                CfgUn = _cfgUn,
                CfgDoc = _cfgDoc,
                CfgSerie = _cfgSerie,
                GrupoAlterado = Enum<CorrecaoCteGrupoAlteradoEnum>.GetDescriptionOf(CorrecaoCteGrupoAlteradoEnum.EnderecoRecebedor),
                CampoAlterado = Enum<CorrecaoCteCampoAlteradoEnum>.GetDescriptionOf(CorrecaoCteCampoAlteradoEnum.NomePais),
                IdCceInfCorrecao = ++_contCorrecao,
                ValorAlterado = paisBacen.Nome
            };
            _cte121Repository.Inserir(cte121);
        }

        private void GravarAlteracaoObservacao(string observacao)
        {
            var cte121 = new Cte121
            {
                CfgUn = _cfgUn,
                CfgDoc = _cfgDoc,
                CfgSerie = _cfgSerie,
                GrupoAlterado = Enum<CorrecaoCteGrupoAlteradoEnum>.GetDescriptionOf(CorrecaoCteGrupoAlteradoEnum.ComplementoCte),
                CampoAlterado = Enum<CorrecaoCteCampoAlteradoEnum>.GetDescriptionOf(CorrecaoCteCampoAlteradoEnum.Observacao),
                IdCceInfCorrecao = ++_contCorrecao,
                ValorAlterado = observacao
            };
            _cte121Repository.Inserir(cte121);
        }

        private void GravarAlteracaoVagao(CorrecaoCte cteCorrecao)
        {
            var contVagao = 0;
            var listaDetalhe = _cteService.ObterCteDetalhePorIdCte(cteCorrecao.Cte.Id.Value);

            for (var indice = 0; indice < listaDetalhe.Count; indice++)
            {
                var cte121 = new Cte121
                {
                    CfgUn = _cfgUn,
                    CfgDoc = _cfgDoc,
                    CfgSerie = _cfgSerie,
                    GrupoAlterado = Enum<CorrecaoCteGrupoAlteradoEnum>.GetDescriptionOf(CorrecaoCteGrupoAlteradoEnum.InformacoesDaUnidadeDeTransporte),
                    CampoAlterado = Enum<CorrecaoCteCampoAlteradoEnum>.GetDescriptionOf(CorrecaoCteCampoAlteradoEnum.CodigoVagao),
                    IdCceInfCorrecao = ++_contCorrecao,
                    ValorAlterado = cteCorrecao.Vagao.Codigo,
                    NroItemAlterado = (++contVagao).ToString()
                };
                _cte121Repository.Inserir(cte121);
            }
        }

        private void GravarInformacoesAlteradasConteiner(IEnumerable<CorrecaoConteinerCte> listaConteiner)
        {
            Cte cte = listaConteiner.FirstOrDefault().Cte;
            IList<string> listaNfe = _cteService.ObterListaNfeXmlEnvio(cte.Id.Value);

            foreach (var correcaoConteinerCte in listaConteiner)
            {
                int posicaoNfeXml = listaNfe.IndexOf(correcaoConteinerCte.ChaveNfe);

                var cte121 = new Cte121
                {
                    CfgUn = _cfgUn,
                    CfgDoc = _cfgDoc,
                    CfgSerie = _cfgSerie,
                    GrupoAlterado = Enum<CorrecaoCteGrupoAlteradoEnum>.GetDescriptionOf(CorrecaoCteGrupoAlteradoEnum.InformacoesDaUnidadeDeCarga),
                    CampoAlterado = Enum<CorrecaoCteCampoAlteradoEnum>.GetDescriptionOf(CorrecaoCteCampoAlteradoEnum.Conteiner),
                    IdCceInfCorrecao = ++_contCorrecao,
                    NroItemAlterado = (++posicaoNfeXml).ToString(),
                    ValorAlterado = correcaoConteinerCte.ConteinerCorrigidoNotaFiscal
                };
                _cte121Repository.Inserir(cte121);
            }
        }

        private int ObterIdentificadorFilial(Cte cte)
        {
            if (_nfe03Filial == null)
            {
                long cnpj;
                Int64.TryParse(cte.CnpjFerrovia, out cnpj);

                _nfe03Filial = _nfe03FilialRepository.ObterFilialPorCnpj(cnpj);

                if (_nfe03Filial != null)
                {
                    return (int)_nfe03Filial.IdFilial;
                }

                // Caso não tenha encontrado a filial
                return -1;
            }

            return (int)_nfe03Filial.IdFilial;
        }

        private string ValidarTelefone(string telefone)
        {
            if (telefone == null)
            {
                return string.Empty;
            }

            string retorno = Tools.TruncateRemoveSpecialChar(telefone, 12, 7);

            var regex = new Regex(Enum<RegexSefaz>.GetDescriptionOf(RegexSefaz.ER36));
            var match = regex.Match(retorno);

            retorno = match.Success ? match.Value : string.Empty;

            return retorno;
        }

        private string ObterSiglaPaisExteriorPeloSap(IEmpresa empresa)
        {
            if (empresa.EnderecoSap != null)
            {
                string mask = "- ";
                var linhaPais = empresa.EnderecoSap;
                var dados = linhaPais.Split(mask.ToCharArray());
                return dados[dados.Length - 1];
            }

            if (empresa.CidadeIbge != null)
            {
                CidadeIbge cidadeIbge = empresa.CidadeIbge;
                return cidadeIbge.SiglaPais;
            }

            return string.Empty;
        }
    }
}