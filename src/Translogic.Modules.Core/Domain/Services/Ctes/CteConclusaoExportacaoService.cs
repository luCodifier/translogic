﻿namespace Translogic.Modules.Core.Domain.Services.Ctes
{
    using System.Collections.Generic;
    using System.Linq;
    using System.ServiceModel;
    using System.ServiceModel.Activation;
    using Translogic.Modules.Core.Domain.Model.CteConclusaoExportacao;
    using Translogic.Modules.Core.Domain.Model.CteConclusaoExportacao.Outputs;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos.Repositories;
    using Translogic.Modules.Core.Domain.Services.Ctes.Interface;

    /// <summary>
    /// serviço de envio de informações do cte
    /// </summary>
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceBehavior]
    public class CteConclusaoExportacaoService : ICteConclusaoExportacaoService
    {
        private readonly ICteConclusaoExportacaoRepository _cteConclusaoExportacaoRepository;
        private readonly INotaFiscalTranslogicRepository _notaFiscalTranslogicRepository;

        /// <summary>
        /// Construtor recebendo a interface de acesso aos dados 
        /// </summary>
        /// <param name="cteConclusaoExportacaoRepository">Interface do Repositorio</param>
        /// <param name="notaFiscalTranslogicRepository">Interface do Repositorio de notas fiscais</param>
        public CteConclusaoExportacaoService(ICteConclusaoExportacaoRepository cteConclusaoExportacaoRepository, INotaFiscalTranslogicRepository notaFiscalTranslogicRepository)
        {
            _cteConclusaoExportacaoRepository = cteConclusaoExportacaoRepository;
            _notaFiscalTranslogicRepository = notaFiscalTranslogicRepository;
        }

        /// <summary>
        /// Metodo que retorna objeto a partir da consulta feita pelo request
        /// </summary>
        /// <param name="request">Objeto com os dados de entrada para a busca</param>
        /// <returns>Retorna Objeto com os dados dos ctes </returns>
        public EletronicBillofLadingExportCompletedRailResponse InformationCteConclusaoExportacao(EletronicBillofLadingExportCompletedRailRequest request)
        {
            var eletronicBillofLadingExportCompletedRailType = new EletronicBillofLadingExportCompletedRailType();

            if (request.ElectronicBillofLadingExportCompletedRail.ElectronicBillofLadingExportCompletedRailType.ElectronicBillofLadingExportCompletedRailDetail.EndDate < request.ElectronicBillofLadingExportCompletedRail.ElectronicBillofLadingExportCompletedRailType.ElectronicBillofLadingExportCompletedRailDetail.StartDate)
            {
                eletronicBillofLadingExportCompletedRailType = new EletronicBillofLadingExportCompletedRailType
                {
                    ReturnMessage =
                        "ERRO-003: Data Inicial deve ser menor que a data Final"
                };
            }
            else
            {
                var dados = _cteConclusaoExportacaoRepository.InformationCteConclusaoExportacao(request)
                    .GroupBy(a => a.CteId)
                    .Select(a => new ElectronicBillofLadingExportCompletedData
                                      {
                                          CteId = a.First().CteId,
                                          CteKey = a.First().CteKey,
                                          DateTimeArrival = a.First().DateTimeArrival,
                                          UnloadedWeight = a.First().UnloadedWeight,
                                          BondedEnclosure = a.First().BondedEnclosure,
                                          Waybills = a.Select(b => new WaybillInformation
                                                                     {
                                                                         WaybillInvoiceExportCompletedId = b.NotaId.ToString(),
                                                                         WaybillInvoiceKey = b.NotaChave
                                                                     }).ToList()
                                      })
                     .ToList();

                /*foreach (var item in dados)
                {
                    var c = new Carregamento
                    {
                        Id = (int)item.Carregamento
                    };
                    var listaNotas = _notaFiscalTranslogicRepository.ObterNotaFiscalPorCarregamento(c);
                    
                    var notas = new List<WaybillInformation>();
                    var waybill = new WaybillInformation();
                    foreach (var nota in item)
                    {
                        waybill.WaybillInvoiceExportCompletedId = nota.NotaId.ToString();
                        waybill.WaybillInvoiceKey = nota.NotaChave;
                        notas.Add(waybill);
                    }

                    item.Key.Waybills = notas;
                }*/

                string dataFim;

                if ((request.ElectronicBillofLadingExportCompletedRail.ElectronicBillofLadingExportCompletedRailType.ElectronicBillofLadingExportCompletedRailDetail.EndDate - request.ElectronicBillofLadingExportCompletedRail.ElectronicBillofLadingExportCompletedRailType.ElectronicBillofLadingExportCompletedRailDetail.StartDate).TotalDays > 30)
                {
                    dataFim =
                        request.ElectronicBillofLadingExportCompletedRail.ElectronicBillofLadingExportCompletedRailType.ElectronicBillofLadingExportCompletedRailDetail.StartDate.
                            AddDays(30).ToShortDateString();
                }
                else
                {
                    dataFim =
                         request.ElectronicBillofLadingExportCompletedRail.ElectronicBillofLadingExportCompletedRailType.ElectronicBillofLadingExportCompletedRailDetail.EndDate.
                             ToShortDateString();
                }

                string dataIni =
                    request.ElectronicBillofLadingExportCompletedRail.ElectronicBillofLadingExportCompletedRailType.ElectronicBillofLadingExportCompletedRailDetail.StartDate.
                        ToShortDateString();

                eletronicBillofLadingExportCompletedRailType = new EletronicBillofLadingExportCompletedRailType
                {
                    EletronicBillOfLadingExportCompletedRailDetail =
                        new EletronicBillOfLadingExportCompletedRailDetail
                            {
                                ElectronicBillofLadingExportCompletedData = dados
                            },

                    ReturnMessage =
                        "PESQUISA REALIZADA ENTRE OS DIAS " + dataIni +
                        " E " + dataFim,
                    VersionIdentifier = "1.0"
                };
            }

            var response = new EletronicBillofLadingExportCompletedRailResponse
            {
                EletronicBillofLadingExportCompletedRailOutput = new EletronicBillofLadingExportCompletedRailOutput
                {
                    EletronicBillofLadingExportCompletedRailType = eletronicBillofLadingExportCompletedRailType
                }
            };

            return response;
        }
    }
}