﻿namespace Translogic.Modules.Core.Domain.Services.Ctes.Interface
{
    using System.ServiceModel;
    using Translogic.Modules.Core.Domain.Model.CteRodoviario;

    /// <summary>
    /// Interface para Serviceo de Ctes Rodoviarios
    /// </summary>
    [ServiceContract]
    public interface ICteRodoviarioService
    {
        /// <summary>
        /// Metodo que retorna os dados das ctes de acordo com o objeto de input
        /// </summary>
        /// <param name="request">Objeto do tipo InformationElectronicBillofLadingHighwayRequest com os dados de entrada</param>
        /// <returns>Retorna objeto de output InformationElectronicBillofLadingHighwayResponse</returns>
        [OperationContract]
        InformationElectronicBillofLadingHighwayResponse InformationCteRodoviario(InformationElectronicBillofLadingHighwayRequest request);
    }
}