﻿namespace Translogic.Modules.Core.Domain.Services.Ctes.Interface
{
    using System.Collections.Generic;
    using Translogic.Modules.Core.Domain.Model.CteRodoviario.Outputs;
    
    /// <summary>
    /// Repositorio para consultas das notas dos Ctes rodoviarios na base do translogic
    /// </summary>
    public interface ICteRodoviarioNotasRepository
    { 
        /// <summary>
        /// Retorna Notas dos Ctes Rodoviarios cadastrados na base do translogic
        /// </summary>
        /// <param name="cte">Id do Cte Rodoviario</param>
        /// <returns>Retorna Lista de Notas vinculadas ao Cte</returns>
        List<WaybillInformation> ObterNotasRodoPorCteId(int cte);
    }
}