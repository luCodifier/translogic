﻿namespace Translogic.Modules.Core.Domain.Services.Ctes.Interface
{
    using System.ServiceModel;
    using Translogic.Modules.Core.Domain.Model.WebServices.CteFerroviario;

    /// <summary>
    /// Serviço de informações do cte.
    /// </summary>
    [ServiceContract]
    public interface ICteFerroviarioService
    {
        /// <summary>
        /// Retorna informações dos ctes
        /// </summary>
        /// <param name="request">Dados do request - <see cref="InformationElectronicNotificationRequest"/>periodo de filtro</param>
        /// <returns> Objeto <see cref="InformationElectronicNotificationResponse"/>Retorna dados do cte referente ao periodo</returns>
        /// ### Metodo WS desabilitado.  Utilizar Somente o metodo InformationElectronicCtesMalhaNorteComMDFE  ###
        /// [OperationContract] 
        InformationElectronicNotificationResponse InformationElectronicCtes(InformationElectronicNotificationRequest request);

        /// <summary>
        /// informações do cte da malha norte com MDFEs
        /// </summary>
        /// <param name="request">informaçoes de requisição</param>
        /// <returns>Retorna dados do cte</returns>
        [OperationContract]
        InformationElectronicNotificationResponse InformationElectronicCtesMalhaNorteComMDFE(InformationElectronicNotificationRequest request);

        /// <summary>
        /// informações do cte da malha sul com MDFEs
        /// </summary>
        /// <param name="request">informaçoes de requisição</param>
        /// <returns>Retorna dados do cte</returns>
        [OperationContract]
        InformationElectronicNotificationResponse InformationElectronicCtesMalhaSulComMdfe(InformationElectronicNotificationRequest request);
    }
}