﻿namespace Translogic.Modules.Core.Domain.Services.Ctes.Interface
{
    using System.Collections.Generic;
    using System.ServiceModel;

    using Translogic.Modules.Core.Domain.Model.CteFerroviarioCancelado;
    using Translogic.Modules.Core.Domain.Model.CteFerroviarioCancelado.Outputs;
    using Translogic.Modules.Core.Domain.Model.Dto;

    /// <summary>
    ///     Serviço de informações do cte.
    /// </summary>
    [ServiceContract]
    public interface ICteFerroviarioCanceladoService
    {
        #region Public Methods and Operators

        /// <summary>
        ///     Envia os Ctes Cancelados com o Recebedor diferente do Próximo Cte para Refaturamento
        /// </summary>
        /// <param name="request">informaçoes de requisição</param>
        /// <returns>Retorna dados do cte</returns>
        EletronicBillofLadingCancelledRailResponse ObterCtesCanceladosParaRefaturamento(EletronicBillofLadingCancelledRailRequest request);

        /// <summary>
        ///     Retorna informações dos ctes
        /// </summary>
        /// <param name="request">Dados do request - <see cref="EletronicBillofLadingCancelledRailRequest" />periodo de filtro</param>
        /// <param name="message">Mensagem de retorno</param>
        /// <returns> Objeto <see cref="EletronicBillofLadingCancelledRailResponse" />Retorna dados do cte referente ao periodo</returns>
        IList<EletronicBillofLadingCancelledData> ObterCtesCanceladosSemDespachoCancelado(EletronicBillofLadingCancelledRailRequest request, out string message);

        /// <summary>
        ///     Obtem os despachos cancelados e os próximos despachos gerados
        /// </summary>
        /// <param name="request">Objeto EletronicBillofLadingCancelledRailRequest com as datas para busca</param>
        /// <param name="message">Mensagem de retorno</param>
        /// <returns>Lista de despachos cancelados</returns>
        IList<DespachoCanceladoDto> ObterDespachosCancelados(EletronicBillofLadingCancelledRailRequest request, out string message);

        /// <summary>
        ///     Obtém os Ctes Cancelados com o Recebedor Diferente do Próximo Cte para Refaturamento da Base Consolidada
        /// </summary>
        /// <param name="request">informaçoes de requisição</param>
        /// <returns>Retorna dados do cte</returns>
        [OperationContract]
        EletronicBillofLadingCancelledRailResponse InformationCte(EletronicBillofLadingCancelledRailRequest request);

        #endregion
    }
}