﻿namespace Translogic.Modules.Core.Domain.Services.Ctes.Interface
{
    using System.ServiceModel;
    using Translogic.Modules.Core.Domain.Model.CteConclusaoExportacao;
    
    /// <summary>
    /// Interface para acessar o servico de Conclusao e Exportacao de cte
    /// </summary>
    [ServiceContract]
    public interface ICteConclusaoExportacaoService
    {
        /// <summary>
        /// Metodo que retorna objeto a partir da consulta feita pelo request
        /// </summary>
        /// <param name="request">Objeto com os dados de entrada para a busca</param>
        /// <returns>Retorna Objeto com os dados dos ctes </returns>
        [OperationContract]
        EletronicBillofLadingExportCompletedRailResponse InformationCteConclusaoExportacao(EletronicBillofLadingExportCompletedRailRequest request);
    }
}