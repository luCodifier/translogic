﻿namespace Translogic.Modules.Core.Domain.Services.Ctes
{
    using Castle.Services.Transaction;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;

    /// <summary>
    /// Classe para gravar os dados do cte enviado para seguradora
    /// </summary>
    [Transactional]
    public class CteEnvioSeguradoraHistService
    {
        private readonly ICteEnvioSeguradoraHistRepository _cteEnvioSeguradoraHistRepository;

        /// <summary>
        /// Initializes a new instance of the CteEnvioSeguradoraHistService
        /// </summary>
        /// <param name="cteEnvioSeguradoraHistRepository">Repositório do cte envio seguradora </param>
        public CteEnvioSeguradoraHistService(ICteEnvioSeguradoraHistRepository cteEnvioSeguradoraHistRepository)
        {
            _cteEnvioSeguradoraHistRepository = cteEnvioSeguradoraHistRepository;
        }

        /// <summary>
        /// construtor da classe
        /// </summary>
        /// <param name="cteEnvioSeguradoraHist">parametro para gravar</param>
        public void GravarCteEnvioSeguradoraHistorico(CteEnvioSeguradoraHist cteEnvioSeguradoraHist)
        {
            _cteEnvioSeguradoraHistRepository.Inserir(cteEnvioSeguradoraHist);
        }
    }
}