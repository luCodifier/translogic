﻿namespace Translogic.Modules.Core.Domain.Services.Ctes
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text.RegularExpressions;
    using ALL.Core.Util;
    using Castle.Services.Transaction;
    using Model.Diversos;
    using Model.Diversos.Bacen;
    using Model.Diversos.Bacen.Repositories;
    using Model.Diversos.Cte;
    using Model.Diversos.Ibge;
    using Model.Estrutura;
    using Model.FluxosComerciais;
    using Model.FluxosComerciais.ConhecimentoTransporteEletronico;
    using Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;
    using Model.FluxosComerciais.TriangulusConfig;
    using Model.FluxosComerciais.TriangulusConfig.Repositories;
    using Util;

    [Transactional]
    public class GravarConfigCteAnulacaoContribuinteService
    {
        private readonly CteService _cteService;
        private readonly ICteAnuladoRepository _cteAnuladoRepository;
        private readonly ICteRepository _cteRepository;
        private readonly ICte37Repository _cte37Repository;
        private readonly INfe03FilialRepository _nfe03FilialRepository;
        private Cte _cte;
        private Nfe03Filial _nfe03Filial;
        private CteLogService _cteLogService;

        public GravarConfigCteAnulacaoContribuinteService(ICteAnuladoRepository cteAnuladoRepository, ICte37Repository cte37Repository, INfe03FilialRepository nfe03FilialRepository, ICteRepository cteRepository, CteService cteService, CteLogService cteLogService)
        {
            _cteService = cteService;
            _cte37Repository = cte37Repository;
            _nfe03FilialRepository = nfe03FilialRepository;
            _cteAnuladoRepository = cteAnuladoRepository;
            _cteLogService = cteLogService;
            _cteRepository = cteRepository;
        }

        /// <summary>
        /// Executa a gravação dos dados da carta de correção
        /// </summary>
        /// <param name="cte">Cte que será processado</param>
        [Transaction]
        public virtual void Executar(Cte cte)
        {
            try
            {
                LimparDados();
                CarregarDados(cte);
                GravarInformacoesAnulacaoCte();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private void GravarInformacoesAnulacaoCte()
        {
            try
            {
                Cte37 cte37 = new Cte37();

                // Identificador da filial do CT-e
                cte37.CfgUn = ObterIdentificadorFilial();

                // Numero do documento CT-e (PK)
                int numConvertido;
                int.TryParse(_cte.Numero, out numConvertido);
                cte37.CfgDoc = numConvertido;

                // Série do CT-e
                cte37.CfgSerie = int.Parse(_cte.Serie).ToString();

                // Chave do cte original a ser anulado - busca na tabela CTE_ANULADO, q a partir do cte de anulaçãom pega o cte pai anulado.
                cte37.InfCteAnuEntChCTe = _cteAnuladoRepository.ObterPorIdCteAnulacao(_cte.Id ?? 0).Cte.Chave;
                
                // Data da anulação - Cte de contribuinte não tem o campo data anulação.
                ////cte37.InfCteAnuEntDemi = _cte.DataAnulacao;

                _cte37Repository.Inserir(cte37);
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        ////private void AtualizarDadosCte(Cte cte)
        ////{
        ////    try
        ////    {
        ////        //cte.SituacaoAtual = SituacaoCteEnum.EnviadoArquivoCancelamento;
        ////        _cteRepository.Atualizar(cte);
        ////    }
        ////    catch (Exception)
        ////    {
                
        ////        throw;
        ////    }
        
        ////}

        private int ObterIdentificadorFilial()
        {
            if (_nfe03Filial == null)
            {
                long cnpj;
                Int64.TryParse(_cte.CnpjFerrovia, out cnpj);

                _nfe03Filial = _nfe03FilialRepository.ObterFilialPorCnpj(cnpj);

                if (_nfe03Filial != null)
                {
                    return (int)_nfe03Filial.IdFilial;
                }

                // Caso não tenha encontrado a filial
                return -1;
            }

            return (int)_nfe03Filial.IdFilial;
        }

        private void LimparDados()
        {
            _cte = null;
        }

        private void CarregarDados(Cte cte)
        {
            _cte = cte;
        }
    }
}