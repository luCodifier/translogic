namespace Translogic.Modules.Core.Domain.Services.Ctes
{
    using System;
    using System.Collections.Generic;
    using FluxosComerciais;
    using Model.Acesso;
    using Model.Diversos;
    using Model.Diversos.Cte;
    using Model.Estrutura;
    using Model.FluxosComerciais.ConhecimentoTransporteEletronico;
    using Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;
    using Model.FluxosComerciais.Pedidos.Despachos;
    using Model.Trem.Veiculo.Vagao;
    using Model.Via;
    using Model.Via.Circulacao;
    using Translogic.Core.Infrastructure;
    using Via;

    /*
    /// <summary>
    /// Servi�o de grava��o de um CTe
    /// </summary>
    public class GravacaoCteService
    {
        private readonly ICteRepository _cteRepository;
        private readonly ICteVersaoRepository _cteVersaoRepository;
        private readonly ICteStatusRepository _cteStatusRepository;
        private readonly CteService _cteService;
        private readonly RoteadorService _roteadorService;

        public GravacaoCteService(ICteRepository cteRepository, CteService cteService, ICteStatusRepository cteStatusRepository, RoteadorService roteadorService, ICteVersaoRepository cteVersaoRepository)
        {
            _cteRepository = cteRepository;
            _cteVersaoRepository = cteVersaoRepository;
            _roteadorService = roteadorService;
            _cteStatusRepository = cteStatusRepository;
            _cteService = cteService;
        }

        public void GravarCtePorDto(GravacaoCteDto dto, Usuario usuario)
        {
            Rota rota = _roteadorService.ObterRotaPorTrechoMenorDistancia(dto.Origem.Codigo, dto.Destino.Codigo);
            if (rota == null)
            {
                throw new TranslogicException("N�o foi encontrada uma rota v�lida para o trecho do fluxo");
            }

            string numeroCte, serieCte;

            IList<EmpresaCteDto> listaEmpresas = _cteService.ObterEmpresasPorRota(rota);
            foreach (EmpresaCteDto empresaCteDto in listaEmpresas)
            {
                _cteService.GerarSequenciaNumeroSerie(out numeroCte, out serieCte);
                string chaveCte = _cteService.GerarChaveCte(out numeroCte, out serieCte);
                _cteService.GerarDigitoVerificador(chaveParcial);
                Cte cte = InserirCte(chaveCte, serieCte, numeroCte, dto);
                InserirStatusCte(usuario, cte);
            }
        }

        private Cte InserirCte(string chaveCte, string serieCte, string numeroCte, GravacaoCteDto dto)
        {
            CteVersao cteVersao = _cteVersaoRepository.ObterVigente();
            Cte cte = new Cte
                          {
                              Chave = chaveCte,
                              Serie = serieCte,
                              Numero = numeroCte,
                              Manutencao = dto.CtePai != null,
                              SituacaoAtual = SituacaoCteEnum.Pendente,
                              Despacho = dto.Despacho,
                              Versao = cteVersao
                          };
            _cteRepository.Inserir(cte);
            return cte;
        }

        private void InserirStatusCte(Usuario usuario, Cte cte)
        {
            CteStatus cteStatus = new CteStatus();
            cteStatus.Usuario = usuario;
            cteStatus.Cte = cte;
            // TODO: VERIFICAR O MAPEAMENTO PARA DEIXAR CASCADE NONE
            cteStatus.CteStatusRetorno = new CteStatusRetorno { Id = 10 };
            cteStatus.DataHora = DateTime.Now;
            _cteStatusRepository.Inserir(cteStatus);
        }
    }

    /// <summary>
    /// Dto da Empresa que deve ser gerado o Cte
    /// </summary>
    public class EmpresaCteDto
    {
        /// <summary>
        /// Empresa que pode gerar o cte
        /// </summary>
        public IEmpresa Empresa { get; set; }

        /// <summary>
        /// Estado de gera��o do Cte
        /// </summary>
        public Estado Estado { get; set; }

        /// <summary>
        /// Serie de despacho por uf
        /// </summary>
        public SerieDespachoUf SerieDespachoUf { get; set; }

        /// <summary>
        /// Empresa/uf do cte
        /// </summary>
        public CteSerieEmpresaUf CteSerieEmpresaUf { get; set; }
    }

    /// <summary>
    /// Dto de grava��o do CTe
    /// </summary>
    public class GravacaoCteDto
    {
        /// <summary>
        /// CT-e que est� sendo feito a manuten��o, caso seja um novo CT-e estar� nulo
        /// </summary>
        public Cte CtePai { get; set; }

        /// <summary>
        /// Esta��o m�e de origem do fluxo comercial
        /// </summary>
        public EstacaoMae Origem { get; set; }

        /// <summary>
        /// Esta��o m�e de destino do fluxo comercial
        /// </summary>
        public EstacaoMae Destino { get; set; }

        /// <summary>
        /// Despacho que est� sendo utilizado para o CTe
        /// </summary>
        public DespachoTranslogic Despacho { get; set; }

        /// <summary>
        /// Vag�o que est� sendo gerado o CTe
        /// </summary>
        public Vagao Vagao { get; set; }

        /// <summary>
        /// Lista de detalhes do CTe
        /// </summary>
        public IList<GravacaoCteDetalheDto> ListaDetalhes { get; set; }
    }

    /// <summary>
    /// Dto de detalhe de grava��o do CTe
    /// </summary>
    public class GravacaoCteDetalheDto
    {
        // TODO: COLOCAR CAMPOS DE DETALHE DO CTE
    }*/
}