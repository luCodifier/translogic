﻿namespace Translogic.Modules.Core.Domain.Services.Ctes
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using ICSharpCode.SharpZipLib.Core;
    using iTextSharp;
    using iTextSharp.text.pdf;
    using Model.Diversos;
    using Model.Diversos.CodigoBarras;
    using Model.Diversos.CodigoBarras.Repositories;
    
    /// <summary>
    /// Gerar Codigo Barras
    /// </summary>
    public class CodigoBarrasService
    {
        private readonly ICodigoBarrasRepository _codigoBarrasRepository;
        private const string Numeros = "0123456789";
        private const string StartA = "STARTA";
        private const string StartB = "STARTB";
        private const string StartC = "STARTC";
        private const string Start = "START";
        private const string Stop = "STOP";
        private const string End = "END";
        private const string Quietzone = "QUIETZONE";
        private const string Code128C = "CODE128C";
        private MemoryStream _fileMemoryStream;
        private string _nomeArquivo;

        /// <summary>
        ///  Initializes a new instance of the <see cref="CodigoBarrasService"/> class.
        /// </summary>
        /// <param name="codigoBarrasRepository">Repositório do Codigo Barras</param>
        public CodigoBarrasService(ICodigoBarrasRepository codigoBarrasRepository)
        {
            _codigoBarrasRepository = codigoBarrasRepository;
        }
        
        #region PROPRIEDADES
        
        /// <summary>
        /// Memory stream com arquivo pdf cte
        /// </summary>
        public MemoryStream ArquivoPdfCte
        {
            get { return _fileMemoryStream; }
        }

        /// <summary>
        /// Nome do arquivo PDF gerado
        /// </summary>
        public string NomeArquivo
        {
            get { return _nomeArquivo; }
        }

        #endregion

        /// <summary>
        /// Retorn código de barras binário
        /// </summary>
        /// <param name="valor">Valor a ser convertido</param>
        /// <param name="tipo">Tipo do Código de barras</param>
        /// <returns>Binario código barras</returns>
        public string GerarCodigoBarras(string valor, string tipo)
        {
            string valorBarra = String.Empty;
            List<bool> codigoBarras = new List<bool>();
		
		    if (valor != String.Empty && valor.Length > 0) 
            {
			    valor = Validar(valor, tipo);

                if (valor.Equals("Error"))
                {
                    return null;
                }

			    string str = Quietzone;
			    string cod = GetCodificacao(str, tipo);
			    codigoBarras.AddRange(Converter(cod));
            
			    str = Start;
			    cod = GetCodificacao(str, tipo);
                codigoBarras.AddRange(Converter(cod));
			
			    for (int i = 0; i < valor.Length; i += 2) 
                {
				    str = valor.Substring(i, 2);
				    cod = GetCodificacao(str, tipo);
                    codigoBarras.AddRange(Converter(cod));
			    }
			
			    int checksum = GetChecksum(valor, tipo);
			    cod = GetCodificacao(checksum, tipo);
                codigoBarras.AddRange(Converter(cod));
			
			    str = Stop;
			    cod = GetCodificacao(str, tipo);
                codigoBarras.AddRange(Converter(cod));
			
			    str = End;
			    cod = GetCodificacao(str, tipo);
                codigoBarras.AddRange(Converter(cod));
			
			    str = Quietzone;
			    cod = GetCodificacao(str, tipo);
                codigoBarras.AddRange(Converter(cod));
		    }

            foreach (var v in codigoBarras)
            {
                valorBarra += v ? "1" : "0";
            }

            return valorBarra;
	}

        /// <summary>
        /// Converter para string
        /// </summary>
        /// <param name="codificacao">Codificacao para binario</param>
        /// <returns>Binario código barras</returns>
        public List<bool> Converter(string codificacao)
        {
            List<bool> codBarras = new List<bool>();
            
            if (codificacao != null)
            {
                char[] ch = codificacao.ToCharArray();
                codBarras.AddRange(ch.Select(t => t == '0' ? false : true));
            }

            return codBarras;
        }
	
        /// <summary>
        /// Converter código de Barras
        /// </summary>
        /// <param name="codigoBarras">Codigo Barras</param>
        /// <returns>String em binário</returns>
        public string Converter(List<bool> codigoBarras)
        {
            string codificacao = String.Empty;

            foreach (bool b in codigoBarras)
            {
                codificacao += b ? "1" : "0";
            }
	
		return codificacao;
	   }

        /// <summary>
        /// Preencher o PDF  com valores CTE Dto
        /// </summary>
        /// <param name="idcte">Codigo Cte para impressão</param>
        /// <param name="chavecte">Chave Cte para impressão</param>
        public void PreencheLayoutPdfCte(string idcte, string chavecte)
        {
            FileStream filePdf;
            string valorBarra = String.Empty;
            string formFile = @"D:\Projetos\Translogic\Branches\Cte\src\Modules\Translogic.Modules.Core\Relatorio\DACTEFERR_ALL_Layout.pdf";
            string pathFile = @"D:\Projetos\Translogic\Branches\Cte\src\Modules\Translogic.Modules.Core\Relatorio\" + chavecte + ".pdf";
            _nomeArquivo = chavecte;

            filePdf = new FileStream(pathFile, FileMode.Create);
            
            PdfReader reader = new PdfReader(formFile);
            PdfStamper stamper = new PdfStamper(reader, filePdf);
            AcroFields fields = stamper.AcroFields;

            fields.SetField("TITULONOME", "Henderson Cantador Buhr");
            fields.SetField("TITULORG", "7101718-4");
            fields.SetField("TITULODATACHEGADA", DateTime.Now.ToString("dd/MM/yyyy"));
            fields.SetField("TITULOHORACHEGADA", DateTime.Now.Hour.ToString("hh:MM:ss"));
            fields.SetField("TITULODATASAIDA", DateTime.Now.ToString("dd/MM/yyyy"));

            valorBarra = GerarCodigoBarras(chavecte, "CODE128C");
            fields.SetField("DACTECodigoBarra", valorBarra.Replace("0", "a"));
            fields.SetField("DACTECHAVECTE", chavecte);
            
            stamper.FormFlattening = true;
            stamper.Close();
            
            filePdf = new FileStream(pathFile, FileMode.Open);
            int length = Convert.ToInt32(filePdf.Length);
            MemoryStream ms = new MemoryStream(length);

            BinaryReader br = new BinaryReader(filePdf);
            byte[] byteBuffer = br.ReadBytes(length);
            ms.Write(byteBuffer, 0, length);
            ms.Seek(0, SeekOrigin.Begin);
            _fileMemoryStream = ms;
        } 

        /// <summary>
        /// Validar valores Código Barras
        /// </summary>
        /// <param name="valor">Valor Código Barras</param>
        /// <param name="tipo">Tipo Código Barras</param>
        /// <returns>String validada</returns>
        private string Validar(string valor, string tipo)
        {
            if (Code128C.Equals(tipo))
            {
                char[] ch = valor.ToCharArray();

                if (ch.Any(t => Numeros.IndexOf(t) < 0))
                {
                    return "Error";
                }

                if (valor.Length % 2 == 1)
                {
                    valor = "0" + valor;
                }
            }
            else
            {
                return "Error";
            }

            return valor;
	   }
	
        /// <summary>
        /// Get Check sum
        /// </summary>
        /// <param name="valor">Valor Código Barras</param>
        /// <param name="tipo">Tipo Código Barras</param>
        /// <returns>Inteiro com check sum</returns>
        private int GetChecksum(string valor, string tipo)
        {
            if (Code128C.Equals(tipo))
            {
                int start = GetIndice(Start, tipo);
                int checksum = start;

                for (int i = 0, peso = 1; i < valor.Length; i += 2, peso++)
                {
                    string str = valor.Substring(i, 2);
                    checksum += GetIndice(str, tipo) * peso;
                }

                return checksum % 103;
            }
            else
            {
                return -1;
            }
        }

        /// <summary>
        /// Get Codificacao Codigo barras por Indice
        /// </summary>
        /// <param name="indice">Indice código Barras</param>
        /// <param name="tipo">Tipo código Barras</param>
        /// <returns>String codificada</returns>
        private string GetCodificacao(int indice, string tipo)
        {
            CodigoBarras c = _codigoBarrasRepository.FindByIndiceAndTipo(indice, tipo);

            if (c != null)
            {
                return c.Codificacao;
            }

            return null;
	   }

        /// <summary>
        /// Get Codificacao Codigo barras
        /// </summary>
        /// <param name="valor">Valor código Barras</param>
        /// <param name="tipo">Tipo código Barras</param>
        /// <returns>String codificada</returns>
        private string GetCodificacao(string valor, string tipo)
        {
	        CodigoBarras c = _codigoBarrasRepository.FindByValorAndTipo(valor, tipo);

            if (c != null)
            {
                return c.Codificacao;
            }

            return null;
	    }

            /// <summary>
            /// Retorna o ìndice do código
            /// </summary>
            /// <param name="valor">Valor do indice</param>
            /// <param name="tipo">Tipo do Indice</param>
            /// <returns>Indice da barra</returns>
           private int GetIndice(string valor, string tipo) 
           {
		        CodigoBarras c = _codigoBarrasRepository.FindByValorAndTipo(valor, tipo);

                if (c != null)
			    {
			        return c.Indice;
			    }

		        return -1;
	    }
    }
}