﻿namespace Translogic.Modules.Core.Domain.Services.Ctes
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.ServiceModel;
    using System.ServiceModel.Activation;

    using ALL.Core.AcessoDados;

    using NHibernate;

    using Translogic.Modules.Core.Domain.Model.CteFerroviarioCancelado;
    using Translogic.Modules.Core.Domain.Model.CteFerroviarioCancelado.Outputs;
    using Translogic.Modules.Core.Domain.Model.Diversos.Cte;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos.Repositories;
    using Translogic.Modules.Core.Domain.Model.WebServices.CteFerroviarioCancelado.Outputs;
    using Translogic.Modules.Core.Domain.Services.Ctes.Interface;

    /// <summary>
    ///     serviço de envio de informações do cte
    /// </summary>
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceBehavior]
    public class CteFerroviarioCanceladoService : ICteFerroviarioCanceladoService
    {
        #region Fields

        private readonly ICteCanceladoRefaturamentoRepository _cteCanceladoRefaturamentoRepository;

        private readonly ICteEmpresasRepository _cteEmpresaRepository;

        private readonly ICteFerroviarioCanceladoRepository _cteFerroviarioCanceladoRepository;

        private readonly ICteRepository _cteRepository;

        private readonly IDespachoTranslogicRepository _despachoTranslogicRepository;

        private readonly INotaFiscalTranslogicRepository _notaFiscalTranslogicRepository;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///     contrutor da classe
        /// </summary>
        /// <param name="cteFerroviarioCanceladoRepository">objeto instanciado</param>
        /// <param name="notaFiscalTranslogicRepository">objeto instanciado</param>
        /// <param name="despachoTranslogicRepository">objeto instanciado</param>
        /// <param name="cteCanceladoRefaturamentoRepository">objeto instanciado</param>
        /// <param name="cteEmpresaRepository">objeto instanciado</param>
        /// <param name="cteRepository">objeto instanciado</param>
        public CteFerroviarioCanceladoService(
            ICteFerroviarioCanceladoRepository cteFerroviarioCanceladoRepository,
            INotaFiscalTranslogicRepository notaFiscalTranslogicRepository,
            IDespachoTranslogicRepository despachoTranslogicRepository,
            ICteCanceladoRefaturamentoRepository cteCanceladoRefaturamentoRepository,
            ICteEmpresasRepository cteEmpresaRepository,
            ICteRepository cteRepository)
        {
            this._cteFerroviarioCanceladoRepository = cteFerroviarioCanceladoRepository;
            this._notaFiscalTranslogicRepository = notaFiscalTranslogicRepository;
            this._despachoTranslogicRepository = despachoTranslogicRepository;
            this._cteCanceladoRefaturamentoRepository = cteCanceladoRefaturamentoRepository;
            this._cteEmpresaRepository = cteEmpresaRepository;
            this._cteRepository = cteRepository;
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     Obtém os Ctes Cancelados com o Recebedor Diferente do Próximo Cte para Refaturamento da Base Consolidada
        /// </summary>
        /// <param name="request">informaçoes de requisição</param>
        /// <returns>Retorna dados do cte</returns>
        public EletronicBillofLadingCancelledRailResponse InformationCte(
            EletronicBillofLadingCancelledRailRequest request)
        {
            string message;
            var listaEletronicBillofLadingCancelledData = new List<EletronicBillofLadingCancelledData>();
            EletronicBillofLadingCancelledRailResponse response;

            if (request.ElectronicBillofLadingCancelledRailType.ElectronicBillofLadingCancelledRailDetail.StartDate
                > request.ElectronicBillofLadingCancelledRailType.ElectronicBillofLadingCancelledRailDetail.EndDate)
            {
                message = "ERRO-003: Data Inicial deve ser menor que a data Final";
            }
            else
            {
                bool maior30Dias =
                    (request.ElectronicBillofLadingCancelledRailType.ElectronicBillofLadingCancelledRailDetail.EndDate
                     - request.ElectronicBillofLadingCancelledRailType.ElectronicBillofLadingCancelledRailDetail
                           .StartDate).TotalDays > 30;
                DateTime inicio =
                    request.ElectronicBillofLadingCancelledRailType.ElectronicBillofLadingCancelledRailDetail.StartDate;
                DateTime fim = maior30Dias
                                   ? request.ElectronicBillofLadingCancelledRailType
                                         .ElectronicBillofLadingCancelledRailDetail.StartDate.AddDays(31)
                                   : request.ElectronicBillofLadingCancelledRailType
                                         .ElectronicBillofLadingCancelledRailDetail.EndDate;
                string dataFim;

                if (
                    (request.ElectronicBillofLadingCancelledRailType.ElectronicBillofLadingCancelledRailDetail.EndDate
                     - request.ElectronicBillofLadingCancelledRailType.ElectronicBillofLadingCancelledRailDetail
                           .StartDate).TotalDays > 30)
                {
                    dataFim =
                        request.ElectronicBillofLadingCancelledRailType.ElectronicBillofLadingCancelledRailDetail
                            .StartDate.AddDays(30).ToShortDateString();
                }
                else
                {
                    dataFim =
                        request.ElectronicBillofLadingCancelledRailType.ElectronicBillofLadingCancelledRailDetail
                            .EndDate.ToShortDateString();
                }

                string dataIni =
                    request.ElectronicBillofLadingCancelledRailType.ElectronicBillofLadingCancelledRailDetail.StartDate
                        .ToShortDateString();

                message = "PESQUISA REALIZADA ENTRE OS DIAS " + dataIni + " E " + dataFim;

                do
                {
                    IList<CteCanceladoRefaturamento> listaCtesCanceladosRefaturamentoPorData =
                        this._cteCanceladoRefaturamentoRepository.ObterCtesCanceladosRefaturamentoBaseConsolidadaPorData
                            (inicio);
                    foreach (
                        CteCanceladoRefaturamento cteCanceladoRefaturamento in listaCtesCanceladosRefaturamentoPorData)
                    {
                        List<WayBillsInfo> listaWayBillsInfoCancelado;
                        List<WayBillsInfo> listaWayBillsInfoNovo;

                        listaWayBillsInfoCancelado =
                            (from notaCteCancelado in
                                 cteCanceladoRefaturamento.ListaNotas.Where(
                                     n => !string.IsNullOrEmpty(n.NotaCanceladaChave) && n.NotaCanceladaId != null)
                                 .ToList()
                             where notaCteCancelado.Id != null
                             select
                                 new WayBillsInfo
                                     {
                                         WaybillId =
                                             (decimal)notaCteCancelado.NotaCanceladaId,
                                         WaybillKey = notaCteCancelado.NotaCanceladaChave
                                     })
                                .ToList();

                        listaWayBillsInfoNovo =
                            (from nota in
                                 cteCanceladoRefaturamento.ListaNotas.Where(
                                     n => !string.IsNullOrEmpty(n.NotaChave) && n.NotaId != null).ToList()
                             where nota.Id != null
                             select
                                 new WayBillsInfo
                                     {
                                         WaybillId = (decimal)nota.NotaId,
                                         WaybillKey = nota.NotaChave
                                     }).ToList();

                        var dado = new EletronicBillofLadingCancelledData
                                       {
                                           CteCancelledId =
                                               (decimal)
                                               cteCanceladoRefaturamento
                                                   .CteCanceladoId,
                                           CteLadingId =
                                               (decimal)
                                               cteCanceladoRefaturamento.CteNovoId,
                                           CteCancelledKey =
                                               cteCanceladoRefaturamento
                                               .CteCanceladoChave,
                                           CteLadingKey =
                                               cteCanceladoRefaturamento.CteNovoChave,
                                           WayBillsInfoCancelled =
                                               listaWayBillsInfoCancelado,
                                           WayBillsInfoLading = listaWayBillsInfoNovo
                                       };
                        listaEletronicBillofLadingCancelledData.Add(dado);
                    }

                    inicio = inicio.AddDays(1);
                }
                while (!inicio.Equals(fim));
            }

            response = new EletronicBillofLadingCancelledRailResponse
                           {
                               EletronicBillofLadingCancelledRailType =
                                   new EletronicBillofLadingCancelledRailType
                                       {
                                           EletronicBillOfLadingCancelledRailDetail
                                               =
                                               new EletronicBillOfLadingCancelledRailDetail
                                                   {
                                                       EletronicBillofLadingCancelledData
                                                           =
                                                           listaEletronicBillofLadingCancelledData,
                                                   },
                                           VersionIdentifier
                                               =
                                               "1.0",
                                           Message
                                               =
                                               message
                                       }
                           };

            return response;
        }

        /// <summary>
        ///     Obtém os Ctes Cancelados com o Recebedor Diferente do Próximo Cte para Refaturamento
        /// </summary>
        /// <param name="request">informaçoes de requisição</param>
        /// <returns>Retorna dados do cte</returns>
        public EletronicBillofLadingCancelledRailResponse ObterCtesCanceladosParaRefaturamento(
            EletronicBillofLadingCancelledRailRequest request)
        {
            var listaEletronicBillofLadingCancelledData = new List<EletronicBillofLadingCancelledData>();

            string messageDespachosCancelados;
            IList<DespachoCanceladoDto> despachosCancelados = this.ObterDespachosCancelados(
                request,
                out messageDespachosCancelados);

            foreach (DespachoCanceladoDto despachoCancelado in despachosCancelados)
            {
                if (!string.IsNullOrEmpty(despachoCancelado.IdProximoDespacho))
                {
                    Cte cteCancelado =
                        this._cteRepository.ObterCtesPorDespacho((int)despachoCancelado.IdDespachoCancelado)
                            .Where(c => this._cteRepository.ExisteStatusRetornoCte((int)c.Id, 220))
                            .OrderByDescending(c => c.DataHora)
                            .FirstOrDefault();

                    if (cteCancelado != null)
                    {
                        string idsProximoDespacho = despachoCancelado.IdProximoDespacho;
                        idsProximoDespacho = idsProximoDespacho ?? string.Empty;
                        int[] idsProximoDespachoSeparados =
                            idsProximoDespacho.ToUpper().Trim().Split('-').Select(int.Parse).ToArray();
                        foreach (int idProximoDespacho in idsProximoDespachoSeparados)
                        {
                            Cte cteNovo =
                                this._cteRepository.ObterCtesPorDespacho(idProximoDespacho)
                                    .FirstOrDefault(c => c.SituacaoAtual == SituacaoCteEnum.Autorizado);

                            if (cteNovo != null && this.ValidarSeOcorreuAlteracaoRecebedorCte(cteCancelado, cteNovo))
                            {
                                List<WayBillsInfo> listaNotasCancelado =
                                    this.ObterWayBillsInfoPorDespacho((int)despachoCancelado.IdDespachoCancelado, true);
                                List<WayBillsInfo> listaNotasNovo = this.ObterWayBillsInfoPorDespacho(
                                    idProximoDespacho,
                                    false);

                                var dado = new EletronicBillofLadingCancelledData
                                               {
                                                   CteCancelledId =
                                                       (decimal)cteCancelado.Id,
                                                   CteLadingId = (decimal)cteNovo.Id,
                                                   CteCancelledKey =
                                                       cteCancelado.Chave,
                                                   CteLadingKey = cteNovo.Chave,
                                                   WayBillsInfoCancelled =
                                                       listaNotasCancelado,
                                                   WayBillsInfoLading =
                                                       listaNotasNovo
                                               };
                                listaEletronicBillofLadingCancelledData.Add(dado);
                            }
                        }
                    }
                }
            }

            string messageCtesCanceladosSemDespachoCancelado;
            IList<EletronicBillofLadingCancelledData> listaCtesCanceladosSemDespachoCancelado =
                this.ObterCtesCanceladosSemDespachoCancelado(request, out messageCtesCanceladosSemDespachoCancelado);
            IList<EletronicBillofLadingCancelledData> listaCtesCanceladosSemDespachoCanceladoAlterouRecebedor =
                new List<EletronicBillofLadingCancelledData>();

            foreach (EletronicBillofLadingCancelledData eletronicBillofLadingCancelledData in
                listaCtesCanceladosSemDespachoCancelado)
            {
                Cte cteCancelado = this._cteRepository.ObterPorId(
                    (int)eletronicBillofLadingCancelledData.CteCancelledId);
                Cte cteNovo = this._cteRepository.ObterPorId((int)eletronicBillofLadingCancelledData.CteLadingId);
                if (this.ValidarSeOcorreuAlteracaoRecebedorCte(cteCancelado, cteNovo))
                {
                    listaCtesCanceladosSemDespachoCanceladoAlterouRecebedor.Add(eletronicBillofLadingCancelledData);
                }
            }

            listaEletronicBillofLadingCancelledData.AddRange(listaCtesCanceladosSemDespachoCanceladoAlterouRecebedor);

            var response = new EletronicBillofLadingCancelledRailResponse
                               {
                                   EletronicBillofLadingCancelledRailType =
                                       new EletronicBillofLadingCancelledRailType
                                           {
                                               EletronicBillOfLadingCancelledRailDetail
                                                   =
                                                   new EletronicBillOfLadingCancelledRailDetail
                                                       {
                                                           EletronicBillofLadingCancelledData
                                                               =
                                                               listaEletronicBillofLadingCancelledData,
                                                       },
                                               VersionIdentifier
                                                   =
                                                   "1.0",
                                               Message
                                                   =
                                                   messageDespachosCancelados
                                                       .Equals
                                                       (
                                                           messageCtesCanceladosSemDespachoCancelado)
                                                       ? messageDespachosCancelados
                                                       : string
                                                             .Format
                                                             (
                                                                 "{0} {1}",
                                                                 messageDespachosCancelados,
                                                                 messageCtesCanceladosSemDespachoCancelado)
                                           }
                               };

            return response;
        }

        /// <summary>
        ///     informações do cte
        /// </summary>
        /// <param name="request">informaçoes de requisição</param>
        /// <param name="message">Mensagem de retorno</param>
        /// <returns>Retorna dados do cte</returns>
        public IList<EletronicBillofLadingCancelledData> ObterCtesCanceladosSemDespachoCancelado(
            EletronicBillofLadingCancelledRailRequest request,
            out string message)
        {
            return this._cteFerroviarioCanceladoRepository.ObterCtesCanceladosSemDespachoCancelado(request, out message);
        }

        /// <summary>
        ///     Despachos cancelados
        /// </summary>
        /// <param name="request">informações da requisição</param>
        /// <param name="message">Mensagem de retorno</param>
        /// <returns>Retorna dados dos despachos cancelados</returns>
        public IList<DespachoCanceladoDto> ObterDespachosCancelados(
            EletronicBillofLadingCancelledRailRequest request,
            out string message)
        {
            return this._cteFerroviarioCanceladoRepository.ObterDespachosCancelados(request, out message);
        }

        /// <summary>
        ///     Obtém o id do Recebedor da CteEmpresas pelo id do Cte
        /// </summary>
        /// <param name="cte">Objeto Cte</param>
        /// <returns>Retorna o id do Recebedor</returns>
        public int? ObterRecebedorCteEmpresas(Cte cte)
        {
            if (cte != null)
            {
                CteEmpresas cteEmpresas = this._cteEmpresaRepository.ObterPorCte(cte);
                if (cteEmpresas != null)
                {
                    if (cteEmpresas.EmpresaRecebedor != null)
                    {
                        return cteEmpresas.EmpresaRecebedor.Id;
                    }
                    return null;
                }
                return null;
            }
            return null;
        }

        /// <summary>
        ///     Obtem a lista de notas retornadas para o despacho
        /// </summary>
        /// <param name="idDespacho">Identificador do despacho</param>
        /// <param name="cancelado">Flag para indicar se é um cte cancelado ou atual</param>
        /// <returns>Retorna lista de notas por despacho</returns>
        public List<WayBillsInfo> ObterWayBillsInfoPorDespacho(int idDespacho, bool cancelado)
        {
            IList<NotaFiscalTranslogic> listaNotasDespacho =
                this._notaFiscalTranslogicRepository.ObterNotaFiscalPorDespacho(
                    this._despachoTranslogicRepository.ObterPorId(idDespacho));

            List<WayBillsInfo> listaWayBillsInfo;

            if (cancelado)
            {
                listaWayBillsInfo = (from nota in listaNotasDespacho
                                     where nota.Id != null
                                     select
                                         new WayBillsInfo
                                             {
                                                 WaybillId = (decimal)nota.Id,
                                                 WaybillKey = nota.ChaveNotaFiscalEletronica
                                             }).ToList();
            }
            else
            {
                listaWayBillsInfo = (from nota in listaNotasDespacho
                                     where nota.Id != null
                                     select
                                         new WayBillsInfo
                                             {
                                                 WaybillId = (decimal)nota.Id,
                                                 WaybillKey = nota.ChaveNotaFiscalEletronica
                                             }).ToList
                    ();
            }

            return listaWayBillsInfo;
        }

        /// <summary>
        ///     Verifica se ocorreu alteração de recbedor do Cte
        /// </summary>
        /// <param name="cteCancelado">Objeto Cte Cancelado</param>
        /// <param name="cteNovo">Objeto Cte Novo</param>
        /// <returns>True se ocorreu alteração</returns>
        public bool ValidarSeOcorreuAlteracaoRecebedorCte(Cte cteCancelado, Cte cteNovo)
        {
            int? recebedorCteCancelado = this.ObterRecebedorCteEmpresas(cteCancelado);
            int? recebedorCtenovo = this.ObterRecebedorCteEmpresas(cteNovo);

            if (recebedorCteCancelado != recebedorCtenovo)
            {
                return true;
            }

            return false;
        }

        #endregion
    }
}