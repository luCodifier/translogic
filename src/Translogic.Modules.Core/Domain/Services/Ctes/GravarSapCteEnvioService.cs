using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Conteiner.Repositories;

namespace Translogic.Modules.Core.Domain.Services.Ctes
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Net;
    using System.Text;
    using Castle.Services.Transaction;
    using FluxosComerciais;
    using Model.Acesso;
    using Model.Diversos;
    using Model.Diversos.Cte;
    using Model.Diversos.Repositories;
    using Model.Estrutura;
    using Model.Estrutura.Repositories;
    using Model.FluxosComerciais;
    using Model.FluxosComerciais.ConhecimentoTransporteEletronico;
    using Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;
    using Model.FluxosComerciais.InterfaceSap;
    using Model.FluxosComerciais.InterfaceSap.Repositories;
    using Model.FluxosComerciais.Pedidos;
    using Model.FluxosComerciais.Pedidos.Despachos;
    using Model.FluxosComerciais.Pedidos.Despachos.Repositories;
    using Model.FluxosComerciais.Pedidos.Repositories;
    using Model.FluxosComerciais.Repositories;
    using Model.Trem.Veiculo.Vagao;
    using Model.Via;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.TriangulusConfig;
    using Util;

    /// <summary>
    /// Classe para gravar os dados do CT-e nas tabelas do SAP
    /// </summary>
    [Transactional]
    public class GravarSapCteEnvioService
    {
        private readonly ICteCanceladoRefaturamentoConfigRepository _cteCanceladoRefaturamentoConfigRepository;
        private readonly ICteInutilizacaoConfigRepository _cteInutilizacaoConfigRepository;
        private readonly IItemDespachoRepository _itemDespachoRepository;
        private readonly IAssociaFluxoInternacionalRepository _associaFluxoInternacionalRepository;
        private readonly IComposicaoFreteContratoRepository _composicaoFreteContratoRepository;
        private readonly ICteDetalheRepository _cteDetalheRepository;
        private readonly ICteComplementadoRepository _cteComplementadoRepository;
        private readonly ICteArvoreRepository _cteArvoreRepository;
        private readonly ICteAgrupamentoRepository _cteAgrupamentoRepository;
        private readonly IDespachoTranslogicRepository _despachoTranslogicRepository;
        private readonly ISerieDespachoUfRepository _serieDespachoUfRepository;
        private readonly IVagaoPedidoRepository _vagaoPedidoRepository;
        private readonly ICotacaoRepository _cotacaoRepository;
        private readonly IEmpresaClienteRepository _empresaClienteRepository;
        private readonly IComposicaoFreteCteRepository _composicaoFreteCteRepository;

        private readonly ISapDespCteRepository _sapDespCteRepository;
        private readonly ISapNotasCteRepository _sapNotasCteRepository;
        private readonly ISapCteAnuladoRepository _sapCteAnuladoRepository;
        private readonly ISapConteinerCteRepository _sapConteinerCteRepository;
        private readonly ISapDocsoutrasferCteRepository _sapDocsoutrasferCteRepository;
        private readonly ISapCteComplementadoRepository _sapCteComplementadoRepository;
        private readonly ISapVagaoCteRepository _sapVagaoCteRepository;
        private readonly ISapChaveCteRepository _sapChaveCteRepository;
        private readonly ISapComposicaoFreteCteRepository _sapComposicaoFreteCteRepository;
        private readonly ICteRepository _cteRepository;
        private readonly ICteInterfaceEnvioSapRepository _cteInterfaceEnvioSapRepository;
        private readonly IEmpresaFerroviaRepository _empresaFerroviaRepository;
        private readonly ICteStatusRepository _cteStatusRepository;
        private readonly IEmpresaInterfaceCteRepository _empresaInterfaceCteRepository;
        private readonly ICteSapHistoricoRepository _cteSapHistoricoRepository;
        private readonly CarregamentoService _carregamentoService;
        private readonly CteService _cteService;
        private readonly FilaProcessamentoService _filaProcessamentoService;
        private readonly ICteEmpresasRepository _cteEmpresasRepository;
        private readonly IConteinerRepository _conteinerRepository;
        private readonly ICteOrigemRepository _cteOrigemRepository;
        private readonly ICteAnuladoRepository _cteAnuladoRepository;

        private SapDespCte _sapDespCte;
        private Cte _cte;
        private Vw209ConsultaCte _consultaCte;
        private Cte _cteComplementar;
        private CteComplementado _cteComplementado;
        private CteAnulado _cteAnulado;
        private DespachoTranslogic _despachoTranslogic;
        private SerieDespachoUf _serieDespachoUf;
        private FluxoComercial _fluxoComercial;
        private IEmpresa _empresaRemetenteFluxoComercial;
        private Cte _cteRaiz;
        private Cte _ctePai;
        private CteAgrupamento _cteAgrupamento;
        private CteVersao _cteVersao;
        private ItemDespacho _itemDespacho;
        private DetalheCarregamento _detalheCarregamento;
        private Carregamento _carregamento;
        private ContratoHistorico _contratoHistorico;
        private Vagao _vagao;
        private VagaoPedido _vagaoPedido;
        private IEmpresa _empresaProprietariaVagao;
        private IList<CteDetalhe> _listaCteDetalhe;
        private IList<CteComplementado> _listaCteComplementado;
        private CteLogService _cteLogService;
        private Contrato _contrato;
        private PedidoDerivado _pedidoDerivado;
        private IEmpresa _empresaFerroviaCliente;
        private Cte _cteRaizAgrupamento;
        private TipoTrafegoMutuoEnum _tipoTrafegoMutuo;
        private EstacaoMae _estacaoMaeOrigemFluxoComercial;
        private EstacaoMae _estacaoMaeDestinoFluxoComercial;
        private bool _partilhaFerroeste;
        private bool _origemFerroeste;

        /// <summary>
        /// Initializes a new instance of the <see cref="GravarSapCteEnvioService"/> class.
        /// </summary>
        /// <param name="itemDespachoRepository">Reposit�rio do ItemDespacho injetado</param>
        /// <param name="composicaoFreteContratoRepository">Reposit�rio da composicao frete contrato injetado</param>
        /// <param name="cteDetalheRepository">Reposit�rio detalhe do cte injetado</param>
        /// <param name="cteArvoreRepository">Reposit�rio de cte arvore injetado</param>
        /// <param name="cteAgrupamentoRepository">Reposit�rio de cte Agrupamento injetado</param>
        /// <param name="despachoTranslogicRepository">Reposit�rio do despachoTranslogic injetado</param>
        /// <param name="associaFluxoInternacionalRepository">Reposit�rio do associa fluxo internacional injetado</param>
        /// <param name="cteLogService">Servi�o de log do Cte injetado</param>
        /// <param name="serieDespachoUfRepository">Repositorio de serie de despacho uf</param>
        /// <param name="vagaoPedidoRepository">Reposit�rio de Vagao Pedido injetado</param>
        /// <param name="cotacaoRepository">Reposit�rio de Cotacao injetado</param>
        /// <param name="empresaClienteRepository">Reposit�rio de Empresa Cliente injetado</param>
        /// <param name="composicaoFreteCteRepository">Reposit�rio de Composicao Frete Cte injetado</param>
        /// <param name="sapDespCteRepository">Reposit�rio de Sap Deps Cte injetado</param>
        /// <param name="sapNotasCteRepository">Reposit�rio de Sap Notas Cte injetado</param>
        /// <param name="sapConteinerCteRepository">Reposit�rio de Sap Conteiner Cte injetado</param>
        /// <param name="sapDocsoutrasferCteRepository">Reposit�rio de Sap Docs Outras Ferrovias Cte injetado</param>
        /// <param name="sapCteComplementadoRepository">Reposit�rio Sap Cte Complementado injetado</param>
        /// <param name="sapVagaoCteRepository">Reposit�rio de Sap Vagao Cte injetado</param>
        /// <param name="sapChaveCteRepository">Reposit�rio de Sap Chave Cte injetado</param>
        /// <param name="sapComposicaoFreteCteRepository">Reposit�rio de Sap Composicao Frete Cte injetado</param>
        /// <param name="cteRepository">Repositorio do CTE injetado</param>
        /// <param name="cteInterfaceEnvioSapRepository">Repositorio de CteInterfaceEnvioSap injetado</param>
        /// <param name="empresaFerroviaRepository">Reposit�rio da empresa ferrovia injetado</param>
        /// <param name="cteStatusRepository">Reposit�rio do status do cte injetado</param>
        /// <param name="cteComplementadoRepository">Reposit�rio do cte complementado injetado</param>
        /// <param name="empresaInterfaceCteRepository">Reposit�rio da empresa Interface cte injetado</param>
        /// <param name="cteSapHistoricoRepository">Reposit�rio do sap historico injetado</param>
        /// <param name="carregamentoService">Servi�o de carregamento injetado</param>
        /// <param name="filaProcessamentoService">Servi�o de fila de processamento</param>
        /// <param name="cteEmpresasRepository">Reposit�rio da cte empresas injetado.</param>
        /// <param name="sapCteAnuladoRepository">Reposit�rio da sap cte anulado injetado.</param>
        /// <param name="cteCanceladoRefaturamentoConfigRepository">Reposit�rios de ctes cancelados enviados para refaturamento</param>
        /// <param name="cteInutilizacaoConfigRepository">Reposit�rios de ctes inutilizados enviados para config</param>
        /// <param name="conteinerRepository">Reposit�rios utilizado para buscar os conteiner do cte</param>
        /// <param name="cteOrigemRepository">Reposit�rios utilizado para buscar os ctes de origem do CTE</param>
        /// <param name="cteAnuladoRepository">Reposit�rios utilizado para buscar os ctes anulados pelo CTE</param>
        public GravarSapCteEnvioService(IItemDespachoRepository itemDespachoRepository, IComposicaoFreteContratoRepository composicaoFreteContratoRepository, ICteDetalheRepository cteDetalheRepository, ICteArvoreRepository cteArvoreRepository, ICteAgrupamentoRepository cteAgrupamentoRepository, IDespachoTranslogicRepository despachoTranslogicRepository, IAssociaFluxoInternacionalRepository associaFluxoInternacionalRepository, CteLogService cteLogService, ISerieDespachoUfRepository serieDespachoUfRepository, IVagaoPedidoRepository vagaoPedidoRepository, ICotacaoRepository cotacaoRepository, IEmpresaClienteRepository empresaClienteRepository, IComposicaoFreteCteRepository composicaoFreteCteRepository, ISapDespCteRepository sapDespCteRepository, ISapNotasCteRepository sapNotasCteRepository, ISapConteinerCteRepository sapConteinerCteRepository, ISapDocsoutrasferCteRepository sapDocsoutrasferCteRepository, ISapCteComplementadoRepository sapCteComplementadoRepository, ISapVagaoCteRepository sapVagaoCteRepository, ISapChaveCteRepository sapChaveCteRepository, ISapComposicaoFreteCteRepository sapComposicaoFreteCteRepository, ICteRepository cteRepository, ICteInterfaceEnvioSapRepository cteInterfaceEnvioSapRepository, IEmpresaFerroviaRepository empresaFerroviaRepository, ICteStatusRepository cteStatusRepository, ICteComplementadoRepository cteComplementadoRepository, IEmpresaInterfaceCteRepository empresaInterfaceCteRepository, ICteSapHistoricoRepository cteSapHistoricoRepository, CarregamentoService carregamentoService, CteService cteService, FilaProcessamentoService filaProcessamentoService, ICteEmpresasRepository cteEmpresasRepository, ISapCteAnuladoRepository sapCteAnuladoRepository, ICteCanceladoRefaturamentoConfigRepository cteCanceladoRefaturamentoConfigRepository, ICteInutilizacaoConfigRepository cteInutilizacaoConfigRepository, IConteinerRepository conteinerRepository, ICteOrigemRepository cteOrigemRepository, ICteAnuladoRepository cteAnuladoRepository)
        {
            _serieDespachoUfRepository = serieDespachoUfRepository;
            _sapCteAnuladoRepository = sapCteAnuladoRepository;
            _cteCanceladoRefaturamentoConfigRepository = cteCanceladoRefaturamentoConfigRepository;
            _cteInutilizacaoConfigRepository = cteInutilizacaoConfigRepository;
            _cteEmpresasRepository = cteEmpresasRepository;
            _conteinerRepository = conteinerRepository;
            _cteOrigemRepository = cteOrigemRepository;
            _filaProcessamentoService = filaProcessamentoService;
            _carregamentoService = carregamentoService;
            _cteService = cteService;
            _cteSapHistoricoRepository = cteSapHistoricoRepository;
            _empresaInterfaceCteRepository = empresaInterfaceCteRepository;
            _cteComplementadoRepository = cteComplementadoRepository;
            _cteRepository = cteRepository;
            _cteLogService = cteLogService;
            _associaFluxoInternacionalRepository = associaFluxoInternacionalRepository;
            _cteArvoreRepository = cteArvoreRepository;
            _cteAgrupamentoRepository = cteAgrupamentoRepository;
            _despachoTranslogicRepository = despachoTranslogicRepository;
            _cteDetalheRepository = cteDetalheRepository;
            _composicaoFreteContratoRepository = composicaoFreteContratoRepository;
            _itemDespachoRepository = itemDespachoRepository;
            _vagaoPedidoRepository = vagaoPedidoRepository;
            _cotacaoRepository = cotacaoRepository;
            _empresaClienteRepository = empresaClienteRepository;
            _composicaoFreteCteRepository = composicaoFreteCteRepository;
            _cteInterfaceEnvioSapRepository = cteInterfaceEnvioSapRepository;
            _empresaFerroviaRepository = empresaFerroviaRepository;
            _cteStatusRepository = cteStatusRepository;

            _sapDespCteRepository = sapDespCteRepository;
            _sapNotasCteRepository = sapNotasCteRepository;
            _sapConteinerCteRepository = sapConteinerCteRepository;
            _sapDocsoutrasferCteRepository = sapDocsoutrasferCteRepository;
            _sapCteComplementadoRepository = sapCteComplementadoRepository;
            _sapVagaoCteRepository = sapVagaoCteRepository;
            _sapChaveCteRepository = sapChaveCteRepository;
            _sapComposicaoFreteCteRepository = sapComposicaoFreteCteRepository;
            _cteAnuladoRepository = cteAnuladoRepository;
        }

        /// <summary>
        /// Executa a grava��o do cte nas tabelas do SAP
        /// </summary>
        /// <param name="cteInterfaceEnvioSap">Cte interface envio SAP</param>
        /// <param name="usuario"> Usu�rio do Robo </param>
        public void Executar(CteInterfaceEnvioSap cteInterfaceEnvioSap, Usuario usuario)
        {
            try
            {
                if (_cteCanceladoRefaturamentoConfigRepository.ObterPorId(cteInterfaceEnvioSap.Cte.Id) == null && _cteInutilizacaoConfigRepository.ObterPorId(cteInterfaceEnvioSap.Cte.Id) == null)
                {
                    if (cteInterfaceEnvioSap.TipoOperacao.Value == TipoOperacaoCteEnum.Anulacao)
                    {
                        ExecutarPorAnulacaoCte(cteInterfaceEnvioSap.Cte, usuario);
                    }
                    else if (cteInterfaceEnvioSap.Cte.TipoCte == TipoCteEnum.Complementar
                                || cteInterfaceEnvioSap.Cte.TipoCte == TipoCteEnum.Virtual)
                    {
                        ExecutarPorCteComplementar(cteInterfaceEnvioSap.Cte, usuario);
                    }
                    else
                    {
                        ExecutarPorCteRaiz(cteInterfaceEnvioSap.Cte, usuario);
                    }
                }
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(_cte, "GravarSapCteEnvioService", string.Format("Executar: {0}", ex.Message), ex);
                throw;
            }
        }

        /// <summary>
        /// Executa a grava��o do cte nas tabelas do SAP
        /// </summary>
        /// <param name="cte">Cte interface envio SAP</param>
        /// <param name="usuario"> Usu�rio do Robo </param>
        public void Executar(Cte cte, Usuario usuario)
        {
            try
            {
                if (_cteCanceladoRefaturamentoConfigRepository.ObterPorId(cte.Id) == null && _cteInutilizacaoConfigRepository.ObterPorId(cte.Id) == null)
                {
                    CteInterfaceEnvioSap cteInterfaceEnvioSap = _cteInterfaceEnvioSapRepository.ObterPorCte(cte.Id.Value);

                    if (cteInterfaceEnvioSap != null)
                    {
                        if (cteInterfaceEnvioSap.TipoOperacao.Value == TipoOperacaoCteEnum.Anulacao || cteInterfaceEnvioSap.TipoOperacao.Value == TipoOperacaoCteEnum.AnulacaoSefaz)
                        {
                            ExecutarPorAnulacaoCte(cte, usuario);
                        }
                        else if (cte.TipoCte == TipoCteEnum.Virtual)
                        {
                            ExecutarPorCteVirtual(cte, usuario);
                        }
                        else if (cte.TipoCte == TipoCteEnum.Complementar || cte.TipoCte == TipoCteEnum.Virtual)
                        {
                            ExecutarPorCteComplementar(cte, usuario);
                        }
                        else
                        {
                            ExecutarPorCteRaiz(cte, usuario);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                string innerMessage = ex.InnerException == null ? "" : ex.InnerException.Message;
                _cteLogService.InserirLogErro(_cte, "GravarSapCteEnvioService", string.Format("Executar: {0}", ex.Message + innerMessage), ex);
                throw;
            }
        }

        /// <summary>
        /// Executar a grava��o do cte em um m�todo transacional
        /// </summary>
        /// <param name="cteInterfaceEnvioSap"> Cte a ser alterado </param>
        /// <param name="usuario"> Usu�rio do Robo </param>
        [Transaction]
        public virtual void ExecutarPorCteRaiz(CteInterfaceEnvioSap cteInterfaceEnvioSap, Usuario usuario)
        {
            _cte = null;
            _cte = _cteRepository.ObterCtePorHql(cteInterfaceEnvioSap.Cte);

            if (!VerificarCteJaEnviadoSap(_cte.Chave, cteInterfaceEnvioSap.TipoOperacao.Value))
            {
                CarregarDados();
                ValidarDados();
                GravarSapDespCte();

                GravarSapNotasCte();
                GravarSapConteinerCte();
                GravarSapDocsoutrasferCte();

                GravarSapVagaoCte();
                GravarSapChaveCte();
                GravarSapComposicaoFreteCte();

                // CteInterfaceEnvioSap interfSap = listaCteInterfaceEnvioSap.FirstOrDefault(c => c.Cte.Id.Equals(_cte.Id));
                _cteInterfaceEnvioSapRepository.AtualizarEnvioSap(_cte);
                _cteStatusRepository.InserirCteStatus(_cte, usuario, new CteStatusRetorno { Id = 27 }, "Enviado as informa��es do CTE para o SAP");
                _cteLogService.InserirLogInfo(_cte, "GravarSapCteEnvioService", string.Format("GravarSapCteEnvioService: {0}", "Enviado para Interface Sap"));
            }
            else
            {
                _cteLogService.InserirLogInfo(_cte, "GravarSapCteEnvioService", string.Format("ExecutarPorCteRaiz: Cte: {0} j� enviado para a interface SAP", _cte.Id.Value));
                _cteInterfaceEnvioSapRepository.RemoverCteInterfaceEnvioSap(_cte);
            }
        }

        /// <summary>
        /// Executar a grava��o do cte em um m�todo transacional
        /// </summary>
        /// <param name="cte"> Cte a ser alterado </param>
        /// <param name="usuario"> Usu�rio do Robo </param>
        [Transaction]
        public virtual void ExecutarPorAnulacaoCte(Cte cte, Usuario usuario)
        {
            _cte = null;
            _cte = _cteRepository.ObterCtePorHql(cte); 

            CteInterfaceEnvioSap cteInterfaceEnvioSap = _cteInterfaceEnvioSapRepository.ObterPorCte(_cte.Id.Value);

            if (!VerificarCteJaEnviadoSap(_cte.Chave, cteInterfaceEnvioSap.TipoOperacao.Value))
            {
                if (cte.TipoCte == TipoCteEnum.Complementar)
                {
                    CarregarDadosCteComplementar();
                    ValidarDadosCteComplementar();
                    GravarSapDespCteComplementar(usuario);
                }
                else
                {
                    CarregarDados();
                    ValidarDados();
                    GravarSapDespCte();
                }

                //// Se � diferente de anula��o sefaz, grava as notas de anulacao do cte, e anula os ctes pela nota de anulacao, se for sefaz, nao tem notas de anula��o, e anula somente o cte sem ser pela nota.
                if (cteInterfaceEnvioSap.TipoOperacao != TipoOperacaoCteEnum.AnulacaoSefaz)
                {
                    GravarSapNotasAnulacaoCte();
                    GravarSapCteAnulado();
                }
                else
                {
                    GravarSapCteAnulado(cte);
                }

                AtualizarEnvioSap(usuario);
            }
            else
            {
                _cteLogService.InserirLogInfo(_cte, "GravarSapCteEnvioService", string.Format("ExecutarPorAnulacaoCte: Cte: {0} j� enviado para a interface SAP", _cte.Id.Value));
                _cteInterfaceEnvioSapRepository.RemoverCteInterfaceEnvioSap(_cte);
            }
        }

        /// <summary>
        /// Executar a grava��o do cte em um m�todo transacional
        /// </summary>
        /// <param name="cte"> Cte a ser alterado </param>
        /// <param name="usuario"> Usu�rio do Robo </param>
        [Transaction]
        public virtual void ExecutarPorCteVirtual(Cte cte, Usuario usuario)
        {
            _cte = null;
            _cte = _cteRepository.ObterCtePorHql(cte);

            CteInterfaceEnvioSap cteInterfaceEnvioSap = _cteInterfaceEnvioSapRepository.ObterPorCte(_cte.Id.Value);

            if (!VerificarCteJaEnviadoSap(_cte.Chave, cteInterfaceEnvioSap.TipoOperacao.Value))
            {
                CarregarDados();
                ValidarDadosCteVirtual();
                GravarSapDespCteVirtual();
                GravarSapNotasCte();

                GravarSapConteinerCte();

                GravarSapVagaoCte();
                GravarSapChaveCte();
                GravarSapComposicaoFreteCte();

                AtualizarEnvioSap(usuario);
            }
            else
            {
                _cteLogService.InserirLogInfo(_cte, "GravarSapCteEnvioService", string.Format("ExecutarPorCteRaiz: Cte: {0} j� enviado para a interface SAP", _cte.Id.Value));
                _cteInterfaceEnvioSapRepository.RemoverCteInterfaceEnvioSap(_cte);
            }
        }

        /// <summary>
        /// Executar a grava��o do cte em um m�todo transacional
        /// </summary>
        /// <param name="cte"> Cte a ser alterado </param>
        /// <param name="usuario"> Usu�rio do Robo </param>
        [Transaction]
        public virtual void ExecutarPorCteRaiz(Cte cte, Usuario usuario)
        {
            _cte = null;
            _cte = _cteRepository.ObterCtePorHql(cte); 

            CteInterfaceEnvioSap cteInterfaceEnvioSap = _cteInterfaceEnvioSapRepository.ObterPorCte(_cte.Id.Value);

            if (!VerificarCteJaEnviadoSap(_cte.Chave, cteInterfaceEnvioSap.TipoOperacao.Value))
            {
                CarregarDados();
                ValidarDados();
                GravarSapDespCte();
                GravarSapNotasCte();

                GravarSapConteinerCte();
                GravarSapDocsoutrasferCte();

                GravarSapVagaoCte();
                GravarSapChaveCte();
                GravarSapComposicaoFreteCte();

                if (_cte.TipoCte == TipoCteEnum.Anulacao)
                {
                    var cteAnulado = _cteAnuladoRepository.ObterPorIdCteAnulacao(_cte.Id ?? 0).Cte;
                    GravarSapCteAnulado(cteAnulado);
                }

                AtualizarEnvioSap(usuario);
            }
            else
            {
                _cteLogService.InserirLogInfo(_cte, "GravarSapCteEnvioService", string.Format("ExecutarPorCteRaiz: Cte: {0} j� enviado para a interface SAP", _cte.Id.Value));
                _cteInterfaceEnvioSapRepository.RemoverCteInterfaceEnvioSap(_cte);
            }
        }

        /// <summary>
        /// Atualiza o envio para o SAP das informa��es
        /// </summary>
        /// <param name="usuario">Usuario do envio para o sap</param>
        [Transaction]
        protected virtual void AtualizarEnvioSap(Usuario usuario)
        {
            try
            {
                CteInterfaceEnvioSap envioSap = _cteInterfaceEnvioSapRepository.ObterPorCte(_cte.Id.Value);

                if (envioSap == null)
                {
                    throw new Exception("N�o encontrado o Cte na interface de envio para o SAP");
                }

                // Insere no historico
                CteSapHistorico sapHistorico = new CteSapHistorico();
                sapHistorico.Cte = envioSap.Cte;
                sapHistorico.DataEnvioSap = DateTime.Now;
                sapHistorico.DataGravacaoRetorno = envioSap.DataGravacaoRetorno;
                sapHistorico.HostName = Dns.GetHostName();
                sapHistorico.TipoOperacao = envioSap.TipoOperacao;
                _cteSapHistoricoRepository.Inserir(sapHistorico);

                _cteInterfaceEnvioSapRepository.RemoverCteInterfaceEnvioSap(_cte);

                _cteStatusRepository.InserirCteStatus(_cte, usuario, new CteStatusRetorno { Id = 27 }, "Enviado as informa��es do CTE para o SAP");
                _cteLogService.InserirLogInfo(_cte, "GravarSapCteEnvioService", string.Format("GravarSapCteEnvioService: {0}", "Enviado para Interface Sap"));
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(_cte, "GravarSapCteEnvioService", string.Format("AtualizarEnvioSap: {0}", ex.Message), ex);
                throw;
            }
        }

        /// <summary>
        /// Executar a grava��o do cte em um m�todo transacional
        /// </summary>
        /// <param name="cteInterfaceEnvioSap"> Cte a ser alterado </param>
        /// <param name="usuario"> Usu�rio do Robo </param>
        /// <param name="forcarGravacao"> For�a a grava��o na interface SAP </param>
        [Transaction]
        protected virtual void ExecutarPorCteRaizAntigo(CteInterfaceEnvioSap cteInterfaceEnvioSap, Usuario usuario, bool forcarGravacao)
        {
            IList<CteAgrupamento> listaAgrupamento = _cteAgrupamentoRepository.ObterAgrupamentoPorCteFilho(cteInterfaceEnvioSap.Cte);
            IList<CteInterfaceEnvioSap> listaCteInterfaceEnvioSap = _cteInterfaceEnvioSapRepository.ObterFilhosPorAgrupamento(listaAgrupamento);

            if (!forcarGravacao)
            {
                if (!listaAgrupamento.Count.Equals(listaCteInterfaceEnvioSap.Count))
                {
                    // n�o deve processar enquanto todos os ctes do agrupamento n�o estiverem na lista de envio para o SAP
                    return;
                }
            }

            _cte = _cteRepository.ObterCtePorHql(cteInterfaceEnvioSap.Cte);

            CarregarDados();
            ValidarDados();
            GravarSapDespCte();

            CteAgrupamento cteAgrupamento;

            string cnpjFerroviaCliente = string.Empty;

            for (int i = 0; i < listaAgrupamento.Count; i++)
            {
                cteAgrupamento = listaAgrupamento[i];

                string sigla;
                // A sigla da Raiz do Agrupamento sempre � a da empresa remetente
                // as siglas dos demais do Agrupamento se referem a Ferrovia que solicitou o servi�o
                if (listaAgrupamento.Count > 1)
                {
                    if (i == 0)
                    {
                        // Obtendo a sigla da Empresa Remetente e guardando o cnpj da ferrovia inicial do servi�o
                        sigla = _empresaRemetenteFluxoComercial.Sigla;
                        cnpjFerroviaCliente = _cte.CnpjFerrovia;
                    }
                    else
                    {
                        // Com base no Cnpj da ferrovia inicial obtem a sigla desta
                        Empresa empresaCliente = _empresaClienteRepository.ObterEmpresaClientePorCnpjDaFerrovia(cnpjFerroviaCliente);
                        sigla = empresaCliente != null ? empresaCliente.Sigla : string.Empty;
                    }
                }
                else
                {
                    // Caso o Agrupamento tenha uma ferrovia, a sigla � a da empresa remetente
                    sigla = _empresaRemetenteFluxoComercial.Sigla;
                }

                _cte = null;
                _cte = _cteRepository.ObterCtePorHql(cteAgrupamento.CteFilho);

                CarregarDados();
                ValidarDados();

                GravarSapNotasCte();
                GravarSapConteinerCte();
                GravarSapDocsoutrasferCte();

                GravarSapVagaoCte();
                GravarSapChaveCte();
                GravarSapComposicaoFreteCte();

                // CteInterfaceEnvioSap interfSap = listaCteInterfaceEnvioSap.FirstOrDefault(c => c.Cte.Id.Equals(_cte.Id));
                _cteInterfaceEnvioSapRepository.AtualizarEnvioSap(_cte);
                _cteStatusRepository.InserirCteStatus(_cte, usuario, new CteStatusRetorno { Id = 27 }, "Enviado as informa��es do CTE para o SAP");
                _cteLogService.InserirLogInfo(_cte, "GravarSapCteEnvioService", string.Format("GravarSapCteEnvioService: {0}", "Enviado para Interface Sap"));
            }
        }

        /// <summary>
        /// Executar a grava��o do cte em um m�todo transacional
        /// </summary>
        /// <param name="cte"> Cte a ser alterado </param>
        /// <param name="usuario"> Usu�rio do Robo </param>
        [Transaction]
        protected virtual void ExecutarPorCteComplementar(Cte cte, Usuario usuario)
        {
            _cte = _cteRepository.ObterCteComplementarPorHql(cte);

            CarregarDadosCteComplementar();
            ValidarDadosCteComplementar();

            GravarSapDespCteComplementar(usuario);
            GravarSapCteComplementado();

            GravarSapChaveCteComplementar();

            AtualizarEnvioSap(usuario);
        }

        /// <summary>
        /// Executar a grava��o do cte em um m�todo transacional
        /// </summary>
        /// <param name="cteInterfaceEnvioSap"> Cte a ser alterado </param>
        /// <param name="usuario"> Usu�rio do Robo </param>
        [Transaction]
        protected virtual void ExecutarPorCteComplementarRaizAntigo(CteInterfaceEnvioSap cteInterfaceEnvioSap, Usuario usuario)
        {
            bool forcarGravacao = true;

            _cteComplementar = _cteRepository.ObterCteComplementarPorHql(cteInterfaceEnvioSap.Cte);
            int countReg = 0;
            // Carregar dados do CTE complementado
            _listaCteComplementado = _cteComplementadoRepository.ObterCteComplementadoPorCteComplementar(_cteComplementar);

            foreach (var cteComplementado in _listaCteComplementado)
            {
                _cteComplementado = cteComplementado;
                _cte = cteComplementado.Cte;
                countReg++;

                IList<CteAgrupamento> listaAgrupamento = _cteAgrupamentoRepository.ObterAgrupamentoPorCteFilho(_cteComplementar);
                IList<CteInterfaceEnvioSap> listaCteInterfaceEnvioSap = _cteInterfaceEnvioSapRepository.ObterFilhosPorAgrupamento(listaAgrupamento);

                if (!forcarGravacao)
                {
                    if (!listaAgrupamento.Count.Equals(listaCteInterfaceEnvioSap.Count))
                    {
                        // n�o deve processar enquanto todos os ctes do agrupamento n�o estiverem na lista de envio para o SAP
                        return;
                    }
                }

                CarregarDados();
                ValidarDados();
                GravarSapDespCte();

                CteAgrupamento cteAgrupamento;

                string cnpjFerroviaCliente = string.Empty;

                for (int i = 0; i < listaAgrupamento.Count; i++)
                {
                    cteAgrupamento = listaAgrupamento[i];

                    string sigla;
                    // A sigla da Raiz do Agrupamento sempre � a da empresa remetente
                    // as siglas dos demais do Agrupamento se referem a Ferrovia que solicitou o servi�o
                    if (listaAgrupamento.Count > 1)
                    {
                        if (i == 0)
                        {
                            // Obtendo a sigla da Empresa Remetente e guardando o cnpj da ferrovia inicial do servi�o
                            sigla = _empresaRemetenteFluxoComercial.Sigla;
                            cnpjFerroviaCliente = _cte.CnpjFerrovia;
                        }
                        else
                        {
                            // Com base no Cnpj da ferrovia inicial obtem a sigla desta
                            Empresa empresaCliente = _empresaClienteRepository.ObterEmpresaClientePorCnpjDaFerrovia(cnpjFerroviaCliente);
                            sigla = empresaCliente != null ? empresaCliente.Sigla : string.Empty;
                        }
                    }
                    else
                    {
                        // Caso o Agrupamento tenha uma ferrovia, a sigla � a da empresa remetente
                        sigla = _empresaRemetenteFluxoComercial.Sigla;
                    }

                    _cte = _cteRepository.ObterCteComplementarPorHql(_cte);

                    CarregarDados();
                    ValidarDados();

                    GravarSapNotasCte();
                    GravarSapConteinerCte();
                    GravarSapDocsoutrasferCte();

                    GravarSapCteComplementado();

                    GravarSapChaveCte();
                    GravarSapComposicaoFreteCte();

                    if (countReg == _listaCteComplementado.Count)
                    {
                        // CteInterfaceEnvioSap interfSap = listaCteInterfaceEnvioSap.FirstOrDefault(c => c.Cte.Id.Equals(_cte.Id));
                        _cteInterfaceEnvioSapRepository.AtualizarEnvioSap(_cteComplementar);

                        _cteStatusRepository.InserirCteStatus(_cte, usuario, new CteStatusRetorno { Id = 27 }, "Enviado as informa��es do CTE para o SAP");

                        _cteLogService.InserirLogInfo(_cte, "GravarSapCteEnvioService", string.Format("GravarSapCteEnvioService: {0}", "Enviado para Interface Sap"));
                    }
                    else
                    {
                        _cteStatusRepository.InserirCteStatus(_cte, usuario, new CteStatusRetorno { Id = 27 }, string.Format("Enviado as informa��es do CTE para o SAP TakeOrPay: {0}", _cte.Id));
                    }
                }
            }
        }

        private void GravarSapDespCteVirtual()
        {
            _sapDespCte = new SapDespCte();

            /*
            Quando a opera��o for de Exclus�o (Cancelamento) ou CR (Cancelamento Rejeitado)
            Dever� se obter a s�rie e o n�mero gerado pelo SAP
            */
            string serieDespachoSap = string.Empty;
            int numeroDespachoSap = 0;
            ObterNumeroSerieDespachoGeradoPeloSap(_cte.Chave, out serieDespachoSap, out numeroDespachoSap);

            _sapDespCte.SerDesp5 = serieDespachoSap;
            _sapDespCte.NroDesp5 = numeroDespachoSap;
            _sapDespCte.SerieDespachoSap = serieDespachoSap;
            _sapDespCte.NumeroDespachoSap = numeroDespachoSap;

            if (_serieDespachoUf != null)
            {
                _sapDespCte.FerroviaUf = _serieDespachoUf.CodigoControle;
            }

            _sapDespCte.DthRemissao = _cte.DataHora;
            _sapDespCte.CodFluxo = Convert.ToString(_cte.FluxoComercial.Codigo);
            _sapDespCte.NroContrato = _contratoHistorico.NumeroContrato;

            CteInterfaceEnvioSap cteInterfaceEnvioSap = _cteInterfaceEnvioSapRepository.ObterPorCte(_cte.Id.Value);

            /*  I=Inclus�o, 
                    *  E=Exclus�o (CANCELADO AUTORIZADO), 
                    *  C=Complemento, IN=Inutilizado, 
                    *  CR=Cancelamento Rejeitado c�digo do SEFAZ 220,
                    *  D=Denegado */
            if (cteInterfaceEnvioSap != null)
            {
                if (cteInterfaceEnvioSap.TipoOperacao != null)
                {
                    _sapDespCte.TipoOperacao = Translogic.Core.Commons.Enum<TipoOperacaoCteEnum>.GetDescriptionOf(cteInterfaceEnvioSap.TipoOperacao.Value);
                }
            }
            else
            {
                _sapDespCte.TipoOperacao = Translogic.Core.Commons.Enum<TipoOperacaoCteEnum>.GetDescriptionOf(TipoOperacaoCteEnum.Exclusao);
            }

            if ((_sapDespCte.TipoOperacao != "E") && (_sapDespCte.TipoOperacao != "CR") && (_sapDespCte.TipoOperacao != "A"))
            {
                if (_cte.Manutencao)
                {
                    // Verifica se � um fluxo de conteiner ou se a mercadoria est� condicionada em conteiner ou se a empresa � BRADO
                    // Se a empresa � brado, e o fluxo � de rateio de CTE, pega como base o peso do cte, gerado no despacho, a partir do peso das notas.
                    var indRateio = _cte.FluxoComercial.IndRateioCte ?? false;
                    if (VerificaEmpresaBrado(_contrato.EmpresaPagadora) && (indRateio))
                    {
                        _sapDespCte.PesoCalculo = _itemDespacho.PesoCalculo ?? 0;
                    }
                    else

                        // Verifica se � um fluxo de conteiner ou se a mercadoria est� condicionada em conteiner ou se a empresa � BRADO
                        if (_carregamentoService.VerificarFluxoConteiner(_fluxoComercial) || VerificaEmpresaBrado(_contrato.EmpresaPagadora))
                        {
                            // Caso conteiner
                            IList<CteDetalhe> listaCteDetalhe = _cteDetalheRepository.ObterPorCte(_cte);
                            List<string> listaConteiner = listaCteDetalhe.GroupBy(g => g.ConteinerNotaFiscal).Select(s => s.Key).ToList();
                            _sapDespCte.PesoCalculo = listaConteiner.Count;
                        }
                        else
                        {
                            switch (_fluxoComercial.UnidadeMedida.Id)
                            {
                                case "M3":
                                    _sapDespCte.PesoCalculo = _cte.VolumeVagao;
                                    break;
                                default:
                                    _sapDespCte.PesoCalculo = _cte.PesoVagao ?? 0;
                                    break;
                            }
                        }

                    _sapDespCte.ToneladaUtil = _cte.PesoVagao ?? 0.0;
                }
                else
                {
                    // Verifica se � um fluxo de conteiner ou se a mercadoria est� condicionada em conteiner ou se a empresa � BRADO
                    // Se a empresa � brado, e o fluxo � de rateio de CTE, pega como base o peso do cte, gerado no despacho, a partir do peso das notas.
                    var indRateio = _cte.FluxoComercial.IndRateioCte ?? false;
                    if (VerificaEmpresaBrado(_contrato.EmpresaPagadora) && (indRateio))
                    {
                        _sapDespCte.PesoCalculo = _itemDespacho.PesoCalculo ?? 0;
                    }
                    else

                        // Verifica se � um fluxo de conteiner ou se a mercadoria est� condicionada em conteiner ou se a empresa � BRADO
                        if (_carregamentoService.VerificarFluxoConteiner(_fluxoComercial) || VerificaEmpresaBrado(_contrato.EmpresaPagadora))
                        {
                            // Caso conteiner
                            IList<CteDetalhe> listaCteDetalhe = _cteDetalheRepository.ObterPorCte(_cte);
                            List<string> listaConteiner = listaCteDetalhe.GroupBy(g => g.ConteinerNotaFiscal).Select(s => s.Key).ToList();
                            _sapDespCte.PesoCalculo = listaConteiner.Count;
                        }
                        else
                        {
                            _sapDespCte.PesoCalculo = _itemDespacho.PesoCalculo ?? 0;
                        }

                    _sapDespCte.ToneladaUtil = _itemDespacho.PesoToneladaUtil;
                }
            }

            _sapDespCte.VersaoXml = Convert.ToDouble(_cteVersao.CodigoVersao, CultureInfo.GetCultureInfo("en-us"));

            _sapDespCte.DthrProc = null;
            _sapDespCte.Timestamp = DateTime.Now;
            _sapDespCte.VlrDescontoPerc = _cte.DescontoPorPercentual.GetValueOrDefault();
            _sapDespCte.VlrDescontoTon = _cte.ValorDescontoTonelada.GetValueOrDefault();
            _sapDespCte.VlrSeguro = _cte.ValorSeguro.GetValueOrDefault();
            _sapDespCte.VrlMercadoria = _cte.ValorTotalMercadoria.GetValueOrDefault();
            _sapDespCte.CotacaoDolar = _sapDespCte.CotacaoDolar = ObterValorCotacao(_despachoTranslogic.DataDespacho);
            _sapDespCte.DataCargaOriginal = _despachoTranslogic.DataDespacho;
            _sapDespCte.FlxOutraFerrovia = null; // TODO em verifica��o

            if (_cte.CteMotivoCancelamento != null)
            {
                _sapDespCte.CodigoMotivoCancelamento = _cte.CteMotivoCancelamento.Id;
            }

            // LIBWBOLAR = EDI caso contrario Manual
            if (_vagaoPedido != null)
            {
                string codTransacao = _vagaoPedido.CodTransacao;
                if (!string.IsNullOrWhiteSpace(codTransacao) && codTransacao.Equals("LIBWBOLAR"))
                {
                    _sapDespCte.Transacao = "E";
                }
                else
                {
                    _sapDespCte.Transacao = "M";
                }
            }
            else
            {
                _sapDespCte.Transacao = "M";
            }

            _sapDespCte.IndProc = "N";
            _sapDespCte.Ambiente = _cte.AmbienteSefaz;
            _sapDespCte.Vencimento = _cte.Vencimento;
            _sapDespCteRepository.Inserir(_sapDespCte);
        }

        private void GravarSapDespCte()
        {
            string serieDesp5 = string.Empty;

            if (_despachoTranslogic.SerieDespacho != null)
            {
                serieDesp5 = _despachoTranslogic.SerieDespacho.SerieDespachoNum;
            }

            _sapDespCte = new SapDespCte();

            if (_tipoTrafegoMutuo == TipoTrafegoMutuoEnum.IntercambioFaturamentoOrigem || PartilhaFerroeste())
            {
                // Quando for Ferroeste manda as informa��es do nosso numero do despacho
                if (PartilhaFerroeste() && OrigemFerroeste())
                {
                    _sapDespCte.SerDesp5 = _despachoTranslogic.SerieDespachoSdi;
                    _sapDespCte.NroDesp5 = _despachoTranslogic.NumDespIntercambio;
                }
                else
                {
                    _sapDespCte.SerDesp5 = serieDesp5 ?? _despachoTranslogic.SerieDespachoSdi;
                    _sapDespCte.NroDesp5 = _despachoTranslogic.NumeroDespacho ?? _despachoTranslogic.NumDespIntercambio;
                }
            }
            else
            {
                if (_despachoTranslogic.SerieDespacho != null)
                {
                    _sapDespCte.SerDesp5 = _despachoTranslogic.SerieDespacho.SerieDespachoNum;
                }

                _sapDespCte.NroDesp5 = _despachoTranslogic.NumeroDespacho;
            }

            // Manda as informa��es do despacho de 6 digitos
            if (_serieDespachoUf != null)
            {
                _sapDespCte.SerDesp6 = Convert.ToString(_serieDespachoUf.NumeroSerieDespacho);
                _sapDespCte.FerroviaUf = _serieDespachoUf.CodigoControle;
            }

            _sapDespCte.NroDesp6 = _despachoTranslogic.NumeroDespachoUf;
            _sapDespCte.DthRemissao = _cte.DataHora;
            _sapDespCte.CodFluxo = Convert.ToString(_cte.FluxoComercial.Codigo);
            _sapDespCte.NroContrato = _contratoHistorico.NumeroContrato;

            CteInterfaceEnvioSap cteInterfaceEnvioSap = _cteInterfaceEnvioSapRepository.ObterPorCte(_cte.Id.Value);

            /*  I=Inclus�o, 
                    *  E=Exclus�o (CANCELADO AUTORIZADO), 
                    *  C=Complemento, IN=Inutilizado, 
                    *  CR=Cancelamento Rejeitado c�digo do SEFAZ 220,
                    *  D=Denegado */

            if (_cte.TipoCte == TipoCteEnum.Anulacao)
            {
                _sapDespCte.TipoOperacao = Translogic.Core.Commons.Enum<TipoOperacaoCteEnum>.GetDescriptionOf(TipoOperacaoCteEnum.AnulacaoSefaz);
            }
            else
            {
                if (cteInterfaceEnvioSap != null)
                {
                    if (cteInterfaceEnvioSap.TipoOperacao != null)
                    {
                        _sapDespCte.TipoOperacao = Translogic.Core.Commons.Enum<TipoOperacaoCteEnum>.GetDescriptionOf(cteInterfaceEnvioSap.TipoOperacao.Value);
                    }
                }
                else
                {
                    _sapDespCte.TipoOperacao = Translogic.Core.Commons.Enum<TipoOperacaoCteEnum>.GetDescriptionOf(TipoOperacaoCteEnum.Exclusao);
                }
            }
            
            if ((_sapDespCte.TipoOperacao != "E") && (_sapDespCte.TipoOperacao != "CR") && (_sapDespCte.TipoOperacao != "A"))
            {
                if (_cte.Manutencao)
                {
                    // Verifica se � um fluxo de conteiner ou se a mercadoria est� condicionada em conteiner ou se a empresa � BRADO
                    // Se a empresa � brado, e o fluxo � de rateio de CTE, pega como base o peso do cte, gerado no despacho, a partir do peso das notas.
                    var indRateio = _cte.FluxoComercial.IndRateioCte ?? false;
                    if (VerificaEmpresaBrado(_contrato.EmpresaPagadora) && (indRateio))
                    {
                        _sapDespCte.PesoCalculo = (double)_carregamentoService.ObterPesoCalculoSapDespCte(_cte.Chave);
                    }
                    else

                        // Verifica se � um fluxo de conteiner ou se a mercadoria est� condicionada em conteiner ou se a empresa � BRADO
                        if (_carregamentoService.VerificarFluxoConteiner(_fluxoComercial) || VerificaEmpresaBrado(_contrato.EmpresaPagadora))
                        {
                            // Caso conteiner
                            IList<CteDetalhe> listaCteDetalhe = _cteDetalheRepository.ObterPorCte(_cte);
                            List<string> listaConteiner = listaCteDetalhe.GroupBy(g => g.ConteinerNotaFiscal).Select(s => s.Key).ToList();
                            _sapDespCte.PesoCalculo = listaConteiner.Count;
                        }
                        else
                        {
                            switch (_fluxoComercial.UnidadeMedida.Id)
                            {
                                case "M3":
                                    _sapDespCte.PesoCalculo = _cte.VolumeVagao;
                                    break;
                                default:
                                    _sapDespCte.PesoCalculo = _cte.PesoVagao ?? 0;
                                    break;
                            }
                        }

                    _sapDespCte.ToneladaUtil = _cte.PesoVagao ?? 0.0;
                }
                else
                {
                    // Verifica se � um fluxo de conteiner ou se a mercadoria est� condicionada em conteiner ou se a empresa � BRADO
                    // Se a empresa � brado, e o fluxo � de rateio de CTE, pega como base o peso do cte, gerado no despacho, a partir do peso das notas.
                    var indRateio = _cte.FluxoComercial.IndRateioCte ?? false;
                    if (VerificaEmpresaBrado(_contrato.EmpresaPagadora) && (indRateio))
                    {
                        _sapDespCte.PesoCalculo = (double)_carregamentoService.ObterPesoCalculoSapDespCte(_cte.Chave);
                    }
                    else

                        // Verifica se � um fluxo de conteiner ou se a mercadoria est� condicionada em conteiner ou se a empresa � BRADO
                        if (_carregamentoService.VerificarFluxoConteiner(_fluxoComercial) || VerificaEmpresaBrado(_contrato.EmpresaPagadora))
                        {
                            // Caso conteiner
                            IList<CteDetalhe> listaCteDetalhe = _cteDetalheRepository.ObterPorCte(_cte);
                            List<string> listaConteiner = listaCteDetalhe.GroupBy(g => g.ConteinerNotaFiscal).Select(s => s.Key).ToList();
                            _sapDespCte.PesoCalculo = listaConteiner.Count;
                        }
                        else
                        {
                            _sapDespCte.PesoCalculo = _itemDespacho.PesoCalculo ?? 0;
                        }

                    _sapDespCte.ToneladaUtil = _itemDespacho.PesoToneladaUtil;
                }

                /* if (_cte.Manutencao)
                {
                    if ((_itemDespacho.NumeroConteiner != null) && (_itemDespacho.NumeroConteiner > 0))
                    {
                        IList<CteDetalhe> listaCteDetalhe = _cteDetalheRepository.ObterPorCte(_cte);
                        List<string> listaConteiner = listaCteDetalhe.GroupBy(g => g.ConteinerNotaFiscal).Select(s => s.Key).ToList();
                        _sapDespCte.PesoCalculo = listaConteiner.Count;
                    }
                    else
                    {
                        switch (_fluxoComercial.UnidadeMedida.Id)
                        {
                            case "M3":
                                _sapDespCte.PesoCalculo = _cte.VolumeVagao;
                                break;
                            default:
                                _sapDespCte.PesoCalculo = _cte.PesoVagao ?? 0;
                                break;
                        }
                    }

                    _sapDespCte.ToneladaUtil = _cte.PesoVagao ?? 0.0;
                }
                else
                {
                    // Regra do conteiner do SAP
                    if (EhConteiner())
                    {
                        if ((_itemDespacho.NumeroConteiner != null) && (_itemDespacho.NumeroConteiner > 0))
                        {
                            _sapDespCte.PesoCalculo = _itemDespacho.NumeroConteiner;
                        }
                        else
                        {
                            _sapDespCte.PesoCalculo = _itemDespacho.PesoCalculo;
                        }

                        _sapDespCte.ToneladaUtil = _itemDespacho.PesoToneladaUtil;
                    }
                    else
                    {
                        _sapDespCte.PesoCalculo = _itemDespacho.PesoCalculo;
                        _sapDespCte.ToneladaUtil = _itemDespacho.PesoToneladaUtil;
                    }
                }  */
            }

            _sapDespCte.VersaoXml = Convert.ToDouble(_cteVersao.CodigoVersao, CultureInfo.GetCultureInfo("en-us"));
            _sapDespCte.TipoServico = _cte.TipoServico;
            _sapDespCte.DthrProc = null;
            _sapDespCte.Timestamp = DateTime.Now;
            _sapDespCte.VlrDescontoPerc = _cte.DescontoPorPercentual.GetValueOrDefault();
            _sapDespCte.VlrDescontoTon = _cte.ValorDescontoTonelada.GetValueOrDefault();
            _sapDespCte.VlrSeguro = _cte.ValorSeguro.GetValueOrDefault();
            _sapDespCte.VrlMercadoria = _cte.ValorTotalMercadoria.GetValueOrDefault();
            _sapDespCte.CotacaoDolar = _sapDespCte.CotacaoDolar = ObterValorCotacao(_despachoTranslogic.DataDespacho);
            _sapDespCte.DataCargaOriginal = _despachoTranslogic.DataDespacho;
            _sapDespCte.FlxOutraFerrovia = null; // TODO em verifica��o

            if (_cte.CteMotivoCancelamento != null)
            {
                _sapDespCte.CodigoMotivoCancelamento = _cte.CteMotivoCancelamento.Id;
            }

            if (_ctePai != null)
            {
                if (_ctePai.Id != _cte.Id)
                {
                    _sapDespCte.CteSubstituido = _ctePai.Chave; // TODO quando faz a manuten��o cancela e gera o novo, nesse campo coloca o que foi cancelado
                    if (_ctePai.Despacho.SerieDespacho != null)
                    {
                        _sapDespCte.Serie5CteSubstituido = _ctePai.Despacho.SerieDespacho.SerieDespachoNum; // Todo S�rie do DCL de 5 d�gitos.Usado qdo o CTE � cancelado.	
                        _sapDespCte.Dcl5CteSubstituido = Convert.ToString(_ctePai.Despacho.NumeroDespacho); // Todo Nro do DCL de 5 d�gitos. Usado qdo o CTE � cancelado.
                    }
                    else if (_tipoTrafegoMutuo == TipoTrafegoMutuoEnum.IntercambioFaturamentoOrigem || PartilhaFerroeste())
                    {
                        _sapDespCte.Serie5CteSubstituido = _ctePai.Despacho.SerieDespachoSdi ?? serieDesp5;
                        _sapDespCte.Dcl5CteSubstituido = Convert.ToString(_ctePai.Despacho.NumDespIntercambio) ?? Convert.ToString(_ctePai.Despacho.NumeroDespacho);
                    }

                    if (_serieDespachoUf != null && (_serieDespachoUf.NumeroSerieDespacho != null) && (_serieDespachoUf.NumeroSerieDespacho > 0))
                    {
                        _sapDespCte.SerDesp6 = Convert.ToString(_serieDespachoUf.NumeroSerieDespacho);
                        _sapDespCte.NroDesp6 = _ctePai.Despacho.NumeroDespachoUf;
                    }
                }
            }

            // LIBWBOLAR = EDI caso contrario Manual
            if (_vagaoPedido != null)
            {
                string codTransacao = _vagaoPedido.CodTransacao;
                if (!string.IsNullOrWhiteSpace(codTransacao) && codTransacao.Equals("LIBWBOLAR"))
                {
                    _sapDespCte.Transacao = "E";
                }
                else
                {
                    _sapDespCte.Transacao = "M";
                }
            }
            else
            {
                _sapDespCte.Transacao = "M";
            }

            _sapDespCte.IndProc = "N";
            _sapDespCte.Ambiente = _cte.AmbienteSefaz;
            _sapDespCte.Vencimento = _cte.Vencimento;

            if (_despachoTranslogicRepository.VerificaPrecificacaoOrigem(_contratoHistorico.Id))
            {
                _sapDespCte.DataDescargaNfe = _despachoTranslogicRepository.ObterDataDescargaNfe(_sapDespCte.NroDesp5, _sapDespCte.SerDesp5);
            }
            
            //// _sapDespCteRepository.InserirSemSessao(_sapDespCte);
            _sapDespCteRepository.Inserir(_sapDespCte);
        }

        /// <summary>
        /// Grava as informa��es do Cte Complemento da tabela do SAP
        /// </summary>
        /// <param name="usuario"> usuario que efetuou a a��o</param>
        private void GravarSapDespCteComplementar(Usuario usuario)
        {
            _sapDespCte = new SapDespCte();

            _sapDespCte.DthRemissao = _cte.DataHora;
            _sapDespCte.DataCargaOriginal = _cte.DataHora;
            _sapDespCte.CodFluxo = Convert.ToString(_cte.FluxoComercial.Codigo);

            _sapDespCte.VersaoXml = Convert.ToDouble(_cteVersao.CodigoVersao, CultureInfo.GetCultureInfo("en-us"));

            CteInterfaceEnvioSap cteInterfaceEnvioSap = _cteInterfaceEnvioSapRepository.ObterPorCte(_cte.Id.Value);

            /*  I=Inclus�o, 
                    *  E=Exclus�o (CANCELADO AUTORIZADO), 
                    *  C=Complemento, IN=Inutilizado, 
                    *  CR=Cancelamento Rejeitado c�digo do SEFAZ 220,
                    *  D=Denegado */
            if (cteInterfaceEnvioSap != null)
            {
                if (cteInterfaceEnvioSap.TipoOperacao != null)
                {
                    _sapDespCte.TipoOperacao = Translogic.Core.Commons.Enum<TipoOperacaoCteEnum>.GetDescriptionOf(cteInterfaceEnvioSap.TipoOperacao.Value);
                }
            }
            else
            {
                _sapDespCte.TipoOperacao = Translogic.Core.Commons.Enum<TipoOperacaoCteEnum>.GetDescriptionOf(TipoOperacaoCteEnum.Exclusao);
            }

            /*
            Quando a opera��o for de Exclus�o (Cancelamento) ou CR (Cancelamento Rejeitado)
            Dever� se obter a s�rie e o n�mero gerado pelo SAP
            */
            string serieDespachoSap = string.Empty;
            int numeroDespachoSap = 0;
            ObterNumeroSerieDespachoGeradoPeloSap(_cte.Chave, out serieDespachoSap, out numeroDespachoSap);

            _sapDespCte.SerDesp5 = serieDespachoSap;
            _sapDespCte.NroDesp5 = numeroDespachoSap;
            _sapDespCte.SerieDespachoSap = serieDespachoSap;
            _sapDespCte.NumeroDespachoSap = numeroDespachoSap;
            /*
            if ((_sapDespCte.TipoOperacao == "E") && (_sapDespCte.TipoOperacao == "CR"))
            {
                if (string.IsNullOrEmpty(_sapDespCte.SerDesp5) || !_sapDespCte.NroDesp5.HasValue)
                {
                    _cteStatusRepository.InserirCteStatus(_cte, usuario, new CteStatusRetorno { Id = 14 }, "serieDespachoSap/numeroDespachoSap n�o encontrado. ");
                    throw new Exception("serieDespachoSap/numeroDespachoSap vazio");
                }
            }
            */
            _sapDespCte.DthrProc = null;
            _sapDespCte.Timestamp = DateTime.Now;
            _sapDespCte.VlrDescontoPerc = _cte.DescontoPorPercentual.GetValueOrDefault();
            _sapDespCte.VlrDescontoTon = _cte.ValorDescontoTonelada.GetValueOrDefault();
            _sapDespCte.VlrSeguro = _cte.ValorSeguro.GetValueOrDefault();
            _sapDespCte.VrlMercadoria = _cte.ValorTotalMercadoria.GetValueOrDefault();
            _sapDespCte.CotacaoDolar = ObterValorCotacao(_cte.DataHora);
            _sapDespCte.Transacao = "M";

            if (_serieDespachoUf != null)
            {
                _sapDespCte.FerroviaUf = _serieDespachoUf.CodigoControle;
            }

            _sapDespCte.IndProc = "N";
            _sapDespCte.Ambiente = _cte.AmbienteSefaz;
            _sapDespCte.Vencimento = _cte.Vencimento;

            _sapDespCteRepository.Inserir(_sapDespCte);

        }

        /// <summary>
        /// Grava SAP Notas CTE
        /// </summary>
        private void GravarSapNotasCte()
        {
            try
            {
                SapNotasCte sapNotasCte;
                string modelo = _cte.FluxoComercial.ModeloNotaFiscal;
                if (string.IsNullOrEmpty(modelo))
                {
                    modelo = "01";
                }

                foreach (var cteDetalhe in _listaCteDetalhe)
                {
                    string conteiner = cteDetalhe.ConteinerNotaFiscal;

                    modelo = string.IsNullOrEmpty(cteDetalhe.ChaveNfe) ? modelo : "55";

                    sapNotasCte = new SapNotasCte();
                    string numeroNotaFiscal = cteDetalhe.NumeroNota.Trim();

                    sapNotasCte.SapDespCte = _sapDespCte;
                    sapNotasCte.SerieNota = Tools.TruncateString(cteDetalhe.SerieNota, 3);
                    // sapNotasCte.NroNota = Tools.TruncateRemoveSpecialChar(cteDetalhe.NumeroNota, 12);
                    sapNotasCte.NroNota = Tools.TruncateRemoveSpecialChar(numeroNotaFiscal, 12);
                    sapNotasCte.Modelo = Tools.TruncateString(modelo, 2);
                    sapNotasCte.Valor = cteDetalhe.ValorNotaFiscal;
                    sapNotasCte.DthRemissao = cteDetalhe.DataNotaFiscal;
                    sapNotasCte.UfRemetente = cteDetalhe.UfRemetente;
                    sapNotasCte.CnpjRemetente = Tools.TruncateRemoveSpecialChar(cteDetalhe.CgcRemetente, 14);
                    sapNotasCte.InsRemetente = Tools.TruncateRemoveSpecialChar(cteDetalhe.InsEstadualRemetente, 14);
                    sapNotasCte.UfDestinatario = cteDetalhe.UfDestinatario;
                    sapNotasCte.CnpjDestinatario = Tools.TruncateRemoveSpecialChar(cteDetalhe.CgcDestinatario, 14);
                    sapNotasCte.InsDestinatario = Tools.TruncateRemoveSpecialChar(cteDetalhe.InsEstadualDestinatario, 14);
                    sapNotasCte.Nfe = cteDetalhe.ChaveNfe;
                    sapNotasCte.PesoTotal = cteDetalhe.PesoTotal;

                    // Verifica se uma nota de conteiner
                    if (!string.IsNullOrEmpty(conteiner))
                    {
                        // Regra do SAP que multiplica por 1000
                        sapNotasCte.PesoTotal = sapNotasCte.PesoTotal;
                    }

                    sapNotasCte.PesoRateio = cteDetalhe.PesoNotaFiscal;
                    sapNotasCte.ComplementoBunge = cteDetalhe.FilialEmissora;

                    _sapNotasCteRepository.InserirOuAtualizar(sapNotasCte);
                }
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(_cte, "GravarSapNotasCte", string.Format("GravarSapNotasCte: {0}", ex.Message), ex);
                throw;
            }
        }

        /// <summary>
        /// Grava Sap Conteiner Cte
        /// </summary>
        private void GravarSapConteinerCte()
        {
            try
            {
                SapConteinerCte sapConteinerCte;

                foreach (var cteDetalhe in _listaCteDetalhe)
                {
                    string conteiner = cteDetalhe.ConteinerNotaFiscal;

                    if (!string.IsNullOrEmpty(conteiner))
                    {
                        sapConteinerCte = new SapConteinerCte();
                        sapConteinerCte.SapDespCte = _sapDespCte;
                        sapConteinerCte.CodConteiner = cteDetalhe.ConteinerNotaFiscal;

                        var _conteiner = _conteinerRepository.ObterPorCodigoSemEstado(cteDetalhe.ConteinerNotaFiscal);
                        if (_conteiner != null)
                        {
                            var idConteiner = _conteiner.Id;
                            var idAgrupamento = _cteService.ObterAgrupamentoContainer(cteDetalhe.Cte.Id ?? 0, idConteiner ?? 0);

                            if (idAgrupamento > 0)
                            {
                                sapConteinerCte.Agrupamento = idAgrupamento;
                            }
                            _sapConteinerCteRepository.InserirOuAtualizar(sapConteinerCte);
                        }
                        else
                        {
                            throw new Exception(string.Format("N�o encontrado o conteiner para o Cte {0} - Detalhe {1}: select * from CONTEINER where CN_NUM_CNT = '{2}'", cteDetalhe.Cte.Id, cteDetalhe.Id, cteDetalhe.ConteinerNotaFiscal));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(_cte, "SapConteinerCte", string.Format("SapConteinerCte: {0}", ex.Message), ex);
                throw;
            }
        }

        /// <summary>
        /// Grava Sap Docs Outras Ferrovias Cte
        /// </summary>
        private void GravarSapDocsoutrasferCte()
        {
            try
            {
                SapDocsoutrasferCte sapDocsoutrasferCte = new SapDocsoutrasferCte();

                // Se for um fluxo internacional informar TIF tabela nota fiscal TIF_num_TIF; 
                // caso contrario, se for fluxo de intercambio (n�o internacional) 
                // utilizar serie e nro despacho intercambio se estiver preenchido DCL = tabela despacho 
                // colunas DP_NUM_SDI e DP_NUM_DPI colocar os dados concatenados na coluna NRO;

                string codigoFluxoInternacional = VerificarFluxoInternacional(_fluxoComercial);

                // _tipoTrafegoMutuo -> J� capturado no CarregarDados
                // _tipoTrafegoMutuo = ObterTipoTrafegoMutuo();

                foreach (CteDetalhe cteDetalhe in _listaCteDetalhe)
                {
                    sapDocsoutrasferCte.SapDespCte = _sapDespCte;

                    if (!string.IsNullOrEmpty(codigoFluxoInternacional))
                    {
                        sapDocsoutrasferCte.TipoDoc = "TIF";
                        sapDocsoutrasferCte.Nro = cteDetalhe.NumeroTif;

                        _sapDocsoutrasferCteRepository.InserirOuAtualizar(sapDocsoutrasferCte);
                    }
                    else
                    {
                        if (_tipoTrafegoMutuo == TipoTrafegoMutuoEnum.IntercambioFaturamentoOrigem || _tipoTrafegoMutuo == TipoTrafegoMutuoEnum.IntercambioFaturamentoDestino)
                        {
                            sapDocsoutrasferCte.TipoDoc = "DCL";
                            sapDocsoutrasferCte.Nro = _despachoTranslogic.SerieDespachoSdi + _despachoTranslogic.NumDespIntercambio;

                            _sapDocsoutrasferCteRepository.InserirOuAtualizar(sapDocsoutrasferCte);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(_cte, "SapDocsoutrasferCte", string.Format("SapDocsoutrasferCte: {0}", ex.Message), ex);
                throw;
            }
        }

        /// <summary>
        /// Grava Sap Cte Complementado
        /// </summary>
        private void GravarSapCteComplementado()
        {
            try
            {
                foreach (CteComplementado cteComplementado in _listaCteComplementado)
                {
                    SapCteComplementado sapCteComplementado = new SapCteComplementado();

                    sapCteComplementado.SapDespCte = _sapDespCte;
                    sapCteComplementado.ChaveCteComplementado = cteComplementado.Cte.Chave;

                    sapCteComplementado.Valor = Math.Round(cteComplementado.ValorDiferenca, 2);
                    sapCteComplementado.AliquotaIcms = cteComplementado.Cte.FluxoComercial.Contrato.PercentualAliquotaIcms;

                    sapCteComplementado.ValorIcms = cteComplementado.Cte.ValorIcms.GetValueOrDefault();
                    sapCteComplementado.ValorIcms = Math.Round(sapCteComplementado.ValorIcms, 2);

                    sapCteComplementado.Timestamp = DateTime.Now;

                    _sapCteComplementadoRepository.Inserir(sapCteComplementado);
                }
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(_cte, "SapCteComplementado", string.Format("SapCteComplementado: {0}", ex.Message), ex);
                throw;
            }
        }

        /// <summary>
        /// Grava Sap Vagao Cte
        /// </summary>
        private void GravarSapVagaoCte()
        {
            try
            {
                SapVagaoCte sapVagaoCte = new SapVagaoCte();

                sapVagaoCte.SapDespCte = _sapDespCte;
                sapVagaoCte.CodVagao = _vagao.Codigo;
                sapVagaoCte.PropVagao = _empresaProprietariaVagao.Sigla;
                sapVagaoCte.SerieVagao = _vagao.Serie.Codigo;

                if ((_sapDespCte.TipoOperacao != "E") && (_sapDespCte.TipoOperacao != "CR"))
                {
                    if (_cte.Manutencao)
                    {
                        // Verifica se � um fluxo de conteiner ou se a mercadoria est� condicionada em conteiner ou se a empresa � BRADO
                        // Se a empresa � brado, e o fluxo � de rateio de CTE, pega como base o peso do cte, gerado no despacho, a partir do peso das notas.
                        var indRateio = _cte.FluxoComercial.IndRateioCte ?? false;
                        if (VerificaEmpresaBrado(_contrato.EmpresaPagadora) && (indRateio))
                        {
                            //_sapDespCte.PesoCalculo = _itemDespacho.PesoCalculo ?? 0;

                            sapVagaoCte.PesoCalculo = (double)_carregamentoService.ObterPesoCalculoSapDespCte(_cte.Chave);
                        }
                        else

                            // Verifica se � um fluxo de conteiner ou se a mercadoria est� condicionada em conteiner ou se a empresa � BRADO
                            if (_carregamentoService.VerificarFluxoConteiner(_fluxoComercial) || VerificaEmpresaBrado(_contrato.EmpresaPagadora))
                            {
                                // Caso conteiner
                                IList<CteDetalhe> listaCteDetalhe = _cteDetalheRepository.ObterPorCte(_cte);
                                List<string> listaConteiner = listaCteDetalhe.GroupBy(g => g.ConteinerNotaFiscal).Select(s => s.Key).ToList();
                                sapVagaoCte.PesoCalculo = listaConteiner.Count;
                            }
                            else
                            {
                                switch (_fluxoComercial.UnidadeMedida.Id)
                                {
                                    case "M3":
                                        sapVagaoCte.PesoCalculo = _cte.VolumeVagao;
                                        break;
                                    default:
                                        sapVagaoCte.PesoCalculo = _cte.PesoVagao ?? 0;
                                        break;
                                }
                            }

                        sapVagaoCte.ToneladaUtil = _cte.PesoVagao ?? 0.0;
                    }
                    else
                    {
                        // Verifica se � um fluxo de conteiner ou se a mercadoria est� condicionada em conteiner ou se a empresa � BRADO
                        // Se a empresa � brado, e o fluxo � de rateio de CTE, pega como base o peso do cte, gerado no despacho, a partir do peso das notas.
                        var indRateio = _cte.FluxoComercial.IndRateioCte ?? false;
                        if (VerificaEmpresaBrado(_contrato.EmpresaPagadora) && (indRateio))
                        {
                            //_sapDespCte.PesoCalculo = _itemDespacho.PesoCalculo ?? 0;

                            sapVagaoCte.PesoCalculo = (double)_carregamentoService.ObterPesoCalculoSapDespCte(_cte.Chave);
                        }
                        else

                            // Verifica se � um fluxo de conteiner ou se a mercadoria est� condicionada em conteiner ou se a empresa � BRADO
                            if (_carregamentoService.VerificarFluxoConteiner(_fluxoComercial) || VerificaEmpresaBrado(_contrato.EmpresaPagadora))
                            {
                                // Caso conteiner
                                IList<CteDetalhe> listaCteDetalhe = _cteDetalheRepository.ObterPorCte(_cte);
                                List<string> listaConteiner = listaCteDetalhe.GroupBy(g => g.ConteinerNotaFiscal).Select(s => s.Key).ToList();
                                sapVagaoCte.PesoCalculo = listaConteiner.Count;
                            }
                            else
                            {
                                sapVagaoCte.PesoCalculo = _itemDespacho.PesoCalculo ?? 0;
                            }

                        sapVagaoCte.ToneladaUtil = _itemDespacho.PesoToneladaUtil;
                    }
                }

                /*
                if ((_sapDespCte.TipoOperacao != "E") || (_sapDespCte.TipoOperacao != "CR"))
                {
                    if (_cte.Manutencao)
                    {
                        if ((_itemDespacho.NumeroConteiner != null) && (_itemDespacho.NumeroConteiner > 0))
                        {
                            IList<CteDetalhe> listaCteDetalhe = _cteDetalheRepository.ObterPorCte(_cte);
                            List<string> listaConteiner = listaCteDetalhe.GroupBy(g => g.ConteinerNotaFiscal).Select(s => s.Key).ToList();
                            sapVagaoCte.PesoCalculo = listaConteiner.Count;
                        }
                        else
                        {
                            switch (_fluxoComercial.UnidadeMedida.Id)
                            {
                                case "M3":
                                    sapVagaoCte.PesoCalculo = _cte.VolumeVagao;
                                    break;
                                default:
                                    sapVagaoCte.PesoCalculo = _cte.PesoVagao ?? 0;
                                    break;
                            }
                        }

                        sapVagaoCte.ToneladaUtil = _cte.PesoVagao ?? 0.0;
                    }
                    else
                    {
                        if (EhConteiner())
                        {
                            if ((_itemDespacho.NumeroConteiner != null) && (_itemDespacho.NumeroConteiner > 0))
                            {
                                sapVagaoCte.PesoCalculo = _itemDespacho.NumeroConteiner;
                            }
                            else
                            {
                                sapVagaoCte.PesoCalculo = _itemDespacho.PesoCalculo;
                            }

                            sapVagaoCte.ToneladaUtil = _itemDespacho.PesoToneladaUtil;
                        }
                        else
                        {
                            sapVagaoCte.PesoCalculo = _itemDespacho.PesoCalculo;
                            sapVagaoCte.ToneladaUtil = _itemDespacho.PesoToneladaUtil;
                        }
                    }
                }
                 */

                _sapVagaoCteRepository.InserirOuAtualizar(sapVagaoCte);
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(_cte, "SapVagaoCte", string.Format("SapVagaoCte: {0}", ex.Message), ex);
                throw;
            }
        }

        /// <summary>
        /// Grava Sap Chave Cte
        /// </summary>
    private void GravarSapChaveCte()
        {
            try
            {
                SapChaveCte sapChaveCte = new SapChaveCte();
                CteEmpresas cteEmpresa = _cteEmpresasRepository.ObterPorCte(_cte);
                var cteOrigem = _cteOrigemRepository.ObterPorCte(_cte).FirstOrDefault();

                IEmpresa empresaClienteSap = null;
                int? statusCte = ObterStatusRetorno();

                if (cteEmpresa == null)
                {
                    empresaClienteSap = _empresaRemetenteFluxoComercial;
                }
                else
                {
                    empresaClienteSap = cteEmpresa.EmpresaTomadora;
                }

                sapChaveCte.SapDespCte = _sapDespCte;

                if (_serieDespachoUf != null)
                {
                    sapChaveCte.Ferrovia = _serieDespachoUf.LetraFerrovia;
                }

                sapChaveCte.ChaveCte = _cte.Chave;
                sapChaveCte.ProtocoloSefaz = ObterProtocoloCte();
                sapChaveCte.DthrAutorizacao = ObterDataHoraProtocolo();
                sapChaveCte.Code = statusCte.HasValue ? statusCte.Value.ToString() : Tools.TruncateString(Convert.ToString(_cte.StatusVigente.CteStatusRetorno.Id), 3);
                sapChaveCte.SerieCte = _cte.Serie;
                sapChaveCte.NroCte = _cte.Numero;
                sapChaveCte.VlrTotal = _cte.ValorCte;
                sapChaveCte.Cnpj = Tools.TruncateRemoveSpecialChar(_cte.CnpjFerrovia, 14); // TODO CNPJ das filiais	Novo campo para atender o SAP no registro do livro fiscal.
                sapChaveCte.ClienteSap = empresaClienteSap.Sigla; // TODO C�digo SAP da empresa cliente, o mesmo do fluxo. Usar EP_SIG_EMP (Verificar o que envia no caso de fatura da Argentina.)
                sapChaveCte.AliquotaIcms = _cte.ContratoHistorico.PercentualAliquotaIcms;
                sapChaveCte.ValorIcms = _cte.ValorIcms;
                sapChaveCte.Observacao = null;
                sapChaveCte.Sequencia = _cteAgrupamento.Sequencia; // Sequencia do CTE no tr�fego m�tuo (ver sequencia no agrupamento)
                sapChaveCte.Cfop = _cte.Cfop;
                sapChaveCte.ChaveCteOrigem = cteOrigem == null ? string.Empty : cteOrigem.ChaveCte ;
                
                _sapChaveCteRepository.Inserir(sapChaveCte);
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(_cte, "SapChaveCte", string.Format("SapChaveCte: {0}", ex.Message), ex);
                throw;
            }
        }

        private void GravarSapChaveCteComplementar()
        {
            try
            {
                SapChaveCte sapChaveCte = new SapChaveCte();
                CteEmpresas cteEmpresa = _cteEmpresasRepository.ObterPorCte(_cte);
                int? statusCte = ObterStatusRetorno();
                IEmpresa empresaClienteSap = null;

                if (cteEmpresa == null)
                {
                    empresaClienteSap = _empresaRemetenteFluxoComercial;
                }
                else
                {
                    empresaClienteSap = cteEmpresa.EmpresaTomadora;
                }

                sapChaveCte.SapDespCte = _sapDespCte;
                sapChaveCte.ChaveCte = _cte.Chave;

                if (_serieDespachoUf != null)
                {
                    sapChaveCte.Ferrovia = _serieDespachoUf.LetraFerrovia;
                }

                sapChaveCte.ProtocoloSefaz = ObterProtocoloCte();
                sapChaveCte.DthrAutorizacao = ObterDataHoraProtocolo();
                sapChaveCte.Code = statusCte.HasValue ? statusCte.Value.ToString() : Tools.TruncateString(Convert.ToString(_cte.StatusVigente.CteStatusRetorno.Id), 3);
                sapChaveCte.SerieCte = _cte.Serie;
                sapChaveCte.NroCte = _cte.Numero;

                if (_listaCteComplementado[0].TipoComplemento == TipoOperacaoCteEnum.ComplementoIcms)
                {
                    sapChaveCte.VlrTotal = _cte.BaseCalculoIcms.Value;
                }
                else
                {
                    sapChaveCte.VlrTotal = Math.Round(_listaCteComplementado.Sum(g => g.ValorDiferenca), 2);
                }

                sapChaveCte.AliquotaIcms = _cte.PercentualAliquotaIcms;

                sapChaveCte.Cnpj = Tools.TruncateRemoveSpecialChar(_cte.CnpjFerrovia, 14);
                sapChaveCte.ClienteSap = empresaClienteSap.Sigla;
                sapChaveCte.ValorIcms = _cte.ValorIcms;
                sapChaveCte.Sequencia = 1;

                _sapChaveCteRepository.Inserir(sapChaveCte);
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(_cte, "SapChaveCte", string.Format("SapChaveCte: {0}", ex.Message), ex);
                throw;
            }
        }

        /// <summary>
        /// Grava Sap Composicao Frete Cte
        /// </summary>
        private void GravarSapComposicaoFreteCte()
        {
            try
            {
                SapComposicaoFreteCte sapComposicaoFreteCte;

                IList<ComposicaoFreteCte> listaComposicaoFreteCte = _composicaoFreteCteRepository.ObterComposicaoFreteCtePorCte(_cte);

                foreach (ComposicaoFreteCte composicaoFreteCte in listaComposicaoFreteCte)
                {
                    sapComposicaoFreteCte = new SapComposicaoFreteCte();
                    sapComposicaoFreteCte.SapDespCte = _sapDespCte;
                    sapComposicaoFreteCte.CondicaoTarifa = composicaoFreteCte.CondicaoTarifa;
                    sapComposicaoFreteCte.Ferrovia = composicaoFreteCte.Ferrovia;
                    sapComposicaoFreteCte.ValorComIcms = composicaoFreteCte.ValorComIcms;
                    sapComposicaoFreteCte.ValorSemIcms = composicaoFreteCte.ValorSemIcms;

                    _sapComposicaoFreteCteRepository.InserirOuAtualizar(sapComposicaoFreteCte);

                    composicaoFreteCte.DataEnvio = DateTime.Now;
                    _composicaoFreteCteRepository.Atualizar(composicaoFreteCte);
                }
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(_cte, "SapComposicaoFreteCte", string.Format("SapComposicaoFreteCte: {0}", ex.Message), ex);
                throw;
            }
        }

        private void CarregarDados()
        {
            try
            {
                if (_cte != null)
                {
                    // Contrato hist�rico
                    _contratoHistorico = _cte.ContratoHistorico;

                    // Dados do Vag�o
                    _vagao = _cte.Vagao;

                    //_despachoTranslogic = _despachoTranslogicRepository.ObterPorIdSemEstado(_cte.Despacho.Id ?? 0);
                    _despachoTranslogic = _cte.Despacho;

                    // Informa��es do fluxo comercial
                    _fluxoComercial = _cte.FluxoComercial;

                    // Lista de detalhes do Cte
                    _listaCteDetalhe = _cteDetalheRepository.ObterPorCte(_cte);

                    _cteVersao = _cte.Versao;
                }

                // Carregar os dados do cte raiz
                CteArvore cteArvore = _cteArvoreRepository.ObterCteArvorePorCteFilho(_cte);
                if (cteArvore != null)
                {
                    _cteRaiz = _cteRepository.ObterCtePorHql(cteArvore.CteRaiz);
                    _ctePai = _cteRepository.ObterCtePorHql(cteArvore.CtePai);
                }

                _cteAgrupamento = _cteAgrupamentoRepository.ObterAgrupamentoRaizPorCteFilho(_cte);
                if (_cteAgrupamento != null)
                {
                    _cteRaizAgrupamento = _cteAgrupamento.CteRaiz;
                }

                _serieDespachoUf = _serieDespachoUfRepository.ObterPorCnpjUf(_cte.CnpjFerrovia, _cte.SiglaUfFerrovia);

                if (_despachoTranslogic != null)
                {
                    // Carregar os dados do Item de Despacho
                    _itemDespacho = _itemDespachoRepository.ObterItemDespachoPorDespacho(_despachoTranslogic);
                }

                if (_itemDespacho != null)
                {
                    // Carregar as informa��es do detalhe carregamento
                    _detalheCarregamento = _itemDespacho.DetalheCarregamento;
                }

                if (_detalheCarregamento != null)
                {
                    // Carregar as informa��es de carregamento
                    _carregamento = _detalheCarregamento.Carregamento;
                }

                if (_carregamento != null)
                {
                    _pedidoDerivado = _carregamento.PedidoDerivado;

                    if (_pedidoDerivado != null)
                    {
                        _vagaoPedido = _vagaoPedidoRepository.ObterVagaoPedidoPorPedidoDerivado(_pedidoDerivado.Id.Value);
                    }
                    else
                    {
                        _vagaoPedido = _vagaoPedidoRepository.ObterVagaoPedidoPorPedido(_carregamento.IdPedido);
                    }
                }

                if (_contratoHistorico != null)
                {
                    _contrato = _contratoHistorico.Contrato;
                }

                if (_vagao != null)
                {
                    _empresaProprietariaVagao = _vagao.EmpresaProprietaria;
                }

                if (_fluxoComercial != null)
                {
                    _empresaRemetenteFluxoComercial = _fluxoComercial.EmpresaRemetente;
                    _estacaoMaeOrigemFluxoComercial = _fluxoComercial.Origem;
                    _estacaoMaeDestinoFluxoComercial = _fluxoComercial.Destino;
                }

                // Verifica qual � o Tipo de Trafego Mutuo
                _tipoTrafegoMutuo = ObterTipoTrafegoMutuo();

                /* Verifica se � uma partilha da ferroeste (Ferropar) */
                /* Verifica se � uma partilha da ferroeste (Ferropar) */
                if ((_estacaoMaeOrigemFluxoComercial != null) && (_estacaoMaeOrigemFluxoComercial.EmpresaConcessionaria != null))
                {
                    _partilhaFerroeste = (_estacaoMaeOrigemFluxoComercial.EmpresaConcessionaria.Id == 49) ? true : false;

                    if (_partilhaFerroeste)
                    {
                        _origemFerroeste = true;
                    }
                }

                if ((_estacaoMaeDestinoFluxoComercial != null) && (_estacaoMaeDestinoFluxoComercial.EmpresaConcessionaria != null))
                {
                    if (!_partilhaFerroeste)
                    {
                        _partilhaFerroeste = (_estacaoMaeDestinoFluxoComercial.EmpresaConcessionaria.Id == 49) ? true : false;
                        if (_partilhaFerroeste)
                        {
                            _origemFerroeste = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(_cte, "GravarConfigCteEnvioService", string.Format("CarregarDados: {0}", ex.Message), ex);
                throw;
            }
        }

        private void CarregarDadosCteComplementar()
        {
            try
            {
                if (_cte != null)
                {
                    // Informa��es do fluxo comercial
                    _fluxoComercial = _cte.FluxoComercial;

                    _cteVersao = _cte.Versao;
                }

                if (_fluxoComercial != null)
                {
                    _empresaRemetenteFluxoComercial = _fluxoComercial.EmpresaRemetente;
                    _estacaoMaeOrigemFluxoComercial = _fluxoComercial.Origem;
                    _estacaoMaeDestinoFluxoComercial = _fluxoComercial.Destino;
                }

                _serieDespachoUf = _serieDespachoUfRepository.ObterPorCnpjUf(_cte.CnpjFerrovia, _cte.SiglaUfFerrovia);

                // Carrega as informa��es dos Cte Complementados
                _listaCteComplementado = _cteComplementadoRepository.ObterCteComplementadoPorCteComplementar(_cte);
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(_cte, "GravarConfigCteEnvioService", string.Format("CarregarDadosCteComplementar: {0}", ex.Message), ex);
                throw;
            }
        }

        private void ValidarDadosCteVirtual()
        {
            StringBuilder buffer = new StringBuilder();

            if (_cte == null)
            {
                buffer.Append("_cte:null ");
            }

            if (_listaCteDetalhe == null)
            {
                buffer.Append("_listaCteDetalhe:null ");
            }

            if (_cteRaiz == null)
            {
                buffer.Append("_cteRaiz:null ");
            }

            if (_ctePai == null)
            {
                buffer.Append("_ctePai:null ");
            }

            if (_cteAgrupamento == null)
            {
                buffer.Append("_cteAgrupamento:null ");
            }

            if (_cteRaizAgrupamento == null)
            {
                buffer.Append("_cteRaizAgrupamento:null ");
            }

            if (_cteVersao == null)
            {
                buffer.Append("_cteVersao:null ");
            }

            if (_fluxoComercial == null)
            {
                buffer.Append("_fluxoComercial:null ");
            }

            if (_empresaRemetenteFluxoComercial == null)
            {
                buffer.Append("_empresaRemetenteFluxoComercial:null ");
            }

            if (_contratoHistorico == null)
            {
                buffer.Append("_contratoHistorico:null ");
            }

            if (_contrato == null)
            {
                buffer.Append("_contrato:null ");
            }

            if (_vagao == null)
            {
                buffer.Append("_vagao:null ");
                // new Exception("Vag�o n�o econtrado");
            }

            if (_empresaProprietariaVagao == null)
            {
                buffer.Append("_empresaProprietariaVagao:null ");
                // new Exception("Empresa Propriet�ria Vag�o n�o econtrado");
            }

            if (buffer.Length > 0)
            {
                throw new Exception(buffer.ToString());
            }
        }

        private void ValidarDados()
        {
            string tipoOperacao = string.Empty;
            CteInterfaceEnvioSap cteInterfaceEnvioSap = _cteInterfaceEnvioSapRepository.ObterPorCte(_cte.Id.Value);
            if (cteInterfaceEnvioSap != null)
            {
                tipoOperacao = Translogic.Core.Commons.Enum<TipoOperacaoCteEnum>.GetDescriptionOf(cteInterfaceEnvioSap.TipoOperacao.Value);
            }

            StringBuilder buffer = new StringBuilder();

            if (_cte == null)
            {
                buffer.Append("_cte:null ");
            }

            if (_listaCteDetalhe == null)
            {
                buffer.Append("_listaCteDetalhe:null ");
            }

            if (_cteRaiz == null)
            {
                buffer.Append("_cteRaiz:null ");
            }

            if (_ctePai == null)
            {
                buffer.Append("_ctePai:null ");
            }

            if (_cteAgrupamento == null)
            {
                buffer.Append("_cteAgrupamento:null ");
            }

            if (_cteRaizAgrupamento == null)
            {
                buffer.Append("_cteRaizAgrupamento:null ");
            }

            if (_cteVersao == null)
            {
                buffer.Append("_cteVersao:null ");
            }

            if (_fluxoComercial == null)
            {
                buffer.Append("_fluxoComercial:null ");
            }

            if (_despachoTranslogic == null)
            {
                buffer.Append("_despachoTranslogic:null ");
            }

            if (_serieDespachoUf == null)
            {
                buffer.Append("_serieDespachoUf:null ");
            }

            if (_empresaRemetenteFluxoComercial == null)
            {
                buffer.Append("_empresaRemetenteFluxoComercial:null ");
            }

            if (_contratoHistorico == null)
            {
                buffer.Append("_contratoHistorico:null ");
            }

            if (_contrato == null)
            {
                buffer.Append("_contrato:null ");
            }

            if (_itemDespacho == null)
            {
                buffer.Append("_itemDespacho:null ");
            }

            if ((tipoOperacao != "E") && (tipoOperacao != "CR"))
            {
                /*
                if (_detalheCarregamento == null)
                {
                        buffer.Append("_detalheCarregamento:null ");
                }

                if (_carregamento == null)
                {
                        buffer.Append("_carregamento:null ");
                        // new Exception("Carregamento n�o econtrado");
                }

                if (_pedidoDerivado == null)
                {
                        buffer.Append("_pedidoDerivado:null ");
                        // new Exception("Pedido Derivado n�o econtrado");
                }
                 */

                // if (_vagaoPedido == null)
                // {
                // 	buffer.Append("_vagaoPedido:null ");
                // new Exception("Vag�o Pedido n�o econtrado");
                // }
            }

            if (_vagao == null)
            {
                buffer.Append("_vagao:null ");
                // new Exception("Vag�o n�o econtrado");
            }

            if (_empresaProprietariaVagao == null)
            {
                buffer.Append("_empresaProprietariaVagao:null ");
                // new Exception("Empresa Propriet�ria Vag�o n�o econtrado");
            }

            if (_serieDespachoUf == null)
            {
                buffer.Append("_serieDespachoUf:null ");
            }

            if (buffer.Length > 0)
            {
                throw new Exception(buffer.ToString());
            }
        }

        private void ValidarDadosCteComplementar()
        {
            StringBuilder buffer = new StringBuilder();

            if (_cte == null)
            {
                buffer.Append("_cte:null ");
            }

            if (_cteVersao == null)
            {
                buffer.Append("_cteVersao:null ");
            }

            if (_fluxoComercial == null)
            {
                buffer.Append("_fluxoComercial:null ");
            }

            if (_empresaRemetenteFluxoComercial == null)
            {
                buffer.Append("_empresaRemetenteFluxoComercial:null ");
            }

            if (_cte.TipoCte != TipoCteEnum.Virtual)
            {
                if ((_listaCteComplementado == null) || (_listaCteComplementado.Count == 0))
                {
                    buffer.Append("_listaCteComplementado:null ou vazio");
                }
            }

            if (buffer.Length > 0)
            {
                throw new Exception(buffer.ToString());
            }
        }

        /// <summary>
        /// Verifica se o fluxo � um fluxo internacional
        /// </summary>
        /// <param name="fluxo"> Objeto FluxoComercial. </param>
        /// <returns> Valor booleano </returns>
        private string VerificarFluxoInternacional(FluxoComercial fluxo)
        {
            AssociaFluxoInternacional associaFluxoInternacional = _associaFluxoInternacionalRepository.ObterPorFluxoComercial(fluxo);

            if (associaFluxoInternacional != null)
            {
                FluxoInternacional fluxoInternacional = associaFluxoInternacional.FluxoInternacional;

                if (fluxoInternacional != null)
                {
                    return fluxoInternacional.CodigoFluxoInternacional;
                }
            }

            return null;
        }

        /// <summary>
        /// Obt�m o tipo de trafego mutuo
        /// </summary>
        /// <returns>Retorna o tipo de trafego mutuo</returns>
        private TipoTrafegoMutuoEnum ObterTipoTrafegoMutuo()
        {
            TipoTrafegoMutuoEnum tipoTrafegoMutuo = TipoTrafegoMutuoEnum.Indefinido;
            if (_contrato.FerroviaFaturamento == null)
            {
                return tipoTrafegoMutuo;
            }

            string siglaFerrovia = ALL.Core.Util.Enum<FerroviaFaturamentoEnum>.GetDescriptionOf(_contrato.FerroviaFaturamento.Value);

            // IEmpresa empresaFerrovia = _empresaFerroviaRepository.ObterPorSiglaUf(siglaFerrovia, _cteRaizAgrupamento.SiglaUfFerrovia);
            IEmpresa empresaFerrovia = ObterEmpresaFerrovia(siglaFerrovia);
            /*
            if (empresaFerrovia == null)
            {
                    empresaFerrovia = _empresaFerroviaCliente;
            }
            */
            if (empresaFerrovia == null)
            {
                return TipoTrafegoMutuoEnum.Indefinido;
            }

            if (empresaFerrovia.IndEmpresaALL == false)
            {
                if (_fluxoComercial.OrigemIntercambio != null)
                {
                    tipoTrafegoMutuo = TipoTrafegoMutuoEnum.IntercambioFaturamentoOrigem;
                }
                else if (_fluxoComercial.DestinoIntercambio != null)
                {
                    tipoTrafegoMutuo = TipoTrafegoMutuoEnum.IntercambioFaturamentoDestino;
                }
                else
                {
                    tipoTrafegoMutuo = TipoTrafegoMutuoEnum.Indefinido;
                }
            }
            else
            {
                tipoTrafegoMutuo = TipoTrafegoMutuoEnum.FaturamentoEmpresaGrupo;
            }

            return tipoTrafegoMutuo;
        }

        private IEmpresa ObterEmpresaFerrovia(string siglaFerrovia)
        {
            IEmpresa empresaFerrovia = _empresaFerroviaRepository.ObterPorSiglaUf(siglaFerrovia, _cteRaizAgrupamento.SiglaUfFerrovia);

            if (empresaFerrovia == null)
            {
                empresaFerrovia = _empresaInterfaceCteRepository.ObterPorSiglaUF(siglaFerrovia, _cteRaizAgrupamento.SiglaUfFerrovia);
            }

            return empresaFerrovia;
        }

        private bool EhConteiner()
        {
            CteDetalhe cteDetalhe = _listaCteDetalhe[0];
            string conteiner = cteDetalhe.ConteinerNotaFiscal;
            if (string.IsNullOrEmpty(conteiner))
            {
                return false;
            }

            return true;
        }

        private bool PartilhaFerroeste()
        {
            return _partilhaFerroeste;
        }

        private bool OrigemFerroeste()
        {
            return _origemFerroeste;
        }

        private double ObterValorCotacao(DateTime dataDespacho)
        {
            double valorCotacao = 1.0;
            string unidade = string.Empty;

            if (_fluxoComercial.Contrato != null)
            {
                unidade = _fluxoComercial.Contrato.UnidadeMonetaria ?? "BRL";
            }
            else
            {
                unidade = "BRL";
            }

            if (!string.IsNullOrEmpty(unidade))
            {
                try
                {
                    Cotacao cotacao = _cotacaoRepository.ObterCotacaoPorData(dataDespacho, unidade);
                    if (cotacao != null)
                    {
                        valorCotacao = cotacao.Valor.Value;
                    }
                }
                catch (Exception)
                {
                    valorCotacao = 1.0;
                }
            }

            return valorCotacao;
        }

        /// <summary>
        /// Obt�m o numero e s�rie do despacho para os Ctes TakeOrPay ou Complementos - Utilizado no cancelamento
        /// </summary>
        /// <param name="chaveCte">N�mero da chave do Cte</param>
        /// <param name="serieDespachoSap">S�rie do despacho gerado pelo SAP</param>
        /// <param name="numeroDespachoSap">Numero do despacho gerado pelo SAP</param>
        private void ObterNumeroSerieDespachoGeradoPeloSap(string chaveCte, out string serieDespachoSap, out int numeroDespachoSap)
        {
            serieDespachoSap = string.Empty;
            numeroDespachoSap = 0;

            SapChaveCte sapChaveCteAux = _sapChaveCteRepository.ObterPorChaveCte(chaveCte);
            if (sapChaveCteAux != null)
            {
                serieDespachoSap = sapChaveCteAux.SapDespCte.SerieDespachoSap;
                numeroDespachoSap = sapChaveCteAux.SapDespCte.NumeroDespachoSap;
            }
        }

        /// <summary>
        /// Verifica se os dados do Cte j� foram enviados para o SAP
        /// </summary>
        /// <param name="chaveCte">Chave do Cte</param>
        /// <param name="tipoOperacaoCteEnum">Tipo da opera��o de envio</param>
        /// <returns>Retorna verdadeiro caso essas informa��es j� tenha sido enviados para o SAP</returns>
        private bool VerificarCteJaEnviadoSap(string chaveCte, TipoOperacaoCteEnum tipoOperacaoCteEnum)
        {
            string tipoOperacao = Translogic.Core.Commons.Enum<TipoOperacaoCteEnum>.GetDescriptionOf(tipoOperacaoCteEnum);
            var sapChavesCteAux = _sapChaveCteRepository.ObterPorChavesCte(chaveCte);
            return sapChavesCteAux.Any(p => p.SapDespCte.TipoOperacao.ToUpperInvariant() == tipoOperacao.ToUpperInvariant());
        }

        /// <summary>
        /// Grava SAP Notas CTE
        /// </summary>
        private void GravarSapNotasAnulacaoCte()
        {
            try
            {
                SapNotasCte sapNotasCte;

                sapNotasCte = new SapNotasCte();

                sapNotasCte.SapDespCte = _sapDespCte;
                sapNotasCte.SerieNota = Tools.TruncateString(_cte.NfeAnulacao.SerieNotaFiscal, 3);
                sapNotasCte.NroNota = Tools.TruncateRemoveSpecialChar(_cte.NfeAnulacao.NumeroNotaFiscal.ToString(), 12);
                sapNotasCte.Modelo = "55";
                sapNotasCte.Valor = _cte.NfeAnulacao.Valor;
                sapNotasCte.DthRemissao = _cte.NfeAnulacao.DataEmissao;
                sapNotasCte.UfRemetente = _cte.NfeAnulacao.UfEmitente;
                sapNotasCte.CnpjRemetente = Tools.TruncateRemoveSpecialChar(_cte.NfeAnulacao.CnpjEmitente, 14);
                sapNotasCte.InsRemetente = Tools.TruncateRemoveSpecialChar(_cte.NfeAnulacao.InscricaoEstadualEmitente, 14);
                sapNotasCte.UfDestinatario = _cte.NfeAnulacao.UfDestinatario;
                sapNotasCte.CnpjDestinatario = Tools.TruncateRemoveSpecialChar(_cte.NfeAnulacao.CnpjDestinatario, 14);
                sapNotasCte.InsDestinatario = Tools.TruncateRemoveSpecialChar(_cte.NfeAnulacao.InscricaoEstadualDestinatario, 14);
                sapNotasCte.Nfe = _cte.NfeAnulacao.ChaveNfe;
                sapNotasCte.PesoTotal = _cte.NfeAnulacao.PesoBruto;
                sapNotasCte.PesoRateio = _cte.NfeAnulacao.Peso;
                sapNotasCte.Protocolo = _cte.NfeAnulacao.Protocolo;
                _sapNotasCteRepository.InserirOuAtualizar(sapNotasCte);
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(_cte, "GravarSapNotasCte", string.Format("GravarSapNotasCte: {0}", ex.Message), ex);
                throw;
            }
        }

        /// <summary>
        /// Grava SAP Notas CTE
        /// </summary>
        private void GravarSapCteAnulado()
        {
            try
            {
                IList<Cte> ctes = _cteRepository.ObterPorChaveAnulacao(_cte.NfeAnulacao);

                foreach (Cte cte in ctes)
                {
                    SapCteAnulado sapCteAnulado = new SapCteAnulado();

                    sapCteAnulado.SapDespCte = _sapDespCte;
                    sapCteAnulado.ChaveCte = cte.Chave;
                    _sapCteAnuladoRepository.InserirOuAtualizar(sapCteAnulado);
                }
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(_cte, "GravarSapNotasCte", string.Format("GravarSapNotasCte: {0}", ex.Message), ex);
                throw;
            }
        }

        /// <summary>
        /// Grava SAP  CTE anulado - por cte.
        /// </summary>
        private void GravarSapCteAnulado(Cte cte)
        {
            try
            {
                SapCteAnulado sapCteAnulado = new SapCteAnulado();

                sapCteAnulado.SapDespCte = _sapDespCte;
                sapCteAnulado.ChaveCte = cte.Chave;
                _sapCteAnuladoRepository.InserirOuAtualizar(sapCteAnulado);
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(_cte, "GravarSapNotasCte", string.Format("GravarSapNotasCte: {0}", ex.Message), ex);
                throw;
            }
        }



        /// <summary>
        /// Verifica se a empresa � Brado
        /// </summary>
        /// <param name="empresa">Empresa para ser verificada</param>
        /// <returns>Retorna verdadeiro caso seja a empresa Brado, falso caso contr�rio</returns>
        private bool VerificaEmpresaBrado(IEmpresa empresa)
        {
            if (empresa == null)
            {
                return false;
            }

            string descricao = empresa.DescricaoResumida.ToUpper();
            return descricao.IndexOf("BRADO") > -1 ? true : false;
        }

        private string ObterProtocoloCte()
        {
            int? statusCte = ObterStatusRetorno();
            string protocolo = string.Empty;

            if (statusCte.HasValue)
            {
                CteStatus cteStatus = _cteStatusRepository.ObterCteStatusPorSituacaoCte(_cte, statusCte.Value);

                if (cteStatus != null)
                {
                    protocolo = cteStatus.Protocolo;
                }
            }

            if (string.IsNullOrEmpty(protocolo))
            {
                protocolo = _cte.NumeroProtocolo;
            }

            return protocolo;
        }

        private DateTime? ObterDataHoraProtocolo()
        {
            int? statusCte = ObterStatusRetorno();
            DateTime? dataProtocolo = null;

            if (statusCte.HasValue)
            {
                CteStatus cteStatus = _cteStatusRepository.ObterCteStatusPorSituacaoCte(_cte, statusCte.Value);

                if (cteStatus != null)
                {
                    dataProtocolo = cteStatus.DhProtocolo;
                }
            }

            if (!dataProtocolo.HasValue)
            {
                if (!string.IsNullOrEmpty(_cte.NumeroProtocolo))
                {
                    _consultaCte = _filaProcessamentoService.ObterRetornoCteProcessadoPorProtocolo(long.Parse(_cte.NumeroProtocolo));
                    dataProtocolo = _consultaCte == null ? null : _consultaCte.DhProtocolo;
                }
            }

            return dataProtocolo;
        }

        private int? ObterStatusRetorno()
        {
            CteInterfaceEnvioSap cteInterfaceEnvioSap = _cteInterfaceEnvioSapRepository.ObterPorCte(_cte.Id.Value);
            int? status = null;

            switch (cteInterfaceEnvioSap.TipoOperacao)
            {
                case TipoOperacaoCteEnum.Complemento:
                    status = 100;
                    break;
                case TipoOperacaoCteEnum.ComplementoIcms:
                    status = 100;
                    break;
                case TipoOperacaoCteEnum.Substituicao:
                    status = 100;
                    break;
                case TipoOperacaoCteEnum.Virtual:
                    status = 100;
                    break;
                case TipoOperacaoCteEnum.Inclusao:
                    status = 100;
                    break;
                case TipoOperacaoCteEnum.CancelamentoRejeitado:
                    status = 220;
                    break;
                case TipoOperacaoCteEnum.Exclusao:
                    status = 135;
                    break;
                case TipoOperacaoCteEnum.Inutilizado:
                    status = 102;
                    break;
                case TipoOperacaoCteEnum.Denegado:
                    status = 110;
                    break;
            }

            return status;
        }
    }
}
