namespace Translogic.Modules.Core.Domain.Services.Ctes
{
	using System;
	using Model.FluxosComerciais.ConhecimentoTransporteEletronico;
	using Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;

	/// <summary>
	/// Classe para grava��o dos dados na config do CT-e de inutiliza��o
	/// </summary>
	public class GravarConfigCteInutilizacaoService
	{
		private readonly ICteRepository _cteRepository;
		private CteLogService _cteLogService;
		private Cte _cte;
		private string _justificativa;

		/// <summary>
		/// Initializes a new instance of the <see cref="GravarConfigCteInutilizacaoService"/> class.
		/// </summary>
		/// <param name="cteRepository">Reposit�rio do Cte injetado</param>
		/// <param name="cteLogService">Servico de log do cte injetado</param>
		public GravarConfigCteInutilizacaoService(ICteRepository cteRepository, CteLogService cteLogService)
		{
			_cteRepository = cteRepository;
			_cteLogService = cteLogService;
		}

		/// <summary>
		/// Executa a grava��o dos dados de cancelamento
		/// </summary>
		/// <param name="cte">Cte que ser� gerado o arquivo de cancelamento</param>
		/// <param name="justificativa">Justificativa do cancelamento</param>
		public void Executar(Cte cte, string justificativa)
		{
			try
			{
				_cte = cte;
				_justificativa = justificativa;

				CarregarDados();

				// Todo: To Implement
			}
			catch (Exception ex)
			{
				_cteLogService.InserirLogErro(_cte, "GravarConfigCteInutilizacaoService", string.Format("Executar: {0}", ex.Message), ex);
				throw;
			}
		}

		private void CarregarDados()
		{
		}
	}
}
