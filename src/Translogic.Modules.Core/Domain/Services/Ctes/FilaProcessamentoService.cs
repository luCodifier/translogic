namespace Translogic.Modules.Core.Domain.Services.Ctes
{
    using System;
    using System.Globalization;
    using System.Linq;
    using Model.Codificador;
    using Model.Codificador.Repositories;
    using Model.Diversos;
    using Model.Diversos.Ibge;
    using Model.Diversos.Ibge.Repositories;
    using Model.Estrutura;
    using Model.FluxosComerciais.ConhecimentoTransporteEletronico;
    using Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;
    using Model.FluxosComerciais.Pedidos.Despachos.Repositories;
    using Model.FluxosComerciais.TriangulusConfig;
    using Model.FluxosComerciais.TriangulusConfig.Repositories;
    using Model.Via;
    using Util;

    /// <summary>
    /// Servi�o para gravar na fila de processamento da Config os Ctes
    /// </summary>
    public class FilaProcessamentoService
    {
        private readonly ICte01ListaRepository _cte01ListaRepository;
        private readonly IConfiguracaoTranslogicRepository _configuracaoTranslogicRepository;
        private readonly IUfIbgeRepository _unidadeFederativaIbgeRepository;
        private readonly IVw209ConsultaCteRepository _consultaCteRepository;
        private readonly ISerieDespachoUfRepository _serieDespachoUfRepository;
        private readonly INfe03FilialRepository _nfe03FilialRepository;
        private readonly ICte51Repository _cte51Repository;
        private readonly INfe20ErroLogRepository _nfe20ErroLogRepository;
        private readonly CteLogService _cteLogService;
        private readonly ICteSerieNumeroEmpresaUfRepository _cteSerieNumeroEmpresaUfRepository;
        private readonly ICteEnvioXmlRepository _cteEnvioXmlRepository;
        private readonly ICte50Repository _cte50Repository;
        private readonly ICteEmpresasRepository _cteEmpresasRepository;

        private readonly string _chaveCteCodigoInternoFilial = "CTE_CODIGO_INTERNO_FILIAL_DEFAULT";
        private readonly string _chaveCteTipoAmbiente = "CTE_TIPO_AMBIENTE";
        private readonly string _chaveVersaoSefaz = "CTE_VERSAO_SEFAZ";
        private readonly string _chaveFtpHost = "CTE_FTP_HOST";
        private readonly string _chaveFtpPath = "CTE_FTP_PATH";
        private readonly string _chaveFtpUsuario = "CTE_FTP_USUARIO";
        private readonly string _chaveFtpSenha = "CTE_FTP_SENHA";

        /// <summary>
        /// Classe para gravar os dados do CT-e na fila de processamento da config
        /// </summary>
        /// <param name="cte01ListaRepository">Reposit�rio da fila de processamento da config (envio) injetado</param>
        /// <param name="configuracaoTranslogicRepository">Reposit�rio das configura��es gerais do translogic injetado</param>
        /// <param name="unidadeFederativaIbgeRepository">Reposit�rio do UF do IBGE injetado</param>
        /// <param name="consultaCteRepository">Reposit�rio de consulta de retorno do Cte injetado</param>
        /// <param name="serieDespachoUfRepository">Repositorio de serie de despacho uf</param>
        /// <param name="nfe03FilialRepository">Reposit�rio da filial injetado</param>
        /// <param name="cte51Repository">Reposit�rio do cte51 dados de ftp para o PDF injetado</param>
        /// <param name="nfe20ErroLogRepository"> Classe de Log de Erro</param>
        /// <param name="cteLogService"> Servi�o do log do cte injetado</param>
        /// <param name="cteSerieNumeroEmpresaUfRepository"> Cte serie Empresa Uf injetado</param>
        /// <param name="cteEnvioXmlRepository">Repositorio de envio do xml do Cte injetado</param>
        /// <param name="cte50Repository">Reposit�rio de gera��o de arquivo pdf por impress�o</param>
        /// <param name="cteEmpresasRepository">Reposit�rio de cte empresas injetado.</param>
        public FilaProcessamentoService(ICte01ListaRepository cte01ListaRepository, IConfiguracaoTranslogicRepository configuracaoTranslogicRepository, IUfIbgeRepository unidadeFederativaIbgeRepository, IVw209ConsultaCteRepository consultaCteRepository, ISerieDespachoUfRepository serieDespachoUfRepository, INfe03FilialRepository nfe03FilialRepository, ICte51Repository cte51Repository, INfe20ErroLogRepository nfe20ErroLogRepository, CteLogService cteLogService, ICteSerieNumeroEmpresaUfRepository cteSerieNumeroEmpresaUfRepository, ICteEnvioXmlRepository cteEnvioXmlRepository, ICte50Repository cte50Repository, ICteEmpresasRepository cteEmpresasRepository)
        {
            _cte01ListaRepository = cte01ListaRepository;
            _cte50Repository = cte50Repository;
            _cteEmpresasRepository = cteEmpresasRepository;
            _cteEnvioXmlRepository = cteEnvioXmlRepository;
            _cteSerieNumeroEmpresaUfRepository = cteSerieNumeroEmpresaUfRepository;
            _cteLogService = cteLogService;
            _nfe20ErroLogRepository = nfe20ErroLogRepository;
            _cte51Repository = cte51Repository;
            _nfe03FilialRepository = nfe03FilialRepository;
            _serieDespachoUfRepository = serieDespachoUfRepository;
            _consultaCteRepository = consultaCteRepository;
            _unidadeFederativaIbgeRepository = unidadeFederativaIbgeRepository;
            _configuracaoTranslogicRepository = configuracaoTranslogicRepository;
        }

        /// <summary>
        /// Insere na fila de processamento da Config o Cte de envio
        /// </summary>
        /// <param name="cte">Cte a ser inserido na fila</param>
        /// <returns>Retorna o Id que o cte esta na lista</returns>
        public int InserirCteEnvioFilaProcessamento(Cte cte)
        {
            try
            {
                long cnpjEmitente = 0;
                long cnpjTomador = 0;
                // ConfiguracaoTranslogic tipoAmbiente = _configuracaoTranslogicRepository.ObterPorId(_chaveCteTipoAmbiente);
                ConfiguracaoTranslogic versaoSefaz = _configuracaoTranslogicRepository.ObterPorId(_chaveVersaoSefaz);

                cnpjEmitente = Convert.ToInt64(cte.CnpjFerrovia);
                CteEmpresas cteEmpresa = _cteEmpresasRepository.ObterPorCte(cte);

                if (cteEmpresa.EmpresaTomadora.TipoPessoa == TipoPessoaEnum.Juridica)
                {
                    cnpjTomador = Convert.ToInt64(Tools.TruncateRemoveSpecialChar(cteEmpresa.EmpresaTomadora.Cgc, 20));
                }
                else
                {
                    cnpjTomador = Convert.ToInt64(Tools.TruncateRemoveSpecialChar(cteEmpresa.EmpresaTomadora.Cpf, 20));
                }

                Nfe03Filial filial = ObterIdentificadorFilial(cte);
                int numeroCte = Convert.ToInt32(cte.Numero);
                int serieCte = Convert.ToInt32(cte.Serie);

                Cte01Lista itemProcessamento = new Cte01Lista();
                itemProcessamento.IdFilial = (int)filial.IdFilial;
                itemProcessamento.Uf = filial.Uf;
                itemProcessamento.Numero = numeroCte;
                itemProcessamento.Serie = serieCte;
                itemProcessamento.DataHoraEmissao = GetDhEmissao(cte);
                itemProcessamento.Tipo = 0; // Cte emitido
                itemProcessamento.CnpjEmitente = cnpjEmitente;
                itemProcessamento.CnpjDestinatario = cnpjTomador;
                itemProcessamento.TipoAmbiente = ObterAmbienteSefaz(cte);
                itemProcessamento.VersaoSefaz = Convert.ToDouble(cte.Versao.CodigoVersao, CultureInfo.GetCultureInfo("en-us"));
                itemProcessamento.ModeloDocumentoFiscal = "57";
                itemProcessamento.Flag = 0;
                itemProcessamento.TipoEmissao = 1;
                itemProcessamento.TipoImpressao = 1;
                itemProcessamento.IdLote = null;
                itemProcessamento.ChaveAcesso = cte.Chave;
                itemProcessamento.DataHoraEntradaLista = DateTime.Now;

                _cte01ListaRepository.Inserir(itemProcessamento);

                // Obs: Como a config implementa uma trigger para gerar a sequence dessa tabela, 
                // foi alterado o mapeamento e � realizado um novo sql para ver qual id foi gerado
                Cte01Lista itemLista = _cte01ListaRepository.ObterListaFilaProcessamento((int)filial.IdFilial, numeroCte, serieCte, cte.Chave, 0, false);
                return itemLista.Id;
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(cte, "FilaProcessamentoService", string.Format("InserirCteEnvioFilaProcessamento: Erro na inser��o da fila de processamento {0}", ex.Message), ex);
                throw;
            }
        }

        /// <summary>
        /// Insere na fila de processamento da Config o Cte de envio
        /// </summary>
        /// <param name="cte">Cte a ser inserido na fila</param>
        /// <returns>Retorna o Id que o cte esta na lista</returns>
        public int InserirCteCorrecaEnvioFilaProcessamento(Cte cte)
        {
            try
            {
                long cnpjEmitente = 0;
                long cnpjTomador = 0;
                // ConfiguracaoTranslogic tipoAmbiente = _configuracaoTranslogicRepository.ObterPorId(_chaveCteTipoAmbiente);
                ConfiguracaoTranslogic versaoSefaz = _configuracaoTranslogicRepository.ObterPorId(_chaveVersaoSefaz);

                cnpjEmitente = Convert.ToInt64(cte.CnpjFerrovia);
                CteEmpresas cteEmpresa = _cteEmpresasRepository.ObterPorCte(cte);
                cnpjTomador = Convert.ToInt64(Tools.TruncateRemoveSpecialChar(cteEmpresa.EmpresaTomadora.Cgc, 20));

                Nfe03Filial filial = ObterIdentificadorFilial(cte);
                int numeroCte = Convert.ToInt32(cte.Numero);
                int serieCte = Convert.ToInt32(cte.Serie);

                Cte01Lista itemProcessamento = new Cte01Lista();
                itemProcessamento.IdFilial = (int)filial.IdFilial;
                itemProcessamento.Uf = filial.Uf;
                itemProcessamento.Numero = numeroCte;
                itemProcessamento.Serie = serieCte;
                itemProcessamento.DataHoraEmissao = GetDhEmissao(cte);
                itemProcessamento.Tipo = 6; // Cte emitido
                itemProcessamento.Justificativa = "Carta de Correcao";
                itemProcessamento.CnpjEmitente = cnpjEmitente;
                itemProcessamento.CnpjDestinatario = cnpjTomador;
                itemProcessamento.TipoAmbiente = ObterAmbienteSefaz(cte);
                itemProcessamento.VersaoSefaz = Convert.ToDouble(ObterVers�oCte(cte), CultureInfo.GetCultureInfo("en-us"));
                itemProcessamento.ModeloDocumentoFiscal = "57";
                itemProcessamento.Flag = 0;
                itemProcessamento.TipoEmissao = 1;
                itemProcessamento.TipoImpressao = 1;
                itemProcessamento.IdLote = null;
                itemProcessamento.ChaveAcesso = cte.Chave;
                itemProcessamento.DataHoraEntradaLista = DateTime.Now;

                _cte01ListaRepository.Inserir(itemProcessamento);

                // Obs: Como a config implementa uma trigger para gerar a sequence dessa tabela, 
                // foi alterado o mapeamento e � realizado um novo sql para ver qual id foi gerado
                Cte01Lista itemLista = _cte01ListaRepository.ObterListaFilaProcessamento((int)filial.IdFilial, numeroCte, serieCte, cte.Chave, 6, false);
                return itemLista.Id;
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(cte, "FilaProcessamentoService", string.Format("InserirCteEnvioFilaProcessamento: Erro na inser��o da fila de processamento {0}", ex.Message), ex);
                throw;
            }
        }

        /// <summary>
        /// Insere na fila de processamento da Config o Cte de complemento
        /// </summary>
        /// <param name="cte">Cte a ser inserido na fila</param>
        /// <returns>Retorna o Id que o cte esta na lista</returns>
        public int InserirCteComplementoFilaProcessamento(Cte cte)
        {
            try
            {
                long cnpjEmitente = 0;
                long cnpjTomador = 0;
                // ConfiguracaoTranslogic tipoAmbiente = _configuracaoTranslogicRepository.ObterPorId(_chaveCteTipoAmbiente);
                ConfiguracaoTranslogic versaoSefaz = _configuracaoTranslogicRepository.ObterPorId(_chaveVersaoSefaz);

                cnpjEmitente = Convert.ToInt64(cte.CnpjFerrovia);

                CteEmpresas cteEmpresa = _cteEmpresasRepository.ObterPorCte(cte);
                cnpjTomador = Convert.ToInt64(Tools.TruncateRemoveSpecialChar(cteEmpresa.EmpresaTomadora.Cgc, 20));

                Nfe03Filial filial = ObterIdentificadorFilial(cte);
                int numeroCte = Convert.ToInt32(cte.Numero);
                int serieCte = Convert.ToInt32(cte.Serie);

                Cte01Lista itemProcessamento = new Cte01Lista();
                itemProcessamento.IdFilial = (int)filial.IdFilial;
                itemProcessamento.Uf = filial.Uf;
                itemProcessamento.Numero = numeroCte;
                itemProcessamento.Serie = serieCte;
                itemProcessamento.DataHoraEmissao = GetDhEmissao(cte);
                itemProcessamento.Tipo = 0; // Cte Complemento
                itemProcessamento.CnpjEmitente = cnpjEmitente;
                itemProcessamento.CnpjDestinatario = cnpjTomador;
                itemProcessamento.TipoAmbiente = ObterAmbienteSefaz(cte);
                itemProcessamento.VersaoSefaz = Convert.ToDouble(cte.Versao.CodigoVersao, CultureInfo.GetCultureInfo("en-us"));
                itemProcessamento.ModeloDocumentoFiscal = "57";
                itemProcessamento.Flag = 0;
                itemProcessamento.TipoEmissao = 1;
                itemProcessamento.TipoImpressao = 1;
                itemProcessamento.IdLote = null;
                itemProcessamento.ChaveAcesso = cte.Chave;
                itemProcessamento.DataHoraEntradaLista = DateTime.Now;

                _cte01ListaRepository.Inserir(itemProcessamento);

                // Obs: Como a config implementa uma trigger para gerar a sequence dessa tabela, 
                // foi alterado o mapeamento e � realizado um novo sql para ver qual id foi gerado
                Cte01Lista itemLista = _cte01ListaRepository.ObterListaFilaProcessamento((int)filial.IdFilial, numeroCte, serieCte, cte.Chave, 0, false);
                return itemLista.Id;
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(cte, "FilaProcessamentoService", string.Format("InserirCteComplementoFilaProcessamento: Erro na inser��o da fila de processamento (complemento) {0}", ex.Message), ex);
                throw;
            }
        }

        /// <summary>
        /// Insere na fila de processamento da Config o Cte de cancelamento
        /// </summary>
        /// <param name="cte">Cte a ser inserido na fila</param>
        /// <returns> C�digo na lista</returns>
        public int InserirCteCancelamentoFilaProcessamento(Cte cte)
        {
            try
            {
                long cnpjEmitente = 0;
                long cnpjTomador = 0;
                // ConfiguracaoTranslogic tipoAmbiente = _configuracaoTranslogicRepository.ObterPorId(_chaveCteTipoAmbiente);
                ConfiguracaoTranslogic versaoSefaz = _configuracaoTranslogicRepository.ObterPorId(_chaveVersaoSefaz);

                try
                {
                    cnpjEmitente = Convert.ToInt64(cte.CnpjFerrovia);
                }
                catch (Exception)
                {
                    cnpjEmitente = 0;
                }

                try
                {
                    CteEmpresas cteEmpresa = _cteEmpresasRepository.ObterPorCte(cte);
                    cnpjTomador = Convert.ToInt64(Tools.TruncateRemoveSpecialChar(cteEmpresa.EmpresaTomadora.Cgc, 20));
                }
                catch (Exception)
                {
                    cnpjTomador = 0;
                }

                Nfe03Filial filial = ObterIdentificadorFilial(cte);
                int numeroCte = Convert.ToInt32(cte.Numero);
                int serieCte = Convert.ToInt32(cte.Serie);

                // Implementa a trava (por causa da concorrencia dos robos) que n�o permite que um CT-e
                // seja enviado mais de uma vez para a Config. Caso o Cte j� tenha sido enviado para a config
                // retorna o id da lista j� enviada
                Cte01Lista itemLista = null;

                /*  itemLista = _cte01ListaRepository.ObterListaFilaProcessamento((int)filial.IdFilial, numeroCte, serieCte, cte.Chave, 2, true);
                  if (itemLista != null)
                  {
                      // Nesse caso um CT-e j� foi enviado anteriormente
                      return itemLista.Id;
                  }
                 */

                Cte01Lista itemProcessamento = new Cte01Lista();
                itemProcessamento.IdFilial = (int)filial.IdFilial;
                itemProcessamento.Uf = filial.Uf;
                itemProcessamento.Numero = numeroCte;
                itemProcessamento.Serie = serieCte;
                itemProcessamento.DataHoraEmissao = GetDhEmissao(cte);
                itemProcessamento.Tipo = 2; // CT-e de Anula��o de Valores;
                itemProcessamento.CnpjEmitente = cnpjEmitente;
                itemProcessamento.CnpjDestinatario = cnpjTomador;
                itemProcessamento.TipoAmbiente = ObterAmbienteSefaz(cte);
                itemProcessamento.VersaoSefaz = Convert.ToDouble(ObterVers�oCte(cte), CultureInfo.GetCultureInfo("en-us"));
                itemProcessamento.ModeloDocumentoFiscal = "57";
                itemProcessamento.Justificativa = "Teste de Justificativa Canc e Inut";
                itemProcessamento.Flag = 0;
                itemProcessamento.TipoImpressao = 1;
                itemProcessamento.TipoEmissao = 1;
                itemProcessamento.IdLote = null;
                itemProcessamento.ChaveAcesso = cte.Chave;
                itemProcessamento.DataHoraEntradaLista = DateTime.Now;

                _cte01ListaRepository.Inserir(itemProcessamento);

                // Obs: Como a config implementa uma trigger para gerar a sequence dessa tabela, 
                // foi alterado o mapeamento e � realizado um novo sql para ver qual id foi gerado
                itemLista = _cte01ListaRepository.ObterListaFilaProcessamento((int)filial.IdFilial, numeroCte, serieCte, cte.Chave, 2, false);
                return itemLista.Id;
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(cte, "FilaProcessamentoService", string.Format("InserirCteCancelamentoFilaProcessamento: Erro na inser��o da fila de processamento (cancelamento) {0}", ex.Message), ex);
                throw;
            }
        }

        /// <summary>
        /// Insere na fila de processamento da Config o Cte de processamento
        /// </summary>
        /// <param name="cte">Cte a ser inserido na fila</param>
        /// <returns> C�digo na lista</returns>
        public int InserirCteInutilizacaoFilaProcessamento(Cte cte)
        {
            try
            {
                long cnpjEmitente = 0;
                long cnpjDestinatario = 0;
                // ConfiguracaoTranslogic tipoAmbiente = _configuracaoTranslogicRepository.ObterPorId(_chaveCteTipoAmbiente);
                ConfiguracaoTranslogic versaoSefaz = _configuracaoTranslogicRepository.ObterPorId(_chaveVersaoSefaz);

                cnpjEmitente = Convert.ToInt64(cte.CnpjFerrovia);
                CteEmpresas cteEmpresa = _cteEmpresasRepository.ObterPorCte(cte);

                if (cteEmpresa != null)
                {
                    cnpjDestinatario = Convert.ToInt64(Tools.TruncateRemoveSpecialChar(cteEmpresa.EmpresaTomadora.Cgc, 20));
                }
                else if (!string.IsNullOrEmpty(cte.FluxoComercial.CnpjDestinatarioFiscal.Trim()))
                {
                    cnpjDestinatario = Convert.ToInt64(Tools.TruncateRemoveSpecialChar(cte.FluxoComercial.CnpjDestinatarioFiscal, 20));
                }
                else if (!string.IsNullOrEmpty(cte.FluxoComercial.CnpjRemetenteFiscal.Trim()))
                {
                    cnpjDestinatario = Convert.ToInt64(Tools.TruncateRemoveSpecialChar(cte.FluxoComercial.CnpjRemetenteFiscal, 20));
                }
                else
                {
                    cnpjDestinatario = Convert.ToInt64(Tools.TruncateRemoveSpecialChar(cte.FluxoComercial.EmpresaDestinataria.Cgc, 20));
                }

                Nfe03Filial filial = ObterIdentificadorFilial(cte);
                int numeroCte = Convert.ToInt32(cte.Numero);
                int serieCte = Convert.ToInt32(cte.Serie);

                Cte01Lista itemProcessamento = new Cte01Lista();
                itemProcessamento.IdFilial = (int)filial.IdFilial;
                itemProcessamento.Uf = filial.Uf;
                itemProcessamento.Numero = numeroCte;
                itemProcessamento.Serie = serieCte;
                itemProcessamento.NumeroFim = numeroCte;
                itemProcessamento.DataHoraEmissao = GetDhEmissao(cte);
                itemProcessamento.Tipo = 3; // CT-e de Inutiliza��o de Valores;
                itemProcessamento.CnpjEmitente = cnpjEmitente;
                itemProcessamento.CnpjDestinatario = cnpjDestinatario;
                itemProcessamento.TipoAmbiente = ObterAmbienteSefaz(cte);
                // itemProcessamento.VersaoSefaz = Convert.ToDouble(cte.Versao.CodigoVersao, CultureInfo.GetCultureInfo("en-us"));
                itemProcessamento.VersaoSefaz = Convert.ToDouble(ObterVers�oCte(cte), CultureInfo.GetCultureInfo("en-us"));
                itemProcessamento.ModeloDocumentoFiscal = "57";
                itemProcessamento.Justificativa = "Teste de Justificativa Canc e Inut";
                itemProcessamento.Flag = 0;
                itemProcessamento.TipoEmissao = 1;
                itemProcessamento.TipoImpressao = 1;
                itemProcessamento.IdLote = null;
                itemProcessamento.ChaveAcesso = cte.Chave;
                itemProcessamento.DataHoraEntradaLista = DateTime.Now;
                itemProcessamento.AnoInutilizacao = DateTime.Now.ToString("yy");

                _cte01ListaRepository.Inserir(itemProcessamento);

                // Obs: Como a config implementa uma trigger para gerar a sequence dessa tabela, 
                // foi alterado o mapeamento e � realizado um novo sql para ver qual id foi gerado
                Cte01Lista itemLista = _cte01ListaRepository.ObterListaFilaProcessamento((int)filial.IdFilial, numeroCte, serieCte, cte.Chave, 3, false);
                return itemLista.Id;
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(cte, "FilaProcessamentoService", string.Format("InserirCteInutilizacaoFilaProcessamento: Erro na inser��o da fila de processamento (inutiliza��o) {0}", ex.Message), ex);
                throw;
            }
        }

        /// <summary>
        /// Insere na fila de processamento da Config o Cte de processamento pelo N�mero e S�rie
        /// </summary>
        /// <param name="numero">N�mero do Cte</param>
        /// <param name="serie">S�rie do Cte</param>
        /// <param name="cnpj">CNPJ do Emitente</param>
        /// <returns> C�digo na lista</returns>
        public int InserirCteInutilizacaoFilaProcessamentoPorNumeroSerie(string numero, string serie, string cnpj)
        {
            try
            {
                long cnpjEmitente = 0;
                long cnpjDestinatario = 0;
                // ConfiguracaoTranslogic tipoAmbiente = _configuracaoTranslogicRepository.ObterPorId(_chaveCteTipoAmbiente);
                ConfiguracaoTranslogic versaoSefaz = _configuracaoTranslogicRepository.ObterPorId(_chaveVersaoSefaz);

                cnpjEmitente = Convert.ToInt64(cnpj);
                cnpjDestinatario = Convert.ToInt64(Tools.TruncateRemoveSpecialChar(cnpj, 20));

                long cnpjFerrovia;
                Int64.TryParse(cnpj, out cnpjFerrovia);

                Nfe03Filial filial = _nfe03FilialRepository.ObterFilialPorCnpj(cnpjFerrovia);
                int numeroCte = Convert.ToInt32(numero);
                int serieCte = Convert.ToInt32(serie);

                Cte01Lista itemProcessamento = new Cte01Lista();
                itemProcessamento.IdFilial = (int)filial.IdFilial;
                itemProcessamento.Uf = filial.Uf;
                itemProcessamento.Numero = numeroCte;
                itemProcessamento.Serie = serieCte;
                itemProcessamento.NumeroFim = numeroCte;
                itemProcessamento.DataHoraEmissao = DateTime.Now;
                itemProcessamento.Tipo = 3; // CT-e de Inutiliza��o de Valores;
                itemProcessamento.CnpjEmitente = cnpjEmitente;
                itemProcessamento.CnpjDestinatario = cnpjDestinatario;
                itemProcessamento.TipoAmbiente = 1;
                // itemProcessamento.VersaoSefaz = Convert.ToDouble(cte.Versao.CodigoVersao, CultureInfo.GetCultureInfo("en-us"));
                itemProcessamento.VersaoSefaz = Convert.ToDouble(2.00, CultureInfo.GetCultureInfo("en-us"));
                itemProcessamento.ModeloDocumentoFiscal = "57";
                itemProcessamento.Justificativa = "Teste de Justificativa Canc e Inut";
                itemProcessamento.Flag = 0;
                itemProcessamento.TipoEmissao = 1;
                itemProcessamento.TipoImpressao = 1;
                itemProcessamento.IdLote = null;
                itemProcessamento.ChaveAcesso = string.Empty;
                itemProcessamento.DataHoraEntradaLista = DateTime.Now;
                itemProcessamento.AnoInutilizacao = DateTime.Now.ToString("yy");

                _cte01ListaRepository.Inserir(itemProcessamento);

                // Obs: Como a config implementa uma trigger para gerar a sequence dessa tabela, 
                // foi alterado o mapeamento e � realizado um novo sql para ver qual id foi gerado
                Cte01Lista itemLista = _cte01ListaRepository.ObterListaFilaProcessamento((int)filial.IdFilial, numeroCte, serieCte, string.Empty, 3, false);
                return itemLista.Id;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Insere na fila a gera��o do PDF de impress�o - ICTE51_DEST
        /// </summary>
        /// <param name="cte">Cte para ser gerado</param>
        public void InserirGeracaArquivosCteNoServidorFtp(Cte cte)
        {
            try
            {
                Nfe03Filial filial = ObterIdentificadorFilial(cte);

                ConfiguracaoTranslogic ftpHost = _configuracaoTranslogicRepository.ObterPorId(_chaveFtpHost);
                ConfiguracaoTranslogic ftpUsuario = _configuracaoTranslogicRepository.ObterPorId(_chaveFtpUsuario);
                ConfiguracaoTranslogic ftpSenha = _configuracaoTranslogicRepository.ObterPorId(_chaveFtpSenha);
                ConfiguracaoTranslogic ftpPath = _configuracaoTranslogicRepository.ObterPorId(_chaveFtpPath);
                Cte51 cte51 = null;

                // #######################################################################
                // Registro de gera��o do arquivo de Pdf para o servidor de FTP
                // #######################################################################
                cte51 = new Cte51();
                cte51.IdFilial = (int)filial.IdFilial; // Codigo interno da filial
                cte51.Numero = Convert.ToInt32(cte.Numero); // Numero do documento CT-e
                cte51.Serie = int.Parse(cte.Serie).ToString(); // Serie do documento CT-e
                cte51.IdDest = 1;
                cte51.FtpHost = ftpHost.Valor;
                cte51.FtpUsuario = ftpUsuario.Valor;
                cte51.FtpSenha = ftpSenha.Valor;
                cte51.FtpPath = ftpPath.Valor;
                cte51.NomeAnexo = "#CHAVE_ACESSO#";

                cte51.TpEnvio = 2; // Arquivo por FTP
                cte51.TpArquivo = 0; // 1 - Arquivo PDF, 0 - XML e 2 - Ambos

                _cte51Repository.Inserir(cte51);
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(cte, "FilaProcessamentoService", string.Format("InserirGeracaArquivosCteNoServidorFtp: Erro na inser��o da fila de processamento (pdf) {0}", ex.Message), ex);
                throw;
            }
        }

        /// <summary>
        /// Insere na fila a gera��o do XML
        /// </summary>
        /// <param name="cte">Cte para ser gerado</param>
        public void InserirGeracaoArquivosCteNoServidorEmail(Cte cte)
        {
            /*
            try
            {
                Nfe03Filial filial = ObterIdentificadorFilial(cte);
                Cte51 cte51 = null;

                // #######################################################################
                // Registro de gera��o do arquivo XML para o email do cliente
                // #######################################################################
                cte51 = new Cte51();
                cte51.IdFilial = (int)filial.IdFilial; // Codigo interno da filial
                cte51.Numero = Convert.ToInt32(cte.Numero); // Numero do documento CT-e
                cte51.Serie = int.Parse(cte.Serie).ToString(); // Serie do documento CT-e
                cte51.IdDest = 1;
                cte51.NomeAnexo = "#CHAVE_ACESSO#";
                string listaEmail = ObterEmailEnviarXml(cte);
                if (!string.IsNullOrEmpty(listaEmail))
                {
                    cte51.EmailPara = listaEmail;
                }

                cte51.TpEnvio = 1; // 1 - Email e 2 - FTP
                cte51.TpArquivo = 2; // 1 - Arquivo PDF, 0 - Arquivo XML e 2 - Ambos

                _cte51Repository.Inserir(cte51);
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(cte, "FilaProcessamentoService", string.Format("InserirGeracaoPdfFilaProcessamento: Erro na inser��o da fila de processamento (pdf) {0}", ex.Message), ex);
                throw;
            }
             */
        }

        /// <summary>
        /// Insere na fila a gera��o do XML
        /// </summary>
        /// <param name="cte">Cte para ser gerado</param>
        public void InserirGeracaoArquivosCteNoServidorEmailCancelamento(Cte cte)
        {
            try
            {
                Nfe03Filial filial = ObterIdentificadorFilial(cte);
                Cte51 cte51 = null;

                // #######################################################################
                // Registro de gera��o do arquivo XML para o email do cliente
                // #######################################################################
                cte51 = new Cte51();
                cte51.IdFilial = (int)filial.IdFilial; // Codigo interno da filial
                cte51.Numero = Convert.ToInt32(cte.Numero); // Numero do documento CT-e
                cte51.Serie = int.Parse(cte.Serie).ToString(); // Serie do documento CT-e
                cte51.IdDest = 1;
                cte51.NomeAnexo = "#CHAVE_ACESSO#";
                string listaEmail = ObterEmailEnviarXml(cte);
                if (!string.IsNullOrEmpty(listaEmail))
                {
                    cte51.EmailPara = listaEmail;
                }

                cte51.TpEnvio = 1; // 1 - Email e 2 - FTP
                cte51.TpArquivo = 2; // 1 - Arquivo PDF, 0 - Arquivo XML e 2 - Ambos

                _cte51Repository.Inserir(cte51);
            }
            catch (Exception ex)
            {
                _cteLogService.InserirLogErro(cte, "FilaProcessamentoService", string.Format("InserirGeracaoPdfFilaProcessamento: Erro na inser��o da fila de processamento (pdf) {0}", ex.Message), ex);
                throw;
            }
        }

        /// <summary>
        /// Insere na gera��o do arquivo PDF por impress�o
        /// </summary>
        /// <param name="cte">Cte para ser gerado o arquivo PDF</param>
        public void InserirGeracaoPdfPorImpressao(Cte cte)
        {
            Nfe03Filial filial = ObterIdentificadorFilial(cte);
            int numeroCte = Convert.ToInt32(cte.Numero);
            // int serieCte = Convert.ToInt32(cte.Serie);

            Cte50 cte50 = new Cte50();
            cte50.IdFilial = (int)filial.IdFilial;
            cte50.Numero = numeroCte;
            cte50.Serie = cte.Serie;
            cte50.IdImp = 0;
            cte50.LocalPrt = "LOCAL_PADRAO";
            cte50.IdentPrt = "TESTE";
            cte50.NrCop = 1;
            cte50.ImpFrenteVerso = 0;
            cte50.Copia = 1;
            cte50.BandejaEntrada = "1";

            _cte50Repository.Inserir(cte50);
        }

        /// <summary>
        /// Obtem o cte de retorne do processamento
        /// </summary>
        /// <param name="idLista">Identificador da lista que o cte esta</param>
        /// <returns>Retorna o objeto de retorno do Cte</returns>
        public Vw209ConsultaCte ObterRetornoCteProcessadoPorIdLista(int idLista)
        {
            return _consultaCteRepository.ObterRetornoPorIdLista(idLista);
        }

        /// <summary>
        /// Obtem o cte de retorne do processamento
        /// </summary>
        /// <param name="idFilial"> Numero da filial</param>
        /// <param name="numero"> Numero do cte</param>
        /// <param name="serie"> Serie do cte</param>
        /// <returns>Retorna o objeto de retorno do Cte</returns>
        public Vw209ConsultaCte ObterRetornoCteProcessadoPorSerieNumero(int idFilial, int numero, int serie)
        {
            return _consultaCteRepository.ObterRetornoPorSerieNumeroUnidade(idFilial, numero, serie);
        }

        /// <summary>
        /// Obtem o cte de retorne do processamento
        /// </summary>
        /// <param name="protocolo">Protocolo da sefaz</param>
        /// <returns>Retorna o objeto de retorno do Cte</returns>
        public Vw209ConsultaCte ObterRetornoCteProcessadoPorProtocolo(long protocolo)
        {
            return _consultaCteRepository.ObterRetornoPorProtocolo(protocolo);
        }

        /// <summary>
        /// Obt�m dados do item da fila de processamento pelo retorno da config
        /// </summary>
        /// <param name="consultaCte">Dados de retorno da config</param>
        /// <returns>Retorna o item da fila de processamento</returns>
        public Cte01Lista ObterItemFilaProcessamentoPorConsultaRetorno(Vw209ConsultaCte consultaCte)
        {
            return _cte01ListaRepository.ObterPorId(consultaCte.IdLista);
        }

        /// <summary>
        /// Obtem o cte de retorne do processamento
        /// </summary>
        /// <param name="chaveCte">chave da sefaz</param>
        /// <returns>Retorna o objeto de retorno do Cte</returns>
        public Vw209ConsultaCte ObterRetornoCteProcessadoPorChave(string chaveCte)
        {
            return _consultaCteRepository.ObterRetornoPorChave(chaveCte);
        }

        /// <summary>
        /// Obtem o cte de retorne do processamento
        /// </summary>
        /// <param name="chaveCte">chave da sefaz</param>
        /// <returns>Retorna o objeto de retorno do Cte</returns>
        public Vw209ConsultaCte ObterRetornoCteAutorizadoPorChave(string chaveCte)
        {
            return _consultaCteRepository.ObterCteJaAutorizado(chaveCte);
        }

        /// <summary>
        /// Obt�m o retorno do processamento 
        /// </summary>
        /// <param name="numero">N�mero do Cte</param>
        /// <param name="idFilial">Filial do Cte</param>
        /// <param name="serie">Serie do Cte</param>
        /// <returns>Retorna o CteProcessado</returns>
        public Vw209ConsultaCte ObterCteJaAutorizadoComDiferencaNaChaveDeAcesso(int numero, int idFilial, int serie)
        {
            return _consultaCteRepository.ObterCteJaAutorizadoComDiferencaNaChaveDeAcesso(numero, idFilial, serie);
        }

        /// <summary>
        /// Obt�m detalhes do erro na tabela nfe20_erro_log
        /// </summary>
        /// <param name="consultaCte">Dados de retorno da config</param>
        /// <returns>Retorna o .log de erro</returns>
        public Nfe20ErroLog ObterLogErroPorConsultaRetorno(Vw209ConsultaCte consultaCte)
        {
            return _nfe20ErroLogRepository.ObterErro(consultaCte.IdFilial, consultaCte.Numero, consultaCte.Serie);
        }

        /// <summary>
        /// Obt�m o codigo interno da filial
        /// </summary>
        /// <param name="cte">Objeto Cte</param>
        /// <returns>Retorna o Identificador da filial</returns>
        public Nfe03Filial ObterIdentificadorFilial(Cte cte)
        {
            long cnpj;
            Int64.TryParse(cte.CnpjFerrovia, out cnpj);

            Nfe03Filial filial = _nfe03FilialRepository.ObterFilialPorCnpj(cnpj);

            return filial;
        }

        private int ObterCodigoUfOrigem(Cte cte)
        {
            int idUfOrigem = 0;
            if (cte.FluxoComercial != null)
            {
                EstacaoMae estacaoMaeOrigem = cte.FluxoComercial.Origem;
                UfIbge objUfIbge = _unidadeFederativaIbgeRepository.ObterPorSigla(estacaoMaeOrigem.Municipio.Estado.Sigla);
                idUfOrigem = objUfIbge.Id;
            }

            return idUfOrigem;
        }

        private IEmpresa ObterDadosEmpresaRemetente(Cte cte)
        {
            IEmpresa empresa = _serieDespachoUfRepository.ObterEmpresaPorCnpjDoGrupo(cte.CnpjFerrovia);
            return empresa;
        }

        /// <summary>
        /// Obtem o Ambiente da Sefaz
        /// (1) - Produ��o
        /// (2) - Homologa��o
        /// </summary>
        /// <param name="cte">Objeto Cte com os dados</param>
        /// <returns>Retorna o ambiente da Sefaz</returns>
        private int ObterAmbienteSefaz(Cte cte)
        {
            EstacaoMae origem = cte.FluxoComercial.OrigemIntercambio ?? cte.FluxoComercial.Origem;

            CteSerieNumeroEmpresaUf serieEmpresaUf = _cteSerieNumeroEmpresaUfRepository.ObterPorEmpresaUf(origem.EmpresaConcessionaria, origem.Municipio.Estado.Sigla);

            if (serieEmpresaUf == null)
            {
                throw new Exception("N�o foi poss�vel recuperar o tipo de ambiente da Sefaz");
            }

            return serieEmpresaUf.ProducaoSefaz ? 1 : 2;
        }

        /// <summary>
        /// Obtem o Ambiente da Sefaz
        /// (1) - Produ��o
        /// (2) - Homologa��o
        /// </summary>
        /// <param name="cte">Objeto Cte com os dados</param>
        /// <returns>Retorna o ambiente da Sefaz</returns>
        private string ObterVers�oCte(Cte cte)
        {
            EstacaoMae origem = cte.FluxoComercial.OrigemIntercambio ?? cte.FluxoComercial.Origem;

            CteSerieNumeroEmpresaUf serieEmpresaUf = _cteSerieNumeroEmpresaUfRepository.ObterPorEmpresaUf(origem.EmpresaConcessionaria, origem.Municipio.Estado.Sigla);

            if (serieEmpresaUf == null)
            {
                throw new Exception("N�o foi poss�vel recuperar a vers�o do Cte");
            }

            return serieEmpresaUf.Versao.CodigoVersao;
        }

        private string ObterEmailEnviarXml(Cte cte)
        {
            var retorno = string.Empty;
            var listaEnvio = _cteEnvioXmlRepository.ObterPorCte(cte);

            if (listaEnvio == null || listaEnvio.Count == 0)
            {
                return retorno;
            }

            var emails = listaEnvio.Where(e => !string.IsNullOrWhiteSpace(e.EnvioPara)).Select(e => e.EnvioPara).ToArray();

            foreach (var mail in emails)
            {
                // -> N�o estoura os 200 varchar do banco da config
                if (retorno.Length + mail.Length + 1 > 200)
                {
                    break;
                }

                retorno += mail + ";";
            }

            if (!string.IsNullOrEmpty(retorno))
            {
                // -> Remove a ultima virgula
                retorno = retorno.Remove(retorno.Length - 1);
            }

            return retorno;
        }

        private DateTime GetDhEmissao(Cte cte)
        {
            var restricoes = new[] { "MT", "MS" };

            if (restricoes.Contains(cte.SiglaUfFerrovia))
            {
                return DateTime.Now.AddHours(-1);
            }

            return DateTime.Now;
        }
    }
}
