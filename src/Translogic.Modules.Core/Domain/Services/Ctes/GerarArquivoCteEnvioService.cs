namespace Translogic.Modules.Core.Domain.Services.Ctes
{
	using System;
	using System.Collections;
	using System.Collections.Generic;
	using System.Globalization;
	using System.IO;
	using System.Linq;
	using System.Text;
	using Model.Codificador;
	using Model.Codificador.Repositories;
	using Model.Diversos.Bacen;
	using Model.Diversos.Bacen.Repositories;
	using Model.Diversos.Ibge;
	using Model.Diversos.Ibge.Repositories;
	using Model.Estrutura;
	using Model.FluxosComerciais;
	using Model.FluxosComerciais.ConhecimentoTransporteEletronico;
	using Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;
	using Model.FluxosComerciais.Pedidos.Despachos;
	using Model.FluxosComerciais.Pedidos.Despachos.Repositories;
	using Model.Trem.Veiculo.Conteiner;
	using Model.Trem.Veiculo.Conteiner.Repositories;
	using Model.Trem.Veiculo.Vagao;
	using Model.Via;
	using Util;

	/// <summary>
	/// Classe para gera��o dos arquivos do CT-e de envio
	/// </summary>
	public class GerarArquivoCteEnvioService
	{
		private readonly string SEPARADOR_COLUNA = ";";
		private readonly string SEPARADOR_LINHA = "\n";
		private readonly ICteRepository _cteRepository;
		private readonly IItemDespachoRepository _itemDespachoRepository;
		private readonly IUfIbgeRepository _unidadeFederativaIbgeRepository;
		private readonly IPaisBacenRepository _paisBacenRepository;
		private readonly IComposicaoFreteContratoRepository _composicaoFreteContratoRepository;
		private readonly IVagaoConteinerVigenteRepository _vagaoConteinerVigenteRepository;
		private readonly ICteDetalheRepository _cteDetalheRepository;
		private readonly IConfiguracaoTranslogicRepository _configuracaoTranslogicRepository;
		private readonly IComposicaoFreteCteRepository _composicaoFreteCteRepository;
		private readonly ICteArvoreRepository _cteArvoreRepository;

		private readonly string _chaveCteFormaEmissao = "CTE_FORMA_EMISSAO";
		private readonly string _chaveCteTipoAmbiente = "CTE_TIPO_AMBIENTE";
		private readonly string _chaveCteVersaoProcesso = "CTE_VERSAO_PROCESSO";

		private Cte _cte;
		private Cte _cteRaiz;
		private ItemDespacho _itemDespacho;
		private FluxoComercial _fluxoComercial;
		private DetalheCarregamento _detalheCarregamento;
		private Carregamento _carregamento;
		private ContratoHistorico _contratoHistorico;
		// private Contrato _contrato;
		private Vagao _vagao;
		private IEmpresa _empresaPagadora;
		private IEmpresa _empresaRemetente;
		private IEmpresa _empresaDestinataria;
		private IList<CteDetalhe> _listaCteDetalhe;
		private CteLogService _cteLogService;

		/// <summary>
		///  Initializes a new instance of the <see cref="GerarArquivoCteEnvioService"/> class.
		/// </summary>
		/// <param name="cteRepository">Reposit�rio do Cte injetado</param>
		/// <param name="itemDespachoRepository">Reposit�rio do ItemDespacho injetado</param>
		/// <param name="unidadeFederativaIbgeRepository">Reposit�rio do UF do IBGE injetado</param>
		/// <param name="paisBacenRepository">Reposit�rio do codigo do pais no BACEN injetado</param>
		/// <param name="composicaoFreteContratoRepository">Reposit�rio da composicao frete contrato injetado</param>
		/// <param name="vagaoConteinerVigenteRepository">Reposit�rio do vag�o conteiner injetado</param>
		/// <param name="cteDetalheRepository">Reposit�rio detalhe do cte injetado</param>
		/// <param name="configuracaoTranslogicRepository">Reposit�rio da configura��o do translogic injetado</param>
		/// <param name="cteLogService">Servi�o de log do cte injetado</param>
		/// <param name="composicaoFreteCteRepository">Reposit�rio de composi��o frete cte injetado</param>
		/// <param name="cteArvoreRepository">Reposit�rio de cte arvore injetado</param>
		public GerarArquivoCteEnvioService(ICteRepository cteRepository, IItemDespachoRepository itemDespachoRepository, IUfIbgeRepository unidadeFederativaIbgeRepository, IPaisBacenRepository paisBacenRepository, IComposicaoFreteContratoRepository composicaoFreteContratoRepository, IVagaoConteinerVigenteRepository vagaoConteinerVigenteRepository, ICteDetalheRepository cteDetalheRepository, IConfiguracaoTranslogicRepository configuracaoTranslogicRepository, CteLogService cteLogService, IComposicaoFreteCteRepository composicaoFreteCteRepository, ICteArvoreRepository cteArvoreRepository)
		{
			_cteRepository = cteRepository;
			_cteArvoreRepository = cteArvoreRepository;
			_composicaoFreteCteRepository = composicaoFreteCteRepository;
			_cteLogService = cteLogService;
			_configuracaoTranslogicRepository = configuracaoTranslogicRepository;
			_cteDetalheRepository = cteDetalheRepository;
			_vagaoConteinerVigenteRepository = vagaoConteinerVigenteRepository;
			_composicaoFreteContratoRepository = composicaoFreteContratoRepository;
			_paisBacenRepository = paisBacenRepository;
			_itemDespachoRepository = itemDespachoRepository;
			_unidadeFederativaIbgeRepository = unidadeFederativaIbgeRepository;
		}

		/// <summary>
		/// Executa a gera��o do arquivo de envio
		/// </summary>
		/// <param name="cte">Cte que ser� gerado o arquivo</param>
		/// <returns>Retorna o StringBuilder com o arquivo gerado</returns>
		public StringBuilder Executar(Cte cte)
		{
			_cte = cte;
			StringBuilder builder = new StringBuilder();
			try
			{
				CarregarDados();

				builder.Append(GerarRegistro00000());
				builder.Append(GerarRegistro00001());
				builder.Append(GerarRegistro10000());
				builder.Append(GerarRegistro11000());
				builder.Append(GerarRegistro11100());
				builder.Append(GerarRegistro11110());
				builder.Append(GerarRegistro11120());
				builder.Append(GerarRegistro11121());
				builder.Append(GerarRegistro11300());
				builder.Append(GerarRegistro12000());
				builder.Append(GerarRegistro12010());
				builder.Append(GerarRegistro12100());
				builder.Append(GerarRegistro12110());

				// Registro de nota fiscal
				switch (ObterTipoNotaFiscal())
				{
					case 0: // NF Manual
						builder.Append(GerarRegistro12120());
						builder.Append(GerarRegistro12121());
						break;
					case 1: // NFe
						builder.Append(GerarRegistro12130());
						break;
					case 2: // NF outros (
						builder.Append(GerarRegistro12140());
						break;
				}

				// F - Dados do expedidor da carga 
				builder.Append(GerarRegistro12200());
				builder.Append(GerarRegistro12210());

				// G - Dados do recebedor da carga
				builder.Append(GerarRegistro12300());
				builder.Append(GerarRegistro12310());

				// H - Dados do destinat�rio
				builder.Append(GerarRegistro12400());
				builder.Append(GerarRegistro12410());
				builder.Append(GerarRegistro12420());

				// I - Valores da presta��o dos servi��es
				builder.Append(GerarRegistro13000());

				// J - Informa��es relativas aos Impostos
				builder.Append(GerarRegistro14000());
				if (_contratoHistorico.Cst == "00")
				{
					builder.Append(GerarRegistro14100());
				}
				else if ((_contratoHistorico.Cst == "40") || (_contratoHistorico.Cst == "41") || (_contratoHistorico.Cst == "51"))
				{
					builder.Append(GerarRegistro14145(_contratoHistorico.Cst));
				}
				else if (_contratoHistorico.Cst == "80")
				{
					builder.Append(GerarRegistro14180());
				}
				else if (_contratoHistorico.Cst == "81")
				{
					builder.Append(GerarRegistro14181());
				}
				else if (_contratoHistorico.Cst == "90")
				{
					builder.Append(GerarRegistro14190());
				}

				// K - Informa��es de CT-e normal ou CT-e emitido em hip�tese de anula��o de d�bito
				builder.Append(GerarRegistro15000());
				builder.Append(GerarRegistro15100());
				builder.Append(GerarRegistro15110());

				// R - Dados espec�ficos do Modal FERROVI�RIO
				builder.Append(GerarRegistro19000());
				// Todo: verificar quando enviar as informa��es do registro 19100
				builder.Append(GerarRegistro19100());
				builder.Append(GerarRegistro19110());
				builder.Append(GerarRegistro19200());
				builder.Append(GerarRegistro19210());
				// Todo: verificar se ser� enviado essa linha 
				builder.Append(GerarRegistro19211());
				// Todo: verificar quando � um cte de conteiner
				/*
				builder.Append(GerarRegistro19212());
				builder.Append(GerarRegistro19300());
				builder.Append(GerarRegistro19310());
				builder.Append(GerarRegistro19320());
				 */
				// U - CT-e Complementados
				// Gerado a partir do arquivo de gera��o de complemento

				// V - CT-e de anula��o de valores
				// builder.Append(GerarRegistro25000());

				/* Verificar se ser�o utilizado essas linhas 
				builder.Append(GerarRegistro11310());
				builder.Append(GerarRegistro11311());
				builder.Append(GerarRegistro11320());
				builder.Append(GerarRegistro11321());
				builder.Append(GerarRegistro11322());
				builder.Append(GerarRegistro11323());
				builder.Append(GerarRegistro11327());
				builder.Append(GerarRegistro11328());
				builder.Append(GerarRegistro11329());
				builder.Append(GerarRegistro11340());
				builder.Append(GerarRegistro11350());
			
				builder.Append(GerarRegistro13110());
				builder.Append(GerarRegistro14120());
				builder.Append(GerarRegistro14300());
				builder.Append(GerarRegistro15200());
				builder.Append(GerarRegistro15210());
				builder.Append(GerarRegistro15300());
				builder.Append(GerarRegistro15310());
				builder.Append(GerarRegistro15320());
				builder.Append(GerarRegistro15321());
				builder.Append(GerarRegistro15322());
				builder.Append(GerarRegistro15400());
				builder.Append(GerarRegistro16000());
				builder.Append(GerarRegistro16100());
				builder.Append(GerarRegistro16200());
				builder.Append(GerarRegistro16210());
				builder.Append(GerarRegistro16300());
				builder.Append(GerarRegistro16310());
				builder.Append(GerarRegistro16400());
				builder.Append(GerarRegistro16410());
				builder.Append(GerarRegistro16500());
				builder.Append(GerarRegistro16600());
				builder.Append(GerarRegistro17000());
				builder.Append(GerarRegistro17100());
				builder.Append(GerarRegistro18000());
				builder.Append(GerarRegistro18100());
				builder.Append(GerarRegistro20000());
				builder.Append(GerarRegistro21000());
				builder.Append(GerarRegistro22000());
				builder.Append(GerarRegistro24120()); 
				builder.Append(GerarRegistro26000());
				builder.Append(GerarRegistro26100());
				builder.Append(GerarRegistro26200());
				builder.Append(GerarRegistro26300());
				builder.Append(GerarRegistro50000());
				builder.Append(GerarRegistro50100());
				 */

				// Insere os dados de Composicao Frete Cte
				InserirComposicaoFreteCte();
			}
			catch (Exception exception)
			{
				// Limpa o string builder
				builder.Remove(0, builder.Length);
				_cteLogService.InserirLogErro(_cte, "GerarArquivoCteEnvioServico", string.Format("Executar: {0}", exception.Message));
			}

			return builder;
		}

		private string GerarRegistro10000()
		{
			StringBuilder buffer = new StringBuilder();

			try
			{
				buffer.Append("10000");
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(SEPARADOR_LINHA);
			}
			catch (Exception ex)
			{
				_cteLogService.InserirLogErro(_cte, "GerarArquivoCteEnvioServico", string.Format("GerarRegistro10000: {0}", ex.Message), ex);
				throw;
			}

			return buffer.ToString();
		}

		private void CarregarDados()
		{
			try
			{
				// Carregar os dados do cte raiz
				CteArvore cteArvore = _cteArvoreRepository.ObterCteArvorePorCteFilho(_cte);
				_cteRaiz = cteArvore.CteRaiz;

				// Carregar os dados do Item de Despacho
				_itemDespacho = _itemDespachoRepository.ObterItemDespachoPorDespacho(_cte.Despacho);

				// Carregar as informa��es do detalhe carregamento
				_detalheCarregamento = _itemDespacho.DetalheCarregamento;

				// Carregar as informa��es de carregamento
				_carregamento = _detalheCarregamento.Carregamento;

				// Informa��es do fluxo comercial
				_fluxoComercial = _cte.FluxoComercial;

				// Contrato hist�rico
				_contratoHistorico = _cte.ContratoHistorico;

				// Dados de contrato
				// _contrato = _contratoHistorico.Contrato;

				// Dados da empresa pagadora
				_empresaPagadora = _contratoHistorico.EmpresaPagadora;

				// Dados da empresa remetente
				_empresaRemetente = _contratoHistorico.EmpresaRemetente;

				// Dados da empresa destinataria
				_empresaDestinataria = _contratoHistorico.EmpresaDestinataria;

				// Dados do Vag�o
				_vagao = _cte.Vagao;

				// Lista de detalhes do Cte
				_listaCteDetalhe = _cteDetalheRepository.ObterPorCte(_cte);
			}
			catch (Exception ex)
			{
				_cteLogService.InserirLogErro(_cte, "GerarArquivoCteEnvioServico", string.Format("CarregarDados: {0}", ex.Message), ex);
				throw;
			}
		}

		private string GerarRegistro50100()
		{
			return string.Empty;
		}

		private string GerarRegistro50000()
		{
			return string.Empty;
		}

		private string GerarRegistro26300()
		{
			return string.Empty;
		}

		private string GerarRegistro26200()
		{
			return string.Empty;
		}

		private string GerarRegistro26100()
		{
			return string.Empty;
		}

		private string GerarRegistro26000()
		{
			return string.Empty;
		}

		private string GerarRegistro25000()
		{
			string chave = _cte.Chave;

			StringBuilder buffer = new StringBuilder();
			try
			{
				buffer.Append("25000");
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(chave);
				buffer.Append(SEPARADOR_COLUNA);
				// Data de emiss�o
				buffer.Append(SEPARADOR_LINHA);
			}
			catch (Exception ex)
			{
				_cteLogService.InserirLogErro(_cte, "GerarArquivoCteEnvioServico", string.Format("CarGerarRegistro25000: {0}", ex.Message), ex);
				throw;
			}

			return buffer.ToString();
		}

		private string GerarRegistro24120()
		{
			return string.Empty;
		}

		private string GerarRegistro22000()
		{
			return string.Empty;
		}

		private string GerarRegistro21000()
		{
			throw new NotImplementedException();
		}

		private string GerarRegistro20000()
		{
			return string.Empty;
		}

		private string GerarRegistro19310()
		{
			StringBuilder buffer = new StringBuilder();

			try
			{
				// Todo: Verificar como pegar o lacre do vag�o
				string lacreVagao = string.Empty;

				buffer.Append("192310");
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(lacreVagao, 20));
				buffer.Append(SEPARADOR_LINHA);
			}
			catch (Exception ex)
			{
				_cteLogService.InserirLogErro(_cte, "GerarArquivoCteEnvioServico", string.Format("GerarRegistro19310: {0}", ex.Message), ex);
				throw;
			}

			return buffer.ToString();
		}

		private string GerarRegistro19320()
		{
			StringBuilder buffer = new StringBuilder();

			try
			{
				// Todo: Verificar quando que � um CTE de conteiner
				string codigoVagao = _vagao.Codigo;
				VagaoConteinerVigente vagaoConteinerVigente = _vagaoConteinerVigenteRepository.ObterPorCodigoVagao(codigoVagao);
				Conteiner conteiner = vagaoConteinerVigente.Conteiner ?? new Conteiner();
				string numeroConteiner = conteiner.Numero ?? string.Empty;

				buffer.Append("19320");
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(numeroConteiner, 20));
				buffer.Append(SEPARADOR_COLUNA);
				// Data prevista de entrega
				buffer.Append(SEPARADOR_LINHA);
			}
			catch (Exception ex)
			{
				_cteLogService.InserirLogErro(_cte, "GerarArquivoCteEnvioServico", string.Format("GerarRegistro19320: {0}", ex.Message), ex);
				throw;
			}

			return buffer.ToString();
		}

		private string GerarRegistro19300()
		{
			StringBuilder buffer = new StringBuilder();
			try
			{
				string codigoVagao = _vagao.Codigo;
				string tipoVagao = _vagao.Serie.Tipo.Codigo;
				double pesoTotal = _itemDespacho.PesoTotalVagao ?? 0.0;
				double toneladaUtil = _cte.PesoVagao ?? 0.0;

				buffer.Append("19300");
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(codigoVagao, 8));
				buffer.Append(SEPARADOR_COLUNA);
				// Capacidade do vag�o
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(tipoVagao, 3));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(pesoTotal.ToString("0.00", CultureInfo.GetCultureInfo("en-US").NumberFormat));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(toneladaUtil.ToString("0.00", CultureInfo.GetCultureInfo("en-US").NumberFormat));
				buffer.Append(SEPARADOR_LINHA);
			}
			catch (Exception ex)
			{
				_cteLogService.InserirLogErro(_cte, "GerarArquivoCteEnvioServico", string.Format("GerarRegistro19300: {0}", ex.Message), ex);
				throw;
			}

			return buffer.ToString();
		}

		private string GerarRegistro19212()
		{
			StringBuilder buffer = new StringBuilder();
			try
			{
				// Todo: Verificar quando que � um CTE de conteiner
				string codigoVagao = _vagao.Codigo;
				VagaoConteinerVigente vagaoConteinerVigente = _vagaoConteinerVigenteRepository.ObterPorCodigoVagao(codigoVagao);
				Conteiner conteiner = vagaoConteinerVigente.Conteiner ?? new Conteiner();
				string numeroConteiner = conteiner.Numero ?? string.Empty;

				buffer.Append("19212");
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(numeroConteiner, 20));
				buffer.Append(SEPARADOR_COLUNA);
				// Data prevista de entrega
				buffer.Append(SEPARADOR_LINHA);
			}
			catch (Exception ex)
			{
				_cteLogService.InserirLogErro(_cte, "GerarArquivoCteEnvioServico", string.Format("GerarRegistro19212: {0}", ex.Message), ex);
				throw;
			}

			return buffer.ToString();
		}

		private string GerarRegistro19211()
		{
			StringBuilder buffer = new StringBuilder();
			try
			{
				// Todo: Verificar como pegar o lacre do vag�o
				string lacreVagao = string.Empty;

				buffer.Append("19211");
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(lacreVagao, 20));
				buffer.Append(SEPARADOR_LINHA);
			}
			catch (Exception ex)
			{
				_cteLogService.InserirLogErro(_cte, "GerarArquivoCteEnvioServico", string.Format("GerarRegistro19211: {0}", ex.Message), ex);
				throw;
			}

			return buffer.ToString();
		}

		private string GerarRegistro19210()
		{
			StringBuilder buffer = new StringBuilder();
			try
			{
				string codigoVagao = _vagao.Codigo;
				string tipoVagao = _vagao.Serie.Tipo.Codigo;
				double pesoTotal = _itemDespacho.PesoTotalVagao ?? 0.0;
				double toneladaUtil = _cte.PesoVagao ?? 0.0;

				buffer.Append("19210");
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(codigoVagao, 8));
				buffer.Append(SEPARADOR_COLUNA);
				// Capacidade do vag�o
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(tipoVagao, 3));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(pesoTotal.ToString("0.00", CultureInfo.GetCultureInfo("en-US").NumberFormat));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(toneladaUtil.ToString("0.00", CultureInfo.GetCultureInfo("en-US").NumberFormat));
				buffer.Append(SEPARADOR_LINHA);
			}
			catch (Exception ex)
			{
				_cteLogService.InserirLogErro(_cte, "GerarArquivoCteEnvioServico", string.Format("GerarRegistro19210: {0}", ex.Message), ex);
				throw;
			}

			return buffer.ToString();
		}

		private string GerarRegistro19200()
		{
			StringBuilder buffer = new StringBuilder();

			try
			{
				double baseCalculoIcms = 0.0;
				DespachoTranslogic despacho = _cte.Despacho;
				string numeroContrato = _contratoHistorico.NumeroContrato;
				int numeroSerieDespacho = despacho.NumeroSerieDespacho ?? 0;
				string serieDespacho = numeroSerieDespacho.ToString();
				int numDespacho = despacho.NumeroDespacho ?? 0;
				string numeroDespacho = numDespacho.ToString();
				string dataDespacho = despacho.DataDespacho.ToString("yyyy-MM-dd");
				int numVagoesCarregado = _carregamento.NumeroVagoesCarregados ?? 0;
				string numeroVagoesCarregados = numVagoesCarregado.ToString();
				double somaPeso = _listaCteDetalhe.Sum(g => g.PesoNotaFiscal);
				int tipoTributacao = 0;
				double valorFrete = 0.0;
				double valorSevicosAcessorios = 0.0;
				double valorTotalServicos = 0.0;

				baseCalculoIcms = CalcularBaseCalculoIcms();
				valorFrete = baseCalculoIcms * somaPeso;
				valorTotalServicos = valorFrete + valorSevicosAcessorios;

				buffer.Append("19200");
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(serieDespacho, 3));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(numeroDespacho, 20));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(dataDespacho);
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(numeroVagoesCarregados);
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(somaPeso.ToString("0.00", CultureInfo.GetCultureInfo("en-US").NumberFormat));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(baseCalculoIcms.ToString("0.00", CultureInfo.GetCultureInfo("en-US").NumberFormat));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(valorFrete.ToString("0.00", CultureInfo.GetCultureInfo("en-US").NumberFormat));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(valorSevicosAcessorios.ToString("0.00", CultureInfo.GetCultureInfo("en-US").NumberFormat));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(valorTotalServicos.ToString("0.00", CultureInfo.GetCultureInfo("en-US").NumberFormat));
				buffer.Append(SEPARADOR_COLUNA);
				// Identifica��o do Trem
				buffer.Append(SEPARADOR_LINHA);
			}
			catch (Exception ex)
			{
				_cteLogService.InserirLogErro(_cte, "GerarArquivoCteEnvioServico", string.Format("GerarRegistro19200: {0}", ex.Message), ex);
				throw;
			}

			return buffer.ToString();
		}

		private string GerarRegistro19110()
		{
			StringBuilder buffer = new StringBuilder();

			try
			{
				string endereco = String.Empty;
				string numeroEndereco = string.Empty;
				string complementoEndereco = string.Empty;
				string bairro = string.Empty;
				// Todo: verificar qual empresa nesse caso, n�o vai ser _empresaPagadora -> outra ferrovia
				string codigoCidadeIbge = _empresaPagadora.CidadeIbge.CodigoIbge.ToString() ?? string.Empty;
				string siglaEstado = _empresaPagadora.CidadeIbge.SiglaEstado.ToString() ?? string.Empty;
				int index = _empresaPagadora.Cep.IndexOf('-');
				string cepCidade;
				if (index > -1)
				{
					cepCidade = _empresaPagadora.Cep.Remove(index, 1);
				}
				else
				{
					cepCidade = _empresaPagadora.Cep;
				}

				buffer.Append("19110");
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(endereco, 255));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(numeroEndereco, 60));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(complementoEndereco, 60));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(bairro, 60));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(codigoCidadeIbge.PadLeft(7, '0'));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(_empresaPagadora.CidadeIbge.Descricao, 60));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(cepCidade);
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(siglaEstado);
				buffer.Append(SEPARADOR_LINHA);
			}
			catch (Exception ex)
			{
				_cteLogService.InserirLogErro(_cte, "GerarArquivoCteEnvioServico", string.Format("GerarRegistro19110: {0}", ex.Message), ex);
				throw;
			}

			return buffer.ToString();
		}

		private string GerarRegistro19100()
		{
			StringBuilder buffer = new StringBuilder();

			try
			{
				string cnpj = String.Empty;
				string codigoInterno = String.Empty;
				string inscricaoEstadual = string.Empty;
				string nomeFerrovia = string.Empty;

				buffer.Append("19100");
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(buffer.Append(cnpj.PadLeft(14, '0')));
				buffer.Append(SEPARADOR_COLUNA);
				// Codigo interno
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(inscricaoEstadual, 14));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(nomeFerrovia);
				buffer.Append(SEPARADOR_LINHA);
			}
			catch (Exception ex)
			{
				_cteLogService.InserirLogErro(_cte, "GerarArquivoCteEnvioServico", string.Format("GerarRegistro19100: {0}", ex.Message), ex);
				throw;
			}

			return buffer.ToString();
		}

		private string GerarRegistro19000()
		{
			StringBuilder buffer = new StringBuilder();
			try
			{
				double baseCalculoIcms = CalcularBaseCalculoIcms();
				double aliquota = _contratoHistorico.PercentualAliquotaIcms ?? 0.0;
				double valorTotalIcms = baseCalculoIcms * aliquota;

				string codigoFluxo = _fluxoComercial.Codigo;

				buffer.Append("19000");
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append("0"); // Todo: Tipo proprio: verificar como pegar esse campo
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(codigoFluxo, 10));
				buffer.Append(SEPARADOR_COLUNA);
				// Identifica��o do Trem
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(baseCalculoIcms.ToString("0.00", CultureInfo.GetCultureInfo("en-US").NumberFormat));
				buffer.Append(SEPARADOR_LINHA);
			}
			catch (Exception ex)
			{
				_cteLogService.InserirLogErro(_cte, "GerarArquivoCteEnvioServico", string.Format("GerarRegistro19000: {0}", ex.Message), ex);
				throw;
			}

			return buffer.ToString();
		}

		private string GerarRegistro18100()
		{
			return string.Empty;
		}

		private string GerarRegistro18000()
		{
			return string.Empty;
		}

		private string GerarRegistro17100()
		{
			return string.Empty;
		}

		private string GerarRegistro17000()
		{
			return string.Empty;
		}

		private string GerarRegistro16600()
		{
			return string.Empty;
		}

		private string GerarRegistro16500()
		{
			return string.Empty;
		}

		private string GerarRegistro16410()
		{
			return string.Empty;
		}

		private string GerarRegistro16400()
		{
			return string.Empty;
		}

		private string GerarRegistro16310()
		{
			return string.Empty;
		}

		private string GerarRegistro16300()
		{
			return string.Empty;
		}

		private string GerarRegistro16210()
		{
			return string.Empty;
		}

		private string GerarRegistro16200()
		{
			return string.Empty;
		}

		private string GerarRegistro16100()
		{
			return string.Empty;
		}

		private string GerarRegistro16000()
		{
			return string.Empty;
		}

		private string GerarRegistro15400()
		{
			return string.Empty;
		}

		private string GerarRegistro15322()
		{
			return string.Empty;
		}

		private string GerarRegistro15321()
		{
			return string.Empty;
		}

		private string GerarRegistro15320()
		{
			return string.Empty;
		}

		private string GerarRegistro15310()
		{
			return string.Empty;
		}

		private string GerarRegistro15300()
		{
			return string.Empty;
		}

		private string GerarRegistro15210()
		{
			return string.Empty;
		}

		private string GerarRegistro15200()
		{
			return string.Empty;
		}

		private string GerarRegistro15110()
		{
			StringBuilder buffer = new StringBuilder();
			try
			{
				buffer.Append("15110");
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append("01");
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append("PESO BRUTO");
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(SEPARADOR_LINHA);
			}
			catch (Exception ex)
			{
				_cteLogService.InserirLogErro(_cte, "GerarArquivoCteEnvioServico", string.Format("GerarRegistro15110: {0}", ex.Message), ex);
				throw;
			}

			return buffer.ToString();
		}

		private string GerarRegistro15100()
		{
			StringBuilder buffer = new StringBuilder();
			try
			{
				string mercadoria = _fluxoComercial.Descricao ?? string.Empty;
				// Soma os valores da nota fiscal
				double totalMercadoria = _listaCteDetalhe.Sum(g => g.ValorNotaFiscal);

				buffer.Append("15100");
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(totalMercadoria.ToString("0.00", CultureInfo.GetCultureInfo("en-US").NumberFormat));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(mercadoria, 60));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(SEPARADOR_LINHA);
			}
			catch (Exception ex)
			{
				_cteLogService.InserirLogErro(_cte, "GerarArquivoCteEnvioServico", string.Format("GerarRegistro15100: {0}", ex.Message), ex);
				throw;
			}

			return buffer.ToString();
		}

		private string GerarRegistro15000()
		{
			StringBuilder buffer = new StringBuilder();
			try
			{
				buffer.Append("15000");
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(SEPARADOR_LINHA);
			}
			catch (Exception ex)
			{
				_cteLogService.InserirLogErro(_cte, "GerarArquivoCteEnvioServico", string.Format("GerarRegistro15000: {0}", ex.Message), ex);
				throw;
			}

			return buffer.ToString();
		}

		private string GerarRegistro14300()
		{
			return string.Empty;
		}

		private string GerarRegistro14190()
		{
			StringBuilder buffer = new StringBuilder();

			try
			{
				double baseCalculoIcms = CalcularBaseCalculoIcms();
				double aliquota = _contratoHistorico.PercentualAliquotaIcms ?? 0.0;
				double valorTotalIcms = baseCalculoIcms * aliquota;

				buffer.Append("14190");
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append("90");
				buffer.Append(SEPARADOR_COLUNA);
				// Campo pRedBC - Nao utilizado no SAP
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(baseCalculoIcms.ToString("0.00", CultureInfo.GetCultureInfo("en-US").NumberFormat));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(aliquota.ToString("0.00", CultureInfo.GetCultureInfo("en-US").NumberFormat));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(valorTotalIcms.ToString("0.00", CultureInfo.GetCultureInfo("en-US").NumberFormat));
				buffer.Append(SEPARADOR_LINHA);
			}
			catch (Exception ex)
			{
				_cteLogService.InserirLogErro(_cte, "GerarArquivoCteEnvioServico", string.Format("GerarRegistro14190: {0}", ex.Message), ex);
				throw;
			}

			return buffer.ToString();
		}

		private string GerarRegistro14181()
		{
			StringBuilder buffer = new StringBuilder();
			try
			{
				double baseCalculoIcms = CalcularBaseCalculoIcms();
				double aliquota = _contratoHistorico.PercentualAliquotaIcms ?? 0.0;
				double valorTotalIcms = baseCalculoIcms * aliquota;

				buffer.Append("14181");
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append("81");
				buffer.Append(SEPARADOR_COLUNA);
				// Campo pRedBC - Nao utilizado no SAP
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(baseCalculoIcms.ToString("0.00", CultureInfo.GetCultureInfo("en-US").NumberFormat));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(aliquota.ToString("0.00", CultureInfo.GetCultureInfo("en-US").NumberFormat));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(valorTotalIcms.ToString("0.00", CultureInfo.GetCultureInfo("en-US").NumberFormat));
			}
			catch (Exception ex)
			{
				_cteLogService.InserirLogErro(_cte, "GerarArquivoCteEnvioServico", string.Format("GerarRegistro14181: {0}", ex.Message), ex);
				throw;
			}

			return buffer.ToString();
		}

		private string GerarRegistro14180()
		{
			StringBuilder buffer = new StringBuilder();

			try
			{
				double baseCalculoIcms = CalcularBaseCalculoIcms();
				double aliquota = _contratoHistorico.PercentualAliquotaIcms ?? 0.0;
				double valorTotalIcms = baseCalculoIcms * aliquota;

				buffer.Append("14180");
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append("80");
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(baseCalculoIcms.ToString("0.00", CultureInfo.GetCultureInfo("en-US").NumberFormat));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(aliquota.ToString("0.00", CultureInfo.GetCultureInfo("en-US").NumberFormat));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(valorTotalIcms.ToString("0.00", CultureInfo.GetCultureInfo("en-US").NumberFormat));
				buffer.Append(SEPARADOR_LINHA);
			}
			catch (Exception ex)
			{
				_cteLogService.InserirLogErro(_cte, "GerarArquivoCteEnvioServico", string.Format("GerarRegistro14180: {0}", ex.Message), ex);
				throw;
			}

			return buffer.ToString();
		}

		private string GerarRegistro14145(string codigoCst)
		{
			StringBuilder buffer = new StringBuilder();
			try
			{
				buffer.Append("14145");
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(codigoCst);
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(SEPARADOR_LINHA);
			}
			catch (Exception ex)
			{
				_cteLogService.InserirLogErro(_cte, "GerarArquivoCteEnvioServico", string.Format("GerarRegistro14145: {0}", ex.Message), ex);
				throw;
			}

			return buffer.ToString();
		}

		private string GerarRegistro14120()
		{
			return string.Empty;
		}

		private string GerarRegistro14100()
		{
			StringBuilder buffer = new StringBuilder();

			try
			{
				double baseCalculoIcms = CalcularBaseCalculoIcms();
				double aliquota = _contratoHistorico.PercentualAliquotaIcms ?? 0.0;
				double valorTotalIcms = baseCalculoIcms * aliquota;

				buffer.Append("14100");
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append("00");
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(baseCalculoIcms.ToString("0.00", CultureInfo.GetCultureInfo("en-US").NumberFormat));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(aliquota.ToString("0.00", CultureInfo.GetCultureInfo("en-US").NumberFormat));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(valorTotalIcms.ToString("0.00", CultureInfo.GetCultureInfo("en-US").NumberFormat));
				buffer.Append(SEPARADOR_LINHA);
			}
			catch (Exception ex)
			{
				_cteLogService.InserirLogErro(_cte, "GerarArquivoCteEnvioServico", string.Format("GerarRegistro14100: {0}", ex.Message), ex);
				throw;
			}

			return buffer.ToString();
		}

		private string GerarRegistro14000()
		{
			StringBuilder buffer = new StringBuilder();
			try
			{
				buffer.Append("14000");
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(SEPARADOR_LINHA);
			}
			catch (Exception ex)
			{
				_cteLogService.InserirLogErro(_cte, "GerarArquivoCteEnvioServico", string.Format("GerarRegistro14000: {0}", ex.Message), ex);
				throw;
			}

			return buffer.ToString();
		}

		private string GerarRegistro13110()
		{
			return string.Empty;
		}

		private string GerarRegistro13000()
		{
			StringBuilder buffer = new StringBuilder();
			try
			{
				double peso = _cte.PesoVagao ?? 0.0;
				double valorPrestacaoServico = CalcularBaseCalculoIcms();
				double descontoPorPeso = (_contratoHistorico.DescontoPorPeso ?? 0.0) * peso;
				double descontoPorPercentual = (_contratoHistorico.PercentualDesconto ?? 0.0) * peso;

				double valorReceber = valorPrestacaoServico - descontoPorPeso - descontoPorPercentual;

				buffer.Append("13000");
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(valorPrestacaoServico.ToString("0.00", CultureInfo.GetCultureInfo("en-US").NumberFormat));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(valorReceber.ToString("0.00", CultureInfo.GetCultureInfo("en-US").NumberFormat));
				buffer.Append(SEPARADOR_LINHA);
			}
			catch (Exception ex)
			{
				_cteLogService.InserirLogErro(_cte, "GerarArquivoCteEnvioServico", string.Format("GerarRegistro13000: {0}", ex.Message), ex);
				// throw;
			}

			return buffer.ToString();
		}

		private string GerarRegistro12420()
		{
			StringBuilder buffer = new StringBuilder();
			try
			{
				string cnpj = _empresaDestinataria.Cgc ?? string.Empty;
				string cpf = string.Empty;
				string numeroEndereco = string.Empty;
				string complementoEndereco = string.Empty;
				string bairro = string.Empty;
				string codigoCidadeIbge = _empresaDestinataria.CidadeIbge.CodigoIbge.ToString() ?? string.Empty;

				buffer.Append("12420");
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(cnpj.PadLeft(14, '0'));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(cpf.PadLeft(11, '0'));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(_empresaDestinataria.RazaoSocial, 60));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(_empresaDestinataria.Endereco, 60));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(numeroEndereco, 60));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(complementoEndereco, 60));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(bairro, 60));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(codigoCidadeIbge.PadLeft(7, '0'));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(_empresaDestinataria.CidadeIbge.Descricao, 60));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(_empresaDestinataria.Estado.Sigla);
				buffer.Append(SEPARADOR_LINHA);
			}
			catch (Exception ex)
			{
				_cteLogService.InserirLogErro(_cte, "GerarArquivoCteEnvioServico", string.Format("GerarRegistro12420: {0}", ex.Message), ex);
				throw;
			}

			return buffer.ToString();
		}

		private string GerarRegistro12410()
		{
			StringBuilder buffer = new StringBuilder();

			try
			{
				string numeroEndereco = string.Empty;
				string complementoEndereco = string.Empty;
				string bairro = string.Empty;
				string codigoCidadeIbge = _empresaDestinataria.CidadeIbge.CodigoIbge.ToString() ?? string.Empty;
				int index = _empresaDestinataria.Cep.IndexOf('-');
				string cepCidade;
				if (index > -1)
				{
					cepCidade = _empresaDestinataria.Cep.Remove(index, 1);
				}
				else
				{
					cepCidade = _empresaDestinataria.Cep;
				}

				PaisBacen paisBacen = _paisBacenRepository.ObterPorSiglaResumida(_empresaDestinataria.CidadeIbge.SiglaPais);

				buffer.Append("12410");
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(_empresaDestinataria.Endereco, 60));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(numeroEndereco, 60));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(complementoEndereco, 60));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(bairro, 60));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(codigoCidadeIbge.PadLeft(7, '0'));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(_empresaDestinataria.CidadeIbge.Descricao, 60));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(cepCidade);
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(_empresaDestinataria.Estado.Sigla);
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(paisBacen.CodigoBacen.Substring(1, 4));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(paisBacen.Nome, 60));
				buffer.Append(SEPARADOR_LINHA);
			}
			catch (Exception ex)
			{
				_cteLogService.InserirLogErro(_cte, "GerarArquivoCteEnvioServico", string.Format("GerarRegistro12410: {0}", ex.Message), ex);
				throw;
			}

			return buffer.ToString();
		}

		private string GerarRegistro12400()
		{
			StringBuilder buffer = new StringBuilder();

			try
			{
				string cnpj = _empresaDestinataria.Cgc ?? string.Empty;
				string cpf = string.Empty;

				buffer.Append("12400");
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(cnpj.PadLeft(14, '0'));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(cpf.PadLeft(11, '0'));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(_empresaDestinataria.InscricaoEstadual, 14));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(_empresaDestinataria.RazaoSocial, 60));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(_empresaDestinataria.Telefone1, 12));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(SEPARADOR_LINHA);
			}
			catch (Exception ex)
			{
				_cteLogService.InserirLogErro(_cte, "GerarArquivoCteEnvioServico", string.Format("GerarRegistro12400: {0}", ex.Message), ex);
				throw;
			}

			return buffer.ToString();
		}

		private string GerarRegistro12310()
		{
			StringBuilder buffer = new StringBuilder();

			try
			{
				string numeroEndereco = string.Empty;
				string complementoEndereco = string.Empty;
				string bairro = string.Empty;
				string codigoCidadeIbge = _empresaDestinataria.CidadeIbge.CodigoIbge.ToString() ?? string.Empty;
				int index = _empresaDestinataria.Cep.IndexOf('-');
				string cepCidade;
				if (index > -1)
				{
					cepCidade = _empresaDestinataria.Cep.Remove(index, 1);
				}
				else
				{
					cepCidade = _empresaDestinataria.Cep;
				}

				PaisBacen paisBacen = _paisBacenRepository.ObterPorSiglaResumida(_empresaDestinataria.CidadeIbge.SiglaPais);

				buffer.Append("12210");
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(_empresaDestinataria.Endereco, 60));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(numeroEndereco, 60));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(complementoEndereco, 60));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(bairro, 60));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(codigoCidadeIbge.PadLeft(7, '0'));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(_empresaDestinataria.CidadeIbge.Descricao, 60));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(cepCidade);
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(_empresaDestinataria.Estado.Sigla);
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(paisBacen.CodigoBacen.Substring(1, 4));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(paisBacen.Nome, 60));
				buffer.Append(SEPARADOR_LINHA);
			}
			catch (Exception ex)
			{
				_cteLogService.InserirLogErro(_cte, "GerarArquivoCteEnvioServico", string.Format("GerarRegistro12310: {0}", ex.Message), ex);
				throw;
			}

			return buffer.ToString();
		}

		private string GerarRegistro12300()
		{
			StringBuilder buffer = new StringBuilder();

			try
			{
				string cnpj = _empresaDestinataria.Cgc ?? string.Empty;
				string cpf = string.Empty;

				buffer.Append("12300");
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(cnpj.PadLeft(14, '0'));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(cpf.PadLeft(11, '0'));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(_empresaDestinataria.InscricaoEstadual, 14));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(_empresaDestinataria.RazaoSocial, 60));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(_empresaDestinataria.Telefone1, 12));
				buffer.Append(SEPARADOR_LINHA);
			}
			catch (Exception ex)
			{
				_cteLogService.InserirLogErro(_cte, "GerarArquivoCteEnvioServico", string.Format("GerarRegistro12300: {0}", ex.Message), ex);
				throw;
			}

			return buffer.ToString();
		}

		private string GerarRegistro12210()
		{
			StringBuilder buffer = new StringBuilder();
			try
			{
				string numeroEndereco = string.Empty;
				string complementoEndereco = string.Empty;
				string bairro = string.Empty;
				string codigoCidadeIbge = _empresaRemetente.CidadeIbge.CodigoIbge.ToString() ?? string.Empty;
				int index = _empresaRemetente.Cep.IndexOf('-');
				string cepCidade;
				if (index > -1)
				{
					cepCidade = _empresaRemetente.Cep.Remove(index, 1);
				}
				else
				{
					cepCidade = _empresaRemetente.Cep;
				}

				PaisBacen paisBacen = _paisBacenRepository.ObterPorSiglaResumida(_empresaRemetente.CidadeIbge.SiglaPais);

				buffer.Append("12210");
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(_empresaRemetente.Endereco, 60));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(numeroEndereco, 60));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(complementoEndereco, 60));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(bairro, 60));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(codigoCidadeIbge.PadLeft(7, '0'));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(_empresaRemetente.CidadeIbge.Descricao, 60));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(cepCidade);
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(_empresaRemetente.Estado.Sigla);
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(paisBacen.CodigoBacen.Substring(1, 4));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(paisBacen.Nome, 60));
				buffer.Append(SEPARADOR_LINHA);
			}
			catch (Exception ex)
			{
				_cteLogService.InserirLogErro(_cte, "GerarArquivoCteEnvioServico", string.Format("GerarRegistro12210: {0}", ex.Message), ex);
				throw;
			}

			return buffer.ToString();
		}

		private string GerarRegistro12200()
		{
			StringBuilder buffer = new StringBuilder();
			try
			{
				string cnpj = _contratoHistorico.EmpresaRemetente.Cgc ?? string.Empty;
				string cpf = string.Empty;

				buffer.Append("12200");
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(cnpj.PadLeft(14, '0'));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(cpf.PadLeft(11, '0'));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(_empresaRemetente.InscricaoEstadual, 14));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(_empresaRemetente.RazaoSocial, 60));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(_empresaRemetente.Telefone1, 12));
				buffer.Append(SEPARADOR_LINHA);
			}
			catch (Exception ex)
			{
				_cteLogService.InserirLogErro(_cte, "GerarArquivoCteEnvioServico", string.Format("GerarRegistro12200: {0}", ex.Message), ex);
				throw;
			}

			return buffer.ToString();
		}

		private string GerarRegistro12140()
		{
			StringBuilder buffer = new StringBuilder();
			try
			{
				string tipoDocumento = "00";
				string descricao = String.Empty;
				string numeroDocumento = "0";
				string dataEmissao = string.Empty;
				double valor = 0.0;

				foreach (CteDetalhe cteDetalhe in _listaCteDetalhe)
				{
					tipoDocumento = "00";
					numeroDocumento = cteDetalhe.NumeroNota;
					dataEmissao = cteDetalhe.DataCadastro.ToString("yyyy-MM-dd");
					valor = cteDetalhe.ValorNotaFiscal;

					buffer.Append("12140");
					buffer.Append(SEPARADOR_COLUNA);
					buffer.Append(tipoDocumento);
					buffer.Append(SEPARADOR_COLUNA);
					buffer.Append(descricao);
					buffer.Append(SEPARADOR_COLUNA);
					buffer.Append(numeroDocumento);
					buffer.Append(SEPARADOR_COLUNA);
					buffer.Append(dataEmissao);
					buffer.Append(SEPARADOR_COLUNA);
					buffer.Append(valor.ToString("0.00", CultureInfo.GetCultureInfo("en-US").NumberFormat));
					buffer.Append(SEPARADOR_LINHA);
				}
			}
			catch (Exception ex)
			{
				_cteLogService.InserirLogErro(_cte, "GerarArquivoCteEnvioServico", string.Format("GerarRegistro12140: {0}", ex.Message), ex);
				throw;
			}

			return buffer.ToString();
		}

		private string GerarRegistro12130()
		{
			StringBuilder buffer = new StringBuilder();

			try
			{
				foreach (CteDetalhe cteDetalhe in _listaCteDetalhe)
				{
					string chaveNotaFiscalEletronica = cteDetalhe.ChaveNfe ?? String.Empty;

					buffer.Append("12130");
					buffer.Append(SEPARADOR_COLUNA);
					buffer.Append(Tools.TruncateString(chaveNotaFiscalEletronica, 44));
					buffer.Append(SEPARADOR_COLUNA);
					// PIN SUFRAMA
					buffer.Append(SEPARADOR_LINHA);
				}
			}
			catch (Exception ex)
			{
				_cteLogService.InserirLogErro(_cte, "GerarArquivoCteEnvioServico", string.Format("GerarRegistro12130: {0}", ex.Message), ex);
				throw;
			}

			return buffer.ToString();
		}

		private string GerarRegistro12121()
		{
			StringBuilder buffer = new StringBuilder();
			try
			{
				EstacaoMae estacaoMaeOrigem = _fluxoComercial.Origem;
				string siglaUf = estacaoMaeOrigem.Municipio.Estado.Sigla;
				string cnpj = _contratoHistorico.EmpresaPagadora.Cgc ?? string.Empty;
				string numeroEndereco = string.Empty;
				string complementoEndereco = string.Empty;
				string bairro = string.Empty;
				string codigoCidadeIbge = _empresaPagadora.CidadeIbge.CodigoIbge.ToString() ?? string.Empty;

				buffer.Append("12121");
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(cnpj.PadLeft(14, '0'));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(_empresaPagadora.RazaoSocial, 60));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(_empresaPagadora.Endereco, 60));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(numeroEndereco, 60));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(complementoEndereco, 60));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(bairro, 60));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(codigoCidadeIbge.PadLeft(7, '0'));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(_empresaPagadora.CidadeIbge.Descricao, 60));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(siglaUf);
				buffer.Append(SEPARADOR_LINHA);
			}
			catch (Exception ex)
			{
				_cteLogService.InserirLogErro(_cte, "GerarArquivoCteEnvioServico", string.Format("GerarRegistro12121: {0}", ex.Message), ex);
				throw;
			}

			return buffer.ToString();
		}

		private string GerarRegistro12120()
		{
			StringBuilder buffer = new StringBuilder();
			try
			{
				string romaneio = string.Empty;
				string pedido = string.Empty;
				string serie = string.Empty;
				string numero = string.Empty;
				DateTime dataEmissao;
				string numeroContrato;
				double aliquotaSt = 0.0;
				double baseCalculoIcms = 0.0;
				double aliquota = 0.0;
				double valorTotalIcms = 0.0;
				double valorTotalProduto = 0.0;
				double valorNotaFiscal = 0.0;
				double baseCalculoIcmsSt = 0.0;
				double valorTotalIcmsSt = 0.0;
				string cfop = string.Empty;
				double peso = 0.0;

				foreach (CteDetalhe cteDetalhe in _listaCteDetalhe)
				{
					romaneio = string.Empty;
					pedido = string.Empty;
					serie = cteDetalhe.SerieNota;
					numero = cteDetalhe.NumeroNota;
					dataEmissao = cteDetalhe.DataNotaFiscal;
					numeroContrato = _contratoHistorico.NumeroContrato;
					aliquotaSt = _contratoHistorico.AliquotaIcmsSubstProduto ?? 0.0;

					baseCalculoIcms = CalcularBaseCalculoIcms();
					aliquota = _contratoHistorico.PercentualAliquotaIcms ?? 0.0;
					valorTotalIcms = baseCalculoIcms * aliquota;

					valorTotalProduto = cteDetalhe.ValorNotaFiscal;

					valorNotaFiscal = CalcularValorTotalNotaFiscal();

					baseCalculoIcmsSt = _contratoHistorico.ValorBaseCalculoSubstituicaoProduto ?? 0.0;
					valorTotalIcmsSt = baseCalculoIcmsSt * aliquotaSt;

					cfop = _contratoHistorico.CfopProduto ?? string.Empty;
					peso = cteDetalhe.PesoNotaFiscal;

					buffer.Append("12120");
					buffer.Append(SEPARADOR_COLUNA);
					// Campo: Numero do Romaneio
					buffer.Append(SEPARADOR_COLUNA);
					// Campo: Numero do Pedido
					buffer.Append(SEPARADOR_COLUNA);
					buffer.Append(Tools.TruncateString(serie, 3));
					buffer.Append(SEPARADOR_COLUNA);
					buffer.Append(Tools.TruncateString(numero, 20));
					buffer.Append(SEPARADOR_COLUNA);
					buffer.Append(dataEmissao.ToString("yyyy-MM-dd"));
					buffer.Append(SEPARADOR_COLUNA);
					// Base de calculo do ICMS ver a regra vBC do registro
					buffer.Append(baseCalculoIcms.ToString("0.00", CultureInfo.GetCultureInfo("en-US").NumberFormat));
					buffer.Append(SEPARADOR_COLUNA);
					buffer.Append(valorTotalIcms.ToString("0.00", CultureInfo.GetCultureInfo("en-US").NumberFormat));
					buffer.Append(SEPARADOR_COLUNA);
					buffer.Append(baseCalculoIcmsSt.ToString("0.00", CultureInfo.GetCultureInfo("en-US").NumberFormat));
					buffer.Append(SEPARADOR_COLUNA);
					buffer.Append(valorTotalIcmsSt.ToString("0.00", CultureInfo.GetCultureInfo("en-US").NumberFormat));
					buffer.Append(SEPARADOR_COLUNA);
					buffer.Append(valorTotalProduto.ToString("0.00", CultureInfo.GetCultureInfo("en-US").NumberFormat));
					buffer.Append(SEPARADOR_COLUNA);
					buffer.Append(valorNotaFiscal.ToString("0.00", CultureInfo.GetCultureInfo("en-US").NumberFormat));
					buffer.Append(SEPARADOR_COLUNA);
					buffer.Append(cfop);
					buffer.Append(SEPARADOR_COLUNA);
					buffer.Append(peso.ToString("0.00", CultureInfo.GetCultureInfo("en-US").NumberFormat));
					buffer.Append(SEPARADOR_COLUNA);
				}

				buffer.Append(SEPARADOR_LINHA);
			}
			catch (Exception ex)
			{
				_cteLogService.InserirLogErro(_cte, "GerarArquivoCteEnvioServico", string.Format("GerarRegistro12120: {0}", ex.Message), ex);
				throw;
			}

			return buffer.ToString();
		}

		private string GerarRegistro12110()
		{
			StringBuilder buffer = new StringBuilder();
			try
			{
				string numeroEndereco = string.Empty;
				string complementoEndereco = string.Empty;
				string bairro = string.Empty;
				string codigoCidadeIbge = _empresaPagadora.CidadeIbge.CodigoIbge.ToString() ?? string.Empty;
				int index = _empresaPagadora.Cep.IndexOf('-');
				string cepCidade;
				if (index > -1)
				{
					cepCidade = _empresaPagadora.Cep.Remove(index, 1);
				}
				else
				{
					cepCidade = _empresaPagadora.Cep;
				}

				PaisBacen paisBacen = _paisBacenRepository.ObterPorSiglaResumida(_empresaPagadora.CidadeIbge.SiglaPais);

				buffer.Append("12110");
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(_empresaPagadora.Endereco, 60));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(numeroEndereco, 60));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(complementoEndereco, 60));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(bairro, 60));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(codigoCidadeIbge.PadLeft(7, '0'));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(_empresaPagadora.CidadeIbge.Descricao, 60));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(cepCidade);
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(_empresaPagadora.Estado.Sigla);
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(paisBacen.CodigoBacen.Substring(1, 4));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(paisBacen.Nome, 60));
				buffer.Append(SEPARADOR_LINHA);
			}
			catch (Exception ex)
			{
				_cteLogService.InserirLogErro(_cte, "GerarArquivoCteEnvioServico", string.Format("GerarRegistro12110: {0}", ex.Message), ex);
				throw;
			}

			return buffer.ToString();
		}

		private string GerarRegistro12100()
		{
			StringBuilder buffer = new StringBuilder();
			try
			{
				string cnpj = _contratoHistorico.EmpresaPagadora.Cgc ?? string.Empty;
				string inscricaoEstadual = _empresaPagadora.InscricaoEstadual;

				buffer.Append("12100");
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(cnpj.PadLeft(14, '0'));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(inscricaoEstadual, 14));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(_empresaPagadora.RazaoSocial, 60));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(_empresaPagadora.NomeFantasia, 60));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(_empresaPagadora.Telefone1, 10));
				buffer.Append(SEPARADOR_LINHA);
			}
			catch (Exception ex)
			{
				_cteLogService.InserirLogErro(_cte, "GerarArquivoCteEnvioServico", string.Format("GerarRegistro12100: {0}", ex.Message), ex);
				throw;
			}

			return buffer.ToString();
		}

		private string GerarRegistro12010()
		{
			StringBuilder buffer = new StringBuilder();

			try
			{
				string numeroEndereco = string.Empty;
				string complementoEndereco = string.Empty;
				string bairro = string.Empty;
				string codigoCidadeIbge = _empresaPagadora.CidadeIbge.CodigoIbge.ToString() ?? string.Empty;
				int index = _empresaPagadora.Cep.IndexOf('-');
				string cepCidade;
				if (index > -1)
				{
					cepCidade = _empresaPagadora.Cep.Remove(index, 1);
				}
				else
				{
					cepCidade = _empresaPagadora.Cep;
				}

				PaisBacen paisBacen = _paisBacenRepository.ObterPorSiglaResumida(_empresaPagadora.CidadeIbge.SiglaPais);

				buffer.Append("12010");
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(_empresaPagadora.Endereco, 60));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(numeroEndereco, 60));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(complementoEndereco, 60));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(bairro, 60));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(codigoCidadeIbge.PadLeft(7, '0'));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(_empresaPagadora.CidadeIbge.Descricao, 60));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(cepCidade);
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(_empresaPagadora.Estado.Sigla);
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(paisBacen.CodigoBacen.Substring(1, 4));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(paisBacen.Nome, 60));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(_empresaPagadora.Telefone1, 12));
				buffer.Append(SEPARADOR_LINHA);
			}
			catch (Exception ex)
			{
				_cteLogService.InserirLogErro(_cte, "GerarArquivoCteEnvioServico", string.Format("GerarRegistro12010: {0}", ex.Message), ex);
				throw;
			}

			return buffer.ToString();
		}

		private string GerarRegistro12000()
		{
			StringBuilder buffer = new StringBuilder();
			try
			{
				string cnpj = _contratoHistorico.EmpresaPagadora.Cgc ?? string.Empty;
				string inscricaoEstadual = _empresaPagadora.InscricaoEstadual;

				buffer.Append("12000");
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(cnpj.PadLeft(14, '0'));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(inscricaoEstadual, 14));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(_empresaPagadora.RazaoSocial, 60));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(_empresaPagadora.NomeFantasia, 60));
				buffer.Append(SEPARADOR_LINHA);
			}
			catch (Exception ex)
			{
				_cteLogService.InserirLogErro(_cte, "GerarArquivoCteEnvioServico", string.Format("GerarRegistro12000: {0}", ex.Message), ex);
				throw;
			}

			return buffer.ToString();
		}

		private string GerarRegistro11350()
		{
			return string.Empty;
		}

		private string GerarRegistro11340()
		{
			return string.Empty;
		}

		private string GerarRegistro11329()
		{
			return string.Empty;
		}

		private string GerarRegistro11328()
		{
			return string.Empty;
		}

		private string GerarRegistro11327()
		{
			return string.Empty;
		}

		private string GerarRegistro11323()
		{
			return string.Empty;
		}

		private string GerarRegistro11322()
		{
			return string.Empty;
		}

		private string GerarRegistro11321()
		{
			return string.Empty;
		}

		private string GerarRegistro11320()
		{
			return string.Empty;
		}

		private string GerarRegistro11311()
		{
			return string.Empty;
		}

		private string GerarRegistro11310()
		{
			return string.Empty;
		}

		private string GerarRegistro11300()
		{
			StringBuilder buffer = new StringBuilder();
			try
			{
				// Fluxo comercial do carregamento
				EstacaoMae estacaoMaeOrigem = _fluxoComercial.Origem;
				EstacaoMae estacaoMaeDestino = _fluxoComercial.Destino;

				string nomeMunicipioOrigem = estacaoMaeOrigem.Municipio.Descricao ?? string.Empty;
				string nomeMunicipioDestino = estacaoMaeDestino.Municipio.Descricao ?? string.Empty;

				buffer.Append("11300");
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append("ENTREGA");
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append("LOGISTICA");
				buffer.Append(SEPARADOR_COLUNA);
				// Funcionario
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(nomeMunicipioOrigem, 40));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(nomeMunicipioDestino, 40));
				buffer.Append(SEPARADOR_COLUNA);
				// Observa��o
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(SEPARADOR_LINHA);
			}
			catch (Exception ex)
			{
				_cteLogService.InserirLogErro(_cte, "GerarArquivoCteEnvioServico", string.Format("GerarRegistro11300: {0}", ex.Message), ex);
				throw;
			}

			return buffer.ToString();
		}

		private string GerarRegistro00001()
		{
			StringBuilder buffer = new StringBuilder();
			try
			{
				buffer.Append("00001");
				buffer.Append(SEPARADOR_COLUNA);
				// Todo: Verificar de onde vai pegar o identificador do lote;
				buffer.Append("000000000000001");
				buffer.Append(SEPARADOR_LINHA);
			}
			catch (Exception ex)
			{
				_cteLogService.InserirLogErro(_cte, "GerarArquivoCteEnvioServico", string.Format("GerarRegistro00001: {0}", ex.Message), ex);
				throw;
			}

			return buffer.ToString();
		}

		private string GerarRegistro00000()
		{
			StringBuilder buffer = new StringBuilder();
			try
			{
				buffer.Append("00000");
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append("1.04");
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append("ENVIO");
				buffer.Append(SEPARADOR_LINHA);
			}
			catch (Exception ex)
			{
				_cteLogService.InserirLogErro(_cte, "GerarArquivoCteEnvioServico", string.Format("GerarRegistro00000: {0}", ex.Message), ex);
				throw;
			}

			return buffer.ToString();
		}

		private string GerarRegistro11000()
		{
			StringBuilder buffer = new StringBuilder();
			try
			{
				buffer.Append("11000");
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(_cte.Versao.CodigoVersao);
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append("CTe");
				buffer.Append(_cte.Chave);
				buffer.Append(SEPARADOR_LINHA);
			}
			catch (Exception ex)
			{
				_cteLogService.InserirLogErro(_cte, "GerarArquivoCteEnvioServico", string.Format("GerarRegistro11000: {0}", ex.Message), ex);
				throw;
			}

			return buffer.ToString();
		}

		private string GerarRegistro11100()
		{
			StringBuilder buffer = new StringBuilder();
			try
			{
				// Configura��o geral
				ConfiguracaoTranslogic formaEmissao = _configuracaoTranslogicRepository.ObterPorId(_chaveCteFormaEmissao);
				ConfiguracaoTranslogic tipoAmbiente = _configuracaoTranslogicRepository.ObterPorId(_chaveCteTipoAmbiente);
				ConfiguracaoTranslogic versaoProcesso = _configuracaoTranslogicRepository.ObterPorId(_chaveCteVersaoProcesso);

				// Fluxo comercial do carregamento
				EstacaoMae estacaoMaeOrigem = _fluxoComercial.Origem;
				EstacaoMae estacaoMaeDestino = _fluxoComercial.Destino;

				string cfop = _contratoHistorico.Cfop ?? string.Empty;
				string descricaoCfop = _contratoHistorico.DescricaoCfop ?? string.Empty;
				string serieCte = _cte.Serie ?? "0";
				string numeroCte = _cte.Numero ?? "0";
				string digitoChaveCte = _cte.Chave[_cte.Chave.Length - 1].ToString();
				string chaveAcessoCteReferenciado = string.Empty;
				string codigoCidadeIbgeOrigem = estacaoMaeOrigem.Municipio.CidadeIbge.CodigoIbge.ToString() ?? string.Empty;
				string nomeMunicipioOrigem = estacaoMaeOrigem.Municipio.Descricao ?? string.Empty;
				string codigoCidadeIbgeDestino = estacaoMaeDestino.Municipio.CidadeIbge.CodigoIbge.ToString() ?? string.Empty;
				string nomeMunicipioDestino = estacaoMaeDestino.Municipio.Descricao ?? string.Empty;
				string detalheRetira = string.Empty;

				string siglaUf = estacaoMaeOrigem.Municipio.Estado.Sigla;
				UfIbge unidadeFederativaIbge = _unidadeFederativaIbgeRepository.ObterPorSigla(siglaUf);

				buffer.Append("11100");
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(unidadeFederativaIbge.Id.ToString().PadLeft(2, '0'));
				buffer.Append(SEPARADOR_COLUNA);
				// Numero aleatorio do CTE
				buffer.Append(_cte.Numero.PadLeft(9, '0'));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(cfop, 4));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(descricaoCfop, 60));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append("1"); // Forma de pagamento pelo servi�o (1 - A pagar)
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append("57"); // Modelo do documento fiscal (57)
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(serieCte, 3));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(numeroCte.PadLeft(9, '0'));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(_cte.DataHora.ToString("yyyy-MM-ddTHH:mm:ss"));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append("1"); // Formato impressao 1 - Retrato
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(formaEmissao.Valor); // Forma de emiss�o 
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(digitoChaveCte);
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(tipoAmbiente.Valor); // Ambiente de homologa��o ou produ��o
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append("0"); // Tipo conhecimento - 1 - Normal
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append("0"); // Processo emiss�o CT-e
				buffer.Append(versaoProcesso.Valor); // Vers�o do Processo
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(chaveAcessoCteReferenciado.PadLeft(44, '0'));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(codigoCidadeIbgeOrigem, 7));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(nomeMunicipioOrigem, 60));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(estacaoMaeOrigem.Municipio.Estado.Sigla);
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append("04"); // Modal
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append("0"); // Tipo de servi�o
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(codigoCidadeIbgeOrigem.PadLeft(7, '0'));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(nomeMunicipioOrigem, 60));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(estacaoMaeOrigem.Municipio.Estado.Sigla);
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(codigoCidadeIbgeDestino.PadLeft(7, '0'));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(nomeMunicipioDestino, 60));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(estacaoMaeDestino.Municipio.Estado.Sigla);
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append("0");
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(detalheRetira, 160));
				buffer.Append(SEPARADOR_LINHA);
			}
			catch (Exception ex)
			{
				_cteLogService.InserirLogErro(_cte, "GerarArquivoCteEnvioServico", string.Format("GerarRegistro11100: {0}", ex.Message), ex);
				throw;
			}

			return buffer.ToString();
		}

		private string GerarRegistro11110()
		{
			StringBuilder buffer = new StringBuilder();
			try
			{
				buffer.Append("11110");
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append("0"); // Tomador do servi�o
				buffer.Append(SEPARADOR_LINHA);
			}
			catch (Exception ex)
			{
				_cteLogService.InserirLogErro(_cte, "GerarArquivoCteEnvioServico", string.Format("GerarRegistro11110: {0}", ex.Message), ex);
				throw;
			}

			return buffer.ToString();
		}

		private string GerarRegistro11120()
		{
			StringBuilder buffer = new StringBuilder();
			try
			{
				string cnpj = _contratoHistorico.EmpresaPagadora.Cgc ?? string.Empty;
				string inscricaoEstadual = _empresaPagadora.InscricaoEstadual;

				buffer.Append("11120");
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append("4"); // Tomador do servi�o
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(cnpj.PadLeft(14, '0'));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(inscricaoEstadual, 14));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(_empresaPagadora.RazaoSocial, 60));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(_empresaPagadora.NomeFantasia, 60));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(_empresaPagadora.Telefone1, 12));
				buffer.Append(SEPARADOR_LINHA);
			}
			catch (Exception ex)
			{
				_cteLogService.InserirLogErro(_cte, "GerarArquivoCteEnvioServico", string.Format("GerarRegistro11120: {0}", ex.Message), ex);
				throw;
			}

			return buffer.ToString();
		}

		private string GerarRegistro11121()
		{
			StringBuilder buffer = new StringBuilder();
			try
			{
				string numeroEndereco = string.Empty;
				string complementoEndereco = string.Empty;
				string bairro = string.Empty;
				string codigoCidadeIbge = _empresaPagadora.CidadeIbge.CodigoIbge.ToString() ?? string.Empty;
				int index = _empresaPagadora.Cep.IndexOf('-');
				string cepCidade;
				if (index > -1)
				{
					cepCidade = _empresaPagadora.Cep.Remove(index, 1);
				}
				else
				{
					cepCidade = _empresaPagadora.Cep;
				}

				PaisBacen paisBacen = _paisBacenRepository.ObterPorSiglaResumida(_empresaPagadora.CidadeIbge.SiglaPais);

				buffer.Append("11121");
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(_empresaPagadora.Endereco, 255));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(numeroEndereco, 60));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(complementoEndereco, 60));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(bairro, 60));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(codigoCidadeIbge.PadLeft(7, '0'));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(_empresaPagadora.CidadeIbge.Descricao, 60));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(cepCidade);
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(_empresaPagadora.Estado.Sigla);
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(paisBacen.CodigoBacen.Substring(1, 4));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(paisBacen.Nome, 60));
				buffer.Append(SEPARADOR_LINHA);
			}
			catch (Exception ex)
			{
				_cteLogService.InserirLogErro(_cte, "GerarArquivoCteEnvioServico", string.Format("GerarRegistro11121: {0}", ex.Message), ex);
				throw;
			}

			return buffer.ToString();
		}

		/// <summary>
		/// Obt�m o tipo da nota fiscal
		/// </summary>
		/// <returns>Retorna 0 - NFe, 1 NF Manual e 2 - Outros</returns>
		/// <returns>Retorna 0 - NF Manual, 1  NFe e 2 - Outros</returns>
		private int ObterTipoNotaFiscal()
		{
			int retorno = -1;

			try
			{
				// Pega o primeiro detalhe do Cte para saber o tipo da nota fiscal. Dessa forma resolve pelo fato n�o
				// poder mais de um tipo de nota fiscal no mesmo Cte. (ou ser� do tipo NF manual ou NF Eletronica).
				CteDetalhe cteDetalhe = _listaCteDetalhe[0];

				// Caso tenha a chave de nota fiscal eletronica � uma NFe

				if (String.IsNullOrEmpty(cteDetalhe.ChaveNfe))
				{
					if (!String.IsNullOrEmpty(cteDetalhe.SerieNota))
					{
						// Verifica se � uma nota fiscal manual ou uma nota de combustivel	
						if (cteDetalhe.SerieNota.Trim().ToUpper() == "V2")
						{
							retorno = 2; // NF outros
						}
						else
						{
							retorno = 0; // NF Manual
						}
					}
					else
					{
						retorno = 0; // NF Manual
					}
				}
				else
				{
					retorno = 1; // NF-e
				}
			}
			catch (Exception ex)
			{
				_cteLogService.InserirLogErro(_cte, "GerarArquivoCteEnvioServico", string.Format("ObterTipoNotaFiscal: {0}", ex.Message), ex);
				throw;
			}

			return retorno;
		}

		private void InserirComposicaoFreteCte()
		{
			ComposicaoFreteCte composicaoFreteCte;
			double valorComIcms;
			double valorSemIcms;

			double totalComIcms = 0.0;
			double totalSemIcms = 0.0;

			string letraFerrovia = _cte.Despacho.SerieDespachoUf.LetraFerrovia;
			double toneladaUtil = _cte.PesoVagao ?? 0.0;
			IList<ComposicaoFreteContrato> listaComposicaoFreteContrato = _composicaoFreteContratoRepository.ObterPorNumeroContrato(_contratoHistorico.NumeroContrato, _cteRaiz.DataHora, letraFerrovia, true);
			IList<ComposicaoFreteContrato> listaComposicaoFreteContratoVg = _composicaoFreteContratoRepository.ObterPorNumeroContrato(_contratoHistorico.NumeroContrato, _cteRaiz.DataHora, "VG", true);

			// Dados da composicao frete contrato
			foreach (ComposicaoFreteContrato composicaoFreteContrato in listaComposicaoFreteContrato)
			{
				valorComIcms = composicaoFreteContrato.ValorComIcms ?? 0;
				valorSemIcms = composicaoFreteContrato.ValorSemIcms ?? 0;

				// Insere na composi��o frete cte
				composicaoFreteCte = new ComposicaoFreteCte();
				composicaoFreteCte.Cte            = _cte;
				composicaoFreteCte.CondicaoTarifa = composicaoFreteContrato.CondicaoTarifa;
				composicaoFreteCte.Ferrovia       = composicaoFreteContrato.Ferrovia;
				composicaoFreteCte.ValorComIcms   = valorComIcms * toneladaUtil;
				composicaoFreteCte.ValorSemIcms   = valorSemIcms * toneladaUtil;
				_composicaoFreteCteRepository.Inserir(composicaoFreteCte);

				totalComIcms += composicaoFreteCte.ValorComIcms.Value;
				totalSemIcms += composicaoFreteCte.ValorSemIcms.Value;
			}

			// Dados da composicao frete contrato VG
			foreach (ComposicaoFreteContrato composicaoFreteContrato in listaComposicaoFreteContratoVg)
			{
				if (_vagao.EmpresaProprietaria.Sigla == "L")
				{
					valorComIcms = composicaoFreteContrato.ValorComIcms ?? 0;
					valorSemIcms = composicaoFreteContrato.ValorSemIcms ?? 0;

					// Insere na composi��o frete cte
					composicaoFreteCte = new ComposicaoFreteCte();
					composicaoFreteCte.Cte            = _cte;
					composicaoFreteCte.CondicaoTarifa = composicaoFreteContrato.CondicaoTarifa;
					composicaoFreteCte.Ferrovia       = composicaoFreteContrato.Ferrovia;
					composicaoFreteCte.ValorComIcms   = valorComIcms * toneladaUtil;
					composicaoFreteCte.ValorSemIcms   = valorSemIcms * toneladaUtil;
					_composicaoFreteCteRepository.Inserir(composicaoFreteCte);

					totalComIcms += composicaoFreteCte.ValorComIcms.Value;
					totalSemIcms += composicaoFreteCte.ValorSemIcms.Value;
				}
			}
			// Grava a totaliza��o dos dados
			// Insere na composi��o frete cte
			composicaoFreteCte = new ComposicaoFreteCte();
			composicaoFreteCte.Cte = _cte;
			composicaoFreteCte.CondicaoTarifa = "ICMI";
			composicaoFreteCte.Ferrovia = letraFerrovia;
			composicaoFreteCte.ValorComIcms = totalComIcms;
			composicaoFreteCte.ValorSemIcms = totalSemIcms;
			_composicaoFreteCteRepository.Inserir(composicaoFreteCte);
		}

		private double CalcularBaseCalculoIcms()
		{
			double valorBaseCalculo = 0.0;
			try
			{
				int tipoTributacao = _contratoHistorico.TipoTributacao ?? 0;

				string letraFerrovia = _cte.Despacho.SerieDespachoUf.LetraFerrovia;
				double toneladaUtil = _cte.PesoVagao ?? 0.0;
				double aliquotaIcms = _contratoHistorico.PercentualAliquotaIcms ?? 0.0;
				double coeficiente = 100 - (100 / aliquotaIcms);

				IList<ComposicaoFreteContrato> listaComposicaoFreteContrato = _composicaoFreteContratoRepository.ObterPorNumeroContrato(_contratoHistorico.NumeroContrato, _cteRaiz.DataHora, letraFerrovia, true);
				IList<ComposicaoFreteContrato> listaComposicaoFreteContratoVg = _composicaoFreteContratoRepository.ObterPorNumeroContrato(_contratoHistorico.NumeroContrato, _cteRaiz.DataHora, "VG", true);

				double? valorTotal = 0.0;

				if (tipoTributacao == 0)
				{
					valorTotal = listaComposicaoFreteContrato.Sum(g => g.ValorSemIcms);
					if ((listaComposicaoFreteContratoVg.Count > 0) && (_vagao.EmpresaProprietaria.Sigla == "L"))
					{
						valorTotal += listaComposicaoFreteContratoVg.Sum(g => g.ValorSemIcms);
					}
				}
				else
				{
					valorTotal = listaComposicaoFreteContrato.Sum(g => g.ValorComIcms);
					if ((listaComposicaoFreteContratoVg.Count > 0) && (_vagao.EmpresaProprietaria.Sigla == "L"))
					{
						valorTotal += listaComposicaoFreteContratoVg.Sum(g => g.ValorComIcms);
					}
				}

				valorTotal *= toneladaUtil;

				valorBaseCalculo = (valorTotal * coeficiente).Value;
			}
			catch (Exception ex)
			{
				valorBaseCalculo = 0.0;
				_cteLogService.InserirLogErro(_cte, "GerarArquivoCteEnvioServico", string.Format("CalcularBaseCalculoIcms: {0}", ex.Message), ex);
			  // throw;
			}

			return valorBaseCalculo;
		}

		private double CalcularValorTotalNotaFiscal()
		{
			return _listaCteDetalhe.Sum(g => g.ValorNotaFiscal);
		}

		private double CalcularPesoTotal()
		{
			return _listaCteDetalhe.Sum(g => g.PesoNotaFiscal);
		}
	}
}
