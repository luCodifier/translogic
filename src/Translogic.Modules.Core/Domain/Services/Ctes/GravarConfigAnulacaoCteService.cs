﻿namespace Translogic.Modules.Core.Domain.Services.Ctes
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text.RegularExpressions;
    using ALL.Core.Util;
    using Castle.Services.Transaction;
    using Model.Diversos;
    using Model.Diversos.Bacen;
    using Model.Diversos.Bacen.Repositories;
    using Model.Diversos.Cte;
    using Model.Diversos.Ibge;
    using Model.Estrutura;
    using Model.FluxosComerciais;
    using Model.FluxosComerciais.ConhecimentoTransporteEletronico;
    using Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;
    using Model.FluxosComerciais.TriangulusConfig;
    using Model.FluxosComerciais.TriangulusConfig.Repositories;
    using Util;

    [Transactional]
    public class GravarConfigAnulacaoCteService
    {
        private readonly CteService _cteService;
        private readonly IAnulacaoCteRepository _anulacaoCteRepository;
        private readonly ICte37Repository _cte37Repository;
        private Cte _cte;

        public GravarConfigAnulacaoCteService(IAnulacaoCteRepository anulacaoCteRepository, ICte37Repository cte37Repository, CteService cteService)
        {
            _cteService = cteService;
            _cte37Repository = cte37Repository;
            _anulacaoCteRepository = anulacaoCteRepository;
        }

        /// <summary>
        /// Executa a gravação dos dados da carta de correção
        /// </summary>
        /// <param name="cte">Cte que será processado</param>
        [Transaction]
        public virtual void Executar(Cte cte)
        {
            try
            {
                LimparDados();
                CarregarDados(cte);
                GravarInformacoesAnulacaoCte();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void GravarInformacoesAnulacaoCte()
        {
            Cte37 cte37 = new Cte37();

            ////cte37.CfgUn = _cte.
            ////cte37.CfgDoc = Int32.Parse(_cte.Numero);
            cte37.CfgSerie = _cte.Serie;

            _cte37Repository.Inserir(cte37);

            ////_anulacaoCteRepository.Inserir(AnulacaoCte);

            ////throw new NotImplementedException();
        }

        private void LimparDados()
        {
            _cte = null;
        }

        private void CarregarDados(Cte cte)
        {
            _cte = cte;
        }
    }
}