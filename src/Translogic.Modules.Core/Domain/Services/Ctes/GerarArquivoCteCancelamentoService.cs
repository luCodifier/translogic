namespace Translogic.Modules.Core.Domain.Services.Ctes
{
	using System;
	using System.IO;
	using System.Text;
	using Model.FluxosComerciais.ConhecimentoTransporteEletronico;
	using Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;
	using Util;

	/// <summary>
	/// Classe para gera��o dos arquivos do CT-e de cancelamento
	/// </summary>
	public class GerarArquivoCteCancelamentoService
	{
		private readonly string SEPARADOR_COLUNA = ";";
		private readonly ICteRepository _cteRepository;

		private Cte _cte;
		private string _justificativa;
		private CteLogService _cteLogService;

		/// <summary>
		/// Initializes a new instance of the <see cref="GerarArquivoCteCancelamentoService"/> class.
		/// </summary>
		/// <param name="cteRepository">Reposit�rio do Cte injetado</param>
		/// <param name="cteLogService">Servico de log do cte injetado</param>
		public GerarArquivoCteCancelamentoService(ICteRepository cteRepository, CteLogService cteLogService)
		{
			_cteRepository = cteRepository;
			_cteLogService = cteLogService;
		}

		/// <summary>
		/// Executa a gera��o do arquivo de cancelamento
		/// </summary>
		/// <param name="cte">Cte que ser� gerado o arquivo de cancelamento</param>
		/// <param name="justificativa">Justificativa do cancelamento</param>
		/// <returns>Retorna o StringBuilder com o arquivo gerado</returns>
		public StringBuilder Executar(Cte cte, string justificativa)
		{
			StringBuilder builder = new StringBuilder();

			try
			{
				_cte = cte;
				_justificativa = justificativa;

				CarregarDados();

				builder.Append(GerarRegistro00000());
				builder.Append(GerarRegistro00010());
			}
			catch (Exception ex)
			{
				_cteLogService.InserirLogErro(_cte, "GerarArquivoCteCancelamentoService", string.Format("Executar: {0}", ex.Message));
				throw;
			}

			return builder;
		}

		private string GerarRegistro00010()
		{
			StringBuilder buffer = new StringBuilder();

			try
			{
				string chaveAcesso = _cte.Chave;
				string numeroProtocolo = _cte.NumeroProtocolo ?? string.Empty;
				string justificativa = _justificativa;

				buffer.Append("00010");
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append("ID");
				buffer.Append(Tools.TruncateString(chaveAcesso, 44));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(1); // Ambiente de homologa��o
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append("CANCELAR");
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(chaveAcesso, 44));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(numeroProtocolo, 15));
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append(Tools.TruncateString(justificativa, 155));
				buffer.Append(SEPARADOR_COLUNA);
			}
			catch (Exception ex)
			{
				_cteLogService.InserirLogErro(_cte, "GerarArquivoCteCancelamentoService", string.Format("GerarRegistro00010: {0}", ex.Message));
				throw;
			}

			return buffer.ToString();
		}

		private string GerarRegistro00000()
		{
			StringBuilder buffer = new StringBuilder();
			try
			{
				buffer.Append("00000");
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append("1.04");
				buffer.Append(SEPARADOR_COLUNA);
				buffer.Append("CANCELAMENTO");
			}
			catch (Exception ex)
			{
				_cteLogService.InserirLogErro(_cte, "GerarArquivoCteCancelamentoService", string.Format("GerarRegistro00000: {0}", ex.Message));
				throw;
			}

			return buffer.ToString();
		}

		private void CarregarDados()
		{
			// Carregar dados do CTE
			// _cte = _cteRepository.ObterPorId(1);
		}
	}
}