﻿namespace Translogic.Modules.Core.Domain.Services.Ctes
{
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;
    using Translogic.Modules.Core.Domain.Services.Ctes.Interface;

    public class CteCanceladoNotasService : ICteCanceladoNotasService
    {
        #region Fields

        private readonly ICteCanceladoNotasRepository _cteCanceladoNotasRepository;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///     contrutor da classe
        /// </summary>
        /// <param name="cteCanceladoNotasRepository">objeto instanciado</param>
        public CteCanceladoNotasService(ICteCanceladoNotasRepository cteCanceladoNotasRepository)
        {
            this._cteCanceladoNotasRepository = cteCanceladoNotasRepository;
        }

        #endregion
    }
}