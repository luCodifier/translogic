﻿using System.ServiceModel;
using System.ServiceModel.Activation;
using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;
using Translogic.Modules.Core.Domain.Model.WebServices.CteFerroviario;
using Translogic.Modules.Core.Domain.Services.Ctes.Interface;

namespace Translogic.Modules.Core.Domain.Services.Ctes
{
    /// <summary>
    /// serviço de envio de informações do cte
    /// </summary>
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceBehavior]
    public class CteFerroviarioService : ICteFerroviarioService
    {
        private readonly ICteFerroviarioRepository _cteFerroviarioRepository;

        /// <summary>
        /// contrutor da classe
        /// </summary>
        /// <param name="cteFerroviarioRepository">objeto instanciado</param>
        public CteFerroviarioService(ICteFerroviarioRepository cteFerroviarioRepository)
        {
            _cteFerroviarioRepository = cteFerroviarioRepository;
        }

        /// <summary>
        /// informações do cte
        /// </summary>
        /// <param name="request">informaçoes de requisição</param>
        /// <returns>Retorna dados do cte</returns>
        public InformationElectronicNotificationResponse InformationElectronicCtes(InformationElectronicNotificationRequest request)
        {
            return _cteFerroviarioRepository.InformationElectronicCtes(request);
        }

        /// <summary>
        /// informações do cte da malha norte com MDFEs
        /// </summary>
        /// <param name="request">informaçoes de requisição</param>
        /// <returns>Retorna dados do cte</returns>
        public InformationElectronicNotificationResponse InformationElectronicCtesMalhaNorteComMDFE(InformationElectronicNotificationRequest request)
        {
            return _cteFerroviarioRepository.InformationElectronicCtesMalhaNorteComMdfesNew(request);
        }


        /// <summary>
        /// informações do cte da malha sul com MDFEs
        /// </summary>
        /// <param name="request">informaçoes de requisição</param>
        /// <returns>Retorna dados do cte</returns>
        public InformationElectronicNotificationResponse InformationElectronicCtesMalhaSulComMdfe(InformationElectronicNotificationRequest request)
        {
            return _cteFerroviarioRepository.InformationElectronicCtesMalhaSulComMdfes(request);
        }
    }
}