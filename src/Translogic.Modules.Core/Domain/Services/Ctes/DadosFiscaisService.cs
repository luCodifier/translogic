﻿namespace Translogic.Modules.Core.Domain.Services.Ctes
{
	using System;
	using System.Collections;
	using System.Collections.Generic;
	using System.Linq;
	using System.ServiceModel.Activation;

	using Translogic.Modules.Core.Domain.Model.Diversos.Repositories;
	using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;
	using Translogic.Modules.Core.Interfaces.DadosFiscais;
	using Translogic.Modules.Core.Interfaces.DadosFiscais.Interfaces;

	/// <summary>
	/// Serviço de dados fiscais
	/// </summary>
	[AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
	public class DadosFiscaisService : IDadosFiscaisService
	{
		private readonly ICteRepository _cteRepository;
		private readonly ISispatRepository _sispatRepository;

		/// <summary>
		/// Initializes a new instance of the <see cref="DadosFiscaisService"/> class.
		/// </summary>
		/// <param name="cteRepository"> The cte repository.  </param>
		/// <param name="sispatRepository"> The sispat Repository. </param>
		public DadosFiscaisService(ICteRepository cteRepository, ISispatRepository sispatRepository)
		{
			_cteRepository = cteRepository;
			_sispatRepository = sispatRepository;
		}

		/// <summary>
		/// Obtém os ctes gerados no dia passado como parametro
		/// </summary>
		/// <param name="data">Dia da consulta</param>
		/// <returns>Lista de Ctes gerados</returns>
		public IList<CteFerroviarioDescarregado> ObterCtesFerroviariosDescarregadosPorData(DateTime data)
		{
			DateTime dataInicio, dataTermino;
			dataInicio = data.Date;
			dataTermino = dataInicio.AddDays(1).AddSeconds(-1);
			IList<CteFerroviarioDescarregado> list = _cteRepository.ObterCtesDescarregaosPorPeriodoSiglaFerrovia(dataInicio, dataTermino, "MT");
			return list;
		}

		/// <summary>
		/// Obtém os ctes cancelados no dia passado como parametro
		/// </summary>
		/// <param name="data">Dia de consulta</param>
		/// <returns>Lista de ctes cancelados</returns>
		public IList<CteFerroviarioCancelado> ObterCtesCanceladosPorData(DateTime data)
		{
			DateTime dataInicio, dataTermino;
			dataInicio = data.Date;
			dataTermino = dataInicio.AddDays(1).AddSeconds(-1);
			IList<CteFerroviarioCancelado> list = _cteRepository.ObterCtesCanceladosPorPeriodoSiglaFerrovia(dataInicio, dataTermino, "MT");
			if (list != null)
			{
				foreach (CteFerroviarioCancelado cancelado in list)
				{
					IList<CteFerroviarioCanceladoNovoCte> posteriores = _cteRepository.ObterCtesGeradosAposCancelamentoPorIdCte(cancelado.IdCte.Value);
					cancelado.ListaNovosCtes = posteriores;
				}

				return list;
			}

			return null;
		}

		/// <summary>
		/// Obtém os ctes rodoviarios no dia passado como parametro
		/// </summary>
		/// <param name="data">Data de descarga</param>
		/// <returns>Lista de Ctes descarregados</returns>
		public IList<CteRodoviarioDescarregado> ObterCtesRodoviariosDescarregadosPorData(DateTime data)
		{
			DateTime dataInicio, dataTermino;
			dataInicio = data.Date;
			dataTermino = dataInicio.AddDays(1).AddSeconds(-1);
			var list = _sispatRepository.ObterCtesDescarregadosPorPeriodo(dataInicio, dataTermino);
			List<CteRodoviarioDescarregado> lst = list.GroupBy(c => new { c.Placa, c.ChaveCte }).Select(
				s => new CteRodoviarioDescarregado
					{
						ChaveCte = s.Key.ChaveCte,
						Placa = s.Key.Placa,
						DataDescarga = s.Select(d => d.DataDescarga).FirstOrDefault(),
						PesoAferido = s.Select(d => d.PesoAferido).FirstOrDefault(),
						PesoTotalDeclarado = s.Sum(d => d.PesoDeclarado),
						ListaNfes = s.Select(d => new NfeCteRodoviario { ChaveNfe = d.ChaveNfe, PesoDeclarado = d.PesoDeclarado }).ToList()
					}).ToList();

			foreach (CteRodoviarioDescarregado cteRodoviarioDescarregado in lst)
			{
				cteRodoviarioDescarregado.Diferenca = cteRodoviarioDescarregado.PesoAferido - cteRodoviarioDescarregado.PesoTotalDeclarado;
			}

			return lst;
		}
	}
}