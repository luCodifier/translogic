﻿

namespace Translogic.Modules.Core.Domain.Services.Administracao
{
    using ALL.Core.Dominio;
    using Model.Administracao;
    using Model.Diversos.Repositories;
    using Translogic.Core.Infrastructure.Web;
    using Castle.Services.Transaction;

    [Transactional]
    public class ConfGeralService
    {
        private readonly IConfGeralRepository _confGeralRepository;
        private readonly ILogConfGeralRepository _logConfGeralRepository;

        public ConfGeralService(IConfGeralRepository confGeralRepository, ILogConfGeralRepository logConfGeralRepository)
        {
            _confGeralRepository = confGeralRepository;
            _logConfGeralRepository = logConfGeralRepository;
        }

        public ResultadoPaginado<ConfGeral> ObterTodos(DetalhesPaginacaoWeb pagination, string chave)
        {
            return _confGeralRepository.ObterTodosPaginado(pagination, chave);
        }

        public ConfGeral Obter(string chave)
        {
            return _confGeralRepository.ObterPorId(chave);
        }

        [Transaction]
        public virtual void Inserir(ConfGeral configuracao, string usuario)
        {
            try
            {
                _confGeralRepository.Inserir(configuracao);

                var log = LogConfGeral.LogNovo(configuracao.Id, usuario, configuracao.Valor);

                _logConfGeralRepository.Inserir(log);
            }
            catch (System.Exception ex)
            {
                
                throw;
            }
        }

        [Transaction]
        public virtual void Deletar(ConfGeral configuracao, string usuario)
        {
            var log = LogConfGeral.LogExclusao(configuracao.Id, usuario, configuracao.Valor);

            _logConfGeralRepository.Inserir(log);

            _confGeralRepository.Remover(configuracao);
        }

        [Transaction]
        public virtual void Atualizar(ConfGeral configuracao, string usuario, string valorAnterior)
        {
            var log = LogConfGeral.LogEdicao(configuracao.Id, usuario, configuracao.Valor, valorAnterior);

            _logConfGeralRepository.Inserir(log);

            _confGeralRepository.Atualizar(configuracao);
        }
    }
}