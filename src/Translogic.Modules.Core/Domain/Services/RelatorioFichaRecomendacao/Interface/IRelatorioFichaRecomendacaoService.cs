﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ALL.Core.Dominio;
using Translogic.Core.Infrastructure.Web;
using Translogic.Modules.Core.Domain.Model.Dto;

namespace Translogic.Modules.Core.Domain.Services.RelatorioFichaRecomendacao.Interface
{
    public interface IRelatorioFichaRecomendacaoService
    {
        /// <summary>
        /// Listagem de Trens Recomendados
        /// </summary>
        /// <param name="detalhesPaginacaoWeb"></param>
        /// <param name="trem"></param>
        /// <param name="numOs"></param>
        /// <param name="avaria"></param>
        /// <returns></returns>
        ResultadoPaginado<RelatorioFichaRecomendacaoDto> ObterTrensRecomendados(DetalhesPaginacaoWeb detalhesPaginacaoWeb, string trem, decimal? numOs, string avaria);

        /// <summary>
        /// Lista de Ficha Recomendações
        /// </summary>
        /// <returns></returns>
        IList<RelatorioFichaRecomendacaoExportDto> ObterListaFichaRecomendacaoExportar(decimal numOS);
    }
}
        