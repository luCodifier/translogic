﻿using Translogic.Modules.Core.Domain.Model.Dto;
using Translogic.Modules.Core.Domain.Services.RelatorioFichaRecomendacao.Interface;
using ALL.Core.Dominio;
using Translogic.Core.Infrastructure.Web;
using Translogic.Modules.Core.Domain.Model.RelatorioFichaRecomendacao.Repositories;
using System.Collections.Generic;

namespace Translogic.Modules.Core.Domain.Services.RelatorioFichaRecomendacao
{

    public class RelatorioFichaRecomendacaoService : IRelatorioFichaRecomendacaoService
    {

        #region Repositórios
        private readonly IRelatorioFichaRecomendacaoRepository _relatorioFichaRecomendacaoRepository;
        #endregion

        #region Construtores
        public RelatorioFichaRecomendacaoService(IRelatorioFichaRecomendacaoRepository relatorioFichaRecomendacaoRepository)
        {
            _relatorioFichaRecomendacaoRepository = relatorioFichaRecomendacaoRepository;
        }
        #endregion

        #region Métodos
        public ResultadoPaginado<RelatorioFichaRecomendacaoDto> ObterTrensRecomendados(DetalhesPaginacaoWeb detalhesPaginacaoWeb, string trem, decimal? numOs, string avaria)
        {
            return _relatorioFichaRecomendacaoRepository.ObterTrensRecomendados(detalhesPaginacaoWeb, trem, numOs, avaria);
        }

        public IList<RelatorioFichaRecomendacaoExportDto> ObterListaFichaRecomendacaoExportar(decimal numOS)
        {
            return _relatorioFichaRecomendacaoRepository.ObterListaFichaRecomendacaoExportar(numOS);
        }

        #endregion
    }
}