﻿namespace Translogic.Modules.Core.Domain.Services.Fornecedor.Interface
{
    using System.Collections.Generic;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Modules.Core.Util;
    using ALL.Core.Dominio;
    using Translogic.Core.Infrastructure.Web;
    using Translogic.Modules.Core.Domain.Model.Fornecedor;

    public interface IFornecedorOsService
    {
        /// <summary>
        /// Busca fornecedorOs por parametros
        /// </summary>        
        /// <param name="nomeFornecedor"></param>
        /// <param name="idLocal"></param>
        /// <param name="tipoServico"></param>
        /// <returns></returns>          
        ResultadoPaginado<FornecedorOsDto> ObterFornecedorOs(DetalhesPaginacaoWeb detalhesPaginacaoWeb, string nomeFornecedor, string local, int? idTipoServico, int? ativo);

        /// <summary>
        /// Retornar lista de FornecedorOS habilitados
        /// </summary>     
        /// <returns></returns>
        ResultadoPaginado<FornecedorOsDto> ObterFornecedoresOsHabilitados(DetalhesPaginacaoWeb detalhesPaginacaoWeb, string nomeFornecedor, string local, int? idServico, string Ativo);
        
        /// <summary>
        /// Retornar os locais Fornecedor por ID
        /// </summary>
        /// <param name="idFornecedor">ID do fornecedor</param>
        /// <param name="idFornecedor">detalhes de paginação</param>
        /// <returns></returns>
        ResultadoPaginado<LocalTipoServicoFornecedorDto> ObterLocaisFornecedorPorId(DetalhesPaginacaoWeb detalhesPaginacaoWeb, int idFornecedor);

        /// <summary>
        /// Retorna nome fornecedor pelo ID
        /// </summary>
        /// <param name="id fornecedor"></param>
        /// <returns></returns>        
        string ObterNomePorId(int? idFornecedor);
        
        /// <summary>
        /// Verifica se nome fornecedor existe na base
        /// </summary>
        /// <param name="nome fornecedor"></param>
        /// <returns></returns>        
        bool ObterPorNome(string nomeFornecedor);   

        /// <summary>
        /// Persiste dados do FornecedorOs
        /// </summary>
        /// <param name="fornecedorOs"></param>
        /// <returns></returns>
       // void InserirAtualizarFornecedor(FornecedorOsDto fornecedorOsDto);

        /// <summary>
        /// Persiste dados do FornecedorOs
        /// </summary>
        /// <param name="fornecedorOs"></param>
        /// <param name="usuario">Código Usuário</param>
        /// <returns></returns>
        void InserirAtualizarFornecedor(FornecedorOsDto fornecedorOsDto, string usuario);

        /// <summary>
        /// Buscar lista de fornecedores do local
        /// </summary>
        /// <param name="local"></param>
        /// <param name="tipoFornecedor"></param>
        /// <param name="todos">Retornar ativos e inativos, caso contrário só ativos e filtrados por local</param>
        /// <returns></returns>
        IList<FornecedorOsDto> ObterFornecedor(string local, EnumTipoFornecedor tipoFornecedor, bool todos);

        /// <summary>
        /// Retornar lista de locais do tipo de serviço
        /// </summary>
        /// <returns></returns>
        IList<OSLocalDto> ObterLocaisParaTipoServico();
        
        /// <summary>
        /// Obter o fornecedor pelo id
        /// </summary>
        /// <param name="idFornecedor">ID do fornecedor</param>
        /// <returns>Objeto</returns>
        FornecedorOsDto ObterPorId(int idFornecedor);

        /// <summary>
        /// Verifica se o fornecedor existe na base
        /// </summary>
        /// <param name="nomeFornecedor"></param>
        /// <param name="idFornecedor"></param>
        /// <returns></returns>
        bool ExisteFornecedorOsPorNomeId(string nomeFornecedor, int idFornecedor);

    }
}