﻿using System;
using System.Collections.Generic;
using Translogic.Modules.Core.Util;
using Translogic.Modules.Core.Domain.Model.LiberacaoFormacaoTrem.Repositories;
using Translogic.Modules.Core.Domain.Services.Fornecedor.Interface;
using Translogic.Core.Infrastructure.Web;
using ALL.Core.Dominio;
using Translogic.Modules.Core.Domain.Model.Dto;



namespace Translogic.Modules.Core.Domain.Services.LiberacaoFormacaoTrem
{
    public class LiberacaoFormacaoTremService : ILiberacaoFormacaoTremService
    {
        private readonly ILiberacaoFormacaoTremRepository _liberacaoFormacaoTremRepository;
        //private readonly ILocalFornecedorRepository _localFornecedorRepository;
        //private readonly ITipoServicoRepository _TipoServicoRepository;
        //private readonly IEstacaoMaeRepository _EstacaoMaeRepository;

        public LiberacaoFormacaoTremService(ILiberacaoFormacaoTremRepository liberacaoFormacaoTremRepository)
        {
            _liberacaoFormacaoTremRepository = liberacaoFormacaoTremRepository;
            //_localFornecedorRepository = localFornecedorRepositry;
            //_TipoServicoRepository = TipoServicoRepository;
            //_EstacaoMaeRepository = EstacaoMaeRepository;
        }

        public ResultadoPaginado<LiberacaoFormacaoTremDto> ObterConsulta(DetalhesPaginacaoWeb detalhesPaginacaoWeb, decimal OrdemServico, string OrigemTrem, string DestinoTrem, string LocalAtual, string NomeSol, string NomeAutorizador, DateTime dataInicial, DateTime dataFinal, string SituacaoTrava)
        {
            return _liberacaoFormacaoTremRepository.ObterConsulta(detalhesPaginacaoWeb, OrdemServico, OrigemTrem, DestinoTrem, LocalAtual, NomeSol, NomeAutorizador, dataInicial, dataFinal, SituacaoTrava);
        }

        public IList<LiberacaoFormacaoTremDto> ObterLiberacaoFormacaoTremExportar(
                                                                           decimal OrdemServico,
                                                                           string OrigemTrem,
                                                                           string DestinoTrem,
                                                                           string LocalAtual,
                                                                           string NomeSol,
                                                                           string NomeAutorizador,
                                                                           DateTime dataInicial,
                                                                           DateTime dataFinal,
                                                                           string SituacaoTrava)
        {
            return _liberacaoFormacaoTremRepository.ObterLiberacaoFormacaoTremExportar(
                                                               OrdemServico,
                                                               OrigemTrem,
                                                               DestinoTrem,
                                                               LocalAtual,
                                                               NomeSol,
                                                               NomeAutorizador,
                                                               dataInicial,
                                                               dataFinal,
                                                               SituacaoTrava);
        }

        /// <summary>
        /// Retornar o timmer definido em base para consulta automatica
        /// </summary>
        /// <returns></returns>
        public int ObterTimmerConsultaLiberacaoFormacaoTrem()
        {
            return _liberacaoFormacaoTremRepository.ObterTimmerConsultaLiberacaoFormacaoTrem();
        }

    }
}