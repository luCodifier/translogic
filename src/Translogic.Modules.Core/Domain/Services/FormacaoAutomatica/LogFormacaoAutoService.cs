﻿namespace Translogic.Modules.Core.Domain.Services.AnexacaoDesanexacao
{
    using Translogic.Core.Infrastructure.Web;
    using ALL.Core.Dominio;
    using System.Collections.Generic;
    using Translogic.Modules.Core.Util;
    using System;
    using System.Text;
    using System.Xml;
    using Translogic.Modules.Core.Domain.Model.FormacaoAutomatica.Repositories;
    using Translogic.Modules.Core.Domain.Services.FormacaoAutomatica.Interface;
    using Translogic.Modules.Core.Domain.Model.Dto.FormacaoAutomatica;

    public class LogFormacaoAutomaticaService : ILogFormacaoAutoService
    {

        private readonly ILogFormacaoAutoRepository _logFormacaoAutoRepository;
        private readonly ILogFormacaoAutoVeiculoRepository _logFormacaoAutoVeiculoRepository;

        public LogFormacaoAutomaticaService(ILogFormacaoAutoRepository logFormacaoAutoRepository,
                                            ILogFormacaoAutoVeiculoRepository logFormacaoAutoVeiculoRepository)
        {
            _logFormacaoAutoRepository = logFormacaoAutoRepository;
            _logFormacaoAutoVeiculoRepository = logFormacaoAutoVeiculoRepository;
        }

        public ResultadoPaginado<FormacaoAutoDto> ObterConsultaFormacaoAutomatica(DetalhesPaginacaoWeb detalhesPaginacaoWeb, FormacaoAutoRequestDto formacaoAutomaticaRequestDto)
        {
            var result = _logFormacaoAutoRepository.ObterConsultaFormacaoAutomatica(detalhesPaginacaoWeb, formacaoAutomaticaRequestDto);
            
            return result;
        }

        public ResultadoPaginado<FormacaoAutoVeiculoDto> ObterConsultaFormacaoAutomaticaVeiculos(DetalhesPaginacaoWeb detalhesPaginacaoWeb, decimal IdMensagem)
        {
            var result = _logFormacaoAutoVeiculoRepository.ObterConsultaFormacaoAutomaticaVeiculos(detalhesPaginacaoWeb, IdMensagem);

            return result;
        }

        public IEnumerable<FormacaoAutoDto> ObterConsultaFormacaoAutomaticaExportar(FormacaoAutoRequestDto formacaoAutoRequestDto)
        {
            var result = _logFormacaoAutoRepository.ObterConsultaFormacaoAutomaticaExportar(formacaoAutoRequestDto);

            return result;
        }

        public void ZeraTentativas(decimal IdMensagem)
        {
            _logFormacaoAutoRepository.ZeraTentativas(IdMensagem);
        }
    }
}