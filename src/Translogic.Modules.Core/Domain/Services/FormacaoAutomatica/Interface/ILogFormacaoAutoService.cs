﻿using ALL.Core.Dominio;
using Translogic.Core.Infrastructure.Web;
using System.Collections.Generic;
using Translogic.Modules.Core.Domain.Model.Dto.FormacaoAutomatica;
namespace Translogic.Modules.Core.Domain.Services.FormacaoAutomatica.Interface
{
    public interface ILogFormacaoAutoService
    {
        /// <summary>
        /// Retorna formação automatica
        /// </summary>
        /// <param name="detalhesPaginacaoWeb"></param>
        /// <param name="formacaoAutomaticaRequestDto"> Filtros pesquisa</param>
        /// <returns></returns>
        ResultadoPaginado<FormacaoAutoDto> ObterConsultaFormacaoAutomatica(DetalhesPaginacaoWeb detalhesPaginacaoWeb, FormacaoAutoRequestDto formacaoAutomaticaRequestDto);

        /// <summary>
        /// Retorna formação automtica veiculos
        /// </summary>
        /// <param name="detalhesPaginacaoWeb"></param>
        /// <param name="os"> os trem</param>
        /// <param name="estacao"> estacao trem</param>
        /// <param name="acao"> acao </param>
        /// <returns></returns>
        ResultadoPaginado<FormacaoAutoVeiculoDto> ObterConsultaFormacaoAutomaticaVeiculos(DetalhesPaginacaoWeb detalhesPaginacaoWeb, decimal os);

        IEnumerable<FormacaoAutoDto> ObterConsultaFormacaoAutomaticaExportar(FormacaoAutoRequestDto formacaoAutoRequestDto);

        void ZeraTentativas(decimal OS);
    }
}