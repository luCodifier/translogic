﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Translogic.Modules.Core.Domain.Services.Tfa.Interface;
using Translogic.Modules.Core.Domain.Model.Tfa.Repositories;
using Translogic.Modules.Core.Domain.Model.Estrutura.Repositories;
using Translogic.Modules.Core.Domain.Model.Tfa;
using Translogic.Core.Infrastructure;
using ALL.Core.Dominio;
using Translogic.Core.Infrastructure.Web;
using Translogic.Modules.Core.Domain.Model.Estrutura;

namespace Translogic.Modules.Core.Domain.Services.Tfa
{
    /// <summary>
    /// Classe de Serviço de EmpresaGrupoTfa
    /// </summary>
    public class EmpresaGrupoTfaService : IEmpresaGrupoTfaService
    {

        private readonly IEmpresaGrupoTfaRepository _empresaGrupoTfaRepository;
        private readonly IGrupoEmpresaTfaRepository _grupoEmpresaTfaRepository;
        private readonly IEmpresaRepository _empresaRepository;

        public EmpresaGrupoTfaService(IEmpresaGrupoTfaRepository empresaGrupoTfaRepository, IGrupoEmpresaTfaRepository grupoEmpresaTfaRepository, IEmpresaRepository empresaRepository)
        {
            _empresaGrupoTfaRepository = empresaGrupoTfaRepository;
            _grupoEmpresaTfaRepository = grupoEmpresaTfaRepository;
            _empresaRepository = empresaRepository;
        }

        /// <summary>
        /// Retorna todas as empresas do Grupo de Empresas passado como parâmetro
        /// </summary>
        /// <param name="paginacao">Dados para paginação em tela</param>
        /// <param name="idGrupoEmpresa">ID do Grupo de Empresas</param>
        /// <returns>Resultado Paginado de EmpresaGrupoTfa</returns>
        public ResultadoPaginado<EmpresaGrupoTfa> ObterTodosPorIdGrupoEmpresa(DetalhesPaginacaoWeb paginacao, int idGrupoEmpresa)
        {
            return _empresaGrupoTfaRepository.ObterTodosPorIdGrupoEmpresa(paginacao, idGrupoEmpresa);
        }

        /// <summary>
        /// Inclui uma empresa em um Grupo de Empresas para gestão de TFAs
        /// </summary>
        /// <param name="cnpj">CNPJ da Empresa no cadastro de Empresas</param>
        /// <param name="IdGrupoEmpresa">ID do Grupo de Empresas</param>
        /// <returns>Retorna EmpresaGrupoTfa inserida no banco de dados</returns>
        public EmpresaGrupoTfa IncluirEmpresaGrupoTfa(string cnpj, int IdGrupoEmpresa)
        {
            if (string.IsNullOrEmpty(cnpj))
            {
                throw new TranslogicException("CNPJ inválido!");
            }

            var empresaGrupo = _empresaGrupoTfaRepository.ObterPorCnpj(cnpj);

            if (empresaGrupo != null)
            {
                throw new TranslogicException("Empresa já cadastrada!");
            }

            var empresa = LocalizarEmpresa(cnpj);

            if (empresa == null)
            {
                throw new TranslogicException("Empresa não encontrada!");
            }

            var grupoEmpresa = _grupoEmpresaTfaRepository.ObterPorId(IdGrupoEmpresa);

            if (grupoEmpresa == null)
            {
                throw new TranslogicException("Grupo de Empresa não encontrado!");
            }


            empresaGrupo = new EmpresaGrupoTfa{Empresa= empresa, GrupoEmpresa=grupoEmpresa};

            return _empresaGrupoTfaRepository.Inserir(empresaGrupo);            

        }

        /// <summary>
        /// Exclui a Empresa do grupo de Empresas
        /// </summary>
        /// <param name="id">Id do registro à ser excluído</param>
        public void ExcluirEmpresaGrupoTfa(int id)
        {
            _empresaGrupoTfaRepository.Remover(id);            
        }

        /// <summary>
        /// Rertorna a Empresa, caso exista no Repositório IEmpresa, de acordo com o CNPJ informado
        /// </summary>
        /// <param name="cnpj">CNPJ da Empresa</param>
        /// <returns>Retorna a instãncia de IEmpresa</returns>
        public IEmpresa LocalizarEmpresa(string cnpj)
        {
            return _empresaRepository.ObterPorCnpj(cnpj);
        }
    }
}