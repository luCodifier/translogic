﻿namespace Translogic.Modules.Core.Domain.Services.Tfa
{
    using System.Threading;
    using System.Globalization;
    using System;
    using System.Collections.Generic;
    using Translogic.Modules.Core.Domain.Model.Tfa.Repositories;
    using Translogic.Modules.Core.Domain.Services.Tfa.Interface;
    using Translogic.Core.Infrastructure.Web;
    using ALL.Core.Dominio;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Modules.Regulatorio.Domains.Models.Repositories;
    using Translogic.Modules.Core.Domain.Model.Estrutura;
    using System.IO;
    using Translogic.Core.Infrastructure.Web.Helpers;
    using Translogic.Core.Infrastructure.Aws;
    using System.Text;
    using Translogic.Modules.Core.Util;
    using Translogic.Modules.Core.Domain.Model.Acesso;
    using Translogic.Modules.Core.Domain.Model.Tfa;
    using Translogic.Modules.Core.Helpers.ControlePerdas;
    using System.Configuration;

    public class TfaService : ITfaService
    {
        private readonly ITfaRepository _tfaRepository;
        private readonly ITfaAnexoRepository _tfaAnexoRepository;
        private readonly ILogTfaRepository _logTfaRepository;

        private Translogic.Modules.Core.Domain.Services.Mdfes.MdfeService _mdfeService;

        private string acima45Dias = "ACIMA DE 45 DIAS";
        private string abaixo45Dias = "ABAIXO DE 45 DIAS";
        private string emAberto = "EM ABERTO";


        public TfaService(ITfaRepository tfaRepository, ITfaAnexoRepository tfaAnexoRepository, Translogic.Modules.Core.Domain.Services.Mdfes.MdfeService mdfeService, ILogTfaRepository logTfaRepository)
        {
            _tfaRepository = tfaRepository;
            _tfaAnexoRepository = tfaAnexoRepository;
            this._mdfeService = mdfeService;
            _logTfaRepository = logTfaRepository;
            Thread.CurrentThread.CurrentCulture = new CultureInfo("pt-BR");
        }

        public ResultadoPaginado<TfaConsultaDto> ObterConsulta(DetalhesPaginacaoWeb detalhesPaginacaoWeb, string dtInicioProcesso, string dtFinalProcesso, string vagao, string terminal, string un, string up, string malha, string provisionado,
                                                     string dtInicioSinistros, string dtFinalSinistros, string numProcesso, string os, string situacao, string causa, string numTermo, string sindicancia, string anexoTFA)
        {
            DateTime? dtIniProcesso = null;
            DateTime? dtFinProcesso = null;
            DateTime? dtIniSinistro = null;
            DateTime? dtFinaSinistro = null;


            if (!string.IsNullOrEmpty(dtInicioProcesso) && dtInicioProcesso != "__/__/____")
            {
                dtIniProcesso = Convert.ToDateTime(dtInicioProcesso);
            }

            if (!string.IsNullOrEmpty(dtFinalProcesso) && dtFinalProcesso != "__/__/____")
            {
                dtFinProcesso = Convert.ToDateTime(dtFinalProcesso);
            }

            if (!string.IsNullOrEmpty(dtInicioSinistros) && dtInicioSinistros != "__/__/____")
            {
                dtIniSinistro = Convert.ToDateTime(dtInicioSinistros);
            }

            if (!string.IsNullOrEmpty(dtFinalSinistros) && dtFinalSinistros != "__/__/____")
            {
                dtFinaSinistro = Convert.ToDateTime(dtFinalSinistros);
            }

            if (!string.IsNullOrWhiteSpace(vagao))
                vagao = FormataParametroArray(vagao);
            if (!string.IsNullOrWhiteSpace(numProcesso))
                numProcesso = FormataParametroArray(numProcesso);
            if (!string.IsNullOrWhiteSpace(os))
                os = FormataParametroArray(os);

            return _tfaRepository.ObterConsulta(detalhesPaginacaoWeb, dtIniProcesso, dtFinProcesso, vagao, terminal, un, up, malha, provisionado,
                                                     dtIniSinistro, dtFinaSinistro, numProcesso, os, situacao, causa, numTermo, sindicancia, anexoTFA);
        }

        public IEnumerable<TfaExportaDto> ObterConsultaTFAExportar(string dtInicioProcesso, string dtFinalProcesso, string vagao, string terminal, string un, string up, string malha, string provisionado,
                                                                  string dtInicioSinistros, string dtFinalSinistros, string numProcesso, string os, string situacao, string causa, string numTermo, string sindicancia, string anexoTFA)
        {

            DateTime? dtIniProcesso = null;
            DateTime? dtFinProcesso = null;
            DateTime? dtIniSinistro = null;
            DateTime? dtFinaSinistro = null;


            if (!string.IsNullOrEmpty(dtInicioProcesso) && dtInicioProcesso != "__/__/____")
            {
                dtIniProcesso = Convert.ToDateTime(dtInicioProcesso);
            }

            if (!string.IsNullOrEmpty(dtFinalProcesso) && dtFinalProcesso != "__/__/____")
            {
                dtFinProcesso = Convert.ToDateTime(dtFinalProcesso);
            }

            if (!string.IsNullOrEmpty(dtInicioSinistros) && dtInicioSinistros != "__/__/____")
            {
                dtIniSinistro = Convert.ToDateTime(dtInicioSinistros);
            }

            if (!string.IsNullOrEmpty(dtFinalSinistros) && dtFinalSinistros != "__/__/____")
            {
                dtFinaSinistro = Convert.ToDateTime(dtFinalSinistros);
            }

            if (!string.IsNullOrWhiteSpace(vagao))
                vagao = FormataParametroArray(vagao);
            if (!string.IsNullOrWhiteSpace(numProcesso))
                numProcesso = FormataParametroArray(numProcesso);
            if (!string.IsNullOrWhiteSpace(os))
                os = FormataParametroArray(os);

            return _tfaRepository.ObterConsultaTFAExportar(dtIniProcesso, dtFinProcesso, vagao, terminal, un, up, malha, provisionado,
                        dtIniSinistro, dtFinaSinistro, numProcesso, os, situacao, causa, numTermo, sindicancia, anexoTFA);

        }

        #region Métodos para cálculos de valores para exportação

        //public decimal CalcularPerdaPagar(TfaExportaDto dto)
        //{
        //    var perda = Convert.ToDecimal((dto.PerdaTonVagao.HasValue ? dto.PerdaTonVagao.Value * 1 : 0));
        //    var tolerancia = Convert.ToDecimal((dto.Tolerancia.HasValue ? dto.Tolerancia.Value * 1 : 0));
        //    var pesoOrigem = Convert.ToDecimal((dto.PesoOrigem.HasValue ? dto.PesoOrigem.Value * 1 : 0));

        //    var total = perda;

        //    if ( dto.dtProcessoLegado == null)
        //    {
        //        if (!string.IsNullOrEmpty(dto.ConsiderarTolerancia) && dto.ConsiderarTolerancia.ToUpper().Equals("S"))
        //        {
        //            total = perda - ((tolerancia * pesoOrigem) / 100);
        //        }
        //    }
        //    return Convert.ToDecimal(total);
        //}

        //public decimal CalcularValorPagarProduto(TfaExportaDto dto)
        //{
        //    var valorMercadoriaUnitario = Convert.ToDecimal(dto.ValorMercadoriaUnitario.HasValue ? dto.ValorMercadoriaUnitario.Value * 1 : 0);
        //    var perdaTonVagao = Convert.ToDecimal(dto.PerdaTonVagao.HasValue ? dto.PerdaTonVagao.Value * 1 : 0);
        //    var tolerancia = Convert.ToDecimal(dto.Tolerancia.HasValue ? dto.Tolerancia.Value * 1 : 0);
        //    var pesoOrigem = Convert.ToDecimal(dto.PesoOrigem.HasValue ? dto.PesoOrigem.Value * 1 : 0);

        //    var total = (valorMercadoriaUnitario * (perdaTonVagao));

        //    if (!string.IsNullOrEmpty(dto.ConsiderarTolerancia))
        //    {
        //        if (dto.ConsiderarTolerancia.ToUpper().Equals("S"))
        //        {
        //            total = valorMercadoriaUnitario * (perdaTonVagao - ((tolerancia * pesoOrigem) / 100));
        //        }
        //    }
        //    return Convert.ToDecimal(total);
        //}

        //public decimal CalcularValorPagarFrete(TfaExportaDto dto)
        //{
        //    var tarifaFrete = Convert.ToDecimal(dto.TarifaFrete.HasValue ? dto.TarifaFrete.Value * 1 : 0);
        //    var perdaPagar = (CalcularPerdaPagar(dto) * 1);
        //    return Convert.ToDecimal(tarifaFrete * perdaPagar);

        //}

        //public decimal CalcularImposto(TfaExportaDto dto)
        //{
        //    var valorPagarProduto = (CalcularValorPagarProduto(dto) * 1);
        //    var pisConfins = Convert.ToDecimal((dto.PisCofins.HasValue ? dto.PisCofins.Value * 1 : 0));
        //    var icms = Convert.ToDecimal((dto.ICMS.HasValue ? dto.ICMS.Value : 0));

        //    return Convert.ToDecimal(  (((valorPagarProduto / ((100 - (pisConfins + icms)) / 100)) * (pisConfins + icms)) / 100)) ;
        //}

        //public decimal CalcularTotalVagao(TfaExportaDto dto)
        //{
        //    var valorPagarProduto = (CalcularValorPagarProduto(dto) * 1);
        //    var valorPagarFrete = (CalcularValorPagarFrete(dto) * 1);
        //    var imposto = (CalcularImposto(dto) * 1);
        //    var desconto = Convert.ToDecimal((dto.Desconto.HasValue ? dto.Desconto.Value * 1 : 0));
        //    return Convert.ToDecimal((valorPagarProduto + valorPagarFrete + imposto - desconto));
        //}

        //public string MostraClasseAnalise(TfaExportaDto dto)
        //{

        //    if (!dto.DtAnalise.HasValue)
        //    {
        //        return emAberto;
        //    }
        //    else if (dto.TempoAnalise.Value > 45)
        //    {
        //        return acima45Dias;
        //    }
        //    else
        //    {
        //        return abaixo45Dias;
        //    }
        //}

        public string FormataParametroArray(string parametro)
        {
            var splitParametro = parametro.Split(',');
            var retorno = "";
            foreach (var valor in splitParametro)
            {
                if (!string.IsNullOrWhiteSpace(valor))
                    retorno += valor + ",";
            }

            return retorno.Length > 0 ? retorno.Substring(0, retorno.LastIndexOf(",")) : string.Empty;
        }
        #endregion
        public IList<UnidadeNegocioDto> ObterUnidadeNegocio()
        {
            return _tfaRepository.ObterListaUnidadeNegocio();
        }

        public Translogic.Modules.Core.Domain.Model.Tfa.Tfa ObterPorNumProcesso(int numProcesso)
        {
            return _tfaRepository.ObterPorNumProcesso(numProcesso);
        }


        public bool UploadTFA(int numeroProcesso)
        {
            LogTfaHelper.Trace("Start UploadTFA", numeroProcesso);

            var tfaPdf = this._tfaRepository.ObterConsultaTFAPdf(numeroProcesso);

            if (tfaPdf != null)
            {
                string viewString = this.GerarHtmlTfa(tfaPdf);

                var pdfTfa = this._mdfeService.HtmlToPdfSemMargin(viewString, "Termo de Falta ou Avaria", true);

                byte[] anexoByteArray = null;
                using (System.IO.MemoryStream ms = new System.IO.MemoryStream())
                {
                    pdfTfa.CopyTo(ms);
                    anexoByteArray = ms.ToArray();
                }
                AmazonUploader uploader = new AmazonUploader();
                LogTfaHelper.Trace("Start UploadObject UploadTFA", numeroProcesso);
                int i = 0;
                bool uploaded = false;
                do
                {
                    i++;
                    LogTfaHelper.Trace("Uploading Try: " + i, numeroProcesso);
                    try
                    {
                        uploaded = uploader.UploadObject(new MemoryStream(anexoByteArray), string.Format("TFA_{0}.pdf", numeroProcesso), numeroProcesso.ToString());
                    }
                    catch (Exception ex)
                    {
                            LogTfaHelper.Trace(ex.Message + ex.StackTrace);
                    }

                } while (!uploaded && i <= 10);

                LogTfaHelper.Trace("End UploadObject UploadTFA: " + uploaded, numeroProcesso);

                LogTfaHelper.Trace("Start GetObject", numeroProcesso);

                var temporaryFolder = Path.Combine(GetTemporaryFolder(), Environment.UserName);
                var file = uploader.GetObject(string.Format("TFA_{0}.pdf", numeroProcesso), temporaryFolder, numeroProcesso.ToString());

                LogTfaHelper.Trace("End GetObject" + (file != null ? "Existe arquivo" : "Não existe arquivo"), numeroProcesso);

                if(file == null){
                    if(!LogTfaHelper.responseMessage.ContainsKey(numeroProcesso.ToString())){
                        LogTfaHelper.responseMessage.Add(numeroProcesso.ToString(), new List<string>());
                    }

                    LogTfaHelper.responseMessage[numeroProcesso.ToString()].Add("Arquivo TFA não existe");

                    var logTfa = new LogTfa()
                    {
                        Id = 0,
                        IdTfa = _tfaRepository.ObterPorNumProcesso(numeroProcesso).Id,
                        Descricao = "UPLOAD - TFA",
                        VersionDate = DateTime.Now
                    };

                    _logTfaRepository.Inserir(logTfa);
                }

                if (uploaded)
                {
                    LogTfaHelper.Trace("Star Criar TfaAnexo", numeroProcesso);

                    var tfaAnexo = this._tfaAnexoRepository.ObterPorNumProcesso(numeroProcesso.ToString());

                    if (tfaAnexo == null)
                    {
                        tfaAnexo = new Translogic.Modules.Core.Domain.Model.Tfa.TfaAnexo();
                        tfaAnexo.NumProcesso = numeroProcesso;
                    }

                    var tfa = this._tfaRepository.ObterPorNumProcesso(numeroProcesso);

                    tfaAnexo.Tfa = tfa;
                    tfaAnexo.ArquivoPdf = anexoByteArray;
                    this._tfaAnexoRepository.InserirOuAtualizar(tfaAnexo);

                    LogTfaHelper.Trace("End Criar TfaAnexo", numeroProcesso);

                    return true;
                }
            }

            LogTfaHelper.Trace("End UploadTFA", numeroProcesso);
            
            return false;
        }

        private const string TemporaryFolderKey = "TemporaryFolder";
        private string GetTemporaryFolder()
        {
            string temporaryFolderValue = string.Empty;
            AppSettingsReader reader = new AppSettingsReader();
            try
            {
                temporaryFolderValue = (string)reader.GetValue(TemporaryFolderKey, temporaryFolderValue.GetType());

                //if (!string.IsNullOrEmpty(temporaryFolderValue))
                //{
                //    var temporaryFolder = Server.MapPath(temporaryFolderValue);
                //    return temporaryFolder;
                //}

            }
            catch { }

            return temporaryFolderValue;
        }

        private string GerarHtmlTfa(Translogic.Modules.Core.Domain.Model.Dto.TfaDto tfaPdf)
        {
            string fileData = Tools.GetFromResources("Views.Tfa.TemplateTfa.htm");
            System.Text.StringBuilder buffer = new System.Text.StringBuilder(fileData);
            buffer.Replace("#PROCESSOID#", string.Format("{0}", tfaPdf.ProcessoId));
            buffer.Replace("#CLIENTE#", string.Format("{0}", tfaPdf.Cliente));
            buffer.Replace("#TERMINALORIGEM#", string.Format("{0}", tfaPdf.TerminalOrigem));
            buffer.Replace("#TERMINALDESTINO#", string.Format("{0}", tfaPdf.TerminalDestino));
            buffer.Replace("#UNIDADE#", string.Format("{0}", tfaPdf.Unidade));
            buffer.Replace("#DTVISTORIA#", string.Format("{0:dd/MM/yyyy}", tfaPdf.DtVistoria));
            buffer.Replace("#HRVISTORIA#", string.Format("{0:HH:mm:ss}", tfaPdf.DtVistoria));
            buffer.Replace("#VAGAO#", string.Format("{0}", tfaPdf.Vagao));
            buffer.Replace("#SERIEVAGAO#", string.Format("{0}", tfaPdf.SerieVagao));
            buffer.Replace("#DESPACHO#", string.Format("{0}", tfaPdf.Despacho));
            buffer.Replace("#SERIEDESPACHO#", string.Format("{0}", tfaPdf.SerieDespacho));
            buffer.Replace("#NF#", string.Format("{0}", tfaPdf.Nf));
            buffer.Replace("#PESOORIGEM#", string.Format("{0} toneladas", tfaPdf.PesoOrigem));
            buffer.Replace("#PESODESTINO#", string.Format("{0} toneladas", tfaPdf.PesoDestino));
            buffer.Replace("#PERDATONVAGAO#", string.Format("{0} toneladas", tfaPdf.PerdaTonVagao));
            buffer.Replace("#VALORMERCADOUNITARIO#", string.Format(CultureInfo.GetCultureInfo("pt-BR"), "{0:C2}", tfaPdf.ValorMercadoUnitario));
            buffer.Replace("#MERCADORIA#", string.Format("{0}", tfaPdf.Mercadoria));
            buffer.Replace("#CAUSA#", string.Format("{0}", tfaPdf.Causa));
            buffer.Replace("#LACRES#", string.Format("{0}", tfaPdf.Lacres));
            buffer.Replace("#GAMBITAGEM#", string.Format("{0}", tfaPdf.Gambitagem));
            buffer.Replace("#LIBERADOPOR#", string.Format("{0}", tfaPdf.LiberadoPor));
            buffer.Replace("#CONCLUSAO#", string.Format("{0}", tfaPdf.Conclusao));
            buffer.Replace("#HISTORICO#", string.Format("{0}", tfaPdf.Historico));
            buffer.Replace("#VISTORIADOR#", string.Format("{0}", tfaPdf.Vistoriador));
            buffer.Replace("#MATRICULAVISTORIADOR#", string.Format("{0}", tfaPdf.MatriculaVistoriador));
            buffer.Replace("#NOMEREPRESENTANTE#", string.Format("{0}", tfaPdf.NomeRepresentante));
            buffer.Replace("#DOCREPRESENTANTE#", string.Format("{0}", tfaPdf.DocRepresentante));
            buffer.Replace("#DATAREPRESENTANTE#", string.Format("{0:dd/MM/yyyy}", tfaPdf.DataRepresentante));
            buffer.Replace("#IMAGEMASSINATURAREPRESENTANTE#", (tfaPdf.ImagemAssinaturaRepresentante != null) ? System.Convert.ToBase64String(tfaPdf.ImagemAssinaturaRepresentante) : "");
            return buffer.ToString();
        }

        public Stream ObterPdfTfa(int numeroProcesso)
        {
            var anexo = _tfaAnexoRepository.ObterPorNumProcesso(numeroProcesso.ToString());
            Stream arquivoPdf = null;

            if (anexo != null && anexo.ArquivoPdf != null)
            {
                arquivoPdf = new MemoryStream(anexo.ArquivoPdf);
            }

            return arquivoPdf;
        }

        public void FecharLote(string[] processos,
                                             string analiseProcesso,
                                             DateTime dtPrepPagamento,
                                             string formaPagamento,
                                             DateTime dtPagamento,
                                             DateTime dtEncerramento,
                                             Usuario usuario)
        {

        if (formaPagamento == string.Empty) {
            throw new Exception("Forma de pagamento é obrigatório.");
        }

        if (dtPagamento == null) {
            throw new Exception("Data de pagamento é obrigatório");
        }

        if (dtEncerramento == null) {
            throw new Exception("Data de encerramento é obrigatório.");
        }

        if (dtPrepPagamento == null)
        {
            throw new Exception("Data de preparação do pagamento é obrigatório.");
        }


            var numProcessos = string.Join(",", processos);
            if (processos.Length > 0)
            {
                _tfaRepository.AtualizarProcessoSeguro(numProcessos, dtPrepPagamento, formaPagamento, dtPagamento, dtEncerramento, usuario.Codigo);

                if (analiseProcesso.Length > 4000)
                {
                    analiseProcesso = analiseProcesso.Substring(0, 3996) + "...";
                }

                for (int i = 0; i < processos.Length; i++)
                {
                    _tfaRepository.AtualizarAnaliseSeguro(processos[i], analiseProcesso, usuario.Codigo);
                }
            }
        }
    }
}
