﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Translogic.Modules.Core.Domain.Services.Tfa.Interface;
using Translogic.Modules.Core.Domain.Model.Tfa;
using Translogic.Modules.Core.Domain.Model.Tfa.Repositories;
using Translogic.Modules.Core.Domain.Model.Dto;

namespace Translogic.Modules.Core.Domain.Services.Tfa
{
    public class TerminalTfaService : ITerminalTfaService
    {
        private readonly ITerminalTfaRepository _terminalTfaRepository;

        public TerminalTfaService(ITerminalTfaRepository terminalTfaRepository)
        {
            _terminalTfaRepository = terminalTfaRepository;
        }
               
        /// <summary>
        /// Obtem a lista de DTOs de Terminais
        /// </summary>
        /// <returns>Retorna lista de DTOs de Terminais</returns>
        public IList<TerminalDto> ObterListaTerminalDto()
        {
            return _terminalTfaRepository.ObterListaTerminalDto();
        }

        /// <summary>
        /// Obtem o TerminalTfa pelo ID do Terminal  informado
        /// </summary>
        /// <param name="idTerminal">Id do Terminal</param>
        /// <returns></returns>
        public TerminalTfa ObterPorIdTerminal(int idTerminal)
        {
            return _terminalTfaRepository.ObterPorIdTerminal(idTerminal);
        }

        /// <summary>
        /// Inclui o TerminalTFA no banco de dados
        /// </summary>
        /// <param name="idTerminal">Id do Terminal</param>
        /// <returns></returns>
        public TerminalTfa InserirTerminalTfa(int idTerminal)
        {
            return _terminalTfaRepository.Inserir(new TerminalTfa { IdTerminal = idTerminal });
        }
    }
}