﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Translogic.Modules.Core.Domain.Services.Tfa.Interface;
using Translogic.Modules.Core.Domain.Model.Tfa;
using Translogic.Modules.Core.Domain.Model.Tfa.Repositories;
using ALL.Core.Dominio;
using Translogic.Core.Infrastructure.Web;

namespace Translogic.Modules.Core.Domain.Services.Tfa
{
    public class TerminalTfaEmailService : ITerminalTfaEmailService
    {

        private readonly ITerminalTfaEmailRepository _terminalTfaEmailRepository;
        private readonly ITerminalTfaRepository _terminalTfaRepository;


        public TerminalTfaEmailService(ITerminalTfaEmailRepository terminalTfaEmailRepository, ITerminalTfaRepository terminalTfaRepository)
        {
            _terminalTfaEmailRepository = terminalTfaEmailRepository;
            _terminalTfaRepository = terminalTfaRepository;
        }

        /// <summary>
        /// Obter Por Id
        /// </summary>
        /// <param name="id">id da entidade</param>
        /// <returns>entidade populada</returns>
        public TerminalTfaEmail ObterPorId(int id)
        {
            return _terminalTfaEmailRepository.ObterPorId(id);
        }

        /// <summary>
        /// Exclui a entidade pelo Id
        /// </summary>
        /// <param name="id">id da entidade</param>
        public void Remover(int id)
        {
            _terminalTfaEmailRepository.Remover(id);
        }

        /// <summary>
        /// Inclui um novo email para o Terminal
        /// </summary>
        /// <param name="terminalTfaEmail"></param>
        /// <returns></returns>
        public TerminalTfaEmail IncluirTerminalTfaEmail(TerminalTfa terminal, string email)
        {
            var terminalTfaEmail = new TerminalTfaEmail { Terminal = terminal, Email = email.ToLower()};
            return _terminalTfaEmailRepository.Inserir(terminalTfaEmail);
        }

        /// <summary>
        /// Obtem a lista de Emails vinculados ao terminal, de acordo com o ID passado como parâmetro
        /// </summary>
        /// <param name="paginacao">objeto com os parâmetros de paginação</param>
        /// <param name="idTerminal">Id do Terminal</param>
        /// <returns>Retorna lista paginada de Emails</returns>
        public ResultadoPaginado<TerminalTfaEmail> ObterTodosPorIdTerminal(DetalhesPaginacaoWeb paginacao, int idTerminal)
        {
            return _terminalTfaEmailRepository.ObterTodosPorIdTerminal(paginacao, idTerminal);
        }


        /// <summary>
        /// Obtem o email do terminal pelo IdTerminal e email
        /// </summary>
        /// <param name="idTerminal">Id do Terminal</param>
        /// <param name="email">e-mail</param>
        /// <returns>retorna o email dop terminal pelo IdTerminal e email</returns>
        public TerminalTfaEmail ObterPoridTerminalEmail(int idTerminal, string email)
        {
            return _terminalTfaEmailRepository.ObterPoridTerminalEmail(idTerminal, email);
        }
    }
}