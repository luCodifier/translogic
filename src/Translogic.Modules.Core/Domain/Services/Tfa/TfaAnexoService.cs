﻿namespace Translogic.Modules.Core.Domain.Services.Tfa
{
    using Translogic.Modules.Core.Domain.Model.Tfa.Repositories;
    using Translogic.Modules.Core.Domain.Services.Tfa.Interface;
    using Translogic.Modules.Core.Domain.Model.Tfa;

    public class TfaAnexoService : ITfaAnexoService
    {
        private readonly ITfaAnexoRepository _tfaAnexoRepository;

        public TfaAnexoService(ITfaAnexoRepository tfaAnexoRepository)
        {
            _tfaAnexoRepository = tfaAnexoRepository;
        }

        public TfaAnexo ObterPorNumProcesso(string numProcesso)
        {
            return _tfaAnexoRepository.ObterPorNumProcesso(numProcesso);
        }

        public void RemoverAnexoTFA(TfaAnexo tfaAnexo)
        {
            _tfaAnexoRepository.Remover(tfaAnexo);
        }

        public TfaAnexo Salvar(TfaAnexo tfaAnexo)
        {
            return _tfaAnexoRepository.InserirOuAtualizar(tfaAnexo);
        }
    }
}