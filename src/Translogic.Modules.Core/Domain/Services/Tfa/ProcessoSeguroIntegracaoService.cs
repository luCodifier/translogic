﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using Translogic.Core;
using Translogic.Core.Commons;
using Translogic.Core.Infrastructure;
using Translogic.Core.Infrastructure.Encryption;
using Translogic.Core.Infrastructure.Extensions;
using Translogic.Modules.Core.Domain.Model.ControlePerdas.Repositories;
using Translogic.Modules.Core.Domain.Model.Diversos.Repositories;
using Translogic.Modules.Core.Domain.Model.Dto;
using Translogic.Modules.Core.Domain.Model.Dto.Despacho;
using Translogic.Modules.Core.Domain.Model.Estrutura.Repositories;
using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;
using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Nfes;
using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Nfes.Repositories;
using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos;
using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos.Repositories;
using Translogic.Modules.Core.Domain.Model.Seguro;
using Translogic.Modules.Core.Domain.Model.Seguro.Repositories;
using Translogic.Modules.Core.Domain.Model.Tfa;
using Translogic.Modules.Core.Domain.Model.Tfa.Repositories;
using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao;
using Translogic.Modules.Core.Domain.Model.Via;
using Translogic.Modules.Core.Domain.Model.Via.Repositories;
using Translogic.Modules.Core.Domain.Services.FluxosComerciais;
using Translogic.Modules.Core.Domain.Services.Seguro.Interface;
using Translogic.Modules.Core.Domain.Services.Tfa.Interface;
using Translogic.Modules.Core.Helpers.ControlePerdas;
using Translogic.Modules.Core.Interfaces.ControlePerdas;
using Translogic.Modules.Core.Util;
using System.ServiceModel;


namespace Translogic.Modules.Core.Domain.Services.Tfa
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall, ConcurrencyMode = ConcurrencyMode.Multiple)] 
    public class ProcessoSeguroIntegracaoService : IProcessoSeguroIntegracaoService
    {
        #region Members of ProcessoSeguroIntegracaoService
        private readonly IAnaliseSeguroRepository _analiseSeguroRepository;
        private readonly IAreaOperacionalRepository _areaOperacionalRepository;
        private readonly ICausaSeguroRepository _causaSeguroRepository;
        private readonly IClienteSeguroRepository _clienteSeguroRepository;
        private readonly IConfGeralRepository _confGeralRepository;
        private readonly IContaContabilSeguroRepository _contaContabilSeguroRepository;
        private readonly ICteDetalheRepository _cteDetalheRepository;
        private readonly ICteEmpresasRepository _cteEmpresasRepository;
        private readonly ICteRepository _cteRepository;
        private readonly IDespachoTranslogicRepository _despachoTranslogicRepository;
        private readonly IEmpresaGrupoTfaRepository _empresaGrupoTfaRepository;
        private readonly IHistoricoSeguroRepository _historicoSeguroRepository;
        private readonly IItemDespachoRepository _itemDespachoRepository;
        private readonly ILogTfaRepository _logTfaRepository;
        private readonly IMangaRepository _mangaRepository;
        private readonly IModalSeguroRepository _modalSeguroRepository;        
        private readonly INfeProdutoReadonlyRepository _nfeProdutoReadonlyRepository;
        private readonly INfeReadonlyRepository _nfeReadonlyRepository;
        private readonly INotaFiscalTranslogicRepository _notaFiscalRepository;
        private readonly IProcessoSeguroRepository _processoSeguroRepository;
        private readonly IProcessoSeguroService _processoSeguroService;
        private readonly IRateioSeguroRepository _rateioSeguroRepository;
        private readonly IRecomendacaoToleranciaRepository _recomendacaoToleranciaRepository;
        private readonly IRecomendacaoVagaoRepository _recomendacaoVagaoRepository;
        private readonly ITfaCacheRepository _tfaCacheRepository;
        private readonly ITfaRepository _tfaRepository;
        private readonly ITfaService _tfaService;
        private readonly ITerminalTfaEmailRepository _terminalTfaEmailRepository;
        private readonly IUnidadeProducaoRepository _unidadeProducaoRepository;
        private readonly IUnidadeSeguroRepository _unidadeSeguroRepository;
        private static string[] arrayKg = new string[] { "kg", "sac" };
        private const string CONFIGURACAO_CONTROLE_PERDAS_EMAIL_SERVER = "CONTROLE_PERDAS_EMAIL_SERVER_CONFIG";
        private const string CONFIGURACAO_EMAIL_COORDENACAO_CONTROLE_PERDAS = "EMAIL_COORDENACAO_CONTROLE_PERDAS";
        private const string CONFIGURACAO_PERCENTUAL_RATEIO_PROCESSO_SEGURO = "PERCENTUAL_RATEIO_PROCESSO_SEGURO";
        private const double DEFAULT_PERCENTUAL_RATEIO_PROCESSO_SEGURO = 100D;
        private const int ID_CONTA_AVARIA = 2;
        private const int ID_CONTA_SINISTRO = 1;

        private TfaEmailDto _model;
        private ProcessoSeguroIntegracaoResponse _response;

        #endregion Members of ProcessoSeguroIntegracaoService

        #region Properties of ProcessoSeguroIntegracaoService
        private DespachoTranslogic Despacho { get; set; }

        private ModalSeguro ModalSeguro { get; set; }

        private TfaEmailDto Model
        {
            get
            {
                if (this.ProcessoSeguro != null || this.Payload.NumeroProcesso.HasValue)
                {
                    _model = _tfaRepository.ObterModelEmailTfa(this.ProcessoSeguro != null ? this.ProcessoSeguro.NumeroProcesso.Value : this.Payload.NumeroProcesso.Value);

                    if (!string.IsNullOrEmpty(this.Payload.Justificativa))
                    {
                        _model.Justificativa = this.Payload.Justificativa;
                    }
                }

                return _model;
            }

            set { _model = value; }
        }

        private ProcessoSeguroIntegracaoRequest Payload { get; set; }

        private ProcessoSeguro ProcessoSeguro { get; set; }

        private ProcessoSeguroIntegracaoResponse Response
        {
            get
            {
                if (_response == null)
                {
                    _response = new ProcessoSeguroIntegracaoResponse();
                    _response.ListaErros = new List<string>();
                }

                return _response;
            }

            set { _response = value; }
        }

        #endregion Properties of ProcessoSeguroIntegracaoService

        #region Methods of ProcessoSeguroIntegracaoService
        public ProcessoSeguroIntegracaoService(IProcessoSeguroService processoSeguroService,
                                    IProcessoSeguroRepository processoSeguroRepository,
                                    IModalSeguroRepository modalSeguroRepository,
                                    IHistoricoSeguroRepository historicoSeguroRepository,
                                    IRateioSeguroRepository rateioSeguroRepository,
                                    IAnaliseSeguroRepository analiseSeguroRepository,
                                    ICausaSeguroRepository causaSeguroRepository,
                                    IDespachoTranslogicRepository despachoTranslogicRepository,
                                    IItemDespachoRepository itemDespachoRepository,
                                    IContaContabilSeguroRepository contaContabilSeguroRepository,
                                    ITfaCacheRepository tfaCacheRepository,
                                    ICteRepository cteRepository,
                                    ICteDetalheRepository cteDetalheRepository,
                                    ICteEmpresasRepository cteEmpresasRepository,
                                    IClienteSeguroRepository clienteSeguroRepository,
                                    IMangaRepository mangaRepository,
                                    INotaFiscalTranslogicRepository notaFiscalRepository,
                                    NfeService nfeService,
                                    ITfaRepository tfaRepository,
                                    ITfaService tfaService,
                                    INfeProdutoReadonlyRepository nfeProdutoReadonlyRepository,
                                    INfeReadonlyRepository nfeReadonlyRepository,
                                    IUnidadeSeguroRepository unidadeSeguroRepository,
                                    IConfGeralRepository confGeralRepository,
                                    IEmpresaGrupoTfaRepository empresaGrupoTfaRepository,
                                    ILogTfaRepository logTfaRepository,
                                    IAreaOperacionalRepository areaOperacionalRepository,
                                    IUnidadeProducaoRepository unidadeProducaoRepository,
                                    IRecomendacaoToleranciaRepository recomendacaoToleranciaRepository,
                                    IRecomendacaoVagaoRepository recomendacaoVagaoRepository,
                                    ITerminalTfaEmailRepository terminalTfaEmailRepository)
        {
            _processoSeguroService = processoSeguroService;
            _processoSeguroRepository = processoSeguroRepository;
            _modalSeguroRepository = modalSeguroRepository;
            _historicoSeguroRepository = historicoSeguroRepository;
            _rateioSeguroRepository = rateioSeguroRepository;
            _analiseSeguroRepository = analiseSeguroRepository;
            _causaSeguroRepository = causaSeguroRepository;
            _despachoTranslogicRepository = despachoTranslogicRepository;
            _itemDespachoRepository = itemDespachoRepository;
            _contaContabilSeguroRepository = contaContabilSeguroRepository;
            _tfaCacheRepository = tfaCacheRepository;
            _cteRepository = cteRepository;
            _cteDetalheRepository = cteDetalheRepository;
            _cteEmpresasRepository = cteEmpresasRepository;
            _clienteSeguroRepository = clienteSeguroRepository; ;
            _mangaRepository = mangaRepository;
            _notaFiscalRepository = notaFiscalRepository;
            _tfaRepository = tfaRepository;
            _tfaService = tfaService;
            _nfeProdutoReadonlyRepository = nfeProdutoReadonlyRepository;
            _nfeReadonlyRepository = nfeReadonlyRepository;
            _unidadeSeguroRepository = unidadeSeguroRepository;
            _confGeralRepository = confGeralRepository;
            _empresaGrupoTfaRepository = empresaGrupoTfaRepository;
            _logTfaRepository = logTfaRepository;
            _areaOperacionalRepository = areaOperacionalRepository;
            _unidadeProducaoRepository = unidadeProducaoRepository;
            _recomendacaoToleranciaRepository = recomendacaoToleranciaRepository;
            _recomendacaoVagaoRepository = recomendacaoVagaoRepository;
            _terminalTfaEmailRepository = terminalTfaEmailRepository;
        }


        /// <summary>
        /// Adiciona mensagens à lista de Erros do objeto de reposta
        /// </summary>
        /// <param name="mensagem">mensagem a ser adicionada à lista</param>
        private void AdicionarMensagemResponse(string mensagem)
        {                      
            this.Response.ListaErros.Add(mensagem);
        }

        /// <summary>
        /// Método para Atualizar o TFA após o Aceite do TFA pelo usuário no CAALL
        /// </summary>
        /// <param name="payloadAtualizacao">Contrato com os parâmetros necessários para a Alteração de Processo de Seguro</param>
        /// <returns>Retorna o contrato com o status da operação (Sucesso ou Falha) e respectivos detalhes</returns>
        public AtualizarDocumentoTfaDto AtualizarDocumentoTfa(AtualizarDocumentoTfaDto payloadAtualizacao)
        {
            try
            {
                limparParametros();


                if (payloadAtualizacao != null && payloadAtualizacao.NumeroProcesso.HasValue)
                {
                    this.Payload = new ProcessoSeguroIntegracaoRequest { NumeroProcesso = payloadAtualizacao.NumeroProcesso };

                    payloadAtualizacao.TfaAtualizado = false;

                    if (_tfaService.UploadTFA(this.Payload.NumeroProcesso.Value))
                    {
                        if (payloadAtualizacao.EnviarEmail.GetValueOrDefault())
                        {
                            EnviarEmailTfa(StatusTfaEnum.ClienteCiente);
                        }
                        
                        payloadAtualizacao.TfaAtualizado = true;
                    }
                }
            }
            catch (TranslogicException ex)
            {
                AdicionarMensagemResponse(ex.Message);
            }
            catch (Exception ex)
            {
                AdicionarMensagemResponse("Erro inesperado ao Atualizar Documento TFA");
                LogarErro(ex);
            }

            if (this.Response != null && this.Response.ListaErros != null)
            {
                payloadAtualizacao.ListaErros = this.Response.ListaErros;
            }

            if (payloadAtualizacao.NumeroProcesso.HasValue)
            {
                AdicionarMensagemResponseUpload(payloadAtualizacao.NumeroProcesso.Value.ToString());            
            }

            return payloadAtualizacao;
        }


        /// <summary>
        /// Método para Atualizar de Processo de Seguro e respectivo dossiê
        /// </summary>
        /// <param name="payload">Contrato com os parâmetros necessários para a Alteração de Processo de Seguro</param>
        /// <returns>Retorna o contrato com o status da operação (Sucesso ou Falha) e respectivos detalhes</returns>
        public ProcessoSeguroIntegracaoResponse AtualizarProcesso(ProcessoSeguroIntegracaoRequest payload)
        {
            try
            {
                limparParametros();

                this.Payload = payload;

                if (!ValidarPayloadGerarProcesso())
                {
                    return this.Response;
                }

                if (this.Payload.NumeroProcesso.HasValue && this.Payload.NumeroProcesso.Value > 0)
                {
                    if (ValidarProcessoSeguro(this.Payload.NumeroProcesso.Value))
                    {
                        ExecutarFluxoProcessoSeguro(StatusTfaEnum.Alterando);
                    }
                }
                else if (this.Payload.IdTfaCache.HasValue && this.Payload.IdTfaCache.Value > 0)
                {
                    AtualizaTfaCache();
                }
            }
            catch (TranslogicException ex)
            {
                AdicionarMensagemResponse(ex.Message);
            }
            catch (AggregateException exception)
            {
                AdicionarMensagemResponse("Erro inesperado ao Atualizar Processo");
                foreach (Exception ex in exception.InnerExceptions)
                {
                    LogarErro(ex);
                }
            }
            catch (Exception ex)
            {
                AdicionarMensagemResponse("Erro inesperado ao Atualizar Processo");
                LogarErro(ex);
            }

            if (this.Payload.NumeroProcesso.HasValue)
            {
                AdicionarMensagemResponseUpload(this.Payload.NumeroProcesso.Value.ToString());
            }

            return this.Response;
        }

        /// <summary>
        /// Valida se o Processo de Seguro existe
        /// </summary>
        /// <param name="numeroProcesso">Número do Processo</param>
        /// <returns></returns>
        private bool ValidarProcessoSeguro(int numeroProcesso)
        {

            var retorno = false;
            var processoSeguro = _processoSeguroRepository.ObterPorId(numeroProcesso);

            if (processoSeguro != null)
            {
                retorno = true;
            }
            else
            {
                AdicionarMensagemResponse("Processo de Seguro não localizado");
            }

            return retorno;
        }

        /// <summary>
        /// Valida se o TfaCache à ser editado é existente e válido
        /// </summary>
        /// <returns>retorna true apenas se o TfaCache existe na base de dados</returns>
        private bool ValidarTfaCache()
        {

            var retorno = false;

            if (this.Payload != null && this.Payload.IdTfaCache.HasValue)
            {
                TfaCache tfaCache = _tfaCacheRepository.ObterPorId(this.Payload.IdTfaCache.Value);

                if (tfaCache != null)
                {
                    retorno = true;
                }
                else
                {
                    var tfa = _tfaRepository.ObterPorIdTfaCache(this.Payload.IdTfaCache.Value);

                    if (tfa != null)
                    {
                        GerarResponseProcesso(tfa.ProcessoSeguro.NumeroProcesso.Value);
                    }
                    else
                    {
                        AdicionarMensagemResponse("IdTfaCache inválido");
                    }
                }
            }
            else
            {
                AdicionarMensagemResponse("IdTfaCache inválido");
            }

            return retorno;
        }

        /// <summary>
        /// Atualiza o Valor da Mercadoria na tabela Modal Seguro
        /// </summary>
        private void AtualizarValorMercadoria()
        {
            LogTfaHelper.Trace("Start AtualizarValorMercadoria");

            if (this.ModalSeguro != null && this.Despacho != null)
            {
                this.ModalSeguro.ValorMercadoria = ObterDespachoValorUnitarioMercadoria();
                _modalSeguroRepository.AtualizarValorMercadoria(this.ModalSeguro);
            }
            LogTfaHelper.Trace("End AtualizarValorMercadoria");

        }

        /// <summary>
        /// Insere ou Atualiza o registro na tabela TFA_CACHE, de acordo com a operção (inclusão/edição de TFA)
        /// </summary>
        private void AtualizaTfaCache()
        {

            if (ValidarTfaCache())
            {
                //Valida os parâmetros do payload enviado
                InserirOuAtualizarTfaCache();

                //Valida se existe item de despacho para o número doe Despacho e número de vagão informado
                CriarDespachoTemporario();

                if (this.Despacho == null)
                {
                    AdicionarMensagemResponse("Conjunto de valores Número e Série do Despacho + Número do Vagão não encontrado na base do Translogic");

                    return;
                }


                //Não permitir inclusão de processo, caso o despacho já tenha sido informado                
                var existeProcessoDespachoSerie = _modalSeguroRepository.VerificaDespachoSerieVagaoExiste(this.Despacho.NumeroDespacho.Value, this.Despacho.SerieDespacho.SerieDespachoNum, this.Payload.NumeroVagao);

                if (existeProcessoDespachoSerie)
                {
                    AdicionarMensagemResponse("Já existe Processo de Seguro para o Número do Despacho e Vagão informado");

                    return;
                }
                

                ExecutarFluxoProcessoSeguro(StatusTfaEnum.Alterando);
            }
        }


        /// <summary>
        /// Converte o valor string/base64 para array de bytes (usado para gerar imagem da assinatura do TFA)
        /// </summary>
        /// <param name="stringBase64">valor string/base64 </param>
        /// <returns>array de bytes para geração de imagem</returns>
        private static byte[] ConverteBase64ToByteArray(string stringBase64)
        {
            byte[] imagemAssinatura = null;

            if (!string.IsNullOrEmpty(stringBase64))
            {
                imagemAssinatura = Convert.FromBase64String(stringBase64);
            }
            return imagemAssinatura;
        }

        /// <summary>
        /// Cria um instância temporária e parcial (sem estado) DespachoTranslogic 
        /// </summary>
        private void CriarDespachoTemporario()
        {

            if (this.Payload != null)
            {
                var despachoProcessoSeguroDto = _despachoTranslogicRepository.ObterDespachoProcessoSeguroDtoPorNumeroDespachoNumeroVagao(this.Payload.NumeroDespacho.Value, this.Payload.SerieDespacho, this.Payload.NumeroVagao);

                if (despachoProcessoSeguroDto != null)
                {
                    this.Despacho = new DespachoTranslogic
                    {
                        Id = despachoProcessoSeguroDto.Id,
                        NumeroDespacho = despachoProcessoSeguroDto.NumeroDespacho,
                        SerieDespacho = new SerieDespacho { SerieDespachoNum = despachoProcessoSeguroDto.Serie }
                    };
                }
            }

        }

        /// <summary>
        /// Valida regra e envia email para recomendação de manutenção de vagão, de acordo com os parâmetros configurados
        /// </summary>
        /// <param name="destinatarios">lista de destinatário para envio de recomendação de manutenção de vagão</param>
        /// <returns></returns>
        private bool EnviarEmailRecomendacaoVagao(string destinatarios)
        {

            StringBuilder destinatariosCC = new StringBuilder();

            try
            {

                var parametroEmailCoordenacao = _confGeralRepository.ObterPorId(CONFIGURACAO_EMAIL_COORDENACAO_CONTROLE_PERDAS);

                if (parametroEmailCoordenacao != null && !string.IsNullOrEmpty(parametroEmailCoordenacao.Valor))
                {
                    destinatariosCC.Append(parametroEmailCoordenacao.Valor);
                }

                var envioEmailOk = EmailTfaHelper.EnviarEmailRecomendacaoVagao(this.Model, destinatarios, destinatariosCC.ToString());

                return envioEmailOk;
            }
            catch (Exception ex)
            {
                inserirLogErroEnvioEmailTfa(StatusTfaEnum.Criando, this.Model.NumeroProcesso, ex.Message);
            }

            return false;
        }


        /// <summary>
        /// Envia email para notificação sobre Criação/Edição/Aceite de TFA
        /// </summary>
        /// <param name="statusTfa">status Criação/Edição/Aceite do TFA</param>
        private void EnviarEmailTfa(StatusTfaEnum statusTfa)
        {

            int? numeroProcesso = null;

            try
            {
                if (this.Model != null)
                {
                    numeroProcesso = this.Model.NumeroProcesso;

                    StringBuilder destinatarios = new StringBuilder();
                    StringBuilder destinatariosCC = new StringBuilder();

                    var pdfTfa = _tfaService.ObterPdfTfa(numeroProcesso.Value);

                    if (pdfTfa == null) {
                        throw new Exception("Anexo TFA não encontrado para envio de email");
                    }
                    Dictionary<string, Stream> anexos = null;

                    anexos = new Dictionary<string, Stream>();

                    anexos.Add(string.Format("TFA_{0}.pdf", numeroProcesso), pdfTfa);

                    var listaEmailCliente = _empresaGrupoTfaRepository.ObterListaEmailTfa(numeroProcesso.Value);

                    if (listaEmailCliente != null && listaEmailCliente.Count > 0)
                    {
                        destinatarios.Append(String.Join(";", listaEmailCliente));
                    }

                    var parametroEmailCoordenacao = _confGeralRepository.ObterPorId(CONFIGURACAO_EMAIL_COORDENACAO_CONTROLE_PERDAS);

                    if (parametroEmailCoordenacao != null && !string.IsNullOrEmpty(parametroEmailCoordenacao.Valor))
                    {
                        destinatariosCC.Append(parametroEmailCoordenacao.Valor);
                    }

                    if (this.Payload != null & this.Payload.IdLocalVistoria.HasValue)
                    {
                        var listaEmailTerminal = _terminalTfaEmailRepository.ObterTodosPorIdTerminalDto(Payload.IdLocalVistoria.Value);

                        if (listaEmailTerminal != null && listaEmailTerminal.Count > 0)
                        {
                            var lista = listaEmailTerminal.Select(x => x.Email).ToList();
                            
                            if (destinatariosCC.Length > 0)
                                destinatariosCC.Append(";");

                            destinatariosCC.Append(String.Join(";", lista));
                        }
                    }

                    var envioEmailOk = EmailTfaHelper.EnviarEmailTfa(statusTfa, this.Model, destinatarios.ToString(), destinatariosCC.ToString(), anexos);

                    if (!envioEmailOk)
                    {
                        AdicionarMensagemResponse("Erro ao enviar e-mail de notificação sobre criação/alteração de TFA");
                    }
                }
            }
            catch (Exception ex)
            {
                inserirLogErroEnvioEmailTfa(statusTfa, numeroProcesso.GetValueOrDefault(), ex.Message);
            }
        }


        /// <summary>
        /// Executa o Fluxo de Criação/Edição/Aceite de Processo de Seguro
        /// </summary>
        /// <param name="statusOperacao">status de Criação/Edição/Aceite do Processo de Seguro</param>
        private void ExecutarFluxoProcessoSeguro(StatusTfaEnum statusOperacao)
        {
            LogTfaHelper.Trace("Start ExecutarFluxoProcessoSeguro");

            //Valida se existe item de despacho para o número doe Despacho e número de vagão informado
            CriarDespachoTemporario();

            //Assumindo a premissa que o payload não gerou divergências, INCLUI o processo na base de dados
            InserirOuAtualizarProcessoSeguro();

            if (this.ProcessoSeguro != null)
            {
                GerarDossie();

                if (this.Payload.IdTfaCache.HasValue)
                {
                    _tfaCacheRepository.Remover(this.Payload.IdTfaCache.Value);
                }

                StatusTfaEnum status = (this.Payload.ResponsavelCiente.HasValue && !this.Payload.ResponsavelCiente.Value) ? StatusTfaEnum.ClienteNaoCiente : statusOperacao;

                EnviarEmailTfa(status);

                RecomendarManutencaoVagao();

                GerarResponseProcesso(this.ProcessoSeguro.NumeroProcesso.Value);
            }
            LogTfaHelper.Trace("End ExecutarFluxoProcessoSeguro");

        }

        private void GerarResponseProcesso(int numeroProcesso)
        {

            //WORKAROUND!! NHibernate - LAZY LOADING PROBLEM -Initializing[]-Could not initialize proxy - no Session."}
            var processoSeguro = _processoSeguroRepository.ObterPorId(numeroProcesso);
            var modalSeguro = _modalSeguroRepository.ObterPorProcesso(processoSeguro);

            if (this.Response == null)
                this.Response = new ProcessoSeguroIntegracaoResponse();

            this.Response.NumeroProcesso = processoSeguro.NumeroProcesso.Value;
            this.Response.CodMercadoria = modalSeguro.CodMercadoria;
            this.Response.ProcessoGerado = true;

            if (processoSeguro.ClienteSeguro != null)
            {
                this.Response.NomeCliente = processoSeguro.ClienteSeguro.Descricao;
            }
            else
            {
                AdicionarMensagemResponse("Cliente Seguro não cadastrado na base de dados do Translogic.");
            }
        }



        /// <summary>
        /// Gera o dossiê do processo de seguro
        /// </summary>
        private void GerarDossie()
        {
            LogTfaHelper.Trace("Start GerarDossie");

            bool incluindo = !(this.Payload.NumeroProcesso.HasValue && this.Payload.NumeroProcesso.Value > 0);

            InserirOuAtualizarTfa();

            AtualizarValorMercadoria();

            if (incluindo)
            {
                var idItemDespacho = _itemDespachoRepository.ObterIDItemDespachoPorIdDespacho(this.Despacho.Id.Value);

                if (idItemDespacho.HasValue)
                {
                    try
                    {
                        var responseMessage = new List<string>();
                        _processoSeguroService.GerarDossie(this.ProcessoSeguro.Id,
                                                        new ItemDespacho
                                                        {
                                                            Id = idItemDespacho,
                                                            Vagao = new Vagao { Codigo = this.Payload.NumeroVagao },
                                                            DespachoTranslogic = this.Despacho
                                                        });

                    }
                    catch (AggregateException exception)
                    {
                        AdicionarMensagemResponse("Erro inesperado ao Gerar Dossiê");
                        foreach (Exception ex in exception.InnerExceptions)
                        {
                            LogarErro(ex);
                        }
                    }
                    catch (Exception ex)
                    {
                        AdicionarMensagemResponse(ex.Message);
                        LogarErro(ex);
                    }
                }
            }
            else
            {
                _tfaService.UploadTFA(this.Payload.NumeroProcesso.Value);
            }

            LogTfaHelper.Trace("End GerarDossie");

        }

        private void AdicionarMensagemResponseUpload(string numeroProcesso)
        {
            if (LogTfaHelper.responseMessage.ContainsKey(numeroProcesso))
            {
                LogTfaHelper.responseMessage[numeroProcesso].ForEach(m => AdicionarMensagemResponse(m));

                LogTfaHelper.responseMessage.Remove(numeroProcesso);
            }
        }

        /// <summary>
        /// Atualiza o dossiê do processo de seguro
        /// </summary>
        public AtualizarDocumentoTfaDto AtualizarDossie(AtualizarDocumentoTfaDto payload)
        {

            try
            {
                if (payload != null && payload.NumeroProcesso.HasValue)
                {
                    _processoSeguroService.GerarDossie(payload.NumeroProcesso.Value);

                    if (payload.EnviarEmail.GetValueOrDefault())
                    {
                        EnviarEmailTfa(StatusTfaEnum.ClienteCiente);
                    }

                    payload.TfaAtualizado = true;
                }
            }
            catch (AggregateException exception)
            {
                AdicionarMensagemResponse("Erro inesperado ao Atualizar Dossiê");
                foreach (Exception ex in exception.InnerExceptions)
                {
                    LogarErro(ex);
                }
            }
            catch (Exception ex)
            {
                AdicionarMensagemResponse(ex.Message);
                LogarErro(ex);
            }

            if (this.Response != null && this.Response.ListaErros != null)
            {
                payload.ListaErros = this.Response.ListaErros;
            }

            if (payload.NumeroProcesso.HasValue)
            {
                AdicionarMensagemResponseUpload(payload.NumeroProcesso.Value.ToString());
            }
            return payload;
        }

        /// <summary>
        /// Método para Atualizar de Processo de Seguro e respectivo dossiê
        /// </summary>
        /// <param name="payload">Contrato com os parâmetros necessários para a Alteração de Processo de Seguro</param>
        /// <returns>Retorna o contrato com o status da operação (Sucesso ou Falha) e respectivos detalhes</returns>
        public ProcessoSeguroIntegracaoResponse GerarProcesso(ProcessoSeguroIntegracaoRequest payload)
        {
            try
            {
                LogTfaHelper.Trace("Start Gerar Processo");
                limparParametros();


                this.Payload = payload;

                //Valida os parâmetros do payload enviado
                if (!ValidarPayloadGerarProcesso())
                {
                    return this.Response;
                }

                LogTfaHelper.Trace("Gerar Processo", null, payload.NumeroDespacho, payload.SerieDespacho, payload.NumeroVagao);

                //Valida se existe item de despacho para o número doe Despacho e número de vagão informado
                CriarDespachoTemporario();

                if (this.Despacho == null)
                {
                    AdicionarMensagemResponse("Conjunto de valores Número e Série do Despacho + Número do Vagão não encontrado na base do Translogic");
                    InserirOuAtualizarTfaCache();
                    return this.Response;
                }



                //Não permitir inclusão de processo, caso o despacho já tenha sido informado
                //Caso esteja em modo de inclusão, valida se já existe processo criado com o número de Despacho                
                if (!this.Payload.ProcessoGerado.Value && !this.Payload.ModoEdicao.Value)
                {
                    var existeProcessoDespachoSerie = _modalSeguroRepository.VerificaDespachoSerieVagaoExiste(this.Despacho.NumeroDespacho.Value, this.Despacho.SerieDespacho.SerieDespachoNum, this.Payload.NumeroVagao);

                    if (existeProcessoDespachoSerie)
                    {
                        AdicionarMensagemResponse("Já existe Processo de Seguro para o Número do Despacho e Vagão informado");

                        InserirOuAtualizarTfaCache();
                        return this.Response;
                    }
                }

                ExecutarFluxoProcessoSeguro(StatusTfaEnum.Criando);


            }
            catch (TranslogicException ex)
            {
                AdicionarMensagemResponse(ex.Message + ex.StackTrace);
            }
            catch (AggregateException exception)
            {
                AdicionarMensagemResponse("Erro inesperado ao Gerar Processo");
                foreach (Exception ex in exception.InnerExceptions)
                {
                    LogarErro(ex);
                }
            }
            catch (Exception ex)
            {
                AdicionarMensagemResponse("Erro inesperado ao Gerar Processo");
                LogarErro(ex);
            }

            if (this.Response.NumeroProcesso.HasValue)
            {
                AdicionarMensagemResponseUpload(this.Response.NumeroProcesso.ToString());
            }
            
            LogTfaHelper.Trace("End Gerar Processo", null, payload.NumeroDespacho, payload.SerieDespacho, payload.NumeroVagao);

            return this.Response;
        }


        /// <summary>
        /// Cria o TFA automaticamente, de acordo com o número do Processo informado
        /// </summary>
        /// <param name="numeroProcesso">Número do Processo para Geração automática do TFA</param>
        /// <returns></returns>
        public AtualizarDocumentoTfaDto CriarTfaProcesso(AtualizarDocumentoTfaDto payload)
        {
            try
            {
                limparParametros();

                if (payload != null && payload.NumeroProcesso.HasValue)
                {
                    var tfa = _tfaRepository.CriarTfaProcesso(payload.NumeroProcesso.Value);

                    payload.TfaAtualizado = (tfa != null);
                    
                }
            }
            catch (TranslogicException ex)
            {
                AdicionarMensagemResponse(ex.Message);
            }
            catch (Exception ex)
            {
                AdicionarMensagemResponse("Erro inesperado ao Atualizar Documento TFA");
                LogarErro(ex);
            }

            if (this.Response != null && this.Response.ListaErros != null)
            {
                payload.ListaErros = this.Response.ListaErros;
            }

            AdicionarMensagemResponseUpload(payload.NumeroProcesso.Value.ToString());
            return payload;
        }

        /// <summary>
        /// Insere log de possíveis erros no envio de e-mails do módulo de controle de perdas
        /// </summary>
        /// <param name="statusTfa"></param>
        /// <param name="numeroProcesso"></param>
        /// <param name="error"></param>
        private void inserirLogErroEnvioEmailTfa(StatusTfaEnum statusTfa, int numeroProcesso, string error)
        {

            var tfa = _tfaRepository.ObterPorNumProcesso(numeroProcesso);

            if (tfa == null)
            {
                throw new TranslogicException(string.Format("Erro ao inserir log de Email TFA. TFA não encontrado para o Processo {0}", numeroProcesso));
            }

            var logTfa = new LogTfa { IdTfa = tfa.Id, VersionDate = DateTime.Now, Descricao = error };

            switch (statusTfa)
            {
                case StatusTfaEnum.Criando:
                    logTfa.CodTipoLogTfa = (int)TiPoLogTfaEnum.ErroEnvioEmailNovoTfa;
                    break;
                case StatusTfaEnum.Alterando:
                    logTfa.CodTipoLogTfa = (int)TiPoLogTfaEnum.ErroEnvioEmailEditarTfa;
                    break;
                case StatusTfaEnum.ClienteCiente:
                    logTfa.CodTipoLogTfa = (int)TiPoLogTfaEnum.ErroEnvioEmailAceiteTfa;
                    break;
                default:
                    break;
            }

            _logTfaRepository.Inserir(logTfa);
        }


        /// <summary>
        /// Insere ou atualiza Processo de Seguro
        /// </summary>
        private void InserirOuAtualizarProcessoSeguro()
        {
            LogTfaHelper.Trace("Start ExecutarFluxoProcessoSeguro");

            ProcessoSeguro processoSeguro = null;
            ModalSeguro modalSeguro = null;
            RateioSeguro rateioSeguro = null;
            HistoricoSeguro historicoSeguro = new HistoricoSeguro();


            var processoCriado = false;
            var historicoCriado = false;
            var modalCriado = false;
            var rateioCriado = false;
            bool incluindoProcesso = !(this.Payload.NumeroProcesso.HasValue && this.Payload.NumeroProcesso.Value > 0);

            try
            {
                //Inicializa as entidades, considerando o estado de inclusão ou alteração de dados do processo
                processoSeguro = incluindoProcesso ? new ProcessoSeguro() : _processoSeguroRepository.ObterPorId(this.Payload.NumeroProcesso.Value);
                modalSeguro = incluindoProcesso ? new ModalSeguro() : _modalSeguroRepository.ObterPorProcesso(processoSeguro);
                rateioSeguro = incluindoProcesso ? new RateioSeguro() : _rateioSeguroRepository.ObterPorProcesso(processoSeguro);

                //Ignora o peso informado no aplicativo e recupera o peso no detalhamento da carga
                //double pesoCarregamento = _despachoTranslogicRepository.ObterPesoCarregamento(this.Despacho.NumeroDespacho.Value, this.Despacho.SerieDespacho.SerieDespachoNum);
                
                //double perdaEmToneladas = this.Payload.PesoOrigem.Value - this.Payload.PesoDestino.Value;
                //double perdaEmToneladas = pesoCarregamento - this.Payload.PesoDestino.Value;

                var cte = ObterCte();

                if (cte == null)
                {
                    throw new TranslogicException("Nenhum CTE cadastrado para o despacho e vagão informados!");
                }

                var listaCteDetalhe = _cteDetalheRepository.ObterPorCte(cte);

                string nota = string.Empty;
                double peso = 0;
                foreach (CteDetalhe cteDetalhe in listaCteDetalhe)
                {
                    if (nota == string.Empty)
                    {
                        nota = cteDetalhe.NumeroNota;
                    }
                    else
                    {
                        nota += ";" + cteDetalhe.NumeroNota;
                    }

                    peso += cteDetalhe.PesoNotaFiscal;
                }

                double perdaEmToneladas = this.Payload.PesoDestino.Value;//peso - this.Payload.PesoDestino.Value; gestor solicitou alteração: campo pesodestino agora recebe perda em  toneladas

                var cteEmpresa = _cteEmpresasRepository.ObterPorCte(cte);

                ClienteSeguro clienteSeguro = null;

                if (cteEmpresa != null)
                {
                    clienteSeguro = _clienteSeguroRepository.ObterClientePorSigla(cteEmpresa.EmpresaTomadora.Sigla);
                }
                //else
                //{
                //    throw new TranslogicException("Cliente Seguro não encontrado para o despacho e vagão informados!");
                //}

                //E2: Valor do peso superior ao peso do vagão
                var manga = Convert.ToDouble(_mangaRepository.ObterPesoMangaPorVagao(cte.Vagao.Codigo));

                if (perdaEmToneladas > manga && manga != 0)
                {
                    throw new TranslogicException("Peso não permitido, não pode ser superior ao peso do vagão.");
                }

                //E3: Valores Negativos
                if (perdaEmToneladas < 0)
                {
                    throw new TranslogicException("Valores negativos não permitidos para peso de Perda.");
                }

                var causaSeguro = _causaSeguroRepository.ObterPorId(this.Payload.IdCausaDano.Value);
                if (causaSeguro == null)
                {
                    throw new TranslogicException("Identificação do Dano não encontrada na base de dados.");
                }

                this.Payload.PesoOrigem = peso;

                var idContaContabil = causaSeguro.TipoSinistro == "S" ? ID_CONTA_SINISTRO : ID_CONTA_AVARIA;

                //*****Inclusão/Alteração do Processo******
                if (incluindoProcesso)
                {
                    processoSeguro.Data = DateTime.Now;
                    processoSeguro.DataCadastro = DateTime.Now;
                    processoSeguro.DataVistoria = this.Payload.DataHoraVistoria.ConvertToDateTime();
                    processoSeguro.DataProcesso = DateTime.Now;
                    processoSeguro.UsuarioCadastro = this.Payload.MatriculaVistoriador;
                    processoSeguro.Vistoriador = this.Payload.NomeVistoriador;
                    processoSeguro.IndicadorSituacao = "P";
                }

                processoSeguro.ClienteSeguro = clienteSeguro;
                processoSeguro.DataSinistro = this.Payload.DataHoraSinistro.ConvertToDateTime();
                processoSeguro.IndicadorIndevido = Enum<IndicadorProcessoIndevidoEnum>.GetDescriptionOf(((IndicadorProcessoIndevidoEnum)this.Payload.CodTipoConclusaoTfa.Value));
                processoSeguro.Sindicancia = this.Payload.Sindicancia;
                processoSeguro.ContaContabil = _contaContabilSeguroRepository.ObterPorId(idContaContabil);

                
                //*****Inclusão do Histórico do Processo******                
                historicoSeguro.TimeStamp = DateTime.Now;
                historicoSeguro.ProcessoSeguro = processoSeguro;
                historicoSeguro.Descricao = incluindoProcesso ? this.Payload.ConsideracoesAdicionais : this.Payload.Justificativa;
                historicoSeguro.Usuario = this.Payload.MatriculaVistoriador;
                

                //*****Inclusão/Alteração do Modal do Processo******
                if (incluindoProcesso)
                {
                    modalSeguro.Data = DateTime.Now;
                    modalSeguro.Usuario = this.Payload.MatriculaVistoriador;
                    
                    if (this.Payload.NomeVistoriador.Length > 30)
                    {
                        modalSeguro.ResponsavelTermo = this.Payload.NomeVistoriador.Substring(0, 30);
                    }
                    else
                    {
                        modalSeguro.ResponsavelTermo = this.Payload.NomeVistoriador;
                    }

                    modalSeguro.Historico = this.Payload.ConsideracoesAdicionais;
                }
                modalSeguro.ProcessoSeguro = processoSeguro;
                modalSeguro.CausaSeguro = causaSeguro;
                modalSeguro.CodFluxo = cte.FluxoComercial.Codigo;
                modalSeguro.CodMercadoria = cte.FluxoComercial.Mercadoria.Codigo;
                modalSeguro.DataTermo = this.Payload.DataHoraSinistro.ConvertToDateTime();
                modalSeguro.Despacho = this.Despacho.NumeroDespacho;
                modalSeguro.Destino = cte.FluxoComercial.Destino.Id;
                modalSeguro.DestinoFluxo = cte.FluxoComercial.Destino.Id;
                modalSeguro.Gambit = Enum<TipoGambitoTfaEnum>.GetDescriptionOf(((TipoGambitoTfaEnum)this.Payload.CodTipoGambito.Value));
                modalSeguro.NotaFiscal = nota;
                modalSeguro.Origem = cte.FluxoComercial.Origem.Id;
                modalSeguro.OrigemFluxo = cte.FluxoComercial.Origem.Id;
                modalSeguro.QuantidadePerda = perdaEmToneladas;
                modalSeguro.Serie = this.Despacho.SerieDespacho.SerieDespachoNum;
                //Conforme solicitado pelo usuário, o campo ficará nulo quando criado pelo aplicativo
                //modalSeguro.Termo = processoSeguro.Id.ToString();
                modalSeguro.Terminal = this.Payload.IdLocalVistoria;
                modalSeguro.Vagao = cte.Vagao;
                //Atualizado posteriormente
                //modalSeguro.ValorMercadoria = processoCache.ValorMercadoria;
                modalSeguro.Lacre = Enum<TipoLacreTfaEnum>.GetDescriptionOf(((TipoLacreTfaEnum)this.Payload.CodTipoLacre.Value)); ;
                modalSeguro.Piscofins = cte.FluxoComercial.Contrato.ValorAliquotaPisProduto + cte.FluxoComercial.Contrato.ValorAliquotaCofinsProduto;


                //*****Inclusão/Alteração do Rateio do Processo******
                if (incluindoProcesso)
                {
                    rateioSeguro.Data = DateTime.Now;
                    rateioSeguro.CodUsuario = this.Payload.MatriculaVistoriador;
                    rateioSeguro.ProcessoSeguro = processoSeguro;
                    rateioSeguro.CentroCusto = "";
                }
                rateioSeguro.PercentualRateio = ObterPercentualRateio();
                rateioSeguro.UnidadeSeguro = _unidadeSeguroRepository.ObterUnidadeSeguroPorIdTerminal(this.Payload.IdLocalVistoria.Value);

                

                //*****Inclusão da Análise do Processo******
                if (incluindoProcesso)
                {                                  
                    _processoSeguroRepository.Inserir(processoSeguro);
                    processoSeguro.NumeroProcesso = processoSeguro.Id;
                    _processoSeguroRepository.Atualizar(processoSeguro);
                    processoCriado = true;

                    _historicoSeguroRepository.Inserir(historicoSeguro);
                    historicoCriado = true;

                    _modalSeguroRepository.Inserir(modalSeguro);
                    modalCriado = true;

                    _rateioSeguroRepository.Inserir(rateioSeguro);
                    rateioCriado = true;

                    AnaliseSeguro analiseSeguro = new AnaliseSeguro();
                    analiseSeguro.Data = DateTime.Now;
                    analiseSeguro.IndicadorSituacao = "A";
                    analiseSeguro.ProcessoSeguro = processoSeguro;
                    _analiseSeguroRepository.Inserir(analiseSeguro);
                }
                else
                {
                    _processoSeguroRepository.Atualizar(processoSeguro);
                    processoCriado = true;

                    _modalSeguroRepository.Atualizar(modalSeguro);
                    modalCriado = true;

                    _rateioSeguroRepository.Atualizar(rateioSeguro);
                    rateioCriado = true;

                }

                this.ProcessoSeguro = processoSeguro;
                this.ModalSeguro = modalSeguro;
                LogTfaHelper.Trace("Processo Seguro Criado", this.ProcessoSeguro.NumeroProcesso, this.Payload.NumeroDespacho, this.Payload.SerieDespacho, this.Payload.NumeroVagao);


            }
            catch (TranslogicException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                if (historicoCriado)
                    _historicoSeguroRepository.Remover(historicoSeguro);

                if (modalCriado && incluindoProcesso)
                    _modalSeguroRepository.Remover(modalSeguro);

                if (rateioCriado && incluindoProcesso)
                    _rateioSeguroRepository.Remover(rateioSeguro);

                if (processoCriado && incluindoProcesso)
                    _processoSeguroRepository.Remover(processoSeguro);

                LogarErro(ex);
                throw new TranslogicException("Erro ao Incluir o Processo de Seguro.");
            }
            LogTfaHelper.Trace("End ExecutarFluxoProcessoSeguro");

        }


        /// <summary>
        /// Insere ou atualiza registro de TFA
        /// </summary>
        private void InserirOuAtualizarTfa()
        {
            LogTfaHelper.Trace("Start InserirOuAtualizarTfa");

            bool incluindo = !(this.Payload.NumeroProcesso.HasValue && this.Payload.NumeroProcesso.Value > 0);

            Translogic.Modules.Core.Domain.Model.Tfa.Tfa tfa = null;

            tfa = incluindo ? new Translogic.Modules.Core.Domain.Model.Tfa.Tfa() : _tfaRepository.ObterPorNumProcesso(this.Payload.NumeroProcesso.Value);
            
            if (tfa == null)
            {
                tfa = new Translogic.Modules.Core.Domain.Model.Tfa.Tfa();
            }

            if (incluindo)
            {
                tfa.MatriculaVistoriador = this.Payload.MatriculaVistoriador;
            }

            tfa.ProcessoSeguro = this.ProcessoSeguro;
            tfa.NomeRepresentante = this.Payload.NomeRepresentante;
            tfa.DocumentoRepresentante = this.Payload.DocumentoRepresentante;
            tfa.ImagemAssinaturaRepresentante = ConverteBase64ToByteArray(this.Payload.ImagemAssinaturaRepresentante);
            tfa.AssinaturaRepresentante = Convert.ToBoolean(this.Payload.ResponsavelCiente);
            tfa.PesoDestino = this.Payload.PesoOrigem - this.Payload.PesoDestino; //mudança solicitada pelo gestor: Pesodestino agora recebe a quantidade de perda em toneladas
            tfa.OrigemProcessoApp = true;
            tfa.StatusClienteCiente = Convert.ToBoolean(this.Payload.ResponsavelCiente);
            tfa.TipoConclusaoTfa = new TipoConclusaoTfa { Id = this.Payload.CodTipoConclusaoTfa.Value };
            tfa.TipoLacreTfa = new TipoLacreTfa { Id = this.Payload.CodTipoLacre.Value };
            tfa.TipoGambitoTfa = new TipoGambitoTfa { Id = this.Payload.CodTipoGambito.Value };
            tfa.DataClienteCiente = this.Payload.ResponsavelCiente.Value ? this.Payload.DataHoraVistoria.ConvertToDateTime() : null;
            tfa.TipoLiberadorDescarga = new TipoLiberadorDescarga { Id = this.Payload.CodTipoLiberadorDescarga.Value };

            if (this.Payload.IdTfaCache.HasValue)
                tfa.IdTfaCache = this.Payload.IdTfaCache.Value;

            _tfaRepository.InserirOuAtualizar(tfa);
            LogTfaHelper.Trace("End InserirOuAtualizarTfa");

        }

        /// <summary>
        /// Insere ou atualiza registro de TFA_CACHE
        /// </summary>
        private TfaCache InserirOuAtualizarTfaCache()
        {

            bool incluindo = !(this.Payload.IdTfaCache.HasValue && this.Payload.IdTfaCache.Value > 0);

            TfaCache tfaCache = incluindo ? new TfaCache() : _tfaCacheRepository.ObterPorId(this.Payload.IdTfaCache.Value);

            if (incluindo)
            {
                tfaCache.DataHoraInclusao = DateTime.Now;
                tfaCache.MatriculaVistoriador = this.Payload.MatriculaVistoriador;
                tfaCache.NomeVistoriador = this.Payload.NomeVistoriador;
            }

            tfaCache.NumeroVagao = this.Payload.NumeroVagao;
            tfaCache.NumeroDespacho = this.Payload.NumeroDespacho.Value;
            tfaCache.SerieDespacho = this.Payload.SerieDespacho;
            tfaCache.PesoOrigem = this.Payload.PesoOrigem.Value;
            tfaCache.PesoDestino = this.Payload.PesoOrigem.Value - this.Payload.PesoDestino.Value;
            tfaCache.IdLocalVistoria = this.Payload.IdLocalVistoria.Value;
            tfaCache.IdCausaDano = this.Payload.IdCausaDano.Value;
            tfaCache.CodTipoLacre = this.Payload.CodTipoLacre.Value;
            tfaCache.CodTipoGambito = this.Payload.CodTipoGambito.Value;
            tfaCache.CodTipoConclusaoTfa = this.Payload.CodTipoConclusaoTfa.Value;
            tfaCache.ConsideracoesAdicionais = this.Payload.ConsideracoesAdicionais;
            tfaCache.NomeRepresentante = this.Payload.NomeRepresentante;
            tfaCache.DocumentoRepresentante = this.Payload.DocumentoRepresentante;
            tfaCache.ImgAssinaturaRepresentante = ConverteBase64ToByteArray(this.Payload.ImagemAssinaturaRepresentante);
            tfaCache.ResponsavelCiente = Convert.ToInt16(this.Payload.ResponsavelCiente.Value);
            tfaCache.DataHoraSinistro = this.Payload.DataHoraSinistro.ConvertToDateTime().Value;
            tfaCache.DataHoraVistoria = this.Payload.DataHoraVistoria.ConvertToDateTime().Value;
            tfaCache.EdicaoManual = Convert.ToInt16(this.Payload.EdicaoManual.Value);
            tfaCache.CodTipoLiberadorDescarga = this.Payload.CodTipoLiberadorDescarga;
            tfaCache.Sindicancia = this.Payload.Sindicancia;
            tfaCache.MatriculaUsuarioAlteracao = this.Payload.MatriculaVistoriador;
            tfaCache.DataHoraAlteracao = DateTime.Now;

            tfaCache = _tfaCacheRepository.InserirOuAtualizar(tfaCache);

            this.Response.IdTfaCache = tfaCache.Id;

            return tfaCache;
        }

        /// <summary>
        /// Limpa todos os campos antes da execução do request
        /// </summary>
        private void limparParametros()
        {
            this.Despacho = null;
            this.ModalSeguro = null;
            this.Model = null;
            this.Payload = null;
            this.ProcessoSeguro = null;
            this.Response = null;
        }

        /// <summary>
        /// Loga possíveis exceções ocorridas na criação/edição/aceite de TFA
        /// </summary>
        /// <param name="ex"></param>
        private void LogarErro(Exception ex)
        {
            string innerException = "";
            if (ex.InnerException != null)
            {
                innerException = ex.InnerException.Message;
            }
            var log = new LogTfa { Descricao = this.Payload.ToString() + "\n" + ex.Message + "\n" + ex.StackTrace + "\n" + innerException, VersionDate = DateTime.Now };
            log.Descricao = log.Descricao.Length > 2000 ? log.Descricao.Substring(0, 2000) : log.Descricao;
            _logTfaRepository.Inserir(log);            
        }

        /// <summary>
        /// método útil para obtenção do CTE vinculado ao Despacho/vagão
        /// </summary>
        /// <returns></returns>
        private Cte ObterCte()
        {

            return _cteRepository.ObterCtesPorDespacho(this.Despacho.Id.Value).FirstOrDefault();
        }

        /// <summary>
        /// Método para obter o valor da mercadoria do despacho
        /// </summary>
        /// <param name="despacho">Despacho</param>
        private double ObterDespachoValorUnitarioMercadoria()
        {

            double valorProdutoUnitario = 0;
            double somaValorProdutoNotas = 0;
            double valorMercadoriaUnitario = 0;

            try
            {


                if (this.Despacho != null)
                {
                    //notas = _notaFiscalRepository.ObterNotaFiscalPorDespacho(despacho);
                    var notas = _notaFiscalRepository.ObterNotaFiscalPorDespacho(new DespachoTranslogic { Id = this.Despacho.Id });

                    if (notas != null && notas.Any())
                    {
                        List<String> chavesNfe = notas.Select(i => i.ChaveNotaFiscalEletronica).ToList<string>();

                        //notasFiscais = _nfeService.ObterDadosNfesBancoDados(chavesNfe);
                        var notasFiscais = _nfeReadonlyRepository.ObterPorChavesNfeSemEstado(chavesNfe);

                        var listNfeLida = new StringCollection();
                        foreach (var nf in notasFiscais)
                        {
                            if (!listNfeLida.Contains(nf.ChaveNfe))
                            {
                                listNfeLida.Add(nf.ChaveNfe);

                                //nfeProdutos = _nfeService.ObterProdutosNfe(nf);
                                var nfeProdutos = _nfeProdutoReadonlyRepository.ObterPorTipoIdSemEstado(nf.Id.Value, nf.Tipo);

                                if (nfeProdutos != null)
                                {
                                    foreach (var prod in nfeProdutos)
                                    {
                                        if (valorProdutoUnitario.Equals(0))
                                        {
                                            valorProdutoUnitario = prod.ValorUnitarioComercializacao;
                                            // Se estiver em KG, multiplica por 1000 para obter valor em toneladas
                                            if (arrayKg.Contains(prod.UnidadeComercial.ToLower()))
                                                valorProdutoUnitario = valorProdutoUnitario * 1000;
                                        }
                                    }

                                    somaValorProdutoNotas += valorProdutoUnitario;
                                    valorProdutoUnitario = 0;
                                }
                            }
                        }


                        // Cálculo da média caso tenha 2 notas ou mais
                        somaValorProdutoNotas = (somaValorProdutoNotas / notas.Count());
                        valorMercadoriaUnitario = Math.Round(somaValorProdutoNotas, 2);
                    }
                }
            }
            catch
            {
                AdicionarMensagemResponse("Erro ao obter valor da mercadoria");
                throw new TranslogicException("Erro ao obter valor da mercadoria");
            }

            return valorMercadoriaUnitario;
        }

        /// <summary>
        /// Obtem a malha vinculada ao Despacho
        /// </summary>
        /// <returns></returns>
        private string ObterMalha()
        {

            string codMalha = null;

            if (this.Despacho != null)
            {
                var cte = ObterCte();

                int? idAreaOperacional = null;

                if (cte != null)
                {
                    idAreaOperacional = cte.FluxoComercial.DestinoIntercambio != null ? cte.FluxoComercial.DestinoIntercambio.Id : cte.FluxoComercial.Destino.Id;
                }

                if (idAreaOperacional.HasValue)
                {
                    var areaOperacional = _areaOperacionalRepository.ObterPorId(idAreaOperacional.Value);

                    if (areaOperacional != null && areaOperacional.UnidadeProducao != null)
                    {
                        var unidadeProducao = _unidadeProducaoRepository.ObterPorId(areaOperacional.UnidadeProducao.Id.Value);

                        if (unidadeProducao != null)
                        {
                            codMalha = unidadeProducao.Malha.Codigo;
                        }
                    }
                }
            }

            return codMalha;
        }

        /// <summary>
        /// Obtem o percentual de rateio (ver tabela RATEIO_SEGURO) configurado na tabela de parâmetros
        /// </summary>
        /// <returns></returns>
        private double ObterPercentualRateio()
        {

            double percentualRateio = DEFAULT_PERCENTUAL_RATEIO_PROCESSO_SEGURO;

            var valorParametro = _confGeralRepository.ObterPorId(CONFIGURACAO_PERCENTUAL_RATEIO_PROCESSO_SEGURO);

            if (valorParametro != null && !string.IsNullOrEmpty(valorParametro.Valor))
            {
                if (double.TryParse(valorParametro.Valor, out percentualRateio))
                {
                    return percentualRateio;
                }

            }

            return DEFAULT_PERCENTUAL_RATEIO_PROCESSO_SEGURO;
        }
        

        /// <summary>
        /// Verifica regras e parâmetros para recomendação de manutenção de vagão
        /// </summary>
        private void RecomendarManutencaoVagao()
        {

            string emailRecomendacao = null;

            var recomendacaoVagao = _recomendacaoVagaoRepository.ObterPorIdCausaSeguro(this.Payload.IdCausaDano.Value);

            if (recomendacaoVagao != null && recomendacaoVagao.Recomendacao)
            {
                var codMalha = ObterMalha();

                if (!string.IsNullOrEmpty(codMalha))
                {
                    var recomendacaoTolerancia = _recomendacaoToleranciaRepository.ObterPodCodMalha(codMalha);

                    if (recomendacaoTolerancia != null)
                    {
                        emailRecomendacao = recomendacaoTolerancia.Email;

                        if (recomendacaoVagao.Tolerancia)
                        {
                            double perdaEmToneladas = this.Payload.PesoDestino.Value; //solicitação do gestor: pesodestino agora recebe perda em toneladas

                            if (perdaEmToneladas <= recomendacaoTolerancia.Valor)
                            {
                                return;
                            }
                        }

                        if (!EnviarEmailRecomendacaoVagao(emailRecomendacao))
                        {
                            AdicionarMensagemResponse("Erro ao enviar e-mail de Recomendação de Manutenção de Vagão");
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Métodp para validação do payload de entrada para a geração de processo de Seguro
        /// </summary>
        /// <param name="listaErros">Parâmetro out com referência de objeto para retornar lista de possíveis erros encontrados na validação</param>
        /// <returns></returns>
        private bool ValidarPayloadGerarProcesso()
        {

            // to ensure "payload" is not null
            if (this.Payload == null)
            {
                AdicionarMensagemResponse("Payload/Contrato de Serviço não informado");
            }

            // to ensure "NumeroVagao" is required (not null)
            if (this.Payload.NumeroVagao == null)
            {
                AdicionarMensagemResponse("Número do Vagão não informado");
            }

            // to ensure "NumeroDespacho" is required (not null)
            if (this.Payload.NumeroDespacho == null)
            {
                AdicionarMensagemResponse("Número do Despacho não informado");
            }

            // to ensure "SerieDespacho" is required (not null)
            if (this.Payload.SerieDespacho == null)
            {
                AdicionarMensagemResponse("Série do Despacho não informado");
            }

            // to ensure "PesoOrigem" is required (not null)
            if (this.Payload.PesoOrigem == null)
            {
                AdicionarMensagemResponse("Peso Origem não informado");
            }

            // to ensure "PesoDestino" is required (not null)
            if (this.Payload.PesoDestino == null)
            {
                AdicionarMensagemResponse("Peso Destino não informado");
            }

            // to ensure "IdLocalVistoria" is required (not null)
            if (this.Payload.IdLocalVistoria == null)
            {
                AdicionarMensagemResponse("Local de Vistoria não informado");
            }


            // to ensure "IdCausaDano" is required (not null)
            if (this.Payload.IdCausaDano == null)
            {
                AdicionarMensagemResponse("Causa do Danos não informada");
            }

            // to ensure "CodTipoLacre" is required (not null)
            if (this.Payload.CodTipoLacre == null)
            {
                AdicionarMensagemResponse("Tipo Lacre não informado");
            }

            // to ensure "CodTipoGambito" is required (not null)
            if (this.Payload.CodTipoGambito == null)
            {
                AdicionarMensagemResponse("Tipo Gambito não informado");
            }

            // to ensure "CodTipoProcessoDevido" is required (not null)
            if (this.Payload.CodTipoConclusaoTfa == null)
            {
                AdicionarMensagemResponse("Processo Devido/Indevido não informado");
            }

            // to ensure "CodTipoLiberadorDescarga" is required (not null)
            if (this.Payload.CodTipoLiberadorDescarga == null)
            {
                AdicionarMensagemResponse("Tipo de Liberador de Descarga não informado");
            }

            // to ensure "EdicaoManual" is required (not null)
            if (this.Payload.EdicaoManual == null)
            {
                AdicionarMensagemResponse("Flag de Edição Manual não informada");
            }

            // to ensure "ResponsavelCiente" is required (not null)
            if (this.Payload.ResponsavelCiente == null)
            {
                AdicionarMensagemResponse("Flag de Cliente Ciente não informada");
            }

            // to ensure "DataHoraSinistro" is required (not null)
            if (this.Payload.DataHoraSinistro == null)
            {
                AdicionarMensagemResponse("Data/Hora do Sinistro não informada");
            }

            // to ensure "DataHoraVistoria" is required (not null)
            if (this.Payload.DataHoraVistoria == null)
            {
                AdicionarMensagemResponse("Data/Hora da Vistoria não informada");
            }

            // to ensure "MatriculaVistoriador" is required (not null)
            if (this.Payload.MatriculaVistoriador == null)
            {
                AdicionarMensagemResponse("Matrícula do Vistoriador não informada");
            }

            // to ensure "NomeVistoriador" is required (not null)
            if (this.Payload.NomeVistoriador == null)
            {
                AdicionarMensagemResponse("Nome do Vistoriador não informado");
            }

            // to ensure "ModoEdicao" is required (not null)
            if (this.Payload.ModoEdicao == null)
            {
                AdicionarMensagemResponse("Flag de Modo de Edição não informada");
            }

            // to ensure "ProcessoGerado" is required (not null)
            if (this.Payload.ProcessoGerado == null)
            {
                AdicionarMensagemResponse("Flag de Processo Gerado não informada");
            }

            // NumeroVagao (string) maxLength
            if (this.Payload.NumeroVagao != null && this.Payload.NumeroVagao.Length > 7)
            {
                AdicionarMensagemResponse("Valor inválido para Número do Vagão, comprimento deve ser menor ou igual a 7.");
            }

            // ConsideracoesAdicionais (string) maxLength
            if (this.Payload.ConsideracoesAdicionais != null && this.Payload.ConsideracoesAdicionais.Length > 500)
            {
                AdicionarMensagemResponse("Valor inválido para Considerações Adcionais, comprimento deve ser menor ou igual a 500.");
            }

            // NomeRepresentante (string) maxLength
            if (this.Payload.NomeRepresentante != null && this.Payload.NomeRepresentante.Length > 60)
            {
                AdicionarMensagemResponse("Valor inválido para Nome do Representante, comprimento deve ser menor ou igual a 60.");
            }

            // DocumentoRepresentante (string) maxLength
            if (this.Payload.DocumentoRepresentante != null && this.Payload.DocumentoRepresentante.Length > 15)
            {
                AdicionarMensagemResponse("Valor inválido para Documento do Representante, comprimento deve ser menor ou igual a 15.");
            }

            // MatriculaVistoriador (string) maxLength
            if (this.Payload.MatriculaVistoriador != null && this.Payload.MatriculaVistoriador.Length > 10)
            {
                AdicionarMensagemResponse("Valor inválido para Matrícula do Vistoriador, comprimento deve ser menor ou igual a 10.");
            }

            // NomeVistoriador (string) maxLength
            if (this.Payload.NomeVistoriador != null && this.Payload.NomeVistoriador.Length > 60)
            {
                AdicionarMensagemResponse("Valor inválido para Nome do Vistoriador, comprimento deve ser menor ou igual a 60.");
            }


            // Justificativa (string) is required (not null)
            if (this.Payload.NumeroProcesso.HasValue && this.Payload.NumeroProcesso.Value > 0 && this.Payload.Justificativa == null)
            {
                AdicionarMensagemResponse("Justificativa não informada");
            }

            // Justificativa (string) maxLength
            if (this.Payload.NumeroProcesso.HasValue && this.Payload.NumeroProcesso.Value > 0 && this.Payload.Justificativa != null && this.Payload.Justificativa.Length > 500)
            {
                AdicionarMensagemResponse("Valor inválido para Justificativa, comprimento deve ser menor ou igual a 500.");
            }

            //Se a lista de erros estiver vazia, retorna valiadação Ok/true
            return (this.Response.ListaErros.Count == 0);

        }
        #endregion Methods of ProcessoSeguroIntegracaoService
    }
}
