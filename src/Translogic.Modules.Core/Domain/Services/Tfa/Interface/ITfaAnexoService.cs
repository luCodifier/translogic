﻿namespace Translogic.Modules.Core.Domain.Services.Tfa.Interface
{
    using Translogic.Modules.Core.Domain.Model.Tfa;

    public interface ITfaAnexoService
    {
        /// <summary>
        ///   Obtem Anexo TFA pelo numProcesso
        /// </summary>
        /// <param name="numProcesso">numProcesso</param>
        /// <returns>Processo Seguro em Cache</returns>
        TfaAnexo ObterPorNumProcesso(string numProcesso);

        /// <summary>
        ///   Remove determinado Anexo TFA da tabela
        /// </summary>
        /// <param name="processoSeguroCache">Anexo TFA a ser Removido</param>
        /// <returns>Retorna se foi possível remover o Anexo</returns>/// 
        void RemoverAnexoTFA(TfaAnexo tfaAnexo);

        /// <summary>
        ///   Salva Anexo TFA
        /// </summary>
        /// <param name="processoSeguroCache">Objeto a ser salvo</param>
        /// <returns></returns>
        TfaAnexo Salvar(TfaAnexo tfaAnexo);
    }
}