﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Translogic.Modules.Core.Domain.Model.Tfa;
using Translogic.Core.Infrastructure.Web;
using ALL.Core.AcessoDados;
using ALL.Core.Dominio;


namespace Translogic.Modules.Core.Domain.Services.Tfa.Interface
{
    /// <summary>
    /// Interface de Serviço de Grupos de Empresas de Gestão de TFAs
    /// </summary>
    public interface IGrupoEmpresaTfaService
    {
        /// <summary>
        /// Localiza o grupo empresa pelo nome
        /// </summary>
        /// <param name="nomeGrupo">Nome</param>
        /// <returns></returns>
        GrupoEmpresaTfa ObterPorNome(string nomeGrupo);

        /// <summary>
        /// Localiza o grupo empresa pelo id
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns></returns>
        GrupoEmpresaTfa ObterPorID(int idGrupoEmpresa);

        /// <summary>
        /// Incluir um novo grupo empresa.
        /// </summary>
        /// <param name="nomeGrupo">Nome do grupo</param>
        /// <returns></returns>
        GrupoEmpresaTfa IncluirGrupoEmpresa(string nomeGrupo);

        /// <summary>
        /// Remove um grupo empresa da base de dados
        /// </summary>
        /// <param name="id">ID</param>
        /// <returns></returns>
        void RemoverGrupoEmpresa(int id);

        /// <summary>
        /// Retorna uma lista paginada de grupo empresa.
        /// </summary>
        /// <param name="detalhesPaginacaoWeb">Detalhes paginacao web</param>
        /// <param name="nomeGrupo">Nome do grupo</param>
        /// <returns></returns>
        ResultadoPaginado<GrupoEmpresaTfa> ObterPaginadoPorNomeGrupo(DetalhesPaginacaoWeb detalhesPaginacaoWeb, string nomeGrupo);


        /// <summary>
        /// Retorna uma lista paginada de grupo empresa.
        /// </summary>
        /// <param name="detalhesPaginacaoWeb">Detalhes paginacao web</param>
        /// <returns></returns>
        ResultadoPaginado<GrupoEmpresaTfa> ObterTodosPaginado(DetalhesPaginacaoWeb detalhesPaginacaoWeb);

        /// <summary>
        /// Obtem a lista de Todos os Grupos de Empresas
        /// </summary>
        /// <returns>IList de GrupoEmpresaTfa</returns>
        IList<GrupoEmpresaTfa> ObterTodos();

        /// <summary>
        /// Altera o nome do Grupo de Empresas
        /// </summary>
        /// <param name="idGrupoEmpresa">ID do Grupo de Empresas</param>
        /// <param name="nomeGrupo">nome do Grupo de Empresas</param>
        void AlterarGrupoEmpresa(int idGrupoEmpresa, string nomeGrupo);
    }
}
