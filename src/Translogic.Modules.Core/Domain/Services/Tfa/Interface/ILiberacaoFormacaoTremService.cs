﻿namespace Translogic.Modules.Core.Domain.Services.Fornecedor.Interface
{
    using System.Collections.Generic;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Modules.Core.Util;
    using ALL.Core.Dominio;
    using Translogic.Core.Infrastructure.Web;
    using Translogic.Modules.Core.Domain.Model.LiberacaoFormacaoTrem;
    using System;

    public interface ILiberacaoFormacaoTremService
    {
        /// <summary>
        /// Busca fornecedorOs por parametros
        /// </summary>        
        /// <param name="nomeFornecedor"></param>
        /// <param name="idLocal"></param>
        /// <param name="tipoServico"></param>
        /// <returns></returns>          
        ResultadoPaginado<LiberacaoFormacaoTremDto> ObterConsulta(DetalhesPaginacaoWeb detalhesPaginacaoWeb, decimal OrdemServico, string OrigemTrem, string DestinoTrem, string LocalAtual, string NomeSol, string NomeAutorizador, DateTime dataInicial, DateTime dataFinal, string SituacaoTrava);


       /// <summary>
       /// Retornar lista de Liberacao Formacao Trem
       /// </summary>     
       /// <returns></returns>
       IList<LiberacaoFormacaoTremDto> ObterLiberacaoFormacaoTremExportar(
                                                                            decimal OrdemServico,
                                                                            string OrigemTrem,
                                                                            string DestinoTrem,
                                                                            string LocalAtual,
                                                                            string NomeSol,
                                                                            string NomeAutorizador,
                                                                            DateTime dataInicial,
                                                                            DateTime dataFinal,
                                                                            string SituacaoTrava);

       /// <summary>
       /// Retornar o timmer definido em base para consulta automatica
       /// </summary>
       /// <returns></returns>
       int ObterTimmerConsultaLiberacaoFormacaoTrem();

    }
}