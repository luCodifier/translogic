﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Translogic.Modules.Core.Domain.Model.Tfa;
using Translogic.Modules.Core.Domain.Model.Dto;

namespace Translogic.Modules.Core.Domain.Services.Tfa.Interface
{
    public interface ITerminalTfaService
    {
        /// <summary>
        /// Obtem o TerminalTfa pelo ID do Terminal  informado
        /// </summary>
        /// <param name="idTerminal">Id do Terminal</param>
        /// <returns></returns>
        TerminalTfa ObterPorIdTerminal(int idTerminal);

        /// <summary>
        /// Inclui o TerminalTFA no banco de dados
        /// </summary>
        /// <param name="idTerminal">Id do Terminal</param>
        /// <returns></returns>
        TerminalTfa InserirTerminalTfa(int idTerminal);        

        /// <summary>
        /// Obtem a lista de DTOs de Terminais
        /// </summary>
        /// <returns>Retorna lista de DTOs de Terminais</returns>
        IList<TerminalDto> ObterListaTerminalDto();
        
    }
}
