﻿namespace Translogic.Modules.Core.Domain.Services.Tfa.Interface
{
    using System.Collections.Generic;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using ALL.Core.Dominio;
    using Translogic.Core.Infrastructure.Web;
    using Translogic.Modules.Core.Domain.Model.Estrutura;
using System.IO;
using Translogic.Modules.Core.Domain.Model.Acesso;
using System;

    public interface ITfaService
    {
        
        ResultadoPaginado<TfaConsultaDto> ObterConsulta(DetalhesPaginacaoWeb detalhesPaginacaoWeb, string dtInicioProcesso, string dtFinalProcesso, string vagao, string terminal, string un, string up, string malha, string provisionado,
                                                      string dtInicioSinistros, string dtFinalSinistros, string numProcesso, string os, string situacao, string causa, string numTermo, string sindicancia, string anexado);
       
       IEnumerable<TfaExportaDto> ObterConsultaTFAExportar(string dtInicioProcesso, string dtFinalProcesso, string vagao, string terminal, string un, string up, string malha, string provisionado,
                                                      string dtInicioSinistros, string dtFinalSinistros, string numProcesso, string os, string situacao, string causa, string numTermo, string sindicancia, string anexoTFA);       


        IList<UnidadeNegocioDto> ObterUnidadeNegocio();

        bool UploadTFA(int numeroProcesso);

        /// <summary>
        /// Buscar tfa pelo numero do processo
        /// </summary>
        /// <param name="numProcesso"></param>
        /// <returns></returns>
        Translogic.Modules.Core.Domain.Model.Tfa.Tfa ObterPorNumProcesso(int numProcesso);

        /// <summary>
        /// Obtém o Pdf do Tfa pelo Número do Processo
        /// </summary>
        /// <param name="numeroProcesso">Número do Processo</param>
        /// <returns></returns>
        Stream ObterPdfTfa(int numeroProcesso);

        /// <summary>
        /// Fechamento de processo em massa
        /// </summary>
        /// <param name="processos">Processos a fechar</param>
        /// <param name="analiseProcesso">Texto da analise</param>
        /// <param name="dtPrepPagamento">Data de preparação</param>
        /// <param name="formaPagamento">Formas de pagamento</param>
        /// <param name="dtPagamento">Data do pagamento</param>
        /// <param name="dtEncerramento">Data do encerramento</param>
        /// <param name="usuario">Usuário fechador do lote</param>
        void FecharLote(string[] processos, string analiseProcesso, DateTime dtPrepPagamento, string formaPagamento, DateTime dtPagamento, DateTime dtEncerramento, Usuario usuario);
    }
}