﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Translogic.Modules.Core.Domain.Services.Tfa.Interface;
using Translogic.Modules.Core.Domain.Model.Tfa.Repositories;
using Translogic.Modules.Core.Domain.Model.Estrutura.Repositories;
using Translogic.Modules.Core.Domain.Model.Tfa;
using Translogic.Core.Infrastructure;
using ALL.Core.Dominio;
using Translogic.Core.Infrastructure.Web;
using Translogic.Modules.Core.Domain.Model.Estrutura;

namespace Translogic.Modules.Core.Domain.Services.Tfa.Interface
{
    /// <summary>
    /// Interface de Serviço de EmpresaGrupoTfa
    /// </summary>
    public interface IEmpresaGrupoTfaService
    {

        /// <summary>
        /// Retorna todas as empresas do Grupo de Empresas passado como parâmetro
        /// </summary>
        /// <param name="paginacao">Dados para paginação em tela</param>
        /// <param name="idGrupoEmpresa">ID do Grupo de Empresas</param>
        /// <returns>Resultado Paginado de EmpresaGrupoTfa</returns>
        ResultadoPaginado<EmpresaGrupoTfa> ObterTodosPorIdGrupoEmpresa(DetalhesPaginacaoWeb paginacao, int idGrupoEmpresa);

        /// <summary>
        /// Inclui uma empresa em um Grupo de Empresas para gestão de TFAs
        /// </summary>
        /// <param name="cnpj">CNPJ da Empresa no cadastro de Empresas</param>
        /// <param name="IdGrupoEmpresa">ID do Grupo de Empresas</param>
        /// <returns>Retorna EmpresaGrupoTfa inserida no banco de dados</returns>
        EmpresaGrupoTfa IncluirEmpresaGrupoTfa(string cnpj, int IdGrupoEmpresa);

        /// <summary>
        /// Exclui a Empresa do grupo de Empresas
        /// </summary>
        /// <param name="id">Id do registro à ser excluído</param>
        void ExcluirEmpresaGrupoTfa(int id);

        /// <summary>
        /// Rertorna a Empresa, caso exista no Repositório IEmpresa, de acordo com o CNPJ informado
        /// </summary>
        /// <param name="cnpj">CNPJ da Empresa</param>
        /// <returns>Retorna a instãncia de IEmpresa</returns>
        IEmpresa LocalizarEmpresa(string cnpj);
        
    }
}