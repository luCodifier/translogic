﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Translogic.Modules.Core.Domain.Model.Tfa;
using ALL.Core.Dominio;
using Translogic.Core.Infrastructure.Web;

namespace Translogic.Modules.Core.Domain.Services.Tfa.Interface
{
    public interface ITerminalTfaEmailService
    {
        /// <summary>
        /// Obter Por Id
        /// </summary>
        /// <param name="id">id da entidade</param>
        /// <returns>entidade populada</returns>
        TerminalTfaEmail ObterPorId(int id);

        /// <summary>
        /// Obtem o email do terminal pelo IdTerminal e email
        /// </summary>
        /// <param name="idTerminal">Id do Terminal</param>
        /// <param name="email">e-mail</param>
        /// <returns>retorna o email dop terminal pelo IdTerminal e email</returns>
        TerminalTfaEmail ObterPoridTerminalEmail(int idTerminal, string email);

        /// <summary>
        /// Exclui a entidade pelo Id
        /// </summary>
        /// <param name="id">id da entidade</param>
        void Remover(int id);

        /// <summary>
        /// Obtem a lista de Emails vinculados ao terminal, de acordo com o ID passado como parâmetro
        /// </summary>
        /// <param name="paginacao">objeto com os parâmetros de paginação</param>
        /// <param name="idTerminal">Id do Terminal</param>
        /// <returns>Retorna lista paginada de Emails</returns>
        ResultadoPaginado<TerminalTfaEmail> ObterTodosPorIdTerminal(DetalhesPaginacaoWeb paginacao, int idTerminal);
        
        /// <summary>
        /// Inclui um novo email para o Terminal
        /// </summary>
        /// <param name="terminalTfaEmail"></param>
        /// <returns></returns>
        TerminalTfaEmail IncluirTerminalTfaEmail(TerminalTfa terminal, string email);
    }
}
