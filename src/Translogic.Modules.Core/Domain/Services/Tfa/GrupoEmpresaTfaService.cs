﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Translogic.Modules.Core.Domain.Services.Tfa.Interface;
using Translogic.Modules.Core.Domain.Model.Tfa;
using Translogic.Core.Infrastructure.Web;
using Translogic.Modules.Core.Domain.Model.Tfa.Repositories;
using ALL.Core.Dominio;
using Translogic.Core.Infrastructure;


namespace Translogic.Modules.Core.Domain.Services.Tfa
{
    /// <summary>
    /// Classe de Serviço de Grupos de Empresas de Gestão de TFAs
    /// </summary>
    public class GrupoEmpresaTfaService : IGrupoEmpresaTfaService
    {
        private readonly IGrupoEmpresaTfaRepository _grupoEmpresaTfaRepository;

        public GrupoEmpresaTfaService(IGrupoEmpresaTfaRepository grupoEmpresaTfaRepository)
        {
            _grupoEmpresaTfaRepository = grupoEmpresaTfaRepository;
        }

        /// <summary>
        /// Obtem o Grupo Empresa de acordo com o Nome do Grupo informado como parâmetro
        /// </summary>
        /// <param name="nomeGrupo">Nome de Grupo de Empresas</param>
        /// <returns></returns>
        public GrupoEmpresaTfa ObterPorNome(string nomeGrupo)
        {
            return _grupoEmpresaTfaRepository.ObterPorNome(nomeGrupo);

        }

        /// <summary>
        /// Obtem o Grupo Empresa de acordo com o ID do Grupo informado como parâmetro
        /// </summary>
        /// <param name="idGrupoEmpresa">ID do Grupo de Empresas</param>
        /// <returns></returns>
        public GrupoEmpresaTfa ObterPorID(int idGrupoEmpresa)
        {
            return _grupoEmpresaTfaRepository.ObterPorId(idGrupoEmpresa);
        }

        /// <summary>
        /// Inclui novo Grupo de Empresas
        /// </summary>
        /// <param name="nomeGrupo">Nome do Grupo de Empresas</param>
        /// <returns></returns>
        public GrupoEmpresaTfa IncluirGrupoEmpresa(string nomeGrupo)
        {
            var grupoEmpresa = new GrupoEmpresaTfa();
            grupoEmpresa.NomeGrupo = nomeGrupo;
            return _grupoEmpresaTfaRepository.Inserir(grupoEmpresa);
        }

        /// <summary>
        /// Exclui o Grupo de Empresas passado como parâmetro
        /// Valida se existem Empresas e/ou Usuários vinculados ao Grupo de Empresas
        /// </summary>
        /// <param name="idGrupoEmpresa">ID do Grupo de Empresas</param>
        public void RemoverGrupoEmpresa(int idGrupoEmpresa)
        {
            if (_grupoEmpresaTfaRepository.ExisteEmpresaVinculada(idGrupoEmpresa))
            {
                throw new TranslogicException("Não é possível excluir o Grupo de Empresas, pois possui empresa(s) vinculada(s)!");
            }

            if (_grupoEmpresaTfaRepository.ExisteUsuarioCAALVinculado(idGrupoEmpresa))
            {
                throw new TranslogicException("Não é possível excluir o Grupo de Empresas, pois possui usuário(s) do sistema CAALL vinculado(s)!");
            }
            _grupoEmpresaTfaRepository.Remover(idGrupoEmpresa);
        }

        /// <summary>
        /// Retorna resultado paginado de Grupos de Empresas
        /// </summary>
        /// <param name="paginacao">Dados para paginação em tela</param>
        /// <returns>Resultado Paginado de GrupoEmpresaTfa</returns>
        public ResultadoPaginado<GrupoEmpresaTfa> ObterTodosPaginado(DetalhesPaginacaoWeb paginacao)
        {
            paginacao.Dir = "Asc";
            paginacao.Sort = "NomeGrupo";
            return _grupoEmpresaTfaRepository.ObterTodosPaginado(paginacao);
        }

        /// <summary>
        /// Obtem a lista de Todos os Grupos de Empresas
        /// </summary>
        /// <returns>IList de GrupoEmpresaTfa</returns>
        public IList<GrupoEmpresaTfa> ObterTodos()
        {
            return _grupoEmpresaTfaRepository.ObterTodos();
        }


        /// <summary>
        /// Retorna resultado paginado de Grupos de Empresas, de acordo com o nome passado como parâmetro (LIKE%)
        /// </summary>
        /// <param name="paginacao">Dados para paginação em tela</param>
        /// <param name="nomeGrupo">Nome do Grupo de Empresas</param>
        /// <returns>Resultado Paginado de GrupoEmpresaTfa</returns>
        public ResultadoPaginado<GrupoEmpresaTfa> ObterPaginadoPorNomeGrupo(DetalhesPaginacaoWeb paginacao, string nomeGrupo)
        {
            return _grupoEmpresaTfaRepository.ObterPaginadoPorNomeGrupo(paginacao, nomeGrupo);
        }

        /// <summary>
        /// Altera os dados do Grupo de Empresas
        /// </summary>
        /// <param name="idGrupoEmpresa">ID do Grupo de Empresas</param>
        /// <param name="nomeGrupo">Nome do Grupo de Empresas</param>
        public void AlterarGrupoEmpresa(int idGrupoEmpresa, string nomeGrupo)
        {
            var grupoEmpresa = _grupoEmpresaTfaRepository.ObterPorId(idGrupoEmpresa);

            if (grupoEmpresa != null)
            {
                grupoEmpresa.NomeGrupo = nomeGrupo;

                _grupoEmpresaTfaRepository.Atualizar(grupoEmpresa);
            }
        }
    }
}