namespace Translogic.Modules.Core.Domain.Services.CadastroUnicoEstacoes
{
	using ALL.Core.Dominio.Services;
	using Model.CadastroUnicoEstacoes;
	using Model.CadastroUnicoEstacoes.Repositories;

	/// <summary>
	/// Servido de Esta��es do cadastro �nico
	/// </summary>
	public class EstacaoService : BaseCrudService<Estacao, int>
	{
		#region CONSTRUTORES
		/// <summary>
		/// Construtor injetando o reposit�rio
		/// </summary>
		/// <param name="repository">Reposit�rio da esta��o</param>
		public EstacaoService(IEstacaoRepository repository) : base(repository)
		{
		}

		#endregion
	}
}