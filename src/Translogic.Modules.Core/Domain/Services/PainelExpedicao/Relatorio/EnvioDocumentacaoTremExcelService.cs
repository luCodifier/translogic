﻿namespace Translogic.Modules.Core.Domain.Services.PainelExpedicao.Relatorio
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using OfficeOpenXml;
    using OfficeOpenXml.Style;
    using Translogic.Modules.Core.Domain.Model.Codificador.Repositories;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Modules.Core.Infrastructure.Services;

    public class EnvioDocumentacaoTremExcelService : GeracaoExcelCustomizadoService
    {
        private IList<MonitoramentoEnvioDocumentacaoDto> _valores = new List<MonitoramentoEnvioDocumentacaoDto>();

        private int _indiceUltimaColuna;

        public int IndiceUltimaColuna
        {
            get { return _indiceUltimaColuna; }
            set { _indiceUltimaColuna = value; }
        }

        public EnvioDocumentacaoTremExcelService(IList<MonitoramentoEnvioDocumentacaoDto> valores)
        {
            this._valores = valores;
            this._indiceUltimaColuna = 83;//Coluna P // Código da tabela ASCII que corresponde a letra da última coluna
        }
        public override Stream GerarRelatorioCustomizado(string tituloRelatorio,
                                                         IList<string> filtros,
                                                         IList<string> colunas)
        {


            var colFontBlackFromHex = System.Drawing.ColorTranslator.FromHtml("#000000"); // Cor Preta
            var colFontWhiteFromHex = System.Drawing.ColorTranslator.FromHtml("#ffffff"); // Cor Branca
            var colFontRedFromHex = System.Drawing.ColorTranslator.FromHtml("#ff0000"); // Cor Vermelha
            var colBackGroundBlueFromHex = System.Drawing.ColorTranslator.FromHtml("#ddebf7"); // Cor Azul Claro

            using (var p = new ExcelPackage(new MemoryStream()))
            {
                p.Workbook.Properties.Author = "Jhonatan - Rumo ALL";
                p.Workbook.Properties.Title = tituloRelatorio;
                p.Workbook.Properties.Company = "Rumo ALL";

                p.Workbook.Worksheets.Add("Relatório");
                var worksheet = p.Workbook.Worksheets[1];

                // Título
                worksheet.Cells["A1"].Value = tituloRelatorio;
                worksheet.Cells["A1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells["A1"].Style.Font.Bold = true;
                worksheet.Cells["A1"].Style.Font.Size = 14;
                worksheet.Cells["A1:G1"].Merge = true;
                worksheet.Cells["A1:G1"].Style.Fill.PatternType = ExcelFillStyle.Solid;

                var colFromHex = System.Drawing.ColorTranslator.FromHtml("#ffffff");
                worksheet.Cells["A1:G1"].Style.Fill.BackgroundColor.SetColor(colFromHex);

                // Filtros
                int caracter = 65;
                char cFinal = (char)caracter;
                int linhaPosicao = 2;
                if (filtros != null)
                {
                    foreach (string filtro in filtros)
                    {
                        char c = (char)caracter;
                        worksheet.Cells[c + linhaPosicao.ToString()].Value = filtro;
                        worksheet.Cells[c + linhaPosicao.ToString()].AutoFitColumns();
                        caracter = caracter + 1;
                    }
                    cFinal = (char)caracter;
                    colFromHex = System.Drawing.ColorTranslator.FromHtml("#c9c9c9");
                    worksheet.Cells["A" + linhaPosicao.ToString() + ":" + cFinal + linhaPosicao.ToString()].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells["A" + linhaPosicao.ToString() + ":" + cFinal + linhaPosicao.ToString()].Style.Fill.BackgroundColor.SetColor(colFromHex);
                    worksheet.Cells["A" + linhaPosicao.ToString() + ":" + cFinal + linhaPosicao.ToString()].Style.Font.Bold = true;
                    worksheet.Cells["A" + linhaPosicao.ToString() + ":" + cFinal + linhaPosicao.ToString()].Style.Font.Size = 10;
                    worksheet.Cells["A" + linhaPosicao.ToString() + ":" + cFinal + linhaPosicao.ToString()].AutoFitColumns();
                    linhaPosicao = linhaPosicao + 1;
                }

                // Colunas dos Valores
                caracter = 65;
                foreach (string co in colunas)
                {
                    char c = (char)caracter;
                    worksheet.Cells[c + linhaPosicao.ToString()].Value = co;
                    worksheet.Cells[c + linhaPosicao.ToString()].Style.Font.Color.SetColor(colFontWhiteFromHex);
                    worksheet.Cells[c + linhaPosicao.ToString()].Style.Font.Bold = true;
                    worksheet.Cells[c + linhaPosicao.ToString()].AutoFitColumns();
                    worksheet.Cells[c + linhaPosicao.ToString()].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    caracter = caracter + 1;
                }
                cFinal = (char)(caracter - 1);
                colFromHex = System.Drawing.ColorTranslator.FromHtml("#9bc2e6");
                worksheet.Cells["A" + linhaPosicao.ToString() + ":" + cFinal + linhaPosicao.ToString()].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells["A" + linhaPosicao.ToString() + ":" + cFinal + linhaPosicao.ToString()].Style.Fill.BackgroundColor.SetColor(colFromHex);
                worksheet.Cells["A" + linhaPosicao.ToString() + ":" + cFinal + linhaPosicao.ToString()].Style.Font.Size = 11;
                worksheet.Cells["A" + linhaPosicao.ToString() + ":" + cFinal + linhaPosicao.ToString()].AutoFitColumns();

                linhaPosicao = linhaPosicao + 1;

                if (_valores != null)
                {
                    //int linhaValor = 4;
                    int indiceUltimaColuna = ObterIndiceUltimaColuna();
                    cFinal = (char)indiceUltimaColuna;
                    colFromHex = System.Drawing.ColorTranslator.FromHtml("#ffffff");
                    foreach (var linha in _valores)
                    {
                        caracter = 65;
                        while (caracter <= indiceUltimaColuna)
                        {
                            char c = (char)caracter;
                            worksheet.Cells[c + linhaPosicao.ToString()].Value = ObterValorLinhaColuna(c, linha);

                            var refaturado = this.Refaturado(linha);
                            var possuiDocCpl = this.PossuiDocComp(linha);
                            var docExcluida = this.DocExcluida(linha);
                            var recomposicao = this.Recomposicao(linha);

                            worksheet.Cells[c + linhaPosicao.ToString()].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            if ((linhaPosicao % 2) == 0)
                                worksheet.Cells[c + linhaPosicao.ToString()].Style.Fill.BackgroundColor.SetColor(colBackGroundBlueFromHex);
                            else
                                worksheet.Cells[c + linhaPosicao.ToString()].Style.Fill.BackgroundColor.SetColor(colFromHex);

                            if ((refaturado) && ((docExcluida) && (!possuiDocCpl) && (recomposicao)))
                                worksheet.Cells[c + linhaPosicao.ToString()].Style.Font.Color.SetColor(colFontRedFromHex);
                            else
                                worksheet.Cells[c + linhaPosicao.ToString()].Style.Font.Color.SetColor(colFontBlackFromHex);

                            caracter = caracter + 1;
                        }
                        worksheet.Cells["A" + linhaPosicao.ToString() + ":" + cFinal + linhaPosicao.ToString()].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        worksheet.Cells["A" + linhaPosicao.ToString() + ":" + cFinal + linhaPosicao.ToString()].AutoFitColumns();
                        worksheet.Cells["A" + linhaPosicao.ToString() + ":" + cFinal + linhaPosicao.ToString()].Style.Font.Size = 11;

                        linhaPosicao = linhaPosicao + 1;
                    }
                }

                p.Save();
                return p.Stream;
            }

        }

        public override int ObterIndiceUltimaColuna()
        {
            return this._indiceUltimaColuna;
        }

        public override string ObterValorLinhaColuna(char coluna, object objetoValor)
        {
            if (objetoValor == null)
                return String.Empty;

            switch (coluna)
            {
                case 'A':
                    return (objetoValor as MonitoramentoEnvioDocumentacaoDto).Enviado;
                case 'B':
                    return (objetoValor as MonitoramentoEnvioDocumentacaoDto).CriadoEm.HasValue ?
                        (objetoValor as MonitoramentoEnvioDocumentacaoDto).CriadoEm.Value.ToString("dd/MM/yyyy HH:mm:ss") : "";
                case 'C':
                    return (objetoValor as MonitoramentoEnvioDocumentacaoDto).EstacaoEnvio;
                case 'D':
                    return (objetoValor as MonitoramentoEnvioDocumentacaoDto).Envio;
                case 'E':
                    return (objetoValor as MonitoramentoEnvioDocumentacaoDto).TremPrefixo;
                case 'F':
                    return (objetoValor as MonitoramentoEnvioDocumentacaoDto).OsNumero.ToString();
                case 'G':
                    return (objetoValor as MonitoramentoEnvioDocumentacaoDto).Origem;
                case 'H':
                    return (objetoValor as MonitoramentoEnvioDocumentacaoDto).Atual;
                case 'I':
                    return (objetoValor as MonitoramentoEnvioDocumentacaoDto).Destino;
                case 'J':
                    return (objetoValor as MonitoramentoEnvioDocumentacaoDto).DataChegadaTrem.HasValue ?
                        (objetoValor as MonitoramentoEnvioDocumentacaoDto).DataChegadaTrem.Value.ToString("dd/MM/yyyy HH:mm:ss") : "";
                case 'K':
                    return (objetoValor as MonitoramentoEnvioDocumentacaoDto).QuantidadeVagoes.ToString();
                case 'L':
                    return (objetoValor as MonitoramentoEnvioDocumentacaoDto).TremEncerrado;
                case 'M':
                    return (objetoValor as MonitoramentoEnvioDocumentacaoDto).Recebedor;
                case 'N':
                    return (objetoValor as MonitoramentoEnvioDocumentacaoDto).UsuarioNome;
                case 'O':
                    return (objetoValor as MonitoramentoEnvioDocumentacaoDto).Refaturamento;
                case 'P':
                    return (objetoValor as MonitoramentoEnvioDocumentacaoDto).Recomposicao;
                case 'Q':
                    return (objetoValor as MonitoramentoEnvioDocumentacaoDto).DocumentacaoCompleta;
                case 'R':
                    return (objetoValor as MonitoramentoEnvioDocumentacaoDto).DocumentacaoExcluida;
                case 'S':
                    return (objetoValor as MonitoramentoEnvioDocumentacaoDto).DocumentacaoExcluida == "N" ?
                        string.Format("http://caall.rumolog.com/DownloadDocumentacao/DownloadDocumentacao?fileName={0}", (objetoValor as MonitoramentoEnvioDocumentacaoDto).LinkDownload) : "";
                default:
                    return String.Empty;
            }
        }

        private bool Refaturado(object objetoValor)
        {
            var pdf = (objetoValor as MonitoramentoEnvioDocumentacaoDto).Refaturamento;
            if (String.IsNullOrEmpty(pdf))
                return false;
            if (pdf.ToUpper() == "N")
                return false;

            return true;
        }

        private bool Recomposicao(object objetoValor)
        {
            var pdf = (objetoValor as MonitoramentoEnvioDocumentacaoDto).Recomposicao;
            if (String.IsNullOrEmpty(pdf))
                return false;
            if (pdf.ToUpper() == "N")
                return false;

            return true;
        }

        private bool PossuiDocComp(object objetoValor)
        {
            var pdf = (objetoValor as MonitoramentoEnvioDocumentacaoDto).DocumentacaoCompleta;
            if (String.IsNullOrEmpty(pdf))
                return false;
            if (pdf.ToUpper() == "N")
                return false;

            return true;
        }

        private bool DocExcluida(object objetoValor)
        {
            var pdf = (objetoValor as MonitoramentoEnvioDocumentacaoDto).DocumentacaoExcluida;
            if (String.IsNullOrEmpty(pdf))
                return false;
            if (pdf.ToUpper() == "N")
                return false;

            return true;
        }
    }
}