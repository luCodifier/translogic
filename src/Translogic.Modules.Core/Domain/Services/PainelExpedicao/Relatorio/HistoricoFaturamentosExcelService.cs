﻿namespace Translogic.Modules.Core.Domain.Services.PainelExpedicao.Relatorio
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using OfficeOpenXml;
    using OfficeOpenXml.Style;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Modules.Core.Infrastructure.Services;

    public class HistoricoFaturamentosExcelService : GeracaoExcelCustomizadoService
    {
        private IList<RelatorioHistoricoFaturamentosDto> _valores = new List<RelatorioHistoricoFaturamentosDto>();

        private int _indiceUltimaColuna;

        public int IndiceUltimaColuna
        {
            get { return _indiceUltimaColuna; }
            set { _indiceUltimaColuna = value; }
        }

        public HistoricoFaturamentosExcelService(IList<RelatorioHistoricoFaturamentosDto> valores)
        {
            this._valores = valores;
            this._indiceUltimaColuna = 88; // Código da tabela ASCII que corresponde a letra da última coluna
        }

        public override Stream GerarRelatorioCustomizado(string tituloRelatorio,
                                                         IList<string> filtros,
                                                         IList<string> colunas)
        {

            var colFontBlackFromHex = System.Drawing.ColorTranslator.FromHtml("#000000"); // Cor Preta
            var colFontWhiteFromHex = System.Drawing.ColorTranslator.FromHtml("#ffffff"); // Cor Branca
            var colFontRedFromHex = System.Drawing.ColorTranslator.FromHtml("#ff0000"); // Cor Vermelha
            var colBackGroundBlueFromHex = System.Drawing.ColorTranslator.FromHtml("#ddebf7"); // Cor Azul Claro

            using (var p = new ExcelPackage(new MemoryStream()))
            {
                p.Workbook.Properties.Author = "Translogic - Rumo ALL";
                p.Workbook.Properties.Title = tituloRelatorio;
                p.Workbook.Properties.Company = "Rumo ALL";

                p.Workbook.Worksheets.Add("Relatório");
                var worksheet = p.Workbook.Worksheets[1];

                // Título
                worksheet.Cells["A1"].Value = tituloRelatorio;
                worksheet.Cells["A1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells["A1"].Style.Font.Bold = true;
                worksheet.Cells["A1"].Style.Font.Size = 14;
                worksheet.Cells["A1:G1"].Merge = true;
                worksheet.Cells["A1:G1"].Style.Fill.PatternType = ExcelFillStyle.Solid;

                var colFromHex = System.Drawing.ColorTranslator.FromHtml("#ffffff");
                worksheet.Cells["A1:G1"].Style.Fill.BackgroundColor.SetColor(colFromHex);

                // Filtros
                int caracter = 65;
                char cFinal = (char)caracter;
                int linhaPosicao = 2;
                if (filtros != null)
                {
                    foreach (string filtro in filtros)
                    {
                        char c = (char)caracter;
                        worksheet.Cells[c + linhaPosicao.ToString()].Value = filtro;
                        worksheet.Cells[c + linhaPosicao.ToString()].AutoFitColumns();
                        caracter = caracter + 1;
                    }
                    cFinal = (char)caracter;
                    colFromHex = System.Drawing.ColorTranslator.FromHtml("#c9c9c9");
                    worksheet.Cells["A" + linhaPosicao.ToString() + ":" + cFinal + linhaPosicao.ToString()].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells["A" + linhaPosicao.ToString() + ":" + cFinal + linhaPosicao.ToString()].Style.Fill.BackgroundColor.SetColor(colFromHex);
                    worksheet.Cells["A" + linhaPosicao.ToString() + ":" + cFinal + linhaPosicao.ToString()].Style.Font.Bold = true;
                    worksheet.Cells["A" + linhaPosicao.ToString() + ":" + cFinal + linhaPosicao.ToString()].Style.Font.Size = 10;
                    worksheet.Cells["A" + linhaPosicao.ToString() + ":" + cFinal + linhaPosicao.ToString()].AutoFitColumns();
                    linhaPosicao = linhaPosicao + 1;
                }

                // Colunas dos Valores
                caracter = 65;
                foreach (string co in colunas)
                {
                    char c = (char)caracter;
                    worksheet.Cells[c + linhaPosicao.ToString()].Value = co;
                    worksheet.Cells[c + linhaPosicao.ToString()].Style.Font.Color.SetColor(colFontWhiteFromHex);
                    worksheet.Cells[c + linhaPosicao.ToString()].Style.Font.Bold = true;
                    worksheet.Cells[c + linhaPosicao.ToString()].AutoFitColumns();
                    worksheet.Cells[c + linhaPosicao.ToString()].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    caracter = caracter + 1;
                }
                cFinal = (char)(caracter - 1);
                colFromHex = System.Drawing.ColorTranslator.FromHtml("#9bc2e6");
                worksheet.Cells["A" + linhaPosicao.ToString() + ":" + cFinal + linhaPosicao.ToString()].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells["A" + linhaPosicao.ToString() + ":" + cFinal + linhaPosicao.ToString()].Style.Fill.BackgroundColor.SetColor(colFromHex);
                worksheet.Cells["A" + linhaPosicao.ToString() + ":" + cFinal + linhaPosicao.ToString()].Style.Font.Size = 11;
                worksheet.Cells["A" + linhaPosicao.ToString() + ":" + cFinal + linhaPosicao.ToString()].AutoFitColumns();

                linhaPosicao = linhaPosicao + 1;

                if (_valores != null)
                {
                    //int linhaValor = 4;
                    int indiceUltimaColuna = ObterIndiceUltimaColuna();
                    cFinal = (char)indiceUltimaColuna;
                    colFromHex = System.Drawing.ColorTranslator.FromHtml("#ffffff");
                    foreach (var linha in _valores)
                    {
                        caracter = 65;
                        while (caracter <= indiceUltimaColuna)
                        {
                            char c = (char)caracter;
                            worksheet.Cells[c + linhaPosicao.ToString()].Value = ObterValorLinhaColuna(c, linha);

                            var faturado = this.Faturado(linha);
                            var possuiPdf = this.PossuiPDF(linha);
                            var possuiTicket = this.PossuiTicket(linha);
                            var ticketObrigatorio = this.SegmentoComTicketObrigatorio(linha);

                            worksheet.Cells[c + linhaPosicao.ToString()].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            if ((linhaPosicao % 2) == 0)
                                worksheet.Cells[c + linhaPosicao.ToString()].Style.Fill.BackgroundColor.SetColor(colBackGroundBlueFromHex);
                            else
                                worksheet.Cells[c + linhaPosicao.ToString()].Style.Fill.BackgroundColor.SetColor(colFromHex);

                            if ((faturado) && (ticketObrigatorio) && (!possuiPdf) && (!possuiTicket))
                                worksheet.Cells[c + linhaPosicao.ToString()].Style.Font.Color.SetColor(colFontRedFromHex);
                            else
                                worksheet.Cells[c + linhaPosicao.ToString()].Style.Font.Color.SetColor(colFontBlackFromHex);

                            caracter = caracter + 1;
                        }
                        worksheet.Cells["A" + linhaPosicao.ToString() + ":" + cFinal + linhaPosicao.ToString()].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        worksheet.Cells["A" + linhaPosicao.ToString() + ":" + cFinal + linhaPosicao.ToString()].AutoFitColumns();
                        worksheet.Cells["A" + linhaPosicao.ToString() + ":" + cFinal + linhaPosicao.ToString()].Style.Font.Size = 11;

                        linhaPosicao = linhaPosicao + 1;
                    }
                }

                p.Save();
                return p.Stream;
            }

        }

        public override int ObterIndiceUltimaColuna()
        {
            return this._indiceUltimaColuna;
        }

        public override string ObterValorLinhaColuna(char coluna, object objetoValor)
        {
            if (objetoValor == null)
                return String.Empty;

            switch (coluna)
            {
                case 'A':
                    return (objetoValor as RelatorioHistoricoFaturamentosDto).Vagao;
                case 'B':
                    return (objetoValor as RelatorioHistoricoFaturamentosDto).DataCarga.ToString("dd/MM/yyyy");
                case 'C':
                    return (objetoValor as RelatorioHistoricoFaturamentosDto).Fluxo;
                case 'D':
                    return (objetoValor as RelatorioHistoricoFaturamentosDto).Segmento;
                case 'E':
                    return (objetoValor as RelatorioHistoricoFaturamentosDto).Origem;
                case 'F':
                    return (objetoValor as RelatorioHistoricoFaturamentosDto).Destino;
                case 'G':
                    return (objetoValor as RelatorioHistoricoFaturamentosDto).Cliente;
                case 'H':
                    return (objetoValor as RelatorioHistoricoFaturamentosDto).Expedidor;
                case 'I':
                    return (objetoValor as RelatorioHistoricoFaturamentosDto).RemetenteFiscal;
                case 'J':
                    return (objetoValor as RelatorioHistoricoFaturamentosDto).Recebedor;
                case 'K':
                    return (objetoValor as RelatorioHistoricoFaturamentosDto).DescricaoMercadoria;
                case 'L':
                    return (objetoValor as RelatorioHistoricoFaturamentosDto).Os.ToString();
                case 'M':
                    return (objetoValor as RelatorioHistoricoFaturamentosDto).Prefixo;
                case 'N':
                    return (objetoValor as RelatorioHistoricoFaturamentosDto).Seq.ToString();
                case 'O':
                    return (objetoValor as RelatorioHistoricoFaturamentosDto).PossuiTicket;
                case 'P':
                    return (objetoValor as RelatorioHistoricoFaturamentosDto).NumeroCte;
                case 'Q':
                    return (objetoValor as RelatorioHistoricoFaturamentosDto).PossuiNfe;
                case 'R':
                    return (objetoValor as RelatorioHistoricoFaturamentosDto).PossuiWebDanfe;
                case 'S':
                    return (objetoValor as RelatorioHistoricoFaturamentosDto).QuantidadeNfeRecebidas.ToString();
                case 'T':
                    return (objetoValor as RelatorioHistoricoFaturamentosDto).TotalNfe.ToString();
                case 'U':
                    return (objetoValor as RelatorioHistoricoFaturamentosDto).PossuiXmlNfe;
                case 'V':
                    return (objetoValor as RelatorioHistoricoFaturamentosDto).PossuiDcl;
                case 'W':
                    return (objetoValor as RelatorioHistoricoFaturamentosDto).PossuiLaudoMercadoria;
                case 'X':
                    return (objetoValor as RelatorioHistoricoFaturamentosDto).UsuarioNome;
                default:
                    return String.Empty;
            }
        }

        private bool PossuiPDF(object objetoValor)
        {
            var pdf = (objetoValor as RelatorioHistoricoFaturamentosDto).PossuiNfe;
            if (String.IsNullOrEmpty(pdf))
                return false;
            if (pdf.ToUpper() == "N")
                return false;

            return true;
        }

        private bool PossuiXmlNfe(object objetoValor)
        {
            var xmlNfe = (objetoValor as RelatorioHistoricoFaturamentosDto).PossuiXmlNfe;
            if (String.IsNullOrEmpty(xmlNfe))
                return false;

            if (xmlNfe.ToUpper() == "N")
                return false;

            return true;
        }

        private bool PossuiTicket(object objetoValor)
        {
            var ticket = (objetoValor as RelatorioHistoricoFaturamentosDto).PossuiTicket;
            if (String.IsNullOrEmpty(ticket))
                return false;
            if (ticket.ToUpper() == "N")
                return false;

            return true;
        }

        private bool PossuiLaudoMercadoria(object objetoValor)
        {
            var laudoMercadoria = (objetoValor as RelatorioHistoricoFaturamentosDto).PossuiLaudoMercadoria;
            if (String.IsNullOrEmpty(laudoMercadoria))
                return false;
            if (laudoMercadoria.ToUpper() == "N")
                return false;

            return true;
        }

        private bool Faturado(object objetoValor)
        {
            return (!(String.IsNullOrEmpty((objetoValor as RelatorioHistoricoFaturamentosDto).UsuarioEventoCarregamento)));
        }

        private bool SegmentoComTicketObrigatorio(object objetoValor)
        {
            var segmento = (objetoValor as RelatorioHistoricoFaturamentosDto).Segmento;
            if ((segmento == "LIQUID") || (segmento == "INDUSTR") || (segmento == "BRADO"))
                return false;

            if (String.IsNullOrEmpty(segmento))
                return false;

            return true;
        }

    }
}