﻿namespace Translogic.Modules.Core.Domain.Services.PainelExpedicao.Relatorio
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.IO;
    using OfficeOpenXml;
    using OfficeOpenXml.Style;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Modules.Core.Infrastructure.Services;

    public class RelatorioLaudosMercadoriaExcelService : GeracaoExcelCustomizadoService
    {
        private IList<PainelExpedicaoRelatorioLaudoDto> _valores = new List<PainelExpedicaoRelatorioLaudoDto>();

        private int _indiceUltimaColuna;

        public int IndiceUltimaColuna
        {
            get { return _indiceUltimaColuna; }
            set { _indiceUltimaColuna = value; }
        }

        public override int ObterIndiceUltimaColuna()
        {
            return this._indiceUltimaColuna;
        }

        public RelatorioLaudosMercadoriaExcelService(IList<PainelExpedicaoRelatorioLaudoDto> valores)
        {
            this._valores = valores;
            this._indiceUltimaColuna = 95; // Código da tabela ASCII que corresponde a letra da última coluna
        }

        public override Stream GerarRelatorioCustomizado(string tituloRelatorio,
                                                         IList<string> filtros,
                                                         IList<string> colunas)
        {

            Color colFontBlackFromHex = System.Drawing.ColorTranslator.FromHtml("#000000"); // Cor Preta
            Color colFontWhiteFromHex = System.Drawing.ColorTranslator.FromHtml("#ffffff"); // Cor Branca
            Color colFontRedFromHex = System.Drawing.ColorTranslator.FromHtml("#ff0000"); // Cor Vermelha
            Color colBackGroundBlueFromHex = System.Drawing.ColorTranslator.FromHtml("#ddebf7"); // Cor Azul Claro

            using (var p = new ExcelPackage(new MemoryStream()))
            {
                #region Aba

                p.Workbook.Properties.Author = "Translogic - Rumo ALL";
                p.Workbook.Properties.Title = tituloRelatorio;
                p.Workbook.Properties.Company = "Rumo ALL";

                p.Workbook.Worksheets.Add("Relatório Laudos");
                var worksheet = p.Workbook.Worksheets[1];

                #endregion

                #region Título

                worksheet.Cells["A1"].Value = tituloRelatorio;
                worksheet.Cells["A1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells["A1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                worksheet.Cells["A1"].Style.Font.Bold = true;
                worksheet.Cells["A1"].Style.Font.Size = 14;
                worksheet.Cells["A1:AF1"].Merge = true;
                worksheet.Cells["A1:AF1"].Style.Fill.PatternType = ExcelFillStyle.Solid;

                var colFromHex = System.Drawing.ColorTranslator.FromHtml("#DDEBF7");
                worksheet.Cells["A1:AF1"].Style.Fill.BackgroundColor.SetColor(colFromHex);

                #endregion

                int caracter = 65; // A
                char cFinal = (char)caracter;
                int linhaPosicao = 2;

                #region Valores

                caracter = 65;
                var ultrapassouZValor = false;
                var cellIndexValor = string.Empty;
                foreach (string co in colunas)
                {
                    char c = (char)caracter;
                    if (caracter > 90)
                    {
                        ultrapassouZValor = true;
                        c = 'A';
                        caracter = 'A';
                    }

                    cellIndexValor = string.Format("{0}{1}", ultrapassouZValor ? "A" : string.Empty,
                                                      c + linhaPosicao.ToString());

                    worksheet.Cells[cellIndexValor].Value = co;
                    worksheet.Cells[cellIndexValor].Style.Font.Color.SetColor(colFontWhiteFromHex);
                    worksheet.Cells[cellIndexValor].Style.Font.Bold = true;
                    worksheet.Cells[cellIndexValor].AutoFitColumns();
                    worksheet.Cells[cellIndexValor].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    caracter = caracter + 1;
                }

                colFromHex = System.Drawing.ColorTranslator.FromHtml("#9bc2e6");
                worksheet.Cells["A" + linhaPosicao.ToString() + ":" + cellIndexValor].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells["A" + linhaPosicao.ToString() + ":" + cellIndexValor].Style.Fill.BackgroundColor.SetColor(colFromHex);
                worksheet.Cells["A" + linhaPosicao.ToString() + ":" + cellIndexValor].Style.Font.Size = 11;
                worksheet.Cells["A" + linhaPosicao.ToString() + ":" + cellIndexValor].AutoFitColumns();

                linhaPosicao = linhaPosicao + 1;

                if (_valores != null)
                {
                    //int linhaValor = 4;
                    int indiceUltimaColuna = ObterIndiceUltimaColuna();
                    cFinal = (char)indiceUltimaColuna;
                    colFromHex = System.Drawing.ColorTranslator.FromHtml("#ffffff");
                    foreach (PainelExpedicaoRelatorioLaudoDto linha in _valores)
                    {
                        caracter = 65;
                        var caracterValor = 65;
                        var ultrapassouZValores = false;
                        var cellIndexValores = string.Empty;
                        while (caracter <= indiceUltimaColuna)
                        {
                            char c = (char)caracter;
                            char cIndex = (char)caracterValor;
                            if (caracterValor > 90)
                            {
                                ultrapassouZValores = true;
                                cIndex = 'A';
                                caracterValor = 'A';
                            }

                            cellIndexValores = string.Format("{0}{1}", ultrapassouZValores ? "A" : string.Empty,
                                                      cIndex + linhaPosicao.ToString());

                            worksheet.Cells[cellIndexValores].Value = ObterValorLinhaColuna(c, linha);

                            worksheet.Cells[cellIndexValores].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            var corFundo = linhaPosicao % 2 == 0 ? colBackGroundBlueFromHex : colFromHex;
                            worksheet.Cells[cellIndexValores].Style.Fill.BackgroundColor.SetColor(corFundo);

                            worksheet.Cells[cellIndexValores].Style.Font.Color.SetColor(colFontBlackFromHex);

                            caracter = caracter + 1;
                            caracterValor = caracterValor + 1;
                        }

                        worksheet.Cells["A" + linhaPosicao.ToString() + ":" + cellIndexValores].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        worksheet.Cells["A" + linhaPosicao.ToString() + ":" + cellIndexValores].AutoFitColumns();
                        worksheet.Cells["A" + linhaPosicao.ToString() + ":" + cellIndexValores].Style.Font.Size = 11;

                        linhaPosicao = linhaPosicao + 1;
                    }
                }

                #endregion

                p.Save();
                return p.Stream;
            }
        }

        public override string ObterValorLinhaColuna(char coluna, object objetoValor)
        {
            if (objetoValor == null)
                return String.Empty;

            decimal? valor = null;

            switch (coluna)
            {
                case 'A':
                    return (objetoValor as PainelExpedicaoRelatorioLaudoDto).DataCarregamento.ToString("dd/MM/yyyy");
                case 'B':
                    return (objetoValor as PainelExpedicaoRelatorioLaudoDto).Vagao;
                case 'C':
                    return (objetoValor as PainelExpedicaoRelatorioLaudoDto).RemetenteNfe;
                case 'D':
                    return (objetoValor as PainelExpedicaoRelatorioLaudoDto).NumeroNfe;
                case 'E':
                    return (objetoValor as PainelExpedicaoRelatorioLaudoDto).Origem;
                case 'F':
                    return (objetoValor as PainelExpedicaoRelatorioLaudoDto).Destino;
                case 'G':
                    return (objetoValor as PainelExpedicaoRelatorioLaudoDto).FluxoSegmento;
                case 'H':
                    return (objetoValor as PainelExpedicaoRelatorioLaudoDto).DataRecebimentoLaudo.ToString("dd/MM/yyyy");
                case 'I':
                    return (objetoValor as PainelExpedicaoRelatorioLaudoDto).LaudoTipo;
                case 'J':
                    return (objetoValor as PainelExpedicaoRelatorioLaudoDto).Produto;
                case 'K':
                    return (objetoValor as PainelExpedicaoRelatorioLaudoDto).Empresa;
                case 'L':
                    return (objetoValor as PainelExpedicaoRelatorioLaudoDto).CnpjEmpresaFormatado;
                case 'M':
                    return (objetoValor as PainelExpedicaoRelatorioLaudoDto).NumeroOsLaudo;
                case 'N':
                    return (objetoValor as PainelExpedicaoRelatorioLaudoDto).NumeroLaudo;
                case 'O':
                    valor = (objetoValor as PainelExpedicaoRelatorioLaudoDto).ClassificacaoUmidade;
                    break;
                case 'P':
                    valor = (objetoValor as PainelExpedicaoRelatorioLaudoDto).ClassificacaoMateriaEstranhaImpura;
                    break;
                case 'Q':
                    valor = (objetoValor as PainelExpedicaoRelatorioLaudoDto).ClassificacaoTotalAvariados;
                    break;
                case 'R':
                    valor = (objetoValor as PainelExpedicaoRelatorioLaudoDto).ClassificacaoDanificado;
                    break;
                case 'S':
                    valor = (objetoValor as PainelExpedicaoRelatorioLaudoDto).ClassificacaoQuebrado;
                    break;
                case 'T':
                    valor = (objetoValor as PainelExpedicaoRelatorioLaudoDto).ClassificacaoArdido;
                    break;
                case 'U':
                    valor = (objetoValor as PainelExpedicaoRelatorioLaudoDto).ClassificacaoPh;
                    break;
                case 'V':
                    valor = (objetoValor as PainelExpedicaoRelatorioLaudoDto).ClassificacaoEsverdeado;
                    break;
                case 'W':
                    valor = (objetoValor as PainelExpedicaoRelatorioLaudoDto).ClassificacaoQueimado;
                    break;
                case 'X':
                    valor = (objetoValor as PainelExpedicaoRelatorioLaudoDto).ClassificacaoPicado;
                    break;
                case 'Y':
                    valor = (objetoValor as PainelExpedicaoRelatorioLaudoDto).ClassificacaoMofado;
                    break;
                case 'Z':
                    valor = (objetoValor as PainelExpedicaoRelatorioLaudoDto).ClassificacaoFallingNumber;
                    break;
                case (char)91:
                    valor = (objetoValor as PainelExpedicaoRelatorioLaudoDto).ClassificacaoGerminado;
                    break;
                case (char)92:
                    valor = (objetoValor as PainelExpedicaoRelatorioLaudoDto).ClassificacaoDanificadoInsetos;
                    break;
                case (char)93:
                    return (objetoValor as PainelExpedicaoRelatorioLaudoDto).Classificador;
                case (char)94:
                    return (objetoValor as PainelExpedicaoRelatorioLaudoDto).CpfClassificadorFormatado;
                case (char)95:
                    return (objetoValor as PainelExpedicaoRelatorioLaudoDto).FluxoComercial;
                default:
                    return String.Empty;
            }

            return valor.HasValue ? valor.Value.ToString("N2") : "-";
        }

        public IList<string> RetornaNomeColunas()
        {
            var colunas = new List<string>();

            colunas.Add("Data Carregamento");
            colunas.Add("Vagão");
            colunas.Add("Remetente NF-e");
            colunas.Add("Número NF-e");
            colunas.Add("Origem");
            colunas.Add("Destino");
            colunas.Add("Segmento");
            colunas.Add("Data Recebimento");
            colunas.Add("Laudo Tipo");
            colunas.Add("Produto");
            colunas.Add("Empresa");
            colunas.Add("CNPJ");
            colunas.Add("Ordem de Serviço");
            colunas.Add("Número Laudo");
            colunas.Add("Umidade");
            colunas.Add("Materia Estranha/Impura");
            colunas.Add("Total de Avariados");
            colunas.Add("Danificado");
            colunas.Add("Quebrado");
            colunas.Add("Ardido");
            colunas.Add("PH");
            colunas.Add("Esverdeado");
            colunas.Add("Queimado");
            colunas.Add("Picado");
            colunas.Add("Mofado");
            colunas.Add("Falling Number");
            colunas.Add("Germinado");
            colunas.Add("Danificado Insetos");
            colunas.Add("Classificador");
            colunas.Add("CPF");
            colunas.Add("Fluxo Comercial");

            return colunas;
        }
    }
}