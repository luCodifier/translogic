﻿namespace Translogic.Modules.Core.Domain.Services.PainelExpedicao
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using ALL.Core.Dominio;
    using Castle.Services.Transaction;
    using Translogic.Core.Infrastructure;
    using Translogic.Core.Infrastructure.Web;
    using Translogic.Modules.Core.Domain.Model.Acesso;
    using Translogic.Modules.Core.Domain.Model.Acesso.Repositories;
    using Translogic.Modules.Core.Domain.Model.Bolar;
    using Translogic.Modules.Core.Domain.Model.Bolar.Repositories;
    using Translogic.Modules.Core.Domain.Model.Codificador.Repositories;
    using Translogic.Modules.Core.Domain.Model.Diversos.Repositories;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Modules.Core.Domain.Model.Estrutura;
    using Translogic.Modules.Core.Domain.Model.Estrutura.Repositories;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos.Repositories;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Repositories;
    using Translogic.Modules.Core.Domain.Model.PainelExpedicao.Documentos.Enums;
    using Translogic.Modules.Core.Domain.Model.PainelExpedicao.Parametrizacao;
    using Translogic.Modules.Core.Domain.Model.PainelExpedicao.Parametrizacao.Enums;
    using Translogic.Modules.Core.Domain.Model.PainelExpedicao.Repositories;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao.Repositories;
    using Translogic.Modules.Core.Domain.Model.Via;
    using Translogic.Modules.Core.Domain.Model.Via.Repositories;
    using Translogic.Modules.Core.Domain.Services.Ctes;
    using System.Diagnostics;
    using System.Reflection;

    public class PedraIntegracaoService
    {
        private readonly IPainelExpedicaoRepository _painelExpedicaoRepository;

        /// <summary>
        ///  Initializes a new instance of the <see cref="PainelExpedicaoService"/> class.
        /// </summary>
        /// <param name="painelExpedicaoRepository">Repositório do Painel de Expedição</param>
        public PedraIntegracaoService(IPainelExpedicaoRepository painelExpedicaoRepository)
        {
            this._painelExpedicaoRepository = painelExpedicaoRepository;
        }

        private void ExecutarConsultaPedraSADE(string _source = "Translogic.JobRunner")
        {
            LogHelper.Log(String.Format("{0}-{1}","Inicio GerenciamentoDocumentacaoJob", _source), EventLogEntryType.Information);

            try
            {
                //Consulta pedra no SADE
            }
            catch (Exception ex)
            {
                LogHelper.Log(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
            }

            LogHelper.Log(String.Format("{0}-{1}", "Fim GerenciamentoDocumentacaoJob", _source), EventLogEntryType.Information);
        }


        private List<string> ExecutarConsultaRealizadoTranslogic(string _source = "Translogic.JobRunner")
        {
            LogHelper.Log(String.Format("{0}-{1}", "Inicio GerenciamentoDocumentacaoJob", _source), EventLogEntryType.Information);

            try
            {
                //Consulta pedra no SADE
            }
            catch (Exception ex)
            {
                LogHelper.Log(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
            }

            LogHelper.Log(String.Format("{0}-{1}", "Fim GerenciamentoDocumentacaoJob", _source), EventLogEntryType.Information);

            return new List<string>();

        }

        private List<string> ExecutarConsultaRealizadoXPlanejadoTranslogic(string _source = "Translogic.JobRunner")
        {
            LogHelper.Log(String.Format("{0}-{1}", "Inicio GerenciamentoDocumentacaoJob", _source), EventLogEntryType.Information);

            try
            {
                //Consulta pedra no SADE
            }
            catch (Exception ex)
            {
                LogHelper.Log(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
            }

            LogHelper.Log(String.Format("{0}-{1}", "Fim GerenciamentoDocumentacaoJob", _source), EventLogEntryType.Information);

            return new List<string>();

        }

        private List<string> InsertUpdatePlanejadoXRealizadoTranslogic(string _source = "Translogic.JobRunner")
        {
            LogHelper.Log(String.Format("{0}-{1}", "Inicio GerenciamentoDocumentacaoJob", _source), EventLogEntryType.Information);

            try
            {
                //Consulta pedra no SADE
            }
            catch (Exception ex)
            {
                LogHelper.Log(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
            }

            LogHelper.Log(String.Format("{0}-{1}", "Fim GerenciamentoDocumentacaoJob", _source), EventLogEntryType.Information);

            return new List<string>();
        }

        public List<string> AtualizaPlanejadoXRealizado(string _source = "Translogic.JobRunner")
        {
            LogHelper.Log(String.Format("{0}-{1}", "Inicio GerenciamentoDocumentacaoJob", _source), EventLogEntryType.Information);

            try
            {
                //Consulta pedra no SADE
            }
            catch (Exception ex)
            {
                LogHelper.Log(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
            }

            LogHelper.Log(String.Format("{0}-{1}", "Fim GerenciamentoDocumentacaoJob", _source), EventLogEntryType.Information);

            return new List<string>();
        }


    }
}