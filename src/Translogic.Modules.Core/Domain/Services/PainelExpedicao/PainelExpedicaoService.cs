﻿namespace Translogic.Modules.Core.Domain.Services.PainelExpedicao
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using ALL.Core.Dominio;
    using Castle.Services.Transaction;
    using Translogic.Core.Infrastructure;
    using Translogic.Core.Infrastructure.Web;
    using Translogic.Modules.Core.Domain.Model.Acesso;
    using Translogic.Modules.Core.Domain.Model.Acesso.Repositories;
    using Translogic.Modules.Core.Domain.Model.Bolar;
    using Translogic.Modules.Core.Domain.Model.Bolar.Repositories;
    using Translogic.Modules.Core.Domain.Model.Codificador.Repositories;
    using Translogic.Modules.Core.Domain.Model.Diversos.Repositories;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Modules.Core.Domain.Model.Estrutura;
    using Translogic.Modules.Core.Domain.Model.Estrutura.Repositories;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos.Repositories;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Repositories;
    using Translogic.Modules.Core.Domain.Model.PainelExpedicao.Documentos.Enums;
    using Translogic.Modules.Core.Domain.Model.PainelExpedicao.Parametrizacao;
    using Translogic.Modules.Core.Domain.Model.PainelExpedicao.Parametrizacao.Enums;
    using Translogic.Modules.Core.Domain.Model.PainelExpedicao.Repositories;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao.Repositories;
    using Translogic.Modules.Core.Domain.Model.Via;
    using Translogic.Modules.Core.Domain.Model.Via.Repositories;
    using Translogic.Modules.Core.Domain.Model.PainelExpedicao;
    using Translogic.Modules.Core.Domain.Model.Codificador;
    using System.Net.Mail;
    using System.Net;
    using System.Net.Mime;
    using System.IO;
    using System.Web.UI;
    using System.Web;
    using Translogic.Modules.Core.Spd.BLL;
    using Translogic.Modules.Core.Spd.Data;

    public class PainelExpedicaoService
    {
        private readonly IPainelExpedicaoRepository _painelExpedicaoRepository;
        private readonly IOrigemRepository _painelExpedicaoOrigemRepository;
        private readonly IPainelExpedicaoIndicadoresRepository _painelExpedicaoIndicadoresRepository;
        private readonly IEstacaoMaeRepository _painelExpedicaoEstacaoMaeRepository;
        private readonly IConfiguracaoTranslogicRepository _configGeralRepository;
        private readonly IMercadoriaRepository _mercadoriaRepository;
        private readonly IFluxoComercialRepository _fluxoComercialRepository;
        private readonly IEmpresaClienteRepository _empresaClienteRepository;
        private readonly IVagaoRepository _vagaoRepository;
        private readonly IGrupoUsuarioRepository _grupoUsuarioRepository;
        private readonly IAgrupamentoUsuarioRepository _agrupamentoUsuarioRepository;
        private readonly IDespachoLocalBloqueioRepository _despachoLocalBloqueioRepository;
        private readonly IClienteTipoIntegracaoRepository _clienteTipoIntegracaoRepository;
        private readonly IBolarBoImpCfgRepository _cfgRepository;
        private readonly IMalhaRepository _malhaRepository;
        private readonly IUsuarioRepository _usuarioRepository;
        private readonly IDensidadeMercadoriaRepository _densidadeMercadoriaRepository;
        private readonly IConfiguracaoTranslogicRepository _configuracaoTranslogicRepository;


        private static readonly string _acaoConfirmarRecusarSuper = "ConfirmarRecusarFull";

        /// <summary>
        ///  Initializes a new instance of the <see cref="PainelExpedicaoService"/> class.
        /// </summary>
        /// <param name="painelExpedicaoRepository">Repositório do Painel de Expedição</param>
        /// <param name="painelExpedicaoOrigemRepository">Repositório das Origens do Painel de Expedição</param>
        /// <param name="painelExpedicaoIndicadoresRepository">Repositório dos Indicadores do Painel de Expedição</param>
        /// <param name="painelExpedicaoEstacaoMaeRepository">Repositório da Estação Mão</param>
        /// <param name="configGeralRepository">Repositório da Configuração Geral</param>
        /// <param name="mercadoriaRepository">Repositório da Mercadoria</param>
        /// <param name="fluxoComercialRepository">Repositório do Fluxo Comercial</param>
        /// <param name="empresaClienteRepository">Repositório da EmpresaCliente</param>
        /// <param name="vagaoRepository">Repositório do Vagão</param>
        /// <param name="grupoUsuarioRepository">Repositório do Grupo do Usuário</param>
        /// <param name="agrupamentoUsuarioRepository">Repositório do Agrupamento do Usuário</param>
        /// <param name="despachoLocalBloqueioRepository">Repositório do Local de Bloqueio de Despacho</param>
        /// <param name="clienteTipoIntegracaoRepository">Repositório do Tipo de Integrção do Cliente</param>
        /// <param name="cfgRepository">Repositório das Configurações</param>
        /// <param name="malhaRepository">Repositório das Malhas</param>
        /// <param name="usuarioRepository">Repositório de Usuários</param>
        /// <param name="densidadeMercadoriaRepository">Repositório de Densidade das Mercadorias</param>
        public PainelExpedicaoService(IPainelExpedicaoRepository painelExpedicaoRepository,
                                      IOrigemRepository painelExpedicaoOrigemRepository,
                                      IPainelExpedicaoIndicadoresRepository painelExpedicaoIndicadoresRepository,
                                      IEstacaoMaeRepository painelExpedicaoEstacaoMaeRepository,
                                      IConfiguracaoTranslogicRepository configGeralRepository,
                                      IMercadoriaRepository mercadoriaRepository,
                                      IFluxoComercialRepository fluxoComercialRepository,
                                      IEmpresaClienteRepository empresaClienteRepository,
                                      IVagaoRepository vagaoRepository,
                                      IGrupoUsuarioRepository grupoUsuarioRepository,
                                      IAgrupamentoUsuarioRepository agrupamentoUsuarioRepository,
                                      IDespachoLocalBloqueioRepository despachoLocalBloqueioRepository,
                                      IClienteTipoIntegracaoRepository clienteTipoIntegracaoRepository,
                                      IBolarBoImpCfgRepository cfgRepository,
                                      IMalhaRepository malhaRepository,
                                      IUsuarioRepository usuarioRepository,
                                      IDensidadeMercadoriaRepository densidadeMercadoriaRepository,
                                      IConfiguracaoTranslogicRepository configuracaoTranslogicRepository)
        {
            this._painelExpedicaoRepository = painelExpedicaoRepository;
            this._painelExpedicaoOrigemRepository = painelExpedicaoOrigemRepository;
            this._painelExpedicaoIndicadoresRepository = painelExpedicaoIndicadoresRepository;
            this._painelExpedicaoEstacaoMaeRepository = painelExpedicaoEstacaoMaeRepository;
            this._configGeralRepository = configGeralRepository;
            this._mercadoriaRepository = mercadoriaRepository;
            this._fluxoComercialRepository = fluxoComercialRepository;
            this._empresaClienteRepository = empresaClienteRepository;
            this._vagaoRepository = vagaoRepository;
            this._grupoUsuarioRepository = grupoUsuarioRepository;
            this._agrupamentoUsuarioRepository = agrupamentoUsuarioRepository;
            this._despachoLocalBloqueioRepository = despachoLocalBloqueioRepository;
            this._clienteTipoIntegracaoRepository = clienteTipoIntegracaoRepository;
            this._cfgRepository = cfgRepository;
            this._malhaRepository = malhaRepository;
            this._usuarioRepository = usuarioRepository;
            this._densidadeMercadoriaRepository = densidadeMercadoriaRepository;
            //this._operacaoMalhaRepository = operacaoRepository;
            //this._regiaoRepository = regiaoRepository;
            //this._regraEmailsRepository = regraEmailsRepository;
            this._configuracaoTranslogicRepository = configuracaoTranslogicRepository;


        }

        #region Visualização

        /// <summary>
        /// Obtem as informacoes do quadro de visualizacao do Painel de Expedicao
        /// </summary>
        /// <returns></returns>
        public IList<PainelExpedicaoVisualizacaoDto> ObterPainelExpedicaoVisualizacao(DetalhesPaginacaoWeb pagination,
                                                                                     DateTime dataInicio,
                                                                                     string uf,
                                                                                     string origem,
                                                                                     string origemFaturamento,
                                                                                     string segmento,
                                                                                     int situacaoVisualizacaoId,
                                                                                     string malhaCentral,
                                                                                     string metricaLarga,
                                                                                     string metricaNorte,
                                                                                     string metricaSul)
        {

            uf = (String.IsNullOrEmpty(uf) ? "" : uf);
            uf = uf.Trim().ToUpper();
            uf = (uf == "TODAS" ? "" : uf);

            origem = (String.IsNullOrEmpty(origem) ? "" : origem);
            origem = origem.Trim().ToUpper();

            origemFaturamento = (String.IsNullOrEmpty(origemFaturamento) ? String.Empty : origemFaturamento);
            origemFaturamento = origemFaturamento.Trim().ToUpper();

            segmento = (String.IsNullOrEmpty(segmento) ? "" : segmento);
            segmento = segmento.Trim().ToUpper();
            segmento = (segmento == "TODOS" ? "" : segmento);

            origemFaturamento = (String.IsNullOrEmpty(origemFaturamento) ? String.Empty : origemFaturamento);
            origemFaturamento = origemFaturamento.Trim().ToUpper();
            origemFaturamento = (origemFaturamento == "TODOS" ? String.Empty : origemFaturamento);

            malhaCentral = (String.IsNullOrEmpty(malhaCentral) ? "" : malhaCentral);
            malhaCentral = malhaCentral.Trim().ToUpper();
            malhaCentral = (malhaCentral == "TODAS" ? "" : malhaCentral);

            var statusMetricaLarga = true;
            if (!string.IsNullOrEmpty(metricaLarga))
            {
                if (!bool.TryParse(metricaLarga, out statusMetricaLarga))
                    statusMetricaLarga = true;
            }

            var statusMetricaNorte = true;
            if (!string.IsNullOrEmpty(metricaNorte))
            {
                if (!bool.TryParse(metricaNorte, out statusMetricaNorte))
                    statusMetricaNorte = true;
            }

            var statusMetricaSul = true;
            if (!string.IsNullOrEmpty(metricaSul))
            {
                if (!bool.TryParse(metricaSul, out statusMetricaSul))
                    statusMetricaSul = true;
            }

            var result = _painelExpedicaoRepository.ObterPainelExpedicaoVisualizacao(pagination,
                                                                                     dataInicio,
                                                                                     uf,
                                                                                     origem,
                                                                                     origemFaturamento,
                                                                                     segmento,
                                                                                     malhaCentral,
                                                                                     statusMetricaLarga,
                                                                                     statusMetricaNorte,
                                                                                     statusMetricaSul);
            var indicadores = this.ObterIndicadores(null);
            int tempoMilisegundosRefreshTela = 99999; // this.ObterPainelExpedicaoTempoRefresh();

            AtualizaColunasValor(ref result);

            if (indicadores != null)
            {
                foreach (Indicador i in indicadores)
                {
                    decimal valorIni = i.Valor1;
                    string operadorIni = i.Operador1.Simbolo;
                    bool bExisteValorFim = (i.OperadorLogicoEnum == OperadorLogicoEnum.Nenhum ? false : true);
                    decimal? valorFim = i.Valor2;
                    string operadorFim = (i.Operador2 != null ? i.Operador2.Simbolo : null);
                    CorEnum cor = i.CorEnum;

                    AtualizaCorColunasIndicadores(i.ColunaEnum,
                                                    valorIni,
                                                    operadorIni,
                                                    bExisteValorFim,
                                                    valorFim,
                                                    operadorFim,
                                                    cor,
                                                    ref result);
                }
            }

            AtualizaColunasVisibilidade(situacaoVisualizacaoId, ref result);

            return result;
        }

        /// <summary>
        /// Obtem as informacoes do quadro de visualizacao do Painel de Expedicao
        /// </summary>
        /// <returns></returns>
        //public IList<PainelExpedicaoPedraDto> ObterPainelExpedicaoPedra()
        public IList<VwPedraRealizado> ObterPainelExpedicaoPedra(string operacao, string regiao, string dataPedra, string uf, string origem, string estacaoFaturamento, string segmento, string ocultarPedraZerada)
        {
            var wherePedraSade = new StringBuilder(@" TO_DATE(DATA_PEDRA) = TO_DATE({filtro_dataPedra}, 'DD/MM/YYYY')
                                            {filtro_operacao} 
                                            {filtro_regiao}
                                            {filtro_uf}
                                            {filtro_origem}
                                            {filtro_estacaoFaturamento}
                                            {filtro_segmento}");

            uf = (String.IsNullOrEmpty(uf) ? "" : uf);
            uf = uf.Trim().ToUpper();
            uf = (uf == "TODOS" ? "" : uf);

            origem = (String.IsNullOrEmpty(origem) ? "" : origem);
            origem = origem.Trim().ToUpper();

            estacaoFaturamento = (String.IsNullOrEmpty(estacaoFaturamento) ? String.Empty : estacaoFaturamento);
            estacaoFaturamento = estacaoFaturamento.Trim().ToUpper();

            segmento = (String.IsNullOrEmpty(segmento) ? "" : segmento);
            segmento = segmento.Trim().ToUpper();
            segmento = (segmento == "TODOS" ? "" : segmento);

            operacao = (String.IsNullOrEmpty(operacao) ? String.Empty : operacao);
            operacao = operacao.Trim().ToUpper();
            operacao = (operacao == "TODOS" ? String.Empty : operacao);

            regiao = (String.IsNullOrEmpty(regiao) ? "" : regiao);
            regiao = regiao.Trim().ToUpper();
            regiao = (regiao == "TODOS" ? "" : regiao);

            ocultarPedraZerada = (String.IsNullOrEmpty(ocultarPedraZerada) ? "" : ocultarPedraZerada);
            ocultarPedraZerada = ocultarPedraZerada.Trim().ToUpper();
            ocultarPedraZerada = (ocultarPedraZerada == "TODAS" ? "" : ocultarPedraZerada);

            dataPedra = dataPedra.ToUpper();

            if (!string.IsNullOrWhiteSpace(dataPedra))
            {
                wherePedraSade.Replace("{filtro_dataPedra}", String.Format("'{0}' ", dataPedra));
            }
            else
            {
                wherePedraSade.Replace("{filtro_dataPedra}", String.Format("'{0}' ", DateTime.Now.ToString("dd/MM/yyyy")));
            }

            operacao = operacao.ToUpper().Trim();
            if (!string.IsNullOrWhiteSpace(operacao) && operacao != "TODOS" && operacao != "TODAS" && operacao != "0")
            {
                wherePedraSade.Replace("{filtro_operacao}", "AND OPERACAO_ID = " + operacao);
            }
            else
            {
                wherePedraSade.Replace("{filtro_operacao}", "");
            }

            regiao = regiao.ToUpper();
            if (!string.IsNullOrWhiteSpace(regiao) && regiao != "TODOS" && regiao != "TODAS" && regiao != "0")
            {
                wherePedraSade.Replace("{filtro_regiao}", "AND REGIAO_ID =" + regiao);
            }
            else
            {
                wherePedraSade.Replace("{filtro_regiao}", "");
            }

            uf = uf.ToUpper();
            if (!string.IsNullOrWhiteSpace(uf) && uf != "TODOS" && uf != "TODAS" && uf != "0")
            {
                wherePedraSade.Replace("{filtro_uf}", "AND UF = '" + uf + "' ");
            }
            else
            {
                wherePedraSade.Replace("{filtro_uf}", "");
            }

            origem = origem.ToUpper();
            if (!string.IsNullOrWhiteSpace(origem) && origem != "TODOS" && origem != "TODAS" && origem != "0")
            {
                wherePedraSade.Replace("{filtro_origem}", "AND ESTACAO_ORIGEM = '" + origem + "' ");
            }
            else
            {
                wherePedraSade.Replace("{filtro_origem}", "");
            }

            estacaoFaturamento = estacaoFaturamento.ToUpper();
            if (!string.IsNullOrWhiteSpace(estacaoFaturamento) && estacaoFaturamento != "TODOS" && estacaoFaturamento != "TODAS" && estacaoFaturamento != "0")
            {
                wherePedraSade.Replace("{filtro_estacaoFaturamento}", "AND ESTACAO_FATURAMENTO_CODIGO = '" + estacaoFaturamento + "' ");
            }
            else
            {
                wherePedraSade.Replace("{filtro_estacaoFaturamento}", "");
            }

            segmento = segmento.ToUpper();
            if (!string.IsNullOrWhiteSpace(segmento) && segmento != "TODOS" && segmento != "TODAS" && segmento != "0")
            {
                wherePedraSade.Replace("{filtro_segmento}", "AND SEGMENTO = '" + segmento + "'");
            }
            else
            {
                wherePedraSade.Replace("{filtro_segmento}", "");
            }

            var pedraSade = BL_VwPedraRealizado.Select(wherePedraSade.ToString());


            var wherePedraEditada = new StringBuilder($" TO_DATE(DATA_PEDRA) = TO_DATE('{dataPedra}', 'DD/MM/YYYY')");
            var pedraEditada = BL_PePedraRealizado.Select(wherePedraEditada.ToString());

            if (pedraEditada.Count > 0)
            {
                for (int i = 0; i < pedraSade.Count; i++)
                {
                    var editado = pedraEditada.FirstOrDefault(p => p.Segmento == pedraSade[i].Segmento && p.DataPedra == pedraSade[i].DataPedra && p.Terminal == pedraSade[i].Terminal && p.EstacaoOrigem == pedraSade[i].EstacaoOrigemId);
                    if (editado != null)
                    {
                        pedraSade[i].PedraEditado = ((editado.DataAlteracao ?? editado.DataCadastro).Value > pedraSade[i].RpTimestamp.Value) && editado.PedraEditado >= 0 ?  editado.PedraEditado : 0;
                        pedraSade[i].StatusEditado = editado.StatusEditado;
                        pedraSade[i].PedraRealizadoId = editado.Id;
                    }
                }
            }


            return pedraSade.Where(p => ocultarPedraZerada.ToUpper() == "FALSE" || p.PedraZerada == false).ToList();
        }

        public string GetHtml<T>(Dictionary<string, Func<T, string>> fields, IList<T> list, string corpo)
        {
            StringWriter sw = new StringWriter();
            HtmlTextWriter tw = new HtmlTextWriter(sw);
            tw.RenderBeginTag(HtmlTextWriterTag.Div);
            tw.Write(HttpUtility.HtmlEncode(corpo));
            tw.RenderEndTag();

            tw.RenderBeginTag(HtmlTextWriterTag.Div);
            tw.AddStyleAttribute("border", "2px solid #ddd");
            tw.AddStyleAttribute("border-collapse", "collapse");
            tw.RenderBeginTag(HtmlTextWriterTag.Table);
            // Cria a linha do titulo
            tw.AddStyleAttribute("font-weight", "bold");
            tw.RenderBeginTag(HtmlTextWriterTag.Tr);
            foreach (string key in fields.Keys)
            {
                tw.AddStyleAttribute("border", "1px solid #ddd");
                tw.RenderBeginTag(HtmlTextWriterTag.Td);
                tw.Write(HttpUtility.HtmlEncode(key));
                tw.RenderEndTag();
            }

            tw.RenderEndTag();
            for (int i = 0; i <= list.Count; i++)
            {
                tw.RenderBeginTag(HtmlTextWriterTag.Tr);
                foreach (var key in fields.Keys)
                {
                    Func<T, string> valor = fields[key];
                    var htmlValor = valor.Invoke(list[i]);

                    //color: red; font - weight: bold;
                    int valorConvertido = 0;

                    if (key.ToUpper().Equals("MONITORAR") && Int32.TryParse(htmlValor, out valorConvertido) && valorConvertido < 0)
                    {
                        tw.AddStyleAttribute("border", "1px solid #ddd");
                        tw.AddStyleAttribute("color", "red");
                        tw.AddStyleAttribute("font-weight", "bold");
                        tw.AddStyleAttribute("text-align", "center");
                    }
                    else
                    {
                        tw.AddStyleAttribute("border", "1px solid #ddd");
                        tw.AddStyleAttribute("text-align", "center");
                    }

                    tw.RenderBeginTag(HtmlTextWriterTag.Td);
                    tw.Write(HttpUtility.HtmlEncode(htmlValor));
                    tw.RenderEndTag();
                }

                tw.RenderEndTag();
            }

            tw.RenderEndTag();
            tw.RenderEndTag();

            return sw.ToString();
        }

        public string GetHtmlPedra(List<Dictionary<string, Func<VwPedraRealizado, string>>> dicList, List<string> sumarios, List<List<VwPedraRealizado>> dataList, string corpo)
        {
            StringWriter sw = new StringWriter();
            HtmlTextWriter tw = new HtmlTextWriter(sw);
            tw.AddStyleAttribute("font-size", "10.0pt");
            tw.AddStyleAttribute("font-family", "Calibri, sans-serif");
            tw.RenderBeginTag(HtmlTextWriterTag.Div);
            tw.Write(HttpUtility.HtmlEncode(corpo));
            tw.RenderEndTag();

            #region empty line
            tw.RenderBeginTag(HtmlTextWriterTag.Br);
            #endregion empty line


            for (int x = 0; x < dicList.Count(); x++)
            {
                #region tabela 
                tw.RenderBeginTag(HtmlTextWriterTag.Div);
                tw.AddStyleAttribute("border", "2px solid #ddd");
                tw.AddStyleAttribute("border-spacing", "0px");
                tw.AddStyleAttribute("border-collapse", "collapse");
                tw.RenderBeginTag(HtmlTextWriterTag.Table);

                // Cria a linha do titulo
                tw.AddStyleAttribute("font-weight", "bold");
                tw.RenderBeginTag(HtmlTextWriterTag.Tr);
                tw.AddStyleAttribute("font-size", "8.0pt");
                tw.AddStyleAttribute("border", "1px solid #ddd");
                tw.AddStyleAttribute("text-align", "center");
                tw.AddStyleAttribute("font-family", "Calibri, sans-serif");
                tw.AddStyleAttribute("color", "white");
                tw.AddStyleAttribute("font-weight", "700");
                tw.AddStyleAttribute("background", "#003366");
                tw.AddStyleAttribute("padding", "2px 6px 2px 6px");
                tw.AddAttribute("COLSPAN", dicList[x].Keys.Count.ToString());
                tw.RenderBeginTag(HtmlTextWriterTag.Td);
                switch (x)
                {
                    case 0:
                        tw.Write("REGIÃO");
                        break;
                    case 1:
                        tw.Write("ORIGEM");
                        break;
                    case 2:
                        tw.Write("SEGMENTO");
                        break;
                    case 3:
                        tw.Write("DETALHE");
                        break;
                    default:
                        break;
                }


                //fecha TD
                tw.RenderEndTag();
                //fecha TR
                tw.RenderEndTag();

                // Cria a linha do titulo
                tw.AddStyleAttribute("font-weight", "bold");
                tw.RenderBeginTag(HtmlTextWriterTag.Tr);
                foreach (string key in dicList[x].Keys)
                {
                    tw.AddStyleAttribute("font-size", "8.0pt");
                    tw.AddStyleAttribute("border", "1px solid #ddd");
                    tw.AddStyleAttribute("text-align", "center");
                    tw.AddStyleAttribute("font-family", "Calibri, sans-serif");
                    tw.AddStyleAttribute("color", "white");
                    tw.AddStyleAttribute("font-weight", "700");
                    tw.AddStyleAttribute("background", "#003366");
                    tw.AddStyleAttribute("padding", "2px 6px 2px 6px");
                    tw.RenderBeginTag(HtmlTextWriterTag.Td);
                    tw.Write(HttpUtility.HtmlEncode(key));
                    tw.RenderEndTag();
                }

                tw.RenderEndTag();
                for (int i = 0; i < dataList[x].Count - 1; i++)
                {
                    tw.RenderBeginTag(HtmlTextWriterTag.Tr);
                    foreach (var key in dicList[x].Keys)
                    {
                        Func<VwPedraRealizado, string> valor = dicList[x][key];
                        var htmlValor = valor.Invoke(dataList[x][i]).ToUpper();

                        //color: red; font - weight: bold;
                        int valorConvertido = 0;

                        if (key.ToUpper().Equals("MONITORAR") && Int32.TryParse(htmlValor, out valorConvertido) && valorConvertido < 0)
                        {
                            tw.AddStyleAttribute("font-size", "8.0pt");
                            tw.AddStyleAttribute("border", "1px solid #ddd");
                            tw.AddStyleAttribute("color", "red");
                            tw.AddStyleAttribute("font-weight", "bold");
                            tw.AddStyleAttribute("text-align", "center");
                            tw.AddStyleAttribute("font-family", "Calibri, sans-serif");
                            tw.AddStyleAttribute("padding", "2px 6px 2px 6px");
                        }
                        else if (key.ToUpper().Equals("MONITORAR") && Int32.TryParse(htmlValor, out valorConvertido) && valorConvertido >= 0)
                        {
                            tw.AddStyleAttribute("font-size", "8.0pt");
                            tw.AddStyleAttribute("border", "1px solid #ddd");
                            tw.AddStyleAttribute("color", "green");
                            tw.AddStyleAttribute("font-weight", "bold");
                            tw.AddStyleAttribute("text-align", "center");
                            tw.AddStyleAttribute("font-family", "Calibri, sans-serif");
                            tw.AddStyleAttribute("padding", "2px 6px 2px 6px");
                        }
                        else
                        {
                            tw.AddStyleAttribute("font-size", "8.0pt");
                            tw.AddStyleAttribute("border", "1px solid #ddd");
                            tw.AddStyleAttribute("text-align", "center");
                            tw.AddStyleAttribute("font-family", "Calibri, sans-serif");
                            tw.AddStyleAttribute("padding", "2px 6px 2px 6px");
                        }

                        tw.RenderBeginTag(HtmlTextWriterTag.Td);
                        tw.Write(HttpUtility.HtmlEncode(htmlValor));
                        tw.RenderEndTag();
                    }

                    tw.RenderEndTag();
                }
                // Cria a linha do sumario
                tw.AddStyleAttribute("font-size", "8.0pt");
                tw.AddStyleAttribute("border", "1px solid #ddd");
                tw.AddStyleAttribute("text-align", "center");
                tw.AddStyleAttribute("font-family", "Calibri, sans-serif");
                tw.AddStyleAttribute("color", "white");
                tw.AddStyleAttribute("font-weight", "700");
                tw.AddStyleAttribute("background", "#003366");
                tw.AddStyleAttribute("padding", "2px 6px 2px 6px");

                tw.RenderBeginTag(HtmlTextWriterTag.Tr);
                foreach (string key in dicList[x].Keys)
                {
                    Func<VwPedraRealizado, string> valor = dicList[x][key];
                    var htmlValor = String.Empty;

                    if (sumarios.Contains(key))
                        htmlValor = valor.Invoke(dataList[x][dataList[x].Count - 1]);

                    tw.AddStyleAttribute("font-size", "8.0pt");
                    tw.AddStyleAttribute("border", "1px solid #ddd");
                    tw.AddStyleAttribute("text-align", "center");
                    tw.AddStyleAttribute("font-family", "Calibri, sans-serif");
                    tw.AddStyleAttribute("color", "white");
                    tw.AddStyleAttribute("font-weight", "700");
                    tw.AddStyleAttribute("background", "#003366");
                    tw.AddStyleAttribute("padding", "2px 6px 2px 6px");
                    tw.RenderBeginTag(HtmlTextWriterTag.Td);
                    tw.Write(HttpUtility.HtmlEncode(htmlValor));
                    tw.RenderEndTag();
                }
                tw.RenderEndTag();

                tw.RenderEndTag();
                tw.RenderEndTag();
                #endregion tabela

                #region empty line
                tw.RenderBeginTag(HtmlTextWriterTag.Br);
                #endregion empty line
            }

            return sw.ToString();
        }

        public string GetHtmlWithSumary<T>(Dictionary<string, Func<T, string>> fields, List<string> sumaryFields, IList<T> list, string corpo)
        {
            StringWriter sw = new StringWriter();
            HtmlTextWriter tw = new HtmlTextWriter(sw);
            tw.AddStyleAttribute("font-size", "10.0pt");
            tw.AddStyleAttribute("font-family", "Calibri, sans-serif");
            tw.RenderBeginTag(HtmlTextWriterTag.Div);

            tw.Write(HttpUtility.HtmlEncode(corpo));
            tw.RenderBeginTag(HtmlTextWriterTag.Br);

            tw.RenderEndTag();

            tw.RenderBeginTag(HtmlTextWriterTag.Div);

            tw.AddStyleAttribute("border-spacing", "0px");
            tw.RenderBeginTag(HtmlTextWriterTag.Table);
            // Cria a linha do titulo
            tw.RenderBeginTag(HtmlTextWriterTag.Tr);
            foreach (string key in fields.Keys)
            {
                tw.AddStyleAttribute("font-size", "8.0pt");
                tw.AddStyleAttribute("border", "1px solid #ddd");
                tw.AddStyleAttribute("text-align", "center");
                tw.AddStyleAttribute("font-family", "Calibri, sans-serif");
                tw.AddStyleAttribute("color", "white");
                tw.AddStyleAttribute("font-weight", "700");
                tw.AddStyleAttribute("background", "#003366");
                tw.AddStyleAttribute("padding", "2px 6px 2px 6px");
                tw.RenderBeginTag(HtmlTextWriterTag.Td);
                tw.Write(HttpUtility.HtmlEncode(key.ToUpper()));
                tw.RenderEndTag();
            }

            tw.RenderEndTag();
            for (int i = 0; i < list.Count - 1; i++)
            {
                tw.RenderBeginTag(HtmlTextWriterTag.Tr);
                foreach (var key in fields.Keys)
                {
                    Func<T, string> valor = fields[key];
                    var htmlValor = valor.Invoke(list[i]).ToUpper();

                    //color: red; font - weight: bold;
                    int valorConvertido = 0;

                    if (key.ToUpper().Equals("MONITORAR") && Int32.TryParse(htmlValor, out valorConvertido) && valorConvertido < 0)
                    {
                        tw.AddStyleAttribute("font-size", "8.0pt");
                        tw.AddStyleAttribute("border", "1px solid #ddd");
                        tw.AddStyleAttribute("color", "red");
                        tw.AddStyleAttribute("font-weight", "bold");
                        tw.AddStyleAttribute("text-align", "center");
                        tw.AddStyleAttribute("font-family", "Calibri, sans-serif");
                        tw.AddStyleAttribute("padding", "2px 6px 2px 6px");
                    }
                    else
                    {
                        tw.AddStyleAttribute("font-size", "8.0pt");
                        tw.AddStyleAttribute("border", "1px solid #ddd");
                        tw.AddStyleAttribute("text-align", "center");
                        tw.AddStyleAttribute("font-family", "Calibri, sans-serif");
                        tw.AddStyleAttribute("padding", "2px 6px 2px 6px");
                    }

                    tw.RenderBeginTag(HtmlTextWriterTag.Td);
                    tw.Write(HttpUtility.HtmlEncode(htmlValor));
                    tw.RenderEndTag();
                }

                tw.RenderEndTag();
            }
            // Cria a linha do sumario
            tw.RenderBeginTag(HtmlTextWriterTag.Tr);
            foreach (string key in fields.Keys)
            {
                Func<T, string> valor = fields[key];
                var htmlValor = String.Empty;

                if (sumaryFields.Contains(key))
                    htmlValor = valor.Invoke(list[list.Count - 1]).ToUpper();

                tw.AddStyleAttribute("border", "1px solid #ddd");
                tw.AddStyleAttribute("text-align", "center");
                tw.AddStyleAttribute("font-size", "8.0pt");
                tw.AddStyleAttribute("font-family", "Calibri, sans-serif");
                tw.AddStyleAttribute("color", "white");
                tw.AddStyleAttribute("font-weight", "700");
                tw.AddStyleAttribute("background", "#003366");
                tw.AddStyleAttribute("padding", "2px 6px 2px 6px");

                tw.RenderBeginTag(HtmlTextWriterTag.Td);
                tw.Write(HttpUtility.HtmlEncode(htmlValor));
                tw.RenderEndTag();
            }

            tw.RenderEndTag();

            tw.RenderEndTag();
            tw.RenderEndTag();

            return sw.ToString();
        }

        public void EnviaEmailDetalhe(string operacao, string regiao, string dataPedra, string uf, string origem, string estacaoFaturamento, string segmento, string ocultarPedraZerada)
        {
            #region configuração email
            ConfiguracaoTranslogic emailServer = _configuracaoTranslogicRepository.ObterPorId("GERENCIAMENTO_DOCUMENTACAO_EMAIL_SERVER");
            ConfiguracaoTranslogic emailRemetente = _configuracaoTranslogicRepository.ObterPorId("EDI_EMAIL_REMETENTE");
            ConfiguracaoTranslogic emailPorta = _configuracaoTranslogicRepository.ObterPorId("GERENCIAMENTO_DOCUMENTACAO_EMAIL_PORTA");
            ConfiguracaoTranslogic senhaSmtp = _configuracaoTranslogicRepository.ObterPorId("GERENCIAMENTO_DOCUMENTACAO_EMAIL_SMTP_SENHA");
            #endregion

            #region dictionary definition
            var dic = new Dictionary<string, Func<VwPedraRealizado, string>>();

            dic["DATA"] = c => c.DataPedra.GetValueOrDefault(DateTime.Now).ToString("dd/MM/yyyy");
            dic["OPERAÇÃO"] = c => c.Operacao != null ? c.Operacao : String.Empty;
            dic["REGIÃO"] = c => c.Regiao != null ? c.Regiao : String.Empty;
            dic["ESTAÇÃO"] = c => c.EstacaoFaturamento != null ? c.EstacaoFaturamento : String.Empty;
            dic["ORIGEM"] = c => c.EstacaoOrigem != null ? c.EstacaoOrigem : String.Empty;
            dic["TERMINAL"] = c => c.Terminal != null ? c.Terminal : String.Empty;
            dic["SEGMENTO"] = c => c.Segmento != null ? c.Segmento : String.Empty;
            dic["PROGRAMADO"] = c => c.Pedra.ToString();
            dic["REALIZADO"] = c => c.Realizado.ToString();
            dic["MONITORAR"] = c => c.Saldo.ToString();
            dic["%"] = c => String.Format("{0}%", c.Porcentagem);
            dic["STATUS"] = c => c.StatusEditado != null ? c.StatusEditado : String.Empty;
            #endregion

            var result = ObterPainelExpedicaoPedra(operacao, regiao, dataPedra, uf, origem, estacaoFaturamento, segmento, ocultarPedraZerada);

            var listEmails = ObterRegraEmails();

            foreach (var item in listEmails)
            {
                List<VwPedraRealizado> resultTemp = result.Where(p => p.OperacaoId == item.OperacaoId && (item.RegiaoId == 0 || p.RegiaoId == item.RegiaoId)).ToList();

                // se tiver registros manda email, caso contrario ignora
                if (resultTemp.Count > 0)
                {
                    var sumary = new VwPedraRealizado()
                    {
                        Segmento = "TOTAL",
                        PedraSade = result.Where(p => p.OperacaoId == item.OperacaoId && (item.RegiaoId == 0 || p.RegiaoId == item.RegiaoId)).Sum(s => s.Pedra),
                        Realizado = result.Where(p => p.OperacaoId == item.OperacaoId && (item.RegiaoId == 0 || p.RegiaoId == item.RegiaoId)).Sum(s => s.Realizado)
                    };

                    resultTemp.Add(sumary);

                    List<string> sumaryFields = new List<string>();

                    sumaryFields.Add("SEGMENTO");
                    sumaryFields.Add("PROGRAMADO");
                    sumaryFields.Add("REALIZADO");
                    sumaryFields.Add("MONITORAR");
                    sumaryFields.Add("%");

                    var htmlGrid = GetHtmlWithSumary<VwPedraRealizado>(dic, sumaryFields, resultTemp, item.CorpoEmail);

                    string tituloEmail = $"{item.TituloEmail} - Horário: {DateTime.Now.ToString("HH:mm")}";

                    SendEmail(emailServer.Valor, emailPorta.Valor, senhaSmtp.Valor, emailRemetente.Valor, item.Destinatarios, htmlGrid, tituloEmail);
                }
            }
        }

        private void SendEmail(string servidor, string porta, string senhaSmtp, string remetente, string destinatarios, string corpoEmail, string assuntoEmail)
        {
            var client = new SmtpClient(servidor, Convert.ToInt32(porta));

            if (!String.IsNullOrEmpty(senhaSmtp))
            {
                client.EnableSsl = true;
                client.Credentials = new NetworkCredential(remetente, senhaSmtp);
            }

            var mailMessage = new MailMessage();
            mailMessage.BodyEncoding = Encoding.UTF8;
            mailMessage.SubjectEncoding = Encoding.UTF8;
            mailMessage.Subject = assuntoEmail;
            mailMessage.From = new MailAddress(remetente);
            mailMessage.To.Add(destinatarios.Replace(';', ','));

            mailMessage.Body = corpoEmail;

            AlternateView html = AlternateView.CreateAlternateViewFromString(mailMessage.Body, null, MediaTypeNames.Text.Html);
            mailMessage.AlternateViews.Add(html);
            mailMessage.IsBodyHtml = true;

            client.Send(mailMessage);
        }

        public void EnviaEmailRegiao(string operacao, string regiao, string dataPedra, string uf, string origem, string estacaoFaturamento, string segmento, string ocultarPedraZerada)
        {
            #region configuração email
            ConfiguracaoTranslogic emailServer = _configuracaoTranslogicRepository.ObterPorId("GERENCIAMENTO_DOCUMENTACAO_EMAIL_SERVER");
            ConfiguracaoTranslogic emailRemetente = _configuracaoTranslogicRepository.ObterPorId("EDI_EMAIL_REMETENTE");
            ConfiguracaoTranslogic emailPorta = _configuracaoTranslogicRepository.ObterPorId("GERENCIAMENTO_DOCUMENTACAO_EMAIL_PORTA");
            ConfiguracaoTranslogic senhaSmtp = _configuracaoTranslogicRepository.ObterPorId("GERENCIAMENTO_DOCUMENTACAO_EMAIL_SMTP_SENHA");
            #endregion

            #region dicionary
            var dic = new Dictionary<string, Func<VwPedraRealizado, string>>();

            dic["DATA"] = c => c.DataPedra.GetValueOrDefault(DateTime.Now).ToString("dd/MM/yyyy");
            dic["REGIÃO"] = c => c.Regiao != null ? c.Regiao : String.Empty;
            dic["PROGRAMADO"] = c => c.Pedra.ToString();
            dic["REALIZADO"] = c => c.Realizado.ToString();
            dic["MONITORAR"] = c => c.Saldo.ToString();
            dic["%"] = c => String.Format("{0}%", c.Porcentagem);
            #endregion

            var listaAnalitico = ObterPainelExpedicaoPedra(operacao, regiao, dataPedra, uf, origem, estacaoFaturamento, segmento, ocultarPedraZerada);

            var regioesGroup = listaAnalitico.GroupBy(g => g.RegiaoId);

            List<VwPedraRealizado> analiticoRegioes = new List<VwPedraRealizado>();

            analiticoRegioes.AddRange(regioesGroup.Select(d => new VwPedraRealizado
            {
                DataPedra = d.First().DataPedra,
                Regiao = d.First().Regiao,
                RegiaoId = d.First().RegiaoId,
                PedraSade = d.Sum(p => p.Pedra),
                Realizado = d.Sum(p => p.Realizado)
            }));

            var listEmails = ObterRegraEmails();

            foreach (var item in listEmails)
            {
                //List<VwPedraRealizado> resultTemp = analiticoRegioes.Where(p => p.RegiaoId == item.RegiaoId).ToList();
                List<VwPedraRealizado> resultTemp = analiticoRegioes.Where(p => p.OperacaoId == item.OperacaoId && (item.RegiaoId == 0 || p.RegiaoId == item.RegiaoId)).ToList();


                if (resultTemp.Count > 0)
                {
                    var sumary = new VwPedraRealizado()
                    {
                        Regiao = "TOTAL",
                        PedraSade = analiticoRegioes.Where(p => p.RegiaoId == item.RegiaoId).Sum(s => s.Pedra),
                        Realizado = analiticoRegioes.Where(p => p.RegiaoId == item.RegiaoId).Sum(s => s.Realizado)
                    };

                    resultTemp.Add(sumary);

                    List<string> sumaryFields = new List<string>();

                    sumaryFields.Add("REGIÃO");
                    sumaryFields.Add("PROGRAMADO");
                    sumaryFields.Add("REALIZADO");
                    sumaryFields.Add("MONITORAR");
                    sumaryFields.Add("%");

                    var htmlGrid = GetHtmlWithSumary<VwPedraRealizado>(dic, sumaryFields, resultTemp, item.CorpoEmail);

                    string tituloEmail = $"{item.TituloEmail} - Horário: {DateTime.Now.ToString("HH:mm")}";

                    // só enviará email quando tiver resultado para exibir
                    SendEmail(emailServer.Valor, emailPorta.Valor, senhaSmtp.Valor, emailRemetente.Valor, item.Destinatarios, htmlGrid, tituloEmail);
                }
            }
        }

        public void EnviaEmailOrigem(string operacao, string regiao, string dataPedra, string uf, string origem, string estacaoFaturamento, string segmento, string ocultarPedraZerada)
        {
            #region configuração email
            ConfiguracaoTranslogic emailServer = _configuracaoTranslogicRepository.ObterPorId("GERENCIAMENTO_DOCUMENTACAO_EMAIL_SERVER");
            ConfiguracaoTranslogic emailRemetente = _configuracaoTranslogicRepository.ObterPorId("EDI_EMAIL_REMETENTE");
            ConfiguracaoTranslogic emailPorta = _configuracaoTranslogicRepository.ObterPorId("GERENCIAMENTO_DOCUMENTACAO_EMAIL_PORTA");
            ConfiguracaoTranslogic senhaSmtp = _configuracaoTranslogicRepository.ObterPorId("GERENCIAMENTO_DOCUMENTACAO_EMAIL_SMTP_SENHA");
            #endregion
            #region dicionary
            var dic = new Dictionary<string, Func<VwPedraRealizado, string>>();

            dic["DATA"] = c => c.DataPedra.GetValueOrDefault(DateTime.Now).ToString("dd/MM/yyyy");
            dic["ORIGEM"] = c => c.EstacaoOrigem != null ? c.EstacaoOrigem : String.Empty;
            dic["PROGRAMADO"] = c => c.Pedra.ToString();
            dic["REALIZADO"] = c => c.Realizado.ToString();
            dic["MONITORAR"] = c => c.Saldo.ToString();
            dic["%"] = c => String.Format("{0}%", c.Porcentagem);
            #endregion

            var listaAnalitico = ObterPainelExpedicaoPedra(operacao, regiao, dataPedra, uf, origem, estacaoFaturamento, segmento, ocultarPedraZerada);

            var origemGroup = listaAnalitico.GroupBy(g => g.EstacaoOrigemId);

            List<VwPedraRealizado> analiticoOrigens = new List<VwPedraRealizado>();

            analiticoOrigens.AddRange(origemGroup.Select(d => new VwPedraRealizado
            {
                DataPedra = d.First().DataPedra,
                EstacaoOrigem = d.First().EstacaoOrigem,
                RegiaoId = d.First().RegiaoId,
                PedraSade = d.Sum(p => p.Pedra),
                Realizado = d.Sum(p => p.Realizado)
            }));

            var listEmails = ObterRegraEmails();

            foreach (var item in listEmails)
            {
                //List<VwPedraRealizado> resultTemp = analiticoOrigens.Where(p => p.RegiaoId == item.RegiaoId).ToList();
                List<VwPedraRealizado> resultTemp = analiticoOrigens.Where(p => p.OperacaoId == item.OperacaoId && (item.RegiaoId == 0 || p.RegiaoId == item.RegiaoId)).ToList();


                if (resultTemp.Count > 0)
                {

                    var sumary = new VwPedraRealizado()
                    {
                        EstacaoOrigem = "TOTAL",
                        PedraSade = analiticoOrigens.Where(p => p.RegiaoId == item.RegiaoId).Sum(s => s.Pedra),
                        Realizado = analiticoOrigens.Where(p => p.RegiaoId == item.RegiaoId).Sum(s => s.Realizado)
                    };

                    resultTemp.Add(sumary);

                    List<string> sumaryFields = new List<string>();

                    sumaryFields.Add("ESTAÇÃO");
                    sumaryFields.Add("PROGRAMADO");
                    sumaryFields.Add("REALIZADO");
                    sumaryFields.Add("MONITORAR");
                    sumaryFields.Add("%");

                    var htmlGrid = GetHtmlWithSumary<VwPedraRealizado>(dic, sumaryFields, resultTemp, item.CorpoEmail);

                    string tituloEmail = $"{item.TituloEmail} - Horário: {DateTime.Now.ToString("HH:mm")}";

                    SendEmail(emailServer.Valor, emailPorta.Valor, senhaSmtp.Valor, emailRemetente.Valor, item.Destinatarios, htmlGrid, tituloEmail);
                }
            }
        }

        public void EnviaEmailGeral(string operacao, string regiao, string dataPedra, string uf, string origem, string estacaoFaturamento, string segmento, string ocultarPedraZerada)
        {

            #region configuração email
            ConfiguracaoTranslogic emailServer = _configuracaoTranslogicRepository.ObterPorId("GERENCIAMENTO_DOCUMENTACAO_EMAIL_SERVER");
            ConfiguracaoTranslogic emailRemetente = _configuracaoTranslogicRepository.ObterPorId("EDI_EMAIL_REMETENTE");
            ConfiguracaoTranslogic emailPorta = _configuracaoTranslogicRepository.ObterPorId("GERENCIAMENTO_DOCUMENTACAO_EMAIL_PORTA");
            ConfiguracaoTranslogic senhaSmtp = _configuracaoTranslogicRepository.ObterPorId("GERENCIAMENTO_DOCUMENTACAO_EMAIL_SMTP_SENHA");
            #endregion
            #region dicionary
            var dicDetalhe = new Dictionary<string, Func<VwPedraRealizado, string>>();

            dicDetalhe["DATA"] = c => c.DataPedra.Value.ToString("dd/MM/yyyy");
            dicDetalhe["OPERAÇÃO"] = c => c.Operacao != null ? c.Operacao : String.Empty;
            dicDetalhe["REGIÃO"] = c => c.Regiao != null ? c.Regiao : String.Empty;
            dicDetalhe["ESTAÇÃO"] = c => c.EstacaoFaturamento != null ? c.EstacaoFaturamento : String.Empty;
            dicDetalhe["ORIGEM"] = c => c.EstacaoOrigem != null ? c.EstacaoOrigem : String.Empty;
            dicDetalhe["TERMINAL"] = c => c.Terminal != null ? c.Terminal : String.Empty;
            dicDetalhe["SEGMENTO"] = c => c.Segmento != null ? c.Segmento : String.Empty;
            dicDetalhe["PROGRAMADO"] = c => c.Pedra.ToString();
            dicDetalhe["REALIZADO"] = c => c.Realizado.ToString();
            dicDetalhe["MONITORAR"] = c => c.Saldo.ToString();
            dicDetalhe["%"] = c => String.Format("{0}%", c.Porcentagem);
            dicDetalhe["STATUS"] = c => c.StatusEditado != null ? c.StatusEditado : String.Empty;

            var dicRegioes = new Dictionary<string, Func<VwPedraRealizado, string>>();

            dicRegioes["DATA"] = c => c.DataPedra.Value.ToString("dd/MM/yyyy");
            dicRegioes["REGIÃO"] = c => c.Regiao != null ? c.Regiao : String.Empty;
            dicRegioes["PROGRAMADO"] = c => c.Pedra.ToString();
            dicRegioes["REALIZADO"] = c => c.Realizado.ToString();
            dicRegioes["MONITORAR"] = c => c.Saldo.ToString();
            dicRegioes["%"] = c => String.Format("{0}%", c.Porcentagem);

            var dicSegmentos = new Dictionary<string, Func<VwPedraRealizado, string>>();

            dicSegmentos["DATA"] = c => c.DataPedra.Value.ToString("dd/MM/yyyy");
            dicSegmentos["SEGMENTO"] = c => c.Segmento != null ? c.Segmento : String.Empty;
            dicSegmentos["PROGRAMADO"] = c => c.Pedra.ToString();
            dicSegmentos["REALIZADO"] = c => c.Realizado.ToString();
            dicSegmentos["MONITORAR"] = c => c.Saldo.ToString();
            dicSegmentos["%"] = c => String.Format("{0}%", c.Porcentagem);

            var dicOrigens = new Dictionary<string, Func<VwPedraRealizado, string>>();

            dicOrigens["DATA"] = c => c.DataPedra.Value.ToString("dd/MM/yyyy");
            dicOrigens["ESTAÇÃO"] = c => c.EstacaoOrigem != null ? c.EstacaoOrigem : String.Empty;
            dicOrigens["PROGRAMADO"] = c => c.Pedra.ToString();
            dicOrigens["REALIZADO"] = c => c.Realizado.ToString();
            dicOrigens["MONITORAR"] = c => c.Saldo.ToString();
            dicOrigens["%"] = c => String.Format("{0}%", c.Porcentagem);

            var dicEstacao = new Dictionary<string, Func<VwPedraRealizado, string>>();

            dicEstacao["DATA"] = c => c.DataPedra.Value.ToString("dd/MM/yyyy");
            dicEstacao["ESTAÇÃO"] = c => c.EstacaoFaturamento != null ? c.EstacaoFaturamento : String.Empty;
            dicEstacao["PROGRAMADO"] = c => c.Pedra.ToString();
            dicEstacao["REALIZADO"] = c => c.Realizado.ToString();
            dicEstacao["MONITORAR"] = c => c.Saldo.ToString();
            dicEstacao["%"] = c => String.Format("{0}%", c.Porcentagem);

            #endregion
            #region data
            var listaGeral = ObterPainelExpedicaoPedra(operacao, regiao, dataPedra, uf, origem, estacaoFaturamento, segmento, ocultarPedraZerada);

            var listEmails = ObterRegraEmails();

            decimal operacaoId = 0;
            Decimal.TryParse(operacao, out operacaoId);

            foreach (var item in listEmails.Where(l => l.RegiaoId == 0 && (operacaoId == 0 || l.OperacaoId == operacaoId)))
            {
                List<Dictionary<string, Func<VwPedraRealizado, string>>> dicList = new List<Dictionary<string, Func<VwPedraRealizado, string>>>();

                List<List<VwPedraRealizado>> dataList = new List<List<VwPedraRealizado>>();

                var listaPorOperacao = listaGeral.Where(o => item.OperacaoId == 0 || o.OperacaoId == item.OperacaoId).ToList();

                var segmentosGroup = listaPorOperacao.GroupBy(g => g.Segmento);

                List<VwPedraRealizado> analiticoSegmentos = new List<VwPedraRealizado>();

                analiticoSegmentos.AddRange(segmentosGroup.Select(d => new VwPedraRealizado
                {
                    DataPedra = d.First().DataPedra,
                    Segmento = d.First().Segmento,
                    RegiaoId = d.First().RegiaoId,
                    PedraSade = d.Sum(p => p.Pedra),
                    Realizado = d.Sum(p => p.Realizado)
                }));


                var regioesGroup = listaPorOperacao.GroupBy(g => g.RegiaoId);

                List<VwPedraRealizado> analiticoRegioes = new List<VwPedraRealizado>();

                analiticoRegioes.AddRange(regioesGroup.Select(d => new VwPedraRealizado
                {
                    DataPedra = d.First().DataPedra,
                    Regiao = d.First().Regiao,
                    RegiaoId = d.First().RegiaoId,
                    PedraSade = d.Sum(p => p.Pedra),
                    Realizado = d.Sum(p => p.Realizado)
                }));

                var origemGroup = listaPorOperacao.GroupBy(g => g.EstacaoOrigemId);

                List<VwPedraRealizado> analiticoOrigens = new List<VwPedraRealizado>();

                analiticoOrigens.AddRange(origemGroup.Select(d => new VwPedraRealizado
                {
                    DataPedra = d.First().DataPedra,
                    EstacaoOrigem = d.First().EstacaoOrigem,
                    RegiaoId = d.First().RegiaoId,
                    PedraSade = d.Sum(p => p.Pedra),
                    Realizado = d.Sum(p => p.Realizado)
                }));

                var estacaoGroup = listaPorOperacao.GroupBy(g => g.EstacaoFaturamento);

                List<VwPedraRealizado> analiticoEstacoes = new List<VwPedraRealizado>();

                analiticoEstacoes.AddRange(estacaoGroup.Select(d => new VwPedraRealizado
                {
                    DataPedra = d.First().DataPedra,
                    EstacaoFaturamento = d.First().EstacaoFaturamento,
                    RegiaoId = d.First().RegiaoId,
                    PedraSade = d.Sum(p => p.Pedra),
                    Realizado = d.Sum(p => p.Realizado)
                }));


                var analiticoSumary = new VwPedraRealizado()
                {
                    DataPedra = listaPorOperacao.First().DataPedra,
                    PedraSade = listaPorOperacao.Sum(p => p.Pedra),
                    Realizado = listaPorOperacao.Sum(p => p.Realizado)
                };

                listaPorOperacao.Add(analiticoSumary);

                var regiaoSumary = new VwPedraRealizado()
                {
                    DataPedra = analiticoRegioes.First().DataPedra,
                    Regiao = analiticoRegioes.First().Regiao,
                    RegiaoId = analiticoRegioes.First().RegiaoId,
                    PedraSade = analiticoRegioes.Sum(p => p.Pedra),
                    Realizado = analiticoRegioes.Sum(p => p.Realizado)
                };

                analiticoRegioes.Add(regiaoSumary);

                var origemSumary = new VwPedraRealizado
                {
                    DataPedra = analiticoOrigens.First().DataPedra,
                    EstacaoOrigem = analiticoOrigens.First().EstacaoOrigem,
                    RegiaoId = analiticoOrigens.First().RegiaoId,
                    PedraSade = analiticoOrigens.Sum(p => p.Pedra),
                    Realizado = analiticoOrigens.Sum(p => p.Realizado)
                };

                analiticoOrigens.Add(origemSumary);

                var estacaoSumary = new VwPedraRealizado
                {
                    DataPedra = analiticoEstacoes.First().DataPedra,
                    EstacaoFaturamento = analiticoEstacoes.First().EstacaoFaturamento,
                    RegiaoId = analiticoEstacoes.First().RegiaoId,
                    PedraSade = analiticoEstacoes.Sum(p => p.Pedra),
                    Realizado = analiticoEstacoes.Sum(p => p.Realizado)
                };

                analiticoEstacoes.Add(estacaoSumary);

                var segmentoSumary = new VwPedraRealizado()
                {
                    DataPedra = analiticoSegmentos.First().DataPedra,
                    Segmento = analiticoSegmentos.First().Segmento,
                    RegiaoId = analiticoSegmentos.First().RegiaoId,
                    PedraSade = analiticoSegmentos.Sum(p => p.Pedra),
                    Realizado = analiticoSegmentos.Sum(p => p.Realizado)
                };

                analiticoSegmentos.Add(segmentoSumary);

                #endregion

                List<string> sumaryFields = new List<string>();

                sumaryFields.Add("PROGRAMADO");
                sumaryFields.Add("REALIZADO");
                sumaryFields.Add("MONITORAR");
                sumaryFields.Add("%");

                dicList.Add(dicRegioes);
                dicList.Add(dicEstacao);
                dicList.Add(dicSegmentos);
                dicList.Add(dicDetalhe);
                //dicList.Add(dicOrigens);

                dataList.Add(analiticoRegioes);
                dataList.Add(analiticoEstacoes);
                dataList.Add(analiticoSegmentos);
                dataList.Add(listaPorOperacao.ToList());
                //dataList.Add(analiticoOrigens);

                var htmlGrid = GetHtmlPedra(dicList, sumaryFields, dataList, item != null ? item.CorpoEmail : "");

                string tituloEmail = $"{item.TituloEmail} - Horário: {DateTime.Now.ToString("HH:mm")}";

                SendEmail(emailServer.Valor, emailPorta.Valor, senhaSmtp.Valor, emailRemetente.Valor, item.Destinatarios, htmlGrid, tituloEmail);
            }

        }


        /// <summary>
        /// Obtem as informacoes do quadro de visualizacao do Painel de Expedicao
        /// </summary>
        /// <returns></returns>
        public IList<PainelExpedicaoVisualizacaoDto> ObterPainelExpedicaoPedraResumoRegiao(DetalhesPaginacaoWeb pagination, string regiaoId, DateTime dataInicio, string uf, string origem, string origemFaturamento, string segmento, string malhaCentral, string pedraId)
        {

            uf = (String.IsNullOrEmpty(uf) ? "" : uf);
            uf = uf.Trim().ToUpper();
            uf = (uf == "TODAS" ? "" : uf);

            origem = (String.IsNullOrEmpty(origem) ? "" : origem);
            origem = origem.Trim().ToUpper();

            origemFaturamento = (String.IsNullOrEmpty(origemFaturamento) ? String.Empty : origemFaturamento);
            origemFaturamento = origemFaturamento.Trim().ToUpper();

            segmento = (String.IsNullOrEmpty(segmento) ? "" : segmento);
            segmento = segmento.Trim().ToUpper();
            segmento = (segmento == "TODOS" ? "" : segmento);

            origemFaturamento = (String.IsNullOrEmpty(origemFaturamento) ? String.Empty : origemFaturamento);
            origemFaturamento = origemFaturamento.Trim().ToUpper();
            origemFaturamento = (origemFaturamento == "TODOS" ? String.Empty : origemFaturamento);

            malhaCentral = (String.IsNullOrEmpty(malhaCentral) ? "" : malhaCentral);
            malhaCentral = malhaCentral.Trim().ToUpper();
            malhaCentral = (malhaCentral == "TODAS" ? "" : malhaCentral);

            var result = _painelExpedicaoRepository.ObterPainelExpedicaoPedra(pagination, regiaoId, dataInicio, uf, origem, origemFaturamento, segmento, malhaCentral, pedraId);

            return result;
        }

        public int ObterPainelExpedicaoTempoRefresh()
        {
            int tempoMilisegundosRefreshTela = 60000; // milisegundos (1 minuto)

            var refreshTelaConfigGeral = _configGeralRepository.ObterPorId("PAINEL_EXPEDICAO_REFRESH_TELA");
            if (!(String.IsNullOrEmpty(refreshTelaConfigGeral.Valor)))
            {
                tempoMilisegundosRefreshTela = Convert.ToInt32(refreshTelaConfigGeral.Valor) * 60000;
                tempoMilisegundosRefreshTela = (tempoMilisegundosRefreshTela == 0 ? 60000 : tempoMilisegundosRefreshTela);
            }

            return tempoMilisegundosRefreshTela;
        }

        private void AtualizaColunasValor(ref IList<PainelExpedicaoVisualizacaoDto> result)
        {
            foreach (PainelExpedicaoVisualizacaoDto r in result)
            {
                if (r.PendenteConfirmacaoValorCor == null)
                    r.PendenteConfirmacaoValorCor = new PainelExpedicaoVisualizacaoColunaDto();

                if (r.TotalRecusadoEstacaoValorCor == null)
                    r.TotalRecusadoEstacaoValorCor = new PainelExpedicaoVisualizacaoColunaDto();

                if (r.FaturadoValorCor == null)
                    r.FaturadoValorCor = new PainelExpedicaoVisualizacaoColunaDto();

                if (r.FaturadoManualValorCor == null)
                    r.FaturadoManualValorCor = new PainelExpedicaoVisualizacaoColunaDto();

                if (r.CteAutorizadoValorCor == null)
                    r.CteAutorizadoValorCor = new PainelExpedicaoVisualizacaoColunaDto();

                if (r.PendenteFaturadoValorCor == null)
                    r.PendenteFaturadoValorCor = new PainelExpedicaoVisualizacaoColunaDto();

                if (r.CtePendenteValorCor == null)
                    r.CtePendenteValorCor = new PainelExpedicaoVisualizacaoColunaDto();

                r.PendenteConfirmacaoValorCor.Valor = r.PendenteConfirmacao;
                r.PendenteConfirmacaoValorCor.Indice = 3;

                r.TotalRecusadoEstacaoValorCor.Valor = r.TotalRecusadoEstacao;
                r.TotalRecusadoEstacaoValorCor.Indice = 4;

                r.PendenteFaturadoValorCor.Valor = r.PendenteFaturado;
                r.PendenteFaturadoValorCor.Indice = 5;

                r.FaturadoValorCor.Valor = r.Faturado;
                r.FaturadoValorCor.Indice = 6;

                r.FaturadoManualValorCor.Valor = r.FaturadoManual;
                r.FaturadoManualValorCor.Indice = 7;

                r.CtePendenteValorCor.Valor = r.CtePendente;
                r.CtePendenteValorCor.Indice = 8;

                r.CteAutorizadoValorCor.Valor = r.CteAutorizado;
                r.CteAutorizadoValorCor.Indice = 9;
            }
        }

        private void AtualizaColunasVisibilidade(int situacaoVisualizacaoId,
                                                 ref IList<PainelExpedicaoVisualizacaoDto> result)
        {

            if (situacaoVisualizacaoId != 0) // diferente de Todas
            {

                if (situacaoVisualizacaoId == 1) // Pendente Confirmação
                {
                    var temp = from r in result
                               where r.PendenteConfirmacao > 0
                               select new PainelExpedicaoVisualizacaoDto()
                               {
                                   Origem = r.Origem,
                                   OrigemUf = r.OrigemUf,
                                   OrigemDescricao = r.OrigemDescricao,
                                   PendenteConfirmacao = r.PendenteConfirmacao,
                                   PendenteConfirmacaoValorCor = r.PendenteConfirmacaoValorCor,
                                   TotalRecusadoEstacao = r.TotalRecusadoEstacao,
                                   TotalRecusadoEstacaoValorCor = r.TotalRecusadoEstacaoValorCor,
                                   PendenteFaturado = r.PendenteFaturado,
                                   PendenteFaturadoValorCor = r.PendenteFaturadoValorCor,
                                   Faturado = r.Faturado,
                                   FaturadoValorCor = r.FaturadoValorCor,
                                   FaturadoManual = r.FaturadoManual,
                                   FaturadoManualValorCor = r.FaturadoManualValorCor,
                                   CtePendente = r.CtePendente,
                                   CtePendenteValorCor = r.CtePendenteValorCor,
                                   CteAutorizado = r.CteAutorizado,
                                   CteAutorizadoValorCor = r.CteAutorizadoValorCor,
                                   TempoRefreshMilisegundos = r.TempoRefreshMilisegundos
                               };

                    result = temp.ToList();

                }
                else if (situacaoVisualizacaoId == 2) // Recusado
                {

                    var temp = from r in result
                               where r.TotalRecusadoEstacao > 0
                               select new PainelExpedicaoVisualizacaoDto()
                               {
                                   Origem = r.Origem,
                                   OrigemUf = r.OrigemUf,
                                   OrigemDescricao = r.OrigemDescricao,
                                   PendenteConfirmacao = r.PendenteConfirmacao,
                                   PendenteConfirmacaoValorCor = r.PendenteConfirmacaoValorCor,
                                   TotalRecusadoEstacao = r.TotalRecusadoEstacao,
                                   TotalRecusadoEstacaoValorCor = r.TotalRecusadoEstacaoValorCor,
                                   PendenteFaturado = r.PendenteFaturado,
                                   PendenteFaturadoValorCor = r.PendenteFaturadoValorCor,
                                   Faturado = r.Faturado,
                                   FaturadoValorCor = r.FaturadoValorCor,
                                   FaturadoManual = r.FaturadoManual,
                                   FaturadoManualValorCor = r.FaturadoManualValorCor,
                                   CtePendente = r.CtePendente,
                                   CtePendenteValorCor = r.CtePendenteValorCor,
                                   CteAutorizado = r.CteAutorizado,
                                   CteAutorizadoValorCor = r.CteAutorizadoValorCor,
                                   TempoRefreshMilisegundos = r.TempoRefreshMilisegundos
                               };

                    result = temp.ToList();
                }
                else if (situacaoVisualizacaoId == 3) // Pendente Faturado
                {
                    var temp = from r in result
                               where r.PendenteFaturado > 0
                               select new PainelExpedicaoVisualizacaoDto()
                               {
                                   Origem = r.Origem,
                                   OrigemUf = r.OrigemUf,
                                   OrigemDescricao = r.OrigemDescricao,
                                   PendenteConfirmacao = r.PendenteConfirmacao,
                                   PendenteConfirmacaoValorCor = r.PendenteConfirmacaoValorCor,
                                   TotalRecusadoEstacao = r.TotalRecusadoEstacao,
                                   TotalRecusadoEstacaoValorCor = r.TotalRecusadoEstacaoValorCor,
                                   PendenteFaturado = r.PendenteFaturado,
                                   PendenteFaturadoValorCor = r.PendenteFaturadoValorCor,
                                   Faturado = r.Faturado,
                                   FaturadoValorCor = r.FaturadoValorCor,
                                   FaturadoManual = r.FaturadoManual,
                                   FaturadoManualValorCor = r.FaturadoManualValorCor,
                                   CtePendente = r.CtePendente,
                                   CtePendenteValorCor = r.CtePendenteValorCor,
                                   CteAutorizado = r.CteAutorizado,
                                   CteAutorizadoValorCor = r.CteAutorizadoValorCor,
                                   TempoRefreshMilisegundos = r.TempoRefreshMilisegundos
                               };

                    result = temp.ToList();
                }
                else if (situacaoVisualizacaoId == 4) // Faturado
                {
                    var temp = from r in result
                               where r.Faturado > 0
                               select new PainelExpedicaoVisualizacaoDto()
                               {
                                   Origem = r.Origem,
                                   OrigemUf = r.OrigemUf,
                                   OrigemDescricao = r.OrigemDescricao,
                                   PendenteConfirmacao = r.PendenteConfirmacao,
                                   PendenteConfirmacaoValorCor = r.PendenteConfirmacaoValorCor,
                                   TotalRecusadoEstacao = r.TotalRecusadoEstacao,
                                   TotalRecusadoEstacaoValorCor = r.TotalRecusadoEstacaoValorCor,
                                   PendenteFaturado = r.PendenteFaturado,
                                   PendenteFaturadoValorCor = r.PendenteFaturadoValorCor,
                                   Faturado = r.Faturado,
                                   FaturadoValorCor = r.FaturadoValorCor,
                                   FaturadoManual = r.FaturadoManual,
                                   FaturadoManualValorCor = r.FaturadoManualValorCor,
                                   CtePendente = r.CtePendente,
                                   CtePendenteValorCor = r.CtePendenteValorCor,
                                   CteAutorizado = r.CteAutorizado,
                                   CteAutorizadoValorCor = r.CteAutorizadoValorCor,
                                   TempoRefreshMilisegundos = r.TempoRefreshMilisegundos
                               };

                    result = temp.ToList();
                }
                else if (situacaoVisualizacaoId == 5) // Cte Pendente
                {
                    var temp = from r in result
                               where r.CtePendente > 0
                               select new PainelExpedicaoVisualizacaoDto()
                               {
                                   Origem = r.Origem,
                                   OrigemUf = r.OrigemUf,
                                   OrigemDescricao = r.OrigemDescricao,
                                   PendenteConfirmacao = r.PendenteConfirmacao,
                                   PendenteConfirmacaoValorCor = r.PendenteConfirmacaoValorCor,
                                   TotalRecusadoEstacao = r.TotalRecusadoEstacao,
                                   TotalRecusadoEstacaoValorCor = r.TotalRecusadoEstacaoValorCor,
                                   PendenteFaturado = r.PendenteFaturado,
                                   PendenteFaturadoValorCor = r.PendenteFaturadoValorCor,
                                   Faturado = r.Faturado,
                                   FaturadoValorCor = r.FaturadoValorCor,
                                   FaturadoManual = r.FaturadoManual,
                                   FaturadoManualValorCor = r.FaturadoManualValorCor,
                                   CtePendente = r.CtePendente,
                                   CtePendenteValorCor = r.CtePendenteValorCor,
                                   CteAutorizado = r.CteAutorizado,
                                   CteAutorizadoValorCor = r.CteAutorizadoValorCor,
                                   TempoRefreshMilisegundos = r.TempoRefreshMilisegundos
                               };

                    result = temp.ToList();
                }
                else if (situacaoVisualizacaoId == 6) // Cte Autorizado
                {
                    var temp = from r in result
                               where r.CteAutorizado > 0
                               select new PainelExpedicaoVisualizacaoDto()
                               {
                                   Origem = r.Origem,
                                   OrigemUf = r.OrigemUf,
                                   OrigemDescricao = r.OrigemDescricao,
                                   PendenteConfirmacao = r.PendenteConfirmacao,
                                   PendenteConfirmacaoValorCor = r.PendenteConfirmacaoValorCor,
                                   TotalRecusadoEstacao = r.TotalRecusadoEstacao,
                                   TotalRecusadoEstacaoValorCor = r.TotalRecusadoEstacaoValorCor,
                                   PendenteFaturado = r.PendenteFaturado,
                                   PendenteFaturadoValorCor = r.PendenteFaturadoValorCor,
                                   Faturado = r.Faturado,
                                   FaturadoValorCor = r.FaturadoValorCor,
                                   FaturadoManual = r.FaturadoManual,
                                   FaturadoManualValorCor = r.FaturadoManualValorCor,
                                   CtePendente = r.CtePendente,
                                   CtePendenteValorCor = r.CtePendenteValorCor,
                                   CteAutorizado = r.CteAutorizado,
                                   CteAutorizadoValorCor = r.CteAutorizadoValorCor,
                                   TempoRefreshMilisegundos = r.TempoRefreshMilisegundos
                               };

                    result = temp.ToList();
                }
                else if (situacaoVisualizacaoId == 7) // Faturado Manual
                {
                    var temp = from r in result
                               where r.FaturadoManual > 0
                               select new PainelExpedicaoVisualizacaoDto()
                               {
                                   Origem = r.Origem,
                                   OrigemUf = r.OrigemUf,
                                   OrigemDescricao = r.OrigemDescricao,
                                   PendenteConfirmacao = r.PendenteConfirmacao,
                                   PendenteConfirmacaoValorCor = r.PendenteConfirmacaoValorCor,
                                   TotalRecusadoEstacao = r.TotalRecusadoEstacao,
                                   TotalRecusadoEstacaoValorCor = r.TotalRecusadoEstacaoValorCor,
                                   PendenteFaturado = r.PendenteFaturado,
                                   PendenteFaturadoValorCor = r.PendenteFaturadoValorCor,
                                   Faturado = r.Faturado,
                                   FaturadoValorCor = r.FaturadoValorCor,
                                   FaturadoManual = r.FaturadoManual,
                                   FaturadoManualValorCor = r.FaturadoManualValorCor,
                                   CtePendente = r.CtePendente,
                                   CtePendenteValorCor = r.CtePendenteValorCor,
                                   CteAutorizado = r.CteAutorizado,
                                   CteAutorizadoValorCor = r.CteAutorizadoValorCor,
                                   TempoRefreshMilisegundos = r.TempoRefreshMilisegundos
                               };

                    result = temp.ToList();
                }
            }
        }


        private void AtualizaCorColunasIndicadores(ColunaEnum colunaAtualizacao, decimal valorIni, string operadorIni, bool bExisteValorFim, decimal? valorFim, string operadorFim, CorEnum cor, ref IList<PainelExpedicaoVisualizacaoDto> result)
        {

            foreach (PainelExpedicaoVisualizacaoDto r in result)
            {

                bool bAtendeCriterioInicial = false;
                bool bAtendeCriterioFinal = true; // Se não existe valor fim então inicializa para true (atende)
                decimal valorRegistro = 0;
                if (colunaAtualizacao == ColunaEnum.PendenteFaturamento)
                    valorRegistro = r.PendenteFaturado;
                else if (colunaAtualizacao == ColunaEnum.CTePendente)
                    valorRegistro = r.CtePendente;
                else if (colunaAtualizacao == ColunaEnum.PendenteConfirmacao)
                    valorRegistro = r.PendenteConfirmacao;
                else if (colunaAtualizacao == ColunaEnum.RecusadoEstacao)
                    valorRegistro = r.TotalRecusadoEstacao;

                bAtendeCriterioInicial = this.ComparaValor(operadorIni, valorRegistro, valorIni);

                if ((bExisteValorFim) && (!String.IsNullOrEmpty(operadorFim)))
                {
                    bAtendeCriterioFinal = false;
                    bAtendeCriterioFinal = this.ComparaValor(operadorFim, valorRegistro, valorFim);
                }

                if ((bAtendeCriterioInicial) && (bAtendeCriterioFinal))
                {
                    if (colunaAtualizacao == ColunaEnum.PendenteFaturamento)
                        r.PendenteFaturadoValorCor.Cor = cor;
                    else if (colunaAtualizacao == ColunaEnum.CTePendente)
                        r.CtePendenteValorCor.Cor = cor;
                    else if (colunaAtualizacao == ColunaEnum.PendenteConfirmacao)
                        r.PendenteConfirmacaoValorCor.Cor = cor;
                    else if (colunaAtualizacao == ColunaEnum.RecusadoEstacao)
                        r.TotalRecusadoEstacaoValorCor.Cor = cor;
                }
            }
        }

        private bool ComparaValor(string operador, decimal? valorColunaTabela, decimal? valorParametro)
        {
            bool resultComparacao = true;
            switch (operador)
            {
                case ">":
                    resultComparacao = (valorColunaTabela > valorParametro);
                    break;
                case ">=":
                    resultComparacao = (valorColunaTabela >= valorParametro);
                    break;
                case "=":
                    resultComparacao = (valorColunaTabela == valorParametro);
                    break;
                case "!=":
                    resultComparacao = (valorColunaTabela != valorParametro);
                    break;
                case "<":
                    resultComparacao = (valorColunaTabela < valorParametro);
                    break;
                case "<=":
                    resultComparacao = (valorColunaTabela <= valorParametro);
                    break;
            }

            return resultComparacao;
        }

        #endregion

        #region Consulta

        public ResultadoPaginado<PainelExpedicaoConsultaDto> ObterPainelExpedicaoConsulta(
            DetalhesPaginacaoWeb pagination,
            DateTime dataInicial,
            DateTime dataFinal,
            string fluxo,
            string origem,
            string destino,
            string vagoes,
            string situacao,
            string lotacao,
            string localAtual,
            string cliente,
            string terminalFaturamento,
            string mercadorias,
            string segmento)
        {
            vagoes = vagoes ?? string.Empty;
            mercadorias = mercadorias ?? string.Empty;
            segmento = segmento.Trim().ToUpper();
            segmento = (segmento == "TODOS" ? "" : segmento);

            string[] codigosDosVagoesSeparados =
                vagoes.ToUpper()
                    .Trim()
                    .Replace(" ", string.Empty)
                    .Replace(",", ";")
                    .Replace(".", ";")
                    .Split(';')
                    .Where(e => !string.IsNullOrWhiteSpace(e))
                    .ToArray();

            string[] codigosDasMercadoriasSeparados =
                mercadorias.ToUpper()
                    .Trim()
                    .Replace(" ", string.Empty)
                    .Replace(",", ";")
                    .Replace(".", ";")
                    .Split(';')
                    .Where(e => !string.IsNullOrWhiteSpace(e))
                    .ToArray();

            var result = this._painelExpedicaoRepository.ObterPainelExpedicaoConsulta(pagination,
                                                                                        dataInicial,
                                                                                        dataFinal,
                                                                                        fluxo,
                                                                                        origem,
                                                                                        destino,
                                                                                        codigosDosVagoesSeparados,
                                                                                        situacao,
                                                                                        lotacao,
                                                                                        localAtual,
                                                                                        cliente,
                                                                                        terminalFaturamento,
                                                                                        codigosDasMercadoriasSeparados,
                                                                                        segmento);
            return result;
        }

        #endregion

        #region Consulta de Documentos

        public ResultadoPaginado<PainelExpedicaoConsultarDocumentosDto> ObterPainelExpedicaoConsultaDocumentos(
                                                                            DetalhesPaginacaoWeb pagination,
                                                                            string dataInicio,
                                                                            string dataFim,
                                                                            string fluxo,
                                                                            string origem,
                                                                            string destino,
                                                                            string vagoes,
                                                                            string cliente,
                                                                            string expedidor,
                                                                            string remetente,
                                                                            string recebedor,
                                                                            string mercadorias,
                                                                            string segmento,
                                                                            string os,
                                                                            string prefixo,
                                                                            string statusCte,
                                                                            string statusNfe,
                                                                            string statusWebDanfe,
                                                                            string statusTicket,
                                                                            string statusDcl,
                                                                            string statusLaudoMercadoria,
                                                                            string outrasFerrovias,
                                                                            string itensDespacho,
                                                                            string cnpjRaizRecebedor,
                                                                            string malhaOrigem,
                                                                            bool exportarExcel)
        {
            #region Formatação dos Parâmetros

            var dataInicioFormatado = DateTime.Now;
            if (!string.IsNullOrEmpty(dataInicio))
                dataInicioFormatado = DateTime.ParseExact(dataInicio, "dd/MM/yyyy", CultureInfo.InvariantCulture);

            var dataFimFormatado = DateTime.Now;
            if (!string.IsNullOrEmpty(dataFim))
                dataFimFormatado = DateTime.ParseExact(dataFim, "dd/MM/yyyy", CultureInfo.InvariantCulture);

            vagoes = vagoes ?? string.Empty;
            mercadorias = mercadorias ?? string.Empty;
            segmento = segmento.Trim().ToUpper();
            segmento = (segmento == "TODOS" ? "" : segmento);
            itensDespacho = itensDespacho ?? string.Empty;

            string[] codigosDosVagoesSeparados =
                vagoes.ToUpper()
                    .Trim()
                    .Replace(" ", string.Empty)
                    .Replace(",", ";")
                    .Replace(".", ";")
                    .Split(';')
                    .Where(e => !string.IsNullOrWhiteSpace(e))
                    .Distinct()
                    .ToArray();

            string[] codigosDasMercadoriasSeparados =
                mercadorias.ToUpper()
                    .Trim()
                    .Replace(" ", string.Empty)
                    .Replace(",", ";")
                    .Replace(".", ";")
                    .Split(';')
                    .Where(e => !string.IsNullOrWhiteSpace(e))
                    .ToArray();

            string[] codigosDosItensDespachoSeparados =
                itensDespacho.ToUpper()
                    .Trim()
                    .Replace(" ", string.Empty)
                    .Replace(",", ";")
                    .Replace(".", ";")
                    .Split(';')
                    .Where(e => !string.IsNullOrWhiteSpace(e))
                    .ToArray();

            bool statusCteFormatado = false;
            if (!string.IsNullOrEmpty(statusCte))
            {
                if (!bool.TryParse(statusCte, out statusCteFormatado))
                    statusCteFormatado = false;
            }

            bool statusNfeFormatado = false;
            if (!string.IsNullOrEmpty(statusNfe))
            {
                if (!bool.TryParse(statusNfe, out statusNfeFormatado))
                    statusNfeFormatado = false;
            }

            bool statusWebDanfeFormatado = false;
            if (!string.IsNullOrEmpty(statusWebDanfe))
            {
                if (!bool.TryParse(statusWebDanfe, out statusWebDanfeFormatado))
                    statusWebDanfeFormatado = false;
            }

            bool statusTicketFormatado = false;
            if (!string.IsNullOrEmpty(statusTicket))
            {
                if (!bool.TryParse(statusTicket, out statusTicketFormatado))
                    statusTicketFormatado = false;
            }

            bool statusDclFormatado = false;
            if (!string.IsNullOrEmpty(statusDcl))
            {
                if (!bool.TryParse(statusDcl, out statusDclFormatado))
                    statusDclFormatado = false;
            }

            bool statusLaudoMercadoriaFormatado = false;
            if (!string.IsNullOrEmpty(statusLaudoMercadoria))
            {
                if (!bool.TryParse(statusLaudoMercadoria, out statusLaudoMercadoriaFormatado))
                    statusLaudoMercadoriaFormatado = false;
            }

            bool outrasFerroviasFormatado = false;
            if (!string.IsNullOrEmpty(outrasFerrovias))
            {
                if (!bool.TryParse(outrasFerrovias, out outrasFerroviasFormatado))
                    outrasFerroviasFormatado = false;
            }

            #endregion

            var result = this._painelExpedicaoRepository.ObterPainelExpedicaoConsultaDocumentos(
                                                                            pagination,
                                                                            dataInicioFormatado,
                                                                            dataFimFormatado,
                                                                            fluxo,
                                                                            origem,
                                                                            destino,
                                                                            codigosDosVagoesSeparados,
                                                                            cliente,
                                                                            expedidor,
                                                                            remetente,
                                                                            recebedor,
                                                                            codigosDasMercadoriasSeparados,
                                                                            segmento,
                                                                            os,
                                                                            prefixo,
                                                                            statusCteFormatado,
                                                                            statusNfeFormatado,
                                                                            statusWebDanfeFormatado,
                                                                            statusTicketFormatado,
                                                                            statusDclFormatado,
                                                                            statusLaudoMercadoriaFormatado,
                                                                            outrasFerroviasFormatado,
                                                                            codigosDosItensDespachoSeparados,
                                                                            cnpjRaizRecebedor,
                                                                            malhaOrigem,
                                                                            exportarExcel);

            // Caso o filtro de vagões vier preenchido, devemos mostrar na ordem que foi inserido no filtro.
            // Isso não é possível realizar dentro do repositorio e devemos fazer a ordenação via código
            var resultadoOrdenado = new ResultadoPaginado<PainelExpedicaoConsultarDocumentosDto>();
            // Se não foi informado nada no filtro vagões, apenas retorna o que o repositório retornou
            if (codigosDosVagoesSeparados.Length == 0)
            {
                resultadoOrdenado = result;
            }
            // Caso contrário, ordenamos a lista em uma nova lista
            else
            {
                // Guardando o total
                resultadoOrdenado.Total = result.Total;
                resultadoOrdenado.Items = new List<PainelExpedicaoConsultarDocumentosDto>();
                // Para cada vagão do filtro
                foreach (var codigoVagao in codigosDosVagoesSeparados)
                {
                    // Percorrendo os itens que voltaram do repositório
                    foreach (var item in result.Items)
                    {
                        // Se o item do repositório for igual ao código do vagão da vez, inclui na nova lista
                        if (item.Vagao == codigoVagao)
                        {
                            resultadoOrdenado.Items.Add(item);
                        }
                    }
                }
            }

            return resultadoOrdenado;
        }

        public List<PainelExpedicaoRelatorioLaudoDto> ObterPainelExpedicaoRelatorioLaudos(
                                                                            DetalhesPaginacaoWeb pagination,
                                                                            string dataInicio,
                                                                            string dataFim,
                                                                            string fluxo,
                                                                            string origem,
                                                                            string destino,
                                                                            string vagoes,
                                                                            string cliente,
                                                                            string expedidor,
                                                                            string remetente,
                                                                            string mercadorias,
                                                                            string segmento,
                                                                            string os,
                                                                            string prefixo,
                                                                            string itensDespacho,
                                                                            string cnpjRaizRecebedor,
                                                                            string malhaOrigem)
        {
            #region Formatação dos Parâmetros

            var dataInicioFormatado = DateTime.Now;
            if (!string.IsNullOrEmpty(dataInicio))
                dataInicioFormatado = DateTime.ParseExact(dataInicio, "dd/MM/yyyy", CultureInfo.InvariantCulture);

            var dataFimFormatado = DateTime.Now;
            if (!string.IsNullOrEmpty(dataInicio))
                dataFimFormatado = DateTime.ParseExact(dataFim, "dd/MM/yyyy", CultureInfo.InvariantCulture);

            vagoes = vagoes ?? string.Empty;
            mercadorias = mercadorias ?? string.Empty;
            segmento = segmento.Trim().ToUpper();
            segmento = (segmento == "TODOS" ? "" : segmento);
            itensDespacho = itensDespacho ?? string.Empty;

            string[] codigosDosVagoesSeparados =
                vagoes.ToUpper()
                    .Trim()
                    .Replace(" ", string.Empty)
                    .Replace(",", ";")
                    .Replace(".", ";")
                    .Split(';')
                    .Where(e => !string.IsNullOrWhiteSpace(e))
                    .Distinct()
                    .ToArray();

            string[] codigosDasMercadoriasSeparados =
                mercadorias.ToUpper()
                    .Trim()
                    .Replace(" ", string.Empty)
                    .Replace(",", ";")
                    .Replace(".", ";")
                    .Split(';')
                    .Where(e => !string.IsNullOrWhiteSpace(e))
                    .ToArray();

            string[] codigosDosItensDespachoSeparados =
                itensDespacho.ToUpper()
                    .Trim()
                    .Replace(" ", string.Empty)
                    .Replace(",", ";")
                    .Replace(".", ";")
                    .Split(';')
                    .Where(e => !string.IsNullOrWhiteSpace(e))
                    .ToArray();

            #endregion

            var results = this._painelExpedicaoRepository.ObterPainelExpedicaoRelatorioLaudos(
                                                                            pagination,
                                                                            dataInicioFormatado,
                                                                            dataFimFormatado,
                                                                            fluxo,
                                                                            origem,
                                                                            destino,
                                                                            codigosDosVagoesSeparados,
                                                                            cliente,
                                                                            expedidor,
                                                                            remetente,
                                                                            codigosDasMercadoriasSeparados,
                                                                            segmento,
                                                                            os,
                                                                            prefixo,
                                                                            codigosDosItensDespachoSeparados,
                                                                            cnpjRaizRecebedor,
                                                                            malhaOrigem);


            var retorno = new List<PainelExpedicaoRelatorioLaudoDto>();

            results.Items = (from list in results.Items.AsEnumerable()
                             group list by new
                             {
                                 list.Vagao,
                                 RemetenteNfe = Regex.Replace(list.RemetenteNfe, "[^\\w\\s_]", ""),
                                 list.DataCarregamento,
                                 list.FluxoComercial,
                                 list.Origem,
                                 list.Destino,
                                 list.FluxoSegmento,
                                 list.DataRecebimentoLaudo,
                                 list.LaudoTipo,
                                 list.Empresa,
                                 list.Produto,
                                 list.CnpjEmpresa,
                                 list.NumeroLaudo,
                                 list.NumeroOsLaudo,
                                 list.Classificador,
                                 list.CpfClassificador,
                                 list.CodigoClassificacao,
                                 list.Porcentagem
                             }
                                 into groupby
                             select new PainelExpedicaoRelatorioLaudoDto
                             {
                                 Vagao = groupby.Key.Vagao,
                                 RemetenteNfe = groupby.Max(x => x.RemetenteNfe),
                                 DataCarregamento = groupby.Key.DataCarregamento,
                                 FluxoComercial = groupby.Key.FluxoComercial,
                                 Origem = groupby.Key.Origem,
                                 Destino = groupby.Key.Destino,
                                 FluxoSegmento = groupby.Key.FluxoSegmento,
                                 DataRecebimentoLaudo = groupby.Key.DataRecebimentoLaudo,
                                 LaudoTipo = groupby.Key.LaudoTipo,
                                 Empresa = groupby.Key.Empresa,
                                 Produto = groupby.Key.Produto,
                                 CnpjEmpresa = groupby.Key.CnpjEmpresa,
                                 NumeroLaudo = groupby.Key.NumeroLaudo,
                                 NumeroOsLaudo = groupby.Key.NumeroOsLaudo,
                                 Classificador = groupby.Key.Classificador,
                                 CpfClassificador = groupby.Key.CpfClassificador,
                                 CodigoClassificacao = groupby.Key.CodigoClassificacao,
                                 Porcentagem = groupby.Key.Porcentagem,
                                 NumeroNfe = string.Join(",", groupby.Select(x => x.NumeroNfe).OrderBy(x => x).Distinct())
                             }).Distinct().ToList();



            foreach (var result in results.Items)
            {
                PainelExpedicaoRelatorioLaudoDto registroEncontrado;

                registroEncontrado = retorno.FirstOrDefault(r => r.Vagao == result.Vagao &&
                                                                 r.FluxoComercial == result.FluxoComercial &&
                                                                 r.DataRecebimentoLaudo == result.DataRecebimentoLaudo &&
                                                                 r.NumeroNfe == result.NumeroNfe);

                var insereRegistro = false;
                if (registroEncontrado == null)
                {
                    registroEncontrado = new PainelExpedicaoRelatorioLaudoDto();
                    registroEncontrado.Vagao = result.Vagao;
                    registroEncontrado.NumeroNfe = result.NumeroNfe;
                    registroEncontrado.RemetenteNfe = result.RemetenteNfe;
                    registroEncontrado.DataCarregamento = result.DataCarregamento;
                    registroEncontrado.FluxoComercial = result.FluxoComercial;
                    registroEncontrado.Origem = result.Origem;
                    registroEncontrado.Destino = result.Destino;
                    registroEncontrado.FluxoSegmento = result.FluxoSegmento;
                    registroEncontrado.DataRecebimentoLaudo = result.DataRecebimentoLaudo;
                    registroEncontrado.LaudoTipo = result.LaudoTipo;
                    registroEncontrado.Empresa = result.Empresa;
                    registroEncontrado.Produto = result.Produto;
                    registroEncontrado.CnpjEmpresa = result.CnpjEmpresa;
                    registroEncontrado.NumeroLaudo = result.NumeroLaudo;
                    registroEncontrado.NumeroOsLaudo = result.NumeroOsLaudo;
                    registroEncontrado.Classificador = result.Classificador;
                    registroEncontrado.CpfClassificador = result.CpfClassificador;

                    insereRegistro = true;
                }

                switch (result.CodigoClassificacaoEnum)
                {
                    case LaudoMercadoriaTipoClassificacaoEnum.Umidade:
                        registroEncontrado.ClassificacaoUmidade = (registroEncontrado.ClassificacaoUmidade ?? 0) + result.Porcentagem;
                        break;
                    case LaudoMercadoriaTipoClassificacaoEnum.MateriaEstranhaImpura:
                        registroEncontrado.ClassificacaoMateriaEstranhaImpura = (registroEncontrado.ClassificacaoMateriaEstranhaImpura ?? 0) + result.Porcentagem;
                        break;
                    case LaudoMercadoriaTipoClassificacaoEnum.TotalAvariados:
                        registroEncontrado.ClassificacaoTotalAvariados = (registroEncontrado.ClassificacaoTotalAvariados ?? 0) + result.Porcentagem;
                        break;
                    case LaudoMercadoriaTipoClassificacaoEnum.Danificado:
                        registroEncontrado.ClassificacaoDanificado = (registroEncontrado.ClassificacaoDanificado ?? 0) + result.Porcentagem;
                        break;
                    case LaudoMercadoriaTipoClassificacaoEnum.Quebrado:
                        registroEncontrado.ClassificacaoQuebrado = (registroEncontrado.ClassificacaoQuebrado ?? 0) + result.Porcentagem;
                        break;
                    case LaudoMercadoriaTipoClassificacaoEnum.Ardido:
                        registroEncontrado.ClassificacaoArdido = (registroEncontrado.ClassificacaoArdido ?? 0) + result.Porcentagem;
                        break;
                    case LaudoMercadoriaTipoClassificacaoEnum.Ph:
                        registroEncontrado.ClassificacaoPh = (registroEncontrado.ClassificacaoPh ?? 0) + result.Porcentagem;
                        break;
                    case LaudoMercadoriaTipoClassificacaoEnum.Esverdeado:
                        registroEncontrado.ClassificacaoEsverdeado = (registroEncontrado.ClassificacaoEsverdeado ?? 0) + result.Porcentagem;
                        break;
                    case LaudoMercadoriaTipoClassificacaoEnum.Queimado:
                        registroEncontrado.ClassificacaoQueimado = (registroEncontrado.ClassificacaoQueimado ?? 0) + result.Porcentagem;
                        break;
                    case LaudoMercadoriaTipoClassificacaoEnum.Picado:
                        registroEncontrado.ClassificacaoPicado = (registroEncontrado.ClassificacaoPicado ?? 0) + result.Porcentagem;
                        break;
                    case LaudoMercadoriaTipoClassificacaoEnum.Mofado:
                        registroEncontrado.ClassificacaoMofado = (registroEncontrado.ClassificacaoMofado ?? 0) + result.Porcentagem;
                        break;
                    case LaudoMercadoriaTipoClassificacaoEnum.FallingNumber:
                        registroEncontrado.ClassificacaoFallingNumber = (registroEncontrado.ClassificacaoFallingNumber ?? 0) + result.Porcentagem;
                        break;
                    case LaudoMercadoriaTipoClassificacaoEnum.Germinado:
                        registroEncontrado.ClassificacaoGerminado = (registroEncontrado.ClassificacaoGerminado ?? 0) + result.Porcentagem;
                        break;
                    case LaudoMercadoriaTipoClassificacaoEnum.DanificadoInsetos:
                        registroEncontrado.ClassificacaoDanificadoInsetos = (registroEncontrado.ClassificacaoDanificadoInsetos ?? 0) + result.Porcentagem;
                        break;
                }

                if (insereRegistro)
                {
                    retorno.Add(registroEncontrado);
                }
            }

            return retorno.OrderBy(r => r.DataCarregamento).ThenBy(r => r.Vagao).ThenBy(r => r.FluxoComercial).ToList();
        }

        public List<PainelExpedicaoRelatorioLaudoDto> ObterRelatorioLaudosPorOsId(decimal osId, decimal recebedorId)
        {
            var retorno = new List<PainelExpedicaoRelatorioLaudoDto>();

            var results = this._painelExpedicaoRepository.ObterRelatorioLaudosPorOsId(osId, recebedorId);

            results = (from list in results.AsEnumerable()
                       group list by new
                       {
                           list.Vagao,
                           RemetenteNfe = Regex.Replace(list.RemetenteNfe, "[^\\w\\s_]", ""),
                           list.DataCarregamento,
                           list.FluxoComercial,
                           list.Origem,
                           list.Destino,
                           list.FluxoSegmento,
                           list.DataRecebimentoLaudo,
                           list.LaudoTipo,
                           list.Empresa,
                           list.Produto,
                           list.CnpjEmpresa,
                           list.NumeroLaudo,
                           list.NumeroOsLaudo,
                           list.Classificador,
                           list.CpfClassificador,
                           list.CodigoClassificacao,
                           list.Porcentagem
                       }
                           into groupby
                       select new PainelExpedicaoRelatorioLaudoDto
                       {
                           Vagao = groupby.Key.Vagao,
                           RemetenteNfe = groupby.Max(x => x.RemetenteNfe),
                           DataCarregamento = groupby.Key.DataCarregamento,
                           FluxoComercial = groupby.Key.FluxoComercial,
                           Origem = groupby.Key.Origem,
                           Destino = groupby.Key.Destino,
                           FluxoSegmento = groupby.Key.FluxoSegmento,
                           DataRecebimentoLaudo = groupby.Key.DataRecebimentoLaudo,
                           LaudoTipo = groupby.Key.LaudoTipo,
                           Empresa = groupby.Key.Empresa,
                           Produto = groupby.Key.Produto,
                           CnpjEmpresa = groupby.Key.CnpjEmpresa,
                           NumeroLaudo = groupby.Key.NumeroLaudo,
                           NumeroOsLaudo = groupby.Key.NumeroOsLaudo,
                           Classificador = groupby.Key.Classificador,
                           CpfClassificador = groupby.Key.CpfClassificador,
                           CodigoClassificacao = groupby.Key.CodigoClassificacao,
                           Porcentagem = groupby.Key.Porcentagem,
                           NumeroNfe = string.Join(",", groupby.Select(x => x.NumeroNfe).OrderBy(x => x).Distinct())
                       }).Distinct().ToList();

            results = results.OrderBy(o => o.DataRecebimentoLaudo).ToList();

            foreach (var result in results)
            {
                PainelExpedicaoRelatorioLaudoDto registroEncontrado;
                if (!String.IsNullOrEmpty(result.NumeroNfe))
                {
                    registroEncontrado = retorno.FirstOrDefault(r => r.Vagao == result.Vagao &&
                                                                     r.FluxoComercial == result.FluxoComercial &&
                                                                     ////r.DataRecebimentoLaudo == result.DataRecebimentoLaudo &&
                                                                     r.NumeroNfe == result.NumeroNfe);
                }
                else
                {
                    registroEncontrado = retorno.FirstOrDefault(r => r.Vagao == result.Vagao &&
                                                                     r.FluxoComercial == result.FluxoComercial ////&&
                                                                                                               ////r.DataRecebimentoLaudo == result.DataRecebimentoLaudo
                    );
                }

                var insereRegistro = false;
                if (registroEncontrado == null)
                {
                    registroEncontrado = new PainelExpedicaoRelatorioLaudoDto();
                    registroEncontrado.Vagao = result.Vagao;
                    registroEncontrado.NumeroNfe = result.NumeroNfe;
                    registroEncontrado.RemetenteNfe = result.RemetenteNfe;
                    registroEncontrado.DataCarregamento = result.DataCarregamento;
                    registroEncontrado.FluxoComercial = result.FluxoComercial;
                    registroEncontrado.Origem = result.Origem;
                    registroEncontrado.Destino = result.Destino;
                    registroEncontrado.FluxoSegmento = result.FluxoSegmento;
                    registroEncontrado.DataRecebimentoLaudo = result.DataRecebimentoLaudo;
                    registroEncontrado.LaudoTipo = result.LaudoTipo;
                    registroEncontrado.Empresa = result.Empresa;
                    registroEncontrado.Produto = result.Produto;
                    registroEncontrado.CnpjEmpresa = result.CnpjEmpresa;
                    registroEncontrado.NumeroLaudo = result.NumeroLaudo;
                    registroEncontrado.NumeroOsLaudo = result.NumeroOsLaudo;
                    registroEncontrado.Classificador = result.Classificador;
                    registroEncontrado.CpfClassificador = result.CpfClassificador;

                    insereRegistro = true;
                }

                switch (result.CodigoClassificacaoEnum)
                {
                    case LaudoMercadoriaTipoClassificacaoEnum.Umidade:
                        registroEncontrado.ClassificacaoUmidade = result.Porcentagem; ////(registroEncontrado.ClassificacaoUmidade ?? 0) + result.Porcentagem;
                        break;
                    case LaudoMercadoriaTipoClassificacaoEnum.MateriaEstranhaImpura:
                        registroEncontrado.ClassificacaoMateriaEstranhaImpura = result.Porcentagem; ////(registroEncontrado.ClassificacaoMateriaEstranhaImpura ?? 0) + result.Porcentagem;
                        break;
                    case LaudoMercadoriaTipoClassificacaoEnum.TotalAvariados:
                        registroEncontrado.ClassificacaoTotalAvariados = result.Porcentagem; ////(registroEncontrado.ClassificacaoTotalAvariados ?? 0) + result.Porcentagem;
                        break;
                    case LaudoMercadoriaTipoClassificacaoEnum.Danificado:
                        registroEncontrado.ClassificacaoDanificado = result.Porcentagem; ////(registroEncontrado.ClassificacaoDanificado ?? 0) + result.Porcentagem;
                        break;
                    case LaudoMercadoriaTipoClassificacaoEnum.Quebrado:
                        registroEncontrado.ClassificacaoQuebrado = result.Porcentagem; ////(registroEncontrado.ClassificacaoQuebrado ?? 0) + result.Porcentagem;
                        break;
                    case LaudoMercadoriaTipoClassificacaoEnum.Ardido:
                        registroEncontrado.ClassificacaoArdido = result.Porcentagem; ////(registroEncontrado.ClassificacaoArdido ?? 0) + result.Porcentagem;
                        break;
                    case LaudoMercadoriaTipoClassificacaoEnum.Ph:
                        registroEncontrado.ClassificacaoPh = result.Porcentagem; ////(registroEncontrado.ClassificacaoPh ?? 0) + result.Porcentagem;
                        break;
                    case LaudoMercadoriaTipoClassificacaoEnum.Esverdeado:
                        registroEncontrado.ClassificacaoEsverdeado = result.Porcentagem; ////(registroEncontrado.ClassificacaoEsverdeado ?? 0) + result.Porcentagem;
                        break;
                    case LaudoMercadoriaTipoClassificacaoEnum.Queimado:
                        registroEncontrado.ClassificacaoQueimado = result.Porcentagem; ////(registroEncontrado.ClassificacaoQueimado ?? 0) + result.Porcentagem;
                        break;
                    case LaudoMercadoriaTipoClassificacaoEnum.Picado:
                        registroEncontrado.ClassificacaoPicado = result.Porcentagem; ////(registroEncontrado.ClassificacaoPicado ?? 0) + result.Porcentagem;
                        break;
                    case LaudoMercadoriaTipoClassificacaoEnum.Mofado:
                        registroEncontrado.ClassificacaoMofado = result.Porcentagem; ////(registroEncontrado.ClassificacaoMofado ?? 0) + result.Porcentagem;
                        break;
                    case LaudoMercadoriaTipoClassificacaoEnum.FallingNumber:
                        registroEncontrado.ClassificacaoFallingNumber = result.Porcentagem; ////(registroEncontrado.ClassificacaoFallingNumber ?? 0) + result.Porcentagem;
                        break;
                    case LaudoMercadoriaTipoClassificacaoEnum.Germinado:
                        registroEncontrado.ClassificacaoGerminado = result.Porcentagem; ////(registroEncontrado.ClassificacaoGerminado ?? 0) + result.Porcentagem;
                        break;
                    case LaudoMercadoriaTipoClassificacaoEnum.DanificadoInsetos:
                        registroEncontrado.ClassificacaoDanificadoInsetos = result.Porcentagem; ////(registroEncontrado.ClassificacaoDanificadoInsetos ?? 0) + result.Porcentagem;
                        break;
                }

                if (insereRegistro)
                {
                    retorno.Add(registroEncontrado);
                }
            }

            return retorno.OrderBy(r => r.Vagao).ThenBy(r => r.FluxoComercial).ToList();
        }

        /* TODO :: Modificar o tipo da lista abaixo para o correto. A ideia é de ser o documento */
        public List<int> ImprimirDocumentos(List<int> ids)
        {
            #region Validações

            if (ids == null || ids.Count == 0)
                throw new TransactionException("Nenhum registro selecionado.");

            #endregion

            /* TODO :: Modificar o tipo da lista abaixo para o correto. A ideia é de ser o documento. */
            List<int> documentos = new List<int>();

            foreach (var id in ids)
            {
                var documento = this._painelExpedicaoRepository.ImprimirDocumentos(id);
                documentos.Add(documento);
            }

            return documentos;
        }

        #endregion

        #region Conferência de Arquivos

        public ResultadoPaginado<PainelExpedicaoConferenciaArquivosDto> ObterPainelExpedicaoConferenciaArquivos(
                                                                DetalhesPaginacaoWeb pagination,
                                                                DateTime dataCadastroBo,
                                                                DateTime dataCadastroFinalBo,
                                                                string fluxo,
                                                                string origem,
                                                                string destino,
                                                                string serie,
                                                                string vagoes,
                                                                string localAtual,
                                                                string cliente,
                                                                string expedidor,
                                                                string mercadorias,
                                                                string segmento,
                                                                bool arquivosPatio,
                                                                Usuario usuario,
                                                                ref PainelExpedicaoPerfilAcessoEnum perfilAcessoUsuario)
        {


            vagoes = vagoes ?? string.Empty;
            mercadorias = mercadorias ?? string.Empty;
            segmento = segmento.Trim().ToUpper();
            segmento = (segmento == "TODOS" ? "" : segmento);

            string[] codigosDosVagoesSeparados =
                vagoes.ToUpper()
                    .Trim()
                    .Replace(" ", string.Empty)
                    .Replace(",", ";")
                    .Replace(".", ";")
                    .Split(';')
                    .Where(e => !string.IsNullOrWhiteSpace(e))
                    .ToArray();

            string[] codigosDasMercadoriasSeparados =
                mercadorias.ToUpper()
                    .Trim()
                    .Replace(" ", string.Empty)
                    .Replace(",", ";")
                    .Replace(".", ";")
                    .Split(';')
                    .Where(e => !string.IsNullOrWhiteSpace(e))
                    .ToArray();


            // Identificar o perfil de acesso do usuário
            // Possíveis perfils:
            // * Pátio (Usuário pertence ao grupo de usuário PAT)
            // * Central (Usuário pertence ao grupo de usuário CFAT)
            perfilAcessoUsuario = this.VerificarPerfilUsuarioPainelExpedicao(usuario);

            var result = this._painelExpedicaoRepository.ObterPainelExpedicaoConferenciaArquivos(pagination,
                                                                                        dataCadastroBo,
                                                                                        dataCadastroFinalBo,
                                                                                        fluxo,
                                                                                        origem,
                                                                                        destino,
                                                                                        serie,
                                                                                        codigosDosVagoesSeparados,
                                                                                        localAtual,
                                                                                        cliente,
                                                                                        expedidor,
                                                                                        codigosDasMercadoriasSeparados,
                                                                                        segmento,
                                                                                        arquivosPatio,
                                                                                        perfilAcessoUsuario);

            // Lógica para incluir cor na grid dos MDs
            // O último registro deve estar setado como NÃO e os anteriores devem estar setados como SIM.
            // A ordenação para termos como base o primeiro e o último baseia-se pelo ID da BO_BOLAR_BO, ou seja, o campo Id.

            // Obtendo todos os códigos dos vagões distintos
            var codigoVagoes = result.Items.Select(i => i.Vagao).Distinct().ToList();
            foreach (var codigoVagao in codigoVagoes)
            {
                var registrosVagao = result.Items.Where(i => i.Vagao == codigoVagao).ToList();

                // Obtendo a sequencia de MD do vagão da iteração ordenado pelo ID da BO_BOLAR_BO
                var sequenciaMds = registrosVagao.GroupBy(x => x.Id)
                                                 .Select(x => x.First())
                                                 .OrderBy(i => i.Id)
                                                 .Select(i => i.MultiploDespacho)
                                                 .ToList();

                bool sucesso = true;
                var quantidadeMd = sequenciaMds.Count;
                for (int i = 0; i < quantidadeMd && sucesso == true; i++)
                {
                    // Verificamos se o campo MD está setado com o último como NAO e os anteriores como SIM.
                    // Se alguem estiver errado, setamos como sucesso = false;
                    if (sequenciaMds[i] != ((i == quantidadeMd - 1) ? "NAO" : "SIM"))
                    {
                        sucesso = false;
                    }
                }

                // Atualizando a cor de sucesso dos registros do vagão de acordo com a lógica anterior.
                foreach (var registro in registrosVagao)
                {
                    registro.CorSucesso = sucesso;
                }
            }

            return result;
        }

        public PainelExpedicaoPerfilAcessoEnum ObterPerfilUsuarioPainelExpedicao(Usuario usuario)
        {
            return this.VerificarPerfilUsuarioPainelExpedicao(usuario);
        }

        /// <summary>
        ///     Verifica se um determinado usuário
        /// </summary>
        /// <param name="usuario">Usuario a ser verificado</param>
        /// <returns>Valor booleano</returns>
        private PainelExpedicaoPerfilAcessoEnum VerificarPerfilUsuarioPainelExpedicao(Usuario usuario)
        {
            PainelExpedicaoPerfilAcessoEnum perfilAcessoUsuario = PainelExpedicaoPerfilAcessoEnum.Indefinido;

            bool verificaPerfilPatio = true;

            GrupoUsuario gpCFAT = _grupoUsuarioRepository.ObterPorCodigo("CFAT");

            if (gpCFAT != null)
            {
                if (this._agrupamentoUsuarioRepository.VerificarUsuarioGrupo(usuario, gpCFAT))
                {
                    perfilAcessoUsuario = PainelExpedicaoPerfilAcessoEnum.Central;
                    verificaPerfilPatio = false;

                    // Verificar se usuário da Central é Super
                    // O usuário Central Super possui acesso para confirmar/recusar arquivos pendentes no Pátio
                    var permissaoUsuario = _usuarioRepository.
                        ObterPermissoes("PAINELEXPEDICAOARQUIVOS", _acaoConfirmarRecusarSuper, usuario);
                    if (permissaoUsuario != null)
                    {
                        if (permissaoUsuario.Count > 0)
                        {
                            object[] permissaoObj = (object[])permissaoUsuario[0];
                            if (permissaoObj[1] != null)
                                perfilAcessoUsuario = PainelExpedicaoPerfilAcessoEnum.CentralSuper;
                        }
                    }
                }
            }

            if (verificaPerfilPatio)
            {
                GrupoUsuario gpPAT = _grupoUsuarioRepository.ObterPorCodigo("PAT");
                if (this._agrupamentoUsuarioRepository.VerificarUsuarioGrupo(usuario, gpPAT))
                {
                    perfilAcessoUsuario = PainelExpedicaoPerfilAcessoEnum.Patio;
                }
            }


            return perfilAcessoUsuario;
        }

        public void SalvarAlteracoesConferenciaArquivos(List<PainelExpedicaoConferenciaArquivosDto> registros)
        {
            if (registros == null)
                return;

            registros = registros.Where(r => r.Id > 0).ToList();

            #region Validações

            foreach (var registro in registros)
            {
                // 1) Vagão e/ou fluxo não pode estar vazio
                if (String.IsNullOrEmpty(registro.Vagao))
                    throw new TransactionException("Informe o valor de todos os vagões.");

                if (String.IsNullOrEmpty(registro.Fluxo))
                    throw new TransactionException("Informe o valor de todos os fluxos.");

                // 2) Valida se o vagão é valido/existente;
                var vagao = _vagaoRepository.ObterPorCodigo(registro.Vagao);
                if (vagao == null)
                    throw new TransactionException(String.Format("Vagão {0} não encontrado ou inexistente.", registro.Vagao));

                // 3) Validações de Fluxo Comercial 
                // 3.1) Valida se o fluxo é válido/existente;
                var fluxo = _fluxoComercialRepository.ObterPorCodigo(registro.Fluxo);
                if (fluxo == null)
                    throw new TransactionException(String.Format("Fluxo {0} não encontrado ou inexistente.", registro.Fluxo));

                // 3.2) Valida se o fluxo está vigente
                if (fluxo.DataVigencia.HasValue && fluxo.DataVigencia.Value < DateTime.Now)
                {
                    throw new TransactionException(String.Format("Fluxo {0} fora de vigência. Por favor, entre em contato no e-mail gestao.fluxos@rumolog.com para regularizá-lo.", registro.Fluxo));
                }

                // 4) Se o peso total e o peso rateio > 0;
                if (registro.PesoTotal <= 0)
                    throw new TransactionException(String.Format("Peso total do vagão {0} deve ser maior que zero.", registro.Vagao));

                if (registro.PesoRateioNfe <= 0)
                    throw new TransactionException(String.Format("Peso rateio do vagão {0} deve ser maior que zero.", registro.Vagao));

                if (registro.PesoRateioNfe > registro.PesoNfe)
                    throw new TransactionException(String.Format("Peso rateio da nota fiscal {0} do vagão {1} é maior que o peso total da nota fiscal.", registro.ChaveNota, registro.Vagao));

                // 5) O somatório dos peso rateio do vagão deve ser menor ou igual ao seu peso total;
                if (fluxo.Segmento != "LIQUID")
                {
                    decimal pesoTotalRateio = 0;
                    pesoTotalRateio = registro.IdAgrupamentoMd.HasValue
                                          ? registros.Where(r => r.IdAgrupamentoMd == registro.IdAgrupamentoMd).Sum(
                                              r => r.PesoRateioNfe)
                                          : (registro.IdAgrupamentoSimples.HasValue
                                                ? registros.Where(r => r.IdAgrupamentoSimples == registro.IdAgrupamentoSimples).Sum(
                                                r => r.PesoRateioNfe) : registro.PesoRateioNfe);

                    if (pesoTotalRateio > registro.PesoTotal)
                        throw new TransactionException(String.Format("O peso total de rateio do vagão {0} é superior seu peso total.", registro.Vagao));
                }

                // 6) Máscara do conteiner;
                if (!String.IsNullOrEmpty(registro.Conteiner))
                {
                    var regex = new Regex("[A-Z]{4}[0-9]{7}");
                    Match match = regex.Match(registro.Conteiner);
                    if (!match.Success)
                        throw new TransactionException(
                            String.Format(
                                "Conteiner do vagão {0} inválido. Deve conter quatro caracteres seguido de sete digitos.",
                                registro.Vagao));
                }

                // 7) Conteiner obrigatório para segmento Brado e opcional para Industrial com mercadoria PALLET. Outros segmentos Conteiner deve ser vazio;
                if (!String.IsNullOrEmpty(registro.Conteiner))
                {
                    if (!(fluxo.Segmento == "BRADO" || (fluxo.Segmento == "INDUSTR" && fluxo.Mercadoria.DescricaoResumida.Contains("PALLET"))))
                    {
                        throw new TransactionException(
                            String.Format(
                                "Conteiner do vagão {0} não deve ser informado pois o segmento do fluxo {1} não é do segmento Brado ou Industrial de mercadoria Pallet.",
                                registro.Vagao,
                                registro.Fluxo));
                    }
                }
                else
                {
                    if (fluxo.Segmento == "BRADO" || (fluxo.Segmento == "INDUSTR" && fluxo.Mercadoria.DescricaoResumida.Contains("PALLET")))
                    {
                        throw new TransactionException(
                            String.Format(
                                "Conteiner do vagão {0} deve ser informado pois o segmento do fluxo {1} é do segmento Brado ou Industrial de mercadoria Pallet.",
                                registro.Vagao,
                                registro.Fluxo));
                    }
                }

                #region Peso Real

                registro.PesoReal = registros.Where(r => r.Vagao == registro.Vagao && r.Fluxo == registro.Fluxo).Sum(r => r.PesoRateioNfe);

                #endregion
            }

            #endregion

            foreach (var registro in registros)
            {
                this._painelExpedicaoRepository.SalvarAlteracoesConferenciaArquivos(registro);
            }
        }

        public void AtualizarStatusConferenciaArquivos(List<PainelExpedicaoConferenciaArquivosDto> registros,
                                                        bool recusar,
                                                        Usuario usuario)
        {
            if (registros == null)
                return;

            registros = registros.Where(r => r.Id > 0).ToList();

            PainelExpedicaoPerfilAcessoEnum perfilAcessoUsuario = this.ObterPerfilUsuarioPainelExpedicao(usuario);

            #region Validações

            #region Vigência dos Fluxos

            if (!recusar)
            {
                string fluxosNaoVigentes = String.Empty;
                DateTime dataAtual = DateTime.Now;
                foreach (var registro in registros)
                {
                    var fluxo = _fluxoComercialRepository.ObterPorCodigoSimplificado(registro.FluxoSemCaracteres);
                    if (fluxo == null)
                        throw new TranslogicException(String.Format("O Fluxo {0} não é válido.",
                                                                    registro.FluxoSemCaracteres));

                    if (fluxo.DataVigencia.HasValue && fluxo.DataVigencia.Value < dataAtual)
                    {
                        if (!fluxosNaoVigentes.Contains(registro.Fluxo))
                        {
                            fluxosNaoVigentes += String.Format("{0}, ", registro.Fluxo);
                        }
                    }
                }

                if (!String.IsNullOrEmpty(fluxosNaoVigentes))
                {
                    var tamanho = fluxosNaoVigentes.Length;
                    // Removendo a última vírgula e espaço da string fluxosNaoVigentes
                    fluxosNaoVigentes = fluxosNaoVigentes.Remove(tamanho - 2);
                    throw new TranslogicException(
                        String.Format(
                            "Os seguintes fluxos estão fora de vigência: {0}. Por favor, entre em contato no e-mail gestao.fluxos@rumolog.com para regularizá-lo(s).",
                            fluxosNaoVigentes));
                }
            }

            #endregion

            #endregion

            foreach (var registro in registros)
            {
                this._painelExpedicaoRepository.AtualizarStatusConferenciaArquivos((int)registro.Id,
                                                                            recusar,
                                                                            (int)usuario.Id,
                                                                            perfilAcessoUsuario,
                                                                            registro.ObservacaoRecusado);
            }
        }

        #endregion

        #region Relatórios

        public ResultadoPaginado<RelatorioHistoricoFaturamentosDto> ObterHistoricoFaturamentos(
                                                                            DetalhesPaginacaoWeb pagination,
                                                                            string dataInicio,
                                                                            string dataFim,
                                                                            string fluxo,
                                                                            string origem,
                                                                            string destino,
                                                                            string vagoes,
                                                                            string cliente,
                                                                            string expedidor,
                                                                            string remetente,
                                                                            string recebedor,
                                                                            string mercadorias,
                                                                            string segmento,
                                                                            string os,
                                                                            string prefixo,
                                                                            string statusCte,
                                                                            string statusNfe,
                                                                            string statusWebDanfe,
                                                                            string statusTicket,
                                                                            string statusDcl,
                                                                            string statusLaudoMercadoria,
                                                                            string outrasFerrovias,
                                                                            string itensDespacho,
                                                                            string cnpjRaizRecebedor,
                                                                            string malhaOrigem,
                                                                            bool exportarExcel)
        {
            #region Formatação dos Parâmetros

            var dataInicioFormatado = DateTime.Now;
            if (!string.IsNullOrEmpty(dataInicio))
            {
                dataInicioFormatado = DateTime.ParseExact(dataInicio, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            }

            var dataFimFormatado = DateTime.Now;
            if (!string.IsNullOrEmpty(dataFim))
            {
                dataFimFormatado = DateTime.ParseExact(dataFim, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                dataFimFormatado = dataFimFormatado.AddDays(1);
            }

            vagoes = vagoes ?? string.Empty;
            mercadorias = mercadorias ?? string.Empty;
            segmento = segmento.Trim().ToUpper();
            segmento = (segmento == "TODOS" ? "" : segmento);
            itensDespacho = itensDespacho ?? string.Empty;

            string[] codigosDosVagoesSeparados =
                vagoes.ToUpper()
                    .Trim()
                    .Replace(" ", string.Empty)
                    .Replace(",", ";")
                    .Replace(".", ";")
                    .Split(';')
                    .Where(e => !string.IsNullOrWhiteSpace(e))
                    .Distinct()
                    .ToArray();

            string[] codigosDasMercadoriasSeparados =
                mercadorias.ToUpper()
                    .Trim()
                    .Replace(" ", string.Empty)
                    .Replace(",", ";")
                    .Replace(".", ";")
                    .Split(';')
                    .Where(e => !string.IsNullOrWhiteSpace(e))
                    .ToArray();

            string[] codigosDosItensDespachoSeparados =
                itensDespacho.ToUpper()
                    .Trim()
                    .Replace(" ", string.Empty)
                    .Replace(",", ";")
                    .Replace(".", ";")
                    .Split(';')
                    .Where(e => !string.IsNullOrWhiteSpace(e))
                    .ToArray();

            bool statusCteFormatado = false;
            if (!string.IsNullOrEmpty(statusCte))
            {
                if (!bool.TryParse(statusCte, out statusCteFormatado))
                    statusCteFormatado = false;
            }

            bool statusNfeFormatado = false;
            if (!string.IsNullOrEmpty(statusNfe))
            {
                if (!bool.TryParse(statusNfe, out statusNfeFormatado))
                    statusNfeFormatado = false;
            }

            bool statusWebDanfeFormatado = false;
            if (!string.IsNullOrEmpty(statusWebDanfe))
            {
                if (!bool.TryParse(statusWebDanfe, out statusWebDanfeFormatado))
                    statusWebDanfeFormatado = false;
            }

            bool statusTicketFormatado = false;
            if (!string.IsNullOrEmpty(statusTicket))
            {
                if (!bool.TryParse(statusTicket, out statusTicketFormatado))
                    statusTicketFormatado = false;
            }

            bool statusDclFormatado = false;
            if (!string.IsNullOrEmpty(statusDcl))
            {
                if (!bool.TryParse(statusDcl, out statusDclFormatado))
                    statusDclFormatado = false;
            }

            bool statusLaudoMercadoriaFormatado = false;
            if (!string.IsNullOrEmpty(statusLaudoMercadoria))
            {
                if (!bool.TryParse(statusLaudoMercadoria, out statusLaudoMercadoriaFormatado))
                    statusLaudoMercadoriaFormatado = false;
            }

            bool outrasFerroviasFormatado = false;
            if (!string.IsNullOrEmpty(outrasFerrovias))
            {
                if (!bool.TryParse(outrasFerrovias, out outrasFerroviasFormatado))
                    outrasFerroviasFormatado = false;
            }

            #endregion

            var result = this._painelExpedicaoRepository.ObterHistoricoFaturamentos(
                                                                            pagination,
                                                                            dataInicioFormatado,
                                                                            dataFimFormatado,
                                                                            fluxo,
                                                                            origem,
                                                                            destino,
                                                                            codigosDosVagoesSeparados,
                                                                            cliente,
                                                                            expedidor,
                                                                            remetente,
                                                                            recebedor,
                                                                            codigosDasMercadoriasSeparados,
                                                                            segmento,
                                                                            os,
                                                                            prefixo,
                                                                            statusCteFormatado,
                                                                            statusNfeFormatado,
                                                                            statusWebDanfeFormatado,
                                                                            statusTicketFormatado,
                                                                            statusDclFormatado,
                                                                            statusLaudoMercadoriaFormatado,
                                                                            outrasFerroviasFormatado,
                                                                            codigosDosItensDespachoSeparados,
                                                                            cnpjRaizRecebedor,
                                                                            malhaOrigem,
                                                                            exportarExcel);

            // Caso o filtro de vagões vier preenchido, devemos mostrar na ordem que foi inserido no filtro.
            // Isso não é possível realizar dentro do repositorio e devemos fazer a ordenação via código
            var resultadoOrdenado = new ResultadoPaginado<RelatorioHistoricoFaturamentosDto>();
            // Se não foi informado nada no filtro vagões, apenas retorna o que o repositório retornou
            if (codigosDosVagoesSeparados.Length == 0)
            {
                resultadoOrdenado = result;
            }
            // Caso contrário, ordenamos a lista em uma nova lista
            else
            {
                // Guardando o total
                resultadoOrdenado.Total = result.Total;
                resultadoOrdenado.Items = new List<RelatorioHistoricoFaturamentosDto>();
                // Para cada vagão do filtro
                foreach (var codigoVagao in codigosDosVagoesSeparados)
                {
                    // Percorrendo os itens que voltaram do repositório
                    foreach (var item in result.Items)
                    {
                        // Se o item do repositório for igual ao código do vagão da vez, inclui na nova lista
                        if (item.Vagao == codigoVagao)
                        {
                            resultadoOrdenado.Items.Add(item);
                        }
                    }
                }
            }

            return resultadoOrdenado;
        }

        #endregion Relatórios

        #region Operadores

        /// <summary>
        /// Obtem os operadores disponíveis para a configuração das cores do painel de expedição
        /// </summary>
        /// <returns></returns>
        public IList<Operador> ObterOperadores()
        {
            return _painelExpedicaoRepository.ObterOperadores();
        }

        #endregion

        #region Origens

        /// <summary>
        /// Obtem as origens cadastradas na parametrização do painel de expedição
        /// </summary>
        /// <returns></returns>
        public IList<Origem> ObterOrigens()
        {
            return _painelExpedicaoOrigemRepository.ObterOrigens();
        }

        /// <summary>
        /// Obtem os dados da origem (Área operacional) informada
        /// </summary>
        /// <returns></returns>
        public Origem ObterOrigem(int id)
        {
            return _painelExpedicaoOrigemRepository.ObterOrigem(id);
        }

        /// <summary>
        /// Obtem os dados da origem (Área operacional) informada
        /// </summary>
        /// <returns></returns>
        public IAreaOperacional ObterOrigem(string codOrigem)
        {
            return _painelExpedicaoEstacaoMaeRepository.ObterPorCodigo(codOrigem);
        }

        /// <summary>
        /// Obtem os dados da origem (Área operacional) informada
        /// </summary>
        /// <returns></returns>
        public IList<Origem> ObterOrigensFaturamento(string uf)
        {
            if (!String.IsNullOrEmpty((uf)))
            {
                uf = (uf.ToUpper() == "TODAS") ? String.Empty : uf;
            }

            return _painelExpedicaoOrigemRepository.ObterOrigemFaturamentoPorUf(uf);
        }

        /// <summary>
        /// Obtem os dados da origem (Área operacional) OTIMIZADA
        /// </summary>
        /// <returns></returns>
        public IList<EstacaoFaturamentoDTO> ObterOrigensFaturamento2(string uf)
        {
            if (!String.IsNullOrEmpty((uf)))
            {
                uf = (uf.ToUpper() == "TODAS") ? String.Empty : uf;
            }

            return _painelExpedicaoRepository.ObterOrigemFaturamentoPorUf(uf);
        }

        /// <summary>
        /// Cadastra uma origem ao painel de expedição
        /// </summary>
        /// <param name="origem">Origem a ser cadastrada no painel de expedição</param>
        /// <returns></returns>
        public Origem InserirOrigem(Origem origem)
        {
            return _painelExpedicaoOrigemRepository.Inserir(origem);
        }


        /// <summary>
        /// Exclui a origem vinculada ao painel de expedição
        /// </summary>
        /// <param name="idOrigem">Id da origem</param>
        public void ExcluirOrigem(int idOrigem)
        {
            Origem origemExclusao = _painelExpedicaoOrigemRepository.ObterPorId(idOrigem);

            if (origemExclusao == null)
            {
                throw new Exception("Não foi localizada a origem para exclusão");
            }

            _painelExpedicaoOrigemRepository.Remover(origemExclusao);
        }

        #endregion

        #region Indicadores

        /// <summary>
        /// Obtem os indicadores cadastradas na parametrização do painel de expedição
        /// </summary>
        /// <returns></returns>
        public IList<Indicador> ObterIndicadores(int? colunaId)
        {
            IList<Indicador> resultados = (colunaId.HasValue) ? _painelExpedicaoIndicadoresRepository.ObterPorColuna(colunaId.Value) : _painelExpedicaoIndicadoresRepository.ObterTodos();
            return resultados;
        }

        public Indicador ObterIndicador(int coluna, int cor)
        {
            return _painelExpedicaoIndicadoresRepository.ObterPorColunaCor(coluna, cor);
        }

        public void SalvarIndicador(Indicador indicador)
        {
            if (!indicador.IdOperadorLogico.HasValue || indicador.IdOperadorLogico.Value == (int)OperadorLogicoEnum.Nenhum)
            {
                indicador.IdOperadorLogico = null;
                indicador.Valor2 = null;
                indicador.Operador2 = null;
            }

            var registroIndicador = this.ObterIndicador(indicador.IdColuna, indicador.IdCor);
            if (registroIndicador != null)
            {
                registroIndicador.Operador1 = indicador.Operador1;
                registroIndicador.Valor1 = indicador.Valor1;
                registroIndicador.IdOperadorLogico = indicador.IdOperadorLogico;
                registroIndicador.Operador2 = indicador.Operador2;
                registroIndicador.Valor2 = indicador.Valor2;
            }
            else
            {
                registroIndicador = indicador;
            }

            _painelExpedicaoIndicadoresRepository.InserirOuAtualizar(registroIndicador);
        }

        public void SalvarIndicadores(IList<Indicador> indicadores)
        {
            if (indicadores == null)
                throw new Exception("Configuração inválida.");

            foreach (var indicador in indicadores)
            {
                this.SalvarIndicador(indicador);
            }
        }

        #endregion

        #region Clientes

        public IList<BolarBoImpCfg> ObterConfiguracoesClientes(string query)
        {
            IList<BolarBoImpCfg> retorno = new List<BolarBoImpCfg>();
            IList<BolarBoImpCfg> resultados = _cfgRepository.ObterTodos();

            if (!String.IsNullOrEmpty(query))
            {
                resultados = resultados.Where(r => r.DescricaoEmpresa.ToUpper().Contains(query.ToUpper())).ToList();
            }

            foreach (var resultado in resultados)
            {
                if (resultado.IndAtivo == true && !retorno.Any(r => r.Id == resultado.Id))
                {
                    retorno.Add(resultado);
                }
            }

            return retorno.OrderBy(r => r.DescricaoEmpresa).ToList();
        }

        public IList<EmpresaCliente> ObterClientes()
        {
            IList<EmpresaCliente> retorno = new List<EmpresaCliente>();
            IList<EmpresaCliente> resultados = _empresaClienteRepository.ObterTodos();

            foreach (var resultado in resultados)
            {
                if (!retorno.Any(r => r.NomeFantasia == resultado.NomeFantasia))
                {
                    retorno.Add(resultado);
                }
            }

            return resultados.OrderBy(r => r.NomeFantasia).ToList();
        }

        public IList<EmpresaCliente> ObterListaNomesClientesDescResumida(string query)
        {
            IList<EmpresaCliente> retorno = new List<EmpresaCliente>();
            IList<EmpresaCliente> resultados = _empresaClienteRepository.ObterListaDescResumida(query);

            foreach (var resultado in resultados)
            {
                if (!retorno.Any(r => r.DescricaoResumida == resultado.DescricaoResumida))
                {
                    retorno.Add(resultado);
                }
            }

            return retorno.OrderBy(r => r.DescricaoResumida).ToList();
        }

        public IList<EmpresaCliente> ObterListaNomesExpedidoresDescResumida(string query)
        {
            IList<EmpresaCliente> retorno = new List<EmpresaCliente>();
            IList<EmpresaCliente> resultados = _empresaClienteRepository.ObterListaDescResumida(query);

            foreach (var resultado in resultados)
            {
                if (!retorno.Any(r => r.DescricaoResumida == resultado.DescricaoResumida))
                {
                    retorno.Add(resultado);
                }
            }

            return retorno.OrderBy(r => r.DescricaoResumida).ToList();
        }

        public IList<EmpresaCliente> ObterListaNomesRemetentesDescResumida(string query)
        {
            IList<EmpresaCliente> retorno = new List<EmpresaCliente>();
            IList<EmpresaCliente> resultados = _empresaClienteRepository.ObterListaDescResumida(query);

            foreach (var resultado in resultados)
            {
                if (!retorno.Any(r => r.DescricaoResumida == resultado.DescricaoResumida))
                {
                    retorno.Add(resultado);
                }
            }

            return retorno.OrderBy(r => r.DescricaoResumida).ToList();
        }

        public IList<EmpresaCliente> ObterListaNomesRecebedoresDescResumida(string query)
        {
            IList<EmpresaCliente> retorno = new List<EmpresaCliente>();
            IList<EmpresaCliente> resultados = _empresaClienteRepository.ObterListaDescResumida(query);

            foreach (var resultado in resultados)
            {
                if (!retorno.Any(r => r.DescricaoResumida == resultado.DescricaoResumida))
                {
                    retorno.Add(resultado);
                }
            }

            return retorno.OrderBy(r => r.DescricaoResumida).ToList();
        }

        #endregion

        #region Mercadorias

        public IList<Mercadoria> ObterMercadorias()
        {
            IList<Mercadoria> resultados = _mercadoriaRepository.ObterTodasMercadorias();
            return resultados;
        }

        #endregion

        #region Segmentos

        public IList<string> ObterSegmentos()
        {
            IList<string> resultados = _fluxoComercialRepository.ObterSegmentos();
            if (resultados != null)
            {
                if (resultados.Any())
                {
                    if (!resultados.Contains(""))
                        resultados.Add(" TODOS ");
                }
            }
            return resultados.OrderBy(r => r).ToList();
        }

        #endregion

        #region Malhas Ferroviárias

        public IList<string> ObterMalhas()
        {
            var resultados = _malhaRepository.ObterTodos();

            return resultados.Select(r => r.Descricao).OrderBy(r => r).ToList();
        }

        #endregion

        #region Bloqueios

        /// <summary>
        /// Obtem os bloqueios cadastrados
        /// </summary>
        /// <returns></returns>
        public IList<DespachoLocalBloqueioDto> ObterBloqueiosDespacho(DetalhesPaginacaoWeb detalhesPaginacao)
        {

            var retorno = _despachoLocalBloqueioRepository.ObterTodosBloqueios(detalhesPaginacao);

            return retorno;

        }

        /// <summary>
        /// Cadastra o bloqueio para a estação/segmento/cliente
        /// </summary>
        /// <param name="origem">Origem a ser cadastrada no painel de expedição</param>
        /// <returns></returns>
        public List<DespachoLocalBloqueioDto> InserirBloqueioDespacho(string origem, string segmento, string cnpj, Usuario usuario)
        {
            origem = (origem.ToUpper().Trim() ?? String.Empty);
            segmento = (segmento.ToUpper().Trim() ?? String.Empty);
            cnpj = (cnpj.ToUpper().Trim() ?? String.Empty);
            string cnpjRaiz = (!String.IsNullOrEmpty(cnpj) ? cnpj.Substring(0, 8) : String.Empty);

            if (String.IsNullOrEmpty(origem))
            {
                throw new Exception("Nenhuma estação foi informada!");
            }

            var areaOperacional = this.ObterOrigem(origem);

            if (areaOperacional == null)
            {
                throw new Exception("A estação informada não é válida!");
            }
            else
            {
                if (areaOperacional.Codigo == null)
                    throw new Exception("A estação informada não é válida!");

                if (areaOperacional.EstacaoMae != null)
                    throw new Exception("A estação informada deve ser uma estação Mãe!");
            }

            if (String.IsNullOrEmpty(segmento))
            {
                throw new Exception("Nenhum segmento foi informado!");
            }

            IList<string> segmentosInserir = new List<string>();
            // validar segmento
            // Se Todos, então obtem todos os segmentos e insere um a um
            if (segmento == "TODOS")
            {
                /*if (String.IsNullOrEmpty(cnpj))
                {
                    throw new Exception("Cnpj deve ser informado para segmento Todos!");
                }*/

                segmentosInserir = this.ObterSegmentos();
            }
            else
            {
                segmentosInserir.Add(segmento);
            }
            // validar cnpj

            string empresaDescricao = "";
            if (!String.IsNullOrEmpty(cnpj))
            {
                // verificar se o CNPJ existe na tabela de Empresas - EmpresaCliente - Tipo C
                var empresa = _empresaClienteRepository.ObterPorCnpj(cnpj);
                if (empresa == null)
                {
                    throw new Exception("O Cnpj informado não foi encontrado na base de clientes!");
                }
                empresaDescricao = empresa.DescricaoResumida;
            }

            IList<DespachoLocalBloqueio> bloqueiosInseridos = new List<DespachoLocalBloqueio>();
            foreach (var seg in segmentosInserir)
            {
                if ((seg.ToUpper().Trim() != "TODOS") && (!String.IsNullOrEmpty(seg)))
                {
                    DespachoLocalBloqueio bloqueio = new DespachoLocalBloqueio();
                    bloqueio.Estacao = areaOperacional;
                    bloqueio.Segmento = seg;
                    bloqueio.CnpjRaiz = cnpjRaiz;
                    bloqueio.Usuario = usuario;
                    bloqueio.DataCadastro = DateTime.Now;

                    var existeBloqueio = _painelExpedicaoRepository.ObterDespachoLocalBloqueio(areaOperacional.Id,
                                                                                        seg, cnpjRaiz, false);
                    if (existeBloqueio == null)
                    {
                        _despachoLocalBloqueioRepository.Inserir(bloqueio);
                    }

                    bloqueiosInseridos.Add(bloqueio);
                }

            }

            return this.ConverteDespachoBloqueioDto(bloqueiosInseridos, empresaDescricao);
        }


        /// <summary>
        /// Exclui a origem vinculada ao painel de expedição
        /// </summary>
        /// <param name="idOrigem">Id da origem</param>
        public void ExcluirBloqueioDespacho(int idBloqueio)
        {
            DespachoLocalBloqueio bloqueioExclusao = _despachoLocalBloqueioRepository.ObterPorId(idBloqueio);

            if (bloqueioExclusao == null)
            {
                throw new Exception("Não foi localizado o bloqueio para exclusão");
            }

            _despachoLocalBloqueioRepository.Remover(bloqueioExclusao);
        }

        private List<DespachoLocalBloqueioDto> ConverteDespachoBloqueioDto(IList<DespachoLocalBloqueio> bloqueios, string cliente)
        {
            List<DespachoLocalBloqueioDto> bloqueiosDto = null;
            if (bloqueios != null)
            {
                bloqueiosDto = new List<DespachoLocalBloqueioDto>();
                foreach (var blo in bloqueios)
                {
                    DespachoLocalBloqueioDto dto = new DespachoLocalBloqueioDto();
                    dto.Id = blo.Id;
                    dto.IdEstacao = Convert.ToDecimal(blo.Estacao.Id);
                    dto.CodigoEstacao = blo.Estacao.Codigo;
                    dto.DescricaoEstacao = blo.Estacao.Descricao;
                    dto.Segmento = blo.Segmento;
                    dto.CnpjRaiz = blo.CnpjRaiz;
                    dto.Cliente = cliente;
                    bloqueiosDto.Add(dto);
                }
            }
            return bloqueiosDto;
        }

        public bool VerificarDespachoLocalBloqueio(int? idEstacaoOrigem, string segmento, string cnpjRaiz)
        {
            DespachoLocalBloqueioDto despachoLocalBloqueio = null;
            segmento = (String.IsNullOrEmpty(segmento) ? "" : segmento);
            segmento = segmento.Trim().ToUpper();
            segmento = (segmento == "TODOS" ? "" : segmento);
            cnpjRaiz = (String.IsNullOrEmpty(cnpjRaiz) ? "" : cnpjRaiz);

            // Para segmento BRADO, não verificar o CNPJ raiz
            if (segmento == "BRADO")
                cnpjRaiz = "";

            despachoLocalBloqueio = _painelExpedicaoRepository.ObterDespachoLocalBloqueio(idEstacaoOrigem,
                                                                                              segmento,
                                                                                              cnpjRaiz, false);

            // Caso não encontre o bloqueio, verifica se possui bloqueio para todos os clientes
            if ((despachoLocalBloqueio == null) && (segmento != "BRADO") && (!String.IsNullOrEmpty(segmento)) && (!String.IsNullOrEmpty(cnpjRaiz)))
            {
                despachoLocalBloqueio = _painelExpedicaoRepository.ObterDespachoLocalBloqueio(idEstacaoOrigem,
                                                                                              segmento,
                                                                                              "", true);
            }

            if (despachoLocalBloqueio != null)
                return true;

            return false;
        }

        #endregion

        #region Tipo de Integração de Clientes

        /// <summary>
        /// Obtem os bloqueios cadastrados
        /// </summary>
        /// <returns></returns>
        public IList<ClienteTipoIntegracaoDto> ObterClientesTipoIntegracao(DetalhesPaginacaoWeb detalhesPaginacao)
        {
            var retorno = _clienteTipoIntegracaoRepository.ObterTodos();

            return this.ConverteClienteTipoIntegracaoDto(retorno);
        }

        private List<ClienteTipoIntegracaoDto>
            ConverteClienteTipoIntegracaoDto(IList<ClienteTipoIntegracao> tiposIntegracao)
        {
            List<ClienteTipoIntegracaoDto> tiposIntegracaoDto = null;
            if (tiposIntegracao != null)
            {
                tiposIntegracaoDto = new List<ClienteTipoIntegracaoDto>();
                foreach (var t in tiposIntegracao)
                {
                    ClienteTipoIntegracaoDto dto = new ClienteTipoIntegracaoDto();
                    dto.Id = t.Id;
                    dto.EstacaoCodigo = t.Estacao.Codigo;
                    dto.Estacao = t.Estacao.Descricao;
                    dto.Cliente = t.Empresa.DescricaoResumida;
                    dto.TipoIntegracaoId = (TipoIntegracaoEnum)t.IdTipo;
                    tiposIntegracaoDto.Add(dto);
                }
            }
            return tiposIntegracaoDto;
        }

        /// <summary>
        /// Cadastra o tipo de migração do cliente
        /// </summary>
        /// <param name="origem">Código Estação</param>
        /// <param name="clienteId">Id do Cliente</param>
        /// <param name="tipoIntegracaoId">Id do Tipo de Integração</param>
        /// <returns></returns>
        public ClienteTipoIntegracaoDto InserirClienteTipoIntegracao(string origem,
                                                              int clienteId,
                                                              int tipoIntegracao)
        {

            var empresa = _empresaClienteRepository.ObterPorId(clienteId);
            if (empresa == null)
            {
                throw new Exception("O cliente informado não foi encontrado na base de clientes!");
            }


            if (String.IsNullOrEmpty(origem))
            {
                throw new Exception("Nenhuma estação foi informada!");
            }

            var areaOperacional = this.ObterOrigem(origem);

            if (areaOperacional == null)
            {
                throw new Exception("A estação informada não é válida!");
            }
            else
            {
                if (areaOperacional.Codigo == null)
                    throw new Exception("A estação informada não é válida!");
            }

            ClienteTipoIntegracaoDto clienteDto = new ClienteTipoIntegracaoDto();
            ClienteTipoIntegracao clienteTipoIntegracao = new ClienteTipoIntegracao();
            clienteTipoIntegracao.Empresa = empresa;
            clienteTipoIntegracao.Estacao = areaOperacional;
            clienteTipoIntegracao.IdTipo = tipoIntegracao;

            bool existeClienteTipoIntegracao = false;
            if (!existeClienteTipoIntegracao)
            {
                var novoClienteTipoIntegracao = _clienteTipoIntegracaoRepository.Inserir(clienteTipoIntegracao);
                IList<ClienteTipoIntegracao> listaCliente = new List<ClienteTipoIntegracao>();
                var listaClienteDto = this.ConverteClienteTipoIntegracaoDto(listaCliente);
                if (listaClienteDto != null)
                {
                    if (listaClienteDto.Count() > 0)
                        clienteDto = listaClienteDto.SingleOrDefault();
                }
            }

            return clienteDto;
        }


        /// <summary>
        /// Exclui o tipo de integração
        /// </summary>
        /// <param name="idOrigem">Id do Cliente Tipo Integração</param>
        public void ExcluirClienteTipoIntegracao(int idClienteTipo)
        {
            ClienteTipoIntegracao clienteTipoExclusao = _clienteTipoIntegracaoRepository.ObterPorId(idClienteTipo);

            if (clienteTipoExclusao == null)
            {
                throw new Exception("Não foi localizado o tipo de integração para exclusão");
            }

            _clienteTipoIntegracaoRepository.Remover(clienteTipoExclusao);
        }

        #endregion

        #region Malhas Ferroviárias

        public List<string> ObterUfsDaMalha(string malha)
        {
            List<string> ufs = new List<string>();

            malha = (String.IsNullOrEmpty(malha) ? "" : malha.ToUpper());
            malha = (malha == "TODAS" ? "" : malha);

            if (String.IsNullOrEmpty(malha))
            {
                ufs.Add("MG");
                ufs.Add("MT");
                ufs.Add("MS");
                ufs.Add("PR");
                ufs.Add("RJ");
                ufs.Add("RS");
                ufs.Add("SC");
                ufs.Add("SP");
            }
            else if (malha == "SUL")
            {
                ufs.Add("PR");
                ufs.Add("RS");
                ufs.Add("SC");
            }
            else if (malha == "NORTE")
            {
                ufs.Add("MG");
                ufs.Add("MT");
                ufs.Add("MS");
                ufs.Add("RJ");
                ufs.Add("SP");
            }

            return ufs;
        }

        #endregion

        #region Vagões

        /// <summary>
        /// Verifica se o vagão é válido
        /// </summary>
        /// <param name="codigo">Código do vagão</param>
        /// <returns>Retorna false for um vagão válido - true quando encontrar algum erro</returns>
        public KeyValuePair<bool, string> VerificarVagao(string codigo)
        {
            if (codigo.Length != 7)
            {
                return new KeyValuePair<bool, string>(true, "Código do Vagão deve conter 7 dígitos.");
            }

            var vagaoRegistro = _vagaoRepository.ObterPorCodigo(codigo);
            return (vagaoRegistro == null) ?
                new KeyValuePair<bool, string>(true, "Vagão não encontrado na base de dados.") :
                new KeyValuePair<bool, string>(false, "Vagão válido.");
        }

        /// <summary>
        /// Verifica vagões
        /// </summary>
        /// <param name="vagoes">Lista de vagões</param>
        /// <returns>Retorna false quando o vagão for valido</returns>
        public Dictionary<string, KeyValuePair<bool, string>> VerificarVagoes(List<string> vagoes)
        {
            var listaRetorno = new Dictionary<string, KeyValuePair<bool, string>>();

            if (vagoes != null)
            {
                var vagoesDistintos = vagoes.Distinct().ToList();
                foreach (var vagao in vagoesDistintos)
                {
                    var statusRetorno = VerificarVagao(vagao);
                    listaRetorno.Add(vagao, statusRetorno);
                }
            }

            return listaRetorno;
        }

        #endregion

        #region OperacaoMalha

        /// <summary>
        /// Obter todas as Operacoes
        /// </summary>
        /// <returns></returns>
        public IList<PeOperacao> ObterOperacoes()
        {
            return BL_PeOperacao.Select();
        }

        /// <summary>
        /// Obtem os dados da Operacao  por ID
        /// </summary>
        /// <returns></returns>
        public PeOperacao ObterOperacaoMalhaPorId(int id)
        {
            return BL_PeOperacao.SelectByPk(new PeOperacao { Id = id });
        }

        /// <summary>
        /// Cadastra uma origem ao painel de expedição
        /// </summary>
        /// <param name="origem">Origem a ser cadastrada no painel de expedição</param>
        /// <returns></returns>
        public void InserirNovaOperacao(PeOperacao model)
        {
            BL_PeOperacao.Insert(model);
        }
        #endregion

        #region Regiao

        /// <summary>
        /// Obter todas as Regioes
        /// </summary>
        /// <returns></returns>
        public IList<PeRegiao> ObterRegioes(string where = "")
        {
            if(!String.IsNullOrEmpty(where))
                return BL_PeRegiao.Select(where);
            else
                return BL_PeRegiao.Select();
        }

        /// <summary>
        /// Cadastra uma origem ao painel de expedição
        /// </summary>
        /// <param name="origem">Origem a ser cadastrada no painel de expedição</param>
        /// <returns></returns>
        public Tuple<bool, string> InserirRegiao(PeRegiao model)
        {
            var modelTemp = new PeRegiao { Nome = model.Nome, OperacaoId = model.OperacaoId };

            modelTemp = BL_PeRegiao.Select(modelTemp).FirstOrDefault();

            if (modelTemp != null && modelTemp.Nome == model.Nome && modelTemp.OperacaoId == model.OperacaoId)
            {
                return new Tuple<bool, string>(false, $"Ja existe uma região com o mesmo nome para a mesma malha de operação [{modelTemp.Nome}]");
            }
            else
            {
                BL_PeRegiao.Insert(model);
                return new Tuple<bool, string>(true, "Região cadastrada com sucesso!");
            }
        }

        /// <summary>
        /// Exclui a regiao vinculada ao painel de expedição
        /// </summary>
        /// <param name="idOrigem">Id da origem</param>
        public void ExcluirRegiao(int id)
        {
            BL_PeRegiao.DeleteByPk(new PeRegiao { Id = id });
        }

        public Tuple<bool, string> AtualizarRegiao(PeRegiao model)
        {
            var modelTemp = new PeRegiao { Nome = model.Nome, OperacaoId = model.OperacaoId };

            modelTemp = BL_PeRegiao.Select(modelTemp).FirstOrDefault();

            if (modelTemp!= null && modelTemp.Nome == model.Nome && modelTemp.OperacaoId == model.OperacaoId)
            {
                return new Tuple<bool, string>(false, $"Ja existe uma região com o mesmo nome para a mesma malha de operação [{modelTemp.Nome}]");
            }
            else
            {
                modelTemp = BL_PeRegiao.SelectByPk(model.Id);

                modelTemp.Nome = model.Nome;
                modelTemp.OperacaoId = model.OperacaoId;

                modelTemp.DataAlteracao = model.DataAlteracao;
                modelTemp.UsuarioAlteracao = model.UsuarioAlteracao;

                BL_PeRegiao.Update(modelTemp);

                return new Tuple<bool, string>(true, "Região alterada com sucesso!");
            }
        }


        #endregion

        #region RegraEnvioEmail

        /// <summary>
        /// Obter todas as Regioes
        /// </summary>
        /// <returns></returns>
        public IList<PeRegraEmails> ObterRegraEmails()
        {
            return BL_PeRegraEmails.Select();
        }

        /// <summary>
        /// Cadastra uma origem ao painel de expedição
        /// </summary>
        /// <param name="origem">Origem a ser cadastrada no painel de expedição</param>
        /// <returns></returns>
        public Tuple<bool, string> InserirRegraEmails(PeRegraEmails model)
        {
            var modelTemp = new PeRegraEmails { OperacaoId = model.OperacaoId, RegiaoId = model.RegiaoId };

            modelTemp = BL_PeRegraEmails.Select(modelTemp).FirstOrDefault();

            if (modelTemp != null && modelTemp.OperacaoId == model.OperacaoId && modelTemp.RegiaoId == model.RegiaoId)
            {
                return new Tuple<bool, string>(false, $"Ja existe uma regra para a mesma Operação/Região");
            }
            else
            {
                BL_PeRegraEmails.Insert(model);
                return new Tuple<bool, string>(true, "Regra cadastrada com sucesso!");
            }
        }

        /// <summary>
        /// Exclui a regra envio emails vinculada ao painel de expedição
        /// </summary>
        /// <param name="idOrigem">Id da origem</param>
        public void ExcluirRegraEnvioEmail(int idRegra)
        {
            BL_PeRegraEmails.DeleteByPk(new PeRegraEmails { Id = idRegra });
        }

        /// <summary>
        /// Busca o registro original, atualiza as informações e salva
        /// </summary>
        /// <param name="model"></param>
        public Tuple<bool, string> AtualizarRegraEmails(PeRegraEmails model)
        {
            var modelTemp = new PeRegraEmails {OperacaoId = model.OperacaoId, RegiaoId = model.RegiaoId };

            modelTemp = BL_PeRegraEmails.Select(modelTemp).FirstOrDefault(i => i.Id != model.Id);

            if (modelTemp != null && modelTemp.OperacaoId == model.OperacaoId && modelTemp.RegiaoId == model.RegiaoId)
            {
                return new Tuple<bool, string>(false, $"Ja existe uma Regra para a mesma Operação/Região");
            }
            else
            {
                var modelSalvar = BL_PeRegraEmails.SelectByPk(model.Id);

                modelSalvar.OperacaoId = model.OperacaoId;
                modelSalvar.RegiaoId = model.RegiaoId;
                modelSalvar.TituloEmail = model.TituloEmail;
                modelSalvar.CorpoEmail = model.CorpoEmail;
                modelSalvar.Destinatarios = model.Destinatarios;

                modelSalvar.UsuarioAlteracao = model.UsuarioAlteracao;
                modelSalvar.DataAlteracao = model.DataAlteracao;

                BL_PeRegraEmails.Update(modelSalvar);

                return new Tuple<bool, string>(true, "Regra alterada com sucesso!");
            }
        }

        #endregion

        #region PedraRealizado

        public void InserirPedraRealizado(PePedraRealizado model)
        {
            BL_PePedraRealizado.Insert(model);
        }

        public void AtualizarPedraEditado(PePedraRealizado model)
        {
            var modelSalvar = BL_PePedraRealizado.SelectByPk(model.Id);

            modelSalvar.PedraEditado = model.PedraEditado;
            modelSalvar.DataAlteracao = model.DataAlteracao;
            modelSalvar.UsuarioAlteracao = model.UsuarioAlteracao;

            BL_PePedraRealizado.Update(modelSalvar);
        }

        public void AtualizarStatusEditado(PePedraRealizado model)
        {
            var modelSalvar = BL_PePedraRealizado.SelectByPk(model.Id);

            modelSalvar.StatusEditado = model.StatusEditado;
            modelSalvar.DataAlteracao = model.DataAlteracao;
            modelSalvar.UsuarioAlteracao = model.UsuarioAlteracao;

            BL_PePedraRealizado.Update(modelSalvar);
        }


        public void ExcluirPedraRealizado(int idRecord)
        {
            BL_PePedraRealizado.DeleteByPk(new PePedraRealizado { Id = idRecord });
        }

        #endregion

    }
}