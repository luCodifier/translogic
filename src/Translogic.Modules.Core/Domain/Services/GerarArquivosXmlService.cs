﻿namespace Translogic.Modules.Core.Domain.Services
{
    using System;
    using System.IO;
    using System.Text;
    using Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;

    public class GerarArquivosXmlService
    {
        private readonly ICteArquivoRepository _cteArquivoRepository;
        private readonly ICteRepository _cteRepository;

        public GerarArquivosXmlService(ICteArquivoRepository cteArquivoRepository, ICteRepository cteRepository)
        {
            _cteArquivoRepository = cteArquivoRepository;
            _cteRepository = cteRepository;
        }

        public void GerarArquivos()
        {
            string[] lines = File.ReadAllLines(@"D:\Temp\Gerar Arquivos Xml\1listaGeracao.txt");
            var file = new StreamWriter(@"D:\Temp\Gerar Arquivos Xml\logGeracao.txt");
            foreach (string line in lines)
            {
                try
                {
                    var cte = _cteRepository.ObterPorChave(line);
                    var arquivo = _cteArquivoRepository.ObterArqPorId(cte.Id);
                    File.WriteAllText(@"D:\Temp\Gerar Arquivos Xml\Arquivos\" + line + ".xml",
                                      arquivo, Encoding.UTF8);
                    file.WriteLine(line + ";" + "OK");
                }
                catch (Exception e)
                {
                    file.WriteLine(line + ";" + "ERRO");
                }
                finally
                {
                    
                }
            }
            file.Close();
        }
    }
}