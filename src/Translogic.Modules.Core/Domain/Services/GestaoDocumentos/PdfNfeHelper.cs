﻿namespace Translogic.Modules.Core.Domain.Services.GestaoDocumentos
{
    using System.Drawing;
    using iTextSharp.text;
    using iTextSharp.text.pdf;
    using Font = iTextSharp.text.Font;
    using Rectangle = iTextSharp.text.Rectangle;

    /// <summary>
    /// Classe para auxiliar na geração de pdf pelo itextSharp
    /// </summary>
    public static class PdfNfeHelper
    {
        #region "Bordas pré-definidas"

        /// <summary>
        /// Todas as bordas
        /// </summary>
        public static readonly Bordas None = new Bordas()
        {
            Left =
              new BorderStyle()
              {
                  Width = 0,
                  Type = BorderType.Solid
              },
            Top = new BorderStyle()
            {
                Width = 0,
                Type = BorderType.Solid
            },
            Right = new BorderStyle()
            {
                Width = 0,
                Type = BorderType.Solid
            },
            Bottom = new BorderStyle()
            {
                Width = 0,
                Type = BorderType.Solid
            }
        };

        /// <summary>
        /// Todas as bordas
        /// </summary>
        public static readonly Bordas All = new Bordas()
        {
            Left =
              new BorderStyle()
              {
                  Width = 1,
                  Type = BorderType.Rounded
              },
            Top = new BorderStyle()
            {
                Width = 1,
                Type = BorderType.Rounded
            },
            Right = new BorderStyle()
            {
                Width = 1,
                Type = BorderType.Rounded
            },
            Bottom = new BorderStyle()
            {
                Width = 1,
                Type = BorderType.Rounded
            }
        };

        /// <summary>
        /// Todas as bordas
        /// </summary>
        public static readonly Bordas AllSolid = new Bordas()
        {
            Left =
              new BorderStyle()
              {
                  Width = 1,
                  Type = BorderType.Solid
              },
            Top = new BorderStyle()
            {
                Width = 1,
                Type = BorderType.Solid
            },
            Right = new BorderStyle()
            {
                Width = 1,
                Type = BorderType.Solid
            },
            Bottom = new BorderStyle()
            {
                Width = 1,
                Type = BorderType.Solid
            }
        };

        /// <summary>
        /// bordas as top left e right
        /// </summary>
        public static readonly Bordas TopLeftRight = new Bordas()
        {
            Left =
              new BorderStyle()
              {
                  Width = 1,
                  Type = BorderType.Rounded
              },
            Top = new BorderStyle()
            {
                Width = 1,
                Type = BorderType.Rounded
            },
            Right = new BorderStyle()
            {
                Width = 1,
                Type = BorderType.Rounded
            },
            Bottom = new BorderStyle()
            {
                Width = 0,
                Type = BorderType.Rounded
            }
        };

        /// <summary>
        /// bordas as top left e right
        /// </summary>
        public static readonly Bordas SolidTopLeftRight = new Bordas()
        {
            Left =
              new BorderStyle()
              {
                  Width = 1,
                  Type = BorderType.Solid
              },
            Top = new BorderStyle()
            {
                Width = 1,
                Type = BorderType.Solid
            },
            Right = new BorderStyle()
            {
                Width = 1,
                Type = BorderType.Solid
            },
            Bottom = new BorderStyle()
            {
                Width = 0,
                Type = BorderType.Solid
            }
        };

        /// <summary>
        /// Borda left e right
        /// </summary>
        public static readonly Bordas LeftRight = new Bordas()
        {
            Left =
              new BorderStyle()
              {
                  Width = 1,
                  Type = BorderType.Rounded
              },
            Top = new BorderStyle()
            {
                Width = 0,
                Type = BorderType.Rounded
            },
            Right = new BorderStyle()
            {
                Width = 1,
                Type = BorderType.Rounded
            },
            Bottom = new BorderStyle()
            {
                Width = 0,
                Type = BorderType.Rounded
            }
        };

        /// <summary>
        /// Borda bottom left e right
        /// </summary>
        public static readonly Bordas BottomLeftRight = new Bordas()
        {
            Left =
              new BorderStyle()
              {
                  Width = 1,
                  Type = BorderType.Rounded
              },
            Top = new BorderStyle()
            {
                Width = 0,
                Type = BorderType.Rounded
            },
            Right = new BorderStyle()
            {
                Width = 1,
                Type = BorderType.Rounded
            },
            Bottom = new BorderStyle()
            {
                Width = 1,
                Type = BorderType.Rounded
            }
        };

        /// <summary>
        /// Borda bottom left e right
        /// </summary>
        public static readonly Bordas Left = new Bordas()
        {
            Left =
                new BorderStyle()
                {
                    Width = 1,
                    Type = BorderType.Rounded
                },
            Top = new BorderStyle()
            {
                Width = 0,
                Type = BorderType.Rounded
            },
            Right = new BorderStyle()
            {
                Width = 0,
                Type = BorderType.Rounded
            },
            Bottom = new BorderStyle()
            {
                Width = 0,
                Type = BorderType.Rounded
            }
        };

        /// <summary>
        /// Borda bottom left e right
        /// </summary>
        public static readonly Bordas Top = new Bordas()
        {
            Left =
              new BorderStyle()
              {
                  Width = 0,
                  Type = BorderType.Rounded
              },
            Top = new BorderStyle()
            {
                Width = 1,
                Type = BorderType.Rounded
            },
            Right = new BorderStyle()
            {
                Width = 0,
                Type = BorderType.Rounded
            },
            Bottom = new BorderStyle()
            {
                Width = 0,
                Type = BorderType.Rounded
            }
        };

        /// <summary>
        /// Borda bottom left e right
        /// </summary>
        public static readonly Bordas Right = new Bordas()
        {
            Left =
              new BorderStyle()
              {
                  Width = 0,
                  Type = BorderType.Rounded
              },
            Top = new BorderStyle()
            {
                Width = 0,
                Type = BorderType.Rounded
            },
            Right = new BorderStyle()
            {
                Width = 1,
                Type = BorderType.Rounded
            },
            Bottom = new BorderStyle()
            {
                Width = 0,
                Type = BorderType.Rounded
            }
        };

        /// <summary>
        /// Borda bottom left e right
        /// </summary>
        public static readonly Bordas Bottom = new Bordas()
        {
            Left =
              new BorderStyle()
              {
                  Width = 0,
                  Type = BorderType.Rounded
              },
            Top = new BorderStyle()
            {
                Width = 0,
                Type = BorderType.Rounded
            },
            Right = new BorderStyle()
            {
                Width = 0,
                Type = BorderType.Rounded
            },
            Bottom = new BorderStyle()
            {
                Width = 1,
                Type = BorderType.Rounded
            }
        };

        /// <summary>
        /// Borda bottom left e right
        /// </summary>
        public static readonly Bordas SolidWithTopDash = new Bordas()
        {
            Left =
              new BorderStyle()
              {
                  Width = 1,
                  Type = BorderType.Rounded
              },
            Top = new BorderStyle()
            {
                Width = 1,
                Type = BorderType.Dotted
            },
            Right = new BorderStyle()
            {
                Width = 1,
                Type = BorderType.Solid
            },
            Bottom = new BorderStyle()
            {
                Width = 1,
                Type = BorderType.Rounded
            }
        };

        #endregion

        #region "Fontes pré-definidas"

        private static readonly Font _font = new Font(Font.FontFamily.TIMES_ROMAN, 7F);
        private static readonly Font _subTitulo = new Font(Font.FontFamily.TIMES_ROMAN, 10F, Font.BOLD);

        #endregion

        #region "Enumeradores"

        /// <summary>
        /// Enumerador de tipo da borda
        /// </summary>
        public enum BorderType
        {
            /// <summary>
            /// borda pontilhada
            /// </summary>
            Dotted = 1,

            /// <summary>
            /// borda Solida
            /// </summary>
            Solid = 2,

            /// <summary>
            ///  borda arredondada
            /// </summary>
            Rounded = 3,
        }

        #endregion

        #region "Métodos publicos"

        /// <summary>
        /// Cria o objetos de bordas
        /// </summary>
        /// <param name="width">Tamanho das bordas(left-top-right-bottom)</param>
        /// <param name="tipos">Tipo das bordas(left-top-right-bottom)</param>
        /// <returns>Retorna o objeto borda</returns>
        public static Bordas CriarBordas(int[] width, BorderType[] tipos)
        {
            return new Bordas()
               {
                   Left =
                     new BorderStyle()
                     {
                         Width = width[0],
                         Type = tipos[0]
                     },
                   Top = new BorderStyle()
                   {
                       Width = width[1],
                       Type = tipos[1]
                   },
                   Right = new BorderStyle()
                   {
                       Width = width[2],
                       Type = tipos[2]
                   },
                   Bottom = new BorderStyle()
                   {
                       Width = width[3],
                       Type = tipos[3]
                   }
               };
        }

        /// <summary>
        /// Cria um coluna de pdf
        /// </summary>
        /// <param name="font">Font da coluna</param>
        /// <param name="text">texto da coluna</param>
        /// <returns>coluna do pdf</returns>
        public static PdfPCell CreateCell(Font font, string text)
        {
            return CreateCell(font, text, Element.ALIGN_LEFT, None, Element.ALIGN_MIDDLE);
        }

        /// <summary>
        /// Cria um coluna de pdf
        /// </summary>
        /// <param name="font">Font da coluna</param>
        /// <param name="text">texto da coluna</param>
        /// <param name="align">Alinhamento da coluna</param>
        /// <returns>coluna do pdf</returns>
        public static PdfPCell CreateCell(Font font, string text, int align)
        {
            return CreateCell(font, text, align, None, Element.ALIGN_MIDDLE);
        }

        /// <summary>
        /// Cria um coluna de pdf
        /// </summary>
        /// <param name="font">Font da coluna</param>
        /// <param name="text">texto da coluna</param>
        /// <param name="align">Alinhamento da coluna</param>
        /// <param name="verticalAlign">Alinhamento da coluna vertical</param>
        /// <returns>coluna do pdf</returns>
        public static PdfPCell CreateCell(Font font, string text, int align, int verticalAlign)
        {
            return CreateCell(font, text, align, None, verticalAlign);
        }

        /// <summary>
        /// Cria um coluna de pdf
        /// </summary>
        /// <param name="font">Font da coluna</param>
        /// <param name="text">texto da coluna</param>
        /// <param name="border">configurações de borda</param>
        /// <returns>coluna do pdf</returns>
        public static PdfPCell CreateCell(Font font, string text, Bordas border)
        {
            return CreateCell(font, text, Element.ALIGN_LEFT, border, Element.ALIGN_MIDDLE);
        }

        /// <summary>
        /// Cria um coluna de pdf
        /// </summary>
        /// <param name="font">Font da coluna</param>
        /// <param name="text">texto da coluna</param>
        /// <returns>coluna do pdf</returns>
        public static PdfPCell CreateHeaderCell(Font font, string text)
        {
            var cell = CreateCell(font, text, Element.ALIGN_LEFT, TopLeftRight, Element.ALIGN_MIDDLE);
            cell.Padding = 0.5f;
            return cell;
        }

        /// <summary>
        /// Cria um coluna de pdf
        /// </summary>
        /// <param name="text">texto da coluna</param>
        /// <param name="textAlign"> alinhamento do texto da coluna</param>
        /// <returns>coluna do pdf</returns>
        public static PdfPCell CreateHeaderCell(string text, int textAlign)
        {
            var cell = CreateCell(_font, text, textAlign, TopLeftRight, Element.ALIGN_MIDDLE);
            cell.Padding = 0.5f;
            return cell;
        }

        /// <summary>
        /// Cria um coluna de pdf
        /// </summary>
        /// <param name="text">texto da coluna</param>
        /// <returns>coluna do pdf</returns>
        public static PdfPCell CreateEndFieldCell(string text)
        {
            return CreateEndFieldCell(_subTitulo, text, Element.ALIGN_CENTER);
        }

        /// <summary>
        /// Cria um coluna de pdf
        /// </summary>
        /// <param name="text">texto da coluna</param>
        /// <param name="align">alinhamento do texto</param>
        /// <returns>coluna do pdf</returns>
        public static PdfPCell CreateEndFieldCell(string text, int align)
        {
            return CreateEndFieldCell(_subTitulo, text, align);
        }

        /// <summary>
        /// Cria um coluna de pdf
        /// </summary>
        /// <param name="font">Font da coluna</param>
        /// <param name="text">texto da coluna</param>
        /// <param name="align">alinhamento do texto</param>
        /// <returns>coluna do pdf</returns>
        public static PdfPCell CreateEndFieldCell(Font font, string text, int align)
        {
            var cell = CreateCell(font, text, align, BottomLeftRight, Element.ALIGN_MIDDLE);
            return cell;
        }

        /// <summary>
        /// Cria um coluna de pdf
        /// </summary>
        /// <param name="font">Font da coluna</param>
        /// <param name="text">texto da coluna</param>
        /// <param name="align">alinhamento do texto</param>
        /// <param name="verticalAlign">alinhamento vertical do texto</param>
        /// <returns>coluna do pdf</returns>
        public static PdfPCell CreateEndFieldCell(Font font, string text, int align, int verticalAlign)
        {
            var cell = CreateCell(font, text, align, BottomLeftRight, verticalAlign);
            return cell;
        }

        /// <summary>
        /// Cria um coluna de pdf
        /// </summary>
        /// <param name="font">Font da coluna</param>
        /// <param name="text">texto da coluna</param>
        /// <returns>coluna do pdf</returns>
        public static PdfPCell CreateEndFieldCell(Font font, string text)
        {
            return CreateEndFieldCell(_font, text, Element.ALIGN_CENTER);
        }

        /// <summary>
        /// Cria um coluna de pdf
        /// </summary>
        /// <param name="text">texto da coluna</param>
        /// <returns>coluna do pdf</returns>
        public static PdfPCell CreateHeaderCell(string text)
        {
            return CreateHeaderCell(_font, text);
        }

        /// <summary>
        /// Cria um coluna de pdf
        /// </summary>
        /// <param name="font">Font da coluna</param>
        /// <param name="text">texto da coluna</param>
        /// <param name="align"> alinhamento da coluna</param>
        /// <param name="border">configurações de borda</param>
        /// <returns>coluna do pdf</returns>
        public static PdfPCell CreateCell(Font font, string text, int align, Bordas border)
        {
            return CreateCell(font, text, align, border, Element.ALIGN_MIDDLE);
        }

        /// <summary>
        /// Cria um coluna de pdf
        /// </summary>
        /// <param name="text">texto da coluna</param>
        /// <param name="border">configurações de borda</param>
        /// <returns>coluna do pdf</returns>
        public static PdfPCell CreateCell(string text, Bordas border)
        {
            return CreateCell(_font, text, Element.ALIGN_LEFT, border, Element.ALIGN_MIDDLE);
        }

        /// <summary>
        /// Cria um coluna de pdf
        /// </summary>
        /// <param name="font">Font da coluna</param>
        /// <param name="text">texto da coluna</param>
        /// <param name="align"> alinhamento da coluna</param>
        /// <param name="border">configurações de borda</param>
        /// <param name="verticalAlign">alinhamento vertical</param>
        /// <returns>coluna do pdf</returns>
        public static PdfPCell CreateCell(Font font, string text, int align, Bordas border, int verticalAlign)
        {
            var cell = new PdfPCell(new Phrase(text, font));
            cell.BorderColor = new BaseColor(Color.Black);
            cell.HorizontalAlignment = align;
            cell.VerticalAlignment = verticalAlign;
            cell.Border = Rectangle.NO_BORDER;

            if (border.HasValue())
            {
                var round = new RoundRectangle(border);
                cell.CellEvent = round;
            }

            return cell;
        }

        #endregion

        #region "Struct de bordas"

        /// <summary>
        /// Objeto com configurações das bordas
        /// </summary>
        public struct Bordas
        {
            /// <summary>
            /// Borda esquerda
            /// </summary>
            public BorderStyle Left;

            /// <summary>
            /// Borda superior
            /// </summary>
            public BorderStyle Top;

            /// <summary>
            /// Borda direita
            /// </summary>
            public BorderStyle Right;

            /// <summary>
            /// Borda inferior
            /// </summary>
            public BorderStyle Bottom;

            /// <summary>
            /// Retorna possui alguma bora preenchida
            /// </summary>
            /// <returns>Retorna true se possuir alguma borda preenchida</returns>
            public bool HasValue()
            {
                return (Left.Width > 0) || (Top.Width > 0) || (Right.Width > 0) || (Bottom.Width > 0);
            }
        }

        /// <summary>
        /// / Estilo das bordas
        /// </summary>
        public struct BorderStyle
        {
            /// <summary>
            /// Espessura da borda
            /// </summary>
            public float Width;

            /// <summary>
            /// Tipo da borda
            /// </summary>
            public BorderType Type;
        }

        #endregion
    }

    /// <summary>
    /// Evento que arrendonda a borda
    /// </summary>
    public class RoundRectangle : IPdfPCellEvent
    {
        private PdfNfeHelper.Bordas _borda;

        /// <summary>
        /// Desenha as bordas
        /// </summary>
        /// <param name="borda"> configurações de borda</param>
        public RoundRectangle(PdfNfeHelper.Bordas borda)
        {
            this._borda = borda;
        }

        /// <summary>
        /// Evento que arredonta a borca
        /// </summary>
        /// <param name="cell">Celula do pdf</param>
        /// <param name="rect">retangulo do pdf</param>
        /// <param name="canvas">canvas do pdf</param>
        public void CellLayout(PdfPCell cell, Rectangle rect, PdfContentByte[] canvas)
        {
            PdfContentByte cb = canvas[PdfPTable.LINECANVAS];
            cb.SetLineWidth(0.5f);
            cb.SetCMYKColorStrokeF(0f, 0f, 0f, 1f);
            this.Round(rect.Left, rect.Bottom, rect.Right - rect.Left, rect.Top - rect.Bottom, 2f, cb);
        }

        private void Round(float x, float y, float w, float h, float r, PdfContentByte cb)
        {
            if (w < 0)
            {
                x += w;
                w = -w;
            }

            if (h < 0)
            {
                y += h;
                h = -h;
            }

            if (r < 0)
            {
                r = -r;
            }

            float b = 0.4477f;

            if (this._borda.Bottom.Width > 0 && this._borda.Left.Width > 0 && this._borda.Bottom.Type == PdfNfeHelper.BorderType.Rounded)
            {
                cb.MoveTo(x + r, y);
            }
            else
            {
                cb.MoveTo(x, y);
            }

            cb.SetLineDash(0f);
            cb.SetLineWidth(0.5f);
            if (this._borda.Bottom.Width > 0 && this._borda.Right.Width > 0 && this._borda.Bottom.Type == PdfNfeHelper.BorderType.Rounded)
            {
                cb.LineTo(x + w - r, y);
                cb.CurveTo(x + w - (r * b), y, x + w, y + (r * b), x + w, y + r);
                cb.Stroke();
                cb.MoveTo(x + w, y + r);
            }
            else if (_borda.Bottom.Width > 0)
            {
                if (_borda.Bottom.Type == PdfNfeHelper.BorderType.Dotted)
                {
                    cb.SetLineDash(3f, 3f);
                }

                cb.LineTo(x + w, y);
                cb.Stroke();
                cb.MoveTo(x + w, y);
            }
            else
            {
                cb.MoveTo(x + w, y);
            }

            cb.SetLineDash(0f);
            cb.SetLineWidth(0.5f);
            if (this._borda.Top.Width > 0 && this._borda.Right.Width > 0 && this._borda.Right.Type == PdfNfeHelper.BorderType.Rounded)
            {
                cb.LineTo(x + w, y + h - r);
                cb.CurveTo(x + w, y + h - (r * b), x + w - (r * b), y + h, x + w - r, y + h);
                cb.Stroke();
                cb.MoveTo(x + w - r, y + h);
            }
            else if (this._borda.Right.Width > 0)
            {
                if (_borda.Right.Type == PdfNfeHelper.BorderType.Dotted)
                {
                    cb.SetLineDash(3f, 3f);
                }

                cb.LineTo(x + w, y + h);
                cb.Stroke();
                cb.MoveTo(x + w, y + h);
            }
            else
            {
                cb.MoveTo(x + w, y + h);
            }

            cb.SetLineDash(0f);
            cb.SetLineWidth(0.5f);
            if (this._borda.Top.Width > 0 && this._borda.Left.Width > 0 && this._borda.Top.Type == PdfNfeHelper.BorderType.Rounded)
            {
                cb.LineTo(x + r, y + h);
                cb.CurveTo(x + (r * b), y + h, x, y + h - (r * b), x, y + h - r);
                cb.Stroke();
                cb.MoveTo(x, y + h - r);
            }
            else if (this._borda.Top.Width > 0)
            {
                if (_borda.Top.Type == PdfNfeHelper.BorderType.Dotted)
                {
                    cb.SetLineDash(3f, 3f);
                }

                cb.LineTo(x, y + h);
                cb.Stroke();
                cb.MoveTo(x, y + h);
            }
            else
            {
                cb.MoveTo(x, y + h);
            }

            cb.SetLineDash(0f);
            cb.SetLineWidth(0.5f);
            if (this._borda.Bottom.Width > 0 && this._borda.Left.Width > 0 && this._borda.Left.Type == PdfNfeHelper.BorderType.Rounded)
            {
                cb.LineTo(x, y + r);
                cb.CurveTo(x, y + (r * b), x + (r * b), y, x + r, y);
                cb.Stroke();
                cb.MoveTo(x, y - r);
            }
            else if (this._borda.Left.Width > 0)
            {
                if (_borda.Left.Type == PdfNfeHelper.BorderType.Dotted)
                {
                    cb.SetLineDash(3f, 3f);
                }

                cb.LineTo(x, y);
                cb.Stroke();
                cb.MoveTo(x, y);
            }
            else
            {
                cb.MoveTo(x, y);
            }
        }
    }
}