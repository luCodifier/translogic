﻿namespace Translogic.Modules.Core.Domain.Services.GestaoDocumentos
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Runtime.Serialization;
    using System.Text;
    using Castle.Services.Transaction;
    using Translogic.Core.Infrastructure;
    using Translogic.Modules.Core.Domain.Model.Estrutura.Repositories;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.DocumentosVagao;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.DocumentosVagao.Repositories;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao.Repositories;
    using Translogic.Modules.Core.Interfaces.GestaoDocumentos;
    using Translogic.Modules.Core.Interfaces.GestaoDocumentos.Balanca;

    /// <summary>
    /// Implementação do serviço de faturamento.
    /// </summary>
    [Transactional]
    public class GestaoDocumentosService : IGestaoDocumentosService
    {
        private readonly ITicketBalancaRepository _ticketBalancaRepository;
        private readonly IVagaoTicketRepository _vagaoTicketRepository;
        private readonly INotaTicketVagaoRepository _notaTicketVagaoRepository;
        private readonly IEmpresaClienteRepository _empresaClienteRepository;
        private readonly IWsTicketControleRecebimentoRepository _ticketControleRecebimentoRepository;
        private readonly IVagaoRepository _vagaoRepository;

        /// <summary>
        /// Construtor do serviço de gestão de documentos.
        /// </summary>
        /// <param name="ticketBalancaRepository">Repositório de ticket da balança injetado.</param>
        /// <param name="vagaoTicketRepository">Repositório de caminhão do ticket da balança injetado.</param>
        /// <param name="notaTicketVagaoRepository">Repositório de notas de caminhão do ticket da balança injetado.</param>
        /// <param name="empresaClienteRepository">Repositório de empresa cliente injetado.</param>
        /// <param name="ticketControleRecebimentoRepository">repositório  de Controle de recebimentos dos tickets de balança injetado.</param>
        /// <param name="vagaoRepository">Repositório de Vagão injetado.</param>
        public GestaoDocumentosService(ITicketBalancaRepository ticketBalancaRepository, IVagaoTicketRepository vagaoTicketRepository, INotaTicketVagaoRepository notaTicketVagaoRepository, IEmpresaClienteRepository empresaClienteRepository, IWsTicketControleRecebimentoRepository ticketControleRecebimentoRepository, IVagaoRepository vagaoRepository)
        {
            _ticketBalancaRepository = ticketBalancaRepository;
            _vagaoTicketRepository = vagaoTicketRepository;
            _notaTicketVagaoRepository = notaTicketVagaoRepository;
            _empresaClienteRepository = empresaClienteRepository;
            _ticketControleRecebimentoRepository = ticketControleRecebimentoRepository;
            _vagaoRepository = vagaoRepository;
        }

        /// <summary>
        /// Informa os dDados do ticket da balança
        /// </summary>
        /// <param name="ticketBalanca">Dados do ticket da balança</param>
        public void InformarTicketBalanca(TicketDto ticketBalanca)
        {
            ProcessarTicket(ticketBalanca);
        }

        /// <summary>
        /// Método responsável por processar os dados do ticket da  balança.
        /// </summary>
        /// <param name="ticketBalanca">Dados do ticket da balança</param>
        protected virtual void ProcessarTicket(TicketDto ticketBalanca)
        {
            WsTicketControleRecebimento controleRecebimento = new WsTicketControleRecebimento();

            try
            {
                controleRecebimento.IndErro = false;

                // Armazena a mensagem recebida
                controleRecebimento.DataCadastro = DateTime.Now;
                controleRecebimento.MensagemRecebida = SerializarMensagem(ticketBalanca);

                // Valida os dados recebidos
                ValidarDadosTicket(ticketBalanca);

                TicketBalanca ticket = ProcessarDadosTicket(ticketBalanca);

                // Grava o protocolo de recebimento
                controleRecebimento.Protocolo = GerarProtocolo(ticketBalanca);

                // Vincula o ticket gerado
                controleRecebimento.Ticket = ticket;
                controleRecebimento.LogRetorno = "Dados processados com sucesso";
                _ticketControleRecebimentoRepository.Inserir(controleRecebimento);
            }
            catch (Exception ex)
            {
                controleRecebimento.IndErro = true;

                StringBuilder sb = new StringBuilder();
                sb.AppendLine(ex.Message);
                sb.AppendLine("[StackTrace]");
                sb.AppendLine(ex.StackTrace);

                controleRecebimento.LogRetorno = sb.ToString();
                _ticketControleRecebimentoRepository.Inserir(controleRecebimento);
                throw;
            }
        }
        
        [Transaction]
        private TicketBalanca ProcessarDadosTicket(TicketDto ticketBalanca)
        {
            // Recupera os dados da empresa de origem, destino e o terminal de descarga.
            var empresaDestino = _empresaClienteRepository.ObterPorCnpj(ticketBalanca.CnpjDestino.ToString().PadLeft(14, '0'));
            var empresaOrigem = _empresaClienteRepository.ObterPorCnpj(ticketBalanca.CnpjOrigem.ToString().PadLeft(14, '0'));

            // Cadastra os dados do ticket da balança
            TicketBalanca ticket = new TicketBalanca()
                {
                    Destino = empresaDestino,
                    Origem = empresaOrigem,
                    Responsavel = ticketBalanca.Responsavel,
                    DataPesagem = ticketBalanca.DataPesagem,
                    Observacao = ticketBalanca.Observacao
                };

            _ticketBalancaRepository.Inserir(ticket);

            // Cadastra os dados do vagão
            VagaoTicket vagao = new VagaoTicket()
                {
                    CodigoVagao = ticketBalanca.Vagao.CodigoVagao,
                    PesoBruto = ticketBalanca.Vagao.PesoBruto,
                    PesoLiquido = ticketBalanca.Vagao.PesoLiquido,
                    PesoTara = ticketBalanca.Vagao.PesoTara,
                    Ticket = ticket
                };

            _vagaoTicketRepository.Inserir(vagao);

            // Cadastra as nfe do vagão
            foreach (var nota in ticketBalanca.Vagao.NotasCarregadas)
            {
                NotaTicketVagao nfe = new NotaTicketVagao()
                    {
                        Vagao = vagao,
                        ChaveNfe = nota.ChaveNfe,
                        PesoRateio = nota.Peso
                    };
                _notaTicketVagaoRepository.Inserir(nfe);
            }

            return ticket;
        }

        private void ValidarDadosTicket(TicketDto ticketBalanca)
        {
            if (ticketBalanca.CnpjOrigem == 0)
            {
                throw new TranslogicValidationException(string.Format("Empresa Origem({0}) não informada.", ticketBalanca.CnpjOrigem));
            }

            if (ticketBalanca.CnpjDestino == 0)
            {
                throw new TranslogicValidationException(string.Format("Empresa Destino({0}) não informada.", ticketBalanca.CnpjDestino));
            }

            var empresaOrigem = _empresaClienteRepository.ObterPorCnpj(ticketBalanca.CnpjOrigem.ToString().PadLeft(14, '0'));
            var empresaDestino = _empresaClienteRepository.ObterPorCnpj(ticketBalanca.CnpjDestino.ToString().PadLeft(14, '0'));

            if (empresaOrigem == null)
            {
                throw new TranslogicValidationException(string.Format("Empresa Origem({0}) não localizada", ticketBalanca.CnpjOrigem));
            }

            if (empresaDestino == null)
            {
                throw new TranslogicValidationException(string.Format("Empresa Destino({0}) não localizada", ticketBalanca.CnpjDestino));
            }

            if (ticketBalanca.Vagao == null)
            {
                throw new TranslogicValidationException(string.Format("Vagão não informado"));
            }

            var codigoVagao = ticketBalanca.Vagao.CodigoVagao.PadLeft(7, '0');

            var vagao = _vagaoRepository.ObterPorCodigo(codigoVagao);
            if (vagao == null)
            {
                throw new TranslogicValidationException(string.Format("Vagão({0}) não localizado", vagao));
            }

            if (ticketBalanca.Vagao.PesoBruto == 0 ||
                ticketBalanca.Vagao.PesoLiquido == 0 ||
                ticketBalanca.Vagao.PesoTara == 0)
            {
                throw new TranslogicValidationException(string.Format("Peso Bruto({0}), Peso Líquido({1}) e PesoTara({2}) não podem ser zero", ticketBalanca.Vagao.PesoBruto, ticketBalanca.Vagao.PesoLiquido, ticketBalanca.Vagao.PesoTara));
            }

            if (ticketBalanca.Vagao.NotasCarregadas.Count <= 0)
            {
                throw new TranslogicValidationException(string.Format("É necessário informar pelo menos uma nf-e."));
            }

            foreach (var nota in ticketBalanca.Vagao.NotasCarregadas)
            {
                var retorno = ValidarChaveNfe(nota.ChaveNfe);
                if (!retorno.Key)
                {
                    throw new TranslogicValidationException(retorno.Value);
                }
            }
        }

        private string SerializarMensagem(TicketDto ticket)
        {
            var output = new MemoryStream();

            using (StreamReader reader = new StreamReader(output))
            {
                DataContractSerializer serializer = new DataContractSerializer(ticket.GetType());
                serializer.WriteObject(output, ticket);
                output.Position = 0;
                return reader.ReadToEnd();
            }
        }

        private string GerarProtocolo(TicketDto ticket)
        {
            return string.Format("{0}{1}", DateTime.Now.ToString("yyyymmdd"), ticket.Vagao.CodigoVagao.PadLeft(7, '0'));
        }

        /// <summary>
        /// Valida a chave NFe
        /// </summary>
        /// <param name="chaveNfe"> The chave nfe. </param>
        /// <returns> Retorna DictionaryEntry com a situacao e mensagem de erro</returns>
        private KeyValuePair<bool, string> ValidarChaveNfe(string chaveNfe)
        {
            int[] pesos = new int[]
                            {
                                4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2
                            };
            if (string.IsNullOrEmpty(chaveNfe))
            {
                return new KeyValuePair<bool, string>(false, "Chave NF-e vazia.");
            }

            if (chaveNfe.Length != 44)
            {
                return new KeyValuePair<bool, string>(false, string.Format("Chave NF-e({0}) inválida.", chaveNfe));
            }

            int somatorio = 0;
            for (int i = 0; i <= chaveNfe.Length - 2; i++)
            {
                int digito;
                try
                {
                    digito = int.Parse(chaveNfe[i].ToString());
                }
                catch (Exception)
                {
                    return new KeyValuePair<bool, string>(false, string.Format("Chave NF-e({0}) contém caracteres inválidos.", chaveNfe));
                }

                somatorio += digito * pesos[i];
            }

            int idEstado;
            if (!int.TryParse(chaveNfe.Substring(0, 2), out idEstado))
            {
                return new KeyValuePair<bool, string>(false, string.Format("Chave NF-e({0}) inválida.", chaveNfe));
            }

            int modeloNota;
            if (int.TryParse(chaveNfe.Substring(20, 2), out modeloNota))
            {
                if (modeloNota != 55)
                {
                    return new KeyValuePair<bool, string>(false, string.Format("O Número da Chave de Acesso digitado {0} não corresponde a uma NF-e. A NF-e utiliza o Modelo 55.", chaveNfe));
                }
            }

            int mod = somatorio % 11;
            int dv = 11 - mod;
            if (mod.Equals(0) || mod.Equals(1))
            {
                dv = 0;
            }

            string stringUltimoDigito = chaveNfe.Substring(43);
            int ultimoDigito;
            int.TryParse(stringUltimoDigito, out ultimoDigito);
            if (!ultimoDigito.Equals(dv))
            {
                return new KeyValuePair<bool, string>(false, string.Format("DV (dígito verificador) da chave de acesso digitada {0} inválido. Verificar o número da chave de acesso e digitá-lo novamente.", chaveNfe));
            }

            return new KeyValuePair<bool, string>(true, "Nfe Válida");
        }
    }
}