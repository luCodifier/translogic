﻿namespace Translogic.Modules.Core.Domain.Services.GestaoDocumentos
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Web.Mvc;
    using System.Xml;
    using ALL.Core.Dominio;
    using ICSharpCode.SharpZipLib.Core;
    using iTextSharp.text;
    using iTextSharp.text.pdf;
    using Model.Via;
    using Model.Via.Repositories;
    using Translogic.Core.Infrastructure;
    using Translogic.Core.Infrastructure.FileSystem.ExtensionMethods;
    using Translogic.Modules.Core.Domain.Model.Diversos.Simconsultas;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Modules.Core.Domain.Model.Estrutura;
    using Translogic.Modules.Core.Domain.Model.Estrutura.Repositories;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Dcls;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.DocumentosVagao;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.DocumentosVagao.Repositories;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Nfes;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Nfes.Repositories;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos.Repositories;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Simconsultas;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Simconsultas.Repositories;
    using Translogic.Modules.Core.Domain.Model.LaudoMercadoria;
    using Translogic.Modules.Core.Domain.Model.PainelExpedicao.Documentos.Dto;
    using Translogic.Modules.Core.Domain.Model.PainelExpedicao.Documentos.Request;
    using Translogic.Modules.Core.Domain.Model.Trem;
    using Translogic.Modules.Core.Domain.Model.Trem.Repositories;
    using Translogic.Modules.Core.Interfaces.GestaoDocumentos.Balanca;
    using Translogic.Modules.Core.Util;
    using Translogic.Modules.EDI.Domain.Models.Nfes;
    using Translogic.Modules.EDI.Domain.Models.Nfes.Repositories;
    using Font = iTextSharp.text.Font;
    using Rectangle = iTextSharp.text.Rectangle;
    using Translogic.Modules.Core.Domain.Services.Documentacao;
    using Translogic.Modules.Core.Domain.Services.FluxosComerciais;
    using Translogic.Modules.EDI.Domain.Services;
    using iTextSharp.text.pdf.draw;
    using Speed.Common;
    using Translogic.Modules.Core.Spd.BLL;

    /// <summary>
    /// Serviço de relatorio dos documentos do despacho.
    /// </summary>
    public class RelatorioDocumentosService
    {
        private readonly ITremRepository _tremRepository;
        private readonly IComposicaoRepository _composicaoRepository;
        private readonly INfeReadonlyRepository _nfeReadonlyRepository;
        private readonly INfeEdiRepository _nfeEdiRepository;
        private readonly INfePdfEdiRepository _nfePdfEdiRepository;
        private readonly INfeSimconsultasRepository _nfeSimconsultasRepository;
        private readonly INfeProdutoReadonlyRepository _nfeProdutoReadonlyRepository;
        private readonly ITicketBalancaRepository _ticketBalancaRepository;
        private readonly INotaTicketVagaoRepository _notaTicketVagaoRepository;
        private readonly IAreaOperacionalRepository _areaOperacionalRepository;
        private readonly IElementoViaRepository _elementoViaRepository;
        private readonly IEmpresaRepository _empresaRepository;
        private readonly ICteEmpresasRepository _cteEmpresaRepository;
        private readonly IItemDespachoRepository _itemDespachoRepository;
        private readonly Font _font = new Font(Font.FontFamily.TIMES_ROMAN, 6F);
        private readonly Font _font7 = new Font(Font.FontFamily.TIMES_ROMAN, 7F);
        private readonly Font _font8 = new Font(Font.FontFamily.TIMES_ROMAN, 8);
        private readonly Font _titulo = new Font(Font.FontFamily.TIMES_ROMAN, 14F, Font.BOLD);
        private readonly Font _titulo2 = new Font(Font.FontFamily.TIMES_ROMAN, 12F, Font.BOLD);
        private readonly Font _subTitulo = new Font(Font.FontFamily.TIMES_ROMAN, 10F, Font.BOLD);
        private readonly Font _subTitulo2 = new Font(Font.FontFamily.TIMES_ROMAN, 8F, Font.BOLD);
        private readonly Font _fontTranslogic = new Font(Font.FontFamily.TIMES_ROMAN, 8F, Font.BOLDITALIC);

        /// <summary>
        /// Construtor do serviço de relatórios de gestão de documentos do despacho.
        /// </summary>
        /// <param name="tremRepository">Repositório de trem injetado.</param>
        /// <param name="composicaoRepository">Repositório de mdfe injetado.</param>
        /// <param name="nfeReadonlyRepository">Repositório de vwnfe injetado.</param>
        /// <param name="nfeEdiRepository">Repositório de edi.edi2_nfe injetado.</param>
        /// <param name="ticketBalancaRepository">Repositório de ticket balança injetado.</param>
        /// <param name="nfeProdutoReadonlyRepository">Repositório de vwnfe produto injetado.</param>
        /// <param name="nfeSimconsultasRepository">Repositório de nfeSimConsultas produto injetado.</param>
        /// <param name="areaOperacionalRepository">Repositório de Areas Operacionais produto injetado.</param>
        /// <param name="elementoViaRepository">Repositório de Elementos da via produto injetado.</param>
        /// <param name="notaTicketVagaoRepository">Repositório de nota vagão ticket balança injetado.</param>
        /// <param name="nfePdfEdiRepository">Repositório de PDF edi</param>
        /// <param name="empresaRepository">Repositório de empresa</param>
        /// <param name="cteEmpresaRepository">Repositório cte empresa</param>
        /// <param name="itemDespachoRepository">Repositório item despacho</param>
        public RelatorioDocumentosService(ITremRepository tremRepository,
                                          IComposicaoRepository composicaoRepository,
                                          INfeReadonlyRepository nfeReadonlyRepository,
                                          INfeEdiRepository nfeEdiRepository,
                                          ITicketBalancaRepository ticketBalancaRepository,
                                          INfeProdutoReadonlyRepository nfeProdutoReadonlyRepository,
                                          INfeSimconsultasRepository nfeSimconsultasRepository,
                                          IAreaOperacionalRepository areaOperacionalRepository,
                                          IElementoViaRepository elementoViaRepository,
                                          INotaTicketVagaoRepository notaTicketVagaoRepository,
                                          INfePdfEdiRepository nfePdfEdiRepository,
                                          IEmpresaRepository empresaRepository,
                                          ICteEmpresasRepository cteEmpresaRepository,
                                          IItemDespachoRepository itemDespachoRepository)
        {
            _tremRepository = tremRepository;
            _composicaoRepository = composicaoRepository;
            _nfeReadonlyRepository = nfeReadonlyRepository;
            _nfeEdiRepository = nfeEdiRepository;
            _ticketBalancaRepository = ticketBalancaRepository;
            _nfeProdutoReadonlyRepository = nfeProdutoReadonlyRepository;
            _nfeSimconsultasRepository = nfeSimconsultasRepository;
            _areaOperacionalRepository = areaOperacionalRepository;
            _elementoViaRepository = elementoViaRepository;
            _notaTicketVagaoRepository = notaTicketVagaoRepository;
            _nfePdfEdiRepository = nfePdfEdiRepository;
            _empresaRepository = empresaRepository;
            _cteEmpresaRepository = cteEmpresaRepository;
            _itemDespachoRepository = itemDespachoRepository;
        }

        /// <summary>
        /// Lista as Mdfes
        /// </summary>
        /// <param name="detalhesPaginacao">detalhes da paginacao</param>
        /// <param name="prefixoTrem">prefixo do trem</param>
        /// <param name="os">os do trem</param>
        /// <param name="dataInicial">data inicial do intervalo</param>
        /// <param name="dataFinal">data final do intervalo</param>
        /// <param name="origem">origem do trem</param>
        /// <param name="destino">destino do trem</param>
        /// <returns>resultado paginado de trens com mdfe</returns>
        public ResultadoPaginado<TremDto> Listar(DetalhesPaginacao detalhesPaginacao, string prefixoTrem, int? os, DateTime? dataInicial, DateTime? dataFinal, string origem, string destino)
        {
            if (string.IsNullOrEmpty(prefixoTrem)
                && string.IsNullOrEmpty(origem)
                && string.IsNullOrEmpty(destino)
                && !os.HasValue
                && !dataInicial.HasValue
                && !dataFinal.HasValue)
            {
                throw new TranslogicException("Informe ao menos um filtro.");
            }

            return _tremRepository.Listar(detalhesPaginacao, prefixoTrem, os, dataInicial, dataFinal, origem, destino, null);
        }

        /// <summary>
        /// Lista as Mdfes
        /// </summary>
        /// <param name="dataInicial">data inicial do intervalo</param>
        /// <param name="dataFinal">data final do intervalo</param>
        /// <param name="vagoes">Lista de Vagões</param>
        /// <param name="estacoes">Estação para filtro</param>
        /// <param name="idLinha">Id da Linha corrente dos vagões</param>
        /// <returns>Retorna os dados da composição</returns>
        public IList<VagaoComposicaoDto> ListarVagoes(DateTime dataInicial, DateTime dataFinal, string[] vagoes, string[] estacoes, int? idLinha)
        {
            return _composicaoRepository.ObterVagoesPorFiltro(dataInicial, dataFinal, vagoes, estacoes, idLinha);
        }

        /// <summary>
        /// Lista as Mdfes
        /// </summary>
        /// <param name="idTrem">id do trem</param>
        /// <returns>resultado paginado de trens com mdfe</returns>
        public TremDto Listar(int idTrem)
        {
            var retorno = _tremRepository.Listar(new DetalhesPaginacao(), string.Empty, null, null, null, string.Empty, string.Empty, idTrem);
            if (retorno.Total == 1)
            {
                return retorno.Items[0];
            }

            throw new TranslogicException(string.Format("Trem não localizado com o id {0}", idTrem));
        }

        /// <summary>
        /// Lista das composições
        /// </summary>
        /// <param name="detalhesPaginacao">detalhes da paginacao</param>
        /// <param name="idTrem">id do trem</param>
        /// <returns>resultado paginado de trens com mdfe</returns>
        public ResultadoPaginado<Composicao> ObterTodasComposicao(DetalhesPaginacao detalhesPaginacao, int idTrem)
        {
            return _composicaoRepository.ObterComposicaoPorTrem(detalhesPaginacao, idTrem);
        }

        public IEmpresa ObterEmpresaCnpj(string cnpj)
        {
            return _empresaRepository.ObterPorCnpj(cnpj);
        }

        /// <summary>
        /// Lista das nfes da composições
        /// </summary>
        /// <param name="idComposicao">Id da composição</param>
        /// <param name="vagoes">lista de Id dos vagões</param>
        /// <returns>lista de nfes</returns>
        public IList<string> ObterNfeVagoesComposicao(int idComposicao, IList<int> vagoes)
        {
            return _composicaoRepository.ObterNfeVagoesComposicao(idComposicao, vagoes);
        }

        /// <summary>
        /// Lista das nfes com PDF anexado
        /// </summary>
        /// <param name="itensDespachos">lista de Id dos itens de despachos</param>
        /// <returns>lista de nfes</returns>
        public IList<decimal> ObterNfePdfItensDespacho(IList<int> itensDespachos)
        {
            return _composicaoRepository.ObterNfePdfItensDespacho(itensDespachos);
        }

        /// <summary>
        /// Lista das nfes com PDF anexado
        /// </summary>
        /// <param name="itensDespachos">lista de Id dos itens de despachos</param>
        /// <returns>lista de nfes</returns>
        public IList<ItemDespachoNfeDto> ObterNfePdfItensDespachoDto(IList<int> itensDespachos)
        {
            return _composicaoRepository.ObterNfePdfItensDespachoDto(itensDespachos);
        }

        /// <summary>
        /// Lista das laudos das mercadorias
        /// </summary>
        /// <param name="listaCodigosVagoes">Lista de Código de Vagões</param>
        /// <returns>Lista de laudos das mercadorias</returns>
        public IList<LaudoMercadoriaDto> ObterLaudoMercadoriaPdfItensDespachoDto(IList<string> listaCodigosVagoes)
        {
            return _composicaoRepository.ObterLaudoMercadoriaPdfItensDespachoDto(listaCodigosVagoes);
        }

        /// <summary>
        /// Lista das nfes da composições
        /// </summary>
        /// <param name="itensDespachos">lista de Id dos itens de despachos</param>
        /// <returns>lista de nfes</returns>
        public IList<string> ObterNfeItensDespacho(IList<int> itensDespachos)
        {
            return _composicaoRepository.ObterNfeItensDespacho(itensDespachos);
        }

        /// <summary>
        /// Lista das Linhas por area operacional
        /// </summary>
        /// <param name="areaOper">Código da área operacional</param>
        /// <returns>lista de linhas</returns>
        public IList<ElementoVia> ObterLinhasPorAreaOperacional(string areaOper)
        {
            IAreaOperacional areaOp;
            try
            {
                areaOp = _areaOperacionalRepository.ObterPorCodigo((areaOper ?? String.Empty).ToUpper());

                if (areaOp == null)
                {
                    return new List<ElementoVia>();
                }
            }
            catch (Exception)
            {
                return new List<ElementoVia>();
            }

            return _elementoViaRepository.ObterPorIdEstacaoMae(areaOp.Id.Value).OrderBy(a => a.Codigo).ToList();
        }

        /// <summary>
        /// Lista de notas
        /// </summary>
        /// <param name="nfes">notas fiscais </param>
        /// <returns>resultado paginado de trens com mdfe</returns>
        public IList<NotaTicketDto> ObterNotasTickets(List<string> nfes)
        {
            return _nfeReadonlyRepository.ObterPorChaveNfe(nfes);
        }

        /// <summary>
        /// Lista de notas
        /// </summary>
        /// <param name="idComposicao">Id da composição</param>
        /// <param name="vagoes">lista de Id dos vagões</param>
        /// <returns>resultado paginado de trens com mdfe</returns>
        public List<NotaTicketVagao> ObterTicketVagoes(int idComposicao, IList<int> vagoes)
        {
            IList<decimal> ids = _ticketBalancaRepository.ObterNotasTicketsPorVagao(idComposicao, vagoes);
            return _notaTicketVagaoRepository.ObterTodos(ids.ToList());
        }

        /// <summary>
        /// Lista de notas para vagões em pátio
        /// </summary>
        /// <param name="itensDespacho">lista de Id dos itens de despacho</param>
        /// <returns>resultado paginado de trens com mdfe</returns>
        public IList<NotaTicketVagao> ObterTicketItensDespacho(IList<int> itensDespacho)
        {
            return _ticketBalancaRepository.ObterNotasTicketItensDespacho(itensDespacho);
        }

        /// <summary>
        /// Lista de notas para vagões em pátio
        /// </summary>
        /// <param name="itensDespacho">lista de Id dos itens de despacho</param>
        /// <param name="historico">informa se a pesquisa esta sendo feita pela tela de histórico devido o Fat2.0</param>
        /// <returns>resultado paginado de trens com mdfe</returns>
        public IList<ItemDespachoNotaTicketVagaoDto> ObterTicketItensDespachoDto(IList<int> itensDespacho, bool historico)
        {
            var tickets = _ticketBalancaRepository.ObterNotasTicketItensDespachoDto(itensDespacho, historico);
            foreach (var ticket in tickets)
            {
                ticket.NotaTicketVagao.Vagao.SerieVagao = ticket.SerieVagao;
            }

            return tickets;
        }

        /// <summary>
        /// Lista de tickets de pesagem e o cliente associado ao mesmo
        /// </summary>
        /// <param name="itensDespacho">lista de Id dos itens de despacho</param>
        /// <param name="historico">informa se a pesquisa esta sendo feita pela tela de histórico devido o Fat2.0</param>
        /// <returns>resultado paginado de tickets</returns>
        public IList<NotaTicketVagaoClienteDto> ObterTicketClienteItensDespacho(IList<int> itensDespacho, bool historico)
        {
            return _ticketBalancaRepository.ObterNotasTicketVagaoClienteItensDespacho(itensDespacho, historico);
        }

        /// <summary>
        /// Lista de notas
        /// </summary>
        /// <param name="idComposicao">Id da composição</param>
        /// <returns>resultado paginado de trens com mdfe</returns>
        public IList<VagaoComposicaoDto> ObterVagoesComposicao(int idComposicao)
        {
            return _composicaoRepository.ObterVagoesComposicao(idComposicao);
        }

        /// <summary>
        /// Obtém o vagão e o cte dos itens de despacho
        /// </summary>
        /// <param name="itensDespacho">IDs dos itens despacho</param>
        /// <param name="codigosVagoes">Códigos dos vagões</param>
        /// <returns>Retorna a instância dos dados do ItemDespacho</returns>
        public IList<ImpressaoDocumentosVagaoItemDespacho> ObterVagoesCtesItensDespacho(IList<int> itensDespacho, IList<string> codigosVagoes)
        {
            var resultadosBanco = _itemDespachoRepository.ObterVagoesCtesItensDespacho(itensDespacho);

            // A ordenação deve ser baseada no que o usuário informar no filtro de vagões da tela
            // Como pode ser aleatório, não é possível realizar no banco a ordenação e por este motivo realizamos 
            // a ordenação aqui. Caso não for informado nenhum vagão, então a ordenação é a padrão (Sequência da Composição)
            IList<ImpressaoDocumentosVagaoItemDespacho> resultadosOrdenados = null;
            if (codigosVagoes == null || codigosVagoes.Count == 0)
            {
                resultadosOrdenados = resultadosBanco;
            }
            else
            {
                resultadosOrdenados = new List<ImpressaoDocumentosVagaoItemDespacho>();

                var codigos = codigosVagoes.Distinct().ToList();
                foreach (var codigo in codigos)
                {
                    var resultados = resultadosBanco.Where(r => r.CodigoVagao == codigo).ToList();
                    resultadosOrdenados = resultadosOrdenados.Union(resultados).ToList();
                }
            }

            return resultadosOrdenados;
        }

        public IList<DespachoTranslogic> ObterDespachosPorItensDespacho(int[] idsItensDespacho)
        {
            return _itemDespachoRepository.ObterDespachosPorItemDespacho(idsItensDespacho);
        }

        public IList<ItemDespachoDespachoDto> ObterDespachosPorItensDespachoDto(int[] idsItensDespacho)
        {
            return _itemDespachoRepository.ObterDespachosPorItemDespachoDto(idsItensDespacho);
        }

        /// <summary>
        /// Returns the excel result generating html automatically
        /// </summary>
        /// <param name="fileName">Nome do arquivo</param>
        /// <param name="chaveNfe">chave da Nf-e</param>
        /// <returns> ActionResult de excel </returns>
        public ActionResult PdfResult(string fileName, string chaveNfe)
        {
            var nfe = BL_VwNfe.ObterPorChaveNfe(chaveNfe);
            Stream returnContent = GerarPdf(nfe);

            var fsr = new FileStreamResult(returnContent, "application/pdf");
            fsr.FileDownloadName = fileName;
            return fsr;
        }

        /// <summary>
        /// Verifica se a chave possui o seu PDF no banco de dados
        /// </summary>
        /// <param name="chavesNfe">As chaves da Nota Fiscal Eletronica</param>
        public bool PossuiNfePdf(string chavesNfe)
        {
            var nfe = BL_VwNfe.ObterPorChaveNfes(chavesNfe);
            return nfe != null && nfe.Count > 0;
        }

        /// <summary>
        /// Retorna a stream de pdf da nfe
        /// </summary>
        /// <param name="chavesNfe">Lista de chave das nfe</param>
        /// <returns>Retorna o pdf das nfe</returns>
        public Stream GerarPdf(IList<string> chavesNfe)
        {
            if (chavesNfe == null)
                throw new Exception("Chave Nfe não informada.");

            var merge = new PdfMerge();

            var listaChavesNfe = chavesNfe.Where(a => !String.IsNullOrWhiteSpace(a)).Distinct().ToList();

            // Obtendo os pdfs enviados pelo e-mail
            var nfesPdfs = _nfePdfEdiRepository.ObterPorChavesNfe(listaChavesNfe);
            // Adicionando os PDFs encontrados
            foreach (var nfePdf in nfesPdfs)
            {
                if (listaChavesNfe.Any(c => c == nfePdf.ChaveNfe))
                {
                    merge.AddFile(nfePdf.Pdf);
                    // Removendo as chaves das notas encontradas no e-mail
                    listaChavesNfe.Remove(nfePdf.ChaveNfe);
                }
            }

            // Precisamos criar o PDF das notas não encontradas
            List<Spd.Data.VwNfe> nfes = null;
            if (listaChavesNfe.Any())
            {
                nfes = BL_VwNfe.ObterPorChaveNfes(listaChavesNfe);
            }

            if (nfes != null)
            {
                foreach (var nfe in nfes)
                {
                    var returnContent = GerarPdf(nfe);
                    merge.AddFile(returnContent);
                }
            }

            return merge.FileListCount() > 0 ? merge.Execute() : new MemoryStream();
        }

        /// <summary>
        /// Retorna a stream de pdf da nfe
        /// </summary>
        /// <param name="chavesNfe">Lista de chave das nfe</param>
        /// <param name="imprimirNfeCliente">Verifica se deve-se obter as NFes do cliente</param>
        /// <param name="imprimirWebDanfe">Verifica se deve-se obter as NFes do WebDanfe</param>
        /// <returns>Retorna o pdf das nfe</returns>
        public List<ItemDespachoNfeDto> GerarPdfNfes(IList<string> chavesNfe, bool imprimirNfeCliente = true, bool imprimirWebDanfe = true)
        {
            if (chavesNfe == null)
                throw new Exception("Chave Nfe não informada.");

            var listaChavesNfe = chavesNfe.Where(a => !String.IsNullOrWhiteSpace(a)).Distinct().ToList();

            var retornoPdfs = new List<ItemDespachoNfeDto>();
            foreach (var chaveNfe in listaChavesNfe)
            {
                retornoPdfs.Add(new ItemDespachoNfeDto
                {
                    ChaveNfe = chaveNfe,
                    Pdf = null,
                    IdItemDespacho = 0,
                    IdNfePdf = 0,
                });
            }

            if (listaChavesNfe.Any())
            {
                /* Obtem todos os PDFs que foram recebidos pelos clientes */
                var nfesPdfsClientes = BL_VwEdiNfePdf.ObterPorChavesNfe(listaChavesNfe.ToArray());

                foreach (var nfePdfCliente in nfesPdfsClientes)
                {
                    var registro =
                        retornoPdfs.FirstOrDefault(reg => reg.ChaveNfe == nfePdfCliente.ChaveNfe && reg.Pdf == null);
                    if (registro != null)
                    {
                        if (imprimirNfeCliente)
                        {
                            registro.Pdf = nfePdfCliente.Pdf;
                        }
                        // Adicionando à lista de PDFs encontrados
                        if (listaChavesNfe.Any(c => c == nfePdfCliente.ChaveNfe))
                        {
                            // Removendo as chaves das notas encontradas no e-mail
                            listaChavesNfe.Remove(nfePdfCliente.ChaveNfe);
                        }
                    }
                }

                if (imprimirWebDanfe)
                {
                    // Precisamos criar o PDF das notas não encontradas
                    List<Spd.Data.VwNfe> nfes = null;
                    if (listaChavesNfe.Any())
                    {
                        //nfes = _nfeReadonlyRepository.ObterPorChavesNfe(listaChavesNfe);
                        nfes = BL_VwNfe.ObterPorChaveNfes(listaChavesNfe);
                    }

                    if (nfes != null)
                    {
                        var returnContents = GerarPdfs(nfes);
                        if (returnContents != null)
                        {
                            foreach (var returnContent in returnContents.Where(r => r.Pdf != null).ToList())
                            {
                                var registro = retornoPdfs.FirstOrDefault(reg => reg.ChaveNfe == returnContent.ChaveNfe && reg.Pdf == null);
                                if (registro != null)
                                {
                                    registro.Pdf = returnContent.Pdf;

                                    // Adicionando à lista de PDFs encontrados
                                    if (listaChavesNfe.Any(c => c == returnContent.ChaveNfe))
                                    {
                                        // Removendo as chaves das notas encontradas no e-mail
                                        listaChavesNfe.Remove(returnContent.ChaveNfe);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return retornoPdfs;
        }

        /// <summary>
        /// Retorna a stream de pdf da nfe
        /// </summary>
        /// <param name="nfesIds">Lista de chave das nfe</param>
        /// <returns>Retorna o pdf das nfe</returns>
        public Stream GerarPdfNotaClienteUpload(IList<int> nfesIds)
        {
            var merge = new PdfMerge();
            var idsInseridos = new List<NotaFiscalPdfIdDto>();

            foreach (var idNfe in nfesIds)
            {
                var nfe = this._nfePdfEdiRepository.ObterPorId(idNfe);

                if (nfe != null)
                {
                    if (nfe.Pdf != null)
                    {
                        if (nfe.IdNfeSimConsultas.HasValue)
                        {
                            if (!idsInseridos.Any(i => i.IdNfeSimConsultas == nfe.IdNfeSimConsultas.Value))
                            {
                                byte[] returnContent = nfe.Pdf;
                                merge.AddFile(returnContent);

                                var inseridoSimConsultas = new NotaFiscalPdfIdDto();
                                inseridoSimConsultas.IdNfeSimConsultas = nfe.IdNfeSimConsultas.Value;
                                idsInseridos.Add(inseridoSimConsultas);
                            }
                        }
                        else if (nfe.Id_Nfe != 0)
                        {
                            if (!idsInseridos.Any(i => i.IdNfeEdi == nfe.Id_Nfe))
                            {
                                byte[] returnContent = nfe.Pdf;
                                merge.AddFile(returnContent);

                                var inseridoEdi = new NotaFiscalPdfIdDto();
                                inseridoEdi.IdNfeEdi = nfe.Id_Nfe;
                                idsInseridos.Add(inseridoEdi);
                            }
                        }
                    }
                }
            }

            return merge.Execute();
        }

        /// <summary>
        /// Retorna a stream de pdf da nfe
        /// </summary>
        /// <param name="nfesIds">Lista de chave das nfe</param>
        /// <returns>Retorna o pdf das nfe</returns>
        public List<NotaFiscalPdfIdDto> ObterPdfsNotaClienteUpload(IList<int> nfesIds)
        {
            var idsInseridos = new List<NotaFiscalPdfIdDto>();

            foreach (var idNfe in nfesIds)
            {
                var nfe = this._nfePdfEdiRepository.ObterPorId(idNfe);

                if (nfe != null)
                {
                    if (nfe.Pdf != null)
                    {
                        var pdf = nfe.Pdf;
                        var chaveNfe = Tools.BuscarChaveNfeDoArquivoPDF(pdf);

                        if (nfe.IdNfeSimConsultas.HasValue)
                        {
                            if (!idsInseridos.Any(i => i.IdNfeSimConsultas == nfe.IdNfeSimConsultas.Value))
                            {
                                var inseridoSimConsultas = new NotaFiscalPdfIdDto();
                                inseridoSimConsultas.IdNfeSimConsultas = nfe.IdNfeSimConsultas.Value;
                                inseridoSimConsultas.ChaveNfe = chaveNfe;
                                inseridoSimConsultas.Pdf = pdf;
                                idsInseridos.Add(inseridoSimConsultas);
                            }
                        }
                        else if (nfe.Id_Nfe != 0)
                        {
                            if (!idsInseridos.Any(i => i.IdNfeEdi == nfe.Id_Nfe))
                            {
                                var inseridoEdi = new NotaFiscalPdfIdDto();
                                inseridoEdi.IdNfeEdi = nfe.Id_Nfe;
                                inseridoEdi.ChaveNfe = chaveNfe;
                                inseridoEdi.Pdf = pdf;
                                idsInseridos.Add(inseridoEdi);
                            }
                        }
                    }
                }
            }

            return idsInseridos;
        }

        private Stream GerarPdf(Spd.Data.VwNfe nfe)
        {
            var tempFilePath = Path.GetTempFileName();
            var document = new Document(PageSize.A4, 5, 5, 5, 5);
            var writer = PdfWriter.GetInstance(document, new FileStream(tempFilePath, FileMode.Create));

            var chaveNfe = nfe.ChaveNfe;
            var notaSimConsultas = BL_VwNfe.ObterPorChaveNfe(chaveNfe, "SCO");
            notaSimConsultas.Xml = BL_VwNfeXml.GetXml(chaveNfe, "SCO");
            if (nfe.Xml == null)
                nfe.Xml = BL_VwNfeXml.GetXml(chaveNfe, "SCO");

            document.Open();

            document.Add(CreateHeaderNf(nfe));
            document.Add(CreateHeaderEmpresa(writer.DirectContent, nfe));
            document.Add(CreateDestRem(nfe));
            document.Add(CreateImpostos(nfe));
            document.Add(CreateTransporte(notaSimConsultas ?? nfe));
            document.Add(CreateDadosProdutos(notaSimConsultas ?? nfe));
            document.Add(CreateDadosAdicionais(notaSimConsultas ?? nfe));

            document.Close();
            writer.Close();

            var sr = new StreamReader(tempFilePath);
            var ms = new MemoryStream();
            StreamUtils.Copy(sr.BaseStream, ms, new byte[4096]);
            sr.Close();
            File.Delete(tempFilePath);
            ms.Seek(0, SeekOrigin.Begin);

            return ms;
        }

        private List<ItemDespachoNfeDto> GerarPdfs(List<Spd.Data.VwNfe> nfes)
        {
            var chavesNfes = nfes.Select(n => n.ChaveNfe).Distinct().ToList();
            var chavesNfesSimConsultas = nfes.Where(n => n.Tipo == "SCO").Select(n => n.ChaveNfe).Distinct().ToList();
            IList<Spd.Data.VwNfe> notasSimConsultas = null;

            if (chavesNfesSimConsultas.Any())
            {
                notasSimConsultas = BL_VwNfe.ObterPorChaveNfes(chavesNfesSimConsultas).Where(p => p.Tipo == "SCO").ToList();
            }
            else
            {
                notasSimConsultas = new List<Spd.Data.VwNfe>();
            }

            var retornos = new List<ItemDespachoNfeDto>();
            foreach (var chaveNfe in chavesNfes)
            {
                retornos.Add(new ItemDespachoNfeDto
                {
                    ChaveNfe = chaveNfe,
                    Pdf = null,
                    IdItemDespacho = 0,
                    IdNfePdf = 0,
                });
            }

            int count = 0;
            foreach (var nfe in nfes)
            {
                count++;
                // Sis.LogTrace("Gerando PDF: " + count + " de " + nfes.Count);
                var tempFilePath = Path.GetTempFileName();
                var document = new Document(PageSize.A4, 5, 5, 5, 5);
                var writer = PdfWriter.GetInstance(document, new FileStream(tempFilePath, FileMode.Create));
                var chaveNfe = nfe.ChaveNfe;
                var notaSimConsultas = notasSimConsultas.FirstOrDefault(n => n.ChaveNfe == chaveNfe);

                document.Open();

                document.Add(CreateHeaderNf(nfe));
                document.Add(CreateHeaderEmpresa(writer.DirectContent, nfe));
                document.Add(CreateDestRem(nfe));
                document.Add(CreateImpostos(nfe));
                document.Add(CreateTransporte(notaSimConsultas ?? (Spd.Data.VwNfe)nfe));
                document.Add(CreateDadosProdutos(notaSimConsultas ?? (Spd.Data.VwNfe)nfe));
                document.Add(CreateDadosAdicionais(notaSimConsultas ?? (Spd.Data.VwNfe)nfe));

                document.Close();
                writer.Close();

                var sr = new StreamReader(tempFilePath);
                var ms = new MemoryStream();
                StreamUtils.Copy(sr.BaseStream, ms, new byte[4096]);
                sr.Close();
                File.Delete(tempFilePath);
                ms.Seek(0, SeekOrigin.Begin);

                var registro = retornos.FirstOrDefault(r => r.ChaveNfe == chaveNfe);
                if (registro != null)
                {
                    registro.Pdf = this.ReadFully(ms);
                }
            }

            return retornos;
        }

        private PdfPTable CreateHeaderNf(Spd.Data.VwNfe nfe)
        {
            PdfPTable tab = new PdfPTable(3);
            tab.WidthPercentage = 100;
            tab.DefaultCell.Border = Rectangle.NO_BORDER;
            StringBuilder texto = new StringBuilder();
            // topo
            texto.AppendLine(string.Format(@"RECEBEMOS DE {0} OS PRODUTOS E/OU SERVIÇOS CONSTANTES DA NOTA FISCAL ELETRÔNICA INDICADA ABAIXO. EMISSÃO: {1} VALOR TOTAL: {2} DESTINATÁRIO: {3} - {4}, {5} - {6} {7} {8}-{9}", nfe.RazaoSocialEmitente, nfe.DataEmissao.GetValueOrDefault().ToShortDateString(), FormatarValor(nfe.Valor), nfe.RazaoSocialDestinatario, nfe.LogradouroDestinatario, nfe.NumeroDestinatario, nfe.ComplementoDestinatario, nfe.BairroDestinatario, nfe.MunicipioDestinatario, nfe.UfDestinatario));
            var cell = PdfNfeHelper.CreateCell(_font7, texto.ToString(), PdfNfeHelper.All);
            cell.Colspan = 2;
            tab.AddCell(cell);

            // nfe
            var numNfe = nfe.NumeroNotaFiscal.ToString().PadLeft(9, '0');
            texto.Clear();
            texto.AppendLine("NF-e");
            texto.AppendLine(string.Format("Nº. {0}.{1}.{2}", numNfe.Substring(0, 3), numNfe.Substring(3, 3), numNfe.Substring(6, 3)));
            texto.AppendLine(string.Format("Série {0}", nfe.SerieNotaFiscal.PadLeft(3, '0')));
            cell = PdfNfeHelper.CreateCell(_titulo2, texto.ToString(), Element.ALIGN_CENTER, PdfNfeHelper.All, Element.ALIGN_MIDDLE);
            cell.Rowspan = 3;
            tab.AddCell(cell);

            // data recebimento
            cell = PdfNfeHelper.CreateHeaderCell(_font, "DATA DE RECEBIMENTO");
            tab.AddCell(cell);

            // assinatura
            cell = PdfNfeHelper.CreateHeaderCell(_font, "IDENTIFICAÇÃO E ASSINATURA DO RECEBEDOR");
            tab.AddCell(cell);

            // data recebimento
            cell = PdfNfeHelper.CreateEndFieldCell(" ");
            tab.AddCell(cell);

            // assinatura
            cell = PdfNfeHelper.CreateEndFieldCell(" ");
            tab.AddCell(cell);

            // assinatura
            cell = PdfNfeHelper.CreateCell(" ", PdfNfeHelper.CriarBordas(new[] { 0, 0, 0, 1 }, new[] { PdfNfeHelper.BorderType.Solid, PdfNfeHelper.BorderType.Solid, PdfNfeHelper.BorderType.Solid, PdfNfeHelper.BorderType.Dotted }));
            cell.Colspan = 3;
            cell.FixedHeight = 4f;
            tab.AddCell(cell);

            // assinatura
            cell = PdfNfeHelper.CreateCell(" ", PdfNfeHelper.None);
            cell.Colspan = 3;
            cell.FixedHeight = 4f;
            tab.AddCell(cell);

            var columnWidths = new[] { 15f, 65f, 20f };
            tab.SetWidths(columnWidths);

            return tab;
        }

        private PdfPTable CreateHeaderEmpresa(PdfContentByte cb, Spd.Data.VwNfe nfe)
        {
            PdfPTable tab = new PdfPTable(3);
            PdfPTable tabEmpresa = new PdfPTable(1);
            PdfPTable tabDANFE = new PdfPTable(3);
            PdfPTable tabMDFE = new PdfPTable(1);
            PdfPTable tabDados = new PdfPTable(3);
            tab.WidthPercentage = 100;
            tabEmpresa.WidthPercentage = 100;
            tabDANFE.WidthPercentage = 100;
            tabMDFE.WidthPercentage = 100;
            tabDados.WidthPercentage = 100;
            tab.DefaultCell.Border = Rectangle.NO_BORDER;
            tabEmpresa.DefaultCell.Border = Rectangle.NO_BORDER;
            tabDANFE.DefaultCell.Border = Rectangle.NO_BORDER;
            tabMDFE.DefaultCell.Border = Rectangle.NO_BORDER;
            tabDados.DefaultCell.Border = Rectangle.NO_BORDER;

            StringBuilder texto = new StringBuilder();

            // empresa

            var cell = PdfNfeHelper.CreateHeaderCell(" ");
            tabEmpresa.AddCell(cell);

            cell = PdfNfeHelper.CreateCell(_subTitulo, nfe.RazaoSocialEmitente, Element.ALIGN_CENTER, PdfNfeHelper.LeftRight);
            tabEmpresa.AddCell(cell);

            cell = PdfNfeHelper.CreateCell(_font8, FormatarLogradouro(nfe.LogradouroEmitente, nfe.NumeroEmitente, nfe.ComplementoEmitente), Element.ALIGN_CENTER, PdfNfeHelper.LeftRight);
            tabEmpresa.AddCell(cell);

            cell = PdfNfeHelper.CreateCell(_font8, string.Format("{0} - {1}", nfe.BairroEmitente, FormatarCep(nfe.CepEmitente)), Element.ALIGN_CENTER, PdfNfeHelper.LeftRight);
            tabEmpresa.AddCell(cell);

            cell = PdfNfeHelper.CreateCell(_font8, string.Format("{0} - {1} Fone/Fax: {2}", nfe.MunicipioEmitente, nfe.UfEmitente, FormatarTelefone(nfe.TelefoneEmitente)), Element.ALIGN_CENTER, PdfNfeHelper.LeftRight);
            tabEmpresa.AddCell(cell);

            cell = PdfNfeHelper.CreateCell(string.Empty, PdfNfeHelper.BottomLeftRight);
            tabEmpresa.AddCell(cell);

            cell = new PdfPCell(tabEmpresa);
            cell.Border = Rectangle.NO_BORDER;
            tab.AddCell(cell);

            // table DANFE
            cell = PdfNfeHelper.CreateCell(_titulo, "DANFE", Element.ALIGN_CENTER, PdfNfeHelper.TopLeftRight);
            cell.Colspan = 3;
            tabDANFE.AddCell(cell);

            // assinatura
            texto.Clear();
            texto.AppendLine("Documento Auxiliar da Nota");
            texto.AppendLine("Fiscal Eletrônica");
            cell = PdfNfeHelper.CreateCell(_font, texto.ToString(), Element.ALIGN_CENTER, PdfNfeHelper.LeftRight);
            cell.Colspan = 3;
            tabDANFE.AddCell(cell);

            cell = PdfNfeHelper.CreateCell(_font, "0 - ENTRADA", PdfNfeHelper.Left);
            tabDANFE.AddCell(cell);

            cell = PdfNfeHelper.CreateCell(_titulo, !string.IsNullOrEmpty(nfe.TipoNotaFiscal) ? nfe.TipoNotaFiscal : "1", Element.ALIGN_CENTER, PdfNfeHelper.All);
            cell.Rowspan = 2;
            tabDANFE.AddCell(cell);

            cell = PdfNfeHelper.CreateCell(_font, string.Empty, PdfNfeHelper.Right);
            tabDANFE.AddCell(cell);

            cell = PdfNfeHelper.CreateCell(_font, "1 - SAÍDA", PdfNfeHelper.Left);
            tabDANFE.AddCell(cell);

            cell = PdfNfeHelper.CreateCell(_font, " ", PdfNfeHelper.Right);
            tabDANFE.AddCell(cell);

            var numNfe = nfe.NumeroNotaFiscal.ToString().PadLeft(9, '0');
            numNfe = string.Format("Nº. {0}.{1}.{2}", numNfe.Substring(0, 3), numNfe.Substring(3, 3), numNfe.Substring(6, 3));

            cell = PdfNfeHelper.CreateCell(_subTitulo, numNfe, Element.ALIGN_CENTER, PdfNfeHelper.LeftRight);
            cell.Colspan = 3;
            tabDANFE.AddCell(cell);

            var serieNfe = string.Format("Série {0}", nfe.SerieNotaFiscal.PadLeft(3, '0'));
            cell = PdfNfeHelper.CreateCell(_subTitulo, serieNfe, Element.ALIGN_CENTER, PdfNfeHelper.LeftRight);
            cell.Colspan = 3;
            tabDANFE.AddCell(cell);

            cell = PdfNfeHelper.CreateCell(_font, "Folha 1/1", Element.ALIGN_CENTER, PdfNfeHelper.BottomLeftRight);
            cell.Colspan = 3;
            tabDANFE.AddCell(cell);

            var columnDanfeWidths = new[] { 15f, 3f, 2f };
            tabDANFE.SetWidths(columnDanfeWidths);

            cell = new PdfPCell(tabDANFE);
            cell.Border = Rectangle.NO_BORDER;
            tab.AddCell(cell);

            // tabela dados mdfe
            cell = PdfNfeHelper.CreateCell(_font, string.Empty, PdfNfeHelper.All);
            cell.Image = GerarCodigoBarra(nfe.ChaveNfe, cb);
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.FixedHeight = 40f;
            tabMDFE.AddCell(cell);

            cell = PdfNfeHelper.CreateHeaderCell("CHAVE DE ACESSO");
            tabMDFE.AddCell(cell);

            int x = 0;
            string chave = string.Empty;
            for (int i = 0; i < nfe.ChaveNfe.Length; i++)
            {
                x++;
                chave += nfe.ChaveNfe[i];
                if (x == 4)
                {
                    chave += " ";
                    x = 0;
                }
            }

            cell = PdfNfeHelper.CreateEndFieldCell(_subTitulo2, chave, Element.ALIGN_CENTER);
            tabMDFE.AddCell(cell);

            cell = PdfNfeHelper.CreateCell(_font8, "Consulta de autenticidade no portal nacional da NF-e", Element.ALIGN_CENTER, PdfNfeHelper.TopLeftRight);
            tabMDFE.AddCell(cell);

            cell = PdfNfeHelper.CreateEndFieldCell(_font8, "www.nfe.fazenda.gov.br/portal ou no site da Sefaz Autorizadora", Element.ALIGN_CENTER);
            tabMDFE.AddCell(cell);

            cell = new PdfPCell(tabMDFE);
            cell.Border = Rectangle.NO_BORDER;
            tab.AddCell(cell);

            var columnWidths = new[] { 40f, 20f, 50f };
            tab.SetWidths(columnWidths);

            cell = PdfNfeHelper.CreateHeaderCell("NATUREZA DA OPERAÇÃO");
            cell.Colspan = 2;
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateHeaderCell(_font, "PROTOCOLO DE AUTORIZAÇÃO DE USO");
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateEndFieldCell(nfe.NaturezaOperacao, Element.ALIGN_CENTER);
            cell.Colspan = 2;
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateEndFieldCell(string.Format("{0} - {1}", " ", " "), Element.ALIGN_CENTER);
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateHeaderCell("INSCRIÇÃO ESTADUAL");
            tabDados.AddCell(cell);

            cell = PdfNfeHelper.CreateHeaderCell("INSCRIÇÃO ESTADUAL DO SUBST. TRIBUT.");
            tabDados.AddCell(cell);

            cell = PdfNfeHelper.CreateHeaderCell("CNPJ");
            tabDados.AddCell(cell);

            cell = PdfNfeHelper.CreateEndFieldCell(nfe.InscricaoEstadualEmitente);
            tabDados.AddCell(cell);

            cell = PdfNfeHelper.CreateEndFieldCell(" ");
            tabDados.AddCell(cell);

            cell = PdfNfeHelper.CreateEndFieldCell(FormatarCnpj(nfe.CnpjEmitente));
            tabDados.AddCell(cell);

            cell = new PdfPCell(tabDados);
            cell.Colspan = 3;
            cell.Border = Rectangle.NO_BORDER;
            tab.AddCell(cell);

            return tab;
        }

        private PdfPTable CreateDestRem(Spd.Data.VwNfe nfe)
        {
            PdfPTable tab = new PdfPTable(3);
            PdfPTable tabLogradouro = new PdfPTable(5);
            tab.WidthPercentage = 100;
            tabLogradouro.WidthPercentage = 100;
            tab.DefaultCell.Border = Rectangle.NO_BORDER;
            tabLogradouro.DefaultCell.Border = Rectangle.NO_BORDER;

            var cell = PdfNfeHelper.CreateCell(_subTitulo2, "DESTINATÁRIO / REMETENTE");
            cell.Colspan = 3;
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateHeaderCell(" NOME / RAZÃO SOCIAL");
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateHeaderCell(" CNPJ / CPF");
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateHeaderCell(" DATA DA EMISSÃO");
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateEndFieldCell(nfe.RazaoSocialDestinatario, Element.ALIGN_LEFT);
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateEndFieldCell(FormatarCnpj(nfe.CnpjDestinatario));
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateEndFieldCell(nfe.DataEmissao.GetValueOrDefault().ToShortDateString());
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateHeaderCell(" ENDEREÇO");
            tabLogradouro.AddCell(cell);

            cell = PdfNfeHelper.CreateHeaderCell(" BAIRRO / DISTRITO");
            cell.Colspan = 2;
            tabLogradouro.AddCell(cell);

            cell = PdfNfeHelper.CreateHeaderCell(" CEP");
            tabLogradouro.AddCell(cell);

            cell = PdfNfeHelper.CreateHeaderCell(" DATA DA SAÍDA");
            tabLogradouro.AddCell(cell);

            cell = PdfNfeHelper.CreateEndFieldCell(FormatarLogradouro(nfe.LogradouroDestinatario, nfe.NumeroDestinatario, nfe.ComplementoDestinatario), Element.ALIGN_LEFT);
            tabLogradouro.AddCell(cell);

            cell = PdfNfeHelper.CreateEndFieldCell(nfe.BairroDestinatario);
            cell.Colspan = 2;
            tabLogradouro.AddCell(cell);

            cell = PdfNfeHelper.CreateEndFieldCell(FormatarCep(nfe.CepDestinatario));
            tabLogradouro.AddCell(cell);

            cell = PdfNfeHelper.CreateEndFieldCell(nfe.DataSaida.HasValue ? nfe.DataSaida.Value.ToShortDateString() : " ");
            tabLogradouro.AddCell(cell);

            cell = PdfNfeHelper.CreateHeaderCell(" MUNICÍPIO");
            tabLogradouro.AddCell(cell);

            cell = PdfNfeHelper.CreateHeaderCell(" UF");
            tabLogradouro.AddCell(cell);

            cell = PdfNfeHelper.CreateHeaderCell(" FONE / FAX");
            tabLogradouro.AddCell(cell);

            cell = PdfNfeHelper.CreateHeaderCell(" INSCRIÇÃO ESTADUAL");
            tabLogradouro.AddCell(cell);

            cell = PdfNfeHelper.CreateHeaderCell(" HORA DA SAÍDA");
            tabLogradouro.AddCell(cell);

            cell = PdfNfeHelper.CreateEndFieldCell(nfe.MunicipioDestinatario, Element.ALIGN_LEFT);
            tabLogradouro.AddCell(cell);

            cell = PdfNfeHelper.CreateEndFieldCell(nfe.UfDestinatario);
            tabLogradouro.AddCell(cell);

            cell = PdfNfeHelper.CreateEndFieldCell(FormatarTelefone(nfe.TelefoneDestinatario));
            tabLogradouro.AddCell(cell);

            cell = PdfNfeHelper.CreateEndFieldCell(nfe.InscricaoEstadualDestinatario);
            tabLogradouro.AddCell(cell);

            cell = PdfNfeHelper.CreateEndFieldCell(nfe.DataSaida.HasValue ? nfe.DataSaida.Value.ToLongTimeString() : " ");
            tabLogradouro.AddCell(cell);

            var columnLogWidths = new[] { 50, 5f, 20f, 20f, 15f };
            tabLogradouro.SetWidths(columnLogWidths);

            cell = new PdfPCell(tabLogradouro);
            cell.Colspan = 3;
            cell.Border = Rectangle.NO_BORDER;
            tab.AddCell(cell);

            var columnWidths = new[] { 60, 20f, 15f };
            tab.SetWidths(columnWidths);

            return tab;
        }

        private PdfPTable CreateImpostos(Spd.Data.VwNfe nfe)
        {
            PdfPTable tab = new PdfPTable(7);
            tab.WidthPercentage = 100;
            tab.DefaultCell.Border = Rectangle.NO_BORDER;

            var cell = PdfNfeHelper.CreateCell(_subTitulo2, "CÁLCULO DO IMPOSTO");
            cell.Colspan = 7;
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateHeaderCell("BASE DE CÁLCULO DO ICMS");
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateHeaderCell("VALOR DO ICMS");
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateHeaderCell("BASE DE CÁLC. ICMS S.T.");
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateHeaderCell("VALOR DO ICMS SUBST.");
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateHeaderCell("VALOR IMP. IMPORTAÇÃO");
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateHeaderCell("VALOR DO PIS");
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateHeaderCell("VALOR TOTAL DOS PRODUTOS");
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateEndFieldCell(FormatarValor(nfe.ValorBaseCalculoIcms), Element.ALIGN_RIGHT);
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateEndFieldCell(FormatarValor(nfe.ValorIcms), Element.ALIGN_RIGHT);
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateEndFieldCell(FormatarValor(nfe.ValorBaseCalculoSubTributaria), Element.ALIGN_RIGHT);
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateEndFieldCell(FormatarValor(nfe.ValorSubTributaria), Element.ALIGN_RIGHT);
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateEndFieldCell(FormatarValor(nfe.ValorImpostoImportacao), Element.ALIGN_RIGHT);
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateEndFieldCell(FormatarValor(nfe.ValorPis), Element.ALIGN_RIGHT);
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateEndFieldCell(FormatarValor(nfe.ValorProduto), Element.ALIGN_RIGHT);
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateHeaderCell(" VALOR DO FRETE");
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateHeaderCell(" VALOR DO SEGURO");
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateHeaderCell(" DESCONTO");
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateHeaderCell(" OUTRAS DESPESAS");
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateHeaderCell(" VALOR TOTAL DO IPI");
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateHeaderCell(" VALOR DA COFINS");
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateHeaderCell(" VALOR TOTAL DA NOTA");
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateEndFieldCell(FormatarValor(nfe.ValorFrete), Element.ALIGN_RIGHT);
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateEndFieldCell(FormatarValor(nfe.ValorSeguro), Element.ALIGN_RIGHT);
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateEndFieldCell(FormatarValor(nfe.ValorDesconto), Element.ALIGN_RIGHT);
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateEndFieldCell(FormatarValor(nfe.ValorOutro), Element.ALIGN_RIGHT);
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateEndFieldCell(FormatarValor(nfe.ValorIpi), Element.ALIGN_RIGHT);
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateEndFieldCell(FormatarValor(nfe.ValorCofins), Element.ALIGN_RIGHT);
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateEndFieldCell(FormatarValor(nfe.Valor), Element.ALIGN_RIGHT);
            tab.AddCell(cell);

            var columnWidths = new[] { 21f, 17f, 18f, 18f, 18f, 17f, 25f };
            tab.SetWidths(columnWidths);

            return tab;
        }

        private PdfPTable CreateTransporte(Spd.Data.VwNfe nfe)
        {
            Spd.Data.VwNfe nota = nfe as Spd.Data.VwNfe;
            PdfPTable tab = new PdfPTable(6);
            tab.WidthPercentage = 100;
            tab.DefaultCell.Border = Rectangle.NO_BORDER;

            PdfPTable tabMercadoria = new PdfPTable(6);
            tabMercadoria.WidthPercentage = 100;
            tabMercadoria.DefaultCell.Border = Rectangle.NO_BORDER;

            var cell = PdfNfeHelper.CreateCell(_subTitulo2, "TRANSPORTADOR / VOLUMES TRANSPORTADOS");
            cell.Colspan = 6;
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateHeaderCell("NOME / RAZÃO SOCIAL");
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateHeaderCell("FRETE POR CONTA");
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateHeaderCell("CÓDIGO ANTT");
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateHeaderCell("PLACA DO VEÍCULO");
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateHeaderCell("UF");
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateHeaderCell("CNPJ / CPF");
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateEndFieldCell(nfe.NomeTransportadora, Element.ALIGN_LEFT);
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateEndFieldCell(FormatarModeloFrete(nfe.ModeloFrete));
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateEndFieldCell(string.Empty);
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateEndFieldCell(nfe.Placa);
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateEndFieldCell(nfe.UfTransportadora);
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateEndFieldCell(FormatarCnpj(nfe.CnpjTransportadora));
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateHeaderCell("ENDEREÇO");
            cell.Colspan = 2;
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateHeaderCell(" MUNICÍPIO");
            cell.Colspan = 2;
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateHeaderCell(" UF");
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateHeaderCell(" INSCRIÇÃO ESTADUAL");
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateEndFieldCell(nfe.EnderecoTransportadora, Element.ALIGN_LEFT);
            cell.Colspan = 2;
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateEndFieldCell(nfe.MunicipioTransportadora);
            cell.Colspan = 2;
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateEndFieldCell(nfe.UfTransportadora);
            tab.AddCell(cell);

            string texto = ObterValorXmlNota(nota, @"TRANSP/TRANSPORTA/IE", false);
            cell = PdfNfeHelper.CreateEndFieldCell(texto);
            tab.AddCell(cell);

            var columnWidths = new[] { 35f, 14f, 14f, 14f, 5f, 23f };
            tab.SetWidths(columnWidths);

            cell = PdfNfeHelper.CreateHeaderCell(" QUANTIDADE");
            tabMercadoria.AddCell(cell);

            cell = PdfNfeHelper.CreateHeaderCell(" ESPÉCIE");
            tabMercadoria.AddCell(cell);

            cell = PdfNfeHelper.CreateHeaderCell(" MARCA");
            tabMercadoria.AddCell(cell);

            cell = PdfNfeHelper.CreateHeaderCell(" NUMERAÇÃO");
            tabMercadoria.AddCell(cell);

            cell = PdfNfeHelper.CreateHeaderCell(" PESO BRUTO");
            tabMercadoria.AddCell(cell);

            cell = PdfNfeHelper.CreateHeaderCell(" PESO LÍQUIDO");
            tabMercadoria.AddCell(cell);

            cell = PdfNfeHelper.CreateEndFieldCell(nfe.PesoBruto.ToString());
            tabMercadoria.AddCell(cell);

            texto = ObterValorXmlNota(nota, @"ESP", true);
            cell = PdfNfeHelper.CreateEndFieldCell(texto);
            tabMercadoria.AddCell(cell);

            texto = ObterValorXmlNota(nota, @"MARCA", true);
            cell = PdfNfeHelper.CreateEndFieldCell(texto);
            tabMercadoria.AddCell(cell);

            cell = PdfNfeHelper.CreateEndFieldCell(string.Empty);
            tabMercadoria.AddCell(cell);

            cell = PdfNfeHelper.CreateEndFieldCell(FormatarPeso(nfe.PesoBruto));
            tabMercadoria.AddCell(cell);

            cell = PdfNfeHelper.CreateEndFieldCell(FormatarPeso(nfe.Peso));
            tabMercadoria.AddCell(cell);

            var columnMercadoriaWidths = new[] { 8f, 10f, 10f, 10f, 14f, 14f };
            tabMercadoria.SetWidths(columnMercadoriaWidths);

            cell = new PdfPCell(tabMercadoria);
            cell.Colspan = 6;
            cell.Border = Rectangle.NO_BORDER;
            tab.AddCell(cell);

            return tab;
        }

        private PdfPTable CreateDadosProdutos(Spd.Data.VwNfe nfe)
        {
            var nota = nfe as Spd.Data.VwNfe;
            var tabLayout = new PdfPTable(1);
            var tab = new PdfPTable(14);
            tab.WidthPercentage = 100;
            tabLayout.WidthPercentage = 100;
            tab.DefaultCell.Border = Rectangle.NO_BORDER;
            tabLayout.DefaultCell.Border = Rectangle.NO_BORDER;

            // TODO: PERFORMANCE2
            //var produtos = _nfeProdutoReadonlyRepository.ObterPorTipoIdHql(nfe.Id.ToInt32(), nfe.Tipo);
            var produtos = BL_VwNfeProd.ObterPorTipoId(nfe.Id.ToInt32(), nfe.Tipo);

            var cell = PdfNfeHelper.CreateCell(_subTitulo2, "DADOS DOS PRODUTOS / SERVIÇOS");
            cell.Colspan = 14;
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateCell(_font, "CÓDIGO PRODUTO", Element.ALIGN_CENTER, PdfNfeHelper.CriarBordas(new[] { 1, 1, 1, 1 }, new[] { PdfNfeHelper.BorderType.Solid, PdfNfeHelper.BorderType.Rounded, PdfNfeHelper.BorderType.Solid, PdfNfeHelper.BorderType.Solid }));
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateCell(_font, "DESCRIÇÃO DO PRODUTO / SERVIÇO", Element.ALIGN_CENTER, PdfNfeHelper.AllSolid, Element.ALIGN_TOP);
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateCell(_font, "NCM/SH", Element.ALIGN_CENTER, PdfNfeHelper.AllSolid, Element.ALIGN_TOP);
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateCell(_font, "O/CST", Element.ALIGN_CENTER, PdfNfeHelper.AllSolid, Element.ALIGN_TOP);
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateCell(_font, "CFOP", Element.ALIGN_CENTER, PdfNfeHelper.AllSolid, Element.ALIGN_TOP);
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateCell(_font, "UN", Element.ALIGN_CENTER, PdfNfeHelper.AllSolid, Element.ALIGN_TOP);
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateCell(_font, "QUANT", Element.ALIGN_CENTER, PdfNfeHelper.AllSolid, Element.ALIGN_TOP);
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateCell(_font, "VALOR UNIT", Element.ALIGN_CENTER, PdfNfeHelper.AllSolid, Element.ALIGN_TOP);
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateCell(_font, "VALOR TOTAL", Element.ALIGN_CENTER, PdfNfeHelper.AllSolid, Element.ALIGN_TOP);
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateCell(_font, "B.CÁLC ICMS", Element.ALIGN_CENTER, PdfNfeHelper.AllSolid, Element.ALIGN_TOP);
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateCell(_font, "VALOR ICMS", Element.ALIGN_CENTER, PdfNfeHelper.AllSolid, Element.ALIGN_TOP);
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateCell(_font, "VALOR IPI", Element.ALIGN_CENTER, PdfNfeHelper.AllSolid, Element.ALIGN_TOP);
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateCell(_font, "ALÍQ. ICMS", Element.ALIGN_CENTER, PdfNfeHelper.AllSolid, Element.ALIGN_TOP);
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateCell(_font, "ALÍQ. IPI", Element.ALIGN_CENTER, PdfNfeHelper.CriarBordas(new[] { 1, 1, 1, 1 }, new[] { PdfNfeHelper.BorderType.Solid, PdfNfeHelper.BorderType.Solid, PdfNfeHelper.BorderType.Rounded, PdfNfeHelper.BorderType.Solid }));
            tab.AddCell(cell);

            foreach (var produto in produtos)
            {

                cell = PdfNfeHelper.CreateCell(_font, produto.CodigoProduto, Element.ALIGN_CENTER, PdfNfeHelper.SolidTopLeftRight, Element.ALIGN_TOP);
                tab.AddCell(cell);

                cell = PdfNfeHelper.CreateCell(_font, produto.DescricaoProduto, Element.ALIGN_LEFT, PdfNfeHelper.SolidTopLeftRight, Element.ALIGN_TOP);
                tab.AddCell(cell);

                cell = PdfNfeHelper.CreateCell(_font, produto.CodigoNcm, Element.ALIGN_CENTER, PdfNfeHelper.SolidTopLeftRight, Element.ALIGN_TOP);
                tab.AddCell(cell);

                cell = PdfNfeHelper.CreateCell(_font, produto.IcmsCst, Element.ALIGN_CENTER, PdfNfeHelper.SolidTopLeftRight, Element.ALIGN_TOP);
                tab.AddCell(cell);

                cell = PdfNfeHelper.CreateCell(_font, produto.Cfop, Element.ALIGN_CENTER, PdfNfeHelper.SolidTopLeftRight, Element.ALIGN_TOP);
                tab.AddCell(cell);

                cell = PdfNfeHelper.CreateCell(_font, produto.UnidadeComercial, Element.ALIGN_CENTER, PdfNfeHelper.SolidTopLeftRight, Element.ALIGN_TOP);
                tab.AddCell(cell);

                cell = PdfNfeHelper.CreateCell(_font, FormatarValor(produto.QuantidadeComercial), Element.ALIGN_RIGHT, PdfNfeHelper.SolidTopLeftRight, Element.ALIGN_TOP);
                tab.AddCell(cell);

                cell = PdfNfeHelper.CreateCell(_font, FormatarValor(produto.ValorUnitarioTributacao), Element.ALIGN_RIGHT, PdfNfeHelper.SolidTopLeftRight, Element.ALIGN_TOP);
                tab.AddCell(cell);

                cell = PdfNfeHelper.CreateCell(_font, FormatarValor(produto.ValorProduto), Element.ALIGN_RIGHT, PdfNfeHelper.SolidTopLeftRight, Element.ALIGN_TOP);
                tab.AddCell(cell);

                try
                {
                    cell = PdfNfeHelper.CreateCell(_font, FormatarValor(produto.ValorBaseCalculoIcms), Element.ALIGN_RIGHT, PdfNfeHelper.SolidTopLeftRight, Element.ALIGN_TOP);
                    tab.AddCell(cell);
                }
                catch (Exception ex)
                {
                    Sis.LogException(ex);
                    Sis.LogTrace(Newtonsoft.Json.JsonConvert.SerializeObject(produto, Newtonsoft.Json.Formatting.Indented));
                    Sis.LogTrace("ValorBaseCalculoIcms: " + produto.ValorBaseCalculoIcms);
                }

                cell = PdfNfeHelper.CreateCell(_font, FormatarValor(produto.ValorIcms), Element.ALIGN_RIGHT, PdfNfeHelper.SolidTopLeftRight, Element.ALIGN_TOP);
                tab.AddCell(cell);

                cell = PdfNfeHelper.CreateCell(_font, FormatarValor(produto.ValorIpi), Element.ALIGN_RIGHT, PdfNfeHelper.SolidTopLeftRight, Element.ALIGN_TOP);
                tab.AddCell(cell);

                cell = PdfNfeHelper.CreateCell(_font, FormatarValor(produto.AliquotaIcms), Element.ALIGN_RIGHT, PdfNfeHelper.SolidTopLeftRight, Element.ALIGN_TOP);
                tab.AddCell(cell);

                cell = PdfNfeHelper.CreateCell(_font, FormatarValor(produto.AliquotaPis), Element.ALIGN_RIGHT, PdfNfeHelper.SolidTopLeftRight, Element.ALIGN_TOP);
                tab.AddCell(cell);
            }

            cell = PdfNfeHelper.CreateCell(" ", PdfNfeHelper.SolidWithTopDash);
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateCell(" ", PdfNfeHelper.SolidWithTopDash);
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateCell(" ", PdfNfeHelper.SolidWithTopDash);
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateCell(" ", PdfNfeHelper.SolidWithTopDash);
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateCell(" ", PdfNfeHelper.SolidWithTopDash);
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateCell(" ", PdfNfeHelper.SolidWithTopDash);
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateCell(" ", PdfNfeHelper.SolidWithTopDash);
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateCell(" ", PdfNfeHelper.SolidWithTopDash);
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateCell(" ", PdfNfeHelper.SolidWithTopDash);
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateCell(" ", PdfNfeHelper.SolidWithTopDash);
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateCell(" ", PdfNfeHelper.SolidWithTopDash);
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateCell(" ", PdfNfeHelper.SolidWithTopDash);
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateCell(" ", PdfNfeHelper.SolidWithTopDash);
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateCell(" ", PdfNfeHelper.SolidWithTopDash);
            tab.AddCell(cell);

            var columnWidths = new[]
                {
                    11f, // cod produto
                    33f, // desc produto
                    6f,  // ncm
                    4f, // cst
                    4f, // cfop
                    3f, // un
                    6f, // quant
                    6, // valor unit
                    5f, // total
                    5f, // icms
                    5f, // valor icms
                    5f, // ipi
                    4f, // aliq icms
                    5f // aliq ipi
                };
            tab.SetWidths(columnWidths);

            var xml = BL_VwNfeXml.GetXml(nfe.ChaveNfe, "EDI"); // _nfeSimconsultasRepository.ObterTagXml(nfe.ChaveNfe, "infCpl");
            string texto = @"Inf. Contribuinte: " + ObterValorXmlNota(xml, "infCpl", true);
            var height = 280f;

            if (texto.Length <= 1000)
            {
                height = 300f;
            }

            cell = new PdfPCell(tab);
            cell.FixedHeight = height;
            cell.Border = Rectangle.NO_BORDER;
            tabLayout.AddCell(cell);

            return tabLayout;
        }

        private PdfPTable CreateDadosAdicionais(Spd.Data.VwNfe nfe)
        {
            var nota = nfe as Spd.Data.VwNfe;
            var tab = new PdfPTable(2);
            tab.WidthPercentage = 100;
            tab.DefaultCell.Border = Rectangle.NO_BORDER;

            var cell = PdfNfeHelper.CreateCell(_subTitulo2, "DADOS ADICIONAIS");
            cell.Colspan = 2;
            cell.Padding = 2f;
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateHeaderCell(_subTitulo2, "INFORMAÇÕES COMPLEMENTARES");
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateHeaderCell(_subTitulo2, "RESERVADO AO FISCO");
            tab.AddCell(cell);

            var informacaoContribuinte = ObterValorXmlNota(nota, "infCpl", true);
            if (string.IsNullOrEmpty(informacaoContribuinte))
            {
                if (nfe != null)
                {
                    // Precisei deixar assim pois o Modules Core não está acessando o schema EDI
                    // TODO: PERFORMANCE2
                    var xml = BL_VwNfeXml.GetXml(nfe.ChaveNfe, "EDI"); // _nfeSimconsultasRepository.ObterTagXml(nfe.ChaveNfe, "infCpl");
                    informacaoContribuinte = ObterValorXmlNota(xml, "infCpl", true);
                }
            }

            var texto = @"Inf. Contribuinte: " + informacaoContribuinte;

            var height = 130f;
            var width = 30f;

            texto = texto.Length.ToString() + texto;
            cell = PdfNfeHelper.CreateEndFieldCell(_font7, texto, Element.ALIGN_LEFT, Element.ALIGN_TOP);
            //// cell.FixedHeight = height;
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateEndFieldCell(_font7, ObterValorXmlNota(nota, "infAdFisco", true), Element.ALIGN_LEFT, Element.ALIGN_TOP);
            //// cell.FixedHeight = height;
            tab.AddCell(cell);

            cell = PdfNfeHelper.CreateCell(_fontTranslogic, string.Format("Impresso com o Translogic ({0}) ", DateTime.Now), Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE);
            cell.Colspan = 2;
            tab.AddCell(cell);

            var columnWidths = new[] { width, 10f };
            tab.SetWidths(columnWidths);

            return tab;
        }

        private string ObterValorXmlNota(Spd.Data.VwNfe nota, string campo, bool regex)
        {
            if (nota != null && nota.Xml != null)
            {
                if (regex)
                {
                    string pattern = string.Format(@"<{0}>(?<TEXTO>.*?)</{0}", campo).ToUpperInvariant();

                    // new Regex("<qCom>(?<link>.*?)</qCom>").Match(@"<CFOP>6504</CFOP><uCom>KG</uCom><qCom>36030.0000</qCom>").Groups["link"]
                    Group groupRegex = new Regex(pattern).Match(nota.Xml.ToUpperInvariant()).Groups["TEXTO"];

                    if (groupRegex.Success)
                    {
                        return groupRegex.Value;
                    }
                }
                else
                {
                    var doc = new XmlDocument();
                    doc.LoadXml(nota.Xml.Replace("&amp;", string.Empty).ToUpperInvariant());
                    var path = @"/NFE/INFNFE/";
                    var node = doc.SelectSingleNode(path + campo);

                    if (node != null)
                    {
                        return node.InnerXml;
                    }
                }
            }

            return string.Empty;
        }

        private string ObterValorXmlNota(string xml, string campo, bool regex)
        {
            if (!string.IsNullOrEmpty(xml))
            {
                if (regex)
                {
                    string pattern = string.Format(@"<{0}>(?<TEXTO>.*?)</{0}", campo).ToUpperInvariant();

                    // new Regex("<qCom>(?<link>.*?)</qCom>").Match(@"<CFOP>6504</CFOP><uCom>KG</uCom><qCom>36030.0000</qCom>").Groups["link"]
                    Group groupRegex = new Regex(pattern).Match(xml.ToUpperInvariant()).Groups["TEXTO"];

                    if (groupRegex.Success)
                    {
                        return groupRegex.Value;
                    }
                }
                else
                {
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(xml.ToUpperInvariant());
                    string path = @"/NFE/INFNFE/";
                    var node = doc.SelectSingleNode(path + campo);

                    if (node != null)
                    {
                        return node.InnerXml;
                    }
                }
            }

            return string.Empty;
        }

        private string FormatarLogradouro(string endereco, string numero, string complemento)
        {
            return string.Format(@"{0}, {1} - {2}", endereco, numero, complemento);
        }

        private string FormatarTelefone(string fone)
        {
            if (string.IsNullOrEmpty(fone))
            {
                return string.Empty;
            }

            fone = fone.PadLeft(10, '0');

            fone = fone.PadRight(10).Substring(fone.Length - 10, 10);

            return string.Format("({0}) {1}-{2}", fone.Substring(0, 2), fone.Substring(2, 4), fone.Substring(6, 4));
        }

        private string FormatarValor(decimal? valor)
        {
            try
            {
                return valor.GetValueOrDefault().ToString("N2", CultureInfo.InvariantCulture);
            }
            catch (Exception ex)
            {
                Sis.LogException(ex, "Erro em FormataValor: " + valor);
                return "????";
            }
        }

        private string FormatarPeso(decimal? valor)
        {
            return valor.GetValueOrDefault().ToString("N3", CultureInfo.InvariantCulture);
        }

        private string FormatarCep(string cep)
        {
            if (string.IsNullOrEmpty(cep))
            {
                return string.Empty;
            }

            cep = cep.Trim().PadLeft(5, '0');
            cep = (cep.Length >= 8 ? cep : cep.Trim().PadRight(8, '0'));
            return string.Format("{0}-{1}", cep.Substring(0, 5), cep.Substring(5, 3));
        }

        private string FormatarModeloFrete(string frete)
        {
            if (string.IsNullOrEmpty(frete))
            {
                return string.Empty;
            }

            if (frete == "0") // ModeloFreteEnum.PorContaEmitente)
            {
                return "(0) - Emitente";
            }

            if (frete == "1") // ModeloFreteEnum.PorContaDestinatarioRemetente)
            {
                return "(1) - Destinatário";
            }

            if (frete == "2") // ModeloFreteEnum.PorContaTerceiros)
            {
                return "(2) - Terceiros";
            }

            return " ";
        }

        public string FormatarCnpj(string cnpj)
        {
            if (string.IsNullOrEmpty(cnpj))
            {
                return string.Empty;
            }

            var retorno = cnpj.PadLeft(14, '0');

            return string.Format("{0}.{1}.{2}/{3}-{4}", retorno.Substring(0, 2), retorno.Substring(2, 3), retorno.Substring(5, 3), retorno.Substring(8, 4), retorno.Substring(12, 2));
        }

        private iTextSharp.text.Image GerarCodigoBarra(string texto, PdfContentByte cb)
        {
            Barcode128 bar = new Barcode128();
            bar.CodeType = Barcode128.CODE_C;
            bar.Code = texto;
            bar.Font = null;
            bar.BarHeight = 35;
            bar.N = 3;
            return bar.CreateImageWithBarcode(cb, null, null);
        }

        private Stream GetLogoImage()
        {
            Stream rumoLogo = LoadFileFromResouce("Translogic.Modules.Core.Resources.rumo.png");
            return rumoLogo;
        }

        private Stream LoadFileFromResouce(string strFileName)
        {
            // Recupera o Assebly.
            // var assembly = Assembly.GetExecutingAssembly();
            var assembly = Assembly.Load("Translogic.Modules.Core");
            // Recupera o arquivo do assembly
            var stream = assembly.GetManifestResourceStream(strFileName);

            try
            {
                // Verifica se o arquivo foi encontrado
                if (stream == null)
                {
                    throw new FileNotFoundException("Não foi possível localizar o arquivo " + strFileName + " no resouce.", strFileName);
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return stream;
        }

        private string ToTitleCase(string str)
        {
            if (String.IsNullOrEmpty(str))
                return String.Empty;

            var cultureInfo = System.Threading.Thread.CurrentThread.CurrentCulture;
            return cultureInfo.TextInfo.ToTitleCase(str.ToLower());
        }

        /// <summary>
        /// Gera DCL em PDF em Lote
        /// </summary>
        /// <param name="dcls">Lista de DCL a serem impressos em PDF</param>
        /// <returns>Stream do pdf</returns>
        public Stream GerarDclPdfLote(List<DclImpressao> dcls)
        {
            string tempFilePath = Path.GetTempFileName();
            Document document = new Document(PageSize.A4, 0, 0, 85, 30);

            PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(tempFilePath, FileMode.Create));

            document.Open();

            // Definição da Fonte
            iTextSharp.text.Font fontTextoNormal = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 8f, Font.NORMAL, iTextSharp.text.BaseColor.BLACK);
            iTextSharp.text.Font fontTextoGrandeNegrito = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 12f, Font.BOLD, iTextSharp.text.BaseColor.BLACK);
            iTextSharp.text.Font fontTextoMedioNegrito = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 10f, Font.BOLD, iTextSharp.text.BaseColor.BLACK);
            iTextSharp.text.Font fontTextoPequenoNegrito = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 8f, Font.BOLD, iTextSharp.text.BaseColor.BLACK);

            // Logo (Rumo.png)
            iTextSharp.text.Image rumoLogo2 = iTextSharp.text.Image.GetInstance(GetLogoImage());
            rumoLogo2.ScaleToFit(78f, 22f);

            foreach (DclImpressao dcl in dcls)
            {

                var dclDocumentos = dcl.ListaDocs;
                var ctes = dcl.Ctes;
                var fluxoComercial = dcl.FluxoComercial;
                var emitente = dcl.Emitente;
                var empresaRemetente = dcl.Remetente;
                var empresaDestinatario = dcl.Destinatario;
                var empresaExpedidor = dcl.Expedidor;
                var empresaRecebedor = dcl.Recebedor;


                // Table Principal - Header
                PdfPTable tp = new PdfPTable(2);
                tp.TotalWidth = 500f;
                tp.LockedWidth = true;
                float[] widths = new float[] { 250f, 250f };
                tp.SetWidths(widths);

                // Célula 1 (table com 5 colunas)
                PdfPTable th1 = new PdfPTable(9);
                th1.TotalWidth = 250f;
                th1.LockedWidth = true;
                widths = new float[] { 20f, 20f, 20f, 20f, 30f, 30f, 30f, 30f, 50f };
                th1.SetWidths(widths);

                PdfPCell c = new PdfPCell(rumoLogo2);
                c.Colspan = 4;
                c.Border = 0;
                c.HorizontalAlignment = 1;
                c.VerticalAlignment = Element.ALIGN_MIDDLE;
                th1.AddCell(c);

                PdfPTable thEmitente = new PdfPTable(1);

                c = new PdfPCell(new Phrase("Emitente", fontTextoMedioNegrito));
                c.Border = 0;
                c.HorizontalAlignment = 1;
                thEmitente.AddCell(c);
                c = new PdfPCell(new Phrase((!String.IsNullOrEmpty(emitente.Endereco) ? this.ToTitleCase(emitente.Endereco) : ""), fontTextoNormal));
                // "Rua Emílio Bertolini, 100 - Vila Oficina"
                c.Border = 0;
                c.HorizontalAlignment = 1;
                thEmitente.AddCell(c);
                c = new PdfPCell(new Phrase((emitente.CidadeIbge != null && (!String.IsNullOrEmpty(emitente.CidadeIbge.Descricao)) ? this.ToTitleCase(emitente.CidadeIbge.Descricao) + " - " + emitente.CidadeIbge.SiglaEstado : "") + " - " + emitente.Cep, fontTextoNormal));
                //"Curitiba - PR - Cep: 82920-30"
                c.Border = 0;
                c.HorizontalAlignment = 1;
                thEmitente.AddCell(c);
                c = new PdfPCell(new Phrase("CNPJ: " + this.FormatarCnpj(emitente.Cgc), fontTextoNormal));
                //"CNPJ: 01.258.944/0005-50"
                c.Border = 0;
                c.HorizontalAlignment = 1;
                thEmitente.AddCell(c);
                c = new PdfPCell(new Phrase("IE: " + emitente.InscricaoEstadual, fontTextoNormal));
                //"IE: 9012219951"
                c.Border = 0;
                c.HorizontalAlignment = 1;
                thEmitente.AddCell(c);

                c = new PdfPCell(thEmitente);
                c.Colspan = 5;
                c.Border = 0;
                c.HorizontalAlignment = 1;
                th1.AddCell(c);

                PdfPCell ch1 = new PdfPCell(th1);
                tp.AddCell(ch1);

                // Célula 2 (table com 8 colunas)
                PdfPTable th2 = new PdfPTable(12);

                PdfPTable thDclTitulo = new PdfPTable(1);
                c = new PdfPCell(new Phrase("DCL", fontTextoGrandeNegrito));
                c.Border = 0;
                c.HorizontalAlignment = 1;
                thDclTitulo.AddCell(c);
                c = new PdfPCell(new Phrase("Documento de Carga e Lotação", fontTextoNormal));
                c.Border = 0;
                c.HorizontalAlignment = 1;
                thDclTitulo.AddCell(c);
                c = new PdfPCell(thDclTitulo);
                c.Colspan = 12;
                c.HorizontalAlignment = 1;
                th2.AddCell(c);


                c = new PdfPCell(new Phrase("Número", fontTextoNormal));
                c.Colspan = 3;
                c.UseVariableBorders = true;
                c.BorderWidthBottom = 0f;
                c.BorderWidthLeft = 0f;
                c.HorizontalAlignment = 1;
                th2.AddCell(c);

                c = new PdfPCell(new Phrase("Série", fontTextoNormal));
                c.Colspan = 2;
                c.UseVariableBorders = true;
                c.BorderWidthBottom = 0f;
                c.BorderWidthLeft = 0f;
                c.HorizontalAlignment = 1;
                th2.AddCell(c);

                c = new PdfPCell(new Phrase("Fluxo Comercial", fontTextoNormal));
                c.Colspan = 3;
                c.UseVariableBorders = true;
                c.BorderWidthBottom = 0f;
                c.BorderWidthLeft = 0f;
                c.HorizontalAlignment = 1;
                th2.AddCell(c);

                c = new PdfPCell(new Phrase("Data/Hora Emissão", fontTextoNormal));
                c.Colspan = 4;
                c.UseVariableBorders = true;
                c.BorderWidthBottom = 0f;
                c.BorderWidthLeft = 0f;
                c.HorizontalAlignment = 1;
                th2.AddCell(c);


                // Número
                c = new PdfPCell(new Phrase((dcl != null ? dcl.Numero : ""), fontTextoNormal));
                c.Colspan = 3;
                c.UseVariableBorders = true;
                c.BorderWidthTop = 0f;
                c.BorderWidthBottom = 0f;
                c.BorderWidthLeft = 0f;
                c.HorizontalAlignment = 1;
                th2.AddCell(c);

                // Série
                c = new PdfPCell(new Phrase((dcl != null ? dcl.Serie : ""), fontTextoNormal));
                c.Colspan = 2;
                c.UseVariableBorders = true;
                c.BorderWidthTop = 0f;
                c.BorderWidthBottom = 0f;
                c.BorderWidthLeft = 0f;
                c.HorizontalAlignment = 1;
                th2.AddCell(c);

                // Fluxo Comercial
                c = new PdfPCell(new Phrase((fluxoComercial != null ? fluxoComercial.Codigo : ""), fontTextoNormal));
                c.Colspan = 3;
                c.UseVariableBorders = true;
                c.BorderWidthTop = 0f;
                c.BorderWidthBottom = 0f;
                c.BorderWidthLeft = 0f;
                c.HorizontalAlignment = 1;
                th2.AddCell(c);

                c = new PdfPCell(new Phrase(DateTime.Now.ToString("dd/MM/yyyy HH:mm"), fontTextoNormal));
                c.Colspan = 4;
                c.UseVariableBorders = true;
                c.BorderWidthTop = 0f;
                c.BorderWidthBottom = 0f;
                c.BorderWidthLeft = 0f;
                c.HorizontalAlignment = 1;
                th2.AddCell(c);

                PdfPTable thChaveCte = new PdfPTable(1);
                c = new PdfPCell(new Phrase("Número(s) CT-e", fontTextoNormal));
                c.Border = 0;
                c.HorizontalAlignment = 0;
                thChaveCte.AddCell(c);
                // Chave CT-e
                //c = new PdfPCell(new Phrase(cte.Chave, fontTextoNormal));
                string ctesNumeros = "";
                if (ctes != null)
                {
                    foreach (var cteNum in ctes)
                    {
                        if (String.IsNullOrEmpty(ctesNumeros))
                            ctesNumeros = cteNum.Numero;
                        else
                            ctesNumeros = ctesNumeros + ", " + cteNum.Numero;
                    }
                }
                c = new PdfPCell(new Phrase(ctesNumeros, fontTextoNormal));
                c.Border = 0;
                c.HorizontalAlignment = 0;
                thChaveCte.AddCell(c);
                c = new PdfPCell(thChaveCte);
                c.Colspan = 12;
                c.HorizontalAlignment = 0;
                th2.AddCell(c);

                PdfPCell ch2 = new PdfPCell(th2);
                tp.AddCell(ch2);
                // Fim Table Principal Header


                // Table Dados (Origem, Destino, Remetente e Destinatário)
                // Table com 2 colunas
                PdfPTable tDadosPrincipal = new PdfPTable(2);
                tDadosPrincipal.TotalWidth = 500f;
                tDadosPrincipal.LockedWidth = true;
                widths = new float[] { 250f, 250f };
                tDadosPrincipal.SetWidths(widths);


                // Table Lado Esquerdo
                PdfPTable tDadosLadoEsquerdo = new PdfPTable(1);
                tDadosLadoEsquerdo.TotalWidth = 250f;
                tDadosLadoEsquerdo.LockedWidth = true;
                widths = new float[] { 250f };
                tDadosLadoEsquerdo.SetWidths(widths);

                c = new PdfPCell(new Phrase("Origem: " + (fluxoComercial != null ? this.ToTitleCase(fluxoComercial.Origem.Descricao) + " - " + fluxoComercial.Origem.Codigo : ""), fontTextoNormal));
                c.HorizontalAlignment = 0;
                tDadosLadoEsquerdo.AddCell(c);
                c = new PdfPCell(new Phrase("Remetente: " + (empresaRemetente != null ? empresaRemetente.RazaoSocial : ""), fontTextoNormal));
                c.Border = 0;
                c.HorizontalAlignment = 0;
                tDadosLadoEsquerdo.AddCell(c);
                c = new PdfPCell(new Phrase("Endereço: " + (empresaRemetente != null ? this.ToTitleCase(empresaRemetente.Endereco) : ""), fontTextoNormal));
                c.Border = 0;
                c.HorizontalAlignment = 0;
                tDadosLadoEsquerdo.AddCell(c);
                c = new PdfPCell(new Phrase("Município: " + (empresaRemetente != null && (empresaRemetente.CidadeIbge != null) ? this.ToTitleCase(empresaRemetente.CidadeIbge.Descricao) + " - " + empresaRemetente.CidadeIbge.SiglaEstado : "") + " CEP: " + (empresaRemetente != null ? this.FormatarCep(empresaRemetente.Cep) : ""), fontTextoNormal));
                c.Border = 0;
                c.HorizontalAlignment = 0;
                tDadosLadoEsquerdo.AddCell(c);
                c = new PdfPCell(new Phrase("CNPJ: " + (empresaRemetente != null ? this.FormatarCnpj(empresaRemetente.Cgc) : "") + " Inscrição Estadual: " + (empresaRemetente != null ? empresaRemetente.InscricaoEstadual : ""), fontTextoNormal));
                c.Border = 0;
                c.HorizontalAlignment = 0;
                tDadosLadoEsquerdo.AddCell(c);

                PdfPCell cDadosLadoEsquerdo = new PdfPCell(tDadosLadoEsquerdo);

                // Table Lado Direito
                PdfPTable tDadosLadoDireito = new PdfPTable(1);
                tDadosLadoDireito.TotalWidth = 250f;
                tDadosLadoDireito.LockedWidth = true;
                widths = new float[] { 250f };
                tDadosLadoDireito.SetWidths(widths);

                c = new PdfPCell(new Phrase("Destino: " + (fluxoComercial != null ? this.ToTitleCase(fluxoComercial.Destino.Descricao) + " - " + fluxoComercial.Destino.Codigo : ""), fontTextoNormal));
                c.HorizontalAlignment = 0;
                tDadosLadoDireito.AddCell(c);
                c = new PdfPCell(new Phrase("Destinatário: " + (empresaDestinatario != null ? empresaDestinatario.RazaoSocial : ""), fontTextoNormal));
                c.Border = 0;
                c.HorizontalAlignment = 0;
                tDadosLadoDireito.AddCell(c);
                c = new PdfPCell(new Phrase("Endereço: " + (empresaDestinatario != null ? this.ToTitleCase(empresaDestinatario.Endereco) : ""), fontTextoNormal));
                c.Border = 0;
                c.HorizontalAlignment = 0;
                tDadosLadoDireito.AddCell(c);
                c = new PdfPCell(new Phrase("Município: " + (empresaDestinatario != null && (empresaDestinatario.CidadeIbge != null) ? this.ToTitleCase(empresaDestinatario.CidadeIbge.Descricao) + " - " + empresaDestinatario.CidadeIbge.SiglaEstado : "") + " CEP: " + (empresaDestinatario != null ? this.FormatarCep(empresaDestinatario.Cep) : ""), fontTextoNormal));
                c.Border = 0;
                c.HorizontalAlignment = 0;
                tDadosLadoDireito.AddCell(c);
                c = new PdfPCell(new Phrase("CNPJ: " + (empresaDestinatario != null ? this.FormatarCnpj(empresaDestinatario.Cgc) : "") + " Inscrição Estadual: " + (empresaDestinatario != null ? empresaDestinatario.InscricaoEstadual : ""), fontTextoNormal));
                c.Border = 0;
                c.HorizontalAlignment = 0;
                tDadosLadoDireito.AddCell(c);

                PdfPCell cDadosLadoDireito = new PdfPCell(tDadosLadoDireito);

                tDadosPrincipal.AddCell(cDadosLadoEsquerdo);
                tDadosPrincipal.AddCell(cDadosLadoDireito);
                //Fim Table Dados (Origem, Destino, Remetente e Destinatário)



                // Table Dados Expedidor e Recebedor
                // Table com 2 colunas
                PdfPTable tDadosPrincipalExpedidorRecebedor = new PdfPTable(2);
                tDadosPrincipalExpedidorRecebedor.TotalWidth = 500f;
                tDadosPrincipalExpedidorRecebedor.LockedWidth = true;
                widths = new float[] { 250f, 250f };
                tDadosPrincipalExpedidorRecebedor.SetWidths(widths);


                // Table Lado Esquerdo - Expedidor
                PdfPTable tDadosLadoEsquerdoExpedidor = new PdfPTable(1);
                tDadosLadoEsquerdoExpedidor.TotalWidth = 250f;
                tDadosLadoEsquerdoExpedidor.LockedWidth = true;
                widths = new float[] { 250f };
                tDadosLadoEsquerdoExpedidor.SetWidths(widths);

                c = new PdfPCell(new Phrase("Expedidor: " + (empresaExpedidor != null ? empresaExpedidor.RazaoSocial : ""), fontTextoNormal));
                c.Border = 0;
                c.HorizontalAlignment = 0;
                tDadosLadoEsquerdoExpedidor.AddCell(c);
                c = new PdfPCell(new Phrase("Endereço: " + (empresaExpedidor != null ? this.ToTitleCase(empresaExpedidor.Endereco) : ""), fontTextoNormal));
                c.Border = 0;
                c.HorizontalAlignment = 0;
                tDadosLadoEsquerdoExpedidor.AddCell(c);
                c = new PdfPCell(new Phrase("Município: " + (empresaExpedidor != null && (empresaExpedidor.CidadeIbge != null) ? this.ToTitleCase(empresaExpedidor.CidadeIbge.Descricao) + " - " + empresaExpedidor.CidadeIbge.SiglaEstado : "") + " CEP: " + (empresaExpedidor != null ? this.FormatarCep(empresaExpedidor.Cep) : ""), fontTextoNormal));
                c.Border = 0;
                c.HorizontalAlignment = 0;
                tDadosLadoEsquerdoExpedidor.AddCell(c);
                c = new PdfPCell(new Phrase("CNPJ: " + (empresaExpedidor != null ? this.FormatarCnpj(empresaExpedidor.Cgc) : "") + " Inscrição Estadual: " + (empresaExpedidor != null ? empresaExpedidor.InscricaoEstadual : ""), fontTextoNormal));
                c.Border = 0;
                c.HorizontalAlignment = 0;
                tDadosLadoEsquerdoExpedidor.AddCell(c);

                PdfPCell cDadosLadoEsquerdoExpedidor = new PdfPCell(tDadosLadoEsquerdoExpedidor);

                // Table Lado Direito - Recebedor
                PdfPTable tDadosLadoDireitoRecebedor = new PdfPTable(1);
                tDadosLadoDireitoRecebedor.TotalWidth = 250f;
                tDadosLadoDireitoRecebedor.LockedWidth = true;
                widths = new float[] { 250f };
                tDadosLadoDireitoRecebedor.SetWidths(widths);

                c = new PdfPCell(new Phrase("Recebedor: " + (empresaRecebedor != null ? empresaRecebedor.RazaoSocial : ""), fontTextoNormal));
                c.Border = 0;
                c.HorizontalAlignment = 0;
                tDadosLadoDireitoRecebedor.AddCell(c);
                c = new PdfPCell(new Phrase("Endereço: " + (empresaRecebedor != null ? this.ToTitleCase(empresaRecebedor.Endereco) : ""), fontTextoNormal));
                c.Border = 0;
                c.HorizontalAlignment = 0;
                tDadosLadoDireitoRecebedor.AddCell(c);
                c = new PdfPCell(new Phrase("Município: " + (empresaRecebedor != null && (empresaRecebedor.CidadeIbge != null) ? this.ToTitleCase(empresaRecebedor.CidadeIbge.Descricao) + " - " + empresaRecebedor.CidadeIbge.SiglaEstado : "") + " CEP: " + (empresaRecebedor != null ? this.FormatarCep(empresaRecebedor.Cep) : ""), fontTextoNormal));
                c.Border = 0;
                c.HorizontalAlignment = 0;
                tDadosLadoDireitoRecebedor.AddCell(c);
                c = new PdfPCell(new Phrase("CNPJ: " + (empresaRecebedor != null ? this.FormatarCnpj(empresaRecebedor.Cgc) : "") + " Inscrição Estadual: " + (empresaRecebedor != null ? empresaRecebedor.InscricaoEstadual : ""), fontTextoNormal));
                c.Border = 0;
                c.HorizontalAlignment = 0;
                tDadosLadoDireitoRecebedor.AddCell(c);

                PdfPCell cDadosLadoDireitoRecebedor = new PdfPCell(tDadosLadoDireitoRecebedor);

                tDadosPrincipalExpedidorRecebedor.AddCell(cDadosLadoEsquerdoExpedidor);
                tDadosPrincipalExpedidorRecebedor.AddCell(cDadosLadoDireitoRecebedor);
                // Fim Table Dados Expedidor e Recebedor



                // Table Titulo Dados de Carregamento
                PdfPTable tDadosPrincipalCarregamentoTitulo = new PdfPTable(1);
                tDadosPrincipalCarregamentoTitulo.TotalWidth = 500f;
                tDadosPrincipalCarregamentoTitulo.LockedWidth = true;
                widths = new float[] { 500f };
                tDadosPrincipalCarregamentoTitulo.SetWidths(widths);

                c = new PdfPCell(new Phrase("Dados de Carregamento", fontTextoMedioNegrito));
                c.HorizontalAlignment = 1;
                tDadosPrincipalCarregamentoTitulo.AddCell(c);
                // Fim Titulo Dados de Carregamento

                // Table Dados de Carregamento
                // Table com 2 colunas
                PdfPTable tDadosPrincipalCarregamentoDetalhes = new PdfPTable(2);
                tDadosPrincipalCarregamentoDetalhes.TotalWidth = 500f;
                tDadosPrincipalCarregamentoDetalhes.LockedWidth = true;
                widths = new float[] { 250f, 250f };
                tDadosPrincipalCarregamentoDetalhes.SetWidths(widths);

                // Lado Esquerdo (Mercadoria)
                PdfPTable tDadosLadoEsquerdoCarregamentoDetalhes = new PdfPTable(1);
                tDadosLadoEsquerdoCarregamentoDetalhes.TotalWidth = 250f;
                tDadosLadoEsquerdoCarregamentoDetalhes.LockedWidth = true;
                widths = new float[] { 250f };
                tDadosLadoEsquerdoCarregamentoDetalhes.SetWidths(widths);

                c = new PdfPCell(new Phrase("Mercadoria: " + (fluxoComercial != null ? fluxoComercial.Mercadoria.DescricaoDetalhada : ""), fontTextoNormal));
                c.HorizontalAlignment = 0;
                tDadosLadoEsquerdoCarregamentoDetalhes.AddCell(c);

                PdfPCell cDadosLadoEsquerdoCarregamentoDetalhes = new PdfPCell(tDadosLadoEsquerdoCarregamentoDetalhes);

                // Lado Direito
                PdfPTable tDadosLadoDireitoCarregamentoDetalhes = new PdfPTable(3);
                tDadosLadoDireitoCarregamentoDetalhes.TotalWidth = 250f;
                tDadosLadoDireitoCarregamentoDetalhes.LockedWidth = true;
                widths = new float[] { 75f, 90f, 85f };
                tDadosLadoDireitoCarregamentoDetalhes.SetWidths(widths);

                c = new PdfPCell(new Phrase("Série Vagão: " + dcl.SerieVagao, fontTextoNormal));
                c.HorizontalAlignment = 0;
                tDadosLadoDireitoCarregamentoDetalhes.AddCell(c);

                c = new PdfPCell(new Phrase("Cód. Vagão: " + dcl.CodigoVagao, fontTextoNormal));
                c.HorizontalAlignment = 0;
                tDadosLadoDireitoCarregamentoDetalhes.AddCell(c);

                c = new PdfPCell(new Phrase("Peso Líq.: " + dcl.PesoLiquido.ToString("N3"), fontTextoNormal));
                c.HorizontalAlignment = 0;
                tDadosLadoDireitoCarregamentoDetalhes.AddCell(c);

                PdfPCell cDadosLadoDireitoCarregamentoDetalhes = new PdfPCell(tDadosLadoDireitoCarregamentoDetalhes);

                tDadosPrincipalCarregamentoDetalhes.AddCell(cDadosLadoEsquerdoCarregamentoDetalhes);
                tDadosPrincipalCarregamentoDetalhes.AddCell(cDadosLadoDireitoCarregamentoDetalhes);
                // Fim Table Dados de Carregamento

                // Table Documentos Originários Título
                PdfPTable tDadosPrincipalDocsTitulo = new PdfPTable(1);
                tDadosPrincipalDocsTitulo.TotalWidth = 500f;
                tDadosPrincipalDocsTitulo.LockedWidth = true;
                widths = new float[] { 500f };
                tDadosPrincipalDocsTitulo.SetWidths(widths);

                c = new PdfPCell(new Phrase("Documentos Originários", fontTextoMedioNegrito));
                c.HorizontalAlignment = 1;
                tDadosPrincipalDocsTitulo.AddCell(c);
                // Fim Table Documentos Originários Título

                // Table Documentos Originários Valores (Tipo, CNPJ Emitente, Número, Chave, Peso, Volume, Conteiner)
                PdfPTable tDadosPrincipaDocsOriginarios = new PdfPTable(1);
                tDadosPrincipaDocsOriginarios.TotalWidth = 500f;
                tDadosPrincipaDocsOriginarios.LockedWidth = true;
                widths = new float[] { 500f };
                tDadosPrincipaDocsOriginarios.SetWidths(widths);

                PdfPTable tDadosDocsOriginarios = new PdfPTable(7);
                tDadosDocsOriginarios.TotalWidth = 500f;
                tDadosDocsOriginarios.LockedWidth = true;
                widths = new float[] { 30f, 85f, 40f, 205f, 40f, 40f, 60f };
                tDadosDocsOriginarios.SetWidths(widths);

                c = new PdfPCell(new Phrase("Tipo: ", fontTextoNormal));
                c.HorizontalAlignment = 0;
                c.UseVariableBorders = true;
                c.BorderWidthTop = 0f;
                c.BorderWidthBottom = 0f;
                c.BorderWidthLeft = 0f;
                tDadosDocsOriginarios.AddCell(c);

                c = new PdfPCell(new Phrase("CNPJ emitente: ", fontTextoNormal));
                c.HorizontalAlignment = 0;
                c.UseVariableBorders = true;
                c.BorderWidthTop = 0f;
                c.BorderWidthBottom = 0f;
                c.BorderWidthLeft = 0f;
                tDadosDocsOriginarios.AddCell(c);

                c = new PdfPCell(new Phrase("Número: ", fontTextoNormal));
                c.HorizontalAlignment = 0;
                c.UseVariableBorders = true;
                c.BorderWidthTop = 0f;
                c.BorderWidthBottom = 0f;
                c.BorderWidthLeft = 0f;
                tDadosDocsOriginarios.AddCell(c);

                c = new PdfPCell(new Phrase("Chave: ", fontTextoNormal));
                c.HorizontalAlignment = 0;
                c.UseVariableBorders = true;
                c.BorderWidthTop = 0f;
                c.BorderWidthBottom = 0f;
                c.BorderWidthLeft = 0f;
                tDadosDocsOriginarios.AddCell(c);

                c = new PdfPCell(new Phrase("Peso: ", fontTextoNormal));
                c.HorizontalAlignment = 0;
                c.UseVariableBorders = true;
                c.BorderWidthTop = 0f;
                c.BorderWidthBottom = 0f;
                c.BorderWidthLeft = 0f;
                tDadosDocsOriginarios.AddCell(c);

                c = new PdfPCell(new Phrase("Volume: ", fontTextoNormal));
                c.HorizontalAlignment = 0;
                c.UseVariableBorders = true;
                c.BorderWidthTop = 0f;
                c.BorderWidthBottom = 0f;
                c.BorderWidthLeft = 0f;
                tDadosDocsOriginarios.AddCell(c);

                c = new PdfPCell(new Phrase("Conteiner: ", fontTextoNormal));
                c.HorizontalAlignment = 0;
                c.UseVariableBorders = true;
                c.BorderWidthTop = 0f;
                c.BorderWidthBottom = 0f;
                c.BorderWidthLeft = 0f;
                c.BorderWidthRight = 0f;
                tDadosDocsOriginarios.AddCell(c);

                if (dclDocumentos != null)
                {
                    if (dclDocumentos.Count > 0)
                    {
                        foreach (var dclDoc in dclDocumentos)
                        {
                            string modeloNota = fluxoComercial.ModeloNotaFiscal;
                            modeloNota = (modeloNota == "55" ? "NF-e" : "NF");

                            c = new PdfPCell(new Phrase(modeloNota, fontTextoNormal));
                            c.HorizontalAlignment = 0;
                            c.UseVariableBorders = true;
                            c.BorderWidthTop = 0f;
                            c.BorderWidthBottom = 0f;
                            c.BorderWidthLeft = 0f;
                            tDadosDocsOriginarios.AddCell(c);

                            c = new PdfPCell(new Phrase(this.FormatarCnpj(dclDoc.CnpjEmitente), fontTextoNormal));
                            c.HorizontalAlignment = 0;
                            c.UseVariableBorders = true;
                            c.BorderWidthTop = 0f;
                            c.BorderWidthBottom = 0f;
                            c.BorderWidthLeft = 0f;
                            tDadosDocsOriginarios.AddCell(c);

                            c = new PdfPCell(new Phrase(dclDoc.Numero, fontTextoNormal));
                            c.HorizontalAlignment = 0;
                            c.UseVariableBorders = true;
                            c.BorderWidthTop = 0f;
                            c.BorderWidthBottom = 0f;
                            c.BorderWidthLeft = 0f;
                            tDadosDocsOriginarios.AddCell(c);

                            c = new PdfPCell(new Phrase(dclDoc.ChaveNfe, fontTextoNormal));
                            c.HorizontalAlignment = 0;
                            c.UseVariableBorders = true;
                            c.BorderWidthTop = 0f;
                            c.BorderWidthBottom = 0f;
                            c.BorderWidthLeft = 0f;
                            tDadosDocsOriginarios.AddCell(c);

                            c = new PdfPCell(new Phrase(string.Format("{0:N3}", dclDoc.Peso), fontTextoNormal));
                            c.HorizontalAlignment = 0;
                            c.UseVariableBorders = true;
                            c.BorderWidthTop = 0f;
                            c.BorderWidthBottom = 0f;
                            c.BorderWidthLeft = 0f;
                            tDadosDocsOriginarios.AddCell(c);

                            c = new PdfPCell(new Phrase(string.Format("{0:N3}", dclDoc.Volume), fontTextoNormal));
                            c.HorizontalAlignment = 0;
                            c.UseVariableBorders = true;
                            c.BorderWidthTop = 0f;
                            c.BorderWidthBottom = 0f;
                            c.BorderWidthLeft = 0f;
                            tDadosDocsOriginarios.AddCell(c);

                            c = new PdfPCell(new Phrase(dclDoc.Conteiner, fontTextoNormal));
                            c.HorizontalAlignment = 0;
                            c.UseVariableBorders = true;
                            c.BorderWidthTop = 0f;
                            c.BorderWidthBottom = 0f;
                            c.BorderWidthLeft = 0f;
                            c.BorderWidthRight = 0f;
                            tDadosDocsOriginarios.AddCell(c);
                        }
                    }

                    PdfPCell cDadosDocsOriginarios =
                        new PdfPCell(tDadosDocsOriginarios);
                    tDadosPrincipaDocsOriginarios.AddCell(cDadosDocsOriginarios);
                }

                // Fim Table Documentos Originários Valores (Tipo, CNPJ Emitente, Número, Chave, Peso, Volume, Conteiner)



                // Table Observações Título
                PdfPTable tDadosPrincipalObservacoesTitulo = new PdfPTable(1);
                tDadosPrincipalObservacoesTitulo.TotalWidth = 500f;
                tDadosPrincipalObservacoesTitulo.LockedWidth = true;
                widths = new float[] { 500f };
                tDadosPrincipalObservacoesTitulo.SetWidths(widths);

                c = new PdfPCell(new Phrase("Observações", fontTextoMedioNegrito));
                c.HorizontalAlignment = 1;
                tDadosPrincipalObservacoesTitulo.AddCell(c);
                // Fim Table Observações Título


                // Table Observações Dados
                PdfPTable tDadosPrincipalObservacoesDadosPai = new PdfPTable(1);
                tDadosPrincipalObservacoesDadosPai.TotalWidth = 500f;
                tDadosPrincipalObservacoesDadosPai.LockedWidth = true;
                widths = new float[] { 500f };
                tDadosPrincipalObservacoesDadosPai.SetWidths(widths);

                PdfPTable tDadosPrincipalObservacoesDados = new PdfPTable(1);
                tDadosPrincipalObservacoesDados.TotalWidth = 500f;
                tDadosPrincipalObservacoesDados.LockedWidth = true;
                widths = new float[] { 500f };
                tDadosPrincipalObservacoesDados.SetWidths(widths);

                c = new PdfPCell(new Phrase("Faturado por: " + dcl.Observacao, fontTextoNormal));
                c.HorizontalAlignment = 0;
                c.Border = 0;
                tDadosPrincipalObservacoesDados.AddCell(c);
                c = new PdfPCell(new Phrase(" ", fontTextoNormal));
                c.HorizontalAlignment = 0;
                c.Border = 0;
                tDadosPrincipalObservacoesDados.AddCell(c);


                PdfPCell cDadosObservacoes =
                        new PdfPCell(tDadosPrincipalObservacoesDados);

                tDadosPrincipalObservacoesDadosPai.AddCell(cDadosObservacoes);
                // Fim Table Observações Dados



                // Table Linha Vazia
                PdfPTable tLinhaVazia = new PdfPTable(1);
                tLinhaVazia.TotalWidth = 500f;
                tLinhaVazia.LockedWidth = true;
                widths = new float[] { 500f };
                tLinhaVazia.SetWidths(widths);

                c = new PdfPCell(new Phrase(" ", fontTextoNormal));
                c.HorizontalAlignment = 1;
                c.Border = 0;
                tLinhaVazia.AddCell(c);

                c = new PdfPCell(new Phrase(" ", fontTextoNormal));
                c.HorizontalAlignment = 1;
                c.Border = 0;
                tLinhaVazia.AddCell(c);
                // Fim Table Linha Vazia


                // Adiciona Tables principais
                document.Add(tp);
                document.Add(tDadosPrincipal);
                document.Add(tDadosPrincipalExpedidorRecebedor);
                document.Add(tDadosPrincipalCarregamentoTitulo);
                document.Add(tDadosPrincipalCarregamentoDetalhes);
                document.Add(tDadosPrincipalDocsTitulo);
                document.Add(tDadosPrincipaDocsOriginarios);
                document.Add(tDadosPrincipalObservacoesTitulo);
                document.Add(tDadosPrincipalObservacoesDadosPai);
                document.Add(tLinhaVazia);
            }

            document.Close();
            document.Dispose();

            StreamReader sr = new StreamReader(tempFilePath);
            MemoryStream ms = new MemoryStream();
            StreamUtils.Copy(sr.BaseStream, ms, new byte[4096]);
            sr.Close();
            File.Delete(tempFilePath);
            ms.Seek(0, SeekOrigin.Begin);
            return ms;
        }

        /// <summary>
        /// Gera Laudo da Mercadoria em PDF
        /// </summary>
        /// <param name="impressao">Classe de impressao a ser gerada o PDF</param>
        /// <returns>Stream do pdf</returns>
        public Stream GerarLaudoMercadoriaPdf(LaudoMercadoriaImpressao impressao)
        {
            string tempFilePath = Path.GetTempFileName();
            var document = new Document(PageSize.A4, 0, 0, 85, 30);
            PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(tempFilePath, FileMode.Create));

            document.Open();

            // Definição da Fonte
            var fontTextoNormal = new Font(Font.FontFamily.HELVETICA, 8f, Font.NORMAL, BaseColor.BLACK);
            var fontTextoNormalNegrito = new Font(Font.FontFamily.HELVETICA, 8f, Font.BOLD, BaseColor.BLACK);
            var fontTextoGrandeNegrito = new Font(Font.FontFamily.HELVETICA, 12f, Font.BOLD, BaseColor.BLACK);
            var fontTextoMedioNegrito = new Font(Font.FontFamily.HELVETICA, 10f, Font.BOLD, BaseColor.BLACK);
            var fontTextoPequenoNegrito = new Font(Font.FontFamily.HELVETICA, 8f, Font.BOLD, BaseColor.BLACK);

            Phrase informacao;

            #region Header

            // Table Principal - Header
            var header = new PdfPTable(1);
            header.TotalWidth = 500f;
            header.LockedWidth = true;
            var widths = new float[]
                                 {
                                     500f  // Informações da empresa geradora do laudo de produtos
                                 };
            header.SetWidths(widths);

            var c = new PdfPCell();

            #region Informações Empresa Geradora do Laudo

            // Célula 1 (table com 2 colunas)
            var th2 = new PdfPTable(2);
            th2.TotalWidth = 500f;
            th2.LockedWidth = true;
            widths = new float[] { 250f, 250f };
            th2.SetWidths(widths);

            PdfPCell empresaCell;
            if (impressao.EmpresaGeradora.Logo != null && impressao.EmpresaGeradora.Logo.Length > 0)
            {
                var empresaLogo = Image.GetInstance(impressao.EmpresaGeradora.Logo);
                empresaLogo.ScaleToFit(160f, 80f);
                empresaCell = new PdfPCell(empresaLogo);
            }
            else
            {
                empresaCell = new PdfPCell();
            }

            empresaCell.Border = 0;
            empresaCell.HorizontalAlignment = 1;
            empresaCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            th2.AddCell(empresaCell);

            var thEmpresa = new PdfPTable(1);
            var empresa = impressao.EmpresaGeradora;

            empresaCell = new PdfPCell(new Phrase(empresa.Nome, fontTextoMedioNegrito));
            empresaCell.Border = 0;
            empresaCell.HorizontalAlignment = 1;
            thEmpresa.AddCell(empresaCell);

            empresaCell = new PdfPCell(new Phrase((!String.IsNullOrEmpty(empresa.Endereco) ? this.ToTitleCase(empresa.Endereco) : String.Empty), fontTextoNormal));
            empresaCell.Border = 0;
            empresaCell.HorizontalAlignment = 1;
            thEmpresa.AddCell(empresaCell);

            empresaCell = new PdfPCell(new Phrase((!String.IsNullOrEmpty(empresa.CidadeDescricao) && !String.IsNullOrEmpty(empresa.EstadoSigla) ? this.ToTitleCase(empresa.CidadeDescricao) + " - " + empresa.EstadoSigla + " - " : String.Empty) + "CEP: " + empresa.Cep, fontTextoNormal));
            empresaCell.Border = 0;
            empresaCell.HorizontalAlignment = 1;
            thEmpresa.AddCell(empresaCell);

            empresaCell = new PdfPCell(new Phrase("CNPJ: " + empresa.CnpjFormatado, fontTextoNormal));
            empresaCell.Border = 0;
            empresaCell.HorizontalAlignment = 1;
            thEmpresa.AddCell(empresaCell);

            if (!String.IsNullOrEmpty(empresa.InscricaoEstadual))
            {
                empresaCell = new PdfPCell(new Phrase("IE: " + empresa.InscricaoEstadual, fontTextoNormal));
                empresaCell.Border = 0;
                empresaCell.HorizontalAlignment = 1;
                thEmpresa.AddCell(empresaCell);
            }

            if (!String.IsNullOrEmpty(empresa.Telefone))
            {
                empresaCell = new PdfPCell(new Phrase("Telefone: " + empresa.Telefone, fontTextoNormal));
                empresaCell.Border = 0;
                empresaCell.HorizontalAlignment = 1;
                thEmpresa.AddCell(empresaCell);
            }

            if (!String.IsNullOrEmpty(empresa.Fax))
            {
                empresaCell = new PdfPCell(new Phrase("Fax: " + empresa.Fax, fontTextoNormal));
                empresaCell.Border = 0;
                empresaCell.HorizontalAlignment = 1;
                thEmpresa.AddCell(empresaCell);
            }

            empresaCell = new PdfPCell(thEmpresa);
            empresaCell.Border = 0;
            empresaCell.HorizontalAlignment = 1;
            th2.AddCell(empresaCell);

            var ch2 = new PdfPCell(th2);
            ch2.Border = 0;
            header.AddCell(ch2);

            #endregion Informações Empresa Geradora do Laudo

            #endregion Header

            // Table Linha Vazia
            PdfPTable tLinhaVaziaHeaderBody = new PdfPTable(1);
            tLinhaVaziaHeaderBody.TotalWidth = 500f;
            tLinhaVaziaHeaderBody.LockedWidth = true;
            widths = new float[] { 500f };
            tLinhaVaziaHeaderBody.SetWidths(widths);

            c = new PdfPCell(new Phrase(" ", fontTextoNormal));
            c.HorizontalAlignment = 0;
            c.Border = 0;
            tLinhaVaziaHeaderBody.AddCell(c);

            #region Body

            #region Carregamento

            // Table Titulo Dados do carregamento
            PdfPTable tDadosCarregamentoTitulo = new PdfPTable(1);
            tDadosCarregamentoTitulo.TotalWidth = 500f;
            tDadosCarregamentoTitulo.LockedWidth = true;
            widths = new float[] { 500f };
            tDadosCarregamentoTitulo.SetWidths(widths);

            c = new PdfPCell(new Phrase("Dados Carregamento", fontTextoMedioNegrito));
            c.HorizontalAlignment = Element.ALIGN_CENTER;
            c.BackgroundColor = BaseColor.GRAY;
            tDadosCarregamentoTitulo.AddCell(c);
            // Fim Titulo Dados de Carregamento

            PdfPTable tDadosPrincipalCarregamentoDetalhes = new PdfPTable(2);
            tDadosPrincipalCarregamentoDetalhes.TotalWidth = 500f;
            tDadosPrincipalCarregamentoDetalhes.LockedWidth = true;
            widths = new float[] { 200f, 300f };
            tDadosPrincipalCarregamentoDetalhes.SetWidths(widths);

            PdfPTable tDadosCarregamentoEsquerdoDetalhes = new PdfPTable(2);
            tDadosCarregamentoEsquerdoDetalhes.TotalWidth = 200f;
            tDadosCarregamentoEsquerdoDetalhes.LockedWidth = true;
            widths = new float[] { 90f, 110f };
            tDadosCarregamentoEsquerdoDetalhes.SetWidths(widths);

            informacao = new Phrase();
            informacao.Add(new Phrase("Cod. Vagão: ", fontTextoNormalNegrito));
            informacao.Add(new Phrase(impressao.CodigoVagao, fontTextoNormal));
            c = new PdfPCell(informacao);
            c.HorizontalAlignment = Element.ALIGN_CENTER;
            c.BorderWidthTop = 0f;
            c.BorderWidthRight = 0f;
            tDadosCarregamentoEsquerdoDetalhes.AddCell(c);

            informacao = new Phrase();
            informacao.Add(new Phrase("Peso: ", fontTextoNormalNegrito));
            informacao.Add(new Phrase(String.Format("{0:N3} TON", impressao.PesoLiquido), fontTextoNormal));
            c = new PdfPCell(informacao);
            c.HorizontalAlignment = Element.ALIGN_CENTER;
            c.BorderWidthTop = 0f;
            c.BorderWidthRight = 0f;
            c.BorderWidthLeft = 0f;
            tDadosCarregamentoEsquerdoDetalhes.AddCell(c);

            PdfPCell cDadosLadoEsquerdoCarregamentoDetalhes = new PdfPCell(tDadosCarregamentoEsquerdoDetalhes);
            cDadosLadoEsquerdoCarregamentoDetalhes.BorderWidthRight = 0f;
            cDadosLadoEsquerdoCarregamentoDetalhes.BorderWidthBottom = 0f;

            PdfPTable tDadosCarregamentoDireitoDetalhes = new PdfPTable(1);
            tDadosCarregamentoDireitoDetalhes.TotalWidth = 300f;
            tDadosCarregamentoDireitoDetalhes.LockedWidth = true;
            widths = new float[] { 300f };
            tDadosCarregamentoDireitoDetalhes.SetWidths(widths);

            if (!String.IsNullOrEmpty(impressao.ChaveNfeFormatada))
            {
                informacao = new Phrase();
                informacao.Add(new Phrase("Chave NF-e: ", fontTextoNormalNegrito));
                informacao.Add(new Phrase(impressao.ChaveNfeFormatada, fontTextoNormal));
            }
            else if (!String.IsNullOrEmpty(impressao.NumeroNfe))
            {
                var quantidadeNotasFiscais = impressao.NumeroNfe.Split('/').Length;
                informacao = new Phrase();
                informacao.Add(new Phrase(quantidadeNotasFiscais == 1 ? "Número NF-e: " : "Números NF-es: ", fontTextoNormalNegrito));
                informacao.Add(new Phrase(impressao.NumeroNfe, fontTextoNormal));
            }
            else
            {
                informacao = new Phrase();
                informacao.Add(new Phrase("NF-e: ", fontTextoNormalNegrito));
                informacao.Add(new Phrase("Não Informada", fontTextoNormal));
            }

            c = new PdfPCell(informacao);
            c.HorizontalAlignment = Element.ALIGN_CENTER;
            c.BorderWidthTop = 0f;
            c.BorderWidthLeft = 0f;
            tDadosCarregamentoDireitoDetalhes.AddCell(c);

            PdfPCell cDadosLadoDireitoCarregamentoDetalhes = new PdfPCell(tDadosCarregamentoDireitoDetalhes);
            cDadosLadoDireitoCarregamentoDetalhes.BorderWidthBottom = 0f;
            cDadosLadoDireitoCarregamentoDetalhes.BorderWidthLeft = 0f;

            tDadosPrincipalCarregamentoDetalhes.AddCell(cDadosLadoEsquerdoCarregamentoDetalhes);
            tDadosPrincipalCarregamentoDetalhes.AddCell(cDadosLadoDireitoCarregamentoDetalhes);

            #endregion Carregamento

            #region Dados do Laudo

            // Table Titulo Dados do Laudo
            PdfPTable tDadosLaudoTitulo = new PdfPTable(1);
            tDadosLaudoTitulo.TotalWidth = 500f;
            tDadosLaudoTitulo.LockedWidth = true;
            widths = new float[] { 500f };
            tDadosLaudoTitulo.SetWidths(widths);

            c = new PdfPCell(new Phrase("Dados Gerais", fontTextoMedioNegrito));
            c.HorizontalAlignment = Element.ALIGN_CENTER;
            c.BackgroundColor = BaseColor.GRAY;
            tDadosLaudoTitulo.AddCell(c);
            // Fim Titulo Dados do Laudo

            PdfPTable tDadosPrincipalLaudoDetalhes = new PdfPTable(4);
            tDadosPrincipalLaudoDetalhes.TotalWidth = 500f;
            tDadosPrincipalLaudoDetalhes.LockedWidth = true;
            widths = new float[] { 125f, 125f, 125f, 125f };
            tDadosPrincipalLaudoDetalhes.SetWidths(widths);

            informacao = new Phrase();
            informacao.Add(new Phrase("Produto: ", fontTextoNormalNegrito));
            informacao.Add(new Phrase(impressao.Produto, fontTextoNormal));
            c = new PdfPCell(informacao);
            c.HorizontalAlignment = Element.ALIGN_LEFT;
            c.BorderWidthTop = 0f;
            c.BorderWidthRight = 0f;
            tDadosPrincipalLaudoDetalhes.AddCell(c);

            informacao = new Phrase();
            informacao.Add(new Phrase("Tipo Produto: ", fontTextoNormalNegrito));
            informacao.Add(new Phrase(impressao.DescricaoProduto, fontTextoNormal));
            c = new PdfPCell(informacao);
            c.HorizontalAlignment = Element.ALIGN_LEFT;
            c.BorderWidthTop = 0f;
            c.BorderWidthRight = 0f;
            c.BorderWidthLeft = 0f;
            tDadosPrincipalLaudoDetalhes.AddCell(c);

            informacao = new Phrase();
            informacao.Add(new Phrase("Tipo Laudo: ", fontTextoNormalNegrito));
            informacao.Add(new Phrase(impressao.TipoLaudo, fontTextoNormal));
            c = new PdfPCell(informacao);
            c.HorizontalAlignment = Element.ALIGN_LEFT;
            c.BorderWidthTop = 0f;
            c.BorderWidthRight = 0f;
            c.BorderWidthLeft = 0f;
            tDadosPrincipalLaudoDetalhes.AddCell(c);

            informacao = new Phrase();
            informacao.Add(new Phrase("Número Laudo: ", fontTextoNormalNegrito));
            informacao.Add(new Phrase(impressao.NumeroLaudo, fontTextoNormal));
            c = new PdfPCell(informacao);
            c.HorizontalAlignment = Element.ALIGN_LEFT;
            c.BorderWidthTop = 0f;
            c.BorderWidthLeft = 0f;
            tDadosPrincipalLaudoDetalhes.AddCell(c);

            informacao = new Phrase();
            informacao.Add(new Phrase("Cliente: ", fontTextoNormalNegrito));
            informacao.Add(new Phrase(impressao.Cliente.Nome, fontTextoNormal));
            c = new PdfPCell(informacao);
            c.HorizontalAlignment = Element.ALIGN_LEFT;
            c.BorderWidthTop = 0f;
            c.BorderWidthRight = 0f;
            tDadosPrincipalLaudoDetalhes.AddCell(c);

            informacao = new Phrase();
            informacao.Add(new Phrase("CNPJ: ", fontTextoNormalNegrito));
            informacao.Add(new Phrase(impressao.Cliente.CnpjFormatado, fontTextoNormal));
            c = new PdfPCell(informacao);
            c.HorizontalAlignment = Element.ALIGN_LEFT;
            c.BorderWidthTop = 0f;
            c.BorderWidthRight = 0f;
            c.BorderWidthLeft = 0f;
            tDadosPrincipalLaudoDetalhes.AddCell(c);

            informacao = new Phrase();
            informacao.Add(new Phrase("Classificador: ", fontTextoNormalNegrito));
            informacao.Add(new Phrase(impressao.Classificador.Nome, fontTextoNormal));
            c = new PdfPCell(informacao);
            c.HorizontalAlignment = Element.ALIGN_LEFT;
            c.BorderWidthTop = 0f;
            c.BorderWidthRight = 0f;
            c.BorderWidthLeft = 0f;
            tDadosPrincipalLaudoDetalhes.AddCell(c);

            informacao = new Phrase();
            informacao.Add(new Phrase("Data Classificação: ", fontTextoNormalNegrito));
            informacao.Add(new Phrase(impressao.DataClassificacao.ToShortDateString(), fontTextoNormal));
            c = new PdfPCell(informacao);
            c.HorizontalAlignment = Element.ALIGN_JUSTIFIED;
            c.BorderWidthTop = 0f;
            c.BorderWidthLeft = 0f;
            tDadosPrincipalLaudoDetalhes.AddCell(c);

            informacao = new Phrase();
            informacao.Add(new Phrase("Empresa: ", fontTextoNormalNegrito));
            informacao.Add(new Phrase(impressao.EmpresaNome, fontTextoNormal));
            c = new PdfPCell(informacao);
            c.HorizontalAlignment = Element.ALIGN_LEFT;
            c.BorderWidthTop = 0f;
            c.BorderWidthRight = 0f;
            tDadosPrincipalLaudoDetalhes.AddCell(c);

            informacao = new Phrase();
            informacao.Add(new Phrase("Autorizado Por: ", fontTextoNormalNegrito));
            informacao.Add(new Phrase(impressao.AutorizadoPor, fontTextoNormal));
            c = new PdfPCell(informacao);
            c.HorizontalAlignment = Element.ALIGN_LEFT;
            c.BorderWidthTop = 0f;
            c.BorderWidthRight = 0f;
            c.BorderWidthLeft = 0f;
            tDadosPrincipalLaudoDetalhes.AddCell(c);

            informacao = new Phrase();
            informacao.Add(new Phrase("Destino: ", fontTextoNormalNegrito));
            informacao.Add(new Phrase(impressao.Destino, fontTextoNormal));
            c = new PdfPCell(informacao);
            c.HorizontalAlignment = Element.ALIGN_LEFT;
            c.BorderWidthTop = 0f;
            c.BorderWidthRight = 0f;
            c.BorderWidthLeft = 0f;
            tDadosPrincipalLaudoDetalhes.AddCell(c);

            informacao = new Phrase();
            informacao.Add(new Phrase("Ordem de Serviço: ", fontTextoNormalNegrito));
            informacao.Add(new Phrase(impressao.NumeroOsLaudo, fontTextoNormal));
            c = new PdfPCell(informacao);
            c.HorizontalAlignment = Element.ALIGN_LEFT;
            c.BorderWidthTop = 0f;
            c.BorderWidthLeft = 0f;
            tDadosPrincipalLaudoDetalhes.AddCell(c);


            #endregion Dados do Laudo

            #region Classificações

            // Table Titulo Dados do carregamento
            PdfPTable tDadosClassificacoesTitulo = new PdfPTable(1);
            tDadosClassificacoesTitulo.TotalWidth = 500f;
            tDadosClassificacoesTitulo.LockedWidth = true;
            widths = new float[] { 500f };
            tDadosClassificacoesTitulo.SetWidths(widths);

            c = new PdfPCell(new Phrase("Classificações", fontTextoMedioNegrito));
            c.HorizontalAlignment = Element.ALIGN_CENTER;
            c.BackgroundColor = BaseColor.GRAY;
            tDadosClassificacoesTitulo.AddCell(c);
            // Fim Titulo Dados de Carregamento

            // Table Classificações Valores (Classificação, Quantidade (%))
            PdfPTable tDadosPrincipalClassificacoes = new PdfPTable(1);
            tDadosPrincipalClassificacoes.TotalWidth = 500f;
            tDadosPrincipalClassificacoes.LockedWidth = true;
            widths = new float[] { 500f };
            tDadosPrincipalClassificacoes.SetWidths(widths);

            PdfPTable tDadosClassificacoes = new PdfPTable(2);
            tDadosClassificacoes.TotalWidth = 500f;
            tDadosClassificacoes.LockedWidth = true;
            widths = new float[] { 350f, 150f };
            tDadosClassificacoes.SetWidths(widths);

            c = new PdfPCell(new Phrase("Classificação", fontTextoNormalNegrito));
            c.HorizontalAlignment = Element.ALIGN_CENTER;
            c.UseVariableBorders = true;
            c.BorderWidthTop = 0f;
            c.BorderWidthBottom = 0f;
            c.BorderWidthLeft = 0f;
            c.BorderWidthRight = 0f;
            c.BackgroundColor = BaseColor.WHITE;
            tDadosClassificacoes.AddCell(c);

            c = new PdfPCell(new Phrase("Total (%)", fontTextoNormalNegrito));
            c.HorizontalAlignment = Element.ALIGN_CENTER;
            c.UseVariableBorders = true;
            c.BorderWidthTop = 0f;
            c.BorderWidthBottom = 0f;
            c.BorderWidthLeft = 0f;
            c.BorderWidthRight = 0f;
            c.BackgroundColor = BaseColor.WHITE;
            tDadosClassificacoes.AddCell(c);

            var contadorLinha = 0;
            foreach (var classificacao in impressao.Classificacoes.OrderBy(cla => cla.Ordem).ThenBy(cla => cla.Descricao).ToList())
            {
                c = new PdfPCell(new Phrase(classificacao.Descricao, fontTextoNormal));
                c.HorizontalAlignment = Element.ALIGN_CENTER;
                c.UseVariableBorders = true;
                c.BorderWidthTop = 0.5f;
                c.BorderWidthBottom = 0f;
                c.BorderWidthLeft = 0f;
                c.BorderWidthRight = 0f;
                c.BackgroundColor = contadorLinha % 2 == 0 ? BaseColor.LIGHT_GRAY : BaseColor.WHITE;
                tDadosClassificacoes.AddCell(c);

                c = new PdfPCell(new Phrase(classificacao.Porcentagem.ToString("N2"), fontTextoNormal));
                c.HorizontalAlignment = Element.ALIGN_CENTER;
                c.UseVariableBorders = true;
                c.BorderWidthTop = 0.5f;
                c.BorderWidthBottom = 0f;
                c.BorderWidthLeft = 0f;
                c.BorderWidthRight = 0f;
                c.BackgroundColor = contadorLinha % 2 == 0 ? BaseColor.LIGHT_GRAY : BaseColor.WHITE;
                tDadosClassificacoes.AddCell(c);

                contadorLinha++;
            }

            PdfPCell cDadosClassificacoes = new PdfPCell(tDadosClassificacoes);
            tDadosPrincipalClassificacoes.AddCell(cDadosClassificacoes);

            #endregion

            #endregion Body

            #region Footer

            // Table Linha Vazia
            PdfPTable tLinhaVaziaFooter = new PdfPTable(1);
            tLinhaVaziaFooter.TotalWidth = 500f;
            tLinhaVaziaFooter.LockedWidth = true;
            widths = new float[] { 500f };
            tLinhaVaziaFooter.SetWidths(widths);

            c = new PdfPCell(new Phrase(" ", fontTextoNormal));
            c.HorizontalAlignment = 1;
            c.Border = 0;
            tLinhaVaziaFooter.AddCell(c);

            var tLinhaVaziaDadosGerais = new PdfPTable(1);
            tLinhaVaziaDadosGerais.TotalWidth = 500f;
            tLinhaVaziaDadosGerais.LockedWidth = true;
            widths = new float[] { 500f };
            tLinhaVaziaDadosGerais.SetWidths(widths);

            c = new PdfPCell(new Phrase(" ", fontTextoNormal));
            c.HorizontalAlignment = 1;
            c.Border = 0;
            tLinhaVaziaDadosGerais.AddCell(c);

            var tLinhaVaziaClassificacoes = new PdfPTable(1);
            tLinhaVaziaClassificacoes.TotalWidth = 500f;
            tLinhaVaziaClassificacoes.LockedWidth = true;
            widths = new float[] { 500f };
            tLinhaVaziaClassificacoes.SetWidths(widths);

            c = new PdfPCell(new Phrase(" ", fontTextoNormal));
            c.HorizontalAlignment = 1;
            c.Border = 0;
            tLinhaVaziaClassificacoes.AddCell(c);

            #endregion Footer

            // Adiciona Tables principais
            document.Add(header);
            document.Add(tLinhaVaziaHeaderBody);
            document.Add(tDadosCarregamentoTitulo);
            document.Add(tDadosPrincipalCarregamentoDetalhes);
            document.Add(tLinhaVaziaDadosGerais);
            document.Add(tDadosLaudoTitulo);
            document.Add(tDadosPrincipalLaudoDetalhes);
            document.Add(tLinhaVaziaClassificacoes);
            document.Add(tDadosClassificacoesTitulo);
            document.Add(tDadosPrincipalClassificacoes);
            document.Add(tLinhaVaziaFooter);

            document.Close();
            document.Dispose();

            var sr = new StreamReader(tempFilePath);
            var ms = new MemoryStream();
            StreamUtils.Copy(sr.BaseStream, ms, new byte[4096]);
            sr.Close();
            File.Delete(tempFilePath);
            ms.Seek(0, SeekOrigin.Begin);
            return ms;
        }

        /// <summary>
        /// Gera Ticket de Pesagem individual em PDF
        /// </summary>
        /// <param name="html">ctes - lista de ctes a serem impressos em PDF</param>
        /// <returns>Stream do pdf</returns>
        public Stream GerarTicketPdfIndividual(NotaTicketVagao notaTicketVagao,
                                               IList<ItemDespachoNotaTicketVagaoDto> notasTickets,
                                               IList<NotaTicketDto> TicketNotas,
                                               string DescricaoCliente)
        {

            string tempFilePath = Path.GetTempFileName();
            Document document = new Document(PageSize.A4, 0, 0, 85, 30);
            float ticketSize = 220f;

            PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(tempFilePath, FileMode.Create));

            document.Open();

            // Definição das Fontes
            iTextSharp.text.Font fontTextoNormal = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 8f, Font.NORMAL, iTextSharp.text.BaseColor.BLACK);
            iTextSharp.text.Font fontTextoGrandeNormal = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 12f, Font.NORMAL, iTextSharp.text.BaseColor.BLACK);
            iTextSharp.text.Font fontTextoGrandeNegrito = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 12f, Font.BOLD, iTextSharp.text.BaseColor.BLACK);
            iTextSharp.text.Font fontTextoMedioNegrito = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 10f, Font.BOLD, iTextSharp.text.BaseColor.BLACK);
            iTextSharp.text.Font fontTextoPequenoNegrito = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 8f, Font.BOLD, iTextSharp.text.BaseColor.BLACK);


            try
            {
                // Dados do Ticket de pesagem
                VagaoTicket vagaoTicket = notaTicketVagao.Vagao;
                NotaTicketDto produtoNota = TicketNotas.FirstOrDefault(np => np.ChaveNfe == notaTicketVagao.ChaveNfe);

                PdfPCell c = new PdfPCell();

                // Table Titulo Dados de Carregamento
                PdfPTable tp = new PdfPTable(1);
                tp.TotalWidth = ticketSize;
                tp.LockedWidth = true;
                float[] widths = new float[] { ticketSize };
                tp.SetWidths(widths);

                c = new PdfPCell(new Phrase(!String.IsNullOrEmpty(DescricaoCliente) ? DescricaoCliente : "América Latina Logistica - ALL", fontTextoGrandeNegrito));
                c.HorizontalAlignment = 1;
                tp.AddCell(c);


                // Dados Basicos - Table com 2 colunas
                PdfPTable tDadosBasicos = new PdfPTable(2);
                tDadosBasicos.TotalWidth = ticketSize;
                tDadosBasicos.LockedWidth = true;
                widths = new float[] { 70f, 150f };
                tDadosBasicos.SetWidths(widths);

                // Origem
                c = new PdfPCell(new Phrase("Origem", fontTextoNormal));
                c.Border = 0;
                c.HorizontalAlignment = 0;
                tDadosBasicos.AddCell(c);

                IEmpresa empresaOrigem = vagaoTicket.Ticket.Origem;
                string origemDescricao = (empresaOrigem != null ? empresaOrigem.DescricaoResumida : String.Empty);

                var origemCnpj = String.Empty;
                if (empresaOrigem != null)
                {
                    if (!String.IsNullOrEmpty(empresaOrigem.Cgc))
                    {
                        origemCnpj = empresaOrigem.Cgc.Length < 14 ? empresaOrigem.Cgc.PadLeft(14, '0') : empresaOrigem.Cgc;
                        origemCnpj = string.Format("{0}.{1}.{2}/{3}-{4}", origemCnpj.Substring(0, 2),
                                                                          origemCnpj.Substring(2, 3),
                                                                          origemCnpj.Substring(5, 3),
                                                                          origemCnpj.Substring(8, 4),
                                                                          origemCnpj.Substring(12, 2));
                    }
                }

                c = new PdfPCell(new Phrase(origemDescricao + " - " + origemCnpj, fontTextoNormal));
                c.Border = 0;
                c.HorizontalAlignment = 0;
                tDadosBasicos.AddCell(c);

                // Destino
                c = new PdfPCell(new Phrase("Destino", fontTextoNormal));
                c.Border = 0;
                c.HorizontalAlignment = 0;
                tDadosBasicos.AddCell(c);

                IEmpresa empresaDestino = vagaoTicket.Ticket.Destino;
                string destinoDescricao = (empresaDestino != null ? empresaDestino.DescricaoResumida : String.Empty);

                var destinoCnpj = String.Empty;
                if (empresaDestino != null)
                {
                    if (!String.IsNullOrEmpty(empresaDestino.Cgc))
                    {
                        destinoCnpj = empresaDestino.Cgc.Length < 14 ? empresaDestino.Cgc.PadLeft(14, '0') : empresaDestino.Cgc;
                        destinoCnpj = string.Format("{0}.{1}.{2}/{3}-{4}", destinoCnpj.Substring(0, 2),
                                                                           destinoCnpj.Substring(2, 3),
                                                                           destinoCnpj.Substring(5, 3),
                                                                           destinoCnpj.Substring(8, 4),
                                                                           destinoCnpj.Substring(12, 2));
                    }
                }

                c = new PdfPCell(new Phrase(destinoDescricao + " - " + destinoCnpj, fontTextoNormal));
                c.Border = 0;
                c.HorizontalAlignment = 0;
                tDadosBasicos.AddCell(c);

                // Data
                c = new PdfPCell(new Phrase("Data", fontTextoNormal));
                c.Border = 0;
                c.HorizontalAlignment = 0;
                tDadosBasicos.AddCell(c);

                c = new PdfPCell(new Phrase(vagaoTicket.Ticket.DataPesagem.ToString("dd/MM/yyyy HH:mm:ss"), fontTextoNormal));
                c.Border = 0;
                c.HorizontalAlignment = 0;
                tDadosBasicos.AddCell(c);

                // Vagão
                c = new PdfPCell(new Phrase("Vagão", fontTextoNormal));
                c.Border = 0;
                c.HorizontalAlignment = 0;
                tDadosBasicos.AddCell(c);

                var codigoVagao = vagaoTicket.CodigoVagao;
                if (!string.IsNullOrEmpty(vagaoTicket.SerieVagao))
                {
                    codigoVagao = string.Format("{0} {1}", vagaoTicket.SerieVagao, vagaoTicket.CodigoVagao);
                }

                c = new PdfPCell(new Phrase(codigoVagao, fontTextoNormal));
                c.Border = 0;
                c.HorizontalAlignment = 0;
                tDadosBasicos.AddCell(c);

                // Produto
                c = new PdfPCell(new Phrase("Produto", fontTextoNormal));
                c.Border = 0;
                c.HorizontalAlignment = 0;
                tDadosBasicos.AddCell(c);

                c = new PdfPCell(new Phrase((produtoNota != null ? produtoNota.Produto : String.Empty), fontTextoNormal));
                c.Border = 0;
                c.HorizontalAlignment = 0;
                tDadosBasicos.AddCell(c);

                // Peso Bruto
                c = new PdfPCell(new Phrase("Peso Bruto", fontTextoNormal));
                c.Border = 0;
                c.HorizontalAlignment = 0;
                tDadosBasicos.AddCell(c);

                c = new PdfPCell(new Phrase(vagaoTicket.PesoBrutoFormatado, fontTextoNormal));
                c.Border = 0;
                c.HorizontalAlignment = 0;
                tDadosBasicos.AddCell(c);

                // Peso Tara
                c = new PdfPCell(new Phrase("Peso Tara", fontTextoNormal));
                c.Border = 0;
                c.HorizontalAlignment = 0;
                tDadosBasicos.AddCell(c);

                c = new PdfPCell(new Phrase(vagaoTicket.PesoTaraFormatado, fontTextoNormal));
                c.Border = 0;
                c.HorizontalAlignment = 0;
                tDadosBasicos.AddCell(c);

                // Peso Líquido
                c = new PdfPCell(new Phrase("Peso Líquido", fontTextoNormal));
                c.Border = 0;
                c.HorizontalAlignment = 0;
                tDadosBasicos.AddCell(c);

                c = new PdfPCell(new Phrase(vagaoTicket.PesoLiquidoFormatado, fontTextoNormal));
                c.Border = 0;
                c.HorizontalAlignment = 0;
                tDadosBasicos.AddCell(c);

                tp.AddCell(tDadosBasicos);



                // Notas Fiscais - Titulo
                PdfPTable tNotasTitulo = new PdfPTable(1);
                tNotasTitulo.TotalWidth = ticketSize;
                tNotasTitulo.LockedWidth = true;
                widths = new float[] { ticketSize };
                tNotasTitulo.SetWidths(widths);

                c = new PdfPCell(new Phrase("Notas Fiscais Carregadas", fontTextoGrandeNormal));
                c.Border = 0;
                c.HorizontalAlignment = 1;
                //c.PaddingLeft = 3f;
                //c.PaddingRight = 3f;
                tNotasTitulo.AddCell(c);

                tp.AddCell(tNotasTitulo);

                // Notas Fiscais - Listagem - Table com 4 colunas
                PdfPTable tNotasListagem = new PdfPTable(4);
                tNotasListagem.TotalWidth = ticketSize;
                tNotasListagem.LockedWidth = true;
                widths = new float[] { 55f, 55f, 55f, 55f };
                tNotasListagem.SetWidths(widths);

                // Títulos
                c = new PdfPCell(new Phrase("Série", fontTextoNormal));
                c.Border = 0;
                c.HorizontalAlignment = 0;
                tNotasListagem.AddCell(c);

                c = new PdfPCell(new Phrase("Número", fontTextoNormal));
                c.Border = 0;
                c.HorizontalAlignment = 0;
                tNotasListagem.AddCell(c);

                c = new PdfPCell(new Phrase("Peso", fontTextoNormal));
                c.Border = 0;
                c.HorizontalAlignment = 0;
                tNotasListagem.AddCell(c);

                c = new PdfPCell(new Phrase("Saldo Disp.", fontTextoNormal));
                c.Border = 0;
                c.HorizontalAlignment = 0;
                tNotasListagem.AddCell(c);

                // Loop para apresentação dos dados das notas
                if (TicketNotas != null)
                {
                    foreach (var nota in TicketNotas)
                    {
                        var notaTicket = notasTickets.FirstOrDefault(n => n.NotaTicketVagao.ChaveNfe == nota.ChaveNfe);

                        var s = nota.Serie;
                        var num = nota.Numero;
                        var peso = (notaTicket != null ? notaTicket.NotaTicketVagao.PesoRateioFormatado : "0.000");
                        var pesoDisp = String.Format("{0:0.000}", nota.PesoDisponivel);

                        c = new PdfPCell(new Phrase(s, fontTextoNormal));
                        c.Border = 0;
                        c.HorizontalAlignment = 0;
                        tNotasListagem.AddCell(c);

                        c = new PdfPCell(new Phrase(num.ToString(), fontTextoNormal));
                        c.Border = 0;
                        c.HorizontalAlignment = 0;
                        tNotasListagem.AddCell(c);

                        c = new PdfPCell(new Phrase(peso.ToString(), fontTextoNormal));
                        c.Border = 0;
                        c.HorizontalAlignment = 0;
                        tNotasListagem.AddCell(c);

                        c = new PdfPCell(new Phrase(pesoDisp.ToString(), fontTextoNormal));
                        c.Border = 0;
                        c.HorizontalAlignment = 0;
                        tNotasListagem.AddCell(c);
                    }
                }
                else
                {
                    c = new PdfPCell(new Phrase(" ", fontTextoNormal));
                    c.Border = 0;
                    c.HorizontalAlignment = 0;
                    tNotasListagem.AddCell(c);

                    c = new PdfPCell(new Phrase(" ", fontTextoNormal));
                    c.Border = 0;
                    c.HorizontalAlignment = 0;
                    tNotasListagem.AddCell(c);

                    c = new PdfPCell(new Phrase(" ", fontTextoNormal));
                    c.Border = 0;
                    c.HorizontalAlignment = 0;
                    tNotasListagem.AddCell(c);

                    c = new PdfPCell(new Phrase(" ", fontTextoNormal));
                    c.Border = 0;
                    c.HorizontalAlignment = 0;
                    tNotasListagem.AddCell(c);
                }

                tp.AddCell(tNotasListagem);


                // Linha em branco - Separador
                PdfPTable tSeparador = new PdfPTable(1);
                tSeparador.TotalWidth = ticketSize;
                tSeparador.LockedWidth = true;
                widths = new float[] { ticketSize };
                tSeparador.SetWidths(widths);

                c = new PdfPCell(new Phrase(" ", fontTextoNormal));
                c.HorizontalAlignment = 1;
                c.Border = 0;
                tSeparador.AddCell(c);

                tp.AddCell(tSeparador);


                // Observação - 2 Colunas
                PdfPTable tObservacao = new PdfPTable(2);
                tObservacao.TotalWidth = ticketSize;
                tObservacao.LockedWidth = true;
                widths = new float[] { 70f, 150f };
                tObservacao.SetWidths(widths);

                c = new PdfPCell(new Phrase(" ", fontTextoNormal));
                c.Border = 0;
                c.HorizontalAlignment = 0;
                tObservacao.AddCell(c);

                c = new PdfPCell(new Phrase(" ", fontTextoNormal));
                c.Border = 0;
                c.HorizontalAlignment = 0;
                tObservacao.AddCell(c);

                c = new PdfPCell(new Phrase("Observação: ", fontTextoNormal));
                c.Border = 0;
                c.HorizontalAlignment = 0;
                tObservacao.AddCell(c);

                string observacao = (notaTicketVagao.Vagao.Ticket != null ? notaTicketVagao.Vagao.Ticket.Observacao : String.Empty);
                c = new PdfPCell(
                    new Phrase(
                        (!String.IsNullOrEmpty(observacao) ?
                            (observacao.Length > 120 ? observacao.Substring(0, 120) : observacao) :
                            String.Empty),
                        fontTextoNormal)
                    );
                c.Border = 0;
                c.HorizontalAlignment = 0;
                tObservacao.AddCell(c);

                c = new PdfPCell(new Phrase(" ", fontTextoNormal));
                c.Border = 0;
                c.HorizontalAlignment = 0;
                tObservacao.AddCell(c);

                c = new PdfPCell(new Phrase(" ", fontTextoNormal));
                c.Border = 0;
                c.HorizontalAlignment = 0;
                tObservacao.AddCell(c);

                tp.AddCell(tObservacao);


                // Responsável, Autenticação e Data - 2 Colunas
                PdfPTable tResp = new PdfPTable(2);
                tResp.TotalWidth = ticketSize;
                tResp.LockedWidth = true;
                widths = new float[] { 90f, 130f };
                tResp.SetWidths(widths);

                c = new PdfPCell(new Phrase("Resp:", fontTextoNormal));
                c.Border = 0;
                c.HorizontalAlignment = 0;
                tResp.AddCell(c);

                string resp = (notaTicketVagao.Vagao.Ticket != null ? notaTicketVagao.Vagao.Ticket.Responsavel : String.Empty);
                c = new PdfPCell(new Phrase(resp, fontTextoNormal));
                c.Border = 0;
                c.HorizontalAlignment = 0;
                tResp.AddCell(c);

                c = new PdfPCell(new Phrase("Autenticacao", fontTextoNormal));
                c.Border = 0;
                c.HorizontalAlignment = 0;
                tResp.AddCell(c);

                c = new PdfPCell(new Phrase(" ", fontTextoNormal));
                c.Border = 0;
                c.HorizontalAlignment = 0;
                tResp.AddCell(c);

                c = new PdfPCell(new Phrase("Data Reimpressão:", fontTextoNormal));
                c.Border = 0;
                c.HorizontalAlignment = 0;
                tResp.AddCell(c);

                c = new PdfPCell(new Phrase(DateTime.Now.ToString(), fontTextoNormal));
                c.Border = 0;
                c.HorizontalAlignment = 0;
                tResp.AddCell(c);

                tp.AddCell(tResp);


                // Adiciona Tables principais
                document.Add(tp);
            }
            finally
            {
                document.Close();
                document.Dispose();
            }

            var sr = new StreamReader(tempFilePath);
            var ms = new MemoryStream();
            StreamUtils.Copy(sr.BaseStream, ms, new byte[4096]);
            sr.Close();
            File.Delete(tempFilePath);
            ms.Seek(0, SeekOrigin.Begin);
            return ms;
        }

        /// <summary>
        /// Gera um arquivo substituto do MDF-e levando em consideração as informações passada como parâmetro
        /// </summary>
        /// <param name="registros">Dados a serem incluídos no arquivo</param>
        /// <returns>Retorna o PDF do arquivo substituto do PDF</returns>
        public Stream GerarManifestoCargaPdf(ICollection<ManifestoCargaDto> registros, List<NotaTicketVagao> vagoesTicket)
        {
            var tempFilePath = Path.GetTempFileName();
            var document = new Document(PageSize.A4.Rotate(), 10, 10, 10, 10);

            var writer = PdfWriter.GetInstance(document, new FileStream(tempFilePath, FileMode.Create));

            document.Open();

            // Definição da Fonte
            var fontTextoNormal = new Font(Font.FontFamily.HELVETICA, 8f, Font.NORMAL, BaseColor.BLACK);
            var fontTextoNegrito = new Font(Font.FontFamily.HELVETICA, 8f, Font.BOLD, BaseColor.BLACK);

            var fontTextoTitulo = new Font(Font.FontFamily.HELVETICA, 18f, Font.BOLD, BaseColor.BLACK);
            var fontTextoSubTitulo = new Font(Font.FontFamily.HELVETICA, 12f, Font.BOLD, BaseColor.BLACK);

            var fontTextoTituloGrid = new Font(Font.FontFamily.HELVETICA, 5f, Font.BOLD, BaseColor.BLACK);
            var fontTextoConteudoGrid = new Font(Font.FontFamily.HELVETICA, 7f, Font.NORMAL, BaseColor.BLACK);

            var fontTextoTituloFooterGrid = new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLDITALIC, BaseColor.BLACK);
            var fontTextoConteudoFooterGrid = new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLDITALIC, BaseColor.BLACK);

            var fontTextoNegritoConteudoFooter = new Font(Font.FontFamily.HELVETICA, 10f, Font.BOLD, BaseColor.BLACK);

            var fontTextoAssinatura = new Font(Font.FontFamily.HELVETICA, 10f, Font.BOLD, BaseColor.BLACK);

            var widthPagina = 800f;

            // Linha Horizontal
            var linhaHorizontal = new Paragraph(string.Empty);
            var linhaSeparadora = new LineSeparator();
            linhaSeparadora.LineWidth = 0.3f;
            linhaHorizontal.Add(linhaSeparadora);

            // Logo (Rumo.png)
            var logoRumo = Image.GetInstance(GetLogoImage());
            logoRumo.ScaleToFit(160f, 50f);

            PdfPCell cell = null;

            var tremPrefixo = registros.Select(r => r.TremPrefixo).First();
            var osNumero = registros.Select(r => r.OsNumero).First();

            #region Header

            // Table Principal - Header
            var header = new PdfPTable(2);
            header.TotalWidth = widthPagina;
            header.LockedWidth = true;
            var headerWidths = new float[] { 160f, (widthPagina - 160f) };
            header.SetWidths(headerWidths);
            header.SpacingAfter = 30f;

            cell = new PdfPCell(logoRumo);
            cell.Border = 0;
            cell.HorizontalAlignment = 1;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            header.AddCell(cell);

            var thTitulo = new PdfPTable(1);
            cell = new PdfPCell(new Phrase("Resumo de Composição", fontTextoTitulo));
            cell.Border = 0;
            cell.HorizontalAlignment = 1;
            thTitulo.AddCell(cell);

            cell = new PdfPCell(new Phrase(string.Format("Trem: {0} - OS: {1}", tremPrefixo, osNumero), fontTextoSubTitulo));
            cell.Border = 0;
            cell.HorizontalAlignment = 1;
            thTitulo.AddCell(cell);

            cell = new PdfPCell(new Phrase(DateTime.Now.ToString(), fontTextoNegrito));
            cell.Border = 0;
            cell.HorizontalAlignment = 1;
            thTitulo.AddCell(cell);

            cell = new PdfPCell(thTitulo);
            cell.Border = 0;
            cell.HorizontalAlignment = 1;
            header.AddCell(cell);

            #endregion Header

            #region Informações do Trem

            var recebedorNome = registros.Select(r => r.RecebedorNome).First();
            var recebedorCnpj = registros.Select(r => r.RecebedorCnpjFormatado).First();
            var tremOrigem = registros.Select(r => r.TremOrigem).First();
            var tremDestino = registros.Select(r => r.TremDestino).First();

            // Table Principal - Header
            var tremTable = new PdfPTable(2);
            tremTable.TotalWidth = widthPagina;
            tremTable.LockedWidth = true;
            var tremTableWidths = new float[] { 100f, (widthPagina - 100f) };
            tremTable.SetWidths(tremTableWidths);
            tremTable.SpacingAfter = 20f;

            #region Lado Esquerdo

            var tremTableLeft = new PdfPTable(1);
            cell = new PdfPCell(new Phrase("Recebedor:", fontTextoNegrito));
            cell.Border = 0;
            cell.HorizontalAlignment = 0;
            tremTableLeft.AddCell(cell);

            cell = new PdfPCell(new Phrase("Origem:", fontTextoNegrito));
            cell.Border = 0;
            cell.HorizontalAlignment = 0;
            tremTableLeft.AddCell(cell);

            cell = new PdfPCell(new Phrase("Destino:", fontTextoNegrito));
            cell.Border = 0;
            cell.HorizontalAlignment = 0;
            tremTableLeft.AddCell(cell);

            cell = new PdfPCell(new Phrase("Prefixo - OS:", fontTextoNegrito));
            cell.Border = 0;
            cell.HorizontalAlignment = 0;
            tremTableLeft.AddCell(cell);

            cell = new PdfPCell(tremTableLeft);
            cell.Border = 0;
            cell.HorizontalAlignment = 1;
            tremTable.AddCell(cell);

            #endregion Lado Esquerdo

            #region Lado Direito

            var tremTableRight = new PdfPTable(1);
            cell = new PdfPCell(new Phrase(string.Format("{0} - {1}", recebedorNome, recebedorCnpj), fontTextoNormal));
            cell.Border = 0;
            cell.HorizontalAlignment = 0;
            tremTableRight.AddCell(cell);

            cell = new PdfPCell(new Phrase(tremOrigem, fontTextoNormal));
            cell.Border = 0;
            cell.HorizontalAlignment = 0;
            tremTableRight.AddCell(cell);

            cell = new PdfPCell(new Phrase(tremDestino, fontTextoNormal));
            cell.Border = 0;
            cell.HorizontalAlignment = 0;
            tremTableRight.AddCell(cell);

            cell = new PdfPCell(new Phrase(string.Format("{0} - {1}", tremPrefixo, osNumero), fontTextoNormal));
            cell.Border = 0;
            cell.HorizontalAlignment = 0;
            tremTableRight.AddCell(cell);

            cell = new PdfPCell(tremTableRight);
            cell.Border = 0;
            cell.HorizontalAlignment = 1;
            tremTable.AddCell(cell);

            #endregion Lado Direito

            #endregion

            #region Informações dos Vagões

            var vagaoTable = new PdfPTable(17);
            vagaoTable.TotalWidth = widthPagina;
            vagaoTable.LockedWidth = true;
            var vagaoTableWidths = new float[]
                                       {
                                           20f, // Sequência
                                           65f, // Vagão
                                           40f, // Número CT-e
                                           40f, // Peso Tara
                                           40f, // Peso Líquido
                                           40f, // Peso Bruto
                                           40f, // Ticket Peso Tara
                                           40f, // Ticket Peso Líquido
                                           40f, // Ticket Peso Bruto
                                           65f, // Mercadoria
                                           60f, // Data Carregamento
                                           60f, // Nota Fiscal
                                           50f, // Data Nota Fiscal
                                           40f, // Peso Total Nota Fiscal
                                           40f, // Peso Rateio Nota Fiscal
                                           60f, // Remetente Nota Fiscal
                                           60f  // Destinatário Nota Fiscal
                                       };
            vagaoTable.SetWidths(vagaoTableWidths);
            vagaoTable.SpacingAfter = 10f;

            #region Headers

            var corLinhaHeader = BaseColor.GRAY;

            cell = new PdfPCell(new Phrase("Seq.", fontTextoTituloGrid));
            cell.Border = 1;
            cell.HorizontalAlignment = 1;
            cell.BackgroundColor = corLinhaHeader;
            vagaoTable.AddCell(cell);

            cell = new PdfPCell(new Phrase("Vagão", fontTextoTituloGrid));
            cell.Border = 1;
            cell.HorizontalAlignment = 1;
            cell.BackgroundColor = corLinhaHeader;
            vagaoTable.AddCell(cell);

            cell = new PdfPCell(new Phrase("Num. CT-e", fontTextoTituloGrid));
            cell.Border = 1;
            cell.HorizontalAlignment = 1;
            cell.BackgroundColor = corLinhaHeader;
            vagaoTable.AddCell(cell);

            cell = new PdfPCell(new Phrase("Tara", fontTextoTituloGrid));
            cell.Border = 1;
            cell.HorizontalAlignment = 1;
            cell.BackgroundColor = corLinhaHeader;
            vagaoTable.AddCell(cell);

            cell = new PdfPCell(new Phrase("TU", fontTextoTituloGrid));
            cell.Border = 1;
            cell.HorizontalAlignment = 1;
            cell.BackgroundColor = corLinhaHeader;
            vagaoTable.AddCell(cell);

            cell = new PdfPCell(new Phrase("TB", fontTextoTituloGrid));
            cell.Border = 1;
            cell.HorizontalAlignment = 1;
            cell.BackgroundColor = corLinhaHeader;
            vagaoTable.AddCell(cell);

            cell = new PdfPCell(new Phrase("Ticket Tara", fontTextoTituloGrid));
            cell.Border = 1;
            cell.HorizontalAlignment = 1;
            cell.BackgroundColor = corLinhaHeader;
            vagaoTable.AddCell(cell);

            cell = new PdfPCell(new Phrase("Ticket TU", fontTextoTituloGrid));
            cell.Border = 1;
            cell.HorizontalAlignment = 1;
            cell.BackgroundColor = corLinhaHeader;
            vagaoTable.AddCell(cell);

            cell = new PdfPCell(new Phrase("Ticket TB", fontTextoTituloGrid));
            cell.Border = 1;
            cell.HorizontalAlignment = 1;
            cell.BackgroundColor = corLinhaHeader;
            vagaoTable.AddCell(cell);

            cell = new PdfPCell(new Phrase("Mercadoria", fontTextoTituloGrid));
            cell.Border = 1;
            cell.HorizontalAlignment = 1;
            cell.BackgroundColor = corLinhaHeader;
            vagaoTable.AddCell(cell);

            cell = new PdfPCell(new Phrase("Data Carregamento", fontTextoTituloGrid));
            cell.Border = 1;
            cell.HorizontalAlignment = 1;
            cell.BackgroundColor = corLinhaHeader;
            vagaoTable.AddCell(cell);

            cell = new PdfPCell(new Phrase("Nota Fiscal (NF)", fontTextoTituloGrid));
            cell.Border = 1;
            cell.HorizontalAlignment = 1;
            cell.BackgroundColor = corLinhaHeader;
            vagaoTable.AddCell(cell);

            cell = new PdfPCell(new Phrase("Data NF", fontTextoTituloGrid));
            cell.Border = 1;
            cell.HorizontalAlignment = 1;
            cell.BackgroundColor = corLinhaHeader;
            vagaoTable.AddCell(cell);

            cell = new PdfPCell(new Phrase("Peso Total NF", fontTextoTituloGrid));
            cell.Border = 1;
            cell.HorizontalAlignment = 1;
            cell.BackgroundColor = corLinhaHeader;
            vagaoTable.AddCell(cell);

            cell = new PdfPCell(new Phrase("Peso Rateio NF", fontTextoTituloGrid));
            cell.Border = 1;
            cell.HorizontalAlignment = 1;
            cell.BackgroundColor = corLinhaHeader;
            vagaoTable.AddCell(cell);

            cell = new PdfPCell(new Phrase("Remetente NF", fontTextoTituloGrid));
            cell.Border = 1;
            cell.HorizontalAlignment = 1;
            cell.BackgroundColor = corLinhaHeader;
            vagaoTable.AddCell(cell);

            cell = new PdfPCell(new Phrase("Destinatário NF", fontTextoTituloGrid));
            cell.Border = 1;
            cell.HorizontalAlignment = 1;
            cell.BackgroundColor = corLinhaHeader;
            vagaoTable.AddCell(cell);

            #endregion Header

            var indexLinha = 1;

            var totalVagoes = 0;
            var totalPesoTara = new decimal(0);
            var totalPesoLiquido = new decimal(0);
            var totalPesoBruto = new decimal(0);
            var totalTicketPesoTara = new decimal(0);
            var totalTicketPesoLiquido = new decimal(0);
            var totalTicketPesoBruto = new decimal(0);

            var vagoesCodigos = registros.Select(r => r.VagaoCodigo).Distinct().ToList();
            foreach (var vagaoCodigo in vagoesCodigos)
            {
                var vagaoRegistro = registros.Where(r => r.VagaoCodigo == vagaoCodigo).First();
                var vagaoSequencia = vagaoRegistro.VagaoSequencia;
                var vagaoPesoTara = vagaoRegistro.PesoTaraVagao;
                var vagaoPesoLiquido = registros.Where(r => r.VagaoCodigo == vagaoCodigo).Sum(x => x.NotaFiscalPesoRateio);
                var vagaoPesoBruto = registros.Where(r => r.VagaoCodigo == vagaoCodigo).Sum(x => x.NotaFiscalPesoRateio) + vagaoRegistro.PesoTaraVagao;
                var vagaoTicketPesoTara = vagaoRegistro.PesoTaraVagao;
                var vagaoTicketPesoLiquido = vagaoRegistro.PesoLiquidoVagao;
                var vagaoTicketPesoBruto = vagaoRegistro.PesoBrutoVagao;
                var vagaoTicket = vagoesTicket.Where(vt => vt.Vagao.CodigoVagao == vagaoCodigo).FirstOrDefault();
                if (vagaoTicket != null)
                {
                    vagaoTicketPesoTara = Decimal.Parse(vagaoTicket.Vagao.PesoTaraFormatado);
                    vagaoTicketPesoLiquido = Decimal.Parse(vagaoTicket.Vagao.PesoLiquidoFormatado);
                    vagaoTicketPesoBruto = Decimal.Parse(vagaoTicket.Vagao.PesoBrutoFormatado);
                }

                totalVagoes++;
                totalPesoTara += vagaoPesoTara;
                totalPesoLiquido += vagaoPesoLiquido;
                totalPesoBruto += vagaoPesoBruto;
                totalTicketPesoTara += vagaoTicketPesoTara;
                totalTicketPesoLiquido += vagaoTicketPesoLiquido;
                totalTicketPesoBruto += vagaoTicketPesoBruto;
                //// loop criado para separar os CTes de MultiploDespacho que estavam sendo suprimidos no relatório.
                var ctes = registros.Where(v => v.VagaoCodigo == vagaoCodigo).Select(x => x.NumeroCte.ToString()).Distinct().ToList();
                foreach (var numeroCTe in ctes)
                {
                    var mercadoria = vagaoRegistro.Mercadoria;
                    var dataCarregamento = vagaoRegistro.DataCarregamento;
                    var vagaoNumeroCte = numeroCTe; ////vagaoRegistro.NumeroCte;
                    var vagaoSerie = vagaoRegistro.VagaoSerie;
                    var notasFiscaisRegistros = registros.Where(r => r.VagaoCodigo == vagaoCodigo && r.NumeroCte.Equals(numeroCTe)).Distinct().ToList();

                    var incluiInfoVagao = true;
                    foreach (var notaFiscalRegistro in notasFiscaisRegistros)
                    {
                        var valor = string.Empty;
                        var corLinha = indexLinha % 2 == 0 ? BaseColor.WHITE : BaseColor.LIGHT_GRAY;

                        // Sequência
                        valor = incluiInfoVagao ? vagaoSequencia.ToString().PadLeft(3, '0') : string.Empty;
                        cell = new PdfPCell(new Phrase(valor, fontTextoConteudoGrid));
                        cell.Border = 0;
                        cell.HorizontalAlignment = 1;
                        cell.BackgroundColor = corLinha;
                        vagaoTable.AddCell(cell);

                        // Vagão
                        valor = incluiInfoVagao ? string.Format("{0}-{1}", vagaoSerie, vagaoCodigo) : string.Empty;
                        cell = new PdfPCell(new Phrase(valor, fontTextoConteudoGrid));
                        cell.Border = 0;
                        cell.HorizontalAlignment = 1;
                        cell.BackgroundColor = corLinha;
                        vagaoTable.AddCell(cell);

                        // Número CT-e
                        valor = incluiInfoVagao ? vagaoNumeroCte : string.Empty;
                        cell = new PdfPCell(new Phrase(valor, fontTextoConteudoGrid));
                        cell.Border = 0;
                        cell.HorizontalAlignment = 1;
                        cell.BackgroundColor = corLinha;
                        vagaoTable.AddCell(cell);

                        // Peso Tara
                        valor = incluiInfoVagao ? vagaoPesoTara.ToString("N3") : string.Empty;
                        cell = new PdfPCell(new Phrase(valor, fontTextoConteudoGrid));
                        cell.Border = 0;
                        cell.HorizontalAlignment = 1;
                        cell.BackgroundColor = corLinha;
                        vagaoTable.AddCell(cell);

                        // Peso Líquido
                        valor = incluiInfoVagao ? vagaoPesoLiquido.ToString("N3") : string.Empty;
                        cell = new PdfPCell(new Phrase(valor, fontTextoConteudoGrid));
                        cell.Border = 0;
                        cell.HorizontalAlignment = 1;
                        cell.BackgroundColor = corLinha;
                        vagaoTable.AddCell(cell);

                        // Peso Bruto
                        valor = incluiInfoVagao ? vagaoPesoBruto.ToString("N3") : string.Empty;
                        cell = new PdfPCell(new Phrase(valor, fontTextoConteudoGrid));
                        cell.Border = 0;
                        cell.HorizontalAlignment = 1;
                        cell.BackgroundColor = corLinha;
                        vagaoTable.AddCell(cell);

                        // Peso Tara do Ticket de Pesagem
                        valor = incluiInfoVagao ? vagaoTicketPesoTara.ToString("N3") : string.Empty;
                        cell = new PdfPCell(new Phrase(valor, fontTextoConteudoGrid));
                        cell.Border = 0;
                        cell.HorizontalAlignment = 1;
                        cell.BackgroundColor = corLinha;
                        vagaoTable.AddCell(cell);

                        // Peso Líquido do Ticket de Pesagem
                        valor = incluiInfoVagao ? vagaoTicketPesoLiquido.ToString("N3") : string.Empty;
                        cell = new PdfPCell(new Phrase(valor, fontTextoConteudoGrid));
                        cell.Border = 0;
                        cell.HorizontalAlignment = 1;
                        cell.BackgroundColor = corLinha;
                        vagaoTable.AddCell(cell);

                        // Peso Bruto do Ticket de Pesagem
                        valor = incluiInfoVagao ? vagaoTicketPesoBruto.ToString("N3") : string.Empty;
                        cell = new PdfPCell(new Phrase(valor, fontTextoConteudoGrid));
                        cell.Border = 0;
                        cell.HorizontalAlignment = 1;
                        cell.BackgroundColor = corLinha;
                        vagaoTable.AddCell(cell);

                        // Mercadoria
                        valor = incluiInfoVagao ? mercadoria : string.Empty;
                        cell = new PdfPCell(new Phrase(valor, fontTextoConteudoGrid));
                        cell.Border = 0;
                        cell.HorizontalAlignment = 1;
                        cell.BackgroundColor = corLinha;
                        vagaoTable.AddCell(cell);

                        // Data Carregamento
                        valor = incluiInfoVagao ? dataCarregamento.ToString() : string.Empty;
                        cell = new PdfPCell(new Phrase(valor, fontTextoConteudoGrid));
                        cell.Border = 0;
                        cell.HorizontalAlignment = 1;
                        cell.BackgroundColor = corLinha;
                        vagaoTable.AddCell(cell);

                        // NF
                        valor = string.Format("{0}-{1}", notaFiscalRegistro.NotaFiscalSerie,
                                              notaFiscalRegistro.NotaFiscalNumero);
                        cell = new PdfPCell(new Phrase(valor, fontTextoConteudoGrid));
                        cell.Border = 0;
                        cell.HorizontalAlignment = 1;
                        cell.BackgroundColor = corLinha;
                        vagaoTable.AddCell(cell);

                        // Data NF
                        valor = notaFiscalRegistro.NotaFiscalData.ToString("dd/MM/yyyy");
                        cell = new PdfPCell(new Phrase(valor, fontTextoConteudoGrid));
                        cell.Border = 0;
                        cell.HorizontalAlignment = 1;
                        cell.BackgroundColor = corLinha;
                        vagaoTable.AddCell(cell);

                        // Peso Total NF
                        valor = notaFiscalRegistro.NotaFiscalPesoTotal.ToString("N3");
                        cell = new PdfPCell(new Phrase(valor, fontTextoConteudoGrid));
                        cell.Border = 0;
                        cell.HorizontalAlignment = 1;
                        cell.BackgroundColor = corLinha;
                        vagaoTable.AddCell(cell);

                        // Peso Rateio NF
                        valor = notaFiscalRegistro.NotaFiscalPesoRateio.ToString("N3");
                        cell = new PdfPCell(new Phrase(valor, fontTextoConteudoGrid));
                        cell.Border = 0;
                        cell.HorizontalAlignment = 1;
                        cell.BackgroundColor = corLinha;
                        vagaoTable.AddCell(cell);

                        // Remetente NF
                        valor = notaFiscalRegistro.NotaFiscalRemetente;
                        cell = new PdfPCell(new Phrase(valor, fontTextoConteudoGrid));
                        cell.Border = 0;
                        cell.HorizontalAlignment = 1;
                        cell.BackgroundColor = corLinha;
                        vagaoTable.AddCell(cell);

                        // Destinátario NF
                        valor = notaFiscalRegistro.NotaFiscalDestinatario;
                        cell = new PdfPCell(new Phrase(valor, fontTextoConteudoGrid));
                        cell.Border = 0;
                        cell.HorizontalAlignment = 1;
                        cell.BackgroundColor = corLinha;
                        vagaoTable.AddCell(cell);

                        incluiInfoVagao = false;
                    }
                }
                indexLinha++;
            }


            #region Totalizadores

            var totalizador = string.Empty;
            var corLinhaTotalizador = BaseColor.GRAY;

            // Sequência
            totalizador = "Total:";
            cell = new PdfPCell(new Phrase(totalizador, fontTextoTituloFooterGrid));
            cell.Border = 0;
            cell.HorizontalAlignment = 1;
            cell.BackgroundColor = corLinhaTotalizador;
            vagaoTable.AddCell(cell);

            // Vagão
            totalizador = string.Format("Qntd. Vagões: {0}", totalVagoes);
            cell = new PdfPCell(new Phrase(totalizador, fontTextoConteudoFooterGrid));
            cell.Border = 0;
            cell.HorizontalAlignment = 1;
            cell.BackgroundColor = corLinhaTotalizador;
            vagaoTable.AddCell(cell);

            // Número CT-e
            totalizador = string.Empty;
            cell = new PdfPCell(new Phrase(totalizador, fontTextoConteudoFooterGrid));
            cell.Border = 0;
            cell.HorizontalAlignment = 1;
            cell.BackgroundColor = corLinhaTotalizador;
            vagaoTable.AddCell(cell);

            // Peso Tara
            totalizador = string.Format("Tara Total: {0}", totalPesoTara.ToString("N3"));
            cell = new PdfPCell(new Phrase(totalizador, fontTextoConteudoFooterGrid));
            cell.Border = 0;
            cell.HorizontalAlignment = 1;
            cell.BackgroundColor = corLinhaTotalizador;
            vagaoTable.AddCell(cell);

            // Peso Líquido
            totalizador = string.Format("TU Total: {0}", totalPesoLiquido.ToString("N3"));
            cell = new PdfPCell(new Phrase(totalizador, fontTextoConteudoFooterGrid));
            cell.Border = 0;
            cell.HorizontalAlignment = 1;
            cell.BackgroundColor = corLinhaTotalizador;
            vagaoTable.AddCell(cell);

            // Peso Bruto
            totalizador = string.Format("TB Total: {0}", totalPesoBruto.ToString("N3"));
            cell = new PdfPCell(new Phrase(totalizador, fontTextoConteudoFooterGrid));
            cell.Border = 0;
            cell.HorizontalAlignment = 1;
            cell.BackgroundColor = corLinhaTotalizador;
            vagaoTable.AddCell(cell);

            // Ticket Peso Tara
            totalizador = string.Format("Ticket Tara Total: {0}", totalTicketPesoTara.ToString("N3"));
            cell = new PdfPCell(new Phrase(totalizador, fontTextoConteudoFooterGrid));
            cell.Border = 0;
            cell.HorizontalAlignment = 1;
            cell.BackgroundColor = corLinhaTotalizador;
            vagaoTable.AddCell(cell);

            // Ticket Peso Líquido
            totalizador = string.Format("Ticket TU Total: {0}", totalTicketPesoLiquido.ToString("N3"));
            cell = new PdfPCell(new Phrase(totalizador, fontTextoConteudoFooterGrid));
            cell.Border = 0;
            cell.HorizontalAlignment = 1;
            cell.BackgroundColor = corLinhaTotalizador;
            vagaoTable.AddCell(cell);

            // Ticket Peso Bruto
            totalizador = string.Format("Ticket TB Total: {0}", totalTicketPesoBruto.ToString("N3"));
            cell = new PdfPCell(new Phrase(totalizador, fontTextoConteudoFooterGrid));
            cell.Border = 0;
            cell.HorizontalAlignment = 1;
            cell.BackgroundColor = corLinhaTotalizador;
            vagaoTable.AddCell(cell);

            // Mercadoria
            totalizador = string.Empty;
            cell = new PdfPCell(new Phrase(totalizador, fontTextoConteudoFooterGrid));
            cell.Border = 0;
            cell.HorizontalAlignment = 1;
            cell.BackgroundColor = corLinhaTotalizador;
            vagaoTable.AddCell(cell);

            // Data Carregamento
            totalizador = string.Empty;
            cell = new PdfPCell(new Phrase(totalizador, fontTextoConteudoFooterGrid));
            cell.Border = 0;
            cell.HorizontalAlignment = 1;
            cell.BackgroundColor = corLinhaTotalizador;
            vagaoTable.AddCell(cell);

            // NF
            totalizador = string.Empty;
            cell = new PdfPCell(new Phrase(totalizador, fontTextoConteudoFooterGrid));
            cell.Border = 0;
            cell.HorizontalAlignment = 1;
            cell.BackgroundColor = corLinhaTotalizador;
            vagaoTable.AddCell(cell);

            // Data NF
            totalizador = string.Empty;
            cell = new PdfPCell(new Phrase(totalizador, fontTextoConteudoFooterGrid));
            cell.Border = 0;
            cell.HorizontalAlignment = 1;
            cell.BackgroundColor = corLinhaTotalizador;
            vagaoTable.AddCell(cell);

            // Peso Total NF
            totalizador = string.Empty;
            cell = new PdfPCell(new Phrase(totalizador, fontTextoConteudoFooterGrid));
            cell.Border = 0;
            cell.HorizontalAlignment = 1;
            cell.BackgroundColor = corLinhaTotalizador;
            vagaoTable.AddCell(cell);

            // Peso Rateio NF
            totalizador = string.Empty;
            cell = new PdfPCell(new Phrase(totalizador, fontTextoConteudoFooterGrid));
            cell.Border = 0;
            cell.HorizontalAlignment = 1;
            cell.BackgroundColor = corLinhaTotalizador;
            vagaoTable.AddCell(cell);

            // Remetente NF
            totalizador = string.Empty;
            cell = new PdfPCell(new Phrase(totalizador, fontTextoConteudoFooterGrid));
            cell.Border = 0;
            cell.HorizontalAlignment = 1;
            cell.BackgroundColor = corLinhaTotalizador;
            vagaoTable.AddCell(cell);

            // Destinátario NF
            totalizador = string.Empty;
            cell = new PdfPCell(new Phrase(totalizador, fontTextoConteudoFooterGrid));
            cell.Border = 0;
            cell.HorizontalAlignment = 1;
            cell.BackgroundColor = corLinhaTotalizador;
            vagaoTable.AddCell(cell);

            #endregion

            #endregion Informações dos Vagões

            #region Footer

            // Table Principal - Footer
            var footerTable = new PdfPTable(1);
            footerTable.TotalWidth = widthPagina;
            footerTable.LockedWidth = true;
            var footerWidths = new float[] { widthPagina };
            footerTable.SetWidths(footerWidths);
            footerTable.SpacingAfter = 20f;

            var textoFooter = new Paragraph();
            textoFooter.Add(linhaSeparadora);
            textoFooter.Add(Chunk.NEWLINE);
            textoFooter.Add(Chunk.NEWLINE);
            textoFooter.Add(Chunk.NEWLINE);
            textoFooter.Add(new Phrase("Recebemos este aviso em: ____/____/________ às ____:____ horas.", fontTextoNegritoConteudoFooter));
            cell = new PdfPCell(textoFooter);
            cell.Border = 0;
            cell.HorizontalAlignment = 1;
            footerTable.AddCell(cell);

            #endregion Footer

            #region Assinaturas

            var assinaturasTable = new PdfPTable(2);
            assinaturasTable.TotalWidth = widthPagina;
            assinaturasTable.LockedWidth = true;
            var assinaturasTableWidths = new float[] { (widthPagina / 2), (widthPagina / 2) };
            assinaturasTable.SetWidths(assinaturasTableWidths);

            #region Assinatura do Agente

            var assinaturaAgente = new PdfPTable(4);
            assinaturaAgente.TotalWidth = widthPagina / 2;
            assinaturaAgente.LockedWidth = true;
            var assinaturaAgenteWidths = new float[] { (widthPagina / 8), 3 * (widthPagina / 8), 3 * (widthPagina / 8), (widthPagina / 8) };
            assinaturaAgente.SetWidths(assinaturaAgenteWidths);

            cell = new PdfPCell();
            cell.Border = 0;
            cell.HorizontalAlignment = 1;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            assinaturaAgente.AddCell(cell);

            var assinaturaAgenteConteudo = new Paragraph();
            assinaturaAgenteConteudo.Add(linhaSeparadora);
            assinaturaAgenteConteudo.Add(new Phrase("Assinatura do Agente", fontTextoAssinatura));
            cell = new PdfPCell(assinaturaAgenteConteudo);
            cell.Colspan = 2;
            cell.Border = 0;
            cell.HorizontalAlignment = 1;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            assinaturaAgente.AddCell(cell);

            cell = new PdfPCell();
            cell.Border = 0;
            cell.HorizontalAlignment = 1;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            assinaturaAgente.AddCell(cell);

            cell = new PdfPCell(assinaturaAgente);
            cell.Border = 0;
            cell.HorizontalAlignment = 1;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            assinaturasTable.AddCell(cell);

            #endregion Assinatura do Agente

            #region Assinatura do Destinatario / Preposto

            var assinaturaDestinatarioPreposto = new PdfPTable(4);
            assinaturaDestinatarioPreposto.TotalWidth = widthPagina / 2;
            assinaturaDestinatarioPreposto.LockedWidth = true;
            var assinaturaDestinatarioPrepostoWidths = new float[] { (widthPagina / 8), 3 * (widthPagina / 8), 3 * (widthPagina / 8), (widthPagina / 8) };
            assinaturaDestinatarioPreposto.SetWidths(assinaturaDestinatarioPrepostoWidths);

            cell = new PdfPCell();
            cell.Border = 0;
            cell.HorizontalAlignment = 1;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            assinaturaDestinatarioPreposto.AddCell(cell);

            var assinaturaDestinatarioPrepostoConteudo = new Paragraph();
            assinaturaDestinatarioPrepostoConteudo.Add(linhaSeparadora);
            assinaturaDestinatarioPrepostoConteudo.Add(new Phrase("Destinatário / Preposto", fontTextoAssinatura));
            cell = new PdfPCell(assinaturaDestinatarioPrepostoConteudo);
            cell.Colspan = 2;
            cell.Border = 0;
            cell.HorizontalAlignment = 1;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            assinaturaDestinatarioPreposto.AddCell(cell);

            cell = new PdfPCell();
            cell.Border = 0;
            cell.HorizontalAlignment = 1;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            assinaturaDestinatarioPreposto.AddCell(cell);

            cell = new PdfPCell(assinaturaDestinatarioPreposto);
            cell.Border = 0;
            cell.HorizontalAlignment = 1;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            assinaturasTable.AddCell(cell);

            #endregion Assinatura do Destinatario / Preposto

            #endregion Assinaturas

            document.Add(header);
            document.Add(Chunk.NEWLINE);
            document.Add(tremTable);
            document.Add(Chunk.NEWLINE);
            document.Add(vagaoTable);
            document.Add(Chunk.NEWLINE);
            document.Add(footerTable);
            document.Add(Chunk.NEWLINE);
            document.Add(assinaturasTable);

            document.Close();
            document.Dispose();

            var sr = new StreamReader(tempFilePath);
            var ms = new MemoryStream();
            StreamUtils.Copy(sr.BaseStream, ms, new byte[4096]);
            sr.Close();
            File.Delete(tempFilePath);
            ms.Seek(0, SeekOrigin.Begin);

            return ms;
        }

        /// <summary>
        /// Obtem todas os IDs das composições que ainda não foram inseridas no log de envios de documentos
        /// </summary>
        /// <param name="areaOperacionalId">ID da área operacional que aciona o envio da documentação</param>
        /// <param name="terminalDestinoId">ID do terminal (empresa) que receberá a documentação</param>
        /// <returns>Retorna a lista de composições que devem ser enviadas por e-mail</returns>
        public IList<decimal> ObterComposicoesPendentesEnvio(int areaOperacionalId, int terminalDestinoId)
        {
            var composicoesPendentes = _composicaoRepository.ObterComposicoesNaoEnviadas(areaOperacionalId, terminalDestinoId);
            return composicoesPendentes;
        }

        private byte[] ReadFully(Stream input)
        {
            var buffer = new byte[16 * 1024];
            using (var ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }
    }
}