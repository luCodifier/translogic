namespace Translogic.Modules.Core.Domain.Services.Trem
{
    using System;
    using System.Linq;
    using ALL.Core.Dominio;
    using Translogic.Modules.Core.Domain.Model.Acesso;
    using Translogic.Modules.Core.Domain.Model.Codificador.Repositories;
    using Translogic.Modules.Core.Domain.Model.Diversos.Repositories;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao.Repositories;

	/// <summary>
    /// Implementação do RelatorioAmaggiService
	/// </summary>
	public class RelatorioAmaggiService
	{
	    private IRelatorioAmaggiRepository _relatorioAmaggiRepository;

        /// <summary>
        /// Construtor Padrao
        /// </summary>
        /// <param name="relatorioAmaggiRepository">relatorioAmaggi Repository</param>
        public RelatorioAmaggiService(IRelatorioAmaggiRepository relatorioAmaggiRepository)
        {
            _relatorioAmaggiRepository = relatorioAmaggiRepository;
        }

        /// <summary>
        /// gera o relatorio da amaggi
        /// </summary>
        /// <param name="dataInicial">data inicial da filtyragem</param>
        /// <param name="dataFinal">data final da filtragem</param>
        /// <param name="estacoes">estações separadas por ;</param>
        /// <param name="emails">e-mails separados por ;</param>
        public void Gerar(DateTime dataInicial, DateTime dataFinal, string estacoes, string emails)
        {
            _relatorioAmaggiRepository.Gerar(dataInicial, dataFinal, estacoes, emails);
        }
	}
}
