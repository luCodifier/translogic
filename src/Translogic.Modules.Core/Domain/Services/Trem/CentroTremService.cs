namespace Translogic.Modules.Core.Domain.Services.Trem
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using ALL.Core.Extensions;
    using Castle.Services.Transaction;
    using DataAccess.Repositories.Trem;
    using DataAccess.Repositories.Trem.Veiculo.Locomotiva;
    using Interfaces.Trem;
    using Interfaces.Trem.OnTime;
    using Interfaces.Trem.OrdemServico;
    using Interfaces.Trem.Veiculo.Locomotiva;
    using Model.Acesso;
    using Model.Acesso.Repositories;
    using Model.Diversos;
    using Model.Diversos.Repositories;
    using Model.Trem;
    using Model.Trem.MovimentacaoTrem;
    using Model.Trem.MovimentacaoTrem.Repositories;
    using Model.Trem.OrdemServico;
    using Model.Trem.OrdemServico.Repositories;
    using Model.Trem.Repositories;
    using Model.Trem.Veiculo.Locomotiva;
    using Model.Trem.Veiculo.Locomotiva.Repositories;
    using Model.Via;
    using Model.Via.Circulacao;
    using Model.Via.Circulacao.Repositories;
    using Model.Via.Repositories;
    using ServicesClients.Domain.Models.Px;
    using ServicesClients.Domain.Models.Px.Enumerations;
    using Translogic.Core.Infrastructure;
    using SituacaoTrem = Interfaces.Trem.OnTime.SituacaoTrem;

    /// <summary>
    /// Implementa��o do servi�o de OS
    /// </summary>
    // [Transactional]
    public class CentroTremService : ICentroTremService
    {
        #region Atributos

        private readonly IAtividadeParadaOrdemServicoProgramadaRepository _atividadeParadaRepository;
        private readonly IOrdemServicoRepository _ordemServicoRepository;
        private readonly IAreaOperacionalRepository _areaOperacionalRepository;
        private readonly IRotaRepository _rotaRepository;
        private readonly IUsuarioRepository _usuarioRepository;
        private readonly ITremRepository _tremRepository;
        private readonly IPlanejamentoSerieLocomotivaRepository _planejamentoSerieLocomotivaRepository;
        private readonly IParadaOrdemServicoProgramadaRepository _paradaOrdemServicoProgramadaRepository;
        private readonly ISerieLocomotivaRepository _serieLocomotivaRepository;
        private readonly IComposicaoRepository _composicaoRepository;
        private readonly IComposicaoLocomotivaRepository _composicaoLocomotivaRepository;
        private readonly IComposicaoMaquinistaRepository _composicaoMaquinistaRepository;
        private readonly IComposicaoEquipamentoRepository _composicaoEquipamentoRepository;
        private readonly IEventoLocomotivaRepository _eventoLocomotivaRepository;
        private readonly IDuracaoAtividadeEstacaoRepository _duracaoAtividadeRepository;
        private readonly OnTimeService _onTimeService;
        private readonly IMovimentacaoTremRealRepository _movimentacaoTremRealRepository;

        #endregion

        #region Construtor

        /// <summary>
        /// Construtor padr�o
        /// </summary>
        /// <param name="ordemServicoRepository">Reposit�rio de <see cref="OrdemServico"/>. <remarks>INJETADO</remarks></param>
        /// <param name="areaOperacionalRepository">Reposit�rio de <see cref="IAreaOperacional"/>. <remarks>INJETADO</remarks></param>
        /// <param name="rotaRepository">Reposit�rio de <see cref="Rota"/>. <remarks>INJETADO</remarks></param>
        /// <param name="usuarioRepository">Reposit�rio de <see cref="Usuario"/>. <remarks>INJETADO</remarks></param>
        /// <param name="tremRepository">Reposit�rio de <see cref="Trem"/>. <remarks>INJETADO</remarks></param>
        /// <param name="planejamentoSerieLocomotivaRepository">Reposit�rio de <see cref="PlanejamentoSerieLocomotiva"/>. <remarks>INJETADO</remarks></param>
        /// <param name="serieLocomotivaRepository">Reposit�rio de <see cref="SerieLocomotivaRepository"/>. <remarks>INJETADO</remarks></param>
        /// <param name="composicaoRepository">Reposit�rio de <see cref="ComposicaoRepository"/>. <remarks>INJETADO</remarks></param>
        /// <param name="composicaoLocomotivaRepository">Reposit�rio de <see cref="ComposicaoLocomotivaRepository"/>. <remarks>INJETADO</remarks></param>
        /// <param name="composicaoMaquinistaRepository">Reposit�rio de <see cref="ComposicaoMaquinistaRepository"/>. <remarks>INJETADO</remarks></param>
        /// <param name="composicaoEquipamentoRepository">Reposit�rio de <see cref="ComposicaoEquipamentoRepository"/>. <remarks>INJETADO</remarks></param>
        /// <param name="eventoLocomotivaRepository">Reposit�rio de <see cref="EventoLocomotivaRepository"/>. <remarks>INJETADO</remarks></param>
        /// <param name="paradaOrdemServicoProgramadaRepository">Reposit�rio de <see cref="ParadaOrdemServicoProgramada"/> INJETADO</param>
        /// <param name="atividadeParadaRepository">Reposit�rio de <see cref="ParadaOrdemServicoProgramada"/> INJETADA</param>
        /// <param name="duracaoAtividadeRepository">Reposit�rio de <see cref="DuracaoAtividadeEstacao"/> INJETADO</param>
        /// <param name="onTimeService">Servi�o de <see cref="onTimeService"/>INJETADO</param>
        /// <param name="movimentacaoTremRealRepository">Reposit�rio de <see cref="movimentacaoTremRealRepository"/>INJETADO</param>
        public CentroTremService(
                IOrdemServicoRepository ordemServicoRepository,
                IAreaOperacionalRepository areaOperacionalRepository,
                IRotaRepository rotaRepository,
                IUsuarioRepository usuarioRepository,
                ITremRepository tremRepository,
                IPlanejamentoSerieLocomotivaRepository planejamentoSerieLocomotivaRepository,
                ISerieLocomotivaRepository serieLocomotivaRepository,
                IComposicaoRepository composicaoRepository,
                IComposicaoLocomotivaRepository composicaoLocomotivaRepository,
                IComposicaoMaquinistaRepository composicaoMaquinistaRepository,
                IComposicaoEquipamentoRepository composicaoEquipamentoRepository,
                IEventoLocomotivaRepository eventoLocomotivaRepository,
                IParadaOrdemServicoProgramadaRepository paradaOrdemServicoProgramadaRepository,
                IAtividadeParadaOrdemServicoProgramadaRepository atividadeParadaRepository,
                IDuracaoAtividadeEstacaoRepository duracaoAtividadeRepository,
                OnTimeService onTimeService,
                IMovimentacaoTremRealRepository movimentacaoTremRealRepository)
        {
            _rotaRepository = rotaRepository;
            _onTimeService = onTimeService;
            _movimentacaoTremRealRepository = movimentacaoTremRealRepository;
            _paradaOrdemServicoProgramadaRepository = paradaOrdemServicoProgramadaRepository;
            _tremRepository = tremRepository;
            _usuarioRepository = usuarioRepository;
            _ordemServicoRepository = ordemServicoRepository;
            _rotaRepository = rotaRepository;
            _areaOperacionalRepository = areaOperacionalRepository;
            _planejamentoSerieLocomotivaRepository = planejamentoSerieLocomotivaRepository;
            _serieLocomotivaRepository = serieLocomotivaRepository;
            _composicaoRepository = composicaoRepository;
            _composicaoLocomotivaRepository = composicaoLocomotivaRepository;
            _composicaoMaquinistaRepository = composicaoMaquinistaRepository;
            _composicaoEquipamentoRepository = composicaoEquipamentoRepository;
            _eventoLocomotivaRepository = eventoLocomotivaRepository;
            _duracaoAtividadeRepository = duracaoAtividadeRepository;
            _atividadeParadaRepository = atividadeParadaRepository;
        }

        #endregion

        #region Obter

        /// <summary>
        /// Obtem todas as OSs suprimidas no intervalo informado
        /// </summary>
        /// <param name="dataInicial">Data inicial (Data oficial de confirma��o da partida prevista)</param>
        /// <param name="dataFinal">Data final (Data oficial de confirma��o da partida prevista)</param>
        /// <returns>Lista de <see cref="OrdemServico"/></returns>
        public IList<OrdemServicoDto> ObterOSSuprimidas(DateTime dataInicial, DateTime dataFinal)
        {
            IList<OrdemServicoDto> ordens = _ordemServicoRepository.ObterOSSuprimidas(dataInicial, dataFinal).Map<OrdemServico, OrdemServicoDto>();
            return ordens;
        }

        /// <summary>
        /// Obtem as OS por <see cref="SituacaoOrdemServicoEnum"/>
        /// </summary>
        /// <param name="situacao">Situa��o da OS</param>
        /// <param name="prefixo">Prefixo da ordem de servi�o</param>
        /// <param name="origem">Origem da ordem de servi�o</param>
        /// <param name="destino">Destino da ordem de servi�o</param>
        /// <param name="inicial">Data inicial (Data oficial de confirma��o da partida prevista)</param>
        /// <param name="final">Data final (Data oficial de confirma��o da partida prevista)</param>
        /// <returns>Lista de <see cref="OrdemServico"/></returns>
        public IList<OrdemServicoDto> ObterPorSituacaoEDataPartidaPrevistaOficial(SituacaoOrdemServicoEnum situacao, string prefixo, string origem, string destino, DateTime inicial, DateTime final)
        {
            IList<OrdemServicoDto> ordens = _ordemServicoRepository.ObterPorTipoSituacaoEDataPartidaPrevistaOficial((int)situacao, prefixo, origem, destino, inicial, final).Map<OrdemServico, OrdemServicoDto>();
            return ordens;
        }

        /// <summary>
        /// Processa uma lista de ordem de servi�o
        /// </summary>
        /// <param name="ordensServico">Ordens de servi�o que ser�o gravadas</param>
        /// <returns>Ordens de servico com os dados atualizados</returns>
        public IList<OrdemServicoDto> ProcessarOrdemServico(IList<OrdemServicoDto> ordensServico)
        {
            return null;
        }

        /// <summary>
        /// Obtem todas as SeriesLocomotivas
        /// </summary>
        /// <returns>Lista contendo todas as SerieLocomotivaDTO</returns>
        public IList<SerieLocomotivaDto> ObterTodasSeriesLocomotivas()
        {
            return _serieLocomotivaRepository.ObterTodos().Map<SerieLocomotiva, SerieLocomotivaDto>();
        }

        #endregion

        #region ObterFichaTrem

        /// <summary>
        /// Obtem a ficha do trem
        /// </summary>
        /// <param name="idOs">Id da PreOS no TLFerro</param>
        /// <returns>Objeto FichaTremDto</returns>
        public FichaTremDto ObterFichaTrem(int idOs)
        {
            return ObterFichaTrem(idOs, true);
        }

        /// <summary>
        /// Obtem a ficha do trem
        /// </summary>
        /// <param name="idPreOs">Id da PreOS no TLFerro</param>
        /// <param name="isOs">Flag para procura por Os</param>
        /// <returns>Objeto FichaTremDto</returns>
        public FichaTremDto ObterFichaTrem(int idPreOs, bool isOs)
        {
            OrdemServico os = _ordemServicoRepository.ObterPorId(idPreOs);
            if (os == null)
            {
                return null;
            }

            Trem trem = _tremRepository.ObterPorIdOrdemServico(isOs ? idPreOs : os.IdPreOs.Value);
            if (trem == null)
            {
                return null;
            }

            Composicao composicao = _composicaoRepository.ObterUltimaPorIdTrem(trem.Id.Value);
            if (composicao == null)
            {
                return null;
            }

            ComposicaoMaquinista composicaoMaquinista = _composicaoMaquinistaRepository.ObterPorComposicao(composicao);
            var fichaTrem = new FichaTremDto();
            if (composicaoMaquinista != null)
            {
                fichaTrem.NomeMaquinista = composicaoMaquinista.Maquinista.Nome;
                fichaTrem.MatriculaMaquinista = composicaoMaquinista.Maquinista.Matricula;
            }

            fichaTrem.Tonelagem = composicao.TB;
            fichaTrem.Comprimento = composicao.Comprimento;

            IList<ComposicaoLocomotiva> composicaoLocomotivas = _composicaoLocomotivaRepository.ObterPorComposicao(composicao);
            IList<Locomotiva> locos = new List<Locomotiva>();
            if (composicaoLocomotivas.Count > 0)
            {
                foreach (ComposicaoLocomotiva cl in composicaoLocomotivas)
                {
                    fichaTrem.Locomotivas += " - " + cl.Locomotiva.Codigo;
                    locos.Add(cl.Locomotiva);
                }

                fichaTrem.Locomotivas = fichaTrem.Locomotivas.Remove(0, 3);
            }

            DateTime dataFim = composicao.DataFim.HasValue ? composicao.DataFim.Value : DateTime.Now;
            IList<EventoLocomotiva> eventoLocomotivas = _eventoLocomotivaRepository.ObterPorDataELocomotivas(composicao.DataInicio, dataFim, locos);
            foreach (EventoLocomotiva el in eventoLocomotivas)
            {
                if ("ANX".Equals(el.Evento.Codigo))
                {
                    DetalheFichaTremDto det = GetDetalheFichaTrem(fichaTrem, el.Locomotiva.Codigo);
                    if (!det.DataAnexacao.HasValue
                            || el.DataOcorrencia < det.DataAnexacao)
                    {
                        det.DataAnexacao = el.DataOcorrencia;
                    }
                }
                else if ("DNX".Equals(el.Evento.Codigo))
                {
                    DetalheFichaTremDto det = GetDetalheFichaTrem(fichaTrem, el.Locomotiva.Codigo);
                    if (!det.DataDesanexacao.HasValue
                            || det.DataDesanexacao < el.DataOcorrencia)
                    {
                        det.DataDesanexacao = el.DataOcorrencia;
                    }
                }
            }

            /*
                        IList<ComposicaoVagao> composicaoVagoes = composicaoVagaoRepository.ObterPorComposicao(composicao);

                        composicaoVagoes = composicaoVagoes.OrderBy(cv => cv.Sequencia).ToList();
                        foreach (ComposicaoVagao cv in composicaoVagoes)
                        {
                            fichaTrem.UltimoVagao = cv.Vagao.Codigo;
                            if (cv.IndCarregado.Value)
                            {
                                fichaTrem.QuantidadeVagoesCarregados++;
                            }
                            else
                            {
                                fichaTrem.QuantidadeVagoesVazios++;
                            }
                        }
            */

            IList<ComposicaoEquipamento> composicaoEquipamentos = _composicaoEquipamentoRepository.ObterPorComposicao(composicao);
            foreach (ComposicaoEquipamento ce in composicaoEquipamentos)
            {
                if (ce.Equipamento.DescricaoResumida.Equals("TELEM�TRICO")
                        || ce.Equipamento.DescricaoResumida.Equals("Telam�trico ")
                        || ce.Equipamento.DescricaoResumida.Equals("Telemetrico")
                        || ce.Equipamento.DescricaoResumida.Equals("Telem�trico")
                        || ce.Equipamento.DescricaoResumida.Equals("Telem�trico ")
                        || ce.Equipamento.DescricaoResumida.Equals("Telem�trico  "))
                {
                    if (fichaTrem.Telemetrico)
                    {
                        fichaTrem.NumeroTelemetrico += " - ";
                    }

                    fichaTrem.Telemetrico = true;
                    fichaTrem.NumeroTelemetrico = ce.Equipamento.Codigo;
                }
            }

            return fichaTrem;
        }

        #endregion

        #region Fechar e Suprimir OSs

        /// <summary>
        /// Alocar s�ries de locomotivas para o <see cref="OrdemServicoDto"/>
        /// </summary>
        /// <param name="idOrdemServico">Ordem de servico que receber� o planejamento de s�ries</param>
        /// <param name="series">Lista de s�ries de locomotivas planejadas <see cref="PlanejamentoSerieLocomotivaDto"/></param>
        [Transaction]
        public virtual void AlocarSeriesLocomotivasOrdemServico(int idOrdemServico, IList<PlanejamentoSerieLocomotivaDto> series)
        {
            OrdemServico ordemServico = _ordemServicoRepository.ObterPorId(idOrdemServico);
            IList<PlanejamentoSerieLocomotiva> pendentes = series.Map<PlanejamentoSerieLocomotivaDto, PlanejamentoSerieLocomotiva>();

            if (ordemServico == null)
            {
                throw new Exception("Ordem de servi�o n�o encontrada!");
            }

            if (ordemServico.Situacao.Id != (int)SituacaoOrdemServicoEnum.EmPlanejamento && ordemServico.Situacao.Id != (int)SituacaoOrdemServicoEnum.EmProgramacao)
            {
                throw new Exception("S� � poss�vel planejar as s�ries de locomotivas enquanto a ordem de servi�o estiver EM PROGRAMA��O ou EM PLANEJAMENTO!");
            }

            _planejamentoSerieLocomotivaRepository.ApagarVigentes(idOrdemServico);

            foreach (PlanejamentoSerieLocomotiva serie in pendentes)
            {
                _planejamentoSerieLocomotivaRepository.Inserir(serie);
            }
        }

        /// <summary>
        /// Fecha o planejamento e a programa��o de uma ordem de servi�o
        /// </summary>
        /// <param name="ordemServico">Ordem de servi�o que ser� gravada</param>
        /// <returns>Ordem de servico com os dados atualizados</returns>
        public OrdemServicoDto FecharOrdemServico(OrdemServicoDto ordemServico)
        {
            Usuario usuarioFechamento = _usuarioRepository.ObterPorCodigo(ordemServico.UsuarioFechamento.Codigo);

            if (usuarioFechamento == null)
            {
                throw new TranslogicException("Usu�rio de fechamento n�o existe!");
            }

            if (!usuarioFechamento.Ativo.GetValueOrDefault())
            {
                throw new TranslogicException(string.Format("O usu�rio de fechamento {0} n�o est� ativo no sistema!", usuarioFechamento.Codigo));
            }

            OrdemServico fechar = _ordemServicoRepository.ObterPorId(ordemServico.Id);
            if (fechar == null)
            {
                throw new Exception(string.Format("S� � poss�vel fechar ordens de servi�o (IdPreOS: {0}) v�lidas no Translogic.", ordemServico.Id));
            }

            if (fechar.Situacao.Id.HasValue && fechar.Situacao.Id.Value != (int)SituacaoOrdemServicoEnum.EmPlanejamento)
            {
                throw new Exception("S� � poss�vel fechar o planejamento de ordens de servi�o com situa��o (EM PLANEJAMENTO).");
            }

            Atualizar(ordemServico, fechar);

            fechar.QtdeVagoesVazios = ordemServico.QtdeVagoesVazios;
            fechar.QtdeVagoesCarregados = ordemServico.QtdeVagoesCarregados;
            fechar.Garantido = ordemServico.Garantido;

            fechar.DataPartidaPrevistaOficial = ordemServico.DataPartidaPrevistaOficial;
            fechar.DataChegadaPrevistaOficial = ordemServico.DataChegadaPrevistaOficial;
            fechar.UsuarioFechamento = usuarioFechamento;

            _ordemServicoRepository.Atualizar(fechar);

            OrdemServico fechadaPlanejamento = _ordemServicoRepository.FecharPlanejamento(fechar);
            if (fechadaPlanejamento == null)
            {
                throw new Exception(string.Format("Imposs�vel fechar OS Id: {0} no sistema", ordemServico.Id));
            }

            int idNovaOs = fechadaPlanejamento.IdPreOs.GetValueOrDefault();

            if (idNovaOs == 0)
            {
                throw new Exception(string.Format("Ordens de servi�o fechada n�o foi processada corretamente (IdOS: {0}) v�lidas no Translogic.", fechadaPlanejamento.Id));
            }

            fechadaPlanejamento = _ordemServicoRepository.ObterPorId(idNovaOs);
            if (fechadaPlanejamento == null)
            {
                throw new Exception(string.Format("S� � poss�vel fechar ordens de servi�o (IdOS: {0}) v�lidas no Translogic.", idNovaOs));
            }

            fechadaPlanejamento.UsuarioFechamento = usuarioFechamento;

            if (fechadaPlanejamento.Situacao.Id.GetValueOrDefault() != (int)SituacaoOrdemServicoEnum.EmProgramacao)
            {
                throw new Exception("S� � poss�vel fechar a programa��o de ordens de servi�o com situa��o (EM PROGRAMA��O).");
            }

            OrdemServico fechadaProgramacao = _ordemServicoRepository.FecharProgramacao(fechadaPlanejamento);
            if (fechadaProgramacao == null)
            {
                throw new Exception(string.Format("S� � poss�vel fechar ordens de servi�o (IdOS: {0}) v�lidas no Translogic.", fechadaPlanejamento.Id));
            }

            fechadaProgramacao.UsuarioCadastro = null;
            fechadaProgramacao.UsuarioFechamento = null;

            return fechadaProgramacao.Map<OrdemServico, OrdemServicoDto>();
        }

        /// <summary>
        /// Fechar o planejamento de ordem servi�o da grade
        /// </summary>
        /// <param name="ordemServico">Ordem de servico que ser� fechado o planejamento</param>
        /// <returns>Ordem de servi�o atualizada</returns>
        public OrdemServicoDto FecharPlanejamentoOrdemServico(OrdemServicoDto ordemServico)
        {
            Usuario usuarioFechamento = _usuarioRepository.ObterPorCodigo(ordemServico.UsuarioFechamento.Codigo);

            if (usuarioFechamento == null)
            {
                throw new TranslogicException("Usu�rio de fechamento n�o existe!");
            }

            if (usuarioFechamento.Ativo.HasValue && !usuarioFechamento.Ativo.Value)
            {
                throw new TranslogicException(string.Format("O usu�rio de fechamento {0} n�o est� ativo no sistema!", usuarioFechamento.Codigo));
            }

            OrdemServico fechar = _ordemServicoRepository.ObterPorId(ordemServico.Id);
            if (fechar == null)
            {
                throw new Exception(string.Format("S� � poss�vel fechar ordens de servi�o (IdPreOS: {0}) v�lidas no Translogic.", ordemServico.Id));
            }

            if (fechar.Situacao.Id.HasValue && fechar.Situacao.Id.Value != (int)SituacaoOrdemServicoEnum.EmPlanejamento)
            {
                throw new Exception("S� � poss�vel fechar o planejamento de ordens de servi�o com situa��o (EM PLANEJAMENTO).");
            }

            Atualizar(ordemServico, fechar);

            fechar.QtdeVagoesVazios = ordemServico.QtdeVagoesVazios;
            fechar.QtdeVagoesCarregados = ordemServico.QtdeVagoesCarregados;
            fechar.Garantido = ordemServico.Garantido;

            fechar.DataPartidaPrevistaOficial = ordemServico.DataPartidaPrevistaOficial;
            fechar.DataChegadaPrevistaOficial = ordemServico.DataChegadaPrevistaOficial;
            fechar.UsuarioFechamento = usuarioFechamento;

            _ordemServicoRepository.Atualizar(fechar);

            fechar = _ordemServicoRepository.FecharPlanejamento(fechar);

            // Obtem o numero da nova OS
            OrdemServico fecharClone = _ordemServicoRepository.ObterPorId(fechar.IdPreOs);
            if (fecharClone == null)
            {
                throw new Exception(string.Format("N�o � poss�vel encontrar ordens de servi�o (IdOS: {0}) v�lidas no Translogic.", fechar.IdPreOs));
            }

            fechar.Numero = fecharClone.Numero;
            fechar.UsuarioCadastro = null;
            fechar.UsuarioFechamento = null;

            return fechar.Map<OrdemServico, OrdemServicoDto>();
        }

        /// <summary>
        /// Fechar a programacao da ordem servi�o
        /// </summary>
        /// <param name="ordemServico">Ordem de servico que ser� fechada a programa��o</param>
        /// <returns>Ordem de servi�o atualizada</returns>
        public OrdemServicoDto FecharProgramacaoOrdemServico(OrdemServicoDto ordemServico)
        {
            Usuario usuarioFechamento = _usuarioRepository.ObterPorCodigo(ordemServico.UsuarioFechamento.Codigo);

            if (usuarioFechamento == null)
            {
                throw new TranslogicException("Usu�rio de fechamento n�o existe!");
            }

            if (!usuarioFechamento.Ativo.GetValueOrDefault())
            {
                throw new TranslogicException(string.Format("O usu�rio de fechamento {0} n�o est� ativo no sistema!", usuarioFechamento.Codigo));
            }

            OrdemServico fechar = _ordemServicoRepository.ObterPorId(ordemServico.Id);
            if (fechar == null)
            {
                throw new Exception(string.Format("S� � poss�vel fechar ordens de servi�o (IdPreOS: {0}) v�lidas no Translogic.", ordemServico.Id));
            }

            int idNovaOs = fechar.IdPreOs.Value;
            fechar = _ordemServicoRepository.ObterPorId(idNovaOs);
            if (fechar == null)
            {
                throw new Exception(string.Format("S� � poss�vel fechar ordens de servi�o (IdOS: {0}) v�lidas no Translogic.", idNovaOs));
            }

            fechar.UsuarioFechamento = usuarioFechamento;

            if (fechar.Situacao.Id.Value != (int)SituacaoOrdemServicoEnum.EmProgramacao)
            {
                throw new Exception("S� � poss�vel fechar a programa��o de ordens de servi�o com situa��o (EM PROGRAMA��O).");
            }

            fechar = _ordemServicoRepository.FecharProgramacao(fechar);
            if (fechar == null)
            {
                throw new Exception(string.Format("S� � poss�vel fechar ordens de servi�o (IdOS: {0}) v�lidas no Translogic.", idNovaOs));
            }

            fechar.UsuarioCadastro = null;
            fechar.UsuarioFechamento = null;

            return fechar.Map<OrdemServico, OrdemServicoDto>();
        }

        /// <summary>
        /// Suprimir (cancelar) ordem servi�o
        /// </summary>
        /// <param name="ordemServico">Ordem de servico que ser� suprimida</param>
        public void SuprimirOrdemServico(OrdemServicoDto ordemServico)
        {
            Usuario usuarioSupressao = _usuarioRepository.ObterPorCodigo(ordemServico.UsuarioSupressao.Codigo);
            if (usuarioSupressao == null)
            {
                throw new TranslogicException("Usu�rio de supress�o n�o existe!");
            }

            OrdemServico suprimir = _ordemServicoRepository.ObterPorId(ordemServico.Id);
            if (suprimir == null)
            {
                throw new Exception("N�o � poss�vel suprimir uma ordem de servi�o n�o encontrado.");
            }

            Trem trem = _tremRepository.ObterPorIdOrdemServico(ordemServico.Id);

            if (trem != null)
            {
                throw new Exception("N�o � poss�vel suprimir uma ordem de servi�o vinculada a um trem.");
            }

            switch (suprimir.Situacao.Id)
            {
                // Supress�o antes da confirma��o
                case (int)SituacaoOrdemServicoEnum.EmPlanejamento:
                    {
                        suprimir.Situacao.Id = (int)SituacaoOrdemServicoEnum.SuprimidaEmPlanejamento;
                        suprimir.UsuarioSupressao = usuarioSupressao;
                        suprimir.DataSupressao = ordemServico.DataSupressao;

                        _ordemServicoRepository.Suprimir(suprimir);
                        break;
                    }

                // Supress�o antes da confirma��o
                case (int)SituacaoOrdemServicoEnum.Pendente:
                    {
                        suprimir.Situacao.Id = (int)SituacaoOrdemServicoEnum.SuprimidaEmProgramacao;
                        suprimir.UsuarioSupressao = usuarioSupressao;
                        suprimir.DataSupressao = ordemServico.DataSupressao;

                        _ordemServicoRepository.Suprimir(suprimir);
                        break;
                    }

                // Supress�o apos ter sido confirmada na grade
                case (int)SituacaoOrdemServicoEnum.Planejada:
                    {
                        suprimir.Situacao.Id = (int)SituacaoOrdemServicoEnum.SuprimidaEmPlanejamento;
                        suprimir.UsuarioSupressao = usuarioSupressao;
                        suprimir.DataSupressao = ordemServico.DataSupressao;

                        _ordemServicoRepository.Suprimir(suprimir);
                        break;
                    }

                // Supress�o apos ter sido confirmada na grade
                case (int)SituacaoOrdemServicoEnum.EmProgramacao:
                case (int)SituacaoOrdemServicoEnum.Programada:
                    {
                        suprimir = _ordemServicoRepository.ObterPorId(suprimir.Id);

                        if (suprimir.Situacao.Id == (int)SituacaoOrdemServicoEnum.Programada)
                        {
                            suprimir.Situacao.Id = (int)SituacaoOrdemServicoEnum.SuprimidaEmPrograma;
                        }
                        else
                        {
                            suprimir.Situacao.Id = (int)SituacaoOrdemServicoEnum.SuprimidaEmProgramacao;
                        }

                        suprimir.UsuarioSupressao = usuarioSupressao;
                        suprimir.DataSupressao = ordemServico.DataSupressao;

                        _ordemServicoRepository.Suprimir(suprimir);
                        break;
                    }

                default:
                    {
                        throw new Exception("A ordem de servi�o n�o pode ser suprimida pois est� em uma situa��o inv�lida.");
                    }
            }
        }

        #endregion

        #region OS

        /// <summary>
        /// Insere uma nova ordem de servi�o
        /// </summary>
        /// <param name="ordemServico">Ordem de servi�o que ser� gravada</param>
        /// <returns>Ordem de servico com os dados atualizados</returns>
        public OrdemServicoDto Inserir(OrdemServicoDto ordemServico)
        {
            if (ordemServico.Origem == null || ordemServico.Destino == null)
            {
                throw new TranslogicException("A OS deve ter origem e destino, sendo esses como p�tio de circula��o");
            }

            if (ordemServico.Usuario == null)
            {
                throw new ArgumentException("A OS n�o possui usu�rio");
            }

            if (!ordemServico.Origem.Codigo.EndsWith("-PC"))
            {
                ordemServico.Origem.Codigo += "-PC";
            }

            var origem = (PatioCirculacao)_areaOperacionalRepository.ObterPorCodigo(ordemServico.Origem.Codigo);

            if (!ordemServico.Destino.Codigo.EndsWith("-PC"))
            {
                ordemServico.Destino.Codigo += "-PC";
            }

            var destino = (PatioCirculacao)_areaOperacionalRepository.ObterPorCodigo(ordemServico.Destino.Codigo);

            if (origem == null || destino == null)
            {
                throw new TranslogicException("A origem " + ordemServico.Origem.Codigo + " ou o destino " + ordemServico.Destino.Codigo + " n�o foi encontrado");
            }

            Rota rota;

            if (ordemServico.Rota == null || ordemServico.Rota.Id == default(int))
            {
                rota = _rotaRepository.ObterPorOrigemDestino(origem, destino);

                if (rota == null || !rota.Ativa)
                {
                    throw new Exception(string.Format("A rota da OS n�o foi encontrada de {0} at� {1} ou n�o ativa", origem, destino));
                }
            }
            else
            {
                rota = _rotaRepository.ObterPorId(ordemServico.Rota.Id);

                if (!rota.Ativa)
                {
                    throw new Exception("A rota n�o est� ativa!");
                }
            }

            ordemServico.Origem = null;
            ordemServico.Destino = null;
            ordemServico.DataChegadaPrevistaGrade = null;
            ordemServico.DataPartidaPrevistaGrade = null;

            OrdemServico novaOrdemServico = ordemServico.Map<OrdemServicoDto, OrdemServico>();

            novaOrdemServico.Situacao = new SituacaoOrdemServico { Id = (int)SituacaoOrdemServicoEnum.Pendente };
            novaOrdemServico.Origem = origem.EstacaoMae;
            novaOrdemServico.Destino = destino.EstacaoMae;
            novaOrdemServico.Rota = rota;
            if (!novaOrdemServico.Rota.Ativa)
            {
                throw new Exception("A rota n�o est� ativa!");
            }

            novaOrdemServico.UsuarioCadastro = _usuarioRepository.ObterPorCodigo(ordemServico.Usuario.Codigo);
            if (novaOrdemServico.UsuarioCadastro == null ||
                !novaOrdemServico.UsuarioCadastro.Ativo.HasValue ||
                !novaOrdemServico.UsuarioCadastro.Ativo.Value)
            {
                throw new Exception("O usu�rio de cadastro n�o est� ativo no sistema!");
            }

            // Insere a ordem em status PENDENTE
            novaOrdemServico = _ordemServicoRepository.Inserir(novaOrdemServico);
            if (novaOrdemServico == null)
            {
                throw new Exception(string.Format("Falha ao inserir OS (Prefixo: {0}, Origem: {1}, Destino: {2}) manualmente no sistema", ordemServico.Prefixo, origem.EstacaoMae.Codigo, destino.EstacaoMae.Codigo));
            }

            Atualizar(ordemServico, novaOrdemServico);

            novaOrdemServico.UsuarioFechamento = _usuarioRepository.ObterPorCodigo(ordemServico.UsuarioFechamento.Codigo);
            if (novaOrdemServico.UsuarioFechamento == null ||
                !novaOrdemServico.UsuarioFechamento.Ativo.HasValue ||
                !novaOrdemServico.UsuarioFechamento.Ativo.Value)
            {
                throw new Exception("O usu�rio de fechamento n�o est� ativo no sistema!");
            }

            OrdemServico fechar = _ordemServicoRepository.FecharPlanejamento(novaOrdemServico);
            if (fechar == null)
            {
                throw new Exception(string.Format("Imposs�vel fechar OS {0}(Id: {1}) no sistema", novaOrdemServico.Numero, novaOrdemServico.Id));
            }

            OrdemServico fecharClone = _ordemServicoRepository.ObterPorId(fechar.IdPreOs);
            if (fecharClone == null)
            {
                throw new Exception(string.Format("N�o � poss�vel encontrar ordens de servi�o (IdOS: {0}) v�lidas no Translogic.", fechar.IdPreOs));
            }

            fecharClone.UsuarioFechamento = novaOrdemServico.UsuarioFechamento;
            fecharClone = _ordemServicoRepository.FecharProgramacao(fecharClone);

            fecharClone.UsuarioCadastro = null;
            fecharClone.UsuarioFechamento = null;

            return fecharClone.Map<OrdemServico, OrdemServicoDto>();
        }

        /// <summary>
        /// Insere uma nova ordem de servi�o
        /// </summary>
        /// <param name="idOs"> Id da Ordem de servi�o que ser� persistida</param>
        /// <param name="dataPartida"> Data Partida da Ordem de servi�o que ser� persistida</param>
        /// <param name="usuario">Usuario que solicitou a altera��o.</param>
        /// <returns>Ordem de servico com os dados atualizados</returns>
        public ResultadoOrdemServico<OrdemServicoDto> AtualizarDataPartidaOficial(int idOs, DateTime dataPartida, string usuario)
        {
            string mensagem = string.Empty;
            bool sucesso = true;
            OrdemServico novaOrdemServico = null;

            try
            {
                novaOrdemServico = _ordemServicoRepository.AtualizarDataPartidaOficial(idOs, dataPartida, usuario);
            }
            catch (Exception ex)
            {
                sucesso = false;
                mensagem = string.Concat("Erro ao atualizar a ordem de servi�o. ", ex.Message);
            }

            return new ResultadoOrdemServico<OrdemServicoDto>
                       {
                           Mensagem = mensagem,
                           Sucesso = sucesso,
                           OrdensServico = novaOrdemServico != null ? new List<OrdemServicoDto> { novaOrdemServico.Map<OrdemServico, OrdemServicoDto>() } : null
                       };
        }

        /// <summary>
        /// Retorna as OS que tiveram a data de partida alterada
        /// </summary>
        /// <param name="inicial">Data inicial da �ltima altera��o</param>
        /// <param name="final">Data final da �ltima altera��o</param>
        /// <returns>Ordem de servico com os dados atualizados</returns>
        public ResultadoOrdemServico<LogAlteracaoOsDto> ObterAlteracoesDataPartida(DateTime inicial, DateTime? final)
        {
            string mensagem = string.Empty;
            bool sucesso = true;
            IList<LogAlteracaoOsDto> ordemServicoAlteradas = null;

            try
            {
                ordemServicoAlteradas = _ordemServicoRepository.ObterAlteracoesDataPartida(inicial, final);
            }
            catch (Exception ex)
            {
                sucesso = false;
                mensagem = string.Concat("Erro ao obter altera��es da ordem de servi�o. ", ex.Message);
            }

            return new ResultadoOrdemServico<LogAlteracaoOsDto>
                       {
                           Mensagem = mensagem,
                           Sucesso = sucesso,
                           OrdensServico = ordemServicoAlteradas
                       };
        }

        /// <summary>
        /// Obtem a OS do trem por id
        /// </summary>
        /// <param name="idOs">Id da OS para importar</param>
        /// <returns>OS importada</returns>
        public OrdemServicoDto ObterOSPorId(int idOs)
        {
            return _ordemServicoRepository.ObterPorId(idOs).Map<OrdemServico, OrdemServicoDto>();
        }

        /// <summary>
        /// Obtem a OS do trem por id
        /// </summary>
        /// <param name="idPreOs">Id da OS para importar</param>
        /// <returns>OS importada</returns>
        public OrdemServicoDto ObterPreOSPorId(int idPreOs)
        {
            return _ordemServicoRepository.ObterPorIdPreOs(idPreOs).Map<OrdemServico, OrdemServicoDto>();
        }

        /// <summary>
        /// Obtem a OS do trem por numero
        /// </summary>
        /// <param name="numeroOrdemServico">Numero da OS para importar</param>
        /// <returns>OS importada</returns>
        public OrdemServicoDto ObterOSPorNumero(int numeroOrdemServico)
        {
            return _ordemServicoRepository.ObterPorNumero(numeroOrdemServico).Map<OrdemServico, OrdemServicoDto>();
        }

        /// <summary>
        /// Obtem os dados das locomotivas da OS
        /// </summary>
        /// <param name="numeroOS">N�mero da OS</param>
        /// <returns>Lista das Locomotivas da OS</returns>
        public LocomotivasOSDto ObterDadosLocomotivasOS(int numeroOS)
        {
            var objOS = _ordemServicoRepository.ObterPorNumero(numeroOS).Map<OrdemServico, OrdemServicoDto>();

            if (objOS == null)
            {
                return null;
            }

            var locomotivas = _ordemServicoRepository.ObterLocomotivasPorOS(numeroOS);

            return new LocomotivasOSDto
            {
                Id = objOS.Id,
                Numero = objOS.Numero ?? 0,
                Prefixo = objOS.Prefixo,
                Locomotivas = locomotivas
            };
        }

        #endregion

        #region OnTime

        /// <summary>
        /// Obtem a lista de Partidas de OSs para OnTime
        /// </summary>
        /// <param name="requisicao">Requisicao de Dados de OnTime</param>
        /// <returns>Dados de OnTime</returns>
        public DadosOnTime ObterDadosOnTime(RequisicaoDadosOnTime requisicao)
        {
            var resultado = new DadosOnTime();

            if (requisicao == null)
            {
                resultado.Detalhes = string.Format("Dados da pesquisa inv�lidos para consulta");

                return resultado;
            }

            DateTime dataInicial;
            DateTime dataFinal;

            bool datasFiltro = false;

            // Data de filtro de partidas
            if (requisicao.DataInicialPartida.HasValue &&
                requisicao.DataFinalPartida.HasValue)
            {
                dataInicial = requisicao.DataInicialPartida.Value;
                dataFinal = requisicao.DataFinalPartida.Value;

                TimeSpan intervalo = dataFinal.Subtract(dataInicial).Duration();

                if (intervalo.TotalDays > 31)
                {
                    resultado.Detalhes = string.Format("Intervalo de pesquisas para partida deve ser inferior a 31 dias (Atual {0} dias)", (int)intervalo.TotalDays);

                    return resultado;
                }

                datasFiltro = true;
            }

            // Data de filtro de chegadas
            if (requisicao.DataInicialChegada.HasValue &&
                requisicao.DataFinalChegada.HasValue)
            {
                dataInicial = requisicao.DataInicialChegada.Value;
                dataFinal = requisicao.DataFinalChegada.Value;

                TimeSpan intervalo = dataFinal.Subtract(dataInicial).Duration();

                if (intervalo.TotalDays > 31)
                {
                    resultado.Detalhes = string.Format("Intervalo de pesquisas para chegada deve ser inferior a 31 dias (Atual {0} dias)", (int)intervalo.TotalDays);

                    return resultado;
                }

                datasFiltro = true;
            }

            // Data de filtro de chegadas
            if (requisicao.DataInicialCadastro.HasValue &&
                requisicao.DataFinalCadastro.HasValue)
            {
                dataInicial = requisicao.DataInicialCadastro.Value;
                dataFinal = requisicao.DataFinalCadastro.Value;

                TimeSpan intervalo = dataFinal.Subtract(dataInicial).Duration();

                if (intervalo.TotalDays > 31)
                {
                    resultado.Detalhes = string.Format("Intervalo de pesquisas para cadastro deve ser inferior a 31 dias (Atual {0} dias)", (int)intervalo.TotalDays);

                    return resultado;
                }

                datasFiltro = true;
            }

            // Data de filtro de edi��o
            if (requisicao.DataInicialEdicao.HasValue &&
                requisicao.DataFinalEdicao.HasValue)
            {
                dataInicial = requisicao.DataInicialEdicao.Value;
                dataFinal = requisicao.DataFinalEdicao.Value;

                TimeSpan intervalo = dataFinal.Subtract(dataInicial).Duration();

                if (intervalo.TotalDays > 31)
                {
                    resultado.Detalhes = string.Format("Intervalo de pesquisas para edi��o deve ser inferior a 31 dias (Atual {0} dias)", (int)intervalo.TotalDays);

                    return resultado;
                }

                datasFiltro = true;
            }

            // Data de filtro de edi��o
            if (requisicao.DataInicialSupressao.HasValue &&
                requisicao.DataFinalSupressao.HasValue)
            {
                dataInicial = requisicao.DataInicialSupressao.Value;
                dataFinal = requisicao.DataFinalSupressao.Value;

                TimeSpan intervalo = dataFinal.Subtract(dataInicial).Duration();

                if (intervalo.TotalDays > 31)
                {
                    resultado.Detalhes = string.Format("Intervalo de pesquisas para supress�o deve ser inferior a 31 dias (Atual {0} dias)", (int)intervalo.TotalDays);

                    return resultado;
                }

                datasFiltro = true;
            }

            // Nenhuma data informada
            if (!datasFiltro)
            {
                resultado.Detalhes = "Intervalo de pesquisas para data de partida, data de chegada ou cadastro devem ser configurados";

                return resultado;
            }

            try
            {
                var partidas = _ordemServicoRepository
                .ObterPartidasOnTime(
                    requisicao.UnidadeProducaoOrigem,
                    requisicao.UnidadeProducaoDestino,
                    requisicao.AreaOperacionalOrigem,
                    requisicao.AreaOperacionalDestino,
                    requisicao.Prefixo,
                    requisicao.NumeroOrdemServico,
                    requisicao.DataInicialCadastro,
                    requisicao.DataFinalCadastro,
                    requisicao.DataInicialEdicao,
                    requisicao.DataFinalEdicao,
                    requisicao.DataInicialSupressao,
                    requisicao.DataFinalSupressao,
                    requisicao.DataInicialPartida,
                    requisicao.DataFinalPartida,
                    requisicao.DataInicialChegada,
                    requisicao.DataFinalChegada,
                    requisicao.PrefixosIncluir,
                    requisicao.PrefixosExcluir);

                var ordensDuplicadas = partidas.GroupBy(c => c.Numero).ToList();

                // Elimina duplicadas
                foreach (IGrouping<long, PartidaTremOnTime> ordemDuplicada in ordensDuplicadas)
                {
                    var ordensEncontradas = ordemDuplicada.OrderBy(c => c.DataEdicaoPartidaAnterior).ToList();

                    // Remove duplicadas exceto ultima
                    for (int i = 0; i < ordensEncontradas.Count - 1; i++)
                    {
                        partidas.Remove(ordensEncontradas[i]);
                    }

                    var objEdit = ordensEncontradas[ordensEncontradas.Count - 1];

                    objEdit.MatriculaUltimaAlteracao = objEdit.MatriculaUltimaAlteracao ?? objEdit.MatriculaCadastro;
                    objEdit.DataUltimaAlteracao = ordensEncontradas.Max(a => a.DataEdicaoPartidaAnterior) ?? objEdit.DataCadastro;
                    objEdit.DataPartidaCadastro = ordensEncontradas.Select(a => a.DataPartidaAnterior).FirstOrDefault(a => a.HasValue) ?? objEdit.DataPartidaPrevista.Value;
                }

                resultado.PartidasTrens = partidas.ToList();
                resultado.Indicadores = __recuperarIndicadores(partidas);
                resultado.Performances = __recuperarUpPerformance(partidas);
            }
            catch (Exception ex)
            {
                resultado.Sucesso = false;
                resultado.Detalhes = string.Format("Falha ao consultar dados de OnTime para requisi��o {0}. Detalhes {1}", requisicao, ex.Message);
            }

            return resultado;
        }

        /// <summary>
        /// Obtem a lista de OSs para OnTime
        /// </summary>
        /// <param name="requisicao">Requisi��o da pesquisa</param>
        /// <returns>OSs encontradas para o intervalo</returns>
        public ResultadoOrdemServicoOnTime ObterOrdensServicoOnTime(RequisicaoOrdemServicoOnTime requisicao)
        {
            var resultado = new ResultadoOrdemServicoOnTime();

            if (requisicao == null)
            {
                resultado.Detalhes = string.Format("Dados da pesquisa inv�lidos para consulta");

                return resultado;
            }

            DateTime dataInicial;
            DateTime dataFinal;

            bool datasFiltro = false;

            // Data de filtro de partidas
            if (requisicao.DataInicialPartida.HasValue &&
                requisicao.DataFinalPartida.HasValue)
            {
                dataInicial = requisicao.DataInicialPartida.Value;
                dataFinal = requisicao.DataFinalPartida.Value;

                TimeSpan intervalo = dataFinal.Subtract(dataInicial).Duration();

                if (intervalo.TotalDays > 31)
                {
                    resultado.Detalhes = string.Format("Intervalo de pesquisas para partida deve ser inferior a 31 dias (Atual {0} dias)", (int)intervalo.TotalDays);

                    return resultado;
                }

                datasFiltro = true;
            }

            // Data de filtro de chegadas
            if (requisicao.DataInicialChegada.HasValue &&
                requisicao.DataFinalChegada.HasValue)
            {
                dataInicial = requisicao.DataInicialChegada.Value;
                dataFinal = requisicao.DataFinalChegada.Value;

                TimeSpan intervalo = dataFinal.Subtract(dataInicial).Duration();

                if (intervalo.TotalDays > 31)
                {
                    resultado.Detalhes = string.Format("Intervalo de pesquisas para chegada deve ser inferior a 31 dias (Atual {0} dias)", (int)intervalo.TotalDays);

                    return resultado;
                }

                datasFiltro = true;
            }

            // Data de filtro de chegadas
            if (requisicao.DataInicialCadastro.HasValue &&
                requisicao.DataFinalCadastro.HasValue)
            {
                dataInicial = requisicao.DataInicialCadastro.Value;
                dataFinal = requisicao.DataFinalCadastro.Value;

                TimeSpan intervalo = dataFinal.Subtract(dataInicial).Duration();

                if (intervalo.TotalDays > 31)
                {
                    resultado.Detalhes = string.Format("Intervalo de pesquisas para cadastro deve ser inferior a 31 dias (Atual {0} dias)", (int)intervalo.TotalDays);

                    return resultado;
                }

                datasFiltro = true;
            }

            // Data de filtro de edi��o
            if (requisicao.DataInicialEdicao.HasValue &&
                requisicao.DataFinalEdicao.HasValue)
            {
                dataInicial = requisicao.DataInicialEdicao.Value;
                dataFinal = requisicao.DataFinalEdicao.Value;

                TimeSpan intervalo = dataFinal.Subtract(dataInicial).Duration();

                if (intervalo.TotalDays > 31)
                {
                    resultado.Detalhes = string.Format("Intervalo de pesquisas para edi��o deve ser inferior a 31 dias (Atual {0} dias)", (int)intervalo.TotalDays);

                    return resultado;
                }

                datasFiltro = true;
            }

            // Data de filtro de edi��o
            if (requisicao.DataInicialSupressao.HasValue &&
                requisicao.DataFinalSupressao.HasValue)
            {
                dataInicial = requisicao.DataInicialSupressao.Value;
                dataFinal = requisicao.DataFinalSupressao.Value;

                TimeSpan intervalo = dataFinal.Subtract(dataInicial).Duration();

                if (intervalo.TotalDays > 31)
                {
                    resultado.Detalhes = string.Format("Intervalo de pesquisas para supress�o deve ser inferior a 31 dias (Atual {0} dias)", (int)intervalo.TotalDays);

                    return resultado;
                }

                datasFiltro = true;
            }

            // Nenhuma data informada
            if (!datasFiltro)
            {
                resultado.Detalhes = "Intervalo de pesquisas para data de partida, data de chegada ou cadastro devem ser configurados";

                return resultado;
            }

            try
            {
                IList<OrdemServicoOnTimeDto> ordens = _ordemServicoRepository.ObterOrdensServicoOnTime(requisicao);

                List<IGrouping<decimal, OrdemServicoOnTimeDto>> ordensDuplicadas = ordens.GroupBy(c => c.Numero).ToList();

                // Elimina duplicadas
                foreach (IGrouping<decimal, OrdemServicoOnTimeDto> ordemDuplicada in ordensDuplicadas)
                {
                    List<OrdemServicoOnTimeDto> ordensEncontradas = ordemDuplicada.OrderBy(c => c.DataEdicaoPartidaAnterior).ToList();

                    // Remove duplicadas exceto ultima
                    for (int i = 0; i < ordensEncontradas.Count - 1; i++)
                    {
                        ordens.Remove(ordensEncontradas[i]);
                    }

                    var objEdit = ordensEncontradas[ordensEncontradas.Count - 1];

                    objEdit.MatriculaUltimaAlteracao = objEdit.MatriculaUltimaAlteracao ?? objEdit.MatriculaCadastro;
                    objEdit.DataUltimaAlteracao = ordensEncontradas.Max(a => a.DataEdicaoPartidaAnterior) ?? objEdit.DataCadastro;
                    objEdit.DataPartidaCadastro = ordensEncontradas.Select(a => a.DataPartidaAnterior).FirstOrDefault(a => a.HasValue) ?? objEdit.DataPartidaPrevista.Value;
                }

                resultado.OrdensServico = ordens;
                resultado.Sucesso = true;
            }
            catch (Exception ex)
            {
                resultado.Sucesso = false;
                resultado.Detalhes = string.Format("Falha ao consultar ordens de servi�o para requisi��o {0}. Detalhes {1}", requisicao, ex.Message);
            }

            return resultado;
        }

        #endregion

        #region Privados

        /// <summary>
        /// Atualiza as datas de partida e chegada real 
        /// </summary>
        public void AtualizarMovimentacaoTremReal()
        {
            ResultadoOnTime retorno = _onTimeService.ObterOnTime(string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, DateTime.Now.AddDays(-10), DateTime.Now.AddDays(10), null, null, string.Empty, "N,E,W,S,V,A");
            var partidas = retorno.Partidas.ToList(); // .Where(x => x.DataPartidaReal.HasValue);

            foreach (var partida in partidas)
            {
                MovimentacaoTremReal ordemServico = _movimentacaoTremRealRepository.ObterPorId(int.Parse(partida.NumeroOrdemServico.ToString()));

                if (ordemServico == null)
                {
                    var movimentacaoReal = new MovimentacaoTremReal
                                               {
                                                   Id = int.Parse(partida.NumeroOrdemServico.ToString()),
                                                   DataPartidaOrigemReal = partida.DataPartidaReal,
                                                   DataChegadaDestinoReal = partida.DataChegadaReal,
                                                   SituacaoTremPx = ObterSituacaoTrem(partida.SituacaoTrem)
                                               };

                    _movimentacaoTremRealRepository.Inserir(movimentacaoReal);
                }
                else
                {
                    ordemServico.DataPartidaOrigemReal = partida.DataPartidaReal;
                    ordemServico.DataChegadaDestinoReal = partida.DataChegadaReal;
                    ordemServico.SituacaoTremPx = ObterSituacaoTrem(partida.SituacaoTrem);
                    _movimentacaoTremRealRepository.Atualizar(ordemServico);
                }
            }
        }

        private static DetalheFichaTremDto GetDetalheFichaTrem(FichaTremDto ficha, string codigoLocomotiva)
        {
            foreach (DetalheFichaTremDto det in ficha.Detalhes)
            {
                if (codigoLocomotiva.Equals(det.CodigoLocomotiva))
                {
                    return det;
                }
            }

            DetalheFichaTremDto detalhe = new DetalheFichaTremDto { CodigoLocomotiva = codigoLocomotiva };
            ficha.Detalhes.Add(detalhe);

            return detalhe;
        }

        private SituacaoTremPxEnum ObterSituacaoTrem(SituacaoTremOnTimeEnum situacao)
        {
            switch (situacao)
            {
                case SituacaoTremOnTimeEnum.Cancelado:
                    return SituacaoTremPxEnum.Cancelado;
                case SituacaoTremOnTimeEnum.Circulando:
                    return SituacaoTremPxEnum.Circulando;
                case SituacaoTremOnTimeEnum.Encerrado:
                    return SituacaoTremPxEnum.Encerrado;
                case SituacaoTremOnTimeEnum.Programado:
                    return SituacaoTremPxEnum.Programado;
                case SituacaoTremOnTimeEnum.Suprimido:
                    return SituacaoTremPxEnum.Suprimido;
                default:
                    return SituacaoTremPxEnum.Indefinido;
            }
        }

        private void Atualizar(OrdemServicoDto ordemServico, OrdemServico ordemServicoAtualizar)
        {
            Trem trem = _tremRepository.ObterPorIdOrdemServico(ordemServico.Id);

            // S� podem ser alterados dados de ordens de servi�o que n�o foram vinculadas a um trem.
            if (trem != null)
            {
                throw new TranslogicException("Esta ordem de servi�o n�o pode ser alterado pois j� foi utilizada em um Trem.");
            }

            // Atualiza a instancia para a base
            foreach (PlanejamentoSerieLocomotivaDto planejamento in ordemServico.PlanejamentoSerieLocomotivas)
            {
                try
                {
                    PlanejamentoSerieLocomotiva plan = ordemServicoAtualizar.PlanejamentoSerieLocomotivas.FirstOrDefault(p => p.Local.Codigo == planejamento.Local.Codigo && p.Serie.Codigo == planejamento.Serie.Codigo);
                    if (plan != null)
                    {
                        plan.QtdeAnexadas = planejamento.QtdeAnexadas;
                        plan.QtdeDesanexadas = planejamento.QtdeDesanexadas;
                    }
                    else
                    {
                        plan = planejamento.Map<PlanejamentoSerieLocomotivaDto, PlanejamentoSerieLocomotiva>();
                        plan.Local = (EstacaoMae)_areaOperacionalRepository.ObterPorCodigo(planejamento.Local.Codigo);
                        if (plan.Local != null)
                        {
                            ordemServicoAtualizar.PlanejamentoSerieLocomotivas.Add(plan);
                        }
                    }
                }
                catch
                {
                }
            }

            if (ordemServicoAtualizar.PlanejamentoSerieLocomotivas != null)
            {
                foreach (PlanejamentoSerieLocomotiva planejamento in ordemServicoAtualizar.PlanejamentoSerieLocomotivas)
                {
                    planejamento.OrdemServico = ordemServicoAtualizar;
                    _planejamentoSerieLocomotivaRepository.InserirOuAtualizar(planejamento);
                }
            }

            /*
                        // Atualiza a instancia para a base
                        foreach (PlanejamentoQuantidadeLocomotivaDto planejamento in ordemServico.PlanejamentoQuantidadeLocomotiva)
                        {
                            try
                            {
                                PlanejamentoQuantidadeLocomotiva plan = ordemServicoAtualizar.PlanejamentoQuantidadeLocomotivas.FirstOrDefault(p => p.Serie.Codigo == planejamento.Serie.Codigo);
                                if (plan != null)
                                {
                                    plan.Quantidade = planejamento.Quantidade;
                                }
                                else
                                {
                                    plan = planejamento.Map<PlanejamentoQuantidadeLocomotivaDto, PlanejamentoQuantidadeLocomotiva>();
                                    ordemServicoAtualizar.PlanejamentoQuantidadeLocomotivas.Add(plan);
                                }
                            }
                            catch
                            {
                            }
                        }

                        if (ordemServicoAtualizar.PlanejamentoQuantidadeLocomotivas != null)
                        {
                            foreach (PlanejamentoQuantidadeLocomotiva planejamento in ordemServicoAtualizar.PlanejamentoQuantidadeLocomotivas)
                            {
                                planejamento.OrdemServico = ordemServicoAtualizar;
                                _planejamentoQuantidadeLocomotivaRepository.InserirOuAtualizar(planejamento);
                            }
                        }
            */

            foreach (ParadaOrdemServicoProgramadaDto parada in ordemServico.ParadasProgramadas)
            {
                try
                {
                    ParadaOrdemServicoProgramada par = ordemServicoAtualizar.ParadasProgramadas.FirstOrDefault(p => p.Estacao.Codigo == parada.Estacao.Codigo);
                    if (par != null)
                    {
                        par.Atividades.Clear();
                        foreach (var atividade in parada.Atividades)
                        {
                            par.Atividades.Add(atividade.Map<AtividadesParadaOrdemServicoProgramadaDto, AtividadesParadaOrdemServicoProgramada>());
                        }

                        par.QtdeVagoesAnexadosCarregado = parada.QtdeVagoesAnexadosCarregado;
                        par.QtdeVagoesAnexadosVazio = parada.QtdeVagoesAnexadosVazio;
                        par.QtdeVagoesDesanexadosCarregado = parada.QtdeVagoesDesanexadosCarregado;
                        par.QtdeVagoesDesanexadosVazio = parada.QtdeVagoesDesanexadosVazio;
                    }
                    else
                    {
                        par = parada.Map<ParadaOrdemServicoProgramadaDto, ParadaOrdemServicoProgramada>();
                        par.Estacao = (EstacaoMae)_areaOperacionalRepository.ObterPorCodigo(parada.Estacao.Codigo);
                        if (par.Estacao != null)
                        {
                            ordemServicoAtualizar.ParadasProgramadas.Add(par);
                        }
                    }
                }
                catch
                {
                }
            }

            if (ordemServicoAtualizar.ParadasProgramadas != null)
            {
                foreach (ParadaOrdemServicoProgramada parada in ordemServicoAtualizar.ParadasProgramadas)
                {
                    parada.OrdemServico = ordemServicoAtualizar;
                    IList<AtividadesParadaOrdemServicoProgramada> atividades = parada.Atividades.ToList();
                    parada.Atividades.Clear();

                    if (parada.Id > 0)
                    {
                        _paradaOrdemServicoProgramadaRepository.Atualizar(parada);
                    }
                    else
                    {
                        _paradaOrdemServicoProgramadaRepository.Inserir(parada);
                    }

                    foreach (AtividadesParadaOrdemServicoProgramada atividade in atividades)
                    {
                        if (atividade.Id <= 0)
                        {
                            atividade.DuracaoAtividadeEstacao = _duracaoAtividadeRepository.ObterPorEstacaoAtividade(atividade.DuracaoAtividadeEstacao.Atividade, parada.Estacao);

                            if (atividade.DuracaoAtividadeEstacao != null)
                            {
                                atividade.ParadaOrdemServicoProgramada = parada;
                                _atividadeParadaRepository.Inserir(atividade);
                                parada.Atividades.Add(atividade);
                            }
                        }
                    }
                }
            }
        }

        #endregion

        #region Privados OnTime

        private List<UpPerformanceOnTime> __recuperarUpPerformance(IList<PartidaTremOnTime> items)
        {
            var upsOrigem = items.Where(x => x.DataPartidaReal.HasValue)
                .GroupBy(x => x.UnidadeProducaoOrigem)
                .Select(grp => new
                {
                    UP = grp.Key,
                    Partida = items.Count(u => u.UnidadeProducaoOrigem.Equals(grp.Key) && u.DataPartidaReal.HasValue),
                    PartidaHorario = items.Count(u => u.UnidadeProducaoOrigem.Equals(grp.Key) && u.DataPartidaReal.HasValue && !u.AtrasoPartida.HasValue)
                })
                .OrderBy(x => x.UP);

            var upsDestino = items.Where(x => x.DataChegadaReal.HasValue)
                .GroupBy(x => x.UnidadeProducaoDestino)
                .Select(grp => new
                {
                    UP = grp.Key,
                    Chegada = items.Count(u => u.UnidadeProducaoDestino.Equals(grp.Key) && u.DataChegadaReal.HasValue),
                    ChegadaHorario = items.Count(u => u.UnidadeProducaoDestino.Equals(grp.Key) && u.DataChegadaReal.HasValue && !u.AtrasoChegada.HasValue)
                })
                .OrderBy(x => x.UP);

            var ups =
                upsOrigem.Select(
                    x =>
                    new UpPerformanceOnTime
                    {
                        UP = x.UP,
                        PartidaHorario = x.PartidaHorario,
                        Partida = x.Partida,
                        ChegadaHorario = upsDestino.Any(u => u.UP.Equals(x.UP)) && upsDestino.Single(u => u.UP.Equals(x.UP)).Chegada > 0 ? upsDestino.Single(u => u.UP.Equals(x.UP)).ChegadaHorario : 0,
                        Chegada = upsDestino.Any(u => u.UP.Equals(x.UP)) && upsDestino.Single(u => u.UP.Equals(x.UP)).Chegada > 0 ? upsDestino.Single(u => u.UP.Equals(x.UP)).Chegada : 0,
                    }).ToList();

            ups.AddRange(upsDestino.Where(u => !ups.Select(p => p.UP).Contains(u.UP))
                          .Select(
                              x =>
                              new UpPerformanceOnTime
                              {
                                  UP = x.UP,
                                  PartidaHorario = upsOrigem.Any(u => u.UP.Equals(x.UP)) && upsOrigem.Single(u => u.UP.Equals(x.UP)).Partida > 0 ? upsOrigem.Single(u => u.UP.Equals(x.UP)).PartidaHorario : 0,
                                  Partida = upsOrigem.Any(u => u.UP.Equals(x.UP)) && upsOrigem.Single(u => u.UP.Equals(x.UP)).Partida > 0 ? upsOrigem.Single(u => u.UP.Equals(x.UP)).Partida : 0,
                                  Chegada = x.Chegada,
                                  ChegadaHorario = x.ChegadaHorario
                              })
                          .ToList()
                );

            ups.Add(new UpPerformanceOnTime
            {
                UP = "Geral",
                Chegada = ups.Sum(x => x.Chegada),
                ChegadaHorario = ups.Sum(x => x.ChegadaHorario),
                Partida = ups.Sum(x => x.Partida),
                PartidaHorario = ups.Sum(x => x.PartidaHorario)
            });

            return ups;
        }

        private IndicadoresOnTime __recuperarIndicadores(IList<PartidaTremOnTime> retorno)
        {
            var indicadores = new IndicadoresOnTime
            {
                // OSs que foram cadastradas ap�s a data de partida prevista (OS ap�s part trem)
                OsAposPartida = retorno.Count(x => x.DataCadastro > x.DataPartidaOficial),
                // que foram cadastradas com um intervalo menor que duas horas em rela��o a data de partida(Intervalo menor que 2 horas)
                OsMenor2Horas = retorno.Count(x => x.DataPartidaReal.HasValue && x.DataCadastro <= x.DataPartidaReal.Value.AddHours(-2)),
                // Perda mudan�a programada: Multa de 6 horas para as OSs que foram suprimidas ap�s as 9:30 referente � data de partida prevista
                // ou para as OSs que foram suprimidas ap�s a data de partida prevista.
                PerdaMudanca = retorno.Count(x => x.DataSupressao.HasValue && ((x.DataSupressao.Value.Hour > 9 && x.DataSupressao.Value.Minute > 30) || x.DataSupressao.Value > x.DataPartidaOficial)),
                TotalTrem = retorno.Count(),
                PartidaPrevistaGrade = retorno.Count(x => x.DataPartidaPrevistaGrade.HasValue),
                TremEncerrados = retorno.Count(x => x.Situacao == SituacaoTrem.Encerrado),
                TremParados = retorno.Count(x => x.Situacao == SituacaoTrem.Circulando || x.Situacao == SituacaoTrem.Programado),
                TremPartida = retorno.Count(x => x.DataPartidaReal.HasValue),
                TremSuprimido = retorno.Count(x => x.Situacao == SituacaoTrem.Suprimido)
            };

            var partidasReais = retorno.Where(x => x.DataChegadaReal.HasValue).ToList();
            indicadores.TremChegada = partidasReais.Count;
            indicadores.TremChegadaHorario = retorno.Count(x => !x.AtrasoChegada.HasValue && partidasReais.Any(p => p.Numero == x.Numero));
            indicadores.TremChegadaAtraso = retorno.Count(x => x.AtrasoChegada.HasValue && partidasReais.Any(p => p.Numero == x.Numero));

            // (qtde Suprimidos + qtde cancelados) * 6h
            indicadores.HorasDeMulta = (retorno.Count(x => x.Situacao == SituacaoTrem.Suprimido) + retorno.Count(x => x.Situacao == SituacaoTrem.Cancelado)) * 6;

            indicadores.TremSuprimido12Horas = retorno.Count(
                x => (x.DataPartidaReal == null && x.DataPartidaOficial.HasValue && (DateTime.Now.Subtract(x.DataPartidaOficial.Value).TotalHours > 12)) || ((x.DataPartidaReal - x.DataPartidaOficial).HasValue && (x.DataPartidaReal - x.DataPartidaOficial).Value.TotalHours > 12));

            // OS Irregular: OSs que foram cadastradas ap�s a data de partida prevista (OS ap�s part trem) 
            // ou que foram cadastradas com um intervalo menor que duas horas em rela��o a data de partida(Intervalo menor que 2 horas).
            var ordemServicoIrregular =
                retorno.Where(
                    x =>
                    (x.DataCadastro > x.DataPartidaOficial ||
                     (x.DataPartidaReal.HasValue && x.DataCadastro > x.DataPartidaReal.Value.AddHours(-2)))).ToList();

            indicadores.OsIrregular = ordemServicoIrregular.Count(x => x.DataPartidaReal.HasValue);

            // OS Regular: Demais OSs que tiveram data de partida e n�o est�o nas condi��es de Irregular.
            var ordemServicoRegular = retorno.Where(x => !ordemServicoIrregular.Contains(x) && x.DataPartidaReal.HasValue).ToList();
            indicadores.OsRegular = ordemServicoRegular.Count(x => x.DataPartidaReal.HasValue);

            // Trens que partiram ap�s o hor�rio oficial da OS
            indicadores.TremPartidaAtraso = retorno.Count(x => x.AtrasoPartida.HasValue);
            indicadores.TremPartidaAtrasoOsRegular = retorno.Count(x => x.AtrasoPartida.HasValue && !ordemServicoIrregular.Any(o => o.Numero == x.Numero));
            indicadores.TremPartidaAtrasoOsIrregular = retorno.Count(x => x.AtrasoPartida.HasValue && ordemServicoIrregular.Any(o => o.Numero == x.Numero));

            var mediaAtrasoOsRegulares = ordemServicoRegular.Sum(
                x =>
                (x.DataPartidaReal - x.DataPartidaOficial).HasValue && (x.DataPartidaReal - x.DataPartidaOficial).Value.TotalMinutes > 0
                    ? (x.DataPartidaReal - x.DataPartidaOficial).Value.TotalMinutes
                    : 0);

            var mediaAtrasoOsIrregulares = ordemServicoIrregular.Sum(
               x =>
               (x.DataPartidaReal - x.DataPartidaOficial).HasValue && (x.DataPartidaReal - x.DataPartidaOficial).Value.TotalMinutes > 0
                   ? (x.DataPartidaReal - x.DataPartidaOficial).Value.TotalMinutes
                   : 0);

            // Tempo m�dio atraso: m�dia da diferen�a entre a data prevista e a data de partida real de todos os trens que tiveram partida (OSs regulares e irregulares).
            indicadores.TempoMedioAtraso = new TimeSpan(0, (int)Math.Round((mediaAtrasoOsIrregulares + mediaAtrasoOsRegulares) / indicadores.TremPartida, 0), 0).ToString(@"hh\:mm");

            // Soma do tempo de atraso dos trens
            var atrasoOnTime = TimeSpan.FromMinutes(mediaAtrasoOsIrregulares + mediaAtrasoOsRegulares);
            indicadores.AtrasoOnTime = string.Format("{0:00}:{1}", (int)atrasoOnTime.TotalHours, atrasoOnTime.Minutes);

            // Multa + Atraso Ontime
            var atrasoOntimeTotal = TimeSpan.FromMinutes(mediaAtrasoOsIrregulares + mediaAtrasoOsRegulares + (indicadores.HorasDeMulta * 60));
            indicadores.AtrasoOnTimeTotal = string.Format("{0:00}:{1}", (int)atrasoOntimeTotal.TotalHours, atrasoOntimeTotal.Minutes);

            indicadores.TrensCancelador371 = retorno.Count(x => x.Situacao == SituacaoTrem.Cancelado);
            indicadores.TrensSuprimidos433 = retorno.Count(x => x.Situacao == SituacaoTrem.Suprimido);

            return indicadores;
        }

        #endregion
    }
}
