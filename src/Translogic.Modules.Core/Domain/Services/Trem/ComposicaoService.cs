namespace Translogic.Modules.Core.Domain.Services.Trem
{
	using System.Collections.Generic;
	using System.Linq;
	using Castle.Services.Transaction;
	using Translogic.Core.Infrastructure;
	using Translogic.Modules.Core.Domain.DataAccess.Repositories.Trem;
	using Translogic.Modules.Core.Domain.DataAccess.Repositories.Trem.Veiculo.Locomotiva;
	using Translogic.Modules.Core.Domain.Model.Trem.MovimentacaoTrem;
	using Translogic.Modules.Core.Domain.Model.Trem.OrdemServico;
	using Translogic.Modules.Core.Domain.Model.Trem.OrdemServico.Repositories;
	using Translogic.Modules.Core.Domain.Model.Trem.Repositories;
	using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao;
	using Translogic.Modules.Core.Domain.Model.Via;
	using Translogic.Modules.Core.Domain.Model.Via.Repositories;

	/// <summary>
	/// Implementa��o do servi�o de Composi��o
	/// </summary>
	// [Transactional]
	public class ComposicaoService
	{
		private readonly IOrdemServicoRepository _ordemServicoRepository;
		private readonly IAreaOperacionalRepository _areaOperacionalRepository;
		private readonly IComposicaoRepository _composicaoRepository;
		private readonly IComposicaoVagaoRepository _composicaoVagaoRepository;
		private readonly ITremRepository _tremRepository;

		/// <summary>
		/// Construtor padr�o
		/// </summary>
		/// <param name="ordemServicoRepository">Reposit�rio de <see cref="OrdemServico"/>. <remarks>INJETADO</remarks></param>
		/// <param name="areaOperacionalRepository">Reposit�rio de <see cref="IAreaOperacional"/>. <remarks>INJETADO</remarks></param>
		/// <param name="composicaoRepository">Reposit�rio de <see cref="ComposicaoRepository"/>. <remarks>INJETADO</remarks></param>
		/// <param name="composicaoVagaoRepository">Reposit�rio de <see cref="ComposicaoVagaoRepository"/>. <remarks>INJETADO</remarks></param>
		/// <param name="tremRepository"> Reposit�rio de <see cref="TremRepository"/> injetado</param>
		public ComposicaoService(
				IOrdemServicoRepository ordemServicoRepository,
				IAreaOperacionalRepository areaOperacionalRepository,
				IComposicaoRepository composicaoRepository,
				IComposicaoVagaoRepository composicaoVagaoRepository,
				ITremRepository tremRepository)
		{
			_ordemServicoRepository = ordemServicoRepository;
			_areaOperacionalRepository = areaOperacionalRepository;
			_composicaoRepository = composicaoRepository;
			_composicaoVagaoRepository = composicaoVagaoRepository;
			_tremRepository = tremRepository;
		}

		/// <summary>
		/// Obt�m as composi��o de vag�o
		/// </summary>
		/// <param name="ordemServico"> Ordem de servico - <see cref="OrdemServico"/>. </param>
		/// <param name="areaOperacional"> Area operacional por onde o trem deve ter passado <see cref="IAreaOperacional"/>. </param>
		/// <returns> Lista de composi��o de vag�o </returns>
		public IList<ComposicaoVagao> ObterComposicaoVagaoPorOrdemServicoAreaOperacional(OrdemServico ordemServico, IAreaOperacional areaOperacional)
		{
			Model.Trem.Trem trem = _tremRepository.ObterPorIdOrdemServico(ordemServico.Id.Value);
			MovimentacaoTrem movimentacaoTrem = trem.ListaMovimentacaoTrem.FirstOrDefault(c => c.Estacao.Equals(areaOperacional));
			if (movimentacaoTrem == null)
			{
				throw new TranslogicException(string.Format("N�o foi encontrada a Area Operacional {0} na rota do trem da OS {1}", areaOperacional.Codigo, ordemServico.Numero));
			}

			return movimentacaoTrem.Composicao.ListaVagoes;
		}
	}
}
