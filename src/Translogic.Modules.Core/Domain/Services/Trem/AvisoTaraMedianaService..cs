﻿namespace Translogic.Modules.Core.Domain.Services.Trem
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Net.Mail;
    using System.Net.Mime;
    using System.Text;
    using Model.Codificador;
    using Model.Codificador.Repositories;
    using Model.Trem.Veiculo.Vagao;
    using Model.Trem.Veiculo.Vagao.Repositories;

    /// <summary>
    /// servico de envio de emails com as taras medianas dos vagoes
    /// </summary>
    public class AvisoTaraMedianaService
    {
        private readonly IVagaoRepository _vagaoRepository;
        private readonly IVagaoTaraEdiDescargaRepository _vagaoTaraEdiRepository;
        private readonly IConfiguracaoTranslogicRepository _configuracaoTranslogicRepository;

        public AvisoTaraMedianaService(IVagaoRepository vagaoRepository, IVagaoTaraEdiDescargaRepository vagaoTaraEdiRepository, IConfiguracaoTranslogicRepository configuracaoTranslogicRepository)
        {
            _vagaoRepository = vagaoRepository;
            _vagaoTaraEdiRepository = vagaoTaraEdiRepository;
            _configuracaoTranslogicRepository = configuracaoTranslogicRepository;
        }

        /// <summary>
        /// metodo de envio de emails com as taras medianas dos vagoes
        /// </summary>
        /// <returns>retorna booleano</returns>
        public bool EnviarEmailAviso()
        {
            var listaVagoes = _vagaoTaraEdiRepository.ObterVagoesComPesagens();
            var listaDivergencias = new List<VagaoLogTara>();

            foreach (var idVagao in listaVagoes)
            {
                var vagao = _vagaoRepository.ObterPorId((int?) idVagao);
                var registroPesagens = _vagaoTaraEdiRepository.ObterPorVagaoId((int) idVagao).OrderByDescending(x => x.DataCadastro).Take(6).ToList();
              
                IEnumerable<decimal> listaTaras = registroPesagens.Select(x => x.Tara);

                var m = CalcularMediana(listaTaras);

                var mediana = CorrigirValor((Math.Truncate(100 * m) / 100));

                if (vagao.PesoTara != null)
                {
                    if (vagao.PesoTara != mediana)
                    {
                        listaDivergencias.Add(new VagaoLogTara
                                                  {
                                                      CodVagao = vagao.Codigo,
                                                      TaraAntiga = (decimal) vagao.PesoTara,
                                                      TaraNova = (decimal) mediana
                                                  });
                    }
                }
                else if (vagao.FolhaEspecificacao.PesoTara > 0)
                {
                    if (vagao.FolhaEspecificacao.PesoTara != mediana)
                    {
                        listaDivergencias.Add(new VagaoLogTara
                        {
                            CodVagao = vagao.Codigo,
                            TaraAntiga = (decimal)vagao.FolhaEspecificacao.PesoTara,
                            TaraNova = (decimal)mediana
                        });
                    }
                }
                else
                {
                    listaDivergencias.Add(new VagaoLogTara
                    {
                        CodVagao = vagao.Codigo,
                        TaraAntiga = 0,
                        TaraNova = (decimal)mediana
                    });
                }
            }


            var strLinhas = string.Empty;
            var y = 0;

            foreach (var divergencia in listaDivergencias.OrderByDescending(x => x.TaraNova))
            {
                strLinhas += string.Format("<tr><td></td><td {0}>{3}</td><td {0}>{1}</td><td {0}>{2}</td></tr>", y % 2 == 0 ? "style='border: 1px solid black;padding: 5px;'" : "style='border: 1px solid black;padding: 5px;background-color:lightblue;'", divergencia.TaraAntiga, divergencia.TaraNova, divergencia.CodVagao);
                y++;
            }

            var strTabela = string.Format(@"<table style='border-collapse:collapse;text-align: right;'>
                                <th style='border-right: 1px solid black;padding: 5px;text-align: right;'>                     
                                    <td style='border: 1px solid black;padding: 5px;background-color:lightslategray;color:white;'>Código do Vagão</td>  
                                    <td style='border: 1px solid black;padding: 5px;background-color:lightslategray;color:white;'>Tara Atual</td>
                                    <td style='border: 1px solid black;padding: 5px;background-color:lightslategray;color:white;'>Mediana</td>                    
                                </th>
                                {0}
                              </table>", strLinhas);

            EnviaEmail(strTabela);

            return true;
        }

        public void EnviaEmail(string tabelaVagoes)
        {
            string mask = ";,: ";

            ConfiguracaoTranslogic configuracaoTranslogic = _configuracaoTranslogicRepository.ObterPorId("CTE_EMAIL_SERVER");
            string[] valorArray = configuracaoTranslogic.Valor.Split(mask.ToCharArray());

            string emailServer = valorArray[0];
            string emailPort = valorArray[1];

            string remetente = "noreply@all-logistica.com";

            ConfiguracaoTranslogic configuracaocteEmailSenhaSmtp = _configuracaoTranslogicRepository.ObterPorId("CTE_EMAIL_SMTP_SENHA");
            string senhaSmtp = configuracaocteEmailSenhaSmtp.Valor;

            var client = new SmtpClient(emailServer, Convert.ToInt32(emailPort));

            if (!string.IsNullOrEmpty(senhaSmtp))
            {
                client.EnableSsl = true;
                client.Credentials = new NetworkCredential(remetente, senhaSmtp);
            }

            var mailMessage = new MailMessage();
            mailMessage.BodyEncoding = Encoding.UTF8;
            mailMessage.Subject = "Acompanhamento de Taras Medianas dos Vagões";
            mailMessage.From = new MailAddress(remetente);

            ConfiguracaoTranslogic recebedores = _configuracaoTranslogicRepository.ObterPorId("EMAIL_ENVIO_TARAS_MEDIANAS");
            string[] arrayRecebedores = recebedores.Valor.Split(mask.ToCharArray());

            foreach (var rec in arrayRecebedores)
            {
                mailMessage.To.Add(rec);
            }
          
            var corpo = new StringBuilder();

            corpo.AppendFormat(@"                                
                                <table>
                                    <tr>
                                        <td>
                                            <p>Bom dia</p>
                                        </td>
                                    </tr>
                                    <tr>                                 
                                        <td>
                                            <p>Seguem medianas dos vagões</p></br>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            {0}
                                        </td>
                                    </tr>                                    
                                </table>", tabelaVagoes);

            mailMessage.Body =
                "<style type='text/css'> p{ font-family: Calibri,sans-serif; font-size: 12pt; } table, tr, th, td { font-family: Calibri,sans-serif; font-size: 10pt; } </style>" +
                corpo;

            AlternateView html = AlternateView.CreateAlternateViewFromString(mailMessage.Body, null,
                                                                             MediaTypeNames.Text.Html);
            mailMessage.AlternateViews.Add(html);
            mailMessage.IsBodyHtml = true;

            client.Send(mailMessage);
        }

        private double CalcularMediana(IEnumerable<decimal> source)
        {
            var sortedList = from number in source
                             orderby number
                             select number;

            int count = sortedList.Count();
            int itemIndex = count / 2;
            if (count % 2 == 0) // Even number of items. 
                return (double)((sortedList.ElementAt(itemIndex) +
                                  sortedList.ElementAt(itemIndex - 1)) / 2);
            // Odd number of items. 
            return (double)sortedList.ElementAt(itemIndex);
        }

        private double CorrigirValor(double tara)
        {
            if (tara > 9999)
            {
                tara = tara / 1000;
            }
            else if (tara > 999)
            {
                tara = tara / 100;
            }

            return tara;
        }
    }
}