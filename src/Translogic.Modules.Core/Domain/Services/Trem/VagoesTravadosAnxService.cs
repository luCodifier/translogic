namespace Translogic.Modules.Core.Domain.Services.Trem
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using ALL.Core.Dominio;
    using Translogic.Modules.Core.Domain.Model.Acesso;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao.Repositories;

    /// <summary>
    ///     Implementa��o do servi�o de Libera��o de Anexa��o
    /// </summary>
    // [Transactional]
    public class VagoesTravadosAnxService
    {
        #region Fields

        private readonly IVagoesTravadosAnxRepository _liberaAnexacaoRepository;
        
        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///     Construtor padr�o
        /// </summary>
        /// <param name="liberaAnexacaoRepository"> 
        ///     Reposit�rio de <see cref="VagoesTravadosAnx" />.
        ///     <remarks>INJETADO</remarks>
        /// </param>
        public VagoesTravadosAnxService(IVagoesTravadosAnxRepository liberaAnexacaoRepository)
        {
            this._liberaAnexacaoRepository = liberaAnexacaoRepository;
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     Libera os vag�es manga
        /// </summary>
        /// <param name="usuario">usuario da liberacao</param>
        /// <param name="ids">ids Dos Vagoes acima do peso</param>
        public void Liberar(Usuario usuario, params int[] ids)
        {
            foreach (int id in ids)
            {
                VagoesTravadosAnx entity = this._liberaAnexacaoRepository.ObterPorId(id);
                entity.DataLiberacao = DateTime.Now;
                entity.UsuarioLib = usuario;
                this._liberaAnexacaoRepository.InserirOuAtualizar(entity);
            }
        }
        
        /// <summary>
        ///     Obter Por Filtro
        /// </summary>
        /// <param name="pagination">Detalhes Paginacao</param>
        /// <param name="dataInicial">data Inicial</param>
        /// <param name="dataFinal">data Final</param>
        /// <param name="local">local q esta o vag�o</param>
        /// <param name="codigosDosVagoes">codigos Dos Vagoes</param>
        /// <returns>Resultado Paginado VagaoAcimaPesoVigente</returns>
        public ResultadoPaginado<VagoesTravadosAnx> ObterPorFiltro(
            DetalhesPaginacao pagination,
            DateTime? dataInicial,
            DateTime? dataFinal,
            string local,
            string prefixo,
            string codigosDosVagoes)
        {
            codigosDosVagoes = codigosDosVagoes ?? string.Empty;

            string[] codigosDosVagoesSeparados =
                codigosDosVagoes.ToUpper()
                    .Trim()
                    .Replace(" ", string.Empty)
                    .Replace(",", ";")
                    .Replace(".", ";")
                    .Split(';')
                    .Where(e => !string.IsNullOrWhiteSpace(e))
                    .ToArray();
            
            return this._liberaAnexacaoRepository.ObterPorFiltro(
                pagination,
                dataInicial,
                dataFinal,
                local,
                prefixo,
                codigosDosVagoesSeparados);
        }
        
        /// <summary>
        ///     Obtem os vag�es manga por lista de ids
        /// </summary>
        /// <param name="ids">ids Dos Vagoes acima do peso</param>
        public IList<VagoesTravadosAnx> ObterPorIds(params int[] ids)
        {
            IList<VagoesTravadosAnx> vagoesLimitePeso = new List<VagoesTravadosAnx>();
            foreach (int id in ids)
            {
                VagoesTravadosAnx entity = this._liberaAnexacaoRepository.ObterPorId(id);
                vagoesLimitePeso.Add(entity);
            }

            return vagoesLimitePeso;
        }
        
        #endregion
    }
}