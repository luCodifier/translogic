namespace Translogic.Modules.Core.Domain.Services.Trem
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using ALL.Core.Dominio;

    using Translogic.Modules.Core.Domain.Model.Acesso;
    using Translogic.Modules.Core.Domain.Model.Codificador;
    using Translogic.Modules.Core.Domain.Model.Codificador.Repositories;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Repositories;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao.Repositories;

    /// <summary>
    ///     Implementa��o do servi�o de Vag�es Manga
    /// </summary>
    // [Transactional]
    public class VagaoLimitePesoService
    {
        #region Constants

        private const string ChaveToleranciaPesoMinimoVagaoFaturamento = "TOLERANCIA_PESO_MINIMO_VAGAO_FATURAMENTO";

        private const string ChaveToleranciaPespVagaoFaturamento = "TOLERANCIA_PESO_VAGAO_FATURAMENTO";

        #endregion

        #region Fields

        private readonly IConfiguracaoTranslogicRepository _configuracaoTranslogicRepository;

        private readonly IFrotaVagaoRepository _frotaVagaoRepository;

        private readonly ILimiteExcecaoFrotaRepository _limiteExcecaoFrotaRepository;

        private readonly ILimiteExcecaoVagaoRepository _limiteExcecaoVagaoRepository;

        private readonly ILimiteMinimoBaseRepository _limiteMinimoBaseRepository;

        private readonly IMercadoriaRepository _mercadoriaRepository;

        private readonly IVagaoLimitePesoRepository _vagaoAcimaPesoRepository;

        private readonly IVagaoLimitePesoVigenteRepository _vagaoAcimaPesoVigenteRepository;

        private readonly IVagaoRepository _vagaoRepository;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///     Construtor padr�o
        /// </summary>
        /// <param name="vagaoAcimaPesoRepository">
        ///     Reposit�rio de <see cref="IVagaoLimitePesoRepository" />.
        ///     <remarks>INJETADO</remarks>
        /// </param>
        /// <param name="vagaoAcimaPesoVigenteRepository">
        ///     Reposit�rio de <see cref="IVagaoLimitePesoVigenteRepository" />.
        ///     <remarks>INJETADO</remarks>
        /// </param>
        /// <param name="configuracaoTranslogicRepository">
        ///     Reposit�rio de <see cref="IConfiguracaoTranslogicRepository" />.
        ///     <remarks>INJETADO</remarks>
        /// </param>
        /// <param name="frotaVagaoRepository">
        ///     Reposit�rio de <see cref="IFrotaVagaoRepository" />.
        ///     <remarks>INJETADO</remarks>
        /// </param>
        /// <param name="limiteExcecaoFrotaRepository">
        ///     Reposit�rio de <see cref="ILimiteExcecaoFrotaRepository" />.
        ///     <remarks>INJETADO</remarks>
        /// </param>
        /// <param name="limiteExcecaoVagaoRepository">
        ///     Reposit�rio de <see cref="LimiteExcecaoVagao" />.
        ///     <remarks>INJETADO</remarks>
        /// </param>
        /// <param name="limiteMinimoBaseRepository">
        ///     Reposit�rio de <see cref="LimiteMinimoBase" />.
        ///     <remarks>INJETADO</remarks>
        /// </param>
        /// <param name="mercadoriaRepository">
        ///     Reposit�rio de <see cref="Mercadoria" />.
        ///     <remarks>INJETADO</remarks>
        /// </param>
        /// <param name="vagaoRepository">
        ///     Reposit�rio de <see cref="Vagao" />.
        ///     <remarks>INJETADO</remarks>
        /// </param>
        public VagaoLimitePesoService(
            IVagaoLimitePesoRepository vagaoAcimaPesoRepository,
            IVagaoLimitePesoVigenteRepository vagaoAcimaPesoVigenteRepository,
            IConfiguracaoTranslogicRepository configuracaoTranslogicRepository,
            IFrotaVagaoRepository frotaVagaoRepository,
            ILimiteExcecaoFrotaRepository limiteExcecaoFrotaRepository,
            ILimiteExcecaoVagaoRepository limiteExcecaoVagaoRepository,
            ILimiteMinimoBaseRepository limiteMinimoBaseRepository,
            IMercadoriaRepository mercadoriaRepository,
            IVagaoRepository vagaoRepository)
        {
            this._vagaoAcimaPesoRepository = vagaoAcimaPesoRepository;
            this._vagaoAcimaPesoVigenteRepository = vagaoAcimaPesoVigenteRepository;
            this._configuracaoTranslogicRepository = configuracaoTranslogicRepository;
            this._frotaVagaoRepository = frotaVagaoRepository;
            this._limiteExcecaoFrotaRepository = limiteExcecaoFrotaRepository;
            this._limiteExcecaoVagaoRepository = limiteExcecaoVagaoRepository;
            this._limiteMinimoBaseRepository = limiteMinimoBaseRepository;
            this._mercadoriaRepository = mercadoriaRepository;
            this._vagaoRepository = vagaoRepository;
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     Libera os vag�es manga
        /// </summary>
        /// <param name="usuario">usuario da liberacao</param>
        /// <param name="ids">ids Dos Vagoes acima do peso</param>
        public void Liberar(Usuario usuario, params int[] ids)
        {
            foreach (int id in ids)
            {
                VagaoLimitePeso entity = this._vagaoAcimaPesoRepository.ObterPorId(id);
                entity.DataLiberacao = DateTime.Now;
                entity.Usuario = usuario;
                this._vagaoAcimaPesoRepository.InserirOuAtualizar(entity);
            }
        }

        /// <summary>
        ///     Obtem as frotas vag�es
        /// </summary>
        /// <param name="frota">c�digo da frota a ser pesquisada</param>
        /// <returns>Retorna a lista de frotas</returns>
        public IList<FrotaVagao> ObterFrotas(string frota)
        {
            return this._frotaVagaoRepository.ObterFrotas(frota);
        }

        /// <summary>
        ///     Obt�m um limite dado uma frota e um peso
        /// </summary>
        /// <param name="idFrota">Id da FrotaVagao para se obter o Limite</param>
        /// <returns>Limite m�ximo de peso para a frota</returns>
        public LimiteExcecaoFrota ObterLimiteExcecaoFrota(int idFrota)
        {
            return this._limiteExcecaoFrotaRepository.ObterPorFrotaPesoMaximo(idFrota);
        }

        /// <summary>
        ///     Obt�m o limite m�nimo exce��o para o vag�o e mercadoria de acordo com os filtros
        /// </summary>
        /// <param name="vagao">Vag�o para verificar o limite</param>
        /// <param name="mercadoria">Mercadoria para o vag�o informado</param>
        /// <param name="pesoMinimo">Limite para o vag�o com a mercadoria informada</param>
        /// <returns>Limite para o vag�o de acordo com os filtros</returns>
        public LimiteExcecaoVagao ObterLimiteExcecaoVagao(string vagao, string mercadoria, decimal pesoMinimo)
        {
            return this._limiteExcecaoVagaoRepository.ObterPorVagaoMercadoriaPesoMinimo(
                vagao.Trim().Substring(0, 3),
                mercadoria,
                pesoMinimo);
        }

        /// <summary>
        ///     Obt�m um limite de exce��o pelo limite m�nimo base
        /// </summary>
        /// <param name="limiteMinimoBaseId">Id do limite m�nimo base</param>
        /// <returns>Limite para o vag�o de acordo com o LimiteMinimoBase informado</returns>
        public LimiteExcecaoVagao ObterLimiteExcecaoVagaoPorLimiteMinimoBaseId(int limiteMinimoBaseId)
        {
            return this._limiteExcecaoVagaoRepository.ObterLimiteExcecaoVagaoPorLimiteMinimoBaseId(limiteMinimoBaseId);
        }

        /// <summary>
        ///     Obter m�dia do peso informado para vag�o travado por peso m�nimo
        /// </summary>
        /// <param name="idVagao">Id do vag�o</param>
        public double? ObterMediaPesoInformadoVagaoTravadoPorPesoMinimo(int idVagao)
        {
            return this._vagaoAcimaPesoRepository.ObterMediaPesoInformadoVagaoTravadoPorPesoMinimo(idVagao);
        }

        /// <summary>
        ///     Obter Por Filtro
        /// </summary>
        /// <param name="pagination">Detalhes Paginacao</param>
        /// <param name="dataInicial">data Inicial</param>
        /// <param name="dataFinal">data Final</param>
        /// <param name="linhaDeOrigem">linha De Origem</param>
        /// <param name="linhaDeDestino">linha De Destino</param>
        /// <param name="codigosDosVagoes">codigos Dos Vagoes</param>
        /// <returns>Resultado Paginado VagaoAcimaPesoVigente</returns>
        public ResultadoPaginado<VagaoLimitePesoVigente> ObterPorFiltro(
            DetalhesPaginacao pagination,
            DateTime? dataInicial,
            DateTime? dataFinal,
            string linhaDeOrigem,
            string linhaDeDestino,
            string codigosDosVagoes)
        {
            codigosDosVagoes = codigosDosVagoes ?? string.Empty;

            string[] codigosDosVagoesSeparados =
                codigosDosVagoes.ToUpper()
                    .Trim()
                    .Replace(" ", string.Empty)
                    .Replace(",", ";")
                    .Replace(".", ";")
                    .Split(';')
                    .Where(e => !string.IsNullOrWhiteSpace(e))
                    .ToArray();

            return this._vagaoAcimaPesoVigenteRepository.ObterPorFiltro(
                pagination,
                dataInicial,
                dataFinal,
                linhaDeOrigem,
                linhaDeDestino,
                codigosDosVagoesSeparados);
        }

        /// <summary>
        ///     Obt�m os vag�es de acordo com o filtro
        /// </summary>
        /// <param name="pagination">Detalhes de pagina��o</param>
        /// <param name="codigoVagao">Vag�o informado na tela</param>
        /// <param name="mercadoria">Mercadoria informada na tela</param>
        /// <returns>Lista de vag�es de acordo com os filtros informados</returns>
        public ResultadoPaginado<LimiteExcecaoVagao> ObterPorFiltro(
            DetalhesPaginacao pagination,
            string codigoVagao,
            string mercadoria)
        {
            return this._limiteExcecaoVagaoRepository.ObterPorFiltros(pagination, codigoVagao, mercadoria);
        }

        /// <summary>
        ///     Obt�m os vag�es de acordo com o filtro
        /// </summary>
        /// <param name="pagination">Detalhes de pagina��o</param>
        /// <param name="frota">Frota informada na tela</param>
        /// <returns>Lista de vag�es de acordo com os filtros informados</returns>
        public ResultadoPaginado<LimiteExcecaoFrota> ObterPorFiltroFrota(DetalhesPaginacao pagination, string frota)
        {
            return this._limiteExcecaoFrotaRepository.ObterPorFiltroFrota(pagination, frota);
        }

        /// <summary>
        ///     Obtem os vag�es manga por lista de ids
        /// </summary>
        /// <param name="ids">ids Dos Vagoes acima do peso</param>
        public IList<VagaoLimitePeso> ObterPorIds(params int[] ids)
        {
            IList<VagaoLimitePeso> vagoesLimitePeso = new List<VagaoLimitePeso>();
            foreach (int id in ids)
            {
                VagaoLimitePeso entity = this._vagaoAcimaPesoRepository.ObterPorId(id);
                vagoesLimitePeso.Add(entity);
            }

            return vagoesLimitePeso;
        }

        /// <summary>
        ///     Obtem os vag�es manga por vag�o
        /// </summary>
        /// <param name="id">id Do Vagao</param>
        public IList<VagaoLimitePeso> ObterPorVagao(int id)
        {
            IList<VagaoLimitePeso> vagoesLimitePeso = this._vagaoAcimaPesoRepository.ObterPorVagao(id);

            return vagoesLimitePeso;
        }

        /// <summary>
        ///     Obter Tolerancia
        /// </summary>
        /// <returns>Valor tolerancia</returns>
        public string ObterTolerancia()
        {
            ConfiguracaoTranslogic config =
                this._configuracaoTranslogicRepository.ObterPorId(ChaveToleranciaPespVagaoFaturamento);
            string result = config != null ? config.Valor : "0";
            return result;
        }

        /// <summary>
        ///     Obter Tolerancia
        /// </summary>
        /// <returns>Valor tolerancia</returns>
        public string ObterToleranciaMinima()
        {
            ConfiguracaoTranslogic config =
                this._configuracaoTranslogicRepository.ObterPorId(ChaveToleranciaPesoMinimoVagaoFaturamento);
            string result = config != null ? config.Valor : "0";
            return result;
        }

        /// <summary>
        ///     Obt�m um vag�o por c�digo
        /// </summary>
        /// <param name="codigoVagao">C�digo do vag�o a ser localizado</param>
        /// <returns>Vag�o de acordo com o c�digo informado</returns>
        public Vagao ObterVagaoPorCodigoExato(string codigoVagao)
        {
            return this._vagaoRepository.ObterVagaoPorCodigoExato(codigoVagao);
        }

        /// <summary>
        ///     Desabilita um limite maximo para a frota setando a data final
        /// </summary>
        /// <param name="limiteExcecaoFrotaId">Id do limite m�ximo selecionado</param>
        public void RemoverLimiteMaximoFrota(int limiteExcecaoFrotaId)
        {
            this._limiteExcecaoFrotaRepository.RemoverLimiteMaximo(limiteExcecaoFrotaId);
        }

        /// <summary>
        ///     Desabilita um limite minimo para o vag�o setando a data final
        /// </summary>
        /// <param name="limiteMinimoVagaoId">Id do limite minimo vag�o selecionado</param>
        public void RemoverLimiteMinimoVagao(int limiteMinimoVagaoId)
        {
            this._limiteExcecaoVagaoRepository.RemoverLimiteMinimoVagao(limiteMinimoVagaoId);
        }

        /// <summary>
        ///     Obtem as frotas vag�es
        /// </summary>
        /// <param name="frotaVagao">c�digo da frota</param>
        /// <param name="limiteMaximo">limite maximo</param>
        /// <param name="usuario">Usu�rio que realizou a altera��o</param>
        public void SalvarLimiteFrotas(FrotaVagao frotaVagao, decimal limiteMaximo, Usuario usuario)
        {
            var frota = new LimiteExcecaoFrota();
            frota.UsuarioCadastro = usuario;
            frota.PesoMaximo = limiteMaximo;
            frota.Frota = frotaVagao;
            this._limiteExcecaoFrotaRepository.Inserir(frota);
        }

        /// <summary>
        ///     Salva um limite m�ximo para a frota
        /// </summary>
        /// <param name="idFrota">Frota que receber� o limite</param>
        /// <param name="peso">o peso m�ximo que a frota aceita</param>
        /// <param name="usuario">Usu�rio logado que est� realizando a opera��o</param>
        public void SalvarLimiteMaximoFrota(int idFrota, string peso, Usuario usuario)
        {
            FrotaVagao frotaVagao = this._frotaVagaoRepository.ObterPorId(idFrota);

            this._limiteExcecaoFrotaRepository.SalvarLimiteMinimoVagao(frotaVagao, peso, usuario);
        }

        /// <summary>
        ///     Salva um limite m�nimo para o vag�o e mercadoria
        /// </summary>
        /// <param name="limiteMinimoBaseId">limite de peso para esta mercadoria neste vag�o</param>
        /// <param name="vagaoCodigo">vag�o que receber� o limite</param>
        /// <param name="pesoMinimo">o peso m�nimo que o vag�o aceita</param>
        /// <param name="usuario">Usu�rio logado que est� realizando a opera��o</param>
        public void SalvarLimiteMinimoVagao(
            int limiteMinimoBaseId,
            string vagaoCodigo,
            string pesoMinimo,
            Usuario usuario)
        {
            LimiteMinimoBase limite = this._limiteMinimoBaseRepository.ObterPorId(limiteMinimoBaseId);
            Vagao vagao = this._vagaoRepository.ObterVagaoPorCodigoExato(vagaoCodigo);

            this._limiteExcecaoVagaoRepository.SalvarLimiteMinimoVagao(limite, vagao, pesoMinimo, usuario);
        }

        /// <summary>
        ///     Verifica o limite para ver se est� sofrendo altera��o
        /// </summary>
        /// <param name="limite">limite de peso para esta mercadoria neste vag�o</param>
        /// <param name="mercadoria">mercadoria que receber� o limite no vag�o</param>
        /// <param name="codigoVagao">vag�o que receber� o limite</param>
        /// <returns>Limite minimo base se existir</returns>
        public LimiteMinimoBase VerificarLimiteMinimoBase(decimal limite, string mercadoria, string codigoVagao)
        {
            Vagao vagao = this._vagaoRepository.ObterVagaoPorCodigoExato(codigoVagao);

            if (vagao == null)
            {
                throw new Exception("Vag�o n�o foi localizado com o c�digo: " + codigoVagao);
            }

            return this._limiteMinimoBaseRepository.ObterPorLimiteMercadoriaVagao(limite, mercadoria, vagao);
        }

        /// <summary>
        ///     Verifica o limite para ver se est� sofrendo altera��o
        /// </summary>
        /// <param name="limite">TuMedia para esta mercadoria neste vag�o</param>
        /// <param name="mercadoria">mercadoria que receber� o limite no vag�o</param>
        /// <param name="codigoVagao">vag�o que receber� o limite</param>
        /// <returns>Limite minimo base se existir</returns>
        public LimiteMinimoBase VerificarLimiteMinimoBaseTuMedia(decimal limite, string mercadoria, string codigoVagao)
        {
            Vagao vagao = this._vagaoRepository.ObterVagaoPorCodigoExato(codigoVagao);

            if (vagao == null)
            {
                throw new Exception("Vag�o n�o foi localizado com o c�digo: " + codigoVagao);
            }

            return this._limiteMinimoBaseRepository.ObterPorTuMercadoriaVagao(limite, mercadoria, vagao);
        }

        #endregion
    }
}