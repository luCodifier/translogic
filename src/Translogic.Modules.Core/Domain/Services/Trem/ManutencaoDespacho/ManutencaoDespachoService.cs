namespace Translogic.Modules.Core.Domain.Services.ManutencaoDespacho
{
	using System.Collections.Generic;
	using ALL.Core.Dominio;
	using Model.Dto;
	using Model.FluxosComerciais.Pedidos.Despachos.Repositories;
	using NHibernate;

	/// <summary>
	/// Servi�o de Manuten��o de Despacho
	/// </summary>
	public class ManutencaoDespachoService
	{
		#region ATRIBUTOS

		private readonly IDespachoTranslogicRepository _despachoTranslogicRepository;

		#endregion

		#region CONSTRUTORES
		/// <summary>
		/// Construtor injetando objetos
		/// </summary>
		/// <param name="despachoTranslogicRepository">Reposit�rio de Despacho Translogic injetado. </param>
		public ManutencaoDespachoService(IDespachoTranslogicRepository despachoTranslogicRepository)
		{
			_despachoTranslogicRepository = despachoTranslogicRepository;
		}

		#endregion

		#region M�TODOS

		/// <summary>
		/// Obtem Dados para Manuten��o de Despacho
		/// </summary>
		/// <param name="filter">Detalhes do filtro</param>
		/// <returns> Lista Dto Despacho </returns>
		public IList<ManutencaoDespachoDto> ObterDadosDespacho(DetalhesFiltro<object>[] filter)
		{
			string serie = string.Empty;
			string despacho = string.Empty;
			string fluxo = string.Empty;
			string vagoes = string.Empty;

			foreach (DetalhesFiltro<object> filtro in filter)
			{
				if (filtro.Campo.Equals("Serie"))
				{
					serie = filtro.Valor[0].ToString();
				}

				if (filtro.Campo.Equals("Despacho"))
				{
					despacho = filtro.Valor[0].ToString();
				}

				if (filtro.Campo.Equals("Fluxo"))
				{
					fluxo = filtro.Valor[0].ToString();
				}

				if (filtro.Campo.Equals("Vagoes"))
				{
					vagoes = filtro.Valor[0].ToString();
				}
			}

			IList<ManutencaoDespachoDto> listaDto = _despachoTranslogicRepository.ObterDadosDespacho();

			return listaDto;
		}

		#endregion
	}
}