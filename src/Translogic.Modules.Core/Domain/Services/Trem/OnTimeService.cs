﻿namespace Translogic.Modules.Core.Domain.Services.Trem
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Interfaces.Trem.OnTime;
    using Model.Acesso;
    using Model.Diversos;
    using Model.Dto;
    using Model.Trem.MovimentacaoTrem.Repositories;
    using Model.Trem.OrdemServico;
    using Model.Trem.OrdemServico.Repositories;
    using Model.Trem.Repositories;
    using OfficeOpenXml;
    using Translogic.Modules.Core.Controllers.Operacao;
    using Translogic.Modules.Core.Domain.Model.Estrutura;
    using Translogic.Modules.Core.Domain.Model.Estrutura.Repositories;
    using Translogic.Modules.ServicesClients.Domain.Models.Px;
    using Translogic.Modules.ServicesClients.Domain.ServicesClients.Px;

    /// <summary>
    /// Serviço de on time do trem.
    /// </summary>
    public class OnTimeService
    {
        private readonly IUnidadeProducaoRepository _unidadeProducaoRepository;
        private readonly IOnTimeClient _onTimeClient;
        private readonly IOrdemServicoRepository _ordemServicoRepository;
        private readonly IMovimentacaoTremRepository _movimentacaoTremRepository;
        private readonly ITremRepository _tremRepository;

        /// <summary>
        /// Contrutor Injetado
        /// </summary>
        /// <param name="unidadeProducaoRepository">Interface de Unidade de Producao</param>
        /// <param name="onTimeClient">Interface do Cliente de busca do OnTime</param>
        /// <param name="ordemServicoRepository">Repositório de OSs</param>
        /// <param name="movimentacaoTremRepository">Repositório de movimentações</param>
        /// <param name="tremRepository">Repositório de Trens</param>
        public OnTimeService(
                IUnidadeProducaoRepository unidadeProducaoRepository,
                IOnTimeClient onTimeClient,
                IOrdemServicoRepository ordemServicoRepository,
                IMovimentacaoTremRepository movimentacaoTremRepository,
                ITremRepository tremRepository)
        {
            _unidadeProducaoRepository = unidadeProducaoRepository;
            _onTimeClient = onTimeClient;
            _ordemServicoRepository = ordemServicoRepository;
            _movimentacaoTremRepository = movimentacaoTremRepository;
            _tremRepository = tremRepository;
        }

        /// <summary>
        /// Obter as unidades de producao
        /// </summary>
        /// <returns>Retorna as Ups</returns>
        public IList<UnidadeProducao> ObterUnidadesProducao()
        {
            return _unidadeProducaoRepository.ObterTodos();
        }

        /// <summary>
        /// Obtem os Ontime
        /// </summary>
        /// <param name="codigoUpOrigem">Up de Origem</param>
        /// <param name="codigoUpDestino">Up de Destino</param>
        /// <param name="estOrigem">Estação de Origem</param>
        /// <param name="estDestino">Estação de Destino</param>
        /// <param name="prefixo">Prefixo do trem</param>
        /// <param name="partidaPlanejadaInicial">Partida planejada inicial</param>
        /// <param name="partidaPlanejadaFinal">Partida planejada Final</param>
        /// <param name="chegadaPlanejadaInicial">Chegada planejada inicial</param>
        /// <param name="chegadaPlanejadaFinal">Chegada planejada Final</param>
        /// <param name="prefixosIncluir">Prefixos a incluir</param>
        /// <param name="prefixosExcluir">Prefixos a excluir</param>
        /// <returns>Retorna os ontime</returns>
        public ResultadoOnTime ObterOnTime(string codigoUpOrigem, string codigoUpDestino, string estOrigem, string estDestino, string prefixo, DateTime? partidaPlanejadaInicial, DateTime? partidaPlanejadaFinal, DateTime? chegadaPlanejadaInicial, DateTime? chegadaPlanejadaFinal, string prefixosIncluir, string prefixosExcluir)
        {
            RequisicaoOnTime requisicao = new RequisicaoOnTime();

            requisicao.UnidadeProducaoOrigem = codigoUpOrigem;
            requisicao.UnidadeProducaoDestino = codigoUpDestino;
            requisicao.AreaOperacionalOrigem = estOrigem.ToUpperInvariant();
            requisicao.AreaOperacionalDestino = estDestino.ToUpperInvariant();
            requisicao.Prefixo = prefixo.ToUpperInvariant();
            requisicao.PrefixoExcluir = prefixosExcluir.ToUpperInvariant();
            requisicao.PrefixosIncluir = prefixosIncluir.ToUpperInvariant();
            requisicao.DataInicialChegada = chegadaPlanejadaInicial;
            requisicao.DataFinalChegada = chegadaPlanejadaFinal;
            requisicao.DataInicialPartida = partidaPlanejadaInicial;
            requisicao.DataFinalPartida = partidaPlanejadaFinal;
            requisicao.IncluirTrensServico = true;
            requisicao.IncluirSuprimidosCancelados = true;
            requisicao.IncluirTrensCircularam = true;

            return _onTimeClient.ObterOnTime(requisicao);
        }

        /// <summary>
        /// Efetuar alteração de OS de OnTime
        /// </summary>
        /// <param name="objUsu">Usuário que efetuou a operação</param>
        /// <param name="idOS">Id da OS para alterar</param>
        public void EfetuarCancelamentoVirtualOSOnTime(Usuario objUsu, int idOS)
        {
            var objAltOSOnTime = new AlteracaoOSOnTime
                                     {
                                         Data = DateTime.Now,
                                         OrdemServico = _ordemServicoRepository.ObterPorId(idOS),
                                         SituacaoTremPx = SituacaoTremPxEnum.Cancelado,
                                         Usuario = objUsu.Codigo,
                                         Tipo = TipoAlteracaoOnTime.Cancelamento
                                     };

            _ordemServicoRepository.InserirAlteracaoOsOnTime(objAltOSOnTime);
        }

        /// <summary>
        /// Efetuar alteração de OS de OnTime
        /// </summary>
        /// <param name="objUsu">Usuário que efetuou a operação</param>
        /// <param name="idOS">Id da OS para alterar</param>
        public void EfetuarAltPartidaTLOSOnTime(Usuario objUsu, int idOS)
        {
            var objOS = _ordemServicoRepository.ObterPorId(idOS);
            var objTrem = _tremRepository.ObterPorIdOrdemServico(idOS);
            var objAltOSOnTime = new AlteracaoOSOnTime
                                    {
                                        Data = DateTime.Now,
                                        OrdemServico = objOS,
                                        Usuario = objUsu.Codigo,
                                        Tipo = TipoAlteracaoOnTime.PartidaReal,
                                        DataPartidaReal = objTrem.ListaMovimentacaoTrem.Min(a => a.DataSaidaRealizada)
                                    };

            _ordemServicoRepository.InserirAlteracaoOsOnTime(objAltOSOnTime);
        }
    }
}