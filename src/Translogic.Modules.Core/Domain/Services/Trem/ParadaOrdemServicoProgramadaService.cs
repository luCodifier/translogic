﻿using System;
using System.Collections.Generic;
using Castle.Services.Transaction;
using Translogic.Modules.Core.Domain.Model.Diversos;
using Translogic.Modules.Core.Domain.Model.Trem.MovimentacaoTrem;
using Translogic.Modules.Core.Domain.Model.Trem.MovimentacaoTrem.Repositories;
using Translogic.Modules.Core.Domain.Model.Trem.OrdemServico;
using Translogic.Modules.Core.Domain.Model.Trem.OrdemServico.Repositories;
using Translogic.Modules.Core.Domain.Model.Trem.Repositories;

namespace Translogic.Modules.Core.Domain.Services.Trem
{
    [Transactional]
    public class ParadaOrdemServicoProgramadaService
    {
        private readonly IMovimentacaoTremRepository _movimentacaoTremRepository;
        private readonly IParadaOrdemServicoProgramadaRepository _paradaOrdemServicoProgramadaRepository;
        private readonly ITremRepository _tremRepository;

        public ParadaOrdemServicoProgramadaService(
            IParadaOrdemServicoProgramadaRepository paradaOrdemServicoProgramadaRepository,
            ITremRepository tremRepository, IMovimentacaoTremRepository movimentacaoTremRepository)
        {
            _paradaOrdemServicoProgramadaRepository = paradaOrdemServicoProgramadaRepository;
            _tremRepository = tremRepository;
            _movimentacaoTremRepository = movimentacaoTremRepository;
        }

        public IList<ParadaOrdemServicoProgramada> ObterParadas(int? os,
            string prefixo, string origem, string destino, DateTime dataInicial,
            DateTime dataFinal, CadCorredor corredor)
        {
            return _paradaOrdemServicoProgramadaRepository.ObterParadas(os, prefixo, origem, destino,
                dataInicial, dataFinal, corredor);
        }

        public bool BloqueiaEdicaoPrevisaoChegada(ParadaOrdemServicoProgramada parada)
        {
            Model.Trem.Trem trem = _tremRepository.ObterPorIdOrdemServico((int) parada.OrdemServico.Id);
            if (trem == null)
            {
                return false;
            }
            MovimentacaoTrem movimentacaoTrem = _movimentacaoTremRepository.ObterPorTremAreaOperacional(trem,
                parada.Estacao);

            if (movimentacaoTrem == null)
            {
                return false;
            }
            if (movimentacaoTrem.DataChegadaRealizada == null)
            {
                return true;
            }
            return false;
        }
    }
}