﻿namespace Translogic.Modules.Core.Domain.Services.LaudoMercadoria
{
    using System.Collections.Generic;
    using System.Linq;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Modules.Core.Domain.Model.LaudoMercadoria;
    using Translogic.Modules.Core.Domain.Model.LaudoMercadoria.Repositories;

    public class LaudoMercadoriaService
    {
        private readonly ILaudoMercadoriaEmpresaLogoRepository _logoRepository;

        public LaudoMercadoriaService(ILaudoMercadoriaEmpresaLogoRepository logoRepository)
        {
            _logoRepository = logoRepository;
        }

        public IList<LaudoMercadoriaImpressao> ObterLaudoMercadoriaImpressao(IList<LaudoMercadoriaDto> registros)
        {
            var resultados = new List<LaudoMercadoriaImpressao>();

            var laudosIds = registros.Select(r => r.Id).Distinct().ToList();
            foreach (var laudoId in laudosIds)
            {
                var informacoesLaudo = registros.Where(r => r.Id == laudoId).ToList();
                if (informacoesLaudo.Count > 0)
                {
                    var resultado = new LaudoMercadoriaImpressao();
                    var primeiroRegistro = informacoesLaudo.FirstOrDefault();
                    if (primeiroRegistro != null && !resultados.Exists(e => e.CodigoVagao == primeiroRegistro.CodigoVagao))
                    {
                        resultado.CodigoVagao = primeiroRegistro.CodigoVagao;
                        resultado.DataClassificacao = primeiroRegistro.DataClassificacao;
                        resultado.NumeroLaudo = primeiroRegistro.NumeroLaudo;
                        resultado.NumeroOsLaudo = primeiroRegistro.NumeroOsLaudo;
                        resultado.PesoLiquido = primeiroRegistro.PesoLiquido;
                        resultado.Destino = primeiroRegistro.Destino;
                        resultado.AutorizadoPor = primeiroRegistro.AutorizadoPor;
                        resultado.TipoLaudo = primeiroRegistro.TipoLaudo;
                        resultado.EmpresaNome = primeiroRegistro.EmpresaNome;
                        resultado.Produto = primeiroRegistro.Produto;
                        resultado.NumeroNfe = primeiroRegistro.NumeroNfe;
                        resultado.ChaveNfe = primeiroRegistro.ChaveNfe;
                        resultado.DescricaoProduto = primeiroRegistro.DescricaoProduto;

                        var empresaGeradora = new LaudoMercadoriaEmpresa();
                        empresaGeradora.Id = primeiroRegistro.EmpresaGeradoraId;
                        empresaGeradora.Nome = primeiroRegistro.EmpresaGeradoraNome;
                        empresaGeradora.Cnpj = primeiroRegistro.EmpresaGeradoraCnpj;
                        empresaGeradora.Endereco = primeiroRegistro.EmpresaGeradoraEndereco;
                        empresaGeradora.Cep = primeiroRegistro.EmpresaGeradoraCep;
                        empresaGeradora.CidadeDescricao = primeiroRegistro.EmpresaGeradoraCidadeDescricao;
                        empresaGeradora.EstadoSigla = primeiroRegistro.EmpresaGeradoraEstadoSigla;
                        empresaGeradora.InscricaoEstadual = primeiroRegistro.EmpresaGeradoraIe;
                        empresaGeradora.Telefone = primeiroRegistro.EmpresaGeradoraTelefone;
                        empresaGeradora.Fax = primeiroRegistro.EmpresaGeradoraFax;
                        var empLogo = _logoRepository.ObterPorId(int.Parse(empresaGeradora.Id.ToString()));
                        if (empLogo != null)
                        {
                            empresaGeradora.Logo = empLogo.Logo;
                        }
                        resultado.EmpresaGeradora = empresaGeradora;

                        var cliente = new LaudoMercadoriaCliente();
                        cliente.Nome = primeiroRegistro.ClienteNome;
                        cliente.Cnpj = primeiroRegistro.ClienteCnpj;
                        resultado.Cliente = cliente;

                        var classificador = new LaudoMercadoriaClassificador();
                        classificador.Nome = primeiroRegistro.ClassificadorNome;
                        classificador.Cpf = primeiroRegistro.ClassificadorCpf;
                        resultado.Classificador = classificador;

                        var classificacoes = new List<LaudoMercadoriaClassificacao>();
                        foreach (var infoClassificacao in informacoesLaudo)
                        {
                            var classificacao = new LaudoMercadoriaClassificacao();
                            classificacao.Descricao = infoClassificacao.Classificacao;
                            classificacao.Porcentagem = infoClassificacao.Porcentagem;
                            classificacao.Ordem = infoClassificacao.ClassificacaoOrdem ?? 99;
                            classificacoes.Add(classificacao);
                        }

                        resultado.Classificacoes = classificacoes;

                        resultados.Add(resultado);
                    }
                }
            }

            return resultados;
        }
    }
}