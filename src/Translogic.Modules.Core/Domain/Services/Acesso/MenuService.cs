namespace Translogic.Modules.Core.Domain.Services.Acesso
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Net.Mail;
    using System.Text;
    using System.Web;
    using System.Web.Mvc;
    using App_GlobalResources;
    using Model.Acesso;
    using Model.Acesso.DTO;
    using Model.Acesso.Repositories;
    using Model.IntegracaoSIV;
    using Model.IntegracaoSIV.Repositories;
    using MvcContrib.Services;
    using Translogic.Core.Infrastructure;
    using Translogic.Modules.Core.Domain.Model.Codificador;
    using Translogic.Modules.Core.Domain.Model.Codificador.Repositories;

    /// <summary>
    /// Servi�os referentes ao menu
    /// </summary>
    public class MenuService
    {
        #region ATRIBUTOS

        private readonly IMenuRepository _menuRepository;
        private readonly IMeuTranslogicRepository _meuTranslogicRepository;
        private readonly IMenuSivRepository _menuSivRepository;
        private readonly ISubMenuSivRepository _subMenuSivRepository;
        private readonly IItemMenuSivRepository _itemMenuSivRepository;
        private readonly IConfiguracaoTranslogicRepository _configuracaoTranslogicRepository;
        private readonly string _chaveTelasRestricao = "RESTRICAO_TELAS";
        private readonly string _chaveHostRestricao = "RESTRICAO_HOST";
        private readonly string _chaveHostSiv = "SIV_HOST";

        #endregion

        #region CONSTRUTORES
        /// <summary>
        /// Construtor injetando os reposit�rios
        /// </summary>
        /// <param name="menuRepository">Reposit�rio do menu</param>
        /// <param name="meuTranslogicRepository">Reposit�rio do meu translogic</param>
        /// <param name="menuSivRepository">Reposit�rio do SIV</param>
        /// <param name="subMenuSivRepository">Reposit�rio dos Submenus SIV</param>
        /// <param name="itemMenuSivRepository">Reposit�rio dos Items SIV</param>
        public MenuService(IMenuRepository menuRepository, IMeuTranslogicRepository meuTranslogicRepository, IMenuSivRepository menuSivRepository, ISubMenuSivRepository subMenuSivRepository, IItemMenuSivRepository itemMenuSivRepository, IConfiguracaoTranslogicRepository configuracaoTranslogicRepository)
        {
            _menuRepository = menuRepository;
            _meuTranslogicRepository = meuTranslogicRepository;
            _menuSivRepository = menuSivRepository;
            _subMenuSivRepository = subMenuSivRepository;
            _itemMenuSivRepository = itemMenuSivRepository;
            _configuracaoTranslogicRepository = configuracaoTranslogicRepository;
        }

        #endregion

        #region M�TODOS

        /// <summary>
        /// Obt�m lista de menus ativos
        /// </summary>
        /// <returns>Lista de menus</returns>
        public IList<Menu> ObterItensAtivos()
        {
            return ObterItensAtivos(null);
        }

        /// <summary>
        /// Obt�m lista de menus ativos
        /// </summary>
        /// <param name="codigoUsuario">Codigo da usuario</param>
        /// <returns>Lista de menus</returns>
        public IList<Menu> ObterItensAtivos(string codigoUsuario)
        {
            bool useCache = true;

            if (!useCache)
            {
                IList<Menu> itens = string.IsNullOrEmpty(codigoUsuario) ? _menuRepository.ObterItensAtivos() : _menuRepository.ObterItensAtivos(codigoUsuario);
                PrepararHierarquia(itens);
                return itens;
            }
            else
            {
                string key = "ObterItensAtivos_" + codigoUsuario;

                IList<Menu> itens = HttpContext.Current.Session[key] as IList<Menu>;
                if (itens == null)
                {
                    itens = string.IsNullOrEmpty(codigoUsuario) ? _menuRepository.ObterItensAtivos() : _menuRepository.ObterItensAtivos(codigoUsuario);
                    HttpContext.Current.Session[key] = itens;
                    PrepararHierarquia(itens);
                }
                return itens;
            }
        }

        /// <summary>
        /// Obt�m lista de itens do menu principal
        /// </summary>
        /// <param name="codigoUsuario"> C�digo do Usuario </param>
        /// <returns>Lista de Menus</returns>
        public IList<Menu> ObterItensMenuPrincipal(string codigoUsuario)
        {
            IList<Menu> ativos = ObterItensAtivos(codigoUsuario);

            return ativos.Where(m => m.Pai == null).ToList();
        }

        /// <summary>
        /// Lista de Itens do sub-menu dado o id do Pai
        /// </summary>
        /// <param name="pai">Id do menu pai</param>
        /// <param name="codigoUsuario">C�digo do usuairo</param>
        /// <returns>Lista de menus</returns>
        public IList<Menu> ObterItensSubMenu(int pai, string codigoUsuario)
        {
            return ObterItensAtivos(codigoUsuario).Where(m => m.Pai != null && m.Pai.Id == pai).ToList();
        }

        /// <summary>
        /// Obt�m Item de menu Siv pelo c�digo
        /// </summary>
        /// <param name="codigoTela">C�digo da Tela</param>
        /// <param name="codigoUsuario">C�digo do Usuario</param>
        /// <returns>Item do Menu do SIV</returns>
        public ItemMenuSiv ObterItemSivPorCodigoTela(int codigoTela, string codigoUsuario)
        {
            return _itemMenuSivRepository.ObterTodosAutorizados(codigoUsuario).FirstOrDefault(t => t.Id.Equals(codigoTela));
        }

        /// <summary>
        /// Obt�m tela pelo c�digo
        /// </summary>
        /// <param name="codigoTela">C�digo da Tela</param>
        /// <param name="codigoUsuario">C�digo do Usu�rio</param>
        /// <returns>Menu do Translogic</returns>
        public Menu ObterPorCodigoTela(int codigoTela, string codigoUsuario)
        {
            return ObterItensAtivos(codigoUsuario).FirstOrDefault(t => t.CodigoTela.Equals(codigoTela));
        }

        /// <summary>
        /// Pesquisa por menus que contenham no t�tulo do menu o valor
        /// </summary>
        /// <param name="query">Valor a ser pesquisado</param>
        /// <param name="codigoUsuario">C�digo do usu�rio </param>
        /// <returns>Lista de menus</returns>
        public IList<Menu> PesquisarTela(string query, string codigoUsuario)
        {
            int codigoTela;

            if (int.TryParse(query, out codigoTela))
            {
                return ObterItensAtivos(codigoUsuario)
                        .Where(
                            menu => menu.CodigoTela.HasValue &&
                                    menu.CodigoTela.Value.ToString(CultureInfo.InvariantCulture)
                                        .StartsWith(codigoTela.ToString(CultureInfo.InvariantCulture))
                        ).ToList();
            }

            var menus = ObterItensAtivos(codigoUsuario).Where(menu => menu.CodigoTela.HasValue && menu.Titulo.ToUpperInvariant().Contains(query.ToUpperInvariant())).ToList();

            return menus;
        }

        /// <summary>
        /// Pesquisa por menus que contenham no t�tulo do menu o valor
        /// </summary>
        /// <param name="query">Valor a ser pesquisado</param>
        /// <param name="codigoUsuario">C�digo do usu�rio</param>
        /// <returns>Lista de menus</returns>
        public IList<ItemMenuSiv> PesquisarSivTela(string query, string codigoUsuario)
        {
            IList<ItemMenuSiv> menus = _itemMenuSivRepository.ObterTodosAutorizados(codigoUsuario).Where(m => m.Nome.ToUpperInvariant().Contains(query.ToUpperInvariant()))
               .ToList();
            return menus;
        }

        /// <summary>
        /// Arruma a hierarquia dos menus
        /// </summary>
        /// <param name="menus">Lista de menus</param>
        public void PrepararHierarquia(IList<Menu> menus)
        {
            foreach (Menu item in menus)
            {
                item.Filhos = menus.Where(m => m.Pai != null && m.Pai.Id == item.Id).ToList();
            }
        }

        /// <summary>
        /// Transforma um item de menu para MenuSimples
        /// </summary>
        /// <param name="menu">Menu do translogic</param>
        /// <returns>Objeto MenuSimples</returns>
        public MenuSimples ToSimples(Menu menu)
        {
            var simples = new MenuSimples
                              {
                                  Id = menu.Id,
                                  Titulo = menu.Titulo,
                                  CodigoTela = menu.CodigoTela,
                                  Path = menu.Path,
                                  Filhos = new List<MenuSimples>(menu.Filhos.Count)
                              };

            return simples;
        }

        /// <summary>
        /// Transforma um item de Siv menu para MenuSimples
        /// </summary>
        /// <param name="menu">Menu do translogic</param>
        /// <returns>Objeto MenuSimples</returns>
        public MenuSimples SivToSimples(MenuSiv menu)
        {
            var simples = new MenuSimples
                              {
                                  Id = Convert.ToInt32(menu.Id),
                                  Titulo = menu.Nome,
                                  SistemaOrigem = "SIV",
                                  Filhos = SubMenuSivToSimples(menu),
                              };

            return simples;
        }

        /// <summary>
        /// Transforma um item de Siv menu para MenuSimples
        /// </summary>
        /// <param name="menu">Menu do translogic</param>
        /// <returns>Objeto MenuSimples</returns>
        public IList<MenuSimples> SubMenuSivToSimples(MenuSiv menu)
        {
            IEnumerable<SubMenuSiv> lis = _subMenuSivRepository.ObterTodos().Where(m => m.Menu.Id == menu.Id && m.Ativo.Equals("S"));
            IList<MenuSimples> submenus = new List<MenuSimples>(lis.Count());

            foreach (SubMenuSiv subMenuSiv in lis)
            {
                var simples = new MenuSimples
                {
                    Id = Convert.ToInt32(subMenuSiv.Id),
                    Titulo = subMenuSiv.Nome,
                    ////Filhos = ItemMenuSivToSimples(subMenuSiv),
                    SistemaOrigem = "SIV"
                };
                submenus.Add(simples);
            }

            return submenus;
        }

        /// <summary>
        /// Transforma um item de Siv menu para MenuSimples
        /// </summary>
        /// <param name="submenu">Menu do translogic</param>
        /// <param name="itemMenu">Indica se e um item de menu no siv</param>
        /// <param name="codigoUsuario">C�digo do usuario </param>
        /// <returns>Objeto MenuSimples</returns>
        public IList<MenuSimples> ItemMenuSivToSimples(int? submenu, bool itemMenu, string codigoUsuario)
        {
            SubMenuSiv subSiv = itemMenu ? _itemMenuSivRepository.ObterPorId(submenu).Submenu : _subMenuSivRepository.ObterPorId(submenu);
            IList<MenuSimples> subSivList = new List<MenuSimples>(1);
            var subSivSimples = new MenuSimples
            {
                Id = Convert.ToInt32(subSiv.Id),
                Titulo = subSiv.Nome,
            };

            IEnumerable<ItemMenuSiv> lis = _itemMenuSivRepository.ObterTodosAutorizados(codigoUsuario).Where(m => m.Submenu != null && m.Submenu.Id == subSiv.Id && m.Ativo.Equals("S"));
            IList<MenuSimples> itensSivList = new List<MenuSimples>(lis.Count());
            foreach (ItemMenuSiv itemMenuSiv in lis)
            {
                var simples = new MenuSimples
                {
                    Id = itemMenuSiv.Id,
                    Titulo = itemMenuSiv.Nome,
                    CodigoTela = itemMenuSiv.Id,
                    Path = itemMenuSiv.Url,
                    Filhos = new List<MenuSimples>(0),
                    SistemaOrigem = "SIV",
                };
                itensSivList.Add(simples);
            }

            subSivSimples.Filhos = itensSivList;

            subSivList.Add(subSivSimples);
            return subSivList;
        }

        /// <summary>
        /// Transforma um item do MeuTranslogic para o tipo simples
        /// </summary>
        /// <param name="meuTranslogic">Item do meuTranslogic</param>
        /// <returns>Item MeuTranslogicSimples</returns>
        public MeuTranslogicSimples ToSimples(MeuTranslogic meuTranslogic)
        {
            if (meuTranslogic.ItemMenuSiv != null)
            {
                return new MeuTranslogicSimples
                {
                    Id = meuTranslogic.Id,
                    Titulo = "SIV - " + _itemMenuSivRepository.ObterPorId(meuTranslogic.ItemMenuSiv.Id).Nome,
                    CodigoTela = meuTranslogic.ItemMenuSiv.Id,
                    EhPasta = false,
                    Ordem = meuTranslogic.Ordem,
                    SistemaOrigem = "SIV"
                };
            }

            if (meuTranslogic.EhPasta)
            {
                var simples = new MeuTranslogicSimples
                                {
                                    Id = meuTranslogic.Id,
                                    Titulo = meuTranslogic.Titulo,
                                    EhPasta = true,
                                    Ordem = meuTranslogic.Ordem,
                                    Filhos = new List<MeuTranslogicSimples>(meuTranslogic.Filhos.Count),
                                };

                return simples;
            }

            return new MeuTranslogicSimples
            {
                Id = meuTranslogic.Id,
                Titulo = meuTranslogic.Menu.Titulo,
                CodigoTela = meuTranslogic.Menu.CodigoTela,
                EhPasta = false,
                Ordem = meuTranslogic.Ordem
            };
        }

        /// <summary>
        /// Transforma lista de menus em Lista de menuSimples
        /// </summary>
        /// <param name="menus">Lista de menus</param>
        /// <param name="limite">Limite da hierarquia</param>
        /// <param name="nivel">At� o nivel</param>
        /// <returns>Lista de menusSimples</returns>
        public IList<MenuSimples> Transformar(IList<Menu> menus, int? limite, int? nivel)
        {
            if ((nivel.HasValue && limite.HasValue) && nivel.Value > limite.Value)
            {
                return null;
            }

            var itens = new List<MenuSimples>(menus.Count);

            foreach (Menu item in menus)
            {
                MenuSimples simples = ToSimples(item);
                simples.Filhos = Transformar(item.Filhos, limite, nivel + 1);

                itens.Add(simples);
            }

            return itens;
        }

        /// <summary>
        /// Transforma Lista de menus em menu simples, at� determinado limite na hierarquica do autorelacionamento
        /// </summary>
        /// <param name="menus">Lista de menus</param>
        /// <param name="limite">Limite da hierarquia</param>
        /// <returns>Lista de MenuSimples</returns>
        public IList<MenuSimples> Transformar(IList<Menu> menus, int limite)
        {
            return Transformar(menus, limite, 1);
        }

        /// <summary>
        /// Transforma lista de menus em MenuSimples
        /// </summary>
        /// <param name="menus">Lista de menus</param>
        /// <returns>Lista de MenuSimples</returns>
        public IList<MenuSimples> Transformar(IList<Menu> menus)
        {
            return Transformar(menus, null, null);
        }

        /// <summary>
        /// Transforma lista de menusSIV em MenuSimples
        /// </summary>
        /// <param name="menus">Lista de menus</param>
        /// <returns>Lista de MenuSimples</returns>
        public MenuSimples TransformarSiv(IEnumerable<MenuSiv> menus)
        {
            IList<MenuSimples> lista = new List<MenuSimples>();
            var menuSiv = new MenuSimples { Titulo = "SIV", Id = -1 };

            foreach (var menuSimplese in menus)
            {
                lista.Add(SivToSimples(menuSimplese));
            }

            menuSiv.Filhos = lista;

            return menuSiv;
        }

        #endregion

        /// <summary>
        /// Adiciona um novo menu no Meu Translogic
        /// </summary>
        /// <param name="usuario">Usu�rio dono do MeuTranslogic</param>
        /// <param name="menu">Menu a ser adicionado</param>
        /// <param name="itemSiv">Item do Menu SIV a ser adicionado</param>
        public void AdicionarMenuAoMeuTranslogic(Usuario usuario, Menu menu, ItemMenuSiv itemSiv)
        {
            if (usuario == null)
            {
                throw new ArgumentNullException("usuario");
            }

            if (menu == null && itemSiv == null)
            {
                throw new ArgumentNullException("menu");
            }

            if (menu != null)
            {
                if (_meuTranslogicRepository.JaExiste(usuario, menu, null))
                {
                    throw new TranslogicException("O menu j� est� cadastrado no MeuTranslogic");
                }
            }

            if (itemSiv != null)
            {
                if (_meuTranslogicRepository.JaExiste(usuario, itemSiv))
                {
                    throw new TranslogicException("O menu SIV j� est� cadastrado no MeuTranslogic");
                }
            }

            var ultimaOrdem = _meuTranslogicRepository.ObterUltimaOrdem(usuario, null);

            var item = new MeuTranslogic { Usuario = usuario, Menu = menu, Ordem = ultimaOrdem + 1, ItemMenuSiv = itemSiv, };

            _meuTranslogicRepository.Inserir(item);
        }

        /// <summary>
        /// Remove item do meu translogic
        /// </summary>
        /// <param name="id">Id do item no meu Translogic</param>
        public void RemoverMenuMeuTranslogic(int id)
        {
            _meuTranslogicRepository.Remover(new[] { id });
        }

        /// <summary>
        /// Obtem o MeuTranslogic pelo usu�rio
        /// </summary>
        /// <param name="usuario">Usu�rio dono do MeuTranslogic</param>
        /// <returns>Lista de <see cref="MeuTranslogic"/></returns>
        public IList<MeuTranslogicSimples> ObterMeuTranslogic(Usuario usuario)
        {
            var itens = _meuTranslogicRepository.ObterPorUsuario(usuario).Distinct().ToList();

            return itens.Select(item => ToSimples(item)).ToList();
        }

        /// <summary>
        /// Obt�m os itens do menu principal
        /// </summary>
        /// <param name="codigoUsuario"> C�digo do Usu�rio </param>
        /// <returns>Lista de menus transformados em <see cref="MenuSimples"/></returns>
        public IList<MenuSimples> ObterMenuPrincipal(string codigoUsuario)
        {
            var itensMenu = ObterItensMenuPrincipal(codigoUsuario);
            IEnumerable<MenuSiv> listaSiv = _menuSivRepository.ObterTodos().Where(m => m.Ativo.Equals("S"));

            IList<MenuSimples> listaMenu = Transformar(itensMenu, 2);
            MenuSimples sivSimples = TransformarSiv(listaSiv);
            if ((codigoUsuario != "FERROESTE") && (codigoUsuario != "ferroeste"))
            {
                listaMenu.Add(sivSimples);
            }

            return listaMenu;
        }

        /// <summary>
        /// Envia a mensagem para o e-mail do Edinho
        /// </summary>
        /// <remarks>N�o foi feito outro service para isto, pois n�o faz sentido</remarks>
        /// <param name="assunto">Assunto da mensagem</param>
        /// <param name="mensagem">Mensagem a ser enviada</param>
        /// <param name="usuario">Usu�rio que est� enviando a mensagem</param>
        /// <param name="contexto">Contexto do controller</param>
        public void EnviarMensagemEdinho(string assunto, string mensagem, Usuario usuario, ControllerContext contexto)
        {
            if (string.IsNullOrEmpty(assunto))
            {
                throw new TranslogicException(Geral.AssuntoNaoVazio);
            }

            if (string.IsNullOrEmpty(mensagem))
            {
                throw new TranslogicException(Geral.MensagemNaoVazio);
            }

            string senderEmail = string.IsNullOrEmpty(usuario.Email) ? "ccodigitacao@all-logistica.com" : usuario.Email;

            contexto.Controller.ViewData["NomeUsuario"] = usuario.Nome;
            contexto.Controller.ViewData["DataEnvio"] = DateTime.Now;
            contexto.Controller.ViewData["Mensagem"] = mensagem.Replace("\n", "<br />");

            EmailTemplateService ets = new EmailTemplateService();
            EmailMetadata emd = new EmailMetadata(senderEmail, "edsonbl@all-logistica.com");
            emd.Cc.Add(new MailAddress("diegorn@all-logistica.com"));
            MailMessage mailMessage = ets.RenderMessage("EmailFaleComEdinho", emd, contexto);

            // mailMessage.To.Add("edsonbl@all-logistica.com");
            // mailMessage.CC.Add("diegorn@all-logistica.com");
            mailMessage.Subject = string.Concat("[FaleComEdinho] ", assunto);
            mailMessage.From = new MailAddress("noreply@all-logistica.com");
            mailMessage.IsBodyHtml = true;
            mailMessage.SubjectEncoding = Encoding.GetEncoding("ISO-8859-1");
            mailMessage.BodyEncoding = Encoding.GetEncoding("ISO-8859-1");

            SmtpClient client = new SmtpClient();
            client.Send(mailMessage);
        }

        /// <summary>
        /// Verifica se a tela do siv tem acesso
        /// </summary>
        /// <param name="codigoTelaSiv">C�digo da tela do SIV</param>
        /// <param name="codigoUsuario">C�digo do Usu�rio</param>
        /// <returns>Valor booleano</returns>
        public bool VerificarAcessoTelaSiv(int codigoTelaSiv, string codigoUsuario)
        {
            ItemMenuSiv itemMenuSiv = _itemMenuSivRepository.ObterPorCodigoTelaAutorizado(codigoTelaSiv, codigoUsuario);
            return itemMenuSiv != null;
        }

        /// <summary>
        /// Verifica se esta no servidor do Restri��o
        /// </summary>
        /// <param name="host">Host a ser verificado</param>
        /// <returns>Valor booleano</returns>
        public bool VerificarServidorRestricao(string host)
        {
            ConfiguracaoTranslogic configuracaoHostRest = _configuracaoTranslogicRepository.ObterPorId(_chaveHostRestricao);
            return configuracaoHostRest.Valor.ToLower().Equals(host);
        }

        /// <summary>
        /// Verifica se esta no servidor do SIV
        /// </summary>
        /// <param name="host">Host a ser verificado</param>
        /// <returns>Valor booleano</returns>
        public bool VerificarServidorSiv(string host)
        {
            var retorno = false;
            var configuracaoHostRest = _configuracaoTranslogicRepository.ObterPorId(_chaveHostSiv);

            if (configuracaoHostRest != null)
                retorno = configuracaoHostRest.Valor.ToLower().Equals(host);

            return retorno;
        }

        /// <summary>
        /// Verifica se � uma tela do restri��o
        /// </summary>
        /// <param name="codigoTela">tela a ser verificada</param>
        /// <returns>Valor booleano</returns>
        public bool VerificarTelaRestricao(string codigoTela)
        {
            ConfiguracaoTranslogic configuracaoTelasRest = _configuracaoTranslogicRepository.ObterPorId(_chaveTelasRestricao);
            return !configuracaoTelasRest.Valor.Split(';').Contains(codigoTela);
        }
    }
}
