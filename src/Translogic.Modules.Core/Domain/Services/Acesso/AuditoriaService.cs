namespace Translogic.Modules.Core.Domain.Services.Acesso
{
	using System;
	using System.Text;
	using System.Threading;
	using System.Xml;
	using Model.Acesso;
	using NHibernate;

	/// <summary>
	/// Servi�o de auditoria
	/// </summary>
	public class AuditoriaService
	{
		#region ATRIBUTOS

		private readonly ISessionFactory _sessionFactory;

		#endregion

		#region CONSTRUTORES
		/// <summary>
		/// Construtor injetando objetos
		/// </summary>
		/// <param name="sessionFactory">Factory de sessions a ser injetado</param>
		public AuditoriaService(ISessionFactory sessionFactory)
		{
			_sessionFactory = sessionFactory;
		}

		#endregion

		#region M�TODOS

		/// <summary>
		/// Audita a atualiza��o
		/// </summary>
		/// <param name="entity"> Entidade a ser auditada. </param>
		/// <param name="id"> Id da entidade. </param>
		/// <param name="propertyNames"> nomes das propriedades. </param>
		/// <param name="state"> Estado novo. </param>
		/// <param name="oldState"> Estado antivo. </param>
		public void AuditarAtualizacao(object entity, object id, string[] propertyNames, object[] state, object[] oldState)
		{
			Logar(OperacaoAuditoria.Atualizacao, entity, id, propertyNames, state, oldState);
		}

		/// <summary>
		/// Audita a exclus�o de uma entidade
		/// </summary>
		/// <param name="entity"> Entidade a ser auditada. </param>
		/// <param name="id"> Id da entidade. </param>
		/// <param name="propertyNames"> nomes das propriedades. </param>
		/// <param name="state"> Estado novo. </param>
		public void AuditarExclusao(object entity, object id, string[] propertyNames, object[] state)
		{
			Logar(OperacaoAuditoria.Exclusao, entity, id, propertyNames, state, null);
		}

		/// <summary>
		/// Audita a inclusao de uma entidade
		/// </summary>
		/// <param name="entity">Entidade a ser auditada</param>
		/// <param name="id">Id da entidade</param>
		/// <param name="propertyNames">Nomes das propriedades</param>
		/// <param name="state">Estado da entidade</param>
		public void AuditarInsercao(object entity, object id, string[] propertyNames, object[] state)
		{
			Logar(OperacaoAuditoria.Insercao, entity, id, propertyNames, state, null);
		}
		
		private void Logar(OperacaoAuditoria operacao, object entity, object entityId, string[] propertyNames, object[] state, object[] oldState)
		{
			if (entity == null)
			{
				throw new ArgumentNullException("entity");
			}

			if (entityId == null)
			{
				throw new ArgumentNullException("entityId");
			}

			if (!(entity is IAuditoria))
			{
				return;
			}

			var currentIdentity = Thread.CurrentPrincipal.Identity as UsuarioIdentity;

			if (currentIdentity == null)
			{
				return;
			}

			if (!currentIdentity.IsAuthenticated)
			{
				return;
			}

			var log = new LogAuditoria
			          	{
			          		Operacao = operacao,
			          		Quando = DateTime.Now,
			          		TipoEntidade = entity.GetType(),
			          		EntidadeId = entityId.ToString(),
			          		Usuario = currentIdentity.Usuario
			          	};

			if (entity is IAuditoriaDetalhada && operacao != OperacaoAuditoria.Exclusao)
			{
				var strWriter = new StringBuilder();

				using (XmlWriter xmlWriter = XmlWriter.Create(strWriter))
				{
					xmlWriter.WriteStartElement("Alteracoes");

					if (operacao == OperacaoAuditoria.Insercao)
					{
						for (int i = 0; i < propertyNames.Length; i++)
						{
							string propertyName = propertyNames[i];
							object value = state[i] ?? string.Empty;

							xmlWriter.WriteStartElement("Propriedade");
							xmlWriter.WriteAttributeString("Nome", propertyName);
							xmlWriter.WriteAttributeString("Atual", value.ToString());
							xmlWriter.WriteEndElement();
						}
					}
					else
					{
						for (int i = 0; i < propertyNames.Length; i++)
						{
							string propertyName = propertyNames[i];
							object newValue = state[i] ?? string.Empty;
							object oldValue = oldState[i] ?? string.Empty;

							bool propertyChanged = !newValue.ToString().Equals(oldValue.ToString());

							if (propertyChanged)
							{
								xmlWriter.WriteStartElement("Propriedade");
								xmlWriter.WriteAttributeString("Nome", propertyName);
								xmlWriter.WriteAttributeString("Anterior", oldValue.ToString());
								xmlWriter.WriteAttributeString("Atual", newValue.ToString());
								xmlWriter.WriteEndElement();
							}
						}
					}

					xmlWriter.WriteEndElement();
				}

				log.Alteracoes = strWriter.ToString();
			}

			using (IStatelessSession session = _sessionFactory.OpenStatelessSession())
			{
				session.Insert(log);
			}
		}

		#endregion
	}
}