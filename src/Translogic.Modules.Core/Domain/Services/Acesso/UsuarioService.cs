namespace Translogic.Modules.Core.Domain.Services.Acesso
{
	using ALL.Core.Dominio.Services;
	using Model.Acesso;
	using Model.Acesso.Repositories;

	/// <summary>
	/// Serviço de dados do usuario
	/// </summary>
	public class UsuarioService : BaseCrudService<Usuario, int?>
	{
		#region CONSTRUTORES
		/// <summary>
		/// Construtor injetando repositórios
		/// </summary>
		/// <param name="repository">Respositório de usuario</param>
		public UsuarioService(IUsuarioRepository repository) : base(repository)
		{
		}

		#endregion
	}
}