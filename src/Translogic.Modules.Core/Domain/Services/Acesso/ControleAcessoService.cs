using System.Text.RegularExpressions;
using Microsoft.Exchange.WebServices.Data;

namespace Translogic.Modules.Core.Domain.Services.Acesso
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.DirectoryServices.AccountManagement;
    using System.Globalization;
    using System.Linq;
    using System.Net.Mail;
    using System.Text;
    using System.Threading;
    using System.Web;
    using System.Web.Mvc;
    using App_GlobalResources;
    using Castle.Core.Logging;
    using Castle.Services.Transaction;
    using Controllers.ViewModel;
    using Model.Acesso;
    using Model.Acesso.Repositories;
    using Model.Codificador;
    using Model.Codificador.Repositories;
    using Translogic.Core.Infrastructure;

	/// <summary>
	/// Enumeration de resultado de autentica��o
	/// </summary>
	public enum AutenticacaoResultado
	{
		/// <summary>
		/// Caso o usu�rio n�o seja encontrado
		/// </summary>
		UsuarioNaoExiste,

		/// <summary>
		/// Caso o usu�rio esteja inativo
		/// </summary>
		UsuarioInativo,

		/// <summary>
		/// Caso o usu�rio esteja bloqueado por errar a senha
		/// </summary>
		UsuarioBloqueadoPorTentativas,

		/// <summary>
		/// Caso a senha esteja incorreta
		/// </summary>
		SenhaIncorreta,

        /// <summary>
        /// Senha inativada por falta de uso h� mais de 7 dias
        /// </summary>
        UsuarioInativo7Dias,

		/// <summary>
		/// Caso a autentica��o seja feita com sucesso
		/// </summary>
		Sucesso
	}

	/// <summary>
	/// Enumeration de resultado de altera��o de senha
	/// </summary>
	public enum AlterarSenhaResultado
	{
		/// <summary>
		/// Caso a senha atual digitada esteja incorreta
		/// </summary>
		SenhaIncorreta,

		/// <summary>
		/// Caso a senha nova n�o conferir com a confirma��o
		/// </summary>
		SenhaNovaNaoConfereConfirmacao,

		/// <summary>
		/// Caso a senha nova seja igual a senha atual
		/// </summary>
		SenhaNovaIgualAtual,

		/// <summary>
		/// Caso a senha nova seja igual ao c�digo do usuario
		/// </summary>
		SenhaNovaIgualUsuario,

		/// <summary>
		/// Caso a senha n�o contenha letras e n�meros na string
		/// </summary>
		SenhaConterLetrasNumeros,
        /// <summary>
        /// Senha deve ser diferente das ultimas 5 senhas
        /// </summary>
        SenhaHistorica51,

		/// <summary>
		/// Caso a senha seja alterada com sucesso
		/// </summary>
		Sucesso
	}

	/// <summary>
	/// Servi�o de controle de acesso
	/// </summary>
	// [Transactional]
	public class ControleAcessoService : IControleAcessoService
	{
		#region ATRIBUTOS READONLY & EST�TICOS

		private readonly IEventoUsuarioRepository _eventoRepository;
		private readonly ILogUsuarioRepository _logRepository;
		private readonly ITokenAcessoRepository _tokenAcessoRepository;
		private readonly IUsuarioRepository _usuarioRepository;
		private readonly IConfiguracaoTranslogicRepository _configuracaoTranslogicRepository;
		private readonly ISessaoExpiradaRepository _sessaoExpiradaRepository;
		private readonly IAgrupamentoUsuarioRepository _agrupamentoUsuarioRepository;
		private readonly IGrupoUsuarioRepository _grupoUsuarioRepository;
		private readonly IUsuarioConfiguracaoRepository _usuarioConfiguracaoRepository;

		#endregion

		#region ATRIBUTOS

		private ILogger _logger = NullLogger.Instance;

		#endregion

		#region CONSTRUTORES
		/// <summary>
		/// Inicializa uma nova inst�ncia da classe
		/// </summary>
		/// <param name="usuarioRepository"> Resposit�rio de usuario a ser injetado </param>
		/// <param name="logRepository"> Resposit�rio de log a ser injetado </param>
		/// <param name="eventoRepository"> Resposit�rio de evento a ser injetado </param>
		/// <param name="tokenAcessoRepository"> Resposit�rio de token de acesso a ser injetado </param>
		/// <param name="configuracaoTranslogicRepository"> Resposit�rio de configuracao do Translogic a ser injetado. </param>
		/// <param name="sessaoExpiradaRepository">Reposit�rio de sess�o expirada</param>
		/// <param name="agrupamentoUsuarioRepository">Reposit�rio de Agrupamento Usu�rio</param>
		/// <param name="grupoUsuarioRepository">Reposit�rio de Grupo Usu�rio</param>
		/// <param name="usuarioConfiguracaoRepository">Repositorio de configura��o de usuario injetado</param>
		public ControleAcessoService(
									 IUsuarioRepository usuarioRepository,
									 ILogUsuarioRepository logRepository,
									 IEventoUsuarioRepository eventoRepository,
									 ITokenAcessoRepository tokenAcessoRepository,
									 IConfiguracaoTranslogicRepository configuracaoTranslogicRepository,
									 ISessaoExpiradaRepository sessaoExpiradaRepository,
									 IAgrupamentoUsuarioRepository agrupamentoUsuarioRepository,
									 IGrupoUsuarioRepository grupoUsuarioRepository, 
									 IUsuarioConfiguracaoRepository usuarioConfiguracaoRepository)
		{
			_usuarioRepository = usuarioRepository;
			_grupoUsuarioRepository = grupoUsuarioRepository;
			_usuarioConfiguracaoRepository = usuarioConfiguracaoRepository;
			_agrupamentoUsuarioRepository = agrupamentoUsuarioRepository;
			_tokenAcessoRepository = tokenAcessoRepository;
			_configuracaoTranslogicRepository = configuracaoTranslogicRepository;
			_eventoRepository = eventoRepository;
			_logRepository = logRepository;
			_sessaoExpiradaRepository = sessaoExpiradaRepository;
		}

		#endregion

		#region PROPRIEDADES

		/// <summary>
		/// Logger injetado
		/// </summary>
		public ILogger Logger
		{
			get { return _logger; }
			set { _logger = value; }
		}

		#endregion

		#region M�TODOS

		/// <summary>
		/// Faz a autentica��o de acesso ao sistema
		/// </summary>
		/// <param name="codigo">
		/// C�digo do usuario.
		/// </param>
		/// <param name="senha">
		/// Senha do usu�rio
		/// </param>
        /// <param name="dataUltimoLogin">
        /// Data do ultimo login para o SOXS
        /// </param>/// 
		/// <returns>
		/// Redireciona para a tela principal ou para a tela de login com erro
		/// </returns>
		[Transaction]
		public virtual AutenticacaoResultado Autenticar(string codigo, string senha)
		{
			Usuario usuario = _usuarioRepository.ObterPorCodigo(codigo);

		    

			if (usuario == null)
			{
				return AutenticacaoResultado.UsuarioNaoExiste;
			}
		    /*if (VerificarSenhaExpiradaInatividade(usuario))
		    {
                return AutenticacaoResultado.UsuarioInativo7Dias;
		    } */

		    if (usuario.Ativo.HasValue && !usuario.Ativo.Value)
			{
				return AutenticacaoResultado.UsuarioInativo;
			}

			LogUsuario ultimoLog = _logRepository.ObterUltimoLogPorUsuario(usuario);

			if (ultimoLog != null)
			{
				if (ultimoLog.Evento.Codigo == EventoUsuarioCodigo.ContaBloqueada)
				{
					if (ultimoLog.AconteceuEm > DateTime.Now - TimeSpan.FromMinutes(30))
					{
						return AutenticacaoResultado.UsuarioBloqueadoPorTentativas;
					}

					LogarEvento(EventoUsuarioCodigo.ContaDesbloqueada, usuario);
				}
			}

			bool senhaCorreta = usuario.Senha.Equals(senha);

			if (senhaCorreta)
			{
				usuario.UltimoAcesso = DateTime.Now;
				_usuarioRepository.Atualizar(usuario);

				LogarEvento(EventoUsuarioCodigo.LoginEfetuadoComSucesso, usuario);
				Logger.InfoFormat("Usu�rio {0} v�lido", codigo);
			}
			else
			{
				LogarEvento(EventoUsuarioCodigo.UsuarioErrouSenha, usuario);
				Logger.InfoFormat("Senha inv�lida para usu�rio {0}", codigo);
			}

			return senhaCorreta ? AutenticacaoResultado.Sucesso : AutenticacaoResultado.SenhaIncorreta;
		}

		/// <summary>
		/// Verifica se a data da ultima altera��o de senha do usuario est� dentro do prazo vigente
		/// </summary>
		/// <param name="usuario">Usu�rio a verifica se a senha expirou</param>
		/// <returns>Valor booleano</returns>
		public virtual bool VerificarSenhaExpirada(Usuario usuario)
		{
			float quantidadeDiasExpiracao = ObterDiasExpiraSenha();
			return usuario.UltimaAlteracaoSenha.AddDays(quantidadeDiasExpiracao) < DateTime.Now;
		}

        /// <summary>
        /// Verifica se a data da ultima altera��o de senha do usuario est� dentro do prazo vigente
        /// </summary>
        /// <param name="dataRef">Data do ultimo acesso da tabela usuario, anterior ao acesso atual.</param>
        /// <returns>Valor booleano</returns>
        public virtual bool VerificarSenhaExpiradaInatividade(DateTime? dataRef)
        {
            float quantidadeDiasExpiracaoInatividade = ObterDiasExpiraSenhaInatividade();
            
            DateTime dataUltimoAcesso =
                (dataRef ?? DateTime.Now).AddDays(quantidadeDiasExpiracaoInatividade);
            
            return dataUltimoAcesso < DateTime.Now;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataRef"></param>
        /// <returns></returns>
        public virtual int SenhaVaiExpirarEm(Usuario usuario)
        {
            int qtd = _usuarioRepository.ObterDiasAlterar2D(usuario);

            return qtd;
        }

	  

	    /// <summary>
		/// Obt�m quantidade de dias em que a senha deve expirar
		/// </summary>
		/// <returns>Quantidade de dias</returns>
		public virtual float ObterDiasExpiraSenha()
		{
			ConfiguracaoTranslogic configuracaoTranslogic = _configuracaoTranslogicRepository.ObterPorId("SENHA_TLF_EXPIRA_EM_X_DIAS");
			float quantidadeDiasExpiracao;
			float.TryParse(configuracaoTranslogic.Valor, out quantidadeDiasExpiracao);
			return quantidadeDiasExpiracao;
		}
        /// <summary>
        /// Obt�m quantidade de dias em que a senha deve expirar por inatividade
        /// </summary>
        /// <returns>Quantidade de dias</returns>
        public virtual float ObterDiasExpiraSenhaInatividade()
        {
            ConfiguracaoTranslogic configuracaoTranslogic = _configuracaoTranslogicRepository.ObterPorId("SENHA_TLF_EXPIRA_DIAS_INATIVO");
            float quantidadeDiasExpiracaoInatividade;
            float.TryParse(configuracaoTranslogic.Valor, out quantidadeDiasExpiracaoInatividade);
            return quantidadeDiasExpiracaoInatividade;
        }

		/// <summary>
		/// Faz as valida��es de troca de senha e caso todas as regras passem a senha � alterada
		/// </summary>
		/// <param name="usuario"><see cref="Usuario"/> atual</param>
		/// <param name="senhaAtual">Senha atual que o usu�rio digitou na tela</param>
		/// <param name="senhaNova">Senha nova</param>
		/// <param name="senhaNovaConfirmacao">Confirma��o da senha nova</param>
		/// <returns>Resultado do tipo<see cref="AlterarSenhaResultado"/></returns>
		[Transaction]
		public virtual AlterarSenhaResultado AlterarSenha(Usuario usuario, string senhaAtual, string senhaNova, string senhaNovaConfirmacao)
		{
			if (!senhaNova.Equals(senhaNovaConfirmacao))
			{
				return AlterarSenhaResultado.SenhaNovaNaoConfereConfirmacao;
			}

			if (!senhaAtual.Equals(usuario.Senha))
			{
				return AlterarSenhaResultado.SenhaIncorreta;
			}

			if (senhaNova.Equals(usuario.Senha))
			{
				return AlterarSenhaResultado.SenhaNovaIgualAtual;
			}

			if (senhaNova.Equals(usuario.Codigo))
			{
				return AlterarSenhaResultado.SenhaNovaIgualUsuario;
			}

			if (!ValidarComposicaoSenha(senhaNova))
			{
				return AlterarSenhaResultado.SenhaConterLetrasNumeros;
			}
            if (ValidarUltimas5Senhas(usuario, senhaNova))
		    {
		        return AlterarSenhaResultado.SenhaHistorica51;
		    }

			usuario.Senha = senhaNova;
			usuario.UltimaAlteracaoSenha = DateTime.Now;
			_usuarioRepository.Atualizar(usuario);

			LogarEvento(EventoUsuarioCodigo.SenhaAlterada, usuario);
			Logger.InfoFormat("Usu�rio {0} alterou a senha", usuario.Codigo);

			return AlterarSenhaResultado.Sucesso;
		}

		/// <summary>
		/// Valida a composi��o da senha, que deve ter letras e n�meros
		/// </summary>
		/// <param name="senha">
		/// String da senha a ser verificada.
		/// </param>
		/// <returns>
		/// Valor booleano
		/// </returns>
		public bool ValidarComposicaoSenha(string senha)
		{
			bool possuiNumeros = false;
			bool possuiLetras = false;
		    bool possuiCaracterEspecial = false;
			for (int i = 0; i < senha.Length; i++)
			{
				int valInt;
				if (int.TryParse(senha[i].ToString(), out valInt))
				{
					possuiNumeros = true;
				}
				else
				{
					possuiLetras = true;
				}
			}
            Regex regex = new Regex(@"[#$%&()!@`�*+,-./:;<=>?_��������^~��]");
            Match match = regex.Match(senha);
            possuiCaracterEspecial = match.Success;

            Regex regex1 = new Regex(@"[A-Za-z]+");
            Match match1 = regex1.Match(senha);
            possuiLetras = match1.Success;
            
            

			return possuiLetras && possuiNumeros && possuiCaracterEspecial;
		}
        /// <summary>
        /// Valida se a senha do usuario � igual as ultimas 5 senhas
        /// </summary>
        /// <param name="usuario">Objeto usuario</param>
        /// <param name="senhaNova">Nova senha digitada</param>/// 
        /// <returns></returns>
	    public bool ValidarUltimas5Senhas(Usuario usuario,String senhaNova)
        {
            return _usuarioRepository.VerificarUltimas5Senhas(usuario, senhaNova);

        }


		/// <summary>
		/// Insere dados do token
		/// </summary>
		/// <param name="token"> String do token. </param>
		/// <param name="codigoUsuario"> Usu�rio a ser gravado. </param>
		/// <param name="idioma"> Idioma do usuario. </param>
		[Transaction]
		public virtual void RegistraAcessoPorToken(string token, string codigoUsuario, string idioma)
		{
			Usuario usuario = _usuarioRepository.ObterPorCodigo(codigoUsuario);

			TokenAcesso tokenAcesso = _tokenAcessoRepository.ObterPorUsuario(usuario);

			if (tokenAcesso == null)
			{
				tokenAcesso = new TokenAcesso();
			}

			tokenAcesso.Idioma = idioma;
			tokenAcesso.Token = token;
			tokenAcesso.Usuario = usuario;

			_tokenAcessoRepository.InserirOuAtualizar(tokenAcesso);
		}

		/// <summary>
		/// Altera o idioma do token de acesso do usu�rio
		/// </summary>
		/// <param name="usuario">Usu�rio a ser alterado o idioma</param>
		/// <param name="culture">Qual ser� a nova cultura</param>
		public void AlterarIdiomaTokenAcesso(Usuario usuario, CultureInfo culture)
		{
			TokenAcesso tokenAcesso = _tokenAcessoRepository.ObterPorUsuario(usuario);

			tokenAcesso.Idioma = culture.Name;

			_tokenAcessoRepository.Atualizar(tokenAcesso);
		}

		/// <summary>
		/// Envia um email para o usu�rio lembrando a sua senha.
		/// </summary>
		/// <param name="loginInfo">Objeto do tipo <see cref="LoginInfo"/></param>
		/// <param name="contexto">Contexto do Controller para pegar o template de e-mail</param>
		public void LembrarSenha(LoginInfo loginInfo, ControllerContext contexto)
		{
			if (string.IsNullOrEmpty(loginInfo.Usuario))
			{
				throw new TranslogicException(Acesso.UsuarioVazio);
			}

			Usuario usuario = _usuarioRepository.ObterPorCodigo(loginInfo.Usuario);

			if (usuario == null)
			{
				throw new TranslogicException(Acesso.UsuarioNaoExiste);
			}

			if (!usuario.Ativo.Value)
			{
				throw new TranslogicException(Acesso.UsuarioNaoExiste);
			}

			LogarEvento(EventoUsuarioCodigo.Usu�rioEsqueceuSenha, usuario.Codigo);

			if (string.IsNullOrEmpty(usuario.Email))
			{
				throw new TranslogicException(Acesso.UsuarioEmailEmBranco);
			}

			loginInfo.Senha = usuario.Senha;
			loginInfo.NomeUsuario = usuario.Nome;

			ConfiguracaoTranslogic configuracaoTranslogic = _configuracaoTranslogicRepository.ObterPorId("EMAIL_SENHA_USUARIO");
			string emailSender = configuracaoTranslogic.Valor;

			contexto.Controller.ViewData.Model = loginInfo;

			// EmailTemplateService ets = new EmailTemplateService();
			// EmailMetadata emd = new EmailMetadata(emailSender, usuario.Email);
			// emd.IsHtmlEmail = true;
			// MailMessage mailMessage = ets.RenderMessage("EmailLembrarSenha", emd, contexto);
			// mailMessage.To.Add(usuario.Email);
            // mailMessage.From = new MailAddress(emailSender);

            MailMessage mailMessage = new MailMessage(emailSender, usuario.Email);
            mailMessage.IsBodyHtml = true;
            mailMessage.Body = ObterHTMLView(contexto, "EmailLembrarSenha");
            mailMessage.Subject = "Lembrete de senha";
            mailMessage.From = new MailAddress("noreply@all-logistica.com");
			mailMessage.SubjectEncoding = Encoding.GetEncoding("ISO-8859-1");
			mailMessage.BodyEncoding = Encoding.GetEncoding("ISO-8859-1");

			SmtpClient client = new SmtpClient();
			client.Send(mailMessage);
		}

		/// <summary>
		/// Loca o Evento do usu�rio
		/// </summary>
		/// <param name="tipoEvento">
		/// Tipo do evento a ser logado conforme o enum - <see cref="EventoUsuarioCodigo"/>.
		/// </param>
		/// <param name="usuario">
		/// Usuario a ser logado o evento.
		/// </param>
		/// <exception cref="ArgumentNullException">
		/// Exce��o disparada caso o par�metro do usu�rio esteja nulo
		/// </exception>
		[Transaction]
		public virtual void LogarEvento(EventoUsuarioCodigo tipoEvento, Usuario usuario)
		{
			if (usuario == null)
			{
				throw new ArgumentNullException("usuario");
			}

			if (tipoEvento == EventoUsuarioCodigo.UsuarioErrouSenha)
			{
				if (UsuarioExcedeuTentativas(usuario))
				{
					tipoEvento = EventoUsuarioCodigo.ContaBloqueada;
				}
			}

			var log = new LogUsuario();

			var eventoUsuario = _eventoRepository.ObterPorCodigo(tipoEvento);

			log.Usuario = usuario;

			log.Evento = eventoUsuario;

			log.AconteceuEm = DateTime.Now;

            if (HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
            {
                log.IP = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
            }
            else if (!string.IsNullOrEmpty(HttpContext.Current.Request.UserHostAddress))
            {
                log.IP = String.IsNullOrEmpty(log.IP) ? HttpContext.Current.Request.UserHostAddress : "," + HttpContext.Current.Request.UserHostAddress;
            }

            if (String.IsNullOrEmpty(log.UsuarioAD) && System.Security.Principal.WindowsIdentity.GetCurrent() != null)
            {
                var windowsIdentity = System.Security.Principal.WindowsIdentity.GetCurrent();
                if (windowsIdentity != null)
                {
                    var userInfo = new System.Security.Principal.WindowsPrincipal(windowsIdentity);
                    log.UsuarioAD = userInfo.Identity.Name;
                }
            }

            if (String.IsNullOrEmpty(log.UsuarioAD))
            {
                var windowsIdentity = System.Security.Principal.WindowsIdentity.GetCurrent();
                if (windowsIdentity != null)
                {
                    log.UsuarioAD = windowsIdentity.Name;
                }
            }

            if (String.IsNullOrEmpty(log.UsuarioAD))
            {
                log.UsuarioAD = Environment.GetEnvironmentVariable("USERNAME");
            }

            if (String.IsNullOrEmpty(log.UsuarioAD))
            {
                log.UsuarioAD = System.Environment.UserName;
            }
            
            _logRepository.Inserir(log);
		}

		/// <summary>
		/// Loga o evento do usu�rio autenticado
		/// </summary>
		/// <param name="tipoEvento">
		/// Tipo do evento a ser logado conforme o enum - <see cref="EventoUsuarioCodigo"/>.
		/// </param>
		/// <exception cref="Exception">
		/// Exce��o disparada caso o usu�rio esteja nulo
		/// </exception>
		public void LogarEvento(EventoUsuarioCodigo tipoEvento)
		{
			if (!Thread.CurrentPrincipal.Identity.IsAuthenticated)
			{
				throw new Exception("N�o foi poss�vel efetuar log do usu�rio. Nenhum usu�rio se encontra autenticado.");
			}

			LogarEvento(tipoEvento, Thread.CurrentPrincipal.Identity.Name);
		}

		/// <summary>
		/// Loga o evento do usu�rio informado pelo c�digo
		/// </summary>
		/// <param name="tipoEvento">Tipo do evento a ser logado conforme o enum - <see cref="EventoUsuarioCodigo"/>.</param>
		/// <param name="codigoUsuario">C�digo do usu�rio a ser logado</param>
		public void LogarEvento(EventoUsuarioCodigo tipoEvento, string codigoUsuario)
		{
			Usuario usuario = _usuarioRepository.ObterPorCodigo(codigoUsuario);

			if (usuario == null)
			{
				throw new Exception(string.Format("Usu�rio '{0}' n�o encontrado.", codigoUsuario));
			}

			LogarEvento(tipoEvento, usuario);
		}

		/// <summary>
		/// Obt�m o usu�rio pelo c�digo
		/// </summary>
		/// <param name="codigoUsuario">String do c�digo do usu�rio</param>
		/// <returns>Objeto <see cref="Usuario"/></returns>
		public Usuario ObterUsuario(string codigoUsuario)
		{
			return _usuarioRepository.ObterPorCodigo(codigoUsuario);
		}

		/// <summary>
		/// Obt�m os grupos do usuario
		/// </summary>
		/// <param name="usuario">c�digo do usuario</param>
		/// <returns>array de string</returns>
		public string[] ObterGruposPorUsuario(string usuario)
		{
			return new string[0];
		}

		/// <summary>
		/// Valida o usu�rio
		/// </summary>
		/// <param name="usuario">c�digo do usuario</param>
		/// <param name="senha">senha do usuario</param>
		/// <returns>Valor booleano</returns>
		public bool Validar(string usuario, string senha)
		{
		    return Autenticar(usuario, senha) == AutenticacaoResultado.Sucesso;
		}

		/// <summary>
		/// Inserir Log de sess�o expirada
		/// </summary>
		/// <param name="codigoUsuario"> Codigo do Usuario. </param>
		/// <param name="ipClient"> IP do cliente </param>
		/// <param name="ipServer"> IP do Server. </param>
		/// <param name="tempoRestante"> Tempo Restante. </param>
		public void InserirLogSessaoExpirada(string codigoUsuario, string ipClient, string ipServer, int tempoRestante)
		{
			SessaoExpirada sessaoExpirada = new SessaoExpirada
												{
													CodigoUsuario = codigoUsuario,
													DataOcorrencia = DateTime.Now,
													IpCliente = ipClient,
													IpServidor = ipServer,
													TempoRestante = tempoRestante
												};

			_sessaoExpiradaRepository.Inserir(sessaoExpirada);
		}

		/// <summary>
		/// Verifica se o usu�rio pode acessar ou nao a tela
		/// </summary>
		/// <param name="transacao"> Transacao a ser autorizada. </param>
		/// <param name="usuario"> Usu�rio a ser autorizado. </param>
		/// <returns> Valor booleano </returns>
		public bool Autorizar(string transacao, Usuario usuario)
		{
			IList listaPermissoes = _usuarioRepository.ObterPermissoes(transacao, usuario);
			return listaPermissoes.Count > 0;
		}

		/// <summary>
		/// Verifica se o usu�rio pode acessar ou nao a tela
		/// </summary>
		/// <param name="transacao"> Transacao que a a��o pertence.  </param>
		/// <param name="acao"> A��o a ser autorizada. </param>
		/// <param name="usuario"> Usu�rio a ser autorizado.  </param>
		/// <returns> Valor booleano </returns>
		public bool Autorizar(string transacao, string acao, Usuario usuario)
		{
			IList listaPermissoes = _usuarioRepository.ObterPermissoes(transacao, acao, usuario);
			if (listaPermissoes.Count > 0)
			{
				object[] tuplas = (object[])listaPermissoes[0];
				return tuplas[1] != null;
			}

			return false;
		}

        /// <summary>
        /// Verifica se o usu�rio pode acessar ou nao a tela
        /// </summary>
        /// <param name="usuario"> Usu�rio para localizar o ultimo acesso.  </param>
        /// <returns> Valor booleano </returns>
        public bool RecuperarUltimoAcesso(Usuario usuario)
        {
            DateTime? dataUltimoAcesso = _usuarioRepository.ObterDataUltimoAcessoSoxs(usuario);
            bool r;

            r = VerificarSenhaExpiradaInatividade(dataUltimoAcesso);
            

            return r;
        }


		/// <summary>
		/// Valida o usu�rio e verificar acesso
		/// </summary>
		/// <param name="codigo">c�digo do usuario</param>
		/// <param name="senha">senha do usuario</param>
		/// <param name="transacao">transacao da tela</param>
		/// <returns>Valor booleano</returns>
		public bool ValidarAutorizarAcesso(string codigo, string senha, string transacao)
		{
			if (Validar(codigo, senha))
			{
				Usuario usuario = _usuarioRepository.ObterPorCodigo(codigo);

				if (Autorizar(transacao, usuario))
				{
					return true;
				}
			}

			return false;
		}

		/// <summary>
		/// Obt�m o usu�rio de interface SAP
		/// </summary>
		/// <returns>Objeto do tipo <see cref="Usuario"/></returns>
		public Usuario ObterUsuarioInterfaceSap()
		{
			return _usuarioRepository.ObterPorCodigo("INTSAP");
		}

		/// <summary>
		/// Verifica se o Usu�rio pertence ao grupo.
		/// </summary>
		/// <param name="codigoUsuario">Usu�rio do grupo.</param>
		/// <param name="codigoGrupo">C�digo do grupo.</param>
		/// <returns>Retorna verdadeiro se o usu�rio pertencer ao grupo.</returns>
		public bool VerificarUsuarioPertenceGrupo(string codigoUsuario, string codigoGrupo)
		{
			GrupoUsuario grupoUsuario = _grupoUsuarioRepository.ObterPorCodigo(codigoGrupo);
			Usuario usuario = _usuarioRepository.ObterPorCodigo(codigoUsuario);

			if (grupoUsuario == null)
			{
				throw new Exception("C�digo do grupo n�o encontrado.");
			}

			return _agrupamentoUsuarioRepository.VerificarUsuarioGrupo(usuario, grupoUsuario);
		}

		/// <summary>
		/// Grava o tema selecionado pelo usuario
		/// </summary>
		/// <param name="usuarioAtual">Usu�rio a ser gravado o tema</param>
		/// <param name="tema">Tema selecionado</param>
		public void GravarTemaUsuario(Usuario usuarioAtual, string tema)
		{
			IList<UsuarioConfiguracao> configuracoes = _usuarioConfiguracaoRepository.ObterPorUsuario(usuarioAtual);
			UsuarioConfiguracao configuracaoTema = configuracoes.FirstOrDefault(c => c.Chave.Equals(UsuarioConfiguracaoEnum.Tema));
			if (configuracaoTema == null)
			{
				configuracaoTema = new UsuarioConfiguracao
					{
						Chave = UsuarioConfiguracaoEnum.Tema,
						DataCadastro = DateTime.Now,
						Usuario = usuarioAtual
					};
			}

			configuracaoTema.Valor = tema;

			_usuarioConfiguracaoRepository.InserirOuAtualizar(configuracaoTema);
		}

		/// <summary>
		/// Obt�m o tema dado o codigo do usuario
		/// </summary>
		/// <param name="codigo">C�digo do usuario</param>
		/// <returns>Tema do usuario</returns>
		public string ObterTemaUsuario(string codigo)
		{
			Usuario usuario = _usuarioRepository.ObterPorCodigo(codigo);
			IList<UsuarioConfiguracao> configuracoes = _usuarioConfiguracaoRepository.ObterPorUsuario(usuario);
			UsuarioConfiguracao configuracaoTema = configuracoes.FirstOrDefault(c => c.Chave.Equals(UsuarioConfiguracaoEnum.Tema));
			if (configuracaoTema == null)
			{
				return null;
			}

			if (Tema.Default.Codigo == configuracaoTema.Valor)
			{
				return null;
			}

			return configuracaoTema.Valor;
		}

        /// <summary>
        /// Renderiza e recupera o HTML de uma view
        /// </summary>
        /// <param name="contextControllerContext">Contexto do Controller</param>
        /// <param name="viewName">Nome da View</param>
        /// <returns>String com o HTML da view renderizada</returns>
        private static string ObterHTMLView(ControllerContext contextControllerContext, string viewName)
        {
            string result;
            var view = ViewEngines.Engines.FindView(contextControllerContext, viewName, null);

            using (var writer = new System.IO.StringWriter())
            {
                var viewContext = new ViewContext(contextControllerContext, view.View, contextControllerContext.Controller.ViewData, contextControllerContext.Controller.TempData, writer);
                view.View.Render(viewContext, writer);
                writer.Flush();
                result = writer.ToString();
            }

            return result;
        }

		/// <summary>
		/// Verificar se o usu�rio excedeu a quantidade de tentativas de login.
		/// Caso isso seja verdade, ele ficar� bloqueado por 10 minutos.
		/// </summary>
		/// <param name="usuario">O usu�rio a ser verificado</param>
		/// <returns>Se o usu�rio excedeu as tentativas</returns>
		private bool UsuarioExcedeuTentativas(Usuario usuario)
		{
			DateTime agora = DateTime.Now;
			IList<LogUsuario> logs = _logRepository.ObterLogsPorUsuarioNoPeriodo(usuario, agora - TimeSpan.FromMinutes(10), agora);

			int totalTentativas = 0;

			foreach (LogUsuario item in logs)
			{
				if (item.Evento.Codigo == EventoUsuarioCodigo.UsuarioErrouSenha)
				{
					totalTentativas++;
				}
				else if (item.Evento.Codigo == EventoUsuarioCodigo.LoginEfetuadoComSucesso ||
						 item.Evento.Codigo == EventoUsuarioCodigo.ContaDesbloqueada)
				{
					break;
				}
			}

			return totalTentativas >= 4;
		}

		#endregion
	}
}
