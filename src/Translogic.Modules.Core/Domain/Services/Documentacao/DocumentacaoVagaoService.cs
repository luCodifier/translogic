﻿namespace Translogic.Modules.Core.Domain.Services.Documentacao
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.Mail;
    using System.Reflection;
    using System.Security.Cryptography;
    using System.Text;
    using Castle.Services.Transaction;
    using ICSharpCode.SharpZipLib.Zip;
    using iTextSharp.text.exceptions;
    using Remotion.Linq.Utilities;
    using Translogic.Core.Infrastructure.FileSystem.ExtensionMethods;
    using Translogic.Modules.Core.Domain.Model.Codificador.Repositories;
    using Translogic.Modules.Core.Domain.Model.Documentacao;
    using Translogic.Modules.Core.Domain.Model.Documentacao.Repositories;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Dcls;
    using Translogic.Modules.Core.Domain.Model.PainelExpedicao.Documentos.Dto;
    using Translogic.Modules.Core.Domain.Model.PainelExpedicao.Documentos.Request;
    using Translogic.Modules.Core.Domain.Model.Trem.OrdemServico.Repositories;
    using Translogic.Modules.Core.Domain.Services.FluxosComerciais;
    using Translogic.Modules.Core.Domain.Services.GestaoDocumentos;
    using Translogic.Modules.Core.Domain.Services.LaudoMercadoria;
    using Translogic.Modules.Core.Domain.Services.Mdfes;
    using Translogic.Modules.Core.Domain.Services.PainelExpedicao;
    using Translogic.Modules.Core.Domain.Services.PainelExpedicao.Relatorio;
    using Translogic.Modules.Core.Interfaces.GestaoDocumentos.Balanca;
    using Translogic.Modules.Core.Util; // Não retirar o using Translogic.Modules.Core.Util
    using Translogic.Modules.EDI.Infrastructure;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories.Documentacao;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.DocumentosVagao;
    using Translogic.Modules.Core.Domain.Model.Trem.Repositories;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao;
    using Microsoft.Exchange.WebServices.Data;
    using BLL = Translogic.Modules.Core.Spd.BLL;
    using Spd = Translogic.Modules.Core.Spd.Data;
    using Speed.Common;
    using Translogic.Core;
    using Translogic.Modules.Core.Spd;
    using Translogic.Modules.Core.Spd.BLL;
    using Speed.Data;

    /// <summary>
    ///     Serviço da NFe PDF
    /// </summary>
    [Transactional]
    public class DocumentacaoVagaoService
    {
        private readonly PainelExpedicaoService _painelExpedicaoService;
        private readonly RelatorioDocumentosService _relatorioDocumentosService;
        private readonly DclService _dclService;
        private readonly LaudoMercadoriaService _laudoMercadoriaService;
        private readonly MdfeService _mdfeService;

        private readonly IConfiguracaoTranslogicRepository _confGeralRepository;
        private readonly IGerenciamentoDocumentacaoRepository _gerenciamentoDocumentacaoRepository;
        private readonly IGerenciamentoDocumentacaoRefaturamentoRepository _gerenciamentoDocumentacaoRefaturamentoRepository;
        private readonly IConfiguracaoEnvioDocumentacaoRepository _configuracaoEnvioDocumentacaoRepository;
        private readonly IOrdemServicoRepository _ordemServicoRepository;
        private readonly IHistoricoEnvioDocumentacaoRepository _historicoEnvioRepository;
        private readonly IGerenciamentoDocumentacaoCompVagaoRepository _gerenciamentoDocumentacaoCompVagaoRepository;
        private readonly IComposicaoRepository _composicaoRepository;
        private readonly IComposicaoVagaoRepository _composicaoVagaoRepository;

        private readonly string _source = "Translogic.JobRunner";
        private readonly string _logName = "Envio de Documentação";

        public DocumentacaoVagaoService(
            PainelExpedicaoService painelExpedicaoService,
            RelatorioDocumentosService relatorioDocumentosService,
            DclService dclService,
            LaudoMercadoriaService laudoMercadoriaService,
            MdfeService mdfeService,
            IConfiguracaoTranslogicRepository confGeralRepository,
            IGerenciamentoDocumentacaoRepository gerenciamentoDocumentacaoRepository,
            IGerenciamentoDocumentacaoRefaturamentoRepository gerenciamentoDocumentacaoRefaturamentoRepository,
            IConfiguracaoEnvioDocumentacaoRepository configuracaoEnvioDocumentacaoRepository,
            IHistoricoEnvioDocumentacaoRepository historicoEnvioRepository,
            IOrdemServicoRepository ordemServicoRepository,
            IGerenciamentoDocumentacaoCompVagaoRepository gerenciamentoDocumentacaoCompVagaoRepository,
            IComposicaoRepository composicaoRepository,
            IComposicaoVagaoRepository composicaoVagaoRepository)
        {
            _painelExpedicaoService = painelExpedicaoService;
            _relatorioDocumentosService = relatorioDocumentosService;
            _dclService = dclService;
            _laudoMercadoriaService = laudoMercadoriaService;
            _mdfeService = mdfeService;

            _confGeralRepository = confGeralRepository;
            _gerenciamentoDocumentacaoRepository = gerenciamentoDocumentacaoRepository;
            _gerenciamentoDocumentacaoRefaturamentoRepository = gerenciamentoDocumentacaoRefaturamentoRepository;
            _configuracaoEnvioDocumentacaoRepository = configuracaoEnvioDocumentacaoRepository;
            _historicoEnvioRepository = historicoEnvioRepository;
            _ordemServicoRepository = ordemServicoRepository;
            _gerenciamentoDocumentacaoCompVagaoRepository = gerenciamentoDocumentacaoCompVagaoRepository;
            _composicaoRepository = composicaoRepository;
            _composicaoVagaoRepository = composicaoVagaoRepository;
        }

        public void ExecutarGerenciamentoDocumentacao(string[] prefixoTrem = null)
        {
            LogHelper.Log("Inicio GerenciamentoDocumentacaoJob", EventLogEntryType.Information, _source, _logName);

            try
            {
                try
                {
                    // Verificando se não há alguma documentação vencida a ser excluída
                    // TODO: PERFORMANCE2
                    this.DeletarDocumentacaoVencidas(prefixoTrem);
                }
                catch (Exception ex)
                {
                    Sis.LogException(ex, "DeletarDocumentacaoVencidas");
                }

                // Gerando a documentação
                try
                {
                    this.GerarDocumentacao(prefixoTrem);
                }
                catch (Exception ex)
                {
                    Sis.LogException(ex, "GerarDocumentacao");
                }
            }
            catch (Exception ex)
            {
                LogHelper.Log(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error, _source, _logName);
            }

            LogHelper.Log("Fim de GerenciamentoDocumentacaoJob", EventLogEntryType.Information, _source, _logName);
        }

        public void ExecutarEnvioDocumentacao(int[] ids = null)
        {
            LogHelper.Log("Inicio EnvioDocumentaçãoJob", EventLogEntryType.Information, _source, _logName);

            try
            {
                // Obtendo todas as configurações ativas
                var configuracoes = _configuracaoEnvioDocumentacaoRepository.ObterTodos()
                                                                            .Where(ced => ced.Ativo == "S")
                                                                            .ToList();

                LogHelper.Log(string.Format("Configurações encontradas :: {0}.", configuracoes.Count), EventLogEntryType.Information, _source, _logName);

                // Para cada configuração ativa, devemos verificar se não há registros pendentes que não foram inseridos na lista do job de envio de documentação
                foreach (var configuracao in configuracoes)
                {
                    LogHelper.Log(string.Format("Verificando se não há envio pendente para a configuração ID {0}.", configuracao.Id), EventLogEntryType.Information, _source, _logName);
                    this.InserirEnviosPendentes(configuracao);
                }

                // Depois de preenchido a lista com registros, devemos pegar todos os registros pendentes de envio
                var enviosPendentes = _historicoEnvioRepository.ObterPendentes();
                enviosPendentes = enviosPendentes.FilterArray<HistoricoEnvioDocumentacao, int>(p => p.Id, ids);

                LogHelper.Log(string.Format("Envios pendentes encontrados :: {0}.", enviosPendentes.Count), EventLogEntryType.Information, _source, _logName);

                if (!enviosPendentes.Any())
                {
                    Sis.LogTrace("Nenhum e-mail à ser enviado");
                }

                foreach (var envioPendente in enviosPendentes)
                {
                    LogHelper.Log(string.Format("Gerando a documentação da composição {0}.", envioPendente.ComposicaoId), EventLogEntryType.Information, _source, _logName);
                    this.EnviarDocumentacao(envioPendente);
                }
            }
            catch (Exception ex)
            {
                LogHelper.Log(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error, _source, _logName);
            }

            LogHelper.Log("Fim de EnvioDocumentaçãoJob", EventLogEntryType.Information, _source, _logName);
        }

        /// <summary>
        /// Excluí as documentações já criadas e estão vencidas
        /// </summary>
        public void DeletarDocumentacaoVencidas(string[] prefixoTrem = null)
        {
            // Obtendo todos os documentos que sairam da vigencia configurada, portanto, devem ser excluídos.
            var documentacoesVencidas = BL_GerenciamentoDoc.ObterNaoVigentes();

            documentacoesVencidas = documentacoesVencidas.FilterArray(p => p.PrefixoTrem, prefixoTrem);

            if (documentacoesVencidas != null && documentacoesVencidas.Any())
            {
                // Sis.LogTrace("Deletando documentações vencidas: " + documentacoesVencidas.Count);
                Impersonate(() => DeletarDocumentacoes(documentacoesVencidas));
            }
        }

        public void EnviarDocumentacao(HistoricoEnvioDocumentacao envioPendente)
        {
            //var enviosPendentes = _historicoEnvioRepository.ObterPendentes();
            var gerenciamentoDocumentacao = envioPendente.Documentacao ??
                                                _gerenciamentoDocumentacaoRepository.ObterPorOs(envioPendente.OsId, envioPendente.RecebedorId, false);

            if (gerenciamentoDocumentacao != null)
            {
                // Envia o e-mail para os e-mails cadastrados
                LogHelper.Log(String.Format("Enviando os e-mails pendentes. OsId: {0} - RecebedorId: {1} - Recomposicao: {2}", envioPendente.OsId, envioPendente.RecebedorId, envioPendente.RecebedorId), EventLogEntryType.Information, _source, _logName);
                this.EnviarEmails(gerenciamentoDocumentacao, envioPendente);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuracao"></param>
        public void InserirDocumentacaoPendenteCriacaoInicial(Spd.ConfEnvioDoc configuracao, string[] prefixoTrem = null)
        {
            var gerarRelatorioLaudoMercadoria = string.IsNullOrEmpty(configuracao.GerarRelLaudoMercadoria) ? "N" : configuracao.GerarRelLaudoMercadoria;
            var terminalId = configuracao.IdEpIdEmpDest.ToInt32N();
            if (terminalId.HasValue)
            {
                #region Validade da Documentação

                decimal? diasValidadeDocumento = null;
                diasValidadeDocumento = configuracao.DiasValidadeDoc;
                if (!diasValidadeDocumento.HasValue)
                {
                    diasValidadeDocumento = 7;

                    // Obtendo da configuração geral o valor parametrizado
                    var diasValidadeDocumentoParametrizado = _confGeralRepository.ObterPorId("GERENCIAMENTO_DOCUMENTACAO_DIAS_VALIDADE");
                    if (diasValidadeDocumentoParametrizado != null)
                    {
                        int diasValidadeConf;
                        if (Int32.TryParse(diasValidadeDocumentoParametrizado.Valor, out diasValidadeConf))
                        {
                            diasValidadeDocumento = diasValidadeConf > 0 ? diasValidadeConf : 7;
                        }
                    }
                }

                #endregion Validade da Documentação

                // Primeiro, geramos os registros que não são de refaturamento
                // Separamos por região pois não queríamos alterar a lógica já criada
                // O Refaturamento surgiu depois e não podíamos correr o risco de alterar algo que já estava funcionando
                // Por este motivo temos códigos quase idênticos
                #region Documentações que não são de refaturamento

                // Obtendo as OS que ainda não foram geradas a documentação
                var osComposicaoPendentesGeracao = _ordemServicoRepository.ObterOsPendenteGeracao(terminalId.Value);

                osComposicaoPendentesGeracao = osComposicaoPendentesGeracao.FilterArray(p => p.PrefixoTrem, prefixoTrem);

                foreach (var osComposicao in osComposicaoPendentesGeracao)
                {
                    var gerenciamentoDocumentacao = new GerenciamentoDocumentacao
                    {
                        OsId = osComposicao.IdOs,
                        ComposicaoId = osComposicao.IdComposicao,
                        DiasValidade = diasValidadeDocumento ?? 7,
                        Tentativas = 0,
                        OsNumero = osComposicao.NumeroOs,
                        PrefixoTrem = osComposicao.PrefixoTrem,
                        RecebedorId = terminalId.Value,
                        GerarRelatorioLaudoMercadoria = gerarRelatorioLaudoMercadoria
                    };

                    _gerenciamentoDocumentacaoRepository.Inserir(gerenciamentoDocumentacao);
                }

                #endregion Documentações que não são de refaturamento

                // Obtendo os registros de Refaturamento
                // Como comentamos acima, apesar do código ser quase parecido, decidimos não alterar o trecho anterior para não correr o risco de
                // impactar o funcionamento atual
                #region Documentações de Refaturamento

                var documentosRefaturamentos = _gerenciamentoDocumentacaoRefaturamentoRepository.ObterPendentesGeracao(terminalId.Value);
                documentosRefaturamentos = documentosRefaturamentos.FilterArray(p => p.PrefixoTrem, prefixoTrem);

                foreach (var documentoRefaturamento in documentosRefaturamentos)
                {
                    var gerenciamentoDocumentacao = new GerenciamentoDocumentacao
                    {
                        OsId = documentoRefaturamento.OsId,
                        ComposicaoId = documentoRefaturamento.ComposicaoId,
                        DiasValidade = diasValidadeDocumento ?? 7,
                        Tentativas = 0,
                        OsNumero = documentoRefaturamento.NumeroOs,
                        PrefixoTrem = documentoRefaturamento.PrefixoTrem,
                        RecebedorId = documentoRefaturamento.RecebedorId,
                        GerarRelatorioLaudoMercadoria = gerarRelatorioLaudoMercadoria,
                        GerenciamentoDocumentacaoRefaturamento = documentoRefaturamento
                    };

                    _gerenciamentoDocumentacaoRepository.Inserir(gerenciamentoDocumentacao);

                    documentoRefaturamento.Documentacao = gerenciamentoDocumentacao;
                    _gerenciamentoDocumentacaoRefaturamentoRepository.Atualizar(documentoRefaturamento);
                }

                #endregion Documentações de Refaturamento
            }
        }

        public void InserirEnviosPendentes(ConfiguracaoEnvioDocumentacao configuracao)
        {
            // Verificamos se possui algum e-mail ativo cadastrado na configuração
            if (configuracao.Emails.Any(email => email.Ativo == "S"))
            {
                // Verificando todas as composições que não foram enviadas por e-mail anteriormente para aquela configuração
                var areaOperacionalId = configuracao.AreaOperacionalEnvio.EstacaoMae != null
                                            ? configuracao.AreaOperacionalEnvio.EstacaoMae.Id
                                            : configuracao.AreaOperacionalEnvio.Id;

                var terminalId = configuracao.EmpresaDestino.Id;

                if (areaOperacionalId.HasValue && terminalId.HasValue)
                {
                    #region Documentação Chegada

                    if (configuracao.EnviarChegada == "S")
                    {
                        // Obtendo as composições de chegada pendentes
                        var osComposicoesPendentesEnvioChegada = _ordemServicoRepository.ObterOsComposicoesNaoEnviadasChegada(areaOperacionalId.Value, terminalId.Value);

                        foreach (var osComposicao in osComposicoesPendentesEnvioChegada)
                        {
                            var documentacaoId = int.Parse(osComposicao.IdDocumentacao.ToString());
                            var documentacao = _gerenciamentoDocumentacaoRepository.ObterPorId(documentacaoId);

                            // Criando o registro a ser enviado pelo Job de Envios de documentação
                            var emails = configuracao.Emails.Where(email => email.Ativo == "S").Select(email => email.Email).ToList();
                            var historicoEnvio = new HistoricoEnvioDocumentacao(documentacao,
                                                                                osComposicao.IdOs,
                                                                                osComposicao.IdComposicao,
                                                                                osComposicao.IdRecebedor,
                                                                                osComposicao.Recomposicao,
                                                                                string.Empty,
                                                                                null,
                                                                                osComposicao.Chegada,
                                                                                osComposicao.Saida,
                                                                                osComposicao.DataChegada,
                                                                                emails);

                            _historicoEnvioRepository.Inserir(historicoEnvio);
                        }
                    }

                    #endregion Documentação Chegada

                    #region Documentação Normal

                    if (string.IsNullOrEmpty(configuracao.EnviarSaida) || configuracao.EnviarSaida == "S")
                    {
                        // Obtendo as composições pendentes
                        var osComposicoesPendentesEnvio = _ordemServicoRepository.ObterOsComposicoesNaoEnviadas(areaOperacionalId.Value, terminalId.Value);
                        foreach (var osComposicao in osComposicoesPendentesEnvio)
                        {
                            var documentacaoId = int.Parse(osComposicao.IdDocumentacao.ToString());
                            var documentacao = _gerenciamentoDocumentacaoRepository.ObterPorId(documentacaoId);

                            // Criando o registro a ser enviado pelo Job de Envios de documentação
                            var emails =
                                configuracao.Emails.Where(email => email.Ativo == "S").Select(email => email.Email).
                                    ToList();
                            var historicoEnvio = new HistoricoEnvioDocumentacao(documentacao,
                                                                                osComposicao.IdOs,
                                                                                osComposicao.IdComposicao,
                                                                                osComposicao.IdRecebedor,
                                                                                osComposicao.Recomposicao,
                                                                                string.Empty,
                                                                                null,
                                                                                osComposicao.Chegada,
                                                                                osComposicao.Saida,
                                                                                osComposicao.DataChegada,
                                                                                emails);

                            _historicoEnvioRepository.Inserir(historicoEnvio);
                        }
                    }

                    #endregion Documentação Normal

                }

                #region Documentação de Refaturamento

                if (terminalId.HasValue)
                {
                    // Obtendo as composições pendentes
                    var enviosRefaturamentoPendentes = _ordemServicoRepository.ObterOsComposicoesNaoEnviadasRefaturamento(terminalId.Value);
                    foreach (var envioRefaturamentoPendente in enviosRefaturamentoPendentes)
                    {
                        var documentacaoId = int.Parse(envioRefaturamentoPendente.IdDocumentacao.ToString());
                        var documentacao = _gerenciamentoDocumentacaoRepository.ObterPorId(documentacaoId);

                        // Criando o registro a ser enviado pelo Job de Envios de documentação
                        var emails = configuracao.Emails.Where(email => email.Ativo == "S").Select(email => email.Email).ToList();
                        var historicoEnvio = new HistoricoEnvioDocumentacao(documentacao,
                                                                            documentacao.OsId,
                                                                            documentacao.ComposicaoId,
                                                                            documentacao.RecebedorId ?? terminalId.Value,
                                                                            "N",
                                                                            string.Empty,
                                                                            null,
                                                                            "N",
                                                                            "N",
                                                                            null,
                                                                            emails);
                        _historicoEnvioRepository.Inserir(historicoEnvio);
                    }
                }

                #endregion Documentação de Refaturamento
            }
        }

        public void InserirEnviosManual(int id, decimal? usuarioId)
        {
            var documentacao = _gerenciamentoDocumentacaoRepository.ObterPorId(id);
            if (documentacao != null)
            {
                var configuracao = _configuracaoEnvioDocumentacaoRepository.ObterPorParametros(string.Empty, Convert.ToInt32(documentacao.RecebedorId.Value));

                // Verificando se já não há um registro manual pendente a ser enviado
                var historicoEnvioPendente = _historicoEnvioRepository.ObterPendentes()
                                                                      .Where(hed =>
                                                                                hed.Documentacao.Id == id &&
                                                                                hed.Manual == "S" &&
                                                                                hed.Enviado == "N")
                                                                      .FirstOrDefault();

                if (historicoEnvioPendente == null)
                {
                    var emails = configuracao.Emails.Where(email => email.Ativo == "S").Select(email => email.Email).ToList();
                    var historicoEnvio = new HistoricoEnvioDocumentacao(documentacao,
                                                                        documentacao.OsId,
                                                                        documentacao.ComposicaoId,
                                                                        documentacao.RecebedorId.Value,
                                                                        string.Empty, // Envio manual
                                                                        "S",
                                                                        usuarioId,
                                                                        "N",    // Chegada
                                                                        "S",    // Saida
                                                                        null,   // Data de Chegada
                                                                        emails);
                    _historicoEnvioRepository.Inserir(historicoEnvio);
                }
            }
        }

        public void InserirEnviosManual(string numeroOs, int? recebedorId, decimal? usuarioId)
        {
            if (string.IsNullOrEmpty(numeroOs))
            {
                throw new ArgumentEmptyException("Informe um número de OS.");
            }

            var documentacoes = _gerenciamentoDocumentacaoRepository.ObterPorNumeroOs(numeroOs, recebedorId);
            foreach (var documentacao in documentacoes)
            {
                var configuracao = _configuracaoEnvioDocumentacaoRepository.ObterPorParametros(string.Empty, Convert.ToInt32(documentacao.RecebedorId.Value));

                // Verificando se já não há um registro manual pendente a ser enviado
                var historicoEnvioPendente = _historicoEnvioRepository.ObterPendentes()
                                                                      .Where(hed =>
                                                                                hed.RecebedorId == documentacao.RecebedorId &&
                                                                                hed.OsId == documentacao.OsId &&
                                                                                hed.Manual == "S" &&
                                                                                hed.Enviado == "N")
                                                                      .FirstOrDefault();
                if (historicoEnvioPendente == null)
                {
                    var emails = configuracao.Emails.Where(email => email.Ativo == "S").Select(email => email.Email).ToList();
                    var historicoEnvio = new HistoricoEnvioDocumentacao(documentacao,
                                                                        documentacao.OsId,
                                                                        documentacao.ComposicaoId,
                                                                        documentacao.RecebedorId.Value,
                                                                        string.Empty,
                                                                        "S",
                                                                        usuarioId,
                                                                        "N",    // Chegada
                                                                        "S",    // Saida
                                                                        null,   // Data de Chegada
                                                                        emails);
                    _historicoEnvioRepository.Inserir(historicoEnvio);
                }
            }
        }

        public void GerarDocumentacao(string[] prefixoTrem = null)
        {
            // Obtendo todas as configurações ativas
            //var configuracoes = _configuracaoEnvioDocumentacaoRepository.ObterTodos().Where(ced => ced.Ativo == "S").ToList();
            var configuracoes = BLL.BL_ConfEnvioDoc.Select(new Spd.ConfEnvioDoc { Ativo = "S" });

            // Para cada configuração ativa, devemos verificar se não há registros pendentes que não foram inseridos na lista do job de envio de documentação
            Sis.LogTrace("InserirDocumentacaoPendenteCriacaoInicial. Configuracoes: " + configuracoes.Count);
            // TODO: PERFORMANCE2
            foreach (var configuracao in configuracoes)
            {
                this.InserirDocumentacaoPendenteCriacaoInicial(configuracao, prefixoTrem);
            }

            Impersonate(() => gerarDocumentacao(prefixoTrem));
        }

        private void gerarDocumentacao(string[] prefixoTrem = null)
        {
            Sis.LogTrace("PrepararCache");
            BL_VwDadosDocumento.PrepararCache();

            Sis.LogTrace("GerarDocumentacaoInicial");
            GerarDocumentacaoInicial(prefixoTrem);

            Sis.LogTrace("GerarDocumentacaoRecomposicao");
            GerarDocumentacaoRecomposicao(prefixoTrem);

            Sis.LogTrace("AtualizarDocumentacaoRefaturamento");
            AtualizarDocumentacaoRefaturamento(prefixoTrem);
        }

        public void GerarDocumentacaoInicial(string[] prefixoTrem = null)
        {
            var documentosPendentes = BL_GerenciamentoDoc.ObterNaoGeradosInicialmente();
            documentosPendentes = documentosPendentes.FilterArray(p => p.PrefixoTrem, prefixoTrem);

            int count = 0;
            foreach (var documento in documentosPendentes)
            {
                count++;
                string msg = string.Format("Processando trem Início -  Trem: {0} - OS: {1} - Composicao: {2} - ID_DOC: {3} --> {4} de {5}", documento.PrefixoTrem, documento.OsNumero, documento.ComposicaoId, documento.Id, count, documentosPendentes.Count);
                Sis.LogTrace(msg);
                var refaturamento = documento.IdGerenciamentoDocRefat != null;
                var urlBase = this.CriarDiretorioTemporario(documento.OsId.ToString(), documento.RecebedorId.ToString(), refaturamento);

                documento.ChaveAcesso = this.GerarChaveAcesso(urlBase);
                documento.UrlAcesso = urlBase;
                documento.Tentativas = documento.Tentativas + 1;

                try
                {
                    this.GerarPdfsDocumentos(documento, urlBase, refaturamento);

                    if (refaturamento)
                    {
                        var gerDocFat = BL_GerenciamentoDocRefat.SelectByPk(documento.IdGerenciamentoDocRefat.GetValueOrDefault());
                        if (gerDocFat != null)
                        {
                            gerDocFat.Processado = "S";
                            BL_GerenciamentoDocRefat.Update(gerDocFat);
                        }
                    }

                    documento.CriadoEm = DateTime.Now;
                }
                catch (Exception ex)
                {
                    LogHelper.Log(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error, _source, _logName);

                    documento.ErroEm = DateTime.Now;
                    documento.MensagemErro = ex.Message;
                }

                Sis.LogTrace("Processando trem Fim: " + documento.PrefixoTrem + " - OS: " + documento.OsNumero);
                BL_GerenciamentoDoc.Update(documento);
            }
        }

        public void GerarDocumentacaoRecomposicao(string[] prefixoTrem = null)
        {
            var osComposicoes = _ordemServicoRepository.ObterOsPendenteAtualizacao();

            osComposicoes = osComposicoes.FilterArray(p => p.PrefixoTrem, prefixoTrem);

            Sis.LogTrace("GerarDocumentacaoRecomposicao: " + osComposicoes.Count + " recomposições");

            int count = 0;
            foreach (var osComposicao in osComposicoes)
            {
                count++;
                //Sis.LogTrace("Processando recomposição - ID: " + osComposicao.IdComposicao + ".  " + count + " de " + osComposicoes.Count + " recomposições");
                var documento = BL_GerenciamentoDoc.ObterPorOs(osComposicao.IdOs, osComposicao.IdRecebedor, false);
                // var documento = _gerenciamentoDocumentacaoRepository.ObterPorOs(osComposicao.IdOs, osComposicao.IdRecebedor, false);
                if (documento != null)
                {
                    var urlBase = documento.UrlAcesso;

                    documento.ComposicaoId = osComposicao.IdComposicao;
                    documento.Tentativas = 1;

                    try
                    {
                        this.GerarPdfsDocumentos(documento, urlBase, false);

                        documento.CriadoEm = DateTime.Now;
                    }
                    catch (Exception ex)
                    {
                        LogHelper.Log(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error, _source, _logName);

                        documento.ErroEm = DateTime.Now;
                        documento.MensagemErro = ex.Message;
                    }

                    BL_GerenciamentoDoc.Update(documento);
                }
            }
        }

        public void AtualizarDocumentacaoRefaturamento(string[] prefixoTrem = null)
        {
            // var documentacoesRefaturamentoPendentes = _gerenciamentoDocumentacaoRefaturamentoRepository.ObterPendentesAtualizacao();
            var documentacoesRefaturamentoPendentes = BL_GerenciamentoDocRefat.ObterPendentesAtualizacao();

            documentacoesRefaturamentoPendentes = documentacoesRefaturamentoPendentes.FilterArray(p => p.PrefixoTrem, prefixoTrem);

            Sis.LogTrace("AtualizarDocumentacaoRefaturamento: " + documentacoesRefaturamentoPendentes.Count + " refaturamentos");

            int count = 0;
            foreach (var documentacaoPendente in documentacoesRefaturamentoPendentes)
            {
                count++;
                // Sis.LogTrace("Processando refaturamento - Trem: " + documentacaoPendente.PrefixoTrem + " - Recebedor: " + documentacaoPendente.RecebedorId + ".  " + count + " de " + documentacoesRefaturamentoPendentes.Count + " recomposições");

                var idOs = documentacaoPendente.OsId;
                var idRecebedor = documentacaoPendente.RecebedorId;
                var documento = BL_GerenciamentoDoc.SelectSingle(new Spd.GerenciamentoDoc { IdGerenciamentoDocRefat = documentacaoPendente.Id });
                if (documento != null)
                {
                    var urlBase = documento.UrlAcesso;

                    documento.Tentativas = 1;
                    BL_GerenciamentoDoc.Update(documento);

                    try
                    {
                        this.GerarPdfsDocumentos(documento, urlBase, true);

                        documentacaoPendente.Processado = "S";
                        BL_GerenciamentoDocRefat.Update(documentacaoPendente);

                        documento.CriadoEm = DateTime.Now;
                    }
                    catch (Exception ex)
                    {
                        LogHelper.Log(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error, _source, _logName);

                        documento.ErroEm = DateTime.Now;
                        documento.MensagemErro = ex.Message;
                    }

                    BL_GerenciamentoDoc.Update(documento);
                }
            }
        }

        public string GerarChaveAcesso(string urlBase)
        {
            LogHelper.Log(String.Format("Gerando a chave de acesso para a URL :: {0}", urlBase), EventLogEntryType.Information, _source, _logName);

            var maxSize = 124;
            var chars = new char[63];
            chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!".ToCharArray();

            var data = new byte[1];
            using (var crypto = new RNGCryptoServiceProvider())
            {
                crypto.GetNonZeroBytes(data);
                data = new byte[maxSize];
                crypto.GetNonZeroBytes(data);
            }

            var result = new StringBuilder(maxSize);
            foreach (byte b in data)
            {
                result.Append(chars[b % (chars.Length)]);
            }

            var chaveAcesso = result.ToString();

            LogHelper.Log(String.Format("Chave de acesso gerada com sucesso para a URL {0} :: {1}", urlBase, chaveAcesso), EventLogEntryType.Information, _source, _logName);

            return chaveAcesso;
        }

        public void GerarPdfsDocumentos(Spd.GerenciamentoDoc documento, string urlBase, bool refaturamento)
        {
            LogHelper.Log(String.Format("Gerando a documentação na pasta :: {0}", urlBase), EventLogEntryType.Information, _source, _logName);

            List<Spd.VwDadosDocumento> dadosDocumento = null;

            if (!refaturamento)
            {
                // TODO: PERFORMANCE2
                Sis.LogTrace("ObterDadosDocumento");
                // dadosDocumento = _gerenciamentoDocumentacaoRepository.ObterDadosDocumento(documento.OsId.GetValueOrDefault(), documento.RecebedorId ?? 0);
                dadosDocumento = BL_VwDadosDocumento.ObterDadosDocumento(documento.OsId, documento.RecebedorId);
            }
            else
            {
                // TODO: PERFORMANCE2
                Sis.LogTrace("ObterDadosDocumentoRefaturamento");
                // dadosDocumento = _gerenciamentoDocumentacaoRepository.ObterDadosDocumentoRefaturamento(documento.OsId.GetValueOrDefault(), documento.RecebedorId ?? 0);
                dadosDocumento = BL_VwDadosDocumento.ObterDadosDocumentoRefaturamento(documento.OsId, documento.RecebedorId);
            }

            if (!dadosDocumento.Any())
            {
                // TODO: PERFORMANCE2
                Sis.LogTrace("ObterDadosDocumentoHistorico");
                //dadosDocumento = _gerenciamentoDocumentacaoRepository.ObterDadosDocumentoHistorico(documento.OsId.GetValueOrDefault(), documento.RecebedorId ?? 0);
                dadosDocumento = BL_VwDadosDocumento.ObterDadosDocumentoHistorico(documento.OsId, documento.RecebedorId);
            }

            Sis.LogTrace("dadosDocumento");
            if (dadosDocumento.Any())
            {
                #region Tratamento dos dados

                var gerarRelatorioLaudo = documento.GerarRelLaudoMercadoria == "S";
                var itensDespachos = dadosDocumento.Select(i => (int)i.IdItemDespacho).Distinct().ToList();

                var dadosVagaoItemDespacho = dadosDocumento.Select(dd => new ImpressaoDocumentosVagaoItemDespacho
                {
                    IdItemDespacho = dd.IdItemDespacho.GetValueOrDefault(),
                    CodigoVagao = dd.CodigoVagao,
                    CodigoFluxo = dd.CodigoFluxo,
                    DataCarga = dd.DataCarga.GetValueOrDefault(),
                    IdCte = 0,
                    IdComposicao = dd.IdComposicao,
                    Sequencia = dd.Sequencia,
                    SegmentoFluxo = dd.SegmentoFluxo
                })
                                                           .GroupBy(dd => dd.IdItemDespacho)
                                                           .Select(x => x.First())
                                                           .ToList();

                var despachoId = dadosDocumento.Select(dd => int.Parse(dd.IdDespacho.ToString())).FirstOrDefault();
                Sis.LogTrace("ObterIdMdfePorDespacho");
                var idMdfe = _mdfeService.ObterIdMdfePorDespacho(despachoId);
                Sis.LogTrace("ObterIdMdfePorDespacho depois");

                var laudoMercadoriasDto = dadosDocumento.Where(dd => dd.LaId.HasValue)
                                                        .Select(dd => new LaudoMercadoriaDto
                                                        {
                                                            Id = dd.LaId ?? 0,
                                                            ItemDespachoId = dd.IdItemDespacho.GetValueOrDefault(),
                                                            CodigoVagao = dd.CodigoVagao,
                                                            ClienteNome = dd.LaClienteNome,
                                                            ClienteCnpj = dd.LaClienteCnpj,
                                                            DataClassificacao = dd.LaDataClassificacao ?? DateTime.Now,
                                                            NumeroLaudo = dd.LaNumeroLaudo,
                                                            NumeroOsLaudo = dd.LaNumeroOsLaudo,
                                                            PesoLiquido = dd.LaPesoLiquido ?? 0,
                                                            Destino = dd.LaDestino,
                                                            ClassificadorNome = dd.LaClassificadorNome,
                                                            ClassificadorCpf = dd.LaClassificadorCpf,
                                                            AutorizadoPor = dd.LaAutorizadoPor,
                                                            TipoLaudo = dd.LaTipoLaudo,
                                                            EmpresaNome = dd.LaEmpresaNome,
                                                            NumeroNfe = dd.LaNumeroNfe,
                                                            ChaveNfe = dd.LaChaveNfe,

                                                            EmpresaGeradoraId = dd.LaEmpresaGeradoraId ?? 0,
                                                            EmpresaGeradoraNome = dd.LaEmpresaGeradoraNome,
                                                            EmpresaGeradoraCnpj = dd.LaEmpresaGeradoraCnpj,
                                                            EmpresaGeradoraEndereco = dd.LaEmpresaGeradoraEndereco,
                                                            EmpresaGeradoraCep = dd.LaEmpresaGeradoraCep,
                                                            EmpresaGeradoraCidadeDescricao = dd.LaEmpresaGeradoraCidade,
                                                            EmpresaGeradoraEstadoSigla = dd.LaEmpresaGeradoraEstSigla,
                                                            EmpresaGeradoraIe = dd.LaEmpresaGeradoraIe,
                                                            EmpresaGeradoraTelefone = dd.LaEmpresaGeradoraTelefone,
                                                            EmpresaGeradoraFax = dd.LaEmpresaGeradoraFax,

                                                            Produto = dd.LaProduto,
                                                            Classificacao = dd.LaClassificacao,
                                                            ClassificacaoOrdem = dd.LaClassificacaoOrdem,
                                                            Porcentagem = dd.LaPorcentagem ?? 0,
                                                            DescricaoProduto = dd.LaDescricaoProduto
                                                        })

                                                        .GroupBy(dd => new
                                                        {
                                                            dd.Id,
                                                            dd.CodigoVagao,
                                                            dd.Classificacao
                                                        })
                                                        .Select(g => g.First())
                                                        .ToList();


                var notasNfe = dadosDocumento.Where(dd => !string.IsNullOrEmpty(dd.ChaveNfe))
                                             .Select(dd => new ItemDespachoNfeDto
                                             {
                                                 IdItemDespacho = dd.IdItemDespacho.GetValueOrDefault(),
                                                 IdNfePdf = dd.IdNfePdf ?? 0,
                                                 ChaveNfe = dd.ChaveNfe
                                             })
                                             .GroupBy(dd => new
                                             {
                                                 dd.IdItemDespacho,
                                                 dd.IdNfePdf,
                                                 dd.ChaveNfe
                                             })
                                             .Select(g => g.First())
                                             .ToList();

                var chavesNfes = notasNfe.Select(nf => nf.ChaveNfe).ToList();
                Sis.LogTrace("_relatorioDocumentosService.GerarPdfNfes");
                var pdfsNfes = _relatorioDocumentosService.GerarPdfNfes(chavesNfes);

                // TODO: PERFORMANCE2
                Sis.LogTrace("ObterTicketItensDespachoDto");
                var notasTickets = _relatorioDocumentosService.ObterTicketItensDespachoDto(itensDespachos, true);
                Sis.LogTrace("ObterTicketClienteItensDespacho");
                var notasTicketsClientes = _relatorioDocumentosService.ObterTicketClienteItensDespacho(itensDespachos, true);
                var dadosNotasTotais = dadosDocumento.Where(dd => !string.IsNullOrEmpty(dd.ChaveNfe))
                                                     .Select(dd => new NotaTicketItemDespachoDto
                                                     {
                                                         IdItemDespacho = dd.IdItemDespacho.GetValueOrDefault(),
                                                         ChaveNfe = dd.ChaveNfe,
                                                         Serie = dd.Serie,
                                                         Numero = int.Parse(dd.Numero.ToString()),
                                                         Produto = dd.Produto,
                                                         Peso = double.Parse(dd.Peso.ToString()),
                                                         PesoDisponivel = double.Parse(dd.PesoDisponivel.ToString())
                                                     })
                                                     .GroupBy(dd => new
                                                     {
                                                         dd.ChaveNfe,
                                                         dd.IdItemDespacho
                                                     })
                                                     .Select(g => g.First())
                                                     .ToList();

                var dadosVagaoTickets = new List<NotaTicketVagao>();

                Sis.LogTrace("ObterDespachosPorItensDespacho");
                var itemDespachos = BL_ItemDespacho.ObterDespachosPorItensDespacho(itensDespachos.ToArray());
                Sis.LogTrace("ObterDclImpressao");
                var dcls = _dclService.ObterDclImpressao(itemDespachos);

                #endregion Tratamento dos dados

                #region Documentações dos Vagões
                Sis.LogTrace("Documentações dos Vagões");

                var documentoNfeCompleto = true;
                var documentoTicketCompleto = true;
                var documentoLaudoCompleto = true;

                var documentos = new List<DocumentosVagaoDto>();
                int count = 0;
                foreach (var dadoVagao in dadosVagaoItemDespacho)
                {
                    count++;
                    // Sis.LogTrace("Processando vagão: " + count + " de " + dadosVagaoItemDespacho.Count);

                    var fluxoSegmentoLiquido = dadoVagao.SegmentoFluxo == "LIQUID";
                    var fluxoSegmentoBrado = dadoVagao.SegmentoFluxo == "BRADO";
                    var fluxoSegmentoIndustrial = dadoVagao.SegmentoFluxo == "INDUSTR";

                    var vagaoNfeCompleto = false;
                    var vagaoTicketCompleto = false;
                    var vagaoLaudoCompleto = false;

                    var inserirTicket = true;
                    var documentoPdfVagao = documentos.FirstOrDefault(dd => dd.CodigoVagao == dadoVagao.CodigoVagao);
                    if (documentoPdfVagao == null)
                    {
                        documentoPdfVagao = new DocumentosVagaoDto();
                        documentoPdfVagao.CodigoVagao = dadoVagao.CodigoVagao;
                        documentoPdfVagao.Sequencia = dadoVagao.Sequencia;
                        documentoPdfVagao.DataCarga = dadoVagao.DataCarga;
                        documentos.Add(documentoPdfVagao);
                    }

                    if (!documentoPdfVagao.CodigoFluxos.Any(fx => fx == dadoVagao.CodigoFluxo))
                    {
                        inserirTicket = true;
                        documentoPdfVagao.CodigoFluxos.Add(dadoVagao.CodigoFluxo);
                    }

                    var documentosDoVagao = documentoPdfVagao.Pdfs;

                    // Listas dos PDFs
                    List<ItemDespachoNfeDto> pdfsNfe = null;
                    Stream pdfDcl = null;
                    Stream pdfLaudoMercadoria = null;
                    Stream ticketPesagemImpressao = null;

                    #region NFe

                    if (notasNfe.Any())
                    {
                        // Obtendo as notas fiscais do vagões da vez
                        var chavesNfeDespacho = notasNfe.Where(n => n.IdItemDespacho == dadoVagao.IdItemDespacho)
                                                        .Select(n => n.ChaveNfe)
                                                        .Distinct()
                                                        .ToList();

                        if (chavesNfeDespacho.Any())
                        {
                            pdfsNfe = pdfsNfes.Where(c => chavesNfeDespacho.Contains(c.ChaveNfe) && c.Pdf != null).ToList();
                            foreach (var pdfNfe in pdfsNfe)
                            {
                                documentosDoVagao.AddFile(pdfNfe.Pdf);
                            }

                            if (pdfsNfe.Count == chavesNfeDespacho.Count)
                            {
                                vagaoNfeCompleto = true;
                            }
                        }
                    }

                    // Espera-se NF-e para todos os vagões
                    documentoNfeCompleto = documentoNfeCompleto && vagaoNfeCompleto;

                    #endregion

                    #region DCL

                    var despachosIds =
                        itemDespachos.Where(d => d.IdItemDespacho == dadoVagao.IdItemDespacho).Select(d => d.IdDespacho).ToList();

                    var dclsVagao = dcls.Where(d => despachosIds.Contains(d.IdDespacho));

                    if (dclsVagao.Any())
                    {
                        var dclsImpressao = new List<DclImpressao>();
                        dclsImpressao.AddRange(dclsVagao.AsEnumerable());
                        pdfDcl = _relatorioDocumentosService.GerarDclPdfLote(dclsImpressao);
                        documentosDoVagao.AddFile(pdfDcl);
                    }

                    #endregion

                    #region Laudo Mercadoria

                    var dtoDespacho =
                        laudoMercadoriasDto.Where(l => l.CodigoVagao == dadoVagao.CodigoVagao && l.DataClassificacao >= dadoVagao.DataCarga.AddDays(-3)).OrderByDescending(x => x.DataClassificacao).Distinct().ToList();
                    var laudoMercadoriaImpressao = _laudoMercadoriaService.ObterLaudoMercadoriaImpressao(dtoDespacho);
                    if (laudoMercadoriaImpressao.Any())
                    {
                        foreach (var impressao in laudoMercadoriaImpressao)
                        {
                            pdfLaudoMercadoria = _relatorioDocumentosService.GerarLaudoMercadoriaPdf(impressao);
                            documentosDoVagao.AddFile(pdfLaudoMercadoria);
                            vagaoLaudoCompleto = true;
                        }
                    }

                    // Espera-se laudo para os documentos que estejam configurados na T1398 como gerar laudo relatório
                    if (documento.GerarRelLaudoMercadoria == "S")
                    {
                        documentoLaudoCompleto = documentoLaudoCompleto && vagaoLaudoCompleto;
                    }

                    #endregion

                    #region Ticket Pesagem

                    if (inserirTicket)
                    {
                        var itemDespachosIds =
                            dadosVagaoItemDespacho.Where(
                                dd => dd.CodigoVagao == dadoVagao.CodigoVagao && dd.CodigoFluxo == dadoVagao.CodigoFluxo)
                                .Select(dd => dd.IdItemDespacho)
                                .Distinct()
                                .ToList();

                        var notasTicket = new List<string>();
                        if ((notasTickets != null) && (notasTickets.Count > 0))
                        {
                            notasTicket =
                                notasTickets.Where(t => itemDespachosIds.Any(itd => itd == t.IdItemDespacho))
                                    .Select(t => t.NotaTicketVagao.ChaveNfe)
                                    .Distinct()
                                    .ToList();
                        }

                        if (notasTicket.Any())
                        {
                            var dadosNotas = dadosNotasTotais.Where(d => notasTicket.Contains(d.ChaveNfe))
                                .Select(dd => new NotaTicketDto
                                {
                                    ChaveNfe = dd.ChaveNfe,
                                    Serie = dd.Serie,
                                    Numero = int.Parse(dd.Numero.ToString()),
                                    Produto = dd.Produto,
                                    Peso = double.Parse(dd.Peso.ToString()),
                                    PesoDisponivel = double.Parse(dd.PesoDisponivel.ToString())
                                })
                                .GroupBy(dd => dd.ChaveNfe)
                                .Select(dd => dd.First())
                                .ToList();

                            var dadosTicket =
                                notasTickets.Where(t => itemDespachosIds.Any(itd => itd == t.IdItemDespacho))
                                    .Select(t => t.NotaTicketVagao)
                                    .ToList();

                            var dadosCliente =
                                notasTicketsClientes.Where(t => t.IdItemDespacho == dadoVagao.IdItemDespacho).ToList();

                            var notasTicketVagao =
                                notasTickets.Where(t => itemDespachosIds.Any(itd => itd == t.IdItemDespacho))
                                    .GroupBy(nt => new
                                    {
                                        nt.NotaTicketVagao.ChaveNfe
                                    })
                                    .Select(g => g.First())
                                    .ToList();

                            dadosVagaoTickets.Add(dadosTicket.First());

                            if (dadosCliente.FirstOrDefault() != null)
                            {
                                ticketPesagemImpressao = _relatorioDocumentosService.GerarTicketPdfIndividual(
                                    dadosTicket.First(),
                                    notasTicketVagao,
                                    dadosNotas,
                                    dadosCliente.First().Cliente);

                                if (ticketPesagemImpressao != null)
                                {
                                    documentosDoVagao.AddFile(ticketPesagemImpressao);
                                    vagaoTicketCompleto = true;
                                }
                            }
                        }

                        // Espera-se ticket para os fluxos que não sejam de Líquidos, Brado e nem Industrial
                        if (!fluxoSegmentoLiquido && !fluxoSegmentoBrado && !fluxoSegmentoIndustrial)
                        {
                            documentoTicketCompleto = documentoTicketCompleto && vagaoTicketCompleto;
                        }
                    }

                    #endregion
                }

                var documentacaoDirectory = Path.Combine(urlBase, "documentacao");
                Directory.CreateDirectory(documentacaoDirectory);

                count = 0;
                Sis.LogTrace("documentos");
                foreach (var dadoVagao in documentos)
                {
                    count++;
                    //Sis.LogTrace("Processando dcumento: " + count + " de " + dadosVagaoItemDespacho.Count);

                    var vagaoDirectory = Path.Combine(documentacaoDirectory,
                                                          String.Format("{0}. {1}",
                                                                        dadoVagao.Sequencia.HasValue
                                                                            ? dadoVagao.Sequencia.Value.ToString("00")
                                                                            : "00",
                                                                       dadoVagao.CodigoVagao));
                    Directory.CreateDirectory(vagaoDirectory);

                    var documentosDoVagao = dadoVagao.Pdfs;

                    if (documentosDoVagao.FileListCount() > 0)
                    {
                        try
                        {
                            var nomeArquivo = "arquivo_" + dadoVagao.CodigoVagao + "_" +
                                              dadoVagao.DataCarga.ToString("yyyyMMdd") + ".pdf";
                            using (var stream = File.Create(Path.Combine(vagaoDirectory, nomeArquivo)))
                            {
                                var pdf = ToByteArray(documentosDoVagao.Execute());
                                stream.Write(pdf, 0, pdf.Length);
                            }
                        }
                        // Ocorreu exception pois algum PDF de NFe ficou corrompido
                        // Isso ocorre normalmente pois o cliente envia incorretamento o PDF.
                        // Foi incluído uma validação no Job.Runner para ignorar os casos corrompidos, portanto, esta exceção é apenas uma precausão.
                        // O Terminal não vai receber a documentação do vagão com erro, mas pelo menos ele não deixa de receber a documentação dos outros vagões.
                        catch (Exception exc)
                        {
                        }
                    }
                }

                #endregion Documentações dos Vagões

                #region MDF-e

                // Gerando o arquivo MDF-e ou seu substituto apenas quando não for refaturamento
                if (!refaturamento)
                {
                    Sis.LogTrace("!refaturamento");
                    var mdfeDirectory = Path.Combine(documentacaoDirectory, "_mdfe");
                    var nomeArquivo = string.Empty;
                    byte[] pdfMdfe = null;

                    if (idMdfe > 0)
                    {
                        nomeArquivo = "01. mdfe.pdf";

                        var dadosMdfe = _mdfeService.GerarPdf(Convert.ToInt32(idMdfe));
                        pdfMdfe = ToByteArray(dadosMdfe);

                        if (pdfMdfe.Length > 0)
                        {
                            Directory.CreateDirectory(mdfeDirectory);

                            using (var stream = File.Create(Path.Combine(mdfeDirectory, nomeArquivo)))
                            {
                                stream.Write(pdfMdfe, 0, pdfMdfe.Length);
                            }
                        }
                    }
                }

                #endregion MDF-e

                #region Relatórios

                var indexRelatorios = 1;

                var relatoriosDirectory = Path.Combine(documentacaoDirectory, "_relatorios");
                Directory.CreateDirectory(relatoriosDirectory);

                #region Resumo Composição

                var nomeArquivoRelatorioComposicao = string.Format("{0}. resumo-composicao.pdf",
                                                                   indexRelatorios.ToString().PadLeft(2, '0'));

                Sis.LogTrace("ObterManifestoCargaPdf");
                var dadosManifestoCarga = _mdfeService.ObterManifestoCargaPdf(documento.OsId.GetValueOrDefault(), documento.RecebedorId ?? 0,
                                                                              dadosVagaoTickets, refaturamento);
                Sis.LogTrace("ObterManifestoCargaPdf depois");
                var pdfRelatorioComposicao = ToByteArray(dadosManifestoCarga);
                if (pdfRelatorioComposicao.Length > 0)
                {
                    using (var stream = File.Create(Path.Combine(relatoriosDirectory, nomeArquivoRelatorioComposicao)))
                    {
                        stream.Write(pdfRelatorioComposicao, 0, pdfRelatorioComposicao.Length);
                        indexRelatorios++;
                    }
                }

                #endregion Resumo Composição

                #region Laudo Mercadorias - Sintético

                if (gerarRelatorioLaudo && !refaturamento)
                {
                    Sis.LogTrace("gerarRelatorioLaudo && !refaturamento");
                    var nomeArquivo = string.Format("{0}. laudo-mercadorias.xlsx",
                                                    indexRelatorios.ToString().PadLeft(2, '0'));
                    // TODO: PERFORMANCE2
                    Sis.LogTrace("ObterRelatorioLaudosPorOsId");
                    var resultados = _painelExpedicaoService.ObterRelatorioLaudosPorOsId(documento.OsId.GetValueOrDefault(),
                                                                                         documento.RecebedorId ?? 0);
                    if (resultados.Count > 0)
                    {
                        var relatorioLaudos = new RelatorioLaudosMercadoriaExcelService(resultados);
                        var colunas = relatorioLaudos.RetornaNomeColunas();

                        var excelStream =
                            relatorioLaudos.GerarRelatorioCustomizado(
                                string.Format("Relatório Laudos Mercadoria - OS {0} : Trem {1}", documento.OsNumero,
                                              documento.PrefixoTrem), null, colunas);

                        var memoryStream = excelStream as MemoryStream;
                        excelStream.Seek(0, SeekOrigin.Begin);

                        var excelLaudo = memoryStream.ToArray();
                        using (var stream = File.Create(Path.Combine(relatoriosDirectory, nomeArquivo)))
                        {
                            stream.Write(excelLaudo, 0, excelLaudo.Length);
                            indexRelatorios++;
                        }
                    }
                }

                #endregion Laudo Mercadorias - Sintético

                #endregion Relatórios

                Sis.LogTrace("Compacta a pasta de documentos");
                // Compacta a pasta de documentos
                var z = new FastZip();
                z.CreateEmptyDirectories = true;
                z.CreateZip(String.Format("{0}/documentacao.zip", urlBase), documentacaoDirectory, true, String.Empty);

                this.DeletarDiretorioTemporario(documentacaoDirectory);

                documento.NfeCompleto = documentoNfeCompleto ? "S" : "N";
                documento.TicketCompleto = documentoTicketCompleto ? "S" : "N";
                documento.LaudoCompleto = documentoLaudoCompleto ? "S" : "N";
                BL_GerenciamentoDoc.Update(documento);

                var codigoVagoes =
                    dadosVagaoItemDespacho.Select(dv => dv.CodigoVagao).Distinct().OrderBy(dv => dv).ToList();
                Sis.LogTrace("InserirVagoesCriados");
                InserirVagoesCriados(documento, codigoVagoes);

                LogHelper.Log(String.Format("Documentação gerada com sucesso na pasta :: {0}", urlBase), EventLogEntryType.Information, _source, _logName);
            }
            else
            {
                LogHelper.Log(String.Format("Não encontrados dados de documentos, documentação não gerada para a Os: {0} e Recebedor: {1}", documento.OsId, documento.RecebedorId), EventLogEntryType.Information, _source, _logName);
            }
            Sis.LogTrace("GerarPdfsDocumentos FIM");
        }

        public void InserirVagoesCriados(Spd.GerenciamentoDoc documento, List<string> codigoVagoes)
        {
            foreach (var codigoVagao in codigoVagoes)
            {
                var gerenciamentoDocumentacaoCompVagao = new Spd.GerenciamentoDocCompvag
                {
                    CodigoVagao = codigoVagao,
                    ComposicaoId = documento.ComposicaoId,
                    CriadoEm = DateTime.Now,
                    GerenciamentoDocId = documento.Id,
                    OsId = documento.OsId,
                    RecebedorId = documento.RecebedorId ?? 0
                };

                BL_GerenciamentoDocCompvag.Insert(gerenciamentoDocumentacaoCompVagao);
            }
        }

        public void EnviarEmails(GerenciamentoDocumentacao documento, HistoricoEnvioDocumentacao envioPendente)
        {
            // Quando ocorrer o erro 'The underlying connection was closed', faz um throw do erro para forçar o job parar
            // só rodará no proóximo agendamento
            Exception ex = null;

            try
            {
                #region Variáveis de configuração de e-mail
                bool isKMM = true;

                var destinatarios = envioPendente.Emails.Where(e => e.Enviado == "N" && !string.IsNullOrEmpty(e.Email)).ToList();
                var emailDestinatarios = string.Join(",", destinatarios.Select(e => e.Email).ToList());

#if DEBUG
                emailDestinatarios = "carlosast@msn.com;";
#endif

                if (!emailDestinatarios.Any())
                {
                    LogHelper.Log(String.Format("Não existe e-mail a ser enviado para a OsId: {0} - RecebedorId: {1} - Recomposicao: {2}", envioPendente.OsId, envioPendente.RecebedorId, envioPendente.RecebedorId), EventLogEntryType.Information, _source, _logName);
                    return;
                }

                var body = this.GerarHtmlEmail(documento, envioPendente);

                var remetente = "noreply@rumolog.com";
                var remetenteConf = _confGeralRepository.ObterPorId("GERENCIAMENTO_DOCUMENTACAO_EMAIL_REMETENTE");
                if (remetenteConf != null && !string.IsNullOrEmpty(remetenteConf.Valor))
                {
                    remetente = remetenteConf.Valor;
                }

                #endregion Variáveis de configuração de e-mail

                LogHelper.Log(String.Format("Enviando os e-mail ID {0} para {1}.", envioPendente.Id, emailDestinatarios), EventLogEntryType.Information, _source, _logName);

                var subject = string.Empty;

                var recomposicao = envioPendente.Recomposicao == "S";
                var refaturamento = documento.GerenciamentoDocumentacaoRefaturamento != null;

                if (!string.IsNullOrEmpty(body))
                {

                    if (recomposicao)
                    {
                        subject = String.Format("Atualização Documentações Trem {0} - OS {1}", documento.PrefixoTrem, documento.OsNumero);
                    }
                    else if (refaturamento)
                    {
                        subject = String.Format("Refaturamento Vagões - Documentação Trem {0} - OS {1}", documento.PrefixoTrem, documento.OsNumero);
                    }
                    else
                    {
                        subject = String.Format("Documentação Trem {0} - OS {1}", documento.PrefixoTrem, documento.OsNumero);
                    }

                    string id = documento.Id + "." + documento.OsId + "." + documento.OsNumero;

                    string html = "<html><body>" + body + "</body></html>";

                    /*
                    string resultado = KMMEmails.Enviar(id, emailDestinatarios, subject, html);

                    if (!resultado.Contains(">200</code>"))
                    {
                        throw new Exception(string.Format("Erro ao enviar o e-mails ID {0} para {1} à KMM: {2}", envioPendente.Id, emailDestinatarios, resultado));
                    }
                    */

                    var _destinatarios = emailDestinatarios.Trim()
                                .Split(new char[] { ';', ',', ' ' }, StringSplitOptions.RemoveEmptyEntries)
                                .Select(p => p.Trim().ToLower()).Distinct().OrderBy(p => p).ToList();

                    Sispat.EnviarEmail(subject, html, _destinatarios);

                    foreach (var email in destinatarios)
                    {
                        email.EnviadoEm = DateTime.Now;
                        email.Enviado = "S";
                    }
                }
                envioPendente.Enviado = "S";
                _historicoEnvioRepository.Atualizar(envioPendente);

                Sis.LogTrace("E-mail enviado com sucesso.");
            }
            catch (SmtpFailedRecipientsException e)
            {
                ex = new Exception("Falha no envio de e-mail: SmtpFailedRecipientsException", e);
                Sis.LogException(ex);
            }
            catch (SmtpFailedRecipientException e)
            {
                ex = new Exception("Falha no envio de e-mail: SmtpFailedRecipientException", e);
                Sis.LogException(ex);
            }
            catch (SmtpException e)
            {
                ex = new Exception("Falha no envio de e-mail: SmtpException", e);
                Sis.LogException(ex);
            }
            catch (Exception e)
            {
                ex = new Exception("Falha no envio de e-mail: Exception", e);
                Sis.LogException(ex);
            }

            if (ex != null && ex.Message.Contains("The underlying connection was closed"))
            {
                throw ex;
            }
        }

        public string GerarHtmlEmail(GerenciamentoDocumentacao documento, HistoricoEnvioDocumentacao envioPendente)
        {
            var urlLogo = _confGeralRepository.ObterPorId("GERENCIAMENTO_DOCUMENTACAO_EMAIL_URL_LOGO");
            var chaveAcesso = documento.ChaveAcesso;
            var diasValidade = int.Parse(documento.DiasValidade.ToString());
            var dataCriacaoDocumentacao = documento.CriadoEm;
            // TODO: Alteração Israel
            var linkUrlDownlodDocumentacao = _confGeralRepository.ObterPorId("GERENCIAMENTO_DOWNLOAD_DOCUMENTACAO_EMAIL").Valor;
            var recomposicao = envioPendente.Recomposicao == "S";
            var refaturamento = documento.GerenciamentoDocumentacaoRefaturamento != null;
            var chegada = envioPendente.Chegada == "S";

            var html = string.Empty;

            #region Recomposição
            if (recomposicao)
            {
                var htmlVagoesAdicionados = string.Empty;
                var htmlVagoesRetirados = string.Empty;

                var composicaoAtualId = envioPendente.ComposicaoId;
                var composicao = _composicaoRepository.ObterDadosComposicao(int.Parse(composicaoAtualId.ToString()));
                if (composicao != null)
                {
                    try
                    {
                        var vagoesEnvioAtual = _composicaoVagaoRepository.ObterPorComposicao(composicao)
                            .Select(comp => comp.Vagao)
                            .ToList();

                        var composicoesAnteriores =
                            documento.GerenciamentoDocumentacaoCompVagao.Select(comp => comp.ComposicaoId)
                                .Where(comp => comp < composicaoAtualId)
                                .Distinct()
                                .ToList();

                        decimal? composicaoAnteriorId = null;
                        if (composicoesAnteriores.Any())
                        {
                            composicaoAnteriorId = composicoesAnteriores.Max(ca => ca);
                        }

                        var vagoesEnvioAnterior = new List<Vagao>();
                        if (composicaoAnteriorId.HasValue)
                        {
                            var composicaoAnterior =
                                _composicaoRepository.ObterDadosComposicao(int.Parse(composicaoAnteriorId.ToString()));
                            vagoesEnvioAnterior = _composicaoVagaoRepository.ObterPorComposicao(composicaoAnterior)
                                .Select(comp => comp.Vagao)
                                .ToList();
                        }

                        var vagoesAdicionados =
                            vagoesEnvioAtual.Where(atual => !vagoesEnvioAnterior.Any(anterior => anterior.Id == atual.Id)).
                                ToList();
                        var vagoesRetirados =
                            vagoesEnvioAnterior.Where(anterior => !vagoesEnvioAtual.Any(atual => atual.Id == anterior.Id)).
                                ToList();

                        #region HTML - Vagões Adicionados

                        htmlVagoesAdicionados =
                            @"
                            <div style='margin:7px; float: left; width: 45%; border-radius: 12px;  background-color: #D5FFB5'>
								<div style='margin:0; text-align: center;'>
                                  <font face='Calibri,sans-serif' size='2'>
                                     <h4>Vagões Adicionados</h4>
                                  </font>
								</div>
								<font face='Calibri,sans-serif' size='2'>
									<ol>";

                        if (vagoesAdicionados.Any())
                        {
                            var codigoVagoesAdicionados =
                                vagoesAdicionados.OrderBy(va => va.Serie.Codigo).ThenBy(va => va.Codigo).ToList();
                            foreach (var vagaoAdicionado in codigoVagoesAdicionados)
                            {
                                htmlVagoesAdicionados +=
                                    string.Format(
                                        @"
                                        <li>
											<span style='font-size:10pt;'>{0} {1}</span>
										</li>",
                                        vagaoAdicionado.Serie.Codigo, vagaoAdicionado.Codigo);
                            }
                        }
                        else
                        {
                            htmlVagoesAdicionados +=
                                @"
                                        <li>
											<span style='font-size:10pt;'>Nenhum vagão adicionado.</span>
										</li>";
                        }

                        htmlVagoesAdicionados +=
                            @"
                                    </ol>
								</font>
							</div>";

                        #endregion HTML - Vagões Adicionados

                        #region HTML - Vagões Retirados

                        htmlVagoesRetirados =
                            @"
                            <div style='margin:7px; float: left; width: 45%; border-radius: 12px;  background-color: #FF9D7C'>
								<div style='margin:0; text-align: center;'>
                                  <font face='Calibri,sans-serif' size='2'>
                                     <h4>Vagões Removidos</h4>
                                  </font>
								</div>
								<font face='Calibri,sans-serif' size='2'>
									<ol>";

                        if (vagoesRetirados.Any())
                        {
                            var codigoVagoesRetirados =
                                vagoesRetirados.OrderBy(vr => vr.Serie.Codigo).ThenBy(vr => vr.Codigo).ToList();
                            foreach (var vagaoRetirado in codigoVagoesRetirados)
                            {
                                htmlVagoesRetirados +=
                                    string.Format(
                                        @"
                                        <li>
											<span style='font-size:10pt;'>{0} {1}</span>
										</li>",
                                        vagaoRetirado.Serie.Codigo, vagaoRetirado.Codigo);
                            }
                        }
                        else
                        {
                            htmlVagoesRetirados +=
                                @"
                                        <li>
											<span style='font-size:10pt;'>Nenhum vagão removido.</span>
										</li>";
                        }

                        htmlVagoesRetirados +=
                            @"
                                    </ol>
								</font>
							</div>";

                        //// verifica se mantem o html q ele montou ou se limpa para não mandar errado, caso tenha sido feito a recomposição apenas das locos
                        if (!vagoesAdicionados.Any() && !vagoesRetirados.Any())
                        {
                            return string.Empty;
                        }

                        #endregion HTML - Vagões Retirados
                    }
                    catch (Exception)
                    {
                    }
                }

                html = string.Format(@"
                <div align='center' style='text-align:center;'>
                   <table width='1034' border='0' cellspacing='0' cellpadding='0' style='border-collapse:collapse;width:620.9pt;'>
                      <tbody>
                         <tr height='126' style='height:75.95pt;'>
                            <td width='1034' valign='top' style='width:620.9pt;height:75.95pt;padding:28.34pt 14.2pt 14.2pt 14.2pt;'>
                               <div style='margin:0;'>
                                  <font face='Calibri,sans-serif' size='2'>
                                     <span style='font-size:11pt;'>
                                        <img width='790' height='92' style='display: inline;' src='{0}' />
                                     </span>
                                  </font>
                               </div>
                               <div style='margin:0;'>
                                  <font face='Calibri,sans-serif' size='2'>
                                     <span style='font-size:11pt;'>
                                        <font face='Arial,sans-serif' size='4'>
                                           <span style='font-size:16pt;'>
                                           <b>&nbsp;</b>
                                           </span>
                                        </font>
                                     </span>
                                  </font>
                               </div>
                               <div style='margin:0;'>
                                  <font face='Calibri,sans-serif' size='2'>
                                     <span style='font-size:11pt;'>
                                        <font face='Arial,sans-serif' size='4' color='black'>
                                           <span style='font-size:16pt;'>
                                              <b>Atualização das Documentações Trem {4} - OS {1}&nbsp;</b>
                                           </span>
                                        </font>
                                     </span>
                                  </font>
                               </div>
                               <div style='margin:0;'>
                                  <font face='Calibri,sans-serif' size='2'>
                                     <span style='font-size:11pt;'>&nbsp;</span>
                                  </font>
                               </div>
                               <div style='margin:0;'>
                                  <font face='Calibri,sans-serif' size='2'>
                                     <span style='font-size:11pt;'>
                                        Prezados,
                                     </span>
                                  </font>
                               </div>
                               <div style='margin:0;'>
                                  <font face='Calibri,sans-serif' size='2'>
                                     <span style='font-size:11pt;'>
                                        &nbsp;
                                     </span>
                                  </font>
                               </div>
                               <div style='margin:0;'>
                                  <font face='Calibri,sans-serif' size='2'>
                                     <span style='font-size:11pt;'>
                                        Relizamos uma atualização dos documentos do trem cuja OS é {1}.
                                        Por favor, atualize os documentos enviados anteriormente.
                                        Os documentos ficarão disponíveis até o dia {2}.
                                     </span>
                                  </font>
                               </div>
                               <div style='margin:0;'>
                                  <font face='Calibri,sans-serif' size='2'>
                                     <span style='font-size:11pt;'>
                                        &nbsp;
                                     </span>
                                  </font>
                               </div>
                               <div style='margin:0;'>
                                  <font face='Calibri,sans-serif' size='2'>
                                     <span style='font-size:11pt;'>Clique <a href='{7}?fileName={3}'>aqui</a> para realizar o download da documentação.</span>
                                  </font>
                               </div>
                               <div style='margin:0;'>
                                  <font face='Calibri,sans-serif' size='2'>
                                     <span style='font-size:11pt;'>
                                        &nbsp;
                                     </span>
                                  </font>
                               </div>
                               <div style='margin-left: 30px; overflow: auto;'>
                                    {5}
                                    {6}
                               </div>
                               <div style='margin:0;'>
                                  <font face='Calibri,sans-serif' size='2'>
                                     <span style='font-size:11pt;'>
                                        &nbsp;
                                     </span>
                                  </font>
                               </div>
                               <div style='margin:0;'>
                                  <font face='Calibri,sans-serif' size='2'>
                                     <span style='font-size:11pt;'>Por favor, não responda este e-mail.</span>
                                  </font>
                               </div>
                               <div style='margin:0;'>
                                  <font face='Calibri,sans-serif' size='2'>
                                     <span style='font-size:11pt;'>&nbsp;</span>
                                  </font>
                               </div>
                               <div style='margin:0;'>
                                  <font face='Calibri,sans-serif' size='2'>
                                     <span style='font-size:11pt;'>
                                        <font face='Arial,sans-serif' size='4'>
                                           <span style='font-size:16pt;'>
                                              <b>
                                                 &nbsp;
                                              </b>
                                           </span>
                                        </font>
                                     </span>
                                  </font>
                               </div>
                            </td>
                         </tr>
                      </tbody>
                   </table>
                </div>",
                                         urlLogo != null ? urlLogo.Valor : string.Empty,
                                         documento.OsNumero,
                                         dataCriacaoDocumentacao.HasValue
                                             ? dataCriacaoDocumentacao.Value.AddDays(diasValidade).ToString("dd/MM/yyyy")
                                             : DateTime.Today.AddDays(diasValidade).ToString("dd/MM/yyyy"),
                                         chaveAcesso,
                                         documento.PrefixoTrem,
                                         htmlVagoesRetirados,
                                         htmlVagoesAdicionados,
                                         linkUrlDownlodDocumentacao);

            }

            #endregion Recomposição

            #region Refaturamento

            else if (refaturamento)
            {
                var descricaoMotivo = documento.GerenciamentoDocumentacaoRefaturamento.MotivoRefaturamento.Motivo;
                html = string.Format(@"
                <div align='center' style='text-align:center;'>
                   <table width='1034' border='0' cellspacing='0' cellpadding='0' style='border-collapse:collapse;width:620.9pt;'>
                      <tbody>
                         <tr height='126' style='height:75.95pt;'>
                            <td width='1034' valign='top' style='width:620.9pt;height:75.95pt;padding:28.34pt 14.2pt 14.2pt 14.2pt;'>
                               <div style='margin:0;'>
                                  <font face='Calibri,sans-serif' size='2'>
                                     <span style='font-size:11pt;'>
                                        <img width='790' height='92' style='display: inline;' src='{0}' />
                                     </span>
                                  </font>
                               </div>
                               <div style='margin:0;'>
                                  <font face='Calibri,sans-serif' size='2'>
                                     <span style='font-size:11pt;'>
                                        <font face='Arial,sans-serif' size='4'>
                                           <span style='font-size:16pt;'>
                                           <b>&nbsp;</b>
                                           </span>
                                        </font>
                                     </span>
                                  </font>
                               </div>
                               <div style='margin:0;'>
                                  <font face='Calibri,sans-serif' size='2'>
                                     <span style='font-size:11pt;'>
                                        <font face='Arial,sans-serif' size='4' color='black'>
                                           <span style='font-size:16pt;'>
                                              <b>Operação Refaturamento Trem {4} - OS {1}&nbsp;</b>
                                           </span>
                                        </font>
                                     </span>
                                  </font>
                               </div>
                               <div style='margin:0;'>
                                  <font face='Calibri,sans-serif' size='2'>
                                     <span style='font-size:11pt;'>&nbsp;</span>
                                  </font>
                               </div>
                               <div style='margin:0;'>
                                  <font face='Calibri,sans-serif' size='2'>
                                     <span style='font-size:11pt;'>
                                        Prezados,
                                     </span>
                                  </font>
                               </div>
                               <div style='margin:0;'>
                                  <font face='Calibri,sans-serif' size='2'>
                                     <span style='font-size:11pt;'>
                                        &nbsp;
                                     </span>
                                  </font>
                               </div>
                               <div style='margin:0;'>
                                  <font face='Calibri,sans-serif' size='2'>
                                     <span style='font-size:11pt;'>
                                        Estamos enviando o link para o download da documentação de refaturamento do trem cuja OS é {1}.
                                        Os documentos ficarão disponíveis até o dia {2}.
                                     </span>
                                  </font>
                               </div>
                               <div style='margin:0;'>
                                  <font face='Calibri,sans-serif' size='2'>
                                     <span style='font-size:11pt;'>
                                        &nbsp;
                                     </span>
                                  </font>
                               </div>
                               <div style='margin:0;'>
                                  <font face='Calibri,sans-serif' size='2'>
                                     <span style='font-size:13pt;'>
                                        <b>Motivo Refaturamento: {5}.</b>
                                     </span>
                                  </font>
                               </div>
                               <div style='margin:0;'>
                                  <font face='Calibri,sans-serif' size='2'>
                                     <span style='font-size:11pt;'>
                                        &nbsp;
                                     </span>
                                  </font>
                               </div>
                               <div style='margin:0;'>
                                  <font face='Calibri,sans-serif' size='2'>
                                     <span style='font-size:11pt;'>Clique <a href='{6}?fileName={3}'>aqui</a> para realizar o download da documentação.</span>
                                  </font>
                               </div>
                               <div style='margin:0;'>
                                  <font face='Calibri,sans-serif' size='2'>
                                     <span style='font-size:11pt;'>
                                        &nbsp;
                                     </span>
                                  </font>
                               </div>
                               <div style='margin:0;'>
                                  <font face='Calibri,sans-serif' size='2'>
                                     <span style='font-size:11pt;'>Por favor, não responda este e-mail.</span>
                                  </font>
                               </div>
                               <div style='margin:0;'>
                                  <font face='Calibri,sans-serif' size='2'>
                                     <span style='font-size:11pt;'>&nbsp;</span>
                                  </font>
                               </div>
                               <div style='margin:0;'>
                                  <font face='Calibri,sans-serif' size='2'>
                                     <span style='font-size:11pt;'>
                                        <font face='Arial,sans-serif' size='4'>
                                           <span style='font-size:16pt;'>
                                              <b>
                                                 &nbsp;
                                              </b>
                                           </span>
                                        </font>
                                     </span>
                                  </font>
                               </div>
                            </td>
                         </tr>
                      </tbody>
                   </table>
                </div>",
                                         urlLogo != null ? urlLogo.Valor : string.Empty,
                                         documento.OsNumero,
                                         dataCriacaoDocumentacao.HasValue
                                             ? dataCriacaoDocumentacao.Value.AddDays(diasValidade).ToString("dd/MM/yyyy")
                                             : DateTime.Today.AddDays(diasValidade).ToString("dd/MM/yyyy"),
                                         chaveAcesso,
                                         documento.PrefixoTrem,
                                         descricaoMotivo,
                                         linkUrlDownlodDocumentacao);
            }

            #endregion Refaturamento

            #region Normal

            else
            {
                if (chegada)
                {
                    html = string.Format(@"
                <div align='center' style='text-align:center;'>
                   <table width='1034' border='0' cellspacing='0' cellpadding='0' style='border-collapse:collapse;width:620.9pt;'>
                      <tbody>
                         <tr height='126' style='height:75.95pt;'>
                            <td width='1034' valign='top' style='width:620.9pt;height:75.95pt;padding:28.34pt 14.2pt 14.2pt 14.2pt;'>
                               <div style='margin:0;'>
                                  <font face='Calibri,sans-serif' size='2'>
                                     <span style='font-size:11pt;'>
                                        <img width='790' height='92' style='display: inline;' src='{0}' />
                                     </span>
                                  </font>
                               </div>
                               <div style='margin:0;'>
                                  <font face='Calibri,sans-serif' size='2'>
                                     <span style='font-size:11pt;'>
                                        <font face='Arial,sans-serif' size='4'>
                                           <span style='font-size:16pt;'>
                                           <b>&nbsp;</b>
                                           </span>
                                        </font>
                                     </span>
                                  </font>
                               </div>
                               <div style='margin:0;'>
                                  <font face='Calibri,sans-serif' size='2'>
                                     <span style='font-size:11pt;'>
                                        <font face='Arial,sans-serif' size='4' color='black'>
                                           <span style='font-size:16pt;'>
                                              <b>Chegada Trem {4} - OS {1}&nbsp;</b>
                                           </span>
                                        </font>
                                     </span>
                                  </font>
                               </div>
                               <div style='margin:0;'>
                                  <font face='Calibri,sans-serif' size='2'>
                                     <span style='font-size:11pt;'>&nbsp;</span>
                                  </font>
                               </div>
                               <div style='margin:0;'>
                                  <font face='Calibri,sans-serif' size='2'>
                                     <span style='font-size:11pt;'>
                                        Prezados,
                                     </span>
                                  </font>
                               </div>
                               <div style='margin:0;'>
                                  <font face='Calibri,sans-serif' size='2'>
                                     <span style='font-size:11pt;'>
                                        &nbsp;
                                     </span>
                                  </font>
                               </div>
                               <div style='margin:0;'>
                                  <font face='Calibri,sans-serif' size='2'>
                                     <span style='font-size:11pt;'>
                                        Informamos que o trem cuja OS é {1} chegou no pátio às {5}.
                                        Sua documentação está no link abaixo e ficará disponível até o dia {2}.
                                     </span>
                                  </font>
                               </div>
                               <div style='margin:0;'>
                                  <font face='Calibri,sans-serif' size='2'>
                                     <span style='font-size:11pt;'>
                                        &nbsp;
                                     </span>
                                  </font>
                               </div>
                               <div style='margin:0;'>
                                  <font face='Calibri,sans-serif' size='2'>
                                     <span style='font-size:11pt;'>Clique <a href='{6}?fileName={3}'>aqui</a> para realizar o download da documentação.</span>
                                  </font>
                               </div>
                               <div style='margin:0;'>
                                  <font face='Calibri,sans-serif' size='2'>
                                     <span style='font-size:11pt;'>
                                        &nbsp;
                                     </span>
                                  </font>
                               </div>
                               <div style='margin:0;'>
                                  <font face='Calibri,sans-serif' size='2'>
                                     <span style='font-size:11pt;'>Por favor, não responda este e-mail.</span>
                                  </font>
                               </div>
                               <div style='margin:0;'>
                                  <font face='Calibri,sans-serif' size='2'>
                                     <span style='font-size:11pt;'>&nbsp;</span>
                                  </font>
                               </div>
                               <div style='margin:0;'>
                                  <font face='Calibri,sans-serif' size='2'>
                                     <span style='font-size:11pt;'>
                                        <font face='Arial,sans-serif' size='4'>
                                           <span style='font-size:16pt;'>
                                              <b>
                                                 &nbsp;
                                              </b>
                                           </span>
                                        </font>
                                     </span>
                                  </font>
                               </div>
                            </td>
                         </tr>
                      </tbody>
                   </table>
                </div>",
                                         urlLogo != null ? urlLogo.Valor : string.Empty,
                                         documento.OsNumero,
                                         dataCriacaoDocumentacao.HasValue
                                             ? dataCriacaoDocumentacao.Value.AddDays(diasValidade).ToString("dd/MM/yyyy")
                                             : DateTime.Today.AddDays(diasValidade).ToString("dd/MM/yyyy"),
                                         chaveAcesso,
                                         documento.PrefixoTrem,
                                         envioPendente.DataChegada.Value.ToString("dd/MM/yyyy HH24:mm:ss"),
                                         linkUrlDownlodDocumentacao);
                }
                else
                {
                    html = string.Format(@"
                <div align='center' style='text-align:center;'>
                   <table width='1034' border='0' cellspacing='0' cellpadding='0' style='border-collapse:collapse;width:620.9pt;'>
                      <tbody>
                         <tr height='126' style='height:75.95pt;'>
                            <td width='1034' valign='top' style='width:620.9pt;height:75.95pt;padding:28.34pt 14.2pt 14.2pt 14.2pt;'>
                               <div style='margin:0;'>
                                  <font face='Calibri,sans-serif' size='2'>
                                     <span style='font-size:11pt;'>
                                        <img width='790' height='92' style='display: inline;' src='{0}' />
                                     </span>
                                  </font>
                               </div>
                               <div style='margin:0;'>
                                  <font face='Calibri,sans-serif' size='2'>
                                     <span style='font-size:11pt;'>
                                        <font face='Arial,sans-serif' size='4'>
                                           <span style='font-size:16pt;'>
                                           <b>&nbsp;</b>
                                           </span>
                                        </font>
                                     </span>
                                  </font>
                               </div>
                               <div style='margin:0;'>
                                  <font face='Calibri,sans-serif' size='2'>
                                     <span style='font-size:11pt;'>
                                        <font face='Arial,sans-serif' size='4' color='black'>
                                           <span style='font-size:16pt;'>
                                              <b>Documentações Trem {4} - OS {1}&nbsp;</b>
                                           </span>
                                        </font>
                                     </span>
                                  </font>
                               </div>
                               <div style='margin:0;'>
                                  <font face='Calibri,sans-serif' size='2'>
                                     <span style='font-size:11pt;'>&nbsp;</span>
                                  </font>
                               </div>
                               <div style='margin:0;'>
                                  <font face='Calibri,sans-serif' size='2'>
                                     <span style='font-size:11pt;'>
                                        Prezados,
                                     </span>
                                  </font>
                               </div>
                               <div style='margin:0;'>
                                  <font face='Calibri,sans-serif' size='2'>
                                     <span style='font-size:11pt;'>
                                        &nbsp;
                                     </span>
                                  </font>
                               </div>
                               <div style='margin:0;'>
                                  <font face='Calibri,sans-serif' size='2'>
                                     <span style='font-size:11pt;'>
                                        Estamos enviando o link para o download da documentação do trem cuja OS é {1}. 
                                        Os documentos ficarão disponíveis até o dia {2}.
                                     </span>
                                  </font>
                               </div>
                               <div style='margin:0;'>
                                  <font face='Calibri,sans-serif' size='2'>
                                     <span style='font-size:11pt;'>
                                        &nbsp;
                                     </span>
                                  </font>
                               </div>
                               <div style='margin:0;'>
                                  <font face='Calibri,sans-serif' size='2'>
                                     <span style='font-size:11pt;'>Clique <a href='{5}?fileName={3}'>aqui</a> para realizar o download da documentação.</span>
                                  </font>
                               </div>
                               <div style='margin:0;'>
                                  <font face='Calibri,sans-serif' size='2'>
                                     <span style='font-size:11pt;'>
                                        &nbsp;
                                     </span>
                                  </font>
                               </div>
                               <div style='margin:0;'>
                                  <font face='Calibri,sans-serif' size='2'>
                                     <span style='font-size:11pt;'>Por favor, não responda este e-mail.</span>
                                  </font>
                               </div>
                               <div style='margin:0;'>
                                  <font face='Calibri,sans-serif' size='2'>
                                     <span style='font-size:11pt;'>&nbsp;</span>
                                  </font>
                               </div>
                               <div style='margin:0;'>
                                  <font face='Calibri,sans-serif' size='2'>
                                     <span style='font-size:11pt;'>
                                        <font face='Arial,sans-serif' size='4'>
                                           <span style='font-size:16pt;'>
                                              <b>
                                                 &nbsp;
                                              </b>
                                           </span>
                                        </font>
                                     </span>
                                  </font>
                               </div>
                            </td>
                         </tr>
                      </tbody>
                   </table>
                </div>",
                                         urlLogo != null ? urlLogo.Valor : string.Empty,
                                         documento.OsNumero,
                                         dataCriacaoDocumentacao.HasValue
                                             ? dataCriacaoDocumentacao.Value.AddDays(diasValidade).ToString("dd/MM/yyyy")
                                             : DateTime.Today.AddDays(diasValidade).ToString("dd/MM/yyyy"),
                                         chaveAcesso,
                                         documento.PrefixoTrem,
                                         linkUrlDownlodDocumentacao);
                }
            }

            #endregion Normal

            return html;
        }

        /// <summary>
        /// Deleta as documentações solicitadas no FileShare (diretório)
        /// </summary>
        /// <param name="documentacoes">A coleção de documentações solicitadas para deletar</param>
        public void DeletarDocumentacoes(List<Spd.GerenciamentoDoc> documentacoes)
        {
            using (var db = Sys.NewDb())
            {
                foreach (var documentacao in documentacoes)
                {
                    //LogHelper.Log(string.Format("Deletando a documentação cuja chave é {0} e URL é {1}.",
                    //                            documentacao.ChaveAcesso,
                    //                            documentacao.UrlAcesso),
                    //              EventLogEntryType.Information,
                    //              _source,
                    //              _logName);

#if !DEBUG
                    this.DeletarDiretorioTemporario(documentacao.UrlAcesso);
#endif

                    documentacao.ExcluidoEm = DateTime.Now;
                    BL_GerenciamentoDoc.Update(db, documentacao);
                }
            }
        }

        /// <summary>
        /// Cria um diretório temporário para guardar os pdfs criados
        /// </summary>
        /// <returns>Retorna a url do diretório temporário</returns>
        public string CriarDiretorioTemporario(string os, string recebedor, bool refaturamento)
        {
            LogHelper.Log(String.Format("Criando o diretório temporário."), EventLogEntryType.Information, _source, _logName);

            var urlFileShare = CacheManager.ConfigGeral.Value.GerenciamentoDocumentacaoUrl ?? string.Empty;
#if DEBUG
            urlFileShare = @"C:\temp\Documentacoes";
#endif

            var directoryName = string.Empty;
            if (refaturamento)
            {
                directoryName = string.Format("Refat_OS_{0}_Recebedor_{1}", os, recebedor);
            }
            else
            {
                directoryName = string.Format("OS_{0}_Recebedor_{1}", os, recebedor);
            }

            var url = Path.Combine(string.IsNullOrEmpty(urlFileShare) ? Path.GetTempPath() : urlFileShare, directoryName);
            Directory.CreateDirectory(url);

            LogHelper.Log(String.Format("Diretório criado com sucesso na pasta :: {0}", url), EventLogEntryType.Information, _source, _logName);

            return url;
        }

        /// <summary>
        /// Cria um diretório temporário para guardar os pdfs criados
        /// </summary>
        /// <returns>Retorna a url do diretório temporário</returns>
        public string CriarDiretorioTemporario()
        {
            var url = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
            Directory.CreateDirectory(url);

            return url;
        }

        /// <summary>
        /// Deleta um diretório temporário
        /// </summary>
        /// <param name="urlBase">A url a ser deletada</param>
        public void DeletarDiretorioTemporario(string urlBase)
        {
            try
            {
                // Se não for informado a urlBase, devemos sair do método
                // Esse if é só por precausão para desenvolvimentos futuros, atualmente a variável urlBase sempre terá valor
                if (string.IsNullOrEmpty(urlBase))
                {
                    return;
                }

                // Se o diretório não existir, devemos sair do método
                if (!Directory.Exists(urlBase))
                {
                    return;
                }

                var directoryInfo = new DirectoryInfo(urlBase);

                // Deletando todos os arquivos do diretório
                foreach (var file in directoryInfo.GetFiles())
                {
                    file.Delete();
                }

                // Deletando todos os diretórios filhos
                foreach (var dir in directoryInfo.GetDirectories())
                {
                    dir.Delete(true);
                }

                // Deletando o diretório solicitado
                Directory.Delete(urlBase);
            }
            catch { }
        }

        public byte[] ToByteArray(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (var ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }

                return ms.ToArray();
            }
        }

        /// <summary>
        /// Para DEBUG não é necessário realizar o login pelo código, deve-se primeiramente realizar o login atraves do WindowsExplorer, ou seja,
        /// acesse a pasta configurada na tabela CONF_GERAL pela chave GERENCIAMENTO_DOCUMENTACAO_URL utilizando o dominio\login e senha encontrados na mesma 
        /// tabela, colunas GERENCIAMENTO_DOCUMENTACAO_URL_USUARIO e GERENCIAMENTO_DOCUMENTACAO_URL_SENHA respectivamente. Depois de acessado, execute 
        /// o Debug normalmente.
        /// </summary>
        /// Em Produção (RELEASE) é necessário o uso do Impersonator para realizar o login na pasta
        /// <param name="action">Ação á ser executada</param>
        private void Impersonate(Action action)
        {
#if DEBUG
            action();
            return;
#endif

            var username = CacheManager.ConfigGeral.Value.GerenciamentoDocumentacaoUrlUsuario;
            if (string.IsNullOrEmpty(username))
            {
                throw new Exception("Usuário não configurado na CONF_GERAL. Chave :: GERENCIAMENTO_DOCUMENTACAO_URL_USUARIO");
            }

            var password = CacheManager.ConfigGeral.Value.GerenciamentoDocumentacaoUrlSenha;
            if (string.IsNullOrEmpty(password))
            {
                throw new Exception("Senha de usuário não configurado na CONF_GERAL. Chave :: GERENCIAMENTO_DOCUMENTACAO_URL_SENHA");
            }

            var dominio = CacheManager.ConfigGeral.Value.GerenciamentoDocumentacaoUrlDominio;
            if (string.IsNullOrEmpty(dominio))
            {
                throw new Exception("Dominio do usuário não configurado na CONF_GERAL. Chave :: GERENCIAMENTO_DOCUMENTACAO_URL_DOMINIO");
            }

            using (var impersonator = new Impersonator(username, password, dominio))
            {
                action();
            }
        }
    }
}