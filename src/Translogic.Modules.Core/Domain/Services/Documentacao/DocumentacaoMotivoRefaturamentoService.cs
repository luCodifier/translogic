﻿namespace Translogic.Modules.Core.Domain.Services.Documentacao
{
    using System.Collections.Generic;
    using System.Linq;
    using Translogic.Modules.Core.Domain.Model.Documentacao;
    using Translogic.Modules.Core.Domain.Model.Documentacao.Repositories;

    public class DocumentacaoMotivoRefaturamentoService
    {
        private readonly IDocumentacaoMotivoRefaturamentoRepository _documentacaoMotivoRefaturamentoRepository;

        public DocumentacaoMotivoRefaturamentoService(IDocumentacaoMotivoRefaturamentoRepository documentacaoMotivoRefaturamentoRepository)
        {
            _documentacaoMotivoRefaturamentoRepository = documentacaoMotivoRefaturamentoRepository;
        }

        public ICollection<DocumentacaoMotivoRefaturamento> ObterAtivos()
        {
            var registros = _documentacaoMotivoRefaturamentoRepository.ObterAtivos();
            return registros.OrderBy(reg => reg.Motivo).ToList();
        }
    }
}