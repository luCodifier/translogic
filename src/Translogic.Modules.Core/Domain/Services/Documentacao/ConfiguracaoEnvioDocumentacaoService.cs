﻿namespace Translogic.Modules.Core.Domain.Services.Documentacao
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text.RegularExpressions;
    using ALL.Core.Dominio;
    using Translogic.Core.Infrastructure.Web;
    using Translogic.Modules.Core.Domain.Model.Codificador.Repositories;
    using Translogic.Modules.Core.Domain.Model.Documentacao;
    using Translogic.Modules.Core.Domain.Model.Documentacao.Repositories;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Modules.Core.Domain.Model.Estrutura;
    using Translogic.Modules.Core.Domain.Model.Estrutura.Repositories;
    using Translogic.Modules.Core.Domain.Model.Via.Repositories;

    public class ConfiguracaoEnvioDocumentacaoService
    {
        private readonly IConfiguracaoEnvioDocumentacaoRepository _configuracaoEnvioDocumentacaoRepository;
        private readonly IEmailEnvioDocumentacaoRepository _emailDocumentacaoRepository;
        private readonly IAreaOperacionalRepository _areaOperacionalRepository;
        private readonly IEmpresaClienteRepository _empresaRepository;
        private readonly IConfiguracaoTranslogicRepository _confGeralRepository;
        private readonly IEmpresaClienteRepository _empresaClienteRepository;

        /// <summary>
        /// Construtor da classe
        /// </summary>
        /// <param name="configuracaoEnvioDocumentacaoRepository"></param>
        /// <param name="emailDocumentacaoRepository"></param>
        /// <param name="areaOperacionalRepository"></param>
        /// <param name="confGeralRepository"></param>
        /// <param name="empresaRepository"></param>
        /// <param name="empresaClienteRepository"></param>
        public ConfiguracaoEnvioDocumentacaoService(
            IConfiguracaoEnvioDocumentacaoRepository configuracaoEnvioDocumentacaoRepository,
            IEmailEnvioDocumentacaoRepository emailDocumentacaoRepository,
            IAreaOperacionalRepository areaOperacionalRepository,
            IEmpresaClienteRepository empresaRepository,
            IConfiguracaoTranslogicRepository confGeralRepository,
            IEmpresaClienteRepository empresaClienteRepository)
        {
            _configuracaoEnvioDocumentacaoRepository = configuracaoEnvioDocumentacaoRepository;
            _emailDocumentacaoRepository = emailDocumentacaoRepository;
            _areaOperacionalRepository = areaOperacionalRepository;
            _empresaRepository = empresaRepository;
            _confGeralRepository = confGeralRepository;
            _empresaClienteRepository = empresaClienteRepository;
        }

        /// <summary>
        /// Insere uma configuração passando como parâmetro o código da área operacional e o ID do terminal de recebimento
        /// </summary>
        /// <param name="areaOperacionalEnvio">Código da área operacional</param>
        /// <param name="terminalRecebimentoId">ID do terminal de recebimento</param>
        /// <param name="gerarRelatorioLaudoMercadoria">Indicador se deve-se gerar o relatório de laudo de mercadorias</param>
        /// <param name="enviarChegada">Informa se deve enviar os e-mails de chegada para o terminal de destino</param>
        /// <param name="enviarSaida">Informa se deve enviar os e-mails de saída para o terminal de destino</param>
        /// <returns></returns>
        public ConfiguracaoEnvioDocumentacao Inserir(string areaOperacionalEnvio, int? terminalRecebimentoId,
            bool gerarRelatorioLaudoMercadoria, bool enviarChegada, bool enviarSaida)
        {
            #region Validações

            if (string.IsNullOrEmpty(areaOperacionalEnvio))
            {
                throw new ArgumentException("Informe um código de área operacional.");
            }

            if (!terminalRecebimentoId.HasValue)
            {
                throw new ArgumentException("Informe um terminal para receber a documentação.");
            }

            areaOperacionalEnvio = areaOperacionalEnvio.ToUpper();
            var areaOperacional = _areaOperacionalRepository.ObterPorCodigo(areaOperacionalEnvio);
            if (areaOperacional == null)
            {
                throw new Exception(String.Format("Área Operacional {0} não encontrada.", areaOperacionalEnvio));
            }

            var empresa = _empresaRepository.ObterPorId(terminalRecebimentoId);
            if (empresa == null)
            {
                throw new Exception("Terminal não encontrado.");
            }

            var registroEncontrado = _configuracaoEnvioDocumentacaoRepository.ObterPorParametros(areaOperacionalEnvio, terminalRecebimentoId.Value);
            if (registroEncontrado != null)
            {
                throw new Exception("A configuração foi inserida anteriormente.");
            }

            var diasValidade = 7;
            // Obtendo da configuração geral o valor parametrizado
            var diasValidadeDocumentoParametrizado = _confGeralRepository.ObterPorId("GERENCIAMENTO_DOCUMENTACAO_DIAS_VALIDADE");
            if (diasValidadeDocumentoParametrizado != null)
            {
                int diasValidadeConf;
                if (Int32.TryParse(diasValidadeDocumentoParametrizado.Valor, out diasValidadeConf))
                {
                    diasValidade = diasValidadeConf > 0 ? diasValidadeConf : 7;
                }
            }

            #endregion Validações

            var registro = new ConfiguracaoEnvioDocumentacao
            {
                AreaOperacionalEnvio = areaOperacional,
                EmpresaDestino = empresa,
                Ativo = "S",
                GerarRelatorioLaudoMercadoria = gerarRelatorioLaudoMercadoria ? "S" : "N",
                EnviarChegada = enviarChegada ? "S" : "N",
                EnviarSaida = enviarSaida ? "S" : "N",
                CriadoEm = DateTime.Now,
                DiasValidadeDocumentacao = diasValidade
            };

            _configuracaoEnvioDocumentacaoRepository.Inserir(registro);

            return registro;
        }

        /// <summary>
        /// Insere um e-mail na configuração
        /// </summary>
        /// <param name="idConfiguracao">ID da configuração que será incluída o e-mail</param>
        /// <param name="email">O e-mail a ser incluído</param>
        /// <returns></returns>
        public EmailEnvioDocumentacao InserirEmail(int? idConfiguracao, string email)
        {
            #region Validações

            if (string.IsNullOrEmpty(email))
            {
                throw new ArgumentException("Informe um e-mail para ser inserido na configuração.");
            }
            email = email.ToLower();

            var regexEmail = new Regex(@"([a-zA-Z0-9_\-\.]+)@((\[[0\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})");

            if (!regexEmail.Match(email).Success)
            {
                throw new ArgumentException(string.Format("O e-mail {0} é inválido.", email));
            }

            if (!idConfiguracao.HasValue)
            {
                throw new ArgumentException("Configuração não informada para a inclusão do e-mail.");
            }

            var configuracao = _configuracaoEnvioDocumentacaoRepository.ObterPorId(idConfiguracao.Value);
            if (configuracao == null)
            {
                throw new Exception("A configuração não encontrada.");
            }

            if (configuracao.Emails.Any(e => e.Email == email))
            {
                throw new Exception(string.Format("Esta configuração já possui o e-mail {0} cadastrado.", email));
            }

            #endregion Validações

            var registro = new EmailEnvioDocumentacao()
            {
                ConfiguracaoEnvio = configuracao,
                Email = email,
                Ativo = "S",
                CriadoEm = DateTime.Now
            };

            _emailDocumentacaoRepository.Inserir(registro);

            return registro;
        }

        /// <summary>
        /// Ativa ou desativa a configuração informada
        /// </summary>
        /// <param name="id">ID da configuração</param>
        public void HabilitarConfiguracao(int id)
        {
            #region Validações

            if (id <= 0)
            {
                throw new Exception("Configuração não informada.");
            }

            var configuracao = _configuracaoEnvioDocumentacaoRepository.ObterPorId(id);
            if (configuracao == null)
            {
                throw new Exception("Configuração não encontrada.");
            }

            #endregion

            configuracao.Ativo = configuracao.Ativo == "S" ? "N" : "S";

            _configuracaoEnvioDocumentacaoRepository.Atualizar(configuracao);
        }

        /// <summary>
        /// Ativa ou desativa o e-mail da configuração
        /// </summary>
        /// <param name="id">ID do e-mail da configuração</param>
        public void HabilitarEmailConfiguracao(int id)
        {
            #region Validações

            if (id <= 0)
            {
                throw new Exception("E-mail da configuração não informado.");
            }

            var email = _emailDocumentacaoRepository.ObterPorId(id);
            if (email == null)
            {
                throw new Exception("E-mail da configuração não encontrada.");
            }

            #endregion

            email.Ativo = email.Ativo == "S" ? "N" : "S";

            _emailDocumentacaoRepository.Atualizar(email);
        }

        /// <summary>
        /// Deleta a configuração cujo ID é a do parâmetro
        /// </summary>
        /// <param name="id">ID da configuração</param>
        public void Deletar(int id)
        {
            #region Validações

            if (id <= 0)
            {
                throw new Exception("Configuração não informada.");
            }

            var configuracao = _configuracaoEnvioDocumentacaoRepository.ObterPorId(id);
            if (configuracao == null)
            {
                throw new Exception("Configuração não encontrada.");
            }

            #endregion

            _configuracaoEnvioDocumentacaoRepository.Remover(configuracao);
        }

        /// <summary>
        /// Deleta o e-mail da configuração
        /// </summary>
        /// <param name="id">ID do e-mail</param>
        public void DeletarEmail(int id)
        {
            #region Validações

            if (id <= 0)
            {
                throw new Exception("E-mail da configuração não informado.");
            }

            var email = _emailDocumentacaoRepository.ObterPorId(id);
            if (email == null)
            {
                throw new Exception("E-mail da configuração não encontrado.");
            }

            #endregion

            _emailDocumentacaoRepository.Remover(email);
        }

        /// <summary>
        /// Atualiza a configuração
        /// </summary>
        /// <param name="id">ID da configuração</param>
        /// <param name="areaOperacionalEnvio">Nova área operacional de envio</param>
        /// <param name="terminalRecebimentoId">Novo terminal de recebimento</param>
        /// <param name="diasValidade">Quantidade de dias que a documentação ficará vigente</param>
        /// <param name="gerarRelatorioLaudoMercadoria">Informa se deve-se criar o relatório de laudo de mercadoria</param>
        /// <param name="enviarChegada">Informa se deve enviar os e-mails de chegada para o terminal de destino</param>
        /// <param name="enviarSaida">Informa se deve enviar os e-mails de saída para o terminal de destino</param>
        /// <returns></returns>
        public ConfiguracaoEnvioDocumentacao Atualizar(int id, string areaOperacionalEnvio, int? terminalRecebimentoId, int? diasValidade,
            bool gerarRelatorioLaudoMercadoria, bool enviarChegada, bool enviarSaida)
        {
            #region Validações

            if (string.IsNullOrEmpty(areaOperacionalEnvio))
            {
                throw new ArgumentException("Informe um código de área operacional.");
            }

            areaOperacionalEnvio = areaOperacionalEnvio.ToUpper();
            var areaOperacional = _areaOperacionalRepository.ObterPorCodigo(areaOperacionalEnvio);
            if (areaOperacional == null)
            {
                throw new Exception(String.Format("Área Operacional {0} não encontrada.", areaOperacionalEnvio));
            }

            var configuracao = _configuracaoEnvioDocumentacaoRepository.ObterPorId(id);
            if (configuracao == null)
            {
                throw new Exception("A configuração não encontrada.");
            }

            if (!terminalRecebimentoId.HasValue)
            {
                terminalRecebimentoId = configuracao.EmpresaDestino.Id;
            }

            if (!terminalRecebimentoId.HasValue)
            {
                throw new Exception("Terminal de recebimento não encontrado.");
            }

            var empresa = _empresaRepository.ObterPorId(terminalRecebimentoId);
            if (empresa == null)
            {
                throw new Exception("Terminal não encontrado.");
            }

            var registroEncontrado = _configuracaoEnvioDocumentacaoRepository.ObterPorParametros(areaOperacionalEnvio, terminalRecebimentoId.Value);
            if (registroEncontrado != null && registroEncontrado.Id != configuracao.Id)
            {
                throw new Exception("A configuração foi inserida anteriormente.");
            }

            if (!diasValidade.HasValue)
            {
                // Obtendo da configuração geral o valor parametrizado
                var diasValidadeDocumentoParametrizado = _confGeralRepository.ObterPorId("GERENCIAMENTO_DOCUMENTACAO_DIAS_VALIDADE");
                if (diasValidadeDocumentoParametrizado != null)
                {
                    int diasValidadeConf;
                    if (Int32.TryParse(diasValidadeDocumentoParametrizado.Valor, out diasValidadeConf))
                    {
                        diasValidade = diasValidadeConf > 0 ? diasValidadeConf : 7;
                    }
                }
            }

            if (diasValidade <= 0)
            {
                throw new Exception("Dias de validade da documentação deve ser maior que zero.");
            }

            #endregion Validações

            configuracao.AreaOperacionalEnvio = areaOperacional;
            configuracao.EmpresaDestino = empresa;
            configuracao.DiasValidadeDocumentacao = diasValidade;
            configuracao.GerarRelatorioLaudoMercadoria = gerarRelatorioLaudoMercadoria ? "S" : "N";
            configuracao.EnviarChegada = enviarChegada ? "S" : "N";
            configuracao.EnviarSaida = enviarSaida ? "S" : "N";

            _configuracaoEnvioDocumentacaoRepository.Atualizar(configuracao);

            return configuracao;
        }

        /// <summary>
        /// Obtem a configuração por ID
        /// </summary>
        /// <param name="id">ID da configuração</param>
        /// <returns>A configuração cujo ID é o passado como parâmetro</returns>
        public ConfiguracaoEnvioDocumentacao ObterPorId(int id)
        {
            if (id <= 0)
            {
                throw new ArgumentException("Configuração não informada.");
            }

            var configuracao = _configuracaoEnvioDocumentacaoRepository.ObterPorId(id);
            return configuracao;
        }

        /// <summary>
        /// Obtem todas as configurações
        /// </summary>
        /// <returns></returns>
        public ICollection<ConfiguracaoEnvioDocumentacao> ObterTodos()
        {
            var configuracoes = _configuracaoEnvioDocumentacaoRepository.ObterTodos()
                                                                        .ToList();
            return configuracoes;
        }

        /// <summary>
        /// Obtem todas as configurações ativas
        /// </summary>
        /// <returns></returns>
        public ICollection<ConfiguracaoEnvioDocumentacao> ObterTodosAtivos()
        {
            var configuracoes = _configuracaoEnvioDocumentacaoRepository.ObterTodos()
                                                                        .Where(ced => ced.Ativo == "S")
                                                                        .ToList();
            return configuracoes;
        }

        /// <summary>
        /// Obtem os resultados da grid da tela de monitoramento de envio das documentações do trem sem papel
        /// </summary>
        /// <param name="pagination">Paginação da Grid</param>
        /// <param name="malha">Número da Ordem de Serviço</param>
        /// <param name="destino">Número da Ordem de Serviço</param>
        /// <param name="recebedor">ID do terminal recebedor da mercadoria</param>
        /// <param name="numeroOs">Número da Ordem de Serviço</param>
        /// <param name="prefixo">Número da Ordem de Serviço</param>
        /// <param name="dataInicio">Número da Ordem de Serviço</param>
        /// <param name="dataFim">Número da Ordem de Serviço</param>
        /// <param name="tremEncerr">Número da Ordem de Serviço</param>
        /// <param name="situacaoEnvioStatus">Número da Ordem de Serviço</param>
        /// <param name="docCompl">Número da Ordem de Serviço</param>
        /// <returns>Retorna o resultado paginado da busca</returns>
        public ResultadoPaginado<MonitoramentoEnvioDocumentacaoDto> ObterMonitoramentoGrid(DetalhesPaginacaoWeb pagination,
                                                                                           string malha,
                                                                                           string destino,
                                                                                           string recebedor,
                                                                                           string numeroOs,
                                                                                           string prefixo,
                                                                                           string dataInicio,
                                                                                           string dataFim,
                                                                                           string tremEncerr,
                                                                                           int situacaoEnvioStatus,
                                                                                           string docCompl)
        {
            numeroOs = numeroOs.Replace("TODOS", string.Empty);

            decimal? recebedorId = null;
            if (!string.IsNullOrEmpty(recebedor))
            {
                decimal recebedorIdDecimal = 0;
                if (Decimal.TryParse(recebedor, out recebedorIdDecimal) && recebedorIdDecimal > 0)
                {
                    recebedorId = recebedorIdDecimal;
                }
            }

            var results = _configuracaoEnvioDocumentacaoRepository.ObterMonitoramentoGrid(pagination,
                                                                                          malha,
                                                                                          destino,
                                                                                          recebedorId,
                                                                                          numeroOs,
                                                                                          prefixo,
                                                                                          dataInicio,
                                                                                          dataFim,
                                                                                          tremEncerr,
                                                                                          situacaoEnvioStatus,
                                                                                          docCompl);

            return results;
        }

        /// <summary>
        /// Obtem os resultados da grid da tela de envio de documentação de refaturamento
        /// </summary>
        /// <param name="pagination">Paginação da Grid</param>
        /// <param name="numeroOs">Número da Ordem de Serviço</param>
        /// <param name="recebedor">ID do terminal recebedor da mercadoria</param>
        /// <returns>Retorna o resultado paginado da busca</returns>
        public ResultadoPaginado<DocumentacaoRefaturamentoDto> ObterRefaturamentosGrid(DetalhesPaginacaoWeb pagination,
                                                                                       string numeroOs,
                                                                                       string recebedor)
        {
            if (string.IsNullOrEmpty(numeroOs))
            {
                throw new ArgumentException("Número da OS não informado.");
            }
            numeroOs = numeroOs.Replace("TODOS", string.Empty);

            decimal? recebedorId = null;
            if (!string.IsNullOrEmpty(recebedor))
            {
                decimal recebedorIdDecimal = 0;
                if (Decimal.TryParse(recebedor, out recebedorIdDecimal) && recebedorIdDecimal > 0)
                {
                    recebedorId = recebedorIdDecimal;
                }
            }

            var results = _configuracaoEnvioDocumentacaoRepository.ObterRefaturamentosGrid(pagination,
                                                                                          numeroOs,
                                                                                          recebedorId);

            return results;
        }

        /// <summary>
        /// Obtem os resultados da grid da tela de parametrização
        /// </summary>
        /// <param name="pagination">Paginação da Grid</param>
        /// <param name="areaOperacionalEnvio">Código da Área Operacional de envio</param>
        /// <param name="terminalDestino">Terminal para ser enviado</param>
        /// <param name="ativo">Bool indicando se é para filtrar por registros ativos ou não. Se independe, envie null</param>
        /// <returns>Retorna o resultado paginado da busca</returns>
        public ResultadoPaginado<ConfiguracaoEnvioDocumentacao> ObterConfiguracoesGrid(DetalhesPaginacaoWeb pagination,
                                                                           string areaOperacionalEnvio,
                                                                           string terminalDestino,
                                                                           string ativo)
        {
            bool? statusAtivo = null;
            if (!string.IsNullOrEmpty(ativo))
            {
                var statusAtivoOut = true;
                if (!bool.TryParse(ativo, out statusAtivoOut))
                {
                    statusAtivoOut = true;
                }

                statusAtivo = statusAtivoOut;
            }

            var results = _configuracaoEnvioDocumentacaoRepository.ObterConfiguracoesGrid(pagination,
                                                                                          areaOperacionalEnvio,
                                                                                          terminalDestino,
                                                                                          statusAtivo);

            return results;
        }

        public ResultadoPaginado<EmailEnvioDocumentacao> ObterEmailsConfiguracaoGrid(DetalhesPaginacaoWeb pagination, int idConfiguracao)
        {
            var results = _emailDocumentacaoRepository.ObterEmailsPorConfiguracaoGrid(pagination, idConfiguracao);
            return results;
        }

        public IList<EmpresaCliente> ObterListaNomesTerminais(string query)
        {
            IList<EmpresaCliente> retorno = new List<EmpresaCliente>();
            IList<EmpresaCliente> resultados = _empresaClienteRepository.ObterListaNomesTerminais(query);

            foreach (var resultado in resultados)
            {
                if (!retorno.Any(r => r.Cgc == resultado.Cgc))
                {
                    retorno.Add(resultado);
                }
            }

            return retorno.OrderBy(r => r.DescricaoResumida).ThenBy(r => r.Cgc).ToList();
        }

        /// <summary>
        /// Obtem todos as configurações ativas que possuem vagões destinados a ele. Omitindo o número da OS, verifica-se diretamente na tabela de configurações.
        /// </summary>
        /// <param name="numeroOs">Número da OS</param>
        /// <returns>Retorna todas as empresas cuja configuraação esteja ativa</returns>
        public List<ConfiguracaoEnvioDocumentacaoDto> ObterTerminaisPorOs(string numeroOs)
        {
            var terminais = _configuracaoEnvioDocumentacaoRepository.ObterTerminaisPorOs(numeroOs);
            return terminais.ToList();
        }
    }
}