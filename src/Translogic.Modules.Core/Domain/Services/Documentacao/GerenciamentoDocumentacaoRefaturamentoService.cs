﻿namespace Translogic.Modules.Core.Domain.Services.Documentacao
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Translogic.Modules.Core.Domain.Model.Documentacao;
    using Translogic.Modules.Core.Domain.Model.Documentacao.Repositories;
    using Translogic.Modules.Core.Domain.Model.Dto;

    public class GerenciamentoDocumentacaoRefaturamentoService
    {
        private readonly IGerenciamentoDocumentacaoRefaturamentoRepository _gerenciamentoDocumentacaoRefaturamentoRepository;
        private readonly IGerenciamentoDocumentacaoRefaturamentoVagaoRepository _gerenciamentoDocumentacaoRefaturamentoVagaoRepository;
        private readonly IDocumentacaoMotivoRefaturamentoRepository _documentacaoMotivoRefaturamentoRepository;

        public GerenciamentoDocumentacaoRefaturamentoService(IGerenciamentoDocumentacaoRefaturamentoRepository gerenciamentoDocumentacaoRefaturamentoRepository,
                                                             IGerenciamentoDocumentacaoRefaturamentoVagaoRepository gerenciamentoDocumentacaoRefaturamentoVagaoRepository,
                                                             IDocumentacaoMotivoRefaturamentoRepository documentacaoMotivoRefaturamentoRepository)
        {
            _gerenciamentoDocumentacaoRefaturamentoRepository = gerenciamentoDocumentacaoRefaturamentoRepository;
            _gerenciamentoDocumentacaoRefaturamentoVagaoRepository = gerenciamentoDocumentacaoRefaturamentoVagaoRepository;
            _documentacaoMotivoRefaturamentoRepository = documentacaoMotivoRefaturamentoRepository;
        }

        public void SalvarDocumentacaoRefaturamento(List<DocumentacaoRefaturamentoDto> documentacoes)
        {
            // Agrupando por ID de OS e Recebedor
            var refaturamentos = documentacoes.Select(d => new GerenciamentoDocumentacaoRefaturamento
                                                               {
                                                                   OsId = d.OsId,
                                                                   ComposicaoId = d.ComposicaoId,
                                                                   NumeroOs = d.NumeroOs,
                                                                   PrefixoTrem = d.PrefixoTrem,
                                                                   RecebedorId = d.RecebedorId,
                                                                   Processado = "N",
                                                                   CriadoEm = DateTime.Now
                                                               })
                                              .GroupBy(d => new
                                                            {
                                                                d.OsId,
                                                                d.RecebedorId
                                                            })
                                              .Select(d => d.First())
                                              .ToList();

            // Para cada OS e Recebedor, devemos verificar se existe um registro no banco já cadastrado
            foreach (var refaturamento in refaturamentos)
            {
                var osId = refaturamento.OsId;
                var recebedorId = refaturamento.RecebedorId;
                var composicaoId = refaturamento.ComposicaoId;

                var motivoId = documentacoes.Where(d => d.OsId == osId && d.RecebedorId == recebedorId && d.MotivoId != null)
                                                .Select(d => int.Parse(d.MotivoId.ToString()))
                                                .First();
                var motivo = _documentacaoMotivoRefaturamentoRepository.ObterPorId(motivoId);

                // Se for null, indica que o registro é novo e devemos realizar apenas INSERTs
                var refaturamentoDb = _gerenciamentoDocumentacaoRefaturamentoRepository.ObterPorOsRecebedor(osId, recebedorId);
                if (refaturamentoDb == null)
                {
                    refaturamento.MotivoRefaturamento = motivo;
                    refaturamento.ComposicaoId = composicaoId;
                    _gerenciamentoDocumentacaoRefaturamentoRepository.Inserir(refaturamento);

                    var vagoesSolicitados = documentacoes.Where(d => d.OsId == osId && d.RecebedorId == recebedorId)
                                                         .Select(d => new GerenciamentoDocumentacaoRefaturamentoVagao
                                                                          {
                                                                              GerenciamentoDocumentacaoRefaturamento = refaturamento,
                                                                              CodigoVagao = d.CodigoVagao,
                                                                              Ativo = "S",
                                                                              CriadoEm = DateTime.Now
                                                                          })
                                                         .ToList();

                    foreach (var vagao in vagoesSolicitados)
                    {
                        _gerenciamentoDocumentacaoRefaturamentoVagaoRepository.Inserir(vagao);
                    }
                }
                // Há um registro no banco já com esta OS e Recebedor
                else
                {
                    refaturamento.ComposicaoId = composicaoId;
                    refaturamentoDb.Processado = "N";
                    refaturamentoDb.CriadoEm = DateTime.Now;
                    refaturamentoDb.MotivoRefaturamento = motivo;
                    _gerenciamentoDocumentacaoRefaturamentoRepository.Atualizar(refaturamentoDb);

                    // Deletando todos os vagões da configuração antiga
                    foreach (var vagao in refaturamentoDb.Vagoes)
                    {
                        vagao.Ativo = "N";
                        _gerenciamentoDocumentacaoRefaturamentoVagaoRepository.Atualizar(vagao);
                    }

                    // Recriando os vagões
                    var vagoesSolicitados = documentacoes.Where(d => d.OsId == osId && d.RecebedorId == recebedorId)
                                                         .Select(d => new GerenciamentoDocumentacaoRefaturamentoVagao
                                                                          {
                                                                              GerenciamentoDocumentacaoRefaturamento = refaturamentoDb,
                                                                              CodigoVagao = d.CodigoVagao,
                                                                              Ativo = "S",
                                                                              CriadoEm = DateTime.Now
                                                                          })
                                                         .ToList();

                    foreach (var vagao in vagoesSolicitados)
                    {
                        _gerenciamentoDocumentacaoRefaturamentoVagaoRepository.Inserir(vagao);
                    }
                }
            }
        }
    }
}