﻿namespace Translogic.Modules.Core.Domain.Services.Documentacao
{
    using System.Collections.Generic;
    using System.Linq;
    using Translogic.Modules.Core.Domain.Model.Documentacao;
    using Translogic.Modules.Core.Domain.Model.Documentacao.Repositories;

    public class HistoricoEnvioDocumentacaoService
    {
        private readonly IHistoricoEnvioDocumentacaoRepository _historicoEnvioRepository;

        public HistoricoEnvioDocumentacaoService(IHistoricoEnvioDocumentacaoRepository historicoEnvioRepository)
        {
            _historicoEnvioRepository = historicoEnvioRepository;
        }

        /// <summary>
        /// Obtem todas os envios
        /// </summary>
        /// <returns></returns>
        public ICollection<HistoricoEnvioDocumentacao> ObterTodos()
        {
            var envios = _historicoEnvioRepository.ObterTodos().ToList();
            return envios;
        }
    }
}