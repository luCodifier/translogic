﻿using System.Linq;
using System.Text.RegularExpressions;
using Castle.Services.Transaction;
using Translogic.Core.Infrastructure.Web;
using Translogic.Modules.Core.Domain.Model.Dto;
using Translogic.Modules.Core.Domain.Model.Estrutura.Repositories;
using Translogic.Modules.Core.Domain.Model.MotivoSituacaoVagao.Repositories;
using Translogic.Modules.Core.Domain.Services.MotivoSituacaoVagao.Interface;
using Translogic.Modules.Core.Domain.Model.MotivoSituacaoVagao;

namespace Translogic.Modules.Core.Domain.Services.MotivoSituacaoVagao
{    
    using System.Collections.Generic;    
    using System;   
    using ALL.Core.Dominio;
    using Translogic.Modules.Core.Domain.Model.Estrutura;
    using System.Globalization;
    using Translogic.Modules.Core.Domain.Model.Acesso;
    using Translogic.Modules.Core.Domain.Model.Acesso.Repositories;

    public class MotivoSituacaoVagaoService : IMotivoSituacaoVagaoService
    {
        private readonly IMotivoSituacaoVagaoRepository _motivoSituacaoVagaoRepository;
     
        /// <summary>
        ///  Initializes a new instance of the <see cref="MotivoSituacaoVagaoService"/> class.
        /// </summary>
        /// <param name="motivoSituacaoVagaoRepository">Repositório de Motivo Situacao Vagao</param>
        public MotivoSituacaoVagaoService(IMotivoSituacaoVagaoRepository motivoSituacaoVagaoRepository)
        {
            this._motivoSituacaoVagaoRepository = motivoSituacaoVagaoRepository;                        
        }

        public IList<MotivoSituacaoVagaoLotacaoDto> ObterLotacoes()
        {
            return _motivoSituacaoVagaoRepository.ObterLotacoes(); 
        }

        public IList<MotivoSituacaoVagaoSituacaoDto> ObterTodasSituacoes()
        {
            return _motivoSituacaoVagaoRepository.ObterTodasSituacoes();
        }

        public IList<MotivoSituacaoVagaoSituacaoDto> ObterSituacoes()
        {
            return _motivoSituacaoVagaoRepository.ObterSituacoes();
        }

        public IList<MotivoSituacaoVagaoMotivoDto> ObterMotivos(string idSituacao)
        {
            return _motivoSituacaoVagaoRepository.ObterMotivos(idSituacao);
        }

        public ResultadoPaginado<MotivoSituacaoVagaoDto> ObterConsultaDeVagoesPatio(DetalhesPaginacaoWeb detalhesPaginacaoWeb, string listaDeVagoes, string idLocal, string idOS, string idLotacao, 
                                                    string idSituacao, string idMotivo)
        {
            return _motivoSituacaoVagaoRepository.ObterConsultaDeVagoesPatio(detalhesPaginacaoWeb, listaDeVagoes, idLocal, idOS, idLotacao, idSituacao, idMotivo);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="vagaoMotivo"></param>
        /// <param name="usuarioAtual"></param>
        /// <returns></returns>
        public bool InserirMotivosVagoes(List<VagaoMotivo> vagaoMotivo, string usuarioAtual)
        {
            bool incluiuTodosOsRegistros = false;

            try{
                foreach (var item in vagaoMotivo)
                {
                    VagaoMotivo _vagaoMotivo;
                    _vagaoMotivo = new Model.MotivoSituacaoVagao.VagaoMotivo();

                    _vagaoMotivo.IdMotivo = item.IdMotivo;
                    _vagaoMotivo.IdVagao = item.IdVagao;
                    _vagaoMotivo.DataDaInclusao = DateTime.Now;
                    _vagaoMotivo.UsuarioInclusao = usuarioAtual;
                    _vagaoMotivo.UsuarioExclusao = null;
                    _vagaoMotivo.DataDaExclusao = null;
                    _vagaoMotivo.DataUltimaAtualizacao = DateTime.Now;

                    _motivoSituacaoVagaoRepository.Inserir(_vagaoMotivo);
                }

                incluiuTodosOsRegistros = true;

            }
            catch
            {
                incluiuTodosOsRegistros = false;
            }
            

            return incluiuTodosOsRegistros;
        }

       /// <summary>
       /// 
       /// </summary>
       /// <param name="idVagaoMotivo"></param>
       /// <param name="idVagao"></param>
       /// <param name="idMotivo"></param>
       /// <param name="usuarioAtual"></param>
       /// <returns></returns>
        public bool ExcluirMotivoDoVagao(string idVagaoMotivo, string idVagao, string idMotivo, string usuarioAtual)
        {
            bool excluiuRegistro = false;

            var vagaoMotivo = _motivoSituacaoVagaoRepository.ObterVagaoMotivo(idVagaoMotivo, idVagao, idMotivo);

            vagaoMotivo.DataDaExclusao = DateTime.Now;
            vagaoMotivo.UsuarioExclusao = usuarioAtual;
            vagaoMotivo.DataUltimaAtualizacao = DateTime.Now;

            try
            {
                _motivoSituacaoVagaoRepository.InserirOuAtualizar(vagaoMotivo);
                excluiuRegistro = true;
            }
            catch
            {
                excluiuRegistro = false;
            }


            return excluiuRegistro;
        }
    }
}