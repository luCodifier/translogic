﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ALL.Core.Dominio;
using Translogic.Core.Infrastructure.Web;
using Translogic.Modules.Core.Domain.Model.Dto;
using Translogic.Modules.Core.Domain.Model.MotivoSituacaoVagao;

namespace Translogic.Modules.Core.Domain.Services.MotivoSituacaoVagao.Interface
{
    public interface IMotivoSituacaoVagaoService
    {
        IList<MotivoSituacaoVagaoLotacaoDto> ObterLotacoes();

        IList<MotivoSituacaoVagaoSituacaoDto> ObterTodasSituacoes();

        IList<MotivoSituacaoVagaoSituacaoDto> ObterSituacoes();

        IList<MotivoSituacaoVagaoMotivoDto> ObterMotivos(string idSituacao);

        ResultadoPaginado<MotivoSituacaoVagaoDto> ObterConsultaDeVagoesPatio(DetalhesPaginacaoWeb detalhesPaginacaoWeb, string listaDeVagoes, string idLocal, string idOS, 
                                                                            string idLotacao, string idSituacao, string idMotivo);

        bool InserirMotivosVagoes(List<VagaoMotivo> vagaoMotivo, string usuarioAtual);

        bool ExcluirMotivoDoVagao(string idVagaoMotivo, string idVagao, string idMotivo, string usuarioAtual);
    }
}
        