﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using Translogic.Modules.Core.Domain.Model.TipoServicos;
using Translogic.Modules.Core.Domain.Services.TipoServico.Interface;
using Translogic.Modules.Core.Domain.Model.TipoServicos.Repositories;
using Translogic.Modules.Core.Domain.Model.Dto;

namespace Translogic.Modules.Core.Domain.Services.TipoServico
{
    public class TipoServicoService : ITipoServicoService
    {
        private readonly ITipoServicoRepository  _TipoServicoRepository;
        public TipoServicoService(ITipoServicoRepository TipoServicoRepository)
        {
            _TipoServicoRepository = TipoServicoRepository;
        }

        public IList<TipoServicoDto> ObterTipoServico()
        {
            return _TipoServicoRepository.ObterTipoServico();
        }

        public TipoServicoDto ObterTipoServicoPorDescricao(string descricaoTipoServico)
        {
            var tipoServico= _TipoServicoRepository.ObterTipoServicoPorDescricaoServico(descricaoTipoServico);
            return Mapper.Map<Translogic.Modules.Core.Domain.Model.TipoServicos.TipoServico, TipoServicoDto>(tipoServico);
        }
    }
}