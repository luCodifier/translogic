﻿
namespace Translogic.Modules.Core.Domain.Services.TipoServico.Interface
{
    using System.Collections.Generic;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Modules.Core.Util;

    public interface ITipoServicoService
    {
        ///<sumary>
        ////Obter lista de tipo de serviços
        ///</sumary>
        ///
        IList<TipoServicoDto> ObterTipoServico();
        
        /// <summary>
        /// Retorna objeto tipo serviço, informando a descrição do serviço
        /// </summary>
        /// <param name="tipoServico"></param>
        /// <returns></returns>        
        TipoServicoDto ObterTipoServicoPorDescricao(string descricaoTipoServico);
    }
}
