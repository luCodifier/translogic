﻿using System.Collections.Generic;
using Translogic.Modules.Core.Domain.Model.Seguro;
using Translogic.Modules.Core.Domain.Model.Seguro.Repositories;

namespace Translogic.Modules.Core.Domain.Services.Seguro
{
	public class UnidadeSeguroService
	{
		private readonly IUnidadeSeguroRepository _unidadeSeguroRepository;

		public UnidadeSeguroService(IUnidadeSeguroRepository unidadeSeguroRepository)
		{
			_unidadeSeguroRepository = unidadeSeguroRepository;
		}

		/// <summary>
		///   Obtem a lista enumerada de terminais
		/// </summary>
		/// <returns>Lista enumerada de Terminais</returns>
		public IEnumerable<UnidadeSeguro> ObterTerminais()
		{
			return _unidadeSeguroRepository.ObterTerminais();
		}

        public IEnumerable<UnidadeSeguro> ObterUnidadesTipoN()
        {
            return _unidadeSeguroRepository.ObterUnidadesTipoN();
        }

        public IEnumerable<UnidadeSeguro> ObterUP()
        {
            return _unidadeSeguroRepository.ObterUP();
        }

        
    }
}