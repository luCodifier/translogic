﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Translogic.Modules.Core.Domain.Model.Seguro.Repositories;
using Translogic.Modules.Core.Domain.Model.Dto;
using Translogic.Modules.Core.Domain.Services.Seguro.Interface;

namespace Translogic.Modules.Core.Domain.Services.Seguro
{
    public class MalhaSeguroService:IMalhaSeguroService
    {
        private readonly IMalhaSeguroRepository _malhaSeguroRepository;

        public MalhaSeguroService(IMalhaSeguroRepository malhaSeguroRepository)
        {
            _malhaSeguroRepository = malhaSeguroRepository;
        }

        public IList<MalhaSeguroDto> ObterMalha()
        {
           return _malhaSeguroRepository.ObterMalha();
        }
    }
}