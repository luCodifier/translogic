﻿namespace Translogic.Modules.Core.Domain.Services.Seguro.Interface
{
    using System.Collections.Generic;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Modules.Core.Util;
    using ALL.Core.Dominio;
    using Translogic.Core.Infrastructure.Web;
    using System;
    using System.Collections;

    public interface IMalhaSeguroService
    {
        /// <summary>
        /// Obtem lista de Malha
        /// </summary>
        /// <returns>IList de malha</returns>
        IList<MalhaSeguroDto> ObterMalha();
    }
}
