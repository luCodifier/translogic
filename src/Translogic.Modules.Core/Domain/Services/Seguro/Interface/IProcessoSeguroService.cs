﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Translogic.Modules.Core.Domain.Model.Seguro;
using System.Web.Mvc;
using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos;

namespace Translogic.Modules.Core.Domain.Services.Seguro.Interface
{
    public interface IProcessoSeguroService
    {
        /// <summary>
        /// Gera o Dossiê para o número de processo informado no parâmetro
        /// </summary>
        /// <param name="numeroProcesso">Número do Processo a ser criado o Dossiê</param>
        void GerarDossie(int numeroProcesso);

        /// <summary>
        /// Gera o Dossiê para o número de processo e número de despacho informado no parâmetro
        /// </summary>
        /// <param name="numeroProcesso">Número do Processo a ser criado o Dossiê</param>
        /// <param name="numeroDespacho">Número do Despacho a ser criado o Dossiê</param>
        void GerarDossie(int numeroProcesso, int numeroDespacho, string serie);


        /// <summary>
        /// Gera o Dossiê para o número de processo e despacho informado no parâmetro
        /// </summary>
        /// <param name="numeroProcesso">Número do Processo a ser criado o Dossiê</param>
        /// <param name="despacho">Despacho a ser criado o Dossiê</param>
        void GerarDossie(int numeroProcesso, DespachoTranslogic despacho);
       
        /// <summary>
        /// Gera o Dossiê para o processo número de processo informado no parâmetro
        /// </summary>
        /// <param name="numeroProcesso">Número do Processo a ser criado o Dossiê</param>
        /// <param name="itemDespacho">Item de Despacho do Processo a ser criado o Dossiê</param>
        void GerarDossie(int numeroProcesso, ItemDespacho itemDespacho);
    }
}
