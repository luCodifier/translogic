﻿using System;
using System.Collections.Generic;
using System.Linq;
using ALL.Core.Dominio;
using Translogic.Core.Infrastructure.Web;
using Translogic.Modules.Core.Domain.Model.Acesso;
using Translogic.Modules.Core.Domain.Model.Dto;
using Translogic.Modules.Core.Domain.Model.Seguro;
using Translogic.Modules.Core.Domain.Model.Seguro.Repositories;

namespace Translogic.Modules.Core.Domain.Services.Seguro
{
	public class MangaService
	{
        private readonly IMangaRepository _MangaRepository;

        public MangaService(IMangaRepository MangaRepository)
		{
			_MangaRepository = MangaRepository;
		}

        /// <summary>
        ///   Retorna Manga
        /// </summary>
        /// <param name="usuario">Manga</param>
        /// <returns>Manga</returns>
        public string ObterPesoMangaPorVagao(string vagao)
        {
            return _MangaRepository.ObterPesoMangaPorVagao(vagao);
        }
	}
}