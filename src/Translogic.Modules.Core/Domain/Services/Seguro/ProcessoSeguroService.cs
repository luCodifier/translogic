﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Translogic.Modules.Core.Domain.Model.PainelExpedicao.Documentos.Request;
using System.IO;
using Translogic.Modules.Core.Domain.Services.GestaoDocumentos;
using Translogic.Modules.Core.Domain.Model.PainelExpedicao.Documentos.Dto;
using Translogic.Modules.Core.Domain.Services.Ctes;
using Translogic.Modules.Core.Domain.Services.Seguro.Interface;
using Translogic.Modules.Core.Domain.Model.Dto;
using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
using Translogic.Core.Infrastructure.FileSystem.ExtensionMethods;
using Translogic.Modules.Core.Domain.Services.Mdfes;
using Translogic.Modules.Core.Domain.Model.Seguro.Repositories;
using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos.Repositories;
using Translogic.Core.Infrastructure.Web.Helpers;
using Translogic.Core.Infrastructure.Aws;
using System.Threading.Tasks;
using Translogic.Modules.Core.Domain.Model.Tfa.Repositories;
using Translogic.Modules.Core.Domain.Services.Tfa.Interface;
using Translogic.Modules.EDI.Domain.Models.Nfes.Repositories;
using Castle.Core.Logging;
using Translogic.Modules.Core.Util;
using Translogic.Modules.Core.Domain.Model.FluxosComerciais.DocumentosVagao;
using Translogic.Modules.Core.Interfaces.GestaoDocumentos.Balanca;
using Translogic.Modules.Core.Domain.Model.Seguro;
using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos;
using Translogic.Modules.EDI.Domain.Models.Dto;
using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Nfes.Repositories;
using Translogic.Modules.Core.Domain.Model.Tfa;
using Translogic.Core;
using Translogic.Modules.Core.Helpers.ControlePerdas;
using System.Threading;
using System.Configuration;

namespace Translogic.Modules.Core.Domain.Services.Seguro
{
    public class ProcessoSeguroService : IProcessoSeguroService
    {
        #region ATRIBUTOS

        private ILogger _logger = NullLogger.Instance;

        #endregion

        const string CONFIGURACAO_GERAR_TFA_LAUDO = "GERAR_TFA_LAUDO";

        private readonly RelatorioDocumentosService _relatorioDocumentosService;

        private readonly CteService _cteService;

        private readonly ITfaService _tfaService;

        private readonly IProcessoSeguroRepository _processoSeguroRepository;

        private readonly IItemDespachoRepository _itemDespachoRepository;

        private readonly IModalSeguroRepository _modalSeguroRepository;

        private readonly MdfeService _mdfeService;

        private readonly INfePdfEdiRepository _nfePdfEdiRepository;
        private readonly INfeReadonlyRepository _nfeReadonlyRepository;

        private readonly IDespachoTranslogicRepository _despachoTranslogicRepository;
   
        private readonly ILogTfaRepository _logTfaRepository;
        
        public ProcessoSeguroService(RelatorioDocumentosService relatorioDocumentosService, CteService cteService, IProcessoSeguroRepository processoSeguroRepository, IItemDespachoRepository itemDespachoRepository, IModalSeguroRepository modalSeguroRepository, ITfaService tfaService, MdfeService mdfeService, INfePdfEdiRepository nfePdfEdiRepository, IDespachoTranslogicRepository despachoTranslogicRepository, INfeReadonlyRepository nfeReadonlyRepository, ILogTfaRepository logTfaRepository)
        {
            _relatorioDocumentosService = relatorioDocumentosService;
            _cteService = cteService;
            _processoSeguroRepository = processoSeguroRepository;
            _itemDespachoRepository = itemDespachoRepository;
            _modalSeguroRepository = modalSeguroRepository;
            _tfaService = tfaService;
            _mdfeService = mdfeService;
            _nfePdfEdiRepository = nfePdfEdiRepository;
            _despachoTranslogicRepository = despachoTranslogicRepository;
            _nfeReadonlyRepository = nfeReadonlyRepository;   
            _logTfaRepository = logTfaRepository;
        }

        /// <summary>
        ///   Gera o Dossiê para o processo número de processo informado como parâmetro
        /// </summary>
        /// <param name="numeroProcesso">Número do Processo a ser criado o Dossiê</param>
        public void GerarDossie(int numeroProcesso)
        {
            try
            {
                var processoSeguro = _processoSeguroRepository.ObterPorId(numeroProcesso);

                if (processoSeguro != null)
                {
                    var modalSeguro = _modalSeguroRepository.ObterPorProcesso(processoSeguro);

                    if (modalSeguro != null && modalSeguro.Despacho != null)
                    {
                        GerarDossie(numeroProcesso, modalSeguro.Despacho.Value, modalSeguro.Serie);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.ErrorFormat("[GERAR_DOSSIE] - Erro ao gerar Dossiê para o processo {0}: {1}", numeroProcesso, ex);
            }

        }

        /// <summary>
        /// Gera o Dossiê para o número de processo e número de despacho informado no parâmetro
        /// </summary>
        /// <param name="numeroProcesso">Número do Processo a ser criado o Dossiê</param>
        /// <param name="numeroDespacho">Número do Despacho a ser criado o Dossiê</param>
        public void GerarDossie(int numeroProcesso, int numeroDespacho, string serie)
        {
            var despacho = _despachoTranslogicRepository.ObterDespachoPeloNumeroDespachoSerie(numeroDespacho, serie).First();

            if (despacho != null)
            {
                var itemDespacho = _itemDespachoRepository.ObterItemDespachoPorDespacho(despacho);

                GerarDossie(numeroProcesso, itemDespacho);
            }


        }

        /// <summary>
        /// Gera o Dossiê para o número de processo e despacho informado no parâmetro
        /// </summary>
        /// <param name="numeroProcesso">Número do Processo a ser criado o Dossiê</param>
        /// <param name="despacho">Despacho a ser criado o Dossiê</param>
        public void GerarDossie(int numeroProcesso, DespachoTranslogic despacho)
        {
            if (despacho != null)
            {
                var itemDespacho = _itemDespachoRepository.ObterItemDespachoPorDespacho(despacho);

                GerarDossie(numeroProcesso, itemDespacho);
            }


        }


        /// <summary>
        /// Gera o Dossiê para o processo número de processo informado no parâmetro
        /// </summary>
        /// <param name="numeroProcesso">Número do Processo a ser criado o Dossiê</param>
        /// <param name="itemDespacho">Item de Despacho do Processo a ser criado o Dossiê</param>
        public void GerarDossie(int numeroProcesso, ItemDespacho itemDespacho)
        {
            LogTfaHelper.Trace("Start GerarDossie Async", numeroProcesso);

            try
            {
                if (itemDespacho != null && itemDespacho.Vagao != null && itemDespacho.DespachoTranslogic != null)
                {
                    var idsItensDespacho = new List<int> { itemDespacho.Id.Value };
                    var listaCodigosVagoes = new List<string> { itemDespacho.Vagao.Codigo };

                    var uploader = new AmazonUploader();
                    //uploader.Client.putObject(uploader.Bucket, numeroProcesso, new ByteArrayInputStream(new byte[0]), null);

                    var TaskCTE = Task.Factory.StartNew(() => UploadCTE(numeroProcesso, itemDespacho.DespachoTranslogic.Id.Value));
                    var TaskNFE = Task.Factory.StartNew(() => UploadNFE(numeroProcesso, idsItensDespacho));
                    var TaskTFA = Task.Factory.StartNew(() => _tfaService.UploadTFA(numeroProcesso));
                    var TaskTicket = Task.Factory.StartNew(() => UploadTicket(numeroProcesso, idsItensDespacho, listaCodigosVagoes));

                    Task.WaitAll(TaskCTE, TaskNFE, TaskTicket, TaskTFA);

                }
            }
            catch (AggregateException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw;
            }
            LogTfaHelper.Trace("End GerarDossie Async", numeroProcesso);
        }

        
        private const string TemporaryFolderKey = "TemporaryFolder";
        private string GetTemporaryFolder()
        {
            string temporaryFolderValue = string.Empty;
            AppSettingsReader reader = new AppSettingsReader();
            try
            {
                temporaryFolderValue = (string)reader.GetValue(TemporaryFolderKey, temporaryFolderValue.GetType());

                //if (!string.IsNullOrEmpty(temporaryFolderValue))
                //{
                //    var temporaryFolder = Server.MapPath(temporaryFolderValue);
                //    return temporaryFolder;
                //}

            }
            catch { }

            return temporaryFolderValue;
        }

        private void UploadCTE(int numeroProcesso, int idDespacho)
        {
            LogTfaHelper.Trace("Start UploadCTE", numeroProcesso);

            AmazonUploader uploader = new AmazonUploader();

            //CTE            
            var pdfsCtes = _cteService.ObterArquivoCtePorDespacho(idDespacho).ToList();

            if (pdfsCtes != null)
            {
                LogTfaHelper.Trace("Qtd CTEs: " + pdfsCtes.Count(), numeroProcesso);

                foreach (var item in pdfsCtes)
                {
                    var i = 0;
                    bool uploaded = false;
                    do
                    {
                        i++;
                        LogTfaHelper.Trace("Uploading CTE Try: " + i, numeroProcesso);
                        try
                        {
                            uploaded = uploader.UploadObject(new MemoryStream(item.ArquivoPdf), string.Format("CTE_{0}.pdf", numeroProcesso), numeroProcesso.ToString());
                        }
                        catch (Exception ex)
                        {
                            LogTfaHelper.Trace(ex.Message + ex.StackTrace);
                        }

                    } while (!uploaded && i <= 10);

                    var temporaryFolder = Path.Combine(GetTemporaryFolder(), Environment.UserName);
                    var file = uploader.GetObject(string.Format("CTE_{0}.pdf", numeroProcesso), temporaryFolder, numeroProcesso.ToString());

                    LogTfaHelper.Trace("End GetObject" + (file != null ? "Existe arquivo" : "Não existe arquivo"), numeroProcesso);

                    if(file == null)
                    {
                        if(!LogTfaHelper.responseMessage.ContainsKey(numeroProcesso.ToString())){
                            LogTfaHelper.responseMessage.Add(numeroProcesso.ToString(), new List<string>());
                        }

                        LogTfaHelper.responseMessage[numeroProcesso.ToString()].Add("Arquivo CTE não existe");
                        //INSERE TABELA LOG_TFA se deu aproblema com upload do arquivo
                        InsereLogUploadArquivo(_tfaService.ObterPorNumProcesso(numeroProcesso).Id, "CTE");
                    }                  
                }
                //pdfsCtes.ForEach(p => uploader.UploadObject(new MemoryStream(p.ArquivoPdf), string.Format("CTE_{0}.pdf", numeroProcesso), numeroProcesso.ToString()));
            }

            LogTfaHelper.Trace("End UploadCTE", numeroProcesso);
            
        }

        private void UploadNFE(int numeroProcesso, List<int> idsItensDespacho)
        {
            LogTfaHelper.Trace("Start UploadNFE", numeroProcesso);

            AmazonUploader uploader = new AmazonUploader();

            //NFE/Danfe
            var notasNfe = _relatorioDocumentosService.ObterNfePdfItensDespachoDto(idsItensDespacho);
            if (notasNfe != null)
            {
                var chavesNfes = notasNfe.Select(nf => nf.ChaveNfe).ToList();
                if (chavesNfes.Any())
                {
                    //pdfsNfes = _nfePdfEdiRepository.ObterPorChavesNfe(chavesNfes).ToList();
                    var pdfsNfes = _nfeReadonlyRepository.ObterPdfEdiPorChavesNfe(chavesNfes).ToList();

                    LogTfaHelper.Trace("Qtd NFEs: " + pdfsNfes.Count(), numeroProcesso);

                    //var pdfsNfes = _relatorioDocumentosService.GerarPdfNfes(chavesNfes, true, false).ToList();

                    foreach (var item in pdfsNfes)
                    {
                        var i = 0;
                        bool uploaded = false;
                        do
                        {
                            i++;
                            LogTfaHelper.Trace("Uploading NFE Try: " + i, numeroProcesso);
                            try
                            {
                                uploaded = uploader.UploadObject(new MemoryStream(item.Pdf), string.Format("NFE_{0}_{1}.pdf", numeroProcesso, item.ChaveNfe), numeroProcesso.ToString());
                            }
                            catch (Exception ex)
                            {
                                LogTfaHelper.Trace(ex.Message + ex.StackTrace);
                            }

                        } while (!uploaded && i <= 10);

                        var temporaryFolder = Path.Combine(GetTemporaryFolder(), Environment.UserName);
                        var file = uploader.GetObject(string.Format("NFE_{0}_{1}.pdf", numeroProcesso, item.ChaveNfe), temporaryFolder, numeroProcesso.ToString());

                        LogTfaHelper.Trace("End GetObject" + (file != null ? "Existe arquivo" : "Não existe arquivo"), numeroProcesso);

                        if (file == null)
                        {
                            if (!LogTfaHelper.responseMessage.ContainsKey(numeroProcesso.ToString()))
                            {
                                LogTfaHelper.responseMessage.Add(numeroProcesso.ToString(), new List<string>());
                            }

                            LogTfaHelper.responseMessage[numeroProcesso.ToString()].Add(string.Format("Arquivo NFE_{0}_{1}.pdf", numeroProcesso, item.ChaveNfe) + "não existe");
                            //INSERE TABELA LOG_TFA se deu aproblema com upload do arquivo
                            InsereLogUploadArquivo(_tfaService.ObterPorNumProcesso(numeroProcesso).Id, "NFE - " + item.ChaveNfe);
                        }    
                    }

                    //pdfsNfes.ForEach(p => uploader.UploadObject(new MemoryStream(p.Pdf), string.Format("NFE_{0}_{1}.pdf", numeroProcesso, p.ChaveNfe), numeroProcesso.ToString()));

                }
            }
            LogTfaHelper.Trace("End UploadNFE", numeroProcesso);

        }

        // Geração e Upload do Ticket no Processo TFA
        private void UploadTicket(int numeroProcesso, List<int> idsItensDespacho, List<string> listaCodigosVagoes)
        {
            LogTfaHelper.Trace("Start UploadTicket", numeroProcesso);

            AmazonUploader uploader = new AmazonUploader();

            /* Variaveis Ticket */
            List<string> notas = new List<string>();
            List<string> vagoesCriados = new List<string>();

            var vagoesItensDespacho = _relatorioDocumentosService.ObterVagoesCtesItensDespacho(idsItensDespacho, listaCodigosVagoes).ToList();

            var notasTickets = _relatorioDocumentosService.ObterTicketItensDespachoDto(idsItensDespacho, false);

            var notasTicketsClientes = _relatorioDocumentosService.ObterTicketClienteItensDespacho(idsItensDespacho, true);

            if (vagoesItensDespacho != null)
            {
                foreach (var vagaoItem in vagoesItensDespacho)
                {
                    if (!vagoesCriados.Any(vc => vc == vagaoItem.CodigoVagao))
                    {
                        if ((notasTickets != null) && (notasTickets.Count > 0))
                        {
                            notas =
                                notasTickets.ToList().Where(
                                    t => t.NotaTicketVagao.Vagao.CodigoVagao == vagaoItem.CodigoVagao).Select(
                                        t => t.NotaTicketVagao.ChaveNfe).Distinct().ToList();
                        }

                        if (notas.Any())
                        {
                            var dadosNotas = _relatorioDocumentosService.ObterNotasTickets(notas).ToList();

                            var notasTicketCliente = notasTicketsClientes.Where(t => t.IdItemDespacho == vagaoItem.IdItemDespacho).ToList();
                            var cliente = notasTicketCliente.FirstOrDefault();
                            notasTicketCliente.Clear();
                            notasTicketCliente.Add(cliente);

                            var notasTicket =
                                    notasTickets.Where(t => t.NotaTicketVagao.Vagao.CodigoVagao == vagaoItem.CodigoVagao)
                                        .Select(t => t.NotaTicketVagao)
                                        .GroupBy(t => t.ChaveNfe)
                                        .Select(t => t.First())
                                        .ToList();

                            var IdVagoes = notasTicket.Select(x => x.Vagao.CodigoVagao).Distinct().ToList();

                            // NOTE: no ascx ta pegando o peso formatado do vagao...debuga q vc vai ver...por isso a diferença de pesos
                            var htmlTicket = new System.Text.StringBuilder(string.Empty);
                            htmlTicket.Append("<div style='page-break-after: always;'>");
                            for (var index = 0; index < IdVagoes.Count; index++)
                            {
                                if (index % 4 == 0 && (index) != 0)
                                {
                                    htmlTicket.Append("</div><div style='page-break-after: always;'>");
                                }
                                var externalTicketTable = new System.Text.StringBuilder(@"
                                <table>
                                    <tr>
                                        <td>#LEFT_TABLE#</td>
                                        <td style='width: 50px'></td>
                                        <td style='width: 290px;'>#RIGHT_TABLE#</td>
                                    </tr>
                                </table>
                                <br />
                            ");
                                externalTicketTable.Replace("#LEFT_TABLE#", this.CreateHTMLInternalTicketTable(0, notasTicket, dadosNotas, notasTicketCliente));
                                index++;
                                externalTicketTable.Replace("#RIGHT_TABLE#", (index < IdVagoes.Count) ? this.CreateHTMLInternalTicketTable(0, notasTicket, dadosNotas, notasTicketCliente) : string.Empty);
                                htmlTicket.Append(externalTicketTable);
                            }
                            htmlTicket.Append("</div>");

                            var pdfTicket = _mdfeService.HtmlToPdf(htmlTicket.ToString(), "ticket", true);
                            uploader.UploadObject(pdfTicket, string.Format("TKT_{0}.pdf", numeroProcesso), numeroProcesso.ToString());

                            var temporaryFolder = Path.Combine(GetTemporaryFolder(), Environment.UserName);
                            var file = uploader.GetObject(string.Format("TKT_{0}.pdf", numeroProcesso), temporaryFolder, numeroProcesso.ToString());

                            LogTfaHelper.Trace("End GetObject" + (file != null ? "Existe arquivo" : "Não existe arquivo"), numeroProcesso);

                            if(file == null)
                            {
                                if(!LogTfaHelper.responseMessage.ContainsKey(numeroProcesso.ToString())){
                                    LogTfaHelper.responseMessage.Add(numeroProcesso.ToString(), new List<string>());
                                }

                                LogTfaHelper.responseMessage[numeroProcesso.ToString()].Add("Arquivo TKT não existe");
                                //INSERE TABELA LOG_TFA se deu aproblema com upload do arquivo
                                InsereLogUploadArquivo(_tfaService.ObterPorNumProcesso(numeroProcesso).Id, "TKT");
                            }  

                            LogTfaHelper.Trace("Tkt Uploaded", numeroProcesso);
                            vagoesCriados.Add(vagaoItem.CodigoVagao);
                        }
                    }
                }
            }

            LogTfaHelper.Trace("End UploadTicket", numeroProcesso);
        }

        private void InsereLogUploadArquivo(int idTfa, string nomeArquivo)
        {
            var logTfa = new LogTfa() {
                Id = 0,
                IdTfa = idTfa,
                Descricao = "UPLOAD - "+ nomeArquivo,
                VersionDate = DateTime.Now
            };

            _logTfaRepository.Inserir(logTfa);
        }

        private string CreateHTMLInternalTicketTable(int index, IList<NotaTicketVagao> notasTicket, IList<NotaTicketDto> dadosNotas, IList<NotaTicketVagaoClienteDto> notasTicketCliente)
        {
            string fileData = Tools.GetFromResources("Views.GestaoDocumentos.TemplateImpressaoTicket.htm");
            System.Text.StringBuilder buffer = new System.Text.StringBuilder(fileData);

            var IdVagoes = notasTicket.Select(x => x.Vagao.CodigoVagao).Distinct().ToList();
            var vagao = notasTicket.FirstOrDefault(x => x.Vagao.CodigoVagao == IdVagoes[index]).Vagao;
            var ticket = vagao.Ticket;
            var notaticket = notasTicket.FirstOrDefault(x => x.Vagao.CodigoVagao == IdVagoes[index]);
            var ticket_cliente = notasTicketCliente.FirstOrDefault(x => x.NotaTicketVagaoId == notaticket.Id);

            buffer.Replace("#TICKET_CLIENTE#", string.Format("{0}", ticket_cliente != null ? ticket_cliente.Cliente : "América Latina Logistica - ALL"));
            buffer.Replace("#TICKET_ORIGEM_DESC_RESUMIDA#", string.Format("{0}", ticket.Origem != null ? ticket.Origem.DescricaoResumida : string.Empty));
            buffer.Replace("#TICKET_ORIGEM_CGC_FORMAT#", ticket.Origem != null ? string.Format("{0}.{1}.{2}/{3}-{4}",
                ticket.Origem.Cgc.Substring(0, 2),
                ticket.Origem.Cgc.Substring(2, 3),
                ticket.Origem.Cgc.Substring(5, 3),
                ticket.Origem.Cgc.Substring(8, 4),
                ticket.Origem.Cgc.Substring(12, 2)) : string.Empty);
            buffer.Replace("#TICKET_DESTINO_DESC_RESUMIDA#", string.Format("{0}", ticket.Destino != null ? ticket.Destino.DescricaoResumida : string.Empty));
            buffer.Replace("#TICKET_DESTINO_CGC_FORMAT#", ticket.Destino != null ? string.Format("{0}.{1}.{2}/{3}-{4}",
                ticket.Destino.Cgc.Substring(0, 2),
                ticket.Destino.Cgc.Substring(2, 3),
                ticket.Destino.Cgc.Substring(5, 3),
                ticket.Destino.Cgc.Substring(8, 4),
                ticket.Destino.Cgc.Substring(12, 2)) : string.Empty);
            buffer.Replace("#TICKET_DATA_PESAGEM#", string.Format("{0}", ticket.DataPesagem));
            buffer.Replace("#TICKET_NUMERO_BALANCA#", string.Format("{0}", ticket.NumeroBalanca ?? String.Empty));
            buffer.Replace("#VAGAO_DESCRICAO#", !string.IsNullOrEmpty(vagao.SerieVagao) ? string.Format("{0} {1}", vagao.SerieVagao, vagao.CodigoVagao) : vagao.CodigoVagao);
            buffer.Replace("#PRODUTO_DESCRICAO#", dadosNotas.FirstOrDefault(n => notasTicket.FirstOrDefault(v => v.Vagao.Id == vagao.Id).ChaveNfe.Equals(n.ChaveNfe)).Produto);
            buffer.Replace("#VAGAO_PESO_BRUTO_FORMATADO#", string.Format("{0}", vagao.PesoBrutoFormatado));
            buffer.Replace("#VAGAO_PESO_TARA_FORMATADO#", string.Format("{0}", vagao.PesoTaraFormatado));
            buffer.Replace("#VAGAO_PESO_LIQUIDO_FORMATADO#", notasTicket.Count > 1 ? String.Format("{0:0.000}", notasTicket.Where(n => n.Vagao.CodigoVagao == vagao.CodigoVagao).Select(x => x.PesoRateio).Sum() / 1000) : vagao.PesoLiquidoFormatado);

            var notasFiscaisCarregadasTable = new System.Text.StringBuilder(string.Empty);
            foreach (var nota in notasTicket.Where(n => n.Vagao.CodigoVagao == vagao.CodigoVagao))
            {
                var dadosNfe = dadosNotas.FirstOrDefault(n => n.ChaveNfe.Equals(nota.ChaveNfe));
                string notasFiscaisCarregadas = string.Format("<tr><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td></tr>",
                    string.Format("{0}", dadosNfe != null ? dadosNfe.Serie : "----"), // #DADOS_NFE_SERIE#
                    string.Format("{0}", dadosNfe != null ? dadosNfe.Numero.ToString() : "----"), // #DADOS_NFE_NUMERO#
                    string.Format("{0}", nota.PesoRateioFormatado), // #NOTA_PESO_RATEIO_FORMATADO#
                    string.Format("{0}", dadosNfe != null ? (dadosNfe.PesoDisponivel > 0 ? dadosNfe.PesoDisponivel.ToString("N3") : "0,000") : "0") // "#DADOS_NFE_PESO_DISPONIVEL#
                );
                notasFiscaisCarregadasTable.Append(notasFiscaisCarregadas);
            }
            buffer.Replace("#NOTAS_FISCAIS_CARREGADAS_TABLE#", notasFiscaisCarregadasTable.ToString());

            buffer.Replace("#TICKET_OBSERVACAO#", string.Format("{0}", ticket.Observacao));
            buffer.Replace("#TICKET_RESPONSAVEL#", string.Format("{0}", ticket.Responsavel));
            buffer.Replace("#DATA_REIMPRESSAO#", string.Format("{0}", DateTime.Now.ToString()));
            return buffer.ToString();
        }
    }
}