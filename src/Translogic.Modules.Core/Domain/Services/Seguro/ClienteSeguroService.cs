﻿using System.Collections.Generic;
using Translogic.Modules.Core.Domain.Model.Seguro;
using Translogic.Modules.Core.Domain.Model.Seguro.Repositories;

namespace Translogic.Modules.Core.Domain.Services.Seguro
{
	public class ClienteSeguroService
	{
		private readonly IClienteSeguroRepository _clienteSeguroRepository;

		public ClienteSeguroService(IClienteSeguroRepository clienteSeguroRepository)
		{
			_clienteSeguroRepository = clienteSeguroRepository;
		}

        /// <summary>
        ///   Obtem o cliente seguro pela sigla
        /// </summary>
        /// <returns>Cliente seguro</returns>
        public ClienteSeguro ObterClientePorSigla(string sigla)
        {
            return _clienteSeguroRepository.ObterClientePorSigla(sigla);
		}
	}
}