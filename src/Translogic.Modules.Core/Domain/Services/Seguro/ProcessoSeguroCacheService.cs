﻿using System;
using System.Collections.Generic;
using System.Linq;
using ALL.Core.Dominio;
using Translogic.Core.Infrastructure.Web;
using Translogic.Modules.Core.Domain.Model.Acesso;
using Translogic.Modules.Core.Domain.Model.Dto;
using Translogic.Modules.Core.Domain.Model.Seguro;
using Translogic.Modules.Core.Domain.Model.Seguro.Repositories;

namespace Translogic.Modules.Core.Domain.Services.Seguro
{
	public class ProcessoSeguroCacheService
	{
		private readonly IProcessoSeguroCacheRepository _processoSeguroCacheRepository;

		public ProcessoSeguroCacheService(IProcessoSeguroCacheRepository processoSeguroCacheRepository)
		{
			_processoSeguroCacheRepository = processoSeguroCacheRepository;
		}

		/// <summary>
		///   Retorna todos os processos em cache do usuário paginado
		/// </summary>
		/// <param name="pagination"> Paginacao pesquisa</param>
		/// <param name="filtros"> Filtros para pesquisa</param>
		/// <param name="usuario">Usuário logado</param>
		/// <returns>lista de processos em cache</returns>
		public ResultadoPaginado<ProcessoSeguroCacheDto> ObterProcessosSeguroCacheUsuario(DetalhesPaginacaoWeb pagination,
			DetalhesFiltro<object>[] filtros, Usuario usuario)
		{
			var listaProcessoSeguroCache = new ResultadoPaginado<ProcessoSeguroCacheDto>();

			listaProcessoSeguroCache = _processoSeguroCacheRepository.ObterProcessosSeguroCacheUsuario(pagination, usuario);
			listaProcessoSeguroCache.Items = listaProcessoSeguroCache.Items.OrderBy(p => p.ProcessoSeguroCacheId).ToList();

			return listaProcessoSeguroCache;
		}

		/// <summary>
		///   Retorna todos os processos em cache do usuário
		/// </summary>
		/// <param name="usuario">Usuário logado</param>
		/// <returns>lista de processos em cache</returns>
		public IList<ProcessoSeguroCache> ObterPorUsuario(Usuario usuario)
		{
			return _processoSeguroCacheRepository.ObterPorUsuario(usuario);
		}

        /// <summary>
        ///   Retorna se o laudo já está cadastrado para o despacho e série informados
        /// </summary>
        /// <param name="despacho">Despacho</param>
        /// <param name="serie">Série</param>
        /// <returns>True para cadastrado, False para não cadastrado</returns>
        public bool ExisteDespachoSerie(int despacho, string serie)
        {
            return _processoSeguroCacheRepository.ExisteDespachoSerie(despacho, serie);
        }

		/// <summary>
		///   Remove determinado processo da tabela de cache
		/// </summary>
		/// <param name="processoSeguroCache">Processo a ser Removido</param>
		/// <returns>Retorna se foi possível remover o processo</returns>/// 
		public bool RemoverProcessoSeguroCache(ProcessoSeguroCache processoSeguroCache)
		{
			return _processoSeguroCacheRepository.RemoverProcessoSeguroCache(processoSeguroCache);
		}

		/// <summary>
		///   Remove todos os processos em cache do usuário
		/// </summary>
		/// <param name="usuario">Usuário</param>
		/// <returns>Retorna se foi possível remover o processo</returns>/// 
		public bool RemoverPorUsuario(Usuario usuario)
		{
			return _processoSeguroCacheRepository.RemoverPorUsuario(usuario);
		}

		/// <summary>
		///   Salva o processo seguro em cache do usuário
		/// </summary>
		/// <param name="processoSeguroCache">Objeto a ser salvo</param>
		/// <returns></returns>
		public ProcessoSeguroCache Salvar(ProcessoSeguroCache processoSeguroCache)
		{
			return _processoSeguroCacheRepository.Salvar(processoSeguroCache);
		}

	    /// <summary>
	    ///   Obtem Processo Seguro em Cache pelo Id
	    /// </summary>
	    /// <param name="id">Id do Processo em Cache</param>
        /// <returns>Processo Seguro em Cache</returns>
	    public ProcessoSeguroCache ObterPorId(int id)
        {
            return _processoSeguroCacheRepository.ObterPorId(id);
        }
	}
}