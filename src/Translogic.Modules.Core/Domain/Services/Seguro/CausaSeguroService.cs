﻿using System.Collections.Generic;
using Translogic.Modules.Core.Domain.Model.Seguro;
using Translogic.Modules.Core.Domain.Model.Seguro.Repositories;
using Translogic.Modules.Core.Domain.Model.Dto;

namespace Translogic.Modules.Core.Domain.Services.Seguro
{
	public class CausaSeguroService
	{
		private readonly ICausaSeguroRepository _causaSeguroRepository;

		public CausaSeguroService(ICausaSeguroRepository causaSeguroRepository)
		{
			_causaSeguroRepository = causaSeguroRepository;
		}

		/// <summary>
		///   Obtem a lista enumerada de causas
		/// </summary>
		/// <returns>Lista enumerada de Causas</returns>
		public IEnumerable<CausaSeguro> ObterCausas()
		{
			return _causaSeguroRepository.ObterCausas();
		}

        public IList<CausaSeguroDto> ObterCausasTipoCausaV()
        {
            return _causaSeguroRepository.ObterCausasTipoCausaV();
        }
	}
}