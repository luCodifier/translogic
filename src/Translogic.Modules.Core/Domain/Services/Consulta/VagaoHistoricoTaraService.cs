﻿using System.Globalization;
using System.Text;

namespace Translogic.Modules.Core.Domain.Services.Consulta
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using ALL.Core.Dominio;
    using Translogic.Core.Infrastructure.Web;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Modules.Core.Domain.Model.Consultas.Repositories;

    public class VagaoHistoricoTaraService
    {
        private readonly IVagaoHistoricoTaraRepository _vagaoHistoricoTaraRepository;

        /// <summary>
        ///  Initializes a new instance of the <see cref="VagaoHistoricoTaraService"/> class.
        /// </summary>
        /// <param name="vagaoHistoricoTaraRepository">Repositório de Motivo Situacao Vagao</param>
        public VagaoHistoricoTaraService(IVagaoHistoricoTaraRepository vagaoHistoricoTaraRepository)
        {
            this._vagaoHistoricoTaraRepository = vagaoHistoricoTaraRepository;
        }

        /*public ResultadoPaginado<VagaoHistoricoTaraDto> ObterHistoricoTaras(DetalhesPaginacaoWeb pagination, DetalhesFiltro<object>[] filtros, string dataInicial, string dataFinal)
        {
            string serie = string.Empty;
            String vagoes = string.Empty;
            List<string> vagoesSL = new List<string>();
            string dataIni = string.Empty;
            string dataFim = string.Empty;

            if (!string.IsNullOrEmpty(dataInicial) && dataInicial != "__/__/____")
            {
                dataIni = dataInicial;
            }

            if (!string.IsNullOrEmpty(dataFinal) && dataFinal != "__/__/____")
            {
                dataFim = dataFinal;
            }

            foreach (var detalheFiltro in filtros)
            {
                if (detalheFiltro.Campo.Equals("vagoes") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                {
                    vagoes = detalheFiltro.Valor[0].ToString().ToUpperInvariant();

                    var vagoesArray = vagoes.Split(',');

                    foreach (var vagao in vagoesArray)
                    {
                        vagoesSL.Add("'" + vagao + "'");
                    }

                    vagoes = string.Empty;
                    foreach (var vagaoFinal in vagoesSL)
                    {
                        vagoes += vagaoFinal + ",";
                    }
                    vagoes = vagoes.Remove(vagoes.Length - 1);
                }

                if (detalheFiltro.Campo.Equals("serie") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                {
                    serie = "'" + detalheFiltro.Valor[0].ToString() + "'";
                }

                //if (detalheFiltro.Campo.Equals("dataFinal") && !string.IsNullOrEmpty(detalheFiltro.Valor[0].ToString()))
                //{
                //    string texto = detalheFiltro.Valor[0].ToString();

                //    dataFim = DateTime.ParseExact(texto, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);

                //    dataFim = dataFim.AddDays(1).AddSeconds(-1);
                //}
            }            
            return _vagaoHistoricoTaraRepository.ObterHistoricoTaras(pagination, vagoes, serie, dataIni, dataFim);
        }*/

        public ResultadoPaginado<VagaoHistoricoTaraDto> ObterHistoricoTaras(DetalhesPaginacaoWeb pagination, string vagoes, string serie, string dataInicial, string dataFinal)
        {
            string dataIni = string.Empty;
            string dataFim = string.Empty;

            if (!string.IsNullOrEmpty(dataInicial) && dataInicial != "__/__/____")
            {
                dataIni = dataInicial;
            }

            if (!string.IsNullOrEmpty(dataFinal) && dataFinal != "__/__/____")
            {
                dataFim = dataFinal;
            }

            if (!string.IsNullOrWhiteSpace(vagoes))
                vagoes = FormataParametroArray(vagoes);
            if (!string.IsNullOrWhiteSpace(serie))
                serie = FormataParametroStringArray(serie);

            return _vagaoHistoricoTaraRepository.ObterHistoricoTaras(pagination, vagoes, serie, dataIni, dataFim);
        }

        public IEnumerable<VagaoHistoricoTaraDto> ExportarHistoricoTara(string vagao, string serie, string dataInicial, string dataFinal)
        {
            /*string serie = string.Empty;
            String vagoes = string.Empty;
            List<string> vagoesSL = new List<string>();*/
            string dataIni = "";
            string dataFim = "";

            if (!string.IsNullOrEmpty(dataInicial) && dataInicial != "__/__/____")
            {
                dataIni = dataInicial;
            }

            if (!string.IsNullOrEmpty(dataFinal) && dataFinal != "__/__/____")
            {
                dataFim = dataFinal;
            }

            if (!string.IsNullOrWhiteSpace(vagao))
                vagao = FormataParametroArray(vagao);
            if (!string.IsNullOrWhiteSpace(serie))
                serie = FormataParametroStringArray(serie);

            return _vagaoHistoricoTaraRepository.ExportarHistoricoTaras(vagao, serie, dataIni, dataFim);
        }

        public string FormataParametroArray(string parametro)
        {
            var splitParametro = parametro.Split(',');
            var retorno = "";
            foreach (var valor in splitParametro)
            {
                if (!string.IsNullOrWhiteSpace(valor))
                    retorno += valor + ",";
            }

            return retorno.Length > 0 ? retorno.Substring(0, retorno.LastIndexOf(",")) : string.Empty;
        }

        public string FormataParametroStringArray(string parametro)
        {
            var splitParametro = parametro.Split(',');
            var retorno = "";
            foreach (var valor in splitParametro)
            {
                if (!string.IsNullOrWhiteSpace(valor))
                    retorno += "'" + valor + "'" + ",";
            }

            return retorno.Length > 0 ? retorno.Substring(0, retorno.LastIndexOf(",")) : string.Empty;
        }
    }
}