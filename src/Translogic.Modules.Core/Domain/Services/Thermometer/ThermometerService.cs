﻿namespace Translogic.Modules.Core.Domain.Services.Thermometer
{
    using System;
    using System.Collections.Generic;
    using Model.Thermometer;
    using Model.Thermometer.Repositories;

    /// <summary>Classe de serviços de termometros</summary>
    public class ThermometerService
    {
        private readonly IThermometerRepository _thermometerRepository;
        private readonly IReadThermometerRepository _readThermometerRepository;

        /// <summary>Contrutor da Classe Termometro service</summary>
        /// <param name="thermometerRepository">The thermometer Repository.</param>
        /// <param name="readThermometerRepository">The IReadThermometerRepository Repository.</param>
        public ThermometerService(IThermometerRepository thermometerRepository, IReadThermometerRepository readThermometerRepository)
        {
            _thermometerRepository = thermometerRepository;
            _readThermometerRepository = readThermometerRepository;
        }

        /// <summary>Método para obter as estações</summary>
        /// <returns>retorna lista de estações</returns>
        public IList<Thermometer> ObterTermometros()
        {
            return _thermometerRepository.ObterTodos();
        }

        /// <summary>Método para obter medições</summary>
        /// <param name="idEstacao">Id da estação</param>
        /// <param name="dataInicial">Data Inicial de pesquisa</param>
        /// <param name="dataFinal">Data Final de pesquisa</param>
        /// <returns> retorna lista de medições </returns>
        public Thermometer ObterMedicoesTermometros(int idEstacao, DateTime dataInicial, DateTime dataFinal)
        {
            return _thermometerRepository.ObterMedicoesTermometros(idEstacao, dataInicial, dataFinal);
        }
    }
}