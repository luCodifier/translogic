﻿
namespace Translogic.Modules.Core.Domain.Services.LocaisFornecedor.Interface
{
    using System.Collections.Generic;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Modules.Core.Domain.Model.LocaisFornecedor;
    using Translogic.Modules.Core.Util;
    using ALL.Core.Dominio;
    using Translogic.Core.Infrastructure.Web;

    public interface ILocalFornecedorService
    {        
        ResultadoPaginado<LocalFornecedorDto> ObterLocaisFornecedorPorId(DetalhesPaginacaoWeb detalhesPaginacaoWeb, int idLocal);

        //bool VerificarExisteFornecedorLocalServico(int idFornecedor, int idLocal, int idServico);

        /// <summary>
        /// Insere ou atualiza local fornecedor
        /// </summary>
        /// <param name="estacaoServicos">Lista EstacaoServicoDto</param>
        void InserirAtualizar(IList<LocalTipoServicoFornecedorDto> estacaoServicos);
    }
}
