﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ALL.Core.Dominio;
using Translogic.Core.Infrastructure.Web;
using Translogic.Modules.Core.Domain.Model.LocaisFornecedor;
using Translogic.Modules.Core.Domain.Services.LocaisFornecedor.Interface;
using Translogic.Modules.Core.Domain.Services.TipoServico.Interface;
using Translogic.Modules.Core.Domain.Model.LocaisFornecedor.Repositories;
using Translogic.Modules.Core.Domain.Model.TipoServicos.Repositories;
using Translogic.Modules.Core.Domain.Model.Via.Repositories;
using Translogic.Modules.Core.Domain.Model.Dto;
using Translogic.Modules.Core.Domain.Model.Fornecedor.Repositories;
using Translogic.Modules.Core.Domain.Model.Via;

namespace Translogic.Modules.Core.Domain.Services.LocaisFornecedor
{
    public class LocalFornecedorService : ILocalFornecedorService
    {
        private readonly ILocalFornecedorRepository _LocalFornecedorRepository;      
        

        public LocalFornecedorService(ILocalFornecedorRepository LocalFornecedorRepository)
        {
            _LocalFornecedorRepository = LocalFornecedorRepository;
           
        }

        //public IList<LocalFornedorDto> ObterLocaisFornecedor()
        //{
        //    return _LocalFornecedorRepository.ObterLocaisFornencedor();
        //}

        public ResultadoPaginado<LocalFornecedorDto> ObterConsultaLocaisFornecedor(DetalhesPaginacaoWeb detalhesPaginacaoWeb, int idLocal)
        {
            return _LocalFornecedorRepository.ObterLocaisFornecedorPorId(detalhesPaginacaoWeb, idLocal);
        }       

        public ResultadoPaginado<LocalFornecedorDto> ObterLocaisFornecedorPorId(DetalhesPaginacaoWeb detalhesPaginacaoWeb, int idLocal)
        {
            return _LocalFornecedorRepository.ObterLocaisFornecedorPorId(detalhesPaginacaoWeb, idLocal);
        }

        //public bool VerificarExisteFornecedorLocalServico(int idFornecedor, int idLocal, int idServico)
        //{
        //    return _LocalFornecedorRepository.VerificarExisteFornecedorLocalServico(idFornecedor, idLocal, idServico);
        //}
        
        public void InserirAtualizar(IList<LocalTipoServicoFornecedorDto> estacaoServicos)
        {
            var locaisFornecedor = AutoMapper.Mapper.Map<IList<LocalTipoServicoFornecedorDto>, IList<LocalFornecedor>>(estacaoServicos);
            if (locaisFornecedor[0].Id>0)
            {
                _LocalFornecedorRepository.Inserir(locaisFornecedor);
            }            
        }

        public LocalFornecedorDto ObterLocalFornecedorPorIds(int idFornecedorOs, int idTipoServico, int idLocal)
        {
            return _LocalFornecedorRepository.ObterLocalFornecedorPorIds(idFornecedorOs, idTipoServico, idLocal);
        }
    }
}