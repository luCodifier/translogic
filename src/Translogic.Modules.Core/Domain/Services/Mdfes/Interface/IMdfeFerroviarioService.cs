﻿namespace Translogic.Modules.Core.Domain.Services.Mdfes.Interface
{
    using System.ServiceModel;
    using Model.WebServices.MdfeFerroviario;

    /// <summary>
    /// Interface do Mdfe Ferroviario Service 
    /// </summary>
    [ServiceContract]
    public interface IMdfeFerroviarioService
    {
        /// <summary>
        /// Contrato do service
        /// </summary>
        /// <param name="request">Objeto de input EletronicManifestInformationRailRequest</param>
        /// <returns>objeto de output EletronicManifestInformationRailResponse</returns>
        [OperationContract]
        EletronicManifestInformationRailResponse InformationMdfe(EletronicManifestInformationRailRequest request);
    }
}