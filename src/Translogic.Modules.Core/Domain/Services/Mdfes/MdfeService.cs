﻿namespace Translogic.Modules.Core.Domain.Services.Mdfes
{
    using System;
    using System.Collections.Generic;
    using System.EnterpriseServices;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Web.Mvc;
    using ALL.Core.Dominio;
    using ICSharpCode.SharpZipLib.Core;
    using iTextSharp.text;
    using iTextSharp.text.pdf;
    using iTextSharp.tool.xml;
    using iTextSharp.tool.xml.html;
    using iTextSharp.tool.xml.parser;
    using iTextSharp.tool.xml.pipeline.css;
    using iTextSharp.tool.xml.pipeline.end;
    using iTextSharp.tool.xml.pipeline.html;
    using Model.Acesso;
    using Model.Acesso.Repositories;
    using Model.Codificador;
    using Model.Codificador.Repositories;
    using Model.Diversos.Mdfe;
    using Model.FluxosComerciais.Mdfes;
    using Model.FluxosComerciais.Mdfes.Repositories;
    using Translogic.Core.Infrastructure;
    using Translogic.Modules.Core.Domain.Model.Diversos;
    using Translogic.Modules.Core.Domain.Model.Diversos.Cte;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Modules.Core.Domain.Model.Estrutura;
    using Translogic.Modules.Core.Domain.Model.Estrutura.Repositories;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes.Repositories.TriangulusConfig;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes.TriangulusConfig;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Repositories;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.TriangulusConfig;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.TriangulusConfig.Repositories;
    using Translogic.Modules.Core.Domain.Model.Trem;
    using Translogic.Modules.Core.Domain.Model.Trem.Repositories;
    using Translogic.Modules.Core.Domain.Model.Via;
    using Translogic.Modules.Core.Domain.Model.Via.Repositories;
    using Translogic.Modules.Core.Domain.Services.GestaoDocumentos;
    using Translogic.Modules.Core.Domain.Model.PainelExpedicao.Documentos.Dto;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.DocumentosVagao;
    using Translogic.Modules.Core.Spd.BLL;
    using Spd_Data = Translogic.Modules.Core.Spd.Data;

    /// <summary>
    /// Classe de serviço do MDF-e
    /// </summary>
    [Transaction]
    public class MdfeService
    {
        private readonly IMdfeRepository _mdfeRepository;
        private readonly IMdfeComposicaoRepository _mdfeComposicaoRepository;
        private readonly IMdfeEnvioPoolingRepository _mdfeEnvioPoolingRepository;
        private readonly MdfeLogService _mdfeLogService;
        private readonly IConfiguracaoTranslogicRepository _configuracaoTranslogicRepository;
        private readonly IUsuarioRepository _usuarioRepository;
        private readonly IMdfeStatusRepository _mdfeStatusRepository;
        private readonly ILogImpressaoReciboRepository _logImpressaoReciboRepository;
        private readonly FilaProcessamentoMdfeService _filaProcessamentoMdfeService;
        private readonly IMdfeInterfaceRecebimentoConfigRepository _mdfeInterfaceRecebimentoConfigRepository;
        private readonly IMdfeRecebimentoPoolingRepository _mdfeRecebimentoPoolingRepository;
        private readonly IMdfeStatusRetornoRepository _mdfeStatusRetornoRepository;
        private readonly IMdfe01ListaRepository _mdfe01ListaRepository;
        private readonly IMdfeArvoreRepository _mdfeArvoreRepository;
        private readonly IMdfeInterfacePdfConfigRepository _mdfeInterfacePdfConfigRepository;
        private readonly IMdfeArquivoRepository _mdfeArquivoRepository;
        private readonly INfe03FilialRepository _empresaRepository;
        private readonly IEmpresaClienteRepository _empClienteRepository;
        private readonly ICteEmpresasRepository _cteEmpresasRepository;
        private readonly ITremRepository _tremRepository;
        private readonly IMercadoriaRepository _mercadoriaRepository;
        private readonly IElementoViaRepository _elementoViaRepository;
        private readonly IAreaOperacionalRepository _areaOperacionalRepository;
        private readonly RelatorioDocumentosService _relatorioDocumentosService;

        private readonly string _chaveMdfeRobo = "MDFE_USUARIO_ROBO";
        private readonly string _chaveMdfeTimer = "MDFE_TIMER";

        /// <summary>
        /// Initializes a new instance of the <see cref="MdfeService"/> class.
        /// </summary>
        /// <param name="mdfeComposicaoRepository">Repositório da composição do MDF-e injetado</param>
        /// <param name="mdfeRepository">Repositório do MDF-e injetado</param>
        /// <param name="mdfeLogService">Serviço do loge do MDF-e</param>
        /// <param name="mdfeEnvioPoolingRepository">Repositório de envio do pooling do MDF-e injetado</param>
        /// <param name="configuracaoTranslogicRepository">Repositório da configuração do Translogic injetado</param>
        /// <param name="usuarioRepository">Repositório do usuário do Translogic injetado</param>
        /// <param name="mdfeStatusRepository">Repositório do status do MDF-e injetado</param>
        /// <param name="filaProcessamentoMdfeService">Serviço da fila de processamento dos MDF-es</param>
        /// <param name="mdfeInterfaceRecebimentoConfigRepository">Repositório da interface de recebimento da Config injetado</param>
        /// <param name="mdfeRecebimentoPoolingRepository">Repositório do recebimento pooling injetado</param>
        /// <param name="mdfeStatusRetornoRepository">Repositório do status de retorno do MDF-e injetado</param>
        /// <param name="mdfe01ListaRepository">Repositório do mdfe01ListaRepository injetado</param>
        /// <param name="mdfeArvoreRepository">Repositório do mdfeArvoreRepository injetado</param>
        /// <param name="mdfeInterfacePdfConfigRepository">Repositório do mdfeArvoreRepository injetado.</param>
        /// <param name="mdfeArquivoRepository">Repositório do MdfeArquivoRepository injetado.</param>
        /// <param name="empresaRepository">Repositório do empresaRepository injetado.</param>
        /// <param name="cteEmpresasRepository">Repositório do cteEmpresasRepository injetado.</param>
        /// <param name="tremRepository">Repositório do tremRepository injetado.</param>
        /// <param name="mercadoriaRepository">Repositório da mercadoriaRepository injetado.</param>
        /// <param name="elementoViaRepository">Repositório da elemento via injetado.</param>
        /// <param name="areaOperacionalRepository">Repositório da area operacional injetado.</param>
        /// <param name="empRepository">Repositório de empresas</param>
        /// <param name="logImpressaoReciboRepository">Repositório de logs de impressão</param>
        /// <param name="relatorioDocumentosService">Serviço de geração de relatórios PDF</param>
        public MdfeService(IMdfeComposicaoRepository mdfeComposicaoRepository,
                           IMdfeRepository mdfeRepository,
                           MdfeLogService mdfeLogService,
                           IMdfeEnvioPoolingRepository mdfeEnvioPoolingRepository,
                           IConfiguracaoTranslogicRepository configuracaoTranslogicRepository,
                           IUsuarioRepository usuarioRepository,
                           IMdfeStatusRepository mdfeStatusRepository,
                           FilaProcessamentoMdfeService filaProcessamentoMdfeService,
                           IMdfeInterfaceRecebimentoConfigRepository mdfeInterfaceRecebimentoConfigRepository,
                           IMdfeRecebimentoPoolingRepository mdfeRecebimentoPoolingRepository,
                           IMdfeStatusRetornoRepository mdfeStatusRetornoRepository,
                           IMdfe01ListaRepository mdfe01ListaRepository,
                           IMdfeArvoreRepository mdfeArvoreRepository,
                           IMdfeInterfacePdfConfigRepository mdfeInterfacePdfConfigRepository,
                           IMdfeArquivoRepository mdfeArquivoRepository,
                           INfe03FilialRepository empresaRepository,
                           ICteEmpresasRepository cteEmpresasRepository,
                           ITremRepository tremRepository,
                           IMercadoriaRepository mercadoriaRepository,
                           IElementoViaRepository elementoViaRepository,
                           IAreaOperacionalRepository areaOperacionalRepository,
                           IEmpresaClienteRepository empRepository,
                           ILogImpressaoReciboRepository logImpressaoReciboRepository,
                           RelatorioDocumentosService relatorioDocumentosService)
        {
            _mdfeComposicaoRepository = mdfeComposicaoRepository;
            _mdfeArquivoRepository = mdfeArquivoRepository;
            _empresaRepository = empresaRepository;
            _cteEmpresasRepository = cteEmpresasRepository;
            _tremRepository = tremRepository;
            _mercadoriaRepository = mercadoriaRepository;
            _elementoViaRepository = elementoViaRepository;
            _areaOperacionalRepository = areaOperacionalRepository;
            _mdfeInterfacePdfConfigRepository = mdfeInterfacePdfConfigRepository;
            _mdfeArvoreRepository = mdfeArvoreRepository;
            _mdfe01ListaRepository = mdfe01ListaRepository;
            _mdfeRepository = mdfeRepository;
            _mdfeLogService = mdfeLogService;
            _mdfeEnvioPoolingRepository = mdfeEnvioPoolingRepository;
            _configuracaoTranslogicRepository = configuracaoTranslogicRepository;
            _usuarioRepository = usuarioRepository;
            _mdfeStatusRepository = mdfeStatusRepository;
            _filaProcessamentoMdfeService = filaProcessamentoMdfeService;
            _mdfeInterfaceRecebimentoConfigRepository = mdfeInterfaceRecebimentoConfigRepository;
            _mdfeRecebimentoPoolingRepository = mdfeRecebimentoPoolingRepository;
            _mdfeStatusRetornoRepository = mdfeStatusRetornoRepository;
            _empClienteRepository = empRepository;
            _logImpressaoReciboRepository = logImpressaoReciboRepository;
            _relatorioDocumentosService = relatorioDocumentosService;
        }

        /// <summary>
        /// Obtém a Lista de Ctes serem processados para envio
        /// </summary>
        /// <param name="indAmbienteSefaz">Indica qual ambiente do SEFAZ</param>
        /// <returns>Lista dos MDF-es</returns> 
        public IList<Mdfe> ObterMdfesProcessarEnvio(int indAmbienteSefaz)
        {
            try
            {
                return _mdfeRepository.ObterStatusProcessamentoEnvio(indAmbienteSefaz);
            }
            catch (Exception ex)
            {
                _mdfeLogService.InserirLogErro(null, "MdfeService", string.Format("ObterMdfesProcessarEnvio: {0}", ex.Message));
                throw;
            }
        }

        /// <summary>
        /// Insere os mdfes da lista no pool de processamento do servidor
        /// </summary>
        /// <param name="listaMdfesNaoProcessados">Lista de mdfes</param>
        /// <param name="hostName">Ip do Servidor</param>
        public void InserirPoolingEnvioMdfe(IList<Mdfe> listaMdfesNaoProcessados, string hostName)
        {
            foreach (Mdfe mdfeAux in listaMdfesNaoProcessados)
            {
                Mdfe mdfe = mdfeAux;
                try
                {
                    MdfeEnvioPooling mep = new MdfeEnvioPooling();
                    mep.Id = mdfe.Id;
                    mep.DataHora = DateTime.Now;
                    mep.HostName = hostName;

                    Usuario usuario = ObterUsuarioRobo();

                    // Insere o mdfe no pooling
                    _mdfeEnvioPoolingRepository.Inserir(mep);

                    switch (mdfe.SituacaoAtual)
                    {
                        case SituacaoMdfeEnum.PendenteArquivoEnvio:
                            mdfe.SituacaoAtual = SituacaoMdfeEnum.EnviadoFilaArquivoEnvio;
                            // Insere no log do mdfe
                            _mdfeLogService.InserirLogInfo(mdfe, "MdfeService", "Adicionado o arquivo de envio no pooling");

                            // Insere o status do mdfe
                            _mdfeStatusRepository.InserirStatus(mdfe, usuario, new MdfeStatusRetorno { Id = 21 });
                            break;
                        case SituacaoMdfeEnum.PendenteArquivoCancelamento:
                        case SituacaoMdfeEnum.AguardandoEncerramento:
                            mdfe.SituacaoAtual = SituacaoMdfeEnum.EnviadoFilaArquivoEncerramento;

                            // Insere o status do mdfe
                            _mdfeStatusRepository.InserirStatus(mdfe, usuario, new MdfeStatusRetorno { Id = 28 });

                            // Insere no log do mdfe
                            _mdfeLogService.InserirLogInfo(mdfe, "MdfeService", "Adicionado o arquivo de encerramento no pooling");
                            break;

                        case SituacaoMdfeEnum.AguardandoCancelamento:

                            mdfe.SituacaoAtual = SituacaoMdfeEnum.EnviadoFilaArquivoCancelamento;
                            // Insere o status do mdfe
                            _mdfeStatusRepository.InserirStatus(mdfe, usuario, new MdfeStatusRetorno { Id = 22 });

                            // Insere no log do mdfe
                            _mdfeLogService.InserirLogInfo(mdfe, "MdfeService", "Adicionado o arquivo de cancelamento no pooling");
                            break;
                    }

                    // Altera o status do MDF-e
                    _mdfeRepository.AtualizarSituacao(mdfe);
                }
                catch (Exception ex)
                {
                    // Insere no log do mdfe
                    _mdfeLogService.InserirLogErro(mdfe, "MdfeService", string.Format("InserirPoolingEnvioMdfe: erro para inserir no pooling - {0}", ex.Message));

                    // Limpa a sessão para processar o próximo
                    _mdfeEnvioPoolingRepository.ClearSession();
                }
            }
        }

        /// <summary>
        /// Obtem o usuário do Robo
        /// </summary>
        /// <returns>Retorna o usuário do Robo</returns>
        public Usuario ObterUsuarioRobo()
        {
            ConfiguracaoTranslogic usuarioRobo = _configuracaoTranslogicRepository.ObterPorId(_chaveMdfeRobo);
            return _usuarioRepository.ObterPorId(Convert.ToInt32(usuarioRobo.Valor));
        }

        /// <summary>
        /// Obtém os MDF-es do pool pelo ip do servStatusProcessamentoEnvio
        /// </summary>
        /// <param name="hostName">Nome do host</param>
        /// <returns>Lista dos MDF-es</returns>
        public IList<Mdfe> ObterPoolingEnvioPorServidor(string hostName)
        {
            try
            {
                int segundos = 0;
                ConfiguracaoTranslogic timer = _configuracaoTranslogicRepository.ObterPorId(_chaveMdfeTimer);
                int.TryParse(timer.Valor, out segundos);
                return _mdfeRepository.ObterListaEnvioPorHost(hostName, segundos);
            }
            catch (Exception ex)
            {
                _mdfeLogService.InserirLogErro(null, "MdfeService", string.Format("ObterPoolingEnvioPorServidor({0}): {1}", hostName, ex.Message));
                throw;
            }
        }

        /// <summary>
        /// Remove os MDF-es do pooling
        /// </summary>
        /// <param name="lista">Lista dos MDF-es processados</param>
        public void RemoverPoolingEnvioMdfe(IList<Mdfe> lista)
        {
            _mdfeEnvioPoolingRepository.RemoverPorLista(lista);
        }

        /// <summary>
        /// Muda a situação do MDF-e
        /// </summary>
        /// <param name="mdfe">MDF-e que será mudado a situação</param>
        /// <param name="situacao">Nova situação do MDF-e</param>
        /// <param name="usuario">Usuário que está mudando a situação</param>
        /// <param name="statusRetorno">Status de retorno que será logado</param>
        /// <param name="mensagemLog">Mensagem que será armazenado no log</param>
        /// <param name="mensagemStatusRetorno">Mensagem  do status de retorno</param>
        public void MudarSituacaoMdfe(Mdfe mdfe, SituacaoMdfeEnum situacao, Usuario usuario, MdfeStatusRetorno statusRetorno, string mensagemLog, string mensagemStatusRetorno)
        {
            mdfe = _mdfeRepository.ObterPorId(mdfe.Id);
            mdfe.SituacaoAtual = situacao;
            _mdfeRepository.Atualizar(mdfe);

            if (usuario == null)
            {
                // Pega o usuario default da conf geral
                usuario = ObterUsuarioRobo();
            }

            _mdfeStatusRepository.InserirStatus(mdfe, usuario, statusRetorno, mensagemStatusRetorno);

            // Insere no log do MDF-e
            _mdfeLogService.InserirLogInfo(mdfe, "MdfeService", string.Format("MudarSituacaoMdfe: {0}", mensagemLog));
        }

        /// <summary>
        /// Insere o arquivo do MDF-e na fila de processamento da Config
        /// </summary>
        /// <param name="mdfe">MDF-e que será inserido na fila de retorno da Config</param>
        public void InserirMdfeEnvioFilaProcessamentoConfig(Mdfe mdfe)
        {
            try
            {
                int idListaConfig = 0;

                // Insere na fila de processamento da Config o MDF-e
                idListaConfig = _filaProcessamentoMdfeService.InserirMdfeEnvioFilaProcessamento(mdfe);

                // Gera o PDF pela impressão
                _filaProcessamentoMdfeService.InserirGeracaoPdfPorImpressao(mdfe);

                // Insere no pooling de recebimento (insere na tabela informando que foi colocado na fila da Config o MDF-e)
                InserirInterfaceRecebimentoConfig(mdfe, idListaConfig);

                // Altera o status do MDF-e
                mdfe.SituacaoAtual = SituacaoMdfeEnum.EnviadoArquivoEnvio;
                _mdfeRepository.Atualizar(mdfe);

                // Pega o usuario default da conf geral
                Usuario usuario = ObterUsuarioRobo();

                // Insere o status do MDF-e
                _mdfeStatusRepository.InserirStatus(mdfe, usuario, new MdfeStatusRetorno { Id = 11 });

                // Insere no log do MDF-e
                _mdfeLogService.InserirLogInfo(mdfe, "MdfeService", "Mdfe enviado para a Config");
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Insere o arquivo do MDF-e na fila de processamento da Config
        /// </summary>
        /// <param name="mdfe">MDF-e que será inserido na fila de retorno da Config</param>
        public void InserirMdfeCancelamentoFilaProcessamentoConfig(Mdfe mdfe)
        {
            try
            {
                int idListaConfig = 0;
                // Insere na fila de processamento da config
                idListaConfig = _filaProcessamentoMdfeService.InserirMdfeCancelamentoFilaProcessamento(mdfe);

                // Insere no pooling de recebimento (insere na tabela informando que foi colocado na fila o MDF-e)
                InserirInterfaceRecebimentoConfig(mdfe, idListaConfig);

                // Altera o status do MDF-e
                mdfe.SituacaoAtual = SituacaoMdfeEnum.EnviadoArquivoCancelamento;
                _mdfeRepository.AtualizarSituacao(mdfe);

                Usuario usuario = ObterUsuarioRobo();

                // Insere o status do MDF-e
                _mdfeStatusRepository.InserirStatus(mdfe, usuario, new MdfeStatusRetorno { Id = 24 });

                // Insere no log do MDF-e
                _mdfeLogService.InserirLogInfo(mdfe, "MdfeService", "Mdfe de cancelamento enviado para a Config");
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Insere o arquivo do MDF-e na fila de processamento da Config
        /// </summary>
        /// <param name="mdfe">MDF-e que será inserido na fila de retorno da Config</param>
        public void InserirMdfeEncerramentoFilaProcessamentoConfig(Mdfe mdfe)
        {
            try
            {
                int idListaConfig = 0;
                // Insere na fila de processamento da config
                idListaConfig = _filaProcessamentoMdfeService.InserirMdfeEncerramentoFilaProcessamento(mdfe);

                // Insere no pooling de recebimento (insere na tabela informando que foi colocado na fila o MDF-e)
                InserirInterfaceRecebimentoConfig(mdfe, idListaConfig);

                // Altera o status do MDF-e
                mdfe.SituacaoAtual = SituacaoMdfeEnum.EnviadoArquivoEncerramento;
                _mdfeRepository.AtualizarSituacao(mdfe);

                Usuario usuario = ObterUsuarioRobo();

                // Insere o status do MDF-e
                _mdfeStatusRepository.InserirStatus(mdfe, usuario, new MdfeStatusRetorno { Id = 29 });

                // Insere no log do MDF-e
                _mdfeLogService.InserirLogInfo(mdfe, "MdfeService", "Mdfe de encerramento enviado para a Config");
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Obtem os MDF-es para processamento do retorno da Config / Sefaz
        /// </summary>
        /// <param name="indAmbienteSefaz">Indicador do ambiente Sefaz. 1 - Produção, 2 - Homologação</param>
        /// <returns>Retorna uma lista com os MDF-es para serem processados</returns>
        public IList<MdfeInterfaceRecebimentoConfig> ObterMdfeProcessarRecebimento(int indAmbienteSefaz)
        {
            try
            {
                IList<MdfeInterfaceRecebimentoConfig> listaPooling = _mdfeInterfaceRecebimentoConfigRepository.ObterNaoProcessados(indAmbienteSefaz);

                foreach (MdfeInterfaceRecebimentoConfig itemInterface in listaPooling)
                {
                    itemInterface.DataUltimaLeitura = DateTime.Now;
                    _mdfeInterfaceRecebimentoConfigRepository.Atualizar(itemInterface);
                }

                return listaPooling;
            }
            catch (Exception ex)
            {
                _mdfeLogService.InserirLogErro(null, "MdfeService", string.Format("ObterMdfeProcessarRecebimento: {0}", ex.Message));
                throw;
            }
        }

        /// <summary>
        /// Insere no pooling de recebimento para processamento
        /// </summary>
        /// <param name="listaMdfeNaoProcessados">Lista dos itens da interface de recebimento da Config</param>
        /// <param name="hostName">Nome do servidor do robo que está processando o item</param>
        public void InserirPoolingRecebimentoMdfe(IList<MdfeInterfaceRecebimentoConfig> listaMdfeNaoProcessados, string hostName)
        {
            foreach (MdfeInterfaceRecebimentoConfig itemRecebimento in listaMdfeNaoProcessados)
            {
                try
                {
                    Mdfe mdfe = itemRecebimento.Mdfe;
                    MdfeRecebimentoPooling mrp = new MdfeRecebimentoPooling
                    {
                        Id = mdfe.Id,
                        IdListaConfig = itemRecebimento.IdListaConfig,
                        DataHora = DateTime.Now,
                        HostName = hostName
                    };

                    _mdfeRecebimentoPoolingRepository.Inserir(mrp);

                    // Insere no log do MDF-e
                    _mdfeLogService.InserirLogInfo(mdfe, "MdfeService", "Adicionado no pooling de recebimento");
                }
                catch (Exception ex)
                {
                    // Limpa a sessão para processar o próximo
                    _mdfeRecebimentoPoolingRepository.ClearSession();
                }
            }
        }

        /// <summary>
        /// Obtém os MDF-es que estão no pool de recebimento pelo hostname
        /// </summary>
        /// <param name="hostName">Nome do Host</param>
        /// <returns>Lista dos MDF-es para serem processados</returns>
        public IList<MdfeRecebimentoPooling> ObterPoolingRecebimentoPorServidor(string hostName)
        {
            try
            {
                return _mdfeRecebimentoPoolingRepository.ObterListaRecebimentoPorHost(hostName);
            }
            catch (Exception ex)
            {
                _mdfeLogService.InserirLogErro(null, "MdfeService", string.Format("ObterPoolingRecebimentoPorServidor ({0}): {1}", hostName, ex.Message));
                throw;
            }
        }

        /// <summary>
        /// Verifica o sttus do retorno da Config e processa o retorno do MDF-e
        /// </summary>
        /// <param name="itemRecebimento">MDF-e do pooling para ser processado o retorno (item de recebimento)</param>
        /// <returns>Verdadeiro se conseguiu processar o retorno</returns>
        public bool ProcessarRetornoPorItemRecebimento(MdfeRecebimentoPooling itemRecebimento)
        {
            Mdfe mdfeAux = null;
            try
            {
                // Verificar como pegar o retorno
                Vw509ConsultaMdfe consultaMdfe = _filaProcessamentoMdfeService.ObterRetornoCteProcessadoPorIdLista(itemRecebimento.IdListaConfig);
                Usuario usuario = ObterUsuarioRobo();

                if (consultaMdfe != null)
                {
                    mdfeAux = _mdfeRepository.ObterPorId(itemRecebimento.Id);

                    if ((consultaMdfe.Tipo == 0 && consultaMdfe.Stat < 999 && consultaMdfe.Stat != 100) || (consultaMdfe.Tipo == 0 && consultaMdfe.Stat == 2013))
                    {
                        int numeroMdfeAux;

                        int.TryParse(mdfeAux.Numero, out numeroMdfeAux);
                        Nfe03Filial filial = _filaProcessamentoMdfeService.ObterIdentificadorFilial(mdfeAux);

                        // Tenta encontrar o registro de autorização pela filial, número e série
                        Vw509ConsultaMdfe retorno = _filaProcessamentoMdfeService.ObterMdfeJaAutorizadoComDiferencaNaChaveDeAcesso(numeroMdfeAux, (int)filial.IdFilial, int.Parse(mdfeAux.Serie));

                        if (retorno != null)
                        {
                            mdfeAux.Chave = retorno.ChaveAcesso;
                            consultaMdfe = retorno;
                        }
                    }

                    MdfeStatusRetorno mdfeStatusRetorno = AtualizarStatusMdfeRetorno(mdfeAux, consultaMdfe);

                    // Processa o MDF-e status de retorno
                    ProcessarMdfeStatusRetorno(mdfeAux, mdfeStatusRetorno, consultaMdfe);

                    if ((mdfeAux.SituacaoAtual == SituacaoMdfeEnum.Autorizado) || (mdfeAux.SituacaoAtual == SituacaoMdfeEnum.AguardandoCancelamento) || (mdfeAux.SituacaoAtual == SituacaoMdfeEnum.AguardandoEncerramento))
                    {
                        // Item da fila de processamento (utilizado para pegar o XML de retorno)
                        Mdfe01Lista retornoConfig = _filaProcessamentoMdfeService.ObterItemFilaProcessamentoPorConsultaRetorno(consultaMdfe);
                        _mdfeStatusRepository.InserirStatus(mdfeAux, usuario, mdfeStatusRetorno, retornoConfig.XmlRetorno);
                    }
                    else
                    {
                        _mdfeStatusRepository.InserirStatus(mdfeAux, usuario, mdfeStatusRetorno, mdfeStatusRetorno.Descricao);
                    }

                    // Atualiaza o protocolo de retorno da sefaz
                    if (consultaMdfe.DhProtocolo.HasValue)
                    {
                        mdfeAux.DataRecibo = consultaMdfe.DhProtocolo.Value;
                        mdfeAux.NumeroProtocolo = string.Format("{0}", consultaMdfe.Protocolo);
                        _mdfeRepository.Atualizar(mdfeAux);
                    }

                    // Verifica se pode ser removido da fila da interface
                    if (mdfeStatusRetorno.RemoveFilaInterface)
                    {
                        // Remove o cte da interface
                        _mdfeInterfaceRecebimentoConfigRepository.RemoverPorMdfe(mdfeAux);
                    }

                    // Verifica se pode ser inserido na interface de geração do PDF
                    if (mdfeAux.SituacaoAtual == SituacaoMdfeEnum.Autorizado)
                    {
                        // Insere na interface de geração arquivo PDF
                        InserirInterfaceArquivoPdf(mdfeAux);
                    }

                    if (mdfeAux.SituacaoAtual == SituacaoMdfeEnum.AutorizadoCancelamento)
                    {
                        // Todo: Implementar Cancelado
                    }

                    if (mdfeAux.SituacaoAtual == SituacaoMdfeEnum.Encerrado)
                    {
                        // Todo: Implementar Encerrado
                    }

                    return true;
                }
            }
            catch (Exception ex)
            {
                _mdfeLogService.InserirLogErro(mdfeAux, "MdfeService", string.Format("ProcessarRetornoPorItemRecebimento: {0}", ex.Message));
                throw;
            }

            return true;
        }

        /// <summary>
        /// Atualiza o status de retorno do MDF-e
        /// </summary>
        /// <param name="mdfe">MDF-e para ser atualizado o status de retorno</param>
        /// <param name="consultaMdfe">View de retorno da Mdfe.</param>
        /// <returns>Retorna o status de retorno (caso não exista o status, o método cria um novo)</returns>
        public MdfeStatusRetorno AtualizarStatusMdfeRetorno(Mdfe mdfe, Vw509ConsultaMdfe consultaMdfe)
        {
            try
            {
                Usuario usuario = ObterUsuarioRobo();
                // Insere no log do cte
                _mdfeLogService.InserirLogInfo(mdfe, "MdfeService", "Processando o MDF-e de retorno");

                MdfeStatusRetorno mdfeStatusRetorno = _mdfeStatusRetornoRepository.ObterPorId(consultaMdfe.Stat);
                if (mdfeStatusRetorno == null)
                {
                    mdfeStatusRetorno = new MdfeStatusRetorno
                    {
                        Descricao = consultaMdfe.DescStat,
                        Id = consultaMdfe.Stat,
                        InformadoPelaReceita = false,
                        PermiteReenvio = false,
                        RemoveFilaInterface = true
                    };

                    mdfeStatusRetorno = _mdfeStatusRetornoRepository.Inserir(mdfeStatusRetorno);
                }

                return mdfeStatusRetorno;
            }
            catch (Exception ex)
            {
                _mdfeLogService.InserirLogErro(mdfe, "MdfeService", string.Format("AtualizarStatusMdfeRetorno: {0}", ex.Message));
                throw;
            }
        }

        /// <summary>
        /// Remove da tabela de Pool de recebimento
        /// </summary>
        /// <param name="listaProcessamento">Lista de arquivos a serem removidos</param>
        public void RemoverPoolingRecebimentoMdfe(IList<MdfeRecebimentoPooling> listaProcessamento)
        {
            _mdfeRecebimentoPoolingRepository.RemoverPorLista(listaProcessamento);
        }

        /// <summary>
        /// Lista as Mdfes
        /// </summary>
        /// <param name="detalhesPaginacao">detalhes da paginacao</param>
        /// <param name="chaveMdfe">chave da mdfe</param>
        /// <param name="numero">numero da mdfe</param>
        /// <param name="serie">serie da mdfe</param>
        /// <param name="prefixoTrem">prefixo do trem</param>
        /// <param name="os">os do trem</param>
        /// <param name="dataInicial">data inicial do intervalo</param>
        /// <param name="dataFinal">data final do intervalo</param>
        /// <param name="origem">origem do trem</param>
        /// <param name="destino">destino do trem</param>
        /// <param name="idTrem">Id do trem selecionado</param>
        /// <returns>resultado paginado de trens com mdfe</returns>
        public ResultadoPaginado<TremMdfeDto> Listar(DetalhesPaginacao detalhesPaginacao, string chaveMdfe, string numero, string serie, string prefixoTrem, int? os, DateTime? dataInicial, DateTime? dataFinal, string origem, string destino, string situacao, int idTrem)
        {
            if ((string.IsNullOrEmpty(numero) && !string.IsNullOrEmpty(serie))
                || (!string.IsNullOrEmpty(numero) && string.IsNullOrEmpty(serie)))
            {
                throw new TranslogicException("O Número da MDFe deve ser informado junto com a Série.");
            }

            if (string.IsNullOrEmpty(numero)
                && string.IsNullOrEmpty(serie)
                && string.IsNullOrEmpty(chaveMdfe)
                && string.IsNullOrEmpty(prefixoTrem)
                && string.IsNullOrEmpty(origem)
                && string.IsNullOrEmpty(destino)
                && !os.HasValue
                && !dataInicial.HasValue
                && !dataFinal.HasValue)
            {
                throw new TranslogicException("Informe ao menos um filtro.");
            }

            // return _mdfeRepository.Listar(detalhesPaginacao, chaveMdfe, numero, serie, prefixoTrem, os, dataInicial, dataFinal, origem, destino, idTrem);
            // usar metodo abaixo quando for autorizada a alteração na pkg_mdfe
            return _mdfeRepository.ListarSemMdfe(detalhesPaginacao, chaveMdfe, numero, serie, prefixoTrem, os, dataInicial, dataFinal, origem, destino, situacao, idTrem);
        }

        /// <summary>
        /// Pesquisa para exportação
        /// </summary>
        /// <param name="chaveMdfe">chave da mdfe</param>
        /// <param name="numero">numero da mdfe</param>
        /// <param name="serie">serie da mdfe</param>
        /// <param name="prefixoTrem">prefixo do trem</param>
        /// <param name="os">os do trem</param>
        /// <param name="dataInicial">data inicial do intervalo</param>
        /// <param name="dataFinal">data final do intervalo</param>
        /// <param name="origem">origem do trem</param>
        /// <param name="destino">destino do trem</param>
        /// <param name="idTrem">Id do trem selecionado</param>
        /// <returns>resultado paginado de trens com mdfe</returns>
        public IList<TremMdfeDto> ExportarExcel(string chave, string numero, string serie, string prefixo, int? os, DateTime? dataInicial, DateTime? dataFinal, string origem, string destino, string situacao)
        {
            if ((string.IsNullOrEmpty(numero) && !string.IsNullOrEmpty(serie))
                || (!string.IsNullOrEmpty(numero) && string.IsNullOrEmpty(serie)))
            {
                throw new TranslogicException("O Número da MDFe deve ser informado junto com a Série.");
            }

            if (string.IsNullOrEmpty(numero)
                && string.IsNullOrEmpty(serie)
                && string.IsNullOrEmpty(chave)
                && string.IsNullOrEmpty(prefixo)
                && string.IsNullOrEmpty(origem)
                && string.IsNullOrEmpty(destino)
                && !os.HasValue
                && !dataInicial.HasValue
                && !dataFinal.HasValue)
            {
                throw new TranslogicException("Informe ao menos um filtro.");
            }

            DetalhesPaginacao detalhesPaginacao = new DetalhesPaginacao();
            detalhesPaginacao.Limite = 1000;

            //var resultado = _mdfeRepository.ListarSemMdfe(detalhesPaginacao, chave, numero, serie, prefixo, os, dataInicial, dataFinal, origem, destino, 0);
            return _mdfeRepository.ExportarExcel(chave, numero, serie, prefixo, os, dataInicial, dataFinal, origem, destino, situacao);

        }

        /// <summary>
        /// Exporta Relatorio MDFe (GDS 1909) para excel
        /// </summary>
        /// <param name="chave"></param>
        /// <param name="numero"></param>
        /// <param name="serie"></param>
        /// <param name="prefixo"></param>
        /// <param name="os"></param>
        /// <param name="dataInicial"></param>
        /// <param name="dataFinal"></param>
        /// <param name="origem"></param>
        /// <param name="destino"></param>
        /// <param name="situacao"></param>
        /// <returns></returns>
        public IList<Spd_Data.VwNfeCteMdfe> ExportarExcelRelatorioMDFe(string chave, string numero, string serie, string prefixo, int? os, DateTime? dataInicial, DateTime? dataFinal, string origem, string destino, string situacao)
        {

            var where = new StringBuilder(@" (DATA_EMISSAO >= TO_DATE({filtro_dataInicial}, 'DD/MM/YYYY') AND DATA_EMISSAO <= TO_DATE({filtro_dataFinal}, 'DD/MM/YYYY'))
                                            {filtro_chave} 
                                            {filtro_numero_serie}
                                            {filtro_prefixo}
                                            {filtro_os}
                                            {filtro_origem}
                                            {filtro_destino}
                                            {filtro_situacao}");

            where.Replace("{filtro_chave}", (String.IsNullOrEmpty(chave) ? "" : $" AND CHAVE_ACESSO_MDFE = '{chave}'"));

            where.Replace("{filtro_numero_serie}", (String.IsNullOrEmpty(numero) || String.IsNullOrEmpty(serie) ? "" : $" AND (NUMERO_MDFE = '{numero}' AND SERIE_MDFE = '{serie}')"));

            where.Replace("{filtro_prefixo}", (String.IsNullOrEmpty(prefixo) ? "" : $" AND TREM = '{prefixo}'"));

            where.Replace("{filtro_os}", (os == null ? "" : $" AND OS = {os}"));

            where.Replace("{filtro_origem}", (String.IsNullOrEmpty(origem) ? "" : $" AND ORIGEM_ESTACAO = '{origem}'"));

            where.Replace("{filtro_destino}", (String.IsNullOrEmpty(destino) ? "" : $" AND DESTINO_ESTACAO = '{destino}'"));

            where.Replace("{filtro_situacao}", (String.IsNullOrEmpty(situacao) ? "" : $" AND SITUACAO_ATUAL = '{situacao}'"));

            where.Replace("{filtro_dataInicial}", (dataInicial == null ? "" : $"'{dataInicial.Value.ToString("dd/MM/yyyy")}'"));

            where.Replace("{filtro_dataFinal}", (dataFinal == null ? "" : $"'{dataFinal.Value.ToString("dd/MM/yyyy")}'"));

            var pedraSade = BL_VwNfeCteMdfe.Select(where.ToString());


            return pedraSade.ToList();
        }

        /// <summary>
        /// Lista Log das Mdfes
        /// </summary>
        /// <param name="detalhesPaginacao">detalhes da paginacao</param>
        /// <param name="tipoLog">Tipo de Log</param>
        /// <param name="nomeServico">Nome do Serviço</param>
        /// <returns>resultado paginado de logs de mdfe</returns>
        public ResultadoPaginado<LogPkgMdfeDto> ListarLogPkg(DetalhesPaginacao detalhesPaginacao, string tipoLog, string nomeServico)
        {
            return _mdfeRepository.ListarLogPkg(detalhesPaginacao, tipoLog, nomeServico);
        }

        /// <summary>
        /// Gera uma Mdfe forçada pela tela 1253
        /// </summary>
        /// <param name="os">Número da OS</param>
        public bool GerarMdfeForcada(int os)
        {
            return _mdfeRepository.GerarMdfeForcada(os);
        }

        /// <summary>
        /// Carrega os detalhes da MDFe
        /// </summary>
        /// <param name="idTrem">id do trem</param>
        /// <returns>resultado paginado</returns>
        public IList<MdfeDto> ListarDetalhesDaMdfe(int idTrem)
        {
            return _mdfeRepository.ListarDetalhesDaMdfe(idTrem);
        }

        /// <summary>
        /// Carrega os detalhes da MDFe
        /// </summary>
        /// <param name="paginacao">Detalhes da paginacao</param>
        /// <param name="idTrem">id do trem</param>
        /// <param name="semaforo">Semaforo que foi selecionado para o filtro</param>
        /// <param name="siglaUfSemaforo">Sigla da UF selecionada no semáforo</param>
        /// <param name="periodoHorasErroSemaforo">O período de tempo selecionado no semáforo</param>
        /// <returns>resultado paginado</returns>
        public ResultadoPaginado<MdfeDto> ListarDetalhesDaMdfe(DetalhesPaginacao paginacao, int idTrem, string semaforo, string siglaUfSemaforo, HorasErroEnum periodoHorasErroSemaforo)
        {
            return _mdfeRepository.ListarDetalhesDaMdfePaginado(paginacao, idTrem, semaforo, siglaUfSemaforo, periodoHorasErroSemaforo);
        }

        /// <summary>
        /// Listar mdfes por despacho
        /// </summary>
        /// <param name="paginacao">Detalhes da paginacao</param>
        /// <param name="idDespacho">Id do despacho selecionado</param>
        /// <returns>Lista de Mdfe do Despacho</returns>
        public ResultadoPaginado<MdfeDto> ListarPorDespacho(DetalhesPaginacao paginacao, int idDespacho)
        {
            return _mdfeRepository.ListarPorDespacho(paginacao, idDespacho);
        }

        /// <summary>
        /// Lista o ID do  MDF-e pelo ID do despacho
        /// </summary>
        /// <param name="idDespacho">Id do Despacho</param>
        /// <returns>O ID do MDF-e</returns>
        public int ObterIdMdfePorDespacho(int idDespacho)
        {
            return _mdfeRepository.ObterIdMdfePorDespacho(idDespacho);
        }

        /// <summary>
        /// Obtém os status de uma Mdfe
        /// </summary>
        /// <param name="idMdfe">Id da Mdfe que se deseja obter os status</param>
        /// <returns>Lista de Status da Mdfe</returns>
        public IList<MdfeStatus> ObterStatusMdfePorIdMdfe(int idMdfe)
        {
            return _mdfeRepository.ObterStatusMdfePorIdMdfe(idMdfe);
        }

        /// <summary>
        /// Carrega os detalhes da composicao
        /// </summary>
        /// <param name="idMdfe">id da Mdfe</param>
        /// <returns>resultado paginado</returns>
        public IList<MdfeComposicaoDto> ListarDetalhesDaComposicaoAtualizada(int idMdfe)
        {
            return _mdfeComposicaoRepository.ListarDetalhesDaComposicao(idMdfe);
        }

        /// <summary>
        /// Carrega os detalhes da composicao pelo Cte Informado
        /// </summary>
        /// <param name="idCte">id do Cte informado</param>
        /// <returns>resultado paginado</returns>
        public int ObterIdMdfeDaComposicaoPorCte(int idCte)
        {
            return _mdfeComposicaoRepository.ObterIdMdfeDaComposicaoPorCte(idCte);
        }

        /// <summary>
        /// Carrega o Mdfe pelo Cte Informado
        /// </summary>
        /// <param name="idCte">id do Cte informado</param>
        /// <returns>Objeto do tipo Mdfe</returns>
        public Mdfe ObterMdfePorCte(int idCte)
        {
            try
            {
                var idMdfe = _mdfeComposicaoRepository.ObterIdMdfeAutDaComposicaoPorCte(idCte);
                return _mdfeRepository.ObterPorId(idMdfe);
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Carrega os detalhes da última composição do trem
        /// </summary>
        /// <param name="idTrem">id do Trem</param>
        /// <param name="idEmpresa">Id da empresa</param>
        /// <param name="vagoes">Lista de vagões para filtro de impressão</param>
        /// <returns>resultado paginado</returns>
        public IList<MdfeComposicaoDto> DetalhesUltComposicaoTrem(int idTrem, int idEmpresa, List<int> vagoes)
        {
            return _mdfeComposicaoRepository.DetalhesUltComposicaoTrem(idTrem, idEmpresa, vagoes);
        }

        /// <summary>
        /// Carrega os detalhes da composicao
        /// </summary>
        /// <param name="idTrem">id da Mdfe</param>
        /// <returns>resultado paginado</returns>
        public IList<FluxosMdfeDto> ListarFluxosComposicao(int idTrem)
        {
            return _mdfeComposicaoRepository.ListarFluxosDaComposicao(idTrem);
        }

        /// <summary>
        /// Carrega os fluxos da por patio
        /// </summary>
        /// <param name="dataInicial">Data inicial para filtro</param>
        /// <param name="dataFinal">Data final para filtro</param>
        /// <param name="codigoEstacao">codigo da estação</param>
        /// <param name="linha">id do trem</param>
        /// <returns>resultado paginado</returns>
        public IList<FluxosMdfeDto> ListarFluxosPorPatio(DateTime dataInicial, DateTime dataFinal, string codigoEstacao, int? linha)
        {
            return _mdfeComposicaoRepository.ListarFluxosPorPatio(dataInicial, dataFinal, codigoEstacao, linha);
        }

        /// <summary>
        /// Carrega os detalhes da composicao
        /// </summary>
        /// <param name="idEmpresa">Id da empresa recebedora</param>
        /// <param name="dataInicial">Data inicial do período</param>
        /// <param name="dataFinal">Data final do período</param>
        /// <param name="estacao">Código da estação</param>
        /// <param name="idLinha">Id da Linha</param>
        /// <returns>resultado paginado</returns>
        public IList<MdfeComposicaoDto> ListarDetalhesDaComposicaoPorPatio(int idEmpresa, DateTime dataInicial, DateTime dataFinal, string estacao, int? idLinha)
        {
            return _mdfeComposicaoRepository.ListarDetalhesDaComposicaoPorPatio(idEmpresa, dataInicial, dataFinal, estacao, idLinha);
        }

        /// <summary>
        /// Carrega os detalhes da composicao
        /// </summary>
        /// <param name="codigoAreaOperacional">Código da area operacional</param>
        /// <returns>resultado paginado</returns>
        public IList<ElementoVia> ListarLinhasEstacao(string codigoAreaOperacional)
        {
            IAreaOperacional areaOp;
            try
            {
                areaOp = _areaOperacionalRepository.ObterPorCodigo((codigoAreaOperacional ?? String.Empty).ToUpper());

                if (areaOp == null)
                {
                    return new List<ElementoVia>();
                }
            }
            catch (Exception)
            {
                return new List<ElementoVia>();
            }

            return _elementoViaRepository.ObterPorIdEstacaoMae(areaOp.Id.Value).OrderBy(a => a.Codigo).ToList();
        }

        /// <summary>
        /// Carrega os detalhes da linha
        /// </summary>
        /// <param name="idLinha">Id da Linha</param>
        /// <returns>Objeto da Linha</returns>
        public ElementoVia ObterLinhaEstacao(int idLinha)
        {
            return _elementoViaRepository.ObterPorId(idLinha);
        }

        /// <summary>
        /// Carrega os detalhes da empresa
        /// </summary>
        /// <param name="idEmpresa">Id da Empresa</param>
        /// <returns>Objeto da Linha</returns>
        public Empresa ObterDadosEmpresa(int idEmpresa)
        {
            return _empClienteRepository.ObterPorId(idEmpresa);
        }

        /// <summary>
        /// Retorna a descricao da mercadoria pelo codigo
        /// </summary>
        /// <param name="codigoMercadoria">código da mercadoria</param>
        /// <returns>Descrição da mercadoria</returns>
        public string ObterDescricaoMercadoria(string codigoMercadoria)
        {
            return _mercadoriaRepository.ObterPorCodigo(codigoMercadoria).DescricaoDetalhada;
        }

        /// <summary>
        /// Carrega os detalhes da composicao
        /// </summary>
        /// <param name="idMdfe">id da Mdfe</param>
        /// <returns>resultado paginado</returns>
        public Mdfe ListarDetalhesDaComposicao(int idMdfe)
        {
            return _mdfeRepository.ObterPorId(idMdfe);
        }

        /// <summary>
        /// Carrega os detalhes da composicao
        /// </summary>
        /// <param name="idMdfe">id da Mdfe</param>
        /// <returns>resultado paginado</returns>
        public Nfe03Filial ObterDadosFerrovia(int idMdfe)
        {
            Mdfe mdfe = _mdfeRepository.ObterPorId(idMdfe);
            return _empresaRepository.ObterFilialPorCnpj(long.Parse(mdfe.CnpjFerrovia));
        }

        /// <summary>
        /// Carrega os detalhes do trem
        /// </summary>
        /// <param name="idTrem">id do trem</param>
        /// <returns>dados do trem</returns>
        public Trem ObterDadosTrem(int idTrem)
        {
            return _tremRepository.ObterPorId(idTrem);
        }

        /// <summary>
        /// Lista as Mdfes
        /// </summary>
        /// <param name="paginacao">detalhes da paginacao</param>
        /// <param name="prefixoTrem">prefixo do trem</param>
        /// <param name="os">os do trem</param>
        /// <param name="dataInicial">data inicial do intervalo</param>
        /// <param name="dataFinal">data final do intervalo</param>
        /// <param name="origem">origem do trem</param>
        /// <param name="destino">destino do trem</param>
        /// <param name="chaveMdfe">Chave Mdfe</param>
        /// <param name="idTrem">id do trem</param>
        /// <returns>resultado paginado de trens com mdfe</returns>
        public ResultadoPaginado<TremDto> BuscarTremPorFiltros(DetalhesPaginacao paginacao, string prefixoTrem, int? os, DateTime? dataInicial, DateTime? dataFinal, string origem, string destino, string chaveMdfe, int? idTrem)
        {
            return _tremRepository.Listar(paginacao, prefixoTrem, os, dataInicial, dataFinal, origem, destino, chaveMdfe, idTrem);
        }

        /// <summary>
        /// Listar os trens de acordo com o despacho informado
        /// </summary>
        /// <param name="paginacao">Detalhes de paginacao</param>
        /// <param name="idDespacho">Id do despacho selecionado</param>
        /// <returns>Lista de trens de acordo com o despacho</returns>
        public ResultadoPaginado<TremDto> BuscarTremPorDespachoId(DetalhesPaginacao paginacao, int idDespacho)
        {
            return _tremRepository.ListarPorDespacho(paginacao, idDespacho);
        }

        /// <summary>
        /// Carrega os detalhes da composicao
        /// </summary>
        /// <param name="mdfeComposicao">composicao da Mdfe</param>
        /// <returns>resultado paginado</returns>
        public IList<CteEmpresas> ObterRecebedores(IList<MdfeComposicaoDto> mdfeComposicao)
        {
            decimal[] ctes = mdfeComposicao.Where(m => m.IdCte != 0).Select(c => c.IdCte).ToArray();
            return _cteEmpresasRepository.ObterPorListaCte(ctes).OrderBy(x => x.EmpresaRecebedor.Cgc).ToList();
        }

        /// <summary>
        /// Carrega os detalhes da composicao
        /// </summary>
        /// <param name="idMdfe">id da Mdfe</param>
        /// <param name="usuario">Usuario que efetuou o cancelamento.</param>
        /// <returns>resultado paginado</returns>
        public bool CancelarMdfe(int idMdfe, Usuario usuario)
        {
            try
            {
                Mdfe mdfe = _mdfeRepository.ObterPorId(idMdfe);
                MudarSituacaoMdfe(mdfe, SituacaoMdfeEnum.AguardandoCancelamento, usuario, new MdfeStatusRetorno { Id = 19 }, "Arquivo Mdfe alterado para aguardando cancelamento", string.Empty);
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Returns the excel result generating html automatically
        /// </summary>
        /// <param name="controller"> The controller. </param>
        /// <param name="titulo">Titulo do pdf</param>
        /// <param name="fileName"> The file name. </param>
        /// <param name="content"> The Content. </param>
        /// <returns> ActionResult de excel </returns>
        public ActionResult PdfResult(
            Controller controller,
            string titulo,
            string fileName,
            string content)
        {
            // string returnContent = GetText(fields, list);
            Stream returnContent = HtmlToPdf(content, titulo, false);

            var fsr = new FileStreamResult(returnContent, "application/pdf");
            fsr.FileDownloadName = fileName;
            return fsr;
            // return new ExcelResult(fileName, returnContent);
        }

        /// <summary>
        /// Returns the excel result generating html automatically
        /// </summary>
        /// <param name="controller"> The controller. </param>
        /// <param name="titulo">Titulo do pdf</param>
        /// <param name="fileName"> The file name. </param>
        /// <param name="content"> The Content. </param>
        /// <param name="retrato"> indica forma de geracao retrato ou paisagem. </param>
        /// <returns> ActionResult de excel </returns>
        public ActionResult PdfResult(
            Controller controller,
            string titulo,
            string fileName,
            string content,
            bool retrato)
        {
            // string returnContent = GetText(fields, list);
            Stream returnContent = HtmlToPdf(content, titulo, retrato);

            var fsr = new FileStreamResult(returnContent, "application/pdf");
            fsr.FileDownloadName = fileName;
            return fsr;
            // return new ExcelResult(fileName, returnContent);
        }

        /// <summary>
        /// Converte html em pdf
        /// </summary>
        /// <param name="html">Conteudo html</param>
        /// <param name="titulo">Titulo do documento</param>
        /// /// <param name="retrato">Impresao em retrato</param>
        /// <returns>Stream do pdf</returns>
        public Stream HtmlToPdf(string html, string titulo, bool retrato)
        {
            TextReader textReader = new StringReader(html);

            string tempFilePath = Path.GetTempFileName();
            Document document = new Document(retrato ? PageSize.A4 : PageSize.A4.Rotate(), 0, 0, 85, 30);

            PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(tempFilePath, FileMode.Create));
            // var pageEventHandler = new HeaderFooterEventHelper();
            // writer.PageEvent = pageEventHandler;
            // pageEventHandler.Title = titulo;

            document.Open();

            var xmlWorkerFontProvider = new XMLWorkerFontProvider();
            IList<string> fonts = new List<string>();
            fonts.Add("Arial");
            foreach (string font in fonts)
            {
                xmlWorkerFontProvider.Register(font);
            }

            var cssAppliers = new CssAppliersImpl(xmlWorkerFontProvider);
            var htmlContext = new HtmlPipelineContext(cssAppliers);

            htmlContext.SetTagFactory(Tags.GetHtmlTagProcessorFactory());
            // htmlContext.SetImageProvider(new TranslogicImageProvider().GetImageRootPath());

            ICSSResolver cssResolver = XMLWorkerHelper.GetInstance().GetDefaultCssResolver(true);
            IPipeline pipeline = new CssResolverPipeline(cssResolver, new HtmlPipeline(htmlContext, new PdfWriterPipeline(document, writer)));
            XMLWorker worker = new XMLWorker(pipeline, true);
            XMLParser p = new XMLParser(true, worker, Encoding.UTF8);
            p.Parse(textReader);
            p.Flush();
            document.Close();
            document.Dispose();

            StreamReader sr = new StreamReader(tempFilePath);
            MemoryStream ms = new MemoryStream();
            StreamUtils.Copy(sr.BaseStream, ms, new byte[4096]);
            sr.Close();
            File.Delete(tempFilePath);
            ms.Seek(0, SeekOrigin.Begin);
            return ms;
        }

        /// <summary>
        /// Converte html em pdf
        /// </summary>
        /// <param name="html">Conteudo html</param>
        /// <param name="titulo">Titulo do documento</param>
        /// /// <param name="retrato">Impresao em retrato</param>
        /// <returns>Stream do pdf</returns>
        public Stream HtmlToPdfSemMargin(string html, string titulo, bool retrato)
        {
            TextReader textReader = new StringReader(html);

            string tempFilePath = Path.GetTempFileName();
            Document document = new Document(retrato ? PageSize.A4 : PageSize.A4.Rotate(), 0, 0, 0, 0);

            PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(tempFilePath, FileMode.Create));
            // var pageEventHandler = new HeaderFooterEventHelper();
            // writer.PageEvent = pageEventHandler;
            // pageEventHandler.Title = titulo;

            document.Open();

            var tagProcessors = (DefaultTagProcessorFactory)Tags.GetHtmlTagProcessorFactory();
            tagProcessors.RemoveProcessor(HTML.Tag.IMG); // remove the default processor
            tagProcessors.AddProcessor(HTML.Tag.IMG, new CustomImageTagProcessor()); // use our new processor

            var xmlWorkerFontProvider = new XMLWorkerFontProvider();
            IList<string> fonts = new List<string>();
            fonts.Add("Arial");
            foreach (string font in fonts)
            {
                xmlWorkerFontProvider.Register(font);
            }

            var cssAppliers = new CssAppliersImpl(xmlWorkerFontProvider);
            var htmlContext = new HtmlPipelineContext(cssAppliers);
            htmlContext.SetAcceptUnknown(true).AutoBookmark(true).SetTagFactory(tagProcessors); // inject the tagProcessors

            //htmlContext.SetTagFactory(Tags.GetHtmlTagProcessorFactory());
            // htmlContext.SetImageProvider(new TranslogicImageProvider().GetImageRootPath());

            ICSSResolver cssResolver = XMLWorkerHelper.GetInstance().GetDefaultCssResolver(true);
            IPipeline pipeline = new CssResolverPipeline(cssResolver, new HtmlPipeline(htmlContext, new PdfWriterPipeline(document, writer)));
            XMLWorker worker = new XMLWorker(pipeline, true);
            XMLParser p = new XMLParser(true, worker, Encoding.UTF8);
            p.Parse(textReader);
            p.Flush();
            document.Close();
            document.Dispose();

            StreamReader sr = new StreamReader(tempFilePath);
            MemoryStream ms = new MemoryStream();
            StreamUtils.Copy(sr.BaseStream, ms, new byte[4096]);
            sr.Close();
            File.Delete(tempFilePath);
            ms.Seek(0, SeekOrigin.Begin);
            return ms;
        }

        public class CustomImageTagProcessor : iTextSharp.tool.xml.html.Image
        {
            public override IList<IElement> End(IWorkerContext ctx, Tag tag, IList<IElement> currentContent)
            {
                IDictionary<string, string> attributes = tag.Attributes;
                string src;
                if (!attributes.TryGetValue(HTML.Attribute.SRC, out src))
                    return new List<IElement>(1);

                if (string.IsNullOrEmpty(src))
                    return new List<IElement>(1);

                if (src.StartsWith("data:image/", StringComparison.InvariantCultureIgnoreCase))
                {
                    // data:[<MIME-type>][;charset=<encoding>][;base64],<data>
                    var base64Data = src.Substring(src.IndexOf(",") + 1);
                    if (!string.IsNullOrEmpty(base64Data))
                    {
                        var imagedata = Convert.FromBase64String(base64Data);
                        var image = iTextSharp.text.Image.GetInstance(imagedata);

                        var list = new List<IElement>();
                        var htmlPipelineContext = GetHtmlPipelineContext(ctx);
                        list.Add(GetCssAppliers().Apply(new Chunk((iTextSharp.text.Image)GetCssAppliers().Apply(image, tag, htmlPipelineContext), 0, 0, true), tag, htmlPipelineContext));
                        return list;
                    }
                }

                return base.End(ctx, tag, currentContent);
            }
        }

        /// <summary>
        /// Carrega os detalhes da composicao
        /// </summary>
        /// <param name="idMdfe">id da Mdfe</param>
        /// <param name="usuario">Usuario que efetuou o encerramento.</param>
        /// <returns>resultado paginado</returns>
        public bool EncerrarMdfe(int idMdfe, Usuario usuario)
        {
            try
            {
                Mdfe mdfe = _mdfeRepository.ObterPorId(idMdfe);
                MudarSituacaoMdfe(mdfe, SituacaoMdfeEnum.AguardandoEncerramento, usuario, new MdfeStatusRetorno { Id = 31 }, "Arquivo Mdfe alterado para aguardando encerramento", string.Empty);
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Carrega os detalhes da composicao
        /// </summary>
        /// <param name="idMdfe">id da Mdfe</param>
        /// <param name="usuario">Usuario que efetuou o encerramento.</param>
        /// <returns>resultado paginado</returns>
        public bool EnviarAprovacao(int idMdfe, Usuario usuario)
        {
            try
            {
                var composicao = ListarDetalhesDaComposicaoAtualizada(idMdfe);

                bool possuiCtesNaoAutorizados = composicao.Any(c => c.SituacaoCte != null && !c.SituacaoCte.Equals(ALL.Core.Util.Enum<SituacaoCteEnum>.GetDescriptionOf(SituacaoCteEnum.Autorizado)));

                if (possuiCtesNaoAutorizados)
                {
                    throw new Exception("Mdfe possui CTes que não estão autorizados.");
                }

                Mdfe mdfe = _mdfeRepository.ObterPorId(idMdfe);
                var listaStatus = _mdfeStatusRepository.ObterPorMdfe(mdfe).ToList();
                int qtdeCancelado = listaStatus.Count(c => c.MdfeStatusRetorno.Id == 22);
                int qtdeEncerrado = listaStatus.Count(c => c.MdfeStatusRetorno.Id == 28);
                SituacaoMdfeEnum situacao = SituacaoMdfeEnum.PendenteArquivoEnvio;

                if (qtdeCancelado > 0)
                {
                    MudarSituacaoMdfe(mdfe, SituacaoMdfeEnum.AguardandoCancelamento, usuario, new MdfeStatusRetorno { Id = 19 }, "Arquivo Mdfe alterado para Aguardando Cancelamento", string.Empty);
                }
                else if (qtdeEncerrado > 0)
                {
                    MudarSituacaoMdfe(mdfe, SituacaoMdfeEnum.AguardandoEncerramento, usuario, new MdfeStatusRetorno { Id = 31 }, "Arquivo Mdfe alterado para Aguardando Encerramento", string.Empty);
                }
                else
                {
                    MudarSituacaoMdfe(mdfe, SituacaoMdfeEnum.PendenteArquivoEnvio, usuario, new MdfeStatusRetorno { Id = 10 }, "Arquivo Mdfe alterado para pendente arquivo envio", string.Empty);
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return true;
        }

        /// <summary>
        /// Retorna todos os Cte filhos do cte atual
        /// </summary>
        /// <param name="cte">Id do Cte para retorno do status</param>
        /// <returns>lista de Cte agrupamento</returns>
        public IList<MdfeStatus> RetornaCteMonitoramentoStatusMdfe(int cte)
        {
            IList<MdfeStatus> mdfeStatus = _mdfeRepository.ObterPorId(cte).ListaStatus.OrderByDescending(g => g.DataHora).ToList();

            return mdfeStatus;
        }

        /// <summary>
        /// Carrega os detalhes da composicao
        /// </summary>
        /// <param name="idMdfe">id da Mdfe</param>
        /// <returns>resultado paginado</returns>
        public Stream GerarPdf(int idMdfe)
        {
            var memoryStream = new MemoryStream();

            // TODO: PERFORMANCE - trocar por BL_MDFE_ARQUIVO
            //var arquivo = _mdfeArquivoRepository.ObterPorId(idMdfe);
            var arquivo = BL_MdfeArquivo.SelectByPk(idMdfe);
            if (arquivo != null)
            {
                memoryStream = new MemoryStream(arquivo.ArquivoPdf, 0, arquivo.ArquivoPdf.Length);
            }

            return memoryStream;
        }

        /// <summary>
        /// Gera um arquivo substitut do MDF-e levando em consideração a última conposição da OS passada no parâmetro
        /// </summary>
        /// <param name="osId">ID da OS</param>
        /// <param name="recebedorId">ID do terminal recebedor da mercadoria</param>
        /// <returns>Retorna o PDF do arquivo substituto do PDF</returns>
        public Stream ObterManifestoCargaPdf(decimal osId, decimal recebedorId, List<NotaTicketVagao> vagoesTicket, bool refat)
        {
            var registrosManifestoCarga = _mdfeArquivoRepository.ObterManifestoCargaPorOsRecebedor(osId, recebedorId, refat);
            if (!registrosManifestoCarga.Any())
            {
                return new MemoryStream();
            }

            var pdf = _relatorioDocumentosService.GerarManifestoCargaPdf(registrosManifestoCarga, vagoesTicket);
            return pdf;
        }

        /// <summary>
        /// Grava um log a cada vez que o usuário clicar no exportar, realizar a impressão
        /// </summary>
        /// <param name="idTrem">Id do trem selecionado para impressão</param>
        /// <param name="idVagao">Id do vagão selecionado para impressão</param>
        /// <param name="idEmpresa">Id da empresa selecionada para impressão</param>
        /// <param name="usuarioLogado">Usuário logado que está realizando a impressão</param>
        public void GravarLogImpressao(int? idTrem, int? idVagao, int? idEmpresa, Usuario usuarioLogado)
        {
            _logImpressaoReciboRepository.GravarLogImpressao(idTrem, idVagao, idEmpresa, usuarioLogado != null ? usuarioLogado.Id.Value : 0);
        }

        /// <summary>
        /// Obtém uma lista com os ctes para importação
        /// </summary>
        /// <param name="hostName">Nome do servidor</param>
        /// <returns>Retorna uma lista com os ctes para serem importados</returns>
        public IList<MdfeInterfacePdfConfig> ObterMdfesImportarPdf(string hostName)
        {
            try
            {
                IList<MdfeInterfacePdfConfig> listaInterface = _mdfeInterfacePdfConfigRepository.ObterListaInterfacePorHost(hostName);
                foreach (MdfeInterfacePdfConfig itemInterface in listaInterface)
                {
                    itemInterface.DataUltimaLeitura = DateTime.Now;
                    itemInterface.DataUltimaLeitura = DateTime.Now;
                    _mdfeInterfacePdfConfigRepository.Atualizar(itemInterface);
                }

                return listaInterface;
            }
            catch (Exception ex)
            {
                _mdfeLogService.InserirLogErro(null, "MdfeService", string.Format("ObterCtesImportarPdf ({0}): {1}", hostName, ex.Message));
                throw;
            }
        }

        /// <summary>
        /// Importa o pdf para dentro do banco
        /// </summary>
        /// <param name="mdfeInterfacePdf">Interface pdf do Mdfe</param>
        /// <param name="pdfPah">Caminho do pdf</param>
        public void ImportarArquivoPdf(MdfeInterfacePdfConfig mdfeInterfacePdf, string pdfPah)
        {
            Mdfe mdfe = _mdfeRepository.ObterPorId(mdfeInterfacePdf.Mdfe.Id);

            string nomeArquivo = Path.Combine(pdfPah, mdfe.Chave + ".pdf");

            try
            {
                // Verifica se o arquivo existe
                if (File.Exists(nomeArquivo))
                {
                    // Insere o arquivo pdf no banco de dados
                    _mdfeArquivoRepository.InserirOuAtualizarPdf(mdfe, nomeArquivo);

                    _mdfeLogService.InserirLogInfo(mdfe, "MdfeService", "Pdf inserido na base (MDFE_ARQUIVO)");

                    // string processado = pdfPah + "Processados\\" + mdfe.Chave + ".pdf";
                    string processado = Path.Combine(ObterDiretorioProcessado(pdfPah, TipoArquivoEnvioEnum.Pdf, mdfe.DataHora), mdfe.Chave + ".pdf");

                    if (File.Exists(processado))
                    {
                        File.Delete(processado);
                    }

                    // Move o arquivo para o dir de processados
                    File.Move(nomeArquivo, processado);

                    _mdfeLogService.InserirLogInfo(mdfe, "MdfeService", string.Format("Movendo arquivo de: {0} para: {1}", nomeArquivo, processado));

                    _mdfeLogService.InserirLogInfo(mdfe, "MdfeService", "Removendo da interface de geração do Pdf");

                    // Remove da lista da interface
                    _mdfeInterfacePdfConfigRepository.Remover(mdfeInterfacePdf);

                    // Atualiza o flag de PDF gerado
                    // mdfe.ArquivoPdfGerado = true;
                    // _mdfeRepository.AtualizarArquivoGeradoCte(cte, TipoArquivoEnvioEnum.Pdf);
                }
                else
                {
                    _mdfeLogService.InserirLogErro(mdfe, "MdfeService", string.Format("Não encontrado o arquivo: {0} ", nomeArquivo));
                }
            }
            catch (Exception ex)
            {
                _mdfeLogService.InserirLogErro(mdfe, "MdfeService", string.Format("ImportarArquivoPdf: Erro na importacao do arquivo em ImportarArquivoPdf: {0}", ex.Message));
            }
        }

        /// <summary>
        /// Obtem o diretório dos arquivos processados
        /// </summary>
        /// <param name="pathInicial">Caminho Inicial</param>
        /// <param name="tipoArquivo">Tipo do arquivo</param>
        /// <param name="dataCte">Data do Cte</param>
        /// <returns>Retorna caminho</returns>
        public string ObterDiretorioProcessado(string pathInicial, TipoArquivoEnvioEnum tipoArquivo, DateTime dataCte)
        {
            string nomeDiretorio = string.Empty;
            if (tipoArquivo == TipoArquivoEnvioEnum.Pdf)
            {
                nomeDiretorio = Path.Combine(pathInicial, @"PDF_PROCESSADOS\");
            }
            else if (tipoArquivo == TipoArquivoEnvioEnum.Xml)
            {
                nomeDiretorio = Path.Combine(pathInicial, @"XML_PROCESSADOS\");
            }
            else
            {
                nomeDiretorio = Path.Combine(pathInicial, @"PROCESSADOS\");
            }

            nomeDiretorio = Path.Combine(nomeDiretorio, String.Format("{0:yyyyMMdd}", dataCte));
            // nomeDiretorio += @"\";

            if (!Directory.Exists(nomeDiretorio))
            {
                Directory.CreateDirectory(nomeDiretorio);
            }

            return nomeDiretorio;
        }

        /// <summary>
        /// Verifica a composição da Mdfe localizada pela Cte que está sendo verificada
        /// </summary>
        /// <param name="idCte">Id da Cte informada</param>
        /// <param name="usuario">Usuario que está na sessão ou do Robô</param>
        public void VerificarComposicaoMdfeEnviarAprovacao(int idCte, Usuario usuario)
        {
            var idMdfe = ObterIdMdfeDaComposicaoPorCte(idCte);

            if (idMdfe > 0)
            {
                EnviarAprovacao(idMdfe, usuario);
            }
        }

        /// <summary>
        /// Retorna a lista de Mdfes com erro para montar semáforo
        /// </summary>
        /// <returns>Lista de Mdfes com erro</returns>
        public IList<SemaforoMdfeDto> RetornaListaMdfesErro()
        {
            return _mdfeRepository.ObterMdfesErro();
        }

        private void InserirInterfaceArquivoPdf(Mdfe mdfe)
        {
            string hostName = Dns.GetHostName();
            MdfeInterfacePdfConfig interfacePdf = new MdfeInterfacePdfConfig();
            interfacePdf.Mdfe = mdfe;
            interfacePdf.Chave = mdfe.Chave;
            interfacePdf.HostName = hostName;
            interfacePdf.DataHora = DateTime.Now;
            interfacePdf.DataUltimaLeitura = DateTime.Now;

            _mdfeInterfacePdfConfigRepository.Inserir(interfacePdf);
        }

        private void InserirInterfaceRecebimentoConfig(Mdfe mdfe, int idListaConfig)
        {
            MdfeInterfaceRecebimentoConfig interfaceConfig = new MdfeInterfaceRecebimentoConfig
            {
                Mdfe = mdfe,
                IdListaConfig = idListaConfig,
                DataHora = DateTime.Now,
                DataUltimaLeitura = DateTime.Now
            };

            _mdfeInterfaceRecebimentoConfigRepository.Inserir(interfaceConfig);
        }

        private void ProcessarMdfeStatusRetorno(Mdfe mdfe, MdfeStatusRetorno mdfeStatusRetorno, Vw509ConsultaMdfe consultaMdfe)
        {
            // Insere no log do cte
            _mdfeLogService.InserirLogInfo(mdfe, "MdfeService", string.Format("Status de retorno do Mdfe: {0}", mdfeStatusRetorno.Id));

            if (mdfeStatusRetorno.Id == 100)
            {
                // Item da fila de processamento (utilizado para pegar o XML de retorno)
                Mdfe01Lista retornoConfig = _filaProcessamentoMdfeService.ObterItemFilaProcessamentoPorConsultaRetorno(consultaMdfe);

                // Cte Autorizado
                mdfe.SituacaoAtual = SituacaoMdfeEnum.Autorizado;
                mdfe.XmlAutorizacao = retornoConfig.XmlRetorno;

                // Recupera a lista de Arvore de mdfe
                IList<MdfeArvore> mdfeArvore = _mdfeArvoreRepository.ObterArvorePorMdfeFilho(mdfe);

                // Recupera o Mdfe Filho
                MdfeArvore arvoreFilho = mdfeArvore.Where(m => m.MdfePai.Id == mdfe.Id && m.MdfeFilho.Id != m.MdfePai.Id).FirstOrDefault();

                if (arvoreFilho != null)
                {
                    if (arvoreFilho.MdfeFilho.SituacaoAtual == SituacaoMdfeEnum.AguardandoMdfeEncerramento)
                    {
                        mdfe.SituacaoAtual = SituacaoMdfeEnum.AguardandoEncerramento;
                        _mdfeRepository.Atualizar(mdfe);
                    }
                }
            }
            else if (mdfeStatusRetorno.Id == 101)
            {
                // Item da fila de processamento (utilizado para pegar o XML de retorno)
                Mdfe01Lista retornoConfig = _filaProcessamentoMdfeService.ObterItemFilaProcessamentoPorConsultaRetorno(consultaMdfe);

                // Cancelamento de Mdf-e homologado
                mdfe.SituacaoAtual = SituacaoMdfeEnum.AutorizadoCancelamento;
                mdfe.XmlCancelamento = retornoConfig.XmlRetorno;
            }
            else if (mdfeStatusRetorno.Id == 135)
            {
                // Item da fila de processamento (utilizado para pegar o XML de retorno)
                Mdfe01Lista retornoConfig = _filaProcessamentoMdfeService.ObterItemFilaProcessamentoPorConsultaRetorno(consultaMdfe);
                var listaStatus = _mdfeStatusRepository.ObterPorMdfe(mdfe).ToList();
                int qtdeCancelado = listaStatus.Count(c => c.MdfeStatusRetorno.Id == 22);
                int qtdeEncerrado = listaStatus.Count(c => c.MdfeStatusRetorno.Id == 28);

                // if (mdfe.SituacaoAtual == SituacaoMdfeEnum.EnviadoArquivoCancelamento)
                if (qtdeCancelado > 0)
                {
                    // Cancelamento de MDF-e homologado
                    mdfe.SituacaoAtual = SituacaoMdfeEnum.Cancelado;
                }
                else if (qtdeEncerrado > 0)
                {
                    // Encerrado de MDF-e homologado
                    mdfe.SituacaoAtual = SituacaoMdfeEnum.Encerrado;
                }

                mdfe.XmlCancelamento = retornoConfig.XmlRetorno;

                // Recupera a lista de Arvore de mdfe
                IList<MdfeArvore> mdfeArvore = _mdfeArvoreRepository.ObterArvorePorMdfeFilho(mdfe);

                // Recupera o Mdfe Filho
                MdfeArvore arvoreFilho = mdfeArvore.Where(m => m.MdfePai.Id == mdfe.Id && m.MdfeFilho.Id != m.MdfePai.Id).FirstOrDefault();

                if (arvoreFilho != null)
                {
                    if (arvoreFilho.MdfeFilho.SituacaoAtual == SituacaoMdfeEnum.AguardandoMdfeEncerramento)
                    {
                        arvoreFilho.MdfeFilho.SituacaoAtual = SituacaoMdfeEnum.PendendeGeracaoChaveMdfe;
                        _mdfeRepository.Atualizar(arvoreFilho.MdfeFilho);
                    }
                }
            }
            else if ((mdfeStatusRetorno.Id >= 103) && (mdfeStatusRetorno.Id <= 110))
            {
                // Sefaz processando o Lote (Nenhuma ação)
                mdfe.SituacaoAtual = SituacaoMdfeEnum.MdfeEmProcessamento;
            }
            else if ((mdfeStatusRetorno.Id >= 111) && (mdfeStatusRetorno.Id <= 112))
            {
                mdfe.SituacaoAtual = SituacaoMdfeEnum.Erro;
            }
            else if (mdfeStatusRetorno.Id >= 200)
            {
                // else if ((mdfeStatusRetorno.Id >= 200) && (mdfeStatusRetorno.Id <= 999))
                // Rejeição
                if (mdfeStatusRetorno.PermiteReenvio)
                {
                    mdfe.SituacaoAtual = SituacaoMdfeEnum.ErroAutorizadoReEnvio;
                }
                else
                {
                    mdfe.SituacaoAtual = SituacaoMdfeEnum.Erro;
                }
            }
            else
            {
                mdfe.SituacaoAtual = SituacaoMdfeEnum.Erro;
            }

            if (!mdfeStatusRetorno.RemoveFilaInterface)
            {
                mdfe.SituacaoAtual = SituacaoMdfeEnum.MdfeEmProcessamento;
            }

            // Insere no log do cte
            _mdfeLogService.InserirLogInfo(mdfe, "MdfeService", string.Format("Atualizando a situação atual do MDF-e para: {0}", mdfe.SituacaoAtual));
            _mdfeRepository.InserirOuAtualizar(mdfe);
        }

        /// <summary>
        /// Gera uma Mdfe Virtual ser vinculo
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public Dictionary<string, KeyValuePair<bool, string>> GerarMdfeVirtualSemVinculo(List<CteVirtuaisSemVinculoMdfeDto> obj, Usuario UsuarioAtual)
        {
            Dictionary<string, KeyValuePair<bool, string>> listaRetorno = new Dictionary<string, KeyValuePair<bool, string>>();

            string mensagem = string.Empty;
            bool status = false;

            foreach (var item in obj)
            {
                status = false;
                if (item.ID_MOVIMENTACAO != 0 && item.ID_AREA_OPERACIONAL != 0 && item.ID_TREM != 0 && item.COMPOSICAO != 0)
                {
                    #region -- Envia os Dados  para a geração do MDF-e virtual sem vinculo --
                    var mdfeVirtual = new CteVirtuaisSemVinculoMdfeDto()
                    {
                        ID_MOVIMENTACAO = item.ID_MOVIMENTACAO,
                        ID_AREA_OPERACIONAL = item.ID_AREA_OPERACIONAL,
                        ID_TREM = item.ID_TREM,
                        COMPOSICAO = item.COMPOSICAO,
                        DATA_EMISSAO_VIRTUAL = DateTime.Now,
                        ID_OS = null,
                        PREFIXO_TREM = null
                    };


                    var consultaTrem = _tremRepository.ObterTremComposicao((int)item.ID_TREM, (int)item.COMPOSICAO);

                    if (consultaTrem != null)
                    {
                        // TODO: Adicionar o update na coluna   AO_ID_AO_PVT = NULL
                        _tremRepository.UpdateTremIdPVT((int)item.ID_TREM);
                    }

                    status = this._mdfeRepository.GerarMdfeVirtualSemVinculo(mdfeVirtual);

                    //var retornoLog = mdfeVirtual
                    KeyValuePair<bool, string> statusRetorno = new KeyValuePair<bool, string>(status, status ?
                                                                "O MDF-e da composição " + item.COMPOSICAO + " foi gerado com sucesso!"
                                                                : "O MDF-e da composição " + item.COMPOSICAO + " não pode ser gerado, verifique!");
                    listaRetorno.Add(item.COMPOSICAO.ToString(), statusRetorno);
                    #endregion

                    #region -- Insere o registro na tabela Log --
                    var logObj = new Spd_Data.LogCteVirtualSemVinculo()
                    {
                        MtIdMov = item.ID_MOVIMENTACAO,
                        AoIdAo = item.ID_AREA_OPERACIONAL,
                        TrIdTrm = item.ID_TREM,
                        CpIdCps = item.COMPOSICAO,
                        DataCadastro = DateTime.Now,
                        Usuario = UsuarioAtual.Codigo
                    };

                    BL_LogCteVirtualSemVinculo.Insert(logObj);
                    #endregion
                }
            }
            return listaRetorno;
        }
    }
}