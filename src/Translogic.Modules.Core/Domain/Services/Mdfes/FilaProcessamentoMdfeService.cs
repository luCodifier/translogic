﻿namespace Translogic.Modules.Core.Domain.Services.Mdfes
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using Model.FluxosComerciais.Mdfes;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Mdfes;
    using Translogic.Modules.Core.Domain.Model.Diversos.Ibge;
    using Translogic.Modules.Core.Domain.Model.Diversos.Ibge.Repositories;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes.Repositories;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes.Repositories.TriangulusConfig;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes.TriangulusConfig;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.TriangulusConfig;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.TriangulusConfig.Repositories;
    using Translogic.Modules.Core.Domain.Model.Via;

    /// <summary>
    /// Classe de serviço da fila de processamento dos MDF-es
    /// </summary>
    public class FilaProcessamentoMdfeService
    {
        private readonly INfe03FilialRepository _nfe03FilialRepository;
        private readonly IMdfe01ListaRepository _mdfe01ListaRepository;
        private readonly IMdfe50ImpRepository _mdfe50ImpRepository;
        private readonly IVw509ConsultaMdfeRepository _509ConsultaMdfeRepository;
        private readonly IMdfeArvoreRepository _mdfeArvoreRepository;
        private readonly ICidadeIbgeRepository _cidadeIbgeRepository;
        private readonly IMdfeSerieEmpresaUfRepository _mdfeSerieEmpresaUfRepository;
        private readonly MdfeLogService _mdfelogService;

        /// <summary>
        /// Serviço de fila de processamento
        /// </summary>
        /// <param name="nfe03FilialRepository">Repositório nfe03FilialRepository Injetado.</param>
        /// <param name="mdfe01ListaRepository">Repositório mdfe01ListaRepository Injetado.</param>
        /// <param name="mdfe50ImpRepository">Repositório mdfe50ImpRepository Injetado.</param>
        /// <param name="mdfelogService">Repositório mdfelogService Injetado.</param>
        /// <param name="consultaMdfeRepository">Repositório vw509ConsultaMdfeRepository Injetado.</param>
        /// <param name="mdfeArvoreRepository">Repositório mdfeArvoreRepository Injetado.</param>
        /// <param name="cidadeIbgeRepository">Repositório cidadeIbgeRepository Injetado.</param>
        /// <param name="mdfeSerieEmpresaUfRepository">Repositório mdfeSerieEmpresaUfRepository Injetado.</param>
        public FilaProcessamentoMdfeService(INfe03FilialRepository nfe03FilialRepository, IMdfe01ListaRepository mdfe01ListaRepository, IMdfe50ImpRepository mdfe50ImpRepository, MdfeLogService mdfelogService, IVw509ConsultaMdfeRepository consultaMdfeRepository, IMdfeArvoreRepository mdfeArvoreRepository, ICidadeIbgeRepository cidadeIbgeRepository, IMdfeSerieEmpresaUfRepository mdfeSerieEmpresaUfRepository)
        {
            _nfe03FilialRepository = nfe03FilialRepository;
            _mdfeSerieEmpresaUfRepository = mdfeSerieEmpresaUfRepository;
            _cidadeIbgeRepository = cidadeIbgeRepository;
            _mdfeArvoreRepository = mdfeArvoreRepository;
            _509ConsultaMdfeRepository = consultaMdfeRepository;
            _mdfelogService = mdfelogService;
            _mdfe50ImpRepository = mdfe50ImpRepository;
            _mdfe01ListaRepository = mdfe01ListaRepository;
        }

        /// <summary>
        /// Insere na fila da processamento o MDF-e
        /// </summary>
        /// <param name="mdfe">MDF-e que será enviado</param>
        /// <returns>Retorna o id da lista</returns>
        public int InserirMdfeEnvioFilaProcessamento(Mdfe mdfe)
        {
            Mdfe01Lista mdfe01Lista = null;
            CidadeIbge cidadeDestino = null;
            Nfe03Filial nfe03Filial;

            try
            {
                mdfe01Lista = new Mdfe01Lista();
                nfe03Filial = ObterIdentificadorFilial(mdfe);

                // Grava os dados chaves para identificação do Mdfe
                mdfe01Lista.Numero = int.Parse(mdfe.Numero);
                mdfe01Lista.NumeroFim = int.Parse(mdfe.Numero);
                mdfe01Lista.Serie = mdfe.Serie;
                mdfe01Lista.IdFilial = (int)nfe03Filial.IdFilial;

                mdfe01Lista.ChaveAcesso = mdfe.Chave;
                mdfe01Lista.UfIbge = nfe03Filial.Uf;
                mdfe01Lista.Tipo = 0;
                mdfe01Lista.CnpjEmitente = nfe03Filial.Cnpj;
                // mdfe01Lista.CnpjDestinatario = nfe03Filial.Cnpj;
                mdfe01Lista.TipoAmbiente = ObterAmbienteSefaz(mdfe);
                mdfe01Lista.VersaoSefaz = double.Parse(mdfe.Versao.CodigoVersao, CultureInfo.GetCultureInfo("en-US"));
                mdfe01Lista.TipoImpressao = 1;
                mdfe01Lista.Modelo = "58";
                mdfe01Lista.Lock = 0;
                mdfe01Lista.TipoEmissao = 1;

                // mdfe01Lista.CodigoMunicipioEncerramento = cidadeDestino.CodigoIbge;
                mdfe01Lista.DataEmissao = GetDhEmissao(mdfe);

                _mdfe01ListaRepository.Inserir(mdfe01Lista);

                // Obs: Como a config implementa uma trigger para gerar a sequence dessa tabela, 
                // foi alterado o mapeamento e é realizado um novo sql para ver qual id foi gerado
                Mdfe01Lista itemLista = _mdfe01ListaRepository.ObterListaFilaProcessamento((int)nfe03Filial.IdFilial, mdfe01Lista.Numero, mdfe01Lista.Serie, 0, false);
                return itemLista.Id;
            }
            catch (Exception ex)
            {
                _mdfelogService.InserirLogErro(mdfe, "InserirMdfeEnvioFilaProc", string.Format("InserirMdfeEnvioFilaProcessamento: Erro na inserção da fila de processamento {0}", ex.Message));
                throw;
            }
        }

        /// <summary>
        /// Obtem o cte de retorne do processamento
        /// </summary>
        /// <param name="idLista">Identificador da lista que o cte esta</param>
        /// <returns>Retorna o objeto de retorno do Cte</returns>
        public Vw509ConsultaMdfe ObterRetornoCteProcessadoPorIdLista(int idLista)
        {
            return _509ConsultaMdfeRepository.ObterRetornoPorIdLista(idLista);
        }

        /// <summary>
        /// Obtém dados do item da fila de processamento pelo retorno da config
        /// </summary>
        /// <param name="consultaMdfe">Dados de retorno da config</param>
        /// <returns>Retorna o item da fila de processamento</returns>
        public Mdfe01Lista ObterItemFilaProcessamentoPorConsultaRetorno(Vw509ConsultaMdfe consultaMdfe)
        {
            return _mdfe01ListaRepository.ObterPorId(consultaMdfe.IdLista);
        }

        /// <summary>
        /// Obtém o retorno do processamento 
        /// </summary>
        /// <param name="numero">Número do Cte</param>
        /// <param name="idFilial">Filial do Cte</param>
        /// <param name="serie">Serie do Cte</param>
        /// <returns>Retorna o mdfe processado</returns>
        public Vw509ConsultaMdfe ObterMdfeJaAutorizadoComDiferencaNaChaveDeAcesso(int numero, int idFilial, int serie)
        {
            return _509ConsultaMdfeRepository.ObterMdfeJaAutorizadoComDiferencaNaChaveDeAcesso(numero, idFilial, serie);
        }

        /// <summary>
        /// Insere na fila da processamento o MDF-e
        /// </summary>
        /// <param name="mdfe">MDF-e que será enviado</param>
        /// <returns>Retorna o id da lista</returns>
        public int InserirMdfeCancelamentoFilaProcessamento(Mdfe mdfe)
        {
            Mdfe01Lista mdfe01Lista = null;
            CidadeIbge cidadeDestino = null;
            Nfe03Filial nfe03Filial;
            Nfe03Filial nfe03FilialEncerramento;
            IList<MdfeArvore> listaArvore = null;
            CidadeIbge cidadeEmitente = null;
            MdfeArvore mdfeFilho = null;

            try
            {
                mdfe01Lista = new Mdfe01Lista();
                nfe03Filial = ObterIdentificadorFilial(mdfe);
                listaArvore = _mdfeArvoreRepository.ObterArvorePorMdfeFilho(mdfe);
                mdfeFilho = listaArvore.Where(m => m.MdfePai.Id == mdfe.Id).FirstOrDefault();

                // Grava os dados chaves para identificação do Mdfe
                mdfe01Lista.Numero = int.Parse(mdfe.Numero);
                mdfe01Lista.NumeroFim = int.Parse(mdfe.Numero);
                mdfe01Lista.Serie = mdfe.Serie;
                mdfe01Lista.IdFilial = (int)nfe03Filial.IdFilial;

                mdfe01Lista.UfIbge = nfe03Filial.Uf;
                mdfe01Lista.Tipo = 2;
                mdfe01Lista.CnpjEmitente = nfe03Filial.Cnpj;
                mdfe01Lista.TipoAmbiente = ObterAmbienteSefaz(mdfe);
                mdfe01Lista.VersaoSefaz = double.Parse(mdfe.Versao.CodigoVersao, CultureInfo.GetCultureInfo("en-US"));
                mdfe01Lista.TipoImpressao = 1;
                mdfe01Lista.Modelo = "58";
                mdfe01Lista.Lock = 0;
                mdfe01Lista.TipoEmissao = 1;
                // mdfe01Lista.Protocolo = long.Parse(mdfe.NumeroProtocolo);
                // mdfe01Lista.DataRecibo = mdfe.DataRecibo;
                // mdfe01Lista.DataEncerramentoProc = DateTime.Now;
                // mdfe01Lista.UfEncerramento = nfe03FilialEncerramento.Uf;
                // mdfe01Lista.CodigoMunicipioEncerramento = cidadeEmitente.CodigoIbge;
                mdfe01Lista.Justificativa = "Teste de cancelamento de MDF-e.";
                mdfe01Lista.DataEmissao = GetDhEmissao(mdfe);

                _mdfe01ListaRepository.Inserir(mdfe01Lista);

                // Obs: Como a config implementa uma trigger para gerar a sequence dessa tabela, 
                // foi alterado o mapeamento e é realizado um novo sql para ver qual id foi gerado
                Mdfe01Lista itemLista = _mdfe01ListaRepository.ObterListaFilaProcessamento((int)nfe03Filial.IdFilial, mdfe01Lista.Numero, mdfe01Lista.Serie, 2, false);
                return itemLista.Id;
            }
            catch (Exception ex)
            {
                _mdfelogService.InserirLogErro(mdfe, "InserirMdfeEnvioFilaProc", string.Format("InserirMdfeEnvioFilaProcessamento: Erro na inserção da fila de processamento {0}", ex.Message));
                throw;
            }
        }

        /// <summary>
        /// Insere na fila da processamento o MDF-e
        /// </summary>
        /// <param name="mdfe">MDF-e que será enviado</param>
        /// <returns>Retorna o id da lista</returns>
        public int InserirMdfeEncerramentoFilaProcessamento(Mdfe mdfe)
        {
            Mdfe01Lista mdfe01Lista = null;
            CidadeIbge cidadeDestino = null;
            Nfe03Filial nfe03Filial;
            Nfe03Filial nfe03FilialEncerramento;
            IList<MdfeArvore> listaArvore = null;
            CidadeIbge cidadeEmitente = null;
            MdfeArvore mdfeFilho = null;

            try
            {
                mdfe01Lista = new Mdfe01Lista();
                nfe03Filial = ObterIdentificadorFilial(mdfe);
                listaArvore = _mdfeArvoreRepository.ObterArvorePorMdfeFilho(mdfe);
                mdfeFilho = listaArvore.Where(m => m.MdfePai.Id == mdfe.Id).FirstOrDefault();

                nfe03FilialEncerramento = ObterIdentificadorFilial(mdfeFilho != null ? mdfeFilho.MdfeFilho : mdfe);
                cidadeEmitente = _cidadeIbgeRepository.ObterPorDescricaoUf(nfe03FilialEncerramento.Cidade.ToUpper().Trim(), mdfeFilho != null ? mdfeFilho.MdfeFilho.SiglaUfFerrovia : mdfe.SiglaUfFerrovia);

                // Grava os dados chaves para identificação do Mdfe
                mdfe01Lista.Numero = int.Parse(mdfe.Numero);
                mdfe01Lista.NumeroFim = int.Parse(mdfe.Numero);
                mdfe01Lista.Serie = mdfe.Serie;
                mdfe01Lista.IdFilial = (int)nfe03Filial.IdFilial;

                mdfe01Lista.UfIbge = nfe03Filial.Uf;
                mdfe01Lista.Tipo = 8;
                mdfe01Lista.CnpjEmitente = nfe03Filial.Cnpj;
                mdfe01Lista.TipoAmbiente = ObterAmbienteSefaz(mdfe);
                mdfe01Lista.VersaoSefaz = double.Parse(mdfe.Versao.CodigoVersao, CultureInfo.GetCultureInfo("en-US"));
                mdfe01Lista.TipoImpressao = 1;
                mdfe01Lista.Modelo = "58";
                mdfe01Lista.Lock = 0;
                mdfe01Lista.TipoEmissao = 1;
                // mdfe01Lista.Protocolo = long.Parse(mdfe.NumeroProtocolo);
                // mdfe01Lista.DataRecibo = mdfe.DataRecibo;
                mdfe01Lista.DataEncerramentoProc = DateTime.Now;
                mdfe01Lista.UfEncerramento = nfe03FilialEncerramento.Uf;
                mdfe01Lista.CodigoMunicipioEncerramento = cidadeEmitente.CodigoIbge;
                mdfe01Lista.Justificativa = "Teste de encerramento de MDF-e.";
                mdfe01Lista.DataEmissao = GetDhEmissao(mdfe);

                _mdfe01ListaRepository.Inserir(mdfe01Lista);

                // Obs: Como a config implementa uma trigger para gerar a sequence dessa tabela, 
                // foi alterado o mapeamento e é realizado um novo sql para ver qual id foi gerado
                Mdfe01Lista itemLista = _mdfe01ListaRepository.ObterListaFilaProcessamento((int)nfe03Filial.IdFilial, mdfe01Lista.Numero, mdfe01Lista.Serie, 8, false);
                return itemLista.Id;
            }
            catch (Exception ex)
            {
                _mdfelogService.InserirLogErro(mdfe, "InserirMdfeEnvioFilaProc", string.Format("InserirMdfeEnvioFilaProcessamento: Erro na inserção da fila de processamento {0}", ex.Message));
                throw;
            }
        }

        /// <summary>
        /// Insere na geração do arquivo PDF por impressão
        /// </summary>
        /// <param name="mdfe">Mdfe para ser gerado o arquivo PDF</param>
        public void InserirGeracaoPdfPorImpressao(Mdfe mdfe)
        {
            Nfe03Filial filial = ObterIdentificadorFilial(mdfe);

            Mdfe50Imp mdfe50Impressao = new Mdfe50Imp();
            mdfe50Impressao.IdFilial = (int)filial.IdFilial;
            mdfe50Impressao.Numero = Convert.ToInt32(mdfe.Numero);
            mdfe50Impressao.Serie = mdfe.Serie;
            mdfe50Impressao.IdImp = 0;
            mdfe50Impressao.LocalImpressora = "LOCAL_PADRAO";
            mdfe50Impressao.NomeImpressora = "TESTE";
            mdfe50Impressao.NumCopiasDocumento = 1;
            mdfe50Impressao.IndImpFrenteVerso = 0;
            mdfe50Impressao.QtdeCopiasAdicionais = 1;
            mdfe50Impressao.NumBandeja = "1";

            _mdfe50ImpRepository.Inserir(mdfe50Impressao);
        }

        /// <summary>
        /// Obtém a filial
        /// </summary>
        /// <param name="mdfe">Objeto Mdfe</param>
        /// <returns>Retorna a filial</returns>
        public Nfe03Filial ObterIdentificadorFilial(Mdfe mdfe)
        {
            long cnpj;
            Int64.TryParse(mdfe.CnpjFerrovia, out cnpj);

            return _nfe03FilialRepository.ObterFilialPorCnpj(cnpj);
        }

        /// <summary>
        /// Recupera a hora de acordo com o fuso horario.
        /// </summary>
        /// <param name="mdfe">mdfe a ser inserido na fila</param>
        /// <returns>Retorna a hora de acordo com o fuso horario.</returns>
        private DateTime GetDhEmissao(Mdfe mdfe)
        {
            if (mdfe.SiglaUfFerrovia == "MS" || mdfe.SiglaUfFerrovia == "MT")
            {
                return mdfe.DataHora.AddHours(-1);
            }
            else
            {
                return mdfe.DataHora;
            }
        }

        /// <summary>
        /// Obtem o Ambiente da Sefaz
        /// (1) - Produção
        /// (2) - Homologação
        /// </summary>
        /// <param name="mdfe">Objeto mdfe.</param>
        /// <returns>Retorna o ambiente da Sefaz</returns>
        private int ObterAmbienteSefaz(Mdfe mdfe)
        {
            var origem = mdfe.Origem.EstacaoMae ?? mdfe.Origem;

            MdfeSerieEmpresaUf serieEmpresaUf = _mdfeSerieEmpresaUfRepository.ObterPorEmpresaUf(origem.EmpresaConcessionaria, origem.Municipio.Estado.Sigla);

            if (serieEmpresaUf == null)
            {
                throw new Exception("Não foi possível recuperar o tipo de ambiente da Sefaz");
            }

            return serieEmpresaUf.ProducaoSefaz ? 1 : 2;
        }
    }
}