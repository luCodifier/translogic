﻿namespace Translogic.Modules.Core.Domain.Services.Mdfes
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using Castle.Services.Transaction;
    using Model.FluxosComerciais.Mdfes;
    using Model.Trem;
    using Model.Diversos;
    using Model.Diversos.Cte;
    using Model.Diversos.Ibge;
    using Model.Diversos.Ibge.Repositories;
    using Model.FluxosComerciais;
    using Model.FluxosComerciais.ConhecimentoTransporteEletronico;
    using Model.FluxosComerciais.Mdfes.Repositories;
    using Model.FluxosComerciais.Mdfes.Repositories.TriangulusConfig;
    using Model.FluxosComerciais.Mdfes.TriangulusConfig;
    using Model.FluxosComerciais.TriangulusConfig;
    using Model.FluxosComerciais.TriangulusConfig.Repositories;
    using Model.Trem.Veiculo.Vagao;
    using Model.Via;
    using Model.Via.Circulacao;

    /// <summary>
    /// Grava dos dados de envio do MDF-e na Config
    /// </summary>
    [Transactional]
    public class GravarConfigDadosEnvioMdfeService
    {
        private readonly INfe03FilialRepository _nfe03FilialRepository;
        private readonly IUfIbgeRepository _estadoIbgeRepository;
        private readonly ICidadeIbgeRepository _cidadeIbgeRepository;
        private readonly IMdfe01InfRepository _mdfe01InfRepository;
        private readonly IMdfe02InfMunCarregaRepository _mdfe02InfMunCarregaRepository;
        private readonly IMdfe03InfPercursoRepository _mdfe03InfPercursoRepository;
        private readonly IMdfe09FerroRepository _mdfe09FerroRepository;
        private readonly IMdfe10FerroVagRepository _mdfe10FerroVagRepository;
        private readonly IMdfe11InfDocRepository _mdfe11InfDocRepository;
        private readonly IMdfe12InfCteRepository _mdfe12InfCteRepository;
        private readonly IMdfe13InfCtRepository _mdfe13InfCtRepository;
        private readonly IMdfe14InfNfeRepository _mdfe14InfNfeRepository;
        private readonly IMdfe15InfNfRepository _mdfe15InfNfRepository;
        private readonly IMdfe01ListaRepository _mdfe01ListaRepository;
        private readonly IMdfeRepository _mdfeRepository;
        private readonly IMdfeComposicaoRepository _mdfeComposicaoRepository;
        private readonly IMdfeLogRepository _mdfeLogRepository;
        private readonly IMdfeSerieEmpresaUfRepository _mdfeSerieEmpresaUfRepository;
        private Nfe03Filial _nfe03Filial;
        private Mdfe _mdfe;
        private IList<MdfeComposicao> _listaComposicao;

        /// <summary>
        /// Construtor da classe
        /// </summary>
        /// <param name="nfe03FilialRepository">Repositório da classe Nfe03Filial injetado.</param>
        /// <param name="estadoIbgeRepository">Repositório da classe estadoIbgeRepository injetado.</param>
        /// <param name="cidadeIbgeRepository">Repositório da classe cidadeIbgeRepository injetado.</param>
        /// <param name="mdfe01InfRepository">Repositório da classe mdfe01InfRepository injetado.</param>
        /// <param name="mdfe02InfMunCarregaRepository">Repositório da classe mdfe02InfMunCarregaRepository injetado.</param>
        /// <param name="mdfe03InfPercursoRepository">Repositório da classe mdfe03InfPercursoRepository injetado.</param>
        /// <param name="mdfe09FerroRepository">Repositório da classe mdfe09FerroRepository injetado.</param>
        /// <param name="mdfe11InfDocRepository">Repositório da classe mdfe11InfDocRepository injetado.</param>
        /// <param name="mdfe12InfCteRepository">Repositório da classe mdfe12InfCteRepository injetado.</param>
        /// <param name="mdfe13InfCtRepository">Repositório da classe mdfe13InfCtRepository injetado.</param>
        /// <param name="mdfe14InfNfeRepository">Repositório da classe mdfe14InfNfeRepository injetado.</param>
        /// <param name="mdfe15InfNfRepository">Repositório da classe mdfe15InfNfRepository injetado.</param>
        /// <param name="mdfe01ListaRepository">Repositório da classe mdfe01ListaRepository injetado.</param>
        /// <param name="mdfe10FerroVagRepository">Repositório da classe mdfe10FerroVagRepository injetado.</param>
        /// <param name="mdfeRepository">Repositório da classe mdfeRepository injetado.</param>
        /// <param name="mdfeComposicaoRepository">Repositório da classe mdfeComposicaoRepository injetado.</param>
        /// <param name="mdfeLogRepository">Repositório da classe mdfeLogRepository injetado.</param>
        /// <param name="mdfeSerieEmpresaUfRepository">Repositório da classe mdfeSerieEmpresaUfRepository injetado.</param>
        public GravarConfigDadosEnvioMdfeService(INfe03FilialRepository nfe03FilialRepository, IUfIbgeRepository estadoIbgeRepository, ICidadeIbgeRepository cidadeIbgeRepository, IMdfe01InfRepository mdfe01InfRepository, IMdfe02InfMunCarregaRepository mdfe02InfMunCarregaRepository, IMdfe03InfPercursoRepository mdfe03InfPercursoRepository, IMdfe09FerroRepository mdfe09FerroRepository, IMdfe11InfDocRepository mdfe11InfDocRepository, IMdfe12InfCteRepository mdfe12InfCteRepository, IMdfe13InfCtRepository mdfe13InfCtRepository, IMdfe14InfNfeRepository mdfe14InfNfeRepository, IMdfe15InfNfRepository mdfe15InfNfRepository, IMdfe01ListaRepository mdfe01ListaRepository, IMdfe10FerroVagRepository mdfe10FerroVagRepository, IMdfeRepository mdfeRepository, IMdfeComposicaoRepository mdfeComposicaoRepository, IMdfeLogRepository mdfeLogRepository, IMdfeSerieEmpresaUfRepository mdfeSerieEmpresaUfRepository)
        {
            _nfe03FilialRepository = nfe03FilialRepository;
            _mdfeSerieEmpresaUfRepository = mdfeSerieEmpresaUfRepository;
            _mdfeLogRepository = mdfeLogRepository;
            _mdfeComposicaoRepository = mdfeComposicaoRepository;
            _mdfeRepository = mdfeRepository;
            _mdfe10FerroVagRepository = mdfe10FerroVagRepository;
            _mdfe01ListaRepository = mdfe01ListaRepository;
            _mdfe15InfNfRepository = mdfe15InfNfRepository;
            _mdfe14InfNfeRepository = mdfe14InfNfeRepository;
            _mdfe13InfCtRepository = mdfe13InfCtRepository;
            _mdfe12InfCteRepository = mdfe12InfCteRepository;
            _mdfe11InfDocRepository = mdfe11InfDocRepository;
            _mdfe09FerroRepository = mdfe09FerroRepository;
            _mdfe03InfPercursoRepository = mdfe03InfPercursoRepository;
            _mdfe02InfMunCarregaRepository = mdfe02InfMunCarregaRepository;
            _mdfe01InfRepository = mdfe01InfRepository;
            _cidadeIbgeRepository = cidadeIbgeRepository;
            _estadoIbgeRepository = estadoIbgeRepository;
        }

        /// <summary>
        /// Executa a gravação dos dados na Config
        /// </summary>
        /// <param name="mdfe">Dados do MDF-e</param>
        public void Executar(Mdfe mdfe)
        {
            try
            {
                _mdfe = mdfe;
                _nfe03Filial = null;
                RemoverMdfeConfigParaReenvio();

                Executar();
            }
            catch (Exception ex)
            {
                _mdfeLogRepository.InserirLogErro(_mdfe, "GravarConfigEnvioService", string.Format("Executar: {0}", ex.Message));
                throw;
            }
        }

        /// <summary>
        /// Remove o registro das tabelas caso já exista, utilizado quando é reenvio
        /// </summary>
        public void RemoverMdfeConfigParaReenvio()
        {
            _mdfe01InfRepository.Remover(ObterIdentificadorFilial(), int.Parse(_mdfe.Numero), _mdfe.Serie);
            _mdfe02InfMunCarregaRepository.Remover(ObterIdentificadorFilial(), int.Parse(_mdfe.Numero), _mdfe.Serie);
            _mdfe03InfPercursoRepository.Remover(ObterIdentificadorFilial(), int.Parse(_mdfe.Numero), _mdfe.Serie);
            _mdfe09FerroRepository.Remover(ObterIdentificadorFilial(), int.Parse(_mdfe.Numero), _mdfe.Serie);
            _mdfe10FerroVagRepository.Remover(ObterIdentificadorFilial(), int.Parse(_mdfe.Numero), _mdfe.Serie);
            _mdfe11InfDocRepository.Remover(ObterIdentificadorFilial(), int.Parse(_mdfe.Numero), _mdfe.Serie);
            _mdfe12InfCteRepository.Remover(ObterIdentificadorFilial(), int.Parse(_mdfe.Numero), _mdfe.Serie);
            _mdfe13InfCtRepository.Remover(ObterIdentificadorFilial(), int.Parse(_mdfe.Numero), _mdfe.Serie);
            _mdfe14InfNfeRepository.Remover(ObterIdentificadorFilial(), int.Parse(_mdfe.Numero), _mdfe.Serie);
            _mdfe15InfNfRepository.Remover(ObterIdentificadorFilial(), int.Parse(_mdfe.Numero), _mdfe.Serie);
        }

        /// <summary>
        ///  Executar a gravação do mdfe em um método transacional
        /// </summary>
        [Transaction]
        protected virtual void Executar()
        {
            try
            {
                _listaComposicao = _mdfeComposicaoRepository.ObterPorMdfe(_mdfe);
                // Foi adicionado a comparação da Sigla da Ferrovia para que enviasse somente os vagões por mdfe e UF. Com isso a DAMDFE na nota apresenta somente a quantidade de vagoes de cada estação, 
                // ou seja,: em MT tinha 12 vagoes na nota vai aparecer os 12, se em MS teve uma composição então so será apresentado aquela composição na nota.
                _listaComposicao = _listaComposicao.Where(c => c.Cte != null && c.Cte.SituacaoAtual == SituacaoCteEnum.Autorizado && c.Cte.SiglaUfFerrovia.Equals(_mdfe.SiglaUfFerrovia)).OrderBy(x => x.Sequencia).ToList();

                if (_listaComposicao.Count < 1)
                {
                    throw new Exception("Trem não possui CT-e.");
                }

                ObterIdentificadorFilial();
                GravarDadosPrincipaisMdfe();
                GravarDadosInfMunicipioCarregamento();
                GravarDadosInfPercurso();
                GravarDadosFerroviario();
                GravarDadosFerroviarioVagao();
                GravarDadosInfDocumentos();
            }
            catch (Exception ex)
            {
                _mdfeLogRepository.InserirLogErro(_mdfe, "GravarConfigEnvioService", string.Format("Executar: {0}", ex.Message));

                throw;
            }
        }

        /// <summary>
        /// Tabela utilizada para dados principais da MDF-e (1-1)
        /// </summary>
        private void GravarDadosPrincipaisMdfe()
        {
            Mdfe01Inf mdfe01Inf = null;
            UfIbge estadoIbgeOrigemPorSigla = null;
            CidadeIbge cidadeEmitente = null;

            try
            {
                // Instância de objeto
                mdfe01Inf = new Mdfe01Inf();

                estadoIbgeOrigemPorSigla = !string.IsNullOrWhiteSpace(_mdfe.SiglaUfFerrovia)
                    ? _estadoIbgeRepository.ObterPorSigla(_mdfe.SiglaUfFerrovia)
                    : _estadoIbgeRepository.ObterPorSigla(_mdfe.Origem.EstacaoMae == null ? _mdfe.Origem.Municipio.Estado.Sigla : _mdfe.Origem.EstacaoMae.Municipio.Estado.Sigla);

                cidadeEmitente = _cidadeIbgeRepository.ObterPorDescricaoUf(_nfe03Filial.Cidade.ToUpper().Trim(), _mdfe.SiglaUfFerrovia);

                // Grava os dados chaves para identificação do Mdfe
                mdfe01Inf.CfgDoc = int.Parse(_mdfe.Numero);
                mdfe01Inf.CfgSerie = _mdfe.Serie;
                mdfe01Inf.CfgUn = ObterIdentificadorFilial();
                mdfe01Inf.InfMdfeVersao = double.Parse(_mdfe.Versao.CodigoVersao, CultureInfo.GetCultureInfo("en-US"));
                mdfe01Inf.IdeCuf = estadoIbgeOrigemPorSigla.Id;

                // Número do Manifesto
                mdfe01Inf.IdeNmdf = int.Parse(_mdfe.Numero);

                // Versão do leiaute específico para o Modal
                mdfe01Inf.InfModalVersao = double.Parse(_mdfe.Versao.CodigoVersao, CultureInfo.GetCultureInfo("en-US"));

                mdfe01Inf.IdeSerie = int.Parse(_mdfe.Serie);

                // Recupera o código gerado aleatóriamente na chave do mdf-e.
                mdfe01Inf.IdeCmdf = int.Parse(_mdfe.Chave.Substring(35, 8));

                // 1 - Produção; 2 - Homologação
                mdfe01Inf.IdeTpAmb = _mdfe.AmbienteSefaz;

                // Utilizar o código 58 para identificação do MDF-e
                mdfe01Inf.IdeMod = 58;

                // 1 - Prestação de serviço de transporte; 2 - Não prestador de serviço de transporte
                mdfe01Inf.IdeTpEmit = 1;

                // Data e Hora de emissão do Manifesto
                mdfe01Inf.IdeDhEmi = GetDhEmissao();

                // Modalidade de transporte - 1 - Rodoviário; 2 - Aéreo; 3 - Aquaviário; 4 - Ferroviário
                mdfe01Inf.IdeModal = 4;

                // Forma de emissão do Manifesto (Normal ou Contingência)  1 - Normal; 2 - Contingência
                mdfe01Inf.IdeTpEmis = 1;

                // Identificação do processo de emissão do Manifesto 
                // 0 - emissão de MDF-e com aplicativo do contribuinte; 3 - emissão MDF-e pelo contribuinte com aplicativo fornecido pelo Fisco
                mdfe01Inf.IdeProcEmi = 0;

                mdfe01Inf.IdeUfIni = estadoIbgeOrigemPorSigla.Sigla;

                // Destino do Trem
                //mdfe01Inf.IdeUfFim = _mdfe.Trem.Destino.Municipio.Estado.Sigla;

                //Busca OBJETO MDFE por ID 
                var objmdtfe = _mdfeRepository.ObterPorId((_mdfe.Id.Value));

                /* Colocar o destino do CTE do MDFE Do trem que está na árvore*/
                if (objmdtfe.Trem != null)
                {
                    var ufDestinoCteDoMdfe = _mdfeComposicaoRepository.BuscarUfDosCteComposicaoMdfe(_mdfe.Id.Value, objmdtfe.Trem.Id.Value);
                    mdfe01Inf.IdeUfFim = ufDestinoCteDoMdfe;
                }
                else
                {
                    mdfe01Inf.IdeUfFim = _listaComposicao.First().Cte.FluxoComercial.Destino.Municipio.Estado.Sigla;
                }

                mdfe01Inf.EmitCnpj = _nfe03Filial.Cnpj;

                mdfe01Inf.EmitIe = long.Parse(_nfe03Filial.Ie);

                if (!string.IsNullOrEmpty(_nfe03Filial.RazSoc))
                {
                    mdfe01Inf.EmitXnome = _nfe03Filial.RazSoc.Trim();
                }

                mdfe01Inf.EmitXfant = _nfe03Filial.NomeReduzido;

                if (!string.IsNullOrEmpty(_nfe03Filial.Endereco))
                {
                    mdfe01Inf.EnderEmitXlgr = _nfe03Filial.Endereco.Trim();
                }

                if (_nfe03Filial.NrLogr != 0)
                {
                    // Número
                    mdfe01Inf.EnderEmitNro = _nfe03Filial.NrLogr.ToString();
                }
                else
                {
                    mdfe01Inf.EnderEmitNro = "S/N";
                }

                // Complemento do endereço
                mdfe01Inf.EnderEmitXcpl = _nfe03Filial.Compl;

                // Bairro do endereço
                mdfe01Inf.EnderEmitXbairro = _nfe03Filial.Bairro;

                if (cidadeEmitente != null)
                {
                    // Código IBGE do município
                    mdfe01Inf.EnderEmitCmun = cidadeEmitente.CodigoIbge;

                    if (cidadeEmitente.CodigoIbge == 9999999)
                    {
                        // Nome do município 
                        mdfe01Inf.EnderEmitXmun = "EXTERIOR";
                        // Código UF 
                        mdfe01Inf.EnderEmitUf = "EX";
                    }
                    else
                    {
                        // Nome do município 
                        mdfe01Inf.EnderEmitXmun = cidadeEmitente.Descricao;
                        // Código UF 
                        mdfe01Inf.EnderEmitUf = _mdfe.SiglaUfFerrovia;
                    }
                }

                if (_nfe03Filial.Cep.HasValue)
                {
                    // Cep 
                    mdfe01Inf.EnderEmitCep = _nfe03Filial.Cep.Value;
                }

                // Telefone
                if (_nfe03Filial.Telefone != null && _nfe03Filial.Telefone != 0)
                {
                    mdfe01Inf.EnderEmitFone = _nfe03Filial.Telefone.Value;
                }

                // Todo: Verificar preenchimento campo e-mail
                mdfe01Inf.EnderEmitEmail = string.Empty;

                mdfe01Inf.TotQcte = _listaComposicao.Where(d => d.Cte != null).Count();

                mdfe01Inf.TotVcarga = Math.Round(_listaComposicao.Sum(c => c.Cte == null ? 0 : c.Cte.ValorCte), 2);

                mdfe01Inf.TotQcarga = _listaComposicao.Sum(c => c.Cte == null ? 0 : c.Cte.PesoParaCalculo).Value;

                mdfe01Inf.TotCunid = 2;

                _mdfe01InfRepository.Inserir(mdfe01Inf);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Tabela utilizada para informações dos Municípios de Carregamento (1-50)
        /// </summary>
        private void GravarDadosInfMunicipioCarregamento()
        {
            Mdfe02InfMunCarrega mdfe02InfMunCarrega = null;
            int idInfMunCarrega = 0;
            CidadeIbge cidadeIbge = null;
            FluxoComercial fluxo = null;
            try
            {
                List<EstacaoMae> listaCarregamentos = _listaComposicao.Select(c => c.Cte == null ? null : c.Cte.FluxoComercial.Origem ?? c.Cte.FluxoComercial.OrigemIntercambio).Distinct().ToList();

                var municipiosCarregados = listaCarregamentos.Select(d => d.Municipio).Distinct().ToList();

                List<int> listaProcessados = new List<int>();

                foreach (var municipioCarregamento in municipiosCarregados)
                {
                    if (municipioCarregamento != null)
                    {
                        cidadeIbge = municipioCarregamento.CidadeIbge;

                        if (!listaProcessados.Any(x => x == cidadeIbge.CodigoIbge))
                        {
                            // Somente as cidades daquele mdfe e estado, pois estava dando problema, onde enviava todas as informações para 3 xml de mdfe's diferentes.
                            if (cidadeIbge.SiglaEstado.Equals(_mdfe.SiglaUfFerrovia))
                            {
                                mdfe02InfMunCarrega = new Mdfe02InfMunCarrega();

                                // Grava os dados chaves para identificação do Mdfe
                                mdfe02InfMunCarrega.CfgDoc = int.Parse(_mdfe.Numero);
                                mdfe02InfMunCarrega.CfgSerie = _mdfe.Serie;
                                mdfe02InfMunCarrega.CfgUn = ObterIdentificadorFilial();

                                // Preenche
                                mdfe02InfMunCarrega.IdInfMunCarega = idInfMunCarrega;
                                mdfe02InfMunCarrega.InfNumCarregaCmunCarrega = cidadeIbge.CodigoIbge;
                                mdfe02InfMunCarrega.InfNumCarregaXmumCarrega = cidadeIbge.Descricao;

                                _mdfe02InfMunCarregaRepository.Inserir(mdfe02InfMunCarrega);

                                listaProcessados.Add(cidadeIbge.CodigoIbge);

                                idInfMunCarrega++;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Tabela utilizada para informações do percurso do MDF-e (0-25)
        /// </summary>
        private void GravarDadosInfPercurso()
        {
            if (_mdfe.Trem == null)
                return;

            Mdfe03InfPercurso mdfe03InfPercurso = null;
            Rota rota = _mdfe.Trem.Rota;
            int idInfPercurso = 0;

            try
            {
                List<string> estadosRotas = rota.Detalhe.Select(r => r.AreaOperacional.Municipio.CidadeIbge.SiglaEstado).Distinct().ToList();

                foreach (string estadoRota in estadosRotas)
                {
                    mdfe03InfPercurso = new Mdfe03InfPercurso();

                    // Grava os dados chaves para identificação do Mdfe
                    mdfe03InfPercurso.CfgDoc = int.Parse(_mdfe.Numero);
                    mdfe03InfPercurso.CfgSerie = _mdfe.Serie;
                    mdfe03InfPercurso.CfgUn = ObterIdentificadorFilial();

                    mdfe03InfPercurso.IdInfPercurso = idInfPercurso;
                    mdfe03InfPercurso.InfPercursoUfPer = estadoRota;

                    _mdfe03InfPercursoRepository.Inserir(mdfe03InfPercurso);
                    idInfPercurso++;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Tabela utilizada para informações do modal Ferroviário (0-1)
        /// </summary>
        private void GravarDadosFerroviario()
        {
            Mdfe09Ferro mdfe09Ferro = null;
            Trem trem = _mdfe.Trem;

            try
            {
                mdfe09Ferro = new Mdfe09Ferro();
                List<MdfeComposicao> qtdeVagao = _listaComposicao.Where(c => c.Cte != null && c.Cte.SituacaoAtual == SituacaoCteEnum.Autorizado).ToList();

                // Grava os dados chaves para identificação do Mdfe
                mdfe09Ferro.CfgDoc = int.Parse(_mdfe.Numero);
                mdfe09Ferro.CfgSerie = _mdfe.Serie;
                mdfe09Ferro.CfgUn = ObterIdentificadorFilial();

                if (trem != null)
                {
                    mdfe09Ferro.TremXpref = trem.Prefixo;
                    mdfe09Ferro.TremDhTrem = trem.DataLiberacao.Value;
                    mdfe09Ferro.TremXori = trem.Origem.Codigo;
                    mdfe09Ferro.TremXdest = trem.Destino.Codigo;
                    mdfe09Ferro.TremQvag = qtdeVagao.Count;
                }
                else
                {
                    var fluxo = _listaComposicao.First().Cte.FluxoComercial;
                    mdfe09Ferro.TremXpref = _mdfe.PrefixoTrem;
                    mdfe09Ferro.TremDhTrem = _mdfe.DataHora;
                    mdfe09Ferro.TremXori = fluxo.Origem.Codigo;
                    mdfe09Ferro.TremXdest = fluxo.Destino.Codigo;
                    mdfe09Ferro.TremQvag = 1;
                }

                _mdfe09FerroRepository.Inserir(mdfe09Ferro);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Tabela utilizada para informações dos Vagões (1-n)
        /// </summary>
        private void GravarDadosFerroviarioVagao()
        {
            Mdfe10FerroVag mdfe10FerroVag = null;
            List<MdfeComposicao> listaComposicao = _listaComposicao.Where(c => c.Cte != null && c.Cte.SituacaoAtual == SituacaoCteEnum.Autorizado).ToList();
            Vagao vagao = null;
            int idVagao = 0;

            try
            {
                foreach (MdfeComposicao composicao in listaComposicao)
                {
                    vagao = composicao.Vagao;

                    mdfe10FerroVag = new Mdfe10FerroVag();

                    // Grava os dados chaves para identificação do Mdfe
                    mdfe10FerroVag.CfgDoc = int.Parse(_mdfe.Numero);
                    mdfe10FerroVag.CfgSerie = _mdfe.Serie;
                    mdfe10FerroVag.CfgUn = ObterIdentificadorFilial();

                    mdfe10FerroVag.IdVag = idVagao;
                    mdfe10FerroVag.VagSerie = vagao.Serie.Codigo;
                    mdfe10FerroVag.VagNvag = int.Parse(vagao.Codigo);
                    mdfe10FerroVag.VagNseq = composicao.Sequencia;
                    mdfe10FerroVag.VagTu = Math.Round(composicao.Cte == null ? 0 : composicao.Cte.PesoParaCalculo.Value, 3);

                    // gravar as informações da mesma forma que gravava no método GravarInformacoesDetVag do cte.
                    mdfe10FerroVag.VagPesoBc = composicao.Cte == null ? 0 : composicao.Cte.PesoParaCalculo ?? 0;
                    mdfe10FerroVag.VagPesoR = composicao.Cte == null ? 0 : composicao.Cte.PesoVagao ?? 0;
                    mdfe10FerroVag.VagTpVag = vagao.Serie.Codigo.Trim();

                    _mdfe10FerroVagRepository.Inserir(mdfe10FerroVag);
                    idVagao++;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Tabela utilizada para informações dos Documentos fiscais vinculados ao manifesto (1-100)
        /// </summary>
        private void GravarDadosInfDocumentos()
        {
            Mdfe11InfDoc mdfe11InfDoc = null;
            int idInfDoc = 0;
            EstacaoMae destino = null;
            CidadeIbge cidadeIbge = null;
            FluxoComercial fluxo = null;
            try
            {
                IEnumerable<EstacaoMae> estacoes = _listaComposicao.Select(c => c.Cte.FluxoComercial.Destino ?? c.Cte.FluxoComercial.DestinoIntercambio).Distinct();

                var municipios = estacoes.Select(d => d.Municipio.Estado.Sigla.Equals("EX") ? _mdfe.Trem.Destino.Municipio : d.Municipio).Distinct().ToList();

                List<int> listaProcessados = new List<int>();
                Trem trem = _mdfe.Trem;

                foreach (Municipio municipio in municipios)
                {
                    if (municipio != null)
                    {
                        cidadeIbge = municipio.CidadeIbge;
                        mdfe11InfDoc = new Mdfe11InfDoc();

                        if (!listaProcessados.Any(x => x == cidadeIbge.CodigoIbge))
                        {
                            // Grava os dados chaves para identificação do Mdfe
                            mdfe11InfDoc.CfgDoc = int.Parse(_mdfe.Numero);
                            mdfe11InfDoc.CfgSerie = _mdfe.Serie;
                            mdfe11InfDoc.CfgUn = ObterIdentificadorFilial();

                            // Preenche os Município de Descarregamento
                            mdfe11InfDoc.IdInfDoc = idInfDoc;
                            mdfe11InfDoc.InfMunDescargaCmunDescarga = cidadeIbge.CodigoIbge;
                            mdfe11InfDoc.InfMunDescargaXmunDescarga = cidadeIbge.Descricao;

                            _mdfe11InfDocRepository.Inserir(mdfe11InfDoc);

                            listaProcessados.Add(municipio.CidadeIbge.CodigoIbge);

                            GravarDadosInfCte(idInfDoc, municipio);
                            idInfDoc++;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Tabela utilizada para informações dos Conhecimento de Transporte (0-2000)
        /// </summary>
        /// <param name="idInfDoc">Código do documento.</param>
        /// <param name="municipio">Composicao do Mdf-e</param>
        private void GravarDadosInfCte(int idInfDoc, Municipio municipio)
        {
            // Declaração de Variáveis
            Mdfe12InfCte mdfe12InfCte = null;
            int idInfcte = 0;
            try
            {
                IEnumerable<Cte> ctes = _listaComposicao.Select(c => c.Cte).Where(c => c.FluxoComercial.Destino.Municipio == municipio || (c.FluxoComercial.Destino.Municipio.Estado.Sigla.Equals("EX") && _mdfe.Trem.Destino.Municipio == municipio));

                foreach (Cte cte in ctes)
                {
                    mdfe12InfCte = new Mdfe12InfCte();

                    // Grava os dados chaves para identificação do Mdfe
                    mdfe12InfCte.CfgDoc = int.Parse(_mdfe.Numero);
                    mdfe12InfCte.CfgSerie = _mdfe.Serie;
                    mdfe12InfCte.CfgUn = ObterIdentificadorFilial();

                    // Grava os Dados do CT-e
                    mdfe12InfCte.IdInfDoc = idInfDoc;
                    mdfe12InfCte.IdInfCte = idInfcte;
                    mdfe12InfCte.InfCteChCte = cte.Chave;

                    _mdfe12InfCteRepository.Inserir(mdfe12InfCte);
                    idInfcte++;
                }
                // Grava os dados da Nota Fiscal
                // GravarDadosNotaFiscal(composicao.Cte, idInfDoc);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Grava os dadods da nota fiscal
        /// </summary>
        /// <param name="cte">Objeto Cte.</param>
        /// <param name="idInfDoc">Código do documento.</param>
        private void GravarDadosNotaFiscal(Cte cte, int idInfDoc)
        {
            int idInfNfe = 0;
            if (cte != null)
            {
                // Percorre as Notas.
                foreach (CteDetalhe cteDetalhe in cte.ListaDetalhes)
                {
                    // Verifica se é NF-e
                    if (string.IsNullOrEmpty(cteDetalhe.ChaveNfe))
                    {
                        // Grava os dados Nota Manual
                        GravarDadosInfNf(cteDetalhe, idInfDoc, idInfNfe);
                    }
                    else
                    {
                        // Grava os dados NF-e
                        GravarDadosInfNfe(cteDetalhe, idInfDoc, idInfNfe);
                    }

                    idInfNfe++;
                }
            }
        }

        /// <summary>
        /// Tabela utilizada para informações de Nota Fiscal Eletrônica (0-2000)
        /// </summary>
        /// <param name="cteDet">Detalhes do Cte</param>
        /// <param name="idNfDoc">código do Documento</param>
        /// <param name="idInfNfe">Código da NF-e</param>
        private void GravarDadosInfNfe(CteDetalhe cteDet, int idNfDoc, int idInfNfe)
        {
            // Declaração de variáveis.
            Mdfe14InfNfe mdfe14InfNfe = null;

            try
            {
                // Percorre as Notas.
                mdfe14InfNfe = new Mdfe14InfNfe();

                // Grava os dados chaves para identificação do Mdfe
                mdfe14InfNfe.CfgDoc = int.Parse(_mdfe.Numero);
                mdfe14InfNfe.CfgSerie = _mdfe.Serie;
                mdfe14InfNfe.CfgUn = ObterIdentificadorFilial();

                // Preenche os dados da nota
                mdfe14InfNfe.IdInfDoc = idNfDoc;
                mdfe14InfNfe.IdInfNfe = idInfNfe;
                mdfe14InfNfe.InfNfeChNfe = cteDet.ChaveNfe;

                _mdfe14InfNfeRepository.Inserir(mdfe14InfNfe);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Tabela utilizada para informações de Nota Fiscal Papel (mod 1 e 1A) (0-2000)
        /// </summary>
        /// <param name="cteDet">Detalhes do Cte</param>
        /// <param name="idNfDoc">código do Documento</param>
        /// <param name="idInfNfe">Código da NF-e</param>
        private void GravarDadosInfNf(CteDetalhe cteDet, int idNfDoc, int idInfNfe)
        {
            // Declaração de variáveis.
            Mdfe15InfNf mdfe15InfNf = null;

            try
            {
                // Percorre as Notas.
                mdfe15InfNf = new Mdfe15InfNf();

                // Grava os dados chaves para identificação do Mdfe
                mdfe15InfNf.CfgDoc = int.Parse(_mdfe.Numero);
                mdfe15InfNf.CfgSerie = _mdfe.Serie;
                mdfe15InfNf.CfgUn = ObterIdentificadorFilial();

                // Preenche os dados da nota
                mdfe15InfNf.IdInfDoc = idNfDoc;
                mdfe15InfNf.IdInfNf = idInfNfe;
                mdfe15InfNf.IdInfCnpj = int.Parse(cteDet.CgcRemetente);
                mdfe15InfNf.IdInfUf = cteDet.UfDestinatario;
                mdfe15InfNf.IdInfNnf = cteDet.NumeroNota;

                // Todo: Verificar serie Nota
                // mdfe15InfNf.IdInfSerie = cteDet.SerieNota;

                mdfe15InfNf.IdInfDemi = cteDet.DataNotaFiscal;
                mdfe15InfNf.IdInfVnf = cteDet.ValorTotalNotaFiscal;

                _mdfe15InfNfRepository.Inserir(mdfe15InfNf);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Retorna o identificador da filial.
        /// </summary>
        /// <returns>Retorna o código do identificador da filial.</returns>
        private int ObterIdentificadorFilial()
        {
            if (_nfe03Filial == null)
            {
                long cnpj;
                Int64.TryParse(_mdfe.CnpjFerrovia, out cnpj);

                _nfe03Filial = _nfe03FilialRepository.ObterFilialPorCnpj(cnpj);

                if (_nfe03Filial != null)
                {
                    return (int)_nfe03Filial.IdFilial;
                }

                // Caso não tenha encontrado a filial
                return -1;
            }

            return (int)_nfe03Filial.IdFilial;
        }

        /// <summary>
        /// Recupera a hora de acordo com o fuso horario.
        /// </summary>
        /// <returns>Retorna a hora de acordo com o fuso horario.</returns>
        private DateTime GetDhEmissao()
        {
            if (_mdfe.SiglaUfFerrovia == "MS" || _mdfe.SiglaUfFerrovia == "MT")
            {
                return _mdfe.DataHora.AddHours(-1);
            }
            else
            {
                return _mdfe.DataHora;
            }
        }

        /// <summary>
        /// Obtem o Ambiente da Sefaz
        /// (1) - Produção
        /// (2) - Homologação
        /// </summary>
        /// <returns>Retorna o ambiente da Sefaz</returns>
        private int ObterAmbienteSefaz()
        {
            IAreaOperacional origem = _mdfe.Origem.EstacaoMae ?? _mdfe.Origem;

            MdfeSerieEmpresaUf serieEmpresaUf = _mdfeSerieEmpresaUfRepository.ObterPorEmpresaUf(origem.EmpresaConcessionaria, origem.Municipio.Estado.Sigla);

            if (serieEmpresaUf == null)
            {
                throw new Exception("Não foi possível recuperar o tipo de ambiente da Sefaz");
            }

            return serieEmpresaUf.ProducaoSefaz ? 1 : 2;
        }
    }
}