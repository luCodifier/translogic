﻿namespace Translogic.Modules.Core.Domain.Services.Mdfes
{
	using Model.FluxosComerciais.Mdfes;

	/// <summary>
	/// Grava os dados de cancelamento do MDF-e na Config
	/// </summary>
	public class GravarConfigDadosCancelamentoMdfeService
	{
		/// <summary>
		/// Executa a gravação dos dados de cancelamento
		/// </summary>
		/// <param name="mdfe">Dados do MDF-e</param>
		 public void Executar(Mdfe mdfe)
		 {
			 /*
			  * OBS: Caso tenha que inserir / atualizar algo no MDF-e antes do cancelamento,
			  * deverá ser implementado nesse método
			  */
		 }
	}
}