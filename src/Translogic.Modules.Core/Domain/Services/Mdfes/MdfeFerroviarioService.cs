﻿namespace Translogic.Modules.Core.Domain.Services.Mdfes
{
    using System.Collections.Generic;
    using System.ServiceModel;
    using System.ServiceModel.Activation;
    using Interface;
    using Model.FluxosComerciais.Mdfes.Repositories;
    using Model.WebServices.MdfeFerroviario;
    using Model.WebServices.MdfeFerroviario.Outputs;

    /// <summary>
    /// Mdfe Ferroviario Service 
    /// </summary>
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceBehavior]
    public class MdfeFerroviarioService : IMdfeFerroviarioService
    {
        private readonly IMdfeFerroviarioRepository _mdfeFerroviarioRepository;

        /// <summary>
        /// Construtor para receber repositorio injetado
        /// </summary>
        /// <param name="imdfeFerroviarioRepository">Repositodio de Mdfe</param>
        public MdfeFerroviarioService(IMdfeFerroviarioRepository imdfeFerroviarioRepository)
        {
            _mdfeFerroviarioRepository = imdfeFerroviarioRepository;
        }

        /// <summary>
        /// Contrato do service
        /// </summary>
        /// <param name="request">Objeto de input EletronicManifestInformationRailRequest</param>
        /// <returns>objeto de output EletronicManifestInformationRailResponse</returns>
        public EletronicManifestInformationRailResponse InformationMdfe(EletronicManifestInformationRailRequest request)
        {
            var dados = new List<EletronicManifestInformationRailOutputDetail>();
            var detail = request.InformationEletronicManifestInformationRail.InformationEletronicManifestInformationRailType.EletronicManifestInformationRailDetail;
            string mensagem;

            if (detail.StartDate > detail.EndDate)
            {
                mensagem = "ERRO-003: Data Inicial deve ser menor que a data Final";
            }
            else
            {
                string dataFim;

                if ((detail.EndDate - detail.StartDate).TotalDays > 30)
                {
                    dataFim =
                       detail.StartDate.
                            AddDays(30).ToShortDateString();
                }
                else
                {
                    dataFim =
                         detail.EndDate.
                             ToShortDateString();
                }

                string dataIni =
                 detail.StartDate.
                        ToShortDateString();

                dados = _mdfeFerroviarioRepository.InformationMdfe(request);
               
                mensagem = "PESQUISA REALIZADA ENTRE OS DIAS " + dataIni + " E " + dataFim;
            }

            var response = new EletronicManifestInformationRailResponse()
                                {
                                    InformationEletronicManifestInformationRailOutput =
                                        new InformationEletronicManifestInformationRailOutput()
                                            {
                                                InformationEletronicManifestInformationRailOutputType =
                                                    new InformationEletronicManifestInformationRailOutputType()
                                                        {
                                                            VersionIdentifier = "1.0",
                                                            EletronicManifestInformationRailDetail = dados,
                                                            ReturnMessage = mensagem
                                                        }
                                            }
                                };

            return response;
        }
    }
}