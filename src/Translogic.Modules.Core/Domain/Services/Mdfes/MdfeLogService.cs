﻿namespace Translogic.Modules.Core.Domain.Services.Mdfes
{
	using System.Text;
	using Model.FluxosComerciais.Mdfes;
	using Model.FluxosComerciais.Mdfes.Repositories;

	/// <summary>
	/// Classe de gravação dos logs do MDF-es
	/// </summary>
	public class MdfeLogService
	{
		private readonly IMdfeLogRepository _mdfeLogRepository;

		/// <summary>
		/// Initializes a new instance of the <see cref="MdfeLogService"/> class.
		/// </summary>
		/// <param name="mdfeLogRepository">Repositório do MDF-e log injetado</param>
		public MdfeLogService(IMdfeLogRepository mdfeLogRepository)
		{
			_mdfeLogRepository = mdfeLogRepository;
		}

		/// <summary>
		/// Insere o log do Cte
		/// </summary>
		/// <param name="mdfe">MDF-e referente ao log</param>
		/// <param name="mensagem">mensagem de log</param>
		public void InserirLogInfo(Mdfe mdfe, string mensagem)
		{
			_mdfeLogRepository.InserirLogInfo(mdfe, mensagem);
		}

		/// <summary>
		/// Insere o log do Cte
		/// </summary>
		/// <param name="mdfe">MDF-e referente ao log</param>
		/// <param name="nomeServico">Nome do servico ou metodo que esta sendo logado</param>
		/// <param name="mensagem">mensagem de log</param>
		public void InserirLogInfo(Mdfe mdfe, string nomeServico, string mensagem)
		{
			_mdfeLogRepository.InserirLogInfo(mdfe, nomeServico, mensagem);
		}

		/// <summary>
		/// Insere o log do Cte
		/// </summary>
		/// <param name="mdfe">MDF-e referente ao log</param>
		/// <param name="builder">mensagem de log</param>
		public void InserirLogInfo(Mdfe mdfe, StringBuilder builder)
		{
			_mdfeLogRepository.InserirLogInfo(mdfe, builder);
		}

		/// <summary>
		/// Insere o log de erro do Cte
		/// </summary>
		/// <param name="mdfe">MDF-e referente ao log</param>
		/// <param name="mensagem">mensagem de log</param>
		public void InserirLogErro(Mdfe mdfe, string mensagem)
		{
			_mdfeLogRepository.InserirLogErro(mdfe, mensagem);
		}

		/// <summary>
		/// Insere o log de erro do Cte
		/// </summary>
		/// <param name="mdfe">MDF-e referente ao log</param>
		/// <param name="nomeServico">Nome do servico ou método que esta sendo logado</param>
		/// <param name="mensagem">mensagem de log</param>
		public void InserirLogErro(Mdfe mdfe, string nomeServico, string mensagem)
		{
			_mdfeLogRepository.InserirLogErro(mdfe, nomeServico, mensagem);
		}

		/// <summary>
		/// Insere o log de erro do Cte
		/// </summary>
		/// <param name="mdfe">MDF-e referente ao log</param>
		/// <param name="builder">mensagem de log</param>
		public void InserirLogErro(Mdfe mdfe, StringBuilder builder)
		{
			_mdfeLogRepository.InserirLogErro(mdfe, builder);
		}
	}
}