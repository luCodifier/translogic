﻿using System;
using System.Collections.Generic;
using Translogic.Modules.Core.Domain.Model.Fornecedor;
using Translogic.Modules.Core.Domain.Model.LocaisFornecedor;
using Translogic.Modules.Core.Domain.Model.LocaisFornecedor.Repositories;
using Translogic.Modules.Core.Domain.Services.Fornecedor.Interface;
using Translogic.Modules.Core.Domain.Model.Fornecedor.Repositories;
using Translogic.Modules.Core.Domain.Model.Dto;
using Translogic.Core.Infrastructure.Web;
using ALL.Core.Dominio;
using Translogic.Modules.Core.Domain.Model.TipoServicos.Repositories;
using Translogic.Modules.Core.Domain.Model.Via.Repositories;
using Translogic.Modules.Core.Domain.Model.Via;
using Translogic.Modules.Core.Util;



namespace Translogic.Modules.Core.Domain.Services.Fornecedor
{
    public class FornecedorOsService : IFornecedorOsService
    {
        private readonly IFornecedorOsRepository _fornecedorOsRepository;
        private readonly ILocalFornecedorRepository _localFornecedorRepository;
        private readonly ITipoServicoRepository _TipoServicoRepository;
        private readonly IEstacaoMaeRepository _EstacaoMaeRepository;

        public FornecedorOsService(IFornecedorOsRepository fornencedorOsRepository,
                                   ILocalFornecedorRepository localFornecedorRepositry,
                                   ITipoServicoRepository TipoServicoRepository,
                                   IEstacaoMaeRepository EstacaoMaeRepository)
        {
            _fornecedorOsRepository = fornencedorOsRepository;
            _localFornecedorRepository = localFornecedorRepositry;
            _TipoServicoRepository = TipoServicoRepository;
            _EstacaoMaeRepository = EstacaoMaeRepository;
        }

        public ResultadoPaginado<FornecedorOsDto> ObterFornecedorOs(DetalhesPaginacaoWeb detalhesPaginacaoWeb,
                                                                    string nomeFornecedor, string local,
                                                                    int? idTipoServico, int? ativo)
        {
            return _fornecedorOsRepository.ObterFornecedorOs(detalhesPaginacaoWeb, nomeFornecedor, local, idTipoServico, ativo);
        }

        public string ObterNomePorId(int? idFornecedor)
        {
            var fornecedor = _fornecedorOsRepository.ObterPorId(idFornecedor.Value);
            return fornecedor.Nome;
        }

        public FornecedorOs ObterFornecedor(int idFornecedor)
        {
            return _fornecedorOsRepository.ObterPorId(idFornecedor);
        }

        public bool ObterPorNome(string nomeFornecedor)
        {
           
            return _fornecedorOsRepository.ObterPorNome(nomeFornecedor);
        }
        
        public ResultadoPaginado<FornecedorOsDto> ObterFornecedoresOsHabilitados(DetalhesPaginacaoWeb detalhesPaginacaoWeb, string nomeFornecedor, string local, int? idServico, string Ativo)
        {
            return _fornecedorOsRepository.ObterFornecedoresOsHabilitados(detalhesPaginacaoWeb, nomeFornecedor, local, idServico, Ativo);
        }
     
        public ResultadoPaginado<LocalTipoServicoFornecedorDto> ObterLocaisFornecedorPorId(DetalhesPaginacaoWeb detalhesPaginacaoWeb, int idFornecedor)
        {
            return _fornecedorOsRepository.ObterLocaisFornecedorPorId(detalhesPaginacaoWeb, idFornecedor);
        }

        public void InserirAtualizarFornecedor(FornecedorOsDto fornecedorOsDto, string usuario)
        {
            var fornecedorOs = AutoMapper.Mapper.Map<FornecedorOsDto, FornecedorOs>(fornecedorOsDto);
            
            fornecedorOs.Login = usuario;
            fornecedorOs.DtRegistro = DateTime.Now;
            fornecedorOs = _fornecedorOsRepository.InserirOuAtualizar(fornecedorOs);

            fornecedorOs.LocaisTiposFornecedor = ObterLocaisFornecedores(fornecedorOsDto.LocaisTiposServicos, fornecedorOs.Id);
           
            if (fornecedorOs.Id > 0)
            {
               _localFornecedorRepository.Remover(Convert.ToInt32(fornecedorOs.Id));
            }
            if (fornecedorOs.LocaisTiposFornecedor != null)
            {
                _localFornecedorRepository.Inserir(fornecedorOs.LocaisTiposFornecedor);
            }
        }

        public IList<LocalFornecedor> ObterLocaisFornecedores(IList<LocalTipoServicoFornecedorDto> estacaoServicos, int idFornecedorOs)
        {
            var lista = new List<LocalFornecedor>();
            LocalFornecedor localFornecedor;
            var fornecedorOs = _fornecedorOsRepository.ObterPorId(idFornecedorOs);
            foreach (var item in estacaoServicos)
            {
                var idTipoServico = Convert.ToInt32(item.IdTipoServico);
                localFornecedor = new LocalFornecedor();
                localFornecedor.EstacaoMae = (EstacaoMae)_EstacaoMaeRepository.ObterPorCodigo(item.Local);
                localFornecedor.TipoServicos = _TipoServicoRepository.ObterPorId(Convert.ToInt32(item.IdTipoServico));
                localFornecedor.FornecedoresOs = fornecedorOs;
                lista.Add(localFornecedor);
            }
            return lista;
        }

        public IList<OSLocalDto> ObterLocaisParaTipoServico()
        {
            return _fornecedorOsRepository.ObterLocaisParaTipoServico();
        }

        public IList<FornecedorOsDto> ObterFornecedor(string local, EnumTipoFornecedor tipoFornecedor, bool todos)
        {
            return _fornecedorOsRepository.ObterFornecedor(local, tipoFornecedor, todos);
        }

        public FornecedorOsDto ObterPorId(int idFornecedor)
        {
            var fornecedorOs = _fornecedorOsRepository.ObterPorId(idFornecedor);

            return AutoMapper.Mapper.Map<FornecedorOs, FornecedorOsDto>(fornecedorOs);
        }


        public bool ExisteFornecedorOsPorNomeId(string nomeFornecedor, int idFornecedor)
        {
            return _fornecedorOsRepository.ExisteFornecedorOsPorNomeId(nomeFornecedor, idFornecedor);
        }
    }
}