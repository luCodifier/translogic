﻿namespace Translogic.Modules.Core.Domain.Services.AcompanhamentoFat
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.Mail;
    using System.Net.Mime;
    using System.Text;
    using Model.Codificador;
    using Model.Codificador.Repositories;
    using Model.Diversos;
    using Model.Diversos.AvisoFat;
    using Model.Diversos.Repositories;
    using Model.Dto;
    using Model.FluxosComerciais.Nfes.Repositories;
    using OfficeOpenXml;
    using OfficeOpenXml.Table;

    public class AcompanhamentoFatService
    {
        int _semanaAtual = 0;
        int _semanaPassada = 0;

        decimal totalNfe = 0;
        decimal totalComXml = 0;
        decimal porcentagem = 0;
        decimal porcentagem2 = 0;

        FileInfo newFile = new FileInfo(@"C:\Translogic\Translogic.JobRunner\modeloFat.xlsx");
        FileInfo novo = new FileInfo(@"C:\Translogic\Translogic.JobRunner\modeloFatEnvio.xlsx");

        private readonly IConfiguracaoTranslogicRepository _configuracaoTranslogicRepository;
        private readonly IContatoAvisoFatRepository _contatoAvisoFatRepository;
        private readonly string _cteEmailSenhaSmtp = "CTE_EMAIL_SMTP_SENHA";
        private readonly string _cteEmailServer = "CTE_EMAIL_SERVER";

        private readonly INfeReadonlyRepository _nfeReadonlyRepository;

        public AcompanhamentoFatService(INfeReadonlyRepository nfeReadonlyRepository,
                                        IContatoAvisoFatRepository contatoAvisoFatRepository,
                                        IConfiguracaoTranslogicRepository configuracaoTranslogicRepository)
        {
            _semanaAtual = GetIso8601WeekOfYear(DateTime.UtcNow.ToLocalTime());
            _semanaPassada = _semanaAtual - 1;

            _nfeReadonlyRepository = nfeReadonlyRepository;
            _contatoAvisoFatRepository = contatoAvisoFatRepository;
            _configuracaoTranslogicRepository = configuracaoTranslogicRepository;
        }

        public void EnviarAcompanhamento()
        {
            Log(string.Format("Acompanhamento fat, Malha Sul, S-1 = {0} ({1})", _semanaPassada, DateTime.UtcNow.ToLocalTime()), EventLogEntryType.Information);
            Log(string.Format("Acompanhamento fat, Malha Sul, S = {0} ({1})", _semanaAtual, DateTime.UtcNow.ToLocalTime()), EventLogEntryType.Information);

            MontaArquivoExcel();

            var acompanhamentos = _nfeReadonlyRepository.ObterAcompanhamentosAgrupadosMalhaSul();
            var tbMercadorias = MontaTabelaMercadorias(acompanhamentos);
            var tbClientes = MontaTabelaClientes(acompanhamentos);

            EnviaEmail(tbMercadorias, tbClientes);
        }

        public static int GetIso8601WeekOfYear(DateTime time)
        {
            DayOfWeek day = CultureInfo.InvariantCulture.Calendar.GetDayOfWeek(time);
            if (day == DayOfWeek.Sunday)
            {
                time = time.AddDays(4);
            }
            if (day >= DayOfWeek.Monday && day <= DayOfWeek.Wednesday)
            {
                time = time.AddDays(3);
            }

            // Return the week of our adjusted day
            return CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(time, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Sunday);
        }

        public void MontaArquivoExcel()
        {
            IList<AcompanhamentoFatDto> acompanhamentos = _nfeReadonlyRepository.ObterAcompanhamentos();

            using (var package = new ExcelPackage(newFile))
            {
                ExcelWorksheet workSheet = package.Workbook.Worksheets.First();

                ExcelTable tbl = workSheet.Tables[0];
                ExcelAddressBase tbAddr = tbl.Address;

                tbl.TableXml.DocumentElement.Attributes["ref"].Value = tbAddr.Start.Address + ":V" +
                                                                       (acompanhamentos.Count() + 1);

                for (int i = 0; i < acompanhamentos.Count(); i++)
                {
                    workSheet.Cells["B" + (2 + i)].FormulaR1C1 = workSheet.Cells["B2"].FormulaR1C1;

                    if (acompanhamentos[i].Estado.Equals("PR") || acompanhamentos[i].Estado.Equals("SC"))
                    {
                        workSheet.Cells["C" + (2 + i)].Value = "PR/SC";
                    }
                    else
                    {
                        if (acompanhamentos[i].Empresa.ToLower().Equals("intercemen") && acompanhamentos[i].Estado.Equals("SP"))
                        {
                            workSheet.Cells["C" + (2 + i)].Value = "PR/SC";
                        }
                        else
                        {
                            workSheet.Cells["C" + (2 + i)].Value = acompanhamentos[i].Estado;
                        }
                    }

                    if (acompanhamentos[i].Estado.Equals("PR") || acompanhamentos[i].Estado.Equals("SC") || acompanhamentos[i].Estado.Equals("RS"))
                    {
                        workSheet.Cells["A" + (2 + i)].Value = "Operação Sul";
                    }
                    else
                    {
                        if (acompanhamentos[i].Empresa.ToLower().Equals("intercemen") && acompanhamentos[i].Estado.Equals("SP"))
                        {
                            workSheet.Cells["A" + (2 + i)].Value = "Operação Sul";
                        }
                        else
                        {
                            workSheet.Cells["A" + (2 + i)].Value = "Operação Norte";
                        }
                    }

                    workSheet.Cells["D" + (2 + i)].Value = acompanhamentos[i].Estado;
                    workSheet.Cells["E" + (2 + i)].Value = acompanhamentos[i].TimeStamp.Date;
                    workSheet.Cells["F" + (2 + i)].Value = acompanhamentos[i].Empresa;
                    workSheet.Cells["G" + (2 + i)].Value = acompanhamentos[i].Correntista;
                    workSheet.Cells["H" + (2 + i)].Value = acompanhamentos[i].Origem;
                    workSheet.Cells["I" + (2 + i)].Value = acompanhamentos[i].Destino;
                    workSheet.Cells["J" + (2 + i)].Value = acompanhamentos[i].Mercadoria;
                    workSheet.Cells["K" + (2 + i)].Value = acompanhamentos[i].Faturamento;
                    workSheet.Cells["L" + (2 + i)].Value = acompanhamentos[i].TotalNfe;
                    workSheet.Cells["M" + (2 + i)].Value = acompanhamentos[i].ComXml;
                    workSheet.Cells["N" + (2 + i)].Value = acompanhamentos[i].SemXml;

                    workSheet.Cells["O" + (2 + i)].FormulaR1C1 = workSheet.Cells["O2"].FormulaR1C1;
                    workSheet.Cells["P" + (2 + i)].FormulaR1C1 = workSheet.Cells["P2"].FormulaR1C1;
                    workSheet.Cells["Q" + (2 + i)].FormulaR1C1 = workSheet.Cells["Q2"].FormulaR1C1;
                    workSheet.Cells["R" + (2 + i)].FormulaR1C1 = workSheet.Cells["R2"].FormulaR1C1;
                    workSheet.Cells["S" + (2 + i)].FormulaR1C1 = workSheet.Cells["S2"].FormulaR1C1;
                    workSheet.Cells["T" + (2 + i)].FormulaR1C1 = workSheet.Cells["T2"].FormulaR1C1;
                    workSheet.Cells["U" + (2 + i)].FormulaR1C1 = workSheet.Cells["U2"].FormulaR1C1;
                    workSheet.Cells["V" + (2 + i)].FormulaR1C1 = workSheet.Cells["V2"].FormulaR1C1;
                }

                package.Workbook.CalcMode = ExcelCalcMode.Automatic;
                package.SaveAs(novo);
            }
        }

        public string MontaTabelaMercadorias(IList<AcompanhamentoFatDto> acompanhamentos)
        {
            decimal porcentagemS = 0;
            decimal porcentagemSm1 = 0;
            decimal porcentagemDm1 = 0;

            IList<string> listaMercadorias = new List<string>();

            foreach (
                AcompanhamentoFatDto acompanhamentoFatDto in
                    acompanhamentos.Where(x => !listaMercadorias.Contains(x.Mercadoria)))
            {
                listaMercadorias.Add(acompanhamentoFatDto.Mercadoria);
            }

            string montaTabelaMercadoria = string.Empty;

            var tbMercadorias = new StringBuilder();

            int z = 0;
            foreach (string mercadoria in listaMercadorias)
            {
                tbMercadorias.Append("<tr style='padding: 5px;'>");

                string complementoCss = z % 2 == 0 ? String.Empty : "background-color:lightblue;";

                tbMercadorias.Append("<td style='border: 1px solid black;padding: 5px;text-align: left;" +
                                     complementoCss + "'>");

                tbMercadorias.Append(mercadoria);
                tbMercadorias.Append("</td>");

                // MERCADORIA SEMANA-1

                totalNfe =
                    acompanhamentos.Where(
                        a =>
                        (a.Estado == "PR" || a.Estado == "SC" || a.Estado == "SP") && a.Mercadoria == mercadoria &&
                        GetIso8601WeekOfYear(a.TimeStamp.Date) == _semanaPassada).Sum(
                            am => am.TotalNfe);
                totalComXml =
                    acompanhamentos.Where(
                        a =>
                        (a.Estado == "PR" || a.Estado == "SC" || a.Estado == "SP") && a.Mercadoria == mercadoria &&
                        GetIso8601WeekOfYear(a.TimeStamp.Date) == _semanaPassada).Sum(
                            am => am.ComXml);

                try
                {
                    porcentagemSm1 = (100 * totalComXml) / totalNfe;
                    if (porcentagemSm1 > 0)
                    {
                        tbMercadorias.Append("<td style='border: 1px solid black;padding: 5px;text-align: center;" +
                                             complementoCss + "'>");
                        tbMercadorias.Append(porcentagemSm1.ToString("0.##") + "%");
                    }
                    else if (totalNfe > 0 && totalComXml == 0)
                    {
                        tbMercadorias.Append("<td style='border: 1px solid black;padding: 5px;text-align: center;" +
                                            complementoCss + "'>");
                        tbMercadorias.Append("0.00 %");
                    }
                    else
                    {
                        tbMercadorias.Append("<td style='border: 1px solid black;padding: 5px;text-align: center;" +
                                             complementoCss + "'>");
                        tbMercadorias.Append("-");
                    }
                }
                catch
                {
                    tbMercadorias.Append("<td style='border: 1px solid black;padding: 5px;text-align: center;" +
                                         complementoCss + "'>");
                    tbMercadorias.Append("-");
                }

                tbMercadorias.Append("</td>");

                // MERCADORIA SEMANA

                totalNfe =
                    acompanhamentos.Where(
                        a =>
                        (a.Estado == "PR" || a.Estado == "SC" || a.Estado == "SP") && a.Mercadoria == mercadoria &&
                        GetIso8601WeekOfYear(a.TimeStamp.Date) == _semanaAtual).Sum(
                            am => am.TotalNfe);

                totalComXml =
                    acompanhamentos.Where(
                        a =>
                        (a.Estado == "PR" || a.Estado == "SC" || a.Estado == "SP") && a.Mercadoria == mercadoria &&
                        GetIso8601WeekOfYear(a.TimeStamp.Date) == _semanaAtual).Sum(
                            am => am.ComXml);

                try
                {
                    porcentagemS = (100 * totalComXml) / totalNfe;
                    if (porcentagemS > 0)
                    {
                        if (porcentagemS < porcentagemSm1)
                        {
                            tbMercadorias.Append("<td style='border: 1px solid black;padding: 5px;text-align: center;color:red;" +
                                             complementoCss + "'>");
                            tbMercadorias.Append(porcentagemS.ToString("0.##") + "%");
                        }
                        else if (totalNfe > 0 && totalComXml == 0)
                        {
                            tbMercadorias.Append("<td style='border: 1px solid black;padding: 5px;text-align: center;" +
                                                complementoCss + "'>");
                            tbMercadorias.Append("0.00 %");
                        }
                        else
                        {
                            tbMercadorias.Append("<td style='border: 1px solid black;padding: 5px;text-align: center;" +
                                             complementoCss + "'>");
                            tbMercadorias.Append(porcentagemS.ToString("0.##") + "%");
                        }
                    }
                    else
                    {
                        tbMercadorias.Append("<td style='border: 1px solid black;padding: 5px;text-align: center;" +
                                             complementoCss + "'>");
                        tbMercadorias.Append("-");
                    }
                }
                catch
                {
                    tbMercadorias.Append("<td style='border: 1px solid black;padding: 5px;text-align: center;" +
                                         complementoCss + "'>");
                    tbMercadorias.Append("-");
                }

                tbMercadorias.Append("</td>");

                // MERCADORIA D-1

                totalNfe =
                    acompanhamentos.Where(
                        a =>
                        (a.Estado == "PR" || a.Estado == "SC" || a.Estado == "SP") && a.Mercadoria == mercadoria &&
                        a.TimeStamp.Date == DateTime.UtcNow.ToLocalTime().AddDays(-1).Date).Sum(am => am.TotalNfe);
                totalComXml =
                    acompanhamentos.Where(
                        a =>
                        (a.Estado == "PR" || a.Estado == "SC" || a.Estado == "SP") && a.Mercadoria == mercadoria &&
                        a.TimeStamp.Date == DateTime.UtcNow.ToLocalTime().AddDays(-1).Date).Sum(am => am.ComXml);

                try
                {
                    porcentagemDm1 = (100 * totalComXml) / totalNfe;
                    if (porcentagemDm1 > 0)
                    {
                        if (porcentagemDm1 < porcentagemS)
                        {
                            tbMercadorias.Append("<td style='border: 1px solid black;padding: 5px;text-align: center;color:red;" +
                                             complementoCss + "'>");
                            tbMercadorias.Append(porcentagemDm1.ToString("0.##") + "%");
                        }
                        else if (totalNfe > 0 && totalComXml == 0)
                        {
                            tbMercadorias.Append("<td style='border: 1px solid black;padding: 5px;text-align: center;" +
                                                complementoCss + "'>");
                            tbMercadorias.Append("0.00 %");
                        }
                        else
                        {
                            tbMercadorias.Append("<td style='border: 1px solid black;padding: 5px;text-align: center;" +
                                             complementoCss + "'>");
                            tbMercadorias.Append(porcentagemDm1.ToString("0.##") + "%");
                        }
                    }
                    else
                    {
                        tbMercadorias.Append("<td style='border: 1px solid black;padding: 5px;text-align: center;" +
                                             complementoCss + "'>");
                        tbMercadorias.Append("-");
                    }
                }
                catch
                {
                    tbMercadorias.Append("<td style='border: 1px solid black;padding: 5px;text-align: center;" +
                                         complementoCss + "'>");
                    tbMercadorias.Append("-");
                }

                tbMercadorias.Append("</td>");


                // RS

                totalNfe =
                    acompanhamentos.Where(
                        a =>
                        a.Estado == "RS" && a.Mercadoria == mercadoria &&
                        GetIso8601WeekOfYear(a.TimeStamp.Date) == _semanaPassada).Sum(
                            am => am.TotalNfe);
                totalComXml =
                    acompanhamentos.Where(
                        a =>
                        a.Estado == "RS" && a.Mercadoria == mercadoria &&
                        GetIso8601WeekOfYear(a.TimeStamp.Date) == _semanaPassada).Sum(
                            am => am.ComXml);

                try
                {
                    porcentagemSm1 = (100 * totalComXml) / totalNfe;
                    if (porcentagemSm1 > 0)
                    {
                        tbMercadorias.Append("<td style='border: 1px solid black;padding: 5px;text-align: center;" +
                                             complementoCss + "'>");
                        tbMercadorias.Append(porcentagemSm1.ToString("0.##") + "%");
                    }
                    else if (totalNfe > 0 && totalComXml == 0)
                    {
                        tbMercadorias.Append("<td style='border: 1px solid black;padding: 5px;text-align: center;" +
                                            complementoCss + "'>");
                        tbMercadorias.Append("0.00 %");
                    }
                    else
                    {
                        tbMercadorias.Append("<td style='border: 1px solid black;padding: 5px;text-align: center;" +
                                             complementoCss + "'>");
                        tbMercadorias.Append("-");
                    }
                }
                catch
                {
                    tbMercadorias.Append("<td style='border: 1px solid black;padding: 5px;text-align: center;" +
                                         complementoCss + "'>");
                    tbMercadorias.Append("-");
                }

                tbMercadorias.Append("</td>");

                // MERCADORIA SEMANA

                totalNfe =
                    acompanhamentos.Where(
                        a =>
                        a.Estado == "RS" && a.Mercadoria == mercadoria &&
                        GetIso8601WeekOfYear(a.TimeStamp.Date) == _semanaAtual).Sum(
                            am => am.TotalNfe);
                totalComXml =
                    acompanhamentos.Where(
                        a =>
                        a.Estado == "RS" && a.Mercadoria == mercadoria &&
                        GetIso8601WeekOfYear(a.TimeStamp.Date) == _semanaAtual).Sum(
                            am => am.ComXml);

                try
                {
                    porcentagemS = (100 * totalComXml) / totalNfe;
                    if (porcentagemS > 0)
                    {
                        if (porcentagemS < porcentagemSm1)
                        {
                            tbMercadorias.Append("<td style='border: 1px solid black;padding: 5px;text-align: center;color:red;" +
                                            complementoCss + "'>");
                            tbMercadorias.Append(porcentagemS.ToString("0.##") + "%");
                        }
                        else if (totalNfe > 0 && totalComXml == 0)
                        {
                            tbMercadorias.Append("<td style='border: 1px solid black;padding: 5px;text-align: center;" +
                                                complementoCss + "'>");
                            tbMercadorias.Append("0.00 %");
                        }
                        else
                        {
                            tbMercadorias.Append("<td style='border: 1px solid black;padding: 5px;text-align: center;" +
                                            complementoCss + "'>");
                            tbMercadorias.Append(porcentagemS.ToString("0.##") + "%");
                        }
                    }
                    else
                    {
                        tbMercadorias.Append("<td style='border: 1px solid black;padding: 5px;text-align: center;" +
                                             complementoCss + "'>");
                        tbMercadorias.Append("-");
                    }
                }
                catch
                {
                    tbMercadorias.Append("<td style='border: 1px solid black;padding: 5px;text-align: center;" +
                                         complementoCss + "'>");
                    tbMercadorias.Append("-");
                }

                tbMercadorias.Append("</td>");


                totalNfe =
                    acompanhamentos.Where(
                        a =>
                        a.Estado == "RS" && a.Mercadoria == mercadoria &&
                        a.TimeStamp.Date == DateTime.UtcNow.ToLocalTime().AddDays(-1).Date).Sum(am => am.TotalNfe);
                totalComXml =
                    acompanhamentos.Where(
                        a =>
                        a.Estado == "RS" && a.Mercadoria == mercadoria &&
                        a.TimeStamp.Date == DateTime.UtcNow.ToLocalTime().AddDays(-1).Date).Sum(am => am.ComXml);

                try
                {
                    porcentagemDm1 = (100 * totalComXml) / totalNfe;
                    if (porcentagemDm1 > 0)
                    {
                        if (porcentagemDm1 < porcentagemS)
                        {
                            tbMercadorias.Append("<td style='border: 1px solid black;padding: 5px;text-align: center;color:red;" +
                                            complementoCss + "'>");
                            tbMercadorias.Append(porcentagemDm1.ToString("0.##") + "%");
                        }
                        else if (totalNfe > 0 && totalComXml == 0)
                        {
                            tbMercadorias.Append("<td style='border: 1px solid black;padding: 5px;text-align: center;" +
                                                complementoCss + "'>");
                            tbMercadorias.Append("0.00 %");
                        }
                        else
                        {
                            tbMercadorias.Append("<td style='border: 1px solid black;padding: 5px;text-align: center;" +
                                            complementoCss + "'>");
                            tbMercadorias.Append(porcentagemDm1.ToString("0.##") + "%");
                        }
                    }
                    else
                    {
                        tbMercadorias.Append("<td style='border: 1px solid black;padding: 5px;text-align: center;" +
                                             complementoCss + "'>");
                        tbMercadorias.Append("-");
                    }
                }
                catch
                {
                    tbMercadorias.Append("<td style='border: 1px solid black;padding: 5px;text-align: center;" +
                                         complementoCss + "'>");
                    tbMercadorias.Append("-");
                }

                tbMercadorias.Append("</td>");
                tbMercadorias.Append("</tr>");
                z++;
            }


            tbMercadorias.Append("<tr><td style='border: 1px solid black;padding: 5px;text-align: left;'>TOTAL</td>");

            // MERCADORIA SEMANA-1

            totalNfe =
                acompanhamentos.Where(
                    a =>
                    (a.Estado == "PR" || a.Estado == "SC" || a.Estado == "SP") && GetIso8601WeekOfYear(a.TimeStamp.Date) == _semanaPassada)
                    .Sum(am => am.TotalNfe);
            totalComXml =
                acompanhamentos.Where(
                    a =>
                    (a.Estado == "PR" || a.Estado == "SC" || a.Estado == "SP") && GetIso8601WeekOfYear(a.TimeStamp.Date) == _semanaPassada)
                    .Sum(am => am.ComXml);

            try
            {
                porcentagemSm1 = (100 * totalComXml) / totalNfe;
                if (porcentagemSm1 > 0)
                {
                    tbMercadorias.Append("<td style='border: 1px solid black;padding: 5px;text-align: center;'>");
                    tbMercadorias.Append(porcentagemSm1.ToString("0.##") + "%");
                }
                else if (totalNfe > 0 && totalComXml == 0)
                {
                    tbMercadorias.Append("<td style='border: 1px solid black;padding: 5px;text-align: center;'>");
                    tbMercadorias.Append("0.00 %");
                }
                else
                {
                    tbMercadorias.Append("<td style='border: 1px solid black;padding: 5px;text-align: center;'>");
                    tbMercadorias.Append("-");
                }
            }
            catch
            {
                tbMercadorias.Append("<td style='border: 1px solid black;padding: 5px;text-align: center;'>");
                tbMercadorias.Append("-");
            }

            tbMercadorias.Append("</td>");

            // SEMANA
            totalNfe =
                acompanhamentos.Where(
                    a => (a.Estado == "PR" || a.Estado == "SC" || a.Estado == "SP") && GetIso8601WeekOfYear(a.TimeStamp.Date) == _semanaAtual).
                    Sum(am => am.TotalNfe);
            totalComXml =
                acompanhamentos.Where(
                    a => (a.Estado == "PR" || a.Estado == "SC" || a.Estado == "SP") && GetIso8601WeekOfYear(a.TimeStamp.Date) == _semanaAtual).
                    Sum(am => am.ComXml);

            try
            {
                porcentagemS = (100 * totalComXml) / totalNfe;
                if (porcentagemS > 0)
                {
                    if (porcentagemS < porcentagemSm1)
                    {
                        tbMercadorias.Append("<td style='border: 1px solid black;padding: 5px;text-align: center;color:red;'>");
                        tbMercadorias.Append(porcentagemS.ToString("0.##") + "%");
                    }
                    else if (totalNfe > 0 && totalComXml == 0)
                    {
                        tbMercadorias.Append("<td style='border: 1px solid black;padding: 5px;text-align: center;'>");
                        tbMercadorias.Append("0.00 %");
                    }
                    else
                    {
                        tbMercadorias.Append("<td style='border: 1px solid black;padding: 5px;text-align: center;'>");
                        tbMercadorias.Append(porcentagemS.ToString("0.##") + "%");
                    }
                }
                else
                {
                    tbMercadorias.Append("<td style='border: 1px solid black;padding: 5px;text-align: center;'>");
                    tbMercadorias.Append("-");
                }
            }
            catch
            {
                tbMercadorias.Append("<td style='border: 1px solid black;padding: 5px;text-align: center;'>");
                tbMercadorias.Append("-");
            }

            tbMercadorias.Append("</td>");

            // MERCADORIA D-1
            totalNfe =
                acompanhamentos.Where(a => (a.Estado == "PR" || a.Estado == "SC" || a.Estado == "SP")  && a.TimeStamp.Date == DateTime.UtcNow.ToLocalTime().AddDays(-1).Date).Sum(
                    am => am.TotalNfe);
            totalComXml =
                acompanhamentos.Where(a => (a.Estado == "PR" || a.Estado == "SC" || a.Estado == "SP")  && a.TimeStamp.Date == DateTime.UtcNow.ToLocalTime().AddDays(-1).Date).Sum(
                    am => am.ComXml);

            try
            {
                porcentagemDm1 = (100 * totalComXml) / totalNfe;
                if (porcentagemDm1 > 0)
                {
                    if (porcentagemDm1 < porcentagemS)
                    {
                        tbMercadorias.Append("<td style='border: 1px solid black;padding: 5px;text-align: center;color:red;'>");
                        tbMercadorias.Append(porcentagemDm1.ToString("0.##") + "%");
                    }
                    else if (totalNfe > 0 && totalComXml == 0)
                    {
                        tbMercadorias.Append("<td style='border: 1px solid black;padding: 5px;text-align: center;'>");
                        tbMercadorias.Append("0.00 %");
                    }
                    else
                    {
                        tbMercadorias.Append("<td style='border: 1px solid black;padding: 5px;text-align: center;'>");
                        tbMercadorias.Append(porcentagemDm1.ToString("0.##") + "%");
                    }
                }
                else
                {
                    tbMercadorias.Append("<td style='border: 1px solid black;padding: 5px;text-align: center;'>");
                    tbMercadorias.Append("-");
                }
            }
            catch
            {
                tbMercadorias.Append("<td style='border: 1px solid black;padding: 5px;text-align: center;'>");
                tbMercadorias.Append("-");
            }

            tbMercadorias.Append("</td>");


            // MERCADORIA SEMANA-1

            totalNfe =
                acompanhamentos.Where(
                    a =>
                    a.Estado == "RS" && GetIso8601WeekOfYear(a.TimeStamp.Date) == _semanaPassada)
                    .Sum(am => am.TotalNfe);
            totalComXml =
                acompanhamentos.Where(
                    a =>
                    a.Estado == "RS" && GetIso8601WeekOfYear(a.TimeStamp.Date) == _semanaPassada)
                    .Sum(am => am.ComXml);

            try
            {
                porcentagemSm1 = (100 * totalComXml) / totalNfe;
                if (porcentagemSm1 > 0)
                {
                    tbMercadorias.Append("<td style='border: 1px solid black;padding: 5px;text-align: center;'>");
                    tbMercadorias.Append(porcentagemSm1.ToString("0.##") + "%");
                }
                else if (totalNfe > 0 && totalComXml == 0)
                {
                    tbMercadorias.Append("<td style='border: 1px solid black;padding: 5px;text-align: center;'>");
                    tbMercadorias.Append("0.00 %");
                }
                else
                {
                    tbMercadorias.Append("<td style='border: 1px solid black;padding: 5px;text-align: center;'>");
                    tbMercadorias.Append("-");
                }
            }
            catch
            {
                tbMercadorias.Append("<td style='border: 1px solid black;padding: 5px;text-align: center;'>");
                tbMercadorias.Append("-");
            }

            tbMercadorias.Append("</td>");

            // SEMANA
            totalNfe =
                acompanhamentos.Where(
                    a => a.Estado == "RS" && GetIso8601WeekOfYear(a.TimeStamp.Date) == _semanaAtual).
                    Sum(am => am.TotalNfe);
            totalComXml =
                acompanhamentos.Where(
                    a => a.Estado == "RS" && GetIso8601WeekOfYear(a.TimeStamp.Date) == _semanaAtual).
                    Sum(am => am.ComXml);

            try
            {
                porcentagemS = (100 * totalComXml) / totalNfe;
                if (porcentagemS > 0)
                {
                    if (porcentagemS < porcentagemSm1)
                    {
                        tbMercadorias.Append("<td style='border: 1px solid black;padding: 5px;text-align: center;color:red;'>");
                        tbMercadorias.Append(porcentagemS.ToString("0.##") + "%");
                    }
                    else if (totalNfe > 0 && totalComXml == 0)
                    {
                        tbMercadorias.Append("<td style='border: 1px solid black;padding: 5px;text-align: center;'>");
                        tbMercadorias.Append("0.00 %");
                    }
                    else
                    {
                        tbMercadorias.Append("<td style='border: 1px solid black;padding: 5px;text-align: center;'>");
                        tbMercadorias.Append(porcentagemS.ToString("0.##") + "%");
                    }
                }
                else
                {
                    tbMercadorias.Append("<td style='border: 1px solid black;padding: 5px;text-align: center;'>");
                    tbMercadorias.Append("-");
                }
            }
            catch
            {
                tbMercadorias.Append("<td style='border: 1px solid black;padding: 5px;text-align: center;'>");
                tbMercadorias.Append("-");
            }

            tbMercadorias.Append("</td>");

            // MERCADORIA D-1
            totalNfe =
                acompanhamentos.Where(a => a.Estado == "RS" && a.TimeStamp.Date == DateTime.UtcNow.ToLocalTime().AddDays(-1).Date).Sum(
                    am => am.TotalNfe);
            totalComXml =
                acompanhamentos.Where(a => a.Estado == "RS" && a.TimeStamp.Date == DateTime.UtcNow.ToLocalTime().AddDays(-1).Date).Sum(
                    am => am.ComXml);

            try
            {
                porcentagemDm1 = (100 * totalComXml) / totalNfe;
                if (porcentagemDm1 > 0)
                {
                    if (porcentagemDm1 < porcentagemS)
                    {
                        tbMercadorias.Append("<td style='border: 1px solid black;padding: 5px;text-align: center;color:red;'>");
                        tbMercadorias.Append(porcentagemDm1.ToString("0.##") + "%");
                    }
                    else if (totalNfe > 0 && totalComXml == 0)
                    {
                        tbMercadorias.Append("<td style='border: 1px solid black;padding: 5px;text-align: center;'>");
                        tbMercadorias.Append("0.00 %");
                    }
                    else
                    {
                        tbMercadorias.Append("<td style='border: 1px solid black;padding: 5px;text-align: center;'>");
                        tbMercadorias.Append(porcentagemDm1.ToString("0.##") + "%");
                    }
                }
                else
                {
                    tbMercadorias.Append("<td style='border: 1px solid black;padding: 5px;text-align: center;'>");
                    tbMercadorias.Append("-");
                }
            }
            catch
            {
                tbMercadorias.Append("<td style='border: 1px solid black;padding: 5px;text-align: center;'>");
                tbMercadorias.Append("-");
            }

            tbMercadorias.Append("</td>");

            tbMercadorias.Append("</tr>");

            montaTabelaMercadoria +=
                "<table style='border-collapse:collapse;'>" +
                    "<tr>" +
                        "<td></td>" +
                        "<td colspan='3' style='border: 1px solid black;padding: 5px;background-color:lightslategray;color:white;text-align: center;'> PR/SC </td>" +
                        "<td  colspan='3' style='border: 1px solid black;padding: 5px;background-color:lightslategray;color:white;text-align: center;'> RS </td>" +
                    "</tr>" +
                    "<tr style='border: 1px solid black;padding: 5px;background-color:lightslategray;color:white;'>" +
                        "<td style='border: 1px solid black;padding: 5px;color:white;text-align: left;'>Produto</td>" +
                        "<td style='border: 1px solid black;padding: 5px;color:white;text-align: center;'>S-1</td>" +
                        "<td style='border: 1px solid black;padding: 5px;color:white;text-align: center;'>S</td>" +
                        "<td style='border: 1px solid black;padding: 5px;color:white;text-align: center;'>D-1</td>" +
                        "<td style='border: 1px solid black;padding: 5px;color:white;text-align: center;'>S-1</td>" +
                        "<td style='border: 1px solid black;padding: 5px;color:white;text-align: center;'>S</td>" +
                        "<td style='border: 1px solid black;padding: 5px;color:white;text-align: center;'>D-1</td>" +
                    "</tr>" +
                tbMercadorias + "</table>";

            return montaTabelaMercadoria;
        }

        public string MontaTabelaClientes(IList<AcompanhamentoFatDto> acompanhamentos)
        {
            IList<string> listaClientes = new List<string>();
            var tbClientes = new StringBuilder();
            string montaTabelaCliente = string.Empty;
            decimal diferenca = 0;

            var listaNova = acompanhamentos.Where(x => x.TimeStamp.Date == DateTime.UtcNow.ToLocalTime().AddDays(-1).Date).GroupBy(hit => hit.Correntista).
                               Select(group => new AcompanhamentoFatDto
                               {
                                   Correntista = group.Key,
                                   TotalNfe = group.Sum(hit => hit.TotalNfe)
                               }).
                               OrderByDescending(hit => hit.TotalNfe);

            foreach (AcompanhamentoFatDto acompanhamentoFatDto in listaNova)
            {
                if (!listaClientes.Contains(acompanhamentoFatDto.Correntista))
                {
                    listaClientes.Add(acompanhamentoFatDto.Correntista);
                }
            }

            int y = 0;
            foreach (string correntista in listaClientes)
            {
                totalNfe = 0;
                List<AcompanhamentoFatDto> listaFiltrada =
                    acompanhamentos.Where(
                        a =>
                        a.Correntista == correntista &&
                        a.TimeStamp.Date == DateTime.UtcNow.ToLocalTime().AddDays(-1).Date).ToList();
                totalNfe +=
                    listaFiltrada.Where(a => a.Faturamento == "MANUAL").Sum(
                        am => am.TotalNfe);
                totalNfe +=
                    listaFiltrada.Where(a => a.Faturamento == "EDI NOVO").Sum(
                        am => am.TotalNfe);
                totalNfe +=
                   listaFiltrada.Where(a => a.Faturamento == "EDI CAALL").Sum(
                       am => am.TotalNfe);
                totalNfe +=
                    listaFiltrada.Where(a => a.Faturamento == "EDI ANTIGO").Sum(
                        am => am.TotalNfe);
                totalNfe +=
                    acompanhamentos.Where(
                        a =>
                        a.Correntista == correntista &&
                        a.TimeStamp.Date == DateTime.UtcNow.ToLocalTime().AddDays(-1).Date).Sum(am => am.TotalNfe);

                if (totalNfe > 0)
                {
                    tbClientes.Append(y % 2 == 0
                                          ? "<tr style='border: 1px solid black;padding: 5px;'>"
                                          : "<tr style='border: 1px solid black;padding: 5px;background-color:lightblue;'>");
                    y++;

                    tbClientes.Append("<td style='border: 1px solid black;padding: 5px;text-align: left;'>");
                    tbClientes.Append(correntista);
                    tbClientes.Append("</td>");

                    // S-1
                    totalNfe =
                        acompanhamentos.Where(
                            a =>
                            a.Correntista == correntista &&
                            GetIso8601WeekOfYear(a.TimeStamp.Date) == _semanaPassada).Sum(
                                am => am.TotalNfe);
                    totalComXml =
                        acompanhamentos.Where(
                            a =>
                            a.Correntista == correntista &&
                            GetIso8601WeekOfYear(a.TimeStamp.Date) == _semanaPassada).Sum(
                                am => am.ComXml);

                    try
                    {
                        porcentagem = (100 * totalComXml) / totalNfe;
                        if (porcentagem == 0 && totalNfe > 0)
                        {
                            tbClientes.Append("<td style='border: 1px solid black;padding: 5px;background-color:lightgray!important;text-align:center;'>");
                            tbClientes.Append("0.00 %");
                        }
                        else if (porcentagem > 0)
                        {
                            tbClientes.Append("<td style='border: 1px solid black;padding: 5px;background-color:lightgray!important;text-align:center;'>");
                            tbClientes.Append(porcentagem.ToString("0.##") + "%");
                        }
                        else
                        {
                            tbClientes.Append("<td style='border: 1px solid black;padding: 5px;text-align:center;background-color:lightgray!important;'>");
                            tbClientes.Append("-");
                        }
                    }
                    catch
                    {
                        tbClientes.Append("<td style='border: 1px solid black;padding: 5px;text-align:center;background-color:lightgray!important;'>");
                        porcentagem = 0;
                        tbClientes.Append("-");
                    }
                    tbClientes.Append("</td>");

                    // S
                    totalNfe =
                        acompanhamentos.Where(
                            a =>
                            a.Correntista == correntista &&
                            GetIso8601WeekOfYear(a.TimeStamp.Date) == _semanaAtual).Sum(
                                am => am.TotalNfe);
                    totalComXml =
                        acompanhamentos.Where(
                            a =>
                            a.Correntista == correntista &&
                            GetIso8601WeekOfYear(a.TimeStamp.Date) == _semanaAtual).Sum(
                                am => am.ComXml);

                    try
                    {
                        porcentagem2 = (100 * totalComXml) / totalNfe;
                        if (porcentagem2 == 0 && totalNfe > 0)
                        {
                            tbClientes.Append("<td style='border: 1px solid black;padding: 5px;background-color:lightgray!important;text-align:center;'>");
                            tbClientes.Append("0.00 %");
                        }
                        else if (porcentagem2 > 0)
                        {
                            tbClientes.Append("<td style='border: 1px solid black;padding: 5px;background-color:lightgray!important;text-align:center;'>");
                            tbClientes.Append(porcentagem2.ToString("0.##") + "%");
                        }
                        else
                        {
                            tbClientes.Append("<td style='border: 1px solid black;padding: 5px;text-align:center;background-color:lightgray!important;'>");
                            tbClientes.Append("-");
                        }
                    }
                    catch
                    {
                        tbClientes.Append("<td style='border: 1px solid black;padding: 5px;text-align:center;background-color:lightgray!important;'>");
                        porcentagem2 = 0;
                        tbClientes.Append("-");
                    }
                    tbClientes.Append("</td>");

                    // DIF
                    diferenca = (porcentagem2 - porcentagem);

                    if (diferenca == 0)
                    {
                        tbClientes.Append("<td style='border: 1px solid black;padding: 5px;text-align:center;background-color:lightgray!important;'>-");
                    }
                    else if (diferenca > 0)
                    {
                        tbClientes.Append("<td style='border: 1px solid black;padding: 5px;background-color:lightgray!important;text-align:center;'>" +
                                          diferenca.ToString("0.##") + "%");
                    }
                    else
                    {
                        tbClientes.Append("<td style='border: 1px solid black;padding: 5px;background-color:lightgray!important;color:red;text-align:center;'>" + diferenca.ToString("0.##") + "%");
                    }

                    tbClientes.Append("</td>");

                    // D-1
                    listaFiltrada =
                        acompanhamentos.Where(
                            a =>
                            a.Correntista == correntista &&
                            a.TimeStamp.Date == DateTime.UtcNow.ToLocalTime().AddDays(-1).Date).ToList();

                    totalNfe =
                        acompanhamentos.Where(
                            a =>
                            a.Correntista == correntista &&
                            a.TimeStamp.Date == DateTime.UtcNow.ToLocalTime().AddDays(-1).Date).Sum(am => am.TotalNfe);
                    totalComXml =
                        acompanhamentos.Where(
                            a =>
                            a.Correntista == correntista &&
                            a.TimeStamp.Date == DateTime.UtcNow.ToLocalTime().AddDays(-1).Date).Sum(am => am.ComXml);

                    tbClientes.Append("<td style='border: 1px solid black;padding: 5px;background-color:lightgray!important;text-align:center;'>");
                    tbClientes.Append(totalNfe);
                    tbClientes.Append("</td>");

                    try
                    {
                        porcentagem = (100 * totalComXml) / totalNfe;
                        if (porcentagem == 0 && totalNfe > 0)
                        {
                            tbClientes.Append("<td style='border: 1px solid black;padding: 5px;background-color:lightgray!important;text-align:center;'>");
                            tbClientes.Append("0.00 %");
                        }
                        else if (porcentagem > 0)
                        {
                            tbClientes.Append("<td style='border: 1px solid black;padding: 5px;background-color:lightgray!important;text-align:center;'>");
                            tbClientes.Append(porcentagem.ToString("0.##") + "%");
                        }
                        else
                        {
                            tbClientes.Append("<td style='border: 1px solid black;padding: 5px;text-align:center;background-color:lightgray!important;'>");
                            tbClientes.Append("-");
                        }
                    }
                    catch
                    {
                        tbClientes.Append("<td style='border: 1px solid black;padding: 5px;text-align:center;background-color:lightgray!important;'>");
                        porcentagem = 0;
                        tbClientes.Append("-");
                    }
                    tbClientes.Append("</td>");


                    // EDI ANTIGO D-1
                    totalNfe =
                        listaFiltrada.Where(a => a.Faturamento == "EDI ANTIGO").Sum(
                            am => am.TotalNfe);
                    totalComXml =
                        listaFiltrada.Where(a => a.Faturamento == "EDI ANTIGO").Sum(
                            am => am.ComXml);

                    tbClientes.Append("<td style='border: 1px solid black;padding: 5px;text-align:center;'>");
                    tbClientes.Append(totalNfe);
                    tbClientes.Append("</td>");

                    try
                    {
                        porcentagem = (100 * totalComXml) / totalNfe;
                        if (porcentagem == 0 && totalNfe > 0)
                        {
                            tbClientes.Append("<td style='border: 1px solid black;padding: 5px;text-align:center;'>");
                            tbClientes.Append("0.00 %");
                        }
                        else if (porcentagem > 0)
                        {
                            tbClientes.Append("<td style='border: 1px solid black;padding: 5px;text-align:center;'>");
                            tbClientes.Append(porcentagem.ToString("0.##") + "%");
                        }
                        else
                        {
                            tbClientes.Append("<td style='border: 1px solid black;padding: 5px;text-align:center;'>");
                            tbClientes.Append("-");
                        }
                    }
                    catch
                    {
                        tbClientes.Append("<td style='border: 1px solid black;padding: 5px;text-align:center;'>");
                        porcentagem = 0;
                        tbClientes.Append("-");
                    }
                    tbClientes.Append("</td>");

                    // EDI NOVO D-1
                    totalNfe =
                        listaFiltrada.Where(a => a.Faturamento == "EDI NOVO").Sum(
                            am => am.TotalNfe);
                    totalComXml =
                        listaFiltrada.Where(a => a.Faturamento == "EDI NOVO").Sum(
                            am => am.ComXml);

                    tbClientes.Append("<td style='border: 1px solid black;padding: 5px;text-align:center;'>");
                    tbClientes.Append(totalNfe);
                    tbClientes.Append("</td>");

                    try
                    {
                        porcentagem = (100 * totalComXml) / totalNfe;
                        if (porcentagem == 0 && totalNfe > 0)
                        {
                            tbClientes.Append("<td style='border: 1px solid black;padding: 5px;text-align:center;'>");
                            tbClientes.Append("0.00 %");
                        }
                        else if (porcentagem > 0)
                        {
                            tbClientes.Append("<td style='border: 1px solid black;padding: 5px;text-align:center;'>");
                            tbClientes.Append(porcentagem.ToString("0.##") + "%");
                        }
                        else
                        {
                            tbClientes.Append("<td style='border: 1px solid black;padding: 5px;text-align:center;'>");
                            tbClientes.Append("-");
                        }
                    }
                    catch
                    {
                        tbClientes.Append("<td style='border: 1px solid black;padding: 5px;text-align:center;'>");
                        porcentagem = 0;
                        tbClientes.Append("-");
                    }
                    tbClientes.Append("</td>");






                    // EDI NOVO D-1
                    totalNfe =
                        listaFiltrada.Where(a => a.Faturamento == "EDI CAALL").Sum(
                            am => am.TotalNfe);
                    totalComXml =
                        listaFiltrada.Where(a => a.Faturamento == "EDI CAALL").Sum(
                            am => am.ComXml);

                    tbClientes.Append("<td style='border: 1px solid black;padding: 5px;text-align:center;'>");
                    tbClientes.Append(totalNfe);
                    tbClientes.Append("</td>");

                    try
                    {
                        porcentagem = (100 * totalComXml) / totalNfe;
                        if (porcentagem == 0 && totalNfe > 0)
                        {
                            tbClientes.Append("<td style='border: 1px solid black;padding: 5px;text-align:center;'>");
                            tbClientes.Append("0.00 %");
                        }
                        else if (porcentagem > 0)
                        {
                            tbClientes.Append("<td style='border: 1px solid black;padding: 5px;text-align:center;'>");
                            tbClientes.Append(porcentagem.ToString("0.##") + "%");
                        }
                        else
                        {
                            tbClientes.Append("<td style='border: 1px solid black;padding: 5px;text-align:center;'>");
                            tbClientes.Append("-");
                        }
                    }
                    catch
                    {
                        tbClientes.Append("<td style='border: 1px solid black;padding: 5px;text-align:center;'>");
                        porcentagem = 0;
                        tbClientes.Append("-");
                    }
                    tbClientes.Append("</td>");







                    // MANUAL D-1
                    totalNfe =
                        listaFiltrada.Where(a => a.Faturamento == "MANUAL").Sum(
                            am => am.TotalNfe);
                    totalComXml =
                        listaFiltrada.Where(a => a.Faturamento == "MANUAL").Sum(
                            am => am.ComXml);

                    tbClientes.Append("<td style='border: 1px solid black;padding: 5px;text-align:center;'>");
                    tbClientes.Append(totalNfe);
                    tbClientes.Append("</td>");

                    try
                    {
                        porcentagem = (100 * totalComXml) / totalNfe;
                        if (porcentagem == 0 && totalNfe > 0)
                        {
                            tbClientes.Append("<td style='border: 1px solid black;padding: 5px;text-align:center;'>");
                            tbClientes.Append("0.00 %");
                        }
                        else if (porcentagem > 0)
                        {
                            tbClientes.Append("<td style='border: 1px solid black;padding: 5px;text-align:center;'>");
                            tbClientes.Append(porcentagem.ToString("0.##") + "%");
                        }
                        else
                        {
                            tbClientes.Append("<td style='border: 1px solid black;padding: 5px;text-align:center;'>");
                            tbClientes.Append("-");
                        }
                    }
                    catch
                    {
                        tbClientes.Append("<td style='border: 1px solid black;padding: 5px;text-align:center;'>");
                        porcentagem = 0;
                        tbClientes.Append("-");
                    }
                    tbClientes.Append("</td>");
                    tbClientes.Append("</tr>");
                }
            }

            // TOTAL
            tbClientes.Append("<tr><td style='border: 1px solid black;padding: 5px;text-align: left;'>TOTAL</td>");

            totalNfe =
                acompanhamentos.Where(
                    a =>
                    GetIso8601WeekOfYear(a.TimeStamp.Date) == _semanaPassada).Sum(
                        am => am.TotalNfe);

            totalComXml =
                acompanhamentos.Where(
                    a =>
                    GetIso8601WeekOfYear(a.TimeStamp.Date) == _semanaPassada).Sum(
                        am => am.ComXml);

            try
            {
                porcentagem = (100 * totalComXml) / totalNfe;
            }
            catch (Exception)
            {
                porcentagem = 0;
            }

            if (porcentagem == 0 && totalNfe > 0)
            {
                tbClientes.Append("<td style='border: 1px solid black;padding: 5px;background-color:lightgray!important;text-align:center;'>");
                tbClientes.Append("0.00 %");
            }
            else if (porcentagem > 0)
            {
                tbClientes.Append("<td style='border: 1px solid black;padding: 5px;background-color:lightgray!important;text-align:center;'>");
                tbClientes.Append(porcentagem.ToString("0.##") + "%");
            }
            else
            {
                tbClientes.Append("<td style='border: 1px solid black;padding: 5px;text-align:center;background-color:lightgray!important;'>");
                tbClientes.Append("-");
            }
            tbClientes.Append("</td>");

            totalNfe =
                  acompanhamentos.Where(
                      a =>
                      GetIso8601WeekOfYear(a.TimeStamp.Date) == _semanaAtual).Sum(
                          am => am.TotalNfe);

            totalComXml =
                acompanhamentos.Where(
                    a =>
                    GetIso8601WeekOfYear(a.TimeStamp.Date) == _semanaAtual).Sum(
                        am => am.ComXml);
            try
            {
                porcentagem2 = (100 * totalComXml) / totalNfe;
            }
            catch
            {
                porcentagem2 = 0;
            }

            if (porcentagem2 == 0 && totalNfe > 0)
            {
                tbClientes.Append("<td style='border: 1px solid black;padding: 5px;background-color:lightgray!important;text-align:center;'>");
                tbClientes.Append("0.00 %");
            }
            else if (porcentagem2 > 0)
            {
                tbClientes.Append("<td style='border: 1px solid black;padding: 5px;background-color:lightgray!important;text-align:center;'>");
                tbClientes.Append(porcentagem2.ToString("0.##") + "%");
            }
            else
            {
                tbClientes.Append("<td style='border: 1px solid black;padding: 5px;text-align:center;background-color:lightgray!important;'>");
                tbClientes.Append("-");
            }
            tbClientes.Append("</td>");

            diferenca = (porcentagem2 - porcentagem);

            if (diferenca == 0)
            {
                tbClientes.Append("<td style='border: 1px solid black;padding: 5px;text-align:center;background-color:lightgray!important;'>-");
            }
            else if (diferenca > 0)
            {
                tbClientes.Append("<td style='border: 1px solid black;padding: 5px;background-color:lightgray!important;text-align:center;'>" + diferenca.ToString("0.##") + "%");
            }
            else
            {
                tbClientes.Append("<td style='border: 1px solid black;padding: 5px;background-color:lightgray!important;text-align:center;color:red;'>" + diferenca.ToString("0.##") + "%");
            }

            tbClientes.Append("</td>");

            List<AcompanhamentoFatDto> listaFilto =
                acompanhamentos.Where(
                    a => a.TimeStamp.Date == DateTime.UtcNow.ToLocalTime().AddDays(-1).Date).ToList();

            // TOTAL
            totalNfe = listaFilto.Sum(am => am.TotalNfe);
            totalComXml = listaFilto.Sum(am => am.ComXml);

            tbClientes.Append("<td style='border: 1px solid black;padding: 5px;background-color:lightgray!important;text-align:center;'>");
            tbClientes.Append(totalNfe);
            tbClientes.Append("</td>");

            try
            {
                porcentagem = (100 * totalComXml) / totalNfe;
                if (porcentagem == 0 && totalNfe > 0)
                {
                    tbClientes.Append("<td style='border: 1px solid black;padding: 5px;background-color:lightgray!important;text-align:center;'>");
                    tbClientes.Append("0.00 %");
                }
                else if (porcentagem > 0)
                {
                    tbClientes.Append("<td style='border: 1px solid black;padding: 5px;background-color:lightgray!important;text-align:center;'>");
                    tbClientes.Append(porcentagem.ToString("0.##") + "%");
                }
                else
                {
                    tbClientes.Append("<td style='border: 1px solid black;padding: 5px;text-align:center;background-color:lightgray!important;'>");
                    tbClientes.Append("-");
                }
            }
            catch
            {
                tbClientes.Append("<td style='border: 1px solid black;padding: 5px;text-align:center;background-color:lightgray!important;'>");
                porcentagem = 0;
                tbClientes.Append("-");
            }
            tbClientes.Append("</td>");

            // EDI ANTIGO
            totalNfe =
                listaFilto.Where(a => a.Faturamento == "EDI ANTIGO").Sum(am => am.TotalNfe);
            totalComXml =
                listaFilto.Where(a => a.Faturamento == "EDI ANTIGO").Sum(am => am.ComXml);

            tbClientes.Append("<td style='border: 1px solid black;padding: 5px;text-align:center;'>");
            tbClientes.Append(totalNfe);
            tbClientes.Append("</td>");

            try
            {
                porcentagem = (100 * totalComXml) / totalNfe;
                if (porcentagem == 0 && totalNfe > 0)
                {
                    tbClientes.Append("<td style='border: 1px solid black;padding: 5px;text-align:center;'>");
                    tbClientes.Append("0.00 %");
                }
                else if (porcentagem > 0)
                {
                    tbClientes.Append("<td style='border: 1px solid black;padding: 5px;text-align:center;'>");
                    tbClientes.Append(porcentagem.ToString("0.##") + "%");
                }
                else
                {
                    tbClientes.Append("<td style='border: 1px solid black;padding: 5px;text-align:center;'>");
                    tbClientes.Append("-");
                }
            }
            catch
            {
                tbClientes.Append("<td style='border: 1px solid black;padding: 5px;text-align:center;'>");
                porcentagem = 0;
                tbClientes.Append("-");
            }
            tbClientes.Append("</td>");


            // EDI NOVO
            totalNfe =
                listaFilto.Where(a => a.Faturamento == "EDI NOVO").Sum(am => am.TotalNfe);
            totalComXml =
                listaFilto.Where(a => a.Faturamento == "EDI NOVO").Sum(am => am.ComXml);

            tbClientes.Append("<td style='border: 1px solid black;padding: 5px;text-align:center;'>");
            tbClientes.Append(totalNfe);
            tbClientes.Append("</td>");

            try
            {
                porcentagem = (100 * totalComXml) / totalNfe;
                if (porcentagem == 0 && totalNfe > 0)
                {
                    tbClientes.Append("<td style='border: 1px solid black;padding: 5px;text-align:center;'>");
                    tbClientes.Append("0.00 %");
                }
                else if (porcentagem > 0)
                {
                    tbClientes.Append("<td style='border: 1px solid black;padding: 5px;text-align:center;'>");
                    tbClientes.Append(porcentagem.ToString("0.##") + "%");
                }
                else
                {
                    tbClientes.Append("<td style='border: 1px solid black;padding: 5px;text-align:center;'>");
                    tbClientes.Append("-");
                }
            }
            catch
            {
                tbClientes.Append("<td style='border: 1px solid black;padding: 5px;text-align:center;'>");
                porcentagem = 0;
                tbClientes.Append("-");
            }
            tbClientes.Append("</td>");







            // EDI NOVO
            totalNfe =
                listaFilto.Where(a => a.Faturamento == "EDI CAALL").Sum(am => am.TotalNfe);
            totalComXml =
                listaFilto.Where(a => a.Faturamento == "EDI CAALL").Sum(am => am.ComXml);

            tbClientes.Append("<td style='border: 1px solid black;padding: 5px;text-align:center;'>");
            tbClientes.Append(totalNfe);
            tbClientes.Append("</td>");

            try
            {
                porcentagem = (100 * totalComXml) / totalNfe;
                if (porcentagem == 0 && totalNfe > 0)
                {
                    tbClientes.Append("<td style='border: 1px solid black;padding: 5px;text-align:center;'>");
                    tbClientes.Append("0.00 %");
                }
                else if (porcentagem > 0)
                {
                    tbClientes.Append("<td style='border: 1px solid black;padding: 5px;text-align:center;'>");
                    tbClientes.Append(porcentagem.ToString("0.##") + "%");
                }
                else
                {
                    tbClientes.Append("<td style='border: 1px solid black;padding: 5px;text-align:center;'>");
                    tbClientes.Append("-");
                }
            }
            catch
            {
                tbClientes.Append("<td style='border: 1px solid black;padding: 5px;text-align:center;'>");
                porcentagem = 0;
                tbClientes.Append("-");
            }
            tbClientes.Append("</td>");






            // MANUAL
            totalNfe =
                listaFilto.Where(a => a.Faturamento == "MANUAL").Sum(am => am.TotalNfe);
            totalComXml =
                listaFilto.Where(a => a.Faturamento == "MANUAL").Sum(am => am.ComXml);

            tbClientes.Append("<td style='border: 1px solid black;padding: 5px;text-align:center;'>");
            tbClientes.Append(totalNfe);
            tbClientes.Append("</td>");

            try
            {
                porcentagem = (100 * totalComXml) / totalNfe;
                if (porcentagem == 0 && totalNfe > 0)
                {
                    tbClientes.Append("<td style='border: 1px solid black;padding: 5px;text-align:center;'>");
                    tbClientes.Append("0.00 %");
                }
                else if (porcentagem > 0)
                {
                    tbClientes.Append("<td style='border: 1px solid black;padding: 5px;text-align:center;'>");
                    tbClientes.Append(porcentagem.ToString("0.##") + "%");
                }
                else
                {
                    tbClientes.Append("<td style='border: 1px solid black;padding: 5px;text-align:center;'>");
                    tbClientes.Append("-");
                }
            }
            catch
            {
                tbClientes.Append("<td style='border: 1px solid black;padding: 5px;text-align:center;'>");
                porcentagem = 0;
                tbClientes.Append("-");
            }
            tbClientes.Append("</td>");

            tbClientes.Append("</tr>");

            if (tbClientes.Length > 1)
            {
                montaTabelaCliente +=
                    "<table style='border-collapse:collapse;text-align: right;'>" +
                    "<tr style='border: 1px solid white;padding: 5px;text-align: center;'>" +
                        "<td style='border: none!important;padding: 5px;'></td>" +
                        "<td style='border: 1px solid black;padding: 5px;background-color:lightslategray;color:white;' colspan='5'>Total Geral</td>" +
                        "<td style='border: 1px solid black;padding: 5px;background-color:lightslategray;color:white;' colspan='2'>EDI Antigo</td>" +
                        "<td style='border: 1px solid black;padding: 5px;background-color:lightslategray;color:white;' colspan='2'>EDI Novo</td>" +
                        "<td style='border: 1px solid black;padding: 5px;background-color:lightslategray;color:white;' colspan='2'>EDI Caall</td>" +
                        "<td style='border: 1px solid black;padding: 5px;background-color:lightslategray;color:white;' colspan='2'>Manual</td>" +
                    "</tr>" +
                    "<tr style='border: 1px solid white;padding: 5px;text-align: center;'>" +
                        "<td style='border: none!important;padding: 5px;'></td>" +
                        "<td style='border: 1px solid black;padding: 5px;background-color:lightslategray;color:white;'>S-1</td>" +
                        "<td style='border: 1px solid black;padding: 5px;background-color:lightslategray;color:white;'>S</td>" +
                        "<td style='border: 1px solid black;padding: 5px;background-color:lightslategray;color:white;'>Δ</td>" +
                        "<td style='border: 1px solid black;padding: 5px;background-color:lightslategray;color:white;' colspan='2'>D-1</td>" +
                        "<td style='border: 1px solid black;padding: 5px;background-color:lightslategray;color:white;' colspan='2'>D-1</td>" +
                        "<td style='border: 1px solid black;padding: 5px;background-color:lightslategray;color:white;' colspan='2'>D-1</td>" +
                        "<td style='border: 1px solid black;padding: 5px;background-color:lightslategray;color:white;' colspan='2'>D-1</td>" +
                        "<td style='border: 1px solid black;padding: 5px;background-color:lightslategray;color:white;' colspan='2'>D-1</td>" +
                    "</tr>" +
                    "<tr style='border: 1px solid black;padding: 5px;text-align: center;'>" +
                        "<td style='border: 1px solid black;padding: 5px;background-color:lightslategray;color:white;text-align: left;'>Cliente</td>" +
                        "<td style='border: 1px solid black;padding: 5px;background-color:lightslategray;color:white;'>% COM XML</td>" +
                        "<td style='border: 1px solid black;padding: 5px;background-color:lightslategray;color:white;'>% COM XML</td>" +
                        "<td style='border: 1px solid black;padding: 5px;background-color:lightslategray;color:white;'>% COM XML</td>" +
                        "<td style='border: 1px solid black;padding: 5px;background-color:lightslategray;color:white;'>Qtde</td>" +
                        "<td style='border: 1px solid black;padding: 5px;background-color:lightslategray;color:white;'>% COM XML</td>" +
                        "<td style='border: 1px solid black;padding: 5px;background-color:lightslategray;color:white;'>Qtde</td>" +
                        "<td style='border: 1px solid black;padding: 5px;background-color:lightslategray;color:white;'>% COM XML</td>" +
                        "<td style='border: 1px solid black;padding: 5px;background-color:lightslategray;color:white;'>Qtde</td>" +
                        "<td style='border: 1px solid black;padding: 5px;background-color:lightslategray;color:white;'>% COM XML</td>" +
                        "<td style='border: 1px solid black;padding: 5px;background-color:lightslategray;color:white;'>Qtde</td>" +
                        "<td style='border: 1px solid black;padding: 5px;background-color:lightslategray;color:white;'>% COM XML</td>" +
                        "<td style='border: 1px solid black;padding: 5px;background-color:lightslategray;color:white;'>Qtde</td>" +
                        "<td style='border: 1px solid black;padding: 5px;background-color:lightslategray;color:white;'>% COM XML</td>" +
                    "</tr>" +
                    tbClientes + "</table>";
            }
            return montaTabelaCliente;
        }

        public void EnviaEmail(string planilha1, string planilha2)
        {
            string mask = ";,: ";

            ConfiguracaoTranslogic configuracaoTranslogic = _configuracaoTranslogicRepository.ObterPorId(_cteEmailServer);
            string[] valorArray = configuracaoTranslogic.Valor.Split(mask.ToCharArray());

            string emailServer = valorArray[0];
            string emailPort = valorArray[1];

            string remetente = "noreply@all-logistica.com";

            ConfiguracaoTranslogic configuracaocteEmailSenhaSmtp =
                _configuracaoTranslogicRepository.ObterPorId(_cteEmailSenhaSmtp);
            string senhaSmtp = configuracaocteEmailSenhaSmtp.Valor;

            var client = new SmtpClient(emailServer, Convert.ToInt32(emailPort));

            if (!string.IsNullOrEmpty(senhaSmtp))
            {
                client.EnableSsl = true;
                client.Credentials = new NetworkCredential(remetente, senhaSmtp);
            }

            var mailMessage = new MailMessage();
            mailMessage.BodyEncoding = Encoding.UTF8;
            mailMessage.Subject = "Acompanhamento Faturamento EDI - Malha Sul";
            mailMessage.From = new MailAddress(remetente);

            IEnumerable<ContatoAvisoFat> contatos =
                _contatoAvisoFatRepository.ObterTodos().Where(x => x.EnviaPlanilha == EnviaMensagemAvisoEnum.Envia);

            foreach (ContatoAvisoFat contatoAvisoFat in contatos)
            {
                mailMessage.To.Add(contatoAvisoFat.Email);
            }

            var corpo = new StringBuilder();

            corpo.AppendFormat(@"                                
                                <table>
                                    <tr>
                                        <td>
                                            <p>Bom dia!</p>
                                        </td>
                                    </tr>
                                    <tr>                                 
                                        <td>
                                            <p>Segue o percentual  por Produto.</p></br>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            {0}
                                        </td>
                                    </tr>  
                                    <tr><td>&nbsp;</td></tr>                                 
                                    <tr>
                                        <td>
                                            <p style='text-align:left;' >Abaixo segue o percentual de cada cliente em D-1.</p></br>
                                        </td>
                                    </tr>       
                                    <tr>
                                        <td>
                                            {1}</br>  
                                        </td>
                                    </tr>
                                </table>", planilha1, planilha2);

            mailMessage.Body =
                "<style type='text/css'> p{ font-family: Calibri,sans-serif; font-size: 12pt; } table, tr, th, td { font-family: Calibri,sans-serif; font-size: 10pt; } </style>" +
                corpo;

            AlternateView html = AlternateView.CreateAlternateViewFromString(mailMessage.Body, null,
                                                                             MediaTypeNames.Text.Html);
            mailMessage.AlternateViews.Add(html);
            mailMessage.IsBodyHtml = true;

            string fileName = "C:/Translogic/Translogic.JobRunner/modeloFatEnvio.xlsx";

            var fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);
            Attachment at = CreateAttachment(fs,
                                             "Acompanhamento_Faturamento_" +
                                             DateTime.Today.Date.ToString().Replace("/", "_").Replace(" ", "") + ".xlsx");
            mailMessage.Attachments.Add(at);

            client.Send(mailMessage);
        }

        public static Attachment CreateAttachment(Stream attachmentFile, string displayName)
        {
            var attachment = new Attachment(attachmentFile, displayName);
            attachment.ContentType = new ContentType("application/vnd.ms-excel");
            attachment.TransferEncoding = TransferEncoding.Base64;
            attachment.NameEncoding = Encoding.UTF8;
            string encodedAttachmentName = Convert.ToBase64String(Encoding.UTF8.GetBytes(displayName));
            encodedAttachmentName = SplitEncodedAttachmentName(encodedAttachmentName);
            attachment.Name = encodedAttachmentName;
            return attachment;
        }

        private static string SplitEncodedAttachmentName(string encoded)
        {
            const string encodingtoken = "=?UTF-8?B?";
            const string softbreak = "?=";
            const int maxChunkLength = 30;
            int splitLength = maxChunkLength - encodingtoken.Length - (softbreak.Length * 2);
            IEnumerable<string> parts = SplitByLength(encoded, splitLength);
            string encodedAttachmentName = encodingtoken;
            foreach (string part in parts)
            {
                encodedAttachmentName += part + softbreak + encodingtoken;
            }
            encodedAttachmentName = encodedAttachmentName.Remove(encodedAttachmentName.Length - encodingtoken.Length,
                                                                 encodingtoken.Length);
            return encodedAttachmentName;
        }

        private static IEnumerable<string> SplitByLength(string stringToSplit, int length)
        {
            while (stringToSplit.Length > length)
            {
                yield return stringToSplit.Substring(0, length);
                stringToSplit = stringToSplit.Substring(length);
            }
            if (stringToSplit.Length > 0)
            {
                yield return stringToSplit;
            }
        }

        private void Log(string message, EventLogEntryType type)
        {
            var source = "Translogic";

            if (!EventLog.SourceExists(source))
            {
                EventLog.CreateEventSource(source, source);
            }

            EventLog.WriteEntry(source, message, type);
        }
    }
}