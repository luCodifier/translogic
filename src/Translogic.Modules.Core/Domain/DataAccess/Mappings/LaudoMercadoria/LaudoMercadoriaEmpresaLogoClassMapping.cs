﻿namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.LaudoMercadoria
{
    using NHibernate;
    using NHibernate.Mapping.ByCode;
    using NHibernate.Mapping.ByCode.Conformist;
    using NHibernate.Type;
    using Translogic.Modules.Core.Domain.Model.LaudoMercadoria;

    public class LaudoMercadoriaEmpresaLogoClassMapping : ClassMapping<LaudoMercadoriaEmpresaLogo>
    {
        public LaudoMercadoriaEmpresaLogoClassMapping()
        {
            this.Lazy(true);
            this.Table("WS_LAUDO_EMPRESA");
            this.Schema("TRANSLOGIC");

            this.Id(
                x => x.Id,
                m =>
                {
                    m.Column("ID_WS_LAUDO_EMPRESA");
                    m.Generator(
                        Generators.Sequence,
                        g => g.Params(new
                        {
                            sequence = "WS_LAUDO_EMP_SEQ_ID"
                        }));
                    m.Type(new Int32Type());
                    m.Access(Accessor.Property);
                });

            this.Property(
                x => x.Logo, 
                map =>
                {
                    map.Column("EMPRESA_LOGO"); 
                    map.Unique(false); 
                    map.NotNullable(false);
                    map.Type(NHibernateUtil.BinaryBlob);
                });
        }
    }
}