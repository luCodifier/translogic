﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Type;
using Translogic.Modules.Core.Domain.Model.Seguro;

namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.Seguro
{
    /// <summary>
    ///     Mapeamento da classe <see cref="ClienteSeguro"  />
    /// </summary>
    public class MangaClassMapping : ClassMapping<Manga>
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="MangaClassMapping" /> class.
        /// </summary>
        public MangaClassMapping() 
        {
            Lazy(true);
            Table("LIMITE_PESO_MANGA");
            Schema("TRANSLOGIC");

            Property(
                x => x.LM_MANGA,
                map =>
                {
                    map.Column("LM_MANGA");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            Property(
                x => x.LM_LIMITE_PESO,
                map =>
                {
                    map.Column("LM_LIMITE_PESO");
                    map.Unique(false);
                    map.NotNullable(false);
                });
        }
    }
}