﻿namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.Documentacao
{
    using NHibernate.Mapping.ByCode;
    using NHibernate.Mapping.ByCode.Conformist;
    using NHibernate.Type;
    using Translogic.Modules.Core.Domain.Model.Documentacao;

    public class GerenciamentoDocumentacaoClassMapping : ClassMapping<GerenciamentoDocumentacao>
    {
        public GerenciamentoDocumentacaoClassMapping()
        {
            this.Lazy(true);
            this.Table("GERENCIAMENTO_DOC");
            this.Schema("TRANSLOGIC");

            this.Id(
                x => x.Id,
                m =>
                {
                    m.Column("ID_GERENCIAMENTO_DOC");
                    m.Generator(
                        Generators.Sequence,
                        g => g.Params(new
                        {
                            sequence = "GERENCIAMENTO_DOC_SEQ_ID"
                        }));
                    m.Type(new Int32Type());
                    m.Access(Accessor.Property);
                });

            this.Property(
                x => x.DespachoId,
                map =>
                {
                    map.Column("ID_DESPACHO");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            this.Property(
                x => x.ComposicaoId,
                map =>
                {
                    map.Column("ID_COMPOSICAO");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            this.Property(
                x => x.OsId,
                map =>
                {
                    map.Column("ID_OS");
                    map.Unique(false);
                    map.NotNullable(true);
                });

            this.ManyToOne(
                    x => x.GerenciamentoDocumentacaoRefaturamento,
                    m =>
                    {
                        m.Column("ID_GERENCIAMENTO_DOC_REFAT");
                        m.Cascade(Cascade.None);
                        m.Access(Accessor.Property);
                        m.Lazy(LazyRelation.NoLazy);
                        m.Fetch(FetchKind.Select);
                        m.Update(true);
                        m.Insert(true);
                        m.Unique(false);
                    });

            this.Property(
                x => x.OsNumero,
                map =>
                {
                    map.Column("OS_NUMERO");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            this.Property(
                x => x.PrefixoTrem,
                map =>
                {
                    map.Column("PREFIXO_TREM");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            this.Property(
                x => x.RecebedorId,
                map =>
                {
                    map.Column("ID_RECEBEDOR");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            this.Property(
                x => x.ChaveAcesso,
                map =>
                {
                    map.Column("CHAVE_ACESSO");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            this.Property(
                x => x.UrlAcesso,
                map =>
                {
                    map.Column("URL_ACESSO");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            this.Property(
                x => x.DiasValidade,
                map =>
                {
                    map.Column("DIAS_VALIDADE");
                    map.Unique(false);
                    map.NotNullable(true);
                });

            this.Property(
                x => x.Tentativas,
                map =>
                {
                    map.Column("NUMERO_TENTATIVAS");
                    map.Unique(false);
                    map.NotNullable(true);
                });

            this.Property(
                x => x.CriadoEm,
                map =>
                {
                    map.Column("CRIADO_EM");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            this.Property(
                x => x.ExcluidoEm,
                map =>
                {
                    map.Column("EXCLUIDO_EM");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            this.Property(
                x => x.ErroEm,
                map =>
                {
                    map.Column("ERRO_EM");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            this.Property(
                x => x.MensagemErro,
                map =>
                {
                    map.Column("MENSAGEM_ERRO");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            this.Property(
                x => x.GerarRelatorioLaudoMercadoria,
                map =>
                {
                    map.Column("GERAR_REL_LAUDO_MERCADORIA");
                    map.Unique(false);
                    map.NotNullable(true);
                });

            this.Property(
                x => x.NfeCompleto,
                map =>
                {
                    map.Column("NFE_COMPLETO");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            this.Property(
                x => x.TicketCompleto,
                map =>
                {
                    map.Column("TICKET_COMPLETO");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            this.Property(
                x => x.LaudoCompleto,
                map =>
                {
                    map.Column("LAUDO_COMPLETO");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            this.Bag(
                 x => x.GerenciamentoDocumentacaoCompVagao,
                 set =>
                 {
                     set.Cascade(Cascade.DeleteOrphans); // set cascade strategy
                     set.Key(k => k.Column(col => col.Name("ID_GERENCIAMENTO_DOC"))); // foreign key
                     set.Inverse(true);
                     set.Lazy(CollectionLazy.NoLazy);
                     set.Fetch(CollectionFetchMode.Select);
                 },
                 a => a.OneToMany());
        }
    }
}