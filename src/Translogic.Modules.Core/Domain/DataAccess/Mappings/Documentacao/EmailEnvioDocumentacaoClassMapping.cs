﻿namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.Documentacao
{
    using NHibernate.Mapping.ByCode;
    using NHibernate.Mapping.ByCode.Conformist;
    using NHibernate.Type;
    using Translogic.Modules.Core.Domain.Model.Documentacao;

    public class EmailEnvioDocumentacaoClassMapping : ClassMapping<EmailEnvioDocumentacao>
    {
        public EmailEnvioDocumentacaoClassMapping()
        {
            this.Lazy(true);
            this.Table("EMAIL_ENVIO_DOC");
            this.Schema("TRANSLOGIC");

            this.Id(
                x => x.Id,
                m =>
                {
                    m.Column("ID_EMAIL_ENVIO_DOC");
                    m.Generator(
                        Generators.Sequence,
                        g => g.Params(new
                        {
                            sequence = "EMAIL_ENVIO_DOC_SEQ_ID"
                        }));
                    m.Type(new Int32Type());
                    m.Access(Accessor.Property);
                });

            this.ManyToOne(
                    x => x.ConfiguracaoEnvio, m =>
                    {
                        m.Column("ID_CONF_ENVIO_DOC_EMAIL");
                        m.Cascade(Cascade.None);
                        m.Access(Accessor.Property);
                        m.Fetch(FetchKind.Join);
                        m.Lazy(LazyRelation.NoLazy);
						m.Update(true);
						m.Insert(true);
						m.Unique(false);
                    });

            this.Property(
                x => x.Email,
                map =>
                {
                    map.Column("EMAIL");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            this.Property(
                x => x.Ativo,
                map =>
                {
                    map.Column("ATIVO");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            this.Property(
                x => x.CriadoEm,
                map =>
                {
                    map.Column("CRIADO_EM");
                    map.Unique(false);
                    map.NotNullable(false);
                });
        }
    }
}