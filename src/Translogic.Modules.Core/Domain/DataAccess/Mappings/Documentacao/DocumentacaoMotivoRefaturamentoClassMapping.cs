﻿namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.Documentacao
{
    using NHibernate.Mapping.ByCode;
    using NHibernate.Mapping.ByCode.Conformist;
    using NHibernate.Type;
    using Translogic.Modules.Core.Domain.Model.Documentacao;

    public class DocumentacaoMotivoRefaturamentoClassMapping : ClassMapping<DocumentacaoMotivoRefaturamento>
    {
        public DocumentacaoMotivoRefaturamentoClassMapping()
        {
            this.Lazy(true);
            this.Table("GERENCIAMENTO_DOC_MOT_REFAT");
            this.Schema("TRANSLOGIC");

            this.Id(
                 x => x.Id,
                 m =>
                 {
                     m.Column("ID_GERENCIAMENTO_DOC_MOT_REFAT");
                     m.Generator(
                         Generators.Sequence,
                         g => g.Params(new
                         {
                             sequence = "GER_DOC_MOT_REFAT_SEQ_ID"
                         }));
                     m.Type(new Int32Type());
                     m.Access(Accessor.Property);
                 });

            this.ManyToOne(
                    x => x.Responsavel,
                    m =>
                    {
                        m.Column("ID_GERENCIAMENTO_DOC_RES_REFAT");
                        m.Cascade(Cascade.None);
                        m.Access(Accessor.Property);
                        m.Lazy(LazyRelation.Proxy);
                        m.Fetch(FetchKind.Select);
                        m.Update(true);
                        m.Insert(true);
                        m.Unique(false);
                    });

            this.Property(
                x => x.Motivo,
                map =>
                {
                    map.Column("MOTIVO");
                    map.Unique(false);
                    map.NotNullable(true);
                });

            this.Property(
                x => x.Observacao,
                map =>
                {
                    map.Column("OBSERVACAO");
                    map.Unique(false);
                    map.NotNullable(true);
                });

            this.Property(
                x => x.Ativo,
                map =>
                {
                    map.Column("ATIVO");
                    map.Unique(false);
                    map.NotNullable(true);
                });

            this.Property(
                x => x.CriadoEm,
                map =>
                {
                    map.Column("CRIADO_EM");
                    map.Unique(false);
                    map.NotNullable(true);
                });
        }
    }
}