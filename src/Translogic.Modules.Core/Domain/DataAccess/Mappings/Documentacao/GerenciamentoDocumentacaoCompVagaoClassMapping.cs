﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Type;
using Translogic.Modules.Core.Domain.Model.Documentacao;

namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.Documentacao
{
    public class GerenciamentoDocumentacaoCompVagaoClassMapping : ClassMapping<GerenciamentoDocumentacaoCompVagao>
    {
        public GerenciamentoDocumentacaoCompVagaoClassMapping()
        {
            this.Lazy(true);
            this.Table("GERENCIAMENTO_DOC_COMPVAG");
            this.Schema("TRANSLOGIC");

            this.Id(
                x => x.Id,
                m =>
                {
                    m.Column("ID_GERENCIAMENTO_DOC_COMPVAG");
                    m.Generator(
                        Generators.Sequence,
                        g => g.Params(new
                        {
                            sequence = "GER_DOC_COMPVAG_SEQ_ID"
                        }));
                    m.Type(new Int32Type());
                    m.Access(Accessor.Property);
                });

            this.Property(
                x => x.OsId,
                map =>
                {
                    map.Column("ID_OS");
                    map.Unique(false);
                    map.NotNullable(true);
                });

            this.Property(
                x => x.ComposicaoId,
                map =>
                {
                    map.Column("ID_COMPOSICAO");
                    map.Unique(false);
                    map.NotNullable(true);
                });

            this.Property(
                x => x.RecebedorId,
                map =>
                {
                    map.Column("ID_RECEBEDOR");
                    map.Unique(false);
                    map.NotNullable(true);
                });

            this.Property(
                x => x.CodigoVagao,
                map =>
                {
                    map.Column("CODIGO_VAGAO");
                    map.Unique(false);
                    map.NotNullable(true);
                });

            this.Property(
                x => x.CriadoEm,
                map =>
                {
                    map.Column("CRIADO_EM");
                    map.Unique(false);
                    map.NotNullable(true);
                });

            this.ManyToOne(
                    x => x.GerenciamentoDocumentacao,
                    m =>
                    {
                        m.Column("ID_GERENCIAMENTO_DOC");
                        m.Cascade(Cascade.None);
                        m.Access(Accessor.Property);
                        m.Lazy(LazyRelation.Proxy);
                        m.Fetch(FetchKind.Select);
                        m.Update(true);
                        m.Insert(true);
                        m.Unique(false);
                    });
        }
    }
}