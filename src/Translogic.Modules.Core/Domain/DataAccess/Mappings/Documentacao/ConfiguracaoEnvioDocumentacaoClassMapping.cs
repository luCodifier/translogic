﻿namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.Documentacao
{
    using NHibernate.Mapping.ByCode;
    using NHibernate.Mapping.ByCode.Conformist;
    using NHibernate.Type;
    using Translogic.Modules.Core.Domain.Model.Documentacao;

    public class ConfiguracaoEnvioDocumentacaoClassMapping : ClassMapping<ConfiguracaoEnvioDocumentacao>
    {
        public ConfiguracaoEnvioDocumentacaoClassMapping()
        {
            this.Lazy(true);
            this.Table("CONF_ENVIO_DOC");
            this.Schema("TRANSLOGIC");

            this.Id(
                x => x.Id,
                m =>
                {
                    m.Column("ID_CONF_ENVIO_DOC");
                    m.Generator(
                        Generators.Sequence,
                        g => g.Params(new
                        {
                            sequence = "CONF_ENVIO_DOC_SEQ_ID"
                        }));
                    m.Type(new Int32Type());
                    m.Access(Accessor.Property);
                });

            this.ManyToOne(
                    x => x.EmpresaDestino, m =>
                    {
                        m.Column("ID_EP_ID_EMP_DEST");
                        m.Cascade(Cascade.None);
                        m.Access(Accessor.Property);
                        m.Fetch(FetchKind.Select);
                        m.Lazy(LazyRelation.NoLazy);
                    });

            this.ManyToOne(
                    x => x.AreaOperacionalEnvio, m =>
                    {
                        m.Column("ID_AO_ID_AO_ENVIO");
                        m.Cascade(Cascade.None);
                        m.Access(Accessor.Property);
                        m.Fetch(FetchKind.Select);
                        m.Lazy(LazyRelation.NoLazy);
                    });

            this.Bag(
                x => x.Emails,
                set =>
                {
                    set.Cascade(Cascade.DeleteOrphans); // set cascade strategy
                    set.Key(k => k.Column(col => col.Name("ID_CONF_ENVIO_DOC_EMAIL"))); // foreign key
                    set.Inverse(true);
                    set.Lazy(CollectionLazy.NoLazy);
                    set.Fetch(CollectionFetchMode.Select);
                },
                a => a.OneToMany());

            this.Property(
                x => x.DiasValidadeDocumentacao,
                map =>
                {
                    map.Column("DIAS_VALIDADE_DOC");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            this.Property(
                x => x.GerarRelatorioLaudoMercadoria,
                map =>
                {
                    map.Column("GERAR_REL_LAUDO_MERCADORIA");
                    map.Unique(false);
                    map.NotNullable(true);
                });

            this.Property(
                x => x.EnviarChegada,
                map =>
                {
                    map.Column("ENVIO_CHEGADA");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            this.Property(
                x => x.EnviarSaida,
                map =>
                {
                    map.Column("ENVIO_SAIDA");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            this.Property(
                x => x.Ativo,
                map =>
                {
                    map.Column("ATIVO");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            this.Property(
                x => x.CriadoEm,
                map =>
                {
                    map.Column("CRIADO_EM");
                    map.Unique(false);
                    map.NotNullable(false);
                });
        }
    }
}