﻿namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.Documentacao
{
    using NHibernate.Mapping.ByCode;
    using NHibernate.Mapping.ByCode.Conformist;
    using NHibernate.Type;
    using Translogic.Modules.Core.Domain.Model.Documentacao;

    public class DocumentacaoResponsavelRefaturamentoClassMapping : ClassMapping<DocumentacaoResponsavelRefaturamento>
    {
        public DocumentacaoResponsavelRefaturamentoClassMapping()
        {
            this.Lazy(true);
            this.Table("GERENCIAMENTO_DOC_RES_REFAT");
            this.Schema("TRANSLOGIC");

            this.Id(
                 x => x.Id,
                 m =>
                 {
                     m.Column("ID_GERENCIAMENTO_DOC_RES_REFAT");
                     m.Generator(
                         Generators.Sequence,
                         g => g.Params(new
                         {
                             sequence = "GER_DOC_RES_REFAT_SEQ_ID"
                         }));
                     m.Type(new Int32Type());
                     m.Access(Accessor.Property);
                 });

            this.Property(
                x => x.Responsavel,
                map =>
                {
                    map.Column("RESPONSAVEL");
                    map.Unique(false);
                    map.NotNullable(true);
                });

            this.Bag(
                 x => x.Motivos,
                 set =>
                 {
                     set.Cascade(Cascade.None); // set cascade strategy
                     set.Key(k => k.Column(col => col.Name("ID_GERENCIAMENTO_DOC_RES_REFAT"))); // foreign key
                     set.Inverse(true);
                     set.Lazy(CollectionLazy.NoLazy);
                     set.Fetch(CollectionFetchMode.Select);
                 },
                 a => a.OneToMany());

            this.Property(
                x => x.CriadoEm,
                map =>
                {
                    map.Column("CRIADO_EM");
                    map.Unique(false);
                    map.NotNullable(true);
                });
        }
    }
}