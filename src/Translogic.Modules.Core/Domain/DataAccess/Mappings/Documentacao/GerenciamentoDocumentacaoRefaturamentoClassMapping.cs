﻿namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.Documentacao
{
    using NHibernate.Mapping.ByCode;
    using NHibernate.Mapping.ByCode.Conformist;
    using NHibernate.Type;
    using Translogic.Modules.Core.Domain.Model.Documentacao;

    public class GerenciamentoDocumentacaoRefaturamentoClassMapping : ClassMapping<GerenciamentoDocumentacaoRefaturamento>
    {
        public GerenciamentoDocumentacaoRefaturamentoClassMapping()
        {
            this.Lazy(true);
            this.Table("GERENCIAMENTO_DOC_REFAT");
            this.Schema("TRANSLOGIC");

            this.Id(
                x => x.Id,
                m =>
                {
                    m.Column("ID_GERENCIAMENTO_DOC_REFAT");
                    m.Generator(
                        Generators.Sequence,
                        g => g.Params(new
                        {
                            sequence = "GER_DOC_REFAT_SEQ_ID"
                        }));
                    m.Type(new Int32Type());
                    m.Access(Accessor.Property);
                });

            this.Property(
                x => x.OsId,
                map =>
                {
                    map.Column("ID_OS");
                    map.Unique(false);
                    map.NotNullable(true);
                });

            this.Property(
                x => x.RecebedorId,
                map =>
                {
                    map.Column("ID_RECEBEDOR");
                    map.Unique(false);
                    map.NotNullable(true);
                });

            this.Property(
                x => x.ComposicaoId,
                map =>
                {
                    map.Column("ID_COMPOSICAO");
                    map.Unique(false);
                    map.NotNullable(true);
                });

            this.Property(
                x => x.NumeroOs,
                map =>
                {
                    map.Column("OS_NUMERO");
                    map.Unique(false);
                    map.NotNullable(true);
                });

            this.Property(
                x => x.PrefixoTrem,
                map =>
                {
                    map.Column("PREFIXO_TREM");
                    map.Unique(false);
                    map.NotNullable(true);
                });


            this.Property(
                x => x.Processado,
                map =>
                {
                    map.Column("PROCESSADO");
                    map.Unique(false);
                    map.NotNullable(true);
                });

            this.Property(
                x => x.CriadoEm,
                map =>
                {
                    map.Column("CRIADO_EM");
                    map.Unique(false);
                    map.NotNullable(true);
                });

            this.ManyToOne(
                    x => x.Documentacao,
                    m =>
                    {
                        m.Column("ID_GERENCIAMENTO_DOC");
                        m.Cascade(Cascade.None);
                        m.Access(Accessor.Property);
                        m.Lazy(LazyRelation.NoLazy);
                        m.Fetch(FetchKind.Select);
                        m.Update(true);
                        m.Insert(true);
                        m.Unique(false);
                    });

            this.ManyToOne(
                    x => x.MotivoRefaturamento,
                    m =>
                    {
                        m.Column("ID_GERENCIAMENTO_DOC_MOT_REFAT");
                        m.Cascade(Cascade.None);
                        m.Access(Accessor.Property);
                        m.Lazy(LazyRelation.NoLazy);
                        m.Fetch(FetchKind.Select);
                        m.Update(true);
                        m.Insert(true);
                        m.Unique(false);
                    });

            this.Bag(
                 x => x.Vagoes,
                 set =>
                 {
                     set.Cascade(Cascade.DeleteOrphans); // set cascade strategy
                     set.Key(k => k.Column(col => col.Name("ID_GERENCIAMENTO_DOC_REFAT"))); // foreign key
                     set.Inverse(true);
                     set.Lazy(CollectionLazy.NoLazy);
                     set.Fetch(CollectionFetchMode.Select);
                 },
                 a => a.OneToMany());
        }
    }
}