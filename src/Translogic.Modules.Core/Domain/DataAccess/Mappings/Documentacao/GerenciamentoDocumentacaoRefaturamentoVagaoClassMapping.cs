﻿namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.Documentacao
{
    using NHibernate.Mapping.ByCode;
    using NHibernate.Mapping.ByCode.Conformist;
    using NHibernate.Type;
    using Translogic.Modules.Core.Domain.Model.Documentacao;

    public class GerenciamentoDocumentacaoRefaturamentoVagaoClassMapping : ClassMapping<GerenciamentoDocumentacaoRefaturamentoVagao>
    {
        public GerenciamentoDocumentacaoRefaturamentoVagaoClassMapping()
        {
            this.Lazy(true);
            this.Table("GERENCIAMENTO_DOC_REFAT_VAG");
            this.Schema("TRANSLOGIC");

            this.Id(
                x => x.Id,
                m =>
                {
                    m.Column("ID_GERENCIAMENTO_DOC_REFAT_VAG");
                    m.Generator(
                        Generators.Sequence,
                        g => g.Params(new
                        {
                            sequence = "GER_DOC_REFAT_VAG_SEQ_ID"
                        }));
                    m.Type(new Int32Type());
                    m.Access(Accessor.Property);
                });

            this.ManyToOne(
                    x => x.GerenciamentoDocumentacaoRefaturamento,
                    m =>
                    {
                        m.Column("ID_GERENCIAMENTO_DOC_REFAT");
                        m.Cascade(Cascade.None);
                        m.Access(Accessor.Property);
                        m.Lazy(LazyRelation.Proxy);
                        m.Fetch(FetchKind.Select);
                        m.Update(true);
                        m.Insert(true);
                        m.Unique(false);
                    });

            this.Property(
                x => x.CodigoVagao,
                map =>
                {
                    map.Column("CODIGO_VAGAO");
                    map.Unique(false);
                    map.NotNullable(true);
                });

            this.Property(
                x => x.Ativo,
                map =>
                {
                    map.Column("ATIVO");
                    map.Unique(false);
                    map.NotNullable(true);
                });

            this.Property(
                x => x.CriadoEm,
                map =>
                {
                    map.Column("CRIADO_EM");
                    map.Unique(false);
                    map.NotNullable(true);
                });
        }
    }
}