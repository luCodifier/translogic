﻿namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.Documentacao
{
    using NHibernate.Mapping.ByCode;
    using NHibernate.Mapping.ByCode.Conformist;
    using NHibernate.Type;
    using Translogic.Modules.Core.Domain.Model.Documentacao;

    public class HistoricoEmailEnvioDocumentacaoClassMapping : ClassMapping<HistoricoEmailEnvioDocumentacao>
    {
        public HistoricoEmailEnvioDocumentacaoClassMapping()
        {
            this.Lazy(true);
            this.Table("HIST_ENVIO_DOC_EMAIL");
            this.Schema("TRANSLOGIC");

            this.Id(
                x => x.Id,
                m =>
                {
                    m.Column("ID_HIST_ENVIO_DOC_EMAIL");
                    m.Generator(
                        Generators.Sequence,
                        g => g.Params(new
                        {
                            sequence = "HIST_ENVIO_DOC_EMAIL_SEQ_ID"
                        }));
                    m.Type(new Int32Type());
                    m.Access(Accessor.Property);
                });

            this.ManyToOne(
                    x => x.HistoricoEnvioDocumentacao,
                    m =>
                    {
                        m.Column("ID_HIST_ENVIO_DOC");
                        m.Cascade(Cascade.All);
                        m.Access(Accessor.Property);
                        m.Lazy(LazyRelation.Proxy);
                        m.Fetch(FetchKind.Select);
                        m.Update(true);
                        m.Insert(true);
                        m.Unique(false);
                    });

            this.Property(
                x => x.Email,
                map =>
                {
                    map.Column("EMAIL");
                    map.Unique(false);
                    map.NotNullable(true);
                });

            this.Property(
                x => x.Enviado,
                map =>
                {
                    map.Column("ENVIADO");
                    map.Unique(false);
                    map.NotNullable(true);
                });

            this.Property(
                x => x.EnviadoEm,
                map =>
                {
                    map.Column("ENVIADO_EM");
                    map.Unique(false);
                    map.NotNullable(false);
                });
        }
    }
}