﻿namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.Documentacao
{
    using NHibernate.Mapping.ByCode;
    using NHibernate.Mapping.ByCode.Conformist;
    using NHibernate.Type;
    using Translogic.Modules.Core.Domain.Model.Documentacao;

    public class HistoricoEnvioDocumentacaoClassMapping : ClassMapping<HistoricoEnvioDocumentacao>
    {
        public HistoricoEnvioDocumentacaoClassMapping()
        {
            this.Lazy(true);
            this.Table("HIST_ENVIO_DOC");
            this.Schema("TRANSLOGIC");

            this.Id(
                 x => x.Id,
                 m =>
                 {
                     m.Column("ID_HIST_ENVIO_DOC");
                     m.Generator(
                         Generators.Sequence,
                         g => g.Params(new
                         {
                             sequence = "HIST_ENVIO_DOC_SEQ_ID"
                         }));
                     m.Type(new Int32Type());
                     m.Access(Accessor.Property);
                 });

            this.ManyToOne(
                    x => x.Documentacao,
                    m =>
                    {
                        m.Column("ID_GERENCIAMENTO_DOC");
                        m.Cascade(Cascade.None);
                        m.Access(Accessor.Property);
                        m.Lazy(LazyRelation.NoLazy);
                        m.Fetch(FetchKind.Select);
                        m.Update(true);
                        m.Insert(true);
                        m.Unique(false);
                    });

            this.Bag(
                 x => x.Emails,
                 set =>
                 {
                     set.Cascade(Cascade.Persist); // set cascade strategy
                     set.Key(k => k.Column(col => col.Name("ID_HIST_ENVIO_DOC"))); // foreign key
                     set.Inverse(true);
                     set.Lazy(CollectionLazy.NoLazy);
                     set.Fetch(CollectionFetchMode.Select);
                 },
                 a => a.OneToMany());

            this.Property(
                x => x.OsId,
                map =>
                {
                    map.Column("ID_OS");
                    map.Unique(false);
                    map.NotNullable(true);
                });


            this.Property(
                x => x.ComposicaoId,
                map =>
                {
                    map.Column("ID_COMPOSICAO");
                    map.Unique(false);
                    map.NotNullable(true);
                });

            this.Property(
                x => x.RecebedorId,
                map =>
                {
                    map.Column("ID_RECEBEDOR");
                    map.Unique(false);
                    map.NotNullable(true);
                });

            this.Property(
                x => x.Enviado,
                map =>
                {
                    map.Column("ENVIADO");
                    map.Unique(false);
                    map.NotNullable(true);
                });

            this.Property(
                x => x.Recomposicao,
                map =>
                {
                    map.Column("RECOMPOSICAO");
                    map.Unique(false);
                    map.NotNullable(true);
                });

            this.Property(
                x => x.Manual,
                map =>
                {
                    map.Column("ENVIO_MANUAL");
                    map.Unique(false);
                    map.NotNullable(true);
                });

            this.Property(
                x => x.UsuarioReenvioId,
                map =>
                {
                    map.Column("ID_USUARIO_REENVIO");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            this.Property(
                x => x.Chegada,
                map =>
                {
                    map.Column("ENVIO_CHEGADA");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            this.Property(
                x => x.Saida,
                map =>
                {
                    map.Column("ENVIO_SAIDA");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            this.Property(
                x => x.DataChegada,
                map =>
                {
                    map.Column("DATA_CHEGADA");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            this.Property(
                x => x.CriadoEm,
                map =>
                {
                    map.Column("CRIADO_EM");
                    map.Unique(false);
                    map.NotNullable(true);
                });
        }
    }
}