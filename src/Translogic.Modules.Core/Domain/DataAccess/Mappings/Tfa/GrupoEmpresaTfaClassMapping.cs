﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NHibernate.Mapping.ByCode.Conformist;
using Translogic.Modules.Core.Domain.Model.Estrutura;
using NHibernate.Mapping.ByCode;
using Translogic.Modules.Core.Domain.Model.Tfa;
using NHibernate.Type;

namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.Tfa
{
    /// <summary>
    ///     Mapeamento da classe <see cref="GrupoEmpresaTfa"  />
    /// </summary>
    public class GrupoEmpresaTfaClassMapping : ClassMapping<GrupoEmpresaTfa>
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="GrupoEmpresaTfa" /> class.
        /// </summary>
        public GrupoEmpresaTfaClassMapping()
        {
            Lazy(true);
            Table("GRUPO_EMPRESA");
            Schema("TRANSLOGIC");

            Id(
              x => x.Id,
              map =>
              {
                  map.Column("ID_GRUPO_EMPRESA");
                  map.Generator(
                      Generators.Sequence,
                      g => g.Params(new
                      {
                          sequence = "GRUPO_EMPRESA_SEQ"
                      }));
                  map.Type(new Int32Type());
                  map.Access(Accessor.Property);
              });

            Property(
                x => x.NomeGrupo,
                map =>
                {
                    map.Column("NOME_GRUPO");
                    map.Length(30);
                    map.NotNullable(true);
                });

           
            this.Bag(
                 x => x.Empresas,
                 set =>
                 {
                     set.Cascade(Cascade.None); // set cascade strategy
                     set.Key(k => k.Column(col => col.Name("ID_GRUPO_EMPRESA"))); // foreign key
                     set.Inverse(true);
                     set.Lazy(CollectionLazy.Lazy);
                     set.Fetch(CollectionFetchMode.Select);
                 },
                 a => a.OneToMany());


        }
    }
}