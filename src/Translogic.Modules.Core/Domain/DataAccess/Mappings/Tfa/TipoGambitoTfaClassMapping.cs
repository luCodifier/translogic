﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Translogic.Modules.Core.Domain.Model.Tfa;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;

namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.Tfa
{
    /// <summary>
    ///   Mapeamento da classe <see cref="TipoGambitoTfa" />
    /// </summary>
    public class TipoGambitoTfaClassMapping : ClassMapping<TipoGambitoTfa>
    {
        public TipoGambitoTfaClassMapping()
        {
            Lazy(true);
            Table("TIPO_GAMBITO_TFA");
            Schema("TRANSLOGIC");

            Id(
               x => x.Id,
               map =>
               {
                   map.Column("COD_TIPO_GAMBITO_TFA");
                   map.Generator(Generators.Assigned);
                   map.Access(Accessor.Property);
               });

            Property(
                    x => x.Descricao,
                    map =>
                    {
                        map.Column("DESC_TIPO_GAMBITO_TFA");
                        map.Length(30);
                        map.Unique(true);
                        map.NotNullable(true);
                    });
        }
    }
}