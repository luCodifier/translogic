﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Type;
using Translogic.Modules.Core.Domain.Model.Tfa;


namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.Tfa
{
    /// <summary>
    ///     Mapeamento da classe <see cref="TfaAnexo"  />
    /// </summary>
    public class TfaAnexoClassMapping : ClassMapping<TfaAnexo>
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="TfaAnexoClassMapping" /> class.
        /// </summary>
        public TfaAnexoClassMapping()
        {
            Lazy(true);
            Table("ANEXO_TFA");
            Schema("TRANSLOGIC");

            Id(
               x => x.Id,
               map =>
               {
                   map.Column("ID_ANEXO_TFA");
                   map.Generator(
                       Generators.Sequence,
                       g => g.Params(new
                       {
                           sequence = "ANEXO_TFA_SEQ"
                       }));
                   map.Type(new Int32Type());
                   map.Access(Accessor.Property);
               });

            Property(
                x => x.NumProcesso,
                map =>
                {
                    map.Column("NUMPROCESSO");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            this.ManyToOne(
                x => x.Tfa, m =>
                {
                    m.Column("ID_TFA");
                    m.Class(typeof(Translogic.Modules.Core.Domain.Model.Tfa.Tfa));
                    m.Cascade(Cascade.None);
                    m.Fetch(FetchKind.Join);
                    m.Lazy(LazyRelation.Proxy);
                });

            Property(
                x => x.ArquivoPdf,
                map =>
                {
                    map.Column("ARQUIVO_PDF");
                    map.Unique(false);
                    map.NotNullable(false);
                });


        }
    }
}