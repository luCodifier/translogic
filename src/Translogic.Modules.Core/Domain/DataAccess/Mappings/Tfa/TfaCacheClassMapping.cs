﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Translogic.Modules.Core.Domain.Model.Tfa;


namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.Tfa
{


    public class TfaCacheClassMapping : ClassMapping<TfaCache>
    {

        public TfaCacheClassMapping()
        {
            Table("TFA_CACHE");
            Schema("TRANSLOGIC");
            Lazy(true);
            Id(x => x.Id, map =>
            {
                map.Column("ID_TFA_CACHE");
                map.Generator(Generators.Sequence, g => g.Params(new { sequence = "TFA_CACHE_SEQ" }));
            });
            Property(x => x.NumeroVagao, map => { map.Column("NUMERO_VAGAO"); map.NotNullable(true); map.Length(7); });
            Property(x => x.NumeroDespacho, map => { map.Column("NUMERO_DESPACHO"); map.NotNullable(true); });
            Property(x => x.SerieDespacho, map => { map.Column("SERIE_DESPACHO"); map.NotNullable(true); map.Length(3); });
            Property(x => x.PesoOrigem, map => { map.Column("PESO_ORIGEM"); map.NotNullable(true); });
            Property(x => x.PesoDestino, map => { map.Column("PESO_DESTINO"); map.NotNullable(true); });
            Property(x => x.IdLocalVistoria, map => { map.Column("ID_LOCAL_VISTORIA"); map.NotNullable(true); });            
            Property(x => x.IdCausaDano, map => { map.Column("ID_CAUSA_DANO"); map.NotNullable(true); });
            Property(x => x.CodTipoLacre, map => { map.Column("COD_TIPO_LACRE"); map.NotNullable(true); });
            Property(x => x.CodTipoGambito, map => { map.Column("COD_TIPO_GAMBITO"); map.NotNullable(true); });
            Property(x => x.CodTipoConclusaoTfa, map => { map.Column("COD_TIPO_CONCLUSAO_TFA"); map.NotNullable(true); });
            Property(x => x.ConsideracoesAdicionais, map => { map.Column("CONSIDERACOES_ADICIONAIS"); map.Length(500);});
            Property(x => x.NomeRepresentante, map => { map.Column("NOME_REPRESENTANTE"); map.Length(60); });
            Property(x => x.DocumentoRepresentante, map => {map.Column("DOCUMENTO_REPRESENTANTE");  map.Length(15);});
            Property(x => x.ImgAssinaturaRepresentante, map => map.Column("IMG_ASSINATURA_REPRESENTANTE"));
            Property(x => x.ResponsavelCiente, map => { map.Column("RESPONSAVEL_CIENTE"); map.NotNullable(true); });
            Property(x => x.DataHoraSinistro, map => { map.Column("DATA_HORA_SINISTRO"); map.NotNullable(true); });
            Property(x => x.DataHoraVistoria, map => { map.Column("DATA_HORA_VISTORIA"); map.NotNullable(true); });
            Property(x => x.MatriculaVistoriador, map => { map.Column("MATRICULA_VISTORIADOR"); map.NotNullable(true); });
            Property(x => x.NomeVistoriador, map => { map.Column("NOME_VISTORIADOR"); map.NotNullable(true); map.Length(60); });
            Property(x => x.EdicaoManual, map => { map.Column("EDICAO_MANUAL"); map.NotNullable(true); });
            Property(x => x.CodTipoLiberadorDescarga, map => { map.Column("COD_TIPO_LIBERADOR_DESCARGA"); map.NotNullable(false);});
            Property(x => x.Sindicancia, map => { map.Column("SINDICANCIA"); map.NotNullable(false); });
            Property(x => x.MatriculaUsuarioAlteracao, map => { map.Column("MATRICULA_USUARIO_ALTERACAO"); map.NotNullable(true); map.Length(10); });
            Property(x => x.DataHoraInclusao, map => { map.Column("DATA_HORA_INCLUSAO"); map.NotNullable(true); });
            Property(x => x.DataHoraAlteracao, map => { map.Column("DATA_HORA_ALTERACAO"); map.NotNullable(true); });
        }
    }
}
