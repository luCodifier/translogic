﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Type;
using Translogic.Modules.Core.Domain.Model.Tfa;
using Translogic.Modules.Core.Domain.Model.Seguro;
using ALL.Core.AcessoDados.TiposCustomizados;
using NHibernate;


namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.Tfa
{
    /// <summary>
    ///     Mapeamento da classe <see cref="Tfa"  />
    /// </summary>
    public class TfaClassMapping : ClassMapping<Translogic.Modules.Core.Domain.Model.Tfa.Tfa>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TfaClassMapping" /> class.
        /// </summary>
        public TfaClassMapping()
        {
            Lazy(true);
            Table("TFA");
            Schema("TRANSLOGIC");

            Id(
               x => x.Id,
               map =>
               {
                   map.Column("ID_TFA");
                   map.Generator(
                       Generators.Sequence,
                       g => g.Params(new
                       {
                           sequence = "TFA_SEQ"
                       }));
                   map.Type(new Int32Type());
                   map.Access(Accessor.Property);
               });

            this.ManyToOne(
               x => x.ProcessoSeguro,
               map =>
               {
                   map.Column("PC_ID_PC");
                   map.Class(typeof(ProcessoSeguro));
                   map.Cascade(Cascade.None);
                   map.Fetch(FetchKind.Join);
                   map.Lazy(LazyRelation.Proxy);
                   map.NotNullable(true);
               });

            Property(
                x => x.PesoDestino,
                map =>
                {
                    map.Column("PESO_DESTINO");
                    map.NotNullable(false);
                });

            this.ManyToOne(
               x => x.TipoLacreTfa,
               map =>
               {
                   map.Column("COD_TIPO_LACRE_TFA");
                   map.Class(typeof(TipoLacreTfa));
                   map.Cascade(Cascade.None);
                   map.Fetch(FetchKind.Join);
                   map.Lazy(LazyRelation.Proxy);
               });

            this.ManyToOne(
               x => x.TipoGambitoTfa,
               map =>
               {
                   map.Column("COD_TIPO_GAMBITO_TFA");
                   map.Class(typeof(TipoGambitoTfa));
                   map.Cascade(Cascade.None);
                   map.Fetch(FetchKind.Join);
                   map.Lazy(LazyRelation.Proxy);
               });

            this.ManyToOne(
              x => x.TipoLiberadorDescarga,
              map =>
              {
                  map.Column("COD_TIPO_LIBERADOR_DESCARGA");
                  map.Class(typeof(TipoLiberadorDescarga));
                  map.Cascade(Cascade.None);
                  map.Fetch(FetchKind.Join);
                  map.Lazy(LazyRelation.Proxy);
              });

            this.ManyToOne(
              x => x.TipoConclusaoTfa,
              map =>
              {
                  map.Column("COD_TIPO_CONCLUSAO_TFA");
                  map.Class(typeof(TipoConclusaoTfa));
                  map.Cascade(Cascade.None);
                  map.Fetch(FetchKind.Join);
                  map.Lazy(LazyRelation.Proxy);
              });

            Property(
                x => x.MatriculaVistoriador,
                map =>
                {
                    map.Column("MATRICULA_VISTORIADOR");
                    map.Length(10);
                    map.NotNullable(false);
                });

            Property(
                x => x.NomeRepresentante,
                map =>
                {
                    map.Column("NOME_REPRESENTANTE");
                    map.Length(60);
                    map.NotNullable(false);
                });

            Property(
               x => x.DocumentoRepresentante,
               map =>
               {
                   map.Column("DOC_REPRESENTANTE");
                   map.Length(15);
                   map.NotNullable(false);
               });

            Property(
               x => x.AssinaturaRepresentante,
               map =>
               {
                   map.Column("ASSINATURA_REPRESENTANTE");
                   map.Type(typeof(BooleanCharType), null);
                   map.NotNullable(true);
               });

            Property(
               x => x.ImagemAssinaturaRepresentante,
               map =>
               {
                   map.Column("IMG_ASSINATURA_REPRESENTANTE");
                   map.Type(NHibernateUtil.BinaryBlob);
               });

            Property(
                x => x.DataClienteCiente,
                map =>
                {
                    map.Column("DATA_CLIENTE_CIENTE");
                    map.NotNullable(false);
                });

            Property(
               x => x.StatusClienteCiente,
               map =>
               {
                   map.Column("STATUS_CLIENTE_CIENTE");
                   map.Type(typeof(BooleanCharType), null);
                   map.NotNullable(false);
               });

            Property(
              x => x.OrigemProcessoApp,
              map =>
              {
                  map.Column("ORIGEM_PROCESSO_APP");
                  map.Type(typeof(BooleanCharType), null);
                  map.NotNullable(true);
              });

            Property(
              x => x.IdTfaCache,
              map =>
              {
                  map.Column("ID_TFA_CACHE");                  
                  map.NotNullable(false);
              });

        }
    }
}