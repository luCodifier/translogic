﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Translogic.Modules.Core.Domain.Model.Tfa;
using NHibernate.Type;

namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.Tfa
{
    /// <summary>
    /// Mapeamento da classe <see cref="TerminalTfaEmail"  />
    /// </summary>
    public class TerminalTfaEmailClassMapping : ClassMapping<TerminalTfaEmail>
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="TerminalTfaEmail" /> class.
        /// </summary>
        public TerminalTfaEmailClassMapping()
        {
            Lazy(true);
            Table("TERMINAL_TFA_EMAIL");
            Schema("TRANSLOGIC");

            Id(
              x => x.Id,
              map =>
              {
                  map.Column("ID_TERMINAL_TFA_EMAIL");
                  map.Generator(
                      Generators.Sequence,
                      g => g.Params(new
                      {
                          sequence = "TERMINAL_TFA_EMAIL_SEQ"
                      }));
                  map.Type(new Int32Type());
                  map.Access(Accessor.Property);
              });

            this.ManyToOne(
               x => x.Terminal,
               map =>
               {
                   map.Column("ID_TERMINAL_TFA");
                   map.Class(typeof(TerminalTfa));
                   map.Cascade(Cascade.None);
                   map.Fetch(FetchKind.Join);
                   map.Lazy(LazyRelation.Proxy);
               });


            Property(
                x => x.Email,
                map =>
                {
                    map.Column("EMAIL");
                    map.Length(50);
                    map.NotNullable(true);
                });

        }
    }
}