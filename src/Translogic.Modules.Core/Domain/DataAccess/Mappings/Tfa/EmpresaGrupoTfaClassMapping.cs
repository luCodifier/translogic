﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NHibernate.Mapping.ByCode.Conformist;
using Translogic.Modules.Core.Domain.Model.Estrutura;
using NHibernate.Mapping.ByCode;
using Translogic.Modules.Core.Domain.Model.Tfa;
using NHibernate.Type;

namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.Tfa
{
    /// <summary>
    ///     Mapeamento da classe <see cref="EmpresaGrupoTfa"  />
    /// </summary>
    public class EmpresaGrupoTfaClassMapping : ClassMapping<EmpresaGrupoTfa>
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="EmpresaGrupoTfa" /> class.
        /// </summary>
        public EmpresaGrupoTfaClassMapping()
        {
            Lazy(true);
            Table("GRUPO_EMPRESA_EMPRESA");
            Schema("TRANSLOGIC");

            Id(
              x => x.Id,
              map =>
              {
                  map.Column("ID_GRUPO_EMPRESA_EMPRESA");
                  map.Generator(
                      Generators.Sequence,
                      g => g.Params(new
                      {
                          sequence = "GRUPO_EMPRESA_EMPRESA_SEQ"
                      }));
                  map.Type(new Int32Type());
                  map.Access(Accessor.Property);
              });

            
            this.ManyToOne(
               x => x.GrupoEmpresa,
               map =>
               {
                   map.Column("ID_GRUPO_EMPRESA");
                   map.Class(typeof(GrupoEmpresaTfa));
                   map.Cascade(Cascade.None);
                   map.Fetch(FetchKind.Join);
                   map.Lazy(LazyRelation.Proxy);
               });

            this.ManyToOne(
               x => x.Empresa,
               map =>
               {
                   map.Column("EP_ID_EMP");
                   map.Class(typeof(IEmpresa));
                   map.Cascade(Cascade.None);
                   map.Fetch(FetchKind.Join);
                   map.Lazy(LazyRelation.Proxy);
               });


        }
    }
}