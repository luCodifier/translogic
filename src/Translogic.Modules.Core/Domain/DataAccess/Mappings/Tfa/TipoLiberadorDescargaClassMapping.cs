﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NHibernate.Mapping.ByCode.Conformist;
using Translogic.Modules.Core.Domain.Model.Tfa;
using NHibernate.Mapping.ByCode;

namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.Tfa
{
    /// <summary>
    ///   Mapeamento da classe <see cref="TipoLiberadorDescargaTfa" />
    /// </summary>
    public class TipoLiberadorDescargaClassMapping : ClassMapping<TipoLiberadorDescarga>
    {
        public TipoLiberadorDescargaClassMapping()
		{
            Lazy(true);
            Table("TIPO_LIBERADOR_DESCARGA");
            Schema("TRANSLOGIC");

            Id(
              x => x.Id,
              map =>
              {
                  map.Column("COD_TIPO_LIBERADOR_DESCARGA");
                  map.Generator(Generators.Assigned);
                  map.Access(Accessor.Property);
              });

            Property(
				    x => x.Descricao,
				    map =>
				    {
                        map.Column("DESC_TIPO_LIBERADOR_DESCARGA");
                        map.Length(30);
                        map.Unique(true);
                        map.NotNullable(true);
				    });
        }
    }
}