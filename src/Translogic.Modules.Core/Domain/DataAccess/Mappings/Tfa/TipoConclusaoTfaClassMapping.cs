﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NHibernate.Mapping.ByCode.Conformist;
using Translogic.Modules.Core.Domain.Model.Tfa;
using NHibernate.Mapping.ByCode;

namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.Tfa
{
    /// <summary>
    ///   Mapeamento da classe <see cref="TipoConclusaoTfa" />
    /// </summary>
    public class TipoConclusaoTfaClassMapping : ClassMapping<TipoConclusaoTfa>
    {
        public TipoConclusaoTfaClassMapping()
		{
            Lazy(true);
            Table("TIPO_CONCLUSAO_TFA");
            Schema("TRANSLOGIC");

            Id(
               x => x.Id,
               map =>
               {
                   map.Column("COD_TIPO_CONCLUSAO_TFA");
                   map.Generator(Generators.Assigned);
                   map.Access(Accessor.Property);
               });

            Property(
				    x => x.Descricao,
				    map =>
				    {
                        map.Column("DESC_TIPO_CONCLUSAO_TFA");
                        map.Length(30);
					    map.Unique(true);
					    map.NotNullable(true);
				    });
        }

    }
}