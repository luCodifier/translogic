﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NHibernate.Mapping.ByCode.Conformist;
using Translogic.Modules.Core.Domain.Model.Estrutura;
using NHibernate.Mapping.ByCode;
using Translogic.Modules.Core.Domain.Model.Tfa;
using NHibernate.Type;

namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.Tfa
{
    /// <summary>
    ///     Mapeamento da classe <see cref="TerminalTfa"  />
    /// </summary>
    public class TerminalTfaClassMapping : ClassMapping<TerminalTfa>
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="TerminalTfa" /> class.
        /// </summary>
        public TerminalTfaClassMapping()
        {
            Lazy(true);
            Table("TERMINAL_TFA");
            Schema("TRANSLOGIC");

            Id(
              x => x.Id,
              map =>
              {
                  map.Column("ID_TERMINAL_TFA");
                  map.Generator(
                      Generators.Sequence,
                      g => g.Params(new
                      {
                          sequence = "TERMINAL_TFA_SEQ"
                      }));
                  map.Type(new Int32Type());
                  map.Access(Accessor.Property);
              });

            Property(
             x => x.IdTerminal,
             map =>
             {
                 map.Column("AO_ID_AO");
                 map.NotNullable(true);
             });


        }
    }
}