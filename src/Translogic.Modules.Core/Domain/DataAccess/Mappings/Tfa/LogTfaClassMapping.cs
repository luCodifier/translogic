﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Type;
using Translogic.Modules.Core.Domain.Model.Tfa;
using Translogic.Modules.Core.Domain.Model.Seguro;
using ALL.Core.AcessoDados.TiposCustomizados;
using NHibernate;


namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.Tfa
{
    /// <summary>
    /// Mapeamento da classe <see cref="LogTfa"  />
    /// </summary>
    public class LogTfaClassMapping : ClassMapping<LogTfa>
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="LogTfaClassMapping" /> class.
        /// </summary>
        public LogTfaClassMapping()
        {
            Lazy(true);
            Table("LOG_TFA");
            Schema("TRANSLOGIC");

            Id(
               x => x.Id,
               map =>
               {
                   map.Column("ID_LOG_TFA");
                   map.Generator(
                       Generators.Sequence,
                       g => g.Params(new
                       {
                           sequence = "LOG_TFA_SEQ"
                       }));
                   map.Type(new Int32Type());
                   map.Access(Accessor.Property);
               });

            Property(
                x => x.IdTfa,
                map =>
                {
                    map.Column("ID_TFA");                    
                    map.NotNullable(false);
                });

            Property(
                x => x.Descricao,
                map =>
                {
                    map.Column("DESC_LOG");
                    map.Length(4000);
                    map.NotNullable(false);
                });


            Property(
               x => x.CodTipoLogTfa,
               map =>
               {
                   map.Column("COD_TIPO_LOG_TFA");                   
                   map.NotNullable(false);
               });
            
            Property(
                x => x.VersionDate,
                map =>
                {
                    map.Column("DATA_HORA_LOG");
                    map.NotNullable(false);
                });

        }
    }
}