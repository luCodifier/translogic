﻿using Translogic.Modules.Core.Domain.Model.Diversos;

namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.Administracao
{
    using NHibernate.Mapping.ByCode;
    using NHibernate.Mapping.ByCode.Conformist;
    using NHibernate.Type;
    using Model.Administracao;

    public class LogConfGeralMapping : ClassMapping<LogConfGeral>
    {
        public LogConfGeralMapping()
        {
            Table("LOG_CONF_GERAL");
            Schema("TRANSLOGIC");

            Id(
                x => x.Id,
                m =>
                {
                    m.Column("LOG_CONF_GERAL_ID");
                    m.Generator(
                        Generators.Sequence,
                        g => g.Params(new
                        {
                            sequence = "LOG_CONF_GERAL_SEQ_ID"
                        }));
                    m.Type(new Int32Type());
                    m.Access(Accessor.Property);
                });

            Property(x => x.Chave);

            Property(x => x.Usuario, map => map.Column("US_USR_IDU"));

            Property(x => x.Anterior);

            Property(x => x.Novo);

            Property(x => x.Tipo);

            Property(x => x.Data, map => map.Column("TIMESTAMP"));
        }
    }
}