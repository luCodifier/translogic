﻿namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.Administracao
{
    using Model.Administracao;
    using NHibernate.Mapping.ByCode.Conformist;
    using NHibernate.Mapping.ByCode;

    public class ConfGeralMapping : ClassMapping<ConfGeral>
    {
        public ConfGeralMapping()
        {
            Table("CONF_GERAL");
            Schema("TRANSLOGIC");

            Id(
                x => x.Id, m =>
                {
                    m.Column("CHAVE");
                    m.Generator(Generators.Assigned);
                    m.Access(Accessor.Property);
                });
            Property(
                x => x.Valor,
                map => map.Column("VALOR"));
            Property(
               x => x.Descricao,
               map => map.Column("DESCRICAO"));
            Property(
               x => x.Data,
               map => map.Column("TIMESTAMP"));
        }
    }
}