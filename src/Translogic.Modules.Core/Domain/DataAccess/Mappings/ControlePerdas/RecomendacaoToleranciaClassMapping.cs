﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NHibernate.Mapping.ByCode.Conformist;
using Translogic.Modules.Core.Domain.Model.ControlePerdas;
using NHibernate.Mapping.ByCode;
using NHibernate.Type;
using Translogic.Modules.Core.Domain.Model.Estrutura;

namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.ControlePerdas
{
    /// <summary>
    ///     Mapeamento da classe <see cref="RecomendacaoTolerancia"  />
    /// </summary>
    public class RecomendacaoToleranciaClassMapping : ClassMapping<RecomendacaoTolerancia>
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="RecomendacaoTolerancia" /> class.
        /// </summary>
        public RecomendacaoToleranciaClassMapping()
        {
            Lazy(true);
            Table("RECOMENDACAO_TOLERANCIA");
            Schema("TRANSLOGIC");

            Id(
              x => x.Id,
              map =>
              {
                  map.Column("ID_RECOMENDACAO_TOLERANCIA");
                  map.Generator(
                      Generators.Sequence,
                      g => g.Params(new
                      {
                          sequence = "RECOMENDACAO_TOLERANCIA_SEQ"
                      }));
                  map.Type(new Int32Type());
                  map.Access(Accessor.Property);
              });

            Property(
                x => x.CodMalha,
                map =>
                {
                    map.Column("ML_CD_ML");
                    map.Length(2);
                    map.NotNullable(true);
                });

            Property(
                x => x.Valor,
                map =>
                {
                    map.Column("VALOR");
                    map.NotNullable(true);
                });

            Property(
                x => x.Email,
                map =>
                {
                    map.Column("EMAIL");
                    map.Length(50);
                    map.NotNullable(true);
                });
        }
    }
}