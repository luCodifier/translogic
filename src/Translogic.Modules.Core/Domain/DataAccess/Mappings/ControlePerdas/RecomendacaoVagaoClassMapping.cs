﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NHibernate.Mapping.ByCode.Conformist;
using Translogic.Modules.Core.Domain.Model.ControlePerdas;
using NHibernate.Mapping.ByCode;
using NHibernate.Type;
using Translogic.Modules.Core.Domain.Model.Seguro;
using ALL.Core.AcessoDados.TiposCustomizados;

namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.ControlePerdas
{
    /// <summary>
    ///     Mapeamento da classe <see cref="RecomendacaoVagao"  />
    /// </summary>
    public class RecomendacaoVagaoClassMapping : ClassMapping<RecomendacaoVagao>
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="RecomendacaoVagao" /> class.
        /// </summary>
        public RecomendacaoVagaoClassMapping()
        {
            Lazy(true);
            Table("RECOMENDACAO_VAGAO");
            Schema("TRANSLOGIC");

            Id(
              x => x.Id,
              map =>
              {
                  map.Column("ID_RECOMENDACAO_VAGAO");
                  map.Generator(
                      Generators.Sequence,
                      g => g.Params(new
                      {
                          sequence = "RECOMENDACAO_VAGAO_SEQ"
                      }));
                  map.Type(new Int32Type());
                  map.Access(Accessor.Property);
              });

            this.ManyToOne(
               x => x.CausaSeguro,
               map =>
               {
                   map.Column("CS_ID_CS");
                   map.Class(typeof(CausaSeguro));
                   map.Cascade(Cascade.None);
                   map.Fetch(FetchKind.Join);
                   map.Lazy(LazyRelation.Proxy);
                   map.NotNullable(true);
               });

            Property(
               x => x.Recomendacao,
               map =>
               {
                   map.Column("RECOMENDACAO");
                   map.Type(typeof(BooleanCharType), null);
                   map.NotNullable(true);
               });

            Property(
               x => x.Tolerancia,
               map =>
               {
                   map.Column("TOLERANCIA");
                   map.Type(typeof(BooleanCharType), null);
                   map.NotNullable(true);
               });
        }
    }
}