﻿namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.AnexacaoDesanexacao
{
    using NHibernate.Mapping.ByCode;
    using NHibernate.Mapping.ByCode.Conformist;
    using Translogic.Modules.Core.Domain.Model.AnexacaoDesanexacao;
    using NHibernate.Type;
    using Translogic.Modules.Core.Domain.Model.FormacaoAutomatica;

    public class LogFormAutoVeiculosClassMapping : ClassMapping<LogFormacaoAutoVeiculo>
    {
        public LogFormAutoVeiculosClassMapping()
        {
            this.Lazy(true);
            this.Table("LOG_FORM_AUTO_VEICULOS");
            this.Schema("TRANSLOGIC");

            this.Id(
                x => x.Id,
                m =>
                {
                    m.Column("ID_LOG_ANX_HIST_TREM");
                    m.Generator(
                        Generators.Sequence,
                        g => g.Params(new
                        {
                            sequence = "LOG_FORM_AUTO_VEICULOS_SEQ_ID"
                        }));
                    m.Type(new Int32Type());
                    m.Access(Accessor.Property);
                });

            ManyToOne(
                x => x.Lfa, m =>
                {
                    m.Column("LFA_ID");
                    m.Class(typeof(LogFormacaoAuto));
                    m.Cascade(Cascade.All);
                    m.Fetch(FetchKind.Select);
                    m.Update(true);
                    m.Insert(true);
                    m.Access(Accessor.Property);
                    m.Unique(false);
                    m.Lazy(LazyRelation.Proxy);
                    m.NotNullable(false);
                });

            Property(
                x => x.LfavNumVei,
                map =>
                {
                    map.Column("LFAV_NUM_VEIC");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            Property(
                x => x.LfavTpVei,
                map =>
                {
                    map.Column("LFAV_TP_VEICULO");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            Property(
                x => x.LfavPosit,
                map =>
                {
                    map.Column("LFAV_POSIT");
                    map.Unique(false);
                    map.NotNullable(false);
                });
             Property(
                x => x.LfaDescricaoAnex,
                map =>
                {
                    map.Column("LFAV_DESC_ANEX");
                    map.Unique(false);
                    map.NotNullable(false);
                });
             Property(
                x => x.LfavTimestamp,
                map =>
                {
                    map.Column("LFAV_TIMESTAMP");
                    map.Unique(false);
                    map.NotNullable(false);
                });
        }
    }
}