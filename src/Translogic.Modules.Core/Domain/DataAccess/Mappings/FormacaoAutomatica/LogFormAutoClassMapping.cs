﻿namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.AnexacaoDesanexacao
{
    using NHibernate.Mapping.ByCode;
    using NHibernate.Mapping.ByCode.Conformist;
    using NHibernate.Type;
    using Translogic.Modules.Core.Domain.Model.FormacaoAutomatica;

    public class LogFormAutoClassMapping : ClassMapping<LogFormacaoAuto>
    {
        public LogFormAutoClassMapping()
        {
            this.Lazy(true);
            this.Table("LOG_FORMACAO_AUTO");
            this.Schema("TRANSLOGIC");

            this.Id(
                x => x.Id,
                m =>
                {
                    m.Column("LFA_ID");
                    m.Generator(
                        Generators.Sequence,
                        g => g.Params(new
                        {
                            sequence = "LOG_FORMACAO_AUTO_SEQ_ID"
                        }));
                    m.Type(new Int32Type());
                    m.Access(Accessor.Property);
                });

            Property(
                x => x.OS,
                map =>
                {
                    map.Column("X1_NRO_OS");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            Property(
                x => x.LfaOrigem,
                map =>
                {
                    map.Column("LFA_ORI");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            Property(
                x => x.LfaDestino,
                map =>
                {
                    map.Column("LFA_DST");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            Property(
                x => x.LfaDescricaoOS,
                map =>
                {
                    map.Column("LFA_DESC_OS");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            Property(
                x => x.LfaDescricaoFTREM,
                map =>
                {
                    map.Column("LFA_DESC_FTREM");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            Property(
                x => x.LfaDescricaoLIB,
                map =>
                {
                    map.Column("LFA_DESC_LIB");
                    map.Unique(false);
                    map.NotNullable(false);
                });
            
            Property(
                x => x.LfaTentativas,
                map =>
                {
                    map.Column("TENTATIVAS");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            Property(
                x => x.LfaTimestamp,
                map =>
                {
                    map.Column("LFA_TIMESTAMP");
                    map.Unique(false);
                    map.NotNullable(false);
                });
        }
    }
}