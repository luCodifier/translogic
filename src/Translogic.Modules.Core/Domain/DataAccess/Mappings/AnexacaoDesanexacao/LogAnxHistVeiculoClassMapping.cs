﻿namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.AnexacaoDesanexacao
{
    using NHibernate.Mapping.ByCode;
    using NHibernate.Mapping.ByCode.Conformist;
    using Translogic.Modules.Core.Domain.Model.AnexacaoDesanexacao;
    using NHibernate.Type;

    public class LogAnxHistVeiculoClassMapping : ClassMapping<LogAnxHistVeiculo>
    {
        public LogAnxHistVeiculoClassMapping()
        {
            this.Lazy(true);
            this.Table("LOG_ANX_HIST_VEICULO");
            this.Schema("TRANSLOGIC");

            this.Id(
                x => x.Id,
                m =>
                {
                    m.Column("ID_LOG_ANX_HIST_VEICULO");
                    m.Generator(
                        Generators.Sequence,
                        g => g.Params(new
                        {
                            sequence = "LOG_ANX_HIST_VEICULO_SEQ"
                        }));
                    m.Type(new Int32Type());
                    m.Access(Accessor.Property);
                });

            Property(
                x => x.OS,
                map =>
                {
                    map.Column("OS");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            Property(
                x => x.DataAcao,
                map =>
                {
                    map.Column("DATA_ACAO");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            Property(
                x => x.NumeroVeiculo,
                map =>
                {
                    map.Column("NUMERO_VEICULO");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            Property(
                x => x.NumeroVeiculoOLD,
                map =>
                {
                    map.Column("NUMERO_VEICULO_OLD");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            Property(
                x => x.Local,
                map =>
                {
                    map.Column("LOCAL");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            Property(
                x => x.Acao,
                map =>
                {
                    map.Column("ACAO");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            Property(
                x => x.Processado,
                map =>
                {
                    map.Column("PROCESSADO");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            Property(
                x => x.Timestamp,
                map =>
                {
                    map.Column("TIMESTAMP");
                    map.Unique(false);
                    map.NotNullable(false);
                });
        }
    }
}