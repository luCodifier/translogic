﻿namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.AnexacaoDesanexacao
{
    using NHibernate.Mapping.ByCode;
    using NHibernate.Mapping.ByCode.Conformist;
    using Translogic.Modules.Core.Domain.Model.AnexacaoDesanexacao;
    using NHibernate.Type;

    public class LogAnxHistTremClassMapping : ClassMapping<LogAnxHistTrem>
    {
        public LogAnxHistTremClassMapping()
        {
            this.Lazy(true);
            this.Table("LOG_ANX_HIST_TREM");
            this.Schema("TRANSLOGIC");

            this.Id(
                x => x.Id,
                m =>
                {
                    m.Column("ID_LOG_ANX_HIST_TREM");
                    m.Generator(
                        Generators.Sequence,
                        g => g.Params(new
                        {
                            sequence = "LOG_ANX_HIST_TREM_SEQ"
                        }));
                    m.Type(new Int32Type());
                    m.Access(Accessor.Property);
                });

            Property(
                x => x.OS,
                map =>
                {
                    map.Column("OS");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            Property(
                x => x.DataAcao,
                map =>
                {
                    map.Column("DATA_ACAO");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            Property(
                x => x.Acao,
                map =>
                {
                    map.Column("ACAO");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            Property(
                x => x.Timestamp,
                map =>
                {
                    map.Column("TIMESTAMP");
                    map.Unique(false);
                    map.NotNullable(false);
                });
        }
    }
}