﻿namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.AnexacaoDesanexacao
{
    using NHibernate.Mapping.ByCode;
    using NHibernate.Mapping.ByCode.Conformist;
    using Translogic.Modules.Core.Domain.Model.AnexacaoDesanexacao;
    using NHibernate.Type;

    public class AnexacaoDesanexacaoClassMapping : ClassMapping<AnexacaoDesanexacaoAuto>
    {
        public AnexacaoDesanexacaoClassMapping()
        {
            this.Lazy(true);
            this.Table("LOG_ERRO_ANX_AUTO");
            this.Schema("TRANSLOGIC");

            this.Id(
                x => x.Id,
                m =>
                {
                    m.Column("LOG_ADX_ID");
                    m.Generator(
                        Generators.Sequence,
                        g => g.Params(new
                        {
                            sequence = "LOG_ERRO_ANX_AUTO_SEQ"
                        }));
                    m.Type(new Int32Type());
                    m.Access(Accessor.Property);
                });

            Property(
                x => x.OS,
                map =>
                {
                    map.Column("OS");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            Property(
                x => x.VagaoLoco,
                map =>
                {
                    map.Column("VAGAO_LOCO");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            Property(
                x => x.Estacao,
                map =>
                {
                    map.Column("ESTACAO");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            Property(
                x => x.Descricao,
                map =>
                {
                    map.Column("DESCRICAO");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            Property(
                x => x.Acao,
                map =>
                {
                    map.Column("ACAO");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            Property(
                x => x.XML,
                map =>
                {
                    map.Column("XML");
                    map.Unique(false);
                    map.NotNullable(false);
                });
            
            Property(
                x => x.Tentativas,
                map =>
                {
                    map.Column("TENTATIVAS");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            Property(
                x => x.Timestamp,
                map =>
                {
                    map.Column("TIMESTAMP");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            Property(
                x => x.Processado,
                map =>
                {
                    map.Column("PROCESSADO");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            Property(
                x => x.StatusParada,
                map =>
                {
                    map.Column("STATUS_PARADA");
                    map.Unique(false);
                    map.NotNullable(false);
                });

        }
    }
}