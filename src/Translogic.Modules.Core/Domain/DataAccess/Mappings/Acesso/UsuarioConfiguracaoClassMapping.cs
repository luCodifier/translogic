﻿namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.Acesso
{
	using NHibernate.Mapping.ByCode;
	using NHibernate.Mapping.ByCode.Conformist;
	using NHibernate.Type;

	using Translogic.Modules.Core.Domain.DataAccess.CustomType;
	using Translogic.Modules.Core.Domain.Model.Acesso;

	/// <summary>
	/// Mapeamento da classe <see cref="UsuarioConfiguracao"/>
	/// </summary>
	public class UsuarioConfiguracaoClassMapping : ClassMapping<UsuarioConfiguracao>
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="UsuarioConfiguracaoClassMapping"/> class.
		/// </summary>
		public UsuarioConfiguracaoClassMapping()
		{
			this.Lazy(true);
			this.Table("USUARIO_CONFIGURACOES");
			this.Schema("TRANSLOGIC");

			this.Id(
				x => x.Id,
				m =>
				{
					m.Column("USC_IDT_CONF");
					m.Generator(
						Generators.Sequence,
						g => g.Params(new
						{
							sequence = "USUARIO_CONFIGURACOES_SEQ_ID"
						}));
					m.Type(new Int32Type());
					m.Access(Accessor.Property);
				});

			this.ManyToOne(
					x => x.Usuario, m =>
					{
						m.Column("US_IDT_USU");
						m.Class(typeof(Usuario));
						m.Cascade(Cascade.All);
						m.Fetch(FetchKind.Select);
						m.Update(true);
						m.Insert(true);
						m.Access(Accessor.Property);
						m.Unique(false);
						m.Lazy(LazyRelation.Proxy);
					});

			this.Property(
				x => x.Chave,
				map => { map.Column("USC_CONF_CHAVE"); map.Type(typeof(UsuarioConfiguracaoEnumCustomType), null); map.Unique(false); map.NotNullable(true); });

			this.Property(
				x => x.Valor,
				map => { map.Column("USC_CONF_VALOR"); map.Length(50); map.Unique(false); map.NotNullable(true); });

			this.Property(
				x => x.DataCadastro,
				map => { map.Column("USC_TIMESTAMP"); map.Unique(false); map.NotNullable(false); });
		}
	}
}