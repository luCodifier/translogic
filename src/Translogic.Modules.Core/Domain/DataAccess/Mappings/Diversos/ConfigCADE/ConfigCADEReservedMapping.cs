﻿namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.Diversos.ConfigCADE
{
    using System;
    using Translogic.Core.Infrastructure.DataAccess;

    /// <summary>
    /// Registra classes para mapings da conexão do SADE
    /// </summary>
    public class ConfigCADEReservedMapping : IReservedMapping
    {
        public string[] Aliases
        {
            get { return new[] {"SADE"}; }
        }

        public Type[] GetReservedMappings()
        {
            return new []
            {
                typeof(TremTipoReservedClassMapping),
                typeof(AjusteTUMensalReservedMapping),
            };
        }
    }
}