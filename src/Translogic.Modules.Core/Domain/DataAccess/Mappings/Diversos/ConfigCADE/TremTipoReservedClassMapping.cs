﻿namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.Diversos.ConfigCADE
{
    using Model.Diversos.ConfigCADE;
    using NHibernate.Mapping.ByCode;
    using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Type;

    public class TremTipoReservedClassMapping : ClassMapping<TremTipo>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TremTipoReservedClassMapping"/> class.
        /// </summary>
        public TremTipoReservedClassMapping()
        {
            this.Lazy(false);
            this.Table("TBL_CADE_TREM_TIPO");
            this.Schema("dbo");
            this.Mutable(false);

            this.Id(
                x => x.Id,
                m =>
                {
                    m.Column("ID_TREM_TIPO");
                    m.Generator(Generators.Identity);
                    m.Type(new Int32Type());
                    m.Access(Accessor.Property);
                });

            this.Property(
                x => x.VersionDate,
                map =>
                {
                    map.Column("DT_INSERTED");
                    map.Unique(false);
                });

            this.Property(
                x => x.Ano,
                map =>
                {
                    map.Column("NU_ANO_REF");
                    map.Unique(false);
                });
            this.Property(
                x => x.Mes,
                map =>
                {
                    map.Column("NU_MES_REF");
                    map.Unique(false);
                });

            this.Property(
                x => x.Origem,
                map =>
                {
                    map.Column("CD_EST_ORI");
                    map.Unique(false);
                });
            this.Property(
                x => x.Destino,
                map =>
                {
                    map.Column("CD_EST_DST");
                    map.Unique(false);
                });
            this.Property(
                x => x.Segmento,
                map =>
                {
                    map.Column("TX_SEGMENTO");
                    map.Unique(false);
                });
            this.Property(
                x => x.Cliente,
                map =>
                {
                    map.Column("TX_CLI_COR");
                    map.Unique(false);
                });
            this.Property(
                x => x.QtdeVagoes,
                map =>
                {
                    map.Column("NU_QTD_VGS");
                    map.Unique(false);
                });
        }
    }
}