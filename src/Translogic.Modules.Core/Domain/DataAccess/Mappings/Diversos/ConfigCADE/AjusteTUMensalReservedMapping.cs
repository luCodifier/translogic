﻿namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.Diversos.ConfigCADE
{
    using Model.Diversos.ConfigCADE;
    using NHibernate.Mapping.ByCode;
    using NHibernate.Mapping.ByCode.Conformist;
    using NHibernate.Type;

    public class AjusteTUMensalReservedMapping : ClassMapping<AjusteTUMensal>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AjusteTUMensalReservedMapping"/> class.
        /// </summary>
        public AjusteTUMensalReservedMapping()
        {
            this.Lazy(false);
            this.Table("TBL_CADE_DEMANDA_MENSAL");
            this.Schema("dbo");
            this.Mutable(false);

            this.Id(
                x => x.Id,
                m =>
                {
                    m.Column("ID_DEMANDA");
                    m.Generator(Generators.Identity);
                    m.Type(new Int32Type());
                    m.Access(Accessor.Property);
                });

            this.Property(
                x => x.VersionDate,
                map =>
                {
                    map.Column("DT_INSERTED");
                    map.Unique(false);
                });

            this.Property(
                x => x.DataReferencia,
                map =>
                {
                    map.Column("DT_REF");
                    map.Unique(false);
                });

            this.Property(
                x => x.Fluxo,
                map =>
                {
                    map.Column("COD_FLUXO");
                    map.Unique(false);
                });
            this.Property(
                x => x.UP,
                map =>
                {
                    map.Column("CD_UN");
                    map.Unique(false);
                });

            this.Property(
                x => x.Origem,
                map =>
                {
                    map.Column("CD_EST_ORI");
                    map.Unique(false);
                });
            this.Property(
                x => x.Destino,
                map =>
                {
                    map.Column("CD_EST_DST");
                    map.Unique(false);
                });
            this.Property(
                x => x.Mercadoria,
                map =>
                {
                    map.Column("CD_MERCADORIA");
                    map.Unique(false);
                });
            this.Property(
                x => x.Segmento,
                map =>
                {
                    map.Column("TX_SEGMENTO");
                    map.Unique(false);
                });
            this.Property(
                x => x.CodClienteCorrentista,
                map =>
                {
                    map.Column("CD_CLI_COR");
                    map.Unique(false);
                });
            this.Property(
                x => x.ClienteCorrentista,
                map =>
                {
                    map.Column("TX_CLI_COR");
                    map.Unique(false);
                });
            this.Property(
                x => x.CodClienteRemetente,
                map =>
                {
                    map.Column("CD_CLI_REM");
                    map.Unique(false);
                });
            this.Property(
                x => x.ClienteRementente,
                map =>
                {
                    map.Column("TX_CLI_REM");
                    map.Unique(false);
                });
            this.Property(
                x => x.TUMes,
                map =>
                {
                    map.Column("NU_TU");
                    map.Unique(false);
                });
            this.Property(
                x => x.Carregamentos,
                map =>
                {
                    map.Column("NU_CGTO");
                    map.Unique(false);
                });
            this.Property(
                x => x.TKUMes,
                map =>
                {
                    map.Column("NU_TKU");
                    map.Unique(false);
                });
            this.Property(
                x => x.TUMesAjust,
                map =>
                {
                    map.Column("NU_TU_CADE");
                    map.Unique(false);
                });
            this.Property(
                x => x.CarregamentosAjust,
                map =>
                {
                    map.Column("NU_CGTO_CADE");
                    map.Unique(false);
                });
        }
    }
}