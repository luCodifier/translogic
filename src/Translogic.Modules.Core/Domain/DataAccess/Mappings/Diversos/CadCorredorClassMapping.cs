﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Type;
using Translogic.Modules.Core.Domain.Model.Diversos;
using Translogic.Modules.Core.Domain.Model.Via;

namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.Diversos
{
    /// <summary>
    ///     Mapeamento da classe <see cref="CadCorredor" />
    /// </summary>
    public class CadCorredorClassMapping : ClassMapping<CadCorredor>
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="CadCorredorClassMapping" /> class.
        /// </summary>
        public CadCorredorClassMapping()
        {
            Lazy(true);
            Table("CAD_CORREDOR");
            Schema("TRANSLOGIC");

            Id(
                x => x.Id,
                m =>
                {
                    m.Column("CC_ID_CC");
                    m.Generator(
                        Generators.Sequence,
                        g => g.Params(new
                        {
                            sequence = "CAD_CORREDOR_SEQ_ID"
                        }));
                    m.Type(new Int32Type());
                    m.Access(Accessor.Property);
                });

            Property(
                x => x.Descricao,
                map =>
                {
                    map.Column("CC_DSC_CC");
                    map.Unique(false);
                    map.NotNullable(true);
                });

            Property(
                x => x.Utilizador,
                map =>
                {
                    map.Column("US_USR_IDU");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            Property(
                x => x.IndicadorAtivo,
                map =>
                {
                    map.Column("CC_IND_ATIV");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            Property(
                x => x.Data,
                map =>
                {
                    map.Column("CC_TIMESTAMP");
                    map.Unique(false);
                    map.NotNullable(true);
                });

            Property(
                x => x.Identificador,
                map =>
                {
                    map.Column("IND");
                    map.Unique(false);
                    map.NotNullable(true);
                });

            Property(
                x => x.Ordem,
                map =>
                {
                    map.Column("ORDEM");
                    map.Unique(false);
                    map.NotNullable(true);
                });

            Property(
                x => x.IndicadorVagao,
                map =>
                {
                    map.Column("IND_VAGAO");
                    map.Unique(false);
                    map.NotNullable(true);
                });

            Property(
                x => x.IndicadorLoco,
                map =>
                {
                    map.Column("IND_LOCO");
                    map.Unique(false);
                    map.NotNullable(true);
                });

            Property(
                x => x.RegiaoMae,
                map =>
                {
                    map.Column("REGIAO_MAE");
                    map.Unique(false);
                    map.NotNullable(true);
                });

            ManyToOne(
                x => x.AreaOperacionalOrigem, m =>
                {
                    m.Column("AO_ID_ORIGEM");
                    m.Class(typeof (EstacaoMae));
                    m.Cascade(Cascade.All);
                    m.Fetch(FetchKind.Select);
                    m.Update(true);
                    m.Insert(true);
                    m.Access(Accessor.Property);
                    m.Unique(false);
                    m.Lazy(LazyRelation.Proxy);
                    m.NotNullable(false);
                });

            ManyToOne(
                x => x.AreaOperacionalDestino, m =>
                {
                    m.Column("AO_ID_DESTINO");
                    m.Class(typeof (EstacaoMae));
                    m.Cascade(Cascade.All);
                    m.Fetch(FetchKind.Select);
                    m.Update(true);
                    m.Insert(true);
                    m.Access(Accessor.Property);
                    m.Unique(false);
                    m.Lazy(LazyRelation.Proxy);
                    m.NotNullable(false);
                });
        }
    }
}