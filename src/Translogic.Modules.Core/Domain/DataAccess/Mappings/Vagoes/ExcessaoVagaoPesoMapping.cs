﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Type;
using Translogic.Modules.Core.Domain.Model.Vagoes;

namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.Vagoes
{
    public class ExcessaoVagaoPesoMapping : ClassMapping<ExcessaoVagaoPeso>
    {
        public ExcessaoVagaoPesoMapping()
        {
            Lazy(true);
            Table("EXCESSOES_VG_PESO");
            Schema("TRANSLOGIC");

            Id(
                x => x.Id,
                m =>
                {
                    m.Column("EP_ID_EP");
                    m.Generator(
                        Generators.Sequence,
                        g => g.Params(new
                        {
                            sequence = "EXCESSOES_VG_PESO_SEQ_ID"
                        }));
                    m.Type(new Int32Type());
                    m.Access(Accessor.Property);
                });

            Property(x => x.CodigoVagao, map => map.Column("VG_COD_VAG"));
            Property(x => x.Peso, map => map.Column("EP_NUM_PESO"));
            Property(x => x.Data, map => map.Column("EP_TIMESTAMP"));
        }
    }
}