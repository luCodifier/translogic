namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.Trem.Veiculo.Vagao
{
    using NHibernate.Mapping.ByCode;
    using NHibernate.Mapping.ByCode.Conformist;
    using NHibernate.Type;
    using Translogic.Modules.Core.Domain.Model.Acesso;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao;

    /// <summary>
    /// Arquivo de mapeamento da classe - <see cref="LimiteExcecaoVagao"/>
    /// </summary>
    public class LimiteExcecaoFrotaClassMapping : ClassMapping<LimiteExcecaoFrota>
    {
        /// <summary>
        /// Construtor mapeando os campos
        /// </summary>
        public LimiteExcecaoFrotaClassMapping()
        {
            this.Lazy(false);
            this.Table("LIMITE_EXCECAO_FROTA");
            this.Schema("TRANSLOGIC");
            this.Mutable(false);

            this.Id(
                x => x.Id,
                m =>
                {
                    m.Column("LEF_ID_LEF");
                    m.Generator(Generators.Sequence, g => g.Params(new { sequence = "LIMITE_EXCECAO_VAGAO_SEQ_ID" }));
                    m.Type(new Int32Type());
                    m.Access(Accessor.Property);
                });

            this.ManyToOne(
                x => x.Frota, m =>
            {
                m.Column("FV_ID_FT");
                m.Class(typeof(FrotaVagao));
                m.Cascade(Cascade.None);
                m.Fetch(FetchKind.Select);
                m.Access(Accessor.Property);
                m.Unique(true);
                m.Lazy(LazyRelation.Proxy);
            });

            this.ManyToOne(
               x => x.UsuarioCadastro, m =>
               {
                   m.Column("US_IDT_USU");
                   m.Class(typeof(Usuario));
                   m.Cascade(Cascade.None);
                   m.Fetch(FetchKind.Join);
                   m.Access(Accessor.Property);
                   m.Unique(false);
                   m.Lazy(LazyRelation.Proxy);
               });

            this.Property(
                x => x.DataInicial,
                map =>
                {
                    map.Column("LEF_DATA_INI");
                    map.Unique(false);
                    map.NotNullable(true);
                });

            this.Property(
                x => x.DataFinal,
                map =>
                {
                    map.Column("LEF_DATA_FIM");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            this.Property(
                x => x.PesoMaximo,
                map =>
                {
                    map.Column("LEF_LIMITE_MAXIMO");
                    map.Unique(false);
                    map.NotNullable(true);
                });
        }
    }
}