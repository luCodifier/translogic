namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.Trem.Veiculo.Locomotiva
{
    using System;
    using System.Linq.Expressions;
    using Model.QuadroEstados.Dimensoes;
    using Model.Trem.Veiculo.Vagao;
    using NHibernate.Mapping.ByCode;
	using NHibernate.Mapping.ByCode.Conformist;
	using NHibernate.Type;

	using Translogic.Modules.Core.Domain.DataAccess.CustomType;
	using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Locomotiva;

	/// <summary>
    /// Arquivo de mapeamento da classe - <see cref="EstadoVagao"/>
    /// </summary>
    public class EstadoVagaoClassMapping : ClassMapping<EstadoVagao>
    {
        /// <summary>
		/// Construtor mapeando os campos
		/// </summary>
        public EstadoVagaoClassMapping()
		{
			this.Lazy(false);
            this.Table("ESTADO_VAGAO_TAB");
			this.Schema("TRANSLOGIC");
            this.Mutable(false); 

			this.ManyToOne(
				x => x.Vagao, m =>
			{
                m.Column("VG_ID_VG");
				m.Class(typeof(Vagao));
				m.Cascade(Cascade.None);
				m.Fetch(FetchKind.Select);
				m.Access(Accessor.Property);
				m.Unique(true);
				m.Lazy(LazyRelation.Proxy);
			});

            this.__mapManyToOneWithouLazy(a => a.Localizacao, "LO_IDT_LCL");
            this.__mapManyToOneWithouLazy(a => a.LocalizacaoVagao, "LV_IDT_LCV");
            this.__mapManyToOneWithouLazy(a => a.Responsavel, "RP_IDT_RSP");
            this.__mapManyToOneWithouLazy(a => a.ResponsavelVagao, "RV_IDT_RPV");
            this.__mapManyToOneWithouLazy(a => a.Situacao, "SI_IDT_STC");
            this.__mapManyToOneWithouLazy(a => a.SituacaoVagao, "SC_IDT_STV");
            this.__mapManyToOneWithouLazy(a => a.Lotacao, "SH_IDT_STC");
            this.__mapManyToOneWithouLazy(a => a.LotacaoVagao, "SG_IDT_STC");
            this.__mapManyToOneWithouLazy(a => a.Intercambio, "TB_IDT_INT");
            this.__mapManyToOneWithouLazy(a => a.IntercambioVagao, "SD_IDT_STI");
            this.__mapManyToOneWithouLazy(a => a.CondicaoUso, "CD_IDT_CUS");
            this.__mapManyToOneWithouLazy(a => a.CondicaoUsoVagao, "CI_IDT_CDV");

            this.Property(a => a.CodigoVagao, a => a.Column("VG_COD_VAG"));
            this.Property(a => a.IdQuadroSituacao, a => a.Column("QS_IDT_QDS"));

            this.Property(a => a.DataLocalizacaoVagao, a => a.Column("LV_TIMESTAMP"));
            this.Property(a => a.DataResponsavelVagao, a => a.Column("RV_TIMESTAMP"));
            this.Property(a => a.DataSituacaoVagao, a => a.Column("SC_TIMESTAMP"));
            this.Property(a => a.DataLotacaoVagao, a => a.Column("SG_TIMESTAMP"));
            this.Property(a => a.DataIntercambioVagao, a => a.Column("SD_TIMESTAMP"));
            this.Property(a => a.DataCondicaoUsoVagao, a => a.Column("CI_TIMESTAMP"));
		}

        private void __mapManyToOneWithouLazy<TProperty>(Expression<Func<EstadoVagao, TProperty>> prop, string colName)
            where TProperty : class
        {
            this.ManyToOne(
                prop, m =>
                {
                    m.Column(colName);
                    m.Class(typeof(TProperty));
                    m.Cascade(Cascade.None);
                    m.Fetch(FetchKind.Select);
                    m.Access(Accessor.Property);
                    m.Unique(false);
                    m.Lazy(LazyRelation.Proxy);
                });
        }
    }
}