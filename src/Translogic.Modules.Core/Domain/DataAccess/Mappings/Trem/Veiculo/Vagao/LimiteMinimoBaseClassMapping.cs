namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.Trem.Veiculo.Vagao
{
    using NHibernate.Mapping.ByCode;
    using NHibernate.Mapping.ByCode.Conformist;
    using NHibernate.Type;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao;

    /// <summary>
    /// Arquivo de mapeamento da classe - <see cref="LimiteMinimoBase"/>
    /// </summary>
    public class LimiteMinimoBaseClassMapping : ClassMapping<LimiteMinimoBase>
    {
        /// <summary>
        /// Construtor mapeando os campos
        /// </summary>
        public LimiteMinimoBaseClassMapping()
        {
            this.Lazy(false);
            this.Table("LIMITE_MINIMO_BASE");
            this.Schema("TRANSLOGIC");
            this.Mutable(false);

            this.Id(
                x => x.Id,
                m =>
                {
                    m.Column("LM_ID_LM");
                    m.Generator(Generators.Sequence, g => g.Params(new { sequence = "LIMITE_MINIMO_BASE_SEQ_ID" }));
                    m.Type(new Int32Type());
                    m.Access(Accessor.Property);
                });

            this.ManyToOne(
                x => x.Mercadoria, m =>
            {
                m.Column("MC_ID_MC");
                m.Class(typeof(Mercadoria));
                m.Cascade(Cascade.None);
                m.Fetch(FetchKind.Select);
                m.Access(Accessor.Property);
                m.Unique(true);
                m.Lazy(LazyRelation.Proxy);
            });

            this.ManyToOne(
                x => x.SerieVagao, m =>
            {
                m.Column("SV_ID_SV");
                m.Class(typeof(SerieVagao));
                m.Cascade(Cascade.None);
                m.Fetch(FetchKind.Select);
                m.Access(Accessor.Property);
                m.Unique(true);
                m.Lazy(LazyRelation.Proxy);
            });

            this.Property(
                x => x.InicialVagao,
                map =>
                {
                    map.Column("INI_COD_VG");
                    map.Unique(false);
                    map.NotNullable(true);
                });

            this.Property(
                x => x.TuMedia,
                map =>
                {
                    map.Column("TU_MEDIA");
                    map.Unique(false);
                    map.NotNullable(true);
                });

            this.Property(
                x => x.CodigoSerieVagao,
                map =>
                {
                    map.Column("COD_SERIE");
                    map.Unique(false);
                    map.NotNullable(true);
                });

            this.Property(
                x => x.CodigoMercadoria,
                map =>
                {
                    map.Column("COD_MERCADORIA");
                    map.Unique(false);
                    map.NotNullable(true);
                });

            this.Property(
                x => x.MenorValorRota,
                map =>
                    {
                        map.Column("MENOR_VLR_ROTA");
                        map.Unique(false);
                        map.NotNullable(true);    
                    });
        }
    }
}