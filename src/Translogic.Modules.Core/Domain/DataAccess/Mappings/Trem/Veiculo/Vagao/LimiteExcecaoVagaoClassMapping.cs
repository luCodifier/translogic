namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.Trem.Veiculo.Vagao
{
    using NHibernate.Mapping.ByCode;
    using NHibernate.Mapping.ByCode.Conformist;
    using NHibernate.Type;
    using Translogic.Modules.Core.Domain.Model.Acesso;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao;

    /// <summary>
    /// Arquivo de mapeamento da classe - <see cref="LimiteExcecaoVagao"/>
    /// </summary>
    public class LimiteExcecaoVagaoClassMapping : ClassMapping<LimiteExcecaoVagao>
    {
        /// <summary>
        /// Construtor mapeando os campos
        /// </summary>
        public LimiteExcecaoVagaoClassMapping()
        {
            this.Lazy(false);
            this.Table("LIMITE_EXCECAO_VAGAO");
            this.Schema("TRANSLOGIC");
            this.Mutable(false);

            this.Id(
                x => x.Id,
                m =>
                {
                    m.Column("LEV_ID_LEV");
                    m.Generator(Generators.Sequence, g => g.Params(new { sequence = "LIMITE_EXCECAO_VAGAO_SEQ_ID" }));
                    m.Type(new Int32Type());
                    m.Access(Accessor.Property);
                });

            this.ManyToOne(
                x => x.LimiteMinimoBase, m =>
            {
                m.Column("LM_ID_LM");
                m.Class(typeof(LimiteMinimoBase));
                m.Cascade(Cascade.None);
                m.Fetch(FetchKind.Select);
                m.Access(Accessor.Property);
                m.Unique(true);
                m.Lazy(LazyRelation.Proxy);
            });

            this.ManyToOne(
                x => x.UsuarioCadastro, m =>
                {
                    m.Column("US_USR_IDU_CAD");
                    m.Class(typeof(Usuario));
                    m.Cascade(Cascade.None);
                    m.Fetch(FetchKind.Join);
                    m.Access(Accessor.Property);
                    m.Unique(false);
                    m.Lazy(LazyRelation.Proxy);
                });

            this.ManyToOne(
                x => x.Vagao, m =>
                {
                    m.Column("VG_ID_VG");
                    m.Class(typeof(Vagao));
                    m.Cascade(Cascade.None);
                    m.Fetch(FetchKind.Select);
                    m.Access(Accessor.Property);
                    m.Unique(false);
                    m.Lazy(LazyRelation.Proxy);
                });

            this.Property(
                x => x.DataInicial,
                map =>
                {
                    map.Column("LEV_DATA_INI");
                    map.Unique(false);
                    map.NotNullable(true);
                });

            this.Property(
                x => x.DataFinal,
                map =>
                {
                    map.Column("LEV_DATA_FIM");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            this.Property(
                x => x.PesoMinimo,
                map =>
                {
                    map.Column("LEV_MINIMO");
                    map.Unique(false);
                    map.NotNullable(false);
                });
        }
    }
}