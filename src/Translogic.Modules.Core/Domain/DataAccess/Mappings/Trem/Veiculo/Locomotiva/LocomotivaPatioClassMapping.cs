namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.Trem.Veiculo.Locomotiva
{
    using ALL.Core.AcessoDados.TiposCustomizados;
    using Model.Via;
    using NHibernate.Mapping.ByCode;
	using NHibernate.Mapping.ByCode.Conformist;
	using NHibernate.Type;

	using Translogic.Modules.Core.Domain.DataAccess.CustomType;
	using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Locomotiva;

	/// <summary>
    /// Arquivo de mapeamento da classe - <see cref="LocomotivaPatio"/>
    /// </summary>
    public class LocomotivaPatioClassMapping : ClassMapping<LocomotivaPatio>
    {
        /// <summary>
		/// Construtor mapeando os campos
		/// </summary>
        public LocomotivaPatioClassMapping()
		{
			this.Lazy(false);
            this.Table("LOCO_PATIO");
			this.Schema("TRANSLOGIC");

			this.Id(
				x => x.Id, 
				m =>
					{
                        m.Column("LP_IDT_PTL");
						m.Generator(
							Generators.Sequence, 
							g => g.Params(new
							{
                                sequence = "LOCO_PATIO_SEQ_ID"
							}));
						m.Type(new Int32Type());
						m.Access(Accessor.Property);
					});

            this.Property(
				x => x.DataCadastro,
                map => { map.Column("LP_TIMESTAMP"); map.Unique(false); map.NotNullable(false); });

			this.Property(
				x => x.DataChegadaPatio,
                map => { map.Column("LP_DAT_CHG"); map.Unique(false); map.NotNullable(false); });

			this.Property(
				x => x.DataSaidaPatio,
                map => { map.Column("LP_DAT_FRM"); map.Unique(false); map.NotNullable(false); });

			this.Property(
				x => x.NumeroSequenciaPatio,
                map => { map.Column("LP_NUM_SEQ"); map.Unique(false); map.NotNullable(false); });

            this.Property(
                x => x.CodigoLocomotiva,
                map => { map.Column("LP_COD_LOC"); map.Unique(false); map.NotNullable(false); });

            this.ManyToOne(
                x => x.Locomotiva, m =>
                {
                    m.Column("LC_ID_LC");
                    m.Class(typeof(Locomotiva));
                    m.Cascade(Cascade.None);
                    m.Fetch(FetchKind.Select);
                    m.Update(true);
                    m.Insert(true);
                    m.Access(Accessor.Property);
                    m.Unique(false);
                    m.Lazy(LazyRelation.Proxy);
                });

            this.ManyToOne(
                x => x.AreaOperacional, m =>
                {
                    m.Column("AO_ID_AO");
                    m.Class(typeof(IAreaOperacional));
                    m.Cascade(Cascade.None);
                    m.Fetch(FetchKind.Select);
                    m.Update(true);
                    m.Insert(true);
                    m.Access(Accessor.Property);
                    m.Unique(false);
                    m.Lazy(LazyRelation.Proxy);
                });

            this.ManyToOne(
                x => x.Trem, m =>
                {
                    m.Column("TR_ID_TRM");
                    m.Class(typeof(Model.Trem.Trem));
                    m.Cascade(Cascade.None);
                    m.Fetch(FetchKind.Select);
                    m.Update(true);
                    m.Insert(true);
                    m.Access(Accessor.Property);
                    m.Unique(false);
                    m.Lazy(LazyRelation.Proxy);
                });

            this.ManyToOne(
                x => x.ElementoVia, m =>
                {
                    m.Column("EV_ID_ELV");
                    m.Class(typeof(ElementoVia));
                    m.Cascade(Cascade.None);
                    m.Fetch(FetchKind.Select);
                    m.Update(true);
                    m.Insert(true);
                    m.Access(Accessor.Property);
                    m.Unique(false);
                    m.Lazy(LazyRelation.Proxy);
                });
		}
    }
}