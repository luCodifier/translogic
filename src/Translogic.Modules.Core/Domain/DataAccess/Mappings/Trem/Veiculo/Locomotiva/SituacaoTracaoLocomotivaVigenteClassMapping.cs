namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.Trem.Veiculo.Locomotiva
{
	using NHibernate.Mapping.ByCode;
	using NHibernate.Mapping.ByCode.Conformist;
	using NHibernate.Type;

	using Translogic.Modules.Core.Domain.DataAccess.CustomType;
	using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Locomotiva;

	/// <summary>
	/// Arquivo de mapeamento da classe - <see cref="SituacaoTracaoLocomotivaVigente"/>
    /// </summary>
    public class SituacaoTracaoLocomotivaVigenteClassMapping : ClassMapping<SituacaoTracaoLocomotivaVigente>
    {
        /// <summary>
		/// Construtor mapeando os campos
		/// </summary>
		public SituacaoTracaoLocomotivaVigenteClassMapping()
		{
			this.Lazy(false);
            this.Table("SITUACAO_TRACAO_VIG");
			this.Schema("TRANSLOGIC");

			this.Id(
				x => x.Id, 
				m =>
					{
                        m.Column("SA_IDT_STT");
						m.Generator(Generators.Assigned);
						m.Type(new Int32Type());
						m.Access(Accessor.Property);
					});

			this.ManyToOne(
				x => x.ComposicaoLocomotiva, m =>
			{
                m.Column("PL_IDT_PTL");
				m.Class(typeof(ComposicaoLocomotiva));
				m.Cascade(Cascade.None);
				m.Fetch(FetchKind.Select);
				m.Update(true);
				m.Insert(true);
				m.Access(Accessor.Property);
				m.Unique(false);
				m.Lazy(LazyRelation.Proxy);
			});

			this.Property(
				x => x.DataCadastro,
                map => { map.Column("SA_TIMESTAMP"); map.Unique(false); map.NotNullable(false); });

			this.Property(
				x => x.DataInicio,
				map => { map.Column("SA_DAT_INC"); map.Unique(false); map.NotNullable(false); });

			this.Property(
				x => x.DataFim,
				map => { map.Column("SA_DAT_FIM"); map.Unique(false); map.NotNullable(false); });

			this.Property(
				x => x.TipoSituacaoComandoLocomotiva,
				map => { map.Column("SA_IND_TPO"); map.Type(typeof(TipoSituacaoTracaoLocomotivaEnumCustomType), null); map.Unique(false); map.NotNullable(false); });
		}
    }
}