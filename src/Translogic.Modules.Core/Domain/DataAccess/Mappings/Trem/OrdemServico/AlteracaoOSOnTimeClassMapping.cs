﻿namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.Trem.OrdemServico
{
    using ALL.Core.AcessoDados.TiposCustomizados;
    using CustomType;
    using NHibernate.Mapping.ByCode;
    using NHibernate.Mapping.ByCode.Conformist;
    using NHibernate.Type;
    using Translogic.Modules.Core.Domain.Model.Trem.OrdemServico;

    /// <summary>
    /// Classe de mapeamento da entidade <see cref="AlteracaoOSOnTime"/>
	/// </summary>
	public class AlteracaoOSOnTimeClassMapping : ClassMapping<AlteracaoOSOnTime>
	{
		/// <summary>
        /// Initializes a new instance of the <see cref="AlteracaoOSOnTimeClassMapping"/> class.
		/// </summary>
        public AlteracaoOSOnTimeClassMapping()
		{
			this.Lazy(true);
            this.Table("ALTERACOES_ONTIME");
			this.Schema("TRANSLOGIC");
            
			this.Id(
				x => x.Id,
				m =>
				{
                    m.Column("AOT_ID_AONT");
                    m.Generator(Generators.Sequence, g => g.Params(new { sequence = "SEQ_ALTERACOES_ONTIME" }));
					m.Type(new Int32Type());
					m.Access(Accessor.Property);
				});

			this.Property(
				x => x.Data,
                map => map.Column("AOT_TIMESTAMP"));

            this.Property(
                x => x.Usuario,
                map => map.Column("AOT_COD_USUARIO"));

            this.Property(
                x => x.Tipo,
                m =>
                {
                    m.Column("AOT_ACAO_ALTER");
                    m.Type<EnumCharType<TipoAlteracaoOnTime>>();
                    m.Length(1);
                });

            this.Property(
                x => x.DataChegadaReal,
                map => map.Column("AOT_DTA_CHEGADA_REAL"));

            this.Property(
                x => x.DataPartidaReal,
                map => map.Column("AOT_DTA_PARTIDA_REAL"));

            this.Property(
                x => x.SituacaoTremPx,
                m =>
                {
                    m.Column("AOT_STT_TREM");
                    m.Type<SituacaoTremPxEnumCustomType>();
                    m.Length(3);
                });
            
            this.ManyToOne(
                x => x.SituacaoOS,
                m => 
                {
                    m.Column("AOT_STT_OS");
                    m.Class(typeof(SituacaoOrdemServico));
                    m.Cascade(Cascade.None);
                    m.Fetch(FetchKind.Join);
                    m.Update(true);
                    m.Insert(true);
                    m.Access(Accessor.Property);
                    m.Unique(false);
                    m.Lazy(LazyRelation.Proxy);
                });

            this.ManyToOne(
                x => x.OrdemServico,
                m =>
                {
                    m.Column("AOT_ID_OS");
                    m.Class(typeof(OrdemServico));
                    m.Cascade(Cascade.None);
                    m.Fetch(FetchKind.Join);
                    m.Update(true);
                    m.Insert(true);
                    m.Access(Accessor.Property);
                    m.Unique(false);
                    m.Lazy(LazyRelation.Proxy);
                });
		}
	}
}