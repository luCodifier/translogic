﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Type;
using Translogic.Modules.Core.Domain.Model.Bolar;

namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.Bolar
{
    public class BolarCteClassMapping : ClassMapping<BolarCte>
    {
        public BolarCteClassMapping()
        {
            this.Lazy(true);
            this.Table("BO_BOLAR_CTE");
            this.Schema("TRANSLOGIC");
 
            this.Id(
                x => x.Id,
                m =>
                {
                    m.Column("ID_CTE");
                    m.Generator(
                        Generators.Sequence,
                        g => g.Params(new
                        {
                            sequence = "BO_BOLAR_CTE_SEQ_ID"
                        }));
                    m.Type(new Int32Type());
                    m.Access(Accessor.Property);
                });

            this.Property(
                x => x.CnpjEmit,
                map => { map.Column("CNPJ_EMIT"); map.Unique(false); map.NotNullable(false); });

            this.Property(
                x => x.UfEmit,
                map => { map.Column("UF_EMIT"); map.Length(2); map.Unique(false); map.NotNullable(false); });

            this.Property(
                x => x.TipoServico,
                map => { map.Column("TIPO_SERVICO_CTE"); map.Length(2); map.Unique(false); map.NotNullable(false); });

            this.Property(
                x => x.ChaveCte,
                map => { map.Column("CHAVE_CTE"); map.Length(44); map.Unique(false); map.NotNullable(false); });

            this.Property(
                x => x.CnpjRem,
                map => { map.Column("CNPJ_REM"); map.Unique(false); map.NotNullable(false); });

            this.Property(
                x => x.CnpjDest,
                map => { map.Column("CNPJ_DEST"); map.Unique(false); map.NotNullable(false); });

            this.Property(
                x => x.PesoCte,
                map => { map.Column("PESO_CTE"); map.Unique(false); map.NotNullable(false); });
        }
    }
}