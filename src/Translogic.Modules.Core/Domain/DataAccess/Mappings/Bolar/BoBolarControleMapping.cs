﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Type;
using Translogic.Modules.Core.Domain.Model.Bolar;

namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.Bolar
{
    public class BoBolarControleMapping : ClassMapping<BoBolarControle>
    {
        public BoBolarControleMapping()
        {
            Table("BO_BOLAR_CONTROLE");
            Schema("TRANSLOGIC");

            Id(
                x => x.Id,
                m =>
                {
                    m.Column("BC_ID_BC");
                    m.Generator(
                        Generators.Sequence,
                        g => g.Params(new
                        {
                            sequence = "BO_BOLAR_CONTROLE_SEQ_ID"
                        }));
                    m.Type(new Int32Type());
                    m.Access(Accessor.Property);
                });

            Property(
                x => x.BoIdBo,
                map => map.Column("BO_ID_BO"));

            Property(
                x => x.NumTentativas,
                map => map.Column("NUM_TENTATIVAS"));

            Property(
               x => x.BcIndLibTela,
               map => map.Column("BC_IND_LIB_TELA"));

            Property(
               x => x.Data,
               map => map.Column("BC_TIMESTAMP"));
            
        }
        
    }
}