﻿namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.Bolar
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using NHibernate.Mapping.ByCode;
    using NHibernate.Mapping.ByCode.Conformist;
    using Translogic.Modules.Core.Domain.Model.Bolar;
    using NHibernate.Type;

    public class BoBolarBolarCteClassMapping : ClassMapping<BolarBoBolarCte>
    {
        public BoBolarBolarCteClassMapping()
        {
            this.Lazy(true);
            this.Table("BO_BOLAR_BOLARCTE");
            this.Schema("TRANSLOGIC");

            this.Id(
                x => x.Id,
                m =>
                {
                    m.Column("ID_BO_CTE");
                    m.Generator(
                        Generators.Sequence,
                        g => g.Params(new
                        {
                            sequence = "BO_BOLAR_BOLARCTE_SEQ_ID"
                        }));
                    m.Type(new Int32Type());
                    m.Access(Accessor.Property);
                });


            this.ManyToOne(
                x => x.BolarBo,
                m =>
                {
                    m.Column("ID_BO_ID");
                    m.Class(typeof(BolarBo));
                    m.Cascade(Cascade.All);
                    m.Fetch(FetchKind.Select);
                    m.Update(true);
                    m.Insert(true);
                    m.Access(Accessor.Property);
                    m.Unique(false);
                    m.Lazy(LazyRelation.Proxy);
                });


            this.ManyToOne(
                x => x.BolarCte,
                m =>
                {
                    m.Column("ID_CTE_ID");
                    m.Class(typeof(BolarCte));
                    m.Cascade(Cascade.All);
                    m.Fetch(FetchKind.Select);
                    m.Update(true);
                    m.Insert(true);
                    m.Access(Accessor.Property);
                    m.Unique(false);
                    m.Lazy(LazyRelation.Proxy);
                });
        }
    }
}