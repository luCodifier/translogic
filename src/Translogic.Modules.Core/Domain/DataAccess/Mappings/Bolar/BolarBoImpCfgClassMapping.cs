﻿namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.Bolar
{
	using ALL.Core.AcessoDados.TiposCustomizados;

	using NHibernate.Mapping.ByCode;
	using NHibernate.Mapping.ByCode.Conformist;
	using NHibernate.Type;

	using Translogic.Modules.Core.Domain.DataAccess.CustomType;
	using Translogic.Modules.Core.Domain.DataAccess.Mappings.Acesso;
	using Translogic.Modules.Core.Domain.Model.Acesso;
	using Translogic.Modules.Core.Domain.Model.Bolar;

	/// <summary>
	/// Mapeamento da classe <see cref="BolarBoImpCfg"/>
	/// </summary>
	public class BolarBoImpCfgClassMapping : ClassMapping<BolarBoImpCfg>
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="BolarBoImpCfgClassMapping"/> class.
		/// </summary>
		public BolarBoImpCfgClassMapping()
		{
			this.Lazy(true);
			this.Table("BO_BOLAR_IMP_CFG");
			this.Schema("TRANSLOGIC");

			this.Id(
				x => x.Id,
				m =>
				{
					m.Column("ID_BO_BOLAR_IMP_CFG");
					m.Generator(
						Generators.Sequence,
						g => g.Params(new
						{
							sequence = "BO_BOLAR_IMP_CFG_SEQ_ID"
						}));
					m.Type(new Int32Type());
					m.Access(Accessor.Property);
				});

			this.Property(
				x => x.Cnpj,
				map => { map.Column("CNPJ"); map.Length(14);  map.Unique(false); map.NotNullable(false); });

			this.Property(
				x => x.CodigoEmpresa,
				map => { map.Column("CLIENTE"); map.Length(3); map.Unique(true); map.NotNullable(true); });

			this.Property(
				x => x.DescricaoEmpresa,
				map => { map.Column("CLIENTE_DSC"); map.Length(50); map.Unique(false); map.NotNullable(true); });

			this.Property(
				x => x.LocalArquivo,
				map => { map.Column("LOCAL_ARQ"); map.Length(500); map.Unique(false); map.NotNullable(true); });

			this.Property(
				x => x.LocalArquivoLog,
				map => { map.Column("LOCAL_ARQ_LOG"); map.Length(500); map.Unique(false); map.NotNullable(true); });

			this.Property(
				x => x.IndAtivo,
				map => { map.Column("IC_ATIVO"); map.Type(typeof(BooleanCharType), null); map.Unique(false); map.NotNullable(true); });

			this.Property(
				x => x.LocalArquivoErro,
				map => { map.Column("LOCAL_ARQ_ERR"); map.Length(500); map.Unique(false); map.NotNullable(true); });

			this.Property(
				x => x.IndIntercambio,
				map => { map.Column("IND_INTERCAMBIO"); map.Type(typeof(BooleanCharType), null); map.Unique(false); map.NotNullable(false); });
		}
	}
}