﻿namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.FluxosComerciais.Pedidos.Despachos
{
	using ALL.Core.AcessoDados.TiposCustomizados;
	using Model.FluxosComerciais.Pedidos.Despachos;
	using NHibernate.Mapping.ByCode;
	using NHibernate.Mapping.ByCode.Conformist;
	using NHibernate.Type;

	/// <summary>
    /// Classe de mapeamento da entidade <see cref="PesagemEstaticaFerro"/>
	/// </summary>
	public class PesagemEstaticaFerroClassMapping : ClassMapping<PesagemEstaticaFerro>
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="PesagemEstaticaFerroClassMapping"/> class.
		/// </summary>
        public PesagemEstaticaFerroClassMapping()
		{
			this.Lazy(true);
			this.Table("PESAGEM_ESTATICA_FERRO");
			this.Schema("TRANSLOGIC");
            this.Mutable(false);

			this.Id(
				x => x.Id,
                m => m.Column("PF_IDT_PF"));

            this.Id(
                x => x.CodigoBalanca,
                m => m.Column("PF_COD_BALANCA"));

            this.Id(
                x => x.CodigoVagao,
                m => m.Column("PF_COD_VAGAO"));

			this.Property(
				x => x.DataCadastro,
                map => map.Column("PF_TIMESTAMP"));

            this.Property(
                x => x.DataPesagem,
                map => map.Column("PF_DT_PESAGEM"));

			this.Property(
				x => x.TaraDoVagao,
				map => map.Column("PF_TARA"));

			this.Property(
				x => x.PesoBruto,
				map => map.Column("PF_PESO_BRUTO"));
		}
	}
}