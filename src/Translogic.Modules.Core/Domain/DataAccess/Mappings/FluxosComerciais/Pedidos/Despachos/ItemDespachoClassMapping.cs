﻿namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.FluxosComerciais.Pedidos.Despachos
{
    using ALL.Core.AcessoDados.TiposCustomizados;

    using NHibernate.Mapping.ByCode;
    using NHibernate.Mapping.ByCode.Conformist;
    using NHibernate.Type;

    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos;
    using Translogic.Modules.Core.Domain.Model.Intercambios;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao;

    /// <summary>
    /// Classe de mapeamento da entidade <see cref="ItemDespacho"/>
    /// </summary>
    public class ItemDespachoClassMapping : ClassMapping<ItemDespacho>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ItemDespachoClassMapping"/> class.
        /// </summary>
        public ItemDespachoClassMapping()
        {
            this.Lazy(true);
            this.Table("ITEM_DESPACHO");
            this.Schema("TRANSLOGIC");

            this.Id(
                x => x.Id,
                m =>
                {
                    m.Column("ID_ID_ITD");
                    m.Generator(Generators.Sequence, g => g.Params(new { sequence = "ITEM_DESPACHO_SEQ_ID" }));
                    m.Type(new Int32Type());
                    m.Access(Accessor.Property);
                });

            this.Property(
                x => x.DtCadastro,
                map =>
                {
                    map.Column("ID_TIMESTAMP");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            this.ManyToOne(
                x => x.DetalheCarregamento, m =>
                {
                    m.Column("DC_ID_CRG");
                    m.Class(typeof(DetalheCarregamento));
                    m.Cascade(Cascade.None);
                    m.Fetch(FetchKind.Join);
                    m.Update(true);
                    m.Insert(true);
                    m.Access(Accessor.Property);
                    m.Unique(false);
                    m.Lazy(LazyRelation.Proxy);
                });

            this.ManyToOne(
                x => x.DespachoTranslogic, m =>
                {
                    m.Column("DP_ID_DP");
                    m.Class(typeof(DespachoTranslogic));
                    m.Cascade(Cascade.None);
                    m.Fetch(FetchKind.Join);
                    m.Update(true);
                    m.Insert(true);
                    m.Access(Accessor.Property);
                    m.Unique(false);
                    m.Lazy(LazyRelation.Proxy);
                });

            this.ManyToOne(
                x => x.DetalheIntercambio, m =>
                {
                    m.Column("DP_ID_DP");
                    m.Class(typeof(DetalheIntercambio));
                    m.Cascade(Cascade.None);
                    m.Fetch(FetchKind.Join);
                    m.Update(true);
                    m.Insert(true);
                    m.Access(Accessor.Property);
                    m.Unique(false);
                    m.Lazy(LazyRelation.Proxy);
                });

            this.ManyToOne(
                x => x.Vagao, m =>
                {
                    m.Column("VG_ID_VG");
                    m.Class(typeof(Vagao));
                    m.Cascade(Cascade.None);
                    m.Fetch(FetchKind.Join);
                    m.Update(true);
                    m.Insert(true);
                    m.Access(Accessor.Property);
                    m.Unique(false);
                    m.Lazy(LazyRelation.Proxy);
                });

            this.Property(
                x => x.IdNotaFiscal,
                map =>
                {
                    map.Column("NF_IDT_NFL");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            this.Property(
                x => x.PesoLiquido,
                map =>
                {
                    map.Column("ID_NUM_PRL");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            this.Property(
                x => x.PesoCalculo,
                map =>
                {
                    map.Column("ID_NUM_PCL");
                    map.Unique(false);
                    map.NotNullable(false);
                    map.Formula("TRUNC(ID_NUM_PCL, 3)");
                });

            this.Property(
                x => x.PesoToneladaUtil,
                map =>
                {
                    map.Column("ID_NUM_PDT");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            this.Property(
                 x => x.IndBaldeio,
                 map =>
                 {
                     map.Column("ID_IND_BDO");
                     map.Unique(false);
                     map.NotNullable(false);
                 });

            this.Property(
                  x => x.IndVagaoDescarregado,
                  map => { map.Column("ID_IND_VGD"); map.Type(typeof(BooleanCharType), null); map.Unique(false); map.NotNullable(false); });

            this.Property(
                  x => x.IndForaDeUso,
                  map => { map.Column("ID_IND_VFU"); map.Type(typeof(BooleanCharType), null); map.Unique(false); map.NotNullable(false); });

            this.Property(
                x => x.Volume,
                map =>
                {
                    map.Column("ID_NUN_VOL");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            this.Property(
                x => x.PesoTotalVagao,
                map =>
                {
                    map.Column("PESO_TOTAL_VG");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            this.Property(
                  x => x.IndPreFaturamento,
                  map => { map.Column("IND_PRE_FAT"); map.Type(typeof(BooleanCharType), null); map.Unique(false); map.NotNullable(false); });

            this.Property(
                  x => x.IndManutencao,
                  map => { map.Column("IND_MANUTENCAO"); map.Type(typeof(BooleanCharType), null); map.Unique(false); map.NotNullable(false); });

            this.Property(
                x => x.NumeroConteiner,
                map =>
                {
                    map.Column("ID_NUM_CONTEINERS");
                    map.Unique(false);
                    map.NotNullable(false);
                });
            this.Property(
                x => x.DetalheCarregamentoOperacional,
                map =>
                {
                    map.Column("DC_ID_CRG_OPER");
                    map.Unique(false);
                    map.NotNullable(false);
                });
        }
    }
}