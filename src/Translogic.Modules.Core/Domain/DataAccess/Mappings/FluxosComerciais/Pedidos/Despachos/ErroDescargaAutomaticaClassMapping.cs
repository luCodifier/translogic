﻿namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.FluxosComerciais.Pedidos.Despachos
{
    using Model.Acesso;
	using Model.FluxosComerciais.Pedidos;
	using NHibernate.Mapping.ByCode;
	using NHibernate.Mapping.ByCode.Conformist;
	using NHibernate.Type;

	using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos;

    /// <summary>
	/// Classe de mapeamento da entidade <see cref="ErroDescargaAutomatica"/>
	/// </summary>
	public class ErroDescargaAutomaticaClassMapping : ClassMapping<ErroDescargaAutomatica>
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="ErroDescargaAutomaticaClassMapping"/> class.
		/// </summary>
        public ErroDescargaAutomaticaClassMapping()
		{
			this.Lazy(true);
            this.Table("ERRO_DESCARGA_AUT");
			this.Schema("TRANSLOGIC");

			this.Id(
				x => x.Id,
				m =>
				{
                    m.Column("ED_ID_EDA");
                    m.Generator(Generators.Sequence, g => g.Params(new { sequence = "SEQ_ERRO_DESCARGA_AUT" }));
					m.Type(new Int32Type());
					m.Access(Accessor.Property);
				});

			this.Property(
				x => x.Data,
				map =>
				{
                    map.Column("ED_DTA_ERRO");
					map.Unique(false);
					map.NotNullable(true);
                });

            this.Property(
                x => x.XMLRetorno,
                map =>
                {
                    map.Column("ED_XML_ERRO");
                    map.Unique(false);
                    map.NotNullable(true);
                    map.Length(4000);
                });

            this.Property(
                x => x.Descricao,
                map =>
                {
                    map.Column("ED_DSC_ERRO");
                    map.Unique(false);
                    map.NotNullable(true);
                    map.Length(4000);
                });

			this.ManyToOne(
				x => x.VagaoPedido, m =>
				{
                    m.Column("VB_IDT_VPT");
					m.Class(typeof(VagaoPedido));
					m.Cascade(Cascade.None);
					m.Fetch(FetchKind.Join);
					m.Update(true);
					m.Insert(true);
					m.Access(Accessor.Property);
					m.Unique(false);
					m.Lazy(LazyRelation.Proxy);
				});

			this.ManyToOne(
				x => x.Usuario, m =>
				{
                    m.Column("ED_USU_ERRO");
					m.Class(typeof(Usuario));
					m.Cascade(Cascade.None);
					m.Fetch(FetchKind.Join);
					m.Update(true);
					m.Insert(true);
					m.Access(Accessor.Property);
					m.Unique(false);
					m.Lazy(LazyRelation.Proxy);
				});
		}
	}
}