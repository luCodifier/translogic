﻿namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.FluxosComerciais.Pedidos.Despachos
{
	using Model.FluxosComerciais.Pedidos.Despachos;
	using Model.Trem.Veiculo.Vagao;
	using NHibernate.Mapping.ByCode;
	using NHibernate.Mapping.ByCode.Conformist;
	using NHibernate.Type;

	/// <summary>
    /// Classe de mapeamento da entidade <see cref="DetalheDescarregamento"/>
	/// </summary>
	public class DetalheDescarregamentoClassMapping : ClassMapping<DetalheDescarregamento>
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="DetalheDescarregamentoClassMapping"/> class.
		/// </summary>
        public DetalheDescarregamentoClassMapping()
		{
			this.Lazy(true);
			this.Table("DETALHE_DESCARREGAMENTO");
			this.Schema("TRANSLOGIC");
            this.Mutable(false);
            
			this.Id(
				x => x.Id,
				m =>
				{
					m.Column("DD_IDT_DDS");
					m.Generator(Generators.Sequence, g => g.Params(new { sequence = "DETALHE_DESCARREGAMENTO_SEQ_ID" }));
					m.Type(new Int32Type());
					m.Access(Accessor.Property);
				});

			this.Property(
				x => x.DtCadastro,
                map => map.Column("DD_TIMESTAMP"));

            this.Property(
                x => x.PesoAntes,
                map => map.Column("DD_QTD_ANT"));

            this.Property(
                x => x.PesoDepois,
                map => map.Column("DD_QTD_DPS"));
            
            this.Property(
                x => x.PesoDescarregado,
                map => map.Column("DD_QTD_PAD"));

            this.Property(
                x => x.VolumeDescarregado,
                map => map.Column("DD_NUM_VOL"));
            
            this.ManyToOne(
                x => x.VagaoIndicado, m =>
                {
                    m.Column("VG_ID_VG");
                    m.Class(typeof(Vagao));
                    m.Cascade(Cascade.None);
                    m.Fetch(FetchKind.Join);
                    m.Update(true);
                    m.Insert(true);
                    m.Access(Accessor.Property);
                    m.Unique(false);
                    m.Lazy(LazyRelation.Proxy);
                });
            
            this.ManyToOne(
                x => x.DetalheCarregamento, m =>
                {
                    m.Column("DC_ID_CRG");
                    m.Class(typeof(DetalheCarregamento));
                    m.Cascade(Cascade.None);
                    m.Fetch(FetchKind.Join);
                    m.Update(true);
                    m.Insert(true);
                    m.Access(Accessor.Property);
                    m.Unique(false);
                    m.Lazy(LazyRelation.Proxy);
                });

            this.ManyToOne(
                x => x.Descarregamento, m =>
                {
                    m.Column("DS_ID_DSC");
                    m.Class(typeof(Descarregamento));
                    m.Cascade(Cascade.None);
                    m.Fetch(FetchKind.Join);
                    m.Update(true);
                    m.Insert(true);
                    m.Access(Accessor.Property);
                    m.Unique(false);
                    m.Lazy(LazyRelation.Proxy);
                });
		}
	}
}