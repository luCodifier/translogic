﻿namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.FluxosComerciais.Atrasos
{
    using DispVia.Domain.Model.Constants;
    using Model.FluxosComerciais.Atrasos;
    using Model.FluxosComerciais.ConhecimentoTransporteEletronico;
    using NHibernate.Mapping.ByCode;
    using NHibernate.Mapping.ByCode.Conformist;
    using NHibernate.SqlTypes;
    using NHibernate.Type;
    using ALL.Core.AcessoDados.TiposCustomizados;

    /// <summary>
    /// Classe de mapeamento da entidade 
    /// </summary>
    public class MotivoAtrasoClassMapping : ClassMapping<MotivoAtraso>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MotivoAtrasoClassMapping"/> class.
        /// </summary>
        public MotivoAtrasoClassMapping()
        {
            this.Lazy(false);
            this.Table("MOTIVO_AVISO_ATRASO");
            this.Schema("TRANSLOGIC");
            this.Mutable(false);

            this.Id(
              x => x.Id,
              m =>
              {
                  m.Column("MATR_ID_MATR");
                  m.Access(Accessor.Property);
              });
            
            this.Property(
                 x => x.Nome,
                 map =>
                 {
                     map.Column("MATR_NOME");
                     map.Unique(false);
                 });

            this.Property(
                 x => x.Descricao,
                 map =>
                 {
                     map.Column("MATR_DESCRICAO");
                     map.Unique(false);
                 });

            this.Property(
                 x => x.Ativo,
                 map =>
                 {
                     map.Column("MATR_ATIVO");
                     map.Type<BooleanCharType>();
                     map.Unique(false);
                 });

            this.Property(
                x => x.VersionDate,
                map =>
                {
                    map.Column("MATR_DTVERSION");
                    map.Unique(false);
                });
        }
    }
}