﻿namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.FluxosComerciais.Atrasos
{
    using Model.Acesso;
    using Model.Estrutura;
    using Model.FluxosComerciais;
    using Model.FluxosComerciais.Atrasos;
    using Model.Via;
    using NHibernate.Mapping.ByCode;
    using NHibernate.Mapping.ByCode.Conformist;
    using NHibernate.Type;

    /// <summary>
    /// Classe de mapeamento da entidade 
    /// </summary>
    public class AvisoAtrasoClassMapping : ClassMapping<AvisoAtraso>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AvisoAtrasoClassMapping"/> class.
        /// </summary>
        public AvisoAtrasoClassMapping()
        {
            this.Lazy(false);
            this.Table("AVISO_ATRASO");
            this.Schema("TRANSLOGIC");
            this.Mutable(true);

            this.Id(
              x => x.Id,
              m =>
              {
                  m.Column("AATR_ID_AATR");
                  m.Generator(Generators.Sequence, g => g.Params(new { sequence = "AVISO_ATRASO_SEQ_ID" }));
                  m.Type(new Int32Type());
                  m.Access(Accessor.Property);
              });

            this.ManyToOne(
              x => x.Tipo, u =>
              {
                  u.Column("TATR_ID_TATR");
                  u.Class(typeof(TipoAtraso));
                  u.Cascade(Cascade.None);
                  u.Fetch(FetchKind.Join);
                  u.Access(Accessor.Property);
                  u.Unique(false);
                  u.Lazy(LazyRelation.Proxy);
              });
            this.ManyToOne(
              x => x.Responsavel, u =>
              {
                  u.Column("RATR_ID_RATR");
                  u.Class(typeof(ResponsavelAtraso));
                  u.Cascade(Cascade.None);
                  u.Fetch(FetchKind.Join);
                  u.Access(Accessor.Property);
                  u.Unique(false);
                  u.Lazy(LazyRelation.Proxy);
              });
            this.ManyToOne(
              x => x.Motivo, u =>
              {
                  u.Column("MATR_ID_MATR");
                  u.Class(typeof(MotivoAtraso));
                  u.Cascade(Cascade.None);
                  u.Fetch(FetchKind.Join);
                  u.Access(Accessor.Property);
                  u.Unique(false);
                  u.Lazy(LazyRelation.Proxy);
              });
            this.ManyToOne(
              x => x.Estacao, u =>
              {
                  u.Column("AO_ID_AO");
                  u.Class(typeof(EstacaoMae));
                  u.Cascade(Cascade.None);
                  u.Fetch(FetchKind.Join);
                  u.Access(Accessor.Property);
                  u.Unique(false);
                  u.Lazy(LazyRelation.Proxy);
              });
            this.ManyToOne(
              x => x.Usuario, u =>
              {
                  u.Column("US_IDT_USU");
                  u.Class(typeof(Usuario));
                  u.Cascade(Cascade.None);
                  u.Fetch(FetchKind.Join);
                  u.Access(Accessor.Property);
                  u.Unique(false);
                  u.Lazy(LazyRelation.Proxy);
              });

            this.Bag(
                x => x.Vagoes, 
                collectionMapping =>
                {
                    collectionMapping.Table("AVISO_ATRASO_VGS");
                    collectionMapping.Cascade(Cascade.None);
                    collectionMapping.Key(k => k.Column("AATR_ID_AATR"));
                },
                map => map.ManyToMany(p => p.Column("VG_ID_VG"))
            );

            this.Property(
                 x => x.Data,
                 map =>
                 {
                     map.Column("AATR_DATA");
                     map.Unique(false);
                 });
            this.Property(
                 x => x.DescResumida, map =>
                 {
                     map.Column("AATR_CLIENTE");
                     map.Unique(false);
                 });
            this.Property(
                 x => x.TU,
                 map =>
                 {
                     map.Column("AATR_TU");
                     map.Unique(false);
                 });
            this.Property(
                 x => x.QtdeVagoes,
                 map =>
                 {
                     map.Column("AATR_QTDE_VGS");
                     map.Unique(false);
                 });
            this.Property(
                 x => x.VagoesDigitados,
                 map =>
                 {
                     map.Column("AATR_VAGOES");
                     map.Unique(false);
                 });
            this.Property(
                 x => x.HorasAtraso,
                 map =>
                 {
                     map.Column("AATR_HRS_ATRASO");
                     map.Unique(false);
                 });
            this.Property(
                 x => x.Observacao,
                 map =>
                 {
                     map.Column("AATR_OBS");
                     map.Unique(false);
                 });
            this.Property(
                 x => x.VersionDate,
                 map =>
                 {
                     map.Column("AATR_DTVERSION");
                     map.Unique(false);
                 });
            this.ManyToOne(
              x => x.Cliente, u =>
              {
                  u.Column("EP_ID_EMP");
                  u.Class(typeof(IEmpresa));
                  u.Cascade(Cascade.None);
                  u.Fetch(FetchKind.Join);
                  u.Access(Accessor.Property);
                  u.Unique(false);
                  u.Lazy(LazyRelation.Proxy);
              });
            this.ManyToOne(
              x => x.Mercadoria, u =>
              {
                  u.Column("MC_ID_MRC");
                  u.Class(typeof(Mercadoria));
                  u.Cascade(Cascade.None);
                  u.Fetch(FetchKind.Join);
                  u.Access(Accessor.Property);
                  u.Unique(false);
                  u.Lazy(LazyRelation.Proxy);
              });
        }
    }
}