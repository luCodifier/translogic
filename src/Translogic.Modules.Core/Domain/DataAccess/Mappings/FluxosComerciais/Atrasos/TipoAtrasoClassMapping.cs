﻿namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.FluxosComerciais.Atrasos
{
    using DispVia.Domain.Model.Constants;
    using Model.FluxosComerciais.Atrasos;
    using Model.FluxosComerciais.ConhecimentoTransporteEletronico;
    using NHibernate.Mapping.ByCode;
    using NHibernate.Mapping.ByCode.Conformist;
    using NHibernate.SqlTypes;
    using NHibernate.Type;
    using ALL.Core.AcessoDados.TiposCustomizados;

    /// <summary>
    /// Classe de mapeamento da entidade 
    /// </summary>
    public class TipoAtrasoClassMapping : ClassMapping<TipoAtraso>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TipoAtrasoClassMapping"/> class.
        /// </summary>
        public TipoAtrasoClassMapping()
        {
            this.Lazy(false);
            this.Table("TIPO_ATRASO");
            this.Schema("TRANSLOGIC");
            this.Mutable(false);

            this.Id(
              x => x.Id,
              m =>
              {
                  m.Column("TATR_ID_TATR");
                  m.Access(Accessor.Property);
              });
            
            this.Property(
                 x => x.Nome,
                 map =>
                 {
                     map.Column("TATR_NOME");
                     map.Unique(false);
                 });

            this.Property(
                 x => x.Descricao,
                 map =>
                 {
                     map.Column("TATR_DESCRICAO");
                     map.Unique(false);
                 });

            this.Property(
                 x => x.Ativo,
                 map =>
                 {
                     map.Column("TATR_ATIVO");
                     map.Type<BooleanCharType>();
                     map.Unique(false);
                 });

            this.Property(
                x => x.VersionDate,
                map =>
                {
                    map.Column("TATR_DTVERSION");
                    map.Unique(false);
                });
        }
    }
}