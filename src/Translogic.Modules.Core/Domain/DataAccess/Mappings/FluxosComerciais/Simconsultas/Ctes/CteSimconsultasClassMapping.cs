﻿namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.FluxosComerciais.Simconsultas.Ctes
{
	using ALL.Core.AcessoDados.TiposCustomizados;

	using NHibernate;
	using NHibernate.Mapping.ByCode;
	using NHibernate.Mapping.ByCode.Conformist;
	using NHibernate.Type;

	using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Simconsultas.Ctes;

	/// <summary>
	/// Classe de mapeamento do CteSimconsultas
	/// </summary>
	public class CteSimconsultasClassMapping : ClassMapping<CteSimconsultas>
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="CteSimconsultasClassMapping"/> class.
		/// </summary>
		public CteSimconsultasClassMapping()
		 {
			 this.Lazy(true);
			 this.Table("CTE_SIMCONSULTAS");
			 this.Schema("TRANSLOGIC");

			 this.Id(
				 x => x.Id,
				 m =>
				 {
					 m.Column("ID_CTE");
					 m.Generator(
						 Generators.Sequence,
						 g => g.Params(new
						 {
							 sequence = "CTE_SIMCONSULTAS_SEQ_ID"
						 }));
					 m.Type(new Int32Type());
					 m.Access(Accessor.Property);
				 });

			 this.Property(
				x => x.ChaveCte,
				map => { map.Column("CHAVE_CTE"); map.Unique(false); map.NotNullable(false); });

			 this.Property(
				x => x.Versao,
				map => { map.Column("VERSAO_CTE"); map.Unique(false); map.NotNullable(false); });

			 this.Property(
				x => x.Cuf,
				map => { map.Column("CUF"); map.Unique(false); map.NotNullable(false); });

			 this.Property(
				x => x.Cfop,
				map => { map.Column("CFOP"); map.Unique(false); map.NotNullable(false); });

			 this.Property(
				x => x.NatOp,
				map => { map.Column("NATOP"); map.Unique(false); map.NotNullable(false); });

			 this.Property(
				x => x.ForPag,
				map => { map.Column("FORPAG"); map.Unique(false); map.NotNullable(false); });

			 this.Property(
				x => x.Mod,
				map => { map.Column("MOD"); map.Unique(false); map.NotNullable(false); });

			 this.Property(
				x => x.Serie,
				map => { map.Column("SERIE"); map.Unique(false); map.NotNullable(false); });

			 this.Property(
				x => x.Numero,
				map => { map.Column("NCT"); map.Unique(false); map.NotNullable(false); });

			 this.Property(
				x => x.DhEmi,
				map => { map.Column("DHEMI"); map.Unique(false); map.NotNullable(false); });

			 this.Property(
				x => x.TpImp,
				map => { map.Column("TPIMP"); map.Unique(false); map.NotNullable(false); });

			 this.Property(
				x => x.TpEmis,
				map => { map.Column("TPEMIS"); map.Unique(false); map.NotNullable(false); });

			 this.Property(
				x => x.Cdv,
				map => { map.Column("CDV"); map.Unique(false); map.NotNullable(false); });

			 this.Property(
				x => x.TpAmb,
				map => { map.Column("TPAMB"); map.Unique(false); map.NotNullable(false); });

			 this.Property(
				x => x.TpCte,
				map => { map.Column("TPCTE"); map.Unique(false); map.NotNullable(false); });

			 this.Property(
				x => x.ProcEmi,
				map => { map.Column("PROCEMI"); map.Unique(false); map.NotNullable(false); });

			 this.Property(
				x => x.VerProc,
				map => { map.Column("VERPROC"); map.Unique(false); map.NotNullable(false); });

			 this.Property(
				x => x.RefCte,
				map => { map.Column("REFCTE"); map.Unique(false); map.NotNullable(false); });

			 this.Property(
				x => x.CmunEmi,
				map => { map.Column("CMUNEMI"); map.Unique(false); map.NotNullable(false); });

			 this.Property(
				x => x.XmunEmi,
				map => { map.Column("XMUNEMI"); map.Unique(false); map.NotNullable(false); });

			 this.Property(
				x => x.UfEmi,
				map => { map.Column("UFEMI"); map.Unique(false); map.NotNullable(false); });

			 this.Property(
				x => x.Modal,
				map => { map.Column("MODAL"); map.Unique(false); map.NotNullable(false); });

			 this.Property(
				x => x.TpServ,
				map => { map.Column("TPSERV"); map.Unique(false); map.NotNullable(false); });

			 this.Property(
				x => x.CmunIni,
				map => { map.Column("CMUNINI"); map.Unique(false); map.NotNullable(false); });

			 this.Property(
				x => x.XmunIni,
				map => { map.Column("XMUNINI"); map.Unique(false); map.NotNullable(false); });

			 this.Property(
				x => x.UfIni,
				map => { map.Column("UFINI"); map.Unique(false); map.NotNullable(false); });

			 this.Property(
				x => x.CmunFim,
				map => { map.Column("CMUNFIM"); map.Unique(false); map.NotNullable(false); });

			 this.Property(
				x => x.XmunFim,
				map => { map.Column("XMUNFIM"); map.Unique(false); map.NotNullable(false); });

			 this.Property(
				x => x.UfFim,
				map => { map.Column("UFFIM"); map.Unique(false); map.NotNullable(false); });

			 this.Property(
				x => x.Retira,
				map => { map.Column("RETIRA"); map.Unique(false); map.NotNullable(false); });

			 this.Property(
				x => x.XdetRetira,
				map => { map.Column("XDETRETIRA"); map.Unique(false); map.NotNullable(false); });

			 this.Property(
				x => x.TipoTomador,
				map => { map.Column("TP_TOMADOR"); map.Unique(false); map.NotNullable(false); });

			 this.Property(
				x => x.Toma,
				map => { map.Column("TOMA"); map.Unique(false); map.NotNullable(false); });

			 this.Property(
				x => x.DhCont,
				map => { map.Column("DHCONT"); map.Unique(false); map.NotNullable(false); });

			 this.Property(
				x => x.Xjust,
				map => { map.Column("XJUST"); map.Unique(false); map.NotNullable(false); });

			 this.Property(
				x => x.VprestVtPrest,
				map => { map.Column("VPREST_VTPREST"); map.Unique(false); map.NotNullable(false); });

			 this.Property(
				x => x.VprestVrec,
				map => { map.Column("VPREST_VREC"); map.Unique(false); map.NotNullable(false); });

			 this.Property(
				x => x.Peso,
				map => { map.Column("PESO"); map.Unique(false); map.NotNullable(false); });

			 this.Property(
				x => x.DataCadastro,
				map => { map.Column("TIMESTAMP"); map.Unique(false); map.NotNullable(false); });

			 this.Property(
				 x => x.CteXml,
				 map => { map.Column("CTE_XML"); map.Type(NHibernateUtil.AnsiString); map.Unique(false); map.NotNullable(false); });

			 this.Property(
				  x => x.IndProcessado,
				  map => { map.Column("IND_PROCESSADO"); map.Type(typeof(BooleanCharType), null); map.Unique(false); map.NotNullable(false); });

			 this.Property(
				 x => x.CodigoErro,
				 map => { map.Column("COD_ERRO_PROC"); map.Unique(false); map.NotNullable(false); });
			 
			this.Property(
				x => x.DescricaoErro,
				map => { map.Column("DESC_ERRO_PROC"); map.Unique(false); map.NotNullable(false); });

			 this.ManyToOne(
				x => x.Toma4, m =>
				{
					m.Column("ID_EMPRESA_TOMA4");
					m.Class(typeof(CteEmpresaSimconsultas));
					m.Cascade(Cascade.All);
					m.Fetch(FetchKind.Select);
					m.Update(true);
					m.Insert(true);
					m.Access(Accessor.Property);
					m.Unique(false);
					m.Lazy(LazyRelation.Proxy);
				});

			 this.ManyToOne(
				x => x.Emitente, m =>
				{
					m.Column("ID_EMPRESA_EMIT");
					m.Class(typeof(CteEmpresaSimconsultas));
					m.Cascade(Cascade.All);
					m.Fetch(FetchKind.Select);
					m.Update(true);
					m.Insert(true);
					m.Access(Accessor.Property);
					m.Unique(false);
					m.Lazy(LazyRelation.Proxy);
				});

			 this.ManyToOne(
				x => x.Remetente, m =>
				{
					m.Column("ID_EMPRESA_REM");
					m.Class(typeof(CteEmpresaSimconsultas));
					m.Cascade(Cascade.All);
					m.Fetch(FetchKind.Select);
					m.Update(true);
					m.Insert(true);
					m.Access(Accessor.Property);
					m.Unique(false);
					m.Lazy(LazyRelation.Proxy);
				});

			 this.ManyToOne(
				x => x.Expedidor, m =>
				{
					m.Column("ID_EMPRESA_EXPED");
					m.Class(typeof(CteEmpresaSimconsultas));
					m.Cascade(Cascade.All);
					m.Fetch(FetchKind.Select);
					m.Update(true);
					m.Insert(true);
					m.Access(Accessor.Property);
					m.Unique(false);
					m.Lazy(LazyRelation.Proxy);
				});

			 this.ManyToOne(
				x => x.Recebedor, m =>
				{
					m.Column("ID_EMPRESA_RECEB");
					m.Class(typeof(CteEmpresaSimconsultas));
					m.Cascade(Cascade.All);
					m.Fetch(FetchKind.Select);
					m.Update(true);
					m.Insert(true);
					m.Access(Accessor.Property);
					m.Unique(false);
					m.Lazy(LazyRelation.Proxy);
				});

			 this.ManyToOne(
				x => x.Destinatario, m =>
				{
					m.Column("ID_EMPRESA_DEST");
					m.Class(typeof(CteEmpresaSimconsultas));
					m.Cascade(Cascade.All);
					m.Fetch(FetchKind.Select);
					m.Update(true);
					m.Insert(true);
					m.Access(Accessor.Property);
					m.Unique(false);
					m.Lazy(LazyRelation.Proxy);
				});

			 /*this.Bag(
				 x => x.ListaComponentes, bag => 
				{
					  bag.Inverse(true); // Is collection inverse?
					  bag.Cascade(Cascade.Persist); // set cascade strategy
					  bag.Key(k => k.Column(col => col.Name("ID_CTE"))); // foreign key in CTE_COMP_PREST_SIMCONSULTAS table
				}, a => a.OneToMany());*/
		 }
	}
}