﻿namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.FluxosComerciais.Simconsultas.Ctes
{
	using NHibernate.Mapping.ByCode;
	using NHibernate.Mapping.ByCode.Conformist;
	using NHibernate.Type;

	using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Simconsultas.Ctes;

	/// <summary>
	/// Classe de mapeamento do <see cref="CteEmpresaSimconsultas"/>
	/// </summary>
	public class CteEmpresaSimconsultasClassMapping : ClassMapping<CteEmpresaSimconsultas>
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="CteEmpresaSimconsultasClassMapping"/> class.
		/// </summary>
		public CteEmpresaSimconsultasClassMapping()
		 {
			 this.Lazy(true);
			 this.Table("CTE_EMP_SIMCONSULTAS");
			 this.Schema("TRANSLOGIC");

			 this.Id(
				 x => x.Id,
				 m =>
				 {
					 m.Column("ID_EMPRESA");
					 m.Generator(
						 Generators.Sequence,
						 g => g.Params(new
						 {
							 sequence = "CTE_EMP_SIMCONSULTAS_SEQ_ID"
						 }));
					 m.Type(new Int32Type());
					 m.Access(Accessor.Property);
				 });

			 this.Property(
				x => x.TipoPessoa,
				map => { map.Column("TP_PESSOA"); map.Unique(false); map.NotNullable(false); });

			 this.Property(
				 x => x.CnpjCpf,
				 map => { map.Column("CNPJ_CPF"); map.Unique(false); map.NotNullable(false); });

			this.Property(
				 x => x.InscricaoEstadual,
				 map => { map.Column("IE"); map.Unique(false); map.NotNullable(false); });

			this.Property(
				 x => x.Xnome,
				 map => { map.Column("XNOME"); map.Unique(false); map.NotNullable(false); });

			this.Property(
				 x => x.Xfant,
				 map => { map.Column("XFANT"); map.Unique(false); map.NotNullable(false); });

			this.Property(
				 x => x.Fone,
				 map => { map.Column("FONE"); map.Unique(false); map.NotNullable(false); });

			this.Property(
				 x => x.Xlgr,
				 map => { map.Column("XLGR"); map.Unique(false); map.NotNullable(false); });

			this.Property(
				 x => x.Nro,
				 map => { map.Column("NRO"); map.Unique(false); map.NotNullable(false); });

			this.Property(
				 x => x.Xcpl,
				 map => { map.Column("XCPL"); map.Unique(false); map.NotNullable(false); });

			this.Property(
				 x => x.Xbairro,
				 map => { map.Column("XBAIRRO"); map.Unique(false); map.NotNullable(false); });

			this.Property(
				 x => x.Cmun,
				 map => { map.Column("CMUN"); map.Unique(false); map.NotNullable(false); });

			this.Property(
				 x => x.Xmun,
				 map => { map.Column("XMUN"); map.Unique(false); map.NotNullable(false); });

			this.Property(
				 x => x.Cep,
				 map => { map.Column("CEP"); map.Unique(false); map.NotNullable(false); });

			this.Property(
				 x => x.Uf,
				 map => { map.Column("UF"); map.Unique(false); map.NotNullable(false); });

			this.Property(
				 x => x.Cpais,
				 map => { map.Column("CPAIS"); map.Unique(false); map.NotNullable(false); });

			this.Property(
				 x => x.Xpais,
				 map => { map.Column("XPAIS"); map.Unique(false); map.NotNullable(false); });

			this.Property(
				 x => x.Email,
				 map => { map.Column("EMAIL"); map.Unique(false); map.NotNullable(false); });

			this.Property(
				 x => x.Isuf,
				 map => { map.Column("ISUF"); map.Unique(false); map.NotNullable(false); });

			/*this.Bag(
				 x => x.ListaDocumentos, bag =>
				 {
					 bag.Inverse(true); // Is collection inverse?
					 bag.Cascade(Cascade.Persist); // set cascade strategy
					 bag.Key(k => k.Column(col => col.Name("ID_EMPRESA"))); // foreign key in CTE_DOCS_REM_SIMCONSULTAS table
				 }, a => a.OneToMany());*/
		 }
	}
}
