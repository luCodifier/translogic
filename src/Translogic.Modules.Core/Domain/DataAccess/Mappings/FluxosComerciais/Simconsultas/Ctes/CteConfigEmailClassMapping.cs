﻿namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.FluxosComerciais.Simconsultas.Ctes
{
    using NHibernate;
    using NHibernate.Mapping.ByCode;
    using NHibernate.Mapping.ByCode.Conformist;
    using NHibernate.Type;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Simconsultas.Ctes;

    /// <summary>
    /// Classe de mapeamento CTE_CONFIG_EMAIL
    /// </summary>
    public class CteConfigEmailClassMapping : ClassMapping<CteConfigEmail>
    {
        /// <summary>
        /// Inicializa a instância e realiza o mapeamento dos campos
        /// </summary>
        public CteConfigEmailClassMapping()
        {
            this.Lazy(true);
            this.Table("CTE_CONFIG_EMAIL");
            this.Schema("TRANSLOGIC");

            this.Id(
                 x => x.Id,
                 m =>
                 {
                     m.Column("EP_ID_EMP");
                     m.Type(new Int32Type());
                     m.Access(Accessor.Property);
                 });

            this.Property(
               x => x.AssuntoEmail,
               map => { map.Column("ASSUNTO_EMAIL"); map.Unique(false); map.NotNullable(false); });
        }
    }
}