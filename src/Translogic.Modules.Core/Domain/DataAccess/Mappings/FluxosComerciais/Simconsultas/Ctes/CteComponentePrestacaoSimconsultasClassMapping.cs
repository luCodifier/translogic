﻿namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.FluxosComerciais.Simconsultas.Ctes
{
	using NHibernate.Mapping.ByCode;
	using NHibernate.Mapping.ByCode.Conformist;
	using NHibernate.Type;

	using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Simconsultas.Ctes;

	/// <summary>
	/// Classe de mapeamento da entidade <see cref="CteComponentePrestacaoSimconsultas"/>
	/// </summary>
	public class CteComponentePrestacaoSimconsultasClassMapping : ClassMapping<CteComponentePrestacaoSimconsultas>
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="CteComponentePrestacaoSimconsultasClassMapping"/> class.
		/// </summary>
		public CteComponentePrestacaoSimconsultasClassMapping()
		 {
			 this.Lazy(true);
			 this.Table("CTE_COMP_PREST_SIMCONSULTAS");
			 this.Schema("TRANSLOGIC");

			 this.Id(
				 x => x.Id,
				 m =>
				 {
					 m.Column("ID_COMPONENTE_PREST");
					 m.Generator(
						 Generators.Sequence,
						 g => g.Params(new
						 {
							 sequence = "CTE_COMP_PREST_SCO_SEQ_ID"
						 }));
					 m.Type(new Int32Type());
					 m.Access(Accessor.Property);
				 });

			 this.Property(
				 x => x.Xnome,
				 map => { map.Column("XNOME"); map.Unique(false); map.NotNullable(false); });

			 this.Property(
				x => x.Vcomp,
				map => { map.Column("VCOMP"); map.Unique(false); map.NotNullable(false); });

			 this.ManyToOne(
				 x => x.CteSimconsultas, m =>
				 {
					 m.Column("ID_CTE");
					 m.Class(typeof(CteSimconsultas));
					 m.Cascade(Cascade.All);
					 m.Fetch(FetchKind.Select);
					 m.Update(true);
					 m.Insert(true);
					 m.Access(Accessor.Property);
					 m.Unique(false);
					 m.Lazy(LazyRelation.Proxy);
				 });
		 }
	}
}