﻿namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.FluxosComerciais.Simconsultas.Ctes
{
	using NHibernate.Mapping.ByCode;
	using NHibernate.Mapping.ByCode.Conformist;
	using NHibernate.Type;

	using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Simconsultas.Ctes;

	/// <summary>
	/// Classe de mapeamento da entidade <see cref="CteDocumentoRemetenteSimconsultas"/>
	/// </summary>
	public class CteDocumentoRemetenteSimconsultasClassMapping : ClassMapping<CteDocumentoRemetenteSimconsultas>
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="CteDocumentoRemetenteSimconsultasClassMapping"/> class.
		/// </summary>
		public CteDocumentoRemetenteSimconsultasClassMapping()
		 {
			 this.Lazy(true);
			 this.Table("CTE_DOCS_REM_SIMCONSULTAS");
			 this.Schema("TRANSLOGIC");

			 this.Id(
				 x => x.Id,
				 m =>
				 {
					 m.Column("ID_DOCUMENTO");
					 m.Generator(
						 Generators.Sequence,
						 g => g.Params(new
						 {
							 sequence = "CTE_DOCS_REM_SCO_SEQ_ID"
						 }));
					 m.Type(new Int32Type());
					 m.Access(Accessor.Property);
				 });

			 this.Property(
				x => x.TipoDocumento,
				map => { map.Column("TP_DOCUMENTO"); map.Unique(false); map.NotNullable(false); });

			 this.Property(
				x => x.NfNroma,
				map => { map.Column("NF_NROMA"); map.Unique(false); map.NotNullable(false); });

			 this.Property(
				x => x.NfNped,
				map => { map.Column("NF_NPED"); map.Unique(false); map.NotNullable(false); });

			 this.Property(
				x => x.NfSerie,
				map => { map.Column("NF_SERIE"); map.Unique(false); map.NotNullable(false); });

			 this.Property(
				x => x.NfNdoc,
				map => { map.Column("NF_NDOC"); map.Unique(false); map.NotNullable(false); });

			 this.Property(
				x => x.NfDemi,
				map => { map.Column("NF_DEMI"); map.Unique(false); map.NotNullable(false); });

			 this.Property(
				x => x.NfVbc,
				map => { map.Column("NF_VBC"); map.Unique(false); map.NotNullable(false); });

			 this.Property(
				x => x.NfVicms,
				map => { map.Column("NF_VICMS"); map.Unique(false); map.NotNullable(false); });

			 this.Property(
				x => x.NfVbcst,
				map => { map.Column("NF_VBCST"); map.Unique(false); map.NotNullable(false); });

			 this.Property(
				x => x.NfVst,
				map => { map.Column("NF_VST"); map.Unique(false); map.NotNullable(false); });

			 this.Property(
				x => x.NfVprod,
				map => { map.Column("NF_VPROD"); map.Unique(false); map.NotNullable(false); });

			 this.Property(
				x => x.NfVnf,
				map => { map.Column("NF_VNF"); map.Unique(false); map.NotNullable(false); });

			 this.Property(
				x => x.NfNCfop,
				map => { map.Column("NF_NCFOP"); map.Unique(false); map.NotNullable(false); });

			 this.Property(
				x => x.NfNpeso,
				map => { map.Column("NF_NPESO"); map.Unique(false); map.NotNullable(false); });

			 this.Property(
				x => x.Pin,
				map => { map.Column("PIN"); map.Unique(false); map.NotNullable(false); });

			 this.Property(
				x => x.NfeChave,
				map => { map.Column("NFE_CHAVE"); map.Unique(false); map.NotNullable(false); });

			 this.Property(
				x => x.OutroTpDoc,
				map => { map.Column("OUTRO_TPDOC"); map.Unique(false); map.NotNullable(false); });

			 this.Property(
				x => x.OutroDescOutros,
				map => { map.Column("OUTRO_DESCOUTROS"); map.Unique(false); map.NotNullable(false); });

			 this.Property(
				x => x.OutroNdoc,
				map => { map.Column("OUTRO_NDOC"); map.Unique(false); map.NotNullable(false); });

			 this.Property(
				x => x.OutroDemi,
				map => { map.Column("OUTRO_DEMI"); map.Unique(false); map.NotNullable(false); });

			 this.Property(
				x => x.OutroVdocFisc,
				map => { map.Column("OUTRO_VDOCFISC"); map.Unique(false); map.NotNullable(false); });

			 this.ManyToOne(
				x => x.CteEmpresaSimconsultas, m =>
				{
					m.Column("ID_EMPRESA");
					m.Class(typeof(CteEmpresaSimconsultas));
					m.Cascade(Cascade.All);
					m.Fetch(FetchKind.Select);
					m.Update(true);
					m.Insert(true);
					m.Access(Accessor.Property);
					m.Unique(false);
					m.Lazy(LazyRelation.Proxy);
				});
		 }
	}
}