﻿namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.FluxosComerciais.Mdfes
{
    using NHibernate.Mapping.ByCode;
    using NHibernate.Mapping.ByCode.Conformist;
    using NHibernate.Type;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes;

    /// <summary>
    /// Classe de mapeamento da entidade <see cref="LogImpressaoRecibo"/>
    /// </summary>
    public class LogImpressaoReciboClassMapping : ClassMapping<LogImpressaoRecibo>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LogImpressaoReciboClassMapping"/> class.
        /// </summary>
        public LogImpressaoReciboClassMapping()
        {
            this.Lazy(false);
            this.Table("LOG_IMPRESSAO_RECIBO");
            this.Schema("TRANSLOGIC");
            this.Mutable(false);

            this.Id(
                x => x.Id,
                m =>
                {
                    m.Column("LG_ID_LOG");
                    m.Generator(Generators.Sequence, g => g.Params(new { sequence = "LOG_IMPRESSAO_RECIBO_SEQ_ID" }));
                    m.Type(new Int32Type());
                    m.Access(Accessor.Property);
                });

            this.Property(
                x => x.UsuarioId,
                map => map.Column("US_IDT_USU"));
            
            this.Property(
                x => x.DataHora,
                map => map.Column("LG_DT_LOG"));

            this.Property(
                x => x.VagaoId,
                map => map.Column("VG_ID_VG"));

            this.Property(
                x => x.TremId,
                map => map.Column("TR_ID_TRM"));

            this.Property(
                x => x.EmpresaId,
                map => map.Column("EP_ID_EMP"));
        }
    }
}