﻿namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.FluxosComerciais.DocumentosVagao
{
    using ALL.Core.AcessoDados.TiposCustomizados;
    using NHibernate.Mapping.ByCode;
    using NHibernate.Mapping.ByCode.Conformist;
    using NHibernate.Type;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.DocumentosVagao;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Nfes;

    /// <summary>
    /// Mapeamento da tabela de Caminhão
    /// </summary>
    public class VagaoTicketClassMapping : ClassMapping<VagaoTicket>
    {
        /// <summary>
        /// Construtor da classe de caminhão
        /// </summary>
        public VagaoTicketClassMapping()
        {
            this.Table("VAGAO_TICKET");
            this.Schema("TRANSLOGIC");

            this.Id(
                x => x.Id,
                m =>
                {
                    m.Column("ID");
                    m.Generator(Generators.Sequence, g => g.Params(new { sequence = "VAGAO_TICKET_SEQ_ID" }));
                    m.Type(new Int32Type());
                    m.Access(Accessor.Property);
                });

            this.Property(
                x => x.PesoBruto,
                map =>
                {
                    map.Column("TB");
                    map.Unique(false);
                    map.NotNullable(true);
                });

            this.Property(
                x => x.PesoTara,
                map =>
                {
                    map.Column("TARA");
                    map.Unique(false);
                    map.NotNullable(true);
                });

            this.Property(
                x => x.PesoLiquido,
                map =>
                {
                    map.Column("TU");
                    map.Unique(false);
                    map.NotNullable(true);
                });

            this.Property(
                x => x.CodigoVagao,
                map =>
                {
                    map.Column("vg_cod_vag");
                    map.Unique(false);
                    map.NotNullable(true);
                });

            this.Property(
                x => x.AjusteTrg,
                map =>
                {
                    map.Column("ajuste_trg");
                    map.Unique(false);
                    map.NotNullable(false);
                    map.Type(typeof(BooleanCharType), null);
                });

            this.ManyToOne(
                x => x.Ticket, m =>
                {
                    m.Column("TICKET_BALANCA_ID");
                    m.Class(typeof(TicketBalanca));
                    m.Cascade(Cascade.None);
                    m.Fetch(FetchKind.Join);
                    m.Lazy(LazyRelation.NoLazy);
                });

            this.Property(
                x => x.DataCadastro,
                map =>
                {
                    map.Column("DATA_CADASTRO");
                    map.Unique(false);
                    map.NotNullable(true);
                });
        }
    }
}