﻿namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.FluxosComerciais.DocumentosVagao
{
    using ALL.Core.AcessoDados.TiposCustomizados;
    using NHibernate;
    using NHibernate.Mapping.ByCode;
    using NHibernate.Mapping.ByCode.Conformist;
    using NHibernate.Type;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.DocumentosVagao;

    /// <summary>
    /// Mapeamento de Controle de recebimentos dos tickets de balança
    /// </summary>
    public class WsTicketControleRecebimentoClassMapping : ClassMapping<WsTicketControleRecebimento>
    {
        /// <summary>
        /// Construtor da classe de Controle de recebimentos dos tickets de balança
        /// </summary>
        public WsTicketControleRecebimentoClassMapping()
        {
            this.Table("WS_TICKET_CTRL_RECEBIMENTO");
            this.Schema("TRANSLOGIC");

            this.Id(
                x => x.Id,
                m =>
                {
                    m.Column("ID");
                    m.Generator(Generators.Sequence, g => g.Params(new { sequence = "WS_TICKET_CTRL_REC_SEQ_ID" }));
                    m.Type(new Int32Type());
                });

            this.Property(
                x => x.IndErro,
                map =>
                {
                    map.Column("IND_ERRO");
                    map.Type(typeof(BooleanCharType), null);
                    map.Unique(false);
                });

            this.Property(
                x => x.LogRetorno,
                map =>
                {
                    map.Column("LOG_RETORNO");
                    map.Type(NHibernateUtil.AnsiString);
                    map.Unique(false);
                });

            this.Property(
              x => x.MensagemRecebida,
              map =>
              {
                  map.Column("MSG_RECEBIDA");
                  map.Type(NHibernateUtil.AnsiString);
                  map.Unique(false);
              });

            this.Property(
              x => x.Protocolo,
              map =>
              {
                  map.Column("PROTOCOLO");
                  map.Type(NHibernateUtil.AnsiString);
                  map.Unique(false);
              });

            this.Property(
              x => x.DataCadastro,
              map =>
              {
                  map.Column("TIMESTAMP");
                  map.Unique(false);
                  map.NotNullable(true);
              });

            this.ManyToOne(
                  x => x.Ticket, m =>
                  {
                      m.Column("TICKET_BALANCA_ID");
                      m.Class(typeof(TicketBalanca));
                      m.Cascade(Cascade.None);
                      m.Fetch(FetchKind.Join);
                      m.Lazy(LazyRelation.Proxy);
                  });
        }
    }
}