﻿namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.FluxosComerciais.DocumentosVagao
{
    using NHibernate.Mapping.ByCode;
    using NHibernate.Mapping.ByCode.Conformist;
    using NHibernate.Type;
    using Translogic.Modules.Core.Domain.Model.Estrutura;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.DocumentosVagao;
    using Translogic.Modules.Core.Domain.Model.Via;

    /// <summary>
    /// Mapeamento da classe de ticket da balanca
    /// </summary>
    public class TicketBalancaClassMapping : ClassMapping<TicketBalanca>
    {
        /// <summary>
        /// Construtor da classe de ticket da balança
        /// </summary>
        public TicketBalancaClassMapping()
        {
            this.Table("TICKET_BALANCA");
            this.Schema("TRANSLOGIC");

            this.Id(
                x => x.Id,
                m =>
                {
                    m.Column("ID");
                    m.Generator(Generators.Sequence, g => g.Params(new { sequence = "TICKET_BALANCA_SEQ_ID" }));
                    m.Type(new Int32Type());
                });

            this.ManyToOne(
                x => x.Destino, m =>
                {
                    m.Column("EP_ID_EMP_DEST");
                    m.Class(typeof(EmpresaCliente));
                    m.Cascade(Cascade.None);
                    m.Fetch(FetchKind.Join);
                    m.Lazy(LazyRelation.Proxy);
                });
            
            this.ManyToOne(
                x => x.Origem, m =>
                {
                    m.Column("EP_ID_EMP_ORI");
                    m.Class(typeof(EmpresaCliente));
                    m.Cascade(Cascade.None);
                    m.Fetch(FetchKind.Join);
                    m.Lazy(LazyRelation.Proxy);
                });
           
            this.Property(
                x => x.DataPesagem,
                map =>
                {
                    map.Column("DT_PESAGEM");
                    map.Unique(false);
                    map.NotNullable(true);
                });

            this.Property(
                x => x.Observacao,
                map =>
                {
                    map.Column("OBSERVACAO");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            this.Property(
                x => x.Responsavel,
                map =>
                {
                    map.Column("RESPONSAVEL");
                    map.Unique(false);
                    map.NotNullable(false);
                });
            
            this.Property(
                x => x.NumeroTicket,
                map =>
                {
                    map.Column("NUM_TICKET");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            this.Property(
                x => x.NumeroBalanca,
                map =>
                {
                    map.Column("NUM_BALANCA");
                    map.Unique(false);
                    map.NotNullable(false);
                });
        }
    }
}