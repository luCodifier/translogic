﻿namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.FluxosComerciais.DocumentosVagao
{
    using NHibernate;
    using NHibernate.Mapping.ByCode;
    using NHibernate.Mapping.ByCode.Conformist;
    using NHibernate.Type;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.DocumentosVagao;

    /// <summary>
    /// Clase de log de ticket de balanca
    /// </summary>
    public class WsTicketLogRecebimentoClassMapping : ClassMapping<WsTicketLogRecebimento>
    {
          /// <summary>
        /// Construtor da classe de log de recebimentos dos tickets de balança
        /// </summary>
        public WsTicketLogRecebimentoClassMapping()
        {
            this.Table("WS_TICKET_LOG_RECEBIMENTO");
            this.Schema("TRANSLOGIC");

            this.Id(
                x => x.Id,
                m =>
                {
                    m.Column("ID");
                    m.Generator(Generators.Sequence, g => g.Params(new { sequence = "WS_TICKET_LOG_REC_SEQ_ID" }));
                    m.Type(new Int32Type());
                });

           this.Property(
                x => x.LogMensagem,
                map =>
                {
                    map.Column("LOG_MESSAGE");
                    map.Type(NHibernateUtil.AnsiString);
                    map.Unique(false);
                });

           this.Property(
              x => x.LogMensagemRetorno,
              map =>
              {
                  map.Column("RETURN_MESSAGE");
                  map.Type(NHibernateUtil.AnsiString);
                  map.Unique(false);
              });

            this.Property(
              x => x.DataCadastro,
              map =>
              {
                  map.Column("TIMESTAMP");
                  map.Unique(false);
                  map.NotNullable(true);
              });
        }
    }
}