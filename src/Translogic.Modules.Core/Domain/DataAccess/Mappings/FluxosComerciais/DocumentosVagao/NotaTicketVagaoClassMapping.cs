﻿namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.FluxosComerciais.DocumentosVagao
{
    using NHibernate.Mapping.ByCode;
    using NHibernate.Mapping.ByCode.Conformist;
    using NHibernate.Type;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.DocumentosVagao;

    /// <summary>
    /// Mapeamento da tabela de notas do caminhão do ticket da balança
    /// </summary>
    public class NotaTicketVagaoClassMapping : ClassMapping<NotaTicketVagao>
    {
        /// <summary>
        /// Construtor da classe de notas do caminhão.
        /// </summary>
         public NotaTicketVagaoClassMapping()
         {
             this.Table("NOTA_TICKET_VAGAO");
             this.Schema("TRANSLOGIC");

             this.Id(
                 x => x.Id,
                 m =>
                 {
                     m.Column("ID");
                     m.Generator(Generators.Sequence, g => g.Params(new { sequence = "NOTA_TICKET_VAGAO_SEQ_ID" }));
                     m.Type(new Int32Type());
                     m.Access(Accessor.Property);
                 });

             this.Property(
                 x => x.ChaveNfe,
                 map =>
                 {
                     map.Column("CHAVE_NFE");
                     map.Unique(false);
                     map.NotNullable(true);
                 });

             this.Property(
                 x => x.PesoRateio,
                 map =>
                 {
                     map.Column("PESO_RATEIO");
                     map.Unique(false);
                     map.NotNullable(true);
                 });

             this.ManyToOne(
                x => x.Vagao, m =>
                {
                    m.Column("VAGAO_TICKET_ID");
                    m.Class(typeof(VagaoTicket));
                    m.Cascade(Cascade.None);
                    m.Fetch(FetchKind.Join);
                    m.Lazy(LazyRelation.NoLazy);
                });
         }
    }
}