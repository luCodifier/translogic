﻿using Translogic.Modules.OsTo.Domain.Services;

namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.FluxosComerciais.ConhecimentoTransporteEletronico
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using NHibernate.Mapping.ByCode;
    using NHibernate.Mapping.ByCode.Conformist;
    using NHibernate.Type;
    using Translogic.Modules.Core.Domain.Model.Acesso;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Conteiner;

    public class CteConteinerClassMapping : ClassMapping<CteConteiner>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DiarioBordoCteClassMapping"/> class.
        /// </summary>
        public CteConteinerClassMapping()
        {
            this.Lazy(false);
            this.Table("CTE_CONTEINER");
            this.Schema("TRANSLOGIC");
            this.Mutable(false);

            this.Id(
                x => x.Id,
                m =>
                {
                    m.Column("ID_CTE_CTN");
                    m.Generator(Generators.Sequence, g => g.Params(new { sequence = "CTE_CONTEINER_SEQ_ID" }));
                    m.Type(new Int32Type());
                    m.Access(Accessor.Property);
                });

            this.ManyToOne(
               x => x.Cte, m =>
               {
                   m.Column("ID_CTE");
                   m.Class(typeof(Cte));
                   m.Cascade(Cascade.None);
                   m.Fetch(FetchKind.Select);
                   m.Access(Accessor.Property);
                   m.Unique(false);
                   m.Lazy(LazyRelation.Proxy);
                   m.Update(false);
                   m.Insert(false);
               });

            this.ManyToOne(
               x => x.Conteiner, m =>
               {
                   m.Column("CN_IDT_CNT");
                   m.Class(typeof(Conteiner));
                   m.Cascade(Cascade.None);
                   m.Fetch(FetchKind.Select);
                   m.Access(Accessor.Property);
                   m.Unique(false);
                   m.Lazy(LazyRelation.Proxy);
                   m.Update(false);
                   m.Insert(false);
               });

            this.Property(
               x => x.Agrupamento,
               map =>
               {
                   map.Column("ID_AGRUPAMENTO");
                   map.Unique(false);
                   map.NotNullable(true);
               });


            this.Property(
               x => x.AgrupamentoCarga,
               map =>
               {
                   map.Column("ID_AGRUP_CARGA");
                   map.Unique(false);
                   map.NotNullable(true);
               });

            this.Property(
               x => x.IndCarregamentoCancelado,
               map =>
               {
                   map.Column("IND_CARR_CANC");
                   map.Unique(false);
                   map.NotNullable(true);
               });

            this.Property(
               x => x.IndCarregamentoFinalizado,
               map =>
               {
                   map.Column("IND_CARR_FIN");
                   map.Unique(false);
                   map.NotNullable(true);
               });

        }
    }
}