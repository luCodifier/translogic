﻿namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.FluxosComerciais.ConhecimentoTransporteEletronico
{
    using NHibernate.Mapping.ByCode;
    using NHibernate.Mapping.ByCode.Conformist;
    using NHibernate.Type;
    using Translogic.Modules.Core.Domain.Model.Acesso;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;

    /// <summary>
    /// Classe de mapeamento da entidade <see cref="DiarioBordoCte"/>
    /// </summary>
    public class DiarioBordoCteClassMapping : ClassMapping<DiarioBordoCte>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DiarioBordoCteClassMapping"/> class.
        /// </summary>
        public DiarioBordoCteClassMapping()
        {
            this.Lazy(false);
            this.Table("DIARIO_BORDO_CTE");
            this.Schema("TRANSLOGIC");
            this.Mutable(false);

            this.Id(
                x => x.Id,
                m =>
                {
                    m.Column("DB_ID_DB");
                    m.Generator(Generators.Sequence, g => g.Params(new { sequence = "DIARIO_BORDO_CTE_SEQ_ID" }));
                    m.Type(new Int32Type());
                    m.Access(Accessor.Property);
                });

            this.Property(
                x => x.DataHora,
                map =>
                {
                    map.Column("DB_DATA_DB");
                    map.Unique(false);
                    map.NotNullable(true);
                });

            this.Property(
                x => x.Acao,
                map =>
                {
                    map.Column("DB_ACAO_DB");
                    map.Unique(false);
                    map.NotNullable(true);
                });

            this.ManyToOne(
               x => x.UsuarioCadastro, m =>
               {
                   m.Column("US_IDT_USU");
                   m.Class(typeof(Usuario));
                   m.Cascade(Cascade.None);
                   m.Fetch(FetchKind.Join);
                   m.Access(Accessor.Property);
                   m.Unique(false);
                   m.Lazy(LazyRelation.Proxy);
               });

            this.ManyToOne(
               x => x.Cte, m =>
               {
                   m.Column("CTE_ID_CTE");
                   m.Class(typeof(Cte));
                   m.Cascade(Cascade.None);
                   m.Fetch(FetchKind.Join);
                   m.Access(Accessor.Property);
                   m.Unique(false);
                   m.Lazy(LazyRelation.Proxy);
               });
        }
    }
}