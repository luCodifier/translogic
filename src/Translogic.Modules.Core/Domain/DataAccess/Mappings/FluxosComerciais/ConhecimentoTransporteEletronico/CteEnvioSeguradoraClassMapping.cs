﻿namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.FluxosComerciais.ConhecimentoTransporteEletronico
{
    using Model.FluxosComerciais.ConhecimentoTransporteEletronico;
    using NHibernate.Mapping.ByCode;
    using NHibernate.Mapping.ByCode.Conformist;
    using NHibernate.Type;

    /// <summary>
    /// Classe de mapeamento da entidade 
    /// </summary>
    public class CteEnvioSeguradoraClassMapping : ClassMapping<CteEnvioSeguradoraHist>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CteEnvioSeguradoraClassMapping"/> class.
        /// </summary>
        public CteEnvioSeguradoraClassMapping()
        {
            this.Lazy(false);
            this.Table("CTE_ENVIO_SEGURADORA_HIST");
            this.Schema("TRANSLOGIC");
            this.Mutable(false);

            this.Id(
              x => x.Id,
              m =>
              {
                  m.Column("ID_CTE_ENVIO_SEGURADORA_HIST");
                  m.Generator(Generators.Sequence, g => g.Params(new { sequence = "CTE_ENVIO_SEGURADORA_HIST_SEQ" }));
                  m.Type(new Int32Type());
                  m.Access(Accessor.Property);
              });

            this.ManyToOne(
              x => x.Cte, u =>
              {
                  u.Column("ID_CTE");
                  u.Class(typeof(Cte));
                  u.Cascade(Cascade.None);
                  u.Fetch(FetchKind.Join);
                  u.Access(Accessor.Property);
                  u.Unique(false);
                  u.Lazy(LazyRelation.Proxy);
              });

            this.Property(
                x => x.TipoEnvio,
                map =>
                {
                    map.Column("TIPO_ENVIO");
                    map.Unique(false);
                });

            this.Property(
                 x => x.Retorno,
                 map =>
                 {
                     map.Column("RETORNO");
                     map.Unique(false);
                 });

            this.Property(
                 x => x.Seguradora,
                 map =>
                 {
                     map.Column("SEGURADORA");
                     map.Unique(false);
                 });

            this.Property(
                 x => x.Email,
                 map =>
                 {
                     map.Column("EMAIL");
                     map.Unique(false);
                 });

            this.Property(
                x => x.StatusEnvio,
                map =>
                {
                    map.Column("STATUS_ENVIO");
                    map.Unique(false);
                });
        }
    }
}