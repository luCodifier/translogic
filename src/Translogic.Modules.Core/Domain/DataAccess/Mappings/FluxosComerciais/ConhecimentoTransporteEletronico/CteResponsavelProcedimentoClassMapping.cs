﻿namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.FluxosComerciais.ConhecimentoTransporteEletronico
{
    using NHibernate.Mapping.ByCode;
    using NHibernate.Mapping.ByCode.Conformist;
    using NHibernate.Type;
    using Translogic.Modules.Core.Domain.Model.Acesso;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
    
    /// <summary>
    /// Classe de mapeamento da entidade <see cref="CteResponsavelProcedimento"/>
    /// </summary>
    public class CteResponsavelProcedimentoClassMapping : ClassMapping<CteResponsavelProcedimento>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CteResponsavelProcedimentoClassMapping"/> class.
        /// </summary>
        public CteResponsavelProcedimentoClassMapping()
        {
            this.Lazy(false);
            this.Table("CTE_RESPONSAVEL_PO");
            this.Schema("TRANSLOGIC");
            this.Mutable(false);

            this.Id(
                x => x.Id,
                m =>
                {
                    m.Column("CRP_ID_CRP");
                    m.Generator(Generators.Sequence, g => g.Params(new { sequence = "CTE_RESPONSAVEL_PO_SEQ_ID" }));
                    m.Type(new Int32Type());
                    m.Access(Accessor.Property);
                });

            this.ManyToOne(
               x => x.UsuarioCadastro, m =>
               {
                   m.Column("US_IDT_USU");
                   m.Class(typeof(Usuario));
                   m.Cascade(Cascade.None);
                   m.Fetch(FetchKind.Join);
                   m.Access(Accessor.Property);
                   m.Unique(false);
                   m.Lazy(LazyRelation.Proxy);
               });

            this.ManyToOne(
               x => x.CteStatusRetorno, m =>
               {
                   m.Column("CODIGO_STATUS_RETORNO");
                   m.Class(typeof(CteStatusRetorno));
                   m.Cascade(Cascade.None);
                   m.Fetch(FetchKind.Join);
                   m.Access(Accessor.Property);
                   m.Unique(false);
                   m.Lazy(LazyRelation.Proxy);
               });

            this.Property(
                x => x.DescricaoProcedimento,
                map =>
                {
                    map.Column("CRP_DESC_PO_CRP");
                    map.Unique(false);
                    map.NotNullable(true);
                });

            this.Property(
                x => x.NomeProcedimento,
                map =>
                {
                    map.Column("CRP_NOME_PO_CRP");
                    map.Unique(false);
                    map.NotNullable(true);
                });

            this.Property(
                x => x.LinkProcedimento,
                map =>
                {
                    map.Column("CRP_LINK_PO_CRP");
                    map.Unique(false);
                    map.NotNullable(true);
                });

            this.Property(
                x => x.ResponsavelProcedimento,
                map =>
                {
                    map.Column("CRP_RESP_PO_CRP");
                    map.Unique(false);
                    map.NotNullable(true);
                });

            this.Property(
                x => x.DataHoraCadastro,
                map =>
                {
                    map.Column("CRP_DATA_CRP");
                    map.Unique(false);
                    map.NotNullable(true);
                });

            this.Property(
                x => x.Ativo,
                map =>
                {
                    map.Column("CRP_IND_ATIVO_CRP");
                    map.Unique(false);
                    map.NotNullable(true);
                });
        }
    }
}