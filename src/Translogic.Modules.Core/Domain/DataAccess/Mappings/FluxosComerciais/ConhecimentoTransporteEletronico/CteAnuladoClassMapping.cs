﻿namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.FluxosComerciais.ConhecimentoTransporteEletronico
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using NHibernate.Mapping.ByCode;
    using NHibernate.Mapping.ByCode.Conformist;
    using NHibernate.Type;
    using Translogic.Modules.Core.Domain.Model.Acesso;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Conteiner;

    public class CteAnuladoClassMapping : ClassMapping<CteAnulado>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CteAnuladoClassMapping"/> class.
        /// </summary>
        public CteAnuladoClassMapping()
        {
            this.Lazy(false);
            this.Table("CTE_ANULADO");
            this.Schema("TRANSLOGIC");
            this.Mutable(false);

            this.Id(
                x => x.Id,
                m =>
                {
                    m.Column("ID_CTE_ANULADO");
                    m.Generator(Generators.Sequence, g => g.Params(new { sequence = "CTE_ANULADO_SEQ_ID" }));
                    m.Type(new Int32Type());
                    m.Access(Accessor.Property);
                });

            this.Property(
                x => x.DataHora ,
                map =>
                {
                    map.Column("DATA_HORA");
                    map.Unique(false);
                    map.NotNullable(false);
                });
            
            this.ManyToOne(
                x => x.Cte, m =>
                {
                    m.Column("ID_CTE");
                    m.Class(typeof(Cte));
                    m.Cascade(Cascade.None);
                    m.Fetch(FetchKind.Select);
                    m.Access(Accessor.Property);
                    m.Unique(false);
                    m.Lazy(LazyRelation.NoLazy);
                    m.Update(true);
                    m.Insert(true);
                });
            
            this.ManyToOne(
                x => x.CteAnulacao, m =>
                {
                    m.Column("ID_CTE_ANULACAO");
                    m.Class(typeof(Cte));
                    m.Cascade(Cascade.None);
                    m.Fetch(FetchKind.Select);
                    m.Access(Accessor.Property);
                    m.Unique(false);
                    m.Lazy(LazyRelation.NoLazy);
                    m.Update(true);
                    m.Insert(true);
                });
            
            this.ManyToOne(
                x => x.Usuario, m =>
                {
                    m.Column("US_IDT_USU");
                    m.Class(typeof(Usuario));
                    m.Cascade(Cascade.None);
                    m.Fetch(FetchKind.Select);
                    m.Access(Accessor.Property);
                    m.Unique(false);
                    m.Lazy(LazyRelation.NoLazy);
                    m.Update(true);
                    m.Insert(true);
                });

            this.Property(
                x => x.TipoAnulacao,
                map =>
                {
                    map.Column("TIPO_ANL");
                    map.Unique(false);
                    map.NotNullable(false);
                });
        }

    }
}