﻿
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Type;
using Translogic.Modules.Core.Domain.Model.Despacho;

namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.Despacho
{
    public class ConfiguracaoDespachoAutomaticoMapping : ClassMapping<ConfiguracaoDespachoAutomatico>
    {
        public ConfiguracaoDespachoAutomaticoMapping()
        {
            Lazy(false);
            Table("BO_BOLAR_CONF_CARGA_AUT");
            Schema("TRANSLOGIC");

            Id(
                 x => x.Id,
                 m =>
                 {
                     m.Column("CA_ID_CA");
                     m.Generator(
                         Generators.Sequence,
                         g => g.Params(new
                         {
                             sequence = "BO_BOLAR_CONF_CA_SEQ_ID"
                         }));
                     m.Type(new Int32Type());
                     m.Access(Accessor.Property);
                 });

            Property(x => x.Cliente, map => map.Column("AO_COD_AOP"));
            Property(x => x.AreaOperacionaId, map => map.Column("AO_ID_EST"));
            Property(x => x.Terminal, map => map.Column("IND_CARGA_TERM"));
            Property(x => x.Patio, map => map.Column("IND_CARGA_PATIO"));
            Property(x => x.LocalizacaoId, map => { map.Column("LO_IDT_LCO"); map.NotNullable(false); });
            Property(x => x.Segmento, map => map.Column("DSC_SEGMENTO"));
            Property(x => x.Data, map => map.Column("CA_TIMESTAMP"));
            Property(x => x.Manual, map => map.Column("CA_LIB_MANUAL"));
        }
    }
}