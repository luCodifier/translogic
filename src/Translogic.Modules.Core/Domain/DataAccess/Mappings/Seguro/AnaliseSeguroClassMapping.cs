﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Type;
using Translogic.Modules.Core.Domain.Model.Seguro;

namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.Seguro
{
    /// <summary>
    ///     Mapeamento da classe <see cref="AnaliseSeguro" />
    /// </summary>
    public class AnaliseSeguroClassMapping : ClassMapping<AnaliseSeguro>
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="AnaliseSeguroClassMapping" /> class.
        /// </summary>
        public AnaliseSeguroClassMapping()
        {
            Lazy(true);
            Table("ANALISE_SEGURO");
            Schema("TRANSLOGIC");

            Id(
                x => x.Id,
                m =>
                {
                    m.Column("AS_ID_AS");
                    m.Generator(
                        Generators.Sequence,
                        g => g.Params(new
                        {
                            sequence = "ANALISE_SEGURO_SEQ_ID"
                        }));
                    m.Type(new Int32Type());
                    m.Access(Accessor.Property);
                });

            Property(
                x => x.IndicadorSituacao,
                map =>
                {
                    map.Column("AS_IND_SIT");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            ManyToOne(
                x => x.ProcessoSeguro, m =>
                {
                    m.Column("PC_ID_PC");
                    m.Class(typeof (ProcessoSeguro));
                    m.Cascade(Cascade.None);
                    m.Fetch(FetchKind.Select);
                    m.Update(true);
                    m.Insert(true);
                    m.Access(Accessor.Property);
                    m.Unique(false);
                    m.Lazy(LazyRelation.Proxy);
                    m.NotNullable(false);
                });

            Property(
                x => x.Data,
                map =>
                {
                    map.Column("AS_TIMESTAMP");
                    map.Unique(false);
                    map.NotNullable(false);
                });
        }
    }
}