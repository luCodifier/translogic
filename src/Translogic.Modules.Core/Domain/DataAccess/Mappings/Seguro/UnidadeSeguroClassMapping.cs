﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Type;
using Translogic.Modules.Core.Domain.Model.Seguro;

namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.Seguro
{
	/// <summary>
	///   Mapeamento da classe <see cref="UnidadeSeguro" />
	/// </summary>
	public class UnidadeSeguroClassMapping : ClassMapping<UnidadeSeguro>
	{
		/// <summary>
		///   Initializes a new instance of the <see cref="UnidadeSeguroClassMapping" /> class.
		/// </summary>
		public UnidadeSeguroClassMapping()
		{
			Lazy(true);
			Table("UNIDADE_SEGURO");
			Schema("TRANSLOGIC");

			Id(
				x => x.Id,
				m =>
				{
					m.Column("UD_ID_UD");
					m.Generator(
						Generators.Sequence,
						g => g.Params(new
						{
							sequence = "UNIDADE_SEGURO_SEQ_ID"
						}));
					m.Type(new Int32Type());
					m.Access(Accessor.Property);
				});

			Property(
				x => x.Descricao,
				map =>
				{
					map.Column("UD_DSC_UNID");
					map.Unique(false);
					map.NotNullable(false);
				});

			Property(
				x => x.Tipo,
				map =>
				{
					map.Column("UD_TPO_UNID");
					map.Unique(false);
					map.NotNullable(false);
				});
		}
	}
}