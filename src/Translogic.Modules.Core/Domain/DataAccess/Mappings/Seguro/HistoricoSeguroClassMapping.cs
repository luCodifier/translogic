﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Type;
using Translogic.Modules.Core.Domain.Model.Seguro;

namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.Seguro
{
    /// <summary>
    ///     Mapeamento da classe <see cref="HistoricoSeguro" />
    /// </summary>
    public class HistoricoSeguroClassMapping : ClassMapping<HistoricoSeguro>
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="HistoricoSeguroClassMapping" /> class.
        /// </summary>
        public HistoricoSeguroClassMapping()
        {
            Lazy(true);
            Table("HISTORICO_SEGURO");
            Schema("TRANSLOGIC");

            Id(
                x => x.Id,
                m =>
                {
                    m.Column("HS_ID_HS");
                    m.Generator(
                        Generators.Sequence,
                        g => g.Params(new
                        {
                            sequence = "HISTORICO_SEGURO_SEQ_ID"
                        }));
                    m.Type(new Int32Type());
                    m.Access(Accessor.Property);
                });

            ManyToOne(
                x => x.ProcessoSeguro, m =>
                {
                    m.Column("PC_ID_PC");
                    m.Class(typeof (ProcessoSeguro));
                    m.Cascade(Cascade.None);
                    m.Fetch(FetchKind.Select);
                    m.Update(true);
                    m.Insert(true);
                    m.Access(Accessor.Property);
                    m.Unique(false);
                    m.Lazy(LazyRelation.Proxy);
                    m.NotNullable(false);
                });

            Property(
                x => x.Usuario,
                map =>
                {
                    map.Column("US_USR_IDU");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            Property(
                x => x.Descricao,
                map =>
                {
                    map.Column("HS_HISTORICO");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            Property(
                x => x.TimeStamp,
                map =>
                {
                    map.Column("HS_TIMESTAMP");
                    map.Unique(false);
                    map.NotNullable(false);
                });
        }
    }
}