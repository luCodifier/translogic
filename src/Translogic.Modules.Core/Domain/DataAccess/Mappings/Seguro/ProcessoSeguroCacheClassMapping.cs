﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Type;
using Translogic.Modules.Core.Domain.Model.Acesso;
using Translogic.Modules.Core.Domain.Model.Seguro;
using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao;

namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.Seguro
{
    /// <summary>
    ///     Mapeamento da classe <see cref="ProcessoSeguroCache" />
    /// </summary>
    public class ProcessoSeguroCacheClassMapping : ClassMapping<ProcessoSeguroCache>
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="ProcessoSeguroCacheClassMapping" /> class.
        /// </summary>
        public ProcessoSeguroCacheClassMapping()
        {
            Lazy(true);
            Table("PROCESSO_SEGURO_CACHE");
            Schema("TRANSLOGIC");

            Id(
                x => x.Id,
                m =>
                {
                    m.Column("PSC_ID_PSC");
                    m.Generator(
                        Generators.Sequence,
                        g => g.Params(new
                        {
                            sequence = "PROCESSO_SEGURO_CACHE_SEQ_ID"
                        }));
                    m.Type(new Int32Type());
                    m.Access(Accessor.Property);
                });

            ManyToOne(
                x => x.Usuario, m =>
                {
                    m.Column("US_IDT_USU");
                    m.Class(typeof (Usuario));
                    m.Cascade(Cascade.All);
                    m.Fetch(FetchKind.Select);
                    m.Update(true);
                    m.Insert(true);
                    m.Access(Accessor.Property);
                    m.Unique(false);
                    m.Lazy(LazyRelation.Proxy);
                    m.NotNullable(false);
                });

            Property(
                x => x.Despacho,
                map =>
                {
                    map.Column("DESPACHO");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            Property(
                x => x.Serie,
                map =>
                {
                    map.Column("SERIE");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            Property(
                x => x.DataSinistro,
                map =>
                {
                    map.Column("DATA_SINISTRO");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            Property(
                x => x.Laudo,
                map =>
                {
                    map.Column("LAUDO");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            Property(
                x => x.Terminal, 
                map =>
                {
                    map.Column("UD_ID_UD");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            Property(
                x => x.Perda,
                map =>
                {
                    map.Column("PERDA");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            ManyToOne(
                x => x.Causa, m =>
                {
                    m.Column("CS_ID_CS");
                    m.Class(typeof (CausaSeguro));
                    m.Cascade(Cascade.All);
                    m.Fetch(FetchKind.Select);
                    m.Update(true);
                    m.Insert(true);
                    m.Access(Accessor.Property);
                    m.Unique(false);
                    m.Lazy(LazyRelation.Proxy);
                    m.NotNullable(false);
                });

            Property(
                x => x.Gambit,
                map =>
                {
                    map.Column("GAMBIT");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            Property(
                x => x.Lacre,
                map =>
                {
                    map.Column("LACRE");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            Property(
                x => x.Vistoriador,
                map =>
                {
                    map.Column("VISTORIADOR");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            Property(
                x => x.Avaliacao,
                map =>
                {
                    map.Column("AVALIACAO");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            ManyToOne(
                x => x.Unidade, m =>
                {
                    m.Column("UNIDADE");
                    m.Class(typeof (UnidadeSeguro));
                    m.Cascade(Cascade.All);
                    m.Fetch(FetchKind.Select);
                    m.Update(true);
                    m.Insert(true);
                    m.Access(Accessor.Property);
                    m.Unique(false);
                    m.Lazy(LazyRelation.Proxy);
                    m.NotNullable(false);
                });

            Property(
                x => x.Rateio,
                map =>
                {
                    map.Column("RATEIO");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            Property(
                x => x.Historico,
                map =>
                {
                    map.Column("HISTORICO");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            Property(
                x => x.Nota,
                map =>
                {
                    map.Column("NOTA");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            Property(
                x => x.Cliente,
                map =>
                {
                    map.Column("CLIENTE");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            Property(
                x => x.Produto,
                map =>
                {
                    map.Column("PRODUTO");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            Property(
                x => x.Peso,
                map =>
                {
                    map.Column("PESO");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            Property(
                x => x.Tipo,
                map =>
                {
                    map.Column("TIPO");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            Property(
                x => x.CodFluxo,
                map =>
                {
                    map.Column("COD_FLUXO");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            Property(
                x => x.CodMercadoria,
                map =>
                {
                    map.Column("COD_MERCADORIA");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            Property(
                x => x.Destino,
                map =>
                {
                    map.Column("DESTINO");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            Property(
                x => x.DestinoFluxo,
                map =>
                {
                    map.Column("DESTINO_FLUXO");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            Property(
                x => x.Origem,
                map =>
                {
                    map.Column("ORIGEM");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            Property(
                x => x.OrigemFluxo,
                map =>
                {
                    map.Column("ORIGEM_FLUXO");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            ManyToOne(
                x => x.Vagao, m =>
                {
                    m.Column("VAGAO");
                    m.Class(typeof (Vagao));
                    m.Cascade(Cascade.All);
                    m.Fetch(FetchKind.Select);
                    m.Update(true);
                    m.Insert(true);
                    m.Access(Accessor.Property);
                    m.Unique(false);
                    m.Lazy(LazyRelation.Proxy);
                    m.NotNullable(false);
                });

            Property(
                x => x.ValorMercadoria,
                map =>
                {
                    map.Column("VALOR_MERCADORIA");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            Property(
                x => x.CodVagao,
                map =>
                {
                    map.Column("COD_VAGAO");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            Property(
                x => x.OrigemDesc,
                map =>
                {
                    map.Column("ORIGEM_DESC");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            Property(
                x => x.Piscofins,
                map =>
                {
                    map.Column("PISCOFINS");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            Property(
                x => x.Sindicancia,
                map =>
                {
                    map.Column("SINDICANCIA");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            ManyToOne(
                x => x.ContaContabil, m =>
                {
                    m.Column("CC_ID_CC");
                    m.Class(typeof(ContaContabilSeguro));
                    m.Cascade(Cascade.All);
                    m.Fetch(FetchKind.Select);
                    m.Update(true);
                    m.Insert(true);
                    m.Access(Accessor.Property);
                    m.Unique(false);
                    m.Lazy(LazyRelation.Proxy);
                    m.NotNullable(false);
                });

            ManyToOne(
                x => x.ClienteSeguro, m =>
                {
                    m.Column("CL_ID_CL");
                    m.Class(typeof(ClienteSeguro));
                    m.Cascade(Cascade.None);
                    m.Fetch(FetchKind.Select);
                    m.Update(true);
                    m.Insert(true);
                    m.Access(Accessor.Property);
                    m.Unique(false);
                    m.Lazy(LazyRelation.Proxy);
                    m.NotNullable(false);
                });
        }
    }
}