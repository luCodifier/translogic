﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Type;
using Translogic.Modules.Core.Domain.Model.Seguro;

namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.Seguro
{
	/// <summary>
	///   Mapeamento da classe <see cref="CausaSeguro" />
	/// </summary>
	public class RateioSeguroClassMapping : ClassMapping<RateioSeguro>
	{
		/// <summary>
		///   Initializes a new instance of the <see cref="RateioSeguroClassMapping" /> class.
		/// </summary>
		public RateioSeguroClassMapping()
		{
			Lazy(true);
			Table("RATEIO_SEGURO");
			Schema("TRANSLOGIC");

			Id(
				x => x.Id,
				m =>
				{
					m.Column("RS_ID_RS");
					m.Generator(
						Generators.Sequence,
						g => g.Params(new
						{
							sequence = "RATEIO_SEGURO_SEQ_ID"
						}));
					m.Type(new Int32Type());
					m.Access(Accessor.Property);
				});

			ManyToOne(
				x => x.UnidadeSeguro, m =>
				{
					m.Column("UD_ID_UD");
					m.Class(typeof(UnidadeSeguro));
					m.Cascade(Cascade.All);
					m.Fetch(FetchKind.Select);
					m.Update(true);
					m.Insert(true);
					m.Access(Accessor.Property);
					m.Unique(false);
					m.Lazy(LazyRelation.Proxy);
					m.NotNullable(false);
				});

			ManyToOne(
				x => x.ProcessoSeguro, m =>
				{
					m.Column("PC_ID_PC");
					m.Class(typeof(ProcessoSeguro));
					m.Cascade(Cascade.None);
					m.Fetch(FetchKind.Select);
					m.Update(true);
					m.Insert(true);
					m.Access(Accessor.Property);
					m.Unique(false);
					m.Lazy(LazyRelation.Proxy);
					m.NotNullable(false);
				});

			Property(
				x => x.PercentualRateio,
				map =>
				{
					map.Column("RS_PERC_RATEIO");
					map.Unique(false);
					map.NotNullable(false);
				});

			Property(
				x => x.CodUsuario,
				map =>
				{
					map.Column("US_USR_IDU");
					map.Unique(false);
					map.NotNullable(false);
				});

			Property(
				x => x.CentroCusto,
				map =>
				{
					map.Column("RS_CENT_CUSTO");
					map.Unique(false);
					map.NotNullable(false);
				});

			Property(
				x => x.Data,
				map =>
				{
					map.Column("RS_TIMESTAMP");
					map.Unique(false);
					map.NotNullable(false);
				});
		}
	}
}