﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Type;
using Translogic.Modules.Core.Domain.Model.Seguro;

namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.Seguro
{
    /// <summary>
    ///     Mapeamento da classe <see cref="ContaContabilSeguro" />
    /// </summary>
    public class ContaContabilSeguroClassMapping : ClassMapping<ContaContabilSeguro>
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="ContaContabilSeguroClassMapping" /> class.
        /// </summary>
        public ContaContabilSeguroClassMapping()
        {
            Lazy(true);
            Table("CONTA_CONTABIL_SEGURO");
            Schema("TRANSLOGIC");

            Id(
                x => x.Id,
                m =>
                {
                    m.Column("CC_ID_CC");
                    m.Generator(
                        Generators.Sequence,
                        g => g.Params(new
                        {
                            sequence = "CONTA_CONTABIL_SEGURO_SEQ_ID"
                        }));
                    m.Type(new Int32Type());
                    m.Access(Accessor.Property);
                });

            Property(
                x => x.Codigo,
                map =>
                {
                    map.Column("CC_CT_CTB");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            Property(
                x => x.Descricao,
                map =>
                {
                    map.Column("CC_DS_CTB");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            Property(
                x => x.IndicadorSituacao,
                map =>
                {
                    map.Column("CC_IND_ATV");
                    map.Unique(false);
                    map.NotNullable(false);
                });
        }
    }
}