﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Type;
using Translogic.Modules.Core.Domain.Model.Seguro;

namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.Seguro
{
	/// <summary>
	///   Mapeamento da classe <see cref="CausaSeguro" />
	/// </summary>
	public class CausaSeguroClassMapping : ClassMapping<CausaSeguro>
	{
		/// <summary>
		///   Initializes a new instance of the <see cref="CausaSeguroClassMapping" /> class.
		/// </summary>
		public CausaSeguroClassMapping()
		{
			Lazy(true);
			Table("CAUSA_SEGURO");
			Schema("TRANSLOGIC");

			Id(
				x => x.Id,
				m =>
				{
					m.Column("CS_ID_CS");
					m.Generator(
						Generators.Sequence,
						g => g.Params(new
						{
							sequence = "CAUSA_SEGURO_SEQ_ID"
						}));
					m.Type(new Int32Type());
					m.Access(Accessor.Property);
				});

			Property(
				x => x.Descricao,
				map =>
				{
					map.Column("CS_DSC_CAUSA");
					map.Unique(false);
					map.NotNullable(false);
				});

			Property(
				x => x.Tipo,
				map =>
				{
					map.Column("CS_TPO_CAUSA");
					map.Unique(false);
					map.NotNullable(false);
				});

			Property(
				x => x.Situacao,
				map =>
				{
					map.Column("CS_IND_SIT");
					map.Unique(false);
					map.NotNullable(false);
				});

            Property(
                x => x.TipoSinistro,
                map =>
                {
                    map.Column("CS_IND_SIN");
                    map.Unique(false);
                    map.NotNullable(false);
                });
		}
	}
}