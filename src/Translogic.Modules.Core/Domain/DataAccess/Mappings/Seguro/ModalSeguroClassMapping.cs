﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Type;
using Translogic.Modules.Core.Domain.Model.Seguro;
using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao;

namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.Seguro
{
	/// <summary>
	///   Mapeamento da classe <see cref="ModalSeguro" />
	/// </summary>
	public class ModalSeguroClassMapping : ClassMapping<ModalSeguro>
	{
		/// <summary>
		///   Initializes a new instance of the <see cref="ModalSeguroClassMapping" /> class.
		/// </summary>
		public ModalSeguroClassMapping()
		{
			Lazy(true);
			Table("MODAL_SEGURO");
			Schema("TRANSLOGIC");

			Id(
				x => x.Id,
				m =>
				{
					m.Column("MO_ID_MO");
					m.Generator(
						Generators.Sequence,
						g => g.Params(new
						{
							sequence = "MODAL_SEGURO_SEQ_ID"
						}));
					m.Type(new Int32Type());
					m.Access(Accessor.Property);
				});

			ManyToOne(
				x => x.Vagao, m =>
				{
					m.Column("VG_ID_VG");
					m.Class(typeof (Vagao));
					m.Cascade(Cascade.None);
					m.Fetch(FetchKind.Select);
					m.Update(true);
					m.Insert(true);
					m.Access(Accessor.Property);
					m.Unique(false);
					m.Lazy(LazyRelation.Proxy);
					m.NotNullable(false);
				});

			this.ManyToOne(
					x => x.ProcessoSeguro, m =>
					{
						m.Column("PC_ID_PC");
						m.Class(typeof(ProcessoSeguro));
						m.Cascade(Cascade.None);
						m.Fetch(FetchKind.Select);
						m.Update(true);
						m.Insert(true);
						m.Access(Accessor.Property);
						m.Unique(false);
						m.Lazy(LazyRelation.Proxy);
						m.NotNullable(false);
					});

			this.ManyToOne(
					x => x.CausaSeguro, m =>
					{
						m.Column("CS_ID_CS");
						m.Class(typeof(CausaSeguro));
						m.Cascade(Cascade.None);
						m.Fetch(FetchKind.Select);
						m.Update(true);
						m.Insert(true);
						m.Access(Accessor.Property);
						m.Unique(false);
						m.Lazy(LazyRelation.Proxy);
						m.NotNullable(false);
					});

			Property(
				x => x.Origem,
				map =>
				{
					map.Column("AO_ID_ORG");
					map.Unique(false);
					map.NotNullable(false);
				});

			Property(
				x => x.Destino,
				map =>
				{
					map.Column("AO_ID_DST");
					map.Unique(false);
					map.NotNullable(false);
				});

            Property(
                x => x.Terminal,
                map =>
                {
                    map.Column("AO_ID_TRM");
                    map.Unique(false);
                    map.NotNullable(false);
                });

			Property(
				x => x.QuantidadePerda,
				map =>
				{
					map.Column("MO_QTD_PERDA");
					map.Unique(false);
					map.NotNullable(false);
				});

			Property(
				x => x.Gambit,
				map =>
				{
					map.Column("MO_IND_GAMB");
					map.Unique(false);
					map.NotNullable(false);
				});

			Property(
				x => x.CodFluxo,
				map =>
				{
					map.Column("FX_COD_FLX");
					map.Unique(false);
					map.NotNullable(false);
				});

			Property(
				x => x.CodFluxo,
				map =>
				{
					map.Column("FX_COD_FLX");
					map.Unique(false);
					map.NotNullable(false);
				});

			Property(
				x => x.CodMercadoria,
				map =>
				{
					map.Column("MC_COD_MRC");
					map.Unique(false);
					map.NotNullable(false);
				});

			Property(
				x => x.OrigemFluxo,
				map =>
				{
					map.Column("AO_ID_ORG_FLX");
					map.Unique(false);
					map.NotNullable(false);
				});

			Property(
				x => x.DestinoFluxo,
				map =>
				{
					map.Column("AO_ID_DST_FLX");
					map.Unique(false);
					map.NotNullable(false);
				});

			Property(
				x => x.Usuario,
				map =>
				{
					map.Column("US_USR_IDU");
					map.Unique(false);
					map.NotNullable(false);
				});

			Property(
				x => x.Termo,
				map =>
				{
					map.Column("MO_NUM_FALTA");
					map.Unique(false);
					map.NotNullable(false);
				});

			Property(
				x => x.DataTermo,
				map =>
				{
					map.Column("MO_DT_FALTA");
					map.Unique(false);
					map.NotNullable(false);
				});

			Property(
				x => x.ResponsavelTermo,
				map =>
				{
					map.Column("MO_RESP_FALTA");
					map.Unique(false);
					map.NotNullable(false);
				});

			Property(
				x => x.ValorMercadoria,
				map =>
				{
					map.Column("MO_VLR_MERC");
					map.Unique(false);
					map.NotNullable(false);
				});

			Property(
				x => x.NotaFiscal,
				map =>
				{
					map.Column("MO_NOTA_FISCAL");
					map.Unique(false);
					map.NotNullable(false);
				});

			Property(
				x => x.Data,
				map =>
				{
					map.Column("MO_TIMESTAMP");
					map.Unique(false);
					map.NotNullable(false);
				});

			Property(
				x => x.Serie,
				map =>
				{
					map.Column("SERIE_DESP");
					map.Unique(false);
					map.NotNullable(false);
				});

			Property(
				x => x.Despacho,
				map =>
				{
					map.Column("NRO_DESP");
					map.Unique(false);
					map.NotNullable(false);
				});

			Property(
				x => x.Lacre,
				map =>
				{
					map.Column("LACRE");
					map.Unique(false);
					map.NotNullable(false);
				});

			Property(
				x => x.Historico,
				map =>
				{
					map.Column("HISTORICO");
					map.Unique(false);
					map.NotNullable(false);
				});

            Property(
                x => x.Piscofins,
                map =>
                {
                    map.Column("PISCOFINS");
                    map.Unique(false);
                    map.NotNullable(false);
                });
		}
	}
}