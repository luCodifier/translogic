﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Type;
using Translogic.Modules.Core.Domain.Model.Seguro;

namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.Seguro
{
	/// <summary>
	///   Mapeamento da classe <see cref="ProcessoSeguro" />
	/// </summary>
	public class ProcessoSeguroClassMapping : ClassMapping<ProcessoSeguro>
	{
		/// <summary>
		///   Initializes a new instance of the <see cref="ProcessoSeguroClassMapping" /> class.
		/// </summary>
		public ProcessoSeguroClassMapping()
		{
			Lazy(true);
			Table("PROCESSO_SEGURO");
			Schema("TRANSLOGIC");

			Id(
				x => x.Id,
				m =>
				{
					m.Column("PC_ID_PC");
					m.Generator(
						Generators.Sequence,
						g => g.Params(new
						{
							sequence = "PROCESSO_SEGURO_SEQ_ID"
						}));
					m.Type(new Int32Type());
					m.Access(Accessor.Property);
				});

			Property(
				x => x.NumeroProcesso,
				map =>
				{
					map.Column("PC_NUM_PROC");
					map.Unique(false);
					map.NotNullable(false);
				});

			Property(
				x => x.DataProcesso,
				map =>
				{
					map.Column("PC_DT_PROC");
					map.Unique(false);
					map.NotNullable(false);
				});

			Property(
				x => x.Vistoriador,
				map =>
				{
					map.Column("PC_VISTORIADOR");
					map.Unique(false);
					map.NotNullable(false);
				});

			Property(
				x => x.DataVistoria,
				map =>
				{
					map.Column("PC_DT_VISTORIA");
					map.Unique(false);
					map.NotNullable(false);
				});

			Property(
				x => x.UsuarioCadastro,
				map =>
				{
					map.Column("PC_USR_CADASTRO");
					map.Unique(false);
					map.NotNullable(false);
				});

			Property(
				x => x.DataCadastro,
				map =>
				{
					map.Column("PC_DT_CADASTRO");
					map.Unique(false);
					map.NotNullable(false);
				});

			Property(
				x => x.Data,
				map =>
				{
					map.Column("PC_TIMESTAMP");
					map.Unique(false);
					map.NotNullable(false);
				});

			Property(
				x => x.DataSinistro,
				map =>
				{
					map.Column("PC_DT_SINISTRO");
					map.Unique(false);
					map.NotNullable(false);
				});

			Property(
				x => x.IndicadorIndevido,
				map =>
				{
					map.Column("PC_IND_INDEVIDO");
					map.Unique(false);
					map.NotNullable(false);
				});

			Property(
				x => x.IndicadorSituacao,
				map =>
				{
					map.Column("PC_IND_SIT");
					map.Unique(false);
					map.NotNullable(false);
				});

            Property(
                x => x.Sindicancia,
                map =>
                {
                    map.Column("PC_SINDICANCIA");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            ManyToOne(
                x => x.ContaContabil, m =>
                {
                    m.Column("CC_ID_CC");
                    m.Class(typeof(ContaContabilSeguro));
                    m.Cascade(Cascade.None);
                    m.Fetch(FetchKind.Select);
                    m.Update(true);
                    m.Insert(true);
                    m.Access(Accessor.Property);
                    m.Unique(false);
                    m.Lazy(LazyRelation.Proxy);
                    m.NotNullable(false);
                });

            ManyToOne(
                x => x.ClienteSeguro, m =>
                {
                    m.Column("CL_ID_CL");
                    m.Class(typeof(ClienteSeguro));
                    m.Cascade(Cascade.None);
                    m.Fetch(FetchKind.Select);
                    m.Update(true);
                    m.Insert(true);
                    m.Access(Accessor.Property);
                    m.Unique(false);
                    m.Lazy(LazyRelation.NoLazy);
                    m.NotNullable(false);
                });
		}
	}
}