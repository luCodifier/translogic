﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Type;
using Translogic.Modules.Core.Domain.Model.Seguro;

namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.Seguro
{
    /// <summary>
    ///     Mapeamento da classe <see cref="ClienteSeguro" />
    /// </summary>
    public class ClienteSeguroClassMapping : ClassMapping<ClienteSeguro>
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="ClienteSeguroClassMapping" /> class.
        /// </summary>
        public ClienteSeguroClassMapping()
        {
            Lazy(true);
            Table("CLIENTE_SEGURO");
            Schema("TRANSLOGIC");

            Id(
                x => x.Id,
                m =>
                {
                    m.Column("CL_ID_CL");
                    m.Generator(
                        Generators.Sequence,
                        g => g.Params(new
                        {
                            sequence = "CLIENTE_SEGURO_SEQ_ID"
                        }));
                    m.Type(new Int32Type());
                    m.Access(Accessor.Property);
                });

            Property(
                x => x.Descricao,
                map =>
                {
                    map.Column("CL_DSC_CLS");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            Property(
                x => x.Banco,
                map =>
                {
                    map.Column("CL_BAN_CLS");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            Property(
                x => x.Agencia,
                map =>
                {
                    map.Column("CL_AGE_CLS");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            Property(
                x => x.ContaCorrente,
                map =>
                {
                    map.Column("CL_CNT_CLS");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            Property(
                x => x.Contato,
                map =>
                {
                    map.Column("CL_CTO_CLS");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            Property(
                x => x.Telefone,
                map =>
                {
                    map.Column("CL_TEL_CLS");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            Property(
                x => x.Email,
                map =>
                {
                    map.Column("CL_EMAIL_CLS");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            Property(
                x => x.Data,
                map =>
                {
                    map.Column("CL_TIMESTAMP");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            Property(
                x => x.Usuario,
                map =>
                {
                    map.Column("US_USR_IDU");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            Property(
                x => x.CodSap,
                map =>
                {
                    map.Column("CL_COD_SAP");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            Property(
                x => x.SiglaEmpresa,
                map =>
                {
                    map.Column("CL_SIG_EMP");
                    map.Unique(false);
                    map.NotNullable(false);
                });
        }
    }
}