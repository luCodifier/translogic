﻿namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.PainelExpedicao
{
    using ALL.Core.AcessoDados.TiposCustomizados;

    using NHibernate.Mapping.ByCode;
    using NHibernate.Mapping.ByCode.Conformist;
    using NHibernate.Type;

    using Translogic.Modules.Core.Domain.Model.PainelExpedicao.Parametrizacao;
    using Translogic.Modules.Core.Domain.Model.PainelExpedicao;

    /// <summary>
    /// Classe de mapeamento da entidade <see cref="RegraEmailsClassMapping"/>
    /// </summary>
    public class RegraEmailsClassMapping : ClassMapping<RegraEmails>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RegraEmailsClassMapping"/> class.
        /// </summary>
        public RegraEmailsClassMapping()
        {
            this.Lazy(true);
            this.Table("PE_REGRA_EMAILS");
            this.Schema("TRANSLOGIC");

            this.Id(
                x => x.Id,
                m =>
                {
                    m.Column("ID");
                    m.Generator(Generators.Sequence, g => g.Params(new { sequence = "PE_REGRA_EMAILS_SEQ_ID" }));
                    m.Type(new Int32Type());
                    m.Access(Accessor.Property);
                });

            this.Property(x => x.Nome, map => { map.Column("NOME"); map.NotNullable(true); map.Length(100); });
            this.Property(x => x.RegiaoId, map => { map.Column("REGIAO_ID"); map.NotNullable(true); });

            this.Property(x => x.Destinatarios, map => { map.Column("DESTINATARIOS"); map.NotNullable(true); map.Length(2000); });
            this.Property(x => x.TituloEmail, map => { map.Column("TITULO_EMAIL"); map.NotNullable(true); map.Length(100); });
            this.Property(x => x.CorpoEmail, map => { map.Column("CORPO_EMAIL"); map.NotNullable(true); map.Length(1000); });
            this.Property(x => x.DataCadastro, map => { map.Column("DATA_CADASTRO"); map.NotNullable(true); map.Length(7); });
            this.Property(x => x.UsuarioCadastro, map => { map.Column("USUARIO_CADASTRO"); map.NotNullable(true); map.Length(22); });
            this.Property(x => x.DataAlteracao, map => { map.Column("DATA_ALTERACAO"); map.Length(7); });
            this.Property(x => x.UsuarioAlteracao, map => { map.Column("USUARIO_ALTERACAO"); map.Length(22); });
        }
    }
}