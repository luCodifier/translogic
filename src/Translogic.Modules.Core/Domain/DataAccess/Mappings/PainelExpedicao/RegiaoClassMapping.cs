﻿namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.PainelExpedicao
{
    using NHibernate.Mapping.ByCode;
    using NHibernate.Mapping.ByCode.Conformist;
    using NHibernate.Type;

    using Translogic.Modules.Core.Domain.Model.PainelExpedicao;

    /// <summary>
    /// Classe de mapeamento da entidade <see cref="Regiao"/>
    /// </summary>
    public class RegiaoClassMapping : ClassMapping<Regiao>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RegiaoClassMapping"/> class.
        /// </summary>
        public RegiaoClassMapping()
        {
            this.Lazy(true);
            this.Table("PE_REGIAO");
            this.Schema("TRANSLOGIC");

            this.Id(
                x => x.Id,
                m =>
                {
                    m.Column("ID");
                    m.Generator(Generators.Sequence, g => g.Params(new { sequence = "PE_REGIAO_SEQ_ID" }));
                    m.Type(new Int32Type());
                    m.Access(Accessor.Property);
                });

            this.Property(x => x.Nome, map => { map.Column("NOME"); map.NotNullable(true); map.Length(50); });
            this.Property(x => x.OperacaoId, map => { map.Column("OPERACAO_ID"); map.NotNullable(true); });


            this.Property(x => x.DataCadastro, map => { map.Column("DATA_CADASTRO"); map.NotNullable(true); map.Length(7); });
            this.Property(x => x.UsuarioCadastro, map => { map.Column("USUARIO_CADASTRO"); map.NotNullable(true); map.Length(22); });
            this.Property(x => x.DataAlteracao, map => { map.Column("DATA_ALTERACAO"); map.Length(7); });
            this.Property(x => x.UsuarioAlteracao, map => { map.Column("USUARIO_ALTERACAO"); map.Length(22); });


        }
    }
}