﻿namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.PainelExpedicao
{
	using ALL.Core.AcessoDados.TiposCustomizados;

	using NHibernate.Mapping.ByCode;
	using NHibernate.Mapping.ByCode.Conformist;
	using NHibernate.Type;

	using Translogic.Modules.Core.Domain.Model.PainelExpedicao;

	/// <summary>
	/// Classe de mapeamento da entidade <see cref="OperacaoMalha"/>
	/// </summary>
	public class PedraRealizadoClassMapping : ClassMapping<PedraRealizado>
	{
		/// <summary>
        /// Initializes a new instance of the <see cref="OperacaoMalhaClassMapping"/> class.
		/// </summary>
        public PedraRealizadoClassMapping()
		{
			this.Lazy(true);
			this.Table("PE_PEDRA_REALIZADO");
			this.Schema("TRANSLOGIC");

			this.Id(
				x => x.Id,
				m =>
				{
					m.Column("ID");
                    m.Generator(Generators.Sequence, g => g.Params(new { sequence = "PE_PEDRA_REALIZADO_SEQ_ID" }));
					m.Type(new Int32Type());
					m.Access(Accessor.Property);
				});

            this.Property(x => x.DataPedra, map => { map.Column("DATA_PEDRA"); map.NotNullable(true); map.Length(7); });
            this.Property(x => x.FluxoId, map => { map.Column("FLUXO_ID"); map.NotNullable(true); map.Length(22); });
            this.Property(x => x.ReuniaoId, map => { map.Column("REUNIAO_ID"); map.NotNullable(true); map.Length(22); });
            this.Property(x => x.ReuniaoDescricao, map => { map.Column("REUNIAO_DESCRICAO"); map.NotNullable(true); map.Length(22); });
            this.Property(x => x.EstacaoOrigem, map => { map.Column("ESTACAO_ORIGEM"); map.NotNullable(true); map.Length(22); });
            this.Property(x => x.TerminalId, map => { map.Column("TERMINAL_ID"); map.NotNullable(true); map.Length(22); });
            this.Property(x => x.Segmento, map => { map.Column("SEGMENTO"); map.Length(200); });
            this.Property(x => x.PedraSade, map => { map.Column("PEDRA_SADE"); map.Length(22); });
            this.Property(x => x.Realizado, map => { map.Column("REALIZADO"); map.Length(22); });
            this.Property(x => x.PedraEditado, map => { map.Column("PEDRA_EDITADO"); map.Length(22); });
            this.Property(x => x.StatusSade, map => { map.Column("STATUS_SADE"); map.Length(500); });
            this.Property(x => x.StatusEditado, map => { map.Column("STATUS_EDITADO"); map.Length(500); });
            this.Property(x => x.DataCadastro, map => { map.Column("DATA_CADASTRO"); map.NotNullable(true); map.Length(7); });
            this.Property(x => x.UsuarioCadastro, map => { map.Column("USUARIO_CADASTRO"); map.NotNullable(true); map.Length(22); });
            this.Property(x => x.DataAlteracao, map => { map.Column("DATA_ALTERACAO"); map.Length(7); });
            this.Property(x => x.UsuarioAlteracao, map => { map.Column("USUARIO_ALTERACAO"); map.Length(22); });

        }
    }
}