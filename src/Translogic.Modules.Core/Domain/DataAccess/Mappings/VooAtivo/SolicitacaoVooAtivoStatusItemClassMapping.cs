﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Type;
using Translogic.Modules.Core.Domain.Model.VooAtivo;

namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.VooAtivo
{
    /// <summary>
    ///     Mapeamento da classe <see cref="SolicitacaoVooAtivoStatusItem" />
    /// </summary>
    public class SolicitacaoVooAtivoStatusItemClassMapping : ClassMapping<SolicitacaoVooAtivoStatusItem>
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="SolicitacaoVooAtivoStatusItemClassMapping" /> class.
        /// </summary>
        public SolicitacaoVooAtivoStatusItemClassMapping()
        {
            Lazy(true);
            Table("SOL_VOO_ATIVOS_STATUS_ITEM");
            Schema("TRANSLOGIC");

            Id(
                x => x.Id,
                m =>
                {
                    m.Column("ID_STATUS_SOL_ITEM_VOO");
                    m.Type(new Int32Type());
                    m.Access(Accessor.Property);
                });

            Property(
                x => x.Descricao,
                map =>
                {
                    map.Column("STATUS_SOL_ITEM_VOO");
                    map.Unique(false);
                    map.NotNullable(false);

                });

        }
    }
}