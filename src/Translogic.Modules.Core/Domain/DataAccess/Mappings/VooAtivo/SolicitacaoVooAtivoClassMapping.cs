﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Type;
using Translogic.Modules.Core.Domain.Model.VooAtivo;

namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.VooAtivo
{
    /// <summary>
    ///     Mapeamento da classe <see cref="ProcessoSeguroCache" />
    /// </summary>
    public class SolicitacaoVooAtivoClassMapping : ClassMapping<SolicitacaoVooAtivo>
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="ProcessoSeguroCacheClassMapping" /> class.
        /// </summary>
        public SolicitacaoVooAtivoClassMapping()
        {
            Lazy(true);
            Table("SOL_VOO_ATIVOS");
            Schema("TRANSLOGIC");

            Id(
                x => x.Id,
                m =>
                {
                    m.Column("ID_SOL_VOO");
                    m.Generator(
                        Generators.Sequence,
                        g => g.Params(new
                        {
                            sequence = "SOL_VOO_ATIVOS_SEQ"
                        }));
                    m.Type(new Int32Type());
                    m.Access(Accessor.Property);
                });

            Property(
                x => x.Justificativa,
                map =>
                {
                    map.Column("JUSTIFICATIVA");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            Property(
                x => x.Motivo,
                map =>
                {
                    map.Column("MOTIVO");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            Property(
                x => x.PatioDestino,
                map =>
                {
                    map.Column("PATIO_DESTINO");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            Property(
              x => x.LinhaDestino,
              map =>
              {
                  map.Column("LINHA_DESTINO");
                  map.Unique(false);
                  map.NotNullable(false);
              });

            Property(
                x => x.Solicitante,
                map =>
                {
                    map.Column("SOLICITANTE");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            Property(
             x => x.PatioSolicitante,
             map =>
             {
                 map.Column("PATIO_SOLICITANTE");
                 map.Unique(false);
                 map.NotNullable(false);
             });

            Property(
                x => x.DataHoraChegada,
                map =>
                {
                    map.Column("DT_CHEGADA");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            Property(
                x => x.AreaResponsavel,
                map =>
                {
                    map.Column("AREA_RESP");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            Property(
                x => x.JustificativaCCO,
                map =>
                {
                    map.Column("JUSTIFICATIVA_CCO");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            Property(
                x => x.ObservacaoCCO,
                map =>
                {
                    map.Column("OBS_CCO");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            Property(
                x => x.DataSolicitacao,
                map =>
                {
                    map.Column("DT_SOLICITACAO");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            Property(
                x => x.DataUltimaAlteracao,
                map =>
                {
                    map.Column("DT_ULT_ALT");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            Property(
             x => x.Usuario,
             map =>
             {
                 map.Column("USUARIO");
                 map.Unique(false);
                 map.NotNullable(false);
             });

            Property(
             x => x.VersionDate,
             map =>
             {
                 map.Column("OLV_TIMESTAMP");
                 map.Unique(false);
                 map.NotNullable(false);
             });

            ManyToOne(
                x => x.SolicitacaoVooAtivoStatus, m =>
                {
                    m.Column("ID_SOL_VOO_STATUS");
                    m.Class(typeof (SolicitacaoVooAtivoStatus));
                    m.Cascade(Cascade.All);
                    m.Fetch(FetchKind.Select);
                    m.Update(true);
                    m.Insert(true);
                    m.Access(Accessor.Property);
                    m.Unique(false);
                    m.Lazy(LazyRelation.Proxy);
                    m.NotNullable(false);
                });

            ManyToOne(
                x => x.SolicitacaoTipoAtivo, m =>
                {
                    m.Column("ID_TIPO_ATIVO");
                    m.Class(typeof (SolicitacaoTipoAtivo));
                    m.Cascade(Cascade.All);
                    m.Fetch(FetchKind.Select);
                    m.Update(true);
                    m.Insert(true);
                    m.Access(Accessor.Property);
                    m.Unique(false);
                    m.Lazy(LazyRelation.Proxy);
                    m.NotNullable(false);
                });
        }
    }
}