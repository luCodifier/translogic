﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Type;
using Translogic.Modules.Core.Domain.Model.VooAtivo;

namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.Seguro
{
    /// <summary>
    ///     Mapeamento da classe <see cref="SolicitacaoVooAtivoItem" />
    /// </summary>
    public class SolicitacaoVooAtivoItemClassMapping : ClassMapping<SolicitacaoVooAtivoItem>
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="SolicitacaoVooAtivoItemClassMapping" /> class.
        /// </summary>
        public SolicitacaoVooAtivoItemClassMapping()
        {
            Lazy(true);
            Table("SOL_VOO_ATIVOS_ITEM");
            Schema("TRANSLOGIC");

            Id(
                 x => x.Id,
                 m =>
                 {
                     m.Column("ID_SOL_VOO_ITEM");
                     m.Generator(
                         Generators.Sequence,
                         g => g.Params(new
                         {
                             sequence = "SOL_VOO_ATIVOS_ITEM_SEQ"
                         }));
                     m.Type(new Int32Type());
                     m.Access(Accessor.Property);
                 });

            Property(
            x => x.IdAtivo,
            map =>
            {
                map.Column("ID_ATIVO");
                map.Unique(false);
                map.NotNullable(false);
            });

            Property(
              x => x.Ativo,
              map =>
              {
                  map.Column("ATIVO");
                  map.Unique(false);
                  map.NotNullable(false);
              });

            Property(
             x => x.DataEvento,
             map =>
             {
                 map.Column("DT_EVENTO");
                 map.Unique(false);
                 map.NotNullable(false);
             });

            Property(
                x => x.PatioOrigem,
                map =>
                {
                    map.Column("PATIO_ORIGEM");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            Property(
                x => x.PatioDestino,
                map =>
                {
                    map.Column("PATIO_DESTINO");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            Property(
                x => x.CondicaoUso,
                map =>
                {
                    map.Column("CONDICOES_USO");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            Property(
                x => x.Situacao,
                map =>
                {
                    map.Column("SITUACAO");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            Property(
                x => x.Local,
                map =>
                {
                    map.Column("LOCAL");
                    map.Unique(false);
                    map.NotNullable(false);
                });

            Property(
             x => x.Responsavel,
             map =>
             {
                 map.Column("RESPONSAVEL");
                 map.Unique(false);
                 map.NotNullable(false);
             });

            Property(
              x => x.Lotacao,
              map =>
              {
                  map.Column("LOTACAO");
                  map.Unique(false);
                  map.NotNullable(false);
              });

            Property(
             x => x.Intercambio,
             map =>
             {
                 map.Column("INTERCAMBIO");
                 map.Unique(false);
                 map.NotNullable(false);
             });

            Property(
               x => x.EstadoFuturo,
               map =>
               {
                   map.Column("ESTADO_FUTURO");
                   map.Unique(false);
                   map.NotNullable(false);
               });

            ManyToOne(
                x => x.SolicitacaoVooAtivoStatusItem, m =>
                {
                    m.Column("ID_SOL_VOO_STATUS_ITEM");
                    m.Class(typeof (SolicitacaoVooAtivoStatusItem));
                    m.Cascade(Cascade.All);
                    m.Fetch(FetchKind.Select);
                    m.Update(true);
                    m.Insert(true);
                    m.Access(Accessor.Property);
                    m.Unique(false);
                    m.Lazy(LazyRelation.Proxy);
                    m.NotNullable(false);
                });

            ManyToOne(
                x => x.SolicitacaoVooAtivo, m =>
                {
                    m.Column("ID_SOL_VOO_ATIVOS");
                    m.Class(typeof (SolicitacaoVooAtivo));
                    m.Cascade(Cascade.All);
                    m.Fetch(FetchKind.Select);
                    m.Update(true);
                    m.Insert(true);
                    m.Access(Accessor.Property);
                    m.Unique(false);
                    m.Lazy(LazyRelation.Proxy);
                    m.NotNullable(false);
                });

             ManyToOne(
                x => x.SolicitacaoTipoAtivo, m =>
                {
                    m.Column("ID_TIPO_ATIVO");
                    m.Class(typeof (SolicitacaoTipoAtivo));
                    m.Cascade(Cascade.All);
                    m.Fetch(FetchKind.Select);
                    m.Update(true);
                    m.Insert(true);
                    m.Access(Accessor.Property);
                    m.Unique(false);
                    m.Lazy(LazyRelation.Proxy);
                    m.NotNullable(false);
                });

             //Property(
             //   x => x.NumSeq,
             //   map =>
             //   {
             //       map.Column("NUM_SEQ");
             //       map.Unique(false);
             //       map.NotNullable(false);
             //   });
        }
    }
}