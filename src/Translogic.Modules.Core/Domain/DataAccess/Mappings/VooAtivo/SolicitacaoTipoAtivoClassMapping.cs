﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Type;
using Translogic.Modules.Core.Domain.Model.VooAtivo;

namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.VooAtivo
{
    /// <summary>
    ///     Mapeamento da classe <see cref="SolicitacaoTipoAtivo" />
    /// </summary>
    public class SolicitacaoTipoAtivoClassMapping : ClassMapping<SolicitacaoTipoAtivo>
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="SolicitacaoTipoAtivoClassMapping" /> class.
        /// </summary>
        public SolicitacaoTipoAtivoClassMapping()
        {
            Lazy(true);
            Table("SOL_TIPO_ATIVO");
            Schema("TRANSLOGIC");

            Id(
                 x => x.Id,
                 m =>
                 {
                     m.Column("ID_TIPO_ATIVO");
                     m.Generator(
                         Generators.Sequence,
                         g => g.Params(new
                         {
                             sequence = "SOL_TIPO_ATIVO_SEQ"
                         }));
                     m.Type(new Int32Type());
                     m.Access(Accessor.Property);
                 });

            Property(
                x => x.Descricao,
                map =>
                {
                    map.Column("TIPO_ATIVO");
                    map.Unique(false);
                    map.NotNullable(false);
                });
        }
    }
}