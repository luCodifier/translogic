﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Type;
using Translogic.Modules.Core.Domain.Model.VooAtivo;

namespace Translogic.Modules.Core.Domain.DataAccess.Mappings.Seguro
{
    /// <summary>
    ///     Mapeamento da classe <see cref="SolicitacaoVooAtivoStatus" />
    /// </summary>
    public class SolicitaoVooAtivoStatusClassMapping : ClassMapping<SolicitacaoVooAtivoStatus>
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="SolicitaoVooAtivoStatusClassMapping" /> class.
        /// </summary>
        public SolicitaoVooAtivoStatusClassMapping()
        {
            Lazy(true);
            Table("SOL_VOO_ATIVOS_STATUS");
            Schema("TRANSLOGIC");

            Id(
                x => x.Id,
                m =>
                {
                    m.Column("ID_STATUS_SOL_VOO");
                    m.Generator(
                        Generators.Sequence,
                        g => g.Params(new
                        {
                            sequence = "SOL_VOO_ATIVOS_STATUS_SEQ"
                        }));
                    m.Type(new Int32Type());
                    m.Access(Accessor.Property);
                });

            Property(
                x => x.Descricao,
                map =>
                {
                    map.Column("STATUS_SOL_VOO");
                    map.Unique(false);
                    map.NotNullable(false);
                });
        }
    }
}