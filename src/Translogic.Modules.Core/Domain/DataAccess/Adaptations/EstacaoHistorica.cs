namespace Translogic.Modules.Core.Domain.DataAccess.Adaptations
{
	using Model.CadastroUnicoEstacoes;
	using Model.Diversos;

	/// <summary>
	/// Classe de dados hist�ricos da esta��o
	/// </summary>
	public class EstacaoHistorica : Estacao
	{
		#region PROPRIEDADES

		/// <summary>
		/// Tipo de opera��o efetuada
		/// </summary>
		public TipoOperacaoEnum Tipo { get; set; }

		#endregion
	}
}