namespace Translogic.Modules.Core.Domain.DataAccess.Adaptations
{
	using System;
	using Model.QuadroEstados;
	using Model.Trem.Veiculo.Vagao;

	/// <summary>
	/// Classe utilitaria para mapear a view que contem o relacionamento do vagao com o quadro de estado 
	/// </summary>
	/// <remarks>
	/// N�o herda de base entity pois � uma composite-id ao inv�s de id simples
	/// </remarks>
	[Serializable]
	public class VagaoQuadroEstadoVigente
	{
		#region PROPRIEDADES

		/// <summary>
		/// Quadro de Estado do Vagao. <see cref="QuadroEstadoVagao"/>
		/// </summary>
		public virtual QuadroEstadoVagao QuadroEstadoVagao { get; set; }

		/// <summary>
		/// Vagao do quadro de estados.
		/// </summary>
		public virtual Vagao Vagao { get; set; }

		#endregion

		#region M�TODOS
		/// <summary>
		/// Sobreescreve o equals
		/// </summary>
		/// <param name="obj">objeto a ser comparado</param>
		/// <returns>Valor booleano</returns>
		public override bool Equals(object obj)
		{
			return base.Equals(obj);
		}

		/// <summary>
		/// Sobreescreve o metodo gethashcode
		/// </summary>
		/// <returns>Hashcode - valor inteiro</returns>
		public override int GetHashCode()
		{
			return base.GetHashCode();
		}

		#endregion
	}
}