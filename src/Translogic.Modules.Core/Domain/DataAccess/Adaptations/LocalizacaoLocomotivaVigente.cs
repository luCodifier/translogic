namespace Translogic.Modules.Core.Domain.DataAccess.Adaptations
{
	using Model.Trem.Veiculo.Locomotiva.QuadroEstado;

	/// <summary>
	/// Classe de adaptação para pegar dados vigentes de localização de locomotiva
	/// </summary>
	public class LocalizacaoLocomotivaVigente : LocalizacaoLocomotiva
	{
	}
}