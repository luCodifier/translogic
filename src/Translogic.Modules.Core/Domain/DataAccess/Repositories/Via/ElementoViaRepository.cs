﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Via
{
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;
    using Model.Via;
    using Model.Via.Repositories;
    using NHibernate;
    using NHibernate.Criterion;
    using NHibernate.SqlCommand;
    using System.Text;
    using NHibernate.Transform;
    using Translogic.Modules.Core.Domain.Model.Dto;

    /// <summary>
    /// Classe Repositório do Trecho Tkb
    /// </summary>
    public class ElementoViaRepository : NHRepository<ElementoVia, int>, IElementoViaRepository
    {
        /// <summary>
        /// Obtém as linhas da areaOperacional
        /// </summary>
        /// <param name="idAreaOperacional"> Id da AreaOperacional da linha. </param>
        /// <returns> Lista de Objetos do tipo <see cref="ElementoVia"/> </returns>
        public IList<ElementoVia> ObterPorIdEstacaoMae(int idAreaOperacional)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.CreateAlias("AreaOperacional", "ao", JoinType.InnerJoin);
            criteria.Add(Restrictions.Eq("ao.EstacaoMae.Id", idAreaOperacional));
            criteria.SetFetchMode("ao", FetchMode.Eager);
            return ObterTodos(criteria);
        }

        public IList<ElementoViaDto> ObterPorPatio(string patio)
        {
            var sql = new StringBuilder(@"SELECT 
              DISTINCT 
              EV.EV_ID_ELV as ""Id"",
              EV.EV_COD_ELV as ""Descricao""              
            FROM ELEMENTO_VIA EV
            INNER JOIN AREA_OPERACIONAL AOP ON (EV.AO_ID_AO = AOP.AO_ID_AO)
            INNER JOIN AREA_OPERACIONAL AOM ON (AOP.AO_ID_AO_INF = AOM.AO_ID_AO)
            WHERE 
              AOM.AO_COD_AOP = '" + patio + "'" +
            "ORDER BY EV.EV_COD_ELV");

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());

                query.SetResultTransformer(Transformers.AliasToBean<ElementoViaDto>());

                var result = query.List<ElementoViaDto>();

                return result;
            }
        }

    }
}
