﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Via
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using ALL.Core.AcessoDados;
    using Model.Estrutura;
    using Model.Via;
    using Model.Via.Repositories;
    using NHibernate.Criterion;

    /// <summary>
    /// Classe Repositório do Trecho Tkb
    /// </summary>
    public class TrechoTkbRepository : NHRepository<TrechoTkb, int?>, ITrechoTkbRepository
    {
        /// <summary>
        /// Método que retorna a lista de trechos por parametros
        /// </summary>
        /// <param name="trechoTkb">Parametro Trecho tkb</param>
        /// <param name="trechoDiesel">Parametro Trecho Diesel</param>
        /// <param name="corredor">Parametro corredor</param>
        /// <param name="up">Paremetro UP</param>
        /// <param name="idUp">Id da Up (Classe)</param>
        /// <returns>Retorna lista de TrechoTkb</returns>
        public IList<TrechoTkb> ObterTodosPorParametro(string trechoTkb, string trechoDiesel, string corredor, string up, int? idUp)
        {
            DetachedCriteria criteria = CriarCriteria();
            if (!String.IsNullOrEmpty(trechoTkb))
            {
                criteria.Add(Restrictions.InsensitiveLike("Trecho", trechoTkb + "%"));
            }

            if (!String.IsNullOrEmpty(trechoDiesel))
            {
                criteria.Add(Restrictions.InsensitiveLike("TrechoDiesel", trechoDiesel + "%"));    
            }
            
            if (!String.IsNullOrEmpty(corredor))
            {
                criteria.Add(Restrictions.InsensitiveLike("Corredor", corredor + "%"));    
            }

            if (idUp != null)
            {
                criteria.CreateAlias("UnidadeProducao", "up");
                criteria.Add(Restrictions.Eq("up.id", idUp));
            }
            
            return ObterTodos(criteria);
        }

    	/// <summary>
    	/// Obtém o trecho Tkb por trecho diesel
    	/// </summary>
    	/// <param name="trechoDiesel"> The trecho diesel. </param>
    	/// <returns> Objeto do tipo <see cref="TrechoTkb"/> </returns>
    	public TrechoTkb ObterPorTrechoDiesel(string trechoDiesel)
    	{
    		DetachedCriteria criteria = CriarCriteria();
    		criteria.Add(Restrictions.Eq("TrechoDiesel", trechoDiesel));
    		return ObterPrimeiro(criteria);
    	}
    }
}
