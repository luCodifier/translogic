﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Via
{
    using System.Collections.Generic;
    using System.Linq;
    using ALL.Core.AcessoDados;
    using Model.Via;
    using Model.Via.Repositories;
    using NHibernate.Linq;

    /// <summary>
    /// IMPLEMENTAÇÃO DE REPOSITORIO DE MARGEM TRECHO
    /// </summary>
    public class MargemTrechoRepository : NHRepository<MargemTrecho, int>, IMargemTrechoRepository
    {
        /// <summary>
        /// Obtém as margens referentes ao fluxo
        /// </summary>
        /// <param name="fluxo"> Fluxo Comercial para filtro. </param>
        /// <returns> Lista de margens referentes as este fluxo </returns>
        public IEnumerable<MargemTrecho> ObterPorDestinoFluxo(Model.FluxosComerciais.FluxoComercial fluxo)
        {
            using (var session = OpenSession())
            {
                var idAoDEstino = fluxo.Destino.Id;
                var query = from mc in session.Query<MargemTrecho>()
                    where mc.MargemDireita.Id == idAoDEstino
                          || mc.MargemEsquerda.Id == idAoDEstino
                    select mc;

                return query.ToList();
            }
        }
    }
}