namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Via
{
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;
    using Model.Via;
    using Model.Via.Repositories;
    using NHibernate.Criterion;
    using NHibernate.SqlCommand;
    using System;
    using NHibernate;
    using System.Text;
    using NHibernate.Transform;
    using Translogic.Modules.Core.Interfaces.Trem.OrdemServico;

    /// <summary>
    /// Implementa��o do Reposit�rio de <see cref="EstacaoMae"/>
    /// </summary>
    public class EstacaoMaeRepository : NHRepository<EstacaoMae, int?>, IEstacaoMaeRepository
    {
        /// <summary>
        /// Obter a area operacional com o c�digo informado
        /// </summary>
        /// <param name="codigo">C�digo da area operacional</param>
        /// <returns>Retorna a area operacional que tenha o c�digo informado</returns>
        public virtual EstacaoMae ObterEstMaePorCodigo(string codigo)
        {
            DetachedCriteria criteria = CriarCriteria().Add(Restrictions.Eq("Codigo", codigo));
            return ObterPrimeiro(criteria);
        }

        /// <summary>
        /// Obter a area operacional com o c�digo informado
        /// </summary>
        /// <param name="codigo">C�digo da area operacional</param>
        /// <returns>Retorna a area operacional que tenha o c�digo informado</returns>
        public virtual IAreaOperacional ObterPorCodigo(string codigo)
        {
            DetachedCriteria criteria = CriarCriteria().Add(Restrictions.Eq("Codigo", codigo));
            return ObterPrimeiro(criteria);
        }

        /// <summary>
        /// Obter a area operacional com o c�digo, descricao, municipio ou estado informado
        /// </summary>
        /// <param name="codigo">C�digo da area operacional</param>
        /// <param name="descricao"> Descri��o da area operacional</param>
        /// <param name="idMunicipio"> Cidade da area operacional</param>
        /// /// <param name="idEstado"> Estado da area operacional</param>
        /// <returns>Retorna as areas operacionais que tenha o filtro informado.</returns>
        public virtual IList<EstacaoMae> ObterEstacao(string codigo, string descricao, int? idMunicipio, int? idEstado)
        {
            DetachedCriteria criteria = CriarCriteria();

            criteria.CreateAlias("Municipio", "cidade", JoinType.InnerJoin);
            criteria.CreateAlias("Municipio.Estado", "estado", JoinType.InnerJoin);

            if (!string.IsNullOrEmpty(codigo))
            {
                criteria.Add(Restrictions.Eq("Codigo", codigo.ToUpper()));
            }

            if (!string.IsNullOrEmpty(descricao))
            {
                criteria.Add(Restrictions.Like("Descricao", string.Concat("%", descricao.ToUpper(), "%")));
            }

            if (idMunicipio.HasValue)
            {
                criteria.Add(Restrictions.Eq("cidade.Id", idMunicipio.Value));
            }

            if (idEstado.HasValue)
            {
                criteria.Add(Restrictions.Eq("estado.Id", idEstado.Value));
            }

            return ObterTodos(criteria);
        }

        /// <summary>
        /// Obter a area operacional com o c�digo, descricao, municipio ou estado informado
        /// </summary>
        /// <param name="codigo">C�digo da area operacional</param>
        /// <returns>Retorna as areas operacionais que tenha o filtro informado.</returns>
        public virtual IList<EstacaoMae> ObterPorUnidadeProducao(int codigo)
        {
            DetachedCriteria criteria = CriarCriteria().Add(Restrictions.Eq("UnidadeProducao.Id", codigo));
            return ObterTodos(criteria);
        }


        public EstacaoMaeDto ObterIdEstacaoMaePorCodigoDoLocal(string codigo)
        {
            #region qry
            var parameters = new List<Action<IQuery>>();
            var sql = new StringBuilder();
            sql.AppendFormat(@"SELECT AO.AO_ID_AO as ""Id""
                               FROM AREA_OPERACIONAL AO
                               WHERE AO.AO_COD_AOP ='{0}'", codigo);

            #endregion

            #region  conectaBase
            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());

                foreach (var item in parameters)
                {
                    item.Invoke(query);
                }

                var obj = query.SetResultTransformer(Transformers.AliasToBean<EstacaoMaeDto>());
                return query.UniqueResult<EstacaoMaeDto>();
            }
            #endregion

        }
    }
}