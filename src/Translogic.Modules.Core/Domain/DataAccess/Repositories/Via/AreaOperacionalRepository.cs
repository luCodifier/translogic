namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Via.Circulacao
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using ALL.Core.AcessoDados;
    using Model.Dto;
    using Model.Via;
    using Model.Via.Repositories;
    using NHibernate;
    using NHibernate.Criterion;
    using NHibernate.Transform;
    using Translogic.Modules.Core.Domain.Model.Estrutura;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Locomotiva;

    /// <summary>
    /// Implementa��o do Reposit�rio de <see cref="IAreaOperacional"/>
    /// </summary>
    public class AreaOperacionalRepository : NHRepository<IAreaOperacional, int?>, IAreaOperacionalRepository
    {
        /// <summary>
        /// Obter a area operacional com o c�digo informado
        /// </summary>
        /// <param name="codigo">C�digo da area operacional</param>
        /// <returns>Retorna a area operacional que tenha o c�digo informado</returns>
        public virtual IAreaOperacional ObterPorCodigo(string codigo)
        {
            DetachedCriteria criteria = CriarCriteria().Add(Restrictions.Eq("Codigo", codigo));
            return ObterPrimeiro(criteria);
        }

        /// <summary>
        /// Obter a area operacional com o c�digo informado
        /// </summary>
        /// <param name="empresaConcessionaria">C�digo da area operacional</param>
        /// <returns>Retorna a area operacional que tenha o c�digo informado</returns>
        public virtual IAreaOperacional ObterPorEmpresaConcessionaria(IEmpresa empresaConcessionaria)
        {
            DetachedCriteria criteria = CriarCriteria().Add(Restrictions.Eq("EmpresaConcessionaria", empresaConcessionaria));
            return ObterPrimeiro(criteria);
        }

        /// <summary>
        /// Metodo que retorna os terminais
        /// </summary>
        /// <returns>Retorna lista de Terminais</returns>
        public List<TerminalSeguroDto> ObterTerminais()
        {
            var sql = @"SELECT 
                                AO.AO_ID_AO    as ""IdTerminal"", 
                                AO.AO_COD_AOP  as ""CodTerminal"", 
                                AO.AO_DSC_AOP  as ""DescricaoTerminal""
                        FROM   
                                AREA_OPERACIONAL AO 
                        WHERE  
                                AO.AO_TRM_CLI = 'S'
                        ORDER BY 
                            AO.AO_DSC_AOP";

            try
            {
                using (var session = OpenSession())
                {
                    var query = session.CreateSQLQuery(sql);

                    query.SetResultTransformer(Transformers.AliasToBean(typeof(TerminalSeguroDto)));
                    IList<TerminalSeguroDto> items = query.List<TerminalSeguroDto>();

                    return items.ToList();
                }
            }
            catch (Exception e)
            {
                Console.Write(e);
                return null;
            }
        }


        /// <summary>
        /// Metodo que retorna os terminais de acordo com o despacho e serie
        /// </summary>
        /// <param name="despacho">Numero do despacho</param>
        /// <param name="serie">Serie do despacho</param>
        /// <returns>Retorna lista de Terminais</returns>
        public List<TerminalSeguroDto> ObterTerminaisPorDespacho(int despacho, string serie)
        {
            var sql = @"SELECT 
                                AO.AO_ID_AO    as ""IdTerminal"", 
                                AO.AO_COD_AOP  as ""CodTerminal"", 
                                AO.AO_DSC_AOP  as ""DescricaoTerminal""
                        FROM   
                                AREA_OPERACIONAL AO 
                        WHERE  
                                AO.AO_TRM_CLI = 'S'
                                AND AO.AO_ID_AO_INF IN 
                                (
                                                select 
                                                        distinct nvl(f.ao_id_int_ds,f.ao_id_est_ds)
                                                from despacho d,
                                                        serie_despacho s,
                                                        item_despacho i,
                                                        detalhe_carregamento dc,
                                                          carregamento c,
                                                        fluxo_comercial f
                                                where 
                                                        d.dp_num_dsp = :despacho
                                                        and d.sk_idt_srd = s.sk_idt_srd
                                                        and s.sk_num_srd = :serie
                                                        and d.dp_id_dp = i.dp_id_dp 
                                                        and dc.dc_id_crg = i.dc_id_crg 
                                                        and c.cg_id_car = dc.cg_id_car
                                                        and c.fx_id_flx_crg = f.fx_id_flx
                                )
                 
                        ORDER BY 
                            AO.AO_DSC_AOP";

            try
            {
                using (var session = OpenSession())
                {
                    var query = session.CreateSQLQuery(sql);
                    query.SetParameter("despacho", despacho);
                    query.SetString("serie", serie.ToUpper());

                    query.SetResultTransformer(Transformers.AliasToBean(typeof(TerminalSeguroDto)));
                    IList<TerminalSeguroDto> items = query.List<TerminalSeguroDto>();

                    return items.ToList();
                }
            }
            catch (Exception e)
            {
                Console.Write(e);
                return null;
            }
        }

        /// <summary>
        /// Metodo que retorna os terminais de acordo com o despacho e serie
        /// </summary>
        /// <param name="despacho">Numero do despacho</param>
        /// <param name="serie">Serie do despacho</param>
        /// <returns>Retorna lista de Terminais</returns>
        public List<TerminalSeguroDto> ObterTerminaisPorDespachoIntercambio(int despacho, string serie)
        {
            var sql = @"SELECT 
                                AO.AO_ID_AO    as ""IdTerminal"", 
                                AO.AO_COD_AOP  as ""CodTerminal"", 
                                AO.AO_DSC_AOP  as ""DescricaoTerminal""
                         FROM   
                                AREA_OPERACIONAL AO 
                        WHERE  
                                AO.AO_TRM_CLI = 'S'
                                AND AO.AO_ID_AO_INF IN 
                                (
                                                select 
                                                        distinct nvl(f.ao_id_int_ds,f.ao_id_est_ds)
                                                from despacho d,
                                                        item_despacho i,
                                                        detalhe_carregamento dc,
                                                        carregamento c,
                                                        fluxo_comercial f
                                                where 
                                                        d.dp_num_sdi = :serie 
                                                        and d.dp_num_dpi = :despacho
                                                        and d.dp_id_dp = i.dp_id_dp 
                                                        and dc.dc_id_crg = i.dc_id_crg 
                                                        and c.cg_id_car = dc.cg_id_car
                                                        and c.fx_id_flx_crg = f.fx_id_flx
                                )
                 
                        ORDER BY 
                            AO.AO_DSC_AOP";

            try
            {
                using (var session = OpenSession())
                {
                    var query = session.CreateSQLQuery(sql);
                    query.SetParameter("despacho", despacho);
                    query.SetString("serie", serie.ToUpper());

                    query.SetResultTransformer(Transformers.AliasToBean(typeof(TerminalSeguroDto)));
                    IList<TerminalSeguroDto> items = query.List<TerminalSeguroDto>();

                    return items.ToList();
                }
            }
            catch (Exception e)
            {
                Console.Write(e);
                return null;
            }
        }


        /// <summary>
        /// M�todo que retorna as �reas operacionais por cliente
        /// </summary>
        /// <param name="codigoEmpresa">c�digo da empresa</param>
        /// <returns>Lista de �rear operacionais</returns>
        public List<AreaOpDto> ObterPorCliente(string codigoEmpresa)
        {
            var sql = @"SELECT
                           ao.AO_ID_AO AS ""IdTerminal"",
                           ao.ao_cod_aop AS ""CodTerminal""
                        FROM
                           area_operacional ao 
                           JOIN fluxo_comercial fc ON fc.ao_id_est_or = ao.ao_id_ao 
                           JOIN bo_bolar_bo bbb    ON substr(fc.fx_cod_flx, 3) = to_char(bbb.cod_flx) 
                        WHERE  bbb.ao_cod_aop = :codigoEmpresa 
                        GROUP BY ao.AO_ID_AO,ao.ao_cod_aop 
                        ORDER BY ao.ao_cod_aop ";

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql);
                query.SetParameter("codigoEmpresa", codigoEmpresa);

                query.SetResultTransformer(Transformers.AliasToBean(typeof(AreaOpDto)));
                var items = query.List<AreaOpDto>();

                return items.ToList();
            }
        }

        public IList<AreaOpDto> ObterTodasComFluxoComercial()
        {
            var sql = @"SELECT 
                        ao.ao_id_ao AS ""IdTerminal"",
                        ao.ao_cod_aop AS ""CodTerminal""
                        FROM area_operacional ao
                        JOIN fluxo_comercial fc ON fc.ao_id_est_or = ao.ao_id_ao
                        GROUP BY ao_id_ao,ao_cod_aop
                        ORDER BY 2";

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql);

                query.SetResultTransformer(Transformers.AliasToBean(typeof(AreaOpDto)));
                var items = query.List<AreaOpDto>();

                return items.ToList();
            }
        }


        public List<TerminalSiglaDto> ObterListaTerminalSigla()
        {
            var sql = @"SELECT DISTINCT E.EP_CGC_EMP AS ""CodigoTerminal"",
                                        E.EP_DSC_RSM || ' - ' || regexp_replace(LPAD(E.EP_CGC_EMP, 14),'([0-9]{2})([0-9]{3})([0-9]{3})([0-9]{4})', '\1.\2.\3/\4-') AS ""SiglaTerminal""
                          FROM CONF_ENVIO_DOC C
                         INNER JOIN EMPRESA E
                            ON C.ID_EP_ID_EMP_DEST = E.EP_ID_EMP
                         ORDER BY 2";

            try
            {
                using (var session = OpenSession())
                {
                    var query = session.CreateSQLQuery(sql);

                    query.SetResultTransformer(Transformers.AliasToBean(typeof(TerminalSiglaDto)));
                    IList<TerminalSiglaDto> items = query.List<TerminalSiglaDto>();

                    return items.ToList();
                }
            }
            catch (Exception e)
            {
                Console.Write(e);
                return null;
            }
        }
    }
}