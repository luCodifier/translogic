namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Via.Circulacao
{
    using System;
    using System.Collections.Generic;
	using System.Linq;
	using ALL.Core.AcessoDados;
    using Interfaces.Trem.OrdemServico;
    using Model.Via;
	using Model.Via.Circulacao;
	using Model.Via.Circulacao.Repositories;
	using NHibernate;
	using NHibernate.Criterion;
    using NHibernate.Transform;

	/// <summary>
	/// Implementa��o do Reposit�rio de <see cref="Rota"/>
	/// </summary>
	public class RotaRepository : NHRepository<Rota, int>, IRotaRepository
	{
		#region IRotaRepository MEMBROS

		/// <summary>
		/// Obtem a rota pela Origem e Destino
		/// </summary>
		/// <param name="origem">Origem da rota</param>
		/// <param name="destino">Destino da rota</param>
		/// <returns>Rota de Origem e Destino</returns>
		public Rota ObterPorOrigemDestino(PatioCirculacao origem, PatioCirculacao destino)
		{
			using (var session = OpenSession())
			{
				IQuery query = session.CreateQuery("from Rota R WHERE R.Origem = :origem AND R.Destino = :destino AND Ativa = 'S'");

				query.SetEntity("origem", origem);
				query.SetEntity("destino", destino);

				IList<Rota> rotas = query.List<Rota>();

				return rotas.FirstOrDefault();
			}
		}

		/// <summary>
		/// Obtem a rota pela Origem e Destino
		/// </summary>
		/// <param name="origem">Origem da rota</param>
		/// <param name="destino">Destino da rota</param>
		/// <returns>Rota de Origem e Destino</returns>
		public Rota ObterPorOrigemDestino(EstacaoMae origem, EstacaoMae destino)
		{
			using (var session = OpenSession())
			{
				IQuery query = session.CreateQuery("from Rota R WHERE R.Origem.EstacaoMae = :origem AND R.Destino.EstacaoMae = :destino AND Ativa = 'S'");

				query.SetEntity("origem", origem);
				query.SetEntity("destino", destino);

				IList<Rota> rotas = query.List<Rota>();

				return rotas.FirstOrDefault();
			}
		}

		/// <summary>
		/// Obtem a rota pela Origem e Destino
		/// </summary>
		/// <param name="idRota"> Id da rota</param>
		/// <returns>Rota de Origem e Destino</returns>
		public IList<EmpresasEnvolvidasDto> ObterFerroviasEnvolvidasPorIdRota(int idRota)
		{
			using (var session = OpenSession())
			{
				ISQLQuery query = session.CreateSQLQuery(@" SELECT MIN(AR.AA_SEQ_RTA) AS Seq, AOF.EP_ID_EMP_CCS as IdEmpresa, ES.ES_SGL_EST  as SiglaEstado
															  FROM ROTA R
																  JOIN AREAOPER_ROTA AR 
																	ON R.RT_ID_RTA = AR.RT_ID_RTA
																  JOIN AREA_OPERACIONAL AOF
																	ON AR.AO_ID_AO = AOF.AO_ID_AO
																  JOIN AREA_OPERACIONAL AOM
																	ON AOF.AO_ID_AO_INF = AOM.AO_ID_AO
																  JOIN MUNICIPIO MN
																	ON AOM.MN_ID_MNC = MN.MN_ID_MNC
																  JOIN ESTADO ES
																	ON MN.ES_ID_EST = ES.ES_ID_EST
																  JOIN EMPRESA EP
																	ON AOF.EP_ID_EMP_CCS = EP.EP_ID_EMP AND EP.EP_IND_ALL='S'
																WHERE R.RT_ID_RTA = :idRota
																GROUP BY AOF.EP_ID_EMP_CCS, ES.ES_SGL_EST");

				query.SetInt32("idRota", idRota);
				query.SetResultTransformer(Transformers.AliasToBean(typeof(EmpresasEnvolvidasDto)));
				return query.List<EmpresasEnvolvidasDto>();
			}
		}

	    /// <summary>
	    /// Obt�m a rota de menor dist�ncia dado o trecho 
	    /// </summary>
	    /// <param name="codigoOrigem"> Codigo de origem. </param>
	    /// <param name="codigoDestino"> Codigo de destino. </param>
	    /// <returns> Objeto Rota </returns>
	    public Rota ObterPorTrechoMenorDistancia(string codigoOrigem, string codigoDestino)
	    {
            using (var session = OpenSession())
            {
                IQuery query = session.CreateQuery(@"
                    FROM Rota R 
                    WHERE 
                        R.Origem.EstacaoMae.Codigo = :origem 
                        AND R.Destino.EstacaoMae.Codigo = :destino 
                        AND Ativa = 'S'
                    ORDER BY R.Extensao ASC");

                query.SetString("origem", codigoOrigem);
                query.SetString("destino", codigoDestino);

                IList<Rota> rotas = query.List<Rota>();

                return rotas.FirstOrDefault();
            }
	    }

	    #endregion
	}
}