namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Indicador.Vagao
{
	using ALL.Core.AcessoDados;
	using Model.Indicador.Vagao;
	using Model.Indicador.Vagao.Repositories;

	/// <summary>
	/// Repositorio para <see cref="IndLotacaoVagoes"/>
	/// </summary>
	public class IndLotacaoVagoesRepository : NHRepository<IndLotacaoVagoes, int?>, IIndLotacaoVagoesRepository
	{
	}
}