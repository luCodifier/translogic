namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Indicador.TKB
{
	using ALL.Core.AcessoDados;
	using Model.Indicador.TKB;
	using Model.Indicador.TKB.Repositories;

	/// <summary>
	/// Repositorio para <see cref="TLTKBCorredor"/>
	/// </summary>
	public class TLTKBCorredorRepository : NHRepository<TLTKBCorredor, int?>, ITLTKBCorredorRepository
	{
	}
}