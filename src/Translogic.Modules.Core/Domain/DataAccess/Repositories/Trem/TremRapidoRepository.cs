namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Trem
{
	using System;
	using System.Collections.Generic;
	using System.Text;
	using ALL.Core.AcessoDados;
	using Model.Trem;
	using Model.Trem.Repositories;
	using NHibernate;
	using NHibernate.Transform;

	/// <summary>
	/// Repositorio para <see cref="TremRapido"/>
	/// </summary>
	public class TremRapidoRepository : NHRepository<TremRapido, int?>, ITremRapidoRepository
	{
		/// <summary>
		/// Obt�m os trens r�pidos vigentes
		/// </summary>
		/// <returns>Lista de trens r�pidos</returns>
		public IList<TremRapido> ObterTrensRapidosVigentes()
		{
			StringBuilder sb = new StringBuilder();
			sb.Append(@"FROM TremRapido tr WHERE 
						SYSDATE BETWEEN tr.DataInicial AND nvl(tr.DataFinal, SYSDATE)");

            ISession session = OpenSession();

			IQuery query = session.CreateQuery(sb.ToString());
			query.SetResultTransformer(Transformers.DistinctRootEntity);

			return query.List<TremRapido>();
		}
	}
}