namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Trem
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using ALL.Core.AcessoDados;
    using ALL.Core.Dominio;
    using Model.Trem;
    using Model.Trem.Repositories;
    using NHibernate;
    using NHibernate.Criterion;
    using NHibernate.SqlCommand;
    using NHibernate.Transform;
    using Translogic.Modules.Core.Domain.Model.Dto;

    /// <summary>
    /// Repositorio para <see cref="Trem"/>
    /// </summary>
    public class TremRepository : NHRepository<Trem, int?>, ITremRepository
    {
        /// <summary>
        /// Obter um trem por id da ordem de servico <see cref="Translogic.Modules.Core.Domain.DataAccess.Repositories.Trem.OrdemServico"/>
        /// </summary>
        /// <param name="idOrdemServico">Id da ordem de servi�o <see cref="Translogic.Modules.Core.Domain.DataAccess.Repositories.Trem.OrdemServico"/></param>
        /// <returns>Um trem que foi criado baseado na <see cref="Translogic.Modules.Core.Domain.DataAccess.Repositories.Trem.OrdemServico"/></returns>
        public Trem ObterPorIdOrdemServico(int idOrdemServico)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.Eq("OrdemServico.Id", idOrdemServico));

            return ObterPrimeiro(criteria);
        }

        /// <summary>
        /// Obt�m um trem dado o n�mero da Ordem de servi�o
        /// </summary>
        /// <param name="numeroOrdemServico"> Numero da ordem de servico. </param>
        /// <returns>Um trem que foi criado baseado na <see cref="Translogic.Modules.Core.Domain.DataAccess.Repositories.Trem.OrdemServico"/></returns>
        public Trem ObterPorNumeroOrdemServico(int numeroOrdemServico)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.CreateAlias("OrdemServico", "os", JoinType.InnerJoin);
            criteria.CreateAlias("ListaMovimentacaoTrem", "lmv", JoinType.InnerJoin);
            criteria.CreateAlias("lmv.Estacao", "em", JoinType.InnerJoin);
            criteria.Add(Restrictions.Eq("os.Numero", numeroOrdemServico));
            criteria.SetFetchMode("os", FetchMode.Join);
            criteria.SetFetchMode("lmv", FetchMode.Eager);
            criteria.SetFetchMode("em", FetchMode.Eager);
            criteria.SetResultTransformer(Transformers.DistinctRootEntity);

            return ObterPrimeiro(criteria);
        }

        /// <summary>
        /// Obt�m um trem dado o id
        /// </summary>
        /// <param name="idTrem"> Id do trem. </param>
        /// <returns>Um trem e suas dependencias para o servico do EDI</returns>
        public Trem ObterPorIdComFetch(int idTrem)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.CreateAlias("Origem", "o", JoinType.InnerJoin);
            criteria.CreateAlias("OrdemServico", "os", JoinType.InnerJoin);

            criteria.Add(Restrictions.Eq("Id", idTrem));
            criteria.SetFetchMode("o", FetchMode.Eager);
            criteria.SetFetchMode("os", FetchMode.Eager);
            criteria.SetResultTransformer(Transformers.DistinctRootEntity);

            return ObterPrimeiro(criteria);
        }

        /// <summary>
        /// Obt�m um trem dado o n�mero da Ordem de servi�o
        /// </summary>
        /// <param name="numeroOrdemServico"> Numero da ordem de servico. </param>
        /// <returns>Um trem que foi criado baseado na <see cref="Translogic.Modules.Core.Domain.DataAccess.Repositories.Trem.OrdemServico"/></returns>
        public Trem ObterPorNumeroOrdemServicoSemFetch(int numeroOrdemServico)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.CreateAlias("OrdemServico", "os", JoinType.InnerJoin);
            criteria.Add(Restrictions.Eq("os.Numero", numeroOrdemServico));
            criteria.SetResultTransformer(Transformers.DistinctRootEntity);

            return ObterPrimeiro(criteria);
        }

        /// <summary>
        /// Lista as Mdfes
        /// </summary>
        /// <param name="detalhesPaginacao">detalhes da paginacao</param>
        /// <param name="prefixoTrem">prefixo do trem</param>
        /// <param name="os">os do trem</param>
        /// <param name="dataInicial">data inicial do intervalo</param>
        /// <param name="dataFinal">data final do intervalo</param>
        /// <param name="origem">origem do trem</param>
        /// <param name="destino">destino do trem</param>
        /// <param name="idTrem">id do trem</param>
        /// <returns>resultado paginado de trens com mdfe</returns>
        public ResultadoPaginado<TremDto> Listar(DetalhesPaginacao detalhesPaginacao, string prefixoTrem, int? os, DateTime? dataInicial, DateTime? dataFinal, string origem, string destino, int? idTrem)
        {
            var parameters = new List<Action<IQuery>>();

            var hql = new StringBuilder(@"
                select 
                    {0}
                  from Trem t
                 inner join AREA_OPERACIONAL origem on t.AO_ID_AO_ORG = origem.AO_ID_AO
                 inner join AREA_OPERACIONAL destino on t.AO_ID_AO_DST = destino.AO_ID_AO
                 inner join T2_OS os on os.x1_id_os = t.of_id_osv
                 where 1=1                  
            ");

            if (dataInicial.HasValue)
            {
                hql.AppendLine(" and t.TR_DTP_PRT >= :dataInicial ");
                parameters.Add(q => q.SetParameter("dataInicial", dataInicial));
            }

            if (dataFinal.HasValue)
            {
                hql.AppendLine(" and t.TR_DTP_PRT <= :dataFinal ");
                parameters.Add(q => q.SetParameter("dataFinal", dataFinal));
            }

            if (!string.IsNullOrWhiteSpace(prefixoTrem))
            {
                hql.AppendLine(" and t.TR_PFX_TRM = :prefixoTrem ");
                parameters.Add(q => q.SetParameter("prefixoTrem", prefixoTrem));
            }

            if (os.HasValue)
            {
                hql.AppendLine(" and os.x1_nro_os = :os ");
                parameters.Add(q => q.SetParameter("os", os));
            }

            if (!string.IsNullOrWhiteSpace(origem))
            {
                hql.AppendLine(" and origem.ao_cod_aop = :origem ");
                parameters.Add(q => q.SetParameter("origem", origem));
            }

            if (!string.IsNullOrWhiteSpace(destino))
            {
                hql.AppendLine(" and destino.ao_cod_aop = :destino ");
                parameters.Add(q => q.SetParameter("destino", destino));
            }

            if (idTrem.HasValue)
            {
                hql.AppendLine(" and t.tr_id_trm = :idTrem ");
                parameters.Add(q => q.SetParameter("idTrem", idTrem));
            }

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(string.Format(hql.ToString(), @"distinct t.tr_id_trm  as ""Id"", t.TR_PFX_TRM as ""Prefixo"", origem.ao_cod_aop as ""Origem"", destino.ao_cod_aop as ""Destino"", os.x1_nro_os as ""OS"", t.TR_DTP_PRT as ""DataRealizadaPartida"" "));

                var queryCount = session.CreateSQLQuery(string.Format(hql.ToString(), "count(t.tr_id_trm)"));

                foreach (var p in parameters)
                {
                    p.Invoke(query);
                    p.Invoke(queryCount);
                }

                query.SetResultTransformer(Transformers.AliasToBean<TremDto>());
                query.SetFirstResult(detalhesPaginacao.Inicio ?? 0);
                query.SetMaxResults(detalhesPaginacao.Limite ?? DetalhesPaginacao.LimitePadrao);

                var total = queryCount.UniqueResult<decimal>();
                var itens = query.List<TremDto>();

                var result = new ResultadoPaginado<TremDto>
                {
                    Items = itens,
                    Total = (long)total
                };

                return result;
            }
        }

        /// <summary>
        /// Lista as Mdfes
        /// </summary>
        /// <param name="detalhesPaginacao">detalhes da paginacao</param>
        /// <param name="prefixoTrem">prefixo do trem</param>
        /// <param name="os">os do trem</param>
        /// <param name="dataInicial">data inicial do intervalo</param>
        /// <param name="dataFinal">data final do intervalo</param>
        /// <param name="origem">origem do trem</param>
        /// <param name="destino">destino do trem</param>
        /// <param name="chaveMdfe">Chave da Mdfe informada no filtro</param>
        /// <param name="idTrem">id do trem</param>
        /// <returns>resultado paginado de trens com mdfe</returns>
        public ResultadoPaginado<TremDto> Listar(DetalhesPaginacao detalhesPaginacao, string prefixoTrem, int? os, DateTime? dataInicial, DateTime? dataFinal, string origem, string destino, string chaveMdfe, int? idTrem)
        {
            var parameters = new List<Action<IQuery>>();

            var hql = new StringBuilder(@"
                select 
                    {0}
                  from Trem t
                 inner join AREA_OPERACIONAL origem on t.AO_ID_AO_ORG = origem.AO_ID_AO
                 inner join AREA_OPERACIONAL destino on t.AO_ID_AO_DST = destino.AO_ID_AO
                 inner join T2_OS os on os.x1_id_os = t.of_id_osv
                 left join Mdfe m on m.tr_id_trm = t.tr_id_trm
                 where 1=1                  
            ");

            if (idTrem.HasValue && idTrem.Value > 0)
            {
                hql.AppendLine(" and t.tr_id_trm = :idTrem ");
                parameters.Add(q => q.SetParameter("idTrem", idTrem));
            }
            else
            {
                if (dataInicial.HasValue)
                {
                    hql.AppendLine(" and t.TR_DTP_PRT >= :dataInicial ");
                    parameters.Add(q => q.SetParameter("dataInicial", dataInicial));
                }

                if (dataFinal.HasValue)
                {
                    hql.AppendLine(" and t.TR_DTP_PRT <= :dataFinal ");
                    parameters.Add(q => q.SetParameter("dataFinal", dataFinal));
                }

                if (!string.IsNullOrWhiteSpace(prefixoTrem))
                {
                    hql.AppendLine(" and t.TR_PFX_TRM = :prefixoTrem ");
                    parameters.Add(q => q.SetParameter("prefixoTrem", prefixoTrem));
                }

                if (os.HasValue)
                {
                    hql.AppendLine(" and os.x1_nro_os = :os ");
                    parameters.Add(q => q.SetParameter("os", os));
                }

                if (!string.IsNullOrWhiteSpace(origem))
                {
                    hql.AppendLine(" and origem.ao_cod_aop = :origem ");
                    parameters.Add(q => q.SetParameter("origem", origem));
                }

                if (!string.IsNullOrWhiteSpace(destino))
                {
                    hql.AppendLine(" and destino.ao_cod_aop = :destino ");
                    parameters.Add(q => q.SetParameter("destino", destino));
                }

                if (!String.IsNullOrWhiteSpace(chaveMdfe))
                {
                    hql.AppendLine(" and m.chave_mdfe = :chaveMdfe ");
                    parameters.Add(q => q.SetParameter("chaveMdfe", chaveMdfe));
                }
            }

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(string.Format(hql.ToString(), @"distinct t.tr_id_trm  as ""Id"", t.TR_PFX_TRM as ""Prefixo"", origem.ao_cod_aop as ""Origem"", destino.ao_cod_aop as ""Destino"", os.x1_nro_os as ""OS"", t.TR_DTP_PRT as ""DataRealizadaPartida"" "));

                var queryCount = session.CreateSQLQuery(string.Format(hql.ToString(), "count(t.tr_id_trm)"));

                foreach (var p in parameters)
                {
                    p.Invoke(query);
                    p.Invoke(queryCount);
                }

                query.SetResultTransformer(Transformers.AliasToBean<TremDto>());
                query.SetFirstResult(detalhesPaginacao.Inicio ?? 0);
                query.SetMaxResults(detalhesPaginacao.Limite ?? DetalhesPaginacao.LimitePadrao);

                var total = queryCount.UniqueResult<decimal>();
                var itens = query.List<TremDto>();

                var result = new ResultadoPaginado<TremDto>
                {
                    Items = itens,
                    Total = (long)total
                };

                return result;
            }
        }

        /// <summary>
        /// Listar os trens de acordo com o despacho informado
        /// </summary>
        /// <param name="detalhesPaginacao">Detalhes de paginacao</param>
        /// <param name="idDespacho">Id do despacho selecionado</param>
        /// <returns>Lista de trens de acordo com o despacho</returns>
        public ResultadoPaginado<TremDto> ListarPorDespacho(DetalhesPaginacao detalhesPaginacao, int idDespacho)
        {
            var sqlQuery = @"SELECT DISTINCT
      T.TR_ID_TRM as ""Id"",
      T.TR_PFX_TRM as ""Prefixo"",
      AO.AO_DSC_AOP as ""Origem"",
      AOO.AO_DSC_AOP as ""Destino"",
      OS.X1_NRO_OS as ""OS""
 FROM VAGAO_PEDIDO VP,
        COMPVAGAO CV,
        COMPOSICAO CP,
        VAGAO V,
        CARREGAMENTO CG,
        DETALHE_CARREGAMENTO DCG,
        ITEM_DESPACHO ITD,
        DESPACHO D,
        NOTA_FISCAL N,
        SERIE_DESPACHO SD,
        CTE C,
        CTE_DET CD,
        FLUXO_COMERCIAL FC,
        Area_Operacional AO,
        Area_Operacional AOO,
        TREM T,
        MDFE M,
        T2_OS OS
  WHERE VP.PT_ID_ORT = CV.PT_ID_ORT(+) 
    AND VP.VG_ID_VG = CV.VG_ID_VG(+)
    AND V.VG_ID_VG = VP.VG_ID_VG
    AND CG.CG_ID_CAR = VP.CG_ID_CAR
    AND CG.CG_ID_CAR = DCG.CG_ID_CAR
    AND ITD.DC_ID_CRG = DCG.DC_ID_CRG 
    AND ITD.VG_ID_VG = DCG.VG_ID_VG
    AND ITD.DP_ID_DP = D.DP_ID_DP
    AND N.DP_ID_DP = D.DP_ID_DP
    AND C.DP_ID_DP(+) = D.DP_ID_DP
    AND CD.ID_CTE(+) = C.ID_CTE
    AND FC.FX_ID_FLX(+) = C.Fx_Id_Flx
    AND SD.SK_IDT_SRD = D.SK_IDT_SRD 
    AND AO.AO_ID_AO(+) = FC.AO_ID_EST_OR
    AND AOO.AO_ID_AO(+)= FC.AO_ID_EST_DS
    AND CP.CP_ID_CPS(+) = CV.CP_ID_CPS
    AND T.Tr_Id_Trm(+) = CP.TR_ID_TRM
    AND M.CP_ID_CPS(+) = CP.CP_ID_CPS
    AND OS.X1_ID_OS(+)= T.Of_Id_Osv
    AND CG.CG_DTR_CRG < sysdate - 2 / 24 / 60
    AND T.TR_ID_TRM IS NOT NULL 
    AND D.DP_ID_DP = :idDespacho
UNION ALL
SELECT DISTINCT
      T.TR_ID_TRM as ""Id"",
      T.TR_PFX_TRM as ""Prefixo"",
      AO.AO_DSC_AOP as ""Origem"",
      AOO.AO_DSC_AOP as ""Destino"",
      OS.X1_NRO_OS as ""OS""
 FROM VAGAO_PEDIDO VP,
        COMPVAGAO CV,
        COMPOSICAO CP,
        VAGAO V,
        CARREGAMENTO CG,
        DETALHE_CARREGAMENTO DCG,
        ITEM_DESPACHO ITD,
        DESPACHO D,
        NOTA_FISCAL N,
        SERIE_DESPACHO SD,
        CTE C,
        CTE_DET CD,
        CTE_COMPLEMENTADO CC,
        CTE COR,
        FLUXO_COMERCIAL FC,
        Area_Operacional AO,
        Area_Operacional AOO,
        TREM T,
        MDFE M,
        T2_OS OS
  WHERE VP.PT_ID_ORT = CV.PT_ID_ORT(+) 
    AND VP.VG_ID_VG = CV.VG_ID_VG(+)
    AND V.VG_ID_VG = VP.VG_ID_VG
    AND CG.CG_ID_CAR = VP.CG_ID_CAR
    AND CG.CG_ID_CAR = DCG.CG_ID_CAR
    AND ITD.DC_ID_CRG = DCG.DC_ID_CRG 
    AND ITD.VG_ID_VG = DCG.VG_ID_VG
    AND ITD.DP_ID_DP = D.DP_ID_DP
    AND N.DP_ID_DP = D.DP_ID_DP
    AND C.DP_ID_DP = D.DP_ID_DP
    AND C.ID_CTE(+) = CC.ID_CTE
    AND COR.ID_CTE(+) = CC.ID_CTE_COMPLEMENTAR
    AND CD.ID_CTE(+) = C.ID_CTE
    AND FC.FX_ID_FLX(+) = C.Fx_Id_Flx
    AND SD.SK_IDT_SRD = D.SK_IDT_SRD 
    AND AO.AO_ID_AO(+) = FC.AO_ID_EST_OR
    AND AOO.AO_ID_AO(+)= FC.AO_ID_EST_DS
    AND CP.CP_ID_CPS(+) = CV.CP_ID_CPS
    AND T.Tr_Id_Trm(+) = CP.TR_ID_TRM
    AND M.CP_ID_CPS(+) = CP.CP_ID_CPS
    AND OS.X1_ID_OS(+)= T.Of_Id_Osv
    AND CG.CG_DTR_CRG < sysdate - 2 / 24 / 60 
    AND D.DP_ID_DP = :idDespacho
    AND T.TR_ID_TRM IS NOT NULL";

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sqlQuery);
                var queryCount = session.CreateSQLQuery(string.Format(@"SELECT COUNT(0) FROM ({0})", sqlQuery));

                query.SetParameter("idDespacho", idDespacho);
                queryCount.SetParameter("idDespacho", idDespacho);

                query.SetResultTransformer(Transformers.AliasToBean<TremDto>());
                query.SetFirstResult(detalhesPaginacao.Inicio ?? 0);
                query.SetMaxResults(detalhesPaginacao.Limite ?? DetalhesPaginacao.LimitePadrao);

                var total = queryCount.UniqueResult<decimal>();
                var itens = query.List<TremDto>();

                var result = new ResultadoPaginado<TremDto>
                {
                    Items = itens,
                    Total = (long)total
                };

                return result;
            }
        }

        public TremDto ObterTremComposicao(int idTrem, int idComposicao)
        {
            StringBuilder sb = new StringBuilder();

            #region -- Query --
            sb.Append(@"SELECT T.TR_PFX_TRM AS ""Prefixo""
                            FROM COMPOSICAO C
                           INNER JOIN TREM T
                              ON C.TR_ID_TRM = T.TR_ID_TRM
                           WHERE T.TR_STT_TRM = 'E'
                             AND T.AO_ID_AO_PVT IS NOT NULL
                             AND T.TR_ID_TRM = :idTrem
                             AND C.CP_ID_CPS = :idComposicao");
            #endregion

            ISession session = OpenSession();
            var auxSb = string.Format(sb.ToString());
            var stringHqlResult = auxSb;
            IQuery query = session.CreateSQLQuery(stringHqlResult);
            if (idTrem != null && idTrem > 0)
            {
                query.SetInt32("idTrem", idTrem);
            }
            if (idComposicao != null && idComposicao > 0)
            {
                query.SetInt32("idComposicao", idComposicao);
            }
            query.SetResultTransformer(Transformers.AliasToBean(typeof(TremDto)));
            var items = query.UniqueResult<TremDto>();
            return items;
        }

        public void UpdateTremIdPVT(int idTrem)
        {
            using (var session = OpenSession())
            {
                var ql = @"UPDATE TREM SET AO_ID_AO_PVT = NULL WHERE TR_ID_TRM = " + idTrem;

                session.CreateSQLQuery(ql).ExecuteUpdate();
                session.Flush();
            }
        }

    }
}