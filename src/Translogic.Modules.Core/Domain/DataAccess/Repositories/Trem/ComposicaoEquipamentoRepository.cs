namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Trem
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using ALL.Core.AcessoDados;
    using Model.Trem;
    using Model.Trem.Repositories;
    using NHibernate;
    using NHibernate.Criterion;

    /// <summary>
    /// Repositorio para <see cref="Trem"/>
    /// </summary>
    public class ComposicaoEquipamentoRepository : NHRepository<ComposicaoEquipamento, int?>, IComposicaoEquipamentoRepository
    {
        /// <summary>
        /// Obtem a partir da composicao
        /// </summary>
        /// <param name="composicao">A Composicao do Equipamento</param>
        /// <returns>A ComposicaoEquipamento</returns>
        public IList<ComposicaoEquipamento> ObterPorComposicao(Composicao composicao)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.Eq("Composicao", composicao));
            criteria.SetFetchMode("Equipamento", FetchMode.Join);

            return ObterTodos(criteria);
        }
    }
}