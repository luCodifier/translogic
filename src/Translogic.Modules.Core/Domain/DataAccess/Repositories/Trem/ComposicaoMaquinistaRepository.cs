namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Trem
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using ALL.Core.AcessoDados;
    using Model.Trem;
    using Model.Trem.Repositories;
    using NHibernate;
    using NHibernate.Criterion;

    /// <summary>
    /// Repositorio para <see cref="Trem"/>
    /// </summary>
    public class ComposicaoMaquinistaRepository : NHRepository<ComposicaoMaquinista, int?>, IComposicaoMaquinistaRepository
    {
        /// <summary>
        /// Obtem a partir da composicao
        /// </summary>
        /// <param name="composicao">Composicao da ComposicaoMaquinista</param>
        /// <returns>A ComposicaoMaquinista</returns>
        public ComposicaoMaquinista ObterPorComposicao(Composicao composicao)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.Eq("Composicao", composicao));

            return ObterPrimeiro(criteria);
        }
    }
}