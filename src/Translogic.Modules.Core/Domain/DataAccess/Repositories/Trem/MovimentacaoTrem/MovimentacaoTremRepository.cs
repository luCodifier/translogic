namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Trem.MovimentacaoTrem
{
	using System;
	using System.Collections.Generic;
    using ALL.Core.AcessoDados;
	using Model.Trem.MovimentacaoTrem;
	using Model.Trem.MovimentacaoTrem.Repositories;
	using Model.Via;
    using NHibernate.Criterion;

    /// <summary>
	/// Implementação do Repositório de <see cref="Translogic.Modules.Core.Domain.DataAccess.Repositories.Trem.MovimentacaoTrem"/>
	/// </summary>
	public class MovimentacaoTremRepository : NHRepository<Model.Trem.MovimentacaoTrem.MovimentacaoTrem, int?>, IMovimentacaoTremRepository
	{
		/// <summary>
		/// Obtém as movimentações dos trens que passaram em determinado período e area operacional
		/// </summary>
		/// <param name="dataInicio">Data inicial do período</param>
		/// <param name="areaOperacional">Area operacional - <see cref="IAreaOperacional"/></param>
		/// <returns>Lista de movimentações</returns>
		public IList<MovimentacaoTrem> ObterPorPeriodoAreaOperacional(DateTime dataInicio, IAreaOperacional areaOperacional)
		{
			DetachedCriteria criteria = CriarCriteria();
			criteria.Add(
						Restrictions.Ge("DataChegadaRealizada", dataInicio)
						&& Restrictions.Le("DataSaidaRealizada", dataInicio)
			             && Restrictions.Eq("Estacao", areaOperacional));
			return ObterTodos(criteria);
		}

		/// <summary>
		/// Obtém as movimentações dos trens que passaram em determinado período e area operacional
		/// </summary>
		/// <param name="trem">Trem - <see cref="Translogic.Modules.Core.Domain.DataAccess.Repositories.Trem"/></param>
		/// <param name="areaOperacional">Area operacional - <see cref="IAreaOperacional"/></param>
		/// <returns>Lista de movimentações</returns>
		public MovimentacaoTrem ObterPorTremAreaOperacional(Model.Trem.Trem trem, IAreaOperacional areaOperacional)
		{
			DetachedCriteria criteria = CriarCriteria();
			criteria.Add(
						Restrictions.Eq("Trem", trem)
						 && Restrictions.Eq("Estacao", areaOperacional));
			return this.ObterPrimeiro(criteria);
		}
	}
}