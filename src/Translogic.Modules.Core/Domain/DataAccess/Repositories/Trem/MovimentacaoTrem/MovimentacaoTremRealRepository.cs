﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Trem.MovimentacaoTrem
{
    using ALL.Core.AcessoDados;
    using Translogic.Modules.Core.Domain.Model.Trem.MovimentacaoTrem;
    using Translogic.Modules.Core.Domain.Model.Trem.MovimentacaoTrem.Repositories;
    
    /// <summary>
    /// Implementação do Repositório de <see cref="MovimentacaoTremReal"/>
    /// </summary>
    public class MovimentacaoTremRealRepository : NHRepository<MovimentacaoTremReal, int>, IMovimentacaoTremRealRepository
    {
    }
}