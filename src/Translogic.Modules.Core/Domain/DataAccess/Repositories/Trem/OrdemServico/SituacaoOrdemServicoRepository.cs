namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Trem.OrdemServico
{
	using ALL.Core.AcessoDados;
	using Model.Trem.OrdemServico;
	using Tests.Domain.Services.Trem.OrdemService;

    /// <summary>
	/// Implementação do Repositório de <see cref="SituacaoOrdemServico"/>
	/// </summary>
	public class SituacaoOrdemServicoRepository : NHRepository<SituacaoOrdemServico, int?>, ISituacaoOrdemServicoRepository
	{
	}
}