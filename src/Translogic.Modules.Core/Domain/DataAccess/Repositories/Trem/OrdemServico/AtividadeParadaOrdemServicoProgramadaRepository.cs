namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Trem.OrdemServico
{
    using System.Collections.Generic;
    using Adaptations;
    using ALL.Core.AcessoDados;
	using Model.Trem;
    using Model.Trem.OrdemServico;
    using Model.Trem.OrdemServico.Repositories;
    using NHibernate.Criterion;

    /// <summary>
	/// Implementação do Repositório de <see cref="AtividadesParadaOrdemServicoProgramada"/>
	/// </summary>
	public class AtividadeParadaOrdemServicoProgramadaRepository : NHRepository<AtividadesParadaOrdemServicoProgramada, int?>, IAtividadeParadaOrdemServicoProgramadaRepository
	{
	}
}