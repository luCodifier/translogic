using System;
using System.Collections.Generic;
using ALL.Core.AcessoDados;
using NHibernate.Criterion;
using NHibernate.SqlCommand;
using Translogic.Modules.Core.Domain.Model.Diversos;
using Translogic.Modules.Core.Domain.Model.Trem;
using Translogic.Modules.Core.Domain.Model.Trem.OrdemServico;
using Translogic.Modules.Core.Domain.Model.Trem.OrdemServico.Repositories;

namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Trem.OrdemServico
{
    /// <summary>
    ///     Implementação do Repositório de <see cref="PlanejamentoSerieLocomotiva" />
    /// </summary>
    public class ParadaOrdemServicoProgramadaRepository : NHRepository<ParadaOrdemServicoProgramada, int?>,
        IParadaOrdemServicoProgramadaRepository
    {
        public IList<ParadaOrdemServicoProgramada> ObterParadas(int? os,
            string prefixo, string origem, string destino, DateTime dataInicial,
            DateTime dataFinal, CadCorredor corredor)
        {
            DetachedCriteria criteria = CriarCriteria();

            criteria.CreateAlias("OrdemServico", "O", JoinType.InnerJoin);
            criteria.CreateAlias("O.Origem", "OO", JoinType.InnerJoin);
            criteria.CreateAlias("O.Destino", "OD", JoinType.InnerJoin);
            criteria.CreateAlias("O.Situacao", "OS", JoinType.InnerJoin);

            //criteria.AddOrder(new Order("V.Codigo", true));

            criteria.Add(Restrictions.Eq("OS.Descricao", "PROGRAMADA"));
            criteria.Add(Restrictions.Between("O.DataPartidaPrevistaOficial", dataInicial, dataFinal));

            if (os != null)
            {
                criteria.Add(Restrictions.Eq("O.Numero", os));
            }

            if (!string.IsNullOrEmpty(prefixo))
            {
                criteria.Add(Restrictions.Eq("O.Prefixo", prefixo));
            }

            if (!string.IsNullOrEmpty(origem))
            {
                criteria.Add(Restrictions.Eq("OO.Codigo", origem));
            }

            if (!string.IsNullOrEmpty(destino))
            {
                criteria.Add(Restrictions.Eq("OD.Codigo", destino));
            }

            // TODO: Implementar filtro de corredor                
            // if (corredor != null)
            // {
            //     query = query.And(a => a.Corredor.Id == corredor);
            // }

            return ObterTodos(criteria);
        }
    }
}