namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Trem.OrdemServico
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.OracleClient;
    using System.Linq;
    using ALL.Core.AcessoDados;
    using ALL.Core.AcessoDados.TiposCustomizados;
    using Interfaces.Trem.OnTime;
    using Interfaces.Trem.OrdemServico;
    using Model.Dto;
    using Model.Trem.OrdemServico;
    using Model.Trem.OrdemServico.Repositories;
    using NHibernate;
    using NHibernate.Criterion;
    using NHibernate.Transform;
    using NHibernate.Type;

    /// <summary>
    /// Implementa��o do Reposit�rio de <see cref="OrdemServico"/>
    /// </summary>
    public class OrdemServicoRepository : NHRepository<OrdemServico, int?>, IOrdemServicoRepository
    {
        /// <summary>
        /// Insere uma altera��o de Os no OnTime
        /// </summary>
        /// <param name="objAltOSOnTime">Altera��o de Os para fins de penaliza��o no OnTime</param>
        public void InserirAlteracaoOsOnTime(AlteracaoOSOnTime objAltOSOnTime)
        {
            using (var session = OpenSession())
            {
                objAltOSOnTime.OrdemServico = session.Get<OrdemServico>(objAltOSOnTime.OrdemServico.Id);
                session.Save(objAltOSOnTime);
                session.Flush();
            }
        }

        /// <summary>
        /// Obtem todas as OSs suprimidas no intervalo informado
        /// </summary>
        /// <param name="dataInicial">Data inicial (Data oficial de confirma��o da partida prevista)</param>
        /// <param name="dataFinal">Data final (Data oficial de confirma��o da partida prevista)</param>
        /// <returns>Lista de <see cref="OrdemServico"/></returns>
        public IList<OrdemServico> ObterOSSuprimidas(DateTime dataInicial, DateTime dataFinal)
        {
            var situacaoSuprimidaEmProgramacao = new SituacaoOrdemServico { Id = (int)SituacaoOrdemServicoEnum.SuprimidaEmProgramacao };
            var situacaoSuprimidaEmPrograma = new SituacaoOrdemServico { Id = (int)SituacaoOrdemServicoEnum.SuprimidaEmPrograma };

            DetachedCriteria criteria = CriarCriteria().
                Add(
                    (Restrictions.Eq("Situacao", situacaoSuprimidaEmProgramacao) ||
                    Restrictions.Eq("Situacao", situacaoSuprimidaEmPrograma)) &&
                    Restrictions.Between("DataPartidaPrevistaOficial", dataInicial, dataFinal)
                );

            return ObterTodos(criteria);
        }

        /// <summary>
        /// Obtem as OS por <see cref="SituacaoOrdemServico"/>
        /// </summary>
        /// <param name="idTipoSituacao">Id da situa��o da OS</param>
        /// <param name="prefixo">Prefixo das ordens de servi�o</param>
        /// <param name="origem">Origem da orgem de servico</param>
        /// <param name="destino">Destino da orgem de servico</param>
        /// <param name="inicial">Data inicial (Data oficial de confirma��o da partida prevista)</param>
        /// <param name="final">Data final (Data oficial de confirma��o da partida prevista)</param>
        /// <returns>Lista de <see cref="OrdemServico"/></returns>
        public IList<OrdemServico> ObterPorTipoSituacaoEDataPartidaPrevistaOficial(int idTipoSituacao, string prefixo, string origem, string destino, DateTime inicial, DateTime final)
        {
            var situacao = new SituacaoOrdemServico { Id = idTipoSituacao };

            DetachedCriteria criteria = CriarCriteria().
                Add(
                    Restrictions.Eq("Situacao", situacao) &&
                    Restrictions.Between("DataPartidaPrevistaOficial", inicial, final)
                );

            if (!string.IsNullOrEmpty(prefixo))
            {
                criteria.Add(Restrictions.Eq("Prefixo", prefixo));
            }

            if (!string.IsNullOrEmpty(origem))
            {
                criteria.CreateAlias("Origem", "o").
                Add(Restrictions.Eq("o.Codigo", origem));
            }

            if (!string.IsNullOrEmpty(destino))
            {
                criteria.CreateAlias("Destino", "d").
                Add(Restrictions.Eq("d.Codigo", destino));
            }

            return ObterTodos(criteria);
        }

        /// <summary>
        /// Insere uma nova <see cref="OrdemServico"/>
        /// </summary>
        /// <param name="ordemServico">Ordem de servi�o que ser� persistida</param>
        /// <returns>A ordem de servi�o persistida com os dados atualizados</returns>
        public new OrdemServico Inserir(OrdemServico ordemServico)
        {
            using (ISession session = SessionManager.OpenSession())
            {
                var sqlCommand = new OracleCommand
                                     {
                                         CommandType = CommandType.StoredProcedure,
                                         CommandText = "PKG_PROGRAMACAO.SP_INS_OS",
                                         Connection = (OracleConnection)session.Connection
                                     };

                if (session.Transaction != null && session.Transaction.IsActive)
                {
                    session.Transaction.Enlist(sqlCommand);
                }

                AddParameter(sqlCommand, "pIdSitOs", OracleType.Number, ordemServico.Situacao.Id, ParameterDirection.Input);
                AddParameter(sqlCommand, "pPrfTrem", OracleType.VarChar, ordemServico.Prefixo, ParameterDirection.Input);
                AddParameter(sqlCommand, "pDataPartida", OracleType.DateTime, ordemServico.DataPartidaPrevistaGrade, ParameterDirection.Input);
                AddParameter(sqlCommand, "pDataChegada", OracleType.DateTime, ordemServico.DataChegadaPrevistaGrade, ParameterDirection.Input);
                AddParameter(sqlCommand, "pDuracao", OracleType.DateTime, ordemServico.Duracao, ParameterDirection.Input);
                AddParameter(sqlCommand, "pDataPartOficial", OracleType.DateTime, ordemServico.DataPartidaPrevistaOficial, ParameterDirection.Input);
                AddParameter(sqlCommand, "pQtdeVagVaz", OracleType.Number, ordemServico.QtdeVagoesVazios, ParameterDirection.Input);
                AddParameter(sqlCommand, "pQtdeVagCar", OracleType.Number, ordemServico.QtdeVagoesCarregados, ParameterDirection.Input);
                AddParameter(sqlCommand, "pIndGar", OracleType.VarChar, "1", ParameterDirection.Input);
                AddParameter(sqlCommand, "pDataFech", OracleType.DateTime, DBNull.Value, ParameterDirection.Input);
                AddParameter(sqlCommand, "pDataSupressao", OracleType.DateTime, DBNull.Value, ParameterDirection.Input);
                AddParameter(sqlCommand, "pIdUsuFec", OracleType.Number, DBNull.Value, ParameterDirection.Input);
                AddParameter(sqlCommand, "pIdUsuSup", OracleType.Number, DBNull.Value, ParameterDirection.Input);
                AddParameter(sqlCommand, "pIdEmpresa", OracleType.Number, 4, ParameterDirection.Input);
                AddParameter(sqlCommand, "pIdRota", OracleType.Number, ordemServico.Rota.Id, ParameterDirection.Input);
                AddParameter(sqlCommand, "pIdEstOri", OracleType.Number, ordemServico.Origem.Id, ParameterDirection.Input);
                AddParameter(sqlCommand, "pIdEstDes", OracleType.Number, ordemServico.Destino.Id, ParameterDirection.Input);
                AddParameter(sqlCommand, "pIdMotSup", OracleType.Number, DBNull.Value, ParameterDirection.Input);
                AddParameter(sqlCommand, "pResumo", OracleType.VarChar, DBNull.Value, ParameterDirection.Input);
                AddParameter(sqlCommand, "pObs", OracleType.VarChar, DBNull.Value, ParameterDirection.Input);
                AddParameter(sqlCommand, "pTipoOs", OracleType.Number, DBNull.Value, ParameterDirection.Input);
                AddParameter(sqlCommand, "pNumeroOS", OracleType.Number, DBNull.Value, ParameterDirection.Input);
                AddParameter(sqlCommand, "pCatTre", OracleType.Number, DBNull.Value, ParameterDirection.Input);
                AddParameter(sqlCommand, "pNumMaq", OracleType.Number, DBNull.Value, ParameterDirection.Input);
                AddParameter(sqlCommand, "pSitCtrl", OracleType.VarChar, DBNull.Value, ParameterDirection.Input);
                AddParameter(sqlCommand, "pClsTre", OracleType.Number, DBNull.Value, ParameterDirection.Input);
                AddParameter(sqlCommand, "pDataChegadaGrade", OracleType.DateTime, ordemServico.DataChegadaPrevistaGrade, ParameterDirection.Input);
                AddParameter(sqlCommand, "pUsuario", OracleType.VarChar, ordemServico.UsuarioCadastro.Codigo, ParameterDirection.Input);
                AddParameter(sqlCommand, "pIdOs", OracleType.Number, DBNull.Value, ParameterDirection.Output);

                sqlCommand.Prepare();
                sqlCommand.ExecuteNonQuery();

                return ObterPorId(int.Parse(sqlCommand.Parameters["pIdOs"].Value.ToString()));
            }
        }

        /// <summary>
        /// Atualizar uma nova <see cref="OrdemServico"/>
        /// </summary>
        /// <param name="atualizar">Ordem de servi�o que ser� persistida</param>
        /// <returns>A ordem de servi�o persistida com os dados atualizados</returns>
        public new OrdemServico Atualizar(OrdemServico atualizar)
        {
            using (ISession session = SessionManager.OpenSession())
            {
                var command = new OracleCommand
                                     {
                                         CommandType = CommandType.StoredProcedure,
                                         CommandText = "PKG_PROGRAMACAO.SP_UPD_OS",
                                         Connection = (OracleConnection)session.Connection
                                     };

                if (session.Transaction != null && session.Transaction.IsActive)
                {
                    session.Transaction.Enlist(command);
                }

                AddParameter(command, "pIdOs", OracleType.Number, atualizar.Id, ParameterDirection.Input);
                AddParameter(command, "pIdSitOs", OracleType.Number, atualizar.Situacao.Id, ParameterDirection.Input);
                AddParameter(command, "pPrfTrem", OracleType.VarChar, atualizar.Prefixo, ParameterDirection.Input);
                AddParameter(command, "pDataPartida", OracleType.DateTime, atualizar.DataPartidaPrevistaGrade, ParameterDirection.Input);
                AddParameter(command, "pDataChegada", OracleType.DateTime, atualizar.DataChegadaPrevistaOficial, ParameterDirection.Input);
                AddParameter(command, "pDuracao", OracleType.DateTime, atualizar.Duracao, ParameterDirection.Input);
                AddParameter(command, "pDataPartOficial", OracleType.DateTime, atualizar.DataPartidaPrevistaOficial, ParameterDirection.Input);
                AddParameter(command, "pQtdeVagVaz", OracleType.Number, atualizar.QtdeVagoesVazios, ParameterDirection.Input);
                AddParameter(command, "pQtdeVagCar", OracleType.Number, atualizar.QtdeVagoesCarregados, ParameterDirection.Input);
                AddParameter(command, "pIndGar", OracleType.VarChar, atualizar.Garantido.Value ? '1' : '0', ParameterDirection.Input);
                AddParameter(command, "pDataFech", OracleType.DateTime, atualizar.DataFechamento, ParameterDirection.Input);
                AddParameter(command, "pDataSupressao", OracleType.DateTime, atualizar.DataSupressao, ParameterDirection.Input);
                AddParameter(command, "pIdUsuFec", OracleType.Number, (atualizar.UsuarioFechamento == null ? (object)DBNull.Value : atualizar.UsuarioFechamento.Id), ParameterDirection.Input);
                AddParameter(command, "pIdUsuSup", OracleType.Number, (atualizar.UsuarioSupressao == null ? (object)DBNull.Value : atualizar.UsuarioSupressao.Id), ParameterDirection.Input);
                AddParameter(command, "pIdEmpresa", OracleType.Number, (atualizar.Empresa == null ? (object)DBNull.Value : atualizar.Empresa.Id), ParameterDirection.Input);
                AddParameter(command, "pIdRota", OracleType.Number, atualizar.Rota.Id, ParameterDirection.Input);
                AddParameter(command, "pIdEstOri", OracleType.Number, atualizar.Origem.Id, ParameterDirection.Input);
                AddParameter(command, "pIdEstDes", OracleType.Number, atualizar.Destino.Id, ParameterDirection.Input);
                AddParameter(command, "pIdMotSup", OracleType.Number, DBNull.Value, ParameterDirection.Input);
                AddParameter(command, "pIdObs", OracleType.Number, DBNull.Value, ParameterDirection.Input);
                AddParameter(command, "pResumo", OracleType.VarChar, DBNull.Value, ParameterDirection.Input);
                AddParameter(command, "pObs", OracleType.VarChar, DBNull.Value, ParameterDirection.Input);
                AddParameter(command, "pTipoOs", OracleType.Number, DBNull.Value, ParameterDirection.Input);
                AddParameter(command, "pNumeroOS", OracleType.Number, atualizar.Numero, ParameterDirection.Input);
                AddParameter(command, "pCatTre", OracleType.Number, DBNull.Value, ParameterDirection.Input);
                AddParameter(command, "pNumMaq", OracleType.Number, DBNull.Value, ParameterDirection.Input);
                AddParameter(command, "pSitCtrl", OracleType.VarChar, DBNull.Value, ParameterDirection.Input);
                AddParameter(command, "pClsTre", OracleType.Number, DBNull.Value, ParameterDirection.Input);
                AddParameter(command, "pDataChegadaGrade", OracleType.DateTime, atualizar.DataChegadaPrevistaGrade, ParameterDirection.Input);

                command.Prepare();
                command.ExecuteNonQuery();

                return ObterPorId(atualizar.Id);
            }
        }

        /// <summary>
        /// Atualizar uma nova <see cref="OrdemServico"/>
        /// </summary>
        /// <param name="idOs"> Id da Ordem de servi�o que ser� persistida</param>
        /// <param name="dataPartida"> Data Partida da Ordem de servi�o que ser� persistida</param>
        /// <param name="usuario">Usuario que solicitou a altera��o.</param>
        /// <returns>A ordem de servi�o persistida com os dados atualizados</returns>
        public OrdemServico AtualizarDataPartidaOficial(int idOs, DateTime dataPartida, string usuario)
        {
            using (ISession session = SessionManager.OpenSession())
            {
                var command = new OracleCommand
                {
                    CommandType = CommandType.StoredProcedure,
                    CommandText = "PKG_PROGRAMACAO.SP_UPD_PARTIDA_OFICIAL",
                    Connection = (OracleConnection)session.Connection
                };

                if (session.Transaction != null && session.Transaction.IsActive)
                {
                    session.Transaction.Enlist(command);
                }

                AddParameter(command, "pIdOs", OracleType.Number, idOs, ParameterDirection.Input);
                AddParameter(command, "pDataPartida", OracleType.DateTime, dataPartida, ParameterDirection.Input);
                AddParameter(command, "pUsuario", OracleType.VarChar, usuario, ParameterDirection.Input);

                command.Prepare();
                command.ExecuteNonQuery();

                return ObterPorId(idOs);
            }
        }

        /// <summary>
        /// Fechar o planejamento de ordem servi�o da grade
        /// </summary>
        /// <param name="ordemServico">Ordem de servico que ser� fechado o planejamento</param>
        /// <returns>Ordem de servi�o atualizada</returns>
        public OrdemServico FecharPlanejamento(OrdemServico ordemServico)
        {
            using (ISession session = SessionManager.OpenSession())
            {
                var command = new OracleCommand
                {
                    CommandType = CommandType.StoredProcedure,
                    CommandText = "PKG_PROGRAMACAO.SP_UPD_CLONAR_OS",
                    Connection = (OracleConnection)session.Connection
                };

                if (session.Transaction != null && session.Transaction.IsActive)
                {
                    session.Transaction.Enlist(command);
                }

                AddParameter(command, "pIdOs", OracleType.Number, ordemServico.Id, ParameterDirection.Input);
                AddParameter(command, "pIdUsuario", OracleType.Number, ordemServico.UsuarioFechamento.Id, ParameterDirection.Input);

                command.Prepare();
                command.ExecuteNonQuery();

                return ObterPorId(ordemServico.Id);
            }
        }

        /// <summary>
        /// Fechar a programacao da ordem servi�o
        /// </summary>
        /// <param name="ordemServico">Ordem de servico que ser� fechada a programa��o</param>
        /// <returns>Ordem de servi�o atualizada</returns>
        public OrdemServico FecharProgramacao(OrdemServico ordemServico)
        {
            using (ISession session = SessionManager.OpenSession())
            {
                var command = new OracleCommand
                {
                    CommandType = CommandType.StoredProcedure,
                    CommandText = "PKG_PROGRAMACAO.SP_UPD_FECHAR_OS",
                    Connection = (OracleConnection)session.Connection
                };

                if (session.Transaction != null && session.Transaction.IsActive)
                {
                    session.Transaction.Enlist(command);
                }

                AddParameter(command, "pIdOs", OracleType.Number, ordemServico.Id, ParameterDirection.Input);
                AddParameter(command, "pIdSituacao", OracleType.Number, SituacaoOrdemServicoEnum.Programada, ParameterDirection.Input);
                AddParameter(command, "pIdUsuario", OracleType.Number, ordemServico.UsuarioFechamento.Id, ParameterDirection.Input);

                command.Prepare();
                command.ExecuteNonQuery();

                return ObterPorId(ordemServico.Id);
            }
        }

        /// <summary>
        /// Suprimir (cancelar) ordem servi�o
        /// </summary>
        /// <param name="ordemServico">Ordem de servico que ser� suprimida</param>
        public void Suprimir(OrdemServico ordemServico)
        {
            Atualizar(ordemServico);
        }

        /// <summary>
        /// Obtem uma OS pelo id da pre OS
        /// </summary>
        /// <param name="idPreOs">Id da pre OS</param>
        /// <returns>A OrdemServico desta PreOS</returns>
        public OrdemServico ObterPorIdPreOs(int idPreOs)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.Eq("IdPreOs", idPreOs));

            return ObterPrimeiro(criteria);
        }

        /// <summary> 
        /// Obt�m a ordem de servi�o pelo n�mero
        /// </summary>
        /// <param name="numeroOrdemServico"> Numero da ordem servico. </param>
        /// <returns> Objeto <see cref="OrdemServico"/> </returns>
        public OrdemServico ObterPorNumero(int numeroOrdemServico)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.Eq("Numero", numeroOrdemServico));

            return ObterPrimeiro(criteria);
        }

        /// <summary> 
        /// Obt�m as locomotivas da ordem de servi�o pelo n�mero
        /// </summary>
        /// <param name="numeroOrdemServico"> Numero da ordem servico. </param>
        /// <returns> Lista de locomotivas na OS </returns>
        public IList<LocomotivaDto> ObterLocomotivasPorOS(int numeroOrdemServico)
        {
            using (IStatelessSession session = SessionManager.OpenStatelessSession())
            {
                var query = session.CreateSQLQuery(@"
SELECT L.COD_LOCOMOTIVA AS ""Locomotiva"",
       CL.SEQ_COMP AS ""Sequencia"",
       CL.POSICAO AS ""Posicao"",
       CL.COMANDO AS ""Comando"",
       CL.TRACAO AS ""Tracao""
FROM VW_CCP_TREM        VV,
       VW_CCP_COMPOSICAO  CP,
       VW_CCP_COMPLOCOMOT CL,
       VW_CCP_LOCOMOTIVA  L
where (VV.ID_TREM = CP.ID_TREM AND CP.ID_COMPOSICAO = CL.ID_COMPOSICAO AND
       CL.ID_LOCO = L.ID_LOCOMOTIVA)
       AND VV.NRO_OS = :numeroOS
ORDER BY SEQ_COMP");

                query.SetParameter("numeroOS", numeroOrdemServico);
                query.AddScalar("Locomotiva", NHibernateUtil.AnsiString);
                query.AddScalar("Sequencia", NHibernateUtil.Int32);
                query.AddScalar("Posicao", NHibernateUtil.AnsiString);
                query.AddScalar("Comando", NHibernateUtil.AnsiString);
                query.AddScalar("Tracao", NHibernateUtil.AnsiString);
                query.SetResultTransformer(Transformers.AliasToBean<LocomotivaDto>());
                return query.List<LocomotivaDto>();
            }
        }

        /// <summary>
        /// Obtem a lista de OSs para OnTime
        /// </summary>
        /// <param name="requisicao">Requisi��o de pesquisa</param>
        /// <returns>OSs encontradas para o intervalo</returns>
        public IList<OrdemServicoOnTimeDto> ObterOrdensServicoOnTime(RequisicaoOrdemServicoOnTime requisicao)
        {
            string sqlquery = @"SELECT	DISTINCT OS.X1_ID_OS            ""Id"",
                                        OS.X1_USR_CADASTRO              ""MatriculaCadastro"",
                                        OS.X1_DAT_PAR_PRV_OFI           ""DataPartidaUltimaAlteracao"",
										OS.X1_NRO_OS                    ""Numero"",
										OS.X1_PFX_TRE                   ""Prefixo"",
										ORI.AO_COD_AOP                  ""AreaOperacionalOrigem"",
										UPORI.UP_DSC_RSM                ""UnidadeProducaoOrigem"",
										DES.AO_COD_AOP                  ""AreaOperacionalDestino"",
										UPDES.UP_DSC_RSM                ""UnidadeProducaoDestino"",
										CASE WHEN TRE.TR_STT_TRM = 'E' AND TRE.TR_MAT_CLD IS NOT NULL and tre.tr_dat_cld is not null THEN 
											3 /* CANCELADA */
										ELSE
											DECODE(OS.X1_IDT_SIT_OS,
												14,
												1, /* PROGRAMADA */
												15,
												2, /* SUPRIMIDA */
												16,
												2 /* SUPRIMIDA */)
										END                             ""IdSituacao"",
										OS.X1_DAT_PAR_PRV_OFI           ""DataPartidaPrevista"",    
										OS.X1_DAT_CHE_PRV               ""DataChegadaPrevista"",
										OS.X1_DAT_SUP                   ""DataSupressao"",
										OS.X1_DT_CADASTRO               ""DataCadastro"",
										LOG.DT_ANTERIOR                 ""DataPartidaAnterior"",
										OS.X1_DAT_PAR_PRV_GRA           ""DataPartidaPrevistaGrade"",
										LOG.OA_TIMESTAMP                ""DataEdicaoPartidaAnterior"",
                                        TRE.TR_DAT_LBR                  ""DataLiberacaoTrem"",
                                        LOG.US_USR_IDU                  ""MatriculaUltimaAlteracao""
								FROM T2_OS                              OS,
									OS_ALTERADAS_D_D1                   LOG,
									AREA_OPERACIONAL                    ORI,
									AREA_OPERACIONAL                    DES,
									UNID_PRODUCAO                       UPORI,
									UNID_PRODUCAO                       UPDES,
									TREM                                TRE
								WHERE OS.X1_ID_OS = LOG.ID_OS(+)
										AND OS.X1_IDT_EST_ORI = ORI.AO_ID_AO(+)
										AND OS.X1_IDT_EST_DES = DES.AO_ID_AO(+)
										AND ORI.UP_ID_UNP = UPORI.UP_ID_UNP(+)
										AND DES.UP_ID_UNP = UPDES.UP_ID_UNP(+)
										AND OS.X1_ID_OS = TRE.OF_ID_OSV(+)
										AND OS.X1_IDT_SIT_OS IN (14, 15, 16)";

            // Consulta parametrizada para datas
            if (requisicao.DataInicialPartida.HasValue &&
                requisicao.DataFinalPartida.HasValue)
            {
                sqlquery += " AND OS.X1_DAT_PAR_PRV_OFI BETWEEN :dataInicialPartida AND :dataFinalPartida";
            }

            if (requisicao.DataInicialEdicao.HasValue && requisicao.DataFinalEdicao.HasValue)
            {
                sqlquery += " AND LOG.OA_TIMESTAMP BETWEEN :dataInicialEdicao AND :dataFinalEdicao";
            }

            if (requisicao.DataInicialSupressao.HasValue &&
                requisicao.DataFinalSupressao.HasValue)
            {
                sqlquery += " AND OS.X1_DAT_SUP BETWEEN :dataInicialSupressao AND :dataFinalSupressao";
            }

            if (requisicao.DataInicialChegada.HasValue &&
                requisicao.DataFinalChegada.HasValue)
            {
                sqlquery += " AND OS.X1_DAT_CHE_PRV BETWEEN :dataInicialChegada AND :dataFinalChegada";
            }

            if (requisicao.DataInicialCadastro.HasValue &&
                requisicao.DataFinalCadastro.HasValue)
            {
                sqlquery += " AND OS.X1_DT_CADASTRO BETWEEN :dataInicialCadastro AND :dataFinalCadastro";
            }

            // Pesquisa por prefixo
            if (!string.IsNullOrWhiteSpace(requisicao.Prefixo))
            {
                sqlquery += " AND OS.X1_PFX_TRE = :prefixo";
            }

            // Prefixo incluidos
            if (!string.IsNullOrWhiteSpace(requisicao.PrefixosIncluir))
            {
                sqlquery += " AND SUBSTR(OS.X1_PFX_TRE, 1, 1) IN (:prefixosIncluir)";
            }

            // Prefixos excluidos
            if (!string.IsNullOrWhiteSpace(requisicao.PrefixosExcluir))
            {
                sqlquery += " AND SUBSTR(OS.X1_PFX_TRE, 1, 1) NOT IN (:prefixosExcluir)";
            }

            // UP de origem
            if (!string.IsNullOrWhiteSpace(requisicao.UnidadeProducaoOrigem))
            {
                sqlquery += " AND UPORI.UP_DSC_RSM IN (:unidadeOrigem)";
            }

            // UP de destino
            if (!string.IsNullOrWhiteSpace(requisicao.UnidadeProducaoDestino))
            {
                sqlquery += " AND UPDES.UP_DSC_RSM IN (:unidadeDestino)";
            }

            // Esta��o de origem
            if (!string.IsNullOrWhiteSpace(requisicao.AreaOperacionalOrigem))
            {
                sqlquery += " AND ORI.AO_COD_AOP IN (:aoOrigem)";
            }

            // Esta��o de destino
            if (!string.IsNullOrWhiteSpace(requisicao.AreaOperacionalDestino))
            {
                sqlquery += " AND DES.AO_COD_AOP IN (:aoDestino)";
            }

            // Esta��o de destino
            if (!string.IsNullOrWhiteSpace(requisicao.NumeroOrdemServico))
            {
                sqlquery += " AND OS.X1_NRO_OS = :ordemServico";
            }

            using (IStatelessSession session = SessionManager.OpenStatelessSession())
            {
                ISQLQuery query = session.CreateSQLQuery(sqlquery);

                // Consulta parametrizada para datas
                if (requisicao.DataInicialPartida.HasValue &&
                    requisicao.DataFinalPartida.HasValue)
                {
                    query.SetDateTime("dataInicialPartida", requisicao.DataInicialPartida.Value);
                    query.SetDateTime("dataFinalPartida", requisicao.DataFinalPartida.Value);
                }

                if (requisicao.DataInicialChegada.HasValue &&
                    requisicao.DataFinalChegada.HasValue)
                {
                    query.SetDateTime("dataInicialChegada", requisicao.DataInicialChegada.Value);
                    query.SetDateTime("dataFinalChegada", requisicao.DataFinalChegada.Value);
                }

                if (requisicao.DataInicialSupressao.HasValue &&
                    requisicao.DataFinalSupressao.HasValue)
                {
                    query.SetDateTime("dataInicialSupressao", requisicao.DataInicialSupressao.Value);
                    query.SetDateTime("dataFinalSupressao", requisicao.DataFinalSupressao.Value);
                }

                if (requisicao.DataInicialEdicao.HasValue &&
                    requisicao.DataFinalEdicao.HasValue)
                {
                    query.SetDateTime("dataInicialEdicao", requisicao.DataInicialEdicao.Value);
                    query.SetDateTime("dataFinalEdicao", requisicao.DataFinalEdicao.Value);
                }

                if (requisicao.DataInicialCadastro.HasValue &&
                    requisicao.DataFinalCadastro.HasValue)
                {
                    query.SetDateTime("dataInicialCadastro", requisicao.DataInicialCadastro.Value);
                    query.SetDateTime("dataFinalCadastro", requisicao.DataFinalCadastro.Value);
                }

                // Pesquisa por prefixo
                if (!string.IsNullOrWhiteSpace(requisicao.Prefixo))
                {
                    query.SetString("prefixo", requisicao.Prefixo);
                }

                // Prefixo incluidos
                if (!string.IsNullOrWhiteSpace(requisicao.PrefixosIncluir))
                {
                    string[] prefixos = requisicao.PrefixosIncluir.Split(',').Select(p => p.Trim()).ToArray();

                    query.SetParameterList("prefixosIncluir", prefixos);
                }

                // Prefixos excluidos
                if (!string.IsNullOrWhiteSpace(requisicao.PrefixosExcluir))
                {
                    string[] prefixos = requisicao.PrefixosExcluir.Split(',').Select(p => p.Trim()).ToArray();

                    query.SetParameterList("prefixosExcluir", prefixos);
                }

                // UP de origem
                if (!string.IsNullOrWhiteSpace(requisicao.UnidadeProducaoOrigem))
                {
                    string[] ups = requisicao.UnidadeProducaoOrigem.Split(',').Select(p => p.Trim()).ToArray();

                    query.SetParameterList("unidadeOrigem", ups);
                }

                // UP de destino
                if (!string.IsNullOrWhiteSpace(requisicao.UnidadeProducaoDestino))
                {
                    string[] ups = requisicao.UnidadeProducaoDestino.Split(',').Select(p => p.Trim()).ToArray();

                    query.SetParameterList("unidadeDestino", ups);
                }

                // Esta��o de origem
                if (!string.IsNullOrWhiteSpace(requisicao.AreaOperacionalOrigem))
                {
                    string[] ups = requisicao.AreaOperacionalOrigem.Split(',').Select(p => p.Trim()).ToArray();

                    query.SetParameterList("aoOrigem", ups);
                }

                // Esta��o de destino
                if (!string.IsNullOrWhiteSpace(requisicao.AreaOperacionalDestino))
                {
                    string[] ups = requisicao.AreaOperacionalDestino.Split(',').Select(p => p.Trim()).ToArray();

                    query.SetParameterList("aoDestino", ups);
                }

                // Numero
                if (!string.IsNullOrWhiteSpace(requisicao.NumeroOrdemServico))
                {
                    query.SetString("ordemServico", requisicao.NumeroOrdemServico);
                }

                query.SetResultTransformer(Transformers.AliasToBean<OrdemServicoOnTimeDto>());
                return query.List<OrdemServicoOnTimeDto>();
            }
        }

        /// <summary>
        /// Obtem a lista de Partidas de OSs para OnTime
        /// </summary>
        /// <param name="codigoUpOrigem">Up de Origem</param>
        /// <param name="codigoUpDestino">Up de Destino</param>
        /// <param name="estOrigem">Esta��o de Origem</param>
        /// <param name="estDestino">Esta��o de Destino</param>
        /// <param name="prefixo">Prefixo do trem</param>
        /// <param name="numOS">N�mero da OS</param>
        /// <param name="dataCadastroInicial">Data de cadastro inicial</param>
        /// <param name="dataCadastroFinal">Data de cadastro final</param>
        /// <param name="dataEdicaoInicial">Data de edi��o inicial</param>
        /// <param name="dataEdicaoFinal">Data de edi��o final</param>
        /// <param name="dataSupressaoInicial">Data de supress�o inicial</param>
        /// <param name="dataSupressaoFinal">Data de supress�o final</param>
        /// <param name="partidaPlanejadaInicial">Partida planejada inicial</param>
        /// <param name="partidaPlanejadaFinal">Partida planejada Final</param>
        /// <param name="chegadaPlanejadaInicial">Chegada planejada inicial</param>
        /// <param name="chegadaPlanejadaFinal">Chegada planejada Final</param>
        /// <param name="prefixosIncluir">Prefixos a incluir</param>
        /// <param name="prefixosExcluir">Prefixos a excluir</param>
        /// <returns>OSs encontradas para o intervalo</returns>
        public IList<PartidaTremOnTime> ObterPartidasOnTime(
            string codigoUpOrigem,
            string codigoUpDestino,
            string estOrigem,
            string estDestino,
            string prefixo,
            string numOS,
            DateTime? dataCadastroInicial,
            DateTime? dataCadastroFinal,
            DateTime? dataEdicaoInicial,
            DateTime? dataEdicaoFinal,
            DateTime? dataSupressaoInicial,
            DateTime? dataSupressaoFinal,
            DateTime? partidaPlanejadaInicial,
            DateTime? partidaPlanejadaFinal,
            DateTime? chegadaPlanejadaInicial,
            DateTime? chegadaPlanejadaFinal,
            string prefixosIncluir,
            string prefixosExcluir)
        {
            string sqlquery = @"
SELECT DISTINCT OS.X1_ID_OS ""Id"",
                OS.X1_USR_CADASTRO ""MatriculaCadastro"",
                OS.X1_DAT_PAR_PRV_OFI ""DataPartidaUltimaAlteracao"",
                OS.X1_NRO_OS ""Numero"",
                OS.X1_PFX_TRE ""Prefixo"",
                ORI.AO_COD_AOP ""AreaOperacionalOrigem"",
                UPORI.UP_DSC_RSM ""UnidadeProducaoOrigem"",
                DES.AO_COD_AOP ""AreaOperacionalDestino"",
                UPDES.UP_DSC_RSM ""UnidadeProducaoDestino"",
                CASE
                  WHEN TRE.TR_STT_TRM = 'E' AND TRE.TR_MAT_CLD IS NOT NULL and
                       tre.tr_dat_cld is not null THEN
                   'C' /* CANCELADA */
                  ELSE
                   DECODE(OS.X1_IDT_SIT_OS,
                          14,
                          'P', /* PROGRAMADA */
                          15,
                          'S', /* SUPRIMIDA */
                          16,
                          'S' /* SUPRIMIDA */)
                END ""SituacaoOS"",
                (CASE
                  WHEN MTRE.DATA_PARTIDA_ORI IS NULL 
                        AND MIN(MT.MT_DTS_RAL) IS NOT NULL 
                        AND AOT.AOT_ID_AONT IS NULL 
                        AND TRE.TR_MAT_CLD IS NULL
                    THEN    'S'
                  ELSE      'N'
                END) ""PermitirAlteracaoOnTime"",
                NVL(AOT.AOT_STT_TREM, 
                CASE
                  WHEN ((AOT.AOT_ID_AONT IS NOT NULL AND AOT.AOT_ACAO_ALTER = 'P') OR MTRE.SITUACAO_TREM_PX IS NULL) OR TRE.TR_MAT_CLD IS NOT NULL
                            THEN    (CASE 
                                        WHEN TRE.TR_MAT_CLD IS NOT NULL THEN 'CND'
                                        WHEN OS.X1_IDT_SIT_OS = 15 OR OS.X1_IDT_SIT_OS = 16
                                          THEN 'SPD'
                                        WHEN TRE.TR_STT_TRM = 'E' THEN 'ENC'
                                        WHEN TRE.TR_STT_TRM = 'C' THEN 'CLO'
                                        WHEN TRE.TR_STT_TRM = 'R' THEN 'CLO'
                                        WHEN TRE.TR_STT_TRM = 'P' THEN 'PGD'
                                        WHEN OS.X1_IDT_SIT_OS = 14 THEN 'PGD'
                                        ELSE 'IND'
                                    END)
                  ELSE      MTRE.SITUACAO_TREM_PX
                END) ""Situacao"",
                OS.X1_DAT_PAR_PRV_OFI ""DataPartidaPrevista"",
                OS.X1_DAT_CHE_PRV ""DataChegadaPrevista"",
                OS.X1_DAT_SUP ""DataSupressao"",
                OS.X1_DT_CADASTRO ""DataCadastro"",
                LOG.DT_ANTERIOR ""DataPartidaAnterior"",
                OS.X1_DAT_PAR_PRV_GRA ""DataPartidaPrevistaGrade"",
                LOG.OA_TIMESTAMP ""DataEdicaoPartidaAnterior"",
                TRE.TR_DAT_LBR ""DataLiberacaoTrem"",
                LOG.US_USR_IDU ""MatriculaUltimaAlteracao"",
                NVL(AOT.AOT_DTA_PARTIDA_REAL, MTRE.DATA_PARTIDA_ORI) ""DataPartidaReal"",
                MTRE.DATA_CHEGADA_DST ""DataChegadaReal"",
                OS.X1_DAT_PAR_PRV_OFI ""DataPartidaOficial"",
                OS.X1_DAT_CHE_PRV ""DataChegadaOficial"",
                MIN(MT.MT_DTS_RAL) ""DataPartidaTranslogic"",
                (CASE WHEN AOT.AOT_ID_AONT IS NOT NULL
                    THEN 'S'
                    ELSE 'N'
                END) ""AlteradoTranslogic""
FROM T2_OS             OS,
     OS_ALTERADAS_D_D1 LOG,
     AREA_OPERACIONAL  ORI,
     AREA_OPERACIONAL  DES,
     UNID_PRODUCAO     UPORI,
     UNID_PRODUCAO     UPDES,
     TREM              TRE,
     MOVIMENTACAO_TREM_REAL MTRE,
     MOVIMENTACAO_TREM MT,
     ALTERACOES_ONTIME AOT
 WHERE OS.X1_ID_OS = LOG.ID_OS(+)
   AND OS.X1_IDT_EST_ORI = ORI.AO_ID_AO(+)
   AND OS.X1_IDT_EST_DES = DES.AO_ID_AO(+)
   AND ORI.UP_ID_UNP = UPORI.UP_ID_UNP(+)
   AND DES.UP_ID_UNP = UPDES.UP_ID_UNP(+)
   AND OS.X1_ID_OS = TRE.OF_ID_OSV(+)
   AND OS.X1_ID_OS = AOT.AOT_ID_OS(+)
   AND OS.X1_IDT_SIT_OS IN (14, 15, 16)
   AND MTRE.NUM_OS(+) = OS.X1_NRO_OS
   AND MT.TR_ID_TRM(+) = TRE.TR_ID_TRM";

            // Consulta parametrizada para datas
            if (partidaPlanejadaInicial.HasValue && partidaPlanejadaFinal.HasValue)
            {
                sqlquery += " AND OS.X1_DAT_PAR_PRV_OFI BETWEEN :dataInicialPartida AND :dataFinalPartida";
            }

            if (dataEdicaoInicial.HasValue && dataEdicaoFinal.HasValue)
            {
                sqlquery += " AND LOG.OA_TIMESTAMP BETWEEN :dataInicialEdicao AND :dataFinalEdicao";
            }

            if (dataSupressaoInicial.HasValue && dataSupressaoFinal.HasValue)
            {
                sqlquery += " AND OS.X1_DAT_SUP BETWEEN :dataInicialSupressao AND :dataFinalSupressao";
            }

            if (chegadaPlanejadaInicial.HasValue && chegadaPlanejadaFinal.HasValue)
            {
                sqlquery += " AND OS.X1_DAT_CHE_PRV BETWEEN :dataInicialChegada AND :dataFinalChegada";
            }

            if (dataCadastroInicial.HasValue && dataCadastroFinal.HasValue)
            {
                sqlquery += " AND OS.X1_DT_CADASTRO BETWEEN :dataInicialCadastro AND :dataFinalCadastro";
            }

            // Pesquisa por prefixo
            if (!string.IsNullOrWhiteSpace(prefixo))
            {
                sqlquery += " AND OS.X1_PFX_TRE = :prefixo";
            }

            // Prefixo incluidos
            if (!string.IsNullOrWhiteSpace(prefixosIncluir))
            {
                sqlquery += " AND SUBSTR(OS.X1_PFX_TRE, 1, 1) IN (:prefixosIncluir)";
            }

            // Prefixos excluidos
            if (!string.IsNullOrWhiteSpace(prefixosExcluir))
            {
                sqlquery += " AND SUBSTR(OS.X1_PFX_TRE, 1, 1) NOT IN (:prefixosExcluir)";
            }

            // UP de origem
            if (!string.IsNullOrWhiteSpace(codigoUpOrigem))
            {
                sqlquery += " AND UPORI.UP_DSC_RSM IN (:unidadeOrigem)";
            }

            // UP de destino
            if (!string.IsNullOrWhiteSpace(codigoUpDestino))
            {
                sqlquery += " AND UPDES.UP_DSC_RSM IN (:unidadeDestino)";
            }

            // Esta��o de origem
            if (!string.IsNullOrWhiteSpace(estOrigem))
            {
                sqlquery += " AND ORI.AO_COD_AOP IN (:aoOrigem)";
            }

            // Esta��o de destino
            if (!string.IsNullOrWhiteSpace(estDestino))
            {
                sqlquery += " AND DES.AO_COD_AOP IN (:aoDestino)";
            }

            // Esta��o de destino
            if (!string.IsNullOrWhiteSpace(numOS))
            {
                sqlquery += " AND OS.X1_NRO_OS = :ordemServico";
            }

            sqlquery +=
                @"
 GROUP BY OS.X1_ID_OS,
          OS.X1_USR_CADASTRO,
          OS.X1_DAT_PAR_PRV_OFI,
          OS.X1_NRO_OS,
          OS.X1_PFX_TRE,
          ORI.AO_COD_AOP,
          UPORI.UP_DSC_RSM,
          DES.AO_COD_AOP,
          UPDES.UP_DSC_RSM,
          OS.X1_DAT_PAR_PRV_OFI,
          OS.X1_DAT_CHE_PRV,
          OS.X1_DAT_SUP,
          OS.X1_DT_CADASTRO,
          LOG.DT_ANTERIOR,
          OS.X1_DAT_PAR_PRV_GRA,
          LOG.OA_TIMESTAMP,
          TRE.TR_DAT_LBR,
          LOG.US_USR_IDU,
          AOT.AOT_DTA_PARTIDA_REAL, MTRE.DATA_PARTIDA_ORI,
          MTRE.DATA_CHEGADA_DST,
          OS.X1_DAT_PAR_PRV_OFI,
          OS.X1_DAT_CHE_PRV,
          TRE.TR_STT_TRM,
          TRE.TR_MAT_CLD,
          tre.tr_dat_cld,
          OS.X1_IDT_SIT_OS,
          AOT.AOT_STT_TREM,
          MTRE.SITUACAO_TREM_PX,
          AOT.AOT_ID_AONT,
          AOT.AOT_ACAO_ALTER";

            using (IStatelessSession session = SessionManager.OpenStatelessSession())
            {
                ISQLQuery query = session.CreateSQLQuery(sqlquery);

                // Consulta parametrizada para datas
                if (partidaPlanejadaInicial.HasValue && partidaPlanejadaFinal.HasValue)
                {
                    var dataPartidaPlanejadaInicial = new DateTime(partidaPlanejadaInicial.Value.Year, partidaPlanejadaInicial.Value.Month, partidaPlanejadaInicial.Value.Day, 0, 0, 0);
                    var dataPartidaPlanejadaFinal = new DateTime(partidaPlanejadaFinal.Value.Year, partidaPlanejadaFinal.Value.Month, partidaPlanejadaFinal.Value.Day, 23, 59, 59);

                    query.SetDateTime("dataInicialPartida", dataPartidaPlanejadaInicial);
                    query.SetDateTime("dataFinalPartida", dataPartidaPlanejadaFinal);
                }

                if (chegadaPlanejadaInicial.HasValue && chegadaPlanejadaFinal.HasValue)
                {
                    query.SetDateTime("dataInicialChegada", chegadaPlanejadaInicial.Value);
                    query.SetDateTime("dataFinalChegada", chegadaPlanejadaFinal.Value);
                }

                if (dataSupressaoInicial.HasValue && dataSupressaoFinal.HasValue)
                {
                    query.SetDateTime("dataInicialSupressao", dataSupressaoInicial.Value);
                    query.SetDateTime("dataFinalSupressao", dataSupressaoFinal.Value);
                }

                if (dataEdicaoInicial.HasValue && dataEdicaoFinal.HasValue)
                {
                    query.SetDateTime("dataInicialEdicao", dataEdicaoInicial.Value);
                    query.SetDateTime("dataFinalEdicao", dataEdicaoFinal.Value);
                }

                if (dataCadastroInicial.HasValue && dataCadastroFinal.HasValue)
                {
                    query.SetDateTime("dataInicialCadastro", dataCadastroInicial.Value);
                    query.SetDateTime("dataFinalCadastro", dataCadastroFinal.Value);
                }

                // Pesquisa por prefixo
                if (!string.IsNullOrWhiteSpace(prefixo))
                {
                    query.SetString("prefixo", prefixo.ToUpper().Trim());
                }
                // Prefixo incluidos
                if (!string.IsNullOrWhiteSpace(prefixosIncluir))
                {
                    string[] prefixos = prefixosIncluir.Split(',').Select(p => p.ToUpper().Trim()).ToArray();

                    query.SetParameterList("prefixosIncluir", prefixos);
                }

                // Prefixos excluidos
                if (!string.IsNullOrWhiteSpace(prefixosExcluir))
                {
                    string[] prefixos = prefixosExcluir.Split(',').Select(p => p.ToUpper().Trim()).ToArray();

                    query.SetParameterList("prefixosExcluir", prefixos);
                }

                // UP de origem
                if (!string.IsNullOrWhiteSpace(codigoUpOrigem))
                {
                    string[] ups = codigoUpOrigem.Split(',').Select(p => p.ToUpper().Trim()).ToArray();

                    query.SetParameterList("unidadeOrigem", ups);
                }

                // UP de destino
                if (!string.IsNullOrWhiteSpace(codigoUpDestino))
                {
                    string[] ups = codigoUpDestino.Split(',').Select(p => p.ToUpper().Trim()).ToArray();

                    query.SetParameterList("unidadeDestino", ups);
                }

                // Esta��o de origem
                if (!string.IsNullOrWhiteSpace(estOrigem))
                {
                    string[] ups = estOrigem.Split(',').Select(p => p.ToUpper().Trim()).ToArray();

                    query.SetParameterList("aoOrigem", ups);
                }

                // Esta��o de destino
                if (!string.IsNullOrWhiteSpace(estDestino))
                {
                    string[] ups = estDestino.Split(',').Select(p => p.ToUpper().Trim()).ToArray();

                    query.SetParameterList("aoDestino", ups);
                }

                // Numero
                if (!string.IsNullOrWhiteSpace(numOS))
                {
                    query.SetString("ordemServico", numOS);
                }

                query.AddScalar("Id", NHibernateUtil.Int64);
                query.AddScalar("MatriculaCadastro", NHibernateUtil.String);
                query.AddScalar("MatriculaUltimaAlteracao", NHibernateUtil.String);
                query.AddScalar("DataPartidaUltimaAlteracao", NHibernateUtil.DateTime);
                query.AddScalar("Numero", NHibernateUtil.Int64);
                query.AddScalar("Prefixo", NHibernateUtil.String);
                query.AddScalar("AreaOperacionalOrigem", NHibernateUtil.String);
                query.AddScalar("UnidadeProducaoOrigem", NHibernateUtil.String);
                query.AddScalar("AreaOperacionalDestino", NHibernateUtil.String);
                query.AddScalar("UnidadeProducaoDestino", NHibernateUtil.String);
                query.AddScalar("SituacaoOS", new EnumCharType<SituacaoOS>());
                query.AddScalar("Situacao", new DescribedEnumStringType<SituacaoTrem>());
                query.AddScalar("DataPartidaPrevista", NHibernateUtil.DateTime);
                query.AddScalar("DataChegadaPrevista", NHibernateUtil.DateTime);
                query.AddScalar("DataSupressao", NHibernateUtil.DateTime);
                query.AddScalar("DataCadastro", NHibernateUtil.DateTime);
                query.AddScalar("DataPartidaAnterior", NHibernateUtil.DateTime);
                query.AddScalar("DataEdicaoPartidaAnterior", NHibernateUtil.DateTime);
                query.AddScalar("DataLiberacaoTrem", NHibernateUtil.DateTime);
                query.AddScalar("DataPartidaReal", NHibernateUtil.DateTime);
                query.AddScalar("DataChegadaReal", NHibernateUtil.DateTime);
                query.AddScalar("DataPartidaOficial", NHibernateUtil.DateTime);
                query.AddScalar("DataChegadaOficial", NHibernateUtil.DateTime);
                query.AddScalar("DataPartidaTranslogic", NHibernateUtil.DateTime);
                query.AddScalar("AlteradoTranslogic", new BooleanCharType());
                query.AddScalar("PermitirAlteracaoOnTime", new BooleanCharType());
                query.SetResultTransformer(Transformers.AliasToBean<PartidaTremOnTime>());
                return query.List<PartidaTremOnTime>();
            }
        }

        /// <summary>
        /// Retorna as OS que tiveram a data de partida alterada
        /// </summary>
        /// <param name="inicial">Data inicial da �ltima altera��o</param>
        /// <param name="final">Data final da �ltima altera��o</param>
        /// <returns>Ordem de servico com os dados atualizados</returns>
        public IList<LogAlteracaoOsDto> ObterAlteracoesDataPartida(DateTime inicial, DateTime? final)
        {
            string sqlquery = @" Select 
                                    log.Id_Os as ""IdOs"", 
                                    log.us_usr_idu as ""UsUsrIdu"", 
                                    log.dt_anterior as ""DataAnteriorPartida"", 
                                    log.dt_nova as ""NovaDataPartida"", 
                                    log.oa_timestamp as ""DataCadastro"" 
                                  From
                                    Os_Alteradas_D_D1 log ";

            // Consulta parametrizada para datas
            if (final.HasValue)
            {
                sqlquery += " Where log.oa_TimeStamp Between :dataInicialEdicao And :dataFinalEdicao ";
            }
            else
            {
                sqlquery += " Where log.oa_TimeStamp > :dataInicialEdicao ";
            }

            using (IStatelessSession session = SessionManager.OpenStatelessSession())
            {
                ISQLQuery query = session.CreateSQLQuery(sqlquery);

                query.SetDateTime("dataInicialEdicao", inicial);

                if (final.HasValue)
                {
                    query.SetDateTime("dataFinalEdicao", final.Value);
                }

                query.SetResultTransformer(Transformers.AliasToBean<LogAlteracaoOsDto>());

                return query.List<LogAlteracaoOsDto>();
            }
        }

        /// <summary>
        /// Obt�m todos os IDs das OS que ainda n�o tiveram a documenta��o normal gerada
        /// </summary>
        /// <param name="terminalDestinoId">Terminal de destino que receber� o e-mail</param>
        /// <returns>Retorna lista de ids das OSs para gera��o de documenta��o</returns>
        public IList<OsComposicaoDto> ObterOsPendenteGeracao(int terminalDestinoId)
        {
            var sql = string.Format(@"
                    SELECT TOS.X1_ID_OS      AS ""IdOs""
                         , MAX(CP.CP_ID_CPS) AS ""IdComposicao""
                         , TOS.X1_PFX_TRE    AS ""PrefixoTrem""
                         , TOS.X1_NRO_OS     AS ""NumeroOs""
                      FROM COMPVAGAO_VIG     CVV
                      JOIN VAGAO_PEDIDO_VIG  VPV  ON VPV.VG_ID_VG  = CVV.VG_ID_VG AND VPV.PT_ID_ORT = CVV.PT_ID_ORT                      
                      JOIN COMPOSICAO        CP   ON CP.CP_ID_CPS  = CVV.CP_ID_CPS
                      JOIN TREM              TR   ON TR.TR_ID_TRM  = CP.TR_ID_TRM
                      JOIN T2_OS             TOS  ON TOS.X1_ID_OS  = TR.OF_ID_OSV
                      JOIN AREA_OPERACIONAL  AOT  ON AOT.AO_ID_AO  = TR.AO_ID_AO_DST
                      JOIN FLUXO_COMERCIAL   FC   ON FC.FX_ID_FLX  = VPV.FX_ID_FLX
                      JOIN MOVIMENTACAO_TREM MT   ON MT.TR_ID_TRM  = TR.TR_ID_TRM
                      JOIN AREA_OPERACIONAL  AOMT ON AOMT.AO_ID_AO = MT.AO_ID_AO
                     WHERE TR.TR_STT_TRM != 'E'                      /* Trem n�o esta encerrado */
                       AND MT.MT_DTS_RAL   IS NOT NULL               /* Movimenta��o realizada */
                       AND FC.EP_ID_EMP_DST  = :terminalDestino
                       AND NOT EXISTS(                               /* E n�o foi gerado a documentacao na tabela de gerenciamento de documentacao */
                                    SELECT 1
                                      FROM GERENCIAMENTO_DOC GER
                                     WHERE GER.ID_OS = TOS.X1_ID_OS
                                       AND GER.ID_RECEBEDOR = FC.EP_ID_EMP_DST
                                       AND GER.ID_GERENCIAMENTO_DOC_REFAT IS NULL /* E n�o � documenta��o de refaturamento */
                                       AND ROWNUM = 1
                       )
                       /* Faturamento 2.0 */
                       /* Valida�ao para n�o rela��o de trem com vag�o faturado com fluxo operacional */
                       AND NOT EXISTS (
                                   SELECT 1
                                     FROM COMPOSICAO CP1,
                                          COMPVAGAO_VIG CVV1, 
                                          VAGAO_PEDIDO_VIG VPV1,
                                          FLUXO_COMERCIAL FX1
                                    WHERE CP1.TR_ID_TRM = TR.TR_ID_TRM
                                      AND CP1.CP_ID_CPS = CVV1.CP_ID_CPS
                                      AND CVV1.VG_ID_VG = VPV1.VG_ID_VG
                                      AND VPV1.FX_ID_FLX = FX1.FX_ID_FLX
                                      AND FX1.FX_IND_OPER = 'S'
                       )
                     GROUP BY TOS.X1_ID_OS
                            , TOS.X1_PFX_TRE
                            , TOS.X1_NRO_OS
                ");

            var session = this.SessionManager.OpenStatelessSession();
            var query = session.CreateSQLQuery(sql);

            query.SetDecimal("terminalDestino", terminalDestinoId);

            query.SetResultTransformer(Transformers.AliasToBean<OsComposicaoDto>());
            return query.List<OsComposicaoDto>();
        }

        /// <summary>
        /// Obt�m todos os IDs das OS que tiveram a documenta��o gerada, por�m houve composi��o diferente da gerada
        /// </summary>
        /// <returns>Retorna lista de ids das OSs para atualiza��o dos documentos</returns>
        public IList<OsComposicaoDto> ObterOsPendenteAtualizacao()
        {
            var sql = string.Format(@"
                SELECT AGRUPADOR.IdOs         AS ""IdOs""
                     , AGRUPADOR.IdRecebedor  AS ""IdRecebedor""
                     , AGRUPADOR.IdComposicao AS ""IdComposicao""
                  FROM (
                            SELECT TOS.X1_ID_OS      AS IdOs
                                 , GER.ID_RECEBEDOR  AS IdRecebedor
                                 , MAX(CP.CP_ID_CPS) AS IdComposicao
                              FROM GERENCIAMENTO_DOC GER
                              JOIN T2_OS             TOS  ON TOS.X1_ID_OS = GER.ID_OS
                              JOIN TREM              TR   ON TR.OF_ID_OSV = TOS.X1_ID_OS
                              JOIN COMPOSICAO        CP   ON CP.TR_ID_TRM = TR.TR_ID_TRM
                             WHERE GER.EXCLUIDO_EM IS     NULL
                               AND GER.CRIADO_EM   IS NOT NULL
                               AND GER.ID_GERENCIAMENTO_DOC_REFAT IS NULL
                             GROUP BY TOS.X1_ID_OS
                                    , GER.ID_RECEBEDOR
                  ) AGRUPADOR
                 WHERE NOT EXISTS(
                            SELECT 1
                              FROM GERENCIAMENTO_DOC GER
                             WHERE GER.ID_OS         = AGRUPADOR.IdOs
                               AND GER.ID_COMPOSICAO = AGRUPADOR.IdComposicao
                               AND GER.ID_RECEBEDOR  = AGRUPADOR.IdRecebedor
                               AND GER.ID_GERENCIAMENTO_DOC_REFAT IS NULL
                               AND ROWNUM = 1
                     )
            ");

            var session = this.SessionManager.OpenStatelessSession();
            var query = session.CreateSQLQuery(sql);

            query.SetResultTransformer(Transformers.AliasToBean<OsComposicaoDto>());
            return query.List<OsComposicaoDto>();
        }

        /// <summary>
        /// Obt�m todos os ID dos documentos pendentes a serem enviados pelo job de envio de documenta��o de refaturamento
        /// </summary>
        /// <param name="recebedorId">Terminal de destino que receber� o e-mail</param>
        /// <returns>Retorna lista de ids dos documentos para envio</returns>
        public IList<OsComposicaoDto> ObterOsComposicoesNaoEnviadasRefaturamento(int recebedorId)
        {
            var sql = string.Format(@"
                        SELECT DISTINCT 
                               GER.ID_GERENCIAMENTO_DOC AS ""IdDocumentacao""
                             , GER.ID_OS                AS ""IdOs""
                             , GER.ID_COMPOSICAO        AS ""IdComposicao""
                             , GER.ID_RECEBEDOR         AS ""IdRecebedor""
                             , 'N'                      AS ""Recomposicao""
                             , 'S'                      AS ""Refaturamento""
                             , 'N'                      AS ""Chegada""
                             , NULL                     AS ""DataChegada""
                             , 'N'                      AS ""Saida""
                          FROM GERENCIAMENTO_DOC_REFAT  GDR
                          JOIN GERENCIAMENTO_DOC        GER  ON GER.ID_GERENCIAMENTO_DOC_REFAT = GDR.ID_GERENCIAMENTO_DOC_REFAT
                          JOIN TREM                     TR   ON TR.OF_ID_OSV  = GER.ID_OS

                         WHERE GDR.PROCESSADO = 'S'         /* Documento de refaturamento processado */
                           AND GER.EXCLUIDO_EM IS     NULL  /* Documento n�o excluido  */
                           AND GER.CRIADO_EM   IS NOT NULL  /* Documento criado        */
                           AND GER.ID_RECEBEDOR  = :recebedorId /* Terminal recebedor foi configurado */
                           AND NOT EXISTS(
                                    SELECT 1
                                     FROM HIST_ENVIO_DOC    HED_TMP
                                     JOIN GERENCIAMENTO_DOC GER_TMP ON GER_TMP.ID_GERENCIAMENTO_DOC = HED_TMP.ID_GERENCIAMENTO_DOC
                                    WHERE GER_TMP.ID_GERENCIAMENTO_DOC = GER.ID_GERENCIAMENTO_DOC
                                      AND ROWNUM = 1
                               )
            ");

            var session = this.SessionManager.OpenStatelessSession();
            var query = session.CreateSQLQuery(sql);

            query.SetDecimal("recebedorId", recebedorId);

            query.SetResultTransformer(Transformers.AliasToBean<OsComposicaoDto>());
            return query.List<OsComposicaoDto>();
        }

        /// <summary>
        /// Obt�m todos os ID das composi��es e trens pendentes a serem enviados pelo job de envio de documenta��o
        /// </summary>
        /// <param name="areaOperacionalEnvioId">�rea operacional m�e onde o trem deve ter passado para enviar a documenta��o</param>
        /// <param name="terminalDestinoId">Terminal de destino que receber� o e-mail</param>
        /// <returns>Retorna lista de ids das composi��es para envio</returns>
        public IList<OsComposicaoDto> ObterOsComposicoesNaoEnviadas(int areaOperacionalEnvioId, int terminalDestinoId)
        {
            var sql = string.Format(@"
                        SELECT DISTINCT
                               GER.ID_GERENCIAMENTO_DOC AS ""IdDocumentacao""
                             , GER.ID_OS                AS ""IdOs""
                             , GER.ID_COMPOSICAO        AS ""IdComposicao""
                             , GER.ID_RECEBEDOR         AS ""IdRecebedor""
                             , CASE WHEN HED.ID_HIST_ENVIO_DOC IS NULL
                                    THEN 'N'
                                    ELSE 'S'
                                    END                 AS ""Recomposicao""
                             , 'N'                      AS ""Refaturamento""
                             , 'N'                      AS ""Chegada""
                             , NULL                     AS ""DataChegada""
                             , 'S'                      AS ""Saida""

                          FROM GERENCIAMENTO_DOC    GER
                    INNER JOIN TREM                 TR   ON TR.OF_ID_OSV  = GER.ID_OS
                    INNER JOIN MOVIMENTACAO_TREM    MT   ON MT.TR_ID_TRM  = TR.TR_ID_TRM
                    INNER JOIN AREA_OPERACIONAL     AOMT ON AOMT.AO_ID_AO = MT.AO_ID_AO
                    INNER JOIN UNID_PRODUCAO        UP   ON UP.UP_ID_UNP  = AOMT.UP_ID_UNP

                    /* Obtendo o �ltimo envio de e-mail do documento */
                    /* Caso o JOIN falhar, indica que nunca foi enviado e-mail algum para o terminal contendo aquela documenta��o */
                    /* Caso retornar resultado, indica que pode ser uma recomposi��o. Deve-se verificar se a composi��o do envio do e-mail � menor que a do documento */
                    /* Se as composi��es forem diferentes, indica que houve recomposi��o e deve-se enviar o e-mail novamente informando o terminal que houve recomposi��o */
                     LEFT JOIN (
                               SELECT HED_TMP.ID_GERENCIAMENTO_DOC   AS ID_GERENCIAMENTO_DOC
                                    , MAX(HED_TMP.ID_HIST_ENVIO_DOC) AS ID_HIST_ENVIO_DOC
                                 FROM HIST_ENVIO_DOC HED_TMP
                                WHERE HED_TMP.ID_GERENCIAMENTO_DOC IS NOT NULL
                                  AND NVL(HED_TMP.ENVIO_SAIDA, 'S') = 'S'
                                GROUP BY HED_TMP.ID_GERENCIAMENTO_DOC
                          ) HED_MAX ON HED_MAX.ID_GERENCIAMENTO_DOC = GER.ID_GERENCIAMENTO_DOC
                     LEFT JOIN HIST_ENVIO_DOC HED ON HED.ID_HIST_ENVIO_DOC = HED_MAX.ID_HIST_ENVIO_DOC

                         WHERE GER.EXCLUIDO_EM IS     NULL  /* Documento n�o excluido  */
                           AND GER.CRIADO_EM   IS NOT NULL  /* Documento criado        */
                           AND TR.TR_STT_TRM != 'E'         /* Trem n�o est� encerrado */
                           AND GER.ID_GERENCIAMENTO_DOC > 1823 /* Este � o ID que houve o relacionamento forte entre HIST_ENVIO_DOC e GERENCIAMENTO_DOC. Isso � necess�rio pois retiramos a clausula de trens encerrados. */
                           AND GER.ID_RECEBEDOR  = :terminalDestino      /* Terminal recebedor foi configurado            */
                           AND AOMT.AO_ID_AO_INF = :areaOperacionalEnvio /* Trem passou pela �rea operacional configurada */
                           AND MT.MT_DTS_RAL >= SYSDATE-15
                            AND MT.MT_DTS_RAL   IS NOT NULL               /* Foi registrado a saida do trem no Translogic  */
                           
                           /* O e-mail ainda n�o foi enviado anteriormente ou houve recomposicao n�o enviada */
                           AND (HED.ID_HIST_ENVIO_DOC IS NULL OR HED.ID_COMPOSICAO < GER.ID_COMPOSICAO)

                           /* N�o � documenta��o de refaturamento */
                           AND GER.ID_GERENCIAMENTO_DOC_REFAT IS NULL

                           /* Ignorando os trens que s�o da MRS (M) e da VLI (U) da malha Norte */
                           AND NOT((TR.TR_PFX_TRM LIKE 'M%' OR TR.TR_PFX_TRM LIKE 'U%')AND (UP.UP_IND_MALHA IN('LG','MN')))
                    UNION ALL                    
                    SELECT DISTINCT
                               GER.ID_GERENCIAMENTO_DOC AS ""IdDocumentacao""
                             , GER.ID_OS                AS ""IdOs""
                             , GER.ID_COMPOSICAO        AS ""IdComposicao""
                             , GER.ID_RECEBEDOR         AS ""IdRecebedor""
                             , CASE WHEN HED.ID_HIST_ENVIO_DOC IS NULL
                                    THEN 'N'
                                    ELSE 'S'
                                    END                 AS ""Recomposicao""
                             , 'N'                      AS ""Refaturamento""
                             , 'N'                      AS ""Chegada""
                             , NULL                     AS ""DataChegada""
                             , 'S'                      AS ""Saida""

                          FROM GERENCIAMENTO_DOC    GER
                    INNER JOIN TREM                 TR   ON TR.OF_ID_OSV  = GER.ID_OS
                    INNER JOIN MOVIMENTACAO_TREM    MT   ON MT.TR_ID_TRM  = TR.TR_ID_TRM
                    INNER JOIN AREA_OPERACIONAL     AOMT ON AOMT.AO_ID_AO = MT.AO_ID_AO
                    INNER JOIN UNID_PRODUCAO        UP   ON UP.UP_ID_UNP  = AOMT.UP_ID_UNP

                    /* Obtendo o �ltimo envio de e-mail do documento */
                    /* Caso o JOIN falhar, indica que nunca foi enviado e-mail algum para o terminal contendo aquela documenta��o */
                    /* Caso retornar resultado, indica que pode ser uma recomposi��o. Deve-se verificar se a composi��o do envio do e-mail � menor que a do documento */
                    /* Se as composi��es forem diferentes, indica que houve recomposi��o e deve-se enviar o e-mail novamente informando o terminal que houve recomposi��o */
                     LEFT JOIN (
                               SELECT HED_TMP.ID_GERENCIAMENTO_DOC   AS ID_GERENCIAMENTO_DOC
                                    , MAX(HED_TMP.ID_HIST_ENVIO_DOC) AS ID_HIST_ENVIO_DOC
                                 FROM HIST_ENVIO_DOC HED_TMP
                                WHERE HED_TMP.ID_GERENCIAMENTO_DOC IS NOT NULL
                                  AND NVL(HED_TMP.ENVIO_SAIDA, 'S') = 'S'
                                GROUP BY HED_TMP.ID_GERENCIAMENTO_DOC
                          ) HED_MAX ON HED_MAX.ID_GERENCIAMENTO_DOC = GER.ID_GERENCIAMENTO_DOC
                     LEFT JOIN HIST_ENVIO_DOC HED ON HED.ID_HIST_ENVIO_DOC = HED_MAX.ID_HIST_ENVIO_DOC

                         WHERE GER.EXCLUIDO_EM IS     NULL  /* Documento n�o excluido  */
                           AND GER.CRIADO_EM   IS NOT NULL  /* Documento criado        */
                           AND TR.TR_STT_TRM = 'E'         /* Trem n�o est� encerrado */
                           AND GER.ID_GERENCIAMENTO_DOC > 1823 /* Este � o ID que houve o relacionamento forte entre HIST_ENVIO_DOC e GERENCIAMENTO_DOC. Isso � necess�rio pois retiramos a clausula de trens encerrados. */
                           AND TR.Tr_Timestamp > sysdate - 30 /* data de corte para n�o reenviar os trens antigos ja encerrados */
                           AND GER.ID_RECEBEDOR  = :terminalDestino      /* Terminal recebedor foi configurado            */
                           AND AOMT.AO_ID_AO_INF = :areaOperacionalEnvio /* Trem passou pela �rea operacional configurada */
                           AND MT.MT_DTS_RAL >= SYSDATE-15
                           AND MT.MT_DTS_RAL   IS NOT NULL               /* Foi registrado a saida do trem no Translogic  */
                                                      
                           /* O e-mail ainda n�o foi enviado anteriormente ou houve recomposicao n�o enviada */
                           AND (HED.ID_HIST_ENVIO_DOC IS NULL OR HED.ID_COMPOSICAO < GER.ID_COMPOSICAO)

                           /* N�o � documenta��o de refaturamento */
                           AND GER.ID_GERENCIAMENTO_DOC_REFAT IS NULL

                           /* Ignorando os trens que s�o da MRS (M) e da VLI (U) das Malha Norte */
                           AND NOT((TR.TR_PFX_TRM LIKE 'M%' OR TR.TR_PFX_TRM LIKE 'U%')AND (UP.UP_IND_MALHA IN('LG','MN')))
                ");

            var session = this.SessionManager.OpenStatelessSession();
            var query = session.CreateSQLQuery(sql);

            query.SetDecimal("terminalDestino", terminalDestinoId);
            query.SetDecimal("areaOperacionalEnvio", areaOperacionalEnvioId);

            query.SetResultTransformer(Transformers.AliasToBean<OsComposicaoDto>());
            return query.List<OsComposicaoDto>();
        }

        /// <summary>
	
        /// </summary>
        /// <param name="areaOperacionalEnvioId">�rea operacional m�e onde o trem deve ter CHEGADO para enviar a documenta��o</param>
        /// <param name="terminalDestinoId">Terminal de destino que receber� o e-mail</param>
        /// <returns>Retorna lista de ids das composi��es para envio</returns>
        public IList<OsComposicaoDto> ObterOsComposicoesNaoEnviadasChegada(int areaOperacionalEnvioId, int terminalDestinoId)
        {
            var sql = string.Format(@"
                        SELECT DISTINCT
                               GER.ID_GERENCIAMENTO_DOC AS ""IdDocumentacao""
                             , GER.ID_OS                AS ""IdOs""
                             , GER.ID_COMPOSICAO        AS ""IdComposicao""
                             , GER.ID_RECEBEDOR         AS ""IdRecebedor""
                             , 'N'                      AS ""Recomposicao""
                             , 'N'                      AS ""Refaturamento""
                             , 'S'                      AS ""Chegada""
                             , MT.MT_DTC_RAL            AS ""DataChegada""
                             , 'N'                      AS ""Saida""
                          FROM GERENCIAMENTO_DOC    GER
                    INNER JOIN TREM                 TR   ON TR.OF_ID_OSV  = GER.ID_OS
                    INNER JOIN MOVIMENTACAO_TREM    MT   ON MT.TR_ID_TRM  = TR.TR_ID_TRM
                    INNER JOIN AREA_OPERACIONAL     AOMT ON AOMT.AO_ID_AO = MT.AO_ID_AO
                    INNER JOIN CONF_ENVIO_DOC       CED ON CED.ID_EP_ID_EMP_DEST = GER.ID_RECEBEDOR AND CED.ID_AO_ID_AO_ENVIO = AOMT.AO_ID_AO_INF
                    INNER JOIN UNID_PRODUCAO        UP   ON UP.UP_ID_UNP  = AOMT.UP_ID_UNP                 

                         WHERE GER.EXCLUIDO_EM IS     NULL  /* Documento n�o excluido  */
                           AND GER.CRIADO_EM   IS NOT NULL  /* Documento criado        */
                           AND TR.TR_STT_TRM != 'E'         /* Trem n�o est� encerrado */
                           AND GER.ID_GERENCIAMENTO_DOC > 1823 /* Este � o ID que houve o relacionamento forte entre HIST_ENVIO_DOC e GERENCIAMENTO_DOC. Isso � necess�rio pois retiramos a clausula de trens encerrados. */
                           AND GER.ID_RECEBEDOR  = :terminalDestino      /* Terminal recebedor foi configurado              */
                           AND AOMT.AO_ID_AO_INF = :areaOperacionalEnvio /* Trem passou pela �rea operacional configurada   */
                           AND CED.ENVIO_CHEGADA = 'S'                   /* Est� configurado para enviar na chegada         */                           
                           AND MT.MT_DTC_RAL >= SYSDATE-15
                            AND MT.MT_DTC_RAL   IS NOT NULL               /* Foi registrado a chegada do trem no Translogic  */

                           /* N�o � documenta��o de refaturamento */
                           AND GER.ID_GERENCIAMENTO_DOC_REFAT IS NULL

                           
                           /* Ignorando os trens que s�o da MRS (M) e da VLI (U) da malha norte */
                           AND NOT((TR.TR_PFX_TRM LIKE 'M%' OR TR.TR_PFX_TRM LIKE 'U%')AND (UP.UP_IND_MALHA IN('LG','MN')))

                           /* N�o foi enviado e-mail de chegada anteriormente da documenta��o */
                           AND NOT EXISTS(
                                            SELECT 1
                                              FROM HIST_ENVIO_DOC HED_TMP
                                             WHERE HED_TMP.ID_GERENCIAMENTO_DOC = GER.ID_GERENCIAMENTO_DOC
                                               AND HED_TMP.ENVIO_CHEGADA = 'S'
                                               AND ROWNUM = 1
                                         )
                    UNION ALL
                    /* PEGA OS TRENS QUE A OPERA��O ENCERRA NA MAO NO TRANSLOGIC */
                    SELECT DISTINCT
                               GER.ID_GERENCIAMENTO_DOC AS ""IdDocumentacao""
                             , GER.ID_OS                AS ""IdOs""
                             , GER.ID_COMPOSICAO        AS ""IdComposicao""
                             , GER.ID_RECEBEDOR         AS ""IdRecebedor""
                             , 'N'                      AS ""Recomposicao""
                             , 'N'                      AS ""Refaturamento""
                             , 'S'                      AS ""Chegada""
                             , MT.MT_DTC_RAL            AS ""DataChegada""
                             , 'N'                      AS ""Saida""
                          FROM GERENCIAMENTO_DOC    GER
                    INNER JOIN TREM                 TR   ON TR.OF_ID_OSV  = GER.ID_OS
                    INNER JOIN MOVIMENTACAO_TREM    MT   ON MT.TR_ID_TRM  = TR.TR_ID_TRM
                    INNER JOIN AREA_OPERACIONAL     AOMT ON AOMT.AO_ID_AO = MT.AO_ID_AO
                    INNER JOIN CONF_ENVIO_DOC       CED ON CED.ID_EP_ID_EMP_DEST = GER.ID_RECEBEDOR AND CED.ID_AO_ID_AO_ENVIO = AOMT.AO_ID_AO_INF
                    INNER JOIN UNID_PRODUCAO        UP   ON UP.UP_ID_UNP  = AOMT.UP_ID_UNP

                         WHERE GER.EXCLUIDO_EM IS     NULL  /* Documento n�o excluido  */
                           AND GER.CRIADO_EM   IS NOT NULL  /* Documento criado        */
                           AND TR.TR_STT_TRM = 'E'         /* Trem est� encerrado */
                           AND GER.ID_GERENCIAMENTO_DOC > 1823 /* Este � o ID que houve o relacionamento forte entre HIST_ENVIO_DOC e GERENCIAMENTO_DOC. Isso � necess�rio pois retiramos a clausula de trens encerrados. */
                           AND TR.Tr_Timestamp > sysdate - 30 /* data de corte para n�o reenviar os trens antigos ja encerrados */
                           AND GER.ID_RECEBEDOR  = :terminalDestino      /* Terminal recebedor foi configurado              */
                           AND AOMT.AO_ID_AO_INF = :areaOperacionalEnvio /* Trem passou pela �rea operacional configurada   */
                           AND CED.ENVIO_CHEGADA = 'S'                   /* Est� configurado para enviar na chegada         */                           
                           AND MT.MT_DTC_RAL >= SYSDATE-15
                           AND MT.MT_DTC_RAL   IS NOT NULL               /* Foi registrado a chegada do trem no Translogic  */

                           /* N�o � documenta��o de refaturamento */
                           AND GER.ID_GERENCIAMENTO_DOC_REFAT IS NULL

                           
                           /* Ignorando os trens que s�o da MRS (M) e da VLI (U) da malha norte */
                           AND NOT((TR.TR_PFX_TRM LIKE 'M%' OR TR.TR_PFX_TRM LIKE 'U%')AND (UP.UP_IND_MALHA IN('LG','MN')))

                           /* N�o foi enviado e-mail de chegada anteriormente da documenta��o */
                           AND NOT EXISTS(
                                            SELECT 1
                                              FROM HIST_ENVIO_DOC HED_TMP
                                             WHERE HED_TMP.ID_GERENCIAMENTO_DOC = GER.ID_GERENCIAMENTO_DOC
                                               AND HED_TMP.ENVIO_CHEGADA = 'S'
                                               AND ROWNUM = 1
                                         )
                ");

            var session = this.SessionManager.OpenStatelessSession();
            var query = session.CreateSQLQuery(sql);

            query.SetDecimal("terminalDestino", terminalDestinoId);
            query.SetDecimal("areaOperacionalEnvio", areaOperacionalEnvioId);

            query.SetResultTransformer(Transformers.AliasToBean<OsComposicaoDto>());
            return query.List<OsComposicaoDto>();
        }
        
        /// <summary>
        /// Adicionar um novo <see cref="OracleParameter"/> ao <see cref="OracleCommand"/>
        /// </summary>
        /// <param name="command"><see cref="OracleCommand"/> onde o <see cref="OracleParameter"/> ser� adicionado</param>
        /// <param name="nome">Nome do par�metro</param>
        /// <param name="tipo">Tipo do par�metro <see cref="OracleType"/></param>
        /// <param name="valor">Valor de entrada</param>
        /// <param name="direcao">Dire��o <see cref="ParameterDirection"/></param>
        private void AddParameter(OracleCommand command, string nome, OracleType tipo, object valor, ParameterDirection direcao)
        {
            OracleParameter parameter = new OracleParameter();
            parameter.ParameterName = nome;
            parameter.OracleType = tipo;
            parameter.Direction = direcao;

            parameter.Value = DBNull.Value;
            if (valor != null)
            {
                parameter.Value = valor;
            }

            command.Parameters.Add(parameter);
        }
    }
}
