namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Trem.OrdemServico
{
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;
	using Model.Trem;
    using Model.Trem.OrdemServico.Repositories;
    using NHibernate.Criterion;

    /// <summary>
	/// Implementa��o do Reposit�rio de <see cref="PlanejamentoQuantidadeLocomotiva"/>
	/// </summary>
	public class PlanejamentoQuantidadeLocomotivaRepository : NHRepository<PlanejamentoQuantidadeLocomotiva, int>, IPlanejamentoQuantidadeLocomotivaRepository
	{
        /// <summary>
		/// Obter lista de <see cref="PlanejamentoQuantidadeLocomotiva"/> vigentes por id da ordem de servi�o
        /// </summary>
        /// <param name="idOrdemServico">Id da ordem de servi�o</param>
		/// <returns>Lista de <see cref="PlanejamentoQuantidadeLocomotiva"/> vigentes</returns>
        public IList<PlanejamentoQuantidadeLocomotiva> ObterPorOrdemServico(int idOrdemServico)
        {
			DetachedCriteria criteria = DetachedCriteria.For<PlanejamentoQuantidadeLocomotiva>().
                                        Add(Restrictions.Eq("OrdemServico.Id", idOrdemServico));

			return ObterTodos<PlanejamentoQuantidadeLocomotiva>(criteria);
        }
	}
}