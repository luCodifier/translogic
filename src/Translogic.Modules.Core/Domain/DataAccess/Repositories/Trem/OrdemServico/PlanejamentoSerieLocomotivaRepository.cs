namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Trem.OrdemServico
{
    using System.Collections.Generic;
    using Adaptations;
    using ALL.Core.AcessoDados;
	using Model.Trem;
	using Model.Trem.OrdemServico.Repositories;
    using NHibernate.Criterion;

    /// <summary>
    /// Implementa��o do Reposit�rio de <see cref="PlanejamentoSerieLocomotiva"/>
	/// </summary>
    public class PlanejamentoSerieLocomotivaRepository : NHRepository<PlanejamentoSerieLocomotiva, int>, IPlanejamentoSerieLocomotivaRepository
	{
        /// <summary>
        /// Obter lista de <see cref="PlanejamentoSerieLocomotiva"/> vigentes por id da ordem de servi�o
        /// </summary>
        /// <param name="idOrdemServico">Id da ordem de servi�o</param>
        /// <returns>Lista de <see cref="PlanejamentoSerieLocomotivaVigente"/> vigentes</returns>
        public IList<PlanejamentoSerieLocomotivaVigente> ObterVigentes(int idOrdemServico)
        {
            DetachedCriteria criteria = DetachedCriteria.For<PlanejamentoSerieLocomotivaVigente>().
                                        Add(Restrictions.Eq("OrdemServico.Id", idOrdemServico));

            return ObterTodos<PlanejamentoSerieLocomotivaVigente>(criteria);
        }

        /// <summary>
        /// Apagar todos os planejamentos por id da ordem de servi�o
        /// </summary>
        /// <param name="idOrdemServico">Id da ordem de servi�o</param>
        public void ApagarVigentes(int idOrdemServico)
        {
            DetachedCriteria criteria = DetachedCriteria.For<PlanejamentoSerieLocomotivaVigente>().
                Add(Restrictions.Eq("OrdemServico.Id", idOrdemServico));

            Remover<PlanejamentoSerieLocomotivaVigente>(criteria);
        }
	}
}