namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Trem
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using ALL.Core.AcessoDados;
    using ALL.Core.Dominio;
    using Model.Trem;
    using Model.Trem.Repositories;
    using NHibernate;
    using NHibernate.Criterion;
    using NHibernate.Transform;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Modules.Core.Domain.Model.PainelExpedicao.Documentos.Dto;

    /// <summary>
    /// Repositorio para <see cref="Trem"/>
    /// </summary>
    public class ComposicaoRepository : NHRepository<Composicao, int?>, IComposicaoRepository
    {
        /// <summary>
        /// Obtem a ultima composicao do trem
        /// </summary>
        /// <param name="idTrem">Id do Trem</param>
        /// <returns>Ultima composicao</returns>
        public Composicao ObterUltimaPorIdTrem(int idTrem)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.Eq("Trem.Id", idTrem));
            criteria.AddOrder(Order.Desc("DataInicio"));

            return ObterPrimeiro(criteria);
        }

        /// <summary>
        /// Obtem os dados da composicao
        /// </summary>
        /// <param name="idComposicao">Identificador da composicao</param>
        /// <returns>Retorna os dados da composi��o</returns>
        public Composicao ObterDadosComposicao(int idComposicao)
        {
            List<Action<IQuery>> parametros = new List<Action<IQuery>>();
            StringBuilder sb = new StringBuilder();

            // LEFT JOIN cp.ListaLocomotivas cl
            // LEFT JOIN cp.ListaVagoes cv
            sb.Append(@" SELECT cp
				FROM Composicao cp
				INNER JOIN FETCH cp.ListaVagoes cv
				INNER JOIN FETCH cp.ListaLocomotivas cl
				WHERE cp.Id = :idComposicao
			");

            parametros.Add(q => q.SetInt32("idComposicao", idComposicao));

            ISession session = OpenSession();
            string sqlQuery = sb.ToString();
            IQuery query = session.CreateQuery(sqlQuery);
            // query.SetResultTransformer(Transformers.DistinctRootEntity);
            foreach (Action<IQuery> action in parametros)
            {
                action.Invoke(query);
            }

            IList<Composicao> lista = query.List<Composicao>();

            return lista.FirstOrDefault();
        }

        /// <summary>
        /// Obtem composicao por trem
        /// </summary>
        /// <param name="detalhesPaginacao">detalhes da paginacao</param>
        /// <param name="idTrem">Identificador do trem</param>
        /// <returns>Retorna os dados da composi��o</returns>
        public ResultadoPaginado<Composicao> ObterComposicaoPorTrem(DetalhesPaginacao detalhesPaginacao, int idTrem)
        {
            List<Action<IQuery>> parametros = new List<Action<IQuery>>();
            StringBuilder hql = new StringBuilder();

            hql.Append(@"   SELECT 
                                {0}
				             FROM Composicao cp
				            INNER JOIN cp.Trem tr
				            INNER JOIN cp.Origem origem
				            LEFT JOIN cp.Destino destino
				            WHERE tr.Id = :idTrem
			");

            using (var session = OpenSession())
            {
                parametros.Add(q => q.SetParameter("idTrem", idTrem));

                var query = session.CreateQuery(string.Format(hql.ToString(), @" cp "));

                var queryCount = session.CreateQuery(string.Format(hql.ToString(), "count(cp.Id)"));

                foreach (var p in parametros)
                {
                    p.Invoke(query);
                    p.Invoke(queryCount);
                }

                query.SetResultTransformer(Transformers.DistinctRootEntity);
                query.SetFirstResult(detalhesPaginacao.Inicio ?? 0);
                query.SetMaxResults(detalhesPaginacao.Limite ?? DetalhesPaginacao.LimitePadrao);

                var total = queryCount.UniqueResult<long>();
                var itens = query.List<Composicao>();

                var result = new ResultadoPaginado<Composicao>
                {
                    Items = itens,
                    Total = (long)total
                };

                return result;
            }
        }

        /// <summary>
        /// Obtem vag�es por filtro
        /// </summary>
        /// <param name="dataInicio">Data de In�cio do Per�odo</param>
        /// <param name="dataTermino">Data de T�rmino do Per�odo</param>
        /// <param name="vagoes">Lista de Vag�es</param>
        /// <param name="estacoes">Esta��o para filtro</param>
        /// <param name="idLinha">Id da Linha corrente dos vag�es</param>
        /// <returns>Retorna os dados da composi��o</returns>
        public IList<VagaoComposicaoDto> ObterVagoesPorFiltro(DateTime dataInicio, DateTime dataTermino, string[] vagoes, string[] estacoes, int? idLinha)
        {
            List<Action<IQuery>> parametros = new List<Action<IQuery>>();
            StringBuilder hql = new StringBuilder();

            hql.Append(@"
                    SELECT 
                        V.VG_ID_VG as ""Id"",
                        ITD.ID_ID_ITD as ""IdItemDespacho"",
                        0 as ""Sequencia"", 
                        SV.SV_COD_SER as ""SerieVagao"", 
                        V.VG_COD_VAG as ""CodigoVagao"",
                        ORIGEM.AO_COD_AOP as ""Origem"",
                        DESTINO.AO_COD_AOP as ""Destino"",
                        MC.MC_DDT_PT as ""Mercadoria"",
                        itd.id_num_pcl as ""TU"",  
                        COR.EP_DSC_RSM AS ""Correntista"",
                        NVL(v.vg_num_tra,fe.fc_tar) as ""Tara"",
                        DEST.EP_DSC_RSM AS ""Destinatario"",
                        CG.CG_DTR_CRG AS ""DataCarregamento"",
                        DS.DS_DAT_DSC AS ""DataDescarregamento"",
                        NF.NF_SER_NF AS ""SerieNf"",
                        NF.NF_NRO_NF AS ""NumeroNf"",
                        NF.NF_DAT_NF AS ""DataNf"",
                        NVL(NF.NF_PESO_NF,NF.NF_VOLUME) AS ""PesoNf"",      
                        ITD.ID_NUM_PCL AS ""PesoRateioNf"",      
                        NF.NF_VALOR_TOTAL AS ""ValorNf"",
                        NF.NFE_CHAVE_NFE AS ""ChaveNfe"",
                        EV.EV_COD_ELV AS ""Linha"",
					    CASE WHEN TB.ID IS NULL THEN 0 ELSE 1 END AS ""PossuiTicket""
                    FROM VAGAO_PATIO_VIG VPV 
                    JOIN VAGAO V ON  VPV.VG_ID_VG = V.VG_ID_VG    
               LEFT JOIN VAGAO_TICKET VT ON V.VG_COD_VAG = LPAD(VT.VG_COD_VAG, 7, '0') 
               LEFT JOIN TICKET_BALANCA TB ON TB.ID = VT.TICKET_BALANCA_ID
               LEFT JOIN NOTA_TICKET_VAGAO NTV ON NTV.VAGAO_TICKET_ID = VT.ID
                    JOIN SERIE_VAGAO SV ON SV.SV_ID_SV = V.SV_ID_SV
                    JOIN NOTA_FISCAL NF ON NF.NFE_CHAVE_NFE = NTV.CHAVE_NFE AND NF.VG_ID_VG = V.VG_ID_VG     
                    JOIN ITEM_DESPACHO ITD ON ITD.DP_ID_DP = NF.DP_ID_DP
                    JOIN DETALHE_CARREGAMENTO DC ON DC.DC_ID_CRG = ITD.DC_ID_CRG
                    JOIN CARREGAMENTO CG ON CG.CG_ID_CAR = DC.CG_ID_CAR
                    JOIN ELEMENTO_VIA EV ON EV.EV_ID_ELV = VPV.EV_ID_ELV
                    JOIN FOLHA_ESPECIF_VAGAO FE ON FE.FC_ID_FC = V.FC_ID_FC
                    JOIN FLUXO_COMERCIAL FC ON FC.FX_ID_FLX = CG.FX_ID_FLX_CRG
                    JOIN EMPRESA COR ON COR.EP_ID_EMP = FC.EP_ID_EMP_COR
                    JOIN EMPRESA DEST ON DEST.EP_ID_EMP = FC.EP_ID_EMP_DST
                    JOIN AREA_OPERACIONAL ORIGEM ON ORIGEM.AO_ID_AO = FC.AO_ID_EST_OR
                    JOIN AREA_OPERACIONAL DESTINO ON DESTINO.AO_ID_AO = FC.AO_ID_EST_DS
                    JOIN MERCADORIA MC ON FC.MC_ID_MRC = MC.MC_ID_MRC
               LEFT JOIN DETALHE_DESCARREGAMENTO DD ON DD.DC_ID_CRG = DC.DC_ID_CRG
               LEFT JOIN DESCARREGAMENTO DS ON DS.DS_ID_DSC = DD.DS_ID_DSC
                JOIN AREA_OPERACIONAL AOV ON EV.AO_ID_AO = AOV.AO_ID_AO  
                JOIN AREA_OPERACIONAL AOVM ON AOV.AO_ID_AO_INF = AOVM.AO_ID_AO    
                WHERE vpv.Vp_Dat_Chg BETWEEN :dataInicial AND :dataFinal
			");

            if (vagoes != null && vagoes.Length > 0)
            {
                hql.Append(@" AND V.VG_COD_VAG IN (" + String.Join(",", vagoes.Select(a => "'" + a + "'")) + ")");
            }

            if (estacoes != null && estacoes.Length > 0)
            {
                hql.Append(@" AND NVL(AOVM.AO_COD_AOP, AOV.AO_COD_AOP) IN (" + String.Join(",", estacoes.Select(a => "'" + a + "'")) + ")");
            }

            if (idLinha.HasValue)
            {
                hql.Append(@" AND EV.EV_ID_ELV = " + idLinha.Value);
            }

            using (var session = OpenSession())
            {
                parametros.Add(q => q.SetParameter("dataInicial", dataInicio));
                parametros.Add(q => q.SetParameter("dataFinal", dataTermino));

                var query = session.CreateSQLQuery(string.Format(hql.ToString()));

                foreach (var p in parametros)
                {
                    p.Invoke(query);
                }

                query.SetResultTransformer(Transformers.AliasToBean<DadosVagaoComposicaoDto>());

                var retorno = query.List<DadosVagaoComposicaoDto>();

                var vagaoComposicao =
                   from x in
                       retorno.Select(
                           k =>
                           new
                           {
                               k.Id,
                               k.IdItemDespacho,
                               k.SerieVagao,
                               k.CodigoVagao,
                               k.Sequencia,
                               k.Origem,
                               k.Destino,
                               k.Mercadoria,
                               k.TU,
                               k.Correntista,
                               k.Tara,
                               k.Destinatario,
                               k.DataCarregamento,
                               k.DataDescarregamento,
                               k.Linha,
                               k.PossuiTicket,
                           }).Distinct()
                   select new VagaoComposicaoDto()
                   {
                       Id = x.Id,
                       IdItemDespacho = x.IdItemDespacho,
                       SerieVagao = x.SerieVagao,
                       CodigoVagao = x.CodigoVagao,
                       Sequencia = x.Sequencia,
                       Origem = x.Origem,
                       Destino = x.Destino,
                       Mercadoria = x.Mercadoria,
                       TU = x.TU,
                       Correntista = x.Correntista,
                       Tara = x.Tara,
                       Destinatario = x.Destinatario,
                       DataCarregamento = x.DataCarregamento,
                       DataDescarregamento = x.DataDescarregamento,
                       Linha = x.Linha,
                       PossuiTicket = x.PossuiTicket > 0,
                       Notas = retorno.Where(n => n.Id == x.Id)
                                  .Select(v => new NotaVagaoDto()
                                  {
                                      SerieNf = v.SerieNf,
                                      NumeroNf = v.NumeroNf,
                                      DataNf = v.DataNf,
                                      PesoNf = v.PesoNf,
                                      PesoRateioNf = v.PesoRateioNf,
                                      ValorNf = v.ValorNf,
                                      ChaveNfe = v.ChaveNfe
                                  }).ToList()
                   };

                return vagaoComposicao.OrderBy(x => x.Sequencia).ToList();
            }
        }

        /// <summary>
        /// Obtem composicao por trem
        /// </summary>
        /// <param name="idComposicao">Identificador do trem</param>
        /// <returns>Retorna os dados da composi��o</returns>
        public IList<VagaoComposicaoDto> ObterVagoesComposicao(int idComposicao)
        {
            List<Action<IQuery>> parametros = new List<Action<IQuery>>();
            StringBuilder hql = new StringBuilder();

            hql.Append(@"SELECT 
                              V.VG_ID_VG as ""Id"",
                              CV.CV_SEQ_CMP as ""Sequencia"", 
                              SV.SV_COD_SER as ""SerieVagao"", 
                              V.VG_COD_VAG as ""CodigoVagao"",
                              ORIGEM.AO_COD_AOP as ""Origem"",
                              DESTINO.AO_COD_AOP as ""Destino"",
                              MC.MC_DDT_PT as ""Mercadoria"",
                              it.id_num_pcl as ""TU"",  
                              COR.EP_DSC_RSM AS ""Correntista"",
                              NVL(v.vg_num_tra,fc.fc_tar) as ""Tara"",
                              DEST.EP_DSC_RSM AS ""Destinatario"",
                              CG.CG_DTR_CRG AS ""DataCarregamento"",
                              DS.DS_DAT_DSC AS ""DataDescarregamento"",
                              NF.NF_SER_NF AS ""SerieNf"",
                              NF.NF_NRO_NF AS ""NumeroNf"",
                              NF.NF_DAT_NF AS ""DataNf"",
                              NVL(NF.NF_PESO_NF,NF.NF_VOLUME) AS ""PesoNf"",      
                              D.DP_NUM_PCL AS ""PesoRateioNf"",      
                              NF.NF_VALOR_TOTAL AS ""ValorNf"",
                              NF.NFE_CHAVE_NFE AS ""ChaveNfe""      
                          FROM COMPVAGAO CV
                          JOIN VAGAO V ON V.VG_ID_VG = CV.VG_ID_VG
                          JOIN FOLHA_ESPECIF_VAGAO FC ON FC.FC_ID_FC = V.FC_ID_FC
                          JOIN T2_PEDIDO PT ON PT.W1_ID_PED = CV.PT_ID_ORT
                          JOIN VAGAO_PEDIDO VP ON VP.VG_ID_VG = V.VG_ID_VG AND vp.pt_id_ort = CV.PT_ID_ORT
                          JOIN DESPACHO D ON D.PX_IDT_PDR = VP.PX_IDT_PDR
                          JOIN ITEM_DESPACHO IT ON IT.Dp_Id_Dp = d.dp_id_dp and it.vg_id_vg = cv.vg_id_vg
                          join PEDIDO_DERIVADO PD ON PD.PX_IDT_PDR = VP.PX_IDT_PDR
                          JOIN FLUXO_COMERCIAL FC ON FC.FX_ID_FLX = PD.FX_ID_FLX
                          JOIN MERCADORIA MC ON FC.MC_ID_MRC = MC.MC_ID_MRC
                          JOIN SERIE_VAGAO SV ON SV.SV_ID_SV = V.SV_ID_SV
                          JOIN AREA_OPERACIONAL ORIGEM ON ORIGEM.AO_ID_AO = FC.AO_ID_EST_OR
                          JOIN AREA_OPERACIONAL DESTINO ON DESTINO.AO_ID_AO = FC.AO_ID_EST_DS  
                          JOIN EMPRESA COR  ON COR.EP_ID_EMP = FC.EP_ID_EMP_COR
                          JOIN EMPRESA DEST ON DEST.EP_ID_EMP = FC.EP_ID_EMP_DST
                          JOIN DETALHE_CARREGAMENTO DC ON DC.DC_ID_CRG = IT.DC_ID_CRG
                          JOIN CARREGAMENTO CG ON CG.CG_ID_CAR = DC.CG_ID_CAR 
                          JOIN NOTA_FISCAL NF ON NF.DP_ID_DP = D.DP_ID_DP
                          LEFT JOIN DETALHE_DESCARREGAMENTO DD ON DD.DC_ID_CRG = DC.DC_ID_CRG
                          LEFT JOIN DESCARREGAMENTO DS ON DS.DS_ID_DSC = DD.DS_ID_DSC  
                         WHERE CV.CP_ID_CPS = :idComposicao
			");

            using (var session = OpenSession())
            {
                parametros.Add(q => q.SetParameter("idComposicao", idComposicao));

                var query = session.CreateSQLQuery(string.Format(hql.ToString()));

                foreach (var p in parametros)
                {
                    p.Invoke(query);
                }

                query.SetResultTransformer(Transformers.AliasToBean<DadosVagaoComposicaoDto>());

                var retorno = query.List<DadosVagaoComposicaoDto>();

                var vagaoComposicao =
                   from x in
                       retorno.Select(
                           k =>
                           new
                           {
                               k.Id,
                               k.SerieVagao,
                               k.CodigoVagao,
                               k.Sequencia,
                               k.Origem,
                               k.Destino,
                               k.Mercadoria,
                               k.TU,
                               k.Correntista,
                               k.Tara,
                               k.Destinatario,
                               k.DataCarregamento,
                               k.DataDescarregamento
                           }).Distinct()
                   select new VagaoComposicaoDto()
                   {
                       Id = x.Id,
                       SerieVagao = x.SerieVagao,
                       CodigoVagao = x.CodigoVagao,
                       Sequencia = x.Sequencia,
                       Origem = x.Origem,
                       Destino = x.Destino,
                       Mercadoria = x.Mercadoria,
                       TU = x.TU,
                       Correntista = x.Correntista,
                       Tara = x.Tara,
                       Destinatario = x.Destinatario,
                       DataCarregamento = x.DataCarregamento,
                       DataDescarregamento = x.DataDescarregamento,
                       Notas = retorno.Where(n => n.Id == x.Id)
                                  .Select(v => new NotaVagaoDto()
                                  {
                                      SerieNf = v.SerieNf,
                                      NumeroNf = v.NumeroNf,
                                      DataNf = v.DataNf,
                                      PesoNf = v.PesoNf,
                                      PesoRateioNf = v.PesoRateioNf,
                                      ValorNf = v.ValorNf,
                                      ChaveNfe = v.ChaveNfe
                                  }).ToList()
                   };

                return vagaoComposicao.OrderBy(x => x.Sequencia).ToList();
            }
        }

        /// <summary>
        /// Obtem as notas da composicao por trem
        /// </summary>
        /// <param name="idComposicao">Identificador da composicao</param>
        /// <param name="vagoes">Identificador dos vagoes</param>
        /// <returns>Retorna as chaves das nfe</returns>
        public IList<string> ObterNfeVagoesComposicao(int idComposicao, IList<int> vagoes)
        {
            List<Action<IQuery>> parametros = new List<Action<IQuery>>();
            StringBuilder hql = new StringBuilder();

            hql.Append(@"SELECT 
                              NF.NFE_CHAVE_NFE   
                          FROM COMPVAGAO CV
                          JOIN VAGAO V ON V.VG_ID_VG = CV.VG_ID_VG
                          JOIN FOLHA_ESPECIF_VAGAO FC ON FC.FC_ID_FC = V.FC_ID_FC
                          JOIN T2_PEDIDO PT ON PT.W1_ID_PED = CV.PT_ID_ORT
                          JOIN VAGAO_PEDIDO VP ON VP.VG_ID_VG = V.VG_ID_VG AND vp.pt_id_ort = CV.PT_ID_ORT
                          JOIN DESPACHO D ON D.PX_IDT_PDR = VP.PX_IDT_PDR
                          JOIN ITEM_DESPACHO IT ON IT.Dp_Id_Dp = d.dp_id_dp
                          join PEDIDO_DERIVADO PD ON PD.PX_IDT_PDR = VP.PX_IDT_PDR
                          JOIN FLUXO_COMERCIAL FC ON FC.FX_ID_FLX = PD.FX_ID_FLX
                          JOIN MERCADORIA MC ON FC.MC_ID_MRC = MC.MC_ID_MRC
                          JOIN SERIE_VAGAO SV ON SV.SV_ID_SV = V.SV_ID_SV
                          JOIN AREA_OPERACIONAL ORIGEM ON ORIGEM.AO_ID_AO = FC.AO_ID_EST_OR
                          JOIN AREA_OPERACIONAL DESTINO ON DESTINO.AO_ID_AO = FC.AO_ID_EST_DS  
                          JOIN EMPRESA COR  ON COR.EP_ID_EMP = FC.EP_ID_EMP_COR
                          JOIN EMPRESA DEST ON DEST.EP_ID_EMP = FC.EP_ID_EMP_DST
                          JOIN DETALHE_CARREGAMENTO DC ON DC.DC_ID_CRG = IT.DC_ID_CRG
                          JOIN CARREGAMENTO CG ON CG.CG_ID_CAR = DC.CG_ID_CAR 
                          JOIN NOTA_FISCAL NF ON NF.DP_ID_DP = D.DP_ID_DP
                          LEFT JOIN DETALHE_DESCARREGAMENTO DD ON DD.DC_ID_CRG = DC.DC_ID_CRG
                          LEFT JOIN DESCARREGAMENTO DS ON DS.DS_ID_DSC = DD.DS_ID_DSC  
                         WHERE CV.CP_ID_CPS = :idComposicao AND CV.VG_ID_VG IN (:idVagoes)
                           AND  NF.NFE_CHAVE_NFE IS NOT NULL
			");

            using (var session = OpenSession())
            {
                parametros.Add(q => q.SetParameter("idComposicao", idComposicao));
                parametros.Add(q => q.SetParameterList("idVagoes", vagoes));

                var query = session.CreateSQLQuery(string.Format(hql.ToString()));

                foreach (var p in parametros)
                {
                    p.Invoke(query);
                }

                return query.List<string>();
            }
        }

        /// <summary>
        /// Obtem as notas dos vag�es em p�tios vigentes
        /// </summary>
        /// <param name="itensDespachos">Identificador dos itens de despacho</param>
        /// <returns>Retorna as chaves das nfe</returns>
        public IList<string> ObterNfeItensDespacho(IList<int> itensDespachos)
        {
            List<Action<IQuery>> parametros = new List<Action<IQuery>>();
            StringBuilder hql = new StringBuilder();

            hql.Append(@"SELECT 
                              NF.NFE_CHAVE_NFE   
                          FROM COMPVAGAO CV
                          JOIN VAGAO V ON V.VG_ID_VG = CV.VG_ID_VG
                          JOIN FOLHA_ESPECIF_VAGAO FC ON FC.FC_ID_FC = V.FC_ID_FC
                          JOIN T2_PEDIDO PT ON PT.W1_ID_PED = CV.PT_ID_ORT
                          JOIN VAGAO_PEDIDO VP ON VP.VG_ID_VG = V.VG_ID_VG AND vp.pt_id_ort = CV.PT_ID_ORT
                          JOIN DESPACHO D ON D.PX_IDT_PDR = VP.PX_IDT_PDR
                          JOIN ITEM_DESPACHO IT ON IT.Dp_Id_Dp = d.dp_id_dp
                          join PEDIDO_DERIVADO PD ON PD.PX_IDT_PDR = VP.PX_IDT_PDR
                          JOIN FLUXO_COMERCIAL FC ON FC.FX_ID_FLX = PD.FX_ID_FLX
                          JOIN MERCADORIA MC ON FC.MC_ID_MRC = MC.MC_ID_MRC
                          JOIN SERIE_VAGAO SV ON SV.SV_ID_SV = V.SV_ID_SV
                          JOIN AREA_OPERACIONAL ORIGEM ON ORIGEM.AO_ID_AO = FC.AO_ID_EST_OR
                          JOIN AREA_OPERACIONAL DESTINO ON DESTINO.AO_ID_AO = FC.AO_ID_EST_DS  
                          JOIN EMPRESA COR  ON COR.EP_ID_EMP = FC.EP_ID_EMP_COR
                          JOIN EMPRESA DEST ON DEST.EP_ID_EMP = FC.EP_ID_EMP_DST
                          JOIN DETALHE_CARREGAMENTO DC ON DC.DC_ID_CRG = IT.DC_ID_CRG
                          JOIN CARREGAMENTO CG ON CG.CG_ID_CAR = DC.CG_ID_CAR 
                          JOIN NOTA_FISCAL NF ON NF.DP_ID_DP = D.DP_ID_DP
                          LEFT JOIN DETALHE_DESCARREGAMENTO DD ON DD.DC_ID_CRG = DC.DC_ID_CRG
                          LEFT JOIN DESCARREGAMENTO DS ON DS.DS_ID_DSC = DD.DS_ID_DSC  
                         WHERE IT.ID_ID_ITD IN (:itensDespachos)
			");

            using (var session = OpenSession())
            {
                parametros.Add(q => q.SetParameterList("itensDespachos", itensDespachos));

                var query = session.CreateSQLQuery(string.Format(hql.ToString()));

                foreach (var p in parametros)
                {
                    p.Invoke(query);
                }

                return query.List<string>();
            }
        }


        /// <summary>
        /// Obtem as notas dos vag�es que possuem PDF anexado no processo de faturamento
        /// </summary>
        /// <param name="itensDespachos">Identificador dos itens de despacho</param>
        /// <returns>Retorna as chaves das nfe</returns>
        public IList<decimal> ObterNfePdfItensDespacho(IList<int> itensDespachos)
        {
            var parametros = new List<Action<IQuery>>();
            var hql = new StringBuilder();

            hql.Append(@"
                SELECT DISTINCT NVL(PDF_EDI.NFE_ID_PDF, PDF_SIM.NFE_ID_PDF)
                  FROM ITEM_DESPACHO           IDS 
                 INNER JOIN NOTA_FISCAL        NF  ON NF.DP_ID_DP      = IDS.DP_ID_DP
                  LEFT JOIN EDI.EDI2_NFE       NFE ON NFE.NFE_CHAVE    = NF.NFE_CHAVE_NFE
                  LEFT JOIN NFE_SIMCONSULTAS   SIM ON SIM.NFE_CHAVE    = NF.NFE_CHAVE_NFE
                  LEFT JOIN EDI.EDI2_NFE_PDF   PDF_EDI ON PDF_EDI.NFE_ID_NFE = NFE.NFE_ID_NFE
                  LEFT JOIN EDI.EDI2_NFE_PDF   PDF_SIM ON PDF_SIM.NFE_ID_SIMCONSULTAS = SIM.NFE_ID_NFE
                 WHERE IDS.ID_ID_ITD IN (:itensDespachos)
                   AND NVL(PDF_EDI.NFE_ID_PDF, PDF_SIM.NFE_ID_PDF) IS NOT NULL
			");

            using (var session = OpenSession())
            {
                parametros.Add(q => q.SetParameterList("itensDespachos", itensDespachos));
                var query = session.CreateSQLQuery(string.Format(hql.ToString()));

                foreach (var p in parametros)
                {
                    p.Invoke(query);
                }

                return query.List<decimal>();
            }
        }


        /// <summary>
        /// Obtem as notas dos vag�es que possuem PDF anexado no processo de faturamento
        /// </summary>
        /// <param name="itensDespachos">Identificador dos itens de despacho</param>
        /// <returns>Retorna as chaves das nfe</returns>
        public IList<ItemDespachoNfeDto> ObterNfePdfItensDespachoDto(IList<int> itensDespachos)
        {
            var parametros = new List<Action<IQuery>>();
            var hql = new StringBuilder();

            hql.Append(@"
                SELECT DISTINCT 
                       IDS.ID_ID_ITD                                AS ""IdItemDespacho""
                     , NVL( NVL( PDF_EDI.NFE_ID_PDF
                               , PDF_SIM.NFE_ID_PDF)
                          , 0)                                      AS ""IdNfePdf""
                     , NF.NFE_CHAVE_NFE                             AS ""ChaveNfe""
                  FROM ITEM_DESPACHO           IDS
                 INNER JOIN NOTA_FISCAL        NF  ON NF.DP_ID_DP      = IDS.DP_ID_DP
                  LEFT JOIN EDI.EDI2_NFE       NFE ON NFE.NFE_CHAVE    = NF.NFE_CHAVE_NFE
                  LEFT JOIN NFE_SIMCONSULTAS   SIM ON SIM.NFE_CHAVE    = NF.NFE_CHAVE_NFE
                  LEFT JOIN EDI.EDI2_NFE_PDF   PDF_EDI ON PDF_EDI.NFE_ID_NFE = NFE.NFE_ID_NFE
                  LEFT JOIN EDI.EDI2_NFE_PDF   PDF_SIM ON PDF_SIM.NFE_ID_SIMCONSULTAS = SIM.NFE_ID_NFE
                 WHERE IDS.ID_ID_ITD IN (:itensDespachos)
            ");

            using (var session = OpenSession())
            {
                parametros.Add(q => q.SetParameterList("itensDespachos", itensDespachos));

                var query = session.CreateSQLQuery(string.Format(hql.ToString()));

                foreach (var p in parametros)
                {
                    p.Invoke(query);
                }

                query.SetResultTransformer(Transformers.AliasToBean<ItemDespachoNfeDto>());
                var itens = query.List<ItemDespachoNfeDto>();

                return itens;
            }
        }

        /// <summary>
        /// Obtem as notas dos vag�es que possuem PDF anexado no processo de faturamento
        /// </summary>
        /// <param name="listaCodigosVagoes">Identificador dos C�digos dos Vag�es</param>
        /// <returns>Retorna as chaves das nfe</returns>
        public IList<LaudoMercadoriaDto> ObterLaudoMercadoriaPdfItensDespachoDto(IList<string> listaCodigosVagoes)
        {
            var parametros = new List<Action<IQuery>>();
            var hql = new StringBuilder();

            hql.Append(@"
                     SELECT DISTINCT
                            L.ID_WS_LAUDO               AS ""Id""
                          , ITD.ID_ID_ITD               AS ""ItemDespachoId""
                          , L.CODIGO_VAGAO              AS ""CodigoVagao""
                          , L.CLIENTE_NOME              AS ""ClienteNome""
                          , L.CLIENTE_CNPJ              AS ""ClienteCnpj""
                          , L.DATA_CLASSIFICACAO        AS ""DataClassificacao""
                          , L.NUMERO_LAUDO              AS ""NumeroLaudo""
                          , L.NUMERO_OS_LAUDO           AS ""NumeroOsLaudo""
                          , L.PESO_LIQUIDO              AS ""PesoLiquido""
                          , L.DESTINO                   AS ""Destino""
                          , L.CLASSIFICADOR_NOME        AS ""ClassificadorNome""
                          , L.CLASSIFICADOR_CPF         AS ""ClassificadorCpf""
                          , L.AUTORIZADO_POR            AS ""AutorizadoPor""
                          , LT.DESCRICAO                AS ""TipoLaudo""
                          , LE.NOME_EMPRESA             AS ""EmpresaNome""
                          , LPT.DESCRICAO               AS ""Produto""
                          , LCT.DESCRICAO               AS ""Classificacao""
                          , LCO.ORDEM                   AS ""ClassificacaoOrdem""
                          , LP.VALOR                    AS ""Porcentagem""
                          , L.NUMERO_NFE                AS ""NumeroNfe""
                          , L.CHAVE_NFE                 AS ""ChaveNfe""
                          , L.DESCRICAO_PRODUTO         AS ""DescricaoProduto""

                          , LE.ID_WS_LAUDO_EMPRESA      AS ""EmpresaGeradoraId""
                          , LE.NOME_EMPRESA             AS ""EmpresaGeradoraNome""
                          , LE.CNPJ_EMPRESA             AS ""EmpresaGeradoraCnpj""
                          , LE.ENDERECO_PRINCIPAL       AS ""EmpresaGeradoraEndereco""
                          , LE.CEP_EMPRESA              AS ""EmpresaGeradoraCep""
                          , LE.CIDADE_DESCRICAO         AS ""EmpresaGeradoraCidadeDescricao""
                          , LE.ESTADO_SIGLA             AS ""EmpresaGeradoraEstadoSigla""
                          , LE.IE_EMPRESA               AS ""EmpresaGeradoraIe""
                          , LE.TELEFONE_EMPRESA         AS ""EmpresaGeradoraTelefone""
                          , LE.FAX_EMPRESA              AS ""EmpresaGeradoraFax""

                       FROM WS_LAUDO_MENSAGEM             MENSAGEM
                 INNER JOIN WS_LAUDO_LOG                  LL  ON LL.ID_WS_LAUDO_MENSAGEM = MENSAGEM.ID_WS_LAUDO_MENSAGEM
                 INNER JOIN WS_LAUDO_EMPRESA              LE  ON LE.ID_WS_LAUDO_EMPRESA = LL.ID_WS_LAUDO_EMPRESA
                 INNER JOIN WS_LAUDO                      L   ON L.ID_WS_LAUDO_LOG = LL.ID_WS_LAUDO_LOG
                 INNER JOIN WS_LAUDO_PRODUTO              LP  ON LP.ID_WS_LAUDO = L.ID_WS_LAUDO
                 INNER JOIN WS_LAUDO_TIPO                 LT  ON LT.ID_WS_LAUDO_TIPO = L.ID_WS_LAUDO_TIPO
                 INNER JOIN WS_LAUDO_PRODUTO_TIPO         LPT ON LPT.ID_WS_LAUDO_PRODUTO_TIPO = L.ID_WS_TIPO_PRODUTO
                 INNER JOIN WS_LAUDO_CLASSIFICACAO_TIPO   LCT ON LCT.ID_WS_LAUDO_CLASSIFICACAO_TIPO = LP.ID_WS_LAUDO_CLASSIFICACAO_TIPO
                  LEFT JOIN WS_LAUDO_CLASSIFICACAO_ORDEM  LCO ON LCO.ID_WS_LAUDO_PRODUTO_TIPO = LPT.ID_WS_LAUDO_PRODUTO_TIPO 
                                                             AND LCO.ID_WS_LAUDO_CLASSIFICACAO_TIPO = LCT.ID_WS_LAUDO_CLASSIFICACAO_TIPO
                 INNER JOIN DESPACHO                        D ON D.DP_ID_DP = L.ID_DESPACHO
                 INNER JOIN ITEM_DESPACHO                 ITD ON ITD.DP_ID_DP = D.DP_ID_DP
                 INNER JOIN (
                       SELECT MAX(TMP.ID_WS_LAUDO_MENSAGEM) AS ID
                            , TMP.PROTOCOLO_REQUEST         AS PROTOCOLO_REQUEST
                            , TMP_LOG.ID_WS_LAUDO_EMPRESA
                         FROM WS_LAUDO_MENSAGEM TMP
                         JOIN WS_LAUDO_LOG TMP_LOG ON TMP_LOG.ID_WS_LAUDO_MENSAGEM = TMP.ID_WS_LAUDO_MENSAGEM
                        GROUP BY TMP_LOG.ID_WS_LAUDO_EMPRESA
                               , TMP.PROTOCOLO_REQUEST
                  ) TMP_MAX ON TMP_MAX.ID = MENSAGEM.ID_WS_LAUDO_MENSAGEM
  
                      WHERE L.CODIGO_VAGAO IN (:listaCodigosVagoes)  /*ITD.ID_ID_ITD IN (:itensDespachos)*/
			");

            using (var session = OpenSession())
            {
                parametros.Add(q => q.SetParameterList("listaCodigosVagoes", listaCodigosVagoes));
                var query = session.CreateSQLQuery(string.Format(hql.ToString()));

                foreach (var p in parametros)
                {
                    p.Invoke(query);
                }

                query.SetResultTransformer(Transformers.AliasToBean<LaudoMercadoriaDto>());
                var itens = query.List<LaudoMercadoriaDto>();

                return itens;
            }
        }

        /// <summary>
        /// Obt�m todos os ID das composi��es e trens pendentes a serem enviados pelo job de envio de documenta��o:
        /// </summary>
        /// <param name="areaOperacionalEnvioId">�rea operacional m�e onde o trem deve ter passado para enviar a documenta��o</param>
        /// <param name="terminalDestinoId">Terminal de destino que receber� o e-mail</param>
        /// <returns>Retorna lista de ids das composi��es para envio</returns>
        public IList<decimal> ObterComposicoesNaoEnviadas(int areaOperacionalEnvioId, int terminalDestinoId)
        {
            var sql = string.Format(@"
                    SELECT DISTINCT CP.CP_ID_CPS
                      FROM COMPVAGAO_VIG        CVV
                      JOIN VAGAO_PEDIDO_VIG     VPV     ON VPV.VG_ID_VG  = CVV.VG_ID_VG AND VPV.PT_ID_ORT = CVV.PT_ID_ORT                      
                      JOIN COMPOSICAO           CP      ON CP.CP_ID_CPS  = CVV.CP_ID_CPS
                      JOIN TREM                 TR      ON TR.TR_ID_TRM  = CP.TR_ID_TRM
                      JOIN AREA_OPERACIONAL     AOT     ON AOT.AO_ID_AO  = TR.AO_ID_AO_DST
                      JOIN FLUXO_COMERCIAL      FC      ON FC.FX_ID_FLX = VPV.FX_ID_FLX                      
                      JOIN MOVIMENTACAO_TREM    MT      ON MT.TR_ID_TRM  = TR.TR_ID_TRM
                      JOIN AREA_OPERACIONAL     AOMT    ON AOMT.AO_ID_AO = MT.AO_ID_AO
                     WHERE TR.TR_STT_TRM != 'E'                      /* Trem n�o est� encerrado */
                       AND TR.AO_ID_AO_PVT IS     NULL               /* Composi��o fechada */
                       AND MT.MT_DTS_RAL   IS NOT NULL               /* Movimenta��o realizada */
                       AND FC.EP_ID_EMP_DST  = :terminalDestino
                       AND CP.AO_ID_AO_ORG   = :areaOperacionalEnvio /* Composi��o das �reas operacionais de envio */
                       AND AOMT.AO_ID_AO_INF = :areaOperacionalEnvio /* Movimenta��o do trem nas �reas operacionais de envio */

                       AND NOT EXISTS(                               /* E n�o foi inserido anteriormente na tabela de envios de documentacao */
                                    SELECT 1
                                      FROM HIST_ENVIO_DOC HIST
                                     WHERE HIST.ID_COMPOSICAO = CP.CP_ID_CPS
                                       AND ROWNUM = 1
                       )
                ");

            var session = this.SessionManager.OpenStatelessSession();
            var query = session.CreateSQLQuery(sql);

            query.SetDecimal("terminalDestino", terminalDestinoId);
            query.SetDecimal("areaOperacionalEnvio", areaOperacionalEnvioId);

            return query.List<decimal>();
        }
    }
}