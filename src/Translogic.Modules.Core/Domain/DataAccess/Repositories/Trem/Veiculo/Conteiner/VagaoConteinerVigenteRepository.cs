namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Trem.Veiculo.Conteiner
{
	using System;
	using ALL.Core.AcessoDados;
	using Model.Trem.Veiculo.Conteiner;
	using Model.Trem.Veiculo.Conteiner.Repositories;
	using Model.Trem.Veiculo.Vagao;
	using NHibernate;
	using NHibernate.Criterion;
	using NHibernate.SqlCommand;

	/// <summary>
	/// Repositorio para <see cref="VagaoConteinerVigente"/>
	/// </summary>
	public class VagaoConteinerVigenteRepository : NHRepository<VagaoConteinerVigente, int?>, IVagaoConteinerVigenteRepository
	{
		/// <summary>
		/// Obt�m o vag�o conteiner por vag�o
		/// </summary>
		/// <param name="vagao">Vag�o que tem o conteiner</param>
		/// <returns>Retorna o objeto vag�o conteiner</returns>
		public VagaoConteinerVigente ObterPorVagao(Vagao vagao)
		{
			DetachedCriteria criteria = CriarCriteria();
			criteria.Add(Restrictions.Eq("Vagao", vagao));
			return ObterPrimeiro(criteria);
		}

		/// <summary>
		/// Obt�m o vag�o conteiner por c�digo do vag�o
		/// </summary>
		/// <param name="codigoVagao">C�digo do vag�o</param>
		/// <returns>Retorna o objeto vag�o conteiner</returns>
		public VagaoConteinerVigente ObterPorCodigoVagao(string codigoVagao)
		{
			DetachedCriteria criteria = CriarCriteria();
			criteria.CreateAlias("Vagao", "vagao", JoinType.InnerJoin);

			criteria.Add(Restrictions.Eq("vagao.Codigo", codigoVagao));

			criteria.SetFetchMode("vagao", FetchMode.Eager);
			return ObterPrimeiro(criteria);
		}

	    /// <summary>
	    /// Obt�m o vag�o conteiner por conteiner
	    /// </summary>
	    /// <param name="conteiner">Objeto conteiner</param>
	    /// <returns>Retorna o objeto vag�o conteiner</returns>
	    public VagaoConteinerVigente ObterPorConteiner(Conteiner conteiner)
	    {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.Eq("Conteiner", conteiner));
            return ObterPrimeiro(criteria);
	    }
	}
}