﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Trem.Veiculo.Conteiner
{
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;
    using NHibernate.Criterion;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Conteiner;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Conteiner.Repositories;

    /// <summary>
    /// Repositorio para <see cref="VagaoConteiner"/>
    /// </summary>
    public class VagaoConteinerRepository : NHRepository<VagaoConteiner, int?>, IVagaoConteinerRepository
    {
        /// <summary>
        /// Retorna o vagão por código
        /// </summary>
        /// <param name="codigo">codigo vagão</param>
        /// <returns>O Vagao objeto</returns>
        public VagaoConteiner ObterPorCodigo(string codigo)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.Like("Codigo", "%" + codigo));
            return ObterPrimeiro(criteria);
        }

        /// <summary>
        /// Retorna o conteiners do vagão
        /// </summary>
        /// <param name="vagao">id vagão do conteiner</param>
        /// <returns>O VagaoConteiner objeto</returns>
        public IList<VagaoConteiner> ObterPorVagao(int vagao)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.Eq("Vagao.Id", vagao));
            return ObterTodos(criteria);
        }
    }
}
