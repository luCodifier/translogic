﻿using System.Linq;
using Translogic.Modules.Core.Domain.Model.Dto;

namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Trem.Veiculo.Conteiner
{
	using System;
	using System.Collections.Generic;
	using System.Data;
	using System.Data.OracleClient;
    using System.Text;
	using ALL.Core.AcessoDados;
	using NHibernate;
	using NHibernate.Criterion;
    using NHibernate.Transform;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Conteiner;
	using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Conteiner.Repositories;

	/// <summary>
	/// Repositorio para <see cref="Conteiner"/>
	/// </summary>
	public class ConteinerRepository : NHRepository<Conteiner, int?>, IConteinerRepository
	{
		/// <summary>
		/// Retorna o Conteiner a partir do código
		/// </summary>
		/// <param name="codigo">Código do conteiner</param>
		/// <returns>Objeto Conteiner</returns>
		public Conteiner ObterPorCodigo(string codigo)
		{
			DetachedCriteria criteria = CriarCriteria();
			criteria.Add(Restrictions.Like("Numero", codigo));
			return ObterPrimeiro(criteria);
		}
        
        /// <summary>
        /// Retorna o Conteiner a partir do código sem estado
        /// </summary>
        /// <param name="codigo">Código do conteiner</param>
        /// <returns>Objeto Conteiner</returns>
        public Conteiner ObterPorCodigoSemEstado(string codigo)
        {
            using (var session = SessionManager.OpenStatelessSession())
            {
                var criteria = session.CreateCriteria<Conteiner>().Add(Restrictions.Like("Numero", codigo));
                return criteria.List<Conteiner>().FirstOrDefault();
            }
        }

		/// <summary>
		/// Voa o vagão para a estação
		/// </summary>
		/// <param name="idConteiner">Número do conteiner</param>
		/// <param name="idAoOri"> Id da área operacional mãe do destino do contêiner</param>
		/// <param name="usuario">Usuário logado no sistema</param>
		public void VoarConteiner(string idConteiner, int idAoOri, string usuario)
		{
			idConteiner = string.Concat(idConteiner, ";");
			string motivo = "1131: Reposicionamento para carregamento";
			string tipoLocalVoado = "E";

			using (ISession session = OpenSession())
			{
				var command = new OracleCommand
				{
					CommandType = CommandType.StoredProcedure,
					CommandText = "PKG_CONTEINER.SP_CONTEINER_VOADOR_LISTA",
					Connection = (OracleConnection)session.Connection
				};

				AddParameter(command, "PLISTA", OracleType.VarChar, idConteiner, ParameterDirection.Input);
				AddParameter(command, "PIDLOCALVOADO", OracleType.Number, idAoOri, ParameterDirection.Input);
				AddParameter(command, "PTIPOLOCALVOADO", OracleType.VarChar, tipoLocalVoado, ParameterDirection.Input);
				AddParameter(command, "PMOTIVO", OracleType.VarChar, motivo, ParameterDirection.Input);
				AddParameter(command, "PPESSOASOLIC", OracleType.VarChar, usuario, ParameterDirection.Input);
				AddParameter(command, "PDATA", OracleType.DateTime, DateTime.Now, ParameterDirection.Input);
				AddParameter(command, "PUSUARIO", OracleType.VarChar, usuario, ParameterDirection.Input);

				session.Transaction.Enlist(command);

				command.Prepare();
				command.ExecuteNonQuery();
			}
		}

		/// <summary>
		/// Retorna a lista de Conteiners a partir dos códigos
		/// </summary>
		/// <param name="codigos">Código do conteiner</param>
		/// <returns>Objeto de lista de Conteiner</returns>
		public IList<Conteiner> ObterPorCodigo(string[] codigos)
		{
			DetachedCriteria criteria = CriarCriteria();
			criteria.Add(Restrictions.In("Numero", codigos));
			return ObterTodos(criteria);
		}

	    /// <summary>
	    /// Retorna a lista de Conteiners a partir do id vagao
	    /// </summary>
	    /// <param name="idVagao">Id do vagão</param>
	    /// <returns>Objeto de lista de Conteiner</returns>
	    public IList<Conteiner> ObterPorVagaoVigente(int idVagao)
	    {
            var comandoSql = new StringBuilder();

	        comandoSql.AppendLine(" select * from conteiner where CN_NUM_CNT in ");
	        comandoSql.AppendLine(" ( ");
	        comandoSql.AppendLine(" select distinct(det.nf_conteiner) as NroConteiner ");
	        comandoSql.AppendLine("   from cte              c, ");
	        comandoSql.AppendLine("        cte_det          det, ");
	        comandoSql.AppendLine("        despacho         do, ");
	        comandoSql.AppendLine("        vagao_pedido_vig v, ");
	        comandoSql.AppendLine("        fluxo_comercial  fl, ");
	        comandoSql.AppendLine("        item_despacho    ide ");
	        comandoSql.AppendLine(" where do.px_idt_pdr = v.px_idt_pdr ");
	        comandoSql.AppendLine("    and do.dp_id_dp = c.dp_id_dp ");
	        comandoSql.AppendLine("    and fl.fx_id_flx = c.fx_id_flx ");
	        comandoSql.AppendLine("    and c.id_cte = det.id_cte ");
	        comandoSql.AppendLine("    and do.dp_id_dp = ide.dp_id_dp ");
            comandoSql.AppendLine("    and v.vg_id_vg = " + idVagao + " ");
            comandoSql.AppendLine(" ) ");

            using (var session = OpenSession())
	        {
                var query = session.CreateSQLQuery(comandoSql.ToString());
	            query.SetResultTransformer(Transformers.AliasToBean(typeof(Conteiner)));
                return query.List<Conteiner>();
	        }
	    }

        /// <summary>
        ///    Retorna a lista de Conteiners do vagao vigente a partir da chave do cte de algum vagao vigente
        /// </summary>
        /// <param name="chaveCte">Chave o cte</param>
        /// <returns>Contagem do total de conteiners</returns>
        public int ObterTotalConteinersVagaoVigente(string chaveCte)
        {
            var comandoSql = new StringBuilder();

            comandoSql.AppendLine(" select count(1) as cont from conteiner where CN_NUM_CNT in ");
            comandoSql.AppendLine(" ( ");
            comandoSql.AppendLine(" select distinct(det.nf_conteiner) as NroConteiner ");
            comandoSql.AppendLine("   from cte               c, ");
            comandoSql.AppendLine("        cte_det           det, ");
            comandoSql.AppendLine("        despacho          do, ");
            comandoSql.AppendLine("        vagao_pedido_vig  v, ");
            comandoSql.AppendLine("        fluxo_comercial   fl, ");
            comandoSql.AppendLine("        item_despacho     ide ");
            comandoSql.AppendLine(" where do.px_idt_pdr = v.px_idt_pdr ");
            comandoSql.AppendLine("    and do.dp_id_dp = c.dp_id_dp ");
            comandoSql.AppendLine("    and fl.fx_id_flx = c.fx_id_flx ");
            comandoSql.AppendLine("    and c.id_cte = det.id_cte ");
            comandoSql.AppendLine("    and do.dp_id_dp = ide.dp_id_dp ");
            comandoSql.AppendLine("    and v.vg_id_vg in (select VG_ID_VG from cte where chave_cte = '" + chaveCte + "')  ");
            comandoSql.AppendLine(" ) ");

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(comandoSql.ToString());
                return (Int32)query.UniqueResult<Decimal>();
            }
        }

        /// <summary>
        ///    Retorna a lista de Conteiners do vagao vigente e fluxo a partir da chave do cte
        /// </summary>
        /// <param name="chaveCte">Chave o cte</param>
        /// <returns>Contagem do total de conteiners</returns>
        public int ObterTotalConteinersVagaoVigenteFluxo(string chaveCte)
        {
            var comandoSql = new StringBuilder();

            comandoSql.AppendLine(" select count(1) as cont from conteiner where CN_NUM_CNT in ");
            comandoSql.AppendLine(" ( ");
            comandoSql.AppendLine(" select distinct(det.nf_conteiner) as NroConteiner ");
            comandoSql.AppendLine("   from cte               c, ");
            comandoSql.AppendLine("        cte_det           det, ");
            comandoSql.AppendLine("        despacho          do, ");
            comandoSql.AppendLine("        vagao_pedido_vig  v, ");
            comandoSql.AppendLine("        fluxo_comercial   fl, ");
            comandoSql.AppendLine("        item_despacho     ide ");
            comandoSql.AppendLine(" where do.px_idt_pdr = v.px_idt_pdr ");
            comandoSql.AppendLine("    and do.dp_id_dp = c.dp_id_dp ");
            comandoSql.AppendLine("    and fl.fx_id_flx = c.fx_id_flx ");
            comandoSql.AppendLine("    and c.id_cte = det.id_cte ");
            comandoSql.AppendLine("    and do.dp_id_dp = ide.dp_id_dp ");
            comandoSql.AppendLine("    and v.vg_id_vg in (select VG_ID_VG from cte where chave_cte = '" + chaveCte + "')  ");
            comandoSql.AppendLine("    and fl.FX_ID_FLX in (select FX_ID_FLX from cte where chave_cte =  '" + chaveCte + "') ");
            comandoSql.AppendLine(" ) ");

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(comandoSql.ToString());
                return (Int32)query.UniqueResult<Decimal>();
            }
        }

        /// <summary>
        /// Retorna a lista de Conteiners a partir do id vagao e CTE
        /// </summary>
        /// <param name="idVagao">Id do vagão</param>
        /// <param name="chaveCte">Cte</param>
        /// <returns>Objeto de lista de Conteiner</returns>
        public IList<ConteinerDto> ObterPorVagaoVigenteCte(string chaveCte)
	    {
            var comandoSql = new StringBuilder();

            comandoSql.AppendLine("  select distinct  ");
            comandoSql.AppendLine("       NroConteiner as \"Nro\" from ");
            comandoSql.AppendLine(" ( ");
            comandoSql.AppendLine(" select distinct(det.nf_conteiner) as NroConteiner ");
            comandoSql.AppendLine("   from cte              c, ");
            comandoSql.AppendLine("        cte_det          det, ");
            comandoSql.AppendLine("        despacho         do, ");
            comandoSql.AppendLine("        vagao_pedido_vig v, ");
            comandoSql.AppendLine("        fluxo_comercial  fl, ");
            comandoSql.AppendLine("        item_despacho    ide ");
            comandoSql.AppendLine(" where do.px_idt_pdr = v.px_idt_pdr ");
            comandoSql.AppendLine("    and do.dp_id_dp = c.dp_id_dp ");
            comandoSql.AppendLine("    and fl.fx_id_flx = c.fx_id_flx ");
            comandoSql.AppendLine("    and c.id_cte = det.id_cte ");
            comandoSql.AppendLine("    and do.dp_id_dp = ide.dp_id_dp ");
            comandoSql.AppendLine("    and v.vg_id_vg in (select VG_ID_VG from cte where chave_cte =  '" + chaveCte + "') ");
            comandoSql.AppendLine("    and det.nf_conteiner in (select distinct nf_conteiner from cte_det cd inner ");
            comandoSql.AppendLine("    join cte c on (c.Id_Cte = cd.Id_Cte) where ");
            comandoSql.AppendLine("    c.chave_cte = '" + chaveCte + "')");
            comandoSql.AppendLine(" ) ");

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(comandoSql.ToString());
                query.SetResultTransformer(Transformers.AliasToBean<ConteinerDto>());
                return query.List<ConteinerDto>();
            }
	    }

        /// <summary>
        /// Retorna a lista de Conteiners a partir do id vagao , fluxo e CTE
        /// </summary>
        /// <param name="idVagao">Id do vagão</param>
        /// <param name="chaveCte">Cte</param>
        /// <returns>Objeto de lista de Conteiner</returns>
        public IList<ConteinerDto> ObterPorVagaoVigenteFluxoCte(string chaveCte)
        {
            var comandoSql = new StringBuilder();

            comandoSql.AppendLine("  select    ");
            comandoSql.AppendLine("       NroConteiner as \"Nro\" from ");
            comandoSql.AppendLine(" ( ");
            comandoSql.AppendLine(" select distinct(det.nf_conteiner) as NroConteiner ");
            comandoSql.AppendLine("   from cte              c, ");
            comandoSql.AppendLine("        cte_det          det, ");
            comandoSql.AppendLine("        despacho         do, ");
            comandoSql.AppendLine("        vagao_pedido_vig v, ");
            comandoSql.AppendLine("        fluxo_comercial  fl, ");
            comandoSql.AppendLine("        item_despacho    ide ");
            comandoSql.AppendLine(" where do.px_idt_pdr = v.px_idt_pdr ");
            comandoSql.AppendLine("    and do.dp_id_dp = c.dp_id_dp ");
            comandoSql.AppendLine("    and fl.fx_id_flx = c.fx_id_flx ");
            comandoSql.AppendLine("    and c.id_cte = det.id_cte ");
            comandoSql.AppendLine("    and do.dp_id_dp = ide.dp_id_dp ");
            comandoSql.AppendLine("    and v.vg_id_vg in (select VG_ID_VG from cte where chave_cte =  '" + chaveCte + "') ");
            comandoSql.AppendLine("    and fl.FX_ID_FLX in (select FX_ID_FLX from cte where chave_cte =  '" + chaveCte + "') ");
            comandoSql.AppendLine("    and det.nf_conteiner in (select distinct nf_conteiner from cte_det cd inner ");
            comandoSql.AppendLine("    join cte c on (c.Id_Cte = cd.Id_Cte) where ");
            comandoSql.AppendLine("    c.chave_cte = '" + chaveCte + "')");
            comandoSql.AppendLine(" ) ");

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(comandoSql.ToString());
                query.SetResultTransformer(Transformers.AliasToBean<ConteinerDto>());
                return query.List<ConteinerDto>();
            }
        }

	    /// <summary>
		/// Adicionar um novo <see cref="OracleParameter"/> ao <see cref="OracleCommand"/>
		/// </summary>
		/// <param name="command"><see cref="OracleCommand"/> onde o <see cref="OracleParameter"/> será adicionado</param>
		/// <param name="nome">Nome do parâmetro</param>
		/// <param name="tipo">Tipo do parâmetro <see cref="OracleType"/></param>
		/// <param name="valor">Valor de entrada</param>
		/// <param name="direcao">Direção <see cref="ParameterDirection"/></param>
		private void AddParameter(OracleCommand command, string nome, OracleType tipo, object valor, ParameterDirection direcao)
		{
			OracleParameter parameter = new OracleParameter();
			parameter.ParameterName = nome;
			parameter.OracleType = tipo;
			parameter.Direction = direcao;

			parameter.Value = DBNull.Value;
			if (valor != null)
			{
				parameter.Value = valor;
			}

			command.Parameters.Add(parameter);
		}	  
    }
}
