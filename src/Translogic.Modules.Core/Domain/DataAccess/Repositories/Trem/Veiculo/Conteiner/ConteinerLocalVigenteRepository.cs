﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Trem.Veiculo.Conteiner
{
    using ALL.Core.AcessoDados;
    using NHibernate.Criterion;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Conteiner;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Conteiner.Repositories;

    /// <summary>
    /// Repositorio para <see cref="Conteiner"/>
    /// </summary>
	public class ConteinerLocalVigenteRepository : NHRepository<ConteinerLocalVigente, int?>, IConteinerLocalVigenteRepository
    {
		/// <summary>
		/// Obtem um ConteinerLocalVigente através do código do conteiner
		/// </summary>
		/// <param name="codigo">Código do conteiner</param>
		/// <returns>Retorna um ConteinerLocalVigente</returns>
		public ConteinerLocalVigente ObterPorCodConteiner(int? codigo)
		{
			DetachedCriteria criteria = CriarCriteria();
			criteria.Add(Restrictions.Eq("CodConteiner", codigo));
			return ObterPrimeiro(criteria);
		}
    }
}
