namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Trem.Veiculo.Vagao
{
    using ALL.Core.AcessoDados;
    using Model.Trem.Veiculo.Vagao;
    using Model.Trem.Veiculo.Vagao.Repositories;

    /// <summary>
    /// Repositório para <see cref="VagaoPatio"/>
    /// </summary>
    public class VagaoPatioRepository : NHRepository<VagaoPatio, int?>, IVagaoPatioRepository
    {
    }
}