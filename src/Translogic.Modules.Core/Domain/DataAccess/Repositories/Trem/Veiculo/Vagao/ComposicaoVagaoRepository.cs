namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Trem.Veiculo.Locomotiva
{
    using System;
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;
    using Model.Trem;
    using Model.Trem.OrdemServico.Repositories;
    using Model.Trem.Veiculo.Locomotiva;
    using Model.Trem.Veiculo.Locomotiva.Repositories;
    using Model.Trem.Veiculo.Vagao;
    using NHibernate;
    using NHibernate.Criterion;

    /// <summary>
    /// Repositório para <see cref="ComposicaoLocomotiva"/>
    /// </summary>
    public class ComposicaoVagaoRepository : NHRepository<ComposicaoVagao, int>, IComposicaoVagaoRepository
    {
        /// <summary>
        /// Retorna a ComposicaoVagao a partir da composicao
        /// </summary>
        /// <param name="composicao">Composicao de referencia</param>
        /// <returns>A ComposicaoVagao</returns>
        public IList<ComposicaoVagao> ObterPorComposicao(Composicao composicao)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.Eq("Composicao", composicao));
            criteria.SetFetchMode("Vagao", FetchMode.Join);

            return ObterTodos(criteria);
        }
    }
}