namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Trem.Veiculo.Vagao
{
    using ALL.Core.AcessoDados;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao.Repositories;

    /// <summary>
    /// Repositório para <see cref="SerieVagao"/>
    /// </summary>
    public class SerieVagaoRepository : NHRepository<SerieVagao, int>, ISerieVagaoRepository
    {
    }
}