namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Trem.Veiculo.Vagao.QuadroEstados
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using ALL.Core.AcessoDados;
    using Model.Trem.Veiculo.Vagao;
    using NHibernate;
    using NHibernate.Criterion;
    using NHibernate.SqlCommand;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao.QuadroEstado;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao.QuadroEstado.Repositories;

    /// <summary>
    /// Reposit�rio para <see cref="SituacaoVagaoVigente"/>
    /// </summary>
    public class SituacaoVagaoVigenteRepository : NHRepository<SituacaoVagaoVigente, int>, ISituacaoVagaoVigenteRepository
    {
        /// <summary>
        /// Obt�m a situa��o do vag�o que est� vigente para o vag�o
        /// </summary>
        /// <param name="vagao">Objeto vagao</param>
        /// <returns>Objeto SituacaoVagaoVigente</returns>
        public SituacaoVagaoVigente ObterPorVagao(Vagao vagao)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.CreateAlias("Situacao", "st", JoinType.InnerJoin);
            criteria.CreateAlias("Vagao", "vg", JoinType.InnerJoin);
            criteria.Add(Restrictions.Eq("vg.Id", vagao.Id));
            criteria.SetFetchMode("st", FetchMode.Eager);
            criteria.SetFetchMode("vg", FetchMode.Eager);
            return ObterPrimeiro(criteria);
        }
    }
}