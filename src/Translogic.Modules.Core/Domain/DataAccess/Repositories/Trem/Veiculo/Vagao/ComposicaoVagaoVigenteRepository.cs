namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Trem.Veiculo.Vagao
{
    using System.Collections.Generic;
    using System.Text;
    using ALL.Core.AcessoDados;
    using Model.Trem.Veiculo.Vagao;
    using Model.Trem.Veiculo.Vagao.Repositories;
    using NHibernate;
    using NHibernate.Criterion;

    using Translogic.Modules.Core.Domain.Model.Trem;

	/// <summary>
    /// Reposit�rio para <see cref="ComposicaoVagaoVigente"/>
    /// </summary>
    public class ComposicaoVagaoVigenteRepository : NHRepository<ComposicaoVagaoVigente, int>, IComposicaoVagaoVigenteRepository
    {
        /// <summary>
        /// Retorna a ComposicaoVagao a partir do c�digo do vag�o
        /// </summary>
        /// <param name="vagao">Objeto Vagao</param>
        /// <returns>A ComposicaoVagaoVigente</returns>
        public ComposicaoVagaoVigente ObterPorVagao(Vagao vagao)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"
                        FROM ComposicaoVagaoVigente cvv
						  WHERE
                            cvv.Vagao.Id = :idVagao");

            ISession session = OpenSession();
            IQuery query = session.CreateQuery(sb.ToString());
            query.SetInt32("idVagao", vagao.Id.Value);
            
            IList<ComposicaoVagaoVigente> list = query.List<ComposicaoVagaoVigente>();

            return list.Count > 0 ? list[0] : null;
        }

		/// <summary>
		/// Retorna as ComposicaoVagaoVigente a partir da composicao
		/// </summary>
		/// <param name="composicao">Composicao de referencia</param>
		/// <returns>Lista de ComposicaoVagaoVigente</returns>
		public IList<ComposicaoVagaoVigente> ObterPorComposicao(Composicao composicao)
	    {
			DetachedCriteria criteria = CriarCriteria();
			criteria.Add(Restrictions.Eq("Composicao", composicao));
			criteria.SetFetchMode("Vagao", FetchMode.Join);

			return ObterTodos(criteria);
	    }
    }
}