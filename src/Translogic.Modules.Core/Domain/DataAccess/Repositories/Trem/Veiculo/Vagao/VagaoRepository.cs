﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Trem.Veiculo.Vagao
{
    using System;
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;
    using ALL.Core.Dominio;
    using NHibernate.Criterion;
    using NHibernate.Transform;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao.Repositories;

    /// <summary>
    /// Repositorio para <see cref="Vagao"/>
    /// </summary>
    public class VagaoRepository : NHRepository<Vagao, int?>, IVagaoRepository
    {
        /// <summary>
        /// Retorna o vagão por código
        /// </summary>
        /// <param name="codigo">codigo vagão</param>
        /// <returns>O Vagao objeto</returns>
        public Vagao ObterPorCodigo(string codigo)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.Like("Codigo", "%" + codigo));
            return ObterPrimeiro(criteria);
        }

        /// <summary>
        /// Retorna o vagão por código
        /// </summary>
        /// <param name="codigo">codigo vagão</param>
        /// <returns>O Vagao objeto</returns>
        public IList<Vagao> ObterSeriePorCodigo(string codigo)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.Eq("Codigo", codigo));
            return ObterTodos(criteria);
        }

        /// <summary>
        /// Obtém um vagão pelo código exato
        /// </summary>
        /// <param name="codigo">Código do vagão</param>
        /// <returns>Vagão localizado</returns>
        public Vagao ObterVagaoPorCodigoExato(string codigo)
        {
            var hqlQuery = @"SELECT V FROM Vagao V WHERE V.Codigo = :codigo";

            using (var session = OpenSession())
            {
                var query = session.CreateQuery(hqlQuery);
                query.SetString("codigo", codigo);

                query.SetResultTransformer(Transformers.DistinctRootEntity);

                return query.UniqueResult<Vagao>();
            }
        }

        /// <summary>
        /// Obter Por Filtro
        /// </summary>
        /// <param name="pagination">Detalhes Paginacao</param>
        /// <param name="codigosDosVagoes">codigos Dos Vagoes</param>
        /// <returns>Resultado Paginado VagaoAcimaPesoVigente</returns>
        public ResultadoPaginado<Vagao> ObterPorFiltro(DetalhesPaginacao pagination, params object[] codigosDosVagoes)
        {
            try
            {
                DetachedCriteria criteria = CriarCriteria();

                criteria.AddOrder(new Order("Codigo", true));

                if (codigosDosVagoes != null && codigosDosVagoes.Length > 0)
                {
                    criteria.Add(Restrictions.In("Codigo", codigosDosVagoes));
                }

                var retorno = ObterPaginado(criteria, pagination);

                return retorno;
            }
            catch(Exception e)
            {
                Console.Write(e);
                return null;
            }
        }
    }
}
