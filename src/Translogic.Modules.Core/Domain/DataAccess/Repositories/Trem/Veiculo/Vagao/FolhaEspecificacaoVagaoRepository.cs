﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Trem.Veiculo.Vagao
{
	using ALL.Core.AcessoDados;
	using Model.Trem.Veiculo.Vagao;
	using Model.Trem.Veiculo.Vagao.Repositories;

	/// <summary>
	/// Classe de repositório da classe <see cref="FolhaEspecificacaoVagao"/>
	/// </summary>
	public class FolhaEspecificacaoVagaoRepository : NHRepository<FolhaEspecificacaoVagao, int>, IFolhaEspecificacaoVagaoRepository 
	{
	}
}