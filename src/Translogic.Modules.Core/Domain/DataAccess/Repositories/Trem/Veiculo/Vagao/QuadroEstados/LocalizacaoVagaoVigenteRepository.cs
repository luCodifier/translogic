namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Trem.Veiculo.Vagao.QuadroEstados
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using ALL.Core.AcessoDados;
    using Model.Trem.Veiculo.Vagao;
    using NHibernate;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao.QuadroEstado;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao.QuadroEstado.Repositories;

    /// <summary>
    /// Reposit�rio para <see cref="LocalizacaoVagaoVigente"/>
    /// </summary>
    public class LocalizacaoVagaoVigenteRepository : NHRepository<LocalizacaoVagaoVigente, int>, ILocalizacaoVagaoVigenteRepository
    {
        /// <summary>
        /// Obt�m os vag�es por localiza��o
        /// </summary>
        /// <param name="codigoVagao">Codigo do Vag�o</param>
        /// <returns> Lista de LocalizacaoVagaoVigente </returns>
        public IList<LocalizacaoVagaoVigente> ObterPorCodigoVagao(string codigoVagao)
        {
            codigoVagao = string.Concat(codigoVagao, "%");
            StringBuilder sb = new StringBuilder();
            sb.Append(@"FROM LocalizacaoVagaoVigente lvv
                            INNER JOIN FETCH lvv.Vagao vg
                            INNER JOIN FETCH vg.Serie sv
						  WHERE 
                            lvv.Localizacao.Codigo='LIN'
                            AND vg.Codigo LIKE :codigoVagao
                            AND NOT EXISTS(SELECT vpv.Id FROM VagaoPatioVigente vpv WHERE vpv.Vagao.Id=lvv.Vagao.Id)
                            AND NOT EXISTS(SELECT cvv.Id FROM ComposicaoVagaoVigente cvv WHERE cvv.Vagao.Id=lvv.Vagao.Id)
                            AND EXISTS(SELECT ivv.Id FROM IntercambioVagaoVigente ivv WHERE ivv.Vagao.Id=lvv.Vagao.Id)
                        ");

            ISession session = OpenSession();
            IQuery query = session.CreateQuery(sb.ToString());
            query.SetString("codigoVagao", codigoVagao);
            // query.SetResultTransformer(Transformers.DistinctRootEntity);

            return query.List<LocalizacaoVagaoVigente>();
        }

        /// <summary>
        /// Obtem a localiza��o do vagao para saber se est� em intercambio
        /// </summary>
        /// <param name="vagao">Objeto Vagao</param>
        /// <returns>Objeto LocalizacaoVagaoVigente</returns>
        public LocalizacaoVagaoVigente ObterEmOutraFerroviaPorVagao(Vagao vagao)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"FROM LocalizacaoVagaoVigente lvv
                            INNER JOIN FETCH lvv.Vagao vg
						  WHERE 
                            lvv.Localizacao.Codigo='LIN'
                            AND vg.Id LIKE :idVagao
                            AND NOT EXISTS(SELECT vpv.Id FROM VagaoPatioVigente vpv WHERE vpv.Vagao.Id=lvv.Vagao.Id)
                            AND NOT EXISTS(SELECT cvv.Id FROM ComposicaoVagaoVigente cvv WHERE cvv.Vagao.Id=lvv.Vagao.Id)
                            AND EXISTS(SELECT ivv.Id FROM IntercambioVagaoVigente ivv WHERE ivv.Vagao.Id=lvv.Vagao.Id)
                        ");

            ISession session = OpenSession();
            IQuery query = session.CreateQuery(sb.ToString());
            query.SetInt32("idVagao", vagao.Id.Value);
            // query.SetResultTransformer(Transformers.DistinctRootEntity);

            IList<LocalizacaoVagaoVigente> ret = query.List<LocalizacaoVagaoVigente>();
            return ret.Count > 0 ? ret[0] : null;
        }
    }
}