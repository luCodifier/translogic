﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Trem.Veiculo.Vagao
{
    using ALL.Core.AcessoDados;
    using Model.Trem.Veiculo.Vagao;
    using Model.Trem.Veiculo.Vagao.Repositories;

    public class VagaoLaudoRepository : NHRepository<VagaoLaudo, int?>, IVagaoLaudoRepository
    {
    }
}