﻿using System.Collections.Generic;

namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Trem.Veiculo.Vagao
{
    using System;
    using System.Linq;
    using ALL.Core.AcessoDados;
    using ALL.Core.AcessoDados.TiposCustomizados;
    using ALL.Core.Dominio;
    using NHibernate;
    using NHibernate.Criterion;
    using NHibernate.SqlCommand;
    using NHibernate.Transform;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao.Repositories;

    /// <summary>
    /// Repositorio para <see cref="VagaoTaraEdiDescargaRepository"/>
    /// </summary>
    public class VagaoTaraEdiDescargaRepository : NHRepository<VagaoTaraEdiDescarga, int?>, IVagaoTaraEdiDescargaRepository
    {
        /// <summary>
        /// Retorna o vagão por código
        /// </summary>
        /// <param name="codigo">codigo vagão</param>
        /// <returns>O Vagao objeto</returns>
        public IList<VagaoTaraEdiDescarga> ObterPorCodigo(string codigo)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.Eq("CodVagao", codigo));
            return ObterTodos(criteria);
        }

        /// <summary>
        /// Retorna o vagão por id
        /// </summary>
        /// <param name="idVagao">codigo vagão</param>
        /// <returns>O Vagao objeto</returns>
        public IList<VagaoTaraEdiDescarga> ObterPorVagaoId(int idVagao)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.Eq("Vagao.Id", idVagao));
            return ObterTodos(criteria);
        }

        /// <summary>
        /// Obter id dos vagoes com pesagem
        /// </summary>
        /// <returns>Retorna id dos vagoes com pesagem</returns>
        public IList<decimal> ObterVagoesComPesagens()
        {
            using (ISession session = OpenSession())
            {
                string sql =
                        @"SELECT 
                                  DISTINCT P.VG_ID_VG 
                            FROM 
                                  TARA_VAGAO_EDI_DESCARGA P,
                                  VAGAO V      
                            WHERE 
                                  P.VG_ID_VG = V.VG_ID_VG
                                  AND P.TV_NU_TARA_TL IS NOT NULL";

                IQuery query = session.CreateSQLQuery(sql);

                query.SetResultTransformer(Transformers.DistinctRootEntity);

                return query.List<decimal>();
            }
        }
    }
}
