﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Trem.Veiculo.Vagao
{
    using ALL.Core.AcessoDados;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao.Repositories;

    /// <summary>
    /// Repositorio para <see cref="VagaoTaraEdiDescargaRepository"/>
    /// </summary>
    public class VagaoLogTaraRepository : NHRepository<VagaoLogTara, int?>, IVagaoLogTaraRepository
    {
    
    }
}
