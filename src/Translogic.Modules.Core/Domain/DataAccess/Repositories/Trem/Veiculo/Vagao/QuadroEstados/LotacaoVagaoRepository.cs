namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Trem.Veiculo.Vagao.QuadroEstados
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using ALL.Core.AcessoDados;
    using Model.Trem.Veiculo.Vagao;
    using NHibernate;
    using NHibernate.Criterion;
    using NHibernate.Transform;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao.QuadroEstado;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao.QuadroEstado.Repositories;

    /// <summary>
    /// Reposit�rio para <see cref="LotacaoVagao"/>
    /// </summary>
    public class LotacaoVagaoRepository : NHRepository<LotacaoVagao, int>, ILotacaoVagaoRepository
    {
        /// <summary>
        /// Indica se o vag�o est� carregado
        /// </summary>
        /// <param name="vagao">Vagao a ser verificado</param>
        /// <param name="data">Data a ser verificada</param>
        /// <returns>Retorna true se o vag�o estiver carregado.</returns>
        public bool VerificarVagaoCarregado(Vagao vagao, DateTime data)
        {
            try
            {
                using (ISession session = OpenSession())
                {
                    string hql = @" select distinct l
                                      FROM LotacaoVagao l
						 			WHERE :data between l.DataInicio and nvl(l.DataTermino,sysdate)
									  AND l.Vagao.Id = :vagao";

                    IQuery query = session.CreateQuery(hql);
                    query.SetInt32("vagao", vagao.Id.Value);
                    query.SetDateTime("data", data);

                    query.SetResultTransformer(Transformers.DistinctRootEntity);

                    var retorno = query.List<LotacaoVagao>();

                    if (retorno.Count > 0)
                    {
                        return retorno[0].Lotacao.Codigo.ToUpperInvariant().Equals("CCR");
                    }

                    return false;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}