﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Trem.Veiculo.Vagao
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using ALL.Core.AcessoDados;
    using ALL.Core.Dominio;
    using NHibernate;
    using NHibernate.Transform;
    using Translogic.Modules.Core.Domain.Model.Acesso;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao.Repositories;

    /// <summary>
    /// Repositorio para <see cref="LimiteExcecaoVagao "/>
    /// </summary>
    public class LimiteExcecaoVagaoRepository : NHRepository<LimiteExcecaoVagao, int>, ILimiteExcecaoVagaoRepository
    {
        /// <summary>
        /// Obtém os vagões por filtro
        /// </summary>
        /// <param name="paginacao">Detalhes de paginação</param>
        /// <param name="codigoVagao">Vagão informado no filtro</param>
        /// <param name="mercadoria">Mercadoria informada no filtro</param>
        /// <returns>Lista de vagões de acordo com os filtros informados</returns>
        public ResultadoPaginado<LimiteExcecaoVagao> ObterPorFiltros(DetalhesPaginacao paginacao, string codigoVagao, string mercadoria)
        {
            var parameters = new List<Action<IQuery>>();

            var hqlQuery = new StringBuilder(
                @"SELECT 
                         {0}
                    FROM LimiteExcecaoVagao L
                   WHERE L.DataFinal IS NULL");

            if (!String.IsNullOrEmpty(codigoVagao) && codigoVagao.ToUpper().Trim() != "T")
            {
                hqlQuery.AppendLine(@" AND L.Vagao.Codigo = :codigoVagao");
                parameters.Add(q => q.SetParameter("codigoVagao", codigoVagao));
            }

            if (!string.IsNullOrWhiteSpace(mercadoria))
            {
                hqlQuery.AppendLine(@" AND L.LimiteMinimoBase.Mercadoria.Codigo = :mercadoria");
                parameters.Add(q => q.SetParameter("mercadoria", mercadoria));
            }

            using (var session = OpenSession())
            {
                var query = session.CreateQuery(string.Format(hqlQuery.ToString(), "L"));
                var queryCount = session.CreateQuery(string.Format(hqlQuery.ToString(), "COUNT (L.Id) "));

                foreach (var p in parameters)
                {
                    p.Invoke(query);
                    p.Invoke(queryCount);
                }

                query.SetResultTransformer(Transformers.DistinctRootEntity);

                if (paginacao != null)
                {
                    if (paginacao.Inicio.HasValue)
                    {
                        query.SetFirstResult(paginacao.Inicio.Value);
                    }

                    int limit = GetLimit(paginacao);

                    query.SetMaxResults(limit > 0 ? limit : DetalhesPaginacao.LimitePadrao);
                }

                var total = queryCount.UniqueResult<long>();
                var itens = query.List<LimiteExcecaoVagao>();

                var result = new ResultadoPaginado<LimiteExcecaoVagao>
                {
                    Items = itens,
                    Total = total
                };

                return result;
            }
        }

        /// <summary>
        /// Obtém o limite mínimo exceção para o vagão e mercadoria de acordo com os filtros
        /// </summary>
        /// <param name="inicialVagao">Vagão para verificar o limite</param>
        /// <param name="mercadoria">Mercadoria para o vagão informado</param>
        /// <param name="pesoMinimo">Limite para o vagão com a mercadoria informada</param>
        /// <returns>Limite para o vagão de acordo com os filtros</returns>
        public LimiteExcecaoVagao ObterPorVagaoMercadoriaPesoMinimo(string inicialVagao, string mercadoria, decimal pesoMinimo)
        {
            var parameters = new List<Action<IQuery>>();

            var hqlQuery = @"SELECT L 
                               FROM LimiteExecaoVagao L 
                              WHERE L.LimiteMinimoBase.InicialVagao = :codigoVagao
                                AND L.LimiteMinimoBase.CodigoMercadoria.Codigo = :mercadoria
                                AND L.PesoMinimo = :pesoMinimo";

            parameters.Add(q => q.SetParameter("codigoVagao", inicialVagao));
            parameters.Add(q => q.SetParameter("mercadoria", mercadoria));
            parameters.Add(q => q.SetParameter("pesoMinimo", pesoMinimo));

            using (var session = OpenSession())
            {
                var query = session.CreateQuery(hqlQuery.ToString());

                foreach (var p in parameters)
                {
                    p.Invoke(query);
                }

                query.SetResultTransformer(Transformers.DistinctRootEntity);

                return query.UniqueResult<LimiteExcecaoVagao>();
            }
        }

        /// <summary>
        /// Obtém um limite de exceção pelo limite mínimo base
        /// </summary>
        /// <param name="limiteMinimoBaseId">Id do limite mínimo base</param>
        /// <returns>Limite para o vagão de acordo com o LimiteMinimoBase informado</returns>
        public LimiteExcecaoVagao ObterLimiteExcecaoVagaoPorLimiteMinimoBaseId(int limiteMinimoBaseId)
        {
            var hqlQuery =
                           @"SELECT L 
                               FROM LimiteExcecaoVagao L 
                              WHERE L.LimiteMinimoBase.Id = :limiteMinimoBaseId
                                AND L.DataFinal IS NULL";

            using (var session = OpenSession())
            {
                var query = session.CreateQuery(hqlQuery.ToString());

                query.SetInt32("limiteMinimoBaseId", limiteMinimoBaseId);

                query.SetResultTransformer(Transformers.DistinctRootEntity);

                return query.UniqueResult<LimiteExcecaoVagao>();
            }
        }

        /// <summary>
        /// Salva um limite mínimo para o vagão e mercadoria
        /// </summary>
        /// <param name="limite">limite de peso para esta mercadoria neste vagão</param>
        /// <param name="vagao">vagão que receberá o limite</param>
        /// <param name="pesoMinimo">o peso mínimo que o vagão aceita</param>
        /// <param name="usuario">Usuário que está realizando a operação</param>
        public void SalvarLimiteMinimoVagao(LimiteMinimoBase limite, Vagao vagao, string pesoMinimo, Usuario usuario)
        {
            var limiteNovo = new LimiteExcecaoVagao()
                                 {
                                     DataInicial = DateTime.Now,
                                     UsuarioCadastro = usuario,
                                     PesoMinimo = Convert.ToDecimal(pesoMinimo.Replace(".", ",")),
                                     Vagao = vagao,
                                     LimiteMinimoBase = limite,
                                     DataFinal = null
                                 };
            Inserir(limiteNovo);
        }

        /// <summary>
        /// Desabilita o limite exceção vagão setando a data final
        /// </summary>
        /// <param name="idLimiteExcecaoVagao">Id do limite para desabilitar</param>
        public void RemoverLimiteMinimoVagao(int idLimiteExcecaoVagao)
        {
            var limite = ObterPorId(idLimiteExcecaoVagao);

            if (limite != null)
            {
                var hqlQuery =
                          @"UPDATE LimiteExcecaoVagao L
                               SET L.DataFinal = :dataFinal
                             WHERE L.Id = :idLimiteExcecaoVagao
                               AND L.DataFinal IS NULL";

                using (var session = OpenSession())
                {
                    var query = session.CreateQuery(hqlQuery.ToString());

                    query.SetInt32("idLimiteExcecaoVagao", idLimiteExcecaoVagao);
                    query.SetParameter("dataFinal", DateTime.Now);
                    query.ExecuteUpdate();
                }

                // limite.DataFinal = DateTime.Now;
                // InserirOuAtualizar(limite);
            }
            else
            {
                throw new Exception("Não foi possível localizar o limite selecionado para remover");
            }
        }
    }
}
