﻿using System.Collections.Generic;

namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Trem.Veiculo.Vagao
{
    using System;
    using ALL.Core.AcessoDados;
    using ALL.Core.Dominio;
    using NHibernate.Criterion;
    using NHibernate.SqlCommand;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao.Repositories;

    /// <summary>
    /// Repositorio para <see cref="VagoesTravadosAnxRepository"/>
    /// </summary>
    public class VagoesTravadosAnxRepository : NHRepository<VagoesTravadosAnx, int?>, IVagoesTravadosAnxRepository
    {
        /// <summary>
        /// Obter Por Filtro
        /// </summary>
        /// <param name="pagination">Detalhes Paginacao</param>
        /// <param name="dataInicial">data Inicial</param>
        /// <param name="dataFinal">data Final</param>
        /// <param name="local">linha De Origem</param>
        /// <param name="prefixo">prefixo do Trem</param>
        /// <param name="codigosDosVagoes">codigos Dos Vagoes</param>
        /// <returns>Resultado Paginado VagaoAcimaPesoVigente</returns>
        public ResultadoPaginado<VagoesTravadosAnx> ObterPorFiltro(DetalhesPaginacao pagination, DateTime? dataInicial, DateTime? dataFinal, string local, string prefixo, params object[] codigosDosVagoes)
        {
            DetachedCriteria criteria = CriarCriteria();

            criteria.CreateAlias("Vagao", "V", JoinType.InnerJoin);
            criteria.CreateAlias("Local", "AO", JoinType.InnerJoin);

            criteria.AddOrder(new Order("V.Codigo", true));

            criteria.Add(Restrictions.IsNull("DataLiberacao"));

            if (dataInicial.HasValue)
            {
                dataInicial = dataInicial.Value.Date;
                criteria.Add(Restrictions.Ge("DataTrava", dataInicial));
            }

            if (dataFinal.HasValue)
            {
                dataFinal = dataFinal.Value.Date.AddDays(1).AddSeconds(-1);
                criteria.Add(Restrictions.Le("DataTrava", dataFinal));
            }

            if (!string.IsNullOrWhiteSpace(local))
            {
                criteria.Add(Restrictions.Eq("AO.Codigo", local));
            }

            //if (!string.IsNullOrWhiteSpace(linhaDeDestino))
            //{
            //    criteria.Add(Restrictions.Eq("AO.Codigo", linhaDeDestino));
            //}

            if(!string.IsNullOrEmpty(prefixo))
            {
                criteria.Add(Restrictions.Eq("Prefixo", prefixo));
            }

            if (codigosDosVagoes != null && codigosDosVagoes.Length > 0)
            {
                 criteria.Add(Restrictions.In("V.Codigo", codigosDosVagoes));
            }
            
            return ObterPaginado(criteria, pagination);
        }
    }
}
