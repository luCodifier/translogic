namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Trem.Veiculo.Vagao
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using ALL.Core.AcessoDados;
    using ALL.Core.Dominio;
    using Model.Dto;
    using Model.Trem.Veiculo.Vagao;
    using Model.Trem.Veiculo.Vagao.Repositories;
    using NHibernate;
    using NHibernate.Criterion;
    using NHibernate.SqlCommand;
    using NHibernate.Transform;

    /// <summary>
    /// Reposit�rio para <see cref="VagaoPatioVigente"/>
    /// </summary>
    public class VagaoPatioVigenteRepository : NHRepository<VagaoPatioVigente, int?>, IVagaoPatioVigenteRepository
    {
    	/// <summary>
    	/// Obt�m a lista de vag�es patio por id de esta��o m�e
    	/// </summary>
    	/// <param name="idEstacaoMae"> Id da estacao mae. </param>
    	/// <param name="idFluxo">Id do fluxo comercial</param>
    	/// <param name="codigoVagao">C�digo do vag�o</param>
    	/// <returns> Lista de Vag�es patios vigentes </returns>
    	public IList<VagaoCarregamentoDto> ObterParaCarregamentoPorIdEstacaoMae(int idEstacaoMae, int idFluxo, string codigoVagao)
        {
            // IndCarregadoMesmoFluxo = false,
            StringBuilder sb = new StringBuilder();
            sb.Append(@"
                        SELECT 
                            vpv.Vagao.Id AS IdVagao,
                            vpv.Vagao.Codigo AS CodigoVagao,
                            vpv.ElementoVia.Codigo AS Linha,
                            vpv.NumeroSequenciaPatio AS Sequencia,
                            vpv.Vagao.Serie.Codigo AS SerieVagao,
                            (SELECT COUNT(vpedv.Id)
                                FROM VagaoPedidoVigente vpedv 
                                WHERE vpedv.Vagao.Id = vpv.Vagao.Id
								AND UPPER(vpedv.FluxoComercial.Contrato.EmpresaPagadora.DescricaoResumida) LIKE 'BRADO%') AS QuantidadeCarregadoFluxoBrado,
                            (SELECT COUNT(vpedv.Id)
                                FROM VagaoPedidoVigente vpedv 
                                WHERE vpedv.Vagao.Id = vpv.Vagao.Id) AS QuantidadeCarregado,
							(SELECT COUNT(vpedv.Id)
                                FROM VagaoPedidoVigente vpedv 
                                WHERE vpedv.Vagao.Id = vpv.Vagao.Id
                                AND vpedv.FluxoComercial.Id = :idFluxoComercial) AS QuantidadeCarregadoMesmoFluxo
                        FROM VagaoPatioVigente vpv
                            INNER JOIN vpv.Vagao vg
                            INNER JOIN vg.Serie sv
						  WHERE
                            EXISTS (SELECT svv.Id 
                                FROM
                                 SituacaoVagaoVigente svv
                                  INNER JOIN svv.Situacao st  
                                WHERE (st.Codigo='SEC' OR st.Codigo='SDC')
                                AND vpv.Vagao.Id = svv.Vagao.Id)
                            AND vpv.Patio.EstacaoMae.Id = :idEstacaoMae {0}");
    		string hql = sb.ToString();
    		string complQuery = string.Empty;
			if (!string.IsNullOrEmpty(codigoVagao))
			{
				complQuery = @"AND vpv.Vagao.Codigo LIKE :codigoVagao";
			}

    		hql = string.Format(hql, complQuery);

            ISession session = OpenSession();
			IQuery query = session.CreateQuery(hql);
            query.SetInt32("idEstacaoMae", idEstacaoMae);
            query.SetInt32("idFluxoComercial", idFluxo);
			if (!string.IsNullOrEmpty(codigoVagao))
			{
				query.SetString("codigoVagao", codigoVagao + "%");
			}

            query.SetResultTransformer(Transformers.AliasToBean(typeof(VagaoCarregamentoDto)));

            return query.List<VagaoCarregamentoDto>();
        }

        /// <summary>
        /// Obt�m o p�tio vigente em que o vag�o est�.
        /// </summary>
        /// <param name="vagao">Objeto vag�o</param>
        /// <returns>Objeto VagaoPatioVigente</returns>
        public VagaoPatioVigente ObterPorVagao(Vagao vagao)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.CreateAlias("Patio", "ao", JoinType.InnerJoin);
            criteria.CreateAlias("Vagao", "vg", JoinType.InnerJoin);
            criteria.Add(Restrictions.Eq("vg.Id", vagao.Id));
            criteria.SetFetchMode("ao", FetchMode.Eager);
            criteria.SetFetchMode("vg", FetchMode.Lazy);
            return ObterPrimeiro(criteria);
        }


        /// <summary>
        /// Obter Por Filtro
        /// </summary>
        /// <param name="pagination">Detalhes Paginacao</param>
        /// <param name="local">linha De Origem</param>
        /// <param name="codigosDosVagoes">codigos Dos Vagoes</param>
        /// <returns>Resultado Paginado VagaoAcimaPesoVigente</returns>
        public ResultadoPaginado<VagaoPatioVigente> ObterPorFiltro(DetalhesPaginacao pagination, string local, params object[] codigosDosVagoes)
        {
            try
            {
                DetachedCriteria criteria = CriarCriteria();
                criteria.CreateAlias("Vagao", "vg", JoinType.InnerJoin);
                criteria.CreateAlias("Patio", "ao", JoinType.InnerJoin);

                criteria.SetFetchMode("ao", FetchMode.Eager);
                criteria.SetFetchMode("vg", FetchMode.Lazy);

                criteria.CreateAlias("Patio.EstacaoMae", "aom", JoinType.InnerJoin);
                criteria.SetFetchMode("aom", FetchMode.Eager);

                if (codigosDosVagoes != null && codigosDosVagoes.Length > 0)
                {
                    criteria.Add(Restrictions.In("vg.Codigo", codigosDosVagoes));
                }

                if (!string.IsNullOrEmpty(local))
                {
                    criteria.Add(Restrictions.Eq("aom.Codigo", local));
                }

                var retorno = ObterPaginado(criteria, pagination);

                return retorno;
            }
            catch (Exception e)
            {
                Console.Write(e);
                return null;
            }
        }
    }
}