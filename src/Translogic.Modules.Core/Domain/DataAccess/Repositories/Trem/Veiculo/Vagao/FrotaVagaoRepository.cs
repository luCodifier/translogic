﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Trem.Veiculo.Vagao
{
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;
    using NHibernate;
    using NHibernate.Transform;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao.Repositories;

    /// <summary>
    /// Repositório de <see cref="FrotaVagao"/>
    /// </summary>
    public class FrotaVagaoRepository : NHRepository<FrotaVagao, int>, IFrotaVagaoRepository
    {
        /// <summary>
        /// Retorna as frotas por filtro de código
        /// </summary>
        /// <param name="frota">Código da frota</param>
        /// <returns>Lista de frotas</returns>
        public IList<FrotaVagao> ObterFrotas(string frota)
        {
            using (ISession session = OpenSession())
            {
                string hql = @"FROM FrotaVagao fv
                  WHERE fv.Codigo like :frota 
                  ORDER BY fv.Codigo DESC
                ";

                IQuery query = session.CreateQuery(hql);
                query.SetString("frota", string.Format("%{0}%", frota));
                
                query.SetResultTransformer(Transformers.DistinctRootEntity);
                
                return query.List<FrotaVagao>();
            }
        }
    }
}