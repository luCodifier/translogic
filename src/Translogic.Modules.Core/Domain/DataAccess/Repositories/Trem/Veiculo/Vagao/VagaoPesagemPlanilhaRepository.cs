﻿using System;
using System.Collections.Generic;
using ALL.Core.AcessoDados.TiposCustomizados;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.SqlCommand;
using NHibernate.Transform;

namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Trem.Veiculo.Vagao
{
    using ALL.Core.AcessoDados;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao.Repositories;
    using ALL.Core.Dominio;
    using System.Text;

    /// <summary>
    /// Repositorio para <see cref="VagaoTaraEdiDescargaRepository"/>
    /// </summary>
    public class VagaoPesagemPlanilhaRepository : NHRepository<VagaoPesagemPlanilha, decimal?>, IVagaoPesagemPlanilhaRepository
    {
        /// <summary>
        /// Retorna o vagão por código
        /// </summary>
        /// <param name="codigo">codigo vagão</param>
        /// <returns>O Vagao objeto</returns>
        public IList<VagaoPesagemPlanilha> ObterPorCodigo(string codigo)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.Eq("CodVagao", codigo));
            criteria.Add(Restrictions.Eq("IndProcessadoComErro", false));
            return ObterTodos(criteria);
        }
    }
}
