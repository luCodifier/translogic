﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Trem.Veiculo.Vagao
{
    using System;
    using System.Text;
    using ALL.Core.AcessoDados;
    using ALL.Core.Dominio;
    using NHibernate.Transform;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao.Repositories;
    
    /// <summary>
    /// Repositorio para <see cref="LimiteExcecaoFrota "/>
    /// </summary>
    public class LimiteExcecaoFrotaRepository : NHRepository<LimiteExcecaoFrota, int>, ILimiteExcecaoFrotaRepository
    {
        /// <summary>
        /// Obtém os limites de acordo com a frota informada
        /// </summary>
        /// <param name="pagination">Detalhes de paginação</param>
        /// <param name="frota">Frota para se obter os limites</param>
        /// <returns>Limites paginados de acordo com o filtro</returns>
        public ResultadoPaginado<LimiteExcecaoFrota> ObterPorFiltroFrota(DetalhesPaginacao pagination, string frota)
        {
            var hqlQuery = new StringBuilder(@"SELECT {0} FROM LimiteExcecaoFrota L WHERE L.DataFinal IS NULL");

            using (var session = OpenSession())
            {
                if (!String.IsNullOrEmpty(frota) && frota.ToUpper().Trim() != "T")
                {
                    hqlQuery.AppendLine(" AND L.Frota.Codigo = :frota");
                }

                var query = session.CreateQuery(string.Format(hqlQuery.ToString(), "L"));
                var queryCount = session.CreateQuery(string.Format(hqlQuery.ToString(), "COUNT(L.Id)"));

                if (!String.IsNullOrEmpty(frota) && frota.ToUpper().Trim() != "T")
                {
                    query.SetString("frota", frota);
                    queryCount.SetString("frota", frota);
                }

                if (pagination != null)
                {
                    if (pagination.Inicio.HasValue)
                    {
                        query.SetFirstResult(pagination.Inicio.Value);
                    }

                    int limit = GetLimit(pagination);

                    query.SetMaxResults(limit > 0 ? limit : DetalhesPaginacao.LimitePadrao);
                }

                query.SetResultTransformer(Transformers.DistinctRootEntity);

                var items = query.List<LimiteExcecaoFrota>();
                var total = queryCount.UniqueResult<long>();

                return new ResultadoPaginado<LimiteExcecaoFrota>()
                           {
                               Items = items,
                               Total = total
                           };
            }
        }

        /// <summary>
        /// Obtém o limite máximo para a frota
        /// </summary>
        /// <param name="idFrota">Id da Frota para se obter o limite</param>
        /// <returns>Limite máximo de peso para a frota</returns>
        public LimiteExcecaoFrota ObterPorFrotaPesoMaximo(int idFrota)
        {
            var hqlQuery = @"FROM LimiteExcecaoFrota L 
                            WHERE L.Frota.Id = :idFrota 
                              AND L.DataFinal IS NULL";

            using (var session = OpenSession())
            {
                var query = session.CreateQuery(hqlQuery);
                query.SetParameter("idFrota", idFrota);

                query.SetResultTransformer(Transformers.DistinctRootEntity);

                return query.UniqueResult<LimiteExcecaoFrota>();
            }
        }

        /// <summary>
        /// Salva um limite máximo para uma frota
        /// </summary>
        /// <param name="frota">Frota para gravação de limite máximo</param>
        /// <param name="pesoMaximo">Peso máximo para o limite por frota</param>
        /// <param name="usuario">Usuário que está realizando a operação</param>
        public void SalvarLimiteMinimoVagao(FrotaVagao frota, string pesoMaximo, Model.Acesso.Usuario usuario)
        {
            var limiteNovo = new LimiteExcecaoFrota()
            {
                DataInicial = DateTime.Now,
                UsuarioCadastro = usuario,
                DataFinal = null,
                Frota = frota,
                PesoMaximo = Convert.ToDecimal(pesoMaximo.Replace(".", ","))
            };
            Inserir(limiteNovo);
        }

        /// <summary>
        /// Desabilita um limite para a frota setando a data final
        /// </summary>
        /// <param name="limiteExcecaoFrotaId">Id do limite para desabilitar</param>
        public void RemoverLimiteMaximo(int limiteExcecaoFrotaId)
        {
            var limite = ObterPorId(limiteExcecaoFrotaId);

            if (limite != null)
            {
                var hqlQuery =
                          @"UPDATE LimiteExcecaoFrota L
                               SET L.DataFinal = :dataFinal
                             WHERE L.Id = :limiteExcecaoFrotaId
                               AND L.DataFinal IS NULL";

                using (var session = OpenSession())
                {
                    var query = session.CreateQuery(hqlQuery.ToString());

                    query.SetInt32("limiteExcecaoFrotaId", limiteExcecaoFrotaId);
                    query.SetParameter("dataFinal", DateTime.Now);
                    query.ExecuteUpdate();
                }

                // limite.DataFinal = DateTime.Now;
                // InserirOuAtualizar(limite);
            }
            else
            {
                throw new Exception("Não foi possível localizar o limite informado para exclusão");
            }
        }
    }
}
