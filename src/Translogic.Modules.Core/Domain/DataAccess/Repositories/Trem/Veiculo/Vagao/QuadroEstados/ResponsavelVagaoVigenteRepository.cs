namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Trem.Veiculo.Vagao.QuadroEstados
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using ALL.Core.AcessoDados;
    using NHibernate;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao.QuadroEstado;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao.QuadroEstado.Repositories;

    /// <summary>
    /// Repositório para <see cref="ResponsavelVagaoVigente"/>
    /// </summary>
    public class ResponsavelVagaoVigenteRepository : NHRepository<ResponsavelVagaoVigente, int>, IResponsavelVagaoVigenteRepository
    {
    }
}