﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Trem.Veiculo.Vagao
{
    using System;
    using ALL.Core.AcessoDados;
    using ALL.Core.Dominio;
    using NHibernate.Criterion;
    using NHibernate.SqlCommand;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao.Repositories;

    /// <summary>
    /// Repositorio para <see cref="VagaoLimitePesoVigenteRepository"/>
    /// </summary>
    public class VagaoLimitePesoVigenteRepository : NHRepository<VagaoLimitePesoVigente, int?>, IVagaoLimitePesoVigenteRepository
    {
        /// <summary>
        /// Obter Por Filtro
        /// </summary>
        /// <param name="pagination">Detalhes Paginacao</param>
        /// <param name="dataInicial">data Inicial</param>
        /// <param name="dataFinal">data Final</param>
        /// <param name="linhaDeOrigem">linha De Origem</param>
        /// <param name="linhaDeDestino">linha De Destino</param>
        /// <param name="codigosDosVagoes">codigos Dos Vagoes</param>
        /// <returns>Resultado Paginado VagaoAcimaPesoVigente</returns>
        public ResultadoPaginado<VagaoLimitePesoVigente> ObterPorFiltro(DetalhesPaginacao pagination, DateTime? dataInicial, DateTime? dataFinal, string linhaDeOrigem, string linhaDeDestino, params string[] codigosDosVagoes)
        {
            DetachedCriteria criteria = CriarCriteria();

            criteria.CreateAlias("Vagao", "V", JoinType.InnerJoin);
            criteria.CreateAlias("FluxoComercial", "FC", JoinType.InnerJoin);
            criteria.CreateAlias("FC.Origem", "O", JoinType.InnerJoin);
            criteria.CreateAlias("FC.Destino", "D", JoinType.InnerJoin);

            criteria.AddOrder(new Order("V.Codigo", true));

            criteria.Add(Restrictions.IsNull("DataLiberacao"));

            if (dataInicial.HasValue)
            {
                dataInicial = dataInicial.Value.Date;
                criteria.Add(Restrictions.Ge("VersionDate", dataInicial));
            }

            if (dataFinal.HasValue)
            {
                dataFinal = dataFinal.Value.Date.AddDays(1).AddSeconds(-1);
                criteria.Add(Restrictions.Le("VersionDate", dataFinal));
            }

            if (!string.IsNullOrWhiteSpace(linhaDeOrigem))
            {
                criteria.Add(Restrictions.Eq("O.Codigo", linhaDeOrigem));
            }

            if (!string.IsNullOrWhiteSpace(linhaDeDestino))
            {
                criteria.Add(Restrictions.Eq("D.Codigo", linhaDeDestino));
            }

            if (codigosDosVagoes != null && codigosDosVagoes.Length > 0)
            {
                 criteria.Add(Restrictions.In("V.Codigo", codigosDosVagoes));
            }

            return ObterPaginado(criteria, pagination);
        }
    }
}
