﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Trem.Veiculo.Vagao
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    using ALL.Core.AcessoDados;

    using NHibernate;
    using NHibernate.Transform;

    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao.Repositories;

    /// <summary>
    ///     Repositorio para <see cref="Vagao" />
    /// </summary>
    public class VagaoLimitePesoRepository : NHRepository<VagaoLimitePeso, int?>, IVagaoLimitePesoRepository
    {
        #region Public Methods and Operators

        /// <summary>
        ///     Obter média do peso informado para vagão travado por peso mínimo
        /// </summary>
        /// <param name="idVagao">Id do vagão</param>
        public double? ObterMediaPesoInformadoVagaoTravadoPorPesoMinimo(int idVagao)
        {
            double? result = null;
            var parametros = new List<Action<IQuery>>();

            var sb = new StringBuilder();
            sb.Append(@"SELECT round(AVG(vlp.PesoInformado), 2)
                        FROM VagaoLimitePeso vlp, Vagao vg
                         WHERE vlp.LimitadoPor = 'A'
                         AND   vlp.Vagao = vg.Id
                         AND vg.Id = :IdVagao
                         GROUP BY vg.Id");

            parametros.Add(q => q.SetInt32("IdVagao", idVagao));

            using (ISession session = this.OpenSession())
            {
                IQuery query = session.CreateQuery(sb.ToString());
                query.SetResultTransformer(Transformers.DistinctRootEntity);

                foreach (var action in parametros)
                {
                    action.Invoke(query);
                }

                result = query.UniqueResult<double>();
            }

            return result;
        }

        /// <summary>
        ///     Retorna os vagões limite peso associados ao id do vagão
        /// </summary>
        /// <param name="id">código do vagão</param>
        /// <returns>Lista de vagões limite peso associados ao id do vagão</returns>
        public IList<VagaoLimitePeso> ObterPorVagao(int id)
        {
            using (ISession session = this.OpenSession())
            {
                string hql = @"FROM VagaoLimitePeso vlp
                  WHERE vlp.Vagao = :vagao
                ";

                IQuery query = session.CreateQuery(hql);
                query.SetInt32("vagao", id);

                query.SetResultTransformer(Transformers.DistinctRootEntity);

                return query.List<VagaoLimitePeso>();
            }
        }

        #endregion
    }
}