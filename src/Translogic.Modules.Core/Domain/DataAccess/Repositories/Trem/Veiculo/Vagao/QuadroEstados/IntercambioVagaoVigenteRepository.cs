namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Trem.Veiculo.Vagao.QuadroEstados
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using ALL.Core.AcessoDados;
    using Model.Trem.Veiculo.Vagao;
    using NHibernate;
    using NHibernate.Criterion;
    using NHibernate.SqlCommand;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao.QuadroEstado;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao.QuadroEstado.Repositories;

    /// <summary>
    /// Reposit�rio para <see cref="LocalizacaoVagao"/>
    /// </summary>
    public class IntercambioVagaoVigenteRepository : NHRepository<IntercambioVagaoVigente, int>, IIntercambioVagaoVigenteRepository
    {
        /// <summary>
        /// Obt�m o Intercambio vigente do vag�o pelo c�digo do vagao
        /// </summary>
        /// <param name="vagao"> Objeto vagao. </param>
        /// <returns> Objeto IntercambioVagaoVigente </returns>
        public IntercambioVagaoVigente ObterPorVagao(Vagao vagao)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.CreateAlias("Vagao", "vg", JoinType.InnerJoin);
            criteria.Add(Restrictions.Eq("vg.Id", vagao.Id));
            criteria.SetFetchMode("vg", FetchMode.Lazy);
            
            return ObterPrimeiro(criteria);
        }
    }
}