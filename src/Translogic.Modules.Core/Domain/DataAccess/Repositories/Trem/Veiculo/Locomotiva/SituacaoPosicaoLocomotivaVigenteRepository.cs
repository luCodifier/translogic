namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Trem.Veiculo.Locomotiva
{
	using ALL.Core.AcessoDados;
	using NHibernate.Criterion;
	using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Locomotiva;
	using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Locomotiva.Repositories;

	/// <summary>
	/// Reposit�rio para <see cref="SituacaoPosicaoLocomotivaVigente"/>
	/// </summary>
	public class SituacaoPosicaoLocomotivaVigenteRepository : NHRepository<SituacaoPosicaoLocomotivaVigente, int?>, ISituacaoPosicaoLocomotivaVigenteRepository
	{
		/// <summary>
		/// Obt�m a situa��o de posicao da composicao da locomotiva
		/// </summary>
		/// <param name="composicaoLocomotiva"> Composicao da Locomotiva a ser buscada a situa��o. </param>
		/// <returns> Objeto da situa��o de posicao </returns>
		public SituacaoPosicaoLocomotivaVigente ObterPorComposicaoLocomotiva(ComposicaoLocomotiva composicaoLocomotiva)
		{
			DetachedCriteria criteria = CriarCriteria();
			criteria.Add(Restrictions.Eq("ComposicaoLocomotiva", composicaoLocomotiva));
			return this.ObterPrimeiro(criteria);
		}
	}
}