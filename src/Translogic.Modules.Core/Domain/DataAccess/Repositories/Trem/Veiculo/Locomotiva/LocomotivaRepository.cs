namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Trem.Veiculo.Locomotiva
{
	using System;
    using System.Collections.Generic;
	using ALL.Core.AcessoDados;
	using NHibernate.Criterion;
	using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Locomotiva;
	using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Locomotiva.Repositories;

	/// <summary>
    /// Reposit�rio para <see cref="Locomotiva"/>
    /// </summary>
    public class LocomotivaRepository : NHRepository<Locomotiva, int>, ILocomotivaRepository
    {
    	/// <summary>
    	/// Obt�m as locomotivas dad um c�digo
    	/// </summary>
    	/// <param name="codigo"> C�digo da locomotiva. </param>
    	/// <returns> Lista de Locomotivas </returns>
    	public IList<Locomotiva> ObterPorCodigo(string codigo)
    	{
    		DetachedCriteria criteria = CriarCriteria();
			criteria.Add(Restrictions.InsensitiveLike("Codigo", codigo, MatchMode.Start));
    		return ObterTodos(criteria);
    	}

		/// <summary>
		/// Obt�m as locomotivas
		/// </summary>
		/// <returns> Lista de Locomotivas </returns>
		public IList<Locomotiva> ObterLocomotivas()
		{
			DetachedCriteria criteria = CriarCriteria();
			criteria.SetProjection(Projections.Distinct(Projections.Property("Id")));  
		    return ObterTodos();
		}
    }
}