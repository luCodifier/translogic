namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Trem.Veiculo.Locomotiva
{
	using System;
	using System.Collections.Generic;
	using System.Text;
	using ALL.Core.AcessoDados;
	using ALL.Core.AcessoDados.TiposCustomizados;
	using NHibernate;
	using NHibernate.Criterion;
	using NHibernate.Transform;
	using Translogic.Modules.Core.Domain.DataAccess.CustomType;
	using Translogic.Modules.Core.Domain.Model.Diversos;
	using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Locomotiva;
	using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Locomotiva.Repositories;

	/// <summary>
	/// Reposit�rio para <see cref="SerieLocomotiva"/>
	/// </summary>
	public class SituacaoComandoLocomotivaRepository : NHRepository<SituacaoComandoLocomotiva, int?>, ISituacaoComandoLocomotivaRepository
	{
		/// <summary>
		/// Obt�m as situa��es de comando dada uma loco e data
		/// </summary>
		/// <param name="locomotiva"> Locomotiva a ser buscada a situa��o de comando. </param>
		/// <param name="data"> Data que a locomotiva estava no comando </param>
		/// <returns> Lista de Situa��es </returns>
		public IList<SituacaoComandoLocomotiva> ObterComandantePorLocomotivaData(Locomotiva locomotiva, DateTime data)
		{
			StringBuilder sb = new StringBuilder();

			sb.Append(
				@"FROM SituacaoComandoLocomotiva sl
							INNER JOIN FETCH sl.ComposicaoLocomotiva cl
							INNER JOIN FETCH cl.Locomotiva l
						  WHERE :data BETWEEN sl.DataInicio AND NVL(sl.DataFim, SYSDATE)
							AND	sl.TipoSituacaoComandoLocomotiva = :tipoSituacao
							AND cl.Locomotiva = :locomotiva");

			ISession session = OpenSession();
			IQuery query = session.CreateQuery(sb.ToString());
			query.SetDateTime("data", data);
			query.SetParameter("tipoSituacao", TipoSituacaoComandoLocomotivaEnum.Comandante, new TipoSituacaoComandoLocomotivaEnumCustomType());
			query.SetParameter("locomotiva", locomotiva);
			query.SetResultTransformer(Transformers.DistinctRootEntity);
			return query.List<SituacaoComandoLocomotiva>();
		}
	}
}