namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Trem.Veiculo.Locomotiva
{
	using System;

	using ALL.Core.AcessoDados;

	using NHibernate;
	using NHibernate.Criterion;
	using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Locomotiva;
	using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Locomotiva.Repositories;

	/// <summary>
	/// Reposit�rio para <see cref="SituacaoTracaoLocomotiva"/>
	/// </summary>
	public class SituacaoTracaoLocomotivaRepository : NHRepository<SituacaoTracaoLocomotiva, int?>, ISituacaoTracaoLocomotivaRepository
	{
		/// <summary>
		/// Obt�m a situa��o de tra��o da composicao da locomotiva
		/// </summary>
		/// <param name="composicaoLocomotiva"> Composicao da Locomotiva a ser buscada a situa��o. </param>
		/// <param name="dataReferencia">Data de referencia</param>
		/// <returns> Objeto da situa��o de tra��o </returns>
		public SituacaoTracaoLocomotiva ObterPorComposicaoLocomotiva(ComposicaoLocomotiva composicaoLocomotiva, DateTime dataReferencia)
		{
			var hql = @"FROM SituacaoTracaoLocomotiva stl 
							WHERE :dataReferencia BETWEEN stl.DataInicio and COALESCE(stl.DataFim, SYSDATE) 
								AND stl.ComposicaoLocomotiva.Id = :idComposicaoLocomotiva
							ORDER BY stl.DataInicio";
			ISession session = this.OpenSession();
			IQuery query = session.CreateQuery(hql);
			query.SetInt32("idComposicaoLocomotiva", composicaoLocomotiva.Id);
			query.SetDateTime("dataReferencia", dataReferencia);
			// query.SetMaxResults(1);
			var list = query.List<SituacaoTracaoLocomotiva>();
			return list.Count > 0 ? list[0] : null;
		}
	}
}