namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Trem.Veiculo.Locomotiva
{
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;
    using Model.Trem;
    using Model.Trem.Veiculo.Locomotiva;
    using Model.Trem.Veiculo.Locomotiva.Repositories;
    using NHibernate;
    using NHibernate.Criterion;

    /// <summary>
    /// Repositório para <see cref="ComposicaoLocomotiva"/>
    /// </summary>
    public class ComposicaoLocomotivaRepository : NHRepository<ComposicaoLocomotiva, int>, IComposicaoLocomotivaRepository
    {
        /// <summary>
        /// Retorna a ComposicaoLocomotiva a partir da composicao
        /// </summary>
        /// <param name="composicao">Composicao de referencia</param>
        /// <returns>A ComposicaoLocomotiva</returns>
        public IList<ComposicaoLocomotiva> ObterPorComposicao(Composicao composicao)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.Eq("Composicao", composicao));
            criteria.SetFetchMode("Locomotiva", FetchMode.Join);

            return ObterTodos(criteria);
        }
    }
}