namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Trem.Veiculo.Locomotiva
{
	using System;

	using ALL.Core.AcessoDados;

	using NHibernate;
	using NHibernate.Criterion;
	using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Locomotiva;
	using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Locomotiva.Repositories;

	/// <summary>
	/// Reposit�rio para <see cref="SituacaoPosicaoLocomotiva"/>
	/// </summary>
	public class SituacaoPosicaoLocomotivaRepository : NHRepository<SituacaoPosicaoLocomotiva, int?>, ISituacaoPosicaoLocomotivaRepository
	{
		/// <summary>
		/// Obt�m a situa��o de posicao da composicao da locomotiva
		/// </summary>
		/// <param name="composicaoLocomotiva"> Composicao da Locomotiva a ser buscada a situa��o. </param>
		/// <param name="dataReferencia">Data de referencia</param>
		/// <returns> Objeto da situa��o de posicao </returns>
		public SituacaoPosicaoLocomotiva ObterPorComposicaoLocomotiva(ComposicaoLocomotiva composicaoLocomotiva, DateTime dataReferencia)
		{
			var hql = @"FROM SituacaoPosicaoLocomotiva spl 
							WHERE :dataReferencia BETWEEN spl.DataInicio and COALESCE(spl.DataFim, SYSDATE) 
								AND spl.ComposicaoLocomotiva.Id = :idComposicaoLocomotiva
							ORDER BY spl.DataInicio";
			ISession session = this.OpenSession();
			IQuery query = session.CreateQuery(hql);
			query.SetInt32("idComposicaoLocomotiva", composicaoLocomotiva.Id);
			query.SetDateTime("dataReferencia", dataReferencia);
			var list = query.List<SituacaoPosicaoLocomotiva>();
			return list.Count > 0 ? list[0] : null;
		}
	}
}