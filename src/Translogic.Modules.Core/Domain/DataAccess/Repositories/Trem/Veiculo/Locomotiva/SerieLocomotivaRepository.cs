namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Trem.Veiculo.Locomotiva
{
    using ALL.Core.AcessoDados;
    using Model.Trem.Veiculo.Locomotiva;
    using Model.Trem.Veiculo.Locomotiva.Repositories;

    /// <summary>
    /// Repositório para <see cref="SerieLocomotiva"/>
    /// </summary>
    public class SerieLocomotivaRepository : NHRepository<SerieLocomotiva, int?>, ISerieLocomotivaRepository
    {
    }
}