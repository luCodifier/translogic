namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Trem.Veiculo.Locomotiva
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using ALL.Core.AcessoDados;
    using Model.Trem.Veiculo.Locomotiva;
    using Model.Trem.Veiculo.Locomotiva.Repositories;
    using NHibernate;
    using NHibernate.Criterion;
    using NHibernate.Mapping;

    /// <summary>
    /// Repositório para <see cref="Locomotiva"/>
    /// </summary>
    public class EventoLocomotivaRepository : NHRepository<EventoLocomotiva, int>, IEventoLocomotivaRepository
    {
        /// <summary>
        /// Obtem todos os eventos que ocorreram com essas locomotivas no intervalo
        /// </summary>
        /// <param name="ini">data de inicio</param>
        /// <param name="fim">data de fim</param>
        /// <param name="locos">locomotivas de interesse</param>
        /// <returns>Lista de Eventos</returns>
        public IList<EventoLocomotiva> ObterPorDataELocomotivas(DateTime ini, DateTime fim, IList<Locomotiva> locos)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.Between("DataOcorrencia", ini, fim));
            criteria.Add(Restrictions.In("Locomotiva", locos.ToArray()));
            criteria.SetFetchMode("Evento", FetchMode.Join);

            return ObterTodos(criteria);
        }
    }
}