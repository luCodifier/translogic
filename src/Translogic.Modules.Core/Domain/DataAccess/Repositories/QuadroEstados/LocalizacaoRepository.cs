﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.QuadroEstados
{
    using ALL.Core.AcessoDados;
    using Model.QuadroEstados.Dimensoes;
    using Model.QuadroEstados.Repositories;

    public class LocalizacaoRepository : NHRepository<Localizacao, int?>, ILocalizacaoRepository
    {
    }
}