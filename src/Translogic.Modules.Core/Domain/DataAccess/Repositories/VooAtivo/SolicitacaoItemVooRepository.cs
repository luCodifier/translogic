﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.VooAtivo
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using ALL.Core.AcessoDados;
    using NHibernate;
    using NHibernate.Transform;
    using NHibernate.Linq;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Modules.Core.Domain.Model.OSLimpezaVagao.Repositories;
    using Translogic.Modules.Core.Domain.Model.OSLimpezaVagao;
    using Translogic.Core.Infrastructure.Web;
    using ALL.Core.Dominio;
    using System.Data.OracleClient;
    using System.Data;
    using Translogic.Modules.Core.Domain.Model.VooAtivo;


    public class SolicitacaoItemVooRepository : NHRepository<SolicitacaoVooAtivoItem, int>, ISolicitacaoVooItemRepository 
    {

        public ResultadoPaginado<SolicitacaoVooAtivoItemDto> BuscarEotDisponiveisVoo(DetalhesPaginacaoWeb detalhesPaginacaoWeb, string[] ativos, string patio, string linha, string os)
        {
            var sql = new StringBuilder(@"SELECT ");

            return ExecuteQueries(detalhesPaginacaoWeb, sql);
        }

        public ResultadoPaginado<SolicitacaoVooAtivoItemDto> BuscarLocomotivasDisponiveisVoo(DetalhesPaginacaoWeb detalhesPaginacaoWeb, string[] ativos, string patio, string linha, string os)
        {
            var sql = new StringBuilder(@"SELECT ");

            return ExecuteQueries(detalhesPaginacaoWeb, sql);
        }

        public ResultadoPaginado<SolicitacaoVooAtivoItemDto> BuscarVagoesDisponiveisVoo(DetalhesPaginacaoWeb detalhesPaginacaoWeb, string[] ativos, string patio, string linha, string os)
        {
            var sql = new StringBuilder(@"SELECT ");

            return ExecuteQueries(detalhesPaginacaoWeb, sql);
        }

        public ResultadoPaginado<SolicitacaoVooAtivoItemDto> BuscarItemsAtivosVooAvaliarPorID(DetalhesPaginacaoWeb detalhesPaginacaoWeb, int? id)
        {
            var sql = new StringBuilder(@"SELECT 
                                                            B.STATUS_SOL_ITEM_VOO    as ""StatusItem"",
                                                               ATIVO            as ""Ativo"",
                                                            C.TIPO_ATIVO        as ""TipoAtivo"",
                                                               DT_EVENTO        as ""DataEvento"",
                                                               PATIO_ORIGEM     as ""PatioOrigem"",
                                                               PATIO_DESTINO    as ""Destino"",
                                                               CONDICOES_USO    as ""CondicaoUso"",
                                                               SITUACAO         as ""Situacao"",
                                                               LOCAL            as ""Local"",
                                                               RESPONSAVEL      as ""Responsavel"",
                                                               LOTACAO          as ""Lotacao"",
                                                               INTERCAMBIO      as ""Intercambio"",
                                                               ESTADO_FUTURO    as ""EstadoFuturo""
                                            FROM SOL_VOO_ATIVOS_ITEM A 
                                                INNER JOIN SOL_VOO_ATIVOS_STATUS_ITEM B ON A.ID_SOL_VOO_STATUS_ITEM = B.ID_STATUS_SOL_ITEM_VOO                        
                                                INNER JOIN SOL_TIPO_ATIVO C ON A.ID_TIPO_ATIVO = C.ID_TIPO_ATIVO"
                );

            #region Where

            var sqlWhere = new StringBuilder();

            //ADICIONA FILTRO POR ID
                sqlWhere.AppendFormat(" AND A.ID_SOL_VOO_ITEM = {0}", id);

            if (sqlWhere.Length > 0)
            {
                string aux = " WHERE " + sqlWhere.ToString().Substring(5);
                sql.Append(aux);
            }

            #endregion


            #region OrderBy

            // Order by (Recupera o indice e o direcionamento (ASC/DESC))
            sql.Append(" ORDER BY A.ID_SOL_VOO_ITEM DESC");

            #endregion

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());
                var queryCount = session.CreateSQLQuery(String.Format(@"SELECT COUNT(1) FROM ({0})", sql.ToString().Replace("{ORDENACAO}", String.Empty)));


                query.SetResultTransformer(Transformers.AliasToBean<SolicitacaoVooAtivoItemDto>());

                if (detalhesPaginacaoWeb.Limite != 0)
                {
                    query.SetFirstResult(detalhesPaginacaoWeb.Inicio ?? 0);
                    query.SetMaxResults(detalhesPaginacaoWeb.Limite ?? DetalhesPaginacao.LimitePadrao);
                }

                var total = queryCount.UniqueResult<decimal>();
                var itens = query.List<SolicitacaoVooAtivoItemDto>();

                var result = new ResultadoPaginado<SolicitacaoVooAtivoItemDto>
                {
                    Items = itens,
                    Total = (long)total
                };

                return result;
            }
        }

        /// <summary>
        /// Executar as queries das pesquisas
        /// </summary>
        /// <param name="detalhesPaginacaoWeb"></param>
        /// <param name="sql"></param>
        /// <returns></returns>
        private ResultadoPaginado<SolicitacaoVooAtivoItemDto> ExecuteQueries(DetalhesPaginacaoWeb detalhesPaginacaoWeb, StringBuilder sql)
        {
            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());
                var queryCount = session.CreateSQLQuery(String.Format(@"SELECT COUNT(1) FROM ({0})", sql.ToString().Replace("{ORDENACAO}", String.Empty)));


                query.SetResultTransformer(Transformers.AliasToBean<SolicitacaoVooAtivoItemDto>());

                if (detalhesPaginacaoWeb.Limite != 0)
                {
                    query.SetFirstResult(detalhesPaginacaoWeb.Inicio ?? 0);
                    query.SetMaxResults(detalhesPaginacaoWeb.Limite ?? DetalhesPaginacao.LimitePadrao);
                }

                var total = queryCount.UniqueResult<decimal>();
                var itens = query.List<SolicitacaoVooAtivoItemDto>();

                var result = new ResultadoPaginado<SolicitacaoVooAtivoItemDto>
                {
                    Items = itens,
                    Total = (long)total
                };

                return result;
            }
        }
    }
}