﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.VooAtivo
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using ALL.Core.AcessoDados;
    using NHibernate;
    using NHibernate.Transform;
    using NHibernate.Linq;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Modules.Core.Domain.Model.OSLimpezaVagao.Repositories;
    using Translogic.Modules.Core.Domain.Model.OSLimpezaVagao;
    using Translogic.Core.Infrastructure.Web;
    using ALL.Core.Dominio;
    using System.Data.OracleClient;
    using System.Data;
    using Translogic.Modules.Core.Domain.Model.VooAtivo;
    using Translogic.Modules.Core.Util;
    using System.Collections;
    using System.Data.SqlClient;


    public class SolicitacaoVooItemRepository : NHRepository<SolicitacaoVooAtivoItem, int>, ISolicitacaoVooItemRepository
    {
        #region Solicitação

        public ResultadoPaginado<SolicitacaoVooAtivoItemDto> BuscarEotDisponiveisVoo(DetalhesPaginacaoWeb detalhesPaginacaoWeb, string ativos, string patio, string os)
        {
            #region Query

            var sql = new StringBuilder(@"SELECT 
				  ID_EQUIP   AS ""IdAtivo"",
				  COD_EQUIP  AS ""Ativo"",
				  'EOT' as ""TipoAtivo"",  
				  NVL(DT_CHG,
					DT_UTL)  AS ""DataEvento"", 
				  COD_ESTACAO AS ""PatioOrigem"",
				  CASE IND_SIT
					WHEN 'A' THEN 'Avariado'
					WHEN 'D' THEN 'Disponivel'
					WHEN 'M' THEN 'Manutencao'
					WHEN 'I' THEN 'Inativo'
				  END AS ""Situacao"",
				  Null as ""CondicaoUso"",
				  CASE
					WHEN TX_TREM IS NULL
					THEN 'Em Pátio'
					ELSE 'Em Trem'
				  END         AS ""Local"",
				  NULL AS ""Responsavel"",
				  NULL AS ""Lotacao"",
				  NULL AS ""Intercambio""
				  ,(CASE WHEN (SELECT COUNT(1) FROM SOL_VOO_ATIVOS_ITEM VI WHERE VI.ID_ATIVO = EQP.ID_EQUIP AND VI.ID_TIPO_ATIVO = 1 AND VI.ID_SOL_VOO_STATUS_ITEM = 1) > 0 THEN 0 ELSE 1 END) as ""Selecionavel""
				FROM
				  (SELECT 
					E.EQ_DSC_RSM TX_EQUIP,    
					E.EQ_ID_EQPT ID_EQUIP,
					EP.AE_COD_EQP COD_EQUIP,
					AO.AO_COD_AOP COD_ESTACAO,
					TO_CHAR(NULL) TX_TREM,
					EP.AE_DAT_CHG DT_CHG,
					EP.AE_DAT_UTL DT_UTL,
					E.EQ_IND_STC IND_SIT
				  FROM TRANSLOGIC.EQPTO_PATIO_VIG ep,
					TRANSLOGIC.EQUIPAMENTO e,
					TRANSLOGIC.AREA_OPERACIONAL ao
				  WHERE ao.AO_ID_AO     = ep.AO_ID_AO
				  AND e.EQ_DSC_RSM NOT IN ( 'CBL' , 'OBC' , 'Sincar' )
				  AND e.EQ_ID_EQPT      = ep.EQ_ID_EQPT
				  UNION ALL
				  SELECT 
					E.EQ_DSC_RSM,    
					E.EQ_ID_EQPT,
					EP.AE_COD_EQP,
					AO.AO_COD_AOP,
					T.TR_PFX_TRM,
					EP.AE_DAT_CHG,
					EP.AE_DAT_UTL,
					E.EQ_IND_STC IND_SIT
				  FROM TRANSLOGIC.EQPTO_PATIO ep,
					TRANSLOGIC.EQUIPAMENTO e,
					TRANSLOGIC.AREA_OPERACIONAL ao,
					(SELECT *
					FROM TRANSLOGIC.TREM
					WHERE tr_pfx_trm NOT LIKE 'N%'
					AND tr_pfx_trm NOT LIKE 'E%'
					AND tr_pfx_trm NOT LIKE 'W%'
					AND tr_pfx_trm NOT LIKE 'S%'
					AND tr_pfx_trm NOT LIKE 'V%'
					AND tr_pfx_trm NOT LIKE 'A%'
					) t,
					TRANSLOGIC.ORDEMFORMACAO os,
					(SELECT ep1.EQ_ID_EQPT IDT,
					  MAX(ep1.AE_DAT_UTL) DAT
					FROM TRANSLOGIC.EQPTO_PATIO ep1
					GROUP BY ep1.EQ_ID_EQPT
					) eqcomp
				  WHERE ao.AO_ID_AO     = ep.AO_ID_AO
				  AND e.EQ_DSC_RSM NOT IN ( 'CBL' , 'OBC' , 'Sincar' )
				  AND e.EQ_IND_LOC      = 'T'
				  AND e.EQ_ID_EQPT      = ep.EQ_ID_EQPT
				  AND t.TR_ID_TRM       = ep.TR_ID_TRM
				  AND os.OF_ID_OSV      = t.OF_ID_OSV
				  AND eqcomp.IDT        = ep.EQ_ID_EQPT
				  AND eqcomp.DAT        = ep.AE_DAT_UTL
				  ) EQP ");

            #endregion

            #region Where

            if (ativos != null
                || patio != string.Empty
                || os != string.Empty)
            {
                sql.Append(" WHERE EQP.TX_TREM IS NULL {0}");

                var sqlWhere = new StringBuilder();

                if (ativos != null)
                {
                    if (ativos.Length > 0)
                    {
                        sqlWhere.AppendFormat(" AND EQP.COD_EQUIP IN ({0}) ", ativos);
                    }
                }

                if (patio != string.Empty)
                {
                    sqlWhere.AppendFormat(" AND EQP.COD_ESTACAO = '{0}' ", patio);
                }

                if (os != string.Empty)
                {
                    sqlWhere.AppendFormat(@" AND EQP.ID_EQUIP IN (SELECT CE.EQ_ID_EQPT
								   FROM COMP_EQPTO CE
								   INNER JOIN COMPOSICAO CP ON (CE.CP_ID_CPS = CP.CP_ID_CPS)
								   INNER JOIN TREM TR ON (CP.TR_ID_TRM = TR.TR_ID_TRM)
								   INNER JOIN T2_OS OS ON (TR.OF_ID_OSV = OS.X1_ID_OS)
								   WHERE OS.X1_NRO_OS = {0}
								   AND CE.CP_ID_CPS = (SELECT MAX(CP1.CP_ID_CPS) 
													   FROM COMPOSICAO CP1 
													   WHERE CP1.TR_ID_TRM = TR.TR_ID_TRM
													   AND CP1.AO_ID_AO_ORG != CP1.AO_ID_AO_ENC)) ", os);
                }

                sql.Replace("{0}", sqlWhere.ToString());
            }

            #endregion

            #region ORDER BY
            sql.AppendLine("ORDER BY EQP.COD_EQUIP ASC");
            #endregion

            return ExecuteQueries(detalhesPaginacaoWeb, sql);
        }

        public ResultadoPaginado<SolicitacaoVooAtivoItemDto> BuscarLocomotivasDisponiveisVoo(DetalhesPaginacaoWeb detalhesPaginacaoWeb, string ativos, string patio, string linha, string os)
        {
            #region Query

            var sql = new StringBuilder(@"SELECT
				  LOC.LC_ID_LC                                                                                                                                                                  AS  ""IdAtivo"",
				  LOC.LC_COD_LOC                                                                                                                                                                AS ""Ativo"",
				  ELVIA.EV_COD_ELV                                                                                                                                                              AS ""Linha"",
				  LPV.LP_NUM_SEQ                                                                                                                                                                AS ""Sequencia"",
				  LOC.TB_COD_BITOLA                                                                                                                                                             AS ""Bitola"",
				  SERLOCO.SL_DSC_PT                                                                                                                                                             AS ""Serie"",
				  'LOCOMOTIVA'                                                                                                                                                                  AS ""TipoAtivo"",  
				  ELV.EL_DAT_OCR                                                                                                                                                                AS ""DataEvento"", 
				  LOC.AO_COD_AOP                                                                                                                                                                AS ""PatioOrigem"",
				  SI.SI_DSC_PT                                                                                                                                                                  AS ""Situacao"",
				  CN.CD_DSC_PT                                                                                                                                                                  AS ""CondicaoUso"",
				  LO.LO_DSC_PT                                                                                                                                                                  AS ""Local"",
				  RE.RP_DSC_PT                                                                                                                                                                  AS ""Responsavel"",
				  IT.TB_DSC_PT                                                                                                                                                                  AS ""Intercambio"",
				  (CASE WHEN (SELECT COUNT(1) FROM SOL_VOO_ATIVOS_ITEM VI WHERE VI.ID_ATIVO = LOC.LC_ID_LC AND VI.ID_TIPO_ATIVO = 2 AND VI.ID_SOL_VOO_STATUS_ITEM = 1) > 0 THEN 0 ELSE 1 END)   AS ""Selecionavel""
				FROM (
				  SELECT 
					LC.LC_ID_LC,
					LC.LC_COD_LOC,
					LC.SL_ID_SL,
                    LC.TB_COD_BITOLA,
					AOM.AO_COD_AOP,
					EV.EV_COD_ELV,
					NULL TR_PFX_TRM,
					'P' AS TIPO_LOCAL
				  FROM LOCOMOTIVA LC
				  INNER JOIN LOCO_PATIO_VIG LPV ON (LC.LC_ID_LC = LPV.LC_ID_LC)
				  INNER JOIN AREA_OPERACIONAL AOP ON (LPV.AO_ID_AO = AOP.AO_ID_AO)
				  INNER JOIN AREA_OPERACIONAL AOM ON (AOP.AO_ID_AO_INF = AOM.AO_ID_AO)
				  INNER JOIN ELEMENTO_VIA EV ON (LPV.EV_ID_ELV = EV.EV_ID_ELV)
				  UNION ALL
				  SELECT 
					LC.LC_ID_LC,
					LC.LC_COD_LOC,
					LC.SL_ID_SL,
                    LC.TB_COD_BITOLA,
					NULL AO_COD_AOP,
					NULL EV_COD_ELV,
					TR.TR_PFX_TRM,
					'T' AS TIPO_LOCAL
				  FROM LOCOMOTIVA LC
				  INNER JOIN COMP_LOCOMOT_VIG CLV ON (LC.LC_ID_LC = CLV.LC_ID_LC)
				  INNER JOIN COMPOSICAO CMP ON (CLV.CP_ID_CPS = CMP.CP_ID_CPS)
				  INNER JOIN TREM TR ON (CMP.TR_ID_TRM = TR.TR_ID_TRM)
				  UNION ALL
				  SELECT 
					LC.LC_ID_LC,
					LC.LC_COD_LOC,
					LC.SL_ID_SL,
                    LC.TB_COD_BITOLA,
					NULL AO_COD_AOP,
					NULL EV_COD_ELV,
					NULL TR_PFX_TRM,
					'I' AS TIPO_LOCAL
				  FROM LOCOMOTIVA LC
				  INNER JOIN LOCAL_LOCO_VIG LLV ON (LC.LC_ID_LC = LLV.LC_ID_LC)
				  WHERE LLV.LO_IDT_LCL = 14 /*INTERCAMBIO*/ 
				) LOC
				INNER JOIN EVENTO_LOCO_VIG ELV ON (LOC.LC_ID_LC = ELV.LC_ID_LC)
				INNER JOIN LOCAL_LOCO_VIG LLV ON (LOC.LC_ID_LC = LLV.LC_ID_LC)
				INNER JOIN LOCALIZACAO LO ON (LLV.LO_IDT_LCL = LO.LO_IDT_LCL)
				INNER JOIN RESP_LOCO_VIG RLV ON (LOC.LC_ID_LC = RLV.LC_ID_LC)
				INNER JOIN RESPONSAVEL RE ON (RLV.RP_IDT_RSP = RE.RP_IDT_RSP)
				INNER JOIN INTERC_LOCO_VIG ILV ON (LOC.LC_ID_LC = ILV.LC_ID_LC)
				INNER JOIN INTERCAMBIO IT ON (ILV.TB_IDT_INT = IT.TB_IDT_INT)
				INNER JOIN SITUACAO_LOCO_VIG SLV ON (LOC.LC_ID_LC = SLV.LC_ID_LC)
				INNER JOIN SITUACAO SI ON (SLV.SI_IDT_STC = SI.SI_IDT_STC)
				INNER JOIN CONDUSO_LOCO_VIG CLV ON (LOC.LC_ID_LC = CLV.LC_ID_LC)
				INNER JOIN CONDICAO_USO CN ON (CLV.CD_IDT_CUS = CN.CD_IDT_CUS)  
				LEFT  JOIN LOCO_PATIO_VIG LPV ON (LOC.LC_ID_LC = LPV.LC_ID_LC)
				LEFT  JOIN ELEMENTO_VIA ELVIA ON (LPV.EV_ID_ELV = ELVIA.EV_ID_ELV)
				LEFT  JOIN SERIE_LOCOS SERLOCO ON (LOC.SL_ID_SL = SERLOCO.SL_ID_SL) ");

            #endregion

            #region Where

            if (ativos != null
                || patio != string.Empty
                || os != string.Empty)
            {
                sql.Append(" WHERE 1=1 {0}");

                var sqlWhere = new StringBuilder();

                if (!string.IsNullOrWhiteSpace(ativos))
                {
                    if (ativos.Length > 0)
                    {
                        sqlWhere.AppendFormat(" AND LOC.LC_COD_LOC IN  ({0}) ", ativos);
                    }
                }

                if (!string.IsNullOrWhiteSpace(patio))
                {
                    sqlWhere.AppendFormat(" AND LOC.AO_COD_AOP = '{0}' ", patio);
                }

                if (!string.IsNullOrWhiteSpace(linha))
                {
                    sqlWhere.AppendFormat(" AND LOC.EV_COD_ELV = '{0}' ", linha);
                }

                if (os != string.Empty)
                {
                    sqlWhere.AppendFormat(@" AND LOC.LC_ID_LC IN (SELECT CL.LC_ID_LC
					   FROM COMP_LOCOMOT CL
					   INNER JOIN COMPOSICAO CP ON (CL.CP_ID_CPS = CP.CP_ID_CPS)
					   INNER JOIN TREM TR ON (CP.TR_ID_TRM = TR.TR_ID_TRM)
					   INNER JOIN T2_OS OS ON (TR.OF_ID_OSV = OS.X1_ID_OS)
					   WHERE OS.X1_NRO_OS = {0}
					   AND CL.CP_ID_CPS = (SELECT MAX(CP1.CP_ID_CPS) 
										   FROM COMPOSICAO CP1 
										   WHERE CP1.TR_ID_TRM = TR.TR_ID_TRM
										   AND CP1.AO_ID_AO_ORG != CP1.AO_ID_AO_ENC)) ", os);
                }

                sql.Replace("{0}", sqlWhere.ToString());
            }

            #endregion

            #region OrderBy

            // Order by (Recupera o indice e o direcionamento (ASC/DESC))
            sql.Append(" ORDER BY ELVIA.EV_COD_ELV ASC, LPV.LP_NUM_SEQ ASC");

            #endregion

            return ExecuteQueries(detalhesPaginacaoWeb, sql);
        }

        public ResultadoPaginado<SolicitacaoVooAtivoItemDto> BuscarVagoesDisponiveisVoo(DetalhesPaginacaoWeb detalhesPaginacaoWeb, string ativos, string patio, string linha, string os)
        {
            #region Query

            var sql = new StringBuilder(@"SELECT 
			  VAG.VG_ID_VG AS ""IdAtivo"",
			  ELVIA.EV_COD_ELV                                                                                                                                                              AS ""Linha"",
			  VPV.VP_NUM_SEQ                                                                                                                                                                AS ""Sequencia"",
			  VAG.TB_COD_BITOLA                                                                                                                                                             AS ""Bitola"",
			  SERVAG.SV_COD_SER                                                                                                                                                             AS ""Serie"",
			  VAG.VG_COD_VAG                                                                                                                                                                AS ""Ativo"",
			  'VAGAO'                                                                                                                                                                       AS ""TipoAtivo"",
			  EV.EA_DTO_EVG                                                                                                                                                                 AS ""DataEvento"", 
			  VAG.AO_COD_AOP                                                                                                                                                                AS ""PatioOrigem"",
			  SI.SI_DSC_PT                                                                                                                                                                  AS ""Situacao"",
			  CN.CD_DSC_PT                                                                                                                                                                  AS ""CondicaoUso"",
			  LO.LO_DSC_PT                                                                                                                                                                  AS ""Local"",
			  RE.RP_DSC_PT                                                                                                                                                                  AS ""Responsavel"",
			  IT.TB_DSC_PT                                                                                                                                                                  AS ""Intercambio"",
			  CG.SH_DSC_PT                                                                                                                                                                  AS ""Lotacao"",
			  (CASE WHEN (SELECT COUNT(1) FROM SOL_VOO_ATIVOS_ITEM VI WHERE VI.ID_ATIVO = VAG.VG_ID_VG AND VI.ID_TIPO_ATIVO = 3 AND VI.ID_SOL_VOO_STATUS_ITEM = 1) > 0 THEN 0 ELSE 1 END)   AS ""Selecionavel""  
			FROM (
			  SELECT 
				VG.VG_ID_VG,
				VG.TB_COD_BITOLA,
				VG.SV_ID_SV,
				VG.VG_COD_VAG,
				AOM.AO_COD_AOP,
				EV.EV_COD_ELV,
				NULL TR_PFX_TRM,
				'P' AS TIPO_LOCAL
			  FROM VAGAO VG
			  INNER JOIN VAGAO_PATIO_VIG VPV ON (VG.VG_ID_VG = VPV.VG_ID_VG)
			  INNER JOIN AREA_OPERACIONAL AOP ON (VPV.AO_ID_AO = AOP.AO_ID_AO)
			  INNER JOIN AREA_OPERACIONAL AOM ON (AOP.AO_ID_AO_INF = AOM.AO_ID_AO)
			  INNER JOIN ELEMENTO_VIA EV ON (VPV.EV_ID_ELV = EV.EV_ID_ELV)
			  UNION ALL
			  SELECT 
				VG.VG_ID_VG,
				VG.TB_COD_BITOLA,
				VG.SV_ID_SV,
				VG.VG_COD_VAG,
				NULL AO_COD_AOP,
				NULL EV_COD_ELV,
				TR.TR_PFX_TRM,
				'T' AS TIPO_LOCAL
			  FROM VAGAO VG
			  INNER JOIN COMPVAGAO_VIG CVV ON (VG.VG_ID_VG = CVV.VG_ID_VG)
			  INNER JOIN COMPOSICAO CMP ON (CVV.CP_ID_CPS = CMP.CP_ID_CPS)
			  INNER JOIN TREM TR ON (CMP.TR_ID_TRM = TR.TR_ID_TRM)
			  UNION ALL
			  SELECT 
				VG.VG_ID_VG,
				VG.TB_COD_BITOLA,
				VG.SV_ID_SV,
				VG.VG_COD_VAG,
				NULL AO_COD_AOP,
				NULL EV_COD_ELV,
				NULL TR_PFX_TRM,
				'I' AS TIPO_LOCAL
			  FROM VAGAO VG
			  INNER JOIN LOCAL_VAGAO_VIG LVV ON (VG.VG_ID_VG = LVV.VG_ID_VG)
			  WHERE LVV.LO_IDT_LCL = 14 /*INTERCAMBIO*/ 
			) VAG
			INNER JOIN EVENTO_VAGAO_VIG EV ON (VAG.VG_ID_VG = EV.VG_ID_VG)
			INNER JOIN LOCAL_VAGAO_VIG LVV ON (VAG.VG_ID_VG = LVV.VG_ID_VG)
			INNER JOIN LOCALIZACAO LO ON (LVV.LO_IDT_LCL = LO.LO_IDT_LCL)
			INNER JOIN RESP_VAGAO_VIG RVV ON (VAG.VG_ID_VG = RVV.VG_ID_VG)
			INNER JOIN RESPONSAVEL RE ON (RVV.RP_IDT_RSP = RE.RP_IDT_RSP)
			INNER JOIN INTERC_VAGAO_VIG IVV ON (VAG.VG_ID_VG = IVV.VG_ID_VG)
			INNER JOIN INTERCAMBIO IT ON (IVV.TB_IDT_INT = IT.TB_IDT_INT)
			INNER JOIN SITUACAO_VAGAO_VIG SVV ON (VAG.VG_ID_VG = SVV.VG_ID_VG)
			INNER JOIN SITUACAO SI ON (SVV.SI_IDT_STC = SI.SI_IDT_STC)
			INNER JOIN CONDUSO_VAGAO_VIG CVV ON (VAG.VG_ID_VG = CVV.VG_ID_VG)
			INNER JOIN CONDICAO_USO CN ON (CVV.CD_IDT_CUS = CN.CD_IDT_CUS)
			INNER JOIN LOTACAO_VIG LTV ON (VAG.VG_ID_VG = LTV.VG_ID_VG)
			INNER JOIN CARGA CG ON (LTV.SH_IDT_STC = CG.SH_IDT_STC)
			LEFT JOIN VAGAO_PATIO_VIG VPV ON (VAG.VG_ID_VG = VPV.VG_ID_VG)		
            LEFT JOIN ELEMENTO_VIA ELVIA ON (VPV.EV_ID_ELV = ELVIA.EV_ID_ELV)
			LEFT JOIN SERIE_VAGAO SERVAG ON (VAG.SV_ID_SV = SERVAG.SV_ID_SV)
");

            #endregion

            #region Where

            if (ativos != null
                || patio != string.Empty
                || os != string.Empty)
            {
                sql.Append(" WHERE 1=1 {0}");

                var sqlWhere = new StringBuilder();

                if (ativos != null)
                {
                    if (ativos.Length > 0)
                    {
                        sqlWhere.AppendFormat(" AND VAG.VG_COD_VAG IN ({0}) ", ativos);
                    }
                }

                if (patio != string.Empty)
                {
                    sqlWhere.AppendFormat(" AND VAG.AO_COD_AOP = '{0}' ", patio);
                }

                if (linha != string.Empty)
                {
                    sqlWhere.AppendFormat(" AND VAG.EV_COD_ELV =  '{0}' ", linha);
                }

                if (os != string.Empty)
                {
                    sqlWhere.AppendFormat(@" AND VAG.VG_ID_VG IN (SELECT CV.VG_ID_VG
					   FROM COMPVAGAO CV
					   INNER JOIN COMPOSICAO CP ON (CV.CP_ID_CPS = CP.CP_ID_CPS)
					   INNER JOIN TREM TR ON (CP.TR_ID_TRM = TR.TR_ID_TRM)
					   INNER JOIN T2_OS OS ON (TR.OF_ID_OSV = OS.X1_ID_OS)
					   WHERE OS.X1_NRO_OS = {0}
					   AND CV.CP_ID_CPS = (SELECT MAX(CP1.CP_ID_CPS) 
										   FROM COMPOSICAO CP1 
										   WHERE CP1.TR_ID_TRM = TR.TR_ID_TRM
										   AND CP1.AO_ID_AO_ORG != CP1.AO_ID_AO_ENC)) ", os);
                }

                sql.Replace("{0}", sqlWhere.ToString());
            }

            #endregion

            #region OrderBy

            // Order by (Recupera o indice e o direcionamento (ASC/DESC))
            sql.Append(" ORDER BY ELVIA.EV_COD_ELV ASC, VPV.VP_NUM_SEQ ASC");

            #endregion

            return ExecuteQueries(detalhesPaginacaoWeb, sql);
        }

        public ResultadoPaginado<SolicitacaoVooAtivoItemDto> BuscarVagoesRefaturamentoDisponiveisVoo(DetalhesPaginacaoWeb detalhesPaginacaoWeb, string ativos, string os)
        {
            #region Query

            var sql = new StringBuilder(@"SELECT 
			VAG.VG_ID_VG                                                                                                                                                                AS ""IdAtivo"",
			ELVIA.EV_COD_ELV                                                                                                                                                            AS ""Linha"",
			CVV.CV_SEQ_CMP                                                                                                                                                              AS ""Sequencia"",
			VAG.TB_COD_BITOLA                                                                                                                                                           AS ""Bitola"",
			SERVAG.SV_COD_SER                                                                                                                                                           AS ""Serie"",
			VAG.VG_COD_VAG                                                                                                                                                              AS ""Ativo"",
			'VAGAO'                                                                                                                                                                     AS ""TipoAtivo"",
			EV.EA_DTO_EVG                                                                                                                                                               AS ""DataEvento"", 
			AOP_ORIGEM.AO_COD_AOP                                                                                                                                                       AS ""PatioOrigem"",
			SI.SI_DSC_PT                                                                                                                                                                AS ""Situacao"",
			CN.CD_DSC_PT                                                                                                                                                                AS ""CondicaoUso"", 
			LO.LO_DSC_PT                                                                                                                                                                AS  ""Local"",
			RE.RP_DSC_PT                                                                                                                                                                AS ""Responsavel"",
			IT.TB_DSC_PT                                                                                                                                                                AS ""Intercambio"",
			CG.SH_DSC_PT                                                                                                                                                                AS ""Lotacao"",
			(CASE WHEN (SELECT COUNT(1) FROM SOL_VOO_ATIVOS_ITEM VI WHERE VI.ID_ATIVO = VAG.VG_ID_VG AND VI.ID_TIPO_ATIVO = 4 AND VI.ID_SOL_VOO_STATUS_ITEM = 1) > 0 THEN 0 ELSE 1 END) AS ""Selecionavel""  
			FROM VAGAO VAG 
			INNER JOIN COMPVAGAO_VIG CVV ON (VAG.VG_ID_VG = CVV.VG_ID_VG)
			INNER JOIN COMPOSICAO CMP ON (CVV.CP_ID_CPS = CMP.CP_ID_CPS) AND CMP.CP_STT_CMP = 'A'
			INNER JOIN TREM TR ON (CMP.TR_ID_TRM = TR.TR_ID_TRM)
			INNER JOIN T2_OS OS ON (TR.OF_ID_OSV = OS.X1_ID_OS)
			INNER JOIN EVENTO_VAGAO_VIG EV ON (VAG.VG_ID_VG = EV.VG_ID_VG)
			INNER JOIN LOCAL_VAGAO_VIG LVV ON (VAG.VG_ID_VG = LVV.VG_ID_VG)
			INNER JOIN LOCALIZACAO LO ON (LVV.LO_IDT_LCL = LO.LO_IDT_LCL)
			INNER JOIN RESP_VAGAO_VIG RVV ON (VAG.VG_ID_VG = RVV.VG_ID_VG)
			INNER JOIN RESPONSAVEL RE ON (RVV.RP_IDT_RSP = RE.RP_IDT_RSP)
			INNER JOIN INTERC_VAGAO_VIG IVV ON (VAG.VG_ID_VG = IVV.VG_ID_VG)
			INNER JOIN INTERCAMBIO IT ON (IVV.TB_IDT_INT = IT.TB_IDT_INT)
			INNER JOIN SITUACAO_VAGAO_VIG SVV ON (VAG.VG_ID_VG = SVV.VG_ID_VG)
			INNER JOIN SITUACAO SI ON (SVV.SI_IDT_STC = SI.SI_IDT_STC)
			INNER JOIN CONDUSO_VAGAO_VIG CVVI ON (VAG.VG_ID_VG = CVVI.VG_ID_VG)
			INNER JOIN CONDICAO_USO CN ON (CVVI.CD_IDT_CUS = CN.CD_IDT_CUS)
			INNER JOIN LOTACAO_VIG LTV ON (VAG.VG_ID_VG = LTV.VG_ID_VG)
			INNER JOIN CARGA CG ON (LTV.SH_IDT_STC = CG.SH_IDT_STC)
			INNER JOIN VAGAO_PEDIDO_VIG VPG ON VPG.VG_ID_VG = VAG.VG_ID_VG
			INNER JOIN T2_PEDIDO PD ON PD.W1_ID_PED = VPG.PT_ID_ORT
			INNER JOIN AREA_OPERACIONAL AOP_ORIGEM ON AOP_ORIGEM.AO_ID_AO = PD.W1_IDT_EST_ORI
			LEFT JOIN VAGAO_PATIO_VIG VPV ON (VAG.VG_ID_VG = VPV.VG_ID_VG)
            LEFT JOIN ELEMENTO_VIA ELVIA ON (VPV.EV_ID_ELV = ELVIA.EV_ID_ELV)
			LEFT JOIN SERIE_VAGAO SERVAG ON (VAG.SV_ID_SV = SERVAG.SV_ID_SV) ");

            #endregion

            #region Where

            if (os != string.Empty)
            {
                sql.AppendFormat(@" WHERE OS.X1_NRO_OS = {0}", os);
            }

            if (ativos != null && ativos.Length > 0)
            {
                sql.AppendFormat(@" AND VAG.VG_COD_VAG IN ({0})", ativos);
            }

            #endregion

            #region OrderBy

            // Order by (Recupera o indice e o direcionamento (ASC/DESC))
            sql.Append(" ORDER BY CVV.CV_SEQ_CMP ASC");

            #endregion

            return ExecuteQueries(detalhesPaginacaoWeb, sql);
        }

        #endregion

        #region Avaliação e Visualização

        /// <summary>
        /// Busca Items Ativos VOO VAGAO
        /// </summary>
        /// <param name="detalhesPaginacaoWeb"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public ResultadoPaginado<SolicitacaoVooAtivoItemDto> BuscarItemsAtivosVooAvaliarVagaoPorID(DetalhesPaginacaoWeb detalhesPaginacaoWeb, int id)
        {
            #region Query

            var sql = new StringBuilder(@"SELECT 
											  IVA.ID_SOL_VOO_ITEM                                                                                               AS ""IdAtivo"",
											  ELVIA.EV_COD_ELV                                                                                                  AS ""Linha"",
											  VPV.VP_NUM_SEQ                                                                                                    AS ""Sequencia"",
											  VAG.TB_COD_BITOLA                                                                                                 AS ""Bitola"",
											  SERVAG.SV_COD_SER                                                                                                 AS ""Serie"",
											  STA.STATUS_SOL_ITEM_VOO                                                                                           AS ""StatusItem"",
												IVA.ATIVO                                                                                                       AS ""Ativo"",
												TPO.TIPO_ATIVO                                                                                                  AS ""TipoAtivo"",
												EV.EA_DTO_EVG                                                                                                   AS ""DataEvento"",
												 (CASE WHEN STA.ID_STATUS_SOL_ITEM_VOO = 2 THEN IVA.PATIO_ORIGEM ELSE  VAG.AO_COD_AOP END)                      AS ""PatioOrigem"",
												IVA.PATIO_DESTINO                                                                                               AS ""PatioDestino"",
												CN.CD_DSC_PT                                                                                                    AS ""CondicaoUso"",
												SI.SI_DSC_PT                                                                                                    AS ""Situacao"",
												LO.LO_DSC_PT                                                                                                    AS ""Local"",
												RE.RP_DSC_PT                                                                                                    AS ""Responsavel"",
												CG.SH_DSC_PT                                                                                                    AS ""Lotacao"",
												IT.TB_DSC_PT                                                                                                    AS ""Intercambio"",
												(SELECT CASE 
														WHEN COUNT(1) > 1 THEN 'Encontrado mais de um estado futuro'
														WHEN COUNT(1) = 1 THEN 'Estado futuro OK'
														ELSE 'Estado nao permitido'
													  END
											   FROM   MUDANCA_QEVG MQ,
													  QD_ESTADO_VAGAO QEV1,
													  LOCALIZACAO LO1,
													  SITUACAO SI1,
													  ELEMENTO_VIA EV1,
													  AREA_OPERACIONAL AO1,
													  AREA_OPERACIONAL AO2
											   WHERE  QEV1.QS_IDT_QDS = MQ.QS_IDT_QDS_FUT
											   AND    QEV1.LO_IDT_LCL = LO1.LO_IDT_LCL
											   AND    QEV1.SI_IDT_STC = SI1.SI_IDT_STC
											   AND    EV1.AO_ID_AO = AO2.AO_ID_AO 
											   AND    AO1.AO_ID_AO = AO2.AO_ID_AO_INF 
											   AND    MQ.QS_IDT_QDS_ATU = EV.QS_IDT_QDS
											   AND    AO1.AO_COD_AOP = IVA.PATIO_DESTINO
											   /* Local atual tem que ser Patio ou Oficina */
											   AND    LO.LO_COD_LCL IN ('LPT','LOF')
											   /* Tipo Evento Voador */
											   AND    MQ.EN_ID_EVT = 35 
											   /* Linha destino sempre L999 (LINHA VIRTUAL) */
											   AND    EV1.EV_COD_ELV = 'L999'
											   /* Verifica se a bitola do vagao é compativel com o destino */
											   AND    (EV1.TB_COD_BITOLA = VAG.TB_COD_BITOLA OR EV1.TB_COD_BITOLA = 'BM')
											   /* Local destino tem que ser Patio ou Oficina */
											   AND    LO1.LO_COD_LCL = (DECODE(AO2.AO_IND_PCR,'S','LPT'
																	   ,DECODE(AO2.AO_IND_OFC,'S','LOF' 
																	   ,DECODE(AO2.AO_IND_PMN,'S','LPT'))))
											   AND    
												  (
													 /* Caso vagao AG.MANOBRA ou AG.TRACAO, com PEDIDO e CARREGADO, vai para = Ag. Manobra */
													 (SI.SI_COD_STC IN('SAM','SAT') AND VPD.VG_ID_VG IS NOT NULL AND PD.W1_IDT_MRC IS NOT NULL AND SI1.SI_COD_STC = 'SAM') OR
													 /* Caso vagao AG.MANOBRA ou AG.TRACAO, com PEDIDO e VAZIO, vai para = Ag. Manobra */
													 (SI.SI_COD_STC IN('SAM','SAT') AND VPD.VG_ID_VG IS NOT NULL AND PD.W1_IDT_MRC IS NULL AND SI1.SI_COD_STC = 'SAM') OR
													 /* Caso vagao AG.MANOBRA ou AG.TRACAO, sem PEDIDO, mantem mesmo quadro de estado */
													 (SI.SI_COD_STC IN('SAM','SAT') AND VPD.VG_ID_VG IS NULL AND EV.QS_IDT_QDS = QEV1.QS_IDT_QDS) OR
													 /* Caso vagao não AG.MANOBRA e não AG.TRACAO, mantem mesmo quadro de estado */
													 (SI.SI_COD_STC NOT IN('SAM','SAT') AND EV.QS_IDT_QDS = QEV1.QS_IDT_QDS)
												  )
											   )                                                                                                                AS ""EstadoFuturo""
											FROM (
											  SELECT 
												VG.VG_ID_VG,
												VG.VG_COD_VAG,
												VG.TB_COD_BITOLA,
												VG.SV_ID_SV,
												AOM.AO_COD_AOP,
												EV.EV_COD_ELV,
												NULL TR_PFX_TRM,
												'P' AS TIPO_LOCAL
											  FROM VAGAO VG
											  INNER JOIN VAGAO_PATIO_VIG VPV ON (VG.VG_ID_VG = VPV.VG_ID_VG)
											  INNER JOIN AREA_OPERACIONAL AOP ON (VPV.AO_ID_AO = AOP.AO_ID_AO)
											  INNER JOIN AREA_OPERACIONAL AOM ON (AOM.AO_ID_AO = AOP.AO_ID_AO_INF)
											  INNER JOIN ELEMENTO_VIA EV ON (VPV.EV_ID_ELV = EV.EV_ID_ELV)
											  UNION ALL
											  SELECT 
												VG.VG_ID_VG,
												VG.VG_COD_VAG,
												VG.TB_COD_BITOLA,
												VG.SV_ID_SV,
												NULL AO_COD_AOP,
												NULL EV_COD_ELV,
												TR.TR_PFX_TRM,
												'T' AS TIPO_LOCAL
											  FROM VAGAO VG
											  INNER JOIN COMPVAGAO_VIG CVV ON (VG.VG_ID_VG = CVV.VG_ID_VG)
											  INNER JOIN COMPOSICAO CMP ON (CVV.CP_ID_CPS = CMP.CP_ID_CPS)
											  INNER JOIN TREM TR ON (CMP.TR_ID_TRM = TR.TR_ID_TRM)
											  UNION ALL
											  SELECT 
												VG.VG_ID_VG,
												VG.VG_COD_VAG,
												VG.TB_COD_BITOLA,
												VG.SV_ID_SV,
												NULL AO_COD_AOP,
												NULL EV_COD_ELV,
												NULL TR_PFX_TRM,
												'I' AS TIPO_LOCAL
											  FROM VAGAO VG
											  INNER JOIN LOCAL_VAGAO_VIG LVV ON (VG.VG_ID_VG = LVV.VG_ID_VG)
											  LEFT JOIN SERIE_VAGAO SV ON (VG.SV_ID_SV = SV.SV_ID_SV)
											  WHERE LVV.LO_IDT_LCL = 14 /*INTERCAMBIO*/ 
											) VAG
											INNER JOIN SOL_VOO_ATIVOS_ITEM IVA ON (IVA.ID_ATIVO = VAG.VG_ID_VG AND IVA.ID_TIPO_ATIVO = 3)
											INNER JOIN SOL_VOO_ATIVOS_STATUS_ITEM STA ON IVA.ID_SOL_VOO_STATUS_ITEM = STA.ID_STATUS_SOL_ITEM_VOO                        
											INNER JOIN SOL_TIPO_ATIVO TPO ON IVA.ID_TIPO_ATIVO = TPO.ID_TIPO_ATIVO
											INNER JOIN EVENTO_VAGAO_VIG EV ON (VAG.VG_ID_VG = EV.VG_ID_VG)
											INNER JOIN LOCAL_VAGAO_VIG LVV ON (VAG.VG_ID_VG = LVV.VG_ID_VG)
											INNER JOIN LOCALIZACAO LO ON (LVV.LO_IDT_LCL = LO.LO_IDT_LCL)
											INNER JOIN RESP_VAGAO_VIG RVV ON (VAG.VG_ID_VG = RVV.VG_ID_VG)
											INNER JOIN RESPONSAVEL RE ON (RVV.RP_IDT_RSP = RE.RP_IDT_RSP)
											INNER JOIN INTERC_VAGAO_VIG IVV ON (VAG.VG_ID_VG = IVV.VG_ID_VG)
											INNER JOIN INTERCAMBIO IT ON (IVV.TB_IDT_INT = IT.TB_IDT_INT)
											INNER JOIN SITUACAO_VAGAO_VIG SVV ON (VAG.VG_ID_VG = SVV.VG_ID_VG)
											INNER JOIN SITUACAO SI ON (SVV.SI_IDT_STC = SI.SI_IDT_STC)
											INNER JOIN CONDUSO_VAGAO_VIG CNV ON (VAG.VG_ID_VG = CNV.VG_ID_VG)
											INNER JOIN CONDICAO_USO CN ON (CNV.CD_IDT_CUS = CN.CD_IDT_CUS)
											INNER JOIN LOTACAO_VIG LTV ON (VAG.VG_ID_VG = LTV.VG_ID_VG)
											INNER JOIN CARGA CG ON (LTV.SH_IDT_STC = CG.SH_IDT_STC)
											LEFT JOIN VAGAO_PEDIDO_VIG VPD ON (VAG.VG_ID_VG = VPD.VG_ID_VG)
											LEFT JOIN VAGAO_PATIO_VIG VPV ON (VAG.VG_ID_VG = VPV.VG_ID_VG)
											LEFT JOIN T2_PEDIDO PD ON (VPD.PT_ID_ORT = PD.W1_ID_PED)
											LEFT JOIN AREA_OPERACIONAL DSTPD ON (PD.W1_IDT_EST_DES = DSTPD.AO_ID_AO)
											LEFT JOIN ELEMENTO_VIA ELVIA ON (VPV.EV_ID_ELV = ELVIA.EV_ID_ELV)
											LEFT JOIN SERIE_VAGAO SERVAG ON (VAG.SV_ID_SV = SERVAG.SV_ID_SV)");

            #endregion

            #region Where

            var sqlWhere = new StringBuilder();

            //ADICIONA FILTRO POR ID
            sqlWhere.AppendFormat(" AND IVA.ID_SOL_VOO_ATIVOS = {0}", id);

            if (sqlWhere.Length > 0)
            {
                string aux = " WHERE " + sqlWhere.ToString().Substring(5);
                sql.Append(aux);
            }

            #endregion

            #region OrderBy

            // Order by (Recupera o indice e o direcionamento (ASC/DESC))
            sql.Append(" ORDER BY ELVIA.EV_COD_ELV ASC, VPV.VP_NUM_SEQ ASC");

            #endregion

            return ExecuteQueries(detalhesPaginacaoWeb, sql);
        }

        /// <summary>
        /// Busca Items Ativos VOO LOCOMOTIVA
        /// </summary>
        /// <param name="detalhesPaginacaoWeb"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public ResultadoPaginado<SolicitacaoVooAtivoItemDto> BuscarItemsAtivosVooAvaliarLocomotivaPorID(DetalhesPaginacaoWeb detalhesPaginacaoWeb, int id)
        {
            #region Query

            var sql = new StringBuilder(@"SELECT 
												  IVA.ID_SOL_VOO_ITEM                                                                       AS ""IdAtivo"",
												  ELVIA.EV_COD_ELV                                                                          AS ""Linha"",
												  LPV.LP_NUM_SEQ                                                                            AS ""Sequencia"",
												  LOC.TB_COD_BITOLA                                                                         AS ""Bitola"",
												  SERLOCO.SL_DSC_PT                                                                         AS ""Serie"",
												  STA.STATUS_SOL_ITEM_VOO                                                                   AS ""StatusItem"",
													IVA.ATIVO                                                                               AS ""Ativo"",
													TPO.TIPO_ATIVO                                                                          AS ""TipoAtivo"",
													EV.EL_DAT_OCR                                                                           AS ""DataEvento"",
													LOC.AO_COD_AOP                                                                          AS ""PatioOrigem"",
													IVA.PATIO_DESTINO                                                                       AS ""PatioDestino"",
													CN.CD_DSC_PT                                                                            AS ""CondicaoUso"",
													SI.SI_DSC_PT                                                                            AS ""Situacao"",
													LO.LO_DSC_PT                                                                            AS ""Local"",
													RE.RP_DSC_PT                                                                            AS ""Responsavel"",
													IT.TB_DSC_PT                                                                            AS ""Intercambio"",
													(SELECT CASE 
															WHEN COUNT(1) > 1 THEN 'Encontrado mais de um estado futuro'
															WHEN COUNT(1) = 1 THEN 'Estado Futuro OK'
															ELSE 'Estado nao permitido'
														  END
												   FROM   MUDANCA_QELC MQ,
														  QD_ESTADO_LOCO QE,
														  LOCALIZACAO LO1,
														  ELEMENTO_VIA EV1,
														  AREA_OPERACIONAL AO1,
														  AREA_OPERACIONAL AO2
												   WHERE  QE.QT_IDT_QEL = MQ.QT_IDT_QEL_FUT
												   AND    QE.LO_IDT_LCL = LO1.LO_IDT_LCL
												   AND    EV1.AO_ID_AO = AO2.AO_ID_AO 
												   AND    AO1.AO_ID_AO = AO2.AO_ID_AO_INF 
												   AND    MQ.QT_IDT_QEL_ATU = EV.QT_IDT_QEL
												   AND    AO1.AO_COD_AOP = IVA.PATIO_DESTINO
                                                   AND SI.SI_COD_STC NOT IN('SMT')												  
                                                   AND    LO.LO_COD_LCL IN ('LPT','LOF')
														   /* Tipo Evento Voador */
														   AND    MQ.EN_ID_EVT = 35 
														   /* Linha destino sempre L999 (LINHA VIRTUAL) */
														   AND    EV1.EV_COD_ELV = 'L999'
														   /* Verifica se a bitola do vagao é compativel com o destino */
														   AND    (EV1.TB_COD_BITOLA = LOC.TB_COD_BITOLA OR EV1.TB_COD_BITOLA = 'BM')
														   /* Local destino tem que ser Patio ou Oficina */
														   AND    LO1.LO_COD_LCL = (DECODE(AO2.AO_IND_PCR,'S','LPT'
														   ,DECODE(AO2.AO_IND_OFC,'S','LOF' 
														   ,DECODE(AO2.AO_IND_PMN,'S','LPT')))))                                            AS ""EstadoFuturo""
												FROM (
												  SELECT 
													LC.LC_ID_LC,
													LC.LC_COD_LOC,
													LC.SL_ID_SL,
													AOM.AO_COD_AOP,
													EV.EV_COD_ELV,
													NULL TR_PFX_TRM,
													LC.TB_COD_BITOLA,
													'P' AS TIPO_LOCAL
												  FROM LOCOMOTIVA LC
												  INNER JOIN LOCO_PATIO_VIG LPV ON (LC.LC_ID_LC = LPV.LC_ID_LC)
												  INNER JOIN AREA_OPERACIONAL AOP ON (LPV.AO_ID_AO = AOP.AO_ID_AO)
												  INNER JOIN AREA_OPERACIONAL AOM ON (AOP.AO_ID_AO_INF = AOM.AO_ID_AO)
												  INNER JOIN ELEMENTO_VIA EV ON (LPV.EV_ID_ELV = EV.EV_ID_ELV)
												  UNION ALL
												  SELECT 
													LC.LC_ID_LC,
													LC.LC_COD_LOC,
													LC.SL_ID_SL,
													NULL AO_COD_AOP,
													NULL EV_COD_ELV,
													TR.TR_PFX_TRM,
													LC.TB_COD_BITOLA,
													'T' AS TIPO_LOCAL
												  FROM LOCOMOTIVA LC
												  INNER JOIN COMP_LOCOMOT_VIG CLV ON (LC.LC_ID_LC = CLV.LC_ID_LC)
												  INNER JOIN COMPOSICAO CMP ON (CLV.CP_ID_CPS = CMP.CP_ID_CPS)
												  INNER JOIN TREM TR ON (CMP.TR_ID_TRM = TR.TR_ID_TRM)
												  UNION ALL
												  SELECT 
													LC.LC_ID_LC,
													LC.LC_COD_LOC,
													LC.SL_ID_SL,
													NULL AO_COD_AOP,
													NULL EV_COD_ELV,
													NULL TR_PFX_TRM,
													LC.TB_COD_BITOLA,
													'I' AS TIPO_LOCAL
												  FROM LOCOMOTIVA LC
												  INNER JOIN LOCAL_LOCO_VIG LLV ON (LC.LC_ID_LC = LLV.LC_ID_LC)
												  WHERE LLV.LO_IDT_LCL = 14 /*INTERCAMBIO*/ 
												) LOC
												INNER JOIN SOL_VOO_ATIVOS_ITEM IVA ON (IVA.ID_ATIVO = LOC.LC_ID_LC AND IVA.ID_TIPO_ATIVO = 2)
												INNER JOIN SOL_VOO_ATIVOS_STATUS_ITEM STA ON (IVA.ID_SOL_VOO_STATUS_ITEM = STA.ID_STATUS_SOL_ITEM_VOO)
												INNER JOIN SOL_TIPO_ATIVO TPO ON (IVA.ID_TIPO_ATIVO = TPO.ID_TIPO_ATIVO)
												INNER JOIN ESTADO_LOCOMOTIVA_NOVA EV ON (LOC.LC_ID_LC = EV.LC_ID_LC)
												INNER JOIN LOCAL_LOCO_VIG LLV ON (LOC.LC_ID_LC = LLV.LC_ID_LC)
												INNER JOIN LOCALIZACAO LO ON (LLV.LO_IDT_LCL = LO.LO_IDT_LCL)
												INNER JOIN RESP_LOCO_VIG RLV ON (LOC.LC_ID_LC = RLV.LC_ID_LC)
												INNER JOIN RESPONSAVEL RE ON (RLV.RP_IDT_RSP = RE.RP_IDT_RSP)
												INNER JOIN INTERC_LOCO_VIG ILV ON (LOC.LC_ID_LC = ILV.LC_ID_LC)
												INNER JOIN INTERCAMBIO IT ON (ILV.TB_IDT_INT = IT.TB_IDT_INT)
												INNER JOIN SITUACAO_LOCO_VIG SLV ON (LOC.LC_ID_LC = SLV.LC_ID_LC)
												INNER JOIN SITUACAO SI ON (SLV.SI_IDT_STC = SI.SI_IDT_STC)
												INNER JOIN CONDUSO_LOCO_VIG CLV ON (LOC.LC_ID_LC = CLV.LC_ID_LC)
												INNER JOIN CONDICAO_USO CN ON (CLV.CD_IDT_CUS = CN.CD_IDT_CUS)
												LEFT JOIN LOCO_PATIO_VIG LPV ON (LOC.LC_ID_LC = LPV.LC_ID_LC)
												LEFT JOIN ELEMENTO_VIA ELVIA ON (LPV.EV_ID_ELV = ELVIA.EV_ID_ELV)
												LEFT JOIN SERIE_LOCOS SERLOCO ON (LOC.SL_ID_SL = SERLOCO.SL_ID_SL)");

            #endregion

            #region Where

            var sqlWhere = new StringBuilder();

            //ADICIONA FILTRO POR ID
            sqlWhere.AppendFormat(" AND IVA.ID_SOL_VOO_ATIVOS = {0}", id);

            if (sqlWhere.Length > 0)
            {
                string aux = " WHERE " + sqlWhere.ToString().Substring(5);
                sql.Append(aux);
            }

            #endregion

            #region OrderBy

            // Order by (Recupera o indice e o direcionamento (ASC/DESC))
            sql.Append(" ORDER BY ELVIA.EV_COD_ELV ASC, LPV.LP_NUM_SEQ ASC");

            #endregion

            return ExecuteQueries(detalhesPaginacaoWeb, sql);
        }

        /// <summary>
        /// Busca Items Ativos VOO EOT
        /// </summary>
        /// <param name="detalhesPaginacaoWeb"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public ResultadoPaginado<SolicitacaoVooAtivoItemDto> BuscarItemsAtivosVooAvaliarEotPorID(DetalhesPaginacaoWeb detalhesPaginacaoWeb, int id)
        {
            #region Query

            var sql = new StringBuilder(@"SELECT 
												IVA.ID_SOL_VOO_ITEM                                 AS ""IdAtivo"",
												STA.STATUS_SOL_ITEM_VOO                             AS ""StatusItem"",
												IVA.ATIVO                                           AS ""Ativo"",
												TPO.TIPO_ATIVO                                      AS ""TipoAtivo"",
												NVL(DT_CHG, DT_UTL)                                 AS ""DataEvento"",
												IVA.PATIO_ORIGEM                                    AS ""PatioOrigem"",
												IVA.PATIO_DESTINO                                   AS ""PatioDestino"",    
											  CASE IND_SIT
												WHEN 'A' THEN 'Avariado'
												WHEN 'D' THEN 'Disponivel'
												WHEN 'M' THEN 'Manutencao'
												WHEN 'I' THEN 'Inativo'
											  END                                                   AS ""Situacao"",
											  CASE
												WHEN TX_TREM IS NULL
												THEN 'Em Patio'
												ELSE 'Em Trem'
											  END                                                   AS ""Local"",
											  (CASE 
												WHEN TX_TREM IS NULL THEN 'Estado Futuro OK'
												ELSE 'Estado nao permitido'
											   END)                                                 AS ""EstadoFuturo""
											FROM
											  (SELECT 
												E.EQ_DSC_RSM TX_EQUIP,    
												E.EQ_ID_EQPT ID_EQUIP,
												EP.AE_COD_EQP COD_EQUIP,
												AO.AO_COD_AOP COD_ESTACAO,
												TO_CHAR(NULL) TX_TREM,
												EP.AE_DAT_CHG DT_CHG,
												EP.AE_DAT_UTL DT_UTL,
												E.EQ_IND_STC IND_SIT
											  FROM TRANSLOGIC.EQPTO_PATIO_VIG ep,
												TRANSLOGIC.EQUIPAMENTO e,
												TRANSLOGIC.AREA_OPERACIONAL ao
											  WHERE ao.AO_ID_AO     = ep.AO_ID_AO
											  AND e.EQ_DSC_RSM NOT IN ( 'CBL' , 'OBC' , 'Sincar' )
											  AND e.EQ_ID_EQPT      = ep.EQ_ID_EQPT
											  UNION ALL
											  SELECT 
												E.EQ_DSC_RSM,    
												E.EQ_ID_EQPT,
												EP.AE_COD_EQP,
												AO.AO_COD_AOP,
												T.TR_PFX_TRM,
												EP.AE_DAT_CHG,
												EP.AE_DAT_UTL,
												E.EQ_IND_STC IND_SIT
											  FROM TRANSLOGIC.EQPTO_PATIO ep,
												TRANSLOGIC.EQUIPAMENTO e,
												TRANSLOGIC.AREA_OPERACIONAL ao,
												(SELECT *
												FROM TRANSLOGIC.TREM
												WHERE tr_pfx_trm NOT LIKE 'N%'
												AND tr_pfx_trm NOT LIKE 'E%'
												AND tr_pfx_trm NOT LIKE 'W%'
												AND tr_pfx_trm NOT LIKE 'S%'
												AND tr_pfx_trm NOT LIKE 'V%'
												AND tr_pfx_trm NOT LIKE 'A%'
												) t,
												TRANSLOGIC.ORDEMFORMACAO os,
												(SELECT ep1.EQ_ID_EQPT IDT,
												  MAX(ep1.AE_DAT_UTL) DAT
												FROM TRANSLOGIC.EQPTO_PATIO ep1
												GROUP BY ep1.EQ_ID_EQPT
												) eqcomp
											  WHERE ao.AO_ID_AO     = ep.AO_ID_AO
											  AND e.EQ_DSC_RSM NOT IN ( 'CBL' , 'OBC' , 'Sincar' )
											  AND e.EQ_IND_LOC      = 'T'
											  AND e.EQ_ID_EQPT      = ep.EQ_ID_EQPT
											  AND t.TR_ID_TRM       = ep.TR_ID_TRM
											  AND os.OF_ID_OSV      = t.OF_ID_OSV
											  AND eqcomp.IDT        = ep.EQ_ID_EQPT
											  AND eqcomp.DAT        = ep.AE_DAT_UTL
											  ) EQP
											INNER JOIN SOL_VOO_ATIVOS_ITEM IVA ON (IVA.ID_ATIVO = EQP.ID_EQUIP AND IVA.ID_TIPO_ATIVO = 1)
											INNER JOIN SOL_VOO_ATIVOS_STATUS_ITEM STA ON IVA.ID_SOL_VOO_STATUS_ITEM = STA.ID_STATUS_SOL_ITEM_VOO                        
											INNER JOIN SOL_TIPO_ATIVO TPO ON IVA.ID_TIPO_ATIVO = TPO.ID_TIPO_ATIVO");

            #endregion

            #region Where

            var sqlWhere = new StringBuilder();

            //ADICIONA FILTRO POR ID
            sqlWhere.AppendFormat(" AND IVA.ID_SOL_VOO_ATIVOS = {0}", id);

            if (sqlWhere.Length > 0)
            {
                string aux = " WHERE " + sqlWhere.ToString().Substring(5);
                sql.Append(aux);
            }

            #endregion

            #region OrderBy

            // Order by (Recupera o indice e o direcionamento (ASC/DESC))
            sql.Append(" ORDER BY IVA.ID_SOL_VOO_ITEM ASC");

            #endregion

            return ExecuteQueries(detalhesPaginacaoWeb, sql);
        }

        /// <summary>
        /// Busca Items Ativos VOO Vagão refaturamento
        /// </summary>
        /// <param name="detalhesPaginacaoWeb"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public ResultadoPaginado<SolicitacaoVooAtivoItemDto> BuscarItemsAtivosVooAvaliarVagaoRefaturamentoPorID(DetalhesPaginacaoWeb detalhesPaginacaoWeb, int id)
        {
            #region Query

            var sql = new StringBuilder(@"SELECT 
			SSI.STATUS_SOL_ITEM_VOO                                                                     AS ""StatusItem"",
			ITEM.ID_SOL_VOO_ITEM                                                                        AS ""IdAtivo"",
			(case when SSI.STATUS_SOL_ITEM_VOO  = 'Refaturando' then 'L999'
            else 
            ELVIA.EV_COD_ELV end)                                                                            AS ""Linha"",
			(case when SSI.STATUS_SOL_ITEM_VOO  = 'Refaturando' then VPV.VP_NUM_SEQ
              ELSE
              CVV.CV_SEQ_CMP END)                                                                               AS ""Sequencia"",
			VGA.TB_COD_BITOLA                                                                           AS ""Bitola"",
			SERVAG.SV_COD_SER                                                                           AS ""Serie"",
			ITEM.ATIVO                                                                                  AS ""Ativo"",
			'Vagao Faturamento'                                                                         AS ""TipoAtivo"",
			ITEM.DT_EVENTO                                                                              AS ""DataEvento"",
			AOP_ORIGEM.AO_COD_AOP                                                                       AS ""PatioOrigem"",
			EVN.CD_DSC_PT                                                                               AS ""CondicaoUso"",
			EVN.SI_DSC_PT                                                                               AS ""Situacao"",
			EVN.LO_DSC_PT                                                                               AS ""Local"",   
			EVN.SH_DSC_PT                                                                               AS ""Lotacao"",
			VPG.VB_DAT_CAR                                                                              AS ""dtDespacho"",
			ATIVOS.OLV_TIMESTAMP                                                                        AS ""OlvTimestamp"",           
			NVL((SELECT CASE 
					  WHEN ITEM.ID_SOL_VOO_STATUS_ITEM <> 4 THEN ''
					  WHEN COUNT(1) > 1 THEN 'Encontrado mais de um estado futuro'
					  WHEN COUNT(1) = 1 THEN 'Estado futuro OK'
                      WHEN  UPPER(SSI.STATUS_SOL_ITEM_VOO) = 'REFATURANDO' THEN 'Aguardando refaturamento'                        
					  ELSE 'Estado nao permitido'
					END
			 FROM   MUDANCA_QEVG MQ,
					QD_ESTADO_VAGAO QEV1,
					LOCALIZACAO LO1,
					SITUACAO SI1,
					ELEMENTO_VIA EV1,
					AREA_OPERACIONAL AO1,
					AREA_OPERACIONAL AO2
			 WHERE  QEV1.QS_IDT_QDS = MQ.QS_IDT_QDS_FUT
			 AND    QEV1.LO_IDT_LCL = LO1.LO_IDT_LCL
			 AND    QEV1.SI_IDT_STC = SI1.SI_IDT_STC
			 AND    EV1.AO_ID_AO = AO2.AO_ID_AO 
			 AND    AO1.AO_ID_AO = AO2.AO_ID_AO_INF 
			 AND    MQ.QS_IDT_QDS_ATU = EVENT.QS_IDT_QDS
			 AND    AO1.AO_COD_AOP = ITEM.PATIO_DESTINO
			 /* Local atual tem que ser Patio ou Oficina */
			 --AND    LO.LO_COD_LCL IN ('LPT','LOF')
			 /* Tipo Evento Voador */
			 AND    MQ.EN_ID_EVT = 35 
			 /* Linha destino sempre L999 (LINHA VIRTUAL) */
			 AND    EV1.EV_COD_ELV = 'L999'
			 /* Verifica se a bitola do vagao é compativel com o destino */
			 AND    (EV1.TB_COD_BITOLA = VGA.TB_COD_BITOLA OR EV1.TB_COD_BITOLA = 'BM')
			 /* Local destino tem que ser Patio ou Oficina */
			 AND    LO1.LO_COD_LCL = (DECODE(AO2.AO_IND_PCR,'S','LPT'
									 ,DECODE(AO2.AO_IND_OFC,'S','LOF' 
									 ,DECODE(AO2.AO_IND_PMN,'S','LPT'))))

                AND    
				(
					/* Caso vagao AG.MANOBRA ou AG.TRACAO, com PEDIDO e CARREGADO, vai para = Ag. Manobra */
					(SI.SI_COD_STC IN('SAM','SAT') AND VPG.VG_ID_VG IS NOT NULL AND PD.W1_IDT_MRC IS NOT NULL AND SI1.SI_COD_STC = 'SAM') OR
					/* Caso vagao AG.MANOBRA ou AG.TRACAO, com PEDIDO e VAZIO, vai para = Ag. Manobra */
					(SI.SI_COD_STC IN('SAM','SAT') AND VPG.VG_ID_VG IS NOT NULL AND PD.W1_IDT_MRC IS NULL AND SI1.SI_COD_STC = 'SAM') OR
					/* Caso vagao AG.MANOBRA ou AG.TRACAO, sem PEDIDO, mantem mesmo quadro de estado */
					(SI.SI_COD_STC IN('SAM','SAT') AND VPG.VG_ID_VG IS NULL AND EVN.QS_IDT_QDS = QEV1.QS_IDT_QDS) OR
					/* Caso vagao não AG.MANOBRA e não AG.TRACAO, mantem mesmo quadro de estado */
					(SI.SI_COD_STC NOT IN('SAM','SAT') 
		  AND EVN.QS_IDT_QDS = QEV1.QS_IDT_QDS)
				)
			 ), 'Aguardando Refaturamento')                                                                            AS ""EstadoFuturo""            
			
			FROM SOL_VOO_ATIVOS_ITEM ITEM
			JOIN VAGAO VGA ON ITEM.ID_ATIVO = VGA.VG_ID_VG
			INNER JOIN SOL_VOO_ATIVOS ATIVOS ON ATIVOS.ID_SOL_VOO = ITEM.ID_SOL_VOO_ATIVOS
			INNER JOIN ESTADO_VAGAO_NOVA EVN ON EVN.VG_ID_VG = ITEM.ID_ATIVO
			LEFT JOIN COMPVAGAO_VIG CVV ON VGA.VG_ID_VG = CVV.VG_ID_VG
			LEFT JOIN COMPOSICAO CPS ON CPS.CP_ID_CPS = CVV.CP_ID_CPS AND CPS.CP_STT_CMP = 'A'  
			LEFT JOIN TREM TRM ON TRM.TR_ID_TRM = CPS.TR_ID_TRM
			LEFT JOIN LOTACAO_VIG LTV ON LTV.VG_ID_VG = VGA.VG_ID_VG
			LEFT JOIN CARGA CRG ON CRG.SH_IDT_STC = LTV.SH_IDT_STC
			LEFT JOIN SOL_VOO_ATIVOS_STATUS_ITEM SSI ON SSI.ID_STATUS_SOL_ITEM_VOO = ITEM.ID_SOL_VOO_STATUS_ITEM
			JOIN SOL_VOO_ATIVOS SVA ON SVA.ID_SOL_VOO = ITEM.ID_SOL_VOO_ATIVOS
			LEFT JOIN VAGAO_PEDIDO_VIG VPG ON VPG.VG_ID_VG = VGA.VG_ID_VG
			LEFT JOIN T2_PEDIDO PD ON PD.W1_ID_PED = VPG.PT_ID_ORT
			LEFT  JOIN AREA_OPERACIONAL AOP_ORIGEM ON AOP_ORIGEM.AO_ID_AO = PD.W1_IDT_EST_ORI
			LEFT  JOIN SITUACAO_VAGAO_VIG SVV ON (VGA.VG_ID_VG = SVV.VG_ID_VG)
			LEFT JOIN SITUACAO SI ON (SVV.SI_IDT_STC = SI.SI_IDT_STC)
			LEFT  JOIN EVENTO_VAGAO_VIG EVENT ON (VGA.VG_ID_VG = EVENT.VG_ID_VG)
			LEFT JOIN VAGAO_PATIO_VIG VPV ON (VGA.VG_ID_VG = VPV.VG_ID_VG)
			LEFT JOIN ELEMENTO_VIA ELVIA ON (AOP_ORIGEM.AO_ID_AO = ELVIA.AO_ID_AO)
			LEFT JOIN SERIE_VAGAO SERVAG ON (VGA.SV_ID_SV = SERVAG.SV_ID_SV)
			WHERE ITEM.ID_SOL_VOO_ATIVOS = " + id);

            #endregion

            #region OrderBy

            // Order by (Recupera o indice e o direcionamento (ASC/DESC))
            sql.Append(" ORDER BY CVV.CV_SEQ_CMP ASC");

            #endregion

            return ExecuteQueries(detalhesPaginacaoWeb, sql);
        }

        #endregion

        #region Aprovação/Reprovação

        public SolicitacaoVooAtivoItemPkgDto BuscarAtivosEotsParaPkgs(string ativo, string patioDestino)
        {
            #region Query

            var sql = new StringBuilder(@"SELECT 
											  EP.EQ_ID_EQPT AS ""IdAtivo"",
											  EV.AE_COD_EQP AS ""Ativo"",
											  AO.AO_ID_AO   AS ""IdPatioOrigem"",
											  AD.AO_ID_AO   AS ""IdPatioDestino"",
											  1             AS ""IdEstadoFuturo""
											FROM TRANSLOGIC.EQPTO_PATIO_VIG EV,
											  TRANSLOGIC.EQUIPAMENTO EP,
											  TRANSLOGIC.AREA_OPERACIONAL AO,
											  AREA_OPERACIONAL AD  
											WHERE AO.AO_ID_AO = EV.AO_ID_AO
											AND EP.EQ_ID_EQPT = EV.EQ_ID_EQPT
											AND EP.EQ_DSC_RSM NOT IN ( 'CBL' , 'OBC' , 'Sincar' )");

            #endregion

            #region Where

            sql.AppendFormat(" AND EV.AE_COD_EQP = '{0}' ", ativo);
            sql.AppendFormat(" AND AD.AO_COD_AOP = '{0}' ", patioDestino);

            #endregion

            return ExecuteQuerieUniqueResult(sql);
        }

        public SolicitacaoVooAtivoItemPkgDto BuscarAtivosLocomotivasParaPkgs(string ativo, string patioDestino)
        {
            #region Query

            var sql = new StringBuilder(@" SELECT 
				T.LC_ID_LC    AS ""IdAtivo"", 
				T.LC_COD_LOC     AS ""Ativo"",
				QEL.LO_IDT_LCL  AS ""IdLocal"",
				QEL.RP_IDT_RSP  AS ""IdResponsavel"",
				QEL.SI_IDT_STC  AS ""IdSituacao"",
				QEL.TB_IDT_INT  AS ""IdIntercambio"",
				QEL.CD_IDT_CUS  AS ""IdCondUso"",
				T.AO_ID_AO_DST  AS ""IdPatioDestino"",
				T.EV_ID_ELV_DST AS ""IdLinhaDestino"",
				T.AO_ID_AO_ORI  AS ""IdPatioOrigem"",
				T.EV_ID_ELV_ORI AS ""IdLinhaOrigem"",
				T.ID_ESTADO_FUTURO  AS ""IdEstadoFuturo""  
					FROM (  
					  SELECT 
						LC.LC_ID_LC,
						LC.LC_COD_LOC,
						AOM.AO_ID_AO AS AO_ID_AO_ORI,
						LPV.EV_ID_ELV AS EV_ID_ELV_ORI,
                        LPV.LP_NUM_SEQ,
						ADM.AO_ID_AO AS AO_ID_AO_DST,
						EVD.EV_ID_ELV AS EV_ID_ELV_DST,
						ELV.QT_IDT_QEL,
						(SELECT CASE 
									WHEN COUNT(1) > 1 THEN NULL
									WHEN COUNT(1) = 1 THEN MAX(MQ.QT_IDT_QEL_FUT)
									ELSE NULL
								  END
						 FROM   MUDANCA_QELC MQ,
								QD_ESTADO_LOCO QEL1,
								LOCALIZACAO LO1,
								ELEMENTO_VIA EV,
								AREA_OPERACIONAL AO1,
								AREA_OPERACIONAL AO2
						 WHERE  QEL1.QT_IDT_QEL = MQ.QT_IDT_QEL_FUT
						 AND    QEL1.LO_IDT_LCL = LO1.LO_IDT_LCL
						 AND    EV.AO_ID_AO = AO2.AO_ID_AO 
						 AND    AO1.AO_ID_AO = AO2.AO_ID_AO_INF 
						 AND    MQ.EN_ID_EVT = 35 /* Evento Voador */
						 AND    MQ.QT_IDT_QEL_ATU = ELV.QT_IDT_QEL
						 AND    AO1.AO_ID_AO = ADM.AO_ID_AO
						 AND    EV.EV_ID_ELV = EVD.EV_ID_ELV
						 AND    LO1.LO_COD_LCL = (DECODE(AO2.AO_IND_PCR,'S','LPT', 
												  DECODE(AO2.AO_IND_OFC,'S','LOF', 
												  DECODE(AO2.AO_IND_PMN,'S','LPT', 
												  DECODE(AO2.AO_TRM_CLI,'S','LCL')))))) AS ID_ESTADO_FUTURO
					  FROM LOCOMOTIVA LC,
						   LOCO_PATIO_VIG LPV,
						   ESTADO_LOCOMOTIVA_NOVA ELV,
						   AREA_OPERACIONAL AOP,
						   AREA_OPERACIONAL AOM,
						   AREA_OPERACIONAL ADP,
						   AREA_OPERACIONAL ADM,
						   ELEMENTO_VIA EVD
					  WHERE LC.LC_ID_LC = LPV.LC_ID_LC
					  AND   LPV.AO_ID_AO = AOP.AO_ID_AO
					  AND   AOM.AO_ID_AO = AOP.AO_ID_AO_INF
					  AND   LC.LC_ID_LC = ELV.LC_ID_LC
					  AND   ADM.AO_ID_AO = ADP.AO_ID_AO_INF
					  AND   ADP.AO_ID_AO = EVD.AO_ID_AO  ");

            #endregion

            #region Where

            sql.AppendFormat(" AND  LC.LC_COD_LOC = '{0}' ", ativo);
            sql.AppendFormat(" AND  ADM.AO_COD_AOP = '{0}' ", patioDestino);

            sql.Append(@" AND  EVD.EV_COD_ELV = 'L999'
					  ) T
					LEFT JOIN QD_ESTADO_LOCO QEL ON (T.ID_ESTADO_FUTURO = QEL.QT_IDT_QEL) ");

            #endregion

            #region OrderBy

            // Order by (direcionamento (ASC/DESC))
            //sql.Append(" ORDER BY T.LP_NUM_SEQ ");

            #endregion

            return ExecuteQuerieUniqueResult(sql);
        }

        public SolicitacaoVooAtivoItemPkgDto BuscarAtivosVagoesParaPkgs(string ativo, string patioDestino)
        {
            #region Query
            var sql = new StringBuilder(@"SELECT 
											T.VG_ID_VG      AS ""IdAtivo"",  
											T.VG_COD_VAG    AS ""Ativo"",
											T.AO_ID_AO_ORI  AS ""IdPatioOrigem"",
											T.EV_ID_ELV_ORI AS ""IdLinhaOrigem"",
											T.AO_ID_AO_DST  AS ""IdPatioDestino"",
											T.EV_ID_ELV_DST AS ""IdLinhaDestino"",
											T.VP_NUM_SEQ    AS ""NumeroSequencia"",
											T.ID_ESTADO_FUTURO   AS ""IdEstadoFuturo"",
											QEV.LO_IDT_LCL  AS ""IdLocal"",
											QEV.RP_IDT_RSP  AS ""IdResponsavel"",
											QEV.SI_IDT_STC  AS ""IdSituacao"",
											QEV.SH_IDT_STC  AS ""IdLotacao"",
											QEV.CD_IDT_CUS  AS ""IdCondUso"",
											QEV.TB_IDT_INT  AS ""IdIntercambio""
											FROM (  
											  SELECT 
												VAG.VG_ID_VG,
												VAG.VG_COD_VAG,
												AOP.AO_ID_AO AS AO_ID_AO_ORI,
												VPV.EV_ID_ELV AS EV_ID_ELV_ORI,
												VPV.VP_NUM_SEQ,
												ADP.AO_ID_AO AS AO_ID_AO_DST,
												EVD.EV_ID_ELV AS EV_ID_ELV_DST,
												EVV.QS_IDT_QDS,
												(SELECT CASE 
															WHEN COUNT(1) > 1 THEN NULL
															WHEN COUNT(1) = 1 THEN MAX(MQ.QS_IDT_QDS_FUT)
															ELSE NULL
														  END
												 FROM   MUDANCA_QEVG MQ,
														QD_ESTADO_VAGAO QEV1,
														LOCALIZACAO LO1,
														SITUACAO SI1,
														ELEMENTO_VIA EV1,
														AREA_OPERACIONAL AO1,
														AREA_OPERACIONAL AO2
												 WHERE  QEV1.QS_IDT_QDS = MQ.QS_IDT_QDS_FUT
												 AND    QEV1.LO_IDT_LCL = LO1.LO_IDT_LCL
												 AND    QEV1.SI_IDT_STC = SI1.SI_IDT_STC
												 AND    EV1.AO_ID_AO = AO2.AO_ID_AO 
												 AND    AO1.AO_ID_AO = AO2.AO_ID_AO_INF 
												 AND    MQ.EN_ID_EVT = 35 /* Evento Voador */
												 AND    MQ.QS_IDT_QDS_ATU = EVV.QS_IDT_QDS
												 AND    AO1.AO_ID_AO = ADM.AO_ID_AO
												 AND    EV1.EV_ID_ELV = EVD.EV_ID_ELV
												 /* Local destino tem que ser Patio ou Oficina */
												 AND    LO1.LO_COD_LCL = (DECODE(AO2.AO_IND_PCR,'S','LPT'
																		 ,DECODE(AO2.AO_IND_OFC,'S','LOF' 
																		 ,DECODE(AO2.AO_IND_PMN,'S','LPT'))))
												 AND    
													(
														 /* Caso vagao AG.MANOBRA ou AG.TRACAO, com PEDIDO e CARREGADO, vai para = Ag. Manobra */
														 (SI.SI_COD_STC IN('SAM','SAT') AND VPD.VG_ID_VG IS NOT NULL AND PD.W1_IDT_MRC IS NOT NULL AND SI1.SI_COD_STC = 'SAM') OR
														 /* Caso vagao AG.MANOBRA ou AG.TRACAO, com PEDIDO e VAZIO, vai para = Ag. Manobra */
														 (SI.SI_COD_STC IN('SAM','SAT') AND VPD.VG_ID_VG IS NOT NULL AND PD.W1_IDT_MRC IS NULL AND SI1.SI_COD_STC = 'SAM') OR
														 /* Caso vagao AG.MANOBRA ou AG.TRACAO, sem PEDIDO, mantem mesmo quadro de estado */
														 (SI.SI_COD_STC IN('SAM','SAT') AND VPD.VG_ID_VG IS NULL AND EVV.QS_IDT_QDS = QEV1.QS_IDT_QDS) OR
														 /* Caso vagao não AG.MANOBRA e não AG.TRACAO, mantem mesmo quadro de estado */
														 (SI.SI_COD_STC NOT IN('SAM','SAT') AND EVV.QS_IDT_QDS = QEV1.QS_IDT_QDS)
													)
												 ) AS ID_ESTADO_FUTURO
											  FROM  VAGAO VAG
											  INNER JOIN VAGAO_PATIO_VIG VPV ON (VAG.VG_ID_VG = VPV.VG_ID_VG)
											  INNER JOIN AREA_OPERACIONAL AOP ON (VPV.AO_ID_AO = AOP.AO_ID_AO)
											  INNER JOIN AREA_OPERACIONAL AOM ON (AOP.AO_ID_AO_INF = AOM.AO_ID_AO)
											  INNER JOIN EVENTO_VAGAO_VIG EVV ON (VAG.VG_ID_VG = EVV.VG_ID_VG)
											  INNER JOIN LOCAL_VAGAO_VIG LVV ON (VAG.VG_ID_VG = LVV.VG_ID_VG)
											  INNER JOIN LOCALIZACAO LO ON (LVV.LO_IDT_LCL = LO.LO_IDT_LCL)
											  INNER JOIN RESP_VAGAO_VIG RVV ON (VAG.VG_ID_VG = RVV.VG_ID_VG)
											  INNER JOIN RESPONSAVEL RE ON (RVV.RP_IDT_RSP = RE.RP_IDT_RSP)
											  INNER JOIN INTERC_VAGAO_VIG IVV ON (VAG.VG_ID_VG = IVV.VG_ID_VG)
											  INNER JOIN INTERCAMBIO IT ON (IVV.TB_IDT_INT = IT.TB_IDT_INT)
											  INNER JOIN SITUACAO_VAGAO_VIG SVV ON (VAG.VG_ID_VG = SVV.VG_ID_VG)
											  INNER JOIN SITUACAO SI ON (SVV.SI_IDT_STC = SI.SI_IDT_STC)
											  INNER JOIN CONDUSO_VAGAO_VIG CVV ON (VAG.VG_ID_VG = CVV.VG_ID_VG)
											  INNER JOIN CONDICAO_USO CN ON (CVV.CD_IDT_CUS = CN.CD_IDT_CUS)
											  INNER JOIN LOTACAO_VIG LTV ON (VAG.VG_ID_VG = LTV.VG_ID_VG)
											  INNER JOIN CARGA CG ON (LTV.SH_IDT_STC = CG.SH_IDT_STC)
											  LEFT JOIN VAGAO_PEDIDO_VIG VPD ON (VAG.VG_ID_VG = VPD.VG_ID_VG)
											  LEFT JOIN T2_PEDIDO PD ON (VPD.PT_ID_ORT = PD.W1_ID_PED)
											  LEFT JOIN AREA_OPERACIONAL DSTPD ON (PD.W1_IDT_EST_DES = DSTPD.AO_ID_AO),
											  AREA_OPERACIONAL ADM 
											  INNER JOIN AREA_OPERACIONAL ADP ON (ADM.AO_ID_AO = ADP.AO_ID_AO_INF)
											  INNER JOIN ELEMENTO_VIA EVD ON (EVD.AO_ID_AO = ADP.AO_ID_AO)
											  WHERE VAG.VG_COD_VAG = ':COD_VAGAO'
											  AND   ADM.AO_COD_AOP = ':COD_PATIO_DESTINO'
											  AND   EVD.EV_COD_ELV = 'L999'
											  ) T
											LEFT JOIN QD_ESTADO_VAGAO QEV ON (T.ID_ESTADO_FUTURO = QEV.QS_IDT_QDS)");

            #endregion

            #region Where

            sql = sql.Replace(":COD_VAGAO", ativo);
            sql = sql.Replace(":COD_PATIO_DESTINO", patioDestino);

            #endregion

            #region OrderBy

            // Order by (direcionamento (ASC/DESC))
            //sql.Append(" ORDER BY T.VP_NUM_SEQ ");

            #endregion

            return ExecuteQuerieUniqueResult(sql);
        }

        public SolicitacaoVooAtivoItemPkgDto BuscarAtivosVagoesRefaturamentoParaPkgs(string ativo, string patioDestino)
        {
            #region Query

            var sql = new StringBuilder(@"SELECT 
				  T.VG_ID_VG            AS ""IdAtivo"",
				  T.VG_COD_VAG          AS ""Ativo"",
				  QEV.LO_IDT_LCL        AS ""IdLocal"",
				  QEV.RP_IDT_RSP        AS ""IdResponsavel"",
				  QEV.SI_IDT_STC        AS ""IdSituacao"",
				  QEV.SH_IDT_STC        AS ""IdLotacao"",
				  QEV.TB_IDT_INT        AS ""IdIntercambio"",
				  QEV.CD_IDT_CUS        AS ""IdCondUso"",
				  T.AO_ID_AO_DST        AS ""IdPatioDestino"",
				  T.EV_ID_ELV_DST       AS ""IdLinhaDestino"",
				  T.AO_ID_AO_ORI        AS ""IdLinhaOrigem"",
				  T.EV_ID_ELV_ORI       AS ""IdPatioOrigem"",
				  T.ID_ESTADO_FUTURO    AS ""IdEstadoFuturo""
				FROM (  
				  SELECT 
					VG.VG_ID_VG,
					VG.VG_COD_VAG,
					AOP.AO_ID_AO AS AO_ID_AO_ORI,
					VPV.EV_ID_ELV AS EV_ID_ELV_ORI,
					ADP.AO_ID_AO AS AO_ID_AO_DST,
					EVD.EV_ID_ELV AS EV_ID_ELV_DST,
					EVV.QS_IDT_QDS,
					(SELECT CASE 
								WHEN COUNT(1) > 1 THEN NULL
								WHEN COUNT(1) = 1 THEN MAX(MQ.QS_IDT_QDS_FUT)
								ELSE NULL
							  END
					 FROM   MUDANCA_QEVG MQ,
							QD_ESTADO_VAGAO QEV1,
							LOCALIZACAO LO1,
							ELEMENTO_VIA EV,
							AREA_OPERACIONAL AO1,
							AREA_OPERACIONAL AO2
					 WHERE  QEV1.QS_IDT_QDS = MQ.QS_IDT_QDS_FUT
					 AND    QEV1.LO_IDT_LCL = LO1.LO_IDT_LCL
					 AND    EV.AO_ID_AO = AO2.AO_ID_AO 
					 AND    AO1.AO_ID_AO = AO2.AO_ID_AO_INF 
					 AND    MQ.EN_ID_EVT = 35 /* Evento Voador */
					 AND    MQ.QS_IDT_QDS_ATU = EVV.QS_IDT_QDS
					 AND    AO1.AO_ID_AO = ADM.AO_ID_AO
					 AND    EV.EV_ID_ELV = EVD.EV_ID_ELV
					 AND    LO1.LO_COD_LCL = (DECODE(AO2.AO_IND_PCR,'S','LPT', 
											  DECODE(AO2.AO_IND_OFC,'S','LOF', 
											  DECODE(AO2.AO_IND_PMN,'S','LPT', 
											  DECODE(AO2.AO_TRM_CLI,'S','LCL')))))) AS ID_ESTADO_FUTURO
				  FROM VAGAO VG,
					   VAGAO_PATIO_VIG VPV,
					   EVENTO_VAGAO_VIG EVV,
					   AREA_OPERACIONAL AOP,
					   AREA_OPERACIONAL AOM,
					   AREA_OPERACIONAL ADP,
					   AREA_OPERACIONAL ADM,
					   ELEMENTO_VIA EVD
				  WHERE VG.VG_ID_VG = VPV.VG_ID_VG
				  AND   VPV.AO_ID_AO = AOP.AO_ID_AO
				  AND   AOM.AO_ID_AO = AOP.AO_ID_AO_INF
				  AND   VG.VG_ID_VG = EVV.VG_ID_VG
				  AND   ADM.AO_ID_AO = ADP.AO_ID_AO_INF
				  AND   ADP.AO_ID_AO = EVD.AO_ID_AO ");

            #endregion

            #region Where

            sql.AppendFormat(" AND  VG.VG_COD_VAG = '{0}' ", ativo);
            sql.AppendFormat(" AND  ADM.AO_COD_AOP = '{0}' ", patioDestino);

            sql.Append(@" AND   EVD.EV_COD_ELV = 'L999'
				  ) T
				LEFT JOIN QD_ESTADO_VAGAO QEV ON (T.ID_ESTADO_FUTURO = QEV.QS_IDT_QDS) ");

            #endregion

            return ExecuteQuerieUniqueResult(sql);
        }

        public DataTable BuscarDadosPrepararRefaturamento(int idAtivo)
        {
            #region Query

            var sql = new StringBuilder(@"SELECT 
											TR.TR_ID_TRM AS ""IdTrem"",
											CPV.VG_ID_VG AS ""IdVagao"",
											CP.CP_ID_CPS AS ""IdComposicao"", 
											(
											SELECT DP.DP_ID_DP
											FROM CARREGAMENTO CG,DETALHE_CARREGAMENTO DC, ITEM_DESPACHO IDS,DESPACHO DP
											WHERE CG.CG_ID_CAR = DC.CG_ID_CAR
											AND   IDS.DC_ID_CRG = DC.DC_ID_CRG
											AND   IDS.VG_ID_VG = CPV.VG_ID_VG
											AND   CG.PT_ID_ORT = VPV.PT_ID_ORT
											AND   IDS.DP_ID_DP = DP.DP_ID_DP
											) AS ""IdDespacho""
										FROM TREM TR
										INNER JOIN COMPOSICAO CP ON CP.TR_ID_TRM = TR.TR_ID_TRM
										INNER JOIN COMPVAGAO_VIG CPV ON CPV.CP_ID_CPS = CP.CP_ID_CPS 
										LEFT JOIN VAGAO_PEDIDO_VIG VPV ON VPV.VG_ID_VG = CPV.VG_ID_VG
										INNER JOIN SOL_VOO_ATIVOS_ITEM ITEM ON ITEM.ID_ATIVO = CPV.VG_ID_VG

										WHERE CP.CP_STT_CMP = 'A'");

            #endregion

            #region Where

            var sqlWhere = new StringBuilder();

            //ADICIONA FILTRO POR ID
            sql.AppendFormat(" AND  ITEM.ID_SOL_VOO_ATIVOS = '{0}' ", idAtivo);

            sql.Append(" order by CPV.CV_SEQ_CMP desc ");

            #endregion

            #region OrderBy

            // Order by (Recupera o indice e o direcionamento (ASC/DESC))
            //sql.Append(" ORDER BY IVA.ID_SOL_VOO_ITEM ASC");

            #endregion

            #region Execução SQL
            using (ISession session = OpenSession())
            {

                var cmd = new OracleCommand
                {
                    CommandType = CommandType.Text,
                    CommandText = sql.ToString(),
                    Connection = (OracleConnection)session.Connection
                };

                var dt = new DataTable();
                var adapter = new OracleDataAdapter(cmd);

                adapter.Fill(dt);
                return dt;
            }
            #endregion
        }

        /// <summary>
        /// Chama a package para Aprovar Ativo Voo Eot.
        /// </summary>
        /// <param name="xml"> Código do fluxo</param>
        /// <returns>Retorna retorno de execução de PKG.</returns>
        public string AprovarEotPKG(string xml)
        {
            string xmlSaida = string.Empty;

            #region Executa PKG
            using (ISession session = OpenSession())
            {
                session.BeginTransaction();
                var command = new OracleCommand
                {
                    CommandType = CommandType.StoredProcedure,
                    CommandText = "PKG_VOO_ATIVOS.SP_EXECUTAR_VOO_EQUIPAMENTO",
                    Connection = (OracleConnection)session.Connection
                };

                AddParameter(command, "pxmlEntrada", OracleType.VarChar, xml, ParameterDirection.Input);
                AddParameter(command, "pxmlSaida", OracleType.VarChar, xmlSaida, ParameterDirection.Output);
                command.Parameters["pxmlSaida"].Size = 4000;
                session.Transaction.Enlist(command);
                try
                {
                    command.Prepare();
                    command.ExecuteNonQuery();
                    session.Transaction.Commit();

                    xmlSaida = command.Parameters["pxmlSaida"].Value.ToString();
                }
                catch (Exception ex)
                {
                    session.Transaction.Rollback();

                    xmlSaida = ex.Message;

                    xmlSaida = FormataXMLSaida(xmlSaida);

                }

                return xmlSaida;
            }
            #endregion
        }

        /// <summary>
        /// Chama a package para Aprovar Ativo Voo VAGAO.
        /// </summary>
        /// <param name="xml"> Código do fluxo</param>
        /// <returns>Retorna retorno de execução de PKG.</returns>
        public string AprovarVagaoPKG(string xml)
        {
            string xmlSaida = string.Empty;

            #region Executa PKG
            using (ISession session = OpenSession())
            {
                session.BeginTransaction();
                var command = new OracleCommand
                {
                    CommandType = CommandType.StoredProcedure,
                    CommandText = "PKG_VOO_ATIVOS.SP_EXECUTAR_VOO_VAGAO",
                    Connection = (OracleConnection)session.Connection
                };

                AddParameter(command, "pxmlEntrada", OracleType.VarChar, xml, ParameterDirection.Input);
                AddParameter(command, "pxmlSaida", OracleType.VarChar, xmlSaida, ParameterDirection.Output);
                command.Parameters["pxmlSaida"].Size = 4000;
                session.Transaction.Enlist(command);
                try
                {
                    command.Prepare();
                    command.ExecuteNonQuery();
                    session.Transaction.Commit();

                    xmlSaida = command.Parameters["pxmlSaida"].Value.ToString();
                }
                catch (Exception ex)
                {
                    session.Transaction.Rollback();

                    xmlSaida = ex.Message;

                    xmlSaida = FormataXMLSaida(xmlSaida);

                }

                return xmlSaida;
            }
            #endregion
        }

        /// <summary>
        /// Chama a package para Aprovar Ativo Voo LOCOMOTIVA.
        /// </summary>
        /// <param name="xml"> Código do fluxo</param>
        /// <returns>Retorna retorno de execução de PKG.</returns>
        public string AprovarLocomotivaPKG(string xml)
        {
            string xmlSaida = string.Empty;

            #region Executa PKG
            using (ISession session = OpenSession())
            {
                session.BeginTransaction();
                var command = new OracleCommand
                {
                    CommandType = CommandType.StoredProcedure,
                    CommandText = "PKG_VOO_ATIVOS.SP_EXECUTAR_VOO_LOCOMOTIVA",
                    Connection = (OracleConnection)session.Connection
                };

                AddParameter(command, "pxmlEntrada", OracleType.VarChar, xml, ParameterDirection.Input);
                AddParameter(command, "pxmlSaida", OracleType.VarChar, xmlSaida, ParameterDirection.Output);
                command.Parameters["pxmlSaida"].Size = 4000;
                session.Transaction.Enlist(command);
                try
                {
                    command.Prepare();
                    command.ExecuteNonQuery();
                    session.Transaction.Commit();

                    xmlSaida = command.Parameters["pxmlSaida"].Value.ToString();
                }
                catch (Exception ex)
                {
                    session.Transaction.Rollback();

                    xmlSaida = ex.Message;

                    xmlSaida = FormataXMLSaida(xmlSaida);
                }

                return xmlSaida;
            }
            #endregion
        }

        public string PrepararVagaoRefaturamentoPKG(string xml)
        {
            #region Executa PKG
            using (ISession session = OpenSession())
            {

                //XmlDocument xmlDocument = new XmlDocument();
                string xmlVagoes = (xml);
                string xmlRetorno = null;

                session.BeginTransaction();
                var command = new OracleCommand
                {
                    CommandType = CommandType.StoredProcedure,
                    CommandText = "PKG_VOO_ATIVOS.SP_EXECUTAR_REFAT_VAGAO",
                    Connection = (OracleConnection)session.Connection
                };

                AddParameter(command, "pxmlEntrada", OracleType.VarChar, xmlVagoes, ParameterDirection.Input);
                AddParameter(command, "pxmlSaida", OracleType.VarChar, xmlRetorno, ParameterDirection.Output);
                command.Parameters["pxmlSaida"].Size = 4000;
                session.Transaction.Enlist(command);
                try
                {
                    command.Prepare();
                    command.ExecuteNonQuery();
                    session.Transaction.Commit();

                    xmlRetorno = command.Parameters["pxmlSaida"].Value.ToString();
                }
                catch (Exception ex)
                {
                    session.Transaction.Rollback();

                    xmlRetorno = ex.Message;

                    xmlRetorno = FormataXMLSaida(xmlRetorno);
                }

                return xmlRetorno;
            }
            #endregion
        }

        #endregion

        #region VerificarItensPendentes

        /// <summary>
        /// Busca Items Ativos VOO VAGAO
        /// </summary>
        /// <param name="detalhesPaginacaoWeb"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public decimal BuscarItemsVagoesPendentes(int id)
        {
            #region Query

            var sql = new StringBuilder(@"SELECT
												IVA.ID_SOL_VOO_ITEM                                                         AS ""IdAtivo"", 
												STA.STATUS_SOL_ITEM_VOO                                                     AS ""StatusItem"",
												IVA.ATIVO                                                                   AS ""Ativo"",
												TPO.TIPO_ATIVO                                                              AS ""TipoAtivo"",
												EV.EA_DTO_EVG                                                               AS ""DataEvento"",
												VAG.AO_COD_AOP                                                              AS ""PatioOrigem"",
												IVA.PATIO_DESTINO                                                           AS ""PatioDestino"",
												CN.CD_DSC_PT                                                                AS ""CondicaoUso"",
												SI.SI_DSC_PT                                                                AS ""Situacao"",
												LO.LO_DSC_PT                                                                AS ""Local"",
												RE.RP_DSC_PT                                                                AS ""Responsavel"",
												CG.SH_DSC_PT                                                                AS ""Lotacao"",
												IT.TB_DSC_PT                                                                AS ""Intercambio"",
														(SELECT CASE 
															WHEN COUNT(1) > 1 THEN 'Encontrado mais de um estado futuro'
															WHEN COUNT(1) = 1 THEN 'Estado Futuro OK'
															ELSE 'Estado nao permitido'
														  END
												   FROM   MUDANCA_QEVG MQ,
														  QD_ESTADO_VAGAO QEV1,
														  LOCALIZACAO LO1,
														  ELEMENTO_VIA EV,
														  AREA_OPERACIONAL AO1,
														  AREA_OPERACIONAL AO2
												   WHERE  QEV1.QS_IDT_QDS = MQ.QS_IDT_QDS_FUT
												   AND    QEV1.LO_IDT_LCL = LO1.LO_IDT_LCL
												   AND    EV.AO_ID_AO = AO2.AO_ID_AO 
												   AND    AO1.AO_ID_AO = AO2.AO_ID_AO_INF 
												   AND    MQ.QS_IDT_QDS_ATU = EV.QS_IDT_QDS
												   AND    AO1.AO_COD_AOP = IVA.PATIO_DESTINO
												   AND    MQ.EN_ID_EVT = 35 /* Evento Voador */
												   AND    EV.EV_COD_ELV = 'L999'
												   AND    LO1.LO_COD_LCL = (DECODE(AO2.AO_IND_PCR,'S','LPT', 
																			DECODE(AO2.AO_IND_OFC,'S','LOF', 
																			DECODE(AO2.AO_IND_PMN,'S','LPT', 
																			DECODE(AO2.AO_TRM_CLI,'S','LCL'))))))           AS ""EstadoFuturo""
												FROM (
												  SELECT 
													VG.VG_ID_VG,
													VG.VG_COD_VAG,
													AOM.AO_COD_AOP,
													EV.EV_COD_ELV,
													NULL TR_PFX_TRM,
													'P' AS TIPO_LOCAL
												  FROM VAGAO VG
												  INNER JOIN VAGAO_PATIO_VIG VPV ON (VG.VG_ID_VG = VPV.VG_ID_VG)
												  INNER JOIN AREA_OPERACIONAL AOP ON (VPV.AO_ID_AO = AOP.AO_ID_AO)
												  INNER JOIN AREA_OPERACIONAL AOM ON (AOM.AO_ID_AO = AOP.AO_ID_AO_INF)
												  INNER JOIN ELEMENTO_VIA EV ON (VPV.EV_ID_ELV = EV.EV_ID_ELV)
												  UNION ALL
												  SELECT 
													VG.VG_ID_VG,
													VG.VG_COD_VAG,
													NULL AO_COD_AOP,
													NULL EV_COD_ELV,
													TR.TR_PFX_TRM,
													'T' AS TIPO_LOCAL
												  FROM VAGAO VG
												  INNER JOIN COMPVAGAO_VIG CVV ON (VG.VG_ID_VG = CVV.VG_ID_VG)
												  INNER JOIN COMPOSICAO CMP ON (CVV.CP_ID_CPS = CMP.CP_ID_CPS)
												  INNER JOIN TREM TR ON (CMP.TR_ID_TRM = TR.TR_ID_TRM)
												  UNION ALL
												  SELECT 
													VG.VG_ID_VG,
													VG.VG_COD_VAG,
													NULL AO_COD_AOP,
													NULL EV_COD_ELV,
													NULL TR_PFX_TRM,
													'I' AS TIPO_LOCAL
												  FROM VAGAO VG
												  INNER JOIN LOCAL_VAGAO_VIG LVV ON (VG.VG_ID_VG = LVV.VG_ID_VG)
												  WHERE LVV.LO_IDT_LCL = 14 /*INTERCAMBIO*/ 
												) VAG
												INNER JOIN SOL_VOO_ATIVOS_ITEM IVA ON (IVA.ATIVO = VAG.VG_COD_VAG AND IVA.ID_TIPO_ATIVO = 3)
												INNER JOIN SOL_VOO_ATIVOS_STATUS_ITEM STA ON IVA.ID_SOL_VOO_STATUS_ITEM = STA.ID_STATUS_SOL_ITEM_VOO                        
												INNER JOIN SOL_TIPO_ATIVO TPO ON IVA.ID_TIPO_ATIVO = TPO.ID_TIPO_ATIVO
												INNER JOIN EVENTO_VAGAO_VIG EV ON (VAG.VG_ID_VG = EV.VG_ID_VG)
												INNER JOIN LOCAL_VAGAO_VIG LVV ON (VAG.VG_ID_VG = LVV.VG_ID_VG)
												INNER JOIN LOCALIZACAO LO ON (LVV.LO_IDT_LCL = LO.LO_IDT_LCL)
												INNER JOIN RESP_VAGAO_VIG RVV ON (VAG.VG_ID_VG = RVV.VG_ID_VG)
												INNER JOIN RESPONSAVEL RE ON (RVV.RP_IDT_RSP = RE.RP_IDT_RSP)
												INNER JOIN INTERC_VAGAO_VIG IVV ON (VAG.VG_ID_VG = IVV.VG_ID_VG)
												INNER JOIN INTERCAMBIO IT ON (IVV.TB_IDT_INT = IT.TB_IDT_INT)
												INNER JOIN SITUACAO_VAGAO_VIG SVV ON (VAG.VG_ID_VG = SVV.VG_ID_VG)
												INNER JOIN SITUACAO SI ON (SVV.SI_IDT_STC = SI.SI_IDT_STC)
												INNER JOIN CONDUSO_VAGAO_VIG CVV ON (VAG.VG_ID_VG = CVV.VG_ID_VG)
												INNER JOIN CONDICAO_USO CN ON (CVV.CD_IDT_CUS = CN.CD_IDT_CUS)
												INNER JOIN LOTACAO_VIG LTV ON (VAG.VG_ID_VG = LTV.VG_ID_VG)
												INNER JOIN CARGA CG ON (LTV.SH_IDT_STC = CG.SH_IDT_STC)");

            #endregion

            #region Where

            var sqlWhere = new StringBuilder();

            //ADICIONA FILTRO POR ID
            sqlWhere.AppendFormat(" AND IVA.ID_SOL_VOO_ATIVOS = {0}", id);

            // Somente pendentes
            sqlWhere.Append(" AND IVA.ID_SOL_VOO_STATUS_ITEM= 1");


            if (sqlWhere.Length > 0)
            {
                string aux = " WHERE " + sqlWhere.ToString().Substring(5);
                sql.Append(aux);
            }

            #endregion

            #region OrderBy

            // Order by (Recupera o indice e o direcionamento (ASC/DESC))
            sql.Append(" ORDER BY IVA.ID_SOL_VOO_ITEM ASC");

            #endregion

            #region Execução SQL Transformer
            using (var session = OpenSession())
            {
                var queryCount = session.CreateSQLQuery(String.Format(@"SELECT COUNT(1) FROM ({0})", sql.ToString().Replace("{ORDENACAO}", String.Empty)));
                var total = queryCount.UniqueResult<decimal>();

                return total;
            }

            #endregion
        }

        /// <summary>
        /// Busca Items Ativos VOO LOCOMOTIVA
        /// </summary>
        /// <param name="detalhesPaginacaoWeb"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public decimal BuscarItemsLocomotivasPendentes(int id)
        {
            #region Query

            var sql = new StringBuilder(@"SELECT 
													IVA.ID_SOL_VOO_ITEM                                                     AS ""IdAtivo"",
													STA.STATUS_SOL_ITEM_VOO                                                 AS ""StatusItem"",
													IVA.ATIVO                                                               AS ""Ativo"",
													TPO.TIPO_ATIVO                                                          AS ""TipoAtivo"",
													EV.EL_DAT_OCR                                                           AS ""DataEvento"",
													LOC.AO_COD_AOP                                                          AS ""PatioOrigem"",
													IVA.PATIO_DESTINO                                                       AS ""PatioDestino"",
													CN.CD_DSC_PT                                                            AS ""CondicaoUso"",
													SI.SI_DSC_PT                                                            AS ""Situacao"",
													LO.LO_DSC_PT                                                            AS ""Local"",
													RE.RP_DSC_PT                                                            AS ""Responsavel"",
													IT.TB_DSC_PT                                                            AS ""Intercambio"",
													(SELECT CASE 
															WHEN COUNT(1) > 1 THEN 'Encontrado mais de um estado futuro'
															WHEN COUNT(1) = 1 THEN 'Estado Futuro OK'
															ELSE 'Estado nao permitido'
														  END
												   FROM   MUDANCA_QELC MQ,
														  QD_ESTADO_LOCO QE,
														  LOCALIZACAO LO1,
														  ELEMENTO_VIA EV,
														  AREA_OPERACIONAL AO1,
														  AREA_OPERACIONAL AO2
												   WHERE  QE.QT_IDT_QEL = MQ.QT_IDT_QEL_FUT
												   AND    QE.LO_IDT_LCL = LO1.LO_IDT_LCL
												   AND    EV.AO_ID_AO = AO2.AO_ID_AO 
												   AND    AO1.AO_ID_AO = AO2.AO_ID_AO_INF 
												   AND    MQ.QT_IDT_QEL_ATU = EV.QT_IDT_QEL
												   AND    AO1.AO_COD_AOP = IVA.PATIO_DESTINO
												   AND    MQ.EN_ID_EVT = 35 /* Evento Voador */
												   AND    EV.EV_COD_ELV = 'L999'
												   AND    LO1.LO_COD_LCL = (DECODE(AO2.AO_IND_PCR,'S','LPT', 
																			DECODE(AO2.AO_IND_OFC,'S','LOF', 
																			DECODE(AO2.AO_IND_PMN,'S','LPT', 
																			DECODE(AO2.AO_TRM_CLI,'S','LCL'))))))           AS ""EstadoFuturo""
												FROM (
												  SELECT 
													LC.LC_ID_LC,
													LC.LC_COD_LOC,
													AOP.AO_COD_AOP,
													EV.EV_COD_ELV,
													NULL TR_PFX_TRM,
													'P' AS TIPO_LOCAL
												  FROM LOCOMOTIVA LC
												  INNER JOIN LOCO_PATIO_VIG LPV ON (LC.LC_ID_LC = LPV.LC_ID_LC)
												  INNER JOIN AREA_OPERACIONAL AOP ON (LPV.AO_ID_AO = AOP.AO_ID_AO)
												  INNER JOIN AREA_OPERACIONAL AOM ON (AOP.AO_ID_AO_INF = AOM.AO_ID_AO)
												  INNER JOIN ELEMENTO_VIA EV ON (LPV.EV_ID_ELV = EV.EV_ID_ELV)
												  UNION ALL
												  SELECT 
													LC.LC_ID_LC,
													LC.LC_COD_LOC,
													NULL AO_COD_AOP,
													NULL EV_COD_ELV,
													TR.TR_PFX_TRM,
													'T' AS TIPO_LOCAL
												  FROM LOCOMOTIVA LC
												  INNER JOIN COMP_LOCOMOT_VIG CLV ON (LC.LC_ID_LC = CLV.LC_ID_LC)
												  INNER JOIN COMPOSICAO CMP ON (CLV.CP_ID_CPS = CMP.CP_ID_CPS)
												  INNER JOIN TREM TR ON (CMP.TR_ID_TRM = TR.TR_ID_TRM)
												  UNION ALL
												  SELECT 
													LC.LC_ID_LC,
													LC.LC_COD_LOC,
													NULL AO_COD_AOP,
													NULL EV_COD_ELV,
													NULL TR_PFX_TRM,
													'I' AS TIPO_LOCAL
												  FROM LOCOMOTIVA LC
												  INNER JOIN LOCAL_LOCO_VIG LLV ON (LC.LC_ID_LC = LLV.LC_ID_LC)
												  WHERE LLV.LO_IDT_LCL = 14 /*INTERCAMBIO*/ 
												) LOC
												INNER JOIN SOL_VOO_ATIVOS_ITEM IVA ON (IVA.ID_ATIVO = LOC.LC_ID_LC AND IVA.ID_TIPO_ATIVO = 2)
												INNER JOIN SOL_VOO_ATIVOS_STATUS_ITEM STA ON (IVA.ID_SOL_VOO_STATUS_ITEM = STA.ID_STATUS_SOL_ITEM_VOO)
												INNER JOIN SOL_TIPO_ATIVO TPO ON (IVA.ID_TIPO_ATIVO = TPO.ID_TIPO_ATIVO)
												INNER JOIN EVENTO_LOCO_VIG EV ON (LOC.LC_ID_LC = EV.LC_ID_LC)
												INNER JOIN LOCAL_LOCO_VIG LLV ON (LOC.LC_ID_LC = LLV.LC_ID_LC)
												INNER JOIN LOCALIZACAO LO ON (LLV.LO_IDT_LCL = LO.LO_IDT_LCL)
												INNER JOIN RESP_LOCO_VIG RLV ON (LOC.LC_ID_LC = RLV.LC_ID_LC)
												INNER JOIN RESPONSAVEL RE ON (RLV.RP_IDT_RSP = RE.RP_IDT_RSP)
												INNER JOIN INTERC_LOCO_VIG ILV ON (LOC.LC_ID_LC = ILV.LC_ID_LC)
												INNER JOIN INTERCAMBIO IT ON (ILV.TB_IDT_INT = IT.TB_IDT_INT)
												INNER JOIN SITUACAO_LOCO_VIG SLV ON (LOC.LC_ID_LC = SLV.LC_ID_LC)
												INNER JOIN SITUACAO SI ON (SLV.SI_IDT_STC = SI.SI_IDT_STC)
												INNER JOIN CONDUSO_LOCO_VIG CLV ON (LOC.LC_ID_LC = CLV.LC_ID_LC)
												INNER JOIN CONDICAO_USO CN ON (CLV.CD_IDT_CUS = CN.CD_IDT_CUS)");

            #endregion

            #region Where

            var sqlWhere = new StringBuilder();

            //ADICIONA FILTRO POR ID
            sqlWhere.AppendFormat(" AND IVA.ID_SOL_VOO_ATIVOS = {0}", id);

            // Somente pendentes
            sqlWhere.Append(" AND IVA.ID_SOL_VOO_STATUS_ITEM= 1");


            if (sqlWhere.Length > 0)
            {
                string aux = " WHERE " + sqlWhere.ToString().Substring(5);
                sql.Append(aux);
            }

            #endregion

            #region OrderBy

            // Order by (Recupera o indice e o direcionamento (ASC/DESC))
            sql.Append(" ORDER BY IVA.ID_SOL_VOO_ITEM ASC");

            #endregion

            #region Execução SQL Transformer
            using (var session = OpenSession())
            {
                var queryCount = session.CreateSQLQuery(String.Format(@"SELECT COUNT(1) FROM ({0})", sql.ToString().Replace("{ORDENACAO}", String.Empty)));
                var total = queryCount.UniqueResult<decimal>();

                return total;
            }

            #endregion
        }

        /// <summary>
        /// Busca Items Ativos VOO EOT
        /// </summary>
        /// <param name="detalhesPaginacaoWeb"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public decimal BuscarItemsEotsPendentes(int id)
        {
            #region Query

            var sql = new StringBuilder(@"SELECT 
												IVA.ID_SOL_VOO_ITEM                                 AS ""IdAtivo"",
												STA.STATUS_SOL_ITEM_VOO                             AS ""StatusItem"",
												IVA.ATIVO                                           AS ""Ativo"",
												TPO.TIPO_ATIVO                                      AS ""TipoAtivo"",
												NVL(DT_CHG, DT_UTL)                                 AS ""DataEvento"",
												CASE IND_SIT
												WHEN 'A' THEN 'Avariado'
												WHEN 'D' THEN 'Disponivel'
												WHEN 'M' THEN 'Manutencao'
												WHEN 'I' THEN 'Inativo'
											  END                                                   AS ""Situacao"",
											  CASE
												WHEN TX_TREM IS NULL
												THEN 'Em Pátio'
												ELSE 'Em Trem'
											  END                                                   AS ""Local"",
											  (CASE 
												WHEN TX_TREM IS NULL THEN 'Estado Futuro OK'
												ELSE 'Estado nao permitido'
											   END)                                                 AS ""EstadoFuturo""
											FROM
											  (SELECT 
												E.EQ_DSC_RSM TX_EQUIP,    
												E.EQ_ID_EQPT ID_EQUIP,
												EP.AE_COD_EQP COD_EQUIP,
												AO.AO_COD_AOP COD_ESTACAO,
												TO_CHAR(NULL) TX_TREM,
												EP.AE_DAT_CHG DT_CHG,
												EP.AE_DAT_UTL DT_UTL,
												E.EQ_IND_STC IND_SIT
											  FROM TRANSLOGIC.EQPTO_PATIO_VIG ep,
												TRANSLOGIC.EQUIPAMENTO e,
												TRANSLOGIC.AREA_OPERACIONAL ao
											  WHERE ao.AO_ID_AO     = ep.AO_ID_AO
											  AND e.EQ_DSC_RSM NOT IN ( 'CBL' , 'OBC' , 'Sincar' )
											  AND e.EQ_ID_EQPT      = ep.EQ_ID_EQPT
											  UNION ALL
											  SELECT 
												E.EQ_DSC_RSM,    
												E.EQ_ID_EQPT,
												EP.AE_COD_EQP,
												AO.AO_COD_AOP,
												T.TR_PFX_TRM,
												EP.AE_DAT_CHG,
												EP.AE_DAT_UTL,
												E.EQ_IND_STC IND_SIT
											  FROM TRANSLOGIC.EQPTO_PATIO ep,
												TRANSLOGIC.EQUIPAMENTO e,
												TRANSLOGIC.AREA_OPERACIONAL ao,
												(SELECT *
												FROM TRANSLOGIC.TREM
												WHERE tr_pfx_trm NOT LIKE 'N%'
												AND tr_pfx_trm NOT LIKE 'E%'
												AND tr_pfx_trm NOT LIKE 'W%'
												AND tr_pfx_trm NOT LIKE 'S%'
												AND tr_pfx_trm NOT LIKE 'V%'
												AND tr_pfx_trm NOT LIKE 'A%'
												) t,
												TRANSLOGIC.ORDEMFORMACAO os,
												(SELECT ep1.EQ_ID_EQPT IDT,
												  MAX(ep1.AE_DAT_UTL) DAT
												FROM TRANSLOGIC.EQPTO_PATIO ep1
												GROUP BY ep1.EQ_ID_EQPT
												) eqcomp
											  WHERE ao.AO_ID_AO     = ep.AO_ID_AO
											  AND e.EQ_DSC_RSM NOT IN ( 'CBL' , 'OBC' , 'Sincar' )
											  AND e.EQ_IND_LOC      = 'T'
											  AND e.EQ_ID_EQPT      = ep.EQ_ID_EQPT
											  AND t.TR_ID_TRM       = ep.TR_ID_TRM
											  AND os.OF_ID_OSV      = t.OF_ID_OSV
											  AND eqcomp.IDT        = ep.EQ_ID_EQPT
											  AND eqcomp.DAT        = ep.AE_DAT_UTL
											  ) EQP
											INNER JOIN SOL_VOO_ATIVOS_ITEM IVA ON (IVA.ID_ATIVO = EQP.ID_EQUIP AND IVA.ID_TIPO_ATIVO = 1)
											INNER JOIN SOL_VOO_ATIVOS_STATUS_ITEM STA ON IVA.ID_SOL_VOO_STATUS_ITEM = STA.ID_STATUS_SOL_ITEM_VOO                        
											INNER JOIN SOL_TIPO_ATIVO TPO ON IVA.ID_TIPO_ATIVO = TPO.ID_TIPO_ATIVO");

            #endregion

            #region Where

            var sqlWhere = new StringBuilder();

            //ADICIONA FILTRO POR ID
            sqlWhere.AppendFormat(" AND IVA.ID_SOL_VOO_ATIVOS = {0}", id);

            // Somente pendentes
            sqlWhere.Append(" AND IVA.ID_SOL_VOO_STATUS_ITEM= 1");

            if (sqlWhere.Length > 0)
            {
                string aux = " WHERE " + sqlWhere.ToString().Substring(5);
                sql.Append(aux);
            }

            #endregion

            #region OrderBy

            // Order by (Recupera o indice e o direcionamento (ASC/DESC))
            sql.Append(" ORDER BY IVA.ID_SOL_VOO_ITEM ASC");

            #endregion

            #region Execução SQL Transformer
            using (var session = OpenSession())
            {
                var queryCount = session.CreateSQLQuery(String.Format(@"SELECT COUNT(1) FROM ({0})", sql.ToString().Replace("{ORDENACAO}", String.Empty)));
                var total = queryCount.UniqueResult<decimal>();

                return total;
            }

            #endregion
        }

        /// <summary>
        /// Buscar a data do último evento do ativo VAGAO
        /// </summary>
        /// <param name="ativo"></param>
        /// <returns></returns>
        public DateTime BuscarDataUltimoEventoVagao(string ativo)
        {    
            #region Query

            var sql = new StringBuilder(@" SELECT MAX(EVV.EA_DTO_EVG) AS DT  FROM   EVENTO_VAGAO_VIG EVV ");
           
            sql.AppendFormat(" WHERE  EVV.VG_ID_VG = (SELECT VG_ID_VG FROM VAGAO VG WHERE  VG.VG_COD_VAG = '{0}')", ativo);

            #endregion

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());
                var data = query.UniqueResult<DateTime>();

                return data;
            }
        }

        
        #endregion

        #region Métodos Comuns
        /// <summary>
        /// Adicionar um novo <see cref="OracleParameter"/> ao <see cref="OracleCommand"/>
        /// </summary>
        /// <param name="command"><see cref="OracleCommand"/> onde o <see cref="OracleParameter"/> será adicionado</param>
        /// <param name="nome">Nome do parâmetro</param>
        /// <param name="tipo">Tipo do parâmetro <see cref="OracleType"/></param>
        /// <param name="valor">Valor de entrada</param>
        /// <param name="direcao">Direção <see cref="ParameterDirection"/></param>
        private void AddParameter(OracleCommand command, string nome, OracleType tipo, object valor, ParameterDirection direcao)
        {
            OracleParameter parameter = new OracleParameter();
            parameter.ParameterName = nome;
            parameter.OracleType = tipo;
            parameter.Direction = direcao;
            parameter.Value = DBNull.Value;
            if (valor != null)
            {
                parameter.Value = valor;
            }

            command.Parameters.Add(parameter);
        }

        /// <summary>
        /// Executar uma query retornando resultado único SolicitacaoVooAtivoItemPkgDto
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        private SolicitacaoVooAtivoItemPkgDto ExecuteQuerieUniqueResult(StringBuilder sql)
        {
            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());
                query.SetResultTransformer(Transformers.AliasToBean<SolicitacaoVooAtivoItemPkgDto>());

                var result = query.UniqueResult<SolicitacaoVooAtivoItemPkgDto>();
                return result;
            }
        }

        /// <summary>
        /// Executar as queries das pesquisas
        /// </summary>
        /// <param name="detalhesPaginacaoWeb"></param>
        /// <param name="sql"></param>
        /// <returns></returns>
        private ResultadoPaginado<SolicitacaoVooAtivoItemDto> ExecuteQueries(DetalhesPaginacaoWeb detalhesPaginacaoWeb, StringBuilder sql)
        {
            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());
                var queryCount = session.CreateSQLQuery(String.Format(@"SELECT COUNT(1) FROM ({0})", sql.ToString().Replace("{ORDENACAO}", String.Empty)));


                query.SetResultTransformer(Transformers.AliasToBean<SolicitacaoVooAtivoItemDto>());

                if (detalhesPaginacaoWeb.Limite != 0)
                {
                    query.SetFirstResult(detalhesPaginacaoWeb.Inicio ?? 0);
                    query.SetMaxResults(detalhesPaginacaoWeb.Limite ?? DetalhesPaginacao.LimitePadrao);
                }

                var total = queryCount.UniqueResult<decimal>();
                var itens = query.List<SolicitacaoVooAtivoItemDto>();

                var result = new ResultadoPaginado<SolicitacaoVooAtivoItemDto>
                {
                    Items = itens,
                    Total = (long)total
                };

                return result;
            }
        }

        /// <summary>
        /// Formatar a string de retorno de erros de PGKs
        /// </summary>
        /// <param name="xmlSaida"></param>
        /// <returns></returns>
        private string FormataXMLSaida(string xmlSaida)
        {
            xmlSaida = xmlSaida.Replace("PKG_EVENTO.EV_VAGAO:", "");
            xmlSaida = xmlSaida.Replace("PKG_EVENTO.EV_VAGAO.ERRO:", "");
            xmlSaida = xmlSaida.Replace("PKG_EVENTO.EV_VAGAO.ERRO_RAISE:", "");
            xmlSaida = xmlSaida.Replace("PKG_EVENTO.EV_VAGAO_VALIDA_OBS:", "");
            xmlSaida = xmlSaida.Replace("PKG_EVENTO.EV_VAGAO_OBSERVACAO:", "");
            xmlSaida = xmlSaida.Replace("PKG_EVENTO.EV_LOCO:", "");
            xmlSaida = xmlSaida.Replace("PKG_EVENTO.EV_LOCO_VALIDA_OBS:", "");
            xmlSaida = xmlSaida.Replace("PKG_EVENTO.EV_LOCO_OBSERVACAO:", "");
            xmlSaida = xmlSaida.Replace("SP_REALIZAR_VOO_VG:", "");
            xmlSaida = xmlSaida.Replace("PKG_MANOBRA_PORTOFER SP_REALIZAR_MANOBRA:", "");
            xmlSaida = xmlSaida.Replace("SP_REALIAR_VOO_VG:", "");
            xmlSaida = xmlSaida.Replace("SP_REALIZAR_VOO_VG:", "");
            xmlSaida = xmlSaida.Replace("SP_REALIZAR_VOO_VAGAO:", "");
            xmlSaida = xmlSaida.Replace("SP_VOAR_VAGAO:", "");
            xmlSaida = xmlSaida.Replace("SP_REALIZAR_VOO_VG:", "");
            xmlSaida = xmlSaida.Replace("SP_MANOBRAR_VG_PORTOFER:", "");
            xmlSaida = xmlSaida.Replace("SP_FILTRAR_XML_ERRO:", "");
            xmlSaida = xmlSaida.Replace("PKG_MANOBRA_PORTOFER SP_ABRIR_VAGAOPATIO:", "");
            xmlSaida = xmlSaida.Replace("PKG_MANOBRA_PORTOFER SP_ORDENA_LINHA:", "");
            xmlSaida = xmlSaida.Replace("PKG_MANOBRA_PORTOFER SP_ORDENA_LINHA_DESTINO:", "");
            xmlSaida = xmlSaida.Replace("PKG_MANOBRA_PORTOFER SP_ABRIR_LOCOPATIO:", "");
            xmlSaida = xmlSaida.Replace("SP_REALIAR_VOO_LC:", "");
            xmlSaida = xmlSaida.Replace("SP_REALIZAR_VOO_LC:", "");
            xmlSaida = xmlSaida.Replace("SP_MANOBRAR_VG_PORTOFER:", "");

            xmlSaida = xmlSaida.Replace("SQLCODE: 0", "");
            xmlSaida = xmlSaida.Replace("SQLCODE: 1", "");
            xmlSaida = xmlSaida.Replace("SQLCODE: -1", "");

            if (xmlSaida.IndexOf("#") > 0)
                xmlSaida = xmlSaida.Replace(xmlSaida.Substring(0, xmlSaida.IndexOf("#") + 1), "");


            if (xmlSaida.IndexOf("</ERRO>") > 0)
                xmlSaida = xmlSaida.Replace(xmlSaida.Substring(xmlSaida.IndexOf("</ERRO>"), (xmlSaida.Length - xmlSaida.IndexOf("</ERRO>"))), "");

            return xmlSaida;
        }

        #endregion
    }
}