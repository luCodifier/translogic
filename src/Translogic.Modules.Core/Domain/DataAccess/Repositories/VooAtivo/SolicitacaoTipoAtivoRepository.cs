﻿

namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.VooAtivo
{
    using System.Collections.Generic;
    using Translogic.Modules.Core.Domain.Model.VooAtivo;
    using Translogic.Modules.Core.Util;
    using System.Text;
    using ALL.Core.AcessoDados;
    using NHibernate.Transform;
    using System;

    public class SolicitacaoTipoAtivoRepository : NHRepository<SolicitacaoTipoAtivo, int>, ISolicitacaoTipoAtivoRepository
    {
    }
}