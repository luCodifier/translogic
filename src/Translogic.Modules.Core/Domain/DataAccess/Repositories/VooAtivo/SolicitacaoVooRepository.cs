﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.VooAtivo
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using ALL.Core.AcessoDados;
    using NHibernate;
    using NHibernate.Transform;
    using NHibernate.Linq;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Modules.Core.Domain.Model.OSLimpezaVagao.Repositories;
    using Translogic.Modules.Core.Domain.Model.OSLimpezaVagao;
    using Translogic.Core.Infrastructure.Web;
    using ALL.Core.Dominio;
    using System.Data.OracleClient;
    using System.Data;
    using Translogic.Modules.Core.Domain.Model.VooAtivo;

    public class SolicitacaoVooRepository : NHRepository<SolicitacaoVooAtivo, int>, ISolicitacaoVooRepository 
    {
        public ResultadoPaginado<SolicitacaoVooAtivosAvaliarDto> BuscarAtivosVooAvaliar(DetalhesPaginacaoWeb detalhesPaginacaoWeb, int? id, DateTime? dtPeriodoInicio, DateTime? dtPeriodoFinal, string solicitante, int? idStatusChamado, int? idTipoSolicitacao)
        {

            var sql = new StringBuilder(@"SELECT
                                        A.ID_SOL_VOO            as     ""Id"",
                                        A.DT_SOLICITACAO        as ""dtSolicitacao"",
                                        C.TIPO_ATIVO            as ""TipoAtivo"",
                                        A.SOLICITANTE           as ""Solicitante"",
                                        A.PATIO_SOLICITANTE         as  ""AreaSolicitante"",
                                        A.MOTIVO                as ""Motivo"",
                                        B.STATUS_SOL_VOO        as ""Status""
                                            FROM SOL_VOO_ATIVOS A
                                                INNER JOIN SOL_VOO_ATIVOS_STATUS B ON A.ID_SOL_VOO_STATUS = B.ID_STATUS_SOL_VOO                        
                                                INNER JOIN SOL_TIPO_ATIVO C ON A.ID_TIPO_ATIVO = C.ID_TIPO_ATIVO"
                                        );

            #region Where

            var sqlWhere = new StringBuilder();

            //ADICIONA FILTROS
            
            //ID
            if (id != null)
            {
                sqlWhere.AppendFormat(" AND A.ID_SOL_VOO = {0}", id);
            }

            //Periodo entre Periodo Inicial e Periodo Final
            if (dtPeriodoInicio.HasValue && dtPeriodoFinal.HasValue)
            {
                sqlWhere.AppendFormat(" AND A.DT_SOLICITACAO BETWEEN TO_DATE('{0} 00:00:00','DD/MM/YYYY HH24:MI:SS') AND TO_DATE('{1} 23:59:59','DD/MM/YYYY HH24:MI:SS') ", dtPeriodoInicio.Value.ToString("dd/MM/yyyy"), dtPeriodoFinal.Value.ToString("dd/MM/yyyy"));
            }
            //Periodo Inicial
            else if (dtPeriodoInicio.HasValue)
            {
                sqlWhere.AppendFormat(" AND A.DT_SOLICITACAO >= TO_DATE('{0} 00:00:00', 'DD/MM/YYYY HH24:MI:SS')", dtPeriodoInicio.Value.ToString("dd/MM/yyyy"));            
            }
            //Periodo Final
            else if (dtPeriodoFinal.HasValue)
            {
                sqlWhere.AppendFormat(" AND A.DT_SOLICITACAO <= TO_DATE('{0} 23:59:59', 'DD/MM/YYYY HH24:MI:SS')", dtPeriodoFinal.Value.ToString("dd/MM/yyyy"));      
            }
            //Solicitante
            if (!string.IsNullOrEmpty(solicitante))
            {
                sqlWhere.AppendFormat(" AND UPPER(A.SOLICITANTE) LIKE '%{0}%'", solicitante.ToUpper());

            }
            //Status
            if (idStatusChamado > 0)
            {
                sqlWhere.AppendFormat(" AND B.ID_STATUS_SOL_VOO = {0}", idStatusChamado);
            }
            //Tipo de Solicitação
            if (idTipoSolicitacao>0 )
            {
                sqlWhere.AppendFormat(" AND C.ID_TIPO_ATIVO = {0}", idTipoSolicitacao);
            }

            if (sqlWhere.Length > 0)
            {
                string aux = " WHERE " + sqlWhere.ToString().Substring(5);
                sql.Append(aux);
            }

            #endregion


            #region OrderBy

            // Order by (Recupera o indice e o direcionamento (ASC/DESC))
            sql.Append(" ORDER BY A.ID_SOL_VOO ASC");

            #endregion

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());
                var queryCount = session.CreateSQLQuery(String.Format(@"SELECT COUNT(1) FROM ({0})", sql.ToString().Replace("{ORDENACAO}", String.Empty)));


                query.SetResultTransformer(Transformers.AliasToBean<SolicitacaoVooAtivosAvaliarDto>());

                if (detalhesPaginacaoWeb.Limite != 0)
                {
                    query.SetFirstResult(detalhesPaginacaoWeb.Inicio ?? 0);
                    query.SetMaxResults(detalhesPaginacaoWeb.Limite ?? DetalhesPaginacao.LimitePadrao);
                }

                var total = queryCount.UniqueResult<decimal>();
                var itens = query.List<SolicitacaoVooAtivosAvaliarDto>();

                var result = new ResultadoPaginado<SolicitacaoVooAtivosAvaliarDto>
                {
                    Items = itens,
                    Total = (long)total
                };

                return result;
            }


        }


        public ResultadoPaginado<SolicitacaoVooAtivosAvaliarDto> BuscarAtivosVooAvaliarPorID(DetalhesPaginacaoWeb detalhesPaginacaoWeb, int? id)
        {
            var sql = new StringBuilder(@"SELECT SELECT 
                                                v.JUSTIFICATIVA as ""Justificxativa"",
                                                v.MOTIVO as ""Motivo"",
                                                v.PATIO_DESTINO as ""PatioDestino"",
                                                v.LINHA_DESTINO as ""LinhaDestino"",
                                                v.SOLICITANTE as ""Solicitante"",
                                                v.DT_CHEGADA as ""dtChegada"",
                                                v.AREA_RESP as ""AreaResponsavel"",
                                                v.JUSTIFICATIVA_CCO as ""JustificativaCCO"",
                                                v.OBS_CCO as ""ObservacaoCCO"",
                                                v.DT_SOLICITACAO as ""dtSolicitacao""
                                            FROM SOL_VOO_ATIVOS v 
                                                WHERE ID_SOL_VOO = 1; ");

            return ExecuteQueries(detalhesPaginacaoWeb, sql);
        }

        /// <summary>
        /// Executar as queries das pesquisas
        /// </summary>
        /// <param name="detalhesPaginacaoWeb"></param>
        /// <param name="sql"></param>
        /// <returns></returns>
        private ResultadoPaginado<SolicitacaoVooAtivosAvaliarDto> ExecuteQueries(DetalhesPaginacaoWeb detalhesPaginacaoWeb, StringBuilder sql)
        {
            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());
                var queryCount = session.CreateSQLQuery(String.Format(@"SELECT COUNT(1) FROM ({0})", sql.ToString().Replace("{ORDENACAO}", String.Empty)));


                query.SetResultTransformer(Transformers.AliasToBean<SolicitacaoVooAtivosAvaliarDto>());

                if (detalhesPaginacaoWeb.Limite != 0)
                {
                    query.SetFirstResult(detalhesPaginacaoWeb.Inicio ?? 0);
                    query.SetMaxResults(detalhesPaginacaoWeb.Limite ?? DetalhesPaginacao.LimitePadrao);
                }

                var total = queryCount.UniqueResult<decimal>();
                var itens = query.List<SolicitacaoVooAtivosAvaliarDto>();

                var result = new ResultadoPaginado<SolicitacaoVooAtivosAvaliarDto>
                {
                    Items = itens,
                    Total = (long)total
                };

                return result;
            }
        }
    }
}