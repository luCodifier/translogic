﻿
namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.VooAtivo
{
    using System.Collections.Generic;
    using Translogic.Modules.Core.Domain.Model.VooAtivo;
    using Translogic.Modules.Core.Util;
    using System.Text;
    using ALL.Core.AcessoDados;
    using NHibernate.Transform;
    using System;

    public class VooMotivoRepository : NHRepository<SolicitacaoVooMotivo, Decimal>, IVooMotivoRepository
    {
        public IList<SolicitacaoVooMotivo> BuscarMotivos(EnumTipoAtivo tipo)
        {
            var sql = new StringBuilder(@"SELECT ");

            switch (tipo)
            {
                case EnumTipoAtivo.Vagao:
                    {
                        sql.Append(@" ELM_IDT_ELM AS ""Id"", ELM_MOT_ELM AS ""Descricao"" FROM EVENTO_MOTIVO WHERE ELM_TPO_ELM = 'VAGAO_VOADOR' AND ELM_IND_ATV = 'S' ORDER BY ELM_MOT_ELM");
                        break;
                    }
                case EnumTipoAtivo.VagaoRefaturamento:
                    {
                        sql.Append(@" ELM_IDT_ELM AS ""Id"", ELM_MOT_ELM AS ""Descricao"" FROM EVENTO_MOTIVO WHERE ELM_TPO_ELM = 'VAGAO_VOADOR' AND ELM_IND_ATV = 'S' ORDER BY ELM_MOT_ELM");
                        break;
                    }
                case EnumTipoAtivo.Locomotiva:
                    {
                        sql.Append(@" ELM_IDT_ELM AS ""Id"", ELM_MOT_ELM AS ""Descricao"" FROM EVENTO_LOCO_VOADOR_MOTIVO  WHERE ELM_IND_ATV = 'S'  ORDER BY ELM_MOT_ELM ");
                        break;
                    }
                case EnumTipoAtivo.Eot:
                    {
                        sql.Append(@" ELM_IDT_ELM AS ""Id"", ELM_MOT_ELM AS ""Descricao"" FROM EVENTO_MOTIVO WHERE ELM_TPO_ELM = 'EQUIP_VOADOR' AND ELM_IND_ATV = 'S' ORDER BY ELM_MOT_ELM");
                        break;
                    }
            }

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());

                query.SetResultTransformer(Transformers.AliasToBean<SolicitacaoVooMotivo>());

                var result = query.List<SolicitacaoVooMotivo>();

                return result;
            }
        }

        /// <summary>
        /// Busca o ID da descrição por motivo
        /// </summary>
        /// <param>tipo</param>
        /// <param>motivo</param>
        /// <returns>Retorna retorno de execução de PKG.</returns>
        public decimal BuscaIDPorDescricaoMotivo(EnumTipoAtivo tipo, string motivo)
        {
            #region Query

            var sql = new StringBuilder(@"SELECT ");

            switch (tipo)
            {
                case EnumTipoAtivo.Vagao:
                    {
                        sql.Append(@" ELM_IDT_ELM FROM EVENTO_MOTIVO WHERE ELM_TPO_ELM = 'VAGAO_VOADOR' AND ELM_IND_ATV = 'S' AND ELM_MOT_ELM = '" + motivo + "'");
                        break;
                    }
                case EnumTipoAtivo.VagaoRefaturamento:
                    {
                        sql.Append(@" ELM_IDT_ELM FROM EVENTO_MOTIVO WHERE ELM_TPO_ELM = 'VAGAO_VOADOR' AND ELM_IND_ATV = 'S' AND ELM_MOT_ELM = '" + motivo + "'");
                        break;
                    }
                case EnumTipoAtivo.Locomotiva:
                    {
                        sql.Append(@" ELM_IDT_ELM FROM EVENTO_LOCO_VOADOR_MOTIVO  WHERE ELM_IND_ATV = 'S' AND ELM_MOT_ELM = '" + motivo + "'");
                        break;
                    }
                case EnumTipoAtivo.Eot:
                    {
                        sql.Append(@" ELM_IDT_ELM FROM EVENTO_MOTIVO WHERE ELM_TPO_ELM = 'EQUIP_VOADOR' AND ELM_IND_ATV = 'S' AND ELM_MOT_ELM = '" + motivo + "'");
                        break;
                    }
            }

            #endregion

            #region Execução SQL Transformer
            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());

                var result = query.UniqueResult<decimal>();

                return result;
            }

            #endregion
        }
    }
}