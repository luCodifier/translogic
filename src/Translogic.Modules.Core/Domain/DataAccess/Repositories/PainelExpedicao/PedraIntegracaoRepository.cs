﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.PainelExpedicao
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using ALL.Core.AcessoDados;
    using ALL.Core.Dominio;
    using NHibernate;
    using NHibernate.Transform;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Modules.Core.Domain.Model.PainelExpedicao;
    using Translogic.Modules.Core.Domain.Model.PainelExpedicao.Parametrizacao;
    using Translogic.Modules.Core.Domain.Model.PainelExpedicao.Parametrizacao.Enums;
    using Translogic.Modules.Core.Domain.Model.PainelExpedicao.Repositories;

    public class PedraIntegracaoRepository : NHRepository<PedraRealizado, int>, IPedraIntegracaoRepository
    {

//        public IList<ConsultaPedraRealizadoDto> ObterPainelExpedicaoPedra(string operacao, string regiao, string dataPedra, string uf, string origem, string estacaoFaturamento, string segmento, string terminal, string ocultarPedraZerada)
//        {
//            var parameters = new List<Action<IQuery>>();

//            var queryPrincipal = new StringBuilder(@"
//SELECT 
//  P.ID                                              AS ""PedraRealizadoId"",
//  T.DT_PEDRA                                        AS ""DataPedra"",
//  OPE.NOME                                          AS ""Operacao"",
//  OPE.ID                                            AS ""OperacaoId"",
//  REG.NOME                                          AS ""Regiao"",
//  PEO.REGIAO_ID                                     AS ""RegiaoId"",
//  T.REUNIAO_ID                                      AS ""ReuniaoId"",
//  T.ID_FLUXO                                        AS ""FluxoId"",
//  AOF.AO_DSC_AOP                                    AS ""EstacaoFaturamento"",
//  AOF.AO_ID_AO                                      AS ""EstacaoFaturamentoId"",
//  AO.AO_COD_AOP                                     AS ""EstacaoOrigem"",
//  AO.AO_ID_AO                                       AS ""EstacaoOrigemId"",
//  TERM.AO_COD_AOP		                            AS ""CodigoLinha"",
//  EXP.STRNOMERESUMIDO	                            AS ""Terminal"",
//  EXP.EP_ID_EMP                                     AS ""TerminalId"",
//  COALESCE(F.FX_COD_SEGMENTO, '')                   AS ""Segmento"",
//  T.PEDRA				                            AS ""PedraSade"",
//  COALESCE(P.PEDRA_EDITADO, 0)		                AS ""PedraEditado"",
//  T.REALIZADO       		                        AS ""Realizado"",
//  COALESCE(P.STATUS_EDITADO, 'EM CARREGAMENTO')    	AS ""StatusEditado""
//FROM 
//  TB_SADE_REUNIAO_PRD_CARGA T
//  LEFT JOIN FLUXO_COMERCIAL F ON T.ID_FLUXO = F.FX_ID_FLX
//  LEFT JOIN EMPRESA EXP ON F.EP_ID_EMP_REM = EXP.EP_ID_EMP
//  JOIN AREA_OPERACIONAL TERM ON F.AO_ID_TER_OR = TERM.AO_ID_AO
//  LEFT JOIN MUNICIPIO MN  ON MN.MN_ID_MNC = TERM.MN_ID_MNC
//  LEFT JOIN ESTADO UF  ON UF.ES_ID_EST = MN.ES_ID_EST
//  JOIN AREA_OPERACIONAL AO ON T.ESTACAO = AO.AO_COD_AOP
//  JOIN PAINEL_EXPEDICAO_ORIGENS PEO ON AO.AO_ID_AO = PEO.ID_PE_OR_ID
//  JOIN AREA_OPERACIONAL AOF ON PEO.ID_PE_ESTACAO_FATURAMENTO_ID = AOF.AO_ID_AO
//  LEFT JOIN PE_PEDRA_REALIZADO P ON T.ID_FLUXO = P.FLUXO_ID AND T.REUNIAO_ID = P.REUNIAO_ID
//  JOIN PE_REGIAO REG ON PEO.REGIAO_ID = REG.ID
//  JOIN PE_OPERACAO OPE ON REG.OPERACAO_ID = OPE.ID
//WHERE TO_DATE(T.DT_PEDRA) = TO_DATE({filtro_dataPedra}, 'DD/MM/YYYY')
//{filtro_operacao} 
//{filtro_regiao}
//{filtro_uf}
//{filtro_origem}
//{filtro_estacaoFaturamento}
//{filtro_segmento}
//{filtro_terminal}
//{filtro_ocultarPedraZerada}
//ORDER BY 
//	OPE.NOME ASC,
//	REG.NOME ASC,
//	AOF.AO_DSC_AOP ASC,
//	AO.AO_COD_AOP ASC,
//	EXP.STRNOMERESUMIDO ASC
//");

//            dataPedra = dataPedra.ToUpper();
//            if (!string.IsNullOrWhiteSpace(dataPedra))
//            {
//                queryPrincipal.Replace("{filtro_dataPedra}", String.Format("'{0}' ", dataPedra));
//            }
//            else
//            {
//                queryPrincipal.Replace("{filtro_dataPedra}", String.Format("'{0}' ", DateTime.Now.ToString("dd/MM/yyyy")));
//            }

//            operacao = operacao.ToUpper().Trim();
//            if (!string.IsNullOrWhiteSpace(operacao) && operacao != "TODOS" && operacao != "TODAS" && operacao != "0")
//            {
//                queryPrincipal.Replace("{filtro_operacao}", "AND OPE.ID = " + operacao);
//            }
//            else
//            {
//                queryPrincipal.Replace("{filtro_operacao}", "");
//            }

//            regiao = regiao.ToUpper();
//            if (!string.IsNullOrWhiteSpace(regiao) && regiao != "TODOS" && regiao != "TODAS" && regiao != "0")
//            {
//                queryPrincipal.Replace("{filtro_regiao}", "AND REG.ID =" + regiao);
//            }
//            else
//            {
//                queryPrincipal.Replace("{filtro_regiao}", "");
//            }

//            uf = uf.ToUpper();
//            if (!string.IsNullOrWhiteSpace(uf) && uf != "TODOS" && uf != "TODAS" && uf != "0")
//            {
//                queryPrincipal.Replace("{filtro_uf}", "AND UF.ES_SGL_EST = '" + uf + "' ");
//            }
//            else
//            {
//                queryPrincipal.Replace("{filtro_uf}", "");
//            }

//            origem = origem.ToUpper();
//            if (!string.IsNullOrWhiteSpace(origem) && origem != "TODOS" && origem != "TODAS" && origem != "0")
//            {
//                queryPrincipal.Replace("{filtro_origem}", "AND T.ESTACAO = '" + origem + "' ");
//            }
//            else
//            {
//                queryPrincipal.Replace("{filtro_origem}", "");
//            }

//            estacaoFaturamento = estacaoFaturamento.ToUpper();
//            if (!string.IsNullOrWhiteSpace(estacaoFaturamento) && estacaoFaturamento != "TODOS" && estacaoFaturamento != "TODAS" && estacaoFaturamento != "0")
//            {
//                queryPrincipal.Replace("{filtro_estacaoFaturamento}", "AND AOF.AO_COD_AOP = '" + estacaoFaturamento + "' ");
//            }
//            else
//            {
//                queryPrincipal.Replace("{filtro_estacaoFaturamento}", "");
//            }

//            terminal = terminal.ToUpper();
//            if (!string.IsNullOrWhiteSpace(terminal) && terminal != "TODOS" && terminal != "TODAS" && terminal != "0")
//            {
//                queryPrincipal.Replace("{filtro_terminal}", "AND TERM.AO_COD_AOP = '" + terminal + "' ");
//            }
//            else
//            {
//                queryPrincipal.Replace("{filtro_terminal}", "");
//            }

//            segmento = segmento.ToUpper();
//            if (!string.IsNullOrWhiteSpace(segmento) && segmento != "TODOS" && segmento != "TODAS" && segmento != "0")
//            {
//                queryPrincipal.Replace("{filtro_segmento}", "AND F.FX_COD_SEGMENTO = '" + segmento + "'");
//            }
//            else
//            {
//                queryPrincipal.Replace("{filtro_segmento}", "");
//            }

//            ocultarPedraZerada = ocultarPedraZerada.ToUpper();
//            if (!string.IsNullOrWhiteSpace(ocultarPedraZerada) && ocultarPedraZerada != "FALSE")
//            {
//                queryPrincipal.Replace("{filtro_ocultarPedraZerada}", " AND (T.PEDRA + P.PEDRA_EDITADO + T.REALIZADO) > 0");
//            }
//            else
//            {
//                queryPrincipal.Replace("{filtro_ocultarPedraZerada}", "");
//            }


//            using (var session = OpenSession())
//            {
//                var query = session.CreateSQLQuery(string.Format(queryPrincipal.ToString()));

//                foreach (var p in parameters)
//                {
//                    p.Invoke(query);
//                }

//                query.SetResultTransformer(Transformers.AliasToBean<VwPedraRealizado>());
//                var itens = query.List<VwPedraRealizado>();

//                return itens;
//            }
//        }

        public void AtualizarExcluirPedraRealizado(string acao, string id, string campo, string valor)
        {
            using (var session = OpenSession())
            {
                var queryPrincipal = new StringBuilder();
                using (var trans = session.BeginTransaction())
                {
                    switch(acao)
                    {
                        case "alterar":
                            queryPrincipal = new StringBuilder(@"
                               UPDATE PE_PEDRA_REALIZADO 
                                SET     {campo}
                                WHERE   ID = {id}");

                            if(campo.ToUpper() == "PEDRA_EDITADO" )
                                queryPrincipal.Replace("{campo}", String.Format("PEDRA_EDITADO = {0}", valor));
                            else
                                queryPrincipal.Replace("{campo}", String.Format("STATUS_EDITADO = {0}", valor));

                            queryPrincipal.Replace("{id}", id);
                            break;
                        case "excluir":
                            queryPrincipal = new StringBuilder(@"
                               DELETE PE_PEDRA_REALIZADO WHERE ID = {id}");

                            queryPrincipal.Replace("{id}", id);

                            break;
                    }


                    var query = session.CreateSQLQuery(string.Format(queryPrincipal.ToString()));

                    query.ExecuteUpdate();


                }
                session.Flush();
            }

        }

    }

}