﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.PainelExpedicao
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using ALL.Core.AcessoDados;
    using ALL.Core.Dominio;
    using NHibernate;
    using NHibernate.Transform;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Modules.Core.Domain.Model.PainelExpedicao;
    using Translogic.Modules.Core.Domain.Model.PainelExpedicao.Parametrizacao;
    using Translogic.Modules.Core.Domain.Model.PainelExpedicao.Parametrizacao.Enums;
    using Translogic.Modules.Core.Domain.Model.PainelExpedicao.Repositories;

    public class PainelExpedicaoRepository : NHRepository<PainelExpedicao, int>, IPainelExpedicaoRepository
    {
        public IList<Operador> ObterOperadores()
        {
            using (ISession session = OpenSession())
            {
                var queryString = @"
                    SELECT IdOperador   AS IdOperador
                         , Simbolo      AS Simbolo
                         , Descricao    AS Descricao
                      FROM Operador";

                IQuery query = session.CreateQuery(queryString);
                query.SetResultTransformer(Transformers.AliasToBean(typeof(Operador)));
                IList<Operador> operadores = query.List<Operador>();

                return operadores;
            }
        }

        public void SalvarConfiguracoesCores(string usuario)
        {
            throw new NotImplementedException();
        }


        public IList<PainelExpedicaoVisualizacaoDto> ObterPainelExpedicaoVisualizacao(DetalhesPaginacao detalhesPaginacao,
                                                                    DateTime dataInicio,
                                                                    string uf,
                                                                    string origem,
                                                                    string origemFaturamento,
                                                                    string segmento,
                                                                    string malhaCentral,
                                                                    bool metricaLarga,
                                                                    bool metricaNorte,
                                                                    bool metricaSul)
        {
            var parameters = new List<Action<IQuery>>();

            var queryPrincipal = new StringBuilder(@"
                SELECT AO.AO_COD_AOP AS ""Origem""
                     , AO.AO_DSC_AOP AS ""OrigemDescricao""
                     , UF.ES_SGL_EST AS ""OrigemUf""
                     , {0}
                     , {1}
                     , {2}
                     , {3}
                     , {4}
                     , {5}
                     , {6}
                  FROM PAINEL_EXPEDICAO_ORIGENS ORI
            INNER JOIN AREA_OPERACIONAL         AO  ON AO.AO_ID_AO  = ORI.ID_PE_OR_ID
             LEFT JOIN AREA_OPERACIONAL         AOF ON AOF.AO_ID_AO = ORI.ID_PE_ESTACAO_FATURAMENTO_ID
             LEFT JOIN MUNICIPIO                MN  ON MN.MN_ID_MNC = AO.MN_ID_MNC
             LEFT JOIN ESTADO                   UF  ON UF.ES_ID_EST = MN.ES_ID_EST
                 WHERE 1 = 1
                   {FiltroUf}
                   {FiltroOrigem}
                   {FiltroOrigemFaturamento}
                   {FiltroSegmentoPrincipal}
                   {FiltroMalhaPrincipal}
              ORDER BY UF.ES_SGL_EST ASC
                     , AO.AO_COD_AOP ASC
            ");

            var subQueryFaturado = new StringBuilder(@"
                (
                      SELECT COUNT(DISTINCT DC.VG_ID_VG)
                        FROM DESPACHO             DP
                        JOIN ITEM_DESPACHO        IDS ON IDS.DP_ID_DP = DP.DP_ID_DP
                        JOIN DETALHE_CARREGAMENTO DC  ON DC.DC_ID_CRG = IDS.DC_ID_CRG
                        JOIN CARREGAMENTO         CG  ON CG.CG_ID_CAR = DC.CG_ID_CAR
                        JOIN FLUXO_COMERCIAL      FC  ON FC.FX_ID_FLX = CG.FX_ID_FLX_CRG

                        JOIN AREA_OPERACIONAL         AO_ORI ON AO_ORI.AO_ID_AO  = NVL(FC.AO_ID_INT_OR, FC.AO_ID_EST_OR)
                        JOIN UNID_PRODUCAO            UP_ORI ON UP_ORI.UP_ID_UNP = AO_ORI.UP_ID_UNP
                        JOIN MALHA                    M_ORI  ON M_ORI.ML_CD_ML   = UP_ORI.UP_IND_MALHA

                       WHERE DP.DP_DT BETWEEN TO_DATE('{DataInicialFiltro}', 'dd/mm/yyyy hh24:mi:Ss') 
                                          AND TO_DATE('{DataFinalFiltro}', 'dd/mm/yyyy hh24:mi:Ss') 
                         AND FC.AO_ID_EST_OR = AO.AO_ID_AO
                         {FiltroSegmento}
                         {FiltroMalha}
                ) AS ""Faturado""
            ");

            var subQueryFaturadoManual = new StringBuilder(@"
                (
                      SELECT COUNT(DC.VG_ID_VG)
                        FROM DESPACHO             DP
                        JOIN ITEM_DESPACHO        IDS  ON IDS.DP_ID_DP = DP.DP_ID_DP
                        JOIN DETALHE_CARREGAMENTO DC   ON DC.DC_ID_CRG = IDS.DC_ID_CRG
                        JOIN CARREGAMENTO         CG   ON CG.CG_ID_CAR = DC.CG_ID_CAR
                        JOIN VAGAO_PEDIDO         VPED ON VPED.CG_ID_CAR = CG.CG_ID_CAR
                        JOIN FLUXO_COMERCIAL      FC   ON FC.FX_ID_FLX = CG.FX_ID_FLX_CRG

                        JOIN AREA_OPERACIONAL     AO_ORI ON AO_ORI.AO_ID_AO  = NVL(FC.AO_ID_INT_OR, FC.AO_ID_EST_OR)
                        JOIN UNID_PRODUCAO        UP_ORI ON UP_ORI.UP_ID_UNP = AO_ORI.UP_ID_UNP
                        JOIN MALHA                M_ORI  ON M_ORI.ML_CD_ML   = UP_ORI.UP_IND_MALHA

                       WHERE DP.DP_DT BETWEEN TO_DATE('{DataInicialFiltro}', 'dd/mm/yyyy hh24:mi:Ss') 
                                          AND TO_DATE('{DataFinalFiltro}', 'dd/mm/yyyy hh24:mi:Ss') 
                         AND FC.AO_ID_EST_OR = AO.AO_ID_AO
                         AND NVL(VPED.VB_COD_TRA, ' ') = 'BOI'
                         {FiltroSegmento}
                         {FiltroMalha}
                ) AS ""FaturadoManual"" 
            ");

            var subQueryPendFaturado = new StringBuilder(@"
                (
                     SELECT COUNT(DISTINCT BO.ID_BO)
                       FROM BO_BOLAR_BO       BO
                       JOIN BO_BOLAR_VAGAO    BV ON BV.ID_BO_ID   = BO.ID_BO
                       JOIN FLUXO_COMERCIAL   FC ON SUBSTR(FC.FX_COD_FLX, 3) = TO_CHAR(BO.COD_FLX)
                       JOIN VAGAO             VG ON VG.VG_COD_VAG = BV.VG_COD_VAG

                       JOIN AREA_OPERACIONAL  AO_ORI ON AO_ORI.AO_ID_AO  = NVL(FC.AO_ID_INT_OR, FC.AO_ID_EST_OR)
                       JOIN UNID_PRODUCAO     UP_ORI ON UP_ORI.UP_ID_UNP = AO_ORI.UP_ID_UNP
                       JOIN MALHA             M_ORI  ON M_ORI.ML_CD_ML   = UP_ORI.UP_IND_MALHA
                       JOIN BO_BOLAR_IMP_CFG BIMP ON BO.AO_COD_AOP = BIMP.CLIENTE

                      WHERE BO.BO_TIMESTAMP BETWEEN TO_DATE('{DataInicialFiltro}', 'dd/mm/yyyy hh24:mi:Ss') 
                                                AND TO_DATE('{DataFinalFiltro}', 'dd/mm/yyyy hh24:mi:Ss') 
                        AND BO.IDT_SIT = 'N'
                        AND FC.AO_ID_EST_OR = AO.AO_ID_AO

                        /* Verifica se já não foi faturado manualmente na 1131 */
                        AND NOT EXISTS(
                            SELECT 1
                              FROM VAGAO_PEDIDO VPE
                             WHERE VPE.VG_ID_VG = VG.VG_ID_VG
                               AND VPE.VB_DAT_CAR BETWEEN BO.BO_TIMESTAMP  AND  BO.BO_TIMESTAMP --TRUNC(BO.BO_TIMESTAMP - 2) AND TRUNC(BO.BO_TIMESTAMP + 2)
                               AND NVL(VPE.VB_COD_TRA, ' ') = 'BOI' /* Faturado Manualmente */
                               AND ROWNUM = 1
                        )

                        /* Verifica se já não foi faturado na 363 porém não foi atualizado o status do BO_BOLAR_BO por motivo de concorrência */
                        AND NOT EXISTS(
                            SELECT 1
                              FROM VAGAO_PEDIDO    VPE
                             WHERE VPE.VG_ID_VG  = VG.VG_ID_VG
                               AND VPE.FX_ID_FLX = FC.FX_ID_FLX
                               AND VPE.VB_DAT_CAR BETWEEN BO.BO_TIMESTAMP AND BO.BO_TIMESTAMP --TRUNC(BO.BO_TIMESTAMP - 2) AND TRUNC(BO.BO_TIMESTAMP + 2)
                               AND NVL(VPE.VB_COD_TRA, ' ') = 'LIBWBOLAR' /* Faturado pela 363 porém não foi alterado o status do BO_BOLAR_BO de 'N' para 'D' por motivo de concorrência */
                               AND ROWNUM = 1
                        )

                        /* Verifica o registro de faturamento não foi recebido e faturado anteriormente.
                           Deve-se fazer isso pois alguns clientes enviam vários registros de faturamento
                           pelo EDI em dias diferentes. Devemos apenas desconsirar esses casos. */
                        AND NOT EXISTS (
                            SELECT 1
                              FROM BO_BOLAR_BO     BO_REP
                              JOIN BO_BOLAR_IMP_CFG BIMP_E ON BO_REP.AO_COD_AOP = BIMP_E.CLIENTE
                              JOIN BO_BOLAR_VAGAO  BV_REP ON BV_REP.ID_BO_ID = BO_REP.ID_BO
                              JOIN FLUXO_COMERCIAL FC_REP ON SUBSTR(FC_REP.FX_COD_FLX, 3) = TO_CHAR(BO_REP.COD_FLX)
                             WHERE BO_REP.BO_TIMESTAMP BETWEEN BO.BO_TIMESTAMP AND BO.BO_TIMESTAMP -- TRUNC(BO.BO_TIMESTAMP - 3) AND TRUNC(BO.BO_TIMESTAMP + 1)
                               AND BO_REP.ID_BO       != BO.ID_BO
                               AND BO_REP.IDT_SIT     IN ( 'D'   /* Despachado                               */
                                                         , 'N'   /* Esperando acao na Tela 363 para despacho */
                                                         , 'Q'   /* Esperando acao do Pátio na Tela 1380     */
                                                         , 'S'   /* Esperando acao da Central na tela 1380   */
                                                         , 'E' ) /* Erro de despacho Tela 363                */
                               -- AND BO_REP.NOM_USR      = BO.NOM_USR
                               AND BIMP_E.CLIENTE =  BIMP.CLIENTE
                               AND BV_REP.VG_COD_VAG   = BV.VG_COD_VAG
                               AND FC_REP.FX_ID_FLX    = FC.FX_ID_FLX
                               AND ROWNUM = 1
                        )
                        {FiltroSegmento}
                        {FiltroMalha}
                ) AS ""PendenteFaturado""
            ");

            var subQueryCteAutorizado = new StringBuilder(@"
                  (
                     SELECT COUNT(DISTINCT DC.VG_ID_VG)
                       FROM DESPACHO             DP
                       JOIN CTE                  C   ON C.DP_ID_DP   = DP.DP_ID_DP
                       JOIN ITEM_DESPACHO        IDS ON IDS.DP_ID_DP = DP.DP_ID_DP
                       JOIN DETALHE_CARREGAMENTO DC  ON DC.DC_ID_CRG = IDS.DC_ID_CRG
                       JOIN CARREGAMENTO         CG  ON CG.CG_ID_CAR = DC.CG_ID_CAR
                       JOIN FLUXO_COMERCIAL      FC  ON FC.FX_ID_FLX = CG.FX_ID_FLX_CRG

                       JOIN AREA_OPERACIONAL     AO_ORI ON AO_ORI.AO_ID_AO  = NVL(FC.AO_ID_INT_OR, FC.AO_ID_EST_OR)
                       JOIN UNID_PRODUCAO        UP_ORI ON UP_ORI.UP_ID_UNP = AO_ORI.UP_ID_UNP
                       JOIN MALHA                M_ORI  ON M_ORI.ML_CD_ML   = UP_ORI.UP_IND_MALHA

                      WHERE DP.DP_DT BETWEEN TO_DATE('{DataInicialFiltro}', 'dd/mm/yyyy hh24:mi:Ss') 
                                         AND TO_DATE('{DataFinalFiltro}', 'dd/mm/yyyy hh24:mi:Ss') 
                        AND FC.AO_ID_EST_OR = AO.AO_ID_AO
                        AND C.SITUACAO_ATUAL = 'AUT'
                        {FiltroSegmento}
                        {FiltroMalha}
                  ) AS ""CteAutorizado""
            ");

            var subQueryCtePendente = new StringBuilder(@"
                  (
                     SELECT COUNT(DISTINCT C.VG_ID_VG)
                       FROM DESPACHO             DP
                       JOIN CTE                  C   ON DP.DP_ID_DP = C.DP_ID_DP
                       JOIN ITEM_DESPACHO        IDS ON DP.DP_ID_DP = IDS.DP_ID_DP
                       JOIN DETALHE_CARREGAMENTO DC  ON IDS.DC_ID_CRG = DC.DC_ID_CRG
                       JOIN CARREGAMENTO         CG  ON DC.CG_ID_CAR = CG.CG_ID_CAR
                       JOIN FLUXO_COMERCIAL      FC  ON CG.FX_ID_FLX_CRG = FC.FX_ID_FLX

                       JOIN AREA_OPERACIONAL     AO_ORI ON AO_ORI.AO_ID_AO  = NVL(FC.AO_ID_INT_OR, FC.AO_ID_EST_OR)
                       JOIN UNID_PRODUCAO        UP_ORI ON UP_ORI.UP_ID_UNP = AO_ORI.UP_ID_UNP
                       JOIN MALHA                M_ORI  ON M_ORI.ML_CD_ML   = UP_ORI.UP_IND_MALHA

                      WHERE DP.DP_DT BETWEEN TO_DATE('{DataInicialFiltro}', 'dd/mm/yyyy hh24:mi:Ss') 
                                         AND TO_DATE('{DataFinalFiltro}', 'dd/mm/yyyy hh24:mi:Ss') 
                        AND FC.AO_ID_EST_OR = AO.AO_ID_AO
                        AND C.SITUACAO_ATUAL NOT IN ('AUT', 'CAN', 'ANL', 'INT', 'AGA')
                        {FiltroSegmento}
                        {FiltroMalha}
                   ) AS ""CtePendente""
            ");

            // Total de BOs aguardando confirmação na tela de Conferencia de Arquivos
            var subQueryPendConfirmacao = new StringBuilder(@"
                (
                     SELECT COUNT(DISTINCT BO.ID_BO)
                       FROM BO_BOLAR_BO       BO
                       JOIN BO_BOLAR_VAGAO    BV ON BV.ID_BO_ID   = BO.ID_BO
                       JOIN FLUXO_COMERCIAL   FC ON SUBSTR(FC.FX_COD_FLX, 3) = TO_CHAR(BO.COD_FLX)
                       JOIN VAGAO             VG ON VG.VG_COD_VAG = BV.VG_COD_VAG

                       JOIN AREA_OPERACIONAL  AO_ORI ON AO_ORI.AO_ID_AO  = NVL(FC.AO_ID_INT_OR, FC.AO_ID_EST_OR)
                       JOIN UNID_PRODUCAO     UP_ORI ON UP_ORI.UP_ID_UNP = AO_ORI.UP_ID_UNP
                       JOIN MALHA             M_ORI  ON M_ORI.ML_CD_ML   = UP_ORI.UP_IND_MALHA

                      WHERE BO.BO_TIMESTAMP BETWEEN TO_DATE('{DataInicialFiltro}', 'dd/mm/yyyy hh24:mi:Ss') 
                                                AND TO_DATE('{DataFinalFiltro}', 'dd/mm/yyyy hh24:mi:Ss') 
                        AND FC.AO_ID_EST_OR  = AO.AO_ID_AO
                        AND BO.IDT_SIT       = 'Q'
                        AND NOT EXISTS( /* Verifica se já não foi faturado manualmente na 1131 */
                            SELECT 1
                              FROM VAGAO_PEDIDO VPE 
                             WHERE VPE.VG_ID_VG = VG.VG_ID_VG 
                               AND VPE.VB_DAT_CAR BETWEEN BO.BO_TIMESTAMP AND BO.BO_TIMESTAMP --TRUNC(BO.BO_TIMESTAMP - 1) AND TRUNC(BO.BO_TIMESTAMP + 2)
                               AND NVL(VPE.VB_COD_TRA, ' ') = 'BOI'
                               AND ROWNUM = 1
                        )
                        {FiltroSegmento}
                        {FiltroMalha}
                ) AS ""PendenteConfirmacao""
            ");

            // Total de BOs recusados pela estação e aguardando confirmação na tela de Conferencia de Arquivos 
            var subQueryTotalRecusadoEstacao = new StringBuilder(@"
                (
                    SELECT COUNT(DISTINCT BO.ID_BO)
                      FROM BO_BOLAR_BO      BO
                      JOIN FLUXO_COMERCIAL  FC ON SUBSTR(FC.FX_COD_FLX, 3) = TO_CHAR(BO.COD_FLX)
                      
                      JOIN AREA_OPERACIONAL AO_ORI ON AO_ORI.AO_ID_AO  = NVL(FC.AO_ID_INT_OR, FC.AO_ID_EST_OR)
                      JOIN UNID_PRODUCAO    UP_ORI ON UP_ORI.UP_ID_UNP = AO_ORI.UP_ID_UNP
                      JOIN MALHA            M_ORI  ON M_ORI.ML_CD_ML   = UP_ORI.UP_IND_MALHA

                    WHERE BO.BO_TIMESTAMP BETWEEN TO_DATE('{DataInicialFiltro}', 'dd/mm/yyyy hh24:mi:Ss') 
                                              AND TO_DATE('{DataFinalFiltro}', 'dd/mm/yyyy hh24:mi:Ss') 
                      AND FC.AO_ID_EST_OR  = AO.AO_ID_AO
                      AND BO.IDT_SIT       = 'S'
                      {FiltroSegmento}
                      {FiltroMalha}
                ) AS ""TotalRecusadoEstacao""
            ");

            subQueryFaturado = subQueryFaturado.Replace("{DataInicialFiltro}", dataInicio.ToString("dd/MM/yyyy 00:00:00"));
            subQueryFaturado = subQueryFaturado.Replace("{DataFinalFiltro}", dataInicio.ToString("dd/MM/yyyy 23:59:59"));

            subQueryPendFaturado = subQueryPendFaturado.Replace("{DataInicialFiltro}", dataInicio.ToString("dd/MM/yyyy 00:00:00"));
            subQueryPendFaturado = subQueryPendFaturado.Replace("{DataFinalFiltro}", dataInicio.ToString("dd/MM/yyyy 23:59:59"));

            subQueryFaturadoManual = subQueryFaturadoManual.Replace("{DataInicialFiltro}", dataInicio.ToString("dd/MM/yyyy 00:00:00"));
            subQueryFaturadoManual = subQueryFaturadoManual.Replace("{DataFinalFiltro}", dataInicio.ToString("dd/MM/yyyy 23:59:59"));

            subQueryCteAutorizado = subQueryCteAutorizado.Replace("{DataInicialFiltro}", dataInicio.ToString("dd/MM/yyyy 00:00:00"));
            subQueryCteAutorizado = subQueryCteAutorizado.Replace("{DataFinalFiltro}", dataInicio.ToString("dd/MM/yyyy 23:59:59"));

            subQueryCtePendente = subQueryCtePendente.Replace("{DataInicialFiltro}", dataInicio.ToString("dd/MM/yyyy 00:00:00"));
            subQueryCtePendente = subQueryCtePendente.Replace("{DataFinalFiltro}", dataInicio.ToString("dd/MM/yyyy 23:59:59"));

            subQueryPendConfirmacao = subQueryPendConfirmacao.Replace("{DataInicialFiltro}", dataInicio.ToString("dd/MM/yyyy 00:00:00"));
            subQueryPendConfirmacao = subQueryPendConfirmacao.Replace("{DataFinalFiltro}", dataInicio.ToString("dd/MM/yyyy 23:59:59"));

            subQueryTotalRecusadoEstacao = subQueryTotalRecusadoEstacao.Replace("{DataInicialFiltro}", dataInicio.ToString("dd/MM/yyyy 00:00:00"));
            subQueryTotalRecusadoEstacao = subQueryTotalRecusadoEstacao.Replace("{DataFinalFiltro}", dataInicio.ToString("dd/MM/yyyy 23:59:59"));

            // Filtro: Fluxo
            var filtroUf = String.Empty;
            uf = uf.ToUpper();
            if (!string.IsNullOrWhiteSpace(uf))
            {
                filtroUf = "AND UF.ES_SGL_EST = '" + uf + "'";
            }

            if (!string.IsNullOrWhiteSpace(malhaCentral))
            {
                if ((malhaCentral.ToUpper() == "SUL") && (string.IsNullOrWhiteSpace(uf)))
                {
                    filtroUf = "AND UF.ES_SGL_EST IN ('PR','SC','RS')";
                }
                else if ((malhaCentral.ToUpper() == "NORTE") && (string.IsNullOrWhiteSpace(uf)))
                {
                    filtroUf = "AND UF.ES_SGL_EST NOT IN ('PR','SC','RS')";
                }
            }

            queryPrincipal.Replace("{FiltroUf}", filtroUf);

            origem = origem.ToUpper();
            if (!string.IsNullOrWhiteSpace(origem))
            {
                queryPrincipal.Replace("{FiltroOrigem}", "AND AO.AO_COD_AOP = '" + origem + "'");
            }
            else
                queryPrincipal.Replace("{FiltroOrigem}", "");

            origemFaturamento = origemFaturamento.ToUpper();
            if (!string.IsNullOrWhiteSpace(origemFaturamento))
            {
                queryPrincipal.Replace("{FiltroOrigemFaturamento}", "AND AOF.AO_COD_AOP = '" + origemFaturamento + "'");
            }
            else
                queryPrincipal.Replace("{FiltroOrigemFaturamento}", String.Empty);

            segmento = segmento.ToUpper();
            if (!string.IsNullOrWhiteSpace(segmento))
            {
                subQueryFaturado.Replace("{FiltroSegmento}", "AND FC.FX_COD_SEGMENTO = '" + segmento + "'");
                subQueryPendFaturado.Replace("{FiltroSegmento}", "AND FC.FX_COD_SEGMENTO = '" + segmento + "'");
                subQueryFaturadoManual.Replace("{FiltroSegmento}", "AND FC.FX_COD_SEGMENTO = '" + segmento + "'");
                subQueryCteAutorizado.Replace("{FiltroSegmento}", "AND FC.FX_COD_SEGMENTO = '" + segmento + "'");
                subQueryCtePendente.Replace("{FiltroSegmento}", "AND FC.FX_COD_SEGMENTO = '" + segmento + "'");
                subQueryPendConfirmacao.Replace("{FiltroSegmento}", "AND FC.FX_COD_SEGMENTO = '" + segmento + "'");
                subQueryTotalRecusadoEstacao.Replace("{FiltroSegmento}", "AND FC.FX_COD_SEGMENTO = '" + segmento + "'");

                queryPrincipal.Replace("{FiltroSegmentoPrincipal}", "AND EXISTS (SELECT 1 FROM FLUXO_COMERCIAL FCS WHERE ROWNUM = 1 AND FCS.AO_ID_EST_OR = AO.AO_ID_AO AND FCS.FX_COD_SEGMENTO = '" + segmento + "')");
            }
            else
            {
                queryPrincipal.Replace("{FiltroSegmentoPrincipal}", "");
                subQueryFaturado.Replace("{FiltroSegmento}", "");
                subQueryPendFaturado.Replace("{FiltroSegmento}", "");
                subQueryFaturadoManual.Replace("{FiltroSegmento}", "");
                subQueryCteAutorizado.Replace("{FiltroSegmento}", "");
                subQueryCtePendente.Replace("{FiltroSegmento}", "");
                subQueryPendConfirmacao.Replace("{FiltroSegmento}", "");
                subQueryTotalRecusadoEstacao.Replace("{FiltroSegmento}", "");
            }

            #region Métricas

            var idsMalhas = new List<int>();
            if (metricaNorte)
            {
                idsMalhas.Add(1);
            }

            if (metricaSul)
            {
                idsMalhas.Add(2);
            }

            if (metricaLarga)
            {
                idsMalhas.Add(3);
            }

            var filtroMalha = String.Empty;
            var filtroMalhaPrincipal = String.Empty;
            if (idsMalhas.Count > 0)
            {
                var ids = String.Join(", ", idsMalhas);
                filtroMalha = String.Format("AND M_ORI.ML_IDT_ML IN ({0})", ids);
                filtroMalhaPrincipal = String.Format(@"
                    AND EXISTS (
                        SELECT 1 
                          FROM FLUXO_COMERCIAL  FCM 
                          JOIN AREA_OPERACIONAL AO_MALHA ON AO_MALHA.AO_ID_AO  = NVL(FCM.AO_ID_INT_OR, FCM.AO_ID_EST_OR)
                          JOIN UNID_PRODUCAO    UP_MALHA ON UP_MALHA.UP_ID_UNP = AO_MALHA.UP_ID_UNP
                          JOIN MALHA            M_MALHA  ON M_MALHA.ML_CD_ML   = UP_MALHA.UP_IND_MALHA
                         WHERE FCM.AO_ID_EST_OR = AO.AO_ID_AO 
                           AND ROWNUM = 1
                           AND M_MALHA.ML_IDT_ML IN ({0}))", ids);
            }

            queryPrincipal.Replace("{FiltroMalhaPrincipal}", filtroMalhaPrincipal);
            subQueryFaturado.Replace("{FiltroMalha}", filtroMalha);
            subQueryPendFaturado.Replace("{FiltroMalha}", filtroMalha);
            subQueryFaturadoManual.Replace("{FiltroMalha}", filtroMalha);
            subQueryCteAutorizado.Replace("{FiltroMalha}", filtroMalha);
            subQueryCtePendente.Replace("{FiltroMalha}", filtroMalha);
            subQueryPendConfirmacao.Replace("{FiltroMalha}", filtroMalha);
            subQueryTotalRecusadoEstacao.Replace("{FiltroMalha}", filtroMalha);

            #endregion Métricas

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(string.Format(queryPrincipal.ToString(),
                                                            subQueryFaturado,
                                                            subQueryFaturadoManual,
                                                            subQueryPendFaturado,
                                                            subQueryCteAutorizado,
                                                            subQueryCtePendente,
                                                            subQueryPendConfirmacao,
                                                            subQueryTotalRecusadoEstacao));

                foreach (var p in parameters)
                {
                    p.Invoke(query);
                }

                query.SetResultTransformer(Transformers.AliasToBean<PainelExpedicaoVisualizacaoDto>());
                var itens = query.List<PainelExpedicaoVisualizacaoDto>();

                return itens;
            }
        }

        public IList<PainelExpedicaoVisualizacaoDto> ObterPainelExpedicaoPedra(DetalhesPaginacao detalhesPaginacao,
                                                            string regiao,
                                                            DateTime dataInicio,
                                                            string uf,
                                                            string origem,
                                                            string origemFaturamento,
                                                            string segmento,
                                                            string malhaCentral,
                                                            string pedra)
        {
            var parameters = new List<Action<IQuery>>();

            var queryPrincipal = new StringBuilder(@"
                SELECT AO.AO_COD_AOP AS ""Origem""
                     , AO.AO_DSC_AOP AS ""OrigemDescricao""
                     , UF.ES_SGL_EST AS ""OrigemUf""
                     , {0}
                     , {1}
                     , {2}
                     , {3}
                     , {4}
                     , {5}
                     , {6}
                  FROM PAINEL_EXPEDICAO_ORIGENS ORI
            INNER JOIN AREA_OPERACIONAL         AO  ON AO.AO_ID_AO  = ORI.ID_PE_OR_ID
             LEFT JOIN AREA_OPERACIONAL         AOF ON AOF.AO_ID_AO = ORI.ID_PE_ESTACAO_FATURAMENTO_ID
             LEFT JOIN MUNICIPIO                MN  ON MN.MN_ID_MNC = AO.MN_ID_MNC
             LEFT JOIN ESTADO                   UF  ON UF.ES_ID_EST = MN.ES_ID_EST
                 WHERE 1 = 1
                   {FiltroUf}
                   {FiltroOrigem}
                   {FiltroOrigemFaturamento}
                   {FiltroSegmentoPrincipal}
                   {FiltroMalhaPrincipal}
              ORDER BY UF.ES_SGL_EST ASC
                     , AO.AO_COD_AOP ASC
            ");

            // Filtro: Fluxo
            var filtroUf = String.Empty;
            uf = uf.ToUpper();
            if (!string.IsNullOrWhiteSpace(uf))
            {
                filtroUf = "AND UF.ES_SGL_EST = '" + uf + "'";
            }

            if (!string.IsNullOrWhiteSpace(malhaCentral))
            {
                if ((malhaCentral.ToUpper() == "SUL") && (string.IsNullOrWhiteSpace(uf)))
                {
                    filtroUf = "AND UF.ES_SGL_EST IN ('PR','SC','RS')";
                }
                else if ((malhaCentral.ToUpper() == "NORTE") && (string.IsNullOrWhiteSpace(uf)))
                {
                    filtroUf = "AND UF.ES_SGL_EST NOT IN ('PR','SC','RS')";
                }
            }

            queryPrincipal.Replace("{FiltroUf}", filtroUf);

            origem = origem.ToUpper();
            if (!string.IsNullOrWhiteSpace(origem))
            {
                queryPrincipal.Replace("{FiltroOrigem}", "AND AO.AO_COD_AOP = '" + origem + "'");
            }
            else
                queryPrincipal.Replace("{FiltroOrigem}", "");

            origemFaturamento = origemFaturamento.ToUpper();
            if (!string.IsNullOrWhiteSpace(origemFaturamento))
            {
                queryPrincipal.Replace("{FiltroOrigemFaturamento}", "AND AOF.AO_COD_AOP = '" + origemFaturamento + "'");
            }
            else
                queryPrincipal.Replace("{FiltroOrigemFaturamento}", String.Empty);

            segmento = segmento.ToUpper();
            if (!string.IsNullOrWhiteSpace(segmento))
            {
                queryPrincipal.Replace("{FiltroSegmentoPrincipal}", "AND EXISTS (SELECT 1 FROM FLUXO_COMERCIAL FCS WHERE ROWNUM = 1 AND FCS.AO_ID_EST_OR = AO.AO_ID_AO AND FCS.FX_COD_SEGMENTO = '" + segmento + "')");
            }
            else
            {
                queryPrincipal.Replace("{FiltroSegmentoPrincipal}", "");
            }

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(string.Format(queryPrincipal.ToString()));

                foreach (var p in parameters)
                {
                    p.Invoke(query);
                }

                query.SetResultTransformer(Transformers.AliasToBean<PainelExpedicaoVisualizacaoDto>());
                var itens = query.List<PainelExpedicaoVisualizacaoDto>();

                return itens;
            }
        }


        public ResultadoPaginado<PainelExpedicaoConsultaDto> ObterPainelExpedicaoConsulta(
                                                                    DetalhesPaginacao detalhesPaginacao,
                                                                    DateTime dataInicio,
                                                                    DateTime dataFinal,
                                                                    string fluxo,
                                                                    string origem,
                                                                    string destino,
                                                                    string[] vagoes,
                                                                    string situacao,
                                                                    string lotacao,
                                                                    string localAtual,
                                                                    string cliente,
                                                                    string terminalFaturamento,
                                                                    string[] mercadorias,
                                                                    string segmento)
        {
            var parameters = new List<Action<IQuery>>();

            var hql = new StringBuilder(@"
     SELECT DISTINCT
            SV.SV_COD_SER    AS ""SerieVagao""
          , VG.VG_COD_VAG    AS ""Vagao""
          , EVN.SH_DSC_PT    AS ""Lotacao""
          , EVN.SI_DSC_PT    AS ""Situacao""

          , NVL(VG.VG_NUM_TRA, FE.FC_TAR)   AS ""PesoTara""
          , IDS.ID_NUM_PCL                  AS ""PesoRealCarregado""
          , NVL(VG.VG_NUM_TRA, FE.FC_TAR) + 
                IDS.ID_NUM_PCL              AS ""PesoBruto""

          , FC.FX_COD_FLX    AS ""Fluxo""
          , ORI.AO_COD_AOP   AS ""Origem""
          , DST.AO_COD_AOP   AS ""Destino""

          , EXPEDIDOR.EP_DSC_RSM     AS ""TerminalFaturamento""
          , NVL(USU.US_NOM_USU, ' ') AS ""Aprovador""

          , CFG.CLIENTE_DSC AS ""Cliente""
          , M.MC_DRS_PT    AS ""Mercadoria""
          
          ,(SELECT LISTAGG(NUM_CONTAINER,',') WITHIN GROUP (ORDER BY 1) AS CONTAINER
            FROM BO_BOLAR_NF
            WHERE BO_BOLAR_NF.ID_VAGAO_ID = BV.ID_VAGAO)          AS ""Container"" 
 
          , AOM.AO_COD_AOP AS ""Local""
          , NULL AS ""Trem""
          , CASE BO.IDT_SIT WHEN 'D' THEN 'S' 
                            WHEN 'E' THEN (CASE NVL(VPED.VB_COD_TRA, ' ') WHEN 'BOI' THEN 'N' ELSE 'S' END)
                            ELSE 'N'
                             END AS ""Faturado""

          , (CASE NVL(VPED.VB_COD_TRA, ' ') WHEN 'BOI' THEN 'S' ELSE 'N' END) AS ""FaturadoManual""

          , '' AS ""ErroFaturamento""
          , (SELECT C.SITUACAO_ATUAL FROM CTE C WHERE C.DP_ID_DP = DP.DP_ID_DP AND ROWNUM = 1) AS ""CteStatus""
          , FC.FX_COD_SEGMENTO AS ""Segmento""

            /* Subquery - Ticket */
          , NVL( (SELECT (CASE WHEN TB.ID IS NOT NULL THEN 'OK' ELSE '-' END)
                   FROM NOTA_FISCAL NF
                      , NOTA_TICKET_VAGAO NTV
                      , VAGAO_TICKET VT
                      , TICKET_BALANCA TB
                      , CONF_GERAL CT
                  WHERE IDS.DP_ID_DP = NF.DP_ID_DP
                    AND IDS.VG_ID_VG = VG.VG_ID_VG
                    AND NTV.chave_nfe = NF.NFE_CHAVE_NFE
                    AND LPAD(VT.VG_COD_VAG, 7, '0') = VG.VG_COD_VAG
                    AND VT.Id = NTV.VAGAO_TICKET_ID
                    AND TB.Id = VT.TICKET_BALANCA_ID
                    AND CT.CHAVE = 'INTERVALO_DIAS_TICKET_TSP'
                    -- CONSIDERA PESAGENS QUE OCORRERAM 00H DO DIA ANTERIOR AO CARREGAMENTO                    
                    AND ((VT.DATA_CADASTRO BETWEEN TRUNC(DP.DP_DT - 3) AND TRUNC(DP.DP_DT + 3) AND VPED.VO_IDT_VPT_OPER IS NULL)
                        OR 
                        (VT.DATA_CADASTRO BETWEEN TRUNC(DP.DP_DT - NVL(CT.VALOR,3)) AND TRUNC(DP.DP_DT + NVL(CT.VALOR,3)) AND VPED.VO_IDT_VPT_OPER IS NOT NULL AND (VT.TU=IDS.ID_NUM_PCL*100)))
                    AND ROWNUM = 1
                 ), '-') AS ""Ticket""

          , TRUNC(DP.DP_DT) AS ""Data""
          , TRUNC(TO_DATE(BO.BO_TIMESTAMP)) AS ""Data_Bo""
          
          , CASE WHEN 
                    NVL(
                        (SELECT 1 
                           FROM DESPACHO_LOCAL_BLOQUEIO DLB 
                          WHERE DLB.DB_EST_ID = NVL(FC.AO_ID_INT_OR, FC.AO_ID_EST_OR) 
                            AND NVL(DLB.DB_CNPJ_RAIZ, SUBSTR(EXPEDIDOR.EP_CGC_EMP, 1, 8)) = SUBSTR(EXPEDIDOR.EP_CGC_EMP, 1, 8)
                            AND DLB.DB_COD_SEGMENTO = FC.FX_COD_SEGMENTO
                            AND ROWNUM = 1)
                        , 0) = 1
                THEN BO.DATA_CONFIRMADO
                ELSE BO.BO_TIMESTAMP
                 END AS ""HorarioRecebimentoFaturamento""

          , CASE WHEN BO.NOM_ARQ = 'EDI' THEN EDI.DATA_CADASTRO
                                         ELSE BO.BO_TIMESTAMP
                                          END AS ""HorarioRecebimentoArquivo""

          , DP.DP_DT AS ""HorarioFaturamento""

          , CASE WHEN 
                    NVL(
                        (SELECT 1 
                           FROM DESPACHO_LOCAL_BLOQUEIO DLB 
                          WHERE DLB.DB_EST_ID = NVL(FC.AO_ID_INT_OR, FC.AO_ID_EST_OR) 
                            AND NVL(DLB.DB_CNPJ_RAIZ, SUBSTR(EXPEDIDOR.EP_CGC_EMP, 1, 8)) = SUBSTR(EXPEDIDOR.EP_CGC_EMP, 1, 8)
                            AND DLB.DB_COD_SEGMENTO = FC.FX_COD_SEGMENTO
                            AND ROWNUM = 1)
                        , 0) = 1
                THEN BO.BO_TIMESTAMP
                ELSE NULL
                 END AS ""HorarioLiberacao1380""

            /* Usuário Evento de Carregamento */
          , NVL( (SELECT USU.US_USR_IDU || ' - ' || USU.US_NOM_USU
                FROM EVENTO_VAGAO EVG
                JOIN USUARIO USU ON USU.US_USR_IDU = EVG.EA_MAT_USU 
                WHERE EVG.EN_ID_EVT = 13 /* Evento Carregar Vagão */
                  AND EVG.VG_ID_VG = VG.VG_ID_VG
                  AND TO_DATE(TO_CHAR(EVG.EA_DTO_EVG, 'YYYY/MM/DD HH24:MI'), 'YYYY/MM/DD HH24:MI')
                    = TO_DATE(TO_CHAR(DP.DP_DT      , 'YYYY/MM/DD HH24:MI'), 'YYYY/MM/DD HH24:MI')
                   AND ROWNUM = 1
               ), '') AS ""UsuarioEventoCarregamento""

          , NVL(BV.MD, 'NAO') AS ""MultiploDespacho""

       FROM VAGAO_PATIO_VIG      VPV
 INNER JOIN VAGAO                VG          ON VG.VG_ID_VG = VPV.VG_ID_VG
 INNER JOIN SERIE_VAGAO          SV          ON SV.SV_ID_SV = VG.SV_ID_SV 
 INNER JOIN FOLHA_ESPECIF_VAGAO  FE          ON FE.FC_ID_FC = VG.FC_ID_FC
 INNER JOIN ESTADO_VAGAO_NOVA    EVN         ON EVN.VG_ID_VG = VG.VG_ID_VG
 INNER JOIN AREA_OPERACIONAL     AOF         ON AOF.AO_ID_AO = VPV.AO_ID_AO
 INNER JOIN AREA_OPERACIONAL     AOM         ON AOM.AO_ID_AO = AOF.AO_ID_AO_INF
 INNER JOIN VAGAO_PEDIDO_VIG     VPED        ON VPED.VG_ID_VG = VG.VG_ID_VG
 INNER JOIN FLUXO_COMERCIAL      FC          ON FC.FX_ID_FLX = VPED.FX_ID_FLX
 INNER JOIN MERCADORIA           M           ON M.MC_ID_MRC = FC.MC_ID_MRC
 INNER JOIN EMPRESA              EXPEDIDOR   ON EXPEDIDOR.EP_ID_EMP = FC.EP_ID_EMP_REM
 INNER JOIN EMPRESA              COR         ON COR.EP_ID_EMP = FC.EP_ID_EMP_COR
 INNER JOIN AREA_OPERACIONAL     ORI         ON ORI.AO_ID_AO = FC.AO_ID_EST_OR
 INNER JOIN AREA_OPERACIONAL     DST         ON DST.AO_ID_AO = FC.AO_ID_EST_DS
 INNER JOIN CARREGAMENTO         CG          ON CG.CG_ID_CAR = VPED.CG_ID_CAR
 INNER JOIN DETALHE_CARREGAMENTO DC          ON DC.VG_ID_VG = VG.VG_ID_VG AND CG.CG_ID_CAR = DC.CG_ID_CAR
 INNER JOIN ITEM_DESPACHO        IDS         ON IDS.DC_ID_CRG = DC.DC_ID_CRG
 INNER JOIN DESPACHO             DP          ON DP.DP_ID_DP = IDS.DP_ID_DP

  LEFT JOIN (
       SELECT MAX(CASE WHEN BO.IDT_SIT = 'D'
                       THEN BO.ID_BO
                       ELSE NULL
                        END)      AS BO_ID
            , MIN(BO.ID_BO)       AS BO_ID_MIN
            , BV.VG_COD_VAG       AS BV_CODIGO_VAGAO
            , TO_CHAR(BO.COD_FLX) AS BO_CODIGO_FLUXO
         FROM BO_BOLAR_BO    BO
         JOIN BO_BOLAR_VAGAO BV ON BV.ID_BO_ID = BO.ID_BO
        WHERE BO.IDT_SIT IN ('D', 'E')
          AND BO.BO_TIMESTAMP BETWEEN TO_DATE('{DataInicialFiltro}', 'MM/DD/YYYY') - 3
                                  AND TO_DATE('{DataFinalFiltro}', 'MM/DD/YYYY')

        GROUP BY BV.VG_COD_VAG
               , BO.COD_FLX
                               ) TMP ON TMP.BV_CODIGO_VAGAO = VG.VG_COD_VAG
                                    AND TMP.BO_CODIGO_FLUXO = SUBSTR(FC.FX_COD_FLX, 3)

  LEFT JOIN BO_BOLAR_BO          BO  ON BO.ID_BO = TMP.BO_ID
  LEFT JOIN BO_BOLAR_VAGAO       BV  ON BV.ID_BO_ID = BO.ID_BO
  LEFT JOIN BO_BOLAR_IMP_CFG     CFG ON CFG.CLIENTE = BO.AO_COD_AOP
  LEFT JOIN USUARIO              USU ON USU.US_IDT_USU = BO.USU_ID_CONFIRMADO

  LEFT JOIN BO_BOLAR_BO          BO_PRIMEIRO ON BO_PRIMEIRO.ID_BO = TMP.BO_ID_MIN
  LEFT JOIN BO_BOLAR_VAGAO       BV_PRIMEIRO ON BV_PRIMEIRO.ID_BO_ID = BO_PRIMEIRO.ID_BO
  LEFT JOIN (
       SELECT WAM.ID_VAGAO_BO
            , WR.DATA_CADASTRO
         FROM EDI.WS_MENSAGEM_RECEBIMENTO   WR
         JOIN EDI.WS_ASSOCIA_MSG_REC_VAGAO WAM ON WAM.ID_MENSAGEM_RECEBIMENTO = WR.ID_MENSAGEM_RECEBIMENTO
        WHERE WR.DATA_CADASTRO BETWEEN TO_DATE('{DataInicialFiltro}', 'MM/DD/YYYY') - 10
                                   AND TO_DATE('{DataFinalFiltro}', 'MM/DD/YYYY')
                               ) EDI ON EDI.ID_VAGAO_BO = BV_PRIMEIRO.ID_VAGAO

      WHERE DP.DP_DT >= TO_DATE('{DataInicialFiltro}', 'MM/DD/YYYY')
        AND DP.DP_DT  < TO_DATE('{DataFinalFiltro}', 'MM/DD/YYYY')
        AND NVL(BO.IDT_SIT, ' ') != 'J'

        {FluxoFiltro}
        {OrigemFiltro}
        {DestinoFiltro}
        {VagoesFiltro}
        {SituacaoFiltro}
        {LotacaoFiltro}
        {LocalFiltro}
        {ClienteFiltro}
        {TerminalFaturamentoFiltro}
        {MercadoriaFiltro}
        {SegmentoFiltro}

  UNION ALL

     SELECT DISTINCT 
            SV.SV_COD_SER  AS ""SerieVagao""
          , VG.VG_COD_VAG  AS ""Vagao""
          , EVN.SH_DSC_PT  AS ""Lotacao""
          , EVN.SI_DSC_PT  AS ""Situacao""

          , NVL(VG.VG_NUM_TRA, FE.FC_TAR)   AS ""PesoTara""
          , IDS.ID_NUM_PCL                  AS ""PesoRealCarregado""
          , NVL(VG.VG_NUM_TRA, FE.FC_TAR) + 
                IDS.ID_NUM_PCL              AS ""PesoBruto""

          , FC.FX_COD_FLX  AS ""Fluxo""
          , ORI.AO_COD_AOP AS ""Origem""
          , DST.AO_COD_AOP AS ""Destino""

          , EXPEDIDOR.EP_DSC_RSM     AS ""TerminalFaturamento""
          , NVL(USU.US_NOM_USU, ' ') AS ""Aprovador""

          , CFG.CLIENTE_DSC AS ""Cliente""
          , M.MC_DRS_PT AS ""Mercadoria""

          ,(SELECT LISTAGG(NUM_CONTAINER,',') WITHIN GROUP (ORDER BY 1) AS CONTAINER
            FROM BO_BOLAR_NF
            WHERE BO_BOLAR_NF.ID_VAGAO_ID = BV.ID_VAGAO)          AS ""Container""  

          , NVL(( SELECT CASE WHEN L.AO_ID_AO_INF IS NOT NULL 
                              THEN (SELECT AOM.AO_COD_AOP FROM AREA_OPERACIONAL AOM WHERE AOM.AO_ID_AO = L.AO_ID_AO_INF)
                              ELSE L.AO_COD_AOP
                               END AS LOCAL_ATUAL
                   FROM MOVIMENTACAO_TREM MTR
                      , AREA_OPERACIONAL L
                  WHERE MTR.MT_ID_MOV = TR.MT_ID_MOV
                    AND MTR.AO_ID_AO = L.AO_ID_AO
 
               ), ORI_TREM.AO_COD_AOP) AS ""Local""
          , TR.TR_PFX_TRM || '-' || TOS.X1_NRO_OS AS ""Trem""
          , CASE BO.IDT_SIT WHEN 'D' THEN 'S' 
                            WHEN 'E' THEN (CASE NVL(VPV.VB_COD_TRA, ' ') WHEN 'BOI' THEN 'N' ELSE 'S' END)
                            ELSE 'N'
                             END AS ""Faturado""
          , (CASE NVL(VPV.VB_COD_TRA, ' ') WHEN 'BOI' THEN 'S' ELSE 'N' END) AS ""FaturadoManual""

          , '' AS ""ErroFaturamento""
          , (SELECT C.SITUACAO_ATUAL FROM CTE C WHERE C.DP_ID_DP = DP.DP_ID_DP AND ROWNUM = 1) AS ""CteStatus""
          , FC.FX_COD_SEGMENTO AS ""Segmento""

            /* Subquery - Ticket */
          , NVL( (SELECT (CASE WHEN TB.ID IS NOT NULL THEN 'OK' ELSE '-' END)
                   FROM NOTA_FISCAL NF
                      , NOTA_TICKET_VAGAO NTV
                      , VAGAO_TICKET VT
                      , TICKET_BALANCA TB
                      , CONF_GERAL CT
                  WHERE IDS.DP_ID_DP = NF.DP_ID_DP
                    AND IDS.VG_ID_VG = VG.VG_ID_VG
                    AND NTV.CHAVE_NFE = NF.NFE_CHAVE_NFE
                    AND LPAD(VT.VG_COD_VAG, 7, '0') = VG.VG_COD_VAG
                    AND VT.ID = NTV.VAGAO_TICKET_ID
                    AND TB.ID = VT.TICKET_BALANCA_ID
                    AND CT.CHAVE = 'INTERVALO_DIAS_TICKET_TSP'
                    -- CONSIDERA PESAGENS QUE OCORRERAM 00H DO DIA ANTERIOR AO CARREGAMENTO
                    AND ((VT.DATA_CADASTRO BETWEEN TRUNC(DP.DP_DT - 3) AND TRUNC(DP.DP_DT + 3) AND VPV.VO_IDT_VPT_OPER IS NULL)
                        OR 
                        (VT.DATA_CADASTRO BETWEEN TRUNC(DP.DP_DT - NVL(CT.VALOR,3)) AND TRUNC(DP.DP_DT + NVL(CT.VALOR,3)) AND VPV.VO_IDT_VPT_OPER IS NOT NULL AND (VT.TU=IDS.ID_NUM_PCL*100)))
                    AND ROWNUM = 1
               ), '-') AS ""Ticket""

          , TRUNC(DP.DP_DT)  AS ""Data""
          , TRUNC(TO_DATE(BO.BO_TIMESTAMP)) AS ""Data_Bo""

          , CASE WHEN 
                    NVL(
                        (SELECT 1 
                           FROM DESPACHO_LOCAL_BLOQUEIO DLB 
                          WHERE DLB.DB_EST_ID = NVL(FC.AO_ID_INT_OR, FC.AO_ID_EST_OR) 
                            AND NVL(DLB.DB_CNPJ_RAIZ, SUBSTR(EXPEDIDOR.EP_CGC_EMP, 1, 8)) = SUBSTR(EXPEDIDOR.EP_CGC_EMP, 1, 8)
                            AND DLB.DB_COD_SEGMENTO = FC.FX_COD_SEGMENTO
                            AND ROWNUM = 1)
                        , 0) = 1
                THEN BO.DATA_CONFIRMADO
                ELSE BO.BO_TIMESTAMP
                 END AS ""HorarioRecebimentoFaturamento""

          , CASE WHEN BO.NOM_ARQ = 'EDI' THEN EDI.DATA_CADASTRO
                                         ELSE BO.BO_TIMESTAMP
                                          END AS ""HorarioRecebimentoArquivo""
          , DP.DP_DT AS ""HorarioFaturamento""

          , CASE WHEN 
                    NVL(
                        (SELECT 1 
                           FROM DESPACHO_LOCAL_BLOQUEIO DLB 
                          WHERE DLB.DB_EST_ID = NVL(FC.AO_ID_INT_OR, FC.AO_ID_EST_OR) 
                            AND NVL(DLB.DB_CNPJ_RAIZ, SUBSTR(EXPEDIDOR.EP_CGC_EMP, 1, 8)) = SUBSTR(EXPEDIDOR.EP_CGC_EMP, 1, 8)
                            AND DLB.DB_COD_SEGMENTO = FC.FX_COD_SEGMENTO
                            AND ROWNUM = 1)
                        , 0) = 1
                THEN BO.BO_TIMESTAMP
                ELSE NULL
                 END AS ""HorarioLiberacao1380""

            /* Usuário Evento de Carregamento */
          , NVL( ( SELECT USU.US_USR_IDU || ' - ' || USU.US_NOM_USU
               FROM EVENTO_VAGAO EVG
               JOIN USUARIO USU ON USU.US_USR_IDU = EVG.EA_MAT_USU 
               WHERE EVG.EN_ID_EVT = 13 /* Evento Carregar Vagão */
                 AND EVG.VG_ID_VG = VG.VG_ID_VG
                 AND TO_DATE(TO_CHAR(EVG.EA_DTO_EVG, 'YYYY/MM/DD HH24:MI'), 'YYYY/MM/DD HH24:MI') = TO_DATE(TO_CHAR(DP.DP_DT, 'YYYY/MM/DD HH24:MI'), 'YYYY/MM/DD HH24:MI')
                  AND ROWNUM = 1
                ), '') AS ""UsuarioEventoCarregamento""

          , NVL(BV.MD, 'NAO') AS ""MultiploDespacho""

       FROM COMPVAGAO_VIG        CVV
 INNER JOIN VAGAO                VG          ON VG.VG_ID_VG = CVV.VG_ID_VG
 INNER JOIN SERIE_VAGAO          SV          ON SV.SV_ID_SV = VG.SV_ID_SV 
 INNER JOIN FOLHA_ESPECIF_VAGAO  FE          ON FE.FC_ID_FC = VG.FC_ID_FC
 INNER JOIN ESTADO_VAGAO_NOVA    EVN         ON EVN.VG_ID_VG = VG.VG_ID_VG
 INNER JOIN COMPOSICAO           CP          ON CP.CP_ID_CPS = CVV.CP_ID_CPS
 INNER JOIN TREM                 TR          ON TR.TR_ID_TRM = CP.TR_ID_TRM
 INNER JOIN T2_OS                TOS         ON TOS.X1_ID_OS = TR.OF_ID_OSV
 INNER JOIN VAGAO_PEDIDO_VIG     VPV         ON VPV.VG_ID_VG = CVV.VG_ID_VG AND VPV.PT_ID_ORT = CVV.PT_ID_ORT
 INNER JOIN FLUXO_COMERCIAL      FC          ON FC.FX_ID_FLX = VPV.FX_ID_FLX
 INNER JOIN AREA_OPERACIONAL     ORI         ON ORI.AO_ID_AO = FC.AO_ID_EST_OR
 INNER JOIN AREA_OPERACIONAL     DST         ON DST.AO_ID_AO = FC.AO_ID_EST_DS
 INNER JOIN EMPRESA              EXPEDIDOR   ON EXPEDIDOR.EP_ID_EMP = FC.EP_ID_EMP_REM
 INNER JOIN EMPRESA              COR         ON COR.EP_ID_EMP = FC.EP_ID_EMP_COR
 INNER JOIN MERCADORIA           M           ON M.MC_ID_MRC = FC.MC_ID_MRC
 INNER JOIN CARREGAMENTO         CG          ON CG.CG_ID_CAR = VPV.CG_ID_CAR 
 INNER JOIN DETALHE_CARREGAMENTO DC          ON DC.VG_ID_VG = VPV.VG_ID_VG AND DC.CG_ID_CAR = CG.CG_ID_CAR
 INNER JOIN ITEM_DESPACHO        IDS         ON IDS.DC_ID_CRG = DC.DC_ID_CRG AND IDS.VG_ID_VG = VPV.VG_ID_VG
 INNER JOIN DESPACHO             DP          ON DP.DP_ID_DP = IDS.DP_ID_DP
 INNER JOIN AREA_OPERACIONAL     ORI_TREM    ON ORI_TREM.AO_ID_AO = TR.AO_ID_AO_ORG

  LEFT JOIN (
       SELECT MAX(CASE WHEN BO.IDT_SIT = 'D'
                       THEN BO.ID_BO
                       ELSE NULL
                        END)      AS BO_ID
            , MIN(BO.ID_BO)       AS BO_ID_MIN
            , BV.VG_COD_VAG       AS BV_CODIGO_VAGAO
            , TO_CHAR(BO.COD_FLX) AS BO_CODIGO_FLUXO
         FROM BO_BOLAR_BO    BO
         JOIN BO_BOLAR_VAGAO BV ON BV.ID_BO_ID = BO.ID_BO
        WHERE BO.IDT_SIT IN ('D', 'E')
          AND BO.BO_TIMESTAMP BETWEEN TO_DATE('{DataInicialFiltro}', 'MM/DD/YYYY') - 3
                                  AND TO_DATE('{DataFinalFiltro}', 'MM/DD/YYYY')

        GROUP BY BV.VG_COD_VAG
               , BO.COD_FLX
                               ) TMP ON TMP.BV_CODIGO_VAGAO = VG.VG_COD_VAG
                                    AND TMP.BO_CODIGO_FLUXO = SUBSTR(FC.FX_COD_FLX, 3)

  LEFT JOIN BO_BOLAR_BO          BO  ON BO.ID_BO = TMP.BO_ID
  LEFT JOIN BO_BOLAR_VAGAO       BV  ON BV.ID_BO_ID = BO.ID_BO
  LEFT JOIN BO_BOLAR_IMP_CFG     CFG ON CFG.CLIENTE = BO.AO_COD_AOP
  LEFT JOIN USUARIO              USU ON USU.US_IDT_USU = BO.USU_ID_CONFIRMADO
  
  LEFT JOIN BO_BOLAR_BO          BO_PRIMEIRO ON BO_PRIMEIRO.ID_BO = TMP.BO_ID_MIN
  LEFT JOIN BO_BOLAR_VAGAO       BV_PRIMEIRO ON BV_PRIMEIRO.ID_BO_ID = BO_PRIMEIRO.ID_BO
  LEFT JOIN (
       SELECT WAM.ID_VAGAO_BO
            , WR.DATA_CADASTRO
         FROM EDI.WS_MENSAGEM_RECEBIMENTO   WR
         JOIN EDI.WS_ASSOCIA_MSG_REC_VAGAO WAM ON WAM.ID_MENSAGEM_RECEBIMENTO = WR.ID_MENSAGEM_RECEBIMENTO
        WHERE WR.DATA_CADASTRO BETWEEN TO_DATE('{DataInicialFiltro}', 'MM/DD/YYYY') - 10
                                   AND TO_DATE('{DataFinalFiltro}', 'MM/DD/YYYY')
                               ) EDI ON EDI.ID_VAGAO_BO = BV_PRIMEIRO.ID_VAGAO

      WHERE DP.DP_DT >= TO_DATE('{DataInicialFiltro}', 'MM/DD/YYYY')
        AND DP.DP_DT  < TO_DATE('{DataFinalFiltro}', 'MM/DD/YYYY')
        AND NVL(BO.IDT_SIT, ' ') != 'J'

        {FluxoFiltro}
        {OrigemFiltro}
        {DestinoFiltro}
        {VagoesFiltro}
        {SituacaoFiltro}
        {LotacaoFiltro}
        {LocalFiltro2}
        {ClienteFiltro}
        {TerminalFaturamentoFiltro}
        {MercadoriaFiltro}
        {SegmentoFiltro}

  UNION ALL

     SELECT DISTINCT 
            SV.SV_COD_SER            AS ""SerieVagao""
          , BV.VG_COD_VAG            AS ""Vagao""
          , EVN.SH_DSC_PT            AS ""Lotacao""
          , EVN.SI_DSC_PT            AS ""Situacao""

          , NVL(VG.VG_NUM_TRA, FE.FC_TAR) AS ""PesoTara""
          , 0                             AS ""PesoRealCarregado""
          , NVL(VG.VG_NUM_TRA, FE.FC_TAR) AS ""PesoBruto""

          , FC.FX_COD_FLX            AS ""Fluxo""
          , ORI.AO_COD_AOP           AS ""Origem""
          , DST.AO_COD_AOP           AS ""Destino""
          , EXPEDIDOR.EP_DSC_RSM     AS ""TerminalFaturamento""
          , NVL(USU.US_NOM_USU, ' ') AS ""Aprovador""
          , CFG.CLIENTE_DSC          AS ""Cliente""
          , M.MC_DRS_PT              AS ""Mercadoria""
          ,(SELECT LISTAGG(NUM_CONTAINER,',') WITHIN GROUP (ORDER BY 1) AS CONTAINER
            FROM BO_BOLAR_NF
            WHERE BO_BOLAR_NF.ID_VAGAO_ID = BV.ID_VAGAO)          AS ""Container""  
          , AOM.AO_COD_AOP           AS ""Local""
          , NULL                     AS ""Trem""
          , 'N'                      AS ""Faturado""
          , 'N'                      AS ""FaturadoManual""
          , BO.DSC_ERR               AS ""ErroFaturamento""
          , NULL                     AS ""CteStatus""
          , FC.FX_COD_SEGMENTO       AS ""Segmento""

            /* Subquery - Ticket */
          , '-'    AS ""Ticket""
          , NULL   AS ""Data""
          , TRUNC(TO_DATE(BO.BO_TIMESTAMP)) AS ""Data_Bo""
          
          , CASE WHEN 
                    NVL(
                        (SELECT 1 
                           FROM DESPACHO_LOCAL_BLOQUEIO DLB 
                          WHERE DLB.DB_EST_ID = NVL(FC.AO_ID_INT_OR, FC.AO_ID_EST_OR) 
                            AND NVL(DLB.DB_CNPJ_RAIZ, SUBSTR(EXPEDIDOR.EP_CGC_EMP, 1, 8)) = SUBSTR(EXPEDIDOR.EP_CGC_EMP, 1, 8)
                            AND DLB.DB_COD_SEGMENTO = FC.FX_COD_SEGMENTO
                            AND ROWNUM = 1)
                        , 0) = 1
                THEN BO.DATA_CONFIRMADO
                ELSE BO.BO_TIMESTAMP
                 END AS ""HorarioRecebimentoFaturamento""

          , CASE WHEN BO.NOM_ARQ = 'EDI' THEN EDI.DATA_CADASTRO
                                         ELSE BO.BO_TIMESTAMP
                                          END AS ""HorarioRecebimentoArquivo""
          , NULL AS ""HorarioFaturamento""
          
          , CASE WHEN 
                    NVL(
                        (SELECT 1 
                           FROM DESPACHO_LOCAL_BLOQUEIO DLB 
                          WHERE DLB.DB_EST_ID = NVL(FC.AO_ID_INT_OR, FC.AO_ID_EST_OR) 
                            AND NVL(DLB.DB_CNPJ_RAIZ, SUBSTR(EXPEDIDOR.EP_CGC_EMP, 1, 8)) = SUBSTR(EXPEDIDOR.EP_CGC_EMP, 1, 8)
                            AND DLB.DB_COD_SEGMENTO = FC.FX_COD_SEGMENTO
                            AND ROWNUM = 1)
                        , 0) = 1
                THEN BO.BO_TIMESTAMP
                ELSE NULL
                 END AS ""HorarioLiberacao1380""

            /* Usuário Evento de Carregamento */
          , '' AS ""UsuarioEventoCarregamento""

          , NVL(BV.MD, 'NAO') AS ""MultiploDespacho""

       FROM BO_BOLAR_BO         BO
  LEFT JOIN USUARIO             USU         ON USU.US_IDT_USU = BO.USU_ID_CONFIRMADO
 INNER JOIN BO_BOLAR_VAGAO      BV          ON BV.ID_BO_ID = BO.ID_BO
 INNER JOIN VAGAO               VG          ON VG.VG_COD_VAG = BV.VG_COD_VAG
 INNER JOIN SERIE_VAGAO         SV          ON SV.SV_ID_SV = VG.SV_ID_SV 
 INNER JOIN FOLHA_ESPECIF_VAGAO FE          ON FE.FC_ID_FC = VG.FC_ID_FC
 INNER JOIN ESTADO_VAGAO_NOVA   EVN         ON EVN.VG_ID_VG = VG.VG_ID_VG
  LEFT JOIN VAGAO_PATIO_VIG     VPV         ON VPV.VG_ID_VG = VG.VG_ID_VG
  LEFT JOIN VAGAO_PEDIDO_VIG    VPE         ON VPE.VG_ID_VG = VG.VG_ID_VG AND VPE.VB_DAT_CAR > TRUNC(BO.BO_TIMESTAMP - 1)
 INNER JOIN FLUXO_COMERCIAL     FC          ON SUBSTR(FC.FX_COD_FLX, 3) = TO_CHAR(BO.COD_FLX)
 INNER JOIN AREA_OPERACIONAL    ORI         ON ORI.AO_ID_AO = FC.AO_ID_EST_OR
 INNER JOIN AREA_OPERACIONAL    DST         ON DST.AO_ID_AO = FC.AO_ID_EST_DS
  LEFT JOIN AREA_OPERACIONAL    AOF         ON AOF.AO_ID_AO = VPV.AO_ID_AO
  LEFT JOIN AREA_OPERACIONAL    AOM         ON AOM.AO_ID_AO = AOF.AO_ID_AO_INF
 INNER JOIN EMPRESA             EXPEDIDOR   ON EXPEDIDOR.EP_ID_EMP = FC.EP_ID_EMP_REM
 INNER JOIN BO_BOLAR_IMP_CFG    CFG         ON CFG.CLIENTE = BO.AO_COD_AOP
 INNER JOIN MERCADORIA          M           ON M.MC_ID_MRC = FC.MC_ID_MRC
 INNER JOIN PAINEL_EXPEDICAO_ORIGENS POR    ON POR.ID_PE_OR_ID = ORI.AO_ID_AO

  LEFT JOIN (
       SELECT WAM.ID_VAGAO_BO
            , WR.DATA_CADASTRO
         FROM EDI.WS_MENSAGEM_RECEBIMENTO   WR
         JOIN EDI.WS_ASSOCIA_MSG_REC_VAGAO WAM ON WAM.ID_MENSAGEM_RECEBIMENTO = WR.ID_MENSAGEM_RECEBIMENTO
        WHERE WR.DATA_CADASTRO BETWEEN TO_DATE('{DataInicialFiltro}', 'MM/DD/YYYY') - 10
                                   AND TO_DATE('{DataFinalFiltro}', 'MM/DD/YYYY')
                            ) EDI          ON EDI.ID_VAGAO_BO = BV.ID_VAGAO

      WHERE BO.IDT_SIT      NOT IN ('D', 'E')
        AND NVL(VPE.VB_COD_TRA, ' ') != 'BOI' /* Nao foram faturados manualmente pela 1131 */
        AND BO.BO_TIMESTAMP >= TO_DATE('{DataInicialFiltro}', 'MM/DD/YYYY')
        AND BO.BO_TIMESTAMP  < TO_DATE('{DataFinalFiltro}', 'MM/DD/YYYY')

        {FluxoFiltro}
        {OrigemFiltro}
        {DestinoFiltro}
        {VagoesFiltro}
        {SituacaoFiltro}
        {LotacaoFiltro}
        {LocalFiltro}
        {ClienteFiltro}
        {TerminalFaturamentoFiltro}
        {MercadoriaFiltro}
        {SegmentoFiltro}

        {Ordenacao}
");

            hql.Replace("{DataInicialFiltro}", dataInicio.ToString("MM/dd/yyyy"));
            hql.Replace("{DataFinalFiltro}", dataFinal.AddDays(1).ToString("MM/dd/yyyy"));

            // Filtro: Fluxo
            fluxo = fluxo.ToUpper();
            if (!string.IsNullOrWhiteSpace(fluxo))
            {
                hql.Replace("{FluxoFiltro}", "AND FC.FX_COD_FLX LIKE '%" + fluxo + "%'");
            }
            else
                hql.Replace("{FluxoFiltro}", "");


            // Filtro: Origem
            origem = origem.ToUpper();
            if (!string.IsNullOrWhiteSpace(origem))
            {
                hql.Replace("{OrigemFiltro}", "AND ORI.AO_COD_AOP = '" + origem + "'");
            }
            else
                hql.Replace("{OrigemFiltro}", "");

            // Filtro: Destino
            destino = destino.ToUpper();
            if (!string.IsNullOrWhiteSpace(destino))
            {
                hql.Replace("{DestinoFiltro}", "AND DST.AO_COD_AOP = '" + destino + "'");
            }
            else
                hql.Replace("{DestinoFiltro}", "");

            // Filtro: Vagões
            if (vagoes.Length > 0)
            {
                string vagoesList = "";
                foreach (string v in vagoes)
                {
                    if (String.IsNullOrEmpty(vagoesList))
                        vagoesList = "'" + v + "'";
                    else
                        vagoesList = vagoesList + ", '" + v + "'";
                }

                if (!(String.IsNullOrEmpty(vagoesList)))
                    hql.Replace("{VagoesFiltro}", "AND VG.VG_COD_VAG in ( " + vagoesList + ")");
                else
                    hql.Replace("{VagoesFiltro}", "");
            }
            else
                hql.Replace("{VagoesFiltro}", "");

            // Filtro Situação
            if (!String.IsNullOrEmpty(situacao))
            {
                int situacaoId;
                if (int.TryParse(situacao, out situacaoId) && situacaoId > 0)
                {
                    hql.Replace("{SituacaoFiltro}", String.Format("AND EVN.SI_IDT_STC = {0}", situacao));
                }
                else
                {
                    hql.Replace("{SituacaoFiltro}", String.Empty);
                }
            }
            else
            {
                hql.Replace("{SituacaoFiltro}", String.Empty);
            }

            // Filtro Lotação
            if (!String.IsNullOrEmpty(lotacao))
            {
                int lotacaoId;
                if (int.TryParse(lotacao, out lotacaoId) && lotacaoId > 0)
                {
                    hql.Replace("{LotacaoFiltro}", String.Format("AND EVN.SH_IDT_STC = {0}", lotacao));
                }
                else
                {
                    hql.Replace("{LotacaoFiltro}", String.Empty);
                }
            }
            else
            {
                hql.Replace("{LotacaoFiltro}", String.Empty);
            }

            // Filtro: Local Atual
            localAtual = localAtual.ToUpper();
            if (!string.IsNullOrWhiteSpace(localAtual))
            {
                hql.Replace("{LocalFiltro}", "AND AOM.AO_COD_AOP = '" + localAtual + "'");

                hql.Replace("{LocalFiltro2}", "AND exists (SELECT 1 " +
                                                          " FROM MOVIMENTACAO_TREM MTR " +
                                                          "     JOIN AREA_OPERACIONAL L " +
                                                                    " on MTR.AO_ID_AO = L.AO_ID_AO " +
                                                          "     LEFT JOIN AREA_OPERACIONAL AOM " +
                                                                    " on AOM.AO_ID_AO = L.AO_ID_AO_INF " +
                                                          " WHERE MTR.MT_ID_MOV = TR.MT_ID_MOV " +
                                                          " AND (AOM.AO_COD_AOP = '" + localAtual + "' OR " +
                                                          "      L.AO_COD_AOP = '" + localAtual + "' OR " +
                                                          "      ORI_TREM.AO_COD_AOP = '" + localAtual + "' ) ) ");
            }
            else
            {
                hql.Replace("{LocalFiltro}", "");
                hql.Replace("{LocalFiltro2}", "");
            }
            // Filtro: Cliente
            if (!string.IsNullOrWhiteSpace(cliente))
            {
                var clienteId = 0;
                if (int.TryParse(cliente, out clienteId) && clienteId > 0)
                {
                    hql.Replace("{ClienteFiltro}", "AND CFG.ID_BO_BOLAR_IMP_CFG = '" + cliente + "'");
                }
                else
                {
                    hql.Replace("{ClienteFiltro}", "");
                }
            }
            else
            {
                hql.Replace("{ClienteFiltro}", "");
            }

            // Filtro: Terminal de Faturamento
            if (!String.IsNullOrEmpty(terminalFaturamento))
            {
                hql.Replace("{TerminalFaturamentoFiltro}", "AND UPPER(EXPEDIDOR.EP_DSC_RSM) LIKE '%" + terminalFaturamento.ToUpper() + "%'");
            }
            else
            {
                hql.Replace("{TerminalFaturamentoFiltro}", String.Empty);
            }

            // Filtro: Mercadorias
            if (mercadorias.Length > 0)
            {
                string mercadoriasList = "";
                foreach (string v in mercadorias)
                {
                    if (String.IsNullOrEmpty(mercadoriasList))
                        mercadoriasList = "'" + v + "'";
                    else
                        mercadoriasList = mercadoriasList + ", '" + v + "'";
                }

                if (!(String.IsNullOrEmpty(mercadoriasList)))
                    hql.Replace("{MercadoriaFiltro}", "AND M.MC_COD_MRC in ( " + mercadoriasList + ")");
                else
                    hql.Replace("{MercadoriaFiltro}", "");
            }
            else
                hql.Replace("{MercadoriaFiltro}", "");


            // Filtro: Segmento
            if (!string.IsNullOrWhiteSpace(segmento))
            {
                hql.Replace("{SegmentoFiltro}", "AND FC.FX_COD_SEGMENTO = '" + segmento + "'");
            }
            else
                hql.Replace("{SegmentoFiltro}", "");

            if (detalhesPaginacao != null)
            {
                // Order by (Recupera o indice e o direcionamento (ASC/DESC))
                var colunasOrdenacao = detalhesPaginacao.ColunasOrdenacao;
                var direcoesOrdenacao = detalhesPaginacao.DirecoesOrdenacao;
                string colOrdenacaoIndex = "14";
                string ordenacaoDirecao = "ASC";

                if (colunasOrdenacao.Count > 0)
                    colOrdenacaoIndex = colunasOrdenacao[0];
                if (direcoesOrdenacao.Count > 0)
                    ordenacaoDirecao = direcoesOrdenacao[0];

                if (colOrdenacaoIndex.ToLower() == "vagao")
                    colOrdenacaoIndex = "1";
                else if (colOrdenacaoIndex.ToLower() == "lotacao")
                    colOrdenacaoIndex = "2";
                else if (colOrdenacaoIndex.ToLower() == "situacao")
                    colOrdenacaoIndex = "3";
                else if (colOrdenacaoIndex.ToLower() == "pesott")
                    colOrdenacaoIndex = "4";
                else if (colOrdenacaoIndex.ToLower() == "pesotu")
                    colOrdenacaoIndex = "5";
                else if (colOrdenacaoIndex.ToLower() == "pesotb")
                    colOrdenacaoIndex = "6";
                else if (colOrdenacaoIndex.ToLower() == "fluxo")
                    colOrdenacaoIndex = "7";
                else if (colOrdenacaoIndex.ToLower() == "origem")
                    colOrdenacaoIndex = "8";
                else if (colOrdenacaoIndex.ToLower() == "destino")
                    colOrdenacaoIndex = "9";
                else if (colOrdenacaoIndex.ToLower() == "terminalfaturamento")
                    colOrdenacaoIndex = "10";
                else if (colOrdenacaoIndex.ToLower() == "aprovador")
                    colOrdenacaoIndex = "11";
                else if (colOrdenacaoIndex.ToLower() == "cliente")
                    colOrdenacaoIndex = "12";
                else if (colOrdenacaoIndex.ToLower() == "mercadoria")
                    colOrdenacaoIndex = "13";
                else if (colOrdenacaoIndex.ToLower() == "local")
                    colOrdenacaoIndex = "14";
                else if (colOrdenacaoIndex.ToLower() == "trem")
                    colOrdenacaoIndex = "15";
                else if (colOrdenacaoIndex.ToLower() == "faturado")
                    colOrdenacaoIndex = "16";
                else if (colOrdenacaoIndex.ToLower() == "faturadomanual")
                    colOrdenacaoIndex = "17";
                else if (colOrdenacaoIndex.ToLower() == "errofaturamento")
                    colOrdenacaoIndex = "18";
                else if (colOrdenacaoIndex.ToLower() == "ctestatus")
                    colOrdenacaoIndex = "19";
                else if (colOrdenacaoIndex.ToLower() == "segmento")
                    colOrdenacaoIndex = "20";
                else if (colOrdenacaoIndex.ToLower() == "ticket")
                    colOrdenacaoIndex = "21";
                else if (colOrdenacaoIndex.ToLower() == "data")
                    colOrdenacaoIndex = "22";
                else if (colOrdenacaoIndex.ToLower() == "databo")
                    colOrdenacaoIndex = "23";
                else if (colOrdenacaoIndex.ToLower() == "horariofaturamento")
                    colOrdenacaoIndex = "24";
                else if (colOrdenacaoIndex.ToLower() == "horariorecebimentofaturamento")
                    colOrdenacaoIndex = "25";
                else if (colOrdenacaoIndex.ToLower() == "horariorecebimentoarquivo")
                    colOrdenacaoIndex = "26";
                else if (colOrdenacaoIndex.ToLower() == "usuariofaturamento")
                    colOrdenacaoIndex = "27";
                else if (colOrdenacaoIndex.ToLower() == "multiplodespacho")
                    colOrdenacaoIndex = "28";
                else
                {
                    colOrdenacaoIndex = "24";
                    ordenacaoDirecao = "ASC";
                }

                hql.Replace("{Ordenacao}", " ORDER BY " + colOrdenacaoIndex + " " + ordenacaoDirecao);
            }
            else
            {
                hql.Replace("{Ordenacao}", String.Empty);
            }

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(hql.ToString());

                foreach (var p in parameters)
                {
                    p.Invoke(query);
                }

                query.SetResultTransformer(Transformers.AliasToBean<PainelExpedicaoConsultaDto>());
                query.SetTimeout(300);
                var itens = query.List<PainelExpedicaoConsultaDto>();

                // Eliminando os registros duplicados.
                // Não é possível corrigir a query(repository) pois a query está certa. O erro está decorrente do cliente que
                // enviando vários registros iguais e isso está duplicando na tela
                // Isso normalmente ocorre quando o cliente envia um registro e da erro durante o faturamento (363). A RumoALL solicita 
                // que o cliente reenvie novamente fazendo com que tenha dois registros no BOLAR_BO. Um com erro e um faturado.
                var resultadosFiltrados = new List<PainelExpedicaoConsultaDto>();
                int registrosEliminados = 0;
                foreach (var item in itens)
                {
                    bool registroUnico = itens.Count(r => r.Vagao == item.Vagao &&
                                                          r.Fluxo == item.Fluxo &&
                                                          r.Origem == item.Origem &&
                                                          r.Destino == item.Destino &&
                                                          r.TerminalFaturamento == item.TerminalFaturamento &&
                                                          r.Cliente == item.Cliente &&
                                                          r.Mercadoria == item.Mercadoria &&
                                                          r.Data_Bo == item.Data_Bo) == 1;
                    if (registroUnico)
                    {
                        resultadosFiltrados.Add(item);
                    }
                    else
                    {
                        var registroInserido = resultadosFiltrados.FirstOrDefault(r => r.Vagao == item.Vagao &&
                                                                                       r.Fluxo == item.Fluxo &&
                                                                                       r.Origem == item.Origem &&
                                                                                       r.Destino == item.Destino &&
                                                                                       r.TerminalFaturamento == item.TerminalFaturamento &&
                                                                                       r.Cliente == item.Cliente &&
                                                                                       r.Mercadoria == item.Mercadoria &&
                                                                                       r.Data_Bo == item.Data_Bo);
                        if (registroInserido == null)
                        {
                            resultadosFiltrados.Add(item);
                        }
                        else
                        {
                            /* Atualizando o registro que já foi inserido */
                            #region Atualização do registro

                            if (registroInserido.Faturado == "N")
                            {
                                registroInserido.Faturado = item.HorarioFaturamento.HasValue ? "S" : "N";
                            }

                            if (!registroInserido.Data.HasValue)
                            {
                                registroInserido.Data = item.Data;
                            }

                            if (!registroInserido.HorarioFaturamento.HasValue)
                            {
                                registroInserido.HorarioFaturamento = item.HorarioFaturamento;
                            }

                            if (String.IsNullOrEmpty(registroInserido.Trem))
                            {
                                registroInserido.Trem = item.Trem;
                            }

                            if (String.IsNullOrEmpty(registroInserido.CteStatus))
                            {
                                registroInserido.CteStatus = item.CteStatus;
                            }

                            if (registroInserido.Ticket == "-")
                            {
                                registroInserido.Ticket = item.Ticket;
                            }

                            if (String.IsNullOrEmpty(registroInserido.UsuarioEventoCarregamento))
                            {
                                registroInserido.UsuarioEventoCarregamento = item.UsuarioEventoCarregamento;
                            }

                            registroInserido.ErroFaturamento = String.IsNullOrEmpty(registroInserido.ErroFaturamento)
                                                                   ? item.ErroFaturamento
                                                                   : registroInserido.ErroFaturamento;

                            registroInserido.PesoRealCarregado = registroInserido.PesoRealCarregado + item.PesoRealCarregado;

                            registroInserido.PesoBruto = registroInserido.PesoRealCarregado + registroInserido.PesoTara;

                            #endregion

                            // Caso for identificado que não devemos incluí-lo, devemos aumentar a variável contador de registros eliminados
                            // para atualizar depois o total de registros da grid
                            registrosEliminados++;
                        }
                    }
                }

                var total = resultadosFiltrados.Count();
                // Atualizando a paginação manualmente
                if (detalhesPaginacao != null)
                {
                    var inicio = detalhesPaginacao.Inicio ?? 0;
                    var limite = detalhesPaginacao.Limite ?? DetalhesPaginacao.LimitePadrao;

                    resultadosFiltrados.RemoveRange(0, inicio);
                    if (resultadosFiltrados.Count() - limite > 0)
                    {
                        resultadosFiltrados.RemoveRange(limite, resultadosFiltrados.Count() - limite);
                    }
                }

                var result = new ResultadoPaginado<PainelExpedicaoConsultaDto>
                {
                    Items = resultadosFiltrados,
                    Total = (long)total
                };

                return result;
            }
        }

        public ResultadoPaginado<PainelExpedicaoConsultarDocumentosDto> ObterPainelExpedicaoConsultaDocumentos(
                                                                            DetalhesPaginacao detalhesPaginacao,
                                                                            DateTime dataInicio,
                                                                            DateTime dataFinal,
                                                                            string fluxo,
                                                                            string origem,
                                                                            string destino,
                                                                            string[] vagoes,
                                                                            string cliente,
                                                                            string expedidor,
                                                                            string remetente,
                                                                            string recebedor,
                                                                            string[] mercadorias,
                                                                            string segmento,
                                                                            string os,
                                                                            string prefixo,
                                                                            bool statusCte,
                                                                            bool statusNfe,
                                                                            bool statusWebDanfe,
                                                                            bool statusTicket,
                                                                            bool statusDcl,
                                                                            bool statusLaudoMercadoria,
                                                                            bool outrasFerrovias,
                                                                            string[] itensDespacho,
                                                                            string cnpjRaizRecebedor,
                                                                            string malhaOrigem,
                                                                            bool exportarExcel)
        {
            #region SQL Query

            var parameters = new List<Action<IQuery>>();
            var sql = new StringBuilder(@"
            SELECT TMP.COD_VAGAO            AS ""Vagao""
                 , TMP.DATA_CARGA           AS ""DataCarga""
                 , TMP.ID_DESPACHO          AS ""IdDespacho""
                 , TMP.ID_DESPACHO_ITEM     AS ""IdDespachoItem""
                 , TMP.ID_CTE               AS ""IdCte""
                 , TMP.FLUXO                AS ""Fluxo""
                 , TMP.ORIGEM               AS ""Origem""
                 , TMP.DESTINO              AS ""Destino""
                 , TMP.CLIENTE              AS ""Cliente""
                 , TMP.EXPEDIDOR            AS ""Expedidor""
                 , TMP.RECEBEDOR            AS ""Recebedor""
                 , TMP.REMETENTE_FISCAL     AS ""RemetenteFiscal""
                 , TMP.COD_MERCADORIA       AS ""CodigoMercadoria""
                 , TMP.DSC_MERCADORIA       AS ""DescricaoMercadoria""
                 , TMP.OS                   AS ""Os""
                 , TMP.PREFIXO              AS ""Prefixo""
                 , TMP.SEQ                  AS ""Seq""
                 , TMP.CTE                  AS ""PossuiCte""
                 , TMP.SEGMENTO             AS ""Segmento""
                 , TMP.TICKET               AS ""PossuiTicket""
                 , TMP.NFE                  AS ""PossuiNfe""
                 , TMP.WEB_DANFE            AS ""PossuiWebDanfe""
                 , TMP.PdfsRecebidos        AS ""QuantidadeNfeRecebidas""
                 , TMP.NotasTotais          AS ""TotalNfe""
                 , TMP.NUMCTE               AS ""NumeroCte""
                 , TMP.USU_FAT              AS ""UsuarioEventoCarregamento""
                 , TMP.CNPJ_RECEBEDOR       AS ""CnpjRecebedor""
                 , TMP.MDFE                 AS ""PossuiMdfe""
                 , TMP.ID_MDFE              AS ""IdMdfe""
                 , TMP.XML_NFE              AS ""PossuiXmlNfe""
                 , TMP.MALHA_ORIGEM         AS ""MalhaOrigem""
                 , TMP.LAUDO_MERCADORIA     AS ""PossuiLaudoMercadoria""
                 , TMP.OUTRAS_FERROVIAS     AS ""OutrasFerrovias""
              FROM (

                SELECT VG.VG_COD_VAG AS COD_VAGAO,
                       DP.DP_DT AS DATA_CARGA,
                       DP.DP_ID_DP AS ID_DESPACHO,
                        (select IDS2.ID_ID_ITD 
                        FROM ITEM_DESPACHO IDS2 
                        where IDS2.DP_ID_DP = DP.DP_ID_DP 
                        AND IDS2.VG_ID_VG = VG.VG_ID_VG
                        AND rownum = 1) AS ID_DESPACHO_ITEM,
                       (SELECT MAX(C.ID_CTE)
                          FROM CTE C
                         WHERE C.DP_ID_DP = DP.DP_ID_DP) AS ID_CTE,
                       FC.FX_COD_FLX AS FLUXO,
                       NVL(ORIF1.AO_COD_AOP, ORIF.AO_COD_AOP) AS ORIGEM,
                       NVL(DSTF1.AO_COD_AOP, DSTF.AO_COD_AOP) AS DESTINO,
                       COR.EP_DSC_RSM AS CLIENTE,
                       EXPEDIDOR.EP_DSC_RSM AS EXPEDIDOR,
                       RECEBEDOR.EP_DSC_RSM AS RECEBEDOR,
                       NVl( (SELECT NVL(RF2.EP_DSC_RSM, RF1.EP_DSC_RSM)
                            FROM NOTA_FISCAL NF
                         LEFT JOIN EMPRESA     RF1 ON RF1.EP_CGC_EMP = NF.EP_CGC_REM
                         LEFT JOIN VW_NFE VW ON VW.NFE_CHAVE = NF.NFE_CHAVE_NFE
                         LEFT JOIN EMPRESA     RF2 ON RF2.EP_CGC_EMP = VW.NFE_CNPJ_ORI
                            WHERE NF.DP_ID_DP = IDS.DP_ID_DP
                            AND rownum = 1) , '') AS REMETENTE_FISCAL,
                       M.MC_COD_MRC AS COD_MERCADORIA,
                       M.MC_DRS_PT AS DSC_MERCADORIA,
                       AOM.AO_COD_AOP AS LOCAL_ATUAL,
                       NULL AS OS,
                       NULL AS PREFIXO,
                       EV.EV_COD_ELV AS COD_LINHA,
                       EV.EV_DSC_PT AS DSC_LINHA,
                       VPV.VP_NUM_SEQ AS SEQ,
                       (SELECT C1.SITUACAO_ATUAL
                          FROM CTE C1
                            INNER JOIN CTE_ARQUIVO ARQ ON ARQ.ID_CTE = C1.ID_CTE
                          WHERE C1.ID_CTE =(SELECT MAX(C.ID_CTE)
                                            FROM CTE C
                                            WHERE C.DP_ID_DP = DP.DP_ID_DP)) CTE,
                       FC.FX_COD_SEGMENTO AS SEGMENTO,
                       /* Subquery - Ticket */
                       NVL((
                           SELECT CASE WHEN TB.ID IS NOT NULL THEN 'S' ELSE 'N' END
                             FROM NOTA_FISCAL        NF
                             JOIN NOTA_TICKET_VAGAO  NTV ON NTV.CHAVE_NFE = NF.NFE_CHAVE_NFE
                             JOIN VAGAO_TICKET       VT ON VT.ID = NTV.VAGAO_TICKET_ID
                             JOIN TICKET_BALANCA     TB ON TB.ID = VT.TICKET_BALANCA_ID
                             LEFT JOIN CONF_GERAL    CG ON CG.CHAVE = 'INTERVALO_DIAS_TICKET_TSP'
                            WHERE ROWNUM = 1
                              AND (VT.DATA_CADASTRO BETWEEN TRUNC(DP.DP_DT - 3) AND TRUNC(DP.DP_DT + 3)
                              OR   VT.DATA_CADASTRO BETWEEN TRUNC(DP.DP_DT - NVL(CG.VALOR , 3)) 
                              AND TRUNC(DP.DP_DT + NVL(CG.VALOR, 3)) AND (VT.TU=IDS.id_num_pcl*100))
                              AND IDS.DP_ID_DP = NF.DP_ID_DP
                              AND IDS.VG_ID_VG = VG.VG_ID_VG
                              AND LPAD(VT.VG_COD_VAG, 7, '0') = VG.VG_COD_VAG)
                       , 'N') AS TICKET,

                        /* Subquery - Nfe */
                            CASE WHEN NVL(AGRUPADOR.PdfsRecebidos, 0) > 0
                                 THEN 'S'
                                 ELSE 'N'
                                  END AS NFE,

                        CASE WHEN NVL(AGRUPADOR.PdfsRecebidos, 0) != NVL(AGRUPADOR.NotasTotais, 0)
                             THEN 'S'
                             ELSE 'N'
                              END AS WEB_DANFE,

                        NVL(AGRUPADOR.PdfsRecebidos, 0) AS PdfsRecebidos,
                        NVL(AGRUPADOR.NotasTotais  , 0) AS NotasTotais,

                        (SELECT C1.NRO_CTE
                          FROM CTE C1
                            INNER JOIN CTE_ARQUIVO ARQ ON ARQ.ID_CTE = C1.ID_CTE
                          WHERE C1.ID_CTE =(SELECT MAX(C.ID_CTE)
                                            FROM CTE C
                                            WHERE C.DP_ID_DP = DP.DP_ID_DP)
                            AND C1.SITUACAO_ATUAL = 'AUT') NUMCTE,

                        Nvl( ( Select USU.US_USR_IDU || ' - ' || USU.US_NOM_USU
                        FROM EVENTO_VAGAO EVG
                             JOIN USUARIO USU ON USU.US_USR_IDU = EVG.EA_MAT_USU 
                        WHERE EVG.en_id_evt = 13 /* Evento Carregar Vagão */
                              AND EVG.VG_ID_VG = VG.VG_ID_VG
                              AND TO_DATE(to_char(EVG.EA_DTO_EVG, 'YYYY/MM/DD HH24:MI'), 'YYYY/MM/DD HH24:MI')
                                  = TO_DATE(to_char(DP.DP_DT, 'YYYY/MM/DD HH24:MI'), 'YYYY/MM/DD HH24:MI')
                              AND rownum = 1
                              ), '' ) AS USU_FAT

                         /* Recebedor */
                        ,(Select RECEB.ep_cgc_emp
                            FROM EMPRESA RECEB
                            WHERE RECEB.Ep_Id_Emp = FC.EP_ID_EMP_DST
                            AND rownum = 1
                        ) as CNPJ_RECEBEDOR

                        /*, 'N' AS MDFE
                          , NULL AS ID_MDFE */


                        , NVL( ( select 'S'
                           from mdfe_composicao MDFE
                           WHERE MDFE.ID_CTE = (SELECT MAX(C.ID_CTE)
                                             FROM CTE C
                                             WHERE C.DP_ID_DP = DP.DP_ID_DP and C.Situacao_Atual = 'AUT')
                           and MDFE.DP_ID_DP = DP.DP_ID_DP
                           and rownum = 1
                         ),'N') AS MDFE
                         
                        , ( select MDFE.ID_MDFE
                           from mdfe_composicao MDFE
                           WHERE MDFE.ID_CTE = (SELECT MAX(C.ID_CTE)
                                             FROM CTE C
                                             WHERE C.DP_ID_DP = DP.DP_ID_DP and C.Situacao_Atual = 'AUT')
                           and MDFE.DP_ID_DP = DP.DP_ID_DP
                           and rownum = 1
                         ) AS ID_MDFE

                        , NVL(
	                        (
	                        SELECT 'S' 
	                          FROM NOTA_FISCAL NF
	                          JOIN (
		                        SELECT EDI.NFE_CHAVE AS CHAVE_NFE 
                                  FROM NFE_DISTRIBUICAO EDI
                                 WHERE EDI.NFE_XML IS NOT NULL
		                         UNION
		                        SELECT EDI.NFE_CHAVE AS CHAVE_NFE 
                                  FROM EDI.EDI2_NFE EDI
                                 WHERE EDI.NFE_XML IS NOT NULL
		                           ) TABELA_NFE ON TABELA_NFE.CHAVE_NFE = NF.NFE_CHAVE_NFE
	                         WHERE NF.DP_ID_DP = IDS.DP_ID_DP
	                           AND ROWNUM = 1
	                           ), 'N') AS XML_NFE

                        , MALHAORIGEM.ML_DSC_ML AS MALHA_ORIGEM
                        , CASE WHEN LAUDO.ID_WS_LAUDO IS NOT NULL THEN 'S' ELSE 'N' END AS LAUDO_MERCADORIA
                        , CASE WHEN C.CZ_FERROVIA_FAT IN ('E', 'F') THEN 'S' ELSE 'N' END AS OUTRAS_FERROVIAS 
                  FROM VAGAO_PATIO_VIG      VPV
            INNER JOIN ELEMENTO_VIA         EV              ON EV.EV_ID_ELV = VPV.EV_ID_ELV
            INNER JOIN VAGAO                VG              ON VG.VG_ID_VG = VPV.VG_ID_VG
            INNER JOIN AREA_OPERACIONAL     AOF             ON AOF.AO_ID_AO = VPV.AO_ID_AO
            INNER JOIN AREA_OPERACIONAL     AOM             ON AOM.AO_ID_AO = AOF.AO_ID_AO_INF
            INNER JOIN VAGAO_PEDIDO_VIG     VPED            ON VPED.VG_ID_VG = VG.VG_ID_VG
            INNER JOIN FLUXO_COMERCIAL      FC              ON FC.FX_ID_FLX = VPED.FX_ID_FLX
            INNER JOIN CONTRATO             C               ON C.CZ_ID_CTT = FC.CZ_ID_CTT
            INNER JOIN MERCADORIA           M               ON M.MC_ID_MRC = FC.MC_ID_MRC
            INNER JOIN EMPRESA              EXPEDIDOR       ON EXPEDIDOR.EP_ID_EMP = FC.EP_ID_EMP_REM
            INNER JOIN EMPRESA              COR             ON COR.EP_ID_EMP = FC.EP_ID_EMP_COR
            INNER JOIN EMPRESA              RECEBEDOR       ON RECEBEDOR.EP_ID_EMP = FC.EP_ID_EMP_DST
            INNER JOIN AREA_OPERACIONAL     ORIF            ON ORIF.AO_ID_AO = FC.AO_ID_EST_OR
             LEFT JOIN AREA_OPERACIONAL     ORIF1           ON ORIF1.AO_ID_AO = FC.AO_ID_INT_OR
            INNER JOIN UNID_PRODUCAO        UNID            ON UNID.UP_ID_UNP = NVL(ORIF1.UP_ID_UNP, ORIF.UP_ID_UNP)
            INNER JOIN MALHA                MALHAORIGEM     ON MALHAORIGEM.ML_CD_ML = UNID.UP_IND_MALHA
            INNER JOIN AREA_OPERACIONAL     DSTF            ON DSTF.AO_ID_AO = FC.AO_ID_EST_DS
             LEFT JOIN AREA_OPERACIONAL     DSTF1           ON DSTF1.AO_ID_AO = FC.AO_ID_INT_DS
            INNER JOIN CARREGAMENTO         CG              ON CG.CG_ID_CAR = VPED.CG_ID_CAR
            INNER JOIN DETALHE_CARREGAMENTO DC              ON CG.CG_ID_CAR = DC.CG_ID_CAR AND DC.VG_ID_VG = VG.VG_ID_VG
            INNER JOIN ITEM_DESPACHO        IDS             ON IDS.DC_ID_CRG = DC.DC_ID_CRG
            INNER JOIN DESPACHO             DP              ON DP.DP_ID_DP = IDS.DP_ID_DP

             LEFT JOIN (
                    SELECT COUNT(DISTINCT NVL(PDF_EDI.NFE_ID_NFE, PDF_SIM.NFE_ID_NFE)) AS PdfsRecebidos
                         , COUNT(DISTINCT  NF.NF_NUM_NFL) AS NotasTotais
                         , NF.DP_ID_DP AS DP_ID_DP
                      FROM NOTA_FISCAL       NF
                INNER JOIN DESPACHO         DP_NF ON DP_NF.DP_ID_DP = NF.DP_ID_DP
                 LEFT JOIN EDI.EDI2_NFE     NFE ON NFE.NFE_CHAVE  = NF.NFE_CHAVE_NFE
                 LEFT JOIN NFE_SIMCONSULTAS SIM ON SIM.NFE_CHAVE  = NF.NFE_CHAVE_NFE
                 LEFT JOIN EDI.EDI2_NFE_PDF PDF_EDI ON PDF_EDI.NFE_ID_NFE = NFE.NFE_ID_NFE
                 LEFT JOIN EDI.EDI2_NFE_PDF PDF_SIM ON PDF_SIM.NFE_ID_SIMCONSULTAS = SIM.NFE_ID_NFE
                     WHERE DP_NF.DP_DT BETWEEN TO_DATE('{dataInicio} 00:00:00', 'DD/MM/YYYY HH24:MI:SS') 
                                           AND TO_DATE('{dataFim} 23:59:59', 'DD/MM/YYYY HH24:MI:SS')
                     GROUP BY NF.DP_ID_DP
                       ) AGRUPADOR ON AGRUPADOR.DP_ID_DP = IDS.DP_ID_DP

             LEFT JOIN BO_BOLAR_VAGAO       VAGAO           ON VAGAO.DP_ID_DP = DP.DP_ID_DP
             LEFT JOIN BO_BOLAR_BO          BO              ON BO.ID_BO = VAGAO.ID_BO_ID

             LEFT JOIN (
                    SELECT MAX(WS.ID_WS_LAUDO)   AS ID_WS_LAUDO
                         , WS.CODIGO_VAGAO        AS CODIGO_VAGAO
                         , WS.DATA_CLASSIFICACAO  AS DATA_CLASSIFICACAO
                      FROM WS_LAUDO WS
                     WHERE NVL(WS.ID_DESPACHO, 0) != 0                       
                  GROUP BY WS.CODIGO_VAGAO, WS.DATA_CLASSIFICACAO
             ) LAUDO ON LAUDO.CODIGO_VAGAO = VG.VG_COD_VAG /*LAUDO.ID_DESPACHO = DP.DP_ID_DP*/
                    AND TO_DATE(TO_CHAR(LAUDO.DATA_CLASSIFICACAO,'dd/MM/yyyy'),'dd/MM/yyyy') >= TO_DATE(TO_CHAR(DP.DP_DT - 3,'dd/MM/yyyy'),'dd/MM/yyyy')
                 WHERE DP.DP_DT BETWEEN
                       TO_DATE('{dataInicio} 00:00:00', 'DD/MM/YYYY HH24:MI:SS') AND
                       TO_DATE('{dataFim} 23:59:59', 'DD/MM/YYYY HH24:MI:SS')

                 UNION ALL

                SELECT VG.VG_COD_VAG AS COD_VAGAO,
                       DP.DP_DT AS DATA_CARGA,
                       DP.DP_ID_DP AS ID_DESPACHO,
                       (select IDS2.ID_ID_ITD 
                        FROM ITEM_DESPACHO IDS2 
                        where IDS2.DP_ID_DP = DP.DP_ID_DP 
                        AND IDS2.VG_ID_VG = VG.VG_ID_VG
                        AND rownum = 1) AS ID_DESPACHO_ITEM,
                       (SELECT MAX(C.ID_CTE)
                          FROM CTE C
                         WHERE C.DP_ID_DP = DP.DP_ID_DP) AS ID_CTE, 
                       FC.FX_COD_FLX AS FLUXO,
                       NVL(ORIF1.AO_COD_AOP, ORI.AO_COD_AOP) AS ORIGEM,
                       NVL(DSTF1.AO_COD_AOP, DST.AO_COD_AOP) AS DESTINO,
                       COR.EP_DSC_RSM AS CLIENTE,
                       EXPEDIDOR.EP_DSC_RSM AS EXPEDIDOR,
                       RECEBEDOR.EP_DSC_RSM AS RECEBEDOR,
                       NVl( (SELECT NVL(RF2.EP_DSC_RSM, RF1.EP_DSC_RSM)
                            FROM NOTA_FISCAL NF
                         LEFT JOIN EMPRESA     RF1 ON RF1.EP_CGC_EMP = NF.EP_CGC_REM
                         LEFT JOIN VW_NFE VW ON VW.NFE_CHAVE = NF.NFE_CHAVE_NFE
                         LEFT JOIN EMPRESA     RF2 ON RF2.EP_CGC_EMP = VW.NFE_CNPJ_ORI
                            WHERE NF.DP_ID_DP = IDS.DP_ID_DP
                            AND rownum = 1) , '') AS REMETENTE_FISCAL,
                       M.MC_COD_MRC AS COD_MERCADORIA,
                       M.MC_DRS_PT AS DSC_MERCADORIA,
                       NVL((SELECT CASE
                                    WHEN L.AO_ID_AO_INF IS NOT NULL THEN
                                     (SELECT AOM.AO_COD_AOP
                                        FROM AREA_OPERACIONAL AOM
                                       WHERE AOM.AO_ID_AO = L.AO_ID_AO_INF)
                                    ELSE
                                     L.AO_COD_AOP
                                  END AS LOCAL_ATUAL
                             FROM MOVIMENTACAO_TREM MTR, AREA_OPERACIONAL L
                            WHERE MTR.MT_ID_MOV = TR.MT_ID_MOV
                              AND MTR.AO_ID_AO = L.AO_ID_AO
                          
                           ),
                           ORI_TREM.AO_COD_AOP) AS LOCAL_ATUAL,
                       TOS.X1_NRO_OS AS OS,
                       TR.TR_PFX_TRM AS PREFIXO,
                       NULL AS COD_LINHA,
                       NULL AS DSC_LINHA,
                       CVV.CV_SEQ_CMP AS SEQ,

                       (SELECT C1.SITUACAO_ATUAL
                          FROM CTE C1
                            INNER JOIN CTE_ARQUIVO ARQ ON ARQ.ID_CTE = C1.ID_CTE
                         WHERE C1.ID_CTE = (SELECT MAX(C.ID_CTE)
                                           FROM CTE C
                                           WHERE C.DP_ID_DP = DP.DP_ID_DP)) CTE,
                       FC.FX_COD_SEGMENTO AS SEGMENTO,

                       /* Subquery - Ticket */
                       NVL((
                           SELECT CASE WHEN TB.ID IS NOT NULL THEN 'S' ELSE 'N' END
                             FROM NOTA_FISCAL        NF
                             JOIN NOTA_TICKET_VAGAO  NTV ON NTV.CHAVE_NFE = NF.NFE_CHAVE_NFE
                             JOIN VAGAO_TICKET       VT ON VT.ID = NTV.VAGAO_TICKET_ID
                             JOIN TICKET_BALANCA     TB ON TB.ID = VT.TICKET_BALANCA_ID
                             LEFT JOIN CONF_GERAL    CG ON CG.CHAVE = 'INTERVALO_DIAS_TICKET_TSP'
                            WHERE ROWNUM = 1
                              AND ((VT.DATA_CADASTRO BETWEEN TRUNC(DP.DP_DT - 3) AND TRUNC(DP.DP_DT + 3) AND VPV.VO_IDT_VPT_OPER IS NULL) 
             OR (VT.DATA_CADASTRO BETWEEN TRUNC(DP.DP_DT - NVL(CG.VALOR, 3)) AND TRUNC(DP.DP_DT + NVL(CG.VALOR, 3)) AND VPV.VO_IDT_VPT_OPER IS NOT NULL AND (VT.TU=IDS.ID_NUM_PCL*100)))
                              AND IDS.DP_ID_DP = NF.DP_ID_DP
                              AND LPAD(VT.VG_COD_VAG, 7, '0') = VG.VG_COD_VAG )
                       , 'N') AS TICKET,

                        /* Subquery - Nfe */
                        CASE WHEN NVL(AGRUPADOR.PdfsRecebidos, 0) > 0
                                 THEN 'S'
                                 ELSE 'N'
                                  END AS NFE,

                        CASE WHEN NVL(AGRUPADOR.PdfsRecebidos, 0) != NVL(AGRUPADOR.NotasTotais, 0)
                                 THEN 'S'
                                 ELSE 'N'
                                  END AS WEB_DANFE,

                        NVL(AGRUPADOR.PdfsRecebidos, 0) AS PdfsRecebidos,
                        NVL(AGRUPADOR.NotasTotais  , 0) AS NotasTotais,

                         (SELECT C1.NRO_CTE
                          FROM CTE C1
                            INNER JOIN CTE_ARQUIVO ARQ ON ARQ.ID_CTE = C1.ID_CTE
                         WHERE C1.ID_CTE = (SELECT MAX(C.ID_CTE)
                                           FROM CTE C
                                           WHERE C.DP_ID_DP = DP.DP_ID_DP)
                         AND C1.SITUACAO_ATUAL = 'AUT') NUMCTE,

                         Nvl( ( Select USU.US_USR_IDU || ' - ' || USU.US_NOM_USU
                        FROM EVENTO_VAGAO EVG
                             JOIN USUARIO USU ON USU.US_USR_IDU = EVG.EA_MAT_USU 
                        WHERE EVG.en_id_evt = 13 /* Evento Carregar Vagão */
                              AND EVG.VG_ID_VG = VG.VG_ID_VG
                              AND TO_DATE(to_char(EVG.EA_DTO_EVG, 'YYYY/MM/DD HH24:MI'), 'YYYY/MM/DD HH24:MI')
                                  = TO_DATE(to_char(DP.DP_DT, 'YYYY/MM/DD HH24:MI'), 'YYYY/MM/DD HH24:MI')
                              AND rownum = 1
                              ), '' ) AS USU_FAT

                      /* Recebedor */
                        ,(Select RECEB.ep_cgc_emp
                            FROM EMPRESA RECEB
                            WHERE RECEB.Ep_Id_Emp = FC.EP_ID_EMP_DST
                            AND rownum = 1
                        ) as CNPJ_RECEBEDOR

                        , NVL( 
                            ( 
                            SELECT 'S'
                              FROM MDFE_COMPOSICAO MDFE
                             WHERE MDFE.ID_CTE = (SELECT MAX(C.ID_CTE)
                                                    FROM CTE C
                                                   WHERE C.DP_ID_DP = DP.DP_ID_DP AND C.SITUACAO_ATUAL = 'AUT')
                               AND MDFE.DP_ID_DP = DP.DP_ID_DP
                               AND ROWNUM = 1
                               ), 'N') AS MDFE
                         
                        , ( SELECT MAX(MDFE.ID_MDFE)
                              FROM MDFE_COMPOSICAO MDFE
                             WHERE MDFE.ID_CTE = (SELECT MAX(C.ID_CTE)
                                                    FROM CTE C
                                                   WHERE C.DP_ID_DP = DP.DP_ID_DP AND C.SITUACAO_ATUAL = 'AUT')
                               AND MDFE.DP_ID_DP = DP.DP_ID_DP
                          ) AS ID_MDFE

                        , NVL(
	                        (
	                        SELECT 'S' 
	                          FROM NOTA_FISCAL NF
	                          JOIN (
		                        SELECT EDI.NFE_CHAVE AS CHAVE_NFE 
                                  FROM NFE_DISTRIBUICAO EDI
                                 WHERE EDI.NFE_XML IS NOT NULL
		                         UNION
		                        SELECT EDI.NFE_CHAVE AS CHAVE_NFE 
                                  FROM EDI.EDI2_NFE EDI
                                 WHERE EDI.NFE_XML IS NOT NULL
		                           ) TABELA_NFE ON TABELA_NFE.CHAVE_NFE = NF.NFE_CHAVE_NFE
	                         WHERE NF.DP_ID_DP = IDS.DP_ID_DP
	                           AND ROWNUM = 1
	                           ), 'N') AS XML_NFE

                        , MALHAORIGEM.ML_DSC_ML AS MALHA_ORIGEM
                        , CASE WHEN LAUDO.ID_WS_LAUDO IS NOT NULL THEN 'S' ELSE 'N' END AS LAUDO_MERCADORIA
                        , CASE WHEN C.CZ_FERROVIA_FAT IN ('E', 'F') THEN 'S' ELSE 'N' END AS OUTRAS_FERROVIAS 
                  FROM COMPVAGAO_VIG        CVV
            INNER JOIN VAGAO                VG              ON VG.VG_ID_VG = CVV.VG_ID_VG
            INNER JOIN COMPOSICAO           CP              ON CP.CP_ID_CPS = CVV.CP_ID_CPS
            INNER JOIN TREM                 TR              ON TR.TR_ID_TRM = CP.TR_ID_TRM
            INNER JOIN T2_OS                TOS             ON TOS.X1_ID_OS = TR.OF_ID_OSV
            INNER JOIN VAGAO_PEDIDO_VIG     VPV             ON VPV.VG_ID_VG = CVV.VG_ID_VG AND VPV.PT_ID_ORT = CVV.PT_ID_ORT
            INNER JOIN FLUXO_COMERCIAL      FC              ON FC.FX_ID_FLX = VPV.FX_ID_FLX
            INNER JOIN CONTRATO             C               ON C.CZ_ID_CTT = FC.CZ_ID_CTT
            INNER JOIN AREA_OPERACIONAL     ORI             ON ORI.AO_ID_AO = FC.AO_ID_EST_OR
             LEFT JOIN AREA_OPERACIONAL     ORIF1           ON ORIF1.AO_ID_AO = FC.AO_ID_INT_OR            
            INNER JOIN UNID_PRODUCAO        UNID            ON UNID.UP_ID_UNP = NVL(ORIF1.UP_ID_UNP, ORI.UP_ID_UNP)
            INNER JOIN MALHA                MALHAORIGEM     ON MALHAORIGEM.ML_CD_ML = UNID.UP_IND_MALHA
            INNER JOIN AREA_OPERACIONAL     DST             ON DST.AO_ID_AO = FC.AO_ID_EST_DS
             LEFT JOIN AREA_OPERACIONAL     DSTF1           ON DSTF1.AO_ID_AO = FC.AO_ID_INT_DS
            INNER JOIN EMPRESA              EXPEDIDOR       ON EXPEDIDOR.EP_ID_EMP = FC.EP_ID_EMP_REM
            INNER JOIN EMPRESA              COR             ON COR.EP_ID_EMP = FC.EP_ID_EMP_COR
            INNER JOIN EMPRESA              RECEBEDOR       ON RECEBEDOR.EP_ID_EMP = FC.EP_ID_EMP_DST
            INNER JOIN MERCADORIA           M               ON M.MC_ID_MRC = FC.MC_ID_MRC
            INNER JOIN DETALHE_CARREGAMENTO DC              ON DC.VG_ID_VG = VPV.VG_ID_VG
            INNER JOIN CARREGAMENTO         CG              ON CG.CG_ID_CAR = VPV.CG_ID_CAR AND CG.CG_ID_CAR = DC.CG_ID_CAR
            INNER JOIN ITEM_DESPACHO        IDS             ON IDS.DC_ID_CRG = DC.DC_ID_CRG AND IDS.VG_ID_VG = VPV.VG_ID_VG
            INNER JOIN DESPACHO             DP              ON DP.DP_ID_DP = IDS.DP_ID_DP

             LEFT JOIN (
                    SELECT COUNT(DISTINCT NVL(PDF_EDI.NFE_ID_NFE, PDF_SIM.NFE_ID_NFE)) AS PdfsRecebidos
                         , COUNT(DISTINCT  NF.NF_NUM_NFL) AS NotasTotais
                         , NF.DP_ID_DP AS DP_ID_DP
                      FROM NOTA_FISCAL       NF
                INNER JOIN DESPACHO         DP_NF ON DP_NF.DP_ID_DP = NF.DP_ID_DP
                 LEFT JOIN EDI.EDI2_NFE     NFE ON NFE.NFE_CHAVE  = NF.NFE_CHAVE_NFE
                 LEFT JOIN NFE_SIMCONSULTAS SIM ON SIM.NFE_CHAVE  = NF.NFE_CHAVE_NFE
                 LEFT JOIN EDI.EDI2_NFE_PDF  PDF_EDI ON PDF_EDI.NFE_ID_NFE = NFE.NFE_ID_NFE
                 LEFT JOIN EDI.EDI2_NFE_PDF PDF_SIM ON PDF_SIM.NFE_ID_SIMCONSULTAS = SIM.NFE_ID_NFE
                     WHERE DP_NF.DP_DT BETWEEN TO_DATE('{dataInicio} 00:00:00', 'DD/MM/YYYY HH24:MI:SS')
                                           AND TO_DATE('{dataFim} 23:59:59', 'DD/MM/YYYY HH24:MI:SS')
                     GROUP BY NF.DP_ID_DP
                       ) AGRUPADOR ON AGRUPADOR.DP_ID_DP = IDS.DP_ID_DP

             LEFT JOIN (
                    SELECT MAX(WS.ID_WS_LAUDO)   AS ID_WS_LAUDO
                         , WS.CODIGO_VAGAO        AS CODIGO_VAGAO
                         , WS.DATA_CLASSIFICACAO  AS DATA_CLASSIFICACAO
                      FROM WS_LAUDO WS
                     WHERE NVL(WS.ID_DESPACHO, 0) != 0
                  GROUP BY WS.CODIGO_VAGAO, WS.DATA_CLASSIFICACAO
             ) LAUDO ON LAUDO.CODIGO_VAGAO = VG.VG_COD_VAG /*LAUDO.ID_DESPACHO = DP.DP_ID_DP*/
                    AND TO_DATE(TO_CHAR(LAUDO.DATA_CLASSIFICACAO,'dd/MM/yyyy'),'dd/MM/yyyy') >= TO_DATE(TO_CHAR(DP.DP_DT - 3,'dd/MM/yyyy'),'dd/MM/yyyy')
            INNER JOIN AREA_OPERACIONAL     ORI_TREM        ON ORI_TREM.AO_ID_AO = TR.AO_ID_AO_ORG
             LEFT JOIN BO_BOLAR_VAGAO       VAGAO           ON VAGAO.DP_ID_DP = DP.DP_ID_DP
             LEFT JOIN BO_BOLAR_BO          BO              ON BO.ID_BO = VAGAO.ID_BO_ID

                 WHERE DP.DP_DT BETWEEN
                       TO_DATE('{dataInicio} 00:00:00', 'DD/MM/YYYY HH24:MI:SS') AND
                       TO_DATE('{dataFim} 23:59:59', 'DD/MM/YYYY HH24:MI:SS')

              ) TMP

             WHERE 1 = 1
                {fluxo}
                {origem}
                {destino}
                {vagoes}
                {cliente}
                {expedidor}
                {remetente}
                {recebedor}
                {mercadorias}
                {segmento}
                {exportarExcel}
                {os}
                {prefixo}
                {filtroItensDespacho}
                {cnpjRaizRecebedor}
                {malhaOrigem}
                {statusCte}
                {statusNfe}
                {statusWebDanfe}
                {statusTicket}
                {statusDcl}
                {statusLaudoMercadoria}
                {outrasFerrovias}

            {ORDENACAO}
            ");

            #endregion

            #region Where

            // 1) Data Inicio
            sql.Replace("{dataInicio}", dataInicio.ToString("dd/MM/yyyy"));

            // 2) Data Fim
            sql.Replace("{dataFim}", dataFinal.ToString("dd/MM/yyyy"));

            // 3) Fluxo
            fluxo = fluxo.ToUpper();
            fluxo = Regex.Replace(fluxo, @"[^\d+]", string.Empty);
            sql.Replace("{fluxo}",
                        !String.IsNullOrEmpty(fluxo) ? "AND REGEXP_REPLACE(TMP.FLUXO, " + "'^(\\D*)'" + ", '') = '" + fluxo + "'" : String.Empty);

            // 4) Origem
            origem = origem.ToUpper();
            sql.Replace("{origem}", !String.IsNullOrEmpty(origem) ? String.Format(" AND TMP.ORIGEM = '{0}' ", origem) : String.Empty);

            // 5) Destino
            destino = destino.ToUpper();
            sql.Replace("{destino}", !String.IsNullOrEmpty(destino) ? String.Format(" AND TMP.DESTINO = '{0}' ", destino) : String.Empty);

            // 6) Vagões
            var vagoesSeparadosVirgula = String.Empty;
            if (vagoes.Length > 0)
            {
                foreach (var vagao in vagoes)
                {
                    vagoesSeparadosVirgula += String.Format("{0},", vagao);
                }
                vagoesSeparadosVirgula = vagoesSeparadosVirgula.Remove(vagoesSeparadosVirgula.Length - 1);
            }

            sql.Replace("{vagoes}", !String.IsNullOrEmpty(vagoesSeparadosVirgula) ? String.Format(" AND TMP.COD_VAGAO IN ({0}) ", vagoesSeparadosVirgula) : String.Empty);

            // 7) Cliente
            sql.Replace("{cliente}", !String.IsNullOrEmpty(cliente) ? String.Format(" AND TMP.CLIENTE like '%{0}%' ", cliente) : String.Empty);

            // 8) Expedidor
            sql.Replace("{expedidor}", !String.IsNullOrEmpty(expedidor) ? String.Format(" AND UPPER(TMP.EXPEDIDOR) LIKE '%{0}%' ", expedidor.ToUpper()) : String.Empty);

            // 9) Remetente
            sql.Replace("{remetente}", !String.IsNullOrEmpty(remetente) ? String.Format(" AND UPPER(TMP.REMETENTE_FISCAL) LIKE '%{0}%' ", remetente.ToUpper()) : String.Empty);

            // 10) Mercadorias
            var mercadoriasSeparadosVirgula = String.Empty;
            if (mercadorias.Length > 0)
            {
                foreach (var mercadoria in mercadorias)
                {
                    mercadoriasSeparadosVirgula += String.Format("'{0}',", mercadoria);
                }

                mercadoriasSeparadosVirgula = mercadoriasSeparadosVirgula.Remove(mercadoriasSeparadosVirgula.Length - 1);
            }

            sql.Replace("{mercadorias}", !String.IsNullOrEmpty(mercadoriasSeparadosVirgula) ? String.Format(" AND TMP.COD_MERCADORIA IN ({0}) ", mercadoriasSeparadosVirgula) : String.Empty);

            // 11) Segmento
            sql.Replace("{segmento}", !String.IsNullOrEmpty(segmento) ? String.Format(" AND TMP.SEGMENTO = '{0}' ", segmento) : String.Empty);

            // 12) OS
            sql.Replace("{os}", !String.IsNullOrEmpty(os) ? String.Format(" AND TMP.OS = '{0}' ", os) : String.Empty);

            // 13) Prefixo
            prefixo = prefixo.ToUpper();
            sql.Replace("{prefixo}", !String.IsNullOrEmpty(prefixo) ? String.Format(" AND TMP.PREFIXO = '{0}' ", prefixo) : String.Empty);

            // 14) Itens Despacho (Quando o usuário seleciona alguns registros da grid através do checkbox
            var itensDespachoSeparadosVirgula = String.Empty;
            if (itensDespacho.Length > 0)
            {
                foreach (var itemDes in itensDespacho)
                {
                    itensDespachoSeparadosVirgula += String.Format("'{0}',", itemDes);
                }

                itensDespachoSeparadosVirgula = itensDespachoSeparadosVirgula.Remove(itensDespachoSeparadosVirgula.Length - 1);
            }

            sql.Replace("{filtroItensDespacho}", !String.IsNullOrEmpty(itensDespachoSeparadosVirgula) ? String.Format(" AND TMP.ID_DESPACHO_ITEM IN ({0}) ", itensDespachoSeparadosVirgula) : String.Empty);


            // 15) Filtro CnpjRaizRecebedor (utilizado pelo CAALL)
            if (String.IsNullOrEmpty(cnpjRaizRecebedor))
            {
                sql.Replace("{cnpjRaizRecebedor}", String.Empty);
            }
            else
            {
                cnpjRaizRecebedor = cnpjRaizRecebedor.Substring(0, 8);
                sql.Replace("{cnpjRaizRecebedor}", String.Format(" AND SUBSTR(TMP.CNPJ_RECEBEDOR, 1, 8) = '{0}' ", cnpjRaizRecebedor));
            }

            // 16) Malha Origem
            if (String.IsNullOrEmpty(malhaOrigem))
            {
                sql.Replace("{malhaOrigem}", String.Empty);
            }
            else
            {
                sql.Replace("{malhaOrigem}", String.Format(" AND UPPER(TMP.MALHA_ORIGEM) LIKE '%{0}%' ", malhaOrigem.ToUpper().Replace("METRICA", String.Empty).Replace("MÉTRICA", String.Empty)));
            }

            // 17) CT-e
            if (statusCte)
            {
                sql.Replace("{statusCte}", " AND TMP.CTE IS NOT NULL");
            }
            else
            {
                sql.Replace("{statusCte}", String.Empty);
            }

            // 18) NF-e
            if (statusNfe && statusWebDanfe == false)
            {
                sql.Replace("{statusNfe}", " AND TMP.NFE = 'S'");
            }
            else
            {
                sql.Replace("{statusNfe}", String.Empty);
            }

            // 19) WebDanfe
            if (statusWebDanfe && statusNfe == false)
            {
                sql.Replace("{statusWebDanfe}", " AND TMP.WEB_DANFE = 'S' AND TMP.XML_NFE = 'S'");
            }
            else
            {
                sql.Replace("{statusWebDanfe}", string.Empty);
            }

            // 20) Ticket
            if (statusTicket)
            {
                sql.Replace("{statusTicket}", " AND TMP.TICKET = 'S'");
            }
            else
            {
                sql.Replace("{statusTicket}", String.Empty);
            }

            // 21) DCL
            // Todos possuem DCL, então podemos retirar o filtro
            sql.Replace("{statusDcl}", String.Empty);

            // 22) Laudo Mercadoria
            if (statusLaudoMercadoria)
            {
                sql.Replace("{statusLaudoMercadoria}", " AND TMP.LAUDO_MERCADORIA = 'S'");
            }
            else
            {
                sql.Replace("{statusLaudoMercadoria}", String.Empty);
            }

            // 23) Outras Ferrovias
            if (outrasFerrovias)
            {
                sql.Replace("{outrasFerrovias}", " AND TMP.OUTRAS_FERROVIAS = 'S'");
            }
            else
            {
                sql.Replace("{outrasFerrovias}", String.Empty);
            }

            // 24) Exportar Excel
            if (string.IsNullOrEmpty(segmento))
            {
                sql.Replace("{exportarExcel}", exportarExcel ? " AND TMP.SEGMENTO NOT IN ('BRADO', 'TRACKAGE')" : String.Empty);
            }
            else
            {
                sql.Replace("{exportarExcel}", String.Empty);
            }

            // 25) Recebedor
            sql.Replace("{recebedor}", !String.IsNullOrEmpty(recebedor) ? String.Format(" AND UPPER(TMP.RECEBEDOR) LIKE '%{0}%' ", recebedor.ToUpper()) : String.Empty);

            #endregion

            #region OrderBy

            // Order by (Recupera o indice e o direcionamento (ASC/DESC))
            var colunasOrdenacao = detalhesPaginacao.ColunasOrdenacao;
            var direcoesOrdenacao = detalhesPaginacao.DirecoesOrdenacao;
            var colOrdenacaoIndex = String.Empty;
            var ordenacaoDirecao = String.Empty;

            var ordenacaoPadrao = colunasOrdenacao.Count == 0 && direcoesOrdenacao.Count == 0;
            if (!ordenacaoPadrao)
            {
                colOrdenacaoIndex = colunasOrdenacao[0];
                ordenacaoDirecao = direcoesOrdenacao[0];

                switch (colOrdenacaoIndex.ToLower())
                {
                    case "vagao":
                        colOrdenacaoIndex = "TMP.COD_VAGAO";
                        break;
                    case "datacarga":
                        colOrdenacaoIndex = "TMP.DATA_CARGA";
                        break;
                    case "fluxo":
                        colOrdenacaoIndex = "TMP.FLUXO";
                        break;
                    case "origem":
                        colOrdenacaoIndex = "TMP.ORIGEM";
                        break;
                    case "destino":
                        colOrdenacaoIndex = "TMP.DESTINO";
                        break;
                    case "cliente":
                        colOrdenacaoIndex = "TMP.CLIENTE";
                        break;
                    case "mercadoria":
                        colOrdenacaoIndex = "TMP.DSC_MERCADORIA";
                        break;
                    case "os":
                        colOrdenacaoIndex = "TMP.OS";
                        break;
                    case "prefixo":
                        colOrdenacaoIndex = "TMP.PREFIXO";
                        break;
                    case "seq":
                        colOrdenacaoIndex = "TMP.SEQ";
                        break;
                    case "ctestatus":
                        colOrdenacaoIndex = "TMP.CTE";
                        break;
                    case "possuiticket":
                        colOrdenacaoIndex = "TMP.TICKET";
                        break;
                    default:
                        colOrdenacaoIndex = "TMP.DATA_CARGA";
                        ordenacaoDirecao = "ASC";
                        break;
                }
            }

            sql.Replace("{ORDENACAO}", String.Format(@"
                    ORDER BY {0}            {1} 
                         {2} TMP.ORIGEM     ASC
                           , TMP.DESTINO    ASC
                           , TMP.PREFIXO    ASC
                           , TMP.SEQ        ASC
                           , TMP.COD_VAGAO  ASC
                           , TMP.DATA_CARGA ASC", ordenacaoPadrao ? String.Empty : colOrdenacaoIndex
                                                , ordenacaoPadrao ? String.Empty : ordenacaoDirecao
                                                , ordenacaoPadrao ? String.Empty : ","));

            #endregion

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());
                var queryCount = session.CreateSQLQuery(String.Format(@"SELECT COUNT(1) FROM ({0})", sql.ToString().Replace("{ORDENACAO}", String.Empty)));

                foreach (var p in parameters)
                {
                    p.Invoke(query);
                    p.Invoke(queryCount);
                }

                query.SetResultTransformer(Transformers.AliasToBean<PainelExpedicaoConsultarDocumentosDto>());
                query.SetFirstResult(detalhesPaginacao.Inicio ?? 0);
                query.SetMaxResults(detalhesPaginacao.Limite ?? DetalhesPaginacao.LimitePadrao);

                var total = queryCount.UniqueResult<decimal>();
                var itens = query.List<PainelExpedicaoConsultarDocumentosDto>();

                var result = new ResultadoPaginado<PainelExpedicaoConsultarDocumentosDto>
                {
                    Items = itens,
                    Total = (long)total
                };

                return result;
            }
        }

        public ResultadoPaginado<RelatorioHistoricoFaturamentosDto> ObterHistoricoFaturamentos(
                                                                            DetalhesPaginacao detalhesPaginacao,
                                                                            DateTime dataInicio,
                                                                            DateTime dataFinal,
                                                                            string fluxo,
                                                                            string origem,
                                                                            string destino,
                                                                            string[] vagoes,
                                                                            string cliente,
                                                                            string expedidor,
                                                                            string remetente,
                                                                            string recebedor,
                                                                            string[] mercadorias,
                                                                            string segmento,
                                                                            string os,
                                                                            string prefixo,
                                                                            bool statusCte,
                                                                            bool statusNfe,
                                                                            bool statusWebDanfe,
                                                                            bool statusTicket,
                                                                            bool statusDcl,
                                                                            bool statusLaudoMercadoria,
                                                                            bool outrasFerrovias,
                                                                            string[] itensDespacho,
                                                                            string cnpjRaizRecebedor,
                                                                            string malhaOrigem,
                                                                            bool exportarExcel)
        {
            #region SQL Query

            var parameters = new List<Action<IQuery>>();
            var sql = new StringBuilder(@"
            SELECT TMP.COD_VAGAO            AS ""Vagao""
                 , TMP.DATA_CARGA           AS ""DataCarga""
                 , TMP.ID_DESPACHO          AS ""IdDespacho""
                 , TMP.ID_DESPACHO_ITEM     AS ""IdDespachoItem""
                 , TMP.ID_CTE               AS ""IdCte""
                 , TMP.FLUXO                AS ""Fluxo""
                 , TMP.ORIGEM               AS ""Origem""
                 , TMP.DESTINO              AS ""Destino""
                 , TMP.CLIENTE              AS ""Cliente""
                 , TMP.EXPEDIDOR            AS ""Expedidor""
                 , TMP.RECEBEDOR            AS ""Recebedor""
                 , TMP.REMETENTE_FISCAL     AS ""RemetenteFiscal""
                 , TMP.COD_MERCADORIA       AS ""CodigoMercadoria""
                 , TMP.DSC_MERCADORIA       AS ""DescricaoMercadoria""
                 , TMP.OS                   AS ""Os""
                 , TMP.PREFIXO              AS ""Prefixo""
                 , TMP.SEQ                  AS ""Seq""
                 , TMP.CTE                  AS ""PossuiCte""
                 , TMP.SEGMENTO             AS ""Segmento""
                 , TMP.TICKET               AS ""PossuiTicket""
                 , TMP.NFE                  AS ""PossuiNfe""
                 , TMP.WEB_DANFE            AS ""PossuiWebDanfe""
                 , TMP.PdfsRecebidos        AS ""QuantidadeNfeRecebidas""
                 , TMP.NotasTotais          AS ""TotalNfe""
                 , TMP.NUMCTE               AS ""NumeroCte""
                 , TMP.USU_FAT              AS ""UsuarioEventoCarregamento""
                 , TMP.CNPJ_RECEBEDOR       AS ""CnpjRecebedor""
                 , TMP.MDFE                 AS ""PossuiMdfe""
                 , TMP.ID_MDFE              AS ""IdMdfe""
                 , TMP.XML_NFE              AS ""PossuiXmlNfe""
                 , TMP.QTD_XML_NFE          AS ""QtdXmlNfe""
                 , TMP.NotasTotais - TMP.QTD_XML_NFE AS ""XmlNaoRecebidos""
                 , TMP.MALHA_ORIGEM         AS ""MalhaOrigem""
                 , TMP.LAUDO_MERCADORIA     AS ""PossuiLaudoMercadoria""
                 , TMP.OUTRAS_FERROVIAS     AS ""OutrasFerrovias""
              FROM (
                     SELECT DISTINCT
                            VG.VG_COD_VAG AS COD_VAGAO
                          , DP.DP_DT AS DATA_CARGA
                          , DP.DP_ID_DP AS ID_DESPACHO
                          , (SELECT IDS2.ID_ID_ITD 
                               FROM ITEM_DESPACHO IDS2 
                              WHERE IDS2.DP_ID_DP = DP.DP_ID_DP 
                                AND IDS2.VG_ID_VG = VG.VG_ID_VG
                                AND ROWNUM = 1) AS ID_DESPACHO_ITEM
                          , ULTIMO_CTE_TEMP.ID_CTE AS ID_CTE
                          , FC.FX_COD_FLX AS FLUXO
                          , NVL(ORIF1.AO_COD_AOP, ORIF.AO_COD_AOP) AS ORIGEM
                          , NVL(DSTF1.AO_COD_AOP, DSTF.AO_COD_AOP) AS DESTINO
                          , COR.EP_DSC_RSM AS CLIENTE
                          , EXPEDIDOR.EP_DSC_RSM AS EXPEDIDOR
                          , RECEBEDOR.EP_DSC_RSM AS RECEBEDOR
                          , NVL( (SELECT NVL(RF2.EP_DSC_RSM, RF1.EP_DSC_RSM)
                                    FROM NOTA_FISCAL      NF
                                    LEFT JOIN EMPRESA     RF1 ON RF1.EP_CGC_EMP = NF.EP_CGC_REM
                                    LEFT JOIN VW_NFE      VW  ON VW.NFE_CHAVE = NF.NFE_CHAVE_NFE
                                    LEFT JOIN EMPRESA     RF2 ON RF2.EP_CGC_EMP = VW.NFE_CNPJ_ORI
                                   WHERE NF.DP_ID_DP = DP.DP_ID_DP
                                     AND ROWNUM = 1) , '') AS REMETENTE_FISCAL
                          , M.MC_COD_MRC AS COD_MERCADORIA
                          , M.MC_DRS_PT AS DSC_MERCADORIA
                          , NVL((SELECT CASE WHEN L.AO_ID_AO_INF IS NOT NULL 
                                             THEN (SELECT AOM.AO_COD_AOP
                                                     FROM AREA_OPERACIONAL AOM
                                                    WHERE AOM.AO_ID_AO = L.AO_ID_AO_INF)
                                             ELSE L.AO_COD_AOP
                                              END AS LOCAL_ATUAL
                                  FROM MOVIMENTACAO_TREM MTR
                                  JOIN AREA_OPERACIONAL  L ON L.AO_ID_AO = MTR.AO_ID_AO
                                 WHERE MTR.MT_ID_MOV = TR.MT_ID_MOV), ORI_TREM.AO_COD_AOP) AS LOCAL_ATUAL
                          , OS.X1_NRO_OS  AS OS
                          , TR.TR_PFX_TRM AS PREFIXO
                          , NULL AS COD_LINHA
                          , NULL AS DSC_LINHA
                          , NVL(CV.CV_SEQ_CMP, 0) AS SEQ
                          , (SELECT C1.SITUACAO_ATUAL
                               FROM CTE C1
                               JOIN CTE_ARQUIVO ARQ ON ARQ.ID_CTE = C1.ID_CTE
                              WHERE C1.ID_CTE = ULTIMO_CTE_TEMP.ID_CTE) CTE
                          , FC.FX_COD_SEGMENTO AS SEGMENTO

                          -- Subquery - Ticket
                          , NVL( (SELECT (CASE WHEN TB.ID IS NOT NULL THEN 'S' ELSE 'N' END)
                                    FROM NOTA_FISCAL NF
                                    JOIN NOTA_TICKET_VAGAO NTV ON NTV.CHAVE_NFE = NF.NFE_CHAVE_NFE
                                    JOIN VAGAO_TICKET VT ON VT.Id = NTV.VAGAO_TICKET_ID
                                    JOIN TICKET_BALANCA TB ON TB.ID = VT.TICKET_BALANCA_ID
                                   WHERE NF.DP_ID_DP = DP.DP_ID_DP
                                     AND LPAD(VT.VG_COD_VAG, 7, '0') = VG.VG_COD_VAG
                                     --CONSIDERA PESAGENS QUE OCORRERAM 00H DO DIA ANTERIOR AO CARREGAMENTO
                                     AND (VT.DATA_CADASTRO BETWEEN TRUNC(DP.DP_DT - 3) AND TRUNC(DP.DP_DT + 3)
                                     OR  VT.DATA_CADASTRO  BETWEEN TRUNC(DP.DP_DT - NVL(CT.VALOR , 3)) AND TRUNC(DP.DP_DT + NVL(CT.VALOR, 3)) AND (VT.TU=ITD.ID_NUM_PCL*100) )
                                     AND ROWNUM = 1 ), 'N') AS TICKET
                          -- Subquery - Nfe
                          , CASE WHEN NVL(AGRUPADOR.PdfsRecebidos, 0) > 0
                                 THEN 'S'
                                 ELSE 'N'
                                  END AS NFE
                          , CASE WHEN NVL(AGRUPADOR.PdfsRecebidos, 0) != NVL(AGRUPADOR.NotasTotais, 0)
                                 THEN 'S'
                                 ELSE 'N'
                                  END AS WEB_DANFE
                          , NVL(AGRUPADOR.PdfsRecebidos, 0) AS PdfsRecebidos
                          , NVL(AGRUPADOR.NotasTotais  , 0) AS NotasTotais
                          , (SELECT C1.NRO_CTE
                               FROM CTE C1
                               JOIN CTE_ARQUIVO ARQ ON ARQ.ID_CTE = C1.ID_CTE
                              WHERE C1.ID_CTE = ULTIMO_CTE_AUT_TEMP.ID_CTE) AS NUMCTE
                          , NVL( (SELECT USU.US_USR_IDU || ' - ' || USU.US_NOM_USU
                                    FROM EVENTO_VAGAO EVG
                                    JOIN USUARIO USU ON USU.US_USR_IDU = EVG.EA_MAT_USU 
                                   WHERE EVG.en_id_evt = 13 -- Evento Carregar Vagão
                                     AND EVG.VG_ID_VG = VG.VG_ID_VG
                                     AND TO_DATE(to_char(EVG.EA_DTO_EVG, 'YYYY/MM/DD HH24:MI'), 'YYYY/MM/DD HH24:MI')
                                                 = TO_DATE(to_char(DP.DP_DT, 'YYYY/MM/DD HH24:MI'), 'YYYY/MM/DD HH24:MI')
                                     AND ROWNUM = 1 ), '') AS USU_FAT

                          -- Recebedor
                          , RECEB.EP_CGC_EMP AS CNPJ_RECEBEDOR
                          , NVL( (SELECT 'S'
                                    FROM MDFE_COMPOSICAO MDFE
                                   WHERE MDFE.ID_CTE = ULTIMO_CTE_AUT_TEMP.ID_CTE
                                     AND MDFE.DP_ID_DP = DP.DP_ID_DP
                                     AND ROWNUM = 1), 'N') AS MDFE
                          , (SELECT MAX(MDFE.ID_MDFE)
                               FROM MDFE_COMPOSICAO MDFE
                              WHERE MDFE.ID_CTE = ULTIMO_CTE_AUT_TEMP.ID_CTE
                                AND MDFE.DP_ID_DP = DP.DP_ID_DP) AS ID_MDFE
                          , NVL( (SELECT 'S' 
                                    FROM NOTA_FISCAL NF
                                   WHERE NF.DP_ID_DP = DP.DP_ID_DP
                                     AND EXISTS(
                                         SELECT EDI.NFE_CHAVE AS CHAVE_NFE 
                                           FROM EDI.EDI2_NFE EDI
                                          WHERE EDI.NFE_CHAVE = NF.NFE_CHAVE_NFE
                                            AND EDI.NFE_XML IS NOT NULL
                                            AND ROWNUM = 1
                                          UNION
                                         SELECT EDI_DIST.NFE_CHAVE AS CHAVE_NFE 
                                           FROM NFE_DISTRIBUICAO EDI_DIST
                                          WHERE EDI_DIST.NFE_CHAVE = NF.NFE_CHAVE_NFE
                                            AND EDI_DIST.NFE_XML IS NOT NULL
                                            AND ROWNUM = 1
                                         )
                                     AND ROWNUM = 1), 'N') AS XML_NFE

                           , (SELECT COUNT(DISTINCT NF.NF_IDT_NFL)
                                  FROM NOTA_FISCAL NF
                                 WHERE NF.DP_ID_DP = DP.DP_ID_DP
                                   AND EXISTS
                                 (SELECT EDI.NFE_CHAVE AS CHAVE_NFE
                                          FROM EDI.EDI2_NFE EDI
                                         WHERE EDI.NFE_CHAVE = NF.NFE_CHAVE_NFE
                                           AND EDI.NFE_XML IS NOT NULL
                                           AND ROWNUM = 1
                                        UNION
                                        SELECT EDI_DIST.NFE_CHAVE AS CHAVE_NFE
                                          FROM NFE_DISTRIBUICAO EDI_DIST
                                         WHERE EDI_DIST.NFE_CHAVE =
                                               NF.NFE_CHAVE_NFE
                                           AND EDI_DIST.NFE_XML IS NOT NULL
                                           AND ROWNUM = 1)
                           ) QTD_XML_NFE

                          , MALHAORIGEM.ML_DSC_ML AS MALHA_ORIGEM
                          , CASE WHEN LAUDO.ID_WS_LAUDO IS NOT NULL 
                                 THEN 'S' 
                                 ELSE 'N' 
                                  END AS LAUDO_MERCADORIA

                          , CASE WHEN C.CZ_FERROVIA_FAT IN ('E', 'F') THEN 'S' ELSE 'N' END AS OUTRAS_FERROVIAS
                       FROM DESPACHO                DP
                 INNER JOIN ITEM_DESPACHO           ITD         ON ITD.DP_ID_DP = DP.DP_ID_DP
                 INNER JOIN VAGAO                   VG          ON VG.VG_ID_VG = ITD.VG_ID_VG
                 INNER JOIN DETALHE_CARREGAMENTO    DC          ON DC.DC_ID_CRG = ITD.DC_ID_CRG
                 INNER JOIN CARREGAMENTO            CG          ON CG.CG_ID_CAR = DC.CG_ID_CAR
                 INNER JOIN FLUXO_COMERCIAL         FC          ON FC.FX_ID_FLX = CG.FX_ID_FLX_CRG
                  LEFT JOIN EMPRESA                 RECEB       ON RECEB.EP_ID_EMP = FC.EP_ID_EMP_DST
                 INNER JOIN CONTRATO                C           ON C.CZ_ID_CTT = FC.CZ_ID_CTT                 
                 INNER JOIN MERCADORIA              M           ON M.MC_ID_MRC = FC.MC_ID_MRC
                 INNER JOIN AREA_OPERACIONAL        ORIF        ON ORIF.AO_ID_AO = FC.AO_ID_EST_OR
                 INNER JOIN UNID_PRODUCAO           UNID        ON UNID.UP_ID_UNP = ORIF.UP_ID_UNP
                 INNER JOIN MALHA                   MALHAORIGEM ON MALHAORIGEM.ML_CD_ML = UNID.UP_IND_MALHA
                  LEFT JOIN AREA_OPERACIONAL        ORIF1       ON ORIF1.AO_ID_AO = FC.AO_ID_INT_OR
                 INNER JOIN AREA_OPERACIONAL        DSTF        ON DSTF.AO_ID_AO = FC.AO_ID_EST_DS
                  LEFT JOIN AREA_OPERACIONAL        DSTF1       ON DSTF1.AO_ID_AO = FC.AO_ID_INT_DS
                 INNER JOIN EMPRESA                 COR         ON COR.EP_ID_EMP = FC.EP_ID_EMP_COR
                 INNER JOIN EMPRESA                 EXPEDIDOR   ON EXPEDIDOR.EP_ID_EMP = FC.EP_ID_EMP_REM
                 INNER JOIN EMPRESA                 RECEBEDOR   ON RECEBEDOR.EP_ID_EMP = FC.EP_ID_EMP_DST
                 INNER JOIN CONF_GERAL              CT          ON CT.CHAVE = 'INTERVALO_DIAS_TICKET_TSP'

                 -- Obtendo o ultimo CTE
                  LEFT JOIN (
                       SELECT MAX(C.ID_CTE) AS ID_CTE
                            , C.DP_ID_DP    AS DP_ID_DP
                         FROM CTE C
                        WHERE C.DATA_CTE BETWEEN TO_DATE('{dataInicio}', 'DD/MM/YYYY')
                                              AND TO_DATE('{dataFim}', 'DD/MM/YYYY')
                        GROUP BY C.DP_ID_DP
                  ) ULTIMO_CTE_TEMP ON ULTIMO_CTE_TEMP.DP_ID_DP = DP.DP_ID_DP
              --    LEFT JOIN CTE ULTIMO_CTE ON ULTIMO_CTE.ID_CTE = ULTIMO_CTE_TEMP.ID_CTE

                 -- Obtendo o ultimo CTE Autenticado
                  LEFT JOIN (
                       SELECT MAX(C.ID_CTE) AS ID_CTE
                            , C.DP_ID_DP    AS DP_ID_DP
                         FROM CTE C
                        WHERE C.SITUACAO_ATUAL = 'AUT'
                          AND C.DATA_CTE BETWEEN TO_DATE('{dataInicio}', 'DD/MM/YYYY')
                                              AND TO_DATE('{dataFim}', 'DD/MM/YYYY')
                        GROUP BY C.DP_ID_DP
                  ) ULTIMO_CTE_AUT_TEMP ON ULTIMO_CTE_AUT_TEMP.DP_ID_DP = DP.DP_ID_DP
              --    LEFT JOIN CTE ULTIMO_CTE_AUT ON ULTIMO_CTE_AUT.ID_CTE = ULTIMO_CTE_AUT_TEMP.ID_CTE

                 -- Obtendo a ultima composicao do vagao daquele despacho
                  LEFT JOIN (
                       SELECT COMPV.VG_ID_VG  AS VG_ID_VG
                            , COMPV.PT_ID_ORT AS PT_ID_ORT
                            , MAX(COMPV.CV_IDT_CVG) AS CV_IDT_CVG
                         FROM COMPVAGAO COMPV
                        INNER JOIN COMPOSICAO C ON(C.CP_ID_CPS = COMPV.CP_ID_CPS)
                        WHERE COMPV.CV_TIMESTAMP BETWEEN TO_DATE('{dataInicio}', 'DD/MM/YYYY') - 30
                                                     AND TO_DATE('{dataFim}', 'DD/MM/YYYY') + 30
                        AND NVL(C.CP_CMP_CPS,0)>0
                        GROUP BY COMPV.VG_ID_VG
                               , COMPV.PT_ID_ORT
                  ) CV_MAX ON CV_MAX.VG_ID_VG = ITD.VG_ID_VG AND CV_MAX.PT_ID_ORT = DP.PT_ID_ORT
                  LEFT JOIN COMPVAGAO CV ON CV.CV_IDT_CVG = CV_MAX.CV_IDT_CVG
                  LEFT JOIN COMPOSICAO CP ON CP.CP_ID_CPS = CV.CP_ID_CPS
                  LEFT JOIN TREM TR ON TR.TR_ID_TRM = CP.TR_ID_TRM
                  LEFT JOIN AREA_OPERACIONAL ORI_TREM ON ORI_TREM.AO_ID_AO = TR.AO_ID_AO_ORG
                  LEFT JOIN T2_OS OS ON OS.X1_ID_OS = TR.OF_ID_OSV

                  LEFT JOIN ( /*index (pdf_edi idx_nfepdf_id_nfe)*/
                       SELECT COUNT(DISTINCT NVL(PDF_EDI.NFE_ID_NFE, PDF_SIM.NFE_ID_NFE)) AS PdfsRecebidos
                            , COUNT(DISTINCT NF.NF_NUM_NFL) AS NotasTotais
                            , NF.DP_ID_DP AS DP_ID_DP
                         FROM NOTA_FISCAL           NF
                        INNER JOIN DESPACHO         DP_NF ON DP_NF.DP_ID_DP = NF.DP_ID_DP
                         LEFT JOIN EDI.EDI2_NFE     NFE ON NFE.NFE_CHAVE  = NF.NFE_CHAVE_NFE
                         LEFT JOIN NFE_SIMCONSULTAS SIM ON SIM.NFE_CHAVE  = NF.NFE_CHAVE_NFE
                         LEFT JOIN EDI.EDI2_NFE_PDF PDF_EDI ON PDF_EDI.NFE_ID_NFE = NFE.NFE_ID_NFE
                         LEFT JOIN EDI.EDI2_NFE_PDF PDF_SIM ON PDF_SIM.NFE_ID_SIMCONSULTAS = SIM.NFE_ID_NFE
                        WHERE DP_NF.DP_DT BETWEEN TO_DATE('{dataInicio}', 'DD/MM/YYYY')
                                              AND TO_DATE('{dataFim}', 'DD/MM/YYYY')
                        GROUP BY NF.DP_ID_DP
                 ) AGRUPADOR ON AGRUPADOR.DP_ID_DP = DP.DP_ID_DP

                  LEFT JOIN (
                       SELECT MAX(WS.ID_WS_LAUDO)   AS ID_WS_LAUDO
                            , WS.CODIGO_VAGAO        AS CODIGO_VAGAO
                            , WS.DATA_CLASSIFICACAO  AS DATA_CLASSIFICACAO
                         FROM WS_LAUDO WS
                        WHERE NVL(WS.ID_DESPACHO, 0) != 0
                        GROUP BY WS.CODIGO_VAGAO, WS.DATA_CLASSIFICACAO
                 ) LAUDO ON LAUDO.CODIGO_VAGAO = VG.VG_COD_VAG /*LAUDO.ID_DESPACHO = DP.DP_ID_DP*/
                        AND TO_DATE(TO_CHAR(LAUDO.DATA_CLASSIFICACAO,'dd/MM/yyyy'),'dd/MM/yyyy') >= TO_DATE(TO_CHAR(DP.DP_DT - 3,'dd/MM/yyyy'),'dd/MM/yyyy')
                 WHERE DP.DP_DT BETWEEN TO_DATE('{dataInicio}', 'DD/MM/YYYY')
                                    AND TO_DATE('{dataFim}', 'DD/MM/YYYY')
                   AND DP.DP_IND_CLD = 'N' -- Despachos não cancelados
                   {vagoes}
                   {fluxo}
                   {cliente}
                   {expedidor}
                   {recebedor}
                   {mercadorias}
                   {segmento}
                   {exportarExcel}
                   {os}
                   {prefixo}
                   {malhaOrigem}
                   {cnpjRaizRecebedor}

              ) TMP

             WHERE 1 = 1
               {origem}
               {destino}
               {remetente}
               {filtroItensDespacho}
               {statusCte}
               {statusNfe}
               {statusWebDanfe}
               {statusTicket}
               {statusDcl}
               {statusLaudoMercadoria}
               {outrasFerrovias}

            {ORDENACAO}
            ");

            #endregion

            #region Where

            // 1) Data Inicio
            sql.Replace("{dataInicio}", dataInicio.ToString("dd/MM/yyyy"));

            // 2) Data Fim
            sql.Replace("{dataFim}", dataFinal.ToString("dd/MM/yyyy"));

            // 3) Fluxo
            fluxo = fluxo.ToUpper();
            fluxo = Regex.Replace(fluxo, @"[^\d+]", string.Empty);
            sql.Replace("{fluxo}",
                        !String.IsNullOrEmpty(fluxo) ? "AND REGEXP_REPLACE(FC.FX_COD_FLX, " + "'^(\\D*)'" + ", '') = '" + fluxo + "'" : String.Empty);

            // 4) Origem
            origem = origem.ToUpper();
            sql.Replace("{origem}", !String.IsNullOrEmpty(origem) ? String.Format(" AND TMP.ORIGEM = '{0}' ", origem) : String.Empty);

            // 5) Destino
            destino = destino.ToUpper();
            sql.Replace("{destino}", !String.IsNullOrEmpty(destino) ? String.Format(" AND TMP.DESTINO = '{0}' ", destino) : String.Empty);

            // 6) Vagões
            var vagoesSeparadosVirgula = String.Empty;
            if (vagoes.Length > 0)
            {
                foreach (var vagao in vagoes)
                {
                    vagoesSeparadosVirgula += String.Format("'{0}',", vagao);
                }
                vagoesSeparadosVirgula = vagoesSeparadosVirgula.Remove(vagoesSeparadosVirgula.Length - 1);
            }

            sql.Replace("{vagoes}", !String.IsNullOrEmpty(vagoesSeparadosVirgula) ? String.Format(" AND VG.VG_COD_VAG IN ({0}) ", vagoesSeparadosVirgula) : String.Empty);

            // 7) Cliente
            sql.Replace("{cliente}", !String.IsNullOrEmpty(cliente) ? String.Format(" AND COR.EP_DSC_RSM LIKE '%{0}%' ", cliente) : String.Empty);

            // 8) Expedidor
            sql.Replace("{expedidor}", !String.IsNullOrEmpty(expedidor) ? String.Format(" AND UPPER(EXPEDIDOR.EP_DSC_RSM) LIKE '%{0}%' ", expedidor.ToUpper()) : String.Empty);

            // 9) Remetente
            sql.Replace("{remetente}", !String.IsNullOrEmpty(remetente) ? String.Format(" AND UPPER(TMP.REMETENTE_FISCAL) LIKE '%{0}%' ", remetente.ToUpper()) : String.Empty);

            // 10) Mercadorias
            var mercadoriasSeparadosVirgula = String.Empty;
            if (mercadorias.Length > 0)
            {
                foreach (var mercadoria in mercadorias)
                {
                    mercadoriasSeparadosVirgula += String.Format("'{0}',", mercadoria);
                }

                mercadoriasSeparadosVirgula = mercadoriasSeparadosVirgula.Remove(mercadoriasSeparadosVirgula.Length - 1);
            }

            sql.Replace("{mercadorias}", !String.IsNullOrEmpty(mercadoriasSeparadosVirgula) ? String.Format(" AND M.MC_COD_MRC IN ({0}) ", mercadoriasSeparadosVirgula) : String.Empty);

            // 11) Segmento
            sql.Replace("{segmento}", !String.IsNullOrEmpty(segmento) ? String.Format(" AND FC.FX_COD_SEGMENTO = '{0}' ", segmento) : String.Empty);

            // 12) OS
            sql.Replace("{os}", !String.IsNullOrEmpty(os) ? String.Format(" AND OS.X1_NRO_OS = '{0}' ", os) : String.Empty);

            // 13) Prefixo
            prefixo = prefixo.ToUpper();
            sql.Replace("{prefixo}", !String.IsNullOrEmpty(prefixo) ? String.Format(" AND TR.TR_PFX_TRM = '{0}' ", prefixo) : String.Empty);

            // 14) Itens Despacho (Quando o usuário seleciona alguns registros da grid através do checkbox
            var itensDespachoSeparadosVirgula = String.Empty;
            if (itensDespacho.Length > 0)
            {
                foreach (var itemDes in itensDespacho)
                {
                    itensDespachoSeparadosVirgula += String.Format("'{0}',", itemDes);
                }

                itensDespachoSeparadosVirgula = itensDespachoSeparadosVirgula.Remove(itensDespachoSeparadosVirgula.Length - 1);
            }

            sql.Replace("{filtroItensDespacho}", !String.IsNullOrEmpty(itensDespachoSeparadosVirgula) ? String.Format(" AND TMP.ID_DESPACHO_ITEM IN ({0}) ", itensDespachoSeparadosVirgula) : String.Empty);

            // 15) Filtro CnpjRaizRecebedor (utilizado pelo CAALL)
            if (String.IsNullOrEmpty(cnpjRaizRecebedor))
            {
                sql.Replace("{cnpjRaizRecebedor}", String.Empty);
            }
            else
            {
                if (cnpjRaizRecebedor.Length < 8)
                {
                    cnpjRaizRecebedor = cnpjRaizRecebedor.PadLeft(8, '0');
                }

                cnpjRaizRecebedor = cnpjRaizRecebedor.Substring(0, 8);
                sql.Replace("{cnpjRaizRecebedor}", String.Format(" AND SUBSTR(RECEB.EP_CGC_EMP, 1, 8) = '{0}' ", cnpjRaizRecebedor));
            }

            // 16) Malha Origem
            if (String.IsNullOrEmpty(malhaOrigem))
            {
                sql.Replace("{malhaOrigem}", String.Empty);
            }
            else
            {
                sql.Replace("{malhaOrigem}", String.Format(" AND UPPER(MALHAORIGEM.ML_DSC_ML) LIKE '%{0}%' ", malhaOrigem.ToUpper().Replace("METRICA", String.Empty).Replace("MÉTRICA", String.Empty)));
            }

            // 17) CT-e
            if (statusCte)
            {
                sql.Replace("{statusCte}", " AND TMP.CTE IS NOT NULL");
            }
            else
            {
                sql.Replace("{statusCte}", String.Empty);
            }

            // 18) NF-e
            if (statusNfe && statusWebDanfe == false)
            {
                sql.Replace("{statusNfe}", " AND TMP.NFE = 'S'");
            }
            else
            {
                sql.Replace("{statusNfe}", String.Empty);
            }

            // 19) WebDanfe
            if (statusWebDanfe && statusNfe == false)
            {
                sql.Replace("{statusWebDanfe}", " AND TMP.WEB_DANFE = 'S' AND TMP.XML_NFE = 'S'");
            }
            else
            {
                sql.Replace("{statusWebDanfe}", String.Empty);
            }

            // 20) Ticket
            if (statusTicket)
            {
                sql.Replace("{statusTicket}", " AND TMP.TICKET = 'S'");
            }
            else
            {
                sql.Replace("{statusTicket}", String.Empty);
            }

            // 21) DCL
            // Todos possuem DCL, então podemos retirar o filtro
            sql.Replace("{statusDcl}", String.Empty);

            // 22) Laudo Mercadoria
            if (statusLaudoMercadoria)
            {
                sql.Replace("{statusLaudoMercadoria}", " AND TMP.LAUDO_MERCADORIA = 'S'");
            }
            else
            {
                sql.Replace("{statusLaudoMercadoria}", String.Empty);
            }

            // 23) Outras Ferrovias
            if (outrasFerrovias)
            {
                sql.Replace("{outrasFerrovias}", " AND TMP.OUTRAS_FERROVIAS = 'S'");
            }
            else
            {
                sql.Replace("{outrasFerrovias}", String.Empty);
            }

            // 24) Exportar Excel: Para excel, devemos excluir os segmentos Brado e Trackage
            if (string.IsNullOrEmpty(segmento))
            {
                sql.Replace("{exportarExcel}", exportarExcel ? " AND FC.FX_COD_SEGMENTO NOT IN ('BRADO', 'TRACKAGE')" : String.Empty);
            }
            else
            {
                sql.Replace("{exportarExcel}", string.Empty);
            }

            // 25) Recebedor
            sql.Replace("{recebedor}", !String.IsNullOrEmpty(recebedor) ? String.Format(" AND UPPER(RECEBEDOR.EP_DSC_RSM) LIKE '%{0}%' ", recebedor.ToUpper()) : String.Empty);

            #endregion

            #region OrderBy

            // Order by (Recupera o indice e o direcionamento (ASC/DESC))
            var colunasOrdenacao = detalhesPaginacao.ColunasOrdenacao;
            var direcoesOrdenacao = detalhesPaginacao.DirecoesOrdenacao;
            var colOrdenacaoIndex = String.Empty;
            var ordenacaoDirecao = String.Empty;

            var ordenacaoPadrao = colunasOrdenacao.Count == 0 && direcoesOrdenacao.Count == 0;
            if (!ordenacaoPadrao)
            {
                colOrdenacaoIndex = colunasOrdenacao[0];
                ordenacaoDirecao = direcoesOrdenacao[0];

                switch (colOrdenacaoIndex.ToLower())
                {
                    case "vagao":
                        colOrdenacaoIndex = "TMP.COD_VAGAO";
                        break;
                    case "datacarga":
                        colOrdenacaoIndex = "TMP.DATA_CARGA";
                        break;
                    case "fluxo":
                        colOrdenacaoIndex = "TMP.FLUXO";
                        break;
                    case "origem":
                        colOrdenacaoIndex = "TMP.ORIGEM";
                        break;
                    case "destino":
                        colOrdenacaoIndex = "TMP.DESTINO";
                        break;
                    case "cliente":
                        colOrdenacaoIndex = "TMP.CLIENTE";
                        break;
                    case "mercadoria":
                        colOrdenacaoIndex = "TMP.DSC_MERCADORIA";
                        break;
                    case "os":
                        colOrdenacaoIndex = "TMP.OS";
                        break;
                    case "prefixo":
                        colOrdenacaoIndex = "TMP.PREFIXO";
                        break;
                    case "seq":
                        colOrdenacaoIndex = "TMP.SEQ";
                        break;
                    case "ctestatus":
                        colOrdenacaoIndex = "TMP.CTE";
                        break;
                    case "possuiticket":
                        colOrdenacaoIndex = "TMP.TICKET";
                        break;
                    default:
                        colOrdenacaoIndex = "TMP.DATA_CARGA";
                        ordenacaoDirecao = "ASC";
                        break;
                }
            }

            var sqlCount = sql.ToString().Replace("{ORDENACAO}", String.Empty);

            sql.Replace("{ORDENACAO}", String.Format(@"
                    ORDER BY {0}            {1} 
                         {2} TMP.COD_VAGAO  ASC
                           , TMP.DATA_CARGA ASC
                           , TMP.ORIGEM     ASC
                           , TMP.DESTINO    ASC
                           , TMP.PREFIXO    ASC
                           , TMP.SEQ        ASC", ordenacaoPadrao ? String.Empty : colOrdenacaoIndex
                                                , ordenacaoPadrao ? String.Empty : ordenacaoDirecao
                                                , ordenacaoPadrao ? String.Empty : ","));

            #endregion

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());
                var queryCount = session.CreateSQLQuery(String.Format(@"SELECT COUNT(1) FROM ({0})", sqlCount));

                foreach (var p in parameters)
                {
                    p.Invoke(query);
                    p.Invoke(queryCount);
                }

                query.SetResultTransformer(Transformers.AliasToBean<RelatorioHistoricoFaturamentosDto>());
                query.SetFirstResult(detalhesPaginacao.Inicio ?? 0);
                query.SetMaxResults(detalhesPaginacao.Limite ?? DetalhesPaginacao.LimitePadrao);

                var total = queryCount.UniqueResult<decimal>();
                var itens = query.List<RelatorioHistoricoFaturamentosDto>();
                var result = new ResultadoPaginado<RelatorioHistoricoFaturamentosDto>
                {
                    Items = itens,
                    Total = (long)total
                };

                return result;
            }
        }

        public ResultadoPaginado<PainelExpedicaoRelatorioLaudoDto> ObterPainelExpedicaoRelatorioLaudos(
                                                                            DetalhesPaginacao detalhesPaginacao,
                                                                            DateTime dataInicio,
                                                                            DateTime dataFinal,
                                                                            string fluxo,
                                                                            string origem,
                                                                            string destino,
                                                                            string[] vagoes,
                                                                            string cliente,
                                                                            string expedidor,
                                                                            string remetente,
                                                                            string[] mercadorias,
                                                                            string segmento,
                                                                            string os,
                                                                            string prefixo,
                                                                            string[] itensDespacho,
                                                                            string cnpjRaizRecebedor,
                                                                            string malhaOrigem)
        {
            #region SQL Query

            var parameters = new List<Action<IQuery>>();
            var sql = new StringBuilder(@"
                SELECT DISTINCT
                       VG.VG_COD_VAG                          AS ""Vagao""                     
                     , NFE.NF_NRO_NF                          AS ""NumeroNfe""
                     , REMETENTE_NFE.EP_RAZ_SCL               AS ""RemetenteNfe""
                     , DP.DP_DT                               AS ""DataCarregamento""
                     , FC.FX_COD_FLX                          AS ""FluxoComercial""
                     , NVL(ORIF1.AO_COD_AOP, ORIF.AO_COD_AOP) AS ""Origem""
                     , NVL(DSTF1.AO_COD_AOP, DSTF.AO_COD_AOP) AS ""Destino""
                     , FC.FX_COD_SEGMENTO                     AS ""FluxoSegmento""
                     , L.NUMERO_LAUDO                         AS ""NumeroLaudo""
                     , L.NUMERO_OS_LAUDO                      AS ""NumeroOsLaudo""
                     , LM.DATA_RECEBIMENTO                    AS ""DataRecebimentoLaudo""
                     , LT.DESCRICAO                           AS ""LaudoTipo""
                     , LE.NOME_EMPRESA                        AS ""Empresa""
                     , LE.CNPJ_EMPRESA                        AS ""CnpjEmpresa""
                     , L.CLASSIFICADOR_NOME                   AS ""Classificador""
                     , L.CLASSIFICADOR_CPF                    AS ""CpfClassificador""
                     , LPT.DESCRICAO                          AS ""Produto""
                     , LCT.CODIGO_CLASSIFICACAO               AS ""CodigoClassificacao""
                     , LCT.DESCRICAO                          AS ""Classificacao""
                     , LP.VALOR                               AS ""Porcentagem""

                  FROM WS_LAUDO_MENSAGEM             LM
                 INNER JOIN (
                       SELECT MAX(TMP.ID_WS_LAUDO_MENSAGEM) AS ID
                            , TMP.PROTOCOLO_REQUEST         AS PROTOCOLO_REQUEST
                            , TMP_LOG.ID_WS_LAUDO_EMPRESA   AS ID_WS_LAUDO_EMPRESA
                         FROM WS_LAUDO_MENSAGEM TMP
                         JOIN WS_LAUDO_LOG TMP_LOG ON TMP_LOG.ID_WS_LAUDO_MENSAGEM = TMP.ID_WS_LAUDO_MENSAGEM
                        GROUP BY TMP_LOG.ID_WS_LAUDO_EMPRESA
                               , TMP.PROTOCOLO_REQUEST
                  ) TMP_MAX ON TMP_MAX.ID = LM.ID_WS_LAUDO_MENSAGEM
                 INNER JOIN WS_LAUDO_LOG                  LL  ON LL.ID_WS_LAUDO_MENSAGEM = LM.ID_WS_LAUDO_MENSAGEM
                 INNER JOIN WS_LAUDO_EMPRESA              LE  ON LE.ID_WS_LAUDO_EMPRESA = LL.ID_WS_LAUDO_EMPRESA
                 INNER JOIN WS_LAUDO                      L   ON L.ID_WS_LAUDO_LOG = LL.ID_WS_LAUDO_LOG
                 INNER JOIN WS_LAUDO_PRODUTO              LP  ON LP.ID_WS_LAUDO = L.ID_WS_LAUDO
                 INNER JOIN WS_LAUDO_TIPO                 LT  ON LT.ID_WS_LAUDO_TIPO = L.ID_WS_LAUDO_TIPO
                 INNER JOIN WS_LAUDO_PRODUTO_TIPO         LPT ON LPT.ID_WS_LAUDO_PRODUTO_TIPO = L.ID_WS_TIPO_PRODUTO
                 INNER JOIN WS_LAUDO_CLASSIFICACAO_TIPO   LCT ON LCT.ID_WS_LAUDO_CLASSIFICACAO_TIPO = LP.ID_WS_LAUDO_CLASSIFICACAO_TIPO
                 INNER JOIN DESPACHO                      DP  ON DP.DP_ID_DP = L.ID_DESPACHO
                 INNER JOIN ITEM_DESPACHO                 IDS ON IDS.DP_ID_DP = DP.DP_ID_DP
                 INNER JOIN DETALHE_CARREGAMENTO          DC  ON DC.DC_ID_CRG = IDS.DC_ID_CRG
                 INNER JOIN CARREGAMENTO                  CG  ON CG.CG_ID_CAR = DC.CG_ID_CAR
                 INNER JOIN VAGAO                         VG  ON VG.VG_ID_VG = DC.VG_ID_VG
                 INNER JOIN VAGAO_PEDIDO                  VPE ON VPE.VG_ID_VG = VG.VG_ID_VG AND VPE.CG_ID_CAR = CG.CG_ID_CAR
                 INNER JOIN FLUXO_COMERCIAL               FC  ON FC.FX_ID_FLX = VPE.FX_ID_FLX
                 INNER JOIN CONTRATO                      C   ON C.CZ_ID_CTT = FC.CZ_ID_CTT
                 INNER JOIN EMPRESA                       USUARIO_CAALL ON USUARIO_CAALL.EP_ID_EMP = C.EP_ID_EMP_REM
                 INNER JOIN MERCADORIA                    M   ON M.MC_ID_MRC = FC.MC_ID_MRC
                 INNER JOIN AREA_OPERACIONAL              ORIF  ON ORIF.AO_ID_AO = FC.AO_ID_EST_OR
                  LEFT JOIN AREA_OPERACIONAL              ORIF1 ON ORIF1.AO_ID_AO = FC.AO_ID_INT_OR
                 INNER JOIN AREA_OPERACIONAL              DSTF  ON DSTF.AO_ID_AO = FC.AO_ID_EST_DS
                  LEFT JOIN AREA_OPERACIONAL              DSTF1 ON DSTF1.AO_ID_AO = FC.AO_ID_INT_DS
                 INNER JOIN NOTA_FISCAL                   NFE   ON NFE.DP_ID_DP = DP.DP_ID_DP
                  LEFT JOIN EMPRESA                       REMETENTE_NFE ON REMETENTE_NFE.EP_CGC_EMP = NFE.EP_CGC_REM

                 WHERE DP.DP_DT BETWEEN TO_DATE('{dataInicio} 00:00:00', 'DD/MM/YYYY HH24:MI:SS') 
                                    AND TO_DATE('{dataFim} 23:59:59', 'DD/MM/YYYY HH24:MI:SS')

                    {fluxo}
                    {origem}
                    {destino}
                    {vagoes}
                    {segmento}
                    {cnpjRaizRecebedor}
            ");

            #endregion

            #region Where

            // 1) Data Inicio
            sql.Replace("{dataInicio}", dataInicio.ToString("dd/MM/yyyy"));

            // 2) Data Fim
            sql.Replace("{dataFim}", dataFinal.ToString("dd/MM/yyyy"));

            // 3) Fluxo
            fluxo = fluxo.ToUpper();
            fluxo = Regex.Replace(fluxo, @"[^\d+]", string.Empty);
            sql.Replace("{fluxo}",
                        !String.IsNullOrEmpty(fluxo) ? "AND REGEXP_REPLACE(FC.FX_COD_FLX, " + "'^(\\D*)'" + ", '') = '" + fluxo + "'" : String.Empty);

            // 4) Origem
            origem = origem.ToUpper();
            sql.Replace("{origem}", !String.IsNullOrEmpty(origem) ? String.Format(" AND NVL(ORIF1.AO_COD_AOP, ORIF.AO_COD_AOP) = '{0}' ", origem) : String.Empty);

            // 5) Destino
            destino = destino.ToUpper();
            sql.Replace("{destino}", !String.IsNullOrEmpty(destino) ? String.Format(" AND NVL(DSTF1.AO_COD_AOP, DSTF.AO_COD_AOP) = '{0}' ", destino) : String.Empty);

            // 6) Vagões
            var vagoesSeparadosVirgula = String.Empty;
            if (vagoes.Length > 0)
            {
                foreach (var vagao in vagoes)
                {
                    vagoesSeparadosVirgula += String.Format("{0},", vagao);
                }
                vagoesSeparadosVirgula = vagoesSeparadosVirgula.Remove(vagoesSeparadosVirgula.Length - 1);
            }

            sql.Replace("{vagoes}", !String.IsNullOrEmpty(vagoesSeparadosVirgula) ? String.Format(" AND VG.VG_COD_VAG IN ({0}) ", vagoesSeparadosVirgula) : String.Empty);

            // 11) Segmento
            sql.Replace("{segmento}", !String.IsNullOrEmpty(segmento) ? String.Format(" AND FC.FX_COD_SEGMENTO = '{0}' ", segmento) : String.Empty);

            // 15) Filtro CnpjRaizRecebedor (utilizado pelo CAALL)
            if (String.IsNullOrEmpty(cnpjRaizRecebedor))
            {
                sql.Replace("{cnpjRaizRecebedor}", String.Empty);
            }
            else
            {
                cnpjRaizRecebedor = cnpjRaizRecebedor.Substring(0, 8);
                sql.Replace("{cnpjRaizRecebedor}", String.Format(" AND SUBSTR(USUARIO_CAALL.EP_CGC_EMP, 1, 8) = '{0}' ", cnpjRaizRecebedor));
            }

            #endregion

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());

                foreach (var p in parameters)
                {
                    p.Invoke(query);
                }

                query.SetResultTransformer(Transformers.AliasToBean<PainelExpedicaoRelatorioLaudoDto>());

                var itens = query.List<PainelExpedicaoRelatorioLaudoDto>();
                var total = itens.Count;

                var result = new ResultadoPaginado<PainelExpedicaoRelatorioLaudoDto>
                {
                    Items = itens,
                    Total = (long)total
                };

                return result;
            }
        }

        public ICollection<PainelExpedicaoRelatorioLaudoDto> ObterRelatorioLaudosPorOsId(decimal osId, decimal recebedorId)
        {
            #region SQL Query

            var sql = string.Format(@"
                SELECT DISTINCT
                       VG.VG_COD_VAG                          AS ""Vagao""                     
                     , NFE.NF_NRO_NF                          AS ""NumeroNfe""
                     , REMETENTE_NFE.EP_RAZ_SCL               AS ""RemetenteNfe""
                     , DP.DP_DT                               AS ""DataCarregamento""
                     , FC.FX_COD_FLX                          AS ""FluxoComercial""
                     , NVL(ORIF1.AO_COD_AOP, ORIF.AO_COD_AOP) AS ""Origem""
                     , NVL(DSTF1.AO_COD_AOP, DSTF.AO_COD_AOP) AS ""Destino""
                     , FC.FX_COD_SEGMENTO                     AS ""FluxoSegmento""
                     , L.NUMERO_LAUDO                         AS ""NumeroLaudo""
                     , L.NUMERO_OS_LAUDO                      AS ""NumeroOsLaudo""
                     , LM.DATA_RECEBIMENTO                    AS ""DataRecebimentoLaudo""
                     , LT.DESCRICAO                           AS ""LaudoTipo""
                     , LE.NOME_EMPRESA                        AS ""Empresa""
                     , LE.CNPJ_EMPRESA                        AS ""CnpjEmpresa""
                     , L.CLASSIFICADOR_NOME                   AS ""Classificador""
                     , L.CLASSIFICADOR_CPF                    AS ""CpfClassificador""
                     , LPT.DESCRICAO                          AS ""Produto""
                     , LCT.CODIGO_CLASSIFICACAO               AS ""CodigoClassificacao""
                     , LCT.DESCRICAO                          AS ""Classificacao""
                     , LP.VALOR                               AS ""Porcentagem""

                  FROM T2_OS       TOS
            INNER JOIN TREM        TR ON TR.OF_ID_OSV = TOS.X1_ID_OS

            INNER JOIN (
                            SELECT CV_TMP.VG_ID_VG       AS VG_ID_VG
                                 , TOS_TMP.X1_ID_OS      AS X1_ID_OS
                                 , MAX(CP_TMP.CP_ID_CPS) AS CP_ID_CPS
                              FROM COMPVAGAO    CV_TMP
                              JOIN COMPOSICAO   CP_TMP   ON CP_TMP.CP_ID_CPS = CV_TMP.CP_ID_CPS
                              JOIN TREM         TR_TMP   ON TR_TMP.TR_ID_TRM = CP_TMP.TR_ID_TRM
                              JOIN T2_OS        TOS_TMP  ON TOS_TMP.X1_ID_OS = TR_TMP.OF_ID_OSV
                             WHERE CP_TMP.CP_TIMESTAMP > SYSDATE - 30 -- 1 mes de histórico
                             GROUP BY CV_TMP.VG_ID_VG
                                    , TOS_TMP.X1_ID_OS
                     ) ULTIMA_COMPOSICAO ON ULTIMA_COMPOSICAO.X1_ID_OS = TOS.X1_ID_OS

            INNER JOIN COMPOSICAO                    CP    ON CP.CP_ID_CPS = ULTIMA_COMPOSICAO.CP_ID_CPS              
            INNER JOIN VAGAO_PEDIDO_VIG              VPE   ON VPE.VG_ID_VG = ULTIMA_COMPOSICAO.VG_ID_VG
            INNER JOIN CARREGAMENTO                  CG    ON CG.CG_ID_CAR = VPE.CG_ID_CAR
            INNER JOIN DETALHE_CARREGAMENTO          DC    ON DC.CG_ID_CAR = CG.CG_ID_CAR
            INNER JOIN ITEM_DESPACHO                 IDS   ON IDS.DC_ID_CRG = DC.DC_ID_CRG
            INNER JOIN DESPACHO                      DP    ON DP.DP_ID_DP = IDS.DP_ID_DP
            INNER JOIN VAGAO                         VG    ON VG.VG_ID_VG = IDS.VG_ID_VG
            INNER JOIN FLUXO_COMERCIAL               FC    ON FC.FX_ID_FLX = VPE.FX_ID_FLX
            INNER JOIN EMPRESA                RECEBEDOR    ON RECEBEDOR.EP_ID_EMP = FC.EP_ID_EMP_DST
            INNER JOIN AREA_OPERACIONAL              ORIF  ON ORIF.AO_ID_AO = FC.AO_ID_EST_OR
             LEFT JOIN AREA_OPERACIONAL              ORIF1 ON ORIF1.AO_ID_AO = FC.AO_ID_INT_OR
            INNER JOIN AREA_OPERACIONAL              DSTF  ON DSTF.AO_ID_AO = FC.AO_ID_EST_DS
             LEFT JOIN AREA_OPERACIONAL              DSTF1 ON DSTF1.AO_ID_AO = FC.AO_ID_INT_DS
            INNER JOIN NOTA_FISCAL                   NFE   ON NFE.DP_ID_DP = DP.DP_ID_DP
             LEFT JOIN EMPRESA                       REMETENTE_NFE ON REMETENTE_NFE.EP_CGC_EMP = NFE.EP_CGC_REM

            INNER JOIN WS_LAUDO                      L   ON L.CODIGO_VAGAO = VG.VG_COD_VAG /*L.ID_DESPACHO = DP.DP_ID_DP*/
                                                        AND TO_DATE(TO_CHAR(L.DATA_CLASSIFICACAO,'dd/MM/yyyy'),'dd/MM/yyyy') >= TO_DATE(TO_CHAR(DP.DP_DT - 3,'dd/MM/yyyy'),'dd/MM/yyyy')
            INNER JOIN WS_LAUDO_PRODUTO              LP  ON LP.ID_WS_LAUDO = L.ID_WS_LAUDO
            INNER JOIN WS_LAUDO_TIPO                 LT  ON LT.ID_WS_LAUDO_TIPO = L.ID_WS_LAUDO_TIPO
            INNER JOIN WS_LAUDO_PRODUTO_TIPO         LPT ON LPT.ID_WS_LAUDO_PRODUTO_TIPO = L.ID_WS_TIPO_PRODUTO
            INNER JOIN WS_LAUDO_CLASSIFICACAO_TIPO   LCT ON LCT.ID_WS_LAUDO_CLASSIFICACAO_TIPO = LP.ID_WS_LAUDO_CLASSIFICACAO_TIPO
            INNER JOIN WS_LAUDO_LOG                  LL  ON LL.ID_WS_LAUDO_LOG = L.ID_WS_LAUDO_LOG
            INNER JOIN WS_LAUDO_EMPRESA              LE  ON LE.ID_WS_LAUDO_EMPRESA = LL.ID_WS_LAUDO_EMPRESA
            INNER JOIN WS_LAUDO_MENSAGEM             LM  ON LM.ID_WS_LAUDO_MENSAGEM = LL.ID_WS_LAUDO_MENSAGEM
            INNER JOIN (
                            SELECT MAX(TMP.ID_WS_LAUDO_MENSAGEM) AS ID
                                 , TMP.PROTOCOLO_REQUEST         AS PROTOCOLO_REQUEST
                                 , TMP_LOG.ID_WS_LAUDO_EMPRESA   AS ID_WS_LAUDO_EMPRESA
                              FROM WS_LAUDO_MENSAGEM TMP
                              JOIN WS_LAUDO_LOG TMP_LOG ON TMP_LOG.ID_WS_LAUDO_MENSAGEM = TMP.ID_WS_LAUDO_MENSAGEM
                             GROUP BY TMP_LOG.ID_WS_LAUDO_EMPRESA
                                    , TMP.PROTOCOLO_REQUEST
                      ) TMP_MAX ON TMP_MAX.ID = LM.ID_WS_LAUDO_MENSAGEM

                  WHERE NFE.EP_CGC_REM IS NOT NULL
                    AND TOS.X1_ID_OS = {0}
                    AND RECEBEDOR.EP_ID_EMP = {1}", osId, recebedorId);

            #endregion

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql);
                query.SetResultTransformer(Transformers.AliasToBean<PainelExpedicaoRelatorioLaudoDto>());
                var itens = query.List<PainelExpedicaoRelatorioLaudoDto>();
                return itens;
            }
        }

        public ResultadoPaginado<PainelExpedicaoConferenciaArquivosDto> ObterPainelExpedicaoConferenciaArquivos(
                                                                    DetalhesPaginacao detalhesPaginacao,
                                                                    DateTime dataCadastroBo,
                                                                    DateTime dataCadastroFinalBo,
                                                                    string fluxo,
                                                                    string origem,
                                                                    string destino,
                                                                    string serie,
                                                                    string[] vagoes,
                                                                    string localAtual,
                                                                    string cliente,
                                                                    string expedidor,
                                                                    string[] mercadorias,
                                                                    string segmento,
                                                                    bool arquivosPatio,
                                                                    PainelExpedicaoPerfilAcessoEnum perfilAcesso)
        {
            var parameters = new List<Action<IQuery>>();

            var hql = new StringBuilder(@"SELECT    DISTINCT agp.agrupamento_codigo as ""IdAgrupamentoMd"",

                                                    CASE WHEN agp.agrupamento_codigo IS NOT NULL 
                                                              THEN NULL
                                                              ELSE bo.id_bo END as ""IdAgrupamentoSimples"",

                                                    CASE WHEN agp.agrupamento_codigo IS NOT NULL 
                                                              THEN vag.vg_cod_vag || ' Cod MD ' || TO_CHAR(agp.agrupamento_codigo) 
                                                              ELSE TO_CHAR(vag.vg_cod_vag) || ' Hora: ' || TO_CHAR(bo.BO_TIMESTAMP, 'HH24:MI') END as ""Agrupador"",

                                                     BO.ID_BO           AS ""Id"",
                                                     BO.IDT_SIT         AS ""Situacao"",
                                                     VAG.ID_VAGAO       AS ""VagaoId"", 
                                                     VAG.VG_COD_VAG     AS ""Vagao"",
                                                     SV.SV_COD_SER      AS ""Serie"",
                                                     VAG.PS_TOTAL       AS ""PesoTotal"", 
                                                     FC.FX_COD_FLX      AS ""Fluxo"",
                                                     FC.FX_COD_SEGMENTO AS ""FluxoSegmento"",
                                                     MRC.MC_DRS_PT      AS ""FluxoMercadoria"",

                                                     Recebedor.EP_DSC_RSM AS ""Recebedor"",
                                                     EXPEDIDOR.EP_DSC_RSM AS ""Expedidor"",
                                                     CFG.CLIENTE_DSC      AS ""Cliente363"",

                                                     nf.nfe_chave_nfe as ""ChaveNota"",
                                                     CASE WHEN BO.NOM_ARQ = 'INT_BR' THEN VAG.PS_TOTAL
                                                          ELSE vnf.NFE_PESO / 1000
                                                     END as ""PesoNfe"",
                                                     vnf.NFE_VOLUME as ""VolumeNfe"",
                                                     nf.peso_nf as ""PesoRateioNfe"",
                                                     nf.Num_Container as ""Conteiner"",
                                                     bo.OBSERVACAO_RECUSADO as ""ObservacaoRecusado"",
                                                     bo.BO_TIMESTAMP as ""DataHoraCadastro"",
                                                     fc.AO_ID_INT_OR as ""Id_Intercambio_Origem"",
                                                     bo.NUM_DPCH_INTERC as ""NumeroDespacho"",
                                                     bo.SERIE_DPCH_INTERC as ""SerieDespacho"",
                                                     bo.DT_CARR_DPCH_INTERC as ""DataDespacho"",
                                                     aom.AO_COD_AOP as ""LocalAtual"",

                                                     VAG.MD AS ""MultiploDespacho"",
                                                     CASE WHEN (MULTIPLO_DESPACHO.Nao >= 1) AND (MULTIPLO_DESPACHO.Sim + MULTIPLO_DESPACHO.Nao = MULTIPLO_DESPACHO.Total)
                                                          THEN 'S'
                                                          ELSE 'N'
                                                           END  AS ""MultiploDespachoStatus"",

                                                    NVL(ORIF1.AO_COD_AOP, ORIF.AO_COD_AOP) AS ""Origem""

                                                 FROM BO_BOLAR_BO       BO
                                                 JOIN BO_BOLAR_VAGAO    VAG         ON VAG.ID_BO_ID = BO.ID_BO
                                                 JOIN BO_BOLAR_NF       NF          ON NF.ID_VAGAO_ID = VAG.ID_VAGAO
                                                 JOIN BO_BOLAR_IMP_CFG  CFG         ON CFG.CLIENTE = BO.AO_COD_AOP
                                                 JOIN FLUXO_COMERCIAL   FC          ON LTRIM(REGEXP_REPLACE(FC.FX_COD_FLX, '^(\D*)', ''), '0') = TO_CHAR(BO.COD_FLX)
                                                 JOIN MERCADORIA        MRC         ON MRC.MC_ID_MRC = FC.MC_ID_MRC

                                                 JOIN AREA_OPERACIONAL      ORIF            ON ORIF.AO_ID_AO = FC.AO_ID_EST_OR
                                                 LEFT JOIN AREA_OPERACIONAL ORIF1           ON ORIF1.AO_ID_AO = FC.AO_ID_INT_OR

                                                 JOIN EMPRESA           RECEBEDOR   ON RECEBEDOR.EP_ID_EMP = FC.EP_ID_EMP_DST
                                                 JOIN EMPRESA           EXPEDIDOR   ON EXPEDIDOR.EP_ID_EMP = FC.EP_ID_EMP_REM
                                                 LEFT JOIN VW_NFE VNF ON VNF.NFE_CHAVE = NF.NFE_CHAVE_NFE AND NF.NFE_CHAVE_NFE IS NOT NULL
                                                 LEFT JOIN BO_BOLAR_AGRUPAMENTO AGP ON AGP.ID_BO = BO.ID_BO

                                                 LEFT JOIN (
                                                            SELECT SUM(CASE WHEN BVA.MD = 'SIM' THEN 1 ELSE 0 END) AS Sim
                                                                 , SUM(CASE WHEN BVA.MD = 'NAO' THEN 1 ELSE 0 END) AS Nao
                                                                 , COUNT(DISTINCT BVA.ID_VAGAO)                  AS Total
                                                                 , BVA.VG_COD_VAG                   AS VG_COD_VAG
                                                              FROM BO_BOLAR_BO          BBO
                                                        INNER JOIN BO_BOLAR_VAGAO       BVA ON BVA.ID_BO_ID = BBO.ID_BO
                                                             WHERE BBO.IDT_SIT IN ('Q', 'N', 'S')
                                                               AND BBO.BO_TIMESTAMP >= TO_DATE('{DataCadastroInicialBoFiltro}', 'MM/DD/YYYY')
                                                               AND BBO.BO_TIMESTAMP  < TO_DATE('{DataCadastroFinalBoFiltro}', 'MM/DD/YYYY')
                                                          GROUP BY BVA.VG_COD_VAG
                                                 ) MULTIPLO_DESPACHO ON MULTIPLO_DESPACHO.VG_COD_VAG = VAG.VG_COD_VAG

                                                 LEFT JOIN VAGAO VG ON VAG.VG_COD_VAG = VG.VG_COD_VAG
                                                 LEFT JOIN SERIE_VAGAO SV ON SV.SV_ID_SV = VG.SV_ID_SV 
                                                 LEFT JOIN VAGAO_PATIO_VIG vpv on vpv.VG_ID_VG = vg.VG_ID_VG
                                                 LEFT JOIN AREA_OPERACIONAL aof on vpv.AO_ID_AO = aof.AO_ID_AO
	                                             LEFT JOIN AREA_OPERACIONAL aom on aof.AO_ID_AO_INF = aom.AO_ID_AO

                                            WHERE bo.idt_sit = {StatusFila}
                                            AND bo.BO_TIMESTAMP >= TO_DATE('{DataCadastroInicialBoFiltro}', 'MM/DD/YYYY')
                                            AND bo.BO_TIMESTAMP  < TO_DATE('{DataCadastroFinalBoFiltro}', 'MM/DD/YYYY')
                                            AND agp.agrupamento_codigo IS NOT NULL 

                                            AND EXISTS (SELECT 1 FROM bo_bolar_agrupamento AGPF
                                                WHERE AGPF.agrupamento_codigo = agp.agrupamento_codigo
                                                AND exists (select 1 from bo_bolar_bo BOF 
                                                                                ,FLUXO_COMERCIAL FCF 
                                                                                ,AREA_OPERACIONAL AOF
                                                                                ,AREA_OPERACIONAL AODF
                                                                                ,EMPRESA EMPF
                                                                                ,Mercadoria MRCF
                                                            where BOF.id_bo = AGPF.id_bo
                                                            AND LTRIM(REGEXP_REPLACE(FCF.fx_cod_flx, '^(\D*)', ''), '0') = TO_CHAR(BOF.cod_flx)
                                                            AND AOF.ao_id_ao = FCF.ao_id_est_or
                                                            AND AODF.ao_id_ao = FCF.ao_id_est_ds
                                                            AND EMPF.ep_id_emp = FCF.ep_id_emp_cor
                                                            AND MRCF.mc_id_mrc = FCF.mc_id_mrc
                                                            {FluxoFiltroAgrupamento} 
                                                            {OrigemFiltroAgrupamento}
                                                            {DestinoFiltroAgrupamento}
                                                            {MercadoriaFiltroAgrupamento}
                                                            {SegmentoFiltroAgrupamento}
                                                            ))

                                            {VagoesFiltroAgrupamento}
                                            {SerieFiltro}
                                            {ExpedidorFiltro}
                                            {ClienteFiltro}

                                            UNION

                                            SELECT    DISTINCT agp.agrupamento_codigo as ""IdAgrupamentoMd"",

                                                    CASE WHEN agp.agrupamento_codigo IS NOT NULL 
                                                              THEN NULL
                                                              ELSE bo.id_bo END as ""IdAgrupamentoSimples"",

                                                    CASE WHEN agp.agrupamento_codigo IS NOT NULL 
                                                              THEN vag.vg_cod_vag || ' Cod MD ' || TO_CHAR(agp.agrupamento_codigo) 
                                                              ELSE TO_CHAR(vag.vg_cod_vag) || ' Hora: ' || TO_CHAR(bo.BO_TIMESTAMP, 'HH24:MI') END as ""Agrupador"",

                                                     BO.ID_BO           AS ""Id"",
                                                     BO.IDT_SIT         AS ""Situacao"",
                                                     VAG.ID_VAGAO       AS ""VagaoId"",
                                                     VAG.VG_COD_VAG     AS ""Vagao"",
                                                     SV.SV_COD_SER      AS ""Serie"",
                                                     VAG.PS_TOTAL       AS ""PesoTotal"",
                                                     FC.FX_COD_FLX      AS ""Fluxo"",
                                                     FC.FX_COD_SEGMENTO AS ""FluxoSegmento"",
                                                     MRC.MC_DRS_PT      AS ""FluxoMercadoria"",

                                                     Recebedor.EP_DSC_RSM AS ""Recebedor"",
                                                     EXPEDIDOR.EP_DSC_RSM AS ""Expedidor"",
                                                     CFG.CLIENTE_DSC      AS ""Cliente363"",

                                                     nf.nfe_chave_nfe as ""ChaveNota"",
                                                     CASE WHEN BO.NOM_ARQ = 'INT_BR' THEN VAG.PS_TOTAL
                                                          ELSE vnf.NFE_PESO / 1000 
                                                     END as ""PesoNfe"",
                                                     vnf.NFE_VOLUME as ""VolumeNfe"",
                                                     nf.peso_nf as ""PesoRateioNfe"",
                                                     nf.Num_Container as ""Conteiner"",
                                                     bo.OBSERVACAO_RECUSADO as ""ObservacaoRecusado"",
                                                     bo.BO_TIMESTAMP as ""DataHoraCadastro"",
                                                     fc.AO_ID_INT_OR as ""Id_Intercambio_Origem"",
                                                     bo.NUM_DPCH_INTERC as ""NumeroDespacho"",
                                                     bo.SERIE_DPCH_INTERC as ""SerieDespacho"",
                                                     bo.DT_CARR_DPCH_INTERC as ""DataDespacho"",
                                                     aom.AO_COD_AOP as ""LocalAtual"",

                                                     VAG.MD AS ""MultiploDespacho"",
                                                     CASE WHEN (MULTIPLO_DESPACHO.Nao >= 1) AND (MULTIPLO_DESPACHO.Sim + MULTIPLO_DESPACHO.Nao = MULTIPLO_DESPACHO.Total)
                                                          THEN 'S'
                                                          ELSE 'N'
                                                           END  AS ""MultiploDespachoStatus"",

                                                     NVL(ORIF1.AO_COD_AOP, ORIF.AO_COD_AOP) AS ""Origem""

                                            FROM BO_BOLAR_BO            bo
                                                 JOIN BO_Bolar_Vagao    vag     ON vag.ID_BO_ID = bo.id_bo
                                                 JOIN BO_BOLAR_NF       nf      ON nf.id_vagao_id = vag.id_vagao
                                                 JOIN BO_BOLAR_IMP_CFG  CFG     ON CFG.CLIENTE = BO.AO_COD_AOP
                                                 JOIN FLUXO_COMERCIAL   fc      ON LTRIM(REGEXP_REPLACE(fc.fx_cod_flx, '^(\D*)', ''), '0') = TO_CHAR(bo.cod_flx)
                                                 JOIN MERCADORIA        MRC     ON MRC.MC_ID_MRC = FC.MC_ID_MRC

                                                 JOIN AREA_OPERACIONAL      ORIF            ON ORIF.AO_ID_AO = FC.AO_ID_EST_OR
                                                 LEFT JOIN AREA_OPERACIONAL ORIF1           ON ORIF1.AO_ID_AO = FC.AO_ID_INT_OR

                                                 JOIN Empresa Recebedor ON Recebedor.Ep_Id_Emp = FC.EP_ID_EMP_DST
                                                 JOIN EMPRESA EXPEDIDOR ON EXPEDIDOR.EP_ID_EMP = FC.EP_ID_EMP_REM                                                 
                                                 LEFT JOIN vw_nfe vnf on vnf.NFE_CHAVE = nf.nfe_chave_nfe and nf.nfe_chave_nfe is not null
                                                 LEFT JOIN bo_bolar_agrupamento agp on agp.id_bo = bo.id_bo

                                                 LEFT JOIN (
                                                            SELECT SUM(CASE WHEN BVA.MD = 'SIM' THEN 1 ELSE 0 END) AS Sim
                                                                 , SUM(CASE WHEN BVA.MD = 'NAO' THEN 1 ELSE 0 END) AS Nao
                                                                 , COUNT(DISTINCT BVA.ID_VAGAO)                    AS Total
                                                                 , BVA.VG_COD_VAG                                  AS VG_COD_VAG
                                                              FROM BO_BOLAR_BO          BBO
                                                        INNER JOIN BO_BOLAR_VAGAO       BVA ON BVA.ID_BO_ID = BBO.ID_BO
                                                             WHERE BBO.IDT_SIT IN ('Q', 'N', 'S')
                                                               AND BBO.BO_TIMESTAMP >= TO_DATE('{DataCadastroInicialBoFiltro}', 'MM/DD/YYYY')
                                                               AND BBO.BO_TIMESTAMP  < TO_DATE('{DataCadastroFinalBoFiltro}', 'MM/DD/YYYY')
                                                          GROUP BY BVA.VG_COD_VAG
                                                 ) MULTIPLO_DESPACHO ON MULTIPLO_DESPACHO.VG_COD_VAG = VAG.VG_COD_VAG
                                                 
                                                 LEFT JOIN VAGAO vg on vag.VG_COD_VAG = vg.VG_COD_VAG
                                                 LEFT JOIN SERIE_VAGAO SV ON SV.SV_ID_SV = VG.SV_ID_SV 
                                                 LEFT JOIN VAGAO_PATIO_VIG vpv on vpv.VG_ID_VG = vg.VG_ID_VG
                                                 LEFT JOIN AREA_OPERACIONAL aof on vpv.AO_ID_AO = aof.AO_ID_AO
	                                             LEFT JOIN AREA_OPERACIONAL aom on aof.AO_ID_AO_INF = aom.AO_ID_AO
                                            WHERE bo.idt_sit = {StatusFila}
                                            AND bo.BO_TIMESTAMP >= TO_DATE('{DataCadastroInicialBoFiltro}', 'MM/DD/YYYY')
                                            AND bo.BO_TIMESTAMP  < TO_DATE('{DataCadastroFinalBoFiltro}', 'MM/DD/YYYY')
                                            AND agp.agrupamento_codigo IS NULL 

                                            AND exists (select 1 from bo_bolar_bo BOF 
                                                                                ,FLUXO_COMERCIAL FCF 
                                                                                ,AREA_OPERACIONAL AOF
                                                                                ,AREA_OPERACIONAL AODF
                                                                                ,EMPRESA EMPF
                                                                                ,Mercadoria MRCF
                                                            where BOF.id_bo = bo.id_bo
                                                            AND LTRIM(REGEXP_REPLACE(FCF.fx_cod_flx, '^(\D*)', ''), '0') = TO_CHAR(BOF.cod_flx) 
                                                            AND AOF.ao_id_ao = FCF.ao_id_est_or
                                                            AND AODF.ao_id_ao = FCF.ao_id_est_ds
                                                            AND EMPF.ep_id_emp = FCF.ep_id_emp_cor
                                                            AND MRCF.mc_id_mrc = FCF.mc_id_mrc
                                                            {FluxoFiltro}
                                                            {OrigemFiltro}
                                                            {DestinoFiltro}
                                                            {MercadoriaFiltro}
                                                            )
											{VagoesFiltro}
                                            {SerieFiltro}
											{SegmentoFiltro}
                                            {ExpedidorFiltro}
                                            {ClienteFiltro}
                                                                                        
                                            UNION
                                            /* CONSULTA UTILIZADA PARA RETORNAR OS REGISTROS INSERIDOS NA BO_BOLAR */
                                            SELECT    DISTINCT agp.agrupamento_codigo as ""IdAgrupamentoMd"",

                                                    CASE WHEN agp.agrupamento_codigo IS NOT NULL 
                                                              THEN NULL
                                                              ELSE bo.id_bo END as ""IdAgrupamentoSimples"",

                                                    CASE WHEN agp.agrupamento_codigo IS NOT NULL 
                                                              THEN vag.vg_cod_vag || ' Cod MD ' || TO_CHAR(agp.agrupamento_codigo) 
                                                              ELSE TO_CHAR(vag.vg_cod_vag) || ' Hora: ' || TO_CHAR(bo.BO_TIMESTAMP, 'HH24:MI') END as ""Agrupador"",

                                                     BO.ID_BO           AS ""Id"",
                                                     BO.IDT_SIT         AS ""Situacao"",
                                                     VAG.ID_VAGAO       AS ""VagaoId"", 
                                                     VAG.VG_COD_VAG     AS ""Vagao"",
                                                     SV.SV_COD_SER      AS ""Serie"",
                                                     VAG.PS_TOTAL       AS ""PesoTotal"", 
                                                     FC.FX_COD_FLX      AS ""Fluxo"",
                                                     FC.FX_COD_SEGMENTO AS ""FluxoSegmento"",
                                                     MRC.MC_DRS_PT      AS ""FluxoMercadoria"",

                                                     Recebedor.EP_DSC_RSM AS ""Recebedor"",
                                                     EXPEDIDOR.EP_DSC_RSM AS ""Expedidor"",
                                                     CFG.CLIENTE_DSC      AS ""Cliente363"",
                                                     nf.nfe_chave_nfe as ""ChaveNota"",
                                                     
                                                     CASE WHEN BO.NOM_ARQ = 'INT_BR' THEN VAG.PS_TOTAL
                                                          ELSE vnf.NFE_PESO / 1000 
                                                     END as ""PesoNfe"",
   
                                                     vnf.NFE_VOLUME as ""VolumeNfe"",
                                                     nf.peso_nf as ""PesoRateioNfe"",
                                                     nf.Num_Container as ""Conteiner"",
                                                     bo.OBSERVACAO_RECUSADO as ""ObservacaoRecusado"",
                                                     bo.BO_TIMESTAMP as ""DataHoraCadastro"",
                                                     fc.AO_ID_INT_OR as ""Id_Intercambio_Origem"",
                                                     bo.NUM_DPCH_INTERC as ""NumeroDespacho"",
                                                     bo.SERIE_DPCH_INTERC as ""SerieDespacho"",
                                                     bo.DT_CARR_DPCH_INTERC as ""DataDespacho"",
                                                     aom.AO_COD_AOP as ""LocalAtual"",

                                                     VAG.MD AS ""MultiploDespacho"",
                                                     CASE WHEN (MULTIPLO_DESPACHO.Nao >= 1) AND (MULTIPLO_DESPACHO.Sim + MULTIPLO_DESPACHO.Nao = MULTIPLO_DESPACHO.Total)
                                                          THEN 'S'
                                                          ELSE 'N'
                                                           END  AS ""MultiploDespachoStatus"",

                                                    NVL(ORIF1.AO_COD_AOP, ORIF.AO_COD_AOP) AS ""Origem""

                                                 FROM BO_BOLAR_BO       BO
                                                 JOIN BO_BOLAR_VAGAO    VAG         ON VAG.ID_BO_ID = BO.ID_BO
                                                 JOIN BO_BOLAR_NF       NF          ON NF.ID_VAGAO_ID = VAG.ID_VAGAO
                                                 JOIN BO_BOLAR_IMP_CFG  CFG         ON CFG.CLIENTE = BO.AO_COD_AOP
                                                 LEFT JOIN FLUXO_COMERCIAL   FC          ON LTRIM(REGEXP_REPLACE(FC.FX_COD_FLX, '^(\D*)', ''), '0') = TO_CHAR(BO.COD_FLX)
                                                 LEFT JOIN MERCADORIA        MRC         ON MRC.MC_ID_MRC = FC.MC_ID_MRC

                                                 LEFT JOIN AREA_OPERACIONAL      ORIF            ON ORIF.AO_ID_AO = FC.AO_ID_EST_OR
                                                 LEFT JOIN AREA_OPERACIONAL ORIF1           ON ORIF1.AO_ID_AO = FC.AO_ID_INT_OR

                                                 LEFT JOIN EMPRESA           RECEBEDOR   ON RECEBEDOR.EP_ID_EMP = FC.EP_ID_EMP_DST
                                                 LEFT JOIN EMPRESA           EXPEDIDOR   ON EXPEDIDOR.EP_ID_EMP = FC.EP_ID_EMP_REM
                                                 LEFT JOIN VW_NFE VNF ON VNF.NFE_CHAVE = NF.NFE_CHAVE_NFE AND NF.NFE_CHAVE_NFE IS NOT NULL
                                                 LEFT JOIN BO_BOLAR_AGRUPAMENTO AGP ON AGP.ID_BO = BO.ID_BO

                                                 LEFT JOIN (
                                                            SELECT SUM(CASE WHEN BVA.MD = 'SIM' THEN 1 ELSE 0 END) AS Sim
                                                                 , SUM(CASE WHEN BVA.MD = 'NAO' THEN 1 ELSE 0 END) AS Nao
                                                                 , COUNT(DISTINCT BVA.ID_VAGAO)                  AS Total
                                                                 , BVA.VG_COD_VAG                   AS VG_COD_VAG
                                                              FROM BO_BOLAR_BO          BBO
                                                        INNER JOIN BO_BOLAR_VAGAO       BVA ON BVA.ID_BO_ID = BBO.ID_BO
                                                             WHERE BBO.IDT_SIT IN ('Q', 'N', 'S')
                                                               AND BBO.BO_TIMESTAMP >= TO_DATE('{DataCadastroInicialBoFiltro}', 'MM/DD/YYYY')
                                                               AND BBO.BO_TIMESTAMP  < TO_DATE('{DataCadastroFinalBoFiltro}', 'MM/DD/YYYY')
                                                          GROUP BY BVA.VG_COD_VAG
                                                 ) MULTIPLO_DESPACHO ON MULTIPLO_DESPACHO.VG_COD_VAG = VAG.VG_COD_VAG

                                                 LEFT JOIN VAGAO VG ON VAG.VG_COD_VAG = VG.VG_COD_VAG
                                                 LEFT JOIN SERIE_VAGAO SV ON SV.SV_ID_SV = VG.SV_ID_SV 
                                                 LEFT JOIN VAGAO_PATIO_VIG vpv on vpv.VG_ID_VG = vg.VG_ID_VG
                                                 LEFT JOIN AREA_OPERACIONAL aof on vpv.AO_ID_AO = aof.AO_ID_AO
	                                             LEFT JOIN AREA_OPERACIONAL aom on aof.AO_ID_AO_INF = aom.AO_ID_AO

                                            WHERE (bo.NOM_ARQ = 'INT_BR' OR bo.NOM_ARQ = 'INT_MOS')
                                            AND bo.idt_sit = {StatusFila}
                                            AND bo.BO_TIMESTAMP >= TO_DATE('{DataCadastroInicialBoFiltro}', 'MM/DD/YYYY')
                                            AND bo.BO_TIMESTAMP  < TO_DATE('{DataCadastroFinalBoFiltro}', 'MM/DD/YYYY')
                                            
                                            {VagoesFiltroAgrupamento}
                                            {SerieFiltro}
                                            {ExpedidorFiltro}
                                            {ClienteFiltro}

                                            {Ordenacao}
                                        ");


            hql.Replace("{DataCadastroInicialBoFiltro}", dataCadastroBo.ToString("MM/dd/yyyy"));
            hql.Replace("{DataCadastroFinalBoFiltro}", dataCadastroFinalBo.ToString("MM/dd/yyyy"));

            string statusFila = "INDEF";
            if (perfilAcesso == PainelExpedicaoPerfilAcessoEnum.Patio)
            {
                statusFila = "Q";
            }
            else if ((perfilAcesso == PainelExpedicaoPerfilAcessoEnum.Central) || (perfilAcesso == PainelExpedicaoPerfilAcessoEnum.CentralSuper))
            {
                statusFila = "S";
            }

            if (arquivosPatio) // Flag que indica se deve listar somente arquivos do Pátio (independente do perfil do usuário)
            {
                statusFila = "Q";
            }

            hql.Replace("{StatusFila}", "'" + statusFila + "'");

            // Filtro: Fluxo
            fluxo = fluxo.ToUpper();
            if (!string.IsNullOrWhiteSpace(fluxo))
            {
                fluxo = Regex.Replace(fluxo, @"[^\d+]", string.Empty);
                hql.Replace("{FluxoFiltro}",
                            "AND REGEXP_REPLACE(FCF.FX_COD_FLX, " + "'^(\\D*)'" + ", '') = '" + fluxo + "'");
                hql.Replace("{FluxoFiltroAgrupamento}",
                            "AND REGEXP_REPLACE(FCF.FX_COD_FLX, " + "'^(\\D*)'" + ", '') = '" + fluxo + "'");
            }
            else
            {
                hql.Replace("{FluxoFiltro}", "");
                hql.Replace("{FluxoFiltroAgrupamento}", "");
            }

            // Filtro: Origem
            origem = origem.ToUpper();
            if (!string.IsNullOrWhiteSpace(origem))
            {
                hql.Replace("{OrigemFiltro}", "AND AOF.AO_COD_AOP = '" + origem + "'");
                hql.Replace("{OrigemFiltroAgrupamento}", "AND AOF.AO_COD_AOP = '" + origem + "'");
            }
            else
            {
                hql.Replace("{OrigemFiltro}", "");
                hql.Replace("{OrigemFiltroAgrupamento}", "");
            }

            // Filtro: Destino
            destino = destino.ToUpper();
            if (!string.IsNullOrWhiteSpace(destino))
            {
                hql.Replace("{DestinoFiltro}", "AND AODF.AO_COD_AOP = '" + destino + "'");
                hql.Replace("{DestinoFiltroAgrupamento}", "AND AODF.AO_COD_AOP = '" + destino + "'");
            }
            else
            {
                hql.Replace("{DestinoFiltro}", "");
                hql.Replace("{DestinoFiltroAgrupamento}", "");
            }

            // Filtro: Vagões
            if (vagoes.Length > 0)
            {

                string vagoesList = "";
                foreach (string v in vagoes)
                {
                    if (String.IsNullOrEmpty(vagoesList))
                        vagoesList = "'" + v + "'";
                    else
                        vagoesList = vagoesList + ", '" + v + "'";
                }

                if (!(String.IsNullOrEmpty(vagoesList)))
                {
                    hql.Replace("{VagoesFiltro}", "AND VAG.VG_COD_VAG in ( " + vagoesList + ")");
                    hql.Replace("{VagoesFiltroAgrupamento}", "AND VAG.VG_COD_VAG in ( " + vagoesList + ")");
                }
                else
                    hql.Replace("{VagoesFiltro}", "");

            }
            else
            {
                hql.Replace("{VagoesFiltro}", "");
                hql.Replace("{VagoesFiltroAgrupamento}", "");
            }

            // Filtro: Série Vagao
            if (!String.IsNullOrEmpty(serie))
            {
                hql.Replace("{SerieFiltro}", "AND SV.SV_COD_SER = '" + serie + "'");
            }
            else
            {
                hql.Replace("{SerieFiltro}", String.Empty);
            }

            // Filtro: Cliente
            if (!string.IsNullOrWhiteSpace(cliente))
            {
                hql.Replace("{ClienteFiltro}", "AND CFG.ID_BO_BOLAR_IMP_CFG = " + cliente);
            }
            else
            {
                hql.Replace("{ClienteFiltro}", "");
            }

            if (!String.IsNullOrEmpty(expedidor))
            {
                hql.Replace("{ExpedidorFiltro}", "AND EXPEDIDOR.EP_DSC_RSM = '" + expedidor + "'");
            }
            else
            {
                hql.Replace("{ExpedidorFiltro}", String.Empty);
            }

            if (mercadorias.Length > 0)
            {
                string mercadoriasList = "";
                foreach (string v in mercadorias)
                {
                    if (String.IsNullOrEmpty(mercadoriasList))
                        mercadoriasList = "'" + v + "'";
                    else
                        mercadoriasList = mercadoriasList + ", '" + v + "'";
                }

                if (!(String.IsNullOrEmpty(mercadoriasList)))
                {
                    hql.Replace("{MercadoriaFiltro}", "AND MRCF.MC_COD_MRC in ( " + mercadoriasList + ")");
                    hql.Replace("{MercadoriaFiltroAgrupamento}", "AND MRCF.MC_COD_MRC in ( " + mercadoriasList + ")");
                }
                else
                {
                    hql.Replace("{MercadoriaFiltro}", "");
                    hql.Replace("{MercadoriaFiltroAgrupamento}", "");
                }
            }
            else
            {
                hql.Replace("{MercadoriaFiltro}", "");
                hql.Replace("{MercadoriaFiltroAgrupamento}", "");
            }

            // Filtro: Segmento
            if (!string.IsNullOrWhiteSpace(segmento))
            {
                hql.Replace("{SegmentoFiltro}", "AND FC.FX_COD_SEGMENTO = '" + segmento + "'");
                hql.Replace("{SegmentoFiltroAgrupamento}", "AND FCF.FX_COD_SEGMENTO = '" + segmento + "'");
            }
            else
            {
                hql.Replace("{SegmentoFiltro}", "");
                hql.Replace("{SegmentoFiltroAgrupamento}", "");
            }

            // Ordenação:
            //      7) Vagão
            //      9) Fluxo
            //      5) BO_ID
            hql.Replace("{Ordenacao}", "ORDER BY 7 ASC, 9 ASC, 5 ASC");

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(hql.ToString());
                //var queryCount = session.CreateSQLQuery(string.Format(@"SELECT COUNT(1) FROM ({0})", hql.ToString()));

                foreach (var p in parameters)
                {
                    p.Invoke(query);
                    //p.Invoke(queryCount);
                }

                query.SetResultTransformer(Transformers.AliasToBean<PainelExpedicaoConferenciaArquivosDto>());
                query.SetFirstResult(detalhesPaginacao.Inicio ?? 0);
                query.SetMaxResults(detalhesPaginacao.Limite ?? 200);

                //var total = queryCount.UniqueResult<decimal>();
                var itens = query.List<PainelExpedicaoConferenciaArquivosDto>();

                // Filtro: Local Atual
                if (itens != null)
                {
                    if (itens.Count > 0)
                    {
                        if (!string.IsNullOrWhiteSpace(localAtual))
                        {
                            localAtual = localAtual.ToUpper();
                            itens = itens
                                .Cast<PainelExpedicaoConferenciaArquivosDto>()
                                .Where(i => i.LocalAtual == localAtual).ToList();

                        }
                    }
                }

                var total = itens.Count;
                var result = new ResultadoPaginado<PainelExpedicaoConferenciaArquivosDto>
                {
                    Items = itens,
                    Total = (long)total
                };

                return result;
            }
        }

        /// <summary>
        /// Salva as configurações editadas
        /// </summary>
        /// <param name="registro">Dados do registro contendo o ID a ser atualizado</param>
        public void SalvarAlteracoesConferenciaArquivos(PainelExpedicaoConferenciaArquivosDto registro)
        {
            if (registro == null || registro.Id <= 0)
                return;

            using (var session = OpenSession())
            {
                using (var trans = session.BeginTransaction())
                {
                    try
                    {
                        #region BO_BOLAR_BO

                        var queryBo = session.CreateSQLQuery(@"
                            UPDATE BO_BOLAR_BO
                               SET COD_FLX    = :codigoFluxo
                                 , LST_VAGOES = :listaVagoes
                             WHERE ID_BO      = :id");

                        queryBo.SetInt64("id", (int)registro.Id);
                        queryBo.SetString("codigoFluxo", registro.FluxoSemCaracteres);
                        queryBo.SetString("listaVagoes", registro.Vagao);

                        queryBo.ExecuteUpdate();

                        #endregion

                        #region BO_BOLAR_VAGAO

                        var queryVagao = session.CreateSQLQuery(@"
                            UPDATE BO_BOLAR_VAGAO
                               SET VG_COD_VAG = :vagao
                                 , PS_REAL    = :pesoReal
                                 , PS_TOTAL   = :pesoTotal
                                 , MD         = :md
                             WHERE ID_VAGAO   = :id");

                        queryVagao.SetInt64("id", (int)registro.VagaoId);
                        queryVagao.SetString("vagao", registro.Vagao);
                        queryVagao.SetDecimal("pesoReal", registro.PesoReal);
                        queryVagao.SetDecimal("pesoTotal", registro.PesoTotal);
                        queryVagao.SetString("md", registro.MultiploDespacho);

                        queryVagao.ExecuteUpdate();

                        #endregion

                        #region BO_BOLAR_NF

                        var queryNf = session.CreateSQLQuery(String.Format(@"
                            UPDATE BO_BOLAR_NF
                               SET PESO_NF          = :pesoRateio
                               {0}
                             WHERE ID_VAGAO_ID      = :vagaoId
                               AND NFE_CHAVE_NFE    = :chaveNota",
                                                             String.IsNullOrEmpty(registro.Conteiner) == false ? " , NUM_CONTAINER    = :container " : String.Empty));

                        queryNf.SetInt64("vagaoId", (int)registro.VagaoId);
                        queryNf.SetParameter("chaveNota", registro.ChaveNota);
                        queryNf.SetParameter("pesoRateio", registro.PesoRateioNfe);

                        if (!String.IsNullOrEmpty(registro.Conteiner))
                            queryNf.SetParameter("container", registro.Conteiner);

                        queryNf.ExecuteUpdate();

                        #endregion

                        session.Flush();
                        trans.Commit();
                    }
                    catch (Exception ex)
                    {
                        session.Flush();
                        trans.Rollback();
                        throw;
                    }
                }
            }
        }

        /// <summary>
        /// Atualiza o status do registro
        /// </summary>
        /// <param name="id">ID do registro a ser atualizado o status</param>
        /// <param name="recusar">Indicandor se foi recusado(true) ou aprovado(false)</param>
        public void AtualizarStatusConferenciaArquivos(int id,
                                                        bool recusar,
                                                        int usuarioId,
                                                        PainelExpedicaoPerfilAcessoEnum perfilAcessoUsuario,
                                                        string observacaoRecusado)
        {
            using (var session = OpenSession())
            {
                using (var trans = session.BeginTransaction())
                {
                    string sqlQueryUpdate = "";
                    string status = "";

                    // Recusando
                    if (recusar)
                    {
                        sqlQueryUpdate = "UPDATE BO_BOLAR_BO " +
                                         "SET IDT_SIT = :status, " +
                                         "      DATA_RECUSADO = sysdate, " +
                                         "      USU_ID_RECUSADO = :usuarioId, " +
                                         "      USU_PERFIL_ID_RECUSADO = :usuarioPerfilId, " +
                                         "      OBSERVACAO_RECUSADO = :observacaoRecusado " +
                                         "WHERE ID_BO   = :id";

                        // Se estiver recusando, o status ficará assim:
                        // * Perfil Patio: Status fica como S (recusado pelo Patio). Dessa maneira, o perfil Central irá visualizar os recusados
                        // * Perfil Central ou Central Super: Status fica como R (recusado definitivamente)
                        status = (perfilAcessoUsuario == PainelExpedicaoPerfilAcessoEnum.Patio ? "S" : "R");

                        var query = session.CreateSQLQuery(sqlQueryUpdate);

                        query.SetInt64("id", id);
                        query.SetParameter("status", status);
                        query.SetInt64("usuarioId", usuarioId);
                        query.SetInt64("usuarioPerfilId", (int)perfilAcessoUsuario);
                        query.SetParameter("observacaoRecusado", observacaoRecusado);

                        query.ExecuteUpdate();

                    }
                    else // Confirmando
                    {
                        status = "N";

                        sqlQueryUpdate = "UPDATE BO_BOLAR_BO " +
                                         "SET IDT_SIT = :status " +
                                         "     , DATA_CONFIRMADO = sysdate " +
                                         "     , USU_ID_CONFIRMADO = :usuarioId " +
                                         "     , USU_PERFIL_ID_CONFIRMADO = :usuarioPerfilId " +
                                         "WHERE ID_BO   = :id";

                        var query = session.CreateSQLQuery(sqlQueryUpdate);

                        query.SetInt64("id", id);
                        query.SetParameter("status", status);
                        query.SetInt64("usuarioId", usuarioId);
                        query.SetInt64("usuarioPerfilId", (int)perfilAcessoUsuario);

                        query.ExecuteUpdate();
                    }

                    session.Flush();
                    trans.Commit();
                }
            }
        }

        public int ImprimirDocumentos(int id)
        {
            throw new NotImplementedException();
        }

        public DespachoLocalBloqueioDto ObterDespachoLocalBloqueio(int? idEstacaoOrigem,
                                                                    string segmento,
                                                                    string cnpjRaiz,
                                                                    bool verBloqueioTodosClientes)
        {
            var parameters = new List<Action<IQuery>>();

            var queryPrincipal = new StringBuilder(@"
                SELECT  BLO.DB_ID_DB as ""Id"",
                        BLO.DB_EST_ID AS ""IdEstacao"", 
                        BLO.DB_COD_SEGMENTO as ""Segmento"", 
                        BLO.DB_CNPJ_RAIZ as ""CnpjRaiz""
                FROM   DESPACHO_LOCAL_BLOQUEIO BLO
                WHERE rownum = 1
                {estacao}
                {segmento}
                {cnpjraiz}
            ");

            if (idEstacaoOrigem.HasValue)
            {
                queryPrincipal.Replace("{estacao}",
                            "AND BLO.DB_EST_ID = " + idEstacaoOrigem.Value.ToString());
            }
            else
            {
                queryPrincipal.Replace("{estacao}", "");
            }


            if (!string.IsNullOrEmpty(segmento))
            {
                queryPrincipal.Replace("{segmento}",
                            "AND BLO.DB_COD_SEGMENTO = '" + segmento + "'");
            }
            else
            {
                queryPrincipal.Replace("{segmento}", "");
            }

            if (verBloqueioTodosClientes)
            {
                queryPrincipal.Replace("{cnpjraiz}",
                                "AND (BLO.DB_CNPJ_RAIZ = '" + "" + "' OR BLO.DB_CNPJ_RAIZ is null)");
            }
            else
            {
                if (!string.IsNullOrEmpty(cnpjRaiz))
                {
                    queryPrincipal.Replace("{cnpjraiz}",
                                "AND BLO.DB_CNPJ_RAIZ = '" + cnpjRaiz + "'");
                }
                else
                {
                    queryPrincipal.Replace("{cnpjraiz}", "");
                }
            }


            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(queryPrincipal.ToString());

                foreach (var p in parameters)
                {
                    p.Invoke(query);
                }

                query.SetResultTransformer(Transformers.AliasToBean<DespachoLocalBloqueioDto>());
                var item = query.UniqueResult<DespachoLocalBloqueioDto>();

                return item;
            }
        }




        public IList<EstacaoFaturamentoDTO> ObterOrigemFaturamentoPorUf(string uf)
        {
            //var parameters = new List<Action<IQuery>>();

            var queryPrincipal = new StringBuilder(@"
                        SELECT DISTINCT
	                        AREA_OPERACIONAL.AO_COD_AOP				AS ""Codigo"",
	                        AREA_OPERACIONAL.AO_DSC_AOP				AS ""Descricao"" ,
	                        ESTADO.ES_SGL_EST						AS ""UF""
                        FROM 
	                        PAINEL_EXPEDICAO_ORIGENS
	                        JOIN AREA_OPERACIONAL ON AREA_OPERACIONAL.AO_ID_AO = PAINEL_EXPEDICAO_ORIGENS.ID_PE_ESTACAO_FATURAMENTO_ID
	                        JOIN MUNICIPIO ON MUNICIPIO.MN_ID_MNC = AREA_OPERACIONAL.MN_ID_MNC
	                        JOIN ESTADO ON ESTADO.ES_ID_EST = MUNICIPIO.ES_ID_EST
                        WHERE 
                            1 = 1
                        ORDER BY
	                        AREA_OPERACIONAL.AO_DSC_AOP
            ");

            // Filtro: Fluxo
            var filtroUf = String.Empty;
            if (!string.IsNullOrWhiteSpace(uf))
            {
                filtroUf = $"AND ESTADO.ES_SGL_EST = '{uf.ToUpper()}'";
            }

            queryPrincipal.Replace("{FiltroUf}", filtroUf);

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(string.Format(queryPrincipal.ToString()));

                query.SetResultTransformer(Transformers.AliasToBean<EstacaoFaturamentoDTO>());
                var itens = query.List<EstacaoFaturamentoDTO>();

                return itens;
            }
        }
    }
}