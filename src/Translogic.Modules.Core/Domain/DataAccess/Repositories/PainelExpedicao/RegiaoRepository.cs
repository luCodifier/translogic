﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ALL.Core.AcessoDados;
using Translogic.Modules.Core.Domain.Model.PainelExpedicao;
using Translogic.Modules.Core.Domain.Model.PainelExpedicao.Repositories;
using NHibernate.Criterion;
using NHibernate.SqlCommand;

namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.PainelExpedicao.Parametrizacao
{
    public class RegiaoRepository : NHRepository<Regiao, int>, IRegiaoRepository
    {
    }
}