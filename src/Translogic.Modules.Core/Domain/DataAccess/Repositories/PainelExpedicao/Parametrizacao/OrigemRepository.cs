﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ALL.Core.AcessoDados;
using Translogic.Modules.Core.Domain.Model.PainelExpedicao.Parametrizacao;
using Translogic.Modules.Core.Domain.Model.PainelExpedicao.Repositories;
using NHibernate.Criterion;
using NHibernate.SqlCommand;

namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.PainelExpedicao.Parametrizacao
{
    public class OrigemRepository : NHRepository<Origem, int>, IOrigemRepository
    {
        public IList<Origem> ObterOrigens()
        {

            DetachedCriteria criteria = CriarCriteria();
            criteria.CreateAlias("AreaOperacional", "ao", JoinType.InnerJoin);
            return ObterTodos(criteria);
        }

        public Origem ObterOrigem(int id)
        {
            DetachedCriteria criteria = CriarCriteria();
            return ObterPorId(id);
        }

        public IList<Origem> ObterOrigemFaturamentoPorUf(string uf)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.CreateAlias("AreaOperacional", "ao", JoinType.InnerJoin);
            criteria.CreateAlias("AreaOperacionalFaturamento", "aof", JoinType.InnerJoin);
            criteria.CreateAlias("aof.Municipio", "mun", JoinType.LeftOuterJoin);
            criteria.CreateAlias("mun.Estado", "est", JoinType.LeftOuterJoin);

            if (!string.IsNullOrEmpty(uf))
            {
                criteria.Add(Restrictions.Eq("est.Sigla", uf.ToUpper()));
            }

            criteria.AddOrder(Order.Asc("ao.Codigo"));

            return ObterTodos(criteria);
        }
    }
}