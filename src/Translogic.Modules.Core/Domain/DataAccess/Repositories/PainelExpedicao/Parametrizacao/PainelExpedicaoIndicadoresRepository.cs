﻿using System;
using System.Collections.Generic;

namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.PainelExpedicao.Parametrizacao
{
    using ALL.Core.AcessoDados;
    using Translogic.Modules.Core.Domain.Model.PainelExpedicao.Parametrizacao;
    using Translogic.Modules.Core.Domain.Model.PainelExpedicao.Repositories;
    using NHibernate.Criterion;
    using NHibernate;

    public class PainelExpedicaoIndicadoresRepository : NHRepository<Indicador, int>, IPainelExpedicaoIndicadoresRepository
    {
        public Indicador ObterPorColunaCor(int coluna, int cor)
        {
            using (var session = OpenSession())
            {
                string sql = @" 
                    FROM Indicador IND
                   WHERE IND.IdColuna  = :idColuna
                     AND IND.IdCor     = :idCor";

                IQuery query = session.CreateQuery(sql);

                query.SetInt32("idColuna", coluna);
                query.SetInt32("idCor", cor);

                IList<Indicador> resultados = query.List<Indicador>();
                if (resultados.Count > 0)
                {
                    return resultados[0];
                }

                return null;
            }
        }

        public IList<Indicador> ObterPorColuna(int coluna)
        {
            using (var session = OpenSession())
            {
                string sql = @" 
                    FROM Indicador IND
                   WHERE IND.IdColuna = :idColuna";

                IQuery query = session.CreateQuery(sql);

                query.SetInt32("idColuna", coluna);

                IList<Indicador> resultados = query.List<Indicador>();

                return resultados;
            }
        }
    }
}