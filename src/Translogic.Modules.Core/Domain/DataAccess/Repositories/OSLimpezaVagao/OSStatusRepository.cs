﻿
namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.OSLimpezaVagao
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using ALL.Core.AcessoDados;
    using NHibernate;
    using NHibernate.Transform;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Modules.Core.Domain.Model.OSLimpezaVagao;
    using Translogic.Modules.Core.Domain.Model.OSLimpezaVagao.Repositories;

    public class OSStatusRepository : NHRepository<OSStatus, int>, IOSStatusRepository
    {
        public IList<OSStatusDto> ObterStatus()
        {
            #region SQL Query

            var parameters = new List<Action<IQuery>>();
            var sql = new StringBuilder(@"SELECT 
                QRY.ID_STATUS  AS ""IdStatus"" 
                ,QRY.DESC_STATUS AS ""DescricaoStatus"" 
                FROM (
                    SELECT 
                      LVS.ID_STATUS,
                      LVS.DESC_STATUS
                    FROM tb_os_limpeza_vagao_status  LVS
                  UNION
                    SELECT
                      0       AS ID_STATUS,
                      'Todos' AS DESC_STATUS
                    FROM DUAL
                ORDER BY ID_STATUS) QRY");

            #endregion

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());

                foreach (var p in parameters)
                {
                    p.Invoke(query);
                }

                query.SetResultTransformer(Transformers.AliasToBean<OSStatusDto>());

                var result = query.List<OSStatusDto>();

                return result;
            }

        }
    }
}