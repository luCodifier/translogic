﻿
namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.OSLimpezaVagao
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Text;
    using ALL.Core.AcessoDados;
    using NHibernate;
    using NHibernate.Transform;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Modules.Core.Domain.Model.OSLimpezaVagao;
    using Translogic.Modules.Core.Domain.Model.OSLimpezaVagao.Repositories;
    using NHibernate.Criterion;
   
    
    public class OSTipoRepository : NHRepository<OSTipoServico, int>, IOSTipoRepository
    {
        public IList<OSTipoDto> ObterTiposOSLimpeza()
        {
            //// Filtrando somente registros do tipo:
            //// 1-Limpeza
            //// 2-Lavagem
            int[] ids = {1, 2}; 

            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.In("Id", ids));

            var lista = ObterTodos(criteria).Select(o => new OSTipoDto {
                 IdTipo = o.Id,
                 DescricaoTipo = o.Descricao
            }).ToList();

            //// Adiciona opção todos no retorno da lista.
            if (lista.Any())
            {
                lista.Insert(0, new OSTipoDto { IdTipo = 0, DescricaoTipo = "Todos" });
            }

            return lista;
        }

        public IList<OSTipoDto> ObterTiposOSRevistamento()
        {
            //// Filtrando somente registros do tipo:
            //// 1-Limpeza
            //// 2-Lavagem
            int[] ids = {3}; 

            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.In("Id", ids));

            var lista = ObterTodos(criteria).Select(o => new OSTipoDto {
                 IdTipo = o.Id,
                 DescricaoTipo = o.Descricao
            }).ToList();

            //// Adiciona opção todos no retorno da lista.
            if (lista.Any())
            {
                lista.Insert(0, new OSTipoDto { IdTipo = 0, DescricaoTipo = "Todos" });
            }

            return lista;
        }
    }
}