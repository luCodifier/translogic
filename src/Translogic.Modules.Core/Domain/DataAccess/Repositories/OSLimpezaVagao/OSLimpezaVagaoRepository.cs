﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.OSLimpezaVagao
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using ALL.Core.AcessoDados;
    using NHibernate;
    using NHibernate.Transform;
    using NHibernate.Linq;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Modules.Core.Domain.Model.OSLimpezaVagao.Repositories;
    using Translogic.Modules.Core.Domain.Model.OSLimpezaVagao;
    using Translogic.Core.Infrastructure.Web;
    using ALL.Core.Dominio;
    using System.Data.OracleClient;
    using System.Data;

    public class OSLimpezaVagaoRepository : NHRepository<OSLimpezaVagao, int>, IOSLimpezaVagaoRepository
    {
        public ResultadoPaginado<OSLimpezaVagaoDto> ObterConsultaListaOs(DetalhesPaginacaoWeb detalhesPaginacaoWeb, DateTime? dataInicial, DateTime? dataFinal, 
             string local, string numOs, string idTipo, string idStatus, bool? retrabalho)
        {
            #region SQL Query

            var parameters = new List<Action<IQuery>>();
            
            var sql = new StringBuilder(@"
                    SELECT
                    os.DT_HR AS ""Data""
                    ,ao.AO_COD_AOP AS ""LocalServico""
                    ,os.ID_OS AS ""IdOs""
                    ,OS.ID_OS_PARCIAL AS ""IdOsParcial""
                    ,tpS.DESC_SERVICO AS ""Tipo""
                    ,fo.NOME_FORNECEDOR AS ""Fornecedor""
                    ,st.DESC_STATUS AS ""Status""
                    ,os.DT_ULT_ALT AS ""UltimaAlteracao""
                    ,(SELECT COUNT(1) FROM TB_OS_LIMPEZA_VAGAO_ITENS WHERE ID_OS=os.ID_OS AND RETRABALHO=1) AS ""QtRetrabalho""
                    ,os.USUARIO AS ""Usuario""
                    FROM TB_OS_LIMPEZA_VAGAO os
                    INNER JOIN TB_OS_LIMPEZA_VAGAO_STATUS st ON os.ID_STATUS=st.ID_STATUS
                    LEFT JOIN AREA_OPERACIONAL ao ON os.ID_LOCAL=ao.AO_ID_AO
                    LEFT JOIN TB_TIPOSERVICO tpS ON os.ID_TIPO_SERVICO=tpS.ID_TIPO_SERVICO
                    LEFT JOIN TB_FORNECEDOR_OS fo ON os.ID_FORNECEDOR_OS=fo.ID_FORNECEDOR_OS                    
                    ");

            #endregion

            #region Where

            var sqlWhere = new StringBuilder();

            if (dataInicial.HasValue && dataFinal.HasValue)
            {
                sqlWhere.AppendFormat(" AND os.DT_HR BETWEEN TO_DATE('{0} 00:00:00','DD/MM/YYYY HH24:MI:SS') AND TO_DATE('{1} 23:59:59','DD/MM/YYYY HH24:MI:SS') ", dataInicial.Value.ToString("dd/MM/yyyy"), dataFinal.Value.ToString("dd/MM/yyyy"));
            }
            else if (dataInicial.HasValue)
            {
                sqlWhere.AppendFormat(" AND os.DT_HR >= TO_DATE('{0} 00:00:00', 'DD/MM/YYYY HH24:MI:SS')", dataInicial.Value.ToString("dd/MM/yyyy"));            
            }
            else if (dataFinal.HasValue)
            {
                sqlWhere.AppendFormat(" AND os.DT_HR <= TO_DATE('{0} 23:59:59', 'DD/MM/YYYY HH24:MI:SS')", dataFinal.Value.ToString("dd/MM/yyyy"));      
            }

            if (!string.IsNullOrEmpty(local))
            {
                sqlWhere.AppendFormat(" AND ao.AO_COD_AOP LIKE '%{0}%'", local.ToUpper());
            }

            if (!string.IsNullOrEmpty(numOs))
            {
                sqlWhere.AppendFormat(" AND os.ID_OS LIKE '%{0}%'", numOs.ToUpper());
            }

            if (!string.IsNullOrEmpty(idTipo))
            {
                sqlWhere.AppendFormat(" AND os.ID_TIPO_SERVICO = {0}", idTipo);
            }

            if (!string.IsNullOrEmpty(idStatus))
            {
                sqlWhere.AppendFormat(" AND os.ID_STATUS in ({0})", idStatus);
            }

            if (retrabalho.HasValue)
            {
                sqlWhere.AppendFormat(" AND (SELECT COUNT(1) FROM TB_OS_LIMPEZA_VAGAO_ITENS WHERE ID_OS=os.ID_OS AND RETRABALHO=1) {0} 0", (retrabalho.Value ? ">" : "="));
            }

            if (sqlWhere.Length > 0)
            {
                string aux = " WHERE " + sqlWhere.ToString().Substring(5);
                sql.Append(aux);
            }

            #endregion

            #region OrderBy

            // Order by (Recupera o indice e o direcionamento (ASC/DESC))
            sql.Append(" ORDER BY os.ID_OS DESC");
            //sql.Append("          QRY.LOCAL, ");
            //sql.Append("          QRY.LINHA, ");
            //sql.Append("          QRY.SEQUENCIA ASC");

            #endregion

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());
                var queryCount = session.CreateSQLQuery(String.Format(@"SELECT COUNT(1) FROM ({0})", sql.ToString().Replace("{ORDENACAO}", String.Empty)));

                foreach (var p in parameters)
                {
                    p.Invoke(query);
                    p.Invoke(queryCount);
                }

                query.SetResultTransformer(Transformers.AliasToBean<OSLimpezaVagaoDto>());

                if (detalhesPaginacaoWeb.Limite != 0)
                {
                    query.SetFirstResult(detalhesPaginacaoWeb.Inicio ?? 0);
                    query.SetMaxResults(detalhesPaginacaoWeb.Limite ?? DetalhesPaginacao.LimitePadrao);
                }

                var total = queryCount.UniqueResult<decimal>();
                var itens = query.List<OSLimpezaVagaoDto>();

                var result = new ResultadoPaginado<OSLimpezaVagaoDto>
                {
                    Items = itens,
                    Total = (long)total
                };

                return result;
            }
        }

        public IList<OSLimpezaVagaoExportaDto> ObterConsultaOsParaExportar(DateTime? dataInicial, DateTime? dataFinal,
       string local, string numOs, string idTipo, string idStatus, bool? retrabalho)
        {
            #region SQL Query

            var parameters = new List<Action<IQuery>>();

            var sql = new StringBuilder(@"SELECT
                os.DT_HR AS ""Data""
                ,os.ID_OS AS ""IdOs""
                ,OS.ID_OS_PARCIAL AS ""IdOsParcial""
                ,ao.AO_COD_AOP AS ""Local""
                ,fo.NOME_FORNECEDOR AS ""Fornecedor""
                ,os.DT_HR_ENTR_FORN AS ""DataHoraEntregaOSFornecedor""
                ,os.DT_HR_DEVO_RUMO AS ""DataHoraDevolucaoOS""
                ,V.VG_COD_VAG AS ""NumeroVagao""
                ,CASE WHEN os.ID_TIPO_SERVICO = 2 THEN 1 ELSE 0 END AS ""Limpeza""
                ,CASE WHEN os.ID_TIPO_SERVICO = 1 THEN 1 ELSE 0 END AS ""Lavagem""
                ,i.RETRABALHO AS ""Retrabalho"" 
                ,i.OBSERVACAO AS ""Observacao"" 
                ,os.NOME_APROV_RUMO as ""NomeAprovadorRumo""
                ,st.DESC_STATUS AS ""Status""
                ,os.DT_ULT_ALT AS ""UltimaAlteracao""
                ,os.USUARIO AS ""Usuario""
                FROM TB_OS_LIMPEZA_VAGAO os
                INNER JOIN TB_OS_LIMPEZA_VAGAO_STATUS st ON os.ID_STATUS=st.ID_STATUS
                LEFT JOIN TB_OS_LIMPEZA_VAGAO_ITENS i on i.ID_OS = os.ID_OS
                LEFT JOIN VAGAO V ON V.VG_ID_VG = i.ID_VAGAO
                LEFT JOIN VAGAO_PATIO_VIG VPV ON VPV.VG_ID_VG = V.VG_ID_VG
                LEFT JOIN AREA_OPERACIONAL ao ON os.ID_LOCAL=ao.AO_ID_AO
                LEFT JOIN TB_TIPOSERVICO tpS ON os.ID_TIPO_SERVICO=tpS.ID_TIPO_SERVICO
                LEFT JOIN TB_FORNECEDOR_OS fo ON os.ID_FORNECEDOR_OS=fo.ID_FORNECEDOR_OS  ");

            #endregion

            #region Where

            var sqlWhere = new StringBuilder();

            if (dataInicial.HasValue && dataFinal.HasValue)
            {
                sqlWhere.AppendFormat(" AND os.DT_HR BETWEEN TO_DATE('{0} 00:00:00','DD/MM/YYYY HH24:MI:SS') AND TO_DATE('{1} 23:59:59','DD/MM/YYYY HH24:MI:SS') ", dataInicial.Value.ToString("dd/MM/yyyy"), dataFinal.Value.ToString("dd/MM/yyyy"));
            }
            else if (dataInicial.HasValue)
            {
                sqlWhere.AppendFormat(" AND os.DT_HR >= TO_DATE('{0} 00:00:00', 'DD/MM/YYYY HH24:MI:SS')", dataInicial.Value.ToString("dd/MM/yyyy"));
            }
            else if (dataFinal.HasValue)
            {
                sqlWhere.AppendFormat(" AND os.DT_HR <= TO_DATE('{0} 23:59:59', 'DD/MM/YYYY HH24:MI:SS')", dataFinal.Value.ToString("dd/MM/yyyy"));
            }

            if (!string.IsNullOrEmpty(local))
            {
                sqlWhere.AppendFormat(" AND ao.AO_COD_AOP LIKE '%{0}%'", local.ToUpper());
            }

            if (!string.IsNullOrEmpty(numOs))
            {
                sqlWhere.AppendFormat(" AND os.ID_OS LIKE '%{0}%'", numOs.ToUpper());
            }

            if (!string.IsNullOrEmpty(idTipo))
            {
                sqlWhere.AppendFormat(" AND os.ID_TIPO_SERVICO = {0}", idTipo);
            }

            if (!string.IsNullOrEmpty(idStatus))
            {
                sqlWhere.AppendFormat(" AND os.ID_STATUS in ({0})", idStatus);
            }

            if (retrabalho.HasValue)
            {
                sqlWhere.AppendFormat(" AND (SELECT COUNT(1) FROM TB_OS_LIMPEZA_VAGAO_ITENS WHERE ID_OS=os.ID_OS AND RETRABALHO=1) {0} 0", (retrabalho.Value ? ">" : "="));
            }

            if (sqlWhere.Length > 0)
            {
                string aux = " WHERE " + sqlWhere.ToString().Substring(5);
                sql.Append(aux);
            }

            #endregion

            #region OrderBy

            sql.Append(@"  ORDER BY os.ID_OS DESC 
                          , AO.AO_COD_AOP
                          , VPV.VP_NUM_SEQ ASC ");

            #endregion

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());
                query.SetResultTransformer(Transformers.AliasToBean<OSLimpezaVagaoExportaDto>());
                var result = query.List<OSLimpezaVagaoExportaDto>();

                return result;
            }
        }
    }
}