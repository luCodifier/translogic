﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.LaudoMercadoria
{
    using ALL.Core.AcessoDados;
    using Translogic.Modules.Core.Domain.Model.LaudoMercadoria;
    using Translogic.Modules.Core.Domain.Model.LaudoMercadoria.Repositories;

    public class LaudoMercadoriaEmpresaLogoRepository : NHRepository<LaudoMercadoriaEmpresaLogo, int?>, ILaudoMercadoriaEmpresaLogoRepository
    {
    }
}