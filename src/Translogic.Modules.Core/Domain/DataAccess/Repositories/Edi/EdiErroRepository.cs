﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Edi
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using ALL.Core.AcessoDados;
    using ALL.Core.Dominio;
    using NHibernate;
    using NHibernate.Criterion;
    using NHibernate.Transform;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Modules.Core.Domain.Model.Edi;
    using Translogic.Modules.Core.Domain.Model.Edi.Repositories;

    public class EdiErroRepository : NHRepository<EdiErro, int>, IEdiErroRepository
    {
        public ResultadoPaginado<EdiErroResultPesquisaDto> ObterEdiErrosPesquisa(
                                                                    DetalhesPaginacao detalhesPaginacao,
                                                                    DateTime dataInicial,
                                                                    DateTime dataFinal,
                                                                    string fluxo,
                                                                    string[] vagoes,
                                                                    string responsavelEnvio)
        {
            var parameters = new List<Action<IQuery>>();

            var hql = new StringBuilder(@"
                SELECT ERR.ID_MENSAGEM_RECEBIMENTO  AS ""IdMensagemRecebimento""
                     , NF.ID_EDI_ERRO_NF            AS ""IdErroEdiNfe""
                     , ('Data: '    || TO_CHAR(ERR.DATA_ERRO, 'DD/MM/YYYY') || ' ' ||
                        'Cliente: ' || SUBSTR(ERR.RESPONSAVEL_ENVIO, 1, 50) || ' ' ||
                        'Vagao: '   || ERR.VAGAO
                       ) AS ""Agrupador""
                     , ERR.VAGAO                AS ""Vagao""
                     , ERR.FLUXO                AS ""Fluxo""
                     , ERR.DATA_ERRO            AS ""DataErro""
                     , ERR.DATA_ENVIO           AS ""DataEnvio""
                     , ERR.RESPONSAVEL_ENVIO    AS ""ResponsavelEnvio""
                     , NF.CHAVE_NFE             AS ""ChaveNfe""
                     , NF.DATA_REGULARIZACAO    AS ""DataRegularizacao""
                     , ERR.REPROCESSADO         AS ""Reprocessado""
                  FROM (
                        SELECT TMP.VAGAO                   AS VAGAO
                             , TMP.FLUXO                   AS FLUXO
                             , TMP.RESPONSAVEL_ENVIO       AS RESPONSAVEL_ENVIO
                             , MAX(TMP.ID_EDI_ERRO)        AS ID_EDI_ERRO
                          FROM EDI_ERRO TMP
                         WHERE TMP.DATA_ERRO BETWEEN TO_DATE('{DataErroInicialFiltro}', 'MM/DD/YYYY')
                                                 AND TO_DATE('{DataErroFinalFiltro}', 'MM/DD/YYYY')
                         GROUP BY TMP.VAGAO
                                , TMP.FLUXO
                                , TMP.RESPONSAVEL_ENVIO
                  ) TMP
                  JOIN EDI_ERRO     ERR ON ERR.ID_EDI_ERRO = TMP.ID_EDI_ERRO
                  JOIN EDI_ERRO_NF  NF  ON NF.ID_EDI_ERRO  = ERR.ID_EDI_ERRO
                  JOIN EDI.WS_MENSAGEM_RECEBIMENTO MR ON MR.ID_MENSAGEM_RECEBIMENTO = ERR.ID_MENSAGEM_RECEBIMENTO
                 WHERE ERR.REPROCESSADO  = 'N'
                   /* Clausula apenas para pegar os registros que continuam com erro.
                    * Pode ocorrer de outra aplicações atualizarem o status para MRR para que o EDI.Runner reprocesse elas.
                    * Com isso, o registro continuara na tela de EDI Erros, porém ele já foi processado corretamente.
                    * Por este motivo que devemos verificar o status da mensagem recebimento como MRE. */
                   AND MR.SITUACAO_ATUAL = 'MRE' 
                   {FluxoFiltro}
                   {VagoesFiltro}
                   {ResponsavelEnvioFiltro}

                   {Ordenacao}
            ");


            hql.Replace("{DataErroInicialFiltro}", dataInicial.ToString("MM/dd/yyyy"));
            hql.Replace("{DataErroFinalFiltro}", dataFinal.AddDays(1).ToString("MM/dd/yyyy"));

            // Filtro: Fluxo
            fluxo = fluxo.ToUpper();
            if (!string.IsNullOrWhiteSpace(fluxo))
            {
                fluxo = Regex.Replace(fluxo, @"[^\d+]", string.Empty);
                hql.Replace("{FluxoFiltro}",
                            "AND REGEXP_REPLACE(ERR.FLUXO, " + "'^(\\D*)'" + ", '') = '" + fluxo + "'");
            }
            else
            {
                hql.Replace("{FluxoFiltro}", "");
            }


            // Filtro: Vagões
            if (vagoes.Length > 0)
            {

                string vagoesList = "";
                foreach (string v in vagoes)
                {
                    if (String.IsNullOrEmpty(vagoesList))
                        vagoesList = "'" + v + "'";
                    else
                        vagoesList = vagoesList + ", '" + v + "'";
                }

                if (!(String.IsNullOrEmpty(vagoesList)))
                {
                    hql.Replace("{VagoesFiltro}", "AND ERR.VAGAO IN ( " + vagoesList + ")");
                }
                else
                    hql.Replace("{VagoesFiltro}", "");

            }
            else
            {
                hql.Replace("{VagoesFiltro}", "");
            }

            // Filtro: Responsável Envio
            if (!string.IsNullOrWhiteSpace(responsavelEnvio))
            {
                hql.Replace("{ResponsavelEnvioFiltro}", "AND ERR.RESPONSAVEL_ENVIO LIKE '%" + responsavelEnvio + "%'");
            }
            else
            {
                hql.Replace("{ResponsavelEnvioFiltro}", "");
            }


            // Order by (Recupera o indice e o direcionamento (ASC/DESC))
            var colunasOrdenacao = detalhesPaginacao.ColunasOrdenacao;
            var direcoesOrdenacao = detalhesPaginacao.DirecoesOrdenacao;
            string colOrdenacaoIndex = "7";
            string ordenacaoDirecao = "ASC";

            if (colunasOrdenacao.Count > 0)
                colOrdenacaoIndex = colunasOrdenacao[0];
            if (direcoesOrdenacao.Count > 0)
                ordenacaoDirecao = direcoesOrdenacao[0];

            if (colOrdenacaoIndex.ToLower() == "vagao")
                colOrdenacaoIndex = "3";
            else if (colOrdenacaoIndex.ToLower() == "fluxo")
                colOrdenacaoIndex = "4";
            else if (colOrdenacaoIndex.ToLower() == "data erro")
                colOrdenacaoIndex = "5";
            else
            {
                colOrdenacaoIndex = "7";
                ordenacaoDirecao = "ASC";
            }


            hql.Replace("{Ordenacao}", "ORDER BY " + colOrdenacaoIndex + " " + ordenacaoDirecao);

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(hql.ToString());
                var queryCount = session.CreateSQLQuery(string.Format(@"SELECT COUNT(0) FROM ({0})", hql.ToString()));

                foreach (var p in parameters)
                {
                    p.Invoke(query);
                    p.Invoke(queryCount);
                }

                query.SetResultTransformer(Transformers.AliasToBean<EdiErroResultPesquisaDto>());
                query.SetFirstResult(detalhesPaginacao.Inicio ?? 0);
                query.SetMaxResults(detalhesPaginacao.Limite ?? DetalhesPaginacao.LimitePadrao);

                var total = queryCount.UniqueResult<decimal>();
                var itens = query.List<EdiErroResultPesquisaDto>();

                var result = new ResultadoPaginado<EdiErroResultPesquisaDto>
                {
                    Items = itens,
                    Total = (long)total
                };

                return result;
            }
        }

        public EdiErro ObterEdiErro(int idMensagemRecebimento)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.Eq("IdMensagemRecebimento", idMensagemRecebimento));
            ObterPrimeiro(criteria);

            return ObterPrimeiro(criteria);
        }

        public IList<MensagemRecebimentoDto> ObterMensagensRecebimentoErro(DateTime dataInicial, DateTime dataFinal)
        {
            var hql = new StringBuilder(@"
                SELECT MR.ID_MENSAGEM_RECEBIMENTO   AS ""Id""
                     , M.ARQUIVO_MENSAGEM           AS ""ArquivoMensagem""
                     , MR.DATA_CADASTRO             AS ""DataCadastro""
                     , ED.ED.ID_EDI_ERRO            AS ""EdiErroId""
                  FROM EDI.WS_MENSAGEM_RECEBIMENTO MR
                  JOIN EDI.WS_MENSAGEM M ON MR.ID_MENSAGEM_REQUEST = M.ID_MENSAGEM
                  LEFT JOIN EDI_ERRO ED ON ED.ID_MENSAGEM_RECEBIMENTO = MR.ID_MENSAGEM_RECEBIMENTO  
                 WHERE MR.SITUACAO_ATUAL = 'MRE'
                   AND MR.DATA_CADASTRO BETWEEN TO_DATE('{{dataInicial}}', 'MM/DD/YYYY') AND TO_DATE('{{dataFinal}}', 'MM/DD/YYYY')
                   and dbms_lob.instr( M.ARQUIVO_MENSAGEM,'NOTA_EXP_FERR') > 0
              ORDER BY MR.ID_MENSAGEM_RECEBIMENTO DESC"
            );

            hql.Replace("{{dataInicial}}", dataInicial.ToString("MM/dd/yyyy"));
            hql.Replace("{{dataFinal}}", dataFinal.ToString("MM/dd/yyyy"));

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(hql.ToString());

                query.SetResultTransformer(Transformers.AliasToBean<MensagemRecebimentoDto>());
                //var itens = query.List<MensagemRecebimentoDto>();

                //return itens.ToList();
                return query.List<MensagemRecebimentoDto>();
            }
        }

        public List<MensagemRecebimentoDto> ObterMensagensRecebimentoParaReprocessar(List<decimal> listaIdMensagemRecebimento)
        {
            var parameters = new List<Action<IQuery>>();

            var hql = new StringBuilder(@"
                SELECT DISTINCT
                       MR.ID_MENSAGEM_RECEBIMENTO AS ""Id""
                     , MR.DATA_CADASTRO           AS ""DataCadastro""
                     , MR.SITUACAO_ATUAL          AS ""Situacao""
                     , CASE WHEN NVL(
                                        (SELECT 1
                                           FROM EDI_ERRO_NF NF
                                          WHERE NF.ID_EDI_ERRO = ERR.ID_EDI_ERRO
                                            AND NF.DATA_REGULARIZACAO IS NULL
                                            AND ROWNUM = 1), 0
                                 ) = 1
                            THEN 'N'
                            ELSE 'S'
                             END AS ""PendenciaNotas""
                  FROM (
                        SELECT TMP.VAGAO                   AS VAGAO
                             , TMP.FLUXO                   AS FLUXO
                             , TMP.RESPONSAVEL_ENVIO       AS RESPONSAVEL_ENVIO
                             , MAX(TMP.ID_EDI_ERRO)        AS ID_EDI_ERRO
                          FROM EDI_ERRO TMP
                         GROUP BY TMP.VAGAO
                                , TMP.FLUXO
                                , TMP.RESPONSAVEL_ENVIO
                  ) ULTIMO_ERR
                  JOIN EDI_ERRO     ERR ON ERR.ID_EDI_ERRO = ULTIMO_ERR.ID_EDI_ERRO
                  JOIN EDI.WS_MENSAGEM_RECEBIMENTO MR ON MR.ID_MENSAGEM_RECEBIMENTO = ERR.ID_MENSAGEM_RECEBIMENTO
                 WHERE ERR.ID_MENSAGEM_RECEBIMENTO IN {FiltroIds}"
            );

            if (listaIdMensagemRecebimento.Count > 0)
            {
                var idsList = String.Empty;
                foreach (decimal i in listaIdMensagemRecebimento)
                {
                    if (String.IsNullOrEmpty(idsList))
                        idsList = i.ToString();
                    else
                        idsList = idsList + ", " + i.ToString();
                }

                if (!(String.IsNullOrEmpty(idsList)))
                {
                    hql.Replace("{FiltroIds}", "( " + idsList + ")");
                }
                else
                {
                    hql.Replace("{FiltroIds}", "IN (0)");
                }
            }
            else
            {
                hql.Replace("{FiltroIds}", "IN (0)");
            }

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(hql.ToString());

                foreach (var p in parameters)
                {
                    p.Invoke(query);
                }

                query.SetResultTransformer(Transformers.AliasToBean<MensagemRecebimentoDto>());
                var itens = query.List<MensagemRecebimentoDto>();

                return itens.ToList();
            }
        }

        public List<string> RetornarChaves(string ids)
        {
            var hql = String.Format(@"
                SELECT DISTINCT CHAVE_NFE
                  FROM EDI_ERRO_NF
                 WHERE ID_EDI_ERRO_NF IN ({0})", ids);

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(hql);

                var itens = query.List<string>();

                return itens.ToList();
            }
        }

        public void AtualizarMensagensRecebimentoParaReprocessar(List<decimal> listaIdMensagemRecebimento)
        {
            using (var session = OpenSession())
            {
                using (var trans = session.BeginTransaction())
                {
                    string sqlQueryUpdate = String.Empty;
                    sqlQueryUpdate = "UPDATE EDI.WS_MENSAGEM_RECEBIMENTO " +
                                     "SET SITUACAO_ATUAL = :situacaoReprocessar " +
                                     "WHERE ID_MENSAGEM_RECEBIMENTO in (:ids)";

                    var query = session.CreateSQLQuery(sqlQueryUpdate);

                    query.SetParameterList("ids", listaIdMensagemRecebimento);
                    query.SetParameter("situacaoReprocessar", "MRR");

                    query.ExecuteUpdate();

                    session.Flush();
                    trans.Commit();
                }
            }
        }

        public void AtualizarEdiErrosProcessados(List<decimal> ids)
        {
            using (var session = OpenSession())
            {
                using (var trans = session.BeginTransaction())
                {
                    string sqlQueryUpdate = String.Empty;
                    sqlQueryUpdate = "UPDATE EDI_ERRO " +
                                      "SET REPROCESSADO = :Reprocessado " +
                                      "WHERE ID_MENSAGEM_RECEBIMENTO in (:ids)";

                    var query = session.CreateSQLQuery(sqlQueryUpdate);

                    query.SetParameterList("ids", ids);
                    query.SetParameter("Reprocessado", "S");

                    query.ExecuteUpdate();

                    session.Flush();
                    trans.Commit();
                }
            }
        }
    }
}