﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ALL.Core.AcessoDados;
using NHibernate.SqlCommand;
using Translogic.Modules.Core.Domain.Model.Edi;
using Translogic.Modules.Core.Domain.Model.Edi.Repositories;
using NHibernate.Criterion;

namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Edi
{
    public class EdiErroNfRepository : NHRepository<EdiErroNf, int>, IEdiErroNfRepository
    {
        public IList<EdiErroNf> ObterPorIdEdiErro(int id)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.CreateAlias("EdiErro", "erro", JoinType.InnerJoin);
            criteria.Add(Restrictions.Eq("erro.Id", id));

            return ObterTodos(criteria);
        }

        /// <summary>
        /// Verifica se existe chave nfe no edi
        /// </summary>
        /// <param name="chaveNfe">Chave nfe</param>
        /// <returns>True/False</returns>
        public bool ExisteChaveNfeEdi(string chaveNfe)
        {
            var sql = String.Format("SELECT 1 FROM EDI.EDI2_NFE WHERE NFE_CHAVE = '{0}' AND ROWNUM = 1", chaveNfe);

            using (var session = OpenSession())
            {
                var queryExiste = session.CreateSQLQuery(sql);

                decimal? existe = queryExiste.UniqueResult<decimal>();

                return (existe == 1 ? true : false);
            }
        }
    }
}