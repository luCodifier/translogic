﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.IntegracaoSiv
{
    using ALL.Core.AcessoDados;
    using Model.IntegracaoSIV;
    using Model.IntegracaoSIV.Repositories;

    /// <summary>
    /// Repositorio para <see cref="SubMenuSivRepository"/>
    /// </summary>
    public class SubMenuSivRepository : NHRepository<SubMenuSiv, int?>, ISubMenuSivRepository
    {
    }
}