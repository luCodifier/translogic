﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.IntegracaoSiv
{
	using ALL.Core.AcessoDados;
	using Model.IntegracaoSIV;
	using Model.IntegracaoSIV.Repositories;

	/// <summary>
	/// Repositorio para <see cref="IDivisionSivRepository"/>
	/// </summary>
	public class DivisionSivRepository : NHRepository<DivisionSiv, int?>, IDivisionSivRepository
	{
	}
}