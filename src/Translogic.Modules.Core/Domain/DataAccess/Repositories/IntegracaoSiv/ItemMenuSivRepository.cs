﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.IntegracaoSiv
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using ALL.Core.AcessoDados;
    using Model.IntegracaoSIV;
    using Model.IntegracaoSIV.Repositories;
    using NHibernate;
    using NHibernate.Criterion;
    using NHibernate.SqlCommand;
    using NHibernate.Transform;

    /// <summary>
    /// Repositorio para <see cref="ItemMenuSivRepository"/>
    /// </summary>
    public class ItemMenuSivRepository : NHRepository<ItemMenuSiv, int?>, IItemMenuSivRepository
    {
        /// <summary>
        /// Obtém todos os itens de menu que possuem autorização
        /// </summary>
        /// <param name="codigoUsuario">Código do usuario</param>
        /// <returns>Lista de itens de menu do siv</returns>
        public IList<ItemMenuSiv> ObterTodosAutorizados(string codigoUsuario)
        {
            using (ISession session = OpenSession())
            {
                try
                {
                    session.EnableFilter("Autorizar").SetParameter("CodigoUsuario", codigoUsuario);

                    DetachedCriteria criteria = CriarCriteria();
                    criteria.SetResultTransformer(new DistinctRootEntityResultTransformer());

                    IList<ItemMenuSiv> menus = ObterTodos(criteria);

                    return menus;
                }
                finally
                {
                    session.DisableFilter("Autorizar");
                }
            }
        }

        /// <summary>
        /// Obtém o item de menu do siv pelo código e que esteja autorizado
        /// </summary>
        /// <param name="codigoTelaSiv">Codigo da tela do SIV</param>
        /// <param name="codigoUsuario">Codigo do Usuario</param>
        /// <returns>Item de menu do siv</returns>
        public ItemMenuSiv ObterPorCodigoTelaAutorizado(int codigoTelaSiv, string codigoUsuario)
        {
            using (ISession session = OpenSession())
            {
                try
                {
                    session.EnableFilter("Autorizar").SetParameter("CodigoUsuario", codigoUsuario);

                    DetachedCriteria criteria = CriarCriteria();
                    criteria.Add(Restrictions.Eq("Id", codigoTelaSiv));
                    criteria.SetResultTransformer(new DistinctRootEntityResultTransformer());

                    return ObterPrimeiro(criteria);
                }
                finally
                {
                    session.DisableFilter("Autorizar");
                }
            }
        }
    }
}