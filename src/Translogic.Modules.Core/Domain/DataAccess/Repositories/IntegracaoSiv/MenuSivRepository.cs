﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.IntegracaoSiv
{
    using ALL.Core.AcessoDados;
    using Model.Indicador.Vagao;
    using Model.IntegracaoSIV;
    using Model.IntegracaoSIV.Repositories;

    /// <summary>
    /// Repositorio para <see cref="MenuSivRepository"/>
    /// </summary>
    public class MenuSivRepository : NHRepository<MenuSiv, int?>, IMenuSivRepository
    {
    }
}