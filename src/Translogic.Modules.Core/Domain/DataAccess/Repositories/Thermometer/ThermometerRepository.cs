﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Thermometer
{
    using System;
    using System.Linq;
    using ALL.Core.AcessoDados;
    using Model.Thermometer;
    using Model.Thermometer.Repositories;
    using NHibernate.Linq;

    /// <summary>
    /// Repositório da classe Thermometer 
    /// </summary>
    public class ThermometerRepository : NHRepository<Thermometer, int?>, IThermometerRepository
    {
        /// <summary>
        /// Obtém as medições de um determinado termômetro
        /// </summary>
        /// <param name="idTermometro">Identificador do termômetro</param>
        /// <param name="dataInicial">Data Inicial de pesquisa</param>
        /// <param name="dataFinal">Data Final de pesquisa</param>
        /// <returns>Retorna as medições</returns>
        public Thermometer ObterMedicoesTermometros(int idTermometro, DateTime dataInicial, DateTime dataFinal)
        {
            using (var session = OpenSession())
            {
                ReadThermometer medicoes = null;

                Thermometer termometro = session.QueryOver<Thermometer>()
                    .Where(x => x.Id == idTermometro)
                    .Fetch(x => x.ListaMedicoes).Eager
                    .Left.JoinAlias(x => x.ListaMedicoes, () => medicoes)
                    .Where(() => medicoes.DataLeitura >= dataInicial && medicoes.DataLeitura <= dataFinal).SingleOrDefault();

                return termometro;
            }
        }
    }
}