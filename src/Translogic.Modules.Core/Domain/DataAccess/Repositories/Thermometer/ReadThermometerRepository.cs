﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Thermometer
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using ALL.Core.AcessoDados;
    using Model.Thermometer;
    using Model.Thermometer.Repositories;
    using NHibernate.Linq;

    /// <summary>
    /// Repositório da classe Thermometer 
    /// </summary>
    public class ReadThermometerRepository : NHRepository<ReadThermometer, int?>, IReadThermometerRepository
    {
        /// <summary>
        /// Obtém as medições de um determinado termômetro
        /// </summary>
        /// <param name="idTermometro">Identificador do termômetro</param>
        /// <param name="dataInicial">Data Inicial de pesquisa</param>
        /// <param name="dataFinal">Data Final de pesquisa</param>
        /// <returns>Retorna as medições</returns>
        public List<ReadThermometer> ObterMedicoesTermometros(int idTermometro, DateTime dataInicial, DateTime dataFinal)
        {
            using (var session = OpenSession())
            {
                var query = session
                    .Query<ReadThermometer>()
                    .Where(x => x.Termometro.Id == idTermometro && x.DataLeitura >= dataInicial && x.DataLeitura <= dataFinal);

                return query.ToList();
            }
        }
    }
}