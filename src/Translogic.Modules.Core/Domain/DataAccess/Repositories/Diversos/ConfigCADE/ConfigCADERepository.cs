﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Diversos.ConfigCADE
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using ALL.Core.AcessoDados;
    using Model.Diversos.ConfigCADE;
    using Model.Diversos.ConfigCADE.Repositories;
    using NHibernate;
    using NHibernate.Linq;

    public class ConfigCADERepository : NHRepository<TremTipo, int?>, IConfigCADERepository
    {
        public IList<TremTipo> ListarTrensTipo(DateTime mesReferencia, string origem, string destino, string cliente, string segmento)
        {
            /*using (var sessionTL = OpenSession(null))
            {
                
            }*/

            using (var session = OpenSession())
            {
                var ano = mesReferencia.Year;
                var mes = mesReferencia.Month;
                var query = session.Query<TremTipo>()
                    .Where(a => a.Ano == ano && a.Mes == mes
                        && (a.Origem == origem || origem == null)
                        && (a.Destino == destino || destino == null)
                        && (a.Cliente == cliente || cliente == null)
                        && (a.Segmento == segmento || segmento == null));

                return query.ToList();
            }
        }

        public IList<AjusteTUMensal> ListarAjustesTU(DateTime mesReferencia, List<string> fluxo, string cliente, string segmento)
        {
            using (var session = OpenSession())
            {
                var dtIni = mesReferencia.Date.AddDays(1 - mesReferencia.Day);
                var dtFim = dtIni.AddMonths(1);

                var query = session.Query<AjusteTUMensal>()
                    .Where(a => a.DataReferencia >= dtIni && a.DataReferencia < dtFim
                        && (a.ClienteRementente == cliente || cliente == null)
                        && (a.Segmento == segmento || segmento == null)
                        && (fluxo.Contains(a.Fluxo) || string.IsNullOrEmpty(fluxo[0])));

                return query.ToList();
            }
        }

        public void SalvarAjustes(IEnumerable<AjusteTUMensal> ajustes)
        {
            using (var session = OpenSession())
            {
                using (var trans = session.BeginTransaction())
                {
                    foreach (var ajuste in ajustes)
                    {
                        var query = session.CreateSQLQuery(@"
    UPDATE TBL_CADE_DEMANDA_MENSAL
    SET     NU_TU_CADE = :tu, NU_CGTO_CADE = :cgto
    WHERE   ID_DEMANDA = :id;");

                        query.SetInt64("id", ajuste.Id);
                        query.SetParameter("tu", ajuste.TUMesAjust);
                        query.SetParameter("cgto", ajuste.CarregamentosAjust);

                        query.ExecuteUpdate();
                    }
                    session.Flush();
                    trans.Commit();
                }
            }
        }

        public void SalvarTrensTipo(IEnumerable<TremTipo> trensTipo)
        {
            using (var session = OpenSession())
            {
                using (var trans = session.BeginTransaction())
                {
                    foreach (var tt in trensTipo)
                    {
                        session.SaveOrUpdate(tt);
                    }
                    session.Flush();
                    trans.Commit();
                }
            }
        }
    }
}