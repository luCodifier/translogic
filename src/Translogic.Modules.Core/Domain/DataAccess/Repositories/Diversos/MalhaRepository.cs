namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Diversos
{
	using ALL.Core.AcessoDados;
	using Model.Diversos;
	using Model.Diversos.Repositories;

	/// <summary>
	/// Implementação de repositório de Malha
	/// </summary>
	public class MalhaRepository : NHRepository<Malha, int?>, IMalhaRepository
	{	
	}
}