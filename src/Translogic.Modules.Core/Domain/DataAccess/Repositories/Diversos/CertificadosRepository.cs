namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Diversos
{
    using ALL.Core.AcessoDados;
    using Model.Diversos;
    using Model.Diversos.Repositories;

    /// <summary>
    /// Repositorio da tabela de envio de sms
    /// </summary>
    public class CertificadosRepository : NHRepository<Certificado, int>, ICertificadosRepository
    {   
    }
}