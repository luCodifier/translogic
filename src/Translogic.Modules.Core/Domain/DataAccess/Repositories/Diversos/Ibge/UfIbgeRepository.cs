namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Diversos.Ibge
{
	using System;
	using ALL.Core.AcessoDados;
	using Model.Diversos.Ibge;
	using Model.Diversos.Ibge.Repositories;
	using NHibernate.Criterion;

	/// <summary>
	/// Reposit�rio da  classe UfIbge
	/// </summary>
	public class UfIbgeRepository : NHRepository<UfIbge, int>, IUfIbgeRepository
	{
		/// <summary>
		/// Obt�m a uf pela sigla do estado
		/// </summary>
		/// <param name="sigla">Sigla do estado</param>
		/// <returns>Retorna a inst�ncia do objeto</returns>
		public UfIbge ObterPorSigla(string sigla)
		{
			DetachedCriteria criteria = CriarCriteria();
			criteria.Add(Restrictions.Eq("Sigla", sigla.ToUpper()));
			return ObterPrimeiro(criteria);
		}
	}
}