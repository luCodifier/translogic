namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Diversos.Ibge
{
	using System;
	using ALL.Core.AcessoDados;
	using Model.Diversos.Ibge;
	using Model.Diversos.Ibge.Repositories;
	using NHibernate.Criterion;

	/// <summary>
	/// Reposit�rio da classe cidade Ibge
	/// </summary>
	public class CidadeIbgeRepository : NHRepository<CidadeIbge, int>, ICidadeIbgeRepository
	{
		/// <summary>
		/// Obt�m a cidade por descri��o e Uf
		/// </summary>
		/// <param name="descricao">Descricao da cidade</param>
		/// <param name="uf">Uf da cidade</param>
		/// <returns>Retorna a instancia do objeto</returns>
		public CidadeIbge ObterPorDescricaoUf(string descricao, string uf)
		{
			DetachedCriteria criteria = CriarCriteria();
			criteria.Add(Restrictions.Eq("Descricao", descricao));
			criteria.Add(Restrictions.Eq("SiglaEstado", uf));
			return ObterPrimeiro(criteria);
		}

		/// <summary>
		/// Obt�m a cidade ibge pelo c�digo do municipio
		/// </summary>
		/// <param name="codigoIbge">C�digo da cidade IBGE</param>
		/// <returns>Retorna a inst�ncia do objeto</returns>
		public CidadeIbge ObterPorCodigoMunicipioIbge(int codigoIbge)
		{
			DetachedCriteria criteria = CriarCriteria();
			criteria.Add(Restrictions.Eq("CodigoIbge", codigoIbge));

			return ObterPrimeiro(criteria);
		}
	}
}