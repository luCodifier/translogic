﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Diversos
{
    using ALL.Core.AcessoDados;
    using Model.Diversos;
    using Model.Diversos.Repositories;
    using System.Collections.Generic;
    using Translogic.Modules.Core.Domain.Model.PainelExpedicao.Parametrizacao;

    using NHibernate.Transform;
    using NHibernate;

    public class PainelExpedicaoRepository : NHRepository<PainelExpedicao, int>, IPainelExpedicaoRepository
    {
        public IList<Operador> ObterOperadores()
        {
            using (ISession session = OpenSession())
            {
                var queryString = @"
                    SELECT IdOperador   AS IdOperador
                         , Simbolo      AS Simbolo
                         , Descricao    AS Descricao
                      FROM Operador";

                IQuery query = session.CreateQuery(queryString);
                query.SetResultTransformer(Transformers.AliasToBean(typeof(Operador)));
                IList<Operador> operadores = query.List<Operador>();

                return operadores;
            }
        }
    }
}