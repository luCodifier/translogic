﻿using System.Collections.Generic;
using System.Linq;
using ALL.Core.AcessoDados;
using NHibernate;
using NHibernate.Linq;
using Translogic.Modules.Core.Domain.Model.Diversos;
using Translogic.Modules.Core.Domain.Model.Diversos.Repositories;

namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Diversos
{
    /// <summary>
    ///     Implementação de repositorio de cadastro de corredores
    /// </summary>
    public class CadCorredorRepository : NHRepository<CadCorredor, int>, ICadCorredorRepository
    {
        /// <summary>
        ///     Obtem a lista enumerada de corredores
        /// </summary>
        /// <returns>Lista enumerada de Corredores</returns>
        public IEnumerable<CadCorredor> ObterCorredores()
        {
            using (ISession session = OpenSession())
            {
                return session.Query<CadCorredor>()
                    .Where(c => c.IndicadorAtivo == "S" && c.IndicadorLoco == "S" && c.Identificador == "C")
                    .OrderBy(c => c.Ordem)
                    .ToList();
            }
        }
    }
}