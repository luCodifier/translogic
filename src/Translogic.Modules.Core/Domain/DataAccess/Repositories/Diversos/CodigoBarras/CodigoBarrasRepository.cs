﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Diversos.CodigoBarras
{
    using System;
    using ALL.Core.AcessoDados;
    using Model.Diversos.CodigoBarras;
    using Model.Diversos.CodigoBarras.Repositories;
    using NHibernate.Criterion;

    /// <summary>
    ///   Repositório da  classe CodigoBarras
    /// </summary>
    public class CodigoBarrasRepository : NHRepository<CodigoBarras, int>, ICodigoBarrasRepository
    {
        /// <summary>
        /// Obtém o tipo do Código de barras
        /// </summary>
        /// <param name="valor">Valor do código de barras</param>
        /// <param name="tipo">Tipo do código de barras</param>
        /// <returns>Retorna a instância do objeto</returns>
        public CodigoBarras FindByValorAndTipo(string valor, string tipo)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.Eq("Valor", valor.ToUpper()));
            criteria.Add(Restrictions.Eq("Tipo", tipo.ToUpper()));
            return ObterPrimeiro(criteria);
        }

        /// <summary>
        /// Obtém CodigoBarras por valor
        /// </summary>
        /// <param name="indice">Indice do código de barras</param>
        /// <param name="tipo">Valor do código de barras</param>
        /// <returns>Retorna a instância do objeto</returns>
        public CodigoBarras FindByIndiceAndTipo(int indice, string tipo)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.Eq("Indice", indice));
            criteria.Add(Restrictions.Eq("Tipo", tipo.ToUpper()));
            return ObterPrimeiro(criteria);
        }
    }
}