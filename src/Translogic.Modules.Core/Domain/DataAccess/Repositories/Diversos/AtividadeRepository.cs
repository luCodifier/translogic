namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Diversos
{
	using ALL.Core.AcessoDados;
	using Model.Diversos;
	using Model.Diversos.Repositories;

	/// <summary>
	/// Implementação de repositório de Atividade
	/// </summary>
	public class AtividadeRepository : NHRepository<Atividade, int?>, IAtividadeRepository
	{	
	}
}