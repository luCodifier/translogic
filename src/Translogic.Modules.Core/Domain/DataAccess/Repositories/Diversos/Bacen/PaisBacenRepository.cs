namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Diversos.Bacen
{
	using System;
	using ALL.Core.AcessoDados;
	using Model.Diversos.Bacen;
	using Model.Diversos.Bacen.Repositories;
	using NHibernate.Criterion;

	/// <summary>
	/// Reposit�rio da classe 
	/// </summary>
	public class PaisBacenRepository : NHRepository<PaisBacen, int>, IPaisBacenRepository
	{
		/// <summary>
		/// Obt�m o pais por sigla resumida
		/// </summary>
		/// <param name="siglaResumida">Sigla resumida do pais</param>
		/// <returns>Retorna o objeto PaisBacen</returns>
		public PaisBacen ObterPorSiglaResumida(string siglaResumida)
		{
			siglaResumida = siglaResumida.ToUpper();
			DetachedCriteria criteria = CriarCriteria();
			criteria.Add(Restrictions.Eq("SiglaResumida", siglaResumida));
			return ObterPrimeiro(criteria);
		}

		/// <summary>
		/// Obtem o pais pelo c�digo do pais
		/// </summary>
		/// <param name="codigoPais">C�digo do pais Bacen</param>
		/// <returns>Retorna o objeto PaisBacen</returns>
		public PaisBacen ObterPorCodigoPais(string codigoPais)
		{
			if (string.IsNullOrEmpty(codigoPais))
			{
				return null;
			}

			codigoPais = codigoPais.Insert(0, "%");
			DetachedCriteria criteria = CriarCriteria();
			criteria.Add(Restrictions.Like("CodigoBacen", codigoPais));
			return ObterPrimeiro(criteria);
		}

		/// <summary>
		/// Obt�m o pais pela descri��o do pais Bacen
		/// </summary>
		/// <param name="descricaoPais">Descri��o do Paiz</param>
		/// <returns>Retorna o objeto PaisBacen</returns>
		public PaisBacen ObterPorDescricaoPais(string descricaoPais)
		{
			if (string.IsNullOrEmpty(descricaoPais))
			{
				return null;
			}

			descricaoPais = descricaoPais.Insert(0, "%");
			DetachedCriteria criteria = CriarCriteria();
			criteria.Add(Restrictions.Like("Nome", descricaoPais));
			return ObterPrimeiro(criteria);
		}
	}
}