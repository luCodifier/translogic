namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Diversos
{
	using System;
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;
	using Model.Diversos;
	using Model.Diversos.Repositories;
	using NHibernate.Criterion;

	/// <summary>
	/// Implementa��o de reposit�rio de causa de desempenho 
	/// </summary>
	public class DesempenhoCausaRepository : NHRepository<DesempenhoCausa, int?>, IDesempenhoCausaRepository
	{
		/// <summary>
		/// Obt�m causas de desempenho com a descri��o informada
		/// </summary>
		/// <param name="descricao"> Descri��o da causa de desempenho. </param>
		/// <returns> Lista de DesempenhoCausa </returns>
		public IList<DesempenhoCausa> ObterPorDescricao(string descricao)
		{
			DetachedCriteria criteria = CriarCriteria();
			if (!string.IsNullOrEmpty(descricao))
			{
				criteria.Add(Restrictions.InsensitiveLike("Descricao", descricao, MatchMode.Start));
			}

			criteria.AddOrder(Order.Asc("Descricao"));
			return ObterTodos(criteria);
		}
	}
}