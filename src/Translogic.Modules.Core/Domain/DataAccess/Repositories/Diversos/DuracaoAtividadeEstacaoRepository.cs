namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Diversos
{
	using System;
	using ALL.Core.AcessoDados;
	using Model.Diversos;
	using Model.Diversos.Repositories;
	using Model.Via;
	using NHibernate.Criterion;

	/// <summary>
	/// Implementa��o de reposit�rio de Dura��o Atividade
	/// </summary>
	public class DuracaoAtividadeEstacaoRepository : NHRepository<DuracaoAtividadeEstacao, int?>, IDuracaoAtividadeEstacaoRepository
	{
		/// <summary>
		/// Atividade Dura��o Esta��o
		/// </summary>
		/// <param name="atividade">Atividade para pesquisa</param>
		/// <param name="estacao">Esta��o de pesquisa</param>
		/// <returns>Dura��o de atividade</returns>
		public DuracaoAtividadeEstacao ObterPorEstacaoAtividade(Atividade atividade, IAreaOperacional estacao)
		{
			DetachedCriteria criteria = CriarCriteria()
										.CreateAlias("Atividade", "a")
										.Add(Restrictions.Eq("a.Codigo", atividade.Codigo))
										.Add(Restrictions.Eq("Estacao", estacao));

			return ObterPrimeiro(criteria);
		}
	}
}