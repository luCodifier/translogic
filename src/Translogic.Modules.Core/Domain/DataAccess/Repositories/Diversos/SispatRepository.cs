﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Diversos
{
	using System;
	using System.Collections.Generic;
	using System.Text;

	using ALL.Core.AcessoDados;

	using NHibernate;
	using NHibernate.Transform;

	using Translogic.Modules.Core.Domain.Model.Diversos.Repositories;
	using Translogic.Modules.Core.Interfaces.DadosFiscais;

	/// <summary>
	/// The sispat repository.
	/// </summary>
	public class SispatRepository : NHRepository, ISispatRepository
	{
		/// <summary>
		/// Obtém ctes descarregados por periodo.
		/// </summary>
		/// <param name="dataInicial"> Data inicial. </param>
		/// <param name="dataFinal"> Data final. </param>
		/// <returns>
		/// Lista de Ctes rodoviarios
		/// </returns>
		public IList<CteNfeRodoviarioDescarregado> ObterCtesDescarregadosPorPeriodo(DateTime dataInicial, DateTime dataFinal)
		{
			StringBuilder builder = new StringBuilder();
			using (var session = OpenSession())
			{
				builder.AppendLine(@"SELECT 
											poc.cte_rodo as ""ChaveCte"", 
											pnf.nfe_chave as ""ChaveNfe"", 
											POC.NUM_PLACA as ""Placa"",
											poc.data_hora_evento_inicial as ""DataDescarga"", 
											pnf.peso_declarado as ""PesoDeclarado"", 
											poc.peso_carregado-poc.peso_vazio  as ""PesoAferido""
									FROM PTR_OPERACAO_CAMINHAO poc
									join ptr_nota_fiscal pnf on poc.sq_caminhao = pnf.sq_caminhao
									where poc.data_hora_pes_car between :dataInicial and :dataFinal
									and poc.status='C'
									and poc.cod_terminal in ('TAG', 'TOM', 'TCS', 'TRO', 'TAC')");

				ISQLQuery query = session.CreateSQLQuery(builder.ToString());
				query.SetDateTime("dataInicial", dataInicial);
				query.SetDateTime("dataFinal", dataFinal);

				query.SetResultTransformer(Transformers.AliasToBean(typeof(CteNfeRodoviarioDescarregado)));
				return query.List<CteNfeRodoviarioDescarregado>();
			}
		}
	}
}