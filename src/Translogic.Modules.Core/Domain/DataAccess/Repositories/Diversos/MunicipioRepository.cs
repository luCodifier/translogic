namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Diversos
{
	using System;
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;
	using Model.Diversos;
	using Model.Diversos.Repositories;
	using NHibernate.Criterion;
	using NHibernate.SqlCommand;

	/// <summary>
	/// Reposit�rio da classe municipio
	/// </summary>
	public class MunicipioRepository : NHRepository<Municipio, int>, IMunicipioRepository
	{
		/// <summary>
		/// Obt�m os municipios pelo id do estado
		/// </summary>
		/// <param name="idEstado">Id do estado</param>
		/// <returns>Lista de municipios</returns>
		public IList<Municipio> ObterPorIdEstado(int idEstado)
		{
			DetachedCriteria criteria = CriarCriteria();
			criteria.CreateAlias("Estado", "est", JoinType.InnerJoin);
			criteria.Add(Restrictions.Eq("est.Id", idEstado));
			criteria.AddOrder(new Order("Descricao", true));
			return ObterTodos(criteria);
		}
	}
}