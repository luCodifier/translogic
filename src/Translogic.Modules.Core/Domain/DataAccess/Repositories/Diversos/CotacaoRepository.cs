namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Diversos
{
	using System;
	using ALL.Core.AcessoDados;
	using Model.Diversos;
	using Model.Diversos.Repositories;
	using NHibernate.Criterion;

	/// <summary>
	/// Repositorio da tabela de Cotacao
	/// </summary>
	public class CotacaoRepository : NHRepository<Cotacao, int>, ICotacaoRepository
	{
		/// <summary>
		/// Obt�m a cota��o pela data de referencia. Obt�m a cota��o pelo Max da dataReferencia - 1
		/// </summary>
		/// <param name="dataReferencia">Data de referencia da cota��o</param>
		/// <param name="moeda">Sigla da moeda para encontrar a cota��o</param>
		/// <returns>Retorna a cota��o</returns>
		public Cotacao ObterCotacaoPorData(DateTime dataReferencia, string moeda)
		{
			DateTime dataAux = new DateTime(dataReferencia.Year, dataReferencia.Month, dataReferencia.Day);
			DetachedCriteria criteria = CriarCriteria();
			criteria.Add(Restrictions.Lt("Data", dataAux));
			criteria.Add(Restrictions.Eq("Moeda", moeda));
			criteria.AddOrder(new Order("Data", false));

			return ObterPrimeiro(criteria);
		}
	}
}