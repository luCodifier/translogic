namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Diversos
{
    using System.Collections.Generic;
    using System.Text;
    using ALL.Core.AcessoDados;
    using Model.Diversos;
    using Model.Diversos.Repositories;
    using NHibernate;
    using NHibernate.Transform;

    /// <summary>
	/// Implementação de repositório de Atividade
	/// </summary>
    public class DeParaOperacaoRepository : NHRepository<DeParaOperacao,int>, IDeParaOperacaoRepository
	{
        /// <summary>
        /// Retorna valor do objeto
        /// </summary>
        /// <param name="tipo">valor de entrada</param>
        /// <param name="valor">tipo do valor de entrada</param>
        /// <returns>retorna valor de saida</returns>
        public string ObterString(string tipo,string valor)
        {
            var builder = new StringBuilder();
            using (var session = OpenSession())
            {
                builder.AppendLine(@"SELECT V.PARA FROM DE_PARA_OPERACAO V WHERE V.TIPO :tipo AND V.DE = :valor");

                ISQLQuery query = session.CreateSQLQuery(builder.ToString());
                query.SetString("valor", valor);
                query.SetString("tipo", tipo);
               
                return (string)query.UniqueResult();
            }
        }

        /// <summary>
        /// Obtem todos 
        /// </summary>
        /// <returns>retorna lista de registros</returns>
        public IList<DeParaOperacao> ObterTodosRegistros()
        {
            ISession session = OpenSession();
            IQuery query = session.CreateQuery("SELECT V.DE AS De, V.PARA as Para, V.TIPO as Tipo FROM DE_PARA_OPERACAO V");
            query.SetResultTransformer(Transformers.AliasToBean(typeof(DeParaOperacao)));

            IList<DeParaOperacao> items = query.List<DeParaOperacao>();
            return items;
        }
	}
}