namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Diversos
{
    using System;
    using System.Data;
    using System.Data.OracleClient;
    using ALL.Core.AcessoDados;
    using Model.Diversos;
    using Model.Diversos.Repositories;
    using NHibernate;

	/// <summary>
    /// Reposit�rio da classe RelatorioAmaggiRepository
	/// </summary>
    public class RelatorioAmaggiRepository : NHRepository<Estado, int>, IRelatorioAmaggiRepository
	{
        /// <summary>
        /// gera o relatorio da amaggi
        /// </summary>
        /// <param name="dataInicial">data inicial da filtyragem</param>
        /// <param name="dataFinal">data final da filtragem</param>
        /// <param name="estacoes">esta��es separadas por ;</param>
        /// <param name="emails">e-mails separados por ;</param>
        public void Gerar(DateTime dataInicial, DateTime dataFinal, string estacoes, string emails)
        {
            using (ISession session = OpenSession())
            {
                var command = new OracleCommand
                {
                    CommandType = CommandType.StoredProcedure,
                    CommandText = "PRC_GERA_ARQUIVO_AMAGGI_TELA",
                    Connection = (OracleConnection)session.Connection
                };

                AddParameter(command, "DATA_INI", OracleType.DateTime, dataInicial, ParameterDirection.Input);
                AddParameter(command, "DATA_FIM", OracleType.DateTime, dataFinal, ParameterDirection.Input);
                AddParameter(command, "LOCAL", OracleType.VarChar, estacoes, ParameterDirection.Input);
                AddParameter(command, "EMAIL", OracleType.VarChar, emails, ParameterDirection.Input);

                session.Transaction.Enlist(command);

                command.Prepare();
                command.ExecuteNonQuery();
            }
        }

        private void AddParameter(OracleCommand command, string nome, OracleType tipo, object valor, ParameterDirection direcao)
        {
            OracleParameter parameter = new OracleParameter();
            parameter.ParameterName = nome;
            parameter.OracleType = tipo;
            parameter.Direction = direcao;
            parameter.Value = DBNull.Value;
            if (valor != null)
            {
                parameter.Value = valor;
            }

            command.Parameters.Add(parameter);
        }
	}
}