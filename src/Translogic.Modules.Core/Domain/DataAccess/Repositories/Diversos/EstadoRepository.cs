namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Diversos
{
	using ALL.Core.AcessoDados;
	using Model.Diversos;
	using Model.Diversos.Repositories;

	/// <summary>
	/// Repositório da classe estado
	/// </summary>
	public class EstadoRepository : NHRepository<Estado, int>, IEstadoRepository
	{
	}
}