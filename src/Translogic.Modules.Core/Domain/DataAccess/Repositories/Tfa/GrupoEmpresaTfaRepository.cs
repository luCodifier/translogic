﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ALL.Core.AcessoDados;
using Translogic.Modules.Core.Domain.Model.Tfa;
using Translogic.Modules.Core.Domain.Model.Tfa.Repositories;
using NHibernate.Criterion;
using ALL.Core.Dominio;
using Translogic.Core.Infrastructure.Web;
using NHibernate;
using System.Text;

namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Tfa
{
    /// <summary>
    /// Classe de Repositório de GrupoEmpresaTfa
    /// </summary>
    public class GrupoEmpresaTfaRepository : NHRepository<GrupoEmpresaTfa, int?>, IGrupoEmpresaTfaRepository
    {
        /// <summary>
        /// Obtem o grupo de empresa pelo nome
        /// </summary>
        /// <param name="nomeGrupo">Nome do Grupo</param>
        /// <returns></returns>
        public GrupoEmpresaTfa ObterPorNome(string nomeGrupo)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.Eq("NomeGrupo", nomeGrupo));

            return ObterPrimeiro(criteria);

        }

        /// <summary>
        /// Retorna resultado paginado de Grupos de Empresas, de acordo com o nome passado como parâmetro (LIKE%)
        /// </summary>
        /// <param name="paginacao">Dados para paginação em tela</param>
        /// <param name="nomeGrupo">Nome do Grupo de Empresas</param>
        /// <returns>Resultado Paginado de GrupoEmpresaTfa</returns>
        public ResultadoPaginado<GrupoEmpresaTfa> ObterPaginadoPorNomeGrupo(DetalhesPaginacaoWeb paginacao, string nomeGrupo)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.InsensitiveLike("NomeGrupo", string.Format("%{0}%",nomeGrupo)));
            criteria.AddOrder(Order.Asc("NomeGrupo"));

            return ObterPaginado(criteria, paginacao);
        }


        /// <summary>
        /// Verifica se existe(m) Empresa(s) vinculada(s) ao ID do Grupo de Empresas passado como parâmetro
        /// </summary>
        /// <param name="idGrupoEmpresa">ID do Grupo de Empresas</param>
        /// <returns>retorna true caso exista(m) empresa(s) vinculada(s) ao grupo de Empresas</returns>
        public bool ExisteEmpresaVinculada(int idGrupoEmpresa)
        {
            using (var session = OpenSession())
            {
                var grupoEmpresa = session.Get(typeof(GrupoEmpresaTfa), idGrupoEmpresa) as GrupoEmpresaTfa;
    
                NHibernateUtil.Initialize(grupoEmpresa.Empresas);

                return grupoEmpresa.Empresas.Count() > 0;
            }
        }

        /// <summary>
        /// Verifica se existe(m) usuário(s) vinculado(s) ao ID do Grupo de Empresas passado como parâmetro
        /// </summary>
        /// <param name="idGrupoEmpresa">ID do Grupo de Empresas</param>
        /// <returns>retorna true caso exista(m) usuário(s) vinculado(s) ao grupo de Empresas</returns>
        public bool ExisteUsuarioCAALVinculado(int idGrupoEmpresa)
        {
            var sql = new StringBuilder(@"SELECT COUNT(ID_GRUPO_EMPRESA) 
                                        FROM TRANSLOGIC.GRUPO_EMPRESA_DBP4_SENHAS
                                        WHERE ID_GRUPO_EMPRESA = {0}");

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(string.Format(sql.ToString(), idGrupoEmpresa));
                return query.UniqueResult<decimal>() > 0 ;

            }

        }
    }
}