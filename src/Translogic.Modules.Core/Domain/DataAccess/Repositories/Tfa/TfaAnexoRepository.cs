﻿namespace Translogic.Modules.Core.Domain.Model.Tfa
{
    using Translogic.Modules.Core.Domain.Model.Tfa.Repositories;
    using System.Text;
    using ALL.Core.AcessoDados;
    using NHibernate.Criterion;
    using System;

    public class TfaAnexoRepository : NHRepository<TfaAnexo, int?>, ITfaAnexoRepository
    {
        public TfaAnexo ObterPorNumProcesso(string numProcesso)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.Eq("NumProcesso", Convert.ToDecimal(numProcesso)));

            return ObterPrimeiro(criteria);
        }
    }
}