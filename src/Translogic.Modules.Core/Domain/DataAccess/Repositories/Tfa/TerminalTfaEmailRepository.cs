﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using ALL.Core.AcessoDados;
using Translogic.Modules.Core.Domain.Model.Tfa.Repositories;
using Translogic.Modules.Core.Domain.Model.Tfa;
using ALL.Core.Dominio;
using NHibernate;
using Translogic.Core.Infrastructure.Web;
using NHibernate.Criterion;
using NHibernate.Transform;
using Translogic.Modules.Core.Domain.Model.Dto;

namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Tfa
{
    /// <summary>
    /// 
    /// </summary>
    public class TerminalTfaEmailRepository : NHRepository<TerminalTfaEmail, int>, ITerminalTfaEmailRepository
    {
        /// <summary>
        /// Obtem a lista de Emails vinculados ao terminal, de acordo com o ID passado como parâmetro
        /// </summary>
        /// <param name="paginacao">objeto com os parâmetros de paginação</param>
        /// <param name="idTerminal">Id do Terminal</param>
        /// <returns>Retorna lista paginada de Emails</returns>
        public ResultadoPaginado<TerminalTfaEmail> ObterTodosPorIdTerminal(DetalhesPaginacaoWeb paginacao, int idTerminal)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.CreateAlias("Terminal", "t");
            criteria.Add(Restrictions.Eq("t.IdTerminal", idTerminal));

            return ObterPaginado(criteria, paginacao);
        }


        /// <summary>
        /// Obtem a lista de Emails vinculados ao terminal, de acordo com o ID passado como parâmetro
        /// </summary>
        /// <param name="idTerminal">Id do Terminal</param>
        /// <returns>Retorna lista de Emails</returns>
        public IList<TerminalTfaEmail> ObterTodosPorIdTerminal(int idTerminal)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.CreateAlias("Terminal", "t");
            criteria.Add(Restrictions.Eq("t.IdTerminal", idTerminal));

            return ObterTodos(criteria);
        }

        /// <summary>
        /// Obtem a lista de Emails vinculados ao terminal, de acordo com o ID passado como parâmetro retorando DTO
        /// </summary>
        /// <param name="idTerminal">Id do Terminal</param>
        /// <returns>Retorna lista de Emails</returns>
        public IList<TerminalTfaEmailDto> ObterTodosPorIdTerminalDto(int idTerminal)
        {
            var sql = new StringBuilder();
            sql.AppendFormat(@"SELECT 
                            TFE.ID_TERMINAL_TFA_EMAIL AS ""IdTerminalEmail""
                            ,TT.ID_TERMINAL_TFA AS ""IdTerminal""
                            ,TFE.EMAIL AS ""Email""
                            FROM AREA_OPERACIONAL AOP
                            INNER JOIN TERMINAL_TFA TT ON AOP.AO_ID_AO = TT.AO_ID_AO
                            INNER JOIN TERMINAL_TFA_EMAIL TFE  ON TT.ID_TERMINAL_TFA = TFE.ID_TERMINAL_TFA
                            WHERE AOP.AO_ID_AO = {0}
                            AND TFE.EMAIL 
                            NOT IN(select COD_USUARIO from sispat.senhas where ID_ACESSO = 'B')", idTerminal);

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());

                query.SetResultTransformer(Transformers.AliasToBean<TerminalTfaEmailDto>());
                return query.List<TerminalTfaEmailDto>();
            }
        }

        /// <summary>
        /// Obtem o email do terminal pelo IdTerminal e email
        /// </summary>
        /// <param name="idTerminal">Id do Terminal</param>
        /// <param name="email">e-mail</param>
        /// <returns>retorna o email dop terminal pelo IdTerminal e email</returns>
        public TerminalTfaEmail ObterPoridTerminalEmail(int idTerminal, string email)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.CreateAlias("Terminal", "t");
            criteria.Add(Restrictions.Eq("t.IdTerminal", idTerminal));
            criteria.Add(Restrictions.Eq("Email", email));

            return ObterPrimeiro(criteria);
        }
    }
}