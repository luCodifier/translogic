﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ALL.Core.AcessoDados;
using Translogic.Modules.Core.Domain.Model.Tfa.Repositories;
using Translogic.Modules.Core.Domain.Model.Tfa;
using ALL.Core.Dominio;
using Translogic.Core.Infrastructure.Web;
using NHibernate.Criterion;
using System.Text;

namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Tfa
{
    public class EmpresaGrupoTfaRepository : NHRepository<EmpresaGrupoTfa, int?>, IEmpresaGrupoTfaRepository
    {
        /// <summary>
        /// Retorna todas as empresas do Grupo de Empresas passado como parâmetro
        /// </summary>
        /// <param name="paginacao">Dados para paginação em tela</param>
        /// <param name="idGrupoEmpresa">ID do Grupo de Empresas</param>
        /// <returns>Resultado Paginado de EmpresaGrupoTfa</returns>
        public ResultadoPaginado<EmpresaGrupoTfa> ObterTodosPorIdGrupoEmpresa(DetalhesPaginacaoWeb paginacao, int idGrupoEmpresa)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.CreateAlias("GrupoEmpresa", "g");
            criteria.CreateAlias("Empresa", "e");
            criteria.Add(Restrictions.Eq("g.id", idGrupoEmpresa));
            criteria.AddOrder(Order.Asc("e.Cgc"));

            return ObterPaginado(criteria, paginacao);
        }

        /// <summary>
        /// Recupera uma EmpresaGrupoTfa pelo número do CNPJ 
        /// </summary>
        /// <param name="cnpj">CNPJ da Empresa</param>
        /// <returns></returns>
        public EmpresaGrupoTfa ObterPorCnpj(string cnpj)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.CreateAlias("Empresa", "e");
            criteria.Add(Restrictions.Eq("e.Cgc", cnpj));

            return ObterPrimeiro(criteria);
        }

        /// <summary>
        /// Retorna a lista de e-mails cadastrados para a empresa do Processo de Seguro
        /// </summary>
        /// <param name="numeroProcesso">Número do Processo</param>
        /// <returns>Retorna a lista de e-mails cadastrados para a empresa do Processo de Seguro</returns>
        public List<string> ObterListaEmailTfa(int numeroProcesso)
        {
            #region SQL Query
            var sql = new StringBuilder();
            sql.AppendFormat(@"SELECT DISTINCT GEBS.COD_USUARIO
                            FROM    
                                TRANSLOGIC.MODAL_SEGURO MO 
                                INNER JOIN TRANSLOGIC.DESPACHO DP ON ( DP.DP_NUM_DSP = MO.NRO_DESP )
                                INNER JOIN TRANSLOGIC.SERIE_DESPACHO SDP ON(DP.SK_IDT_SRD = SDP.SK_IDT_SRD AND SDP.SK_NUM_SRD = MO.SERIE_DESP)
                                INNER JOIN TRANSLOGIC.CTE CT ON ( CT.DP_ID_DP = DP.DP_ID_DP )
                                INNER JOIN TRANSLOGIC.CTE_EMPRESAS CE ON ( CE.ID_CTE = CT.ID_CTE )    
                                INNER JOIN TRANSLOGIC.GRUPO_EMPRESA_EMPRESA GEE ON(GEE.EP_ID_EMP = CE.EP_ID_EMP_TOMA)
                                INNER JOIN GRUPO_EMPRESA_DBP4_SENHAS GEBS ON (GEE.ID_GRUPO_EMPRESA = GEBS.ID_GRUPO_EMPRESA)
                            WHERE    
                                MO.PC_ID_PC =  {0}
                                AND GEBS.COD_USUARIO NOT IN(select COD_USUARIO from sispat.senhas WHERE ID_ACESSO = 'B')", numeroProcesso);
                                        
            #endregion

            #region conectabase

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());

                var result = query.List<string>();

                //Verifica se SELECT retornou valor
                return result.ToList<string>();
            }

            #endregion
        }
    }
}