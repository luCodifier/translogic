﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Translogic.Modules.Core.Domain.Model.Tfa.Repositories;
using Translogic.Modules.Core.Domain.Model.Tfa;
using ALL.Core.AcessoDados;
using NHibernate.Criterion;
using NHibernate.Linq;
using Translogic.Modules.Core.Domain.Model.Estrutura;
using System.Text;
using Translogic.Modules.Core.Domain.Model.Dto;
using NHibernate;
using NHibernate.Transform;

namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Tfa
{
    public class TerminalTfaRepository : NHRepository<TerminalTfa, int?>, ITerminalTfaRepository
    {
       
        /// <summary>
        /// Obtem a lista de DTOs de Terminais
        /// </summary>
        /// <returns>Retorna lista de DTOs de Terminais</returns>
        public IList<TerminalDto> ObterListaTerminalDto()
        {
            var sql = new StringBuilder(@"SELECT
                                        AO.AO_ID_AO AS ""IdTerminal"",
                                        AOM.AO_COD_AOP
                                        || ' - '
                                        || AO.AO_DSC_AOP AS ""DescricaoTerminal""
                                        FROM
                                            AREA_OPERACIONAL AO,
                                            AREA_OPERACIONAL AOM
                                        WHERE
                                            AO.AO_ID_AO_INF = AOM.AO_ID_AO
                                            AND AO.AO_TRM_CLI = 'S'
                                        ORDER BY
                                            AOM.AO_COD_AOP, AO.AO_DSC_AOP");

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());

                query.AddScalar("IdTerminal", NHibernateUtil.Int32);
                query.AddScalar("DescricaoTerminal", NHibernateUtil.String);

                query.SetResultTransformer(Transformers.AliasToBean<TerminalDto>());
                return query.List<TerminalDto>();

            }
        }


        /// <summary>
        /// Obtem o TerminalTfa pelo ID do Terminal  informado
        /// </summary>
        /// <param name="idTerminal">Id do Terminal</param>
        /// <returns></returns>
        public TerminalTfa ObterPorIdTerminal(int idTerminal)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.Eq("IdTerminal", idTerminal));

            return ObterPrimeiro(criteria);
        }
       
    }
}