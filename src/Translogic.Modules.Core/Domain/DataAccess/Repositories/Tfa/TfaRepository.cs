﻿using System;
using System.Collections.Generic;
using Translogic.Modules.Core.Domain.Model.Tfa.Repositories;
using NHibernate;
using NHibernate.Transform;
using System.Text;
using Translogic.Modules.Core.Domain.Model.Dto;
using ALL.Core.AcessoDados;
using ALL.Core.Dominio;
using ALL.Core.Extensions;
using Translogic.Modules.Core.Domain.Model.Tfa;
using Translogic.Core.Infrastructure.Web;
using Translogic.Modules.Core.Domain.Model.Estrutura;
using Translogic.Modules.Core.Domain.Model.Diversos.Repositories;
using Translogic.Modules.Core.Domain.Model.Administracao;
using Translogic.Modules.Core.Domain.Model.Seguro.Repositories;
using NHibernate.Criterion;

namespace Translogic.Modules.Core.Domain.Model.Tfa
{
    public class TfaRepository : NHRepository<Tfa, int>, ITfaRepository
    {
        
        private readonly IProcessoSeguroRepository _processoSeguroRepository;
        private readonly IModalSeguroRepository _modalSeguroRepository;

        public TfaRepository(IProcessoSeguroRepository processoSeguroRepository, IModalSeguroRepository modalSeguroRepository)
        {            
            _processoSeguroRepository = processoSeguroRepository;
            _modalSeguroRepository = modalSeguroRepository;
        }

        public ResultadoPaginado<TfaConsultaDto> ObterConsulta(DetalhesPaginacaoWeb detalhesPaginacaoWeb,
                                                                                    DateTime? dtInicioProcesso, DateTime? dtFinalProcesso, string vagao,
                                                                                    string terminal, string un, string up, string malha,
                                                                                    string provisionado, DateTime? dtInicioSinistros, DateTime? dtFinalSinistros,
                                                                                    string numProcesso, string os, string situacao, string causa,
                                                                                    string numTermo, string sindicancia, string anexado)
        {
            #region Consulta

            var sql = new StringBuilder(@"SELECT DISTINCT
                        PC.PC_ID_PC                                                                                                                                             AS ""IdProcesso"",
                        (SELECT distinct
                               FIRST_VALUE(MO_NUM_FALTA) 
                                 OVER (ORDER BY MO.MO_ID_MO DESC) AS NumTermo
                        FROM   MODAL_SEGURO MO
                        WHERE (PC.PC_ID_PC = MO.PC_ID_PC))                                                                                                                      AS ""NumTermo"",
                        PC.PC_NUM_PROC                                                                                                                                          AS ""NumProcesso"",
                        PC.PC_DT_PROC                                                                                                                                           AS ""DataProcesso"",
                        DECODE(PC.PC_IND_SIT, 'P', 'PENDENTE','U','PENDENTE','G', 'PENDENTE','A','ENCERRADO SEM PAGAMENTO', 'O','PAGO','E', 'ENCERRADO SEM PAGAMENTO', 'R', 'PREPARADO P/ PAGAMENTO', 'C', 'ENCONTRO DE CONTAS') AS ""Situacao"",
                        JOIN(CURSOR(SELECT UD.UD_DSC_UNID FROM UNIDADE_SEGURO UD, RATEIO_SEGURO  RT WHERE  RT.UD_ID_UD = UD.UD_ID_UD AND    RT.PC_ID_PC = PC.PC_ID_PC),'/')     AS ""UnidEnvolvida"",             
                        (SELECT CC.CC_DS_CTB || '-' || CC.CC_CT_CTB FROM CONTA_CONTABIL_SEGURO CC WHERE CC.CC_ID_CC = PC.CC_ID_CC)                                              AS ""ContaContabil"",                                                                   
                        (SELECT ASS1.AS_DT_ANALISE FROM ANALISE_SEGURO ASS1 WHERE ASS1.PC_ID_PC = PC.PC_ID_PC AND ASS1.AS_TIMESTAMP = 
                        (SELECT MAX(ASS.AS_TIMESTAMP) FROM ANALISE_SEGURO ASS WHERE ASS.PC_ID_PC = PC.PC_ID_PC) AND ASS1.AS_DT_ANALISE =                                                           
                        (SELECT MAX(ASS.AS_DT_ANALISE) FROM ANALISE_SEGURO ASS WHERE ASS.PC_ID_PC = PC.PC_ID_PC) AND ASS1.AS_ID_AS =                                                                
                        (SELECT MAX(ASS.AS_ID_AS) FROM ANALISE_SEGURO ASS WHERE ASS.PC_ID_PC = PC.PC_ID_PC))                                                                    AS ""DataAnalise"",                                                                     
                        PC.PC_DT_PAGAMENTO                                                                                                                                      AS ""DataPagamento"",                                                  
                        PC.PC_DT_ENCERRADO                                                                                                                                      AS ""DataEncerrado"",                                                  
                        PC.PC_OS                                                                                                                                                AS ""OrdemServico"",                                                             
                        PC.DT_PROVISIONAMENTO                                                                                                                                   AS ""DataProvisao"",                                         
                        (select up.up_ind_malha from area_operacional ao, unid_producao up where pc.ao_id_org = ao.ao_id_ao and   ao.up_id_unp = up.up_id_unp)                  AS ""Malha"",
                        CASE WHEN AT.NUMPROCESSO IS NULL then 1 ELSE 0 END                                                                                                      AS ""TfaAnexo"",
                        DECODE( TFA.STATUS_CLIENTE_CIENTE, 'S', 'SIM', 'N', 'N&Atilde;O', null, '')                                                                             AS ""StatusCiente"",
                        CASE WHEN AT.NUMPROCESSO IS NULL then 1 ELSE DECODE( TFA.ORIGEM_PROCESSO_APP, 'S', 1, 'N', 0, null, 0) END                                              AS ""PermiteExcluir""
                                  FROM PROCESSO_SEGURO PC
                                  INNER JOIN MODAL_SEGURO   MO ON (PC.PC_ID_PC = MO.PC_ID_PC)                                                               
                                  INNER JOIN CAUSA_SEGURO   CS ON (MO.CS_ID_CS = CS.CS_ID_CS)                                                               
                                  INNER JOIN RATEIO_SEGURO  RS ON (PC.PC_ID_PC = RS.PC_ID_PC)                                                               
                                  INNER JOIN UNIDADE_SEGURO UD ON (RS.UD_ID_UD = UD.UD_ID_UD)    
                                  LEFT JOIN ANEXO_TFA AT ON (AT.NUMPROCESSO = PC.PC_NUM_PROC) 
                                  LEFT JOIN TFA TFA ON (TFA.PC_ID_PC = PC.PC_ID_PC)  WHERE 1=1");

            DefinirFiltros(sql, dtInicioProcesso, dtFinalProcesso, vagao, terminal, un, up, malha, provisionado, dtInicioSinistros, dtFinalSinistros, numProcesso, os, situacao, causa, numTermo, sindicancia, anexado);

            sql.Append(" ORDER BY PC.PC_DT_PROC, PC.PC_ID_PC");


            #endregion

            #region Acesso a base
            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());
                var queryCount = session.CreateSQLQuery(String.Format(@"SELECT COUNT(1) FROM ({0})", sql.ToString().Replace("{ORDENACAO}", String.Empty)));

                query.SetResultTransformer(Transformers.AliasToBean<TfaConsultaDto>());

                if (detalhesPaginacaoWeb.Limite != 0)
                {
                    query.SetFirstResult(detalhesPaginacaoWeb.Inicio ?? 0);
                    query.SetMaxResults(detalhesPaginacaoWeb.Limite ?? DetalhesPaginacao.LimitePadrao);
                }

                var total = queryCount.UniqueResult<decimal>();
                var itens = query.List<TfaConsultaDto>();

                var result = new ResultadoPaginado<TfaConsultaDto>
                {
                    Items = itens,
                    Total = (long)total
                };

                return result;
            }
            #endregion
        }

        public IEnumerable<TfaExportaDto> ObterConsultaTFAExportar(DateTime? dtInicioProcesso, DateTime? dtFinalProcesso, string vagao,
                                                                                    string terminal, string un, string up, string malha,
                                                                                    string provisionado, DateTime? dtInicioSinistros, DateTime? dtFinalSinistros,
                                                                                    string numProcesso, string os, string situacao, string causa,
                                                                                    string numTermo, string sindicancia, string anexado)
        {
            var parameters = new List<Action<IQuery>>();

            #region Consulta Antiga

//            var sql = new StringBuilder(@" SELECT   
//             MO.NRO_DESP                                                                                                                                                                                                                                                                                           AS ""Despacho"",
//             MO.SERIE_DESP                                                                                                                                                                                                                                                                                         AS ""SerieDespacho"",  
//             PC.PC_OS                                                                                                                                                                                                                                                                                              AS ""OS"", 
//             PC.PC_NUM_PROC                                                                                                                                                                                                                                                                                        AS ""NumProcesso"",
//             MO.MO_NUM_FALTA                                                                                                                                                                                                                                                                                       AS ""NumTermo"",
//             DECODE(PC.PC_IND_SIT, 'P', 'PENDENTE','U','PENDENTE','G', 'PENDENTE','A','ENCERRADO SEM PAGAMENTO', 'O','PAGO','E', 'ENCERRADO SEM PAGAMENTO', 'R', 'PREPARADO P/ PAGAMENTO')                                                                                                                                                                                      AS ""Status"",
//             (SELECT VG.VG_COD_VAG FROM VAGAO VG WHERE VG.VG_ID_VG = MO.VG_ID_VG)                                                                                                                                                                                                                                  AS ""Vagao"",
//             (SELECT SV.SV_COD_SER FROM VAGAO VG, SERIE_VAGAO SV WHERE VG.VG_ID_VG = MO.VG_ID_VG AND VG.SV_ID_SV = SV.SV_ID_SV)                                                                                                                                                                                    AS ""SerieVagao"",
//             REPLACE(MO.MO_NOTA_FISCAL, ';', ',')                                                                                                                                                                                                                                                                                     AS ""NF"",
//             MO.FX_COD_FLX                                                                                                                                                                                                                                                                                         AS ""Fluxo"",
//             (SELECT UN.UN_COD_UNG FROM   FLUXO_COMERCIAL FC, UNID_NEGOCIO UN WHERE FC.UN_ID_UN = UN.UN_ID_UN AND FC.FX_COD_FLX = MO.FX_COD_FLX)                                                                                                                                                                   AS ""UN"",
//             (SELECT DECODE(UP.UP_IND_MALHA, 'LG', 'LARGA','MS', 'SUL','MN','NORTE' ) FROM AREA_OPERACIONAL AO, UNID_PRODUCAO UP WHERE AO.UP_ID_UNP = UP.UP_ID_UNP AND AO.AO_ID_AO = MO.AO_ID_ORG)                                                                                                                                                                  AS ""Malha"",
//             (SELECT DECODE(AO.TB_COD_BITOLA,'BL', 'LARGA', 'BM', 'MISTA', 'BE', 'ESTREITA') FROM AREA_OPERACIONAL AO WHERE AO.AO_ID_AO = MO.AO_ID_ORG)                                                                                                                                                                                                                   AS ""Bitola"",
//             (SELECT AO.AO_COD_AOP FROM   AREA_OPERACIONAL AO WHERE  AO.AO_ID_AO = MO.AO_ID_ORG)                                                                                                                                                                                                                   AS ""EstacaoOrigem"",
//             (SELECT AO.AO_COD_AOP FROM   AREA_OPERACIONAL AO WHERE  AO.AO_ID_AO = MO.AO_ID_DST)                                                                                                                                                                                                                   AS ""EstacaoDestino"", 
//             CS.CS_DSC_CAUSA                                                                                                                                                                                                                                                                                       AS ""Causa"",
//             (SELECT CL.CL_COD_SAP FROM CLIENTE_SEGURO CL WHERE CL.CL_ID_CL = PC.CL_ID_CL)                                                                                                                                                                                                                         AS ""CodCliente"",
//             (SELECT CL.CL_DSC_CLS FROM CLIENTE_SEGURO CL WHERE CL.CL_ID_CL = PC.CL_ID_CL)                                                                                                                                                                                                                         AS ""Cliente"",
//             MO.MC_COD_MRC                                                                                                                                                                                                                                                                                         AS ""CodMercadoria"",
//             (SELECT MC.MC_DDT_PT FROM MERCADORIA MC WHERE MC.MC_COD_MRC = MO.MC_COD_MRC)                                                                                                                                                                                                                          AS ""Mercadoria"",
//             PC.PC_DT_SINISTRO                                                                                                                                                                                                                                                                                     AS ""DtSinistro"", 
//             PC.PC_DT_VISTORIA                                                                                                                                                                                                                                                                                     AS ""DtVistoria"", 
//             PC.PC_DT_PROC                                                                                                                                                                                                                                                                                         AS ""DtProcesso"",
//             PC.PC_DT_PAGAMENTO                                                                                                                                                                                                                                                                                    AS ""DtPgtoCliente"",
//             PC.DT_PROVISIONAMENTO                                                                                                                                                                                                                                                                                 AS ""DtProvisionamento"",
//             UD.UD_DSC_UNID                                                                                                                                                                                                                                                                                        AS ""Unidade"",
//             RS.RS_PERC_RATEIO                                                                                                                                                                                                                                                                                     AS ""Percentual"",
//             (SELECT sum(DC.DC_QTD_INF) FROM   DESPACHO DP, SERIE_DESPACHO SD, DETALHE_CARREGAMENTO DC, ITEM_DESPACHO IDS WHERE  DP.DP_NUM_DSP = MO.NRO_DESP AND    DP.SK_IDT_SRD = SD.SK_IDT_SRD AND    SD.SK_NUM_SRD = MO.SERIE_DESP AND    DP.DP_ID_DP   = IDS.DP_ID_DP AND    IDS.DC_ID_CRG = DC.DC_ID_CRG)    AS ""PesoOrigem"",
//             MO.MO_QTD_PERDA                                                                                                                                                                                                                                                                                       AS ""PerdaTonVagao"",
//             (SELECT TL.TL_TOLERANCIA FROM TOLERANCIA_SEGURO TL, PROCESSO_SEGURO PC1 WHERE TL.CL_ID_CL = PC1.CL_ID_CL AND PC1.PC_ID_PC = PC.PC_ID_PC AND ((PC1.PC_DT_PROC BETWEEN TL.TL_DT_INICIO AND NVL(TL.TL_DT_FIM, SYSDATE))))                                                                                AS ""Tolerancia"",
//             MO.CONSID_TOLERANCIA                                                                                                                                                                                                                                                                                  AS ""ConsiderarTolerancia"",
//             MO.MO_VLR_MERC                                                                                                                                                                                                                                                                                        AS ""ValorMercadoriaUnitario"",
//             --(SELECT CH.TARIFATOTAL FROM CONTRATO_HIST CH, CTE C, DESPACHO DP, SERIE_DESPACHO SD WHERE  CH.CZ_ID_CTT_HIST=C.CZ_ID_CTT_HIST AND    C.DP_ID_DP = DP.DP_ID_DP AND    DP.DP_NUM_DSP=MO.NRO_DESP AND    DP.SK_IDT_SRD=SD.SK_IDT_SRD AND    SD.SK_NUM_SRD=MO.SERIE_DESP AND ROWNUM = 1 )                 AS ""TarifaFrete"",
//             MO.PISCOFINS                                                                                                                                                                                                                                                                                          AS ""PisCofins"",
//             MO.ICMS                                                                                                                                                                                                                                                                                               AS ""ICMS"",
//             MO.DESCONTO                                                                                                                                                                                                                                                                                           AS ""Desconto"",
//             MO.MOTIVO                                                                                                                                                                                                                                                                                             AS ""Motivo"",
//             (MO.MO_VLR_MERC * MO.MO_QTD_PERDA)                                                                                                                                                                                                                      AS ""TotalVagao"",
//             ((SELECT FM.FM_VLR_FRANQUIA FROM FRANQUIA_MERCADORIA FM WHERE FM.FM_IND_ATIVO = 'A' AND FM.MC_COD_MERC = MO.MC_COD_MRC) * RS.RS_PERC_RATEIO / 100)                                                                                                                                                    AS ""Franquia"",
//             DECODE(CS.CS_DSC_CAUSA, 'FURTO', 'FURTO', 'FECHAMENTO DE LOTE','FECHAMENTO DE LOTE','AVARIA')                                                                                                                                                                                                         AS ""GrupoCausa"",
//             TO_CHAR(PC.PC_DT_CADASTRO, 'MM')                                                                                                                                                                                                                                                                      AS ""MesSinistro"",
//             NVL(PC.PC_IND_SEGURADO, 'N')                                                                                                                                                                                                                                                                          AS ""Seguro"", 
//             (SELECT SS.SS_REGULADORA FROM SALVADO_SEGURO SS JOIN MODAL_SEGURO MO ON SS.MC_COD_MRC = MO.MC_COD_MRC WHERE SS.PC_ID_PC = PC.PC_ID_PC AND ROWNUM = 1)                                                                                                                                                 AS ""Reguladora"",
//             (SELECT MAX(SG.SG_DT_RECEB) FROM   SEGUROS_SEGURO SG WHERE  SG.PC_ID_PC = PC.PC_ID_PC AND SG.MC_COD_MRC = MO.MC_COD_MRC)                                                                                                                                                                              AS ""DtRessarcimentoSeguradora"",
//             (SELECT SUM(SG.SG_VLR_RECEB) FROM   SEGUROS_SEGURO SG WHERE  SG.PC_ID_PC = PC.PC_ID_PC AND    SG.MC_COD_MRC = MO.MC_COD_MRC)                                                                                                                          AS ""ValorRessarcimentoSeguradora"",     
//             (SELECT MAX(SS.SS_DT_VENDA) FROM   SALVADO_SEGURO SS WHERE  SS.PC_ID_PC = PC.PC_ID_PC AND SS.MC_COD_MRC = MO.MC_COD_MRC)                                                                                                                                                                              AS ""DtCreditoSalvadosReguladora"",      
//             (SELECT SUM(SS.SS_TOT_MERC) FROM   SALVADO_SEGURO SS WHERE  SS.PC_ID_PC = PC.PC_ID_PC AND SS.MC_COD_MRC = MO.MC_COD_MRC)                                                                                                                              AS ""ValorCreditoSalvadosReguladora"", 
//             (SELECT MAX(RE.RE_DT_DEPOSITO) FROM   SALVADO_SEGURO SS, RECEITA_SEGURO RE WHERE SS.SS_ID_SS = RE.SS_ID_SS AND    RE.PC_ID_PC = PC.PC_ID_PC AND SS.MC_COD_MRC = MO.MC_COD_MRC AND    RE.UD_ID_UD = UD.UD_ID_UD  )                                                                                     AS ""DtCreditoSalvadosUnidade"",
//             (SELECT SUM(RE.RE_VALOR) FROM   SALVADO_SEGURO SS, RECEITA_SEGURO RE WHERE  SS.SS_ID_SS = RE.SS_ID_SS AND RE.PC_ID_PC = PC.PC_ID_PC AND SS.MC_COD_MRC = MO.MC_COD_MRC AND    RE.UD_ID_UD = UD.UD_ID_UD  )                                            AS ""ValorCreditoSalvadosUnidade"",
//             (SELECT SUM(SS.SS_QTD_SVD) FROM   SALVADO_SEGURO SS WHERE  SS.PC_ID_PC = PC.PC_ID_PC AND    SS.MC_COD_MRC = MO.MC_COD_MRC)                                                                                                                                                                            AS ""TonSalvado"",
//             (SELECT SUM(SS.SS_CUSTO_REG) FROM   SALVADO_SEGURO SS WHERE  SS.PC_ID_PC = PC.PC_ID_PC AND    SS.MC_COD_MRC = MO.MC_COD_MRC)                                                                                                                                                                          AS ""CustoRegulacao"",
//             MO.MO_IND_GAMB                                                                                                                                                                                                                                                                                        AS ""Gambitagem"",
//             (SELECT AN.AS_DT_ANALISE FROM ANALISE_SEGURO AN WHERE AN.UD_ID_UD IS NULL AND AN.PC_ID_PC = PC.PC_ID_PC)                                                                                                                                                                                              AS ""DtAnalise"", 
//             DECODE(UD.UD_DSC_UNID,'INDEVIDO','ENCERRADO SEM PAGAMENTO','A PAGAR')                                                                                                                                                                                                                                 AS ""StatusPagamento"",
//             TRUNC(NVL((SELECT AN.AS_DT_ANALISE FROM   ANALISE_SEGURO AN WHERE  AN.UD_ID_UD IS NULL AND AN.PC_ID_PC = PC.PC_ID_PC),SYSDATE) - PC.PC_DT_SINISTRO)                                                                                                                                                   AS ""TempoAnalise"",
//             PC.PC_VISTORIADOR                                                                                                                                                                                                                                                                                     AS ""Vistoriador"",
//             (SELECT AO.AO_COD_AOP FROM   AREA_OPERACIONAL AO WHERE  AO.AO_ID_AO = MO.AO_ID_ORG)                                                                                                                                                                                                                   AS ""EstacaoOrigem"",
//             (SELECT AO.AO_COD_AOP FROM  FLUXO_COMERCIAL FC INNER JOIN AREA_OPERACIONAL AO  ON(FC.AO_ID_TER_OR = AO.AO_ID_AO) AND AO_TRM_CLI = 'S' WHERE MO.FX_COD_FLX = FC.FX_COD_FLX)                                                                                                                            AS ""TerminalOrigem"",       
//             (SELECT AO.AO_COD_AOP FROM  FLUXO_COMERCIAL FC INNER JOIN AREA_OPERACIONAL AO  ON(FC.AO_ID_TER_DS = AO.AO_ID_AO) AND AO_TRM_CLI = 'S' WHERE MO.FX_COD_FLX = FC.FX_COD_FLX)                                                                                                                            AS ""TerminalDestino"",
//             (SELECT CASE WHEN COUNT(NUMPROCESSO) > 0  THEN 'SIM' ELSE 'NÃO' END AS TFAANEXO FROM ANEXO_TFA WHERE NUMPROCESSO = PC.PC_NUM_PROC)                                                                                                                                                                 AS ""TfaAnexado"",                             
//             (SELECT PC1.PC_DT_PROC FROM PROCESSO_SEGURO PC1 WHERE PC1.PC_DT_PROC < TO_DATE('21/12/2015', 'dd/mm/yyyy') AND PC1.PC_ID_PC = PC.PC_ID_PC)                                                                                                                                                            AS ""dtProcessoLegado""                                                                                              
//               
//                FROM PROCESSO_SEGURO PC                                                                                                            
//                    INNER JOIN MODAL_SEGURO   MO ON (PC.PC_ID_PC = MO.PC_ID_PC)                                                               
//                    INNER JOIN CAUSA_SEGURO   CS ON (MO.CS_ID_CS = CS.CS_ID_CS)                                                               
//                    INNER JOIN RATEIO_SEGURO  RS ON (PC.PC_ID_PC = RS.PC_ID_PC)                                                               
//                    INNER JOIN UNIDADE_SEGURO UD ON (RS.UD_ID_UD = UD.UD_ID_UD)
//                    WHERE 1=1 ");

            //DefinirFiltros(sql, dtInicioProcesso, dtFinalProcesso, vagao, terminal, un, up, malha, provisionado, dtInicioSinistros, dtFinalSinistros, numProcesso, os, situacao, causa, numTermo, anexado);
            //sql.Append(" ORDER BY PC.PC_DT_PROC, PC.PC_ID_PC");

            #endregion

            #region Nova Consulta em tabela Consolidada

            var sql = new StringBuilder(@"  SELECT  DESPACHO as ""Despacho"", 
                                                   SERIEDESPACHO as ""SerieDespacho"", 
                                                   OS  as ""OS"" , 
                                                   NUMPROCESSO  AS ""NumProcesso"", 
                                                   NUMTERMO     AS ""NumTermo"", 
                                                   STATUS   AS ""Status"", 
                                                   VAGAO    AS ""Vagao"", 
                                                   SERIEVAGAO   AS ""SerieVagao"", 
                                                   NF   AS ""NF"", 
                                                   FLUXO    AS ""Fluxo"", 
                                                   UN AS ""UN"", 
                                                   MALHA AS ""Malha"", 
                                                   BITOLA AS ""Bitola"", 
                                                   ESTACAOORIGEM AS ""EstacaoOrigem"", 
                                                   ESTACAODESTINO AS ""EstacaoDestino"", 
                                                   CAUSA AS ""Causa"", 
                                                   CODCLIENTE AS ""CodCliente"", 
                                                   CLIENTE AS ""Cliente"", 
                                                   CODMERCADORIA AS ""CodMercadoria"", 
                                                   MERCADORIA AS ""Mercadoria"", 
                                                   TO_CHAR(DTSINISTRO) AS ""DtSinistro"", 
                                                   TO_CHAR(DTVISTORIA)   AS ""DtVistoria"", 
                                                   TO_CHAR(DTPROCESSO)   AS ""DtProcesso"", 
                                                   TO_CHAR(DTPGTOCLIENTE)  AS ""DtPgtoCliente"",
                                                   UNIDADE  AS ""Unidade"", 
                                                   PERCENTUAL AS ""Percentual"", 
                                                   PESOORIGEM AS ""PesoOrigem"", 
                                                   PERDATONVAGAO AS ""PerdaTonVagao"", 
                                                   TOLERANCIA AS ""Tolerancia"", 
                                                   CONSIDERARTOLERANCIA AS ""ConsiderarTolerancia"", 
                                                   
                                                   CASE WHEN DTPROCESSOLEGADO IS NULL THEN
                                                        CASE WHEN CONSIDERARTOLERANCIA = 'S' THEN NVL(PERDATONVAGAO,0) - ((NVL(TOLERANCIA,0) * NVL(PESOORIGEM,0)) / 100)                                                        
                                                        ELSE NVL(PERDATONVAGAO,0) END
                                                   ELSE NVL(PERDATONVAGAO,0) END AS ""PerdaPagar"",
                                                   
                                                   VALORMERCADORIAUNITARIO AS ""ValorMercadoriaUnitario"", 

                                                   CASE WHEN VALORMERCADORIAUNITARIO IS NOT NULL AND PERDATONVAGAO IS NOT NULL THEN
                                                      CASE WHEN CONSIDERARTOLERANCIA = 'S' THEN NVL(VALORMERCADORIAUNITARIO,0) * (NVL(PERDATONVAGAO,0) - ((NVL(TOLERANCIA,0) * NVL(PESOORIGEM,0)) / 100))
                                                      ELSE (NVL(VALORMERCADORIAUNITARIO,0) * NVL(PERDATONVAGAO,0)) END
                                                   ELSE 0 END AS ""ValorPagarProduto"",

                                                   TARIFAFRETE AS ""TarifaFrete"",
                                                  
                                                   CASE WHEN TARIFAFRETE IS NOT NULL THEN 
                                                      CASE WHEN DTPROCESSOLEGADO IS NULL THEN
                                                            CASE WHEN CONSIDERARTOLERANCIA = 'S' THEN 
                                                                CASE WHEN PERDATONVAGAO IS NOT NULL AND TOLERANCIA IS NOT NULL AND PESOORIGEM IS NOT NULL THEN NVL(PERDATONVAGAO,0) - ((NVL(TOLERANCIA,0) * NVL(PESOORIGEM,0)) / 100)
                                                                ELSE PERDATONVAGAO END
                                                            ELSE PERDATONVAGAO END
                                                      ELSE PERDATONVAGAO END
                                                   ELSE PERDATONVAGAO END * TARIFAFRETE AS ""ValorPagarFrete"",
                                                  
                                                   PISCOFINS AS ""PisCofins"", 
                                                   ICMS AS ""ICMS"",
                                                   
                                                 TRUNC((((CASE WHEN VALORMERCADORIAUNITARIO IS NOT NULL AND PERDATONVAGAO IS NOT NULL THEN
                                                      CASE WHEN CONSIDERARTOLERANCIA = 'S' THEN NVL(VALORMERCADORIAUNITARIO,0) * (NVL(PERDATONVAGAO,0) - ((NVL(TOLERANCIA,0) * NVL(PESOORIGEM,0)) / 100))
                                                      ELSE (NVL(VALORMERCADORIAUNITARIO,0) * NVL(PERDATONVAGAO,0)) END
                                                   ELSE 0 END
                                                    / ((100 - (NVL(PISCOFINS,0) + NVL(ICMS,0))) / 100)) * (NVL(PISCOFINS,0) + NVL(ICMS,0))) / 100),15) AS ""Imposto"",
                                                  
                                                   DESCONTO AS ""Desconto"", 
                                                   MOTIVO AS ""Motivo"",                   
                                                   TRUNC(
                                                    (
                                                     ( CASE WHEN VALORMERCADORIAUNITARIO IS NOT NULL AND PERDATONVAGAO IS NOT NULL THEN
                                                          CASE WHEN CONSIDERARTOLERANCIA = 'S' THEN VALORMERCADORIAUNITARIO * (NVL(PERDATONVAGAO,0) - ((NVL(TOLERANCIA,0) * NVL(PESOORIGEM,0)) / 100))
                                                            ELSE (NVL(VALORMERCADORIAUNITARIO,0) * NVL(PERDATONVAGAO,0)) END
                                                      ELSE 0 END) 
                                                      +
                                                     ( CASE WHEN TARIFAFRETE IS NOT NULL THEN 
                                                          CASE WHEN DTPROCESSOLEGADO IS NULL THEN
                                                                CASE WHEN CONSIDERARTOLERANCIA = 'S' THEN NVL(PERDATONVAGAO,0) - ((NVL(TOLERANCIA,0) * NVL(PESOORIGEM,0)) / 100)
                                                                ELSE NVL(PERDATONVAGAO,0) END
                                                          ELSE NVL(PERDATONVAGAO,0) END
                                                       ELSE NVL(PERDATONVAGAO,0) END * NVL(TARIFAFRETE,0))
                                                       +
                                                        (((CASE WHEN VALORMERCADORIAUNITARIO IS NOT NULL AND PERDATONVAGAO IS NOT NULL THEN
                                                            CASE WHEN CONSIDERARTOLERANCIA = 'S' THEN NVL(VALORMERCADORIAUNITARIO,0) * (NVL(PERDATONVAGAO,0) - ((NVL(TOLERANCIA,0) * NVL(PESOORIGEM,0)) / 100))
                                                            ELSE (NVL(VALORMERCADORIAUNITARIO,0) * NVL(PERDATONVAGAO,0)) END
                                                         ELSE 0 END
                                                          / ((100 - (NVL(PISCOFINS,0) + NVL(ICMS,0))) / 100)) * (NVL(PISCOFINS,0) + NVL(ICMS,0))) / 100)
                                                        -
                                                        DESCONTO),15 )  AS ""TotalVagao"", 
                                                   
                                                   FRANQUIA AS ""Franquia"", 
                                                   GRUPOCAUSA AS ""GrupoCausa"", 
                                                   MESSINISTRO AS ""MesSinistro"", 
                                                   SEGURO AS ""Seguro"", 
                                                   REGULADORA AS ""Reguladora"",
                                                   TO_CHAR(DTRESSARCIMENTOSEGURADORA)   AS ""DtRessarcimentoSeguradora"", 
                                                   VALORRESSARCIMENTOSEGURADORA AS ""ValorRessarcimentoSeguradora"", 
                                                   TO_CHAR(DTCREDITOSALVADOSREGULADORA)  AS ""DtCreditoSalvadosReguladora"", 
                                                   VALORCREDITOSALVADOSREGULADORA AS ""ValorCreditoSalvadosReguladora"", 
                                                   TO_CHAR(DTCREDITOSALVADOSUNIDADE)   AS ""DtCreditoSalvadosUnidade"", 
                                                   VALORCREDITOSALVADOSUNIDADE AS ""ValorCreditoSalvadosUnidade"", 
                                                   TONSALVADO AS ""TonSalvado"", 
                                                   CUSTOREGULACAO AS ""CustoRegulacao"", 
                                                   GAMBITAGEM AS ""Gambitagem"", 
                                                   TO_CHAR(DTANALISE)   AS ""DtAnalise"",  
                                                   STATUSPAGAMENTO AS ""StatusPagamento"", 
                                                   TEMPOANALISE AS ""TempoAnalise"", 
                                                   CASE WHEN DTANALISE IS NULL THEN 'EM ABERTO'
                                                      WHEN TEMPOANALISE > 45 THEN 'ACIMA DE 45 DIAS'
                                                   ELSE 'ABAIXO DE 45 DIAS' END AS ""ClasseAnalise"",
                                                   VISTORIADOR AS ""Vistoriador"", 
                                                   TERMINALORIGEM AS ""TerminalOrigem"", 
                                                   TERMINALDESTINO AS ""TerminalDestino"", 
                                                   CASE WHEN TFAANEXADO = 'TRUE' THEN UNISTR('SIM') ELSE UNISTR('N\00C3O') END AS ""TfaAnexado"",
                                                   SINDICANCIA as ""Sindicancia"",
                                                   DECODE( TFA.STATUS_CLIENTE_CIENTE, 'S', UNISTR('SIM'), 'N', UNISTR('N\00C3O'), null, UNISTR(''))  AS ""Ciente"",
                                                   NUMERO_CTE as ""NumeroCte"",
                                                   TO_CHAR(DATA_CTE) as ""DataCte"",
                                                   EMPRESA_ORIG as ""EmpresaOrigem"",
                                                   EMPRESA_DEST as ""EmpresaDestino"",
                                                   NUM_BALANCA as ""NumeroBalanca""                                                   
                                            FROM   CONSULTA_LAUDOS
                                            LEFT JOIN TFA TFA ON (TFA.PC_ID_PC = CONSULTA_LAUDOS.NUMPROCESSO) 
                                            WHERE 1=1 ");

            DefinirFiltrosExportacao(sql, dtInicioProcesso, dtFinalProcesso, vagao, terminal, un, up, malha, provisionado, dtInicioSinistros, dtFinalSinistros, numProcesso, os, situacao, causa, numTermo, sindicancia, anexado);

            sql.Append(" ORDER BY dtProcesso, ProcessoId");
            #endregion

            #region Acesso a base
            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());

                query.SetResultTransformer(Transformers.AliasToBean<TfaExportaDto>());
                
                return query.List<TfaExportaDto>();

            }
            #endregion
        }

        /// <summary>
        /// Filtros usados em Query de Exportação
        /// </summary>
        public StringBuilder DefinirFiltrosExportacao(StringBuilder sql, DateTime? dtInicioProcesso, DateTime? dtFinalProcesso, string vagao,
                                                                                    string terminal, string un, string up, string malha,
                                                                                    string provisionado, DateTime? dtInicioSinistros, DateTime? dtFinalSinistros,
                                                                                    string numProcesso, string os, string situacao, string causa,
                                                                                    string numTermo, string sindicancia, string anexado)
        {
            if (!string.IsNullOrEmpty(numProcesso))
            {
                sql.Append(" AND NumProcesso IN (" + numProcesso + ")");
            }
            else
            {
                if (dtInicioSinistros.HasValue && dtInicioSinistros.Value != new DateTime())
                {
                    sql.Append(" AND DtSinistro BETWEEN TO_DATE('" + dtInicioSinistros.Value.ToString("dd/MM/yyyy") + " 00:00:00' , 'DD/MM/YYYY HH24:MI:SS')");
                    sql.Append(" AND TO_DATE('" + dtFinalSinistros.Value.ToString("dd/MM/yyyy") + " 23:59:59' , 'DD/MM/YYYY HH24:MI:SS')");

                }
                else
                {
                    if (dtInicioProcesso.HasValue && dtInicioProcesso.Value != new DateTime())
                    {
                        sql.Append(" AND DtProcesso BETWEEN TO_DATE('" + dtInicioProcesso.Value.ToString("dd/MM/yyyy") + " 00:00:00' ,'DD/MM/YYYY HH24:MI:SS')");
                        sql.Append(" AND TO_DATE('" + dtFinalProcesso.Value.ToString("dd/MM/yyyy") + " 23:59:59' ,'DD/MM/YYYY HH24:MI:SS')");
                    }
                }
            }

            if (!string.IsNullOrEmpty(vagao))
            {
                sql.AppendFormat(@" AND 
                            (ProcessoId IN (SELECT MO.PC_ID_PC 
                            FROM MODAL_SEGURO MO, VAGAO VG,PROCESSO_SEGURO PS1, 
                                 ( 
                                   SELECT MAX(ps.pc_timestamp) AS tm 
                                   FROM MODAL_SEGURO MO, VAGAO VG, processo_seguro ps 
                                   WHERE MO.VG_ID_VG = VG.VG_ID_VG 
                                   AND   mo.pc_id_pc = ps.pc_id_pc 
                                   AND VG.VG_COD_VAG IN ({0}) 
                                 )tab 
                            WHERE MO.VG_ID_VG = VG.VG_ID_VG 
                            AND   MO.PC_ID_PC = PS1.PC_ID_PC 
                            --AND   PS1.PC_TIMESTAMP=tab.tm 
                            AND VG.VG_COD_VAG IN ({0})))", vagao.Trim());
            }

            if (!string.IsNullOrEmpty(causa))
            {
                sql.Append(@" AND (ProcessoId IN (SELECT MO.PC_ID_PC
                                                     FROM MODAL_SEGURO MO
                                               WHERE MO.CS_ID_CS = " + causa + "))");
            }

            // Filtros não são mais utilizados
            //if (!string.IsNullOrEmpty(up))
            //{
            //    sql.Append(" AND (UnidadeOrigem = '" + up + "' OR UnidadeDestino = '" + up + "')");
            //}

            //if (!string.IsNullOrEmpty(terminal))
            //{
            //    sql.Append(" AND (UnidadeOrigem = '" + terminal + "' OR UnidadeDestino = '" + terminal + "')");
            //}

            if (!string.IsNullOrEmpty(un))
            {
                sql.Append(" AND (UN='" + un + "')");
            }

            if (!string.IsNullOrEmpty(situacao))
            {
                Dictionary<string, string> situacoes = new Dictionary<string,string>();
                situacoes.Add("P", "PENDENTE");
                situacoes.Add("E", "ENCERRADO SEM PAGAMENTO");
                situacoes.Add("R", "PREPARADO P/ PAGAMENTO");
                situacoes.Add("O", "PAGO");
                situacoes.Add("C", "ENCONTRO DE CONTAS");
               
                var situacaoFiltro = "";

                if (situacoes.ContainsKey(situacao))
                {
                    situacaoFiltro = situacoes[situacao];
                    sql.Append(" AND (Status = '" + situacaoFiltro + "')");
                }
            }

            if (!string.IsNullOrEmpty(os))
            {
                sql.Append(" AND (OS IN (" + os + "))");
            }

            if (!string.IsNullOrEmpty(malha))
            {
                sql.Append(@" AND EXISTS(select up.up_ind_malha                                               
                                           from area_operacional ao,                                     
                                           unid_producao up                                              
                                           where pc.ao_id_org = ao.ao_id_ao                              
                                           and ao.up_id_unp = up.up_id_unp                               
                                           and up_ind_malha = '" + malha + "')");
            }

            if (!string.IsNullOrEmpty(numTermo))
            {
                sql.AppendFormat(@" AND (SELECT distinct
                        FIRST_VALUE(MO_NUM_FALTA) 
                        OVER (ORDER BY MO.MO_ID_MO DESC) AS NumTermo
                        FROM   MODAL_SEGURO MO
                        WHERE (ProcessoId = MO.PC_ID_PC)) IN ('{0}')", numTermo.Trim().Replace(",", "','"));
            }

            if (!string.IsNullOrEmpty(provisionado))
            {
                if (provisionado.ToUpper().Equals("S"))
                {
                    sql.Append(" AND (DtProvisionamento IS NOT NULL)");
                }
                else
                {
                    sql.Append(" AND (DtProvisionamento IS NULL)");
                }
            }

            if (!string.IsNullOrEmpty(anexado))
            {
                if (anexado.ToUpper().Equals("S"))
                {
                    sql.Append(" AND NumProcesso  IN (SELECT NUMPROCESSO FROM ANEXO_TFA WHERE NUMPROCESSO = NumProcesso)");
                }
                else if (anexado.ToUpper().Equals("N"))
                {
                    sql.Append(" AND NumProcesso NOT IN (SELECT NUMPROCESSO FROM ANEXO_TFA WHERE NUMPROCESSO = NumProcesso)");
                }
            }

            if (!string.IsNullOrEmpty(sindicancia))
            {
                sql.Append(" AND SINDICANCIA IN (" + sindicancia.Replace(",,",",").Trim() + ")");
            }

            return sql;

        }

     

        public StringBuilder DefinirFiltros(StringBuilder sql, DateTime? dtInicioProcesso, DateTime? dtFinalProcesso, string vagao,
                                                                                    string terminal, string un, string up, string malha,
                                                                                    string provisionado, DateTime? dtInicioSinistros, DateTime? dtFinalSinistros,
                                                                                    string numProcesso, string os, string situacao, string causa,
                                                                                    string numTermo, string sindicancia, string anexado)
        {
            if (!string.IsNullOrEmpty(numProcesso))
            {
                sql.Append(" AND PC.PC_NUM_PROC IN (" + numProcesso + ")");
            }
            else
            {
                if (dtInicioSinistros.HasValue && dtInicioSinistros.Value != new DateTime())
                {
                    sql.Append(" AND PC.PC_DT_SINISTRO BETWEEN TO_DATE('" + dtInicioSinistros.Value.ToString("dd/MM/yyyy") + " 00:00:00' , 'DD/MM/YYYY HH24:MI:SS')");
                    sql.Append(" AND TO_DATE('" + dtFinalSinistros.Value.ToString("dd/MM/yyyy") + " 23:59:59' , 'DD/MM/YYYY HH24:MI:SS')");

                }
                else
                {
                    if (dtInicioProcesso.HasValue && dtInicioProcesso.Value != new DateTime())
                    {
                        sql.Append(" AND PC.PC_DT_PROC BETWEEN TO_DATE('" + dtInicioProcesso.Value.ToString("dd/MM/yyyy") + " 00:00:00' ,'DD/MM/YYYY HH24:MI:SS')");
                        sql.Append(" AND TO_DATE('" + dtFinalProcesso.Value.ToString("dd/MM/yyyy") + " 23:59:59' ,'DD/MM/YYYY HH24:MI:SS')");
                    }
                }
            }

            if (!string.IsNullOrEmpty(vagao))
            {
                sql.AppendFormat(@" AND 
                            (PC.PC_ID_PC IN (SELECT MO.PC_ID_PC 
                            FROM MODAL_SEGURO MO, VAGAO VG,PROCESSO_SEGURO PS1, 
                                 ( 
                                   SELECT MAX(ps.pc_timestamp) AS tm 
                                   FROM MODAL_SEGURO MO, VAGAO VG, processo_seguro ps 
                                   WHERE MO.VG_ID_VG = VG.VG_ID_VG 
                                   AND   mo.pc_id_pc = ps.pc_id_pc 
                                   AND VG.VG_COD_VAG IN ({0}) 
                                 )tab 
                            WHERE MO.VG_ID_VG = VG.VG_ID_VG 
                            AND   MO.PC_ID_PC = PS1.PC_ID_PC 
                            --AND   PS1.PC_TIMESTAMP=tab.tm 
                            AND VG.VG_COD_VAG IN ({0})))", vagao.Trim());
            }

            if (!string.IsNullOrEmpty(causa))
            {
                sql.Append(@" AND (PC.PC_ID_PC IN (SELECT MO.PC_ID_PC
                                                     FROM MODAL_SEGURO MO
                                               WHERE MO.CS_ID_CS = " + causa + "))");
            }

            // Campos não foram mais utilizados nos filtros
            //if (!string.IsNullOrEmpty(up))
            //{
            //    sql.Append(" AND (PC.UD_ID_ORG = '" + up + "' OR PC.UD_ID_DST = '" + up + "')");
            //}

            //if (!string.IsNullOrEmpty(terminal))
            //{
            //    sql.Append(" AND (PC.UD_ID_ORG = '" + terminal + "' OR PC.UD_ID_DST = '" + terminal + "')");
            //}

            if (!string.IsNullOrEmpty(un))
            {
                sql.AppendFormat(" AND (SELECT UN.UN_COD_UNG FROM FLUXO_COMERCIAL FC, UNID_NEGOCIO UN WHERE FC.UN_ID_UN = UN.UN_ID_UN AND FC.FX_COD_FLX = MO.FX_COD_FLX) = '{0}'", un);
            }

            if (!string.IsNullOrEmpty(situacao))
            {
                if (situacao == "P")
                {
                    sql.Append(" AND (PC.PC_IND_SIT in ('P','U','G'))");
                }
                else
                {
                    sql.Append(" AND (PC.PC_IND_SIT = '" + situacao + "')");
                }
            }

            if (!string.IsNullOrEmpty(os))
            {
                sql.Append(" AND (PC.PC_OS IN (" + os + "))");
            }

            if (!string.IsNullOrEmpty(malha))
            {
                sql.Append(@" AND EXISTS(select up.up_ind_malha                                               
                                           from area_operacional ao,                                     
                                           unid_producao up                                              
                                           where pc.ao_id_org = ao.ao_id_ao                              
                                           and ao.up_id_unp = up.up_id_unp                               
                                           and up_ind_malha = '" + malha + "')");
            }

            if (!string.IsNullOrEmpty(numTermo))
            {
                sql.AppendFormat(@" AND (SELECT distinct
                        FIRST_VALUE(MO_NUM_FALTA) 
                        OVER (ORDER BY MO.MO_ID_MO DESC) AS NumTermo
                        FROM   MODAL_SEGURO MO
                        WHERE (PC.PC_ID_PC = MO.PC_ID_PC)) IN ('{0}')", numTermo.Trim().Replace(",","','"));
            }

            if (!string.IsNullOrEmpty(provisionado))
            {
                if (provisionado.ToUpper().Equals("S"))
                {
                    sql.Append(" AND (PC.DT_PROVISIONAMENTO IS NOT NULL)");
                }
                else
                {
                    sql.Append(" AND (PC.DT_PROVISIONAMENTO IS NULL)");
                }
            }

            if (!string.IsNullOrEmpty(anexado))
            {
                if (anexado.ToUpper().Equals("S"))
                {
                    sql.Append(" AND PC.PC_NUM_PROC  IN (SELECT NUMPROCESSO FROM ANEXO_TFA WHERE NUMPROCESSO = PC.PC_NUM_PROC)");
                }
                else if (anexado.ToUpper().Equals("N"))
                {
                    sql.Append(" AND PC.PC_NUM_PROC NOT IN (SELECT NUMPROCESSO FROM ANEXO_TFA WHERE NUMPROCESSO = PC.PC_NUM_PROC)");
                }
            }

            if (!string.IsNullOrEmpty(sindicancia))
            {
                sql.Append(" AND PC_SINDICANCIA IN (" + sindicancia.Replace(",,", ",").Trim() + ")");
            }

            return sql;

        }

        public IList<UnidadeNegocioDto> ObterListaUnidadeNegocio()
        {
            var sql = new StringBuilder(@"SELECT UN_ID_UN AS ""Id"",
                  UN_COD_UNG AS ""Codigo""
                FROM UNID_NEGOCIO WHERE UN_COD_UNG IN ('UNLG','UNMN','UNPR','UNRS')");

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());

                query.SetResultTransformer(Transformers.AliasToBean<UnidadeNegocioDto>());
                return query.List<UnidadeNegocioDto>();

            }
        }

        /// <summary>
        /// Cria o TFA automaticamente, de acordo com o número do Processo informado
        /// </summary>
        /// <param name="numeroProcesso">Número do Processo para Geração automática do TFA</param>
        /// <returns></returns>
        public Tfa CriarTfaProcesso(int numeroProcesso)
        {
            Tfa novoTfa = null;

            var processoSeguro = _processoSeguroRepository.ObterPorId(numeroProcesso);  
                          
            if (processoSeguro != null)
            {
                var modalSeguro = _modalSeguroRepository.ObterPorProcesso(processoSeguro);

                if (modalSeguro != null)
                {
                    DetachedCriteria criteria = CriarCriteria()
                    .Add(Restrictions.Eq("ProcessoSeguro", processoSeguro));

                    if (!this.Existe(criteria))
                    {
                        var tfa = new Tfa();
                        tfa.ProcessoSeguro = processoSeguro;
                        
                        tfa.TipoLacreTfa = ConverterTipoLacreModalSeguro(modalSeguro.Lacre);
                        tfa.TipoGambitoTfa = ConverterTipoGambitoModalSeguro(modalSeguro.Gambit);
                        tfa.TipoConclusaoTfa = ConverterTipoConclusaoTfa(processoSeguro.IndicadorIndevido);
                        tfa.StatusClienteCiente = false;
                        
                        novoTfa = this.Inserir(tfa);
                    }
                }
            }           

            return novoTfa;
        }


        private TipoLacreTfa ConverterTipoLacreModalSeguro(string lacre)
        {
            int codTipoLacre = 0;

            switch (lacre)
            {
                case "S":
                    codTipoLacre = 1;
                    break;

                case "P":
                    codTipoLacre = 2;
                    break;

                case "N":
                    codTipoLacre = 3;
                    break;

                default:
                    return null;
            }

            using (var session = OpenSession())
            {
                return session.Get(typeof(TipoLacreTfa), codTipoLacre) as TipoLacreTfa;
            }
            
        }

        private TipoGambitoTfa ConverterTipoGambitoModalSeguro(string gambito)
        {
            
            int codTipogambito = 0;

            switch (gambito)
            {
                case "S":
                    codTipogambito = 1;
                    break;

                case "P":
                    codTipogambito = 2;
                    break;

                case "N":
                    codTipogambito = 3;
                    break;

                default:
                    return null;
            }

            using (var session = OpenSession())
            {
                return session.Get(typeof(TipoGambitoTfa), codTipogambito) as TipoGambitoTfa;
            }
        }

        private TipoConclusaoTfa ConverterTipoConclusaoTfa(string indicadorIndevido)
        {
            //lógica inversa, pois flag na tabela é Processo INDEVIDO
            int codTipoConclusao = 0;

            switch (indicadorIndevido)
            {
                case "N":
                    codTipoConclusao = 1;
                    break;

                case "S":
                    codTipoConclusao = 2;
                    break;

                case "C":
                    codTipoConclusao = 3;
                    break;

                default:
                    return null;
            }

            using (var session = OpenSession())
            {
                return session.Get(typeof(TipoConclusaoTfa), codTipoConclusao) as TipoConclusaoTfa;
            }
        }

        public TfaDto ObterConsultaTFAPdf(int numProcesso)
        {

            string sql = String.Format(@"  SELECT 
                                                PC.PC_ID_PC AS ""ProcessoId"",    
                                                (SELECT EP.EP_RAZ_SCL FROM  FLUXO_COMERCIAL FC INNER JOIN EMPRESA EP  ON(FC.EP_ID_EMP_COR = EP.EP_ID_EMP) WHERE MO.FX_COD_FLX = FC.FX_COD_FLX) AS ""Cliente"",
                                                (SELECT EP.EP_DSC_RSM FROM  FLUXO_COMERCIAL FC INNER JOIN EMPRESA EP  ON(FC.EP_ID_EMP_REM = EP.EP_ID_EMP) WHERE MO.FX_COD_FLX = FC.FX_COD_FLX) AS ""TerminalOrigem"",       
                                                (SELECT EP.EP_DSC_RSM FROM  FLUXO_COMERCIAL FC INNER JOIN EMPRESA EP  ON(FC.EP_ID_EMP_DST = EP.EP_ID_EMP) WHERE MO.FX_COD_FLX = FC.FX_COD_FLX) AS ""TerminalDestino"",       
                                                AO.AO_DSC_AOP AS ""Unidade"",
                                                PC.PC_DT_VISTORIA AS ""DtVistoria"",
                                                (SELECT VG.VG_COD_VAG FROM VAGAO VG WHERE VG.VG_ID_VG = MO.VG_ID_VG)  AS ""Vagao"",
                                                (SELECT SEV.SV_COD_SER FROM VAGAO VG LEFT JOIN SERIE_VAGAO SEV ON VG.SV_ID_SV = SEV.SV_ID_SV WHERE VG.VG_ID_VG = MO.VG_ID_VG) AS ""SerieVagao"",
                                                MO.NRO_DESP  AS ""Despacho"",
                                                MO.SERIE_DESP  AS ""SerieDespacho"",                                            
                                                REPLACE(MO.MO_NOTA_FISCAL, ';', ',') AS ""Nf"",  
                                                (SELECT SUM(DC.DC_QTD_INF) FROM   DESPACHO DP, SERIE_DESPACHO SD, DETALHE_CARREGAMENTO DC, ITEM_DESPACHO IDS WHERE  DP.DP_NUM_DSP = MO.NRO_DESP AND    DP.SK_IDT_SRD = SD.SK_IDT_SRD AND    SD.SK_NUM_SRD = MO.SERIE_DESP AND    DP.DP_ID_DP   = IDS.DP_ID_DP AND    IDS.DC_ID_CRG = DC.DC_ID_CRG) AS ""PesoOrigem"",
                                                NVL(TFA.PESO_DESTINO, 0) AS ""PesoDestino"",        
                                                MO.MO_QTD_PERDA AS ""PerdaTonVagao"",
                                                MO.MO_VLR_MERC AS ""ValorMercadoUnitario"",
                                                (SELECT MC.MC_DDT_PT FROM MERCADORIA MC WHERE MC.MC_COD_MRC = MO.MC_COD_MRC) AS ""Mercadoria"",
                                                CS.CS_DSC_CAUSA AS ""Causa"", 
                                                LC.DESC_TIPO_LACRE_TFA AS ""Lacres"",
                                                GB.DESC_TIPO_GAMBITO_TFA AS ""Gambitagem"",                                                
                                                NVL(LD.DESC_TIPO_LIBERADOR_DESCARGA, '') AS ""LiberadoPor"",
                                                NVL(CT.DESC_TIPO_CONCLUSAO_TFA, '') AS ""Conclusao"",
                                                NVL((SELECT H.HS_HISTORICO FROM (SELECT HS.HS_HISTORICO, ROW_NUMBER() OVER (ORDER BY HS.HS_ID_HS) AS LINHA FROM TRANSLOGIC.HISTORICO_SEGURO HS WHERE HS.PC_ID_PC = {0}) H WHERE H.LINHA = 1), ' ') AS ""Historico"",
                                                PC.PC_VISTORIADOR AS ""Vistoriador"",
                                                NVL(TFA.MATRICULA_VISTORIADOR, '') AS ""MatriculaVistoriador"",
                                                NVL(TFA.NOME_REPRESENTANTE, '') AS ""NomeRepresentante"",
                                                NVL(TFA.DOC_REPRESENTANTE, '') AS ""DocRepresentante"",
                                                TFA.DATA_CLIENTE_CIENTE AS ""DataRepresentante"",
                                                TFA.IMG_ASSINATURA_REPRESENTANTE AS ""ImagemAssinaturaRepresentante""
                                             FROM 
                                                TRANSLOGIC.PROCESSO_SEGURO PC                                                                                                            
                                                INNER JOIN TRANSLOGIC.MODAL_SEGURO   MO ON (PC.PC_ID_PC = MO.PC_ID_PC)                                                               
                                                LEFT JOIN TRANSLOGIC.CAUSA_SEGURO   CS ON (MO.CS_ID_CS = CS.CS_ID_CS)                                                               
                                                LEFT JOIN TRANSLOGIC.AREA_OPERACIONAL AO ON (MO.AO_ID_TRM = AO.AO_ID_AO)          
                                                INNER JOIN TRANSLOGIC.TFA TFA ON (PC.PC_ID_PC = TFA.PC_ID_PC)
                                                LEFT JOIN TRANSLOGIC.TIPO_LACRE_TFA LC ON (TFA.COD_TIPO_LACRE_TFA = LC.COD_TIPO_LACRE_TFA)
                                                LEFT JOIN TRANSLOGIC.TIPO_GAMBITO_TFA GB ON (TFA.COD_TIPO_GAMBITO_TFA = GB.COD_TIPO_GAMBITO_TFA)
                                                LEFT JOIN TRANSLOGIC.TIPO_LIBERADOR_DESCARGA LD ON (TFA.COD_TIPO_LIBERADOR_DESCARGA = LD.COD_TIPO_LIBERADOR_DESCARGA)
                                                LEFT JOIN TRANSLOGIC.TIPO_CONCLUSAO_TFA CT ON (TFA.COD_TIPO_CONCLUSAO_TFA = CT.COD_TIPO_CONCLUSAO_TFA)
                                            WHERE 
                                                PC.PC_ID_PC = {0}", numProcesso);

           
            #region Acesso a base
            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql);

                query.SetResultTransformer(Transformers.AliasToBean<TfaDto>());

                return query.UniqueResult<TfaDto>();

            }
            #endregion
        }


        public Tfa ObterPorNumProcesso(int numProcesso)
        {

             DetachedCriteria criteria = CriarCriteria();
                criteria.CreateAlias("ProcessoSeguro", "ps");
                criteria.Add(Restrictions.Eq("ps.NumeroProcesso", numProcesso));

                return ObterPrimeiro(criteria);
           
        }


        /// <summary>
        /// Buscar o Modelo do Tfa pelo número do processo
        /// </summary>
        /// <param name="numProcesso"></param>
        /// <returns></returns>
        public TfaEmailDto ObterModelEmailTfa(int numProcesso)
        {
            var sql = new StringBuilder(@"SELECT
                            MS.PC_ID_PC AS ""NumeroProcesso"",
                            VG.VG_COD_VAG AS ""NumeroVagao"",
                            SV.SV_COD_SER AS ""SerieVagao"",
                            MS.MO_NOTA_FISCAL AS ""NumeroNotaFiscal""
                        FROM
                            MODAL_SEGURO MS
                            INNER JOIN VAGAO VG ON ( MS.VG_ID_VG = VG.VG_ID_VG )
                            INNER JOIN SERIE_VAGAO SV ON ( VG.SV_ID_SV = SV.SV_ID_SV )
                        WHERE
                            MS.PC_ID_PC =" + numProcesso.ToString());

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());
                query.AddScalar("NumeroProcesso", NHibernateUtil.Int32);
                query.AddScalar("NumeroVagao", NHibernateUtil.String);
                query.AddScalar("SerieVagao", NHibernateUtil.String);
                query.AddScalar("NumeroNotaFiscal", NHibernateUtil.String);

                query.SetResultTransformer(Transformers.AliasToBean<TfaEmailDto>());
                return query.UniqueResult<TfaEmailDto>();

            }
        }


        /// <summary>
        /// Buscar o TFA pelo número do idTfaCache
        /// </summary>
        /// <param name="idTfaCache">Id do TfaCache</param>
        /// <returns></returns>
        public Tfa ObterPorIdTfaCache(int idTfaCache)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.Eq("IdTfaCache", idTfaCache));

            return ObterPrimeiro(criteria);
        }

        /// <summary>
        /// Realizar atualização de status de processos
        /// </summary>
        /// <param name="processos">Números de processos separados por vírgula</param>
        public void AtualizarProcessoSeguro(string processos, DateTime dtPrepPagamento, string formaPagamento, DateTime dtPagamento, DateTime dtEncerramento, string usuario)
        {
            #region Consulta

            var hql = new StringBuilder();
            hql.AppendFormat(@"UPDATE PROCESSO_SEGURO PS
                                            SET 
                                            PS.PC_DT_PREPG     = TO_DATE('{1}', 'DD/MM/YYYY'),
                                            PS.PC_FORMA_PAGTO  = '{2}',
                                            PS.PC_DT_PAGAMENTO = TO_DATE('{3}', 'DD/MM/YYYY'),
                                            PS.PC_DT_ENCERRADO = TO_DATE('{4}', 'DD/MM/YYYY'),
                                            PS.US_USR_IDU      = trim('{5}'), 
                                            PS.PC_TIMESTAMP    = SYSDATE,
						                    PS.PC_IND_SIT      = 'O'
                                            WHERE PS.PC_ID_PC IN({0})", processos, dtPrepPagamento.ToString("dd/MM/yyyy"), formaPagamento, dtPagamento.ToString("dd/MM/yyyy"), dtEncerramento.ToString("dd/MM/yyyy"), usuario);


            #endregion

            #region Acessa a base

            using (var session = OpenSession())
            {
                var parameters = new List<Action<IQuery>>();
                //parameters.Add(q => q.SetString("processos", processos));

                var query = session.CreateSQLQuery(string.Format(hql.ToString()));
                var queryCount = session.CreateSQLQuery(String.Format(@"SELECT COUNT(1) FROM ({0})", query.ToString()));

                foreach (var p in parameters)
                {
                    p.Invoke(query);
                    p.Invoke(queryCount);
                }

                query.ExecuteUpdate();
            }
            #endregion
        }

        /// <summary>
        /// Realizar atualização de status de processos
        /// </summary>
        /// <param name="processos">Números de processos separados por vírgula</param>
        public void AtualizarAnaliseSeguro(string processos, string analiseProcesso, string usuario)
        {

            #region Incluir

            var hql = new StringBuilder();
            hql.AppendFormat(@"INSERT INTO ANALISE_SEGURO SG
                                                    (SG.AS_ID_AS,
                                                     SG.PC_ID_PC,
                                                     SG.UD_ID_UD,
                                                     SG.AS_ANALISE,
                                                     SG.AS_IND_SIT,
                                                     SG.AS_TIMESTAMP,
                                                     SG.US_USR_IDU,
                                                     SG.AS_DT_ANALISE)
                                       VALUES 
                                       (ANALISE_SEGURO_SEQ_ID.NEXTVAL,
                                        '{0}',
                                        (SELECT RS.UD_ID_UD AS UNIDADE FROM RATEIO_SEGURO RS WHERE RS.PC_ID_PC = '{0}'),
                                        '{1}',
                                        '{2}',
                                        SYSDATE,
                                        '{3}',
                                        SYSDATE)", processos, analiseProcesso, "O", usuario);
            #endregion

            #region Acessa a base

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(string.Format(hql.ToString()));
                var queryCount = session.CreateSQLQuery(String.Format(@"SELECT COUNT(1) FROM ({0})", query.ToString()));

                query.ExecuteUpdate();
            }
            #endregion
        }
    }
}