﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ALL.Core.AcessoDados;
using Translogic.Modules.Core.Domain.Model.Tfa.Repositories;
using Translogic.Modules.Core.Domain.Model.Tfa;

namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Tfa
{
    public class LogTfaRepository : NHRepository<LogTfa, int>, ILogTfaRepository
    {
    }
}