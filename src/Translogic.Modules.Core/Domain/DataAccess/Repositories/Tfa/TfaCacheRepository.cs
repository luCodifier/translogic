﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ALL.Core.AcessoDados;
using Translogic.Modules.Core.Domain.Model.Tfa;
using Translogic.Modules.Core.Domain.Model.Tfa.Repositories;

namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Tfa
{
    public class TfaCacheRepository : NHRepository<TfaCache, int>, ITfaCacheRepository
    {
    }
}