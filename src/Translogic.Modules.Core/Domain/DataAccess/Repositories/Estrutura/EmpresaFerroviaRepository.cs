namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Estrutura
{
	using System;
	using ALL.Core.AcessoDados;
	using Model.Estrutura;
	using Model.Estrutura.Repositories;
	using NHibernate;
	using NHibernate.Criterion;
	using NHibernate.SqlCommand;

	/// <summary>
	/// Implementa��o de reposit�rio de empresa ferrovia com NH
	/// </summary>
	public class EmpresaFerroviaRepository : NHRepository<EmpresaFerrovia, int?>, IEmpresaFerroviaRepository
	{
		/// <summary>
		/// Obt�m a empresa ferrovia pela sigla da empresa
		/// </summary>
		/// <param name="siglaEmpresaFerrovia">Sigla da empresa Ferrovia</param>
		/// <returns>Retorna a empresa ferrovia</returns>
		public EmpresaFerrovia ObterPorSigla(string siglaEmpresaFerrovia)
		{
			DetachedCriteria criteria = CriarCriteria();
			criteria.Add(Restrictions.Eq("Sigla", siglaEmpresaFerrovia));
			return ObterPrimeiro(criteria);
		}

		/// <summary>
		/// Obt�m a empresa ferrovia pela sigla da empresa e pela unidade federativa
		/// </summary>
		/// <param name="siglaEmpresaFerrovia">Sigla da empresa Ferrovia</param>
		/// <param name="uf">Unidade Federativa</param>
		/// <returns>Retorna a empresa ferrovia</returns>
		public EmpresaFerrovia ObterPorSiglaUf(string siglaEmpresaFerrovia, string uf)
		{
			DetachedCriteria criteria = CriarCriteria();
			criteria.CreateAlias("Estado", "est", JoinType.InnerJoin);
			criteria.Add(Restrictions.Like("Sigla", string.Concat(siglaEmpresaFerrovia, "%")));
			criteria.Add(Restrictions.Eq("est.Sigla", uf));
			criteria.SetFetchMode("est", FetchMode.Eager);

			return ObterPrimeiro(criteria);
		}
	}
}