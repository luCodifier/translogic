﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Estrutura
{
    using System;
    using ALL.Core.AcessoDados;
    using Model.Estrutura;
    using Model.Estrutura.Repositories;
    using NHibernate.Criterion;

    /// <summary>
    /// Implementação de repositório de EmpresaClientePais
    /// </summary>
    public class EmpresaClientePaisRepository : NHRepository<EmpresaClientePais, int?>, IEmpresaClientePaisRepository
    {
        /// <summary>
        /// Obtém o país da empresa
        /// </summary>
        /// <param name="empresa">Empresa cliente</param>
        /// <returns>Objeto EmpresaClientePais</returns>
        public EmpresaClientePais ObterPorEmpresa(EmpresaCliente empresa)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.Eq("EmpresaCliente.Id", empresa.Id));
            return ObterPrimeiro(criteria);
        }
    }
}
