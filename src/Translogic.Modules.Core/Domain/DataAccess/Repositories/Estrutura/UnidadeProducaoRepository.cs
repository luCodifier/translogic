﻿
namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Estrutura
{
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;
	using Model.Estrutura;
	using Model.Estrutura.Repositories;
	using NHibernate.Criterion;
	using Translogic.Modules.Core.Domain.Model.Diversos;

	/// <summary>
	/// Implementação de repositório de Unidade de Produção
	/// </summary>
	public class UnidadeProducaoRepository : NHRepository<UnidadeProducao, int?>, IUnidadeProducaoRepository
	{
		/// <summary>
		/// Obtem todas as unidades de produção.
		/// </summary>
		/// <param name="malha"> A malha onde está o ponto de abastecimento </param>			
		/// <returns>Lista de Unidade de produção</returns>
		public IList<UnidadeProducao> ObterPorMalha(Malha malha)
		{
			DetachedCriteria criteria = CriarCriteria();

			if (malha != null)
			{
				criteria.Add(Restrictions.Eq("Malha", malha));
			}

			return ObterTodos(criteria);
		}

		/// <summary>
		/// Obtem todas as unidades de produção filtrando por Id.
		/// </summary>
		/// <param name="listaIdsUps">Lista de Id para filtro </param>			
		/// <returns>Lista de Unidade de produção</returns>
		public IList<UnidadeProducao> ObterPorListaId(int[] listaIdsUps)
		{
			DetachedCriteria criteria = CriarCriteria();

			criteria.Add(Restrictions.In("Id", listaIdsUps));
			
			return ObterTodos(criteria);
		}
	}
}
