namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Estrutura
{
	using ALL.Core.AcessoDados;
	using Model.Estrutura;
	using Model.Estrutura.Repositories;

	using NHibernate.Criterion;

    /// <summary>
	/// Implementação do repositório de empresa
	/// </summary>
	public class EmpresaRepository : NHRepository<Empresa, int?>, IEmpresaRepository
	{
        /// <summary>
        /// Retorna a Empresa por CNPJ
        /// </summary>
        /// <param name="cnpj">CNPJ da Empresa</param>
        /// <returns>Objeto Empresa</returns>
	    public Empresa ObterPorCnpj(string cnpj)
	    {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.Eq("Cgc", cnpj));
            return ObterPrimeiro(criteria);
	    }
	}
}