namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Estrutura
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using ALL.Core.AcessoDados;
    using ALL.Core.AcessoDados.TiposCustomizados;
    using Model.Estrutura;
    using Model.Estrutura.Repositories;
    using NHibernate;
    using NHibernate.Criterion;
    using NHibernate.Linq;
    using NHibernate.Transform;

    /// <summary>
    /// Implementa��o reposit�rio de empresa cliente com NH
    /// </summary>
    public class EmpresaClienteRepository : NHRepository<EmpresaCliente, int?>, IEmpresaClienteRepository
    {
        /// <summary>
        /// M�todo para obter a empresa cliente por cnpj
        /// </summary>
        /// <param name="cnpj"> Cnpj da empresa </param>
        /// <returns> Retorna e empresa cliente </returns>
        public EmpresaCliente ObterPorCnpj(string cnpj)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.Eq("Cgc", cnpj));
            var retorno = ObterTodos(criteria);

            if (retorno.Count > 0)
            {
                return retorno[0];
            }

            return null;
        }

        /// <summary>
        /// Obtem empresa cliente pela sigla do SAP
        /// </summary>
        /// <param name="siglaSap">Sigla do SAP </param>
        /// <returns>Retorna a empresa cliente</returns>
        public EmpresaCliente ObterPorSiglaSap(string siglaSap)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.Eq("Sigla", siglaSap));
            return ObterUnico(criteria);
        }

        /// <summary>
        /// Obt�m a empresa pelo CNPJ
        /// </summary>
        /// <param name="cnpj"> Cnpj da empresa. </param>
        /// <param name="codigoUf"> Codigo da Uf. </param>
        /// <returns> Retorna objeto empresa cliente </returns>
        public EmpresaCliente ObterPorCnpj(string cnpj, string codigoUf)
        {
            using (ISession session = OpenSession())
            {
                string hql = @"FROM EmpresaCliente ec
                              WHERE 
                              ec.Cgc = :cnpj
                              AND ec.Estado.Sigla = :codigoUf";
                IQuery query = session.CreateQuery(hql);
                query.SetString("cnpj", cnpj);
                query.SetString("codigoUf", codigoUf);
                query.SetResultTransformer(Transformers.DistinctRootEntity);

                IList<EmpresaCliente> lista = query.List<EmpresaCliente>();
                if (lista.Count > 0)
                {
                    return lista[0];
                }

                return null;
            }
        }

        /// <summary>
        /// Obt�m todas empresas que possuam o cnpj
        /// </summary>
        /// <param name="cnpj">Cnpj da empresa</param>
        /// <returns>Lista de empresas</returns>
        public IList<EmpresaCliente> ObterEmpresasPorCnpj(string cnpj)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.Eq("Cgc", cnpj));
            return ObterTodos(criteria);
        }

        /// <summary>
        /// Obt�m todas empresas cujo Cnpj cont�m uma parte do cnpj informado como par�metro
        /// </summary>
        /// <param name="cnpj">Cnpj da empresa (Parcial ou Completo)</param>
        /// <returns>Lista de empresas</returns>
        public IList<EmpresaCliente> ObterEmpresasContemCnpj(string cnpj)
        {
            using (var session = OpenSession())
            {
                var tq = cnpj.Trim().ToUpper();
                var qr = from em in session.Query<EmpresaCliente>()
                         where em.Cgc.ToUpper().Contains(tq)
                         && em.FaturamentoBloqueado == false
                         select em;

                return qr.Distinct().ToList();
            }
        }

        /// <summary>
        /// Obt�m a empresa Cliente pelo cnpj da ferrovia
        /// </summary>
        /// <param name="cnpj">CNPJ a ser consultado</param>
        /// <returns>Objeto EmpresaCliente</returns>
        public Empresa ObterEmpresaClientePorCnpjDaFerrovia(string cnpj)
        {
            using (ISession session = OpenSession())
            {
                string hql = @"Select emp FROM IEmpresa emp, 
                              SerieDespachoUf sdu 
                              WHERE 
                              emp.Cgc = sdu.CnpjFerrovia 
                              AND LENGTH(SUBSTR(emp.Sigla, INSTR(emp.Sigla, '-' )+ 1 )) = 1 
                              AND INSTR(emp.Sigla, '-' ) > 0 
                              AND emp.Cgc = :cnpjFerrovia ";

                IQuery query = session.CreateQuery(hql);
                query.SetString("cnpjFerrovia", cnpj);

                query.SetResultTransformer(Transformers.DistinctRootEntity);

                Empresa empresaCliente = (Empresa)query.UniqueResult();

                return empresaCliente;
            }
        }

        /// <summary>
        /// Obt�m a empresa Cliente pelo cnpj ou razao social
        /// </summary>
        /// <param name="cnpj">CNPJ a ser consultado</param>
        /// <param name="razaoSocial">Razao Social da empresa</param>
        /// <returns>Objeto EmpresaCliente</returns>
        public IList<EmpresaCliente> ObterEmpresaClientePorCnpjRazaoSocialDaFerrovia(string cnpj, string razaoSocial)
        {
            using (ISession session = OpenSession())
            {
                string hql =
                        @"FROM EmpresaCliente ec
                              WHERE 
                                 (UPPER(ec.RazaoSocial) LIKE :razaoSocial
                                 OR UPPER(ec.NomeFantasia) like :razaoSocial)
                                 AND FaturamentoBloqueado = :tpoFatura
                                 AND LENGTH(SUBSTR(ec.Sigla, INSTR(ec.Sigla, '-' )+ 1 )) = 1 
								 AND INSTR(ec.Sigla, '-' ) > 0 ";

                if (!string.IsNullOrEmpty(cnpj))
                {
                    hql += @" AND ec.Cgc = :cnpj";
                }

                IQuery query = session.CreateQuery(hql);

                if (!string.IsNullOrEmpty(cnpj))
                {
                    query.SetString("cnpj", cnpj);
                }

                query.SetParameter("tpoFatura", false, new BooleanCharType());
                query.SetString("razaoSocial", string.Concat(razaoSocial.ToUpperInvariant(), "%"));

                query.SetResultTransformer(Transformers.DistinctRootEntity);

                IList<EmpresaCliente> empresaCliente = query.List<EmpresaCliente>();
                return empresaCliente;
            }
        }

        /// <summary>
        /// Obt�m a empresa Cliente pelo cnpj da ferrovia
        /// </summary>
        /// <param name="cnpj">CNPJ a ser consultado</param>
        /// <param name="uf">Unidade Federativa</param>
        /// <returns>Objeto EmpresaCliente</returns>
        public Empresa ObterEmpresaClientePorCnpjDaFerrovia(string cnpj, string uf)
        {
            using (ISession session = OpenSession())
            {
                string hql = @"Select emp 
											FROM IEmpresa emp, 
											SerieDespachoUf sdu,
											Estado e 
											WHERE emp.Cgc = sdu.CnpjFerrovia 
											AND LENGTH(SUBSTR(emp.Sigla, INSTR(emp.Sigla, '-' )+ 1 )) = 1 
											AND INSTR(emp.Sigla, '-' ) > 0 
											AND emp.Cgc = :cnpjFerrovia 
											AND emp.Estado.Sigla = :uf";

                IQuery query = session.CreateQuery(hql);
                query.SetString("cnpjFerrovia", cnpj);
                query.SetString("uf", uf);

                query.SetResultTransformer(Transformers.DistinctRootEntity);

                var resultado = query.List();

                Empresa empresaCliente = null;

                if (resultado.Count > 0)
                {
                    empresaCliente = (Empresa)query.List()[0];
                }

                return empresaCliente;
            }
        }

        /// <summary>
        /// Lista de cliente
        /// </summary>
        /// <param name="query"> Filtro para os clientes</param>
        /// <returns>Lista de clientes</returns>
        public List<EmpresaCliente> ObterListaDescResumida(string query)
        {
            using (var session = OpenSession())
            {
                var tq = query.Trim().ToUpper();
                var qr = from em in session.Query<EmpresaCliente>()
                         where em.DescricaoResumida.ToUpper().Contains(tq)
                         && em.FaturamentoBloqueado == false
                         select em;

                return qr.Distinct().ToList();
            }
        }

        /// <summary>
        /// Lista de cliente - Auto complete bem r�pido
        /// </summary>
        /// <param name="query"> Filtro para os clientes</param>
        /// <returns>Lista de clientes</returns>
        public List<EmpresaPesquisaPorNome> ObterListaNomesDescResumida(string query)
        {
            using (var session = OpenSession())
            {
                var tq = query.Trim().ToUpper();
                var qr = from em in session.Query<EmpresaCliente>()
                         where em.DescricaoResumida.ToUpper().Contains(tq)
                         && em.FaturamentoBloqueado == false
                         select new EmpresaPesquisaPorNome()
                         {
                             Id = (int)em.Id,
                             DescricaoResumida = em.DescricaoResumida,
                             Cgc = em.Cgc
                         };

                var temp = qr.ToList();

                return temp.Distinct().ToList();
            }
        }

        /// <summary>
        /// Lista de Terminais
        /// </summary>
        /// <param name="query"> Filtro para os terminais</param>
        /// <returns>Lista de terminais</returns>
        public List<EmpresaCliente> ObterListaNomesTerminais(string query)
        {
            using (var session = OpenSession())
            {
                var tq = query.Trim().ToUpper();
                var qr = from em in session.Query<EmpresaCliente>()
                         where (em.DescricaoResumida.ToUpper().Contains(tq) || em.Cgc.ToUpper().Contains(tq))
                         && em.FaturamentoBloqueado == false
                         select em;

                return qr.Distinct().ToList();
            }
        }
               
        public List<TaraEmpresa> GetAllTarasEmpresa()
        {
            using (ISession session = OpenSession())
            {
                var sql = @"SELECT BTE.ID_TARA_EMPRESA AS ""IdTaraEmpresa"", BTE.EP_ID_EMP AS ""IdEmpresa"" from BUSCA_TARA_EMPRESA BTE ";
                IQuery query = session.CreateSQLQuery(sql);
                query.SetResultTransformer(Transformers.AliasToBean<TaraEmpresa>());
                IList<TaraEmpresa> lista = query.List<TaraEmpresa>();

                if (lista.Count > 0)
                {
                    return lista.ToList();
                }

                return null;
            }
        }

    }
}