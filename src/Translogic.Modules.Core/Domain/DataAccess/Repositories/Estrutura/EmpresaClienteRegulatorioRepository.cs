﻿

namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Estrutura
{
    using System.Collections.Generic;
    using System.Linq;
    using ALL.Core.AcessoDados;
    using ALL.Core.Dominio;
    using Model.Diversos;
    using Model.Diversos.Ibge;
    using NHibernate;
    using NHibernate.Criterion;
    using NHibernate.Linq;
    using Translogic.Modules.Core.Domain.Model.Estrutura;
    using Translogic.Modules.Core.Domain.Model.Estrutura.Repositories;

    /// <summary>
    /// Repositório de clientes
    /// </summary>
    public class EmpresaClienteRegulatorioRepository : NHRepository<Empresa, int?>, IEmpresaClienteRegulatorioRepository
    {
        /// <summary>
        /// Lista os clientes por razão social
        /// </summary>
        /// <param name="razaoSocial">Nome a pesquisar</param>
        /// <returns>Clientes resultantes</returns>
        public IEnumerable<Empresa> ListarPorRazaoSocial(string razaoSocial)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.Like("RazaoSocial", razaoSocial));
            criteria.SetMaxResults(10);
            return this.ObterTodos(criteria);
        }

        /// <summary>
        /// Lista os clientes por CNPJ
        /// </summary>
        /// <param name="cnpj">CNPJ a pesquisar</param>
        /// <returns>Clientes resultantes</returns>
        public IEnumerable<Empresa> ListarPorCNPJ(string cnpj)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.Like("Cgc", cnpj));
            criteria.SetMaxResults(10);
            return this.ObterTodos(criteria);
        }

        /// <summary>
        /// Obtem o cliente pelo código
        /// </summary>
        /// <param name="sigla">Código a pesquisar</param>
        /// <returns>Cliente resultante</returns>
        public IEnumerable<Empresa> ListarPorSigla(string sigla)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.Like("Sigla", sigla));
            criteria.SetMaxResults(10);
            return this.ObterTodos(criteria);
        }

        /// <summary>
        /// Método para Obter os Dados da Tabela Categorias
        /// </summary>
        /// <param name="pagination"> Paginação da Grid </param>
        /// <param name="razaoSocial"> Razão social </param>
        /// <param name="nomeFantasia"> Nome fantasia </param>
        /// <param name="cnpj"> Cnpj da empresa </param>         
        /// <returns> Retorna Ilist de Categorias </returns>
        public ResultadoPaginado<Empresa> ObterPorParametro(DetalhesPaginacao pagination, string razaoSocial, string nomeFantasia, string cnpj)
        {
            DetachedCriteria criteria = CriarCriteria();
            if (!string.IsNullOrEmpty(razaoSocial))
            {
                criteria.Add(Restrictions.Like("RazaoSocial", "%" + razaoSocial.ToUpper() + "%"));
            }

            if (!string.IsNullOrEmpty(nomeFantasia))
            {
                criteria.Add(Restrictions.Like("NomeFantasia", "%" + nomeFantasia.ToUpper() + "%"));
            }

            if (!string.IsNullOrEmpty(cnpj))
            {
                criteria.Add(Restrictions.Eq("Cgc", cnpj));
            }

            return ObterPaginado(criteria, pagination);
        }

        /// <summary>
        /// Lista todos os estados (UFs)
        /// </summary>
        /// <returns>Listagem dos estados</returns>
        public IEnumerable<Estado> ListarEstados()
        {
            ISession sessao = this.OpenSession();
            return sessao.Query<Estado>().ToList();
        }

        /// <summary>
        /// Lista as cidades de um determinado estado
        /// </summary>
        /// <param name="sigla">Estado desejado</param>
        /// <returns>Cidades do estado</returns>
        public IEnumerable<CidadeIbge> ListarCidades(string sigla)
        {
            ISession sessao = this.OpenSession();
            return sessao.Query<CidadeIbge>().Where(e => e.SiglaEstado == sigla).ToList();
        }

        /// <summary>
        /// Obtem o estado (UF)
        /// </summary>
        /// <param name="sigla">Estado desejado</param>
        /// <returns>Obtem o estado</returns>
        public Estado ObterEstado(string sigla)
        {
            ISession sessao = this.OpenSession();
            return sessao.Query<Estado>().Where(e => e.Sigla == sigla).FirstOrDefault();
        }

        /// <summary>
        /// Obtem a cidade
        /// </summary>
        /// <param name="cidadeId">Id da cidade desejada</param>
        /// <returns>Obtem a cidade desejada</returns>
        public CidadeIbge ObterCidade(int cidadeId)
        {
            ISession sessao = this.OpenSession();
            return sessao.Query<CidadeIbge>().Where(e => e.Id == cidadeId).FirstOrDefault();
        }
    }
}